﻿$PBExportHeader$w_carico_acquisti.srw
$PBExportComments$Finestra Carico Acquisti - Creazione Bolle Acquisto da Ordini Acquisto
forward
global type w_carico_acquisti from w_cs_xx_principale
end type
type dw_ext_label from datawindow within w_carico_acquisti
end type
type tab_1 from tab within w_carico_acquisti
end type
type det_1 from userobject within tab_1
end type
type dw_ext_ddt_file from u_dw_search within det_1
end type
type cb_ricerca from commandbutton within det_1
end type
type cb_reset from commandbutton within det_1
end type
type cbx_righe_libere from checkbox within det_1
end type
type cbx_quan_ordinata from checkbox within det_1
end type
type cbx_quan_arrivata from checkbox within det_1
end type
type cbx_data_consegna from checkbox within det_1
end type
type cbx_cod_prod_fornitore from checkbox within det_1
end type
type dw_ext_carico_acquisti from u_dw_search within det_1
end type
type det_1 from userobject within tab_1
dw_ext_ddt_file dw_ext_ddt_file
cb_ricerca cb_ricerca
cb_reset cb_reset
cbx_righe_libere cbx_righe_libere
cbx_quan_ordinata cbx_quan_ordinata
cbx_quan_arrivata cbx_quan_arrivata
cbx_data_consegna cbx_data_consegna
cbx_cod_prod_fornitore cbx_cod_prod_fornitore
dw_ext_carico_acquisti dw_ext_carico_acquisti
end type
type det_2 from userobject within tab_1
end type
type rb_attiva_scarico_terzista from radiobutton within det_2
end type
type rb_niente from radiobutton within det_2
end type
type rb_attiva_scarico from radiobutton within det_2
end type
type cbx_ddt from checkbox within det_2
end type
type dw_depositi from uo_std_dw within det_2
end type
type cb_modifica from commandbutton within det_2
end type
type cb_salda_righe_vuote from commandbutton within det_2
end type
type cb_mem_prezzo from commandbutton within det_2
end type
type cb_genera_fatture from commandbutton within det_2
end type
type cb_genera_bolle from commandbutton within det_2
end type
type cb_etichette from commandbutton within det_2
end type
type dw_ordini_acquisto_lista from uo_cs_xx_dw within det_2
end type
type dw_ext_carico_acq_det from uo_std_dw within det_2
end type
type det_2 from userobject within tab_1
rb_attiva_scarico_terzista rb_attiva_scarico_terzista
rb_niente rb_niente
rb_attiva_scarico rb_attiva_scarico
cbx_ddt cbx_ddt
dw_depositi dw_depositi
cb_modifica cb_modifica
cb_salda_righe_vuote cb_salda_righe_vuote
cb_mem_prezzo cb_mem_prezzo
cb_genera_fatture cb_genera_fatture
cb_genera_bolle cb_genera_bolle
cb_etichette cb_etichette
dw_ordini_acquisto_lista dw_ordini_acquisto_lista
dw_ext_carico_acq_det dw_ext_carico_acq_det
end type
type det_3 from userobject within tab_1
end type
type cb_avan_produzione from commandbutton within det_3
end type
type dw_produzione from datawindow within det_3
end type
type det_3 from userobject within tab_1
cb_avan_produzione cb_avan_produzione
dw_produzione dw_produzione
end type
type det_4 from userobject within tab_1
end type
type dw_avan_produzione from datawindow within det_4
end type
type det_4 from userobject within tab_1
dw_avan_produzione dw_avan_produzione
end type
type det_5 from userobject within tab_1
end type
type dw_scarico_terzista from datawindow within det_5
end type
type det_5 from userobject within tab_1
dw_scarico_terzista dw_scarico_terzista
end type
type tab_1 from tab within w_carico_acquisti
det_1 det_1
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
end type
end forward

global type w_carico_acquisti from w_cs_xx_principale
integer width = 4046
integer height = 2592
string title = "Carico Acquisti"
event ue_initial_size ( )
dw_ext_label dw_ext_label
tab_1 tab_1
end type
global w_carico_acquisti w_carico_acquisti

type variables
boolean ib_proviene_da_bolla = false
string is_cod_fornitore
long il_num_bolla_acq
integer ii_anno_bolla_acq
long il_progressivo
string is_cod_doc_origine
string is_numeratore_doc_origine
integer ii_anno_doc_origine
long il_num_doc_origine
long il_prog_fattura
long il_anno_reg_fat_acq
long il_num_reg_fat_acq
long il_prezzi_modificati = 0
datawindow idw_elenco_prodotti_entrata
string is_cod_parametro_blocco_prodotto="CAQ"

// stefanop 07/07/2010: progetto 140
private:
	long il_color_green = 8573565
	long il_color_red = 6911983
	string is_cod_tipo_movimento_grezzo
	string is_cod_deposito
	boolean ib_scarico_grezzi = true
	boolean ib_upper_size = true
	long ll_offset_height
	string id_cod_depostio_documento
	boolean ib_load_dddw_depositi = true
	
	//attivazione creazione ddt ingresso da terzista, con carco prodotto della riga e scarico MP dal deposito del terzista
	//questa cosa sarà possibile leggendo la commessa sulla riga del ddt (proveniente dall'ordine di acquisto)
	//si tratta di effettuare un avanzamento parziale del ramo commessa
	//quindi la conferma del ddt (con commesse sulle righe causa questo e non il semplice movimento di magazzino)
	boolean ib_SPT = false
	
end variables

forward prototypes
public function integer wf_carica_dett_fatture (ref string fs_messaggio)
public function integer wf_carica_dett_bolle (ref string fs_messaggio, integer fi_anno_bolla_acq, long fl_num_bolla_acq)
public function integer wf_genera_bolle_acq (ref string fs_messaggio)
public function integer wf_calcola_stato_ordine (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_genera_fatture_acq (ref string fs_messaggio)
public function integer wf_avanzamento_produzione (ref string fs_messaggio)
public function integer wf_cerca_materie_prime (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_in_produzione, long fl_num_livello_cor, ref string fs_cod_materia_prima[], ref decimal fd_quan_mp[], ref string fs_errore)
public function integer wf_carica_tipo_movimento_grezzo (boolean ab_fat_acq)
public function integer wf_carica_deposito (string as_cod_fornitore)
public function integer wf_get_giacenza_deposito (string fs_cod_deposito, string fs_cod_prodotto, ref decimal fd_giacenza)
public function integer wf_aggiorna_mov_grezzo_referenza (long fl_anno_mov, long fl_numero_mov, datetime fdt_data_doc, string fs_num_documento, string fs_referenza)
public function window get_window ()
public function integer wf_ricalcola_giacenze ()
public function integer wf_genera_bolla_trasferimento (long al_anno, long al_numero, string as_table)
public function string wf_get_deposito_fornitore (string as_cod_fornitore)
public function integer wf_seleziona_file_ddt (string fs_cod_fornitore, ref string fs_ritorno)
public subroutine wf_check_before_import (string fs_cod_fornitore, string fs_path_file)
public function integer wf_prepara_tracciato_file_ddt (integer fi_tipo_tracciato, ref long fl_fields[], ref string fs_errore)
public function integer wf_carica_ds_da_file_ddt (string fs_values[], integer fi_tipo_tracciato, ref datastore fds_data, ref string fs_errore)
public function integer wf_carica_da_ddt_file (ref datastore fds_data, ref string fs_errore)
public function integer wf_aggiungi_da_ddt (datastore fds_data, ref string fs_errore)
public subroutine wf_aggiungi_riga (integer row, boolean fb_from_file_ddt)
public function integer wf_salva_fat_conv_acq (ref string as_errore)
public function integer wf_get_info_riga_ordine (integer al_anno_reg_acq, long al_num_reg_acq, long al_prog_riga_acq, ref string as_cod_prodotto, ref string as_des_prodotto, ref decimal ad_quan_ordinata, ref decimal ad_quan_arrivata, ref string as_cod_misura, ref decimal ad_prezzo_acquisto, ref decimal ad_fat_conversione_acq, ref string as_errore)
public function integer wf_fat_conversione (string as_cod_misura_mag, decimal ad_quan_ddt, decimal ad_quan2_ddt, long al_misura_in_metri, ref decimal ad_fat_conversione_acq)
public function integer wf_materie_prime_terzista (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, string as_cod_terzista, string as_cod_deposito_terzista, decimal ad_quan_acquisto_um_mag, ref string as_errore)
public function integer wf_crea_mov_magazzino (long al_row, datastore fds_rag_acq, datetime fdt_data_registrazione, ref long al_anno_mov_mag, ref long al_num_mov_mag, ref string as_errore)
end prototypes

event ue_initial_size();this.x = w_cs_xx_mdi.x + 40
this.y = w_cs_xx_mdi.y + 40
this.width = w_cs_xx_mdi.width - 120
this.height = w_cs_xx_mdi.height - 450


end event

public function integer wf_carica_dett_fatture (ref string fs_messaggio);string  ls_cod_tipo_ord_acq, ls_flag_tipo_doc, ls_cod_fornitore[], &
		  ls_cod_tipo_analisi_ord, ls_cod_deposito, ls_cod_tipo_analisi_fat, &
		  ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, &
		  ls_cod_iva, ls_cod_prod_fornitore, ls_cod_valuta, &
		  ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
		  ls_flag_sola_iva, ls_flag_saldo, ls_cod_cliente[], ls_cod_prodotto_raggruppato,&
		  ls_referenza, ls_flag_evasione, ls_flag_agg_costo_ultimo, ls_flag_tipo_riga, ls_cod_prodotto_alt
		 
dec{6}  ld_cambio_acq
dec{5}  ld_fat_conversione_acq
dec{4}  ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_ordine, &
		  ld_sconto_pagamento, ld_quan_ordine, ld_prezzo_acquisto, &
		  ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
		  ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		  ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
		  ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
		  ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
		  ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
		  ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
		  ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
		  ld_val_sconto_pagamento, ld_val_evaso, ld_quan_ordinata_new, ld_residua, &
		  ld_quan_ordinata,ld_quan_ordinata_new_raggr, ld_quan_raggruppo, ld_quan_acquisto_alt

long    ll_anno_ordine, ll_num_ordine, ll_prog_riga_documento, ll_prog_riga_fat_acq ,&
		  ll_prog_riga_ordine_acq, ll_anno_commessa, ll_num_commessa, &
		  ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_i, ll_anno_reg_dest_stock, &
		  ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], &
		  ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_find

datetime	ldt_data_registrazione

datastore lds_rag_documenti
pointer lp_old_pointer
lp_old_pointer = SetPointer(HourGlass!)


lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_ord_acq"
lds_rag_documenti.settransobject(sqlca)

select cod_valuta,
		data_registrazione
 into  :ls_cod_valuta,
 		:ldt_data_registrazione
 from  tes_fat_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_reg_fat_acq and
		 num_registrazione = :il_num_reg_fat_acq;

// *** michela 18/03/04: tener conto se si vuole aggiornare l'ultimo costo del prodotto
// *** quindi leggo il flag e lo memorizzo nella det_ord_acq

for ll_i = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
	tab_1.det_2.dw_ext_carico_acq_det.accepttext()
	ll_anno_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "anno_registrazione")
	ll_num_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ordine_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "prog_riga_ord_acq")
	ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "quan_acquisto")
	ls_flag_agg_costo_ultimo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_agg_costo_ultimo")
	
	if isnull(ls_flag_agg_costo_ultimo) then ls_flag_agg_costo_ultimo = "N"
	
	if ls_flag_agg_costo_ultimo = "S" then
	
		update det_ord_acq
		set    flag_agg_costo_ultimo = :ls_flag_agg_costo_ultimo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ordine and
				 num_registrazione = :ll_num_ordine and
				 prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante Aggiornamento Flag Aggiorna Costo Ultimo."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if			 

	end if

	select tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_tipo_ord_acq,
			 tes_ord_acq.cod_deposito
	 into  :ls_cod_fornitore[1],
			 :ls_cod_tipo_ord_acq,
			 :ls_cod_deposito
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_acq.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	ls_flag_tipo_riga = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_tipo_riga")
	
	ls_cod_prodotto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "cod_prodotto_alt")
	if ls_cod_prodotto_alt="" then setnull(ls_cod_prodotto_alt)
	
	ld_quan_acquisto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_i, "quan_acquisto_alt")
	//------------------------------------------------------------------------------------------------------------

	lds_rag_documenti.insertrow(0)
	lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_ordine_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_fornitore", ls_cod_fornitore[1])
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_acq	)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_saldo", ls_flag_saldo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto", ld_quan_acquisto)
	
	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_tipo_riga", ls_flag_tipo_riga)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_prodotto_alt", ls_cod_prodotto_alt)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto_alt", ld_quan_acquisto_alt)
	//------------------------------------------------------------------------------------------------------------
	
next

for ll_i = 1 to lds_rag_documenti.rowcount()
	select max(prog_riga_fat_acq)
		into :ll_prog_riga_fat_acq
		from det_fat_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_reg_fat_acq and
				 num_registrazione = :il_num_reg_fat_acq;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Fatture Acquisto." + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	if isnull(ll_prog_riga_fat_acq) then ll_prog_riga_fat_acq = 0
	ll_prog_riga_fat_acq = ll_prog_riga_fat_acq  + 10
			
	ll_anno_ordine = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_ordine = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ll_prog_riga_ordine_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")

	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")
	ls_cod_tipo_ord_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	
	select	det_ord_acq.cod_tipo_det_acq,   
				det_ord_acq.cod_prodotto,   
				det_ord_acq.cod_misura,   
				det_ord_acq.des_prodotto,   
				det_ord_acq.quan_ordinata,
				det_ord_acq.prezzo_acquisto,   
				det_ord_acq.fat_conversione,   
				det_ord_acq.sconto_1,   
				det_ord_acq.sconto_2,  
				det_ord_acq.sconto_3,  
				det_ord_acq.cod_iva,   
				det_ord_acq.cod_prod_fornitore,
				det_ord_acq.quan_arrivata,
				det_ord_acq.val_riga,				
				det_ord_acq.nota_dettaglio,   
				det_ord_acq.anno_commessa,
				det_ord_acq.num_commessa,
				det_ord_acq.cod_centro_costo,
				det_ord_acq.sconto_4,   
				det_ord_acq.sconto_5,   
				det_ord_acq.sconto_6,   
				det_ord_acq.sconto_7,   
				det_ord_acq.sconto_8,   
				det_ord_acq.sconto_9,   
				det_ord_acq.sconto_10
	into		:ls_cod_tipo_det_acq, 
				:ls_cod_prodotto, 
				:ls_cod_misura, 
				:ls_des_prodotto, 
				:ld_quan_ordine,
				:ld_prezzo_acquisto, 
				:ld_fat_conversione_acq, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_sconto_3, 
				:ls_cod_iva, 
				:ls_cod_prod_fornitore,
				:ld_quan_arrivata,
				:ld_val_riga,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10
	from 		det_ord_acq  
	where 	det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_ord_acq.anno_registrazione = :ll_anno_ordine and  
				det_ord_acq.num_registrazione = :ll_num_ordine and
				det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq
	order by det_ord_acq.prog_riga_ordine_acq asc;

	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Ordini Acquisto.~n~r" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	//############################################################
	//rileggo il fattore conversione acquisti dalla datawindow, potrebbe essere cambiato
	ll_find = tab_1.det_2.dw_ext_carico_acq_det.find(	"anno_registrazione="+string(ll_anno_ordine)+" "+&
																	"and num_registrazione="+string(ll_num_ordine)+" "+&
																	"and prog_riga_ord_acq="+string(ll_prog_riga_ordine_acq)+"", &
																	1, tab_1.det_2.dw_ext_carico_acq_det.rowcount())
	if ll_find>0 then
		ld_fat_conversione_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "fat_conversione")
		
		//ricalcolo il prezzo unitario acquisto  (€/UMmag) = (€/UMacq) * (UMacq/UMmag)
		//ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto") * ld_fat_conversione_acq
		ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto_mag")
	end if
	//############################################################
	

	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_flag_tipo_det_acq = "M" then
		// fare update anag_prodotti
		select quan_ordinata
		into :ld_quan_ordinata
		from anag_prodotti
		where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		ld_quan_ordinata_new = ld_quan_ordinata
		
		ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
		
		ld_residua = (ld_quan_ordine - ld_quan_arrivata)
		if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
			ls_flag_saldo = "S"
			ld_quan_ordinata_new = ld_quan_ordinata - ld_residua
		else
			if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
				ld_quan_ordinata_new = ld_quan_ordinata - (ld_residua)
			else
				ld_quan_ordinata_new = ld_quan_ordinata - ld_quan_acquisto
			end if
		end if

		update anag_prodotti  
			set quan_ordinata = :ld_quan_ordinata_new
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode = -1 then
			fs_messaggio= "Si è verificato un errore in fase di aggiornamento magazzino."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			rollback;
			return -1
		end if	
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			select quan_ordinata
			into   :ld_quan_ordinata
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
					 
			ld_quan_ordinata_new_raggr = ld_quan_ordinata
			
			ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
			
			ld_residua = (ld_quan_ordine - ld_quan_arrivata)
			
			if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
				ls_flag_saldo = "S"
				ld_quan_ordinata_new_raggr = ld_quan_ordinata - f_converti_qta_raggruppo(ls_cod_prodotto, ld_residua, "M")
			else
				if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
					ld_quan_ordinata_new_raggr = ld_quan_ordinata - f_converti_qta_raggruppo(ls_cod_prodotto, ld_residua, "M")
				else
					ld_quan_ordinata_new_raggr = ld_quan_ordinata - f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_acquisto, "M")
				end if
			end if
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_raggruppato, ld_quan_ordinata_new_raggr, "M")

			update anag_prodotti  
			set 	 quan_ordinata = :ld_quan_raggruppo
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;

			if sqlca.sqlcode = -1 then
				fs_messaggio = "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				rollback;
				return -1
			end if
		end if
		
	end if
	
	update det_ord_acq
	set    quan_arrivata = quan_arrivata + :ld_quan_acquisto,
			 flag_saldo    = :ls_flag_saldo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_ordine and
			 num_registrazione = :ll_num_ordine and
			 prog_riga_ordine_acq = :ll_prog_riga_ordine_acq ;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento righe ordine"
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	// stefanop 16/07/2010: progetto 140 funzione 3: creo movimenti di magazzino
	if tab_1.det_2.rb_attiva_scarico.checked and  lds_rag_documenti.getitemstring(ll_i, "flag_tipo_riga") <> "3"  and&	
			 lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")<>"" and not isnull( lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")) then
		
		if wf_crea_mov_magazzino(ll_i, lds_rag_documenti, ldt_data_registrazione, ref ll_anno_reg_mov_mag, ref ll_num_reg_mov_mag, ref fs_messaggio) < 0 then
			
			rollback;
			return -1
		end if
	else
		setnull(ll_anno_reg_mov_mag)
		setnull(ll_num_reg_mov_mag)
	end if
	// ----

	insert into det_fat_acq
	   (cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_fat_acq,   
		cod_tipo_det_acq,   
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_fatturata,   
		prezzo_acquisto,   
		fat_conversione,   
		sconto_1,   
		sconto_2,   
		sconto_3,   
		cod_iva,   
		cod_deposito,   
		cod_ubicazione,   
		cod_lotto,   
		data_stock,   
		cod_tipo_movimento,   
		num_registrazione_mov_mag,   
		cod_conto,   
		nota_dettaglio,   
		anno_bolla_acq,   
		num_bolla_acq,   
		prog_riga_bolla_acq,   
		flag_accettazione,   
		anno_commessa,   
		num_commessa,   
		cod_centro_costo,   
		sconto_4,   
		sconto_5,   
		sconto_6,   
		sconto_7,   
		sconto_8,   
		sconto_9,   
		sconto_10,     
		prog_stock,   
		anno_reg_des_mov,   
		num_reg_des_mov,   
		anno_reg_ord_acq,   
		num_reg_ord_acq,   
		prog_riga_ordine_acq,   
		prog_bolla_acq,
		anno_reg_mov_mag_ret,
		num_reg_mov_mag_ret,
		anno_registrazione_mov_mag,
		anno_reg_mov_mag_terzista, // stefanop 22/07/2010
		num_reg_mov_mag_terzista)  
values (:s_cs_xx.cod_azienda,   
		:il_anno_reg_fat_acq,   
		:il_num_reg_fat_acq,   
		:ll_prog_riga_fat_acq,   
		:ls_cod_tipo_det_acq,   
		:ls_cod_misura,   
		:ls_cod_prodotto,   
		:ls_des_prodotto,   
		:ld_quan_acquisto,   
		:ld_prezzo_acquisto,   
		:ld_fat_conversione_acq,   
		:ld_sconto_1,   
		:ld_sconto_2,   
		:ld_sconto_3,   
		:ls_cod_iva,   
		:ls_cod_deposito,   
		null,   
		null,
		null,   
		:ls_cod_tipo_movimento,   
		null,   
		null,   
		null,   
		null,   
		null,   
		null,   
		'N',   
		null,   
		null,   
		:ls_cod_centro_costo,   
		:ld_sconto_4,   
		:ld_sconto_5,   
		:ld_sconto_6,   
		:ld_sconto_7,   
		:ld_sconto_8,   
		:ld_sconto_9,   
		:ld_sconto_10,   
		null,   
		:ll_anno_reg_dest_stock,
		:ll_num_reg_dest_stock,
		:ll_anno_ordine,
		:ll_num_ordine,
		:ll_prog_riga_ordine_acq,
		null,
		null,
		null,
		null,
		:ll_anno_reg_mov_mag, // stefanop 22/07/2010
		:ll_num_reg_mov_mag);

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Fatture Acquisto.~n~r" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	wf_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine)
next

commit;

destroy lds_rag_documenti
SetPointer(lp_old_pointer)
return 0
end function

public function integer wf_carica_dett_bolle (ref string fs_messaggio, integer fi_anno_bolla_acq, long fl_num_bolla_acq);string ls_cod_tipo_ord_acq, ls_flag_tipo_doc, ls_cod_fornitore[], &
		 ls_cod_tipo_analisi_ord, ls_cod_deposito, ls_lire, ls_cod_tipo_analisi_bol, &
		 ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, &
		 ls_cod_iva, ls_cod_prod_fornitore, ls_cod_valuta, &
		 ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
		 ls_flag_sola_iva, ls_flag_saldo, ls_cod_cliente[], ls_cod_prodotto_raggruppato, &
		 ls_referenza, ls_flag_evasione, ls_flag_agg_costo_ultimo, ls_flag_tipo_riga, ls_cod_prodotto_alt,&
		 ls_referenza_mov_grezzo, ls_cod_bol_acq
		 
dec{6} ld_cambio_acq
dec{5} ld_fat_conversione_acq, ld_fat_conversione
dec{4} ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_quan_ordine, ld_prezzo_acquisto, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
		 ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
		 ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
		 ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
		 ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
		 ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
		 ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
		 ld_val_sconto_pagamento, ld_val_evaso, ld_quan_ordinata, ld_quan_ordinata_new, &
		 ld_residua, ld_quan_ordinata_new_raggr,ld_quan_raggruppo, ld_quan_acquisto_alt

long 	 ll_anno_ordine, ll_num_ordine, ll_prog_riga_documento, ll_prog_riga_bol_acq ,&
	  	 ll_prog_riga_ordine_acq, ll_anno_commessa, ll_num_commessa, ll_prog_stock[], &
		 ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_i, ll_anno_reg_dest_stock, &
		 ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], &
		 ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_find
datetime ldt_data_bolla, ldt_data_registrazione

datastore lds_rag_documenti
pointer lp_old_pointer
lp_old_pointer = SetPointer(HourGlass!)


lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_ord_acq"
lds_rag_documenti.settransobject(sqlca)

select cod_deposito,   
		 cod_valuta,
		 data_registrazione
 into  :ls_cod_deposito,   
		 :ls_cod_valuta,
		 :ldt_data_registrazione
 from  tes_bol_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_bolla_acq = :ii_anno_bolla_acq and
		 num_bolla_acq = :il_num_bolla_acq;

//recupero i dati che potrebbero servire scrivere nei mov dei grezzi
select 	data_bolla,
			num_bolla_fornitore
into 		:ldt_data_bolla,
			:ls_cod_bol_acq
from tes_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
				anno_bolla_acq = :fi_anno_bolla_acq and
				num_bolla_acq = :fl_num_bolla_acq;
// ----------------------------

for ll_i = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
	tab_1.det_2.dw_ext_carico_acq_det.accepttext()
	ll_anno_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "anno_registrazione")
	ll_num_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ordine_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "prog_riga_ord_acq")
	ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "quan_acquisto")
	ld_fat_conversione = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "fat_conversione")
	ls_flag_agg_costo_ultimo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_agg_costo_ultimo")
	
	if isnull(ls_flag_agg_costo_ultimo) then ls_flag_agg_costo_ultimo = "N"
	
	if ls_flag_agg_costo_ultimo = "S" then
	
		update det_ord_acq
		set    flag_agg_costo_ultimo = :ls_flag_agg_costo_ultimo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ordine and
				 num_registrazione = :ll_num_ordine and
				 prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante Aggiornamento Flag Aggiorna Costo Ultimo."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if			 

	end if	
	
	ld_quan_acquisto = ld_quan_acquisto / ld_fat_conversione
	ld_quan_acquisto = round(ld_quan_acquisto, 4)

	select tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_tipo_ord_acq
	 into  :ls_cod_fornitore[1],
			 :ls_cod_tipo_ord_acq
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_acq.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	//salvo la ragione sociale del cod_fornitore
	ls_referenza_mov_grezzo = f_des_tabella("anag_fornitori",  "cod_fornitore='"+ls_cod_fornitore[1]+"'", "rag_soc_1")
	
	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	ls_flag_tipo_riga = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_tipo_riga")
	
	ls_cod_prodotto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "cod_prodotto_alt")
	if ls_cod_prodotto_alt="" then setnull(ls_cod_prodotto_alt)
	
	ld_quan_acquisto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_i, "quan_acquisto_alt")
	//------------------------------------------------------------------------------------------------------------

	lds_rag_documenti.insertrow(0)
	lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_ordine_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_fornitore", ls_cod_fornitore[1])
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_acq	)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_saldo", ls_flag_saldo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto", ld_quan_acquisto)
	
	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_tipo_riga", ls_flag_tipo_riga)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_prodotto_alt", ls_cod_prodotto_alt)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto_alt", ld_quan_acquisto_alt)
	//------------------------------------------------------------------------------------------------------------
	
next

for ll_i = 1 to lds_rag_documenti.rowcount()
	select max(prog_riga_bolla_acq)
		into :ll_prog_riga_bol_acq
		from det_bol_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_bolla_acq = :fi_anno_bolla_acq and
				num_bolla_acq = :fl_num_bolla_acq;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Bolle Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	if isnull(ll_prog_riga_bol_acq) then ll_prog_riga_bol_acq = 0
	ll_prog_riga_bol_acq = ll_prog_riga_bol_acq  + 10
			
	ll_anno_ordine = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_ordine = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ll_prog_riga_ordine_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")

	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")
	ls_cod_tipo_ord_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	
	select	det_ord_acq.cod_tipo_det_acq,   
				det_ord_acq.cod_prodotto,   
				det_ord_acq.cod_misura,   
				det_ord_acq.des_prodotto,   
				det_ord_acq.quan_ordinata,
				det_ord_acq.prezzo_acquisto,   
				det_ord_acq.fat_conversione,   
				det_ord_acq.sconto_1,   
				det_ord_acq.sconto_2,  
				det_ord_acq.sconto_3,  
				det_ord_acq.cod_iva,   
				det_ord_acq.cod_prod_fornitore,
				det_ord_acq.quan_arrivata,
				det_ord_acq.val_riga,				
				det_ord_acq.nota_dettaglio,   
				det_ord_acq.anno_commessa,
				det_ord_acq.num_commessa,
				det_ord_acq.cod_centro_costo,
				det_ord_acq.sconto_4,   
				det_ord_acq.sconto_5,   
				det_ord_acq.sconto_6,   
				det_ord_acq.sconto_7,   
				det_ord_acq.sconto_8,   
				det_ord_acq.sconto_9,   
				det_ord_acq.sconto_10
	into		:ls_cod_tipo_det_acq, 
				:ls_cod_prodotto, 
				:ls_cod_misura, 
				:ls_des_prodotto, 
				:ld_quan_ordine,
				:ld_prezzo_acquisto, 
				:ld_fat_conversione_acq, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_sconto_3, 
				:ls_cod_iva, 
				:ls_cod_prod_fornitore,
				:ld_quan_arrivata,
				:ld_val_riga,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10
	from 		det_ord_acq  
	where 	det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_ord_acq.anno_registrazione = :ll_anno_ordine and  
				det_ord_acq.num_registrazione = :ll_num_ordine and
				det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq
	order by det_ord_acq.prog_riga_ordine_acq asc;

	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Ordini Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	

	
	//############################################################
	//rileggo il fattore conversione acquisti dalla datawindow, potrebbe essere cambiato
	ll_find = tab_1.det_2.dw_ext_carico_acq_det.find(	"anno_registrazione="+string(ll_anno_ordine)+" "+&
																	"and num_registrazione="+string(ll_num_ordine)+" "+&
																	"and prog_riga_ord_acq="+string(ll_prog_riga_ordine_acq)+"", &
																	1, tab_1.det_2.dw_ext_carico_acq_det.rowcount())
	if ll_find>0 then
		ld_fat_conversione_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "fat_conversione")
		
		//ricalcolo il prezzo unitario acquisto  (€/UMmag) = (€/UMacq) * (UMacq/UMmag)
		//ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto") * ld_fat_conversione_acq
		ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto_mag")
	end if
	//############################################################
	
	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_flag_tipo_det_acq = "M" then
		// fare update anag_prodotti
		select quan_ordinata
		into   :ld_quan_ordinata
		from   anag_prodotti
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
				 
		ld_quan_ordinata_new = ld_quan_ordinata
		
		ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
		
		ld_residua = (ld_quan_ordine - ld_quan_arrivata)
		if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
			ls_flag_saldo = "S"
			ld_quan_ordinata_new = ld_quan_ordinata - ld_residua
		else
			if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
				ld_quan_ordinata_new = ld_quan_ordinata - (ld_residua)
			else
				ld_quan_ordinata_new = ld_quan_ordinata - ld_quan_acquisto
			end if
		end if

		update anag_prodotti  
			set quan_ordinata = :ld_quan_ordinata_new
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode = -1 then
			fs_messaggio= "Si è verificato un errore in fase di aggiornamento magazzino."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			rollback;
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato (faccio la stessa cosa che ho fatto per il prodotto principale)
		
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
				
			select quan_ordinata
			into   :ld_quan_ordinata
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_raggruppato;
					 
			ld_quan_ordinata_new_raggr = ld_quan_ordinata
			
			ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
			
			ld_residua = (ld_quan_ordine - ld_quan_arrivata)
			if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
				ls_flag_saldo = "S"
				ld_quan_ordinata_new_raggr = ld_quan_ordinata - ld_residua
			else
				if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
					ld_quan_ordinata_new_raggr = ld_quan_ordinata - (ld_residua)
				else
					ld_quan_ordinata_new_raggr = ld_quan_ordinata - ld_quan_acquisto
				end if
			end if
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_raggruppato, ld_quan_ordinata_new_raggr, "M")
			
			update anag_prodotti  
				set quan_ordinata = :ld_quan_raggruppo
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;

			if sqlca.sqlcode = -1 then
				fs_messaggio= "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				rollback;
				return -1
			end if
		end if
		
	end if	
	
	update det_ord_acq
	set    quan_arrivata = quan_arrivata + :ld_quan_acquisto,
			 flag_saldo    = :ls_flag_saldo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_ordine and
			 num_registrazione = :ll_num_ordine and
			 prog_riga_ordine_acq = :ll_prog_riga_ordine_acq ;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento righe ordine"
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	// stefanop 16/07/2010: progetto 140 funzione 3: creo movimenti di magazzino
	if tab_1.det_2.rb_attiva_scarico.checked and  lds_rag_documenti.getitemstring(ll_i, "flag_tipo_riga") <> "3"  and&	
			 lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")<>"" and not isnull( lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")) then
		
		if wf_crea_mov_magazzino(ll_i, lds_rag_documenti, ldt_data_registrazione, ref ll_anno_reg_mov_mag, ref ll_num_reg_mov_mag, ref fs_messaggio) < 0 then
			
			rollback;
			return -1
		end if
		
		wf_aggiorna_mov_grezzo_referenza(ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ldt_data_bolla, ls_cod_bol_acq, ls_referenza_mov_grezzo)
													
	else
		setnull(ll_anno_reg_mov_mag)
		setnull(ll_num_reg_mov_mag)
	end if
	// ----

	insert into det_bol_acq
					(cod_azienda,   
					 anno_bolla_acq,   
					 num_bolla_acq,   
					 prog_riga_bolla_acq,   
					 cod_tipo_det_acq,   
					 cod_misura,   
					 cod_prodotto,   
					 des_prodotto,   
					 quan_arrivata,   
					 prezzo_acquisto,   
					 fat_conversione,   
					 sconto_1,   
					 sconto_2,   
					 sconto_3,   
					 cod_iva,   
					 quan_fatturata,
					 val_riga,				 
					 cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 data_stock,
					 cod_tipo_movimento,
					 num_registrazione_mov_mag,
					 flag_saldo,
					 flag_accettazione,
					 flag_blocco,
					 nota_dettaglio,   
					 anno_registrazione,   
					 num_registrazione,   
					 prog_riga_ordine_acq,
					 anno_commessa,
					 num_commessa,
					 cod_centro_costo,
					 sconto_4,   
					 sconto_5,   
					 sconto_6,   
					 sconto_7,   
					 sconto_8,   
					 sconto_9,   
					 sconto_10,
					 anno_registrazione_mov_mag,
					 anno_reg_bol_ven,
					 num_reg_bol_ven,
					 prog_riga_bol_ven,
					 prog_stock,
					 anno_reg_des_mov,
					 num_reg_des_mov,
					 anno_reg_mov_mag_terzista, // stefanop 22/07/2010
					 num_reg_mov_mag_terzista)
  values			(:s_cs_xx.cod_azienda,   
					 :fi_anno_bolla_acq,   
					 :fl_num_bolla_acq,     
					 :ll_prog_riga_bol_acq,   
					 :ls_cod_tipo_det_acq,   
					 :ls_cod_misura,   
					 :ls_cod_prodotto,   
					 :ls_des_prodotto,   
					 :ld_quan_acquisto,   
					 :ld_prezzo_acquisto,   
					 :ld_fat_conversione_acq,   
					 :ld_sconto_1,   
					 :ld_sconto_2,  
					 :ld_sconto_3,  
					 :ls_cod_iva,   
					 null,
					 :ld_val_riga_sconto_10,
					 :ls_cod_deposito,
					 null,
					 null,
					 null,
					 :ls_cod_tipo_movimento,
					 null,
					 :ls_flag_saldo,
					 'N',
					 'N',
					 :ls_nota_dettaglio,   
					 :ll_anno_ordine,   
					 :ll_num_ordine,   
					 :ll_prog_riga_ordine_acq,
					 :ll_anno_commessa,   
					 :ll_num_commessa,   
					 :ls_cod_centro_costo,   
					 :ld_sconto_4,   
					 :ld_sconto_5,   
					 :ld_sconto_6,   
					 :ld_sconto_7,   
					 :ld_sconto_8,   
					 :ld_sconto_9,   
					 :ld_sconto_10,
					 null,
					 null,
					 null,
					 null,
					 :ll_prog_stock[1],
					 :ll_anno_reg_des_mov,
					 :ll_num_reg_des_mov,
					 :ll_anno_reg_mov_mag, // stefanop 22/07/2010
					 :ll_num_reg_mov_mag);

	if sqlca.sqlcode <> 0 then
//		messagebox(string(fi_anno_bolla_acq),fl_num_bolla_acq)
//		messagebox("Apice", ll_prog_riga_bol_acq)
//		messagebox("prova", string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
		fs_messaggio = "Errore durante la scrittura Dettagli Bolle Acquisto." + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	wf_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine)
next
commit;

destroy lds_rag_documenti
SetPointer(lp_old_pointer)

return 0
end function

public function integer wf_genera_bolle_acq (ref string fs_messaggio);string 	ls_cod_tipo_rag, ls_cod_tipo_ord_acq, ls_flag_riep_bol, ls_cod_tipo_bol_acq, &
			ls_flag_tipo_doc, ls_flag_destinazione, ls_flag_num_doc, ls_flag_imballo, &
			ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, ls_flag_resa, &
			ls_flag_data_consegna, ls_cod_valuta, ls_cod_pagamento, &
			ls_cod_banca, ls_cod_banca_clien_for, ls_cod_des_fornitore, &
			ls_num_ord_fornitore, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
			ls_cod_porto, ls_cod_resa, ls_sort, ls_key_sort, ls_key_sort_old, ls_cod_tipo_bol_ven, &
			ls_cod_operatore, ls_cod_fornitore[], ls_rag_soc_1, ls_rag_soc_2, ls_cod_tipo_analisi_ord, &
			ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_deposito[], &
			ls_cod_tipo_listino_prodotto, ls_nota_testata, ls_nota_piede, ls_lire, &
			ls_cod_tipo_analisi_bol, ls_cod_bol_acq, ls_cod_fil_fornitore, ls_cod_tipo_det_acq, &
			ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_cod_prod_fornitore, &
			ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
			ls_flag_sola_iva, ls_flag_saldo, ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], &
			ls_referenza, ls_flag_evasione, ls_cod_iva_det_acq, ls_flag_conferma, ls_flag_acc_materiali, &
			ls_flag_doc_suc_det, ls_flag_agg_costo_ultimo,ls_cod_prodotto_raggruppato, ls_referenza_mov_grezzo,&
			ls_flag_tipo_riga, ls_cod_prodotto_alt, ls_cod_natura_intra
		 
dec{6} 	ld_cambio_acq
dec{5}	ld_fat_conversione_acq, ld_fat_conversione
dec{4}	ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_ordine, &
			ld_sconto_pagamento, ld_quan_ordine, ld_prezzo_acquisto, &
			ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
			ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
			ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
			ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
			ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
			ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
			ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
			ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
			ld_val_sconto_pagamento, ld_val_evaso, ld_quan_ordinata, ld_quan_ordinata_new, &
		 	ld_residua,ld_quan_ordinata_new_raggr, ld_quan_raggruppo, ld_quan_acquisto_alt
		 
long 		ll_anno_ordine, ll_num_ordine, ll_prog_riga_documento, ll_progressivo, ll_prog_riga_bol_acq ,&
	  		ll_prog_riga_ordine_acq, ll_anno_commessa, ll_num_commessa, ll_prog_stock[], &
	  		ll_anno_reg_dest_stock, ll_i, ll_num_acc_materiali, &
	  		ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], &
	  		ll_anno_esercizio,ll_righe_non_saldate, ll_num_righe, ll_anno_bol_ven, ll_num_bol_ven, ll_riga_bol_ven, &
			ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_num_documento, ll_find
			  
datetime ldt_data_consegna, ldt_data_ordine, ldt_data_ord_fornitore, ldt_data_registrazione, &
			ldt_data_bolla, ldt_data_consegna_ordine, ldt_data_stock[]
			

datastore lds_rag_documenti, lds_riga_ord_acq
pointer lp_old_pointer
lp_old_pointer = SetPointer(HourGlass!)


lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_ord_acq"
lds_rag_documenti.settransobject(sqlca)
ls_cod_tipo_bol_acq = s_cs_xx.parametri.parametro_s_1
ls_cod_bol_acq = s_cs_xx.parametri.parametro_s_2
ls_cod_deposito[1] = s_cs_xx.parametri.parametro_s_3

//se il codice ubicazione è stringa vuota forza a NULL
if trim(s_cs_xx.parametri.parametro_s_4)="" then setnull(s_cs_xx.parametri.parametro_s_4)
ls_cod_ubicazione[1] = s_cs_xx.parametri.parametro_s_4

//se il codice lotto è stringa vuota forza a NULL
if trim(s_cs_xx.parametri.parametro_s_5)="" then setnull(s_cs_xx.parametri.parametro_s_5)
ls_cod_lotto[1] = s_cs_xx.parametri.parametro_s_5

ldt_data_bolla = s_cs_xx.parametri.parametro_data_1
ls_cod_tipo_rag = s_cs_xx.parametri.parametro_s_7
ls_flag_conferma = s_cs_xx.parametri.parametro_s_8
ls_flag_acc_materiali = s_cs_xx.parametri.parametro_s_9
ll_anno_esercizio = f_anno_esercizio()

ll_anno_bol_ven = s_cs_xx.parametri.parametro_d_1
ll_num_bol_ven = s_cs_xx.parametri.parametro_d_2
ll_riga_bol_ven = s_cs_xx.parametri.parametro_d_3

try
	ldt_data_registrazione = s_cs_xx.parametri.parametro_data_2
catch (runtimeerror err)
	setnull(ldt_data_registrazione)
end try

if isnull(ldt_data_registrazione) or year(date(ldt_data_registrazione))<=1950 then
	//se per disgrazia e sfiga la data registrazione arriva vuota metti data sistema
	ldt_data_registrazione = datetime(today(), 00:00:00)
else
	ldt_data_registrazione = datetime(date(ldt_data_registrazione), 00:00:00)
end if

for ll_i = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
	tab_1.det_2.dw_ext_carico_acq_det.accepttext()
	ll_anno_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "anno_registrazione")
	ll_num_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ordine_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "prog_riga_ord_acq")
	ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "quan_acquisto")
	ls_flag_agg_costo_ultimo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_agg_costo_ultimo")
	
	if isnull(ls_flag_agg_costo_ultimo) then ls_flag_agg_costo_ultimo = "N"
	
	if ls_flag_agg_costo_ultimo = "S" then
	
		update det_ord_acq
		set    flag_agg_costo_ultimo = :ls_flag_agg_costo_ultimo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ordine and
				 num_registrazione = :ll_num_ordine and
				 prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante Aggiornamento Flag Aggiorna Costo Ultimo."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if			 

	end if	
	
	
	ld_fat_conversione = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "fat_conversione")
	ld_quan_acquisto = ld_quan_acquisto / ld_fat_conversione
	ld_quan_acquisto = round(ld_quan_acquisto, 4)
	
	if not isnull(ls_cod_tipo_rag) and trim(ls_cod_tipo_rag) <> "" then
		select tab_tipi_rag.flag_tipo_doc,   
				 tab_tipi_rag.flag_destinazione,   
				 tab_tipi_rag.flag_num_doc,   
				 tab_tipi_rag.flag_imballo,   
				 tab_tipi_rag.flag_vettore,   
				 tab_tipi_rag.flag_inoltro,   
				 tab_tipi_rag.flag_mezzo,   
				 tab_tipi_rag.flag_porto,   
				 tab_tipi_rag.flag_resa,   
				 tab_tipi_rag.flag_data_consegna  
		into   :ls_flag_tipo_doc,   
				 :ls_flag_destinazione,   
				 :ls_flag_num_doc,   
				 :ls_flag_imballo,   
				 :ls_flag_vettore,   
				 :ls_flag_inoltro,   
				 :ls_flag_mezzo,   
				 :ls_flag_porto,   
				 :ls_flag_resa,   
				 :ls_flag_data_consegna  
		from   tab_tipi_rag  
		where  tab_tipi_rag.cod_azienda = :s_cs_xx.cod_azienda and
				 tab_tipi_rag.cod_tipo_rag = :ls_cod_tipo_rag;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la lettura Tabella Tipi Ragruppamenti.~n~r" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	else
		ls_flag_tipo_doc = 'N'
		ls_flag_destinazione = 'N'
		ls_flag_num_doc = 'N'
		ls_flag_imballo = 'N'
		ls_flag_vettore = 'N'
		ls_flag_inoltro = 'N'
		ls_flag_mezzo = 'N'
		ls_flag_porto = 'N'
		ls_flag_resa = 'N'
		ls_flag_data_consegna = 'N'
	end if
	
	select tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_valuta,   
			 tes_ord_acq.cod_pagamento,   
			 tes_ord_acq.cod_banca,   
			 tes_ord_acq.cod_banca_clien_for,
			 tes_ord_acq.cod_tipo_ord_acq,
			 tes_ord_acq.cod_des_fornitore,   
			 tes_ord_acq.num_ord_fornitore,   
			 tes_ord_acq.cod_imballo,   
			 tes_ord_acq.cod_vettore,   
			 tes_ord_acq.cod_inoltro,   
			 tes_ord_acq.cod_mezzo,
			 tes_ord_acq.cod_porto,   
			 tes_ord_acq.cod_resa
	 into  :ls_cod_fornitore[1],
			 :ls_cod_valuta,   
			 :ls_cod_pagamento,   
			 :ls_cod_banca,   
			 :ls_cod_banca_clien_for,
			 :ls_cod_tipo_ord_acq,
			 :ls_cod_des_fornitore,   
			 :ls_num_ord_fornitore,   
			 :ls_cod_imballo,   
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_cod_porto,
			 :ls_cod_resa
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_acq.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Acquisto.~n~r" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	//salvo la ragione sociale del cod_fornitore
	ls_referenza_mov_grezzo = f_des_tabella("anag_fornitori",  "cod_fornitore='"+ls_cod_fornitore[1]+"'", "rag_soc_1")
	
	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	ls_flag_tipo_riga = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_tipo_riga")
	
	ls_cod_prodotto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "cod_prodotto_alt")
	if ls_cod_prodotto_alt="" then setnull(ls_cod_prodotto_alt)
	
	ld_quan_acquisto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_i, "quan_acquisto_alt")
	//------------------------------------------------------------------------------------------------------------
	
	lds_rag_documenti.insertrow(0)
	lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_ordine_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_fornitore", ls_cod_fornitore[1])
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_acq	)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_saldo", ls_flag_saldo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto", ld_quan_acquisto)

	if ls_flag_data_consegna = "S" then
		select   det_ord_acq.data_consegna
		into		:ldt_data_consegna
		from     det_ord_acq  
		where    det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
					det_ord_acq.anno_registrazione = :ll_anno_ordine and
					det_ord_acq.num_registrazione = :ll_num_ordine and
					det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "data_consegna", ldt_data_consegna)
	end if
	
	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_tipo_riga", ls_flag_tipo_riga)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_prodotto_alt", ls_cod_prodotto_alt)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto_alt", ld_quan_acquisto_alt)
	//------------------------------------------------------------------------------------------------------------
	
next

ls_sort = "cod_fornitore A, cod_valuta A, cod_pagamento A, cod_banca A, cod_banca_clien_for A"

if ls_flag_tipo_doc = "S" then
	ls_sort = ls_sort + ", cod_tipo_documento A"
end if

if ls_flag_destinazione = "S" then
	ls_sort = ls_sort + ", cod_destinazione A"
end if

if ls_flag_num_doc = "S" then
	ls_sort = ls_sort + ", cod_doc_esterno A"
end if

if ls_flag_imballo = "S" then
	ls_sort = ls_sort + ", cod_imballo A"
end if

if ls_flag_vettore = "S" then
	ls_sort = ls_sort + ", cod_vettore A"
end if

if ls_flag_inoltro = "S" then
	ls_sort = ls_sort + ", cod_inoltro A"
end if

if ls_flag_mezzo = "S" then
	ls_sort = ls_sort + ", cod_mezzo A"
end if

if ls_flag_porto = "S" then
	ls_sort = ls_sort + ", cod_porto A"
end if

if ls_flag_resa = "S" then
	ls_sort = ls_sort + ", cod_resa A"
end if

if ls_flag_data_consegna = "S" then
	ls_sort = ls_sort + ", data_consegna A"
end if

ls_sort = ls_sort + ", anno_documento A, num_documento A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""

for ll_i = 1 to lds_rag_documenti.rowcount()
	ll_anno_ordine = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_ordine = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ll_prog_riga_ordine_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")

	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_tipo_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_destinazione = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_num_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_imballo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_imballo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_imballo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_vettore = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_vettore")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_vettore")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_inoltro = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_mezzo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_porto = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_porto")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_porto")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_resa = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_resa")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_resa")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_data_consegna = "S" and &
		not isnull(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")) then
		ls_key_sort = ls_key_sort + string(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna"))
	else
		ls_key_sort = ls_key_sort + "        "
	end if

	ldt_data_consegna = lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")
	ls_cod_tipo_ord_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tab_tipi_ord_acq.cod_tipo_analisi
	into   :ls_cod_tipo_analisi_ord
	from   tab_tipi_ord_acq  
	where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura della tabella Tipi Ordini Acquisto.~n~r" + sqlca.sqlerrtext
		return -1
	end if
	
	select tes_ord_acq.cod_operatore,   
			 tes_ord_acq.data_registrazione,   
			 tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_des_fornitore,   
			 tes_ord_acq.cod_fil_fornitore,   			 
			 tes_ord_acq.rag_soc_1,   
			 tes_ord_acq.rag_soc_2,   
			 tes_ord_acq.indirizzo,   
			 tes_ord_acq.localita,   
			 tes_ord_acq.frazione,   
			 tes_ord_acq.cap,   
			 tes_ord_acq.provincia,   
			 tes_ord_acq.cod_valuta,   
			 tes_ord_acq.cambio_acq,   
			 tes_ord_acq.cod_tipo_listino_prodotto,   
			 tes_ord_acq.cod_pagamento,   
			 tes_ord_acq.sconto,   
			 tes_ord_acq.cod_banca,   
			 tes_ord_acq.num_ord_fornitore,   
			 tes_ord_acq.data_ord_fornitore,   
			 tes_ord_acq.cod_imballo,   
			 tes_ord_acq.cod_vettore,   
			 tes_ord_acq.cod_inoltro,   
			 tes_ord_acq.cod_mezzo,
			 tes_ord_acq.cod_porto,   
			 tes_ord_acq.cod_resa,   
			 tes_ord_acq.nota_testata,   
			 tes_ord_acq.nota_piede,   
			 tes_ord_acq.tot_val_evaso,
			 tes_ord_acq.tot_val_ordine,
			 tes_ord_acq.cod_banca_clien_for,
			 tes_ord_acq.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_fornitore[1],   
			 :ls_cod_des_fornitore,   
			 :ls_cod_fil_fornitore,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_valuta,   
			 :ld_cambio_acq,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_banca,   
			 :ls_num_ord_fornitore,   
			 :ldt_data_ord_fornitore,   
			 :ls_cod_imballo,   
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_natura_intra
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_acq.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Vendita.~n~r" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	if ls_cod_mezzo="" then setnull(ls_cod_mezzo)
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)
	if ls_cod_porto="" then setnull(ls_cod_porto)

	select tab_tipi_bol_acq.cod_tipo_analisi
	into	 :ls_cod_tipo_analisi_bol
	from   tab_tipi_bol_acq  
	where  tab_tipi_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_acq.cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Bolle.~n~r" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_key_sort <> ls_key_sort_old then
		
		if ls_key_sort_old <> "" and ls_flag_conferma = "S" then
			if f_conferma_bol_acq( ll_anno_esercizio, il_num_bolla_acq, &
											ref fs_messaggio, ls_flag_acc_materiali ) = -1 then
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if
		end if

		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
		
		// -------------------------------- gestione dell'incremento del numero ordine ---------------------------
		// se is_cod_fornitore <> null allora arrivo da testata bolla e mi porto dentro le righe ordine
		if isnull(is_cod_fornitore) then
			ii_anno_bolla_acq = f_anno_esercizio()
		
			il_num_bolla_acq = 0
			select max(num_bolla_acq) + 1
			  into :il_num_bolla_acq
			  from tes_bol_acq
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_bolla_acq = :ii_anno_bolla_acq;			
			if isnull(il_num_bolla_acq)  or il_num_bolla_acq = 0 then il_num_bolla_acq = 1
		end if
		
		//----------------------------------------------------------------------------------
		
		insert into tes_bol_acq	
						(cod_azienda,
						anno_bolla_acq,   
						num_bolla_acq,   
						cod_fornitore,   
						cod_cliente,   
						cod_tipo_bol_acq,   
						data_registrazione,   
						cod_operatore,   
						cod_fil_fornitore,   
						cod_deposito,   
						cod_ubicazione,   
						cod_valuta,   
						cambio_acq,   
						cod_tipo_listino_prodotto,   
						cod_pagamento,   
						cod_banca_clien_for,   
						totale_val_bolla,   
						flag_movimenti,   
						flag_riorganizzabile,   
						cod_banca,   
						data_bolla,   
						sconto,   
						flag_blocco,
						flag_gen_fat,
						num_bolla_fornitore,
						cod_mezzo,
						cod_natura_intra,
						cod_porto)
		   values	(:s_cs_xx.cod_azienda,   
						:ll_anno_esercizio,   
						:il_num_bolla_acq,   
						:ls_cod_fornitore[1],   
						null,   
						:ls_cod_tipo_bol_acq,
						:ldt_data_registrazione,   
						:ls_cod_operatore,   
						:ls_cod_fil_fornitore,   
						:ls_cod_deposito[1],   
						:ls_cod_ubicazione[1],   
						:ls_cod_valuta,   
						:ld_cambio_acq,   
						:ls_cod_tipo_listino_prodotto,   
						:ls_cod_pagamento,   
						:ls_cod_banca_clien_for,   
						null,   
						'N',
						'N',   
						:ls_cod_banca,   
						:ldt_data_bolla,   
						:ld_sconto_testata,   
						'N',
						'N',
						:ls_cod_bol_acq,
						:ls_cod_mezzo,
						:ls_cod_natura_intra,
						:ls_cod_porto);
						 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Testata Bolle Acquisto.~n~r" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if

		ll_prog_riga_bol_acq = 0
		fs_messaggio = fs_messaggio + " Bolla Generata: " + string(ll_anno_esercizio) + "/" + string(il_num_bolla_acq)
	
	end if

	ll_prog_riga_bol_acq = ll_prog_riga_bol_acq + 10
	
	lds_riga_ord_acq = create datastore
	lds_riga_ord_acq.dataobject = "d_ds_riga_ord_acq"
	lds_riga_ord_acq.settransobject(sqlca)
	ll_num_righe = lds_riga_ord_acq.retrieve(s_cs_xx.cod_azienda, ll_anno_ordine,ll_num_ordine,ll_prog_riga_ordine_acq)
	if ll_num_righe < 1 then
		fs_messaggio =  "Errore durante la lettura riga Ordine Acquisto.~n~r" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
		
	ls_cod_tipo_det_acq = lds_riga_ord_acq.getitemstring(1,"cod_tipo_det_acq")
	ls_cod_prodotto = lds_riga_ord_acq.getitemstring(1,"cod_prodotto") 
	ls_cod_misura = lds_riga_ord_acq.getitemstring(1,"cod_misura") 
	ls_des_prodotto = lds_riga_ord_acq.getitemstring(1,"des_prodotto")  
	ld_quan_ordine = lds_riga_ord_acq.getitemnumber(1,"quan_ordinata") 
	ld_prezzo_acquisto = lds_riga_ord_acq.getitemnumber(1,"prezzo_acquisto") 
	ld_fat_conversione_acq = lds_riga_ord_acq.getitemnumber(1,"fat_conversione") 
	ld_sconto_1 = lds_riga_ord_acq.getitemnumber(1,"sconto_1") 
	ld_sconto_2 = lds_riga_ord_acq.getitemnumber(1,"sconto_2") 
	ld_sconto_3 = lds_riga_ord_acq.getitemnumber(1,"sconto_3") 
	ls_cod_iva = lds_riga_ord_acq.getitemstring(1,"cod_iva")  
	ldt_data_consegna_ordine = lds_riga_ord_acq.getitemdatetime(1,"data_consegna")  
	ls_cod_prod_fornitore = lds_riga_ord_acq.getitemstring(1,"cod_prod_fornitore")  
	ld_quan_arrivata = lds_riga_ord_acq.getitemnumber(1,"quan_arrivata")  
	ld_val_riga = lds_riga_ord_acq.getitemnumber(1,"val_riga")  
	ls_nota_dettaglio = lds_riga_ord_acq.getitemstring(1,"nota_dettaglio")   
	ll_anno_commessa = lds_riga_ord_acq.getitemnumber(1,"anno_commessa")  
	ll_num_commessa = lds_riga_ord_acq.getitemnumber(1,"num_commessa")  
	ls_cod_centro_costo = lds_riga_ord_acq.getitemstring(1,"cod_centro_costo")   
	ld_sconto_4 = lds_riga_ord_acq.getitemnumber(1,"sconto_4")  
	ld_sconto_5 = lds_riga_ord_acq.getitemnumber(1,"sconto_5")  
	ld_sconto_6 = lds_riga_ord_acq.getitemnumber(1,"sconto_6")  
	ld_sconto_7 = lds_riga_ord_acq.getitemnumber(1,"sconto_7")  
	ld_sconto_8 = lds_riga_ord_acq.getitemnumber(1,"sconto_8")  
	ld_sconto_9 = lds_riga_ord_acq.getitemnumber(1,"sconto_9")  
	ld_sconto_10 = lds_riga_ord_acq.getitemnumber(1,"sconto_10") 
	ls_flag_doc_suc_det = lds_riga_ord_acq.getitemstring(1,"flag_doc_suc_det")   
	
	destroy lds_riga_ord_acq
	
	//############################################################
	//rileggo il fattore conversione acquisti dalla datawindow, potrebbe essere cambiato
	ll_find = tab_1.det_2.dw_ext_carico_acq_det.find(	"anno_registrazione="+string(ll_anno_ordine)+" "+&
																	"and num_registrazione="+string(ll_num_ordine)+" "+&
																	"and prog_riga_ord_acq="+string(ll_prog_riga_ordine_acq)+"", &
																	1, tab_1.det_2.dw_ext_carico_acq_det.rowcount())
	if ll_find>0 then
		ld_fat_conversione_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "fat_conversione")
		
		//ricalcolo il prezzo unitario acquisto  (€/UMmag) = (€/UMacq) * (UMacq/UMmag)
		//ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto") * ld_fat_conversione_acq
		ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto_mag")
	end if
	//############################################################

	if (ls_flag_data_consegna = "S" and (ldt_data_consegna = ldt_data_consegna_ordine) or &
		 isnull(ldt_data_consegna) and isnull(ldt_data_consegna_ordine)) or &
	 	 ls_flag_data_consegna = "N" then
		select tab_tipi_det_acq.flag_tipo_det_acq,
				 tab_tipi_det_acq.cod_tipo_movimento,
				 tab_tipi_det_acq.flag_sola_iva,
				 tab_tipi_det_acq.cod_iva
		into   :ls_flag_tipo_det_acq,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva,
				 :ls_cod_iva_det_acq
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli.~n~r" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		if isnull(ls_cod_iva) or ls_cod_iva = "" then ls_cod_iva = ls_cod_iva_det_acq
		
		if ls_flag_tipo_det_acq = "M" then
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli.~n~r" + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if

			setnull(ldt_data_stock[1])
			setnull(ll_prog_stock[1])
			setnull(ls_cod_cliente[1])
			setnull(ls_cod_fornitore[1])
			
			setnull(ls_referenza)
			
			if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
													ls_cod_prodotto, &
													ls_cod_deposito[], &
													ls_cod_ubicazione[], &
													ls_cod_lotto[], &
													ldt_data_stock[], &
													ll_prog_stock[], &
													ls_cod_cliente[], &
													ls_cod_fornitore[], &
													ll_anno_reg_dest_stock, &
													ll_num_reg_dest_stock ) = -1 then
				ROLLBACK;
				fs_messaggio = "Si è verificato un errore in fase di Creazione Destinazioni Magazzino.~n~r" + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if
			
			if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
											 ll_num_reg_dest_stock, &
											 ls_cod_tipo_movimento, &
											 ls_cod_prodotto) = -1 then
				ROLLBACK;
				fs_messaggio = "Si è verificato un errore in fase di Creazione di Magazzino.~n~r" + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if
		end if
	
		// fare update anag_prodotti	
		if ls_flag_tipo_det_acq = "M" then
			select quan_ordinata
			into :ld_quan_ordinata
			from anag_prodotti
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			ld_quan_ordinata_new = ld_quan_ordinata
			
			ld_residua = (ld_quan_ordine - ld_quan_arrivata)
			if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
				ls_flag_saldo = "S"
				ld_quan_ordinata_new = ld_quan_ordinata - ld_residua
			else
				if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
					ld_quan_ordinata_new = ld_quan_ordinata - (ld_residua)
				else
					ld_quan_ordinata_new = ld_quan_ordinata - ld_quan_acquisto
				end if
			end if
			
			ld_quan_ordinata_new = round(ld_quan_ordinata_new, 4)
			
			// faccio la differenza fra la quantita ordinata originale e quella attuale per eventuale aggiornamento del prodotto raggruppato
			ld_quan_ordinata_new_raggr = ld_quan_ordinata - ld_quan_ordinata_new
			
			update anag_prodotti  
				set quan_ordinata = :ld_quan_ordinata_new 
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode = -1 then
				fs_messaggio= "Si è verificato un errore in fase di aggiornamento magazzino.~n~r" + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				rollback;
				return -1
			end if
			
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
//				select quan_ordinata
//				into :ld_quan_ordinata
//				from anag_prodotti
//				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
//						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
//				ld_quan_ordinata_new_raggr = ld_quan_ordinata
//				
//				ld_residua = (ld_quan_ordine - ld_quan_arrivata)
//				if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
//					ls_flag_saldo = "S"
//					ld_quan_ordinata_new_raggr = ld_quan_ordinata - ld_residua
//				else
//					if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
//						ld_quan_ordinata_new_raggr = ld_quan_ordinata - (ld_residua)
//					else
//						ld_quan_ordinata_new_raggr = ld_quan_ordinata - ld_quan_acquisto
//					end if
//				end if
//				
//				ld_quan_ordinata_new_raggr = round(ld_quan_ordinata_new, 4)

				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordinata_new_raggr, "M")
				
				update anag_prodotti  
				set    quan_ordinata = quan_ordinata -  :ld_quan_raggruppo
				where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					fs_messaggio= "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext
					destroy lds_rag_documenti
					SetPointer(lp_old_pointer)
					rollback;
					return -1
				end if
			end if
			
		end if
		
		ld_quan_acquisto = round(ld_quan_acquisto,4)
		
		update det_ord_acq
			set quan_arrivata = quan_arrivata + :ld_quan_acquisto,
				 flag_saldo = :ls_flag_saldo
		 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_acq.anno_registrazione = :ll_anno_ordine and  
				 det_ord_acq.num_registrazione = :ll_num_ordine and
				 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
	
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Acquisto.~n~r" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		
		select count(*)
		into   :ll_righe_non_saldate
		from   det_ord_acq
	   where  cod_azienda = :s_cs_xx.cod_azienda
		and	 anno_registrazione = :ll_anno_ordine
		and	 num_registrazione = :ll_num_ordine
		and    flag_saldo='N';

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Ordine Acquisto.~n~r" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	
		if ll_righe_non_saldate >0 then
			ls_flag_evasione = "P"
		else
			ls_flag_evasione = "E"
		end if
		
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_bl
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_acq
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			setnull(ls_nota_dettaglio)
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		
	
	//07/02/2011 i dati li devo prendere dal datastore e non dalla dw, visto che il datatstore ha subito un sort diverso .....
	
		// stefanop 16/07/2010: progetto 140 funzione 3: creo movimenti di magazzino
		if tab_1.det_2.rb_attiva_scarico.checked and lds_rag_documenti.getitemstring(ll_i, "flag_tipo_riga") <> "3"  and&	
			 lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")<>"" and not isnull( lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")) then
			
			if wf_crea_mov_magazzino(ll_i, lds_rag_documenti, ldt_data_registrazione, ref ll_anno_reg_mov_mag, ref ll_num_reg_mov_mag, ref fs_messaggio) < 0 then
				rollback;
				return -1
			end if
			
			wf_aggiorna_mov_grezzo_referenza(ll_anno_reg_mov_mag, ll_num_reg_mov_mag, &
													ldt_data_bolla, ls_cod_bol_acq, ls_referenza_mov_grezzo)
			//----------------------------------------------------------------------------------------------------------------------
			
		else
			setnull(ll_anno_reg_mov_mag)
			setnull(ll_num_reg_mov_mag)
		end if
		// ----

		insert into det_bol_acq
						(cod_azienda,   
						 anno_bolla_acq,   
						 num_bolla_acq,    
						 prog_riga_bolla_acq,   
						 cod_tipo_det_acq,   
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_arrivata,   
						 prezzo_acquisto,   
						 fat_conversione,   
						 sconto_1,   
						 sconto_2,   
						 sconto_3,   
						 cod_iva,   
						 quan_fatturata,
						 val_riga,				 
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 flag_saldo,
						 flag_accettazione,
						 flag_blocco,
						 nota_dettaglio,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_ordine_acq,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 prog_stock,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 anno_reg_bol_ven,
						 num_reg_bol_ven,
						 prog_riga_bol_ven,
						 anno_reg_mov_mag_terzista, // stefanop 22/07/2010
						 num_reg_mov_mag_terzista)						 
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_esercizio,   
						 :il_num_bolla_acq,     
						 :ll_prog_riga_bol_acq,   
						 :ls_cod_tipo_det_acq,   
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_acquisto,   
						 :ld_prezzo_acquisto,   
						 :ld_fat_conversione_acq,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_sconto_3,  
						 :ls_cod_iva,   
						 null,
						 :ld_val_riga_sconto_10,
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ldt_data_stock[1],
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_flag_saldo,
						 :ls_flag_acc_materiali,
						 'N',
						 :ls_nota_dettaglio,   
						 :ll_anno_ordine,   
						 :ll_num_ordine,   
						 :ll_prog_riga_ordine_acq,
						 :ll_anno_commessa,
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 :ll_prog_stock[1],
						 :ll_anno_reg_dest_stock,
						 :ll_num_reg_dest_stock,
						 'I',
						 'I',
						 :ll_anno_bol_ven,
						 :ll_num_bol_ven,
						 :ll_riga_bol_ven,
						 :ll_anno_reg_mov_mag, // stefanop 22/07/2010
						 :ll_num_reg_mov_mag);
	
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Dettagli Bolle Acquisto.~n~r" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		
	end if
	wf_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine)
	ls_key_sort_old = ls_key_sort
	
next

if wf_avanzamento_produzione(ref fs_messaggio) = -1 then
	destroy lds_rag_documenti
	SetPointer(lp_old_pointer)
	rollback;
	return -1
end if

commit;

if ls_flag_conferma = "S" then
	if f_conferma_bol_acq( ll_anno_esercizio, il_num_bolla_acq, ref fs_messaggio, ls_flag_acc_materiali ) = -1 then
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		rollback;
		return -1
	else
		
		commit;
		
	end if
end if

destroy lds_rag_documenti
SetPointer(lp_old_pointer)
return 0
end function

public function integer wf_calcola_stato_ordine (long fl_anno_registrazione, long fl_num_registrazione);string ls_flag_stato_ordine
long   ll_cont=0, ll_evasi=0, ll_parziali=0, ll_aperti=0

select count(*)
into   :ll_aperti
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_prodotto is not null and
		 quan_arrivata = 0 and
		 flag_saldo = 'N';
if sqlca.sqlcode = -1 then return -1
		 
select count(*)
into   :ll_parziali
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 quan_arrivata > 0 and
		 quan_arrivata < quan_ordinata and
		 flag_saldo = 'N';
if sqlca.sqlcode = -1 then return -1
		 
select count(*)
into   :ll_evasi
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 (quan_arrivata = quan_ordinata or flag_saldo = 'S');
if sqlca.sqlcode = -1 then return -1

if isnull(ll_evasi) then ll_evasi = 0
if isnull(ll_aperti) then ll_aperti = 0
if isnull(ll_parziali) then ll_parziali = 0

ll_cont = ll_evasi + ll_parziali + ll_aperti
if ll_cont = 0 then 
	ls_flag_stato_ordine = "A"
else
	if ll_cont = ll_evasi then
		ls_flag_stato_ordine = "E"
	elseif ll_cont = ll_aperti then
		ls_flag_stato_ordine = "A"
	else
		ls_flag_stato_ordine = "P"
	end if
end if

update tes_ord_acq
set    flag_evasione = :ls_flag_stato_ordine
where  cod_azienda  = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento STATO ORDINE di acquisto. " + sqlca.sqlerrtext)
	return -1
end if

return 0
end function

public function integer wf_genera_fatture_acq (ref string fs_messaggio);string 	ls_cod_tipo_ord_acq, ls_flag_riep_fat, ls_cod_tipo_fat_acq, ls_flag_calcolo, &
			ls_flag_destinazione, ls_flag_num_doc, ls_flag_imballo, &
		 	ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, ls_flag_resa, &
		 	ls_flag_data_consegna, ls_cod_fornitore[], ls_cod_valuta, ls_cod_pagamento, &
		 	ls_cod_banca, ls_cod_banca_clien_for, ls_cod_des_fornitore, &
		 	ls_num_ord_fornitore, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
		 	ls_cod_porto, ls_cod_resa, ls_sort, ls_key_sort, ls_key_sort_old, ls_cod_tipo_fat_ven, &
		 	ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_cod_tipo_analisi_ord, &
		 	ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_deposito[], &
		 	ls_cod_tipo_listino_prodotto, ls_nota_testata, ls_nota_piede, ls_lire, &
		 	ls_cod_tipo_analisi_fat, ls_cod_doc_origine, ls_cod_fil_fornitore, ls_cod_tipo_det_acq, &
		 	ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_cod_prod_fornitore, &
		 	ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
		 	ls_flag_sola_iva, ls_flag_saldo, ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], &
		 	ls_referenza, ls_flag_evasione, ls_numeratore_doc_origine, ls_flag_conferma, ls_flag_acc_materiali, &
		 	ls_flag_doc_suc_det, ls_flag_agg_costo_ultimo,ls_cod_prodotto_raggruppato, ls_flag_tipo_riga, ls_cod_prodotto_alt, &
			ls_referenza_mov_grezzo, ls_cod_deposito_trasf, ls_cod_natura_intra
		 
integer 	li_anno_doc_origine, li_numero

dec{6} 	ld_cambio_acq
dec{5}	ld_fat_conversione_acq, ld_fat_conversione
dec{4}	ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_ordine, &
		 	ld_sconto_pagamento, ld_quan_ordine, ld_prezzo_acquisto, &
		 	ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
		 	ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 	ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
		 	ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
		 	ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
		 	ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
		 	ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
		 	ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
		 	ld_val_sconto_pagamento, ld_val_evaso, ld_quan_ordinata_new, ld_residua , &
		 	ld_quan_ordinata, ld_quan_ordinata_new_raggr,ld_quan_raggruppo, &
			ld_quan_acquisto_alt
			 
		 
long 		ll_anno_ordine, ll_num_ordine, ll_prog_riga_documento, ll_progressivo, ll_prog_riga_fat_acq ,&
	  		ll_prog_riga_ordine_acq, ll_anno_commessa, ll_num_commessa, ll_prog_stock[], &
	  		ll_anno_reg_dest_stock, ll_i, ll_max_num_reg, &
	  		ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], &
	  		ll_anno_esercizio, ll_num_doc_origine,ll_righe_non_saldate, &
			ll_anno_reg_mov_mag, ll_num_reg_mov_mag,ll_find
	  
datetime ldt_data_consegna, ldt_data_ordine, ldt_data_ord_fornitore, &
			ldt_data_fattura, ldt_data_consegna_ordine, ldt_data_stock[], ldt_oggi

datastore lds_rag_documenti
uo_calcola_documento_euro luo_doc

pointer lp_old_pointer
lp_old_pointer = SetPointer(HourGlass!)


//----------------------------------------------------------------------------------
select numero
  into :li_numero		
  from parametri_azienda
 where cod_azienda = :s_cs_xx.cod_azienda
	and flag_parametro = 'N'
	and cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati da tabella parametri azienda.~n~r" + sqlca.sqlerrtext
	return -1
end if

if isnull(is_cod_fornitore) then
//	if isnull(is_anno_reg_fat_acq) or isnull(is_num_reg_fat_acq) then
		il_anno_reg_fat_acq = li_numero
	
		select max(num_registrazione) + 1
		  into :il_num_reg_fat_acq
		  from tes_fat_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
			and anno_registrazione = :il_anno_reg_fat_acq;			
		
		if isnull(il_num_reg_fat_acq) or il_num_reg_fat_acq < 1 then il_num_reg_fat_acq = 1
//	end if			
end if
//----------------------------------------------------------------------------------

lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_ord_acq"
lds_rag_documenti.settransobject(sqlca)

ls_cod_deposito[1] = s_cs_xx.parametri.parametro_s_1

//se il codice ubicazione è stringa vuota forza a NULL
if trim(s_cs_xx.parametri.parametro_s_2)="" then setnull(s_cs_xx.parametri.parametro_s_2)
ls_cod_ubicazione[1] = s_cs_xx.parametri.parametro_s_2

//se il codice lotto è stringa vuota forza a NULL
if trim(s_cs_xx.parametri.parametro_s_3)="" then setnull(s_cs_xx.parametri.parametro_s_3)
ls_cod_lotto[1] = s_cs_xx.parametri.parametro_s_3

ls_flag_conferma = s_cs_xx.parametri.parametro_s_4
ls_flag_acc_materiali = s_cs_xx.parametri.parametro_s_5
ls_flag_calcolo = s_cs_xx.parametri.parametro_s_6
ldt_data_fattura = s_cs_xx.parametri.parametro_data_1
ll_anno_esercizio = f_anno_esercizio()
li_anno_doc_origine = f_anno_esercizio()
ldt_oggi = datetime(today())
fs_messaggio = "Fatture Generate: "

for ll_i = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
	tab_1.det_2.dw_ext_carico_acq_det.accepttext()
	ll_anno_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "anno_registrazione")
	ll_num_ordine = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ordine_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "prog_riga_ord_acq")
	ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "quan_acquisto")
	ls_flag_agg_costo_ultimo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_agg_costo_ultimo")
	
	if isnull(ls_flag_agg_costo_ultimo) then ls_flag_agg_costo_ultimo = "N"
	
	if ls_flag_agg_costo_ultimo = "S" then
	
		update det_ord_acq
		set    flag_agg_costo_ultimo = :ls_flag_agg_costo_ultimo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ordine and
				 num_registrazione = :ll_num_ordine and
				 prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante Aggiornamento Flag Aggiorna Costo Ultimo."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if			 

	end if
	
	ld_fat_conversione = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "fat_conversione")
	ld_quan_acquisto = ld_quan_acquisto / ld_fat_conversione
	ld_quan_acquisto = round(ld_quan_acquisto, 4)
	select tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_valuta,   
			 tes_ord_acq.cod_pagamento,   
			 tes_ord_acq.cod_banca,   
			 tes_ord_acq.cod_banca_clien_for,
			 tes_ord_acq.cod_tipo_ord_acq,
			 tes_ord_acq.cod_des_fornitore,   
			 tes_ord_acq.num_ord_fornitore
	 into  :ls_cod_fornitore[1],
			 :ls_cod_valuta,   
			 :ls_cod_pagamento,   
			 :ls_cod_banca,   
			 :ls_cod_banca_clien_for,
			 :ls_cod_tipo_ord_acq,
			 :ls_cod_des_fornitore,   
			 :ls_num_ord_fornitore
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_acq.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	//salvo la ragione sociale del cod_fornitore
	ls_referenza_mov_grezzo = f_des_tabella("anag_fornitori",  "cod_fornitore='"+ls_cod_fornitore[1]+"'", "rag_soc_1")
	
	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	ls_flag_tipo_riga = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_tipo_riga")
	
	ls_cod_prodotto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "cod_prodotto_alt")
	if ls_cod_prodotto_alt="" then setnull(ls_cod_prodotto_alt)
	
	ld_quan_acquisto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_i, "quan_acquisto_alt")
	//------------------------------------------------------------------------------------------------------------

	lds_rag_documenti.insertrow(0)
	lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_ordine_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_fornitore", ls_cod_fornitore[1])
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_acq	)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_saldo", ls_flag_saldo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto", ld_quan_acquisto)
	
	//07/02/2011  salvo anche i dati eventuali del grezzo -------------------------------------------------
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_tipo_riga", ls_flag_tipo_riga)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_prodotto_alt", ls_cod_prodotto_alt)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto_alt", ld_quan_acquisto_alt)
	//------------------------------------------------------------------------------------------------------------
	
next

ls_sort = "cod_fornitore A, cod_valuta A, cod_pagamento A, cod_banca A, cod_banca_clien_for A, " + &
			 "anno_documento A, num_documento A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""
for ll_i = 1 to lds_rag_documenti.rowcount()
	ll_anno_ordine = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_ordine = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ll_prog_riga_ordine_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")

	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	ls_cod_tipo_ord_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tes_ord_acq.cod_operatore,   
			 tes_ord_acq.data_registrazione,   
			 tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_des_fornitore,   
			 tes_ord_acq.cod_fil_fornitore,   			 
			 tes_ord_acq.rag_soc_1,   
			 tes_ord_acq.rag_soc_2,   
			 tes_ord_acq.indirizzo,   
			 tes_ord_acq.localita,   
			 tes_ord_acq.frazione,   
			 tes_ord_acq.cap,   
			 tes_ord_acq.provincia,   
			 tes_ord_acq.cod_deposito,
			 tes_ord_acq.cod_deposito_trasf,
			 tes_ord_acq.cod_valuta,   
			 tes_ord_acq.cambio_acq,   
			 tes_ord_acq.cod_tipo_listino_prodotto,   
			 tes_ord_acq.cod_pagamento,   
			 tes_ord_acq.sconto,   
			 tes_ord_acq.cod_banca,   
			 tes_ord_acq.num_ord_fornitore,   
			 tes_ord_acq.data_ord_fornitore,   
			 tes_ord_acq.nota_testata,   
			 tes_ord_acq.nota_piede,   
			 tes_ord_acq.tot_val_evaso,
			 tes_ord_acq.tot_val_ordine,
			 tes_ord_acq.cod_banca_clien_for,
			 tes_ord_acq.cod_mezzo,
			 tes_ord_acq.cod_natura_intra,
			 tes_ord_acq.cod_porto
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_fornitore[1],   
			 :ls_cod_des_fornitore,   
			 :ls_cod_fil_fornitore,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],
			 :ls_cod_deposito_trasf,
			 :ls_cod_valuta,   
			 :ld_cambio_acq,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_banca,   
			 :ls_num_ord_fornitore,   
			 :ldt_data_ord_fornitore,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_mezzo,
			 :ls_cod_natura_intra,
			 :ls_cod_porto
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_acq.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	if ls_cod_mezzo="" then setnull(ls_cod_mezzo)
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)
	if ls_cod_porto="" then setnull(ls_cod_porto)
	
	if not isnull(ls_cod_deposito_trasf) and ls_cod_deposito_trasf<>"" then
		ls_cod_deposito[1] = ls_cod_deposito_trasf
	end if
	

	if ls_key_sort <> ls_key_sort_old then
		if ls_key_sort_old <> "" and ls_flag_conferma = "S" then
			if f_conferma_fat_acq_new( il_anno_reg_fat_acq, il_num_reg_fat_acq, ref fs_messaggio, ls_flag_acc_materiali ) = -1 then
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if
		end if
	
		if ls_key_sort_old <> "" and ls_flag_calcolo = "S" then
			luo_doc = create uo_calcola_documento_euro
			
			if luo_doc.uof_calcola_documento(il_anno_reg_fat_acq, il_num_reg_fat_acq,"fat_acq",fs_messaggio) <> 0 then
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if
			destroy luo_doc
			
		end if
		
		select	cod_tipo_fat_acq   
		into 		:ls_cod_tipo_fat_acq 
		from 		tab_tipi_ord_acq
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
		
		if isnull(ls_cod_tipo_fat_acq) or ls_cod_tipo_fat_acq = "" then
			fs_messaggio = "Tipo Fattura di Acquisto non Definito in Tabella Tipi Ordini di Acquisto"
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		
		select	cod_documento,   
					numeratore_documento  
		into 		:ls_cod_doc_origine,   
					:ls_numeratore_doc_origine  
		from 		tab_tipi_fat_acq  
		where 	tab_tipi_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and
					tab_tipi_fat_acq.cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
			
		if isnull(ls_cod_doc_origine) or ls_cod_doc_origine = "" then
			fs_messaggio = "Documento non Definito in Tabella Tipi Fattura Acquisto"
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		
		if isnull(ls_numeratore_doc_origine) or ls_cod_doc_origine = "" then
			fs_messaggio = "Numeratore non Definito in Tabella Tipi Fattura Acquisto"
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
			
		select	num_documento
		into 		:ll_num_doc_origine
		from 		numeratori
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_documento = :ls_cod_doc_origine and
					numeratore_documento = :ls_numeratore_doc_origine and
					anno_documento = :li_anno_doc_origine;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Numeratore non Definito per Codice Documento e Anno Esercizio Correnti"
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
			
		ll_num_doc_origine ++
			
		update numeratori
		set    num_documento = :ll_num_doc_origine
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_documento = :ls_cod_doc_origine and
				 numeratore_documento = :ls_numeratore_doc_origine and
				 anno_documento = :li_anno_doc_origine;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Numeratori."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		
		select max(tes_fat_acq.progressivo)
		into   :ll_progressivo  
		from   tes_fat_acq 
		where  tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_acq.cod_doc_origine = :ls_cod_doc_origine and
				 tes_fat_acq.numeratore_doc_origine = :ls_numeratore_doc_origine and
				 tes_fat_acq.anno_doc_origine = :li_anno_doc_origine and
				 tes_fat_acq.num_doc_origine = :ll_num_doc_origine;

		if isnull(ll_progressivo) or ll_progressivo = 0 then
			ll_progressivo = 1
		else
			ll_progressivo ++
		end if

		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if

		select max(num_registrazione) + 1
		  into :ll_max_num_reg
		  from tes_fat_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and anno_registrazione = :il_anno_reg_fat_acq;
			
		if isnull(ll_max_num_reg) then ll_max_num_reg = 1

		insert into tes_fat_acq
						( cod_azienda,
						anno_registrazione,
						num_registrazione,  
						cod_tipo_fat_acq,   
						data_doc_origine,   
						cod_documento,   
						numeratore_documento,   
						anno_documento,   
						num_documento,   
						data_registrazione,   
						cod_operatore,   
						cod_fornitore,   
						cod_fil_fornitore,   
						cod_valuta,   
						cambio_acq,   
						cod_tipo_listino_prodotto,   
						cod_pagamento,   
						cod_banca,   
						cod_banca_clien_for,   
						cod_documento_nota,   
						numeratore_nota,   
						anno_nota,   
						num_nota,   
						tot_val_fat_acq,   
						tot_valuta_fat_acq,   
						importo_iva,   
						imponibile_iva,   
						importo_valuta_iva,   
						imponibile_valuta_iva,   
						importo_iva_indetraibile,   
						flag_fat_confermata,   
						sconto,
						cod_mezzo,
						cod_natura_intra,
						cod_porto)
		values 		( :s_cs_xx.cod_azienda,   
						:il_anno_reg_fat_acq,   
						:ll_max_num_reg,   
						:ls_cod_tipo_fat_acq,   
						null,   
						null,   
						null,   
						null,   
						null,   
						:ldt_data_fattura,   
						:ls_cod_operatore,   
						:ls_cod_fornitore[1],   
						:ls_cod_fil_fornitore,   
						:ls_cod_valuta,   
						:ld_cambio_acq,   
						:ls_cod_tipo_listino_prodotto,   
						:ls_cod_pagamento,   
						:ls_cod_banca,   
						:ls_cod_banca_clien_for,   
						null,   
						null,   
						null,   
						null,
						0,
						0,
						0, 
						0,
						0,   
						0,   
						0,   
						'N',   
						0,
						:ls_cod_mezzo,
						:ls_cod_natura_intra,
						:ls_cod_porto);						

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Testata Fatture Acquisto.~n~r" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if

		ll_prog_riga_fat_acq = 0

	end if

	fs_messaggio = fs_messaggio + string(il_anno_reg_fat_acq) + "/" + string(ll_max_num_reg)

	ll_prog_riga_fat_acq = ll_prog_riga_fat_acq + 10
	
	select	det_ord_acq.cod_tipo_det_acq,   
				det_ord_acq.cod_prodotto,   
				det_ord_acq.cod_misura,   
				det_ord_acq.des_prodotto,   
				det_ord_acq.quan_ordinata,
				det_ord_acq.prezzo_acquisto,   
				det_ord_acq.fat_conversione,   
				det_ord_acq.sconto_1,   
				det_ord_acq.sconto_2,  
				det_ord_acq.sconto_3,  
				det_ord_acq.cod_iva,   
				det_ord_acq.data_consegna,
				det_ord_acq.cod_prod_fornitore,
				det_ord_acq.quan_arrivata,
				det_ord_acq.val_riga,				
				det_ord_acq.nota_dettaglio,   
				det_ord_acq.anno_commessa,
				det_ord_acq.num_commessa,
				det_ord_acq.cod_centro_costo,
				det_ord_acq.sconto_4,   
				det_ord_acq.sconto_5,   
				det_ord_acq.sconto_6,   
				det_ord_acq.sconto_7,   
				det_ord_acq.sconto_8,   
				det_ord_acq.sconto_9,   
				det_ord_acq.sconto_10,
				det_ord_acq.flag_doc_suc_det				
	into		:ls_cod_tipo_det_acq, 
				:ls_cod_prodotto, 
				:ls_cod_misura, 
				:ls_des_prodotto, 
				:ld_quan_ordine,
				:ld_prezzo_acquisto, 
				:ld_fat_conversione_acq, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_sconto_3, 
				:ls_cod_iva, 
				:ldt_data_consegna_ordine,
				:ls_cod_prod_fornitore,
				:ld_quan_arrivata,
				:ld_val_riga,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_flag_doc_suc_det				
	from 		det_ord_acq  
	where 	det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_ord_acq.anno_registrazione = :ll_anno_ordine and  
				det_ord_acq.num_registrazione = :ll_num_ordine and
				det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq
	order by det_ord_acq.prog_riga_ordine_acq asc;

	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Ordini Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	//############################################################
	//rileggo il fattore conversione acquisti dalla datawindow, potrebbe essere cambiato
	ll_find = tab_1.det_2.dw_ext_carico_acq_det.find(	"anno_registrazione="+string(ll_anno_ordine)+" "+&
																	"and num_registrazione="+string(ll_num_ordine)+" "+&
																	"and prog_riga_ord_acq="+string(ll_prog_riga_ordine_acq)+"", &
																	1, tab_1.det_2.dw_ext_carico_acq_det.rowcount())
	if ll_find>0 then
		ld_fat_conversione_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "fat_conversione")
		
		//ricalcolo il prezzo unitario acquisto  (€/UMmag) = (€/UMacq) * (UMacq/UMmag)
		//ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto") * ld_fat_conversione_acq
		ld_prezzo_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_find, "prezzo_acquisto_mag")
	end if
	//############################################################
	
	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_flag_tipo_det_acq = "M" then
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if

		setnull(ldt_data_stock[1])
		setnull(ll_prog_stock[1])
		setnull(ls_cod_cliente[1])
		setnull(ls_cod_fornitore[1])
		
		setnull(ls_referenza)
		
		if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
												ls_cod_prodotto, &
												ls_cod_deposito[], &
												ls_cod_ubicazione[], &
												ls_cod_lotto[], &
												ldt_data_stock[], &
												ll_prog_stock[], &
												ls_cod_cliente[], &
												ls_cod_fornitore[], &
												ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock ) = -1 then
			ROLLBACK;
			fs_messaggio = "Si è verificato un errore in fase di Creazione Destinazioni Magazzino."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
										 ll_num_reg_dest_stock, &
										 ls_cod_tipo_movimento, &
										 ls_cod_prodotto) = -1 then
			ROLLBACK;
			fs_messaggio = "Si è verificato un errore in fase di Creazione di Magazzino."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	end if

	if ls_flag_tipo_det_acq = "M" then
// fare update anag_prodotti
		select quan_ordinata
		into :ld_quan_ordinata
		from anag_prodotti
		where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		ld_quan_ordinata_new = ld_quan_ordinata
		
		ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
		
		ld_residua = (ld_quan_ordine - ld_quan_arrivata)
		if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
			ls_flag_saldo = "S"
			ld_quan_ordinata_new = ld_quan_ordinata - ld_residua
		else
			if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
				ld_quan_ordinata_new = ld_quan_ordinata - (ld_residua)
			else
				ld_quan_ordinata_new = ld_quan_ordinata - ld_quan_acquisto
			end if
		end if
		
		update anag_prodotti  
			set quan_ordinata = :ld_quan_ordinata_new
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode = -1 then
			fs_messaggio= "Si è verificato un errore in fase di aggiornamento magazzino."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			rollback;
			return -1
		end if
		
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			select quan_ordinata
			into :ld_quan_ordinata
			from anag_prodotti
			where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			ld_quan_ordinata_new_raggr = ld_quan_ordinata
			
			ls_flag_saldo = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
			
			ld_residua = (ld_quan_ordine - ld_quan_arrivata)
			
			if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
				ls_flag_saldo = "S"
				ld_quan_ordinata_new_raggr = ld_quan_ordinata -  f_converti_qta_raggruppo(ls_cod_prodotto, ld_residua, "M")
			else
				if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
					ld_quan_ordinata_new_raggr = ld_quan_ordinata - f_converti_qta_raggruppo(ls_cod_prodotto, ld_residua, "M")
				else
					ld_quan_ordinata_new_raggr = ld_quan_ordinata - f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_acquisto, "M")
				end if
			end if

			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordinata_new_raggr, "M")

			update anag_prodotti  
			set    quan_ordinata = :ld_quan_raggruppo
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;

			if sqlca.sqlcode = -1 then
				fs_messaggio= "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				rollback;
				return -1
			end if
		end if
		
	end if
	// fine update anag_prodotti

	update det_ord_acq
		set quan_arrivata = quan_arrivata + :ld_quan_acquisto,
			 flag_saldo = :ls_flag_saldo
	 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 det_ord_acq.num_registrazione = :ll_num_ordine and
			 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	select count(*)
	into   :ll_righe_non_saldate
	from   det_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 anno_registrazione = :ll_anno_ordine
	and	 num_registrazione = :ll_num_ordine
	and    flag_saldo='N';

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Ordine Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ll_righe_non_saldate >0 then
		ls_flag_evasione = "P"
	else
		ls_flag_evasione = "E"
	end if

//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_ft
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_acq
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			setnull(ls_nota_dettaglio)
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		

	// stefanop 16/07/2010: progetto 140 funzione 3: creo movimenti di magazzino
	if tab_1.det_2.rb_attiva_scarico.checked and  lds_rag_documenti.getitemstring(ll_i, "flag_tipo_riga") <> "3"  and&	
			 lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")<>"" and not isnull( lds_rag_documenti.getitemstring(ll_i, "cod_prodotto_alt")) then
		
		if wf_crea_mov_magazzino(ll_i, lds_rag_documenti, ldt_data_fattura, ref ll_anno_reg_mov_mag, ref ll_num_reg_mov_mag, ref fs_messaggio) < 0 then
			
			rollback;
			return -1
		end if
	else
		setnull(ll_anno_reg_mov_mag)
		setnull(ll_num_reg_mov_mag)
	end if
	// ----

	insert into	det_fat_acq
					(cod_azienda,   
					anno_registrazione,   
					num_registrazione,   
					prog_riga_fat_acq,   
					cod_tipo_det_acq,   
					cod_misura,   
					cod_prodotto,   
					des_prodotto,   
					quan_fatturata,   
					prezzo_acquisto,   
					fat_conversione,   
					sconto_1,   
					sconto_2,   
					sconto_3,   
					cod_iva,   
					cod_deposito,   
					cod_ubicazione,   
					cod_lotto,   
					data_stock,   
					cod_tipo_movimento,   
					num_registrazione_mov_mag,   
					cod_conto,   
					nota_dettaglio,   
					anno_bolla_acq,   
					num_bolla_acq,   
					prog_riga_bolla_acq,   
					flag_accettazione,   
					anno_commessa,   
					num_commessa,   
					cod_centro_costo,   
					sconto_4,   
					sconto_5,   
					sconto_6,   
					sconto_7,   
					sconto_8,   
					sconto_9,   
					sconto_10,   
					prog_stock,   
					anno_reg_des_mov,   
					num_reg_des_mov,
					anno_reg_ord_acq,
					num_reg_ord_acq,
					prog_riga_ordine_acq,
					prog_bolla_acq,
					anno_reg_mov_mag_ret,
					num_reg_mov_mag_ret,
					flag_st_note_det,
					anno_registrazione_mov_mag,
					anno_reg_mov_mag_terzista, // stefanop 22/07/2010
					num_reg_mov_mag_terzista)					
	values		(:s_cs_xx.cod_azienda,   
				   :il_anno_reg_fat_acq, 
				   :ll_max_num_reg,
					:ll_prog_riga_fat_acq,   
					:ls_cod_tipo_det_acq,   
					:ls_cod_misura,   
					:ls_cod_prodotto,   
					:ls_des_prodotto,   
					:ld_quan_acquisto,   
					:ld_prezzo_acquisto,   
					:ld_fat_conversione_acq,   
					:ld_sconto_1,   
					:ld_sconto_2,   
					:ld_sconto_3,   
					:ls_cod_iva,   
					:ls_cod_deposito[1],   
					:ls_cod_ubicazione[1],   
					:ls_cod_lotto[1],   
					:ldt_data_stock[1],   
					:ls_cod_tipo_movimento,   
					null,   
					null,   
					:ls_nota_dettaglio,   
					null,   
					null,   
					null,   
					:ls_flag_acc_materiali,   
					:ll_anno_commessa,   
					:ll_num_commessa,   
					:ls_cod_centro_costo,   
					:ld_sconto_4,   
					:ld_sconto_5,   
					:ld_sconto_6,   
					:ld_sconto_7,   
					:ld_sconto_8,   
					:ld_sconto_9,   
					:ld_sconto_10,   
					:ll_prog_stock[1],   
					:ll_anno_reg_dest_stock,
					:ll_num_reg_dest_stock,
					:ll_anno_ordine,
					:ll_num_ordine,
					:ll_prog_riga_ordine_acq,
					null,
					null,
					null,
					'I',
					null,
					:ll_anno_reg_mov_mag, // stefanop 22/07/2010
					:ll_num_reg_mov_mag);					

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Fatture Acquisto.~n~r" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	wf_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine)
	
	if ll_i = lds_rag_documenti.rowcount() and ls_flag_conferma = "S" then
		if f_conferma_fat_acq_new( il_anno_reg_fat_acq, il_num_reg_fat_acq, ref fs_messaggio, ls_flag_acc_materiali ) = -1 then
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	end if

	if ll_i = lds_rag_documenti.rowcount() and ls_flag_calcolo = "S" then
		
		luo_doc = create uo_calcola_documento_euro
		
		if luo_doc.uof_calcola_documento(il_anno_reg_fat_acq, il_num_reg_fat_acq,"fat_acq",fs_messaggio) <> 0 then
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		else
			commit;
		end if
		destroy luo_doc
		
	end if
	ls_key_sort_old = ls_key_sort	
next

destroy lds_rag_documenti

SetPointer(lp_old_pointer)

return 0
end function

public function integer wf_avanzamento_produzione (ref string fs_messaggio);// procedura che esegue avanzamento produzione sulla base delle fasi selezionate nella tab_1.det_4.dw_avan_produzione


string   ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_prodotto_fase,& 
			ls_cod_versione,ls_errore,ls_cod_mov_magazzino, ls_cod_prodotto, ls_cod_versione_fase, &
			ls_cod_tipo_det_ven,ls_cod_tipo_bol_ven,ls_conferma, ls_cod_fornitore_ingresso, ls_cod_prodotto_scarico,& 
			ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[], ls_flag_tipo_avanzamento,&
			ls_cod_deposito_sl[],ls_cod_tipo_mov_ver_sl,ls_cod_ubicazione_test, ls_cod_deposito_fornitore[],& 
			ls_cod_lotto_test,ls_flag_cliente,ls_flag_fornitore,ls_cod_tipo_mov_scarico_terzista,ls_cod_materia_prima[]

long     ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_ingresso,ll_t,ll_anno_reg_sl,ll_num_reg_sl, & 
			ll_prog_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_anno_registrazione[],ll_num_registrazione[], & 
		   ll_righe_scarico,ll_num_fasi_aperte,ll_anno_commessa,ll_risposta, ll_anno_reg_ord_acq, ll_num_reg_ord_acq, &
			ll_prog_riga_ord_acq, ll_i

dec{4}   ldd_quan_entrata,ldd_quan_prelevata,ldd_quan_in_produzione,ldd_quan_in_produzione_prog,ldd_quan_prodotta,&
			ldd_quan_impegnata_attuale,ldd_quan_impegnata_fornitore,ldd_quan_utilizzo,ldd_quan_impegnata,&
			ldd_somma_quan_prelevata,ld_quan_mp[],ldd_quan_disponibile,ld_quan_residua_scarico,ld_quan_prelevata

datetime ldt_oggi,ld_data_stock[]

uo_magazzino luo_mag

/* dichiarazione cursori */
declare  cu_righe_stock cursor for
SELECT  	cod_ubicazione,   
		  	cod_lotto,   
		  	data_stock,   
		  	prog_stock,
		  	giacenza_stock - quan_assegnata - quan_in_spedizione  
FROM   	stock   
where  	cod_azienda  = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_materia_prima[ll_t] and
			cod_deposito = :ls_cod_deposito_fornitore[1] and
			giacenza_stock - quan_assegnata - quan_in_spedizione >0
ORDER BY data_stock asc;




ll_righe_scarico = tab_1.det_4.dw_avan_produzione.rowcount()

if ll_righe_scarico < 1 then return 0

ll_anno_reg_ord_acq = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"anno_registrazione")
ll_num_reg_ord_acq = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"num_registrazione")
ll_prog_riga_ord_acq = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"prog_riga_ord_acq")

ll_anno_commessa = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"anno_commessa")
ll_num_commessa = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"num_commessa")
ll_prog_riga = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"prog_riga")
ls_cod_prodotto_fase = tab_1.det_4.dw_avan_produzione.getitemstring(tab_1.det_4.dw_avan_produzione.getrow(),"cod_prodotto_fase")		
ls_cod_versione_fase = tab_1.det_4.dw_avan_produzione.getitemstring(tab_1.det_4.dw_avan_produzione.getrow(),"cod_versione")
ldd_quan_entrata = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"quan_entrata")
ls_cod_reparto = tab_1.det_4.dw_avan_produzione.getitemstring(tab_1.det_4.dw_avan_produzione.getrow(),"cod_reparto")		
ls_cod_lavorazione = tab_1.det_4.dw_avan_produzione.getitemstring(tab_1.det_4.dw_avan_produzione.getrow(),"cod_lavorazione")		
ldd_quan_utilizzo = tab_1.det_4.dw_avan_produzione.getitemnumber(tab_1.det_4.dw_avan_produzione.getrow(),"quan_utilizzo")
ls_cod_prodotto_scarico = tab_1.det_4.dw_avan_produzione.getitemstring(tab_1.det_4.dw_avan_produzione.getrow(),"cod_prodotto_scarico")

ldt_oggi = datetime(today(),time('00:00:00'))

select cod_fornitore
into   :ls_cod_fornitore_ingresso
from   tes_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_reg_ord_acq and
		 num_registrazione = :ll_num_reg_ord_acq;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca fornitore in testata ordine~r~n" + sqlca.sqlerrtext
	return -1
end if

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda  and
		 anno_commessa = :ll_anno_commessa  and
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca commessa~r~n" + sqlca.sqlerrtext
	return -1
end if

select cod_deposito_sl,
		 cod_tipo_mov_ver_sl,
		 cod_tipo_mov_scarico_terzista
into   :ls_cod_deposito_sl[1],
		 :ls_cod_tipo_mov_ver_sl,
		 :ls_cod_tipo_mov_scarico_terzista
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca tipo commessa~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_deposito_sl[1]) or isnull(ls_cod_tipo_mov_ver_sl) then
	fs_messaggio = "Attenzione! Il tipo commessa selezionato non è correttamente impostato per eseguire le movimentazioni dei Semilavorati (deposito SL o tipo movimento versamento SL mancante)."
	return -1
end if

select anno_reg_sl,
		 num_reg_sl
into   :ll_anno_reg_sl,
		 :ll_num_reg_sl
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :ll_anno_commessa  AND    
		 num_commessa = :ll_num_commessa    AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca dati avanzamento produzione~r~n" + sqlca.sqlerrtext
	return -1
end if

//*************************** Inizio Verifica correttezza tipo movimento mag per SL

declare righe_det_tipi_m cursor for
select  cod_ubicazione,
		  cod_lotto,
		  flag_cliente,
		  flag_fornitore
from    det_tipi_movimenti
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_movimento=:ls_cod_tipo_mov_ver_sl;

open righe_det_tipi_m;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in OPEN cursore righe_det_tipi_m~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch righe_det_tipi_m
	into  :ls_cod_ubicazione_test,
			:ls_cod_lotto_test,
			:ls_flag_cliente,
			:ls_flag_fornitore;

	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore fetch cursore righe_det_tipi_m~r~n" + sqlca.sqlerrtext
		close righe_det_tipi_m;
		return -1
	end if
	

	if isnull(ls_cod_ubicazione_test) then
		fs_messaggio = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poichè il codice ubicazione non è stato indicato."
		close righe_det_tipi_m;
		return -1
	end if

	if isnull(ls_cod_lotto_test) then
		fs_messaggio = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poichè il codice lotto non è stato indicato."
		close righe_det_tipi_m;
		return -1
	end if

	if ls_flag_cliente='S' then
		fs_messaggio = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poichè viene richiesto il codice cliente."
		close righe_det_tipi_m;
		return -1
	end if

	if ls_flag_fornitore='S' then
		fs_messaggio = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poiche' viene richiesto il codice fornitore."
		close righe_det_tipi_m;
		return -1
	end if

loop

close righe_det_tipi_m;

//*************************** Fine Verifica correttezza tipo movimento mag per SL

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ld_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])
			
if not isnull(ll_anno_reg_sl) and not isnull(ll_num_reg_sl) then
	select cod_ubicazione,
			 cod_lotto,
			 data_stock,
			 prog_stock
	into   :ls_cod_ubicazione[1],
			 :ls_cod_lotto[1],
			 :ld_data_stock[1],
			 :ll_prog_stock[1]
	from   mov_magazzino
	where  cod_azienda =:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_reg_sl
	and	 num_registrazione=:ll_num_reg_sl;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in ricerca del movimento di magazzino di carico SL~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
end if

ll_anno_reg_sl = 0
ll_num_reg_sl = 0

if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase, ls_cod_deposito_sl[], & 
							  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
							  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
							  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

	fs_messaggio = "Si è verificato un errore in preparazione destinazione movimenti."
	return  -1

end if

if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
									ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase) = -1 then

	fs_messaggio = "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino."
	return -1

end if

luo_mag = create uo_magazzino

ll_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
										 ls_cod_tipo_mov_ver_sl, &
										 "N", &
										 ls_cod_prodotto_fase, &
										 ldd_quan_entrata, &
										 1, &
										 ll_num_commessa, &
										 datetime(today(),time('00:00:00')), &
										 "", &
										 ll_anno_reg_des_mov, &
										 ll_num_reg_des_mov, &
										 ls_cod_deposito_sl[], &
										 ls_cod_ubicazione[], &
										 ls_cod_lotto[], &
										 ld_data_stock[], &
										 ll_prog_stock[], &
										 ls_cod_fornitore[], &
										 ls_cod_cliente[], &
										 ll_anno_registrazione[], &
										 ll_num_registrazione[])
										 
if ll_risposta=-1 then
	fs_messaggio = "Errore in creazione movimento di carico del semilavorato."
	return -1
end if
	
ll_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

if ll_risposta = -1 then
	fs_messaggio = "Si è verificato un errore in fase di cancellazione destinazioni movimenti."
	return -1
end if

// stefanop: 15/12/2011: movimenti magazzino prod_raggruppato
long ll_anno_reg_mov_rag[], ll_num_reg_mov_rag[], ll_null

setnull(ll_null)

if luo_mag.uof_movimenti_mag_ragguppato(datetime(today(),time('00:00:00')), &
				ls_cod_tipo_mov_ver_sl, &
				"N", &
				ls_cod_prodotto_fase, &
				ldd_quan_entrata, &
				1, &
				ll_num_commessa, &
				datetime(today(),time('00:00:00')), &
				"", &
				ll_anno_reg_des_mov, &
				ll_num_reg_des_mov, &
				ls_cod_deposito_sl[], &
				ls_cod_ubicazione[], &
				ls_cod_lotto[], &
				ld_data_stock[], &
				ll_prog_stock[], &
				ls_cod_fornitore[], &
				ls_cod_cliente[], &
				ll_anno_reg_mov_rag[], &
				ll_num_reg_mov_rag[],&
				ll_null,&
				ll_anno_registrazione[1],&
				ll_num_registrazione[1]) = -1 then
				
	fs_messaggio = "Si è verificato nel movimento del prodotto raggruppato."
	destroy luo_mag
	return -1
end if
// -----

destroy luo_mag

ll_anno_reg_sl = ll_anno_registrazione[1]
ll_num_reg_sl = ll_num_registrazione[1]

select quan_in_produzione
into   :ldd_quan_in_produzione
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda    AND    
       anno_commessa = :ll_anno_commessa  	AND    
		 num_commessa = :ll_num_commessa       AND    
		 prog_riga = :ll_prog_riga 				AND    
		 cod_prodotto = :ls_cod_prodotto_fase  and
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  			AND    
		 cod_lavorazione = :ls_cod_lavorazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca quantità in produzione ~r~n" + sqlca.sqlerrtext
	return -1
end if

ldd_quan_in_produzione = ldd_quan_in_produzione - ldd_quan_entrata
if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0

UPDATE avan_produzione_com
SET    quan_prodotta = quan_prodotta + :ldd_quan_entrata,
		 quan_in_produzione = :ldd_quan_in_produzione,
		 anno_reg_sl = :ll_anno_reg_sl,
		 num_reg_sl = :ll_num_reg_sl
WHERE  cod_azienda = :s_cs_xx.cod_azienda 	AND    
       anno_commessa = :ll_anno_commessa  	AND    
		 num_commessa = :ll_num_commessa  		AND    
		 prog_riga = :ll_prog_riga 				AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  			AND    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in aggiornamento dati avanzamento produzione~r~n" + sqlca.sqlerrtext
	return -1
end if


//***********
//INIZIO INSERIMENTO NUOVO ARRIVO IN TABELLA PROD_BOLLE_IN_COM
select max(prog_ingresso)
into   :ll_prog_ingresso
from   prod_bolle_in_com
where cod_azienda = :s_cs_xx.cod_azienda		and   
		anno_commessa = :ll_anno_commessa		and   
		num_commessa = :ll_num_commessa			and   
		prog_riga = :ll_prog_riga					and   
		cod_prodotto_fase = :ls_cod_prodotto_fase and   
		cod_versione = :ls_cod_versione_fase   and
		cod_reparto = :ls_cod_reparto				and   
		cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in calcolo progressivo bolle entrata (prod_bolle_in_com)~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_prog_ingresso) then
	ll_prog_ingresso = 1
else
	ll_prog_ingresso++
end if

INSERT INTO prod_bolle_in_com  
		( cod_azienda,   
		  anno_commessa,   
		  cod_prodotto_fase, 
		  cod_versione,
		  num_commessa,   
		  prog_riga,   
		  cod_reparto,   
		  cod_lavorazione,   
		  prog_ingresso,   
		  anno_acc_materiali,   
		  num_acc_materiali,   
		  anno_bolla_acq,   
		  num_bolla_acq,   
		  prog_riga_bolla_acq,   
		  cod_prodotto_in,   
		  cod_deposito,   
		  cod_ubicazione,   
		  cod_lotto,   
		  data_stock,   
		  prog_stock,   
		  quan_arrivata,   
		  cod_fornitore,   
		  data_ingresso )  
VALUES ( :s_cs_xx.cod_azienda,   
		  :ll_anno_commessa,   
		  :ls_cod_prodotto_fase,
		  :ls_cod_versione_fase,
		  :ll_num_commessa,   
		  :ll_prog_riga,   
		  :ls_cod_reparto,   
		  :ls_cod_lavorazione,   
		  :ll_prog_ingresso,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  :ls_cod_prodotto_fase,   
		  :ls_cod_deposito_sl[1],   
		  :ls_cod_ubicazione[1],   
		  :ls_cod_lotto[1],   
		  :ld_data_stock[1],   
		  :ll_prog_stock[1],   
		  :ldd_quan_entrata,   
		  :ls_cod_fornitore_ingresso,   
		  :ldt_oggi )  ;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in registrazione ingresso merce in prod_bolle_in_com~r~n" + sqlca.sqlerrtext
	return -1
end if

//FINE INSERIMENTO NUOVO ARRIVO


/*   Gestione automatica degli scarichi delle materie prime del fornitore   */

ll_risposta = wf_cerca_materie_prime(ll_anno_commessa, &
											 ll_num_commessa,ll_prog_riga, &
											 ls_cod_prodotto_fase, &
											 ls_cod_versione_fase, &
											 ldd_quan_entrata, &
											 1, &
											 ref ls_cod_materia_prima[], &
											 ref ld_quan_mp[], &
											 ref ls_errore)
if ll_risposta <> 0 then
	fs_messaggio = ls_errore
	return -1
end if


/*  Verifica della disponibilità di ogni materia prima nel magazzino del fornitore  */

select 	cod_deposito
into   	:ls_cod_deposito_fornitore[1]
from   	anag_fornitori
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_fornitore=:ls_cod_fornitore_ingresso;
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore in ricerca deposito del terzista~r~n" + sqlca.sqlerrtext
	return -1
end if

fs_messaggio = ""

for ll_i = 1 to upperbound(ls_cod_materia_prima)

	SELECT  	sum(giacenza_stock - quan_assegnata - quan_in_spedizione)
	into     :ldd_quan_disponibile
	FROM   	stock   
	where  	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto =:ls_cod_materia_prima[ll_i] and
				cod_deposito =:ls_cod_deposito_fornitore[1] and
				giacenza_stock - quan_assegnata - quan_in_spedizione > 0;
	
	if ldd_quan_disponibile < ld_quan_mp[ll_i] then
		if fs_messaggio = "" then fs_messaggio = "Materie Prime Indisponibili:"
		fs_messaggio = fs_messaggio + "~r~n" + ls_cod_materia_prima[ll_i] + " Manca q.tà " + string(ld_quan_mp[ll_i] - ldd_quan_disponibile, "#,##0.0000")
	end if
	
next

if len(fs_messaggio) > 0 then
	// alcune materie prime non sono disponibili: annullo tutto e segnalo
	return -1
end if

/*   procedo con creazione dei movimenti di scarico delle MP   */

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ld_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
ls_cod_fornitore[1] = ls_cod_fornitore_ingresso

ll_righe_scarico = upperbound(ls_cod_materia_prima)

for ll_t = 1 to ll_righe_scarico
	
	open cu_righe_stock;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in OPEN cu_righe_stock, elenco stock materie prime da scaricare~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	ld_quan_residua_scarico = ld_quan_mp[ll_t]
	
	do while true
		
		fetch cu_righe_stock into :ls_cod_ubicazione[1], :ls_cod_lotto[1], :ld_data_stock[1], :ll_prog_stock[1], :ldd_quan_disponibile;
	
		choose case sqlca.sqlcode  
			case 100
				exit
			case is < 0 
				fs_messaggio = "Errore in FETCH cu_righe_stock, elenco stock materie prime da scaricare~r~n" + sqlca.sqlerrtext
				return -1
		end choose
		
		if ld_quan_residua_scarico <= 0 then exit
	
		ls_cod_prodotto_scarico = ls_cod_materia_prima[ll_t]
		if ld_quan_residua_scarico <= ldd_quan_disponibile then
			ldd_quan_prelevata = ld_quan_residua_scarico
			ld_quan_residua_scarico = 0
		else
			ld_quan_prelevata = ldd_quan_disponibile
			ld_quan_residua_scarico = ld_quan_residua_scarico - ldd_quan_disponibile
		end if

		if ldd_quan_prelevata > 0 then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_scarico_terzista, ls_cod_prodotto_scarico, ls_cod_deposito_fornitore[], & 
										  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
										  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			
				fs_messaggio = "Si è verificato un errore preparazione movimenti."
				return  -1
			
			end if
			
			if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
												ls_cod_tipo_mov_scarico_terzista, ls_cod_prodotto_scarico) = -1 then
			
				fs_messaggio = "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino."
				return -1
			
			end if
			
			luo_mag = create uo_magazzino
			
			ll_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
													 ls_cod_tipo_mov_scarico_terzista, &
													 "N", &
													 ls_cod_prodotto_scarico, &
													 ldd_quan_prelevata, &
													 1, &
													 ll_num_commessa, &
													 datetime(today(),time('00:00:00')), &
													 "", &
													 ll_anno_reg_des_mov, &
													 ll_num_reg_des_mov, &
													 ls_cod_deposito_fornitore[], &
													 ls_cod_ubicazione[], &
													 ls_cod_lotto[], &
													 ld_data_stock[], &
													 ll_prog_stock[], &
													 ls_cod_fornitore[], &
													 ls_cod_cliente[], &
													 ll_anno_registrazione[], &
													 ll_num_registrazione[])
													 
			
			
			if ll_risposta=-1 then
				fs_messaggio = "Errore in creazione movimenti magazzino."
				return -1
			end if
				
			ll_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
			
			if ll_risposta = -1 then
				fs_messaggio = "Si è verificato un errore in fase di cancellazione destinazioni movimenti."
				return -1
			end if
		end if
		
		// stefanop: 15/12/2011: movimenti magazzino prod_raggruppato	
		if luo_mag.uof_movimenti_mag_ragguppato(datetime(today(),time('00:00:00')), &
					ls_cod_tipo_mov_scarico_terzista, &
					"N", &
					ls_cod_prodotto_scarico, &
					ldd_quan_prelevata, &
					1, &
					ll_num_commessa, &
					datetime(today(),time('00:00:00')), &
					"", &
					ll_anno_reg_des_mov, &
					ll_num_reg_des_mov, &
					ls_cod_deposito_fornitore[], &
					ls_cod_ubicazione[], &
					ls_cod_lotto[], &
					ld_data_stock[], &
					ll_prog_stock[], &
					ls_cod_fornitore[], &
					ls_cod_cliente[], &
					ll_anno_registrazione[], &
					ll_num_registrazione[],&
					ll_null,&
					ll_anno_registrazione[1],&
					ll_num_registrazione[1]) = -1 then
			destroy luo_mag
			return -1
		end if
		// -----
		
		destroy luo_mag
	loop
		
	close cu_righe_stock;
	
	ldd_quan_prelevata = ld_quan_mp[ll_t]

	//***************
	//INIZIO DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE
					
	select quan_impegnata
	into   :ldd_quan_impegnata
	from   impegni_deposito_fasi_com
	where  cod_azienda   = :s_cs_xx.cod_azienda and    
			 anno_commessa = :ll_anno_commessa and    
			 num_commessa  = :ll_num_commessa and    
			 prog_riga     = :ll_prog_riga and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase and
			 cod_versione    = :ls_cod_versione_fase and    
			 cod_reparto     = :ls_cod_reparto	and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito    = :ls_cod_deposito_fornitore[1] and    
			 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in ricerca impegno fornitore~r~n" + sqlca.sqlerrtext
		return -1
	end if
		
	if ldd_quan_impegnata > ldd_quan_prelevata then
		update impegni_deposito_fasi_com
		set    quan_impegnata = quan_impegnata - :ldd_quan_prelevata
		where  cod_azienda = :s_cs_xx.cod_azienda    and    
				 anno_commessa = :ll_anno_commessa		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga					and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and    
				 cod_versione = :ls_cod_versione_fase  and
				 cod_reparto = :ls_cod_reparto			and    
				 cod_lavorazione = :ls_cod_lavorazione	and    
				 cod_deposito = :ls_cod_deposito_fornitore[1] and    
				 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in fase di disimpegno dal deposito fornitore (impegni_deposito_fasi_com)~r~n" + sqlca.sqlerrtext
			return -1
		end if

	else
		delete impegni_deposito_fasi_com
		where  cod_azienda = :s_cs_xx.cod_azienda		and    
				 anno_commessa = :ll_anno_commessa		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga					and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_versione = :ls_cod_versione_fase  and    
				 cod_reparto = :ls_cod_reparto			and    
				 cod_lavorazione = :ls_cod_lavorazione and    
				 cod_deposito = :ls_cod_deposito_fornitore[1]  and    
				 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in cancellazione impegno dal deposito fornitore (impegni_deposito_fasi_com)~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
	end if
	
	//FINE DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE
	//***************
	
	//****************************************************************************************************************
	// DISIMPEGNO LE MP CONTROLLANDO CONTEMPORANEAMENTE ANCHE LA TABELLA IMPEGNO_MAT_PRIME_COMMESSA

	select quan_impegnata_attuale
	into   :ldd_quan_impegnata_attuale
	from   impegno_mat_prime_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:ll_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    cod_prodotto=:ls_cod_prodotto_scarico;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in disimpegno MP della commessa~r~n " + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then // ROUTINE PER LE COMMESSE GENERATE PRIMA DI AGGIUNGERE LA TAB IMPEGNO_MAT_PRIME COMMESSA
	
		select quan_impegnata
		into   :ldd_quan_impegnata_attuale
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca prodotto "+ls_cod_prodotto+"~r~n" + sqlca.sqlerrtext
			return -1
		end if
	
		if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
		else
			update anag_prodotti																		
			set 	 quan_impegnata = 0
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
		end if
		
	else // ROUTINE PER LE COMMESSE GENERATE DOPO L'AGGIUNTA DELLA TAB IMPEGNO_MAT_PRIME COMMESSA

		if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
			
			update impegno_mat_prime_commessa
			set 	 quan_impegnata_attuale= quan_impegnata_attuale - :ldd_quan_prelevata
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:ll_anno_commessa
			and    num_commessa=:ll_num_commessa
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in aggiornamento impegno commessa~r~n" + sqlca.sqlerrtext
				return -1
			end if
	
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
		else
			update impegno_mat_prime_commessa
			set 	 quan_impegnata_attuale = 0
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:ll_anno_commessa
			and    num_commessa=:ll_num_commessa
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in aggiornamento impegno commessa~r~n" + sqlca.sqlerrtext
				return -1
			end if
	
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata - :ldd_quan_impegnata_attuale
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext
				return -1
			end if
					
		
		end if

	end if
	//****************************************************************************************************************

next

//FINE SCARICO MAGAZZINO FORNITORE
//********************************


select quan_in_produzione
into   :ldd_quan_in_produzione		
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :ll_anno_commessa  AND    
		 num_commessa = :ll_num_commessa  	AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore ricerca quantità in produzione in avan_produzione_com~r~n" + sqlca.sqlerrtext
	return -1
end if

//*****************************
//INIZIO CHIUSURA FASE

if ldd_quan_in_produzione = 0 then

	update avan_produzione_com
	set    flag_fine_fase='S'
	where  cod_azienda = :s_cs_xx.cod_azienda   and 	 
			 anno_commessa = :ll_anno_commessa	  and    
			 num_commessa = :ll_num_commessa		  and    
			 prog_riga = :ll_prog_riga				  and    
			 cod_prodotto = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase and    
			 cod_reparto = :ls_cod_reparto		  and    
			 cod_lavorazione = :ls_cod_lavorazione;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in aggiornamento flag_fine_fase in avan_produzione_com~r~n" + sqlca.sqlerrtext
		return -1
	end if

	
	//**** Individua se viene chiusa l'ultima fase
	
	select count(*)
	into   :ll_num_fasi_aperte
	from   avan_produzione_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and 	 anno_commessa=:ll_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga
	and    flag_fine_fase='N';
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in conteggio fasi aperte~r~n" + sqlca.sqlerrtext
		return -1
	end if


	if ll_num_fasi_aperte = 0 then
		select quan_in_produzione
		into   :ldd_quan_in_produzione_prog
		from   det_anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:ll_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
	
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca quantità in produzione dalla commessa~r~n" + sqlca.sqlerrtext
			return -1
		end if

		
		select cod_prodotto,
				 flag_tipo_avanzamento
		into   :ls_cod_prodotto,
				 :ls_flag_tipo_avanzamento
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:ll_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca commessa " + sqlca.sqlerrtext
			return -1
		end if

		
		s_cs_xx.parametri.parametro_i_1 = ll_anno_commessa
		s_cs_xx.parametri.parametro_ul_1 = ll_num_commessa
		s_cs_xx.parametri.parametro_ul_2 = ll_prog_riga
		s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_5 = ls_cod_versione
	
		window_open(w_rileva_quantita,0)
		
		if s_cs_xx.parametri.parametro_b_1 = true then	
			fs_messaggio = "Operazione annullata"
			return -1
		end if
		
		update det_anag_commesse
		set    quan_in_produzione=0,
				 quan_prodotta=:s_cs_xx.parametri.parametro_d_1
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:ll_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
	
		if sqlca.sqlcode < 0 then
			fs_messaggio = "Errore in aggiornamento quantità della commessa~r~n" + sqlca.sqlerrtext
			return -1
		end if

	
		select quan_prodotta,
				 quan_in_produzione
		into   :ldd_quan_prodotta,
				 :ldd_quan_in_produzione
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:ll_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca dati commessa~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		ldd_quan_in_produzione = ldd_quan_in_produzione - s_cs_xx.parametri.parametro_d_1
		if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0
		ldd_quan_prodotta = ldd_quan_prodotta + s_cs_xx.parametri.parametro_d_1
	
		choose case ls_flag_tipo_avanzamento //seleziona il tipo avanzamento
			case '1' // Assegnazione
				ls_flag_tipo_avanzamento = '9' // Assegnazione Chiusa
			case '2' // Attivazione
				ls_flag_tipo_avanzamento = '8' // Attivazione Chiusa
		end choose 
	
		update anag_commesse
		set    quan_in_produzione=:ldd_quan_in_produzione,	
				 quan_prodotta=:ldd_quan_prodotta,
				 flag_tipo_avanzamento = :ls_flag_tipo_avanzamento
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:ll_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		if sqlca.sqlcode < 0 then
			fs_messaggio = "Errore in aggiornamento quantità della commessa~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		ll_risposta = f_mov_mag_chiusura_fasi (ll_anno_commessa,ll_num_commessa, ll_prog_riga,ls_errore )
	
		if ll_risposta = -1 then 
			fs_messaggio = "Errore in generazione movimento di chiusura commessa~r~n" + ls_errore + sqlca.sqlerrtext
			return -1
		end if
		
			
		return 0
	end if

end if

return 0
end function

public function integer wf_cerca_materie_prime (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_in_produzione, long fl_num_livello_cor, ref string fs_cod_materia_prima[], ref decimal fd_quan_mp[], ref string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, &
			 ls_cod_prodotto_inserito,ls_cod_lavorazione,ls_test, ls_flag_fine_fase, ls_cod_versione_figlio, ls_cod_versione_inserito, ls_cod_versione_variante, & 
			 ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante, ls_flag_ramo_descrittivo
long      ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_handle,ll_i,ll_num_righe_dw, ll_cont_mp
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_quan_utilizzo_variante,ldd_quan_utilizzo_1
boolean   lb_flag_trovata
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fd_quan_in_produzione
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ls_flag_ramo_descrittivo = lds_righe_distinta.getitemstring(ll_num_figli,"distinta_flag_ramo_descrittivo")	
	
	if ls_flag_ramo_descrittivo ="S" then continue

	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	select cod_prodotto,
			 cod_versione_variante,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_cod_versione_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda		and    
			 anno_commessa = :fl_anno_commessa		and    
			 num_commessa = :fl_num_commessa			and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione = :fs_cod_versione 		and
			 cod_versione_figlio = :ls_cod_versione_figlio;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante* fd_quan_in_produzione
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	//***************************
	//ricerca se già inserito
	lb_flag_trovata = false
	if upperbound(fs_cod_materia_prima[]) > 0 then
		for ll_i = 1 to upperbound(fs_cod_materia_prima[])
			if ls_cod_prodotto_inserito = fs_cod_materia_prima[ll_i] then 
				lb_flag_trovata=true
				ldd_quan_utilizzo_1 = fd_quan_mp[ll_i]
				ldd_quan_utilizzo = ldd_quan_utilizzo + ldd_quan_utilizzo_1 
				fd_quan_mp[ll_i] = ldd_quan_utilizzo
			end if
		next
	end if
	
	if lb_flag_trovata then continue
	//***************************

	if ls_flag_materia_prima = 'N' then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
				 cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		if sqlca.sqlcode = 100 then 
			
			ll_num_righe_dw = upperbound(fs_cod_materia_prima[])
			
			ll_num_righe_dw++
			
			fs_cod_materia_prima[ll_num_righe_dw] = ls_cod_prodotto_inserito
			fd_quan_mp[ll_num_righe_dw]           = ldd_quan_utilizzo
			
			continue
		end if
	else
		
		ll_num_righe_dw = upperbound(fs_cod_materia_prima[])

		ll_num_righe_dw++

		fs_cod_materia_prima[ll_num_righe_dw] = ls_cod_prodotto_inserito
		fd_quan_mp[ll_num_righe_dw]           = ldd_quan_utilizzo
		
		continue
	end if

	

	//***************************
	//Verifica se il prodotto è un SL di una fase già finita (flag_fine_fase = 'S'), in questo caso è come se fosse una MP
	select flag_fine_fase
	into   :ls_flag_fine_fase
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda		and    
			 anno_commessa = :fl_anno_commessa		and    
			 num_commessa = :fl_num_commessa			and    
			 prog_riga = :fl_prog_riga					and    
			 cod_prodotto = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if sqlca.sqlcode < 0 then
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_fine_fase = 'S' then 
		ll_num_righe_dw = upperbound(fs_cod_materia_prima[])

		ll_num_righe_dw++

		fs_cod_materia_prima[ll_num_righe_dw] = ls_cod_prodotto_inserito
		fd_quan_mp[ll_num_righe_dw]           = ldd_quan_utilizzo
		continue
	end if
	
	//***************************


	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=wf_cerca_materie_prime(fl_anno_commessa, &
		                                     fl_num_commessa,fl_prog_riga, &
														 ls_cod_prodotto_inserito, &
														 ls_cod_versione_inserito, &
														 ldd_quan_utilizzo, &
														 fl_num_livello_cor + 1, &
														 ref fs_cod_materia_prima[], &
														 ref fd_quan_mp[], &
														 ref ls_errore)

		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

public function integer wf_carica_tipo_movimento_grezzo (boolean ab_fat_acq);/**
 * Stefanop
 * 16/07/2010
 *
 * Recupera il codice per il movimento di magazzino
 **/
 

if ab_fat_acq then
	select cod_tipo_movimento_grezzo
	into :is_cod_tipo_movimento_grezzo
	from con_fat_acq
	where
		cod_azienda = :s_cs_xx.cod_azienda;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("APICE", "Errore durante il recupero del tipo movimento grezzo per le fatture di acquisto")
		return -1
	elseif sqlca.sqlcode = 100 or isnull(is_cod_tipo_movimento_grezzo) or is_cod_tipo_movimento_grezzo = "" then
		g_mb.show("APICE", "Attenzione: non è stato associato nessun tipo di movimento grezzo per le fatture di acquisto")
		return -1
	end if
else
	select cod_tipo_movimento_grezzo
	into :is_cod_tipo_movimento_grezzo
	from con_bol_acq
	where
		cod_azienda = :s_cs_xx.cod_azienda;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("APICE", "Errore durante il recupero del tipo movimento grezzo per le bolle di acquisto")
		return -1
	elseif sqlca.sqlcode = 100 or isnull(is_cod_tipo_movimento_grezzo) or is_cod_tipo_movimento_grezzo = "" then
		g_mb.show("APICE", "Attenzione: non è stato associato nessun tipo di movimento grezzo per le bolle di acquisto")
		return -1
	end if
end if

return 0
end function

public function integer wf_carica_deposito (string as_cod_fornitore);/**
 * Stefanop 
 * 16/07/2010
 *
 * Carica il codice deposito assegnato al fornitore
 **/
 
// stefanop: 24/02/2012: Il deposito ora viene prelavato dalla dw_depositi
string ls_cod_deposito

ls_cod_deposito = tab_1.det_2.dw_depositi.getitemstring(1, "cod_deposito")

if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
	g_mb.warning("Selezionare un deposito tra quelli disponibili per il fornitore!")
	return -1
else
	is_cod_deposito = ls_cod_deposito
	return 0
end if
// ----

//string ls_rag_soc
//
//select cod_deposito
//into :is_cod_deposito
//from anag_depositi
//where
//	cod_azienda = :s_cs_xx.cod_azienda and
//	cod_fornitore = :as_cod_fornitore;
//	
//if sqlca.sqlcode < 0 then
//	g_mb.error("APICE", "Errore durante il controllo del deposito assegnato al fornitore " + is_cod_fornitore + "~r~nDettaglio:" + sqlca.sqlerrtext)
//	return -1
//elseif sqlca.sqlcode = 100 or isnull(is_cod_deposito) or is_cod_deposito = "" then
//	select rag_soc_1
//	into :ls_rag_soc
//	from anag_fornitori
//	where
//		cod_azienda = :s_cs_xx.cod_azienda and
//		cod_fornitore = :as_cod_fornitore;
//		
//	g_mb.show("APICE", "Attenzione: il fornitore " + as_cod_fornitore + " - " + ls_rag_soc + " non è assegnato a nessun deposito")
//	return -1
//end if
//
//return 0
end function

public function integer wf_get_giacenza_deposito (string fs_cod_deposito, string fs_cod_prodotto, ref decimal fd_giacenza);uo_magazzino luo_magazzino

string ls_where, ls_error, ls_chiave[]
datetime ldt_data_a
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
integer li_return

ldt_data_a = datetime(today(), 23:59:59)

ls_where = "and cod_deposito=~~'"+fs_cod_deposito+"~~'"

luo_magazzino = CREATE uo_magazzino
		
li_return = luo_magazzino.uof_saldo_prod_date_decimal(fs_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", &
																			ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

destroy luo_magazzino

if li_return <0 then
	g_mb.messagebox("Errore Inventario Magazzino: prodotto " + fs_cod_prodotto, ls_error)
	return -1
end if

//prendo solo il primo (ce ne sarà uno solo perchè gli ho passato il flitro per deposito fornitore)
if upperbound(ld_giacenza_stock[]) > 0 then
	fd_giacenza = ld_giacenza_stock[1]
end if

if isnull(fd_giacenza) then fd_giacenza = 0

return 1
end function

public function integer wf_aggiorna_mov_grezzo_referenza (long fl_anno_mov, long fl_numero_mov, datetime fdt_data_doc, string fs_num_documento, string fs_referenza);string ls_referenza
long ll_num_documento


//memorizzo nel movimento anno, numero documento
ll_num_documento = long(fs_num_documento)

//limito a 20 chars perchè referenza è varchar(20) in mov_magazzino mentre rag_soc_1 è varchar(40)
ls_referenza = left(fs_referenza, 20)

update mov_magazzino
	set 	data_documento=:fdt_data_doc,
			num_documento=:ll_num_documento,
			referenza = :ls_referenza
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_mov and
			num_registrazione=:fl_numero_mov;
				

return 1
end function

public function window get_window ();return this
end function

public function integer wf_ricalcola_giacenze ();/**
**/

string ls_cod_deposito, ls_cod_prodotto_alt
int li_row
long ll_i
decimal ld_giacenza_stock, ld_giacenza_usata, ld_quan_acquisto

ls_cod_deposito = tab_1.det_2.dw_depositi.getitemstring(1, "cod_deposito")

for ll_i = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
	
	ld_giacenza_usata = 0
	ld_quan_acquisto = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "quan_acquisto")
	ls_cod_prodotto_alt = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(ll_i, "cod_prodotto_alt")
	
	if isnull(ls_cod_prodotto_alt) or ls_cod_prodotto_alt = "" then
		// CASO 3 della specifica non è assegnato il codice alternativo
		continue
	end if
	
	if wf_get_giacenza_deposito(ls_cod_deposito, ls_cod_prodotto_alt, ld_giacenza_stock) < 0 then
		g_mb.error("APICE", "Errore durante il controllo della giacenza.~r~nProdotto: " + ls_cod_prodotto_alt + "~r~nDeposito: " + ls_cod_deposito + "~r~nDettaglio: " + sqlca.sqlerrtext)
		return -1
	end if
	
	// calcolo lo scarico per altre righe dello stesso codice
	for li_row = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
		if li_row <> ll_i and tab_1.det_2.dw_ext_carico_acq_det.getitemstring(li_row, "cod_prodotto_alt") = ls_cod_prodotto_alt then
			ld_giacenza_usata += tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(li_row, "quan_acquisto_alt")
		end if
	next
	// ----
		
//	if ab_read_quan_alt then
//		ld_quan_acquisto = dw_ext_carico_acq_det.getitemnumber(ai_row, "quan_acquisto_alt")
//	end if
	
	if ld_quan_acquisto <= (ld_giacenza_stock - ld_giacenza_usata) then
		// CASO 1, codice alt presente e giacenza ok
		tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_i, "flag_tipo_riga", "1")
		tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_i, "colore_riga", il_color_green)
		tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_i, "quan_acquisto_alt", ld_quan_acquisto)
	else
		// CASO 2, codice alt presente ma giacenza non sufficiente
		tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_i, "flag_tipo_riga", "2")
		tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_i, "colore_riga", il_color_red)
		tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_i, "quan_acquisto_alt", ld_giacenza_stock - ld_giacenza_usata)
	end if
	
next


return 0
end function

public function integer wf_genera_bolla_trasferimento (long al_anno, long al_numero, string as_table);/**
 * stefanop
 * 28/02/2012
 *
 * Genero il DDT di trasferimento
 **/

string ls_cod_deposito_trasf, ls_cod_deposito, ls_errore, ls_cod_cliente_bol_trasf
long ll_anno_ord, ll_num_ord, ll_anno_bolla, ll_num_bolla, ll_i, ll_ret
s_dati_bolla_trasferimento lstr_data
uo_generazione_documenti luo_generazione_documenti
s_det_riga lstr_det_righe[]



//inutile procedere al ddt di tarsferimento se non viene impostato il cliente in parametri vendita
//recupero cliente per fare la bolla dai parametri vendite
select cod_cliente_bol_trasf
into :ls_cod_cliente_bol_trasf
from con_vendite
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_cod_cliente_bol_trasf) or len(ls_cod_cliente_bol_trasf) < 1 then
	//Non è impostato nessun cliente per la generazione di bolle di traferimento"
	return 0
end if


ll_anno_ord = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(1, "anno_registrazione")
ll_num_ord = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(1, "num_registrazione")

select cod_deposito, cod_deposito_trasf
into :ls_cod_deposito_trasf, :ls_cod_deposito
from tes_ord_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_ord and
		 num_registrazione = :ll_num_ord;

// Se è impostato il deposito di trasferimento o se il deposito selezionato nella finestra prima della bolla è diverso da quello dell'ordine allora
// devo creare una bolla di trasferimento.
if (not isnull(ls_cod_deposito_trasf) and ls_cod_deposito_trasf <> ls_cod_deposito) or id_cod_depostio_documento <> ls_cod_deposito  then
		
	if isnull(ls_cod_deposito_trasf) or ls_cod_deposito_trasf = "" then ls_cod_deposito_trasf = id_cod_depostio_documento
		
	s_cs_xx.parametri.parametro_s_1 = ls_cod_deposito_trasf
	s_cs_xx.parametri.parametro_i_1 = 1
 
	window_open(w_conferma_bolla_trasferimento, 0)
	
	lstr_data = message.powerobjectparm
	
	if isnull(lstr_data) or not isvalid(lstr_data) then return 0
	
	luo_generazione_documenti = CREATE uo_generazione_documenti
	
	luo_generazione_documenti.is_cod_tipo_bol_ven = lstr_data.cod_tipo_bol_ven
	luo_generazione_documenti.is_cod_causale = lstr_data.cod_causale
	luo_generazione_documenti.is_aspetto_beni =lstr_data.aspetto_beni
	luo_generazione_documenti.is_cod_mezzo =lstr_data.cod_mezzo
	luo_generazione_documenti.is_cod_porto =lstr_data.cod_porto
	luo_generazione_documenti.is_cod_resa =lstr_data.cod_resa
	luo_generazione_documenti.is_cod_vettore =lstr_data.cod_vettore
	luo_generazione_documenti.is_cod_inoltro =lstr_data.cod_inoltro
	luo_generazione_documenti.is_cod_imballo =lstr_data.cod_imballo
	luo_generazione_documenti.is_destinazione =lstr_data.cod_destinazione
	luo_generazione_documenti.id_num_colli = lstr_data.num_colli
	luo_generazione_documenti.id_peso_netto = lstr_data.peso_netto
	luo_generazione_documenti.id_peso_lordo = lstr_data.peso_lordo
	luo_generazione_documenti.idt_data_inizio_trasporto = lstr_data.data_inizio_trasp
	luo_generazione_documenti.it_ora_inizio_trasporto = lstr_data.ora_inizio_trasp
	luo_generazione_documenti.is_rag_soc_1 = lstr_data.rag_soc_1
	luo_generazione_documenti.is_rag_soc_2 = lstr_data.rag_soc_2
	luo_generazione_documenti.is_indirizzo = lstr_data.indirizzo
	luo_generazione_documenti.is_localita = lstr_data.localita
	luo_generazione_documenti.is_frazione = lstr_data.frazione
	luo_generazione_documenti.is_provincia = lstr_data.provincia
	luo_generazione_documenti.is_cap = lstr_data.cap
	luo_generazione_documenti.is_nota_piede = lstr_data.nota_piede
	luo_generazione_documenti.is_num_ord_cliente =lstr_data.num_ord_cliente
	luo_generazione_documenti.idt_data_ord_cliente =lstr_data.data_ord_cliente
		
	ll_anno_bolla = 0
	ll_num_bolla = 0
	
	if len(luo_generazione_documenti.is_cod_causale) < 1 then setnull(luo_generazione_documenti.is_cod_causale)
	if len(luo_generazione_documenti.is_cod_mezzo) < 1 then setnull(luo_generazione_documenti.is_cod_mezzo)
	if len(luo_generazione_documenti.is_cod_porto) < 1 then setnull(luo_generazione_documenti.is_cod_porto)
	if len(luo_generazione_documenti.is_cod_resa) < 1 then setnull(luo_generazione_documenti.is_cod_resa)
	if len(luo_generazione_documenti.is_cod_vettore) < 1 then setnull(luo_generazione_documenti.is_cod_vettore)
	if len(luo_generazione_documenti.is_cod_inoltro) < 1 then setnull(luo_generazione_documenti.is_cod_inoltro)
	if len(luo_generazione_documenti.is_cod_imballo) < 1 then setnull(luo_generazione_documenti.is_cod_imballo)
	if len(luo_generazione_documenti.is_destinazione) < 1 then setnull(luo_generazione_documenti.is_destinazione)
			
	ll_ret = luo_generazione_documenti.uof_crea_bolla_trasferimento(ls_cod_deposito, ls_cod_deposito_trasf, as_table, al_anno, al_numero, ref ll_anno_bolla,ref ll_num_bolla, ref ls_errore)
	
	if ll_ret < 0 then
		rollback;
		g_mb.error("Errore durante la generazione della bolla di trasferimento.~r~n" + ls_errore)
	else 
		commit;
		g_mb.success("Generata bolla di trasferimento " + string(ll_anno_bolla) + "/" + string(ll_num_bolla))
	end if
else
	return 0
end if
 
return 1
end function

public function string wf_get_deposito_fornitore (string as_cod_fornitore);string ls_sql, ls_return
int li_rows
datastore lds_store

setnull(ls_return)

ls_sql =	"select cod_deposito from anag_depositi "+&
			"where cod_azienda='" + s_cs_xx.cod_azienda + "' and " +&
					 "cod_fornitore='" + as_cod_fornitore + "' and "+&
		 			"((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + "))"

li_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql)

if li_rows < 0 then
	g_mb.error("APICE", "Errore durante il controllo del deposito.~r~n" + sqlca.sqlerrtext)
elseif li_rows = 0 then
	g_mb.show("APICE", "Nessun deposito trovato per il fornitore " + as_cod_fornitore + ".")
else
	//prendo sempre il primo
	ls_return = lds_store.getitemstring(1, 1)
end if

destroy lds_store
return ls_return
end function

public function integer wf_seleziona_file_ddt (string fs_cod_fornitore, ref string fs_ritorno);//05/03/2012 Donato
//Esegue la selezione del ddt inviato dal fornitore


string ls_estensione, ls_docname[], ls_docpath, ls_path
integer	li_count, li_ret, li_tracciato

if isnull(fs_cod_fornitore) or fs_cod_fornitore="" then
	fs_ritorno = "Selezionare il fornitore!"
	return -1
end if

//-----------------------------------------------------------------------------------------------------------
li_tracciato = guo_functions.uof_tipo_tracciato_ord_acq(fs_cod_fornitore, fs_ritorno)
if li_tracciato<0 then
	//in fs_ritorno il messaggio
	return -1
end if

choose case li_tracciato
	case 1	//VIV/INDINVEST
		fs_ritorno = "Tipo tracciato "+string(li_tracciato)+" con specifiche in attesa di invio da parte del fornitore ..."
		return -1
		ls_estensione = "xls"
		
	case 2	//ANODALL/EXTRUSION
//		fs_ritorno = "Tipo tracciato "+string(li_tracciato)+" con specifiche in attesa di invio da parte del fornitore ..."
//		return -1
		ls_estensione = "txt"
		
	case 3	//BUSTREO/PETTENUZZO
		ls_estensione = "csv"
		
	case else
		fs_ritorno = "Fornitore non tra quelli previsti!"
		return -1
		
end choose
//-----------------------------------------------------------------------------------------------------------

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File "+ls_estensione+" da importare", fs_ritorno, ls_docname[], ls_estensione,&
								+ "Files Excel (*."+ls_estensione+"),*."+ls_estensione+"", 	ls_path)

if li_ret < 1 then return 0
li_count = Upperbound(ls_docname)

return li_count

end function

public subroutine wf_check_before_import (string fs_cod_fornitore, string fs_path_file);//05/03/2012 Donato
//Esegue alcuni controlli prima di inziare a leggere le righe del dddt inviato dal fornitore

string ls_esito, ls_errore
integer li_tracciato

if fs_cod_fornitore<>"" and not isnull(fs_cod_fornitore) then
	//il fornitore c'è
	if fs_path_file="" or isnull(fs_path_file) then
		//non hai specificato il file
		ls_esito= "N"
		tab_1.det_1.dw_ext_ddt_file.setitem(1, "esito", ls_esito)
		return
	end if
	
	//sia il fornitore che il path file ci sono, verifica il tipo tracciato
	li_tracciato = guo_functions.uof_tipo_tracciato_ord_acq(fs_cod_fornitore, ls_errore)
	if li_tracciato>0 and li_tracciato<=3 then
		ls_esito= "S"
	else
		ls_esito= "E"
	end if
else
	//il fornitore non c'è torna subito N
	ls_esito= "N"
end if

tab_1.det_1.dw_ext_ddt_file.setitem(1, "esito", ls_esito)

return
end subroutine

public function integer wf_prepara_tracciato_file_ddt (integer fi_tipo_tracciato, ref long fl_fields[], ref string fs_errore);//05/03/2012 Donato
//preparazione dinamica del tracciato in base al fornitore

choose case fi_tipo_tracciato
		
	//****************************************************************************************
	case 1
		//TRACCIATO DDT DI VIV-INDIVEST DA IMPORTARE --------------------------
		fs_errore = string(fi_tipo_tracciato)+ ": Tipo tracciato con specifiche in attesa di invio da parte del fornitore ..."
		return -1
	
	//****************************************************************************************
	case 2
		//TRACCIATO DDT DI ANODALL/Extrusion DA IMPORTARE --------------------------
//		fs_errore = string(fi_tipo_tracciato)+ ": Tipo tracciato con specifiche in attesa di invio da parte del fornitore ..."
//		return -1
		
		/*
		------------------------------------------------------------------------------------------------
			NOME CAMPO					TIPO		L		ESEMPIO
		------------------------------------------------------------------------------------------------
		1	Codice_Cli_For					vch		6		'001599'
		2	Tipo_Documento				vch		1		'B'									('O' se ordine)
		3	Data_Documento				vch		10		'21/02/2012'
		4	Numero_Documento			num		15		'000000000000162'        		(0 decimali)
		5	Riga_Documento				num		6		'000010'							(0 decimali)
		6	Tipo_Riga						num		1		'0'									(0 articolo, 2 note)
		7	Codice_Articolo					vch		25		'0AE3081                   '
		8	Descrizione_Articolo			vch		100	'PR FRONTALE MED85-120 CM 405 BI                                                                     '
		9	Var1_Finitura_Articolo		vch		10		'910       '
		10	Var2_Misura_Articolo			num		5		'00405'							(0 decimali)
		11	Var3_Stato_Lega_Articolo	vch		2		'T6'
		12	Var4_Variante4					vch		3		'   '								(sempre blanks, vuota)
		13	Quantita_1						num		14		'00000000030000'				->30.0000 (3 decimali): il punto sep.dec. è soppresso
		14	UM_QTA1						vch		3		'BA '								in genere barre
		15	Quantita_2						num		14		'00000000216000'				->216.000 (3 decimali): il punto sep.dec. è soppresso
		16	UM_QTA2						vch		3		'KG '								in genere chilogrammi
		17	Riferimento_Ordine			vch		25		'162-10                   '			dovrebbe essere il riferimento riga ordine n°ord-n°riga
		18	Data_Consegna				vch		10		'13/01/2012'
		19	Numero_Pacco					vch		25		'                         '
		20	Destinazione_Merce			vch		14		'100           '
		21	RagSoc_Destinazione			vch		40		'SACCOLONGO                              '
		22	Indirizzo_Destinazione		vch		40		'VIA EINAUDI, 35                         '
		23	CAP_Destinazione				vch		8		'35030   '
		24	Localita_Destinazione			vch		40		'SACCOLONGO                              '
		25 Provincia_Destinazione		vch		2		'PD'
		26 Note_Riga						vch		70		'Nota di riga .....'
		------------------------------------------------------------------------------------------------
		*/
		fl_fields[1] = 6
		fl_fields[2] = 1
		fl_fields[3] = 10
		fl_fields[4] = 15
		fl_fields[5] = 6
		fl_fields[6] = 1
		fl_fields[7] = 25
		fl_fields[8] = 100
		fl_fields[9] = 10
		fl_fields[10] = 5
		fl_fields[11] = 2
		fl_fields[12] = 3
		fl_fields[13] = 14
		fl_fields[14] = 3
		fl_fields[15] = 14
		fl_fields[16] = 3
		fl_fields[17] = 25
		fl_fields[18] = 10
		fl_fields[19] = 25
		fl_fields[20] = 14
		fl_fields[21] = 40
		fl_fields[22] = 40
		fl_fields[23] = 8
		fl_fields[24] = 40
		fl_fields[25] = 2
		fl_fields[26] = 70
	
	//****************************************************************************************
	case 3
		//TRACCIATO DDT DI BUSTREO-PETTENUZZO DA IMPORTARE --------------------------
		
		//sospeso per ora ...
		fs_errore = string(fi_tipo_tracciato)+ ": Tipo tracciato con specifiche in attesa di invio da parte del fornitore ..."
		return -1
		
		/*
		------------------------------------------------------------------------------------------------
			NOME CAMPO			TIPO		L		ESEMPIO
		------------------------------------------------------------------------------------------------
		1	NsRif						vch		14		'20120002580001'
		2	CArt						vch		15		'H30400         '
		3	Descriz					vch		40		'SEMI TESTATA DX-SX ALU MED85            '
		4	Qta						flt			8		'000120.0'        		(1 decimale)
		5	PrezzoUnitNetto		flt			10		'0000001.86'			(2 decimali)
		6	Um						vch		3		'CP '
		7	DataDdt					dtm		8		'27012012'
		8	NrDdt						vch		10		'000045    '				(N.B. fare trim prima di acquisire, se serve il valore ...)
		9	FlagChiusaCons		vch		1		'1'							(è un flag 0/1: evas.completa della riga ordine cui il ddt si riferisce)
		------------------------------------------------------------------------------------------------
		*/
		fl_fields[1] = 14
		fl_fields[2] = 15
		fl_fields[3] = 40
		fl_fields[4] = 8
		fl_fields[5] = 10
		fl_fields[6] = 3
		fl_fields[7] = 8
		fl_fields[8] = 10
		fl_fields[9] = 1
	
	case else
		fs_errore = string(fi_tipo_tracciato)+ ": Tipo tracciato non previsto (1, 2, 3)"
		return -1
	
end choose

return 1
end function

public function integer wf_carica_ds_da_file_ddt (string fs_values[], integer fi_tipo_tracciato, ref datastore fds_data, ref string fs_errore);//05/03/2012 Donato
//ricopia la riga, letta dal file ddt del fornitore, nel datastore


long				ll_new, ll_num_reg_acq, ll_prog_riga_acq, ll_misura, ll_ordine_esistente
integer			li_anno_reg_acq
string				ls_appo, ls_cod_prodotto, ls_cod_misura_mag, ls_des_prodotto, ls_dec_sep, ls_dec_sep_2, ls_cod_fornitore, &
					ls_riga, ls_temp[], ls_um, ls_um_ddt, ls_fornitore_sel, ls_cod_misura_acq, ls_cod_misura
dec{3}			ld_quan_ordinata, ld_quan_arrivata, ld_prezzo_acquisto, ld_quan_ddt, ld_quan2_ddt, ld_qta_residua
dec{5}			ld_fat_conversione_acq, ld_fat_conversione_pezzi

long				ll_riga_presente


/*
------------------------- fds_data ---------------------------------------------
anno_registrazione							dec(0)
num_registrazione								dec(0)
prog_riga_ordine_acq							dec(0)
cod_prodotto									vch(15)
articolo_file										vch(30)
des_prodotto									vch(40)
quan_ordinata									dec(4)
quan_arrivata									dec(4)
cod_misura_mag								vch(3)
quan_ordinata as quan_ddt					dec(4)
um_ddt											vch(30)
prezzo_acquisto								dec(4)
prezzo_acquisto as prezzo_ddt				dec(4)
rif_su_ddt										vch(100)
fat_conversione								dec(5)
flag_saldo										vch(1)
cod_misura_acq								vch(3)
quan_ordinata as quan_ddt_2				dec(4)
um_ddt_2										vch(30)
q_residua_pezzi								dec(4)
flag_residuo										vch(1):  se S allora è riuscito a calcolare se evadere l'ordine oppure no con i pezzi arrivati
-----------------------------------------------------------------------------------
*/

ls_dec_sep = ","
ls_dec_sep_2 = "."

ls_fornitore_sel = tab_1.det_1.dw_ext_ddt_file.getitemstring(1, "cod_fornitore")

if fi_tipo_tracciato = 2 then
	//solo nel caso ANODALL e Extrusion fare questo controllo
	ls_appo = fs_values[6]		//Tipo_Riga						deve essere 0, altrimenti è una riga di note
	if ls_appo<>"0" then return 0
end if


ll_new = fds_data.insertrow(0)
ls_riga = "Riga file: " + string(ll_new)

choose case fi_tipo_tracciato
	case 1	//VIV/INDIVEST
		fs_errore  = "Tracciato d.d.t. VIV/INDIVEST non ancora comunicato dal fornitore"
		return -1
			
	case 2	//ANODALL/EXTRUSION
//		fs_errore  = "Tracciato d.d.t. ANODALL/EXTRUSION non ancora comunicato dal fornitore"
//		return -1
		
		ls_appo = fs_values[2]		//Tipo_Documento				deve essere B
		if ls_appo <> "B" then
			fs_errore  = ls_riga + " Tracciato d.d.t. ANODALL/Extrusion: tipo riga diverso da B; impossibile continuare!"
			return -1
		end if
		
		//-------------------------------------
		ls_appo = fs_values[17]		//Riferimento_ordine			contiene il anno-numero-riga ordine acquisto di riferimento
		g_str.explode(ls_appo, "-", ls_temp[])
		
		if upperbound(ls_temp[]) <> 3 then
			g_mb.warning( "Attenzione!", "Riferimento alla riga ordine di acquisto mancante alla riga "+ls_riga+ "! Questa riga sarà esclusa!")
			fds_data.deleterow(ll_new)
			
			return 0
		end if
		
		li_anno_reg_acq = integer(ls_temp[1])				 //anno_registrazione			dec(0)
		ll_num_reg_acq = integer(ls_temp[2])				//num_registrazione			dec(0)
		ll_prog_riga_acq = integer(ls_temp[3])				//prog_riga_ordine_acq		dec(0)
		
		if li_anno_reg_acq>0 and ll_num_reg_acq>0 and ll_prog_riga_acq>0 then
			
			setnull(ll_ordine_esistente)
			
			select count(*)
			into :ll_ordine_esistente
			from det_ord_acq
			where cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_reg_acq and
					num_registrazione=:ll_num_reg_acq and
					prog_riga_ordine_acq=:ll_prog_riga_acq and
					quan_arrivata < quan_ordinata and flag_saldo<>'S';
			
			if ll_ordine_esistente>0 then
			else
				g_mb.warning( "Attenzione!", "La riga riga ordine di acquisto "+string(li_anno_reg_acq)+"/"+string(ll_num_reg_acq)+"/"+string(ll_prog_riga_acq)+&
														" è inesistente oppure risulta già evasa (q.arrivata coincide con q.ordinata) alla riga "+ls_riga+ "! Questa riga sarà esclusa!")
				fds_data.deleterow(ll_new)
				
				return 0
			end if
			
		else
			g_mb.warning( "Attenzione!", "Valori di riferimento alla riga ordine di acquisto non validi alla riga "+ls_riga+ "! Questa riga sarà esclusa!")
			fds_data.deleterow(ll_new)
			
			return 0
		end if
		
		
		ls_cod_fornitore = f_des_tabella("tes_ord_acq","anno_registrazione="+string(li_anno_reg_acq)+" and num_registrazione="+string(ll_num_reg_acq),"cod_fornitore")
		if ls_cod_fornitore<>ls_fornitore_sel then
			fs_errore  = ls_riga + ": Il fornitore selezionato nel filtro è diverso dal fornitore della riga del ddt nel file!"
			return -1
		end if
		
		
		//recupero info dalla riga ordine di acquisto ----------------
		//nella riga ordine la quantità viene memorizzata in UMmag
		if wf_get_info_riga_ordine(	li_anno_reg_acq, ll_num_reg_acq, ll_prog_riga_acq, &
											ls_cod_prodotto, ls_des_prodotto, ld_quan_ordinata, ld_quan_arrivata, ls_cod_misura, ld_prezzo_acquisto, &
											ld_fat_conversione_acq, fs_errore) < 0 then
			//in fs_errore il messaggio
			fs_errore = ls_riga + " " + fs_errore
			return -1
		end if
		
		//se la riga ordine è già presente nel datastore, avvisa e somma le quantità
		ll_riga_presente = 0
		ll_riga_presente = fds_data.find(	"anno_registrazione="+string(li_anno_reg_acq)+&
													" and num_registrazione="+string(ll_num_reg_acq)+&
													" and prog_riga_ordine_acq="+string(ll_prog_riga_acq), &
													1, &
													fds_data.rowcount())						
		
		if ll_riga_presente>0 then
			//#####################################################################################################
			//riga già presente: somma le quantità, avvisando prima l'utente
			g_mb.warning("Attenzione! Nel file del ddt la riga ordine "+string(li_anno_reg_acq)+"/"+string(ll_num_reg_acq)+"/"+string(ll_prog_riga_acq)+" è riportata più volte, "+&
									"quindi verrà accorpata in un'unica riga, sommando le quantità!")
			
			
			//devo aggiornare le seguenti colonne del datastore
			//quan_ddt				quan_ddt_2					fat_conversione
			
			
			//quan_ddt -----------------------------------------------------------------------------------------------------------
			ls_appo = fs_values[13]		//Quantita_1											dec(3)
												//caso ANODALL/EXTRUSION è sempre in PZ o BARRE
			//introduco il separatore decimale (3 cifre)  
			ls_appo = left(ls_appo, len(ls_appo) - 3) + "," + right(ls_appo, 3)
			guo_functions.uof_replace_string(ls_appo, ls_dec_sep_2, ls_dec_sep)
			ld_quan_ddt = dec(ls_appo)
			
			//sommo e aggiorno nel datastore
			ld_quan_ddt = ld_quan_ddt + fds_data.getitemnumber(ll_riga_presente, "quan_ddt")
			fds_data.setitem(ll_riga_presente, "quan_ddt", ld_quan_ddt)
			
			
			
			//quan_ddt_2 -------------------------------------------------------------------------------------------------------
			ls_appo = fs_values[15]		//Quantita_2
												//caso ANODALL/EXTRUSION è in genere in KG
			//introduco il separatore decimale (3 cifre)
			ls_appo = left(ls_appo, len(ls_appo) - 3) + "," + right(ls_appo, 3)
			guo_functions.uof_replace_string(ls_appo, ls_dec_sep_2, ls_dec_sep)
			ld_quan2_ddt = dec(ls_appo)
			
			//sommo e aggiorno nel datastore
			ld_quan2_ddt = ld_quan2_ddt + fds_data.getitemnumber(ll_riga_presente, "quan_ddt_2")
			fds_data.setitem(ll_riga_presente, "quan_ddt_2", ld_quan2_ddt)
		
			
			
			//fat_conversione --------------------------------------------------------------------------------------------------
			ls_cod_misura_mag = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")
			
			
			ls_appo = fs_values[10]			//Var2_Misura_Articolo espresso in millimetri
			if not isnumber(ls_appo) then
				fs_errore= ls_riga + " Var2_Misura_Articolo non numerico nel file ddt!"
				return -1
			end if
			ll_misura = long(ls_appo)		//in ll_misura la misura dell'articolo fornitore in mm, ovvero la lunghezza dell'articolo [MM/PZ]
			ll_misura = ll_misura / 1000	//converto in ML, metri lineari
			
			if wf_fat_conversione(ls_cod_misura_mag, ld_quan_ddt, ld_quan2_ddt, ll_misura, ld_fat_conversione_acq) < 0 then
				//calcolare a mano il fattore di conversione nuovo
				//di default proporre quello scritto nella relativa riga ordine acquisto
			end if
			
			fds_data.setitem(ll_riga_presente, "fat_conversione", ld_fat_conversione_acq)
			
			
			//infine rimuovo la riga inserita, in quanto è stata accorpata
			fds_data.deleterow(ll_new)
			
		else
			//#####################################################################################################
			//riga NON presente, quindi continua
			
			fds_data.setitem(ll_new, "anno_registrazione", li_anno_reg_acq)
			fds_data.setitem(ll_new, "num_registrazione", ll_num_reg_acq)
			fds_data.setitem(ll_new, "prog_riga_ordine_acq", ll_prog_riga_acq)
			
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
				fds_data.setitem(ll_new, "cod_prodotto", ls_cod_prodotto)				//cod_prodotto									vch(15)
				fds_data.setitem(ll_new, "des_prodotto", ls_des_prodotto)				//des_prodotto									vch(40)
			end if
			
			ls_appo = fs_values[7]		//Codice_Articolo
			fds_data.setitem(ll_new, "articolo_file", ls_appo)								//articolo_file									vch(30)
		
			//informazioni diretta dalla riga ordine 
			fds_data.setitem(ll_new, "quan_ordinata", ld_quan_ordinata)				//quan_ordinata								dec(4) in UMmag
			fds_data.setitem(ll_new, "quan_arrivata", ld_quan_arrivata)				//quan_arrivata								dec(4) in UMmag
			
			//fds_data.setitem(ll_new, "cod_misura_mag", ls_cod_misura_mag)		//cod_misura_mag									vch(3)
			ls_cod_misura_mag = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")
			fds_data.setitem(ll_new, "cod_misura_mag", ls_cod_misura_mag)
			
			//qui ci metto al UM memorizzata in cod_misura della riga ordine acquisto
			fds_data.setitem(ll_new, "cod_misura_acq", ls_cod_misura)			//cod_misura_acq (la prendo da quella memorizzata nella riga ordine)		vch(3)
	
	
			ls_appo = fs_values[13]		//Quantita_1											dec(3)
												//caso ANODALL/EXTRUSION è sempre in PZ o BARRE
			//introduco il separatore decimale (3 cifre)  
			ls_appo = left(ls_appo, len(ls_appo) - 3) + "," + right(ls_appo, 3)
			guo_functions.uof_replace_string(ls_appo, ls_dec_sep_2, ls_dec_sep)
			ld_quan_ddt = dec(ls_appo)
			fds_data.setitem(ll_new, "quan_ddt", ld_quan_ddt)
			
			
			ls_appo = fs_values[14]		//um_ddt	 sarebbe UM_QTA1						vch(30)
			fds_data.setitem(ll_new, "um_ddt", ls_appo)
												//UM ddt, caso ANODALL/EXTRUSION è sempre PZ o BA
			
			
			ls_appo = fs_values[15]		//Quantita_2
												//caso ANODALL/EXTRUSION è in genere in KG
			//introduco il separatore decimale (3 cifre)
			ls_appo = left(ls_appo, len(ls_appo) - 3) + "," + right(ls_appo, 3)
			guo_functions.uof_replace_string(ls_appo, ls_dec_sep_2, ls_dec_sep)
			ld_quan2_ddt = dec(ls_appo)
			fds_data.setitem(ll_new, "quan_ddt_2", ld_quan2_ddt)
			
			ls_appo = fs_values[16]		//UM ddt 2, caso ANODALL/EXTRUSION è in genere KG
			fds_data.setitem(ll_new, "um_ddt_2", ls_appo)
			
			//calcolo la quantità residua in pezzi ---------------------------------------------------------------------------
			ld_qta_residua = ld_quan_ordinata - ld_quan_arrivata    //in UMmag
			ld_fat_conversione_pezzi = f_dec_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "fat_conversione_pezzi")
			
			if ld_fat_conversione_pezzi>0 and ld_fat_conversione_acq>0 then
				ld_qta_residua = (ld_qta_residua * ld_fat_conversione_acq) / ld_fat_conversione_pezzi
				fds_data.setitem(ll_new, "q_residua_pezzi", ld_qta_residua)
				
			elseif ls_cod_misura_mag="PZ" then
				//in ld_qta_residua c'è già il residuo in pezzi o barre, non occorrono altre operazioni
				fds_data.setitem(ll_new, "q_residua_pezzi", ld_qta_residua)
				
			elseif ls_cod_misura_mag="ML" then
				ls_appo = fs_values[10]			//Var2_Misura_Articolo espresso in millimetri
				if not isnumber(ls_appo) then
					fs_errore= ls_riga + " Var2_Misura_Articolo non numerico nel file ddt!"
					return -1
				end if
				ll_misura = long(ls_appo)		//in ll_misura la misura dell'articolo fornitore in mm, ovvero la lunghezza dell'articolo [MM/PZ]
				ll_misura = ll_misura / 1000	//converto in ML, metri lineari
				if ll_misura>0 then
					ld_qta_residua = ld_qta_residua / ll_misura
				else
					fs_errore= ls_riga + " Var2_Misura_Articolo NULLO nel file ddt!"
					return -1
				end if
				fds_data.setitem(ll_new, "q_residua_pezzi", ld_qta_residua)
				
			else
				//impossibile calcolare il residuo in pezzi, quibndi la cella non apparira colorata rosso-giallo-verde (stato evasione della riga)
				fds_data.setitem(ll_new, "flag_residuo", "N")
			end if
			
			//-------------------------------------------------------------------------------------------------------------------
		
			//esprimo il fattore conversione acquisti
			//in alcuni casi posso calcolarlo automaticamente (vedi SR Modifiche_acquisti rev.2 pagina 8)
			
			ls_appo = fs_values[10]			//Var2_Misura_Articolo espresso in millimetri
			if not isnumber(ls_appo) then
				fs_errore= ls_riga + " Var2_Misura_Articolo non numerico nel file ddt!"
				return -1
			end if
			ll_misura = long(ls_appo)		//in ll_misura la misura dell'articolo fornitore in mm, ovvero la lunghezza dell'articolo [MM/PZ]
			ll_misura = ll_misura / 1000	//converto in ML, metri lineari
			
			if wf_fat_conversione(ls_cod_misura_mag, ld_quan_ddt, ld_quan2_ddt, ll_misura, ld_fat_conversione_acq) < 0 then
				//calcolare a mano il fattore di conversione nuovo
				//di default proporre quello scritto nella relativa riga ordine acquisto
			end if
			
			
//			choose case ls_cod_misura_mag
//				case "PZ"
//					//allora le UM file ddt e ordine acquisto sono entrambe in PZ o Barre
//					//il fattore di conversione acq [UMacq]/[UMmag] = [UMacq]/PZ si calcolerà cosi:
//					//qtaKG del file / qtaPZ nel file
//					if ld_quan2_ddt > 0 then	ld_fat_conversione_acq = ld_quan2_ddt / ld_quan_ddt
//					
//				case "ML"
//					ls_appo = fs_values[10]			//Var2_Misura_Articolo espresso in millimetri
//					if not isnumber(ls_appo) then
//						fs_errore= ls_riga + " Var2_Misura_Articolo non numerico nel file ddt!"
//						return -1
//					end if
//					ll_misura = long(ls_appo)		//in ll_misura la misura dell'articolo fornitore in mm, ovvero la lunghezza dell'articolo [MM/PZ]
//					ll_misura = ll_misura / 1000	//converto in ML, metri lineari
//					
//					//il fattore di conversione acq [UMacq]/[UMmag] = [UMacq]/ML si calcolerà cosi:
//					//qtaKG del file / ( qtaPZ nel file * L)
//					//							dove L è la misura in ML di 1 PZ infatti
//					//							[KG] / ([PZ] * ([ML]/[PZ]) )   -> ottiene [KG]/[ML] che sono le diemnsione del fatt.conv.acquisti
//					//	in tal caso L = (ll_misura / 1000)
//					if ld_quan2_ddt > 0 and ll_misura > 0 then	ld_fat_conversione_acq = ld_quan2_ddt / (ld_quan_ddt * ll_misura)
//					
//					
//				case else
//					//calcolare a mano il fattore di conversione nuovo
//					//di default proporre quello scritto nella relativa riga ordine acquisto
//					
//					
//			end choose
		
			fds_data.setitem(ll_new, "fat_conversione", ld_fat_conversione_acq)
			
			//--------------------------------------
			ls_appo = "Vs DDT n° " + fs_values[4] + " del " + fs_values[3]	//Numero_Documento + Data_Documento
			fds_data.setitem(ll_new, "rif_su_ddt", ls_appo)
		
		end if
		//#####################################################################################################
		

		
	case 3	//BUSTREO/PETTENUZZO
		
		fs_errore  = "Tracciato d.d.t. BUSTREO/PETTENUZZO non ancora comunicato dal fornitore"
		return -1
		
//		ls_appo = fs_values[1]		//NSRif  YYYYNNNNNNPPPP
//		
//		li_anno_reg_acq = integer(left(ls_appo, 4))
//		ll_num_reg_acq = long(mid(ls_appo, 5, 6))
//		ll_prog_riga_acq = integer(mid(ls_appo, 11, 4))
//		
//		fds_data.setitem(ll_new, "anno_registrazione", li_anno_reg_acq)
//		fds_data.setitem(ll_new, "num_registrazione", ll_num_reg_acq)
//		fds_data.setitem(ll_new, "prog_riga_ordine_acq", ll_prog_riga_acq)
//		
//		if wf_get_info_riga_ordine(	li_anno_reg_acq, ll_num_reg_acq, ll_prog_riga_acq, &
//											ls_cod_prodotto, ls_des_prodotto, ld_quan_ordinata, ld_quan_arrivata, ls_cod_misura, ld_prezzo_acquisto, &
//											ld_fat_conversione_acq, fs_errore) < 0 then
//			//in fs_errore il messaggio
//			return -1
//		end if
//		
//		if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
//			fds_data.setitem(ll_new, "cod_prodotto", ls_cod_prodotto)
//			fds_data.setitem(ll_new, "des_prodotto", ls_des_prodotto)
//		end if
//		
//		ls_appo = fs_values[2]		//CArt
//		fds_data.setitem(ll_new, "articolo_file", ls_appo)
//		
//		fds_data.setitem(ll_new, "quan_ordinata", ld_quan_ordinata)
//		fds_data.setitem(ll_new, "quan_arrivata", ld_quan_arrivata)
//		fds_data.setitem(ll_new, "cod_misura", ls_cod_misura)
//		
//		ls_appo = fs_values[4]		//Qta
//		guo_functions.uof_replace_string(ls_appo, ls_dec_sep_2, ls_dec_sep)
//		ld_quan_ordinata = dec(ls_appo)
//		fds_data.setitem(ll_new, "quan_ddt", ld_quan_ordinata)
//		
//		ls_appo = fs_values[6]		//Um
//		fds_data.setitem(ll_new, "um_ddt", ls_appo)
//		
//		fds_data.setitem(ll_new, "prezzo_acquisto", ld_prezzo_acquisto)
//		
//		ls_appo = fs_values[5]		//PrezzoUnitNetto
//		guo_functions.uof_replace_string(ls_appo, ls_dec_sep_2, ls_dec_sep)
//		ld_prezzo_acquisto = dec(ls_appo)
//		fds_data.setitem(ll_new, "prezzo_ddt", ld_prezzo_acquisto)
//		
//		ls_appo = "Ns DDT n° " + fs_values[8] + " del " + fs_values[7]	//DataDdt + NrDdt
//		fds_data.setitem(ll_new, "rif_su_ddt", ls_appo)
		
end choose

return 1
end function

public function integer wf_carica_da_ddt_file (ref datastore fds_data, ref string fs_errore);//05/03/2012 Donato
//questa funzione viene chiamata per eseguire la lettura delle righe degli ordini da evadere, leggendole da un file ddt
//in un formato prestabilito per singolo fornitore


long						ll_file, ll_err, ll_fields[], ll_inizio, ll_len, ll_index, ll_upperbound, ll_righe, ll_ret, ll_new

string						ls_stringa, ls_read, ls_values[], ls_vuoto[], ls_path_file, ls_cod_fornitore, ls_ret, &
							ls_cod_deposito_grezzo
							
s_cs_xx_parametri		ls_struttura

integer					li_tipo_tracciato

boolean					lb_attiva_grezzo=false


ls_cod_fornitore = tab_1.det_1.dw_ext_ddt_file.getitemstring(1, "cod_fornitore")
if isnull(ls_cod_fornitore) or ls_cod_fornitore="" then
	fs_errore = "Indicare il fornitore del D.d.T.!"
	return -1
end if

//tipo tracciato: fi_tipo_tracciato
//1		VIV/INDIVEST
//2		ANODALL/EXTRUSION
//3		BUSTREO/PETTENUZZO
li_tipo_tracciato = guo_functions.uof_tipo_tracciato_ord_acq( ls_cod_fornitore, fs_errore)


ls_path_file = tab_1.det_1.dw_ext_ddt_file.getitemstring(1, "path_file")
if isnull(ls_path_file) or ls_path_file="" then
	fs_errore = "Indicare il percorso del file D.d.T.!"
	return -1
end if

//----------------------------------------------------------------------------------------------------------
//###########################################################
//impostazione flag attiva scarico grezzo e relativo deposito ---------------------------------------
if tab_1.det_1.dw_ext_ddt_file.getitemstring(1, "flag_attiva_grezzo")="S" then
	lb_attiva_grezzo = true
	//carica il deposito
	ls_cod_deposito_grezzo = tab_1.det_1.dw_ext_ddt_file.getitemstring(1, "cod_deposito_grezzo")
	if ls_cod_deposito_grezzo="" then setnull(ls_cod_deposito_grezzo)
else
	lb_attiva_grezzo = false
	setnull(ls_cod_deposito_grezzo)
end if

tab_1.det_2.rb_attiva_scarico.checked = lb_attiva_grezzo
tab_1.det_2.rb_attiva_scarico.triggerevent(clicked!)

tab_1.det_2.dw_depositi.setitem(1, "cod_deposito", ls_cod_deposito_grezzo)
//----------------------------------------------------------------------------------------------------------
//###########################################################


ll_file = fileopen(ls_path_file, LineMode!)
if ll_file < 0 then
	fs_errore = "Errore in apertura del file: "+ls_path_file
	return -1
end if

//preparazione dinamica del tracciato in base al fornitore
if wf_prepara_tracciato_file_ddt(li_tipo_tracciato, ll_fields[], fs_errore)<0 then
	fileclose(ll_file)
	return -1
end if

ll_righe = 0

//-----------------------------------------------------------------------------------------------
do while true
	ll_err = fileread(ll_file, ls_stringa)
	if ll_err < 0 then exit			//dal loop while

	ll_righe += 1
	ll_inizio = 1
	ll_len = 0
	
	ls_values[] = ls_vuoto[]
	
	//#########################################
	for ll_index=1 to upperbound(ll_fields[])
		ll_len = ll_fields[ll_index]
		
		ls_values[ll_index] = trim(mid(ls_stringa, ll_inizio, ll_len))
		
		//nuovo inizio
		ll_inizio = ll_inizio + ll_len
	next
	//#########################################
	
	//carica il datastore copiando le righe da quelle del file ddt del fornitore, in base al tipo tracciato
	if wf_carica_ds_da_file_ddt(ls_values[], li_tipo_tracciato, fds_data, fs_errore)<0 then
		fileclose(ll_file)
		return -1
	end if
	
loop
//-----------------------------------------------------------------------------------------------

fileclose(ll_file)

if fds_data.rowcount() > 0 then
	ls_struttura.parametro_ds_1 = fds_data
	ls_struttura.parametro_i_1 = li_tipo_tracciato
	openwithparm(w_lettura_ddt_acq_file, ls_struttura)
	
	ls_struttura = message.powerobjectparm
	ls_ret = ls_struttura.parametro_s_1
	
	if ls_ret="OK" then
		//l'utente ha confermato: torna il datastore e poi cicla tutte le righe per elaborarle
		//quindi non fare qui il destroy del datastore ...
		fds_data.reset()
		fds_data = ls_struttura.parametro_ds_1
		
		ll_ret = fds_data.rowcount()
		
		//inserisce nelle datawindow del folder ORDINI le righe che devono essere caricate
		if wf_aggiungi_da_ddt(fds_data, fs_errore)<0 then
			//in fs_errore il messaggio
			return -1
		end if
		
		return 1
		
	else
		//annullato dall'utente ...
		fs_errore = "Annullato dall'utente!"
		return 0
	end if
	
end if

fs_errore = "Nessun dato presente nel file!"
return 0


end function

public function integer wf_aggiungi_da_ddt (datastore fds_data, ref string fs_errore);//05/03/2012 Donato
//questa funzione aggiunge automanticamente le righe degli ordini da evadere, leggendole
//dal file ddt inviato dal fornitore

long ll_index, ll_tot, ll_new
string ls_filtro
dec{4} ldd_quan_acquisto, ldd_fat_conversione, ld_prezzo_acquisto_mag

ll_tot = fds_data.rowcount()

if ll_tot<=0 then
	fs_errore = "Non ci sono righe da inserire!"
	return -1
end if

for ll_index=1 to ll_tot
	
	if ll_index>1 then ls_filtro+=" or "
	
	ls_filtro += "(det_ord_acq_anno_registrazione="+string(fds_data.getitemnumber(ll_index, "anno_registrazione"))+" and "
	ls_filtro += "det_ord_acq_num_registrazione="+string(fds_data.getitemnumber(ll_index, "num_registrazione"))+" and "
	ls_filtro += "det_ord_acq_prog_riga_ordine_acq="+string(fds_data.getitemnumber(ll_index, "prog_riga_ordine_acq"))+")"
	
next

tab_1.det_2.dw_ordini_acquisto_lista.setfilter(ls_filtro)
tab_1.det_2.dw_ordini_acquisto_lista.filter()

tab_1.selecttab(2)
tab_1.det_2.dw_ordini_acquisto_lista.change_dw_current()
get_window().triggerevent("pc_retrieve")

ll_tot = tab_1.det_2.dw_ordini_acquisto_lista.rowcount()

tab_1.det_2.dw_ext_carico_acq_det.reset()

for ll_index=1 to ll_tot
	//giu solo la riga del doppio clic (come prima)
	//solo che la quantità proposta sarà quella del ddt e riportare anche il fattore conversione, 
	//nell'ipotesi che l'utente lo abbia modificato o che sia stato ricalcolato
	wf_aggiungi_riga(ll_index, true)
	
	ls_filtro = ""
	ls_filtro += "anno_registrazione="+string(tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(ll_index, "det_ord_acq_anno_registrazione"))+" and "
	ls_filtro += "num_registrazione="+string(tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(ll_index, "det_ord_acq_num_registrazione"))+" and "
	ls_filtro += "prog_riga_ordine_acq="+string(tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(ll_index, "det_ord_acq_prog_riga_ordine_acq"))
	fds_data.setfilter(ls_filtro)
	fds_data.filter()
	
	//quan_acquisto
	//ldd_quan_acquisto = fds_data.getitemnumber(1, "quan_ddt")
	ldd_quan_acquisto = fds_data.getitemnumber(1, "quan_ddt_2")
	
//	//la quantità acquisto deve essere convertita in Um di acquisto
//	//fat_conversione
//	ldd_fat_conversione = f_dec_tabella("det_ord_acq", ls_filtro, "fat_conversione")
//	ldd_quan_acquisto = ldd_quan_acquisto * ldd_fat_conversione

	tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_index, "fat_conversione",  fds_data.getitemnumber(1, "fat_conversione"))
	tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_index, "flag_saldo",  fds_data.getitemstring(1, "flag_saldo"))
	
	tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_index, "quan_acquisto", ldd_quan_acquisto)
	
	ld_prezzo_acquisto_mag = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_index,"prezzo_acquisto") * fds_data.getitemnumber(1, "fat_conversione")
	tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_index, "prezzo_acquisto_mag", ld_prezzo_acquisto_mag)
	
	if tab_1.det_2.rb_attiva_scarico.checked then
		tab_1.det_2.dw_ext_carico_acq_det.event trigger ue_controlla_riga(ll_index, true, false)
	end if
	
next


return 1
end function

public subroutine wf_aggiungi_riga (integer row, boolean fb_from_file_ddt);string  ls_cod_banca, ls_cod_banca_old, ls_cod_banca_clien_for, ls_cod_banca_clien_for_old, &
		ls_cod_valuta, ls_cod_valuta_old, ls_cod_tipo_listino_prodotto, ls_cod_tipo_listino_prodotto_old, &
		ls_cod_pagamento, ls_cod_pagamento_old, ls_cod_imballo, ls_cod_imballo_old, ls_cod_inoltro, &
		ls_cod_inoltro_old, ls_cod_porto, ls_cod_porto_old, ls_cod_fornitore_old, ls_cod_fornitore, ls_cod_deposito_old, &
		ls_cod_deposito_trasf_old, ls_cod_deposito, ls_cod_deposito_trasf, ls_differenze_message, ls_cod_prodotto, ls_messaggio
long 	ll_anno_registrazione, ll_num_registrazione, ll_riga, ll_i, ll_anno_reg_ord_acq_old, &
		ll_num_reg_ord_acq_old, ll_num_rows, ll_prog_riga_ord_acq, li_ret
decimal ld_cambio_acq, ld_sconto, ld_cambio_acq_old, ld_sconto_old, ld_prezzo_acq, ld_fat_conversione
datetime ldt_data_registrazione
uo_fido_cliente luo_fido_cliente

luo_fido_cliente = create uo_fido_cliente
ldt_data_registrazione = datetime(today(),00:00:00)
ls_cod_prodotto = tab_1.det_2.dw_ordini_acquisto_lista.getitemstring(row, "det_ord_acq_cod_prodotto")
luo_fido_cliente = create uo_fido_cliente
li_ret = luo_fido_cliente.uof_get_blocco_prodotto( ls_cod_prodotto, ldt_data_registrazione, is_cod_parametro_blocco_prodotto, ls_messaggio)
if li_ret=2 then
	//blocco
	g_mb.error(ls_messaggio)
	destroy luo_fido_cliente
	return
elseif li_ret=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
destroy luo_fido_cliente



ls_differenze_message = ""
ll_anno_registrazione = tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_anno_registrazione")
ll_num_registrazione = tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_num_registrazione")
ll_prog_riga_ord_acq = tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_prog_riga_ordine_acq")

// controllo che la riga non sia già stata sposta giù
for ll_i = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
	if ll_anno_registrazione = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "anno_registrazione") and &
		ll_num_registrazione  = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "num_registrazione") and &
		ll_prog_riga_ord_acq  = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_i, "prog_riga_ord_acq") then
			g_mb.messagebox("Carico Acquisti","Questa riga ordine è già stata spostata in acquisto",Information!)
			return
	end if
next
	
if tab_1.det_2.dw_ext_carico_acq_det.rowcount() >= 1 then
	ll_anno_reg_ord_acq_old = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(1, "anno_registrazione")
	ll_num_reg_ord_acq_old  = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(1, "num_registrazione")
	
	select cod_fornitore,
			 cod_banca,
			 cod_banca_clien_for,
			 cod_valuta,
			 cambio_acq,
			 cod_tipo_listino_prodotto,
			 cod_pagamento,
			 sconto,
			 cod_imballo,
			 cod_inoltro,
			 cod_porto,
			 cod_deposito,
			 cod_deposito_trasf
	into   :ls_cod_fornitore_old,
			 :ls_cod_banca_old,
			 :ls_cod_banca_clien_for_old,
			 :ls_cod_valuta_old,
			 :ld_cambio_acq_old,
			 :ls_cod_tipo_listino_prodotto_old,
			 :ls_cod_pagamento_old,
			 :ld_sconto_old,
			 :ls_cod_imballo_old,
			 :ls_cod_inoltro_old,
			 :ls_cod_porto_old,
			 :ls_cod_deposito_old,
			 :ls_cod_deposito_trasf_old
	from   tes_ord_acq
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_ord_acq.anno_registrazione = :ll_anno_reg_ord_acq_old and
			 tes_ord_acq.num_registrazione = :ll_num_reg_ord_acq_old;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante ricerca ordine acquisto")
		return
	end if	 

	select cod_fornitore,
			 cod_banca,
			 cod_banca_clien_for,
			 cod_valuta,
			 cambio_acq,
			 cod_tipo_listino_prodotto,
			 cod_pagamento,
			 sconto,
			 cod_imballo,
			 cod_inoltro,
			 cod_porto,
			 cod_deposito,
			 cod_deposito_trasf
	into   :ls_cod_fornitore,
			 :ls_cod_banca,
			 :ls_cod_banca_clien_for,
			 :ls_cod_valuta,
			 :ld_cambio_acq,
			 :ls_cod_tipo_listino_prodotto,
			 :ls_cod_pagamento,
			 :ld_sconto,
			 :ls_cod_imballo,
			 :ls_cod_inoltro,
			 :ls_cod_porto,
			 :ls_cod_deposito,
			 :ls_cod_deposito_trasf
	from   tes_ord_acq
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_ord_acq.anno_registrazione = :ll_anno_registrazione and
			 tes_ord_acq.num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante ricerca ordine acquisto")
		return
	end if	 

	if ls_cod_fornitore <> ls_cod_fornitore_old then
		g_mb.messagebox("APICE","Attenzione: è stato selezionato un fornitore diverso da quello iniziale")
		return
	end if
	
	// stefanop 28/02/2012: Controllo che siano impostate gli stessi depositi
	if tab_1.det_2.cbx_ddt.checked then
		
		if ls_cod_deposito_old <> ls_cod_deposito then
			ls_differenze_message += "- Codice deposito [" + ls_cod_deposito_old + " - " + ls_cod_deposito + "]~r~n"
		end if
		
		if ls_cod_deposito_trasf_old <> ls_cod_deposito_trasf then
			ls_differenze_message += "- Codice deposito trasferimento [" + ls_cod_deposito_trasf_old + " - " + ls_cod_deposito_trasf + "]~r~n"
		end if
		
	end if
	// ----
	
	if ls_cod_valuta <> ls_cod_valuta_old then ls_differenze_message += "- Valuta [" + ls_cod_valuta_old + " - " + ls_cod_valuta + "]~r~n"
	if ld_cambio_acq <> ld_cambio_acq_old then ls_differenze_message += "- Cambio [" + string(ld_cambio_acq_old) + " - " + string(ld_cambio_acq) + "]~r~n" 
	if ls_cod_tipo_listino_prodotto <> ls_cod_tipo_listino_prodotto_old then ls_differenze_message += "- Tipo Listino [" + ls_cod_tipo_listino_prodotto_old + " - " + ls_cod_tipo_listino_prodotto + "]~r~n"
	if ls_cod_pagamento <> ls_cod_pagamento_old then ls_differenze_message += "- Pagamento [" + ls_cod_pagamento_old + " - " + ls_cod_pagamento + "]~r~n"
	if ld_sconto <> ld_sconto_old then ls_differenze_message += "- Sconto [" + string(ld_sconto_old) + " - " + string(ld_sconto) + "]~r~n"
	if ls_cod_banca <> ls_cod_banca_old then ls_differenze_message += "- Banca [" + ls_cod_banca_old + " - " + ls_cod_banca + "]~r~n"
	if ls_cod_banca_clien_for <> ls_cod_banca_clien_for then ls_differenze_message += "- Banca Cli./For. [" + ls_cod_banca_clien_for + " - " + ls_cod_banca_clien_for + "]~r~n"
	if ls_cod_imballo <> ls_cod_imballo_old then ls_differenze_message += "- Imballo [" + ls_cod_imballo_old + " - " + ls_cod_imballo + "]~r~n"
	if ls_cod_inoltro <> ls_cod_inoltro_old then ls_differenze_message += "- Inoltro [" + ls_cod_inoltro_old + " - " + ls_cod_inoltro + "]~r~n"
	if ls_cod_porto <> ls_cod_porto_old then ls_differenze_message += "- Porto [" + ls_cod_porto_old + " - " + ls_cod_porto + "]~r~n"
		
	if len(ls_differenze_message) > 1 then
		if not g_mb.confirm("APICE","Incongruenze fra questo ordine ed il primo selezionato:~r~n" + ls_differenze_message + "Proseguo lo stesso?") then return
	end if
	
end if

// stefanop: 24/02/2012: carico i depositi del fornitore
if ib_load_dddw_depositi then
	
	select cod_fornitore
	into :ls_cod_fornitore
	from tes_ord_acq
	where tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_ord_acq.anno_registrazione = :ll_anno_registrazione and
			 tes_ord_acq.num_registrazione = :ll_num_registrazione;
			 
	f_po_loaddddw_dw(tab_1.det_2.dw_depositi, &
		"cod_deposito", &
		sqlca, &
		"anag_depositi", &
		"cod_deposito", &
		"des_deposito", &
		"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_fornitore='"+ ls_cod_fornitore +"'")

	ib_load_dddw_depositi = false
// -----
end if
	
tab_1.det_2.dw_ext_carico_acq_det.setfocus()
ll_riga = tab_1.det_2.dw_ext_carico_acq_det.insertrow(0)
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "anno_registrazione", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_anno_registrazione") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "num_registrazione", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_num_registrazione") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "prog_riga_ord_acq", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_prog_riga_ordine_acq") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "cod_fornitore", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemstring(row, "tes_ord_acq_cod_fornitore") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "data_registrazione", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemdatetime(row, "tes_ord_acq_data_registrazione") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "cod_prod_fornitore", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemstring(row, "det_ord_acq_cod_prod_fornitore") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "cod_prodotto", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemstring(row, "det_ord_acq_cod_prodotto") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "des_prodotto", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemstring(row, "cf_des_prodotto") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "cod_misura", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemstring(row, "det_ord_acq_cod_misura") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "quan_acquisto", &
	tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "cf_quan_evasione") )

// stefanop: 29/06/2016: migliorato con i messaggi di errore
//ld_prezzo_acq = tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_prezzo_acquisto") / tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_fat_conversione")
ld_prezzo_acq = tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_prezzo_acquisto")
ld_fat_conversione = tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_fat_conversione")

if isnull(ld_prezzo_acq) then
	ld_prezzo_acq = 0
	g_mb.warning("Attenzione il prezzo della riga " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/"  + &
		string(ll_prog_riga_ord_acq) + " è NULL. Controllare la riga dell'ordine.")
end if

if isnull(ld_fat_conversione) or ld_fat_conversione = 0 then
	ld_fat_conversione = 1
end if

ld_prezzo_acq = ld_prezzo_acq / ld_fat_conversione
// ----
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "prezzo_acquisto", ld_prezzo_acq )

tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "prezzo_acquisto_mag", tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_prezzo_acquisto") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "fat_conversione", tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_fat_conversione") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "flag_saldo", "N" )

tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "anno_commessa", tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_anno_commessa") )
tab_1.det_2.dw_ext_carico_acq_det.setitem(ll_riga, "num_commessa",  tab_1.det_2.dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_num_commessa") )

//05/03/2012 Donato
//se la chiamata viene da lettura file ddt salta questo controllo
//verrà richiamato dalla funzione che ho fatto io per gestire il carico da file ddt del fornitore
if fb_from_file_ddt then
else
	// stefanop 07/07/2010: progetto 140
	if tab_1.det_2.rb_attiva_scarico.checked then
		tab_1.det_2.dw_ext_carico_acq_det.event post ue_controlla_riga(ll_riga, true, false)
		
	elseif tab_1.det_2.rb_attiva_scarico_terzista.checked then
		tab_1.det_2.dw_ext_carico_acq_det.event post ue_controlla_riga_terzista(ll_riga)
		
	end if
end if
// ----

return
end subroutine

public function integer wf_salva_fat_conv_acq (ref string as_errore);//long						ll_index, ll_numero, ll_prog_riga_ord_acq
//integer					li_anno
//dwItemStatus			ldw_status
//dec{5}					ld_fat_conversione
//
//for ll_index = 1 to tab_1.det_2.dw_ext_carico_acq_det.rowcount()
//	ldw_status = tab_1.det_2.dw_ext_carico_acq_det.GetItemStatus(ll_index, "fat_conversione", Primary!)
//	
//	if ldw_status = DataModified! then
//		//aggiorna in anag_prodotti il fattore di conversione acquisti
//		
//		li_anno = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_index, "anno_registrazione")
//		ll_numero = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_index, "num_registrazione")
//		ll_prog_riga_ord_acq = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(ll_index, "prog_riga_ord_acq")
//		ld_fat_conversione = tab_1.det_2.dw_ext_carico_acq_det.getitemdecimal(ll_index, "fat_conversione")
//		
//		update det_ord_acq
//		set fat_conversione=:ld_fat_conversione
//		where 	cod_azienda=:s_cs_xx.cod_azienda and
//					anno_registrazione=:li_anno and
//					num_registrazione=:ll_numero and
//					prog_riga_ordine_acq=:ll_prog_riga_ord_acq;
//		
//		if sqlca.sqlcode<0 then
//			as_errore = "Errore in aggiornamento fattore conv.acq. riga ordine "+string(li_anno)+string(ll_numero)+string(ll_prog_riga_ord_acq)+ " : "+sqlca.sqlerrtext
//			return -1
//		end if
//		
//	end if
//next

return 0
end function

public function integer wf_get_info_riga_ordine (integer al_anno_reg_acq, long al_num_reg_acq, long al_prog_riga_acq, ref string as_cod_prodotto, ref string as_des_prodotto, ref decimal ad_quan_ordinata, ref decimal ad_quan_arrivata, ref string as_cod_misura, ref decimal ad_prezzo_acquisto, ref decimal ad_fat_conversione_acq, ref string as_errore);

select d.cod_prodotto, 
		p.des_prodotto,
		d.quan_ordinata,
		d.quan_arrivata,
		d.cod_misura,
		d.prezzo_acquisto,
		d.fat_conversione
into 	:as_cod_prodotto,
		:as_des_prodotto,
		:ad_quan_ordinata,
		:ad_quan_arrivata,
		:as_cod_misura,
		:ad_prezzo_acquisto,
		:ad_fat_conversione_acq
from det_ord_acq as d
join anag_prodotti as p on p.cod_azienda=d.cod_azienda and
					p.cod_prodotto=d.cod_prodotto
where d.cod_azienda=:s_cs_xx.cod_azienda and
		d.anno_registrazione=:al_anno_reg_acq and
		d.num_registrazione=:al_num_reg_acq and
		d.prog_riga_ordine_acq=:al_prog_riga_acq;
		
if sqlca.sqlcode<0 then
	as_errore  = "Errore in lettura dati riga ordine "+string(al_anno_reg_acq)+"/"+string(al_num_reg_acq)+"/"+string(al_prog_riga_acq)+" : "+&	
						sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore  = "Riga ordine "+string(al_anno_reg_acq)+"/"+string(al_num_reg_acq)+"/"+string(al_prog_riga_acq)+" NON trovata!"
	return -1
end if

return 0
end function

public function integer wf_fat_conversione (string as_cod_misura_mag, decimal ad_quan_ddt, decimal ad_quan2_ddt, long al_misura_in_metri, ref decimal ad_fat_conversione_acq);

//esprimo il fattore conversione acquisti
//in alcuni casi posso calcolarlo automaticamente (vedi SR Modifiche_acquisti rev.2 pagina 8)
choose case as_cod_misura_mag
	case "PZ"
		//allora le UM file ddt e ordine acquisto sono entrambe in PZ o Barre
		//il fattore di conversione acq [UMacq]/[UMmag] = [UMacq]/PZ si calcolerà cosi:
		//qtaKG del file / qtaPZ nel file
		if ad_quan_ddt > 0 then	ad_fat_conversione_acq = ad_quan2_ddt / ad_quan_ddt
		
	case "ML"
		//il fattore di conversione acq [UMacq]/[UMmag] = [UMacq]/ML si calcolerà cosi:
		//qtaKG del file / ( qtaPZ nel file * L)
		//							dove L è la misura in ML di 1 PZ infatti
		//							[KG] / ([PZ] * ([ML]/[PZ]) )   -> ottiene [KG]/[ML] che sono le diemnsione del fatt.conv.acquisti
		//	in tal caso L = (ll_misura / 1000)
		if ad_quan_ddt > 0 and al_misura_in_metri > 0 then	ad_fat_conversione_acq = ad_quan2_ddt / (ad_quan_ddt * al_misura_in_metri)
		
		
	case else
		//calcolare a mano il fattore di conversione nuovo
		//di default proporre quello scritto nella relativa riga ordine acquisto
		
		return -1
		
end choose

return 0
end function

public function integer wf_materie_prime_terzista (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, string as_cod_terzista, string as_cod_deposito_terzista, decimal ad_quan_acquisto_um_mag, ref string as_errore);
uo_magazzino			luo_mag

uo_funzioni_1			luo_f1

datetime					ldt_data_a

dec{4}					ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_qta_ordine_commessa, ld_quan_sl_commessa, &
							ld_qta_fabb_sl_std

string						ls_deposito_mp, ls_where, ls_materia_prima[], ls_versione_prima[], ls_chiave[], ls_des_deposito, ls_sql, ls_cod_mp, ls_cod_prodotto_commessa

integer					li_ret

double					ldd_quantita_utilizzo[], ldd_giacenza, ldd_qta_mp

long						ll_new, ll_index, ll_find, ll_tot

boolean					lb_negativa, lb_ricalcola_mp

datastore				lds_data


ls_deposito_mp = as_cod_deposito_terzista
lb_ricalcola_mp = false

if isnull(ls_deposito_mp) or ls_deposito_mp="" then
	//leggi il/i depositi del terzista; se + di uno chiedi quale utilizzare ...
	ls_sql = "SELECT cod_deposito FROM anag_depositi "+&
				"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
						"cod_fornitore='" + as_cod_terzista + "' and "+&
						"((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + "))"
						

	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql)

	if ll_tot < 0 then
		as_errore = "Errore durante la lettura dei depositi del terzista : " + sqlca.sqlerrtext
		return -1
		
		
	elseif ll_tot = 0 then
		as_errore = "Nessun deposito trovato per il terzista " + as_cod_terzista + ". " + &
						"Non saranno visualizzate eventuali giacenze presso terzista delle materie prime del prodotto " + as_cod_semilavorato + " della riga!"
		return 1
		
		
	elseif ll_tot>1 then
		//chiedi quale deposito considerare
		
		openwithparm(w_sel_deposito_terzista, as_cod_terzista)
		
		ls_deposito_mp = message.stringparm
		as_cod_deposito_terzista = ls_deposito_mp
		
	else
		//ce n'è solo uno
		ls_deposito_mp = lds_data.getitemstring(1, 1)
		as_cod_deposito_terzista = ls_deposito_mp
	end if
	
	tab_1.det_2.dw_depositi.reset()
	tab_1.det_2.dw_depositi.insertrow(0)
	tab_1.det_2.dw_depositi.setitem(1, "cod_deposito", ls_deposito_mp)

end if




ls_where = "and cod_deposito=~~'"+ls_deposito_mp+"~~'"
ldt_data_a = datetime(today(), 00:00:00)
lb_negativa = false


//lettura MP
luo_f1 = create uo_funzioni_1


//-1		errore critico
//1			warning e dato non elaborato
//0			tutto OK
li_ret = luo_f1.uof_trova_mat_prime_varianti(	ai_anno_commessa, al_num_commessa, as_cod_semilavorato, true, &
															ls_materia_prima[], ls_versione_prima[], ldd_quantita_utilizzo[], ls_deposito_mp, as_errore)
ld_quan_sl_commessa = luo_f1.id_quan_semilavorato

if isnull(ld_quan_sl_commessa) then ld_quan_sl_commessa = 0

destroy luo_f1

if li_ret<>0 then return li_ret

//---------------------------------------------------------------------
select		quan_ordine, cod_prodotto
into		:ld_qta_ordine_commessa, :ls_cod_prodotto_commessa
from anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:ai_anno_commessa and
			num_commessa=:al_num_commessa;

	//questa cosa qui solo se il prodotto della riga ordine coincide con il prodotto della commessa
	//cioè solo se siamo in fase evasione semilavorato commessa
	//se il prodotto della riga ordine è il prodotto della commessa 		&
	//		la qta ordinata della commessa	 è diversa da zero 				&
	//		la qta ordinata della commessa è diversa da quella che si sta evadendo
if as_cod_semilavorato=ls_cod_prodotto_commessa and ld_qta_ordine_commessa > 0 and ld_qta_ordine_commessa<>ad_quan_acquisto_um_mag then
	
	//ricalcola il coefficiente del fabbisogno materie prime per visualizzarlo nella dw delle giacenze mp presso terzista
	//		Qfabb_mp_new = [Qsl_da evadere / Qsl_ordinata] * Qfabb_mp
	ld_qta_fabb_sl_std = ld_qta_ordine_commessa
	lb_ricalcola_mp = true
	

	//se invece il prodotto della riga ordine NON coincide con il prodotto della commessa
	//cioè solo se siamo in fase evasione semilavorato intermedio
	//		se la qta fabbisogno del semilavorato intermedio è diversa da zero		&
	//		la qta fabbisogno del semilavorato intermedio è diversa da quella che si sta evadendo
elseif ld_quan_sl_commessa<> 0 and ld_quan_sl_commessa<>ad_quan_acquisto_um_mag then
	
	//ricalcola il coefficiente del fabbisogno materie prime per visualizzarlo nella dw delle giacenze mp presso terzista
	//		Qfabb_mp_new = [Qsl_da evadere / Qsl_ordinata] * Qfabb_mp
	ld_qta_fabb_sl_std = ld_quan_sl_commessa
	lb_ricalcola_mp = true
	
end if

if lb_ricalcola_mp then
	for ll_index = 1 to upperbound(ls_materia_prima[])
		ldd_quantita_utilizzo[ll_index] = (ad_quan_acquisto_um_mag / ld_qta_fabb_sl_std) * ldd_quantita_utilizzo[ll_index]
	next
end if
//--------------------------------------------------------------------


if isnull(ls_deposito_mp) or ls_deposito_mp="" then ls_deposito_mp = as_cod_deposito_terzista

luo_mag = CREATE uo_magazzino


ls_des_deposito = f_des_tabella("anag_depositi","cod_deposito='" + ls_deposito_mp +"'" ,"des_deposito")

for ll_index=1 to upperbound(ls_materia_prima[])
	ls_cod_mp = ls_materia_prima[ll_index]
	ldd_qta_mp = ldd_quantita_utilizzo[ll_index]
	
	ll_find = tab_1.det_5.dw_scarico_terzista.find("cod_prodotto='"+ls_cod_mp+"'", 1, tab_1.det_5.dw_scarico_terzista.rowcount())
	
	if ll_find>0 then
		//riga esistente, vai ad incremento
		tab_1.det_5.dw_scarico_terzista.setitem(ll_find, "quantita", tab_1.det_5.dw_scarico_terzista.getitemdecimal(ll_find, "quantita") + ldd_qta_mp)
		ll_new = ll_find
	else
		//----------------------------------------------------------------------------------------------------
		ll_new = tab_1.det_5.dw_scarico_terzista.insertrow(0)
		tab_1.det_5.dw_scarico_terzista.setitem(ll_new, "cod_prodotto", ls_cod_mp)
		tab_1.det_5.dw_scarico_terzista.setitem(ll_new, "cod_deposito", ls_deposito_mp)
		tab_1.det_5.dw_scarico_terzista.setitem(ll_new, "des_deposito", ls_des_deposito)
		tab_1.det_5.dw_scarico_terzista.setitem(ll_new, "quantita", ldd_qta_mp)
		tab_1.det_5.dw_scarico_terzista.setitem(ll_new, "um", f_des_tabella("anag_prodotti","cod_prodotto='" + ls_cod_mp +"'" ,"cod_misura_mag"))
		//*****************
		li_ret = luo_mag.uof_saldo_prod_date_decimal(ls_cod_mp, ldt_data_a, ls_where, ld_quant_val[], as_errore, "D", &
																					ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
		if li_ret <0 then
			destroy luo_mag
			return -1
		end if
	
		//prendo solo il primo (ce ne sarà uno solo perchè gli ho passato il flitro per deposito fornitore)
		if upperbound(ld_giacenza_stock[]) > 0 then
			ldd_giacenza = ld_giacenza_stock[1]
		end if
		
		if isnull(ldd_giacenza) then ldd_giacenza = 0
		//*****************

		tab_1.det_5.dw_scarico_terzista.setitem(ll_new, "giacenza", ldd_giacenza)
		//----------------------------------------------------------------------------------------------------
	end if
	
	
	//se ottengo una giacenza negativa metto un'immagine apposita nel testo del folder
	if not lb_negativa then
		if ldd_giacenza - tab_1.det_5.dw_scarico_terzista.getitemdecimal(ll_new, "quantita") < 0 then
			lb_negativa = true
			 tab_1.det_5.PictureName = "DeleteWatch5!"
		end if
	end if
	
next

destroy luo_mag

tab_1.det_5.text = "Scarico da Terzista (" + string(tab_1.det_5.dw_scarico_terzista.rowcount()) + ")"


return 0
end function

public function integer wf_crea_mov_magazzino (long al_row, datastore fds_rag_acq, datetime fdt_data_registrazione, ref long al_anno_mov_mag, ref long al_num_mov_mag, ref string as_errore);/** 
 * 
 **/
 
string ls_cod_prodotto, ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_referenza, ls_cod_fornitore[], &
			ls_cod_deposito[]
decimal ld_quan_acquisto_alt
long 	ll_prog_stock[], ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_quantita, ll_valore, ll_num_documento, &
		ll_anno_registrazione[], ll_num_registrazione[], ll_anno_reg_mov_rag[], ll_num_reg_mov_rag[]
datetime ldt_data_stock[],ldt_data_documento
uo_magazzino luo_magazzino

if isnull(fdt_data_registrazione) then
	fdt_data_registrazione = datetime(today(), 00:00:00)
end if

if fds_rag_acq.getitemstring(al_row, "flag_tipo_riga") = "3" then
	return 0
end if

ls_cod_prodotto = fds_rag_acq.getitemstring(al_row, "cod_prodotto_alt")
ld_quan_acquisto_alt = fds_rag_acq.getitemnumber(al_row, "quan_acquisto_alt")

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])
ls_cod_deposito[1] = is_cod_deposito

if f_crea_dest_mov_magazzino (is_cod_tipo_movimento_grezzo, &
										ls_cod_prodotto, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_cliente[], &
										ls_cod_fornitore[], &
										ref ll_anno_reg_dest_stock, &
										ref ll_num_reg_dest_stock) = -1 then
	as_errore = "Errore nella creazione della destinazione movimento magazzino"
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
								 ll_num_reg_dest_stock, &
								 is_cod_tipo_movimento_grezzo, &
								 ls_cod_prodotto) = -1 then
	as_errore=  "Errore nella verifica della destinazione movimento magazzino"
	return -1
end if

luo_magazzino = create uo_magazzino

if luo_magazzino.uof_movimenti_mag (&
					fdt_data_registrazione, &
					is_cod_tipo_movimento_grezzo, &
					"S", &
					ls_cod_prodotto, &
					ld_quan_acquisto_alt, &
					ll_valore, &
					ll_num_documento, &
					ldt_data_documento, &
					ls_referenza, &
					ll_anno_reg_dest_stock, &
					ll_num_reg_dest_stock, &
					ls_cod_deposito[], &
					ls_cod_ubicazione[], &
					ls_cod_lotto[], &
					ldt_data_stock[], &
					ll_prog_stock[], &
					ls_cod_fornitore[], &
					ls_cod_cliente[], &
					ref ll_anno_registrazione[], &
					ref ll_num_registrazione[]) = 0 then
					

	if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, ll_num_reg_dest_stock) = -1 then
		//ROLLBACK;
		return -1
	end if
	//COMMIT;
	
	// stefanop 06/12/2011: movimento anche il prodotto raggruppato
	if luo_magazzino.uof_movimenti_mag_ragguppato(fdt_data_registrazione, &
								is_cod_tipo_movimento_grezzo, &
								"S", &
								ls_cod_prodotto, &
								ld_quan_acquisto_alt, &
								ll_valore, &
								ll_num_documento, &
								ldt_data_documento, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ll_anno_reg_mov_rag[], &
								ll_num_reg_mov_rag[],&
								long(ls_referenza),&
								ll_anno_registrazione[1],&
								ll_num_registrazione[1]) = -1 then
		destroy luo_magazzino
		as_errore = "Movimento non eseguito a causa di un errore"
		return -1
	end if					
	// ----
	
	destroy luo_magazzino
	
	al_anno_mov_mag = ll_anno_registrazione[1]
	al_num_mov_mag = ll_num_registrazione[1]
	setnull(as_errore)
	return 0
else
	destroy luo_magazzino
	//ROLLBACK;
	as_errore = "Movimento non eseguito a causa di un errore"
	return -1
end if



return  1
end function

event pc_setwindow;call super::pc_setwindow;string					l_criteriacolumn[], l_searchtable[], l_searchcolumn[], ls_ASG
windowobject		lw_oggetti[],l_objects[]
boolean				lb_EOF, lb_GIB
integer				li_anno_ordine
long					ll_num_ordine, ll_temp


save_on_close(c_socnosave)
set_w_options(c_noresizewin + c_NoEnablePopup)


tab_1.det_2.dw_ordini_acquisto_lista.set_dw_options(sqlca, &
                                        pcca.null_object, &
													 c_nonew + &
													 c_nomodify + &
													 c_nodelete + &
													 c_disablecc + &
													 c_noretrieveonopen, &
													 c_default)

tab_1.det_3.dw_produzione.settransobject(sqlca)
dw_ext_label.insertrow(0)
tab_1.det_2.dw_depositi.insertrow(0)

iuo_dw_main = tab_1.det_2.dw_ordini_acquisto_lista


l_criteriacolumn[1] = "cod_fornitore"
l_criteriacolumn[2] = "cod_prodotto"
l_criteriacolumn[3] = "anno_registrazione"
l_criteriacolumn[4] = "num_registrazione"
l_criteriacolumn[5] = "anno_commessa"
l_criteriacolumn[6] = "num_commessa"

l_searchtable[1] = "tes_ord_acq"
l_searchtable[2] = "tes_ord_acq"
l_searchtable[3] = "tes_ord_acq"
l_searchtable[4] = "tes_ord_acq"
l_searchtable[5] = "tes_ord_acq"
l_searchtable[6] = "det_ord_acq"

l_searchcolumn[1] = "tes_ord_acq_cod_fornitore"
l_searchcolumn[2] = "det_ord_acq_cod_prodotto"
l_searchcolumn[3] = "det_ord_acq_anno_registrazione"
l_searchcolumn[4] = "det_ord_acq_num_registrazione"
l_searchcolumn[5] = "det_ord_acq_anno_commessa"
l_searchcolumn[6] = "det_ord_acq_num_commessa"

tab_1.det_1.dw_ext_carico_acquisti.fu_wiredw(l_criteriacolumn[], &
											tab_1.det_2.dw_ordini_acquisto_lista, &
											l_searchtable[], &
											l_searchcolumn[], &
											SQLCA)

tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_quan_ordinata.visible = 0
tab_1.det_2.dw_ordini_acquisto_lista.object.cf_quan_ordinata.visible = 0
tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_quan_arrivata.visible = 0
tab_1.det_2.dw_ordini_acquisto_lista.object.cf_quan_arrivata.visible = 0
tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_cod_prod_fornitore.visible = 1
tab_1.det_2.dw_ordini_acquisto_lista.object.det_ord_acq_cod_prod_fornitore.visible = 1

//13/12/2012 Donato
//per ora solo per GIBUS e con parametro EOF, abilita l'evasione ordine acquisto da file ddt inviato dal fornitore
guo_functions.uof_get_parametro_azienda("GIB", lb_GIB)
guo_functions.uof_get_parametro_azienda("EOF", lb_EOF)

if lb_EOF and lb_GIB then
	tab_1.det_1.dw_ext_ddt_file.visible = true
	tab_1.det_1.dw_ext_ddt_file.visible = true
end if
//----------------------------------------------------------------------------------------------------------------------------------

tab_1.det_2.dw_ext_carico_acq_det.Object.p_remove.filename= s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"

tab_1.det_3.enabled = false
tab_1.det_2.dw_ordini_acquisto_lista.change_dw_current()
tab_1.det_1.dw_ext_carico_acquisti.setitem(tab_1.det_1.dw_ext_carico_acquisti.getrow(),"data_consegna_inizio",datetime(date("01/01/1900")))
tab_1.det_1.dw_ext_carico_acquisti.setitem(tab_1.det_1.dw_ext_carico_acquisti.getrow(),"data_consegna_fine",datetime(date("31/12/2999")))

setnull(is_cod_fornitore)
setnull(il_num_bolla_acq)
setnull(ii_anno_bolla_acq)
setnull(il_anno_reg_fat_acq)
setnull(il_num_reg_fat_acq)

if not isnull(s_cs_xx.parametri.parametro_s_14) and s_cs_xx.parametri.parametro_s_14 <> "" then 
	ib_proviene_da_bolla = true
	is_cod_fornitore = s_cs_xx.parametri.parametro_s_14
	tab_1.det_1.dw_ext_carico_acquisti.setitem(1, "cod_fornitore", is_cod_fornitore)	
	tab_1.det_1.dw_ext_carico_acquisti.object.cod_fornitore.protect = 1	
//	cb_ricerca_fornitore.enabled = false
else
	setnull(is_cod_fornitore)
end if

if not isnull(s_cs_xx.parametri.parametro_s_12) and s_cs_xx.parametri.parametro_s_12 <> "" and s_cs_xx.parametri.parametro_s_12 = "Bolla" then 
	if not isnull(s_cs_xx.parametri.parametro_ul_3) and s_cs_xx.parametri.parametro_ul_3 > 0 then 
		il_num_bolla_acq = s_cs_xx.parametri.parametro_ul_3
	else
		setnull(il_num_bolla_acq)
	end if
	
	if not isnull(s_cs_xx.parametri.parametro_i_1) and s_cs_xx.parametri.parametro_i_1 <> 0 then 
		ii_anno_bolla_acq = s_cs_xx.parametri.parametro_i_1
	else
		setnull(ii_anno_bolla_acq)
	end if
		
	tab_1.det_2.cb_genera_bolle.enabled = true
elseif not isnull(s_cs_xx.parametri.parametro_s_12) and s_cs_xx.parametri.parametro_s_12 <> "" and s_cs_xx.parametri.parametro_s_12 = "Fattura" then 
	if not isnull(s_cs_xx.parametri.parametro_ul_3) and s_cs_xx.parametri.parametro_ul_3 <> 0 then 
		il_anno_reg_fat_acq = s_cs_xx.parametri.parametro_ul_3
	else
		setnull(il_anno_reg_fat_acq)
	end if

	if not isnull(s_cs_xx.parametri.parametro_ul_2) and s_cs_xx.parametri.parametro_ul_2 <> 0 then 
		il_num_reg_fat_acq = s_cs_xx.parametri.parametro_ul_2
	else
		setnull(il_num_reg_fat_acq)
	end if
	tab_1.det_2.cb_genera_fatture.enabled = true
else
	tab_1.det_2.cb_genera_bolle.enabled = true
	tab_1.det_2.cb_genera_fatture.enabled = true
end if

setnull(s_cs_xx.parametri.parametro_s_14)
setnull(s_cs_xx.parametri.parametro_s_12)
setnull(s_cs_xx.parametri.parametro_ul_3)
setnull(s_cs_xx.parametri.parametro_ul_2)
setnull(s_cs_xx.parametri.parametro_i_1)

// stefanop 22/07/2010
//ll_offset_height = dw_ext_carico_acq_det.y - dw_ordini_acquisto_lista.y
tab_1.det_2.dw_ext_carico_acq_det.object.p_arrow.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\arrow_up.png"
// ----


//attiva visualizzazione giacenza materie prime del prodotto di riga nella commessa indicata nella riga
//Scarico Parziale ramo Commessa
guo_functions.uof_get_parametro("SPT", ib_SPT)
if ib_SPT then
else
	tab_1.det_2.rb_attiva_scarico_terzista.visible=false
	tab_1.det_5.visible = false
end if

//attiva scarico grezzo
select flag
into   :ls_ASG
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'ASG' and flag_parametro='F';

if sqlca.sqlcode <> 0 then
	ls_ASG = "N"
end if


if ls_ASG = "S" then
	//attiva scarico da terzista di default
	tab_1.det_2.rb_attiva_scarico.checked=true
	tab_1.det_2.rb_niente.checked=false
else
	//imposta radiobutton su NON ATTIVARE
	tab_1.det_2.rb_attiva_scarico.checked=false
	tab_1.det_2.rb_niente.checked=true
	
	//imposta altezza riga datawindow
	tab_1.det_2.dw_ext_carico_acq_det.object.datawindow.detail.height = 72
	tab_1.det_2.dw_depositi.visible = false
	tab_1.det_5.enabled = false
	
end if

event post ue_initial_size()


//carico acquisti direttamente da finestra ordini di acquisto
try
	ll_temp = s_cs_xx.parametri.parametro_d_1_a[1]
catch (runtimeerror err)
	ll_temp = 0
end try

if ll_temp = 6969 then
	li_anno_ordine = s_cs_xx.parametri.parametro_d_1_a[2]
	ll_num_ordine = s_cs_xx.parametri.parametro_d_1_a[3]
	
	tab_1.det_1.dw_ext_carico_acquisti.setitem(tab_1.det_1.dw_ext_carico_acquisti.getrow(), "anno_registrazione", li_anno_ordine)
	tab_1.det_1.dw_ext_carico_acquisti.setitem(tab_1.det_1.dw_ext_carico_acquisti.getrow(), "num_registrazione", ll_num_ordine)
	tab_1.det_1.dw_ext_carico_acquisti.setitem(tab_1.det_1.dw_ext_carico_acquisti.getrow(), "cod_fornitore", s_cs_xx.parametri.parametro_s_1)
	
	//reset variabili globali
	s_cs_xx.parametri.parametro_d_1_a[1] = 0	
	s_cs_xx.parametri.parametro_d_1_a[2] = 0
	s_cs_xx.parametri.parametro_d_1_a[3] = 0
	s_cs_xx.parametri.parametro_s_1 = ""
	
	tab_1.det_1.cb_ricerca.postevent(clicked!)
	
end if



end event

on w_carico_acquisti.create
int iCurrent
call super::create
this.dw_ext_label=create dw_ext_label
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ext_label
this.Control[iCurrent+2]=this.tab_1
end on

on w_carico_acquisti.destroy
call super::destroy
destroy(this.dw_ext_label)
destroy(this.tab_1)
end on

event resize;/** TOLTO ANCHESTOR SCRIPT **/

dw_ext_label.move(20, newheight - dw_ext_label.height - 20)
tab_1.resize(newwidth - 40, dw_ext_label.y - 20)

tab_1.event ue_resize()
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_1.det_1.dw_ext_carico_acquisti, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_tipo_deposito='I'")

//per ora non serve
//f_po_loaddddw_dw(tab_1.det_1.dw_ext_carico_acquisti, &
//	"cod_deposito_trasf", &
//	sqlca, &
//	"anag_depositi", &
//	"cod_deposito", &
//	"des_deposito", &
//	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_tipo_deposito='T'")
end event

type dw_ext_label from datawindow within w_carico_acquisti
integer x = 32
integer y = 2096
integer width = 3671
integer height = 384
integer taborder = 130
string title = "none"
string dataobject = "d_ext_carico_acquisti_label"
boolean border = false
boolean livescroll = true
end type

type tab_1 from tab within w_carico_acquisti
event create ( )
event destroy ( )
event ue_resize ( )
integer x = 27
integer y = 20
integer width = 3968
integer height = 2040
integer taborder = 90
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
det_1 det_1
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
end type

on tab_1.create
this.det_1=create det_1
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
this.det_5=create det_5
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4,&
this.det_5}
end on

on tab_1.destroy
destroy(this.det_1)
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
destroy(this.det_5)
end on

event ue_resize();long ll_height

// Pulsanti - Dettaglio 2
det_2.cb_genera_bolle.move(det_2.width - det_2.cb_genera_bolle.width - 20, det_2.height - det_2.cb_genera_bolle.height - 20)
det_2.cb_genera_fatture.move(det_2.cb_genera_bolle.x - det_2.cb_genera_bolle.width - 20, det_2.cb_genera_bolle.y)
det_2.cb_modifica.move(det_2.cb_genera_fatture.x - det_2.cb_genera_fatture.width - 20, det_2.cb_genera_bolle.y)
det_2.cb_mem_prezzo.move(det_2.cb_modifica.x - det_2.cb_modifica.width - 20, det_2.cb_genera_bolle.y)
det_2.cb_salda_righe_vuote.move(det_2.cb_mem_prezzo.x - det_2.cb_mem_prezzo.width - 20, det_2.cb_genera_bolle.y)
det_2.cb_etichette.move(det_2.cb_salda_righe_vuote.x - det_2.cb_salda_righe_vuote.width - 20, det_2.cb_genera_bolle.y)

det_2.cbx_ddt.move(20, det_2.cb_genera_bolle.y)

// DW - Dettaglio 2
ll_height = (det_2.cb_genera_bolle.y / 2) - det_2.dw_depositi.height
det_2.dw_ordini_acquisto_lista.move(0,0)
det_2.dw_ordini_acquisto_lista.resize(det_2.width, ll_height)

det_2.rb_attiva_scarico.move(20, det_2.dw_ordini_acquisto_lista.height + 5)
det_2.rb_niente.move(det_2.rb_attiva_scarico.x + det_2.rb_attiva_scarico.width + 20, det_2.rb_attiva_scarico.y)
det_2.rb_attiva_scarico_terzista.move(det_2.rb_niente.x + det_2.rb_niente.width + 20, det_2.rb_attiva_scarico.y)
det_2.dw_depositi.move(det_2.rb_attiva_scarico_terzista.x + det_2.rb_attiva_scarico_terzista.width + 20 , det_2.rb_attiva_scarico.y - 5)

det_2.dw_ext_carico_acq_det.move(0, det_2.dw_depositi.y + det_2.dw_depositi.height)
det_2.dw_ext_carico_acq_det.resize(det_2.width, ll_height)
end event

type det_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3931
integer height = 1912
long backcolor = 12632256
string text = "Filtra Ordini"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ext_ddt_file dw_ext_ddt_file
cb_ricerca cb_ricerca
cb_reset cb_reset
cbx_righe_libere cbx_righe_libere
cbx_quan_ordinata cbx_quan_ordinata
cbx_quan_arrivata cbx_quan_arrivata
cbx_data_consegna cbx_data_consegna
cbx_cod_prod_fornitore cbx_cod_prod_fornitore
dw_ext_carico_acquisti dw_ext_carico_acquisti
end type

on det_1.create
this.dw_ext_ddt_file=create dw_ext_ddt_file
this.cb_ricerca=create cb_ricerca
this.cb_reset=create cb_reset
this.cbx_righe_libere=create cbx_righe_libere
this.cbx_quan_ordinata=create cbx_quan_ordinata
this.cbx_quan_arrivata=create cbx_quan_arrivata
this.cbx_data_consegna=create cbx_data_consegna
this.cbx_cod_prod_fornitore=create cbx_cod_prod_fornitore
this.dw_ext_carico_acquisti=create dw_ext_carico_acquisti
this.Control[]={this.dw_ext_ddt_file,&
this.cb_ricerca,&
this.cb_reset,&
this.cbx_righe_libere,&
this.cbx_quan_ordinata,&
this.cbx_quan_arrivata,&
this.cbx_data_consegna,&
this.cbx_cod_prod_fornitore,&
this.dw_ext_carico_acquisti}
end on

on det_1.destroy
destroy(this.dw_ext_ddt_file)
destroy(this.cb_ricerca)
destroy(this.cb_reset)
destroy(this.cbx_righe_libere)
destroy(this.cbx_quan_ordinata)
destroy(this.cbx_quan_arrivata)
destroy(this.cbx_data_consegna)
destroy(this.cbx_cod_prod_fornitore)
destroy(this.dw_ext_carico_acquisti)
end on

type dw_ext_ddt_file from u_dw_search within det_1
boolean visible = false
integer x = 18
integer y = 1136
integer width = 2994
integer height = 756
integer taborder = 120
boolean bringtotop = true
string dataobject = "d_ext_lettura_ddt_acq_file"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_null, ls_cod_fornitore, ls_path_file,ls_errore
integer li_ret
datastore lds_data
datawindow ldw_null
any la_any[]

accepttext()
setnull(ls_null)

choose case dwo.name
	//-------------------------------------------------------------------------------------------
	case "b_sel_fornitore"
		ls_path_file = dw_ext_ddt_file.getitemstring(1, "path_file")
		
		setnull(ldw_null)
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_fornitore(ldw_null,"cod_fornitore")
		guo_ricerca.uof_get_last_results(la_any[])
		ls_cod_fornitore = ""
		
		if upperbound(la_any[]) > 0 then
			ls_cod_fornitore = la_any[1]
		end if
		
		if isnull(ls_cod_fornitore) or ls_cod_fornitore="" then
			dw_ext_ddt_file.setitem(1, "cod_fornitore", ls_null)
			ls_cod_fornitore = ""
		else
			dw_ext_ddt_file.setitem(1, "cod_fornitore", ls_cod_fornitore)
		end if
		
		f_po_loaddddw_dw(	tab_1.det_1.dw_ext_ddt_file, &
									"cod_deposito_grezzo", &
									sqlca, &
									"anag_depositi", &
									"cod_deposito", &
									"des_deposito", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
												"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
												"cod_fornitore='"+ ls_cod_fornitore +"'")
	
		
		//controlli dopo selezione del file e/o fornitore
		wf_check_before_import(ls_cod_fornitore, ls_path_file)
		
	
	//-------------------------------------------------------------------------------------------
	case "b_sel_file"
		
		ls_cod_fornitore = dw_ext_ddt_file.getitemstring(1, "cod_fornitore")
		
		//selezione del file
		li_ret = wf_seleziona_file_ddt(ls_cod_fornitore, ls_path_file)
		choose case li_ret
			case is <0
				g_mb.error(ls_path_file)
				
			case 1
				setitem(1, "path_file", ls_path_file)
				
			case else
				//probabile che l'utente abbia annullato tutto ... zio
				
		end choose
		
		//controlli dopo selezione del file e/o fornitore
		wf_check_before_import(ls_cod_fornitore, ls_path_file)
		
		return
	
	//-------------------------------------------------------------------------------------------
	case "b_reset"
		setitem(1, "cod_fornitore", ls_null)
		setitem(1, "path_file", ls_null)
		setitem(1, "flag_attiva_grezzo", "N")
		setitem(1, "cod_deposito_grezzo", ls_null)
	
	//-------------------------------------------------------------------------------------------
	case "b_procedi"
		
		//imposto datastore generico lettura da file ---------------------------------
		lds_data = create datastore
		lds_data.dataobject = "d_lettura_ddt_acq_file"
		lds_data.settransobject(sqlca)
		//--------------
		
		li_ret = wf_carica_da_ddt_file(lds_data, ls_errore)
		if li_ret<0 then
			destroy lds_data;
			g_mb.error(ls_errore)
			return
			
		elseif li_ret= 0 then
			destroy lds_data;
			g_mb.warning(ls_errore)
			return
			
		else
			//alla fine distruggi sto datastore
			destroy lds_data;
		end if
	
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_cod_fornitore, ls_path_file

if row>0 then
else
	return 
end if

choose case dwo.name
	case "path_file"
		ls_cod_fornitore = tab_1.det_1.dw_ext_ddt_file.getitemstring(1, "cod_fornitore")
		wf_check_before_import(ls_cod_fornitore, data)
		
	case "cod_fornitore"
		ls_path_file = tab_1.det_1.dw_ext_ddt_file.getitemstring(1, "path_file")
		
		f_po_loaddddw_dw(	tab_1.det_1.dw_ext_ddt_file, &
									"cod_deposito_grezzo", &
									sqlca, &
									"anag_depositi", &
									"cod_deposito", &
									"des_deposito", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
												"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
												"cod_fornitore='"+ data +"'")
		
		wf_check_before_import(data, ls_path_file)
		
end choose
end event

type cb_ricerca from commandbutton within det_1
integer x = 2167
integer y = 588
integer width = 361
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string				ls_filtro, ls_cod_fornitore, ls_cod_prodotto, ls_str, ls_filtro_1, ls_cod_deposito, ls_cod_deposito_ricerca//, ls_cod_deposito_trasf_ricerca
datetime			ldt_inizio, ldt_fine
integer			li_anno_ordine
long				ll_num_ordine


ib_load_dddw_depositi = true

if tab_1.det_2.cb_mem_prezzo.enabled = true then
	
	if g_mb.messagebox("APICE","Ignorare la memorizzazione del prezzo modificato?",question!,yesno!,2) = 2 then
		return 1
	else
		tab_1.det_2.cb_mem_prezzo.enabled = false
	end if	
		
end if

dw_ext_carico_acquisti.setcolumn(3)
dw_ext_carico_acquisti.setcolumn(4)
dw_ext_carico_acquisti.triggerevent("itemchanged")

ls_cod_fornitore = dw_ext_carico_acquisti.getitemstring(dw_ext_carico_acquisti.getrow(),"cod_fornitore")
ls_cod_prodotto  = dw_ext_carico_acquisti.getitemstring(dw_ext_carico_acquisti.getrow(),"cod_prodotto")
ldt_inizio = dw_ext_carico_acquisti.getitemdatetime(dw_ext_carico_acquisti.getrow(),"data_consegna_inizio")
ldt_fine   = dw_ext_carico_acquisti.getitemdatetime(dw_ext_carico_acquisti.getrow(),"data_consegna_fine")
li_anno_ordine = dw_ext_carico_acquisti.getitemnumber(dw_ext_carico_acquisti.getrow(),"anno_registrazione")
ll_num_ordine = dw_ext_carico_acquisti.getitemnumber(dw_ext_carico_acquisti.getrow(),"num_registrazione")


ls_filtro = ""
ls_filtro_1 = ""

if not isnull(ls_cod_fornitore) then
	ls_filtro = ls_filtro + "(tes_ord_acq_cod_fornitore = '" + ls_cod_fornitore + "'"
	ls_filtro_1 = ls_filtro
	if not isnull(ls_cod_prodotto) then
		ls_filtro = ls_filtro + " and "
	else
		if not isnull(ldt_inizio) and ldt_inizio > datetime(date("01/01/1900")) then
			ls_filtro = ls_filtro + " and "
		elseif not isnull(ldt_fine  ) and ldt_fine   < datetime(date("31/12/2999")) then
			  ls_filtro = ls_filtro + " and "
		end if
	end if
end if

if not isnull(ls_cod_prodotto) then
	if ls_filtro = "" then ls_filtro = ls_filtro + "("
	ls_filtro = ls_filtro + "det_ord_acq_cod_prodotto = '" + ls_cod_prodotto + "'"
	ls_filtro_1 = ls_filtro
	if not isnull(ldt_inizio) and ldt_inizio > datetime(date("01/01/1900")) then
		ls_filtro = ls_filtro + " and "
	elseif not isnull(ldt_fine  ) and ldt_fine   < datetime(date("31/12/2999")) then
		  ls_filtro = ls_filtro + " and "
	end if
end if

if not isnull(ldt_inizio) and ldt_inizio > datetime(date("01/01/1900")) then
	if ls_filtro = "" and ls_filtro_1 <> "" then ls_filtro = ls_filtro + "("
	ls_str = string(date(ldt_inizio))
	ls_filtro = ls_filtro + "(det_ord_acq_data_consegna >= datetime('" + ls_str + "'))"
	if not isnull(ldt_fine) and ldt_fine < datetime(date("31/12/2999")) then
		ls_filtro = ls_filtro + " and "
	end if
end if

if not isnull(ldt_fine) and ldt_fine < datetime(date("31/12/2999")) then
	ls_str = string(date(ldt_fine))
	ls_filtro = ls_filtro + "(det_ord_acq_data_consegna <= datetime('" + ls_str + "'))"
end if

if cbx_data_consegna.thirdstate then
	if cbx_data_consegna.checked then
		if ls_filtro_1 <> "" then
			ls_filtro = ls_filtro + ")"
		end if
	else
		if ls_filtro_1 <> "" then
			ls_filtro = ls_filtro + ")"
		end if
	end if
else
	if cbx_data_consegna.checked then
		if ls_filtro_1 = "" then
			ls_filtro = ls_filtro + " or isnull(det_ord_acq_data_consegna)"
		else
			ls_filtro = ls_filtro + ") and " + ls_filtro_1 +" and isnull(det_ord_acq_data_consegna))"
		end if
	else
		if ls_filtro_1 <> "" then
			ls_filtro = ls_filtro + ") and " + ls_filtro_1 +" and not isnull(det_ord_acq_data_consegna))"
		end if
	end if
end if

if cbx_righe_libere.checked = false then
	if ls_filtro = "" then
		ls_filtro = " not isnull(det_ord_acq_cod_prodotto) "
	else
		ls_filtro = ls_filtro + " and not isnull(det_ord_acq_cod_prodotto) "
	end if
end if

//Donato 29/06/2012 ricerca ordini acquisto per deposito e deposito trasferimento
ls_cod_deposito_ricerca = dw_ext_carico_acquisti.getitemstring(dw_ext_carico_acquisti.getrow(),"cod_deposito")

if ls_cod_deposito_ricerca<>"" and not isnull(ls_cod_deposito_ricerca) then
	ls_filtro += " and tes_ord_acq_cod_deposito='"+ls_cod_deposito_ricerca+"' "
end if
//----------------------------------------------------------------------------------------------


if li_anno_ordine>0 then
	if ls_filtro<>"" then ls_filtro += " and"
	ls_filtro += " det_ord_acq_anno_registrazione="+string(li_anno_ordine)+" "
end if

if ll_num_ordine>0 then
	if ls_filtro<>"" then ls_filtro += " and"
	ls_filtro += " det_ord_acq_num_registrazione="+string(ll_num_ordine)+" "
end if



tab_1.det_2.dw_ordini_acquisto_lista.setfilter(ls_filtro)
tab_1.det_2.dw_ordini_acquisto_lista.filter()

// se ho impostato il fornitore filtro già la lista dei depositi
if not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
	
	ib_load_dddw_depositi = false
	
	f_po_loaddddw_dw(tab_1.det_2.dw_depositi, &
		"cod_deposito", &
		sqlca, &
		"anag_depositi", &
		"cod_deposito", &
		"des_deposito", &
		"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_fornitore='"+ ls_cod_fornitore +"'")
		
	ls_cod_deposito = wf_get_deposito_fornitore(ls_cod_fornitore)
	if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
		tab_1.det_2.dw_depositi.setitem(1, "cod_deposito", ls_cod_deposito)
	end if
	
end if

tab_1.selecttab(2)
tab_1.det_2.dw_ordini_acquisto_lista.change_dw_current()
get_window().triggerevent("pc_retrieve")

end event

type cb_reset from commandbutton within det_1
integer x = 1778
integer y = 588
integer width = 361
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string ls_filtro, ls_null
long   ll_null
datetime ldt_null


setnull(ls_null)
setnull(ldt_null)
setnull(ll_null)

if tab_1.det_2.cb_mem_prezzo.enabled = true then
	
	if g_mb.messagebox("APICE","Ignorare la memorizzazione del prezzo modificato?",question!,yesno!,2) = 2 then
		return 1
	else
		tab_1.det_2.cb_mem_prezzo.enabled = false
	end if	
		
end if

if not isnull(is_cod_fornitore) then 
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"cod_prodotto", ls_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"data_consegna_inizio",ldt_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"data_consegna_fine"  ,ldt_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"anno_commessa"  ,ll_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"num_commessa"  ,ll_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"anno_registrazione"  ,ll_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"num_registrazione"  ,ll_null)
else
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"cod_prodotto", ls_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"cod_fornitore", ls_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"data_consegna_inizio",ldt_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"data_consegna_fine"  ,ldt_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"anno_registrazione"  ,ll_null)
	dw_ext_carico_acquisti.setitem(dw_ext_carico_acquisti.getrow(),"num_registrazione"  ,ll_null)
	ls_filtro = ""
	tab_1.det_2.dw_ordini_acquisto_lista.setfilter(ls_filtro)
	tab_1.det_2.dw_ordini_acquisto_lista.filter()
	tab_1.selecttab(2)
	tab_1.det_2.dw_ordini_acquisto_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
end if
end event

type cbx_righe_libere from checkbox within det_1
integer x = 46
integer y = 940
integer width = 2240
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza Righe Senza Codice Prodotto (Righe Libere o Costi Aggiuntivi)"
boolean checked = true
end type

type cbx_quan_ordinata from checkbox within det_1
integer x = 41
integer y = 620
integer width = 809
integer height = 76
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza Quantità Ordinata"
end type

event clicked;if this.checked then
	tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_quan_ordinata.visible = 1
	tab_1.det_2.dw_ordini_acquisto_lista.object.cf_quan_ordinata.visible = 1
else
	tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_quan_ordinata.visible = 0
	tab_1.det_2.dw_ordini_acquisto_lista.object.cf_quan_ordinata.visible = 0
end if
end event

type cbx_quan_arrivata from checkbox within det_1
integer x = 41
integer y = 700
integer width = 809
integer height = 76
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza Quantità Arrivata"
end type

event clicked;if this.checked then
	tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_quan_arrivata.visible = 1
	tab_1.det_2.dw_ordini_acquisto_lista.object.cf_quan_arrivata.visible = 1
else	
	tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_quan_arrivata.visible = 0
	tab_1.det_2.dw_ordini_acquisto_lista.object.cf_quan_arrivata.visible = 0
end if
end event

type cbx_data_consegna from checkbox within det_1
event clicked pbm_bnclicked
integer x = 41
integer y = 860
integer width = 1248
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza Righe Ordini senza Date Consegna"
boolean threestate = true
boolean thirdstate = true
end type

type cbx_cod_prod_fornitore from checkbox within det_1
integer x = 41
integer y = 780
integer width = 1015
integer height = 76
integer taborder = 170
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza Codice Prodotto Fornitore"
boolean checked = true
end type

event clicked;if this.checked then
	tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_cod_prod_fornitore.visible = 1
	tab_1.det_2.dw_ordini_acquisto_lista.object.det_ord_acq_cod_prod_fornitore.visible = 1
	tab_1.det_2.dw_ext_carico_acq_det.object.st_cod_prod_fornitore.visible = 1
	tab_1.det_2.dw_ext_carico_acq_det.object.cod_prod_fornitore.visible = 1
else
	tab_1.det_2.dw_ordini_acquisto_lista.object.st_det_ord_acq_cod_prod_fornitore.visible = 0
	tab_1.det_2.dw_ordini_acquisto_lista.object.det_ord_acq_cod_prod_fornitore.visible = 0
	tab_1.det_2.dw_ext_carico_acq_det.object.st_cod_prod_fornitore.visible = 0
	tab_1.det_2.dw_ext_carico_acq_det.object.cod_prod_fornitore.visible = 0
end if
end event

type dw_ext_carico_acquisti from u_dw_search within det_1
integer x = 18
integer y = 20
integer width = 2555
integer height = 552
integer taborder = 40
string dataobject = "d_ext_carico_acquisti"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ext_carico_acquisti,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ext_carico_acquisti,"cod_prodotto")
end choose
end event

type det_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3931
integer height = 1912
long backcolor = 12632256
string text = "Ordini"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
rb_attiva_scarico_terzista rb_attiva_scarico_terzista
rb_niente rb_niente
rb_attiva_scarico rb_attiva_scarico
cbx_ddt cbx_ddt
dw_depositi dw_depositi
cb_modifica cb_modifica
cb_salda_righe_vuote cb_salda_righe_vuote
cb_mem_prezzo cb_mem_prezzo
cb_genera_fatture cb_genera_fatture
cb_genera_bolle cb_genera_bolle
cb_etichette cb_etichette
dw_ordini_acquisto_lista dw_ordini_acquisto_lista
dw_ext_carico_acq_det dw_ext_carico_acq_det
end type

on det_2.create
this.rb_attiva_scarico_terzista=create rb_attiva_scarico_terzista
this.rb_niente=create rb_niente
this.rb_attiva_scarico=create rb_attiva_scarico
this.cbx_ddt=create cbx_ddt
this.dw_depositi=create dw_depositi
this.cb_modifica=create cb_modifica
this.cb_salda_righe_vuote=create cb_salda_righe_vuote
this.cb_mem_prezzo=create cb_mem_prezzo
this.cb_genera_fatture=create cb_genera_fatture
this.cb_genera_bolle=create cb_genera_bolle
this.cb_etichette=create cb_etichette
this.dw_ordini_acquisto_lista=create dw_ordini_acquisto_lista
this.dw_ext_carico_acq_det=create dw_ext_carico_acq_det
this.Control[]={this.rb_attiva_scarico_terzista,&
this.rb_niente,&
this.rb_attiva_scarico,&
this.cbx_ddt,&
this.dw_depositi,&
this.cb_modifica,&
this.cb_salda_righe_vuote,&
this.cb_mem_prezzo,&
this.cb_genera_fatture,&
this.cb_genera_bolle,&
this.cb_etichette,&
this.dw_ordini_acquisto_lista,&
this.dw_ext_carico_acq_det}
end on

on det_2.destroy
destroy(this.rb_attiva_scarico_terzista)
destroy(this.rb_niente)
destroy(this.rb_attiva_scarico)
destroy(this.cbx_ddt)
destroy(this.dw_depositi)
destroy(this.cb_modifica)
destroy(this.cb_salda_righe_vuote)
destroy(this.cb_mem_prezzo)
destroy(this.cb_genera_fatture)
destroy(this.cb_genera_bolle)
destroy(this.cb_etichette)
destroy(this.dw_ordini_acquisto_lista)
destroy(this.dw_ext_carico_acq_det)
end on

type rb_attiva_scarico_terzista from radiobutton within det_2
integer x = 1047
integer y = 900
integer width = 823
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Visualizza giacenze Terzista"
end type

event clicked;dw_ext_carico_acq_det.object.datawindow.detail.height = 72
dw_depositi.visible = true
dw_depositi.object.cod_deposito_t.text = "Deposito Scarico Terzista:"

tab_1.det_5.enabled = true
tab_1.det_2.cbx_ddt.checked = false


end event

type rb_niente from radiobutton within det_2
integer x = 645
integer y = 900
integer width = 398
integer height = 76
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Non attivare"
boolean checked = true
end type

event clicked;dw_ext_carico_acq_det.object.datawindow.detail.height = 72
dw_depositi.visible = false

tab_1.det_5.enabled = false
end event

type rb_attiva_scarico from radiobutton within det_2
integer x = 23
integer y = 896
integer width = 617
integer height = 84
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Attiva Scarico Grezzi"
end type

event clicked;//if checked then
//	dw_ext_carico_acq_det.object.datawindow.detail.height = 144
//	dw_depositi.visible = true
//else
//	dw_ext_carico_acq_det.object.datawindow.detail.height = 72
//	dw_depositi.visible = false
//end if

dw_ext_carico_acq_det.object.datawindow.detail.height = 144
dw_depositi.visible = true
dw_depositi.object.cod_deposito_t.text = "Deposito Scarico Grezzo:"

tab_1.det_5.enabled = false

end event

type cbx_ddt from checkbox within det_2
integer x = 32
integer y = 1808
integer width = 896
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Prepara per DDT Trasferimento"
boolean checked = true
end type

type dw_depositi from uo_std_dw within det_2
integer x = 1829
integer y = 888
integer width = 2103
integer height = 100
integer taborder = 180
string dataobject = "d_ext_carico_acquisti_depositi"
boolean border = false
end type

event itemchanged;call super::itemchanged;if not isnull(data) and data <> "" then
	
	wf_ricalcola_giacenze()
	
end if
end event

type cb_modifica from commandbutton within det_2
integer x = 2766
integer y = 1800
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Modifica"
end type

event clicked;long ll_i


if dw_ext_carico_acq_det.getrow() > 0 then
	s_cs_xx.parametri.parametro_dw_1 = dw_ext_carico_acq_det
	s_cs_xx.parametri.parametro_i_1 = dw_ext_carico_acq_det.getrow()
	open(w_mod_carico_acq)
	for ll_i = 1 to dw_ext_carico_acq_det.rowcount()
		if dw_ext_carico_acq_det.getitemstring(ll_i,"prezzo_modificato") = "S" then
			cb_mem_prezzo.enabled = true
		end if
	next	
else	
	g_mb.messagebox("Carico Acquisti","Nessuna riga da modificare!")
end if
end event

type cb_salda_righe_vuote from commandbutton within det_2
integer x = 1989
integer y = 1800
integer width = 361
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "S.do Vuote"
end type

event clicked;string ls_flag_saldo,ls_cod_prodotto
long ll_anno_ordine, ll_num_ordine,ll_prog_riga_ordine_acq, ll_i

if dw_ext_carico_acq_det.rowcount() > 0 then
	if g_mb.messagebox("APICE","Sei sicuro di voler saldare (rendere evase) le righe libere (prodotti non a magazzino, trasporti e altre spese) ?",Question!,YesNo!,2) = 2 then return

	for ll_i = 1 to dw_ext_carico_acq_det.rowcount()
		dw_ext_carico_acq_det.accepttext()
		ll_anno_ordine = dw_ext_carico_acq_det.getitemnumber(ll_i, "anno_registrazione")
		ll_num_ordine = dw_ext_carico_acq_det.getitemnumber(ll_i, "num_registrazione")
		ll_prog_riga_ordine_acq = dw_ext_carico_acq_det.getitemnumber(ll_i, "prog_riga_ord_acq")
		ls_flag_saldo = dw_ext_carico_acq_det.getitemstring(ll_i, "flag_saldo")
		ls_cod_prodotto = dw_ext_carico_acq_det.getitemstring(ll_i, "cod_prodotto")
		if isnull(ls_cod_prodotto) and ls_flag_saldo = "S" then
			update det_ord_acq
			set    flag_saldo    = 'S'
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ordine and
					 num_registrazione = :ll_num_ordine and
					 prog_riga_ordine_acq = :ll_prog_riga_ordine_acq ;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore durante l'operazione di saldo delle righe libere.~r~n" + sqlca.sqlerrtext)
			end if
			
		end if	
		
		if wf_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine) = -1 then
			rollback;
			return
		end if
		
	next
	commit;
	
	dw_ext_carico_acq_det.reset()
	tab_1.det_1.cb_ricerca.triggerevent("clicked")
else
	g_mb.messagebox("Carico Acquisti", "Nulla da saldare !!!!", exclamation!)
end if

end event

type cb_mem_prezzo from commandbutton within det_2
integer x = 2377
integer y = 1800
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Mem. &Prezzo"
end type

event clicked;string   ls_prodotto, ls_fornitore, ls_valuta, ls_misura, ls_operazione

long     ll_anno, ll_num, ll_i, ll_count

dec{4}   ld_prezzo, ld_quantita

dec{6}   ld_fat_conv

datetime ldt_oggi


dw_ext_carico_acq_det.accepttext()

ldt_oggi = datetime(today(),now())

ld_quantita = 99999999.9999

for ll_i = 1 to dw_ext_carico_acq_det.rowcount()
	
	if dw_ext_carico_acq_det.getitemstring(ll_i,"prezzo_modificato") = "N" then
		continue
	end if	

	ls_prodotto = dw_ext_carico_acq_det.getitemstring(ll_i,"cod_prodotto")
	ls_fornitore = dw_ext_carico_acq_det.getitemstring(ll_i,"cod_fornitore")
	ls_misura = dw_ext_carico_acq_det.getitemstring(ll_i,"cod_misura")
	ll_anno = dw_ext_carico_acq_det.getitemnumber(ll_i,"anno_registrazione")
	ll_num = dw_ext_carico_acq_det.getitemnumber(ll_i,"num_registrazione")
	ld_prezzo = dw_ext_carico_acq_det.getitemnumber(ll_i,"prezzo_acquisto")
	ld_fat_conv = dw_ext_carico_acq_det.getitemnumber(ll_i,"fat_conversione")
	
	if ld_fat_conv = 0 or isnull(ld_fat_conv) then
		ld_fat_conv = 1
	end if
	
	select cod_valuta
	into   :ls_valuta
	from   tes_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_num;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella memorizzazione del prezzo modificato nella riga " + string(ll_i) + ":~nErrore nella select di tes_ord_acq: " + sqlca.sqlerrtext)
		return -1
	end if
	
	select count(*)
	into   :ll_count
	from   listini_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_prodotto and
			 cod_fornitore = :ls_fornitore and
			 cod_valuta = :ls_valuta and
			 data_inizio_val = :ldt_oggi;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella memorizzazione del prezzo modificato nella riga " + string(ll_i) + ":~nErrore nella select di listini_fornitori: " + sqlca.sqlerrtext)
		return -1
	end if
	
	if ll_count > 0 then
		
		ls_operazione = "update"
		
		update listini_fornitori
		set    cod_azienda = :s_cs_xx.cod_azienda,
				 cod_prodotto = :ls_prodotto,
				 cod_fornitore = :ls_fornitore,
				 cod_valuta = :ls_valuta,
				 data_inizio_val = :ldt_oggi,
				 quantita_1 = :ld_quantita,
				 prezzo_1 = :ld_prezzo,
				 prezzo_ult_acquisto = :ld_prezzo,
				 cod_misura = :ls_misura,
				 fat_conversione = :ld_fat_conv
		where  cod_azienda = :s_cs_xx.cod_azienda and
			 	 cod_prodotto = :ls_prodotto and
			 	 cod_fornitore = :ls_fornitore and
			 	 cod_valuta = :ls_valuta and
			 	 data_inizio_val = :ldt_oggi;
		
	else
		
		ls_operazione = "insert"
	
		insert
		into   listini_fornitori
				 (cod_azienda,
				  cod_prodotto,
				  cod_fornitore,
				  cod_valuta,
				  data_inizio_val,
				  quantita_1,
				  prezzo_1,
				  prezzo_ult_acquisto,
				  cod_misura,
				  flag_for_pref,
				  fat_conversione)
		values (:s_cs_xx.cod_azienda,
				  :ls_prodotto,
				  :ls_fornitore,
				  :ls_valuta,
				  :ldt_oggi,
				  :ld_quantita,
				  :ld_prezzo,
				  :ld_prezzo,
				  :ls_misura,
				  'N',
				  :ld_fat_conv);
	
	end if
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella memorizzazione del prezzo modificato nella riga " + string(ll_i) + ":~nErrore nella " + ls_operazione + " di listini_fornitori: " + sqlca.sqlerrtext)
		rollback;
		return -1
	else
		commit;
		dw_ext_carico_acq_det.setitem(ll_i,"prezzo_modificato","N")
	end if
	
next	

il_prezzi_modificati = 0

enabled = false
end event

type cb_genera_fatture from commandbutton within det_2
integer x = 3154
integer y = 1800
integer width = 361
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Gen. Fatture"
end type

event clicked;integer li_ret
string ls_messaggio, ls_cod_deposito, ls_cod_deposito_trasf
long ll_anno_ord, ll_num_ord

if dw_ext_carico_acq_det.rowcount() > 0 then
	
	dw_ext_carico_acq_det.accepttext()
	
//	//-----------------------------------------------------------------------------------------------------------------------------
//	//Donato 04/04/2013
//	//se sono stati modificati i fattori di conversione, salvali nella riga ordine acquisto prima di procedere
//	//SR Modifiche_acquisti
//	li_ret = wf_salva_fat_conv_acq(ls_messaggio)
//	if li_ret < 0 then
//		g_mb.error(ls_messaggio)
//		rollback;
//		
//		return -1
//	end if
//	//-----------------------------------------------------------------------------------------------------------------------------
	
	// stefanop 16/07/2010: progetto 140, funzione 3: carico tipo movimento
	if rb_attiva_scarico.checked then
		if wf_carica_tipo_movimento_grezzo(false) < 0 then return -1
		
		// se il codice fornitore è vuoto allora lo prelevo dall'ordine
		if isnull(is_cod_fornitore) then
			if wf_carica_deposito(dw_ext_carico_acq_det.getitemstring(dw_ext_carico_acq_det.getrow(), "cod_fornitore")) < 0 then return -1
		else
			if wf_carica_deposito(is_cod_fornitore) < 0 then return -1
		end if
	end if
	// ----
		
	if isnull(is_cod_fornitore) then
		// stefanop 28/02/2012: suggeristo il deposito nella finestra
		ll_anno_ord = dw_ext_carico_acq_det.getitemnumber(1, "anno_registrazione")
		ll_num_ord = dw_ext_carico_acq_det.getitemnumber(1, "num_registrazione")
		
		select cod_deposito, cod_deposito_trasf
		into :ls_cod_deposito, :ls_cod_deposito_trasf
		from tes_ord_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ord and
				 num_registrazione = :ll_num_ord;
				 
		if not isnull(ls_cod_deposito_trasf) and ls_cod_deposito_trasf <> "" then
			s_cs_xx.parametri.parametro_s_3 = ls_cod_deposito_trasf
		elseif not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
			s_cs_xx.parametri.parametro_s_3 = ls_cod_deposito
		end if
		// ----
		
		window_open(w_ext_dati_fattura, 0)

		if isnull(s_cs_xx.parametri.parametro_s_1)  then
			return
		end if
		
		li_ret = wf_genera_fatture_acq(ref ls_messaggio)
	else
		li_ret = wf_carica_dett_fatture(ref ls_messaggio)
	end if
	
	if li_ret < 0 then
		//ERRORE
		g_mb.messagebox("Carico Acquisti: ERRORE", "Si è verificato un errore in fase di generazione fattura di acquisto~r~n" + ls_messaggio, StopSign!)
		rollback;
		
		//stefanop 19/07/2010:
		return -1
	else
		// Visualizzo il messaggio che è tutto andato a buon fine.
		commit; 
		if ls_messaggio <> "" and not isnull(ls_messaggio) then 
			g_mb.messagebox("Carico Acquisti", ls_messaggio, exclamation!)
		end if

	end if
	
	// stefanop 28/02/2012: Nel caso l'ordine presenti un desposito di trasferimento oppure il deposito scelto nella finestrella
	// prima della generazione è diverso da quello della testata originaria allora devo chiedere per un DDT di trasferimento
	wf_genera_bolla_trasferimento(il_anno_reg_fat_acq, il_num_reg_fat_acq, "fat_acq")
	// ----

	dw_ext_carico_acq_det.reset()
	
	il_prezzi_modificati = 0
	
	cb_mem_prezzo.enabled = false
	
	tab_1.det_1.cb_ricerca.triggerevent("clicked")
	tab_1.det_5.dw_scarico_terzista.reset()
else
	
	g_mb.messagebox("Carico Acquisti", "Nessun Ordine da Caricare", exclamation!)
	
	rollback;
	
end if

end event

type cb_genera_bolle from commandbutton within det_2
integer x = 3538
integer y = 1800
integer width = 361
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Genera Bolle"
end type

event clicked;integer li_ret
string ls_messaggio, ls_cod_fornitore, ls_cod_deposito, ls_cod_deposito_trasf, ls_des_deposito
long ll_anno_ord, ll_num_ord

if il_prezzi_modificati > 0 then
	if g_mb.messagebox("APICE",string(il_prezzi_modificati) + " prezzi sono stati modificati.~nMemorizzarli prima di continuare?",question!,yesno!,1) = 1 then
		cb_mem_prezzo.triggerevent("clicked")
	end if
end if	

if dw_ext_carico_acq_det.rowcount() > 0 then
	
	dw_ext_carico_acq_det.accepttext()
	
//	//-----------------------------------------------------------------------------------------------------------------------------
//	//Donato 04/04/2013
//	//se sono stati modificati i fattori di conversione, salvali nella riga ordine acquisto prima di procedere
//	//SR Modifiche_acquisti
//	li_ret = wf_salva_fat_conv_acq(ls_messaggio)
//	if li_ret < 0 then
//		g_mb.error(ls_messaggio)
//		rollback;
//		
//		return -1
//	end if
//	//-----------------------------------------------------------------------------------------------------------------------------
	
	// stefanop 16/07/2010: progetto 140, funzione 3: carico tipo movimento
	if rb_attiva_scarico.checked then
		if wf_carica_tipo_movimento_grezzo(false) < 0 then return -1
		
		// se il codice fornitore è vuoto allora lo prelevo dall'ordine
		if isnull(is_cod_fornitore) then
			if wf_carica_deposito(dw_ext_carico_acq_det.getitemstring(dw_ext_carico_acq_det.getrow(), "cod_fornitore")) < 0 then return -1
		else
			if wf_carica_deposito(is_cod_fornitore) < 0 then return -1
		end if
	end if
	// ----
	
	if isnull(is_cod_fornitore) then
		// stefanop 28/02/2012: suggeristo il deposito nella finestra
		ll_anno_ord = dw_ext_carico_acq_det.getitemnumber(1, "anno_registrazione")
		ll_num_ord = dw_ext_carico_acq_det.getitemnumber(1, "num_registrazione")
		
		select cod_deposito, cod_deposito_trasf
		into :ls_cod_deposito, :ls_cod_deposito_trasf
		from tes_ord_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ord and
				 num_registrazione = :ll_num_ord;
				 
		if not isnull(ls_cod_deposito_trasf) and ls_cod_deposito_trasf <> "" then
			s_cs_xx.parametri.parametro_s_3 = ls_cod_deposito_trasf
		elseif not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
			s_cs_xx.parametri.parametro_s_3 = ls_cod_deposito
		end if
		// ----
		
		window_open(w_ext_dati_bolla, 0)
		if isnull(s_cs_xx.parametri.parametro_s_1)  then
			rollback;
			return
		end if
		
		li_ret = wf_genera_bolle_acq(ref ls_messaggio)
	else
				
		li_ret = wf_carica_dett_bolle(ref ls_messaggio, ii_anno_bolla_acq, il_num_bolla_acq)
	end if
	
	if li_ret = 0 then
		
		g_mb.success("E' stata creata la Bolla " + string(ii_anno_bolla_acq) + "/" + string(il_num_bolla_acq) + " !")
		commit;
		
	else
		
		if ls_messaggio <> "" and not isnull(ls_messaggio) then
			g_mb.messagebox("Carico Acquisti", ls_messaggio, exclamation!)
		end if
		rollback;
		// stefanop 16/07/2010
		return -1
		
	end if
	
	uo_calcola_documento_euro luo_calcolo
	
	luo_calcolo = create uo_calcola_documento_euro
	
	if luo_calcolo.uof_calcola_documento(ii_anno_bolla_acq,il_num_bolla_acq,"bol_acq",ls_messaggio) <> 0 then
		g_mb.messagebox("APICE",ls_messaggio)
	end if
	
	destroy luo_calcolo
	
	// stefanop 28/02/2012: Nel caso l'ordine presenti un desposito di trasferimento oppure il deposito scelto nella finestrella
	// prima della generazione è diverso da quello della testata originaria allora devo chiedere per un DDT di trasferimento
	wf_genera_bolla_trasferimento(ii_anno_bolla_acq, il_num_bolla_acq, "bol_acq")
	// ----
	
	dw_ext_carico_acq_det.reset()
	
	il_prezzi_modificati = 0
	
	cb_mem_prezzo.enabled = false
	
	tab_1.det_1.cb_ricerca.triggerevent("clicked")
	tab_1.det_5.dw_scarico_terzista.reset()
else
	g_mb.messagebox("Carico Acquisti", "Nessun Ordine da Caricare", exclamation!)
end if

commit;
end event

type cb_etichette from commandbutton within det_2
integer x = 1600
integer y = 1800
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichette"
end type

event clicked;s_cs_xx.parametri.parametro_dw_1 = dw_ext_carico_acq_det

string ls_personalizzato

select flag
into   :ls_personalizzato
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'GLC';
		 
if sqlca.sqlcode = 0 and not isnull(ls_personalizzato) and ls_personalizzato = "S" then
	window_open(w_report_etichette_barcode_new,-1)
else
	window_open(w_report_etichette_barcode,-1)
end if
end event

type dw_ordini_acquisto_lista from uo_cs_xx_dw within det_2
integer x = 32
integer y = 20
integer width = 3584
integer height = 860
integer taborder = 50
string dataobject = "d_lista_ordini_acquisto"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;call super::doubleclicked;long ll_anno_ord_acq, ll_num_ord_acq, ll_index, ll_tot, ll_anno_ord_acq_r, ll_num_ord_acq_r

if row > 0 then
	
	if dwo.name = "det_ord_acq_num_registrazione" then
		//su colonna num_ordine devi tirare giù tutte le righe con questo numero
		ll_anno_ord_acq = dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_anno_registrazione")
		ll_num_ord_acq = dw_ordini_acquisto_lista.getitemnumber(row, "det_ord_acq_num_registrazione")
		
		ll_tot = dw_ordini_acquisto_lista.rowcount()
		for ll_index = 1 to ll_tot
			ll_anno_ord_acq_r = dw_ordini_acquisto_lista.getitemnumber(ll_index, "det_ord_acq_anno_registrazione")
			ll_num_ord_acq_r = dw_ordini_acquisto_lista.getitemnumber(ll_index, "det_ord_acq_num_registrazione")
			
			if ll_anno_ord_acq_r=ll_anno_ord_acq and ll_num_ord_acq_r=ll_num_ord_acq then
				wf_aggiungi_riga(ll_index, false)
			end if
			
		next
		
	else
		//giu solo la riga del doppio clic (come prima)
		wf_aggiungi_riga(row, false)
	end if

end if


end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_ext_carico_acq_det from uo_std_dw within det_2
event ue_imposta_current ( )
event type integer ue_controlla_riga ( integer ai_row,  boolean ab_delete_on_error,  boolean ab_read_quan_alt )
event type integer ue_controlla_riga_terzista ( integer ai_row )
event type integer ue_aggiorna_riga_terzista ( integer ai_row )
integer x = 32
integer y = 1008
integer width = 3589
integer height = 740
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_ext_carico_acq_det"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_imposta_current();iuo_dw_main = dw_ordini_acquisto_lista
change_dw_current(dw_ordini_acquisto_lista)
end event

event type integer ue_controlla_riga(integer ai_row, boolean ab_delete_on_error, boolean ab_read_quan_alt);/**
 * Stefanop
 * 07/07/2010
 *
 * Progetto 140, funzione 3
 * Controllo se al prodotto è presente un codice alternativo e mostro,
 * Sono possibili 3 casi (che corrispondo al numero impostato sul campo flag_tipo_riga) da specifica.
 *
 * ai_row = riga da controllare
 * ab_delete_on_error = cancella la riga in caso di errore
 * ab_read_quan_alt = indica se la quantità da analizzare viene presa da quella alternativa inserita dall'utente
 **/
 
string ls_cod_prodotto, ls_cod_prodotto_alt, ls_des_prodotto, ls_cod_misura, ls_cod_deposito, ls_cod_fornitore, &
		ls_sql
decimal {4} ld_giacenza_stock, ld_quan_acquisto, ld_giacenza_usata, ld_fat_conversione
int li_row, li_rows
datastore lds_store

ld_giacenza_usata = 0
ls_cod_prodotto = this.getitemstring(ai_row, "cod_prodotto")
ls_cod_fornitore = this.getitemstring(ai_row, "cod_fornitore")
ld_quan_acquisto = this.getitemnumber(ai_row, "quan_acquisto")
ls_cod_prodotto_alt = this.getitemstring(ai_row, "cod_prodotto_alt")

// carico il codice alternativo solo se non è stato impostato manualmente dall'operatore
if isnull(ls_cod_prodotto_alt) or ls_cod_prodotto_alt = "" then
	select cod_prodotto_alt
	into :ls_cod_prodotto_alt
	from anag_prodotti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("APICE", "Errore durante il controllo del codice alternativo per il prodotto " + ls_cod_prodotto)
		if ab_delete_on_error then deleterow(ai_row)
		
		return -1
	end if
end if

if isnull(ls_cod_prodotto_alt) or ls_cod_prodotto_alt = "" then
	// CASO 3 della specifica non è assegnato il codice alternativo
	this.setitem(ai_row, "flag_tipo_riga", "3")
	return 0
else
	select des_prodotto, cod_misura_mag, fat_conversione_acq
	into :ls_des_prodotto, :ls_cod_misura, :ld_fat_conversione
	from anag_prodotti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto_alt;
		
	if sqlca.sqlcode <> 0 then return -1
	
	this.setitem(ai_row, "cod_prodotto_alt", ls_cod_prodotto_alt)
	this.setitem(ai_row, "des_prodotto_alt", ls_des_prodotto)
	this.setitem(ai_row, "cod_misura_alt", ls_cod_misura)
	
	// Recupero giacenza dallo stock
	// stefanop 09/02/2012: abilito multideposito
	ls_cod_deposito = dw_depositi.getitemstring(1, "cod_deposito")
	if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
	
		g_mb.warning("Non è stato selezionato nessun deposito. Verrà considerato il primo trovato tra quelli disponibili del fornitore.")
		
		ls_cod_deposito = wf_get_deposito_fornitore(ls_cod_fornitore)
		if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
			tab_1.det_2.dw_depositi.setitem(1, "cod_deposito", ls_cod_deposito)
		end if
		
	end if
	

	if wf_get_giacenza_deposito(ls_cod_deposito, ls_cod_prodotto_alt, ld_giacenza_stock) < 0 then
		g_mb.error("APICE", "Errore durante il controllo della giacenza.~r~nProdotto: " + ls_cod_prodotto_alt + "~r~nDeposito: " + ls_cod_deposito + "~r~nDettaglio: " + sqlca.sqlerrtext)
		if ab_delete_on_error then deleterow(ai_row)
		return -1
	end if
	
	// calcolo lo scarico per altre righe dello stesso codice
	for li_row = 1 to rowcount()
		if li_row <> ai_row and getitemstring(li_row, "cod_prodotto_alt") = ls_cod_prodotto_alt then
			ld_giacenza_usata += getitemnumber(li_row, "quan_acquisto_alt")
		end if
	next
	// ----
		
	if ab_read_quan_alt then
		ld_quan_acquisto = dw_ext_carico_acq_det.getitemnumber(ai_row, "quan_acquisto_alt")
	end if
	
	
	if ld_quan_acquisto <= (ld_giacenza_stock - ld_giacenza_usata) then
		// CASO 1, codice alt presente e giacenza ok
		this.setitem(ai_row, "flag_tipo_riga", "1")
		this.setitem(ai_row, "colore_riga", il_color_green)
		this.setitem(ai_row, "quan_acquisto_alt", ld_quan_acquisto)
	else
		// CASO 2, codice alt presente ma giacenza non sufficiente
		this.setitem(ai_row, "flag_tipo_riga", "2")
		this.setitem(ai_row, "colore_riga", il_color_red)
		this.setitem(ai_row, "quan_acquisto_alt", ld_giacenza_stock - ld_giacenza_usata)
	end if
end if
end event

event type integer ue_controlla_riga_terzista(integer ai_row);/**
 * Controllo se al prodotto è presente una distinta base; in tal caso:
 - ricavo i prodotti/quantità di Primo Livello (ovvero MP)
 - inserisco aggiorno la dw sul folder det_5 (dw_scarico_terzista)

 **/
 
string				ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_cod_deposito, ls_cod_fornitore, ls_sql, ls_errore
decimal {4} 		ld_giacenza_stock, ld_quan_acquisto, ld_giacenza_usata, ld_quan_acquisto_um_mag
decimal{5}		ld_fat_conversione
int					li_row, li_rows, li_anno_commessa, li_anno_ord_acq, li_ret
datastore		lds_store
long				ll_num_commessa, ll_num_ord_acq, ll_prog_riga_ordine_acq



ld_giacenza_usata = 0
ls_cod_prodotto = this.getitemstring(ai_row, "cod_prodotto")
ls_cod_fornitore = this.getitemstring(ai_row, "cod_fornitore")
ld_quan_acquisto = this.getitemnumber(ai_row, "quan_acquisto")
li_anno_commessa = this.getitemnumber(ai_row, "anno_commessa")
ll_num_commessa = this.getitemnumber(ai_row, "num_commessa")
ld_fat_conversione =  this.getitemnumber(ai_row, "fat_conversione")
li_anno_ord_acq = this.getitemnumber(ai_row, "anno_registrazione")
ll_num_ord_acq = this.getitemnumber(ai_row, "num_registrazione")
ll_prog_riga_ordine_acq = this.getitemnumber(ai_row, "prog_riga_ord_acq")

if ld_fat_conversione<> 0 then ld_quan_acquisto_um_mag = ld_quan_acquisto / ld_fat_conversione
ld_quan_acquisto_um_mag = round(ld_quan_acquisto_um_mag, 4)

ls_cod_deposito = dw_depositi.getitemstring(1, "cod_deposito")


if li_anno_commessa>0 and ll_num_commessa>0 and ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	//leggi le materie prime del prodotto della riga e valuta le giacenze nel deposito del terzista
	li_ret = wf_materie_prime_terzista(li_anno_commessa, ll_num_commessa, ls_cod_prodotto, ls_cod_fornitore, ls_cod_deposito, ld_quan_acquisto_um_mag, ls_errore)
	
	if li_ret<0 then
		g_mb.error(ls_errore)
		
	elseif li_ret > 0 then
		g_mb.warning(ls_errore)
		return 0
	end if
	
else
		g_mb.warning("La riga d'ordine "+string(li_anno_ord_acq)+"/"+string(ll_num_ord_acq)+"/"+string(ll_prog_riga_ordine_acq)+" è senza commessa. "+&
							"Non saranno visualizzate eventuali giacenze presso terzista delle materie prime del prodotto " + ls_cod_prodotto + " della riga!")
	return 0
end if




return 0

end event

event type integer ue_aggiorna_riga_terzista(integer ai_row);/**
 * In caso di rimozione riga ordine acquisto e scarico MP da terzista attivato
	aggiorna il folder delle MP
 **/
 
string				ls_cod_prodotto, ls_cod_deposito, ls_cod_fornitore, ls_errore, ls_materia_prima[], ls_versione_prima[]
decimal {4} 		ld_giacenza_stock, ld_quan_acquisto, ld_giacenza_usata, ld_quantita, ld_qta_ordine_commessa, ld_quan_residua_um_mag
dec{5}			ld_fat_conversione
double			ldd_quantita_utilizzo[]
int					li_row, li_rows, li_anno_commessa, li_anno_ord_acq, li_ret
datastore		lds_store
long				ll_num_commessa, ll_num_ord_acq, ll_prog_riga_ordine_acq, ll_index, ll_find
uo_funzioni_1	luo_f1


ld_giacenza_usata = 0
ls_cod_prodotto = this.getitemstring(ai_row, "cod_prodotto")
ls_cod_fornitore = this.getitemstring(ai_row, "cod_fornitore")
ld_quan_acquisto = this.getitemnumber(ai_row, "quan_acquisto")
li_anno_commessa = this.getitemnumber(ai_row, "anno_commessa")
ll_num_commessa = this.getitemnumber(ai_row, "num_commessa")
ld_fat_conversione = this.getitemnumber(ai_row, "fat_conversione")
li_anno_ord_acq = this.getitemnumber(ai_row, "anno_registrazione")
ll_num_ord_acq = this.getitemnumber(ai_row, "num_registrazione")
ll_prog_riga_ordine_acq = this.getitemnumber(ai_row, "prog_riga_ord_acq")

ls_cod_deposito = dw_depositi.getitemstring(1, "cod_deposito")
if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
	ls_cod_deposito = wf_get_deposito_fornitore(ls_cod_fornitore)
	if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
		tab_1.det_2.dw_depositi.setitem(1, "cod_deposito", ls_cod_deposito)
	end if
end if

//solo se esiste una commessa ed il prodotto della riga
if li_anno_commessa>0 and ll_num_commessa>0 and ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	//leggi le materie prime del prodotto della riga e aggiorna il folder delle MP
	
	select quan_ordine
	into :ld_qta_ordine_commessa
	from anag_commesse
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:li_anno_commessa and
				num_commessa=:ll_num_commessa;
	
	select quan_ordinata - quan_arrivata, fat_conversione
	into :ld_quan_residua_um_mag, :ld_fat_conversione
	from det_ord_acq
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_ord_acq and
				num_registrazione=:ll_num_ord_acq and
				prog_riga_ordine_acq=:ll_prog_riga_ordine_acq;
	
	
	luo_f1 = create uo_funzioni_1
	li_ret = luo_f1.uof_trova_mat_prime_varianti(	li_anno_commessa, ll_num_commessa, ls_cod_prodotto, true, &
																ls_materia_prima[], ls_versione_prima[], ldd_quantita_utilizzo[], ls_cod_deposito, ls_errore)
	destroy luo_f1
	
	
	//le quantità fabbisogno vengono ricalcolate in base alla quantità residua della riga ordine
	if ld_qta_ordine_commessa <> 0 and ld_qta_ordine_commessa<>ld_quan_residua_um_mag then
		//ricalcolo quantità fabbisogno
		for ll_index = 1 to upperbound(ls_materia_prima[])
			ldd_quantita_utilizzo[ll_index] = (ld_quan_residua_um_mag / ld_qta_ordine_commessa) * ldd_quantita_utilizzo[ll_index]
		next
	end if
	//--------------------------------------------------------------------
	
	
	for ll_index=1 to upperbound(ls_materia_prima[])
		ll_find = tab_1.det_5.dw_scarico_terzista.find("cod_prodotto='"+ls_materia_prima[ll_index]+"'", 1, tab_1.det_5.dw_scarico_terzista.rowcount())

		if ll_find> 0 then
			ld_quantita = tab_1.det_5.dw_scarico_terzista.getitemdecimal(ll_find, "quantita")
			ld_quantita = ld_quantita - ldd_quantita_utilizzo[ll_index]

			if ld_quantita<0 then ld_quantita = 0
			tab_1.det_5.dw_scarico_terzista.setitem(ll_find, "quantita", ld_quantita)

			if ld_quantita=0 then
				//se hai azzerato il fabbisogno della MP,  rimuovi la riga della MP
				tab_1.det_5.dw_scarico_terzista.deleterow(ll_find)
			end if
		end if
	next

	
	tab_1.det_5.PictureName = ""
	
	if tab_1.det_5.dw_scarico_terzista.rowcount() > 0 then
		for ll_index=1 to tab_1.det_5.dw_scarico_terzista.rowcount()
			if tab_1.det_5.dw_scarico_terzista.getitemdecimal(ll_index, "giacenza") - tab_1.det_5.dw_scarico_terzista.getitemdecimal(ll_index, "quantita") < 0 then
				//c'è ancora qualche riga con giacenza negativa
				tab_1.det_5.PictureName = "DeleteWatch5!"
				exit
			end if
		next
	end if
	
	tab_1.det_5.text = "Scarico da Terzista ("+string(tab_1.det_5.dw_scarico_terzista.rowcount())+")"
end if

return 0

end event

event clicked;setrow(row)

tab_1.det_3.enabled = false

choose case dwo.name
	case "p_arrow"
			if ib_upper_size then
				dw_ext_carico_acq_det.y -= ll_offset_height
				dw_ext_carico_acq_det.height += ll_offset_height
				dw_ordini_acquisto_lista.visible = false
				dw_ext_carico_acq_det.object.p_arrow.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\arrow_down.png"
			else
				dw_ext_carico_acq_det.y += ll_offset_height
				dw_ext_carico_acq_det.height -= ll_offset_height
				dw_ordini_acquisto_lista.visible = true
				dw_ext_carico_acq_det.object.p_arrow.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\arrow_up.png"
			end if
			
			ib_upper_size = not ib_upper_size
	
	case "cb_allinea"
		// APRE LA FINESTRA DEI MOVIMENTI MANUALI, (non da specifica ma molto più veloce)
		
//		string ls_parametro_laped
//		window lw_window
//		
//		select stringa
//		into   :ls_parametro_laped
//		from   parametri_azienda
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_parametro = 'LAP';
//				 
//		if sqlca.sqlcode = 0 then
//			if not isnull(ls_parametro_laped) and ls_parametro_laped <> "" then
//				window_type_open(lw_window, ls_parametro_laped, 0)
//			end if
//		else
//			window_open(w_mov_magazzino_det, 0)
//			event post ue_controlla_riga(row)
//		end if
		
		event post ue_controlla_riga(row, false, false)
	
	case else
		if row > 0 and isvalid(dwo) then
			tab_1.det_3.dw_produzione.triggerevent("ue_retrieve")
		end if
		
end choose


end event

event doubleclicked;// stefanop 07/07/2010
string ls_cod_prodotto_alt
int li_row
dec{4} ld_giacenza_usata

choose case dwo.name
	case "p_arrow"
		return 0
		
	case "cod_prodotto_alt", "des_prodotto_alt", "cod_misura_alt", "quan_acquisto_alt", "t_cod_alt_assente"	
		s_cs_xx.parametri.parametro_dw_1 = this
		s_cs_xx.parametri.parametro_s_1 = dw_depositi.getitemstring(1, "cod_deposito")
		window_open(w_giacenza_deposito_fornitore, 0)
		
		if s_cs_xx.parametri.parametro_b_1 then
			this.setitem(row, "cod_prodotto_alt", s_cs_xx.parametri.parametro_s_1)
			event post ue_controlla_riga(row, false, false)
		end if
	
		setnull(s_cs_xx.parametri.parametro_s_1)
		setnull(s_cs_xx.parametri.parametro_b_1)
		setnull(s_cs_xx.parametri.parametro_dw_1)
		
	case "p_remove"
		if getitemstring(row,"prezzo_modificato") = "S" then
			il_prezzi_modificati --
			if il_prezzi_modificati = 0 then
				cb_mem_prezzo.enabled = false
			end if
		end if	
		
		//se attivato lo scarico Mp da terzista rimuovi le quantita delle MP dal relativo folder
		if tab_1.det_2.rb_attiva_scarico_terzista.checked then
			//questo evento va chiamato come trigger non post (MI RACCOMANDO !!!!!)
			tab_1.det_2.dw_ext_carico_acq_det.event trigger ue_aggiorna_riga_terzista(row)
		end if
		
		this.deleterow(row)
		
end choose
// ----
end event

event itemchanged;choose case dwo.name
	case "prezzo_acquisto"	
		il_prezzi_modificati++
		setitem(getrow(),"prezzo_modificato","S")
		cb_mem_prezzo.enabled = true
		
	case "quan_acquisto"
		event post ue_controlla_riga(row, false, false)
		
	case  "quan_acquisto_alt"
		event post ue_controlla_riga(row, false, true)
	
	case "fat_conversione"
		dec{5} ld_fat_conversione_acq
		dec{4} ld_prezzo_acquisto
		
		ld_fat_conversione_acq = round( dec(data), 5)
		ld_prezzo_acquisto = getitemdecimal(row, "prezzo_acquisto") * ld_fat_conversione_acq

		setitem(row, "prezzo_acquisto_mag", ld_prezzo_acquisto )

end choose
end event

event ue_setup_dw;//Sovrascrivo l'ancestor per riuscire a colorare il background dellA RIGA DEL GREZZO

//N.B. il flag "Extend Ancestor Script va lasciato non attivo!!!

return
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_excel"
		if dw_ext_carico_acq_det.saveas("", Excel!, true) = 1 then
			g_mb.success("Esportazione avvenuta con successo!")
			return
		end if
		
		
end choose
end event

type det_3 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3931
integer height = 1912
long backcolor = 12632256
string text = "Terzisti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_avan_produzione cb_avan_produzione
dw_produzione dw_produzione
end type

on det_3.create
this.cb_avan_produzione=create cb_avan_produzione
this.dw_produzione=create dw_produzione
this.Control[]={this.cb_avan_produzione,&
this.dw_produzione}
end on

on det_3.destroy
destroy(this.cb_avan_produzione)
destroy(this.dw_produzione)
end on

type cb_avan_produzione from commandbutton within det_3
integer x = 3273
integer y = 1736
integer width = 361
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;long ll_row,ll_i


dw_produzione.accepttext()

for ll_i = 1 to tab_1.det_3.dw_produzione.rowcount()
	
	
	if dw_produzione.getitemstring(ll_i, "flag_selezione") = "S" then
	
		ll_row = tab_1.det_4.dw_avan_produzione.insertrow(0)
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "anno_registrazione", tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "anno_registrazione"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "num_registrazione", tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "num_registrazione"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "prog_riga_ord_acq", tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "prog_riga_ord_acq"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "anno_commessa", tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "anno_commessa"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "num_commessa", tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "num_commessa"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "prog_riga", dw_produzione.getitemnumber(ll_i, "avan_produzione_com_prog_riga"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "cod_prodotto_fase", dw_produzione.getitemstring(ll_i, "avan_produzione_com_cod_prodotto"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "cod_versione", dw_produzione.getitemstring(ll_i, "avan_produzione_com_cod_versione"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "cod_reparto", dw_produzione.getitemstring(ll_i, "avan_produzione_com_cod_reparto"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "cod_lavorazione", dw_produzione.getitemstring(ll_i, "avan_produzione_com_cod_lavorazione"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "quan_utilizzo", dw_produzione.getitemnumber(ll_i, "quan_utilizzo"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "cod_prodotto_scarico", dw_produzione.getitemstring(ll_i, "prod_bolle_out_com_cod_prodotto_spedito"))
		tab_1.det_4.dw_avan_produzione.setitem(ll_row, "quan_entrata", tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "quan_acquisto"))

	end if
	
next

end event

type dw_produzione from datawindow within det_3
event ue_retrieve ( )
integer x = 18
integer y = 20
integer width = 3561
integer height = 1560
integer taborder = 190
string title = "none"
string dataobject = "d_carico_acquisti_produzione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();string ls_cod_fornitore,ls_flag_produzione
long ll_anno_commessa,ll_num_commessa,ll_row,ll_i


if tab_1.det_2.rb_attiva_scarico_terzista.checked then
	return
end if


if tab_1.det_2.dw_ext_carico_acq_det.getrow() > 0 then
	
	tab_1.det_2.dw_ext_carico_acq_det.accepttext()
	ls_cod_fornitore = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "cod_fornitore")
	ll_anno_commessa = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "anno_commessa")
	ll_num_commessa  = tab_1.det_2.dw_ext_carico_acq_det.getitemnumber(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "num_commessa")
	ls_flag_produzione = tab_1.det_2.dw_ext_carico_acq_det.getitemstring(tab_1.det_2.dw_ext_carico_acq_det.getrow(), "flag_avan_produzione")
	
	if ls_flag_produzione = "S" and ( not isnull(ll_anno_commessa) and ll_anno_commessa > 0 ) then
		tab_1.det_3.enabled = true
		ll_row = retrieve(s_cs_xx.cod_azienda, ll_anno_commessa, ll_num_commessa, ls_cod_fornitore)
		if ll_row > 0 then
			for ll_i = 1 to ll_row
				setitem(ll_i, "quan_utilizzo", getitemnumber(ll_i, "prod_bolle_out_com_quan_spedita") )
			next
		end if
	end if
			
end if
end event

type det_4 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3931
integer height = 1912
long backcolor = 12632256
string text = "Riassunto Avanz. Prod."
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_avan_produzione dw_avan_produzione
end type

on det_4.create
this.dw_avan_produzione=create dw_avan_produzione
this.Control[]={this.dw_avan_produzione}
end on

on det_4.destroy
destroy(this.dw_avan_produzione)
end on

type dw_avan_produzione from datawindow within det_4
event ue_avan_produzione_com ( )
integer x = 18
integer y = 20
integer width = 3561
integer height = 1560
integer taborder = 200
string title = "none"
string dataobject = "d_carico_acquisti_avan_produzione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_avan_produzione_com();// procedura di avanzamento produzione delle fasi selezionate.


end event

type det_5 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 3931
integer height = 1912
boolean enabled = false
long backcolor = 12632256
string text = "Scarico da Terzista"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_scarico_terzista dw_scarico_terzista
end type

on det_5.create
this.dw_scarico_terzista=create dw_scarico_terzista
this.Control[]={this.dw_scarico_terzista}
end on

on det_5.destroy
destroy(this.dw_scarico_terzista)
end on

type dw_scarico_terzista from datawindow within det_5
integer x = 37
integer y = 132
integer width = 3886
integer height = 1768
integer taborder = 140
string title = "none"
string dataobject = "d_giacenza_prodotti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose case dwo.name
	case "b_reset"
		
		if rowcount()>0 then
			if g_mb.confirm("Resettare la lista di visualizzazione delle giacenze?") then
				reset()
			end if
		end if
end choose
end event

