﻿$PBExportHeader$w_con_ord_acq.srw
$PBExportComments$Finestra Gestione Parametri Ordini Acquisti
forward
global type w_con_ord_acq from w_cs_xx_principale
end type
type dw_con_ord_acq from uo_cs_xx_dw within w_con_ord_acq
end type
end forward

global type w_con_ord_acq from w_cs_xx_principale
integer width = 2359
integer height = 616
string title = "Gestione Parametri Ordini Acquisto"
dw_con_ord_acq dw_con_ord_acq
end type
global w_con_ord_acq w_con_ord_acq

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_con_ord_acq, &
                 "cod_tipo_ord_acq", &
                 sqlca, &
                 "tab_tipi_ord_acq", &
                 "cod_tipo_ord_acq", &
                 "des_tipo_ord_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_ord_acq.set_dw_key("cod_azienda")
dw_con_ord_acq.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end on

on w_con_ord_acq.create
int iCurrent
call super::create
this.dw_con_ord_acq=create dw_con_ord_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_ord_acq
end on

on w_con_ord_acq.destroy
call super::destroy
destroy(this.dw_con_ord_acq)
end on

type dw_con_ord_acq from uo_cs_xx_dw within w_con_ord_acq
integer x = 23
integer y = 20
integer width = 2286
integer height = 460
string dataobject = "d_con_ord_acq"
borderstyle borderstyle = styleraised!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

