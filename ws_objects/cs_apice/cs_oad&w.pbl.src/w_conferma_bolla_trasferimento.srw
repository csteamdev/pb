﻿$PBExportHeader$w_conferma_bolla_trasferimento.srw
forward
global type w_conferma_bolla_trasferimento from w_cs_xx_risposta
end type
type cb_no from commandbutton within w_conferma_bolla_trasferimento
end type
type cb_yes from commandbutton within w_conferma_bolla_trasferimento
end type
type st_1 from statictext within w_conferma_bolla_trasferimento
end type
type cb_annulla from commandbutton within w_conferma_bolla_trasferimento
end type
type cb_conferma from commandbutton within w_conferma_bolla_trasferimento
end type
type dw_1 from uo_std_dw within w_conferma_bolla_trasferimento
end type
end forward

global type w_conferma_bolla_trasferimento from w_cs_xx_risposta
integer width = 3470
integer height = 1320
string title = "Conferma Dati Bolla Trasferimento"
cb_no cb_no
cb_yes cb_yes
st_1 st_1
cb_annulla cb_annulla
cb_conferma cb_conferma
dw_1 dw_1
end type
global w_conferma_bolla_trasferimento w_conferma_bolla_trasferimento

type variables
private:
	string is_cod_deposito
end variables

forward prototypes
public subroutine wf_componi_messaggio ()
public subroutine wf_imposta_layout (integer ai_step)
end prototypes

public subroutine wf_componi_messaggio ();string ls_des_deposito

ls_des_deposito = f_des_tabella("anag_depositi", "cod_deposito='" + is_cod_deposito + "'", "des_deposito")

st_1.text = "L'ordine  prevedeva la consegna presso il deposito " + is_cod_deposito + " - " + ls_des_deposito + ". Vuoi generare il DDT di trasferimento?"
end subroutine

public subroutine wf_imposta_layout (integer ai_step);boolean lb_first

lb_first = (ai_step = 1)

st_1.visible = lb_first
cb_yes.visible = lb_first
cb_no.visible = lb_first

dw_1.visible = not lb_first
cb_conferma.visible = not lb_first
cb_annulla.visible = not lb_first

if lb_first then
	this.height = 408	
else
	height = 1428
end if
end subroutine

on w_conferma_bolla_trasferimento.create
int iCurrent
call super::create
this.cb_no=create cb_no
this.cb_yes=create cb_yes
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_no
this.Control[iCurrent+2]=this.cb_yes
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_conferma
this.Control[iCurrent+6]=this.dw_1
end on

on w_conferma_bolla_trasferimento.destroy
call super::destroy
destroy(this.cb_no)
destroy(this.cb_yes)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;int li_layout

is_cod_deposito = s_cs_xx.parametri.parametro_s_1
li_layout = s_cs_xx.parametri.parametro_i_1

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_i_1)

if isnull(li_layout) or li_layout < 1 then li_layout = 1

wf_imposta_layout(li_layout)
wf_componi_messaggio()

dw_1.insertrow(0)
end event

event resize;/* TOLTO ANCESTOR SCRIPT **/

st_1.move(20,20)
st_1.width = newwidth - 20
cb_yes.move(newwidth - cb_yes.width - 20, newheight - cb_yes.height - 20)
cb_no.move(cb_yes.x - cb_no.width - 20, cb_yes.y)

dw_1.move(20,20)
cb_conferma.move(newwidth - cb_conferma.width - 20, dw_1.y + dw_1.height + 20)
cb_annulla.move(cb_conferma.x - cb_annulla.width - 20, cb_conferma.y)
end event

event pc_setddlb;call super::pc_setddlb;string ls_cod_cliente

//recupero cliente per fare la bolla dai parametri vendite
select cod_cliente_bol_trasf
into :ls_cod_cliente
from con_vendite;

if sqlca.sqlcode <> 0 or isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
	g_mb.error("Non è impostato nessun cliente per la generazione di bolle di traferimento.~r~nControllare i Parametri Vendita")
end if
// ----

f_po_loaddddw_dw(dw_1, &
					  "cod_destinazione", &
					  sqlca, &
					  "anag_des_clienti", &
					  "cod_des_cliente", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_imballo", &
					  sqlca, &
					  "tab_imballi", &
					  "cod_imballo", &
					  "des_imballo", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_vettore", &
					  sqlca, &
					  "anag_vettori", &
					  "cod_vettore", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_inoltro", &
					  sqlca, &
					  "anag_vettori", &
					  "cod_vettore", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_mezzo", &
					  sqlca, &
					  "tab_mezzi", &
					  "cod_mezzo", &
					  "des_mezzo", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_porto", &
					  sqlca, &
					  "tab_porti", &
					  "cod_porto", &
					  "des_porto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_resa", &
					  sqlca, &
					  "tab_rese", &
					  "cod_resa", &
					  "des_resa", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_causale", &
					  sqlca, &
					  "tab_causali_trasp", &
					  "cod_causale", &
					  "des_causale", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_1, &
					  "cod_tipo_bolla", &
					  sqlca, &
					  "tab_tipi_bol_ven", &
					  "cod_tipo_bol_ven", &
					  "des_tipo_bol_ven", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )
end event

type cb_no from commandbutton within w_conferma_bolla_trasferimento
integer x = 2583
integer y = 180
integer width = 402
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "No"
end type

event clicked;close(parent)
end event

type cb_yes from commandbutton within w_conferma_bolla_trasferimento
integer x = 2994
integer y = 180
integer width = 402
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Si"
end type

event clicked;wf_imposta_layout(2)
end event

type st_1 from statictext within w_conferma_bolla_trasferimento
integer x = 23
integer y = 20
integer width = 3383
integer height = 140
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_conferma_bolla_trasferimento
integer x = 2537
integer y = 1100
integer width = 402
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_conferma from commandbutton within w_conferma_bolla_trasferimento
integer x = 2994
integer y = 1100
integer width = 402
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;s_dati_bolla_trasferimento lstr_data

lstr_data.cod_tipo_bol_ven = dw_1.getitemstring(1,"cod_tipo_bolla")

if isnull(lstr_data.cod_tipo_bol_ven) then
	g_mb.warning("ATTENZIONE!!! L'indicazione del tipo bolla è OBBLIGATORIO. ")
	return
end if

lstr_data.cod_causale =dw_1.getitemstring(1,"cod_causale")
lstr_data.aspetto_beni =dw_1.getitemstring(1,"aspetto_beni")
lstr_data.cod_mezzo =dw_1.getitemstring(1,"cod_mezzo")
lstr_data.cod_porto =dw_1.getitemstring(1,"cod_porto")
lstr_data.cod_resa =dw_1.getitemstring(1,"cod_resa")
lstr_data.cod_vettore =dw_1.getitemstring(1,"cod_vettore")
lstr_data.cod_inoltro =dw_1.getitemstring(1,"cod_inoltro")
lstr_data.cod_imballo =dw_1.getitemstring(1,"cod_imballo")
lstr_data.cod_destinazione =dw_1.getitemstring(1,"cod_destinazione")
lstr_data.num_colli =dw_1.getitemnumber(1,"num_colli")
lstr_data.peso_netto =dw_1.getitemnumber(1,"peso_netto")
lstr_data.peso_lordo =dw_1.getitemnumber(1,"peso_lordo")
lstr_data.data_inizio_trasp =dw_1.getitemdatetime(1,"data_inizio_trasp")
lstr_data.ora_inizio_trasp =dw_1.getitemtime(1,"ora_inizio_trasp")
lstr_data.rag_soc_1 =dw_1.getitemstring(1,"rag_soc_1")
lstr_data.rag_soc_2 =dw_1.getitemstring(1,"rag_soc_2")
lstr_data.indirizzo =dw_1.getitemstring(1,"indirizzo")
lstr_data.localita =dw_1.getitemstring(1,"localita")
lstr_data.frazione =dw_1.getitemstring(1,"frazione")
lstr_data.provincia =dw_1.getitemstring(1,"provincia")
lstr_data.cap =dw_1.getitemstring(1,"cap")
lstr_data.nota_piede =dw_1.getitemstring(1,"note_piede")
lstr_data.num_ord_cliente =dw_1.getitemstring(1,"num_ord_cliente")
lstr_data.data_ord_cliente =dw_1.getitemdatetime(1,"data_ord_cliente")

closeWithReturn(parent, lstr_data)
end event

type dw_1 from uo_std_dw within w_conferma_bolla_trasferimento
integer x = 23
integer y = 20
integer width = 3406
integer height = 1080
integer taborder = 10
string dataobject = "d_evasione_ddt_trasferimento"
boolean border = false
end type

