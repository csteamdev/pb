﻿$PBExportHeader$w_det_ord_acq.srw
$PBExportComments$Finestra Dettaglio Ordini di Acquisto
forward
global type w_det_ord_acq from w_cs_xx_principale
end type
type st_1 from statictext within w_det_ord_acq
end type
type pb_prod_view from cb_listview within w_det_ord_acq
end type
type cb_annulla_commessa from commandbutton within w_det_ord_acq
end type
type cb_blocca from commandbutton within w_det_ord_acq
end type
type cb_c_industriale from commandbutton within w_det_ord_acq
end type
type cb_corrispondenze from commandbutton within w_det_ord_acq
end type
type cb_prezzo from commandbutton within w_det_ord_acq
end type
type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_ord_acq
end type
type cb_sblocca from commandbutton within w_det_ord_acq
end type
type cb_sconti from commandbutton within w_det_ord_acq
end type
type st_2 from statictext within w_det_ord_acq
end type
type dw_det_ord_acq_lista from uo_cs_xx_dw within w_det_ord_acq
end type
type dw_folder from u_folder within w_det_ord_acq
end type
type dw_det_ord_acq_det_1 from uo_cs_xx_dw within w_det_ord_acq
end type
type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_ord_acq
end type
type dw_det_ord_acq_det_3 from uo_cs_xx_dw within w_det_ord_acq
end type
type dw_det_ord_acq_det_2 from uo_cs_xx_dw within w_det_ord_acq
end type
end forward

global type w_det_ord_acq from w_cs_xx_principale
integer width = 4667
integer height = 2204
string title = "Righe Ordini Acquisto"
boolean minbox = false
event ue_imposta_colori ( )
event ue_maximize ( )
st_1 st_1
pb_prod_view pb_prod_view
cb_annulla_commessa cb_annulla_commessa
cb_blocca cb_blocca
cb_c_industriale cb_c_industriale
cb_corrispondenze cb_corrispondenze
cb_prezzo cb_prezzo
cb_prodotti_note_ricerca cb_prodotti_note_ricerca
cb_sblocca cb_sblocca
cb_sconti cb_sconti
st_2 st_2
dw_det_ord_acq_lista dw_det_ord_acq_lista
dw_folder dw_folder
dw_det_ord_acq_det_1 dw_det_ord_acq_det_1
dw_documenti dw_documenti
dw_det_ord_acq_det_3 dw_det_ord_acq_det_3
dw_det_ord_acq_det_2 dw_det_ord_acq_det_2
end type
global w_det_ord_acq w_det_ord_acq

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_supervisore=false

string is_NDA="N"

boolean		ib_NPA = false

private:
	// parametro aziendale: Prezzo Listini Fornitori
	boolean ib_PLF = true


end variables

forward prototypes
public subroutine wf_tasti_funzione (string fs_nome_colonna)
public subroutine wf_proteggi_colonne (double fd_quan_arrivata)
public function integer wf_modifica_qta ()
public function integer wf_duplica_riga (long fl_row)
public function integer wf_copia_riga (long fl_row, long fl_newrow)
public function any wf_get_values (datawindow fdw_data, long fl_row, long fl_col_number, ref string fs_msg)
public subroutine wf_componi_nota (long fl_row, string fs_data, string fs_nome_colonna)
public function integer wf_prezzo_um (string fs_cod_misura, decimal fd_prezzo_acquisto, decimal fd_fat_conversione, decimal fd_quan_ordinata)
public function integer wf_popola_imballo2 (ref string as_errore)
public function integer wf_aggiorna_imballi (string as_cod_prodotto, long al_row)
public subroutine wf_set_disegno (long al_row, string as_cod_prodotto)
end prototypes

event ue_imposta_colori();string ls_modify ,ls_flag_tipo_det_riga  

select flag_tipo_des_riga  
into   :ls_flag_tipo_det_riga  
from   con_ord_acq  
where  cod_azienda = :s_cs_xx.cod_azienda;

if ls_flag_tipo_det_riga  = "S" and sqlca.sqlcode = 0 then
	ls_modify = dw_det_ord_acq_lista.Modify("des_prodotto.color='0~tif(des_prodotto <> anag_prodotti_des_prodotto or isnull(des_prodotto), 255, 0)'")
	if ls_modify <> "" then
		messagebox("APICE", ls_modify)
	end if
end if


end event

event ue_maximize();if is_NDA="S" then
	move(250,0)
	this.height = w_cs_xx_mdi.mdi_1.height - 100
	this.width = w_cs_xx_mdi.mdi_1.width - 300
end if
end event

public subroutine wf_tasti_funzione (string fs_nome_colonna);st_2.text = ""
choose case fs_nome_colonna
	case "cod_prod_fornitore"
		st_2.text = "SHIFT - F1 = Memo Codice Fornitore"
	case "cod_prodotto"
		st_2.text = "SHIFT - F1 = Ricerca Prodotto"
	case "prezzo_acquisto"
		st_2.text = "SHIFT - F1 = Storico Acq.  SHIFT - F2 = Memo Prezzo"
end choose

end subroutine

public subroutine wf_proteggi_colonne (double fd_quan_arrivata);dw_det_ord_acq_lista.setredraw(false)
dw_det_ord_acq_det_1.setredraw(false)
if fd_quan_arrivata > 0 then
	//*******claudia dovrei permettere la modifica
//	dw_det_ord_acq_lista.object.quan_ordinata.background.color=12632256
//	dw_det_ord_acq_lista.object.quan_ordinata.protect = 1
	
	dw_det_ord_acq_lista.object.cod_prodotto.background.color=12632256
	dw_det_ord_acq_lista.object.cod_prodotto.protect = 1
	
	dw_det_ord_acq_det_1.object.cod_prodotto.background.color=12632256
	dw_det_ord_acq_det_1.object.cod_prodotto.protect = 1
	
//	dw_det_ord_acq_det_1.object.quan_ordinata.background.color=12632256
//	dw_det_ord_acq_det_1.object.quan_ordinata.protect = 1
//
	dw_det_ord_acq_det_1.object.fat_conversione.background.color=12632256
	dw_det_ord_acq_det_1.object.fat_conversione.protect = 1

	dw_det_ord_acq_det_1.object.cod_misura.background.color=12632256
	dw_det_ord_acq_det_1.object.cod_misura.protect = 1
else
//	dw_det_ord_acq_lista.object.quan_ordinata.background.color=16777215
//	dw_det_ord_acq_lista.object.quan_ordinata.protect = 0
//	
	dw_det_ord_acq_lista.object.cod_prodotto.background.color=16777215
	dw_det_ord_acq_lista.object.cod_prodotto.protect = 0
	
	dw_det_ord_acq_det_1.object.cod_prodotto.background.color=16777215
	dw_det_ord_acq_det_1.object.cod_prodotto.protect = 0
	
//	dw_det_ord_acq_det_1.object.quan_ordinata.background.color=16777215
//	dw_det_ord_acq_det_1.object.quan_ordinata.protect = 0
//
	dw_det_ord_acq_det_1.object.fat_conversione.background.color=16777215
	dw_det_ord_acq_det_1.object.fat_conversione.protect = 0

	dw_det_ord_acq_det_1.object.cod_misura.background.color=16777215
	dw_det_ord_acq_det_1.object.cod_misura.protect = 0
end if
dw_det_ord_acq_lista.setredraw(true)
dw_det_ord_acq_det_1.setredraw(true)

return
end subroutine

public function integer wf_modifica_qta ();string ls_flag_abilita_quan_mag, ls_flag_abilita_prezzo_mag, ls_flag_abilita_quan_um, ls_flag_abilita_prezzo_um, &
       ls_colonna_quantita, ls_colonna_prezzo, ls_cod_misura_prodotto, ls_cod_misura_ven, ls_cod_prodotto


ls_colonna_quantita = "quan_ordinata"
ls_colonna_prezzo = "prezzo_vacquisto"
dw_det_ord_acq_det_1.modify(ls_colonna_quantita + ".protect=0")
dw_det_ord_acq_det_1.modify(ls_colonna_quantita + ".background.color='16777215'")


return 1
end function

public function integer wf_duplica_riga (long fl_row);long ll_prog_riga_ordine_acq, ll_new

ll_prog_riga_ordine_acq = dw_det_ord_acq_lista.getitemnumber(fl_row, "prog_riga_ordine_acq")

if not g_mb.confirm("APICE", "Duplicare la riga "+string(ll_prog_riga_ordine_acq)+"?", 1) then
	return 0
end if

//inserisci la nuova riga
dw_det_ord_acq_lista.triggerevent("pcd_new")
ll_new = dw_det_ord_acq_lista.getrow()

//trasferisci i valori
if wf_copia_riga(fl_row, ll_new) < 0 then
	//messaggio già dato
	return -1
end if

//salva
dw_det_ord_acq_lista.triggerevent("pcd_save")

//g_mb.show("riga duplicata! NUOVA riga:"+string(ll_new))

return 1
end function

public function integer wf_copia_riga (long fl_row, long fl_newrow);long ll_index, ll_inizio, ll_tot, ll_null
string ls_msg, ls_count

ll_inizio = 5	//parti dalla colonna cod_tipo_det_acq (#5)
ls_count = dw_det_ord_acq_lista.Describe("DataWindow.Column.Count")

ll_tot = long(ls_count)

for ll_index = ll_inizio to ll_tot
	ls_msg = ""
	
	dw_det_ord_acq_lista.getcolumnname( )
	
	try
		dw_det_ord_acq_lista.setitem(fl_newrow, ll_index, wf_get_values(dw_det_ord_acq_lista, fl_row, ll_index, ls_msg))
		
		if ls_msg<>"" then
			g_mb.error("APICE", ls_msg)
			return -1
		end if
		
	catch (throwable err)
		if ls_msg<>"" then
			g_mb.error("APICE", ls_msg)			
		else
			g_mb.error("APICE", "Errore in acquisizione dato: colonna: "+string(ll_index))
		end if
		
		return -1
		
	end try
	
	//alla fine del ciclo annulla anno e numero commessa
	if ll_index=ll_tot then
		setnull(ll_null)
		dw_det_ord_acq_lista.setitem(fl_newrow, "anno_commessa",ll_null)
		dw_det_ord_acq_lista.setitem(fl_newrow, "num_commessa",ll_null)
	end if
	
next

return 1
end function

public function any wf_get_values (datawindow fdw_data, long fl_row, long fl_col_number, ref string fs_msg);string ls_coltype, ls_column_name

fs_msg = ""
ls_column_name = "#"+string(fl_col_number)
ls_coltype = fdw_data.Describe(ls_column_name + ".ColType")

choose case left(ls_coltype, 5)
	case "char("
		return fdw_data.getitemstring(fl_row, fl_col_number)
	
	case	"numbe", "decim", "long", "int"
		return fdw_data.getitemNumber(fl_row, fl_col_number)
		
	case	"date", "datet", "times", "time"
		return fdw_data.getitemdatetime(fl_row, fl_col_number)
		
	case "!"
		fs_msg = "Impossibile trovare il tipo per la colonna " + ls_column_name
		return false
		
end choose
end function

public subroutine wf_componi_nota (long fl_row, string fs_data, string fs_nome_colonna);string			ls_nota, ls_qta, ls_cod_prodotto, ls_um, ls_cod_veloce, ls_verniciatura, ls_cod_verniciatura, ls_imballo1, ls_imballo2, ls_cod_classe_merceol, &
				ls_verniciatura_dw, ls_old, ls_like
integer		ll_pos_verniciatura, ll_pos_cod_classe_merc
dec{4}		ld_pezzi_collo, ld_qta_riga
dec{5}		ld_fat_conv_pezzi, ld_fat_conversione_acq
long			ll_count
date			ldt_oggi
s_cs_xx_parametri	ls_cs_xx_parametri

if is_NDA<>"S" then return

if fl_row>0 then
else
	return
end if

ls_nota = ""

//posizione del carattere corrispondente al codice verniciatura nella stringa cod_prodotto
ll_pos_verniciatura = 6

//posizione del carattere codice classe merceologica
ll_pos_cod_classe_merc = 2

choose case fs_nome_colonna
	case "cod_prodotto"
		ld_qta_riga = dw_det_ord_acq_det_1.getitemdecimal(fl_row,"quan_ordinata")
		ls_qta = string(ld_qta_riga)
		ls_cod_prodotto = fs_data
//		ls_imballo1 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo1")
//		ls_imballo2 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo2")
		
		//se hai cambiato prodotto, allora rileggi i codici imballo 1 e 2 da anag_prodotti
		ls_old = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_prodotto")
		if isnull(ls_old) then ls_old=""
		
		if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
			if ls_old<>ls_cod_prodotto then
				//hai cambiato prodotto o prima non c'era ...
				
				select		pezzi_collo,
							cod_imballo
				into		:ld_pezzi_collo,
							:ls_imballo2
				from anag_prodotti
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_prodotto=:ls_cod_prodotto;
				
				ls_imballo1 = string(long(ld_pezzi_collo))
				if isnull(ls_imballo1) then ls_imballo1 = ""
				
			else
				//il prodotto non è cambiato, lascio i valori già presenti
				ls_imballo1 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo1")
				ls_imballo2 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo2")
			end if
		else
			//prodotto vuoto
			ls_imballo1 = ""
			ls_imballo2 = ""
		end if
		
	case "quan_ordinata"
		ls_qta = fs_data
		ls_old = ls_qta
		guo_functions.uof_replace_string(ls_old, ".", ",")
		ld_qta_riga = dec(ls_old)
		
		ls_cod_prodotto = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_prodotto")
		ls_imballo1 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo1")
		ls_imballo2 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo2")
		
		//##############################################################
		if ls_imballo1="" or isnull(ls_imballo1) then
			//leggo da anag_prodotti: pezzi per collo
			select		pezzi_collo
			into		:ld_pezzi_collo
			from anag_prodotti
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_prodotto=:ls_cod_prodotto;
			
			ls_imballo1 = string(long(ld_pezzi_collo))
			if isnull(ls_imballo1) then ls_imballo1 = ""
			
		end if
		if ls_imballo2="" or isnull(ls_imballo2) then
			//leggo da anag_prodotti: tipo imballo
			select		cod_imballo
			into		:ls_imballo2
			from anag_prodotti
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_prodotto=:ls_cod_prodotto;
		end if
		//##############################################################
		
	
	case "cod_imballo1"
		ld_qta_riga = dw_det_ord_acq_det_1.getitemdecimal(fl_row,"quan_ordinata")
		ls_qta = string(ld_qta_riga)
		ls_cod_prodotto =dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_prodotto")
		ls_imballo1 = fs_data
		ls_imballo2 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo2")
	
	case "cod_imballo2"
		ld_qta_riga = dw_det_ord_acq_det_1.getitemdecimal(fl_row,"quan_ordinata")
		ls_qta = string(ld_qta_riga)
		ls_cod_prodotto =dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_prodotto")
		ls_imballo1 = dw_det_ord_acq_det_1.getitemstring(fl_row,"cod_imballo1")
		ls_imballo2 = fs_data
		
end choose

//if lb_leggi_des_imballo and ls_imballo2<>"" and not isnull(ls_imballo2) then
if ls_imballo2<>"" and not isnull(ls_imballo2) then
	ls_imballo2 = f_des_tabella("tab_imballi","cod_imballo = '" +  ls_imballo2 +"'", "des_imballo")
end if


//###################################################################################
//Donato 03/04/2013 come da SR Modifiche_acquisti, l'autocomposizione deve attivarsi sempre, anche se la seconda cifra del codice prodotto non è "2"
//quindo commento l'IF seguente

////l'autocomposizione della nota si deve attivare solo se la 2nda cifra del cod_prodotto è 2
//if mid(ls_cod_prodotto, 2, 1) = "2" then
//else
//	return
//end if

//fine modifica
//###################################################################################



//se possibile esprimere in unita PEZZI con il fatt_conversione_pezzi
if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	
	select		fat_conversione_acq, fat_conversione_pezzi
	into		:ld_fat_conversione_acq, :ld_fat_conv_pezzi
	from anag_prodotti
	where		cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
	
	if ld_fat_conv_pezzi>0 then
		ld_qta_riga = (ld_qta_riga * ld_fat_conversione_acq) / ld_fat_conv_pezzi
		ls_qta = string(ld_qta_riga, "####0.0####")
	end if
end if


//QTA --------------------------------------
if isnull(ls_qta) then ls_qta=""
if isnumber(ls_qta) then ls_qta = string(long(ls_qta))

ls_nota += ls_qta
//--------------------------------------------

//UM ---------------------------------------
if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then
	ls_um = ""
else
	
	if ld_fat_conv_pezzi>0 then
		ls_um = "PZ"
	else
		ls_um = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")
		if isnull(ls_um) then ls_um=""
	end if
end if

//se prima c'era qualcosa oppure ora hai qualcosa da mettere allora metti prima lo spazio
if ls_nota<>"" or ls_um<>"" then ls_nota+=" "
ls_nota += ls_um
//------------------------------------------

//VERNICIATURA ------------------------
ls_cod_classe_merceol = mid(ls_cod_prodotto, 2, 1)

if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then
	ls_verniciatura = ""
	setnull( ls_verniciatura_dw )
else
	ls_cod_veloce = mid(ls_cod_prodotto, ll_pos_verniciatura, 1)
	if isnull(ls_cod_veloce) or ls_cod_veloce="" then
		ls_verniciatura = ""
		setnull( ls_verniciatura_dw )
	else
		//se c'è + di una verniciatura apro un pop-up per permettere all'utente di scegliere
		ls_like = "%" + ls_cod_classe_merceol + "%"
		ldt_oggi = today()
		
		select count(*)
		into :ll_count
		from tab_verniciatura
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_veloce=:ls_cod_veloce and
					cod_classe_merceolo like :ls_like and
					((flag_blocco<>'S') or (flag_blocco = 'S' and data_blocco>:ldt_oggi));
		
		if ll_count=1 then
			//una sola verniciatura, metto quella
			ls_cod_verniciatura = f_des_tabella("tab_verniciatura","cod_veloce='" +  ls_cod_veloce +"' and cod_classe_merceolo like '%"+ls_cod_classe_merceol+"%'", "cod_verniciatura")
			
		elseif ll_count>1 then
			//più verniciature corrispondenti, apro pop-up per far scegliere ...
			ls_cs_xx_parametri.parametro_s_1_a[1] = ls_cod_veloce
			ls_cs_xx_parametri.parametro_s_1_a[2] = ls_like
			
			openwithparm(w_sel_verniciature_ord_acq, ls_cs_xx_parametri)
			
			ls_cod_verniciatura = message.stringparm
			
		else
			ls_cod_verniciatura = ""
		end if
		//ls_cod_verniciatura = f_des_tabella("tab_verniciatura","cod_veloce='" +  ls_cod_veloce +"' and cod_classe_merceolo like '%"+ls_cod_classe_merceol+"%'", "cod_verniciatura")
		//---------------------------------------------------------
		
		if isnull(ls_cod_verniciatura) or ls_cod_verniciatura="" then
			ls_verniciatura = ""
			setnull( ls_verniciatura_dw )
		else
			ls_verniciatura = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_verniciatura +"'", "des_prodotto")
			if isnull(ls_verniciatura) then ls_verniciatura=""
			
			ls_verniciatura_dw = ls_cod_verniciatura
		end if
		
	end if
end if

//memorizzo nella det_ord_ven il codice verniciatura
dw_det_ord_acq_det_1.setitem(fl_row, "cod_verniciatura", ls_verniciatura_dw)
//------------------------------------------


//se prima c'era qualcosa oppure ora hai qualcosa da mettere allora metti prima lo spazio
if ls_nota<>"" or ls_verniciatura<>"" then ls_nota+=" "
ls_nota += ls_verniciatura
//------------------------------------------

//IMBALLO1
if ls_imballo1="" or isnull(ls_imballo1) or ls_imballo1="0" then
	ls_imballo1 = ""
else
	ls_imballo1 = "Imballo a "+ls_imballo1
end if

//se prima c'era qualcosa oppure ora hai qualcosa da mettere allora metti prima lo spazio
if ls_nota<>"" or ls_imballo1<>"" then ls_nota+=" "
ls_nota += ls_imballo1
//------------------------------------------

//IMBALLO2
if ls_imballo2="" or isnull(ls_imballo2) then
	ls_imballo2 = ""
end if

//se prima c'era qualcosa oppure ora hai qualcosa da mettere allora metti prima lo spazio
if ls_nota<>"" or ls_imballo2<>"" then ls_nota+=" "
ls_nota += ls_imballo2
//------------------------------------------

ls_nota = trim(ls_nota)
dw_det_ord_acq_det_1.setitem(fl_row, "nota_dettaglio", ls_nota)
end subroutine

public function integer wf_prezzo_um (string fs_cod_misura, decimal fd_prezzo_acquisto, decimal fd_fat_conversione, decimal fd_quan_ordinata);
dw_det_ord_acq_det_1.accepttext()

s_cs_xx.parametri.parametro_s_1 = fs_cod_misura //dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(),"cod_misura")
s_cs_xx.parametri.parametro_d_1 = fd_prezzo_acquisto //dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"prezzo_acquisto")
s_cs_xx.parametri.parametro_d_2 = fd_fat_conversione //dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"fat_conversione")
s_cs_xx.parametri.parametro_d_3 = fd_quan_ordinata //dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"quan_ordinata")

window_open(w_prezzo_um, 0)

if s_cs_xx.parametri.parametro_d_1 <> 0 then
   dw_det_ord_acq_det_1.setitem(dw_det_ord_acq_det_1.getrow(), "prezzo_acquisto", s_cs_xx.parametri.parametro_d_1)
end if

if s_cs_xx.parametri.parametro_d_3 <> 0 then
   dw_det_ord_acq_det_1.setitem(dw_det_ord_acq_det_1.getrow(), "quan_ordinata", s_cs_xx.parametri.parametro_d_3)
end if

return 1
end function

public function integer wf_popola_imballo2 (ref string as_errore);
datastore			lds_data
string					ls_sql, ls_valore, ls_cod_imballo, ls_des_imballo, ls_imballo2
long					ll_tot, ll_index

ls_sql = 	"select cod_imballo, des_imballo "+&
			"from tab_imballi "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + "))"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot<0 then
	return -1
end if

if ll_tot=0 then return 0

ls_imballo2 = ""
for ll_index=1 to ll_tot
	ls_cod_imballo = lds_data.getitemstring(ll_index, 1)
	ls_des_imballo = lds_data.getitemstring(ll_index, 2)
	
	//tolgo eventuale carattere "/" dalla descrizione che da fastidio nella composizione della dropdownlistbox ...
	guo_functions.uof_replace_string(ls_des_imballo, "/", "-")
	
	ls_valore = ls_des_imballo + "~t" + ls_cod_imballo
	
	ls_imballo2 += ls_valore
	
	if ll_index < ll_tot then ls_imballo2 += "/"
	
next

dw_det_ord_acq_lista.modify("cod_imballo2.Values='" + ls_imballo2 + "'")
dw_det_ord_acq_lista.modify("cod_imballo2.ddlb.VScrollBar='Yes'")


return 0
end function

public function integer wf_aggiorna_imballi (string as_cod_prodotto, long al_row);long		ll_pezzi_collo
string		ls_imballo1, ls_imballo2

		
if as_cod_prodotto="" or isnull(as_cod_prodotto) then return 0

//aggiorna anag_prodotti
ls_imballo1 = dw_det_ord_acq_lista.getitemstring(al_row, "cod_imballo1")
ls_imballo2 = dw_det_ord_acq_lista.getitemstring(al_row, "cod_imballo2")


if ls_imballo1<>"" and not isnull(ls_imballo1) then
	
	ll_pezzi_collo = long(ls_imballo1)
	
	if ll_pezzi_collo > 0 then
	
		update anag_prodotti
		set pezzi_collo=:ll_pezzi_collo
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:as_cod_prodotto;
		
		//la gestione dell'errore è omessa volutamente, in quanto non è un update critico
		
	end if
end if

//------------------------------

if ls_imballo2<>"" and not isnull(ls_imballo2) then
	
	update anag_prodotti
	set cod_imballo=:ls_imballo2
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto;
	
	//la gestione dell'errore è omessa volutamente, in quanto non è un update critico
	
end if

end function

public subroutine wf_set_disegno (long al_row, string as_cod_prodotto);string					ls_cod_disegno, ls_rev_disegno

if as_cod_prodotto = "" or isnull(as_cod_prodotto) then
	ls_cod_disegno = ""
	ls_rev_disegno = ""
else
	select cod_disegno, rev_disegno
	into :ls_cod_disegno, :ls_rev_disegno
	from anag_prodotti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :as_cod_prodotto;
	
	if isnull(ls_cod_disegno) then ls_cod_disegno = ""
	if isnull(ls_rev_disegno) then ls_rev_disegno = ""
end if

dw_det_ord_acq_lista.setitem(al_row, "cod_disegno", ls_cod_disegno)
dw_det_ord_acq_lista.setitem(al_row, "rev_disegno", ls_rev_disegno)

return
end subroutine

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_ord_acq_lista, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
f_po_loaddddw_dw(	dw_det_ord_acq_lista, &
                					"cod_reparto", &
                 					sqlca, &
                 					"anag_reparti", &
                					"cod_reparto", &
                 					"des_reparto", &
                 					"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_mrp='S' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_det_ord_acq_det_1, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_det_ord_acq_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_det_ord_acq_det_3, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(	dw_det_ord_acq_det_3, &
                					"cod_reparto", &
                 					sqlca, &
                 					"anag_reparti", &
                					"cod_reparto", &
                 					"des_reparto", &
                 					"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

if is_NDA="S" then
	

	f_PO_LoadDDDW_DW(dw_det_ord_acq_lista,"cod_verniciatura",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer "+&
					  																				"from tab_cat_mer_tabelle "+&
																									  "where cod_azienda = '" +s_cs_xx.cod_azienda + "' and "+&
												  						" nome_tabella = 'tab_verniciatura') and "+&
																		  "cod_prodotto in (select cod_verniciatura "+&
																		  "from tab_verniciatura "+&
																		  "where cod_azienda = '" +s_cs_xx.cod_azienda+"') " )	
end if
end event

event pc_setwindow;call super::pc_setwindow;string				ls_errore, ls_flag_supervisore



dw_det_ord_acq_lista.set_dw_key("cod_azienda")
dw_det_ord_acq_lista.set_dw_key("anno_registrazione")
dw_det_ord_acq_lista.set_dw_key("num_registrazione")

dw_det_ord_acq_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)
												
dw_det_ord_acq_det_1.set_dw_options(sqlca, &
                                    dw_det_ord_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_det_ord_acq_det_2.set_dw_options(sqlca, &
                                    dw_det_ord_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_det_ord_acq_det_3.set_dw_options(sqlca, &
                                    dw_det_ord_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

windowobject lw_oggetti[], lw_vuoto[]

lw_oggetti[1] = dw_det_ord_acq_det_2
lw_oggetti[2] = st_2
dw_folder.fu_assigntab(2, "Note", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_det_ord_acq_det_3
lw_oggetti[2] = st_2
dw_folder.fu_assigntab(3, "Analitica", lw_oggetti[])

//eliminata la gestione su cb_prodotti_ricerca e pb_prod_view
//pulsante cb_prodotti_ricerca direttamente si dw
lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_det_ord_acq_det_1
lw_oggetti[2] = cb_sconti
lw_oggetti[3] = cb_prezzo
lw_oggetti[4] = cb_blocca
lw_oggetti[5] = cb_sblocca
lw_oggetti[6] = cb_c_industriale
lw_oggetti[7] = cb_corrispondenze
lw_oggetti[8] = st_2
lw_oggetti[9] = dw_documenti
dw_folder.fu_assigntab(1, "Dettaglio", lw_oggetti[])

dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_documenti.uof_set_management("ORDACQ", dw_det_ord_acq_lista)
dw_documenti.settransobject(sqlca)
dw_documenti.uof_enabled_delete_blob( )

iuo_dw_main=dw_det_ord_acq_lista
cb_prezzo.enabled=false
cb_blocca.enabled=false
cb_sblocca.enabled=false
cb_c_industriale.enabled=false
cb_corrispondenze.enabled = false


guo_functions.uof_get_parametro("NPA", ib_NPA)
if isnull(ib_NPA) then ib_NPA = false


// Parametro Prezzo Listino Fornitore
guo_functions.uof_get_parametro_azienda("PLF", ib_PLF)
if isnull(ib_PLF) then ib_PLF = true

// parametro per attivare l'autocomposizione della nota dettaglio
//fatto per ptenda
//	qta+um+des_verniciatura+imballo
//N.B. la des verniciatura viene letta dal rispettivo codice presente nella 6° cifra del cod prodotto 

select flag
into   :is_NDA
from   parametri_azienda
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='NDA' and flag_parametro='F';

if sqlca.sqlcode < 0 or isnull(is_NDA) or is_NDA="" or (is_NDA<>"N" and is_NDA<>"S") then
	is_NDA = "N"
end if
//tale parametro agisce anche sulla visibilità dei campi:
//cod_:imballo1, cod_imballo2, e nota dettaglio sulla lista
if is_NDA="S" then
	dw_det_ord_acq_lista.object.nota_dettaglio.visible = 1
	dw_det_ord_acq_lista.object.cod_imballo2.visible = 1
	dw_det_ord_acq_lista.object.cod_imballo1.visible = 1
	
	dw_det_ord_acq_lista.object.cod_iva.visible = 0
	dw_det_ord_acq_lista.object.data_consegna.visible = 0
	
	dw_det_ord_acq_lista.object.cod_verniciatura.visible = 1
	dw_det_ord_acq_lista.object.cod_verniciatura_t.visible = 1
	
	// stefanop: 17/04/2012: Sistemato taborder, segnalazione da Paolo Brunazzetto
	//-------------------------------------------------------------------------------
	dw_det_ord_acq_lista.setTabOrder("nota_dettaglio", 110)
	//-------------------------------------------------------------------------------
	
	wf_popola_imballo2(ls_errore)
	
else
	dw_det_ord_acq_lista.object.nota_dettaglio.visible = 0
	dw_det_ord_acq_lista.object.cod_imballo2.visible = 0
	dw_det_ord_acq_lista.object.cod_imballo1.visible = 0
	
	dw_det_ord_acq_lista.object.cod_iva.visible = 1
	dw_det_ord_acq_lista.object.data_consegna.visible = 1
	
	dw_det_ord_acq_lista.object.cod_verniciatura.visible = 0
	dw_det_ord_acq_lista.object.cod_verniciatura_t.visible = 0

	dw_det_ord_acq_lista.object.cod_reparto.visible = 0
	dw_det_ord_acq_lista.object.cod_reparto_t.visible = 0
end if
//-------------------------------------------------------------------------------

ib_supervisore = false
if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then ib_supervisore = true
else
	ib_supervisore = true
end if

try
	dw_det_ord_acq_det_3.object.p_commessa.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
catch (runtimeerror err)
end try

dw_det_ord_acq_lista.is_cod_parametro_blocco_prodotto = 'POF'
dw_det_ord_acq_det_1.is_cod_parametro_blocco_prodotto = 'POF'

this.width += 1
postevent("ue_imposta_colori")
postevent("ue_maximize")
end event

on w_det_ord_acq.create
int iCurrent
call super::create
this.st_1=create st_1
this.pb_prod_view=create pb_prod_view
this.cb_annulla_commessa=create cb_annulla_commessa
this.cb_blocca=create cb_blocca
this.cb_c_industriale=create cb_c_industriale
this.cb_corrispondenze=create cb_corrispondenze
this.cb_prezzo=create cb_prezzo
this.cb_prodotti_note_ricerca=create cb_prodotti_note_ricerca
this.cb_sblocca=create cb_sblocca
this.cb_sconti=create cb_sconti
this.st_2=create st_2
this.dw_det_ord_acq_lista=create dw_det_ord_acq_lista
this.dw_folder=create dw_folder
this.dw_det_ord_acq_det_1=create dw_det_ord_acq_det_1
this.dw_documenti=create dw_documenti
this.dw_det_ord_acq_det_3=create dw_det_ord_acq_det_3
this.dw_det_ord_acq_det_2=create dw_det_ord_acq_det_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.pb_prod_view
this.Control[iCurrent+3]=this.cb_annulla_commessa
this.Control[iCurrent+4]=this.cb_blocca
this.Control[iCurrent+5]=this.cb_c_industriale
this.Control[iCurrent+6]=this.cb_corrispondenze
this.Control[iCurrent+7]=this.cb_prezzo
this.Control[iCurrent+8]=this.cb_prodotti_note_ricerca
this.Control[iCurrent+9]=this.cb_sblocca
this.Control[iCurrent+10]=this.cb_sconti
this.Control[iCurrent+11]=this.st_2
this.Control[iCurrent+12]=this.dw_det_ord_acq_lista
this.Control[iCurrent+13]=this.dw_folder
this.Control[iCurrent+14]=this.dw_det_ord_acq_det_1
this.Control[iCurrent+15]=this.dw_documenti
this.Control[iCurrent+16]=this.dw_det_ord_acq_det_3
this.Control[iCurrent+17]=this.dw_det_ord_acq_det_2
end on

on w_det_ord_acq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.pb_prod_view)
destroy(this.cb_annulla_commessa)
destroy(this.cb_blocca)
destroy(this.cb_c_industriale)
destroy(this.cb_corrispondenze)
destroy(this.cb_prezzo)
destroy(this.cb_prodotti_note_ricerca)
destroy(this.cb_sblocca)
destroy(this.cb_sconti)
destroy(this.st_2)
destroy(this.dw_det_ord_acq_lista)
destroy(this.dw_folder)
destroy(this.dw_det_ord_acq_det_1)
destroy(this.dw_documenti)
destroy(this.dw_det_ord_acq_det_3)
destroy(this.dw_det_ord_acq_det_2)
end on

event pc_delete;call super::pc_delete;long ll_i

//if dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_evasione") = "E" then
//   messagebox("Attenzione", "Ordine non cancellabile! Ha già subito un'evasione.", exclamation!, ok!)
//   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
//   pcca.error = c_fatal
//   return
//end if
for ll_i = 1 to dw_det_ord_acq_lista.deletedcount()
	if dw_det_ord_acq_lista.getitemstring(ll_i, "flag_blocco", delete!, true) = "S" then
	   messagebox("Attenzione", "Ordine non cancellabile! E' stato bloccato.", exclamation!, ok!)
	   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
next

cb_prezzo.enabled=false
cb_blocca.enabled=false
cb_sblocca.enabled=false
cb_c_industriale.enabled=false
cb_corrispondenze.enabled = false
cb_annulla_commessa.enabled = false

dw_det_ord_acq_lista.object.b_duplica.enabled = false
end event

event pc_modify;call super::pc_modify;//if dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
//   messagebox("Attenzione", "Ordine non modificabile! Ha già subito un'evasione.", &
//              exclamation!, ok!)
//   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
//   pcca.error = c_fatal
//   return
//end if

if dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "flag_blocco") = "S" then
   messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_new;call super::pc_new;//if dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
//   messagebox("Attenzione", "Ordine non modificabile! Ha già subito un'evasione.", &
//              exclamation!, ok!)
//   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
//   dw_det_ord_acq_det_1.set_dw_view(c_ignorechanges)
//   dw_det_ord_acq_det_2.set_dw_view(c_ignorechanges)
//   dw_det_ord_acq_det_3.set_dw_view(c_ignorechanges)
//   pcca.error = c_fatal
//   return
//end if

if dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
   dw_det_ord_acq_det_1.set_dw_view(c_ignorechanges)
   dw_det_ord_acq_det_2.set_dw_view(c_ignorechanges)
   dw_det_ord_acq_det_3.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

type st_1 from statictext within w_det_ord_acq
integer x = 274
integer y = 2000
integer width = 919
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SHIFT - F4 sulla lista = Nuova Riga"
boolean focusrectangle = false
end type

type pb_prod_view from cb_listview within w_det_ord_acq
boolean visible = false
integer x = 4091
integer y = 1264
integer width = 73
integer height = 76
integer taborder = 10
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_det_ord_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type cb_annulla_commessa from commandbutton within w_det_ord_acq
boolean visible = false
integer x = 3913
integer y = 1168
integer width = 361
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;long ll_null

setnull(ll_null)

//dw_det_ord_acq_det_3.setcolumn("anno_commessa")
//dw_det_ord_acq_det_3.settext(ls_null)
//dw_det_ord_acq_det_3.setcolumn("num_commessa")
//dw_det_ord_acq_det_3.settext(ls_null)

dw_det_ord_acq_lista.setitem(dw_det_ord_acq_lista.getrow(),"anno_commessa",ll_null)
dw_det_ord_acq_lista.setitem(dw_det_ord_acq_lista.getrow(),"num_commessa",ll_null)
dw_det_ord_acq_det_3.setitem(dw_det_ord_acq_det_3.getrow(),"anno_commessa",ll_null)
dw_det_ord_acq_det_3.setitem(dw_det_ord_acq_det_3.getrow(),"num_commessa",ll_null)

end event

type cb_blocca from commandbutton within w_det_ord_acq
event clicked pbm_bnclicked
integer x = 2341
integer y = 1988
integer width = 361
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Blocca"
end type

event clicked;string ls_cod_tipo_det_acq, ls_cod_prodotto, ls_flag_tipo_det_acq, ls_flag_saldo,ls_cod_prodotto_raggruppato

dec{4} ld_quan_ordinata, ld_quan_arrivata, ld_quantita, ld_quan_raggruppo

long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine_acq, ll_i, ll_blocco


if dw_det_ord_acq_lista.getrow() > 0 then
	if dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "flag_blocco") = "N" then
		ls_cod_tipo_det_acq = dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "cod_tipo_det_acq")
		ls_flag_saldo = dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "flag_saldo")
		ls_cod_prodotto = dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "cod_prodotto")
		ld_quan_ordinata = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "quan_ordinata")
		ld_quan_arrivata = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "quan_arrivata")
		
		select tab_tipi_det_acq.flag_tipo_det_acq
		into   :ls_flag_tipo_det_acq
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		
		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", exclamation!, ok!)
			return
		end if
		
		if ls_flag_tipo_det_acq = "M" and ls_flag_saldo = 'N' and ld_quan_arrivata < ld_quan_ordinata then
			update anag_prodotti  
				set quan_ordinata = quan_ordinata - (:ld_quan_ordinata - :ld_quan_arrivata)
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
				rollback;
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordinata - ld_quan_arrivata), "M")
				
				update anag_prodotti  
					set quan_ordinata = quan_ordinata - :ld_quan_raggruppo
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
					rollback;
					return
				end if
			end if
			
		end if

		ll_anno_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "num_registrazione")
		ll_prog_riga_ordine_acq = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "prog_riga_ordine_acq")

		update det_ord_acq
			set flag_blocco = 'S'
		 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_acq.anno_registrazione = :ll_anno_registrazione and  
				 det_ord_acq.num_registrazione = :ll_num_registrazione and
				 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", exclamation!, ok!)
			rollback;
			return
		end if

		dw_det_ord_acq_det_1.setitem(dw_det_ord_acq_det_1.getrow(), "flag_blocco", "S")
		dw_det_ord_acq_det_1.setitemstatus(dw_det_ord_acq_det_1.getrow(), "flag_blocco", primary!, notmodified!)

		ll_blocco = 0

		for ll_i = 1 to dw_det_ord_acq_det_1.rowcount()
			if dw_det_ord_acq_lista.getitemstring(ll_i, "flag_blocco") = "S" then
				ll_blocco ++
			end if
		next

		if ll_blocco = dw_det_ord_acq_det_1.rowcount() then
			update tes_ord_acq
			set flag_blocco = 'S'
			where tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_ord_acq.anno_registrazione = :ll_anno_registrazione and  
					tes_ord_acq.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
							  exclamation!, ok!)
				dw_det_ord_acq_lista.i_parentdw.setitem(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
				dw_det_ord_acq_lista.i_parentdw.setitemstatus(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
				rollback;
				return
			end if
			dw_det_ord_acq_lista.i_parentdw.setitem(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "S")
			dw_det_ord_acq_lista.i_parentdw.setitemstatus(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
		end if
		commit;
	end if
end if
end event

type cb_c_industriale from commandbutton within w_det_ord_acq
event clicked pbm_bnclicked
integer x = 1563
integer y = 1988
integer width = 361
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C.Industriale"
end type

event clicked;
window_open_parm(w_det_ord_acq_stat,-1,dw_det_ord_acq_lista)
end event

event getfocus;call super::getfocus;string ls_null


setnull(ls_null)
dw_det_ord_acq_det_3.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_commessa"
s_cs_xx.parametri.parametro_s_2 = "num_commessa"

//s_cs_xx.parametri.parametro_s_10 = dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "cod_prodotto")
end event

type cb_corrispondenze from commandbutton within w_det_ord_acq
event clicked pbm_bnclicked
integer x = 1175
integer y = 1988
integer width = 361
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Ord_Acq"
window_open_parm(w_det_acq_ven_corr, -1, dw_det_ord_acq_det_1)

end event

type cb_prezzo from commandbutton within w_det_ord_acq
event clicked pbm_bnclicked
integer x = 2729
integer y = 1988
integer width = 361
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Prezzo"
end type

event clicked;//dw_det_ord_acq_det_1.accepttext()
//
//s_cs_xx.parametri.parametro_s_1 = dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(),"cod_misura")
//s_cs_xx.parametri.parametro_d_1 = dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"prezzo_acquisto")
//s_cs_xx.parametri.parametro_d_2 = dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"fat_conversione")
//s_cs_xx.parametri.parametro_d_3 = dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"quan_ordinata")
//
//window_open(w_prezzo_um, 0)
//
//if s_cs_xx.parametri.parametro_d_1 <> 0 then
//   dw_det_ord_acq_det_1.setitem(dw_det_ord_acq_det_1.getrow(), "prezzo_acquisto", s_cs_xx.parametri.parametro_d_1)
//end if
//
//if s_cs_xx.parametri.parametro_d_3 <> 0 then
//   dw_det_ord_acq_det_1.setitem(dw_det_ord_acq_det_1.getrow(), "quan_ordinata", s_cs_xx.parametri.parametro_d_3)
//end if

//Donato 14/12/2010
//modifica necessaria con integrazione di una funzione perchè tale finestra è stato richiesto di aprirla anche sull'itemchangerd di un campo
//il cui valore viene passato alla finestra, il problema che tale valore non viene acquisito perchè è nell'argomento "data" dell'itemchanged

string ls_cod_misura
decimal ld_prezzo_acquisto, ld_fat_conversione, ld_quan_ordinata

ls_cod_misura = dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(),"cod_misura")
ld_prezzo_acquisto = dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"prezzo_acquisto")
ld_fat_conversione = dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"fat_conversione")
ld_quan_ordinata = dw_det_ord_acq_det_1.getitemnumber(dw_det_ord_acq_det_1.getrow(),"quan_ordinata")

wf_prezzo_um(ls_cod_misura, ld_prezzo_acquisto, ld_fat_conversione, ld_quan_ordinata)
end event

type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_ord_acq
boolean visible = false
integer x = 3918
integer y = 1356
integer height = 80
integer taborder = 50
boolean bringtotop = true
end type

event getfocus;call super::getfocus;s_cs_xx.parametri.parametro_s_1 = dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(),"cod_prodotto")
dw_det_ord_acq_det_2.change_dw_current()
end event

type cb_sblocca from commandbutton within w_det_ord_acq
event clicked pbm_bnclicked
integer x = 1952
integer y = 1988
integer width = 361
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sbl&occa"
end type

event clicked;string ls_cod_tipo_det_acq, ls_cod_prodotto, ls_flag_tipo_det_acq, ls_flag_saldo,ls_cod_prodotto_raggruppato
dec{4} ld_quan_ordinata, ld_quan_arrivata, ld_quantita, ld_quan_raggruppo
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine_acq


if dw_det_ord_acq_lista.getrow() > 0 then
	if dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "flag_blocco") = "S" then
		ls_cod_tipo_det_acq = dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "cod_tipo_det_acq")
		ls_flag_saldo = dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "flag_saldo")
		ls_cod_prodotto = dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_lista.getrow(), "cod_prodotto")
		ld_quan_ordinata = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "quan_ordinata")
		ld_quan_arrivata = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "quan_arrivata")

		select tab_tipi_det_acq.flag_tipo_det_acq
		into   :ls_flag_tipo_det_acq
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		
		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", exclamation!, ok!)
			return
		end if
		
		if ls_flag_tipo_det_acq = "M" and ls_flag_saldo = 'N' and ld_quan_arrivata < ld_quan_ordinata then
			update anag_prodotti  
				set quan_ordinata = quan_ordinata + (:ld_quan_ordinata - :ld_quan_arrivata)
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
				rollback;
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordinata - ld_quan_arrivata), "M")
				
				update anag_prodotti  
					set quan_ordinata = quan_ordinata + :ld_quan_raggruppo
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
					return 1
				end if
			end if
			
			
		end if

		ll_anno_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "num_registrazione")
		ll_prog_riga_ordine_acq = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "prog_riga_ordine_acq")

		update det_ord_acq
			set flag_blocco = 'N'
		 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_acq.anno_registrazione = :ll_anno_registrazione and  
				 det_ord_acq.num_registrazione = :ll_num_registrazione and
				 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		update tes_ord_acq
		set flag_blocco = 'N'
		where tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_ord_acq.anno_registrazione = :ll_anno_registrazione and  
				tes_ord_acq.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		dw_det_ord_acq_det_1.setitem(dw_det_ord_acq_det_1.getrow(), "flag_blocco", "N")
		dw_det_ord_acq_det_1.setitemstatus(dw_det_ord_acq_det_1.getrow(), "flag_blocco", primary!, notmodified!)
		dw_det_ord_acq_lista.i_parentdw.setitem(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
		dw_det_ord_acq_lista.i_parentdw.setitemstatus(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
		commit;
	end if
end if
end event

type cb_sconti from commandbutton within w_det_ord_acq
integer x = 3118
integer y = 1988
integer width = 361
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_ord_acq_lista
window_open(w_sconti, 0)

end event

type st_2 from statictext within w_det_ord_acq
integer x = 274
integer y = 1928
integer width = 3200
integer height = 68
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_det_ord_acq_lista from uo_cs_xx_dw within w_det_ord_acq
integer y = 20
integer width = 4613
integer height = 988
integer taborder = 20
string dataobject = "d_det_ord_acq_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_cod_tipo_ord_acq, ls_flag_tipo_det_acq, &
          ls_modify, ls_null, ls_cod_fornitore, ls_cod_iva, ls_cod_reparto
   long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine_acq, ll_row
   datetime ldt_data_consegna ,ldt_data_consegna_for, ldt_data_esenzione_iva, ldt_data_registrazione

   setnull(ls_null)

   ll_anno_registrazione = dw_det_ord_acq_lista.i_parentdw.getitemnumber(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
   ll_num_registrazione = dw_det_ord_acq_lista.i_parentdw.getitemnumber(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
   ldt_data_registrazione = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_fornitore = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
   ldt_data_consegna = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_consegna")
   ldt_data_consegna_for = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_consegna_fornitore")

   select max(det_ord_acq.prog_riga_ordine_acq)
   into   :ll_prog_riga_ordine_acq
   from   det_ord_acq
   where  det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
          det_ord_acq.anno_registrazione = :ll_anno_registrazione and
          det_ord_acq.num_registrazione = :ll_num_registrazione;


	ll_row = dw_det_ord_acq_det_1.getrow()

   if isnull(ll_prog_riga_ordine_acq) then
      dw_det_ord_acq_det_1.setitem(ll_row, "prog_riga_ordine_acq", 10)
   else
      dw_det_ord_acq_det_1.setitem(ll_row, "prog_riga_ordine_acq", ll_prog_riga_ordine_acq + 10)
   end if

   ls_cod_tipo_ord_acq = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_ord_acq")

   select tab_tipi_ord_acq.cod_tipo_det_acq
   into   :ls_cod_tipo_det_acq
   from   tab_tipi_ord_acq
   where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
   
   if sqlca.sqlcode = 0 then
      dw_det_ord_acq_det_1.setitem(ll_row, "cod_tipo_det_acq", ls_cod_tipo_det_acq)
		
      if isnull(this.getitemstring(ll_row, "cod_iva")) then
           select tab_tipi_det_acq.cod_iva  
           into   :ls_cod_iva  
           from   tab_tipi_det_acq  
           where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                  tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
           if sqlca.sqlcode = 0 then
              this.setitem(ll_row, "cod_iva", ls_cod_iva)
           end if
      end if
   else
      ls_cod_tipo_det_acq = ls_null
   end if
	
	
	if ll_row>1 then
		ls_cod_reparto = getitemstring(ll_row -1, "cod_reparto")
		if ls_cod_reparto="" then setnull(ls_cod_reparto)
		
		setitem(ll_row, "cod_reparto", ls_cod_reparto)
	end if
	
	

   dw_det_ord_acq_det_1.setitem(ll_row, "quan_ordinata", 1)
   dw_det_ord_acq_det_1.setitem(ll_row, "data_consegna", ldt_data_consegna)
   dw_det_ord_acq_det_1.setitem(ll_row, "data_consegna_fornitore", ldt_data_consegna_for)
   
   select anag_fornitori.cod_iva,
          anag_fornitori.data_esenzione_iva
   into   :ls_cod_iva,
          :ldt_data_esenzione_iva
   from   anag_fornitori
   where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
          anag_fornitori.cod_fornitore = :ls_cod_fornitore;
   
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and &
         (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
         this.setitem(ll_row, "cod_iva", ls_cod_iva)
      end if
   end if
   
   ls_modify = "cod_tipo_det_acq.protect='0'~t"
   dw_det_ord_acq_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='16777215'~t"
   dw_det_ord_acq_det_1.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_acq) then
      dw_det_ord_acq_det_1.setitem(ll_row, "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      ld_datawindow = dw_det_ord_acq_det_1
		lp_prod_view = pb_prod_view
		setnull(lc_prodotti_ricerca)
      f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
   else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "quan_ordinata.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "quan_ordinata.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "data_consegna.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "data_consegna.background.color='12632256'~t"
      ls_modify = "data_consegna_fornitore.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "data_consegna_fornitore.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prod_fornitore.protect='1'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prod_fornitore.background.color='12632256'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
   end if

	ib_nuovo = true
	cb_sconti.enabled = true
	
	//pulsanti direttamente sulla dw
	try
		//Donato 30/01/2012
		//TULIO!!!! se ti piglio te tronkooooo!!!!!
		//dw_det_ord_acq_det_3.object.b_commesse_ric.enabled = true
		dw_det_ord_acq_det_3.object.b_ricerca_commessa.enabled = true
		
		dw_det_ord_acq_det_3.object.b_annulla_commessa.enabled = true
		
		dw_det_ord_acq_det_1.object.b_saldo.enabled = false
	catch (throwable err)
	end try
//	cb_commesse_ric.enabled = true
	cb_annulla_commessa.enabled = true
	
	cb_prezzo.enabled=true
	cb_blocca.enabled=false
	cb_sblocca.enabled=false
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
	
	object.b_duplica.enabled = false
	
	dw_det_ord_acq_det_2.setitem(ll_row, "flag_doc_suc_det", "I")
	dw_det_ord_acq_det_2.setitem(ll_row, "flag_st_note_det", "I")	
	
	setcolumn("cod_prodotto")
	setfocus()
	
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
   long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i2, ll_i3, &
		  ll_prog_riga_ordine_acq, ll_controllo, ll_prog_riga_ord_acq, ll_evasi, ll_aperti, ll_parziali
   dec{4} ld_sconto_testata, ld_quan_ordinata_old, ld_quan_ordinata,ldd_quan_arrivata, ld_residua, ld_qta_vecchia,&
	       ld_qta_evasa, 	ld_quantita, ld_quan_raggruppo
   string ls_cod_pagamento, ls_cod_valuta, ls_tabella, ls_quan_documento, &
          ls_tot_val_documento, ls_cod_prodotto, ls_cod_prodotto_old, ls_null, &
			 ls_flag_tipo_det_acq, ls_nome_prog, ls_cod_tipo_det_acq, ls_flag_saldo, &
			 ls_cod_prodotto_raggruppato, ls_flag_stato_ordine, ls_flag_saldo_2, ls_imballo1, ls_imballo2
	dwItemStatus ldw_status
	setnull(ls_null)
   ll_i = i_parentdw.i_selectedrows[1]

	//-----------------------------------------------------------------------------------------------
	//reset itemstatus per npon avere problemi in salbvataggio sulle colonne computed (inesistenti in tabella)
	for ll_i2=1 to this.rowcount()
		this.SetItemStatus(ll_i2, "cod_imballo1", Primary!, NotModified!)
		this.SetItemStatus(ll_i2, "cod_imballo2", Primary!, NotModified!)
	next
	//-----------------------------------------------------------------------------------------------

   ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
   ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")
   ld_sconto_testata = i_parentdw.getitemnumber(ll_i, "sconto")
   ls_cod_pagamento = i_parentdw.getitemstring(ll_i, "cod_pagamento")
   ls_cod_valuta = i_parentdw.getitemstring(ll_i, "cod_valuta")
	ll_prog_riga_ord_acq = getitemnumber(this.getrow(),"prog_riga_ordine_acq")
   ls_tabella = "tes_ord_acq"
   ls_quan_documento = "quan_ordinata"
   ls_tot_val_documento = "tot_val_ordine"

	ib_modifica = false
	ib_nuovo = false
	
	//pulsante ricerca prodotto direttamente su dw
	try
		dw_det_ord_acq_det_1.object.b_ricerca_prodotto.enabled = false
	catch (throwable err1)
	end try
	
	pb_prod_view.enabled = false
		
	//pulsante prodotti note ricerca direttamente su dw
	try
		dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
	catch (throwable err2)
	end try
	cb_prodotti_note_ricerca.enabled = false
	
	cb_sconti.enabled = false
	
	//pulsanti direttamente sulla dw
	try
		//Donato 30/01/2012
		//TULIO!!!! se ti piglio te tronkooooo!!!!!
		//dw_det_ord_acq_det_3.object.b_commesse_ric.enabled = false
		dw_det_ord_acq_det_3.object.b_ricerca_commessa.enabled = false
		
		
		dw_det_ord_acq_det_3.object.b_annulla_commessa.enabled = true
	catch (throwable err)
	end try
//	cb_commesse_ric.enabled = false
	cb_annulla_commessa.enabled = true
	
	cb_prezzo.enabled = false
	cb_blocca.enabled = true
	cb_sblocca.enabled = true
	cb_c_industriale.enabled = true
	cb_corrispondenze.enabled = true

   this.change_dw_current()
   f_crea_listini_fornitori()

	for ll_i2 = 1 to this.rowcount()
		ldw_status = this.GetItemStatus( ll_i2, 0, Primary!)
		if ldw_status = DataModified!	 or ldw_status = New!	 or ldw_status = NewModified!	then
			ls_cod_tipo_det_acq = this.getitemstring(ll_i2, "cod_tipo_det_acq")
			ldd_quan_arrivata = this.getitemnumber(ll_i2, "quan_arrivata")
	
			select tab_tipi_det_acq.flag_tipo_det_acq
			into   :ls_flag_tipo_det_acq
			from   tab_tipi_det_acq
			where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	
			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", exclamation!, ok!)
				rollback;
				return 1
			end if
	
			if ls_flag_tipo_det_acq = "M" and (ldd_quan_arrivata = 0 or isnull(ldd_quan_arrivata)) and dw_det_ord_acq_det_1.getitemstring(getrow(), "flag_saldo")  = 'N' then
				ll_prog_riga_ordine_acq = this.getitemnumber(ll_i2, "prog_riga_ordine_acq")
	
				select det_ord_acq.prog_riga_ordine_acq
				  into :ll_controllo
				  from det_ord_acq
				 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and 
						 det_ord_acq.anno_registrazione = :ll_anno_registrazione and 
						 det_ord_acq.num_registrazione = :ll_num_registrazione and 
						 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
	
				if sqlca.sqlcode = 0 then
					ls_cod_prodotto_old = this.getitemstring(ll_i2, "cod_prodotto", primary!, true)
					ld_quan_ordinata_old = this.getitemnumber(ll_i2, "quan_ordinata", primary!, true)
	
					update anag_prodotti  
						set quan_ordinata = quan_ordinata - :ld_quan_ordinata_old  
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto_old;
					if sqlca.sqlcode = -1 then
						messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità ordinata del prodotto " + ls_cod_prodotto_old, exclamation!, ok!)
						rollback;
						return 1
					end if
					
					// enme 08/1/2006 gestione prodotto raggruppato
					setnull(ls_cod_prodotto_raggruppato)
					
					select cod_prodotto_raggruppato
					into   :ls_cod_prodotto_raggruppato
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto_old;
							 
					if not isnull(ls_cod_prodotto_raggruppato) then
						
						ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_old, ld_quan_ordinata_old, "M")
						
						update anag_prodotti  
							set quan_ordinata = quan_ordinata - :ld_quan_raggruppo
						 where cod_azienda = :s_cs_xx.cod_azienda and  
								 cod_prodotto = :ls_cod_prodotto_raggruppato;
		
						if sqlca.sqlcode = -1 then
							messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto_old+". Dettaglio " + sqlca.sqlerrtext)
							rollback;
							return 1
						end if
					end if
					
				end if
	
				ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto", primary!, false)
				ld_quan_ordinata = this.getitemnumber(ll_i2, "quan_ordinata", primary!, false)
	
				update anag_prodotti  
					set quan_ordinata = quan_ordinata + :ld_quan_ordinata  
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto;
				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità ordinata del prodotto " + ls_cod_prodotto, exclamation!, ok!)
					rollback;
					return 1
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordinata, "M")
					
					update anag_prodotti  
						set quan_ordinata = quan_ordinata + :ld_quan_raggruppo    
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto_raggruppato;
	
					if sqlca.sqlcode = -1 then
						messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
						rollback;
						return 1
					end if
				end if
			else
				//**** nel caso in cui si modifichi una riga evasa o parzialmente evasa claudia 05/02/07
				//--------ovviamente non si può cambiare il prodotto visto che ne abbiamo già ricevuto
				
				//	e nel caso in cui sia stato fatto il saldo usando il pulsante salda allora devo agire in modo diverso
				ld_qta_vecchia =  this.getitemnumber(getrow(), "quan_ordinata", primary!, true)
				ld_qta_evasa = this.getitemnumber(getrow(), "quan_arrivata", primary!, false)
         		ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto", primary!, false)
				ld_quantita = this.getitemnumber(getrow(), "quan_ordinata", primary!, false)
				
				
				if ls_flag_tipo_det_acq = "M" then
					if dw_det_ord_acq_det_1.getitemstring(getrow(), "flag_saldo")  = 'S'  then
						// allora devo modificare l'importo ordinato 
						ll_prog_riga_ordine_acq = this.getitemnumber(ll_i2, "prog_riga_ordine_acq")
		
						select det_ord_acq.prog_riga_ordine_acq
						  into :ll_controllo
						  from det_ord_acq
						 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and 
								 det_ord_acq.anno_registrazione = :ll_anno_registrazione and 
								 det_ord_acq.num_registrazione = :ll_num_registrazione and 
								 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
		
						if sqlca.sqlcode = 0 then
							ls_cod_prodotto_old = this.getitemstring(ll_i2, "cod_prodotto", primary!, true)
							ld_quan_ordinata_old = this.getitemnumber(ll_i2, "quan_ordinata", primary!, true)
							
							//----------non modifico la quantità ordinata ora lo faccio dopo
							//--------ovviamente non si può cambiare il prodotto visto che abbiamo già ricevuto della merce
							if ls_cod_prodotto_old <> ls_cod_prodotto then
								messagebox("Attenzione", "Non è possibile modificare il prodotto di righe evase. ")
								rollback;
								return 1
							end if
						
						end if
			
						ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto", primary!, false)
						ld_quan_ordinata = this.getitemnumber(ll_i2, "quan_ordinata", primary!, false)
						//--------metto solo la quantità effettivamente in attesa per 2 perchè poi viene tolta metà
						update anag_prodotti  
							set quan_ordinata = quan_ordinata + ((:ld_quan_ordinata  - :ldd_quan_arrivata)*2)
						 where cod_azienda = :s_cs_xx.cod_azienda and  
								 cod_prodotto = :ls_cod_prodotto;
						if sqlca.sqlcode = -1 then
							messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità ordinata del prodotto " + ls_cod_prodotto, exclamation!, ok!)
							rollback;
							return 1
						end if
						
						// enme 08/1/2006 gestione prodotto raggruppato
						setnull(ls_cod_prodotto_raggruppato)
						
						select cod_prodotto_raggruppato
						into   :ls_cod_prodotto_raggruppato
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
							 
						if not isnull(ls_cod_prodotto_raggruppato) then
							
							ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ((ld_quan_ordinata  - ldd_quan_arrivata)*2), "M")
							
							update anag_prodotti  
								set quan_ordinata = quan_ordinata + :ld_quan_raggruppo
							 where cod_azienda = :s_cs_xx.cod_azienda and  
									 cod_prodotto = :ls_cod_prodotto_raggruppato;
			
							if sqlca.sqlcode = -1 then
								messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
								rollback;
								return 1
							end if
						end if
						
						if ld_qta_evasa < ld_quantita then
							ls_flag_saldo_2 = 'N'
						elseif ld_qta_evasa = 0 then
							ls_flag_saldo_2 = 'N'	
						elseif ld_qta_evasa = ld_quantita then
							ls_flag_saldo_2 = 'S'
						end if
						
						update det_ord_acq
						set    flag_saldo = :ls_flag_saldo_2
						where  cod_azienda  = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 prog_riga_ordine_acq = :ll_prog_riga_ord_acq;
						if sqlca.sqlcode <> 0 then
							messagebox("APICE","Errore in aggiornamento STATO ORDINE di acquisto, riga:"+string(ll_prog_riga_ord_acq)+". " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if
		
						//imposto stato ordine
						ll_anno_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "anno_registrazione")
						ll_num_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "num_registrazione")
				
						//conto tutte le righe evase
						select count(*)
						into   :ll_evasi
						from   det_ord_acq
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 flag_saldo = 'S' ;
						if sqlca.sqlcode = -1 then
							rollback;
							return -1
						end if
						
						if isnull(ll_evasi) then ll_evasi = 0 //RIGHE CON FLAG A SI
						
						//conto tutte le righe aperte
						select count(*)
						into   :ll_aperti
						from   det_ord_acq
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 quan_arrivata = 0;
						if sqlca.sqlcode = -1 then 
							rollback;
							return -1
						end if
						
						if isnull(ll_aperti) then ll_aperti = 0 
						
						//conto tutte le righe aperte
						select count(*)
						into   :ll_parziali
						from   det_ord_acq
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 quan_arrivata <> 0;
						if sqlca.sqlcode = -1 then 
							rollback;
							return -1
						end if
						
						if isnull(ll_parziali) then ll_parziali = 0 
		
						ls_flag_stato_ordine = "A"
						if ll_aperti = 0 and ll_parziali = 0 and ll_evasi > 0 then 
							ls_flag_stato_ordine = "E"
						elseif ll_evasi = 0 and ll_aperti > 0 and ll_parziali = 0 then
							ls_flag_stato_ordine = "A"
						elseif ll_parziali > 0 then 
							ls_flag_stato_ordine = "P"
						else
							ls_flag_stato_ordine = "A"					
						end if
						
						update tes_ord_acq
						set    flag_evasione = :ls_flag_stato_ordine
						where  cod_azienda  = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione;
						if sqlca.sqlcode <> 0 then
							messagebox("APICE","Errore in aggiornamento STATO ORDINE di acquisto. " + sqlca.sqlerrtext)
							rollback;
							return -1
							
						end if
					else
						//------------caso in cui parzialmente evasa la riga
						ll_prog_riga_ordine_acq = this.getitemnumber(ll_i2, "prog_riga_ordine_acq")
		
						select det_ord_acq.prog_riga_ordine_acq
						  into :ll_controllo
						  from det_ord_acq
						 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and 
								 det_ord_acq.anno_registrazione = :ll_anno_registrazione and 
								 det_ord_acq.num_registrazione = :ll_num_registrazione and 
								 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
		
						if sqlca.sqlcode = 0 then
							ls_cod_prodotto_old = this.getitemstring(ll_i2, "cod_prodotto", primary!, true)
							ld_quan_ordinata_old = this.getitemnumber(ll_i2, "quan_ordinata", primary!, true)
							//--------ovviamente non si può cambiare il prodotto visto che abbiamo già ricevuto della merce
							if ls_cod_prodotto_old <> ls_cod_prodotto then
								messagebox("Attenzione", "Non è possibile modificare il prodotto di righe evase. ")
								rollback;
								return 1
							end if
							//------tolgo solo la parte non ancora arrivata
							update anag_prodotti  
								set quan_ordinata = quan_ordinata - (:ld_quan_ordinata_old  - :ldd_quan_arrivata)
							 where cod_azienda = :s_cs_xx.cod_azienda and  
									 cod_prodotto = :ls_cod_prodotto_old;
							if sqlca.sqlcode = -1 then
								messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità ordinata del prodotto " + ls_cod_prodotto_old, exclamation!, ok!)
								rollback;
								return 1
							end if
						
						// enme 08/1/2006 gestione prodotto raggruppato
							setnull(ls_cod_prodotto_raggruppato)
							
							select cod_prodotto_raggruppato
							into   :ls_cod_prodotto_raggruppato
							from   anag_prodotti
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_prodotto_old;
									 
							if not isnull(ls_cod_prodotto_raggruppato) then
								
								ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_old, (ld_quan_ordinata_old   - ldd_quan_arrivata), "M")
								
								update anag_prodotti  
									set quan_ordinata = quan_ordinata - :ld_quan_raggruppo
								 where cod_azienda = :s_cs_xx.cod_azienda and  
										 cod_prodotto = :ls_cod_prodotto_raggruppato;
				
								if sqlca.sqlcode = -1 then
									messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto_old+". Dettaglio " + sqlca.sqlerrtext)
									rollback;
									return 1
								end if
							end if
						
						end if
			
						ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto", primary!, false)
						ld_quan_ordinata = this.getitemnumber(ll_i2, "quan_ordinata", primary!, false)
						//--------aggiungo solo la parte non ancora arrivata in base al nuovo importo
						update anag_prodotti  
							set quan_ordinata = quan_ordinata + (:ld_quan_ordinata  - :ldd_quan_arrivata)
						 where cod_azienda = :s_cs_xx.cod_azienda and  
								 cod_prodotto = :ls_cod_prodotto;
						if sqlca.sqlcode = -1 then
							messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità ordinata del prodotto " + ls_cod_prodotto, exclamation!, ok!)
							rollback;
							return 1
						end if
						
						// enme 08/1/2006 gestione prodotto raggruppato
						setnull(ls_cod_prodotto_raggruppato)
						
						select cod_prodotto_raggruppato
						into   :ls_cod_prodotto_raggruppato
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
							 
						if not isnull(ls_cod_prodotto_raggruppato) then
							
							ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordinata  - ldd_quan_arrivata), "M")
										
							update anag_prodotti  
								set quan_ordinata = quan_ordinata + :ld_quan_raggruppo
							 where cod_azienda = :s_cs_xx.cod_azienda and  
									 cod_prodotto = :ls_cod_prodotto_raggruppato;
			
							if sqlca.sqlcode = -1 then
								messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
								rollback;
								return 1
							end if
						end if
						
						if ld_qta_evasa < ld_quantita then
							ls_flag_saldo_2 = 'N'
						elseif ld_qta_evasa = 0 then
							ls_flag_saldo_2 = 'N'	
						elseif ld_qta_evasa = ld_quantita then
							ls_flag_saldo_2 = 'S'
						end if
						
						update det_ord_acq
						set    flag_saldo = :ls_flag_saldo_2
						where  cod_azienda  = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 prog_riga_ordine_acq = :ll_prog_riga_ord_acq;
						if sqlca.sqlcode <> 0 then
							messagebox("APICE","Errore in aggiornamento STATO ORDINE di acquisto, riga:"+string(ll_prog_riga_ord_acq)+". " + sqlca.sqlerrtext)
							return -1
						end if
		
						//imposto stato ordine
						ll_anno_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "anno_registrazione")
						ll_num_registrazione = dw_det_ord_acq_lista.getitemnumber(dw_det_ord_acq_lista.getrow(), "num_registrazione")
				
						//conto tutte le righe evase
						select count(*)
						into   :ll_evasi
						from   det_ord_acq
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 flag_saldo = 'S' ;
						if sqlca.sqlcode = -1 then 
							rollback;
							return -1
						end if
						
						if isnull(ll_evasi) then ll_evasi = 0 //RIGHE CON FLAG A SI
						
						//conto tutte le righe aperte
						select count(*)
						into   :ll_aperti
						from   det_ord_acq
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 quan_arrivata = 0;
						if sqlca.sqlcode = -1 then 
							rollback;
							return -1
						end if
						
						if isnull(ll_aperti) then ll_aperti = 0 
						
						//conto tutte le righe aperte
						select count(*)
						into   :ll_parziali
						from   det_ord_acq
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione and
								 quan_arrivata <> 0;
						if sqlca.sqlcode = -1 then 
							rollback;
							return -1
						end if
						
						if isnull(ll_parziali) then ll_parziali = 0 
		
						ls_flag_stato_ordine = "A"
						if ll_aperti = 0 and ll_parziali = 0 and ll_evasi > 0 then 
							ls_flag_stato_ordine = "E"
						elseif ll_evasi = 0 and ll_aperti > 0 and ll_parziali = 0 then
							ls_flag_stato_ordine = "A"
						elseif ll_parziali > 0 then 
							ls_flag_stato_ordine = "P"
						else
							ls_flag_stato_ordine = "A"					
						end if
						
						update tes_ord_acq
						set    flag_evasione = :ls_flag_stato_ordine
						where  cod_azienda  = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_registrazione and
								 num_registrazione = :ll_num_registrazione;
						if sqlca.sqlcode <> 0 then
							messagebox("APICE","Errore in aggiornamento STATO ORDINE di acquisto. " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if

						
					end if
					
				end if	

			end if
			
			
		//--------------------------------------------------------------------------------------------------------------------------------
		//Donato 04/04/2013
		//aggiornamento pezzi per collo e imballo in anagrafica prodotti: SR Modifiche_acquisti
		ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
		wf_aggiorna_imballi(ls_cod_prodotto, ll_i2)
		//--------------------------------------------------------------------------------------------------------------------------------
			
		end if
		
	next

	for ll_i3 = 1 to deletedcount()
		ls_cod_tipo_det_acq = this.getitemstring(ll_i3, "cod_tipo_det_acq", delete!, true)
		ldd_quan_arrivata = getitemnumber(ll_i3,"quan_arrivata",delete!,true)
		
		if ldd_quan_arrivata > 0 then
			messagebox("Attenzione", "Una delle righe selezionate per la cancellazione ha già subito un evasione: non è più possibile eliminarla. Riselezionare le righe da cancellare.", stopsign!)
			return 1
		end if
		select flag_tipo_det_acq
		into   :ls_flag_tipo_det_acq
		from   tab_tipi_det_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_acq = :ls_cod_tipo_det_acq;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", exclamation!, ok!)
			return 1
		end if

		if ls_flag_tipo_det_acq = "M" then
			ls_cod_prodotto = this.getitemstring(ll_i3, "cod_prodotto", delete!, true)
			ld_quan_ordinata = this.getitemnumber(ll_i3, "quan_ordinata", delete!, true)
	
			update anag_prodotti  
				set quan_ordinata = quan_ordinata - :ld_quan_ordinata  
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;

			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità ordinata del prodotto " + ls_cod_prodotto, exclamation!, ok!)
				rollback;
				return 1
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordinata, "M")
				
				update anag_prodotti  
					set quan_ordinata = quan_ordinata - :ld_quan_raggruppo
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
					rollback;
					return 1
				end if
			end if
		end if
	next

	ls_tabella = "det_ord_acq_stat"
	ls_nome_prog = "prog_riga_ordine_acq"
	
   for ll_i = 1 to this.deletedcount()
		ll_prog_riga_ord_acq = this.getitemnumber(ll_i, "prog_riga_ordine_acq", delete!, true)		
      if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, ll_prog_riga_ord_acq) = -1 then
			return 1
		end if
   next
	
	for ll_i = 1 to dw_det_ord_acq_det_1.rowcount()
		
		ll_prog_riga_ord_acq = dw_det_ord_acq_det_1.getitemnumber(ll_i, "prog_riga_ordine_acq")
		
		if dw_det_ord_acq_det_1.getitemstring(ll_i,"flag_saldo") = "S" then			
						
			select flag_saldo
			into   :ls_flag_saldo
			from   det_ord_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ordine_acq = :ll_prog_riga_ord_acq;
			if sqlca.sqlcode <> 0 then
				messagebox("APICE","Errore nella select di det_ord_acq: " + sqlca.sqlerrtext)
				return 1
			end if
			
			if ls_flag_saldo = "N" then
				
				ls_cod_prodotto = dw_det_ord_acq_det_1.getitemstring(ll_i,"cod_prodotto")
				ld_residua = dw_det_ord_acq_det_1.getitemnumber(ll_i,"quan_ordinata") - dw_det_ord_acq_det_1.getitemnumber(ll_i,"quan_arrivata")
				
				update anag_prodotti
				set    quan_ordinata = quan_ordinata - :ld_residua
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if sqlca.sqlcode <> 0 then
					messagebox("APICE","Errore in aggiornamento quantità ordinata in anag_prodotti: " + sqlca.sqlerrtext)
					return 1
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_residua, "M")

					update anag_prodotti  
						set quan_ordinata = quan_ordinata - :ld_quan_raggruppo 
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto_raggruppato;
	
					if sqlca.sqlcode = -1 then
						messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
						return 1
					end if
				end if
				
			end if	
			
		end if
		
		if not isnull(dw_det_ord_acq_det_1.getitemstring(ll_i,"cod_centro_costo")) and &
			(not isnull(dw_det_ord_acq_det_1.getitemnumber(ll_i,"anno_commessa")) or &
			not isnull(dw_det_ord_acq_det_1.getitemnumber(ll_i,"num_commessa"))) then
			
			messagebox("APICE","ATTENZIONE!~nNella riga numero " + string(ll_prog_riga_ord_acq) + &
						  " sono stati selezionati sia commessa che centro di costo.")
		end if
		
   next
	
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	ib_modifica = false
	ib_nuovo = false
	
	//pulsante prodotti note ricerca direttamente su dw
	try
		dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
	catch (throwable err2)
	end try
	cb_prodotti_note_ricerca.enabled = false
	
	//pulsante ricerca prodotto direttamente su dw
	try
		dw_det_ord_acq_det_1.object.b_ricerca_prodotto.enabled = false
	catch (throwable err1)
	end try
	
	pb_prod_view.enabled = false
	cb_sconti.enabled = false
	
	//pulsanti direttamente sulla dw
	try
		//Donato 30/01/2012
		//TULIO!!!! se ti piglio te tronkooooo!!!!!
		//dw_det_ord_acq_det_3.object.b_commesse_ric.enabled = false
		dw_det_ord_acq_det_3.object.b_ricerca_commessa.enabled = false
		
		dw_det_ord_acq_det_3.object.b_annulla_commessa.enabled = false
		
		dw_det_ord_acq_det_1.object.b_saldo.enabled = true
	catch (throwable err)
	end try
//	cb_commesse_ric.enabled = false
	cb_annulla_commessa.enabled = false
	
	cb_prezzo.enabled=false
	
	object.b_duplica.enabled = true
	
	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
			cb_c_industriale.enabled=true
			cb_blocca.enabled=true
			cb_sblocca.enabled=true
			cb_corrispondenze.enabled = true
		end if
	else
		cb_c_industriale.enabled=false
		cb_blocca.enabled=false
		cb_sblocca.enabled=false
		cb_corrispondenze.enabled = false
	end if
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_fornitore, ls_cod_iva, ls_cod_tipo_det_acq, ls_modify, ls_cod_prodotto, &
	       ls_cod_misura_mag
   datetime ldt_data_registrazione, ldt_data_esenzione_iva

   ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
   ldt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")

	if this.getrow() > 0 then
		
		dw_documenti.uof_retrieve_blob(this.getrow())
		
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		

      select cod_misura_mag
      into   :ls_cod_misura_mag
      from   anag_prodotti
      where  cod_azienda = :s_cs_xx.cod_azienda and 
             cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode = 0 then
	   	dw_det_ord_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
	   	dw_det_ord_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
		else
	   	dw_det_ord_acq_det_1.modify("st_prezzo.text='Prezzo:'")
	   	dw_det_ord_acq_det_1.modify("st_quantita.text='Quantità:'")
		end if
		
		if not isnull(this.getitemstring(this.getrow(),"cod_misura")) then
		   cb_prezzo.text = "Prezzo al " + this.getitemstring(this.getrow(),"cod_misura")
		else
		   cb_prezzo.text = "Prezzo"
		end if
	
		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_ord_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_ord_acq_det_1.modify("st_fattore_conv.text=''")		
		end if
	
		select anag_fornitori.cod_iva,
				 anag_fornitori.data_esenzione_iva
		into   :ls_cod_iva,
				 :ldt_data_esenzione_iva
		from   anag_fornitori
		where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
	
		if sqlca.sqlcode = 0 then
			if ls_cod_iva <> "" and ldt_data_esenzione_iva <= ldt_data_registrazione then
				f_po_loaddddw_dw(dw_det_ord_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
				f_po_loaddddw_dw(dw_det_ord_acq_lista, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
			else
				f_po_loaddddw_dw(dw_det_ord_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
				f_po_loaddddw_dw(dw_det_ord_acq_lista, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
			end if
		end if
	
		if ib_modifica or ib_nuovo then
			ls_cod_tipo_det_acq = dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(), "cod_tipo_det_acq")
			ld_datawindow = dw_det_ord_acq_det_1
			lp_prod_view = pb_prod_view
		
			if not isnull(dw_det_ord_acq_lista.getitemstring(dw_det_ord_acq_det_1.getrow(),"cod_prodotto")) then
				//pulsante prodotti note ricerca direttamente su dw
				try
					dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = true
				catch (throwable err2)
				end try
				cb_prodotti_note_ricerca.enabled = true
			else
				//pulsante prodotti note ricerca direttamente su dw
				try
					dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
				catch (throwable err3)
				end try
				cb_prodotti_note_ricerca.enabled = false
			end if
			if getrow() > 0 then wf_proteggi_colonne(getitemnumber(getrow(),"quan_arrivata"))
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
			f_tipo_dettaglio_acq_lista(dw_det_ord_acq_lista, ls_cod_tipo_det_acq)
		end if
		
	end if

   if ib_nuovo then
      ls_modify = "cod_tipo_det_acq.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_det_acq.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_ord_acq_det_1.modify(ls_modify)
   end if
	
end if
end event

event updateend;call super::updateend;if rowsdeleted + rowsinserted + rowsdeleted > 0 then
	
	this.postevent("pcd_view")
	
	if isvalid(s_cs_xx.parametri.parametro_w_ord_acq) then
		s_cs_xx.parametri.parametro_w_ord_acq.postevent("ue_calcola_documento")
	end if
	
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
next

end on

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

parent.title = "Dettaglio Righe Ordine "+string(ll_anno_registrazione)+"/"+string(ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	datawindow ld_datawindow
	commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	string ls_cod_tipo_det_acq, ls_modify


	ib_modifica = true

	ls_modify = "cod_tipo_det_acq.protect='1'~t"
	dw_det_ord_acq_lista.modify(ls_modify)
	ls_modify = "cod_tipo_det_acq.background.color='12632256'~t"
	dw_det_ord_acq_lista.modify(ls_modify)
	ls_modify = "cod_tipo_det_acq.protect='1'~t"
	dw_det_ord_acq_det_1.modify(ls_modify)
	ls_modify = "cod_tipo_det_acq.background.color='12632256'~t"
	dw_det_ord_acq_det_1.modify(ls_modify)
	ls_cod_tipo_det_acq = dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(), "cod_tipo_det_acq")
	ld_datawindow = dw_det_ord_acq_det_1
	lp_prod_view = pb_prod_view
	setnull(lc_prodotti_ricerca)
	f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)

	if not isnull(dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(),"cod_prodotto")) then
		//pulsante prodotti note ricerca direttamente su dw
		try
			dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = true
		catch (throwable err2)
		end try
		cb_prodotti_note_ricerca.enabled = true
	else
		//pulsante prodotti note ricerca direttamente su dw
		try
			dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
		catch (throwable err3)
		end try
	end if
	
	cb_sconti.enabled = true
	
	//pulsanti direttamente sulla dw
	try
		//Donato 30/01/2012
		//TULIO!!!! se ti piglio te tronkooooo!!!!!
		//dw_det_ord_acq_det_3.object.b_commesse_ric.enabled = true
		dw_det_ord_acq_det_3.object.b_ricerca_commessa.enabled = true
		
		dw_det_ord_acq_det_3.object.b_annulla_commessa.enabled = true
		
		
		dw_det_ord_acq_det_1.object.b_saldo.enabled = false
	catch (throwable err)
	end try
//	cb_commesse_ric.enabled = true
	cb_annulla_commessa.enabled = true
	
	cb_prezzo.enabled=true
	cb_blocca.enabled=false
	cb_sblocca.enabled=false
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
	
	object.b_duplica.enabled = false
	
	wf_proteggi_colonne(getitemnumber(getrow(),"quan_arrivata"))
	
end if

wf_tasti_funzione(this.getcolumnname())
wf_modifica_qta()

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i1,ll_prog_riga_ord_acq
	string  ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_ord_acq,ls_cod_tipo_det_acq,ls_test
	integer li_risposta

	ll_i = i_parentdw.i_selectedrows[1]
	ll_i1 = getrow()

	ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_acq = getitemnumber(ll_i1, "prog_riga_ordine_acq")
	ls_cod_tipo_det_acq = getitemstring(ll_i1, "cod_tipo_det_acq")
	ls_cod_tipo_ord_acq = i_parentdw.getitemstring(ll_i, "cod_tipo_ord_acq")
	
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_ord_acq
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;

	select cod_azienda
	into   :ls_test
	from 	 det_ord_acq_stat
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_ordine_acq=:ll_prog_riga_ord_acq;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione(ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_det_acq,ll_num_registrazione,ll_anno_registrazione,ll_prog_riga_ord_acq,2)
		if li_risposta=-1 then
			messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
		end if
	end if

end if
end event

event pcd_validaterow;call super::pcd_validaterow;if this.getitemstring(this.getrow(), "flag_blocco") = "S" then
   messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if



// --------------------  aggiunto enrico 16.09.98 ------------------------------------

string ls_modify, ls_flag_tipo_det_acq, ls_cod_tipo_det_acq, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_acq = this.getitemstring(i_rownbr,"cod_tipo_det_acq")

   select tab_tipi_det_acq.flag_tipo_det_acq
   into   :ls_flag_tipo_det_acq
   from   tab_tipi_det_acq
   where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

   if sqlca.sqlcode <> 0 then
      messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Acquisto.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_acq = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Codice prodotto obbligatorio.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Unità di misura obbligatoria.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_ordinata")
         if dec(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	messagebox("Attenzione", "Quantità ordinata obbligatoria.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_acq = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Codice prodotto obbligatorio.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
		datawindow				ld_datawindow
		commandbutton		lc_prodotti_ricerca
		picturebutton			lp_prod_view
		
		string				ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_iva_esente, ls_cod_iva_prodotto, ls_cod_tipo_listino_prodotto, &
							ls_cod_fornitore, ls_cod_valuta, ls_flag_tipo_det_acq, ls_modify, ls_null, ls_cod_tipo_det_acq, ls_flag_decimali, &
							ls_cod_prod_fornitore, ls_cod_misura_mag, ls_nota_prodotto,ls_flag_tipo_det_riga, ls_des_prodotto, ls_flag_tipo_det_ven, &
							ls_flag_blocco, ls_nota, ls_nota_testata, ls_stampa_piede, ls_nota_video
		long				ll_riga_origine, ll_i
		
		dec{4}			ld_quantita, ld_cambio_acq, ld_quan_minima, ld_inc_ordine,&
							ld_quan_massima, ld_tempo_app, ld_qta_vecchia, ld_qta_evasa, ld_salva_prezzo
		dec{5}			ld_fat_conversione
		
		datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	
	
	setnull(ls_null)
	ls_cod_tipo_listino_prodotto = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
	ldt_data_registrazione = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	ls_cod_fornitore = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
	ls_cod_valuta = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
	ld_cambio_acq = dw_det_ord_acq_lista.i_parentdw.getitemnumber(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cambio_acq")
	ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")
   
	choose case i_colname
		//----------------------------------------------------------------------------------------------------------------
		case "cod_imballo1", "cod_imballo2"
			wf_componi_nota(i_rownbr, data, i_colname)
		
		//----------------------------------------------------------------------------------------------------------------
		case "prog_riga_ordine_acq"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_ordine_acq") then
	            messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_ordine_acq", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_ordine_acq", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
		
		//----------------------------------------------------------------------------------------------------------------
		case "cod_tipo_det_acq"
			ls_modify = "cod_prodotto.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_prodotto.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_misura.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_misura.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "fat_conversione.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "fat_conversione.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "quan_ordinata.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "quan_ordinata.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "prezzo_acquisto.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "prezzo_acquisto.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_1.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_1.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_2.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_2.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_iva.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_iva.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna_fornitore.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna_fornitore.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_prod_fornitore.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_prod_fornitore.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
   
			ld_datawindow = dw_det_ord_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
			
			f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
			
			if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
				//pulsante prodotti note ricerca direttamente su dw
				try
					dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
				catch (throwable err2)
				end try
				cb_prodotti_note_ricerca.enabled=false
			end if

			select tab_tipi_det_acq.cod_iva  
			into   :ls_cod_iva  
			from   tab_tipi_det_acq  
			where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
			if sqlca.sqlcode = 0 then
			  this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
			end if
 			dw_det_ord_acq_det_2.setitem(dw_det_ord_acq_det_2.getrow(), "nota_dettaglio", ls_null)
			 
		//----------------------------------------------------------------------------------------------------------------
		case "cod_prodotto"

			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

			setnull(ls_cod_iva)
			setnull(ls_cod_iva_prodotto)
			select		des_prodotto,
						cod_misura_mag,
						cod_misura_acq,
						fat_conversione_acq,
						flag_decimali,
						cod_iva,
						quan_minima,
						inc_ordine,
						quan_massima,
						tempo_app,
						flag_blocco
			into   	:ls_des_prodotto,
					:ls_cod_misura_mag,
					:ls_cod_misura,
					:ld_fat_conversione,
					:ls_flag_decimali,
					:ls_cod_iva_prodotto,
					:ld_quan_minima,
					:ld_inc_ordine,
					:ld_quan_massima,
					:ld_tempo_app,
					:ls_flag_blocco
			from   anag_prodotti
			where  	cod_azienda = :s_cs_xx.cod_azienda and 
						cod_prodotto = :i_coltext;
   
			if sqlca.sqlcode = 0 then
				
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "ORD_ACQ", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
				
				// 17/06/2008 aggiunto segnalazione blocco prodotto su richiesta di beatrice.
				if ls_flag_blocco = "S" then
					g_mb.messagebox("APICE","Attenzione: prodotto bloccato da anagrafica prodotti")
				end if
				
				this.setitem(i_rownbr, "cod_misura", ls_cod_misura)

				dw_det_ord_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_ord_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
	
				if ld_fat_conversione <> 0 then
					this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
				else
					this.setitem(i_rownbr, "fat_conversione", 1)
				end if

				wf_set_disegno(i_rownbr, i_coltext)

				select flag_tipo_des_riga  
				into   :ls_flag_tipo_det_riga  
				from   con_ord_acq  
				where  cod_azienda = :s_cs_xx.cod_azienda;

				if ls_flag_tipo_det_riga  = "S" and sqlca.sqlcode = 0 then
					this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)
				end if
			else
				dw_det_ord_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_ord_acq_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_ord_acq_lista.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_ord_acq_lista,"cod_prodotto")	
			end if

			if not isnull(this.getitemstring(i_rownbr, "cod_misura")) then
				cb_prezzo.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			else
				cb_prezzo.text= "Prezzo"
			end if

			select anag_fornitori.cod_iva,
					 anag_fornitori.data_esenzione_iva
			into   :ls_cod_iva_esente,
					 :ldt_data_esenzione_iva
			from   anag_fornitori
			where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_fornitori.cod_fornitore = :ls_cod_fornitore;

			if sqlca.sqlcode = 0 then											// fornitore esente da iva
				if not isnull(ls_cod_iva_esente) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva_esente)
				else																	// fornitore soggetto ad iva
					if not isnull(ls_cod_iva_prodotto) then				// iva inserita in anag_prodotti
						this.setitem(i_rownbr, "cod_iva", ls_cod_iva_prodotto)
					else																// iba da tipo dettaglio
						ls_cod_tipo_det_acq = this.getitemstring(getrow(),"cod_tipo_det_acq")
						if not isnull(ls_cod_tipo_det_acq) then
								select cod_iva
								into   :ls_cod_iva
								from   tab_tipi_det_acq
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
						end if
						
						this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
					end if
				end if
			end if

			ls_cod_prodotto = i_coltext
			ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
			else
				ldt_data_consegna = ldt_data_registrazione
			end if
			if ls_flag_decimali = "N" and &
				ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				this.setitem(i_rownbr, "quan_ordinata", ld_quantita)
			end if

			change_dw_current()
         
			ld_salva_prezzo = getitemdecimal(i_rownbr, "prezzo_acquisto")
			
			f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)
			
			if is_NDA="N" and (getitemdecimal(i_rownbr, "prezzo_acquisto")<=0 or isnull(getitemdecimal(i_rownbr, "prezzo_acquisto"))) then
				//rimetti il prezzo che c'era prima, almeno era un valore diverso da zero
				setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
			end if

			if ld_quantita < ld_quan_minima and ld_quan_minima > 0 then
				this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
			end if
			if ld_quantita > ld_quan_massima and ld_quan_massima > 0 then
				this.setitem(i_rownbr, "quan_ordinata", ld_quan_massima)
			end if
			if ld_inc_ordine > 0 then
				this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
			end if
			if relativedate(date(ldt_data_registrazione), ld_tempo_app) > date(this.getitemdatetime(i_rownbr, "data_consegna")) and ld_tempo_app > 0 and &
				not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
								exclamation!, ok!)
			end if

			if not isnull(i_coltext) then
				//pulsante prodotti note ricerca direttamente su dw
				try
					dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = true
				catch (throwable err3)
				end try
				cb_prodotti_note_ricerca.enabled = true
			else
				//pulsante prodotti note ricerca direttamente su dw
				try
					dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
				catch (throwable err4)
				end try
				cb_prodotti_note_ricerca.enabled = false
			end if

			select tab_prod_fornitori.cod_prod_fornitore
			into   :ls_cod_prod_fornitore
			from   tab_prod_fornitori
			where  tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_prod_fornitori.cod_prodotto = :i_coltext and
					 tab_prod_fornitori.cod_fornitore = :ls_cod_fornitore;

			if sqlca.sqlcode = 0 then
				this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
			else
				this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
			end if

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text=''")
			end if

			select  anag_prodotti_note.nota_prodotto  
			into    :ls_nota_prodotto  
			from    anag_prodotti_note  
			where ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         		( anag_prodotti_note.cod_prodotto = :ls_cod_prodotto ) AND  
         		( anag_prodotti_note.cod_nota_prodotto = 
							 ( select parametri_azienda.stringa  
								from parametri_azienda  
								where (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								      (parametri_azienda.flag_parametro = 'S' ) AND  
                              (parametri_azienda.cod_parametro = 'CNP' )  )) ;
			if sqlca.sqlcode = 0 then
				dw_det_ord_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_ord_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if
			
			wf_componi_nota(i_rownbr, data, "cod_prodotto")
		
		//----------------------------------------------------------------------------------------------------------------
		case "cod_misura"
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo.text = "Prezzo al " + i_coltext
			else
				cb_prezzo.text = "Prezzo"
			end if

			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			
			select anag_prodotti.cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext and len(trim(i_coltext)) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text=''")
			end if
		
		//----------------------------------------------------------------------------------------------------------------
		case "fat_conversione"
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			
			select anag_prodotti.cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text=''")
			end if
	
	
		//----------------------------------------------------------------------------------------------------------------
		case "quan_ordinata" 
			//*****modifica claudia
			ld_qta_vecchia =  this.getitemnumber(getrow(), "quan_ordinata")
			ld_qta_evasa = this.getitemnumber(getrow(), "quan_arrivata")
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			
			select anag_prodotti.flag_decimali,
					 anag_prodotti.quan_minima,
					 anag_prodotti.inc_ordine,
					 anag_prodotti.quan_massima
			into   :ls_flag_decimali,
					 :ld_quan_minima,
					 :ld_inc_ordine,
					 :ld_quan_massima
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
			if sqlca.sqlcode = 0 then
				if ls_flag_decimali = "N" and &
					(double(i_coltext) - int(double(i_coltext))) > 0 then
					i_coltext = string(ceiling(double(i_coltext)))
					this.setitem(i_rownbr, "quan_ordinata", double(i_coltext))
					this.settext(i_coltext)
					return 2
				end if
			end if

			ld_quantita = double(i_coltext)
			
			if ld_qta_evasa > ld_quantita then
				messagebox("Attenzione", "Non è possibile modificare la quantità, inserire un importo più alto.", exclamation!, ok!)
				rollback;
				return 1
			end if
			
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
			else
				ldt_data_consegna = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
			end if
			
			if ib_PLF then
				//se attivo il parametro multiaziendale NPA, cambiando la quantità non devo cambiare il prezzo, rileggendolo dal listino
				//stessa cosa per la nota dettaglio
				ld_salva_prezzo = getitemdecimal(i_rownbr, "prezzo_acquisto")
				f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)
	
				if ib_NPA then
					//se c'era un prezzo caricato ripristinalo
					if ld_salva_prezzo>0 then setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
				else
					if is_NDA="N" and (getitemdecimal(i_rownbr, "prezzo_acquisto")<=0 or isnull(getitemdecimal(i_rownbr, "prezzo_acquisto"))) then
						//rimetti il prezzo che c'era prima, almeno era un valore diverso da zero
						setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
					end if
				end if
			end if

			if double(i_coltext) < ld_quan_minima and ld_quan_minima > 0 then
				messagebox("Attenzione", "Quantità Ordinata inferiore alla quantità minima ordinabile: " + string(ld_quan_minima), &
								exclamation!, ok!)
			end if
			if double(i_coltext) > ld_quan_massima and ld_quan_massima > 0 then
				messagebox("Attenzione", "Quantità Ordinata superiore alla quantità massima ordinabile: " + string(ld_quan_massima), &
								exclamation!, ok!)
			end if
			if ld_inc_ordine > 0 then
				if mod(double(i_coltext),ld_inc_ordine) <> 0 then
					messagebox("Attenzione", "Proporzione incremento ordine non rispettato: " + string(ld_inc_ordine), exclamation!, ok!)
				end if
			end if
		
			ls_nota = getitemstring(i_rownbr, "nota_dettaglio")
			if ib_NPA and ls_nota <>"" and not isnull(ls_nota) then
			else
				wf_componi_nota(i_rownbr, data, "quan_ordinata")
			end if
			
		
		
		//----------------------------------------------------------------------------------------------------------------
		case "data_consegna"
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
			this.AcceptText()
			ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")			

			if ib_PLF then
				ld_salva_prezzo = getitemdecimal(i_rownbr, "prezzo_acquisto")
	
				f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)
	
				if is_NDA="N" and (getitemdecimal(i_rownbr, "prezzo_acquisto")<=0 or isnull(getitemdecimal(i_rownbr, "prezzo_acquisto"))) then
					//rimetti il prezzo che c'era prima, almeno era un valore diverso da zero
					setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
				end if
			end if
	
			select anag_prodotti.inc_ordine
			into   :ld_tempo_app
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
					 
			if relativedate(date(ldt_data_registrazione), ld_tempo_app) > date(i_coltext) and ld_tempo_app > 0 then
				messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
								exclamation!, ok!)
			end if

			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna_fornitore")) then
				if messagebox("Ordini di Acquisto", "Aggiornanre la Data di Consegna Fornitore?", question!,yesno!) = 1 then
					this.setitem(i_rownbr, "data_consegna_fornitore", ldt_data_consegna)
				end if
			else
				this.setitem(i_rownbr, "data_consegna_fornitore", ldt_data_consegna)
			end if
			
   end choose
	
end if
end event

event ue_key;call super::ue_key;long ll_row
string ls_cod_misura
decimal ld_prezzo_acquisto, ld_fat_conversione, ld_quan_ordinata

choose case this.getcolumnname()
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_acq_lista,"cod_prodotto")	
		end if
	case "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_acq_lista,"cod_prodotto")			
		end if
	case "prezzo_acquisto"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.getrow(), "cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "O"
			//-CLAUDIA 23/07/07 MIGLIERAMENTO PERCHè SI POSSA RICERCARE PER DESCRIZIONE
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
		
	case "nota_dettaglio"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
//		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
//			this.triggerevent("pcd_save")
//			this.postevent("pcd_new")
//		end if
		
	case "cod_reparto"
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
		
	case "fat_conversione"		// "prezzo_acquisto"
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 and is_NDA="S" then
			//apri la w_prezzo_um
			//cb_prezzo.triggerevent(clicked!)
			
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			ld_prezzo_acquisto = this.getitemnumber(this.getrow(),"prezzo_acquisto")
			this.accepttext()
			ld_fat_conversione = this.getitemnumber(this.getrow(),"fat_conversione")
			ld_quan_ordinata = this.getitemnumber(this.getrow(),"quan_ordinata")
			
			wf_prezzo_um(ls_cod_misura, ld_prezzo_acquisto, ld_fat_conversione, ld_quan_ordinata)
			
		end if
		
//	case "cod_imballo2"
//		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then 
//			dw_det_ord_acq_lista.setcolumn("nota_dettaglio")
//		end if
				
end choose

if key = keyF4! and keyflags = 1 then
	triggerevent("pcd_save")
	postevent("pcd_new")
end if

//DUPLICA RIGA con CTRL+SHIFT+D
if (key = keyD! and keyflags = 3) then
//or (key = keyd! and keyflags = 3) then
	ll_row = getrow()
	if ll_row>0 and object.b_duplica.enabled="1" then
		wf_duplica_riga(ll_row)
	end if
end if

//choose case this.getcolumnname()
//	case "nota_dettaglio"
//		if key = keyenter! then
//			this.triggerevent("pcd_save")
//			this.postevent("pcd_new")
//		end if
//		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
//			this.triggerevent("pcd_save")
//			this.postevent("pcd_new")
//		end if
//		
//	case "fat_conversione"		// "prezzo_acquisto"
//		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 and is_NDA="S" then
//			//apri la w_prezzo_um
//			//cb_prezzo.triggerevent(clicked!)
//			
//			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
//			ld_prezzo_acquisto = this.getitemnumber(this.getrow(),"prezzo_acquisto")
//			this.accepttext()
//			ld_fat_conversione = this.getitemnumber(this.getrow(),"fat_conversione")
//			ld_quan_ordinata = this.getitemnumber(this.getrow(),"quan_ordinata")
//			
//			wf_prezzo_um(ls_cod_misura, ld_prezzo_acquisto, ld_fat_conversione, ld_quan_ordinata)
//			
//		end if
//end choose
end event

event itemfocuschanged;call super::itemfocuschanged;if i_extendmode then
	wf_tasti_funzione(dwo.name)
end if
end event

event buttonclicked;call super::buttonclicked;if row > 0 then
	choose case dwo.name
		case "b_duplica"
			wf_duplica_riga(row)
	end choose
end if
end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	
	choose case dwo.name
		case "des_prodotto"
			if this.object.b_duplica.enabled = "0" then
				//ok sei in modifica
				setitem(row, "des_prodotto", dw_det_ord_acq_det_1.object.cf_des_prodotto[row])
			else
				g_mb.show("APICE", "E' necessario passare prima alla modalità Modifica!")
			end if
			
	end choose
	
end if
end event

type dw_folder from u_folder within w_det_ord_acq
integer x = 37
integer y = 1040
integer width = 4567
integer height = 1040
integer taborder = 60
end type

type dw_det_ord_acq_det_1 from uo_cs_xx_dw within w_det_ord_acq
integer x = 55
integer y = 1136
integer width = 3447
integer height = 796
integer taborder = 140
boolean bringtotop = true
string dataobject = "d_det_ord_acq_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	
		datawindow ld_datawindow
		commandbutton lc_prodotti_ricerca
		picturebutton lp_prod_view
		
		string			ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_iva_esente, ls_cod_iva_prodotto, ls_cod_tipo_listino_prodotto, &
						ls_cod_fornitore, ls_cod_valuta, ls_flag_tipo_det_acq, ls_modify, ls_null, ls_cod_tipo_det_acq, ls_flag_decimali, &
						ls_cod_prod_fornitore, ls_cod_misura_mag, ls_nota_prodotto, ls_flag_saldo,ls_flag_tipo_det_riga, ls_des_prodotto,&
						ls_flag_tipo_det_ven, ls_flag_stato_ordine, ls_cod_tipo_politica_riordino, ls_flag_modo_riordino,ls_flag_blocco, ls_nota,&
						ls_nota_testata, ls_stampa_piede, ls_nota_video
						
		long			ll_riga_origine, ll_i, ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_acq, &
						ll_cont=0, ll_evasi=0, ll_parziali=0, ll_aperti=0
						
		dec{4}		ld_quantita, ld_cambio_acq, ld_quan_minima, ld_inc_ordine, ld_quan_massima, ld_tempo_app, &
						ld_qta_vecchia ,	ld_qta_evasa, ld_lotto_economico, ld_salva_prezzo
		dec{5}		ld_fat_conversione
		
		datetime		ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva


   setnull(ls_null)
   ls_cod_tipo_listino_prodotto = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_fornitore = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
   ls_cod_valuta = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_acq = dw_det_ord_acq_lista.i_parentdw.getitemnumber(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cambio_acq")
   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")


   choose case i_colname
		//----------------------------------------------------------------------------------------------------------------
		case "prog_riga_ordine_acq"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_ordine_acq") then
					messagebox("Attenzione", "Progressivo riga già utilizzato.", exclamation!, ok!)
					i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_ordine_acq", primary!,true))
					this.setitem(ll_riga_origine, "prog_riga_ordine_acq", double(i_coltext))
					this.settext(i_coltext)
					return 2
					pcca.error = c_fatal
				end if
			next

		//----------------------------------------------------------------------------------------------------------------
		case "cod_tipo_det_acq"
			ls_modify = "cod_prodotto.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_prodotto.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_misura.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_misura.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "fat_conversione.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "fat_conversione.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "quan_ordinata.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "quan_ordinata.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "prezzo_acquisto.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "prezzo_acquisto.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_1.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_1.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_2.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "sconto_2.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_iva.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_iva.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna_fornitore.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "data_consegna_fornitore.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_prod_fornitore.protect='0'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
			ls_modify = "cod_prod_fornitore.background.color='16777215'~t"
			dw_det_ord_acq_det_1.modify(ls_modify)
		
			ld_datawindow = dw_det_ord_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
			
			if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
				//pulsante prodotti note ricerca direttamente su dw
				try
					dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
				catch (throwable err2)
				end try
				cb_prodotti_note_ricerca.enabled=false
			end if
	
			if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
				  select tab_tipi_det_acq.cod_iva  
				  into   :ls_cod_iva  
				  from   tab_tipi_det_acq  
				  where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
							tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
				  if sqlca.sqlcode = 0 then
					  this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
				  end if
			end if
			dw_det_ord_acq_det_2.setitem(dw_det_ord_acq_det_2.getrow(), "nota_dettaglio", ls_null)
			
		//----------------------------------------------------------------------------------------------------------------
     	case "cod_prodotto"
prodotto:
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
			
			setnull(ls_cod_iva_prodotto)
			select 	des_prodotto,
						cod_misura_mag,
						cod_misura_acq,
						fat_conversione_acq,
						flag_decimali,
						cod_iva,
						quan_minima,
						inc_ordine,
						quan_massima,
						tempo_app,
						flag_blocco
			into		:ls_des_prodotto,
						:ls_cod_misura_mag,
						:ls_cod_misura,
						:ld_fat_conversione,
						:ls_flag_decimali,
						:ls_cod_iva_prodotto,
						:ld_quan_minima,
						:ld_inc_ordine,
						:ld_quan_massima,
						:ld_tempo_app,
						:ls_flag_blocco
			from   anag_prodotti
			where	anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
						anag_prodotti.cod_prodotto = :i_coltext;
   
			if sqlca.sqlcode = 0 then
				
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "ORD_ACQ", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
				if ls_flag_blocco = "S" then
					g_mb.messagebox("APICE","Attenzione: prodotto bloccato da anagrafica prodotti")
				end if

           		this.setitem(i_rownbr, "cod_misura", ls_cod_misura)

  				dw_det_ord_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
  				dw_det_ord_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
				
				if ld_fat_conversione <> 0 then
              		this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
				else
               		this.setitem(i_rownbr, "fat_conversione", 1)
            		end if

				wf_set_disegno(i_rownbr, i_coltext)

				select flag_tipo_des_riga  
				into   :ls_flag_tipo_det_riga  
				from   con_ord_acq  
				where  cod_azienda = :s_cs_xx.cod_azienda;

				if ls_flag_tipo_det_riga  = "S" and sqlca.sqlcode = 0 then
               		this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)
				end if
			else
				dw_det_ord_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_ord_acq_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_ord_acq_lista.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_ord_acq_det_1,"cod_prodotto")	
         	end if

			if not isnull(this.getitemstring(i_rownbr, "cod_misura")) then
				cb_prezzo.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			else
				cb_prezzo.text= "Prezzo"
			end if

			select		anag_fornitori.cod_iva,
						anag_fornitori.data_esenzione_iva
			into	:ls_cod_iva_esente,
					:ldt_data_esenzione_iva
			from	anag_fornitori
			where	anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
						anag_fornitori.cod_fornitore = :ls_cod_fornitore;
						
			if sqlca.sqlcode = 0 then											// fornitore esente da iva
				if not isnull(ls_cod_iva_esente) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva_esente)
				else																	// il fornitore è soggetto a iva
					if not isnull(ls_cod_iva_prodotto) then				// iva inserita in anagrafica del prodotto
						this.setitem(i_rownbr, "cod_iva", ls_cod_iva_prodotto)
					else																// iva del tipo dettaglio prodotto
						ls_cod_tipo_det_acq = this.getitemstring(getrow(),"cod_tipo_det_acq")
						if not isnull(ls_cod_tipo_det_acq) then
							select cod_iva
							into   :ls_cod_iva
							from   tab_tipi_det_acq
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
						end if
						this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
					end if
				end if
			end if

			ls_cod_prodotto = i_coltext
			ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
			
			// Modifica Daniele 23 Aprile 2008
			select lotto_economico, cod_tipo_politica_riordino
			into :ld_lotto_economico, :ls_cod_tipo_politica_riordino
			from anag_prodotti
			where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and
					anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "APICE", "Errore in ricerca del prodotto in anagrafica prodotti~r~n" + sqlca.sqlerrtext )
			end if
				
			if not isnull(ld_lotto_economico) and ld_lotto_economico > 0 and not isnull(ls_cod_tipo_politica_riordino) then 
				select flag_modo_riordino
				into :ls_flag_modo_riordino
				from tab_tipi_politiche_riordino
				where cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox( "APICE", "Errore in ricerca politiche riordino~r~n" + sqlca.sqlerrtext )
				end if
					
				if ls_flag_modo_riordino = 'R' then
					setitem(i_rownbr, "quan_ordinata", ld_lotto_economico)
				else 
					ld_quantita = 1
					this.setitem(i_rownbr, "quan_ordinata", ld_quantita)
				end if		
			else 
					ld_quantita = 1
					this.setitem(i_rownbr, "quan_ordinata", ld_quantita)
			end if
		
			
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
			else
				ldt_data_consegna = ldt_data_registrazione
			end if
			
			if ls_flag_decimali = "N" and &
				ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				this.setitem(i_rownbr, "quan_ordinata", ld_quantita)
			end if

			change_dw_current()

			ld_salva_prezzo = getitemdecimal(i_rownbr, "prezzo_acquisto")
		
			f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)

			if is_NDA="N" and (getitemdecimal(i_rownbr, "prezzo_acquisto")<=0 or isnull(getitemdecimal(i_rownbr, "prezzo_acquisto"))) then
				//rimetti il prezzo che c'era prima, almeno era un valore diverso da zero
				setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
			end if

			if ld_quantita < ld_quan_minima and ld_quan_minima > 0 then
				this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
			end if
			if ld_quantita > ld_quan_massima and ld_quan_massima > 0 then
				this.setitem(i_rownbr, "quan_ordinata", ld_quan_massima)
			end if
			if ld_inc_ordine > 0 then
				this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
			end if
			if relativedate(date(ldt_data_registrazione), ld_tempo_app) > date(this.getitemdatetime(i_rownbr, "data_consegna")) and ld_tempo_app > 0 and &
				not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
								exclamation!, ok!)
			end if

			if not isnull(i_coltext) then
			//pulsante prodotti note ricerca direttamente su dw
			try
				dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = true
			catch (throwable err3)
			end try
			cb_prodotti_note_ricerca.enabled = true
			else
			//pulsante prodotti note ricerca direttamente su dw
			try
				dw_det_ord_acq_det_2.object.b_prodotti_note_ricerca.enabled = false
			catch (throwable err4)
			end try
			cb_prodotti_note_ricerca.enabled = false
			end if

			select tab_prod_fornitori.cod_prod_fornitore
			into   :ls_cod_prod_fornitore
			from   tab_prod_fornitori
			where  tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_prod_fornitori.cod_prodotto = :i_coltext and
					 tab_prod_fornitori.cod_fornitore = :ls_cod_fornitore;

			if sqlca.sqlcode = 0 then
				this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
			else
				this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
			end if

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text=''")
			end if

			select  anag_prodotti_note.nota_prodotto  
			into    :ls_nota_prodotto  
			from    anag_prodotti_note  
			where ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         		( anag_prodotti_note.cod_prodotto = :ls_cod_prodotto ) AND  
         		( anag_prodotti_note.cod_nota_prodotto = 
							 ( select parametri_azienda.stringa  
								from parametri_azienda  
								where (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								      (parametri_azienda.flag_parametro = 'S' ) AND  
                              (parametri_azienda.cod_parametro = 'CNP' )  )) ;
			if sqlca.sqlcode = 0 then
				dw_det_ord_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_ord_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if
			
			wf_componi_nota(i_rownbr, data, "cod_prodotto")
		
		
		//----------------------------------------------------------------------------------------------------------------
		case "cod_misura"
			
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo.text = "Prezzo al " + i_coltext
			else
				cb_prezzo.text = "Prezzo"
			end if

			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			select anag_prodotti.cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext and len(trim(i_coltext)) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text=''")
			end if
		
		
		//----------------------------------------------------------------------------------------------------------------
		
		case "fat_conversione"
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			select anag_prodotti.cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_ord_acq_det_1.modify("st_fattore_conv.text=''")
			end if
	
	
		//----------------------------------------------------------------------------------------------------------------
		case "quan_ordinata"
			
			//*****modifica claudia
			ld_qta_vecchia =  this.getitemnumber(getrow(), "quan_ordinata")
			ld_qta_evasa = this.getitemnumber(getrow(), "quan_arrivata")
        		ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				  
			select anag_prodotti.flag_decimali,
					 anag_prodotti.quan_minima,
					 anag_prodotti.inc_ordine,
					 anag_prodotti.quan_massima
			into   :ls_flag_decimali,
					 :ld_quan_minima,
					 :ld_inc_ordine,
					 :ld_quan_massima
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
			if sqlca.sqlcode = 0 then
				if ls_flag_decimali = "N" and &
					(double(i_coltext) - int(double(i_coltext))) > 0 then
					i_coltext = string(ceiling(double(i_coltext)))
					this.setitem(i_rownbr, "quan_ordinata", double(i_coltext))
					this.settext(i_coltext)
					return 2
				end if
			end if

			ld_quantita = double(i_coltext)
			
			if ld_qta_evasa > ld_quantita then
				messagebox("Attenzione", "Non è possibile modificare la quantità, inserire un importo più alto.", exclamation!, ok!)
				rollback;
				return 1
			end if
			
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
			else
				ldt_data_consegna = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
			end if
		
			if ib_PLF then
				//se attivo il parametro multiaziendale NPA, cambiando la quantità non devo cambiare il prezzo, rileggendolo dal listino
				//stessa cosa per la nota dettaglio
				ld_salva_prezzo = getitemdecimal(i_rownbr, "prezzo_acquisto")
				f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)
			
				if ib_NPA then
					//se c'era un prezzo caricato ripristinalo
					if ld_salva_prezzo>0 then setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
				else
					if is_NDA="N" and (getitemdecimal(i_rownbr, "prezzo_acquisto")<=0 or isnull(getitemdecimal(i_rownbr, "prezzo_acquisto"))) then
						//rimetti il prezzo che c'era prima, almeno era un valore diverso da zero
						setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
					end if
				end if
			end if

			if double(i_coltext) < ld_quan_minima and ld_quan_minima > 0 then
				messagebox("Attenzione", "Quantità Ordinata inferiore alla quantità minima ordinabile: " + string(ld_quan_minima), &
								exclamation!, ok!)
			end if
			if double(i_coltext) > ld_quan_massima and ld_quan_massima > 0 then
				messagebox("Attenzione", "Quantità Ordinata superiore alla quantità massima ordinabile: " + string(ld_quan_massima), &
								exclamation!, ok!)
			end if
			if ld_inc_ordine > 0 then
				if mod(double(i_coltext),ld_inc_ordine) <> 0 then
					messagebox("Attenzione", "Proporzione incremento ordine non rispettato: " + string(ld_inc_ordine), exclamation!, ok!)
				end if
			end if
		
			ls_nota = getitemstring(i_rownbr, "nota_dettaglio")
			if ib_NPA and ls_nota <>"" and not isnull(ls_nota) then
			else
				wf_componi_nota(i_rownbr, data, "quan_ordinata")
			end if


		//----------------------------------------------------------------------------------------------------------------
		case "data_consegna"
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
			this.AcceptText()
			ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")			
	
			if ib_PLF then
				ld_salva_prezzo = getitemdecimal(i_rownbr, "prezzo_acquisto")
		
				f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)
		
				if is_NDA="N" and (getitemdecimal(i_rownbr, "prezzo_acquisto")<=0 or isnull(getitemdecimal(i_rownbr, "prezzo_acquisto"))) then
					//rimetti il prezzo che c'era prima, almeno era un valore diverso da zero
					setitem(i_rownbr, "prezzo_acquisto", ld_salva_prezzo)
				end if
			end if

			select anag_prodotti.inc_ordine
			into   :ld_tempo_app
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
					 
			if relativedate(date(ldt_data_registrazione), ld_tempo_app) > date(i_coltext) and ld_tempo_app > 0 then
				messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
								exclamation!, ok!)
			end if
			
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna_fornitore")) then
				if messagebox("Ordini di Acquisto", "Aggiornare la Data di Consegna Fornitore?", &
								question!,yesno!) = 1 then this.setitem(i_rownbr, "data_consegna_fornitore", ldt_data_consegna)
			else
				this.setitem(i_rownbr, "data_consegna_fornitore", ldt_data_consegna)
			end if
		
		
		//----------------------------------------------------------------------------------------------------------------
		case "cod_prod_fornitore"
			
			if isnull(getitemstring(row, "cod_prodotto")) then
				select cod_prodotto
				into   :ls_cod_prodotto
				from   tab_prod_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prod_fornitore = :i_coltext and
						 cod_fornitore = :ls_cod_fornitore;
				if sqlca.sqlcode = 0 then
					this.setitem(this.getrow(),"cod_prodotto", ls_cod_prodotto)
					beep(1)
					i_coltext = ls_cod_prodotto
					goto prodotto
				end if
			end if
			
   end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_modify, ls_flag_tipo_det_acq, ls_cod_tipo_det_acq, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_acq = this.getitemstring(i_rownbr,"cod_tipo_det_acq")

   select flag_tipo_det_acq
   into   :ls_flag_tipo_det_acq
   from   tab_tipi_det_acq
   where  cod_azienda = :s_cs_xx.cod_azienda and 
          cod_tipo_det_acq = :ls_cod_tipo_det_acq;
   if sqlca.sqlcode <> 0 then
      messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Acquisto.", exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_acq = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Codice prodotto obbligatorio.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Unità di misura obbligatoria.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_ordinata")
         if dec(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	messagebox("Attenzione", "Quantità ordinata obbligatoria.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_acq = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Codice prodotto obbligatorio.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event ue_key;call super::ue_key;string							ls_cod_valuta, ls_cod_misura, ls_DCL
long							ll_cont
double						ld_prezzo_acquisto, ld_sconto_tot, ld_sconti[], ld_fat_conversione
datetime						ldt_data_consegna
uo_condizioni_cliente		luo_condizioni_cliente

choose case this.getcolumnname()
	//------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_acq_det_1,"cod_prodotto")	
		end if
	
	//------------------------------------------------------------------------------------------------------------------------------------------
	case "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_acq_det_1,"cod_prodotto")		
		end if
	
	//------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_prod_fornitore"
		string ls_cod_prodotto, ls_cod_prod_fornitore, ls_des_prod_fornitore, ls_cod_fornitore, ls_des_prod_old, ls_null
		
		if key = keyF2!  and keyflags = 1 then
			
			ls_cod_prodotto       = this.getitemstring(this.getrow(),"cod_prodotto")
			ls_cod_prod_fornitore = this.gettext()
			ls_des_prod_fornitore = this.getitemstring(this.getrow(),"des_prodotto")
			ls_cod_fornitore      = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
			
			if isnull(ls_cod_prodotto) or isnull(ls_cod_prod_fornitore) then
				g_mb.warning("Inserire un codice prodotto fornitore e un codice prodotto di magazzino: impossibile memorizzare!")
				return
			end if
			
			setnull(ls_null)
			
			select des_prod_fornitore
			into   :ls_des_prod_old
			from   tab_prod_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_fornitore = :ls_cod_fornitore and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if sqlca.sqlcode = 0 then
				if g_mb.confirm("Per questo prodotto di magazzino esiste già un codice prodotto del fornitore: lo aggiorno?") then
					
					if isnull(ls_des_prod_fornitore) or len(ls_des_prod_fornitore) = 0 then	ls_des_prod_fornitore = ls_des_prod_old
					
					update tab_prod_fornitori
					set    cod_prod_fornitore = :ls_cod_prod_fornitore, des_prod_fornitore = :ls_des_prod_fornitore
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_fornitore = :ls_cod_fornitore and
							 cod_prodotto = :ls_cod_prodotto;
							 
					if sqlca.sqlcode <> 0 then
						g_mb.error("Errore in fase di aggiornamento codice prodotto fornitore in tabella prodotti fornitori. Dettaglio " + sqlca.sqlerrtext)
						return;
					end if
					
					//this.setitem(this.getrow(),"des_prodotto", ls_null)
					
				end if
			else
				if g_mb.confirm("Inserisco un codice prodotto del fornitore?") then
					
					insert into tab_prod_fornitori
								(cod_azienda,
								cod_fornitore,
								cod_prodotto,
								cod_prod_fornitore,
								des_prod_fornitore)
					values  (	:s_cs_xx.cod_azienda,
								:ls_cod_fornitore,
								:ls_cod_prodotto,
								:ls_cod_prod_fornitore,
								:ls_des_prod_fornitore);
								
					if sqlca.sqlcode <> 0 then
						g_mb.error("Errore in fase di aggiornamento codice prodotto fornitore in tabella prodotti fornitori. Dettaglio " + sqlca.sqlerrtext)
						return;
					end if
					
					//this.setitem(this.getrow(),"des_prodotto", ls_null)
					
				end if
			end if				
		end if
	
	//------------------------------------------------------------------------------------------------------------------------------------------
	case "prezzo_acquisto"
		
		//ricerca nello storico
		//SHIFT + F1
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.getrow(), "cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "O"
			//-CLAUDIA 23/07/07 MIGLIORAMENTO PERCHE' SI POSSA RICERCARE PER DESCRIZIONE
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
		
		//SHIFT + F2
		if key = keyF2!  and keyflags = 1 then
			if g_mb.confirm("Memorizzo il prezzo di acquisto nei listini fornitori?") then
				ls_cod_prodotto       = this.getitemstring(this.getrow(),"cod_prodotto")
				ls_cod_fornitore      = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
				ls_cod_valuta         = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
				ls_cod_misura         = this.getitemstring(this.getrow(),"cod_misura")
				ld_fat_conversione    = this.getitemnumber(this.getrow(),"fat_conversione")
				
				//**************************************************************
				//01/07/2015 Donato
				//se esiste il parametro multiaziendale DCL e vale "reg" allora devi memorizzare con la data registrazione e non con la data consegna
				guo_functions.uof_get_parametro("DCL", ls_DCL)
				
				if upper(ls_DCL) = "REG" then
					ldt_data_consegna = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				else
					//tutto come prima
					if not isnull(this.getitemdatetime(this.getrow(), "data_consegna")) then
						ldt_data_consegna = this.getitemdatetime(this.getrow(), "data_consegna")
					else
						//se la data consegna è null allora usa la data registrazione ordine
						ldt_data_consegna = dw_det_ord_acq_lista.i_parentdw.getitemdatetime(dw_det_ord_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
					end if
				end if
				//**************************************************************
				
				ld_prezzo_acquisto = double(this.gettext())
				ld_sconti[1] = this.getitemnumber(this.getrow(),"sconto_1")
				ld_sconti[2] = this.getitemnumber(this.getrow(),"sconto_2")
				ld_sconti[3] = this.getitemnumber(this.getrow(),"sconto_3")
				ld_sconti[4] = this.getitemnumber(this.getrow(),"sconto_4")
				ld_sconti[5] = this.getitemnumber(this.getrow(),"sconto_5")
				ld_sconti[6] = this.getitemnumber(this.getrow(),"sconto_6")
				ld_sconti[7] = this.getitemnumber(this.getrow(),"sconto_7")
				ld_sconti[8] = this.getitemnumber(this.getrow(),"sconto_8")
				ld_sconti[9] = this.getitemnumber(this.getrow(),"sconto_9")
				ld_sconti[10] = this.getitemnumber(this.getrow(),"sconto_10")
				luo_condizioni_cliente = CREATE uo_condizioni_cliente 
				luo_condizioni_cliente.uof_calcola_sconto_tot(ld_sconti[], ld_sconto_tot)
				destroy luo_condizioni_cliente
				
				select count(*) 
				into   :ll_cont
				from   listini_fornitori
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 cod_prodotto  = :ls_cod_prodotto and
						 cod_fornitore = :ls_cod_fornitore and
						 cod_valuta    = :ls_cod_valuta and
						 data_inizio_val = :ldt_data_consegna;
						 
				if ll_cont = 0 then
					//----------------------------------------------------------------------------------------
					//il record NON esiste, quindi faccio INSERT
					INSERT INTO listini_fornitori  
							( cod_azienda,   
							  cod_prodotto,   
							  cod_fornitore,   
							  cod_valuta,   
							  data_inizio_val,   
							  des_listino_for,   
							  quantita_1,   
							  prezzo_1,   
							  sconto_1,
							  quantita_2,prezzo_2,sconto_2,quantita_3,prezzo_3,sconto_3,quantita_4,prezzo_4,sconto_4,quantita_5,prezzo_5,sconto_5,   
							  flag_for_pref,prezzo_ult_acquisto,data_ult_acquisto,   
							  cod_misura,   
							  fat_conversione )  
					VALUES (	:s_cs_xx.cod_azienda,
									:ls_cod_prodotto,
									:ls_cod_fornitore,
									:ls_cod_valuta,
									:ldt_data_consegna,
									null,
									99999999,
									:ld_prezzo_acquisto,
									:ld_sconto_tot,
									0, 0,0,0,0,0,0,0,0,0,0,0,'N',0,null,
									:ls_cod_misura,
									:ld_fat_conversione);
							  
					if sqlca.sqlcode <> 0 then
						g_mb.error("Errore in inserimento condizione fornitore-prodotto. Dettaglio: " + sqlca.sqlerrtext)
						return
					end if
						
				else
					//----------------------------------------------------------------------------------------
					//il record già esiste, quindi chiedo se devo fare UPDATE
					if g_mb.confirm("Aggiorno la condizione del fornitore?") then
						
						update listini_fornitori
						set    prezzo_1 = :ld_prezzo_acquisto, 
								 sconto_1 = :ld_sconto_tot
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 cod_fornitore = :ls_cod_fornitore and
								 cod_valuta = :ls_cod_valuta and
								 data_inizio_val = :ldt_data_consegna;
								 
						if sqlca.sqlcode <> 0 then
							g_mb.error("Errore in aggiornamento condizione fornitore-prodotto. Dettaglio: " + sqlca.sqlerrtext)
							return
						end if
					end if
				end if			
			end if
		end if
	
	//------------------------------------------------------------------------------------------------------------------------------------------
	case "des_specifica"
		// questa opzione è stata aggiunta per PTENDA.
		// quindi solo nel caso di ptenda esiste il parametro CSO (che tra l'altro è usato anche per lo storico ordini
		// se esiste questo parametro, concedo di andare in ricerca tessuti.
		
		string ls_cod_parametro
		
		setnull(ls_cod_parametro)
		
		select cod_parametro
		into   :ls_cod_parametro
		from   parametri_azienda
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_parametro = 'CSO';
		
		if key = keyF1!  and keyflags = 1 and ls_cod_parametro = "CSO" then
		
			s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_ord_acq_det_1
			s_cs_xx.parametri.parametro_s_1 = "des_specifica"
			s_cs_xx.parametri.parametro_s_2 = "cod_prodotto"
			
			window_type_open(w_cs_xx_risposta, "w_tessuti_ricerca", 0)
			
			dw_det_ord_acq_det_1.setcolumn("cod_prodotto")
			dw_det_ord_acq_det_1.settext(s_cs_xx.parametri.parametro_s_1)
			dw_det_ord_acq_det_1.setcolumn("des_specifica")
			dw_det_ord_acq_det_1.settext(s_cs_xx.parametri.parametro_s_2)
			dw_det_ord_acq_det_1.accepttext()
			
		end if 
		
end choose


if key = keyF4! and keyflags = 1 then
	triggerevent("pcd_save")
	postevent("pcd_new")
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_det_ord_acq_lista.setrow(getrow())
end if
end event

event itemfocuschanged;call super::itemfocuschanged;if i_extendmode then
	wf_tasti_funzione(dwo.name)
	wf_proteggi_colonne(getitemnumber(getrow(),"quan_arrivata"))
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	wf_tasti_funzione(this.getcolumnname())
	wf_proteggi_colonne(getitemnumber(getrow(),"quan_arrivata"))
end if
end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	
	choose case dwo.name
		case "cf_des_prodotto"//"des_prodotto"
			if dw_det_ord_acq_lista.object.b_duplica.enabled = "0" then
				//ok sei in modifica
				setitem(row, "des_prodotto", object.cf_des_prodotto[row])
			else
				g_mb.show("APICE", "E' necessario passare prima alla modalità Modifica!")
			end if
			
	end choose
	
end if
end event

event buttonclicked;call super::buttonclicked;integer				li_anno_ordine
long					ll_num_ordine, ll_prog_riga_ordine
string					ls_flag_saldo
dec{4}				ld_quan_ordinata, ld_quan_arrivata
boolean				lb_esegui = false

if row > 0 then
	choose case dwo.name
		case "b_ricerca_prodotto"
			//get_focus
			dw_det_ord_acq_det_1.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_acq_det_1,"cod_prodotto")
			
		case "b_saldo"
			if row>0 then
				li_anno_ordine = getitemnumber(row, "anno_registrazione")
				ll_num_ordine = getitemnumber(row, "num_registrazione")
				ll_prog_riga_ordine = getitemnumber(row, "prog_riga_ordine_acq")
				ls_flag_saldo = getitemstring(row, "flag_saldo")
				ld_quan_ordinata = getitemnumber(row, "quan_ordinata")
				ld_quan_arrivata = getitemnumber(row, "quan_arrivata")
				
				if isnull(ld_quan_ordinata) then ld_quan_ordinata=0
				if isnull(ld_quan_arrivata) then ld_quan_arrivata=0
				
				if li_anno_ordine>0 and ll_num_ordine>0 and ll_prog_riga_ordine>0 and ld_quan_ordinata > ld_quan_arrivata then
					
					if not ib_supervisore then
						g_mb.warning("Solo i supervisori sono autorizzati ad effettuare questa operazione!")
						return
					end if
					
					
					if ls_flag_saldo = "S" then
						//azzerare il saldo
						ls_flag_saldo = "N"
						lb_esegui = g_mb.confirm("Impostare la riga come NON saldata?")
						
					else
						//metti come saldata
						ls_flag_saldo = "S"
						lb_esegui = g_mb.confirm("Impostare la riga come SALDATA?")
					end if
					
					if lb_esegui then
						update det_ord_acq
						set flag_saldo=:ls_flag_saldo
						where 	cod_azienda=:s_cs_xx.cod_azienda and
									anno_registrazione=:li_anno_ordine and
									num_registrazione=:ll_num_ordine and
									prog_riga_ordine_acq=:ll_prog_riga_ordine;
						
						if sqlca.sqlcode=0 then
							commit;
							
							setitem(row, "flag_saldo", ls_flag_saldo)
							setitemstatus(row, "flag_saldo", Primary!, NotModified!)
						end if
					end if
					
				end if
			end if
			
	end choose
end if
end event

type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_ord_acq
integer x = 3520
integer y = 1136
integer width = 1015
integer height = 860
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_det_ord_acq_det_note_blob"
boolean vscrollbar = true
end type

type dw_det_ord_acq_det_3 from uo_cs_xx_dw within w_det_ord_acq
integer x = 59
integer y = 1160
integer width = 3406
integer height = 600
integer taborder = 40
string dataobject = "d_det_ord_acq_det_3"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_null
long ll_null

if row>0 then
	choose case dwo.name

		case "b_ricerca_commessa"
			dw_det_ord_acq_det_3.change_dw_current()
			guo_ricerca.uof_ricerca_commessa(dw_det_ord_acq_det_3,"anno_commessa","num_commessa")
			
		case "b_annulla_commessa"
			setnull(ll_null)
			dw_det_ord_acq_lista.setitem(dw_det_ord_acq_lista.getrow(),"anno_commessa",ll_null)
			dw_det_ord_acq_lista.setitem(dw_det_ord_acq_lista.getrow(),"num_commessa",ll_null)
			dw_det_ord_acq_det_3.setitem(dw_det_ord_acq_det_3.getrow(),"anno_commessa",ll_null)
			dw_det_ord_acq_det_3.setitem(dw_det_ord_acq_det_3.getrow(),"num_commessa",ll_null)
			
	end choose
end if
end event

event clicked;call super::clicked;long										ll_num_commessa, ll_row
integer									li_anno_commessa
s_cs_xx_parametri						lstr_param
w_commesse_produzione_tv		lw_window

choose case dwo.name
	case "p_commessa"
		//apri la window delle commesse produzione
		ll_row = dw_det_ord_acq_det_3.getrow()
		
		if ll_row>0 then
			li_anno_commessa = dw_det_ord_acq_det_3.getitemnumber(ll_row, "anno_commessa")
			ll_num_commessa = dw_det_ord_acq_det_3.getitemnumber(ll_row, "num_commessa")
				
			if li_anno_commessa>0 and ll_num_commessa>0 then
				lstr_param.parametro_ul_1 = li_anno_commessa
				lstr_param.parametro_ul_2 = ll_num_commessa
				
				opensheetwithparm(lw_window, lstr_param, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_commesse_produzione_tv, lstr_param)
			else
				//nessuna commessa associata
				return
			end if
		end if
		
end choose
end event

type dw_det_ord_acq_det_2 from uo_cs_xx_dw within w_det_ord_acq
integer x = 55
integer y = 1140
integer width = 3241
integer height = 640
integer taborder = 70
string dataobject = "d_det_ord_acq_det_2"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;if row>0 then
	choose case dwo.name
		case "b_prodotti_note_ricerca"
			//get_focus
			s_cs_xx.parametri.parametro_s_1 = dw_det_ord_acq_det_1.getitemstring(dw_det_ord_acq_det_1.getrow(),"cod_prodotto")
			dw_det_ord_acq_det_2.change_dw_current()
			
			//clicked
			setnull(s_cs_xx.parametri.parametro_s_2)
			window_open(w_prod_note_ricerca, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_2) then
				pcca.window_currentdw.setcolumn("nota_dettaglio")
				pcca.window_currentdw.settext(pcca.window_currentdw.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
			end if
			
	end choose
end if
end event

