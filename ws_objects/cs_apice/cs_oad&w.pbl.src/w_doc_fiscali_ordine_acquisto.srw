﻿$PBExportHeader$w_doc_fiscali_ordine_acquisto.srw
forward
global type w_doc_fiscali_ordine_acquisto from w_cs_xx_principale
end type
type tab_1 from tab within w_doc_fiscali_ordine_acquisto
end type
type det_1 from userobject within tab_1
end type
type st_etichetta_rda from statictext within det_1
end type
type dw_lista_rda from uo_cs_xx_dw within det_1
end type
type det_1 from userobject within tab_1
st_etichetta_rda st_etichetta_rda
dw_lista_rda dw_lista_rda
end type
type det_2 from userobject within tab_1
end type
type dw_det from uo_cs_xx_dw within det_2
end type
type st_etichetta from statictext within det_2
end type
type dw_lista from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_1
dw_det dw_det
st_etichetta st_etichetta
dw_lista dw_lista
end type
type tab_1 from tab within w_doc_fiscali_ordine_acquisto
det_1 det_1
det_2 det_2
end type
end forward

global type w_doc_fiscali_ordine_acquisto from w_cs_xx_principale
integer width = 4933
integer height = 2396
string title = "Visualizzazione Doc. Collegati Ordine"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
event ue_postopen ( )
tab_1 tab_1
end type
global w_doc_fiscali_ordine_acquisto w_doc_fiscali_ordine_acquisto

type variables

end variables

event ue_postopen();string				ls_modify


ls_modify = ".font.weight=~"400~tif( anno_ordine= anno_ordine_arg and  num_ordine=num_ordine_arg, 700, 400)~""


tab_1.det_2.dw_det.modify("riga"+ls_modify)
tab_1.det_2.dw_det.modify("cf_riga_ordine"+ls_modify)
tab_1.det_2.dw_det.modify("cod_tipo_det_acq"+ls_modify)
tab_1.det_2.dw_det.modify("cod_prodotto"+ls_modify)
tab_1.det_2.dw_det.modify("des_prodotto"+ls_modify)
tab_1.det_2.dw_det.modify("quantita"+ls_modify)
tab_1.det_2.dw_det.modify("cod_misura"+ls_modify)
tab_1.det_2.dw_det.modify("prezzo_acquisto"+ls_modify)
tab_1.det_2.dw_det.modify("imponibile_iva"+ls_modify)
tab_1.det_2.dw_det.modify("sconto_1"+ls_modify)
tab_1.det_2.dw_det.modify("sconto_2"+ls_modify)
tab_1.det_2.dw_det.modify("cf_mov_mag"+ls_modify)

end event

on w_doc_fiscali_ordine_acquisto.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_doc_fiscali_ordine_acquisto.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;

tab_1.det_2.dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent + c_noNew + c_noModify + c_noDelete, &
                                    c_default)
												
tab_1.det_1.dw_lista_rda.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent + c_noNew + c_noModify + c_noDelete, &
                                    c_default + c_nohighlightselected)
												
tab_1.det_2.dw_det.set_dw_options(	sqlca, &
								pcca.null_object, &
                                    c_noretrieveOnOpen + c_noNew + c_noModify + c_noDelete + c_default, &
                                    c_default)

this.event post ue_postopen()

end event

type tab_1 from tab within w_doc_fiscali_ordine_acquisto
event create ( )
event destroy ( )
integer x = 41
integer y = 24
integer width = 4864
integer height = 2268
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
det_1 det_1
det_2 det_2
end type

on tab_1.create
this.det_1=create det_1
this.det_2=create det_2
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_1.destroy
destroy(this.det_1)
destroy(this.det_2)
end on

type det_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4827
integer height = 2144
long backcolor = 12632256
string text = "RDA"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_etichetta_rda st_etichetta_rda
dw_lista_rda dw_lista_rda
end type

on det_1.create
this.st_etichetta_rda=create st_etichetta_rda
this.dw_lista_rda=create dw_lista_rda
this.Control[]={this.st_etichetta_rda,&
this.dw_lista_rda}
end on

on det_1.destroy
destroy(this.st_etichetta_rda)
destroy(this.dw_lista_rda)
end on

type st_etichetta_rda from statictext within det_1
boolean visible = false
integer x = 46
integer y = 192
integer width = 3305
integer height = 96
integer textsize = -14
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 134217747
long backcolor = 12632256
string text = "Nessuna RDA risulta presente per quest~'ordine ..."
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_lista_rda from uo_cs_xx_dw within det_1
integer x = 37
integer y = 32
integer width = 4800
integer height = 2092
integer taborder = 30
string dataobject = "d_rda_ordine_acquisto_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;string				ls_cod_fornitore, ls_rag_soc_1, ls_tipo_doc, ls_text
long				ll_errore, ll_num_ordine, ll_num_doc_fiscale, ll_row
integer			li_anno_ordine, li_anno_doc_fiscale

li_anno_ordine = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_ordine = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")

select rag_soc_1
into   :ls_rag_soc_1
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_fornitore = :ls_cod_fornitore;
		 
if sqlca.sqlcode = 0 then
	w_doc_fiscali_ordine_acquisto.title = "Doc. collegati per Ordine " + &
						string(li_anno_ordine,"####") + "/" + &
						string(ll_num_ordine) + "  Fornitore: " + ls_rag_soc_1
end if

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_ordine, ll_num_ordine)

if ll_errore < 0 then
   pcca.error = c_fatal
	return
end if

ls_text = "RDA"

if ll_errore>0 then
	st_etichetta_rda.visible = false
	ls_text += " (Tot. " + string(ll_errore) + ")"
else
	st_etichetta_rda.visible = true
end if

parent.text = ls_text
end event

type det_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 4827
integer height = 2144
long backcolor = 12632256
string text = "DDT / Fatture"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det dw_det
st_etichetta st_etichetta
dw_lista dw_lista
end type

on det_2.create
this.dw_det=create dw_det
this.st_etichetta=create st_etichetta
this.dw_lista=create dw_lista
this.Control[]={this.dw_det,&
this.st_etichetta,&
this.dw_lista}
end on

on det_2.destroy
destroy(this.dw_det)
destroy(this.st_etichetta)
destroy(this.dw_lista)
end on

type dw_det from uo_cs_xx_dw within det_2
integer x = 32
integer y = 592
integer width = 4773
integer height = 1552
integer taborder = 30
string dataobject = "d_doc_fiscali_ordine_acquisto_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

type st_etichetta from statictext within det_2
boolean visible = false
integer x = 46
integer y = 192
integer width = 3305
integer height = 96
integer textsize = -14
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 134217747
long backcolor = 12632256
string text = "Nessun Documento di evasione (DdT o Fattura) risulta presente per quest~'ordine ..."
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_lista from uo_cs_xx_dw within det_2
integer x = 23
integer y = 12
integer width = 4005
integer height = 556
integer taborder = 10
string dataobject = "d_doc_fiscali_ordine_acquisto_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event rowfocuschanged;string				ls_tipo_doc
integer			li_anno_ordine, li_anno_doc_fiscale
long				ll_num_ordine, ll_num_doc_fiscale


if currentrow>0 then
	
	selectrow(0, false)
	selectrow(currentrow, true)
	
	li_anno_ordine = getitemnumber(currentrow, "anno_ordine")
	ll_num_ordine = getitemnumber(currentrow, "num_ordine")
	li_anno_doc_fiscale = getitemnumber(currentrow, "anno")
	ll_num_doc_fiscale = getitemnumber(currentrow, "num")
	ls_tipo_doc = upper(getitemstring(currentrow, "tipo_doc"))
	
	if ls_tipo_doc="D.D.T." then
		ls_tipo_doc = "B"
	elseif ls_tipo_doc="FATTURA" then
		ls_tipo_doc = "F"
	else
		dw_det.reset()
		return
	end if
	
	//retrieve dettagli documento fiscale
	dw_det.retrieve(s_cs_xx.cod_azienda, li_anno_doc_fiscale, ll_num_doc_fiscale, ls_tipo_doc, li_anno_ordine, ll_num_ordine)
	
	
else
	//dw_det.reset()
	//selectrow(0, false)
end if
end event

event pcd_retrieve;call super::pcd_retrieve;string				ls_tipo_doc, ls_text
long				ll_errore, ll_num_ordine, ll_num_doc_fiscale, ll_row
integer			li_anno_ordine, li_anno_doc_fiscale

li_anno_ordine = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_ordine = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_ordine, ll_num_ordine)

if ll_errore < 0 then
   pcca.error = c_fatal
	return
end if

ls_text = "DDT / Fatture"

if ll_errore>0 then
	st_etichetta.visible = false
	ls_text += " (" + string(ll_errore) + ")"
else
	st_etichetta.visible = true
end if

parent.text = ls_text
		
ll_row = getrow()

if ll_row>0 then
	li_anno_doc_fiscale = getitemnumber(ll_row, "anno")
	ll_num_doc_fiscale = getitemnumber(ll_row, "num")
	ls_tipo_doc = upper(getitemstring(ll_row, "tipo_doc"))
	
	if ls_tipo_doc="D.D.T." then
		ls_tipo_doc = "B"
	elseif ls_tipo_doc="FATTURA" then
		ls_tipo_doc = "F"
	else
		dw_det.reset()
		return
	end if
	
	//retrieve dettagli documento fiscale
	dw_det.retrieve(s_cs_xx.cod_azienda, li_anno_doc_fiscale, ll_num_doc_fiscale, ls_tipo_doc, li_anno_ordine, ll_num_ordine)
	
	
else
	dw_det.reset()
end if
end event

