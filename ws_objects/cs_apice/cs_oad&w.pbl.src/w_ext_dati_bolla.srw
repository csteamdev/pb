﻿$PBExportHeader$w_ext_dati_bolla.srw
$PBExportComments$Finestra Richiesta Dati Bolla Acquisto
forward
global type w_ext_dati_bolla from w_cs_xx_risposta
end type
type cb_ric_bol_acq from commandbutton within w_ext_dati_bolla
end type
type dw_ext_dati_bolla from uo_cs_xx_dw within w_ext_dati_bolla
end type
type cb_1 from commandbutton within w_ext_dati_bolla
end type
type cb_annulla from commandbutton within w_ext_dati_bolla
end type
end forward

global type w_ext_dati_bolla from w_cs_xx_risposta
integer width = 2263
integer height = 1628
string title = "Dati Bolla Acquisto"
event ue_imposta ( )
cb_ric_bol_acq cb_ric_bol_acq
dw_ext_dati_bolla dw_ext_dati_bolla
cb_1 cb_1
cb_annulla cb_annulla
end type
global w_ext_dati_bolla w_ext_dati_bolla

event ue_imposta();dw_ext_dati_bolla.setitem(1, "cod_deposito", s_cs_xx.parametri.parametro_s_3)
setnull(s_cs_xx.parametri.parametro_s_3)
end event

on w_ext_dati_bolla.create
int iCurrent
call super::create
this.cb_ric_bol_acq=create cb_ric_bol_acq
this.dw_ext_dati_bolla=create dw_ext_dati_bolla
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ric_bol_acq
this.Control[iCurrent+2]=this.dw_ext_dati_bolla
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.cb_annulla
end on

on w_ext_dati_bolla.destroy
call super::destroy
destroy(this.cb_ric_bol_acq)
destroy(this.dw_ext_dati_bolla)
destroy(this.cb_1)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;dw_ext_dati_bolla.set_dw_options(sqlca, &
                                 pcca.null_object,&
											c_nodelete + &
											c_newonopen + &
											c_disablecc, &
											c_default)

save_on_close(c_socnosave)

event post ue_imposta()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_dati_bolla,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ext_dati_bolla,"cod_tipo_bol_acq",sqlca,&
                 "tab_tipi_bol_acq","cod_tipo_bol_acq","des_tipo_bol_acq", &
                 "tab_tipi_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ext_dati_bolla, "cod_rag", sqlca, &
                 "tab_tipi_rag", "cod_tipo_rag", "des_tipo_rag", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + &
					  "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  s_cs_xx.db_funzioni.oggi + "))")
end event

type cb_ric_bol_acq from commandbutton within w_ext_dati_bolla
integer x = 2043
integer y = 1224
integer width = 82
integer height = 72
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;dw_ext_dati_bolla.change_dw_current()

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw

s_cs_xx.parametri.parametro_s_1 = "anno_bol_ven"

s_cs_xx.parametri.parametro_s_2 = "num_bol_ven"

s_cs_xx.parametri.parametro_s_3 = "riga_bol_ven"

window_open(w_ricerca_bolle_ven, 0)
end event

type dw_ext_dati_bolla from uo_cs_xx_dw within w_ext_dati_bolla
integer x = 23
integer y = 20
integer width = 2171
integer height = 1348
integer taborder = 20
string dataobject = "d_ext_dati_bolla"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null
setnull(ls_null)

choose case i_colname
	case "rag_singole" 
		if i_coltext = "S" then
			this.setitem(1, "cod_rag", ls_null)
		end if
	case "cod_rag"
		if isnull(i_coltext) or i_coltext = "" then
			this.setitem(1, "rag_singole", "S")
		else
			this.setitem(1, "rag_singole", "N")
		end if
	case "flag_conferma" 
		if i_coltext = "N" then
			this.setitem(1, "flag_acc_materiali", "N")
		end if
end choose
	
end event

type cb_1 from commandbutton within w_ext_dati_bolla
integer x = 1824
integer y = 1412
integer width = 361
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;datetime			ldt_data_registrazione


dw_ext_dati_bolla.accepttext()

dw_ext_dati_bolla.setcolumn(2)
dw_ext_dati_bolla.setcolumn(4)

//---------------------------------------------------------------------
if isnull(dw_ext_dati_bolla.getitemstring(1, "cod_tipo_bol_acq")) then
	g_mb.warning("Il tipo DdT è obbligatorio!")
	return
end if

//---------------------------------------------------------------------
ldt_data_registrazione = dw_ext_dati_bolla.getitemdatetime(1, "data_registrazione")

if isnull(ldt_data_registrazione) or year(date(ldt_data_registrazione))<=1950 then
	g_mb.warning("La Data Registrazione del DdT è obbligatoria, anche perchè verrà usata per registrare i carichi di magazzino alla conferma del DdT stesso!")
	return
end if

//---------------------------------------------------------------------
if isnull(dw_ext_dati_bolla.getitemstring(1, "num_bolla_acq"))  and dw_ext_dati_bolla.getitemstring(1, "flag_acc_materiali") = "S" then
	g_mb.warning("il numero DdT del fornitore è obbligatorio!")
	return
end if

//---------------------------------------------------------------------
if isnull(dw_ext_dati_bolla.getitemdatetime(1, "data_bolla_acq")) and dw_ext_dati_bolla.getitemstring(1, "flag_acc_materiali") = "S" then
	g_mb.warning("La data DdT del fornitore è obbligatoria!")
	return
end if

//---------------------------------------------------------------------
if isnull(dw_ext_dati_bolla.getitemstring(1, "cod_deposito")) then
	g_mb.warning("Il deposito è obbligatorio!")
	return
end if


s_cs_xx.parametri.parametro_s_1 = dw_ext_dati_bolla.getitemstring(1, "cod_tipo_bol_acq")
s_cs_xx.parametri.parametro_s_2 = dw_ext_dati_bolla.getitemstring(1, "num_bolla_acq")
s_cs_xx.parametri.parametro_s_3 = dw_ext_dati_bolla.getitemstring(1, "cod_deposito")
s_cs_xx.parametri.parametro_s_4 = dw_ext_dati_bolla.getitemstring(1, "cod_ubicazione")
s_cs_xx.parametri.parametro_s_5 = dw_ext_dati_bolla.getitemstring(1, "cod_lotto")
s_cs_xx.parametri.parametro_s_6 = dw_ext_dati_bolla.getitemstring(1, "rag_singole")
s_cs_xx.parametri.parametro_s_7 = dw_ext_dati_bolla.getitemstring(1, "cod_rag")
s_cs_xx.parametri.parametro_s_8 = dw_ext_dati_bolla.getitemstring(1, "flag_conferma")
s_cs_xx.parametri.parametro_s_9 = dw_ext_dati_bolla.getitemstring(1, "flag_acc_materiali")
s_cs_xx.parametri.parametro_data_1 = dw_ext_dati_bolla.getitemdatetime(1, "data_bolla_acq")
s_cs_xx.parametri.parametro_d_1 = dw_ext_dati_bolla.getitemnumber(1,"anno_bol_ven")
s_cs_xx.parametri.parametro_d_2 = dw_ext_dati_bolla.getitemnumber(1,"num_bol_ven")
s_cs_xx.parametri.parametro_d_3 = dw_ext_dati_bolla.getitemnumber(1,"riga_bol_ven")
s_cs_xx.parametri.parametro_data_2 = dw_ext_dati_bolla.getitemdatetime(1, "data_registrazione")


close(parent)
end event

type cb_annulla from commandbutton within w_ext_dati_bolla
event clicked pbm_bnclicked
integer x = 1440
integer y = 1412
integer width = 361
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_s_3)
setnull(s_cs_xx.parametri.parametro_s_4)
setnull(s_cs_xx.parametri.parametro_s_5)
setnull(s_cs_xx.parametri.parametro_s_6)
setnull(s_cs_xx.parametri.parametro_s_7)
setnull(s_cs_xx.parametri.parametro_s_8)
setnull(s_cs_xx.parametri.parametro_s_9)
setnull(s_cs_xx.parametri.parametro_data_1)

close(parent)
end event

