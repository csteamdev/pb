﻿$PBExportHeader$w_gen_bolla_uscita_lavorazione.srw
forward
global type w_gen_bolla_uscita_lavorazione from w_std_principale
end type
type tab_1 from tab within w_gen_bolla_uscita_lavorazione
end type
type tabpage_1 from userobject within tab_1
end type
type dw_lista_bolle_pronte from datawindow within tabpage_1
end type
type cb_nuova_bolla from commandbutton within tabpage_1
end type
type cb_5 from commandbutton within tabpage_1
end type
type cb_aggiungi_a_bolla from commandbutton within tabpage_1
end type
type dw_ext_dati_bolla from uo_std_dw within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
cb_5 cb_5
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
end type
type tabpage_3 from userobject within tab_1
end type
type st_1 from statictext within tabpage_3
end type
type cb_conferma from commandbutton within tabpage_3
end type
type cb_deseleziona from commandbutton within tabpage_3
end type
type cb_seleziona from commandbutton within tabpage_3
end type
type dw_righe_ordine from datawindow within tabpage_3
end type
type tabpage_3 from userobject within tab_1
st_1 st_1
cb_conferma cb_conferma
cb_deseleziona cb_deseleziona
cb_seleziona cb_seleziona
dw_righe_ordine dw_righe_ordine
end type
type tab_1 from tab within w_gen_bolla_uscita_lavorazione
tabpage_1 tabpage_1
tabpage_3 tabpage_3
end type
end forward

global type w_gen_bolla_uscita_lavorazione from w_std_principale
integer y = 200
integer width = 3657
integer height = 2000
string title = "Generazione DDT lavorazione da Ordini Fornitori"
tab_1 tab_1
end type
global w_gen_bolla_uscita_lavorazione w_gen_bolla_uscita_lavorazione

type variables
string  is_flag_bol_fat

boolean ib_anticipo, ib_evasione_parziale
long il_anno_registrazione[], il_num_registrazione[]

s_gen_bolla_uscita_lavorazione is_dati

// stefanop 09/05/2010: progetto 140
private:
	string is_tipo_default = ""
	boolean ib_change_mov = false
end variables

forward prototypes
public subroutine wf_carica_righe_ordine ()
public function integer wf_controlla_movimenti (string as_cod_tipo_bol_fat, ref string as_errore)
end prototypes

public subroutine wf_carica_righe_ordine ();string ls_cod_prodotto, ls_cod_tipo_det_acq, ls_cod_misura, ls_des_prodotto, ls_cod_misura_mag, ls_sql
long ll_prog_riga_ordine_acq,ll_riga, ll_ret, ll_rows, ll_i
dec{4} ld_quan_ordinata, ld_prezzo_acquisto, ld_imponibile_iva, ld_quan_arrivata, ld_sconto_1, ld_sconto_2
datetime ldt_data_consegna
datastore lds_righe_ord_acq

ls_sql = " SELECT prog_riga_ordine_acq, cod_prodotto,  cod_tipo_det_acq, cod_misura, des_prodotto, quan_ordinata, prezzo_acquisto, sconto_1, sconto_2, data_consegna, imponibile_iva, quan_arrivata " + &
			" FROM det_ord_acq " + &
			" WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &  
					 " anno_registrazione = " + string(il_anno_registrazione[1] ) + " and " + &  
					 " num_registrazione  = " + string(il_num_registrazione[1]  ) + " and " + &  
					 " (quan_ordinata - quan_arrivata) > 0 and flag_saldo = 'N' " + &
			" order by prog_riga_ordine_acq ASC "


ll_rows = guo_functions.uof_crea_datastore(ref lds_righe_ord_acq, ls_sql)

if ll_rows = 0 then
	rollback;
	return
end if

ll_rows = lds_righe_ord_acq.retrieve()

for ll_i = 1 to ll_rows
	
	ll_prog_riga_ordine_acq = lds_righe_ord_acq.getitemnumber(ll_i, 1)
	ls_cod_prodotto			= lds_righe_ord_acq.getitemstring(ll_i, 2)
	ls_cod_tipo_det_acq		= lds_righe_ord_acq.getitemstring(ll_i, 3)
	ls_cod_misura				= lds_righe_ord_acq.getitemstring(ll_i, 4)
	ls_des_prodotto			= lds_righe_ord_acq.getitemstring(ll_i, 5)
	ld_quan_ordinata			= lds_righe_ord_acq.getitemnumber(ll_i, 6)
	ld_prezzo_acquisto		= lds_righe_ord_acq.getitemnumber(ll_i, 7)
	ld_sconto_1					= lds_righe_ord_acq.getitemnumber(ll_i, 8)
	ld_sconto_2					= lds_righe_ord_acq.getitemnumber(ll_i, 9)
	ldt_data_consegna			= lds_righe_ord_acq.getitemdatetime(ll_i, 10)
	ld_imponibile_iva			= lds_righe_ord_acq.getitemnumber(ll_i, 11)
	ld_quan_arrivata			= lds_righe_ord_acq.getitemnumber(ll_i, 12)
	
	
	if not isnull(ls_cod_prodotto) then
		select cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
	end if	
	
	ll_riga = tab_1.tabpage_3.dw_righe_ordine.insertrow(0)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prog_riga_ordine_acq", ll_prog_riga_ordine_acq)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_tipo_det_acq", ls_cod_tipo_det_acq)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_misura_mag", ls_cod_misura_mag)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_misura_ven", ls_cod_misura)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_ordinata", ld_quan_ordinata)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prezzo_acquisto", ld_prezzo_acquisto)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "sconto_1", ld_sconto_1)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "sconto_2", ld_sconto_2)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_arrivata", ld_quan_arrivata)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "val_riga", ld_imponibile_iva)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "data_consegna", ldt_data_consegna)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_residua", ld_quan_ordinata - ld_quan_arrivata)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_da_evadere", 0)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "flag_evasione_totale", "N")
next

destroy lds_righe_ord_acq;

// si tratta di evasione totale
if not ib_evasione_parziale then
	tab_1.tabpage_3.cb_seleziona.event clicked( )
	tab_1.tabpage_3.cb_conferma.postevent("clicked")
end if

return
end subroutine

public function integer wf_controlla_movimenti (string as_cod_tipo_bol_fat, ref string as_errore);/**
 * Stefano
 * 10/06/2010 
 *
 * Controllo se il tipo di movimento è lo steso oppure avviso e chiedo conferma all'utente del cambio
 * Ritorno:
 * -1 Errore
 *  1 SI
 *  2 NO
 **/
 
string ls_cod_tipo_movimento, ls_des_tipo_movimento, ls_flag_tipo_det_ven

// controllo il tipo det ven che deve essere "Prodotti a magazzino"
if is_flag_bol_fat = "B" then
	select flag_tipo_det_ven, cod_tipo_movimento
	into :ls_flag_tipo_det_ven, :ls_cod_tipo_movimento
	from tab_tipi_det_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_det_ven in (
			select cod_tipo_det_ven
			from tab_tipi_bol_ven
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_bol_ven = :as_cod_tipo_bol_fat
			);
else
	select flag_tipo_det_ven, cod_tipo_movimento
	into :ls_flag_tipo_det_ven, :ls_cod_tipo_movimento
	from tab_tipi_det_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_det_ven in (
			select cod_tipo_det_ven
			from tab_tipi_bol_ven
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_fat_ven = :as_cod_tipo_bol_fat
			);
end if

if sqlca.sqlcode <> 0 then
	as_errore = "Errore durante il controllo del tipo dettaglio.~r~n" + sqlca.sqlerrtext
	return -1
elseif isnull(ls_flag_tipo_det_ven) or ls_flag_tipo_det_ven <> 'M' then
	as_errore = "Attenzione: il tipo dettaglio deve consentire un movimento di magazzino"
	return -1
end if

// recupero il tipo di movimento per chiedere conferma sulla sostituzione
select des_tipo_movimento
into :ls_des_tipo_movimento
from tab_tipi_movimenti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_movimento = :ls_cod_tipo_movimento;
	
if sqlca.sqlcode <> 0 then
	as_errore = "Errore durante il recupero del dettaglio di movimento"
	return -1
end if
// ----

if g_mb.confirm("APICE", "Attenzione: si vuole sotituire i movimenti di magazzino con il seguente?~r~n" + ls_cod_tipo_movimento + " - " + ls_des_tipo_movimento) then
	return 1
else
	return 2
end if
end function

on w_gen_bolla_uscita_lavorazione.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_gen_bolla_uscita_lavorazione.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;string ls_cod_fornitore, ls_cod_tipo_ord_acq
long ll_ret


tab_1.tabpage_1.dw_lista_bolle_pronte.settransobject(sqlca)
tab_1.tabpage_1.dw_lista_bolle_pronte.setrowfocusindicator(FocusRect!)

il_anno_registrazione[1] = is_dati.anno_ord_acq
il_num_registrazione[1] = is_dati.num_ord_acq

select cod_fornitore,
		 cod_tipo_ord_acq
into   :ls_cod_fornitore,
		 :ls_cod_tipo_ord_acq
from   tes_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = : il_anno_registrazione[1] and
		 num_registrazione = :il_num_registrazione[1];
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo ordine in testata ordine. "  + sqlca.sqlerrtext)
	postevent("close")
	return
end if


ll_ret = g_mb.messagebox("APICE", "Procedo con la spedizione TOTALE di questo in lavorazione presso il terzista?",Question!, YesNo!,1)

if ll_ret = 1 then
	ib_evasione_parziale = false
	tab_1.tabpage_1.enabled = true
	tab_1.SelectTab(1)			
else
	ib_evasione_parziale = true
	tab_1.tabpage_1.enabled = false
	tab_1.SelectTab(2)			
end if

tab_1.tabpage_1.enabled = false
tab_1.tabpage_3.enabled = true
tab_1.tabpage_3.postevent("ue_carica_righe_ordine")

end event

event open;call super::open;is_dati = message.powerobjectparm

end event

type tab_1 from tab within w_gen_bolla_uscita_lavorazione
integer x = 23
integer y = 20
integer width = 3566
integer height = 1856
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
tabpage_1 tabpage_1
tabpage_3 tabpage_3
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_3)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 3529
integer height = 1728
long backcolor = 12632256
string text = "Bolle"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
cb_5 cb_5
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
end type

on tabpage_1.create
this.dw_lista_bolle_pronte=create dw_lista_bolle_pronte
this.cb_nuova_bolla=create cb_nuova_bolla
this.cb_5=create cb_5
this.cb_aggiungi_a_bolla=create cb_aggiungi_a_bolla
this.dw_ext_dati_bolla=create dw_ext_dati_bolla
this.Control[]={this.dw_lista_bolle_pronte,&
this.cb_nuova_bolla,&
this.cb_5,&
this.cb_aggiungi_a_bolla,&
this.dw_ext_dati_bolla}
end on

on tabpage_1.destroy
destroy(this.dw_lista_bolle_pronte)
destroy(this.cb_nuova_bolla)
destroy(this.cb_5)
destroy(this.cb_aggiungi_a_bolla)
destroy(this.dw_ext_dati_bolla)
end on

type dw_lista_bolle_pronte from datawindow within tabpage_1
integer width = 3525
integer height = 480
integer taborder = 20
string title = "none"
string dataobject = "d_gen_bolla_uscita_lavorazione_lista_bolle_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_nuova_bolla from commandbutton within tabpage_1
integer x = 5
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo DDT"
end type

event clicked;string ls_errore, ls_cod_fornitore, ls_cod_tipo_bol_ven
int li_response
long ll_anno_bolla, ll_num_bolla, ll_i, ll_ret
uo_generazione_documenti luo_generazione_documenti

if isnull(dw_ext_dati_bolla.getitemstring(1,"cod_deposito")) then
	g_mb.messagebox("APICE","E' obbligatorio indicare un deposito di destinazione.", Stopsign!)
end if
	
if isnull(dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla")) then
	g_mb.messagebox("APICE","E' obbligatorio indicare il TIPO BOLLA di uscita.", Stopsign!)
end if



if g_mb.messagebox("APICE","Sei sicuro di voler creare una nuova bolla per tutte le righe assegnate di questo ordine?", Question!, YEsNo!, 2) = 1 then
	
	// stefanop 09/06/2010: controllo tipo bolla/fattura e se diverso dal default chiedo conferma per cambiare il movimento magazzino	
	ls_cod_tipo_bol_ven =  tab_1.tabpage_1.dw_ext_dati_bolla.getitemstring(1, "cod_tipo_bolla")
	
	dw_ext_dati_bolla.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_fornitore")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_fornitore")
	//stefanop 08/06/2010: progetto 140
	if is_tipo_default <> dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla") then
		luo_generazione_documenti.is_cod_tipo_bol_ven = dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla")
		luo_generazione_documenti.ib_change_det_ven = ib_change_mov
		luo_generazione_documenti.ib_change_mov = ib_change_mov
	end if
	// ----
	
	luo_generazione_documenti.is_cod_deposito = dw_ext_dati_bolla.getitemstring(1,"cod_deposito")
	
	ll_anno_bolla = 0
	ll_num_bolla = 0
		
	ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_lavorazione(il_anno_registrazione[1], il_num_registrazione[1], ref tab_1.tabpage_3.dw_righe_ordine, ref ll_anno_bolla, ref ll_num_bolla, ref ls_errore)
	//*************** fine modifica
	
	// verifico se l'uscita è dovuta ad un errore e al limite faccio rollback di tutto.
	if ll_ret <> 0 then
		rollback;
		g_mb.messagebox("APICE","Errore durante la generazione del DDT di lavorazione~r~n. " + ls_errore)
	else
		commit;
		g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
		
	end if
	destroy luo_generazione_documenti
	tab_1.postevent("clicked")
	
	select cod_fornitore
	into   :ls_cod_fornitore
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_bolla and
			 num_registrazione = :ll_num_bolla;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante la ricerca della bolla" + string(ll_anno_bolla) + "-" + string(ll_num_bolla) + "~r~n. " + ls_errore)
		return
	end if
	
	tab_1.tabpage_1.dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)

end if

end event

type cb_5 from commandbutton within tabpage_1
integer x = 3141
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa Bol."
end type

event clicked;long     ll_anno_reg_mov, ll_num_reg_mov, ll_progr_stock, ll_prog_registrazione

string   ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_nome_tabella, ls_cod_valuta, ls_test, &
         ls_messaggio, ls_window = "w_report_bol_ven_euro"

datetime ldt_data_stock

window   lw_window

uo_calcola_documento_euro luo_calcola_documento_euro


if dw_lista_bolle_pronte.rowcount() < 1 or dw_lista_bolle_pronte.getrow() < 1 then
	g_mb.messagebox("APICE","Nessuna bolla selezionata!")
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_num_registrazione")

declare cu_det_bol_ven cursor for
 select anno_reg_des_mov,
	     num_reg_des_mov
   from det_bol_ven
  where cod_azienda = :s_cs_xx.cod_azienda
    and anno_registrazione = :s_cs_xx.parametri.parametro_d_1
	 and num_registrazione = :s_cs_xx.parametri.parametro_d_2;

open cu_det_bol_ven;

do while 1 = 1 
	fetch cu_det_bol_ven into :ll_anno_reg_mov, :ll_num_reg_mov;
   
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Anno registrazione " + string(s_cs_xx.parametri.parametro_d_1) + "  num_registrazione " + string(s_cs_xx.parametri.parametro_d_2) + " non esistono in DET_BOL_VEV")
		exit
	end if	

	if sqlca.sqlcode = 0 then
		declare cu_des_mov_magazzino cursor for
		 select cod_deposito,
				  cod_ubicazione,
				  cod_lotto,
				  data_stock,
				  prog_stock,
				  prog_registrazione
			from dest_mov_magazzino	  
		  where cod_azienda = :s_cs_xx.cod_azienda
			 and anno_registrazione = :ll_anno_reg_mov
			 and num_registrazione = :ll_num_reg_mov;
			 
		open cu_des_mov_magazzino;
		
		do while 1 = 1 
			fetch cu_des_mov_magazzino into :ls_cod_deposito, :ls_cod_ubicazione, :ls_cod_lotto, :ldt_data_stock, :ll_progr_stock, :ll_prog_registrazione;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Errore", "Non è stato trovato nessun movimento di destinazione magazzino in corrispondenza dell'anno registrazione " + string(ll_anno_reg_mov) + " e del numero registrazione " + string(ll_num_reg_mov))
				exit
			end if
			if sqlca.sqlcode = 0 then
				if isnull(ls_cod_deposito) or isnull(ls_cod_ubicazione) or isnull(ls_cod_lotto) or &
					isnull(ldt_data_stock) or isnull(ll_progr_stock) then
					g_mb.messagebox("Attenzione", "Attenzione nella tabella DES_MOV_MAGAZZINO i dati dello stock corrispondenti all'anno registrazione " + string(ll_anno_reg_mov) + ", numero registrazione " + string(ll_num_reg_mov) + " e prog. registrazione " + string(ll_prog_registrazione) + " sono incompleti")
				end if
			end if	
		loop
		
		close cu_des_mov_magazzino;
	end if
loop
close cu_det_bol_ven;

s_cs_xx.parametri.parametro_i_1 = 0

if isnull(dw_lista_bolle_pronte.getitemstring(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_cod_documento")) then
	window_open(w_tipo_stampa_bol_ven, 0)
	if s_cs_xx.parametri.parametro_i_1 = -1 then
		rollback;
		return -1
	end if
end if

s_cs_xx.parametri.parametro_d_1 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_num_registrazione")

luo_calcola_documento_euro = create uo_calcola_documento_euro
	
if luo_calcola_documento_euro.uof_calcola_documento(s_cs_xx.parametri.parametro_d_1,s_cs_xx.parametri.parametro_d_2,"bol_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_calcola_documento_euro
	rollback;
	return -1
else
	commit;
end if	
	
destroy luo_calcola_documento_euro


select stringa
into   :ls_test
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'BVE';
				 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 then		
	window_open(w_report_bol_ven, -1)
elseif sqlca.sqlcode = 0 then			
	window_type_open(lw_window,ls_window, -1)
end if

close(w_richiesta_dati_bolla)
end event

type cb_aggiungi_a_bolla from commandbutton within tabpage_1
boolean visible = false
integer x = 3159
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;string ls_errore, ls_des_ord, ls_des_bol, ls_rag_soc_ord, ls_ind_ord, ls_cap_ord, ls_loc_ord, ls_prov_ord,&
		ls_rag_soc_bol, ls_ind_bol, ls_cap_bol, ls_loc_bol, ls_prov_bol,ls_cod_tipo_bol_ven
int li_response	 
long ll_anno_bolla, ll_num_bolla, ll_i, ll_ret
uo_generazione_documenti luo_generazione_documenti

if g_mb.messagebox("APICE","Sei sicuro di voler evadere tutte le righe asssegnate per questo ordine aggiungendole ad una bolla già esistente?", Question!, YEsNo!, 2) = 1 then
	
	// stefanop 09/06/2010: controllo tipo bolla/fattura e se diverso dal default chiedo conferma per cambiare il movimento magazzino	
	ls_cod_tipo_bol_ven =  tab_1.tabpage_1.dw_ext_dati_bolla.getitemstring(1, "cod_tipo_bolla")
	if is_tipo_default <> ls_cod_tipo_bol_ven then
		li_response = wf_controlla_movimenti(ls_cod_tipo_bol_ven, ref ls_errore)
		choose case li_response
			case -1 // Errore
				g_mb.error("APICE", ls_errore)
				return -1
			case 1 // SI
				ib_change_mov = true
				
			case 2 // NO
				ib_change_mov = false
		end choose
	end if
	// ----
		
	dw_ext_dati_bolla.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_cliente")
	//stefanop 08/06/2010: progetto 140
	if is_tipo_default <> dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla") then
		luo_generazione_documenti.is_cod_tipo_bol_ven = dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla")
		luo_generazione_documenti.ib_change_det_ven = ib_change_mov
		luo_generazione_documenti.ib_change_mov = ib_change_mov
	end if
	// ----
	
	if dw_lista_bolle_pronte.getrow() > 0 then
		ll_anno_bolla = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_anno_registrazione")
		ll_num_bolla  = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_num_registrazione")
	else
		g_mb.messagebox("APICE","Selezionare una bolla fra la lista di quelle pronte")
		destroy luo_generazione_documenti
		return
	end if
	
	select rag_soc_1,
			 indirizzo,
			 cap,
			 localita,
			 provincia
	into   :ls_rag_soc_ord,
			 :ls_ind_ord,
			 :ls_cap_ord,
			 :ls_loc_ord,
			 :ls_prov_ord
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione[1] and
			 num_registrazione = :il_num_registrazione[1];
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella verifica delle destinazioni~nErrore nella select di tes_ord_ven: " + sqlca.sqlerrtext)
		destroy luo_generazione_documenti
		return
	end if
	
	select rag_soc_1,
			 indirizzo,
			 cap,
			 localita,
			 provincia
	into   :ls_rag_soc_bol,
			 :ls_ind_bol,
			 :ls_cap_bol,
			 :ls_loc_bol,
			 :ls_prov_bol
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_bolla and
			 num_registrazione = :ll_num_bolla;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella verifica delle destinazioni~nErrore nella select di tes_bol_ven: " + sqlca.sqlerrtext)
		destroy luo_generazione_documenti
		return
	end if
	
//********************* Modifica Michele per sistemazione PTENDA 24/01/2003 *****************************
	
//	if not( &
//	(isnull(ls_rag_soc_ord) or ls_rag_soc_ord = "") and &
//	(isnull(ls_ind_ord) or ls_ind_ord = "") and &
//	(isnull(ls_cap_ord) or ls_cap_ord = "") and &
//	(isnull(ls_loc_ord) or ls_loc_ord = "") and &
//	(isnull(ls_prov_ord) or ls_prov_ord = "") &			
//	) then
	
		if (ls_rag_soc_ord <> ls_rag_soc_bol) or &  
			(isnull(ls_rag_soc_bol) and not isnull(ls_rag_soc_ord)) or &
			(not isnull(ls_rag_soc_bol) and isnull(ls_rag_soc_ord)) or &
			(ls_ind_ord <> ls_ind_bol) or &
			(isnull(ls_ind_bol) and not isnull(ls_ind_ord)) or &
			(not isnull(ls_ind_bol) and isnull(ls_ind_ord)) or &
			(ls_cap_ord <> ls_cap_bol) or &
			(isnull(ls_cap_bol) and not isnull(ls_cap_ord)) or &
			(not isnull(ls_cap_bol) and isnull(ls_cap_ord)) or &
			(ls_loc_ord <> ls_loc_bol) or &
			(isnull(ls_loc_bol) and not isnull(ls_loc_ord)) or &
			(not isnull(ls_loc_bol) and isnull(ls_loc_ord)) or &
			(ls_prov_ord <> ls_prov_bol) or &
			(isnull(ls_prov_bol) and not isnull(ls_prov_ord)) or &
			(not isnull(ls_prov_bol) and isnull(ls_prov_ord)) then
			
			if g_mb.messagebox("APICE","ATTENZIONE: la destinazione della bolla selezionata non corrisponde alla " + &
							  "destinazione dell'ordine~nSI DESIDERA CONTINUARE UGUALMENTE?",question!,yesno!,2) = 2	then
				destroy luo_generazione_documenti
				return
			end if
			
		end if
		
//	end if

//********************************* Fine Modifica Michele *********************************************


	// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
	if s_cs_xx.parametri.parametro_s_5 = "PACKLIST" then luo_generazione_documenti.is_rif_packing_list = "Rif.Packing List " + s_cs_xx.parametri.parametro_s_6

	//*** MICHELA 14/12/2005: SPECIFICA EUGANEA PANNELLI REPORT DDT FATTURE
	//                        solo se esiste il parametro flag aziendale 'EUG' a si allora procedo con l'ordinamento
	//                        delle righe in base alla specifica
		
	string ls_flag, ls_vuoto[]
		
	select flag
	into   :ls_flag
	from   parametri_azienda 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'EUG';
				 
	if sqlca.sqlcode = 0  and not isnull(ls_flag) and ls_flag = 'S' and UpperBound(s_cs_xx.parametri.parametro_s_1_a) > 0 then
	
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_pack(s_cs_xx.parametri.parametro_s_1_a, il_anno_registrazione, il_num_registrazione, ref ll_anno_bolla, ref ll_num_bolla, ref ls_errore)	
		s_cs_xx.parametri.parametro_s_1_a = ls_vuoto
	else
		
		// *** stefanop 21/07/2008: Modifiche per MRBoxer, per aggiornamento flag del portale
		int			li_mr_ret
		boolean 	lb_mrboxer = false
		
		select flag
		into    :ls_flag
		from   parametri_azienda
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_parametro = 'MRB';
				 
		if sqlca.sqlcode = 0 then
			if ls_flag = "S" and not isnull(ls_flag) then
				lb_mrboxer = true
			end if
		end if
		
		// Solo per MRBoxer mi connetto al database remoto
		if lb_mrboxer then
			
			uo_mrboxer luo_mrboxer
			luo_mrboxer = create uo_mrboxer
			
			li_mr_ret = luo_mrboxer.uof_connect_db()
			if li_mr_ret < 0 then
				messagebox("Apice" , "Impossibile contattare il server remoto")
				rollback;
			end if
			
		end if

		for ll_i = 1 to upperbound(il_anno_registrazione)
			ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla(il_anno_registrazione[ll_i], il_num_registrazione[ll_i], ll_anno_bolla, ll_num_bolla, ls_errore)
			if ll_ret <> 0 then exit
			
			// MRBoxer
			if lb_mrboxer then
				li_mr_ret = luo_mrboxer.uof_update_stato_ordine_portale(il_anno_registrazione[ll_i], il_num_registrazione[ll_i])
				
				if li_mr_ret < 0 then
					rollback;
				end if
			end if
			
		next
		
		// MRBoxer
		if lb_mrboxer then
			luo_mrboxer.uof_disconnect_db()
		end if
		// -----------------------------------------
		
	end if
	// fine modifica 
	
	// verifico se l'uscita è per errore e al limite eseguo rollback di tutto
	if ll_ret <> 0 then
		g_mb.messagebox("APICE","Errore durante la generazione della bolla. " + ls_errore)
		destroy luo_generazione_documenti		
		rollback;
		setnull(s_cs_xx.parametri.parametro_d_1)
		setnull(s_cs_xx.parametri.parametro_d_2)		
		return
	else
		g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
		destroy luo_generazione_documenti
		commit;
		// *** michela: se i flag di stampa rif interscambio e rif vostro cliente sono a si creo due righe
		s_cs_xx.parametri.parametro_d_1 = ll_anno_bolla
		s_cs_xx.parametri.parametro_d_2 = ll_num_bolla
		// ***		
		return
	end if
	
end if

end event

type dw_ext_dati_bolla from uo_std_dw within tabpage_1
integer x = 5
integer y = 488
integer width = 3520
integer height = 1080
integer taborder = 30
string title = "none"
string dataobject = "d_gen_bolla_uscita_lavorazione_ext_dati_bolla"
boolean border = false
end type

event itemchanged;string ls_null, ls_cod_causale

if isvalid(dwo) then
	
	choose case dwo.name
		case "cod_tipo_bolla"
			
			if isnull(data) or len(data) < 1 then

				setnull(ls_null)
				setitem(getrow(), "cod_causale", ls_null)
			
			else
				
				select cod_causale
				into   :ls_cod_causale
				from   tab_tipi_bol_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_bol_ven = :data;
						 
				setitem(getrow(), "cod_causale", ls_cod_causale)
				
			end if	
	end choose
	
end if
end event

type tabpage_3 from userobject within tab_1
event ue_carica_righe_ordine ( )
integer x = 18
integer y = 112
integer width = 3529
integer height = 1728
long backcolor = 12632256
string text = "Righe Ordine"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_1 st_1
cb_conferma cb_conferma
cb_deseleziona cb_deseleziona
cb_seleziona cb_seleziona
dw_righe_ordine dw_righe_ordine
end type

event ue_carica_righe_ordine;wf_carica_righe_ordine()
end event

on tabpage_3.create
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.cb_deseleziona=create cb_deseleziona
this.cb_seleziona=create cb_seleziona
this.dw_righe_ordine=create dw_righe_ordine
this.Control[]={this.st_1,&
this.cb_conferma,&
this.cb_deseleziona,&
this.cb_seleziona,&
this.dw_righe_ordine}
end on

on tabpage_3.destroy
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.cb_deseleziona)
destroy(this.cb_seleziona)
destroy(this.dw_righe_ordine)
end on

type st_1 from statictext within tabpage_3
integer x = 919
integer y = 1608
integer width = 1678
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "SHIFT-F1 = SELEZIONA TUTTO     SHIFT-F2 = DESELEZIONA TUTTO"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within tabpage_3
integer x = 5
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spedisci"
end type

event clicked;long ll_riga, ll_ret
string ls_cod_fornitore, ls_cod_des_fornitore, ls_cod_causale, ls_cod_porto, ls_cod_mezzo, ls_cod_resa, ls_cod_vettore, ls_cod_inoltro, ls_nota_piede, &
       ls_aspetto_beni, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita,ls_provincia, ls_null, ls_num_ord_fornitore, &
		 ls_nota_testata, ls_nota_video, ls_cod_tipo_ord_acq, ls_cod_tipo_bol_ven, ls_cod_imballo
dec{4} ld_num_colli, ld_peso_lordo, ld_peso_netto
datetime ldt_oggi, ldt_data_ord_fornitore


dw_righe_ordine.accepttext()
ll_ret = g_mb.messagebox("APICE","Procedo con la preparazione della BOLLA di USCITA ?", Question!, YesNo!, 1)
if ll_ret = 2 then return

// ----------------------------- attivo la maschera di input dati bolla -----------------------------


ldt_oggi = datetime(today(), 00:00:00)
setnull(ls_null)

select cod_fornitore,     cod_des_fornitore,      cod_porto,     cod_mezzo,     cod_resa,     cod_vettore,     cod_inoltro,     nota_piede,     rag_soc_1,     rag_soc_2,     indirizzo,     frazione,     cap,     localita,    provincia,     num_ord_fornitore,     data_ord_fornitore,      cod_tipo_ord_acq,     cod_imballo
into   :ls_cod_fornitore, :ls_cod_des_fornitore, :ls_cod_porto, :ls_cod_mezzo, :ls_cod_resa, :ls_cod_vettore, :ls_cod_inoltro,  :ls_nota_piede, :ls_rag_soc_1, :ls_rag_soc_2, :ls_indirizzo, :ls_frazione, :ls_cap, :ls_localita,:ls_provincia,  :ls_num_ord_fornitore, :ldt_data_ord_fornitore, :ls_cod_tipo_ord_acq, :ls_cod_imballo
from   tes_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione[1] and
		 num_registrazione = :il_num_registrazione[1];
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca ordine " + string(il_anno_registrazione[1]) + "/" + string(il_num_registrazione[1])+ ". Impossibile proseguire", stopsign!)
	return
end if

tab_1.tabpage_1.enabled = true
tab_1.tabpage_1.dw_lista_bolle_pronte.setredraw(false)
tab_1.tabpage_1.dw_ext_dati_bolla.setredraw(false)
tab_1.tabpage_1.dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)
tab_1.tabpage_1.dw_ext_dati_bolla.reset()
ll_riga = tab_1.tabpage_1.dw_ext_dati_bolla.insertrow(0)
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_destinazione", &
					  sqlca, &
					  "anag_des_fornitori", &
					  "cod_des_fornitore", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_imballo", &
					  sqlca, &
					  "tab_imballi", &
					  "cod_imballo", &
					  "des_imballo", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_vettore", &
					  sqlca, &
					  "anag_vettori", &
					  "cod_vettore", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_inoltro", &
					  sqlca, &
					  "anag_vettori", &
					  "cod_vettore", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_mezzo", &
					  sqlca, &
					  "tab_mezzi", &
					  "cod_mezzo", &
					  "des_mezzo", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_porto", &
					  sqlca, &
					  "tab_porti", &
					  "cod_porto", &
					  "des_porto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_resa", &
					  sqlca, &
					  "tab_rese", &
					  "cod_resa", &
					  "des_resa", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_causale", &
					  sqlca, &
					  "tab_causali_trasp", &
					  "cod_causale", &
					  "des_causale", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_tipo_bolla", &
					  sqlca, &
					  "tab_tipi_bol_ven", &
					  "cod_tipo_bol_ven", &
					  "des_tipo_bol_ven", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_bol_ven = 'C' ")	

f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
					  "cod_deposito", &
					  sqlca, &
					  "anag_depositi", &
					  "cod_deposito", &
					  "des_deposito", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
// ----

tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_fornitore", ls_cod_fornitore)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_destinazione", ls_cod_des_fornitore)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_causale", ls_cod_causale)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_porto", ls_cod_porto)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_resa", ls_cod_resa)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_imballo", ls_cod_imballo)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "aspetto_beni", ls_aspetto_beni)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_2", ls_rag_soc_2)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "indirizzo", ls_indirizzo)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "frazione", ls_frazione)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "localita", ls_localita)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cap", ls_cap)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "provincia", ls_provincia)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "num_colli", ld_num_colli)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "peso_netto", ld_peso_netto)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "peso_lordo", ld_peso_lordo)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "num_ord_fornitore", ls_num_ord_fornitore)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "data_ord_fornitore", ldt_data_ord_fornitore)
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "note_piede", ls_nota_piede)

// stefanop 08/06/2010: cerco il tipo bolla vendita di default: progetto 140
select cod_tipo_bol_ven
into :ls_cod_tipo_bol_ven
from tab_tipi_ord_ven
where 
	cod_azienda=:s_cs_xx.cod_azienda and
	cod_tipo_ord_ven in (
		select cod_tipo_ord_ven 
		from tes_ord_ven 
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione[1] and
			num_registrazione = :il_num_registrazione[1]);

if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_bol_ven) then ls_cod_tipo_bol_ven = ""

is_tipo_default = ls_cod_tipo_bol_ven
tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_tipo_bolla", ls_cod_tipo_bol_ven)
setnull(ls_cod_tipo_bol_ven)
// ----

tab_1.tabpage_1.enabled = true
tab_1.tabpage_1.dw_lista_bolle_pronte.setredraw(true)
tab_1.tabpage_1.dw_ext_dati_bolla.setredraw(true)

if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null, "BOL_VEN", ls_null, ldt_oggi, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
else
	if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
end if

tab_1.SelectTab(1)
	  
end event

type cb_deseleziona from commandbutton within tabpage_3
integer x = 2770
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Deseleziona"
end type

event clicked;long ll_i

for ll_i = 1 to dw_righe_ordine.rowcount()
	dw_righe_ordine.setitem(ll_i, "flag_evasione_totale", "N")
	dw_righe_ordine.setitem(ll_i, "quan_da_evadere", 0)
next
end event

type cb_seleziona from commandbutton within tabpage_3
integer x = 3159
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sel.Tutto"
end type

event clicked;long ll_i

for ll_i = 1 to dw_righe_ordine.rowcount()
	dw_righe_ordine.setitem(ll_i, "flag_evasione_totale", "S")
	dw_righe_ordine.setitem(ll_i, "quan_da_evadere", dw_righe_ordine.getitemnumber(ll_i, "quan_residua"))
next
dw_righe_ordine.accepttext()
end event

type dw_righe_ordine from datawindow within tabpage_3
event ue_key pbm_dwnkey
integer y = 8
integer width = 3520
integer height = 1580
integer taborder = 10
string dataobject = "d_gen_bolla_uscita_lavorazione_righe_da_evadere"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;if key = keyf1! and keyflags = 1 then
	cb_seleziona.triggerevent("Clicked")
end if
if key = keyF2! and keyflags = 1 then
	cb_deseleziona.triggerevent("Clicked")
end if

			
end event

event itemchanged;if isnull(dwo.name) then return
choose case dwo.name
	case "flag_evasione_totale"
		if data = "S" then
			setitem(row, "quan_da_evadere", getitemnumber(row,"quan_residua"))
		else
			setitem(row, "quan_da_evadere", 0)
		end if
	case "quan_da_evadere"
		if dec(data) < getitemnumber(row,"quan_residua") then
			setitem(row,"flag_evasione_totale","N")
		else
			setitem(row,"flag_evasione_totale","S")
		end if
end choose
end event

