﻿$PBExportHeader$w_gen_bolla_uscita_lavorazione_richiesta.srw
forward
global type w_gen_bolla_uscita_lavorazione_richiesta from w_std_principale
end type
type dw_richiesta from datawindow within w_gen_bolla_uscita_lavorazione_richiesta
end type
type cb_1 from commandbutton within w_gen_bolla_uscita_lavorazione_richiesta
end type
end forward

global type w_gen_bolla_uscita_lavorazione_richiesta from w_std_principale
integer width = 3913
integer height = 1632
string title = "Associazione Grezzi"
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_richiesta dw_richiesta
cb_1 cb_1
end type
global w_gen_bolla_uscita_lavorazione_richiesta w_gen_bolla_uscita_lavorazione_richiesta

type variables
s_gen_bolla_uscita_lavorazione_richiesta istr_grezzo
end variables

on w_gen_bolla_uscita_lavorazione_richiesta.create
int iCurrent
call super::create
this.dw_richiesta=create dw_richiesta
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_richiesta
this.Control[iCurrent+2]=this.cb_1
end on

on w_gen_bolla_uscita_lavorazione_richiesta.destroy
call super::destroy
destroy(this.dw_richiesta)
destroy(this.cb_1)
end on

event open;call super::open;string ls_cod_prodotto_precedente="<>", ls_des_prodotto
long ll_i, ll_rows, ll_riga

istr_grezzo = message.powerobjectparm

ll_rows = upperbound(istr_grezzo.cod_prodotto)

for ll_i = 1 to ll_rows
	
	ll_riga = dw_richiesta.insertrow(0)
	
	if istr_grezzo.cod_prodotto[ll_i] <> ls_cod_prodotto_precedente AND isnull( istr_grezzo.cod_grezzo[ll_i] )   then
	
		dw_richiesta.setitem(ll_riga, "cod_prodotto", istr_grezzo.cod_prodotto[ll_i] )
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :istr_grezzo.cod_prodotto[ll_i];
		
		dw_richiesta.setitem(ll_riga, "des_prodotto", ls_des_prodotto )

	end if
	
	ls_cod_prodotto_precedente = istr_grezzo.cod_prodotto[ll_i]
	
next
end event

event closequery;call super::closequery;message.powerobjectparm = istr_grezzo
end event

type dw_richiesta from datawindow within w_gen_bolla_uscita_lavorazione_richiesta
integer x = 14
integer y = 8
integer width = 3831
integer height = 1380
integer taborder = 10
string title = "none"
string dataobject = "d_gen_bolla_uscita_lavorazione_richiesta"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;if isvalid(dwo) then
	
	choose case dwo.name
		case "b_ricerca_prodotto"
		
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			setnull(s_cs_xx.parametri.parametro_uo_dw_search)
			guo_ricerca.uof_ricerca_prodotto(dw_richiesta,"cod_grezzo")	

			if not isnull(s_cs_xx.parametri.parametro_s_10) then
				setitem(row,"cod_grezzo", s_cs_xx.parametri.parametro_s_10)
			end if
			
	end choose

end if
end event

type cb_1 from commandbutton within w_gen_bolla_uscita_lavorazione_richiesta
integer x = 3410
integer y = 1420
integer width = 453
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Prosegui >>>"
end type

event clicked;string ls_cod_prodotto_precedente="<>", ls_des_prodotto
long ll_i, ll_rows, ll_riga, ll_y

//lstr_grezzo = message.powerobjectparm

dw_richiesta.accepttext()
ll_rows = upperbound(istr_grezzo.cod_prodotto)

for ll_i = 1 to ll_rows
	
	if isnull( istr_grezzo.cod_grezzo[ll_i] ) then
	
		for ll_y = 1 to dw_richiesta.rowcount( )
	
			if dw_richiesta.getitemstring(ll_y, "cod_prodotto") = istr_grezzo.cod_prodotto[ll_i] then
				istr_grezzo.cod_grezzo[ll_i] = dw_richiesta.getitemstring(ll_y, "cod_grezzo")
			end if
			
		next

	end if
	
next

close(parent)
end event

