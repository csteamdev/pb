﻿$PBExportHeader$w_lettura_ddt_acq_file.srw
forward
global type w_lettura_ddt_acq_file from window
end type
type st_1 from statictext within w_lettura_ddt_acq_file
end type
type cb_annulla from commandbutton within w_lettura_ddt_acq_file
end type
type cb_conferma from commandbutton within w_lettura_ddt_acq_file
end type
type dw_lista from datawindow within w_lettura_ddt_acq_file
end type
end forward

global type w_lettura_ddt_acq_file from window
integer width = 4507
integer height = 1632
boolean titlebar = true
string title = "Riepilogo lettura file"
windowtype windowtype = response!
long backcolor = 12632256
boolean center = true
event ue_resize ( )
st_1 st_1
cb_annulla cb_annulla
cb_conferma cb_conferma
dw_lista dw_lista
end type
global w_lettura_ddt_acq_file w_lettura_ddt_acq_file

event ue_resize();dw_lista.width = this.width - 100
dw_lista.height = this.height - 200
end event

on w_lettura_ddt_acq_file.create
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.dw_lista=create dw_lista
this.Control[]={this.st_1,&
this.cb_annulla,&
this.cb_conferma,&
this.dw_lista}
end on

on w_lettura_ddt_acq_file.destroy
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.dw_lista)
end on

event open;s_cs_xx_parametri		ls_struttura
datastore				lds_data
integer					li_tipo_tracciato
long						ll_riga, ll_index

ls_struttura = message.powerobjectparm

lds_data = ls_struttura.parametro_ds_1
li_tipo_tracciato = ls_struttura.parametro_i_1

lds_data.RowsCopy(1, lds_data.RowCount(), Primary!, dw_lista, 1, Primary!)


//this.width = w_cs_xx_mdi.mdi_1.width - 200
//this.height = w_cs_xx_mdi.mdi_1.height - 100
//postevent("ue_resize")
end event

type st_1 from statictext within w_lettura_ddt_acq_file
integer x = 942
integer y = 44
integer width = 3342
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Le righe in verde saranno evase, quelle in giallo rimaranno parziali"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_lettura_ddt_acq_file
integer x = 498
integer y = 28
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx_parametri		ls_struttura


ls_struttura.parametro_s_1 = ""

closewithreturn(parent, ls_struttura)
end event

type cb_conferma from commandbutton within w_lettura_ddt_acq_file
integer x = 55
integer y = 28
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;s_cs_xx_parametri		ls_struttura
datastore				lds_data
long						ll_tot


lds_data = create datastore
lds_data.dataobject = "d_lettura_ddt_acq_file"
lds_data.settransobject(sqlca)

//inoltre ricopio la dw nel datastore e lo faccio ritornare
dw_lista.RowsCopy(1, dw_lista.RowCount(), Primary!, lds_data, 1, Primary!)

ls_struttura.parametro_s_1 = "OK"
ls_struttura.parametro_ds_1 = lds_data

ll_tot = lds_data.rowcount()

closewithreturn(parent, ls_struttura)
end event

type dw_lista from datawindow within w_lettura_ddt_acq_file
integer x = 27
integer y = 148
integer width = 4453
integer height = 1372
integer taborder = 10
string title = "none"
string dataobject = "d_lettura_ddt_acq_file"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

