﻿$PBExportHeader$w_mod_carico_acq.srw
forward
global type w_mod_carico_acq from window
end type
type em_fat_conv_quantita from editmask within w_mod_carico_acq
end type
type st_3 from statictext within w_mod_carico_acq
end type
type st_2 from statictext within w_mod_carico_acq
end type
type rb_3 from radiobutton within w_mod_carico_acq
end type
type st_1 from statictext within w_mod_carico_acq
end type
type rb_2 from radiobutton within w_mod_carico_acq
end type
type rb_1 from radiobutton within w_mod_carico_acq
end type
type em_fat_conv from editmask within w_mod_carico_acq
end type
type em_prezzo from editmask within w_mod_carico_acq
end type
type cb_annulla from commandbutton within w_mod_carico_acq
end type
type cb_ok from commandbutton within w_mod_carico_acq
end type
end forward

global type w_mod_carico_acq from window
integer width = 1253
integer height = 1064
boolean titlebar = true
string title = "Modifica Riga Ordine"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
em_fat_conv_quantita em_fat_conv_quantita
st_3 st_3
st_2 st_2
rb_3 rb_3
st_1 st_1
rb_2 rb_2
rb_1 rb_1
em_fat_conv em_fat_conv
em_prezzo em_prezzo
cb_annulla cb_annulla
cb_ok cb_ok
end type
global w_mod_carico_acq w_mod_carico_acq

type variables
long il_row

datawindow i_dw
end variables

event open;i_dw = s_cs_xx.parametri.parametro_dw_1
il_row = s_cs_xx.parametri.parametro_i_1

em_prezzo.text = string(i_dw.getitemnumber(il_row,"prezzo_acquisto"))
em_fat_conv.text = string(i_dw.getitemnumber(il_row,"fat_conversione"))
end event

on w_mod_carico_acq.create
this.em_fat_conv_quantita=create em_fat_conv_quantita
this.st_3=create st_3
this.st_2=create st_2
this.rb_3=create rb_3
this.st_1=create st_1
this.rb_2=create rb_2
this.rb_1=create rb_1
this.em_fat_conv=create em_fat_conv
this.em_prezzo=create em_prezzo
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.Control[]={this.em_fat_conv_quantita,&
this.st_3,&
this.st_2,&
this.rb_3,&
this.st_1,&
this.rb_2,&
this.rb_1,&
this.em_fat_conv,&
this.em_prezzo,&
this.cb_annulla,&
this.cb_ok}
end on

on w_mod_carico_acq.destroy
destroy(this.em_fat_conv_quantita)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.rb_3)
destroy(this.st_1)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.em_fat_conv)
destroy(this.em_prezzo)
destroy(this.cb_annulla)
destroy(this.cb_ok)
end on

type em_fat_conv_quantita from editmask within w_mod_carico_acq
integer x = 809
integer y = 312
integer width = 402
integer height = 76
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###,###,##0.#####"
end type

type st_3 from statictext within w_mod_carico_acq
integer x = 14
integer y = 380
integer width = 768
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = " (Ricalcolo solo quantità)"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_mod_carico_acq
integer x = 14
integer y = 188
integer width = 768
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = " (Ricalcolo quantità e prezzo)"
alignment alignment = right!
boolean focusrectangle = false
end type

type rb_3 from radiobutton within w_mod_carico_acq
integer x = 101
integer y = 296
integer width = 663
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Fattore di Conversione"
end type

event clicked;em_prezzo.enabled = false
em_fat_conv.enabled = false
em_fat_conv_quantita.enabled = true

em_prezzo.text = string(i_dw.getitemnumber(il_row,"prezzo_acquisto"))
end event

type st_1 from statictext within w_mod_carico_acq
integer x = 46
integer y = 484
integer width = 1134
integer height = 360
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 255
long backcolor = 12632256
string text = "ATTENZIONE! Cambiando il fattore di conversione quantità e prezzo della riga corrente verranno aggiornati automaticamente. Il cambiamento del prezzo non implica invece alcun aggiornamento degli altri valori"
alignment alignment = center!
boolean focusrectangle = false
end type

type rb_2 from radiobutton within w_mod_carico_acq
integer x = 101
integer y = 120
integer width = 699
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Fattore di Conversione"
end type

event clicked;em_prezzo.enabled = false
em_fat_conv.enabled = true
em_fat_conv_quantita.enabled = false

em_prezzo.text = string(i_dw.getitemnumber(il_row,"prezzo_acquisto"))
end event

type rb_1 from radiobutton within w_mod_carico_acq
integer x = 101
integer y = 20
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Prezzo"
end type

event clicked;em_prezzo.enabled = true
em_fat_conv.enabled = false

em_fat_conv.text = string(i_dw.getitemnumber(il_row,"fat_conversione"))
end event

type em_fat_conv from editmask within w_mod_carico_acq
integer x = 809
integer y = 148
integer width = 402
integer height = 76
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###,###,##0.#####"
end type

type em_prezzo from editmask within w_mod_carico_acq
integer x = 809
integer y = 20
integer width = 402
integer height = 76
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###,###,##0.####"
end type

type cb_annulla from commandbutton within w_mod_carico_acq
integer x = 434
integer y = 884
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_mod_carico_acq
integer x = 823
integer y = 884
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;string  ls_nota, ls_cod_valuta, ls_messaggio

long    ll_anno, ll_num, ll_riga, ll_riga_new

dec{6}  ld_prezzo_mod, ld_conv_mod, ld_prezzo_ord, ld_conv_ord, ld_ordinata_ord, ld_evasa_ord, &
        ld_prezzo_conv,ld_conv_mod_quan

uo_condizioni_cliente luo_arrotonda


ll_anno = i_dw.getitemnumber(il_row,"anno_registrazione")
ll_num = i_dw.getitemnumber(il_row,"num_registrazione")
ll_riga = i_dw.getitemnumber(il_row,"prog_riga_ord_acq")
ld_prezzo_mod = dec(em_prezzo.text)
ld_conv_mod = dec(em_fat_conv.text)
ld_conv_mod_quan = dec(em_fat_conv_quantita.text)

select cod_valuta
into   :ls_cod_valuta
from   tes_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_num;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella modifica della riga ordine~nErrore nella select di tes_ord_acq: " + sqlca.sqlerrtext)
	return -1
end if

select prezzo_acquisto,
		 fat_conversione,
		 quan_ordinata,
		 quan_arrivata
into   :ld_prezzo_ord,
		 :ld_conv_ord,
		 :ld_ordinata_ord,
		 :ld_evasa_ord
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_num and
		 prog_riga_ordine_acq = :ll_riga;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella modifica della riga ordine~nErrore nella select di det_ord_acq: " + sqlca.sqlerrtext)
	return -1
end if

if ld_prezzo_ord = ld_prezzo_mod and ld_conv_ord = ld_conv_mod then
	close(parent)
	return 0
end if

if i_dw.getitemnumber(il_row,"prezzo_acquisto") <> ld_prezzo_mod then
	i_dw.setitem(il_row,"prezzo_acquisto",ld_prezzo_mod)
	i_dw.setitem(il_row,"prezzo_modificato","S")
	ld_prezzo_mod = ld_prezzo_mod * ld_conv_mod
	luo_arrotonda = create uo_condizioni_cliente
	luo_arrotonda.uof_arrotonda_prezzo_decimal(ld_prezzo_mod,ls_cod_valuta,ld_prezzo_mod,ls_messaggio)
	destroy luo_arrotonda
end if

if i_dw.getitemnumber(il_row,"fat_conversione") <> ld_conv_mod then
	i_dw.setitem(il_row,"fat_conversione",ld_conv_mod)
	i_dw.setitem(il_row,"quan_acquisto",round((ld_ordinata_ord - ld_evasa_ord)*ld_conv_mod,4))
	ld_prezzo_conv = ld_prezzo_ord / ld_conv_mod
	luo_arrotonda = create uo_condizioni_cliente
	luo_arrotonda.uof_arrotonda_prezzo_decimal(ld_prezzo_conv,ls_cod_valuta,ld_prezzo_conv,ls_messaggio)
	destroy luo_arrotonda
	i_dw.setitem(il_row,"prezzo_acquisto",ld_prezzo_conv)
	i_dw.setitem(il_row,"prezzo_modificato","S")
	ld_prezzo_mod = ld_prezzo_ord
end if

// aggiunto da Enrico per specifica MODIFICHE_VARIE rev.2 del 22/10/2004
if i_dw.getitemnumber(il_row,"fat_conversione") <> ld_conv_mod_quan then
	i_dw.setitem(il_row,"fat_conversione",ld_conv_mod_quan)
	i_dw.setitem(il_row,"quan_acquisto",round((ld_ordinata_ord - ld_evasa_ord)*ld_conv_mod_quan,4))
	ld_conv_mod = ld_conv_mod_quan
/*	ld_prezzo_conv = ld_prezzo_ord / ld_conv_mod
	luo_arrotonda = create uo_condizioni_cliente
	luo_arrotonda.uof_arrotonda_prezzo_decimal(ld_prezzo_conv,ls_cod_valuta,ld_prezzo_conv,ls_messaggio)
	destroy luo_arrotonda
	i_dw.setitem(il_row,"prezzo_acquisto",ld_prezzo_conv)
	i_dw.setitem(il_row,"prezzo_modificato","S")
	ld_prezzo_mod = ld_prezzo_ord */
end if


if ld_evasa_ord = 0 then

	update det_ord_acq
	set    prezzo_acquisto = :ld_prezzo_mod,
			 fat_conversione = :ld_conv_mod
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_num and
			 prog_riga_ordine_acq = :ll_riga;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella modifica della riga ordine~nErrore nella update di det_ord_acq: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

else

	select nota_dettaglio
	into   :ls_nota
	from   det_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_num and
			 prog_riga_ordine_acq = :ll_riga;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella modifica della riga ordine~nErrore nella select di det_ord_acq: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	if isnull(ls_nota) then
		ls_nota = "Rif. riga " + string(ll_riga)
	else
		ls_nota = ls_nota + " ~nRif. riga " + string(ll_riga)
	end if

	select max(prog_riga_ordine_acq) + 10
	into   :ll_riga_new
	from   det_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_num;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella modifica della riga ordine~nErrore nella select di det_ord_acq: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	insert 
	into   det_ord_acq  
			 (cod_azienda,   
			 anno_registrazione,   
			 num_registrazione,   
			 prog_riga_ordine_acq,   
			 cod_tipo_det_acq,   
			 cod_prodotto,   
			 cod_misura,   
			 des_prodotto,   
			 quan_ordinata,   
			 prezzo_acquisto,   
			 fat_conversione,   
			 sconto_1,   
			 sconto_2,   
			 sconto_3,   
			 cod_iva,   
			 data_consegna,   
			 cod_prod_fornitore,   
			 quan_arrivata,   
			 val_riga,   
			 flag_saldo,   
			 flag_blocco,   
			 nota_dettaglio,   
			 anno_commessa,   
			 num_commessa,   
			 cod_centro_costo,   
			 sconto_4,   
			 sconto_5,   
			 sconto_6,   
			 sconto_7,   
			 sconto_8,   
			 sconto_9,   
			 sconto_10,   
			 anno_off_acq,   
			 num_off_acq,   
			 prog_riga_off_acq,   
			 data_consegna_fornitore,   
			 num_confezioni,   
			 num_pezzi_confezione,   
			 flag_doc_suc_det,   
			 flag_st_note_det,   
			 imponibile_iva,   
			 imponibile_iva_valuta )  
	select cod_azienda,   
			 anno_registrazione,   
			 num_registrazione,   
			 :ll_riga_new,   
			 cod_tipo_det_acq,   
			 cod_prodotto,   
			 cod_misura,   
			 des_prodotto,   
			 quan_ordinata - quan_arrivata,   
			 :ld_prezzo_mod,   
			 :ld_conv_mod,   
			 sconto_1,   
			 sconto_2,   
			 sconto_3,   
			 cod_iva,   
			 data_consegna,   
			 cod_prod_fornitore,   
			 0,   
			 0,   
			 'N',   
			 'N',   
			 :ls_nota,   
			 anno_commessa,   
			 num_commessa,   
			 cod_centro_costo,   
			 sconto_4,   
			 sconto_5,   
			 sconto_6,   
			 sconto_7,   
			 sconto_8,   
			 sconto_9,   
			 sconto_10,   
			 anno_off_acq,   
			 num_off_acq,   
			 prog_riga_off_acq,   
			 data_consegna_fornitore,   
			 num_confezioni,   
			 num_pezzi_confezione,   
			 flag_doc_suc_det,   
			 flag_st_note_det,   
			 0,   
			 0
	from   det_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_num and
			 prog_riga_ordine_acq = :ll_riga;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella modifica della riga ordine~nErrore nella update di det_ord_acq: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	update det_ord_acq
	set    flag_saldo = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_num and
			 prog_riga_ordine_acq = :ll_riga;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella modifica della riga ordine~nErrore nella update di det_ord_acq: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	i_dw.setitem(il_row,"prog_riga_ord_acq",ll_riga_new)
	
end if

close(parent)

return 0
end event

