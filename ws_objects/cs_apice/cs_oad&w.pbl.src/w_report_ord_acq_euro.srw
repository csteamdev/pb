﻿$PBExportHeader$w_report_ord_acq_euro.srw
$PBExportComments$Report Ordine Acquisto
forward
global type w_report_ord_acq_euro from w_cs_xx_principale
end type
type dw_report_ord_acq from uo_cs_xx_dw within w_report_ord_acq_euro
end type
end forward

global type w_report_ord_acq_euro from w_cs_xx_principale
integer width = 3909
integer height = 2948
string title = "Stampa Ordini Acquisto"
dw_report_ord_acq dw_report_ord_acq
end type
global w_report_ord_acq_euro w_report_ord_acq_euro

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_stampa_residuo
long il_anno_registrazione, il_num_registrazione
end variables

forward prototypes
public subroutine wf_report ()
end prototypes

public subroutine wf_report ();string 	ls_stringa, ls_cod_tipo_ord_acq, ls_cod_banca_clien_for, ls_cod_fornitore, ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_fornitore, ls_cod_porto, ls_cod_resa, ls_nota_testata, ls_nota_piede, ls_rag_soc_1, &
		 	ls_rag_soc_2, ls_indirizzo,  ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, ls_cod_prodotto, &
		 	ls_des_prodotto, ls_rag_soc_1_for, ls_rag_soc_2_for, ls_indirizzo_for, ls_localita_for, ls_frazione_for, ls_cap_for, &
		 	ls_provincia_for, ls_partita_iva_for, ls_cod_fiscale_for, ls_cod_lingua, ls_flag_tipo_fornitore, ls_des_tipo_ord_acq, &
		 	ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , &
		 	ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_acq, ls_des_prodotto_lingua, ls_cod_prod_fornitore, &
		 	ls_cod_fil_fornitore, ls_rag_soc_1_fil, ls_rag_soc_2_fil, ls_indirizzo_fil, ls_localita_fil, ls_frazione_fil, ls_cap_fil, &
		 	ls_provincia_fil, ls_cod_operatore, ls_des_operatore, ls_des_prodotto_fornitore, ls_flag_saldo, ls_rif_interno, &
		 	ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_formato, ls_cod_banca, ls_cod_abi, ls_cod_cab,ls_flag_tipo_pagamento, &
			ls_telefono, ls_fax, ls_telefono_fil, ls_fax_fil, ls_iban, ls_flag_stampa_disegno, ls_cod_disegno, ls_rev_disegno
		 
long     ll_errore

dec{4}   ld_tot_val_ordine, ld_sconto, ld_quan_ordinata, ld_prezzo_acquisto, ld_sconto_1, ld_sconto_2, ld_val_riga, ld_sconto_pagamento, &
		   ld_quan_arrivata, ld_imponibile_iva
			
dec{5}   ld_fat_conversione
		 
datetime ldt_data_ord_fornitore, ldt_data_registrazione, ldt_data_consegna


dw_report_ord_acq.setredraw(false)
dw_report_ord_acq.reset()

//il_anno_registrazione = dw_report_ord_acq.i_parentdw.getitemnumber(dw_report_ord_acq.i_parentdw.i_selectedrows[1], "anno_registrazione")
//il_num_registrazione = dw_report_ord_acq.i_parentdw.getitemnumber(dw_report_ord_acq.i_parentdw.i_selectedrows[1], "num_registrazione")

select stringa  
into   :ls_stringa  
from   parametri_azienda  
where  cod_azienda = :s_cs_xx.cod_azienda and  
       flag_parametro = 'S' and  
       cod_parametro = 'CVL';
if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select tes_ord_acq.cod_tipo_ord_acq,   
		 tes_ord_acq.data_registrazione,   
		 tes_ord_acq.cod_fornitore,   
		 tes_ord_acq.cod_fil_fornitore,
		 tes_ord_acq.cod_valuta,   
		 tes_ord_acq.cod_pagamento,   
		 tes_ord_acq.sconto,   
		 tes_ord_acq.cod_banca_clien_for,   
		 tes_ord_acq.cod_banca,   
		 tes_ord_acq.num_ord_fornitore,   
		 tes_ord_acq.data_ord_fornitore,   
		 tes_ord_acq.cod_porto,   
		 tes_ord_acq.cod_resa,   
		 tes_ord_acq.cod_mezzo,   
		 tes_ord_acq.nota_testata,   
		 tes_ord_acq.nota_piede,   
		 tes_ord_acq.tot_val_ordine,
		 tes_ord_acq.rag_soc_1,
		 tes_ord_acq.rag_soc_2,
		 tes_ord_acq.indirizzo,
		 tes_ord_acq.localita,
		 tes_ord_acq.frazione,
		 tes_ord_acq.cap,
		 tes_ord_acq.provincia,
		 tes_ord_acq.cod_operatore,
		 tes_ord_acq.flag_st_note_tes,
		 tes_ord_acq.flag_st_note_pie,
		 tes_ord_acq.imponibile_iva,
		 tes_ord_acq.flag_stampa_disegno
into   :ls_cod_tipo_ord_acq,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_fornitore,   
		 :ls_cod_fil_fornitore,
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_cod_banca,   
		 :ls_num_ord_fornitore,   
		 :ldt_data_ord_fornitore,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_cod_mezzo,
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,
		 :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_localita,
		 :ls_frazione,
		 :ls_cap,
		 :ls_provincia, 
		 :ls_cod_operatore,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ld_imponibile_iva,
		 :ls_flag_stampa_disegno
from   tes_ord_acq  
where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_acq.anno_registrazione = :il_anno_registrazione and 
		 tes_ord_acq.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_acq)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_fornitore)
	setnull(ls_cod_fil_fornitore)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_fornitore)
	setnull(ldt_data_ord_fornitore)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	ls_flag_stampa_disegno = "N"
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return
end if	

//modifiche Michela: aggiungo il telefono e il fax

select anag_fornitori.rag_soc_1,   
       anag_fornitori.rag_soc_2,   
       anag_fornitori.indirizzo,   
       anag_fornitori.localita,   
       anag_fornitori.frazione,   
       anag_fornitori.cap,   
       anag_fornitori.provincia,   
       anag_fornitori.partita_iva,   
       anag_fornitori.cod_fiscale,   
       anag_fornitori.cod_lingua,   
       anag_fornitori.flag_tipo_fornitore,
		 anag_fornitori.rif_interno,
		 anag_fornitori.telefono,
		 anag_fornitori.fax
into   :ls_rag_soc_1_for,   
       :ls_rag_soc_2_for,   
       :ls_indirizzo_for,   
       :ls_localita_for,   
       :ls_frazione_for,   
       :ls_cap_for,   
       :ls_provincia_for,   
       :ls_partita_iva_for,   
       :ls_cod_fiscale_for,   
       :ls_cod_lingua,   
       :ls_flag_tipo_fornitore,
		 :ls_rif_interno,
		 :ls_telefono,
		 :ls_fax
from   anag_fornitori  
where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fornitori.cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_for)
	setnull(ls_rag_soc_2_for)
	setnull(ls_indirizzo_for)
	setnull(ls_localita_for)
	setnull(ls_frazione_for)
	setnull(ls_cap_for)
	setnull(ls_provincia_for)
	setnull(ls_partita_iva_for)
	setnull(ls_cod_fiscale_for)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_fornitore)
	setnull(ls_telefono)
	setnull(ls_fax)
end if

if ls_flag_tipo_fornitore = 'E' then
	ls_partita_iva_for = ls_cod_fiscale_for
end if

select anag_fil_fornitori.rag_soc_1,   
       anag_fil_fornitori.rag_soc_2,   
       anag_fil_fornitori.indirizzo,   
       anag_fil_fornitori.localita,   
       anag_fil_fornitori.frazione,   
       anag_fil_fornitori.cap,   
       anag_fil_fornitori.provincia,
		 anag_fil_fornitori.telefono,
		 anag_fil_fornitori.fax
into   :ls_rag_soc_1_fil,   
       :ls_rag_soc_2_fil,   
       :ls_indirizzo_fil,   
       :ls_localita_fil,   
       :ls_frazione_fil,   
       :ls_cap_fil,   
       :ls_provincia_fil,
		 :ls_telefono_fil,
		 :ls_fax_fil
from   anag_fil_fornitori
where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and
		 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_fil)
	setnull(ls_rag_soc_2_fil)
	setnull(ls_indirizzo_fil)
	setnull(ls_localita_fil)
	setnull(ls_frazione_fil)
	setnull(ls_cap_fil)
	setnull(ls_provincia_fil)
	setnull(ls_telefono_fil)
	setnull(ls_fax_fil)
end if

select tab_tipi_ord_acq.des_tipo_ord_acq  
into   :ls_des_tipo_ord_acq  
from   tab_tipi_ord_acq  
where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_acq)
end if

select des_pagamento,   
       sconto,
		 flag_tipo_pagamento
into   :ls_des_pagamento,   
       :ld_sconto_pagamento,
		 :ls_flag_tipo_pagamento
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

choose case ls_flag_tipo_pagamento
	case "R"
		select des_banca, cod_abi, cod_cab,
				 iban
		into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab,
				:ls_iban
		from   anag_banche
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_banca = :ls_cod_banca;
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_banca)
			setnull(ls_iban)
		else
			if isnull(ls_des_banca) then ls_des_banca = ""
			if isnull(ls_cod_abi) then ls_cod_abi = "- - -"
			if isnull(ls_cod_cab) then ls_cod_cab = "- - -"
			ls_des_banca = ls_des_banca + " ABI: " + ls_cod_abi + " CAB:" + ls_cod_cab
		end if
	case else
		select des_banca, cod_abi, cod_cab,iban
		into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab, :ls_iban
		from   anag_banche_clien_for  
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_banca_clien_for = :ls_cod_banca_clien_for;
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_banca)
			setnull(ls_iban)
		else
			if isnull(ls_des_banca) then ls_des_banca = ""
			if isnull(ls_cod_abi) then ls_cod_abi = "- - -"
			if isnull(ls_cod_cab) then ls_cod_cab = "- - -"
			ls_des_banca = ls_des_banca + " ABI: " + ls_cod_abi + " CAB:" + ls_cod_cab
		end if
end choose

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_mezzo
into   :ls_des_mezzo
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo   = :ls_cod_mezzo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_mezzo   = :ls_cod_mezzo and  
       cod_lingua  = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if

declare cu_dettagli cursor for 
	select   det_ord_acq.cod_tipo_det_acq, 
				det_ord_acq.cod_misura, 
				det_ord_acq.quan_ordinata, 
				det_ord_acq.prezzo_acquisto, 
				det_ord_acq.sconto_1, 
				det_ord_acq.sconto_2, 
				det_ord_acq.imponibile_iva_valuta, 
				det_ord_acq.data_consegna, 
				det_ord_acq.nota_dettaglio, 
				det_ord_acq.cod_prodotto, 
				det_ord_acq.des_prodotto,
				det_ord_acq.cod_prod_fornitore,
				det_ord_acq.fat_conversione,
				det_ord_acq.flag_saldo,
				det_ord_acq.quan_arrivata,
				det_ord_acq.flag_st_note_det,
				det_ord_acq.cod_disegno,
				det_ord_acq.rev_disegno
	from     det_ord_acq 
	where    det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				det_ord_acq.anno_registrazione = :il_anno_registrazione and 
				det_ord_acq.num_registrazione = :il_num_registrazione
	order by det_ord_acq.cod_azienda, 
				det_ord_acq.anno_registrazione, 
				det_ord_acq.num_registrazione,
				det_ord_acq.prog_riga_ordine_acq;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ls_cod_tipo_det_acq, 
								  :ls_cod_misura, 
								  :ld_quan_ordinata, 
								  :ld_prezzo_acquisto,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ls_cod_prod_fornitore,
								  :ld_fat_conversione,
								  :ls_flag_saldo,
								  :ld_quan_arrivata,
								  :ls_flag_st_note_det,
								  :ls_cod_disegno,
								  :ls_rev_disegno;								  

   if sqlca.sqlcode <> 0 then exit
	
	if not(ib_stampa_residuo) or (ls_flag_saldo ="N" and ib_stampa_residuo and ld_quan_ordinata > ld_quan_arrivata) then

		dw_report_ord_acq.insertrow(0)
		dw_report_ord_acq.setrow(dw_report_ord_acq.rowcount())
	
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_anno_registrazione", il_anno_registrazione)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_num_registrazione", il_num_registrazione)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_tipo_ord_acq", ls_cod_tipo_ord_acq)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_data_registrazione", ldt_data_registrazione)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_fornitore", ls_cod_fornitore)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_fil_fornitore", ls_cod_fil_fornitore)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_valuta", ls_cod_valuta)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_sconto", ld_sconto)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_num_ord_fornitore", ls_num_ord_fornitore)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_data_ord_fornitore", ldt_data_ord_fornitore)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "formato", ls_formato)
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_st_note_tes = 'I' then   //nota di testata
		select flag_st_note
		  into :ls_flag_st_note_tes
		  from tab_tipi_ord_acq	
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
	end if		

	if ls_flag_st_note_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_st_note_pie = 'I' then //nota di piede
		select flag_st_note
		  into :ls_flag_st_note_pie
		  from tab_tipi_ord_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
	end if			
	
	if ls_flag_st_note_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_nota_testata", ls_nota_testata)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_nota_piede", ls_nota_piede)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_porto", ls_cod_porto)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_resa", ls_cod_resa)
		if ls_flag_tipo_fornitore = "C" then
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_tot_val_ordine", ld_imponibile_iva)	
		else
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_tot_val_ordine", ld_tot_val_ordine)	
		end if
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_indirizzo", ls_indirizzo)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_localita", ls_localita)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_frazione", ls_frazione)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cap", ls_cap)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_provincia", ls_provincia)
	
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_rag_soc_1", ls_rag_soc_1_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_rag_soc_2", ls_rag_soc_2_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_indirizzo", ls_indirizzo_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_localita", ls_localita_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_frazione", ls_frazione_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_cap", ls_cap_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_provincia", ls_provincia_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_partita_iva", ls_partita_iva_for)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_rif_interno", ls_rif_interno)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_telefono", ls_telefono)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_fax", ls_fax)
		
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_rag_soc_1", ls_rag_soc_1_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_rag_soc_2", ls_rag_soc_2_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_indirizzo", ls_indirizzo_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_localita", ls_localita_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_frazione", ls_frazione_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_cap", ls_cap_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_provincia", ls_provincia_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_telefono", ls_telefono_fil)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_fax", ls_fax_fil)		
	
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_tipi_ord_acq_des_tipo_ord_acq", ls_des_tipo_ord_acq)
		
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_banche_clien_for_iban", ls_iban)
	
		select tab_tipi_det_acq.flag_stampa_ordine
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = 'S' then
			select anag_prodotti.des_prodotto  
			into   :ls_des_prodotto_anag  
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	
			select anag_prodotti_lingue.des_prodotto  
			into   :ls_des_prodotto_lingua  
			from   anag_prodotti_lingue  
			where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
					 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_lingua)
			end if
	
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_cod_misura", ls_cod_misura)
			if ib_stampa_residuo then ld_quan_ordinata = ld_quan_ordinata - ld_quan_arrivata
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_quan_ordinata", ld_quan_ordinata * ld_fat_conversione)
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_prezzo_acquisto", ld_prezzo_acquisto / ld_fat_conversione)
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_sconto_1", ld_sconto_1)
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_sconto_2", ld_sconto_2)
			if not(ib_stampa_residuo) then 
				dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_val_riga", ld_val_riga)
			else
				dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_val_riga", 0)
			end if
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_data_consegna", ldt_data_consegna)
			
			select des_prod_fornitore
			  into :ls_des_prodotto_fornitore
			  from tab_prod_fornitori
			 where cod_azienda   = :s_cs_xx.cod_azienda
				and cod_prodotto  = :ls_cod_prodotto
				and cod_fornitore = :ls_cod_fornitore;
	
			if sqlca.sqlcode <> 0 then
				ls_des_prodotto_fornitore = ""
			end if	
	
			if not isnull(ls_cod_prod_fornitore) and not isnull(ls_nota_dettaglio) then
				if isnull(ls_des_prodotto_fornitore) then ls_des_prodotto_fornitore = ""
				ls_nota_dettaglio = "Vs.Codice : " + ls_cod_prod_fornitore + " " + ls_des_prodotto_fornitore + "~r~n" +  "                 " + ls_nota_dettaglio
			elseif not isnull(ls_cod_prod_fornitore) and isnull(ls_nota_dettaglio) then
				if isnull(ls_des_prodotto_fornitore) then ls_des_prodotto_fornitore = ""
				ls_nota_dettaglio = "Vs.Codice: " + ls_cod_prod_fornitore + " " + ls_des_prodotto_fornitore
			elseif not isnull(ls_cod_prod_fornitore) and isnull(ls_nota_dettaglio) then			
				ls_nota_dettaglio = "                 " + ls_nota_dettaglio			
			elseif (isnull(ls_cod_prod_fornitore) or ls_cod_prod_fornitore = "") and (isnull(ls_nota_dettaglio) or ls_nota_dettaglio = "") then	
				setnull(ls_nota_dettaglio)
			end if	
			
			//------------------------------------------------- Modifica Nicola ---------------------------------------------
			if ls_flag_st_note_det = 'I' then //nota dettaglio
				select flag_st_note_or
				  into :ls_flag_st_note_det
				  from tab_tipi_det_acq
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
			end if		
			
			if ls_flag_st_note_det = 'N' then
				ls_nota_dettaglio = ""
			end if				
			//-------------------------------------------------- Fine Modifica ----------------------------------------------			
			
			//05/02/2015: stampa codice disegno e revisione in coda alla nota di dettaglio
			if ls_flag_stampa_disegno="S" and ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
				if ls_cod_disegno<>"" and not isnull(ls_cod_disegno) then
					
					if isnull(ls_nota_dettaglio) then ls_nota_dettaglio = ""

					//se c'è già qualcosa vai prima a capo
					if ls_nota_dettaglio<>"" then ls_nota_dettaglio += "~r~n"
					
					ls_nota_dettaglio += +"                 "+"ESEGUITO SECONDO IL DISEGNO N. "+ls_cod_disegno
					
					if ls_rev_disegno<>"" and not isnull(ls_rev_disegno) then
						ls_nota_dettaglio += " - REV." + ls_rev_disegno
					end if
				end if
			end if
			//--------------------------------
			
			
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_nota_dettaglio", ls_nota_dettaglio)
			
			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_cod_prodotto", ls_cod_prodotto)
			if not isnull(ls_des_prodotto) or len(ls_des_prodotto) > 0 then
				dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_des_prodotto", ls_des_prodotto)
	//		elseif not isnull(ls_des_prodotto_fornitore) or len(ls_des_prodotto_fornitore) > 0 then
	//			dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_des_prodotto", ls_des_prodotto_fornitore)
			elseif not isnull(ls_des_prodotto_lingua) or len(ls_des_prodotto_lingua) > 0 then
				dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_des_prodotto", ls_des_prodotto_lingua)
			elseif not isnull(ls_des_prodotto_anag) or len(ls_des_prodotto_anag) > 0 then
				dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_des_prodotto", ls_des_prodotto_anag)
			end if
		end if
		
		select des_operatore 
		  into :ls_des_operatore
		  from tab_operatori
		 where cod_azienda   = :s_cs_xx.cod_azienda
			and cod_operatore = :ls_cod_operatore;
			
		if sqlca.sqlcode <> 0 then setnull(ls_cod_operatore)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_operatori_des_operatore", ls_des_operatore)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_porti_des_porto", ls_des_porto)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_rese_des_resa", ls_des_resa)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_mezzi_des_mezzo", ls_des_mezzo)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_mezzi_lingue_des_mezzo", ls_des_mezzo_lingua)
	end if
loop
close cu_dettagli;
//dw_report_ord_acq.reset_dw_modified(c_resetchildren)
//dw_report_ord_acq.change_dw_current()


dw_report_ord_acq.resetupdate()
dw_report_ord_acq.change_dw_current()

dw_report_ord_acq.object.datawindow.print.preview = "Yes"
dw_report_ord_acq.setredraw(true)

end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify, ls_path_firma,ls_cod_oggetto
s_report_ordine ls_report_ordine
dw_report_ord_acq.ib_dw_report = true

ls_report_ordine = message.powerobjectparm
ib_stampa_residuo = ls_report_ordine.ab_flag_residuo
il_anno_registrazione = ls_report_ordine.al_anno_registrazione
il_num_registrazione = ls_report_ordine.al_num_registrazione

dw_report_ord_acq.set_document_name("Ordine Acquisto " + string(il_anno_registrazione) + "/" + string(il_num_registrazione))
set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)

dw_report_ord_acq.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_disablecc, &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)


// *** Michela 19/12/2005: se la data del documento è inferiore al parametro aziendale DLD metto 
//                         il logo contenuto nel parametro LI2

long     ll_anno_registrazione, ll_num_registrazione
datetime ldt_dld, ldt_data_registrazione
											
ll_anno_registrazione = il_anno_registrazione //dw_report_ord_acq.i_parentdw.getitemnumber(dw_report_ord_acq.i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = il_num_registrazione //dw_report_ord_acq.i_parentdw.getitemnumber(dw_report_ord_acq.i_parentdw.i_selectedrows[1], "num_registrazione")

select data
into   :ldt_dld
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'DLD';

if sqlca.sqlcode <> 0 then
	setnull(ldt_dld)
end if

setnull(ldt_data_registrazione)

select data_registrazione 
into   :ldt_data_registrazione	 
from   tes_ord_acq  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :ll_anno_registrazione and 
		 num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode <> 0 then	
	setnull(ldt_data_registrazione)
end if		 

if not isnull(ldt_data_registrazione) and not isnull(ldt_dld) then
	if ldt_data_registrazione < ldt_dld then
		select parametri_azienda.stringa
		into   :ls_path_logo_1
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'I2L';		
	else
		select parametri_azienda.stringa
		into   :ls_path_logo_1
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LO1';		
	end if
else
		select parametri_azienda.stringa
		into   :ls_path_logo_1
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LO1';	
end if												
												
select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

select parametri_azienda.stringa
into   :ls_cod_oggetto
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'FR1';

if sqlca.sqlcode = 0 then
	select path_oggetto
	into   :ls_path_firma
	from   utenti_oggetti
	where  cod_azienda = :s_cs_xx.cod_azienda and
   	    cod_utente = :s_cs_xx.cod_utente and 
      	 cod_oggetto = :ls_cod_oggetto;

	ls_modify = "firma.filename='" + ls_path_firma + "'~t"
	dw_report_ord_acq.modify(ls_modify)
end if

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_ord_acq.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"
dw_report_ord_acq.modify(ls_modify)


if ib_stampa_residuo then
	g_mb.messagebox("APICE","Durante la stampa del residuo ordine non verrà visualizzato il valore riga",Information!)
end if

this.y = 0
this.height = w_cs_xx_mdi.mdi_1.height - 50


end event

on w_report_ord_acq_euro.create
int iCurrent
call super::create
this.dw_report_ord_acq=create dw_report_ord_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_ord_acq
end on

on w_report_ord_acq_euro.destroy
call super::destroy
destroy(this.dw_report_ord_acq)
end on

event resize;dw_report_ord_acq.resize(newwidth - 40, newheight -40)
end event

type dw_report_ord_acq from uo_cs_xx_dw within w_report_ord_acq_euro
integer x = 23
integer y = 20
integer width = 3817
integer height = 2796
integer taborder = 20
string dataobject = "d_report_ord_acq_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean ib_dw_report = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

