﻿$PBExportHeader$w_report_solleciti.srw
$PBExportComments$Finestra Report Solleciti da Ordini di Acquisto
forward
global type w_report_solleciti from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_solleciti
end type
type dw_report_solleciti from uo_cs_xx_dw within w_report_solleciti
end type
type em_data_riferimento from editmask within w_report_solleciti
end type
type st_1 from statictext within w_report_solleciti
end type
end forward

global type w_report_solleciti from w_cs_xx_principale
integer width = 3561
integer height = 1664
string title = "Report Solleciti Giornalieri"
cb_annulla cb_annulla
dw_report_solleciti dw_report_solleciti
em_data_riferimento em_data_riferimento
st_1 st_1
end type
global w_report_solleciti w_report_solleciti

type variables
string is_flag_tipo_nc
end variables

event pc_setwindow;call super::pc_setwindow;dw_report_solleciti.ib_dw_report = true

em_data_riferimento.text = string(today())
set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup + c_toolbarnone)

dw_report_solleciti.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nomodify + &
                                c_nodelete + &
                                c_newonopen + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )

iuo_dw_main = dw_report_solleciti
save_on_close(c_socnosave)
end event

on w_report_solleciti.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.dw_report_solleciti=create dw_report_solleciti
this.em_data_riferimento=create em_data_riferimento
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.dw_report_solleciti
this.Control[iCurrent+3]=this.em_data_riferimento
this.Control[iCurrent+4]=this.st_1
end on

on w_report_solleciti.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.dw_report_solleciti)
destroy(this.em_data_riferimento)
destroy(this.st_1)
end on

type cb_annulla from commandbutton within w_report_solleciti
integer x = 3127
integer y = 1460
integer width = 361
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

on clicked;close(parent)
end on

type dw_report_solleciti from uo_cs_xx_dw within w_report_solleciti
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_solleciti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
date ldate_data_riferimento

ldate_data_riferimento = date(em_data_riferimento.text)

ll_errore = retrieve(s_cs_xx.cod_azienda, ldate_data_riferimento)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type em_data_riferimento from editmask within w_report_solleciti
integer x = 2720
integer y = 1460
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
string minmax = "01/01/1900~~31/12/29999"
end type

event modified;long ll_errore
date ldate_data_riferimento

ldate_data_riferimento = date(em_data_riferimento.text)

ll_errore = dw_report_solleciti.retrieve(s_cs_xx.cod_azienda, ldate_data_riferimento)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type st_1 from statictext within w_report_solleciti
integer x = 2263
integer y = 1460
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Data Selezione: "
alignment alignment = right!
boolean border = true
boolean focusrectangle = false
end type

