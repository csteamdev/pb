﻿$PBExportHeader$w_sel_deposito_terzista.srw
forward
global type w_sel_deposito_terzista from window
end type
type st_titolo from statictext within w_sel_deposito_terzista
end type
type st_1 from statictext within w_sel_deposito_terzista
end type
type cb_seleziona from commandbutton within w_sel_deposito_terzista
end type
type dw_lista from datawindow within w_sel_deposito_terzista
end type
end forward

global type w_sel_deposito_terzista from window
integer width = 2450
integer height = 1192
boolean titlebar = true
string title = "Seleziona Deposito"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_titolo st_titolo
st_1 st_1
cb_seleziona cb_seleziona
dw_lista dw_lista
end type
global w_sel_deposito_terzista w_sel_deposito_terzista

on w_sel_deposito_terzista.create
this.st_titolo=create st_titolo
this.st_1=create st_1
this.cb_seleziona=create cb_seleziona
this.dw_lista=create dw_lista
this.Control[]={this.st_titolo,&
this.st_1,&
this.cb_seleziona,&
this.dw_lista}
end on

on w_sel_deposito_terzista.destroy
destroy(this.st_titolo)
destroy(this.st_1)
destroy(this.cb_seleziona)
destroy(this.dw_lista)
end on

event open;string			ls_cod_fornitore, ls_rag_soc_1


ls_cod_fornitore = message.stringparm
ls_rag_soc_1 = f_des_tabella("anag_fornitori","cod_fornitore = '" +  ls_cod_fornitore +"'", "rag_soc_1")
st_titolo.text = "Depositi del fornitore " + ls_cod_fornitore + " - " + ls_rag_soc_1


dw_lista.settransobject(sqlca)
dw_lista.setfilter("cod_fornitore='"+ls_cod_fornitore+"'")
dw_lista.filter()

dw_lista.retrieve(s_cs_xx.cod_azienda)
end event

type st_titolo from statictext within w_sel_deposito_terzista
integer x = 32
integer y = 16
integer width = 2382
integer height = 124
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Depositi del fornitore"
boolean focusrectangle = false
end type

type st_1 from statictext within w_sel_deposito_terzista
integer x = 507
integer y = 164
integer width = 1888
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Doppio clic sul deposito oppure seleziona il deposito e clic su OK"
boolean focusrectangle = false
end type

type cb_seleziona from commandbutton within w_sel_deposito_terzista
integer x = 41
integer y = 148
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;long			ll_row
string			ls_cod_deposito

ll_row = dw_lista.getrow()

if ll_row>0 then
	ls_cod_deposito = dw_lista.getitemstring(ll_row, "cod_deposito")
	
	if g_mb.confirm("Selezionare il deposito "+ ls_cod_deposito + "?") then
		closewithreturn(parent, ls_cod_deposito)
	end if
else
	g_mb.warning("Selezionare una riga!")
	return
end if
end event

type dw_lista from datawindow within w_sel_deposito_terzista
integer x = 32
integer y = 252
integer width = 2363
integer height = 840
integer taborder = 10
string title = "none"
string dataobject = "d_depositi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;
string			ls_cod_deposito


if row>0 then
	ls_cod_deposito = dw_lista.getitemstring(row, "cod_deposito")
	
	if g_mb.confirm("Selezionare il deposito "+ ls_cod_deposito + "?") then
		closewithreturn(parent, ls_cod_deposito)
	end if
else
	g_mb.warning("Selezionare una riga!")
	return
end if
end event

event rowfocuschanged;

if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

