﻿$PBExportHeader$w_sel_verniciature_ord_acq.srw
forward
global type w_sel_verniciature_ord_acq from window
end type
type cb_annulla from commandbutton within w_sel_verniciature_ord_acq
end type
type st_1 from statictext within w_sel_verniciature_ord_acq
end type
type dw_lista from datawindow within w_sel_verniciature_ord_acq
end type
end forward

global type w_sel_verniciature_ord_acq from window
integer width = 3186
integer height = 1156
boolean titlebar = true
string title = "Seleziona Verniciatura"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
st_1 st_1
dw_lista dw_lista
end type
global w_sel_verniciature_ord_acq w_sel_verniciature_ord_acq

on w_sel_verniciature_ord_acq.create
this.cb_annulla=create cb_annulla
this.st_1=create st_1
this.dw_lista=create dw_lista
this.Control[]={this.cb_annulla,&
this.st_1,&
this.dw_lista}
end on

on w_sel_verniciature_ord_acq.destroy
destroy(this.cb_annulla)
destroy(this.st_1)
destroy(this.dw_lista)
end on

event open;s_cs_xx_parametri			ls_cs_xx_parametri
string								ls_cod_veloce, ls_like, ls_sql

ls_cs_xx_parametri = message.powerobjectparm

ls_cod_veloce = ls_cs_xx_parametri.parametro_s_1_a[1]
ls_like = ls_cs_xx_parametri.parametro_s_1_a[2]

dw_lista.settransobject(sqlca)


ls_sql = dw_lista.getsqlselect()
ls_sql += " where tab_verniciatura.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"tab_verniciatura.cod_veloce='"+ls_cod_veloce+"' and "+&
						"tab_verniciatura.cod_classe_merceolo like '"+ls_like+"' and "+&
						"((tab_verniciatura.flag_blocco<>'S') or (tab_verniciatura.flag_blocco='S' and tab_verniciatura.data_blocco>"+s_cs_xx.db_funzioni.oggi+"))"

dw_lista.setsqlselect(ls_sql)
dw_lista.retrieve()
end event

type cb_annulla from commandbutton within w_sel_verniciature_ord_acq
integer x = 2747
integer y = 948
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;closewithreturn(parent, "")
end event

type st_1 from statictext within w_sel_verniciature_ord_acq
integer x = 32
integer y = 32
integer width = 2459
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Doppio clic sulla singola verniciatura per selezionare"
boolean focusrectangle = false
end type

type dw_lista from datawindow within w_sel_verniciature_ord_acq
integer x = 27
integer y = 112
integer width = 3122
integer height = 792
integer taborder = 10
string title = "none"
string dataobject = "d_sel_verniciature_ord_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;string			ls_cod_verniciatura

if row>0 then
	ls_cod_verniciatura = dw_lista.getitemstring(row, "cod_verniciatura")
	closewithreturn(parent, ls_cod_verniciatura)
end if
end event

event rowfocuschanged;

if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

