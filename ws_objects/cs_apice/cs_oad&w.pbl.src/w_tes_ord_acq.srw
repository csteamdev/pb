﻿$PBExportHeader$w_tes_ord_acq.srw
forward
global type w_tes_ord_acq from w_cs_xx_treeview
end type
type dw_documenti from uo_dw_drag_doc_acq_ven within det_1
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_3 dw_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_4 dw_4
end type
type ws_record from structure within w_tes_ord_acq
end type
end forward

type ws_record from structure
	long		anno_registrazione
	long		num_registrazione
	string		descrizione_2
	string		codice
	string		descrizione
	long		handle_padre
	string		tipo_livello
	integer		livello
end type

global type w_tes_ord_acq from w_cs_xx_treeview
integer width = 4768
integer height = 2440
string title = "Ordini Acquisto"
event type integer ue_calcola_documento ( )
event pc_menu_salda ( )
event pc_menu_duplica ( )
event pc_menu_calcola ( )
event pc_menu_blocca ( )
event pc_menu_sblocca ( )
event pc_menu_communicazioni ( )
event pc_menu_invia ( )
event pc_menu_respingi ( )
event pc_menu_corrispondenze ( )
event pc_menu_note ( )
event pc_menu_gen_bolla_uscita ( )
event pc_menu_stampa ( )
event pc_menu_dettagli ( )
event pc_esporta ( )
event pc_esporta_xls_std ( )
event pc_esporta_xls_viv ( )
event pc_esporta_txt_anodall ( )
event pc_esporta_csv_bustreo ( )
event pc_menu_stampa_residuo ( )
event pc_menu_carico_acquisti ( )
event pc_menu_doc_fiscali ( )
end type
global w_tes_ord_acq w_tes_ord_acq

type variables
private:
	datastore ids_store
	long il_livello
	string is_sql_prodotto
	
	// icone
	int ICONA_ORIDINE, ICONA_OPERATORE, ICONA_TIPO_ORDINE, ICONA_ANNO, ICONA_DATA_REGISTRAZIONE
	int ICONA_DATA_CONSEGNA, ICONA_DEPOSITO, ICONA_DEPOSITO_TRANF, ICONA_FORNITORE, ICONA_PRODOTTO
	
	boolean ib_new
	
	string is_cod_nota_excel = "XLS", is_estensione_excel="xls",  is_des_nota="Esportazione Ordine Acquisto", is_cod_nota_txt = "TXT"
	
	string	is_cod_parametro_blocco="BOF"

	//solo per tracciato VIV
	string	is_chiave_misura = "", is_chiave_hh_imp=""	//gestione con chiavi prodotti

	//parametro aziendale CCF per tracciato ANODALL-EXTRUSION
	string is_codice_cliente_fornitore="" //(001599 per gruppo GIBUS)
	
	//parametro aziendale CSM per tracciato BUSTREO-PETTENUZZO
	string is_CodSedeMago="" //(001599 per gruppo GIBUS)
	
	boolean ib_esporta_gibus = false
	
	long il_max_row = 255
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function long wf_inserisci_ordini_acquisti (long al_handle)
public subroutine wf_valori_livelli ()
public subroutine wf_treeview_icons ()
public function long wf_inserisci_operatori (long al_handle)
public function long wf_inserisci_tipo_ordine (long al_handle)
public function long wf_inserisci_anno (long al_handle)
public function long wf_inserisci_data_registrazione (long al_handle)
public function long wf_inserisci_data_consegna (long al_handle)
public function long wf_inserisci_depositi (long al_handle)
public function long wf_inserisci_depositi_trasf (long al_handle)
public function long wf_inserisci_fornitori (long al_handle)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_inserisci_prodotti (long al_handle)
public function integer wf_duplica_righe (long fl_anno, long fl_numero, long fl_anno_new, long fl_num_new)
public function boolean wf_user_can (long al_anno, long al_numero, string as_cod_tipo_ord_acq)
public function integer wf_firma_documento ()
public function long wf_inserisci_ordine_acquisto (long al_handle, long al_anno_registrazione, long al_num_registrazione)
public function integer wf_duplica (long fl_anno, long fl_numero, ref long fl_anno_new, ref long fl_numero_new)
public function integer wf_chiavi_prodotto (string fs_cod_prodotto, string fs_chiave, decimal fd_fattore, ref string fs_valore, ref string fs_errore)
public function integer wf_cod_cliente_fornitore (ref string fs_errore)
public function boolean wf_conferma_esportazione (long fl_anno_reg, long fl_num_reg, string fs_cod_nota, ref boolean fb_esiste, ref string fs_errore)
public function string wf_prepara_campo (string fs_valore, long fl_lunghezza)
public function string wf_prepara_campo_numerico (decimal fd_valore, long fl_lunghezza, long fl_decimali)
public function integer wf_estrai_articolo_fornitore (string fs_codice, ref string fs_articolo_fornitore, ref string fs_errore)
public function integer wf_leggi_chiave (string fs_parametro, ref string fs_cod_chiave, ref string fs_errore)
public function integer wf_salva_file (string fs_path, long fl_anno_reg, long fl_num_reg, boolean fb_esiste, string fs_cod_nota, string fs_estensione, ref string fs_errore)
public function string wf_verniciatura (string fs_cod_prodotto)
public function integer wf_esporta_standard (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore)
public function integer wf_esporta_viv (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore)
public function integer wf_esporta_anodall (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore)
public function integer wf_esporta_bustreo (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore)
public function string wf_prepara_campo_numerico (decimal fd_valore, long fl_decimali)
public function integer wf_destinazione_bustreo (string fs_separatore, string fs_cod_deposito, string fs_cod_deposito_trasf, string fs_rag_soc_1, string fs_rag_soc_2, string fs_indirizzo, string fs_cap, string fs_localita, string fs_provincia, ref string fs_riga)
public subroutine wf_stampa (boolean ab_flag_residuo)
public function integer wf_verniciatura_transcodifica (string fs_cod_verniciatura, string fs_cod_fornitore, ref string fs_verniciatura, ref string fs_errore)
public function string wf_prepara_campo_numerico_bustreo (decimal fd_valore, long fl_lunghezza, long fl_decimali)
public function integer wf_tab_prod_fornitori (string fs_cod_prodotto, string fs_cod_fornitore, ref string fs_cod_prod_fornitore, ref long fl_misura, ref string fs_errore)
public function integer wf_leggi_cod_prod_fornitore_2 (string fs_cod_prodotto, string fs_cod_fornitore, ref string fs_cod_prod_fornitore_2, ref string fs_errore)
public function integer wf_chiedi_file (ref string fs_path, long fl_anno_reg, long fl_num_reg, string fs_estensione, string fs_prefisso_file, ref string fs_errore)
public function integer wf_converti_in_pezzi (integer fi_anno, long fl_numero, long fl_riga, ref decimal fd_quantita, ref string fs_errore)
public function integer wf_destinazione (string fs_cod_deposito, string fs_cod_deposito_trasf, string fs_rag_soc_1, string fs_indirizzo, string fs_cap, string fs_localita, string fs_provincia, ref string fs_riga)
public function integer wf_reimposta_data_consegna (long al_row, integer ai_modalita_cambio, ref string as_errore)
end prototypes

event type integer ue_calcola_documento();string ls_messaggio

long   ll_anno_reg, ll_num_reg

uo_calcola_documento_euro luo_doc


ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber( tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg =  tab_dettaglio.det_1.dw_1.getitemnumber( tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

if not isnull(ll_anno_reg) and ll_anno_reg <> 0 and not isnull(ll_num_reg) and ll_num_reg <> 0 then

	luo_doc = create uo_calcola_documento_euro

	if luo_doc.uof_calcola_documento(ll_anno_reg,ll_num_reg,"ord_acq",ls_messaggio) <> 0 then
		g_mb.messagebox("Apice",ls_messaggio)
		rollback;
		destroy luo_doc
		return -1
	else
		commit;	
	end if

	destroy luo_doc

	 tab_dettaglio.det_1.dw_1.postevent("pcd_retrieve")
	
end if	

return 0
end event

event pc_menu_salda();string ls_cod_tipo_det_acq, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_acq, &
		 ls_flag_blocco, ls_flag_saldo,ls_cod_prodotto_raggruppato
		 
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine_acq

dec{4} ld_quan_ordinata, ld_quan_arrivata,ld_quan_raggruppo

if tab_dettaglio.det_1.dw_1.getrow() > 0 then
	if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_evasione") <> "E" then
		ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
		ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")

		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select det_ord_acq.cod_tipo_det_acq, det_ord_acq.prog_riga_ordine_acq, det_ord_acq.cod_prodotto, det_ord_acq.quan_ordinata, det_ord_acq.quan_arrivata, det_ord_acq.flag_blocco, det_ord_acq.flag_saldo from det_ord_acq where det_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_acq.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_acq.num_registrazione = " + string(ll_num_registrazione)

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ll_i = 0
		do while 0 = 0
		   ll_i ++
		   fetch cu_dettagli into :ls_cod_tipo_det_acq, :ll_prog_riga_ordine_acq, :ls_cod_prodotto, :ld_quan_ordinata, :ld_quan_arrivata, :ls_flag_blocco, :ls_flag_saldo;
			   if sqlca.sqlcode = -1 then
		      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura per aggiornamento magazzino.", exclamation!, ok!)
				close cu_dettagli;
				rollback;
		  	   return
		   end if

		   if sqlca.sqlcode <> 0 then exit

		   select tab_tipi_det_acq.flag_tipo_det_acq
		   into   :ls_flag_tipo_det_acq
		   from   tab_tipi_det_acq
		   where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
		          tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

		   if sqlca.sqlcode = -1 then
		      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.", &
		                 exclamation!, ok!)
				close cu_dettagli;
				rollback;
			   return
		   end if

			if ls_flag_tipo_det_acq = "M" and ls_flag_saldo = 'N' and ld_quan_arrivata < ld_quan_ordinata and ls_flag_blocco = "N" then
				update anag_prodotti  
					set quan_ordinata = quan_ordinata - (:ld_quan_ordinata - :ld_quan_arrivata)
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
					rollback;
					close cu_dettagli;
					return
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordinata - ld_quan_arrivata), "M")
					
					update anag_prodotti  
						set quan_ordinata = quan_ordinata - :ld_quan_raggruppo
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto_raggruppato;
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
						rollback;
						close cu_dettagli;
						return
					end if
				end if
				
				
			end if				
			
			update det_ord_acq
			set flag_saldo ='S'
				//set flag_evasione = 'E'
			 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_acq.anno_registrazione = :ll_anno_registrazione and  
					 det_ord_acq.num_registrazione = :ll_num_registrazione and
					 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
							  exclamation!, ok!)
				rollback;
				close cu_dettagli;
				return
			end if

			update tes_ord_acq
			set flag_evasione = 'E'
			where tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_ord_acq.anno_registrazione = :ll_anno_registrazione and  
					tes_ord_acq.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
							  exclamation!, ok!)
				rollback;
				close cu_dettagli;
				return
			end if
		loop

		close cu_dettagli;
	
		tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_evasione", "E")
		tab_dettaglio.det_1.dw_1.setitemstatus(tab_dettaglio.det_1.dw_1.getrow(), "flag_evasione", primary!, notmodified!)
		commit;
	end if
end if


end event

event pc_menu_duplica();long ll_row, ll_anno, ll_numero, ll_anno_new, ll_numero_new

ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
	ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_numero = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	
	setpointer(Hourglass!)
	
	if wf_duplica(ll_anno, ll_numero, ll_anno_new, ll_numero_new) > 0 then
		
		wf_inserisci_ordine_acquisto(0, ll_anno_new, ll_numero_new)
		
	end if
	
	setpointer(Arrow!)
end if
end event

event pc_menu_calcola();string ls_messaggio

long   ll_anno_reg, ll_num_reg

uo_calcola_documento_euro luo_doc


ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

if not isnull(ll_anno_reg) and ll_anno_reg <> 0 and not isnull(ll_num_reg) and ll_num_reg <> 0 then

	luo_doc = create uo_calcola_documento_euro

	if luo_doc.uof_calcola_documento(ll_anno_reg,ll_num_reg,"ord_acq",ls_messaggio) <> 0 then
		g_mb.messagebox("Apice",ls_messaggio)
		rollback;
		destroy luo_doc
		return
	else
		commit;	
	end if

	destroy luo_doc

	tab_dettaglio.det_1.dw_1.postevent("pcd_retrieve")
	
end if	

return
end event

event pc_menu_blocca();string ls_cod_tipo_det_acq, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_acq, &
		 ls_flag_blocco, ls_flag_saldo,ls_cod_prodotto_raggruppato

long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine_acq

dec{4} ld_quan_ordinata, ld_quan_arrivata, ld_quan_raggruppo



if tab_dettaglio.det_1.dw_1.getrow() > 0 then
	if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = "N" then
		ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
		ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")

		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select det_ord_acq.cod_tipo_det_acq, det_ord_acq.prog_riga_ordine_acq, det_ord_acq.cod_prodotto, det_ord_acq.quan_ordinata, det_ord_acq.quan_arrivata, det_ord_acq.flag_blocco, det_ord_acq.flag_saldo from det_ord_acq where det_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_acq.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_acq.num_registrazione = " + string(ll_num_registrazione)

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ll_i = 0
		do while true
		   ll_i ++
		   fetch cu_dettagli into :ls_cod_tipo_det_acq, :ll_prog_riga_ordine_acq, :ls_cod_prodotto, :ld_quan_ordinata, :ld_quan_arrivata, :ls_flag_blocco, :ls_flag_saldo;
			   if sqlca.sqlcode = -1 then
		      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura per aggiornamento magazzino.", exclamation!, ok!)
				close cu_dettagli;
				rollback;
		  	   return
		   end if

		   if sqlca.sqlcode <> 0 then exit

		   select tab_tipi_det_acq.flag_tipo_det_acq
		   into   :ls_flag_tipo_det_acq
		   from   tab_tipi_det_acq
		   where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
		          tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

		   if sqlca.sqlcode = -1 then
		      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.", &
		                 exclamation!, ok!)
				close cu_dettagli;
				rollback;
			   return
		   end if

			if ls_flag_tipo_det_acq = "M" and ls_flag_saldo = 'N' and ld_quan_arrivata < ld_quan_ordinata and ls_flag_blocco = "N" then
				update anag_prodotti  
					set quan_ordinata = quan_ordinata - (:ld_quan_ordinata - :ld_quan_arrivata)
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
					rollback;
					close cu_dettagli;
					return
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordinata - ld_quan_arrivata), "M")
					
					update anag_prodotti  
						set quan_ordinata = quan_ordinata - :ld_quan_raggruppo
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto_raggruppato;
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
						return
					end if
				end if
				
				
			end if				
			
			update det_ord_acq
				set flag_blocco = 'S'
			 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_acq.anno_registrazione = :ll_anno_registrazione and  
					 det_ord_acq.num_registrazione = :ll_num_registrazione and
					 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
							  exclamation!, ok!)
				rollback;
				close cu_dettagli;
				return
			end if

			update tes_ord_acq
			set flag_blocco = 'S'
			where tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_ord_acq.anno_registrazione = :ll_anno_registrazione and  
					tes_ord_acq.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
							  exclamation!, ok!)
				rollback;
				close cu_dettagli;
				return
			end if
		loop

		close cu_dettagli;
		tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco", "S")
		tab_dettaglio.det_1.dw_1.setitemstatus(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco", primary!, notmodified!)
		commit;
	end if
end if
end event

event pc_menu_sblocca();string ls_cod_tipo_det_acq, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_acq, &
		 ls_flag_saldo, ls_flag_blocco,ls_cod_prodotto_raggruppato

long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine_acq

dec{4} ld_quan_ordinata, ld_quan_arrivata,ld_quan_raggruppo



if tab_dettaglio.det_1.dw_1.getrow() > 0 then
	ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
	ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")

	declare cu_dettagli dynamic cursor for sqlsa;

	ls_sql_stringa = "select det_ord_acq.cod_tipo_det_acq, det_ord_acq.prog_riga_ordine_acq, det_ord_acq.cod_prodotto, det_ord_acq.quan_ordinata, det_ord_acq.quan_arrivata, det_ord_acq.flag_saldo, det_ord_acq.flag_blocco from det_ord_acq where det_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_acq.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_acq.num_registrazione = " + string(ll_num_registrazione)

	prepare sqlsa from :ls_sql_stringa;

	open dynamic cu_dettagli;

	ll_i = 0
	do while 0 = 0
		ll_i ++
		fetch cu_dettagli into :ls_cod_tipo_det_acq, :ll_prog_riga_ordine_acq, :ls_cod_prodotto, :ld_quan_ordinata, :ld_quan_arrivata, :ls_flag_saldo, :ls_flag_blocco;
			if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura per aggiornamento magazzino.", &
						  exclamation!, ok!)
			close cu_dettagli;
			rollback;
			return
		end if

		if sqlca.sqlcode <> 0 then exit

		select tab_tipi_det_acq.flag_tipo_det_acq
		into   :ls_flag_tipo_det_acq
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.", &
						  exclamation!, ok!)
			close cu_dettagli;
			rollback;
			return
		end if

		if ls_flag_tipo_det_acq = "M" and ls_flag_saldo = 'N' and ls_flag_blocco = 'S' and ld_quan_arrivata < ld_quan_ordinata then
			update   anag_prodotti  
				set   quan_ordinata = quan_ordinata + (:ld_quan_ordinata - :ld_quan_arrivata)
				where cod_azienda = :s_cs_xx.cod_azienda and  
					   cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
				rollback;
				close cu_dettagli;
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordinata - ld_quan_arrivata), "M")
				
				update anag_prodotti  
				set    quan_ordinata = quan_ordinata + :ld_quan_raggruppo
				where  cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
					return
				end if
			end if
		end if				
		
		update det_ord_acq
			set flag_blocco = 'N'
		 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_acq.anno_registrazione = :ll_anno_registrazione and  
				 det_ord_acq.num_registrazione = :ll_num_registrazione and
				 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
						  exclamation!, ok!)
			rollback;
			close cu_dettagli;
			return
		end if

		update tes_ord_acq
		set flag_blocco = 'N'
		where tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_ord_acq.anno_registrazione = :ll_anno_registrazione and  
				tes_ord_acq.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
						  exclamation!, ok!)
			rollback;
			close cu_dettagli;
			return
		end if
	loop

	commit;
	close cu_dettagli;
	tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco", "N")
	tab_dettaglio.det_1.dw_1.setitemstatus(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco", primary!, notmodified!)
end if
end event

event pc_menu_communicazioni();long   ll_numero, ll_anno, ll_row

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row < 1 then return

ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_numero = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)

s_cs_xx.parametri.parametro_d_1 = ll_anno
s_cs_xx.parametri.parametro_d_2 = ll_numero

window_open(w_tes_ord_acq_det_comunicazioni,-1)
end event

event pc_menu_invia();string		ls_firma_chiusura, ls_cod_tipo_ord_acq, ls_cod_tipo_lista_dist, ls_cod_lista_dist, &
			ls_flag_gestione_fasi, ls_user_can
long		ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente
boolean	ib_flag_gestione_fasi = true
datetime ldt_firma

ll_riga = tab_dettaglio.det_1.dw_1.getrow()
if ll_riga < 1 then return

// se l'utente non è un resp. di fase esci
ls_cod_tipo_ord_acq = tab_dettaglio.det_1.dw_1.getitemstring(ll_riga, "cod_tipo_ord_acq")
ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "anno_registrazione")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "num_registrazione")

// sequenza corrente, prelevo da DB perchè nella DW potrebbe non essere aggiornato
select num_sequenza_corrente, firma_chiusura, data_chiusura
into :ll_num_sequenza_corrente, :ls_firma_chiusura, :ldt_firma
from tes_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno and
	num_registrazione = :ll_num;
// ----

// già firmata
if not isnull(ls_firma_chiusura) or ls_firma_chiusura <> "" then
	g_mb.show("APICE", "Ordine di acquisto già firmato in data " + string(ldt_firma, "dd/mm/yyyy") + " dal mansionario " + ls_firma_chiusura)
	return
end if

// L'utente può confermare l'ordine di acquisto?
select cod_resp_divisione
into   :ls_user_can
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_approva_ord_acq = 'S';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return 
elseif sqlca.sqlcode = 100 or isnull(ls_user_can) then
	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato all'approvazione dell'ordine di acquisto")
	return
end if
// ----

// Controllo lista dist associata
if isnull(ls_cod_tipo_ord_acq) or ls_cod_tipo_ord_acq = "" then return

select cod_tipo_lista_dist, cod_lista_dist
into :ls_cod_tipo_lista_dist, :ls_cod_lista_dist
from tab_tipi_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
	
if sqlca.sqlcode < 0 then
	g_mb.error("APICE", "Errore durante il controllo della lista distribuzione associata al tipio ordine")
	return
end if

if isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist = "" or isnull(ls_cod_lista_dist) or ls_cod_lista_dist = "" then
	g_mb.show("APICE", "Il tipo ordine non ha nessuna lista di distribuzione associato")
	return
end if

// Lista OK, controllo le fasi
select flag_gestione_fasi
into :ls_flag_gestione_fasi
from tab_tipi_liste_dist
where
	cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
	
if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"

if ls_flag_gestione_fasi = "S" then
	// L'utente è presente nell'attuale fase?
	if wf_user_can(ll_anno, ll_num, ls_cod_tipo_ord_acq) = false then return
	// ----
else
	ib_flag_gestione_fasi = false
end if

// Invio Mail
s_cs_xx.parametri.parametro_s_1 = "L"
s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
s_cs_xx.parametri.parametro_s_4 = ""
s_cs_xx.parametri.parametro_s_5 = "E' stata modificato l'ordine " + string(ll_anno) + "/" + string(ll_num)
s_cs_xx.parametri.parametro_s_6 = "E' stata modificato l'ordine " + string(ll_anno) + "/" + string(ll_num)
s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari della fase successiva?"
s_cs_xx.parametri.parametro_s_8 = ""
s_cs_xx.parametri.parametro_s_11 = string(ll_anno)
s_cs_xx.parametri.parametro_s_12 = string(ll_num)
s_cs_xx.parametri.parametro_s_15 = "OA"
s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
s_cs_xx.parametri.parametro_b_1 = ib_flag_gestione_fasi
					
openwithparm( w_invio_messaggi, 0)	

if s_cs_xx.parametri.parametro_b_2 then
	wf_firma_documento()
elseif ib_flag_gestione_fasi = false then
	if g_mb.confirm("OMNIA", "Considero l'ordine di acquisto come confermato?") then
		wf_firma_documento()
	end if
elseif not isnull(s_cs_xx.parametri.parametro_ul_1) then
	
	if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
	
	update tes_ord_acq
	set
		num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno and
		num_registrazione = :ll_num;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	commit;
end if
end event

event pc_menu_respingi();string		ls_firma_chiusura, ls_cod_tipo_ord_acq, ls_cod_tipo_lista_dist, ls_cod_lista_dist, &
			ls_flag_gestione_fasi, ls_user_can
long		ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente
datetime ldt_firma

ll_riga = tab_dettaglio.det_1.dw_1.getrow()
if ll_riga < 1 then return

// se l'utente non è un resp. di fase esci
ls_cod_tipo_ord_acq = tab_dettaglio.det_1.dw_1.getitemstring(ll_riga, "cod_tipo_ord_acq")
ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "anno_registrazione")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "num_registrazione")

// sequenza corrente, prelevo da DB perchè nella DW potrebbe non essere aggiornato
select num_sequenza_corrente, firma_chiusura, data_chiusura
into :ll_num_sequenza_corrente, :ls_firma_chiusura, :ldt_firma
from tes_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno and
	num_registrazione = :ll_num;
// ----

// già firmata
if not isnull(ls_firma_chiusura) or ls_firma_chiusura <> "" then
	g_mb.show("APICE", "Ordine di acquisto già firmato in data " + string(ldt_firma, "dd/mm/yyyy") + " dal mansionario " + ls_firma_chiusura)
	return
end if

// L'utente può confermare l'ordine di acquisto?
select cod_resp_divisione
into   :ls_user_can
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_approva_ord_acq = 'S';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ls_user_can) then
	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato a respingere dell'ordine di acquisto")
	return
end if
// ----

// L'utente è presente nell'attuale fase?
if wf_user_can(ll_anno, ll_num, ls_cod_tipo_ord_acq) = false then return
// ----

// Controllo lista dist associata
if isnull(ls_cod_tipo_ord_acq) or ls_cod_tipo_ord_acq = "" then return

select cod_tipo_lista_dist, cod_lista_dist
into :ls_cod_tipo_lista_dist, :ls_cod_lista_dist
from tab_tipi_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
	
if sqlca.sqlcode < 0 then
	g_mb.error("APICE", "Errore durante il controllo della lista distribuzione associata al tipio ordine")
	return
end if

if isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist = "" or isnull(ls_cod_lista_dist) or ls_cod_lista_dist = "" then
	g_mb.show("APICE", "Il tipo ordine non ha nessuna lista di distribuzione associato")
	return
end if

// Lista OK, controllo le fasi
select flag_gestione_fasi
into :ls_flag_gestione_fasi
from tab_tipi_liste_dist
where
	cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
	
if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"

if ls_flag_gestione_fasi <> "S" then return

// Invio Mail
s_cs_xx.parametri.parametro_s_1 = "L"
s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
s_cs_xx.parametri.parametro_s_4 = ""
s_cs_xx.parametri.parametro_s_5 = "E' stata respinto l'ordine " + string(ll_anno) + "/" + string(ll_num)
s_cs_xx.parametri.parametro_s_6 = "E' stata respinto l'ordine " + string(ll_anno) + "/" + string(ll_num)
s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari della fase successiva?"
s_cs_xx.parametri.parametro_s_8 = ""
s_cs_xx.parametri.parametro_s_11 = string(ll_anno)
s_cs_xx.parametri.parametro_s_12 = string(ll_num)
s_cs_xx.parametri.parametro_s_15 = "OA"
s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
s_cs_xx.parametri.parametro_b_1 = false
					
openwithparm( w_invio_messaggi, 0)	

if not isnull(s_cs_xx.parametri.parametro_ul_1) then
	
	if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
	
	update tes_ord_acq
	set
		num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno and
		num_registrazione = :ll_num;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	commit;
end if
end event

event pc_menu_corrispondenze();s_cs_xx.parametri.parametro_s_1 = "Ord_Acq"
window_open_parm(w_acq_ven_corr, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_menu_note();s_cs_xx.parametri.parametro_s_1 = "Ord_Acq"
window_open_parm(w_acq_ven_note, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_menu_gen_bolla_uscita();s_gen_bolla_uscita_lavorazione ls_bolla

if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"flag_blocco") = "S" then
	g_mb.messagebox("APICE","Ordine di acquisto bloccato: impossibile procedere",stopsign!)
	return
end if

if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"flag_evasione") = "E" then
	g_mb.messagebox("APICE","Ordine evaso: impossibile procedere",stopsign!)
	return
end if

if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"flag_evasione") = "P" then
	g_mb.messagebox("APICE","Ordine parzialmente evaso; saranno inviate in lavorazione solo quantità parziali.",stopsign!)
	return
end if

ls_bolla.cod_fornitore 	= tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_fornitore")
ls_bolla.anno_ord_acq   = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ls_bolla.num_ord_acq		= tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

openwithparm(w_gen_bolla_uscita_lavorazione, ls_bolla, this)
end event

event pc_menu_stampa();wf_stampa(false)
end event

event pc_menu_dettagli();s_cs_xx.parametri.parametro_w_ord_acq = this

if tab_dettaglio.det_1.dw_1.getitemnumber(1, "anno_registrazione") < 1 then return

window_open_parm(w_det_ord_acq, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_esporta();long ll_anno_reg, ll_num_reg, ll_getrow, ll_tracciato_esportazione, ll_ret
string ls_cod_fornitore, ls_errore
boolean lb_standard = false

ll_getrow = tab_dettaglio.det_1.dw_1.getrow()
if ll_getrow>0 then
else
	return
end if

ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")
ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_fornitore")

if ll_anno_reg>0 and ll_num_reg>0 and ls_cod_fornitore<>"" and not isnull(ls_cod_fornitore) then
else
	return
end if

if ib_esporta_gibus then
	//solo per gruppo gibus
	ll_tracciato_esportazione = guo_functions.uof_tipo_tracciato_ord_acq(ls_cod_fornitore, ls_errore)
	if ll_tracciato_esportazione<0 then
		g_mb.error(ls_errore)
		return
	end if
	
	//nel caso di tracciato 3 non mi serve attualmente nessuna chiave
	if ll_tracciato_esportazione=1 or ll_tracciato_esportazione=2 then
		
		is_chiave_misura = ""
		is_chiave_hh_imp=""
		
		//leggo il codice della chiave prodotto per MISURA che mi servirà nel tracciato VIV, ANODALL-EXTRUSION
		//per recuperarne i rispettivi valori
		ll_ret = wf_leggi_chiave("CP1", is_chiave_misura, ls_errore)
		if ll_ret < 0 then
			g_mb.error(ls_errore)
			return
			
		elseif ll_ret=0 then
			//non bloccante
			g_mb.show(ls_errore)
		end if
	end if
	
	choose case ll_tracciato_esportazione
		case 1		//tracciato file VIV: Verniciatura Industriale Veneta
			
			//leggo il codice della chiave prodotto per HH_IMP che mi servirà nel tracciato VIV
			//per recuperarne i rispettivi valori
			ll_ret = wf_leggi_chiave("CP2", is_chiave_hh_imp, ls_errore)
			if ll_ret < 0 then
				g_mb.error(ls_errore)
				return
				
			elseif ll_ret=0 then
				//non bloccante
				g_mb.show(ls_errore)
			end if
			
			this.postevent( "pc_esporta_xls_viv" )
			
		case 2		//tracciato file ANODALL-EXTRUSION
			//preparo la var. istanza del cod_cliente_fornitore
			if wf_cod_cliente_fornitore(ls_errore)<0 then
				g_mb.error(ls_errore)
				return
			end if
			this.postevent( "pc_esporta_txt_anodall" )
		
		case 3		//tracciato file FONDERIA BUSTREO
			//preparo la var. istanza del CodSedeMago
			if wf_cod_cliente_fornitore(ls_errore)<0 then
				g_mb.error(ls_errore)
				return
			end if
			this.postevent( "pc_esporta_csv_bustreo" )
			
		
		case else		//tracciato standard
			this.postevent( "pc_esporta_xls_std" )
			
	end choose
	
else
	//tracciato standard
	this.postevent( "pc_esporta_xls_std" )
end if


end event

event pc_esporta_xls_std();//esportazione standard

long					ll_anno_reg, ll_num_reg
string					ls_errore, ls_path
boolean				lb_esiste = false

ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

if not wf_conferma_esportazione(ll_anno_reg, ll_num_reg, is_cod_nota_excel, lb_esiste, ls_errore) then
	g_mb.error(ls_errore)
	return
end if

if wf_esporta_standard( ll_anno_reg, ll_num_reg, ls_path, ls_errore) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if
end if

//se arrivi fin qui procedi al salvataggio del blob in tabella note: in ls_path_file ho il file salvato
if wf_salva_file( ls_path, ll_anno_reg, ll_num_reg, lb_esiste, is_cod_nota_excel, is_estensione_excel, ls_errore) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if
end if
	

return
end event

event pc_esporta_xls_viv();//solo per gruppo gibus
long					ll_anno_reg, ll_num_reg
string					ls_errore, ls_path_file
boolean				lb_esiste = false
	

ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber( tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg =  tab_dettaglio.det_1.dw_1.getitemnumber( tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

if not wf_conferma_esportazione(ll_anno_reg, ll_num_reg, is_cod_nota_excel, lb_esiste, ls_errore) then
	g_mb.error(ls_errore)
	return
end if

if wf_esporta_viv( ll_anno_reg, ll_num_reg, ls_path_file, ls_errore ) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if

end if
//----------------------------------------------------------------------------------------------------------------------

//se arrivi fin qui procedi al salvataggio del blob in tabella note: in ls_path_file ho il file salvato
if wf_salva_file( ls_path_file, ll_anno_reg, ll_num_reg, lb_esiste, is_cod_nota_excel, is_estensione_excel, ls_errore) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if
end if
	

return
end event

event pc_esporta_txt_anodall();//solo per gruppo gibus
long					ll_anno_reg, ll_num_reg
string					ls_errore, ls_path_file
boolean				lb_esiste = false
	

ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

if not wf_conferma_esportazione(ll_anno_reg, ll_num_reg, is_cod_nota_txt, lb_esiste, ls_errore) then
	g_mb.error(ls_errore)
	return
end if

if wf_esporta_anodall( ll_anno_reg, ll_num_reg, ls_path_file, ls_errore ) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if

end if
//----------------------------------------------------------------------------------------------------------------------

//se arrivi fin qui procedi al salvataggio del blob in tabella note: in ls_path_file ho il file salvato
if wf_salva_file( ls_path_file, ll_anno_reg, ll_num_reg, lb_esiste, is_cod_nota_txt, "txt", ls_errore) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if
end if
	

return
end event

event pc_esporta_csv_bustreo();//solo per gruppo gibus
long					ll_anno_reg, ll_num_reg
string					ls_errore, ls_path_file
boolean				lb_esiste = false
	

ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

if not wf_conferma_esportazione(ll_anno_reg, ll_num_reg, is_cod_nota_txt, lb_esiste, ls_errore) then
	g_mb.error(ls_errore)
	return
end if

if wf_esporta_bustreo( ll_anno_reg, ll_num_reg, ls_path_file, ls_errore ) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if

end if
//----------------------------------------------------------------------------------------------------------------------

//se arrivi fin qui procedi al salvataggio del blob in tabella note: in ls_path_file ho il file salvato
if wf_salva_file( ls_path_file, ll_anno_reg, ll_num_reg, lb_esiste, is_cod_nota_txt, "csv", ls_errore) < 0 then
	//errore
	rollback;
	g_mb.error( ls_errore )
	return
else
	//Tutto a posto
	commit;
	
	if ls_errore<>"" and not isnull(ls_errore) then
		//messaggio di conferma, non di errore
		g_mb.show( ls_errore )
	end if
end if
	

return
end event

event pc_menu_stampa_residuo();wf_stampa(true)
end event

event pc_menu_carico_acquisti();long			ll_row, ll_numero
integer		li_anno
string			ls_cod_fornitore

ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
	li_anno = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_numero = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")
	
	if li_anno>0 and ll_numero>0 then
		
		if ls_cod_fornitore="" then setnull(ls_cod_fornitore)
		
		s_cs_xx.parametri.parametro_d_1_a[1] = 6969
		s_cs_xx.parametri.parametro_d_1_a[2] = li_anno
		s_cs_xx.parametri.parametro_d_1_a[3] = ll_numero
		s_cs_xx.parametri.parametro_s_1 = ls_cod_fornitore
		
		window_open(w_carico_acquisti, -1)
		
	end if
end if

return
end event

event pc_menu_doc_fiscali();integer			li_anno_ordine
long				ll_num_ordine

li_anno_ordine = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
ll_num_ordine = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")

if li_anno_ordine>0 and ll_num_ordine>0 then
else
	return
end if

window_open_parm(w_doc_fiscali_ordine_acquisto, -1, tab_dettaglio.det_1.dw_1)

return
end event

public subroutine wf_imposta_ricerca ();long ll_anno_registrazione, ll_num_registrazione

is_sql_prodotto = ""
is_sql_filtro = ""
ll_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_registrazione")

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 1900 then
	is_sql_filtro += " AND tes_ord_acq.anno_registrazione=" + string(ll_anno_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	is_sql_filtro += " AND tes_ord_acq.num_registrazione=" + string(ll_num_registrazione)
end if
if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 and not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	// mi fermo non serve altro, filtro solo la singolo ordine
	return
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_consegna")) and year(date(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_consegna")))>1950 then
	is_sql_filtro += " AND tes_ord_acq.data_consegna='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_consegna"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione")) and year(date(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione")))>1950 then
	is_sql_filtro += " AND tes_ord_acq.data_registrazione='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_ord_acq")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_ord_acq")<>"" then
	is_sql_filtro += " AND tes_ord_acq.cod_tipo_ord_acq='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_ord_acq") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operatore")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operatore")<>"" then
	is_sql_filtro += " AND tes_ord_acq.cod_operatore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operatore") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")<>"" then
	is_sql_filtro += " AND tes_ord_acq.cod_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")<>"" then
	is_sql_filtro += " AND det_ord_acq.cod_prodotto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto") +"'"
end if
if (not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")<>"" ) OR wf_has_livello("X") then
	is_sql_prodotto = " JOIN det_ord_acq on det_ord_acq.cod_azienda=tes_ord_acq.cod_azienda and "+&
							"det_ord_acq.anno_registrazione=tes_ord_acq.anno_registrazione and "+&
							"det_ord_acq.num_registrazione=tes_ord_acq.num_registrazione "
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")<>"" then
	is_sql_filtro += " AND tes_ord_acq.cod_deposito='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito_trasf")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito_trasf")<>"" then
	is_sql_filtro += " AND tes_ord_acq.cod_deposito_trasf='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito_trasf") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_evasione")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_evasione")<>"" then
	
	if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_evasione")="AP" then
		is_sql_filtro += " AND ("+&
										"tes_ord_acq.flag_evasione='A' or "+&
										"tes_ord_acq.flag_evasione='P'"+&
									")"
	else
		is_sql_filtro += " AND tes_ord_acq.flag_evasione='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_evasione") +"'"
	end if
	
end if

end subroutine

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "O"
		return wf_inserisci_operatori(al_handle)
		
	case "T"
		return wf_inserisci_tipo_ordine(al_handle)
		
	case "A"
		return wf_inserisci_anno(al_handle)
		
	case "R"
		return wf_inserisci_data_registrazione(al_handle)
		
	case "C"
		return wf_inserisci_data_consegna(al_handle)
		
	case "D"
		return wf_inserisci_depositi(al_handle)
		
	case "P"
		return wf_inserisci_depositi_trasf(al_handle)
		
	case "F"
		return wf_inserisci_fornitori(al_handle)
		
	case "X"
		return wf_inserisci_prodotti(al_handle)
				
	case else
		return wf_inserisci_ordini_acquisti(al_handle)
		
end choose
end function

public function long wf_inserisci_ordini_acquisti (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT tes_ord_acq.anno_registrazione, tes_ord_acq.num_registrazione FROM tes_ord_acq " + is_sql_prodotto + " WHERE tes_ord_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

//imposto l'ordinamento per anno e numero
ls_sql += " ORDER BY tes_ord_acq.anno_registrazione, tes_ord_acq.num_registrazione"

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc, #2 asc")
ids_store.sort()

// stefanop 02/07/2012: aggiungo controllo per il numero massimo di risultati
if ll_rows > il_max_row  then
	if not g_mb.confirm("Sono stati recuperati " + string(ll_rows) + " ordini superando il limite consigliato.~r~nProcedere con la visualizzazione di tutti gli ordini?") then
		// Visualizzo solo le richieste nel limite
		ll_rows = il_max_row
	end if
end if

for ll_i = 1 to ll_rows
	
	wf_inserisci_ordine_acquisto(al_handle, ids_store.getitemnumber(ll_i, 1), ids_store.getitemnumber(ll_i, 2))

next

return ll_rows
end function

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Operatore", "O")
wf_add_valore_livello("Tipo Ordine", "T")
wf_add_valore_livello("Anno Registrazione", "A")
wf_add_valore_livello("Data Registrazione", "R")
wf_add_valore_livello("Data Consegna", "C")
wf_add_valore_livello("Deposito", "D")
wf_add_valore_livello("Deposito Transf.", "P")
wf_add_valore_livello("Fornitore", "F")
wf_add_valore_livello("Prodotto", "X")
end subroutine

public subroutine wf_treeview_icons ();ICONA_ORIDINE = wf_treeview_add_icon("treeview\tipo_documento.png")
ICONA_OPERATORE = wf_treeview_add_icon("treeview\operatore.png")
ICONA_TIPO_ORDINE = wf_treeview_add_icon("treeview\documento_blu.png")
ICONA_ANNO = wf_treeview_add_icon("treeview\anno.png")
ICONA_DATA_REGISTRAZIONE = wf_treeview_add_icon("treeview\data_1.png")
ICONA_DATA_CONSEGNA = wf_treeview_add_icon("treeview\data_2.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_DEPOSITO_TRANF = wf_treeview_add_icon("treeview\deposito.png")
ICONA_FORNITORE = wf_treeview_add_icon("treeview\cliente.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")
end subroutine

public function long wf_inserisci_operatori (long al_handle);string ls_sql, ls_label, ls_cod_operatore, ls_des_operatore, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_operatori.cod_operatore, tab_operatori.des_operatore FROM tes_ord_acq &
LEFT OUTER JOIN tab_operatori ON tab_operatori.cod_azienda = tes_ord_acq.cod_azienda AND &
tab_operatori.cod_operatore = tes_ord_acq.cod_operatore " + is_sql_prodotto + " &
WHERE tes_ord_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_operatore = ids_store.getitemstring(ll_i, 1)
	 ls_des_operatore = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "O"
	lstr_data.codice = ls_cod_operatore
	
	if isnull(ls_cod_operatore) and isnull(ls_des_operatore) then
		ls_label = "<Operatore mancante>"
	elseif isnull(ls_des_operatore) then
		ls_label = ls_cod_operatore
	else
		ls_label = ls_cod_operatore + " - " + ls_des_operatore
	end if
	
	ltvi_item = wf_new_item(true, ICONA_OPERATORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_tipo_ordine (long al_handle);string ls_sql, ls_label, ls_cod_tipo_ord_acq, ls_des_tipo_ord_acq, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_tipi_ord_acq.cod_tipo_ord_acq, tab_tipi_ord_acq.des_tipo_ord_acq FROM tes_ord_acq &
LEFT OUTER JOIN tab_tipi_ord_acq ON tab_tipi_ord_acq.cod_azienda = tes_ord_acq.cod_azienda AND &
tab_tipi_ord_acq.cod_tipo_ord_acq = tes_ord_acq.cod_tipo_ord_acq " + is_sql_prodotto + " &
WHERE tes_ord_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_tipo_ord_acq = ids_store.getitemstring(ll_i, 1)
	 ls_des_tipo_ord_acq = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ls_cod_tipo_ord_acq
	
	if isnull(ls_cod_tipo_ord_acq) and isnull(ls_des_tipo_ord_acq) then
		ls_label = "<Tipo Ordine mancante>"
	elseif isnull(ls_des_tipo_ord_acq) then
		ls_label = ls_cod_tipo_ord_acq
	else
		ls_label = ls_cod_tipo_ord_acq + " - " + ls_des_tipo_ord_acq
	end if
	
	ltvi_item = wf_new_item(true, ICONA_TIPO_ORDINE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_anno (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anno_registrazione FROM tes_ord_acq " + is_sql_prodotto + " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "A"
	lstr_data.codice = string(ids_store.getitemnumber(ll_i, 1))
	
	ltvi_item = wf_new_item(true, ICONA_ANNO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = string(ids_store.getitemnumber(ll_i, 1))
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_data_registrazione (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT data_registrazione FROM tes_ord_acq " + is_sql_prodotto + " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "R"
	lstr_data.codice = string(ids_store.getitemdatetime(ll_i, 1), s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA_REGISTRAZIONE)
	
	ltvi_item.data = lstr_data
	if isnull(ids_store.getitemdatetime(ll_i, 1)) then
		ltvi_item.label = "<Data registrazione mancante>"
	else
		ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_data_consegna (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT  DISTINCT data_consegna FROM tes_ord_acq " + is_sql_prodotto + " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql = ls_sql + " order by data_consegna desc "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "C"
	lstr_data.codice = string(ids_store.getitemdatetime(ll_i, 1), s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA_CONSEGNA)
	
	ltvi_item.data = lstr_data
	if isnull(ids_store.getitemdatetime(ll_i, 1)) then
		ltvi_item.label = "<Data consegna mancante>"
	else
		ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_depositi (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_depositi.cod_deposito, anag_depositi.des_deposito FROM tes_ord_acq &
LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = tes_ord_acq.cod_azienda AND &
anag_depositi.cod_deposito = tes_ord_acq.cod_deposito " + is_sql_prodotto + " &
WHERE tes_ord_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' AND anag_depositi.flag_tipo_deposito <> 'T' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_depositi_trasf (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_depositi.cod_deposito, anag_depositi.des_deposito FROM tes_ord_acq &
LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = tes_ord_acq.cod_azienda AND &
anag_depositi.cod_deposito = tes_ord_acq.cod_deposito " + is_sql_prodotto + " &
WHERE tes_ord_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' AND anag_depositi.flag_tipo_deposito='T' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "P"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito Transf. mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO_TRANF)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_fornitori (long al_handle);string ls_sql, ls_label, ls_cod_fornitore, ls_des_fornitore, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_fornitori.cod_fornitore, anag_fornitori.rag_soc_1 FROM tes_ord_acq &
LEFT OUTER JOIN anag_fornitori ON anag_fornitori.cod_azienda = tes_ord_acq.cod_azienda AND &
anag_fornitori.cod_fornitore = tes_ord_acq.cod_fornitore " + is_sql_prodotto + " &
WHERE tes_ord_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_fornitore = ids_store.getitemstring(ll_i, 1)
	 ls_des_fornitore = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "F"
	lstr_data.codice = ls_cod_fornitore
	
	if isnull(ls_cod_fornitore) and isnull(ls_des_fornitore) then
		ls_label = "<Fornitore mancante>"
	elseif isnull(ls_des_fornitore) then
		ls_label = ls_cod_fornitore
	else
		ls_label = ls_cod_fornitore + " - " + ls_des_fornitore
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO_TRANF)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "O"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.cod_operatore is null "
			else
				as_sql += " AND tes_ord_acq.cod_operatore = '" + lstr_data.codice + "' "
			end if
			
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.cod_tipo_ord_acq is null "
			else
				as_sql += " AND tes_ord_acq.cod_tipo_ord_acq = '" + lstr_data.codice + "' "
			end if
			
		case "A"
			as_sql += " AND tes_ord_acq.anno_registrazione = " + lstr_data.codice
			
		case "R"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.data_registrazione is null "
			else
				as_sql += " AND tes_ord_acq.data_registrazione = '" + lstr_data.codice + "' "
			end if
			
		case "C"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.data_consegna is null "
			else
				as_sql += " AND tes_ord_acq.data_consegna = '" + lstr_data.codice + "' "
			end if
			
		case "D"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.cod_deposito is null "
			else
				as_sql += " AND tes_ord_acq.cod_deposito = '" + lstr_data.codice + "' "
			end if
			
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.cod_deposito_trasf is null "
			else
				as_sql += " AND tes_ord_acq.cod_deposito_trasf = '" + lstr_data.codice + "' "
			end if

		case "F"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.cod_fornitore is null "
			else
				as_sql += " AND tes_ord_acq.cod_fornitore = '" + lstr_data.codice + "' "
			end if
			
		case "X"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND det_ord_acq.cod_prodotto is null "
			else
				as_sql += " AND det_ord_acq.cod_prodotto = '" + lstr_data.codice + "' "
			end if
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function long wf_inserisci_prodotti (long al_handle);string ls_sql, ls_label, ls_error, ls_cod_prodotto, ls_des_prodotto
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto FROM tes_ord_acq " + is_sql_prodotto + " &
LEFT OUTER JOIN anag_prodotti ON anag_prodotti.cod_azienda = tes_ord_acq.cod_azienda AND &
anag_prodotti.cod_prodotto = det_ord_acq.cod_prodotto &
WHERE tes_ord_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_prodotto = ids_store.getitemstring(ll_i, 1)
	 ls_des_prodotto = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "X"
	lstr_data.codice = ls_cod_prodotto
	
	if isnull(ls_cod_prodotto) and isnull(ls_des_prodotto) then
		ls_label = "<Prodotto mancante>"
	elseif isnull(ls_des_prodotto) then
		ls_label = ls_cod_prodotto
	else
		ls_label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if

	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_duplica_righe (long fl_anno, long fl_numero, long fl_anno_new, long fl_num_new);long ll_null

//variabili di colonna
long			ll_prog_riga_ordine_acq, ll_anno_off_acq, ll_num_off_acq, ll_prog_riga_off_acq, ll_num_confezioni, ll_num_pezzi_confezione,&
				ll_anno_reg_rda, ll_num_reg_rda, ll_prog_riga_rda

string			ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_cod_prod_fornitore,&
				ls_flag_saldo, ls_flag_blocco, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_doc_suc_det,ls_flag_st_note_det,&
				ls_flag_agg_costo_ultimo, ls_des_specifica, ls_cod_disegno, ls_rev_disegno

decimal		ld_quan_ordinata,ld_prezzo_acquisto, ld_fat_conversione, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
				ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10,&
				ld_quan_arrivata, ld_val_riga, ld_imponibile_iva, ld_imponibile_iva_valuta
				
datetime 	ldt_data_consegna, ldt_data_consegna_fornitore

setnull(ll_null)

//cursore
declare cu_righe cursor for
	select		prog_riga_ordine_acq,   
				cod_tipo_det_acq,   
				cod_prodotto,   
				cod_misura,   
				des_prodotto,   
				quan_ordinata,   
				prezzo_acquisto,   
				fat_conversione,   
				sconto_1,   
				sconto_2,   
				sconto_3,   
				cod_iva,   
				data_consegna,   
				cod_prod_fornitore,   
				quan_arrivata,   
				val_riga,   
				flag_saldo,   
				flag_blocco,   
				nota_dettaglio,   
				cod_centro_costo,   
				sconto_4,   
				sconto_5,   
				sconto_6,   
				sconto_7,   
				sconto_8,   
				sconto_9,   
				sconto_10,   
				anno_off_acq,   
				num_off_acq,   
				prog_riga_off_acq,   
				data_consegna_fornitore,   
				num_confezioni,   
				num_pezzi_confezione,   
				flag_doc_suc_det,   
				flag_st_note_det,   
				imponibile_iva,   
				imponibile_iva_valuta,   
				anno_reg_rda,   
				num_reg_rda,   
				prog_riga_rda,   
				flag_agg_costo_ultimo,   
				des_specifica
	from det_ord_acq
	where	cod_azienda = :s_cs_xx.cod_azienda and
		 		anno_registrazione = :fl_anno and num_registrazione=:fl_numero;
													
open cu_righe;

if sqlca.sqlcode <> 0 then
	g_mb.error("APICE", "Errore in costruzione elenco righe dettaglio (Open cursore dettagli): " + sqlca.sqlerrtext)
	rollback;
	return -1
end if
	
do while true
	fetch cu_righe
	into 	:ll_prog_riga_ordine_acq,   
			:ls_cod_tipo_det_acq,   
			:ls_cod_prodotto,   
			:ls_cod_misura,   
			:ls_des_prodotto,   
			:ld_quan_ordinata,   
			:ld_prezzo_acquisto,   
			:ld_fat_conversione,   
			:ld_sconto_1,   
			:ld_sconto_2,   
			:ld_sconto_3,   
			:ls_cod_iva,   
			:ldt_data_consegna,   
			:ls_cod_prod_fornitore,   
			:ld_quan_arrivata,   
			:ld_val_riga,   
			:ls_flag_saldo,   
			:ls_flag_blocco,   
			:ls_nota_dettaglio,   
			:ls_cod_centro_costo,   
			:ld_sconto_4,   
			:ld_sconto_5,   
			:ld_sconto_6,   
			:ld_sconto_7,   
			:ld_sconto_8,   
			:ld_sconto_9,   
			:ld_sconto_10,   
			:ll_anno_off_acq,   
			:ll_num_off_acq,   
			:ll_prog_riga_off_acq,   
			:ldt_data_consegna_fornitore,   
			:ll_num_confezioni,   
			:ll_num_pezzi_confezione,   
			:ls_flag_doc_suc_det,   
			:ls_flag_st_note_det,   
			:ld_imponibile_iva,   
			:ld_imponibile_iva_valuta,   
			:ll_anno_reg_rda,   
			:ll_num_reg_rda,   
			:ll_prog_riga_rda,   
			:ls_flag_agg_costo_ultimo,   
			:ls_des_specifica;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("APICE","Errore in costruzione elenco righr dettaglio (Fetch del cursore rughe): " + sqlca.sqlerrtext)
		close cu_righe;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		close cu_righe;
		exit
	end if
	
	// stefanop 13/07/2012: azzero le date durante la duplicazione; beatrice
	// -----------------------------------------------
	setnull(ldt_data_consegna)
	setnull(ldt_data_consegna_fornitore)
	// -----------------------------------------------

	//per cod disegno e rev disegno prendo quelli che ho al momento in anagrafica prodotto
	ls_cod_disegno = ""
	ls_rev_disegno = ""

	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		select cod_disegno, rev_disegno
		into :ls_cod_disegno, :ls_rev_disegno
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_cod_prodotto;

		if isnull(ls_cod_disegno) then ls_cod_disegno=""
		if isnull(ls_rev_disegno) then ls_rev_disegno=""

	end if

	insert into det_ord_acq
			(cod_azienda,   
			anno_registrazione,   
			num_registrazione,   
			prog_riga_ordine_acq,   
			cod_tipo_det_acq,   
			cod_prodotto,   
			cod_misura,   
			des_prodotto,   
			quan_ordinata,   
			prezzo_acquisto,   
			fat_conversione,   
			sconto_1,   
			sconto_2,   
			sconto_3,   
			cod_iva,   
			data_consegna,   
			cod_prod_fornitore,   
			quan_arrivata,   
			val_riga,   
			flag_saldo,   
			flag_blocco,   
			nota_dettaglio,   
			anno_commessa,
			num_commessa,
			cod_centro_costo,   
			sconto_4,   
			sconto_5,   
			sconto_6,   
			sconto_7,   
			sconto_8,   
			sconto_9,   
			sconto_10,   
			anno_off_acq,   
			num_off_acq,   
			prog_riga_off_acq,   
			data_consegna_fornitore,   
			num_confezioni,   
			num_pezzi_confezione,   
			flag_doc_suc_det,   
			flag_st_note_det,   
			imponibile_iva,   
			imponibile_iva_valuta,   
			anno_reg_rda,   
			num_reg_rda,   
			prog_riga_rda,   
			flag_agg_costo_ultimo,   
			des_specifica,   
			anno_reg_bol_ven,
			num_reg_bol_ven,
			prog_riga_bol_ven,
			cod_disegno,
			rev_disegno)
		values	(	:s_cs_xx.cod_azienda,   
						:fl_anno_new,   
						:fl_num_new,   
						:ll_prog_riga_ordine_acq,   
						:ls_cod_tipo_det_acq,   
						:ls_cod_prodotto,   
						:ls_cod_misura,   
						:ls_des_prodotto,   
						:ld_quan_ordinata,   
						:ld_prezzo_acquisto,   
						:ld_fat_conversione,   
						:ld_sconto_1,   
						:ld_sconto_2,   
						:ld_sconto_3,   
						:ls_cod_iva,   
						:ldt_data_consegna,   
						:ls_cod_prod_fornitore,   
						0, //:ld_quan_arrivata,   
						:ld_val_riga,   
						'N', //:ls_flag_saldo,   
						:ls_flag_blocco,   
						:ls_nota_dettaglio,   
						:ll_null,   
						:ll_null,   
						:ls_cod_centro_costo,   
						:ld_sconto_4,   
						:ld_sconto_5,   
						:ld_sconto_6,   
						:ld_sconto_7,   
						:ld_sconto_8,   
						:ld_sconto_9,   
						:ld_sconto_10,   
						:ll_null, //:ll_anno_off_acq,   
						:ll_null, //:ll_num_off_acq,   
						:ll_null, //:ll_prog_riga_off_acq,   
						:ldt_data_consegna_fornitore,   
						:ll_num_confezioni,   
						:ll_num_pezzi_confezione,   
						:ls_flag_doc_suc_det,   
						:ls_flag_st_note_det,   
						:ld_imponibile_iva,   
						:ld_imponibile_iva_valuta,   
						:ll_null, //:ll_anno_reg_rda,   
						:ll_null, //:ll_num_reg_rda,   
						:ll_null, //:ll_prog_riga_rda,   
						:ls_flag_agg_costo_ultimo,   
						:ls_des_specifica,   
						:ll_null,   
						:ll_null,   
						:ll_null,
						:ls_cod_disegno,
						:ls_rev_disegno);

	if sqlca.sqlcode<0 then
		g_mb.error("APICE", "Errore in duplicazione dettagli: "+sqlca.sqlerrtext)
		close cu_righe;
		rollback;
		return -1
	end if

loop

close cu_righe;

return 1
end function

public function boolean wf_user_can (long al_anno, long al_numero, string as_cod_tipo_ord_acq);/**
 * Stefanop
 * 04/08/2010
 *
 * Controllo che il mansionario collegato possa inviare, respingere o firmare l'ordine di acquisto
 * all'auttale fase
 **/
 
string ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_destinatario_mail, ls_cod_destinatario
long ll_num_sequenza_corrente, ll_num_sequenza

select cod_tipo_lista_dist, cod_lista_dist
into :ls_cod_tipo_lista_dist, :ls_cod_lista_dist
from tab_tipi_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_ord_acq = :as_cod_tipo_ord_acq;
	
if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_lista_dist) or isnull(ls_cod_lista_dist) then return false

// controllo numero sequenza corrente
select num_sequenza_corrente
into :ll_num_sequenza_corrente
from tes_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno and
	num_registrazione = :al_numero;
	
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0
// ----

// !!! Parametro ignoto !!!
select stringa
into  :ls_cod_destinatario
from parametri_omnia
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'PCU';
		 
if sqlca.sqlcode <> 0 then
	g_mb.error( "OMNIA", "Errore durante la ricerca del parametro PCU: " + sqlca.sqlerrtext)
	return false
end if
// ----

// recupero sequenza successiva
select num_sequenza
into :ll_num_sequenza
from det_liste_dist
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
	cod_lista_dist = :ls_cod_lista_dist and
	num_sequenza_prec = :ll_num_sequenza_corrente and
	cod_destinatario = :ls_cod_destinatario;

if sqlca.sqlcode <> 0 and isnull(ll_num_sequenza) then
	g_mb.error("OMNIA", "Errore durante il controllo della sequenza del ciclo di vita")
	return false
end if
// ----

// recuper cod_destinatario associato all'utente
select cod_destinatario
into :ls_cod_destinatario_mail
from tab_ind_dest
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode < 0 then
	g_mb.error("OMNIA", "Errore durante il recupero del destinatario associato all'utente")
	return false
elseif sqlca.sqlcode = 100 then
	g_mb.error("OMNIA", "Attenzione: nessun destinatario associato all'utente collegato")
	return false
end if
// ----

// controllo se l'utente è presente nella fase
select cod_destinatario_mail
into :ls_cod_destinatario_mail
from det_liste_dist_destinatari
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
	cod_lista_dist = :ls_cod_lista_dist and
	num_sequenza = :ll_num_sequenza and
	cod_destinatario_mail = :ls_cod_destinatario_mail and
	cod_destinatario = :ls_cod_destinatario;

if sqlca.sqlcode < 0  then
	g_mb.error("OMNIA", "Errore durante il controllo del mansionario all'interno della ciclo di vita")
	return false
elseif sqlca.sqlcode = 100 then
	g_mb.error("OMNIA", "Attenzione: l'utente non è abilitato ad eseguire l'operazione all'attuale fase di approvazione")
	return false
end if
// ----

return true

	
end function

public function integer wf_firma_documento ();/**
 * Stefanop 
 * 04/08/2010
 * 
 * La funzione firma il documento se si è all'ultima fase
 **/
 
string   ls_firma_chiusura, ls_cod_tipo_ord, ls_cod_tipo_lista_dist, ls_cod_lista_dist
long     ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente, ll_sequenza_destinatari
datetime ldt_chiusura

ll_riga = tab_dettaglio.det_1.dw_1.getrow()
if ll_riga < 0 then return -1

//questi dati li prendiamo direttamente dalla tabella
ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_registrazione")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_registrazione")
ls_cod_tipo_ord = tab_dettaglio.det_1.dw_1.getitemstring(ll_riga, "cod_tipo_ord_acq")

select num_sequenza_corrente,
	firma_chiusura,
	data_chiusura
into
	:ll_num_sequenza_corrente,
	:ls_firma_chiusura,
	:ldt_chiusura
from tes_ord_acq
where 
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno and
	num_registrazione = :ll_num;
	
if not isnull(ls_firma_chiusura) and not isnull(ldt_chiusura) then	
	g_mb.messagebox("OMNIA","Questa non conformità è già stata chiusa in data " + string(ldt_chiusura) + " dal mansionario " + ls_firma_chiusura)
	return -1
end if

select cod_tipo_lista_dist, cod_lista_dist
into :ls_cod_tipo_lista_dist, :ls_cod_lista_dist
from tab_tipi_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_ord_acq = :ls_cod_tipo_ord;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("OMNIA", "Errore durante il controllo della lista di distribuzione")
	return -1
end if

select num_sequenza
into :ll_sequenza_destinatari
from det_liste_dist
where  
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
	cod_lista_dist = :ls_cod_lista_dist and
	num_sequenza_prec = :ll_num_sequenza_corrente;

ll_num_sequenza_corrente = ll_sequenza_destinatari
setnull(ll_sequenza_destinatari)

setnull(ls_firma_chiusura)

select cod_resp_divisione
into   :ls_firma_chiusura
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_chiudi_ord_acq = 'S';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_firma_chiusura) then
	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato alla chiusura dell'ordine di acquisto")
	return -1
end if

ldt_chiusura = datetime(today(),now())

update tes_ord_acq
set
	firma_chiusura = :ls_firma_chiusura,
	data_chiusura = :ldt_chiusura,
	flag_confermato = 'S'
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno and
	num_registrazione = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in chiusura non_conformità.~nErrore nella update di non_conformita: " + sqlca.sqlerrtext)
	rollback;
	return -1
else
	g_mb.show("OMNIA", "Ordine di acquisto firmato con successo")
	commit;
end if

tab_dettaglio.det_1.dw_1.triggerevent("pcd_retrieve")
return 0
end function

public function long wf_inserisci_ordine_acquisto (long al_handle, long al_anno_registrazione, long al_num_registrazione);string ls_sql, ls_error, ls_cod_fornitore, ls_rag_soc_1
dec{4} ld_tot_val_ordine
treeviewitem ltvi_item

str_treeview lstr_data
	
lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = al_anno_registrazione
lstr_data.decimale[2] = al_num_registrazione

ltvi_item = wf_new_item(false, ICONA_ORIDINE)

//mettiamo sul nodo le info principali -----------------------------
select 	tot_val_ordine,
			cod_fornitore
into   		:ld_tot_val_ordine,
			:ls_cod_fornitore
from   tes_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;

if isnull(ld_tot_val_ordine) then ld_tot_val_ordine=0.00

ltvi_item.label = string(al_anno_registrazione) + "/"+ string(al_num_registrazione)

ltvi_item.label += " Val. "+string(ld_tot_val_ordine, "#,###,###,##0.00") 

if ls_cod_fornitore<>"" and not isnull(ls_cod_fornitore) then

	select rag_soc_1
	into :ls_rag_soc_1
	from anag_fornitori
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_fornitore=:ls_cod_fornitore;
			
	ltvi_item.label += " - " + ls_rag_soc_1 + " (" + ls_cod_fornitore + ")"

end if
//---------------------------------------------------------------------

ltvi_item.data = lstr_data
//ltvi_item.label = string(long(lstr_data.decimale[1])) + "/" + string(long(lstr_data.decimale[2]))

tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)

return 1
end function

public function integer wf_duplica (long fl_anno, long fl_numero, ref long fl_anno_new, ref long fl_numero_new);string ls_cod_deposito, ls_errore, ls_cod_operatore, ls_cod_fornitore, ls_messaggio
long ll_anno_new, ll_num_new, ll_max_registrazione, ll_null, ll_errore, ll_ret
datetime ldt_today
uo_fido_cliente luo_fido_cliente


ldt_today = datetime(today(), 00:00:00)

if fl_anno>0 and fl_numero>0 then
else
	return 0
end if

select cod_fornitore
into	:ls_cod_fornitore
from	tes_ord_acq
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno and
			num_Registrazione = :fl_numero;

if not g_mb.confirm("APICE", "Duplicare l'ordine N."+string(fl_anno)+"/"+string(fl_numero), 1) then return 0

// verifico fornitore bloccato
luo_fido_cliente = create uo_fido_cliente
ll_ret = luo_fido_cliente.uof_get_blocco_fornitore(ls_cod_fornitore, ldt_today, is_cod_parametro_blocco, ls_messaggio)
choose case ll_ret
	case 1
		//solo warning
		g_mb.warning(ls_messaggio)
	case 2
		//blocco
		g_mb.error(ls_messaggio)
		destroy luo_fido_cliente
		return 0
end choose
destroy luo_fido_cliente


//inserisci testata
ll_anno_new = f_anno_esercizio()

// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
ls_cod_operatore = guo_functions.uof_get_operatore_utente()
// ----

if ll_anno_new > 0 then
else
	g_mb.show("Ordini Fornitori","Impostare l'anno di esercizio in parametri aziendali")
	return 0
end if

select num_registrazione
into :ll_num_new
from con_ord_acq
where con_ord_acq.cod_azienda = :s_cs_xx.cod_azienda;

choose case sqlca.sqlcode
	case 0
		ll_num_new ++
	case 100
		g_mb.show("Ordini acquisto","Errore in assegnazione numero ordine: verificare di aver impostato i parametri offerte di vendita. ~r~nIl salvataggio verrà effettuato ugualmente")
	case else
		g_mb.show("Ordini acquisto","Errore in assegnazione numero ordine.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verrà effettuato ugualmente")
end choose
		
ll_max_registrazione = 0
select max(num_registrazione)
into :ll_max_registrazione
from tes_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_new;
		 
if not isnull(ll_max_registrazione) then
	if ll_max_registrazione >= ll_num_new then ll_num_new = ll_max_registrazione + 1
end if
			
update con_ord_acq
set con_ord_acq.num_registrazione = :ll_num_new
where con_ord_acq.cod_azienda = :s_cs_xx.cod_azienda;

insert into tes_ord_acq (
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	cod_tipo_ord_acq,   
	data_registrazione,   
	cod_operatore,   
	cod_fornitore,   
	rag_soc_1,   
	rag_soc_2,   
	indirizzo,   
	localita,   
	frazione,   
	cap,   
	provincia,   
	cod_fil_fornitore,   
	cod_des_fornitore,   
	cod_deposito,   
	cod_valuta,   
	cambio_acq,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	sconto,   
	cod_banca_clien_for,   
	num_ord_fornitore,   
	data_ord_fornitore,   
	cod_imballo,   
	cod_vettore,   
	cod_inoltro,   
	cod_mezzo,   
	cod_porto,   
	cod_resa,   
	flag_blocco,   
	flag_evasione,   
	tot_val_ordine,   
	tot_val_evaso,   
	nota_testata,   
	nota_piede,   
	data_consegna,   
	cod_banca,   
	data_consegna_fornitore,   
	flag_doc_suc_tes,   
	flag_doc_suc_pie,   
	flag_st_note_tes,   
	flag_st_note_pie,   
	imponibile_iva,   
	importo_iva,   
	imponibile_iva_valuta,   
	importo_iva_valuta,   
	tot_val_ordine_valuta,   
	tot_merci,   
	tot_spese_trasporto,   
	tot_spese_imballo,   
	tot_spese_bolli,   
	tot_spese_varie,   
	tot_spese_cassa,   
	tot_sconti_commerciali,   
	flag_confermato,   
	firma_chiusura,   
	data_chiusura,   
	num_sequenza_corrente,
	cod_deposito_trasf,
	flag_stampa_disegno
) select
		:s_cs_xx.cod_azienda,   
		:ll_anno_new,   
		:ll_num_new,   
		cod_tipo_ord_acq,   
		:ldt_today, //data_registrazione, -- imposto la data di duplicazione e non quella di quella copiata
		:ls_cod_operatore, //cod_operatore,   
		cod_fornitore,   
		rag_soc_1,   
		rag_soc_2,   
		indirizzo,   
		localita,   
		frazione,   
		cap,   
		provincia,   
		cod_fil_fornitore,   
		cod_des_fornitore,   
		:ls_cod_deposito, //cod_deposito,   
		cod_valuta,   
		cambio_acq,   
		cod_tipo_listino_prodotto,   
		cod_pagamento,   
		sconto,   
		cod_banca_clien_for,   
		num_ord_fornitore,   
		data_ord_fornitore,   
		cod_imballo,   
		cod_vettore,   
		cod_inoltro,   
		cod_mezzo,   
		cod_porto,   
		cod_resa,   
		'N',   
		'A',   
		tot_val_ordine,   
		tot_val_evaso,   
		nota_testata,   
		nota_piede,   
		NULL, //data_consegna,   
		cod_banca,   
		NULL, //data_consegna_fornitore,   
		flag_doc_suc_tes,   
		flag_doc_suc_pie,   
		flag_st_note_tes,   
		flag_st_note_pie,   
		imponibile_iva,   
		importo_iva,   
		imponibile_iva_valuta,   
		importo_iva_valuta,   
		tot_val_ordine_valuta,   
		tot_merci,   
		tot_spese_trasporto,   
		tot_spese_imballo,   
		tot_spese_bolli,   
		tot_spese_varie,   
		tot_spese_cassa,   
		tot_sconti_commerciali,   
		flag_confermato,   
		firma_chiusura,   
		data_chiusura,   
		num_sequenza_corrente,
		NULL,
		flag_stampa_disegno
	from tes_ord_acq
	where  
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno and
		num_registrazione = :fl_numero;
				 
if sqlca.sqlcode<0 then
	g_mb.error("APICE", "Errore in duplicazione testata: "+sqlca.sqlerrtext)
	rollback;
	return -1
end if

//inserisci dettagli
setnull(ll_null)

if wf_duplica_righe(fl_anno, fl_numero, ll_anno_new, ll_num_new) = 1 then
	
	commit;
	g_mb.show("APICE", "Ordine duplicato con successo! Generato Ordine di acquisto "+string(ll_anno_new)+"/"+string(ll_num_new))
	
else
	//errore già dato
	rollback;
	return -1
end if

fl_anno_new = ll_anno_new
fl_numero_new = ll_num_new

return 1
end function

public function integer wf_chiavi_prodotto (string fs_cod_prodotto, string fs_chiave, decimal fd_fattore, ref string fs_valore, ref string fs_errore);string ls_tipo, ls_des_campo_libero
long ll_progressivo
datetime ldt_campo_data
decimal ld_campo_numero

select flag_tipo_colonna
into :ls_tipo
from tab_colonne_dinamiche
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_colonna_dinamica=:fs_chiave;

if sqlca.sqlcode<0 then
	fs_errore = "Errore il lettura tipologia colonna dinamica "+fs_chiave+": " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or ls_tipo="" or isnull( ls_tipo ) then
	fs_valore = ""
	return 1
	
end if

//select progressivo, des_campo_libero, campo_data, campo_numero
//into :ll_progressivo, :ls_des_campo_libero, :ldt_campo_data, :ld_campo_numero
select id_valore_lista, valore_stringa, valore_data, valore_numero
into :ll_progressivo, :ls_des_campo_libero, :ldt_campo_data, :ld_campo_numero
from anag_prodotti_col_din
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto and
			cod_colonna_dinamica=:fs_chiave;
			
if sqlca.sqlcode<0 then
	fs_errore = "Errore il lettura campi colonna "+fs_chiave+" da anag_prodotti_col_din: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_valore = ""
	return 1
	
end if
	
choose case ls_tipo
	case "C"	//campo libero
		if isnull(ls_des_campo_libero) then ls_des_campo_libero=""
		fs_valore = ls_des_campo_libero
		
	case "L"	//lista
		select des_valore
		into :fs_valore
		from tab_colonne_dinamiche_valori
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_colonna_dinamica=:fs_chiave and
					progressivo=:ll_progressivo;
					
		if sqlca.sqlcode<0 then
			fs_errore = "Errore il lettura valore chiave lista "+fs_chiave+" (tab_chiavi_valori): " + sqlca.sqlerrtext
			return -1
			
		elseif sqlca.sqlcode=100 or isnull( fs_valore ) then
			fs_valore = ""
			return 1
			
		end if
		
		
	case "N"	//numerico decimale
		if isnull(ld_campo_numero) then ld_campo_numero = 0
		
		//in alcuni casi il valore numerico potrebbe essere in una unità di misura diversa per il fornitore
		//es. espresso in cm al posto di mm, quindi moltiplico ad. es. per un fattore 10 (altrimenti passo sempre fd_fattore=1 )
		ld_campo_numero = ld_campo_numero * fd_fattore
		
		//metto sempre un valore intero
		fs_valore = string(ld_campo_numero, "########0")
		
	case "D"	//datetime
		if not isnull(ldt_campo_data) and year(date(ldt_campo_data))>=1950 then
			fs_valore = string(ldt_campo_data, "dd/mm/yyyy")
		else
			fs_valore = ""
			return 1
		end if
		
	case "Q"	//non so che cazzo sia, esco subito con valore vuoto
		fs_valore = ""
		return 1
		
end choose

return 1
end function

public function integer wf_cod_cliente_fornitore (ref string fs_errore);//leggo il codice_cliente_fornitore da mettere nel tracciato del file e lo memorizzo nella var. istanza is_codice_cliente_fornitore
is_codice_cliente_fornitore = ""
is_CodSedeMago = ""

//per tracciato ANODALL/Extrusion --------------
select stringa
into :is_codice_cliente_fornitore
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='CCF' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'CCF' (codice cliente-fornitore per esportazione ordine): "+sqlca.sqlerrtext
	return -1
end if

if isnull(is_codice_cliente_fornitore) or is_codice_cliente_fornitore="" then
	fs_errore = "Manca il parametro aziendale 'CCF' (codice cliente-fornitore per esportazione ordine) "+sqlca.sqlerrtext
	return -1
end if


//per tracciato BUSTREO/PETTENUZZO --------------
select stringa
into :is_CodSedeMago
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='CSM' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'CSM' (CodSedeMAgo per esportazione ordine): "+sqlca.sqlerrtext
	return -1
end if

if isnull(is_CodSedeMago) or is_CodSedeMago="" then
//	fs_errore = "Manca il parametro aziendale 'CSM' (CodSedeMAgo per esportazione ordine) "+sqlca.sqlerrtext
//	return -1
	is_CodSedeMago = ""
end if



return 1
end function

public function boolean wf_conferma_esportazione (long fl_anno_reg, long fl_num_reg, string fs_cod_nota, ref boolean fb_esiste, ref string fs_errore);string ls_avviso
long ll_count


select count(*)
into :ll_count
from tes_ord_acq_note
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_reg and
			num_registrazione=:fl_num_reg and
			cod_nota=:fs_cod_nota;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo esportazione count(*)!"
	return false
end if

if isnull(ll_count) or ll_count=0 then
	ls_avviso = "Esportare il dettaglio dell'ordine?"
	fb_esiste = false
else
	ls_avviso = "L'ordine risulta già esportato. Vuoi riesportarlo sovrascrivendo l'allegato precedente"
	fb_esiste = true
end if

if not g_mb.confirm("APICE" , ls_avviso, 1 ) then
	fs_errore = "Operazione annullata dall'utente!"
	return false
end if

return true

end function

public function string wf_prepara_campo (string fs_valore, long fl_lunghezza);//torna un campo stringa lungo fl_lunghezza.
//se fs_valore è inferiore a fl_lunghezza la funzione ci mette gli spazi necessari

fs_valore = left(fs_valore + space(fl_lunghezza), fl_lunghezza)

return fs_valore
end function

public function string wf_prepara_campo_numerico (decimal fd_valore, long fl_lunghezza, long fl_decimali);//allineamento a destra (numeri) con riempimento con ZERI e senza separatore decimale
string ls_valore


fd_valore = round(fd_valore, fl_decimali)															//34.98  ->  34.980
ls_valore = string(fd_valore)																			//"34.980"

//tolgo eventuali separatori migliaia o decimali
guo_functions.uof_replace_string(ls_valore,".", "")
guo_functions.uof_replace_string(ls_valore,",", "")													//34980

ls_valore = right(Fill ("0", fl_lunghezza) + ls_valore, fl_lunghezza)								//00000034980

return ls_valore
end function

public function integer wf_estrai_articolo_fornitore (string fs_codice, ref string fs_articolo_fornitore, ref string fs_errore);//formato codice
//.......L=........
//prendo tutto quello che c'è prima di "L="  al più privato di spazi

integer ll_pos

ll_pos = pos(fs_codice, "L=")

if ll_pos>0 then
	fs_articolo_fornitore = left(fs_codice, ll_pos - 1)
	fs_articolo_fornitore = trim(fs_articolo_fornitore)
else
	fs_errore="Impossibile trovare 'L=' in codice articolo fornitore!"
	return -1
end if

return 1
end function

public function integer wf_leggi_chiave (string fs_parametro, ref string fs_cod_chiave, ref string fs_errore);string	ls_errore

//al momento fs_parametro= "CP1" e "CP2" per MISURA e HH_IMP
choose case fs_parametro
	case "CP1"
		ls_errore = "chiave prodotto per colonna MISURA"
	case "CP2"
		ls_errore = "chiave prodotto per colonna HH_IMP"
	case else
		ls_errore = "chiave prodotto"
end choose


//leggo il codice della chiave prodotto corrispondente
select stringa
into :fs_cod_chiave
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro=:fs_parametro and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale '"+fs_parametro+"' ("+ls_errore+"): "+sqlca.sqlerrtext
	return -1
end if

if isnull(fs_cod_chiave) or fs_cod_chiave="" then
	fs_errore = "Parametro aziendale "+fs_parametro+" ("+ls_errore+") non impostato! Non verrà esportata la colonna corrispondente!"
	fs_cod_chiave = ""
	return 0
end if

return 1
end function

public function integer wf_salva_file (string fs_path, long fl_anno_reg, long fl_num_reg, boolean fb_esiste, string fs_cod_nota, string fs_estensione, ref string fs_errore);integer				li_prog_mimetype
blob					lbl_file
string					ls_note

if f_file_to_blob( fs_path, lbl_file )<0 then
	fs_errore =  "Errore nella creazione del blob del documento!"
	return -1
end if


//recupero il prog mimetype
select prog_mimetype
into :li_prog_mimetype
from tab_mimetype
where 	cod_azienda = :s_cs_xx.cod_azienda and
			estensione = :fs_estensione;
	
if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante il controllo dell'estensione "+is_estensione_excel+" all'interno del database!"
	return -1
	
elseif sqlca.sqlcode = 100 or isnull(li_prog_mimetype) or li_prog_mimetype = 0 then
	fs_errore = "Estensione " + fs_estensione + " non trovata all'interno del database! File esportato non salvato in tabella note!"
	return -1
	
end if

ls_note = "Data esportazione: "+string(today(), "dd/mm/yyyy") + " ore: "+string( now( ), "hh:mm" )

if not fb_esiste then
	insert into tes_ord_acq_note
	(cod_azienda, anno_registrazione, num_registrazione, cod_nota, des_nota, note, prog_mimetype )
	values
	(:s_cs_xx.cod_azienda, :fl_anno_reg, :fl_num_reg, :fs_cod_nota, :is_des_nota, :ls_note, :li_prog_mimetype);

else
	update tes_ord_acq_note
	set 	des_nota = :is_des_nota,
			note = :ls_note,
			prog_mimetype=:li_prog_mimetype
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno_reg and
				num_registrazione=:fl_num_reg and
				cod_nota=:fs_cod_nota;

end if

if sqlca.sqlcode<0 then
	fs_errore = "Errore durante la preparazione del salvataggio in tabella note del file esportato ! "+sqlca.sqlerrtext
	return -1
end if

updateblob tes_ord_acq_note
set note_esterne = :lbl_file
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_reg and
			num_registrazione=:fl_num_reg and
			cod_nota=:fs_cod_nota;

if sqlca.sqlcode<0 then
	fs_errore = "Errore durante il salvataggio in tabella note del file esportato (UPDATEBLOB)! "+sqlca.sqlerrtext
	return -1
end if

fs_errore = "Operazione avvenuta con successo!"
return 1
end function

public function string wf_verniciatura (string fs_cod_prodotto);string				ls_cod_classe_merceol, ls_cod_veloce, ls_cod_verniciatura
long				ll_pos_verniciatura

if isnull( fs_cod_prodotto ) or fs_cod_prodotto="" then return ""

//posizione del carattere corrispondente al codice verniciatura nella strringa cod_prodotto
ll_pos_verniciatura = 6

ls_cod_classe_merceol = mid(fs_cod_prodotto, 2, 1)
ls_cod_veloce = mid(fs_cod_prodotto, ll_pos_verniciatura, 1)

if isnull(ls_cod_veloce) or ls_cod_veloce="" then
	return ""
else
	ls_cod_verniciatura = f_des_tabella("tab_verniciatura","cod_veloce='" +  ls_cod_veloce +"' and cod_classe_merceolo like '%"+ls_cod_classe_merceol+"%'", "cod_verniciatura")
	
	if isnull(ls_cod_verniciatura) then
		ls_cod_verniciatura = ""
	end if
	
	return ls_cod_verniciatura
	
	
end if

end function

public function integer wf_esporta_standard (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore);string					ls_sql, ls_errore, ls_doc_folder, ls_file, ls_foglio
long					ll_count, ll_colonna, ll_riga, ll_index
datastore			lds_excel
integer				li_rc
uo_excel_listini		luo_excel

//se arrivi fin qui continua
ls_sql = "select distinct prog_riga_ordine_acq,"+&
				"cod_prodotto, "+&
				"quan_ordinata,"+&
				"nota_dettaglio "+&
			"from det_ord_acq "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(fl_anno_reg)+" and num_registrazione="+string(fl_num_reg) +" and "+&
						"flag_blocco<>'S' "+&
			"order by prog_riga_ordine_acq "

ll_count = guo_functions.uof_crea_datastore(lds_excel, ls_sql, sqlca, ls_errore)
if ll_count<0 then
	fs_errore = ls_errore
	return -1
	
elseif ll_count=0 then
	fs_errore = "Nessuna riga nell'ordine d'acquisto da esportare!" 
	return -1
	
end if

if wf_chiedi_file(fs_path, fl_anno_reg, fl_num_reg, is_estensione_excel, "", fs_errore)<0 then
	//in fs_errore_il messaggio
	return -1
end if

open( w_report_prog_periodo_excel_child, this )

w_report_prog_periodo_excel_child.st_1.text = "ATTENDERE senza cliccare sul foglio Excel ..."

luo_excel = create uo_excel_listini
luo_excel.ib_OleObjectVisible = true
luo_excel.uof_inizio()

ls_foglio = "Ordine_"+string(fl_anno_reg) + right("000000"+string(fl_num_reg) ,6 )
luo_excel.uof_crea_foglio(ls_foglio)


//intestazione -------------------------------------------------------------------------------
ll_colonna = 1
ll_riga = 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Riga",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Articolo",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Q.tà",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Note",false,true,"none")


//dettaglio righe -------------------------------------------------------------------------
for ll_index=1 to ll_count
	ll_riga += 1
	
	ll_colonna = 1
	luo_excel.uof_scrivi_dec(ls_foglio,ll_riga,ll_colonna, lds_excel.getitemdecimal(ll_index, "prog_riga_ordine_acq"),false,false,"none","0" )	//Riga
	ll_colonna += 1
	luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,lds_excel.getitemstring(ll_index, "cod_prodotto"),false,false,"none")		//Articolo
	ll_colonna += 1
	luo_excel.uof_scrivi_dec(ls_foglio,ll_riga,ll_colonna, lds_excel.getitemdecimal(ll_index, "quan_ordinata"),false,false,"none","0,00" )	//Q.tà
	ll_colonna += 1
	luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna, lds_excel.getitemstring(ll_index, "nota_dettaglio"), false, false, "none")		//Nota dettaglio
	
next

luo_excel.uof_save_as_format( fs_path, 2003 )

luo_excel.uof_close( )

destroy lds_excel;
destroy luo_excel;

close( w_report_prog_periodo_excel_child )
this.setfocus()

//mi serve qualche attimo prima di procedere
fs_errore = "Ordine esportato. Clic su procedi per salvare il documento nella tabella note ..."

return 1
end function

public function integer wf_esporta_viv (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore);string					ls_sql,ls_errore, ls_doc_folder, ls_file, ls_foglio, ls_riferimento, ls_cod_verniciatura, ls_cod_prodotto, ls_chiave_valore,ls_verniciatura,&
						ls_note, ls_cod_fornitore, ls_cod_prod_fornitore, ls_um, ls_destinazione, ls_temp
long					ll_count, ll_riga, ll_colonna, ll_index, ll_misura
//longlong				ll_riferimento
datastore			lds_excel
integer				li_rc, li_prog_mimetype
uo_excel_listini		luo_excel
blob					lbl_file
boolean				lb_esiste
decimal				ld_quan_ordinata
datetime				ldt_data_consegna
date					ldt_consegna


ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_fornitore")

//se arrivi fin qui continua
ls_sql = "select distinct d.prog_riga_ordine_acq,"+&
				"d.cod_prod_fornitore, "+&
				"d.cod_prodotto, "+&
				"d.quan_ordinata,"+&
				"d.cod_misura,"+&
				"d.cod_verniciatura,"+&
				"d.nota_dettaglio,"+&
				"t.cod_deposito,"+&
				"t.cod_deposito_trasf,"+&
				"t.data_consegna "+&
			"from det_ord_acq as d "+&
			"join tes_ord_acq as t on t.cod_azienda=d.cod_azienda and "+&
											"t.anno_registrazione=d.anno_registrazione and "+&
											"t.num_registrazione=d.num_registrazione "+&
			"where d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"d.anno_registrazione="+string(fl_anno_reg)+" and d.num_registrazione="+string(fl_num_reg) +" and "+&
						"d.flag_blocco<>'S' "+&
			"order by prog_riga_ordine_acq "

ll_count = guo_functions.uof_crea_datastore(lds_excel, ls_sql, sqlca, ls_errore)
if ll_count<0 then
	fs_errore = ls_errore
	return -1
	
elseif ll_count=0 then
	fs_errore = "Nessuna riga nell'ordine d'acquisto da esportare!" 
	return -1
	
end if

if wf_chiedi_file(fs_path, fl_anno_reg, fl_num_reg, is_estensione_excel, "", fs_errore)<0 then
	//in fs_errore_il messaggio
	return -1
end if

open( w_report_prog_periodo_excel_child, this )

luo_excel = create uo_excel_listini
luo_excel.ib_OleObjectVisible = true
luo_excel.uof_inizio()

//ls_foglio = "Ordine_"+string(fl_anno_reg) + right("000000"+string(fl_num_reg) ,6 )
ls_foglio = "Foglio1"
luo_excel.uof_crea_foglio(ls_foglio)


//intestazione -------------------------------------------------------------------------------
ll_colonna = 1
ll_riga = 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Articolo",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Misura",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Colore",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Note",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Riferimento",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"hh_imp",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"Selezionati",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"destinazione",false,true,"none")
ll_colonna += 1
luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,"consegna",false,true,"none")

//dettaglio righe -------------------------------------------------------------------------
for ll_index=1 to ll_count
	ll_riga += 1
																																		//prog_riga_ordine_acq
	ls_riferimento = string(fl_anno_reg) + right("000000"+string(fl_num_reg) , 6) + right("0000"+string(lds_excel.getitemnumber(ll_index, 1)) , 4)
	
	//ll_riferimento = longlong( ls_riferimento )
	
	ls_cod_verniciatura = lds_excel.getitemstring(ll_index, 6)							//cod_verniciatura
	ls_cod_prodotto = lds_excel.getitemstring(ll_index, 3)								//cod_prodotto
	
	ls_destinazione = lds_excel.getitemstring(ll_index, 9)				//cod_deposito_trasf
	if ls_destinazione<>"" and not isnull(ls_destinazione) then
	else
		//prendi il cod_deposito
		ls_destinazione = lds_excel.getitemstring(ll_index, 8)			//cod_deposito
	end if
	
	//considera la prima cifra del cod_deposito in ls_destinazione
	ls_destinazione = left(ls_destinazione, 1)
	
	choose case ls_destinazione
		case "1"
			//saccolongo
			ls_destinazione = "10"
		case "2"
			//veggiano
			ls_destinazione = "11"
		case "3"
			//pistoia
			ls_destinazione = "12"
		case "4"
			//cuneo
			ls_destinazione = "13"
	end choose
	
	ldt_data_consegna = lds_excel.getitemdatetime(ll_index, 10)
	ldt_consegna = date(ldt_data_consegna)
	
	//Articolo ---------------------------------------------------------------------------------------------------------
	ll_colonna = 1
											//cod_prod_fornitore
	if wf_estrai_articolo_fornitore(lds_excel.getitemstring(ll_index, 2), ls_cod_prod_fornitore, fs_errore)<0 then
		close( w_report_prog_periodo_excel_child )
		this.setfocus()
		return -1
	end if
	
	ls_temp = ls_cod_prod_fornitore
	
	//rimandato a dopo
	//luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,ls_cod_prod_fornitore,false,false,"none")
	
	
	//Misura ----------------------------------------------------------------------------------------------------------
	//ll_colonna += 1
	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		if wf_tab_prod_fornitori(ls_cod_prodotto, ls_cod_fornitore, ls_cod_prod_fornitore, ll_misura, fs_errore)<0 then
			//in fs_errore il messaggio
			close( w_report_prog_periodo_excel_child )
			this.setfocus()
			return -1
		end if
	end if
	
	//Donato 11/12/2012
	//come da accordi con il programmatore VIV Michele Manfro
	//esprimo in millimetri
	ll_misura = ll_misura * 10
	ls_chiave_valore = string(ll_misura)
	
	//Articolo lo scrivo come codice fornitore seguito dalla misura in millimetri, separati da trattino "-"
	ls_cod_prod_fornitore = ls_temp + "-" + ls_chiave_valore
	luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna,ls_cod_prod_fornitore,false,false,"none")
	//Misura
	ll_colonna += 1
	luo_excel.uof_scrivi( ls_foglio, ll_riga,ll_colonna, ls_chiave_valore, false, false, "none" )
	
	
	//Colore -----------------------------------------------------------------------------------------------------------------
	ll_colonna += 1
	if ls_cod_verniciatura="" or isnull(ls_cod_verniciatura) then
		//provo a recuperare la verniciatura dal cod prodotto
		ls_cod_verniciatura = wf_verniciatura( ls_cod_prodotto )
	end if
	
	if ls_cod_verniciatura<>"" then
		if wf_verniciatura_transcodifica(ls_cod_verniciatura, ls_cod_fornitore, ls_verniciatura, fs_errore) < 0 then
			close( w_report_prog_periodo_excel_child )
			this.setfocus()
			//in fs_errore il messaggio
			return -1
		end if
	else
		ls_verniciatura = ""
	end if
	
	if isnumber(ls_verniciatura) then ls_verniciatura = "'" + ls_verniciatura
	
	luo_excel.uof_scrivi(ls_foglio, ll_riga, ll_colonna, ls_verniciatura, false, false, "none")		
	
	
	//Note ----------------------------------------------------------------------------------------------------------------------
	ll_colonna += 1											//nota_dettaglio
	luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna, lds_excel.getitemstring(ll_index, 7), false, false, "none")
	
	
	//Riferimento -----------------------------------------------------------------------------------------------------------------
	ll_colonna += 1
	ls_riferimento += " " + ls_cod_prodotto
	luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna, ls_riferimento, false, false, "none")		
	//luo_excel.uof_scrivi_dec(ls_foglio,ll_riga,ll_colonna, ll_riferimento,false,false,"none","0" )
	
	
	//hh_imp  -----------------------------------------------------------------------------------------------------------------
	ll_colonna += 1
	ls_chiave_valore=""
	if wf_chiavi_prodotto( ls_cod_prodotto, is_chiave_hh_imp, 1, ls_chiave_valore, ls_errore) < 0 then
		close( w_report_prog_periodo_excel_child )
		this.setfocus()
		fs_errore =  ls_errore
		return -1
	end if
	luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna, ls_chiave_valore, false, false, "none")		
	
	
	//Selezionati -----------------------------------------------------------------------------------------------------------------
	ll_colonna += 1
	
	
	if wf_converti_in_pezzi(fl_anno_reg, fl_num_reg, lds_excel.getitemnumber(ll_index, 1), ld_quan_ordinata, fs_errore) < 0 then
		//in fs_errore il messaggio
		close( w_report_prog_periodo_excel_child )
		this.setfocus()
		return -1
	end if
	
//	//ls_um = lds_excel.getitemstring(ll_index, "cod_misura")
//	ls_um = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")
//	ld_quan_ordinata = lds_excel.getitemdecimal(ll_index, 4)			//quan_ordinata
//	
//	//la quantità occorre espimerla sempre in pezzi PZ, quindi se diverso va convertita utilizzando la variabile ll_misura
//	//ll_misura è in cm, ed è da convertire in metri lineari (es. 500 cm che corrispondono a 500/100=5 metri)
//	if ls_um<>"PZ" and ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
//		//converti
//		if ll_misura>0 then
//			ld_quan_ordinata = ld_quan_ordinata / (ll_misura / 100)
//		else
//			fs_errore = "Riga "+ls_riferimento + ": Impossibile convertire in PEZZI: il fattore conversione 'L=' è nullo ..."
//			close( w_report_prog_periodo_excel_child )
//			this.setfocus()
//			return -1
//		end if
//	end if

	luo_excel.uof_scrivi_dec(ls_foglio,ll_riga,ll_colonna, ld_quan_ordinata,false,false,"none","0,00")
	
	
	//destinazione ----------------------------------------------------------------------------------------------------------------------
	ll_colonna += 1
	luo_excel.uof_scrivi(ls_foglio,ll_riga,ll_colonna, ls_destinazione, false, false, "none")
	
	
	//consegna   (data di consegna ordine)
	ll_colonna += 1
	luo_excel.uof_scrivi_data(ls_foglio,ll_riga, ll_colonna, ldt_data_consegna,false,false,"none","gg/mm/aaaa")
	
next

luo_excel.uof_save_as_format( fs_path, 2003 )

luo_excel.uof_close( )

destroy lds_excel;
destroy luo_excel;

close( w_report_prog_periodo_excel_child )
this.setfocus()

fs_errore = "Ordine esportato. Clic su procedi per salvare il documento nella tabella note ..."

return 1

end function

public function integer wf_esporta_anodall (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore);string					ls_sql, ls_doc_folder, ls_file, ls_cod_prodotto, ls_rag_soc_1, ls_cod_prodotto_fornitore, &
						ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_cod_misura, ls_riga, ls_des_prodotto, ls_um_qta_2,&
						ls_cop_deposito, ls_cod_deposito_trasf, ls_cod_verniciatura,ls_verniciatura, ls_cod_fornitore,ls_misura, &
						ls_cod_prodotto_fornitore_2, ls_prefisso_file, ls_ragionesocialefornitore, ls_nota_dettaglio, ls_numero_documento
long					ll_count, ll_index, ll_prog_riga, ll_ret, ll_misura
datastore				lds_data
integer				li_FileNum
datetime				ldt_data_registrazione, ldt_data_consegna
decimal				ld_quan_ordinata, ld_fat_conversione, ld_quantità_2, ld_quan_ordinata_conv

//recupero info dalla testata dell'ordine di acquisto
select 	rag_soc_1,
			data_registrazione,
			indirizzo,
			cap,
			localita,
			provincia,
			cod_deposito,
			cod_deposito_trasf,
			cod_fornitore
into		:ls_rag_soc_1,
			:ldt_data_registrazione,
			:ls_indirizzo,
			:ls_cap,
			:ls_localita,
			:ls_provincia,
			:ls_cop_deposito,
			:ls_cod_deposito_trasf,
			:ls_cod_fornitore
from tes_ord_acq
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_reg and
			num_registrazione=:fl_num_reg;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura info testata ordine: "+sqlca.sqlerrtext 
	return -1
end if

if isnull(ls_rag_soc_1) then ls_rag_soc_1 = ""
if isnull(ls_indirizzo) then ls_indirizzo = ""
if isnull(ls_cap) then ls_cap = ""
if isnull(ls_localita) then ls_localita = ""
if isnull(ls_provincia) then ls_provincia = ""


select rag_soc_1
into :ls_ragionesocialefornitore
from anag_fornitori
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_fornitore=:ls_cod_fornitore;

if isnull(ls_ragionesocialefornitore) then ls_ragionesocialefornitore=""

//info su dettagli
//quan_ordinata 		è in genere la qta in PZ , 	cioè la qta in UM mag che nel file corrisponderà Quantita_1
//cod_misura 			è in genere in KG, 			cioè la UM acquisto che nel file corrisponderà a UM_QTA2

//per ricavare Quantita_2 e UM_QTA1 del file procedere cosi
//		leggere fat_conversione che corrisponde a [qta in UMacq]/[qta in UMmag] = [qta in KG]/[qta in PZ]
//		quan_ordinata[PZ] * fat_conversione[KG/PZ]  ed otteniamo qta in[KG] ovvero Quantita_2 nel file
//per UM_QTA1 (in genere in PZ) leggere  cod_misura_mag da anag_prodotti mediante f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")

ls_sql = "select distinct prog_riga_ordine_acq,"+&
				"cod_prod_fornitore, "+&
				"cod_prodotto, "+&
				"des_prodotto,"+&
				"quan_ordinata,"+&
				"cod_misura,"+&
				"fat_conversione,"+&
				"data_consegna,"+&
				"cod_verniciatura,"+&
				"nota_dettaglio "+&
			"from det_ord_acq "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(fl_anno_reg)+" and num_registrazione="+string(fl_num_reg) +" and "+&
						"flag_blocco<>'S' "+&
			"order by prog_riga_ordine_acq "

ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, fs_errore)
if ll_count<0 then
	return -1
	
elseif ll_count=0 then
	fs_errore = "Nessuna riga nell'ordine d'acquisto da esportare!" 
	return -1
	
end if

//16/11/2012 Se ANODALL EXTRUSION il nome del file deve avere prefisso 1_ ...
//					altrimenti 2_ ...
if pos(UPPER(ls_ragionesocialefornitore), "EXTRUSION") > 0 then
	ls_prefisso_file = "1_"
else
	ls_prefisso_file = "2_"
end if
//---------------------------------------------------------------------------------------------

if wf_chiedi_file(fs_path, fl_anno_reg, fl_num_reg, "txt", ls_prefisso_file, fs_errore)<0 then
	//in fs_errore_il messaggio
	return -1
end if

open( w_report_prog_periodo_excel_child, this )

w_report_prog_periodo_excel_child.st_1.text = "ATTENDERE ..."

//apri il file sql per la scrittura solo all'inizio
li_FileNum = FileOpen(fs_path, StreamMode!, Write!, LockWrite!,Replace! )


//intestazione -------------------------------------------------------------------------------
/*
POS	LEN	DESCRIZIONE
1		6		numerico intero
7		1		alfanum 								(B/O)
8		10		alfanum 								GG/MM/YYYY
18		15		alfanum
33		6		numerico intero
39		1		numerico intero 					(0/1 no prodotto/prodotto)
40		25		alfanumerico
65		100	alfanumerico
165	10		alfanumerico
175	5		numerico intero
180	2		alfanumerico
182	3		alfanumerico 						SEMPRE VUOTO
185	14		numerico a 3 decimali
199	3		alfanumerico
202	14		numerico a 3 decimali
216	3		alfanumerico
219	25		alfanumerico 						...nnnn-..pp
244	10		alfanumerico						GG/MM/YYYY
254	25		alfanumerico
279	14		alfanumerico
293	40		alfanumerico
333	40		alfanumerico
373	8		alfanumerico
381	40		alfanumerico
421	2		alfanumerico
423	70		alfanumerico   						note riga
*/

//dettaglio righe -------------------------------------------------------------------------
for ll_index=1 to ll_count
	
	ll_prog_riga = lds_data.getitemnumber(ll_index, "prog_riga_ordine_acq")
	
	ls_cod_prodotto = lds_data.getitemstring(ll_index, "cod_prodotto")
	if isnull( ls_cod_prodotto ) then ls_cod_prodotto=""
	
	ls_cod_prodotto_fornitore = lds_data.getitemstring(ll_index, "cod_prod_fornitore")
	if isnull( ls_cod_prodotto_fornitore ) then ls_cod_prodotto_fornitore=""
	if wf_estrai_articolo_fornitore(ls_cod_prodotto_fornitore, ls_cod_prodotto_fornitore, fs_errore)<0 then
		fs_errore = "Riga "+string(ll_prog_riga) + " : " + fs_errore
		close( w_report_prog_periodo_excel_child )
		this.setfocus()
		return -1
	end if
	
	ls_des_prodotto = lds_data.getitemstring(ll_index, "des_prodotto")
	if isnull(ls_des_prodotto) or ls_des_prodotto="" then
		 ls_des_prodotto = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "des_prodotto")
	end if
	if isnull(ls_des_prodotto) then ls_des_prodotto=""
	 
	ld_quan_ordinata =  lds_data.getitemdecimal(ll_index, "quan_ordinata")
	if isnull(ld_quan_ordinata) then ld_quan_ordinata = 0
	
	ls_cod_misura = lds_data.getitemstring(ll_index, "cod_misura")
	if isnull(ls_cod_misura) then ls_cod_misura = ""
	
	ld_fat_conversione =  lds_data.getitemdecimal(ll_index, "fat_conversione")
	if isnull(ld_fat_conversione) then ld_fat_conversione = 0
	
	ldt_data_consegna = lds_data.getitemdatetime(ll_index, "data_consegna")
	
	ls_cod_verniciatura = lds_data.getitemstring(ll_index, "cod_verniciatura")
	
	ls_nota_dettaglio = lds_data.getitemstring(ll_index, "nota_dettaglio")
	
	
	//UM qta acquisto corrisponde nel file a UM_QTA_2
	
	//leggo UM magazzino che corrisponde nel file a UM_QTA_1
	ls_um_qta_2 = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")
	if isnull(ls_um_qta_2) then ls_um_qta_2=""
	
	ls_riga = ""	//variabile che poi sarà scritta nel file
	//																	POS	LEN	DESCRIZIONE
	//Codice_Cli_For 												1		6		numerico intero
	ls_riga += wf_prepara_campo(is_codice_cliente_fornitore, 6)
	
	//Tipo_Documento												7		1		alfanum 								(B/O)
	ls_riga += "O"
	
	//Data_Documento											8		10		alfanum 								GG/MM/YYYY
	ls_riga += string(ldt_data_registrazione, "dd/mm/yyyy")

	//Numero_Documento										18		15		alfanum
	ls_numero_documento = string(fl_anno_reg) + "-" + string(fl_num_reg)
	ls_riga += wf_prepara_campo(ls_numero_documento, 15)
	
	//Riga_Documento												33		6		numerico intero
	ls_riga += wf_prepara_campo_numerico(ll_prog_riga, 6, 0)
	//ls_riga += wf_prepara_campo(string(ll_prog_riga, "###0"), 6)
	
	//Tipo_Riga														39		1		numerico intero 					(0/1 no prodotto/prodotto)
	if ls_cod_prodotto_fornitore<>"" then
		ls_riga += "0"	//riga articolo
	else
		ls_riga += "2"	//riga note
	end if
	
	//Codice_Articolo												40		25		alfanumerico
	//21/02/2012 per Anodall/Extrusion esiste una ulteriore transcodifica tab_prod_fornitori.cod_prod_fornitore_2
	if wf_leggi_cod_prod_fornitore_2(ls_cod_prodotto, ls_cod_fornitore, ls_cod_prodotto_fornitore_2, fs_errore) < 0 then
		//in fs_errore il messaggio
		fs_errore = "Riga "+string(ll_prog_riga) + ": " + fs_errore
		close( w_report_prog_periodo_excel_child )
		this.setfocus()
		return -1
	end if
	if isnull(ls_cod_prodotto_fornitore_2) or ls_cod_prodotto_fornitore_2="" then ls_cod_prodotto_fornitore_2=ls_cod_prodotto_fornitore
	
	ls_riga += wf_prepara_campo(ls_cod_prodotto_fornitore_2, 25)
	//---------------------------------------------------------------------------------------------------------------------------------
	
	//Descrizione_Articolo										65		100	alfanumerico
	ls_riga += wf_prepara_campo(ls_des_prodotto, 100)
	
	//-------------------------
	//Var1_Finitura_Articolo										165	10		alfanumerico		(si tratta della verniciatura transcodificata per fornitore)
	
	if isnull(ls_cod_verniciatura) then ls_cod_verniciatura=""
//	if ls_cod_verniciatura="" or isnull(ls_cod_verniciatura) then
//		//provo a recuperare la verniciatura dal cod prodotto
//		ls_cod_verniciatura = wf_verniciatura( ls_cod_prodotto )
//	end if
	
	if ls_cod_verniciatura<>"" then
		if wf_verniciatura_transcodifica(ls_cod_verniciatura, ls_cod_fornitore, ls_verniciatura, fs_errore) < 0 then
			//in fs_errore il messaggio
			fs_errore = "Riga "+string(ll_prog_riga) + " : " + fs_errore
			close( w_report_prog_periodo_excel_child )
			this.setfocus()
			return -1
		end if
	else
		ls_verniciatura = ""
	end if
	ls_verniciatura = left(ls_verniciatura, 10)
	ls_riga += wf_prepara_campo(ls_verniciatura, 10)
	//--------------------------
	
	//--------------------------
	
	//SALVO LA QUANTITA ORDINE
	ld_quan_ordinata_conv = ld_quan_ordinata
	
	//Var2_Misura_Articolo										175	5		numerico intero		(lunghezza articolo in mm)
	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		
		if wf_converti_in_pezzi(fl_anno_reg, fl_num_reg, ll_prog_riga, ld_quan_ordinata, fs_errore) < 0 then
			//in fs_errore il messaggio
			fs_errore = "Riga "+string(ll_prog_riga) + " : " + fs_errore
			close( w_report_prog_periodo_excel_child )
			this.setfocus()
			return -1
		end if
		
//		if wf_tab_prod_fornitori(ls_cod_prodotto, ls_cod_fornitore, ls_cod_prodotto_fornitore, ll_misura, fs_errore)<0 then
//			//in fs_errore il messaggio
//			close( w_report_prog_periodo_excel_child )
//			this.setfocus()
//			return -1
//		end if
	end if
	
	if wf_tab_prod_fornitori(ls_cod_prodotto, ls_cod_fornitore, ls_cod_prodotto_fornitore, ll_misura, fs_errore)<0 then
		//in fs_errore il messaggio
		fs_errore = "Riga "+string(ll_prog_riga) + " : " + fs_errore
		close( w_report_prog_periodo_excel_child )
		this.setfocus()
		return -1
	end if
	
	//esprimo il valore in millimetri
	ll_misura = ll_misura * 10
	
	ls_misura = left(string(ll_misura), 5)
	ls_riga += wf_prepara_campo_numerico(long(ls_misura), 5, 0)
	//---------------------------
	
	//Var3_Stato_Lega_Articolo 								180	2		alfanumerico						SEMPRE T6
	//16/11/2012 Se ANODALL EXTRUSION passare A0, che corrisponde poi ad un loro specifico valore (6060 T6)
	if pos(UPPER(ls_ragionesocialefornitore), "EXTRUSION") > 0 then
		ls_riga += wf_prepara_campo("A0", 2)
	else
		ls_riga += wf_prepara_campo("", 2)
	end if
	//ls_riga += wf_prepara_campo("T6", 2)
	
	
	//Var4_Variante4												182	3		alfanumerico 						SEMPRE VUOTO
	ls_riga += wf_prepara_campo("", 3)
	
	
	//Quantita_1													185	14		numerico a 3 decimali
//	//la quantità occorre espimerla sempre in pezzi PZ, quindi se diverso va convertita utilizzando la variabile ll_misura
//	//ll_misura è in mm, ed è da convertire in metri lineari (es. 6000 mm che corrispondono a 6000/1000=6 metri)
//	//fai questo solo se non è già espressa in PZ
//	if ls_um_qta_2<>"PZ" and ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
//		//converti
//		if ll_misura>0 then
//			ld_quan_ordinata_conv = ld_quan_ordinata / (ll_misura / 1000)
//		else
//			fs_errore = "Impossibile convertire in PEZZI: il fattore conversione 'L=' è nullo ..."
//			close( w_report_prog_periodo_excel_child )
//			this.setfocus()
//			return -1
//		end if
//		
//		ls_riga += wf_prepara_campo_numerico(ld_quan_ordinata_conv, 14, 3)	
//	else
//		ls_riga += wf_prepara_campo_numerico(ld_quan_ordinata, 14, 3)
//	end if
	ls_riga += wf_prepara_campo_numerico(ld_quan_ordinata, 14, 3)
	
	
	//UM_QTA1														199	3		alfanumerico   SEMPRE in BA  (barre = pezzi)
	//ls_riga += wf_prepara_campo(ls_um_qta_2, 3)
	ls_riga += wf_prepara_campo("BA", 3)
	
	//Quantità_2													202	14		numerico a 3 decimali
	ld_quantità_2 = ld_quan_ordinata_conv * ld_fat_conversione
	ls_riga += wf_prepara_campo_numerico(ld_quantità_2, 14, 3)
	//ls_riga += wf_prepara_campo(string(ld_quantità_2, "#########0.000"), 14)
	
	//UM_QTA2														216	3		alfanumerico
	ls_riga += wf_prepara_campo(ls_cod_misura, 3)
	
	//Riferimento_Ordine											219	25		alfanumerico 						...nnnn-..pp
	ls_riga += wf_prepara_campo(string(fl_num_reg, "#####0")+"-"+string(ll_prog_riga, "###0"), 25)
	
	//Data_Consegna												244	10		alfanumerico						GG/MM/YYYY
	ls_riga += string(ldt_data_consegna, "dd/mm/yyyy")
	
	//Numero_Pacco												254	25		alfanumerico						SEMPRE VUOTO
	ls_riga += wf_prepara_campo("", 25)
	
	//gestione campi destinazione
	wf_destinazione(ls_cop_deposito, ls_cod_deposito_trasf, ls_rag_soc_1, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_riga)
	
	
	ls_riga += left(ls_nota_dettaglio, 70)		//ho a disposizione 70 caratteri nel file
	
	//a capo
	ls_riga += char(13)+char(10)
	
	FileWrite(li_FileNum, ls_riga)
next

FileClose (li_FileNum)

close( w_report_prog_periodo_excel_child )
this.setfocus()

destroy lds_data;


fs_errore = "Ordine esportato. Clic su procedi per salvare il documento nella tabella note ..."

return 1

end function

public function integer wf_esporta_bustreo (long fl_anno_reg, long fl_num_reg, ref string fs_path, ref string fs_errore);datetime				ldt_data_registrazione, ldt_data_consegna
datastore			lds_data
string					ls_sql, ls_errore, ls_riga, ls_separatore, ls_riga_ordine
string					ls_rag_soc_1, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_cod_deposito, ls_cod_deposito_trasf, ls_cod_fornitore, &
						ls_des_prodotto,ls_cod_misura, ls_cod_prodotto,ls_cod_verniciatura, ls_nota_dettaglio, ls_rag_soc_2
long					ll_count, ll_index, ll_prog_riga
integer				li_FileNum
decimal				ld_quan_ordinata, ld_fat_conversione, ld_quantità_2, ld_prezzo_unit, ld_prezzo_unit_netto, ld_sconto_1, ld_sconto_2

ls_separatore = "" //","		//non serve + la virgola come separatore

//recupero info dalla testata dell'ordine di acquisto
select 	rag_soc_1,
			rag_soc_2,
			data_registrazione,
			indirizzo,
			cap,
			localita,
			provincia,
			cod_deposito,
			cod_deposito_trasf,
			cod_fornitore
into		:ls_rag_soc_1,
			:ls_rag_soc_2,
			:ldt_data_registrazione,
			:ls_indirizzo,
			:ls_cap,
			:ls_localita,
			:ls_provincia,
			:ls_cod_deposito,
			:ls_cod_deposito_trasf,
			:ls_cod_fornitore
from tes_ord_acq
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_reg and
			num_registrazione=:fl_num_reg;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura info testata ordine: "+sqlca.sqlerrtext 
	return -1
end if

if isnull(ls_rag_soc_1) then ls_rag_soc_1 = ""
if isnull(ls_indirizzo) then ls_indirizzo = ""
if isnull(ls_cap) then ls_cap = ""
if isnull(ls_localita) then ls_localita = ""
if isnull(ls_provincia) then ls_provincia = ""

//info su dettagli
//quan_ordinata 		è in genere la qta in PZ (ML) , 	cioè la qta in UM mag che nel file corrisponderà Quantita_1
//cod_misura 			è in genere in KG, 			cioè la UM acquisto che nel file corrisponderà a UM_QTA2

//per ricavare Quantita_2 e UM_QTA1 del file procedere cosi
//		leggere fat_conversione che corrisponde a [qta in UMacq]/[qta in UMmag] = [qta in KG]/[qta in PZ]
//		quan_ordinata[PZ] * fat_conversione[KG/PZ]  ed otteniamo qta in[KG] ovvero Quantita_2 nel file
//per UM_QTA1 (in genere in PZ) leggere  cod_misura_mag da anag_prodotti mediante f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")

ls_sql = "select distinct prog_riga_ordine_acq,"+&
				"cod_prodotto, "+&
				"des_prodotto,"+&
				"quan_ordinata,"+&
				"cod_misura,"+&
				"fat_conversione,"+&
				"data_consegna,"+&
				"prezzo_acquisto,"+&
				"sconto_1,"+&
				"sconto_2,"+&
				"nota_dettaglio,"+&
				"cod_verniciatura "+&
			"from det_ord_acq "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(fl_anno_reg)+" and num_registrazione="+string(fl_num_reg) +" and "+&
						"flag_blocco<>'S' "+&
			"order by prog_riga_ordine_acq "

ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, ls_errore)
if ll_count<0 then
	fs_errore = ls_errore
	return -1
	
elseif ll_count=0 then
	fs_errore = "Nessuna riga nell'ordine d'acquisto da esportare!" 
	return -1
	
end if

if wf_chiedi_file(fs_path, fl_anno_reg, fl_num_reg, "csv", "", fs_errore)<0 then
	//in fs_errore_il messaggio
	return -1
end if

open( w_report_prog_periodo_excel_child, this )

w_report_prog_periodo_excel_child.st_1.text = "ATTENDERE ..."

//apri il file sql per la scrittura solo all'inizio
li_FileNum = FileOpen(fs_path, StreamMode!, Write!, LockWrite!,Replace! )


//intestazione -------------------------------------------------------------------------------
//file csv, separato da virgola o punto e virgola
/*									max
NOMECAMPO					LEN			DESCRIZIONE
-----------------------------------------------------------------------------------------------
NsRif								14				alfanumerico 						YYYYnnnnnnPPPP
CArt								15				alfanum 								quello di APICE
Descriz							40				alfanum 								quella di APICE
Qta								8				float, 1 decimal con separatore decimale .
DataCons						8														GG/MM/AAAA
PrezzoUnit						10				float, 2 decimal con separatore decimale .
Sconto_1							8				float, 2 decimal con separatore decimale .
Sconto_2							8				float, 2 decimal con separatore decimale .
PrezzoUnitNetto				10				float, 2 decimal con separatore decimale .
Nota								40				alfanumerico
Um								3				alfanumerico 						
DestSedeFinale_RagSoc1	40				alfanumerico
DestSedeFinale_RagSoc2	40				alfanumerico
DestSedeFinale_Indirizzo	40				    alfanumerico
DestSedeFinale_Localita		40				alfanumerico
DestSedeFinale_Cap			5				alfanumerico 	
DestSedeFinale_Prov			2				alfanumerico
LuogoDest_RagSoc1			40				alfanumerico
LuogoDest_RagSoc2			40				alfanumerico
LuogoDest_Indirizzo			40				alfanumerico
LuogoDest_Localita			40				alfanumerico
LuogoDest_Cap				     5				alfanumerico
LuogoDest_Prov				2				alfanumerico
CodSedeMago					8				alfanumerico -> sede proposta poi nei ddt come sede consegna merce
*/

//dettaglio righe -------------------------------------------------------------------------
for ll_index=1 to ll_count
	
	ll_prog_riga = lds_data.getitemnumber(ll_index, "prog_riga_ordine_acq")
	ls_riga_ordine =  	wf_prepara_campo_numerico(fl_anno_reg, 4, 0) + &
							wf_prepara_campo_numerico(fl_num_reg, 6, 0) + &
							 wf_prepara_campo_numerico(ll_prog_riga, 4, 0)
	
	ls_cod_prodotto = lds_data.getitemstring(ll_index, "cod_prodotto")
	if isnull( ls_cod_prodotto ) then ls_cod_prodotto=""
	
	ls_des_prodotto = lds_data.getitemstring(ll_index, "des_prodotto")
	if isnull(ls_des_prodotto) or ls_des_prodotto="" then
		 ls_des_prodotto = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "des_prodotto")
	end if
	if isnull(ls_des_prodotto) then ls_des_prodotto=""
	 
	ld_quan_ordinata =  lds_data.getitemdecimal(ll_index, "quan_ordinata")
	if isnull(ld_quan_ordinata) then ld_quan_ordinata = 0
	
	ls_cod_misura = lds_data.getitemstring(ll_index, "cod_misura")
	if isnull(ls_cod_misura) then ls_cod_misura = ""
	
	ld_fat_conversione =  lds_data.getitemdecimal(ll_index, "fat_conversione")
	if isnull(ld_fat_conversione) then ld_fat_conversione = 0
	
	ldt_data_consegna = lds_data.getitemdatetime(ll_index, "data_consegna")
	ls_cod_verniciatura = lds_data.getitemstring(ll_index, "cod_verniciatura")
	
	ld_prezzo_unit = lds_data.getitemdecimal(ll_index, "prezzo_acquisto")
	ld_prezzo_unit_netto = ld_prezzo_unit_netto
	if isnull(ld_prezzo_unit) then ld_prezzo_unit = 0
	if isnull(ld_prezzo_unit_netto) then ld_prezzo_unit_netto = 0
	
	ls_nota_dettaglio = lds_data.getitemstring(ll_index, "nota_dettaglio")
	if isnull(ls_nota_dettaglio) then ls_nota_dettaglio = ""
	
	ld_sconto_1 = lds_data.getitemdecimal(ll_index, "sconto_1")
	if isnull(ld_sconto_1) then ld_sconto_1 = 0
	
	ld_sconto_2 = lds_data.getitemdecimal(ll_index, "sconto_2")
	if isnull(ld_sconto_2) then ld_sconto_2 = 0
	
	//-----------------------------------------------------------
	ls_riga = ""	//variabile che poi sarà scritta nel file
	
	//NsRif								14				alfanumerico 						YYYYnnnnnnPPPP
	ls_riga += ls_riga_ordine
	
	//CArt								15				alfanum 								quello di APICE
	//ls_riga += ls_separatore + ls_cod_prodotto
	ls_riga += ls_separatore + wf_prepara_campo(ls_cod_prodotto, 15)
	
	//Descriz								40				alfanum 								quella di APICE
	//ls_riga += ls_separatore + ls_des_prodotto
	ls_riga += ls_separatore + wf_prepara_campo(ls_des_prodotto, 40)
	
	//Qta									8				float 1 decimal con separatore decimale
	//ls_riga += ls_separatore + wf_prepara_campo_numerico(ld_quan_ordinata, 1)
	ls_riga += ls_separatore + wf_prepara_campo_numerico_bustreo(ld_quan_ordinata, 8, 1)
	
	//DataCons							8														GG/MM/AAAA
	ls_riga += ls_separatore + string(ldt_data_consegna, "ddmmyyyy")
	
	//PrezzoUnit							10				float 2 decimal con separatore decimale
	//ls_riga += ls_separatore + wf_prepara_campo_numerico(ld_prezzo_unit, 2)
	ls_riga += ls_separatore + wf_prepara_campo_numerico_bustreo(ld_prezzo_unit, 10, 3)    //2)
	
	//Sconto_1							8				float 2 decimal con separatore decimale
	//ls_riga += ls_separatore + wf_prepara_campo_numerico(ld_sconto_1, 2)
	ls_riga += ls_separatore + wf_prepara_campo_numerico_bustreo(ld_sconto_1, 8, 2)
	
	//Sconto_2							8				float 2 decimal con separatore decimale
	//ls_riga += ls_separatore + wf_prepara_campo_numerico(ld_sconto_2, 2)
	ls_riga += ls_separatore + wf_prepara_campo_numerico_bustreo(ld_sconto_2, 8, 2)
	
	//PrezzoUnitNetto					10				float 2 decimal senza separatore decimale
	//ls_riga += ls_separatore + wf_prepara_campo_numerico(ld_prezzo_unit, 2)
	ls_riga += ls_separatore + wf_prepara_campo_numerico_bustreo(ld_prezzo_unit, 10, 3)   //2)
	
	//Nota								40				alfanumerico
	//ls_riga += ls_separatore + left(ls_nota_dettaglio, 40)
	ls_riga += ls_separatore + wf_prepara_campo(ls_nota_dettaglio, 40)
	
	//Um									3				alfanumerico 		
	//ls_riga += ls_separatore + ls_cod_misura
	ls_riga += ls_separatore + wf_prepara_campo(ls_cod_misura, 3)
	
	//DestSedeFinale_RagSoc1		40				alfanumerico
	//DestSedeFinale_RagSoc2		40				alfanumerico
	//DestSedeFinale_Indirizzo		40				alfanumerico
	//DestSedeFinale_Localita		40				alfanumerico
	//DestSedeFinale_Cap			5				alfanumerico 	
	//DestSedeFinale_Prov			2				alfanumerico
	//LuogoDest_RagSoc1			40				alfanumerico
	//LuogoDest_RagSoc2			40				alfanumerico
	//LuogoDest_Indirizzo			    40				alfanumerico
	//LuogoDest_Localita				40				alfanumerico
	//LuogoDest_Cap					5				alfanumerico
	//LuogoDest_Prov					2				alfanumerico
	wf_destinazione_bustreo(ls_separatore, ls_cod_deposito, ls_cod_deposito_trasf, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_riga)
	
	//CodSedeMago					8				alfanumerico -> sede proposta poi nei ddt come sede consegna merce
	//ls_riga += ls_separatore + ls_cod_sede_mago
	ls_riga += ls_separatore + wf_prepara_campo(is_CodSedeMago, 8)


	//a capo
	ls_riga += char(13)+char(10)
	
	FileWrite(li_FileNum, ls_riga)
next

FileClose (li_FileNum)

close( w_report_prog_periodo_excel_child )
this.setfocus()

destroy lds_data;


fs_errore = "Ordine esportato. Clic su procedi per salvare il documento nella tabella note ..."

return 1

end function

public function string wf_prepara_campo_numerico (decimal fd_valore, long fl_decimali);//allineamento a destra (numeri) senza riepimento con ZERI ma con separatore decimale
string ls_valore


fd_valore = round(fd_valore, fl_decimali)		//34.98  ->  34.980

ls_valore = string(fd_valore)

//tolgo eventuali separatori decimali virgola e lo imposto come punto
guo_functions.uof_replace_string(ls_valore,",", ".")

return ls_valore
end function

public function integer wf_destinazione_bustreo (string fs_separatore, string fs_cod_deposito, string fs_cod_deposito_trasf, string fs_rag_soc_1, string fs_rag_soc_2, string fs_indirizzo, string fs_cap, string fs_localita, string fs_provincia, ref string fs_riga);string ls_riga, ls_deposito
string ls_rag_soc_1, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_cod_fornitore, ls_rag_soc_2

//tes_ord_acq.cod_deposito  		 ->    stabilimento origine ordine   	->  LuogoDest_...
//tes_ord_acq.cod_deposito_trasf 	 ->    stabilimento consegna finale   	->  LuogoDestFinale_...

//DESTINAZIONE SEDE FINALE (CONSEGNA FINALE PRODOTTI) #############################################
//se "tes_ord_acq.cod_deposito_trasf" non viene specificato in LuogoDestFinale_... metti LuogoDest_...

//verifico se è stato specificato il deposito trasferimento
if isnull(fs_cod_deposito_trasf) or fs_cod_deposito_trasf = "" then
	//PRESUMI ALLORA CHE TRASFERIMENTO COINCIDE CON DEPOSITO ORIGINE ORDINE
	fs_cod_deposito_trasf = fs_cod_deposito
end if

//leggo da anag_deposito per il deposito corrispondente ---------------------------
ls_rag_soc_1 = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito_trasf +"'", "des_deposito")
ls_rag_soc_2 = ls_rag_soc_1
ls_indirizzo = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito_trasf +"'", "indirizzo")
ls_cap = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito_trasf +"'", "cap")
ls_localita = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito_trasf +"'", "localita")
ls_provincia = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito_trasf +"'", "provincia")

//scrivi su file -------------------------------------------------------------------------
//DestSedeFinale_RagSoc1					40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_rag_soc_1, 40)

//DestSedeFinale_RagSoc2					40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_rag_soc_2, 40)

//DestSedeFinale_Indirizzo					40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_indirizzo, 40)

//DestSedeFinale_Localita					40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_localita, 40)

//DestSedeFinale_Cap						5		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_cap, 5)

//DestSedeFinale_Prov						2		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_provincia, 2)



//LUOGO DEST.  (STABILIMENTO ORIGINE ORDINE) ###############################################

//leggo da anag_deposito per il deposito corrispondente ---------------------------------------
ls_rag_soc_1 = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito +"'", "des_deposito")
ls_rag_soc_2 = ls_rag_soc_1
ls_indirizzo = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito +"'", "indirizzo")
ls_cap = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito +"'", "cap")
ls_localita = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito +"'", "localita")
ls_provincia = f_des_tabella("anag_depositi","cod_deposito = '" +  fs_cod_deposito +"'", "provincia")

//scrivi su file ---------------------------------------------------------------------------------------
//LuogoDest_RagSoc1					40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_rag_soc_1, 40)

//LuogoDeste_RagSoc2					40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_rag_soc_2, 40)

//LuogoDest_Indirizzo					40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_indirizzo, 40)

//LuogoDest_Localita						40		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_localita, 40)

//LuogoDest_Cap							5		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_cap, 5)

//LuogoDest_Prov							2		alfanumerico
fs_riga += fs_separatore + wf_prepara_campo(ls_provincia, 2)

return 1
end function

public subroutine wf_stampa (boolean ab_flag_residuo);long   ll_count, ll_anno_reg, ll_num_reg
s_report_ordine ls_report_ordine

ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
ll_num_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")

select count(*)
into   :ll_count
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg and
		 num_registrazione = :ll_num_reg;
		 
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Apice","Errore nella select di det_off_acq: " + sqlca.sqlerrtext)
	return
end if

if isnull(ll_count) then ll_count = 0

if ll_count < 1 then
	g_mb.warning("Non ci sono dettagli per questo documento, impossibile stampare")
	return
end if	

triggerevent("pc_menu_calcola")

ls_report_ordine.al_anno_registrazione = ll_anno_reg
ls_report_ordine.al_num_registrazione = ll_num_reg
ls_report_ordine.ab_flag_residuo = ab_flag_residuo
window_open_parm(w_report_ord_acq_euro, -1,  ls_report_ordine)
end subroutine

public function integer wf_verniciatura_transcodifica (string fs_cod_verniciatura, string fs_cod_fornitore, ref string fs_verniciatura, ref string fs_errore);
fs_verniciatura = ""

select cod_trans_1
into :fs_verniciatura
from tab_verniciature_transcodifica
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_verniciatura=:fs_cod_verniciatura and
			cod_fornitore=:fs_cod_fornitore;
				
if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura verniciatura transcodifica: non sarà riportato il colore per la riga "
	return -1
end if

if isnull(fs_verniciatura) then fs_verniciatura=""

return 1
end function

public function string wf_prepara_campo_numerico_bustreo (decimal fd_valore, long fl_lunghezza, long fl_decimali);//allineamento a destra (numeri) con riempimento con ZERI e con separatore decimale
string ls_valore


//Nota, se gli arriva valore zero o nullo, non mi mette il decimal separetor, allora faccio cosi
if isnull(fd_valore) or fd_valore=0 then
	ls_valore = Fill("0", fl_lunghezza - (fl_decimali + 1)) + "." + Fill("0", fl_decimali)
	
	return ls_valore
end if

																												//se 2 decimali
fd_valore = round(fd_valore, fl_decimali)															//34.987  ->  34.99
ls_valore = string(fd_valore)																			//"34.99"

//tolgo eventuali separatori decimali virgola e imposto il punto
guo_functions.uof_replace_string(ls_valore,",", ".")													//34.99
																		
																												//se lunghezza 8
ls_valore = right(Fill ("0", fl_lunghezza) + ls_valore, fl_lunghezza)								//00034.99

return ls_valore
end function

public function integer wf_tab_prod_fornitori (string fs_cod_prodotto, string fs_cod_fornitore, ref string fs_cod_prod_fornitore, ref long fl_misura, ref string fs_errore);string ls_messaggio, ls_misura
long ll_pos, ll_pos1

ls_messaggio = "(Prodotto: "+fs_cod_prodotto+" - Fornitore: "+ fs_cod_fornitore+") "

select cod_prod_fornitore
into :fs_cod_prod_fornitore
from tab_prod_fornitori
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto and
			cod_fornitore=:fs_cod_fornitore;

if sqlca.sqlcode<0 then
	fs_errore="Errore in ricerca Codice Prodotto-Fornitore in tab_prod_fornitori "+ls_messaggio+": "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore="Codice Prodotto-Fornitore non trovato in tab_prod_fornitori "+ls_messaggio
	return -1
end if

//prova con 'L='
ll_pos = pos(fs_cod_prod_fornitore, "L=")

if ll_pos>0 then
	ll_pos += 2
else
	//prova con 'L ='
	ll_pos1 = pos(fs_cod_prod_fornitore, "L =")
	
	if ll_pos1>0 then
		ll_pos = ll_pos1 + 3
	else
		fs_errore="Impossibile trovare 'L=' (o 'L =') in codice articolo fornitore "+ls_messaggio
		return -1
	end if
end if

//se arrivi fin qui prosegui ad estrarre il valore numerico (cerca il carattere SPAZIO successivo o fine stringa)
ll_pos1 = pos(fs_cod_prod_fornitore, " ", ll_pos)
if ll_pos1>0 then
	//spazio trovato, estrai
	ls_misura = mid(fs_cod_prod_fornitore, ll_pos, ll_pos1 - ll_pos + 1)
else
	//spazio non trovato, vai a fine stringa ed estrai
	ls_misura = mid(fs_cod_prod_fornitore, ll_pos, 200)
end if

//controlla se numerico
if isnumber(ls_misura) then
	fl_misura = long(ls_misura)
	
	if fl_misura=0 then
		fs_errore="Valore nullo corrispondente ad L= : ("+ls_misura+")  in codice articolo fornitore "+ls_messaggio
		return -1
	end if
	
else
	if isnull(ls_misura) then ls_misura=""
	fs_errore="Non è stato possibile estrarre il valore corrispondente ad L= : ("+ls_misura+")  in codice articolo fornitore "+ls_messaggio
	return -1
end if

return 1
end function

public function integer wf_leggi_cod_prod_fornitore_2 (string fs_cod_prodotto, string fs_cod_fornitore, ref string fs_cod_prod_fornitore_2, ref string fs_errore);string ls_cod_prod_fornitore


select cod_prod_fornitore, cod_prod_fornitore_2
into :ls_cod_prod_fornitore, :fs_cod_prod_fornitore_2
from tab_prod_fornitori
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_prodotto=:fs_cod_prodotto and
		cod_fornitore=:fs_cod_fornitore;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura da tab_prod_fornitori, (Fornitore:"+fs_cod_fornitore+" - Prodotto:"+fs_cod_prodotto+"): "+sqlca.sqlerrtext
	return -1
end if
		
if isnull(fs_cod_prod_fornitore_2) or fs_cod_prod_fornitore_2="" then
	////torna cod_prod_fornitore
	//fs_cod_prod_fornitore_2 = ls_cod_prod_fornitore
	
	//segnala ed interrompi l'esportazione
	fs_errore = "Per il Fornitore:"+fs_cod_fornitore+" e Prodotto:"+fs_cod_prodotto+" manca la transcodifica. Impossibile esportare. Contattare Ufficio Acquisti!"
	return -1
end if

//if isnull(fs_cod_prod_fornitore_2) then fs_cod_prod_fornitore_2=""

return 1
end function

public function integer wf_chiedi_file (ref string fs_path, long fl_anno_reg, long fl_num_reg, string fs_estensione, string fs_prefisso_file, ref string fs_errore);string ls_filtro, ls_doc_folder, ls_file
integer li_FileNum

choose case fs_estensione
	case "txt"
		ls_filtro = "Files Testo"
	
	case "csv"
		ls_filtro = "Files CSV"
		
	case "xls"
		ls_filtro = "Files Excel"
		
	case else
		fs_errore = "Estensione file non consentita: "+fs_estensione
		return -1
		
end choose


ls_doc_folder = guo_functions.uof_get_user_documents_folder( )

//tolgo "/" finale, se c'è
if right(ls_doc_folder, 1) = "/" or right(ls_doc_folder, 1)="\" then ls_doc_folder = left( ls_doc_folder, len(ls_doc_folder) - 1 )

fs_path = ""
if fs_prefisso_file<>"" and not isnull(fs_prefisso_file) then
	fs_path += fs_prefisso_file
end if

fs_path += string(fl_anno_reg) + right("000000"+string(fl_num_reg) ,6 ) + "."+fs_estensione
//fs_path = string(fl_anno_reg) + right("000000"+string(fl_num_reg) ,6 ) + "."+fs_estensione

li_FileNum = GetFileSaveName ( "Seleziona File", fs_path, ls_file, fs_estensione, ls_filtro + " (*."+fs_estensione+"),*."+fs_estensione , ls_doc_folder)

if li_FileNum = 1 Then
else
	fs_errore = "Operazione annullata!" 
	return -1
end If

return 1
end function

public function integer wf_converti_in_pezzi (integer fi_anno, long fl_numero, long fl_riga, ref decimal fd_quantita, ref string fs_errore);//converte la quantita ordine in PEZZI

string ls_um, ls_riga, ls_cod_prodotto, ls_cod_fornitore, ls_cod_prodotto_fornitore
long ll_misura
dec{4} ld_misura

ls_riga = string(fi_anno)+"/"+string(fl_numero)+"/"+string(fl_riga)

ls_cod_prodotto =  f_des_tabella("det_ord_acq","anno_registrazione=" +  string(fi_anno) +" and "+&
																		"num_registrazione="+string(fl_numero)+" and "+&
																		"prog_riga_ordine_acq="+string(fl_riga), "cod_prodotto")

//la quan_ordine è sempre in unita misura mag
fd_quantita = f_dec_tabella("det_ord_acq","anno_registrazione=" +  string(fi_anno) +" and "+&
														"num_registrazione="+string(fl_numero)+" and "+&
														"prog_riga_ordine_acq="+string(fl_riga), "quan_ordinata")
														
ls_um = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "cod_misura_mag")


choose case ls_um
	case "PZ"
		//torna la quan_ordine che è già in PZ
		return 0
		
	case "ML"
		
		ls_cod_fornitore = f_des_tabella("tes_ord_acq","anno_registrazione=" +  string(fi_anno) +" and num_registrazione="+string(fl_numero), "cod_fornitore")
		
		if wf_tab_prod_fornitori(ls_cod_prodotto, ls_cod_fornitore, ls_cod_prodotto_fornitore, ll_misura, fs_errore) < 0 then
			return -1
		end if
		
		//in ll_misura la misura in CM di un singolo pezzo
		//esprimo in M (metri)
		ld_misura = ll_misura / 100
		
		fd_quantita = fd_quantita / ld_misura
		
		return 0
		
	case else
		fs_errore = ls_riga + ": Non è stato possibile esprimere la quantità otdine in pezzi o barre!"
		return -1
		
end choose

end function

public function integer wf_destinazione (string fs_cod_deposito, string fs_cod_deposito_trasf, string fs_rag_soc_1, string fs_indirizzo, string fs_cap, string fs_localita, string fs_provincia, ref string fs_riga);string ls_riga, ls_deposito, ls_rag_soc_1, ls_indirizzo, ls_cap, ls_localita, ls_provincia

//se c'è il deposito trasferiemnto metti quello, altrimenti lascia il deposito di testata
if not isnull(fs_cod_deposito_trasf) and fs_cod_deposito_trasf<> "" then
	ls_deposito = fs_cod_deposito_trasf
	
elseif not isnull(fs_cod_deposito) and fs_cod_deposito <> "" then
	ls_deposito = fs_cod_deposito
	
else
	ls_deposito = ""
end if


if ls_deposito<>"" then
	//leggo da anag_deposito per il deposito corrispondente
	ls_rag_soc_1 = f_des_tabella("anag_depositi","cod_deposito = '" +  ls_deposito +"'", "des_deposito")
	ls_indirizzo = f_des_tabella("anag_depositi","cod_deposito = '" +  ls_deposito +"'", "indirizzo")
	ls_cap = f_des_tabella("anag_depositi","cod_deposito = '" +  ls_deposito +"'", "cap")
	ls_localita = f_des_tabella("anag_depositi","cod_deposito = '" +  ls_deposito +"'", "localita")
	ls_provincia = f_des_tabella("anag_depositi","cod_deposito = '" +  ls_deposito +"'", "provincia")
	
//else
//	//prendi quelli della destinazione testata
//	ls_deposito = ""
//	ls_rag_soc_1 = fs_rag_soc_1
//	ls_indirizzo = fs_indirizzo
//	ls_cap = fs_cap
//	ls_localita = fs_localita
//	ls_provincia = fs_provincia
end if

//prepara la riga
if isnull(ls_deposito) then ls_deposito=""
if isnull(ls_rag_soc_1) then ls_rag_soc_1=""
if isnull(ls_indirizzo) then ls_indirizzo=""
if isnull(ls_cap) then ls_cap=""
if isnull(ls_localita) then ls_localita=""
if isnull(ls_provincia) then ls_provincia=""
	
//Destinazione_Merce											279	14		alfanumerico
fs_riga += wf_prepara_campo(ls_deposito, 14)
//RagSoc_Destinazione										293	40		alfanumerico
fs_riga += wf_prepara_campo(ls_rag_soc_1, 40)
//Indirizzo_Destinazione										333	40		alfanumerico
fs_riga += wf_prepara_campo(ls_indirizzo, 40)
//CAP_Destinazione											373	8		alfanumerico
fs_riga += wf_prepara_campo(ls_cap, 8)
//Località_Destinazione										381	40		alfanumerico
fs_riga += wf_prepara_campo(ls_localita, 40)
//Provincia_Destinazione										421	2		alfanumerico
fs_riga += wf_prepara_campo(ls_provincia, 2)

return 0
end function

public function integer wf_reimposta_data_consegna (long al_row, integer ai_modalita_cambio, ref string as_errore);
integer			li_anno
long				ll_numero, ll_cont_righe
string				ls_msg_conferma
datetime			ldt_data_consegna, ldt_data_consegna_fornitore

as_errore = ""
if ai_modalita_cambio = 0 then return 0

if al_row > 0 then
else
	return 0
end if

li_anno = tab_dettaglio.det_1.dw_1.getitemnumber( al_row, "anno_registrazione")
ll_numero = tab_dettaglio.det_1.dw_1.getitemnumber( al_row, "num_registrazione")

ll_cont_righe = 0

select count(*)
into   :ll_cont_righe
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :li_anno and
		 num_registrazione = :ll_numero;


//se non ci sono righe da allineare allora non fare null'altro
if ll_cont_righe<=0 or isnull(ll_cont_righe) then return 0
	
if ai_modalita_cambio = 1 then
	//hai cambiato solo la data consegna
	ls_msg_conferma = "Data Consegna ordine modificata. Si desidera aggiornare la Data Consegna anche su tutte le righe presenti nell'ordine?"
elseif ai_modalita_cambio = 2 then
	//hai cambiato solo la data consegna fornitore
	ls_msg_conferma = "Data Consegna Fornitore modificata. Si desidera aggiornare la Data Consegna Fornitore anche su tutte le righe presenti nell'ordine?"
else
	//hai cambiato entrambe
	ls_msg_conferma = "Data Consegna ordine e Data Consegna Fornitore modificate. Si desidera aggiornare le rispettive date su tutte le righe presenti nell'ordine?"
end if


if not g_mb.confirm(ls_msg_conferma) then return 0
		
ldt_data_consegna = tab_dettaglio.det_1.dw_1.getitemdatetime(al_row, "data_consegna")
ldt_data_consegna_fornitore = tab_dettaglio.det_1.dw_1.getitemdatetime(al_row, "data_consegna_fornitore")

if ai_modalita_cambio=1 then
	//hai cambiato solo la data consegna
	update  det_ord_acq
	set     data_consegna = :ldt_data_consegna
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :li_anno and
			  num_registrazione = :ll_numero;
	
elseif  ai_modalita_cambio = 2 then
	//hai cambiato solo la data consegna fornitore
	update  det_ord_acq
	set    data_consegna_fornitore = :ldt_data_consegna_fornitore
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :li_anno and
			  num_registrazione = :ll_numero;
	
else
	//hai cambiato entrambe
	update  det_ord_acq
	set     data_consegna = :ldt_data_consegna,
			data_consegna_fornitore = :ldt_data_consegna_fornitore
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :li_anno and
			  num_registrazione = :ll_numero;
end if

		
if sqlca.sqlcode < 0 then
	as_errore = "Errore in aggiornamento date consegna su righe ordine :" + sqlca.sqlerrtext
	return -1
end if
	
return 0


end function

on w_tes_ord_acq.create
int iCurrent
call super::create
end on

on w_tes_ord_acq.destroy
call super::destroy
end on

event pc_setddlb;call super::pc_setddlb;// Rircerca
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_tipo_deposito='I'")
					  
//f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
//	"cod_deposito_trasf", &
//	sqlca, &
//	"anag_depositi", &
//	"cod_deposito", &
//	"des_deposito", &
//	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_tipo_deposito='T'")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_deposito_trasf", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_tipo_ord_acq", &
	sqlca, &
	"tab_tipi_ord_acq", &
	"cod_tipo_ord_acq", &
	"des_tipo_ord_acq", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
// DW
// Rircerca
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_tipo_deposito='I'")
					  
//f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
//                 "cod_deposito_trasf", &
//                 sqlca, &
//                 "anag_depositi", &
//                 "cod_deposito", &
//                 "des_deposito", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_tipo_deposito='T'")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_deposito_trasf", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


string ls_select_operatori
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
		s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
		s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
		s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_operatore", &
	sqlca, &
	"tab_operatori", &
	"cod_operatore", &
	"des_operatore", &
	ls_select_operatori)
	
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_tipo_ord_acq", &
	sqlca, &
	"tab_tipi_ord_acq", &
	"cod_tipo_ord_acq", &
	"des_tipo_ord_acq", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_valuta", &
	sqlca, &
	"tab_valute", &
	"cod_valuta", &
	"des_valuta", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_tipo_listino_prodotto", &
	sqlca, &
	"tab_tipi_listini_prodotti", &
	"cod_tipo_listino_prodotto", &
	"des_tipo_listino_prodotto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_pagamento", &
	sqlca, &
	"tab_pagamenti", &
	"cod_pagamento", &
	"des_pagamento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_banca_clien_for", &
	sqlca, &
	"anag_banche_clien_for", &
	"cod_banca_clien_for", &
	"des_banca", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_banca", &
	sqlca, &
	"anag_banche", &
	"cod_banca", &
	"des_banca", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_imballo", &
	sqlca, &
	"tab_imballi", &
	"cod_imballo", &
	"des_imballo", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_vettore", &
	sqlca, &
	"anag_vettori", &
	"cod_vettore", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_inoltro", &
	sqlca, &
	"anag_vettori", &
	"cod_vettore", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_mezzo", &
	sqlca, &
	"tab_mezzi", &
	"cod_mezzo", &
	"des_mezzo", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_porto", &
	sqlca, &
	"tab_porti", &
	"cod_porto", &
	"des_porto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_resa", &
	sqlca, &
	"tab_rese", &
	"cod_resa", &
	"des_resa", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_natura_intra", &
	sqlca, &
	"tab_natura_intra", &
	"cod_natura_intra", &
	"descrizione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	

	
end event

event pc_setwindow;call super::pc_setwindow;string				ls_wizard, ls_prova, ls_postit, ls_file,ls_expression

is_codice_filtro = "ORA"

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_3.dw_3.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_4.dw_4.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true
tab_dettaglio.det_3.dw_3.ib_dw_detail = true
tab_dettaglio.det_4.dw_4.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1


tab_dettaglio.det_1.dw_documenti.uof_set_management("TESORDACQ", tab_dettaglio.det_1.dw_1)
tab_dettaglio.det_1.dw_documenti.settransobject(sqlca)

//Distinzione tra Post-it e file
ls_postit		= s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_giallo_p.png"
ls_file			= s_cs_xx.volume + s_cs_xx.risorse + "treeview\table.png"
ls_expression = "case (flag_postit when 'S' then '"+ls_postit+"' else '"+ls_file+"') "
ls_expression = "bitmap("+ls_expression+")"
tab_dettaglio.det_1.dw_documenti.object.cf_bitmap.expression = ls_expression

tab_dettaglio.det_1.dw_documenti.uof_enabled_delete_blob()

il_livello = 0

//verifico se posso esportare in excel secondo gruppo gibus) -----------------------------
select flag
into   :ls_prova
from   parametri_azienda
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='NDA' and flag_parametro='F';

if sqlca.sqlcode < 0 or isnull(ls_prova) or ls_prova="" or (ls_prova<>"N" and ls_prova<>"S") then
	 ib_esporta_gibus = false
else
	 ib_esporta_gibus = true
end if
end event

event pc_delete;call super::pc_delete;integer li_i


for li_i = 1 to tab_dettaglio.det_1.dw_1.deletedcount()
	if tab_dettaglio.det_1.dw_1.getitemstring(li_i, "flag_evasione", delete!, true) <> "A" then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! Ha già subito un'evasione.", exclamation!, ok!)
	   tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if

	if tab_dettaglio.det_1.dw_1.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! E' stato bloccato.", exclamation!, ok!)
	   tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
next


tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.enabled = false
tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.enabled = false
end event

event pc_modify;call super::pc_modify;if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", exclamation!, ok!)
   tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_tes_ord_acq
integer width = 3333
integer height = 2264
det_2 det_2
det_3 det_3
det_4 det_4
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
end on

event tab_dettaglio::ue_resize;call super::ue_resize;

// ridimensiono TV

tab_dettaglio.det_1.dw_documenti.resize(tab_dettaglio.det_1.dw_1.width, tab_dettaglio.det_1.height - tab_dettaglio.det_1.dw_1.height - 70)

end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3296
integer height = 2140
string text = "Testata"
dw_documenti dw_documenti
dw_1 dw_1
end type

on det_1.create
this.dw_documenti=create dw_documenti
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_documenti
this.Control[iCurrent+2]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_documenti)
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_tes_ord_acq
integer height = 2304
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer height = 2180
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer height = 2132
string dataobject = "d_tes_ord_acq_ricerca_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_operatore"
		guo_ricerca.uof_ricerca_operatore(dw_ricerca, "cod_operatore")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca, "cod_fornitore")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer height = 2180
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
end type

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::rightclicked;call super::rightclicked;long ll_row
treeviewitem ltvi_item
str_treeview lstr_data
m_ordini_acquisto lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row = tab_dettaglio.det_1.dw_1.getrow()

//tab_ricerca.selezione.tv_selezione.selectitem(handle)
tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then

	lm_menu = create m_ordini_acquisto
	
	lm_menu.m_salda.enabled =  (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_evasione") <> "E")
	lm_menu.m_blocca.enabled = (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_blocco") = "N")
	lm_menu.m_sblocca.enabled = (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_blocco") = "S")
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

event tv_selezione::doubleclicked;call super::doubleclicked;if not wf_is_valid(handle) then return -1

tab_ricerca.selezione.tv_selezione.selectitem(handle)

getwindow().postevent("pc_menu_dettagli")
end event

event tv_selezione::key;call super::key;if key = KeyX! and keyflags=3 and s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	if g_mb.confirm("Ricalcolare tutte gli ordini nella lista?") then

		long	ll_handle
		string ls_messaggio
		treeviewitem ltv_item
		str_treeview lws_record
		uo_calcola_documento_euro luo_calcola_documento_euro
		
		ll_handle = tv_selezione.FindItem(CurrentTreeItem!, 0)
		
		setpointer(HourGlass!)
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tv_selezione.getitem(ll_handle, ltv_item)
			
			lws_record = ltv_item.data
			
			if lws_record.decimale[1] > 0 and lws_record.decimale[2] > 0 then
			
				luo_calcola_documento_euro = create uo_calcola_documento_euro
				
				luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
				
				w_cs_xx_mdi.setmicrohelp("Calcolo Ordine Acq. " + string(lws_record.decimale[1]) + "-"  + string(lws_record.decimale[2]))
				Yield()
				
				if luo_calcola_documento_euro.uof_calcola_documento(lws_record.decimale[1], lws_record.decimale[2],"ord_acq",ls_messaggio) <> 0 then
					if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
					rollback;
				else
					commit;
				end if
				destroy luo_calcola_documento_euro
			end if
			
			ll_handle = tv_selezione.finditem( NextTreeItem! ,ll_handle )

		loop
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if

end if
end event

type dw_documenti from uo_dw_drag_doc_acq_ven within det_1
integer x = 18
integer y = 1532
integer width = 3278
integer height = 604
integer taborder = 40
string dataobject = "d_tes_ord_acq_note"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_1 from uo_cs_xx_dw within det_1
integer x = 18
integer y = 28
integer width = 3278
integer height = 1488
integer taborder = 30
string dataobject = "d_tes_ord_acq_det_1"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if


if ll_errore > 0 then
	//retrieve docs
	dw_documenti.uof_retrieve_blob(tab_dettaglio.det_1.dw_1.getrow())
end if

change_dw_current()
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(dw_1,"cod_banca_clien_for")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_1,"cod_fornitore")
		
end choose
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_null, ls_cod_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, &
          	ls_fax, ls_telex, ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
          	ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_testata, ls_nota_piede, &
			ls_nota_video, ls_cod_banca, ls_cod_tipo_ord_acq, ls_cod_tipo_det_acq, ls_cod_prodotto, ls_des_prodotto,ls_errore, ls_flag_tipo_deposito, ls_messaggio
	integer	li_ret
	double ld_sconto
	datetime ldt_data_registrazione, ldt_data_consegna
	long 	ll_anno_registrazione, ll_num_registrazione, ll_num_righe_dettagli, ll_prog_riga_ordine_acq,ll_cont_righe
	uo_fido_cliente luo_fido_cliente
	
	declare	cu_det_ord_acq cursor for 
	select		prog_riga_ordine_acq, cod_prodotto
	from		det_ord_acq
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione
	order by 	prog_riga_ordine_acq;


   setnull(ls_null)

   choose case i_colname
		case "cod_operatore"
			if len(data) > 0 then
				guo_functions.uof_get_stabilimento_operatore(data, ls_cod_deposito, ls_errore)
				if len(ls_cod_deposito) > 0 then setitem(row, "cod_deposito", ls_cod_deposito)
			end if

      case "data_registrazione"
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         f_cambio_acq(ls_cod_valuta, datetime(date(i_coltext)))
			
      case "cod_fornitore"
		ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
		luo_fido_cliente = create uo_fido_cliente
		li_ret = luo_fido_cliente.uof_get_blocco_fornitore( data, ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			this.setitem(i_rownbr, i_colname, ls_null)
			this.SetColumn ( i_colname )
			destroy luo_fido_cliente
			return 2
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		destroy luo_fido_cliente
			
         select anag_fornitori.rag_soc_1,
                anag_fornitori.rag_soc_2,
                anag_fornitori.indirizzo,
                anag_fornitori.frazione,
                anag_fornitori.cap,
                anag_fornitori.localita,
                anag_fornitori.provincia,
                anag_fornitori.telefono,
                anag_fornitori.fax,
                anag_fornitori.telex,
                anag_fornitori.partita_iva,
                anag_fornitori.cod_fiscale,
                anag_fornitori.cod_deposito,
                anag_fornitori.cod_valuta,
                anag_fornitori.cod_tipo_listino_prodotto,
                anag_fornitori.cod_pagamento,
                anag_fornitori.sconto,
                anag_fornitori.cod_banca_clien_for,
                anag_fornitori.cod_banca,
                anag_fornitori.cod_imballo,
                anag_fornitori.cod_vettore,
                anag_fornitori.cod_inoltro,
                anag_fornitori.cod_mezzo,
                anag_fornitori.cod_porto,
                anag_fornitori.cod_resa
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale,
                :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for,
                :ls_cod_banca,
                :ls_cod_imballo,
                :ls_cod_vettore,
                :ls_cod_inoltro,
                :ls_cod_mezzo,
                :ls_cod_porto,
                :ls_cod_resa
         from   anag_fornitori
         where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fornitori.cod_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            this.setitem(i_rownbr, "cod_banca", ls_cod_banca)
            if not isnull(i_coltext) then
               tab_dettaglio.det_2.dw_2.modify("st_cod_fornitore.text='" + i_coltext + "'")
            else
               tab_dettaglio.det_2.dw_2.modify("st_cod_fornitore.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
                tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
                tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
                tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
                tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
                tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
                tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
                tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
                tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
            end if
            if not isnull(ls_partita_iva) then
                tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
                tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
            end if
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_imballo", ls_cod_imballo)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_vettore", ls_cod_vettore)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_inoltro", ls_cod_inoltro)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_mezzo", ls_cod_mezzo)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_porto", ls_cod_porto)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_resa", ls_cod_resa)
   
            f_po_loaddddw_dw(this, &
                             "cod_des_fornitore", &
                             sqlca, &
                             "anag_des_fornitori", &
                             "cod_des_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)

            f_po_loaddddw_dw(this, &
                             "cod_fil_fornitore", &
                             sqlca, &
                             "anag_fil_fornitori", &
                             "cod_fil_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)

            ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_acq(ls_cod_valuta, ldt_data_registrazione)
            
         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_banca", ls_null)
             tab_dettaglio.det_2.dw_2.modify("st_cod_fornitore.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
             tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_imballo", ls_null)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_vettore", ls_null)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_inoltro", ls_null)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_mezzo", ls_null)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_porto", ls_null)
             tab_dettaglio.det_3.dw_3.setitem(i_rownbr, "cod_resa", ls_null)
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)
				return 1
         end if 
			setnull(ls_null)
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			
			if guo_functions.uof_get_note_fisse(ls_null, data, ls_null, "ORD_ACQ", ls_null, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if

		case "cod_fil_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")

         select anag_fil_fornitori.rag_soc_1,
                anag_fil_fornitori.rag_soc_2,
                anag_fil_fornitori.indirizzo,
                anag_fil_fornitori.frazione,
                anag_fil_fornitori.cap,
                anag_fil_fornitori.localita,
                anag_fil_fornitori.provincia,
                anag_fil_fornitori.telefono,
                anag_fil_fornitori.fax,
                anag_fil_fornitori.telex
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex
         from   anag_fil_fornitori
         where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and 
					 anag_fil_fornitori.cod_fil_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            if not isnull(ls_rag_soc_1) then
                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
                tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
                tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
                tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
                tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
                tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
                tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
                tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
                tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
            else
                tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
            end if
			else
	         select anag_fornitori.rag_soc_1,
   	             anag_fornitori.rag_soc_2,
      	          anag_fornitori.indirizzo,
	                anag_fornitori.frazione,
   	             anag_fornitori.cap,
      	          anag_fornitori.localita,
         	       anag_fornitori.provincia,
            	    anag_fornitori.telefono,
	                anag_fornitori.fax,
	                anag_fornitori.telex
	         into   :ls_rag_soc_1,    
	                :ls_rag_soc_2,
	                :ls_indirizzo,    
	                :ls_frazione,
	                :ls_cap,
	                :ls_localita,
	                :ls_provincia,
	                :ls_telefono,
	                :ls_fax,
	                :ls_telex
	         from   anag_fornitori
	         where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
	                anag_fornitori.cod_fornitore = :ls_cod_fornitore;
 
	         if sqlca.sqlcode = 0 then 
	            if not isnull(ls_rag_soc_1) then
	                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
	            end if
	            if not isnull(ls_rag_soc_2) then
	                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
	            end if
	            if not isnull(ls_indirizzo) then
	                tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
	            end if
	            if not isnull(ls_frazione) then
	                tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
	            end if
	            if not isnull(ls_cap) then
	                tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
	            end if
	            if not isnull(ls_localita) then
	                tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
	            end if
	            if not isnull(ls_provincia) then
	                tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
	            end if
	            if not isnull(ls_telefono) then
	                tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
	            end if
	            if not isnull(ls_fax) then
	                tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
	            end if
	            if not isnull(ls_telex) then
	                tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
	            else
	                tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
	            end if
	         else
	             tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
	             tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
				end if 
         end if
      case "cod_des_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
         select anag_des_fornitori.rag_soc_1,
                anag_des_fornitori.rag_soc_2,
                anag_des_fornitori.indirizzo,
                anag_des_fornitori.frazione,
                anag_des_fornitori.cap,
                anag_des_fornitori.localita,
                anag_des_fornitori.provincia
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia
         from   anag_des_fornitori
         where  anag_des_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_des_fornitori.cod_fornitore = :ls_cod_fornitore and
                anag_des_fornitori.cod_des_fornitore = :i_coltext;
   
         if sqlca.sqlcode = 0 then
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "rag_soc_1", ls_rag_soc_1)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "rag_soc_2", ls_rag_soc_2)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "indirizzo", ls_indirizzo)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "frazione", ls_frazione)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "cap", ls_cap)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "localita", ls_localita)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "provincia", ls_provincia)
         else
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "rag_soc_1", ls_null)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "rag_soc_2", ls_null)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "indirizzo", ls_null)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "frazione", ls_null)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "cap", ls_null)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "localita", ls_null)
             tab_dettaglio.det_2.dw_2.setitem(i_rownbr, "provincia", ls_null)
         end if
      case "cod_valuta"
         ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
         f_cambio_acq(i_coltext, ldt_data_registrazione)
      case "data_consegna"
			this.Accepttext()
         ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna_fornitore")) then
				if g_mb.messagebox("Ordini di Acquisto", "Aggiornare la Data di Consegna Fornitore?", &
								question!,yesno!) = 1 then this.setitem(i_rownbr, "data_consegna_fornitore", ldt_data_consegna)
			else
				this.setitem(i_rownbr, "data_consegna_fornitore", ldt_data_consegna)
			end if
			
		// Daniele 14, 28 Apr 2008
		case "cod_tipo_ord_acq"
			ls_cod_tipo_ord_acq = data
			if len(trim(ls_cod_tipo_ord_acq)) > 0 and not isnull(ls_cod_tipo_ord_acq) then
				select cod_tipo_det_acq
				into   :ls_cod_tipo_det_acq 
				from   tab_tipi_ord_acq
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Ordini di Acquisto", "Errore in ricerca tipo dettaglio acquisti~r~n" + sqlca.sqlerrtext)
					return 2
				end if
				
				ll_anno_registrazione = this.getitemnumber( i_rownbr, "anno_registrazione")
				ll_num_registrazione = this.getitemnumber( i_rownbr, "num_registrazione")
	
				// Ha  fatto un nuovo ordine o uno esistente?
				ll_num_righe_dettagli = 0
				select count (*)
				into :ll_num_righe_dettagli
				from det_ord_acq 
				where cod_azienda = :s_cs_xx.cod_azienda and 
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione;

				if sqlca.sqlcode <> 0 then
					messagebox("Ordini di Acquisto", "Errore nel conteggio righe dettagli acquisto~r~n" + sqlca.sqlerrtext)
					return 2
				end if
				
				
				if ll_num_righe_dettagli > 0 then // l'ordine che sto trattando non è nuovo
					
					open cu_det_ord_acq;
					
					if sqlca.sqlcode = -1 then
						messagebox("Ordini di Acquisto", "Errore nell'aggiornamento tipi dettaglio acquisto~r~n" + sqlca.sqlerrtext)
					end if
					
					do while true
						fetch cu_det_ord_acq into :ll_prog_riga_ordine_acq, :ls_cod_prodotto ;
						if sqlca.sqlcode = -1 then
							messagebox("Ordini di Acquisto", "Errore del fetch~r~n" + sqlca.sqlerrtext)
						end if
						if sqlca.sqlcode = 100 then // ho finito
							exit
						end if
						select des_prodotto
						into :ls_des_prodotto
						from anag_prodotti
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_prodotto = :ls_cod_prodotto;
						
						if sqlca.sqlcode < 0 then
							messagebox("Ordini di Acquisto", "Errore della lettura della descrizione del prodotto~r~n" + sqlca.sqlerrtext)							
						end if
						
						if g_mb.messagebox("Ordini di Acquisto",  "Vuoi impostare il tipo dettaglio predefinito sulla righe di dettaglio per il prodotto?~r~n" + &
													"Codice prodotto: " + string(ls_cod_prodotto) + "~r~n"+ &
													"Descrizione prodotto: " + ls_des_prodotto, &
											question!, yesno!) = 1 then
							// sql di aggiornamento
							update det_ord_acq 
							set cod_tipo_det_acq = :ls_cod_tipo_det_acq
							where cod_azienda = :s_cs_xx.cod_azienda and 
									anno_registrazione = :ll_anno_registrazione and
									num_registrazione = :ll_num_registrazione and
									prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;
							if sqlca.sqlcode <> 0 then
								messagebox("Ordini di Acquisto", "Errore nell'aggiornamento tipi dettaglio acquisto~r~n" + sqlca.sqlerrtext)
								return 2
							end if
						end if
					loop
					
					close cu_det_ord_acq ;
				
				end if				
				
				if ll_num_righe_dettagli > 0 then
					triggerevent("pcd_save")
				end if
			else
				g_mb.messagebox("Ordini di Acquisto", "Attenzione, è stato impostato un valore nullo: inserire un valore.")
			end if			
		// Fine Modifica Daniele 14 Apr 2008
		
		// stefanop 19/12/2012
		case "cod_deposito_trasf"
			if not isnull(data) and data <> "" then
				
				select flag_tipo_deposito
				into :ls_flag_tipo_deposito
				from anag_depositi
				where cod_azienda = :s_cs_xx.cod_azienda and
						 cod_deposito = :data;
						 
					
				if ls_flag_tipo_deposito <> "T" then
					
					g_mb.warning("Attenzione: si è selezionato un deposito di trasferimento che non è impostato come trasferimento")
					
				end if
				
			end if

	end choose
end if
end event

event pcd_modify;call super::pcd_modify;object.b_ricerca_banca_for.enabled = true
object.b_ricerca_fornitore.enabled = true
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_ord_acq, ls_cod_operatore, ls_stringa, ls_cod_deposito, ls_errore

   select con_ord_acq.cod_tipo_ord_acq
   into   :ls_cod_tipo_ord_acq
   from   con_ord_acq
   where  con_ord_acq.cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_tipo_ord_acq", ls_cod_tipo_ord_acq)
		this.setitem(this.getrow(), "data_registrazione", datetime(today()))
	end if

	select cod_operatore
	  into :ls_cod_operatore
	  from tab_operatori_utenti
	 where cod_azienda = :s_cs_xx.cod_azienda and
	 		 cod_utente = :s_cs_xx.cod_utente and
			 flag_default = 'S';
			 
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
		pcca.error = c_fatal
		return
	elseif sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
	end if
	
	// banca per RIBA - richiesto da colombin / meco, ma utile per tutti
	select stringa
	into   :ls_stringa
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'BA1';
	if sqlca.sqlcode = 0 then 
		this.setitem(this.getrow(), "cod_banca_clien_for", ls_stringa)
	end if
			 
	// banca per B.B. - richiesto da colombin / meco, ma utile per tutti
	select stringa
	into   :ls_stringa
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'BA2';
	if sqlca.sqlcode = 0 then 
		this.setitem(this.getrow(), "cod_banca", ls_stringa)
	end if
			 
   
	object.b_ricerca_banca_for.enabled =true
	object.b_ricerca_fornitore.enabled = true
			
	ib_new = true
	
	tab_dettaglio.det_4.dw_4.setitem(getrow(), "flag_doc_suc_tes", "I")
	tab_dettaglio.det_4.dw_4.setitem(getrow(), "flag_st_note_tes", "I")
	tab_dettaglio.det_4.dw_4.setitem(getrow(), "flag_doc_suc_pie", "I")
	tab_dettaglio.det_4.dw_4.setitem(getrow(), "flag_st_note_pie", "I")
	
	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
	if not isnull(ls_cod_deposito) then tab_dettaglio.det_1.dw_1.setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----
	
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	if pcca.error = c_success then
		object.b_ricerca_banca_for.enabled = false
		object.b_ricerca_fornitore.enabled = false	
		
		ib_new = false
	end if
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if this.getitemnumber(ll_i, "anno_registrazione") = 0 or isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
		ll_anno_registrazione = f_anno_esercizio()
      	if ll_anno_registrazione > 0 then
         	this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
      	else
			g_mb.messagebox("Ordini Fornitori","Impostare l'anno di esercizio in parametri aziendali")
		end if
		
		select con_ord_acq.num_registrazione
		into   :ll_num_registrazione
		from   con_ord_acq
		where  con_ord_acq.cod_azienda = :s_cs_xx.cod_azienda;
		
		choose case sqlca.sqlcode
			case 0
				ll_num_registrazione ++
			case 100
				g_mb.messagebox("Ordini acquisto","Errore in assegnazione numero ordine: verificare di aver impostato i parametri offerte di vendita. ~r~nIl salvataggio verrà effettuato ugualmente")
			case else
				g_mb.messagebox("Ordini acquisto","Errore in assegnazione numero ordine.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verrà effettuato ugualmente")
		end choose
		
		ll_max_registrazione = 0
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
				 
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= ll_num_registrazione then ll_num_registrazione = ll_max_registrazione + 1
		end if
			
		this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
		update con_ord_acq
		set    con_ord_acq.num_registrazione = :ll_num_registrazione
		where  con_ord_acq.cod_azienda = :s_cs_xx.cod_azienda;
	end if
next      
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	object.b_ricerca_banca_for.enabled = false
	object.b_ricerca_fornitore.enabled = false

	ib_new = false
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string		ls_cod_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, &
				ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
				ls_partita_iva, ls_cod_fiscale, ls_cod_fil_fornitore


	if this.getrow() > 0 then
		ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
		ls_cod_fil_fornitore = this.getitemstring(this.getrow(), "cod_fil_fornitore")

		select anag_fornitori.rag_soc_1,
				 anag_fornitori.rag_soc_2,
				 anag_fornitori.indirizzo,
				 anag_fornitori.frazione,
				 anag_fornitori.cap,
				 anag_fornitori.localita,
				 anag_fornitori.provincia,
				 anag_fornitori.telefono,
				 anag_fornitori.fax,
				 anag_fornitori.telex,
				 anag_fornitori.partita_iva,
				 anag_fornitori.cod_fiscale
		into   :ls_rag_soc_1,    
				 :ls_rag_soc_2,
				 :ls_indirizzo,    
				 :ls_frazione,
				 :ls_cap,
				 :ls_localita,
				 :ls_provincia,
				 :ls_telefono,
				 :ls_fax,
				 :ls_telex,
				 :ls_partita_iva,
				 :ls_cod_fiscale
		from   anag_fornitori
		where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_fornitori.cod_fornitore = :ls_cod_fornitore;

		if sqlca.sqlcode = 0 then
			if not isnull(ls_cod_fornitore) then
				tab_dettaglio.det_2.dw_2.modify("st_cod_fornitore.text='" + ls_cod_fornitore + "'")
			else
				ls_cod_fornitore = ""
				tab_dettaglio.det_2.dw_2.modify("st_cod_fornitore.text=''")
			end if
			
			if not isnull(ls_rag_soc_1) then
				tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
			end if
			
			if not isnull(ls_rag_soc_2) then
				tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
			end if
			
			if not isnull(ls_indirizzo) then
				tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
			end if
			
			if not isnull(ls_frazione) then
				tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
			end if
			
			if not isnull(ls_cap) then
				tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
			end if
			
			if not isnull(ls_localita) then
				tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
			end if
			
			if not isnull(ls_provincia) then
				tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
			end if
			
			if not isnull(ls_telefono) then
				tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
			end if
			
			if not isnull(ls_fax) then
				tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
			end if
			
			if not isnull(ls_telex) then
				tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
			end if
			
			if not isnull(ls_partita_iva) then
				tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
			end if
			
			if not isnull(ls_cod_fiscale) then
				tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
			else
				tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
			end if

			f_po_loaddddw_dw(this, &
								  "cod_des_fornitore", &
								  sqlca, &
								  "anag_des_fornitori", &
								  "cod_des_fornitore", &
								  "rag_soc_1", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			
			f_po_loaddddw_dw(this, &
								  "cod_fil_fornitore", &
								  sqlca, &
								  "anag_fil_fornitori", &
								  "cod_fil_fornitore", &
								  "rag_soc_1", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

			select anag_fil_fornitori.rag_soc_1,
					 anag_fil_fornitori.rag_soc_2,
					 anag_fil_fornitori.indirizzo,
					 anag_fil_fornitori.frazione,
					 anag_fil_fornitori.cap,
					 anag_fil_fornitori.localita,
					 anag_fil_fornitori.provincia,
					 anag_fil_fornitori.telefono,
					 anag_fil_fornitori.fax,
					 anag_fil_fornitori.telex
			into   :ls_rag_soc_1,    
					 :ls_rag_soc_2,
					 :ls_indirizzo,    
					 :ls_frazione,
					 :ls_cap,
					 :ls_localita,
					 :ls_provincia,
					 :ls_telefono,
					 :ls_fax,
					 :ls_telex
			from   anag_fil_fornitori
			where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and 
					 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;
 
			if sqlca.sqlcode = 0 then
				
				if not isnull(ls_rag_soc_1) then
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
				end if
				if not isnull(ls_rag_soc_2) then
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
				end if
				if not isnull(ls_indirizzo) then
					tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
				end if
				if not isnull(ls_frazione) then
					tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
				end if
				if not isnull(ls_cap) then
					tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
				end if
				if not isnull(ls_localita) then
					tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
				end if
				if not isnull(ls_provincia) then
					tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
				end if
				if not isnull(ls_telefono) then
					tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
				end if
				if not isnull(ls_fax) then
					tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
				end if
				if not isnull(ls_telex) then
					tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
				end if
				
			end if
		else
			tab_dettaglio.det_2.dw_2.modify("st_cod_fornitore.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
			tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
			
			tab_dettaglio.det_1.dw_documenti.uof_retrieve_blob( tab_dettaglio.det_1.dw_1.getrow())
			
		end if
	end if
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	string					ls_tabella, ls_sql_stringa, ls_cod_prodotto, ls_nome_prog, ls_cod_cliente, ls_cod_fornitore, ls_nota_testata, ls_nota_piede, ls_nota_old, ls_null, ls_nota_video, &
							ls_cod_tipo_ord_acq, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_errore
							
	datetime				ldt_data_registrazione,ldt_data_consegna, ldt_data_consegna_fornitore
	
	long					ll_anno_registrazione, ll_num_registrazione, ll_i,ll_cont_righe
	
	dec{4}				ld_quan_ordinata
	
	integer				li_modo_cambio_data
	

	
	// controllo righe cancellate ed eseguo eventuali cancellazioni di righe referenziate.
	for ll_i = 1 to this.deletedcount()
		ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione", delete!, true)
		ls_cod_tipo_ord_acq = this.getitemstring(ll_i, "cod_tipo_ord_acq", delete!, true)
		
		// stefanop 05/08/2010: cancello comunicazioni se necessario
		select cod_tipo_lista_dist, cod_lista_dist
		into :ls_cod_tipo_lista_dist, :ls_cod_lista_dist
		from tab_tipi_ord_acq
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
			
		if not isnull(ls_cod_tipo_lista_dist) and ls_cod_tipo_lista_dist <> "" and not isnull(ls_cod_lista_dist) and ls_cod_lista_dist <> "" then
			// controllo gestione fasi
			select flag_gestione_fasi
			into :ls_flag_gestione_fasi
			from tab_tipi_liste_dist
			where cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
			
			if not isnull(ls_flag_gestione_fasi) and ls_flag_gestione_fasi = "S" then
				// cancello comunicazioni
				delete from det_liste_dist_fasi_com
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					anno_ord_acq = :ll_anno_registrazione and
					num_ord_acq = :ll_num_registrazione;
					
				if sqlca.sqlcode = -1 then
					g_mb.error("APICE", "Errore durante la cancellazione delle comunicazioni relafive ai cicli di vita.~r~n"+sqlca.sqlerrtext)
					return 1
				end if
			end if
		end if
		// ----


		delete scad_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione scadenze." + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
		
		delete from iva_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante l'eliminazione delle righe iva. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if
		
		ls_tabella = "det_ord_acq_stat"
		ls_nome_prog = ""

	      if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione,ls_nome_prog,0) = -1 then
			return 1
		end if

      if f_riag_quan_ordinata(ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
				
		delete det_ord_acq_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione corrispondenze dettaglio ordine" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if

      ls_tabella = "det_ord_acq"
      if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then
		set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if
	
	//elimino nodo selezionato
	long ll_handle
	tab_ricerca.selezione.tv_selezione.findItem(CurrentTreeItem!, ll_handle)
	if ll_handle > 0 then tab_ricerca.selezione.tv_selezione.deleteitem(ll_handle)
     if DeletedCount() > 0 then
 		wf_set_deleted_item_status()
	end if
   next

	
	// stefanop: aggiungo l'icona all'albero
	if ib_new then
		wf_inserisci_ordine_acquisto(0, getitemnumber(getrow(), "anno_registrazione"), getitemnumber(getrow(), "num_registrazione"))
	end if
	// 
	
	// controllo le righe modificate ed eventuale impatto su tabelle collegate
	for ll_i = 1 to rowcount()
	
		li_modo_cambio_data = 0
		ls_errore = ""
	
		if getitemstatus( ll_i, "data_consegna", primary!) = DataModified! then
			li_modo_cambio_data += 1
		end if
		if getitemstatus( ll_i, "data_consegna_fornitore", primary!) = DataModified! then
			li_modo_cambio_data += 2
		end if
	
		if li_modo_cambio_data>0 then
			if wf_reimposta_data_consegna(ll_i, li_modo_cambio_data, ls_errore) < 0 then
				g_mb.error(ls_errore)
				return 1
			end if
		end if
		
		
		
		if this.getitemstatus(ll_i, 0, primary!) = NewModified!  then
			ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_registrazione")
			ll_num_registrazione = this.getitemnumber(this.getrow(), "num_registrazione")
			setnull(ls_null)
			ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			
			if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null, "ORD_ACQ", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
				if isnull(ls_nota_old) then ls_nota_old = ""
				this.setitem(this.getrow(), "nota_testata", ls_nota_testata + ls_nota_old )
				ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
				if isnull(ls_nota_old) then ls_nota_old = ""
				this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
			end if
			
			// stefanop 05/08/2010: controllo tipo ord_acq e ciclo di vita associato
			ls_cod_tipo_ord_acq = this.getitemstring(ll_i, "cod_tipo_ord_acq")
		
			select cod_tipo_lista_dist, cod_lista_dist
			into :ls_cod_tipo_lista_dist, :ls_cod_lista_dist
			from tab_tipi_ord_acq
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
			
			if not isnull(ls_cod_tipo_lista_dist) and ls_cod_tipo_lista_dist <> "" and not isnull(ls_cod_lista_dist) and ls_cod_lista_dist <> "" then
				setitem(ll_i, "flag_confermato", "N")
			else
				setitem(ll_i, "flag_confermato", "S")
			end if
			// ----
		end if
	next
	
	
	
	// altri controlli di coerenza
   if this.getrow() > 0 then
      ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_registrazione")
      ll_num_registrazione = this.getitemnumber(this.getrow(), "num_registrazione")
      if ll_anno_registrazione <> 0 then
         ls_tabella = "det_ord_acq"
			
			if getitemstring(getrow(), "flag_blocco") = "S" then
				g_mb.messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", exclamation!, ok!)
				set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
      end if
   end if
end if
end event

event ue_key;call super::ue_key;// richiesto da Paolo Brunazzetto

if keyflags = 2 then
	
	if key = KeyS! then
		wf_stampa(false)
		
	elseif key = KeyD! then
		getwindow().postevent("pc_menu_dettagli")
		
	end if
	
end if

end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3296
integer height = 2140
long backcolor = 12632256
string text = "Fornitori/Destinazione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer x = 9
integer y = 4
integer width = 2962
integer height = 1352
integer taborder = 11
string dataobject = "d_tes_ord_acq_det_2"
boolean border = false
end type

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3296
integer height = 2140
long backcolor = 12632256
string text = "Trasporto"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from uo_cs_xx_dw within det_3
integer x = 9
integer y = 16
integer width = 3040
integer height = 1088
integer taborder = 11
string dataobject = "d_tes_ord_acq_det_3"
boolean border = false
end type

type det_4 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3296
integer height = 2140
long backcolor = 12632256
string text = "Note"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_4 dw_4
end type

on det_4.create
this.dw_4=create dw_4
this.Control[]={this.dw_4}
end on

on det_4.destroy
destroy(this.dw_4)
end on

type dw_4 from uo_cs_xx_dw within det_4
event ue_key_press pbm_dwnkey
integer x = 5
integer y = 4
integer width = 3118
integer height = 1288
integer taborder = 11
string dataobject = "d_tes_ord_acq_det_4"
boolean border = false
end type

event ue_key_press;string ls_nota_fissa

choose case this.getcolumnname()
	
	case "nota_testata"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = "%"
			s_cs_xx.parametri.parametro_s_2 = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "S"
			s_cs_xx.parametri.parametro_s_9 = "T"
			s_cs_xx.parametri.parametro_data_1 = tab_dettaglio.det_1.dw_1.getitemdatetime(tab_dettaglio.det_1.dw_1.getrow(),"data_registrazione")
			
			
			change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then
					if len(this.gettext()) > 0 then
						this.setcolumn("nota_testata")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else
						this.setcolumn("nota_testata")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if
		
	case "nota_piede"
		if key = keyF1!  and keyflags = 1 then		
			s_cs_xx.parametri.parametro_s_1 = "%"
			s_cs_xx.parametri.parametro_s_2 = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "S"
			s_cs_xx.parametri.parametro_s_9 = "P"
			s_cs_xx.parametri.parametro_data_1 = tab_dettaglio.det_1.dw_1.getitemdatetime(tab_dettaglio.det_1.dw_1.getrow(),"data_registrazione")
			
			
			change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then	
					if len(this.gettext()) > 0 then					
						this.setcolumn("nota_piede")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else	
						this.setcolumn("nota_piede")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if	
		
end choose

end event

