﻿$PBExportHeader$w_tes_ord_acq_det_comunicazioni.srw
$PBExportComments$Finestra Testata Ordini di Acquisto Dettaglio
forward
global type w_tes_ord_acq_det_comunicazioni from w_cs_xx_risposta
end type
type dw_schede_intervento_comunicazioni from uo_cs_xx_dw within w_tes_ord_acq_det_comunicazioni
end type
end forward

global type w_tes_ord_acq_det_comunicazioni from w_cs_xx_risposta
integer width = 2985
integer height = 980
string title = "Comunicazioni Cicli di Vita"
dw_schede_intervento_comunicazioni dw_schede_intervento_comunicazioni
end type
global w_tes_ord_acq_det_comunicazioni w_tes_ord_acq_det_comunicazioni

type prototypes
function long GetTempPathA  (Long nBufferLength, ref String lpBuffer) Library  "kernel32.dll" alias for "GetTempPathA;Ansi"


end prototypes

type variables
long il_anno, il_num
end variables

on w_tes_ord_acq_det_comunicazioni.create
int iCurrent
call super::create
this.dw_schede_intervento_comunicazioni=create dw_schede_intervento_comunicazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_schede_intervento_comunicazioni
end on

on w_tes_ord_acq_det_comunicazioni.destroy
call super::destroy
destroy(this.dw_schede_intervento_comunicazioni)
end on

event pc_setwindow;call super::pc_setwindow;
dw_schede_intervento_comunicazioni.set_dw_options(sqlca,pcca.null_object,c_nonew + c_default,c_default)
																	  
il_anno = s_cs_xx.parametri.parametro_d_1																	  
il_num = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_schede_intervento_comunicazioni, &
                  "cod_destinatario", &
						sqlca, &
                  "tab_ind_dest", &
						"cod_destinatario", &
						"des_destinatario", &
                  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						
f_PO_LoadDDDW_DW( dw_schede_intervento_comunicazioni, &
                  "cod_utente", &
						sqlca, &
                  "utenti", &
						"cod_utente", &
						"nome_cognome", &
                  "")						

end event

type dw_schede_intervento_comunicazioni from uo_cs_xx_dw within w_tes_ord_acq_det_comunicazioni
integer x = 23
integer y = 20
integer width = 2903
integer height = 840
integer taborder = 0
string dataobject = "d_tes_ord_acq_det_comunicazioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, il_anno, il_num)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

