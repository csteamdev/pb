﻿$PBExportHeader$uo_importa_excel_ord_ven.sru
forward
global type uo_importa_excel_ord_ven from nonvisualobject
end type
end forward

global type uo_importa_excel_ord_ven from nonvisualobject
end type
global uo_importa_excel_ord_ven uo_importa_excel_ord_ven

type variables
/**
 * stefanop
 * 02/07/2014
 *
 * Importazione di un foglio excel all'interno dell'ordine di vendita
 **/
 
 
 constant string CODPRO = "CODPRO"
 constant string QUAN_P = "QUAN_P"
 constant long ROW_LOOP_MAX_ROW = 1000
end variables

forward prototypes
public function integer uof_seleziona_file (ref string as_filepath)
public function integer uof_importa_in_ordine (long al_anno, long al_numero, string as_filepath, ref string as_error)
public function long uof_get_colonne_gestione (string as_tipo_gestione, ref datastore lds_store, ref string as_error)
private function integer uof_create_soap (ref uo_cs_wssoap auo_proxy, ref string as_error)
public function integer uof_leggi_variabile (string as_cod_variabile, string as_flag_uso_formule, ref tns__str_variabili astr_variabile, ref string as_error)
end prototypes

public function integer uof_seleziona_file (ref string as_filepath);/**
 * stefanop
 * 02/07/2014
 *
 * Apre la finestra di seleziona file
 **/
 
string ls_docpath, ls_docname[], ls_desktop
int li_return

ls_desktop = guo_functions.uof_get_user_desktop_folder( )
 
return GetFileOpenName(&
			"File da importare", &
			as_filepath, ls_docname[], &
			"xls", &
			"Excel,*.xls;*.xlsx,", &
			ls_desktop)
end function

public function integer uof_importa_in_ordine (long al_anno, long al_numero, string as_filepath, ref string as_error);/**
 * stefanop
 * 02/07/2014
 *
 * Importa il file all'interno dell'ordine passato
 *
 * return: -1 Error, 0 Warning,  1 tutto ok
 **/

any la_value
string ls_cod_cliente, ls_cod_variabile, ls_col_excel, ls_flag_uso_formule
long ll_count_colonne_excel, ll_riga, ll_colonna, ll_count, ll_count_variabili, ll_soap_return, ll_excel_row_count, ll_righe_lette
datastore lds_variabili
uo_excel luo_excel
uo_cs_wssoap luo_proxy
//tns__str_carica_confezionato_input lstr_confezionato 
tns__str_carica_sfuso_input lstr_sfuso_input
tns__str_carica_sfuso_output lstr_sfuso_output

// Pulizia variabili
setnull(as_error)
ll_riga = 15
ll_count = 0
ll_excel_row_count = 0
ll_righe_lette = 0

// recupero colonne da leggere
ll_count_colonne_excel = uof_get_colonne_gestione("ORDVEN", lds_variabili, as_error)

if ll_count_colonne_excel < 0 then
	return -1
elseif ll_count_colonne_excel = 0 then
	as_error = "Mancano le impostazioni per l'importazione del tipo ORDVEN.~r~nPopolare la tabella parametri_import"
	return 0
end if

select cod_cliente
into :ls_cod_cliente
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and 
		 num_registrazione = :al_numero;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la lettura del cliente in testata ordine.~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_error = "Attenzione, l'ordine " + string(al_anno) + "/" + string(al_numero) + " non è stato trovato!"
	return -1
end if
	
try 
	// Apro file excel
	luo_excel = create uo_excel
	if not luo_excel.uof_open( as_filepath, false, true) then
		as_error = luo_excel.uof_get_error( )
		return -1
	end if
		
	// Creo struttura WS	
//	lstr_confezionato.al_anno_documento = al_anno
//	lstr_confezionato.al_num_documento = al_numero
	
	// ciclo tutte le righe del foglio excel
	do while true
		
		la_value = luo_excel.uof_read(ll_riga, "A")
		
		if isnull(la_value) then exit
		if string(la_value) = "FINE" or string(la_value) = "#FINE" then exit
		
		ll_righe_lette++
		
		// Prevent loop
		if ll_excel_row_count >= ROW_LOOP_MAX_ROW then
			if g_mb.confirm("Sembra si stia generando un loop. Blocco il processo?") then
				as_error = "Uscita forzata per prevenire un loop nella lettura del foglio excel"
				return 0
			else 
				ll_excel_row_count = 0
			end if
		end if
				
		ll_excel_row_count++
		ll_count++
		lstr_sfuso_input.as_cod_anagrafica = ls_cod_cliente
		lstr_sfuso_input.as_flag_tipo_anagrafica = "C"
		lstr_sfuso_input.as_tipo_gestione = "OFF_VEN"
		lstr_sfuso_input.as_tipo_configurazione = "OFF_VEN"
		
		ll_count_variabili = 0
		tns__str_variabili lstr_variabili[]
		
		// cicolo colonne excel e preparo la struttura per l'invio al webservice
		for ll_colonna = 1 to ll_count_colonne_excel
			
			ls_cod_variabile = lds_variabili.getitemstring(ll_colonna, 1)
			ls_col_excel = lds_variabili.getitemstring(ll_colonna, 2)
			ls_flag_uso_formule = lds_variabili.getitemstring(ll_colonna, 3)
			
			ll_count_variabili++
			
			if CODPRO <> ls_cod_variabile then
				
				// Leggo variabile
				if uof_leggi_variabile(ls_cod_variabile, ls_flag_uso_formule, ref lstr_variabili[ll_count_variabili], as_error) < 0 then 
					return -1
				end if
				
				la_value =  luo_excel.uof_read(ll_riga, ls_col_excel)
				
				choose case lstr_variabili[ll_count_variabili].as_flag_datatype
					case "S"
						lstr_variabili[ll_count_variabili].as_string = string(la_value)
						
					case "N"
						lstr_variabili[ll_count_variabili].ad_number = dec(la_value)
						
				end choose
				
			else
				lstr_sfuso_input.as_cod_prodotto_modello = luo_excel.uof_read_string(ll_riga, ls_col_excel)
				
				lstr_variabili[ll_count_variabili].as_cod_variabile = CODPRO
				lstr_variabili[ll_count_variabili].as_flag_datatype = "S"
				lstr_variabili[ll_count_variabili].as_string = lstr_sfuso_input.as_cod_prodotto_modello
				
			end if
			
			lstr_variabili[ll_count_variabili].as_uso_formule = ls_flag_uso_formule
			lstr_variabili[ll_count_variabili].as_flag_optional = "N"
			
		next
		
		lstr_sfuso_input.astr_variabili = lstr_variabili
		ll_riga++
		
	loop
	
//	lstr_sfuso.astr_carica_sfuso_input = lstr_sfuso_input
	
	if ll_righe_lette = 0 then
		g_mb.warning("Attenzione, sono state lette 0 righe dal foglio excel.~r~nVerifica che rispetti il layout richiesto!")
		return -1
	end if
	
	
	// CREO SERVIZIO
	if uof_create_soap(ref luo_proxy, ref as_error) = -1 then return -1
	
	tns__str_carica_sfuso_output lstr_response
	
	luo_proxy.uof_carica_sfuso(lstr_sfuso_input, lstr_response)
	
	//errorcode
		
// Controllo eccezioni		
catch (SoapException e1)
	as_error = g_str.append({"Errore SOAP~r~n", e1.getMessage()})
	return -1
	
catch (PBXRuntimeError e2)
	as_error = g_str.append({"Errore PBXRuntime~r~n", e2.getMessage()})
	return -1
	
catch (RuntimeError e)
	as_error = g_str.append({"Errore runtime~r~n", e.getMessage()})
	return -1
	
finally
	// Pulizia generale
	destroy luo_excel
end try

return 1
end function

public function long uof_get_colonne_gestione (string as_tipo_gestione, ref datastore lds_store, ref string as_error);/**
 * stefanop
 * 02/07/2014
 *
 * Ottengo le colonna da leggere nel foglio excel e le relative variabili
 **/
 
string ls_sql

ls_sql = "SELECT cod_variabile, col_excel, flag_uso_formule FROM parametri_import WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_tipo_gestione='" + as_tipo_gestione &
			+ "' ORDER BY col_excel"
			
return guo_functions.uof_crea_datastore(ref lds_store, ls_sql,  ref as_error)
end function

private function integer uof_create_soap (ref uo_cs_wssoap auo_proxy, ref string as_error);/**
 * stefanop
 * 03/07/2014
 *
 * Creo web service proxy
 **/
 
string ls_endpoint, ls_soap_log 
long ll_soap_return
soapconnection luo_soap

select endpoint, log_path
into :ls_endpoint, :ls_soap_log
from parametri_webservice
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_webservice = "CS_WS";
		 
try
	luo_soap = create soapconnection
	
	// LOG
	if g_str.isnotempty(ls_soap_log) then
		luo_soap.SetOptions("SoapLog=~"" + ls_soap_log + "~"")
	end if
	
	if g_str.isnotempty(ls_endpoint) then
		ll_soap_return = luo_soap.CreateInstance(auo_proxy, "uo_cs_wssoap", ls_endpoint)
	else
		ll_soap_return = luo_soap.CreateInstance(auo_proxy, "uo_cs_wssoap")
	end if
	
	if ll_soap_return = 0 then
		return 1
		
	elseif ll_soap_return = 100 then
		as_error = "Invalid Proxy Name"
		
	elseif ll_soap_return = 101 then
		as_error = "Errore durante la creazione del Proxy"
	
	else 
		as_error = "Errore generico duranta la creazione del Proxy"
		
	end if
catch(Exception ex)
	as_error = "Errore durante la creazione dell'oggetto soapconnection.~r~nVerificare di avere installato le DTK con l'opzione SOAP.~r~nAver installato .NetFramework 4"
	return -1
end try

return -1
end function

public function integer uof_leggi_variabile (string as_cod_variabile, string as_flag_uso_formule, ref tns__str_variabili astr_variabile, ref string as_error);/**
 * stefanop
 * 02/07/2014
 *
 * Leggo le informazioni della variabile
 *
 * Return -1 error, 1 ok
 **/
string ls_flag_tipo_dato

astr_variabile.as_cod_variabile = as_cod_variabile

//if as_flag_uso_formule = "S" then
select flag_tipo_dato
into :ls_flag_tipo_dato
from tab_variabili_formule
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_variabile = :as_cod_variabile;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la lettura della variabile " + string(as_cod_variabile) + ".~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	//as_error = "Impossibile trovare la variabile " + string(as_cod_variabile) + ".~r~nControllare la configurazione per l'importazione!"
	// Imposto fisso a Stringa
	ls_flag_tipo_dato = "S"
	//return -1
end if

astr_variabile.as_flag_datatype = ls_flag_tipo_dato
//else
//	
//	// Per il momento è fissa a stringa, bisogna trovare un modo per renderlo dinamico!!
//	astr_variabile.as_flag_datatype = "S"
//end if




//if QUAN_P = as_cod_variabile then
//	astr_variabile.as_uso_formule = 'N'
//else
//	astr_variabile.as_uso_formule = 'S'
//end if

return 0
end function

on uo_importa_excel_ord_ven.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_importa_excel_ord_ven.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

