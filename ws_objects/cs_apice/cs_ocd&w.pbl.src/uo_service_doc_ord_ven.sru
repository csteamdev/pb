﻿$PBExportHeader$uo_service_doc_ord_ven.sru
forward
global type uo_service_doc_ord_ven from uo_service_base
end type
end forward

global type uo_service_doc_ord_ven from uo_service_base
end type
global uo_service_doc_ord_ven uo_service_doc_ord_ven

type variables
long 				il_maxKB = 0

boolean			ib_attiva_max_dim = true
end variables

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
public function integer uof_elabora_file (string as_nome_file, string as_percorso_completo, ref string as_errore)
public function integer uof_controlla_estensione (string as_nome_file, ref string as_errore)
public function integer wf_controlla_esistenza_riga (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_errore)
public function integer wf_estrai_riga_da_barcode (long ll_barcode, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref long al_prog_riga_ord_ven, ref string as_errore)
public function integer uof_cancella_file (string as_percorso_completo, ref string as_errore)
public function integer wf_estrai_prima_riga (long al_barcode_tes, integer ai_anno_registrazione, long al_num_registrazione, ref long al_prog_riga_ord_ven, ref string as_errore)
public function integer uof_unisci_documenti (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga, ref string as_errore)
public function integer uof_unisci_documenti (integer ai_anno_registrazione, long al_num_registrazione, ref string as_errore)
public function integer uof_estrai_blob (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_nota, ref blob abl_documento, ref string as_errore)
public function integer uof_salva_file_tmp (long al_prog_mimetype, string as_temp_dir, blob ab_blob, ref string as_files[])
public function integer uof_cancella_files (string as_files[])
public function integer uof_unisci_documenti (string as_files[], string as_temp_dir, integer ai_anno, long al_numero, long al_riga, string as_like, ref string as_errore)
public function integer uof_unisci_documenti_testata (integer ai_anno_registrazione, long al_num_registrazione, ref string as_errore)
public function unsignedlong uof_inserisci_file (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_errore)
public function unsignedlong uof_inserisci_file_tran (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref transaction at_tran, ref string as_errore)
public function integer uof_crea_transazione_docs (ref transaction a_tran, ref string as_errore)
public function integer uof_unisci_documenti_tran (string as_files[], string as_temp_dir, integer ai_anno, long al_numero, long al_riga, string as_like, ref transaction a_tran_blob, ref string as_errore)
public function integer uof_estrai_riga_ordine (string as_nome_file, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref long al_prog_riga, ref string as_errore)
public function integer wf_controlla_esistenza_ordine (integer ai_anno_registrazione, long al_num_registrazione, string as_tipo, ref string as_errore)
public function unsignedlong uof_inserisci_file_testata (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, string as_tipo, ref string as_errore)
public function unsignedlong uof_inserisci_file_testata_tran (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, string as_tipo, ref transaction at_tran, ref string as_errore)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);string			ls_PDA, ls_condizione, ls_files_trovati[], ls_errore, ls_path_file, ls_path_log, sss
long			ll_tot_files, ll_index, ll_ok, ll_ko, ll_skip
date			ldt_today
time			ltm_now
integer		li_ret


//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=doc_ord_ven,A01,n
//																			dove n è un numero che indica la verbosità del log (min 1, max 5, default 2)
//##########################################################################################


//-----------------------------------------------------------------------------------------------------------------------------------------------------
//imposto l'azienda (se non passo niente metto A01) ed il livello di logging (1..5 se nopn passo niente metto 2)
uof_set_s_cs_xx(auo_params.uof_get_string(1,"A01"))
iuo_log.set_log_level(auo_params.uof_get_int(2, 2))


//-----------------------------------------------------------------------------------------------------------------------------------------------------
//leggo il percorso iniziale, dove i files sono disponibili: parametro aziendale PDA
guo_functions.uof_get_parametro_azienda( "PDA", ls_PDA)
if ls_PDA="" or isnull(ls_PDA) then
	iuo_log.error("Parametro aziendale PDA non trovato! Indica il percorso iniziale dei file da elaborare e aggiungere alle righe ordine di vendita ...")
	return 0
end if

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//mi accerto che il percorso finisca con il backslash, in caso contrario glielo aggiungo
if right(ls_PDA, 1) <> "\" then ls_PDA += "\"

//------------------------------------------------------------------------------------------------------------------------------------------------------
//leggo la max dimensione consentita al file, se specificata
guo_functions.uof_get_parametro("MLD", il_maxKB)
if isnull(il_maxKB) or il_maxKB < 0 then il_maxKB = 0


//-----------------------------------------------------------------------------------------------------------------------------------------------------
//assegno il nome al file di log    YYYYMMDD_hhmmss.log
ldt_today = today()
ltm_now = now()
ls_path_log = ls_PDA + "LOG\" + &
								string(year(ldt_today))+ right("00" + string(month(ldt_today)), 2) + right("00" + string(day(ldt_today)), 2) + ".log"

iuo_log.set_file( ls_path_log, false)

iuo_log.warn("### INIZIO ELABORAZIONE #######################")

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//carico i files in un array prima della relativa elaborazione
ls_condizione = "*.*"
//ls_condizione = auo_params.uof_get_string(3, "*.*")


ll_tot_files = guo_functions.uof_estrai_files(ls_PDA, ls_condizione, ls_files_trovati[], ls_errore)

if ll_tot_files < 0 then
	iuo_log.error("Errore durante l'estrazione dei files: " + ls_errore)
	return -1
	
elseif ll_tot_files = 0 then
	iuo_log.warn("Nessun files da elaborare nel percorso '"+ls_PDA+"'")
	return 0
end if

iuo_log.warn("Tot. files da elaborare: " + string(ll_tot_files))

 ll_ok = 0
 ll_ko = 0
 ll_skip = 0

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//se arrivi fin qui vuol dire che ci sono files da elaborare
for ll_index=1 to ll_tot_files
	//percorso completo del file
	ls_path_file = ls_PDA + ls_files_trovati[ll_index]
	iuo_log.log("Elaborazione in corso del file " + ls_path_file)
	
	//---------------------------------------------------------------------------------
	//0 tutto OK
	//1 fallito, scrivi nel log e prosegui con altri file presenti
	//-1 fallito, scrivi nel log ed interrompi l'elaborazione  --> 28/04/2014: CHIESTO DA ALBERTO DI CONTINUARE COMUNQUE L'ELABORAZIONE ...
	li_ret = uof_elabora_file(ls_files_trovati[ll_index], ls_path_file, ls_errore)
	
	if li_ret < 0 then
		
		//Donato 18/04/2014
		//Chiesto da Alberto: in caso di errore critico (ritorno -1) non annullare tutta l'elaborazione
		//ma semplicemente saltare il file
		
		//iuo_log.error(ls_errore + ". Operazione interrotta!")
		//return -1
		
		ll_ko += 1
		iuo_log.error(ls_errore + " [RET:"+string(li_ret)+"]. File " + ls_files_trovati[ll_index] + " saltato!")
		continue
		//fine modifica
		
	elseif li_ret <> 0 then
		//ll_ko += 1
		ll_skip += 1
		iuo_log.error(ls_errore + "[RET:"+string(li_ret)+"]. File " + ls_files_trovati[ll_index] + " saltato!")
		continue
		
	end if
	//---------------------------------------------------------------------------------
	
	ll_ok += 1
	
next

iuo_log.warn("Elaborazione terminata, Tot. Elaborati: " + string(ll_tot_files) + "  -  Con Successo: " + string(ll_ok) + "  -  Saltati: " + string(ll_skip) + "  -  Saltati e con Errori : " + string(ll_ko))
iuo_log.warn("### FINE ELABORAZIONE #######################")

return 0

end function

public function integer uof_elabora_file (string as_nome_file, string as_percorso_completo, ref string as_errore);integer		li_ret, li_anno_registrazione
long			ll_num_registrazione, ll_prog_riga_ord_ven
transaction	lt_tran
string			ls_msg


//----------------------------------------------------------------------------------------------------
//sono permesse solo le estensioni previste in tab_mimetype
li_ret = uof_controlla_estensione(as_nome_file, as_errore)
if li_ret<> 0 then
	return li_ret
end if


//----------------------------------------------------------------------------------------------------
//cerca di estrarre la riga ordine a cui va associato il file
li_ret = uof_estrai_riga_ordine(as_nome_file, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, as_errore)
if li_ret<> 0 then
	return li_ret
end if

ls_msg = ""

choose case left(as_nome_file, 1)
	case "A"
		//testata ordine acquisto
		if not guo_functions.uof_create_transaction_from( sqlca, lt_tran, as_errore)  then
			as_errore = "(default) Errore in creazione transazione: " + as_errore
			li_ret = -1
		else
			//a questa funzione gli arriverà già una transazione preparata (=ad SQLCA)
			li_ret = uof_inserisci_file_testata_tran(as_percorso_completo, li_anno_registrazione, ll_num_registrazione,  "TESORDACQ", lt_tran, as_errore)
			
			ls_msg = "Testata Ord.Acq. "+string(li_anno_registrazione)+"/"+string(ll_num_registrazione)
		end if
		
		
	case else
		//tutti gli altri casi

		//18/03/2014
		//se la funzione precedente riporta la variabile REF ll_prog_riga_ord_ven a NULL,
		//vuol dire che si tratta di un file di testata, da salvare nella tes_ord_ven_note (SR varie_per_produzione)
		
		//----------------------------------------------------------------------------------------------------
		//salva il file nella riga ordine (o testata ordine se  ll_prog_riga_ord_ven è NULL)
		setnull(li_ret)
		
		if isnull(ll_prog_riga_ord_ven) then
			ls_msg = "Testata Ord.Ven. "+string(li_anno_registrazione)+"/"+string(ll_num_registrazione)
			li_ret = uof_inserisci_file_testata_tran(as_percorso_completo, li_anno_registrazione, ll_num_registrazione,  "TESORDVEN", lt_tran, as_errore)
		else
			ls_msg = "Riga Ord.Ven. "+string(li_anno_registrazione)+"/"+string(ll_num_registrazione)+"/"+string(ll_prog_riga_ord_ven)
			li_ret = uof_inserisci_file_tran(as_percorso_completo, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, lt_tran, as_errore)
		end if
	

end choose

//**************************************************************************
if li_ret<> 0 then
	rollback using lt_tran;		//su ogni singolo file elaborato
	disconnect using lt_tran;
	destroy lt_tran;
	return li_ret
end if

commit using lt_tran;			//su ogni singolo file elaborato
disconnect using lt_tran;
destroy lt_tran;
//************************************************************************


//----------------------------------------------------------------------------------------------------
//cancella il file elaborato
li_ret = uof_cancella_file(as_percorso_completo, as_errore)
if li_ret<> 0 then
	return li_ret
end if

iuo_log.log("File '" + as_percorso_completo + "' associato a "+ls_msg+" e successivamente eliminato dalla cartella")

return 0
end function

public function integer uof_controlla_estensione (string as_nome_file, ref string as_errore);string				ls_estensione, ls_array[]
integer			li_count

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//estraggo la estensione del file
guo_functions.uof_explode(as_nome_file, ".", ls_array[])
if upperbound(ls_array[]) > 1 then
	ls_estensione = ls_array[2]
	
	if ls_estensione="" or isnull(ls_estensione) then
		as_errore = "Estensione non trovata per il file " + as_nome_file
		return 1
	end if
	
else
	//estensione non presente: errore bloccante per questo file, ma non dell'intero processo, cioè vai avanti con i prossimi files, se ce ne sono
	as_errore = "Estensione non trovata per il file " + as_nome_file
	return 1
end if


//-----------------------------------------------------------------------------------------------------------------------------------------------------
//controlla se presente il mimetype
select count(*)
into :li_count
from tab_mimetype
where 	cod_azienda=:s_cs_xx.cod_azienda and
			estensione=:ls_estensione;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo esistenza estensione " + ls_estensione + ": "+sqlca.sqlerrtext
	return -1
end if

if not li_count > 0 then
	as_errore = "Estensione file " + ls_estensione + " non trovata in tab_mimetype."
	return 1
end if

return 0
end function

public function integer wf_controlla_esistenza_riga (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_errore);long			ll_count

//controlla che la riga esista
setnull(ll_count)

select count(*)
into :ll_count
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_registrazione and
			num_registrazione=:al_num_registrazione and
			prog_riga_ord_ven=:al_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	as_errore = "Errore controllo esistenza riga ordine vendita (" + string(ai_anno_registrazione) + "/" + string(al_num_registrazione) + "/" + string(al_prog_riga_ord_ven) + ": " + sqlca.sqlerrtext
	return -1
	
elseif ll_count<=0 or isnull(ll_count) then
	as_errore = "Riga ordine vendita " + string(ai_anno_registrazione) + "/" + string(al_num_registrazione) + "/" + string(al_prog_riga_ord_ven)+" non trovata."
	return 1
	
end if

return 0
end function

public function integer wf_estrai_riga_da_barcode (long ll_barcode, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref long al_prog_riga_ord_ven, ref string as_errore);integer				li_ret



//cerco prima nella tes_ordini_produzione
select 	anno_registrazione,
			num_registrazione
into		:ai_anno_registrazione,
			:al_num_registrazione
from tes_ordini_produzione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			progr_tes_produzione = :ll_barcode;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura da tes_ordini_produzione per barcode "+string(ll_barcode) + " : "+sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 100 or isnull(ai_anno_registrazione) then
	//provo a cercare nella det_ordini_produzione
	select 	anno_registrazione,
				num_registrazione,
				prog_riga_ord_ven
	into		:ai_anno_registrazione,
				:al_num_registrazione,
				:al_prog_riga_ord_ven
	from det_ordini_produzione
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione = :ll_barcode;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura da det_ordini_produzione per barcode "+string(ll_barcode) + " : "+sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 or isnull(ai_anno_registrazione) then
		as_errore = "nessun lancio di produzione per il barcode "+string(ll_barcode) + "!"
		return 1		//non bloccante
	end if
	
	//se arrivi fin qui, il lancio di produzione c'è e l'ordine è del tipo report 1
	return 0
	
end if


//se arrivi fin qui vuol dire che hai trovato qualcosa in tes ordini produzione: ordine tipo report 2 o report 3
//estrai la prima riga ordine disponibile (prog. più basso)
li_ret = wf_estrai_prima_riga(ll_barcode, ai_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, as_errore)
if li_ret <> 0 then
	return li_ret
end if


return 0
end function

public function integer uof_cancella_file (string as_percorso_completo, ref string as_errore);boolean			lb_ret

lb_ret = filedelete(as_percorso_completo)

if not lb_ret then
	as_errore = "Errore in cancellazione file '" + as_percorso_completo + "'"
	return 1
end if

return 0
end function

public function integer wf_estrai_prima_riga (long al_barcode_tes, integer ai_anno_registrazione, long al_num_registrazione, ref long al_prog_riga_ord_ven, ref string as_errore);


//estrai la prima riga dell'ordine
setnull(al_prog_riga_ord_ven)

if al_barcode_tes > 0 then

	//per estrarre la prima riga disponibile, se il file è del tipo Pnnnnn (cioè da barcode di produzione)
	//nel caso in cui si tratti do ordini tipo 2 o tipo 3 (barcode di tes_ordini_produzione), 
	//bisogna riferirsi al righe che sono riferite a tale barcode
	//infatti ci possono essere casi in cui i reparti di produzione siano più di uno 
	//es. 	righe 10,30, 40 -> reparto 1			-> ESTRARRE LA RIGA 10
	//		righe 20,50      -> reparto 2				-> ESTRARRE LA RIGA 20
	select min(prog_riga_ord_ven)
	into :al_prog_riga_ord_ven
	from det_ordini_produzione
	where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_registrazione and
			num_registrazione=:al_num_registrazione and 
			progr_tes_produzione=:al_barcode_tes;

else
	
	select min(prog_riga_ord_ven)
	into :al_prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_registrazione and
				num_registrazione=:al_num_registrazione;

end if

if sqlca.sqlcode < 0 then
	as_errore = "Errore estrazione prima riga ordine vendita (" + string(ai_anno_registrazione) + "/" + string(al_num_registrazione)  + ") : " + sqlca.sqlerrtext
	return -1
	
elseif al_prog_riga_ord_ven<=0 or isnull(al_prog_riga_ord_ven) then
	as_errore = "In ricerca prima riga ordine vendita, non è stato trovato niente oppure l'ordine è inesistente (" + string(ai_anno_registrazione) + "/" + string(al_num_registrazione) + ")"
	return 1
	
end if

return 0
end function

public function integer uof_unisci_documenti (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga, ref string as_errore);
//elabora tutti i fuile che iniziano per 
//		P
//		T
//		O
//associati alla riga e li unisce in un unico file P, un unico file T un unico file O
//eliminando quelli precedenti
//
//		PYYYYnnnnnnRRRR.pdf
//		TYYYYnnnnnn.pdf
//		OYYYYnnnnnnRRRR.pdf

datastore				lds_data
string						ls_sql_base, ls_sql, ls_cod_nota, ls_files[], ls_temp_dir, ls_vuoto[]
long						ll_index, ll_tot, ll_prog_mimtype
blob						lbl_documento
integer					li_ret
transaction				lt_tran_blob



ls_temp_dir = guo_functions.uof_get_user_temp_folder( )

ls_sql_base = "select cod_nota, prog_mimetype "+&
					"from det_ord_ven_note "+&
					"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"anno_registrazione = "+string(ai_anno_registrazione)+ " and "+&
								"num_registrazione = "+string(al_num_registrazione)+ " and "+&
								"prog_riga_ord_ven = "+string(al_prog_riga) + " and "+&
								"isnumeric(substring(des_nota, 2, 4))=1 "

ls_sql_base += " and (disabilitato=0 or disabilitato is null) "

//##################################################################################
//elabora documenti che iniziano per P
ls_files[] = ls_vuoto[]
ls_sql = ls_sql_base + " and des_nota like 'P%'"
ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_tot<0 then
	//errore
	return -1
	
elseif ll_tot>1 then
	//procedi all'unione
	
	for ll_index=1 to ll_tot
		ls_cod_nota = lds_data.getitemstring(ll_index, 1)
		ll_prog_mimtype = lds_data.getitemnumber(ll_index, 2)
		
		//estrazione blob di ogni singolo file ----------------------------
		if uof_estrai_blob(ai_anno_registrazione, al_num_registrazione, al_prog_riga, ls_cod_nota, lbl_documento, as_errore) < 0 then
			destroy lds_data
			return -1
		end if
		
		if len(lbl_documento) > 0 then
		else
			continue
		end if
		
		//salva file in cartella temporanea -------------------------------
		if uof_salva_file_tmp(ll_prog_mimtype, ls_temp_dir, lbl_documento, ls_files[]) <> 0 then continue
		
	next
	
	//se ho dei files pdf nell'array ls_files[], devo procedere ad unirli tutti, eliminare dalla tabella note quelli presenti che sono stati uniti
	//salvare il file-unione, eliminare da disco i file temporanei e faccio anche un COMMIT (per ogni tipologia)
	
	//#############################################
	if uof_crea_transazione_docs(lt_tran_blob, as_errore) < 0 then
		return -1
	end if
	//#############################################
	
	li_ret = uof_unisci_documenti_tran(ls_files[], ls_temp_dir, ai_anno_registrazione, al_num_registrazione, al_prog_riga, "P", lt_tran_blob, as_errore)
	if li_ret < 0 then
		rollback using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		
		//prova comunque a cancellare i file temporanei
		uof_cancella_files(ls_files[])
		return -1
	else
		commit using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		//file temporanei già cancellati
	end if
else
	//nessun errore, ma ci vogliono almeno 2 documenti da unire, quindi non fare niente ...
end if

destroy lds_data


//##################################################################################
//elabora documenti che iniziano per T
ls_files[] = ls_vuoto[]
ls_sql = ls_sql_base + " and des_nota like 'T%'"
ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_tot<0 then
	//errore
	return -1
	
elseif ll_tot>1 then
	//procedi all'unione
	for ll_index=1 to ll_tot
		ls_cod_nota = lds_data.getitemstring(ll_index, 1)
		ll_prog_mimtype = lds_data.getitemnumber(ll_index, 2)
		
		//estrazione blob di ogni singolo file ----------------------------
		if uof_estrai_blob(ai_anno_registrazione, al_num_registrazione, al_prog_riga, ls_cod_nota, lbl_documento, as_errore) < 0 then
			destroy lds_data
			return -1
		end if
		
		if len(lbl_documento) > 0 then
		else
			continue
		end if
		
		//salva file in cartella temporanea -------------------------------
		if uof_salva_file_tmp(ll_prog_mimtype, ls_temp_dir, lbl_documento, ls_files[]) <> 0 then continue
		
	next
	
	//se ho dei files pdf nell'array ls_files[], devo procedere ad unirli tutti, eliminare dalla tabella note quelli presenti che sono stati uniti
	//salvare il file-unione, eliminare da disco i file temporanei e faccio anche un COMMIT (per ogni tipologia)
	
	//#############################################
	if uof_crea_transazione_docs(lt_tran_blob, as_errore) < 0 then
		return -1
	end if
	//#############################################
	
	li_ret = uof_unisci_documenti_tran(ls_files[], ls_temp_dir, ai_anno_registrazione, al_num_registrazione, al_prog_riga, "T", lt_tran_blob, as_errore)
	if li_ret < 0 then
		rollback using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		
		//prova comunque a cancellare i file temporanei
		uof_cancella_files(ls_files[])
		return -1
		
	else
		commit using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		//file temporanei già cancellati
		
	end if
	
else
	//nessun errore, ma ci vogliono almeno 2 documenti da unire, quindi non fare niente ...
end if

destroy lds_data



//##################################################################################
//elabora documenti che iniziano per O
ls_files[] = ls_vuoto[]
ls_sql = ls_sql_base + " and des_nota like 'O%'"
ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_tot<0 then
	//errore
	return -1
	
elseif ll_tot>1 then
	//procedi all'unione
	for ll_index=1 to ll_tot
		ls_cod_nota = lds_data.getitemstring(ll_index, 1)
		ll_prog_mimtype = lds_data.getitemnumber(ll_index, 2)
		
		//estrazione blob di ogni singolo file ----------------------------
		if uof_estrai_blob(ai_anno_registrazione, al_num_registrazione, al_prog_riga, ls_cod_nota, lbl_documento, as_errore) < 0 then
			destroy lds_data
			return -1
		end if
		
		if len(lbl_documento) > 0 then
		else
			continue
		end if
		
		//salva file in cartella temporanea -------------------------------
		if uof_salva_file_tmp(ll_prog_mimtype, ls_temp_dir, lbl_documento, ls_files[]) <> 0 then continue
		
	next
	
	//se ho dei files pdf nell'array ls_files[], devo procedere ad unirli tutti, eliminare dalla tabella note quelli presenti che sono stati uniti
	//salvare il file-unione, eliminare da disco i file temporanei e faccio anche un COMMIT (per ogni tipologia)
	
	//#############################################
	if uof_crea_transazione_docs(lt_tran_blob, as_errore) < 0 then
		return -1
	end if
	//#############################################
	
	li_ret = uof_unisci_documenti_tran(ls_files[], ls_temp_dir, ai_anno_registrazione, al_num_registrazione, al_prog_riga, "O", lt_tran_blob, as_errore)
	if li_ret < 0 then
		rollback using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		
		//prova comunque a cancellare i file temporanei
		uof_cancella_files(ls_files[])
		return -1
		
	else
		commit using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		//file temporanei già cancellati
		
	end if
	
else
	//nessun errore, ma ci vogliono almeno 2 documenti da unire, quindi non fare niente ...
end if


return 0
end function

public function integer uof_unisci_documenti (integer ai_anno_registrazione, long al_num_registrazione, ref string as_errore);

long					ll_riga_ordine, ll_index, ll_tot
integer				li_ret
datastore			lds_data
string				ls_sql


ls_sql = "select prog_riga_ord_ven "+&
			"from det_ord_ven "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(ai_anno_registrazione)+" and "+&
						"num_registrazione="+string(al_num_registrazione)+" "+&
			"order by prog_riga_ord_ven"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)

if ll_tot < 0 then
	return -1
end if

for ll_index=1 to ll_tot
	
	ll_riga_ordine = lds_data.getitemnumber(ll_index, 1)

	//all'interno della funzione viene fatto un COMMIT, riga per riga e per tipologia (P, T, O)
	li_ret = uof_unisci_documenti(ai_anno_registrazione, al_num_registrazione, ll_riga_ordine, as_errore)
	
	if li_ret < 0 then
		destroy lds_data
		return -1
	end if
next

destroy lds_data


//aggiungo qui un pezzo per unire i documenti inizianti per T eventualmente presenti nella tabella tes_ord_ven_note

//all'interno della funzione viene fatto un COMMIT
li_ret = uof_unisci_documenti_testata(ai_anno_registrazione, al_num_registrazione, as_errore)
if li_ret < 0 then
	destroy lds_data
	return -1
end if

return 0

end function

public function integer uof_estrai_blob (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_nota, ref blob abl_documento, ref string as_errore);string			ls_tipologia


if al_riga_ordine > 0 then

	selectblob 	note_esterne
	into 			:abl_documento
	from det_ord_ven_note
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_ordine and
			num_registrazione = :al_num_ordine and
			prog_riga_ord_ven = :al_riga_ordine and
			cod_nota = :as_cod_nota;
	
	ls_tipologia = " (DET) "
	
else
	selectblob 	note_esterne
	into 			:abl_documento
	from tes_ord_ven_note
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_ordine and
			num_registrazione = :al_num_ordine and
			cod_nota = :as_cod_nota;
	
	ls_tipologia = " (TES) "
	
end if
	
if sqlca.sqlcode<0 then
	as_errore = ls_tipologia + "Errore in lettura documento dalla tabella (sqlcode -1): "+sqlca.sqlerrtext
	return -1

elseif sqlca.sqlcode = 100 then
	as_errore = ls_tipologia + "Documento non trovato! Potrebbe essere stato cancellato da un altro utente!"
	return -1
	
elseif sqlca.sqlcode <> 0 then
	as_errore = ls_tipologia + "Errore in lettura documento dalla tabella (sqlcode <>-1,<>0 e <>100)"
	return -1
	
end if


return 0
end function

public function integer uof_salva_file_tmp (long al_prog_mimetype, string as_temp_dir, blob ab_blob, ref string as_files[]);string				ls_estensione, ls_rnd,ls_file_pdf
long					ll_index


//salva su cartella temporanea
select estensione
into :ls_estensione
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		prog_mimetype = :al_prog_mimetype;

//stampa solo quelli di tipo PDF
if upper(ls_estensione)<>"PDF" then return 1


ll_index = upperbound(as_files[])

ll_index += 1

//nome file:    YYMMDD_hhmmss_nn.pdf
ls_rnd = string( year(today()),"00") + string( month(today()),"00") + string( day(today()),"00") + "_" + &
			string( hour(now()),"00") + string( minute(now()),"00") + string( second(now()),"00") + "_"+string(ll_index)
			
ls_file_pdf =  as_temp_dir + ls_rnd + "." + ls_estensione
guo_functions.uof_blob_to_file( ab_blob, ls_file_pdf)


//------------------------------------------------------------------------------------------------------------------------------------
//salvo nell'array i files pdf che via via vado a creare, per poterli cancellare dopo la stampa
as_files[ll_index] = ls_file_pdf

return 0
end function

public function integer uof_cancella_files (string as_files[]);long		ll_index
integer	li_ret
string	ls_errore


for ll_index=1 to upperbound(as_files[])

	//li_ret = uof_cancella_file('"'+as_files[ll_index]+'"', ls_errore)
	li_ret = uof_cancella_file(as_files[ll_index], ls_errore)

next

return 0
end function

public function integer uof_unisci_documenti (string as_files[], string as_temp_dir, integer ai_anno, long al_numero, long al_riga, string as_like, ref string as_errore);long					ll_index, ll_tot

string					ls_path_exe, ls_files_list, ls_cmd, ls_dest_file, ls_dest_file_2, ls_des_nota, ls_like, ls_tipologia

integer				li_ret



//se ho dei files pdf nell'array as_files[], devo procedere a
//		- unirli tutti
//		- eliminare dalla tabella note quelli presenti che sono stati uniti
//		- salvare il file-unione
//		- eliminare da disco i file temporanei

//all'uscita di questa funzione verrà fatto un COMMIT (per tipologia), altrimenti rollback con interruzione di tutto il processo

ll_tot = upperbound(as_files[])

if ll_tot>0 then
else
	return 1
end if

if al_riga>0 then
	ls_tipologia = " (DET) "
else
	ls_tipologia = " (TES) "
end if


//eseguibile del comando che unisce i pdf -------------------------------------------------
ls_path_exe = s_cs_xx.volume + s_cs_xx.risorse + 'libs\pdftk.exe'

if FileExists(ls_path_exe) then
else
	as_errore = ls_tipologia + "Impossibile unire i documenti: percorso dell'utility pdftk.exe non trovato! ("+ls_path_exe+")"
	return -1
end if


//ci attacco le doppie virgolette per sicurezza --------------------------------------------
ls_path_exe = '"' + ls_path_exe + '"'


//nome del file destinazione dell'unione ----------------------------------------------------
ls_des_nota = as_like + string(ai_anno) + string(al_numero, "000000")

if as_like<>"T" then
	//"P" oppure "O"
	ls_des_nota += string(al_riga, "0000")
end if

ls_des_nota += ".pdf"

//percorso completo del file destinazione dell'unione -----------------------------------
ls_dest_file_2 = as_temp_dir + ls_des_nota
ls_dest_file = '"' + ls_dest_file_2 + '"'			//doppi apici agli estremi



//preparazione comando di unione --------------------------------------------------------
ls_files_list = ""
for ll_index=1 to ll_tot
	ls_files_list += ' "' + as_files[ll_index] + '" '
next
ls_cmd = ls_path_exe + " " + ls_files_list + ' output ' + ls_dest_file + " dont_ask"


//unisci ----------------------------------------------------------------------------------------
li_ret = run(ls_cmd, Minimized!)

sleep(3)

if li_ret<0 then
	as_errore = ls_tipologia + "Unione documenti " + as_like + " non riuscita! Errore generico!"
	return -1
end if


//elimina dalla tabella i documenti uniti -----------------------------------------------
ls_like = as_like + "%"

if al_riga>0 then

	delete from det_ord_ven_note
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno and
				num_registrazione=:al_numero and
				prog_riga_ord_ven=:al_riga and
				des_nota like :ls_like;
			
else
	delete from tes_ord_ven_note
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno and
				num_registrazione=:al_numero and
				des_nota like :ls_like;
				
end if


if sqlca.sqlcode<0 then
	as_errore = ls_tipologia + "Errore in cancellazione documenti  "+ as_like + " uniti: "+sqlca.sqlerrtext
	return -1
end if


//inserisci il file risultato dell'unione ---------------------------------------------------
ib_attiva_max_dim = false
li_ret = uof_inserisci_file(ls_dest_file_2, ai_anno, al_numero, al_riga, as_errore)
ib_attiva_max_dim = true

if li_ret < 0 then
	return -1
end if


//cancella file destinazione dal disco ---------------------------------------------------
uof_cancella_file(ls_dest_file_2, as_errore)


//cancella files temporanei  dal disco --------------------------------------------------
uof_cancella_files(as_files[])


return 0
end function

public function integer uof_unisci_documenti_testata (integer ai_anno_registrazione, long al_num_registrazione, ref string as_errore);
//elabora tutti i fuile che iniziano per 
//		T
//associati alla testata e li unisce in un unico file T
//eliminando quelli precedenti
//
//		TYYYYnnnnnn.pdf
//		OYYYYnnnnnnRRRR.pdf

datastore				lds_data
string						ls_sql, ls_cod_nota, ls_files[], ls_temp_dir, ls_vuoto[]
long						ll_index, ll_tot, ll_prog_mimtype
blob						lbl_documento
integer					li_ret
transaction				lt_tran_blob



ls_temp_dir = guo_functions.uof_get_user_temp_folder( )

ls_sql	= 			"select cod_nota, prog_mimetype "+&
					"from tes_ord_ven_note "+&
					"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"anno_registrazione = "+string(ai_anno_registrazione)+ " and "+&
								"num_registrazione = "+string(al_num_registrazione)+ " and "+&
								"isnumeric(substring(des_nota, 2, 4))=1 and "+&
								"des_nota like 'T%'"

ls_sql += " and (disabilitato=0 or disabilitato is null) "

//##################################################################################
//elabora documenti che iniziano per T
ls_files[] = ls_vuoto[]
ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_tot<0 then
	//errore
	return -1
	
elseif ll_tot>1 then
	//procedi all'unione
	for ll_index=1 to ll_tot
		ls_cod_nota = lds_data.getitemstring(ll_index, 1)
		ll_prog_mimtype = lds_data.getitemnumber(ll_index, 2)
		
		//estrazione blob di ogni singolo file ----------------------------
		if uof_estrai_blob(ai_anno_registrazione, al_num_registrazione, 0, ls_cod_nota, lbl_documento, as_errore) < 0 then
			destroy lds_data
			return -1
		end if
		
		if len(lbl_documento) > 0 then
		else
			continue
		end if
		
		//salva file in cartella temporanea -------------------------------
		if uof_salva_file_tmp(ll_prog_mimtype, ls_temp_dir, lbl_documento, ls_files[]) <> 0 then continue
		
	next
	
	//se ho dei files pdf nell'array ls_files[], devo procedere ad unirli tutti, eliminare dalla tabella note quelli presenti che sono stati uniti
	//salvare il file-unione, eliminare da disco i file temporanei e faccio anche un COMMIT (per ogni tipologia)
	
	//#############################################
	if uof_crea_transazione_docs(lt_tran_blob, as_errore) < 0 then
		return -1
	end if
	//#############################################
	
	li_ret = uof_unisci_documenti_tran(ls_files[], ls_temp_dir, ai_anno_registrazione, al_num_registrazione, 0, "T", lt_tran_blob, as_errore)
	if li_ret < 0 then
		rollback using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		
		//prova comunque a cancellare i file temporanei
		uof_cancella_files(ls_files[])
		return -1
		
	else
		commit using lt_tran_blob;
		disconnect using lt_tran_blob;
		destroy lt_tran_blob;
		//file temporanei già cancellati
		
	end if
	
else
	//nessun errore, ma ci vogliono almeno 2 documenti da unire, quindi non fare niente ...
end if

destroy lds_data


return 0
end function

public function unsignedlong uof_inserisci_file (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_errore);long				ll_cont, ll_prog_mimetype, ll_len
string				ls_cod_nota, ls_dir, ls_file, ls_ext, ls_cont, ls_tipologia
blob				lb_note_esterne


if al_prog_riga_ord_ven > 0 then

	select 	max(cod_nota) 
	into 		:ls_cont
	from 		det_ord_ven_note
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				prog_riga_ord_ven = :al_prog_riga_ord_ven and
				isnumeric(cod_nota)=1;
				
	ls_tipologia = " (DET) "
				
else
	select 	max(cod_nota) 
	into 		:ls_cont
	from 		tes_ord_ven_note
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				isnumeric(cod_nota)=1;
				
	ls_tipologia = " (TES) "
	
end if
				

if isnull(ls_cont) or ls_cont="" then ls_cont = "0"
ll_cont = long(ls_cont)
ll_cont += 1
//----------------------------------------------------------------------



ls_cod_nota = string(ll_cont, "000") 

guo_functions.uof_file_to_blob(as_percorso_completo, lb_note_esterne)

//lunghezza in BYTE del documento in esame
ll_len = lenA(lb_note_esterne)

if ib_attiva_max_dim then
	if il_maxKB>0 then
		if ll_len > il_maxKB * 1024 then
			//documento oltre la grandezza massima pre-stabilita, quindi saltalo, ma non interrompere la procedura
			as_errore = ls_tipologia + "Attenzione: grandezza file (" + string(ll_len) + " BYTE) superiore a quella prestabilita ("+string(il_maxKB * 1024)+" BYTE) come da parametro multiazienda MLD"
			return 1
		end if
	end if
end if


guo_functions.uof_get_file_info( as_percorso_completo, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;


if al_prog_riga_ord_ven > 0 then

	insert into det_ord_ven_note  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ord_ven,   
			  cod_nota,   
			  des_nota,   
			  note,
			  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :ai_anno_registrazione,   
			  :al_num_registrazione,   
			  :al_prog_riga_ord_ven,   
			  :ls_cod_nota,   
			  :ls_file,   
			  null,
			 :ll_prog_mimetype);


	if sqlca.sqlcode = 0 then
		
		updateblob det_ord_ven_note
		set note_esterne = :lb_note_esterne
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				prog_riga_ord_ven = :al_prog_riga_ord_ven and
				cod_nota = :ls_cod_nota;
				
		if sqlca.sqlcode <> 0 then
			as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
	else
		as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + sqlca.sqlerrtext
		return -1

	end if
	
else
	insert into tes_ord_ven_note  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  cod_nota,   
			  des_nota,   
			  note,
			  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :ai_anno_registrazione,   
			  :al_num_registrazione,   
			  :ls_cod_nota,   
			  :ls_file,   
			  null,
			 :ll_prog_mimetype);

	if sqlca.sqlcode = 0 then
		
		updateblob tes_ord_ven_note
		set note_esterne = :lb_note_esterne
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				cod_nota = :ls_cod_nota;
				
		if sqlca.sqlcode <> 0 then
			as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
	else
		as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + sqlca.sqlerrtext
		return -1
	
	end if
	
end if
	

return 0
end function

public function unsignedlong uof_inserisci_file_tran (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref transaction at_tran, ref string as_errore);long				ll_cont, ll_prog_mimetype, ll_len, ll_len_after
string				ls_cod_nota, ls_dir, ls_file, ls_ext, ls_cont, ls_tipologia
blob				lb_note_esterne, lb_note_esterne_after


if not isvalid(at_tran) then
	if uof_crea_transazione_docs(at_tran, as_errore) < 0 then
		return -1
	end if
end if

if al_prog_riga_ord_ven > 0 then

	select 	max(cod_nota) 
	into 		:ls_cont
	from 		det_ord_ven_note
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				prog_riga_ord_ven = :al_prog_riga_ord_ven and
				isnumeric(cod_nota)=1
	using at_tran;
				
	ls_tipologia = " (DET) "
				
else
	select 	max(cod_nota) 
	into 		:ls_cont
	from 		tes_ord_ven_note
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				isnumeric(cod_nota)=1
	using at_tran;
				
	ls_tipologia = " (TES) "
	
end if
				

if isnull(ls_cont) or ls_cont="" then ls_cont = "0"
ll_cont = long(ls_cont)
ll_cont += 1
//----------------------------------------------------------------------



ls_cod_nota = string(ll_cont, "000") 

guo_functions.uof_file_to_blob(as_percorso_completo, lb_note_esterne)

//lunghezza in BYTE del documento in esame
ll_len = lenA(lb_note_esterne)
if isnull(ll_len) then ll_len = 0

if ll_len = 0 then
	as_errore = ls_tipologia + "Attenzione: grandezza file pari a ZERO!"
	return 1
end if

if ib_attiva_max_dim then
	if il_maxKB>0 then
		if ll_len > il_maxKB * 1024 then
			//documento oltre la grandezza massima pre-stabilita, quindi saltalo, ma non interrompere la procedura
			as_errore = ls_tipologia + "Attenzione: grandezza file (" + string(ll_len) + " BYTE) superiore a quella prestabilita ("+string(il_maxKB * 1024)+" BYTE) come da parametro multiazienda MLD"
			return 1
		end if
	end if
end if


guo_functions.uof_get_file_info( as_percorso_completo, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;

//if uof_crea_transazione_docs(at_tran, as_errore) < 0 then
//	return -1
//end if

if al_prog_riga_ord_ven > 0 then

	insert into det_ord_ven_note  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ord_ven,   
			  cod_nota,   
			  des_nota,   
			  note,
			  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :ai_anno_registrazione,   
			  :al_num_registrazione,   
			  :al_prog_riga_ord_ven,   
			  :ls_cod_nota,   
			  :ls_file,   
			  null,
			 :ll_prog_mimetype)
	using at_tran;

	if at_tran.sqlcode = 0 then
		
		updateblob det_ord_ven_note
		set note_esterne = :lb_note_esterne
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				prog_riga_ord_ven = :al_prog_riga_ord_ven and
				cod_nota = :ls_cod_nota
		using at_tran;
				
		if at_tran.sqlcode <> 0 then
			as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + at_tran.sqlerrtext
			return -1
		end if
		
	else
		as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + at_tran.sqlerrtext
		return -1

	end if
	
else
	insert into tes_ord_ven_note  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  cod_nota,   
			  des_nota,   
			  note,
			  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :ai_anno_registrazione,   
			  :al_num_registrazione,   
			  :ls_cod_nota,   
			  :ls_file,   
			  null,
			 :ll_prog_mimetype)
	using at_tran;

	if at_tran.sqlcode = 0 then
		
		updateblob tes_ord_ven_note
		set note_esterne = :lb_note_esterne
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				cod_nota = :ls_cod_nota
		using at_tran;
				
		if at_tran.sqlcode <> 0 then
			as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + at_tran.sqlerrtext
			return -1
		end if
		
	else
		as_errore = ls_tipologia + "Errore nell'inserimento del documento digitale.~r~n" + at_tran.sqlerrtext
		return -1

	end if
	
end if
	
	
//se arrivo fin qui l'inserimento è avvenuto e sqlcode della transazione non e tornato con valore negativo
//#########################################################################################
//IMPLEMENTAZIONE VERIFICA INCROCIATA
//qui rileggo il blob inserito: deve avere la stessa lunghezza originaria per confermare l'avvenuto inserimento in maniera corretta
if al_prog_riga_ord_ven > 0 then
	
	selectblob note_esterne
	into :lb_note_esterne_after
	from det_ord_ven_note
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_registrazione and
			num_registrazione = :al_num_registrazione and
			prog_riga_ord_ven = :al_prog_riga_ord_ven and
			cod_nota = :ls_cod_nota
	using at_tran;
	
else
	selectblob note_esterne
	into :lb_note_esterne_after
	from tes_ord_ven_note
	where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				cod_nota = :ls_cod_nota
	using at_tran;
	
end if

if at_tran.sqlcode <> 0 then
	as_errore = ls_tipologia + "Errore in lettura dato binario per controllo incrociato: " + at_tran.sqlerrtext
	return -1
end if
		
ll_len_after = lenA(lb_note_esterne)
if isnull(ll_len_after) then ll_len_after = 0

if ll_len_after<>ll_len then
	as_errore = ls_tipologia + "Inconguenza nella dimensione del file salvato: dimensione originale ("+string(ll_len)+") dimensione salvata ("+string(ll_len_after)+")"
	return -1
end if
//#########################################################################################

return 0
end function

public function integer uof_crea_transazione_docs (ref transaction a_tran, ref string as_errore);string			ls_databasedocs, ls_servernamedocs


ls_databasedocs = ""
ls_servernamedocs = ""
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "databasedocs",ls_databasedocs)
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "servernamedocs",ls_servernamedocs)


if not isnull(ls_servernamedocs) and len(ls_servernamedocs) > 0 and not isnull(ls_databasedocs) and len(ls_databasedocs) > 0 then
	if not guo_functions.uof_create_transaction_from( sqlca, a_tran, ls_servernamedocs, ls_databasedocs, as_errore)  then
		as_errore = "(*) Errore in creazione transazione: " + as_errore
		return -1
	end if
else
	if not guo_functions.uof_create_transaction_from( sqlca, a_tran, as_errore)  then
		as_errore = "(default) Errore in creazione transazione: " + as_errore
		return -1
	end if
end if

return 0
end function

public function integer uof_unisci_documenti_tran (string as_files[], string as_temp_dir, integer ai_anno, long al_numero, long al_riga, string as_like, ref transaction a_tran_blob, ref string as_errore);long					ll_index, ll_tot

string					ls_path_exe, ls_files_list, ls_cmd, ls_dest_file, ls_dest_file_2, ls_des_nota, ls_like, ls_tipologia

integer				li_ret



//se ho dei files pdf nell'array as_files[], devo procedere a
//		- unirli tutti
//		- eliminare dalla tabella note quelli presenti che sono stati uniti
//		- salvare il file-unione
//		- eliminare da disco i file temporanei

//all'uscita di questa funzione verrà fatto un COMMIT (per tipologia), altrimenti rollback con interruzione di tutto il processo

ll_tot = upperbound(as_files[])

if ll_tot>0 then
else
	return 1
end if

if al_riga>0 then
	ls_tipologia = " (DET) "
else
	ls_tipologia = " (TES) "
end if


//eseguibile del comando che unisce i pdf -------------------------------------------------
ls_path_exe = s_cs_xx.volume + s_cs_xx.risorse + 'libs\pdftk.exe'

if FileExists(ls_path_exe) then
else
	as_errore = ls_tipologia + "Impossibile unire i documenti: percorso dell'utility pdftk.exe non trovato! ("+ls_path_exe+")"
	return -1
end if


//ci attacco le doppie virgolette per sicurezza --------------------------------------------
ls_path_exe = '"' + ls_path_exe + '"'


//nome del file destinazione dell'unione ----------------------------------------------------
ls_des_nota = as_like + string(ai_anno) + string(al_numero, "000000")

if as_like<>"T" then
	//"P" oppure "O"
	ls_des_nota += string(al_riga, "0000")
end if

ls_des_nota += ".pdf"

//percorso completo del file destinazione dell'unione -----------------------------------
ls_dest_file_2 = as_temp_dir + ls_des_nota
ls_dest_file = '"' + ls_dest_file_2 + '"'			//doppi apici agli estremi



//preparazione comando di unione --------------------------------------------------------
ls_files_list = ""
for ll_index=1 to ll_tot
	ls_files_list += ' "' + as_files[ll_index] + '" '
next
ls_cmd = ls_path_exe + " " + ls_files_list + ' output ' + ls_dest_file + " dont_ask"


//unisci ----------------------------------------------------------------------------------------
li_ret = run(ls_cmd, Minimized!)

sleep(3)

if li_ret<0 then
	as_errore = ls_tipologia + "Unione documenti " + as_like + " non riuscita! Errore generico!"
	return -1
end if


//elimina dalla tabella i documenti uniti -----------------------------------------------
ls_like = as_like + "%"

if al_riga>0 then

	delete from det_ord_ven_note
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno and
				num_registrazione=:al_numero and
				prog_riga_ord_ven=:al_riga and
				des_nota like :ls_like and
				(disabilitato=0 or disabilitato is null)
	using a_tran_blob;
			
else
	delete from tes_ord_ven_note
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno and
				num_registrazione=:al_numero and
				des_nota like :ls_like and
				(disabilitato=0 or disabilitato is null)
	using a_tran_blob;
				
end if


if a_tran_blob.sqlcode<0 then
	as_errore = ls_tipologia + "Errore in cancellazione documenti  "+ as_like + " uniti: "+a_tran_blob.sqlerrtext
	return -1
end if


//inserisci il file risultato dell'unione ---------------------------------------------------
ib_attiva_max_dim = false
li_ret = uof_inserisci_file_tran(ls_dest_file_2, ai_anno, al_numero, al_riga, a_tran_blob, as_errore)
ib_attiva_max_dim = true

if li_ret < 0 then
	return -1
end if


//cancella file destinazione dal disco ---------------------------------------------------
uof_cancella_file(ls_dest_file_2, as_errore)


//cancella files temporanei  dal disco --------------------------------------------------
uof_cancella_files(as_files[])


return 0
end function

public function integer uof_estrai_riga_ordine (string as_nome_file, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref long al_prog_riga, ref string as_errore);string				ls_tipo_file, ls_tipi_previsti, ls_array[], ls_temp
long				ll_count, ll_pos, ll_barcode
integer			li_ret


ls_tipo_file = left(as_nome_file, 1)

choose case ls_tipo_file
	//##################################################################################################
	case "P"			//barcode di produzione  (es.  P12453.pdf   P12345_nnn.pdf)
		
		ll_pos = pos(as_nome_file, "_", 2)
		
		if not ll_pos > 0 then		//se questo If non è verificato siamo nel caso ad esempio P123456_001.pdf
		//cerco la presenza del punto separatore tra nome file ed estensione
			//es. P123456.pdf
			ll_pos = pos(as_nome_file, ".", 2)
			
			if not ll_pos>0 then
				//c'è qualcosa che non va
				as_errore = "Estrazione barcode produzione fallita o non numerico: file '" + as_nome_file + "'"
				return 1
			end if
		end if
		
		ls_temp = mid(as_nome_file, 2, ll_pos - 2)
		if isnumber(ls_temp) then
			ll_barcode = long(ls_temp)
		else
			as_errore = "Estrazione barcode produzione fallita o non numerico: file '" + as_nome_file + "'"
			return 1
		end if
		
		li_ret = wf_estrai_riga_da_barcode(ll_barcode, ai_anno_registrazione, al_num_registrazione, al_prog_riga, as_errore)
		if li_ret <> 0 then
			as_errore += " : file '" + as_nome_file + "'"
			return li_ret
		end if
		
		iuo_log.log("Estratta riga ordine vendita " + string(ai_anno_registrazione) + "/" + string(al_num_registrazione) + "/" + string(al_prog_riga) + " da barcode di produzione " +string(ll_barcode))
			
			
	//##################################################################################################
	case "T", "O"			//testata ordine vendita: assegnare alla prima riga ordine  -  dettaglio riga ordine
		//es. T2013012568.pdf    oppure   O20130125680020.pdf
		ls_temp = mid(as_nome_file, 2, 4)
		if isnumber(ls_temp) then
			ai_anno_registrazione = integer(ls_temp)
		else
			as_errore = "Estrazione anno ordine vendita fallita o non numerico: file '" + as_nome_file + "'"
			return 1
		end if
		
		ls_temp = mid(as_nome_file, 6, 6)
		if isnumber(ls_temp) then
			al_num_registrazione = long(ls_temp)
		else
			as_errore = "Estrazione numero ordine vendita fallita o non numerica: file '" + as_nome_file + "'"
			return 1
		end if
		
		if ls_tipo_file = "O" then
			//---------------------------------------------------------------------------
			//caso in cui viene specificato nel nome del file anno numero e riga
			ls_temp = mid(as_nome_file, 12, 4)
			if isnumber(ls_temp) then
				al_prog_riga = long(ls_temp)
			end if
			
			//controlla che la riga esista
			li_ret = wf_controlla_esistenza_riga(ai_anno_registrazione, al_num_registrazione, al_prog_riga, as_errore)
			if li_ret <> 0 then
				as_errore += " : file '" + as_nome_file + "'"
				return li_ret
			end if
			
		else
			li_ret = wf_controlla_esistenza_ordine(ai_anno_registrazione, al_num_registrazione, "TESORDVEN", as_errore)
			if li_ret <> 0 then
				as_errore += " : file '" + as_nome_file + "'"
				return li_ret
			end if
			//---------------------------------------------------------------------------
			//18/03/2014: salva nella tes_ord_ven_note, quindi non occorre + la prima riga (SR Varie_per_produzione)
			//caso in cui viene specificato nel nome del file SOLO anno e numero
			//estrai la prima riga dell'ordine, se disponibile
			setnull(al_prog_riga)
		
		end if
	
	//##################################################################################################
	case "A"	//testata ordine vendita
		//es. A2013012568.pdf
		ls_temp = mid(as_nome_file, 2, 4)
		if isnumber(ls_temp) then
			ai_anno_registrazione = integer(ls_temp)
		else
			as_errore = "Estrazione anno ordine acquisto fallita o non numerico: file '" + as_nome_file + "'"
			return 1
		end if
		
		ls_temp = mid(as_nome_file, 6, 6)
		if isnumber(ls_temp) then
			al_num_registrazione = long(ls_temp)
		else
			as_errore = "Estrazione numero ordine acquisto fallita o non numerica: file '" + as_nome_file + "'"
			return 1
		end if
		
		li_ret = wf_controlla_esistenza_ordine(ai_anno_registrazione, al_num_registrazione, "TESORDACQ", as_errore)
		if li_ret <> 0 then
			as_errore += " : file '" + as_nome_file + "'"
			return li_ret
		end if
		
		setnull(al_prog_riga)
	
	
	//##################################################################################################
	case else
		//caso non previsto
		as_errore = "La prima lettera del nome del file '" + as_nome_file + "' non è tra quelli previsti ("+ls_tipi_previsti+")"
		return 1
		
end choose


return 0
end function

public function integer wf_controlla_esistenza_ordine (integer ai_anno_registrazione, long al_num_registrazione, string as_tipo, ref string as_errore);long			ll_count

//controlla che la riga esista
setnull(ll_count)

choose case as_tipo

	case "TESORDVEN"
		select count(*)
		into :ll_count
		from tes_ord_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno_registrazione and
					num_registrazione=:al_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore controllo esistenza testata ordine vendita (" + string(ai_anno_registrazione) + "/" + string(al_num_registrazione)+ ") : " + sqlca.sqlerrtext
			return -1
			
		elseif ll_count<=0 or isnull(ll_count) then
			as_errore = "Testata ordine vendita " + string(ai_anno_registrazione) + "/" + string(al_num_registrazione) + " non trovata."
			return 1
			
		end if
		
		
		case "TESORDACQ"
		select count(*)
		into :ll_count
		from tes_ord_acq
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno_registrazione and
					num_registrazione=:al_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore controllo esistenza testata ordine acquisto (" + string(ai_anno_registrazione) + "/" + string(al_num_registrazione)+ ") : " + sqlca.sqlerrtext
			return -1
			
		elseif ll_count<=0 or isnull(ll_count) then
			as_errore = "Testata ordine acquisto " + string(ai_anno_registrazione) + "/" + string(al_num_registrazione) + " non trovata."
			return 1
			
		end if
		

end choose

return 0
end function

public function unsignedlong uof_inserisci_file_testata (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, string as_tipo, ref string as_errore);long				ll_cont, ll_prog_mimetype, ll_len
string				ls_cod_nota, ls_dir, ls_file, ls_ext, ls_cont
blob				lb_note_esterne

//18/03/2014 Donato: se al_prog_riga_ord_ven è NULL allora salvo in tes_ord_ven_note (SR_varie_per_produzione)

choose case as_tipo
		
	case "TESORDVEN"
		select 	max(cod_nota) 
		into 		:ls_cont
		from 		tes_ord_ven_note
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					isnumeric(cod_nota)=1;
	
	case "TESORDACQ"
		select 	max(cod_nota) 
		into 		:ls_cont
		from 		tes_ord_acq_note
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					isnumeric(cod_nota)=1;
		
end choose
				

if isnull(ls_cont) or ls_cont="" then ls_cont = "0"
ll_cont = long(ls_cont)
ll_cont += 1
//----------------------------------------------------------------------



ls_cod_nota = string(ll_cont, "000") 

guo_functions.uof_file_to_blob(as_percorso_completo, lb_note_esterne)

//lunghezza in BYTE del documento in esame
ll_len = lenA(lb_note_esterne)

if ib_attiva_max_dim then
	if il_maxKB>0 then
		if ll_len > il_maxKB * 1024 then
			//documento oltre la grandezza massima pre-stabilita, quindi saltalo, ma non interrompere la procedura
			as_errore = "Attenzione: grandezza file (" + string(ll_len) + " BYTE) superiore a quella prestabilita ("+string(il_maxKB * 1024)+" BYTE) come da parametro multiazienda MLD"
			return 1
		end if
	end if
end if


guo_functions.uof_get_file_info( as_percorso_completo, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;

choose case as_tipo
	//########################################################
	case "TESORDVEN"
		insert into tes_ord_ven_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_nota,   
				  des_nota,   
				  note,
				  prog_mimetype)  
		values ( :s_cs_xx.cod_azienda,
				  :ai_anno_registrazione,   
				  :al_num_registrazione,   
				  :ls_cod_nota,   
				  :ls_file,   
				  null,
				 :ll_prog_mimetype);

	if sqlca.sqlcode = 0 then
		updateblob tes_ord_ven_note
		set note_esterne = :lb_note_esterne
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				cod_nota = :ls_cod_nota;
				
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore nell'inserimento del documento digitale in testata ordine vendita: " + sqlca.sqlerrtext
			return -1
		end if
		
	else
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore nell'inserimento del documento digitale in testata ordine vendita: " + sqlca.sqlerrtext
			return -1
		end if
	end if
	
	//########################################################
	case "TESORDACQ"
		insert into tes_ord_acq_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_nota,   
				  des_nota,   
				  note,
				  prog_mimetype)  
		values ( :s_cs_xx.cod_azienda,
				  :ai_anno_registrazione,   
				  :al_num_registrazione,   
				  :ls_cod_nota,   
				  :ls_file,   
				  null,
				 :ll_prog_mimetype);

	if sqlca.sqlcode = 0 then
	
		updateblob tes_ord_acq_note
		set note_esterne = :lb_note_esterne
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				cod_nota = :ls_cod_nota;
				
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore nell'inserimento del documento digitale in testata ordine acquisto: " + sqlca.sqlerrtext
			return -1
		end if
		
	else
		
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore nell'inserimento del documento digitale in testata ordine acquisto: " + sqlca.sqlerrtext
			return -1
		end if
	end if
	
end choose

return 0
end function

public function unsignedlong uof_inserisci_file_testata_tran (string as_percorso_completo, integer ai_anno_registrazione, long al_num_registrazione, string as_tipo, ref transaction at_tran, ref string as_errore);long				ll_cont, ll_prog_mimetype, ll_len
string				ls_cod_nota, ls_dir, ls_file, ls_ext, ls_cont
blob				lb_note_esterne

//18/03/2014 Donato: se al_prog_riga_ord_ven è NULL allora salvo in tes_ord_ven_note (SR_varie_per_produzione)

if not isvalid(at_tran) then
	if uof_crea_transazione_docs(at_tran, as_errore) < 0 then
		return -1
	end if
end if


choose case as_tipo
		
	case "TESORDVEN"
		select 	max(cod_nota) 
		into 		:ls_cont
		from 		tes_ord_ven_note
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					isnumeric(cod_nota)=1
		using at_tran;
		
	
	case "TESORDACQ"
		select 	max(cod_nota) 
		into 		:ls_cont
		from 		tes_ord_acq_note
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					isnumeric(cod_nota)=1
		using at_tran;
		
end choose
		

if isnull(ls_cont) or ls_cont="" then ls_cont = "0"
ll_cont = long(ls_cont)
ll_cont += 1
//----------------------------------------------------------------------

ls_cod_nota = string(ll_cont, "000") 

guo_functions.uof_file_to_blob(as_percorso_completo, lb_note_esterne)

//lunghezza in BYTE del documento in esame
ll_len = lenA(lb_note_esterne)

if ib_attiva_max_dim then
	if il_maxKB>0 then
		if ll_len > il_maxKB * 1024 then
			//documento oltre la grandezza massima pre-stabilita, quindi saltalo, ma non interrompere la procedura
			as_errore = "Attenzione: grandezza file (" + string(ll_len) + " BYTE) superiore a quella prestabilita ("+string(il_maxKB * 1024)+" BYTE) come da parametro multiazienda MLD"
			return 1
		end if
	end if
end if


guo_functions.uof_get_file_info( as_percorso_completo, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;


choose case as_tipo
		
	case "TESORDVEN"
		insert into tes_ord_ven_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_nota,   
				  des_nota,   
				  note,
				  prog_mimetype)  
		values ( :s_cs_xx.cod_azienda,
				  :ai_anno_registrazione,   
				  :al_num_registrazione,   
				  :ls_cod_nota,   
				  :ls_file,   
				  null,
				 :ll_prog_mimetype)
		using at_tran;
		
		if at_tran.sqlcode = 0 then
		
			updateblob tes_ord_ven_note
			set note_esterne = :lb_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					cod_nota = :ls_cod_nota
			using at_tran;
					
			if at_tran.sqlcode <> 0 then
				as_errore = "Errore nell'inserimento del documento digitale in testata.~r~n" + at_tran.sqlerrtext
				return -1
			end if
			
		else
			
			if at_tran.sqlcode <> 0 then
				as_errore = "Errore nell'inserimento del documento digitale in testata.~r~n" + at_tran.sqlerrtext
				return -1
			end if
		end if
		
		
	case "TESORDACQ"
		insert into tes_ord_acq_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_nota,   
				  des_nota,   
				  note,
				  prog_mimetype)  
		values ( :s_cs_xx.cod_azienda,
				  :ai_anno_registrazione,   
				  :al_num_registrazione,   
				  :ls_cod_nota,   
				  :ls_file,   
				  null,
				 :ll_prog_mimetype)
		using at_tran;
		
		if at_tran.sqlcode = 0 then
		
			updateblob tes_ord_acq_note
			set note_esterne = :lb_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					cod_nota = :ls_cod_nota
			using at_tran;
					
			if at_tran.sqlcode <> 0 then
				as_errore = "Errore nell'inserimento del documento digitale in testata.~r~n" + at_tran.sqlerrtext
				return -1
			end if
			
		else
			
			if at_tran.sqlcode <> 0 then
				as_errore = "Errore nell'inserimento del documento digitale in testata.~r~n" + at_tran.sqlerrtext
				return -1
			end if
		end if
		
end choose

return 0
end function

on uo_service_doc_ord_ven.create
call super::create
end on

on uo_service_doc_ord_ven.destroy
call super::destroy
end on

