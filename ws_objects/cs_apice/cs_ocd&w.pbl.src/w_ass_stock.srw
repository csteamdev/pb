﻿$PBExportHeader$w_ass_stock.srw
$PBExportComments$Finestra Assegnazione Stock
forward
global type w_ass_stock from w_cs_xx_risposta
end type
type tab_1 from tab within w_ass_stock
end type
type tabpage_1 from userobject within tab_1
end type
type ddlb_1 from dropdownlistbox within tabpage_1
end type
type st_1 from statictext within tabpage_1
end type
type em_quan_da_assegnare from editmask within tabpage_1
end type
type st_3 from statictext within tabpage_1
end type
type em_quan_assegnata from editmask within tabpage_1
end type
type dw_ext_ass_stock from datawindow within tabpage_1
end type
type cb_2 from commandbutton within tabpage_1
end type
type cb_1 from commandbutton within tabpage_1
end type
type cb_4 from commandbutton within tabpage_1
end type
type tabpage_1 from userobject within tab_1
ddlb_1 ddlb_1
st_1 st_1
em_quan_da_assegnare em_quan_da_assegnare
st_3 st_3
em_quan_assegnata em_quan_assegnata
dw_ext_ass_stock dw_ext_ass_stock
cb_2 cb_2
cb_1 cb_1
cb_4 cb_4
end type
type tabpage_2 from userobject within tab_1
end type
type dw_lista_bolle_pronte from datawindow within tabpage_2
end type
type cb_nuova_bolla from commandbutton within tabpage_2
end type
type st_2 from statictext within tabpage_2
end type
type cb_aggiungi_a_bolla from commandbutton within tabpage_2
end type
type dw_ext_dati_bolla from datawindow within tabpage_2
end type
type cb_5 from commandbutton within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
st_2 st_2
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
cb_5 cb_5
end type
type tabpage_3 from userobject within tab_1
end type
type dw_lista_fatture_pronte from datawindow within tabpage_3
end type
type cb_nuova_fattura from commandbutton within tabpage_3
end type
type cb_aggiungi_a_fattura from commandbutton within tabpage_3
end type
type dw_ext_dati_fattura from datawindow within tabpage_3
end type
type st_4 from statictext within tabpage_3
end type
type cb_3 from commandbutton within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_lista_fatture_pronte dw_lista_fatture_pronte
cb_nuova_fattura cb_nuova_fattura
cb_aggiungi_a_fattura cb_aggiungi_a_fattura
dw_ext_dati_fattura dw_ext_dati_fattura
st_4 st_4
cb_3 cb_3
end type
type tabpage_4 from userobject within tab_1
end type
type dw_lista_comm_spedizione from datawindow within tabpage_4
end type
type em_quan_spedizione from editmask within tabpage_4
end type
type st_5 from statictext within tabpage_4
end type
type cb_anticipo from commandbutton within tabpage_4
end type
type tabpage_4 from userobject within tab_1
dw_lista_comm_spedizione dw_lista_comm_spedizione
em_quan_spedizione em_quan_spedizione
st_5 st_5
cb_anticipo cb_anticipo
end type
type tab_1 from tab within w_ass_stock
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
end type
end forward

global type w_ass_stock from w_cs_xx_risposta
integer width = 3488
integer height = 2040
string title = "Assegnazione Stock"
tab_1 tab_1
end type
global w_ass_stock w_ass_stock

type variables
boolean ib_assegna=FALSE, ib_anticipo=FALSE
string is_cod_tipo_det_ven, is_cod_deposito, &
         is_cod_ubicazione,is_cod_prodotto
string is_destinazione
long il_anno_commessa, il_num_commessa
long il_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven
double id_quan_assegnabile

longlong il_prog_sessione

end variables

forward prototypes
public function integer wf_mat_prime_commessa_stock (string as_cod_ubicazione, string as_cod_lotto, datetime adt_data_stock, long al_prog_stock, double ad_quan_da_assegnare, string as_cod_deposito)
public function integer wf_det_ord_ven (double ad_quan_in_evasione)
public subroutine wf_anticipo_spedizioni (boolean fb_aggiungi)
public function integer wf_mov_anticipo (ref long fl_anno_reg_mov_mag, ref long fl_num_reg_mov_mag, ref string fs_messaggio)
public function longlong wf_get_prog_sessione ()
public function integer wf_carica_stock (string as_cod_prodotto, string as_cod_deposito, boolean ab_stock_con_giacenza)
public function integer wf_evas_ord_ven (string as_cod_ubicazione, string as_cod_lotto, datetime adt_data_stock, long al_prog_stock, double ad_quan_da_assegnare, string as_cod_deposito, longlong al_prog_sessione)
end prototypes

public function integer wf_mat_prime_commessa_stock (string as_cod_ubicazione, string as_cod_lotto, datetime adt_data_stock, long al_prog_stock, double ad_quan_da_assegnare, string as_cod_deposito);integer li_anno_commessa
long    ll_prog_riga,ll_num_commessa
string  ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito

li_anno_commessa = integer(s_cs_xx.parametri.parametro_ul_1)
ll_num_commessa = s_cs_xx.parametri.parametro_ul_2
ll_prog_riga = s_cs_xx.parametri.parametro_ul_3
ls_cod_tipo_movimento = s_cs_xx.parametri.parametro_s_10
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1   
ls_cod_deposito = as_cod_deposito

INSERT INTO mat_prime_commessa_stock
         			 (cod_azienda,   
           			  anno_commessa,   
			           num_commessa,   
         			  prog_riga,   
			           cod_prodotto,   
			           cod_deposito,   
			           cod_ubicazione,   
			           cod_lotto,   
			           data_stock,   
			           prog_stock,   
			           anno_registrazione,   
			           num_registrazione,   
			           quan_assegnata,   
			           cod_tipo_movimento,
						  anno_reg_des_mov,
						  num_reg_des_mov,
						  quan_assegnata_copia )  
	    VALUES 		 (:s_cs_xx.cod_azienda,   
	     		        :li_anno_commessa,   
 	            	  :ll_num_commessa,   
		              :ll_prog_riga,   
	                 :ls_cod_prodotto,   
     		           :ls_cod_deposito,   
          		     :as_cod_ubicazione,   
		              :as_cod_lotto,   
	                 :adt_data_stock,   
	                 :al_prog_stock,   
     		           null,   
          		     null,   
	                 :ad_quan_da_assegnare,   
     		           :ls_cod_tipo_movimento,
						  null,
						  null,
						  :ad_quan_da_assegnare ) ;


if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento mat_prime_commessa_stock.", &
				  exclamation!, ok!)
	rollback;
	return -1
end if

return 0
end function

public function integer wf_det_ord_ven (double ad_quan_in_evasione);string ls_cod_prodotto_raggruppato
dec{4} ld_det_ord_ven_quan_ordine, ld_det_ord_ven_quan_in_evasione, ld_quan_raggruppo1, ld_quan_raggruppo2

select quan_ordine, quan_in_evasione
into   :ld_det_ord_ven_quan_ordine, :ld_det_ord_ven_quan_in_evasione
from   det_ord_ven
 where cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :s_cs_xx.parametri.parametro_ul_1 and  
		 num_registrazione = :s_cs_xx.parametri.parametro_ul_2 and
		 prog_riga_ord_ven = :s_cs_xx.parametri.parametro_ul_3;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di ricerca dettaglio ordine.", exclamation!, ok!)
	rollback;
	return -1
end if
		
if ld_det_ord_ven_quan_ordine - ld_det_ord_ven_quan_in_evasione < ad_quan_in_evasione then
	update anag_prodotti  
		set quan_impegnata = quan_impegnata - (:ld_det_ord_ven_quan_ordine - :ld_det_ord_ven_quan_in_evasione),
			 quan_assegnata = quan_assegnata + :ad_quan_in_evasione
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :s_cs_xx.parametri.parametro_s_1;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
		rollback;
		return -1
	end if
			 
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :s_cs_xx.parametri.parametro_s_1;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo1 = f_converti_qta_raggruppo(s_cs_xx.parametri.parametro_s_1, (ld_det_ord_ven_quan_ordine - ld_det_ord_ven_quan_in_evasione), "M")
		ld_quan_raggruppo2 = f_converti_qta_raggruppo(s_cs_xx.parametri.parametro_s_1, ad_quan_in_evasione, "M")
		
		update anag_prodotti  
			set quan_impegnata = quan_impegnata - :ld_quan_raggruppo1,
				 quan_assegnata = quan_assegnata + :ld_quan_raggruppo2
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
			rollback;
			return -1
		end if
	end if
			 
else
	update anag_prodotti  
		set quan_impegnata = quan_impegnata - :ad_quan_in_evasione,
			 quan_assegnata = quan_assegnata + :ad_quan_in_evasione
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :s_cs_xx.parametri.parametro_s_1;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
		rollback;
		return -1
	end if
			 
			 
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :s_cs_xx.parametri.parametro_s_1;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo1 = f_converti_qta_raggruppo(s_cs_xx.parametri.parametro_s_1, ad_quan_in_evasione, "M")
		
		update anag_prodotti  
			set quan_impegnata = quan_impegnata - :ld_quan_raggruppo1,
				 quan_assegnata = quan_assegnata + :ld_quan_raggruppo1
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
			rollback;
			return -1
		end if
			 
	end if			 
			 
end if


update det_ord_ven
	set det_ord_ven.quan_in_evasione = :ad_quan_in_evasione
 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :s_cs_xx.parametri.parametro_ul_1 and  
		 num_registrazione = :s_cs_xx.parametri.parametro_ul_2 and
		 prog_riga_ord_ven = :s_cs_xx.parametri.parametro_ul_3;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", exclamation!, ok!)
	rollback;
	return -1
end if

return 0
end function

public subroutine wf_anticipo_spedizioni (boolean fb_aggiungi);string ls_messaggio, ls_cod_prodotto , ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
       ls_cod_tipo_ord_ven,ls_cod_prodotto_raggruppato
		 
long   ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_anno_commessa, ll_num_commessa, ll_prog_stock, ll_anno_esercizio, &
		 ll_anno_bolla, ll_num_bolla, ll_prog_riga_comm, ll_cont, ll_prog_riga_bol_ven
		 
dec{4} ld_val_movimento, ld_quan_assegnabile,ld_quan_movimento,ld_quan_ordinata, ld_quan_in_evasione, ld_quan_evasa, &
		 ld_quan_assegnazione, ld_quan_da_assegnare, ld_quan_raggruppo
		 
datetime ldt_data_stock




if wf_mov_anticipo(ll_anno_reg_mov_mag, ll_num_reg_mov_mag,ls_messaggio) = -1 then
	g_mb.messagebox("APICE","Errore durante creazione movimento di anticipo spedizione prodotti" + "~r~n" + ls_messaggio, StopSign!) 
	ROLLBACK;
	return
end if

select cod_prodotto,
       cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 data_stock,
		 prog_stock,
		 quan_movimento,
		 val_movimento
into   :ls_cod_prodotto,
       :ls_cod_deposito,
		 :ls_cod_ubicazione,
		 :ls_cod_lotto,
		 :ldt_data_stock,
		 :ll_prog_stock,
		 :ld_quan_movimento,
		 :ld_val_movimento
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_reg_mov_mag and
       num_registrazione  = :ll_num_reg_mov_mag ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca del movimento carico anticipo. Dettaglio errore" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

ll_anno_commessa = tab_1.tabpage_4.dw_lista_comm_spedizione.getitemnumber(tab_1.tabpage_4.dw_lista_comm_spedizione.getrow(),"anno_commessa")
ll_num_commessa = tab_1.tabpage_4.dw_lista_comm_spedizione.getitemnumber(tab_1.tabpage_4.dw_lista_comm_spedizione.getrow(),"num_commessa")
ll_prog_riga_comm = tab_1.tabpage_4.dw_lista_comm_spedizione.getitemnumber(tab_1.tabpage_4.dw_lista_comm_spedizione.getrow(),"prog_riga")

select quan_ordine,
		 quan_in_evasione,
		 quan_evasa
into   :ld_quan_ordinata,
		 :ld_quan_in_evasione,
		 :ld_quan_evasa
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione and
		 prog_riga_ord_ven = :il_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati della riga d'ordine. Dettaglio errore:" + sqlca.sqlerrtext,StopSign!)
	rollback;
   return
end if

ld_quan_assegnabile = ld_quan_ordinata - ld_quan_in_evasione - ld_quan_evasa
if ld_quan_movimento > ld_quan_assegnabile then
	ld_quan_assegnazione = ld_quan_assegnabile
else
	ld_quan_assegnazione = ld_quan_movimento
end if

// ------------------------  aggiornamento anag_prodotti -------------------------------
update anag_prodotti  
	set quan_impegnata = quan_impegnata - :ld_quan_assegnazione,
		 quan_assegnata = quan_assegnata + :ld_quan_assegnazione
 where cod_azienda  = :s_cs_xx.cod_azienda and  
		 cod_prodotto = :ls_cod_prodotto;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", stopsign!)
	rollback;
	return
end if


// enme 08/1/2006 gestione prodotto raggruppato
setnull(ls_cod_prodotto_raggruppato)

select cod_prodotto_raggruppato
into   :ls_cod_prodotto_raggruppato
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;
		 
if not isnull(ls_cod_prodotto_raggruppato) then
	
	ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_assegnazione, "M")
	
	update anag_prodotti  
		set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
	 where cod_azienda  = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :ls_cod_prodotto_raggruppato;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", stopsign!)
		rollback;
		return
	end if
end if




// -------------------------aggiornamento ordine ---------------------------------------

update det_ord_ven
	set quan_in_evasione   = quan_in_evasione + :ld_quan_assegnazione
 where cod_azienda        = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :il_anno_registrazione and  
		 num_registrazione  = :il_num_registrazione and
		 prog_riga_ord_ven  = :il_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", stopsign!)
	rollback;
	return
end if
			
//-------------------------- aggiornamento stock ------------------------------------------

update stock  
	set stock.quan_assegnata = stock.quan_assegnata + :ld_quan_assegnazione
 where stock.cod_azienda    = :s_cs_xx.cod_azienda and
		 stock.cod_prodotto   = :ls_cod_prodotto and 
		 stock.cod_deposito   = :ls_cod_deposito and
		 stock.cod_ubicazione = :ls_cod_ubicazione and
		 stock.cod_lotto      = :ls_cod_lotto and
		 stock.data_stock     = :ldt_data_stock and
		 stock.prog_stock     = :ll_prog_stock;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento lotto.", stopsign!)
	rollback;
	return
end if

// --------------------------- creazione bolla da ordine -----------------------------------
if not fb_aggiungi then
	setnull(ll_anno_bolla)
	setnull(ll_num_bolla)
else
	ll_anno_bolla = tab_1.tabpage_2.dw_lista_bolle_pronte.getitemnumber(tab_1.tabpage_2.dw_lista_bolle_pronte.getrow(),"tes_bol_ven_anno_registrazione")
	ll_num_bolla = tab_1.tabpage_2.dw_lista_bolle_pronte.getitemnumber(tab_1.tabpage_2.dw_lista_bolle_pronte.getrow(),"tes_bol_ven_num_registrazione")
end if

uo_generazione_documenti luo_generazione_documenti
luo_generazione_documenti = CREATE uo_generazione_documenti

luo_generazione_documenti.id_quan_anticipo = ld_quan_movimento
luo_generazione_documenti.id_quan_assegnazione = ld_quan_assegnazione
luo_generazione_documenti.is_cod_deposito = ls_cod_deposito
luo_generazione_documenti.is_cod_ubicazione = ls_cod_ubicazione
luo_generazione_documenti.is_cod_lotto = ls_cod_lotto
luo_generazione_documenti.idt_data_stock = ldt_data_stock
luo_generazione_documenti.il_prog_stock = ll_prog_stock
luo_generazione_documenti.il_anno_mov_mag_ant = ll_anno_reg_mov_mag
luo_generazione_documenti.il_num_mov_mag_ant = ll_num_reg_mov_mag
if luo_generazione_documenti.uof_crea_aggiungi_bolla_anticipo(il_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven, ll_anno_bolla, ll_num_bolla, ll_prog_riga_bol_ven,ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_generazione_documenti
	rollback;
	return
end if
destroy luo_generazione_documenti
				 
// ------------------------- aggiorno tabella det_anag_comm_anticipi -----------------------				 
setnull(ll_cont)

select count(*)
into   :ll_cont
from   det_anag_comm_anticipi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa and
		 prog_riga     = :ll_prog_riga_comm  ;
if isnull(ll_cont) or ll_cont = 0 or sqlca.sqlcode <> 0 then
	ll_cont = 10
else
	select max(prog_riga_anticipo)
	into   :ll_cont
	from   det_anag_comm_anticipi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_commessa = :ll_anno_commessa and
	       num_commessa  = :ll_num_commessa and
			 prog_riga     = :ll_prog_riga_comm  ;
	if isnull(ll_cont) or ll_cont = 0 or sqlca.sqlcode <> 0 then
		ll_cont = 10
	else
		ll_cont = ll_cont + 10
	end if
end if
		
insert into det_anag_comm_anticipi
         ( cod_azienda,   
           anno_commessa,   
           num_commessa,   
           prog_riga,   
           prog_riga_anticipo,   
           quan_anticipo,   
           anno_reg_mov_mag,   
           num_reg_mov_mag,   
           anno_reg_bol_ven,   
           num_reg_bol_ven,   
           prog_riga_bol_ven )  
   values (:s_cs_xx.cod_azienda,   
           :ll_anno_commessa,   
           :ll_num_commessa,   
           :ll_prog_riga_comm,   
           :ll_cont,   
           :ld_quan_movimento,   
           :ll_anno_reg_mov_mag,   
           :ll_num_reg_mov_mag,   
           :ll_anno_bolla,   
           :ll_num_bolla,   
           :ll_prog_riga_bol_ven )  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in inserimento in dettagli anticipi commesse. Dettaglio errore" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if


update det_anag_commesse
set    quan_anticipo = quan_anticipo + :ld_quan_movimento
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa and
		 prog_riga = :ll_prog_riga_comm;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in inserimento in dettagli anticipi commesse. Dettaglio errore" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if
COMMIT;
g_mb.messagebox("ANTICIPO SPEDIZIONI","Elaborazione eseguita con successo! ~r~nGenerato dettagli in Bolla " + string(ll_anno_bolla) + "/" + string(ll_num_bolla), Information!)
end subroutine

public function integer wf_mov_anticipo (ref long fl_anno_reg_mov_mag, ref long fl_num_reg_mov_mag, ref string fs_messaggio);string ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_referenza, ls_cod_ubicazione[], ls_cod_lotto[], &
       ls_cod_fornitore[], ls_cod_cliente[], ls_cod_deposito_versamento, ls_cod_tipo_commessa
long   ll_num_registrazione, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_progr_stock[], ll_anno_reg_mov_mag[], &
       ll_num_reg_mov_mag[], ll_anno_commessa, ll_num_commessa, ll_num_sottocomm, ll_num_documento, ll_anno_reg_mov[], &
		 ll_num_reg_mov[], ll_cont, ll_anno_mov_ant, ll_num_mov_ant
double ld_quantita_anticipo, ld_valore, ld_quan_ordinata, ld_quan_in_evasione, ld_quan_evasa
datetime ldt_data_registrazione, ldt_data_stock[], ldt_oggi, ldt_data_documento

uo_magazzino luo_mag


setnull(fs_messaggio)
setnull(fl_anno_reg_mov_mag)
setnull(fl_num_reg_mov_mag)
ldt_oggi = datetime(today(), 00:00:00)


select stringa
into   :ls_cod_tipo_movimento
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and
       parametri_azienda.cod_parametro = 'TMC';
if sqlca.sqlcode <> 0 then
	fs_messaggio= "Parametro TMC mancante in parametri azienda: impossibile proseguire"
   return -1
end if

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ldt_data_stock[1])
setnull(ll_progr_stock[1])
setnull(ls_cod_fornitore[1])
setnull(ll_num_documento)
setnull(ldt_data_documento)
setnull(ls_referenza)
ld_valore   = 0
ll_anno_commessa = tab_1.tabpage_4.dw_lista_comm_spedizione.getitemnumber(tab_1.tabpage_4.dw_lista_comm_spedizione.getrow(),"anno_commessa")
ll_num_commessa = tab_1.tabpage_4.dw_lista_comm_spedizione.getitemnumber(tab_1.tabpage_4.dw_lista_comm_spedizione.getrow(),"num_commessa")
ll_num_sottocomm = tab_1.tabpage_4.dw_lista_comm_spedizione.getitemnumber(tab_1.tabpage_4.dw_lista_comm_spedizione.getrow(),"prog_riga")
ld_quantita_anticipo = double(tab_1.tabpage_4.em_quan_spedizione.text)
if ld_quantita_anticipo = 0 then
	fs_messaggio = "Attenzione quantità anticipo a zero: impossibile proseguire!"
	return -1
end if

select cod_prodotto,
       cod_tipo_commessa
into   :ls_cod_prodotto,
       :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
		 num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca dati commessa: impossibile proseguire"
   return -1
end if

select cod_deposito_versamento
into   :ls_cod_deposito_versamento
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_commessa = :ls_cod_tipo_commessa;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca tipo commessa: impossibile proseguire"
   return -1
end if
if isnull(ls_cod_tipo_commessa) then
	fs_messaggio = "Non è stato impostato il deposito di versamento in tipi commesse: impossibile proseguire"
   return -1
end if

select quan_ordine,
		 quan_in_evasione,
		 quan_evasa
into   :ld_quan_ordinata,
		 :ld_quan_in_evasione,
		 :ld_quan_evasa
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione and
		 prog_riga_ord_ven = :il_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca dati ordine: impossibile proseguire"
   return -1
end if
	
if (ld_quan_ordinata - ld_quan_in_evasione - ld_quan_evasa) < ld_quantita_anticipo then
	if g_mb.messagebox("Anticipo Spedizioni","Quantità anticipo maggiore della quantità residua da evadere dall'ordine: continuo?",Question!,YesNo!, 1) = 2 then
		return -1
	end if
end if

ls_cod_lotto[1] = string(ll_anno_commessa, "####") + string(ll_num_commessa,"######")

select cod_cliente,
       cod_deposito,
       cod_ubicazione
into   :ls_cod_cliente[1],
		 :ls_cod_deposito[1],
       :ls_cod_ubicazione[1]
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione  = :il_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca deposito e ubicazione in testata ordine: impossibile proseguire"
   return -1
end if

setnull(ll_cont)
select min(prog_riga_anticipo)
into   :ll_cont
from   det_anag_comm_anticipi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa ;
		 
if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont <> 0 then
	select anno_reg_mov_mag,
	       num_reg_mov_mag
	into   :ll_anno_mov_ant, 
	       :ll_num_mov_ant
	from   det_anag_comm_anticipi
	where  cod_azienda        = :s_cs_xx.cod_azienda and
			 anno_commessa      = :ll_anno_commessa and
			 num_commessa       = :ll_num_commessa and
			 prog_riga_anticipo = :ll_cont;
	if sqlca.sqlcode = 0 then
		select cod_deposito,
		       cod_ubicazione,
				 cod_lotto,
				 data_stock,
				 prog_stock
		into   :ls_cod_deposito[1],
		       :ls_cod_ubicazione[1],
				 :ls_cod_lotto[1],
				 :ldt_data_stock[1],
				 :ll_progr_stock[1]
		from   mov_magazzino
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_mov_ant and
				 num_registrazione = :ll_num_mov_ant ;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca stock primo movimento di anticipo"
			return -1
		end if
	end if
end if
		 
if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
										ls_cod_prodotto, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_progr_stock[], &
										ls_cod_cliente[], &
										ls_cod_fornitore[], &
										ll_anno_reg_des_mov, &
										ll_num_reg_des_mov ) = -1 then
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, &
								 ll_num_reg_des_mov, &
								 ls_cod_tipo_movimento, &
								 ls_cod_prodotto) = -1 then
	return -1
end if

luo_mag = create uo_magazzino

if luo_mag.uof_movimenti_mag ( ldt_oggi, &
							ls_cod_tipo_movimento, &
							"S", &
							ls_cod_prodotto, &
							ld_quantita_anticipo, &
							ld_valore, &
							ll_num_documento, &
							ldt_data_documento, &
							ls_referenza, &
							ll_anno_reg_des_mov, &
							ll_num_reg_des_mov, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_progr_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							ll_anno_reg_mov[], &
							ll_num_reg_mov[] ) = -1 then
	destroy luo_mag
	return -1
end if

destroy luo_mag

f_elimina_dest_mov_mag (ll_anno_reg_des_mov, &
								ll_num_reg_des_mov)
// ---------------------------------------------------------------------------------------------------------------

fl_anno_reg_mov_mag = ll_anno_reg_mov[1]
fl_num_reg_mov_mag  = ll_num_reg_mov[1]
return 0
end function

public function longlong wf_get_prog_sessione ();longlong			ll_id
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")
ll_id = longlong(ls_temp)

return ll_id
end function

public function integer wf_carica_stock (string as_cod_prodotto, string as_cod_deposito, boolean ab_stock_con_giacenza);string ls_sql, ls_errore, ls_error, ls_chiave[], ls_vuoto[], ls_where, ls_array_elementi[]
long	ll_ret, ll_return, ll_row, ll_t, ll_elemento_prog_stock,ll_i, ll_riga
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_vuoto[]
datetime ldt_data_rif, ldt_elemento_data_stock,ldt_data_riferimento
datastore 		lds_data	
uo_magazzino luo_mag



ldt_data_rif = datetime(today(),00:00:00)

ld_quant_val = ld_vuoto
ld_giacenza_stock = ld_vuoto
ld_costo_medio_stock = ld_vuoto
ld_quan_costo_medio_stock = ld_vuoto

ls_chiave = ls_vuoto

luo_mag = create uo_magazzino

if not isnull(as_cod_deposito) then  luo_mag.is_cod_depositi_in = "('" +  as_cod_deposito + "')"

ll_return = luo_mag.uof_saldo_prod_date_decimal ( as_cod_prodotto, ldt_data_rif, "", ref ld_quant_val[], ref ls_error, "S", ref ls_chiave[], ref ld_giacenza_stock[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[] )

lds_data = create datastore
lds_data.reset()

lds_data.dataobject = 'd_ds_funzioni_1_giacenza_stock'

if upperbound(ls_chiave) > 0 then
	for ll_t = 1 to upperbound(ls_chiave)

		if len( trim(ls_chiave[1])) = 0 then continue 
		
		
		g_str.explode( ls_chiave[ll_t], "-", ref ls_array_elementi[])
		ldt_elemento_data_stock = datetime( date(ls_array_elementi[5]), 00:00:00)
		ll_elemento_prog_stock = long(ls_array_elementi[4])
		
		if not isnull(ldt_data_riferimento) and ldt_elemento_data_stock < ldt_data_riferimento then continue

		ll_row = lds_data.insertrow(0)

		lds_data.setitem(ll_row, 1, as_cod_prodotto)
		lds_data.setitem(ll_row, 2, ls_array_elementi[1])
		lds_data.setitem(ll_row, 3, ls_array_elementi[2])
		lds_data.setitem(ll_row, 4, ls_array_elementi[3])
		lds_data.setitem(ll_row, 5, ldt_elemento_data_stock)
		lds_data.setitem(ll_row, 6, ll_elemento_prog_stock)
		lds_data.setitem(ll_row, 7, ld_giacenza_stock[ll_t])
	next
end if		

destroy luo_mag

lds_data.setsort("#5 D, #6 D, #4 D")
lds_data.sort()

//lds_data.dataobject = 'd_ds_funzioni_1_giacenza_stock'
w_cs_xx_mdi.SetMicroHelp("Ricerca dei lotti in corso ")	

//luo_funzioni_1 = create uo_funzioni_1
//luo_funzioni_1.uof_giacenze_stock_prodotto_deposito(as_cod_prodotto, as_cod_deposito, ldt_data_riferimento, ref lds_data)
//destroy luo_funzioni_1

tab_1.tabpage_1.dw_ext_ass_stock.reset()

for ll_i = 1 to lds_data.rowcount()
	ll_riga = tab_1.tabpage_1.dw_ext_ass_stock.insertrow(0)
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "cod_deposito", lds_data.getitemstring(ll_i,2))
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "cod_ubicazione", lds_data.getitemstring(ll_i,3))
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "cod_lotto", lds_data.getitemstring(ll_i,4))
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "data_stock", lds_data.getitemdatetime(ll_i,5))
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "prog_stock", lds_data.getitemnumber(ll_i,6))
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "quan_giacenza", lds_data.getitemnumber(ll_i,7))
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "quan_assegnata", 0)
	tab_1.tabpage_1.dw_ext_ass_stock.setitem(ll_riga, "quan_spedizione", 0)
next


w_cs_xx_mdi.SetMicroHelp("Pronto!")	

return 0


/*
string ls_sql, ls_errore, ls_cod_prodotto, ls_error, ls_chiave[], ls_vuoto[], ls_where
long	ll_ret, ll_i, ll_return, ll_row, ll_t
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_vuoto[]
datetime ldt_data_rif
uo_magazzino	luo_mag
datastore 		lds_data	


ls_sql = g_str.format( "SELECT anag_prodotti.cod_prodotto,  anag_prodotti.des_prodotto,   anag_prodotti.cod_misura_mag ,stock.cod_deposito, stock.cod_ubicazione, stock.cod_lotto, stock.data_stock, stock.prog_stock " + &
							" FROM stock " + &
							" LEFT JOIN anag_prodotti  ON anag_prodotti.cod_azienda = stock.cod_azienda AND anag_prodotti.cod_prodotto = stock.cod_prodotto " + &
							" LEFT JOIN anag_depositi ON anag_depositi.cod_azienda = stock.cod_azienda AND anag_depositi.cod_deposito = stock.cod_deposito " + &
							" WHERE anag_prodotti.cod_azienda = '$1'  ", s_cs_xx.cod_azienda)

if not isnull(as_cod_prodotto) then ls_sql += g_str.format(" AND anag_prodotti.cod_prodotto = '$1' ", dw_search.getitemstring(1,"cod_prodotto"))
if not isnull(as_cod_deposito) then ls_sql += g_str.format(" AND stock.cod_deposito = '$1' ", dw_search.getitemstring(1,"cod_deposito"))

ls_sql += " order by 1 "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
if ll_ret < 0 then
	g_mb.error(ls_errore)
	return
end if

luo_mag = Create uo_magazzino
ldt_data_rif = datetime(today(),00:00:00)

for ll_i = 1 to ll_ret
	ld_quant_val = ld_vuoto
	ld_giacenza_stock = ld_vuoto
	ld_costo_medio_stock = ld_vuoto
	ld_quan_costo_medio_stock = ld_vuoto
	
	ls_chiave = ls_vuoto
	
	ls_cod_prodotto = lds_data.getitemstring(ll_i,1)
/*	
	if not isnull(dw_search.getitemstring(1,"cod_deposito")) then 
		luo_mag.is_considera_depositi_fornitori = "I"
		luo_mag.is_cod_depositi_in = "('" +  dw_search.getitemstring(1,"cod_deposito") + "')"
	end if
*/	
	ll_return = luo_mag.uof_saldo_prod_date_decimal ( ls_cod_prodotto, ldt_data_rif, "", ref ld_quant_val[], ref ls_error, "D", ref ls_chiave[], ref ld_giacenza_stock[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[] )
	
	if upperbound(ls_chiave) > 0 then
		for ll_t = 1 to upperbound(ls_chiave)
			if ld_giacenza_stock[ll_t] <= 0 and ab_stock_con_giacenza then continue
			ll_row = insertrow(0)
			setitem(ll_row, 1, ls_chiave[ll_t])
//			setitem(ll_row, 2, ls_cod_prodotto)
//			setitem(ll_row, 3, lds_data.getitemstring(ll_i,2))
//			setitem(ll_row, 4, lds_data.getitemstring(ll_i,3))
			setitem(ll_row, 5, ld_giacenza_stock[ll_t])
		next
	end if		
next

destroy luo_mag
destroy lds_data

dw_search.object.t_wait.visible='0'
*/
end function

public function integer wf_evas_ord_ven (string as_cod_ubicazione, string as_cod_lotto, datetime adt_data_stock, long al_prog_stock, double ad_quan_da_assegnare, string as_cod_deposito, longlong al_prog_sessione);long ll_prog_riga

select max(prog_riga)
into	 :ll_prog_riga
from	 evas_ord_ven
where  evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
		 evas_ord_ven.anno_registrazione = :s_cs_xx.parametri.parametro_ul_1 and
		 evas_ord_ven.num_registrazione = :s_cs_xx.parametri.parametro_ul_2 and
		 evas_ord_ven.prog_riga_ord_ven = :s_cs_xx.parametri.parametro_ul_3;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura evas_ord_ven.", &
				  exclamation!, ok!)
	rollback;
	return -1
end if

if isnull(ll_prog_riga) then
	ll_prog_riga = 1
else
	ll_prog_riga ++
end if

insert into evas_ord_ven  
				(cod_azienda,   
				 anno_registrazione,   
				 num_registrazione,   
				 prog_riga_ord_ven,   
				 prog_riga,
				 cod_deposito,   
				 cod_ubicazione,   
				 cod_lotto,   
				 data_stock,   
				 prog_stock,   
				 cod_prodotto,   
				 quan_in_evasione,
				 cod_utente,
				 prog_sessione)  
values      (:s_cs_xx.cod_azienda,   
				 :s_cs_xx.parametri.parametro_ul_1,   
				 :s_cs_xx.parametri.parametro_ul_2,   
				 :s_cs_xx.parametri.parametro_ul_3,   
				 :ll_prog_riga,
				 :as_cod_deposito,   
				 :as_cod_ubicazione,   
				 :as_cod_lotto,   
				 :adt_data_stock,   
				 :al_prog_stock,   
				 :s_cs_xx.parametri.parametro_s_1,   
				 :ad_quan_da_assegnare,
				 :s_cs_xx.cod_utente,
				 :al_prog_sessione); 

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento evas_ord_ven.", &
				  exclamation!, ok!)
	rollback;
	return -1
end if

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_cod_tipo_bol_ven,ls_cod_tipo_fat_ven

// ------------- lettura parametri passati dalla maschera ordini -----------------------

is_cod_prodotto = s_cs_xx.parametri.parametro_s_1
is_cod_deposito = s_cs_xx.parametri.parametro_s_2
if isnull(s_cs_xx.parametri.parametro_s_3) or s_cs_xx.parametri.parametro_s_3 = "" then
	is_cod_ubicazione = "%"
else
	is_cod_ubicazione = s_cs_xx.parametri.parametro_s_3
end if
is_cod_tipo_det_ven = s_cs_xx.parametri.parametro_s_4
il_anno_commessa = s_cs_xx.parametri.parametro_d_2
il_num_commessa = s_cs_xx.parametri.parametro_d_3
il_anno_registrazione = s_cs_xx.parametri.parametro_ul_1
il_num_registrazione = s_cs_xx.parametri.parametro_ul_2
il_prog_riga_ord_ven =s_cs_xx.parametri.parametro_ul_3

// ------------- controllo se generazione bolla o fattura -----------------------------

tab_1.tabpage_2.enabled = false
tab_1.tabpage_3.enabled = false

// -------------- script di inizializzazione maschera ------------------------------
double ld_quan_da_evadere, ld_quan_prodotta

// imposto eventuale quantità proveniente da commessa di produzione

if il_anno_commessa = 0 or isnull(il_anno_commessa) then
	id_quan_assegnabile = s_cs_xx.parametri.parametro_d_1
else
	select quan_prodotta
	into   :ld_quan_prodotta  
	from   anag_commesse
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :il_anno_commessa  and  
			 num_commessa  = :il_num_commessa   ;
	if sqlca.sqlcode = 0 then
		if s_cs_xx.parametri.parametro_d_1 > ld_quan_prodotta then
			g_mb.messagebox("APICE","Residuo ordine maggiore della quantità prodotta nella commessa", Information!)
			id_quan_assegnabile = ld_quan_prodotta
		else
			id_quan_assegnabile = s_cs_xx.parametri.parametro_d_1
		end if
	else
		g_mb.messagebox("Assegnazione","Errore in lettura anagrafica commessa~r~nDettaglio errore:" + sqlca.sqlerrtext+"~r~nPROVO AD ASSEGNARE LA QUANTITA' ORDINE")
		id_quan_assegnabile = s_cs_xx.parametri.parametro_d_1
	end if
end if
tab_1.tabpage_1.em_quan_da_assegnare.text = string(id_quan_assegnabile)
tab_1.tabpage_1.dw_ext_ass_stock.reset()
tab_1.tabpage_1.dw_ext_ass_stock.triggerevent("retrieve")

tab_1.tabpage_2.dw_lista_bolle_pronte.settransobject(sqlca)
tab_1.tabpage_2.dw_lista_bolle_pronte.setrowfocusindicator(FocusRect!)

tab_1.tabpage_3.dw_lista_fatture_pronte.settransobject(sqlca)
tab_1.tabpage_3.dw_lista_fatture_pronte.setrowfocusindicator(FocusRect!)

tab_1.tabpage_4.dw_lista_comm_spedizione.settransobject(sqlca)
tab_1.tabpage_4.dw_lista_comm_spedizione.setrowfocusindicator(hand!)

// creo lock su riga ordine e su prodotto per evitare modifiche della stessa durante l'assegnazione
w_cs_xx_mdi.SetMicroHelp("Lock Records")
update det_ord_ven
set    cod_prodotto = cod_prodotto
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione and
		 prog_riga_ord_ven = :il_prog_riga_ord_ven;

update anag_prodotti
set    des_prodotto = des_prodotto
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :is_cod_prodotto;
		 
// fine lock
w_cs_xx_mdi.SetMicroHelp("Riga Ordine e Prodotto sono bloccati in uso esclusivo !")



end event

on w_ass_stock.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_ass_stock.destroy
call super::destroy
destroy(this.tab_1)
end on

event close;call super::close;if not ib_assegna then
	s_cs_xx.parametri.parametro_d_1 = 0
end if

// in realtà questo commit è inutile, ma lo ho messo perchè al centro gibus
// mi hanno segnalato che ci sono dei blocchi nel sistema che avvengono
// in particolare dopo la generazione di bolle di uscita merce.
commit;

end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_deposito,"cod_deposito",sqlca,&
//                 "anag_depositi","cod_deposito","des_deposito", &
//                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//


f_PO_LoadDDLB(tab_1.tabpage_1.ddlb_1, sqlca,  "anag_depositi","cod_deposito","des_deposito", &
				"anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
				"(All)")


end event

type tab_1 from tab within w_ass_stock
event create ( )
event destroy ( )
integer x = 23
integer y = 20
integer width = 3406
integer height = 1908
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean fixedwidth = true
boolean raggedright = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
end on

event clicked;long ll_riga
string ls_cod_cliente, ls_cod_des_cliente, ls_cod_causale, ls_cod_porto, ls_cod_mezzo, ls_cod_resa, ls_cod_vettore, ls_cod_inoltro, ls_nota_piede, &
       ls_aspetto_beni, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita,ls_provincia, ls_null, ls_num_ord_cliente, &
		 ls_nota_testata, ls_nota_video, ls_cod_tipo_ord_ven, ls_cod_tipo_bol_ven, ls_cod_imballo
double ld_num_colli, ld_peso_lordo, ld_peso_netto
datetime ldt_oggi, ldt_data_ord_cliente

ldt_oggi = datetime(today(), 00:00:00)
setnull(ls_null)

if index > 1 then
	select cod_cliente, cod_des_cliente, cod_porto, cod_mezzo, cod_resa, cod_vettore, cod_inoltro, aspetto_beni, nota_piede,
	       rag_soc_1, rag_soc_2, indirizzo, frazione, cap, localita, provincia, num_colli, peso_lordo, peso_netto, cod_causale, num_ord_cliente, data_ord_cliente, cod_tipo_ord_ven, cod_imballo
	into   :ls_cod_cliente, :ls_cod_des_cliente, :ls_cod_porto, :ls_cod_mezzo, :ls_cod_resa, :ls_cod_vettore, :ls_cod_inoltro, :ls_aspetto_beni, :ls_nota_piede,
	       :ls_rag_soc_1, :ls_rag_soc_2, :ls_indirizzo, :ls_frazione, :ls_cap, :ls_localita,:ls_provincia, :ld_num_colli, :ld_peso_lordo, :ld_peso_netto, :ls_cod_causale, :ls_num_ord_cliente, :ldt_data_ord_cliente, :ls_cod_tipo_ord_ven, :ls_cod_imballo
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca ordine " + string(il_anno_registrazione) + "/" + string(il_num_registrazione)+ ". Impossibile proseguire", stopsign!)
		return
	end if
	
	select anno_commessa, num_commessa
	into   :il_anno_commessa, :il_num_commessa
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione and
			 prog_riga_ord_ven = :il_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca riga dell'ordine " + string(il_anno_registrazione) + "/" + string(il_num_registrazione)+ "/" + string(il_prog_riga_ord_ven)+". Impossibile proseguire", stopsign!)
		return
	end if
end if

choose case index
	case 2, 4		///   bolla
		tabpage_2.dw_lista_bolle_pronte.setredraw(false)
		tabpage_2.dw_ext_dati_bolla.setredraw(false)
		tabpage_2.dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)
		tabpage_2.dw_ext_dati_bolla.reset()
		ll_riga = tabpage_2.dw_ext_dati_bolla.insertrow(0)
		if index=4 then
			tabpage_1.enabled = false
			tabpage_2.enabled = false
			tabpage_3.enabled = false
			if isnull(il_anno_commessa) or il_anno_commessa = 0 then
				g_mb.messagebox("APICE","Nessuna commessa generata per questo dettaglio: impossibile eseguire anticipo", Stopsign!)
				return
			end if
			
			ll_riga = tabpage_4.dw_lista_comm_spedizione.retrieve(s_cs_xx.cod_azienda, il_anno_commessa, il_num_commessa)
			if ll_riga <= 0 then
				g_mb.messagebox("APICE","Non è presente nessun dato di dettaglio delle commesse; verificare l'avanzamento di produzione o l'attivazione delle commesse", Stopsign!)
				return
			end if
			ib_anticipo = true
		end if
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_destinazione", &
							  sqlca, &
							  "anag_des_clienti", &
							  "cod_des_cliente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_imballo", &
							  sqlca, &
							  "tab_imballi", &
							  "cod_imballo", &
							  "des_imballo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_vettore", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_inoltro", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_mezzo", &
							  sqlca, &
							  "tab_mezzi", &
							  "cod_mezzo", &
							  "des_mezzo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_porto", &
							  sqlca, &
							  "tab_porti", &
							  "cod_porto", &
							  "des_porto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_resa", &
							  sqlca, &
							  "tab_rese", &
							  "cod_resa", &
							  "des_resa", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_bolla, &
							  "cod_causale", &
							  sqlca, &
							  "tab_causali_trasp", &
							  "cod_causale", &
							  "des_causale", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
				
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_destinazione", ls_cod_des_cliente)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_causale", ls_cod_causale)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_porto", ls_cod_porto)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_resa", ls_cod_resa)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_imballo", ls_cod_imballo)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "aspetto_beni", ls_aspetto_beni)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_2", ls_rag_soc_2)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "indirizzo", ls_indirizzo)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "frazione", ls_frazione)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "localita", ls_localita)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cap", ls_cap)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "provincia", ls_provincia)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "num_colli", ld_num_colli)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "peso_netto", ld_peso_netto)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "peso_lordo", ld_peso_lordo)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "num_ord_cliente", ls_num_ord_cliente)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "data_ord_cliente", ldt_data_ord_cliente)
		tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "note_piede", ls_nota_piede)
		tabpage_2.dw_lista_bolle_pronte.setredraw(true)
		tabpage_2.dw_ext_dati_bolla.setredraw(true)

		if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "BOL_VEN", ls_null, ldt_oggi, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
		end if

		if isnull(ls_cod_causale) then
			select cod_tipo_bol_ven
			into   :ls_cod_tipo_bol_ven
			from   tab_tipi_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			if not isnull(ls_cod_tipo_bol_ven) then
				select cod_causale
				into   :ls_cod_causale
				from   tab_tipi_bol_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
				if not isnull(ls_cod_causale) then
					tab_1.tabpage_2.dw_ext_dati_bolla.setitem(ll_riga, "cod_causale", ls_cod_causale)
				end if
			end if
		end if
		
	case 3			/// fattura
		tabpage_3.dw_lista_fatture_pronte.setredraw(false)
		tabpage_3.dw_ext_dati_fattura.setredraw(false)
		tabpage_3.dw_lista_fatture_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)
		tabpage_3.dw_ext_dati_fattura.reset()
		ll_riga = tabpage_3.dw_ext_dati_fattura.insertrow(0)
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_destinazione", &
							  sqlca, &
							  "anag_des_clienti", &
							  "cod_des_cliente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_imballo", &
							  sqlca, &
							  "tab_imballi", &
							  "cod_imballo", &
							  "des_imballo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_vettore", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_inoltro", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_mezzo", &
							  sqlca, &
							  "tab_mezzi", &
							  "cod_mezzo", &
							  "des_mezzo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_porto", &
							  sqlca, &
							  "tab_porti", &
							  "cod_porto", &
							  "des_porto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_resa", &
							  sqlca, &
							  "tab_rese", &
							  "cod_resa", &
							  "des_resa", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_3.dw_ext_dati_fattura, &
							  "cod_causale", &
							  sqlca, &
							  "tab_causali_trasp", &
							  "cod_causale", &
							  "des_causale", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
				
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_destinazione", ls_cod_des_cliente)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_causale", ls_cod_causale)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_porto", ls_cod_porto)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_resa", ls_cod_resa)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "aspetto_beni", ls_aspetto_beni)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "rag_soc_2", ls_rag_soc_2)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "indirizzo", ls_indirizzo)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "frazione", ls_frazione)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "localita", ls_localita)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "cap", ls_cap)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "provincia", ls_provincia)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "num_colli", ld_num_colli)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "peso_netto", ld_peso_netto)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "peso_lordo", ld_peso_lordo)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "num_ord_cliente", ls_num_ord_cliente)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "data_ord_cliente", ldt_data_ord_cliente)
		tab_1.tabpage_3.dw_ext_dati_fattura.setitem(ll_riga, "note_piede", ls_nota_piede)
		tabpage_3.dw_lista_fatture_pronte.setredraw(true)
		tabpage_3.dw_ext_dati_fattura.setredraw(true)
end choose
		  
end event

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3369
integer height = 1784
long backcolor = 79741120
string text = "Assegnazione"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
ddlb_1 ddlb_1
st_1 st_1
em_quan_da_assegnare em_quan_da_assegnare
st_3 st_3
em_quan_assegnata em_quan_assegnata
dw_ext_ass_stock dw_ext_ass_stock
cb_2 cb_2
cb_1 cb_1
cb_4 cb_4
end type

on tabpage_1.create
this.ddlb_1=create ddlb_1
this.st_1=create st_1
this.em_quan_da_assegnare=create em_quan_da_assegnare
this.st_3=create st_3
this.em_quan_assegnata=create em_quan_assegnata
this.dw_ext_ass_stock=create dw_ext_ass_stock
this.cb_2=create cb_2
this.cb_1=create cb_1
this.cb_4=create cb_4
this.Control[]={this.ddlb_1,&
this.st_1,&
this.em_quan_da_assegnare,&
this.st_3,&
this.em_quan_assegnata,&
this.dw_ext_ass_stock,&
this.cb_2,&
this.cb_1,&
this.cb_4}
end on

on tabpage_1.destroy
destroy(this.ddlb_1)
destroy(this.st_1)
destroy(this.em_quan_da_assegnare)
destroy(this.st_3)
destroy(this.em_quan_assegnata)
destroy(this.dw_ext_ass_stock)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.cb_4)
end on

type ddlb_1 from dropdownlistbox within tabpage_1
integer x = 5
integer y = 12
integer width = 1303
integer height = 420
integer taborder = 2
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean vscrollbar = true
end type

event selectionchanged;string ls_cod_deposito


ls_cod_deposito = f_PO_SelectDDLB( ddlb_1)

is_cod_ubicazione = "%"
if isnull(ls_cod_deposito) or len(ls_cod_deposito) <= 0 then
	is_cod_deposito = "%"
else
	is_cod_deposito = ls_cod_deposito 
end if

dw_ext_ass_stock.triggerevent("retrieve")

end event

type st_1 from statictext within tabpage_1
integer x = 1353
integer y = 32
integer width = 485
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Q.ta in spedizione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_quan_da_assegnare from editmask within tabpage_1
integer x = 1856
integer y = 12
integer width = 526
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = right!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
string mask = "##,###,###.0000"
string displaydata = ""
end type

type st_3 from statictext within tabpage_1
integer x = 2405
integer y = 32
integer width = 306
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Assegnato:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_quan_assegnata from editmask within tabpage_1
integer x = 2747
integer y = 12
integer width = 526
integer height = 80
integer taborder = 12
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
alignment alignment = right!
textcase textcase = upper!
borderstyle borderstyle = styleraised!
string mask = "##,###,###.0000"
string displaydata = ""
end type

type dw_ext_ass_stock from datawindow within tabpage_1
event retrieve ( )
integer x = 5
integer y = 132
integer width = 3351
integer height = 1440
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_ext_ass_stock"
boolean vscrollbar = true
boolean livescroll = true
end type

event retrieve();
wf_carica_stock( is_cod_prodotto, is_cod_deposito, false)

/*
string ls_sql, ls_deposito, ls_ubicazione, ls_lotto, ls_flag_tipo_det_ven, ls_flag_negativo
datetime ldt_data_stock
long ll_prog_stock, ll_riga
double ld_giacenza, ld_assegnata, ld_spedizione

w_cs_xx_mdi.SetMicroHelp("Ricerca dei lotti in corso")
dw_ext_ass_stock.reset()

declare cu_stock dynamic cursor for sqlsa;

select flag_tipo_det_ven
  into :ls_flag_tipo_det_ven
  from tab_tipi_det_ven
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_tipo_det_ven = :is_cod_tipo_det_ven;
	
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Attenzione", "E' avvenuto un errore in fase di lettura tabella TAB_TIPI_DET_VEN.", exclamation!)
	rollback;
	return
end if	

if ls_flag_tipo_det_ven <> 'C' and ls_flag_tipo_det_ven <> 'N' then
	
	ls_sql = "select cod_deposito, cod_ubicazione, cod_lotto, data_stock, prog_stock, giacenza_stock, quan_assegnata, quan_in_spedizione from stock where cod_azienda = '"+s_cs_xx.cod_azienda+ "' and cod_prodotto = '" + is_cod_prodotto + "' and cod_deposito like '" + is_cod_deposito + "' and cod_ubicazione like '" + is_cod_ubicazione + "' "
	
	select flag
	into   :ls_flag_negativo
	from	 parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'CMN' and
			 flag_parametro = 'F';
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in lettura parametro CMN da parametri_azienda: " + sqlca.sqlerrtext,stopsign!)
		ls_flag_negativo = "N"
	elseif sqlca.sqlcode = 100 or isnull(ls_flag_negativo) then
		ls_flag_negativo = "N"
	end if
	
	if ls_flag_negativo <> "S" then
		ls_sql += " and (giacenza_stock - quan_assegnata - quan_in_spedizione) > 0 "
	end if
	
	ls_Sql += " order by cod_ubicazione, cod_lotto, data_stock, prog_stock "
	
	prepare sqlsa from :ls_sql;
	
	open dynamic cu_stock;
	
	do while 1=1
		fetch cu_stock into :ls_deposito, :ls_ubicazione, :ls_lotto, :ldt_data_stock, :ll_prog_stock, :ld_giacenza, :ld_assegnata, :ld_spedizione;
		if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		ll_riga = dw_ext_ass_stock.insertrow(0)
		w_cs_xx_mdi.SetMicroHelp("Ricerca dei lotti in corso " + string(ll_riga) + "...")	
		dw_ext_ass_stock.setitem(ll_riga, "cod_deposito", ls_deposito)
		dw_ext_ass_stock.setitem(ll_riga, "cod_ubicazione", ls_ubicazione)
		dw_ext_ass_stock.setitem(ll_riga, "cod_lotto", ls_lotto)
		dw_ext_ass_stock.setitem(ll_riga, "data_stock", ldt_data_stock)
		dw_ext_ass_stock.setitem(ll_riga, "prog_stock", ll_prog_stock)
		dw_ext_ass_stock.setitem(ll_riga, "quan_giacenza", ld_giacenza)
		dw_ext_ass_stock.setitem(ll_riga, "quan_assegnata", ld_assegnata)
		dw_ext_ass_stock.setitem(ll_riga, "quan_spedizione", ld_spedizione)
	loop
	close cu_stock;
	commit;
	dw_ext_ass_stock.resetupdate()
	w_cs_xx_mdi.SetMicroHelp("Caricamento dei lotti eseguito")
end if

*/
end event

event editchanged;string ls_flag_negativo

dec{4} ld_quan_da_assegnare, ld_quan_assegnata, ld_quan_da_assegnare_old, ld_tot_assegnazione


select flag
into   :ls_flag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro CMN da parametri_azienda: " + sqlca.sqlerrtext,stopsign!)
	ls_flag_negativo = "N"
elseif sqlca.sqlcode = 100 or isnull(ls_flag_negativo) then
	ls_flag_negativo = "N"
end if

if ls_flag_negativo <> "S" and double(data) > this.getitemnumber(this.getrow(), "cf_quan_disponibile") then
	g_mb.messagebox("Attenzione", "Non è possibile assegnare una quantità maggiore di quella disponibile nello stock", exclamation!)
	return 1
end if

em_quan_da_assegnare.getdata(ld_quan_da_assegnare)
if ld_quan_da_assegnare <= 0 then
	g_mb.messagebox("APICE","Indicare prima la quantità da assegnare")
	em_quan_da_assegnare.setfocus()
	return
end if

em_quan_assegnata.getdata(ld_quan_assegnata)
ld_quan_da_assegnare_old = this.getitemnumber(row, "quan_assegnazione")
ld_tot_assegnazione = ld_quan_assegnata - ld_quan_da_assegnare_old + double(data)

if ld_tot_assegnazione > (ld_quan_da_assegnare + ld_quan_assegnata) then
	g_mb.messagebox("APICE","Non è possibile assegnare una quantità superiore della quantità in spedizione")
	this.setitem(row, "quan_assegnazione",ld_quan_da_assegnare_old)
	this.accepttext()
	return
else
	ld_quan_da_assegnare = ld_quan_da_assegnare + ld_quan_da_assegnare_old - double(data)
end if

em_quan_assegnata.text = string(ld_tot_assegnazione)
em_quan_da_assegnare.text = string(ld_quan_da_assegnare)
this.accepttext()
end event

type cb_2 from commandbutton within tabpage_1
integer x = 5
integer y = 1592
integer width = 366
integer height = 80
integer taborder = 12
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Tutti Stock"
end type

event clicked;is_cod_ubicazione = "%"
is_cod_deposito = "%"
dw_ext_ass_stock.triggerevent("retrieve")

end event

type cb_1 from commandbutton within tabpage_1
integer x = 3003
integer y = 1592
integer width = 366
integer height = 80
integer taborder = 13
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&ss. Stock"
end type

event clicked;string ls_cod_ubicazione, ls_cod_lotto, ls_cod_deposito, ls_cod_tipo_bol_ven, ls_cod_tipo_fat_ven, ls_flag_tipo_det_ven
long  ll_prog_stock, ll_i, ll_risp, ll_num_righe_ass
double ld_quan_da_assegnare, ld_quan_da_assegnare_tot, ld_quan_in_evasione, ld_quan_commessa, ld_quan_evasa
datetime ldt_data_stock
integer li_return
uo_generazione_documenti luo_gen_doc

dw_ext_ass_stock.accepttext()
em_quan_da_assegnare.getdata(ld_quan_da_assegnare_tot)
ld_quan_in_evasione = 0

//genero un progressivo sessione univoco #########################
il_prog_sessione = wf_get_prog_sessione()
//#################################################

select flag_tipo_det_ven
  into :ls_flag_tipo_det_ven
  from tab_tipi_det_ven
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_tipo_det_ven = :is_cod_tipo_det_ven;
	
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Attenzione", "E' avvenuto un errore in fase di lettura tabella TAB_TIPI_DET_VEN.", exclamation!)
	rollback;
	return
end if	

if ld_quan_da_assegnare_tot < 0 then
	g_mb.messagebox("Attenzione", "E' stata assegnata una quantità superiore a quella da assegnare", exclamation!)
	ROLLBACK;
	return
end if
if ld_quan_da_assegnare_tot > 0 and ls_flag_tipo_det_ven <> 'C' and ls_flag_tipo_det_ven <> 'N' then
	g_mb.messagebox("Attenzione", "E' presente ancora quantità da assegnare. ", exclamation!)
	ROLLBACK;
	return
end if

if ls_flag_tipo_det_ven <> 'C' and ls_flag_tipo_det_ven <> 'N' then

	ll_num_righe_ass = dw_ext_ass_stock.rowcount()
	
	for ll_i = 1 to ll_num_righe_ass
		ld_quan_da_assegnare = dw_ext_ass_stock.getitemnumber(ll_i, "quan_assegnazione")
	
		if ld_quan_da_assegnare > 0 then
			ls_cod_deposito = dw_ext_ass_stock.getitemstring(ll_i, "cod_deposito")
			ls_cod_ubicazione = dw_ext_ass_stock.getitemstring(ll_i, "cod_ubicazione")
			ls_cod_lotto = dw_ext_ass_stock.getitemstring(ll_i, "cod_lotto")
			ldt_data_stock = datetime(dw_ext_ass_stock.getitemdate(ll_i, "data_stock"), 00:00:00)
			ll_prog_stock = dw_ext_ass_stock.getitemnumber(ll_i, "prog_stock")
		
			ld_quan_in_evasione = ld_quan_in_evasione + ld_quan_da_assegnare
	
			if s_cs_xx.parametri.parametro_s_15 <> "CO" then
				update stock  
					set quan_assegnata = quan_assegnata + :ld_quan_da_assegnare
				 where cod_azienda    = :s_cs_xx.cod_azienda and
						 cod_prodotto   = :is_cod_prodotto and 
						 cod_deposito   = :ls_cod_deposito and
						 cod_ubicazione = :ls_cod_ubicazione and
						 cod_lotto  = :ls_cod_lotto and
						 data_stock = :ldt_data_stock and
						 prog_stock = :ll_prog_stock;
			end if
	
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento stock.", exclamation!, ok!)
				rollback;
				return
			end if
	
			// Aggiornamento evas_ord_ven
			if s_cs_xx.parametri.parametro_s_15 = "OC" then
				if wf_evas_ord_ven(ls_cod_ubicazione, ls_cod_lotto, ldt_data_stock, ll_prog_stock, ld_quan_da_assegnare, ls_cod_deposito, il_prog_sessione) = -1 then
					rollback;
					return
				end if
			end if
			
			// Aggiornamento mat_prime_commessa_stock
			if s_cs_xx.parametri.parametro_s_15 = "CO" then
				if wf_mat_prime_commessa_stock (ls_cod_ubicazione, ls_cod_lotto, ldt_data_stock, ll_prog_stock, ld_quan_da_assegnare, ls_cod_deposito ) = -1 then
					rollback;
					return
				end if
			end if
	
		end if
	next
	
	if ld_quan_da_assegnare_tot = 0 then
		// Aggiornamento det_ord_ven
		if s_cs_xx.parametri.parametro_s_15 = "OC" then
			if wf_det_ord_ven(ld_quan_in_evasione) = -1 then
				rollback;
				return
			end if
		end if
		
		ib_assegna = true
		s_cs_xx.parametri.parametro_d_1 = ld_quan_in_evasione
	
	end if
end if
//----------------------------------------------- Modifica Nicola ----------------------------------------------
if ls_flag_tipo_det_ven = 'C' or ls_flag_tipo_det_ven = 'N' then
	ld_quan_evasa = 0
	ld_quan_commessa = 0
	
	id_quan_assegnabile = ld_quan_da_assegnare_tot
	
	luo_gen_doc = create uo_generazione_documenti
	li_return = luo_gen_doc.uof_ass_ord_ven(	il_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven, &
															is_cod_tipo_det_ven, is_cod_deposito, is_cod_ubicazione, is_cod_prodotto, &
															id_quan_assegnabile, ld_quan_commessa, ld_quan_evasa, &
															il_prog_sessione)
	destroy luo_gen_doc
	
	if li_return = 3 then
		g_mb.messagebox("APICE","Non è stato trovato nessuno stock da assegnare")
		commit;
		return
	end if
	
	if li_return = 2 then
		select quan_in_evasione
		into :ld_quan_in_evasione
		from det_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and
				num_registrazione  = :il_num_registrazione and
				prog_riga_ord_ven  = :il_prog_riga_ord_ven;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "E' avvenuto un errore in fase di lettura dettagli.", exclamation!)
					rollback;
					return
				end if
	end if
end if	
//----------------------------------------------- Fine Modifica ----------------------------------------------

commit;
g_mb.messagebox("APICE","Assegnazione effettuata con successo!")

cb_1.enabled = false
cb_4.enabled = false

select cod_tipo_bol_ven,
       cod_tipo_fat_ven
into   :ls_cod_tipo_bol_ven,
       :ls_cod_tipo_fat_ven
from   tab_tipi_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_ord_ven = ( select cod_tipo_ord_ven 
		                      from   tes_ord_ven 
									 where  cod_azienda = :s_cs_xx.cod_azienda and
									        anno_registrazione = :il_anno_registrazione and
											  num_registrazione = :il_num_registrazione);
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo bolla o fattura in tipo ordine di vendita. Dettaglio errore: " + sqlca.sqlerrtext)
	tab_1.tabpage_2.enabled = false
	tab_1.tabpage_3.enabled = false
else
	if not isnull(ls_cod_tipo_bol_ven) then tab_1.tabpage_2.enabled = true
	if not isnull(ls_cod_tipo_fat_ven) then tab_1.tabpage_3.enabled = true
end if

end event

type cb_4 from commandbutton within tabpage_1
integer x = 2610
integer y = 1592
integer width = 366
integer height = 80
integer taborder = 13
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&utomatico"
end type

event clicked;// procedura di assegnazione automatica 
string ls_cod_tipo_bol_ven, ls_cod_tipo_fat_ven
integer li_return
long ld_quan_commessa, ld_quan_evasa, ld_quan_in_evasione
uo_generazione_documenti luo_gen_doc

ld_quan_evasa = 0
ld_quan_commessa = 0

//genero un progressivo sessione univoco #########################
il_prog_sessione = wf_get_prog_sessione()
//#################################################

luo_gen_doc = create uo_generazione_documenti
li_return = luo_gen_doc.uof_ass_ord_ven(	il_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven, &
														is_cod_tipo_det_ven, is_cod_deposito, is_cod_ubicazione, is_cod_prodotto, &
														id_quan_assegnabile, ld_quan_commessa, ld_quan_evasa, &
														il_prog_sessione)
destroy luo_gen_doc

//li_return = f_ass_ord_ven(il_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven, is_cod_tipo_det_ven, is_cod_deposito, is_cod_ubicazione, is_cod_prodotto, id_quan_assegnabile, ld_quan_commessa, ld_quan_evasa)

if li_return = 3 then
	g_mb.messagebox("APICE","Non è stato trovato nessuno stock da assegnare")
	commit;
	return
end if

if li_return = 2 then
	select quan_in_evasione
	into :ld_quan_in_evasione
	from det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and 
			anno_registrazione = :il_anno_registrazione and
			num_registrazione  = :il_num_registrazione and
			prog_riga_ord_ven  = :il_prog_riga_ord_ven;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "E' avvenuto un errore in fase di lettura dettagli.", exclamation!)
				rollback;
				return
			end if
end if

commit;
g_mb.messagebox("APICE","Assegnazione eseguita con successo!")

cb_1.enabled = false
cb_4.enabled = false

select cod_tipo_bol_ven,
       cod_tipo_fat_ven
into   :ls_cod_tipo_bol_ven,
       :ls_cod_tipo_fat_ven
from   tab_tipi_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_ord_ven = ( select cod_tipo_ord_ven 
		                      from   tes_ord_ven 
									 where  cod_azienda = :s_cs_xx.cod_azienda and
									        anno_registrazione = :il_anno_registrazione and
											  num_registrazione = :il_num_registrazione);
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo bolla o fattura in tipo ordine di vendita. Dettaglio errore: " + sqlca.sqlerrtext)
	tab_1.tabpage_2.enabled = false
	tab_1.tabpage_3.enabled = false
else
	if not isnull(ls_cod_tipo_bol_ven) then tab_1.tabpage_2.enabled = true
	if not isnull(ls_cod_tipo_fat_ven) then tab_1.tabpage_3.enabled = true
end if

end event

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3369
integer height = 1784
long backcolor = 79741120
string text = "Bolle"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
st_2 st_2
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
cb_5 cb_5
end type

on tabpage_2.create
this.dw_lista_bolle_pronte=create dw_lista_bolle_pronte
this.cb_nuova_bolla=create cb_nuova_bolla
this.st_2=create st_2
this.cb_aggiungi_a_bolla=create cb_aggiungi_a_bolla
this.dw_ext_dati_bolla=create dw_ext_dati_bolla
this.cb_5=create cb_5
this.Control[]={this.dw_lista_bolle_pronte,&
this.cb_nuova_bolla,&
this.st_2,&
this.cb_aggiungi_a_bolla,&
this.dw_ext_dati_bolla,&
this.cb_5}
end on

on tabpage_2.destroy
destroy(this.dw_lista_bolle_pronte)
destroy(this.cb_nuova_bolla)
destroy(this.st_2)
destroy(this.cb_aggiungi_a_bolla)
destroy(this.dw_ext_dati_bolla)
destroy(this.cb_5)
end on

type dw_lista_bolle_pronte from datawindow within tabpage_2
integer x = 5
integer y = 132
integer width = 3360
integer height = 480
integer taborder = 1
boolean bringtotop = true
string dataobject = "d_lista_bolle_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_nuova_bolla from commandbutton within tabpage_2
integer x = 5
integer y = 1692
integer width = 366
integer height = 80
integer taborder = 13
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo DDT"
end type

event clicked;if not ib_anticipo then
	if g_mb.messagebox("APICE","Sei sicuro di voler creare una nuova bolla per tutte le righe assegnate di questo ordine?", Question!, YEsNo!, 2) = 1 then
		string ls_errore
		long   ll_anno_bolla, ll_num_bolla
		uo_generazione_documenti luo_generazione_documenti
		
		dw_ext_dati_bolla.accepttext()
		luo_generazione_documenti = CREATE uo_generazione_documenti
		luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
		luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
		luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
		luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
		luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
		luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
		luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
		luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
		luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
		luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
		luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
		luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
		luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
		luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
		luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
		luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
		luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
		luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
		luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
		luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
		luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
		luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
		luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
		luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_cliente")
		luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_cliente")
		ll_anno_bolla = 0
		ll_num_bolla = 0
		if luo_generazione_documenti.uof_crea_aggiungi_bolla(il_anno_registrazione, il_num_registrazione, ll_anno_bolla, ll_num_bolla, ls_errore) <> 0 then
			g_mb.messagebox("APICE","Errore durante la generazione della bolla. " + ls_errore)
			destroy luo_generazione_documenti
			ROLLBACK;
			return
		else
			g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
			destroy luo_generazione_documenti
			COMMIT;
			return
		end if
	end if
else
	wf_anticipo_spedizioni(false)
end if
COMMIT;
end event

type st_2 from statictext within tabpage_2
integer x = 5
integer y = 12
integer width = 3273
integer height = 76
boolean bringtotop = true
integer textsize = -11
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
boolean enabled = false
string text = "Elenco delle bolle di uscita del cliente che non sono state ne confermate ne stampate in definitivo"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_aggiungi_a_bolla from commandbutton within tabpage_2
integer x = 2999
integer y = 1692
integer width = 366
integer height = 80
integer taborder = 14
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;if not ib_anticipo then
	if g_mb.messagebox("APICE","Sei sicuro di voler evadere tutte le righe asssegnate per questo ordine aggiungendole ad una bolla già esistente?", Question!, YEsNo!, 2) = 1 then
		string ls_errore
		long   ll_anno_bolla, ll_num_bolla
		uo_generazione_documenti luo_generazione_documenti
		
		dw_ext_dati_bolla.accepttext()
		luo_generazione_documenti = CREATE uo_generazione_documenti
		luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
		luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
		luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
		luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
		luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
		luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
		luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
		luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
		luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
		luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
		luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
		luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
		luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
		luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
		luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
		luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
		luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
		luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
		luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
		luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
		luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
		luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
		luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
		luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_cliente")
		luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_cliente")
		if dw_lista_bolle_pronte.getrow() > 0 then
			ll_anno_bolla = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_anno_registrazione")
			ll_num_bolla  = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_num_registrazione")
		else
			g_mb.messagebox("APICE","Selezionare una bolla fra la lista di quelle pronte")
		end if
		if luo_generazione_documenti.uof_crea_aggiungi_bolla(il_anno_registrazione, il_num_registrazione, ll_anno_bolla, ll_num_bolla, ls_errore) <> 0 then
			g_mb.messagebox("APICE","Errore durante la generazione della bolla. " + ls_errore)
			destroy luo_generazione_documenti
			rollback;
			return
		else
			g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
			destroy luo_generazione_documenti
			commit;
			return
		end if
	end if
else
	wf_anticipo_spedizioni(true)
end if
COMMIT;
end event

type dw_ext_dati_bolla from datawindow within tabpage_2
integer x = 5
integer y = 612
integer width = 3360
integer height = 1080
integer taborder = 12
boolean bringtotop = true
string dataobject = "d_ext_dati_bolla_assegnazione"
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;string ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_cod_cliente, &
       ls_cod_des_cliente, ls_null
datetime ldt_oggi

ldt_oggi = datetime(today(), 00:00:00)
setnull(ls_null)
ls_cod_cliente = getitemstring(getrow(),"cod_cliente")
if dwo.name = "cod_destinazione" then
	if not isnull(data) and not isnull(ls_cod_cliente) then
		ls_cod_des_cliente = data	
		
		select anag_des_clienti.rag_soc_1,
				 anag_des_clienti.rag_soc_2,
				 anag_des_clienti.indirizzo,
				 anag_des_clienti.frazione,
				 anag_des_clienti.cap,
				 anag_des_clienti.localita,
				 anag_des_clienti.provincia
		into   :ls_rag_soc_1,    
				 :ls_rag_soc_2,
				 :ls_indirizzo,    
				 :ls_frazione,
				 :ls_cap,
				 :ls_localita,
				 :ls_provincia
		from   anag_des_clienti
		where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_des_clienti.cod_cliente = :ls_cod_cliente and
				 anag_des_clienti.cod_des_cliente = :ls_cod_des_cliente and
				 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
		if sqlca.sqlcode = 0 then
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "rag_soc_1", ls_rag_soc_1)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "rag_soc_2", ls_rag_soc_2)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "indirizzo", ls_indirizzo)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "frazione", ls_frazione)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "cap", ls_cap)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "localita", ls_localita)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "provincia", ls_provincia)
		else
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "rag_soc_1", ls_null)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "rag_soc_2", ls_null)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "indirizzo", ls_null)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "frazione", ls_null)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "cap", ls_null)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "localita", ls_null)
			tab_1.tabpage_2.dw_ext_dati_bolla.setitem(1, "provincia", ls_null)
		end if
	end if
end if
end event

type cb_5 from commandbutton within tabpage_2
integer x = 2610
integer y = 1692
integer width = 366
integer height = 80
integer taborder = 13
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa Bol."
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_d_1 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_num_registrazione")

window_open(w_tipo_stampa_bol_ven, 0)
parent.triggerevent("pc_retrieve")

if s_cs_xx.parametri.parametro_i_1 <> -1 then
	window_open(w_report_bol_ven, -1)
end if
close(w_ass_stock)
return

end event

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3369
integer height = 1784
long backcolor = 79741120
string text = "Fatture"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_lista_fatture_pronte dw_lista_fatture_pronte
cb_nuova_fattura cb_nuova_fattura
cb_aggiungi_a_fattura cb_aggiungi_a_fattura
dw_ext_dati_fattura dw_ext_dati_fattura
st_4 st_4
cb_3 cb_3
end type

on tabpage_3.create
this.dw_lista_fatture_pronte=create dw_lista_fatture_pronte
this.cb_nuova_fattura=create cb_nuova_fattura
this.cb_aggiungi_a_fattura=create cb_aggiungi_a_fattura
this.dw_ext_dati_fattura=create dw_ext_dati_fattura
this.st_4=create st_4
this.cb_3=create cb_3
this.Control[]={this.dw_lista_fatture_pronte,&
this.cb_nuova_fattura,&
this.cb_aggiungi_a_fattura,&
this.dw_ext_dati_fattura,&
this.st_4,&
this.cb_3}
end on

on tabpage_3.destroy
destroy(this.dw_lista_fatture_pronte)
destroy(this.cb_nuova_fattura)
destroy(this.cb_aggiungi_a_fattura)
destroy(this.dw_ext_dati_fattura)
destroy(this.st_4)
destroy(this.cb_3)
end on

type dw_lista_fatture_pronte from datawindow within tabpage_3
integer x = 5
integer y = 132
integer width = 3360
integer height = 480
integer taborder = 12
boolean bringtotop = true
string dataobject = "d_lista_fatture_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_nuova_fattura from commandbutton within tabpage_3
integer x = 5
integer y = 1692
integer width = 366
integer height = 80
integer taborder = 13
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuova Fat."
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di voler creare una nuova fattura per tutte le righe assegnate di questo ordine?", Question!, YEsNo!, 2) = 1 then
	string ls_errore
	long   ll_anno_fattura, ll_num_fattura
	uo_generazione_documenti luo_generazione_documenti
	
	dw_ext_dati_fattura.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_fattura.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_fattura.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_fattura.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_fattura.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_fattura.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_fattura.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_fattura.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_fattura.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_fattura.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_fattura.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_fattura.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_fattura.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_fattura.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_fattura.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_fattura.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_fattura.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_fattura.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_fattura.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_fattura.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_fattura.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_fattura.getitemdatetime(1,"data_ord_cliente")
	ll_anno_fattura = 0
	ll_num_fattura = 0
	if luo_generazione_documenti.uof_crea_aggiungi_fattura(il_anno_registrazione, il_num_registrazione, ll_anno_fattura, ll_num_fattura, ls_errore) <> 0 then
		g_mb.messagebox("APICE","Errore durante la generazione della fattura. " + ls_errore)
		destroy luo_generazione_documenti
		rollback;
		return
	else
		g_mb.messagebox("APICE","E' stata generata la fattura " + string(ll_anno_fattura) + "-" + string(ll_num_fattura))
		destroy luo_generazione_documenti
		commit;
		return
	end if
end if
commit;
end event

type cb_aggiungi_a_fattura from commandbutton within tabpage_3
integer x = 2999
integer y = 1692
integer width = 366
integer height = 80
integer taborder = 14
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di voler evadere tutte le righe asssegnate per questo ordine aggiungendole ad una fattura già esistente?", Question!, YEsNo!, 2) = 1 then
	string ls_errore
	long   ll_anno_fattura, ll_num_fattura
	uo_generazione_documenti luo_generazione_documenti
	
	dw_ext_dati_fattura.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_fattura.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_fattura.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_fattura.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_fattura.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_fattura.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_fattura.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_fattura.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_fattura.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_fattura.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_fattura.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_fattura.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_fattura.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_fattura.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_fattura.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_fattura.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_fattura.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_fattura.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_fattura.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_fattura.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_fattura.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_fattura.getitemdatetime(1,"data_ord_cliente")
	if dw_lista_fatture_pronte.getrow() > 0 then
		ll_anno_fattura = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
		ll_num_fattura  = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")
	else
		g_mb.messagebox("APICE","Selezionare una fattura fra la lista di quelle pronte")
	end if
	if luo_generazione_documenti.uof_crea_aggiungi_fattura(il_anno_registrazione, il_num_registrazione, ll_anno_fattura, ll_num_fattura, ls_errore) <> 0 then
		g_mb.messagebox("APICE","Errore durante la generazione della fattura. " + ls_errore)
		destroy luo_generazione_documenti
		rollback;
		return
	else
		g_mb.messagebox("APICE","E' stata generata la fattura " + string(ll_anno_fattura) + "-" + string(ll_num_fattura))
		destroy luo_generazione_documenti
		commit;
		return
	end if
end if
end event

type dw_ext_dati_fattura from datawindow within tabpage_3
integer x = 5
integer y = 612
integer width = 3360
integer height = 1080
integer taborder = 13
boolean bringtotop = true
string dataobject = "d_ext_dati_bolla_assegnazione"
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_4 from statictext within tabpage_3
integer x = 5
integer y = 12
integer width = 3273
integer height = 76
boolean bringtotop = true
integer textsize = -11
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
boolean enabled = false
string text = "Elenco delle fatture di uscita del cliente che non sono state ne confermate ne stampate in definitivo"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_3 from commandbutton within tabpage_3
integer x = 2610
integer y = 1692
integer width = 366
integer height = 80
integer taborder = 13
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa Fat."
end type

event clicked;string ls_messaggio

uo_calcola_documento_euro luo_calcola_documento_euro


s_cs_xx.parametri.parametro_d_1 = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2  = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")

w_cs_xx_mdi.setmicrohelp("Calcolo fattura in corso.....")

luo_calcola_documento_euro = create uo_calcola_documento_euro

if luo_calcola_documento_euro.uof_calcola_documento(s_cs_xx.parametri.parametro_d_1,s_cs_xx.parametri.parametro_d_2,"fat_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	rollback;
	destroy luo_calcola_documento_euro
	return -1
else
	commit;
end if

destroy luo_calcola_documento_euro

s_cs_xx.parametri.parametro_i_1 = 0

w_cs_xx_mdi.setmicrohelp("Stampa fattura in corso.....")

window_open(w_tipo_stampa_fat_ven, 0)

parent.triggerevent("pc_retrieve")

if s_cs_xx.parametri.parametro_i_1 <> -1 then
	window_open(w_report_fat_ven_euro, -1)
end if

w_cs_xx_mdi.setmicrohelp("Processo di stampa terminato")

close(w_ass_stock)

return
end event

type tabpage_4 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3369
integer height = 1784
long backcolor = 79741120
string text = "Anticipo"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_lista_comm_spedizione dw_lista_comm_spedizione
em_quan_spedizione em_quan_spedizione
st_5 st_5
cb_anticipo cb_anticipo
end type

on tabpage_4.create
this.dw_lista_comm_spedizione=create dw_lista_comm_spedizione
this.em_quan_spedizione=create em_quan_spedizione
this.st_5=create st_5
this.cb_anticipo=create cb_anticipo
this.Control[]={this.dw_lista_comm_spedizione,&
this.em_quan_spedizione,&
this.st_5,&
this.cb_anticipo}
end on

on tabpage_4.destroy
destroy(this.dw_lista_comm_spedizione)
destroy(this.em_quan_spedizione)
destroy(this.st_5)
destroy(this.cb_anticipo)
end on

type dw_lista_comm_spedizione from datawindow within tabpage_4
integer x = 1353
integer y = 12
integer width = 2011
integer height = 700
integer taborder = 12
boolean bringtotop = true
string dataobject = "d_lista_comm_spedizione"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type em_quan_spedizione from editmask within tabpage_4
integer x = 645
integer y = 12
integer width = 709
integer height = 100
integer taborder = 11
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
string mask = "##,###,###,##0.0000"
boolean spin = true
string displaydata = "ˆ"
string minmax = "0~~99999999999"
end type

type st_5 from statictext within tabpage_4
integer x = 27
integer y = 32
integer width = 603
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Quantità in Spedizione:"
boolean focusrectangle = false
end type

type cb_anticipo from commandbutton within tabpage_4
integer x = 2999
integer y = 732
integer width = 366
integer height = 80
integer taborder = 12
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Anticipo"
end type

event clicked;tab_1.tabpage_2.enabled =true
ib_anticipo = true
end event

