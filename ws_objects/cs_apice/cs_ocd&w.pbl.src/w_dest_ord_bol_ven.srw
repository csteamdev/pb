﻿$PBExportHeader$w_dest_ord_bol_ven.srw
$PBExportComments$destinazione bolla esistente da ordine
forward
global type w_dest_ord_bol_ven from w_cs_xx_principale
end type
type dw_dest_ord_bol_ven from uo_cs_xx_dw within w_dest_ord_bol_ven
end type
type st_cliente from statictext within w_dest_ord_bol_ven
end type
type cb_1 from commandbutton within w_dest_ord_bol_ven
end type
end forward

global type w_dest_ord_bol_ven from w_cs_xx_principale
int Width=2885
int Height=973
boolean TitleBar=true
string Title="Aggiunta di righe ordini su bolle"
dw_dest_ord_bol_ven dw_dest_ord_bol_ven
st_cliente st_cliente
cb_1 cb_1
end type
global w_dest_ord_bol_ven w_dest_ord_bol_ven

type variables
string is_cod_cliente, is_cod_tipo_rag 
end variables

forward prototypes
public function integer wf_controlli_testata ()
end prototypes

public function integer wf_controlli_testata ();long ll_selected_bol[], ll_anno_reg_bol, ll_num_reg_bol, ll_riga, ll_selected[], &
	  ll_i, ll_anno_ordine, ll_num_ordine, ll_risp
datetime ldt_data_reg_bol
string ls_cod_tipo_bol_ven_bol, ls_cod_des_cliente_bol, ls_cod_valuta_bol, ls_cod_pagamento_bol, &
		 ls_cod_agente_1_bol, ls_cod_agente_2_bol, ls_cod_banca_bol, ls_cod_banca_clien_for_bol, &
		 ls_cod_imballo_bol, ls_cod_vettore_bol, ls_cod_inoltro_bol, ls_cod_mezzo_bol, &
		 ls_cod_porto_bol, ls_cod_resa_bol, ls_cod_tipo_ord_ven, ls_flag_riep_bol, &
		 ls_cod_tipo_bol_ven, ls_cod_valuta, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, &
		 ls_cod_banca, ls_cod_banca_clien_for, &
		 ls_cod_des_cliente, ls_num_ord_cliente, ls_cod_imballo, &
		 ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, &
		 ls_flag_tipo_doc, ls_flag_destinazione, ls_flag_num_doc, ls_flag_imballo, &
		 ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, ls_flag_resa, &
		 ls_flag_data_consegna  

uo_cs_xx_dw ds_gen_bol_ven_fat_imm


dw_dest_ord_bol_ven.get_selected_rows(ll_selected_bol[])
if upperbound(ll_selected_bol) = 0 then
	g_mb.messagebox("Apice","Selezionare la bolla di destinazione")
	return -1
end if

ll_riga = ll_selected_bol[1]

ll_anno_reg_bol = dw_dest_ord_bol_ven.getitemnumber(ll_riga, "anno_registrazione")
ll_num_reg_bol = dw_dest_ord_bol_ven.getitemnumber(ll_riga, "num_registrazione")
ldt_data_reg_bol = dw_dest_ord_bol_ven.getitemdatetime(ll_riga, "data_registrazione")
ls_cod_tipo_bol_ven_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_tipo_bol_ven")
ls_cod_des_cliente_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_des_cliente")
ls_cod_valuta_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_valuta")
ls_cod_pagamento_bol= dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_pagamento")
ls_cod_agente_1_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_agente_1")
ls_cod_agente_2_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_agente_2")
ls_cod_banca_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_banca")
ls_cod_banca_clien_for_bol= dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_banca_clien_for")
ls_cod_imballo_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_imballo")
ls_cod_vettore_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_vettore")
ls_cod_inoltro_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_inoltro")
ls_cod_mezzo_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_mezzo")
ls_cod_porto_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_porto")
ls_cod_resa_bol = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_resa")


select tab_tipi_rag.flag_tipo_doc,   
		 tab_tipi_rag.flag_destinazione,   
		 tab_tipi_rag.flag_num_doc,   
		 tab_tipi_rag.flag_imballo,   
		 tab_tipi_rag.flag_vettore,   
		 tab_tipi_rag.flag_inoltro,   
		 tab_tipi_rag.flag_mezzo,   
		 tab_tipi_rag.flag_porto,   
		 tab_tipi_rag.flag_resa,   
		 tab_tipi_rag.flag_data_consegna  
into   :ls_flag_tipo_doc,   		
		 :ls_flag_destinazione,    
		 :ls_flag_num_doc,   		
		 :ls_flag_imballo,   
		 :ls_flag_vettore,   
		 :ls_flag_inoltro,   
		 :ls_flag_mezzo,   
		 :ls_flag_porto,   
		 :ls_flag_resa,   
		 :ls_flag_data_consegna  
from   tab_tipi_rag  
where  tab_tipi_rag.cod_azienda = :s_cs_xx.cod_azienda and
		 tab_tipi_rag.cod_tipo_rag = :is_cod_tipo_rag;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore durante la lettura Tabella Tipi Ragruppamenti.")
	return -1
end if


//ls_flag_num_doc, ls_flag_data_consegna     		// da fare

ds_gen_bol_ven_fat_imm = i_openparm
ds_gen_bol_ven_fat_imm.get_selected_rows(ll_selected[])

for ll_i = 1 to upperbound(ll_selected)
	ls_cod_tipo_ord_ven = ds_gen_bol_ven_fat_imm.getitemstring(ll_selected[ll_i], "tes_ord_ven_cod_tipo_ord_ven")
	ll_anno_ordine = ds_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_anno_registrazione")
	ll_num_ordine = ds_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_num_registrazione")

	select tes_ord_ven.flag_riep_bol
	into   :ls_flag_riep_bol
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and 
			 tes_ord_ven.num_registrazione = :ll_num_ordine;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice","Errore durante la lettura della Testata Ordini Vendita")
		return -1 -1
	end if

	if ls_flag_riep_bol = 'N' then
		ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
			"L'ordine è indicato come non riepilogabile: continuare?", Question!, YesNo! )
		if ll_risp = 2 then return -1
	end if
	
	select tab_tipi_ord_ven.cod_tipo_bol_ven
	into   :ls_cod_tipo_bol_ven
	from   tab_tipi_ord_ven  
	where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore durante la lettura della tabella Tipi Ordini Vendita.")
		return -1
	end if
	
	if ((not isnull(ls_cod_tipo_bol_ven))  and (ls_cod_tipo_bol_ven <> ls_cod_tipo_bol_ven_bol)) then
		ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
			"L'ordine indicato prevede un altro tipo di bolla: continuare?", Question!, YesNo! )
		if ll_risp = 2 then return -1
	end if
		
	select cod_valuta,   cod_pagamento,   	cod_agente_1,  		cod_agente_2,
		 cod_banca,   	cod_banca_clien_for, 
		 cod_des_cliente,   cod_imballo,   
		 cod_vettore,    cod_inoltro,   cod_mezzo, cod_porto, cod_resa
	 into :ls_cod_valuta, :ls_cod_pagamento, :ls_cod_agente_1, :ls_cod_agente_2,
			:ls_cod_banca, :ls_cod_banca_clien_for,
			:ls_cod_des_cliente, :ls_cod_imballo,   
			:ls_cod_vettore, :ls_cod_inoltro, :ls_cod_mezzo, :ls_cod_porto, :ls_cod_resa
	from   tes_ord_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :ll_anno_ordine and  
			 num_registrazione = :ll_num_ordine;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice",  "Errore durante la lettura Testata Ordine Vendita.")
		return -1
	end if
	if ls_cod_valuta <> ls_cod_valuta_bol then
		g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
			"L'ordine indicato prevede un'altra valuta! ", StopSign!, OK! )
		return -1
	end if
	if ls_cod_pagamento <> ls_cod_pagamento_bol then
		g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
			"L'ordine indicato prevede un'altro pagamento! ", StopSign!, OK! )
		return -1
	end if
	
	if (not isnull(ls_cod_banca_bol)) and  (ls_cod_banca <> ls_cod_banca_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altra banca: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if
	
	if (not isnull(ls_cod_banca_clien_for_bol)) and  (ls_cod_banca_clien_for <> ls_cod_banca_clien_for_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altra banca cliente: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if

	if (ls_flag_destinazione="S") and  (ls_cod_des_cliente <> ls_cod_des_cliente_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altra destinazione cliente: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if

	if (not isnull(ls_cod_agente_2_bol)) and  (ls_cod_agente_2 <> ls_cod_agente_2_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un altro agente_2: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if
	
	if (not isnull(ls_cod_agente_1_bol)) and  (ls_cod_agente_1 <> ls_cod_agente_1_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un altro agente_1: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if
	
	if (ls_flag_imballo = "S") and  (ls_cod_imballo <> ls_cod_imballo_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altro imballo: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if

	if (ls_flag_vettore = "S") and  (ls_cod_vettore <> ls_cod_vettore_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altro vettore: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if

	if (ls_flag_inoltro = "S" ) and  (ls_cod_inoltro <> ls_cod_inoltro_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altro inoltro: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if

	if (ls_flag_mezzo = "S") and  (ls_cod_mezzo <> ls_cod_mezzo_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altro cod. mezzo: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if

	if (ls_flag_porto = "S") and  (ls_cod_porto <> ls_cod_porto_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altro cod. porto: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if

	if (ls_flag_resa = "S") and  (ls_cod_resa <> ls_cod_resa_bol) then
	ll_risp = g_mb.messagebox("Ordine " + string(ll_anno_ordine) + " - " + string(ll_num_ordine), &
		"L'ordine indicato prevede un'altro cod. resa: continuare?", Question!, YesNo! )
	if ll_risp = 2 then return -1
	end if
next 




return 0
end function

on w_dest_ord_bol_ven.create
int iCurrent
call w_cs_xx_principale::create
this.dw_dest_ord_bol_ven=create dw_dest_ord_bol_ven
this.st_cliente=create st_cliente
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_dest_ord_bol_ven
this.Control[iCurrent+2]=st_cliente
this.Control[iCurrent+3]=cb_1
end on

on w_dest_ord_bol_ven.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_dest_ord_bol_ven)
destroy(this.st_cliente)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_dest_ord_bol_ven.set_dw_options(sqlca, &
                                  	  pcca.null_object, &
												  c_NoMultiSelect + &
												  c_nonew + &
												  c_nomodify + &
												  c_nodelete + &
												  c_disablecc + &
												  c_disableccinsert, &
												  c_default)


is_cod_cliente = s_cs_xx.parametri.parametro_s_1
is_cod_tipo_rag = s_cs_xx.parametri.parametro_s_2

st_cliente.text = "Cliente: " + is_cod_cliente + f_des_tabella ( "anag_clienti", "cod_cliente = '" + is_cod_cliente + "'", "rag_soc_1" )


end event

type dw_dest_ord_bol_ven from uo_cs_xx_dw within w_dest_ord_bol_ven
int X=23
int Y=121
int Width=2789
int Height=621
string DataObject="d_dest_ord_bol_ven"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_cliente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type st_cliente from statictext within w_dest_ord_bol_ven
int X=23
int Y=21
int Width=2766
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Cliente:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_dest_ord_bol_ven
int X=2446
int Y=761
int Width=366
int Height=81
int TabOrder=2
boolean BringToTop=true
string Text="Aggiungi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_risp, ll_i, ll_selected[], ll_anno_ordine, ll_num_ordine, ll_prog_riga_bol_ven, &
		ll_selected_bol[], ll_anno_reg_bol, ll_num_reg_bol, ll_riga, ll_prog_riga_ord_ven, &
		ll_prog_riga, ll_progr_stock[], ll_anno_commessa, ll_num_commessa, ll_num_confezioni, &
		ll_num_pezzi_confezione, ll_anno_reg_des_mov, ll_num_reg_des_mov
string ls_cod_deposito[],  ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_tipo_det_ven, &
		 ls_des_prodotto, ls_cod_misura, ls_cod_prodotto, ls_cod_iva, ls_nota_dettaglio, &
		 ls_cod_centro_costo, ls_cod_versione, ls_flag_tipo_det_ven, ls_cod_tipo_movimento, &
		 ls_flag_sola_iva, ls_cod_cliente[], ls_cod_fornitore[], ls_cod_tipo_bol_ven, &
		 ls_cod_tipo_analisi_bol,  	ls_flag_abilita_rif_ord_ven,  ls_cod_tipo_det_ven_rif_ord_ven, &
		 ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven,		 ls_flag_rif_num_ord_ven, &
		 ls_flag_rif_data_ord_ven, ls_note_dettaglio_rif, ls_des_riferimento, ls_num_ord_cliente, &
		 ls_cod_iva_rif, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
		 ls_cod_tipo_ord_ven, ls_cod_tipo_analisi_ord, ls_nulla, ls_flag_evasione, ls_cod_valuta, &
		 ls_lire, ls_ordini_evasi
datetime ldt_data_stock[], ldt_data_ordine, ldt_data_ord_cliente
double ld_quan_in_evasione, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, &
		 ld_fat_conversione_ven, ld_sconto_2, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_evasa, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
		 ld_sconto_8, ld_sconto_9, ld_sconto_10 , &
		 ld_val_riga_sconto_1, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_2, ld_val_sconto_2, &
		 ld_val_riga_sconto_3, ld_val_sconto_3, ld_val_riga_sconto_4, ld_val_sconto_4, &
		 ld_val_riga_sconto_5, ld_val_sconto_5, ld_val_riga_sconto_6, ld_val_sconto_6, &
		 ld_val_riga_sconto_7, ld_val_sconto_7, ld_val_riga_sconto_8, ld_val_sconto_8, &
		 ld_val_riga_sconto_9, ld_val_sconto_9, ld_val_riga_sconto_10, ld_val_sconto_10, &
		 ld_val_sconto_testata, ld_sconto_testata, ld_val_riga_sconto_testata, &
		 ld_val_sconto_pagamento, ld_sconto_pagamento, ld_val_evaso, ld_val_tot_evaso, &
		 ld_tot_val_evaso, ld_tot_val_ordine
		 
boolean lb_riferimento
uo_cs_xx_dw ds_gen_bol_ven_fat_imm

ls_ordini_evasi = " "
setnull(ls_nulla)
// controlli delle testate bolla e ordini vendita
ll_risp = wf_controlli_testata()
if ll_risp = -1 then
	return
end if

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'S' and &
		 parametri_azienda.cod_parametro = 'LIR';
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Configurare il codice valuta per le Lire Italiane.")
	return
end if


commit;
// parametri bolla
dw_dest_ord_bol_ven.get_selected_rows(ll_selected_bol[])
ll_riga = ll_selected_bol[1]
ll_anno_reg_bol = dw_dest_ord_bol_ven.getitemnumber(ll_riga, "anno_registrazione")
ll_num_reg_bol = dw_dest_ord_bol_ven.getitemnumber(ll_riga, "num_registrazione")
ls_cod_tipo_bol_ven = dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_tipo_bol_ven")
ls_cod_valuta =  dw_dest_ord_bol_ven.getitemstring(ll_riga, "cod_valuta")

select max(prog_riga_bol_ven)
into :ll_prog_riga_bol_ven
from det_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_reg_bol and 
		num_registrazione = :ll_num_reg_bol;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice", "Errore 1 in lettura righe bolla di vendita")
	return
end if
if sqlca.sqlcode = 100 or isnull(ll_prog_riga_bol_ven) then ll_prog_riga_bol_ven = 0

select cod_tipo_analisi, 				flag_abilita_rif_ord_ven, 			cod_tipo_det_ven_rif_ord_ven,
		 des_riferimento_ord_ven, 		flag_rif_anno_ord_ven, 	 			flag_rif_num_ord_ven,
		 flag_rif_data_ord_ven
into	 :ls_cod_tipo_analisi_bol,  	:ls_flag_abilita_rif_ord_ven,  :ls_cod_tipo_det_ven_rif_ord_ven,
		 :ls_des_riferimento_ord_ven, :ls_flag_rif_anno_ord_ven,		 :ls_flag_rif_num_ord_ven,
		 :ls_flag_rif_data_ord_ven
from   tab_tipi_bol_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Si è verificato un errore nella lettura della Tabella Tipi Bolle.")
	return
end if





// ordini vendita
ds_gen_bol_ven_fat_imm = i_openparm
ds_gen_bol_ven_fat_imm.get_selected_rows(ll_selected[])

// messagebox("debug", "Fine controlli; ultima riga bolla: " + string(ll_prog_riga_bol_ven))

for ll_i = 1 to upperbound(ll_selected)

	ll_anno_ordine = ds_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_anno_registrazione")
	ll_num_ordine = ds_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_num_registrazione")
	ls_cod_cliente[1] = is_cod_cliente
	ls_cod_fornitore[1] = ls_nulla
	setnull(ldt_data_ordine)
	select data_registrazione, cod_tipo_ord_ven, tot_val_ordine, tot_val_evaso
	into :ldt_data_ordine, :ls_cod_tipo_ord_ven, :ld_tot_val_ordine, :ld_tot_val_evaso
	from tes_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and 
			anno_registrazione = :ll_anno_ordine and
			num_registrazione =:ll_num_ordine;
	if sqlca.sqlcode <0 then 
		g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Errore lettura data in testata ordine " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :ll_anno_ordine and
				evas_ord_ven.num_registrazione = :ll_num_ordine 
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	lb_riferimento = true

	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),  "Errore durante la lettura Evasione Ordini Vendita " + sqlca.sqlerrtext)
			close cu_evas_ord_ven;
			rollback;
			return
		end if

		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		
		select	cod_tipo_det_ven,      cod_misura,    cod_prodotto,      des_prodotto,   
					quan_ordine,     prezzo_vendita,      fat_conversione_ven,    sconto_1,   
					sconto_2,  		provvigione_1, 	provvigione_2, 	cod_iva,   		quan_evasa,
					nota_dettaglio,   	anno_commessa, 		num_commessa, 	cod_centro_costo,
					sconto_3,   	sconto_4,   	sconto_5,   	sconto_6,   	sconto_7,   
					sconto_8,   	sconto_9,   	sconto_10, 	 cod_versione, 		num_confezioni,
					num_pezzi_confezione
		into		:ls_cod_tipo_det_ven, :ls_cod_misura, :ls_cod_prodotto, :ls_des_prodotto, 
					:ld_quan_ordine, :ld_prezzo_vendita, :ld_fat_conversione_ven, :ld_sconto_1, 
					:ld_sconto_2, :ld_provvigione_1, :ld_provvigione_2, :ls_cod_iva, :ld_quan_evasa,
					:ls_nota_dettaglio, :ll_anno_commessa, :ll_num_commessa, :ls_cod_centro_costo,
					:ld_sconto_3, :ld_sconto_4,  :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, 
					:ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ls_cod_versione, :ll_num_confezioni,
					:ll_num_pezzi_confezione
		  from 	det_ord_ven  
		 where 	cod_azienda = :s_cs_xx.cod_azienda and  
					anno_registrazione = :ll_anno_ordine and  
					num_registrazione = :ll_num_ordine and
					prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),  "Errore durante la lettura Dettagli Ordini Vendita " + sqlca.sqlerrtext)
			close cu_evas_ord_ven;
			rollback;
			return 
		end if
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine), "Si è verificato un errore in fase di lettura tipi dettagli.")
			close cu_evas_ord_ven;
			rollback;
			return
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine), "Si è verificato un errore in fase di creazione destinazioni movimenti.")
				close cu_evas_ord_ven;
				rollback;
				return
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine), "Si è verificato un errore in fase di verifica destinazioni movimenti.")
				close cu_evas_ord_ven;
				rollback;
				return
			end if
		end if
		ls_des_riferimento = " "
		if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(ll_anno_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(ll_anno_ordine)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(ll_num_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ll_num_ordine)
			end if
			if ls_flag_rif_data_ord_ven = "S" then
				if not isnull(ldt_data_ordine) then ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			if not isnull(ls_num_ord_cliente) then
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if

			select tab_tipi_det_ven.cod_iva
			into   :ls_cod_iva_rif
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven_rif_ord_ven;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Generazione Bolla singola","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite",Information!)
			end if
			
			insert into det_bol_ven  
							(cod_azienda, 				anno_registrazione,   num_registrazione,   prog_riga_bol_ven,   
							 cod_tipo_det_ven, 		cod_deposito,			 cod_ubicazione, 		 cod_lotto,
							 progr_stock,				data_stock,				 cod_misura,   		 cod_prodotto,   
							 des_prodotto,   			quan_consegnata,   	 prezzo_vendita,   	 fat_conversione_ven,   
							 sconto_1,   				sconto_2,   			 provvigione_1,		 provvigione_2,
							 cod_iva,   				cod_tipo_movimento,	 num_registrazione_mov_mag,
							 nota_dettaglio,   		anno_registrazione_ord_ven,   				 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,		anno_commessa,			 num_commessa,			 cod_centro_costo,
							 sconto_3,   				sconto_4,   			 sconto_5,   			 sconto_6,   
							 sconto_7,   				sconto_8,   			 sconto_9,   			 sconto_10,
							 anno_registrazione_mov_mag,					 anno_reg_bol_acq,	 num_reg_bol_acq,
							 prog_riga_bol_acq,		anno_reg_des_mov,		 num_reg_des_mov,		 cod_versione,
							 num_confezioni,		 	num_pezzi_confezione)
		  values			(:s_cs_xx.cod_azienda, 	:ll_anno_reg_bol, :ll_num_reg_bol,   :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven_rif_ord_ven, null,		 null,					 null,
							 null,						 null,					 null,   				 null,   
							 :ls_des_riferimento,    0,   					 0,   					 1,   
							 0,   						 0,  						 0,						 0,
							 :ls_cod_iva_rif,   		 null,					 null,
							 :ls_note_dettaglio_rif, null,  					 null,   				 
							 null,					 	 null,  					 null,  					 null,   				 
							 0,   					 	 0,  						 0,  						 0,   
							 0,   						 0,   					 0,   					 0,
							 null,						 null,					 null,
							 null,						 null,					 null,					 null,
							 0,							 0);
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Errore durante la scrittura Dettagli Bolle Vendita " + sqlca.sqlerrtext)
				close cu_evas_ord_ven;
				rollback;
				return
			end if
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if	

		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_reg_bol,   
						 :ll_num_reg_bol,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ll_progr_stock[1],
						 :ldt_data_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_fat_conversione_ven,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_nota_dettaglio,   
						 :ll_anno_ordine,   
						 :ll_num_ordine,   
						 :ll_prog_riga_ord_ven,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 null,
						 null,
						 null,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :ls_cod_versione,
						 :ll_num_confezioni,
						 :ll_num_pezzi_confezione);

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Errore durante la scrittura Dettagli Bolle Vendita " + sqlca.sqlerrtext)
			close cu_evas_ord_ven;
			rollback;
			return 
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Si è verificato un errore in fase di aggiornamento magazzino " + sqlca.sqlerrtext)
				close cu_evas_ord_ven;
				rollback;
				return 
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Si è verificato un errore in fase di aggiornamento stock " + sqlca.sqlerrtext)
				close cu_evas_ord_ven;
				rollback;
				return
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_bol_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_bol_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven, ll_anno_reg_bol, ll_num_reg_bol, ll_prog_riga_bol_ven) = -1 then
			close cu_evas_ord_ven;
			rollback;
			return
		end if	
		select  cod_tipo_analisi
		into    :ls_cod_tipo_analisi_ord
		from   tab_tipi_ord_ven  
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Errore durante la lettura della tabella Tipi Ordini Vendita " + sqlca.sqlerrtext)
			rollback;
			return
		end if

		if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_reg_bol, ll_anno_reg_bol, ll_prog_riga_bol_ven, 5) = -1 then
				g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Si è verificato un errore nella Generazione dei Dettagli Statistici.")
				close cu_evas_ord_ven;
				rollback;
				return
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :ll_anno_ordine and  
				 det_ord_ven.num_registrazione = :ll_num_ordine and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita " + sqlca.sqlerrtext)
			close cu_evas_ord_ven;
			rollback;
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :ll_anno_ordine and
						evas_ord_ven.num_registrazione = :ll_num_ordine and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Si è verificato un errore in fase di cancellazione Evasione Ordini " + sqlca.sqlerrtext)
			close cu_evas_ord_ven;
			rollback;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

	update tes_ord_ven  
		set tot_val_evaso = :ld_tot_val_evaso,
			 flag_evasione = :ls_flag_evasione
	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_ven.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine),"Si è verificato un errore in fase di aggiornamento Testata Ordine Vendita " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	// quest'ordine è stato regolarmente evaso: faccio commit
	ls_ordini_evasi = ls_ordini_evasi + "Ordine:" + string(ll_anno_ordine) + "/" + string(ll_num_ordine) + "~r " 
 	commit;
next

g_mb.messagebox("Apice: ordini evasi", ls_ordini_evasi)




end event

