﻿$PBExportHeader$w_det_gen_commesse.srw
$PBExportComments$Finestra Dettaglio Generazione Commesse
forward
global type w_det_gen_commesse from w_cs_xx_risposta
end type
type cb_genera from uo_cb_ok within w_det_gen_commesse
end type
type cb_1 from uo_cb_close within w_det_gen_commesse
end type
type cbx_forza_numerazione from checkbox within w_det_gen_commesse
end type
type sle_num_commessa_forzato from singlelineedit within w_det_gen_commesse
end type
type dw_det_gen_commesse from uo_cs_xx_dw within w_det_gen_commesse
end type
end forward

global type w_det_gen_commesse from w_cs_xx_risposta
integer width = 3145
integer height = 1920
string title = "Dettaglio Generazione Commesse"
cb_genera cb_genera
cb_1 cb_1
cbx_forza_numerazione cbx_forza_numerazione
sle_num_commessa_forzato sle_num_commessa_forzato
dw_det_gen_commesse dw_det_gen_commesse
end type
global w_det_gen_commesse w_det_gen_commesse

on w_det_gen_commesse.create
int iCurrent
call super::create
this.cb_genera=create cb_genera
this.cb_1=create cb_1
this.cbx_forza_numerazione=create cbx_forza_numerazione
this.sle_num_commessa_forzato=create sle_num_commessa_forzato
this.dw_det_gen_commesse=create dw_det_gen_commesse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_genera
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cbx_forza_numerazione
this.Control[iCurrent+4]=this.sle_num_commessa_forzato
this.Control[iCurrent+5]=this.dw_det_gen_commesse
end on

on w_det_gen_commesse.destroy
call super::destroy
destroy(this.cb_genera)
destroy(this.cb_1)
destroy(this.cbx_forza_numerazione)
destroy(this.sle_num_commessa_forzato)
destroy(this.dw_det_gen_commesse)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_gen_commesse.set_dw_options(sqlca, &
                                   pcca.null_object, &
                                   c_multiselect + &
									 		  c_modifyonopen + &
						   			     c_nonew + &
											  c_nodelete + &
											  c_disablecc + &
											  c_disableccinsert, &
                                   c_default)
save_on_close(c_socnosave)

end event

type cb_genera from uo_cb_ok within w_det_gen_commesse
event clicked pbm_bnclicked
integer x = 1070
integer y = 1720
integer width = 361
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;call super::clicked;long    ll_selected[], ll_anno_ordine, ll_null, ll_num_commessa,&
	     ll_num_ordine, ll_prog_riga_ord_ven, ll_i,ll_test,ll_num_commessa_forzato
string  ls_cod_tipo_ord_ven,ls_errore
double  ld_vuoto[]
integer li_risposta,li_anno_commessa
uo_funzioni_1 luo_funzioni_1

luo_funzioni_1 = create uo_funzioni_1

setpointer(hourglass!)

if dw_det_gen_commesse.get_selected_rows(ll_selected[]) > 0 then

	ls_cod_tipo_ord_ven = s_cs_xx.parametri.parametro_s_1
	ll_anno_ordine = s_cs_xx.parametri.parametro_d_1
	ll_num_ordine = s_cs_xx.parametri.parametro_d_2

	s_cs_xx.parametri.parametro_d_1_A[] = ld_vuoto[]
	s_cs_xx.parametri.parametro_d_2_A[] = ld_vuoto[]
	s_cs_xx.parametri.parametro_d_3_A[] = ld_vuoto[]
	s_cs_xx.parametri.parametro_d_4_A[] = ld_vuoto[]

	for ll_i = 1 to upperbound(ll_selected)
		ll_prog_riga_ord_ven = dw_det_gen_commesse.getitemnumber(ll_selected[ll_i], "det_ord_ven_prog_riga_ord_ven")

		if cbx_forza_numerazione.checked = true then
			ll_num_commessa_forzato = long(sle_num_commessa_forzato.text)
		else
			setnull(ll_num_commessa_forzato)
		end if

		if s_cs_xx.parametri.parametro_s_5 = "C" then
			luo_funzioni_1.uof_set_flag_non_collegati( true)
		end if
		li_risposta = luo_funzioni_1.uof_genera_commesse ( ls_cod_tipo_ord_ven, ll_anno_ordine, ll_num_ordine, & 
														ll_prog_riga_ord_ven, ll_num_commessa_forzato, & 
														li_anno_commessa, ll_num_commessa, ls_errore )
		
		if li_risposta < 0 then
			g_mb.messagebox("Sep",ls_errore,stopsign!)
			rollback;
			return
		end if
		
	
		s_cs_xx.parametri.parametro_d_1_A[ll_i] = ll_anno_ordine
		s_cs_xx.parametri.parametro_d_2_A[ll_i] = ll_num_ordine
		s_cs_xx.parametri.parametro_d_3_A[ll_i] = li_anno_commessa
		s_cs_xx.parametri.parametro_d_4_A[ll_i] = ll_num_commessa
		s_cs_xx.parametri.parametro_b_1 = true
		
		if not isnull(ll_num_commessa_forzato) then 
			ll_num_commessa_forzato++
			sle_num_commessa_forzato.text = string(ll_num_commessa_forzato)
		end if
		
	next
	
	commit;
elseif dw_det_gen_commesse.rowcount() = 1 then
	
	ls_cod_tipo_ord_ven = s_cs_xx.parametri.parametro_s_1
	ll_anno_ordine = s_cs_xx.parametri.parametro_d_1
	ll_num_ordine = s_cs_xx.parametri.parametro_d_2

	s_cs_xx.parametri.parametro_d_1_A[] = ld_vuoto[]
	s_cs_xx.parametri.parametro_d_2_A[] = ld_vuoto[]
	s_cs_xx.parametri.parametro_d_3_A[] = ld_vuoto[]
	s_cs_xx.parametri.parametro_d_4_A[] = ld_vuoto[]

	ll_prog_riga_ord_ven = dw_det_gen_commesse.getitemnumber( 1, "det_ord_ven_prog_riga_ord_ven")

	if cbx_forza_numerazione.checked = true then
		ll_num_commessa_forzato = long(sle_num_commessa_forzato.text)
	else
		setnull(ll_num_commessa_forzato)
	end if

	if s_cs_xx.parametri.parametro_s_5 = "C" then
		luo_funzioni_1.uof_set_flag_non_collegati( true)
	end if
	li_risposta = luo_funzioni_1.uof_genera_commesse ( ls_cod_tipo_ord_ven, ll_anno_ordine, ll_num_ordine, & 
													ll_prog_riga_ord_ven, ll_num_commessa_forzato, & 
													li_anno_commessa, ll_num_commessa, ls_errore )
		
	if li_risposta < 0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		rollback;
		return
	end if
		
	
	s_cs_xx.parametri.parametro_d_1_A[1] = ll_anno_ordine
	s_cs_xx.parametri.parametro_d_2_A[1] = ll_num_ordine
	s_cs_xx.parametri.parametro_d_3_A[1] = li_anno_commessa
	s_cs_xx.parametri.parametro_d_4_A[1] = ll_num_commessa
	s_cs_xx.parametri.parametro_b_1 = true
	
	if not isnull(ll_num_commessa_forzato) then 
		ll_num_commessa_forzato++
		sle_num_commessa_forzato.text = string(ll_num_commessa_forzato)
	end if
		
	commit;	
	
end if

destroy luo_funzioni_1

dw_det_gen_commesse.triggerevent("pcd_retrieve")
setpointer(arrow!)
end event

type cb_1 from uo_cb_close within w_det_gen_commesse
integer x = 2720
integer y = 1720
integer width = 361
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type cbx_forza_numerazione from checkbox within w_det_gen_commesse
integer x = 18
integer y = 1720
integer width = 590
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Forza Num. Commessa"
boolean lefttext = true
end type

event clicked;if sle_num_commessa_forzato.enabled=true then
	sle_num_commessa_forzato.enabled=false
else
	sle_num_commessa_forzato.enabled=true
end if

end event

type sle_num_commessa_forzato from singlelineedit within w_det_gen_commesse
integer x = 640
integer y = 1720
integer width = 320
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
boolean autohscroll = false
end type

type dw_det_gen_commesse from uo_cs_xx_dw within w_det_gen_commesse
integer x = 23
integer y = 20
integer width = 3063
integer height = 1680
integer taborder = 20
string dataobject = "d_det_gen_commesse"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long   ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2,s_cs_xx.parametri.parametro_data_1,s_cs_xx.parametri.parametro_data_2, 'M', s_cs_xx.parametri.parametro_s_5)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

