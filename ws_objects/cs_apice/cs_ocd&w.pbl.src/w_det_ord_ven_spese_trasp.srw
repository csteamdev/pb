﻿$PBExportHeader$w_det_ord_ven_spese_trasp.srw
forward
global type w_det_ord_ven_spese_trasp from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_det_ord_ven_spese_trasp
end type
end forward

global type w_det_ord_ven_spese_trasp from w_cs_xx_principale
integer width = 3803
integer height = 2204
string title = "Riepilogo calcolo Spese Trasporto"
dw_lista dw_lista
end type
global w_det_ord_ven_spese_trasp w_det_ord_ven_spese_trasp

on w_det_ord_ven_spese_trasp.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_det_ord_ven_spese_trasp.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag
windowobject lw_oggetti[], lw_vuoto[]

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("anno_registrazione")
dw_lista.set_dw_key("num_registrazione")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent + c_nonew + c_nomodify + c_nodelete, &
                                    c_default )


end event

type dw_lista from uo_cs_xx_dw within w_det_ord_ven_spese_trasp
integer x = 27
integer y = 32
integer width = 3707
integer height = 2040
integer taborder = 10
string dataobject = "d_det_ord_ven_spese_trasp"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;integer			li_anno_registrazione
long				ll_errore, ll_num_registrazione
string				ls_cod_cliente, ls_rag_soc_1

li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente ;
if sqlca.sqlcode = 0 then
	parent.title = "Riepilogo Calcolo Spese Trasp. per Ordine " + &
						string(li_anno_registrazione,"####") + "/" + &
						string(ll_num_registrazione) + "  Cliente: " + ls_rag_soc_1
end if

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

