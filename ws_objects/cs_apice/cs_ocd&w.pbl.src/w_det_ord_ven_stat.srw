﻿$PBExportHeader$w_det_ord_ven_stat.srw
$PBExportComments$Finestra Gestione Dettaglio Ordini Vendita Satatistici
forward
global type w_det_ord_ven_stat from w_cs_xx_principale
end type
type dw_det_ord_ven_stat from uo_cs_xx_dw within w_det_ord_ven_stat
end type
type ddlb_cod_tipo_analisi from dropdownlistbox within w_det_ord_ven_stat
end type
type st_cod_tipo_analisi from statictext within w_det_ord_ven_stat
end type
type st_1 from statictext within w_det_ord_ven_stat
end type
end forward

global type w_det_ord_ven_stat from w_cs_xx_principale
int Width=3443
int Height=1465
boolean TitleBar=true
string Title="Distribuzione % Dettaglio Ordini Vendita"
dw_det_ord_ven_stat dw_det_ord_ven_stat
ddlb_cod_tipo_analisi ddlb_cod_tipo_analisi
st_cod_tipo_analisi st_cod_tipo_analisi
st_1 st_1
end type
global w_det_ord_ven_stat w_det_ord_ven_stat

event pc_setwindow;call super::pc_setwindow;dw_det_ord_ven_stat.set_dw_key("cod_azienda")
dw_det_ord_ven_stat.set_dw_key("anno_registrazione")
dw_det_ord_ven_stat.set_dw_key("num_registrazione")
dw_det_ord_ven_stat.set_dw_key("prog_riga_ord_ven")
dw_det_ord_ven_stat.set_dw_key("cod_tipo_analisi")
dw_det_ord_ven_stat.set_dw_key("progressivo")

dw_det_ord_ven_stat.set_dw_options(sqlca, i_openparm,c_default,c_default)

end event

event closequery;	integer li_somma_percentuale
	if dw_det_ord_ven_stat.rowcount()>0 then	
		li_somma_percentuale = dw_det_ord_ven_stat.getitemnumber(dw_det_ord_ven_stat.getrow(),"somma_percentuale")

		if li_somma_percentuale <> 100 then
			g_mb.messagebox("Apice","Attenzione! La somma delle percentuali è diversa da 100",exclamation!)
			return 1
		end if
	end if


//******************************************************************
//  PC Module     : w_Main
//  Event         : CloseQuery
//  Description   : Verifies that it is Okay to close the window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_TriggeredEvent, l_HaveChanges
INTEGER       l_NumChangedDWs,  l_Answer,       l_Idx
UNSIGNEDLONG  l_SaveOnClose
UNSIGNEDLONG  l_RefreshCmd,     l_RefCmd[]
UO_DW_MAIN    l_RootDW,         l_ChangedDWs[], l_TmpDW

//------------------------------------------------------------------
//  Remember if we were triggered internally.  If we are, don't
//  ask any questions, just try to close.  If we are an internally
//  triggered event, we were most likely triggered by pcd_Close.
//------------------------------------------------------------------

l_TriggeredEvent = PCCA.Do_Event
PCCA.Do_Event    = FALSE
PCCA.Error       = c_Success

IF NOT l_TriggeredEvent THEN
IF i_FirstDWIsValid     THEN

   //---------------------------------------------------------------
   //  If we get here, it was because the close was triggered
   //  externally.  Check to see if all of the DataWindows on
   //  this window are ready to close.
   //---------------------------------------------------------------

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Close, c_ShowMark)

   //---------------------------------------------------------------
   //  Make sure that all of the DataWindows are saved.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
      l_RefCmd[l_Idx] = c_RefreshUndefined
   NEXT

   l_SaveOnClose = i_SaveOnClose

   //---------------------------------------------------------------
   //  If we are to prompt the user once, figure out if any
   //  DataWindows have changed and prompt them once.
   //---------------------------------------------------------------

   IF l_SaveOnClose = c_SOCPromptUserOnce THEN

      l_HaveChanges = FALSE
      FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs

         l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]

         IF l_TmpDW.i_InUse THEN
            IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN
               l_HaveChanges = TRUE
               l_RefCmd[l_Idx] = l_RefreshCmd
               l_TmpDW.Build_Changed_List(l_NumChangedDWs, &
                                          l_ChangedDWs[])
            END IF
         END IF
      NEXT

      IF l_HaveChanges THEN
         l_Answer = i_FirstDW.Ask_User(l_NumChangedDWs, &
                                       l_ChangedDWs[])
         CHOOSE CASE l_Answer
            CASE 2
              l_SaveOnClose = c_SOCNoSave

            CASE 3

               //---------------------------------------------
               //  The user canceled.
               //---------------------------------------------

               PCCA.MDI.fu_Pop()
               PCCA.Error = c_Fatal
               GOTO Finished

            CASE ELSE
              l_SaveOnClose = c_SOCSave
         END CHOOSE
      END IF
   END IF

   CHOOSE CASE l_SaveOnClose

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  reset the modification flags.
      //------------------------------------------------------------

      CASE c_SOCNoSave

         FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
            l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
            IF l_TmpDW.i_InUse THEN
               IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN

                  //------------------------------------------------
                  //  Clear the modification flags.
                  //------------------------------------------------

                  l_TmpDW.Reset_DW_Modified(c_ResetChildren)

                  //------------------------------------------------
                  //  If the close aborts, we'll need to refresh
                  //  the DataWindows since the modification flags
                  //  have been cleard.
                  //------------------------------------------------

                  l_RefCmd[l_Idx]            = c_RefreshSame
                  l_TmpDW.i_RetrieveMySelf   = TRUE
                  l_TmpDW.i_RetrieveChildren = TRUE
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  save any DataWindows which have been modified.
      //------------------------------------------------------------

      CASE c_SOCSave

         FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
            l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
            IF l_TmpDW.i_InUse THEN
               IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN

                  //------------------------------------------------
                  //  Save the DataWindow.
                  //------------------------------------------------

                  l_RefCmd[l_Idx] = c_RefreshSave

                  l_RootDW = l_TmpDW.i_RootDW
                  l_RootDW.is_EventControl.No_Refresh_After_Save = &
                     TRUE
                  l_RootDW.TriggerEvent("pcd_Save")

                  //------------------------------------------------
                  //  If PCCA.Error does not indicate success, then
                  //  pcd_Save failed (e.g. validation error).
                  //------------------------------------------------

                  IF PCCA.Error <> c_Success THEN
                     l_RefCmd[l_Idx] = c_RefreshUndefined

                     //---------------------------------------------
                     //  Pop the close prompt off and exit the
                     //  event.
                     //---------------------------------------------

                     PCCA.MDI.fu_Pop()
                     PCCA.Error = c_Fatal
                     GOTO Finished
                  END IF

                  //------------------------------------------------
                  //  If a save happened, the DataWindow hierarchy
                  //  may need refreshing.
                  //------------------------------------------------

                  IF l_RootDW         <> l_TmpDW            THEN
                  IF l_RefreshCmd     <> c_RefreshUndefined THEN
                     l_RootDW.is_EventControl.Skip_DW_Valid = TRUE
                     l_RootDW.is_EventControl.Skip_DW       = l_TmpDW
                     l_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
                     l_RootDW.TriggerEvent("pcd_Refresh")

                     //---------------------------------------------
                     //  If an error occurred during the refresh,
                     //  don't allow the close to happen.
                     //---------------------------------------------

                     IF PCCA.Error <> c_Success THEN
                        PCCA.MDI.fu_Pop()
                        PCCA.Error = c_Fatal
                        GOTO Finished
                     END IF
                  END IF
                  END IF
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  prompt the user for any that have been modified.
      //------------------------------------------------------------

      CASE c_SOCPromptUser

         FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
            l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
            IF l_TmpDW.i_InUse THEN
               l_TmpDW.Check_Save(l_RefreshCmd)
               l_RefCmd[l_Idx] = l_RefreshCmd

               //---------------------------------------------------
               //  If PCCA.Error does not indicate success, then
               //  the user hit cancel or pcd_Save failed (e.g.
               //  validation error).
               //---------------------------------------------------

               IF PCCA.Error <> c_Success THEN

                  //------------------------------------------------
                  //  Pop the close prompt off and exit the event.
                  //------------------------------------------------

                  PCCA.MDI.fu_Pop()
                  PCCA.Error = c_Fatal
                  GOTO Finished
               END IF

               //---------------------------------------------------
               //  If a save, the DataWindow hierarchy may need
               //  refreshing.
               //---------------------------------------------------

               l_RootDW = l_TmpDW.i_RootDW

               IF l_RootDW         <> l_TmpDW            THEN
               IF l_RefreshCmd     <> c_RefreshUndefined THEN
                  l_RootDW.is_EventControl.Skip_DW_Valid = TRUE
                  l_RootDW.is_EventControl.Skip_DW       = l_TmpDW
                  l_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
                  l_RootDW.TriggerEvent("pcd_Refresh")

                  //------------------------------------------------------
                  //  If an error occurred during the refresh, don't
                  //  allow the close to happen.
                  //------------------------------------------------------

                  IF PCCA.Error <> c_Success THEN
                     PCCA.MDI.fu_Pop()
                     PCCA.Error = c_Fatal
                     GOTO Finished
                  END IF
               END IF
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  This case was already taken care of.  If we get to here
      //  there are no changes to be saved.
      //------------------------------------------------------------

      CASE c_SOCPromptUserOnce
   END CHOOSE

   //---------------------------------------------------------------
   //  Now we can close them.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs

      //------------------------------------------------------------
      //  Set the No_Close flag to tell the DataWindow that it is
      //  being triggered by CloseQuery.
      //------------------------------------------------------------

      l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
      IF l_TmpDW.i_InUse THEN
         l_TmpDW.is_EventControl.No_Close = TRUE
         l_TmpDW.TriggerEvent("pcd_Close")

         //---------------------------------------------------------
         //  If there was an error, abort the close process.
         //---------------------------------------------------------

         IF PCCA.Error <> c_Success THEN
            PCCA.MDI.fu_Pop()
            PCCA.Error = c_Fatal
            GOTO Finished
         END IF
      END IF
   NEXT

   i_FirstDWIsValid = FALSE

   PCCA.MDI.fu_Pop()
END IF
END IF

Finished:

//------------------------------------------------------------------
//  If we are not closing the DataWindow, we need to make sure
//  that any DataWindows get refreshed correctly.
//------------------------------------------------------------------

IF PCCA.Error <> c_Success THEN
IF i_FirstDWIsValid        THEN
   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
      IF l_RefCmd[l_Idx] <> c_RefreshUndefined THEN
         l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
         l_TmpDW.is_EventControl.Refresh_Cmd = l_RefCmd[l_Idx]
         l_TmpDW.TriggerEvent("pcd_Refresh")
      END IF
   NEXT
   PCCA.Error = c_Fatal
END IF
END IF

IF PCCA.Error <> c_Success THEN
   Message.ReturnValue = 1
   RETURN
END IF
end event

on w_det_ord_ven_stat.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_ord_ven_stat=create dw_det_ord_ven_stat
this.ddlb_cod_tipo_analisi=create ddlb_cod_tipo_analisi
this.st_cod_tipo_analisi=create st_cod_tipo_analisi
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_ord_ven_stat
this.Control[iCurrent+2]=ddlb_cod_tipo_analisi
this.Control[iCurrent+3]=st_cod_tipo_analisi
this.Control[iCurrent+4]=st_1
end on

on w_det_ord_ven_stat.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_ord_ven_stat)
destroy(this.ddlb_cod_tipo_analisi)
destroy(this.st_cod_tipo_analisi)
destroy(this.st_1)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_cod_tipo_analisi, &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

type dw_det_ord_ven_stat from uo_cs_xx_dw within w_det_ord_ven_stat
event losefocus pbm_dwnkillfocus
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
event pcd_validaterow pbm_custom74
int X=23
int Y=141
int Width=3361
int Height=1201
string DataObject="d_det_ord_ven_stat"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event losefocus;call super::losefocus;if i_extendmode and rowcount()>0 then
	integer li_somma_percentuale
	
	li_somma_percentuale = getitemnumber(getrow(),"somma_percentuale")

	if li_somma_percentuale <> 100 then
		g_mb.messagebox("Apice","Attenzione! La somma delle percentuali è diversa da 100",exclamation!)
		return
	end if

end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long l_idx,ll_progressivo,ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ll_i,ll_i1
	string ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_det_ven,ls_cod_tipo_ord_ven
	integer li_somma_percentuale,li_percentuale
	
	ll_i = i_parentdw.i_parentdw.i_selectedrows[1]
	ll_i1 = i_parentdw.i_selectedrows[1]

	ll_anno_registrazione = i_parentdw.i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = i_parentdw.i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = i_parentdw.getitemnumber(ll_i1, "prog_riga_ord_ven")
	ls_cod_tipo_det_ven = i_parentdw.getitemstring(ll_i1, "cod_tipo_det_ven")
	ls_cod_tipo_ord_ven = i_parentdw.i_parentdw.getitemstring(ll_i, "cod_tipo_ord_ven")
	
	ls_cod_tipo_analisi = st_cod_tipo_analisi.text

	if isnull(ls_cod_tipo_analisi) or ls_cod_tipo_analisi="" then
		SELECT cod_tipo_analisi
		INTO   :ls_cod_tipo_analisi  
		FROM   tab_tipi_ord_ven
		WHERE  cod_azienda = :s_cs_xx.cod_azienda 
		AND    cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if

	s_cs_xx.parametri.parametro_s_1 = ls_cod_tipo_analisi

	li_somma_percentuale = getitemnumber(getrow(),"somma_percentuale")
	li_percentuale = 100 - li_somma_percentuale

	select max(progressivo)
	into   :ll_progressivo
	from   det_ord_ven_stat
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_ord_ven=:ll_prog_riga_ord_ven
	and    cod_tipo_analisi=:ls_cod_tipo_analisi;

	if isnull(ll_progressivo) or ll_progressivo=0 then
		ll_progressivo=1
	else
		ll_progressivo++
	end if

	FOR l_Idx = 1 TO RowCount()
		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
   		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
			SetItem(l_Idx, "anno_registrazione", ll_anno_registrazione)
			SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
			SetItem(l_Idx, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
			SetItem(l_Idx, "cod_tipo_analisi", ls_cod_tipo_analisi)
			SetItem(l_Idx, "progressivo", ll_progressivo)
			SetItem(l_Idx, "percentuale", li_percentuale)
  		end if
	NEXT
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG   l_Error,ll_anno_registrazione,ll_num_registrazione, & 
		 ll_prog_riga_ord_ven,ll_i,ll_i1
string ls_cod_tipo_analisi, &
		 ls_cod_tipo_ord_ven, &
		 ls_cod_tipo_det_ven, &
		 ls_label_livello_1,&
 		 ls_label_livello_2,&
	 	 ls_label_livello_3,&
		 ls_label_livello_4,&
		 ls_label_livello_5,&
		 ls_label_livello_6,&
		 ls_label_livello_7,&
		 ls_label_livello_8,&
		 ls_label_livello_9,&
		 ls_label_livello_10,&
   	 ls_des_tipo_analisi

integer li_num_liv_gestiti

ll_i = i_parentdw.i_parentdw.i_selectedrows[1]
ll_i1 = i_parentdw.i_selectedrows[1]

ll_anno_registrazione = i_parentdw.i_parentdw.getitemnumber(ll_i, "anno_registrazione")
ll_num_registrazione = i_parentdw.i_parentdw.getitemnumber(ll_i, "num_registrazione")
ll_prog_riga_ord_ven = i_parentdw.getitemnumber(ll_i1, "prog_riga_ord_ven")
ls_cod_tipo_det_ven = i_parentdw.getitemstring(ll_i1, "cod_tipo_det_ven")
ls_cod_tipo_ord_ven = i_parentdw.i_parentdw.getitemstring(ll_i, "cod_tipo_ord_ven")
	
ls_cod_tipo_analisi = st_cod_tipo_analisi.text

if isnull(ls_cod_tipo_analisi) or ls_cod_tipo_analisi="" then
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_ord_ven
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

	select des_tipo_analisi
	into   :ls_des_tipo_analisi
	from   tab_tipi_analisi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_analisi=:ls_cod_tipo_analisi;

	st_cod_tipo_analisi.text=ls_cod_tipo_analisi 
	ddlb_cod_tipo_analisi.text=ls_des_tipo_analisi

end if

s_cs_xx.parametri.parametro_s_1 = ls_cod_tipo_analisi
l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_cod_tipo_analisi)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
 
select num_liv_gestiti,
		 label_livello_1,
		 label_livello_2,
		 label_livello_3,
		 label_livello_4,
		 label_livello_5,
		 label_livello_6,
		 label_livello_7,
		 label_livello_8,
		 label_livello_9,
		 label_livello_10
into   :li_num_liv_gestiti,
		 :ls_label_livello_1,
 		 :ls_label_livello_2,
	 	 :ls_label_livello_3,
		 :ls_label_livello_4,
		 :ls_label_livello_5,
		 :ls_label_livello_6,
		 :ls_label_livello_7,
		 :ls_label_livello_8,
		 :ls_label_livello_9,
		 :ls_label_livello_10
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ls_label_livello_1) then ls_label_livello_1=""
if isnull(ls_label_livello_2) then ls_label_livello_2=""
if isnull(ls_label_livello_3) then ls_label_livello_3=""
if isnull(ls_label_livello_4) then ls_label_livello_4=""
if isnull(ls_label_livello_5) then ls_label_livello_5=""
if isnull(ls_label_livello_6) then ls_label_livello_6=""
if isnull(ls_label_livello_7) then ls_label_livello_7=""
if isnull(ls_label_livello_8) then ls_label_livello_8=""
if isnull(ls_label_livello_9) then ls_label_livello_9=""
if isnull(ls_label_livello_10) then ls_label_livello_10=""

choose case li_num_liv_gestiti
	case 1
		Object.cod_statistico_1_t.Text = ls_label_livello_1
		Object.percentuale.visible = 0
		Object.cod_statistico_1.visible = 1
		Object.cod_statistico_2.visible = 0
		Object.cod_statistico_3.visible = 0
		Object.cod_statistico_4.visible = 0
		Object.cod_statistico_5.visible = 0
		Object.cod_statistico_6.visible = 0
		Object.cod_statistico_7.visible = 0
		Object.cod_statistico_8.visible = 0
		Object.cod_statistico_9.visible = 0
		Object.cod_statistico_10.visible = 0
		Object.percentuale.visible = 1
	  	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
   	         "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

		case 2
			Object.cod_statistico_1_t.Text =ls_label_livello_1
			Object.cod_statistico_2_t.Text =ls_label_livello_2
			Object.percentuale.visible = 0
			Object.cod_statistico_1.visible = 1
			Object.cod_statistico_2.visible = 1
			Object.cod_statistico_3.visible = 0
			Object.cod_statistico_4.visible = 0
			Object.cod_statistico_5.visible = 0
			Object.cod_statistico_6.visible = 0
			Object.cod_statistico_7.visible = 0
			Object.cod_statistico_8.visible = 0
			Object.cod_statistico_9.visible = 0
			Object.cod_statistico_10.visible = 0
			Object.percentuale.visible = 1
	       	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 3
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 0
					Object.cod_statistico_5.visible = 0
					Object.cod_statistico_6.visible = 0
					Object.cod_statistico_7.visible = 0
					Object.cod_statistico_8.visible = 0
					Object.cod_statistico_9.visible = 0
					Object.cod_statistico_10.visible = 0
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 4
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.cod_statistico_4_t.Text =ls_label_livello_4
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 1
					Object.cod_statistico_5.visible = 0
					Object.cod_statistico_6.visible = 0
					Object.cod_statistico_7.visible = 0
					Object.cod_statistico_8.visible = 0
					Object.cod_statistico_9.visible = 0
					Object.cod_statistico_10.visible = 0
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 5
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.cod_statistico_4_t.Text =ls_label_livello_4
					Object.cod_statistico_5_t.Text =ls_label_livello_5
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 1
					Object.cod_statistico_5.visible = 1
					Object.cod_statistico_6.visible = 0
					Object.cod_statistico_7.visible = 0
					Object.cod_statistico_8.visible = 0
					Object.cod_statistico_9.visible = 0
					Object.cod_statistico_10.visible = 0
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 6
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.cod_statistico_4_t.Text =ls_label_livello_4
					Object.cod_statistico_5_t.Text =ls_label_livello_5
					Object.cod_statistico_6_t.Text =ls_label_livello_6
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 1
					Object.cod_statistico_5.visible = 1
					Object.cod_statistico_6.visible = 1
					Object.cod_statistico_7.visible = 0
					Object.cod_statistico_8.visible = 0
					Object.cod_statistico_9.visible = 0
					Object.cod_statistico_10.visible = 0
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 7
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.cod_statistico_4_t.Text =ls_label_livello_4
					Object.cod_statistico_5_t.Text =ls_label_livello_5
					Object.cod_statistico_6_t.Text =ls_label_livello_6
					Object.cod_statistico_7_t.Text =ls_label_livello_7
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 1
					Object.cod_statistico_5.visible = 1
					Object.cod_statistico_6.visible = 1
					Object.cod_statistico_7.visible = 1
					Object.cod_statistico_8.visible = 0
					Object.cod_statistico_9.visible = 0
					Object.cod_statistico_10.visible = 0
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 8
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.cod_statistico_4_t.Text =ls_label_livello_4
					Object.cod_statistico_5_t.Text =ls_label_livello_5
					Object.cod_statistico_6_t.Text =ls_label_livello_6
					Object.cod_statistico_7_t.Text =ls_label_livello_7
			   	Object.cod_statistico_8_t.Text =ls_label_livello_8
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 1
					Object.cod_statistico_5.visible = 1
					Object.cod_statistico_6.visible = 1
					Object.cod_statistico_7.visible = 1
					Object.cod_statistico_8.visible = 1
					Object.cod_statistico_9.visible = 0
					Object.cod_statistico_10.visible = 0
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_8",sqlca,&
               "tab_statistica_8","cod_statistico_8","des_statistico_8","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 9
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.cod_statistico_4_t.Text =ls_label_livello_4
					Object.cod_statistico_5_t.Text =ls_label_livello_5
					Object.cod_statistico_6_t.Text =ls_label_livello_6
					Object.cod_statistico_7_t.Text =ls_label_livello_7
			   	Object.cod_statistico_8_t.Text =ls_label_livello_8
					Object.cod_statistico_9_t.Text =ls_label_livello_9
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 1
					Object.cod_statistico_5.visible = 1
					Object.cod_statistico_6.visible = 1
					Object.cod_statistico_7.visible = 1
					Object.cod_statistico_8.visible = 1
					Object.cod_statistico_9.visible = 1
					Object.cod_statistico_10.visible = 0
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_8",sqlca,&
               "tab_statistica_8","cod_statistico_8","des_statistico_8","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_9",sqlca,&
               "tab_statistica_9","cod_statistico_9","des_statistico_9","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
				case 10
					Object.cod_statistico_1_t.Text =ls_label_livello_1
					Object.cod_statistico_2_t.Text =ls_label_livello_2
					Object.cod_statistico_3_t.Text =ls_label_livello_3
					Object.cod_statistico_4_t.Text =ls_label_livello_4
					Object.cod_statistico_5_t.Text =ls_label_livello_5
					Object.cod_statistico_6_t.Text =ls_label_livello_6
					Object.cod_statistico_7_t.Text =ls_label_livello_7
			   	Object.cod_statistico_8_t.Text =ls_label_livello_8
					Object.cod_statistico_9_t.Text =ls_label_livello_9
					Object.cod_statistico_10_t.Text =ls_label_livello_10
					Object.percentuale.visible = 0
					Object.cod_statistico_1.visible = 1
					Object.cod_statistico_2.visible = 1
					Object.cod_statistico_3.visible = 1
					Object.cod_statistico_4.visible = 1
					Object.cod_statistico_5.visible = 1
					Object.cod_statistico_6.visible = 1
					Object.cod_statistico_7.visible = 1
					Object.cod_statistico_8.visible = 1
					Object.cod_statistico_9.visible = 1
					Object.cod_statistico_10.visible = 1
					Object.percentuale.visible = 1
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_8",sqlca,&
               "tab_statistica_8","cod_statistico_8","des_statistico_8","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_9",sqlca,&
               "tab_statistica_9","cod_statistico_9","des_statistico_9","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_det_ord_ven_stat,"cod_statistico_10",sqlca,&
               "tab_statistica_10","cod_statistico_10","des_statistico_10","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

end choose

end event

event pcd_validaterow;call super::pcd_validaterow;integer li_somma_percentuale, li_t,li_num_livelli
string ls_cod_statistico_1, &
		 ls_cod_statistico_2, &
 		 ls_cod_statistico_3, &
		 ls_cod_statistico_4, &
		 ls_cod_statistico_5, &
		 ls_cod_statistico_6, &
		 ls_cod_statistico_7, &
		 ls_cod_statistico_8, &
		 ls_cod_statistico_9, &
		 ls_cod_statistico_10, &
		 ls_test,ls_cod_tipo_analisi
boolean lb_1,lb_2,lb_3,lb_4,lb_5,lb_6,lb_7,lb_8,lb_9,lb_10
	
lb_1=false
lb_2=false
lb_3=false
lb_4=false
lb_5=false
lb_6=false
lb_7=false
lb_8=false
lb_9=false
lb_10=false
	
ls_cod_tipo_analisi = getitemstring(getrow(),"cod_tipo_analisi")

select num_liv_gestiti
into   :li_num_livelli
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and	 cod_tipo_analisi=:ls_cod_tipo_analisi;

choose case li_num_livelli
	case 1
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_2=true
		lb_3=true
		lb_4=true
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 2
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_3=true
		lb_4=true
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 3
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_4=true
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 4
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(getrow(),"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 5
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(getrow(),"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(getrow(),"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 6
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(getrow(),"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(getrow(),"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(getrow(),"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 7
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(getrow(),"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(getrow(),"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(getrow(),"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(getrow(),"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_8=true
		lb_9=true
		lb_10=true

	case 8
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(getrow(),"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(getrow(),"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(getrow(),"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(getrow(),"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_8 = getitemstring(getrow(),"cod_statistico_8")
		if ls_cod_statistico_8= "" or isnull(ls_cod_statistico_8) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 8.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_9=true
		lb_10=true

	case 9
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(getrow(),"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(getrow(),"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(getrow(),"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(getrow(),"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_8 = getitemstring(getrow(),"cod_statistico_8")
		if ls_cod_statistico_8= "" or isnull(ls_cod_statistico_8) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 8.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_9 = getitemstring(getrow(),"cod_statistico_9")
		if ls_cod_statistico_9= "" or isnull(ls_cod_statistico_9) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 9.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_10=true

	case 10
		ls_cod_statistico_1 = getitemstring(getrow(),"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(getrow(),"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(getrow(),"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(getrow(),"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(getrow(),"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(getrow(),"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(getrow(),"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_8 = getitemstring(getrow(),"cod_statistico_8")
		if ls_cod_statistico_8= "" or isnull(ls_cod_statistico_8) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 8.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_9 = getitemstring(getrow(),"cod_statistico_9")
		if ls_cod_statistico_9= "" or isnull(ls_cod_statistico_9) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 9.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_10 = getitemstring(getrow(),"cod_statistico_10")
		if ls_cod_statistico_10= "" or isnull(ls_cod_statistico_10) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 10.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
end choose

li_somma_percentuale = getitemnumber(getrow(),"somma_percentuale")

for li_t = 1 to rowcount()
	if li_t=getrow() then continue
	if ls_cod_statistico_1=getitemstring(li_t,"cod_statistico_1") then lb_1=true
	if ls_cod_statistico_2=getitemstring(li_t,"cod_statistico_2") then lb_2=true
	if ls_cod_statistico_3=getitemstring(li_t,"cod_statistico_3") then lb_3=true
	if ls_cod_statistico_4=getitemstring(li_t,"cod_statistico_4") then lb_4=true
	if ls_cod_statistico_5=getitemstring(li_t,"cod_statistico_5") then lb_5=true
	if ls_cod_statistico_6=getitemstring(li_t,"cod_statistico_6") then lb_6=true
	if ls_cod_statistico_7=getitemstring(li_t,"cod_statistico_7") then lb_7=true
	if ls_cod_statistico_8=getitemstring(li_t,"cod_statistico_8") then lb_8=true
	if ls_cod_statistico_9=getitemstring(li_t,"cod_statistico_9") then lb_9=true
	if ls_cod_statistico_10=getitemstring(li_t,"cod_statistico_10") then lb_10=true
	if lb_1 and lb_2 and lb_3 and lb_4 and lb_5 and lb_6 and lb_7 and lb_8 and lb_9 and lb_10 then
		li_t=rowcount()
		g_mb.messagebox("Apice","Attenzione! Esiste già una suddivisione come quella impostata, controllare e reimpostare i livelli",exclamation!)
		pcca.error=c_valfailed
		continue
	end if
	choose case li_num_livelli
		case 1	
			lb_1=false
		case 2
			lb_1=false
			lb_2=false
		case 3
			lb_1=false
			lb_2=false
			lb_3=false
		case 4
			lb_1=false
			lb_2=false
			lb_3=false
			lb_4=false
		case 5
			lb_1=false
			lb_2=false
			lb_3=false
			lb_4=false
			lb_5=false
		case 6
			lb_1=false
			lb_2=false
			lb_3=false
			lb_4=false
			lb_5=false
			lb_6=false
		case 7
			lb_1=false
			lb_2=false
			lb_3=false
			lb_4=false
			lb_5=false
			lb_6=false
			lb_7=false
		case 8
			lb_1=false
			lb_2=false
			lb_3=false
			lb_4=false
			lb_5=false
			lb_6=false
			lb_7=false
			lb_8=false
		case 9	
			lb_1=false
			lb_2=false
			lb_3=false
			lb_4=false
			lb_5=false
			lb_6=false
			lb_7=false
			lb_8=false
			lb_9=false
		case 10
			lb_1=false
			lb_2=false
			lb_3=false
			lb_4=false
			lb_5=false
			lb_6=false
			lb_7=false
			lb_8=false
			lb_9=false
			lb_10=false
	end choose
next 

if (li_somma_percentuale > 100) then
	g_mb.messagebox("Apice","Attenzione! La somma % è errata, reimpostare la distribuzione %.",exclamation!)
	pcca.error=c_valfailed
end if

end event

type ddlb_cod_tipo_analisi from dropdownlistbox within w_det_ord_ven_stat
event selectionchanged pbm_cbnselchange
int X=732
int Y=21
int Width=915
int Height=1321
int TabOrder=1
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;st_cod_tipo_analisi.text=f_po_selectddlb(ddlb_cod_tipo_analisi)
parent.triggerevent("pc_retrieve")

end event

type st_cod_tipo_analisi from statictext within w_det_ord_ven_stat
int X=343
int Y=21
int Width=389
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
long BorderColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_det_ord_ven_stat
int X=23
int Y=21
int Width=321
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Tipo Analisi:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

