﻿$PBExportHeader$w_det_ord_ven_tv.srw
$PBExportComments$Finestra Dettaglio Ordini di Vendita
forward
global type w_det_ord_ven_tv from w_cs_xx_principale
end type
type cbx_depositi from checkbox within w_det_ord_ven_tv
end type
type uo_1 from uo_situazione_prodotto within w_det_ord_ven_tv
end type
type dw_depositi from datawindow within w_det_ord_ven_tv
end type
type cb_varianti from picturebutton within w_det_ord_ven_tv
end type
type dw_folder from u_folder within w_det_ord_ven_tv
end type
type dw_det_ord_ven_lista from uo_cs_xx_dw within w_det_ord_ven_tv
end type
type dw_det_ord_ven_det_1 from uo_cs_xx_dw within w_det_ord_ven_tv
end type
type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_ord_ven_tv
end type
type cb_acquisto from commandbutton within w_det_ord_ven_tv
end type
type cb_annulla_acq from commandbutton within w_det_ord_ven_tv
end type
type cb_assegnazione from commandbutton within w_det_ord_ven_tv
end type
type cb_azzera from commandbutton within w_det_ord_ven_tv
end type
type cb_blocca from commandbutton within w_det_ord_ven_tv
end type
type cb_c_industriale from commandbutton within w_det_ord_ven_tv
end type
type cb_chiudi_commessa from commandbutton within w_det_ord_ven_tv
end type
type cb_corrispondenze from commandbutton within w_det_ord_ven_tv
end type
type cb_sblocca from commandbutton within w_det_ord_ven_tv
end type
type cb_sconti from commandbutton within w_det_ord_ven_tv
end type
end forward

global type w_det_ord_ven_tv from w_cs_xx_principale
integer x = 0
integer y = 48
integer width = 4489
integer height = 1868
string title = "Righe Ordini Clienti"
boolean minbox = false
event ue_posizione ( )
cbx_depositi cbx_depositi
uo_1 uo_1
dw_depositi dw_depositi
cb_varianti cb_varianti
dw_folder dw_folder
dw_det_ord_ven_lista dw_det_ord_ven_lista
dw_det_ord_ven_det_1 dw_det_ord_ven_det_1
dw_documenti dw_documenti
cb_acquisto cb_acquisto
cb_annulla_acq cb_annulla_acq
cb_assegnazione cb_assegnazione
cb_azzera cb_azzera
cb_blocca cb_blocca
cb_c_industriale cb_c_industriale
cb_chiudi_commessa cb_chiudi_commessa
cb_corrispondenze cb_corrispondenze
cb_sblocca cb_sblocca
cb_sconti cb_sconti
end type
global w_det_ord_ven_tv w_det_ord_ven_tv

type variables
boolean ib_esposizione=false
long il_indice_esp
datastore ids_esposizione
uo_condizioni_cliente iuo_condizioni_cliente
uo_gestione_conversioni iuo_gestione_conversioni

end variables

forward prototypes
public subroutine wf_proteggi_colonne (double fd_quan_evasa, double fd_quan_in_evasione, long fl_anno_commessa)
public subroutine wf_tipo_det_ven_lista (datawindow fdw_datawindow, string fs_cod_tipo_det_ven, long fl_riga)
public function integer wf_ricalcola ()
public subroutine wf_depositi (string fs_cod_prodotto)
public subroutine wf_tipo_det_ven (datawindow fdw_datawindow, dwobject flc_ricerca_prodotti, picturebutton flp_prod_view, string fs_cod_tipo_det_ven, string fs_cod_agente_1, string fs_cod_agente_2)
public subroutine wf_tipo_det_ven_det (datawindow fdw_datawindow, dwobject fbo_ricerca_prodotti, picturebutton fpb_listview, string fs_cod_tipo_det_ven, long fl_riga)
end prototypes

public subroutine wf_proteggi_colonne (double fd_quan_evasa, double fd_quan_in_evasione, long fl_anno_commessa);dw_det_ord_ven_lista.setredraw(false)
dw_det_ord_ven_det_1.setredraw(false)
if fd_quan_evasa > 0 or fd_quan_in_evasione > 0 or fl_anno_commessa > 0 or not isnull(fl_anno_commessa) then
	dw_det_ord_ven_lista.object.quan_ordine.background.color=12632256
	dw_det_ord_ven_lista.object.quan_ordine.protect = 1
	
	dw_det_ord_ven_lista.object.cod_prodotto.background.color=12632256
	dw_det_ord_ven_lista.object.cod_prodotto.protect = 1
	
	dw_det_ord_ven_det_1.object.cod_prodotto.background.color=12632256
	dw_det_ord_ven_det_1.object.cod_prodotto.protect = 1
	
	dw_det_ord_ven_det_1.object.quan_ordine.background.color=12632256
	dw_det_ord_ven_det_1.object.quan_ordine.protect = 1

	dw_det_ord_ven_det_1.object.cod_versione.background.color=12632256
	dw_det_ord_ven_det_1.object.cod_versione.protect = 1
	
	// Modifica Daniele 14 Apr 2008
//	dw_det_ord_ven_det_1.object.data_consegna.background.color=12632256
//	dw_det_ord_ven_det_1.object.data_consegna.protect = 1
	// Fine Modifica
	
	
	dw_det_ord_ven_det_1.object.num_confezioni.background.color=12632256
	dw_det_ord_ven_det_1.object.num_confezioni.protect = 1
	
	dw_det_ord_ven_det_1.object.num_pezzi_confezione.background.color=12632256
	dw_det_ord_ven_det_1.object.num_pezzi_confezione.protect = 1
else
	dw_det_ord_ven_lista.object.quan_ordine.background.color=16777215
	dw_det_ord_ven_lista.object.quan_ordine.protect = 0
	
	dw_det_ord_ven_lista.object.cod_prodotto.background.color=16777215
	dw_det_ord_ven_lista.object.cod_prodotto.protect = 0
	
	dw_det_ord_ven_det_1.object.cod_prodotto.background.color=16777215
	dw_det_ord_ven_det_1.object.cod_prodotto.protect = 0
	
	dw_det_ord_ven_det_1.object.quan_ordine.background.color=16777215
	dw_det_ord_ven_det_1.object.quan_ordine.protect = 0

	dw_det_ord_ven_det_1.object.cod_versione.background.color=16777215
	dw_det_ord_ven_det_1.object.cod_versione.protect = 0

	// Modifica Daniele 14 Apr 2008
//	dw_det_ord_ven_det_1.object.data_consegna.background.color=16777215
//	dw_det_ord_ven_det_1.object.data_consegna.protect = 0
	// fine modifica

	dw_det_ord_ven_det_1.object.num_confezioni.background.color=16777215
	dw_det_ord_ven_det_1.object.num_confezioni.protect = 0

	dw_det_ord_ven_det_1.object.num_pezzi_confezione.background.color=16777215
	dw_det_ord_ven_det_1.object.num_pezzi_confezione.protect = 0
	
end if
dw_det_ord_ven_lista.setredraw(true)
dw_det_ord_ven_det_1.setredraw(true)

return
end subroutine

public subroutine wf_tipo_det_ven_lista (datawindow fdw_datawindow, string fs_cod_tipo_det_ven, long fl_riga);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio
double ld_prov_agente_1, ld_prov_agente_2, ld_provv_riga_1, ld_provv_riga_2


setnull(ls_null)
if not isnull(fs_cod_tipo_det_ven) then
	select tab_tipi_det_ven.flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :fs_cod_tipo_det_ven;
	
	if sqlca.sqlcode <> 0 then
		messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
					  exclamation!, ok!)
		return
	end if
	
	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
		ls_modify = "cod_prodotto.protect='0'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
		fdw_datawindow.modify(ls_modify)
	else
		if not isnull(fdw_datawindow.getitemstring(fl_riga,"cod_prodotto")) then
			fdw_datawindow.setitem(fl_riga,"cod_prodotto",ls_null)
		end if
		ls_modify = "cod_prodotto.protect='1'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='12632256'~t"
		fdw_datawindow.modify(ls_modify)
	end if
end if
end subroutine

public function integer wf_ricalcola ();long   ll_i

string ls_parametro, ls_prodotto, ls_misura_mag, ls_misura_ven

dec{4} ld_quan_mag, ld_prezzo_mag, ld_quan_ven, ld_prezzo_ven, ld_fat_conversione, ld_prec_mag, ld_prec_ven

uo_calcola_documento_euro luo_calcolo


select stringa
into   :ls_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'TRD';
		 
if sqlca.sqlcode < 0 then
	messagebox("APICE","Errore in lettura parametro TRD da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_parametro) or (ls_parametro <> "M" and ls_parametro <> "V") then
	return 0
end if

select precisione_prezzo_mag,
		 precisione_prezzo_ven
into   :ld_prec_mag,
		 :ld_prec_ven
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	messagebox("APICE","Errore in lettura precisione prezzi da parametri vendite: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ld_prec_mag) or ld_prec_mag = 0 then
	messagebox("APICE","Impostare la precisione del prezzo di magazzino in PARAMETRI VENDITE")
	return -1
end if

if isnull(ld_prec_ven) or ld_prec_ven = 0 then
	messagebox("APICE","Impostare la precisione del prezzo di vendita in PARAMETRI VENDITE")
	return -1
end if

for ll_i = 1 to dw_det_ord_ven_lista.rowcount()
	
	ls_prodotto = dw_det_ord_ven_lista.getitemstring(ll_i,"cod_prodotto")
	
	if isnull(ls_prodotto) then
		continue
	end if
	
	ls_misura_ven = dw_det_ord_ven_lista.getitemstring(ll_i,"cod_misura")
	
	select cod_misura_mag
	into   :ls_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_prodotto;
			 
	if sqlca.sqlcode < 0 then
		messagebox("APICE","Errore in lettura dati prodotto da anag_prodotti: " + sqlca.sqlerrtext)
		return -1
	end if
	
	if ls_misura_ven = ls_misura_mag then
		continue
	end if
	
	ld_quan_mag = dw_det_ord_ven_lista.getitemnumber(ll_i,"quan_ordine")
	
	ld_prezzo_mag = dw_det_ord_ven_lista.getitemnumber(ll_i,"prezzo_vendita")
	
	ld_quan_ven = dw_det_ord_ven_lista.getitemnumber(ll_i,"quantita_um")
	
	ld_prezzo_ven = dw_det_ord_ven_lista.getitemnumber(ll_i,"prezzo_um")
	
	ld_fat_conversione = dw_det_ord_ven_lista.getitemnumber(ll_i,"fat_conversione_ven")
	
	choose case ls_parametro
			
		case "M"
			
			ld_quan_ven = round(ld_quan_mag * ld_fat_conversione , 4)
			
			ld_prezzo_ven = ld_prezzo_mag / ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_ven,ld_prec_ven,"round")
			
			destroy luo_calcolo
			
		case "V"
			
			ld_quan_mag = round(ld_quan_ven / ld_fat_conversione , 4)
			
			ld_prezzo_mag = ld_prezzo_ven * ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_mag,ld_prec_mag,"round")
			
			destroy luo_calcolo
			
	end choose
	
	dw_det_ord_ven_lista.setitem(ll_i,"quan_ordine",ld_quan_mag)
	
	dw_det_ord_ven_lista.setitem(ll_i,"prezzo_vendita",ld_prezzo_mag)
	
	dw_det_ord_ven_lista.setitem(ll_i,"quantita_um",ld_quan_ven)
	
	dw_det_ord_ven_lista.setitem(ll_i,"prezzo_um",ld_prezzo_ven)
	
next

return 0
end function

public subroutine wf_depositi (string fs_cod_prodotto);string ls_where, ls_errore, ls_chiave[],ls_cod_deposito[],ls_des_deposito
long ll_ret,ll_y
dec{4} ld_quantita, ld_giacenza[], ld_quantita_giacenza[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_riferimento
uo_magazzino luo_magazzino


dw_depositi.reset()
ldt_data_riferimento = datetime(today(),00:00:00)
ls_where = ""
		
for ll_ret = 1 to 14
	ld_quantita_giacenza[ll_ret] = 0
next	

luo_magazzino = CREATE uo_magazzino
ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(fs_cod_prodotto, ldt_data_riferimento, ls_where, ld_quantita_giacenza, ls_errore, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
destroy luo_magazzino	

for ll_y = 1 to upperbound(ls_chiave)
	
	if ld_giacenza_stock[ll_y] = 0 then continue
	
	ll_ret = dw_depositi.insertrow(0)
			
	dw_depositi.setitem(ll_ret, "cod_deposito", ls_chiave[ll_y])
			
	dw_depositi.setitem(ll_ret, "giacenza", ld_giacenza_stock[ll_y])
			
	select des_deposito
	into   :ls_des_deposito
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_deposito = :ls_chiave[ll_y];
			 
	dw_depositi.setitem(ll_ret, "des_deposito", ls_des_deposito)

next
		

end subroutine

public subroutine wf_tipo_det_ven (datawindow fdw_datawindow, dwobject flc_ricerca_prodotti, picturebutton flp_prod_view, string fs_cod_tipo_det_ven, string fs_cod_agente_1, string fs_cod_agente_2);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio
double ld_prov_agente_1, ld_prov_agente_2, ld_provv_riga_1, ld_provv_riga_2


setnull(ls_null)
select tab_tipi_det_ven.flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_tipi_det_ven.cod_tipo_det_ven = :fs_cod_tipo_det_ven;

if sqlca.sqlcode <> 0 then
   messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
              exclamation!, ok!)
   return
end if

if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
   flc_ricerca_prodotti.enabled = true
//   flp_prod_view.enabled = true
   ls_modify = "cod_prodotto.protect='0'~t"
   ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
   ls_modify = ls_modify + "cod_versione.protect='0'~t"
   ls_modify = ls_modify + "cod_versione.background.color='16777215'~t"
   fdw_datawindow.modify(ls_modify)
else
   flc_ricerca_prodotti.enabled = false
//   flp_prod_view.enabled = false
   if not isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_prodotto")) then
      fdw_datawindow.setitem(fdw_datawindow.getrow(),"cod_prodotto",ls_null)
   end if
   ls_modify = "cod_prodotto.protect='1'~t"
   ls_modify = ls_modify + "cod_prodotto.background.color='12632256'~t"
   ls_modify = ls_modify + "cod_versione.protect='1'~t"
   ls_modify = ls_modify + "cod_versione.background.color='12632256'~t"
   fdw_datawindow.modify(ls_modify)
end if

if ls_flag_tipo_det_ven <> "M" and ls_flag_tipo_det_ven <> "C" and &
   ls_flag_tipo_det_ven <> "N" and ls_flag_tipo_det_ven <> "S" then
   if fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_1") > 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(),"provvigione_1",0)
   end if
   if fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_2") > 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(),"provvigione_2",0)
   end if
else
   select anag_agenti.prov_agente
   into   :ld_prov_agente_1
   from   anag_agenti
   where  anag_agenti.cod_azienda = :s_cs_xx.cod_azienda and 
          anag_agenti.cod_agente = :fs_cod_agente_1;

	ld_provv_riga_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_1")
   if sqlca.sqlcode = 0 and ld_provv_riga_1 = 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_1", ld_prov_agente_1)
	elseif sqlca.sqlcode <> 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_1", 0)
	end if

   select anag_agenti.prov_agente
   into   :ld_prov_agente_2
   from   anag_agenti
   where  anag_agenti.cod_azienda = :s_cs_xx.cod_azienda and 
          anag_agenti.cod_agente = :fs_cod_agente_2;

	ld_provv_riga_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_2")
   if sqlca.sqlcode = 0 and ld_provv_riga_2 = 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_2", ld_prov_agente_2)
	elseif sqlca.sqlcode <> 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_2", 0)
   end if
end if
end subroutine

public subroutine wf_tipo_det_ven_det (datawindow fdw_datawindow, dwobject fbo_ricerca_prodotti, picturebutton fpb_listview, string fs_cod_tipo_det_ven, long fl_riga);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio

setnull(ls_null)
if not isnull(fs_cod_tipo_det_ven) then
	select tab_tipi_det_ven.flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :fs_cod_tipo_det_ven;
	
	if sqlca.sqlcode <> 0 then
		messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
					  exclamation!, ok!)
		return
	end if
	
	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
		fbo_ricerca_prodotti.enabled = true
//		fpb_listview.enabled = true
		ls_modify = "cod_prodotto.protect='0'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_versione.protect='0'~t"
		ls_modify = ls_modify + "cod_versione.background.color='16777215'~t"
		fdw_datawindow.modify(ls_modify)
	else
		fbo_ricerca_prodotti.enabled = false
//		fpb_listview.enabled = false
		if not isnull(fdw_datawindow.getitemstring(fl_riga,"cod_prodotto")) then
			fdw_datawindow.setitem(fl_riga,"cod_prodotto",ls_null)
		end if
		ls_modify = "cod_prodotto.protect='1'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_versione.protect='1'~t"
		ls_modify = ls_modify + "cod_versione.background.color='12632256'~t"
		fdw_datawindow.modify(ls_modify)
	end if
end if
end subroutine

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco<>'S'")
f_po_loaddddw_dw(dw_det_ord_ven_lista, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco<>'S'")
f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_tipo_causa_sospensione", &
                 sqlca, &
                 "tab_tipi_cause_sospensione", &
                 "cod_tipo_causa_sospensione", &
                 "des_tipo_causa_sospensione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;string ls_flag
windowobject lw_oggetti[]

dw_det_ord_ven_lista.set_dw_key("cod_azienda")
dw_det_ord_ven_lista.set_dw_key("anno_registrazione")
dw_det_ord_ven_lista.set_dw_key("num_registrazione")

dw_det_ord_ven_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)
dw_det_ord_ven_det_1.set_dw_options(sqlca, &
                                    dw_det_ord_ven_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

lw_oggetti[1] = dw_depositi
lw_oggetti[2] = dw_det_ord_ven_lista
lw_oggetti[3] = uo_1
lw_oggetti[4] = cbx_depositi
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])

lw_oggetti[1] = dw_det_ord_ven_det_1
lw_oggetti[2] = cb_varianti
lw_oggetti[3] = cb_chiudi_commessa
//Donato 24-09-2008 modifica per gestione selezione del dettaglio acquisto
lw_oggetti[4] = cb_acquisto
lw_oggetti[5] = cb_annulla_acq
lw_oggetti[6] = dw_documenti
//---------------------------------------------------------------------------
ls_flag = "N"
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_documenti.uof_set_management("ORDVEN", dw_det_ord_ven_lista)
dw_documenti.settransobject(sqlca)
dw_documenti.object.p_collegato.FileName = s_cs_xx.volume + s_cs_xx.risorse + + "menu\indietro.png"
dw_documenti.uof_enabled_delete_blob()

dw_depositi.visible = false

iuo_dw_main = dw_det_ord_ven_lista
cb_c_industriale.enabled = false
cb_blocca.enabled = false
cb_sblocca.enabled = false
cb_assegnazione.enabled = false
cb_azzera.enabled = false
cb_corrispondenze.enabled = false

ls_flag = "N"
select flag
into   :ls_flag
from   parametri_azienda	
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and
		 parametri_azienda.cod_parametro = 'VPR';
if ls_flag = 'N' then
	dw_det_ord_ven_det_1.object.provvigione_1.visible = 0
	dw_det_ord_ven_det_1.object.provvigione_1_t.visible = 0
	dw_det_ord_ven_det_1.object.provvigione_2.visible = 0
	dw_det_ord_ven_det_1.object.provvigione_2_t.visible = 0
end if

cb_varianti.picturename = s_cs_xx.volume + s_cs_xx.risorse + "varianti_04a.bmp"
cb_varianti.disabledname = s_cs_xx.volume + s_cs_xx.risorse + "varianti_04a.bmp"

try
	dw_det_ord_ven_det_1.object.p_commessa.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
catch (runtimeerror err)
end try

dw_det_ord_ven_lista.is_cod_parametro_blocco_prodotto = 'POC'
dw_det_ord_ven_det_1.is_cod_parametro_blocco_prodotto = 'POC'

uo_1.hide()
postevent("ue_posizione")
dw_det_ord_ven_lista.postevent("ue_postopen")
return 0
end event

on w_det_ord_ven_tv.create
int iCurrent
call super::create
this.cbx_depositi=create cbx_depositi
this.uo_1=create uo_1
this.dw_depositi=create dw_depositi
this.cb_varianti=create cb_varianti
this.dw_folder=create dw_folder
this.dw_det_ord_ven_lista=create dw_det_ord_ven_lista
this.dw_det_ord_ven_det_1=create dw_det_ord_ven_det_1
this.dw_documenti=create dw_documenti
this.cb_acquisto=create cb_acquisto
this.cb_annulla_acq=create cb_annulla_acq
this.cb_assegnazione=create cb_assegnazione
this.cb_azzera=create cb_azzera
this.cb_blocca=create cb_blocca
this.cb_c_industriale=create cb_c_industriale
this.cb_chiudi_commessa=create cb_chiudi_commessa
this.cb_corrispondenze=create cb_corrispondenze
this.cb_sblocca=create cb_sblocca
this.cb_sconti=create cb_sconti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_depositi
this.Control[iCurrent+2]=this.uo_1
this.Control[iCurrent+3]=this.dw_depositi
this.Control[iCurrent+4]=this.cb_varianti
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_det_ord_ven_lista
this.Control[iCurrent+7]=this.dw_det_ord_ven_det_1
this.Control[iCurrent+8]=this.dw_documenti
this.Control[iCurrent+9]=this.cb_acquisto
this.Control[iCurrent+10]=this.cb_annulla_acq
this.Control[iCurrent+11]=this.cb_assegnazione
this.Control[iCurrent+12]=this.cb_azzera
this.Control[iCurrent+13]=this.cb_blocca
this.Control[iCurrent+14]=this.cb_c_industriale
this.Control[iCurrent+15]=this.cb_chiudi_commessa
this.Control[iCurrent+16]=this.cb_corrispondenze
this.Control[iCurrent+17]=this.cb_sblocca
this.Control[iCurrent+18]=this.cb_sconti
end on

on w_det_ord_ven_tv.destroy
call super::destroy
destroy(this.cbx_depositi)
destroy(this.uo_1)
destroy(this.dw_depositi)
destroy(this.cb_varianti)
destroy(this.dw_folder)
destroy(this.dw_det_ord_ven_lista)
destroy(this.dw_det_ord_ven_det_1)
destroy(this.dw_documenti)
destroy(this.cb_acquisto)
destroy(this.cb_annulla_acq)
destroy(this.cb_assegnazione)
destroy(this.cb_azzera)
destroy(this.cb_blocca)
destroy(this.cb_c_industriale)
destroy(this.cb_chiudi_commessa)
destroy(this.cb_corrispondenze)
destroy(this.cb_sblocca)
destroy(this.cb_sconti)
end on

event pc_delete;call super::pc_delete;long ll_i

for ll_i = 1 to dw_det_ord_ven_lista.deletedcount()
	if dw_det_ord_ven_lista.getitemstring(ll_i, "flag_evasione", delete!, true) <> "A" then
		messagebox("Attenzione", "Ordine non cancellabile! Ha già subito un'evasione.", &
					  exclamation!, ok!)
		dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if dw_det_ord_ven_lista.getitemstring(ll_i, "flag_blocco", delete!, true) = "S" then
		messagebox("Attenzione", "Ordine non cancellabile! E' stato bloccato.", &
					  exclamation!, ok!)
		dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if dw_det_ord_ven_lista.getitemnumber(ll_i, "quan_in_evasione", delete!, true) > 0 then
		messagebox("Attenzione", "Ordine non cancellabile! Quantità in evasione > di zero.", &
					  exclamation!, ok!)
		dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if not isnull(dw_det_ord_ven_lista.getitemnumber(ll_i, "anno_commessa", delete!, true)) then
		messagebox("Attenzione", "Ordine non cancellabile! Dettaglio collegato alle commesse.", &
					  exclamation!, ok!)
		dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
next

cb_c_industriale.enabled = false
cb_blocca.enabled = false
cb_sblocca.enabled = false
cb_assegnazione.enabled = false
cb_azzera.enabled = false
cb_corrispondenze.enabled = false
end event

event pc_modify;call super::pc_modify;if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "S" then
   messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_new;call super::pc_new;if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
   dw_det_ord_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if
end event

event close;call super::close;destroy iuo_condizioni_cliente
destroy iuo_gestione_conversioni
end event

event open;call super::open;iuo_condizioni_cliente = create uo_condizioni_cliente
iuo_gestione_conversioni = create uo_gestione_conversioni
end event

type cbx_depositi from checkbox within w_det_ord_ven_tv
integer x = 91
integer y = 1232
integer width = 311
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Depositi"
end type

event clicked;if checked then
	dw_det_ord_ven_lista.height = 540
	dw_depositi.visible = true
else
	dw_det_ord_ven_lista.height = 1080
	dw_depositi.visible = false
end if

dw_det_ord_ven_lista.postevent("ue_giacenze_depositi")
end event

type uo_1 from uo_situazione_prodotto within w_det_ord_ven_tv
integer x = 46
integer y = 1368
integer width = 4334
integer height = 256
integer taborder = 40
boolean border = false
end type

on uo_1.destroy
call uo_situazione_prodotto::destroy
end on

type dw_depositi from datawindow within w_det_ord_ven_tv
integer x = 69
integer y = 680
integer width = 3429
integer height = 540
integer taborder = 21
boolean bringtotop = true
string title = "none"
string dataobject = "d_det_ord_ven_giacenze_deposito"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_varianti from picturebutton within w_det_ord_ven_tv
integer x = 2245
integer y = 240
integer width = 73
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "\cs_70\framework\risorse\varianti_04a.bmp"
string disabledname = "\cs_70\framework\risorse\varianti_04a.bmp"
alignment htextalign = left!
end type

event clicked;long ll_numero

select parametri.numero  
into   :ll_numero  
from   parametri  
where (parametri.flag_parametro = 'N' ) and
      (parametri.cod_parametro = 'GVA' )   ;
if ll_numero > 0 and dw_det_ord_ven_lista.getrow() > 0 then
	s_cs_xx.parametri.parametro_dw_1 = dw_det_ord_ven_lista
	window_open(w_varianti_ordini,-1)
end if
end event

type dw_folder from u_folder within w_det_ord_ven_tv
integer x = 23
integer y = 20
integer width = 4393
integer height = 1620
integer taborder = 50
end type

event type long po_tabclicked(unsignedlong wparam, long lparam);call super::po_tabclicked;if isnull(dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"anno_commessa")) then cb_chiudi_commessa.visible = false
return 0

end event

type dw_det_ord_ven_lista from uo_cs_xx_dw within w_det_ord_ven_tv
event ue_key pbm_dwnkey
event ue_postopen ( )
event post_rowfocuschanged ( )
event ue_giacenze_depositi ( )
integer x = 69
integer y = 140
integer width = 4302
integer height = 1080
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_det_ord_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	       ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume
datetime ldt_data_registrazione
s_report_stato_magazzino ls_report_stato_magazzino

// pressione tasto SHIFT-F8: procedo con report stato magazzino
if key = keyF8!  and keyflags = 1 then
	ls_report_stato_magazzino.cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
	
	if not isnull(ls_report_stato_magazzino.cod_prodotto) and len(ls_report_stato_magazzino.cod_prodotto) > 0 then
		message.powerobjectparm = ls_report_stato_magazzino
		window_open(w_report_stato_magazzino, -1)
	end if
end if


choose case this.getcolumnname()

	case "sconto_1"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
		case "data_consegna"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
			
	case "prezzo_vendita"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		else
			if (key = keyF1! or key = keyF2! or key = keyF3! or key = keyF4! or key = keyF5!) and keyflags = 1 then
				setpointer(hourglass!)
				ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
				ls_cod_valuta = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
				ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
				ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
				ld_cambio_ven = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
				ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
				ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
				ld_quantita = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine")
				choose case key
					case keyF1!
						ls_listino = 'LI1'
					case keyF2!
						ls_listino = 'LI2'
					case keyF3!
						ls_listino = 'LI3'
					case keyF4!
						ls_listino = 'LI4'
					case keyF5!
						ls_listino = 'LI5'
					case else
						return
				end choose
				if ls_listino <> "LI5" then
					select stringa
					into  :ls_stringa
					from  parametri_azienda
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_parametro = :ls_listino ;
					if sqlca.sqlcode = 0 then
						if mid(f_flag_controllo(),3,1) = "S" then
							iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
							iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_stringa
							iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
							iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
							if mid(f_flag_controllo(),7,1) = "S" then
								iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
							else
								iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
							end if
							iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
							iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
							iuo_condizioni_cliente.str_parametri.dim_1 = 0
							iuo_condizioni_cliente.str_parametri.dim_2 = 0
							iuo_condizioni_cliente.str_parametri.valore = 0
							iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
							iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
							iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
							iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
							iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
							iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
							iuo_condizioni_cliente.ib_prezzi = false
							for ll_i = 1 to 10
								if i_colname = "sconto_" + string(ll_i) then
									iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
								else
									iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
								end if
							next
							iuo_condizioni_cliente.wf_condizioni_cliente()
						end if
					end if			
					setpointer(arrow!)
				else
					select prezzo_acquisto
					into   :ld_prezzo_acquisto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_prodotto = :ls_cod_prodotto;
					dw_det_ord_ven_lista.setitem(i_rownbr, "prezzo_vendita", ld_prezzo_acquisto)
				end if					 
				setpointer(arrow!)
			end if
		end if
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_lista,"cod_prodotto")
		end if
	case "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_lista,"cod_prodotto")
		end if
		

end choose


end event

event ue_postopen();if this.getrow() > 0 then
	datawindow ld_datawindow
	
	uo_gestione_conversioni iuo_oggetto
	iuo_oggetto = create uo_gestione_conversioni
	ld_datawindow  = dw_det_ord_ven_det_1
	iuo_oggetto.uof_visualizza_um(ld_datawindow, "ord_ven")
	if ib_stato_nuovo or ib_stato_modifica then 	iuo_gestione_conversioni.uof_blocca_colonne(ld_datawindow,"ord_ven")
	destroy iuo_oggetto
	
	if isnull(getitemnumber(getrow(),"anno_commessa")) then cb_chiudi_commessa.visible = false
end if
end event

event post_rowfocuschanged();datawindow ld_datawindow

uo_gestione_conversioni iuo_oggetto
iuo_oggetto = create uo_gestione_conversioni
ld_datawindow  = dw_det_ord_ven_det_1
iuo_oggetto.uof_visualizza_um(ld_datawindow, "ord_ven")
if ib_stato_modifica or ib_stato_nuovo then
	iuo_oggetto.uof_blocca_colonne(ld_datawindow, "ord_ven")
end if
destroy iuo_oggetto

end event

event ue_giacenze_depositi();string ls_cod_prodotto

if cbx_depositi.checked then
	ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_prodotto")
	
	if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
		wf_depositi(ls_cod_prodotto)
	end if
end if
end event

event pcd_new;call super::pcd_new;setitem(getrow(),"flag_stampa_settimana","N")

if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, ls_flag_tipo_det_ven, &
          ls_modify, ls_null, ls_cod_agente_1, ls_cod_agente_2, ls_cod_cliente, &
          ls_cod_iva
   long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
   datetime ldt_data_consegna, ldt_data_esenzione_iva, ldt_data_registrazione
	double ld_quan_proposta

	dw_det_ord_ven_lista.object.cod_tipo_det_ven.protect = 0
   setnull(ls_null)

   ll_anno_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
   ll_num_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
   ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_consegna")

   select max(det_ord_ven.prog_riga_ord_ven)
   into   :ll_prog_riga_ord_ven
   from   det_ord_ven
   where  det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
          det_ord_ven.anno_registrazione = :ll_anno_registrazione and
          det_ord_ven.num_registrazione = :ll_num_registrazione;

   if isnull(ll_prog_riga_ord_ven) then
      dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven", 10)
   else
      dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven", ll_prog_riga_ord_ven + 10)
   end if

   ls_cod_tipo_ord_ven = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_ord_ven")

   select tab_tipi_ord_ven.cod_tipo_det_ven
   into   :ls_cod_tipo_det_ven
   from   tab_tipi_ord_ven
   where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
   
   if sqlca.sqlcode = 0 then
      dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
      if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
         select tab_tipi_det_ven.cod_iva  
         into   :ls_cod_iva  
         from   tab_tipi_det_ven  
         where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
         end if
      end if

   else
      ls_cod_tipo_det_ven = ls_null
   end if

//-------------------------------------- Modifica Nicola ---------------------------------------------------------------

	select quan_default
	  into :ld_quan_proposta
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda 
	   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode < 0 then
		messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_ord_ven " + sqlca.sqlerrtext)
		return 0
	end if
	
	if isnull(ld_quan_proposta) then ld_quan_proposta = 0

//--------------------------------------- Fine Modifica ----------------------------------------------------------------

   dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "quan_ordine", ld_quan_proposta)
   dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_lista.getrow(), "data_consegna", ldt_data_consegna)
   
   if not isnull(ls_cod_cliente) then
      select anag_clienti.cod_iva,
             anag_clienti.data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_clienti
      where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_clienti.cod_cliente = :ls_cod_cliente;
   end if
   
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and &
         (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
         this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
      end if
   end if
   
   ls_modify = "cod_tipo_det_ven.protect='0'~t"
   dw_det_ord_ven_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_ven.background.color='16777215'~t"
   dw_det_ord_ven_det_1.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_ven) then
      dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
      ld_datawindow = dw_det_ord_ven_det_1
      ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
      ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
      wf_tipo_det_ven(ld_datawindow, dw_det_ord_ven_det_1.object.b_ricerca_prodotto, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)
      wf_tipo_det_ven_det(ld_datawindow, dw_det_ord_ven_det_1.object.b_ricerca_prodotto, lp_prod_view, ls_cod_tipo_det_ven, getrow())
      ld_datawindow       = dw_det_ord_ven_lista
      wf_tipo_det_ven_lista(ld_datawindow, ls_cod_tipo_det_ven, getrow())
		
   else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_misura.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_misura.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "fat_conversione_ven.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "fat_conversione_ven.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "quan_ordine.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "quan_ordine.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "prezzo_vendita.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "prezzo_vendita.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_1.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_1.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_2.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_2.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_iva.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_iva.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_1.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_1.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_2.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_2.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "data_consegna.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "data_consegna.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_centro_costo.protect='1'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_centro_costo.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
   end if

   cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_blocca.enabled = false
	cb_sblocca.enabled = false
	cb_assegnazione.enabled = false
	cb_azzera.enabled = false
	cb_varianti.enabled = false
	cb_corrispondenze.enabled = false
	
	//Donato 24-09-2008 Modifica per selezione dettaglio dell'acquisto al fornitore
	cb_acquisto.enabled = true
	cb_annulla_acq.enabled = true
	//------------------------------------------------------------------------------
	
	dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "flag_st_note_det", 'I')
	dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "flag_doc_suc_det", 'I')	
	
end if
return 0
end event

event pcd_view;call super::pcd_view;if i_extendmode then
  dw_det_ord_ven_det_1.object.b_ricerca_prodotto.enabled = false
   cb_sconti.enabled = false
	cb_blocca.enabled = true
	cb_sblocca.enabled = true
	cb_varianti.enabled = true
	
	//Donato 24-09-2008 modifica per gestione selezione dettaglio acquisto
	cb_acquisto.enabled = false
	cb_annulla_acq.enabled = false
	//------------------------------------------------------------------------

	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
			cb_c_industriale.enabled=true
			cb_corrispondenze.enabled = true
		end if
	else
		cb_c_industriale.enabled=false
		cb_corrispondenze.enabled = false
	end if

	cb_assegnazione.enabled = true
	cb_azzera.enabled = true
	
end if
return 0
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_cliente, ls_cod_iva, ls_cod_tipo_det_ven, ls_modify, ls_cod_prodotto, ls_cod_misura_mag
	long ll_anno_commessa
   datetime ldt_data_registrazione, ldt_data_esenzione_iva

   ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
   ldt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")
	
	if this.getrow() > 0 then
		
		dw_documenti.uof_retrieve_blob(i_CursorRow)
		
		ll_anno_commessa = this.getitemnumber(i_CursorRow, "anno_commessa")
		if isnull(ll_anno_commessa) then
			cb_chiudi_commessa.enabled = false
		else
			cb_chiudi_commessa.enabled = true
		end if
		ls_cod_prodotto = this.getitemstring(currentrow,"cod_prodotto")		
		
		uo_1.uof_aggiorna(ls_cod_prodotto)

		f_PO_LoadDDDW_DW(dw_det_ord_ven_det_1,"cod_versione",sqlca,&
   	              	  "distinta_padri","cod_versione","des_versione",&
	  			           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(i_CursorRow,"cod_prodotto") + "'")
	
		if not isnull(ls_cod_cliente) then
			select anag_clienti.cod_iva,
					 anag_clienti.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_clienti.cod_cliente = :ls_cod_cliente;
		end if
	
		if sqlca.sqlcode = 0 then
			if ls_cod_iva <> "" and ldt_data_esenzione_iva <= ldt_data_registrazione then
				f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
			else
				f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			end if
		end if
	
		ld_datawindow  = dw_det_ord_ven_det_1
		if ib_stato_modifica or ib_stato_nuovo then
			ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(i_CursorRow, "cod_tipo_det_ven")
			ld_datawindow       = dw_det_ord_ven_det_1
			wf_tipo_det_ven_det(ld_datawindow, dw_det_ord_ven_det_1.object.b_ricerca_prodotto, lp_prod_view, ls_cod_tipo_det_ven,i_CursorRow)
			ld_datawindow       = dw_det_ord_ven_lista
			wf_tipo_det_ven_lista(ld_datawindow, ls_cod_tipo_det_ven,i_CursorRow)
			if getrow() > 0 then wf_proteggi_colonne(getitemnumber(i_CursorRow,"quan_evasa"), getitemnumber(i_CursorRow,"quan_in_evasione"), getitemnumber(i_CursorRow,"anno_commessa"))
		end if
		postevent("post_rowfocuschanged")
	end if

   if ib_stato_nuovo then
      ls_modify = "cod_tipo_det_ven.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_det_ven.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
   end if
	
end if

postevent("ue_giacenze_depositi")
return 0
end event

event updatestart;call super::updatestart;if i_extendmode then
   long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_riga_ord_ven, ll_i2, &
		  ll_i3, ll_controllo, ll_anno_commessa, ll_riga_per_peso, ll_null

   dec{4} ld_sconto_testata, ld_quan_ordine_old, ld_quan_ordine, ld_quan_evasa, ld_quan_in_evasione,ld_quan_raggruppo, ld_peso_netto_unitario

   string ls_cod_pagamento, ls_cod_valuta, ls_tabella, ls_quan_documento, & 
			 ls_tot_val_documento, ls_nome_prog, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
			 ls_cod_prodotto_old, ls_cod_prodotto, ls_aspetto_beni, ls_des_imballo,ls_cod_prodotto_raggruppato, ls_flag_evaso, ls_errore

	dec{4} ld_valore_riga, ld_imponibile_ordine, ld_valore_ordine_origine
	
	uo_funzioni_1 luo_funzioni_1
   
	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	ld_sconto_testata = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "sconto")
	ls_cod_pagamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_pagamento")
	ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
	ll_prog_riga_ord_ven = getitemnumber(getrow(),"prog_riga_ord_ven")
	ls_aspetto_beni = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "aspetto_beni")
	
	ls_tabella = "tes_ord_ven"
	ls_quan_documento = "quan_ordine"
	ls_tot_val_documento = "tot_val_ordine"
	
	dw_det_ord_ven_lista.change_dw_current()
  

	// ***************** Fine modifica 15/06/2001 *******************************
	setnull(ll_null)
	for ll_i2 = 1 to this.rowcount()
		
		//per eurocablaggi (modifica manuale riferimento riga offerta)
		if getitemnumber(ll_i2, "anno_registrazione_off")<=0 then setitem(ll_i2, "anno_registrazione_off", ll_null)
		if getitemnumber(ll_i2, "num_registrazione_off")<=0 then setitem(ll_i2, "num_registrazione_off", ll_null)
		if getitemnumber(ll_i2, "prog_riga_off_ven")<=0 then setitem(ll_i2, "prog_riga_off_ven", ll_null)
		
		
		ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
		
		select des_imballo
		into  :ls_des_imballo
		from  tab_imballi
		where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_imballo in (select cod_imballo 
								 from	  anag_prodotti
								 where  cod_azienda = :s_cs_xx.cod_azienda and
										  cod_prodotto = :ls_cod_prodotto);

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Errore Durante l'Estrazione Descrizione Imballo. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if

		if (pos(ls_aspetto_beni, ls_des_imballo) = 0 or isnull(ls_aspetto_beni) or ls_aspetto_beni = "") and &
			ls_des_imballo <> "" and not isnull(ls_des_imballo) then
			if len(ls_aspetto_beni) > 0 then
				ls_aspetto_beni = ls_aspetto_beni + " - " + ls_des_imballo
			else
				ls_aspetto_beni = ls_des_imballo
			end if
		end if
		
		ls_cod_tipo_det_ven = this.getitemstring(ll_i2, "cod_tipo_det_ven")

		select flag_tipo_det_ven 
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if

		ld_quan_evasa = getitemnumber(ll_i2,"quan_evasa")
		ld_quan_in_evasione = getitemnumber(ll_i2,"quan_in_evasione")
		ll_anno_commessa = getitemnumber(ll_i2, "anno_commessa")

		if ls_flag_tipo_det_ven = "M"  and (ll_anno_commessa = 0 or isnull(ll_anno_commessa)) then
			if  ld_quan_evasa = 0 and ld_quan_in_evasione = 0 then
				ll_prog_riga_ord_ven = this.getitemnumber(ll_i2, "prog_riga_ord_ven")

				select prog_riga_ord_ven
				into :ll_controllo
				from det_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and 
						anno_registrazione = :ll_anno_registrazione and 
						num_registrazione  = :ll_num_registrazione and 
						prog_riga_ord_ven  = :ll_prog_riga_ord_ven;

				if sqlca.sqlcode = 0 then
					ls_cod_prodotto_old = this.getitemstring(ll_i2, "cod_prodotto", primary!, true)
					ld_quan_ordine_old = this.getitemnumber(ll_i2, "quan_ordine", primary!, true)

					update anag_prodotti  
						set quan_impegnata = quan_impegnata - :ld_quan_ordine_old  
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_old;

					if sqlca.sqlcode = -1 then
						messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto_old+". Dettaglio " + sqlca.sqlerrtext)
						return 1
					end if
				
					// enme 08/1/2006 gestione prodotto raggruppato
					setnull(ls_cod_prodotto_raggruppato)
				
					select cod_prodotto_raggruppato
					into   :ls_cod_prodotto_raggruppato
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto_old;
						 
					if not isnull(ls_cod_prodotto_raggruppato) then

						ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_old, ld_quan_ordine_old, "M")

						update anag_prodotti  
							set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
						 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
								 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
	
						if sqlca.sqlcode = -1 then
							messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto_old+". Dettaglio " + sqlca.sqlerrtext)
							return 1
						end if
					end if
				
				end if

				ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
				ld_quan_ordine = this.getitemnumber(ll_i2, "quan_ordine")

				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_ordine  
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
					return 1
				end if
			
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
			
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
					 
				if not isnull(ls_cod_prodotto_raggruppato) then

					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")

					update anag_prodotti  
						set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
	
					if sqlca.sqlcode = -1 then
						messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
						return 1
					end if
				end if
			else
				
				//-claudia se parzialmente evasa
				select flag_evasione
				into :ls_flag_evaso
				from det_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and 
						anno_registrazione = :ll_anno_registrazione and 
						num_registrazione  = :ll_num_registrazione and 
						prog_riga_ord_ven  = :ll_prog_riga_ord_ven;

				if sqlca.sqlcode = 0 then
					if  ls_flag_evaso <>'E' then
						ll_prog_riga_ord_ven = this.getitemnumber(ll_i2, "prog_riga_ord_ven")

						select prog_riga_ord_ven
						into :ll_controllo
						from det_ord_ven
						where cod_azienda = :s_cs_xx.cod_azienda and 
								anno_registrazione = :ll_anno_registrazione and 
								num_registrazione  = :ll_num_registrazione and 
								prog_riga_ord_ven  = :ll_prog_riga_ord_ven;
		
						if sqlca.sqlcode = 0 then
							ls_cod_prodotto_old = this.getitemstring(ll_i2, "cod_prodotto", primary!, true)
							ld_quan_ordine_old = this.getitemnumber(ll_i2, "quan_ordine", primary!, true)
			
							update anag_prodotti  
								set quan_impegnata = quan_impegnata - :ld_quan_ordine_old  
							 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
									 anag_prodotti.cod_prodotto = :ls_cod_prodotto_old;
			
							if sqlca.sqlcode = -1 then
								messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto_old+". Dettaglio " + sqlca.sqlerrtext)
								return 1
							end if
						
							// enme 08/1/2006 gestione prodotto raggruppato
							setnull(ls_cod_prodotto_raggruppato)
							
							select cod_prodotto_raggruppato
							into   :ls_cod_prodotto_raggruppato
							from   anag_prodotti
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_prodotto_old;
								 
							if not isnull(ls_cod_prodotto_raggruppato) then
							
								ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_old, ld_quan_ordine_old, "M")
							
								update anag_prodotti  
									set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
								 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
										 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			
								if sqlca.sqlcode = -1 then
									messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto_old+". Dettaglio " + sqlca.sqlerrtext)
									return 1
								end if
							end if
						
						end if
		
						ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
						ld_quan_ordine = this.getitemnumber(ll_i2, "quan_ordine")
		
						update anag_prodotti  
							set quan_impegnata = quan_impegnata + :ld_quan_ordine  
						 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
								 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
						if sqlca.sqlcode = -1 then
							messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
							return 1
						end if
					
						// enme 08/1/2006 gestione prodotto raggruppato
						setnull(ls_cod_prodotto_raggruppato)
					
						select cod_prodotto_raggruppato
						into   :ls_cod_prodotto_raggruppato
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
							 
						if not isnull(ls_cod_prodotto_raggruppato) then
						
							ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
						
							update anag_prodotti  
								set quan_impegnata = quan_impegnata + :ld_quan_raggruppo  
							 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
									 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			
							if sqlca.sqlcode = -1 then
								messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
								return 1
							end if
						end if
							
					end if
				end if
			end if
		end if
	
		// calcolo imponibile riga per controllo fido		
		ld_valore_riga = f_calcola_riga_euro ( getitemstring(ll_i2,"cod_tipo_det_ven"), &
											  getitemnumber(ll_i2,"quan_ordine"), &
											  getitemnumber(ll_i2,"prezzo_vendita"), &
											  i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta"), &
											  i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "cambio_ven"), &
											  getitemnumber(ll_i2,"sconto_1"), &
											  getitemnumber(ll_i2,"sconto_2"), &
											  getitemnumber(ll_i2,"sconto_3"), &
											  getitemnumber(ll_i2,"sconto_4"), &
											  getitemnumber(ll_i2,"sconto_5"), &
											  getitemnumber(ll_i2,"sconto_6"), &
											  getitemnumber(ll_i2,"sconto_7"), &
											  getitemnumber(ll_i2,"sconto_8"), &
											  getitemnumber(ll_i2,"sconto_9"), &
											  getitemnumber(ll_i2,"sconto_10") )
										  
		if isnull( ld_valore_riga ) then ld_valore_riga = 0
	
		ld_imponibile_ordine += ld_valore_riga
		//---------------------------------------------
		
	next

	for ll_i3 = 1 to deletedcount()
		ls_cod_tipo_det_ven = this.getitemstring(ll_i3, "cod_tipo_det_ven", delete!, true)

		select tab_tipi_det_ven.flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if

   		if ls_flag_tipo_det_ven = "M" then
			ls_cod_prodotto = this.getitemstring(ll_i3, "cod_prodotto", delete!, true)
			ld_quan_ordine = this.getitemnumber(ll_i3, "quan_ordine", delete!, true)
			
			update anag_prodotti  
				set quan_impegnata = quan_impegnata - :ld_quan_ordine  
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino. Dettaglio " + sqlca.sqlerrtext)
				return 1
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
	
				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino. Dettaglio " + sqlca.sqlerrtext)
					return 1
				end if
			end if
		end if
	next

	update tes_ord_ven
	set	 aspetto_beni = :ls_aspetto_beni
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode = -1 then
		messagebox("Attenzione", "Errore Durante l'Aggiornamento Imballo Ordine. Dettaglio " + sqlca.sqlerrtext)
		return 1
	end if

	ls_tabella = "det_ord_ven_stat"
	ls_nome_prog = "prog_riga_ord_ven"
	
	for ll_i = 1 to this.deletedcount()
		ll_prog_riga_ord_ven = this.getitemnumber(ll_i, "prog_riga_ord_ven", delete!, true)		
		if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, ll_prog_riga_ord_ven) = -1 then
			return 1
		end if

		delete varianti_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;

		if sqlca.sqlcode < 0 then
			messagebox("Apice","Errore durante cancellazione varianti ordini. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if
		
		delete integrazioni_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;

		if sqlca.sqlcode < 0 then
			messagebox("Apice","Errore durante cancellazione varianti ordini. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_ord_ven_note		
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			messagebox("Apice","Errore durante cancellazione note dettaglio ordine",stopsign!)
			return 1
		end if
		
		delete det_ord_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione
		and    prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			messagebox("Apice","Errore durante cancellazione corrispondenze dettaglio ordine",stopsign!)
			return 1
		end if
		
		//------------------------------------------------------------------------------------------------------
		//pulizia tabella evasione da dati eventuali per evitare errore di FK
		delete from evas_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione  = :ll_num_registrazione and
					prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore durante pulizia tabella evas_ord_ven dalla riga ordine: "+sqlca.sqlerrtext
			g_mb.error(ls_errore)
			return 1
		end if
		//------------------------------------------------------------------------------------------------------
		
   next
end if

wf_ricalcola()

//inserire qui il codice per il calcolo dell'esposizione
uo_fido_cliente l_uo_fido_cliente
long ll_return
string ls_cod_cliente, ls_messaggio

l_uo_fido_cliente = create uo_fido_cliente

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

select 	sum(imponibile_iva)
into   	:ld_valore_ordine_origine
from   	det_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
		
if isnull(ld_valore_ordine_origine) then ld_valore_ordine_origine = 0

l_uo_fido_cliente.id_importo = ld_imponibile_ordine - ld_valore_ordine_origine
//l_uo_fido_cliente.id_importo = getitemnumber(getrow(),"prezzo_vendita")

l_uo_fido_cliente.is_flag_autorizza_sblocco = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "flag_aut_sblocco_fido")

ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cod_cliente,datetime(today(),00:00:00),ls_messaggio)
destroy l_uo_fido_cliente

if ll_return = -1 then
	g_mb.messagebox("APICE","Errore in verifica esposizione cliente.~n" + ls_messaggio,exclamation!)
	return -1
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi
if ll_return = 2 then
	//blocca perchè non sono autorizzato
	return 1
end if
//fine modifica ---------------------------
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
next

end on

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_rag_soc_1
long ll_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente ;
if sqlca.sqlcode = 0 then
	parent.title = "Ordine " + &
						string(ll_anno_registrazione,"####") + "/" + &
						string(ll_num_registrazione) + "  Cliente: " + ls_rag_soc_1
end if
ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_ven, ls_modify, ls_cod_agente_1, ls_cod_agente_2

	dw_det_ord_ven_lista.object.cod_tipo_det_ven.protect = 1
	dw_det_ord_ven_lista.object.cod_tipo_det_ven.background.color='12632256'

   ls_modify = "cod_tipo_det_ven.protect='1'~t"
   dw_det_ord_ven_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_ven.background.color='12632256'~t"
   dw_det_ord_ven_det_1.modify(ls_modify)
   ls_cod_tipo_det_ven = dw_det_ord_ven_det_1.getitemstring(dw_det_ord_ven_det_1.getrow(), "cod_tipo_det_ven")
   ld_datawindow = dw_det_ord_ven_det_1
   ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   wf_tipo_det_ven(ld_datawindow, dw_det_ord_ven_det_1.object.b_ricerca_prodotto, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)

   cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_blocca.enabled = false
	cb_sblocca.enabled = false
	cb_assegnazione.enabled = false
	cb_azzera.enabled = false
	cb_varianti.enabled = false
	cb_corrispondenze.enabled = false
	
	//Donato 24-09-2008 Modifica per selezione dettaglio dell'acquisto al fornitore
	cb_acquisto.enabled = true
	cb_annulla_acq.enabled = true
	//------------------------------------------------------------------------------
	
	wf_proteggi_colonne(getitemnumber(getrow(),"quan_evasa"), getitemnumber(getrow(),"quan_in_evasione"), getitemnumber(getrow(),"anno_commessa"))
	
	uo_gestione_conversioni iuo_oggetto
	iuo_oggetto = create uo_gestione_conversioni
	ld_datawindow  = dw_det_ord_ven_det_1
	iuo_oggetto.uof_blocca_colonne(ld_datawindow, "ord_ven")
	iuo_oggetto.uof_visualizza_um(ld_datawindow, "ord_ven")
	destroy iuo_oggetto
	
end if
return 0
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i1,ll_prog_riga_ord_ven
	string  ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_ord_ven,ls_cod_tipo_det_ven,ls_test
	integer li_risposta

	ll_i = i_parentdw.i_selectedrows[1]
	ll_i1 = getrow()

	ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = getitemnumber(ll_i1, "prog_riga_ord_ven")
	ls_cod_tipo_det_ven = getitemstring(ll_i1, "cod_tipo_det_ven")
	ls_cod_tipo_ord_ven = i_parentdw.getitemstring(ll_i, "cod_tipo_ord_ven")
	
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_ord_ven
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

	select cod_azienda
	into   :ls_test
	from 	 det_ord_ven_stat
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione(ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_det_ven,ll_num_registrazione,ll_anno_registrazione,ll_prog_riga_ord_ven,4)
		if li_risposta=-1 then
			messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
			pcca.error = c_fatal
		end if
	end if
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if this.getitemstatus(this.getrow(), 0, primary!) = datamodified! then
	if this.getitemstring(this.getrow(), "flag_blocco") = "S" then
		messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
					  exclamation!, ok!)
		this.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if
end if
return 0
end event

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_colonna_sconto, &
          ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, &
          ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_des_prodotto, &
          ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_messaggio, &
			 ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_flag_prezzo, ls_flag_blocco, ls_nota_testata, ls_stampa_piede, ls_nota_video
   double ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_variazioni[], ld_num_confezioni, ld_num_pezzi, &
	       ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], ld_min_fat_altezza,ld_min_fat_larghezza, &
			 ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume, ld_quan_proposta
   datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	long ll_riga_origine, ll_i, ll_y

	dec{4}		ld_prezzo_vendita

   setnull(ls_null)


   ls_cod_tipo_listino_prodotto = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_valuta = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_ven = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
   ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
   
   choose case i_colname
		case "prog_riga_ord_ven"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_ord_ven") then
	            messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_ord_ven", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_ord_ven", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
      case "cod_tipo_det_ven"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "quan_ordine.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "quan_ordine.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
   
         ld_datawindow = dw_det_ord_ven_det_1
         wf_tipo_det_ven(ld_datawindow, dw_det_ord_ven_det_1.object.b_ricerca_prodotto, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
         ld_datawindow = dw_det_ord_ven_lista
         wf_tipo_det_ven_lista(ld_datawindow, i_coltext, getrow())

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_ven.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_ven  
            where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "nota_dettaglio", ls_null)
			
//-------------------------------------- Modifica Nicola ---------------------------------------------------------------

			select quan_default
			  into :ld_quan_proposta
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda 
				and cod_tipo_det_ven = :i_coltext;
				
			if sqlca.sqlcode < 0 then
				messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_ord_ven " + sqlca.sqlerrtext)
				return
			end if
			
			if isnull(ld_quan_proposta) then ld_quan_proposta = 0
			dw_det_ord_ven_lista.setitem(dw_det_ord_ven_lista.getrow(), "quan_ordine", ld_quan_proposta)			

//--------------------------------------- Fine Modifica ----------------------------------------------------------------			
			
      case "cod_prodotto"
		if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

         select cod_misura_mag,
					 cod_misura_ven,
                fat_conversione_ven,
                flag_decimali,
                cod_iva,
					 des_prodotto,
				flag_blocco
         into   :ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva,
					 :ls_des_prodotto,
					 :ls_flag_blocco
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
			if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "ORD_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
				
			if ls_flag_blocco="S" then
				g_mb.warning("Informazione all'operatore: questo articolo risulta bloccato in anagrafica!")
			end if
				
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
            this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)


            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
            else
					ld_fat_conversione = 1
               this.setitem(i_rownbr, "fat_conversione_ven", 1)
            end if
            if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				
			uo_1.uof_aggiorna(i_coltext)
			ls_cod_prodotto = i_coltext
			uo_1.uof_ultimo_prezzo(ls_cod_cliente, ls_cod_prodotto)
		else
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_lista,"cod_prodotto")
			return
		end if
		
		dw_det_ord_ven_lista.postevent("ue_postopen")

		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_ord_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_ord_ven_det_1.modify("st_fattore_conv.text=''")
		end if
			
         if not isnull(ls_cod_cliente) then
            select anag_clienti.cod_iva,
                   anag_clienti.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_clienti
            where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_clienti.cod_cliente = :ls_cod_cliente;
         end if

         if sqlca.sqlcode = 0 then
            if ls_cod_iva <> "" and &
               (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
            end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_ordine", ld_quantita)
         end if

			if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_ord_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_ord_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			f_PO_LoadDDDW_DW(dw_det_ord_ven_det_1,"cod_versione",sqlca,&
                 	  "distinta_padri","cod_versione","des_versione",&
	  		           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")
		
			uo_default_prodotto luo_default_prodotto
			luo_default_prodotto = CREATE uo_default_prodotto
			if luo_default_prodotto.uof_flag_gen_commessa(i_coltext) = 0 then
				if luo_default_prodotto.is_cod_versione = "" then setnull(luo_default_prodotto.is_cod_versione)
				setitem(getrow(),"cod_versione",luo_default_prodotto.is_cod_versione)
				setitem(getrow(),"flag_gen_commessa",luo_default_prodotto.is_flag_gen_commessa)
			end if
			destroy luo_default_prodotto
			
			dw_det_ord_ven_det_1.postevent("ue_ricalcola_colonne")
			
		case "quan_ordine"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_ordine", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

			ld_quantita = double(i_coltext)
			if mid(f_flag_controllo(),2,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "quan_doc", &
			              												ld_quantita, &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			
		case "data_consegna"
			if mid(f_flag_controllo(),4,1) = "S" then
				
				//leggo ilprezzo prima dell'itemchanged
				ld_prezzo_vendita = getitemnumber(getrow(),"prezzo_vendita")
				
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
				
				if getitemnumber(getrow(),"prezzo_vendita")=0 or isnull(getitemnumber(getrow(),"prezzo_vendita")) then
						
					//se prima c'era un valore diverso da zero ripristinalo
					if ld_prezzo_vendita>0 then
						setitem(getrow(),"prezzo_vendita",ld_prezzo_vendita)
					end if
				end if
				
			end if
			
			
			
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			if mid(f_flag_controllo(),9,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "prezzo_vendita"
			if mid(f_flag_controllo(),3,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_doc", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
end choose
end if
end event

event updateend;call super::updateend;long   ll_i, ll_anno_registrazione, ll_num_registrazione, ll_riga_per_peso, ll_cont
string  ls_messaggio
dec{5} ld_peso_netto_unitario
uo_funzioni_1 luo_funzioni_1
uo_spese_trasporto luo_spese_trasporto

luo_spese_trasporto = create uo_spese_trasporto

for ll_i = 1 to this.rowcount()
	//donato 24/01/2012
	//calcolo peso netto unitario della riga
	//###############################################
	ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione")
	ll_riga_per_peso = this.getitemnumber(ll_i, "prog_riga_ord_ven")
	
	// stefanop 19/02/2014
	if luo_spese_trasporto.uof_imposta_spese_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_riga_per_peso, false, ref ls_messaggio) < 0 then
		destroy luo_spese_trasporto
		rollback;
		g_mb.error("Attenzione", ls_messaggio)
		return 1
	end if
	
	luo_funzioni_1 = create uo_funzioni_1
	ld_peso_netto_unitario  = 0
	ll_cont = luo_funzioni_1.uof_calcola_peso_riga_ordine(ll_anno_registrazione, ll_num_registrazione, ll_riga_per_peso, ld_peso_netto_unitario, ls_messaggio)
	destroy luo_funzioni_1
	
	if ll_cont<0 then
		rollback;
		g_mb.error("Attenzione", "Si è verificato un errore in fase di calcolo peso netto unitario della riga:" + ls_messaggio)
		return 1
	else
		//se ho ottenuto zero lascio il valore originale
		if ld_peso_netto_unitario>0 then
			
			update det_ord_ven
			set peso_netto=:ld_peso_netto_unitario
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:ll_anno_registrazione and
						num_registrazione=:ll_num_registrazione and
						prog_riga_ord_ven=:ll_riga_per_peso;
			
			setitem(ll_i,"peso_netto", ld_peso_netto_unitario)
			setitemstatus(ll_i, "peso_netto", primary!, NotModified!)
		end if
		
	end if
		
	//##############################################
next

commit;

if rowsinserted + rowsupdated + rowsdeleted > 0 then
	if not isvalid(s_cs_xx.parametri.parametro_w_ord_ven) then return 0
	
	s_cs_xx.parametri.parametro_w_ord_ven.postevent("ue_calcola")
end if

destroy luo_spese_trasporto
end event

type dw_det_ord_ven_det_1 from uo_cs_xx_dw within w_det_ord_ven_tv
event ue_key pbm_dwnkey
event ue_ricalcola_colonne ( )
integer x = 46
integer y = 140
integer width = 3451
integer height = 1220
integer taborder = 160
boolean bringtotop = true
string dataobject = "d_det_ord_ven_det_1"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_det_1,"cod_prodotto")
		end if
	case "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_det_1,"cod_prodotto")	
		end if
	case "nota_dettaglio"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = dw_det_ord_ven_det_1.getitemstring(this.getrow(),"cod_prodotto")
			dw_det_ord_ven_det_1.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_2)
			window_open(w_prod_note_ricerca, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_2) then
				this.setcolumn("nota_dettaglio")
				this.settext(this.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
			end if
		end if
		case "prezzo_vendita"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_7 = "V"
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.getrow(), "cod_cliente")
			s_cs_xx.parametri.parametro_s_10 = "F"
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			//***claudia moficare window
			if isvalid(w_det_ord_ven_storico) then
				w_det_ord_ven_storico.show()
			else
				window_open(w_det_ord_ven_storico, 0)
			end if
		end if
		case "prezzo_um"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_7 = "U"
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.getrow(), "cod_cliente")
			s_cs_xx.parametri.parametro_s_10 = "F"
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			//***claudia moficare window
			if isvalid(w_det_ord_ven_storico) then
				w_det_ord_ven_storico.show()
			else
				window_open(w_det_ord_ven_storico, 0)
			end if
		end if
end choose


end event

event ue_ricalcola_colonne;iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_doc", &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta"))

end event

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_colonna_sconto, ls_cod_tipo_listino_prodotto, ls_cod_cliente, &
			 ls_cod_valuta, ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_des_prodotto, &
          ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_messaggio, ls_nota_testata, ls_stampa_piede, ls_nota_video,&
			 ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_flag_prezzo, ls_flag_blocco
   dec{5} ld_fat_conversione
	double ld_quantita, ld_cambio_ven, ld_variazioni[], ld_num_confezioni, ld_num_pezzi, &
	       ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], ld_min_fat_altezza,ld_min_fat_larghezza, &
			 ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume, ld_qta_vecchia, ld_qta_evasa
   datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	long ll_riga_origine, ll_i, ll_y, ll_anno_registrazione, ll_num_registrazione
	uo_generazione_documenti luo_stato
	
	dec{4} ld_prezzo_vendita
	
   setnull(ls_null)


   ls_cod_tipo_listino_prodotto = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_valuta = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_ven = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
   ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
   ll_anno_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
   
	choose case i_colname
		case "prog_riga_ord_ven"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_ord_ven") then
	            messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_ord_ven", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_ord_ven", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
      case "cod_tipo_det_ven"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "quan_ordine.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "quan_ordine.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.protect='0'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.background.color='16777215'~t"
         dw_det_ord_ven_det_1.modify(ls_modify)
   
         ld_datawindow = dw_det_ord_ven_det_1
         wf_tipo_det_ven(ld_datawindow, dw_det_ord_ven_det_1.object.b_ricerca_prodotto, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
         ld_datawindow = dw_det_ord_ven_lista
         wf_tipo_det_ven_lista(ld_datawindow, i_coltext, getrow())

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_ven.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_ven  
            where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "nota_dettaglio", ls_null)
			
			
      case "cod_prodotto"

		if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
		
         select	cod_misura_mag,
					cod_misura_ven,
					fat_conversione_ven,
					flag_decimali,
					cod_iva,
					des_prodotto,
					flag_blocco
         into   	:ls_cod_misura_mag,
					:ls_cod_misura,
                		:ld_fat_conversione,
                		:ls_flag_decimali,
                		:ls_cod_iva,
					:ls_des_prodotto,
					:ls_flag_blocco
         from   	anag_prodotti
         where  	cod_azienda = :s_cs_xx.cod_azienda and 
                		cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
				
			if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "ORD_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
			
			if ls_flag_blocco="S" then
				g_mb.warning("Informazione all'operatore: questo articolo risulta bloccato in anagrafica!")
			end if
				
			this.setitem(getrow(), "cod_misura", ls_cod_misura)
			this.setitem(getrow(), "des_prodotto", ls_des_prodotto)

			if ld_fat_conversione <> 0 then
				this.setitem(getrow(), "fat_conversione_ven", ld_fat_conversione)
			else
				ld_fat_conversione = 1
              	 this.setitem(getrow(), "fat_conversione_ven", 1)
			end if
				
			if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
			
			uo_1.uof_aggiorna(i_coltext)
			ls_cod_prodotto = i_coltext
			uo_1.uof_ultimo_prezzo(ls_cod_cliente, ls_cod_prodotto)
		else
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_det_1,"cod_prodotto")
			return
		end if
			
		dw_det_ord_ven_lista.postevent("ue_postopen")

		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_ord_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_ord_ven_det_1.modify("st_fattore_conv.text=''")
		end if
			
		if not isnull(ls_cod_cliente) then
			select cod_iva,
					 data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_cliente = :ls_cod_cliente;
		end if

		if sqlca.sqlcode = 0 then
			if ls_cod_iva <> "" and &
				(ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
				this.setitem(getrow(), "cod_iva", ls_cod_iva)
			end if
		end if

		ls_cod_prodotto = i_coltext
		ld_quantita = this.getitemnumber(getrow(), "quan_ordine")
		if ls_flag_decimali = "N" and &
			ld_quantita - int(ld_quantita) > 0 then
			ld_quantita = ceiling(ld_quantita)
			this.setitem(getrow(), "quan_ordine", ld_quantita)
		end if
			
		if mid(f_flag_controllo(),1,1) = "S" then
			iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
			iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
			iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
			iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
			if mid(f_flag_controllo(),7,1) = "S" then
				iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
			else
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
			end if
			iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
			iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
			iuo_condizioni_cliente.str_parametri.dim_1 = 0
			iuo_condizioni_cliente.str_parametri.dim_2 = 0
			iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
			iuo_condizioni_cliente.str_parametri.valore = 0
			iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
			iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
			iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
			iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
			iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
			iuo_condizioni_cliente.wf_condizioni_cliente()
		end if
			
		if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
			dw_det_ord_ven_det_1.setitem(getrow(), "nota_dettaglio", ls_nota_prodotto)
		else
			dw_det_ord_ven_det_1.setitem(getrow(), "nota_dettaglio", ls_null)
		end if

		f_PO_LoadDDDW_DW(dw_det_ord_ven_det_1,"cod_versione",sqlca,&
					  "distinta_padri","cod_versione","des_versione",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")
	
		uo_default_prodotto luo_default_prodotto
		luo_default_prodotto = CREATE uo_default_prodotto
		if luo_default_prodotto.uof_flag_gen_commessa(i_coltext) = 0 then
			if luo_default_prodotto.is_cod_versione = "" then setnull(luo_default_prodotto.is_cod_versione)
			setitem(getrow(),"cod_versione",luo_default_prodotto.is_cod_versione)
			setitem(getrow(),"flag_gen_commessa",luo_default_prodotto.is_flag_gen_commessa)
		end if
		destroy luo_default_prodotto
		
		postevent("ue_ricalcola_colonne")

	case "cod_misura"
			dw_det_ord_ven_lista.postevent("ue_postopen")
			
		case "fat_conversione_ven"
         ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(getrow(), "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(getrow(), "cod_misura") + ")'")
			else
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "fattore", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												double(i_coltext), &
																			ls_cod_valuta)
			
	case "quan_ordine"
         ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
			ld_qta_vecchia =  this.getitemnumber(getrow(), "quan_ordine")
			ld_qta_evasa = this.getitemnumber(getrow(), "quan_evasa")
			
         
			select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(getrow(), "quan_ordine", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

			ld_quantita = double(i_coltext)
			if ld_qta_evasa > ld_quantita then
				messagebox("Attenzione", "Non è possibile modificare la quantità, inserire un importo più alto.", &
			                 exclamation!, ok!)
					rollback;
			  	   return 1
			end if
			if mid(f_flag_controllo(),2,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "quan_doc", &
			              												ld_quantita, &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
																			
				
				//aggiorno evasione della riga
				
				if ld_qta_evasa < ld_quantita  and ld_qta_evasa > 0 then
					dw_det_ord_ven_det_1.setitem(getrow(), "flag_evasione", 'P')
				elseif ld_qta_evasa >= ld_quantita then
					dw_det_ord_ven_det_1.setitem(getrow(), "flag_evasione", 'E')
				elseif ld_qta_evasa = 0 then
					dw_det_ord_ven_det_1.setitem(getrow(), "flag_evasione", 'A')		
				end if

				luo_stato = create uo_generazione_documenti
				
				if luo_stato.uof_calcola_stato_ordine(ll_anno_registrazione,ll_num_registrazione) <> 0 then
					destroy luo_stato
					return -1
				end if
				
				destroy luo_stato
																				  
	case "data_consegna"
			
			// Modifica Daniele 15 Apri 2008
			if row <= 0 then 
				return
			end if
			
			if dwo.name = "data_consegna" then
				if dw_det_ord_ven_det_1.object.data_consegna.protect = "0" then
					if not isnull(getitemdatetime(row , "data_consegna"))  then
						messageBox("Dettaglio Ordini", "Attenzione! E' stata modificata la data di consegna di una riga d'rdine collegata ad una commessa. Non sarà aggiornata la data di consegna della commessa.", Exclamation!, OK!)
						dw_det_ord_ven_det_1.setfocus( )
					end if	
				end if
			end if
			// Fine Modifica
			
			if mid(f_flag_controllo(),4,1) = "S" then
				
				ld_prezzo_vendita = getitemnumber(getrow(),"prezzo_vendita")
				
				ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
				ld_quantita = this.getitemnumber(getrow(), "quan_ordine")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
				
				if getitemnumber(getrow(),"prezzo_vendita")=0 or isnull(getitemnumber(getrow(),"prezzo_vendita")) then
						
					//se prima c'era un valore diverso da zero ripristinalo
					if ld_prezzo_vendita>0 then
						setitem(getrow(),"prezzo_vendita",ld_prezzo_vendita)
					end if
				end if
				
			end if
			
			
	case "num_confezioni"
         ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_ordine")
			ld_num_confezioni = double(i_coltext)
			ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi * ld_num_confezioni
						this.setitem(getrow(), "quan_ordine", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(getrow(), "quan_ordine", ld_quantita)
					end if
				end if
			else
				messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if		
			if mid(f_flag_controllo(),6,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
	case "num_pezzi_confezione"
	         ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_ordine")
			ld_num_pezzi = double(i_coltext)
			ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi * ld_num_confezioni
						this.setitem(getrow(), "quan_ordine", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(getrow(), "quan_ordine", ld_quantita)
					end if
				end if
			else
				messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if				
			if mid(f_flag_controllo(),5,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			if mid(f_flag_controllo(),9,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "prezzo_vendita"
			if mid(f_flag_controllo(),3,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_doc", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
		case "quantita_um"  
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "quan_um", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
		case "prezzo_um"  
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_um", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			
end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_modify, ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_ven = this.getitemstring(i_rownbr,"cod_tipo_det_ven")

   select tab_tipi_det_ven.flag_tipo_det_ven
   into   :ls_flag_tipo_det_ven
   from   tab_tipi_det_ven
   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

   if sqlca.sqlcode <> 0 then
      messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_ven = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Unità di misura obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_ordine")
         if double(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	messagebox("Attenzione", "Quantit ordine obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_ven = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	dw_documenti.uof_retrieve_blob(getrow())
	dw_det_ord_ven_lista.setrow(getrow())

end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_det_1,"cod_prodotto")
end choose
end event

event clicked;call super::clicked;long										ll_num_commessa, ll_row
integer									li_anno_commessa
s_cs_xx_parametri						lstr_param
w_commesse_produzione_tv		lw_window


choose case dwo.name
	case "p_commessa"
		//apri la window delle commesse produzione
		ll_row = dw_det_ord_ven_det_1.getrow()
		
		if ll_row>0 then
			li_anno_commessa = dw_det_ord_ven_det_1.getitemnumber(ll_row, "anno_commessa")
			ll_num_commessa = dw_det_ord_ven_det_1.getitemnumber(ll_row, "num_commessa")
				
			if li_anno_commessa>0 and ll_num_commessa>0 then
				lstr_param.parametro_ul_1 = li_anno_commessa
				lstr_param.parametro_ul_2 = ll_num_commessa
				
				lstr_param.parametro_s_1 = "Commessa da riga ordine vendita n. "+&
														string(dw_det_ord_ven_det_1.getitemnumber(ll_row, "anno_registrazione")) + " / "+&
														string(dw_det_ord_ven_det_1.getitemnumber(ll_row, "num_registrazione")) + " / " + &
														string(dw_det_ord_ven_det_1.getitemnumber(ll_row, "prog_riga_ord_ven"))
				
				opensheetwithparm(lw_window, lstr_param, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_commesse_produzione_tv, lstr_param)
			else
				//nessuna commessa associata
				return
			end if
		end if
		
end choose
end event

type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_ord_ven_tv
integer x = 3552
integer y = 140
integer width = 837
integer height = 1464
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_det_ord_ven_note_blob"
end type

type cb_acquisto from commandbutton within w_det_ord_ven_tv
integer x = 1760
integer y = 1220
integer width = 69
integer height = 84
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;decimal ld_anno_reg_ord_acq, ld_num_reg_ord_acq, ld_prog_riga_ord_acq, ld_null
long ll_row
string ls_cod_prodotto

if this.enabled then
	ll_row = dw_det_ord_ven_det_1.getrow()
	if ll_row <=0 then return
	
	ls_cod_prodotto = dw_det_ord_ven_det_1.getitemstring(ll_row,"cod_prodotto")
	if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
		g_mb.messagebox("OMNIA","Selezionare prima il Prodotto!")
		return
	end if
	
	s_cs_xx.parametri.parametro_s_8 = ls_cod_prodotto
	
	setnull(s_cs_xx.parametri.parametro_s_9)
	//s_cs_xx.parametri.parametro_s_9 = dw_det_ord_acq_lista.i_parentdw.getitemstring(dw_det_ord_acq_lista.i_parentdw.getrow(), "cod_fornitore")
	
	s_cs_xx.parametri.parametro_s_10 = "O"
	
	s_cs_xx.parametri.parametro_s_12 = "%"
	/*
	//-CLAUDIA 23/07/07 MIGLIERAMENTO PERCHè SI POSSA RICERCARE PER DESCRIZIONE
	s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
	*/
	
	s_cs_xx.parametri.parametro_d_1 = 0
	s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_ord_ven_det_1
	if isvalid(w_det_ord_acq_x_vendite) then
		w_det_ord_acq_x_vendite.show()
	else
		window_open(w_det_ord_acq_x_vendite, 0)
	end if
	
//	dw_det_ord_ven_det_1.setitem(ll_row,"anno_reg_ord_acq",ld_null)
//	dw_det_ord_ven_det_1.setitem(ll_row,"num_reg_ord_acq",ld_null)
//	dw_det_ord_ven_det_1.setitem(ll_row,"prog_riga_ord_acq",ld_null)
end if
end event

type cb_annulla_acq from commandbutton within w_det_ord_ven_tv
integer x = 1829
integer y = 1220
integer width = 101
integer height = 88
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
string text = "C"
end type

event clicked;decimal ld_anno_reg_ord_acq, ld_num_reg_ord_acq, ld_prog_riga_ord_acq, ld_null
long ll_row

if this.enabled then
	ll_row = dw_det_ord_ven_det_1.getrow()
	if ll_row <=0 then return
	
	ld_anno_reg_ord_acq = dw_det_ord_ven_det_1.getitemdecimal(ll_row,"anno_reg_ord_acq")
	ld_num_reg_ord_acq = dw_det_ord_ven_det_1.getitemdecimal(ll_row,"num_reg_ord_acq")
	ld_prog_riga_ord_acq = dw_det_ord_ven_det_1.getitemdecimal(ll_row,"prog_riga_ord_acq")
	
	if not isnull(ld_anno_reg_ord_acq) and not isnull(ld_num_reg_ord_acq) and not isnull(ld_prog_riga_ord_acq) then
		if g_mb.messagebox("OMNIA","Eliminare l'associazione con il dettaglio dell'ordine al Fornitore?",Question!,YesNo!,1) = 1 then
			setnull(ld_null)
			dw_det_ord_ven_det_1.setitem(ll_row,"anno_reg_ord_acq",ld_null)
	 		dw_det_ord_ven_det_1.setitem(ll_row,"num_reg_ord_acq",ld_null)
			dw_det_ord_ven_det_1.setitem(ll_row,"prog_riga_ord_acq",ld_null)
		end if
	end if	
end if
end event

type cb_assegnazione from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 2875
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Assegna"
end type

event clicked;integer li_return
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, &
     ll_num_commessa, ll_riga
string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_flag_tipo_det_ven,ls_cod_tipo_causa_sospensione,ls_des_tipo_causa_sospensione
double ld_quan_evasa, ld_quan_in_evasione, ld_quan_commessa


if dw_det_ord_ven_lista.getrow() > 0 then
	if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_fuori_fido") = "N" then

		if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_evasione") <> "E" and &
		   dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "N" and &
			dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_in_evasione") = 0 then

			ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
			ll_num_registrazione  = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
			ll_anno_commessa  = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_commessa")
			ll_num_commessa   = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_commessa")
			ls_cod_deposito   = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_deposito")
			ls_cod_ubicazione = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_ubicazione")
			ls_cod_tipo_det_ven  = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
			ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
			ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto") 
			
			if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_riga_sospesa") = "S" then
				
				ls_cod_tipo_causa_sospensione = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_causa_sospensione")
				
				select des_tipo_causa_sospensione
				into :ls_des_tipo_causa_sospensione
				from tab_tipi_cause_sospensione
				where cod_azienda = :s_cs_xx.cod_azienda and
							cod_tipo_causa_sospensione = :ls_cod_tipo_causa_sospensione;
							
				if g_mb.messagebox("APICE", "La riga d'ordine risulta sospesa per la seguente causa: " + ls_des_tipo_causa_sospensione + ".~r~nPROSEGUO ?", Question!, YesNo!, 2) = 2 then return
			end if
			
			
			
			s_cs_xx.parametri.parametro_d_1 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine") - &
														  dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
			ld_quan_evasa = 0
			
			
			if ll_anno_commessa <> 0 and not isnull(ll_anno_commessa) then
				s_cs_xx.parametri.parametro_d_2 = ll_anno_commessa
				s_cs_xx.parametri.parametro_d_3 = ll_num_commessa
			else
				setnull(s_cs_xx.parametri.parametro_d_2)
				setnull(s_cs_xx.parametri.parametro_d_3)
			end if
				
//				if s_cs_xx.parametri.parametro_s_1 = "stock" then

			select tab_tipi_det_ven.flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			
			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.",  exclamation!, ok!)
				return
			end if
			

			s_cs_xx.parametri.parametro_s_4 = ls_cod_tipo_det_ven
			s_cs_xx.parametri.parametro_ul_1 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
			s_cs_xx.parametri.parametro_ul_2 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
			s_cs_xx.parametri.parametro_ul_3 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
			s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
			s_cs_xx.parametri.parametro_s_2 = ls_cod_deposito
			s_cs_xx.parametri.parametro_s_15 = "OC"
			if isnull(ls_cod_ubicazione) or ls_cod_ubicazione = "" then
				s_cs_xx.parametri.parametro_s_3 = "%"
			else
				s_cs_xx.parametri.parametro_s_3 = ls_cod_ubicazione
			end if

			window_open(w_ass_stock, 0)
			
			ll_riga = dw_det_ord_ven_lista.getrow()
			dw_det_ord_ven_lista.triggerevent("pcd_retrieve")
			dw_det_ord_ven_lista.scrolltorow(ll_riga)
		else
			messagebox("APICE","Riga già evasa, bloccata o in corso di evasione: impossibile assegnare")	
		end if
	end if

end if

commit;

//
// **************************  vecchia versione da qui in giè ----------------------------------------------------------
//

//integer li_return
//long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, &
//     ll_num_commessa
//string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, &
//		 ls_flag_tipo_det_ven
//double ld_quan_evasa, ld_quan_in_evasione, ld_quan_commessa
//
//
//if dw_det_ord_ven_lista.getrow() > 0 then
//	if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_fuori_fido") = "N" then
//
//		if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_evasione") <> "E" and &
//		   dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "N" and &
//			dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_in_evasione") = 0 then
//
//				ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
//				ll_num_registrazione  = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
//				ll_anno_commessa  = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_commessa")
//				ll_num_commessa   = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_commessa")
//				ls_cod_deposito   = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_deposito")
//				ls_cod_ubicazione = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_ubicazione")
//				ls_cod_tipo_det_ven  = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
//				ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
//				ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto") 
//				s_cs_xx.parametri.parametro_d_1 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine") - &
//									                    dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
//				ld_quan_evasa = 0
//									
//				if ll_anno_commessa <> 0 and not isnull(ll_anno_commessa) then
//					s_cs_xx.parametri.parametro_d_2 = ll_anno_commessa
//					s_cs_xx.parametri.parametro_d_3 = ll_num_commessa
//				else
//					setnull(s_cs_xx.parametri.parametro_d_2)
//					setnull(s_cs_xx.parametri.parametro_d_3)
//				end if
//					
//				window_open(w_ass_quan, 0)
//				
//				if s_cs_xx.parametri.parametro_d_1 = 0 then
//					return
//				end if
//
//				if s_cs_xx.parametri.parametro_s_1 = "stock" then
//
//					select tab_tipi_det_ven.flag_tipo_det_ven
//					into   :ls_flag_tipo_det_ven
//					from   tab_tipi_det_ven
//					where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
//							 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//					
//					if sqlca.sqlcode = -1 then
//						messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.", &
//									  exclamation!, ok!)
//						return
//					end if
//					
//					if ls_flag_tipo_det_ven <> "M" then
//						messagebox("Attenzione", "Si sta assegnando un dettaglio non collegato con il magazzino.", &
//									  exclamation!, ok!)
// 						return
//					end if
//
//					s_cs_xx.parametri.parametro_ul_1 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
//					s_cs_xx.parametri.parametro_ul_2 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
//					s_cs_xx.parametri.parametro_ul_3 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
//					s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
//					s_cs_xx.parametri.parametro_s_2 = ls_cod_deposito
//					s_cs_xx.parametri.parametro_s_15 = "OC"
//					if isnull(ls_cod_ubicazione) or ls_cod_ubicazione = "" then
//						s_cs_xx.parametri.parametro_s_3 = "%"
//					else
//						s_cs_xx.parametri.parametro_s_3 = ls_cod_ubicazione
//					end if
//
//					window_open(w_ass_stock, 0)
//
//					li_return = 3
//
//					if  s_cs_xx.parametri.parametro_d_1 = 0 then
//						return
//					end if
//
//					if li_return <> 0 then
//						dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", s_cs_xx.parametri.parametro_d_1)
//						dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", primary!, notmodified!)
//					end if
//				else
//					li_return = f_ass_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_tipo_det_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_prodotto, s_cs_xx.parametri.parametro_d_1, ld_quan_commessa, ld_quan_evasa)
//					if li_return = 1 then
//						dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", s_cs_xx.parametri.parametro_d_1)
//						dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", primary!, notmodified!)
//						commit;
//					elseif li_return = 2 then
//						select det_ord_ven.quan_in_evasione
//						into :ld_quan_in_evasione
//						from det_ord_ven
//						where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
//								det_ord_ven.anno_registrazione = :ll_anno_registrazione and
//								det_ord_ven.num_registrazione = :ll_num_registrazione and
//								det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
//
//								if sqlca.sqlcode <> 0 then
//									messagebox("Attenzione", "E' avvenuto un errore in fase di lettura dettagli.", exclamation!)
//									rollback;
//									return
//								end if
//
//						dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", ld_quan_in_evasione)
//						dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", primary!, notmodified!)
//						commit;
//					end if
//				end if
//		end if
//	end if
//
//	if li_return = 0 then
//		messagebox("Attenzione", "Assegnazione non eseguita.", &
//					  exclamation!)
//		return
//	end if
//
//	if li_return = 2 then
//		messagebox("Attenzione", "Assegnazione eseguita parzialmente.", &
//					  exclamation!)
//		return
//	end if
//
//	messagebox("Informazione", "Assegnazione avvenuta con successo.", &
//				  information!)
//end if
end event

type cb_azzera from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 2487
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&zzera Ass"
end type

event clicked;integer li_return
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
string ls_messaggio
uo_generazione_documenti luo_gen_doc

if dw_det_ord_ven_lista.getrow() > 0 then
	if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_fuori_fido") = "N" then
		if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_evasione") <> "E" and &
		   dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "N" then

			ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
			ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
			ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
			setnull(ls_messaggio)
			
			luo_gen_doc = create uo_generazione_documenti
			li_return = luo_gen_doc.uof_azz_ass_ord_ven_sessione(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven,0 , ls_messaggio)
			destroy luo_gen_doc
			
			if not isnull(ls_messaggio) then messagebox("Azzeramento assegnazione",ls_messaggio, information!)

			if li_return <> 0 then
				dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", 0)
				dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", primary!, notmodified!)
			end if
		end if		
	end if
	
	if li_return = 0 then
		messagebox("Attenzione", "Azzeramento Assegnazione non avvenuta.", &
					  exclamation!)
		return
	end if

	messagebox("Informazione", "Azzeramento Assegnazione avvenuta con successo.", &
				  information!)
end if
end event

type cb_blocca from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 3653
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Blocca"
end type

event clicked;string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_flag_tipo_det_ven,ls_cod_prodotto_raggruppato

dec{4} ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_quantita, ld_quan_raggruppo

long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_i, ll_blocco



if dw_det_ord_ven_lista.getrow() > 0 then
	if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "N" then
		if dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_in_evasione") > 0 then
			messagebox("Attenzione", "Dettaglio non chiudibile. E' presente della quantità in evasione.", &
						  exclamation!, ok!)
			return
		end if
		
		ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
		ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
		ld_quan_ordine = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine")
		ld_quan_evasa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
		
		select tab_tipi_det_ven.flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
						  exclamation!, ok!)
			return
		end if
		
		if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine then
			update anag_prodotti  
				set quan_impegnata = quan_impegnata - (:ld_quan_ordine - :ld_quan_evasa)
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", &
							  exclamation!, ok!)
				rollback;
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			
				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
					rollback;
					return
				end if
			end if
			
		end if

		ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
		ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")

		update det_ord_ven
			set flag_blocco = 'S'
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
				 det_ord_ven.num_registrazione = :ll_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "flag_blocco", "S")
		dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)

		ll_blocco = 0

		for ll_i = 1 to dw_det_ord_ven_det_1.rowcount()
			if dw_det_ord_ven_lista.getitemstring(ll_i, "flag_blocco") = "S" then
				ll_blocco ++
			end if
		next

		if ll_blocco = dw_det_ord_ven_det_1.rowcount() then
			update tes_ord_ven
			set flag_blocco = 'S'
			where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
					tes_ord_ven.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
							  exclamation!, ok!)
				dw_det_ord_ven_lista.i_parentdw.setitem(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
				dw_det_ord_ven_lista.i_parentdw.setitemstatus(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
				rollback;
				return
			end if
			dw_det_ord_ven_lista.i_parentdw.setitem(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "S")
			dw_det_ord_ven_lista.i_parentdw.setitemstatus(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
		end if
		commit;
	end if
end if
end event

type cb_c_industriale from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 1719
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C.Industriale"
end type

event clicked;window_open_parm(w_det_ord_ven_stat,-1,dw_det_ord_ven_lista)
end event

type cb_chiudi_commessa from commandbutton within w_det_ord_ven_tv
integer x = 1161
integer y = 712
integer width = 91
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C."
end type

event clicked;integer li_anno_commessa,li_risposta
long    ll_num_commessa
string  ls_flag_tipo_avanzamento,ls_errore, ls_null

setnull(ls_null)


li_anno_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_commessa")
ll_num_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_commessa")

if li_anno_commessa = 0 or isnull(li_anno_commessa) then 
	messagebox("Apice","Attenzione! Non esiste alcuna commessa associata al dettaglio ordine. Generare prima la commessa!",stopsign!)
	return
end if

if messagebox("APICE","Attenzione; sei sicuro di voler chiudere la commessa associata a questa riga d'ordine?",Question!,YesNo!,2) = 2 then return

select flag_tipo_avanzamento
into   :ls_flag_tipo_avanzamento
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode < 0 then
	messagebox("Apice","Attenzione! Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or &
	ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
	messagebox("Apice","Attenzione! La commessa si trova in uno stato per il quale non è più possibile effettuare un avanzamento automatico.",stopsign!)
	return
else
	uo_funzioni_1 luo_funzioni_1
	
	luo_funzioni_1 = CREATE uo_funzioni_1
	li_risposta = luo_funzioni_1.uof_avanza_commessa(li_anno_commessa,ll_num_commessa,ls_null,ls_errore)
	
	if li_risposta < 0 then 
		li_risposta = f_scrivi_log ("Errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + ". Dettaglio errore: " + ls_errore)
		messagebox("Sep","Si è manifestato un errore durante la chiusura commessa: consultare il file di log.",stopsign!) 
		if li_risposta= -1 then
			messagebox("Sep","Si è manifestato un errore durante la chiusura commessa ma non è stato possibile scrivere il file di log. Questo errore non è bloccante (riguarda esclusivamente la creazione del file log molto probabilmente manca il parametro log).",stopsign!) 
		end if	
		
		rollback;
		destroy luo_funzioni_1
		return
		
	end if
	
	commit;
	destroy luo_funzioni_1
	messagebox("Sep","Chiusura commessa avvenuta con successo!",information!) 
end if 

end event

type cb_corrispondenze from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 2107
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Ord_Ven"
window_open_parm(w_det_acq_ven_corr, -1, dw_det_ord_ven_det_1)

end event

type cb_sblocca from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 3264
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sbl&occa"
end type

event clicked;string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_flag_tipo_det_ven,ls_cod_prodotto_raggruppato

dec{4} ld_quan_ordine, ld_quan_evasa, ld_quantita, ld_quan_raggruppo

long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven



if dw_det_ord_ven_lista.getrow() > 0 then
	if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "S" then
		ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
		ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
		ld_quan_ordine = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine")
		ld_quan_evasa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")

		select tab_tipi_det_ven.flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
						  exclamation!, ok!)
			return
		end if
		
		if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine then
			update anag_prodotti  
				set quan_impegnata = quan_impegnata + (:ld_quan_ordine - :ld_quan_evasa)
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
			if sqlca.sqlcode = -1 then
				messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
				rollback;
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then

				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")

				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			
				if sqlca.sqlcode = -1 then
					messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
					rollback;
					return
				end if
			end if
		end if

		ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
		ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")

		update det_ord_ven
			set flag_blocco = 'N'
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
				 det_ord_ven.num_registrazione = :ll_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		update tes_ord_ven
		set flag_blocco = 'N'
		where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
				tes_ord_ven.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "flag_blocco", "N")
		dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
		dw_det_ord_ven_lista.i_parentdw.setitem(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
		dw_det_ord_ven_lista.i_parentdw.setitemstatus(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
		commit;
	end if
end if
end event

type cb_sconti from commandbutton within w_det_ord_ven_tv
integer x = 4041
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_ord_ven_lista
window_open(w_sconti, 0)

end event

