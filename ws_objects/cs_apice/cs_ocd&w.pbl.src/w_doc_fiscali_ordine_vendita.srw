﻿$PBExportHeader$w_doc_fiscali_ordine_vendita.srw
forward
global type w_doc_fiscali_ordine_vendita from w_cs_xx_principale
end type
type dw_blob_tes from uo_cs_xx_dw within w_doc_fiscali_ordine_vendita
end type
type st_etichetta from statictext within w_doc_fiscali_ordine_vendita
end type
type dw_det from uo_cs_xx_dw within w_doc_fiscali_ordine_vendita
end type
type dw_lista from uo_cs_xx_dw within w_doc_fiscali_ordine_vendita
end type
end forward

global type w_doc_fiscali_ordine_vendita from w_cs_xx_principale
integer width = 4690
integer height = 2456
string title = "Visualizzazione Doc. Fiscali Ordine"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
event ue_postopen ( )
dw_blob_tes dw_blob_tes
st_etichetta st_etichetta
dw_det dw_det
dw_lista dw_lista
end type
global w_doc_fiscali_ordine_vendita w_doc_fiscali_ordine_vendita

type variables
string			is_path_base_png
end variables

forward prototypes
public function integer wf_visualizza_doc (long al_row, ref string as_errore)
end prototypes

event ue_postopen();string				ls_modify


ls_modify = ".font.weight=~"400~tif( anno_ordine= anno_ordine_arg and  num_ordine=num_ordine_arg, 700, 400)~""


dw_det.modify("riga"+ls_modify)
dw_det.modify("cod_tipo_det_ven"+ls_modify)
dw_det.modify("cod_prodotto"+ls_modify)
dw_det.modify("des_prodotto"+ls_modify)
dw_det.modify("quantita"+ls_modify)
dw_det.modify("cod_misura"+ls_modify)
dw_det.modify("prezzo_vendita"+ls_modify)
dw_det.modify("imponibile_iva"+ls_modify)
dw_det.modify("sconto_1"+ls_modify)
dw_det.modify("sconto_2"+ls_modify)
dw_det.modify("cf_mov_mag"+ls_modify)

end event

public function integer wf_visualizza_doc (long al_row, ref string as_errore);long			ll_len, ll_num_doc, ll_prog_mimetype
string			ls_estensione, ls_tipo_doc, ls_temp_dir, ls_rnd, ls_cod_nota, ls_file_name
integer		li_anno_doc
blob			lb_blob

uo_shellexecute		luo_run


if al_row>0 then
else
	as_errore = "Selezionare una riga con i file da visualizzare!"
	return -1
end if


li_anno_doc = dw_blob_tes.getitemnumber(al_row, "anno_registrazione")
ll_num_doc = dw_blob_tes.getitemnumber(al_row, "num_registrazione")
ls_cod_nota = dw_blob_tes.getitemstring(al_row, "cod_nota")
ls_tipo_doc = dw_blob_tes.getitemstring(al_row, "tipo_doc")
ls_estensione = dw_blob_tes.getitemstring(al_row, "estensione")

choose case UPPER(ls_tipo_doc)
	case "F"
		selectblob 	note_esterne
		into 			:lb_blob
		from tes_fat_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_doc and
				num_registrazione = :ll_num_doc and
				cod_nota = :ls_cod_nota;
		
	case "B"
		selectblob 	note_esterne
		into 			:lb_blob
		from tes_bol_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_doc and
				num_registrazione = :ll_num_doc and
				cod_nota = :ls_cod_nota;
		
	case else
		as_errore = "Gestione non prevista (nè F, nè B)!"
		return -1
		
end choose

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura documento: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Dato non trovato!"
	return -1
end if


ll_len = lenA(lb_blob)
ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
	
ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
	
luo_run = create uo_shellexecute
luo_run.uof_run( handle(this), ls_file_name )
destroy(luo_run)
end function

on w_doc_fiscali_ordine_vendita.create
int iCurrent
call super::create
this.dw_blob_tes=create dw_blob_tes
this.st_etichetta=create st_etichetta
this.dw_det=create dw_det
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_blob_tes
this.Control[iCurrent+2]=this.st_etichetta
this.Control[iCurrent+3]=this.dw_det
this.Control[iCurrent+4]=this.dw_lista
end on

on w_doc_fiscali_ordine_vendita.destroy
call super::destroy
destroy(this.dw_blob_tes)
destroy(this.st_etichetta)
destroy(this.dw_det)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent + c_noNew + c_noModify + c_noDelete, &
                                    c_default)
												
dw_det.set_dw_options(	sqlca, &
								pcca.null_object, &
                                    c_noretrieveOnOpen + c_noNew + c_noModify + c_noDelete + c_default, &
                                    c_default)

dw_blob_tes.set_dw_options(	sqlca, &
								pcca.null_object, &
                                    c_noretrieveOnOpen + c_noNew + c_noModify + c_noDelete + c_default, &
                                    c_default)
												
is_path_base_png = s_cs_xx.volume + s_cs_xx.risorse + "mimetype\small\"

this.event post ue_postopen()
end event

type dw_blob_tes from uo_cs_xx_dw within w_doc_fiscali_ordine_vendita
integer x = 3369
integer y = 16
integer width = 1289
integer height = 652
integer taborder = 20
string dataobject = "d_doc_fiscali_ordine_vendita_blob"
boolean vscrollbar = true
end type

event doubleclicked;//ancestor script disattivato

string			ls_errore

setpointer(Hourglass!)

if wf_visualizza_doc(row, ls_errore) < 0 then
	g_mb.error(ls_errore)
	
	setpointer(Arrow!)
	return
end if

setpointer(Arrow!)
end event

type st_etichetta from statictext within w_doc_fiscali_ordine_vendita
boolean visible = false
integer x = 50
integer y = 216
integer width = 3232
integer height = 96
integer textsize = -14
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 134217747
long backcolor = 12632256
string text = "Nessun Documento Fiscale (DdT o Fattura) risulta presente per quest~'ordine ..."
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_det from uo_cs_xx_dw within w_doc_fiscali_ordine_vendita
integer x = 23
integer y = 692
integer width = 4635
integer height = 1660
integer taborder = 20
string dataobject = "d_doc_fiscali_ordine_vendita_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

type dw_lista from uo_cs_xx_dw within w_doc_fiscali_ordine_vendita
integer x = 23
integer y = 16
integer width = 3328
integer height = 652
integer taborder = 10
string dataobject = "d_doc_fiscali_ordine_vendita_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string				ls_cod_cliente, ls_rag_soc_1, ls_tipo_doc
long				ll_errore, ll_num_ordine, ll_num_doc_fiscale, ll_row
integer			li_anno_ordine, li_anno_doc_fiscale

li_anno_ordine = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_ordine = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
		 
if sqlca.sqlcode = 0 then
	parent.title = "Doc. Fiscali per Ordine " + &
						string(li_anno_ordine,"####") + "/" + &
						string(ll_num_ordine) + "  Cliente: " + ls_rag_soc_1
end if

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_ordine, ll_num_ordine)

if ll_errore < 0 then
   pcca.error = c_fatal
	return
end if

if ll_errore>0 then
	st_etichetta.visible = false
else
	st_etichetta.visible = true
end if

		
ll_row = getrow()

if ll_row>0 then
	li_anno_doc_fiscale = getitemnumber(ll_row, "anno")
	ll_num_doc_fiscale = getitemnumber(ll_row, "num")
	ls_tipo_doc = upper(getitemstring(ll_row, "tipo_doc"))
	
	if ls_tipo_doc="D.D.T." then
		ls_tipo_doc = "B"
	elseif ls_tipo_doc="FATTURA" then
		ls_tipo_doc = "F"
	else
		dw_det.reset()
		dw_blob_tes.reset()
		return
	end if
	
	//retrieve dettagli documento fiscale
	dw_det.retrieve(s_cs_xx.cod_azienda, li_anno_doc_fiscale, ll_num_doc_fiscale, ls_tipo_doc, li_anno_ordine, ll_num_ordine)
	
	//retrieve focumenti associati alla testata
	dw_blob_tes.retrieve(s_cs_xx.cod_azienda, li_anno_doc_fiscale, ll_num_doc_fiscale, ls_tipo_doc, is_path_base_png)
	
else
	dw_det.reset()
	dw_blob_tes.reset()
end if
end event

event rowfocuschanged;string				ls_tipo_doc
integer			li_anno_ordine, li_anno_doc_fiscale
long				ll_num_ordine, ll_num_doc_fiscale


if currentrow>0 then
	
	selectrow(0, false)
	selectrow(currentrow, true)
	
	li_anno_ordine = getitemnumber(currentrow, "anno_ordine")
	ll_num_ordine = getitemnumber(currentrow, "num_ordine")
	li_anno_doc_fiscale = getitemnumber(currentrow, "anno")
	ll_num_doc_fiscale = getitemnumber(currentrow, "num")
	ls_tipo_doc = upper(getitemstring(currentrow, "tipo_doc"))
	
	if ls_tipo_doc="D.D.T." then
		ls_tipo_doc = "B"
	elseif ls_tipo_doc="FATTURA" then
		ls_tipo_doc = "F"
	else
		dw_det.reset()
		dw_blob_tes.reset()
		return
	end if
	
	//retrieve dettagli documento fiscale
	dw_det.retrieve(s_cs_xx.cod_azienda, li_anno_doc_fiscale, ll_num_doc_fiscale, ls_tipo_doc, li_anno_ordine, ll_num_ordine)
	
	//retrive documenti testata
	dw_blob_tes.retrieve(s_cs_xx.cod_azienda, li_anno_doc_fiscale, ll_num_doc_fiscale, ls_tipo_doc, is_path_base_png)
	
else
	//dw_det.reset()
	//selectrow(0, false)
end if
end event

