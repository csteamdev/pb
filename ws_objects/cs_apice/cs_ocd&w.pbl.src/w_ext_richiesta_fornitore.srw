﻿$PBExportHeader$w_ext_richiesta_fornitore.srw
$PBExportComments$Richiesta Fornitore durante generazione ordini acquisto da fabbisogni
forward
global type w_ext_richiesta_fornitore from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_ext_richiesta_fornitore
end type
type cb_2 from commandbutton within w_ext_richiesta_fornitore
end type
type dw_ext_richiesta_fornitore from uo_cs_xx_dw within w_ext_richiesta_fornitore
end type
end forward

global type w_ext_richiesta_fornitore from w_cs_xx_risposta
int Width=2291
int Height=697
boolean TitleBar=true
string Title="Richiesta Fornitore"
boolean ControlMenu=false
boolean Resizable=false
cb_1 cb_1
cb_2 cb_2
dw_ext_richiesta_fornitore dw_ext_richiesta_fornitore
end type
global w_ext_richiesta_fornitore w_ext_richiesta_fornitore

event pc_setwindow;call super::pc_setwindow;dw_ext_richiesta_fornitore.set_dw_options( sqlca, &
														 pcca.null_object, &
														 c_nomodify + &
														 c_nodelete + &
														 c_newonopen + &
														 c_disableCC, &
														 c_noresizedw + &
														 c_nohighlightselected + &
														 c_cursorrowpointer)
save_on_close(c_socnosave)
end event

on w_ext_richiesta_fornitore.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_ext_richiesta_fornitore=create dw_ext_richiesta_fornitore
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=cb_2
this.Control[iCurrent+3]=dw_ext_richiesta_fornitore
end on

on w_ext_richiesta_fornitore.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_ext_richiesta_fornitore)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ext_richiesta_fornitore, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_1 from commandbutton within w_ext_richiesta_fornitore
int X=1898
int Y=521
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if isnull(dw_ext_richiesta_fornitore.getitemstring(dw_ext_richiesta_fornitore.getrow(),"cod_fornitore")) then
	g_mb.messagebox("Richiesta Fornitori", "Specificare un fornitore !")
	return
else
	s_cs_xx.parametri.parametro_s_3 = dw_ext_richiesta_fornitore.getitemstring(dw_ext_richiesta_fornitore.getrow(),"cod_fornitore")
end if

close(parent)
end event

type cb_2 from commandbutton within w_ext_richiesta_fornitore
int X=1509
int Y=521
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_3 = ""
close(parent)
end event

type dw_ext_richiesta_fornitore from uo_cs_xx_dw within w_ext_richiesta_fornitore
int X=1
int Y=1
int Width=2286
int Height=521
int TabOrder=30
string DataObject="d_ext_richiesta_fornitore"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_new;call super::pcd_new;dw_ext_richiesta_fornitore.setitem(lparam, "cod_prodotto", s_cs_xx.parametri.parametro_s_1)
dw_ext_richiesta_fornitore.setitem(lparam, "des_prodotto", s_cs_xx.parametri.parametro_s_2)
dw_ext_richiesta_fornitore.setitem(lparam, "quan_ordine", s_cs_xx.parametri.parametro_d_1)

end event

