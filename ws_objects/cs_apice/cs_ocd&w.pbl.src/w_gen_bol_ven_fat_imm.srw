﻿$PBExportHeader$w_gen_bol_ven_fat_imm.srw
$PBExportComments$Finestra Generazione Bolle Vendita / Fatture Immediate
forward
global type w_gen_bol_ven_fat_imm from w_cs_xx_principale
end type
type cb_genera from uo_cb_ok within w_gen_bol_ven_fat_imm
end type
type cb_1 from uo_cb_close within w_gen_bol_ven_fat_imm
end type
type cbx_singole from checkbox within w_gen_bol_ven_fat_imm
end type
type ddlb_cod_tipo_rag from dropdownlistbox within w_gen_bol_ven_fat_imm
end type
type st_cod_tipo_rag from statictext within w_gen_bol_ven_fat_imm
end type
type st_2 from statictext within w_gen_bol_ven_fat_imm
end type
type gb_2 from groupbox within w_gen_bol_ven_fat_imm
end type
type gb_1 from groupbox within w_gen_bol_ven_fat_imm
end type
type rb_bolle from radiobutton within w_gen_bol_ven_fat_imm
end type
type rb_fatture from radiobutton within w_gen_bol_ven_fat_imm
end type
type rb_entrambe from radiobutton within w_gen_bol_ven_fat_imm
end type
type cb_add_bolla from commandbutton within w_gen_bol_ven_fat_imm
end type
type dw_gen_orig_dest from uo_cs_xx_dw within w_gen_bol_ven_fat_imm
end type
type dw_folder from u_folder within w_gen_bol_ven_fat_imm
end type
type dw_gen_bol_ven_fat_imm from uo_cs_xx_dw within w_gen_bol_ven_fat_imm
end type
end forward

global type w_gen_bol_ven_fat_imm from w_cs_xx_principale
integer width = 2757
integer height = 1660
string title = "Generazione Bolle / Fatture Immediate"
cb_genera cb_genera
cb_1 cb_1
cbx_singole cbx_singole
ddlb_cod_tipo_rag ddlb_cod_tipo_rag
st_cod_tipo_rag st_cod_tipo_rag
st_2 st_2
gb_2 gb_2
gb_1 gb_1
rb_bolle rb_bolle
rb_fatture rb_fatture
rb_entrambe rb_entrambe
cb_add_bolla cb_add_bolla
dw_gen_orig_dest dw_gen_orig_dest
dw_folder dw_folder
dw_gen_bol_ven_fat_imm dw_gen_bol_ven_fat_imm
end type
global w_gen_bol_ven_fat_imm w_gen_bol_ven_fat_imm

type variables

end variables

forward prototypes
public function integer wf_progetto_tenda (long fl_anno_ordine, long fl_num_ordine, long fl_anno_bolla, long fl_num_bolla)
public function integer wf_genera_bolle_singole (long al_anno_registrazione, ref string ls_messaggio)
public function integer wf_genera_fatture_singole (long al_anno_registrazione, ref string ls_messaggio)
public function integer wf_genera_bolle_riepilogate (long al_anno_registrazione, ref string ls_messaggio)
public function integer wf_genera_bolle_singole_1 (long al_anno_registrazione, string as_cod_tipo_ord_ven, long al_anno_ordine, long al_num_ordine, ref string ls_messaggio)
public function integer wf_genera_fatture_riepilogate (long al_anno_registrazione, ref string ls_messaggio)
public function integer wf_crea_nota_fissa (string fs_documento, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer wf_genera_fatture_singole_1 (long al_anno_registrazione, string as_cod_tipo_ord_ven, long al_anno_ordine, long al_num_ordine, ref string ls_messaggio)
end prototypes

public function integer wf_progetto_tenda (long fl_anno_ordine, long fl_num_ordine, long fl_anno_bolla, long fl_num_bolla);	// ************************************************* michela 30/04/2004
	// *** specifica passaggio_ordini progetto tenda: se nell'ordine esistono i campi per stampare 
	// *** riferimento interscambio e rif vs ordine allora vuol dire che siamo in progettotenda.
	// *** quindi se nell'apertura del cursore c'è errore, vuol dire che i campi non esistono e che 
	// *** quindi non siamo in progetto tenda...quindi si continua con il for.
string  ls_flag_stampa_rif_intersc, ls_flag_stampa_num_ordine, ls_rif_interscambio, ls_num_ord_cliente, &
        ls_cod_tipo_bol_ven, ls_cod_tipo_det_ven_rif_ord_ven
long    ll_max
	
declare cu_ordine cursor for
select flag_stampa_rif_intersc,
       flag_stampa_num_ordine,
		 rif_interscambio,
		 num_ord_cliente
from   tes_ord_ven 
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_ordine and
		 num_registrazione = :fl_num_ordine;
			 
open cu_ordine;
	
if sqlca.sqlcode < 0 then 
	close cu_ordine;
	return 0
end if
	
fetch cu_ordine into :ls_flag_stampa_rif_intersc,
                     :ls_flag_stampa_num_ordine,
							:ls_rif_interscambio,
							:ls_num_ord_cliente;
if sqlca.sqlcode = 100 then return 0
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la fetch del cursore. " + sqlca.sqlerrtext)
	close cu_ordine;
	return -1
end if
	
close cu_ordine;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la chiusura del cursore. " + sqlca.sqlerrtext)
	return -1
end if	
	
if ls_flag_stampa_rif_intersc <> "S" and ls_flag_stampa_num_ordine <> "S" then return 0

select cod_tipo_bol_ven
into   :ls_cod_tipo_bol_ven
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_bolla and
		 num_registrazione = :fl_num_bolla ;
		 
select cod_tipo_det_ven_rif_ord_ven
into   :ls_cod_tipo_det_ven_rif_ord_ven
from   tab_tipi_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
			 
	
select max(prog_riga_bol_ven)
into   :ll_max
from   det_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_bolla and
		 num_registrazione = :fl_num_bolla;
			 
if isnull(ll_max) then ll_max = 0
ll_max = ll_max + 10
	
if ls_flag_stampa_rif_intersc = "S" and not isnull(ls_rif_interscambio) then
	insert into det_bol_ven(cod_azienda,
	                        anno_registrazione,
									num_registrazione,
									prog_riga_bol_ven,
									cod_tipo_det_ven,
									des_prodotto)
						  values(:s_cs_xx.cod_azienda,
						         :fl_anno_bolla,
									:fl_num_bolla,
									:ll_max,
									:ls_cod_tipo_det_ven_rif_ord_ven,
									:ls_rif_interscambio);
										
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante l'inserimento del riferimento. " + sqlca.sqlerrtext)
		return -1
	end if		
	ll_max = ll_max + 10
end if
	
if ls_flag_stampa_num_ordine = "S" and not isnull(ls_num_ord_cliente) then
	insert into det_bol_ven(cod_azienda,
	                        anno_registrazione,
									num_registrazione,
									prog_riga_bol_ven,
									cod_tipo_det_ven,
									des_prodotto)
						  values(:s_cs_xx.cod_azienda,
						         :fl_anno_bolla,
									:fl_num_bolla,
									:ll_max,
									:ls_cod_tipo_det_ven_rif_ord_ven,
									:ls_num_ord_cliente);		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante l'inserimento del riferimento vostro ordine. " + sqlca.sqlerrtext)
		return -1
	end if				
		
end if	
return 0	
	// **************************************************
	// **************************************************
end function

public function integer wf_genera_bolle_singole (long al_anno_registrazione, ref string ls_messaggio);long ll_i, ll_selected[], ll_anno_ordine, ll_num_ordine
string ls_cod_tipo_ord_ven


dw_gen_bol_ven_fat_imm.get_selected_rows(ll_selected[])
for ll_i = 1 to upperbound(ll_selected)
	ls_cod_tipo_ord_ven = dw_gen_bol_ven_fat_imm.getitemstring(ll_selected[ll_i], "tes_ord_ven_cod_tipo_ord_ven")
	ll_anno_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_anno_registrazione")
	ll_num_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_num_registrazione")
	if wf_genera_bolle_singole_1(al_anno_registrazione, ls_cod_tipo_ord_ven, ll_anno_ordine, ll_num_ordine, ls_messaggio) = -1 then
		return -1
	end if
next

return 0

end function

public function integer wf_genera_fatture_singole (long al_anno_registrazione, ref string ls_messaggio);long ll_i, ll_selected[], ll_anno_ordine, ll_num_ordine
string ls_cod_tipo_ord_ven

dw_gen_bol_ven_fat_imm.get_selected_rows(ll_selected[])
for ll_i = 1 to upperbound(ll_selected)
	ls_cod_tipo_ord_ven = dw_gen_bol_ven_fat_imm.getitemstring(ll_selected[ll_i], "tes_ord_ven_cod_tipo_ord_ven")
	ll_anno_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_anno_registrazione")
	ll_num_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_num_registrazione")
	if wf_genera_fatture_singole_1(al_anno_registrazione, ls_cod_tipo_ord_ven, ll_anno_ordine, ll_num_ordine, ls_messaggio) = -1 then
		return -1
	end if
next

return 0
end function

public function integer wf_genera_bolle_riepilogate (long al_anno_registrazione, ref string ls_messaggio);datastore lds_rag_documenti
boolean lb_riferimento
long ll_i, ll_selected[], ll_anno_ordine, ll_num_ordine, ll_prog_riga_documento, &
	  ll_num_registrazione, ll_prog_riga_bol_ven, ll_prog_riga_ord_ven, ll_prog_riga, &
	  ll_progr_stock[1], ll_anno_commessa, ll_num_commessa, ll_anno_reg_des_mov, &
	  ll_num_reg_des_mov, ll_num_ordine_old, ll_num_confezioni, ll_num_pezzi_confezione, &
	  ll_anno_registrazione
string ls_cod_tipo_ord_ven, ls_flag_riep_bol, ls_cod_tipo_bol_ven, ls_cod_cliente[1], &
		 ls_cod_valuta, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, &
		 ls_cod_banca_clien_for, ls_cod_des_cliente, ls_num_ord_cliente, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_flag_tipo_doc, ls_flag_destinazione, ls_flag_num_doc, &
		 ls_flag_imballo, ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, &
		 ls_flag_resa, ls_flag_data_consegna, ls_cod_tipo_rag, ls_sort, ls_key_sort, &
		 ls_key_sort_old, ls_cod_tipo_analisi_bol, ls_causale_trasporto_tab, ls_lire, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_tipo_listino_prodotto, ls_aspetto_beni, ls_causale_trasporto, ls_nota_testata, &
		 ls_nota_piede, ls_flag_riep_fat, ls_vostro_ordine, ls_vostro_ordine_data, &
		 ls_cod_deposito[1], ls_cod_ubicazione[1], ls_cod_lotto[1], ls_cod_tipo_det_ven, &
		 ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio,  &
		 ls_cod_centro_costo, ls_flag_tipo_det_ven, ls_cod_tipo_movimento, ls_flag_sola_iva, &
		 ls_cod_fornitore[1], ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_cod_tipo_analisi_ord, ls_flag_evasione,ls_cod_versione, &
		 ls_cod_causale, ls_flag_abilita_rif_ord_ven, ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, &
		 ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, &
		 ls_cod_causale_tab, ls_des_riferimento, ls_cod_iva_rif, ls_note_dettaglio_rif, &
		 ls_db, ls_nota_piede_1, ls_nota_testata_1, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, &
		 ls_flag_doc_suc_det, ls_cod_natura_intra
datetime ldt_data_consegna, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_registrazione, &
		   ldt_data_stock[1], ldt_data_consegna_ordine
decimal ld_sconto_pagamento, ld_cambio_ven, ld_sconto_testata, ld_peso_netto, ld_peso_lordo, &
		 ld_num_colli, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_quan_in_evasione, ld_quan_ordine, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_provvigione_1, ld_provvigione_2, ld_quan_evasa, &
		 ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
		 ld_sconto_9, ld_sconto_10, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um

integer li_risposta
string	ls_cod_operatore_new

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

ll_anno_registrazione = f_anno_esercizio()
dw_gen_bol_ven_fat_imm.get_selected_rows(ll_selected[])
lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_documenti"
lds_rag_documenti.settransobject(sqlca)
ls_cod_tipo_rag = f_po_selectddlb(ddlb_cod_tipo_rag)
if isnull(ls_cod_tipo_rag) or len(ls_cod_tipo_rag) < 1 then
	g_mb.messagebox("APICE","Indicare il tipo raggrupamento da utilizzare")
	return 0
end if

for ll_i = 1 to upperbound(ll_selected)
	ls_cod_tipo_ord_ven = dw_gen_bol_ven_fat_imm.getitemstring(ll_selected[ll_i], "tes_ord_ven_cod_tipo_ord_ven")
	ll_anno_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_anno_registrazione")
	ll_num_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_num_registrazione")

	select tes_ord_ven.flag_riep_bol
	into   :ls_flag_riep_bol
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and 
			 tes_ord_ven.num_registrazione = :ll_num_ordine;
			 
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della Testata Ordini Vendita"
		return -1
	end if

	if ls_flag_riep_bol = 'N' then
		if wf_genera_bolle_singole_1(al_anno_registrazione, ls_cod_tipo_ord_ven, ll_anno_ordine, ll_num_ordine, ls_messaggio) = -1 then
			return -1
		end if
	else
		select tab_tipi_ord_ven.cod_tipo_bol_ven
		into   :ls_cod_tipo_bol_ven
		from   tab_tipi_ord_ven  
		where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita."
			return -1
		end if
		
		if not isnull(ls_cod_tipo_bol_ven) then
			if not isnull(ls_cod_tipo_rag) and trim(ls_cod_tipo_rag) <> "" then
				select tab_tipi_rag.flag_tipo_doc,   
						 tab_tipi_rag.flag_destinazione,   
						 tab_tipi_rag.flag_num_doc,   
						 tab_tipi_rag.flag_imballo,   
						 tab_tipi_rag.flag_vettore,   
						 tab_tipi_rag.flag_inoltro,   
						 tab_tipi_rag.flag_mezzo,   
						 tab_tipi_rag.flag_porto,   
						 tab_tipi_rag.flag_resa,   
						 tab_tipi_rag.flag_data_consegna  
				into   :ls_flag_tipo_doc,   
						 :ls_flag_destinazione,   
						 :ls_flag_num_doc,   
						 :ls_flag_imballo,   
						 :ls_flag_vettore,   
						 :ls_flag_inoltro,   
						 :ls_flag_mezzo,   
						 :ls_flag_porto,   
						 :ls_flag_resa,   
						 :ls_flag_data_consegna  
				from   tab_tipi_rag  
				where  tab_tipi_rag.cod_azienda = :s_cs_xx.cod_azienda and
						 tab_tipi_rag.cod_tipo_rag = :ls_cod_tipo_rag;
	
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Errore durante la lettura Tabella Tipi Ragruppamenti."
					destroy lds_rag_documenti
					return -1
				end if
			end if
			
			select tes_ord_ven.cod_cliente,   
					 tes_ord_ven.cod_valuta,   
					 tes_ord_ven.cod_pagamento,   
					 tes_ord_ven.cod_agente_1,
					 tes_ord_ven.cod_agente_2,
					 tes_ord_ven.cod_banca,   
					 tes_ord_ven.cod_banca_clien_for,
					 tes_ord_ven.cod_tipo_ord_ven,
					 tes_ord_ven.cod_des_cliente,   
					 tes_ord_ven.num_ord_cliente,   
					 tes_ord_ven.cod_imballo,   
					 tes_ord_ven.cod_vettore,   
					 tes_ord_ven.cod_inoltro,   
					 tes_ord_ven.cod_mezzo,
					 tes_ord_ven.cod_porto,   
					 tes_ord_ven.cod_resa
			 into  :ls_cod_cliente[1],
					 :ls_cod_valuta,   
					 :ls_cod_pagamento,   
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_cod_banca_clien_for,
					 :ls_cod_tipo_ord_ven,
					 :ls_cod_des_cliente,   
					 :ls_num_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_cod_porto,
					 :ls_cod_resa
			from   tes_ord_ven  
			where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
					 tes_ord_ven.num_registrazione = :ll_num_ordine;
		
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la lettura Testata Ordine Vendita."
				destroy lds_rag_documenti
				return -1
			end if

			if ls_flag_data_consegna = "S" then
				declare cu_det_ord_ven cursor for  
				select   det_ord_ven.prog_riga_ord_ven,
							det_ord_ven.data_consegna
				from     det_ord_ven  
				where    det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							det_ord_ven.anno_registrazione = :ll_anno_ordine and
							det_ord_ven.num_registrazione = :ll_num_ordine
				order by det_ord_ven.data_consegna asc;

				open cu_det_ord_ven;
				
				do while 0 = 0
					fetch cu_det_ord_ven into :ll_prog_riga_documento, :ldt_data_consegna;
			
					if sqlca.sqlcode = 100 then exit
					
					if sqlca.sqlcode <> 0 then
						ls_messaggio = "Errore durante la lettura Dettagli Ordine di Vendita."
						destroy lds_rag_documenti
						close cu_det_ord_ven;
						return -1
					end if

					lds_rag_documenti.insertrow(0)
					lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_documento)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_cliente", ls_cod_cliente[1])
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_1", ls_cod_agente_1)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_2", ls_cod_agente_2)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_ven)
//					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_doc_esterno", ls_num_ord_cliente)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "data_consegna", ldt_data_consegna)
				loop
				close cu_det_ord_ven;
			else
				lds_rag_documenti.insertrow(0)
				lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_cliente", ls_cod_cliente[1])
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_1", ls_cod_agente_1)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_2", ls_cod_agente_2)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_ven)
//				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_doc_esterno", ls_num_ord_cliente)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
			end if
		end if
	end if
next

ls_sort = "cod_cliente A, cod_valuta A, cod_pagamento A, cod_agente_1 A, cod_agente_2 A, cod_banca A, cod_banca_clien_for A"

if ls_flag_tipo_doc = "S" then
	ls_sort = ls_sort + ", cod_tipo_documento A"
end if

if ls_flag_destinazione = "S" then
	ls_sort = ls_sort + ", cod_destinazione A"
end if

if ls_flag_num_doc = "S" then
	ls_sort = ls_sort + ", cod_doc_esterno A"
end if

if ls_flag_imballo = "S" then
	ls_sort = ls_sort + ", cod_imballo A"
end if

if ls_flag_vettore = "S" then
	ls_sort = ls_sort + ", cod_vettore A"
end if

if ls_flag_inoltro = "S" then
	ls_sort = ls_sort + ", cod_inoltro A"
end if

if ls_flag_mezzo = "S" then
	ls_sort = ls_sort + ", cod_mezzo A"
end if

if ls_flag_porto = "S" then
	ls_sort = ls_sort + ", cod_porto A"
end if

if ls_flag_resa = "S" then
	ls_sort = ls_sort + ", cod_resa A"
end if

if ls_flag_data_consegna = "S" then
	ls_sort = ls_sort + ", data_consegna A"
end if

ls_sort = ls_sort + ", anno_documento A, num_documento A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""
ll_num_ordine_old = 0
for ll_i = 1 to lds_rag_documenti.rowcount()
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_cliente")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_cliente")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_tipo_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_destinazione = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_num_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_imballo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_imballo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_imballo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_vettore = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_vettore")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_vettore")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_inoltro = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_mezzo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_porto = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_porto")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_porto")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_resa = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_resa")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_resa")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_data_consegna = "S" and &
		not isnull(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")) then
		ls_key_sort = ls_key_sort + string(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna"))
	else
		ls_key_sort = ls_key_sort + "        "
	end if

	ll_anno_ordine = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_ordine = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ldt_data_consegna = lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")
	ls_cod_tipo_ord_ven = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tab_tipi_ord_ven.cod_tipo_bol_ven,
			 tab_tipi_ord_ven.cod_tipo_analisi
	into   :ls_cod_tipo_bol_ven,
			 :ls_cod_tipo_analisi_ord
	from   tab_tipi_ord_ven  
	where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita."
		return -1
	end if
	
	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie,
			 :ls_cod_natura_intra
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_ven.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Ordine Vendita."
		destroy lds_rag_documenti
		return -1
	end if
	
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)

	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Bolle."
		destroy lds_rag_documenti
		return -1
	end if
	
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if isnull(ls_nota_piede) then
		ls_nota_piede = ""
	else
//		ls_nota_piede = ls_nota_piede + "~r~n"
		ls_nota_piede = ls_nota_piede
	end if

	if not isnull(ls_num_ord_cliente) and ls_flag_abilita_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
//	if ls_flag_doc_suc_tes = 'I' then //nota di testata
//		select flag_doc_suc
//		  into :ls_flag_doc_suc_tes
//		  from tab_tipi_ord_ven
//		 where cod_azienda = :s_cs_xx.cod_azienda
//		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
//	end if		
//
//	if ls_flag_abilita_rif_ord_ven = "T" then
//		if ls_flag_doc_suc_tes = 'S' then
//			ls_nota_testata = "Rif. ns. Ordine Nr. " + string(ll_anno_ordine) + "/" + &
//									string(ll_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
//									ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata + &
//									"~r~n"			
//		elseif ls_flag_doc_suc_tes = 'N' then
//			ls_nota_testata = "Rif. ns. Ordine Nr. " + string(ll_anno_ordine) + "/" + &
//									string(ll_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
//									ls_vostro_ordine + ls_vostro_ordine_data + "~r~n"
//		end if	
//	else
//		if ls_flag_doc_suc_tes = 'N' then
//			ls_nota_testata = ""
//		end if			
//	end if	
//	
//	if ls_flag_doc_suc_pie = 'I' then //nota di piede
//		select flag_doc_suc
//		  into :ls_flag_doc_suc_pie
//		  from tab_tipi_ord_ven
//		 where cod_azienda = :s_cs_xx.cod_azienda
//		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
//	end if			
//	
//	if ls_flag_doc_suc_pie = 'N' then
//		ls_nota_piede = ""
//	end if				
//
//--------------------------------------- Fine Modifica --------------------------------------------------------

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------



	if ls_flag_abilita_rif_ord_ven = "T" then
		ls_nota_testata = "Rif. ns. Ordine Nr. " + string(ll_anno_ordine) + "/" + &
								string(ll_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata + &
								"~r~n"
	end if
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE

	
	if ls_key_sort <> ls_key_sort_old then
		select con_bol_ven.num_registrazione
		into   :ll_num_registrazione
		from   con_bol_ven
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la lettura della tabella Controllo Bolle Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	
		ll_num_registrazione ++
		
		update con_bol_ven
		set    con_bol_ven.num_registrazione = :ll_num_registrazione
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita."+ sqlca.sqlerrtext
			destroy lds_rag_documenti
			return -1
		end if
	
		ldt_data_registrazione = datetime(today())

	   ///  spostato da qui select tab_tipi_bol _ven
	
		if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
			ls_causale_trasporto = ls_causale_trasporto_tab
		end if
		if isnull(ls_cod_causale) or trim(ls_cod_causale) = "" then
			ls_cod_causale = ls_cod_causale_tab
		end if
	
		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Configurare il codice valuta per le Lire Italiane."
			destroy lds_rag_documenti
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
		
		//prima di fare la insert metti l'operatore collegato per il nuovo documento
		if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
			ls_cod_operatore = ls_cod_operatore_new
		end if
	
		insert into tes_bol_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_bol_ven,   
						 cod_cliente,
						 cod_fornitore,
						 cod_des_cliente,   
						 cod_fil_fornitore,
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_deposito_tras,
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_bolla,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_riep_fat,
						 cod_banca_clien_for,
						 flag_gen_fat,
						 cod_causale,
						 flag_doc_suc_tes,
						 flag_st_note_tes,
						 flag_doc_suc_pie,
						 flag_st_note_pie,
						 cod_natura_intra)  
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_bol_ven,   
						 :ls_cod_cliente[1],
						 null,
						 :ls_cod_des_cliente,   
						 null,
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 null,
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 '',   
						 '',   
						 'N',
						 'N',   
						 'N',
						 :ls_flag_riep_fat,
						 :ls_cod_banca_clien_for,
						 'N',
						 :ls_cod_causale,
						 'I',
						 'I',
						 'I',
						 'I',
						 :ls_cod_natura_intra);
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura Testata Bolle Vendita."+ sqlca.sqlerrtext
			destroy lds_rag_documenti
			return -1
		end if
		
		if wf_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

		ll_prog_riga_bol_ven = 0
	end if

	declare cu_evas_ord_ven cursor for  
	select   evas_ord_ven.prog_riga_ord_ven,   
			   evas_ord_ven.prog_riga,   
			   evas_ord_ven.cod_deposito,   
			   evas_ord_ven.cod_ubicazione,   
			   evas_ord_ven.cod_lotto,   
			   evas_ord_ven.data_stock,   
			   evas_ord_ven.prog_stock,   
			   evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :ll_anno_ordine and
				evas_ord_ven.num_registrazione = :ll_num_ordine 
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	lb_riferimento = true
	open cu_evas_ord_ven;
	
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Evasione Ordini Vendita."
			close cu_evas_ord_ven;
			destroy lds_rag_documenti
			return -1
		end if

		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.data_consegna,
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ldt_data_consegna_ordine,
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :ll_anno_ordine and  
					det_ord_ven.num_registrazione = :ll_num_ordine and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita."
			close cu_evas_ord_ven;
			destroy lds_rag_documenti
			return -1
		end if


//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if

//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = "I" then //nota dettaglio
			select flag_doc_suc_or
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		if ls_flag_doc_suc_det = 'N' then
			setnull(ls_nota_dettaglio)
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		
		
		if (ls_flag_data_consegna = "S" and &
			 (ldt_data_consegna = ldt_data_consegna_ordine) or &
			  isnull(ldt_data_consegna) and isnull(ldt_data_consegna_ordine)) or &
			ls_flag_data_consegna = "N" then
			select tab_tipi_det_ven.flag_tipo_det_ven,
					 tab_tipi_det_ven.cod_tipo_movimento,
					 tab_tipi_det_ven.flag_sola_iva
			into   :ls_flag_tipo_det_ven,
					 :ls_cod_tipo_movimento,
					 :ls_flag_sola_iva
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
					ls_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
	
				if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if
			
			if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
				lb_riferimento = false
				setnull(ls_note_dettaglio_rif)
				ls_des_riferimento = ls_des_riferimento_ord_ven
				if ls_flag_rif_anno_ord_ven = "S" and not isnull(ll_anno_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" +string(ll_anno_ordine)
				end if
				if ls_flag_rif_num_ord_ven = "S" and not isnull(ll_num_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(ll_num_ordine)
				end if
				if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
				end if
				if not isnull(ls_num_ord_cliente) then
					ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
					if not isnull(ldt_data_ord_cliente) then
						ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
					end if
				end if
									
				if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
					g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
				end if
				
				insert into det_bol_ven  
								(cod_azienda,   
								 anno_registrazione,   
								 num_registrazione,   
								 prog_riga_bol_ven,   
								 cod_tipo_det_ven,   
								 cod_deposito,
								 cod_ubicazione,
								 cod_lotto,
								 progr_stock,
								 data_stock,
								 cod_misura,   
								 cod_prodotto,   
								 des_prodotto,   
								 quan_consegnata,   
								 prezzo_vendita,   
								 fat_conversione_ven,   
								 sconto_1,   
								 sconto_2,   
								 provvigione_1,
								 provvigione_2,
								 cod_iva,   
								 cod_tipo_movimento,
								 num_registrazione_mov_mag,
								 nota_dettaglio,   
								 anno_registrazione_ord_ven,   
								 num_registrazione_ord_ven,   
								 prog_riga_ord_ven,
								 anno_commessa,
								 num_commessa,
								 cod_centro_costo,
								 sconto_3,   
								 sconto_4,   
								 sconto_5,   
								 sconto_6,   
								 sconto_7,   
								 sconto_8,   
								 sconto_9,   
								 sconto_10,
								 anno_registrazione_mov_mag,
								 anno_reg_bol_acq,
								 num_reg_bol_acq,
								 prog_riga_bol_acq,
								 anno_reg_des_mov,
								 num_reg_des_mov,
								 cod_versione,
								 num_confezioni,
								 num_pezzi_confezione,
								 flag_doc_suc_det,
								 flag_st_note_det,
								 quantita_um,
								 prezzo_um)
			  values			(:s_cs_xx.cod_azienda,   
								 :ll_anno_registrazione,   
								 :ll_num_registrazione,   
								 :ll_prog_riga_bol_ven,   
								 :ls_cod_tipo_det_ven_rif_ord_ven,   
								 null,
								 null,
								 null,
								 null,
								 null,
								 null,   
								 null,   
								 :ls_des_riferimento,   
								 0,   
								 0,   
								 1,   
								 0,   
								 0,  
								 0,
								 0,
								 :ls_cod_iva_rif,   
								 null,
								 null,
								 :ls_note_dettaglio_rif,   
								 null,   
								 null,   
								 null,
								 null,   
								 null,   
								 null,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,
								 null,
								 null,
								 null,
								 null,
								 null,
								 null,
								 null,
								 0,
								 0,
								 'I',
								 'I',
								 0,
								 0);
		
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita."+ sqlca.sqlerrtext
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
				ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
			end if	
	
	
			insert into det_bol_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_bol_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 progr_stock,
							 data_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_consegnata,   
							 prezzo_vendita,   
							 fat_conversione_ven,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 nota_dettaglio,   
							 anno_registrazione_ord_ven,   
							 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 anno_reg_bol_acq,
							 num_reg_bol_acq,
							 prog_riga_bol_acq,
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven,   
							 :ls_cod_deposito[1],
							 :ls_cod_ubicazione[1],
							 :ls_cod_lotto[1],
							 :ll_progr_stock[1],
							 :ldt_data_stock[1],
							 :ls_cod_misura,   
							 :ls_cod_prodotto,   
							 :ls_des_prodotto,   
							 :ld_quan_in_evasione,   
							 :ld_prezzo_vendita,   
							 :ld_fat_conversione_ven,   
							 :ld_sconto_1,   
							 :ld_sconto_2,  
							 :ld_provvigione_1,
							 :ld_provvigione_2,
							 :ls_cod_iva,   
							 :ls_cod_tipo_movimento,
							 null,
							 :ls_nota_dettaglio,   
							 :ll_anno_ordine,   
							 :ll_num_ordine,   
							 :ll_prog_riga_ord_ven,
							 :ll_anno_commessa,   
							 :ll_num_commessa,   
							 :ls_cod_centro_costo,   
							 :ld_sconto_3,   
							 :ld_sconto_4,   
							 :ld_sconto_5,   
							 :ld_sconto_6,   
							 :ld_sconto_7,   
							 :ld_sconto_8,   
							 :ld_sconto_9,   
							 :ld_sconto_10,
							 null,
							 null,
							 null,
							 null,
							 :ll_anno_reg_des_mov,
							 :ll_num_reg_des_mov,
							 :ls_cod_versione,
							 :ll_num_confezioni,
							 :ll_num_pezzi_confezione,
							 'I',
							 'I',
							 :ld_quantita_um,
							 :ld_prezzo_um);							 
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita."+ sqlca.sqlerrtext
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				update anag_prodotti  
					set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
						 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
				
				update stock 
				set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
						 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
				where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
						 stock.cod_prodotto = :ls_cod_prodotto and 
						 stock.cod_deposito = :ls_cod_deposito[1] and 
						 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
						 stock.cod_lotto = :ls_cod_lotto[1] and 
						 stock.data_stock = :ldt_data_stock[1] and 
						 stock.prog_stock = :ll_progr_stock[1];
	
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Si è verificato un errore in fase di aggiornamento stock."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if				
	
			ls_tabella_orig = "det_ord_ven_stat"
			ls_tabella_des = "det_bol_ven_stat"
			ls_prog_riga_doc_orig = "prog_riga_ord_ven"
			ls_prog_riga_doc_des = "prog_riga_bol_ven"
	
			if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven, al_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if	
	
			if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
				if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, al_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
					ls_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if
	
			if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
				ls_flag_evasione = "E"
			else
				ls_flag_evasione = "P"
			end if
			
			update det_ord_ven  
				set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
					 quan_evasa = quan_evasa + :ld_quan_in_evasione,
					 flag_evasione = :ls_flag_evasione
			 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_ven.anno_registrazione = :ll_anno_ordine and  
					 det_ord_ven.num_registrazione = :ll_num_ordine and
					 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_sola_iva = 'N' then
				ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
				ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
				ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
				ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
				ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
				ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
				ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
				ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
				ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
				ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
				ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
				ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
				ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
				ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
				ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
				ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
				ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
				ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
				ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
				ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
		
				if ls_cod_valuta = ls_lire then
					ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
				else
					ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
				end if   
		
				if ls_flag_tipo_det_ven = "M" or &
					ls_flag_tipo_det_ven = "C" or &
					ls_flag_tipo_det_ven = "N" then
					ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
					ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
					ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
					ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
				elseif ls_flag_tipo_det_ven = "S" then
					ld_val_evaso = ld_val_riga_sconto_10 * -1
				else
					ld_val_evaso = ld_val_riga_sconto_10
				end if
				ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
			end if
	
			if ls_cod_valuta = ls_lire then
				ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
			end if   
	
			delete from evas_ord_ven 
			where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							evas_ord_ven.anno_registrazione = :ll_anno_ordine and
							evas_ord_ven.num_registrazione = :ll_num_ordine and 
							evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
							evas_ord_ven.prog_riga = :ll_prog_riga;
	
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
		end if
	loop
	close cu_evas_ord_ven;

// ------------------------------  NUOVO SUSTEMA DI CALCOLO STATO ORDINE ---------------------------------------
	uo_generazione_documenti luo_generazione_documenti
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.uof_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine)
	DESTROY luo_generazione_documenti
//	if ld_tot_val_ordine <= ld_tot_val_evaso then
//		ls_flag_evasione = "E"
//	else
//		ls_flag_evasione = "P"
//	end if
//
//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso,
//			 flag_evasione = :ls_flag_evasione
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
//			 tes_ord_ven.num_registrazione = :ll_num_ordine;
//
//	if sqlca.sqlcode = -1 then
//		ls_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Ordine Vendita."
//		destroy lds_rag_documenti
//		return -1
//	end if

	if (string(ll_num_ordine) + ls_key_sort) <> (string(ll_num_ordine_old) + ls_key_sort_old) then
		
		select nota_testata, nota_piede
			into :ls_nota_testata_1, :ls_nota_piede_1
		from tes_bol_ven
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :al_anno_registrazione and  
				 num_registrazione = :ll_num_registrazione;
				 
		if isnull(ls_nota_testata_1) or len(ls_nota_testata_1) < 1 then
			ls_nota_testata_1 = trim(ls_nota_testata)
		else
			ls_nota_testata_1 = ls_nota_testata_1 + "~r~n" + trim(ls_nota_testata)
		end if			
		
		if isnull(ls_nota_piede_1) or len(ls_nota_piede_1) < 1 then
			ls_nota_piede_1 = trim(ls_nota_piede)
		else
			ls_nota_piede_1 = ls_nota_piede_1+ "~r~n" + trim(ls_nota_piede)
		end if
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE

		update tes_bol_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_bol_ven.anno_registrazione = :al_anno_registrazione and  
				 tes_bol_ven.num_registrazione = :ll_num_registrazione;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Bolla Vendita."
			destroy lds_rag_documenti
			return -1
		end if
		
		luo_calcolo = create uo_calcola_documento_euro

		if luo_calcolo.uof_calcola_documento(al_anno_registrazione,ll_num_registrazione,"bol_ven",ls_messaggio) <> 0 then
			destroy luo_calcolo
			return -1
		end if
		
		destroy luo_calcolo

		dw_gen_orig_dest.insertrow(0)
		dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", ll_anno_ordine)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig", ll_num_ordine)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", al_anno_registrazione)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest", ll_num_registrazione)
	end if
	
	ls_key_sort_old = ls_key_sort
	ll_num_ordine_old = ll_num_ordine
	
	
	// ************************************************* michela 30/04/2004
	// *** specifica passaggio_ordini progetto tenda: se nell'ordine esistono i campi per stampare 
	// *** riferimento interscambio e rif vs ordine allora vuol dire che siamo in progettotenda.
	// *** quindi se nell'apertura del cursore c'è errore, vuol dire che i campi non esistono e che 
	// *** quindi non siamo in progetto tenda...quindi si continua con il for.
	long ll_ret
	
	ll_ret = wf_progetto_tenda(ll_anno_ordine, ll_num_ordine, al_anno_registrazione, ll_num_registrazione)
	if ll_ret < 0 then
		destroy lds_rag_documenti
		return -1		
	end if
	// **************************************************
	// **************************************************
	
	
	
next

destroy lds_rag_documenti

return 0
end function

public function integer wf_genera_bolle_singole_1 (long al_anno_registrazione, string as_cod_tipo_ord_ven, long al_anno_ordine, long al_num_ordine, ref string ls_messaggio);boolean lb_riferimento
long ll_num_registrazione, &
	  ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_num_confezioni, ll_num_pezzi_confezione, ll_anno_registrazione
string ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, &
		 ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, &
		 ls_flag_abilita_rif_ord_ven,  ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, &
  		 ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, &
		 ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
		 ls_db, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, &
		 ls_cod_tipo_ord_ven, ls_cod_natura_intra
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		   ldt_vostro_ordine_data
decimal ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta
string ls_cod_operatore_new

uo_calcola_documento_euro luo_calcolo

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

ll_anno_registrazione = f_anno_esercizio()
lb_riferimento = true
setnull(ls_cod_fornitore[1])

select tab_tipi_ord_ven.cod_tipo_bol_ven,  
		 tab_tipi_ord_ven.cod_tipo_analisi  
into   :ls_cod_tipo_bol_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_ord_ven.cod_tipo_ord_ven = :as_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	ls_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita."
	return -1
end if

if not isnull(ls_cod_tipo_bol_ven) then
	select con_bol_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_bol_ven
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Controllo Bolle Vendita."
		return -1
	end if

	ll_num_registrazione ++
	
	update con_bol_ven
	set    con_bol_ven.num_registrazione = :ll_num_registrazione
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita."+ sqlca.sqlerrtext
		return -1
	end if

	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes,
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_tipo_ord_ven,
			 tes_ord_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes,
			 :ls_flag_doc_suc_pie,
			 :ls_cod_tipo_ord_ven,
			 :ls_cod_natura_intra
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :al_anno_ordine and  
			 tes_ord_ven.num_registrazione = :al_num_ordine;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Ordine Vendita."
		return -1
	end if
	
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)

	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Bolle."
		return -1
	end if


	ldt_data_registrazione = datetime(today())



//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------

//	if isnull(ls_nota_testata) then
//		ls_nota_testata = ""
//	end if

	if not isnull(ls_num_ord_cliente) and ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_nota_testata = ls_des_riferimento_ord_ven + string(al_anno_ordine) + "/" + &
								string(al_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE

	
	
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	//prima di fare la insert metti l'operatore collegato per il nuovo documento
	if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
		ls_cod_operatore = ls_cod_operatore_new
	end if

	insert into tes_bol_ven
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_tipo_bol_ven,   
					 cod_cliente,
					 cod_fornitore,
					 cod_des_cliente,   
					 cod_fil_fornitore,
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,   
					 cod_deposito,   
					 cod_deposito_tras,
					 cod_ubicazione,
					 cod_valuta,   
					 cambio_ven,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_agente_1,
					 cod_agente_2,
					 cod_banca,   
					 num_ord_cliente,   
					 data_ord_cliente,   
					 cod_imballo,   
					 aspetto_beni,
					 peso_netto,
					 peso_lordo,
					 num_colli,
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,
					 causale_trasporto,
					 cod_porto,   
					 cod_resa,   
					 cod_documento,
					 numeratore_documento,
					 anno_documento,
					 num_documento,
					 data_bolla,
					 nota_testata,   
					 nota_piede,   
					 flag_fuori_fido,
					 flag_blocco,   
					 flag_movimenti,  
					 flag_riep_fat,
					 cod_banca_clien_for,
					 flag_gen_fat,
					 cod_causale,
					 flag_doc_suc_tes,
					 flag_st_note_tes,
					 flag_doc_suc_pie,
					 flag_st_note_pie,
					 cod_natura_intra)  					 
	values      (:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ldt_data_registrazione,   
					 :ls_cod_operatore,   
					 :ls_cod_tipo_bol_ven,   
					 :ls_cod_cliente[1],
					 null,
					 :ls_cod_des_cliente,   
					 null,
					 :ls_rag_soc_1,   
					 :ls_rag_soc_2,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,   
					 :ls_cod_deposito[1],   
					 null,
					 :ls_cod_ubicazione[1],
					 :ls_cod_valuta,   
					 :ld_cambio_ven,   
					 :ls_cod_tipo_listino_prodotto,   
					 :ls_cod_pagamento,   
					 :ld_sconto_testata,
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_num_ord_cliente,   
					 :ldt_data_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_aspetto_beni,
					 :ld_peso_netto,
					 :ld_peso_lordo,
					 :ld_num_colli,
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_causale_trasporto,
					 :ls_cod_porto,   
					 :ls_cod_resa,   
					 null,
					 null,
					 0,
					 0,
					 null,
					 :ls_nota_testata,   
					 :ls_nota_piede,   
					 'N',
					 'N',   
					 'N',
					 :ls_flag_riep_fat,
					 :ls_cod_banca_clien_for,
					 'N',
					 :ls_cod_causale,
					 'I',
					 'I',
					 'I',
					 'I',
					 :ls_cod_natura_intra);

	 if sqlca.sqlcode <> 0 then
		 ls_messaggio = "Errore durante la scrittura Testata Bolle Vendita."+ sqlca.sqlerrtext
		 return -1
	 end if

	 if wf_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :al_anno_ordine and
				evas_ord_ven.num_registrazione = :al_num_ordine 
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	lb_riferimento = true
	ll_prog_riga_bol_ven = 0
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Evasione Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :al_anno_ordine and  
					det_ord_ven.num_registrazione = :al_num_ordine and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_or
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			ls_nota_dettaglio = ""
		end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------			
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				ls_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(al_anno_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(al_anno_ordine)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(al_num_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(al_num_ordine)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			if not isnull(ls_num_ord_cliente) then
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
									
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_bol_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_bol_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 progr_stock,
							 data_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_consegnata,   
							 prezzo_vendita,   
							 fat_conversione_ven,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 nota_dettaglio,   
							 anno_registrazione_ord_ven,   
							 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 anno_reg_bol_acq,
							 num_reg_bol_acq,
							 prog_riga_bol_acq,
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven_rif_ord_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 1,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,   
							 null,   
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 0,
							 0,
							 'I',
							 'I',
							 0,
							 0);
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita."+ sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if	

		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ll_progr_stock[1],
						 :ldt_data_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_fat_conversione_ven,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_nota_dettaglio,   
						 :al_anno_ordine,   
						 :al_num_ordine,   
						 :ll_prog_riga_ord_ven,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 null,
						 null,
						 null,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :ls_cod_versione,
						 :ll_num_confezioni,
						 :ll_num_pezzi_confezione,
						 'I',
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita."+ sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino."
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento stock."
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_bol_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_bol_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, al_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, al_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
				ls_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :al_anno_ordine and  
				 det_ord_ven.num_registrazione = :al_num_ordine and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :al_anno_ordine and
						evas_ord_ven.num_registrazione = :al_num_ordine and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga;

		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

// ------------------------------  NUOVO SUSTEMA DI CALCOLO STATO ORDINE ---------------------------------------
	uo_generazione_documenti luo_generazione_documenti
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.uof_calcola_stato_ordine(al_anno_ordine, al_num_ordine)
	DESTROY luo_generazione_documenti
//	if ld_tot_val_ordine <= ld_tot_val_evaso then
//		ls_flag_evasione = "E"
//	else
//		ls_flag_evasione = "P"
//	end if
//
//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso,
//			 flag_evasione = :ls_flag_evasione
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :al_anno_ordine and  
//			 tes_ord_ven.num_registrazione = :al_num_ordine;
//
//	if sqlca.sqlcode = -1 then
//		ls_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Ordine Vendita."
//		return -1
//	end if

	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(al_anno_registrazione,ll_num_registrazione,"bol_ven",ls_messaggio) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo

	dw_gen_orig_dest.insertrow(0)
	dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", al_anno_ordine)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig", al_num_ordine)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", al_anno_registrazione)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest", ll_num_registrazione)
	
	// ************************************************* michela 30/04/2004
	// *** specifica passaggio_ordini progetto tenda: se nell'ordine esistono i campi per stampare 
	// *** riferimento interscambio e rif vs ordine allora vuol dire che siamo in progettotenda.
	// *** quindi se nell'apertura del cursore c'è errore, vuol dire che i campi non esistono e che 
	// *** quindi non siamo in progetto tenda...quindi si continua con il for.
	long ll_ret
	
	ll_ret = wf_progetto_tenda(al_anno_ordine, al_num_ordine, ll_anno_registrazione, ll_num_registrazione)
	if ll_ret < 0 then
		return -1		
	end if
	// **************************************************
	// **************************************************

end if
return 0
end function

public function integer wf_genera_fatture_riepilogate (long al_anno_registrazione, ref string ls_messaggio);datastore lds_rag_documenti
boolean lb_riferimento
long ll_i, ll_selected[], ll_anno_ordine, ll_num_ordine, ll_prog_riga_documento, &
	  ll_num_registrazione, ll_prog_riga_fat_ven, ll_prog_riga_ord_ven, ll_prog_riga, &
	  ll_progr_stock[1], ll_anno_commessa, ll_num_commessa, ll_anno_reg_des_mov, &
	  ll_num_reg_des_mov, ll_num_ordine_old, ll_num_confezioni, ll_num_pezzi_confezione, &
	  ll_anno_registrazione
string ls_cod_tipo_ord_ven, ls_flag_riep_fat, ls_cod_tipo_fat_ven, ls_cod_cliente[1], &
		 ls_cod_valuta, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, &
		 ls_cod_banca_clien_for, ls_cod_des_cliente, ls_num_ord_cliente, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_flag_tipo_doc, ls_flag_destinazione, ls_flag_num_doc, &
		 ls_flag_imballo, ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, &
		 ls_flag_resa, ls_flag_data_consegna, ls_cod_tipo_rag, ls_sort, ls_key_sort, &
		 ls_key_sort_old, ls_cod_tipo_analisi_fat, ls_causale_trasporto_tab, ls_lire, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_tipo_listino_prodotto, ls_aspetto_beni, ls_causale_trasporto, ls_nota_testata, &
		 ls_nota_piede, ls_vostro_ordine, ls_vostro_ordine_data, &
		 ls_cod_deposito[1], ls_cod_ubicazione[1], ls_cod_lotto[1], ls_cod_tipo_det_ven, &
		 ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio,  &
		 ls_cod_centro_costo, ls_flag_tipo_det_ven, ls_cod_tipo_movimento, ls_flag_sola_iva, &
		 ls_cod_fornitore[1], ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_cod_tipo_analisi_ord, ls_flag_evasione,ls_cod_versione, &
		 ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_fat_ven, &
		 ls_flag_rif_cod_documento, ls_flag_rif_numeratore_doc, ls_flag_rif_anno_documento, &
		 ls_flag_rif_num_documento, ls_flag_rif_data_fattura, ls_cod_tipo_analisi, &
		 ls_des_riferimento_ord, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, &
		 ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, ls_note_dettaglio_rif, &
		 ls_db, ls_nota_testata_1, ls_nota_piede_1, &
		 ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det
datetime ldt_data_consegna, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_registrazione, &
	  		ldt_data_stock[1], ldt_data_consegna_ordine
double ld_sconto_pagamento, ld_cambio_ven, ld_sconto_testata, ld_peso_netto, ld_peso_lordo, &
		 ld_num_colli, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_quan_in_evasione, ld_quan_ordine, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_provvigione_1, ld_provvigione_2, ld_quan_evasa, &
		 ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
		 ld_sconto_9, ld_sconto_10, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta
string ls_cod_operatore_new, ls_cod_natura_intra

uo_calcola_documento_euro luo_calcolo

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)


ll_anno_registrazione = f_anno_esercizio()
dw_gen_bol_ven_fat_imm.get_selected_rows(ll_selected[])
lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_documenti"
lds_rag_documenti.settransobject(sqlca)
ls_cod_tipo_rag = f_po_selectddlb(ddlb_cod_tipo_rag)
if isnull(ls_cod_tipo_rag) or len(ls_cod_tipo_rag) < 1 then
	g_mb.messagebox("APICE","Indicare il tipo raggrupamento da utilizzare")
	return 0
end if

for ll_i = 1 to upperbound(ll_selected)
	ls_cod_tipo_ord_ven = dw_gen_bol_ven_fat_imm.getitemstring(ll_selected[ll_i], "tes_ord_ven_cod_tipo_ord_ven")
	ll_anno_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_anno_registrazione")
	ll_num_ordine = dw_gen_bol_ven_fat_imm.getitemnumber(ll_selected[ll_i], "evas_ord_ven_num_registrazione")

	select tes_ord_ven.flag_riep_fat
	into   :ls_flag_riep_fat
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and 
			 tes_ord_ven.num_registrazione = :ll_num_ordine;
			 
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della Testata Ordini Vendita"
		return -1
	end if

	if ls_flag_riep_fat = 'N' then
		if wf_genera_fatture_singole_1(al_anno_registrazione, ls_cod_tipo_ord_ven, ll_anno_ordine, ll_num_ordine, ls_messaggio) = -1 then
			return -1
		end if
	else
		select tab_tipi_ord_ven.cod_tipo_fat_ven
		into   :ls_cod_tipo_fat_ven
		from   tab_tipi_ord_ven  
		where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita."
			return -1
		end if
		
		if not isnull(ls_cod_tipo_fat_ven) then
			if not isnull(ls_cod_tipo_rag) and trim(ls_cod_tipo_rag) <> "" then
				select tab_tipi_rag.flag_tipo_doc,   
						 tab_tipi_rag.flag_destinazione,   
						 tab_tipi_rag.flag_num_doc,   
						 tab_tipi_rag.flag_imballo,   
						 tab_tipi_rag.flag_vettore,   
						 tab_tipi_rag.flag_inoltro,   
						 tab_tipi_rag.flag_mezzo,   
						 tab_tipi_rag.flag_porto,   
						 tab_tipi_rag.flag_resa,   
						 tab_tipi_rag.flag_data_consegna  
				into   :ls_flag_tipo_doc,   
						 :ls_flag_destinazione,   
						 :ls_flag_num_doc,   
						 :ls_flag_imballo,   
						 :ls_flag_vettore,   
						 :ls_flag_inoltro,   
						 :ls_flag_mezzo,   
						 :ls_flag_porto,   
						 :ls_flag_resa,   
						 :ls_flag_data_consegna  
				from   tab_tipi_rag  
				where  tab_tipi_rag.cod_azienda = :s_cs_xx.cod_azienda and
						 tab_tipi_rag.cod_tipo_rag = :ls_cod_tipo_rag;
	
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Errore durante la lettura Tabella Tipi Ragruppamenti."
					destroy lds_rag_documenti
					return -1
				end if
			end if
			
			select tes_ord_ven.cod_cliente,   
					 tes_ord_ven.cod_valuta,   
					 tes_ord_ven.cod_pagamento,   
					 tes_ord_ven.cod_agente_1,
					 tes_ord_ven.cod_agente_2,
					 tes_ord_ven.cod_banca,   
					 tes_ord_ven.cod_banca_clien_for,
					 tes_ord_ven.cod_tipo_ord_ven,
					 tes_ord_ven.cod_des_cliente,   
					 tes_ord_ven.num_ord_cliente,   
					 tes_ord_ven.cod_imballo,   
					 tes_ord_ven.cod_vettore,   
					 tes_ord_ven.cod_inoltro,   
					 tes_ord_ven.cod_mezzo,
					 tes_ord_ven.cod_porto,   
					 tes_ord_ven.cod_resa
			 into  :ls_cod_cliente[1],
					 :ls_cod_valuta,   
					 :ls_cod_pagamento,   
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_cod_banca_clien_for,
					 :ls_cod_tipo_ord_ven,
					 :ls_cod_des_cliente,   
					 :ls_num_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_cod_porto,
					 :ls_cod_resa
			from   tes_ord_ven  
			where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
					 tes_ord_ven.num_registrazione = :ll_num_ordine;
		
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la lettura Testata Ordine Vendita."
				destroy lds_rag_documenti
				return -1
			end if

			if ls_flag_data_consegna = "S" then
				declare cu_det_ord_ven cursor for  
				select   det_ord_ven.prog_riga_ord_ven,
							det_ord_ven.data_consegna
				from     det_ord_ven  
				where    det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							det_ord_ven.anno_registrazione = :ll_anno_ordine and
							det_ord_ven.num_registrazione = :ll_num_ordine
				order by det_ord_ven.data_consegna asc;

				open cu_det_ord_ven;
				
				do while 0 = 0
					fetch cu_det_ord_ven into :ll_prog_riga_documento, :ldt_data_consegna;
			
					if sqlca.sqlcode = 100 then exit
					
					if sqlca.sqlcode <> 0 then
						ls_messaggio = "Errore durante la lettura Dettagli Ordine di Vendita."
						destroy lds_rag_documenti
						close cu_det_ord_ven;
						return -1
					end if

					lds_rag_documenti.insertrow(0)
					lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_documento)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_cliente", ls_cod_cliente[1])
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_1", ls_cod_agente_1)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_2", ls_cod_agente_2)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_ven)
//					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_doc_esterno", ls_num_ord_cliente)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "data_consegna", ldt_data_consegna)
				loop
				close cu_det_ord_ven;
			else
				lds_rag_documenti.insertrow(0)
				lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_ordine)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_ordine)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_cliente", ls_cod_cliente[1])
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_1", ls_cod_agente_1)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_2", ls_cod_agente_2)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_ven)
//				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_doc_esterno", ls_num_ord_cliente)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
			end if
		end if
	end if
next

ls_sort = "cod_cliente A, cod_valuta A, cod_pagamento A, cod_agente_1 A, cod_agente_2 A, cod_banca A, cod_banca_clien_for A"

if ls_flag_tipo_doc = "S" then
	ls_sort = ls_sort + ", cod_tipo_documento A"
end if

if ls_flag_destinazione = "S" then
	ls_sort = ls_sort + ", cod_destinazione A"
end if

if ls_flag_num_doc = "S" then
	ls_sort = ls_sort + ", cod_doc_esterno A"
end if

if ls_flag_imballo = "S" then
	ls_sort = ls_sort + ", cod_imballo A"
end if

if ls_flag_vettore = "S" then
	ls_sort = ls_sort + ", cod_vettore A"
end if

if ls_flag_inoltro = "S" then
	ls_sort = ls_sort + ", cod_inoltro A"
end if

if ls_flag_mezzo = "S" then
	ls_sort = ls_sort + ", cod_mezzo A"
end if

if ls_flag_porto = "S" then
	ls_sort = ls_sort + ", cod_porto A"
end if

if ls_flag_resa = "S" then
	ls_sort = ls_sort + ", cod_resa A"
end if

if ls_flag_data_consegna = "S" then
	ls_sort = ls_sort + ", data_consegna A"
end if

ls_sort = ls_sort + ", anno_documento A, num_documento A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""
ll_num_ordine_old = 0
for ll_i = 1 to lds_rag_documenti.rowcount()
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_cliente")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_cliente")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_tipo_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_destinazione = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_num_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_imballo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_imballo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_imballo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_vettore = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_vettore")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_vettore")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_inoltro = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_mezzo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_porto = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_porto")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_porto")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_resa = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_resa")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_resa")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_data_consegna = "S" and &
		not isnull(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")) then
		ls_key_sort = ls_key_sort + string(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna"))
	else
		ls_key_sort = ls_key_sort + "        "
	end if

	ll_anno_ordine = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_ordine = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ldt_data_consegna = lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")
	ls_cod_tipo_ord_ven = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tab_tipi_ord_ven.cod_tipo_fat_ven,
			 tab_tipi_ord_ven.cod_tipo_analisi
	into   :ls_cod_tipo_fat_ven,
			 :ls_cod_tipo_analisi_ord
	from   tab_tipi_ord_ven  
	where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita."
		return -1
	end if
	
	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.flag_doc_suc_tes,
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_flag_doc_suc_tes,
			 :ls_flag_doc_suc_pie,
			 :ls_cod_natura_intra
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_ven.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Ordine Vendita."
		destroy lds_rag_documenti
		return -1
	end if

	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_fat_ven,
			 tab_tipi_fat_ven.flag_rif_cod_documento,
			 tab_tipi_fat_ven.flag_rif_numeratore_doc,
			 tab_tipi_fat_ven.flag_rif_anno_documento,
			 tab_tipi_fat_ven.flag_rif_num_documento,
			 tab_tipi_fat_ven.flag_rif_data_fattura,
			 tab_tipi_fat_ven.des_riferimento_ord_ven,
			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
			 tab_tipi_fat_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi,
			 :ls_causale_trasporto_tab,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_fat_ven,
			 :ls_flag_rif_cod_documento,
			 :ls_flag_rif_numeratore_doc,
			 :ls_flag_rif_anno_documento,
			 :ls_flag_rif_num_documento,
			 :ls_flag_rif_data_fattura,
			 :ls_des_riferimento_ord,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Fatture."
		destroy lds_rag_documenti
		return -1
	end if
	
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if isnull(ls_nota_piede) then
		ls_nota_piede = ""
	else
		ls_nota_piede = ls_nota_piede
	end if

	if not isnull(ls_num_ord_cliente)  and ls_flag_abilita_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = "I" then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = "N" then
		ls_nota_testata = ""
	end if			
	
	if ls_flag_doc_suc_pie = "I" then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = "N" then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------

   if ls_flag_abilita_rif_bol_ven = "T" then
		ls_nota_testata = ls_des_riferimento_fat_ven + string(ll_anno_ordine) + "/" + &
								string(ll_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata + &
								"~r~n"
   end if	
	
	
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE


	if ls_key_sort <> ls_key_sort_old then
		select con_fat_ven.num_registrazione
		into   :ll_num_registrazione
		from   con_fat_ven
		where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la lettura della tabella Controllo Fatture Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	
		ll_num_registrazione ++
		
		update con_fat_ven
		set    con_fat_ven.num_registrazione = :ll_num_registrazione
		where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita."+ sqlca.sqlerrtext
			destroy lds_rag_documenti
			return -1
		end if
	
		ldt_data_registrazione = datetime(today())
	
		// spostato da qui la select tab_tipi_fat_ven	
		if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
			ls_causale_trasporto = ls_causale_trasporto_tab
		end if
	
		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Configurare il codice valuta per le Lire Italiane."
			destroy lds_rag_documenti
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
	
		//prima di fare la insert metti l'operatore collegato per il nuovo documento
		if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
			ls_cod_operatore = ls_cod_operatore_new
		end if
	
		insert into tes_fat_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_fat_ven,   
						 cod_cliente,
						 cod_des_cliente,   
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_fattura,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_contabilita,
						 tot_merci,
						 tot_spese_trasporto,
						 tot_spese_imballo,
						 tot_spese_bolli,
						 tot_spese_varie,
						 tot_sconto_cassa,
						 tot_sconti_commerciali,
						 imponibile_provvigioni_1,
						 imponibile_provvigioni_2,
						 tot_provvigioni_1,
						 tot_provvigioni_2,
						 importo_iva,
						 imponibile_iva,
						 importo_iva_valuta,
						 imponibile_iva_valuta,
						 tot_fattura,
						 tot_fattura_valuta,
						 flag_cambio_zero,
						 cod_banca_clien_for,
						 flag_st_note_tes,
						 flag_st_note_pie,
						 cod_natura_intra)  						   
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_fat_ven,   
						 :ls_cod_cliente[1],
						 :ls_cod_des_cliente,   
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 '',   
						 '',   
						 'N',
						 'N',   
						 'N',
						 'N',
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 'N',
						 :ls_cod_banca_clien_for,
						 'I',
						 'I',
						 :ls_cod_natura_intra);
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura Testata Fatture Vendita."+ sqlca.sqlerrtext
			destroy lds_rag_documenti
			return -1
		end if

		if wf_crea_nota_fissa("FAT_VEN", ll_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

		ll_prog_riga_fat_ven = 0
	end if

	declare cu_evas_ord_ven cursor for  
	select   evas_ord_ven.prog_riga_ord_ven,   
			   evas_ord_ven.prog_riga,   
			   evas_ord_ven.cod_deposito,   
			   evas_ord_ven.cod_ubicazione,   
			   evas_ord_ven.cod_lotto,   
			   evas_ord_ven.data_stock,   
			   evas_ord_ven.prog_stock,   
			   evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :ll_anno_ordine and
				evas_ord_ven.num_registrazione = :ll_num_ordine 
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	lb_riferimento = true
	open cu_evas_ord_ven;
	
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Evasione Ordini Vendita."
			close cu_evas_ord_ven;
			destroy lds_rag_documenti
			return -1
		end if

		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.data_consegna,
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ldt_data_consegna_ordine,
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :ll_anno_ordine and  
					det_ord_ven.num_registrazione = :ll_num_ordine and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita."
			close cu_evas_ord_ven;
			destroy lds_rag_documenti
			return -1
		end if
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
		
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	if ls_flag_doc_suc_det = 'N' then
		ls_nota_dettaglio = ""
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
				
		if (ls_flag_data_consegna = "S" and &
			 (ldt_data_consegna = ldt_data_consegna_ordine) or &
			  isnull(ldt_data_consegna) and isnull(ldt_data_consegna_ordine)) or &
			ls_flag_data_consegna = "N" then
			select tab_tipi_det_ven.flag_tipo_det_ven,
					 tab_tipi_det_ven.cod_tipo_movimento,
					 tab_tipi_det_ven.flag_sola_iva
			into   :ls_flag_tipo_det_ven,
					 :ls_cod_tipo_movimento,
					 :ls_flag_sola_iva
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
					ls_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
	
				if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if

			if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
				lb_riferimento = false
				setnull(ls_note_dettaglio_rif)
				ls_des_riferimento = ls_des_riferimento_ord
				if ls_flag_rif_anno_ord_ven = "S" and not isnull(ll_anno_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" +string(ll_anno_ordine)
				end if
				if ls_flag_rif_num_ord_ven = "S" and not isnull(ll_num_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(ll_num_ordine)
				end if
				if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
				end if
				if not isnull(ls_num_ord_cliente) then
					ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
					if not isnull(ldt_data_ord_cliente) then
						ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
					end if
				end if
					
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Fattura Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
				
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 prezzo_um,
							 quantita_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven_rif_bol_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 1,
							 null,
							 null,
							 null,   
							 null,   
							 null,
							 null,
							 0,
							 0,
							 'I',
							 0,
							 0);
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Errore durante la scrittura Dettagli Fatture Vendita."+ sqlca.sqlerrtext
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
				ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
			end if	
	
	
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
                      num_confezioni,
					       num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven,   
							 :ls_cod_deposito[1],
							 :ls_cod_ubicazione[1],
							 :ls_cod_lotto[1],
							 :ldt_data_stock[1],
							 :ll_progr_stock[1],
							 :ls_cod_misura,   
							 :ls_cod_prodotto,   
							 :ls_des_prodotto,   
							 :ld_quan_in_evasione,   
							 :ld_prezzo_vendita,   
							 :ld_sconto_1,   
							 :ld_sconto_2,  
							 :ld_provvigione_1,
							 :ld_provvigione_2,
							 :ls_cod_iva,   
							 :ls_cod_tipo_movimento,
							 null,
							 null,
							 null,
							 :ls_nota_dettaglio,   
							 null,
							 null,
							 null,
							 :ll_anno_commessa,   
							 :ll_num_commessa,   
							 :ls_cod_centro_costo,   
							 :ld_sconto_3,   
							 :ld_sconto_4,   
							 :ld_sconto_5,   
							 :ld_sconto_6,   
							 :ld_sconto_7,   
							 :ld_sconto_8,   
							 :ld_sconto_9,   
							 :ld_sconto_10,
							 null,
							 :ld_fat_conversione_ven,
							 :ll_anno_reg_des_mov,
							 :ll_num_reg_des_mov,
							 :ll_anno_ordine,   
							 :ll_num_ordine,   
							 :ll_prog_riga_ord_ven,
							 :ls_cod_versione,
                      :ll_num_confezioni,
					       :ll_num_pezzi_confezione,
							 'I',
							 :ld_quantita_um,
							 :ld_prezzo_um);
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la scrittura Dettagli Fatture Vendita."+ sqlca.sqlerrtext
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				update anag_prodotti  
					set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
						 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
				
				update stock 
				set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
						 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
				where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
						 stock.cod_prodotto = :ls_cod_prodotto and 
						 stock.cod_deposito = :ls_cod_deposito[1] and 
						 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
						 stock.cod_lotto = :ls_cod_lotto[1] and 
						 stock.data_stock = :ldt_data_stock[1] and 
						 stock.prog_stock = :ll_progr_stock[1];
	
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Si è verificato un errore in fase di aggiornamento stock."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if				
	
			ls_tabella_orig = "det_ord_ven_stat"
			ls_tabella_des = "det_fat_ven_stat"
			ls_prog_riga_doc_orig = "prog_riga_ord_ven"
			ls_prog_riga_doc_des = "prog_riga_fat_ven"
	
			if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven, al_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if	
	
			if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
				if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, al_anno_registrazione, ll_prog_riga_fat_ven, 6) = -1 then
					ls_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if
	
			if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
				ls_flag_evasione = "E"
			else
				ls_flag_evasione = "P"
			end if
			
			update det_ord_ven  
				set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
					 quan_evasa = quan_evasa + :ld_quan_in_evasione,
					 flag_evasione = :ls_flag_evasione
			 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_ven.anno_registrazione = :ll_anno_ordine and  
					 det_ord_ven.num_registrazione = :ll_num_ordine and
					 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_sola_iva = 'N' then
				ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
				ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
				ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
				ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
				ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
				ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
				ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
				ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
				ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
				ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
				ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
				ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
				ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
				ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
				ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
				ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
				ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
				ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
				ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
				ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
		
				if ls_cod_valuta = ls_lire then
					ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
				else
					ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
				end if   
		
				if ls_flag_tipo_det_ven = "M" or &
					ls_flag_tipo_det_ven = "C" or &
					ls_flag_tipo_det_ven = "N" then
					ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
					ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
					ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
					ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
				elseif ls_flag_tipo_det_ven = "S" then
					ld_val_evaso = ld_val_riga_sconto_10 * -1
				else
					ld_val_evaso = ld_val_riga_sconto_10
				end if
				ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
			end if
	
			if ls_cod_valuta = ls_lire then
				ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
			end if   
	
			delete from evas_ord_ven 
			where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							evas_ord_ven.anno_registrazione = :ll_anno_ordine and
							evas_ord_ven.num_registrazione = :ll_num_ordine and 
							evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
							evas_ord_ven.prog_riga = :ll_prog_riga;
	
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
		end if
	loop
	close cu_evas_ord_ven;

// ------------------------------  NUOVO SUSTEMA DI CALCOLO STATO ORDINE ---------------------------------------
	uo_generazione_documenti luo_generazione_documenti
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.uof_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine)
	DESTROY luo_generazione_documenti
//	if ld_tot_val_ordine <= ld_tot_val_evaso then
//		ls_flag_evasione = "E"
//	else
//		ls_flag_evasione = "P"
//	end if
//
//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso,
//			 flag_evasione = :ls_flag_evasione
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
//			 tes_ord_ven.num_registrazione = :ll_num_ordine;
//
//	if sqlca.sqlcode = -1 then
//		ls_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Ordine Vendita."
//		destroy lds_rag_documenti
//		return -1
//	end if

	if (string(ll_num_ordine) + ls_key_sort) <> (string(ll_num_ordine_old) + ls_key_sort_old) then
		select nota_testata, nota_piede
			into :ls_nota_testata_1, :ls_nota_piede_1
		from tes_fat_ven
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :al_anno_registrazione and  
				 num_registrazione = :ll_num_registrazione;
				 
		if isnull(ls_nota_testata_1) or len(ls_nota_testata_1) < 1 then		 
			ls_nota_testata_1 = ls_nota_testata
		else
			ls_nota_testata_1 = ls_nota_testata_1  + "~r~n" + ls_nota_testata
		end if
		
		if isnull(ls_nota_piede_1) or len(ls_nota_piede_1) < 1 then		 
			ls_nota_piede_1 = ls_nota_piede
		else
			ls_nota_piede_1 = ls_nota_piede_1  + "~r~n" + ls_nota_piede
		end if
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE

		update tes_fat_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_ven.anno_registrazione = :al_anno_registrazione and  
				 tes_fat_ven.num_registrazione = :ll_num_registrazione;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Fattura Vendita."
			destroy lds_rag_documenti
			return -1
		end if
		
		luo_calcolo = create uo_calcola_documento_euro

		if luo_calcolo.uof_calcola_documento(al_anno_registrazione,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
			destroy luo_calcolo
			return -1
		end if
		
		destroy luo_calcolo
		
		dw_gen_orig_dest.insertrow(0)
		dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", ll_anno_ordine)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig", ll_num_ordine)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", al_anno_registrazione)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest", ll_num_registrazione)
	end if
	
	ls_key_sort_old = ls_key_sort
	ll_num_ordine_old = ll_num_ordine
next

destroy lds_rag_documenti

return 0
end function

public function integer wf_crea_nota_fissa (string fs_documento, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);string ls_cod_cliente, ls_cod_fornitore, ls_nota_testata, ls_nota_piede, ls_nota_video, &
		 ls_profilo_corrente, ls_db, ls_nota_testata_1, ls_nota_piede_1, ls_null
datetime ldt_data_registrazione
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
setnull(ls_null)

choose case fs_documento
	case "FAT_VEN"

		select cod_cliente, 
				 nota_testata,
				 nota_piede,
				 data_registrazione
		into   :ls_cod_cliente,
				 :ls_nota_testata,
				 :ls_nota_piede,
				 :ldt_data_registrazione
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca fattura per inserimento nota fissa: verificare"
			return -1
		end if
		
		setnull(ls_cod_fornitore)
		
		if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_FAT_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
		end if
//		f_crea_nota_fissa(ls_cod_cliente, ls_cod_fornitore, "GEN_FAT_VEN", ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_video)
		
		ls_nota_testata_1 = trim(ls_nota_testata)
		ls_nota_piede_1 = trim(ls_nota_piede)
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		update tes_fat_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_ven.anno_registrazione = :fl_anno_registrazione and  
				 tes_fat_ven.num_registrazione = :fl_num_registrazione;
				
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento note nella Testata Fattura Vendita."
			return -1
		end if

case "BOL_VEN"
		select cod_cliente, 
				 nota_testata,
				 nota_piede,
				 data_registrazione
		into   :ls_cod_cliente,
				 :ls_nota_testata,
				 :ls_nota_piede,
				 :ldt_data_registrazione
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca bolla per inserimento nota fissa: verificare"
			return -1
		end if
		
		setnull(ls_cod_fornitore)
		if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_BOL_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
		end if
//		f_crea_nota_fissa(ls_cod_cliente, ls_cod_fornitore, "GEN_BOL_VEN", ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_video)
		
		ls_nota_testata_1 = trim(ls_nota_testata)
		ls_nota_piede_1 = trim(ls_nota_piede)
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		update tes_bol_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :fl_anno_registrazione and  
				 num_registrazione = :fl_num_registrazione;
				
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento note nella Testata Bolle di Uscita."
			return -1
		end if
end choose
return 0
end function

public function integer wf_genera_fatture_singole_1 (long al_anno_registrazione, string as_cod_tipo_ord_ven, long al_anno_ordine, long al_num_ordine, ref string ls_messaggio);boolean lb_riferimento
long ll_num_registrazione, &
	  ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_fat_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_num_confezioni, ll_num_pezzi_confezione, ll_anno_registrazione
string ls_cod_tipo_fat_ven, ls_nota_testata, ls_cod_cliente[1], &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
		 ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, &
		 ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab,ls_cod_versione, ls_flag_abilita_rif_bol_ven, &
		 ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, &
		 ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, &
		 ls_note_dettaglio_rif, ls_db, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, &
		 ls_flag_doc_suc_det, ls_cod_tipo_ord_ven
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		 ldt_vostro_ordine_data
double ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta
string ls_cod_operatore_new, ls_cod_natura_intra

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

ll_anno_registrazione = f_anno_esercizio()
lb_riferimento = true
setnull(ls_cod_fornitore[1])

select tab_tipi_ord_ven.cod_tipo_fat_ven,  
		 tab_tipi_ord_ven.cod_tipo_analisi  
into   :ls_cod_tipo_fat_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_ord_ven.cod_tipo_ord_ven = :as_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	ls_messaggio = "Errore durante la lettura della tabella Tipi Fatture Vendita."
	return -1
end if

if not isnull(ls_cod_tipo_fat_ven) then
	select con_fat_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_fat_ven
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Controllo Fatture Vendita."
		return -1
	end if

	ll_num_registrazione ++
	
	update con_fat_ven
	set    con_fat_ven.num_registrazione = :ll_num_registrazione
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita."+ sqlca.sqlerrtext
		return -1
	end if

	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.flag_doc_suc_tes,
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_tipo_ord_ven,
			 tes_ord_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_flag_doc_suc_tes,
			 :ls_flag_doc_suc_pie,
			 :ls_cod_tipo_ord_ven,
			 :ls_cod_natura_intra
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :al_anno_ordine and  
			 tes_ord_ven.num_registrazione = :al_num_ordine;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Ordine Vendita."
		return -1
	end if
	
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_ord_ven,
			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
			 tab_tipi_fat_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_fat,
			 :ls_causale_trasporto,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Fatture."
		return -1
	end if
	
	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------
	
	
	if ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_nota_testata = "Rif. ns. Ordine Nr. " + string(al_anno_ordine) + "/" + &
								string(al_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
		
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE


	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if

	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	//prima di fare la insert metti l'operatore collegato per il nuovo documento
	if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
		ls_cod_operatore = ls_cod_operatore_new
	end if

	insert into tes_fat_ven
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_tipo_fat_ven,   
					 cod_cliente,
					 cod_des_cliente,   
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,   
					 cod_deposito,   
					 cod_ubicazione,
					 cod_valuta,   
					 cambio_ven,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_agente_1,
					 cod_agente_2,
					 cod_banca,   
					 num_ord_cliente,   
					 data_ord_cliente,   
					 cod_imballo,   
					 aspetto_beni,
					 peso_netto,
					 peso_lordo,
					 num_colli,
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,
					 causale_trasporto,
					 cod_porto,   
					 cod_resa,   
					 cod_documento,
					 numeratore_documento,
					 anno_documento,
					 num_documento,
					 data_fattura,
					 nota_testata,   
					 nota_piede,   
					 flag_fuori_fido,
					 flag_blocco,   
					 flag_movimenti,  
					 flag_contabilita,
					 tot_merci,
					 tot_spese_trasporto,
					 tot_spese_imballo,
					 tot_spese_bolli,
					 tot_spese_varie,
					 tot_sconto_cassa,
					 tot_sconti_commerciali,
					 imponibile_provvigioni_1,
					 imponibile_provvigioni_2,
					 tot_provvigioni_1,
					 tot_provvigioni_2,
					 importo_iva,
					 imponibile_iva,
					 importo_iva_valuta,
					 imponibile_iva_valuta,
					 tot_fattura,
					 tot_fattura_valuta,
					 flag_cambio_zero,
					 cod_banca_clien_for,
					 flag_st_note_tes,
					 flag_st_note_pie,
					 cod_natura_intra)  					 
	values      (:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ldt_data_registrazione,   
					 :ls_cod_operatore,   
					 :ls_cod_tipo_fat_ven,   
					 :ls_cod_cliente[1],
					 :ls_cod_des_cliente,   
					 :ls_rag_soc_1,   
					 :ls_rag_soc_2,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,   
					 :ls_cod_deposito[1],   
					 :ls_cod_ubicazione[1],
					 :ls_cod_valuta,   
					 :ld_cambio_ven,   
					 :ls_cod_tipo_listino_prodotto,   
					 :ls_cod_pagamento,   
					 :ld_sconto_testata,
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_num_ord_cliente,   
					 :ldt_data_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_aspetto_beni,
					 :ld_peso_netto,
					 :ld_peso_lordo,
					 :ld_num_colli,
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_causale_trasporto,
					 :ls_cod_porto,   
					 :ls_cod_resa,   
					 null,
					 null,
					 0,
					 0,
					 null,
					 :ls_nota_testata,   
					 :ls_nota_piede,   
					 'N',
					 'N',   
					 'N',
					 'N',
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 'N',
					 :ls_cod_banca_clien_for,
					 'I',
					 'I',
					 :ls_cod_natura_intra);					 

	 if sqlca.sqlcode <> 0 then
		 ls_messaggio = "Errore durante la scrittura Testata Fatture Vendita."+ sqlca.sqlerrtext
		 return -1
	 end if

	 if wf_crea_nota_fissa("FAT_VEN",ll_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :al_anno_ordine and
				evas_ord_ven.num_registrazione = :al_num_ordine 
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	ll_prog_riga_fat_ven = 0
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Evasione Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :al_anno_ordine and  
					det_ord_ven.num_registrazione = :al_num_ordine and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if
			
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
//    ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_or
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			ls_nota_dettaglio = ""
		end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------			
					
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				ls_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(al_anno_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(al_anno_ordine)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(al_num_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(al_num_ordine)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			if not isnull(ls_num_ord_cliente) then
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
				
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Fattura Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven_rif_bol_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 1,
							 null,
							 null,
							 null,   
							 null,   
							 null,
							 null,
							 0,
							 0,
							 'I',
							 0,
							 0);							 
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita."+ sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
		end if	

		insert into det_fat_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_fat_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 progr_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_fatturata,   
						 prezzo_vendita,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 anno_registrazione_fat,
						 num_registrazione_fat,
						 nota_dettaglio,   
						 anno_registrazione_bol_ven,   
						 num_registrazione_bol_ven,   
						 prog_riga_bol_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 fat_conversione_ven,   
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 anno_reg_ord_ven,   
						 num_reg_ord_ven,   
						 prog_riga_ord_ven,
						 cod_versione,
                   num_confezioni,
					    num_pezzi_confezione,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_fat_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ldt_data_stock[1],
						 :ll_progr_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 null,
						 null,
						 :ls_nota_dettaglio,   
						 null,
						 null,
						 null,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 :ld_fat_conversione_ven,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :al_anno_ordine,   
						 :al_num_ordine,   
						 :ll_prog_riga_ord_ven,
						 :ls_cod_versione,
                   :ll_num_confezioni,
					    :ll_num_pezzi_confezione,
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);

		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura Dettagli Fatture Vendita."+ sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino."
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento stock."
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_fat_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_fat_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, al_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, al_anno_registrazione, ll_prog_riga_fat_ven, 6) = -1 then
				ls_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :al_anno_ordine and  
				 det_ord_ven.num_registrazione = :al_num_ordine and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :al_anno_ordine and
						evas_ord_ven.num_registrazione = :al_num_ordine and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga;

		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

// ------------------------------  NUOVO SUSTEMA DI CALCOLO STATO ORDINE ---------------------------------------
	uo_generazione_documenti luo_generazione_documenti
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.uof_calcola_stato_ordine(al_anno_ordine, al_num_ordine)
	DESTROY luo_generazione_documenti
//	if ld_tot_val_ordine <= ld_tot_val_evaso then
//		ls_flag_evasione = "E"
//	else
//		ls_flag_evasione = "P"
//	end if
//
//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso,
//			 flag_evasione = :ls_flag_evasione
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :al_anno_ordine and  
//			 tes_ord_ven.num_registrazione = :al_num_ordine;
//
//	if sqlca.sqlcode = -1 then
//		ls_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Ordine Vendita."
//		return -1
//	end if
	
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(al_anno_registrazione,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo
	
	dw_gen_orig_dest.insertrow(0)
	dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", al_anno_ordine)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig", al_num_ordine)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", al_anno_registrazione)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest", ll_num_registrazione)
end if

return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


set_w_options(c_noenablepopup)

lw_oggetti[1] = dw_gen_bol_ven_fat_imm
dw_folder.fu_assigntab(1, "Ordini", lw_oggetti[])
lw_oggetti[1] = dw_gen_orig_dest
dw_folder.fu_assigntab(2, "Bolle / Fatture", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_gen_bol_ven_fat_imm.set_dw_options(sqlca, &
                                  	  pcca.null_object, &
												  c_multiselect + &
												  c_nonew + &
												  c_nomodify + &
												  c_nodelete + &
												  c_disablecc + &
												  c_disableccinsert, &
												  c_default)

dw_gen_orig_dest.set_dw_options(sqlca, &
                             	  pcca.null_object, &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete + &
										  c_disablecc + &
										  c_disableccinsert, &
	                             c_viewmodeblack)
													 
save_on_close(c_socnosave)

end event

on w_gen_bol_ven_fat_imm.create
int iCurrent
call super::create
this.cb_genera=create cb_genera
this.cb_1=create cb_1
this.cbx_singole=create cbx_singole
this.ddlb_cod_tipo_rag=create ddlb_cod_tipo_rag
this.st_cod_tipo_rag=create st_cod_tipo_rag
this.st_2=create st_2
this.gb_2=create gb_2
this.gb_1=create gb_1
this.rb_bolle=create rb_bolle
this.rb_fatture=create rb_fatture
this.rb_entrambe=create rb_entrambe
this.cb_add_bolla=create cb_add_bolla
this.dw_gen_orig_dest=create dw_gen_orig_dest
this.dw_folder=create dw_folder
this.dw_gen_bol_ven_fat_imm=create dw_gen_bol_ven_fat_imm
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_genera
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cbx_singole
this.Control[iCurrent+4]=this.ddlb_cod_tipo_rag
this.Control[iCurrent+5]=this.st_cod_tipo_rag
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.gb_2
this.Control[iCurrent+8]=this.gb_1
this.Control[iCurrent+9]=this.rb_bolle
this.Control[iCurrent+10]=this.rb_fatture
this.Control[iCurrent+11]=this.rb_entrambe
this.Control[iCurrent+12]=this.cb_add_bolla
this.Control[iCurrent+13]=this.dw_gen_orig_dest
this.Control[iCurrent+14]=this.dw_folder
this.Control[iCurrent+15]=this.dw_gen_bol_ven_fat_imm
end on

on w_gen_bol_ven_fat_imm.destroy
call super::destroy
destroy(this.cb_genera)
destroy(this.cb_1)
destroy(this.cbx_singole)
destroy(this.ddlb_cod_tipo_rag)
destroy(this.st_cod_tipo_rag)
destroy(this.st_2)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.rb_bolle)
destroy(this.rb_fatture)
destroy(this.rb_entrambe)
destroy(this.cb_add_bolla)
destroy(this.dw_gen_orig_dest)
destroy(this.dw_folder)
destroy(this.dw_gen_bol_ven_fat_imm)
end on

type cb_genera from uo_cb_ok within w_gen_bol_ven_fat_imm
event clicked pbm_bnclicked
integer x = 2331
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;call super::clicked;string ls_messaggio
long ll_anno_registrazione

setpointer(hourglass!)

select parametri_azienda.numero
into   :ll_anno_registrazione
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'N' and &
		 parametri_azienda.cod_parametro = 'ESC';

// *** michela 30/03/2004: parametri per salvare numero bolla
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
// ***

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore durante la lettura del parametro ESC.", &
				  Exclamation!, ok!)
	setpointer(arrow!)
	return
end if

if cbx_singole.checked then
	if rb_bolle.checked or rb_entrambe.checked then
		if wf_genera_bolle_singole(ll_anno_registrazione, ls_messaggio) = -1 then
			g_mb.messagebox("Attenzione", ls_messaggio, Exclamation!, ok!)
			rollback;
			setpointer(arrow!)
			return
		end if
	end if
	if rb_entrambe.checked and dw_gen_orig_dest.getrow() > 0 then
		dw_gen_orig_dest.insertrow(0)
	end if
	if rb_fatture.checked or rb_entrambe.checked then
		if wf_genera_fatture_singole(ll_anno_registrazione, ls_messaggio) = -1 then
			g_mb.messagebox("Attenzione", ls_messaggio, Exclamation!, ok!)
			rollback;
			setpointer(arrow!)
			return
		end if
	end if
else
	if rb_bolle.checked or rb_entrambe.checked then
		if wf_genera_bolle_riepilogate(ll_anno_registrazione, ls_messaggio) = -1 then
			g_mb.messagebox("Attenzione", ls_messaggio, Exclamation!, ok!)
			rollback;
			setpointer(arrow!)
			return
		end if
	end if
	if rb_entrambe.checked and dw_gen_orig_dest.getrow() > 0 then
		dw_gen_orig_dest.insertrow(0)
	end if
	if rb_fatture.checked or rb_entrambe.checked then
		if wf_genera_fatture_riepilogate(ll_anno_registrazione, ls_messaggio) = -1 then
			g_mb.messagebox("Attenzione", ls_messaggio, Exclamation!, ok!)
			rollback;
			setpointer(arrow!)
			return
		end if
	end if
end if

commit;

dw_folder.fu_selecttab(2)
dw_gen_bol_ven_fat_imm.triggerevent("pcd_retrieve")

setpointer(arrow!)
end event

type cb_1 from uo_cb_close within w_gen_bol_ven_fat_imm
integer x = 2331
integer y = 1360
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type cbx_singole from checkbox within w_gen_bol_ven_fat_imm
integer x = 46
integer y = 1380
integer width = 274
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Singole"
boolean lefttext = true
end type

event clicked;st_cod_tipo_rag.text=""
ddlb_cod_tipo_rag.reset()
f_po_loadddlb(ddlb_cod_tipo_rag, &
                 sqlca, &
                 "tab_tipi_rag", &
                 "cod_tipo_rag", &
                 "des_tipo_rag", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
                 "")
end event

type ddlb_cod_tipo_rag from dropdownlistbox within w_gen_bol_ven_fat_imm
integer x = 869
integer y = 1440
integer width = 1029
integer height = 500
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;cbx_singole.checked = false
st_cod_tipo_rag.text=f_po_selectddlb(ddlb_cod_tipo_rag)
end event

event getfocus;ddlb_cod_tipo_rag.reset()
f_po_loadddlb(ddlb_cod_tipo_rag, &
                 sqlca, &
                 "tab_tipi_rag", &
                 "cod_tipo_rag", &
                 "des_tipo_rag", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
                 "")
end event

type st_cod_tipo_rag from statictext within w_gen_bol_ven_fat_imm
integer x = 503
integer y = 1440
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_gen_bol_ven_fat_imm
integer x = 46
integer y = 1440
integer width = 457
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Raggruppate Per:"
boolean focusrectangle = false
end type

type gb_2 from groupbox within w_gen_bol_ven_fat_imm
integer x = 23
integer y = 1160
integer width = 1189
integer height = 140
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Generazione"
end type

type gb_1 from groupbox within w_gen_bol_ven_fat_imm
integer x = 23
integer y = 1320
integer width = 1897
integer height = 220
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo Generazione"
end type

type rb_bolle from radiobutton within w_gen_bol_ven_fat_imm
integer x = 46
integer y = 1220
integer width = 206
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Bolle"
boolean lefttext = true
end type

event clicked;string ls_select


ls_select = "select   tes_ord_ven.cod_tipo_ord_ven, " + &
							"tes_ord_ven.cod_cliente, " + &
							"evas_ord_ven.anno_registrazione, " + &
							"evas_ord_ven.num_registrazione " + &  
				"from     evas_ord_ven, " + &
							"tes_ord_ven, " + &
							"tab_tipi_ord_ven " + &
				"where    evas_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and " + &
						   "evas_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and " + &
							"evas_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and " + &
							"tab_tipi_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and " + &
							"tab_tipi_ord_ven.cod_tipo_ord_ven = tes_ord_ven.cod_tipo_ord_ven and " + &
							"evas_ord_ven.cod_azienda = ~~'" + s_cs_xx.cod_azienda + "~~' and " + &  
							"tab_tipi_ord_ven.cod_tipo_bol_ven is not null " + &
				"group by tes_ord_ven.cod_tipo_ord_ven, " + &
							"tes_ord_ven.cod_cliente, " + &
							"evas_ord_ven.anno_registrazione, " + &
							"evas_ord_ven.num_registrazione " + &
				"order by evas_ord_ven.anno_registrazione asc, " + &
							"evas_ord_ven.num_registrazione asc"


dw_gen_bol_ven_fat_imm.modify("datawindow.table.select='" + ls_select + "'")

dw_gen_bol_ven_fat_imm.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type rb_fatture from radiobutton within w_gen_bol_ven_fat_imm
integer x = 274
integer y = 1220
integer width = 549
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Fatture Immediate"
boolean lefttext = true
end type

event clicked;string ls_select


ls_select = "select   tes_ord_ven.cod_tipo_ord_ven, " + &
							"tes_ord_ven.cod_cliente, " + &
							"evas_ord_ven.anno_registrazione, " + &
							"evas_ord_ven.num_registrazione " + &  
				"from     evas_ord_ven, " + &
							"tes_ord_ven, " + &
							"tab_tipi_ord_ven " + &
				"where    evas_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and " + &
						   "evas_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and " + &
							"evas_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and " + &
							"tab_tipi_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and " + &
							"tab_tipi_ord_ven.cod_tipo_ord_ven = tes_ord_ven.cod_tipo_ord_ven and " + &
							"evas_ord_ven.cod_azienda = ~~'" + s_cs_xx.cod_azienda + "~~' and " + &  
							"tab_tipi_ord_ven.cod_tipo_fat_ven is not null " + &
				"group by tes_ord_ven.cod_tipo_ord_ven, " + &
							"tes_ord_ven.cod_cliente, " + &
							"evas_ord_ven.anno_registrazione, " + &
							"evas_ord_ven.num_registrazione " + &
				"order by evas_ord_ven.anno_registrazione asc, " + &
							"evas_ord_ven.num_registrazione asc"


dw_gen_bol_ven_fat_imm.modify("datawindow.table.select='" + ls_select + "'")

dw_gen_bol_ven_fat_imm.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type rb_entrambe from radiobutton within w_gen_bol_ven_fat_imm
integer x = 846
integer y = 1220
integer width = 343
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Entrambe"
boolean checked = true
boolean lefttext = true
end type

event clicked;string ls_select


ls_select = "select   tes_ord_ven.cod_tipo_ord_ven, " + &
							"tes_ord_ven.cod_cliente, " + &
							"evas_ord_ven.anno_registrazione, " + &
							"evas_ord_ven.num_registrazione " + &  
				"from     evas_ord_ven, " + &
							"tes_ord_ven, " + &
							"tab_tipi_ord_ven " + &
				"where    evas_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and " + &
						   "evas_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and " + &
							"evas_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and " + &
							"tab_tipi_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and " + &
							"tab_tipi_ord_ven.cod_tipo_ord_ven = tes_ord_ven.cod_tipo_ord_ven and " + &
							"evas_ord_ven.cod_azienda = ~~'" + s_cs_xx.cod_azienda + "~~' and " + &  
			            "(tab_tipi_ord_ven.cod_tipo_bol_ven is not null or " + &
			            " tab_tipi_ord_ven.cod_tipo_fat_ven is not null) " + &
				"group by tes_ord_ven.cod_tipo_ord_ven, " + &
							"tes_ord_ven.cod_cliente, " + &
							"evas_ord_ven.anno_registrazione, " + &
							"evas_ord_ven.num_registrazione " + &
				"order by evas_ord_ven.anno_registrazione asc, " + &
							"evas_ord_ven.num_registrazione asc"


dw_gen_bol_ven_fat_imm.modify("datawindow.table.select='" + ls_select + "'")

dw_gen_bol_ven_fat_imm.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type cb_add_bolla from commandbutton within w_gen_bol_ven_fat_imm
integer x = 2331
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 41
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Agg. a Bolla"
end type

event clicked;long ll_i, ll_selected[], ll_num_righe_sel
string ls_cod_cliente_old, ls_cod_cliente, ls_cod_tipo_rag 

dw_gen_bol_ven_fat_imm.get_selected_rows(ll_selected[])
ll_num_righe_sel = upperbound(ll_selected)
if ll_num_righe_sel = 0 then
	g_mb.messagebox("Apice", "Selezionare almeno una riga d'ordine")
	return
end if

ls_cod_tipo_rag = f_po_selectddlb(ddlb_cod_tipo_rag)
if isnull(ls_cod_tipo_rag) or len(ls_cod_tipo_rag) < 1 then
	g_mb.messagebox("APICE","Indicare il tipo raggrupamento da utilizzare")
	return 0
end if

// controllo che gli ordini siano riferiti allo stesso cliente
ls_cod_cliente_old = dw_gen_bol_ven_fat_imm.getitemstring(ll_selected[1], "tes_ord_ven_cod_cliente")
for ll_i = 2 to ll_num_righe_sel 
	ls_cod_cliente = dw_gen_bol_ven_fat_imm.getitemstring(ll_selected[ll_i], "tes_ord_ven_cod_cliente")
	if ls_cod_cliente_old <> ls_cod_cliente then
		g_mb.messagebox("Apice", "Selezionare righe d'ordine riferite allo stesso cliente")
		return
	end if
next

s_cs_xx.parametri.parametro_s_1 = ls_cod_cliente_old
s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_rag 
window_open_parm(w_dest_ord_bol_ven, -1, dw_gen_bol_ven_fat_imm)




end event

type dw_gen_orig_dest from uo_cs_xx_dw within w_gen_bol_ven_fat_imm
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 120
integer width = 2651
integer height = 1000
integer taborder = 10
string dataobject = "d_gen_orig_dest"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type dw_folder from u_folder within w_gen_bol_ven_fat_imm
integer y = 20
integer width = 2697
integer height = 1120
integer taborder = 20
end type

type dw_gen_bol_ven_fat_imm from uo_cs_xx_dw within w_gen_bol_ven_fat_imm
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 120
integer width = 2629
integer height = 1000
integer taborder = 30
string dataobject = "d_gen_bol_ven_fat_imm"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

