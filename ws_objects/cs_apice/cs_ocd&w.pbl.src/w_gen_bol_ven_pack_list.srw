﻿$PBExportHeader$w_gen_bol_ven_pack_list.srw
forward
global type w_gen_bol_ven_pack_list from w_cs_xx_principale
end type
type evasione from checkbox within w_gen_bol_ven_pack_list
end type
type cb_genera from commandbutton within w_gen_bol_ven_pack_list
end type
type cb_cerca from commandbutton within w_gen_bol_ven_pack_list
end type
type dw_lista_pl from uo_cs_xx_dw within w_gen_bol_ven_pack_list
end type
type dw_selezione from uo_cs_xx_dw within w_gen_bol_ven_pack_list
end type
end forward

global type w_gen_bol_ven_pack_list from w_cs_xx_principale
integer width = 3456
integer height = 1948
string title = "Evasione Packing List"
evasione evasione
cb_genera cb_genera
cb_cerca cb_cerca
dw_lista_pl dw_lista_pl
dw_selezione dw_selezione
end type
global w_gen_bol_ven_pack_list w_gen_bol_ven_pack_list

type variables
longlong il_prog_sessione
end variables

forward prototypes
public subroutine wf_evasione_packing_list ()
public function integer wf_get_prog_sessione ()
end prototypes

public subroutine wf_evasione_packing_list ();boolean lb_avanti=false, lb_no_bolla = false
string ls_cod_tipo_bol_ven, ls_cod_tipo_fat_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, &
     	 ls_cod_cliente,ls_cod_prodotto, ls_elenco_pl[], ls_stato, ls_cod_prodotto_raggruppato
integer li_return
long ll_return, ll_i, ll_prog_riga_ord_ven,ll_anno_registrazione,ll_num_registrazione, ll_y, &
     ll_num_pack_list, ll_num_collo, ll_z, ll_max, ll_quan_evasa, ll_ret
dec{4} ld_quan_commessa, ld_quan_evasa, ld_quan_in_evasione, ld_quan_pack_list, ld_quan_ordine, ld_quan_raggruppo

ll_y = 0

for ll_i = 1 to dw_lista_pl.rowcount()
	
	if dw_lista_pl.getitemstring(ll_i, "flag_spedizione") <> "S" then continue
	
	ll_anno_registrazione = dw_lista_pl.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione =  dw_lista_pl.getitemnumber(ll_i, "num_registrazione")
	ld_quan_pack_list = dw_lista_pl.getitemnumber( ll_i, "quan_pack_list")
	
	setnull(ls_cod_ubicazione)
	
	select cod_deposito
	into   :ls_cod_deposito
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca codice deposito in testata ordine. " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if isnull(ls_cod_deposito) then
		g_mb.messagebox("APICE","Attenzione! Manca l'indicazione del deposito di prelievo nella testata ordine.")
		rollback;
		return
	end if
//	
	if ld_quan_pack_list > 0 then	
		
		ld_quan_evasa = 0
		ld_quan_commessa = 0
		
		ll_prog_riga_ord_ven = dw_lista_pl.getitemnumber(ll_i, "prog_riga_ord_ven")
		ll_num_pack_list = dw_lista_pl.getitemnumber(ll_i, "num_pack_list")
		ll_num_collo = dw_lista_pl.getitemnumber(ll_i, "num_collo")
		
		select cod_prodotto, 
				 cod_tipo_det_ven, 
				 quan_evasa,
				 quan_ordine
		into   :ls_cod_prodotto, 
				 :ls_cod_tipo_det_ven,
				 :ld_quan_evasa,
				 :ld_quan_ordine
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca riga ordine. " + sqlca.sqlerrtext)
			rollback;
			return
		end if				 

		if ( ld_quan_evasa + ld_quan_pack_list ) < ld_quan_ordine then
			ls_stato = "P"
		else
			ls_stato = "E"
		end if
		
		update det_ord_ven
		set    quan_evasa = quan_evasa + :ld_quan_pack_list,
		       flag_evasione = :ls_stato
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca riga ordine. " + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		// *** aggiorno i progressivi dell' anagrafica
		
		update anag_prodotti  
		set    quan_impegnata = quan_impegnata - :ld_quan_pack_list					 
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto;	

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in aggiornamento progressivi anag prodotto: " + sqlca.sqlerrtext)
			rollback;
			return
		end if		
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_pack_list, "M")
			
			update anag_prodotti  
			set    quan_impegnata = quan_impegnata - :ld_quan_raggruppo					 
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_raggruppato;	
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in aggiornamento progressivi anag prodotto: " + sqlca.sqlerrtext)
				rollback;
				return
			end if		
		end if
		
		
		
		
		
		uo_generazione_documenti luo_obj
		luo_obj = create uo_generazione_documenti
		ll_ret = luo_obj.uof_calcola_stato_ordine( ll_anno_registrazione, ll_num_registrazione)
		
		if ll_ret < 0 then
			g_mb.messagebox("APICE","Errore in ricerca riga ordine. " + sqlca.sqlerrtext)
			rollback;
			return			
		end if
		
		update det_ord_ven_pack_list
		set    flag_spedito = 'S'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven and
				 num_pack_list = :ll_num_pack_list and
				 num_collo = :ll_num_collo;			
			
		if sqlca.sqlcode <> 0 then		 
			g_mb.messagebox("APICE","Errore in impostazione dell stato Spedito del packing list.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	end if

next

commit;

cb_cerca.postevent("clicked")

end subroutine

public function integer wf_get_prog_sessione ();longlong			ll_id
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")
ll_id = longlong(ls_temp)

return ll_id
end function

on w_gen_bol_ven_pack_list.create
int iCurrent
call super::create
this.evasione=create evasione
this.cb_genera=create cb_genera
this.cb_cerca=create cb_cerca
this.dw_lista_pl=create dw_lista_pl
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.evasione
this.Control[iCurrent+2]=this.cb_genera
this.Control[iCurrent+3]=this.cb_cerca
this.Control[iCurrent+4]=this.dw_lista_pl
this.Control[iCurrent+5]=this.dw_selezione
end on

on w_gen_bol_ven_pack_list.destroy
call super::destroy
destroy(this.evasione)
destroy(this.cb_genera)
destroy(this.cb_cerca)
destroy(this.dw_lista_pl)
destroy(this.dw_selezione)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)

save_on_close(c_socnosave)

dw_lista_pl.set_dw_options(sqlca, &
									pcca.null_object, &
									c_nomodify + &
									c_nodelete + &
									c_newonopen + &
									c_noretrieveonopen + &
									c_disableCC, &
									c_noresizedw + &
									c_nohighlightselected + &
									c_nocursorrowpointer +&
									c_nocursorrowfocusrect +&
									c_ViewModeColorUnchanged)

dw_selezione.set_dw_options(sqlca, &
									pcca.null_object, &
									c_noretrieveonopen + &
									c_nomodify + &
									c_nodelete + &
									c_newonopen + &
									c_noenablemodifyonopen + &
									c_scrollparent + &
									c_disablecc, &
									c_noresizedw + &
									c_nohighlightselected + &
									c_nocursorrowfocusrect + &
									c_nocursorrowpointer + &
									c_ViewModeColorUnchanged)


end event

type evasione from checkbox within w_gen_bol_ven_pack_list
integer x = 1760
integer y = 1760
integer width = 1189
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Evasione Paking list senza generare bolla:"
boolean lefttext = true
end type

type cb_genera from commandbutton within w_gen_bol_ven_pack_list
integer x = 3026
integer y = 1756
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Genera"
end type

event clicked;boolean lb_avanti=false, lb_no_bolla = false
string ls_cod_tipo_bol_ven, ls_cod_tipo_fat_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, &
     	 ls_cod_cliente,ls_cod_prodotto, ls_elenco_pl[]
integer li_return
long ll_return, ll_i, ll_prog_riga_ord_ven,ll_anno_registrazione,ll_num_registrazione, ll_y, &
     ll_num_pack_list, ll_num_collo, ll_z, ll_max
decimal ld_quan_commessa, ld_quan_evasa, ld_quan_in_evasione, ld_quan_pack_list
double  ld_vuoto[]
uo_generazione_documenti luo_gen_doc

dw_lista_pl.accepttext()
//***modifica Claudia per permettere di evadere i paking list (ordini) senza emettere bolle

//genero un progressivo sessione univoco #########################
il_prog_sessione = wf_get_prog_sessione()
//#################################################


if evasione.checked  = true then
	lb_no_bolla = true
else
	lb_no_bolla = false
end if
if lb_no_bolla then
	ll_return = g_mb.messagebox("APICE","Vuoi evadere le righe selezionate senza generare la bolla? La conferma sarà definitiva.", Question!, YesNo!, 1)
	if ll_return = 2 then return
	wf_evasione_packing_list()
	
	commit;

	cb_cerca.postevent("clicked")
	return
else
	ll_return = g_mb.messagebox("APICE","Vuoi spedire le righe selezionate? La conferma sarà definitiva.", Question!, YesNo!, 1)
	if ll_return = 2 then return
end if
if ll_return = 2 then return

ll_y = 0

s_cs_xx.parametri.parametro_d_1_a[] = ld_vuoto
s_cs_xx.parametri.parametro_d_2_a[] = ld_vuoto

for ll_i = 1 to dw_lista_pl.rowcount()
	
	if dw_lista_pl.getitemstring(ll_i, "flag_spedizione") <> "S" then continue
	
	ll_anno_registrazione = dw_lista_pl.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione =  dw_lista_pl.getitemnumber(ll_i, "num_registrazione")
	ld_quan_pack_list = dw_lista_pl.getitemnumber( ll_i, "quan_pack_list")
	setnull(ls_cod_ubicazione)
	
	select cod_deposito
	into   :ls_cod_deposito
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca codice deposito in testata ordine. " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if isnull(ls_cod_deposito) then
		g_mb.messagebox("APICE","Attenzione! Manca l'indicazione del deposito di prelievo nella testata ordine.")
		rollback;
		return
	end if
	
	if dw_lista_pl.getitemnumber(ll_i, "quan_pack_list") > 0 then	
		
		ld_quan_evasa = 0
		ld_quan_commessa = 0
		
		ll_prog_riga_ord_ven = dw_lista_pl.getitemnumber(ll_i, "prog_riga_ord_ven")
		ll_num_pack_list = dw_lista_pl.getitemnumber(ll_i, "num_pack_list")
		ll_num_collo = dw_lista_pl.getitemnumber(ll_i, "num_collo")
		
		select cod_prodotto, 
				 cod_tipo_det_ven
		into   :ls_cod_prodotto, 
				 :ls_cod_tipo_det_ven
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca riga ordine. " + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		//*** MICHELA 14/12/2005: SPECIFICA EUGANEA PANNELLI REPORT DDT FATTURE
		//                        solo se esiste il parametro flag aziendale 'EUG' a si allora procedo con l'ordinamento
		//                        delle righe in base alla specifica
		
		long   ll_num_evaso
		string ls_flag
		
		select flag
		into   :ls_flag
		from   parametri_azienda 
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_parametro = 'EUG';
				 
		if sqlca.sqlcode = 0  and not isnull(ls_flag) and ls_flag = 'S' then
	
			li_return = f_ass_ord_ven_pack(ll_anno_registrazione, &
											  ll_num_registrazione, &
											  ll_prog_riga_ord_ven, &
											  ls_cod_tipo_det_ven, &
											  ls_cod_deposito, &
											  ls_cod_ubicazione, &
											  ls_cod_prodotto, &
											  dw_lista_pl.getitemnumber(ll_i, "quan_pack_list"), &
											  0, &
											  0,&
											  ll_num_pack_list, &
											  ll_num_collo)
			if li_return = 3 then
				g_mb.messagebox("APICE","Ordine " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga nr." + string(ll_prog_riga_ord_ven) + "~r~nNon è stato trovata suffiente quantità negli STOCK per evadere l'ordine: operazione totalmente annullata!", stopsign! )
				rollback;
				return
			end if
			
			ll_y ++
			s_cs_xx.parametri.parametro_d_1_a[ll_y] = ll_anno_registrazione
			s_cs_xx.parametri.parametro_d_2_a[ll_y]  = ll_num_registrazione
			lb_avanti = true
			
			update det_ord_ven_pack_list
			set    flag_spedito = 'S'
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ord_ven = :ll_prog_riga_ord_ven and
					 num_pack_list = :ll_num_pack_list and
					 num_collo = :ll_num_collo;			
			
		else
			
			luo_gen_doc = create uo_generazione_documenti
			li_return = luo_gen_doc.uof_ass_ord_ven(	ll_anno_registrazione, &
																	ll_num_registrazione, &
																	ll_prog_riga_ord_ven, &
																	ls_cod_tipo_det_ven, &
																	ls_cod_deposito, &
																	ls_cod_ubicazione, &
																	ls_cod_prodotto, &
																	dw_lista_pl.getitemnumber(ll_i, "quan_pack_list"), &
																	0, &
																	0, &
																	il_prog_sessione)
			destroy luo_gen_doc
//			li_return = f_ass_ord_ven(ll_anno_registrazione, &
//											  ll_num_registrazione, &
//											  ll_prog_riga_ord_ven, &
//											  ls_cod_tipo_det_ven, &
//											  ls_cod_deposito, &
//											  ls_cod_ubicazione, &
//											  ls_cod_prodotto, &
//											  dw_lista_pl.getitemnumber(ll_i, "quan_pack_list"), &
//											  0, &
//											  0)
			if li_return = 3 then
				g_mb.messagebox("APICE","Ordine " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga nr." + string(ll_prog_riga_ord_ven) + "~r~nNon è stato trovata suffiente quantità negli STOCK per evadere l'ordine: operazione totalmente annullata!", stopsign! )
				rollback;
				return
			end if
			
			ll_y ++
			s_cs_xx.parametri.parametro_d_1_a[ll_y] = ll_anno_registrazione
			s_cs_xx.parametri.parametro_d_2_a[ll_y]  = ll_num_registrazione
			lb_avanti = true
			
			update det_ord_ven_pack_list
			set    flag_spedito = 'S'
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ord_ven = :ll_prog_riga_ord_ven and
					 num_pack_list = :ll_num_pack_list and
					 num_collo = :ll_num_collo;
		end if
		
		// ****************  FINE MODIFICA
		
				 
		if sqlca.sqlcode <> 0 then		 
			g_mb.messagebox("APICE","Errore in impostazione dell stato Spedito del packing list.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		ll_max = upperbound(ls_elenco_pl)
		if ll_max = 0 then
			ls_elenco_pl[1] = string(ll_anno_registrazione) + "/" + string(ll_num_pack_list)
		else
			for ll_z = 1 to ll_max
				if ls_elenco_pl[ll_z] = string(ll_anno_registrazione) + "/" + string(ll_num_pack_list) then exit
				if ll_z = ll_max then
					ls_elenco_pl[ll_max + 1] = string(ll_anno_registrazione) + "/" + string(ll_num_pack_list)
				end if
			next 
		end if		
	end if
	
next

//commit;

if not lb_avanti then
	s_cs_xx.parametri.parametro_d_1_a[] = ld_vuoto
	s_cs_xx.parametri.parametro_d_2_a[] = ld_vuoto
	return
end if

g_mb.messagebox("APICE","Assegnazione delle righe selezionate eseguita con successo!")

// ----------------------------- riaggiorno righe ordine ------------------------------------------------------
//wf_carica_righe_ordine()

// ----------------------------- attivo la maschera di input dati bolla o fattura -----------------------------

//tab_1.postevent("clicked")
//
//w_richiesta_dati_bolla.postevent("ue_seleziona_tab")

s_cs_xx.parametri.parametro_s_5 = "PACKLIST"
s_cs_xx.parametri.parametro_s_6 = ""
ll_max = upperbound(ls_elenco_pl)
for ll_z = 1 to ll_max
	if ll_z > 1 then s_cs_xx.parametri.parametro_s_6 += "-"
	s_cs_xx.parametri.parametro_s_6 += ls_elenco_pl[ll_z]
next 


//////////////////////////////////////////////////////////////////////// ho aggiunto questo
s_cs_xx.parametri.parametro_s_1_a = ls_elenco_pl

// l'elenco degli ordini da evadere è passato tramite gli array
// il_anno_reg_ord_ven[], il_num_reg_ord_ven[]
window_open(w_richiesta_dati_bolla, 0)

commit;

cb_cerca.postevent("clicked")

end event

type cb_cerca from commandbutton within w_gen_bol_ven_pack_list
integer x = 2706
integer y = 20
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_selezione.accepttext()

iuo_dw_main = dw_lista_pl
dw_lista_pl.change_dw_current()
dw_lista_pl.triggerevent("pcd_retrieve")
end event

type dw_lista_pl from uo_cs_xx_dw within w_gen_bol_ven_pack_list
integer x = 23
integer y = 488
integer width = 3378
integer height = 1256
integer taborder = 50
string dataobject = "d_gen_bol_ven_pack_list_ext"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente
long   ll_num_pack_list, ll_rows, ll_i, ll_riga
datetime ldt_data_reg_inizio, ldt_data_reg_fine, ldt_data_cons_inizio, ldt_data_cons_fine
dec{4} ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa
datastore lds_pl

ls_cod_cliente = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_cliente")
ll_num_pack_list = dw_selezione.getitemnumber(dw_selezione.getrow(),"num_pack_list")

if isnull(ls_cod_cliente) then
	g_mb.messagebox("APICE","L'indicazione del cliente è obbligatoria.")
	return
end if

ldt_data_reg_inizio = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_reg_inizio")
if isnull(ldt_data_reg_inizio) then ldt_data_reg_inizio = datetime(date("01/01/1900"), 00:00:00)

ldt_data_reg_fine = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_reg_fine")
if isnull(ldt_data_reg_fine) then ldt_data_reg_fine = datetime(date("31/12/2099"), 00:00:00)

ldt_data_cons_inizio = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_cons_inizio")
if isnull(ldt_data_cons_inizio) then ldt_data_cons_inizio = datetime(date("01/01/1900"), 00:00:00)

ldt_data_cons_fine = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_cons_fine")
if isnull(ldt_data_cons_fine) then ldt_data_cons_fine = datetime(date("31/12/2099"), 00:00:00)

lds_pl = create datastore
lds_pl.dataobject='d_gen_bol_ven_pack_list_datastore'
lds_pl.settransobject(sqlca)
ll_rows = lds_pl.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente, date(ldt_data_reg_inizio), date(ldt_data_reg_fine), date(ldt_data_cons_inizio), date(ldt_data_cons_fine))
if ll_rows = 0 then
	g_mb.messagebox("APICE","Non è stato trovato alcun Packing List con le selezioni correnti.")
	return
end if

dw_lista_pl.change_dw_current()

dw_lista_pl.reset()

for ll_i = 1 to ll_rows
	
	ll_riga = dw_lista_pl.insertrow(0)
	
	dw_lista_pl.setitem(ll_riga, "anno_registrazione", lds_pl.getitemnumber(ll_i, "det_ord_ven_pack_list_anno_registrazione") )
	dw_lista_pl.setitem(ll_riga, "num_registrazione", lds_pl.getitemnumber(ll_i, "det_ord_ven_pack_list_num_registrazione") )
	dw_lista_pl.setitem(ll_riga, "prog_riga_ord_ven", lds_pl.getitemnumber(ll_i, "det_ord_ven_pack_list_prog_riga_ord_ven") )
	dw_lista_pl.setitem(ll_riga, "num_pack_list", lds_pl.getitemnumber(ll_i, "det_ord_ven_pack_list_num_pack_list") )
	dw_lista_pl.setitem(ll_riga, "num_collo", lds_pl.getitemnumber(ll_i, "det_ord_ven_pack_list_num_collo") )
	dw_lista_pl.setitem(ll_riga, "quan_pack_list", lds_pl.getitemnumber(ll_i, "det_ord_ven_pack_list_quan_pack_list") )
	dw_lista_pl.setitem(ll_riga, "data_consegna", lds_pl.getitemdatetime(ll_i, "det_ord_ven_data_consegna") )
	dw_lista_pl.setitem(ll_riga, "cod_prodotto", lds_pl.getitemstring(ll_i, "det_ord_ven_cod_prodotto") )
	dw_lista_pl.setitem(ll_riga, "des_prodotto", lds_pl.getitemstring(ll_i, "det_ord_ven_des_prodotto") )
	ld_quan_ordine = lds_pl.getitemnumber(ll_i, "det_ord_ven_quan_ordine")
	ld_quan_in_evasione = lds_pl.getitemnumber(ll_i, "det_ord_ven_quan_in_evasione")
	ld_quan_evasa = lds_pl.getitemnumber(ll_i, "det_ord_ven_quan_evasa")
	dw_lista_pl.setitem(ll_riga, "quan_ordine", ld_quan_ordine )
	dw_lista_pl.setitem(ll_riga, "quan_residua",ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa )
	dw_lista_pl.setitem(ll_riga, "flag_spedizione", "N" )
	dw_lista_pl.setitem(ll_riga, "data_registrazione", lds_pl.getitemdatetime(ll_i, "tes_ord_ven_data_registrazione") )

next

destroy lds_pl

if ll_num_pack_list > 0 and not isnull(ll_num_pack_list) then
	dw_lista_pl.setfilter("num_pack_list = " + string(ll_num_pack_list))
	dw_lista_pl.filter()
end if

end event

type dw_selezione from uo_cs_xx_dw within w_gen_bol_ven_pack_list
integer x = 27
integer y = 16
integer width = 2661
integer height = 448
integer taborder = 10
string dataobject = "d_gen_bol_ven_pack_list_selezione"
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	end choose
end event

