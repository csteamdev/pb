﻿$PBExportHeader$w_packing_list.srw
$PBExportComments$finestra packing list
forward
global type w_packing_list from w_cs_xx_principale
end type
type cb_aggiorna from commandbutton within w_packing_list
end type
type dw_selezione_pl from uo_cs_xx_dw within w_packing_list
end type
type cb_cancella from commandbutton within w_packing_list
end type
type dw_packing_list from datawindow within w_packing_list
end type
type dw_colli from datawindow within w_packing_list
end type
type dw_report_pl from uo_cs_xx_dw within w_packing_list
end type
type cb_avanti from commandbutton within w_packing_list
end type
type st_1 from statictext within w_packing_list
end type
type dw_eti_colli from uo_cs_xx_dw within w_packing_list
end type
type cb_salva from commandbutton within w_packing_list
end type
type cb_ritorna from commandbutton within w_packing_list
end type
type cb_modifica from commandbutton within w_packing_list
end type
type cb_indietro from commandbutton within w_packing_list
end type
type cb_stampa from commandbutton within w_packing_list
end type
type cb_elimina from commandbutton within w_packing_list
end type
type cb_stampa_pl from commandbutton within w_packing_list
end type
type cb_etichette from commandbutton within w_packing_list
end type
type cb_stampa_eti from commandbutton within w_packing_list
end type
type em_num_eti from editmask within w_packing_list
end type
end forward

global type w_packing_list from w_cs_xx_principale
integer width = 3534
integer height = 1924
string title = "Packing List"
boolean resizable = false
event ue_imposta_dw ( )
event ue_posiziona_w ( )
cb_aggiorna cb_aggiorna
dw_selezione_pl dw_selezione_pl
cb_cancella cb_cancella
dw_packing_list dw_packing_list
dw_colli dw_colli
dw_report_pl dw_report_pl
cb_avanti cb_avanti
st_1 st_1
dw_eti_colli dw_eti_colli
cb_salva cb_salva
cb_ritorna cb_ritorna
cb_modifica cb_modifica
cb_indietro cb_indietro
cb_stampa cb_stampa
cb_elimina cb_elimina
cb_stampa_pl cb_stampa_pl
cb_etichette cb_etichette
cb_stampa_eti cb_stampa_eti
em_num_eti em_num_eti
end type
global w_packing_list w_packing_list

type variables
boolean ib_collo_modified, ib_pl_modified, ib_nuovo_pl, ib_modifica
string  is_stampa
end variables

forward prototypes
public function integer wf_carica_pl ()
public function integer wf_carica_ordine ()
end prototypes

event ue_imposta_dw;dw_selezione_pl.setitem(1,"anno_registrazione",f_anno_esercizio())
dw_selezione_pl.setitem(1,"num_registrazione",0)
dw_selezione_pl.setitem(1,"num_pl",0)
dw_selezione_pl.setitem(1,"cod_cliente","")
end event

event ue_posiziona_w;this.x = 0
this.y = 0

end event

public function integer wf_carica_pl ();long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga, ll_num_collo, ll_num_pl, ll_riga
dec{4}  ld_quan_pl, ld_quan_altri_pl, ld_quan_residua, ld_quan_ordine, ld_quan_evasa
string ls_cod_prodotto, ls_des_prodotto, ls_misura, ls_cod_cliente
boolean lb_primo

ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")
	
declare det_pl cursor for
select anno_registrazione, num_registrazione, prog_riga_ord_ven, quan_pack_list, num_collo
from det_ord_ven_pack_list
where cod_azienda = :s_cs_xx.cod_azienda and
		num_pack_list = :ll_num_pl
order by prog_riga_ord_ven;
		
open det_pl;		

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore det_pl. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if	

dw_packing_list.reset()
lb_primo = true

do while true
	
	fetch det_pl into :ll_anno_registrazione, :ll_num_registrazione, :ll_prog_riga, :ld_quan_pl, :ll_num_collo;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore det_pl. Dettaglio = " + sqlca.sqlerrtext)
		close det_pl;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		close det_pl;
		exit		
	end if

	lb_primo = false
	
	select cod_cliente
	into :ls_cod_cliente
	from tes_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tes_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
		close det_pl;
		return -1
	end if
	
	dw_selezione_pl.setitem(1,"anno_registrazione",ll_anno_registrazione)
	dw_selezione_pl.setitem(1,"num_registrazione",ll_num_registrazione)
	dw_selezione_pl.setitem(1,"cod_cliente",ls_cod_cliente)
	
	select cod_prodotto, des_prodotto, quan_ordine, quan_evasa
	into :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_ordine, :ld_quan_evasa
	from det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
		close det_pl;
		return -1
	end if
	
	select cod_misura_mag
	into :ls_misura
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tab_misure. Dettaglio = " + sqlca.sqlerrtext)
		close det_pl;
		return -1
	end if	

	select sum(quan_pack_list)
	into :ld_quan_altri_pl
	from det_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		close det_pl;
		return -1
	end if	

	ld_quan_residua = ld_quan_ordine - ld_quan_evasa - ld_quan_altri_pl		

	ll_riga = dw_packing_list.insertrow(0)
	dw_packing_list.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	dw_packing_list.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	dw_packing_list.setitem(ll_riga,"unita_misura",ls_misura)
	dw_packing_list.setitem(ll_riga,"quantita_residua",ld_quan_residua)
	dw_packing_list.setitem(ll_riga,"quantita_pl",ld_quan_pl)
	dw_packing_list.setitem(ll_riga,"collo",ll_num_collo)
	dw_packing_list.setitem(ll_riga,"num_reg",ll_num_registrazione)
	dw_packing_list.setitem(ll_riga,"anno_reg",ll_anno_registrazione)
	dw_packing_list.setitem(ll_riga,"riga",ll_prog_riga)
	dw_packing_list.setitem(ll_riga,"originale",0)

loop

close det_pl;

if lb_primo = false then
	cb_modifica.enabled = true
	cb_elimina.enabled = true
	ib_modifica = false
	cb_avanti.enabled = true
	cb_cancella.enabled = true
end if	

return 0
end function

public function integer wf_carica_ordine ();string ls_cod_prodotto, ls_des_prodotto, ls_misura, ls_cod_cliente
long ll_riga, ll_prog_riga, ll_anno_registrazione, ll_num_registrazione
dec{4} ld_quan_ordine, ld_quan_evasa, ld_quan_residua, ld_quan_altri_pl	  
boolean lb_primo, lb_trovato	  

ll_num_registrazione = dw_selezione_pl.getitemnumber(1,"num_registrazione")
ll_anno_registrazione = dw_selezione_pl.getitemnumber(1,"anno_registrazione")

declare det_ord cursor for
select cod_prodotto, des_prodotto, quan_ordine, quan_evasa, prog_riga_ord_ven
from det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		(quan_ordine - quan_evasa) > 0
order by	prog_riga_ord_ven;

open det_ord;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore det_ord. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select cod_cliente
into :ls_cod_cliente
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
	close det_ord;
	return -1
end if	
		
dw_packing_list.reset()
dw_selezione_pl.setitem(1,"cod_cliente",ls_cod_cliente)
lb_primo = true
lb_trovato = false

do while true
	
	fetch det_ord into :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_ordine, :ld_quan_evasa, :ll_prog_riga;
		
	if sqlca.sqlcode = 100 then
		close det_ord;
		exit
	elseif sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore det_ord. Dettaglio = " + sqlca.sqlerrtext)
		close det_ord;
		rollback;
		return -1
	end if
	
	lb_primo = false
	
	select cod_misura_mag
	into :ls_misura
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tab_misure. Dettaglio = " + sqlca.sqlerrtext)
		close det_ord;
		return -1
	end if	

	select sum(quan_pack_list)
	into :ld_quan_altri_pl
	from det_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga and
			flag_spedito = 'N';

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		close det_ord;
		return -1
	end if
	
	if isnull(ld_quan_altri_pl) then ld_quan_altri_pl = 0

	ld_quan_residua = ld_quan_ordine - ld_quan_evasa - ld_quan_altri_pl
	
	if ld_quan_residua = 0 then 
		continue
	else
		lb_trovato = true
	end if	

	ll_riga = dw_packing_list.insertrow(0)
	dw_packing_list.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	dw_packing_list.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	dw_packing_list.setitem(ll_riga,"unita_misura",ls_misura)
	dw_packing_list.setitem(ll_riga,"quantita_residua",ld_quan_residua)
	dw_packing_list.setitem(ll_riga,"num_reg",ll_num_registrazione)
	dw_packing_list.setitem(ll_riga,"anno_reg",ll_anno_registrazione)
	dw_packing_list.setitem(ll_riga,"riga",ll_prog_riga)
	dw_packing_list.setitem(ll_riga,"originale",1)
	
	
	
loop

close det_ord;

if lb_primo = false then
	cb_modifica.enabled = false
	cb_elimina.enabled = false
	ib_modifica = false
	dw_colli.reset()
	cb_cancella.enabled = true
	if lb_trovato = true then
		cb_avanti.enabled = true		
	end if	
end if	

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione_pl.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report_pl.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

dw_eti_colli.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("APICE","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("APICE","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_pl.modify(ls_modify)
dw_eti_colli.modify(ls_modify)
									 

dw_selezione_pl.change_dw_current()
dw_selezione_pl.setfocus()
postevent("ue_imposta_dw")
postevent("ue_posiziona_w")
dw_packing_list.setrowfocusindicator(hand!)
end event

on w_packing_list.create
int iCurrent
call super::create
this.cb_aggiorna=create cb_aggiorna
this.dw_selezione_pl=create dw_selezione_pl
this.cb_cancella=create cb_cancella
this.dw_packing_list=create dw_packing_list
this.dw_colli=create dw_colli
this.dw_report_pl=create dw_report_pl
this.cb_avanti=create cb_avanti
this.st_1=create st_1
this.dw_eti_colli=create dw_eti_colli
this.cb_salva=create cb_salva
this.cb_ritorna=create cb_ritorna
this.cb_modifica=create cb_modifica
this.cb_indietro=create cb_indietro
this.cb_stampa=create cb_stampa
this.cb_elimina=create cb_elimina
this.cb_stampa_pl=create cb_stampa_pl
this.cb_etichette=create cb_etichette
this.cb_stampa_eti=create cb_stampa_eti
this.em_num_eti=create em_num_eti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_aggiorna
this.Control[iCurrent+2]=this.dw_selezione_pl
this.Control[iCurrent+3]=this.cb_cancella
this.Control[iCurrent+4]=this.dw_packing_list
this.Control[iCurrent+5]=this.dw_colli
this.Control[iCurrent+6]=this.dw_report_pl
this.Control[iCurrent+7]=this.cb_avanti
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.dw_eti_colli
this.Control[iCurrent+10]=this.cb_salva
this.Control[iCurrent+11]=this.cb_ritorna
this.Control[iCurrent+12]=this.cb_modifica
this.Control[iCurrent+13]=this.cb_indietro
this.Control[iCurrent+14]=this.cb_stampa
this.Control[iCurrent+15]=this.cb_elimina
this.Control[iCurrent+16]=this.cb_stampa_pl
this.Control[iCurrent+17]=this.cb_etichette
this.Control[iCurrent+18]=this.cb_stampa_eti
this.Control[iCurrent+19]=this.em_num_eti
end on

on w_packing_list.destroy
call super::destroy
destroy(this.cb_aggiorna)
destroy(this.dw_selezione_pl)
destroy(this.cb_cancella)
destroy(this.dw_packing_list)
destroy(this.dw_colli)
destroy(this.dw_report_pl)
destroy(this.cb_avanti)
destroy(this.st_1)
destroy(this.dw_eti_colli)
destroy(this.cb_salva)
destroy(this.cb_ritorna)
destroy(this.cb_modifica)
destroy(this.cb_indietro)
destroy(this.cb_stampa)
destroy(this.cb_elimina)
destroy(this.cb_stampa_pl)
destroy(this.cb_etichette)
destroy(this.cb_stampa_eti)
destroy(this.em_num_eti)
end on

type cb_aggiorna from commandbutton within w_packing_list
integer x = 3081
integer y = 144
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;long ll_num_pl, ll_num_registrazione, ll_scelta

if ib_pl_modified = true or ib_collo_modified = true then
	ll_scelta = g_mb.messagebox("Attenzione","Il packing list è stato modificato. Continuare?",question!,yesno!,2)
	if ll_scelta = 2 then return -1
end if	

dw_selezione_pl.accepttext()
ll_num_registrazione = dw_selezione_pl.getitemnumber(1,"num_registrazione")
ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")

if (isnull(ll_num_registrazione) or ll_num_registrazione = 0) and (isnull(ll_num_pl) or ll_num_pl = 0) then
	g_mb.messagebox("Dati incompleti","E' necessario selezionare un ordine oppure un packing list!")
	return -1
elseif isnull(ll_num_pl) or ll_num_pl = 0 then
	dw_selezione_pl.setitem(1,"num_pl",0)
	ib_nuovo_pl = true
	wf_carica_ordine()
	dw_packing_list.object.quantita_pl.protect = false
	dw_packing_list.object.collo.protect = false
else
	dw_packing_list.object.quantita_pl.protect = true
	dw_packing_list.object.collo.protect = true
	ib_nuovo_pl = false
	dw_colli.object.altezza.protect = true
	dw_colli.object.larghezza.protect = true
	dw_colli.object.profondita.protect = true
	dw_colli.object.peso.protect = true
	wf_carica_pl()
end if
dw_colli.reset()
cb_avanti.enabled = true
cb_cancella.enabled = true
ib_pl_modified = false
ib_collo_modified = false
end event

type dw_selezione_pl from uo_cs_xx_dw within w_packing_list
integer x = 23
integer width = 3451
integer height = 220
integer taborder = 10
string dataobject = "d_selezione_pl"
end type

event itemchanged;call super::itemchanged;string ls_cod_cliente, ls_where, ls_anno_reg, ls_num_reg

choose case i_colname
	case "cod_cliente"
		ls_cod_cliente = i_coltext
		ls_anno_reg = string(dw_selezione_pl.getitemnumber(1,"anno_registrazione"))
		dw_selezione_pl.setitem(1,"num_registrazione",0)
		dw_selezione_pl.setitem(1,"num_pl",0)
		if ls_cod_cliente <> "" and not isnull(ls_cod_cliente) then
			f_PO_LoadDDDW_DW (dw_selezione_pl,"num_registrazione",sqlca,"tes_ord_ven","num_registrazione",&
			"num_registrazione","cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " &
			+ ls_anno_reg + " and cod_cliente = '" + ls_cod_cliente + "'")
			
			f_PO_LoadDDDW_DW (dw_selezione_pl,"num_pl",sqlca,"tes_ord_ven_pack_list","num_pack_list",&
			"num_pack_list","cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " &
			+ ls_anno_reg + " and num_registrazione in(select num_registrazione from tes_ord_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + ls_anno_reg + " and cod_cliente = '" + ls_cod_cliente + "')")
		end if	
	case "anno_registrazione"
		ls_cod_cliente = dw_selezione_pl.getitemstring(1,"cod_cliente")
		ls_anno_reg = i_coltext
		dw_selezione_pl.setitem(1,"num_registrazione",0)
		dw_selezione_pl.setitem(1,"num_pl",0)
		if ls_cod_cliente <> "" and not isnull(ls_cod_cliente) then
			f_PO_LoadDDDW_DW (dw_selezione_pl,"num_registrazione",sqlca,"tes_ord_ven","num_registrazione",&
			"num_registrazione","cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " &
			+ ls_anno_reg + " and cod_cliente = '" + ls_cod_cliente + "'")
		
			f_PO_LoadDDDW_DW (dw_selezione_pl,"num_pl",sqlca,"tes_ord_ven_pack_list","num_pack_list",&
			"num_pack_list","cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " &
			+ ls_anno_reg + " and num_registrazione in(select num_registrazione from tes_ord_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + ls_anno_reg + " and cod_cliente = '" + ls_cod_cliente + "')")
		end if
	case "num_registrazione"
		ls_cod_cliente = dw_selezione_pl.getitemstring(1,"cod_cliente")
		ls_anno_reg = string(dw_selezione_pl.getitemnumber(1,"anno_registrazione"))
		ls_num_reg = i_coltext
		dw_selezione_pl.setitem(1,"num_pl",0)
		if ls_num_reg <> "" and not isnull(ls_num_reg) and ls_num_reg <> "0" then
			f_PO_LoadDDDW_DW (dw_selezione_pl,"num_pl",sqlca,"tes_ord_ven_pack_list","num_pack_list",&
			"num_pack_list","cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " &
			+ ls_anno_reg + " and num_registrazione = " + ls_num_reg)
		elseif ls_cod_cliente <> "" and not isnull(ls_cod_cliente) then
			f_PO_LoadDDDW_DW (dw_selezione_pl,"num_pl",sqlca,"tes_ord_ven_pack_list","num_pack_list",&
			"num_pack_list","cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " &
			+ ls_anno_reg + " and num_registrazione in(select num_registrazione from tes_ord_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + ls_anno_reg + " and cod_cliente = '" + ls_cod_cliente + "')")
		end if
end choose
end event

event getfocus;call super::getfocus;s_cs_xx.parametri.parametro_uo_dw_1 = dw_selezione_pl
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione_pl,"cod_cliente")
end choose
end event

type cb_cancella from commandbutton within w_packing_list
integer x = 2697
integer y = 144
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "A&nnulla"
end type

event clicked;long ll_scelta

if ib_pl_modified = true or ib_collo_modified = true then
	ll_scelta = g_mb.messagebox("Attenzione","Il packing list è stato modificato. Continuare?",question!,yesno!,2)
	if ll_scelta = 2 then return -1
end if

dw_selezione_pl.setitem(1,"anno_registrazione",f_anno_esercizio())
dw_selezione_pl.setitem(1,"num_registrazione",0)
dw_selezione_pl.setitem(1,"num_pl",0)
dw_selezione_pl.setitem(1,"cod_cliente","")
dw_selezione_pl.object.b_ricerca_cliente.enabled = true
cb_modifica.enabled = false
cb_elimina.enabled = false
cb_avanti.enabled = false
cb_cancella.enabled = false
dw_packing_list.object.quantita_pl.protect = false
dw_packing_list.object.collo.protect = false
dw_colli.object.altezza.protect = false
dw_colli.object.larghezza.protect = false
dw_colli.object.profondita.protect = false
dw_colli.object.peso.protect = false
dw_packing_list.reset()
dw_colli.reset()
ib_modifica = false
ib_pl_modified = false
ib_collo_modified = false
end event

type dw_packing_list from datawindow within w_packing_list
event ue_elimina_righe ( )
integer x = 23
integer y = 260
integer width = 3451
integer height = 1460
integer taborder = 30
string dataobject = "d_packing_list"
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_elimina_righe();dec{4} ld_quantita_pl, ld_quantita_residua
string ls_cod_prodotto, ls_des_prodotto, ls_misura
long ll_num_registrazione, ll_anno_registrazione, ll_prog_riga, ll_i,&
     ll_originale, ll_prog_riga_check, ll_scelta

ld_quantita_pl = dw_packing_list.getitemnumber(getrow(),"quantita_pl")
ld_quantita_residua = dw_packing_list.getitemnumber(getrow(),"quantita_residua")
ls_cod_prodotto = dw_packing_list.getitemstring(getrow(),"cod_prodotto")
ls_des_prodotto = dw_packing_list.getitemstring(getrow(),"des_prodotto")
ls_misura = dw_packing_list.getitemstring(getrow(),"unita_misura")
ll_num_registrazione = dw_packing_list.getitemnumber(getrow(),"num_reg")
ll_anno_registrazione = dw_packing_list.getitemnumber(getrow(),"anno_reg")
ll_prog_riga = dw_packing_list.getitemnumber(getrow(),"riga")

if ld_quantita_pl > ld_quantita_residua then

	/*
	ll_scelta = messagebox("Attenzione","La quantità digitata è maggiore della quantità disponibile. Si desidera accettare comunque il dato?",question!,yesno!,2)
	if ll_scelta = 2 then return 1
	*/

elseif ld_quantita_pl <= ld_quantita_residua then
	
	for ll_i = (getrow() + 1) to dw_packing_list.rowcount()
		
		ll_prog_riga_check = dw_packing_list.getitemnumber(ll_i,"riga")
		ll_originale = dw_packing_list.getitemnumber(ll_i,"originale")
		
		if ll_prog_riga_check = ll_prog_riga and ll_originale = 0 then
			dw_packing_list.deleterow(ll_i)
			ll_i = getrow()
		end if	
		
	next	
	
	if ld_quantita_residua - ld_quantita_pl <> 0 then
		
		dw_packing_list.insertrow(getrow() + 1)
		dw_packing_list.setitem(getrow() + 1,"cod_prodotto",ls_cod_prodotto)
		dw_packing_list.setitem(getrow() + 1,"des_prodotto",ls_des_prodotto)
		dw_packing_list.setitem(getrow() + 1,"unita_misura",ls_misura)
		dw_packing_list.setitem(getrow() + 1,"quantita_residua",ld_quantita_residua - ld_quantita_pl)	
		dw_packing_list.setitem(getrow() + 1,"num_reg",ll_num_registrazione)
		dw_packing_list.setitem(getrow() + 1,"anno_reg",ll_anno_registrazione)
		dw_packing_list.setitem(getrow() + 1,"riga",ll_prog_riga)
		dw_packing_list.setitem(getrow() + 1,"originale",0)
		
	end if
	
end if
end event

event itemchanged;choose case dwo.name

	case "quantita_pl"
		
		postevent("ue_elimina_righe")	
		
end choose

ib_pl_modified = true
end event

event itemfocuschanged;selecttext(1,len(gettext()))
end event

type dw_colli from datawindow within w_packing_list
boolean visible = false
integer x = 23
integer y = 20
integer width = 3451
integer height = 1700
integer taborder = 10
string dataobject = "d_colli"
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;ib_collo_modified = true
end event

event itemfocuschanged;selecttext(1,len(gettext()))
end event

type dw_report_pl from uo_cs_xx_dw within w_packing_list
boolean visible = false
integer x = 23
integer y = 20
integer width = 3451
integer height = 1700
integer taborder = 10
string dataobject = "d_report_pl"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type cb_avanti from commandbutton within w_packing_list
integer x = 3109
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "A&vanti"
end type

event clicked;long ll_i, ll_j, ll_num_collo_inserito, ll_num_collo, ll_riga,&
	  ll_anno_reg, ll_num_reg, ll_num_pl, ll_prog, ll_prog_next, ll_num_collo_next
boolean lb_trovato, lb_collo
dec{4} ld_altezza, ld_larghezza, ld_profondita, ld_peso, ld_quantita_pl;

if dw_packing_list.rowcount() = 0 then	return -1

dw_packing_list.accepttext()

lb_collo = false

dw_colli.reset()

if (ib_nuovo_pl = false) or (ib_modifica = true) then

	ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")
	
	select anno_registrazione, num_registrazione
	into :ll_anno_reg, :ll_num_reg
	from tes_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_pack_list = :ll_num_pl;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		return -1
	end if	
			
	declare colli cursor for
	select num_collo, altezza, larghezza, profondita, peso
	from tes_ord_ven_pack_list_colli
	where cod_azienda= :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_reg and
			num_registrazione = :ll_num_reg and
			num_pack_list = :ll_num_pl
	order by num_collo;
	
	open colli;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella open del cursore colli. Dettaglio = " + sqlca.sqlerrtext)
		return -1
	end if
	
	do while true
		fetch colli into :ll_num_collo, :ld_altezza, :ld_larghezza, :ld_profondita, :ld_peso;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore nella fetch del cursore colli. Dettaglio = " + sqlca.sqlerrtext)
			close colli;
			rollback;
			return -1
		elseif sqlca.sqlcode = 100 then
			exit
		end if
	
		ll_riga = dw_colli.insertrow(0)	
		dw_colli.setitem(ll_riga,"num_collo",ll_num_collo)	
		dw_colli.setitem(ll_riga,"altezza",ld_altezza)
		dw_colli.setitem(ll_riga,"larghezza",ld_larghezza)		
		dw_colli.setitem(ll_riga,"profondita",ld_profondita)
		dw_colli.setitem(ll_riga,"peso",ld_peso)
		
	loop
	
	close colli;
	
end if	

if (ib_nuovo_pl = true) or (ib_modifica = true) then

	for ll_i = 1 to dw_packing_list.rowcount()
		ld_quantita_pl = dw_packing_list.getitemnumber(ll_i,"quantita_pl")
		ll_num_collo_inserito = dw_packing_list.getitemnumber(ll_i,"collo")
		if ll_i < dw_packing_list.rowcount() then
			ll_prog = dw_packing_list.getitemnumber(ll_i,"riga")			
			ll_prog_next = dw_packing_list.getitemnumber(ll_i + 1,"riga")
			ll_num_collo_next = dw_packing_list.getitemnumber(ll_i + 1,"collo")
			if ll_prog_next = ll_prog and ll_num_collo_next = ll_num_collo_inserito then
				g_mb.messagebox("Dati non validi","Nella riga numero " + string(ll_i + 1) + " si specifica di inserire nello stesso collo della riga precedente una quantità riferita allo stesso dettaglio ordine.~nUnire le due righe e riprovare.") 
				return -1
			end if	
		end if	
		if (ld_quantita_pl = 0 or isnull(ld_quantita_pl)) and (isnull(ll_num_collo_inserito)  or ll_num_collo_inserito = 0) then
			continue
		elseif (ld_quantita_pl = 0 or isnull(ld_quantita_pl)) or (isnull(ll_num_collo_inserito)  or ll_num_collo_inserito = 0) then
			g_mb.messagebox("Dati incompleti","Le righe modificate devono essere complete di quantita e numero collo")
			return -1	
		else		
			lb_collo = true
			lb_trovato = false
			for ll_j = 1 to dw_colli.rowcount()
				ll_num_collo = dw_colli.getitemnumber(ll_j,"num_collo")
				if ll_num_collo = ll_num_collo_inserito then
					lb_trovato = true
				end if
			next
			if lb_trovato = false then
				ll_riga = dw_colli.insertrow(0)
				dw_colli.setitem(ll_riga,"num_collo",ll_num_collo_inserito)
			end if	
		end if		
	next

	if lb_collo = false then
		g_mb.messagebox("Dati incompleti","Nessun collo impostato")
		return -1
	end if

end if

for ll_i = 1 to dw_colli.rowcount()
	
	lb_trovato = false
	
	ll_num_collo = dw_colli.getitemnumber(ll_i,"num_collo")
	
	for ll_j = 1 to dw_packing_list.rowcount()
		
		ll_num_collo_inserito = dw_packing_list.getitemnumber(ll_j,"collo")
		
		if ll_num_collo_inserito = ll_num_collo then
			lb_trovato = true
			exit
		end if	
		
	next
	
	if lb_trovato = false then
		dw_colli.deleterow(ll_i)
	end if	
	
next	

dw_selezione_pl.hide()
dw_packing_list.hide()
cb_avanti.hide()
cb_elimina.hide()
cb_modifica.hide()
dw_selezione_pl.object.b_ricerca_cliente.visible=false
cb_aggiorna.hide()
cb_cancella.hide()
dw_colli.show()
cb_salva.show()
cb_indietro.show()
cb_stampa_pl.show()
cb_stampa_eti.show()
dw_colli.setfocus()
ib_collo_modified = false
if ib_nuovo_pl = false then
	cb_salva.enabled = false
	cb_stampa_pl.enabled = true
	cb_stampa_eti.enabled = true
end if	
if (ib_nuovo_pl = true) or (ib_modifica = true) then
	cb_salva.enabled = true
	cb_stampa_pl.enabled = false
	cb_stampa_eti.enabled = false
end if	

dw_colli.selecttext(1,len(dw_colli.gettext()))
end event

type st_1 from statictext within w_packing_list
boolean visible = false
integer x = 1326
integer y = 1760
integer width = 617
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Etichette per ogni collo:"
boolean focusrectangle = false
end type

type dw_eti_colli from uo_cs_xx_dw within w_packing_list
boolean visible = false
integer x = 23
integer y = 20
integer width = 3451
integer height = 1700
integer taborder = 10
string dataobject = "d_eti_colli"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type cb_salva from commandbutton within w_packing_list
boolean visible = false
integer x = 3109
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Salva"
end type

event clicked;long ll_anno_reg, ll_num_reg, ll_num_pl, ll_tot_colli, ll_num_collo, ll_i, ll_prog_riga
dec{4} ld_tot_volume, ld_tot_peso, ld_altezza, ld_larghezza, ld_profondita, ld_peso,&
		 ld_quan_pl

dw_colli.accepttext()

//for ll_i = 1 to dw_colli.rowcount()
//	ld_altezza = dw_colli.getitemnumber(ll_i,"altezza")
//	ld_larghezza = dw_colli.getitemnumber(ll_i,"larghezza")
//	ld_profondita = dw_colli.getitemnumber(ll_i,"profondita")
//	ld_peso = dw_colli.getitemnumber(ll_i,"peso")
//	if ld_altezza = 0 or ld_larghezza = 0 or ld_profondita = 0 or ld_peso = 0 then
//		messagebox("Dati incompleti","Tutti i colli devono essere impostati prima di poter salvare il packing list")
//		return -1
//	end if	
//next

ll_anno_reg = dw_selezione_pl.getitemnumber(1,"anno_registrazione")
ll_num_reg = dw_selezione_pl.getitemnumber(1,"num_registrazione")

if ib_modifica = true then
	
	ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")
	
	delete from det_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_pack_list = :ll_num_pl;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella delete di det_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	delete from tes_ord_ven_pack_list_colli
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_pack_list = :ll_num_pl;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella delete di tes_ord_ven_pack_list_colli. Dettaglio = " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	delete from tes_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_pack_list = :ll_num_pl;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella delete di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
elseif ib_modifica = false then
	
	select max(num_pack_list)
	into :ll_num_pl
	from tes_ord_ven_pack_list;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		return -1
	end if

	if isnull(ll_num_pl) then ll_num_pl = 0
	ll_num_pl = ll_num_pl + 1

end if	

ll_tot_colli = dw_colli.rowcount()
ld_tot_volume = 0
ld_tot_peso = 0

for ll_i = 1 to dw_packing_list.rowcount()
	ld_quan_pl = dw_packing_list.getitemnumber(ll_i,"quantita_pl")
	ll_num_collo = dw_packing_list.getitemnumber(ll_i,"collo")
	ll_prog_riga = dw_packing_list.getitemnumber(ll_i,"riga")
	if ld_quan_pl = 0  or isnull(ld_quan_pl) or ll_num_collo = 0 or isnull(ll_num_collo) then
		continue
	else
		insert into det_ord_ven_pack_list
		(cod_azienda,anno_registrazione,num_registrazione,prog_riga_ord_ven,num_pack_list,quan_pack_list,num_collo)
		values (:s_cs_xx.cod_azienda,:ll_anno_reg,:ll_num_reg,:ll_prog_riga,:ll_num_pl,:ld_quan_pl,:ll_num_collo);
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore nella insert di det_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
	end if	
next

insert into tes_ord_ven_pack_list
(cod_azienda,anno_registrazione,num_registrazione,num_pack_list,tot_colli)
values (:s_cs_xx.cod_azienda,:ll_anno_reg,:ll_num_reg,:ll_num_pl,:ll_tot_colli);

dw_selezione_pl.setitem(1,"num_pl",ll_num_pl)

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella insert di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

for ll_i = 1 to dw_colli.rowcount()
	ll_num_collo = dw_colli.getitemnumber(ll_i,"num_collo")
	ld_altezza = dw_colli.getitemnumber(ll_i,"altezza")
	ld_larghezza = dw_colli.getitemnumber(ll_i,"larghezza")
	ld_profondita = dw_colli.getitemnumber(ll_i,"profondita")
	ld_peso = dw_colli.getitemnumber(ll_i,"peso")
	ld_tot_volume = ld_tot_volume + (ld_altezza * ld_larghezza * ld_profondita) / 1000000
	ld_tot_peso = ld_tot_peso + ld_peso
	insert into tes_ord_ven_pack_list_colli
	(cod_azienda,anno_registrazione,num_registrazione,num_pack_list,num_collo,altezza,larghezza,profondita,peso)
	values (:s_cs_xx.cod_azienda,:ll_anno_reg,:ll_num_reg,:ll_num_pl,:ll_num_collo,:ld_altezza,:ld_larghezza,:ld_profondita,:ld_peso);
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella insert di tes_ord_ven_pack_list_colli. Dettaglio = " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if	
next

ld_tot_volume = round(ld_tot_volume,4)

update tes_ord_ven_pack_list
set tot_volume = :ld_tot_volume, tot_peso =:ld_tot_peso 
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_reg and
		num_registrazione = :ll_num_reg and
		num_pack_list = :ll_num_pl;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella update di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

ib_pl_modified = false
ib_collo_modified = false
ib_nuovo_pl = false
ib_modifica = false
dw_packing_list.object.quantita_pl.protect = true
dw_packing_list.object.collo.protect = true
dw_colli.object.altezza.protect = true
dw_colli.object.larghezza.protect = true
dw_colli.object.profondita.protect = true
dw_colli.object.peso.protect = true
cb_salva.enabled = false
cb_elimina.enabled = true
cb_modifica.enabled = true
cb_stampa_pl.enabled = true
cb_stampa_eti.enabled = true
wf_carica_pl()
end event

type cb_ritorna from commandbutton within w_packing_list
boolean visible = false
integer x = 3109
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ritorna"
end type

event clicked;dw_report_pl.hide()
dw_eti_colli.hide()
cb_etichette.hide()
cb_ritorna.hide()
cb_stampa.hide()
cb_stampa.enabled=true
em_num_eti.hide()
st_1.hide()
cb_stampa_eti.show()
cb_stampa_pl.show()
cb_indietro.show()
cb_salva.show()
is_stampa = ""
end event

type cb_modifica from commandbutton within w_packing_list
integer x = 2720
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Modifica"
end type

event clicked;string ls_cod_prodotto, ls_des_prodotto, ls_misura, ls_cod_cliente
long ll_riga, ll_prog_riga, ll_anno_registrazione, ll_num_registrazione, ll_num_pl,&
	  ll_anno_reg_check, ll_num_reg_check, ll_riga_check, ll_i
dec{4} ld_quan_ordine, ld_quan_evasa, ld_quan_residua, ld_quan_altri_pl, ld_quan_check
boolean lb_trovato	  

ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")

select anno_registrazione, num_registrazione
into :ll_anno_registrazione, :ll_num_registrazione
from tes_ord_ven_pack_list
where cod_azienda = :s_cs_xx.cod_azienda and
		num_pack_list = :ll_num_pl;
		
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select cod_cliente
into :ls_cod_cliente
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if	
	
declare det_ord cursor for
select cod_prodotto, des_prodotto, quan_ordine, quan_evasa, prog_riga_ord_ven
from det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		(quan_ordine - quan_evasa) > 0;

open det_ord;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore det_ord. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if
		
dw_selezione_pl.setitem(1,"cod_cliente",ls_cod_cliente)

do while true
	
	fetch det_ord into :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_ordine, :ld_quan_evasa, :ll_prog_riga;
		
	if sqlca.sqlcode = 100 then
		close det_ord;
		exit
	elseif sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore det_ord. Dettaglio = " + sqlca.sqlerrtext)
		close det_ord;
		rollback;
		return -1
	end if
	
	select cod_misura_mag
	into :ls_misura
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tab_misure. Dettaglio = " + sqlca.sqlerrtext)
		close det_ord;
		return -1
	end if	

	select sum(quan_pack_list)
	into :ld_quan_altri_pl
	from det_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga and
			num_pack_list <> :ll_num_pl;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		close det_ord;
		return -1
	end if
	
	if isnull(ld_quan_altri_pl) then ld_quan_altri_pl = 0

	ld_quan_residua = ld_quan_ordine - ld_quan_evasa - ld_quan_altri_pl
	
	if ld_quan_residua = 0 then continue

	lb_trovato = false
	
	for ll_i = 1 to dw_packing_list.rowcount()
		ll_anno_reg_check = dw_packing_list.getitemnumber(ll_i,"anno_reg")
		ll_num_reg_check = dw_packing_list.getitemnumber(ll_i,"num_reg")
		ll_riga_check = dw_packing_list.getitemnumber(ll_i,"riga")
		ld_quan_check = dw_packing_list.getitemnumber(ll_i,"quantita_pl")
		if ll_anno_reg_check = ll_anno_registrazione and ll_num_reg_check = ll_num_registrazione and ll_riga_check = ll_prog_riga then
			dw_packing_list.setitem(ll_i,"quantita_residua",ld_quan_residua)
			if lb_trovato = false then
				dw_packing_list.setitem(ll_i,"originale",1)
			end if	
			ld_quan_residua = ld_quan_residua - ld_quan_check			
			lb_trovato = true
		end if	
	next
	
	if lb_trovato = false then
		ll_riga = dw_packing_list.insertrow(0)
		dw_packing_list.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
		dw_packing_list.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
		dw_packing_list.setitem(ll_riga,"unita_misura",ls_misura)
		dw_packing_list.setitem(ll_riga,"quantita_residua",ld_quan_residua)
		dw_packing_list.setitem(ll_riga,"num_reg",ll_num_registrazione)
		dw_packing_list.setitem(ll_riga,"anno_reg",ll_anno_registrazione)
		dw_packing_list.setitem(ll_riga,"riga",ll_prog_riga)
		dw_packing_list.setitem(ll_riga,"originale",1)
	end if
	
	if lb_trovato = true and ld_quan_residua > 0	then
		ll_riga = dw_packing_list.insertrow(ll_i + 1)
		dw_packing_list.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
		dw_packing_list.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
		dw_packing_list.setitem(ll_riga,"unita_misura",ls_misura)
		dw_packing_list.setitem(ll_riga,"quantita_residua",ld_quan_residua)
		dw_packing_list.setitem(ll_riga,"num_reg",ll_num_registrazione)
		dw_packing_list.setitem(ll_riga,"anno_reg",ll_anno_registrazione)
		dw_packing_list.setitem(ll_riga,"riga",ll_prog_riga)
		dw_packing_list.setitem(ll_riga,"originale",0)
	end if
	
loop

close det_ord;
	
ib_modifica = true
cb_salva.enabled = true
cb_modifica.enabled = false
dw_packing_list.object.quantita_pl.protect = false
dw_packing_list.object.collo.protect = false
dw_colli.object.altezza.protect = false
dw_colli.object.larghezza.protect = false
dw_colli.object.profondita.protect = false
dw_colli.object.peso.protect = false
	
end event

type cb_indietro from commandbutton within w_packing_list
boolean visible = false
integer x = 2720
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Indietro"
end type

event clicked;long ll_scelta

if ib_collo_modified = true then
	ll_scelta = g_mb.messagebox("Attenzione","Le modifiche non salvate verranno perse. Continuare?",question!,yesno!,2)
	if ll_scelta = 2 then return
end if

dw_colli.hide()
cb_indietro.hide()
cb_stampa_pl.hide()
cb_stampa_eti.hide()
dw_selezione_pl.show()
dw_packing_list.show()
cb_avanti.show()
cb_aggiorna.show()
cb_cancella.show()
dw_selezione_pl.object.b_ricerca_cliente.visible=true
cb_elimina.show()
cb_modifica.show()
dw_selezione_pl.change_dw_current()
dw_selezione_pl.setfocus()
end event

type cb_stampa from commandbutton within w_packing_list
boolean visible = false
integer x = 2720
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;if is_stampa = "pl" then
	dw_report_pl.print()
elseif is_stampa = "eti" then
	dw_eti_colli.print()
end if	
end event

type cb_elimina from commandbutton within w_packing_list
integer x = 2331
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Cancella"
end type

event clicked;long ll_scelta, ll_num_pl, ll_cont

ll_scelta = g_mb.messagebox("APICE","Si è sicuri di voler eliminare il packing list corrente?",question!,yesno!,2)

if ll_scelta = 1 then
	
	ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")
	
	select count(*)
	into   :ll_cont
	from   det_ord_ven_pack_list
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 num_pack_list = :ll_num_pl and
			 flag_spedito = 'S';
	
	if ll_cont > 0 then
		g_mb.messagebox("APICE","Non è possibile cancellare Packing List spediti anche se parzialmente!")
		rollback;
		return -1
	end if

	delete from det_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_pack_list = :ll_num_pl;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella delete di det_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	delete from tes_ord_ven_pack_list_colli
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_pack_list = :ll_num_pl;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella delete di tes_ord_ven_pack_list_colli. Dettaglio = " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	delete from tes_ord_ven_pack_list
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_pack_list = :ll_num_pl;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella delete di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	commit;
	
	dw_colli.reset()
	dw_packing_list.reset()
	cb_elimina.enabled = false
	cb_modifica.enabled = false
	dw_selezione_pl.setitem(1,"anno_registrazione",f_anno_esercizio())
	dw_selezione_pl.setitem(1,"num_registrazione",0)
	dw_selezione_pl.setitem(1,"num_pl",0)
	dw_selezione_pl.setitem(1,"cod_cliente","")
	dw_selezione_pl.object.num_pl.protect = false
	dw_selezione_pl.object.num_registrazione.protect = false
	dw_selezione_pl.object.anno_registrazione.protect = false
	dw_selezione_pl.object.cod_cliente.protect = false
	dw_selezione_pl.object.b_ricerca_cliente.enabled = true
	cb_modifica.enabled = false
	dw_packing_list.object.quantita_pl.protect = false
	dw_packing_list.object.collo.protect = false
	dw_colli.object.altezza.protect = false
	dw_colli.object.larghezza.protect = false
	dw_colli.object.profondita.protect = false
	dw_colli.object.peso.protect = false
	dw_packing_list.reset()
	ib_modifica = false
	ib_pl_modified = false
	
end if	
end event

type cb_stampa_pl from commandbutton within w_packing_list
boolean visible = false
integer x = 2331
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Stampa &PL"
end type

event clicked;long 	  ll_num_pl, ll_anno_registrazione, ll_num_registrazione, ll_num_collo, ll_prog_riga,&
	  	  ll_riga, ll_num_collo_prec[], ll_i, ll_j
	  
dec{4}  ld_quan_pl, ld_altezza, ld_larghezza, ld_profondita, ld_peso, ld_tot_volume, ld_tot_peso
		 
string  ls_cod_cliente, ls_rag_soc, ls_indirizzo, ls_localita, ls_cap, ls_provincia, ls_nazione,&
        ls_cod_prodotto, ls_des_prodotto, ls_misura, ls_rag_soc_dest, ls_indirizzo_dest, ls_localita_dest, &
		  ls_cap_dest, ls_provincia_dest, ls_nota_dettaglio, ls_frazione, ls_frazione_dest, ls_telefono
		 
boolean lb_fatto


ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")
is_stampa = "pl"
ld_tot_volume = 0
ld_tot_peso = 0
ll_i = 1

dw_report_pl.reset()

select anno_registrazione, num_registrazione
into :ll_anno_registrazione, :ll_num_registrazione
from tes_ord_ven_pack_list
where cod_azienda = :s_cs_xx.cod_azienda and
		num_pack_list = :ll_num_pl;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select cod_cliente, rag_soc_1, indirizzo, localita, cap, provincia, frazione
into :ls_cod_cliente, :ls_rag_soc_dest, :ls_indirizzo_dest, :ls_localita_dest, :ls_cap_dest, :ls_provincia_dest, :ls_frazione_dest
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select rag_soc_1, indirizzo, localita, cap, provincia, frazione, telefono
into :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_cap, :ls_provincia, :ls_frazione, :ls_telefono
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di anag_clienti. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select des_nazione
into :ls_nazione
from tab_nazioni
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_nazione = (select cod_nazione from anag_clienti where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :ls_cod_cliente);

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tab_nazioni. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ls_rag_soc_dest) then
	ls_rag_soc_dest = ""
end if

if isnull(ls_indirizzo_dest) then
	ls_indirizzo_dest = ""
end if

if isnull(ls_localita_dest) then
	ls_localita_dest = ""
end if

if isnull(ls_cap_dest) then
	ls_cap_dest = ""
end if

if isnull(ls_provincia_dest) then
	ls_provincia_dest = ""
end if

if ls_rag_soc_dest = "" and ls_indirizzo_dest = "" and ls_localita_dest = "" and ls_cap_dest = "" and ls_provincia_dest = "" then
	ls_rag_soc_dest = ls_rag_soc
	ls_indirizzo_dest = ls_indirizzo
	ls_localita_dest = ls_localita
	ls_cap_dest = ls_cap
	ls_provincia_dest = ls_provincia
	ls_frazione_dest = ls_frazione
end if	

declare report_pl cursor for
select quan_pack_list, num_collo, prog_riga_ord_ven
from det_ord_ven_pack_list
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		num_pack_list = :ll_num_pl
order by num_collo;

open report_pl;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore report_pl. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

do while true

	lb_fatto = false
	
	fetch report_pl into :ld_quan_pl, :ll_num_collo, :ll_prog_riga;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore report_pl. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		close report_pl;
		exit
	end if
	
	select cod_prodotto, des_prodotto, nota_dettaglio
	into :ls_cod_prodotto, :ls_des_prodotto, :ls_nota_dettaglio
	from det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		return -1
	end if
	
	select cod_misura_mag
	into :ls_misura
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di anag_prodotti. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		return -1
	end if

	select larghezza, altezza, profondita, peso
	into :ld_larghezza, :ld_altezza, :ld_profondita, :ld_peso
	from tes_ord_ven_pack_list_colli
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			num_pack_list = :ll_num_pl and
			num_collo = :ll_num_collo;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list_colli. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		return -1
	end if
	
	for ll_j = 1 to upperbound(ll_num_collo_prec)
		if ll_num_collo = ll_num_collo_prec[ll_j] then
			lb_fatto = true
		end if
	next	

	if lb_fatto = false then
		ld_tot_volume = ld_tot_volume + (ld_altezza*ld_larghezza*ld_profondita/1000000)
		ld_tot_peso = ld_tot_peso + ld_peso
		ll_num_collo_prec[ll_i] = ll_num_collo
		ll_i++
	end if

	if len(trim(ls_nota_dettaglio)) > 0 then ls_des_prodotto = ls_des_prodotto + "~r~n" + ls_nota_dettaglio

	ll_riga = dw_report_pl.insertrow(0)
	dw_report_pl.setitem(ll_riga,"anno_registrazione",ll_anno_registrazione)
	dw_report_pl.setitem(ll_riga,"num_registrazione",ll_num_registrazione)
	dw_report_pl.setitem(ll_riga,"rag_soc",ls_rag_soc)
	dw_report_pl.setitem(ll_riga,"indirizzo",ls_indirizzo)
	dw_report_pl.setitem(ll_riga,"indirizzo_2",ls_cap + " "+ ls_localita + " (" + ls_provincia + ")")
	dw_report_pl.setitem(ll_riga,"nazione",ls_nazione)
	dw_report_pl.setitem(ll_riga,"rag_soc_dest",ls_rag_soc_dest)
	dw_report_pl.setitem(ll_riga,"indirizzo_dest",ls_indirizzo_dest)
	dw_report_pl.setitem(ll_riga,"indirizzo_2_dest",ls_cap_dest + " " + ls_localita_dest + " (" + ls_provincia_dest + ")")
	dw_report_pl.setitem(ll_riga,"nazione_dest",ls_nazione)
	dw_report_pl.setitem(ll_riga,"num_ordine",ll_num_registrazione)
	dw_report_pl.setitem(ll_riga,"num_collo",ll_num_collo)
	dw_report_pl.setitem(ll_riga,"altezza",ld_altezza)
	dw_report_pl.setitem(ll_riga,"larghezza",ld_larghezza)
	dw_report_pl.setitem(ll_riga,"profondita",ld_profondita)
	dw_report_pl.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	dw_report_pl.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	dw_report_pl.setitem(ll_riga,"misura",ls_misura)
	dw_report_pl.setitem(ll_riga,"quantita",ld_quan_pl)
	dw_report_pl.setitem(ll_riga,"peso",ld_peso)
	dw_report_pl.setitem(ll_riga,"tot_volume",ld_tot_volume)
	dw_report_pl.setitem(ll_riga,"tot_peso",ld_tot_peso)	
	dw_report_pl.setitem(ll_riga,"frazione",ls_frazione)	
	dw_report_pl.setitem(ll_riga,"frazione_dest",ls_frazione_dest)
	dw_report_pl.setitem(ll_riga,"telefono",ls_telefono)
	dw_report_pl.setitem(ll_riga,"num_pl",ll_num_pl)
	
loop

close report_pl;

cb_salva.hide()
cb_indietro.hide()
cb_stampa_eti.hide()
cb_stampa_pl.hide()
dw_report_pl.change_dw_current()
dw_report_pl.setfocus()
dw_report_pl.object.datawindow.print.preview = 'Yes'
dw_report_pl.object.datawindow.print.preview.rulers = 'Yes'
dw_report_pl.show()
cb_ritorna.show()
cb_stampa.show()
end event

type cb_etichette from commandbutton within w_packing_list
boolean visible = false
integer x = 2331
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etichette"
end type

event clicked;long ll_riga, ll_num_eti, ll_i, ll_num_pl, ll_anno_registrazione, ll_num_registrazione,&
	  ll_num_collo
dec{4} ld_altezza, ld_larghezza, ld_profondita, ld_peso
string ls_cod_cliente, ls_rag_soc, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_nazione	  

dw_eti_colli.reset()

ll_num_eti = long(em_num_eti.text)

ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")

select anno_registrazione, num_registrazione
into :ll_anno_registrazione, :ll_num_registrazione
from tes_ord_ven_pack_list
where cod_azienda = :s_cs_xx.cod_azienda and
		num_pack_list = :ll_num_pl;
		
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if	

select cod_cliente, rag_soc_1, indirizzo, localita, cap, provincia
into :ls_cod_cliente, :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_cap, :ls_provincia
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		num_registrazione = :ll_num_registrazione and
		anno_registrazione = :ll_anno_registrazione;
		
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select des_nazione
into :ls_nazione
from tab_nazioni
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_nazione = (select cod_nazione from anag_clienti where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :ls_cod_cliente);

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tab_nazioni. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ls_rag_soc) then
	ls_rag_soc = ""
end if

if isnull(ls_indirizzo) then
	ls_indirizzo = ""
end if

if isnull(ls_localita) then
	ls_localita = ""
end if

if isnull(ls_cap) then
	ls_cap = ""
end if

if isnull(ls_provincia) then
	ls_provincia = ""
end if

if ls_rag_soc = "" and ls_indirizzo = "" and ls_localita = "" and ls_cap = "" and ls_provincia = "" then
	
	select rag_soc_1, indirizzo, localita, cap, provincia
	into :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_cap, :ls_provincia
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di anag_clienti. Dettaglio = " + sqlca.sqlerrtext)
		return -1
	end if
	
end if	

declare etichette_colli cursor for
select num_collo,altezza, larghezza, profondita, peso
from tes_ord_ven_pack_list_colli
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		num_pack_list = :ll_num_pl
order by num_collo;
		
open etichette_colli;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore etichette_colli. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch etichette_colli into :ll_num_collo, :ld_altezza, :ld_larghezza, :ld_profondita, :ld_peso;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore etichette_colli. Dettaglio = " + sqlca.sqlerrtext)
		close etichette_colli;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		close etichette_colli;
		exit		
	end if
	
	if ld_altezza = 0 then
		setnull(ld_altezza)
	end if
	
	if ld_larghezza = 0 then
		setnull(ld_larghezza)
	end if
	
	if ld_profondita = 0 then
		setnull(ld_profondita)
	end if
	
	if ld_peso = 0 then
		setnull(ld_peso)
	end if
	
	for ll_i = 1 to ll_num_eti
		ll_riga = dw_eti_colli.insertrow(0)
		dw_eti_colli.setitem(ll_riga,"num_collo",ll_num_collo)
		dw_eti_colli.setitem(ll_riga,"altezza",ld_altezza)
		dw_eti_colli.setitem(ll_riga,"larghezza",ld_larghezza)
		dw_eti_colli.setitem(ll_riga,"profondita",ld_profondita)
		dw_eti_colli.setitem(ll_riga,"peso",ld_peso)
		dw_eti_colli.setitem(ll_riga,"rag_soc",ls_rag_soc)
		dw_eti_colli.setitem(ll_riga,"indirizzo",ls_indirizzo)
		dw_eti_colli.setitem(ll_riga,"indirizzo_2",ls_cap + " " + ls_localita + " (" + ls_provincia + ")")
		dw_eti_colli.setitem(ll_riga,"nazione",ls_nazione)
	next
	
loop	

close etichette_colli;

cb_stampa.enabled=true
end event

type cb_stampa_eti from commandbutton within w_packing_list
integer x = 1943
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Stampa &et."
end type

event clicked;string ls_parametro

select flag
into   :ls_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TSE';

if sqlca.sqlcode <> 0 then
	ls_parametro = "N"
end if

if ls_parametro = "S" then
	// stampa etichetta su stampante termica con driver windows.
	s_cs_xx.parametri.parametro_i_1 = dw_selezione_pl.getitemnumber(1,"anno_registrazione")
	s_cs_xx.parametri.parametro_i_2 = dw_selezione_pl.getitemnumber(1,"num_pl")

	open(w_stampa_etichette_pl)
else

	is_stampa = "eti"
	cb_stampa.enabled=false
	cb_salva.hide()
	cb_indietro.hide()
	cb_stampa_eti.hide()
	cb_stampa_pl.hide()
	dw_eti_colli.change_dw_current()
	dw_eti_colli.setfocus()
	dw_eti_colli.show()
	cb_etichette.show()
	cb_ritorna.show()
	cb_stampa.show()
	em_num_eti.show()
	em_num_eti.text = "1"
	st_1.show()
	dw_eti_colli.reset()
	dw_eti_colli.object.datawindow.print.preview = 'Yes'
	dw_eti_colli.object.datawindow.print.preview.rulers = 'Yes'
end if
end event

type em_num_eti from editmask within w_packing_list
boolean visible = false
integer x = 1943
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
double increment = 1
string minmax = "1~~99"
end type

