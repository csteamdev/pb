﻿$PBExportHeader$w_post_it.srw
forward
global type w_post_it from w_cs_xx_principale
end type
type dw_post_it from uo_cs_xx_dw within w_post_it
end type
type dw_post_it_lista from uo_cs_xx_dw within w_post_it
end type
end forward

global type w_post_it from w_cs_xx_principale
integer width = 2386
integer height = 1852
string title = "Lista Post-It"
dw_post_it dw_post_it
dw_post_it_lista dw_post_it_lista
end type
global w_post_it w_post_it

type variables
boolean			ib_start_drag = false, ib_supervisore = false
end variables

on w_post_it.create
int iCurrent
call super::create
this.dw_post_it=create dw_post_it
this.dw_post_it_lista=create dw_post_it_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_post_it
this.Control[iCurrent+2]=this.dw_post_it_lista
end on

on w_post_it.destroy
call super::destroy
destroy(this.dw_post_it)
destroy(this.dw_post_it_lista)
end on

event pc_setwindow;call super::pc_setwindow;string				ls_flag_supervisore

dw_post_it_lista.set_dw_key("cod_azienda")



if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then ib_supervisore = true
else
	ib_supervisore = true
end if

if ib_supervisore then
	dw_post_it_lista.set_dw_options(		sqlca, pcca.null_object, c_default, c_default								)
	dw_post_it.set_dw_options(				sqlca, dw_post_it_lista, c_sharedata + c_scrollparent,c_default		)
	
else
	dw_post_it_lista.set_dw_options(			sqlca, &
														pcca.null_object, &
														c_nonew + &
														c_nomodify + &
														c_nodelete, &
														c_default)
														
	dw_post_it.set_dw_options(				sqlca,dw_post_it_lista,&
													c_nonew + &
													c_nomodify + &
													c_nodelete + &
													c_sharedata +  &
													c_scrollparent, &
													c_default	)
end if

iuo_dw_main = dw_post_it_lista
end event

type dw_post_it from uo_cs_xx_dw within w_post_it
integer x = 32
integer y = 900
integer width = 2281
integer height = 808
integer taborder = 20
string dataobject = "d_post_it"
borderstyle borderstyle = styleraised!
end type

event clicked;call super::clicked;


choose case dwo.name
	case "cf_start_drag"
		if ib_start_drag then
			drag(Begin!)
		else
			drag(End!)
		end if
		
end choose
end event

event constructor;call super::constructor;

this.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "cs_sep.ico"
end event

type dw_post_it_lista from uo_cs_xx_dw within w_post_it
integer x = 32
integer y = 28
integer width = 2281
integer height = 840
integer taborder = 10
string dataobject = "d_post_it_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long			ll_error

ll_error = retrieve(s_cs_xx.cod_azienda)

if ll_error < 0 then
   PCCA.Error = c_Fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;long		ll_index

for ll_index = 1 to rowcount()
	if isnull(getitemstring(ll_index, "cod_azienda")) or getitemstring(ll_index, "cod_azienda")="" then
		setitem(ll_index, "cod_azienda", s_cs_xx.cod_azienda)
	end if
next

end event

event pcd_new;call super::pcd_new;
ib_start_drag = false
end event

event pcd_view;call super::pcd_view;

ib_start_drag = true
end event

event pcd_modify;call super::pcd_modify;

ib_start_drag = false
end event

