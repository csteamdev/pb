﻿$PBExportHeader$w_report_marginalita_vendite.srw
$PBExportComments$Finestra report scadenze analisi campionamento
forward
global type w_report_marginalita_vendite from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_report_marginalita_vendite
end type
type cb_report from commandbutton within w_report_marginalita_vendite
end type
type dw_selezione from uo_cs_xx_dw within w_report_marginalita_vendite
end type
type dw_report from uo_cs_xx_dw within w_report_marginalita_vendite
end type
type dw_folder from u_folder within w_report_marginalita_vendite
end type
end forward

global type w_report_marginalita_vendite from w_cs_xx_principale
integer width = 3744
integer height = 2360
string title = "Report Marginalità Vendite"
boolean resizable = false
cb_reset cb_reset
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_marginalita_vendite w_report_marginalita_vendite

type variables

end variables

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string ls_null, ls_cod_fornitore, ls_cod_cliente, ls_cod_prodotto, ls_sql, ls_rag_soc_cliente_rif, ls_desc_prodotto_rif
datetime  ldt_data_vendita_da, ldt_data_vendita_a
decimal ld_sconto_1_acq, ld_sconto_2_acq, ld_sconto_1_ven, ld_sconto_2_ven
long      ll_riga, ll_index

setpointer(HourGlass!)
dw_selezione.accepttext()
dw_report.reset()

//// loghi del report ****************************************
//string ls_logos, ls_modify
//
//select parametri_azienda.stringa
//into   :ls_logos
//from   parametri_azienda
//where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//       parametri_azienda.flag_parametro = 'S' and &
//       parametri_azienda.cod_parametro = 'LOS';
//
//if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
//
//ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
//dw_report.modify(ls_modify)
//
//select parametri_azienda.stringa
//into   :ls_logos
//from   parametri_azienda
//where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//       parametri_azienda.flag_parametro = 'S' and &
//       parametri_azienda.cod_parametro = 'LOC';
//
//if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
//
//ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
//dw_report.modify(ls_modify)


setnull(ls_null)
ldt_data_vendita_da = dw_selezione.getitemdatetime( 1, "data_vendita_da")
ldt_data_vendita_a = dw_selezione.getitemdatetime( 1, "data_vendita_a")
ls_cod_fornitore = dw_selezione.getitemstring( 1, "cod_fornitore")
ls_cod_prodotto = dw_selezione.getitemstring( 1, "cod_prodotto")
ls_cod_cliente = dw_selezione.getitemstring( 1, "cod_cliente")

if ls_cod_fornitore="" or isnull(ls_cod_fornitore) then
	g_mb.messagebox("OMNIA","Selezionare il fornitore!")
	rollback;
	return -1
end if
if isnull(ldt_data_vendita_da) then
	g_mb.messagebox("OMNIA","Selezionare una data di inizio periodo!")
	rollback;
	return -1
end if
if isnull(ldt_data_vendita_a) then
	g_mb.messagebox("OMNIA","Selezionare una data di fine periodo!")
	rollback;
	return -1
end if

if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	select rag_soc_1
	into :ls_rag_soc_cliente_rif
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :ls_cod_cliente
	;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante la ricerca della ragione sociale del cliente:"+ sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
end if

if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
	select des_prodotto
	into :ls_desc_prodotto_rif
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :ls_cod_prodotto
	;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante la ricerca della descrizione del prodotto:"+ sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
end if

decimal ld_anno_reg_acq_cu, ld_num_reg_acq_cu, ld_prog_riga_acq_cu, ld_prezzo_acquisto_cu
decimal ld_anno_reg_ven_cu, ld_num_reg_ven_cu, ld_prog_riga_ven_cu
decimal ld_prezzo_vendita_cu, ld_quan_ordine_cu
string ls_cod_fornitore_cu, ls_des_fornitore_cu, ls_cod_cliente_cu, ls_des_cliente_cu
string ls_cod_prodotto_cu, ls_des_prodotto_cu, ls_cod_misura_cu

ls_sql = "SELECT " + &
				"det_ord_acq.anno_registrazione," + &
				"det_ord_acq.num_registrazione," + &
				"det_ord_acq.prog_riga_ordine_acq," + &
				"tes_ord_acq.cod_fornitore," + &
				"anag_fornitori.rag_soc_1," + &
				"det_ord_acq.prezzo_acquisto," + &
				"tes_ord_ven.cod_cliente," + &
				"anag_clienti.rag_soc_1," + &
				"det_ord_ven.anno_registrazione," + &
				"det_ord_ven.num_registrazione," + &
				"det_ord_ven.prog_riga_ord_ven," + &
				"det_ord_ven.cod_prodotto," + &
				"anag_prodotti.des_prodotto," + &
				"det_ord_ven.prezzo_vendita," + &
				"det_ord_ven.quan_ordine," + &
				"det_ord_ven.cod_misura," + &
				"det_ord_acq.sconto_1," + &
				"det_ord_acq.sconto_2," + &
				"det_ord_ven.sconto_1," + &
				"det_ord_ven.sconto_2 " + &
			"FROM det_ord_acq " + &
			"JOIN tes_ord_acq ON tes_ord_acq.cod_azienda=det_ord_acq.cod_azienda " + &
						"AND tes_ord_acq.anno_registrazione=det_ord_acq.anno_registrazione " + &
						"AND tes_ord_acq.num_registrazione=det_ord_acq.num_registrazione " + &
			"JOIN det_ord_ven ON det_ord_ven.cod_azienda=det_ord_acq.cod_azienda " + &
						"AND det_ord_ven.anno_reg_ord_acq=det_ord_acq.anno_registrazione " + &
						"AND det_ord_ven.num_reg_ord_acq= det_ord_acq.num_registrazione " + &
						"AND det_ord_ven.prog_riga_ord_acq= det_ord_acq.prog_riga_ordine_acq " + &
			"JOIN tes_ord_ven ON tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda " + &
						"AND tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione " + &
						"AND tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione " + &
			"JOIN anag_fornitori ON anag_fornitori.cod_azienda=tes_ord_acq.cod_azienda " + &
						"AND anag_fornitori.cod_fornitore=tes_ord_acq.cod_fornitore " + &
			"JOIN anag_clienti ON anag_clienti.cod_azienda=tes_ord_ven.cod_azienda " + &
						"AND anag_clienti.cod_cliente=tes_ord_ven.cod_cliente " + &
			"JOIN anag_prodotti ON anag_prodotti.cod_azienda=det_ord_ven.cod_azienda " + &
						"AND anag_prodotti.cod_prodotto=det_ord_ven.cod_prodotto " + &
			"WHERE  det_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
				"and tes_ord_acq.cod_fornitore = '"+ls_cod_fornitore+"' " + &
			"AND tes_ord_ven.data_registrazione >= '" + string(ldt_data_vendita_da, s_cs_xx.db_funzioni.formato_data) + "' " + &
			"AND tes_ord_ven.data_registrazione <= '" + string(ldt_data_vendita_a, s_cs_xx.db_funzioni.formato_data) + "' "

if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	ls_sql += "AND tes_ord_ven.cod_cliente='"+ls_cod_cliente+"' "
end if

if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
	ls_sql += "AND det_ord_acq.cod_prodotto='"+ls_cod_prodotto+"' "
end if

DECLARE cu_vendite DYNAMIC CURSOR FOR SQLSA;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cu_vendite;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA", "Errore durante l'apertura di cu_vendite: " + sqlca.sqlerrtext)
	setpointer(Arrow!)
	return -1
end if
	
ll_riga = 0
	
do while 1 = 1
		
	FETCH cu_vendite INTO 	:ld_anno_reg_acq_cu, 
										:ld_num_reg_acq_cu, 
										:ld_prog_riga_acq_cu,
										:ls_cod_fornitore_cu, 
										:ls_des_fornitore_cu,
										:ld_prezzo_acquisto_cu,
										:ls_cod_cliente_cu,
										:ls_des_cliente_cu,
										:ld_anno_reg_ven_cu,
										:ld_num_reg_ven_cu,
										:ld_prog_riga_ven_cu,
										:ls_cod_prodotto_cu,
										:ls_des_prodotto_cu,
										:ld_prezzo_vendita_cu,
										:ld_quan_ordine_cu,
										:ls_cod_misura_cu,
										:ld_sconto_1_acq,
										:ld_sconto_2_acq,
										:ld_sconto_1_ven,
										:ld_sconto_2_ven
	;
																	 
	if sqlca.sqlcode = 100 then exit
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_vendite: " + sqlca.sqlerrtext)
		close cu_vendite;
		setpointer(Arrow!)
		return -1
	end if
	
	ll_riga = dw_report.insertrow(0)
	dw_report.setitem(ll_riga, "anno_reg_ord_acq", ld_anno_reg_acq_cu)
	dw_report.setitem(ll_riga, "num_reg_ord_acq", ld_num_reg_acq_cu)
	dw_report.setitem(ll_riga, "prog_riga_ord_acq", ld_prog_riga_acq_cu)
	dw_report.setitem(ll_riga, "cod_fornitore", ls_cod_fornitore_cu)
	dw_report.setitem(ll_riga, "rag_soc_fornitore", ls_des_fornitore_cu)
	
	dw_report.setitem(ll_riga, "cod_cliente_rif", ls_cod_cliente)
	dw_report.setitem(ll_riga, "rag_soc_cliente_rif", ls_rag_soc_cliente_rif)
	dw_report.setitem(ll_riga, "cod_prodotto_rif", ls_cod_prodotto)
	dw_report.setitem(ll_riga, "des_prodotto_rif", ls_desc_prodotto_rif)
	
	//tenere conto dei due eventuali sconti
	if ld_sconto_1_acq > 0 then
		ld_prezzo_acquisto_cu = ld_prezzo_acquisto_cu - (ld_prezzo_acquisto_cu/100) * ld_sconto_1_acq
		if ld_sconto_2_acq > 0 then
			ld_prezzo_acquisto_cu = ld_prezzo_acquisto_cu - (ld_prezzo_acquisto_cu/100) * ld_sconto_2_acq
		end if
	end if
	dw_report.setitem(ll_riga, "prezzo_acquisto", ld_prezzo_acquisto_cu)
	
	dw_report.setitem(ll_riga, "cod_cliente", ls_cod_cliente_cu)
	dw_report.setitem(ll_riga, "rag_soc_cliente", ls_des_cliente_cu)
	dw_report.setitem(ll_riga, "anno_reg_ord_ven", ld_anno_reg_ven_cu)
	dw_report.setitem(ll_riga, "num_reg_ord_ven", ld_num_reg_ven_cu)
	dw_report.setitem(ll_riga, "prog_riga_ord_ven", ld_prog_riga_ven_cu)
	dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto_cu)
	dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto_cu)
	
	//tenere conto dei due eventuali sconti
	if ld_sconto_1_ven > 0 then
		ld_prezzo_vendita_cu = ld_prezzo_vendita_cu - (ld_prezzo_vendita_cu/100) * ld_sconto_1_ven
		if ld_sconto_2_ven > 0 then
			ld_prezzo_vendita_cu = ld_prezzo_vendita_cu - (ld_prezzo_vendita_cu/100) * ld_sconto_2_ven
		end if
	end if
	dw_report.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita_cu)
	
	dw_report.setitem(ll_riga, "quan_ordine", ld_quan_ordine_cu)
	dw_report.setitem(ll_riga, "cod_misura", ls_cod_misura_cu)
	
	dw_report.setitem(ll_riga, "data_rif_da", ldt_data_vendita_da)
	dw_report.setitem(ll_riga, "data_rif_a", ldt_data_vendita_a)
	
loop
	
CLOSE cu_vendite;	
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_vendite: " + sqlca.sqlerrtext)
	setpointer(Arrow!)
	return -1
end if

dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview = 'Yes'
dw_report.sort()

dw_folder.fu_selecttab(2)

dw_report.change_dw_current()

setpointer(Arrow!)

rollback;

return 0



end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

//select parametri_azienda.stringa
//into   :ls_path_logo
//from   parametri_azienda
//where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//       parametri_azienda.flag_parametro = 'S' and &
//       parametri_azienda.cod_parametro = 'LO5';
//
//if sqlca.sqlcode < 0 then messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
//
//ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
//dw_report.modify(ls_modify)


//string ls_logos
//
//select parametri_azienda.stringa
//into   :ls_logos
//from   parametri_azienda
//where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//       parametri_azienda.flag_parametro = 'S' and &
//       parametri_azienda.cod_parametro = 'LOS';
//
//if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
//
//ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
//dw_report.modify(ls_modify)
//
//select parametri_azienda.stringa
//into   :ls_logos
//from   parametri_azienda
//where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//       parametri_azienda.flag_parametro = 'S' and &
//       parametri_azienda.cod_parametro = 'LOC';
//
//if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
//
//ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
//dw_report.modify(ls_modify)

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_reset
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

cb_reset.postevent(clicked!)


end event

on w_report_marginalita_vendite.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_marginalita_vendite.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_mer",sqlca,&
//                 "tab_cat_mer","cod_cat_mer","des_cat_mer",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type cb_reset from commandbutton within w_report_marginalita_vendite
integer x = 1600
integer y = 520
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;datetime ldt_oggi, ldt_precedente
string ls_null
integer li_days_prec

setnull(ls_null)
li_days_prec = - 30

dw_selezione.setitem(1,"cod_fornitore",ls_null)
dw_selezione.setitem(1,"cod_prodotto",ls_null)
dw_selezione.setitem(1,"cod_cliente",ls_null)

ldt_oggi = datetime(today(),time("23:59:59"))
dw_selezione.setitem(1,"data_vendita_a",ldt_oggi)

ldt_precedente = datetime(RelativeDate (date(ldt_oggi), li_days_prec),time("00:00:00"))
dw_selezione.setitem(1,"data_vendita_da",ldt_precedente)
end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type cb_report from commandbutton within w_report_marginalita_vendite
integer x = 1984
integer y = 524
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;wf_report()
end event

type dw_selezione from uo_cs_xx_dw within w_report_marginalita_vendite
integer x = 23
integer y = 120
integer width = 2857
integer height = 540
integer taborder = 20
string dataobject = "d_sel_report_marginalita_ven_filtro"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_marginalita_vendite
integer x = 23
integer y = 120
integer width = 3657
integer height = 2140
integer taborder = 10
string dataobject = "d_report_marginalita_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_marginalita_vendite
integer width = 3707
integer height = 2272
integer taborder = 40
boolean border = false
end type

