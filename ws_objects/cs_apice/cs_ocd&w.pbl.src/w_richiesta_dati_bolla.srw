﻿$PBExportHeader$w_richiesta_dati_bolla.srw
forward
global type w_richiesta_dati_bolla from w_cs_xx_risposta
end type
type tab_1 from tab within w_richiesta_dati_bolla
end type
type tabpage_1 from userobject within tab_1
end type
type dw_lista_bolle_pronte from datawindow within tabpage_1
end type
type cb_nuova_bolla from commandbutton within tabpage_1
end type
type cb_5 from commandbutton within tabpage_1
end type
type cb_aggiungi_a_bolla from commandbutton within tabpage_1
end type
type dw_ext_dati_bolla from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
cb_5 cb_5
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
end type
type tabpage_2 from userobject within tab_1
end type
type dw_lista_fatture_pronte from datawindow within tabpage_2
end type
type cb_nuova_fattura from commandbutton within tabpage_2
end type
type cb_3 from commandbutton within tabpage_2
end type
type cb_aggiungi_a_fattura from commandbutton within tabpage_2
end type
type dw_ext_dati_fattura from datawindow within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_lista_fatture_pronte dw_lista_fatture_pronte
cb_nuova_fattura cb_nuova_fattura
cb_3 cb_3
cb_aggiungi_a_fattura cb_aggiungi_a_fattura
dw_ext_dati_fattura dw_ext_dati_fattura
end type
type tabpage_3 from userobject within tab_1
end type
type st_1 from statictext within tabpage_3
end type
type cb_1 from commandbutton within tabpage_3
end type
type cb_conferma from commandbutton within tabpage_3
end type
type cb_deseleziona from commandbutton within tabpage_3
end type
type cb_seleziona from commandbutton within tabpage_3
end type
type dw_righe_ordine from datawindow within tabpage_3
end type
type tabpage_3 from userobject within tab_1
st_1 st_1
cb_1 cb_1
cb_conferma cb_conferma
cb_deseleziona cb_deseleziona
cb_seleziona cb_seleziona
dw_righe_ordine dw_righe_ordine
end type
type tab_1 from tab within w_richiesta_dati_bolla
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
end forward

global type w_richiesta_dati_bolla from w_cs_xx_risposta
integer x = 0
integer y = 200
integer width = 3991
integer height = 1992
string title = "Creazione Bolle e Fatture da Ordini"
event ue_seleziona_tab ( )
tab_1 tab_1
end type
global w_richiesta_dati_bolla w_richiesta_dati_bolla

type variables
string  is_flag_bol_fat

boolean ib_anticipo, ib_evasione_parziale
long il_anno_registrazione[], il_num_registrazione[]

longlong il_prog_sessione

// stefanop 09/05/2010: progetto 140
private:
	string is_tipo_default = ""
	boolean ib_change_mov = false
end variables

forward prototypes
public subroutine wf_carica_righe_ordine ()
public function integer wf_controlla_movimenti (string as_cod_tipo_bol_fat, ref string as_errore)
public function longlong wf_get_prog_sessione ()
public function integer wf_controlla_depositi_doc (string as_tipo_doc, integer ai_anno_doc, long al_num_doc, integer ai_anno_ordine, long al_num_ordine, ref string as_messaggio)
public function integer wf_controlla_pianificazione (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, ref string as_errore)
end prototypes

event ue_seleziona_tab();choose case is_flag_bol_fat
	case "B"
		tab_1.tabpage_1.enabled = true
		tab_1.tabpage_2.enabled = false
		tab_1.SelectTab(1)
		tab_1.event clicked(1)
	case "F"
		tab_1.tabpage_1.enabled = false
		tab_1.tabpage_2.enabled = true
		tab_1.SelectTab(2)			
		tab_1.event clicked(2)
end choose
end event

public subroutine wf_carica_righe_ordine ();string ls_cod_prodotto, ls_cod_tipo_det_ven, ls_cod_misura, ls_des_prodotto, ls_cod_misura_mag
long ll_prog_riga_ord_ven,ll_num_commessa, ll_anno_commessa, ll_riga
dec{4} ld_quan_ordine, ld_prezzo_vendita, ld_val_riga, ld_quan_in_evasione, ld_quan_evasa, ld_quantita_um, ld_prezzo_um, ld_sconto_1, ld_sconto_2
datetime ldt_data_consegna

DECLARE cu_righe_ordini CURSOR FOR  
SELECT prog_riga_ord_ven, cod_prodotto,  cod_tipo_det_ven, cod_misura, des_prodotto, quan_ordine, prezzo_vendita, sconto_1, sconto_2, data_consegna, val_riga, quan_in_evasione, quan_evasa, anno_commessa, num_commessa, quantita_um, prezzo_um  
 FROM det_ord_ven  
WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( anno_registrazione = :il_anno_registrazione[1] ) AND  
		( num_registrazione = :il_num_registrazione[1] ) AND  
		( ( quan_ordine - quan_in_evasione - quan_evasa) > 0  ) 
ORDER BY prog_riga_ord_ven ASC;

open cu_righe_ordini;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore in open del cursore cu_righe_ordini " + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_righe_ordini into :ll_prog_riga_ord_ven, :ls_cod_prodotto, :ls_cod_tipo_det_ven, :ls_cod_misura, :ls_des_prodotto, :ld_quan_ordine, :ld_prezzo_vendita, :ld_sconto_1, :ld_sconto_2, :ldt_data_consegna, :ld_val_riga, :ld_quan_in_evasione, :ld_quan_evasa, :ll_anno_commessa, :ll_num_commessa, :ld_quantita_um, :ld_prezzo_um;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in lettura righe ordini " + sqlca.sqlerrtext)
		close cu_righe_ordini;
		rollback;
		return
	end if
	if sqlca.sqlcode = 100 then exit
	
	if not isnull(ls_cod_prodotto) then
		select cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
	end if	
	
	ll_riga = tab_1.tabpage_3.dw_righe_ordine.insertrow(0)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_tipo_det_ven", ls_cod_tipo_det_ven)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_misura_mag", ls_cod_misura_mag)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_misura_ven", ls_cod_misura)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_ordine", ld_quan_ordine)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quantita_um", ld_quantita_um)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "sconto_1", ld_sconto_1)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "sconto_2", ld_sconto_2)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prezzo_um", ld_prezzo_um)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_in_evasione", ld_quan_in_evasione)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_evasa", ld_quan_evasa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "val_riga", ld_val_riga)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "anno_commessa", ll_anno_commessa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "num_commessa", ll_num_commessa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "data_consegna", ldt_data_consegna)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_residua", ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_da_evadere", 0)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "flag_evasione_totale", "N")
loop
close cu_righe_ordini;
commit;
tab_1.selecttab(3)
return
end subroutine

public function integer wf_controlla_movimenti (string as_cod_tipo_bol_fat, ref string as_errore);/**
 * Stefano
 * 10/06/2010 
 *
 * Controllo se il tipo di movimento è lo steso oppure avviso e chiedo conferma all'utente del cambio
 * Ritorno:
 * -1 Errore
 *  1 SI
 *  2 NO
 **/
 
string ls_cod_tipo_movimento, ls_des_tipo_movimento, ls_flag_tipo_det_ven

// controllo il tipo det ven che deve essere "Prodotti a magazzino"
if is_flag_bol_fat = "B" then
	select flag_tipo_det_ven, cod_tipo_movimento
	into :ls_flag_tipo_det_ven, :ls_cod_tipo_movimento
	from tab_tipi_det_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_det_ven in (
			select cod_tipo_det_ven
			from tab_tipi_bol_ven
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_bol_ven = :as_cod_tipo_bol_fat
			);
else
	select flag_tipo_det_ven, cod_tipo_movimento
	into :ls_flag_tipo_det_ven, :ls_cod_tipo_movimento
	from tab_tipi_det_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_det_ven in (
			select cod_tipo_det_ven
			from tab_tipi_bol_ven
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_fat_ven = :as_cod_tipo_bol_fat
			);
end if

if sqlca.sqlcode <> 0 then
	as_errore = "Errore durante il controllo del tipo dettaglio.~r~n" + sqlca.sqlerrtext
	return -1
elseif isnull(ls_flag_tipo_det_ven) or ls_flag_tipo_det_ven <> 'M' then
	as_errore = "Attenzione: il tipo dettaglio deve consentire un movimento di magazzino"
	return -1
end if

// recupero il tipo di movimento per chiedere conferma sulla sostituzione
select des_tipo_movimento
into :ls_des_tipo_movimento
from tab_tipi_movimenti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_movimento = :ls_cod_tipo_movimento;
	
if sqlca.sqlcode <> 0 then
	as_errore = "Errore durante il recupero del dettaglio di movimento"
	return -1
end if
// ----

if g_mb.confirm("APICE", "Attenzione: si vuole sotituire i movimenti di magazzino con il seguente?~r~n" + ls_cod_tipo_movimento + " - " + ls_des_tipo_movimento) then
	return 1
else
	return 2
end if
end function

public function longlong wf_get_prog_sessione ();longlong			ll_id
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")
ll_id = longlong(ls_temp)

return ll_id
end function

public function integer wf_controlla_depositi_doc (string as_tipo_doc, integer ai_anno_doc, long al_num_doc, integer ai_anno_ordine, long al_num_ordine, ref string as_messaggio);
//controllo il deposito del ddt a cui voglio aggiungere
//Se l'ordine di vendita ha un giro di consegna con deposito, controlla che questo deposito coincida con quello del ddt a cui lo si vuole aggiungere

string				ls_cod_deposito, ls_cod_deposito_doc, ls_documento



if ai_anno_doc>0 and al_num_doc>0 and ai_anno_ordine>0 and al_num_ordine>0 then
else
	return 0
end if


select tes_giri_consegne.cod_deposito
into :ls_cod_deposito
from tes_ord_ven
join tes_giri_consegne on 	tes_giri_consegne.cod_azienda=tes_ord_ven.cod_azienda and
									tes_giri_consegne.cod_giro_consegna=tes_ord_ven.cod_giro_consegna
where 	tes_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
			tes_ord_ven.anno_registrazione=:ai_anno_ordine and
			tes_ord_ven.num_registrazione=:al_num_ordine;
			
if g_str.isnotempty(ls_cod_deposito) then
	//este un giro consegna con deposito specificato
	//leggi il deposito del documento fiscale
	
	choose case as_tipo_doc
		case "bol_ven"
			select cod_deposito
			into :ls_cod_deposito_doc
			from tes_bol_ven
			where	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:ai_anno_doc and
						num_registrazione=:al_num_doc;
		
		ls_documento = "del DdT di vendita"
		
		case "fat_ven"
			select cod_deposito
			into :ls_cod_deposito_doc
			from tes_fat_ven
			where	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:ai_anno_doc and
						num_registrazione=:al_num_doc;
			
			ls_documento = "della Fattura di vendita"
			
		case else	
			return 0
			
	end choose
	
	if ls_cod_deposito<>ls_cod_deposito_doc then
		if g_str.isnotempty(ls_cod_deposito_doc) then ls_cod_deposito_doc = "<NULL>"
		
		as_messaggio = 	"Il deposito " + ls_cod_deposito_doc + " " + ls_documento + " n. "+string(ai_anno_doc) + "/" + string(al_num_doc) + &
								" risulta diverso dal deposito di consegna dell'ordine n." + string(ai_anno_ordine) + "/" + string(al_num_ordine)+&
								"che risulta essere il deposito " + ls_cod_deposito
		
		return -1
	end if
	
	
end if


return 0
end function

public function integer wf_controlla_pianificazione (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, ref string as_errore);
long			ll_count


setnull(ll_count)

if isnull(as_cod_semilavorato) or as_cod_semilavorato="" then
	select count(*)
	into :ll_count
	from buffer_chiusura_commesse
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:ai_anno_commessa and
				num_commessa=:al_num_commessa and
				cod_semilavorato is null;
else
	select count(*)
	into :ll_count
	from buffer_chiusura_commesse
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:ai_anno_commessa and
				num_commessa=:al_num_commessa and
				cod_semilavorato=:as_cod_semilavorato;
end if

if sqlca.sqlcode<0 then
	//errore critico
	as_errore = "Errore controllo esistenza pianificazione commessa: "+sqlca.sqlerrtext
	return -1
	
elseif ll_count>0 then
	//esiste pianificazione
	return 0
	
else
	//NON esiste pianificazione
	//commessa ancora aperta: avvisa e blocca
	as_errore = "Esiste ancora la commessa che risulta aperta e non pianificata n°"+string(ai_anno_commessa)+"/"+string(al_num_commessa)+" associata ad una riga dell'ordine! "
	return 1
end if

end function

on w_richiesta_dati_bolla.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_richiesta_dati_bolla.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;string ls_cod_cliente, ls_cod_tipo_ord_ven

tab_1.tabpage_1.dw_lista_bolle_pronte.settransobject(sqlca)
tab_1.tabpage_1.dw_lista_bolle_pronte.setrowfocusindicator(FocusRect!)

tab_1.tabpage_2.dw_lista_fatture_pronte.settransobject(sqlca)
tab_1.tabpage_2.dw_lista_fatture_pronte.setrowfocusindicator(FocusRect!)

if s_cs_xx.parametri.parametro_s_10<>"" and not isnull(s_cs_xx.parametri.parametro_s_10) and isnumber(s_cs_xx.parametri.parametro_s_10) then
	il_prog_sessione = longlong(s_cs_xx.parametri.parametro_s_10)
else
	setnull(il_prog_sessione)
end if
s_cs_xx.parametri.parametro_s_10 = ""

// ---------------  in base al documento da creare attivo il folder appropriato ---------------

if s_cs_xx.parametri.parametro_s_5 = "PACKLIST" then
	il_anno_registrazione[] = s_cs_xx.parametri.parametro_d_1_a[]
	il_num_registrazione[] = s_cs_xx.parametri.parametro_d_2_a[]
else
	il_anno_registrazione[1] = s_cs_xx.parametri.parametro_ul_1
	il_num_registrazione[1] = s_cs_xx.parametri.parametro_ul_2
end if

select cod_cliente,
		 cod_tipo_ord_ven
into   :ls_cod_cliente,
		 :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = : il_anno_registrazione[1] and
		 num_registrazione = :il_num_registrazione[1];
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo ordine in testata ordine. "  + sqlca.sqlerrtext)
	postevent("close")
	return
end if

select flag_bol_fat
into   :is_flag_bol_fat
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca flag_bol_fat in anagrafica clienti. "  + sqlca.sqlerrtext)
	postevent("close")
	return
end if

if isnull(is_flag_bol_fat) then

	select flag_tipo_bol_fat
	into   :is_flag_bol_fat
	from   tab_tipi_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca flag_tipo_bol_fat in tabella tipi ordini. "  + sqlca.sqlerrtext)
		postevent("close")
		return
	end if
	
end if

if s_cs_xx.parametri.parametro_s_5 = "PARZIALE" then
	ib_evasione_parziale = true
else
	ib_evasione_parziale = false
end if

if	ib_evasione_parziale then
	tab_1.tabpage_1.enabled = false
	tab_1.tabpage_2.enabled = false
	tab_1.tabpage_3.enabled = true
	tab_1.tabpage_3.postevent("ue_carica_righe_ordine")
	return
end if

tab_1.postevent("clicked")
end event

event open;call super::open;if not ib_evasione_parziale then
	postevent("ue_seleziona_tab")
end if
end event

event close;call super::close;s_cs_xx.parametri.parametro_s_5 = ""
end event

type tab_1 from tab within w_richiesta_dati_bolla
integer x = 23
integer y = 24
integer width = 3922
integer height = 1852
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type

event clicked;long 	ll_riga, ll_cont_destinazioni
string ls_cod_cliente, ls_cod_des_cliente, ls_cod_causale, ls_cod_porto, ls_cod_mezzo, ls_cod_resa, ls_cod_vettore, ls_cod_inoltro, ls_nota_piede, &
		ls_aspetto_beni, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita,ls_provincia, ls_null, ls_num_ord_cliente, &
		ls_nota_testata, ls_nota_video, ls_cod_tipo_ord_ven, ls_cod_tipo_bol_ven, ls_cod_imballo,ls_cod_tipo_fat_ven
dec{4} ld_num_colli, ld_peso_lordo, ld_peso_netto
datetime ldt_oggi, ldt_data_ord_cliente


if isnull(index) then return 

ldt_oggi = datetime(today(), 00:00:00)
setnull(ls_null)

select cod_cliente, cod_des_cliente, cod_porto, cod_mezzo, cod_resa, cod_vettore, cod_inoltro, aspetto_beni, nota_piede,
		 rag_soc_1, rag_soc_2, indirizzo, frazione, cap, localita, provincia, num_colli, peso_lordo, peso_netto, cod_causale, num_ord_cliente, data_ord_cliente, cod_tipo_ord_ven, cod_imballo
into   :ls_cod_cliente, :ls_cod_des_cliente, :ls_cod_porto, :ls_cod_mezzo, :ls_cod_resa, :ls_cod_vettore, :ls_cod_inoltro, :ls_aspetto_beni, :ls_nota_piede,
		 :ls_rag_soc_1, :ls_rag_soc_2, :ls_indirizzo, :ls_frazione, :ls_cap, :ls_localita,:ls_provincia, :ld_num_colli, :ld_peso_lordo, :ld_peso_netto, :ls_cod_causale, :ls_num_ord_cliente, :ldt_data_ord_cliente, :ls_cod_tipo_ord_ven, :ls_cod_imballo
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione[1] and
		 num_registrazione = :il_num_registrazione[1];
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca ordine " + string(il_anno_registrazione[1]) + "/" + string(il_num_registrazione[1])+ ". Impossibile proseguire", stopsign!)
	return
end if

choose case is_flag_bol_fat
	case "B"
		tab_1.tabpage_1.enabled = true
		tab_1.tabpage_2.enabled = false
		tabpage_1.dw_lista_bolle_pronte.setredraw(false)
		tabpage_1.dw_ext_dati_bolla.setredraw(false)
		tabpage_1.dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)
		tabpage_1.dw_ext_dati_bolla.reset()
		ll_riga = tabpage_1.dw_ext_dati_bolla.insertrow(0)
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_destinazione", &
							  sqlca, &
							  "anag_des_clienti", &
							  "cod_des_cliente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_imballo", &
							  sqlca, &
							  "tab_imballi", &
							  "cod_imballo", &
							  "des_imballo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_vettore", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_inoltro", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_mezzo", &
							  sqlca, &
							  "tab_mezzi", &
							  "cod_mezzo", &
							  "des_mezzo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_porto", &
							  sqlca, &
							  "tab_porti", &
							  "cod_porto", &
							  "des_porto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_resa", &
							  sqlca, &
							  "tab_rese", &
							  "cod_resa", &
							  "des_resa", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_causale", &
							  sqlca, &
							  "tab_causali_trasp", &
							  "cod_causale", &
							  "des_causale", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

		//stefanop 08/06/2010: progetto 140: carico tipi bolla		
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
                 "cod_tipo_bolla", &
                 sqlca, &
                 "tab_tipi_bol_ven", &
                 "cod_tipo_bol_ven", &
                 "des_tipo_bol_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")	
		// ----
		
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
		
		if index = 1 then

			// 09-11-2016 Aggiunto da Enrico per controllo destinazione merce
			select count(*)
			into	:ll_cont_destinazioni
			from	anag_des_clienti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_cliente = :ls_cod_cliente and
					flag_dest_merce_fat = 'S' and
					flag_blocco = 'N';
			if ll_cont_destinazioni > 1 then
				g_mb.warning("APICE","Attenzione, per questo cliente esiste più di una destinazione merce.")
			elseif ll_cont_destinazioni = 1 and isnull(ls_cod_des_cliente) and (isnull(ls_rag_soc_1) or len(ls_rag_soc_1) = 0) and (isnull(ls_indirizzo) or len(ls_indirizzo) = 0) then
				g_mb.warning("APICE","Attenzione, questo cliente ha 1 destinazione merce che verrà impostata automaticamente!.")
				
				select cod_des_cliente,
						rag_soc_1,
						rag_soc_2,
						indirizzo,
						frazione,
						localita,
						cap,
						provincia
				into	:ls_cod_des_cliente,
						:ls_rag_soc_1,
						:ls_rag_soc_2,
						:ls_indirizzo,
						:ls_frazione,
						:ls_localita,
						:ls_cap,
						:ls_provincia
				from	anag_des_clienti
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_cliente = :ls_cod_cliente and
						flag_dest_merce_fat = 'S' and
						flag_blocco = 'N';
			end if
		end if
			
		
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_destinazione", ls_cod_des_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_causale", ls_cod_causale)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_porto", ls_cod_porto)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_resa", ls_cod_resa)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_imballo", ls_cod_imballo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "aspetto_beni", ls_aspetto_beni)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_2", ls_rag_soc_2)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "indirizzo", ls_indirizzo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "frazione", ls_frazione)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "localita", ls_localita)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cap", ls_cap)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "provincia", ls_provincia)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "num_colli", ld_num_colli)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "peso_netto", ld_peso_netto)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "peso_lordo", ld_peso_lordo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "num_ord_cliente", ls_num_ord_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "data_ord_cliente", ldt_data_ord_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "note_piede", ls_nota_piede)
		
		// stefanop 08/06/2010: cerco il tipo bolla vendita di default: progetto 140
		select cod_tipo_bol_ven
		into :ls_cod_tipo_bol_ven
		from tab_tipi_ord_ven
		where 
			cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven in (
				select cod_tipo_ord_ven 
				from tes_ord_ven 
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :il_anno_registrazione[1] and
					num_registrazione = :il_num_registrazione[1]);
		
		if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_bol_ven) then ls_cod_tipo_bol_ven = ""

		is_tipo_default = ls_cod_tipo_bol_ven
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_tipo_bolla", ls_cod_tipo_bol_ven)
		setnull(ls_cod_tipo_bol_ven)
		// ----
		
		tabpage_1.dw_lista_bolle_pronte.setredraw(true)
		tabpage_1.dw_ext_dati_bolla.setredraw(true)
		
		if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "BOL_VEN", ls_null, ldt_oggi, ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
		end if
		
		if isnull(ls_cod_causale) then
			select cod_tipo_bol_ven
			into   :ls_cod_tipo_bol_ven
			from   tab_tipi_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			if not isnull(ls_cod_tipo_bol_ven) then
				select cod_causale
				into   :ls_cod_causale
				from   tab_tipi_bol_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
				if not isnull(ls_cod_causale) then
					tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_causale", ls_cod_causale)
				end if
			end if
		end if
		
	case "F"
		tab_1.tabpage_1.enabled = false
		tab_1.tabpage_2.enabled = true
		tabpage_2.dw_lista_fatture_pronte.setredraw(false)
		tabpage_2.dw_ext_dati_fattura.setredraw(false)
		tabpage_2.dw_lista_fatture_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)
		tabpage_2.dw_ext_dati_fattura.reset()
		ll_riga = tabpage_2.dw_ext_dati_fattura.insertrow(0)
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_destinazione", &
							  sqlca, &
							  "anag_des_clienti", &
							  "cod_des_cliente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_imballo", &
							  sqlca, &
							  "tab_imballi", &
							  "cod_imballo", &
							  "des_imballo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_vettore", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_inoltro", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_mezzo", &
							  sqlca, &
							  "tab_mezzi", &
							  "cod_mezzo", &
							  "des_mezzo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_porto", &
							  sqlca, &
							  "tab_porti", &
							  "cod_porto", &
							  "des_porto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_resa", &
							  sqlca, &
							  "tab_rese", &
							  "cod_resa", &
							  "des_resa", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_causale", &
							  sqlca, &
							  "tab_causali_trasp", &
							  "cod_causale", &
							  "des_causale", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
				
		//stefanop 08/06/2010: progetto 140: carico tipi fattura		
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
                 "cod_tipo_bolla", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")	
		// ----
		
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
		
		// 09-11-2016 Aggiunto da Enrico per controllo destinazione merce
		if index = 2 then
			select count(*)
			into	:ll_cont_destinazioni
			from	anag_des_clienti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_cliente = :ls_cod_cliente and
					flag_dest_merce_fat = 'S';
					
			if ll_cont_destinazioni > 1 then
				g_mb.warning("APICE","Attenzione, per questo cliente esiste più di una destinazione merce.")
			elseif ll_cont_destinazioni = 1 and isnull(ls_cod_des_cliente) then
				g_mb.warning("APICE","Attenzione, questo cliente ha 1 destinazione merce che verrà impostata automaticamente!.")
				
				select cod_des_cliente,
						rag_soc_1,
						rag_soc_2,
						indirizzo,
						frazione,
						localita,
						cap,
						provincia
				into	:ls_cod_des_cliente,
						:ls_rag_soc_1,
						:ls_rag_soc_2,
						:ls_indirizzo,
						:ls_frazione,
						:ls_localita,
						:ls_cap,
						:ls_provincia
				from	anag_des_clienti
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_cliente = :ls_cod_cliente and
						flag_dest_merce_fat = 'S' and
						flag_blocco = 'N';
			end if
		end if
		
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_destinazione", ls_cod_des_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_causale", ls_cod_causale)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_porto", ls_cod_porto)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_resa", ls_cod_resa)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "aspetto_beni", ls_aspetto_beni)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "rag_soc_2", ls_rag_soc_2)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "indirizzo", ls_indirizzo)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "frazione", ls_frazione)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "localita", ls_localita)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cap", ls_cap)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "provincia", ls_provincia)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "num_colli", ld_num_colli)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "peso_netto", ld_peso_netto)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "peso_lordo", ld_peso_lordo)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "num_ord_cliente", ls_num_ord_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "data_ord_cliente", ldt_data_ord_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "note_piede", ls_nota_piede)
		
		// stefanop 08/06/2010: cerco il tipo fattura vendita di default: progetto 140
		select cod_tipo_fat_ven
		into :ls_cod_tipo_fat_ven
		from tab_tipi_ord_ven
		where 
			cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven in (
				select cod_tipo_ord_ven 
				from tes_ord_ven 
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :il_anno_registrazione[1] and
					num_registrazione = :il_num_registrazione[1]);
		
		if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_fat_ven) then ls_cod_tipo_fat_ven = ""

		is_tipo_default = ls_cod_tipo_fat_ven
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_tipo_bolla", ls_cod_tipo_fat_ven)
		setnull(ls_cod_tipo_fat_ven)
		// ----
		
		if isnull(ls_cod_causale) then
			select cod_tipo_fat_ven
			into   :ls_cod_tipo_fat_ven
			from   tab_tipi_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			if not isnull(ls_cod_tipo_fat_ven) then
				select cod_causale
				into   :ls_cod_causale
				from   tab_tipi_fat_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
				if not isnull(ls_cod_causale) then
					tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_causale", ls_cod_causale)
				end if
			end if
		end if
		
		tabpage_2.dw_lista_fatture_pronte.setredraw(true)
		tabpage_2.dw_ext_dati_fattura.setredraw(true)
end choose
		  
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 3886
integer height = 1724
long backcolor = 12632256
string text = "Bolle"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
cb_5 cb_5
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
end type

on tabpage_1.create
this.dw_lista_bolle_pronte=create dw_lista_bolle_pronte
this.cb_nuova_bolla=create cb_nuova_bolla
this.cb_5=create cb_5
this.cb_aggiungi_a_bolla=create cb_aggiungi_a_bolla
this.dw_ext_dati_bolla=create dw_ext_dati_bolla
this.Control[]={this.dw_lista_bolle_pronte,&
this.cb_nuova_bolla,&
this.cb_5,&
this.cb_aggiungi_a_bolla,&
this.dw_ext_dati_bolla}
end on

on tabpage_1.destroy
destroy(this.dw_lista_bolle_pronte)
destroy(this.cb_nuova_bolla)
destroy(this.cb_5)
destroy(this.cb_aggiungi_a_bolla)
destroy(this.dw_ext_dati_bolla)
end on

type dw_lista_bolle_pronte from datawindow within tabpage_1
integer width = 3877
integer height = 480
integer taborder = 20
string title = "none"
string dataobject = "d_richiesta_dati_bolla_lista_bolle"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;if isvalid(dwo) then
	choose case dwo.name
		case "b_stampa"
			tab_1.tabpage_1.cb_5.event clicked()
			
		case "b_aggiungi"
			tab_1.tabpage_1.cb_aggiungi_a_bolla.event clicked()
			
	end choose
end if
end event

type cb_nuova_bolla from commandbutton within tabpage_1
integer x = 5
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo DDT"
end type

event clicked;string ls_errore, ls_cod_cliente, ls_cod_tipo_bol_ven
int li_response
long ll_anno_bolla, ll_num_bolla, ll_i, ll_ret
uo_generazione_documenti luo_generazione_documenti

if g_mb.messagebox("APICE","Sei sicuro di voler creare una nuova bolla per tutte le righe assegnate di questo ordine?", Question!, YEsNo!, 2) = 1 then
	
	// stefanop 09/06/2010: controllo tipo bolla/fattura e se diverso dal default chiedo conferma per cambiare il movimento magazzino	
	ls_cod_tipo_bol_ven =  tab_1.tabpage_1.dw_ext_dati_bolla.getitemstring(1, "cod_tipo_bolla")
	if is_tipo_default <> ls_cod_tipo_bol_ven then
		li_response = wf_controlla_movimenti(ls_cod_tipo_bol_ven, ref ls_errore)
		choose case li_response
			case -1 // Errore
				g_mb.error("APICE", ls_errore)
				return -1
			case 1 // SI
				ib_change_mov = true
				
			case 2 // NO
				ib_change_mov = false
		end choose
	end if
	// ----
	
	
	
	dw_ext_dati_bolla.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_cliente")
	//stefanop 08/06/2010: progetto 140
	if is_tipo_default <> dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla") then
		luo_generazione_documenti.is_cod_tipo_bol_ven = dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla")
		luo_generazione_documenti.ib_change_det_ven = ib_change_mov
		luo_generazione_documenti.ib_change_mov = ib_change_mov
	end if
	// ----
	
	ll_anno_bolla = 0
	ll_num_bolla = 0
	
	if s_cs_xx.parametri.parametro_s_5 = "PACKLIST" then luo_generazione_documenti.is_rif_packing_list = "Rif.Packing List " + s_cs_xx.parametri.parametro_s_6
	
	//*** MICHELA 14/12/2005: SPECIFICA EUGANEA PANNELLI REPORT DDT FATTURE
	//                        solo se esiste il parametro flag aziendale 'EUG' a si allora procedo con l'ordinamento
	//                        delle righe in base alla specifica
		
	string ls_flag, ls_vuoto[]
		
	select flag
	into   :ls_flag
	from   parametri_azienda 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'EUG';
				 
	if sqlca.sqlcode = 0  and not isnull(ls_flag) and ls_flag = 'S' and UpperBound(s_cs_xx.parametri.parametro_s_1_a) > 0 then
	
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_pack(s_cs_xx.parametri.parametro_s_1_a, il_anno_registrazione, il_num_registrazione, ref ll_anno_bolla, ref ll_num_bolla, ref ls_errore)	
		s_cs_xx.parametri.parametro_s_1_a = ls_vuoto
		
	else
		
		// *** stefanop 21/07/2008: Modifiche per MRBoxer, per aggiornamento flag del portale
		int			li_mr_ret
		boolean 	lb_mrboxer = false
		
		select flag
		into    :ls_flag
		from   parametri_azienda
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_parametro = 'MRB';
				 
		if sqlca.sqlcode = 0 then
			if ls_flag = "S" and not isnull(ls_flag) then
				lb_mrboxer = true
			end if
		end if
	
		// Solo per MRBoxer mi connetto al database remoto
		if lb_mrboxer then
			
			uo_mrboxer luo_mrboxer
			luo_mrboxer = create uo_mrboxer
			
			li_mr_ret = luo_mrboxer.uof_connect_db()
			if li_mr_ret < 0 then
				rollback;
				messagebox("Apice" , "Impossibile contattare il server remoto")
			end if
			
		end if
			
		// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
		for ll_i = 1 to upperbound(il_anno_registrazione)
			
			ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_sessione(		il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																											ref ll_anno_bolla, ref ll_num_bolla, il_prog_sessione, ref ls_errore)
			if ll_ret <> 0 then exit
			
			// MRBoxer
			if lb_mrboxer then
				li_mr_ret = luo_mrboxer.uof_update_stato_ordine_portale(il_anno_registrazione[ll_i], il_num_registrazione[ll_i])
				
				if li_mr_ret < 0 then
					rollback;
				end if
			end if
		next
		
		// MRBoxer
		if lb_mrboxer then
			luo_mrboxer.uof_disconnect_db()
		end if
		// -----------------------------------------
			
	end if

	//*************** fine modifica
	
	// verifico se l'uscita è dovuta ad un errore e al limite faccio rollback di tutto.
	if ll_ret <> 0 then
		rollback;
		g_mb.messagebox("APICE","Errore durante la generazione della bolla. " + ls_errore)
		setnull(s_cs_xx.parametri.parametro_d_1)
		setnull(s_cs_xx.parametri.parametro_d_2)
	else
		commit;
		g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
		
		
		// *** michela: se i flag di stampa rif interscambio e rif vostro cliente sono a si creo due righe
		s_cs_xx.parametri.parametro_d_1 = ll_anno_bolla
		s_cs_xx.parametri.parametro_d_2 = ll_num_bolla
		// ***		
	end if
	destroy luo_generazione_documenti
	tab_1.event post clicked(1)
end if
end event

type cb_5 from commandbutton within tabpage_1
integer x = 2610
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa Bol."
end type

event clicked;long     ll_anno_reg_mov, ll_num_reg_mov, ll_progr_stock, ll_prog_registrazione

string   ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_nome_tabella, ls_cod_valuta, ls_test, &
         ls_messaggio, ls_window = "w_report_bol_ven_euro"

datetime ldt_data_stock

window   lw_window

uo_calcola_documento_euro luo_calcola_documento_euro


if dw_lista_bolle_pronte.rowcount() < 1 or dw_lista_bolle_pronte.getrow() < 1 then
	g_mb.messagebox("APICE","Nessuna bolla selezionata!")
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_num_registrazione")

declare cu_det_bol_ven cursor for
 select anno_reg_des_mov,
	     num_reg_des_mov
   from det_bol_ven
  where cod_azienda = :s_cs_xx.cod_azienda
    and anno_registrazione = :s_cs_xx.parametri.parametro_d_1
	 and num_registrazione = :s_cs_xx.parametri.parametro_d_2;

open cu_det_bol_ven;

do while 1 = 1 
	fetch cu_det_bol_ven into :ll_anno_reg_mov, :ll_num_reg_mov;
   
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Anno registrazione " + string(s_cs_xx.parametri.parametro_d_1) + "  num_registrazione " + string(s_cs_xx.parametri.parametro_d_2) + " non esistono in DET_BOL_VEV")
		exit
	end if	

	if sqlca.sqlcode = 0 then
		declare cu_des_mov_magazzino cursor for
		 select cod_deposito,
				  cod_ubicazione,
				  cod_lotto,
				  data_stock,
				  prog_stock,
				  prog_registrazione
			from dest_mov_magazzino	  
		  where cod_azienda = :s_cs_xx.cod_azienda
			 and anno_registrazione = :ll_anno_reg_mov
			 and num_registrazione = :ll_num_reg_mov;
			 
		open cu_des_mov_magazzino;
		
		do while 1 = 1 
			fetch cu_des_mov_magazzino into :ls_cod_deposito, :ls_cod_ubicazione, :ls_cod_lotto, :ldt_data_stock, :ll_progr_stock, :ll_prog_registrazione;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Errore", "Non è stato trovato nessun movimento di destinazione magazzino in corrispondenza dell'anno registrazione " + string(ll_anno_reg_mov) + " e del numero registrazione " + string(ll_num_reg_mov))
				exit
			end if
			if sqlca.sqlcode = 0 then
				if isnull(ls_cod_deposito) or isnull(ls_cod_ubicazione) or isnull(ls_cod_lotto) or &
					isnull(ldt_data_stock) or isnull(ll_progr_stock) then
					g_mb.messagebox("Attenzione", "Attenzione nella tabella DES_MOV_MAGAZZINO i dati dello stock corrispondenti all'anno registrazione " + string(ll_anno_reg_mov) + ", numero registrazione " + string(ll_num_reg_mov) + " e prog. registrazione " + string(ll_prog_registrazione) + " sono incompleti")
				end if
			end if	
		loop
		
		close cu_des_mov_magazzino;
	end if
loop
close cu_det_bol_ven;

s_cs_xx.parametri.parametro_i_1 = 0

if isnull(dw_lista_bolle_pronte.getitemstring(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_cod_documento")) then
	window_open(w_tipo_stampa_bol_ven, 0)
	if s_cs_xx.parametri.parametro_i_1 = -1 then
		rollback;
		return -1
	end if
end if

s_cs_xx.parametri.parametro_d_1 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_num_registrazione")

luo_calcola_documento_euro = create uo_calcola_documento_euro
	
if luo_calcola_documento_euro.uof_calcola_documento(s_cs_xx.parametri.parametro_d_1,s_cs_xx.parametri.parametro_d_2,"bol_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_calcola_documento_euro
	rollback;
	return -1
else
	commit;
end if	
	
destroy luo_calcola_documento_euro


select stringa
into   :ls_test
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'BVE';
				 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 then		
	window_open(w_report_bol_ven, -1)
elseif sqlca.sqlcode = 0 then			
	window_type_open(lw_window,ls_window, -1)
end if

close(w_richiesta_dati_bolla)
end event

type cb_aggiungi_a_bolla from commandbutton within tabpage_1
integer x = 2999
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;string ls_errore, ls_des_ord, ls_des_bol, ls_rag_soc_ord, ls_ind_ord, ls_cap_ord, ls_loc_ord, ls_prov_ord,&
		ls_rag_soc_bol, ls_ind_bol, ls_cap_bol, ls_loc_bol, ls_prov_bol,ls_cod_tipo_bol_ven
int li_response	 
long ll_anno_bolla, ll_num_bolla, ll_i, ll_ret
uo_generazione_documenti luo_generazione_documenti

if g_mb.messagebox("APICE","Sei sicuro di voler evadere tutte le righe asssegnate per questo ordine aggiungendole ad una bolla già esistente?", Question!, YEsNo!, 2) = 1 then
	
	// stefanop 09/06/2010: controllo tipo bolla/fattura e se diverso dal default chiedo conferma per cambiare il movimento magazzino	
	ls_cod_tipo_bol_ven =  tab_1.tabpage_1.dw_ext_dati_bolla.getitemstring(1, "cod_tipo_bolla")
	if is_tipo_default <> ls_cod_tipo_bol_ven then
		li_response = wf_controlla_movimenti(ls_cod_tipo_bol_ven, ref ls_errore)
		choose case li_response
			case -1 // Errore
				g_mb.error("APICE", ls_errore)
				return -1
			case 1 // SI
				ib_change_mov = true
				
			case 2 // NO
				ib_change_mov = false
		end choose
	end if
	// ----
		
	dw_ext_dati_bolla.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_cliente")
	//stefanop 08/06/2010: progetto 140
	if is_tipo_default <> dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla") then
		luo_generazione_documenti.is_cod_tipo_bol_ven = dw_ext_dati_bolla.getitemstring(1,"cod_tipo_bolla")
		luo_generazione_documenti.ib_change_det_ven = ib_change_mov
		luo_generazione_documenti.ib_change_mov = ib_change_mov
	end if
	// ----
	
	if dw_lista_bolle_pronte.getrow() > 0 then
		ll_anno_bolla = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_anno_registrazione")
		ll_num_bolla  = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_num_registrazione")
		
		//controllo il deposito del ddt a cui voglio aggiungere
		//Se l'ordine di vendita ha un giro di consegna con deposito, controlla che questo deposito coincida con quello del ddt a cui lo si vuole aggiungere
		li_response = wf_controlla_depositi_doc("bol_ven", ll_anno_bolla, ll_num_bolla, il_anno_registrazione[1], il_num_registrazione[1], ls_errore)
		if li_response<0 then
			g_mb.warning(ls_errore)
			destroy luo_generazione_documenti
			return
		end if
		
	else
		g_mb.messagebox("APICE","Selezionare una bolla fra la lista di quelle pronte")
		destroy luo_generazione_documenti
		return
	end if
	
	select rag_soc_1,
			 indirizzo,
			 cap,
			 localita,
			 provincia
	into   :ls_rag_soc_ord,
			 :ls_ind_ord,
			 :ls_cap_ord,
			 :ls_loc_ord,
			 :ls_prov_ord
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione[1] and
			 num_registrazione = :il_num_registrazione[1];
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella verifica delle destinazioni~nErrore nella select di tes_ord_ven: " + sqlca.sqlerrtext)
		destroy luo_generazione_documenti
		return
	end if
	
	select rag_soc_1,
			 indirizzo,
			 cap,
			 localita,
			 provincia
	into   :ls_rag_soc_bol,
			 :ls_ind_bol,
			 :ls_cap_bol,
			 :ls_loc_bol,
			 :ls_prov_bol
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_bolla and
			 num_registrazione = :ll_num_bolla;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella verifica delle destinazioni~nErrore nella select di tes_bol_ven: " + sqlca.sqlerrtext)
		destroy luo_generazione_documenti
		return
	end if
	

	if (ls_rag_soc_ord <> ls_rag_soc_bol) or &  
		(isnull(ls_rag_soc_bol) and not isnull(ls_rag_soc_ord)) or &
		(not isnull(ls_rag_soc_bol) and isnull(ls_rag_soc_ord)) or &
		(ls_ind_ord <> ls_ind_bol) or &
		(isnull(ls_ind_bol) and not isnull(ls_ind_ord)) or &
		(not isnull(ls_ind_bol) and isnull(ls_ind_ord)) or &
		(ls_cap_ord <> ls_cap_bol) or &
		(isnull(ls_cap_bol) and not isnull(ls_cap_ord)) or &
		(not isnull(ls_cap_bol) and isnull(ls_cap_ord)) or &
		(ls_loc_ord <> ls_loc_bol) or &
		(isnull(ls_loc_bol) and not isnull(ls_loc_ord)) or &
		(not isnull(ls_loc_bol) and isnull(ls_loc_ord)) or &
		(ls_prov_ord <> ls_prov_bol) or &
		(isnull(ls_prov_bol) and not isnull(ls_prov_ord)) or &
		(not isnull(ls_prov_bol) and isnull(ls_prov_ord)) 					then
			
			if g_mb.messagebox("APICE","ATTENZIONE: la destinazione della bolla selezionata non corrisponde alla " + &
							  "destinazione dell'ordine~nSI DESIDERA CONTINUARE UGUALMENTE?",question!,yesno!,2) = 2	then
				destroy luo_generazione_documenti
				return
			end if
			
	end if
		


	// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
	if s_cs_xx.parametri.parametro_s_5 = "PACKLIST" then luo_generazione_documenti.is_rif_packing_list = "Rif.Packing List " + s_cs_xx.parametri.parametro_s_6

	//*** MICHELA 14/12/2005: SPECIFICA EUGANEA PANNELLI REPORT DDT FATTURE
	//                        solo se esiste il parametro flag aziendale 'EUG' a si allora procedo con l'ordinamento
	//                        delle righe in base alla specifica
		
	string ls_flag, ls_vuoto[]
		
	select flag
	into   :ls_flag
	from   parametri_azienda 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'EUG';
				 
	if sqlca.sqlcode = 0  and not isnull(ls_flag) and ls_flag = 'S' and UpperBound(s_cs_xx.parametri.parametro_s_1_a) > 0 then
	
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_pack(s_cs_xx.parametri.parametro_s_1_a, il_anno_registrazione, il_num_registrazione, ref ll_anno_bolla, ref ll_num_bolla, ref ls_errore)	
		s_cs_xx.parametri.parametro_s_1_a = ls_vuoto
	else
		
		// *** stefanop 21/07/2008: Modifiche per MRBoxer, per aggiornamento flag del portale
		int			li_mr_ret
		boolean 	lb_mrboxer = false
		
		select flag
		into    :ls_flag
		from   parametri_azienda
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_parametro = 'MRB';
				 
		if sqlca.sqlcode = 0 then
			if ls_flag = "S" and not isnull(ls_flag) then
				lb_mrboxer = true
			end if
		end if
		
		// Solo per MRBoxer mi connetto al database remoto
		if lb_mrboxer then
			
			uo_mrboxer luo_mrboxer
			luo_mrboxer = create uo_mrboxer
			
			li_mr_ret = luo_mrboxer.uof_connect_db()
			if li_mr_ret < 0 then
				rollback;
				messagebox("Apice" , "Impossibile contattare il server remoto")
			end if
			
		end if

		for ll_i = 1 to upperbound(il_anno_registrazione)
			
			ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_sessione(		il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																											ref ll_anno_bolla, ref ll_num_bolla, il_prog_sessione, ref ls_errore)
			if ll_ret <> 0 then exit
			
			// MRBoxer
			if lb_mrboxer then
				li_mr_ret = luo_mrboxer.uof_update_stato_ordine_portale(il_anno_registrazione[ll_i], il_num_registrazione[ll_i])
				
				if li_mr_ret < 0 then
					rollback;
				end if
			end if
			
		next
		
		// MRBoxer
		if lb_mrboxer then
			luo_mrboxer.uof_disconnect_db()
		end if
		// -----------------------------------------
		
	end if
	// fine modifica 
	
	// verifico se l'uscita è per errore e al limite eseguo rollback di tutto
	if ll_ret <> 0 then
		destroy luo_generazione_documenti		
		rollback;
		g_mb.messagebox("APICE","Errore durante la generazione della bolla. " + ls_errore)
		
		setnull(s_cs_xx.parametri.parametro_d_1)
		setnull(s_cs_xx.parametri.parametro_d_2)		
		return
	else
		destroy luo_generazione_documenti
		commit;
		g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
		
		// *** michela: se i flag di stampa rif interscambio e rif vostro cliente sono a si creo due righe
		s_cs_xx.parametri.parametro_d_1 = ll_anno_bolla
		s_cs_xx.parametri.parametro_d_2 = ll_num_bolla
		// ***		
		return
	end if
	
end if

end event

type dw_ext_dati_bolla from datawindow within tabpage_1
integer x = 5
integer y = 488
integer width = 3872
integer height = 1080
integer taborder = 30
string title = "none"
string dataobject = "d_ext_dati_bolla_assegnazione"
boolean border = false
boolean livescroll = true
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 3886
integer height = 1724
long backcolor = 12632256
string text = "Fatture"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista_fatture_pronte dw_lista_fatture_pronte
cb_nuova_fattura cb_nuova_fattura
cb_3 cb_3
cb_aggiungi_a_fattura cb_aggiungi_a_fattura
dw_ext_dati_fattura dw_ext_dati_fattura
end type

on tabpage_2.create
this.dw_lista_fatture_pronte=create dw_lista_fatture_pronte
this.cb_nuova_fattura=create cb_nuova_fattura
this.cb_3=create cb_3
this.cb_aggiungi_a_fattura=create cb_aggiungi_a_fattura
this.dw_ext_dati_fattura=create dw_ext_dati_fattura
this.Control[]={this.dw_lista_fatture_pronte,&
this.cb_nuova_fattura,&
this.cb_3,&
this.cb_aggiungi_a_fattura,&
this.dw_ext_dati_fattura}
end on

on tabpage_2.destroy
destroy(this.dw_lista_fatture_pronte)
destroy(this.cb_nuova_fattura)
destroy(this.cb_3)
destroy(this.cb_aggiungi_a_fattura)
destroy(this.dw_ext_dati_fattura)
end on

type dw_lista_fatture_pronte from datawindow within tabpage_2
integer y = 4
integer width = 3877
integer height = 472
integer taborder = 20
string title = "none"
string dataobject = "d_richiesta_dati_bolla_lista_fatture"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;if isvalid(dwo) then
	choose case dwo.name
		case "b_stampa"
			tab_1.tabpage_2.cb_3.event clicked()
			
		case "b_aggiungi"
			tab_1.tabpage_2.cb_aggiungi_a_fattura.event clicked()
			
	end choose
end if
end event

type cb_nuova_fattura from commandbutton within tabpage_2
integer x = 5
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuova Fat."
end type

event clicked;string ls_errore, ls_cod_cliente, ls_cod_tipo_fat_ven
int li_response
long ll_anno_fattura, ll_num_fattura, ll_i, ll_ret
uo_generazione_documenti luo_generazione_documenti
	
if g_mb.messagebox("APICE","Sei sicuro di voler creare una nuova fattura per tutte le righe assegnate di questo ordine?", Question!, YEsNo!, 2) = 1 then

	// stefanop 09/06/2010: controllo tipo bolla/fattura e se diverso dal default chiedo conferma per cambiare il movimento magazzino	
	ls_cod_tipo_fat_ven = dw_ext_dati_fattura.getitemstring(1, "cod_tipo_bolla")
	if is_tipo_default <> ls_cod_tipo_fat_ven then
		li_response = wf_controlla_movimenti(ls_cod_tipo_fat_ven, ref ls_errore)
		choose case li_response
			case -1 // Errore
				g_mb.error("APICE", ls_errore)
				return -1
			case 1 // SI
				ib_change_mov = true
				
			case 2 // NO
				ib_change_mov = false
		end choose
	end if
	// ----
	
	dw_ext_dati_fattura.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_fattura.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_fattura.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_fattura.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_fattura.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_fattura.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_fattura.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_fattura.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_fattura.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_fattura.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_fattura.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_fattura.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_fattura.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_fattura.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_fattura.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_fattura.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_fattura.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_fattura.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_fattura.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_fattura.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_fattura.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_fattura.getitemdatetime(1,"data_ord_cliente")
	//stefanop 08/06/2010: progetto 140
	if is_tipo_default <> dw_ext_dati_fattura.getitemstring(1,"cod_tipo_bolla") then
		luo_generazione_documenti.is_cod_tipo_fat_ven = dw_ext_dati_fattura.getitemstring(1,"cod_tipo_bolla")
		luo_generazione_documenti.ib_change_det_ven = ib_change_mov
		luo_generazione_documenti.ib_change_mov = ib_change_mov
	end if
	// ----
	
	ll_anno_fattura = 0
	ll_num_fattura = 0
	
	// *** stefanop 21/07/2008: Modifiche per MRBoxer, per aggiornamento flag del portale
	string 	ls_flag
	int			li_mr_ret
	boolean 	lb_mrboxer = false
	
	select flag
	into    :ls_flag
	from   parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'MRB';
			 
	if sqlca.sqlcode = 0 then
		if ls_flag = "S" and not isnull(ls_flag) then
			lb_mrboxer = true
		end if
	end if
	
	// Solo per MRBoxer mi connetto al database remoto
	if lb_mrboxer then
		
		uo_mrboxer luo_mrboxer
		luo_mrboxer = create uo_mrboxer
		
		li_mr_ret = luo_mrboxer.uof_connect_db()
		if li_mr_ret < 0 then
			rollback;
			messagebox("Apice" , "Impossibile contattare il server remoto")
		end if
		
	end if
	
	// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
	for ll_i = 1 to upperbound(il_anno_registrazione)
		
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_fattura_sessione(	il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																										ref ll_anno_fattura, ref ll_num_fattura, il_prog_sessione, ref ls_errore)
		if ll_ret <> 0 then exit
		
		// MRBoxer
		if lb_mrboxer then
			li_mr_ret = luo_mrboxer.uof_update_stato_ordine_portale(il_anno_registrazione[ll_i], il_num_registrazione[ll_i])
			
			if li_mr_ret < 0 then
				rollback;
			end if
		end if
		
	next
	
	// MRBoxer
	if lb_mrboxer then
		luo_mrboxer.uof_disconnect_db()
	end if
	// -----------------------------------------
	
	// verifico se l'uscita è per errore
	if ll_ret <> 0 then
		rollback;
		g_mb.messagebox("APICE","Errore durante la generazione della fattura. " + ls_errore)
	else
		commit;
		g_mb.messagebox("APICE","E' stata generata la fattura " + string(ll_anno_fattura) + "-" + string(ll_num_fattura))
	end if
	destroy luo_generazione_documenti
	tab_1.event post clicked(2)
end if
end event

type cb_3 from commandbutton within tabpage_2
integer x = 2615
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa Fat."
end type

event clicked;string ls_messaggio

uo_calcola_documento_euro luo_calcola_documento_euro

if dw_lista_fatture_pronte.rowcount() < 1 or dw_lista_fatture_pronte.getrow() < 1 then
	g_mb.messagebox("APICE","Nessuna fattura selezionata!")
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2  = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")

s_cs_xx.parametri.parametro_i_1 = 0

if isnull(dw_lista_fatture_pronte.getitemstring(dw_lista_fatture_pronte.getrow(), "tes_fat_ven_cod_documento")) then
	window_open(w_tipo_stampa_fat_ven, 0)
	if s_cs_xx.parametri.parametro_i_1 = -1 then
		rollback;
		return -1
	end if
end if

s_cs_xx.parametri.parametro_d_1 = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2  = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")

luo_calcola_documento_euro = create uo_calcola_documento_euro
	
if luo_calcola_documento_euro.uof_calcola_documento(s_cs_xx.parametri.parametro_d_1,s_cs_xx.parametri.parametro_d_2,"fat_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_calcola_documento_euro
	rollback;
	return -1
else
	commit;		
end if
	
destroy luo_calcola_documento_euro

window_open(w_report_fat_ven_euro, -1)

close(w_richiesta_dati_bolla)
end event

type cb_aggiungi_a_fattura from commandbutton within tabpage_2
integer x = 3003
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;string ls_errore, ls_cod_tipo_fat_ven
int li_response
long ll_anno_fattura, ll_num_fattura, ll_ret, ll_i
uo_generazione_documenti luo_generazione_documenti
	
if g_mb.messagebox("APICE","Sei sicuro di voler evadere tutte le righe asssegnate per questo ordine aggiungendole ad una fattura già esistente?", Question!, YEsNo!, 2) = 1 then
	
	// stefanop 09/06/2010: controllo tipo bolla/fattura e se diverso dal default chiedo conferma per cambiare il movimento magazzino	
	ls_cod_tipo_fat_ven = dw_ext_dati_fattura.getitemstring(1, "cod_tipo_bolla")
	if is_tipo_default <> ls_cod_tipo_fat_ven then
		li_response = wf_controlla_movimenti(ls_cod_tipo_fat_ven, ref ls_errore)
		choose case li_response
			case -1 // Errore
				g_mb.error("APICE", ls_errore)
				return -1
			case 1 // SI
				ib_change_mov = true
				
			case 2 // NO
				ib_change_mov = false
		end choose
	end if
	// ----
	
	dw_ext_dati_fattura.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_fattura.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_fattura.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_fattura.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_fattura.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_fattura.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_fattura.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_fattura.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_fattura.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_fattura.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_fattura.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_fattura.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_fattura.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_fattura.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_fattura.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_fattura.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_fattura.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_fattura.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_fattura.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_fattura.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_fattura.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_fattura.getitemdatetime(1,"data_ord_cliente")
	//stefanop 08/06/2010: progetto 140
	if is_tipo_default <> dw_ext_dati_fattura.getitemstring(1,"cod_tipo_bolla") then
		luo_generazione_documenti.is_cod_tipo_fat_ven = dw_ext_dati_fattura.getitemstring(1,"cod_tipo_bolla")
		luo_generazione_documenti.ib_change_det_ven = ib_change_mov
		luo_generazione_documenti.ib_change_mov = ib_change_mov
	end if
	// --
	
	if dw_lista_fatture_pronte.getrow() > 0 then
		ll_anno_fattura = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
		ll_num_fattura  = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")
		
		//controllo il deposito del ddt a cui voglio aggiungere
		//Se l'ordine di vendita ha un giro di consegna con deposito, controlla che questo deposito coincida con quello del ddt a cui lo si vuole aggiungere
		li_response = wf_controlla_depositi_doc("fat_ven", ll_anno_fattura, ll_num_fattura, il_anno_registrazione[1], il_num_registrazione[1], ls_errore)
		if li_response<0 then
			g_mb.warning(ls_errore)
			destroy luo_generazione_documenti
			return
		end if
		
	else
		g_mb.messagebox("APICE","Selezionare una fattura fra la lista di quelle pronte")
	end if
	
	// *** stefanop 21/07/2008: Modifiche per MRBoxer, per aggiornamento flag del portale
	string 	ls_flag
	int			li_mr_ret
	boolean 	lb_mrboxer = false
	
	select flag
	into    :ls_flag
	from   parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'MRB';
			 
	if sqlca.sqlcode = 0 then
		if ls_flag = "S" and not isnull(ls_flag) then
			lb_mrboxer = true
		end if
	end if
	
	// Solo per MRBoxer mi connetto al database remoto
	if lb_mrboxer then
		
		uo_mrboxer luo_mrboxer
		luo_mrboxer = create uo_mrboxer
		
		li_mr_ret = luo_mrboxer.uof_connect_db()
		if li_mr_ret < 0 then
			rollback;
			messagebox("Apice" , "Impossibile contattare il server remoto")
		end if
	end if
	
	// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
	for ll_i = 1 to upperbound(il_anno_registrazione)
		
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_fattura_sessione(	il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																										ref ll_anno_fattura, ref ll_num_fattura, il_prog_sessione, ref ls_errore)
		if ll_ret <> 0 then exit
		
		// MRBoxer
		if lb_mrboxer then
			li_mr_ret = luo_mrboxer.uof_update_stato_ordine_portale(il_anno_registrazione[ll_i], il_num_registrazione[ll_i])
			if li_mr_ret < 0 then
				rollback;
			end if
		end if
	next
	// fine modifica
	
	// MRBoxer
	if lb_mrboxer then
		luo_mrboxer.uof_disconnect_db()
	end if
	// -----------------------------------------
	
	// verifico se l'uscita è per errore e al limite faccio rollback di tutto.
	if ll_ret <> 0 then
		destroy luo_generazione_documenti
		rollback;
		g_mb.messagebox("APICE","Errore durante la generazione della fattura. " + ls_errore)
		
		return
	else
		destroy luo_generazione_documenti
		commit;
		g_mb.messagebox("APICE","E' stata generata la fattura " + string(ll_anno_fattura) + "-" + string(ll_num_fattura))
		
		return
	end if
end if
end event

type dw_ext_dati_fattura from datawindow within tabpage_2
integer x = 5
integer y = 488
integer width = 3867
integer height = 1080
integer taborder = 20
string title = "none"
string dataobject = "d_ext_dati_bolla_assegnazione"
boolean border = false
boolean livescroll = true
end type

type tabpage_3 from userobject within tab_1
event ue_carica_righe_ordine ( )
integer x = 18
integer y = 112
integer width = 3886
integer height = 1724
long backcolor = 12632256
string text = "Righe Ordine"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_1 st_1
cb_1 cb_1
cb_conferma cb_conferma
cb_deseleziona cb_deseleziona
cb_seleziona cb_seleziona
dw_righe_ordine dw_righe_ordine
end type

event ue_carica_righe_ordine;wf_carica_righe_ordine()
end event

on tabpage_3.create
this.st_1=create st_1
this.cb_1=create cb_1
this.cb_conferma=create cb_conferma
this.cb_deseleziona=create cb_deseleziona
this.cb_seleziona=create cb_seleziona
this.dw_righe_ordine=create dw_righe_ordine
this.Control[]={this.st_1,&
this.cb_1,&
this.cb_conferma,&
this.cb_deseleziona,&
this.cb_seleziona,&
this.dw_righe_ordine}
end on

on tabpage_3.destroy
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.cb_conferma)
destroy(this.cb_deseleziona)
destroy(this.cb_seleziona)
destroy(this.dw_righe_ordine)
end on

type st_1 from statictext within tabpage_3
integer x = 919
integer y = 1608
integer width = 1678
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "SHIFT-F1 = SELEZIONA TUTTO     SHIFT-F2 = DESELEZIONA TUTTO"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within tabpage_3
integer x = 393
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Sped."
end type

event clicked;integer li_return
long ll_i, ll_prog_riga_ord_ven
string ls_messaggio
uo_generazione_documenti luo_gen_doc

for ll_i = 1 to dw_righe_ordine.rowcount()
	ll_prog_riga_ord_ven = dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
	setnull(ls_messaggio)
	
	luo_gen_doc = create uo_generazione_documenti
	li_return = luo_gen_doc.uof_azz_ass_ord_ven_sessione(il_anno_registrazione[1], il_num_registrazione[1], ll_prog_riga_ord_ven, 0, ls_messaggio)
	destroy luo_gen_doc
	
	if not isnull(ls_messaggio) then 
		g_mb.messagebox("Azzeramento assegnazione",ls_messaggio, information!)
	end if
	
	if li_return = 0 then
		g_mb.messagebox("Attenzione", "Azzeramento Assegnazione non avvenuta.", exclamation!)
		rollback;
		return
	end if
	g_mb.messagebox("Informazione", "Azzeramento Assegnazione avvenuta con successo.", information!)
next
commit;

end event

type cb_conferma from commandbutton within tabpage_3
integer x = 5
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spedisci"
end type

event clicked;string					ls_cod_tipo_bol_ven, ls_cod_tipo_fat_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, &
						ls_cod_cliente, ls_cod_giro_consegna, ls_cod_deposito_giro, ls_null
						
integer				li_return, li_ret

long					ll_return, ll_i, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa

decimal				ld_quan_commessa, ld_quan_evasa, ld_quan_in_evasione

string					ls_cod_prodotto, ls_flag_tipo_avanzamento, ls_msg

uo_generazione_documenti luo_gen_doc
uo_log_sistema		luo_log

boolean				lb_PCC = false, lb_ok = false




dw_righe_ordine.accepttext()
ll_return = g_mb.messagebox("APICE","Vuoi assegnare le righe selezionate ?", Question!, YesNo!, 1)
if ll_return = 2 then return

//genero un progressivo sessione univoco #########################
il_prog_sessione = wf_get_prog_sessione()
//#################################################

setnull(ls_cod_ubicazione)
select cod_deposito, cod_giro_consegna
into   :ls_cod_deposito, :ls_cod_giro_consegna
from   tes_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione[1] and
			num_registrazione = :il_num_registrazione[1];
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca codice deposito in testata ordine. " + sqlca.sqlerrtext)
	rollback;
	return
end if


//----------------------------------------------------------------------------------
//se l'ordine ha un giro consegna con deposito applicato e diverso da quello origine ordine, allora il deposito da assegnare deve essere questo
if g_str.isnotempty(ls_cod_giro_consegna)then

	select cod_deposito
	into :ls_cod_deposito_giro
	from tes_giri_consegne
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_giro_consegna = :ls_cod_giro_consegna;

	if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito<>ls_cod_deposito_giro then
		ls_cod_deposito = ls_cod_deposito_giro
		
		luo_log = create uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.EVAS.PARZ.", "Applicato deposito codice "+ls_cod_deposito_giro+" del  giro consegna dell'ordine "&
																								+string(il_anno_registrazione[1])+"/" + string(il_num_registrazione[1])+&
																						" al posto del deposito origine "+ls_cod_deposito+" in assegnazione ordine" + &
																						" da evasione parziale.")
		destroy luo_log
	end if
end if
//----------------------------------------------------------------------------------

//leggo il parametro di pianificazione chiusura commessa posticipata
guo_functions.uof_get_parametro_azienda( "PCC", lb_PCC)


for ll_i = 1 to dw_righe_ordine.rowcount()
	if  dw_righe_ordine.getitemnumber(ll_i, "quan_da_evadere") > 0 then
		
		ld_quan_evasa = 0
		ld_quan_commessa = 0
		ls_cod_tipo_det_ven = dw_righe_ordine.getitemstring(ll_i, "cod_tipo_det_ven")
		ll_prog_riga_ord_ven = dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
		
		//Donato 04/05/2010 controlla se alla riga da evadere è associata una commessa ancora aperta
		select anno_commessa, num_commessa, cod_prodotto
		into :ll_anno_commessa, :ll_num_commessa, :ls_cod_prodotto
		from det_ord_ven
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:il_anno_registrazione[1] and num_registrazione=:il_num_registrazione[1] and
				prog_riga_ord_ven=:ll_prog_riga_ord_ven;
		
		if sqlca.sqlcode<0 then
			g_mb.messagebox("APICE","Ordine " + string(il_anno_registrazione[1]) + "/" + string(il_num_registrazione[1]) + " riga nr." + string(ll_prog_riga_ord_ven) + "Errore in lettura commessa associata: operazione totalmente annullata!", stopsign! )
			rollback;
			return
		end if
		
		if ll_anno_commessa>0 and ll_num_commessa>0 then
			
			select flag_tipo_avanzamento
			into :ls_flag_tipo_avanzamento
			from anag_commesse
			where cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa;
				
			if sqlca.sqlcode<0 then
				ls_msg = "Errore di lettura avanzamento commesse! "
				ls_msg += "Riga ordine "+string(ll_prog_riga_ord_ven)+char(13)
				ls_msg += "Prodotto "+ls_cod_prodotto+char(13)
				ls_msg += "Commessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)+char(13)
				ls_msg += sqlca.sqlerrtext
				
				g_mb.messagebox("APICE",ls_msg, stopsign! )
				rollback;				
				return
			elseif sqlca.sqlcode=100 then
				ls_msg = "Commessa non trovata! "
				ls_msg += "Riga ordine "+string(ll_prog_riga_ord_ven)+char(13)
				ls_msg += "Prodotto "+ls_cod_prodotto+char(13)
				ls_msg += "Commessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)+char(13)
				
				g_mb.messagebox("APICE",ls_msg, stopsign! )
				rollback;
				return
			end if
			
			
			lb_ok = false
			
			//9 CHIUSA ASSEGN 		8 CHIUSA ATTIV		7 CHIUSA AUTOM
			if ls_flag_tipo_avanzamento<> "9" and ls_flag_tipo_avanzamento<>"8" and ls_flag_tipo_avanzamento<>"7" then
				
				//se la commessa è ancora aperta ma è attivo il sistema di posticipazione chiusura
				//ed esiste una pianificazione del nostro tipo (anno, numero NULL) allora non dare il messaggio ma fai andare avanti
				//nella generazione del documento
				if lb_PCC then
					//è attiva l'elaborazione posticipata delle commesse
					setnull(ls_null)
			
					//	-1	errore critico
					//	0	esiste pianificazione di chiusura, quindi vai pure avanti
					//	1	NON esiste pianificazione di chiusura, quindi non permetetre di andare avanti
					ls_msg = ""
					li_ret = wf_controlla_pianificazione(ll_anno_commessa, ll_num_commessa, ls_null, ls_msg)
					if li_ret<0 then
						lb_ok = false
						if g_str.isnotempty(ls_msg) then g_mb.warning(ls_msg)
						rollback;
						return
						
					elseif li_ret = 0 then
						//OK, la commessa non risulta chiusa ma c'è una pianificazione di chiusura
						lb_ok = true
					else
						//commessa ancora aperta: avvisa e blocca
						lb_ok = false
					end if
				else
					//commessa ancora aperta: avvisa e blocca (pianificazione chiusura non ammessa perchè il parametro PCC non esiste o vale N)
					lb_ok = false
				end if
				
				
				if not lb_ok then
					ls_msg = "Esiste ancora una commessa associata ad una riga dell'ordine che risulta aperta! "
					ls_msg += "Riga ordine "+string(ll_prog_riga_ord_ven)
					ls_msg += "~r~nProdotto "+ls_cod_prodotto + " - Commessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)
					g_mb.warning(ls_msg)
					rollback;
					return
				end if
				
				////commessa ancora aperta: avvisa e blocca
				//ls_msg ="Esiste ancora una commessa associata ad una riga dell'ordine che risulta aperta! "
				//ls_msg += "Riga ordine "+string(ll_prog_riga_ord_ven)+char(13)
				//ls_msg += "Prodotto "+ls_cod_prodotto+char(13)
				//ls_msg += "Commessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)+char(13)
				
				//g_mb.messagebox("APICE",ls_msg, stopsign! )
				//rollback;
				//return
			end if
		end if
		
			
		luo_gen_doc = create uo_generazione_documenti
		li_return = luo_gen_doc.uof_ass_ord_ven(	il_anno_registrazione[1], &
																il_num_registrazione[1], &
																ll_prog_riga_ord_ven, &
																ls_cod_tipo_det_ven, &
																ls_cod_deposito, &
																ls_cod_ubicazione, &
																dw_righe_ordine.getitemstring(ll_i, "cod_prodotto"), &
																dw_righe_ordine.getitemnumber(ll_i, "quan_da_evadere"), &
																0, &
																0,&
																il_prog_sessione)
		destroy luo_gen_doc
		
		if li_return = 3 then
			g_mb.messagebox("APICE","Ordine " + string(il_anno_registrazione[1]) + "/" + string(il_num_registrazione[1]) + " riga nr." + string(ll_prog_riga_ord_ven) + "~r~nNon è stato trovata suffiente quantità negli STOCK per evadere l'ordine: operazione totalmente annullata!", stopsign! )
			rollback;
			return
		end if
	end if
next

commit;
g_mb.messagebox("APICE","Assegnazione delle righe selezionate eseguita con successo!")

// ----------------------------- riaggiorno righe ordine ------------------------------------------------------
wf_carica_righe_ordine()

// ----------------------------- attivo la maschera di input dati bolla o fattura -----------------------------

tab_1.postevent("clicked")

w_richiesta_dati_bolla.postevent("ue_seleziona_tab")

return
end event

type cb_deseleziona from commandbutton within tabpage_3
integer x = 3104
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Deseleziona"
end type

event clicked;long ll_i

for ll_i = 1 to dw_righe_ordine.rowcount()
	dw_righe_ordine.setitem(ll_i, "flag_evasione_totale", "N")
	dw_righe_ordine.setitem(ll_i, "quan_da_evadere", 0)
next
end event

type cb_seleziona from commandbutton within tabpage_3
integer x = 3493
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sel.Tutto"
end type

event clicked;long ll_i

for ll_i = 1 to dw_righe_ordine.rowcount()
	dw_righe_ordine.setitem(ll_i, "flag_evasione_totale", "S")
	dw_righe_ordine.setitem(ll_i, "quan_da_evadere", dw_righe_ordine.getitemnumber(ll_i, "quan_residua"))
next
dw_righe_ordine.accepttext()
end event

type dw_righe_ordine from datawindow within tabpage_3
event ue_key pbm_dwnkey
integer y = 8
integer width = 3867
integer height = 1580
integer taborder = 10
string dataobject = "d_lista_righe_da_evadere"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;if key = keyf1! and keyflags = 1 then
	cb_seleziona.triggerevent("Clicked")
end if
if key = keyF2! and keyflags = 1 then
	cb_deseleziona.triggerevent("Clicked")
end if

			
end event

event itemchanged;if isnull(dwo.name) then return
choose case dwo.name
	case "flag_evasione_totale"
		if data = "S" then
			setitem(row, "quan_da_evadere", getitemnumber(row,"quan_residua"))
		else
			setitem(row, "quan_da_evadere", 0)
		end if
	case "quan_da_evadere"
		if dec(data) < getitemnumber(row,"quan_residua") then
			setitem(row,"flag_evasione_totale","N")
		else
			setitem(row,"flag_evasione_totale","S")
		end if
end choose
end event

