﻿$PBExportHeader$w_riepilogo_documenti_fido.srw
forward
global type w_riepilogo_documenti_fido from w_cs_xx_risposta
end type
type dw_1 from datawindow within w_riepilogo_documenti_fido
end type
end forward

global type w_riepilogo_documenti_fido from w_cs_xx_risposta
integer width = 2688
integer height = 2384
string title = "RIEPILOGO"
boolean controlmenu = false
dw_1 dw_1
end type
global w_riepilogo_documenti_fido w_riepilogo_documenti_fido

on w_riepilogo_documenti_fido.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_riepilogo_documenti_fido.destroy
call super::destroy
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;datastore			lds_data
string					ls_titolo


lds_data = s_cs_xx.parametri.parametro_ds_1
ls_titolo = s_cs_xx.parametri.parametro_s_10

lds_data.rowscopy(1, lds_data.rowcount(), primary!, dw_1, 1, primary!)

dw_1.object.t_titolo.text = ls_titolo

if s_cs_xx.parametri.parametro_b_1 then
	dw_1.object.b_chiudi.text = "PROSEGUI"
end if

 s_cs_xx.parametri.parametro_b_1 = false
 s_cs_xx.parametri.parametro_s_10 = ""

dw_1.sort()
dw_1.groupcalc()
end event

type dw_1 from datawindow within w_riepilogo_documenti_fido
integer x = 27
integer y = 24
integer width = 2606
integer height = 2232
integer taborder = 10
string title = "none"
string dataobject = "d_riepilogo_documenti_fido"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;choose case dwo.name
	case "b_stampa"
		dw_1.print( true, true)
		
	case "b_chiudi"
		close(parent)
		
end choose
end event

