﻿$PBExportHeader$w_riferimenti_commesse.srw
forward
global type w_riferimenti_commesse from window
end type
type st_1 from statictext within w_riferimenti_commesse
end type
type cb_1 from commandbutton within w_riferimenti_commesse
end type
end forward

global type w_riferimenti_commesse from window
integer width = 1129
integer height = 212
boolean titlebar = true
string title = "Controllo riferimenti commesse"
long backcolor = 67108864
st_1 st_1
cb_1 cb_1
end type
global w_riferimenti_commesse w_riferimenti_commesse

on w_riferimenti_commesse.create
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.st_1,&
this.cb_1}
end on

on w_riferimenti_commesse.destroy
destroy(this.st_1)
destroy(this.cb_1)
end on

type st_1 from statictext within w_riferimenti_commesse
integer x = 411
integer y = 32
integer width = 677
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_riferimenti_commesse
integer x = 23
integer y = 20
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Scansione"
end type

event clicked;long ll_anno_commessa, ll_num_commessa, ll_count, ll_anno_registrazione, ll_num_registrazione

string ls_cod_lire


enabled = false

st_1.text = "Inizio procedura..."

select stringa
into 	 :ls_cod_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if	

declare commesse cursor for
select anno_commessa,
		 num_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda;

open commesse;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore commesse: " + sqlca.sqlerrtext)
	return -1
end if

do while true
		
	fetch commesse
	into	:ll_anno_commessa,
			:ll_num_commessa;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore commesse: " + sqlca.sqlerrtext)
		close commesse;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	st_1.text = string(ll_anno_commessa) + " / " + string(ll_num_commessa)
	
	select count(*)
	into   :ll_count
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa;
			 
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_ord_ven: " + sqlca.sqlerrtext)
		close commesse;
		return -1
	elseif sqlca.sqlcode = 100 then
		continue
	end if
	
	if ll_count <= 1 then
		continue
	end if
	
	select anno_registrazione,
			 num_registrazione
	into   :ll_anno_registrazione,
			 :ll_num_registrazione
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione in (select anno_registrazione from det_ord_ven where anno_commessa = :ll_anno_commessa and num_commessa = :ll_num_commessa) and
			 num_registrazione in (select num_registrazione from det_ord_ven where anno_commessa = :ll_anno_commessa and num_commessa = :ll_num_commessa) and
			 cod_valuta = :ls_cod_lire;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella select di tes_ord_ven: " + sqlca.sqlerrtext)
		close commesse;
		return -1
	end if
	
	update det_ord_ven
	set    anno_commessa = null,
			 num_commessa = null
	where  anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella update di det_ord_ven: " + sqlca.sqlerrtext)
		close commesse;
		rollback;
		return -1
	end if	
	
loop

close commesse;

commit;

st_1.text = "Operazione completata"

enabled = true

return 0
end event

