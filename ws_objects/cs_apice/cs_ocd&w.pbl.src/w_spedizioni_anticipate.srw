﻿$PBExportHeader$w_spedizioni_anticipate.srw
$PBExportComments$Finestra Spedizioni Anticipate
forward
global type w_spedizioni_anticipate from w_cs_xx_principale
end type
type dw_selezione_commesse from u_dw_search within w_spedizioni_anticipate
end type
type dw_lista_fatture_pronte from datawindow within w_spedizioni_anticipate
end type
type dw_selezione_bolle from uo_cs_xx_dw within w_spedizioni_anticipate
end type
type dw_selezione_fatture from uo_cs_xx_dw within w_spedizioni_anticipate
end type
type cb_esegui from commandbutton within w_spedizioni_anticipate
end type
type cb_crea_ft from commandbutton within w_spedizioni_anticipate
end type
type dw_folder from u_folder within w_spedizioni_anticipate
end type
type dw_lista_bolle_pronte from datawindow within w_spedizioni_anticipate
end type
type dw_lista_comm_spedizione from uo_cs_xx_dw within w_spedizioni_anticipate
end type
type cb_ricerca from commandbutton within w_spedizioni_anticipate
end type
type cb_annulla from commandbutton within w_spedizioni_anticipate
end type
end forward

global type w_spedizioni_anticipate from w_cs_xx_principale
integer width = 3104
integer height = 1756
string title = "Carico Parziale Prodotto Finito"
dw_selezione_commesse dw_selezione_commesse
dw_lista_fatture_pronte dw_lista_fatture_pronte
dw_selezione_bolle dw_selezione_bolle
dw_selezione_fatture dw_selezione_fatture
cb_esegui cb_esegui
cb_crea_ft cb_crea_ft
dw_folder dw_folder
dw_lista_bolle_pronte dw_lista_bolle_pronte
dw_lista_comm_spedizione dw_lista_comm_spedizione
cb_ricerca cb_ricerca
cb_annulla cb_annulla
end type
global w_spedizioni_anticipate w_spedizioni_anticipate

forward prototypes
public function integer wf_movimento_carico (ref string fs_messaggio, ref long fl_anno_reg_mov_mag, ref long fl_num_reg_mov_mag)
public function integer wf_crea_fattura (string fs_cod_tipo_ord_ven, string fs_cod_tipo_fat_ven, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, long fl_quan_anticipo, integer fl_quan_assegnata, long fl_anno_registrazione, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ordine, ref string fs_messaggio, string fs_flag_aggiungi, ref long fl_anno_fattura, ref long fl_num_fattura, ref long fl_prog_riga_fat_ven)
public function integer wf_crea_bolla (string fs_cod_tipo_ord_ven, string fs_cod_tipo_bol_ven, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, long fl_quan_anticipo, integer fl_quan_assegnata, long fl_anno_registrazione, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ordine, ref string fs_messaggio, string fs_flag_aggiungi, ref long fl_anno_bolla, ref long fl_num_bolla, ref long fl_prog_riga_bol_ven)
end prototypes

public function integer wf_movimento_carico (ref string fs_messaggio, ref long fl_anno_reg_mov_mag, ref long fl_num_reg_mov_mag);string ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_referenza, &
       ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[], &
		 ls_cod_deposito_versamento, ls_cod_tipo_commessa, ls_cod_prodotto_raggruppato, ls_empty[]
long   ll_num_registrazione, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_progr_stock[], &
       ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], ll_anno_commessa, ll_num_commessa, &
		 ll_anno_ord_ven, ll_num_ord_ven, ll_prog_riga_ord_ven, ll_num_sottocomm, &
		 ll_num_documento, ll_anno_reg_mov[], ll_num_reg_mov[], ll_quan_ordinata, &
		 ll_quan_in_evasione, ll_quan_evasa, ll_cont, ll_anno_mov_ant, ll_num_mov_ant, ll_empty[], &
		 ll_anno_reg_mov_rag[], ll_num_reg_mov_rag[]
double ld_quantita_anticipo, ld_valore
datetime ldt_data_registrazione, ldt_data_stock[], ldt_oggi, ldt_data_documento, ldt_empty[]

uo_magazzino luo_mag


setnull(fs_messaggio)
setnull(fl_anno_reg_mov_mag)
setnull(fl_num_reg_mov_mag)
ldt_oggi = datetime(today())


select stringa
into   :ls_cod_tipo_movimento
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and
       parametri_azienda.cod_parametro = 'TMC';
if sqlca.sqlcode <> 0 then
	fs_messaggio= "Parametro TMC mancante in parametri azienda: impossibile proseguire"
   return -1
end if

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ldt_data_stock[1])
setnull(ll_progr_stock[1])
setnull(ls_cod_fornitore[1])
setnull(ll_num_documento)
setnull(ldt_data_documento)
setnull(ls_referenza)
ld_valore   = 0
ll_anno_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"anno_commessa")
ll_num_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"num_commessa")
ll_num_sottocomm = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"prog_riga")
ld_quantita_anticipo = dw_selezione_commesse.getitemnumber(dw_selezione_commesse.getrow(),"quan_spedizione")
if ld_quantita_anticipo = 0 then
	fs_messaggio = "Attenzione quantità spedizione a zero: impossibile proseguire!"
	return -1
end if

ll_num_documento = ll_num_commessa
ls_referenza = string(ll_anno_commessa)

select cod_prodotto,
       cod_tipo_commessa
into   :ls_cod_prodotto,
       :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
		 num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca dati commessa: impossibile proseguire"
   return -1
end if

select cod_deposito_versamento
into   :ls_cod_deposito_versamento
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_commessa = :ls_cod_tipo_commessa;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca tipo commessa: impossibile proseguire"
   return -1
end if
	
if isnull(ls_cod_tipo_commessa) then
	fs_messaggio = "Non è stato impostato il deposito di versamento in tipi commesse: impossibile proseguire"
   return -1
end if

select anno_registrazione,
       num_registrazione,
		 prog_riga_ord_ven,
		 quan_ordine,
		 quan_in_evasione,
		 quan_evasa
into   :ll_anno_ord_ven,
       :ll_num_ord_ven,
		 :ll_prog_riga_ord_ven,
		 :ll_quan_ordinata,
		 :ll_quan_in_evasione,
		 :ll_quan_evasa
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
		 num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca dati ordine: impossibile proseguire"
   return -1
end if
	
if (ll_quan_ordinata - ll_quan_in_evasione - ll_quan_evasa) < ld_quantita_anticipo then
	if g_mb.messagebox("Anticipo Spedizioni","Quantità spedizione maggiore della quantità residua da evadere dall'ordine: continuo?",Question!,YesNo!, 1) = 2 then
		return -1
	end if
end if

ls_cod_lotto[1] = string(ll_anno_commessa, "####") + string(ll_num_commessa,"######")

select cod_cliente,
       cod_deposito,
       cod_ubicazione
into   :ls_cod_cliente[1],
		 :ls_cod_deposito[1],
       :ls_cod_ubicazione[1]
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_ord_ven and
		 num_registrazione = :ll_num_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca deposito e ubicazione in testata ordine: impossibile proseguire"
   return -1
end if

setnull(ll_cont)
select min(prog_riga_anticipo)
into   :ll_cont
from   det_anag_comm_anticipi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa ;
		 
if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont <> 0 then
	select anno_reg_mov_mag,
	       num_reg_mov_mag
	into   :ll_anno_mov_ant, 
	       :ll_num_mov_ant
	from   det_anag_comm_anticipi
	where  cod_azienda        = :s_cs_xx.cod_azienda and
			 anno_commessa      = :ll_anno_commessa and
			 num_commessa       = :ll_num_commessa and
			 prog_riga_anticipo = :ll_cont;
	if sqlca.sqlcode = 0 then
		select cod_deposito,
		       cod_ubicazione,
				 cod_lotto,
				 data_stock,
				 prog_stock
		into   :ls_cod_deposito[1],
		       :ls_cod_ubicazione[1],
				 :ls_cod_lotto[1],
				 :ldt_data_stock[1],
				 :ll_progr_stock[1]
		from   mov_magazzino
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_mov_ant and
				 num_registrazione = :ll_num_mov_ant ;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca stock primo movimento di carico "
			return -1
		end if
	end if
end if
		 
if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
										ls_cod_prodotto, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_progr_stock[], &
										ls_cod_cliente[], &
										ls_cod_fornitore[], &
										ll_anno_reg_des_mov, &
										ll_num_reg_des_mov ) = -1 then
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, &
								 ll_num_reg_des_mov, &
								 ls_cod_tipo_movimento, &
								 ls_cod_prodotto) = -1 then
	return -1
end if

luo_mag = create uo_magazzino

if luo_mag.uof_movimenti_mag ( ldt_oggi, &
							ls_cod_tipo_movimento, &
							"S", &
							ls_cod_prodotto, &
							ld_quantita_anticipo, &
							ld_valore, &
							ll_num_documento, &
							ldt_data_documento, &
							ls_referenza, &
							ll_anno_reg_des_mov, &
							ll_num_reg_des_mov, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_progr_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							ll_anno_reg_mov[], &
							ll_num_reg_mov[] ) = -1 then
	destroy luo_mag
	return -1
end if

f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)
// ---------------------------------------------------------------------------------------------------------------

// stefanop 06/12/2011: movimento anche il prodotto raggruppato
if luo_mag.uof_movimenti_mag_ragguppato(ldt_oggi, &
							ls_cod_tipo_movimento, &
							"S", &
							ls_cod_prodotto, &
							ld_quantita_anticipo, &
							ld_valore, &
							ll_num_documento, &
							ldt_data_documento, &
							ls_referenza, &
							ll_anno_reg_des_mov, &
							ll_num_reg_des_mov, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_progr_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							ll_anno_reg_mov_rag[], &
							ll_num_reg_mov_rag[],&
							long(ls_referenza),&
							ll_anno_reg_mov[1],&
							ll_num_reg_mov[1]) = -1 then
	destroy luo_mag
	return -1
end if					
// ----

destroy luo_mag

fl_anno_reg_mov_mag = ll_anno_reg_mov[1]
fl_num_reg_mov_mag  = ll_num_reg_mov[1]
return 0
end function

public function integer wf_crea_fattura (string fs_cod_tipo_ord_ven, string fs_cod_tipo_fat_ven, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, long fl_quan_anticipo, integer fl_quan_assegnata, long fl_anno_registrazione, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ordine, ref string fs_messaggio, string fs_flag_aggiungi, ref long fl_anno_fattura, ref long fl_num_fattura, ref long fl_prog_riga_fat_ven);boolean lb_riferimento
long ll_num_registrazione, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_fat_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga
string ls_cod_tipo_fat_ven, ls_nota_testata, ls_cod_cliente[1], &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, &
		 ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, &
		 ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, &
  		 ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, &
		 ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		   ldt_vostro_ordine_data
dec{4} ld_cambio_ven, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso
uo_generazione_documenti luo_generazione_documenti

uo_calcola_documento_euro luo_calcolo


lb_riferimento = true
setnull(ls_cod_fornitore[1])

select cod_tipo_fat_ven,  
		 cod_tipo_analisi  
into   :ls_cod_tipo_fat_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_ord_ven = :fs_cod_tipo_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita. Errore DB:" + sqlca.sqlerrtext
	return -1
end if

// se è stato scelto un tipo fattura, lo sostituisco a indicato nel tipo ordine
if not isnull(fs_cod_tipo_fat_ven) and len(fs_cod_tipo_fat_ven) > 0 then
	ls_cod_tipo_fat_ven = fs_cod_tipo_fat_ven
end if

if not isnull(ls_cod_tipo_fat_ven) then
	if fs_flag_aggiungi <> "S" then
		fl_anno_fattura = f_anno_esercizio()
		select max(num_registrazione)
		into   :fl_num_fattura
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_fattura;
		if isnull(fl_num_fattura) or fl_num_fattura < 1 then
			fl_num_fattura = 1
		else
			fl_num_fattura ++
		end if
		
		update con_fat_ven
		set    num_registrazione = :ll_num_registrazione
		where  cod_azienda = :s_cs_xx.cod_azienda;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita. Errore DB:" + sqlca.sqlerrtext
			return -1
		end if
	end if
	
	ll_num_registrazione = fl_num_fattura

	select cod_operatore,   
			 data_registrazione,   
			 cod_cliente,   
			 cod_des_cliente,   
			 rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 cod_deposito,   
			 cod_ubicazione,   
			 cod_valuta,   
			 cambio_ven,   
			 cod_tipo_listino_prodotto,   
			 cod_pagamento,   
			 sconto,   
			 cod_agente_1,
			 cod_agente_2,
			 cod_banca,   
			 num_ord_cliente,   
			 data_ord_cliente,   
			 cod_imballo,   
			 aspetto_beni,
			 peso_netto,
			 peso_lordo,
			 num_colli,
			 cod_vettore,   
			 cod_inoltro,   
			 cod_mezzo,
			 causale_trasporto,
			 cod_porto,   
			 cod_resa,   
			 nota_testata,   
			 nota_piede,   
			 flag_riep_fat,
			 tot_val_evaso,
			 tot_val_ordine,
			 cod_banca_clien_for,
			 cod_causale
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale
	from   tes_ord_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fl_anno_ordine and  
			 num_registrazione = :fl_num_ordine;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if

	select cod_tipo_analisi,
			 causale_trasporto,
			 cod_causale,
			 cod_tipo_det_ven_rif_bol_ven,
			 des_riferimento_ord_ven,
			 flag_rif_anno_ord_ven,
			 flag_rif_num_ord_ven,
			 flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_fat,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Fatture. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if


	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente) and ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_nota_testata = ls_des_riferimento_ord_ven + string(fl_anno_ordine) + "/" + &
								string(fl_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	if fs_flag_aggiungi <> "S" then
		insert into tes_fat_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_fat_ven,   
						 cod_cliente,
						 cod_fornitore,
						 cod_des_cliente,   
						 cod_fil_fornitore,
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_fattura,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 cod_banca_clien_for,
						 flag_contabilita,
						 cod_causale)  
		values      (:s_cs_xx.cod_azienda,   
						 :fl_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_fat_ven,   
						 :ls_cod_cliente[1],
						 null,
						 :ls_cod_des_cliente,   
						 null,
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 :ls_cod_banca_clien_for,
						 'N',
						 :ls_cod_causale);
	
		 if sqlca.sqlcode <> 0 then
			 fs_messaggio = "Errore durante la scrittura Testata Fatture Vendita. Errore DB:" + sqlca.sqlerrtext
			 return -1
		 end if
	 end if
	 
	lb_riferimento = true
	ll_prog_riga_fat_ven = 0

	ls_cod_deposito[1] = fs_cod_deposito
	ls_cod_ubicazione[1] = fs_cod_ubicazione
   ls_cod_lotto[1] = fs_cod_lotto
   ldt_data_stock[1] = fdt_data_stock
	ll_progr_stock[1] = fl_prog_stock

	if fs_flag_aggiungi = "S" then
		setnull(ll_prog_riga_fat_ven)
		
		select max(prog_riga_fat_ven)
		into   :ll_prog_riga_fat_ven
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_fattura and
				 num_registrazione = :fl_num_fattura;
		if sqlca.sqlcode <> 0 or isnull(ll_prog_riga_fat_ven) then
			ll_prog_riga_fat_ven = 10
		else				
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
		end if
	else
		ll_prog_riga_fat_ven = 10
	end if
		
	select	cod_tipo_det_ven,   
				cod_misura,   
				cod_prodotto,   
				des_prodotto,   
				quan_ordine,
				prezzo_vendita,   
				fat_conversione_ven,   
				sconto_1,   
				sconto_2,  
				provvigione_1,
				provvigione_2,
				cod_iva,   
				quan_evasa,
				nota_dettaglio,   
				anno_commessa,
				num_commessa,
				cod_centro_costo,
				sconto_3,   
				sconto_4,   
				sconto_5,   
				sconto_6,   
				sconto_7,   
				sconto_8,   
				sconto_9,   
				sconto_10,
				cod_versione
	into		:ls_cod_tipo_det_ven, 
				:ls_cod_misura, 
				:ls_cod_prodotto, 
				:ls_des_prodotto, 
				:ld_quan_ordine,
				:ld_prezzo_vendita, 
				:ld_fat_conversione_ven, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_provvigione_1, 
				:ld_provvigione_2, 
				:ls_cod_iva, 
				:ld_quan_evasa,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_3, 
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_cod_versione
	  from 	det_ord_ven  
	 where 	cod_azienda = :s_cs_xx.cod_azienda and  
				anno_registrazione = :fl_anno_ordine and  
				num_registrazione = :fl_num_ordine and
				prog_riga_ord_ven = :fl_prog_riga_ordine
	order by prog_riga_ord_ven asc;

	if sqlca.sqlcode <> 0 then
		fs_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if
			
	select tab_tipi_det_ven.flag_tipo_det_ven,
			 tab_tipi_det_ven.cod_tipo_movimento,
			 tab_tipi_det_ven.flag_sola_iva
	into   :ls_flag_tipo_det_ven,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			fs_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
			return -1
		end if
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
			return -1
		end if
	end if

	if lb_riferimento then
		lb_riferimento = false
		setnull(ls_note_dettaglio_rif)
		ls_des_riferimento = ls_des_riferimento_ord_ven
		if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_ordine) then
			ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_ordine)
		end if
		if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_ordine) then
			ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_ordine)
		end if
		if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
			ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
		end if
		if not isnull(ls_num_ord_cliente) then
			ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
			if not isnull(ldt_data_ord_cliente) then
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
			end if
		end if
			
		select tab_tipi_det_ven.cod_iva
		into   :ls_cod_iva_rif
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven_rif_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Generazione Fattura","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite",Information!)
		end if
		
		insert into det_fat_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_fat_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_fatturata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_reg_ord_ven,   
						 num_reg_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 cod_versione)
	  values			(:s_cs_xx.cod_azienda,   
						 :fl_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_fat_ven,   
						 :ls_cod_tipo_det_ven_rif_ord_ven,   
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,   
						 null,   
						 :ls_des_riferimento,   
						 0,   
						 0,   
						 1,   
						 0,   
						 0,  
						 0,
						 0,
						 :ls_cod_iva_rif,   
						 null,
						 null,
						 :ls_note_dettaglio_rif,   
						 null,   
						 null,   
						 null,
						 null,   
						 null,   
						 null,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,
						 null,
						 null);

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Dettagli Fatture Vendita. Errore DB:" + sqlca.sqlerrtext
			return -1
		end if
		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
	end if	

	insert into det_fat_ven  
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 prog_riga_fat_ven,   
					 cod_tipo_det_ven,   
					 cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 progr_stock,
					 data_stock,
					 cod_misura,   
					 cod_prodotto,   
					 des_prodotto,   
					 quan_fatturata,   
					 prezzo_vendita,   
					 fat_conversione_ven,   
					 sconto_1,   
					 sconto_2,   
					 provvigione_1,
					 provvigione_2,
					 cod_iva,   
					 cod_tipo_movimento,
					 num_registrazione_mov_mag,
					 nota_dettaglio,   
					 anno_reg_ord_ven,   
					 num_reg_ord_ven,   
					 prog_riga_ord_ven,
					 anno_commessa,
					 num_commessa,
					 cod_centro_costo,
					 sconto_3,   
					 sconto_4,   
					 sconto_5,   
					 sconto_6,   
					 sconto_7,   
					 sconto_8,   
					 sconto_9,   
					 sconto_10,
					 anno_registrazione_mov_mag,
					 num_reg_des_mov,
					 cod_versione)
  values			(:s_cs_xx.cod_azienda,   
					 :fl_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ll_prog_riga_fat_ven,   
					 :ls_cod_tipo_det_ven,   
					 :ls_cod_deposito[1],
					 :ls_cod_ubicazione[1],
					 :ls_cod_lotto[1],
					 :ll_progr_stock[1],
					 :ldt_data_stock[1],
					 :ls_cod_misura,   
					 :ls_cod_prodotto,   
					 :ls_des_prodotto,   
					 :fl_quan_anticipo,   
					 :ld_prezzo_vendita,   
					 :ld_fat_conversione_ven,   
					 :ld_sconto_1,   
					 :ld_sconto_2,  
					 :ld_provvigione_1,
					 :ld_provvigione_2,
					 :ls_cod_iva,   
					 :ls_cod_tipo_movimento,
					 null,
					 :ls_nota_dettaglio,   
					 :fl_anno_ordine,   
					 :fl_num_ordine,   
					 :fl_prog_riga_ordine,
					 :ll_anno_commessa,   
					 :ll_num_commessa,   
					 :ls_cod_centro_costo,   
					 :ld_sconto_3,   
					 :ld_sconto_4,   
					 :ld_sconto_5,   
					 :ld_sconto_6,   
					 :ld_sconto_7,   
					 :ld_sconto_8,   
					 :ld_sconto_9,   
					 :ld_sconto_10,
					 :ll_anno_reg_des_mov,
					 :ll_num_reg_des_mov,
					 :ls_cod_versione);

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Fatture Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" then
		update anag_prodotti  
			set quan_assegnata = quan_assegnata - :fl_quan_assegnata,
				 quan_in_spedizione = quan_in_spedizione + :fl_quan_anticipo
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione in anagrafica prodotti.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
			return -1
		end if
		
		update stock 
		set 	 quan_assegnata = quan_assegnata - :fl_quan_assegnata,   
				 quan_in_spedizione = quan_in_spedizione + :fl_quan_anticipo
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_prodotto = :ls_cod_prodotto and 
				 cod_deposito = :ls_cod_deposito[1] and 
				 cod_ubicazione = :ls_cod_ubicazione[1] and 
				 cod_lotto = :ls_cod_lotto[1] and 
				 data_stock = :ldt_data_stock[1] and 
				 prog_stock = :ll_progr_stock[1];

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione stock.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
			return -1
		end if
	end if				

	ls_tabella_orig = "det_ord_ven_stat"
	ls_tabella_des = "det_fat_ven_stat"
	ls_prog_riga_doc_orig = "prog_riga_ord_ven"
	ls_prog_riga_doc_des = "prog_riga_fat_ven"

	if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_ordine, fl_num_ordine, fl_prog_riga_ordine, fl_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
		return -1
	end if	

	if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
		if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, fl_anno_registrazione, ll_prog_riga_fat_ven, 5) = -1 then
			fs_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
			return -1
		end if
	end if

	if ld_quan_ordine = ld_quan_evasa + fl_quan_assegnata then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if
	
	update det_ord_ven  
		set quan_in_evasione = quan_in_evasione - :fl_quan_assegnata,
			 quan_evasa = quan_evasa + :fl_quan_assegnata,
			 flag_evasione = :ls_flag_evasione
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fl_anno_ordine and  
			 num_registrazione = :fl_num_ordine and
			 prog_riga_ord_ven = :fl_prog_riga_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione in Dettaglio Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_sola_iva = 'N' then
		ld_val_riga = fl_quan_assegnata * ld_prezzo_vendita
		ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100

		if ls_cod_valuta = ls_lire then
			ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
		else
			ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		end if   

		if ls_flag_tipo_det_ven = "M" or &
			ls_flag_tipo_det_ven = "C" or &
			ls_flag_tipo_det_ven = "N" then
			ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
			ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
			ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
			ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		elseif ls_flag_tipo_det_ven = "S" then
			ld_val_evaso = ld_val_riga_sconto_10 * -1
		else
			ld_val_evaso = ld_val_riga_sconto_10
		end if
		ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
	end if

	if ls_cod_valuta = ls_lire then
		ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
	end if   

//	if ld_tot_val_ordine = ld_tot_val_evaso then
//		ls_flag_evasione = "E"
//	else
//		ls_flag_evasione = "P"
//	end if

	update tes_ord_ven  
		set tot_val_evaso = :ld_tot_val_evaso
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fl_anno_ordine and  
			 num_registrazione = :fl_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento valore evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if

	// calcolo stato ordine in base al nuovo sistema EnMe 18/03/2004
	luo_generazione_documenti = CREATE uo_generazione_documenti
		if luo_generazione_documenti.uof_calcola_stato_ordine(fl_anno_ordine, fl_num_ordine) < 0 then
		return -1
	end if
	destroy luo_generazione_documenti
	
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(fl_anno_registrazione,ll_num_registrazione,"fat_ven",fs_messaggio) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo

	fl_anno_fattura = fl_anno_registrazione
	fl_num_fattura  = ll_num_registrazione
	fl_prog_riga_fat_ven = ll_prog_riga_fat_ven

end if
return 0
end function

public function integer wf_crea_bolla (string fs_cod_tipo_ord_ven, string fs_cod_tipo_bol_ven, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, long fl_quan_anticipo, integer fl_quan_assegnata, long fl_anno_registrazione, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ordine, ref string fs_messaggio, string fs_flag_aggiungi, ref long fl_anno_bolla, ref long fl_num_bolla, ref long fl_prog_riga_bol_ven);boolean lb_riferimento
long ll_num_registrazione, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, ll_ret
string ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, &
		 ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, &
		 ls_flag_abilita_rif_ord_ven,  ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, &
  		 ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, &
		 ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		   ldt_vostro_ordine_data
dec{4} ld_cambio_ven, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso
uo_generazione_documenti luo_generazione_documenti

uo_calcola_documento_euro luo_calcolo


lb_riferimento = true
setnull(ls_cod_fornitore[1])

select cod_tipo_bol_ven,  
		 cod_tipo_analisi  
into   :ls_cod_tipo_bol_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_ord_ven = :fs_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita. Errore DB:" + sqlca.sqlerrtext
	return -1
end if

// se è stato scelto un tipo fattura, lo sostituisco a indicato nel tipo ordine
if not isnull(fs_cod_tipo_bol_ven) and len(fs_cod_tipo_bol_ven) > 0 then
	ls_cod_tipo_bol_ven = fs_cod_tipo_bol_ven
end if

if not isnull(ls_cod_tipo_bol_ven) then
	
	if fs_flag_aggiungi <> "S" then
		
		// -- stefanop 14/01/2010: Ticket 2010/6
		// Il massimo deve essere calcolato sull'anno corrente altrimenti da errore di chiave duplicata
		/**
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_ordine;
		**/
		
		long ll_anno_esercizio_corrente
		ll_anno_esercizio_corrente = f_anno_esercizio()
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_esercizio_corrente;
		// ----
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la lettura della tabella Controllo Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
			return -1
		end if
	
		if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione ++
		end if
		
	else
		
		ll_num_registrazione = fl_num_bolla
		
	end if



	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_ordine and  
			 tes_ord_ven.num_registrazione = :fl_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if

	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Bolle. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if


	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente) and ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_nota_testata = ls_des_riferimento_ord_ven + string(fl_anno_ordine) + "/" + &
								string(fl_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	if fs_flag_aggiungi <> "S" then
		insert into tes_bol_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_bol_ven,   
						 cod_cliente,
						 cod_fornitore,
						 cod_des_cliente,   
						 cod_fil_fornitore,
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_deposito_tras,
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_bolla,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_riep_fat,
						 cod_banca_clien_for,
						 flag_gen_fat,
						 cod_causale)  
		values      (:s_cs_xx.cod_azienda,   
						 :fl_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_bol_ven,   
						 :ls_cod_cliente[1],
						 null,
						 :ls_cod_des_cliente,   
						 null,
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 null,
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 :ls_flag_riep_fat,
						 :ls_cod_banca_clien_for,
						 'N',
						 :ls_cod_causale);
	
		 if sqlca.sqlcode <> 0 then
			 fs_messaggio = "Errore durante la scrittura Testata Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
			 return -1
		 end if
	 end if
	 
	lb_riferimento = true
	ll_prog_riga_bol_ven = 0

	ls_cod_deposito[1] = fs_cod_deposito
	ls_cod_ubicazione[1] = fs_cod_ubicazione
   ls_cod_lotto[1] = fs_cod_lotto
   ldt_data_stock[1] = fdt_data_stock
	ll_progr_stock[1] = fl_prog_stock

	if fs_flag_aggiungi = "S" then
		setnull(ll_prog_riga_bol_ven)
		
		select max(prog_riga_bol_ven)
		into   :ll_prog_riga_bol_ven
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_bolla and
				 num_registrazione = :fl_num_bolla;
		if sqlca.sqlcode <> 0 or isnull(ll_prog_riga_bol_ven) then
			ll_prog_riga_bol_ven = 10
		else				
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if
	else
		ll_prog_riga_bol_ven = 10
	end if
		
	select	det_ord_ven.cod_tipo_det_ven,   
				det_ord_ven.cod_misura,   
				det_ord_ven.cod_prodotto,   
				det_ord_ven.des_prodotto,   
				det_ord_ven.quan_ordine,
				det_ord_ven.prezzo_vendita,   
				det_ord_ven.fat_conversione_ven,   
				det_ord_ven.sconto_1,   
				det_ord_ven.sconto_2,  
				det_ord_ven.provvigione_1,
				det_ord_ven.provvigione_2,
				det_ord_ven.cod_iva,   
				det_ord_ven.quan_evasa,
				det_ord_ven.nota_dettaglio,   
				det_ord_ven.anno_commessa,
				det_ord_ven.num_commessa,
				det_ord_ven.cod_centro_costo,
				det_ord_ven.sconto_3,   
				det_ord_ven.sconto_4,   
				det_ord_ven.sconto_5,   
				det_ord_ven.sconto_6,   
				det_ord_ven.sconto_7,   
				det_ord_ven.sconto_8,   
				det_ord_ven.sconto_9,   
				det_ord_ven.sconto_10,
				det_ord_ven.cod_versione
	into		:ls_cod_tipo_det_ven, 
				:ls_cod_misura, 
				:ls_cod_prodotto, 
				:ls_des_prodotto, 
				:ld_quan_ordine,
				:ld_prezzo_vendita, 
				:ld_fat_conversione_ven, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_provvigione_1, 
				:ld_provvigione_2, 
				:ls_cod_iva, 
				:ld_quan_evasa,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_3, 
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_cod_versione
	  from 	det_ord_ven  
	 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				det_ord_ven.anno_registrazione = :fl_anno_ordine and  
				det_ord_ven.num_registrazione = :fl_num_ordine and
				det_ord_ven.prog_riga_ord_ven = :fl_prog_riga_ordine
	order by det_ord_ven.prog_riga_ord_ven asc;

	if sqlca.sqlcode <> 0 then
		fs_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if
			
	select tab_tipi_det_ven.flag_tipo_det_ven,
			 tab_tipi_det_ven.cod_tipo_movimento,
			 tab_tipi_det_ven.flag_sola_iva
	into   :ls_flag_tipo_det_ven,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			fs_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
			return -1
		end if
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
			return -1
		end if
	end if

	if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
		lb_riferimento = false
		setnull(ls_note_dettaglio_rif)
		ls_des_riferimento = ls_des_riferimento_ord_ven
		if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_ordine) then
			ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_ordine)
		end if
		if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_ordine) then
			ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_ordine)
		end if
		if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
			ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
		end if
		if not isnull(ls_num_ord_cliente) then
			ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
			if not isnull(ldt_data_ord_cliente) then
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
			end if
		end if
			
		select tab_tipi_det_ven.cod_iva
		into   :ls_cod_iva_rif
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven_rif_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Generazione Bolla singola","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite",Information!)
		end if
		
		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione)
	  values			(:s_cs_xx.cod_azienda,   
						 :fl_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven_rif_ord_ven,   
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,   
						 null,   
						 :ls_des_riferimento,   
						 0,   
						 0,   
						 1,   
						 0,   
						 0,  
						 0,
						 0,
						 :ls_cod_iva_rif,   
						 null,
						 null,
						 :ls_note_dettaglio_rif,   
						 null,   
						 null,   
						 null,
						 null,   
						 null,   
						 null,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null);

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
			return -1
		end if
		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
	end if	

	insert into det_bol_ven  
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 prog_riga_bol_ven,   
					 cod_tipo_det_ven,   
					 cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 progr_stock,
					 data_stock,
					 cod_misura,   
					 cod_prodotto,   
					 des_prodotto,   
					 quan_consegnata,   
					 prezzo_vendita,   
					 fat_conversione_ven,   
					 sconto_1,   
					 sconto_2,   
					 provvigione_1,
					 provvigione_2,
					 cod_iva,   
					 cod_tipo_movimento,
					 num_registrazione_mov_mag,
					 nota_dettaglio,   
					 anno_registrazione_ord_ven,   
					 num_registrazione_ord_ven,   
					 prog_riga_ord_ven,
					 anno_commessa,
					 num_commessa,
					 cod_centro_costo,
					 sconto_3,   
					 sconto_4,   
					 sconto_5,   
					 sconto_6,   
					 sconto_7,   
					 sconto_8,   
					 sconto_9,   
					 sconto_10,
					 anno_registrazione_mov_mag,
					 anno_reg_bol_acq,
					 num_reg_bol_acq,
					 prog_riga_bol_acq,
					 anno_reg_des_mov,
					 num_reg_des_mov,
					 cod_versione)
  values			(:s_cs_xx.cod_azienda,   
					 :fl_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ll_prog_riga_bol_ven,   
					 :ls_cod_tipo_det_ven,   
					 :ls_cod_deposito[1],
					 :ls_cod_ubicazione[1],
					 :ls_cod_lotto[1],
					 :ll_progr_stock[1],
					 :ldt_data_stock[1],
					 :ls_cod_misura,   
					 :ls_cod_prodotto,   
					 :ls_des_prodotto,   
					 :fl_quan_anticipo,   
					 :ld_prezzo_vendita,   
					 :ld_fat_conversione_ven,   
					 :ld_sconto_1,   
					 :ld_sconto_2,  
					 :ld_provvigione_1,
					 :ld_provvigione_2,
					 :ls_cod_iva,   
					 :ls_cod_tipo_movimento,
					 null,
					 :ls_nota_dettaglio,   
					 :fl_anno_ordine,   
					 :fl_num_ordine,   
					 :fl_prog_riga_ordine,
					 :ll_anno_commessa,   
					 :ll_num_commessa,   
					 :ls_cod_centro_costo,   
					 :ld_sconto_3,   
					 :ld_sconto_4,   
					 :ld_sconto_5,   
					 :ld_sconto_6,   
					 :ld_sconto_7,   
					 :ld_sconto_8,   
					 :ld_sconto_9,   
					 :ld_sconto_10,
					 null,
					 null,
					 null,
					 null,
					 :ll_anno_reg_des_mov,
					 :ll_num_reg_des_mov,
					 :ls_cod_versione);

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" then
		update anag_prodotti  
			set quan_assegnata = quan_assegnata - :fl_quan_assegnata,
				 quan_in_spedizione = quan_in_spedizione + :fl_quan_anticipo
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione in anagrafica prodotti.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
			return -1
		end if
		
		update stock 
		set 	 quan_assegnata = quan_assegnata - :fl_quan_assegnata,   
				 quan_in_spedizione = quan_in_spedizione + :fl_quan_anticipo
		where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
				 stock.cod_prodotto = :ls_cod_prodotto and 
				 stock.cod_deposito = :ls_cod_deposito[1] and 
				 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
				 stock.cod_lotto = :ls_cod_lotto[1] and 
				 stock.data_stock = :ldt_data_stock[1] and 
				 stock.prog_stock = :ll_progr_stock[1];

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione stock.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
			return -1
		end if
	end if				

	ls_tabella_orig = "det_ord_ven_stat"
	ls_tabella_des = "det_bol_ven_stat"
	ls_prog_riga_doc_orig = "prog_riga_ord_ven"
	ls_prog_riga_doc_des = "prog_riga_bol_ven"

	if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_ordine, fl_num_ordine, fl_prog_riga_ordine, fl_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
		return -1
	end if	

	if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
		if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, fl_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
			fs_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
			return -1
		end if
	end if

	if ld_quan_ordine = ld_quan_evasa + fl_quan_assegnata then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if
	
	update det_ord_ven  
		set quan_in_evasione = quan_in_evasione - :fl_quan_assegnata,
			 quan_evasa = quan_evasa + :fl_quan_assegnata,
			 flag_evasione = :ls_flag_evasione
	 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_ord_ven.anno_registrazione = :fl_anno_ordine and  
			 det_ord_ven.num_registrazione = :fl_num_ordine and
			 det_ord_ven.prog_riga_ord_ven = :fl_prog_riga_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione in Dettaglio Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if
	


	if ls_flag_sola_iva = 'N' then
		ld_val_riga = fl_quan_assegnata * ld_prezzo_vendita
		ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100

		if ls_cod_valuta = ls_lire then
			ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
		else
			ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		end if   

		if ls_flag_tipo_det_ven = "M" or &
			ls_flag_tipo_det_ven = "C" or &
			ls_flag_tipo_det_ven = "N" then
			ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
			ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
			ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
			ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		elseif ls_flag_tipo_det_ven = "S" then
			ld_val_evaso = ld_val_riga_sconto_10 * -1
		else
			ld_val_evaso = ld_val_riga_sconto_10
		end if
		ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
	end if

	if ls_cod_valuta = ls_lire then
		ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
	end if   

//	if ld_tot_val_ordine = ld_tot_val_evaso then
//		ls_flag_evasione = "E"
//	else
//		ls_flag_evasione = "P"
//	end if

	update tes_ord_ven  
		set tot_val_evaso = :ld_tot_val_evaso
	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_ordine and  
			 tes_ord_ven.num_registrazione = :fl_num_ordine;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento valore evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if
	
	// calcolo stato ordine in base al nuovo sistema EnMe 18/03/2004
	luo_generazione_documenti = CREATE uo_generazione_documenti
		if luo_generazione_documenti.uof_calcola_stato_ordine(fl_anno_ordine, fl_num_ordine) < 0 then
		return -1
	end if
	destroy luo_generazione_documenti
	
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(fl_anno_registrazione,ll_num_registrazione,"bol_ven",fs_messaggio) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo
	
	fl_anno_bolla = fl_anno_registrazione
	fl_num_bolla  = ll_num_registrazione
	fl_prog_riga_bol_ven = ll_prog_riga_bol_ven

end if
return 0
end function

on w_spedizioni_anticipate.create
int iCurrent
call super::create
this.dw_selezione_commesse=create dw_selezione_commesse
this.dw_lista_fatture_pronte=create dw_lista_fatture_pronte
this.dw_selezione_bolle=create dw_selezione_bolle
this.dw_selezione_fatture=create dw_selezione_fatture
this.cb_esegui=create cb_esegui
this.cb_crea_ft=create cb_crea_ft
this.dw_folder=create dw_folder
this.dw_lista_bolle_pronte=create dw_lista_bolle_pronte
this.dw_lista_comm_spedizione=create dw_lista_comm_spedizione
this.cb_ricerca=create cb_ricerca
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione_commesse
this.Control[iCurrent+2]=this.dw_lista_fatture_pronte
this.Control[iCurrent+3]=this.dw_selezione_bolle
this.Control[iCurrent+4]=this.dw_selezione_fatture
this.Control[iCurrent+5]=this.cb_esegui
this.Control[iCurrent+6]=this.cb_crea_ft
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_lista_bolle_pronte
this.Control[iCurrent+9]=this.dw_lista_comm_spedizione
this.Control[iCurrent+10]=this.cb_ricerca
this.Control[iCurrent+11]=this.cb_annulla
end on

on w_spedizioni_anticipate.destroy
call super::destroy
destroy(this.dw_selezione_commesse)
destroy(this.dw_lista_fatture_pronte)
destroy(this.dw_selezione_bolle)
destroy(this.dw_selezione_fatture)
destroy(this.cb_esegui)
destroy(this.cb_crea_ft)
destroy(this.dw_folder)
destroy(this.dw_lista_bolle_pronte)
destroy(this.dw_lista_comm_spedizione)
destroy(this.cb_ricerca)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]

set_w_options(c_NoEnablePopup + c_CloseNoSave)
Save_On_Close(c_SOCNoSave)

dw_lista_comm_spedizione.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_noretrieveonopen + &
													 c_nonew + &
													 c_nomodify + &
													 c_nodelete + &
													 c_nomultiselect, &
                                        c_default + c_NoHighlightSelected)

dw_selezione_bolle.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_noretrieveonopen + &
													 c_newonopen + &
													 c_nomodify + &
													 c_nodelete + &
													 c_nomultiselect, &
                                        c_default)

dw_selezione_fatture.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_noretrieveonopen + &
													 c_newonopen + &
													 c_nomodify + &
													 c_nodelete + &
													 c_nomultiselect, &
                                        c_default)


lw_oggetti[1] = dw_lista_bolle_pronte
lw_oggetti[2] = dw_selezione_bolle
lw_oggetti[3] = cb_esegui
dw_folder.fu_assigntab(2, "Spediz. con Bolla", lw_oggetti[])

lw_oggetti[1] = dw_lista_fatture_pronte
lw_oggetti[2] = dw_selezione_fatture
lw_oggetti[3] = cb_crea_ft
dw_folder.fu_assigntab(3, "Spediz. con Fattura", lw_oggetti[])
lw_oggetti[1] = dw_selezione_commesse
lw_oggetti[2] = cb_ricerca
lw_oggetti[3] = cb_annulla
lw_oggetti[4] = dw_lista_comm_spedizione
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
dw_folder.fu_foldercreate(3, 3)
dw_folder.fu_selecttab(1)

l_criteriacolumn[1] = "anno_commessa"
l_criteriacolumn[2] = "num_commessa"

l_searchtable[1] = "det_anag_commesse"
l_searchtable[2] = "det_anag_commesse"

l_searchcolumn[1] = "anno_commessa"
l_searchcolumn[2] = "num_commessa"

dw_selezione_commesse.fu_wiredw(l_criteriacolumn[], &
                     dw_lista_comm_spedizione, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_lista_bolle_pronte.settransobject(sqlca)
dw_lista_fatture_pronte.settransobject(sqlca)

dw_folder.fu_disabletab(2)
dw_folder.fu_disabletab(3)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione_fatture, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_fat_ven in ('I','P')")

f_po_loaddddw_dw(dw_selezione_bolle, &
                 "cod_tipo_bol_ven", &
                 sqlca, &
                 "tab_tipi_bol_ven", &
                 "cod_tipo_bol_ven", &
                 "des_tipo_bol_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event getfocus;call super::getfocus;string ls_null

setnull(ls_null)
s_cs_xx.parametri.parametro_uo_dw_search = dw_selezione_commesse
setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_s_1 = "anno_commessa"
s_cs_xx.parametri.parametro_s_2 = "num_commessa"
setnull(s_cs_xx.parametri.parametro_s_10)
end event

type dw_selezione_commesse from u_dw_search within w_spedizioni_anticipate
integer x = 46
integer y = 140
integer width = 2491
integer height = 340
integer taborder = 30
string dataobject = "d_selezione_commesse"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_cliente,ls_rag_soc_1, ls_null
long ll_anno_commessa, ll_num_commessa, ll_anno_registrazione, ll_num_registrazione, ll_righe, ll_null

setnull(ll_null)
setnull(ls_null)
if isvalid(dwo) then
	choose case dwo.name
		case "anno_commessa"
			ll_anno_commessa = long(data)
			ll_num_commessa = getitemnumber(getrow(),"num_commessa")
			dw_lista_bolle_pronte.reset()
			dw_lista_fatture_pronte.reset()
			setitem(1,"quan_spedizione", 0)
			dw_selezione_commesse.setitem(1,"cod_cliente",ls_null)
			dw_selezione_commesse.setitem(1,"rag_soc_1",ls_null)
		case "num_commessa"
			ll_anno_commessa = getitemnumber(getrow(),"anno_commessa")
			ll_num_commessa = long(data)
			dw_lista_bolle_pronte.reset()
			dw_lista_fatture_pronte.reset()
			setitem(1,"quan_spedizione", 0)
			dw_selezione_commesse.setitem(1,"cod_cliente",ls_null)
			dw_selezione_commesse.setitem(1,"rag_soc_1",ls_null)
	end choose		
	
	if dwo.name = "anno_commessa" or dwo.name = "num_commessa" then
		if ll_anno_commessa > 0 and not isnull(ll_anno_commessa) and ll_num_commessa > 0 and not isnull(ll_num_commessa) then
	
			select anno_registrazione, 
					 num_registrazione
			into   :ll_anno_registrazione,
					 :ll_num_registrazione
			from   det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_commessa = :ll_anno_commessa and
					 num_commessa = :ll_num_commessa;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in ricerca della riga d'ordine da cui è nata la commessa; non sarà possibile visualizzare l'elenco delle bolle aperte")
				return 0
			end if
			
			select cod_cliente
			into   :ls_cod_cliente
			from   tes_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in ricerca della testata dell'ordine da cui è nata la commessa; non sarà possibile visualizzare l'elenco delle bolle aperte")
				return 0
			end if
			
			select rag_soc_1
			into   :ls_rag_soc_1
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cliente = :ls_cod_cliente;
				
			dw_selezione_commesse.setitem(1,"cod_cliente",ls_cod_cliente)
			dw_selezione_commesse.setitem(1,"rag_soc_1",ls_rag_soc_1)
			
			dw_lista_bolle_pronte.setredraw(false)
			ll_righe = dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)
			dw_lista_bolle_pronte.setredraw(true)
			if ll_righe < 0 then
				g_mb.messagebox("APICE","Errore in visualizzazione bolle aperte; non sarà possibile visualizzare l'elenco delle bolle aperte")
				return 0
			end if
			
			dw_lista_fatture_pronte.setredraw(false)
			ll_righe = dw_lista_fatture_pronte.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)
			dw_lista_fatture_pronte.setredraw(true)
			if ll_righe < 0 then
				g_mb.messagebox("APICE","Errore in visualizzazione fatture aperte; non sarà possibile visualizzare l'elenco delle fatture aperte")
				return 0
			end if
			
			cb_ricerca.triggerevent("clicked")
		end if
	end if
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_commessa"
		guo_ricerca.uof_ricerca_commessa(dw_selezione_commesse,"anno_commessa","num_commessa")
end choose
end event

type dw_lista_fatture_pronte from datawindow within w_spedizioni_anticipate
integer x = 46
integer y = 540
integer width = 2971
integer height = 1060
integer taborder = 60
string title = "none"
string dataobject = "d_lista_fatture_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if row > 0 then
	dw_selezione_fatture.setitem(1,"anno_fat_ven", getitemnumber(row,"tes_fat_ven_anno_registrazione"))
	dw_selezione_fatture.setitem(1,"num_fat_ven", getitemnumber(row,"tes_fat_ven_num_registrazione"))
	dw_selezione_fatture.setitem(1,"flag_nuova_fattura", "S")
end if
	
end event

type dw_selezione_bolle from uo_cs_xx_dw within w_spedizioni_anticipate
integer x = 91
integer y = 120
integer width = 2491
integer height = 400
integer taborder = 60
string dataobject = "d_sped_antic_selezione_bolle"
boolean border = false
end type

type dw_selezione_fatture from uo_cs_xx_dw within w_spedizioni_anticipate
integer x = 91
integer y = 140
integer width = 2491
integer height = 400
integer taborder = 60
string dataobject = "d_sped_antic_selezione_fatture"
boolean border = false
end type

type cb_esegui from commandbutton within w_spedizioni_anticipate
integer x = 2606
integer y = 160
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Bolla"
end type

event clicked;string ls_messaggio, ls_cod_prodotto , ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
       ls_cod_tipo_ord_ven, ls_flag_aggiungi, ls_cod_tipo_bol_ven,ls_cod_prodotto_raggruppato
long   ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_anno_commessa, ll_num_commessa, &
       ll_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_prog_stock, ll_anno_ord_ven, ll_num_ord_ven, ll_prog_riga_ord_ven, &
		 ll_anno_esercizio, ll_anno_bolla, ll_num_bolla, ll_prog_riga_comm, ll_cont, ll_prog_riga_bol_ven
dec{4} ld_val_movimento,ld_quan_assegnabile, ld_quan_movimento,ld_quan_ordinata, ld_quan_in_evasione, ld_quan_assegnazione, ld_quan_evasa, &
       ld_quan_da_assegnare, ld_quan_raggruppo
datetime ldt_data_stock

dw_selezione_commesse.accepttext()
dw_selezione_bolle.accepttext()

if dw_selezione_bolle.getitemstring(1,"flag_nuova_bolla") = "S" then
	if g_mb.messagebox("APICE","Sei sicuro di voler AGGIUNGERE la merce alla bolla selezionata ?",Question!,YesNo!,2) = 2 then
		return
	end if
else
	if g_mb.messagebox("APICE","Sei sicuro di voler CREARE la bolla di spedizione ?",Question!,YesNo!,2) = 2 then
		return
	end if
end if

if wf_movimento_carico(ls_messaggio, &
                       ll_anno_reg_mov_mag, &
							  ll_num_reg_mov_mag) = -1 then
	g_mb.messagebox("APICE","Errore durante in fase di generazione movimenti di carico " + "~r~n" + ls_messaggio, StopSign!) 
	ROLLBACK;
	return
end if

select cod_prodotto,
       cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 data_stock,
		 prog_stock,
		 quan_movimento,
		 val_movimento
into   :ls_cod_prodotto,
       :ls_cod_deposito,
		 :ls_cod_ubicazione,
		 :ls_cod_lotto,
		 :ldt_data_stock,
		 :ll_prog_stock,
		 :ld_quan_movimento,
		 :ld_val_movimento
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_reg_mov_mag and
       num_registrazione  = :ll_num_reg_mov_mag ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca movimento carico : " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

ll_anno_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"anno_commessa")
ll_num_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"num_commessa")
ll_prog_riga_comm = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"prog_riga")
ls_cod_tipo_bol_ven = dw_selezione_bolle.getitemstring(dw_selezione_bolle.getrow(),"cod_tipo_bol_ven")

select anno_registrazione,
       num_registrazione,
		 prog_riga_ord_ven,
		 quan_ordine,
		 quan_in_evasione,
		 quan_evasa
into   :ll_anno_ord_ven,
       :ll_num_ord_ven,
		 :ll_prog_riga_ord_ven,
		 :ld_quan_ordinata,
		 :ld_quan_in_evasione,
		 :ld_quan_evasa
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
		 num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati ordine: impossibile proseguire: " + sqlca.sqlerrtext,StopSign!)
	rollback;
   return
end if

ld_quan_assegnabile = ld_quan_ordinata - ld_quan_in_evasione - ld_quan_evasa
if ld_quan_movimento > ld_quan_assegnabile then
	ld_quan_assegnazione = ld_quan_assegnabile
else
	ld_quan_assegnazione = ld_quan_movimento
end if


// ------------------------  aggiornamento anag_prodotti -------------------------------

update anag_prodotti  
	set quan_impegnata = quan_impegnata - :ld_quan_assegnazione,
		 quan_assegnata = quan_assegnata + :ld_quan_assegnazione
 where cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_prodotto = :ls_cod_prodotto;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino: " + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if

// enme 08/1/2006 gestione prodotto raggruppato
setnull(ls_cod_prodotto_raggruppato)

select cod_prodotto_raggruppato
into   :ls_cod_prodotto_raggruppato
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;
		 
if not isnull(ls_cod_prodotto_raggruppato) then
	
	ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_assegnazione, "M")
	
	update anag_prodotti  
		set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :ls_cod_prodotto_raggruppato;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino: " + sqlca.sqlerrtext, stopsign!)
		rollback;
		return
	end if
end if






// -------------------------aggiornamento ordine ---------------------------------------

update det_ord_ven
	set quan_in_evasione   = quan_in_evasione + :ld_quan_assegnazione
 where cod_azienda        = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :ll_anno_ord_ven and  
		 num_registrazione  = :ll_num_ord_ven and
		 prog_riga_ord_ven  = :ll_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine: " + sqlca.sqlerrtext, &
				  stopsign!)
	rollback;
	return
end if
		
			
//-------------------------- aggiornamento stock ------------------------------------------

update stock  
	set stock.quan_assegnata = stock.quan_assegnata + :ld_quan_assegnazione
 where stock.cod_azienda    = :s_cs_xx.cod_azienda and
		 stock.cod_prodotto   = :ls_cod_prodotto and 
		 stock.cod_deposito   = :ls_cod_deposito and
		 stock.cod_ubicazione = :ls_cod_ubicazione and
		 stock.cod_lotto      = :ls_cod_lotto and
		 stock.data_stock     = :ldt_data_stock and
		 stock.prog_stock     = :ll_prog_stock;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento lotto: " + sqlca.sqlerrtext, &
				  stopsign!)
	rollback;
	return
end if


// --------------------------- creazione bolla da ordine -----------------------------------

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_ord_ven and
		 num_registrazione = :ll_num_ord_ven ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata ordine di vendita: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

ll_anno_esercizio = f_anno_esercizio()
ls_flag_aggiungi = dw_selezione_bolle.getitemstring(dw_selezione_bolle.getrow(),"flag_nuova_bolla")
if ls_flag_aggiungi <> "S" then
	setnull(ll_anno_bolla)
	setnull(ll_num_bolla)
else
	ll_anno_bolla = dw_selezione_bolle.getitemnumber(dw_selezione_bolle.getrow(),"anno_bol_ven")
	ll_num_bolla = dw_selezione_bolle.getitemnumber(dw_selezione_bolle.getrow(),"num_bol_ven")
	ll_anno_esercizio = ll_anno_bolla
end if

//scambiati i due argomenti quan_movimento con quan_assegnazione

if wf_crea_bolla(ls_cod_tipo_ord_ven, &
				 ls_cod_tipo_bol_ven, &
             ls_cod_deposito, &
				 ls_cod_ubicazione, &
				 ls_cod_lotto, &
				 ldt_data_stock, &
				 ll_prog_stock, &
				 ld_quan_movimento, &            
				 ld_quan_assegnazione, &
				 ll_anno_esercizio, &
				 ll_anno_ord_ven, &
				 ll_num_ord_ven, &
				 ll_prog_riga_ord_ven, &
				 ls_messaggio, &
				 ls_flag_aggiungi, &
				 ll_anno_bolla, &
				 ll_num_bolla,&
				 ll_prog_riga_bol_ven) <> 0 then
	g_mb.messagebox("APICE","Errore durante la generazione della bolla " + "~r~n" + ls_messaggio, StopSign!) 
	ROLLBACK;
	return
end if

				 
// ------------------------- aggiorno tabella det_anag_comm_anticipi -----------------------				 
setnull(ll_cont)

select count(*)
into   :ll_cont
from   det_anag_comm_anticipi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa and
		 prog_riga     = :ll_prog_riga_comm  ;
if isnull(ll_cont) or ll_cont = 0 or sqlca.sqlcode <> 0 then
	ll_cont = 10
else
	select max(prog_riga_anticipo)
	into   :ll_cont
	from   det_anag_comm_anticipi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_commessa = :ll_anno_commessa and
	       num_commessa  = :ll_num_commessa and
			 prog_riga     = :ll_prog_riga_comm  ;
	if isnull(ll_cont) or ll_cont = 0 or sqlca.sqlcode <> 0 then
		ll_cont = 10
	else
		ll_cont = ll_cont + 10
	end if
end if
		
insert into det_anag_comm_anticipi
         ( cod_azienda,   
           anno_commessa,   
           num_commessa,   
           prog_riga,   
           prog_riga_anticipo,   
           quan_anticipo,   
           anno_reg_mov_mag,   
           num_reg_mov_mag,   
           anno_reg_bol_ven,   
           num_reg_bol_ven,   
           prog_riga_bol_ven )  
   values (:s_cs_xx.cod_azienda,   
           :ll_anno_commessa,   
           :ll_num_commessa,   
           :ll_prog_riga_comm,   
           :ll_cont,   
           :ld_quan_movimento,   
           :ll_anno_reg_mov_mag,   
           :ll_num_reg_mov_mag,   
           :ll_anno_bolla,   
           :ll_num_bolla,   
           :ll_prog_riga_bol_ven);
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in inserimento in dettagli anticipi commesse: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if


update det_anag_commesse
set    quan_anticipo = quan_anticipo + :ld_quan_movimento
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa ;

COMMIT;

dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, dw_selezione_commesse.getitemstring(1,"cod_cliente"))
dw_lista_fatture_pronte.retrieve(s_cs_xx.cod_azienda, dw_selezione_commesse.getitemstring(1,"cod_cliente"))
g_mb.messagebox("APICE","Elaborazione eseguita con successo! ~r~nGenerato dettagli in Bolla" + string(ll_anno_bolla) + "/" + string(ll_num_bolla), Information!)


end event

type cb_crea_ft from commandbutton within w_spedizioni_anticipate
integer x = 2606
integer y = 160
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Fatt."
end type

event clicked;string ls_messaggio, ls_cod_prodotto , ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
       ls_cod_tipo_ord_ven, ls_flag_aggiungi, ls_cod_tipo_fat_ven,ls_cod_prodotto_raggruppato
long   ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_anno_commessa, ll_num_commessa, &
       ll_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_quan_assegnabile, ll_prog_stock, &
		 ll_quan_movimento, ll_anno_ord_ven, ll_num_ord_ven, ll_prog_riga_ord_ven, &
		 ll_quan_ordinata, ll_quan_in_evasione, ll_quan_evasa, ll_quan_assegnazione, &
		 ll_quan_da_assegnare, ll_anno_esercizio, ll_anno_fattura, ll_num_fattura, &
		 ll_prog_riga_comm, ll_cont, ll_prog_riga_fat_ven
dec{4} ld_val_movimento, ld_quan_raggruppo
datetime ldt_data_stock

dw_selezione_commesse.accepttext()
dw_selezione_fatture.accepttext()
if dw_selezione_fatture.getitemstring(1,"flag_nuova_fattura") = "S" then
	if g_mb.messagebox("APICE","Sei sicuro di voler AGGIUNGERE la merce alla fattura selezionata ?",Question!,YesNo!,2) = 2 then
		return
	end if
else
	if g_mb.messagebox("APICE","Sei sicuro di voler CREARE la fattura per la spedizione ?",Question!,YesNo!,2) = 2 then
		return
	end if
end if

if wf_movimento_carico(ls_messaggio, &
                       ll_anno_reg_mov_mag, &
							  ll_num_reg_mov_mag) = -1 then
	g_mb.messagebox("APICE","Errore durante in fase di generazione movimenti di carico " + "~r~n" + ls_messaggio, StopSign!) 
	ROLLBACK;
	return
end if

select cod_prodotto,
       cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 data_stock,
		 prog_stock,
		 quan_movimento,
		 val_movimento
into   :ls_cod_prodotto,
       :ls_cod_deposito,
		 :ls_cod_ubicazione,
		 :ls_cod_lotto,
		 :ldt_data_stock,
		 :ll_prog_stock,
		 :ll_quan_movimento,
		 :ld_val_movimento
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_reg_mov_mag and
       num_registrazione  = :ll_num_reg_mov_mag ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca movimento carico : " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

ll_anno_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"anno_commessa")
ll_num_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"num_commessa")
ll_prog_riga_comm = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"prog_riga")
ls_cod_tipo_fat_ven = dw_selezione_fatture.getitemstring(dw_selezione_fatture.getrow(),"cod_tipo_fat_ven")

select anno_registrazione,
       num_registrazione,
		 prog_riga_ord_ven,
		 quan_ordine,
		 quan_in_evasione,
		 quan_evasa
into   :ll_anno_ord_ven,
       :ll_num_ord_ven,
		 :ll_prog_riga_ord_ven,
		 :ll_quan_ordinata,
		 :ll_quan_in_evasione,
		 :ll_quan_evasa
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
		 num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati ordine: impossibile proseguire: " + sqlca.sqlerrtext,StopSign!)
	rollback;
   return
end if

ll_quan_assegnabile = ll_quan_ordinata - ll_quan_in_evasione - ll_quan_evasa
if ll_quan_movimento > ll_quan_assegnabile then
	ll_quan_assegnazione = ll_quan_assegnabile
else
	ll_quan_assegnazione = ll_quan_movimento
end if


// ------------------------  aggiornamento anag_prodotti -------------------------------

update anag_prodotti  
	set quan_impegnata = quan_impegnata - :ll_quan_assegnazione,
		 quan_assegnata = quan_assegnata + :ll_quan_assegnazione
 where cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_prodotto = :ls_cod_prodotto;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino: " + sqlca.sqlerrtext, &
				  stopsign!)
	rollback;
	return
end if

// enme 08/1/2006 gestione prodotto raggruppato
setnull(ls_cod_prodotto_raggruppato)

select cod_prodotto_raggruppato
into   :ls_cod_prodotto_raggruppato
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;
		 
if not isnull(ls_cod_prodotto_raggruppato) then
	
	ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ll_quan_assegnazione, "M")
	
	update anag_prodotti  
		set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :ls_cod_prodotto_raggruppato;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino: " + sqlca.sqlerrtext, &
					  stopsign!)
		rollback;
		return
	end if
end if





// -------------------------aggiornamento ordine ---------------------------------------

update det_ord_ven
	set quan_in_evasione   = quan_in_evasione + :ll_quan_assegnazione
 where cod_azienda        = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :ll_anno_ord_ven and  
		 num_registrazione  = :ll_num_ord_ven and
		 prog_riga_ord_ven  = :ll_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine: " + sqlca.sqlerrtext, &
				  stopsign!)
	rollback;
	return
end if
		
			
//-------------------------- aggiornamento stock ------------------------------------------

update stock  
	set stock.quan_assegnata = stock.quan_assegnata + :ll_quan_assegnazione
 where stock.cod_azienda    = :s_cs_xx.cod_azienda and
		 stock.cod_prodotto   = :ls_cod_prodotto and 
		 stock.cod_deposito   = :ls_cod_deposito and
		 stock.cod_ubicazione = :ls_cod_ubicazione and
		 stock.cod_lotto      = :ls_cod_lotto and
		 stock.data_stock     = :ldt_data_stock and
		 stock.prog_stock     = :ll_prog_stock;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento lotto: " + sqlca.sqlerrtext, &
				  stopsign!)
	rollback;
	return
end if


// --------------------------- creazione fattura da ordine -----------------------------------

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_ord_ven and
		 num_registrazione = :ll_num_ord_ven ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata ordine di vendita: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if


ll_anno_esercizio = f_anno_esercizio()
ls_flag_aggiungi = dw_selezione_fatture.getitemstring(dw_selezione_fatture.getrow(),"flag_nuova_fattura")
if ls_flag_aggiungi <> "S" then
	setnull(ll_anno_fattura)
	setnull(ll_num_fattura)
else
	ll_anno_fattura = dw_selezione_fatture.getitemnumber(dw_selezione_fatture.getrow(),"anno_fat_ven")
	ll_num_fattura = dw_selezione_fatture.getitemnumber(dw_selezione_fatture.getrow(),"num_fat_ven")
	ll_anno_esercizio = ll_anno_fattura
end if

//scambiati i due argomenti quan_movimento con quan_assegnazione

if wf_crea_fattura(ls_cod_tipo_ord_ven, &
				 ls_cod_tipo_fat_ven, &
             ls_cod_deposito, &
				 ls_cod_ubicazione, &
				 ls_cod_lotto, &
				 ldt_data_stock, &
				 ll_prog_stock, &
				 ll_quan_movimento, &            
				 ll_quan_assegnazione, &
				 ll_anno_esercizio, &
				 ll_anno_ord_ven, &
				 ll_num_ord_ven, &
				 ll_prog_riga_ord_ven, &
				 ls_messaggio, &
				 ls_flag_aggiungi, &
				 ll_anno_fattura, &
				 ll_num_fattura,&
				 ll_prog_riga_fat_ven) <> 0 then
	g_mb.messagebox("APICE","Errore durante la generazione della fattura " + "~r~n" + ls_messaggio, StopSign!) 
	ROLLBACK;
	return
end if

				 
// ------------------------- aggiorno tabella det_anag_comm_anticipi -----------------------				 
setnull(ll_cont)

select count(*)
into   :ll_cont
from   det_anag_comm_anticipi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa and
		 prog_riga     = :ll_prog_riga_comm  ;
if isnull(ll_cont) or ll_cont = 0 or sqlca.sqlcode <> 0 then
	ll_cont = 10
else
	select max(prog_riga_anticipo)
	into   :ll_cont
	from   det_anag_comm_anticipi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_commessa = :ll_anno_commessa and
	       num_commessa  = :ll_num_commessa and
			 prog_riga     = :ll_prog_riga_comm  ;
	if isnull(ll_cont) or ll_cont = 0 or sqlca.sqlcode <> 0 then
		ll_cont = 10
	else
		ll_cont = ll_cont + 10
	end if
end if
	
insert into det_anag_comm_anticipi
         ( cod_azienda,   
           anno_commessa,   
           num_commessa,   
           prog_riga,   
           prog_riga_anticipo,   
           quan_anticipo,   
           anno_reg_mov_mag,   
           num_reg_mov_mag,   
           anno_reg_bol_ven,   
           num_reg_bol_ven,   
           prog_riga_bol_ven,
           anno_reg_fat_ven,   
           num_reg_fat_ven,   
           prog_riga_fat_ven )  
   values (:s_cs_xx.cod_azienda,   
           :ll_anno_commessa,   
           :ll_num_commessa,   
           :ll_prog_riga_comm,   
           :ll_cont,   
           :ll_quan_movimento,   
           :ll_anno_reg_mov_mag,   
           :ll_num_reg_mov_mag,   
           null,   
           null,   
           null,
			  :ll_anno_fattura,
			  :ll_num_fattura,
			  :ll_prog_riga_fat_ven)  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in inserimento in dettagli anticipi commesse: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if


update det_anag_commesse
set    quan_anticipo = quan_anticipo + :ll_quan_movimento
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
       num_commessa  = :ll_num_commessa ;

COMMIT;

dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, dw_selezione_commesse.getitemstring(1,"cod_cliente"))
dw_lista_fatture_pronte.retrieve(s_cs_xx.cod_azienda, dw_selezione_commesse.getitemstring(1,"cod_cliente"))
g_mb.messagebox("APICE","Elaborazione eseguita con successo! ~r~n" + &
               "Generato dettagli in Fattura" + string(ll_anno_fattura) + "/" + string(ll_num_fattura), Information!)
end event

type dw_folder from u_folder within w_spedizioni_anticipate
integer x = 23
integer y = 20
integer width = 3017
integer height = 1620
integer taborder = 40
end type

event po_tabclicked;call super::po_tabclicked;long ll_anno_commessa,ll_num_commessa
dec{4} ld_quan_spedizione, ld_quan_spedibile,ld_quan_ordinata,ld_quan_in_evasione,ld_quan_evasa

choose case i_SelectedTab
	case 2, 3

	ld_quan_spedizione = dw_selezione_commesse.getitemnumber(dw_selezione_commesse.getrow(),"quan_spedizione")
	
	if ld_quan_spedizione <> 0 then
		if dw_lista_comm_spedizione.getrow() > 0 then
			ld_quan_spedibile = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"cf_quantita")
			if ld_quan_spedizione > ld_quan_spedibile then
				g_mb.messagebox("APICE","Attenzione! Quantità in spedizione maggiore della quantità spedibile~r~n(SPEDIBILE = PRODUZIONE - ANTICIPATA)")
			end if
		end if
	end if
	
	ll_anno_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"anno_commessa")
	ll_num_commessa = dw_lista_comm_spedizione.getitemnumber(dw_lista_comm_spedizione.getrow(),"num_commessa")

	select quan_ordine,
			 quan_in_evasione,
			 quan_evasa
	into   :ld_quan_ordinata,
			 :ld_quan_in_evasione,
			 :ld_quan_evasa
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa;
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE","A questa commessa non corrisponde alcun ordine di vendita!",StopSign!)
		return
	end if
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca dati ordine: impossibile proseguire: " + sqlca.sqlerrtext,StopSign!)
		return
	end if
	
	if ld_quan_spedizione > ld_quan_ordinata then
		g_mb.messagebox("APICE","Attenzione!! Quantità in spedizione maggiore della quantità in ordine")
		return
	end if
	
	if ld_quan_spedizione > ( ld_quan_ordinata - ld_quan_in_evasione - ld_quan_evasa ) then
		g_mb.messagebox("APICE","Attenzione!! Quantità in spedizione maggiore della quantità residua dell'ordine.~r~nORDINE="+string(ld_quan_ordinata) + "  EVASA= " + string(ld_quan_in_evasione + ld_quan_evasa) )
	end if
	
end choose

end event

type dw_lista_bolle_pronte from datawindow within w_spedizioni_anticipate
integer x = 46
integer y = 580
integer width = 2971
integer height = 1032
integer taborder = 50
string title = "none"
string dataobject = "d_lista_bolle_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if row > 0 then
	dw_selezione_bolle.setitem(1,"anno_bol_ven", getitemnumber(row,"tes_bol_ven_anno_registrazione"))
	dw_selezione_bolle.setitem(1,"num_bol_ven", getitemnumber(row,"tes_bol_ven_num_registrazione"))
	dw_selezione_bolle.setitem(1,"flag_nuova_bolla", "S")
end if
	
end event

type dw_lista_comm_spedizione from uo_cs_xx_dw within w_spedizioni_anticipate
integer x = 69
integer y = 540
integer width = 2949
integer height = 620
integer taborder = 50
string dataobject = "d_lista_comm_spedizione"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  ll_error, ll_anno_commessa, ll_num_commessa


ll_anno_commessa = dw_selezione_commesse.getitemnumber(dw_selezione_commesse.getrow(),"anno_commessa")
ll_num_commessa  = dw_selezione_commesse.getitemnumber(dw_selezione_commesse.getrow(),"num_commessa")
if isnull(ll_anno_commessa) or ll_anno_commessa < 1 or isnull(ll_num_commessa) or ll_num_commessa = 0 then
	g_mb.messagebox("APICE","Impostare prima anno e numero commessa nella finestra di ricerca",Information!)
	return -1
end if

ll_error = retrieve(s_cs_xx.cod_azienda, ll_anno_commessa, ll_num_commessa)

IF ll_Error < 0 THEN
   PCCA.Error = c_Fatal
elseif ll_Error > 0 then
	dw_folder.fu_enabletab(2)
	dw_folder.fu_enabletab(3)
end if
end event

type cb_ricerca from commandbutton within w_spedizioni_anticipate
event clicked pbm_bnclicked
integer x = 2606
integer y = 160
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_selezione_commesse.fu_BuildSearch(TRUE)
//dw_folder.fu_SelectTab(2)
dw_lista_comm_spedizione.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_annulla from commandbutton within w_spedizioni_anticipate
event clicked pbm_bnclicked
integer x = 2606
integer y = 260
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;dw_selezione_commesse.fu_Reset()
dw_lista_bolle_pronte.reset()
end event

