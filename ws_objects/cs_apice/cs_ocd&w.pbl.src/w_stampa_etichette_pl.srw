﻿$PBExportHeader$w_stampa_etichette_pl.srw
forward
global type w_stampa_etichette_pl from window
end type
type dw_etichetta_dett from datawindow within w_stampa_etichette_pl
end type
type em_2 from editmask within w_stampa_etichette_pl
end type
type st_2 from statictext within w_stampa_etichette_pl
end type
type st_1 from statictext within w_stampa_etichette_pl
end type
type em_1 from editmask within w_stampa_etichette_pl
end type
type cb_1 from commandbutton within w_stampa_etichette_pl
end type
type dw_etichetta_testata from datawindow within w_stampa_etichette_pl
end type
end forward

global type w_stampa_etichette_pl from window
integer width = 1952
integer height = 1080
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_etichetta_dett dw_etichetta_dett
em_2 em_2
st_2 st_2
st_1 st_1
em_1 em_1
cb_1 cb_1
dw_etichetta_testata dw_etichetta_testata
end type
global w_stampa_etichette_pl w_stampa_etichette_pl

on w_stampa_etichette_pl.create
this.dw_etichetta_dett=create dw_etichetta_dett
this.em_2=create em_2
this.st_2=create st_2
this.st_1=create st_1
this.em_1=create em_1
this.cb_1=create cb_1
this.dw_etichetta_testata=create dw_etichetta_testata
this.Control[]={this.dw_etichetta_dett,&
this.em_2,&
this.st_2,&
this.st_1,&
this.em_1,&
this.cb_1,&
this.dw_etichetta_testata}
end on

on w_stampa_etichette_pl.destroy
destroy(this.dw_etichetta_dett)
destroy(this.em_2)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_1)
destroy(this.cb_1)
destroy(this.dw_etichetta_testata)
end on

type dw_etichetta_dett from datawindow within w_stampa_etichette_pl
integer x = 2194
integer y = 328
integer width = 1006
integer height = 272
integer taborder = 10
string title = "none"
string dataobject = "d_report_etichette_pl_2"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type em_2 from editmask within w_stampa_etichette_pl
integer x = 1568
integer y = 752
integer width = 302
integer height = 152
integer taborder = 30
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "4"
borderstyle borderstyle = stylelowered!
string mask = "#0"
boolean spin = true
double increment = 1
string minmax = "1~~999"
end type

type st_2 from statictext within w_stampa_etichette_pl
integer x = 55
integer y = 764
integer width = 1495
integer height = 128
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Q.ta Etichette Lati Pacco"
boolean focusrectangle = false
end type

type st_1 from statictext within w_stampa_etichette_pl
integer x = 59
integer y = 572
integer width = 1467
integer height = 128
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Q.ta Etichette Pacco"
boolean focusrectangle = false
end type

type em_1 from editmask within w_stampa_etichette_pl
integer x = 1568
integer y = 560
integer width = 302
integer height = 152
integer taborder = 30
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "4"
borderstyle borderstyle = stylelowered!
string mask = "#0"
boolean spin = true
double increment = 1
string minmax = "1~~999"
end type

type cb_1 from commandbutton within w_stampa_etichette_pl
integer x = 210
integer y = 48
integer width = 1349
integer height = 392
integer taborder = 20
integer textsize = -48
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA"
end type

event clicked;string ls_flag_spedito, ls_cod_cliente, ls_rag_soc_1, ls_localita, ls_dest_rag_soc_1, &
       ls_dest_localita, ls_provincia, ls_dest_provincia, ls_azienda, ls_az1, ls_az2, ls_az3, &
		 ls_cod_prodotto, ls_des_prodotto_ord, ls_des_prodotto_mag
long ll_num_ordine, ll_prog_riga_ord_ven, ll_num_pl, ll_num_collo, ll_num_collo_old, ll_ret, &
     ll_copie_testata, ll_copie_dett, ll_copie, ll_cont_prodotti, ll_ret_dett
dec{4} ld_quan_pack_list

printsetup()

select rag_soc_1, localita, provincia
into   :ls_az1, :ls_az2, :ls_az3
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

ls_azienda = ls_az1 + " - " + ls_az2 + " (" + ls_az3 + ")"
  
ll_copie_testata = long(em_1.text)  
ll_copie_dett    = long(em_2.text)  

declare cu_pl cursor for
SELECT num_registrazione,   
		 prog_riga_ord_ven,   
		 num_pack_list,   
		 num_collo,   
		 quan_pack_list,   
		 flag_spedito
 FROM  det_ord_ven_pack_list  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :s_cs_xx.parametri.parametro_i_1 and  
		 num_pack_list = :s_cs_xx.parametri.parametro_i_2 
order by num_collo;

open cu_pl;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE", "Errore in OPEN cursore cu_pl~r~n"+sqlca.sqlerrtext)
	rollback;
	close(parent)
end if 

ll_num_collo_old = -1
ll_cont_prodotti = 0

dw_etichetta_dett.reset()
ll_ret_dett = dw_etichetta_dett.insertrow(0)


do while true
	fetch cu_pl into :ll_num_ordine, 
	                 :ll_prog_riga_ord_ven, 
						  :ll_num_pl,
						  :ll_num_collo,
						  :ld_quan_pack_list,
						  :ls_flag_spedito;
	
	if sqlca.sqlcode = 100 then
		
		if ll_cont_prodotti > 0 then
			for ll_copie = 1 to ll_copie_dett
				dw_etichetta_dett.print()
			next

		end if
		
		exit
		
	end if
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore in FETCH cursore cu_pl~r~n"+sqlca.sqlerrtext)
		rollback;
		close(parent)
	end if 
	
	// verifico quandi stampare l'etichetta di testata
	if ll_num_collo <> ll_num_collo_old then
		
		if ll_cont_prodotti > 0 then
			
			for ll_copie = 1 to ll_copie_dett
				dw_etichetta_dett.print()
			next

			dw_etichetta_dett.reset()
			ll_ret_dett = dw_etichetta_dett.insertrow(0)
			
			ll_cont_prodotti = 0
				
		end if
		
		select cod_cliente, rag_soc_1, localita, provincia
		into   :ls_cod_cliente, :ls_dest_rag_soc_1, :ls_dest_localita, :ls_dest_provincia
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :s_cs_xx.parametri.parametro_i_1 and
				 num_registrazione = :ll_num_ordine;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in ricerca testata ordine~r~n"+sqlca.sqlerrtext)
			rollback;
			close(parent)
		end if 
		
		select rag_soc_1, localita, provincia
		into   :ls_rag_soc_1, :ls_localita, :ls_provincia
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in ricerca testata ordine~r~n"+sqlca.sqlerrtext)
			rollback;
			close(parent)
		end if 
		
		// procedo con scrittura campi in etichetta
		
		dw_etichetta_testata.reset()
		ll_ret = dw_etichetta_testata.insertrow(0)

		// ** se esiste il parametro EUG esiste ed è a S allora non stampo il collo nell'etichetta
		string ls_flag
		
		setnull(ls_flag)
		
		select flag
		into   :ls_flag
		from   parametri_azienda
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_parametro = 'EUG';
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag)
		end if
		
		if not isnull(ls_flag) and ls_flag = "S" then
			dw_etichetta_testata.setitem(ll_ret, "num_pl", right(string(s_cs_xx.parametri.parametro_i_1,"0000"),2)+ "/" + string(ll_num_pl)) // + "/" + string(ll_num_collo) )
		else
			dw_etichetta_testata.setitem(ll_ret, "num_pl", right(string(s_cs_xx.parametri.parametro_i_1,"0000"),2)+ "/" + string(ll_num_pl) + "/" + string(ll_num_collo) )
		end if
		dw_etichetta_testata.setitem(ll_ret, "ordine", "ORD. " + string(s_cs_xx.parametri.parametro_i_1,"0000") + " / " + string(ll_num_ordine) )  
		dw_etichetta_testata.setitem(ll_ret, "cliente", "CLIENTE: " + ls_rag_soc_1 )  
		if not isnull(ls_dest_localita) and len(ls_dest_localita) > 0 then
			dw_etichetta_testata.setitem(ll_ret, "destinazione", ls_dest_localita + " ("+ ls_dest_provincia + ")")  
		else
			dw_etichetta_testata.setitem(ll_ret, "destinazione", ls_localita + " ("+ ls_provincia + ")")  
		end if		
		dw_etichetta_testata.setitem(ll_ret, "azienda", ls_azienda)
		
		for ll_copie = 1 to ll_copie_testata
			dw_etichetta_testata.print()
		next
		
		ll_num_collo_old = ll_num_collo
		
	end if
	
	ll_cont_prodotti ++

	// procedo con etichetta prodotto
	
	select cod_prodotto, des_prodotto
	into   :ls_cod_prodotto, :ls_des_prodotto_ord
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_i_1 and
			 num_registrazione = :ll_num_ordine and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			 
	select des_prodotto
	into   :ls_des_prodotto_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
			 
	
	dw_etichetta_dett.setitem(ll_ret_dett, "cod_prodotto_" + string(ll_cont_prodotti), ls_cod_prodotto)
	
	if not isnull(ls_des_prodotto_ord) and len(ls_des_prodotto_ord) > 0 then
		dw_etichetta_dett.setitem(ll_ret_dett, "des_prodotto_" + string(ll_cont_prodotti), ls_des_prodotto_ord)
	else
		dw_etichetta_dett.setitem(ll_ret_dett, "des_prodotto_" + string(ll_cont_prodotti), ls_des_prodotto_mag)
	end if
	
	dw_etichetta_dett.setitem(ll_ret_dett, "quan_spedizione_" + string(ll_cont_prodotti), ld_quan_pack_list)
	dw_etichetta_dett.setitem(ll_ret_dett, "num_pl", right(string(s_cs_xx.parametri.parametro_i_1,"0000"),2)+ "/" + string(ll_num_pl)+ "/" + string(ll_num_collo) )

	
loop


CLOSE cu_pl;

dw_etichetta_dett.reset()
dw_etichetta_testata.reset()


close(parent)




end event

type dw_etichetta_testata from datawindow within w_stampa_etichette_pl
integer x = 2185
integer y = 28
integer width = 1006
integer height = 272
integer taborder = 10
string title = "none"
string dataobject = "d_report_etichette_pl_1"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

