﻿$PBExportHeader$w_tes_gen_commesse.srw
$PBExportComments$Finestra Testata Generazione Commesse
forward
global type w_tes_gen_commesse from w_cs_xx_principale
end type
type dw_elenco_operatori_selezionati from datawindow within w_tes_gen_commesse
end type
type sle_num_commessa_forzato from singlelineedit within w_tes_gen_commesse
end type
type cbx_forza_numerazione from checkbox within w_tes_gen_commesse
end type
type cb_genera from uo_cb_ok within w_tes_gen_commesse
end type
type cb_dettaglio from commandbutton within w_tes_gen_commesse
end type
type dw_tes_gen_commesse from uo_cs_xx_dw within w_tes_gen_commesse
end type
type cb_reset from commandbutton within w_tes_gen_commesse
end type
type cb_aggiorna from commandbutton within w_tes_gen_commesse
end type
type cbx_sel_tutti from checkbox within w_tes_gen_commesse
end type
type dw_ext_selezione_gen_commesse from uo_cs_xx_dw within w_tes_gen_commesse
end type
type dw_gen_orig_dest from uo_cs_xx_dw within w_tes_gen_commesse
end type
type dw_folder from u_folder within w_tes_gen_commesse
end type
end forward

global type w_tes_gen_commesse from w_cs_xx_principale
integer width = 3986
integer height = 1924
string title = "Generazione Commesse"
event ue_imposta_data ( )
dw_elenco_operatori_selezionati dw_elenco_operatori_selezionati
sle_num_commessa_forzato sle_num_commessa_forzato
cbx_forza_numerazione cbx_forza_numerazione
cb_genera cb_genera
cb_dettaglio cb_dettaglio
dw_tes_gen_commesse dw_tes_gen_commesse
cb_reset cb_reset
cb_aggiorna cb_aggiorna
cbx_sel_tutti cbx_sel_tutti
dw_ext_selezione_gen_commesse dw_ext_selezione_gen_commesse
dw_gen_orig_dest dw_gen_orig_dest
dw_folder dw_folder
end type
global w_tes_gen_commesse w_tes_gen_commesse

type variables


uo_fido_cliente				iuo_fido_cliente
string							is_cod_parametro_blocco = "ICM"
end variables

forward prototypes
public function integer f_elimina_array (ref string fs_array[])
public subroutine wf_carica_operatori ()
end prototypes

event ue_imposta_data();integer li_anno

li_anno = f_anno_esercizio()

dw_ext_selezione_gen_commesse.setitem(1,"data_consegna_inizio",date("01/01/"+string(li_anno)))
dw_ext_selezione_gen_commesse.setitem(1,"data_consegna_fine",date("31/12/"+string(li_anno)))
dw_ext_selezione_gen_commesse.setitem(1,"anno_ordine", li_anno)
end event

public function integer f_elimina_array (ref string fs_array[]);string ls_array[]

fs_array[]=ls_array

return 0
end function

public subroutine wf_carica_operatori ();string			ls_cod_operatore, ls_des_operatore, ls_cod_operatore_utente
long			ll_i

//funzione chiamata all'inizio
//serve a caricare la dw degli operatori ed a impostare l'operatore di default associato all'utente collegato

dw_elenco_operatori_selezionati.visible = false

select cod_operatore
into :ls_cod_operatore_utente
from tab_operatori_utenti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente and
			flag_default='S';

if sqlca.sqlcode=0 and ls_cod_operatore_utente<>"" and not isnull(ls_cod_operatore_utente) then
else
	ls_cod_operatore_utente = ""
end if

declare cu_operatori cursor for
select cod_operatore, des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_blocco = 'N'
order by cod_operatore;
open cu_operatori;
do while true
	fetch cu_operatori into :ls_cod_operatore, :ls_des_operatore;
	if sqlca.sqlcode <> 0 then exit
	ll_i = dw_elenco_operatori_selezionati.insertrow(0)
	dw_elenco_operatori_selezionati.setitem(ll_i, "cod_operatore",ls_cod_operatore)
	dw_elenco_operatori_selezionati.setitem(ll_i, "des_operatore",ls_des_operatore)
	
	//pre-seleziona l'operatore default dell'utente collegato
	if ls_cod_operatore_utente<>"" and ls_cod_operatore_utente= ls_cod_operatore then
		dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", "S")
	else
		dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", "N")
	end if
	
loop
close cu_operatori;

dw_elenco_operatori_selezionati.setsort("flag_selezionato desc, cod_operatore asc")
dw_elenco_operatori_selezionati.sort()

return
end subroutine

event pc_setwindow;call super::pc_setwindow;
windowobject lw_oggetti_1[],lw_oggetti_2[],lw_oggetti_3[]

set_w_options(c_noenablepopup)

lw_oggetti_1[1] = dw_ext_selezione_gen_commesse
lw_oggetti_1[2] = dw_elenco_operatori_selezionati
lw_oggetti_1[3] = cb_aggiorna
//lw_oggetti_1[4] = cb_ricerca_cliente
lw_oggetti_1[4] = cb_reset
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti_1[])
lw_oggetti_1[1] = dw_tes_gen_commesse
lw_oggetti_1[2] = cbx_forza_numerazione
lw_oggetti_1[3] = sle_num_commessa_forzato
lw_oggetti_1[4] = cb_dettaglio
lw_oggetti_1[5] = cb_genera
dw_folder.fu_assigntab(2, "Ordini", lw_oggetti_1[])
lw_oggetti_2[1] = dw_gen_orig_dest
dw_folder.fu_assigntab(3, "Commesse", lw_oggetti_2[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_tes_gen_commesse.set_dw_options(sqlca, &
                                   pcca.null_object, &
											  c_noretrieveonopen + &
                                   c_multiselect + &
						   			     c_nonew + &
							   		     c_nomodify + &
											  c_nodelete + &
											  c_disablecc + &
											  c_disableccinsert, &
                                   c_default)

dw_gen_orig_dest.set_dw_options(sqlca, &
                             	  pcca.null_object, &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete + &
										  c_disablecc + &
										  c_disableccinsert, &
	                             c_viewmodeblack)
													 

dw_ext_selezione_gen_commesse.set_dw_options(sqlca, &
                             	  pcca.null_object,&
										  c_newonopen, &
	                             c_default)
													 
save_on_close(c_socnosave)


iuo_fido_cliente = create uo_fido_cliente

postevent("ue_imposta_data")

wf_carica_operatori()
end event

on w_tes_gen_commesse.create
int iCurrent
call super::create
this.dw_elenco_operatori_selezionati=create dw_elenco_operatori_selezionati
this.sle_num_commessa_forzato=create sle_num_commessa_forzato
this.cbx_forza_numerazione=create cbx_forza_numerazione
this.cb_genera=create cb_genera
this.cb_dettaglio=create cb_dettaglio
this.dw_tes_gen_commesse=create dw_tes_gen_commesse
this.cb_reset=create cb_reset
this.cb_aggiorna=create cb_aggiorna
this.cbx_sel_tutti=create cbx_sel_tutti
this.dw_ext_selezione_gen_commesse=create dw_ext_selezione_gen_commesse
this.dw_gen_orig_dest=create dw_gen_orig_dest
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_elenco_operatori_selezionati
this.Control[iCurrent+2]=this.sle_num_commessa_forzato
this.Control[iCurrent+3]=this.cbx_forza_numerazione
this.Control[iCurrent+4]=this.cb_genera
this.Control[iCurrent+5]=this.cb_dettaglio
this.Control[iCurrent+6]=this.dw_tes_gen_commesse
this.Control[iCurrent+7]=this.cb_reset
this.Control[iCurrent+8]=this.cb_aggiorna
this.Control[iCurrent+9]=this.cbx_sel_tutti
this.Control[iCurrent+10]=this.dw_ext_selezione_gen_commesse
this.Control[iCurrent+11]=this.dw_gen_orig_dest
this.Control[iCurrent+12]=this.dw_folder
end on

on w_tes_gen_commesse.destroy
call super::destroy
destroy(this.dw_elenco_operatori_selezionati)
destroy(this.sle_num_commessa_forzato)
destroy(this.cbx_forza_numerazione)
destroy(this.cb_genera)
destroy(this.cb_dettaglio)
destroy(this.dw_tes_gen_commesse)
destroy(this.cb_reset)
destroy(this.cb_aggiorna)
destroy(this.cbx_sel_tutti)
destroy(this.dw_ext_selezione_gen_commesse)
destroy(this.dw_gen_orig_dest)
destroy(this.dw_folder)
end on

event close;call super::close;

destroy iuo_fido_cliente
end event

type dw_elenco_operatori_selezionati from datawindow within w_tes_gen_commesse
boolean visible = false
integer x = 46
integer y = 880
integer width = 1897
integer height = 740
integer taborder = 40
string title = "none"
string dataobject = "d_elenco_operatori_selezionati"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;choose case dwo.name
	case "flag_selezionato"
		if data="N" then
			//hai deselezionato l'operatore della riga: metti il checkbox "Seleziona Tutti a N"
			cbx_sel_tutti.checked = false
		end if
		
end choose
end event

type sle_num_commessa_forzato from singlelineedit within w_tes_gen_commesse
integer x = 3314
integer y = 1696
integer width = 320
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
boolean autohscroll = false
end type

type cbx_forza_numerazione from checkbox within w_tes_gen_commesse
event clicked pbm_bnclicked
integer x = 2693
integer y = 1716
integer width = 590
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Forza Num. Commessa"
boolean lefttext = true
end type

event clicked;if sle_num_commessa_forzato.enabled=true then
	sle_num_commessa_forzato.enabled=false
else
	sle_num_commessa_forzato.enabled=true
end if

end event

type cb_genera from uo_cb_ok within w_tes_gen_commesse
event clicked pbm_bnclicked
integer x = 1943
integer y = 1700
integer width = 361
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;call super::clicked;long			ll_anno_registrazione, ll_num_registrazione, ll_selected[], ll_anno_ordine, ll_null, ll_tot_sel, ll_row_ds, &
				ll_num_ordine, ll_prog_riga_ord_ven, ll_i,ll_test,ll_num_commessa_forzato,ll_num_sequenza,ll_num_commessa
				
string			ls_cod_tipo_ord_ven, ls_cod_operatore, ls_cod_deposito_versamento, ls_errore, &
				ls_cod_deposito_prelievo, ls_cod_tipo_det_ven, ls_cod_prodotto, ls_flag_tipo_det_ven, &
				ls_cod_tipo_commessa,ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_misura, & 
				ls_cod_prodotto_variante,ls_cod_versione,ls_des_estesa,ls_flag_esclusione, & 
				ls_formula_tempo,ls_flag_materia_prima,ls_flag_non_collegati, ls_messaggio,ls_cod_cliente, &
				ls_clienti_con_warning[], ls_clienti_elaborati[], ls_clienti_con_blocchi[]
				
datetime		ldt_data_consegna_inizio,ldt_data_consegna_fine, ldt_oggi

dec{4}		ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa,ldd_quan_tecnica,ldd_quan_utilizzo, & 
				ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_coef_calcolo
				
integer		li_risposta,li_anno_commessa

boolean		lb_salta, lb_esclusi

uo_funzioni_1		luo_funzioni_1
uo_log_sistema		luo_log

datastore			lds_data


setpointer(Hourglass!)

lb_esclusi = false

// Enrico 03-04-2013 -----
// Prima di eseguire la procedura devo verificare che un altro utente non la stia eseguendo in parallelo
luo_log = create uo_log_sistema

if luo_log.uof_verify_process("GCI", "GCF", ls_messaggio) > 0 then
	
	if g_mb.messagebox("Apice", ls_messaggio, Question!, YesNo!, 2) = 2 then
		setpointer(Arrow!)
		return
	else
		if g_mb.messagebox("Apice", "Sei assolutamente certo di voler procedere (questa conferma viene registrata) ?",Question!, YesNo!, 2) = 2 then
			setpointer(Arrow!)
			return
		end if
	end if
	
end if

// Segno nella log_sistema che sto eseguendo la procedura, così altri utenti saranno avvisati
luo_log = create uo_log_sistema
luo_log.uof_write_log_sistema("GCI","GENERAZIONE COMMESSE INIZIO")

commit using sqlca;
destroy luo_log

// ora posso iniziare

luo_funzioni_1 = create uo_funzioni_1

setpointer(hourglass!)

ldt_data_consegna_inizio = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_inizio")
ldt_data_consegna_fine = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_fine")
ls_flag_non_collegati = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"flag_non_collegati")

if isnull(ls_flag_non_collegati) then ls_flag_non_collegati = 'N'

dw_tes_gen_commesse.get_selected_rows(ll_selected[])

ll_tot_sel = upperbound(ll_selected[])

if cbx_forza_numerazione.checked = true then
	
	ll_num_commessa_forzato = long(sle_num_commessa_forzato.text)
	
	ll_anno_registrazione = f_anno_esercizio()
	
	select count( *)
	into   :ll_test
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
			 anno_commessa = :ll_anno_registrazione	and    
			 num_commessa = :ll_num_commessa_forzato;

	if sqlca.sqlcode = 0 and not isnull(ll_test) and ll_test > 0 then
		setpointer(Arrow!)
		g_mb.messagebox( "APICE", "Attenzione: il numero di commessa indicato esiste già! E' necessario indicare un altro numero.", Stopsign!, ok!)
		return
	end if			
	
end if

setnull(ll_anno_registrazione)
setnull(ll_num_commessa_forzato)
ldt_oggi = datetime(today(), 00:00:00)

lds_data = create datastore
lds_data.dataobject = "d_riepilogo_documenti_fido"
lds_data.settransobject(sqlca)


for ll_i = 1 to ll_tot_sel
	Yield()
	
	ls_cod_tipo_ord_ven = dw_tes_gen_commesse.getitemstring(ll_selected[ll_i], "cod_tipo_ord_ven")
	ll_anno_ordine = dw_tes_gen_commesse.getitemnumber(ll_selected[ll_i], "anno_registrazione")
	ll_num_ordine = dw_tes_gen_commesse.getitemnumber(ll_selected[ll_i], "num_registrazione")

	select cod_cliente
	into :ls_cod_cliente
	from tes_ord_ven
	where 	cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :ll_anno_ordine and  
				num_registrazione = :ll_num_ordine;
	
	//#################################################################################
	li_risposta = iuo_fido_cliente.uof_get_blocco_cliente_loop(	ls_cod_cliente, ldt_oggi, is_cod_parametro_blocco, &
																				ls_clienti_elaborati[], ls_clienti_con_blocchi[], ls_clienti_con_warning[], &
																				lb_salta, ls_messaggio)
	if li_risposta <> 0 then
		
		//commentato perchè c'è un unico riepilogo alla fine !!!!!!
		
		//if ls_messaggio<>"" then
			//dai il messaggio (capita solo la prima volta che incontri un cliente bloccato o con warning nell'azione corrente)
			//if li_risposta=2 then
				//g_mb.error(ls_messaggio)
			//else
				//g_mb.warning(ls_messaggio)
			//end if
		//end if
		
		
		//salvo in un datastore
		ll_row_ds = lds_data.insertrow(0)
		lds_data.setitem(ll_row_ds, "cod_cliente", ls_cod_cliente)
		lds_data.setitem(ll_row_ds, "rag_soc_1", f_des_tabella ( "anag_clienti", "cod_cliente = '" +  ls_cod_cliente + "'", "rag_soc_1" ))
		lds_data.setitem(ll_row_ds, "anno_ordine", ll_anno_ordine)
		lds_data.setitem(ll_row_ds, "num_ordine", ll_num_ordine)
		if ls_messaggio<>"" then
			lds_data.setitem(ll_row_ds, "messaggio", ls_messaggio)
			lds_data.setitem(ll_row_ds, "ordinamento", 0)
		else
			lds_data.setitem(ll_row_ds, "ordinamento", 1)
		end if
		
		lds_data.setitem(ll_row_ds, "tipo_blocco", li_risposta)
		
	end if
	
	if lb_salta then
		//caso di blocco, quindi salto riga
		lb_esclusi = true
		continue
	end if
	//#################################################################################
	
	pcca.mdi_frame.setmicrohelp("Elaborazione Ordine N° " + string(ll_anno_ordine) + " / " + string(ll_num_ordine) + " in corso ...")


	declare cu_det_ord_ven cursor for  
	select	prog_riga_ord_ven
	from 	   det_ord_ven  
	where 	cod_azienda = :s_cs_xx.cod_azienda and anno_registrazione = :ll_anno_ordine and  
				num_registrazione = :ll_num_ordine and quan_in_evasione + det_ord_ven.quan_evasa < quan_ordine and flag_blocco = 'N' and 
				data_consegna between :ldt_data_consegna_inizio and :ldt_data_consegna_fine and
				flag_gen_commessa='S' and anno_commessa is null
	order by prog_riga_ord_ven asc;

	open cu_det_ord_ven;
	
	do while 0 = 0
		fetch cu_det_ord_ven 
		into  :ll_prog_riga_ord_ven;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			setpointer(Arrow!)
			pcca.mdi_frame.setmicrohelp("Errore!")
			g_mb.messagebox("Attenzione", "Errore durante la lettura Dettagli Ordini Vendita.", Exclamation!, ok!)
			rollback;
			close cu_det_ord_ven;
			return
		end if
		
		if cbx_forza_numerazione.checked = true then
			ll_num_commessa_forzato = long(sle_num_commessa_forzato.text)
		else
			setnull(ll_num_commessa_forzato)
		end if
		
		// *** il flag imposta non collegati lo metto sempre a si, perchè se trovo l'ordine di quel tipo in questa lista, vuol 
		//     dire che ho impostato il filtro e ho deciso di includerli
		if ls_flag_non_collegati = "S" then
			luo_funzioni_1.uof_set_flag_non_collegati(true)
		else
			luo_funzioni_1.uof_set_flag_non_collegati(false)
		end if
		
		li_risposta = luo_funzioni_1.uof_genera_commesse ( ls_cod_tipo_ord_ven, ll_anno_ordine, ll_num_ordine, & 
																			ll_prog_riga_ord_ven, ll_num_commessa_forzato, & 
																			li_anno_commessa, ll_num_commessa, ls_errore )
		
		if li_risposta < 0 then
			setpointer(Arrow!)
			pcca.mdi_frame.setmicrohelp("Errore!")
			g_mb.messagebox("Sep",ls_errore,stopsign!)
			rollback;
			return
		end if
		
		dw_gen_orig_dest.insertrow(0)
		dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", ll_anno_ordine)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig", ll_num_ordine)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", li_anno_commessa)
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest", ll_num_commessa)
	
		if not isnull(ll_num_commessa_forzato) then 
			ll_num_commessa_forzato++
			sle_num_commessa_forzato.text = string(ll_num_commessa_forzato)
		end if
		
	loop
	close cu_det_ord_ven;
next

commit;

pcca.mdi_frame.setmicrohelp("Elaborazione terminata!")

destroy luo_funzioni_1

luo_log = create uo_log_sistema
luo_log.uof_write_log_sistema("GCF","GENERAZIONE COMMESSE FINE")

commit using sqlca;
destroy luo_log

setpointer(Arrow!)

dw_tes_gen_commesse.reset()
cb_aggiorna.triggerevent("clicked")

dw_gen_orig_dest.reset_dw_modified(c_resetchildren)
dw_tes_gen_commesse.reset_dw_modified(c_resetchildren)

setpointer(arrow!)
dw_folder.fu_selecttab(3)


if lb_esclusi then
	g_mb.warning("Alcuni clienti negli ordini risultano bloccati o con blocco finanziario "+&
						"e la tabella di configurazione 'parametri blocco' indica di bloccare la generazione delle commesse!~r~nPer questi ordini non saranno generate commesse!~r~n"+&
						"Premere OK per visualizzare il riepilogo.")
end if

if lds_data.rowcount() > 0 then
	s_cs_xx.parametri.parametro_ds_1 = lds_data
	s_cs_xx.parametri.parametro_s_10 = "ORDINI CON COMMESSE NON CREATE (PER BLOCCO CLIENTE) O CREATE (CON WARNING)"
	window_open(w_riepilogo_documenti_fido, 0)
end if

destroy lds_data


end event

type cb_dettaglio from commandbutton within w_tes_gen_commesse
event clicked pbm_bnclicked
integer x = 2327
integer y = 1700
integer width = 361
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;long ll_selected[], ll_i


dw_tes_gen_commesse.get_selected_rows(ll_selected[])

if upperbound(ll_selected) > 1 then
   g_mb.messagebox("Attenzione", "Selezionare solamente un ordine.", exclamation!, ok!)
   return
end if

if dw_tes_gen_commesse.getrow() > 0 then
	s_cs_xx.parametri.parametro_s_1 = dw_tes_gen_commesse.getitemstring(dw_tes_gen_commesse.getrow(), "cod_tipo_ord_ven")
	s_cs_xx.parametri.parametro_d_1 = dw_tes_gen_commesse.getitemnumber(dw_tes_gen_commesse.getrow(), "anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = dw_tes_gen_commesse.getitemnumber(dw_tes_gen_commesse.getrow(), "num_registrazione")
	s_cs_xx.parametri.parametro_b_1 = false
	s_cs_xx.parametri.parametro_data_1 = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_inizio")
	s_cs_xx.parametri.parametro_data_2 = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_fine")
	
	if dw_ext_selezione_gen_commesse.getitemstring( dw_ext_selezione_gen_commesse.getrow(), "flag_non_collegati") = "S" then
		s_cs_xx.parametri.parametro_s_5 = "C"
	else
		s_cs_xx.parametri.parametro_s_5 = "N"
	end if
	
end if

window_open(w_det_gen_commesse, 0)

if s_cs_xx.parametri.parametro_b_1 then
	for ll_i=1 to upperbound(s_cs_xx.parametri.parametro_d_1_a)
		dw_gen_orig_dest.insertrow(0)
		dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", s_cs_xx.parametri.parametro_d_1_a[ll_i])
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig",  s_cs_xx.parametri.parametro_d_2_a[ll_i])
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", s_cs_xx.parametri.parametro_d_3_a[ll_i])
		dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest",  s_cs_xx.parametri.parametro_d_4_a[ll_i])
	next

	dw_folder.fu_selecttab(2)
	dw_tes_gen_commesse.triggerevent("pcd_retrieve")
end if

end event

type dw_tes_gen_commesse from uo_cs_xx_dw within w_tes_gen_commesse
event pcd_retrieve pbm_custom60
integer x = 69
integer y = 140
integer width = 3817
integer height = 1460
integer taborder = 40
string dataobject = "d_tes_gen_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;boolean					lb_ok, lb_fsu
string						ls_cod_cliente, ls_cod_tipo_ord_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven,ls_flag_gen_commessa, ls_sql, &
							ls_flag_operatori,ls_cod_operatore, ls_flag_stampata_bcl,ls_rag_soc_1, ls_flag_non_collegati, ls_cod_deposito, ls_error
long						ll_anno_registrazione, ll_num_registrazione, ll_i, ll_max, ll_y, ll_z, ll_num_ordine_filtro
datetime					ldt_data_registrazione,ldt_data_consegna_inizio,ldt_data_consegna_fine, ldt_data_reg_inizio, ldt_data_reg_fine, &
							ldt_data_consegna
integer					li_anno_ordine_filtro

ldt_data_consegna_inizio = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_inizio")
ldt_data_consegna_fine = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_fine")
ldt_data_reg_inizio = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_inizio")
ldt_data_reg_fine = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_fine")
ls_cod_cliente = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"cod_cliente")
ls_flag_operatori = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"flag_operatori")
ls_flag_non_collegati = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"flag_non_collegati")

li_anno_ordine_filtro = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"anno_ordine")
ll_num_ordine_filtro = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"num_ordine")

if isnull(ls_flag_non_collegati) then ls_flag_non_collegati = 'N'

dw_tes_gen_commesse.reset()

declare cu_tes_ord_ven dynamic cursor for sqlsa;
ls_sql = " select anno_registrazione, num_registrazione, data_registrazione, cod_cliente, "+&
					"cod_tipo_ord_ven, flag_stampata_bcl, data_consegna from tes_ord_ven  " + &
         	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' and  flag_evasione <> 'E' and " + &
				" ( (data_registrazione between '" + string(ldt_data_reg_inizio,s_cs_xx.db_funzioni.formato_data) + "' and '" + string(ldt_data_reg_fine,s_cs_xx.db_funzioni.formato_data) + "') "+&
										"or data_registrazione is null) "
				
if not isnull(ls_cod_cliente) and len(ls_cod_cliente) > 0 then
	ls_sql = ls_sql + " and cod_cliente = '" + ls_cod_cliente + "'"
end if

if li_anno_ordine_filtro > 0 then
	ls_sql = ls_sql + " and anno_registrazione = " + string(li_anno_ordine_filtro) + " "
end if

if ll_num_ordine_filtro > 0 then
	ls_sql = ls_sql + " and num_registrazione = " + string(ll_num_ordine_filtro) + " "
end if


// stefanop: 09/01/2012: recupero deposito collegato all'utente collegato
guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_error)

guo_functions.uof_get_parametro_azienda("FSU", lb_fsu)

if len(ls_cod_deposito) > 0 and not lb_fsu then
	ls_sql += " and cod_deposito='" + ls_cod_deposito + "' "
end if
// ---


ll_z = 0
ll_y = 0
if ls_flag_operatori = "S" then
	// conto gli operatori selezionati
	for ll_i = 1 to dw_elenco_operatori_selezionati.rowcount()
		if dw_elenco_operatori_selezionati.getitemstring(ll_i, "flag_selezionato") = "S" then ll_z ++
	next
   // creo sintassi SQL
	for ll_i = 1 to dw_elenco_operatori_selezionati.rowcount()
		if dw_elenco_operatori_selezionati.getitemstring(ll_i, "flag_selezionato") = "S" then
			ls_cod_operatore = dw_elenco_operatori_selezionati.getitemstring(ll_i, "cod_operatore")
			if not isnull(ls_cod_operatore) then
				ll_y ++
				if ll_y = 1 then ls_sql = ls_sql + " and cod_operatore in ("
				
				ls_sql = ls_sql + "'" + ls_cod_operatore + "'"
				
				if ll_y = ll_z then 
					ls_sql = ls_sql + ")"
				else
					ls_sql = ls_sql + ","
				end if
			end if
		end if
	next
end if			
ls_sql = ls_sql + " order by anno_registrazione asc, num_registrazione asc"

PREPARE SQLSA FROM :ls_sql;

open dynamic cu_tes_ord_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante la creazione del cursore cu_tes_ord_ven. Dettaglio " + sqlca.sqlerrtext)
	return
end if
do while 0 = 0
   fetch cu_tes_ord_ven into :ll_anno_registrazione,   
									  :ll_num_registrazione,   
									  :ldt_data_registrazione,   
									  :ls_cod_cliente,   
									  :ls_cod_tipo_ord_ven,
									  :ls_flag_stampata_bcl,
									  :ldt_data_consegna;

   if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore durante la fetch del cursore cu_tes_ord_ven. Dettaglio " + sqlca.sqlerrtext)
		rollback;
		return
	end if

	declare cu_det_ord_ven cursor for 
		select cod_tipo_det_ven,
				 flag_gen_commessa
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :ll_anno_registrazione and 
				 num_registrazione = :ll_num_registrazione and 
				 quan_in_evasione + quan_evasa < quan_ordine and  
				 flag_blocco = 'N' and  
  				 data_consegna between :ldt_data_consegna_inizio and :ldt_data_consegna_fine and
				 anno_commessa is null;


	open cu_det_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante la creazione del cursore cu_det_ord_ven. Dettaglio " + sqlca.sqlerrtext)
		return
	end if
	lb_ok = false
	do while 0 = 0
		fetch cu_det_ord_ven 
		into :ls_cod_tipo_det_ven,
			  :ls_flag_gen_commessa;
	
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante la fetch del cursore cu_det_ord_ven. Dettaglio " + sqlca.sqlerrtext)
			rollback;
			return
		end if

		select flag_tipo_det_ven 
		into   :ls_flag_tipo_det_ven  
		from   tab_tipi_det_ven  
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if ls_flag_tipo_det_ven = 'M' and ls_flag_gen_commessa='S' then
			lb_ok = true
			exit
		else
			if ls_flag_gen_commessa = 'S' then
				if ls_flag_non_collegati = 'S' and ls_flag_tipo_det_ven = 'C' then
					lb_ok = true
					exit
				end if
			end if
		end if
	loop
	close cu_det_ord_ven;

	if lb_ok = true then
		dw_tes_gen_commesse.insertrow(0)
		dw_tes_gen_commesse.setrow(dw_tes_gen_commesse.rowcount())
		dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "anno_registrazione", ll_anno_registrazione)
		dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "num_registrazione", ll_num_registrazione)
		dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "data_registrazione", ldt_data_registrazione)
		dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "cod_cliente", ls_cod_cliente)
		dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "flag_stampata_bcl", ls_flag_stampata_bcl)
		dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "data_consegna", ldt_data_consegna)
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cliente = :ls_cod_cliente;
		if sqlca.sqlcode = 0 then
			dw_tes_gen_commesse.setitem(dw_tes_gen_commesse.getrow(), "rag_soc_1", ls_rag_soc_1)
		end if
	end if
loop
close cu_tes_ord_ven;
resetupdate()
commit;
end event

type cb_reset from commandbutton within w_tes_gen_commesse
integer x = 1851
integer y = 660
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string					ls_null
datetime				ldt_data
integer				li_null

setnull(ls_null)
setnull(li_null)
ldt_data = datetime(today(),00:00:00)

dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_inizio",datetime(date("01/01/1900"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_fine",datetime(date("31/12/2099"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_inizio",datetime(date("01/01/1900"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_fine",datetime(date("31/12/2099"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"cod_cliente", ls_null)
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"flag_operatori", "N")
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"flag_non_collegati", "N")

dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"anno_ordine", f_anno_esercizio())
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"num_ordine", li_null)




end event

type cb_aggiorna from commandbutton within w_tes_gen_commesse
integer x = 2240
integer y = 660
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_ext_selezione_gen_commesse.accepttext()
dw_tes_gen_commesse.change_dw_current()

setpointer(Hourglass!)
parent.triggerevent("pc_retrieve")
dw_folder.fu_selecttab(2)
setpointer(Arrow!)
end event

type cbx_sel_tutti from checkbox within w_tes_gen_commesse
boolean visible = false
integer x = 1330
integer y = 780
integer width = 475
integer height = 88
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seleziona tutti"
boolean checked = true
boolean lefttext = true
end type

event clicked;/**
 * Stefano
 * 08/04/2010
 *
 * Progetto 140
 **/
 
string ls_flag = "N"
long ll_i

if cbx_sel_tutti.checked then ls_flag = "S"

for ll_i = 1 to dw_elenco_operatori_selezionati.rowcount()
	dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", ls_flag)
next
end event

type dw_ext_selezione_gen_commesse from uo_cs_xx_dw within w_tes_gen_commesse
integer x = 69
integer y = 140
integer width = 2697
integer height = 660
integer taborder = 30
string dataobject = "d_ext_selezione_gen_commesse"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_operatore, ls_des_operatore
long ll_i

if i_extendmode then
	choose case i_colname
		case "flag_operatori"
			if i_coltext = "N" then
				//dw_elenco_operatori_selezionati.reset()
				cbx_sel_tutti.visible = false
				
				dw_elenco_operatori_selezionati.visible = false
				
			else
				cbx_sel_tutti.visible = true
				cbx_sel_tutti.checked = true
				
				dw_elenco_operatori_selezionati.visible = true
				
				//dw_elenco_operatori_selezionati.reset()
//				declare cu_operatori cursor for
//				select cod_operatore, des_operatore
//				from   tab_operatori
//				where  cod_azienda = :s_cs_xx.cod_azienda and
//				       flag_blocco = 'N'
//				order by cod_operatore;
//				open cu_operatori;
//				do while true
//					fetch cu_operatori into :ls_cod_operatore, :ls_des_operatore;
//					if sqlca.sqlcode <> 0 then exit
//					ll_i = dw_elenco_operatori_selezionati.insertrow(0)
//					dw_elenco_operatori_selezionati.setitem(ll_i, "cod_operatore",ls_cod_operatore)
//					dw_elenco_operatori_selezionati.setitem(ll_i, "des_operatore",ls_des_operatore)
//				loop
//				close cu_operatori;
//				commit;
			end if
	end choose		
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_ext_selezione_gen_commesse,"cod_cliente")
	end choose
end event

type dw_gen_orig_dest from uo_cs_xx_dw within w_tes_gen_commesse
event pcd_retrieve pbm_custom60
integer x = 69
integer y = 140
integer width = 2240
integer height = 1520
integer taborder = 20
string dataobject = "d_gen_orig_dest_com"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type dw_folder from u_folder within w_tes_gen_commesse
integer x = 23
integer y = 20
integer width = 3909
integer height = 1780
integer taborder = 30
end type

event po_tabclicked;call super::po_tabclicked;string			ls_flag_operatori
long			ll_row



if upper(i_SelectedTabName) = "SELEZIONE" then

	ll_row = dw_ext_selezione_gen_commesse.getrow()

	if ll_row>0 then
	else
		return
	end if
	
	ls_flag_operatori = dw_ext_selezione_gen_commesse.getitemstring(ll_row, "flag_operatori")
	
	if ls_flag_operatori = "N" then		
		dw_elenco_operatori_selezionati.visible = false
	else
		dw_elenco_operatori_selezionati.visible = true
	end if

end if
end event

