﻿$PBExportHeader$w_tes_ord_ven_sessioni_evas_ord_ven.srw
forward
global type w_tes_ord_ven_sessioni_evas_ord_ven from w_cs_xx_principale
end type
type dw_evas_ord_ven from uo_std_dw within w_tes_ord_ven_sessioni_evas_ord_ven
end type
end forward

global type w_tes_ord_ven_sessioni_evas_ord_ven from w_cs_xx_principale
integer width = 3922
string title = "Evasioni Ordine"
dw_evas_ord_ven dw_evas_ord_ven
end type
global w_tes_ord_ven_sessioni_evas_ord_ven w_tes_ord_ven_sessioni_evas_ord_ven

type variables
uo_cs_xx_dw luo_dw_parent
long il_anno_ordine, il_num_ordine
end variables

event pc_setwindow;call super::pc_setwindow;luo_dw_parent = create uo_cs_xx_dw
luo_dw_parent = i_openparm

dw_evas_ord_ven.settransobject(sqlca)

il_anno_ordine = luo_dw_parent.getitemnumber(luo_dw_parent.getrow(),"anno_registrazione")
il_num_ordine = luo_dw_parent.getitemnumber(luo_dw_parent.getrow(),"num_registrazione")

dw_evas_ord_ven.event ue_retrieve()

end event

on w_tes_ord_ven_sessioni_evas_ord_ven.create
int iCurrent
call super::create
this.dw_evas_ord_ven=create dw_evas_ord_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_evas_ord_ven
end on

on w_tes_ord_ven_sessioni_evas_ord_ven.destroy
call super::destroy
destroy(this.dw_evas_ord_ven)
end on

type dw_evas_ord_ven from uo_std_dw within w_tes_ord_ven_sessioni_evas_ord_ven
event ue_retrieve ( )
integer x = 23
integer y = 16
integer width = 3840
integer height = 1304
integer taborder = 10
string dataobject = "d_tes_ord_ven_evas_ord_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
boolean ib_dw_report = true
end type

event ue_retrieve();long ll_ret

ll_ret = retrieve(s_cs_xx.cod_azienda, il_anno_ordine, il_num_ordine)

end event

event buttonclicked;call super::buttonclicked;integer								li_return
long									ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
longlong								ll_sessione
string									ls_perc, ls_messaggio, ls_flag_evasione, ls_flag_blocco
uo_generazione_documenti		luo_gen_doc

choose case dwo.name
	case "b_elimina_sessione", "b_elimina_riga"

		if row > 0 then
			
			ll_anno_registrazione = getitemnumber(row, "evas_ord_ven_anno_registrazione")
			ll_num_registrazione = getitemnumber(row, "evas_ord_ven_num_registrazione")
			if  dwo.name = "b_elimina_riga" then
				ll_prog_riga_ord_ven = getitemnumber(row, "evas_ord_ven_prog_riga_ord_ven")
			else
				ll_prog_riga_ord_ven = 0
			end if
			ll_sessione = getitemnumber(row, "evas_ord_ven_prog_sessione")
			
			if ll_anno_registrazione= 0 or ll_num_registrazione = 0 then
				return
			end if
			
			select flag_evasione,
					flag_blocco
			into	:ls_flag_evasione,
					:ls_flag_blocco
			from 	tes_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione;
			
			if ls_flag_evasione="E" then
				g_mb.warning("Attenzione", "Annullamento Sessione Impossibile: Testata Ordine già Evasa!")
				return
				
			elseif ls_flag_blocco="S" then
				g_mb.warning("Attenzione", "Annullamento Sessione Impossibile: Testata Ordine Bloccata!")
				return
				
			end if
			
			//se arrivi fin qui continua ...
			luo_gen_doc = create uo_generazione_documenti
			li_return = luo_gen_doc.uof_azz_ass_ord_ven_sessione( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_sessione, ls_messaggio)
			destroy luo_gen_doc;
			
			if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("Azzeramento assegnazione",ls_messaggio)
		
			if li_return = 0 then
				g_mb.error("Attenzione", "Azzeramento Assegnazione non avvenuta!")
				return
			end if
		
			g_mb.success("Informazione", "Azzeramento Assegnazione avvenuta con successo!")
			
		else
			
			return
		end if		
		
end choose

event ue_retrieve()
end event

