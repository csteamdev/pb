﻿$PBExportHeader$w_tes_ord_ven_tv.srw
$PBExportComments$Finestra Gestione Testata Ordini di Vendita
forward
global type w_tes_ord_ven_tv from w_cs_xx_principale
end type
type dw_tes_off_ven from datawindow within w_tes_ord_ven_tv
end type
type dw_folder_tv from u_folder within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_3 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_ricerca from uo_std_dw within w_tes_ord_ven_tv
end type
type cb_duplica_doc from commandbutton within w_tes_ord_ven_tv
end type
type tv_1 from treeview within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_1 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_2 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_4 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type dw_folder from u_folder within w_tes_ord_ven_tv
end type
end forward

global type w_tes_ord_ven_tv from w_cs_xx_principale
integer x = 0
integer y = 0
integer width = 4585
integer height = 2992
string title = "Ordini Clienti"
event ue_genera_bolla ( )
event ue_duplica_sconto ( )
event ue_proforma ( )
event ue_evas_parziale ( )
event ue_chiudi_commessa ( )
event ue_azzera ( )
event ue_assegnazione ( )
event ue_corrispondenze ( )
event ue_note ( )
event ue_sblocca ( )
event ue_blocca ( )
event ue_calcola ( )
event ue_stampa ( )
event ue_dettaglio ( )
event ue_salda ( )
event ue_aggiungi_conferma_ordine ( )
event ue_stampa_conferma_ordine ( )
event ue_menu_azzera_spese_trasporto ( )
event ue_menu_abilita_spese_trasporto ( )
event ue_visualizza_spese_trasporto ( )
event ue_importa_excel ( )
event ue_documenti_fiscali ( )
dw_tes_off_ven dw_tes_off_ven
dw_folder_tv dw_folder_tv
dw_tes_ord_ven_det_3 dw_tes_ord_ven_det_3
dw_tes_ord_ven_ricerca dw_tes_ord_ven_ricerca
cb_duplica_doc cb_duplica_doc
tv_1 tv_1
dw_tes_ord_ven_det_1 dw_tes_ord_ven_det_1
dw_tes_ord_ven_det_2 dw_tes_ord_ven_det_2
dw_tes_ord_ven_det_4 dw_tes_ord_ven_det_4
dw_folder dw_folder
end type
global w_tes_ord_ven_tv w_tes_ord_ven_tv

type variables
boolean						ib_esiste_padre=TRUE, ib_new, ib_errore_ass = false, ib_gen_bolla
long							il_anno_registrazione, il_num_registrazione

integer						ii_protect_autorizza_sblocco = 1 //cioè protect

string							is_sql_filtro=""
string							is_join_det_ord_ven = " join det_ord_ven on det_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and "+&
																					"det_ord_ven.anno_registrazione=tes_ord_ven.anno_registrazione and "+&
																					"det_ord_ven.num_registrazione=tes_ord_ven.num_registrazione "
																					
boolean 						ib_tree_deleting = false, ib_retrieve=true
long 							il_livello_corrente, il_numero_riga, il_handle, il_handle_modificato, il_handle_cancellato

//								Aggiunto per conferma ordine
long							il_anno_conferma[], il_num_conferma[]

// spese trasporto
uo_spese_trasporto		iuo_spese_trasporto
uo_fido_cliente				iuo_fido_cliente
string							is_cod_parametro_blocco = "IOC"

private:
	// Indica se è stata configurata la tabella di importazione 
	// e quindi visualizza il menu "Importa da Excel"
	boolean ib_import_configured = false
end variables
forward prototypes
public subroutine wf_proteggi_colonne (long fl_anno_registrazione, long fl_num_registrazione, string fs_flag_evasione)
public subroutine wf_annulla ()
public subroutine wf_leggi_filtro ()
public subroutine wf_memorizza_filtro ()
public subroutine wf_predefinito ()
public subroutine wf_imposta_predefinito ()
public function integer wf_inserisci_anni (long fl_handle)
public function integer wf_leggi_parent (long fl_handle, ref string fs_sql)
public function boolean wf_liv_prodotto_is_present ()
public function integer wf_inserisci_clienti (long fl_handle)
public function integer wf_inserisci_data_consegna (long fl_handle)
public function integer wf_inserisci_data_registrazione (long fl_handle)
public function integer wf_inserisci_depositi (long fl_handle)
public function integer wf_inserisci_operatori (long fl_handle)
public function integer wf_inserisci_ordini (long fl_handle)
public function integer wf_inserisci_prodotti (long fl_handle)
public function integer wf_inserisci_singolo_ordine (long fl_handle, long fl_anno, long fl_numero)
public function integer wf_inserisci_tipi_ordini (long fl_handle)
public subroutine wf_ricerca ()
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_treeview ()
public subroutine wf_imposta_immagini_tv ()
public function integer wf_apri_offerta (long fl_anno_offerta, long fl_num_offerta)
public function integer wf_retrieve_offerte_vendita (long fl_anno_reg_ordine, long fl_num_reg_ordine)
public function longlong wf_get_prog_sessione ()
public function integer wf_apri_offerta (integer ai_anno_offerta, long al_num_offerta)
public function integer wf_elabora_excel (string as_file_name)
end prototypes

event ue_genera_bolla();//Donato 19-11-2008 prima di fare tutto verifica l'esposizione e lo scaduto del cliente
uo_fido_cliente			l_uo_fido_cliente
long						ll_return, ll_num_registrazione, ll_row
integer					li_anno_registrazione
string						ls_messaggio, ls_cod_cliente, ls_cod_tipo_ord_ven, ls_flag_tipo_bol_fat, ls_cod_parametro_blocco, ls_flag_evasione
datetime					ldt_oggi



ll_row = dw_tes_ord_ven_det_1.getrow()
if ll_row>0 then
else
	return
end if

ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(ll_row, "cod_cliente")
if ls_cod_cliente = "" or isnull(ls_cod_cliente) then return

li_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row,"anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row,"num_registrazione")
ls_flag_evasione = dw_tes_ord_ven_det_1.getitemstring(ll_row,"flag_evasione")

if li_anno_registrazione>0 and ll_num_registrazione>0 then
	
	if ls_flag_evasione="E" then
		g_mb.warning("Attenzione! L'ordine risulta già evaso!")
		return
	end if
	
else
	return
end if



//*******************************************************************************
//controllo parametri blocco
ldt_oggi = datetime(today(), 00:00:00)
ls_cod_tipo_ord_ven = dw_tes_ord_ven_det_1.getitemstring(ll_row, "cod_tipo_ord_ven")

select flag_tipo_bol_fat
into :ls_flag_tipo_bol_fat
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if ls_flag_tipo_bol_fat="B" then
	//segue ddt
	ls_cod_parametro_blocco = "GBV"
else
	//segue fattura
	ls_cod_parametro_blocco = "GFV"
end if

l_uo_fido_cliente = create uo_fido_cliente
ll_return = l_uo_fido_cliente.uof_get_blocco_cliente( ls_cod_cliente, ldt_oggi, ls_cod_parametro_blocco, ls_messaggio)

if ll_return=2 then
	//blocco
	destroy l_uo_fido_cliente
	g_mb.error(ls_messaggio)
	return
elseif ll_return=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
//*******************************************************************************


l_uo_fido_cliente.is_flag_autorizza_sblocco = dw_tes_ord_ven_det_1.getitemstring(ll_row, "flag_aut_sblocco_fido")

ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cod_cliente,datetime(today(),00:00:00),ls_messaggio)
destroy l_uo_fido_cliente

if ll_return = -1 then
	g_mb.messagebox("APICE","Errore in verifica esposizione cliente.~n" + ls_messaggio,exclamation!)
	return
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi
if ll_return = 2 then
	//blocca perchè non sono autorizzato
	return
end if
//fine modifica ---------------------------

// ------------------------------------------------------------------------------------------------------
// procedura di assegnazione automatica 
//
// nuovo pulsante di assegnazione, evasione, creazione bolla; specifica \gibus\progetti\SEMPLIFICAZIONI
// enrico 6/4/2001
// -------------------------------------------------------------------------------------------------------


//se ci sono righe con quantita in evasione diversa da zero segnala
select count(*)
into :ll_return
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_registrazione and
			num_registrazione=:ll_num_registrazione and
			flag_blocco='N' and
			quan_in_evasione>0 and
			flag_evasione<>'E';

if ll_return > 0 then
	if not g_mb.confirm(	"Attenzione! In quest'ordine risultano righe aperte/parziali con quantità assegnata diversa da zero! "+&
								"Si consiglia di eseguire prima l'azzeramento assegnazione! " +&
								"Premere OK per proseguire comunque con la generazione del documento fiscale altrimenti premere NO per annullare!") then
		
		g_mb.show("Generazione documento annullata!")
		return
		
	end if
end if



// ---------------------- assegno tutte le righe dell'ordine --------------------
ib_errore_ass = false

ib_gen_bolla = true

this.triggerevent("ue_assegnazione")
//in s_cs_xx.parametri.parametro_s_10 c', convertito in stringa, il prog sessione della spedizione

ib_gen_bolla = false

if ib_errore_ass = true then
	g_mb.show("Generazione bolla interrotta dall'utente")
	this.triggerevent("ue_azzera")
	return
end if

// ---------------------- apro finestra di richiesta dati bolla -----------------

s_cs_xx.parametri.parametro_s_5 = "TOTALE"
s_cs_xx.parametri.parametro_ul_1 = li_anno_registrazione
s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione
window_open(w_richiesta_dati_bolla, 0)
end event

event ue_duplica_sconto();long    ll_anno, ll_num

dec{4}  ld_sconto

ll_anno = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
ll_num = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")

if ll_anno>0 and ll_num>0 then
else
	return
end if

this.triggerevent("pc_retrieve")

if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "S" then
	g_mb.error("Ordine non modificabile! E' Bloccato.")
	return
end if

if not g_mb.confirm("Riportare lo sconto di testata su tutte le righe di dettaglio?") then
	return
end if

ld_sconto = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"sconto")

update det_ord_ven
set    sconto_1 = :ld_sconto
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore nella update di det_ord_ven: " + sqlca.sqlerrtext)
	rollback;
	return
end if

update tes_ord_ven
set    sconto = 0
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore nella update di tes_ord_ven: " + sqlca.sqlerrtext)
	rollback;
	return
end if

commit;

this.triggerevent("pc_retrieve")
end event

event ue_proforma();string ls_errore
long ll_anno_registrazione, ll_num_registrazione, ll_anno_fattura,ll_num_fattura
uo_generazione_documenti luo_generazione_documenti

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

if g_mb.messagebox("APICE","Sei sicuro di voler creare la fattura PROFORMA partendo da questo ordine?",Question!,YesNo!,2) = 1 then
	luo_generazione_documenti = create uo_generazione_documenti
	if luo_generazione_documenti.uof_crea_fattura_proforma(ll_anno_registrazione, ll_num_registrazione, ref ll_anno_fattura, ref ll_num_fattura, ref ls_errore) = -1 then
		rollback;
		g_mb.error("Errore in generazione fatture PROFORMA.~r~n" + ls_errore)
	else
		commit;
		g_mb.show("Generata con successo la fattura proforma " + string(ll_anno_fattura) + "/" + string(ll_num_fattura))
	end if
	destroy luo_generazione_documenti
end if
return

end event

event ue_evas_parziale();//Donato 19-11-2008 prima di fare tutto verifica l'esposizione e lo scaduto del cliente
uo_fido_cliente		l_uo_fido_cliente
long					ll_return, ll_num_registrazione, ll_row
integer				li_anno_registrazione
string					ls_messaggio, ls_cod_cliente, ls_cod_parametro_blocco, ls_flag_tipo_bol_fat, ls_cod_tipo_ord_ven, ls_flag_evasione
datetime				ldt_oggi


ll_row = dw_tes_ord_ven_det_1.getrow()

ll_row = dw_tes_ord_ven_det_1.getrow()
if ll_row>0 then
else
	return
end if


ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(ll_row, "cod_cliente")
if ls_cod_cliente = "" or isnull(ls_cod_cliente) then return

li_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row,"anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row,"num_registrazione")
ls_flag_evasione = dw_tes_ord_ven_det_1.getitemstring(ll_row,"flag_evasione")

if li_anno_registrazione>0 and ll_num_registrazione>0 then
	
	if ls_flag_evasione="E" then
		g_mb.warning("Attenzione! L'ordine risulta già evaso!")
		return
	end if
	
else
	return
end if


ldt_oggi = datetime(today(), 00:00:00)

//*******************************************************************************
//controllo parametri blocco
ls_cod_tipo_ord_ven = dw_tes_ord_ven_det_1.getitemstring(ll_row, "cod_tipo_ord_ven")

select flag_tipo_bol_fat
into :ls_flag_tipo_bol_fat
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if ls_flag_tipo_bol_fat="B" then
	//segue ddt
	ls_cod_parametro_blocco = "GBV"
else
	//segue fattura
	ls_cod_parametro_blocco = "GFV"
end if

l_uo_fido_cliente = create uo_fido_cliente
ll_return = l_uo_fido_cliente.uof_get_blocco_cliente( ls_cod_cliente, ldt_oggi, ls_cod_parametro_blocco, ls_messaggio)

if ll_return=2 then
	//blocco
	destroy l_uo_fido_cliente
	g_mb.error(ls_messaggio)
	return
elseif ll_return=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
//*******************************************************************************


l_uo_fido_cliente.is_flag_autorizza_sblocco = dw_tes_ord_ven_det_1.getitemstring(ll_row, "flag_aut_sblocco_fido")

ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cod_cliente,datetime(today(),00:00:00),ls_messaggio)
destroy l_uo_fido_cliente

if ll_return = -1 then
	g_mb.error("Errore in verifica esposizione cliente.~n" + ls_messaggio)
	return
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi
if ll_return = 2 then
	//blocca perchè non sono autorizzato
	return
end if
//fine modifica ---------------------------


// ------------------------------------------------------------------------------------------------------
// procedura di evasione parziale ordine


//se ci sono righe con quantita in evasione diversa da zero segnala
select count(*)
into :ll_return
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_registrazione and
			num_registrazione=:ll_num_registrazione and
			flag_blocco='N' and
			quan_in_evasione>0 and
			flag_evasione<>'E';

if ll_return > 0 then
	if not g_mb.confirm(	"Attenzione! In quest'ordine risultano righe aperte/parziali con quantità assegnata diversa da zero! "+&
								"Si consiglia di eseguire prima l'azzeramento assegnazione! " +&
								"Premere OK per proseguire comunque con l'evasione parziale dell'ordine altrimenti premere NO per annullare!") then
		
		g_mb.show("Operazione annullata!")
		return
		
	end if
end if


//
// nuovo pulsante di assegnazione, evasione, creazione bolla (PARZIALE); specifica \gibus\progetti\SEMPLIFICAZIONI
// enrico 6/4/2001
// -------------------------------------------------------------------------------------------------------


// ---------------------- apro finestra di richiesta righe ordine -----------------

s_cs_xx.parametri.parametro_s_5 = "PARZIALE"
s_cs_xx.parametri.parametro_ul_1 = li_anno_registrazione
s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione
window_open(w_richiesta_dati_bolla, 0)
end event

event ue_chiudi_commessa();integer li_anno_registrazione,li_anno_commessa[],li_flag_errore,li_risposta,li_anno_commessa_1,li_anno_c
long ll_num_registrazione,ll_num_commessa[],ll_prog_riga_ord_ven,ll_i,ll_t,ll_num_commessa_1,ll_num_c
string ls_flag_tipo_avanzamento,ls_errore, ls_null


if dw_tes_ord_ven_det_1.getrow()>0 then
else
	return
end if

setnull(ls_null)
li_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if li_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

ll_i = 0
li_flag_errore = 0

declare righe_det_ord_ven cursor for
select anno_commessa,
		 num_commessa
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione;

open righe_det_ord_ven;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore sul DB: " + sqlca.sqlerrtext)
	return
end if

do while 1=1
	fetch righe_det_ord_ven
	into  :li_anno_c,
			:ll_num_c;
			
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore sul DB: " + sqlca.sqlerrtext)
		exit
	end if
	
	if sqlca.sqlcode = 100 then exit

	if not isnull(li_anno_c)  and not isnull(ll_num_c) then 
		ll_i++
		li_anno_commessa[ll_i]= li_anno_c
		ll_num_commessa[ll_i]=ll_num_c
	end if
	
loop

close righe_det_ord_ven;


for ll_t = 1 to upperbound(li_anno_commessa[])
	
	li_anno_commessa_1 = li_anno_commessa[ll_t]
	ll_num_commessa_1 = ll_num_commessa[ll_t]
	
	select flag_tipo_avanzamento
	into   :ls_flag_tipo_avanzamento
	from   anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa_1
	and    num_commessa=:ll_num_commessa_1;

	if sqlca.sqlcode < 0 then
		g_mb.error("Attenzione! Errore sul DB: " + sqlca.sqlerrtext)
		return
	end if
			
	
	if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or &
		ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
		continue
	else
		
		uo_funzioni_1 luo_funzioni_1
		
		luo_funzioni_1 = CREATE uo_funzioni_1
		
		li_risposta = luo_funzioni_1.uof_avanza_commessa(li_anno_commessa_1,ll_num_commessa_1, ls_null,ls_errore)
		
		if li_risposta < 0 then 
			li_risposta = f_scrivi_log ("Errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + ". Dettaglio errore: " + ls_errore)

			li_flag_errore = - 1
			
			if li_risposta= -1 then
				if not g_mb.confirm("Non è stato possibile scrivere il file di log. "+&
											"Questo avviso non è bloccante (riguarda esclusivamente la creazione del file log molto probabilmente manca il parametro log.), "+&
											"la produzione può continuare normalmente, vuoi proseguire?") then
					rollback;
					destroy luo_funzioni_1
					return					
				end if
			end if	
			
			destroy luo_funzioni_1
			rollback;
			continue
		end if
		
		
		commit;
		destroy luo_funzioni_1
		
	end if 
next

if li_flag_errore = 0 then
	g_mb.show("Chiusura automatica commesse avvenuta con successo")
else
	g_mb.show("Chiusura automatica commesse è stata eseguita ma qualche commessa ha avuto dei problemi. Consultare il file di log.")
end if
end event

event ue_azzera();window_open_parm ( w_tes_ord_ven_sessioni_evas_ord_ven, -1, dw_tes_ord_ven_det_1 )

/*
integer li_return
long ll_anno_registrazione, ll_num_registrazione
string ls_perc, ls_messaggio
uo_generazione_documenti luo_gen_doc

ls_perc = "%"

if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if
	
	if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_fuori_fido") = "N" and &
		dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") <> "E" and &
		dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "N" then
		
		luo_gen_doc = create uo_generazione_documenti
		li_return = luo_gen_doc.uof_azz_ass_ord_ven(ll_anno_registrazione, ll_num_registrazione, ls_perc, ls_messaggio)
		destroy luo_gen_doc
		//li_return = f_azz_ass_ord_ven(ll_anno_registrazione, ll_num_registrazione, ls_perc, ls_messaggio)
		
		if not isnull(ls_messaggio) and len(trim(ls_messaggio)) > 0 then
			g_mb.show("Azzeramento assegnazione",ls_messaggio)
		end if
		
	end if
	
	if li_return = 0 then
		g_mb.error("Azzeramento Assegnazione non avvenuta.")
		return
	end if

	g_mb.show("Azzeramento Assegnazione avvenuta con successo.")
end if
*/
end event

event ue_assegnazione();integer li_return

double  ld_quan_ordine, ld_quan_evasa, ld_quan_prodotta, ld_quan_commessa

long    ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa,&
		  ll_num_commessa, ll_errore, ll_quan
		  
string  ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_cod_deposito, ls_cod_ubicazione, ls_flag_riga_sospesa, &
		  ls_prod_quan, ls_des_prodotto,ls_cod_tipo_causa_sospensione,ls_des_tipo_causa_sospensione

longlong			ll_prog_sessione
uo_generazione_documenti luo_gen_doc

if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if
	
	if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_fuori_fido") = "N" and &
		dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") <> "E" and &
		dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "N" then

		ls_cod_deposito = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_deposito")
		ls_cod_ubicazione = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_ubicazione")
		
		
		
		//genero un progressivo sessione univoco #########################
		ll_prog_sessione = wf_get_prog_sessione()
		s_cs_xx.parametri.parametro_s_10 = string(ll_prog_sessione)
		//#################################################
		
		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select cod_tipo_det_ven, " + &
							  "prog_riga_ord_ven, " + &
							  "cod_prodotto, " + &
							  "des_prodotto, " + &
							  "quan_ordine, " + &
							  "quan_evasa, " + &
							  "flag_riga_sospesa, " + &
							  "cod_tipo_causa_sospensione, " + &
							  "anno_commessa, " + &
							  "num_commessa " + &
							  "from det_ord_ven " + &
							  "where det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_ven.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_ven.num_registrazione = " + string(ll_num_registrazione) + " and det_ord_ven.flag_blocco = 'N' and det_ord_ven.flag_evasione <> 'E' and det_ord_ven.quan_in_evasione = 0 order by det_ord_ven.prog_riga_ord_ven"

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ls_prod_quan = ""
		
		do while 0 = 0
		   fetch cu_dettagli into :ls_cod_tipo_det_ven, 
										  :ll_prog_riga_ord_ven, 
										  :ls_cod_prodotto,
										  :ls_des_prodotto,
										  :ld_quan_ordine, 
										  :ld_quan_evasa,
										  :ls_flag_riga_sospesa,
										  :ls_cod_tipo_causa_sospensione,
										  :ll_anno_commessa,
										  :ll_num_commessa;

			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di lettura per aggiornamento magazzino.")
				close cu_dettagli;
				rollback;
				return
			end if

			if sqlca.sqlcode <> 0 then exit
			
			if isnull(ls_des_prodotto) then
				ls_des_prodotto = ""
			end if
			
			if ls_flag_riga_sospesa = "S" and not isnull(ls_flag_riga_sospesa) then
				
				select	des_tipo_causa_sospensione
				into		:ls_des_tipo_causa_sospensione
				from		tab_tipi_cause_sospensione
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							cod_tipo_causa_sospensione = :ls_cod_tipo_causa_sospensione;
							
				if g_mb.confirm("La riga d'ordine "+string(ll_prog_riga_ord_ven)+" Prodotto " + ls_cod_prodotto + "risulta sospesa per la seguente causa: " + ls_des_tipo_causa_sospensione + ".~r~nSALTO LA RIGA ?") then continue
				
			end if
			
			if not isnull(ll_anno_commessa) and not isnull(ll_num_commessa) then
				select anag_commesse.quan_prodotta
				into   :ld_quan_prodotta
				from   anag_commesse
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno_commessa  and  
						 num_commessa  = :ll_num_commessa   ;
				if sqlca.sqlcode = 0 then
					ld_quan_commessa = ld_quan_prodotta		// assegno la quantità prodotta dall'ordine
				else
					ld_quan_commessa = 0							// vado avanti con la quantità dell'ordine
				end if
			else
				ld_quan_commessa = 0
			end if
			
			luo_gen_doc = create uo_generazione_documenti
			li_return =  luo_gen_doc.uof_ass_ord_ven( 	ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_tipo_det_ven, &
																	ls_cod_deposito, ls_cod_ubicazione, ls_cod_prodotto, ld_quan_ordine, ld_quan_commessa, &
																	ld_quan_evasa, ll_prog_sessione)
			destroy luo_gen_doc
			//li_return = f_ass_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_tipo_det_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_prodotto, ld_quan_ordine, ld_quan_commessa, ld_quan_evasa)
			
			choose case li_return
				case 0
					g_mb.error("Il seguente prodotto ha causato un errore:~n" + ls_cod_prodotto + " - " + ls_des_prodotto + "~n" + "Operazione interrotta")
					close cu_dettagli;
					rollback;
					return
				case 2,3
					ls_prod_quan = ls_prod_quan + ls_cod_prodotto + " - " + ls_des_prodotto + "~n"
			end choose
		loop
		close cu_dettagli;
		commit;
	end if
	
	if ls_prod_quan <> "" then
		ls_prod_quan = "I seguenti prodotti non hanno sufficiente quantità a magazzino:~n" + ls_prod_quan
	end if
	
	if ls_prod_quan <> "" then
		if ib_gen_bolla then
			ls_prod_quan = ls_prod_quan + "Si desidera continuare ugualmente?"
			if not g_mb.confirm(ls_prod_quan) then
				ib_errore_ass = true
				return
			end if
		else
			g_mb.error(ls_prod_quan)
		end if
	else
		g_mb.show("Assegnazione avvenuta con successo")
	end if
	
end if
end event

event ue_corrispondenze();long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

s_cs_xx.parametri.parametro_s_1 = "Ord_Ven"
window_open_parm(w_acq_ven_corr, -1, dw_tes_ord_ven_det_1)
end event

event ue_note();long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if


s_cs_xx.parametri.parametro_s_1 = "Ord_Ven"
window_open_parm(w_acq_ven_note, -1, dw_tes_ord_ven_det_1)

end event

event ue_sblocca();long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		 ls_flag_blocco,ls_cod_prodotto_raggruppato
dec{4} ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_quan_raggruppo

if dw_tes_ord_ven_det_1.getrow() > 0 then
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if
	
	declare cu_dettagli dynamic cursor for sqlsa;

	ls_sql_stringa = "select det_ord_ven.cod_tipo_det_ven, det_ord_ven.prog_riga_ord_ven, det_ord_ven.cod_prodotto, det_ord_ven.quan_ordine, det_ord_ven.quan_in_evasione, det_ord_ven.quan_evasa, det_ord_ven.flag_blocco from det_ord_ven where det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_ven.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_ven.num_registrazione = " + string(ll_num_registrazione)

	prepare sqlsa from :ls_sql_stringa;

	open dynamic cu_dettagli;

	ll_i = 0
	do while 0 = 0
		ll_i ++
		fetch cu_dettagli into :ls_cod_tipo_det_ven, :ll_prog_riga_ord_ven, :ls_cod_prodotto, :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_flag_blocco;
			if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di lettura per aggiornamento magazzino.")
			close cu_dettagli;
			rollback;
			return
		end if

		if sqlca.sqlcode <> 0 then exit

		select tab_tipi_det_ven.flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di lettura tipi dettagli.")
			close cu_dettagli;
			rollback;
			return
		end if

		if ls_flag_tipo_det_ven = "M" and ls_flag_blocco = 'S' and ld_quan_evasa < ld_quan_ordine then
			update anag_prodotti  
				set quan_impegnata = quan_impegnata + (:ld_quan_ordine - :ld_quan_evasa)
				where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.")
				rollback;
				close cu_dettagli;
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
					where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
		
				if sqlca.sqlcode = -1 then
					g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.")
					rollback;
					close cu_dettagli;
					return
				end if
			end if
			
		end if				
		
		update det_ord_ven
			set flag_blocco = 'N'
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
				 det_ord_ven.num_registrazione = :ll_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
						  exclamation!, ok!)
			rollback;
			close cu_dettagli;
			return
		end if

		update tes_ord_ven
		set flag_blocco = 'N'
		where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
				tes_ord_ven.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
						  exclamation!, ok!)
			rollback;
			close cu_dettagli;
			return
		end if
	loop

	commit;
	close cu_dettagli;
	dw_tes_ord_ven_det_1.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", "N")
	dw_tes_ord_ven_det_1.setitemstatus(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
end if
end event

event ue_blocca();long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		 ls_flag_blocco,ls_cod_prodotto_raggruppato
dec{4} ld_quan_ordine, ld_quan_evasa, ld_quan_in_evasione, ld_quan_raggruppo

if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if
	
	if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "N" then

		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select det_ord_ven.cod_tipo_det_ven, det_ord_ven.prog_riga_ord_ven, det_ord_ven.cod_prodotto, det_ord_ven.quan_ordine, det_ord_ven.quan_in_evasione, det_ord_ven.quan_evasa, det_ord_ven.flag_blocco from det_ord_ven where det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_ven.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_ven.num_registrazione = " + string(ll_num_registrazione)

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ll_i = 0
		do while 0 = 0
		   ll_i ++
		   fetch cu_dettagli into :ls_cod_tipo_det_ven, :ll_prog_riga_ord_ven, :ls_cod_prodotto, :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_flag_blocco;
				if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di lettura per aggiornamento magazzino.")
				close cu_dettagli;
				rollback;
		  	   return
		   end if

		   if sqlca.sqlcode <> 0 then exit

		   select tab_tipi_det_ven.flag_tipo_det_ven
		   into   :ls_flag_tipo_det_ven
		   from   tab_tipi_det_ven
		   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
		          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		   if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di lettura tipi dettagli.")
				close cu_dettagli;
				rollback;
			   return
		   end if

			if ld_quan_in_evasione <> 0 then
		      g_mb.error("Vi sono dettagli con quantità in evasione diversa da 0.")
				close cu_dettagli;
				rollback;
			   return
			end if
			
			if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine and ls_flag_blocco = "N" then
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - (:ld_quan_ordine - :ld_quan_evasa)
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode = -1 then
					g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.")
					rollback;
					close cu_dettagli;
					rollback;
					return
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")
					
					update anag_prodotti  
						set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			
					if sqlca.sqlcode = -1 then
						g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.")
						rollback;
						close cu_dettagli;
						rollback;
						return
					end if
				end if
			end if				
			
			update det_ord_ven
				set flag_blocco = 'S'
			 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
					 det_ord_ven.num_registrazione = :ll_num_registrazione and
					 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di aggiornamento dettaglio ordine.")
				rollback;
				close cu_dettagli;
				return
			end if

			update tes_ord_ven
			set flag_blocco = 'S'
			where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
					tes_ord_ven.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di aggiornamento testata ordine.")
				rollback;
				close cu_dettagli;
				return
			end if
		loop

		close cu_dettagli;
		dw_tes_ord_ven_det_1.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", "S")
		dw_tes_ord_ven_det_1.setitemstatus(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
		commit;
	end if
end if
end event

event ue_calcola();string											ls_messaggio
long											ll_num_registrazione, ll_anno_registrazione
uo_calcola_documento_euro			luo_calcola_documento_euro
uo_funzioni_1								luo_funzioni_1
integer										li_ret


if dw_tes_ord_ven_det_1.rowcount() < 1 then
	g_mb.error("Nessun ordine selezionato: calcolo bloccato")
	return
end if

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

//calcolo documento -----------------------------------------------------------------------------------------------------------
luo_calcola_documento_euro = create uo_calcola_documento_euro

//Donato 06-11-2008 metto a true per non fare di nuovo il controllo sull'esposizione cliente
luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
//-----------------
li_ret = luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"ord_ven",ls_messaggio)
destroy luo_calcola_documento_euro

if li_ret <> 0 then
	//Donato 05-11-2008 dare msg solo se c'
	if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.error("APICE",ls_messaggio)
	//g_mb.messagebox("APICE",ls_messaggio)
	rollback;
else
	commit;
end if
//---------------------------------------------------------------------------------------------------------------------------------


//Donato 22/01/2013: se il tipo ordine lo prevede, aggiorna il campo peso netto in testata ---------------------------
luo_funzioni_1 = create uo_funzioni_1
li_ret = luo_funzioni_1.uof_calcola_peso_testata_ordine(ll_anno_registrazione, ll_num_registrazione, ls_messaggio)
destroy luo_funzioni_1

if li_ret=0 then
	//la funzione ha calcolato il totale e fatto update in tes_ord_ven
	//necessita COMMIT
	commit;
	
elseif li_ret=1 then
	//la funzione non ha calcolato perchè non era previsto
	//non fare niente e vai avanti
else
	//errore, segnala
	g_mb.error("APICE", ls_messaggio)
	rollback;
end if
//-------------------------------------------------------------------------------------------------------------------------------



dw_tes_ord_ven_det_1.change_dw_current()

this.triggerevent("pc_retrieve")
end event

event ue_stampa();long			ll_righe_dett, ll_anno_reg, ll_num_reg
string			ls_cod_cliente, ls_messaggio
integer		li_ret


if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	ll_anno_reg = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
	ll_num_reg = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")
	
	if ll_anno_reg>0 and ll_num_reg>0 then
	else
		return
	end if
	
	this.triggerevent("ue_calcola")
	
	
	ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_cliente")
		
	//il controllo su blocco/warning azione lo faccio solo la prima volta
	//in quanto tutti gli ordini saranno dello stesso cliente ...
	li_ret = iuo_fido_cliente.uof_get_blocco_cliente(	ls_cod_cliente,&
																	datetime(today(), 00:00:00), "SOC", ls_messaggio)
	if li_ret=2 then
		//blocco
		g_mb.warning(ls_messaggio)
		return
	elseif li_ret=1 then
		//solo warning
		g_mb.warning(ls_messaggio)
	end if
	
	
	s_cs_xx.parametri.parametro_d_1 = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")
	
	select count(*)
	into   :ll_righe_dett
	from   det_ord_ven
	where  cod_azienda        = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione  = :s_cs_xx.parametri.parametro_d_2;
			 
	if ll_righe_dett = 0 or isnull(ll_righe_dett) then
		g_mb.error("Nessun dettaglio presente in questo ordine: impossibile stampare!")
		return
	end if
	
	window_open(w_report_ord_ven_euro, -1)
	
end if
end event

event ue_dettaglio();long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

s_cs_xx.parametri.parametro_w_ord_ven = this

window_open_parm(w_det_ord_ven_tv, -1, dw_tes_ord_ven_det_1)
end event

event ue_salda();long				ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_i, ll_row
string				ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, ls_flag_evasione, &
					ls_flag_blocco,ls_cod_prodotto_raggruppato
dec{4}			ld_quan_ordine, ld_quan_evasa, ld_quan_in_evasione, ld_quan_raggruppo, ld_quan_impegnata, ld_quan_impegnata_new


ll_row = dw_tes_ord_ven_det_1.getrow()

if ll_row > 0 then
	
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "num_registrazione")
	ls_flag_evasione = dw_tes_ord_ven_det_1.getitemstring(ll_row,"flag_evasione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
		
		if ls_flag_evasione="E" then
			g_mb.warning("Attenzione! L'ordine risulta già evaso!")
			return
			
		else
			if not g_mb.confirm("Procedere con il saldo dell'ordine?") then
				return
			end if
		end if
		
	else
		return
	end if
	
	if ls_flag_evasione <> "E" then
		
		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select det_ord_ven.cod_tipo_det_ven, det_ord_ven.prog_riga_ord_ven, det_ord_ven.cod_prodotto, det_ord_ven.quan_ordine, det_ord_ven.quan_in_evasione, det_ord_ven.quan_evasa, det_ord_ven.flag_blocco from det_ord_ven where det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_ven.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_ven.num_registrazione = " + string(ll_num_registrazione)

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ll_i = 0
		do while 0 = 0
		   ll_i ++
		   fetch cu_dettagli into :ls_cod_tipo_det_ven, :ll_prog_riga_ord_ven, :ls_cod_prodotto, :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_flag_blocco;
			   if sqlca.sqlcode = -1 then
		      g_mb.error("Si è verificato un errore in fase di lettura per aggiornamento magazzino.")
				close cu_dettagli;
				rollback;
		  	   return
		   end if

		   if sqlca.sqlcode <> 0 then exit

		   select tab_tipi_det_ven.flag_tipo_det_ven
		   into   :ls_flag_tipo_det_ven
		   from   tab_tipi_det_ven
		   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
		          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		   if sqlca.sqlcode = -1 then
		      g_mb.error("Si è verificato un errore in fase di lettura tipi dettagli.")
				close cu_dettagli;
				rollback;
			   return
		   end if

			if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine and ls_flag_blocco = "N" then
				
				//-------------------------------------------------------------------------------------
				//memorizzo in questa variabile la q ordine - q evasa
				ld_quan_impegnata_new = ld_quan_ordine - ld_quan_evasa
				
				//Prima leggo la q impegnata presente in anagrafica prodotto
				select quan_impegnata
				into :ld_quan_impegnata
				from anag_prodotti
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
						 
				if isnull(ld_quan_impegnata) then ld_quan_impegnata = 0
				
				//controllo se per caso andrà in negativo nel successivo update
				if (ld_quan_impegnata < ld_quan_impegnata_new) then
					ld_quan_impegnata_new = ld_quan_impegnata
					//cosi al + andrà a zero
				else
					//lascio ld_quan_impegnata_new a ld_quan_ordine - ld_quan_evasa
				end if
				//--------------------------------------------------------------------------------------
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_impegnata_new
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
				/*
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - (:ld_quan_ordine - :ld_quan_evasa)
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
				*/
		
				if sqlca.sqlcode < 0 then
					g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino: "+sqlca.sqlerrtext)
					rollback;
					close cu_dettagli;
					rollback;
					return
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")
					
					//-------------------------------------------------------------------------------------
					//Prima leggo la q impegnata
					select quan_impegnata
					into :ld_quan_impegnata
					from anag_prodotti
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
							 
					if isnull(ld_quan_impegnata) then ld_quan_impegnata = 0
					
					//controllo se per caso andrà in negativo
					if (ld_quan_impegnata < ld_quan_raggruppo) then
						ld_quan_raggruppo = ld_quan_impegnata
						//cosi al + andrà a zero
					else
						//lascio in ld_quan_raggruppo il suo valore
					end if
					//--------------------------------------------------------------------------------------
					
					update anag_prodotti  
						set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			
					if sqlca.sqlcode < 0 then
						g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino: "+sqlca.sqlerrtext)
						rollback;
						close cu_dettagli;
						rollback;
						return
					end if
				end if
				
			end if				
			
			update det_ord_ven
				set flag_evasione = 'E'
			 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
					 det_ord_ven.num_registrazione = :ll_num_registrazione and
					 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di aggiornamento dettaglio ordine.")
				rollback;
				close cu_dettagli;
				return
			end if

			update tes_ord_ven
			set flag_evasione = 'E'
			where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
					tes_ord_ven.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di aggiornamento testata ordine.")
				rollback;
				close cu_dettagli;
				return
			end if
		loop

		close cu_dettagli;
		dw_tes_ord_ven_det_1.setitem(ll_row, "flag_evasione", "E")
		dw_tes_ord_ven_det_1.setitemstatus(ll_row, "flag_evasione", primary!, notmodified!)
		commit;
	end if
end if
end event

event ue_aggiungi_conferma_ordine();if g_mb.confirm("Aggiungo l'ordine alla stampa della conferma?") then

	treeviewitem ltv_item
	ss_record ls_record
	string		ls_cod_cliente, ls_cod_deposito, ls_cod_porto, ls_cod_mezzo, ls_cod_resa, ls_cod_causale, &
				ls_cod_cliente1, ls_cod_deposito1, ls_cod_porto1, ls_cod_mezzo1, ls_cod_resa1, ls_cod_causale1
	long 	 	ll_i, ll_anno_ordine, ll_num_ordine
	
	tv_1.getitem(il_handle,ltv_item)
	ls_record = ltv_item.data
	
	ll_anno_ordine = long(ls_record.anno_registrazione)
	ll_num_ordine = long(ls_record.num_registrazione)
	
	ll_i = upperbound(il_anno_conferma)
	
	if ll_i < 1 or isnull(ll_i) then ll_i = 0
	
	ll_i ++
	
	// controllo congruenza ordine con altri ordini inseriti; deve essere omogeneo per Cliente - Deposito - Pagamento - Porto - Mezzo - Resa - Causale
	if ll_i > 1 then
		// leggo il primo ordine
		select cod_cliente, cod_deposito, cod_porto, cod_mezzo, cod_resa, cod_causale
		into 	:ls_cod_cliente, :ls_cod_deposito, :ls_cod_porto, :ls_cod_mezzo, :ls_cod_resa, :ls_cod_causale
		from 	tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_conferma[1] and
				 num_registrazione = :il_num_conferma[1] ;
				 
		//l'ordine che ho scelto di aggiungere
		select cod_cliente, cod_deposito, cod_porto, cod_mezzo, cod_resa, cod_causale
		into 	:ls_cod_cliente1, :ls_cod_deposito1, :ls_cod_porto1, :ls_cod_mezzo1, :ls_cod_resa1, :ls_cod_causale1
		from 	tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ordine and
				 num_registrazione = :ll_num_ordine ;
				 
		if ls_cod_cliente <> ls_cod_cliente1 or ls_cod_deposito <> ls_cod_deposito1 or ls_cod_porto <> ls_cod_porto1 or ls_cod_mezzo <> ls_cod_mezzo1 or ls_cod_resa <> ls_cod_resa1 or ls_cod_causale <> ls_cod_causale1 then
			g_mb.warning("Impossibile aggiungere l'ordine perchè il cliente, deposito, porto, mezzo, resa o causale sono diversi dagli altri ordini!")
			return
		end if
	end if
	
	il_anno_conferma[ll_i] = ll_anno_ordine
	il_num_conferma[ll_i] = ll_num_ordine

end if
end event

event ue_stampa_conferma_ordine();

return
end event

event ue_menu_azzera_spese_trasporto();/**
 * stefanop
 * 18/02/2014
 *
 * Azzero le spese di trasporto dei dettagli
 **/
 
string ls_message
int li_anno, li_ret, li_row
long ll_numero

li_row = dw_tes_ord_ven_det_1.getrow()
li_anno = dw_tes_ord_ven_det_1.getitemnumber(li_row, "anno_registrazione")
ll_numero = dw_tes_ord_ven_det_1.getitemnumber(li_row, "num_registrazione")

if g_mb.confirm("Azzero le spese di trasporto per l'ordine " + string(li_anno) + "/" + string(ll_numero) + "?") then
 
	li_ret = iuo_spese_trasporto.uof_azzera_spese_ordine(li_anno, ll_numero, "S", ref ls_message)
	
	if li_ret = 0 then
		dw_tes_ord_ven_det_1.setitem(li_row, "flag_azzera_spese_trasp", "S")
		dw_tes_ord_ven_det_1.setitemstatus(li_row, "flag_azzera_spese_trasp", primary!, NotModified!)
	elseif li_ret = 1 then
		g_mb.warning(ls_message)
	else
		g_mb.error(ls_message)
	end if
	
end if
end event

event ue_menu_abilita_spese_trasporto();/**
 * stefanop
 * 18/02/2014
 *
 * Azzero le spese di trasporto dei dettagli
 **/
 
string ls_message
int li_anno, li_ret, li_row
long ll_numero

li_row = dw_tes_ord_ven_det_1.getrow()
li_anno = dw_tes_ord_ven_det_1.getitemnumber(li_row, "anno_registrazione")
ll_numero = dw_tes_ord_ven_det_1.getitemnumber(li_row, "num_registrazione")

if g_mb.confirm("Abilito le spese di trasporto per l'ordine " + string(li_anno) + "/" + string(ll_numero) + "?") then
 
	li_ret = iuo_spese_trasporto.uof_azzera_spese_ordine(li_anno, ll_numero, "N", ref ls_message)
	
	if li_ret = 0 then
		dw_tes_ord_ven_det_1.setitem(li_row, "flag_azzera_spese_trasp", "N")
		dw_tes_ord_ven_det_1.setitemstatus(li_row, "flag_azzera_spese_trasp", primary!, NotModified!)
	elseif li_ret = 1 then
		g_mb.warning(ls_message)
	else
		g_mb.error(ls_message)
	end if
	
end if
end event

event ue_visualizza_spese_trasporto();integer		li_anno_registrazione
long			ll_num_registrazione

li_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
if li_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

window_open_parm(w_det_ord_ven_spese_trasp, -1, dw_tes_ord_ven_det_1)

return
end event

event ue_importa_excel();/**
 * stefanop
 * 11/06/2014
 *
 * Esegue importazione di un foglio excel all'interno dell'ordine.
 **/

string ls_filepath, ls_error
int li_return
long ll_anno, ll_numero

uo_importa_excel_ord_ven luo_importa

luo_importa = create uo_importa_excel_ord_ven
if luo_importa.uof_seleziona_file(ref ls_filepath) = 1 then
	
	ll_anno = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_numero = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione") 

	li_return = luo_importa.uof_importa_in_ordine(ll_anno, ll_numero, ls_filepath, ref ls_error)
	
	if  li_return= 1 then
		commit;
		g_mb.success("Importazione eseguita con successo")
	else
		rollback;
		
		if li_return = 0 then
			g_mb.warning(ls_error)
		else
			g_mb.error(ls_error)
		end if
		
	end if
	
end if

destroy luo_importa
end event

event ue_documenti_fiscali();integer			li_anno_ordine
long				ll_num_ordine

li_anno_ordine = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_ordine = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if li_anno_ordine>0 and ll_num_ordine>0 then
else
	return
end if

window_open_parm(w_doc_fiscali_ordine_vendita, -1, dw_tes_ord_ven_det_1)

return
end event

public subroutine wf_proteggi_colonne (long fl_anno_registrazione, long fl_num_registrazione, string fs_flag_evasione);if f_controlla_quan_in_evasione(fl_anno_registrazione, fl_num_registrazione) or fs_flag_evasione <> "A" then
	dw_tes_ord_ven_det_1.object.cod_cliente.protect = 1
	dw_tes_ord_ven_det_1.object.data_consegna.protect = 1
	dw_tes_ord_ven_det_1.object.cod_deposito.protect = 1
	dw_tes_ord_ven_det_1.object.cod_ubicazione.protect = 1
	dw_tes_ord_ven_det_1.object.cod_valuta.protect = 1
	dw_tes_ord_ven_det_1.object.cambio_ven.protect = 1
else
	dw_tes_ord_ven_det_1.object.cod_cliente.protect = 0
	dw_tes_ord_ven_det_1.object.data_consegna.protect = 0
	dw_tes_ord_ven_det_1.object.cod_deposito.protect = 0
	dw_tes_ord_ven_det_1.object.cod_ubicazione.protect = 0
	dw_tes_ord_ven_det_1.object.cod_valuta.protect = 0
	dw_tes_ord_ven_det_1.object.cambio_ven.protect = 0
	
end if
return
end subroutine

public subroutine wf_annulla ();string				ls_null
long				ll_null
datetime			ldt_null

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

dw_tes_ord_ven_ricerca.setitem(1,		 "anno_registrazione",		ll_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"num_registrazione",			ll_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"data_registrazione_d",		ldt_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"data_consegna_d",			ldt_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_tipo_ord_ven",			ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_valuta",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_cliente",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_prodotto",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1,		"cod_operatore",				ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"destinazione",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_deposito",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_agente_1",				ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_agente_2",				ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_evasione",				"A")
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_visualizza_offerte",		"N")
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_controllato",				"T")

dw_tes_ord_ven_ricerca.setitem(1, 		"flag_tipo_livello_1",			"N")
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_tipo_livello_2",			"N")
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_tipo_livello_3",			"N")

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_tes_ord_ven_ricerca.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'ORDVEN';
if sqlca.sqlcode = 100 then
	//g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_tes_ord_ven_ricerca.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_tes_ord_ven_ricerca.getitemdatetime(dw_tes_ord_ven_ricerca.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_tes_ord_ven_ricerca.getitemnumber(dw_tes_ord_ven_ricerca.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'ORDVEN';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'ORDVEN';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'ORDVEN');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_predefinito ();wf_leggi_filtro()
end subroutine

public subroutine wf_imposta_predefinito ();dw_tes_ord_ven_ricerca.accepttext()
wf_memorizza_filtro()
end subroutine

public function integer wf_inserisci_anni (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello anno fattura vendita
//		return -1 = errore
//		return  1 = tutto OK dati caricati
//		return  0 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_sql_livello
long      ll_risposta, ll_livello, ll_anno
ss_record lstr_record
treeviewitem ltvi_campo


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.anno_registrazione "+&
			"from tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
			
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.anno_registrazione desc "

declare cu_anni dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_anni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_anni (wf_inserisci_anni): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_anni;
	return -1
end if

do while 1=1
   fetch cu_anni into :ll_anno;
									
   if (sqlca.sqlcode = 100) then
		close cu_anni;
		if lb_dati then return 1
		return 0
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_anni (wf_inserisci_anni)~r~n" + sqlca.sqlerrtext)
		close cu_anni;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = string(ll_anno)
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "A"
	
	ltvi_campo.data = lstr_record
	ltvi_campo.label = "Anno "+string(ll_anno)

	ltvi_campo.pictureindex = 4
	ltvi_campo.selectedpictureindex = 4
	ltvi_campo.overlaypictureindex = 4
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento anno nel TREEVIEW al livello ANNO ORDINE VENDITA")
		close cu_anni;
		return -1
	end if

loop
close cu_anni;

return 1

end function

public function integer wf_leggi_parent (long fl_handle, ref string fs_sql);ss_record lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = fl_handle

if ll_item = 0 then
	return 0
end if

do
	tv_1.getitem(ll_item,ltv_item)
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello		
		case "T"
			fs_sql += " and tes_ord_ven.cod_tipo_ord_ven = '" + lstr_item.codice + "' "
		case "A"
			fs_sql += " and tes_ord_ven.anno_registrazione = " + lstr_item.codice + " "
		case "C"
			fs_sql += " and tes_ord_ven.cod_cliente = '" + lstr_item.codice + "' "	
		case "P"
			fs_sql += " and det_ord_ven.cod_prodotto = '" + lstr_item.codice + "' "	
		case "O"
			fs_sql += " and tes_ord_ven.cod_operatore = '" + lstr_item.codice + "' "	
		case "D"
			fs_sql += " and tes_ord_ven.cod_deposito = '" + lstr_item.codice + "' "	
		case "R"
			fs_sql += " and tes_ord_ven.data_registrazione = '" + lstr_item.codice + "' "	
		case "S"
			fs_sql += " and tes_ord_ven.data_consegna = '" + lstr_item.codice + "' "	
			
		case "N"
			
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public function boolean wf_liv_prodotto_is_present ();long ll_index, ll_max_index
string ls_flag_tipo_livello

ll_max_index = 3

for ll_index = 1 to ll_max_index
	ls_flag_tipo_livello = ""
	ls_flag_tipo_livello = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_tipo_livello_"+string(ll_index))
	
	if ls_flag_tipo_livello= "P" then
		//è stato specificato uno dei livelli come Prodotto
		return true
	end if
	
next

return false
end function

public function integer wf_inserisci_clienti (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello clienti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_cliente, ls_des_cliente, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.cod_cliente, anag_clienti.rag_soc_1 "+&
			"from tes_ord_ven "+&
			"join anag_clienti on anag_clienti.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"anag_clienti.cod_cliente=tes_ord_ven.cod_cliente "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by anag_clienti.rag_soc_1 asc "

declare cu_clienti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_clienti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_clienti (wf_inserisci_clienti): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_clienti;
	return 0
end if

do while 1=1
   fetch cu_clienti into :ls_cod_cliente, 
							:ls_des_cliente;
									
   if (sqlca.sqlcode = 100) then
		close cu_clienti;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_clienti (wf_inserisci_clienti)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_clienti;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_cliente
	lstr_record.descrizione = ls_des_cliente
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "C"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_cliente + " - " + ls_des_cliente

	ltvi_campo.pictureindex = 1
	ltvi_campo.selectedpictureindex = 1
	ltvi_campo.overlaypictureindex = 1
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento cliente nel TREEVIEW al livello CLIENTI")
		setpointer(Arrow!)
		close cu_clienti;
		return 0
	end if

loop
close cu_clienti;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_data_consegna (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello data consegna ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_sql_livello, ls_data_consegna
long      ll_risposta, ll_livello
datetime ldt_data_consegna
ss_record lstr_record
treeviewitem ltvi_campo


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.data_consegna "+&
			"from tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.data_consegna desc "

declare cu_date dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_date;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_date (wf_inserisci_data_consegna): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_date;
	return 0
end if

do while 1=1
   fetch cu_date into :ldt_data_consegna;
									
   if (sqlca.sqlcode = 100) then
		close cu_date;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_date (wf_inserisci_data_consegna)~r~n" + sqlca.sqlerrtext)
		close cu_date;
		return -1
	end if
	
	ls_data_consegna = string(ldt_data_consegna, s_cs_xx.db_funzioni.formato_data)
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_data_consegna
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "S"
	
	ltvi_campo.data = lstr_record
	ltvi_campo.label = "Data Cons. "+string(ldt_data_consegna, "dd/mm/yyyy")

	ltvi_campo.pictureindex = 6
	ltvi_campo.selectedpictureindex = 6
	ltvi_campo.overlaypictureindex = 6
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.error("Errore","Errore in inserimento data consegna nel TREEVIEW al livello DATA CONS. ORDINE VENDITA")
		close cu_date;
		return 0
	end if

loop
close cu_date;

return 0

end function

public function integer wf_inserisci_data_registrazione (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello data registrazione ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_sql_livello, ls_data_registrazione
long      ll_risposta, ll_livello
datetime ldt_data_registrazione
ss_record lstr_record
treeviewitem ltvi_campo


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.data_registrazione "+&
			"from tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
			
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.data_registrazione desc "

declare cu_date dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_date;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_date (wf_inserisci_data_registrazione): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_date;
	return 0
end if

do while 1=1
   fetch cu_date into :ldt_data_registrazione;
									
   if (sqlca.sqlcode = 100) then
		close cu_date;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_date (wf_inserisci_data_registrazione)~r~n" + sqlca.sqlerrtext)
		close cu_date;
		return -1
	end if
	
	ls_data_registrazione = string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data)
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_data_registrazione
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "R"
	
	ltvi_campo.data = lstr_record
	ltvi_campo.label = "Data Reg. "+string(ldt_data_registrazione, "dd/mm/yyyy")

	ltvi_campo.pictureindex = 5
	ltvi_campo.selectedpictureindex = 5
	ltvi_campo.overlaypictureindex = 5
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.error("Errore","Errore in inserimento data registrazione nel TREEVIEW al livello DATA REG. ORDINE VENDITA")
		close cu_date;
		return 0
	end if

loop
close cu_date;

return 0

end function

public function integer wf_inserisci_depositi (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello depositi
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_deposito, ls_des_deposito, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.cod_deposito, anag_depositi.des_deposito "+&
			"from tes_ord_ven "+&
			"join anag_depositi on anag_depositi.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"anag_depositi.cod_deposito=tes_ord_ven.cod_deposito "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "


if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.cod_deposito asc "

declare cu_depositi dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_depositi;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_depositi (wf_inserisci_depositi): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_depositi;
	return 0
end if

do while 1=1
   fetch cu_depositi into :ls_cod_deposito, 
							:ls_des_deposito;
									
   if (sqlca.sqlcode = 100) then
		close cu_depositi;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_depositi (wf_inserisci_depositi)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_depositi;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_deposito
	lstr_record.descrizione = ls_des_deposito
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "D"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_deposito + " - " + ls_des_deposito

	ltvi_campo.pictureindex = 3
	ltvi_campo.selectedpictureindex = 3
	ltvi_campo.overlaypictureindex = 3
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento deposito nel TREEVIEW al livello Depositi")
		setpointer(Arrow!)
		close cu_depositi;
		return 0
	end if

loop
close cu_depositi;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_operatori (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello operatori
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_operatore, ls_des_operatore, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.cod_operatore, tab_operatori.des_operatore "+&
			"from tes_ord_ven "+&
			"join tab_operatori on tab_operatori.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"tab_operatori.cod_operatore=tes_ord_ven.cod_operatore "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
										
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tab_operatori.des_operatore asc "

declare cu_operatori dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_operatori;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_operatori (wf_inserisci_operatori): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_operatori;
	return 0
end if

do while 1=1
   fetch cu_operatori into :ls_cod_operatore, 
							:ls_des_operatore;
									
   if (sqlca.sqlcode = 100) then
		close cu_operatori;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_operatori (wf_inserisci_operatori)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_operatori;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_operatore
	lstr_record.descrizione = ls_des_operatore
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "O"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_operatore + " - " + ls_des_operatore

	ltvi_campo.pictureindex = 2
	ltvi_campo.selectedpictureindex = 2
	ltvi_campo.overlaypictureindex = 2
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento operatore nel TREEVIEW al livello Operatori")
		setpointer(Arrow!)
		close cu_operatori;
		return 0
	end if

loop
close cu_operatori;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_ordini (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento ordini sul tree
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

ss_record		lstr_record
treeviewitem	ltvi_campo
long				ll_livello, ll_count, ll_max_count, ll_anno_registrazione, ll_num_registrazione, ll_risposta, ll_index, ll_ret_offerte
boolean			ib_max, lb_dati = false
string				ls_sql, ls_sql_livello, ls_flag_visualizza_offerte, ls_flag_evasione, ls_cod_cliente, ls_rag_soc_1
dec{4} 			ld_tot_val_ordine

ll_livello = il_livello_corrente
ll_count = 0
ll_max_count = 500
ib_max = false
ls_flag_visualizza_offerte = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_visualizza_offerte")

setpointer(Hourglass!)

declare cu_items dynamic cursor for sqlsa;
ls_sql = "select distinct 	 tes_ord_ven.anno_registrazione," +&
         						"tes_ord_ven.num_registrazione,"+&
								"tes_ord_ven.flag_evasione,"+&
								"tes_ord_ven.cod_cliente,"+&
								"tes_ord_ven.tot_val_ordine "+&
			"from   tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
			
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql +=ls_sql_livello
end if

ls_sql = ls_sql + " order by tes_ord_ven.anno_registrazione, tes_ord_ven.num_registrazione "

prepare sqlsa from :ls_sql;
open dynamic cu_items;

if sqlca.sqlcode <> 0 then
	g_mb.error("OMNIA","Errore in OPEN cursore cu_items (wf_inserisci_ordini)~r~n" + sqlca.sqlerrtext)
	close cu_items;
	setpointer(Arrow!)
	return -1
end if

do while true
	
	fetch cu_items into :ll_anno_registrazione, 
	                            :ll_num_registrazione,
							 :ls_flag_evasione,
							 :ls_cod_cliente,
							 :ld_tot_val_ordine;

   if (sqlca.sqlcode = 100) then
		close cu_items;
		setpointer(Arrow!)
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_items (wf_inserisci_ordini)~r~n" + sqlca.sqlerrtext)
		close cu_items;
		setpointer(Arrow!)
		return -1
	end if
	
	ll_count += 1
	lb_dati = true
		
	lstr_record.anno_registrazione = ll_anno_registrazione	
	lstr_record.num_registrazione = ll_num_registrazione
	lstr_record.codice = ""		
	lstr_record.descrizione = ""
	lstr_record.livello = 100
	lstr_record.tipo_livello = "X"	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = string(ll_anno_registrazione) + "/"+ string(ll_num_registrazione)
	
	if isnull(ld_tot_val_ordine) then ld_tot_val_ordine=0
	ltvi_campo.label += " Val. "+string(ld_tot_val_ordine, "###,###,##0.00")
	
	if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
		
		select rag_soc_1
		into :ls_rag_soc_1
		from anag_clienti
		where		cod_azienda=:s_cs_xx.cod_azienda and
					cod_cliente=:ls_cod_cliente;
		
		ltvi_campo.label += " - "+ls_rag_soc_1+" ("+ls_cod_cliente+")"
		
	end if
	
	
	choose case ls_flag_evasione
		case "E"
			ll_index = 10
		case "P"
			ll_index = 9
		case else
			ll_index = 8
	end choose
	
	ltvi_campo.pictureindex = ll_index
	ltvi_campo.selectedpictureindex = ll_index
	ltvi_campo.overlaypictureindex = ll_index
	ltvi_campo.selected = false
	ltvi_campo.children = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)

	
	if ll_count > 500 and not ib_max then
		ib_max = true
		if g_mb.messagebox("APICE","Attenzione troppi dati caricati nell'albero degli ordini. Continuare?" + & 
					  "(Si consiglia di caricare annullare scegliendo NO, impostare qualche altro elemento nel filtro e riprovare.)",Question!, YesNo!,2)=2 then
			close cu_items;
			setpointer(Arrow!)
			return 0
		end if
	end if
loop

close cu_items;
setpointer(Arrow!)
return 0
end function

public function integer wf_inserisci_prodotti (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello prodotti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_prodotto, ls_des_prodotto, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct det_ord_ven.cod_prodotto, anag_prodotti.des_prodotto "+&
			"from tes_ord_ven "

//metto sempre la join con la det_ord_ven
ls_sql += is_join_det_ord_ven
			
ls_sql += "join anag_prodotti on anag_prodotti.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"anag_prodotti.cod_prodotto=det_ord_ven.cod_prodotto "

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			
ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by det_ord_ven.cod_prodotto asc "

declare cu_prodotti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_prodotti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_prodotti (wf_inserisci_prodotti): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_prodotti;
	return 0
end if

do while 1=1
   fetch cu_prodotti into :ls_cod_prodotto, 
							:ls_des_prodotto;
									
   if (sqlca.sqlcode = 100) then
		close cu_prodotti;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_prodotti (wf_inserisci_prodotti)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_prodotti;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_prodotto
	lstr_record.descrizione = ls_des_prodotto
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "P"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_prodotto + " - " + ls_des_prodotto

	ltvi_campo.pictureindex = 12
	ltvi_campo.selectedpictureindex = 12
	ltvi_campo.overlaypictureindex = 12
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento prodotti nel TREEVIEW al livello Prodotti")
		setpointer(Arrow!)
		close cu_prodotti;
		return 0
	end if

loop
close cu_prodotti;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_singolo_ordine (long fl_handle, long fl_anno, long fl_numero);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento singolo ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

treeviewitem	ltvi_campo
string				ls_temp, ls_flag_visualizza_offerte, ls_flag_evasione, ls_cod_cliente, ls_rag_soc_1
ss_record		lstr_record
long				ll_risposta, ll_index
dec{4}			ld_tot_val_ordine

ls_flag_visualizza_offerte = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_visualizza_offerte")

select 	cod_azienda, flag_evasione, tot_val_ordine, cod_cliente
into   		:ls_temp, :ls_flag_evasione, :ld_tot_val_ordine, :ls_cod_cliente
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno and
		 num_registrazione = :fl_numero;

if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","Ordine inesistente!")
	return 0
end if

lstr_record.anno_registrazione = fl_anno
lstr_record.num_registrazione = fl_numero
lstr_record.codice = ""		
lstr_record.descrizione = ""
lstr_record.livello = 100
lstr_record.tipo_livello = "X"

ltvi_campo.label = 	string(fl_anno) + "/"+ string(fl_numero)

if isnull(ld_tot_val_ordine) then ld_tot_val_ordine=0
ltvi_campo.label += " Val. "+string(ld_tot_val_ordine, "###,###,##0.00")

if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
		
	select rag_soc_1
	into :ls_rag_soc_1
	from anag_clienti
	where		cod_azienda=:s_cs_xx.cod_azienda and
				cod_cliente=:ls_cod_cliente;
	
	ltvi_campo.label += " - "+ls_rag_soc_1+" ("+ls_cod_cliente+")"
	
end if

choose case ls_flag_evasione
	case "E"
		ll_index = 10
	case "P"
		ll_index = 9
	case else
		ll_index = 8
end choose

ltvi_campo.pictureindex = ll_index
ltvi_campo.selectedpictureindex = ll_index
ltvi_campo.overlaypictureindex = ll_index
ltvi_campo.data = lstr_record
ltvi_campo.selected = false
ltvi_campo.children = false

ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)

return 0
end function

public function integer wf_inserisci_tipi_ordini (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello tipi ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean					lb_dati = false
string						ls_sql, ls_cod_tipo_ord_ven, ls_des_tipo_ord_ven, ls_sql_livello
ss_record				lstr_record
treeviewitem			ltvi_campo
long						ll_livello,ll_risposta

ll_livello = il_livello_corrente + 1

ls_sql =  "select distinct tes_ord_ven.cod_tipo_ord_ven, tab_tipi_ord_ven.des_tipo_ord_ven "+&
			"from tes_ord_ven "+&
			"join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"tab_tipi_ord_ven.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
										
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.cod_tipo_ord_ven asc "

declare cu_tipi_ord_ven dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_tipi_ord_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_tipi_ord_ven: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_tipi_ord_ven;
	return 0
end if

do while 1=1
   fetch cu_tipi_ord_ven into 	:ls_cod_tipo_ord_ven, 
								   		:ls_des_tipo_ord_ven;
									
   if (sqlca.sqlcode = 100) then
		close cu_tipi_ord_ven;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_tipi_ord_ven (wf_inserisci_tipi_ordini)~r~n" + sqlca.sqlerrtext)
		close cu_tipi_ord_ven;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_tipo_ord_ven
	lstr_record.descrizione = ls_des_tipo_ord_ven
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "T"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_tipo_ord_ven + " -  " +  ls_des_tipo_ord_ven

	ltvi_campo.pictureindex = 7
	ltvi_campo.selectedpictureindex = 7
	ltvi_campo.overlaypictureindex = 7
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento tipo ordine vendita nel TREEVIEW al livello TIPO ORDINE VENDITA")
		close cu_tipi_ord_ven;
		return 0
	end if

loop
close cu_tipi_ord_ven;

ll_risposta = tv_1.FindItem(RootTreeItem! , 0)
if ll_risposta > 0 then tv_1.ExpandItem(ll_risposta)

return 0

end function

public subroutine wf_ricerca ();long			ll_anno_registrazione, ll_num_registrazione
datetime		ldt_data_registrazione, ldt_data_consegna
string			ls_tipo_ordine, ls_cod_valuta, ls_cod_cliente, ls_cod_operatore, ls_destinazione, ls_flag_controllato, &
				ls_cod_deposito, ls_cod_agente_1, ls_cod_agente_2, ls_flag_evasione, ls_cod_prodotto, ls_flag_blocco

dw_tes_ord_ven_ricerca.accepttext()

ll_anno_registrazione 				= dw_tes_ord_ven_ricerca.getitemnumber(1,		 "anno_registrazione")
ll_num_registrazione 					= dw_tes_ord_ven_ricerca.getitemnumber(1, 		"num_registrazione")
ldt_data_registrazione 				= dw_tes_ord_ven_ricerca.getitemdatetime(1, 	"data_registrazione_d")
ldt_data_consegna 					= dw_tes_ord_ven_ricerca.getitemdatetime(1, 	"data_consegna_d")
ls_tipo_ordine							= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_tipo_ord_ven")
ls_cod_valuta							= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_valuta")
ls_cod_cliente							= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_cliente")
ls_cod_prodotto						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_prodotto")
ls_cod_operatore						= dw_tes_ord_ven_ricerca.getitemstring(1,			"cod_operatore")
ls_destinazione							= dw_tes_ord_ven_ricerca.getitemstring(1, 		"destinazione")
ls_cod_deposito						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_deposito")
ls_cod_agente_1						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_agente_1")
ls_cod_agente_2						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_agente_2")
ls_flag_evasione						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"flag_evasione")
ls_flag_controllato						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"flag_controllato")
ls_flag_blocco = dw_tes_ord_ven_ricerca.getitemstring(1, "flag_blocco")
// -------------  compongo SQL con filtri ---------------------
is_sql_filtro = ""

if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 then
	is_sql_filtro += " and tes_ord_ven.anno_registrazione = " + string(ll_anno_registrazione) + " "
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	is_sql_filtro += " and tes_ord_ven.num_registrazione = " + string(ll_num_registrazione) + " "
end if
if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_cliente = '" + ls_cod_cliente + "' "
end if
if not isnull(ls_tipo_ordine) and ls_tipo_ordine<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_tipo_ord_ven = '" + ls_tipo_ordine + "' "
end if
if not isnull(ls_cod_valuta) and ls_cod_valuta<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_valuta = '" + ls_cod_valuta + "' "
end if
if not isnull(ldt_data_registrazione) then
	is_sql_filtro += " and tes_ord_ven.data_registrazione = '" + string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_consegna) then
	is_sql_filtro += " and tes_ord_ven.data_consegna = '" + string(ldt_data_consegna, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ls_cod_operatore) and ls_cod_operatore<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_operatore = '" + ls_cod_operatore + "' "
end if
if not isnull(ls_destinazione) and ls_destinazione<>"" then
	is_sql_filtro += " and tes_ord_ven.rag_soc_1 like '" + ls_destinazione + "' "
end if
if not isnull(ls_cod_deposito) and ls_cod_deposito<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_deposito = '" + ls_cod_deposito + "' "
end if
if not isnull(ls_cod_agente_1) and ls_cod_agente_1<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_agente_1 = '" + ls_cod_agente_1 + "' "
end if
if not isnull(ls_cod_agente_2) and ls_cod_agente_2<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_agente_2 = '" + ls_cod_agente_2 + "' "
end if
if not isnull(ls_flag_blocco) and ls_flag_blocco <> "X" then
	is_sql_filtro += " and tes_ord_ven.flag_blocco='" + ls_flag_blocco + "' "
end if

choose case ls_flag_evasione
	case "A"
		is_sql_filtro += " and tes_ord_ven.flag_evasione = 'A' "
	case "P"
		is_sql_filtro += " and tes_ord_ven.flag_evasione = 'P' "
	case "E"
		is_sql_filtro += " and tes_ord_ven.flag_evasione = 'E' "
	case "R"
		is_sql_filtro += " and (tes_ord_ven.flag_evasione = 'A' or tes_ord_ven.flag_evasione = 'P') "
end choose

if ls_flag_controllato="S" or ls_flag_controllato="N" then
	is_sql_filtro += " and tes_ord_ven.flag_controllato = '"+ls_flag_controllato+"' "
end if

if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
	//NOTA: mettere nella select la join con la det_ord_ven
	is_sql_filtro += " and det_ord_ven.cod_prodotto = '" + ls_cod_prodotto + "' "
end if

// ------------------------------------------------------------

wf_cancella_treeview()
wf_imposta_treeview()

tv_1.setfocus()

dw_folder_tv.fu_SelectTab(2)
dw_folder.fu_SelectTab(1)

dw_tes_ord_ven_det_1.change_dw_current()
end subroutine

public subroutine wf_cancella_treeview ();ib_tree_deleting = true
tv_1.deleteitem(0)
ib_tree_deleting = false
end subroutine

public subroutine wf_imposta_treeview ();string ls_null, ls_cod_attrezzatura, ls_des_attrezzatura
long ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione
ss_record lstr_record
treeviewitem ltvi_campo

setnull(ls_null)

ltvi_campo.expanded = false
ltvi_campo.selected = false
ltvi_campo.children = false	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ll_i = 0
setpointer(HourGlass!)

tv_1.setredraw(false)

ll_anno_registrazione = dw_tes_ord_ven_ricerca.getitemnumber(1,"anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_ricerca.getitemnumber(1,"num_registrazione")

// ----------------- se è stata richiesta una specifica registrazione propongo solo quella e niente altro------------------
if ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) and &
		ll_num_registrazione > 0 and not isnull(ll_num_registrazione) then
		
	ib_retrieve=true
	wf_inserisci_singolo_ordine(0, ll_anno_registrazione, ll_num_registrazione )
	tv_1.setredraw(true)
	return
end if

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_tes_ord_ven_ricerca.getitemstring(1,"flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "T"
		//caricamento tipo ordine
		wf_inserisci_tipi_ordini(0)
		
	case "A"
		//caricamento anni
		wf_inserisci_anni(0)

	case "C"
		//caricamento clienti
		wf_inserisci_clienti(0)
		
	case "O"
		//caricamento operatori
		wf_inserisci_operatori(0)
		
	case "D"
		//caricamento depositi
		wf_inserisci_depositi(0)
		
	case "P"
		//caricamento prodotti
		wf_inserisci_prodotti(0)
		
	case "R" 
		//caricamento date registrazioni
		wf_inserisci_data_registrazione(0)
		
	case "S" 
		//caricamento date consegna
		wf_inserisci_data_consegna(0)
		
	case "N"
		wf_inserisci_ordini(0)

end choose

tv_1.setredraw(true)

setpointer(arrow!)
return
end subroutine

public subroutine wf_imposta_immagini_tv ();string ls_path

//datawindow offerte vendita ##############################################
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "\treeview\documento_grigio.png"
dw_tes_off_ven.object.p_tes_off_ven.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "\treeview\prodotto.png"
dw_tes_off_ven.object.p_det_off_ven.FileName = ls_path


//treeview ordini vendita #################################################
//immagini tree
tv_1.deletepictures()

//1 cliente
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\cliente.png")

//2 operatore
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\operatore.png")

//3 deposito
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\deposito.png")

//4 anno registrazione
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\anno.png")

//5 data registrazione
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\data_1.png")

//6 data consegna
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\data_2.png")

//7 tipo ordine
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\tipo_documento.png")

//8,9,10 ordine aperto, parziale, evaso
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_verde.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_giallo.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_rosso.png")

//11 offerte di vendita di un item ordine
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_grigio.png")

//12 prodotto
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\prodotto.png")


////5 fattura eliminata
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\man_reg_eliminata.png")
//
////6 fattura selezionata
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\man_reg_modificata.png")
end subroutine

public function integer wf_apri_offerta (long fl_anno_offerta, long fl_num_offerta);long 	 ll_righe_dett

s_cs_xx.parametri.parametro_d_1 = fl_anno_offerta
s_cs_xx.parametri.parametro_d_2 = fl_num_offerta

select count(*)
into   :ll_righe_dett
from   det_off_ven
where  cod_azienda        = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_offerta and
		 num_registrazione  = :fl_num_offerta;

if ll_righe_dett = 0 or isnull(ll_righe_dett) then
	g_mb.show("Nessun dettaglio presente in questa offerta (oppure l'offerta è inesistente): impossibile stampare!")
	return 0
end if

window_open(w_report_off_ven_euro,-1)

return 1
end function

public function integer wf_retrieve_offerte_vendita (long fl_anno_reg_ordine, long fl_num_reg_ordine);long ll_tot
string ls_flag_visualizza_offerte

ls_flag_visualizza_offerte = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_visualizza_offerte")

if ls_flag_visualizza_offerte<> "S" then return 0

ll_tot = dw_tes_off_ven.retrieve(s_cs_xx.cod_azienda, fl_anno_reg_ordine, fl_num_reg_ordine)

return ll_tot
end function

public function longlong wf_get_prog_sessione ();longlong			ll_id
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")
ll_id = longlong(ls_temp)

return ll_id
end function

public function integer wf_apri_offerta (integer ai_anno_offerta, long al_num_offerta);long 	 ll_righe_dett

s_cs_xx.parametri.parametro_d_1 = ai_anno_offerta
s_cs_xx.parametri.parametro_d_2 = al_num_offerta

select count(*)
into   :ll_righe_dett
from   det_off_ven
where  cod_azienda        = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ai_anno_offerta and
		 num_registrazione  = :al_num_offerta;

if ll_righe_dett = 0 or isnull(ll_righe_dett) then
	g_mb.messagebox("Stampa Offerta Vendita","Nessun dettaglio presente in questa offerta: impossibile stampare!",StopSign!)
	return 0
end if

window_open(w_report_off_ven_euro,-1)

return 1
end function

public function integer wf_elabora_excel (string as_file_name);/**
 * stefanop 
 * 11/06/2014
 *
 * Elaboro il file excel.
 * Return: 1 tutto ok, -1 errore
 **/
 
any la_value
string ls_value
int li_riga
uo_excel luo_excel

// Recuper informazioni di configurazione
select num_riga_inizio
into :li_riga
from con_ord_ven_import
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la lettura della configurazione per l'importazione del file excel.", sqlca)
	return -1
elseif sqlca.sqlcode = 100 then
	g_mb.error("Impossibile procere con l'importazione perchè non è configurata la tabella con_ord_ven_import")
	return -1
end if

luo_excel = create uo_excel
luo_excel.uof_open(as_file_name, false, true)

do while true
	
	la_value = luo_excel.uof_read(li_riga, "A")
	if isnull(la_value) then
		// End loop
		exit
	end if
	
	
	
	li_riga++
loop


destroy uo_excel
 
return 1
end function

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_tipo_ord_ven", &
                 sqlca, &
                 "tab_tipi_ord_ven", &
                 "cod_tipo_ord_ven", &
                 "des_tipo_ord_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_causale", &
                 sqlca, &
                 "tab_causali_trasp", &
                 "cod_causale", &
                 "des_causale", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_giro_consegna", &
                 sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
	"cod_natura_intra", &
	sqlca, &
	"tab_natura_intra", &
	"cod_natura_intra", &
	"descrizione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	

					  
// dw_tes_ord_ven_ricerca ---------------------------------------------------
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_tipo_ord_ven", &
				sqlca, &
                  "tab_tipi_ord_ven", &
			    "cod_tipo_ord_ven", &
				"des_tipo_ord_ven", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_agente_1", &
				sqlca, &
                  "anag_agenti", &
				"cod_agente", &
				"rag_soc_1", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_agente_2", &
				sqlca, &
                  "anag_agenti", &
				"cod_agente", &
				"rag_soc_1", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_valuta", &
				sqlca, &
				"tab_valute", &
                  "cod_valuta", &
				"des_valuta", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_operatore", &
				sqlca, &
				"tab_operatori", &
                 	"cod_operatore", &
				"des_operatore", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )

				
					  
end event

event pc_setwindow;call super::pc_setwindow;//unsignedlong lul_modalita
string ls_flag_aut_pagamenti
int li_count
windowobject lw_oggetti[],  lw_vuoto[]
s_open_parm	lstr_parm


lstr_parm = message.powerobjectparm

iuo_spese_trasporto = create uo_spese_trasporto
iuo_fido_cliente = create uo_fido_cliente


dw_tes_ord_ven_det_1.set_dw_key("cod_azienda")

dw_tes_ord_ven_det_1.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen, &
                                    c_default)

dw_tes_ord_ven_det_2.set_dw_options(sqlca, &
                                    dw_tes_ord_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_tes_ord_ven_det_3.set_dw_options(sqlca, &
                                    dw_tes_ord_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_tes_ord_ven_det_4.set_dw_options(sqlca, &
                                    dw_tes_ord_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

dw_tes_ord_ven_ricerca.insertrow(0)

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tes_ord_ven_det_2
lw_oggetti[2] = dw_tes_off_ven
dw_folder.fu_assigntab(2, "Cliente/Destinazione", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tes_ord_ven_det_4
lw_oggetti[2] = dw_tes_off_ven
dw_folder.fu_assigntab(4, "Note", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tes_ord_ven_det_3
lw_oggetti[2] = dw_tes_off_ven
dw_folder.fu_assigntab(3, "Trasporto/Totali", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tes_ord_ven_det_1
lw_oggetti[2] = dw_tes_off_ven
dw_folder.fu_assigntab(1, "Testata", lw_oggetti[])

dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_tes_ord_ven_det_1
ib_prima_riga = false

dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = false
cb_duplica_doc.enabled = false

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tes_ord_ven_ricerca
dw_folder_tv.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = tv_1
dw_folder_tv.fu_assigntab(2, "Ordini", lw_oggetti[])

dw_folder_tv.fu_foldercreate(2, 2)
dw_folder_tv.fu_selecttab(1)

dw_tes_ord_ven_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())
dw_tes_ord_ven_ricerca.setitem(1, "flag_evasione", "A")

wf_imposta_immagini_tv()

//Donato 19-11-2008 gestione fidi PTENDA -----------------------------
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ii_protect_autorizza_sblocco = 0 //cioè not protect
else
	select flag_pagamenti
	into :ls_flag_aut_pagamenti
	from utenti
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_utente = :s_cs_xx.cod_utente;
		
	if sqlca.sqlcode <> 0  then
		g_mb.messagebox("APICE","Errore durante la lettura del flag autorizza sblocco dalla tabella utenti!",StopSign!)	
	else
		if ls_flag_aut_pagamenti = "S" then ii_protect_autorizza_sblocco = 0 //cioè not protect
	end if
end if
//------------------------------------------------------------------------------------

// stefanop: 10/06/2014: controllo impostazione importazione da excel
select count(*)
into :li_count
from parametri_import
where cod_azienda=:s_cs_xx.cod_azienda and
		 cod_tipo_gestione = "ORDVEN";

ib_import_configured = sqlca.sqlcode = 0 and li_count > 0
// ----

//wf_predefinito()
guo_functions.uof_leggi_filtro(dw_tes_ord_ven_ricerca, "ORDVEN")

dw_tes_off_ven.settransobject(sqlca)

if isvalid(lstr_parm) and not isnull(lstr_parm) then
	if lstr_parm.anno_registrazione > 0 then
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(), "anno_registrazione", lstr_parm.anno_registrazione)
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(), "num_registrazione", lstr_parm.num_registrazione)
		wf_ricerca()
	end if
end if

// stefanop: 29/06/2016: imposto le icone dei filtri
dw_tes_ord_ven_ricerca.object.b_leggi_filtro.filename = s_cs_xx.volume + s_cs_xx.risorse + "treeview\filter.png"
dw_tes_ord_ven_ricerca.object.b_memorizza_filtro.filename = s_cs_xx.volume + s_cs_xx.risorse + "treeview\filter-add.png"
dw_tes_ord_ven_ricerca.object.b_cancella_filtro.filename = s_cs_xx.volume + s_cs_xx.risorse + "treeview\filter-delete.png"
dw_tes_ord_ven_ricerca.object.b_leggi_filtro.OriginalSize = false
dw_tes_ord_ven_ricerca.object.b_leggi_filtro.width = 73
dw_tes_ord_ven_ricerca.object.b_leggi_filtro.height = 64
dw_tes_ord_ven_ricerca.object.b_memorizza_filtro.OriginalSize = false
dw_tes_ord_ven_ricerca.object.b_memorizza_filtro.width = 73
dw_tes_ord_ven_ricerca.object.b_memorizza_filtro.height = 64
dw_tes_ord_ven_ricerca.object.b_cancella_filtro.OriginalSize = false
dw_tes_ord_ven_ricerca.object.b_cancella_filtro.width = 73
dw_tes_ord_ven_ricerca.object.b_cancella_filtro.height = 64

end event

event pc_delete;call super::pc_delete;string ls_tabella
long ll_anno_registrazione, ll_num_registrazione, ll_num_pl
integer li_i


for li_i = 1 to dw_tes_ord_ven_det_1.deletedcount()
	if dw_tes_ord_ven_det_1.getitemstring(li_i, "flag_evasione", delete!, true) <> "A" then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! Ha già subito un'evasione.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if

	if dw_tes_ord_ven_det_1.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! E' Bloccato.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if

	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(li_i, "anno_registrazione", delete!, true)
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(li_i, "num_registrazione", delete!, true)
	if f_controlla_quan_in_evasione(ll_anno_registrazione, ll_num_registrazione) then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! Vi sono dettagli con quantità in evasione.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
	
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(li_i, "anno_registrazione", delete!, true)
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(li_i, "num_registrazione", delete!, true)
	
	select count(*)
	into   :ll_num_pl
	from   tes_ord_ven_pack_list
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in controllo packing list collegati: " + sqlca.sqlerrtext)
	elseif sqlca.sqlcode = 0 and ll_num_pl > 0 then
		g_mb.messagebox("Attenzione", "Ordine non cancellabile! Vi sono packing list collegati.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if

	ls_tabella = "det_ord_ven"
	if f_controlla_coll_commesse(ls_tabella, ll_anno_registrazione, ll_num_registrazione) then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! Vi sono dettagli collegati con le commesse.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
next


dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = false

//cb_aspetto.enabled = false



end event

on w_tes_ord_ven_tv.create
int iCurrent
call super::create
this.dw_tes_off_ven=create dw_tes_off_ven
this.dw_folder_tv=create dw_folder_tv
this.dw_tes_ord_ven_det_3=create dw_tes_ord_ven_det_3
this.dw_tes_ord_ven_ricerca=create dw_tes_ord_ven_ricerca
this.cb_duplica_doc=create cb_duplica_doc
this.tv_1=create tv_1
this.dw_tes_ord_ven_det_1=create dw_tes_ord_ven_det_1
this.dw_tes_ord_ven_det_2=create dw_tes_ord_ven_det_2
this.dw_tes_ord_ven_det_4=create dw_tes_ord_ven_det_4
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_off_ven
this.Control[iCurrent+2]=this.dw_folder_tv
this.Control[iCurrent+3]=this.dw_tes_ord_ven_det_3
this.Control[iCurrent+4]=this.dw_tes_ord_ven_ricerca
this.Control[iCurrent+5]=this.cb_duplica_doc
this.Control[iCurrent+6]=this.tv_1
this.Control[iCurrent+7]=this.dw_tes_ord_ven_det_1
this.Control[iCurrent+8]=this.dw_tes_ord_ven_det_2
this.Control[iCurrent+9]=this.dw_tes_ord_ven_det_4
this.Control[iCurrent+10]=this.dw_folder
end on

on w_tes_ord_ven_tv.destroy
call super::destroy
destroy(this.dw_tes_off_ven)
destroy(this.dw_folder_tv)
destroy(this.dw_tes_ord_ven_det_3)
destroy(this.dw_tes_ord_ven_ricerca)
destroy(this.cb_duplica_doc)
destroy(this.tv_1)
destroy(this.dw_tes_ord_ven_det_1)
destroy(this.dw_tes_ord_ven_det_2)
destroy(this.dw_tes_ord_ven_det_4)
destroy(this.dw_folder)
end on

event pc_modify;call super::pc_modify;string ls_tabella
long ll_anno_registrazione, ll_num_registrazione 


if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! E' Bloccato.", &
              exclamation!, ok!)
   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_close;call super::pc_close;

destroy iuo_spese_trasporto
destroy iuo_fido_cliente
end event

type dw_tes_off_ven from datawindow within w_tes_ord_ven_tv
integer x = 1303
integer y = 1620
integer width = 3200
integer height = 1104
integer taborder = 190
string title = "none"
string dataobject = "d_tes_ord_ven_offerte_tv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event buttonclicked;long ll_anno_reg_off, ll_num_reg_off

if row>0 then
	choose case dwo.name
		case "b_offerta"
			
			ll_anno_reg_off = dw_tes_off_ven.getitemnumber(row, "det_off_ven_anno_registrazione")
			ll_num_reg_off = dw_tes_off_ven.getitemnumber(row, "det_off_ven_num_registrazione")
			
			if ll_anno_reg_off > 0 and ll_num_reg_off > 0 then
				wf_apri_offerta(ll_anno_reg_off, ll_num_reg_off)
			else
				return
			end if
			
	end choose
end if
end event

type dw_folder_tv from u_folder within w_tes_ord_ven_tv
integer x = 23
integer y = 20
integer width = 1234
integer height = 2840
integer taborder = 30
end type

type dw_tes_ord_ven_det_3 from uo_cs_xx_dw within w_tes_ord_ven_tv
integer x = 1298
integer y = 128
integer width = 3081
integer height = 1444
integer taborder = 150
string dataobject = "d_tes_ord_ven_det_3"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_aspetto_beni"
		setnull(s_cs_xx.parametri.parametro_s_1)
		window_open(w_aspetto_beni, 0)
		setitem(row, "aspetto_beni", s_cs_xx.parametri.parametro_s_1)
		
end choose
end event

type dw_tes_ord_ven_ricerca from uo_std_dw within w_tes_ord_ven_tv
event ue_key pbm_dwnkey
integer x = 46
integer y = 120
integer width = 1166
integer height = 2700
integer taborder = 200
boolean bringtotop = true
string dataobject = "d_tes_ord_ven_ricerca_tv"
boolean controlmenu = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_key;if key = keyenter! then
	wf_ricerca()
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_sel_cliente"
		//selezione cliente -------------------------------------------------------------------------
		guo_ricerca.uof_ricerca_cliente(dw_tes_ord_ven_ricerca,"cod_cliente")
	
	case "b_sel_prodotto"
		//selezione prodotto per ricerca -------------------------------------------------------
		guo_ricerca.uof_ricerca_prodotto(dw_tes_ord_ven_ricerca,"cod_prodotto")
		
	case "b_cerca"
		//ricerca -------------------------------------------------------------------------------------
		wf_ricerca()
		
	case "b_annulla"
		//reset dei campi ----------------------------------------------------------------------------
		wf_annulla()
		
end choose
end event

event clicked;call super::clicked;choose case dwo.name
	case "b_cancella_filtro"
		guo_functions.uof_cancella_filtro(dw_tes_ord_ven_ricerca, "ORDVEN")
		
	case "b_leggi_filtro"
		//carica filtro impostato ----------------------------------------------------------------------
		guo_functions.uof_leggi_filtro(dw_tes_ord_ven_ricerca, "ORDVEN")
		
	case "b_memorizza_filtro"
		//salva filtro impostato sulla dw -----------------------------------------------------------
		guo_functions.uof_memorizza_filtro(dw_tes_ord_ven_ricerca, "ORDVEN")
end choose
end event

type cb_duplica_doc from commandbutton within w_tes_ord_ven_tv
event clicked pbm_bnclicked
boolean visible = false
integer x = 864
integer y = 1636
integer width = 361
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica Doc"
end type

type tv_1 from treeview within w_tes_ord_ven_tv
event ue_rightclick ( long handle )
event ue_menu_azzera_spese_trasporto ( )
event ue_importa_excel ( )
integer x = 64
integer y = 124
integer width = 1157
integer height = 2700
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean linesatroot = true
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event ue_rightclick(long handle);treeviewitem ltv_item
ss_record ls_record
string ls_tipo_nodo, ls_numero_ordine, ls_str, ls_spese_trasp
long 	ll_i
boolean lb_evaso

if handle < 1 then return 

il_handle = handle
selectitem(handle)
getitem(handle,ltv_item)
ls_record = ltv_item.data
ls_tipo_nodo = ls_record.tipo_livello

if ls_record.tipo_livello = "X" then
	//continua
	ls_numero_ordine = "O " + string(ls_record.anno_registrazione)+"/"+string(ls_record.num_registrazione)
else
	//non sono su un elemento di tipo ordine vendita, esci
	return
end if

lb_evaso = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") = "E"

m_pop_ordini_std lm_menu
lm_menu = create m_pop_ordini_std

lm_menu.m_ordinen.text = ls_numero_ordine
ls_spese_trasp = dw_tes_ord_ven_det_1.getitemstring(1, "flag_azzera_spese_trasp")

if upperbound(il_anno_conferma) > 0 then
	lm_menu.m_confermaaggiungi.enabled = true
	lm_menu.m_confermastampa.enabled = true
	ls_str = ""
	for ll_i = 1 to upperbound(il_anno_conferma)
		ls_str += "~r~n" + string(il_anno_conferma[ll_i]) + " - " + string(il_num_conferma[ll_i])
	next
	lm_menu.m_confermaaggiungi.text += ls_str
else
	lm_menu.m_confermastampa.enabled = false
end if

// spese trasp
lm_menu.m_azzeraspesetrasporto.visible = ls_spese_trasp = "N"
lm_menu.m_abilitaspesetrasporto.visible = ls_spese_trasp = "S"

// Se posso importare e NON sono in nuovo e NON è evaso
lm_menu.m_importa_excel.enabled = ib_import_configured and not dw_tes_ord_ven_det_1.ib_stato_nuovo and not lb_evaso

if not dw_tes_ord_ven_det_1.ib_stato_nuovo and not dw_tes_ord_ven_det_1.ib_stato_modifica then 
	//non sono in modifica o nuovo, disabilita quello che c'è da disabilitare
	lm_menu.m_assegna.enabled = true
	lm_menu.m_azzeraassegnazione.enabled = true
	lm_menu.m_bloccaordine.enabled = true
	lm_menu.m_calcolaordine.enabled = true
	lm_menu.m_chiudicommessa.enabled = true
	lm_menu.m_duplicasconto.enabled = true
	lm_menu.m_corrispondenze.enabled = true
	lm_menu.m_dettaglio.enabled = true
	lm_menu.m_evasioneparziale.enabled = true
	lm_menu.m_fatturaproforma.enabled = true
	lm_menu.m_generabolla.enabled = true
	lm_menu.m_note.enabled = true
	lm_menu.m_salda.enabled = true
	lm_menu.m_sbloccaordine.enabled = true
	lm_menu.m_stampaordine.enabled = true
else
	//sono in stato modifica o nuovo, disabilita quello che c'è da disabilitare
	lm_menu.m_assegna.enabled = false
	lm_menu.m_azzeraassegnazione.enabled = false
	lm_menu.m_bloccaordine.enabled = false
	lm_menu.m_calcolaordine.enabled = false
	lm_menu.m_chiudicommessa.enabled = false
	lm_menu.m_duplicasconto.enabled = false
	lm_menu.m_corrispondenze.enabled = false
	lm_menu.m_dettaglio.enabled = false
	lm_menu.m_evasioneparziale.enabled = false
	lm_menu.m_fatturaproforma.enabled = false
	lm_menu.m_generabolla.enabled = false
	lm_menu.m_note.enabled = false
	lm_menu.m_salda.enabled = false
	lm_menu.m_sbloccaordine.enabled = false
	lm_menu.m_stampaordine.enabled = false
end if

pcca.window_current  = parent

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu
end event

event itempopulate;string ls_null
long ll_livello = 0
treeviewitem ltv_item
ss_record ls_record

//pcca.window_current = parent

setnull(ls_null)

if isnull(handle) or handle <= 0 or ib_tree_deleting then
	return 0
end if

getitem(handle,ltv_item)

ls_record = ltv_item.data
ll_livello = ls_record.livello

il_livello_corrente = ll_livello

setpointer(HourGlass!)

if ll_livello < 3 then
	// controlla cosa c'è al livello successivo ???
	choose case dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(),"flag_tipo_livello_" + string(ll_livello + 1))
		case "T"
			// caricamento tipi ordini vendita
			if wf_inserisci_tipi_ordini(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "A"
			// caricamento anni registrazione
			if wf_inserisci_anni(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "C"
			// caricamento clienti
			if wf_inserisci_clienti(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "O"
			// caricamento operatori
			if wf_inserisci_operatori(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "D"
			// caricamento depositi
			if wf_inserisci_depositi(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "P"
			//caricamento prodotti
			if wf_inserisci_prodotti(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "R"
			// caricamento date registrazioni
			if wf_inserisci_data_registrazione(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "S"
			// caricamento date consegna
			if wf_inserisci_data_consegna(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "N" 
			if wf_inserisci_ordini(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
		
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
else
	// inserisco le registrazioni
	if wf_inserisci_ordini(handle) = 1 then
		ltv_item.children = false
		tv_1.setitem(handle, ltv_item)
	end if
	
end if

setpointer(Arrow!)
end event

event rightclicked;event post ue_rightclick(handle)
end event

event selectionchanged;treeviewitem ltv_item
ss_record lws_record

if isnull(newhandle) or newhandle <= 0 or ib_tree_deleting then
	return 0
end if
il_handle = newhandle
getitem(newhandle,ltv_item)
lws_record = ltv_item.data

dw_tes_ord_ven_det_1.change_dw_current()
parent.postevent("pc_retrieve")
end event

event key;if key = KeyX! and keyflags=3 and s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	if g_mb.confirm("Ricalcolare tutti gli ordini nella lista?") then

		long	ll_handle
		string ls_messaggio
		treeviewitem ltv_item
		ss_record lws_record
		uo_calcola_documento_euro luo_calcola_documento_euro
		
		ll_handle = il_handle
		
		setpointer(HourGlass!)
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tv_1.getitem(ll_handle, ltv_item)
			
			lws_record = ltv_item.data
			
			if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then
			
				luo_calcola_documento_euro = create uo_calcola_documento_euro
				
				luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
				
				w_cs_xx_mdi.setmicrohelp("Calcolo Ordine " + string(lws_record.anno_registrazione) + "-"  + string(lws_record.num_registrazione))
				Yield()
				
				if luo_calcola_documento_euro.uof_calcola_documento(lws_record.anno_registrazione, lws_record.num_registrazione,"ord_ven",ls_messaggio) <> 0 then
					if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
					rollback;
				else
					commit;
				end if
				destroy luo_calcola_documento_euro
			end if
			
			ll_handle = tv_1.finditem( NextTreeItem! ,ll_handle )

		loop
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if

end if
end event

type dw_tes_ord_ven_det_1 from uo_cs_xx_dw within w_tes_ord_ven_tv
integer x = 1303
integer y = 124
integer width = 3104
integer height = 1440
integer taborder = 130
string dataobject = "d_tes_ord_ven_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string			ls_null, ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
					ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, &
					ls_fax, ls_telex, ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, &
					ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
					ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca_clien_for, ls_cod_banca,&
					ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
					ls_cod_porto, ls_cod_resa, ls_flag_fuori_fido, ls_flag_riep_boll, &
					ls_flag_riep_fatt, ls_nota_testata, ls_nota_piede, ls_nota_video, &
					ls_messaggio, ls_flag_blocco
					
	double		ld_sconto
	
	datetime		ldt_data_registrazione, ldt_data_blocco
	integer		li_ret
	

   setnull(ls_null)

   choose case i_colname
      case "data_registrazione"
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         f_cambio_ven(ls_cod_valuta, datetime(date(i_coltext)))
		
		li_ret = iuo_fido_cliente.uof_get_blocco_cliente( getitemstring(i_rownbr, "cod_cliente"),&
																		datetime(date(i_coltext), 00:00:00), is_cod_parametro_blocco, ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			return 2
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		

      case "cod_cliente"
			ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")

			li_ret = iuo_fido_cliente.uof_get_blocco_cliente( data, ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
			if li_ret=2 then
				//blocco
				g_mb.error(ls_messaggio)
				this.setitem(i_rownbr, i_colname, ls_null)
				this.SetColumn ( i_colname )
				return 2
			elseif li_ret=1 then
				//solo warning
				g_mb.warning(ls_messaggio)
			end if
			
			
         select anag_clienti.rag_soc_1,
                anag_clienti.rag_soc_2,
                anag_clienti.indirizzo,
                anag_clienti.frazione,
                anag_clienti.cap,
                anag_clienti.localita,
                anag_clienti.provincia,
                anag_clienti.telefono,
                anag_clienti.fax,
                anag_clienti.telex,
                anag_clienti.partita_iva,
                anag_clienti.cod_fiscale,
                anag_clienti.cod_deposito,
                anag_clienti.cod_valuta,
                anag_clienti.cod_tipo_listino_prodotto,
                anag_clienti.cod_pagamento,
                anag_clienti.sconto,
                anag_clienti.cod_agente_1,
                anag_clienti.cod_agente_2,
                anag_clienti.cod_banca_clien_for,
                anag_clienti.cod_banca,
                anag_clienti.cod_imballo,
                anag_clienti.cod_vettore,
                anag_clienti.cod_inoltro,
                anag_clienti.cod_mezzo,
                anag_clienti.cod_porto,
                anag_clienti.cod_resa,
                anag_clienti.flag_fuori_fido,
                anag_clienti.flag_riep_boll,
                anag_clienti.flag_riep_fatt,
			   anag_clienti.flag_blocco,
			   anag_clienti.data_blocco					 
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale,
                :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_agente_1,
                :ls_cod_agente_2,
                :ls_cod_banca_clien_for,
                :ls_cod_banca,
                :ls_cod_imballo,
                :ls_cod_vettore,
                :ls_cod_inoltro,
                :ls_cod_mezzo,
                :ls_cod_porto,
                :ls_cod_resa,
                :ls_flag_fuori_fido,
                :ls_flag_riep_boll,
                :ls_flag_riep_fatt,
					 :ls_flag_blocco,
					 :ldt_data_blocco					 
         from   anag_clienti
         where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_clienti.cod_cliente = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
				
				if ldt_data_blocco < ldt_data_registrazione and ls_flag_blocco = "S" and li_ret=0 then
					g_mb.messagebox("Apice","Attenzione cliente bloccato!")
					this.setitem(i_rownbr, i_colname, ls_null)
					this.SetColumn ( i_colname )
					return 1
				end if					
				
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_agente_1", ls_cod_agente_1)
            this.setitem(i_rownbr, "cod_agente_2", ls_cod_agente_2)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            this.setitem(i_rownbr, "cod_banca", ls_cod_banca)
            this.setitem(i_rownbr, "flag_fuori_fido", ls_flag_fuori_fido)
            this.setitem(i_rownbr, "flag_riep_bol", ls_flag_riep_boll)
            this.setitem(i_rownbr, "flag_riep_fat", ls_flag_riep_fatt)
            if not isnull(i_coltext) then
               dw_tes_ord_ven_det_2.modify("st_cod_cliente.text='" + i_coltext + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_ord_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_ord_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_ord_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_ord_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_ord_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_ord_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_ord_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_ord_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_telex.text=''")
            end if
            if not isnull(ls_partita_iva) then
               dw_tes_ord_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
            end if
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_cod_imballo)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_cod_vettore)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_cod_inoltro)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_cod_mezzo)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_porto", ls_cod_porto)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_resa", ls_cod_resa)
   
            f_po_loaddddw_dw(this, &
                             "cod_des_cliente", &
                             sqlca, &
                             "anag_des_clienti", &
                             "cod_des_cliente", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_des_cliente", ls_null)
            ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_ven(ls_cod_valuta, ldt_data_registrazione)
				
            if f_cerca_destinazione(dw_tes_ord_ven_det_2, i_rownbr, i_coltext, "C", ls_null, ls_messaggio) = -1 then
					g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
				end if
         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_agente_1", ls_null)
            this.setitem(i_rownbr, "cod_agente_2", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_banca", ls_null)
            this.setitem(i_rownbr, "flag_fuori_fido", ls_null)
            this.setitem(i_rownbr, "flag_riep_bol", ls_null)
            this.setitem(i_rownbr, "flag_riep_fat", ls_null)
            dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
            dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
            dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
            dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
            dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
            dw_tes_ord_ven_det_2.modify("st_cap.text=''")
            dw_tes_ord_ven_det_2.modify("st_localita.text=''")
            dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
            dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
            dw_tes_ord_ven_det_2.modify("st_fax.text=''")
            dw_tes_ord_ven_det_2.modify("st_telex.text=''")
            dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
            dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_porto", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_resa", ls_null)
            this.setitem(i_rownbr, "cod_des_cliente", ls_null)
				g_mb.messagebox("Ordini Clienti","Indicare un codice cliente valido.",StopSign!)
				return 1
         end if 
			setnull(ls_null)
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			
			if guo_functions.uof_get_note_fisse(i_coltext, ls_null, ls_null, "ORD_VEN", ls_null, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
			
      case "cod_des_cliente"
         ls_cod_cliente = this.getitemstring(i_rownbr, "cod_cliente")
			if f_cerca_destinazione(dw_tes_ord_ven_det_2, i_rownbr, ls_cod_cliente, "C", i_coltext, ls_messaggio) = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
			end if
      case "cod_valuta"
         ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
         f_cambio_ven(i_coltext, ldt_data_registrazione)
   end choose
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   string ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, &
          ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
          ls_partita_iva, ls_cod_fiscale
	long   ll_anno_registrazione , ll_num_registrazione


   if this.getrow() > 0 then
		ll_anno_registrazione = this.getitemnumber(this.getrow(),"anno_registrazione")
		ll_num_registrazione  = this.getitemnumber(this.getrow(),"num_registrazione")
		wf_proteggi_colonne(ll_anno_registrazione, ll_num_registrazione, this.getitemstring(this.getrow(),"flag_evasione") )
      ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
      select anag_clienti.rag_soc_1,
             anag_clienti.rag_soc_2,
             anag_clienti.indirizzo,
             anag_clienti.frazione,
             anag_clienti.cap,
             anag_clienti.localita,
             anag_clienti.provincia,
             anag_clienti.telefono,
             anag_clienti.fax,
             anag_clienti.telex,
             anag_clienti.partita_iva,
             anag_clienti.cod_fiscale
      into   :ls_rag_soc_1,    
             :ls_rag_soc_2,
             :ls_indirizzo,    
             :ls_frazione,
             :ls_cap,
             :ls_localita,
             :ls_provincia,
             :ls_telefono,
             :ls_fax,
             :ls_telex,
             :ls_partita_iva,
             :ls_cod_fiscale
      from   anag_clienti
      where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_clienti.cod_cliente = :ls_cod_cliente;

      if sqlca.sqlcode = 0 then
         if not isnull(ls_cod_cliente) then
            dw_tes_ord_ven_det_2.modify("st_cod_cliente.text='" + ls_cod_cliente + "'")
         else
            ls_cod_cliente = ""
            dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
         end if
         if not isnull(ls_rag_soc_1) then
            dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
         end if
         if not isnull(ls_rag_soc_2) then
            dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
         end if
         if not isnull(ls_indirizzo) then
            dw_tes_ord_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
         end if
         if not isnull(ls_frazione) then
            dw_tes_ord_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
         end if
         if not isnull(ls_cap) then
            dw_tes_ord_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_cap.text=''")
         end if
         if not isnull(ls_localita) then
            dw_tes_ord_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_localita.text=''")
         end if
         if not isnull(ls_provincia) then
            dw_tes_ord_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
         end if
         if not isnull(ls_telefono) then
            dw_tes_ord_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
         end if
         if not isnull(ls_fax) then
            dw_tes_ord_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_fax.text=''")
         end if
         if not isnull(ls_telex) then
            dw_tes_ord_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_telex.text=''")
         end if
         if not isnull(ls_partita_iva) then
            dw_tes_ord_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
         end if
         if not isnull(ls_cod_fiscale) then
            dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
         end if

         f_po_loaddddw_dw(this, &
                          "cod_des_cliente", &
                          sqlca, &
                          "anag_des_clienti", &
                          "cod_des_cliente", &
                          "rag_soc_1", &
                          "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
      else
         dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
			dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
			dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
			dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
			dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
			dw_tes_ord_ven_det_2.modify("st_cap.text=''")
			dw_tes_ord_ven_det_2.modify("st_localita.text=''")
			dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
			dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
			dw_tes_ord_ven_det_2.modify("st_fax.text=''")
			dw_tes_ord_ven_det_2.modify("st_telex.text=''")
			dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
			dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
		end if
   end if
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = false
	dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
	dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = false
	
	ib_new = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_ord_ven, ls_cod_operatore, ls_cod_deposito, ls_errore

   select con_ord_ven.cod_tipo_ord_ven
   into   :ls_cod_tipo_ord_ven
   from   con_ord_ven
   where  con_ord_ven.cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		this.setitem(this.getrow(), "data_registrazione", datetime(today()))
	end if
	
	select cod_operatore
	  into :ls_cod_operatore
	  from tab_operatori_utenti
	 where cod_azienda = :s_cs_xx.cod_azienda and
	 		 cod_utente = :s_cs_xx.cod_utente and
			 flag_default = 'S';
			 
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
		pcca.error = c_fatal
		return
	elseif sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
	end if
	
	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
	if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----

	dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = true
	
	dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = true
	ib_new = true
	
	dw_tes_ord_ven_det_4.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_doc_suc_tes", 'I')
	dw_tes_ord_ven_det_4.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_st_note_tes", 'I')
	dw_tes_ord_ven_det_4.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_doc_suc_pie", 'I')
	dw_tes_ord_ven_det_4.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_st_note_pie", 'I')	
	
	//Donato 19-11-2008 gestione fidi
	this.object.flag_aut_sblocco_fido.protect = ii_protect_autorizza_sblocco
	
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if pcca.error = c_success then
		dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = false
		dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = false
		dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
	
		ib_new = false
   end if
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_tot_documento, ls_nome_prog, ls_cod_cliente, &
			 ls_nota_testata, ls_nota_piede, ls_nota_video, ls_nota_old, ls_null
	datetime ldt_data_registrazione

   long ll_anno_registrazione, ll_num_registrazione

	
   for li_i = 1 to this.deletedcount()
      ll_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
      ll_num_registrazione = this.getitemnumber(li_i, "num_registrazione", delete!, true)
		
		delete from iva_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante l'eliminazione delle righe iva. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if
		
		ls_tabella = "det_ord_ven_stat"
		ls_nome_prog = ""

      if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, 0) = -1 then
			return 1
		end if
			
		delete varianti_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione varianti ordini" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if

		delete integrazioni_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione integrazioni ordini" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if

		delete det_ord_ven_note		
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione note dettaglio ordine" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
		
		delete det_ord_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione corrispondenze dettaglio ordine" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
      
		delete scad_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione scadenze." + sqlca.sqlerrtext,stopsign!)
			return 1
		end if

		delete iva_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione dati iva dell'ordine." + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
		
		//------------------------------------------------------------------------------------------------------
		//pulizia tabella evasione da dati eventuali per evitare errore di FK
		delete from evas_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante pulizia tabella evas_ord_ven dalla riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		//------------------------------------------------------------------------------------------------------

      ls_tabella = "det_ord_ven"

      if f_riag_quan_impegnata(ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if

      if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
   next

   if this.getrow() > 0 then
      ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_registrazione")
      ll_num_registrazione = this.getitemnumber(this.getrow(), "num_registrazione")
      if ll_anno_registrazione <> 0 then
         ls_tabella = "det_ord_ven"

			if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") <> "A" then
				g_mb.messagebox("Attenzione", "Ordine non modificabile! Ha già subito un'evasione.", &
							  exclamation!, ok!)
				dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
			
			if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "E" then
				g_mb.messagebox("Attenzione", "Ordine non modificabile! E' Bloccato.", &
							  exclamation!, ok!)
				dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
			
			if f_controlla_quan_in_evasione(ll_anno_registrazione, ll_num_registrazione) then
				g_mb.messagebox("Attenzione", "Ordine non modificabile! Vi sono dettagli con quantità in evasione.", &
							  exclamation!, ok!)
				dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
						
			if ib_new then
				setnull(ls_null)
				ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
				ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
				
				if guo_functions.uof_get_note_fisse(i_coltext, ls_null, ls_null, "ORD_VEN", ls_null, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
					
					ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
					if isnull(ls_nota_old) then ls_nota_old = ""
					this.setitem(this.getrow(), "nota_testata", ls_nota_testata + ls_nota_old )
					
					ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
					if isnull(ls_nota_old) then ls_nota_old = ""
					this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
				end if
			end if
      end if
   end if
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	//cb_clienti_ricerca.enabled = true
	dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = true
	dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = true
	wf_proteggi_colonne(this.getitemnumber(this.getrow(),"anno_registrazione"), this.getitemnumber(this.getrow(),"num_registrazione"), this.getitemstring(this.getrow(),"flag_evasione") )
	
	//Donato 19-11-2008 gestione fidi
	this.object.flag_aut_sblocco_fido.protect = ii_protect_autorizza_sblocco
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_registrazione")) then

		ll_anno_registrazione = f_anno_esercizio()
      if ll_anno_registrazione > 0 then
         this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
      else
			g_mb.messagebox("Ordini Clienti","Impostare l'anno di esercizio in parametri aziendali")
		end if
      select con_ord_ven.num_registrazione
      into   :ll_num_registrazione
      from   con_ord_ven
      where  con_ord_ven.cod_azienda = :s_cs_xx.cod_azienda;

		choose case sqlca.sqlcode
			case 0
				ll_num_registrazione ++
			case 100
				g_mb.messagebox("Ordini di Vendita","Errore in assegnazione numero ordine: verificare di aver impostato i parametri offerte di vendita. ~r~nIl salvataggio verràeffettuato ugualmente")
			case else
				g_mb.messagebox("Ordini di Vendita","Errore in assegnazione numero ordine.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verràeffettuato ugualmente")
		end choose
		
		ll_max_registrazione = 0
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= ll_num_registrazione then ll_num_registrazione = ll_max_registrazione + 1
		end if
		
      this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
      update con_ord_ven
      set    con_ord_ven.num_registrazione = :ll_num_registrazione
      where  con_ord_ven.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next      
end event

event pcd_retrieve;call super::pcd_retrieve;//long ll_errore, ll_anno_registrazione, ll_num_registrazione
//
//choose case ib_esiste_padre
//	case false
//		ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
//		ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
//	case true
//		ll_anno_registrazione = il_anno_registrazione
//		ll_num_registrazione = il_num_registrazione
//end choose		
//ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
//
//if ll_errore < 0 then
//   pcca.error = c_fatal
//end if

long  l_Error
ss_record lstr_record
treeviewitem ltvi_campo

if tv_1.getitem(il_handle, ltvi_campo) = -1 then
	return
end if
lstr_record = ltvi_campo.data

if not(lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0 and lstr_record.tipo_livello = "X")  then
	setnull(lstr_record.anno_registrazione)
	setnull(lstr_record.num_registrazione)
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
if l_Error < 0 then
	PCCA.Error = c_Fatal
end if

//retrieve offerte
wf_retrieve_offerte_vendita(lstr_record.anno_registrazione, lstr_record.num_registrazione)


il_anno_registrazione = lstr_record.anno_registrazione
il_num_registrazione = lstr_record.num_registrazione
end event

event pcd_delete;call super::pcd_delete;//aggiunto dopo aver messo il tree

treeviewitem ltv_item
ss_record ls_record

il_handle_modificato = 0
il_handle_cancellato = 0

if deletedcount() > 0 then
	il_handle_cancellato = tv_1.finditem(CurrentTreeItem!, 0)
	
	if il_handle_cancellato>0 then
		tv_1.getitem(il_handle_cancellato,ltv_item)
		ls_record = ltv_item.data
		
		if ls_record.tipo_livello = "X" then
			//è un documento, rimuovilo
			tv_1.deleteitem(il_handle_cancellato)
		end if
		
	end if
	
end if
end event

event updateend;call super::updateend;long					ll_row, ll_risposta
ss_record			ls_record
 treeviewitem		ltv_item

//inserimento elemento sull'albero se nuovo
ll_row = this.getrow()

choose case this.getitemstatus(ll_row, 0, Primary!)
	case NewModified!	
		
		ls_record.anno_registrazione = this.getitemnumber( ll_row, "anno_registrazione")
		ls_record.num_registrazione = this.getitemnumber( ll_row, "num_registrazione")
		ls_record.codice = ""
		ls_record.descrizione = ""
		ls_record.livello = 100
		ls_record.tipo_livello = "X"
		
		ltv_item.data = ls_record
		ltv_item.label = "NEW! " + string(ls_record.anno_registrazione) + "/" + string(ls_record.num_registrazione)
		
		ltv_item.pictureindex = 8
		ltv_item.selectedpictureindex = 8
		ltv_item.overlaypictureindex = 8
					
		ltv_item.children = false
		ltv_item.selected = false
	
		ll_risposta = tv_1.insertitemLast(0, ltv_item)
		tv_1.SelectItem(ll_risposta)
		
	case New!
		
end choose
end event

event buttonclicked;call super::buttonclicked;if row>0 then
	choose case dwo.name
			
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_tes_ord_ven_det_1,"cod_cliente")
			
		case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(dw_tes_ord_ven_det_1,"cod_banca_clien_for")
			
			
	end choose
end if
end event

type dw_tes_ord_ven_det_2 from uo_cs_xx_dw within w_tes_ord_ven_tv
integer x = 1321
integer y = 120
integer width = 3058
integer height = 1364
integer taborder = 140
string dataobject = "d_tes_ord_ven_det_2"
boolean border = false
end type

type dw_tes_ord_ven_det_4 from uo_cs_xx_dw within w_tes_ord_ven_tv
integer x = 1298
integer y = 120
integer width = 3081
integer height = 1156
integer taborder = 160
string dataobject = "d_tes_ord_ven_det_4"
boolean border = false
end type

type dw_folder from u_folder within w_tes_ord_ven_tv
integer x = 1280
integer y = 20
integer width = 3246
integer height = 2840
integer taborder = 0
end type

event po_tabclicked;call super::po_tabclicked;dw_tes_ord_ven_det_3.settaborder("cod_imballo",5)
end event

