﻿$PBExportHeader$w_tipi_ord_ven.srw
$PBExportComments$Finestra Gestione Tipi Ordini Vendita
forward
global type w_tipi_ord_ven from w_cs_xx_principale
end type
type dw_tipi_ord_ven_lista from uo_cs_xx_dw within w_tipi_ord_ven
end type
type dw_tipi_ord_ven_det from uo_cs_xx_dw within w_tipi_ord_ven
end type
end forward

global type w_tipi_ord_ven from w_cs_xx_principale
integer width = 2587
integer height = 2136
string title = "Tipi Ordini Vendita"
dw_tipi_ord_ven_lista dw_tipi_ord_ven_lista
dw_tipi_ord_ven_det dw_tipi_ord_ven_det
end type
global w_tipi_ord_ven w_tipi_ord_ven

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_ord_ven_lista.set_dw_key("cod_azienda")
dw_tipi_ord_ven_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tipi_ord_ven_det.set_dw_options(sqlca, &
                                   dw_tipi_ord_ven_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_tipi_ord_ven_lista
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_ord_ven_det, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_ord_ven_det, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_fat_ven = 'I'")
f_po_loaddddw_dw(dw_tipi_ord_ven_det, &
                 "cod_tipo_bol_ven", &
                 sqlca, &
                 "tab_tipi_bol_ven", &
                 "cod_tipo_bol_ven", &
                 "des_tipo_bol_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_bol_ven = 'V'")    
f_po_loaddddw_dw(dw_tipi_ord_ven_det, &
                 "cod_tipo_analisi", &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_ord_ven_det, &
                 "cod_tipo_commessa", &
                 sqlca, &
                 "tab_tipi_commessa", &
                 "cod_tipo_commessa", &
                 "des_tipo_commessa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_ord_ven_det, &
                 "cod_tipo_fat_proforma", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_fat_ven = 'P'")
					  
f_po_loaddddw_dw(dw_tipi_ord_ven_det, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
				"")

end event

on w_tipi_ord_ven.create
int iCurrent
call super::create
this.dw_tipi_ord_ven_lista=create dw_tipi_ord_ven_lista
this.dw_tipi_ord_ven_det=create dw_tipi_ord_ven_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_ord_ven_lista
this.Control[iCurrent+2]=this.dw_tipi_ord_ven_det
end on

on w_tipi_ord_ven.destroy
call super::destroy
destroy(this.dw_tipi_ord_ven_lista)
destroy(this.dw_tipi_ord_ven_det)
end on

type dw_tipi_ord_ven_lista from uo_cs_xx_dw within w_tipi_ord_ven
integer x = 23
integer y = 20
integer width = 2514
integer height = 500
integer taborder = 10
string dataobject = "d_tipi_ord_ven_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_new;call super::pcd_new;dw_tipi_ord_ven_det.setitem(dw_tipi_ord_ven_lista.getrow(), "flag_doc_suc", "S")
dw_tipi_ord_ven_det.setitem(dw_tipi_ord_ven_lista.getrow(), "flag_st_note", "S")
dw_tipi_ord_ven_det.object.b_moduli.enabled = false
end event

event pcd_modify;call super::pcd_modify;dw_tipi_ord_ven_det.object.b_moduli.enabled = false
end event

event pcd_view;call super::pcd_view;dw_tipi_ord_ven_det.object.b_moduli.enabled = true
end event

event pcd_save;call super::pcd_save;dw_tipi_ord_ven_det.object.b_moduli.enabled = true
end event

type dw_tipi_ord_ven_det from uo_cs_xx_dw within w_tipi_ord_ven
integer x = 23
integer y = 540
integer width = 2510
integer height = 1472
integer taborder = 20
string dataobject = "d_tipi_ord_ven_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_moduli"
		dw_tipi_ord_ven_lista.accepttext()
		window_open_parm(w_tipi_ord_ven_lingue_dw, -1, dw_tipi_ord_ven_lista)
end choose
end event

