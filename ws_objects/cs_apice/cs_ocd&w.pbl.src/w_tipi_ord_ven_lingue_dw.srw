﻿$PBExportHeader$w_tipi_ord_ven_lingue_dw.srw
forward
global type w_tipi_ord_ven_lingue_dw from w_cs_xx_principale
end type
type dw_tipi_ord_ven_lingue_dw from uo_cs_xx_dw within w_tipi_ord_ven_lingue_dw
end type
end forward

global type w_tipi_ord_ven_lingue_dw from w_cs_xx_principale
integer width = 3611
integer height = 1124
string title = "Moduli"
dw_tipi_ord_ven_lingue_dw dw_tipi_ord_ven_lingue_dw
end type
global w_tipi_ord_ven_lingue_dw w_tipi_ord_ven_lingue_dw

on w_tipi_ord_ven_lingue_dw.create
int iCurrent
call super::create
this.dw_tipi_ord_ven_lingue_dw=create dw_tipi_ord_ven_lingue_dw
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_ord_ven_lingue_dw
end on

on w_tipi_ord_ven_lingue_dw.destroy
call super::destroy
destroy(this.dw_tipi_ord_ven_lingue_dw)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_ord_ven_lingue_dw.set_dw_key("cod_azienda")
dw_tipi_ord_ven_lingue_dw.set_dw_key("cod_tipo_ord_ven")
dw_tipi_ord_ven_lingue_dw.set_dw_key("cod_lingua")
dw_tipi_ord_ven_lingue_dw.set_dw_key("progressivo")

dw_tipi_ord_ven_lingue_dw.set_dw_options(sqlca, &
											 i_openparm, &
											 c_scrollparent, &
											 c_default)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_ord_ven_lingue_dw, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_tipi_ord_ven_lingue_dw from uo_cs_xx_dw within w_tipi_ord_ven_lingue_dw
integer width = 3552
integer height = 1000
integer taborder = 10
string dataobject = "d_tipi_ord_ven_lingue_dw"
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_ord_ven, ls_des_tipo_ord_ven
long l_errore

ls_cod_tipo_ord_ven = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_ord_ven")
ls_des_tipo_ord_ven = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "des_tipo_ord_ven")

parent.title = "MODULI " + ls_des_tipo_ord_ven 
	
l_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_ord_ven)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_ord_ven
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max

ls_cod_tipo_ord_ven = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_ord_ven")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_tipo_ord_ven")) or &
      this.getitemstring(ll_i, "cod_tipo_ord_ven") = "" then
      this.setitem(ll_i, "cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
   end if

   if isnull(this.getitemnumber(ll_i, "progressivo")) or &
      this.getitemnumber(ll_i, "progressivo") = 0 then
		
		ll_max = 0
		
		select max(progressivo)
		into   :ll_max
		from   tab_tipi_ord_ven_lingue_dw
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

		if ll_max = 0 or isnull(ll_max) then
			ll_max = 10
		else
			ll_max += 10
		end if
		
      this.setitem(ll_i, "progressivo", ll_max)
   end if
	
next

end event

event clicked;call super::clicked;choose case dwo.name
	case "b_cliente"
		
	setnull(s_cs_xx.parametri.parametro_uo_dw_search)
	guo_ricerca.uof_ricerca_cliente(dw_tipi_ord_ven_lingue_dw,"cod_cliente")
		
end choose
end event

event pcd_new;call super::pcd_new;dw_tipi_ord_ven_lingue_dw.object.b_ricerca_cliente.enabled = true
end event

event pcd_modify;call super::pcd_modify;dw_tipi_ord_ven_lingue_dw.object.b_ricerca_cliente.enabled = true
end event

event pcd_save;call super::pcd_save;dw_tipi_ord_ven_lingue_dw.object.b_ricerca_cliente.enabled = false
end event

event pcd_view;call super::pcd_view;dw_tipi_ord_ven_lingue_dw.object.b_ricerca_cliente.enabled = false
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tipi_ord_ven_lingue_dw,"cod_cliente")
end choose
end event

