﻿$PBExportHeader$w_con_off_ven.srw
$PBExportComments$Finestra Gestione Parametri Offerte Vendita
forward
global type w_con_off_ven from w_cs_xx_principale
end type
type dw_con_off_ven from uo_cs_xx_dw within w_con_off_ven
end type
end forward

global type w_con_off_ven from w_cs_xx_principale
integer width = 2459
integer height = 1000
string title = "Gestione Parametri Offerte Vendita"
dw_con_off_ven dw_con_off_ven
end type
global w_con_off_ven w_con_off_ven

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_off_ven, &
                 "cod_tipo_off_ven", &
                 sqlca, &
                 "tab_tipi_off_ven", &
                 "cod_tipo_off_ven", &
                 "des_tipo_off_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;dw_con_off_ven.set_dw_key("cod_azienda")
dw_con_off_ven.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end event

on w_con_off_ven.create
int iCurrent
call super::create
this.dw_con_off_ven=create dw_con_off_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_off_ven
end on

on w_con_off_ven.destroy
call super::destroy
destroy(this.dw_con_off_ven)
end on

type dw_con_off_ven from uo_cs_xx_dw within w_con_off_ven
integer width = 2423
integer height = 900
string dataobject = "d_con_off_ven"
boolean border = false
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

