﻿$PBExportHeader$w_det_off_ven.srw
$PBExportComments$Finestra Dettaglio Offerte di Vendita
forward
global type w_det_off_ven from w_cs_xx_principale
end type
type cb_sconti from commandbutton within w_det_off_ven
end type
type uo_1 from uo_situazione_prodotto within w_det_off_ven
end type
type cb_c_industriale from commandbutton within w_det_off_ven
end type
type cb_corrispondenze from commandbutton within w_det_off_ven
end type
type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_off_ven
end type
type dw_det_off_ven_lista from uo_cs_xx_dw within w_det_off_ven
end type
type dw_folder from u_folder within w_det_off_ven
end type
type dw_det_off_ven_det_1 from uo_cs_xx_dw within w_det_off_ven
end type
end forward

global type w_det_off_ven from w_cs_xx_principale
integer width = 4613
integer height = 1748
string title = "Dett.Offerte Clienti"
boolean minbox = false
cb_sconti cb_sconti
uo_1 uo_1
cb_c_industriale cb_c_industriale
cb_corrispondenze cb_corrispondenze
dw_documenti dw_documenti
dw_det_off_ven_lista dw_det_off_ven_lista
dw_folder dw_folder
dw_det_off_ven_det_1 dw_det_off_ven_det_1
end type
global w_det_off_ven w_det_off_ven

type variables
boolean ib_richieste=false
long il_anno_reg_richiesta=0, il_num_reg_richiesta=0

uo_condizioni_cliente iuo_condizioni_cliente
uo_gestione_conversioni iuo_gestione_conversioni 

private:
	string is_cod_subtotale
	long il_riga_precedente = -1

end variables

forward prototypes
public function integer wf_ricalcola ()
public function boolean wf_duplica (integer ai_row)
public function boolean wf_duplica_righe (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_off_ven_orig, long al_prog_riga_off_ven)
public function boolean wf_get_num_riga (ref long al_prog_riga_off_ven)
public function integer wf_duplica_altre_tabelle (long fl_anno, long fl_numero, long fl_prog, long fl_prog_new, ref string fs_msg)
public subroutine wf_tipo_dettaglio_ven_det (datawindow ad_datawindow, commandbutton ac_prodotti_ricerca, picturebutton ap_prod_view, string as_cod_tipo_det_ven, long al_row)
end prototypes

public function integer wf_ricalcola ();long   ll_i

string ls_parametro, ls_prodotto, ls_misura_mag, ls_misura_ven

dec{4} ld_quan_mag, ld_prezzo_mag, ld_quan_ven, ld_prezzo_ven, ld_fat_conversione, ld_prec_mag, ld_prec_ven

uo_calcola_documento_euro luo_calcolo


select stringa
into   :ls_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'TRD';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro TRD da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_parametro) or (ls_parametro <> "M" and ls_parametro <> "V") then
	return 0
end if

select precisione_prezzo_mag,
		 precisione_prezzo_ven
into   :ld_prec_mag,
		 :ld_prec_ven
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in lettura precisione prezzi da parametri vendite: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ld_prec_mag) or ld_prec_mag = 0 then
	g_mb.messagebox("APICE","Impostare la precisione del prezzo di magazzino in PARAMETRI VENDITE")
	return -1
end if

if isnull(ld_prec_ven) or ld_prec_ven = 0 then
	g_mb.messagebox("APICE","Impostare la precisione del prezzo di vendita in PARAMETRI VENDITE")
	return -1
end if

for ll_i = 1 to dw_det_off_ven_lista.rowcount()
	
	ls_prodotto = dw_det_off_ven_lista.getitemstring(ll_i,"cod_prodotto")
	
	if isnull(ls_prodotto) then
		continue
	end if
	
	ls_misura_ven = dw_det_off_ven_lista.getitemstring(ll_i,"cod_misura")
	
	select cod_misura_mag
	into   :ls_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_prodotto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in lettura dati prodotto da anag_prodotti: " + sqlca.sqlerrtext)
		return -1
	end if
	
	if ls_misura_ven = ls_misura_mag then
		continue
	end if
	
	ld_quan_mag = dw_det_off_ven_lista.getitemnumber(ll_i,"quan_offerta")
	
	ld_prezzo_mag = dw_det_off_ven_lista.getitemnumber(ll_i,"prezzo_vendita")
	
	ld_quan_ven = dw_det_off_ven_lista.getitemnumber(ll_i,"quantita_um")
	
	ld_prezzo_ven = dw_det_off_ven_lista.getitemnumber(ll_i,"prezzo_um")
	
	ld_fat_conversione = dw_det_off_ven_lista.getitemnumber(ll_i,"fat_conversione_ven")
	
	choose case ls_parametro
			
		case "M"
			
			ld_quan_ven = round(ld_quan_mag * ld_fat_conversione , 4)
			
			ld_prezzo_ven = ld_prezzo_mag / ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_ven,ld_prec_ven,"round")
			
			destroy luo_calcolo
			
		case "V"
			
			ld_quan_mag = round(ld_quan_ven / ld_fat_conversione , 4)
			
			ld_prezzo_mag = ld_prezzo_ven * ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_mag,ld_prec_mag,"round")
			
			destroy luo_calcolo
			
	end choose
	
	dw_det_off_ven_lista.setitem(ll_i,"quan_offerta",ld_quan_mag)
	
	dw_det_off_ven_lista.setitem(ll_i,"prezzo_vendita",ld_prezzo_mag)
	
	dw_det_off_ven_lista.setitem(ll_i,"quantita_um",ld_quan_ven)
	
	dw_det_off_ven_lista.setitem(ll_i,"prezzo_um",ld_prezzo_ven)
	
next

return 0
end function

public function boolean wf_duplica (integer ai_row);/**
**/

string ls_error
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven_orig,  ll_prog_riga_off_ven_dupl

setpointer(HourGlass!)


ll_anno_registrazione = dw_det_off_ven_lista.getitemnumber(ai_row, "anno_registrazione")
ll_num_registrazione = dw_det_off_ven_lista.getitemnumber(ai_row, "num_registrazione")
ll_prog_riga_off_ven_orig  = dw_det_off_ven_lista.getitemnumber(ai_row, "prog_riga_off_ven")

// calcolo nuova riga
select max(prog_riga_off_ven)
into :ll_prog_riga_off_ven_dupl
from det_off_ven
where    	
	cod_azienda = :s_cs_xx.cod_azienda and  
	anno_registrazione = :ll_anno_registrazione and  
	num_registrazione = :ll_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il calcolo del progressivo della nuova riga.", sqlca)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_prog_riga_off_ven_dupl) or ll_prog_riga_off_ven_dupl < 1 then
	ll_prog_riga_off_ven_dupl = 0
//else
//	ll_prog_riga_off_ven_dupl++
end if

ll_prog_riga_off_ven_dupl = int(ll_prog_riga_off_ven_dupl / 10)
if ll_prog_riga_off_ven_dupl = 0 then
	ll_prog_riga_off_ven_dupl = 10
else
	ll_prog_riga_off_ven_dupl = ll_prog_riga_off_ven_dupl * 10 + 10
end if

// Duplico riga dettaglio
insert into det_off_ven (
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	prog_riga_off_ven,   
	cod_tipo_det_ven,   
	cod_misura,   
	cod_prodotto,   
	des_prodotto,   
	quan_offerta,   
	prezzo_vendita,   
	fat_conversione_ven,   
	sconto_1,   
	sconto_2,   
	provvigione_1,   
	provvigione_2,   
	val_riga,   
	cod_iva,   
	data_consegna,   
	nota_dettaglio,   
	sconto_3,   
	sconto_4,   
	sconto_5,   
	sconto_6,   
	sconto_7,   
	sconto_8,   
	sconto_9,   
	sconto_10,   
	cod_centro_costo,
	anno_reg_richiesta,
	num_reg_richiesta,
	flag_gen_commessa)  
select 
		det_off_ven.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		:ll_prog_riga_off_ven_dupl,   
		det_off_ven.cod_tipo_det_ven,   
		det_off_ven.cod_misura,   
		det_off_ven.cod_prodotto,   
		det_off_ven.des_prodotto,   
		det_off_ven.quan_offerta,   
		det_off_ven.prezzo_vendita,   
		det_off_ven.fat_conversione_ven,   
		det_off_ven.sconto_1,   
		det_off_ven.sconto_2,   
		det_off_ven.provvigione_1,   
		det_off_ven.provvigione_2,   
		det_off_ven.val_riga,   
		det_off_ven.cod_iva,   
		det_off_ven.data_consegna,   
		det_off_ven.nota_dettaglio,   
		det_off_ven.sconto_3,   
		det_off_ven.sconto_4,   
		det_off_ven.sconto_5,   
		det_off_ven.sconto_6,   
		det_off_ven.sconto_7,   
		det_off_ven.sconto_8,   
		det_off_ven.sconto_9,   
		det_off_ven.sconto_10,   
		det_off_ven.cod_centro_costo,
		det_off_ven.anno_reg_richiesta,
		det_off_ven.num_reg_richiesta,
		det_off_ven.flag_gen_commessa
	from det_off_ven
	where
		det_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		det_off_ven.anno_registrazione = :ll_anno_registrazione and  
		det_off_ven.num_registrazione = :ll_num_registrazione and
		det_off_ven.prog_riga_off_ven = :ll_prog_riga_off_ven_orig;

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di duplicazione dettagli offerte.", sqlca)
	rollback;
	return false
end if

if wf_duplica_altre_tabelle(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven_orig, ll_prog_riga_off_ven_dupl, ls_error) < 0 then
	g_mb.error(ls_error, sqlca)
	rollback;
	return false
end if


// Duplico riga dettaglio statico
insert into det_off_ven_stat (
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	prog_riga_off_ven,   
	cod_tipo_analisi,   
	progressivo,   
	cod_statistico_1,   
	cod_statistico_2,   
	cod_statistico_3,   
	cod_statistico_4,   
	cod_statistico_5,   
	cod_statistico_6,   
	cod_statistico_7,   
	cod_statistico_8,   
	cod_statistico_9,   
	cod_statistico_10,   
	percentuale,   
	flag_trasferito)  
	select
		det_off_ven_stat.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		:ll_prog_riga_off_ven_dupl,
		det_off_ven_stat.cod_tipo_analisi,
		det_off_ven_stat.progressivo,
		det_off_ven_stat.cod_statistico_1,
		det_off_ven_stat.cod_statistico_2,
		det_off_ven_stat.cod_statistico_3,
		det_off_ven_stat.cod_statistico_4,
		det_off_ven_stat.cod_statistico_5,
		det_off_ven_stat.cod_statistico_6,
		det_off_ven_stat.cod_statistico_7,
		det_off_ven_stat.cod_statistico_8,   
		det_off_ven_stat.cod_statistico_9,
		det_off_ven_stat.cod_statistico_10,
		det_off_ven_stat.percentuale,   
		'N'  
	from det_off_ven_stat  
	where
		det_off_ven_stat.cod_azienda = :s_cs_xx.cod_azienda and   
		det_off_ven_stat.anno_registrazione = :ll_anno_registrazione and   
		det_off_ven_stat.num_registrazione = :ll_num_registrazione and
		det_off_ven_stat.prog_riga_off_ven = :ll_prog_riga_off_ven_orig;

if not wf_duplica_righe(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven_orig, ll_prog_riga_off_ven_dupl) then
	rollback;
	return false
end if

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di duplicazione dettagli statistici.", sqlca)
	rollback;
	return false
end if

return true
end function

public function boolean wf_duplica_righe (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_off_ven_orig, long al_prog_riga_off_ven);string ls_sql, ls_error
long ll_anno_reg_cu, ll_num_reg_cu, ll_prog_riga_ord_ven_cu, ll_prog_riga

ls_sql = "select anno_registrazione, num_registrazione, prog_riga_off_ven from det_off_ven " + &
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and anno_registrazione="+string(al_anno_registrazione)+" and "+&
						"num_registrazione="+string(al_num_registrazione)+" and num_riga_appartenenza="+string(al_prog_riga_off_ven_orig)

declare cu_cursore dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_cursore;

if sqlca.sqlcode <> 0 then 
	g_mb.error("Errore nella open del cursore delle righe collegate.", sqlca)
	return false
end if

ll_prog_riga = al_prog_riga_off_ven

do while true
	fetch cu_cursore
	into :ll_anno_reg_cu, :ll_num_reg_cu, :ll_prog_riga_ord_ven_cu;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore nella fetch del cursore delle righe collegate.", sqlca)
		close cu_cursore;
		return false
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	ll_prog_riga += 1
	
	//duplica la riga collegata
	// Duplico riga dettaglio
	insert into det_off_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_off_ven,   
		cod_tipo_det_ven,   
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_offerta,   
		prezzo_vendita,   
		fat_conversione_ven,   
		sconto_1,   
		sconto_2,   
		provvigione_1,   
		provvigione_2,   
		val_riga,   
		cod_iva,   
		data_consegna,   
		nota_dettaglio,   
		sconto_3,   
		sconto_4,   
		sconto_5,   
		sconto_6,   
		sconto_7,   
		sconto_8,   
		sconto_9,   
		sconto_10,   
		cod_centro_costo,
		anno_reg_richiesta,
		num_reg_richiesta,
		num_riga_appartenenza)  
	select 
		det_off_ven.cod_azienda,   
		:al_anno_registrazione,   
		:al_num_registrazione,   
		:ll_prog_riga,   
		det_off_ven.cod_tipo_det_ven,   
		det_off_ven.cod_misura,   
		det_off_ven.cod_prodotto,   
		det_off_ven.des_prodotto,   
		det_off_ven.quan_offerta,   
		det_off_ven.prezzo_vendita,   
		det_off_ven.fat_conversione_ven,   
		det_off_ven.sconto_1,   
		det_off_ven.sconto_2,   
		det_off_ven.provvigione_1,   
		det_off_ven.provvigione_2,   
		det_off_ven.val_riga,   
		det_off_ven.cod_iva,   
		det_off_ven.data_consegna,   
		det_off_ven.nota_dettaglio,   
		det_off_ven.sconto_3,   
		det_off_ven.sconto_4,   
		det_off_ven.sconto_5,   
		det_off_ven.sconto_6,   
		det_off_ven.sconto_7,   
		det_off_ven.sconto_8,   
		det_off_ven.sconto_9,   
		det_off_ven.sconto_10,   
		det_off_ven.cod_centro_costo,
		det_off_ven.anno_reg_richiesta,
		det_off_ven.num_reg_richiesta,
		:al_prog_riga_off_ven
	from det_off_ven
	where
		det_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		det_off_ven.anno_registrazione = :ll_anno_reg_cu and  
		det_off_ven.num_registrazione = :ll_num_reg_cu and
		det_off_ven.prog_riga_off_ven = :ll_prog_riga_ord_ven_cu;

	if sqlca.sqlcode=0 then
	else
		g_mb.error("Errore in inserimento duplicato riga collegata "+string(ll_prog_riga_ord_ven_cu), sqlca)	
		close cu_cursore;
		return false
	end if
	
	if wf_duplica_altre_tabelle(ll_anno_reg_cu, ll_num_reg_cu, ll_prog_riga_ord_ven_cu, ll_prog_riga, ls_error) < 0 then
		g_mb.error(ls_error, sqlca)
		rollback;
		return false
	end if

	// Duplico riga dettaglio statico
	insert into det_off_ven_stat (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_off_ven,   
		cod_tipo_analisi,   
		progressivo,   
		cod_statistico_1,   
		cod_statistico_2,   
		cod_statistico_3,   
		cod_statistico_4,   
		cod_statistico_5,   
		cod_statistico_6,   
		cod_statistico_7,   
		cod_statistico_8,   
		cod_statistico_9,   
		cod_statistico_10,   
		percentuale,   
		flag_trasferito)  
		select
			det_off_ven_stat.cod_azienda,
			:al_anno_registrazione,
			:al_num_registrazione,
			:ll_prog_riga,
			det_off_ven_stat.cod_tipo_analisi,
			det_off_ven_stat.progressivo,
			det_off_ven_stat.cod_statistico_1,
			det_off_ven_stat.cod_statistico_2,
			det_off_ven_stat.cod_statistico_3,
			det_off_ven_stat.cod_statistico_4,
			det_off_ven_stat.cod_statistico_5,
			det_off_ven_stat.cod_statistico_6,
			det_off_ven_stat.cod_statistico_7,
			det_off_ven_stat.cod_statistico_8,   
			det_off_ven_stat.cod_statistico_9,
			det_off_ven_stat.cod_statistico_10,
			det_off_ven_stat.percentuale,   
			'N'  
		from det_off_ven_stat  
		where
			det_off_ven_stat.cod_azienda = :s_cs_xx.cod_azienda and   
			det_off_ven_stat.anno_registrazione = :al_anno_registrazione and   
			det_off_ven_stat.num_registrazione = :al_num_registrazione and
			det_off_ven_stat.prog_riga_off_ven = :ll_prog_riga_ord_ven_cu;
loop

close cu_cursore;

return true
end function

public function boolean wf_get_num_riga (ref long al_prog_riga_off_ven);long ll_anno_registrazione, ll_num_registrazione, ll_cur_riga_off_ven, ll_prog_riga_off_ven


if il_riga_precedente = 1 and dw_det_off_ven_lista.rowcount() = 1 then
	// lascio il numero impostato dalla pcd_new della DW
	return false
elseif il_riga_precedente > 0 then
	// ho selezionato una riga e ne calcolo il successivo
	ll_cur_riga_off_ven = dw_det_off_ven_lista.getitemnumber(il_riga_precedente, "prog_riga_off_ven")
	ll_prog_riga_off_ven = ll_cur_riga_off_ven + 1
	do
		// se avevo seleziona la riga 10 ed è libera la 11 allora diventa 11
		if truncate(ll_cur_riga_off_ven / 10, 0) = truncate(((ll_prog_riga_off_ven) / 10), 0) then
			
			if dw_det_off_ven_lista.find("prog_riga_off_ven=" + string(ll_prog_riga_off_ven), 0, dw_det_off_ven_lista.rowcount()) <= 0 then
				al_prog_riga_off_ven = ll_prog_riga_off_ven
				return true
			end if
			
			ll_prog_riga_off_ven++
			
		else
			exit
		end if
	loop while true
end if

return false
end function

public function integer wf_duplica_altre_tabelle (long fl_anno, long fl_numero, long fl_prog, long fl_prog_new, ref string fs_msg);

//integrazioni_det_off_ven ----------------------------------------------------------------
insert into integrazioni_det_off_ven
(				cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				prog_riga_off_ven, 
				progressivo,   
				cod_prodotto_padre,   
				num_sequenza,   
				cod_prodotto_figlio,   
				cod_misura,   
				quan_tecnica,   
				quan_utilizzo,   
				dim_x,   
				dim_y,   
				dim_z,   
				dim_t,   
				coef_calcolo,   
				des_estesa,   
				cod_formula_quan_utilizzo,   
				flag_escludibile,   
				flag_materia_prima,   
				flag_arrotonda,   
				cod_versione_padre,   
				cod_versione_figlio,   
				lead_time)
  select 	cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				:fl_prog_new, 
				progressivo,   
				cod_prodotto_padre,   
				num_sequenza,   
				cod_prodotto_figlio,   
				cod_misura,   
				quan_tecnica,   
				quan_utilizzo,   
				dim_x,   
				dim_y,   
				dim_z,   
				dim_t,   
				coef_calcolo,   
				des_estesa,   
				cod_formula_quan_utilizzo,   
				flag_escludibile,   
				flag_materia_prima,   
				flag_arrotonda,   
				cod_versione_padre,   
				cod_versione_figlio,   
				lead_time
	from integrazioni_det_off_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_off_ven=:fl_prog;

if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento integrazioni_det_ff_ven  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine integrazioni_det_off_ven -------------------------------------------------------------------------------------------------

//det_off_ven_note --------------------------------------------------------------------------------------------------------------
insert into det_off_ven_note
(				cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				prog_riga_off_ven,
				cod_nota,   
				des_nota,   
				note)
  	select 	cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				:fl_prog_new,
				cod_nota,   
				des_nota,   
				note
	from det_off_ven_note
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_off_ven=:fl_prog;

if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento det_off_ven_note  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine det_off_ven_note ------------------------------------------------------------------------------------------------------

//det_off_ven_corrispondenze ------------------------------------------------------------------------------------------------
insert into det_off_ven_corrispondenze
(			cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         prog_riga_off_ven,   
         cod_corrispondenza,   
         data_corrispondenza,   
         ora_corrispondenza,   
         prog_corrispondenza,   
         intestatario,   
         data_pros_corrispondenza,   
         ora_pros_corrispondenza,   
         oggetto,   
         num_reg_lista,   
         num_versione,   
         num_edizione,   
         num_reg_lista_comp,   
         prog_liste_con_comp,   
         flag_tempo,   
         tempo_speso,   
         note,   
	    	cod_tipo_agenda,   
         cod_utente,            
         ora_agenda,   
         costo,   
         cod_operaio,
	    det_off_ven_corrispondenze.data_agenda)
	select
			cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         :fl_prog_new,   
         cod_corrispondenza,   
         data_corrispondenza,   
         ora_corrispondenza,   
         prog_corrispondenza,   
         intestatario,   
         data_pros_corrispondenza,   
         ora_pros_corrispondenza,   
         oggetto,   
         num_reg_lista,   
         num_versione,   
         num_edizione,   
         num_reg_lista_comp,   
         prog_liste_con_comp,   
         flag_tempo,   
         tempo_speso,   
         note,   
	    	cod_tipo_agenda,   
         cod_utente,            
         ora_agenda,   
         costo,   
         cod_operaio,
	    	data_agenda
	from det_off_ven_corrispondenze
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_off_ven=:fl_prog;
				
if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento det_off_ven_corrispondenze  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine det_off_ven_corrispondenze ------------------------------------------------------------------------------------------

//varianti_det_off_ven -------------------------------------------------------------------------------------------------------------
insert into varianti_det_off_ven
(			cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         prog_riga_off_ven,   
         cod_prodotto_padre,   
         num_sequenza,   
         cod_prodotto_figlio,   
         cod_versione,   
         cod_versione_figlio,   
         cod_prodotto,   
         cod_versione_variante,   
         cod_misura,   
         quan_tecnica,   
         quan_utilizzo,   
         dim_x,   
         dim_y,   
         dim_z,   
         dim_t,   
         coef_calcolo,   
         flag_esclusione,   
         formula_tempo,   
         des_estesa,   
         flag_materia_prima,   
         lead_time)
	select
	    	cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         :fl_prog_new,   
         cod_prodotto_padre,   
         num_sequenza,   
         cod_prodotto_figlio,   
         cod_versione,   
         cod_versione_figlio,   
         cod_prodotto,   
         cod_versione_variante,   
         cod_misura,   
         quan_tecnica,   
         quan_utilizzo,   
         dim_x,   
         dim_y,   
         dim_z,   
         dim_t,   
         coef_calcolo,   
         flag_esclusione,   
         formula_tempo,   
         des_estesa,   
         flag_materia_prima,   
         lead_time
	from varianti_det_off_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_off_ven=:fl_prog;
				
if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento varianti_det_off_ven  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine varianti_det_off_ven ------------------------------------------------------------------------------------------------------

return 1
end function

public subroutine wf_tipo_dettaglio_ven_det (datawindow ad_datawindow, commandbutton ac_prodotti_ricerca, picturebutton ap_prod_view, string as_cod_tipo_det_ven, long al_row);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio

setnull(ls_null)
select tab_tipi_det_ven.flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_tipi_det_ven.cod_tipo_det_ven = :as_cod_tipo_det_ven;

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
              exclamation!, ok!)
   return
end if

if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
	if isnull(ac_prodotti_ricerca) then
		ad_datawindow.object.b_ricerca_prodotto.enabled = true
	else
	   ac_prodotti_ricerca.enabled = true
	end if
   ls_modify = "cod_prodotto.protect='0'~t"
   ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
   ls_modify = ls_modify + "cod_versione.protect='0'~t"
   ls_modify = ls_modify + "cod_versione.background.color='16777215'~t"
   ad_datawindow.modify(ls_modify)
else
  if isnull(ac_prodotti_ricerca) then
		ad_datawindow.object.b_ricerca_prodotto.enabled = false
	else
	   ac_prodotti_ricerca.enabled = false
	end if
   if not isnull(ad_datawindow.getitemstring(al_row,"cod_prodotto")) then
      ad_datawindow.setitem(al_row,"cod_prodotto",ls_null)
   end if
   ls_modify = "cod_prodotto.protect='1'~t"
   ls_modify = ls_modify + "cod_prodotto.background.color='12632256'~t"
   ls_modify = ls_modify + "cod_versione.protect='1'~t"
   ls_modify = ls_modify + "cod_versione.background.color='12632256'~t"
   ad_datawindow.modify(ls_modify)
end if

end subroutine

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_off_ven_det_1, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco<>'S'")
f_po_loaddddw_dw(dw_det_off_ven_lista, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco<>'S'")
f_po_loaddddw_dw(dw_det_off_ven_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_off_ven_det_1, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


dw_det_off_ven_lista.set_dw_key("cod_azienda")
dw_det_off_ven_lista.set_dw_key("anno_registrazione")
dw_det_off_ven_lista.set_dw_key("num_registrazione")

setnull(il_anno_reg_richiesta)
setnull(il_num_reg_richiesta)
if s_cs_xx.parametri.parametro_s_10 = "richieste" then
	ib_richieste = true
	il_anno_reg_richiesta = s_cs_xx.parametri.parametro_d_1
	il_num_reg_richiesta = s_cs_xx.parametri.parametro_d_2
	title = title + " / RICHIESTA " + string(il_anno_reg_richiesta) + "-" + string(il_num_reg_richiesta)
end if	

setnull(s_cs_xx.parametri.parametro_s_10)
s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_2 = 0

dw_det_off_ven_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)
dw_det_off_ven_det_1.set_dw_options(sqlca, &
                                    dw_det_off_ven_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)

lw_oggetti[1] = dw_det_off_ven_lista
lw_oggetti[2] = uo_1
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])

lw_oggetti[1] = dw_det_off_ven_det_1
lw_oggetti[2] = uo_1
lw_oggetti[3] = dw_documenti
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_documenti.uof_set_management("OFFVEN", dw_det_off_ven_lista)
dw_documenti.settransobject(sqlca)
dw_documenti.uof_enabled_delete_blob()


iuo_dw_main=dw_det_off_ven_lista
cb_c_industriale.enabled = false
cb_corrispondenze.enabled = false

dw_det_off_ven_lista.is_cod_parametro_blocco_prodotto="POV"
dw_det_off_ven_det_1.is_cod_parametro_blocco_prodotto="POV"
end event

on w_det_off_ven.create
int iCurrent
call super::create
this.cb_sconti=create cb_sconti
this.uo_1=create uo_1
this.cb_c_industriale=create cb_c_industriale
this.cb_corrispondenze=create cb_corrispondenze
this.dw_documenti=create dw_documenti
this.dw_det_off_ven_lista=create dw_det_off_ven_lista
this.dw_folder=create dw_folder
this.dw_det_off_ven_det_1=create dw_det_off_ven_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_sconti
this.Control[iCurrent+2]=this.uo_1
this.Control[iCurrent+3]=this.cb_c_industriale
this.Control[iCurrent+4]=this.cb_corrispondenze
this.Control[iCurrent+5]=this.dw_documenti
this.Control[iCurrent+6]=this.dw_det_off_ven_lista
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_det_off_ven_det_1
end on

on w_det_off_ven.destroy
call super::destroy
destroy(this.cb_sconti)
destroy(this.uo_1)
destroy(this.cb_c_industriale)
destroy(this.cb_corrispondenze)
destroy(this.dw_documenti)
destroy(this.dw_det_off_ven_lista)
destroy(this.dw_folder)
destroy(this.dw_det_off_ven_det_1)
end on

event pc_delete;call super::pc_delete;if dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non cancellabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_det_off_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return 0
end if

cb_c_industriale.enabled = false
cb_corrispondenze.enabled = false
return 0
end event

event pc_modify;call super::pc_modify;if dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_det_off_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_new;call super::pc_new;if dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_det_off_ven_lista.set_dw_view(c_ignorechanges)
   dw_det_off_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event open;call super::open;iuo_condizioni_cliente = CREATE uo_condizioni_cliente
iuo_gestione_conversioni = CREATE uo_gestione_conversioni 
dw_det_off_ven_det_1.is_cod_parametro_blocco_prodotto = "POV"
dw_det_off_ven_lista.is_cod_parametro_blocco_prodotto = "POV"

end event

event close;call super::close;destroy iuo_condizioni_cliente
destroy iuo_gestione_conversioni
end event

type cb_sconti from commandbutton within w_det_off_ven
integer x = 4183
integer y = 1544
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

on clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_off_ven_lista
window_open(w_sconti, 0)

end on

type uo_1 from uo_situazione_prodotto within w_det_off_ven
integer x = 46
integer y = 1256
integer width = 3451
integer height = 248
integer taborder = 30
boolean bringtotop = true
boolean border = false
end type

on uo_1.destroy
call uo_situazione_prodotto::destroy
end on

type cb_c_industriale from commandbutton within w_det_off_ven
integer x = 3406
integer y = 1544
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C.Industriale"
end type

event clicked;window_open_parm(w_det_off_ven_stat,-1,dw_det_off_ven_lista)
end event

type cb_corrispondenze from commandbutton within w_det_off_ven
integer x = 3794
integer y = 1544
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Off_Ven"
window_open_parm(w_det_acq_ven_corr, -1, dw_det_off_ven_det_1)

end event

type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_off_ven
integer x = 3566
integer y = 160
integer width = 923
integer height = 1076
integer taborder = 120
boolean bringtotop = true
string dataobject = "d_det_off_ven_note_blob"
boolean vscrollbar = true
end type

type dw_det_off_ven_lista from uo_cs_xx_dw within w_det_off_ven
event ue_key pbm_dwnkey
event ue_postopen ( )
event post_rowfocuschanged ( )
event ue_inserisci_kit ( )
event ue_subtotale ( )
integer x = 50
integer y = 132
integer width = 4475
integer height = 1120
integer taborder = 20
string dataobject = "d_det_off_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio, &
		 ls_cod_tipo_listino_prodotto, ls_cod_det_tipo_ven, ls_cod_tipo_off_ven
long ll_i, ll_y, ll_progressivo, ll_row
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	       ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume, &
			 ld_prezzo_speciale
datetime ldt_data_registrazione

// sub-totale e reset
if keyflags = 2 and (key = KeyS! or key = KeyR!) then
	
	ls_cod_tipo_off_ven = i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_off_ven")

	if isnull(ls_cod_tipo_off_ven) or len(ls_cod_tipo_off_ven) < 1 then return
	
	if  key = KeyS!  then
	
		select cod_tipo_det_subtot
		into :is_cod_subtotale
		from tab_tipi_off_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_off_ven = :ls_cod_tipo_off_ven;
		
	elseif key = KeyR! then 
		
		select cod_tipo_det_subtot_reset
		into :is_cod_subtotale
		from tab_tipi_off_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_off_ven = :ls_cod_tipo_off_ven;
			
	else
		return 0
	end if
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la lettura delle tipologie di subtotali per il tipo offerta." + ls_cod_tipo_off_ven, sqlca)
		return
	end if
	
	il_riga_precedente = getrow()
	parent.triggerevent("pc_new")
	postevent("ue_subtotale")
		
	return 0
end if

choose case this.getcolumnname()
		
	case "sconto_1"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
				
	case "prezzo_vendita"
		
		choose case key
				
			case KeyF1!
				
				if keyflags=1 then // premuto SHIFT: memo prezzo speciale cliente
					
					ldt_data_registrazione = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
					ls_cod_cliente = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
					ls_cod_tipo_listino_prodotto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
					ls_cod_valuta = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
					ls_cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
					ls_cod_agente_2 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
					ld_cambio_ven = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
					ls_cod_tipo_det_ven = dw_det_off_ven_lista.getitemstring(dw_det_off_ven_lista.getrow(), "cod_tipo_det_ven")
					ls_cod_prodotto = dw_det_off_ven_lista.getitemstring(dw_det_off_ven_lista.getrow(), "cod_prodotto")
					ld_quantita = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(), "quan_offerta")
					ld_prezzo_speciale = dec (gettext())
					
					ll_progressivo = 0
					
					select max(progressivo)
					into   :ll_progressivo
					from   listini_vendite
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
							 cod_valuta = :ls_cod_valuta and
							 data_inizio_val = :ldt_data_registrazione;
					
					if isnull(ll_progressivo) or ll_progressivo = 0 then
						ll_progressivo = 1
					else
						ll_progressivo++
					end if

					INSERT INTO listini_vendite  
							( cod_azienda,   
							  cod_tipo_listino_prodotto,   
							  cod_valuta,   
							  data_inizio_val,   
							  progressivo,   
							  cod_cat_mer,   
							  cod_prodotto,   
							  cod_categoria,   
							  cod_cliente,   
							  des_listino_vendite,   
							  minimo_fatt_altezza,   
							  minimo_fatt_larghezza,   
							  minimo_fatt_profondita,   
							  minimo_fatt_superficie,   
							  minimo_fatt_volume,   
							  flag_sconto_a_parte,   
							  flag_tipo_scaglioni,   
							  scaglione_1,   
							  scaglione_2,   
							  scaglione_3,   
							  scaglione_4,   
							  scaglione_5,   
							  flag_sconto_mag_prezzo_1,   
							  flag_sconto_mag_prezzo_2,   
							  flag_sconto_mag_prezzo_3,   
							  flag_sconto_mag_prezzo_4,   
							  flag_sconto_mag_prezzo_5,   
							  variazione_1,   
							  variazione_2,   
							  variazione_3,   
							  variazione_4,   
							  variazione_5,   
							  flag_origine_prezzo_1,   
							  flag_origine_prezzo_2,   
							  flag_origine_prezzo_3,   
							  flag_origine_prezzo_4,   
							  flag_origine_prezzo_5 )  
					VALUES ( :s_cs_xx.cod_azienda,   
							  :ls_cod_tipo_listino_prodotto,   
							  :ls_cod_valuta,   
							  :ldt_data_registrazione,   
							  :ll_progressivo,   
							  null,   
							  :ls_cod_prodotto,   
							  null,   
							  :ls_cod_cliente,   
							  'LISTINO PERSONALE CLIENTE',   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  'S',   
							  'Q',   
							  99999999,   
							  null,   
							  null,   
							  null,   
							  null,   
							  'P',   
							  'P',   
							  'P',   
							  'P',   
							  'P',   
							  :ld_prezzo_speciale,   
							  0,   
							  0,   
							  0,   
							  0,   
							  'N',   
							  'N',   
							  'N',   
							  'N',   
							  'N' )  ;
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("APICE","Errore in fase di inserimento PREZZO SPECIALE CLIENTE~r~n" + sqlca.sqlerrtext)
						rollback;
						return
					end if
					
				end if
				
			case keyenter!
				
				this.triggerevent("pcd_save")
				this.postevent("pcd_new")
			
			case else
				
				if (key = keyF1! or key = keyF2! or key = keyF3! or key = keyF4! or key = keyF5!) and keyflags = 1 then
					setpointer(hourglass!)
					ldt_data_registrazione = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
					ls_cod_cliente = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
					ls_cod_valuta = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
					ls_cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
					ls_cod_agente_2 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
					ld_cambio_ven = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
					ls_cod_tipo_det_ven = dw_det_off_ven_lista.getitemstring(dw_det_off_ven_lista.getrow(), "cod_tipo_det_ven")
					ls_cod_prodotto = dw_det_off_ven_lista.getitemstring(dw_det_off_ven_lista.getrow(), "cod_prodotto")
					ld_quantita = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(), "quan_offerta")
					choose case key
						case keyF1!
							ls_listino = 'LI1'
						case keyF2!
							ls_listino = 'LI2'
						case keyF3!
							ls_listino = 'LI3'
						case keyF4!
							ls_listino = 'LI4'
						case keyF5!
							ls_listino = 'LI5'
						case else
							return 0
					end choose
					if ls_listino <> "LI5" then
						select stringa
						into  :ls_stringa
						from  parametri_azienda
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_parametro = :ls_listino ;
						if sqlca.sqlcode = 0 then
							f_ricerca_provvigioni(ls_stringa, &
														 ls_cod_cliente, &
														 ls_cod_valuta, &
														 ld_cambio_ven, &
														 ls_cod_prodotto, &
														 ld_quantita, &
														 ldt_data_registrazione, &
														 ls_cod_agente_1, &
														 ls_cod_agente_2, &
														 ls_cod_tipo_det_ven)
							
							iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
							iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_stringa
							iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
							iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
							iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
							iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
							iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
							iuo_condizioni_cliente.str_parametri.dim_1 = 0
							iuo_condizioni_cliente.str_parametri.dim_2 = 0
							iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
							iuo_condizioni_cliente.str_parametri.valore = 0
							iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
							iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
							iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
							iuo_condizioni_cliente.wf_condizioni_cliente()
						end if			
						setpointer(arrow!)
					else
						select prezzo_acquisto
						into   :ld_prezzo_acquisto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
						dw_det_off_ven_lista.setitem(i_rownbr, "prezzo_vendita", ld_prezzo_acquisto)
					end if					 
					setpointer(arrow!)
					
				end if
			
		end choose
		
	case "cod_prodotto", "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			//this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_off_ven_lista, "cod_prodotto")
		end if
end choose

return 0

end event

event ue_postopen();if this.getrow() > 0 then
	datawindow ld_datawindow
	
	uo_gestione_conversioni iuo_oggetto
	iuo_oggetto = create uo_gestione_conversioni
	ld_datawindow  = dw_det_off_ven_det_1
	iuo_oggetto.uof_visualizza_um(ld_datawindow, "ord_ven")
	if ib_stato_nuovo or ib_stato_modifica then 	iuo_gestione_conversioni.uof_blocca_colonne(ld_datawindow,"ord_ven")
	destroy iuo_oggetto
end if
end event

event post_rowfocuschanged();datawindow ld_datawindow
uo_gestione_conversioni iuo_oggetto
iuo_oggetto = create uo_gestione_conversioni
ld_datawindow  = dw_det_off_ven_det_1
iuo_oggetto.uof_visualizza_um(ld_datawindow, "off_ven")
if ib_stato_modifica or ib_stato_nuovo then
	iuo_oggetto.uof_blocca_colonne(ld_datawindow, "ord_ven")
end if
destroy iuo_oggetto

end event

event ue_inserisci_kit();/* EnMe 19-11-2010
Inserimento del KIT proveniente dalla distinta base.
*/

string ls_return, ls_cod_prodotto_padre, ls_cod_versione_padre, ls_stringa_sep, ls_cod_prodotto_figlio, &
		 ls_des_prodotto_figlio,ls_cod_misura_figlio, ls_cod_iva_figlio, ls_cod_tipo_det_ven, ls_cod_tipo_off_ven, &
		 ls_cod_iva,ls_cod_cliente, ls_cod_contatto, ls_flag_decimali, ls_cod_misura_mag_figlio, &
		 ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_agente_1, ls_cod_agente_2, ls_cod_valuta_testata, ls_messaggio
long 	 ll_pos, ll_len2, ll_index, ll_num_righe, ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_off_ven, &
		 ll_row, ll_i, ll_y
double ld_sconti[], ld_prezzo_da_arrotondare, ld_prezzo_arrotondato, ld_cambio_ven
decimal ld_quan_utilizzo_figlio, ld_fat_conversione_ven
dec{4}  ld_fat_conversione, ld_quantita, ld_ultimo_prezzo
datetime ldt_data_consegna, ldt_data_esenzione_iva, ldt_data_registrazione
datastore lds_componenti_kit
datastore lds_det_off_ven_lista
uo_condizioni_cliente luo_condizioni_cliente


lds_componenti_kit = create datastore
lds_componenti_kit.dataobject = "d_ds_componenti_kit"
lds_componenti_kit.settransobject(sqlca)

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione  = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
ls_cod_tipo_off_ven   = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tipo_off_ven")
ls_cod_prodotto_padre = getitemstring(getrow(), "cod_prodotto")
ls_cod_versione_padre = getitemstring(getrow(), "cod_versione")
ldt_data_consegna		 = i_parentdw.getitemdatetime(i_parentdw.getrow(), "data_consegna")
ldt_data_registrazione= i_parentdw.getitemdatetime(i_parentdw.getrow(), "data_registrazione")
ls_cod_cliente        = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_cliente")
ls_cod_contatto       = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_contatto")
ls_cod_tipo_listino_prodotto= i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tipo_listino_prodotto")
ls_cod_agente_1 = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_agente_1")
ls_cod_agente_2 = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_agente_2")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_valuta")
ld_cambio_ven = i_parentdw.getitemnumber(i_parentdw.getrow(), "cambio_ven")


ll_num_righe = lds_componenti_kit.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto_padre, ls_cod_versione_padre)

select cod_tipo_det_ven
into   :ls_cod_tipo_det_ven
from   tab_tipi_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_off_ven = :ls_cod_tipo_off_ven;

i_extendmode= false
ib_stato_modifica=false
ib_stato_nuovo=false
dw_det_off_ven_lista.setredraw(false)
dw_det_off_ven_det_1.setredraw(false)

ll_prog_riga_off_ven = 0

select max(prog_riga_off_ven)
into   :ll_prog_riga_off_ven
from   det_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione;
		 
if isnull(ll_prog_riga_off_ven) or ll_prog_riga_off_ven < 1 then
	ll_prog_riga_off_ven = 10
end if

for ll_index = 1 to ll_num_righe
	
	ls_cod_prodotto_figlio = lds_componenti_kit.getitemstring(ll_index, "distinta_cod_prodotto_figlio")
	ls_cod_misura_figlio = lds_componenti_kit.getitemstring(ll_index, "distinta_cod_misura")
	ld_quan_utilizzo_figlio = lds_componenti_kit.getitemdecimal(ll_index, "distinta_quan_utilizzo")
	ls_des_prodotto_figlio = lds_componenti_kit.getitemstring(ll_index, "anag_prodotti_des_prodotto")
	ls_cod_iva_figlio = lds_componenti_kit.getitemstring(ll_index, "anag_prodotti_cod_iva")
			
	if ls_cod_misura_figlio = "" then setnull(ls_cod_misura_figlio)
	if ls_cod_iva_figlio    = "" then setnull(ls_cod_iva_figlio)
	
	if ll_index = 1 then deleterow(getrow() )
	
	ll_row = insertrow(0)
	
	if ll_index > 1 or not isnull(ll_prog_riga_off_ven) then ll_prog_riga_off_ven = ll_prog_riga_off_ven + 10
	
	setitem(ll_row, "prog_riga_off_ven", ll_prog_riga_off_ven)
	setitem(ll_row, "cod_tipo_det_ven", ls_cod_tipo_det_ven)
	
	
   if not isnull(ls_cod_cliente) then
      select cod_iva,
             data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_clienti
      where  cod_azienda = :s_cs_xx.cod_azienda and 
             cod_cliente = :ls_cod_cliente;
   else
      select cod_iva,
             data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_contatti
      where  cod_azienda = :s_cs_xx.cod_azienda and 
             cod_contatto = :ls_cod_contatto;
   end if
   
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and not isnull(ls_cod_iva)   and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
         this.setitem(ll_row, "cod_iva", ls_cod_iva)
		else
			if not isnull(ls_cod_iva_figlio) then
				setitem(ll_row, "cod_iva", ls_cod_iva_figlio)
			else
				select cod_iva  
				into   :ls_cod_iva  
				from   tab_tipi_det_ven  
				where  cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				if sqlca.sqlcode = 0 then
					this.setitem(ll_row, "cod_iva", ls_cod_iva)
				end if
			end if
		end if
	end if
	
	setitem(ll_row, "cod_prodotto", ls_cod_prodotto_figlio)
	setitem(ll_row, "des_prodotto", ls_des_prodotto_figlio)
	setitem(ll_row, "quan_offerta", ld_quan_utilizzo_figlio)
	setitem(ll_row, "cod_misura",   ls_cod_misura_figlio)
	

	setitem(ll_row, "flag_doc_suc_det", "I")
	setitem(ll_row, "flag_st_note_det", "I")	
	
	luo_condizioni_cliente = create uo_condizioni_cliente
	
	luo_condizioni_cliente.str_output.sconti = ld_sconti[]

	
	if mid(f_flag_controllo(),1,1) = "S" then
		luo_condizioni_cliente.ib_setitem=false
		luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
		luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
		iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
		if mid(f_flag_controllo(),7,1) = "S" then
			luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_consegna
		else
			luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
		end if
		luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
		luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto_figlio
		luo_condizioni_cliente.str_parametri.dim_1 = 0
		luo_condizioni_cliente.str_parametri.dim_2 = 0
		luo_condizioni_cliente.str_parametri.quantita = ld_quantita
		luo_condizioni_cliente.str_parametri.valore = 0
		luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
		luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
		luo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
		luo_condizioni_cliente.wf_condizioni_cliente()	
		
		for ll_i = 1 to upperbound(luo_condizioni_cliente.str_output.variazioni)
			
			ld_prezzo_da_arrotondare = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
			if luo_condizioni_cliente.uof_arrotonda_prezzo( ld_prezzo_da_arrotondare, ls_cod_valuta, ld_prezzo_arrotondato, ls_messaggio) = -1 then
				g_mb.messagebox("Errore arrotondamento prezzo", ls_messaggio)
			else
				if ld_fat_conversione_ven <> 1 and not isnull(ld_fat_conversione_ven) and ld_fat_conversione_ven <> 0 then
					setitem(ll_row, "prezzo_um", ld_prezzo_arrotondato / ld_fat_conversione_ven)
					setitem(ll_row, "prezzo_vendita", ld_prezzo_arrotondato)
				else
					setitem(ll_row, "prezzo_vendita", ld_prezzo_arrotondato)
				end if
			end if
				
		next
		
		for ll_i = 1 to upperbound(luo_condizioni_cliente.str_output.sconti)
			setitem(ll_row, "sconto_" + string(ll_i), 0)
			if luo_condizioni_cliente.str_output.sconti[ll_i] > 0 and not isnull(luo_condizioni_cliente.str_output.sconti[ll_i]) then
				ll_y = ll_y + 1
				if ll_y > 10 then exit
				setitem(ll_row, "sconto_" + string(ll_i), luo_condizioni_cliente.str_output.sconti[ll_i])
			end if				
		next
		
		destroy luo_condizioni_cliente
		
	end if
			
next


destroy lds_componenti_kit

i_extendmode= true
ib_stato_modifica = true
dw_det_off_ven_lista.setredraw(true)
dw_det_off_ven_det_1.setredraw(true)
end event

event ue_subtotale();string ls_cod_tipo_off_ven, ls_cod_det_tipo_ven
long ll_row, ll_prog_riga_off_ven

if isnull(is_cod_subtotale) or len(is_cod_subtotale) < 1 then return

ll_row = getrow()

setrow(ll_row)
if  wf_get_num_riga(ll_prog_riga_off_ven) then setitem(ll_row, "prog_riga_off_ven", ll_prog_riga_off_ven)
setitem(ll_row, "cod_tipo_det_ven", is_cod_subtotale)
//event itemchanged(ll_row, object.cod_tipo_det_ven, is_cod_subtotale)

SetColumn("des_prodotto")
postevent("rowfocuschanged")
end event

event pcd_new;call super::pcd_new;setitem(getrow(),"flag_stampa_settimana","N")

if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_ven, ls_cod_tipo_off_ven, ls_flag_tipo_det_ven, &
          ls_modify, ls_null, ls_cod_agente_1, ls_cod_agente_2, ls_cod_cliente, &
          ls_cod_contatto, ls_cod_iva
   long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven
   datetime ldt_data_consegna, ldt_data_esenzione_iva, ldt_data_registrazione
	double ld_quan_proposta

   setnull(ls_null)

	dw_det_off_ven_lista.object.cod_tipo_det_ven.protect = 0

   ll_anno_registrazione = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
   ll_num_registrazione = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
   ldt_data_registrazione = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_contatto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_contatto")
   ldt_data_consegna = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_consegna")

   select max(det_off_ven.prog_riga_off_ven)
   into   :ll_prog_riga_off_ven
   from   det_off_ven
   where  det_off_ven.cod_azienda = :s_cs_xx.cod_azienda and
          det_off_ven.anno_registrazione = :ll_anno_registrazione and
          det_off_ven.num_registrazione = :ll_num_registrazione;

   if isnull(ll_prog_riga_off_ven) then
      dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "prog_riga_off_ven", 10)
   else
      dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "prog_riga_off_ven", ll_prog_riga_off_ven + 10)
   end if

   ls_cod_tipo_off_ven = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_off_ven")

   select tab_tipi_off_ven.cod_tipo_det_ven
   into   :ls_cod_tipo_det_ven
   from   tab_tipi_off_ven
   where  tab_tipi_off_ven.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_off_ven.cod_tipo_off_ven = :ls_cod_tipo_off_ven;
   
   if sqlca.sqlcode = 0 then
      dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
      if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
         select tab_tipi_det_ven.cod_iva  
         into   :ls_cod_iva  
         from   tab_tipi_det_ven  
         where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
         end if
      end if

   else
      ls_cod_tipo_det_ven = ls_null
   end if

//-------------------------------------- Modifica Nicola ---------------------------------------------------------------

	select quan_default
	  into :ld_quan_proposta
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda 
	   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_det_ven " + sqlca.sqlerrtext)
		return 0
	end if
	
	if isnull(ld_quan_proposta) then ld_quan_proposta = 0

//--------------------------------------- Fine Modifica ----------------------------------------------------------------

   dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "quan_offerta", ld_quan_proposta)
   dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "data_consegna", ldt_data_consegna)
   
   if not isnull(ls_cod_cliente) then
      select anag_clienti.cod_iva,
             anag_clienti.data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_clienti
      where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_clienti.cod_cliente = :ls_cod_cliente;
   else
      select anag_contatti.cod_iva,
             anag_contatti.data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_contatti
      where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_contatti.cod_contatto = :ls_cod_contatto;
   end if
   
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and &
         (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
         this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
      end if
   end if
   
   ls_modify = "cod_tipo_det_ven.protect='0'~t"
   dw_det_off_ven_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_ven.background.color='16777215'~t"
   dw_det_off_ven_det_1.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_ven) then
      dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
      ld_datawindow = dw_det_off_ven_det_1
      ls_cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
      ls_cod_agente_2 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
	 setnull(lc_prodotti_ricerca)
	 setnull(lp_prod_view)
      f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)
      ld_datawindow = dw_det_off_ven_lista
      f_tipo_dettaglio_ven_lista(ld_datawindow, ls_cod_tipo_det_ven)
   else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_misura.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_misura.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "fat_conversione_ven.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "fat_conversione_ven.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "quan_offerta.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "quan_offerta.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "prezzo_vendita.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "prezzo_vendita.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_1.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_1.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_2.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_2.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_iva.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_iva.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_1.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_1.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_2.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_2.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "data_consegna.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "data_consegna.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_centro_costo.protect='1'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_centro_costo.background.color='12632256'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
   end if

   cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
	
	dw_det_off_ven_det_1.setitem(dw_det_off_ven_lista.getrow(), "flag_doc_suc_det", "I")
	dw_det_off_ven_det_1.setitem(dw_det_off_ven_lista.getrow(), "flag_st_note_det", "I")	
	
end if
return 0
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   dw_det_off_ven_det_1.object.b_ricerca_prodotto.enabled = false
   cb_sconti.enabled = false
	
	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
			cb_c_industriale.enabled=true
			cb_corrispondenze.enabled = true
		end if
	else
		cb_c_industriale.enabled=false
		cb_corrispondenze.enabled = false
	end if
end if
return 0
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	commandbutton 	lc_prodotti_ricerca
	picturebutton 		lp_prod_view
	
	string					ls_cod_cliente, ls_cod_contatto, ls_cod_iva, ls_cod_tipo_det_ven, ls_modify, &
							ls_cod_prodotto, ls_cod_misura_mag
			
	datetime 			ldt_data_registrazione, ldt_data_esenzione_iva
	long					ll_row

	ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
	ldt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")

	ll_row = this.getrow()

	if ll_row > 0 then
		
		ls_cod_prodotto = this.getitemstring(ll_row,"cod_prodotto")		

		select anag_prodotti.cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		uo_1.uof_aggiorna(ls_cod_prodotto)

		f_po_LoadDDDW_DW(dw_det_off_ven_det_1,"cod_versione",sqlca,&
									  "distinta_padri","cod_versione","des_versione",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(ll_row,"cod_prodotto") + "'")
	
		if ls_cod_misura_mag <> this.getitemstring(ll_row,"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_off_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(ll_row,"fat_conversione_ven"))) + " " + this.getitemstring(ll_row,"cod_misura") + ")'")
		else
			dw_det_off_ven_det_1.modify("st_fattore_conv.text=''")		
		end if
		
		if not isnull(ls_cod_cliente) then
			select anag_clienti.cod_iva,
					 anag_clienti.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_clienti.cod_cliente = :ls_cod_cliente;
		else
			ls_cod_contatto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_contatto")
	
			select anag_contatti.cod_iva,
					 anag_contatti.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_contatti
			where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_contatti.cod_contatto = :ls_cod_contatto;
		end if
	
		if sqlca.sqlcode = 0 then
			if ls_cod_iva <> "" and ldt_data_esenzione_iva <= ldt_data_registrazione then
				f_po_loaddddw_dw(dw_det_off_ven_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
			else
				f_po_loaddddw_dw(dw_det_off_ven_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			end if
		end if
	
		dw_documenti.uof_retrieve_blob(ll_row)
	
		datawindow ld_datawindow
//		uo_gestione_conversioni iuo_oggetto
//		iuo_oggetto = create uo_gestione_conversioni
//		ld_datawindow  = dw_det_off_ven_det_1
//		iuo_oggetto.uof_visualizza_um(ld_datawindow, "off_ven")

		if ib_stato_modifica or ib_stato_nuovo then
			ls_cod_tipo_det_ven = dw_det_off_ven_det_1.getitemstring(ll_row, "cod_tipo_det_ven")
			ld_datawindow = dw_det_off_ven_det_1
			setnull(lc_prodotti_ricerca)
			 setnull(lp_prod_view)
			//f_tipo_dettaglio_ven_det(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven)
			wf_tipo_dettaglio_ven_det(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven, ll_row)
			
			ld_datawindow = dw_det_off_ven_lista
			f_tipo_dettaglio_ven_lista(ld_datawindow, ls_cod_tipo_det_ven)
			
			
			
//			iuo_oggetto.uof_blocca_colonne(ld_datawindow, "ord_ven")
		end if
//		destroy iuo_oggetto
		postevent("post_rowfocuschanged")
	end if
	
	if ib_stato_nuovo then
      ls_modify = "cod_tipo_det_ven.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_det_ven.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_off_ven_det_1.modify(ls_modify)
	end if


end if
end event

event updatestart;call super::updatestart;if i_extendmode then
long 			ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_riga_off_ven
double 		ld_sconto_testata
string 		ls_cod_pagamento, ls_cod_valuta, ls_tabella, ls_quan_documento, ls_cod_prodotto, ls_tot_val_documento, &
				ls_nome_prog, ls_aspetto_beni, ls_des_imballo, ls_nota_testata, ls_stampa_piede, ls_nota_video,ls_null, ls_nota_dettaglio
datetime	ldt_data_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ld_sconto_testata = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "sconto")
ls_cod_pagamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_pagamento")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ls_aspetto_beni = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "aspetto_beni")
ldt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")
ll_prog_riga_off_ven = getitemnumber(getrow(),"prog_riga_off_ven")
ls_nota_dettaglio = getitemstring(getrow(),"nota_dettaglio")
ls_tabella = "tes_off_ven"
ls_quan_documento = "quan_offerta"
ls_tot_val_documento = "tot_val_offerta"

setnull(ls_null)

dw_det_off_ven_lista.change_dw_current()

dw_det_off_ven_det_1.object.b_ricerca_prodotto.enabled = false
cb_sconti.enabled = false

for ll_i = 1 to this.rowcount()
	ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto")
	select des_imballo
	 into  :ls_des_imballo
	 from  tab_imballi
	 where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_imballo = (select cod_imballo 
								 from	  anag_prodotti
								 where  cod_azienda = :s_cs_xx.cod_azienda and
										  cod_prodotto = :ls_cod_prodotto);

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Errore Durante l'Estrazione Descrizione Imballo.", &
						exclamation!, ok!)
		return 1
	end if

	if (pos(ls_aspetto_beni, ls_des_imballo) = 0 or isnull(ls_aspetto_beni) or ls_aspetto_beni = "") and &
		ls_des_imballo <> "" and not isnull(ls_des_imballo) then
		if len(ls_aspetto_beni) > 0 then
			ls_aspetto_beni = ls_aspetto_beni + " - " + ls_des_imballo
		else
			ls_aspetto_beni = ls_des_imballo
		end if
	end if
	
	if getitemstatus(ll_i,0,Primary!) = NewModified! then
		if not isnull(ls_cod_prodotto) then
			if guo_functions.uof_get_note_fisse(ls_null, ls_null, ls_cod_prodotto, "OFF_VEN", ls_null, ldt_data_registrazione , ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_testata) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_nota_testata},"~r~n")
				if len(ls_stampa_piede) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_piede},"~r~n")
				if len(ls_nota_video) > 0 then g_mb.warning( "NOTA FISSA", ls_nota_video)
			end if
		end if	
		
	end if
next

update tes_off_ven
set	 aspetto_beni = :ls_aspetto_beni
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
		
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Errore Durante l'Aggiornamento Imballo Offerta.", exclamation!, ok!)
	return 1
end if

ls_tabella = "det_off_ven_stat"
ls_nome_prog = "prog_riga_off_ven"
	
for ll_i = 1 to this.deletedcount()
	ll_prog_riga_off_ven = this.getitemnumber(ll_i, "prog_riga_off_ven", delete!, true)		
	if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione,ls_nome_prog,ll_prog_riga_off_ven) = -1 then
		return 1
	end if
	
	delete varianti_det_off_ven
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_off_ven =: ll_prog_riga_off_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice","Errore durante cancellazione varianti offerte",stopsign!)
		return 1
	end if
	
	delete det_off_ven_note
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_off_ven =: ll_prog_riga_off_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice","Errore durante cancellazione note offerte",stopsign!)
		return 1
	end if
	
	delete 	det_off_ven_corrispondenze
	where  	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione and
				prog_riga_off_ven =: ll_prog_riga_off_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice","Errore durante cancellazione corrispondenze offerte",stopsign!)
		return 1
	end if
	
	delete det_off_ven_conf_variabili	
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione = :ll_anno_registrazione
	and    num_registrazione = :ll_num_registrazione and
			 prog_riga_off_ven =: ll_prog_riga_off_ven;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore durante cancellazione variabili configuratore",stopsign!)
		return 1
	end if
	
	delete comp_det_off_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione = :ll_anno_registrazione
	and    num_registrazione = :ll_num_registrazione and
			 prog_riga_off_ven =: ll_prog_riga_off_ven;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore durante cancellazione variabili configuratore",stopsign!)
		return 1
	end if

next
end if

wf_ricalcola()
end event

event updateend;call super::updateend;this.postevent("pcd_view")

if isvalid(s_cs_xx.parametri.parametro_w_off_ven) then 
	s_cs_xx.parametri.parametro_w_off_ven.postevent("pc_menu_calcola")
end if

/*
long   ll_i



if not isvalid(s_cs_xx.parametri.parametro_w_off_ven) then return 0

for ll_i = 1 to upperbound(s_cs_xx.parametri.parametro_w_off_ven.control)	
   if s_cs_xx.parametri.parametro_w_off_ven.control[ll_i].classname() = "cb_calcola" then
      s_cs_xx.parametri.parametro_w_off_ven.control[ll_i].postevent("clicked")
      exit
   end if	
next
*/
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
	
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
	
   if isnull(this.getitemnumber(ll_i, "anno_reg_richiesta")) or this.getitemnumber(ll_i, "anno_reg_richiesta") = 0 then
      this.setitem(ll_i, "anno_reg_richiesta", il_anno_reg_richiesta)
   end if
	
   if isnull(this.getitemnumber(ll_i, "num_reg_richiesta")) or this.getitemnumber(ll_i, "num_reg_richiesta") = 0 then
      this.setitem(ll_i, "num_reg_richiesta", il_num_reg_richiesta)
   end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_rag_soc_1
long ll_errore, ll_anno_registrazione, ll_num_registrazione


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente ;
if sqlca.sqlcode = 0 then
	parent.title = "Offerta " + &
						string(ll_anno_registrazione,"####") + "/" + &
						string(ll_num_registrazione) + "  Cliente: " + ls_rag_soc_1
end if

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_ven, ls_modify, ls_cod_agente_1, ls_cod_agente_2
	
	dw_det_off_ven_lista.object.cod_tipo_det_ven.protect = 1
	dw_det_off_ven_lista.object.cod_tipo_det_ven.background.color='12632256'


   ls_modify = "cod_tipo_det_ven.protect='1'~t"
   dw_det_off_ven_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_ven.background.color='12632256'~t"
   dw_det_off_ven_det_1.modify(ls_modify)
   ls_cod_tipo_det_ven = dw_det_off_ven_det_1.getitemstring(dw_det_off_ven_det_1.getrow(), "cod_tipo_det_ven")
   ld_datawindow = dw_det_off_ven_det_1
   ls_cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
	setnull(lc_prodotti_ricerca)
	setnull(lp_prod_view)
   f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)

   cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
end if
return 0
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i1,ll_prog_riga_off_ven
	string  ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_off_ven,ls_cod_tipo_det_ven,ls_test
	integer li_risposta

	ll_i = i_parentdw.i_selectedrows[1]
	ll_i1 = getrow()

	ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_off_ven = getitemnumber(ll_i1, "prog_riga_off_ven")
	ls_cod_tipo_det_ven = getitemstring(ll_i1, "cod_tipo_det_ven")
	ls_cod_tipo_off_ven = i_parentdw.getitemstring(ll_i, "cod_tipo_off_ven")
	
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_off_ven
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_tipo_off_ven = :ls_cod_tipo_off_ven;

	select cod_azienda
	into   :ls_test
	from 	 det_off_ven_stat
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_off_ven=:ll_prog_riga_off_ven;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione(ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_det_ven,ll_num_registrazione,ll_anno_registrazione,ll_prog_riga_off_ven,3)
		if li_risposta=-1 then
			g_mb.messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
		end if
	end if
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	datawindow			ld_datawindow
	commandbutton	lc_prodotti_ricerca
	picturebutton		lp_prod_view
	string				ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_des_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, &
						ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_messaggio, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_cod_contatto, &
						ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_colonna_sconto, ls_flag_esplodi_in_doc, ls_flag_blocco, &
						ls_nota_testata, ls_stampa_piede, ls_nota_video
						
	double				ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_variazioni[], ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
						ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume, ld_quan_proposta
						
	datetime			ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	long				ll_riga_origine, ll_i, ll_y, ll_z, ll_num_confezione, ll_num_pezzi_confezioni,li_ret


   setnull(ls_null)

   ls_cod_tipo_listino_prodotto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_contatto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_contatto")
   ls_cod_valuta = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_ven = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
   ls_cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
	if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
		ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
	else
		ldt_data_consegna = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	end if
   
   choose case i_colname
		case "prog_riga_off_ven"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_off_ven") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_off_ven", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_off_ven", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
      case "cod_tipo_det_ven"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "quan_offerta.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "quan_offerta.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
   
         ld_datawindow = dw_det_off_ven_det_1
			setnull(lc_prodotti_ricerca)
			setnull(lp_prod_view)
         f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
         ld_datawindow = dw_det_off_ven_lista
         f_tipo_dettaglio_ven_lista(this, i_coltext)

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_ven.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_ven  
            where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "nota_dettaglio", ls_null)
			
//-------------------------------------- Modifica Nicola ---------------------------------------------------------------

			select quan_default
			  into :ld_quan_proposta
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda 
				and cod_tipo_det_ven = :i_coltext;
				
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_det_ven " + sqlca.sqlerrtext)
				return
			end if
			
			if isnull(ld_quan_proposta) then ld_quan_proposta = 0
			dw_det_off_ven_lista.setitem(dw_det_off_ven_lista.getrow(), "quan_offerta", ld_quan_proposta)			

//--------------------------------------- Fine Modifica ----------------------------------------------------------------			
			
      case "cod_prodotto"
			
		if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
		
		uo_fido_cliente luo_fido_cliente
		
         select		cod_misura_mag,
					cod_misura_ven,
					fat_conversione_ven,
					flag_decimali,
					cod_iva,
					pezzi_collo,
					des_prodotto,
					flag_blocco
         into	:ls_cod_misura_mag,
				:ls_cod_misura,
				:ld_fat_conversione,
				:ls_flag_decimali,
				:ls_cod_iva,
				:ll_num_pezzi_confezioni,
				:ls_des_prodotto,
				:ls_flag_blocco
         from   anag_prodotti
         where  cod_azienda = :s_cs_xx.cod_azienda and 
                cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
				
			if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "OFF_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
				
			this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
			this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)

			if ld_fat_conversione <> 0 then
				this.setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
			else
				ld_fat_conversione = 1
				this.setitem(i_rownbr, "fat_conversione_ven", 1)
            end if
            if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				uo_1.uof_aggiorna(i_coltext)
				uo_1.uof_ultimo_prezzo(ls_cod_cliente, i_coltext)
			else
				this.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_off_ven_lista, "cod_prodotto")
				return
			end if
			dw_det_off_ven_lista.postevent("ue_postopen")

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_off_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			
         if not isnull(ls_cod_cliente) then
            select anag_clienti.cod_iva,
                   anag_clienti.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_clienti
            where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_clienti.cod_cliente = :ls_cod_cliente;
         else
            select anag_contatti.cod_iva,
                   anag_contatti.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_contatti
            where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_contatti.cod_contatto = :ls_cod_contatto;
         end if

         if sqlca.sqlcode = 0 then
            if ls_cod_iva <> "" and &
               (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
            end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_offerta")
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_offerta", ld_quantita)
         end if
			if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_off_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_off_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			f_PO_LoadDDDW_DW(dw_det_off_ven_det_1,"cod_versione",sqlca,&
   		              	  "distinta_padri","cod_versione","des_versione",&
	      		           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")

			uo_default_prodotto luo_default_prodotto
			luo_default_prodotto = CREATE uo_default_prodotto
			if luo_default_prodotto.uof_flag_gen_commessa(i_coltext) = 0 then
				setitem(getrow(),"cod_versione",luo_default_prodotto.is_cod_versione)
				setitem(getrow(),"flag_gen_commessa",luo_default_prodotto.is_flag_gen_commessa)
			end if
			destroy luo_default_prodotto
			
			dw_det_off_ven_det_1.postevent("ue_ricalcola_colonne")
			
		case "quan_offerta"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_offerta", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if
         ld_quantita = double(i_coltext)
			if mid(f_flag_controllo(),2,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "data_consegna"
			if mid(f_flag_controllo(),4,1) = "S" then
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(i_rownbr, "quan_offerta")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(i_coltext)
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			if mid(f_flag_controllo(),9,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_offerta")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "prezzo_vendita"
			if mid(f_flag_controllo(),3,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_offerta")
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
		case "cod_versione"

			// Gestione del KIT prodotto.
			if not isnull(i_coltext) and len(i_coltext) > 0 then
			
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ls_flag_esplodi_in_doc = "N"
				
				select flag_esplodi_in_doc
				into   :ls_flag_esplodi_in_doc
				from   distinta_padri
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 cod_versione = :i_coltext and
						 flag_blocco = 'N';
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore in ricerca ed esplosione KIT prodotti", Information!)
				end if
				
				if sqlca.sqlcode = 0 and ls_flag_esplodi_in_doc ="S" then
					// si tratta di un KIT di prodotti e quindi mi fermo per inserire il KIT
					postevent("ue_inserisci_kit")
					return 0
				end if
				
			end if
									
	end choose
end if

end event

event buttonclicked;call super::buttonclicked;long   ll_i

choose case dwo.name
		
	case "b_duplica"
		if dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "flag_evasione") = "A" then
			if wf_duplica(row) then
				commit;
				
				if isvalid(s_cs_xx.parametri.parametro_w_off_ven) then 
					s_cs_xx.parametri.parametro_w_off_ven.postevent("pc_menu_calcola")
				end if
				
				g_mb.show("Riga di dettaglio duplicata con successo!")
				parent.postevent("pc_retrieve")
			end if
			
			setpointer(Arrow!)
		else
			 g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", exclamation!, ok!)
		end if
		
end choose
end event

type dw_folder from u_folder within w_det_off_ven
integer x = 23
integer y = 20
integer width = 4530
integer height = 1500
integer taborder = 40
end type

type dw_det_off_ven_det_1 from uo_cs_xx_dw within w_det_off_ven
event ue_key pbm_dwnkey
event ue_ricalcola_colonne ( )
integer x = 50
integer y = 160
integer width = 3451
integer height = 1076
integer taborder = 110
string dataobject = "d_det_off_ven_det_1"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo
datetime ldt_data_registrazione


choose case this.getcolumnname()

// MA CHI CAGA SUL CODICE????
//	case "cod_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			this.change_dw_current()
//			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//			s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
//			s_cs_xx.parametri.parametro_tipo_ricerca = 1
//			if not isvalid(w_prodotti_ricerca) then
//				window_open(w_prodotti_ricerca, 0)
//			end if
//			w_prodotti_ricerca.show()		
//		end if
//	case "des_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			this.change_dw_current()
//			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//			s_cs_xx.parametri.parametro_pos_ricerca = this.gettext()
//			s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
//			s_cs_xx.parametri.parametro_tipo_ricerca = 2
//			if not isvalid(w_prodotti_ricerca) then
//				window_open(w_prodotti_ricerca, 0)
//			end if
//			w_prodotti_ricerca.show()		
//		end if

	case "nota_dettaglio"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = dw_det_off_ven_det_1.getitemstring(this.getrow(),"cod_prodotto")
			dw_det_off_ven_det_1.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_2)
			window_open(w_prod_note_ricerca, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_2) then
				this.setcolumn("nota_dettaglio")
				this.settext(this.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
			end if
		end if
		
	case "cod_prodotto", "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			//this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_off_ven_det_1, "cod_prodotto")
		end if
		
	case "prezzo_vendita"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_7 = "V"
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(), "cod_cliente")
			s_cs_xx.parametri.parametro_s_10 = "F"
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			//***claudia moficare window
			if isvalid(w_det_ord_ven_storico) then
				w_det_ord_ven_storico.show()
			else
				window_open(w_det_ord_ven_storico, 0)
			end if
		end if
		case "prezzo_um"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_7 = "U"
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(), "cod_cliente")
			s_cs_xx.parametri.parametro_s_10 = "F"
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			//***claudia moficare window
			if isvalid(w_det_ord_ven_storico) then
				w_det_ord_ven_storico.show()
			else
				window_open(w_det_ord_ven_storico, 0)
			end if
		end if
		
end choose
return 0
end event

event ue_ricalcola_colonne;iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_off_ven_det_1, "off_ven", "prezzo_doc", &
																dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quan_offerta"), &
																dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_vendita"), &
																dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quantita_um"), &
																dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_um"), &
																dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"fat_conversione_ven"), &
																dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta"))

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	datawindow			ld_datawindow
	commandbutton	lc_prodotti_ricerca
	picturebutton		lp_prod_view
	
	string				ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_flag_prezzo, ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, &
						ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_messaggio, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_cod_contatto, &
						ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_colonna_sconto, ls_des_prodotto, ls_flag_blocco, ls_nota_testata, ls_stampa_piede, ls_nota_video
						
   double				ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_variazioni[], ld_ultimo_prezzo, ld_num_confezioni, ld_num_pezzi, ll_sconti[], ll_maggiorazioni[], &
						ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume
						
	datetime			ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	
	long				ll_riga_origine, ll_i, ll_y, ll_z

   setnull(ls_null)
   ls_cod_tipo_listino_prodotto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_contatto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_contatto")
   ls_cod_valuta = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_ven = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
   ls_cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
	if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
		ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
	else
		ldt_data_consegna = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	end if
   
   choose case i_colname
		case "prog_riga_off_ven"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_off_ven") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_off_ven", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_off_ven", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
			
      case "cod_tipo_det_ven"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_versione.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "fat_conversione_ven.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "quan_offerta.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "quan_offerta.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "prezzo_vendita.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_1.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "provvigione_2.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "data_consegna.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.protect='0'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
         ls_modify = "cod_centro_costo.background.color='16777215'~t"
         dw_det_off_ven_det_1.modify(ls_modify)
   
         ld_datawindow = dw_det_off_ven_det_1
		setnull(lp_prod_view)
		setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
         ld_datawindow = dw_det_off_ven_lista
         f_tipo_dettaglio_ven_lista(ld_datawindow, i_coltext)
         if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
         end if

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_ven.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_ven  
            where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "nota_dettaglio", ls_null)
			
      case "cod_prodotto"
		if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
		
         select		cod_misura_mag,
					cod_misura_ven,
					fat_conversione_ven,
					flag_decimali,
					cod_iva,
					des_prodotto,
					flag_blocco
         into	:ls_cod_misura_mag,
				:ls_cod_misura,
				:ld_fat_conversione,
				:ls_flag_decimali,
				:ls_cod_iva,
				:ls_des_prodotto,
				:ls_flag_blocco
         from   anag_prodotti
         where	anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
				
			if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "OFF_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
				
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
            this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)

				dw_det_off_ven_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_off_ven_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")

            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
            else
					ld_fat_conversione = 1
               this.setitem(i_rownbr, "fat_conversione_ven", 1)
            end if
            if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				uo_1.uof_aggiorna(i_coltext)
				uo_1.uof_ultimo_prezzo(ls_cod_cliente, i_coltext)
			else
				dw_det_off_ven_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_off_ven_det_1.modify("st_quantita.text='Quantità:'")
				
				this.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto( dw_det_off_ven_det_1, "cod_prodotto")
				
				return
			end if

			dw_det_off_ven_lista.postevent("ue_postopen")

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_off_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			
         if not isnull(ls_cod_cliente) then
            select anag_clienti.cod_iva,
                   anag_clienti.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_clienti
            where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_clienti.cod_cliente = :ls_cod_cliente;
         else
            select anag_contatti.cod_iva,
                   anag_contatti.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_contatti
            where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_contatti.cod_contatto = :ls_cod_contatto;
         end if

         if sqlca.sqlcode = 0 then
            if ls_cod_iva <> "" and &
               (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
            end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_offerta")
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_offerta", ld_quantita)
         end if
			if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_off_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_off_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			f_PO_LoadDDDW_DW(dw_det_off_ven_det_1,"cod_versione",sqlca,&
   		              	  "distinta_padri","cod_versione","des_versione",&
	      		           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")

			uo_default_prodotto luo_default_prodotto
			luo_default_prodotto = CREATE uo_default_prodotto
			if luo_default_prodotto.uof_flag_gen_commessa(i_coltext) = 0 then
				setitem(getrow(),"cod_versione",luo_default_prodotto.is_cod_versione)
				setitem(getrow(),"flag_gen_commessa",luo_default_prodotto.is_flag_gen_commessa)
			end if
			destroy luo_default_prodotto
			
			postevent("ue_ricalcola_colonne")
			
		case "cod_misura"
			dw_det_off_ven_lista.postevent("ue_postopen")

		case "fat_conversione_ven"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_off_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_off_ven_det_1, "off_ven", "fattore", &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quan_offerta"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_um"), &
			              												double(i_coltext), &
																			ls_cod_valuta)

case "quan_offerta"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_offerta", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if
			ld_quantita = double(i_coltext)
			if mid(f_flag_controllo(),2,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_off_ven_det_1, "off_ven", "quan_doc", &
			              												ld_quantita, &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
																			  
		case "data_consegna"
			if mid(f_flag_controllo(),3,1) = "S" then
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(i_rownbr, "quan_offerta")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(i_coltext)
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "num_confezioni"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_offerta")
			ld_num_confezioni = double(i_coltext)
			ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi * ld_num_confezioni
						this.setitem(i_rownbr, "quan_offerta", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(i_rownbr, "quan_offerta", ld_quantita)
					end if
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if		
			if mid(f_flag_controllo(),6,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "num_pezzi_confezione"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_offerta")
			ld_num_pezzi = double(i_coltext)
			ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi * ld_num_confezioni
						this.setitem(i_rownbr, "quan_offerta", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(i_rownbr, "quan_offerta", ld_quantita)
					end if
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if				
			if mid(f_flag_controllo(),5,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			if mid(f_flag_controllo(),9,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_offerta")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "prezzo_vendita"
			if mid(f_flag_controllo(),3,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_offerta")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_off_ven_det_1, "off_ven", "prezzo_doc", &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quan_offerta"), &
			              												double(i_coltext), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
																			
		case "quantita_um"  
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_off_ven_det_1, "off_ven", "quan_um", &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quan_offerta"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_vendita"), &
			              												double(i_coltext), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
		case "prezzo_um"  
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_off_ven_det_1, "off_ven", "prezzo_um", &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quan_offerta"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quantita_um"), &
			              												double(i_coltext), &
			              												dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			
		case "cod_versione"
			
			string ls_flag_esplodi_in_doc

			// Gestione del KIT prodotto.
			if not isnull(i_coltext) and len(i_coltext) > 0 then
			
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ls_flag_esplodi_in_doc = "N"
				
				select flag_esplodi_in_doc
				into   :ls_flag_esplodi_in_doc
				from   distinta_padri
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 cod_versione = :i_coltext and
						 flag_blocco = 'N';
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore in ricerca ed esplosione KIT prodotti", Information!)
				end if
				
				if sqlca.sqlcode = 0 and ls_flag_esplodi_in_doc ="S" then
					// si tratta di un KIT di prodotti e quindi mi fermo per inserire il KIT
					dw_det_off_ven_lista.postevent("ue_inserisci_kit")
					return 0
				end if
				
			end if
			
	end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_modify, ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_ven = this.getitemstring(i_rownbr,"cod_tipo_det_ven")

   select tab_tipi_det_ven.flag_tipo_det_ven
   into   :ls_flag_tipo_det_ven
   from   tab_tipi_det_ven
   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_ven = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Unità di misura obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_offerta")
         if double(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	g_mb.messagebox("Attenzione", "Quantità offerta obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_ven = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	dw_documenti.uof_retrieve_blob(getrow())
	dw_det_off_ven_lista.setrow(getrow())
	
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_off_ven_det_1,"cod_prodotto")
		
	case "b_varianti"
		long ll_numero
		
		select parametri.numero  
		into   :ll_numero  
		from   parametri  
		where (parametri.flag_parametro = 'N' ) and
				(parametri.cod_parametro = 'GVA' )   ;
		if ll_numero > 0 and dw_det_off_ven_lista.getrow() > 0 then
			s_cs_xx.parametri.parametro_dw_1 = dw_det_off_ven_lista
			window_open(w_varianti_offerte,-1)
		end if		
			
end choose
end event

