﻿$PBExportHeader$w_det_ord_ven_storico.srw
forward
global type w_det_ord_ven_storico from w_cs_xx_principale
end type
type cb_3 from commandbutton within w_det_ord_ven_storico
end type
type cb_2 from cb_prod_ricerca within w_det_ord_ven_storico
end type
type cb_cerca from commandbutton within w_det_ord_ven_storico
end type
type dw_det_ord_acq_storico from uo_cs_xx_dw within w_det_ord_ven_storico
end type
type dw_det_ord_acq_storico_selezione from uo_cs_xx_dw within w_det_ord_ven_storico
end type
end forward

global type w_det_ord_ven_storico from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3218
string title = "Storico Acquisti"
event ue_imposta_selezione ( )
cb_3 cb_3
cb_2 cb_2
cb_cerca cb_cerca
dw_det_ord_acq_storico dw_det_ord_acq_storico
dw_det_ord_acq_storico_selezione dw_det_ord_acq_storico_selezione
end type
global w_det_ord_ven_storico w_det_ord_ven_storico

type variables
boolean ib_deactivate=true
string is_cod_prodotto, is_cod_fornitore
long il_giorni
end variables

event ue_imposta_selezione();is_cod_prodotto = s_cs_xx.parametri.parametro_s_8
is_cod_fornitore = s_cs_xx.parametri.parametro_s_9

dw_det_ord_acq_storico_selezione.setitem(dw_det_ord_acq_storico_selezione.getrow(),"cod_prodotto", is_cod_prodotto)
dw_det_ord_acq_storico_selezione.setitem(dw_det_ord_acq_storico_selezione.getrow(),"cod_cliente", is_cod_fornitore)
//dw_det_ord_acq_storico_selezione.setitem(dw_det_ord_acq_storico_selezione.getrow(),"tipo_doc", s_cs_xx.parametri.parametro_s_10)
dw_det_ord_acq_storico_selezione.setitem(dw_det_ord_acq_storico_selezione.getrow(),"tipo_doc", 'T')

ib_deactivate = true
if isnull(is_cod_prodotto) or len(trim(is_cod_prodotto)) < 1 then
	ib_deactivate = false
	g_mb.messagebox("APICE","L'indicazione del prodotto è obbligatorio per la ricerca !!!", information!)
	close(this)
	return
else
	cb_cerca.postevent(clicked!)
end if
end event

on w_det_ord_ven_storico.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_cerca=create cb_cerca
this.dw_det_ord_acq_storico=create dw_det_ord_acq_storico
this.dw_det_ord_acq_storico_selezione=create dw_det_ord_acq_storico_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_cerca
this.Control[iCurrent+4]=this.dw_det_ord_acq_storico
this.Control[iCurrent+5]=this.dw_det_ord_acq_storico_selezione
end on

on w_det_ord_ven_storico.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_cerca)
destroy(this.dw_det_ord_acq_storico)
destroy(this.dw_det_ord_acq_storico_selezione)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_det_ord_acq_storico.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_det_ord_acq_storico_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_det_ord_acq_storico



end event

event activate;call super::activate;postevent("ue_imposta_selezione")
end event

event deactivate;call super::deactivate;if ib_deactivate then this.hide()
end event

type cb_3 from commandbutton within w_det_ord_ven_storico
integer x = 2793
integer y = 216
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;long ll_i

if dw_det_ord_acq_storico.getrow() > 0 then

	
	if s_cs_xx.parametri.parametro_s_7 ="U" then
		
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("prezzo_um")
		s_cs_xx.parametri.parametro_uo_dw_1.SetText( string( dw_det_ord_acq_storico.getitemnumber(dw_det_ord_acq_storico.getrow(),"prezzo") ) )
		s_cs_xx.parametri.parametro_uo_dw_1.AcceptText()
		
		if s_cs_xx.parametri.parametro_s_11 <> "rda" or isnull(s_cs_xx.parametri.parametro_s_11) then
		
			for ll_i = 1 to 10
				s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), "sconto_" + string(ll_i), dw_det_ord_acq_storico.getitemnumber(dw_det_ord_acq_storico.getrow(),"sconto_" + string(ll_i) ) )
		//		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("sconto_" + string(ll_i))
		//		s_cs_xx.parametri.parametro_uo_dw_1.SetText( string( dw_det_ord_acq_storico.getitemnumber(dw_det_ord_acq_storico.getrow(),"sconto_" + string(ll_i)) ) )
		//		s_cs_xx.parametri.parametro_uo_dw_1.AcceptText()
			next
		else
			
			s_cs_xx.parametri.parametro_s_11 = ""
			
		end if
		
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("prezzo_um")
		s_cs_xx.parametri.parametro_uo_dw_1.setfocus()		
		
	elseif s_cs_xx.parametri.parametro_s_7 ="V" then
		
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("prezzo_vendita")
		s_cs_xx.parametri.parametro_uo_dw_1.SetText( string( dw_det_ord_acq_storico.getitemnumber(dw_det_ord_acq_storico.getrow(),"prezzo") ) )
		s_cs_xx.parametri.parametro_uo_dw_1.AcceptText()
		
		if s_cs_xx.parametri.parametro_s_11 <> "rda" or isnull(s_cs_xx.parametri.parametro_s_11) then
		
			for ll_i = 1 to 10
				s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), "sconto_" + string(ll_i), dw_det_ord_acq_storico.getitemnumber(dw_det_ord_acq_storico.getrow(),"sconto_" + string(ll_i) ) )
		//		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("sconto_" + string(ll_i))
		//		s_cs_xx.parametri.parametro_uo_dw_1.SetText( string( dw_det_ord_acq_storico.getitemnumber(dw_det_ord_acq_storico.getrow(),"sconto_" + string(ll_i)) ) )
		//		s_cs_xx.parametri.parametro_uo_dw_1.AcceptText()
			next
		else
			
			s_cs_xx.parametri.parametro_s_11 = ""
			
		end if
		
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("prezzo_vendita")
		s_cs_xx.parametri.parametro_uo_dw_1.setfocus()
	end if

	parent.hide()
end if

end event

type cb_2 from cb_prod_ricerca within w_det_ord_ven_storico
integer x = 2117
integer y = 24
integer width = 73
integer height = 72
integer taborder = 30
end type

event getfocus;call super::getfocus;ib_deactivate = false
dw_det_ord_acq_storico_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
s_cs_xx.parametri.parametro_tipo_ricerca = 2
end event

type cb_cerca from commandbutton within w_det_ord_ven_storico
integer x = 2793
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// ----------------------------------------------------------------------------------------
// devo entrare in questo script avendo già valorizzato le seguenti variabili di istanza
//
//		is_cod_prodotto = codice prodotto
//		is_cod_fornitore = codice del fornitore
//		il_giorni = numero giorni ricerca nello storico
//		is_gestione = ricerca da effettuare in F=offerte di acquisto
//															O=ordini di acquisito
//															B=bolle di entrata merce
//															F=Fatture di acquisto
//
// ----------------------------------------------------------------------------------------
string ls_tabella, ls_sql,ls_colonna_cod_fornitore,ls_colonna_rag_soc_1,ls_colonna_data,ls_colonna_quantita,ls_colonna_valuta,ls_colonna_prezzo,&
       ls_colonna_prezzo_netto,ls_colonna_sconto[10], ls_gestione, ls_sconti,ls_colonna_cod_misura,ls_colonna_fat_conversione, ls_flag_data,&
		 ls_prezzo_um, ls_quantita_um, ls_colonna_prezzo_um, ls_colonna_quantita_um, ls_cod_cliente
long ll_num, ll_i, ll_riga, ll_z
date ld_data_limite
dec{4} ld_prezzo_netto
datastore lds_ricerca

dw_det_ord_acq_storico_selezione.accepttext()
dw_det_ord_acq_storico.reset()
dw_det_ord_acq_storico.setredraw(false)

ls_flag_data = dw_det_ord_acq_storico_selezione.getitemstring(dw_det_ord_acq_storico_selezione.getrow(), "flag_data")
ls_gestione = dw_det_ord_acq_storico_selezione.getitemstring(dw_det_ord_acq_storico_selezione.getrow(), "tipo_doc")
ls_cod_cliente = dw_det_ord_acq_storico_selezione.getitemstring(dw_det_ord_acq_storico_selezione.getrow(), "cod_fornitore")
if isnull(ls_cod_cliente) or ls_cod_cliente = "" then setnull(is_cod_fornitore)

if isnull(is_cod_prodotto) or len(trim(is_cod_prodotto)) < 1 then 
	ib_deactivate = false
	g_mb.messagebox("APICE","L'indicazione del prodotto è obbligatorio per la ricerca !!!", information!)
	ib_deactivate = true
	return
end if
ld_data_limite = relativedate(today(), il_giorni * -1 )

lds_ricerca = CREATE datastore

choose case ls_gestione
		
	case "O" 			/// ordini di acquisto
		lds_ricerca.dataobject = 'd_ds_det_ord_ven_storico_ordini'
		lds_ricerca.settransobject(sqlca)
		ls_sql = lds_ricerca.getsqlselect()
		
		dw_det_ord_acq_storico.setredraw(false)
		
		if pos(ls_sql,"where") = 0 AND pos(ls_sql,"WHERE") = 0 AND pos(ls_sql,"Where") = 0 then
			ls_sql += " where "
		else
			ls_sql += " and "
		end if
		
		ls_sql += " ( tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "
		
		if ls_flag_data = "N" then
			ls_sql += " and (tes_ord_ven.data_registrazione >= '"+ string(ld_data_limite, s_cs_xx.db_funzioni.formato_data) +"') "
		end if

		if not isnull(is_cod_prodotto) and len(is_cod_prodotto) > 0 then
			ls_sql += " and ( det_ord_ven.cod_prodotto = '" + is_cod_prodotto + "') "
		end if

		if not isnull(is_cod_fornitore) and len(is_cod_fornitore) > 0 then
			ls_sql += " and ( tes_ord_ven.cod_cliente = '" + is_cod_fornitore + "') "
		else
			ls_sql += "and ( tes_ord_ven.cod_cliente like '%' )"
		end if

		ll_num = lds_ricerca.setsqlselect(ls_sql)
		if ll_num = -1 then
			g_mb.messagebox("APICE","Errore nell'impostazione della SELECT nella DW")
			return
		end if

		ll_num = lds_ricerca.retrieve()
		if ll_num < 0 then
			parent.title = "Storico Vendite - ** Errore ** "
		else
			parent.title = "Storico Vendite - Trovato " + string(ll_num) + " condizioni "
		end if
		
		ls_colonna_cod_fornitore = "tes_ord_ven_cod_cliente"
		ls_colonna_rag_soc_1 = "anag_clienti_rag_soc_1"
		ls_colonna_data = "tes_ord_ven_data_registrazione"
		ls_colonna_quantita = "det_ord_ven_quan_ordine"
		ls_colonna_valuta = "tes_ord_ven_cod_valuta"
		ls_colonna_prezzo = "det_ord_ven_prezzo_vendita"
		ls_colonna_prezzo_netto = "det_ord_ven_imponibile_iva_valuta"
		ls_colonna_sconto[1] = "det_ord_ven_sconto_1"
		ls_colonna_sconto[2] = "det_ord_ven_sconto_2"
		ls_colonna_sconto[3] = "det_ord_ven_sconto_3"
		ls_colonna_sconto[4] = "det_ord_ven_sconto_4"
		ls_colonna_sconto[5] = "det_ord_ven_sconto_5"
		ls_colonna_sconto[6] = "det_ord_ven_sconto_6"
		ls_colonna_sconto[7] = "det_ord_ven_sconto_7"
		ls_colonna_sconto[8] = "det_ord_ven_sconto_8"
		ls_colonna_sconto[9] = "det_ord_ven_sconto_9"
		ls_colonna_sconto[10] = "det_ord_ven_sconto_10"
		ls_colonna_cod_misura = "det_ord_ven_cod_misura"
		ls_colonna_fat_conversione = "det_ord_ven_fat_conversione_ven"
 
		
	case "B" 			/// bolle di entrata
		lds_ricerca.dataobject = 'd_ds_det_ord_ven_storico_bolle'
		lds_ricerca.settransobject(sqlca)
		ls_sql = lds_ricerca.getsqlselect()
		
		dw_det_ord_acq_storico.setredraw(false)
		
		if pos(ls_sql,"where") = 0 AND pos(ls_sql,"WHERE") = 0  AND pos(ls_sql,"Where") = 0 then
			ls_sql += " where "
		else
			ls_sql += " and "
		end if
		
		ls_sql += " ( tes_bol_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "
		
		if ls_flag_data = "N" then
			ls_sql += " and (tes_bol_ven.data_bolla >= '"+ string(ld_data_limite, s_cs_xx.db_funzioni.formato_data) +"') "
		end if
		
		if not isnull(is_cod_prodotto) and len(is_cod_prodotto) > 0 then
			ls_sql += " and ( det_bol_ven.cod_prodotto = '" + is_cod_prodotto + "') "
		end if

		if not isnull(is_cod_fornitore) and len(is_cod_fornitore) > 0 then
			ls_sql += " and ( tes_bol_ven.cod_cliente = '" + is_cod_fornitore + "') "		
		else
			ls_sql += "and ( tes_bol_ven.cod_cliente like '%' )"
		end if

		ll_num = lds_ricerca.setsqlselect(ls_sql)
		if ll_num = -1 then
			g_mb.messagebox("APICE","Errore nell'impostazione della SELECT nella DW")
			return
		end if

		ll_num = lds_ricerca.retrieve()
		if ll_num < 0 then
			parent.title = "Storico Vendite - ** Errore ** "
		else
			parent.title = "Storico Vendite - Trovato " + string(ll_num) + " condizioni "
		end if
		
		ls_colonna_cod_fornitore = "tes_bol_ven_cod_cliente"
		ls_colonna_rag_soc_1 = "anag_clienti_rag_soc_1"
		ls_colonna_data = "tes_bol_ven_data_bolla"
		ls_colonna_quantita = "det_bol_ven_quan_consegnata"
		ls_colonna_valuta = "tes_bol_ven_cod_valuta"
		ls_colonna_prezzo = "det_bol_ven_prezzo_vendita"
		ls_colonna_prezzo_netto = "det_bol_ven_imponibile_iva_valuta"
		ls_colonna_sconto[1] = "det_bol_ven_sconto_1"
		ls_colonna_sconto[2] = "det_bol_ven_sconto_2"
		ls_colonna_sconto[3] = "det_bol_ven_sconto_3"
		ls_colonna_sconto[4] = "det_bol_ven_sconto_4"
		ls_colonna_sconto[5] = "det_bol_ven_sconto_5"
		ls_colonna_sconto[6] = "det_bol_ven_sconto_6"
		ls_colonna_sconto[7] = "det_bol_ven_sconto_7"
		ls_colonna_sconto[8] = "det_bol_ven_sconto_8"
		ls_colonna_sconto[9] = "det_bol_ven_sconto_9"
		ls_colonna_sconto[10] = "det_bol_ven_sconto_10"
		ls_colonna_cod_misura = "det_bol_ven_cod_misura"
		ls_colonna_fat_conversione = "det_bol_ven_fat_conversione_ven"
		
		
		
		
		

	case "T" 			/// fatture di acquisto
		lds_ricerca.dataobject = 'd_ds_det_ord_ven_storico_fatture'
		lds_ricerca.settransobject(sqlca)
		ls_sql = lds_ricerca.getsqlselect()
		
		dw_det_ord_acq_storico.setredraw(false)
		
		if pos(ls_sql,"where") = 0 AND pos(ls_sql,"WHERE") = 0  AND pos(ls_sql,"Where") = 0 then
			ls_sql += " where "
		else
			ls_sql += " and "
		end if
		
		ls_sql += " ( tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "
		
		if ls_flag_data = "N" then
			ls_sql += " and (tes_fat_ven.data_fattura >= '"+ string(ld_data_limite, s_cs_xx.db_funzioni.formato_data) +"') "
		end if
		
		if not isnull(is_cod_prodotto) and len(is_cod_prodotto) > 0 then
			ls_sql += " and ( det_fat_ven.cod_prodotto = '" + is_cod_prodotto + "') "
		end if

		if not isnull(is_cod_fornitore) and len(is_cod_fornitore) > 0 then
			ls_sql += " and ( tes_fat_ven.cod_cliente = '" + is_cod_fornitore + "') "
		else
			ls_sql += "and ( tes_fat_ven.cod_cliente like '%' )"
		end if

		ll_num = lds_ricerca.setsqlselect(ls_sql)
		if ll_num = -1 then
			g_mb.messagebox("APICE","Errore nell'impostazione della SELECT nella DW")
			return
		end if

		ll_num = lds_ricerca.retrieve()
		if ll_num < 0 then
			parent.title = "Storico vendite - ** Errore ** "
		else
			parent.title = "Storico Vendite - Trovato " + string(ll_num) + " condizioni "
		end if
		
		ls_colonna_cod_fornitore = "tes_fat_ven_cod_cliente"
		ls_colonna_rag_soc_1 = "anag_clienti_rag_soc_1"
		ls_colonna_data = "tes_fat_ven_data_fattura"
		ls_colonna_quantita = "det_fat_ven_quan_fatturata"
		ls_colonna_valuta = "tes_fat_ven_cod_valuta"
		ls_colonna_prezzo = "det_fat_ven_prezzo_vendita"
		ls_colonna_prezzo_netto = "det_fat_ven_imponibile_iva_valuta"
		ls_colonna_sconto[1] = "det_fat_ven_sconto_1"
		ls_colonna_sconto[2] = "det_fat_ven_sconto_2"
		ls_colonna_sconto[3] = "det_fat_ven_sconto_3"
		ls_colonna_sconto[4] = "det_fat_ven_sconto_4"
		ls_colonna_sconto[5] = "det_fat_ven_sconto_5"
		ls_colonna_sconto[6] = "det_fat_ven_sconto_6"
		ls_colonna_sconto[7] = "det_fat_ven_sconto_7"
		ls_colonna_sconto[8] = "det_fat_ven_sconto_8"
		ls_colonna_sconto[9] = "det_fat_ven_sconto_9"
		ls_colonna_sconto[10] = "det_fat_ven_sconto_10"
		ls_colonna_cod_misura = "det_fat_ven_cod_misura"
		ls_colonna_fat_conversione = "det_fat_ven_fat_conversione_ven"
		ls_colonna_prezzo_um = "det_fat_ven_prezzo_um"
		ls_colonna_quantita_um = "det_fat_ven_quantita_um"


	case "F" 			/// offerte di acquisto
		lds_ricerca.dataobject = 'd_ds_det_ord_ven_storico_offerte'
		lds_ricerca.settransobject(sqlca)
		ls_sql = lds_ricerca.getsqlselect()
		
		dw_det_ord_acq_storico.setredraw(false)
		
		if pos(ls_sql,"where") = 0 AND pos(ls_sql,"WHERE") = 0  AND pos(ls_sql,"Where") = 0 then
			ls_sql += " where "
		else
			ls_sql += " and "
		end if
		
		ls_sql += " ( tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "
		
		if ls_flag_data = "N" then
			ls_sql += " and (tes_off_ven.data_registrazione >= '"+ string(ld_data_limite, s_cs_xx.db_funzioni.formato_data) +"') "
		end if

		if not isnull(is_cod_prodotto) and len(is_cod_prodotto) > 0 then
			ls_sql += " and ( det_off_ven.cod_prodotto = '" + is_cod_prodotto + "') "
		end if

		if not isnull(is_cod_fornitore) and len(is_cod_fornitore) > 0 then
			ls_sql += " and ( tes_off_ven.cod_cliente = '" + is_cod_fornitore + "') "
		else
			ls_sql += "and ( tes_off_ven.cod_cliente like '%' )"
		end if

		ll_num = lds_ricerca.setsqlselect(ls_sql)
		if ll_num = -1 then
			g_mb.messagebox("APICE","Errore nell'impostazione della SELECT nella DW")
			return
		end if

		ll_num = lds_ricerca.retrieve()
		if ll_num < 0 then
			parent.title = "Storico Vendite - ** Errore ** "
		else
			parent.title = "Storico Vendite - Trovato " + string(ll_num) + " condizioni "
		end if
		
		ls_colonna_cod_fornitore = "tes_off_ven_cod_cliente"
		ls_colonna_rag_soc_1 = "anag_clienti_rag_soc_1"
		ls_colonna_data = "tes_off_ven_data_registrazione"
		ls_colonna_quantita = "det_off_ven_quan_offerta"
		ls_colonna_valuta = "tes_off_ven_cod_valuta"
		ls_colonna_prezzo = "det_off_ven_prezzo_vendita"
		ls_colonna_prezzo_netto = "det_off_ven_imponibile_iva_valuta"
		ls_colonna_sconto[1] = "det_off_ven_sconto_1"
		ls_colonna_sconto[2] = "det_off_ven_sconto_2"
		ls_colonna_sconto[3] = "det_off_ven_sconto_3"
		ls_colonna_sconto[4] = "det_off_ven_sconto_4"
		ls_colonna_sconto[5] = "det_off_ven_sconto_5"
		ls_colonna_sconto[6] = "det_off_ven_sconto_6"
		ls_colonna_sconto[7] = "det_off_ven_sconto_7"
		ls_colonna_sconto[8] = "det_off_ven_sconto_8"
		ls_colonna_sconto[9] = "det_off_ven_sconto_9"
		ls_colonna_sconto[10] = "det_off_ven_sconto_10"
		ls_colonna_cod_misura = "det_off_ven_cod_misura"
		ls_colonna_fat_conversione = "det_off_ven_fat_conversione_ven"
 

end choose
//
for ll_i = 1 to ll_num
	ll_riga = dw_det_ord_acq_storico.insertrow(0)
	dw_det_ord_acq_storico.setitem(ll_riga, "cod_cliente", lds_ricerca.getitemstring(ll_i,ls_colonna_cod_fornitore) )
	dw_det_ord_acq_storico.setitem(ll_riga, "rag_soc_1", lds_ricerca.getitemstring(ll_i,ls_colonna_rag_soc_1) )
	dw_det_ord_acq_storico.setitem(ll_riga, "data_registrazione", lds_ricerca.getitemdatetime(ll_i, ls_colonna_data) )
	dw_det_ord_acq_storico.setitem(ll_riga, "cod_valuta", lds_ricerca.getitemstring(ll_i, ls_colonna_valuta) )
	//if 
	if s_cs_xx.parametri.parametro_s_7 ="V" then
		dw_det_ord_acq_storico.setitem(ll_riga, "quantita", lds_ricerca.getitemnumber(ll_i,ls_colonna_quantita) )
		dw_det_ord_acq_storico.setitem(ll_riga, "prezzo", lds_ricerca.getitemnumber(ll_i,ls_colonna_prezzo))
	elseif s_cs_xx.parametri.parametro_s_7 ="U" then
		dw_det_ord_acq_storico.setitem(ll_riga, "quantita", lds_ricerca.getitemnumber(ll_i,ls_colonna_quantita_um) )
		dw_det_ord_acq_storico.setitem(ll_riga, "prezzo", lds_ricerca.getitemnumber(ll_i,ls_colonna_prezzo_um))
	end if
	dw_det_ord_acq_storico.setitem(ll_riga, "cod_misura", lds_ricerca.getitemstring(ll_i, ls_colonna_cod_misura))
	dw_det_ord_acq_storico.setitem(ll_riga, "fat_conversione", lds_ricerca.getitemnumber(ll_i,ls_colonna_fat_conversione))
	
	ld_prezzo_netto = f_calcola_riga_euro("M", &
	                                      lds_ricerca.getitemnumber(ll_i,ls_colonna_quantita), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_prezzo), &
													  lds_ricerca.getitemstring(ll_i, ls_colonna_valuta) , &
													  1, &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[1]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[2]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[3]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[4]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[5]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[6]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[7]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[8]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[9]), &
													  lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[10]) )

	if lds_ricerca.getitemnumber(ll_i, ls_colonna_quantita) = 0 or isnull(lds_ricerca.getitemnumber(ll_i, ls_colonna_quantita) = 0) then
		ld_prezzo_netto = 0
	else
		ld_prezzo_netto = ld_prezzo_netto / lds_ricerca.getitemnumber(ll_i, ls_colonna_quantita)
	end if
	dw_det_ord_acq_storico.setitem(ll_riga, "prezzo_netto", ld_prezzo_netto)
	
	ls_sconti = ""
	
	for ll_z = 1 to 10
		dw_det_ord_acq_storico.setitem(ll_riga, "sconto_" + string(ll_z), lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[ll_z]))
		if lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[ll_z]) > 0 then
			if len(ls_sconti) > 0 then
				ls_sconti += " + " + string(lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[ll_z]), "##0.0#")
			else
				ls_sconti += string(lds_ricerca.getitemnumber(ll_i,ls_colonna_sconto[ll_z]), "##0.0#")
			end if
		end if
	next
	
	dw_det_ord_acq_storico.setitem(ll_riga, "sconti", ls_sconti)
	
	
				
next

destroy lds_ricerca
dw_det_ord_acq_storico.setredraw(true)

end event

type dw_det_ord_acq_storico from uo_cs_xx_dw within w_det_ord_ven_storico
integer x = 18
integer y = 332
integer width = 3154
integer height = 972
integer taborder = 20
string dataobject = "d_det_ord_ven_storico"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;cb_3.triggerevent(clicked!)

end event

type dw_det_ord_acq_storico_selezione from uo_cs_xx_dw within w_det_ord_ven_storico
integer x = 18
integer y = 20
integer width = 2706
integer height = 312
integer taborder = 10
string dataobject = "d_det_ord_ven_storico_selezione"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_prodotto"
		is_cod_prodotto = i_coltext
	case "cod_cliente"
		is_cod_fornitore = i_coltext
	case "num_giorni"
		il_giorni = long(i_coltext)
	case "tipo_doc"
		cb_cerca.postevent("clicked")
end choose
end event

event pcd_new;call super::pcd_new;long ll_numero

select numero
into :ll_numero
from  parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_parametro = 'RTM';
if sqlca.sqlcode <> 0 then
	ll_numero = 365
end if

setitem(getrow(),"num_giorni",ll_numero)

il_giorni = ll_numero

setitem(getrow(),"tipo_doc",s_cs_xx.parametri.parametro_s_10)

end event

