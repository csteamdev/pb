﻿$PBExportHeader$w_report_offerte_ordini.srw
forward
global type w_report_offerte_ordini from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_offerte_ordini
end type
type dw_folder from u_folder within w_report_offerte_ordini
end type
type dw_ricerca from u_dw_search within w_report_offerte_ordini
end type
end forward

global type w_report_offerte_ordini from w_cs_xx_principale
integer width = 5033
integer height = 2448
string title = "Report Offerte Ordini"
dw_report dw_report
dw_folder dw_folder
dw_ricerca dw_ricerca
end type
global w_report_offerte_ordini w_report_offerte_ordini

forward prototypes
public subroutine wf_reset ()
public function integer wf_report (ref string as_errore)
public function integer wf_get_righe_ordini (integer ai_anno_offerta, long al_num_offerta, long al_riga_offerta, ref datastore ads_righe_ordini, ref string as_errore)
end prototypes

public subroutine wf_reset ();
string				ls_null
datetime			ldt_null


setnull(ls_null)
setnull(ldt_null)

dw_ricerca.setitem(1,"data_registrazione_inizio", ldt_null)
dw_ricerca.setitem(1,"data_registrazione_fine", ldt_null)
dw_ricerca.setitem(1,"flag_ordini_successivi","N")
dw_ricerca.setitem(1,"cod_prodotto_inizio", ls_null)
dw_ricerca.setitem(1,"cod_prodotto_fine", ls_null)
dw_ricerca.setitem(1,"cod_cliente", ls_null)
		

return
end subroutine

public function integer wf_report (ref string as_errore);
string				ls_cod_cliente_sel, ls_cod_prodotto_inizio_sel, ls_cod_prodotto_fine_sel, ls_flag_ordini_successivi, ls_sql, ls_log, ls_filtro, ls_flag_solo_contatti
datetime			ldt_data_reg_inizio_sel, ldt_data_reg_fine_sel, ldt_data_reg_ds
datastore		lds_offerte
long				ll_index, ll_tot, ll_new, ll_num_offerta_ds, ll_riga_offerta_ds, ll_index2, ll_tot2, ll_numero_ordine_ds, ll_riga_ordine_ds
integer			li_anno_offerta_ds, ll_ret, li_anno_ordine_ds
string				ls_cod_prodotto_ds, ls_des_prodotto_anag_ds, ls_des_prodotto_ds, ls_cod_cliente_ds, ls_rag_soc_1_ds, &
					ls_cod_prodotto_ord_ds, ls_des_prodotto_ord_anag_ds, ls_des_prodotto_ord_ds, ls_cod_contatto_ds, ls_codice, ls_ragionesociale
datastore		lds_righe_ordini


dw_report.Reset() 
dw_ricerca.accepttext()


ldt_data_reg_inizio_sel = dw_ricerca.getitemdatetime(1,"data_registrazione_inizio")
ldt_data_reg_fine_sel = dw_ricerca.getitemdatetime(1,"data_registrazione_fine")
ls_flag_ordini_successivi = dw_ricerca.getitemstring(1,"flag_ordini_successivi")
ls_cod_prodotto_inizio_sel = dw_ricerca.getitemstring(1,"cod_prodotto_inizio")
ls_cod_prodotto_fine_sel = dw_ricerca.getitemstring(1,"cod_prodotto_fine")
ls_cod_cliente_sel = dw_ricerca.getitemstring(1,"cod_cliente")
ls_flag_solo_contatti = dw_ricerca.getitemstring(1,"flag_solo_contatti")



ls_sql = 	"select d.anno_registrazione,d.num_registrazione,d.prog_riga_off_ven,t.data_registrazione,"+&
						"d.cod_prodotto,p.des_prodotto,d.des_prodotto,"+&
						"t.cod_cliente,c.rag_soc_1,"+&
						"t.cod_contatto, ct.rag_soc_1 "+&
			"from det_off_ven as d "+&
			"left outer join anag_prodotti as p on p.cod_azienda=d.cod_azienda and "+&
													"p.cod_prodotto=d.cod_prodotto "+&
			"join tes_off_ven as t on t.cod_azienda=d.cod_azienda and "+&
											"t.anno_registrazione=d.anno_registrazione and "+&
											"t.num_registrazione=d.num_registrazione "+&
			"left outer join anag_clienti as c on c.cod_azienda=t.cod_azienda and "+&
													"c.cod_cliente=t.cod_cliente "+&
			"left outer join anag_contatti as ct on ct.cod_azienda=t.cod_azienda and "+&
													"ct.cod_contatto=t.cod_contatto "+&
			"where d.cod_azienda='"+s_cs_xx.cod_azienda+"' "

if not isnull(ldt_data_reg_inizio_sel) and year(date(ldt_data_reg_inizio_sel))>1950 then
	ls_sql += " and t.data_registrazione>='"+string(ldt_data_reg_inizio_sel, s_cs_xx.db_funzioni.formato_data)+"'"
	ls_filtro += "Da Data Reg.:"+string(ldt_data_reg_inizio_sel, "dd/mm/yyyy")
end if

if not isnull(ldt_data_reg_fine_sel) and year(date(ldt_data_reg_fine_sel))>1950 then
	ls_sql += " and t.data_registrazione<='"+string(ldt_data_reg_fine_sel, s_cs_xx.db_funzioni.formato_data)+"'"
	ls_filtro += " - Fino a Data Reg.:"+string(ldt_data_reg_fine_sel, "dd/mm/yyyy")
end if

if ls_flag_ordini_successivi<>"S" then
	ls_flag_ordini_successivi="N"
end if

ls_filtro += " - Ordini Succ.:"+ls_flag_ordini_successivi

if ls_flag_solo_contatti<>"S" then
	ls_flag_ordini_successivi="N"
else
	ls_sql += " and t.cod_contatto is not null"
end if

ls_filtro += " - Solo Contatti:"+ls_flag_solo_contatti


if not isnull(ls_cod_cliente_sel) and ls_cod_cliente_sel<>"" then
	ls_sql += " and t.cod_cliente='"+ls_cod_cliente_sel+"'"
	ls_filtro += " - Cliente:"+ls_cod_cliente_sel
end if

if not isnull(ls_cod_prodotto_inizio_sel) and ls_cod_prodotto_inizio_sel<>"" then
	ls_sql += " and d.cod_prodotto>='"+ls_cod_prodotto_inizio_sel+"'"
	ls_filtro += " - da Prodotto:"+ls_cod_prodotto_inizio_sel
end if

if not isnull(ls_cod_prodotto_fine_sel) and ls_cod_prodotto_fine_sel<>"" then
	ls_sql += " and d.cod_prodotto<='"+ls_cod_prodotto_fine_sel+"'"
	ls_filtro += " - Fino a Prodotto:"+ls_cod_prodotto_fine_sel
end if


dw_report.object.filtro_t.text = ls_filtro


ll_tot = guo_functions.uof_crea_datastore(lds_offerte, ls_sql, as_errore)
if ll_tot<0 then
	return -1
end if

if ll_tot=0 then
	as_errore = "Nessuna offerta di vendita corrisponde al filtro specificato!"
	return 1
end if

for ll_index=1 to ll_tot
	
	li_anno_offerta_ds = lds_offerte.getitemnumber(ll_index, 1)
	ll_num_offerta_ds = lds_offerte.getitemnumber(ll_index, 2)
	ll_riga_offerta_ds = lds_offerte.getitemnumber(ll_index, 3)
	
	Yield()
	
	ls_log = "("+string(ll_index) +" di "+ string(ll_tot)+") Elab. riga offerta " + string(li_anno_offerta_ds)+"/"+string(ll_num_offerta_ds)+"/"+string(ll_riga_offerta_ds)
	dw_ricerca.object.t_log.text = ls_log
	
	ldt_data_reg_ds = lds_offerte.getitemdatetime(ll_index, 4)
	ls_cod_prodotto_ds = lds_offerte.getitemstring(ll_index, 5)
	ls_des_prodotto_anag_ds = lds_offerte.getitemstring(ll_index, 6)
	ls_des_prodotto_ds = lds_offerte.getitemstring(ll_index, 7)
	ls_cod_cliente_ds = lds_offerte.getitemstring(ll_index, 8)
	ls_rag_soc_1_ds = lds_offerte.getitemstring(ll_index, 9)
	
	ls_codice = lds_offerte.getitemstring(ll_index, 10)
	ls_ragionesociale = lds_offerte.getitemstring(ll_index, 11)
	
	if isnull(ls_codice) or ls_codice="" then
		//leggi il cliente
		if isnull(ls_cod_cliente_ds) or ls_cod_cliente_ds="" then
			//questa possibilità è assurda ma la metto lo stesso (impossibile generare offerta senza nè cliente o contatto)
			ls_codice = ""
			ls_ragionesociale = "NESSUN CLIENTE/CONTATTO SPECIFICATO"
		else
			ls_codice = ls_cod_cliente_ds
			ls_ragionesociale =ls_rag_soc_1_ds
		end if
	end if
	
	//inserisco riga nel report
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new, "anno_offerta", li_anno_offerta_ds)
	dw_report.setitem(ll_new, "num_offerta", ll_num_offerta_ds)
	dw_report.setitem(ll_new, "riga_offerta", ll_riga_offerta_ds)
	dw_report.setitem(ll_new, "data_registrazione", ldt_data_reg_ds)
	dw_report.setitem(ll_new, "cod_prodotto_off", ls_cod_prodotto_ds)
	
	if ls_des_prodotto_ds="" or isnull(ls_des_prodotto_ds) then ls_des_prodotto_ds = ls_des_prodotto_anag_ds
	dw_report.setitem(ll_new, "des_prodotto_off", ls_des_prodotto_ds)
	
	dw_report.setitem(ll_new, "count_off", 1)
	dw_report.setitem(ll_new, "cod_cliente", ls_codice)
	dw_report.setitem(ll_new, "rag_soc_1", ls_ragionesociale)
	
	//cerca le righe ordini di questa offerta
	ll_ret = wf_get_righe_ordini(li_anno_offerta_ds, ll_num_offerta_ds, ll_riga_offerta_ds, lds_righe_ordini, as_errore)
	if ll_ret < 0 then
		destroy lds_offerte
		return -1
		
	elseif ll_ret=0 then
		//no righe ordini per questa riga offerta
		continue
	end if
	
	if ls_flag_ordini_successivi="S" then
		//metto il totale righe ordini provenienti dalla riga offerta corrente
		ll_tot2 = ll_ret
	else
		ll_tot2 = 1
	end if
	
	for ll_index2=1 to ll_tot2
		//campi datastore
		//1		d.anno_registrazione,
		//2		d.num_registrazione,
		//3		d.prog_riga_ord_ven,
		//4		d.cod_prodotto,
		//5		d.des_prodotto,
		//6		p.des_prodotto,
		//7		t.data_registrazione
		
		li_anno_ordine_ds = lds_righe_ordini.getitemnumber(ll_index2, 1)
		ll_numero_ordine_ds = lds_righe_ordini.getitemnumber(ll_index2, 2)
		ll_riga_ordine_ds = lds_righe_ordini.getitemnumber(ll_index2, 3)
		
		
		dw_ricerca.object.t_log.text = ls_log + " -> riga ordine " + string(li_anno_ordine_ds)+"/"+string(ll_numero_ordine_ds)+"/"+string(ll_riga_ordine_ds)
		
		
		ls_cod_prodotto_ord_ds = lds_righe_ordini.getitemstring(ll_index2, 4)
		ls_des_prodotto_ord_ds = lds_righe_ordini.getitemstring(ll_index2, 5)
		ls_des_prodotto_ord_anag_ds = lds_righe_ordini.getitemstring(ll_index2, 6)
		
		if isnull(ls_des_prodotto_ord_ds) or ls_des_prodotto_ord_ds="" then
			ls_des_prodotto_ord_ds = ls_des_prodotto_ord_anag_ds
		end if
		
		//se riga successiva alla prima faccio insert row di nuovo
		//e re-imposto i campi relativi alla riga offerta
		if ll_index2>1 then
			ll_new = dw_report.insertrow(0)
			
			dw_report.setitem(ll_new, "anno_offerta", li_anno_offerta_ds)
			dw_report.setitem(ll_new, "num_offerta", ll_num_offerta_ds)
			dw_report.setitem(ll_new, "riga_offerta", ll_riga_offerta_ds)
			dw_report.setitem(ll_new, "data_registrazione", ldt_data_reg_ds)
			dw_report.setitem(ll_new, "cod_prodotto_off", ls_cod_prodotto_ds)
			
			if ls_des_prodotto_ds="" or isnull(ls_des_prodotto_ds) then ls_des_prodotto_ds = ls_des_prodotto_anag_ds
			dw_report.setitem(ll_new, "des_prodotto_off", ls_des_prodotto_ds)
			
			//questa riga offerta deve contare zero nelle statistiche
			dw_report.setitem(ll_new, "count_off", 0)
			
			dw_report.setitem(ll_new, "cod_cliente", ls_ragionesociale)
			dw_report.setitem(ll_new, "rag_soc_1", ls_ragionesociale)
			
		end if
		
		dw_report.setitem(ll_new, "anno_ordine", li_anno_ordine_ds)
		dw_report.setitem(ll_new, "num_ordine", ll_numero_ordine_ds)
		dw_report.setitem(ll_new, "riga_ordine", ll_riga_ordine_ds)
		dw_report.setitem(ll_new, "cod_prodotto_ord", ls_cod_prodotto_ord_ds)
		dw_report.setitem(ll_new, "des_prodotto_ord", ls_des_prodotto_ord_ds)
		dw_report.setitem(ll_new, "count_ord", 1)

	next

	//distruggi il datastore utilizzato per le righe ordini
	destroy lds_righe_ordini
	
next

destroy lds_offerte

dw_report.sort()
dw_report.groupcalc()

dw_folder.fu_selecttab(2)


return 0
end function

public function integer wf_get_righe_ordini (integer ai_anno_offerta, long al_num_offerta, long al_riga_offerta, ref datastore ads_righe_ordini, ref string as_errore);long			ll_tot
string			ls_sql


ls_sql = "select    d.anno_registrazione,d.num_registrazione,d.prog_riga_ord_ven,"+&
					"d.cod_prodotto,d.des_prodotto,p.des_prodotto, t.data_registrazione "+&
			"from det_ord_ven as d "+&
			"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and "+&
											"t.anno_registrazione=d.anno_registrazione and "+&
											"t.num_registrazione=d.num_registrazione "+&
			"left join anag_prodotti as p on p.cod_azienda=d.cod_azienda and "+&
											"p.cod_prodotto=d.cod_prodotto "+&
			"where 	d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"d.anno_registrazione_off="+string(ai_anno_offerta)+" and "+&
						"d.num_registrazione_off="+string(al_num_offerta)+" and "+&
						"d.prog_riga_off_ven="+string(al_riga_offerta)+" "+&
			"order by t.data_registrazione "

ll_tot = guo_functions.uof_crea_datastore(ads_righe_ordini, ls_sql, as_errore)

return ll_tot
end function

on w_report_offerte_ordini.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_ricerca
end on

on w_report_offerte_ordini.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;windowobject 	l_objects[ ]
string				ls_azienda


dw_report.ib_dw_report = true
dw_report.ib_dw_detail = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
								 
                            
save_on_close(c_socnosave)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])

l_objects[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

select rag_soc_1
into :ls_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

dw_report.object.azienda_t.text = ls_azienda




end event

type dw_report from uo_cs_xx_dw within w_report_offerte_ordini
integer x = 41
integer y = 120
integer width = 4910
integer height = 2196
integer taborder = 20
string dataobject = "d_report_offerte_ordini"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_offerte_ordini
integer x = 23
integer y = 16
integer width = 4965
integer height = 2304
integer taborder = 10
boolean border = false
end type

type dw_ricerca from u_dw_search within w_report_offerte_ordini
integer x = 46
integer y = 116
integer width = 3086
integer height = 852
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_report_offerte_ordini_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string				ls_errore
integer			li_ret


choose case dwo.name
	case "b_report"
		setpointer(Hourglass!)
		dw_report.setredraw(false)
		dw_ricerca.object.t_log.text = "Elaborazione dati in corso, attendere ..."
		li_ret = wf_report(ls_errore)
		dw_report.setredraw(true)
		
		if li_ret < 0 then
			dw_ricerca.object.t_log.text = "Errore!"
			g_mb.error(ls_errore)
			setpointer(Arrow!)
			return
			
		elseif li_ret = 1 then
			dw_ricerca.object.t_log.text = "Pronto!"
			g_mb.warning(ls_errore)
			setpointer(Arrow!)
			return
			
		else
			dw_ricerca.object.t_log.text = "Pronto!"
		end if
		
		//se arrivi fin qui il report è elaborato senza errori/warning
		
		setpointer(Arrow!)
			
			
		
	case "b_reset"
		wf_reset()
		
	case "b_sel_prodotto_inizio"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto_inizio")
		
	case "b_sel_prodotto_fine"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto_fine")
		
	case "b_sel_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca, "cod_cliente")
		
end choose
end event

