﻿$PBExportHeader$w_report_raccolta_dati.srw
$PBExportComments$Finestra Report modulo raccolta dati
forward
global type w_report_raccolta_dati from w_cs_xx_principale
end type
type dw_report_raccolta_dati from uo_cs_xx_dw within w_report_raccolta_dati
end type
end forward

global type w_report_raccolta_dati from w_cs_xx_principale
integer width = 3639
integer height = 4608
string title = "Report Offerte Clienti"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_raccolta_dati dw_report_raccolta_dati
end type
global w_report_raccolta_dati w_report_raccolta_dati

type variables
long il_anno_registrazione, il_num_registrazione
end variables

forward prototypes
public subroutine wf_report ()
end prototypes

public subroutine wf_report ();string 	ls_cod_cliente, ls_cod_operatore,ls_nota_testata, ls_rag_soc_1, ls_rag_soc_2,ls_des_operatore, &
			ls_rag_soc_azienda
long     ll_num_offerta
datetime ldt_data_registrazione, ldt_data_consegna
integer  li_anno_offerta

dw_report_raccolta_dati.reset()

select cod_cliente,   
		 nota_testata,
		 data_registrazione,
		 data_consegna,
		 cod_operatore
into   :ls_cod_cliente,   
	 	 :ls_nota_testata,
		 :ldt_data_registrazione,
		 :ldt_data_consegna,
		 :ls_cod_operatore
from   tes_off_ven  
where  tes_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_off_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_off_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_cliente)
	setnull(ls_nota_testata)
end if

select rag_soc_1,   
       rag_soc_2   
into   :ls_rag_soc_1,   
       :ls_rag_soc_2
from   anag_clienti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
end if

select des_operatore   
into   :ls_des_operatore
from   tab_operatori 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operatore = :ls_cod_operatore;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_operatore)
end if


select rag_soc_1   
into   :ls_rag_soc_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;


if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_azienda)
end if


dw_report_raccolta_dati.insertrow(0)

dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "cod_cliente", ls_cod_cliente)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "rag_soc", ls_rag_soc_1)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "nota_testata", ls_nota_testata)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "cod_cliente", ls_cod_cliente)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "data_registrazione", ldt_data_registrazione)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "data_consegna", ldt_data_consegna)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "cod_operatore", ls_cod_operatore)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "anno_offerta", il_anno_registrazione)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "num_offerta", il_num_registrazione)
dw_report_raccolta_dati.setitem(dw_report_raccolta_dati.getrow(), "des_operatore", ls_des_operatore)
dw_report_raccolta_dati.resetupdate()


dw_report_raccolta_dati.Modify ( "rag_soc_azienda.text='" + ls_rag_soc_azienda +"'" )
dw_report_raccolta_dati.change_dw_current()
end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify, ls_path_firma,ls_cod_oggetto

dw_report_raccolta_dati.ib_dw_report = true

set_w_options(c_noresizewin)
il_anno_registrazione = long(s_cs_xx.parametri.parametro_d_1)
il_num_registrazione = long(s_cs_xx.parametri.parametro_d_2)

dw_report_raccolta_dati.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_disablecc, &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)




end event

on w_report_raccolta_dati.create
int iCurrent
call super::create
this.dw_report_raccolta_dati=create dw_report_raccolta_dati
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_raccolta_dati
end on

on w_report_raccolta_dati.destroy
call super::destroy
destroy(this.dw_report_raccolta_dati)
end on

type dw_report_raccolta_dati from uo_cs_xx_dw within w_report_raccolta_dati
integer x = 23
integer y = 20
integer width = 3474
integer height = 4380
string dataobject = "d_report_raccolta_dati"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

