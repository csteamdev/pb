﻿$PBExportHeader$w_tes_gen_ord_ven.srw
$PBExportComments$Finestra Testata Generazione Ordini Vendita
forward
global type w_tes_gen_ord_ven from w_cs_xx_principale
end type
type tab_1 from tab within w_tes_gen_ord_ven
end type
type page_1 from userobject within tab_1
end type
type dw_ricerca from datawindow within page_1
end type
type page_1 from userobject within tab_1
dw_ricerca dw_ricerca
end type
type page_2 from userobject within tab_1
end type
type cb_seleziona from commandbutton within page_2
end type
type cb_refresh from commandbutton within page_2
end type
type dw_tes_gen_ord_ven from uo_cs_xx_dw within page_2
end type
type page_2 from userobject within tab_1
cb_seleziona cb_seleziona
cb_refresh cb_refresh
dw_tes_gen_ord_ven dw_tes_gen_ord_ven
end type
type page_3 from userobject within tab_1
end type
type dw_det_gen_ord_ven_options from uo_std_dw within page_3
end type
type cb_gen from commandbutton within page_3
end type
type dw_det_off_ven from uo_std_dw within page_3
end type
type page_3 from userobject within tab_1
dw_det_gen_ord_ven_options dw_det_gen_ord_ven_options
cb_gen cb_gen
dw_det_off_ven dw_det_off_ven
end type
type page_4 from userobject within tab_1
end type
type dw_gen_orig_dest from uo_cs_xx_dw within page_4
end type
type page_4 from userobject within tab_1
dw_gen_orig_dest dw_gen_orig_dest
end type
type tab_1 from tab within w_tes_gen_ord_ven
page_1 page_1
page_2 page_2
page_3 page_3
page_4 page_4
end type
end forward

global type w_tes_gen_ord_ven from w_cs_xx_principale
integer width = 4512
integer height = 2280
string title = "Generazione Ordini Vendita"
tab_1 tab_1
end type
global w_tes_gen_ord_ven w_tes_gen_ord_ven

type variables
private:
	string is_button_step1_text = "Seleziona"
	string is_button_step2_text = "Genera Ordine"
	
	long il_current_step = -1
	long il_anno_registrazione
	long il_num_registrazione
	
	boolean ib_gibus = false
	uo_fido_cliente	iuo_fido
	string is_cod_parametro_blocco = "GOC"
	
	
	string is_default_sql
end variables

forward prototypes
public function string wf_get_righe_det ()
public subroutine wf_genera_ordine ()
public subroutine wf_tipo_ord_gibus ()
public subroutine wf_get_tipi_ord_ven (ref string as_tipi_ord_ven[])
public function integer wf_genera_ordine (string as_cod_tipo_ord_ven, date adt_data_consegna)
public function string wf_righe_dettaglio_selezionate (boolean ab_righe_collegate, string as_cod_tipo_ord_ven, date adt_data_consegna)
public function string wf_righe_dettaglio_selezionate (boolean ab_righe_collegate, date adt_data_consegna)
public function datetime wf_calcola_data_pronto (datetime adt_data_consegna)
public function string wf_calcola_giro_consegna (string as_cod_cliente)
public function integer wf_inserisci_righe_collegate (long al_num_riga_off_appartenenza, long al_num_riga_ord_appartenenza, ref string as_messaggio)
public subroutine wf_conserva_progressivi (ref s_cs_xx_parametri al_progressivi, long al_num_offerta, long al_riga_offerta, long al_riga_ordine)
public function long wf_leggi_num_appartenenza (s_cs_xx_parametri al_progressivi, long al_num_offerta, long al_riga_offerta)
public subroutine wf_ricerca ()
end prototypes

public function string wf_get_righe_det ();/**
 * stefanop
 * 25/10/2011
 *
 * Controllo quali sono le righe di dettaglio da copiare
 **/

string ls_prog_righe[], ls_prog_riga
long ll_i, ll_j, ll_prog_riga, ll_num_riga_appartenenza, ll_rowcount

ll_rowcount = tab_1.page_3.dw_det_off_ven.rowcount()
	
for ll_i = 1 to ll_rowcount
	if tab_1.page_3.dw_det_off_ven.getitemstring(ll_i, "selected") = "S" then
		ll_prog_riga = tab_1.page_3.dw_det_off_ven.getitemnumber(ll_i, "prog_riga_off_ven")
		ll_num_riga_appartenenza = tab_1.page_3.dw_det_off_ven.getitemnumber(ll_i, "num_riga_appartenenza")
		
		if isnull(ll_num_riga_appartenenza) or ll_num_riga_appartenenza < 1 then 
			ls_prog_righe[upperbound(ls_prog_righe) + 1] = string(ll_prog_riga)
			continue
		end if
		
		// TODO: serve ciclo qui !!!
		ll_j = tab_1.page_3.dw_det_off_ven.find("prog_riga_off_ven=" + string(ll_num_riga_appartenenza) + " and selected='S'", 0, ll_rowcount)
		
		// controllo righe collegate
		if ll_j > 0 then 
			ls_prog_righe[upperbound(ls_prog_righe) + 1] = string(ll_prog_riga)
		end if
	end if
next

return guo_functions.uof_implode(ls_prog_righe, ",")
end function

public subroutine wf_genera_ordine ();/**
 * stefanop
 * 04/11/2011
 *
 * Genero ordini
 **/
 
string					ls_sql, ls_error, ls_cod_documento, ls_numeratore_documento, ls_tipi_ord_ven[],ls_flag_disgrega_ordini_diversi, ls_cod_cliente
int						li_count, li_i, li_j, li_row
long					ll_anno_documento, ll_num_documento, ll_anno_registrazione, ll_num_registrazione, ll_num_revisione
date					ldt_data_consegna
datastore			lds_store
datetime				ldt_oggi

setnull(ldt_data_consegna)
li_row = tab_1.page_2.dw_tes_gen_ord_ven.getrow()

tab_1.page_3.dw_det_off_ven.accepttext()
ldt_oggi = datetime(today(), 00:00:00)

ls_flag_disgrega_ordini_diversi = tab_1.page_3.dw_det_gen_ord_ven_options.getitemstring(1,"flag_disgrega_ordini_diversi")
if isnull(ls_flag_disgrega_ordini_diversi) then ls_flag_disgrega_ordini_diversi = "N"

ll_anno_registrazione = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(li_row, "anno_registrazione")
ll_num_registrazione = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(li_row, "num_registrazione")
ls_cod_documento = tab_1.page_2.dw_tes_gen_ord_ven.getitemstring(li_row, "cod_documento")
ll_anno_documento = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(li_row, "anno_documento")
ls_numeratore_documento = tab_1.page_2.dw_tes_gen_ord_ven.getitemstring(li_row, "numeratore_documento")
ll_num_documento = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(li_row, "num_documento")
ll_num_revisione = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(li_row, "num_revisione")
ls_cod_cliente = tab_1.page_2.dw_tes_gen_ord_ven.getitemstring(li_row, "cod_cliente")


//*******************************************************************************
//controllo parametri blocco
li_i = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente, ldt_oggi, is_cod_parametro_blocco, ls_error)
if li_i=2 then
	//blocco
	g_mb.error(ls_error)
	return
elseif li_i=1 then
	//solo warning
	g_mb.warning(ls_error)
end if
//*******************************************************************************


// NUOVO
wf_get_tipi_ord_ven(ls_tipi_ord_ven)

if upperbound(ls_tipi_ord_ven) < 1 then
	g_mb.error("Selezionare almeno un Tipo Ordine Vendita prima di generare l'ordine.")
	return
end if

for li_i = 1 to upperbound(ls_tipi_ord_ven)
	
	// divido per data consegna?
	if ls_flag_disgrega_ordini_diversi="S" then
		// recupero le date di consegna per eseguire la query
		ls_sql = "SELECT DISTINCT data_consegna FROM det_off_ven WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' "
		ls_sql += " AND anno_registrazione=" + string(ll_anno_registrazione)
		ls_sql += " AND num_registrazione=" + string(ll_num_registrazione)
		ls_sql += " AND prog_riga_off_ven IN (" + wf_righe_dettaglio_selezionate(false, ls_tipi_ord_ven[li_i], ldt_data_consegna) +  ") "
		ls_sql += " ORDER BY data_consegna"
		
		li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
		
		if li_count < 0 then
			g_mb.error(ls_error, sqlca)
			return
		end if
		
		for li_j = 1 to li_count
			
			ldt_data_consegna = date(lds_store.getitemdatetime(li_j, 1))
			
			if wf_genera_ordine(ls_tipi_ord_ven[li_i], ldt_data_consegna) < 0 then
				rollback;
				return
			end if
		next
	else
		// genera un ordine unico
		if wf_genera_ordine(ls_tipi_ord_ven[li_i], ldt_data_consegna) < 0 then
			rollback;
			return
		end if
	end if
	
next
// ---

if not isnull(ls_cod_documento) then
	// Metto in evasione tutte le offerte legate al numero interno
	update 	tes_off_ven  
	set 		flag_evasione = 'E'  
	where 	cod_azienda = :s_cs_xx.cod_azienda and  
				cod_documento = :ls_cod_documento and  
				anno_documento =  :ll_anno_documento and
				numeratore_documento = :ls_numeratore_documento and
				num_documento = :ll_num_documento;
		
	// Blocco le altre revisioni
	update 	tes_off_ven  
	set 		flag_blocco = 'S' 
	where 	cod_azienda = :s_cs_xx.cod_azienda and  
				cod_documento = :ls_cod_documento and  
				anno_documento =  :ll_anno_documento and
				numeratore_documento = :ls_numeratore_documento and
				num_documento = :ll_num_documento and 
				num_revisione <> :ll_num_revisione;
else
	// Metto in evasione tutte le offerte
	update 	tes_off_ven  
	set 		flag_evasione = 'E'  
	where	cod_azienda = :s_cs_xx.cod_azienda and  
				anno_registrazione = :ll_anno_registrazione and  
				num_registrazione = :ll_num_registrazione;
end if

if sqlca.sqlcode = -1 then
	g_mb.error("Si è verificato un errore in fase di aggiornamento Testata Offerte venuisto.", sqlca)
	rollback;
	return
end if

commit;
tab_1.selecttab(4)

tab_1.page_2.dw_tes_gen_ord_ven.triggerevent("pcd_retrieve")

end subroutine

public subroutine wf_tipo_ord_gibus ();string ls_flag_tipo_report, ls_cod_prodotto
long ll_i, ll_num_riga_appartenenza

//for ll_i = 1 to dw_det_off_ven.rowcount()
//	
//	ls_cod_prodotto = dw_det_off_ven.getitemstring(ll_i, "cod_prodotto ")
//	ll_num_riga_appartenenza = dw_det_off_ven.getitemnumber(ll_i, "num_riga_appartenenza")
//	
//	
//	// affinche una riga si di tipo sfuso NON deve avere la riga appartenenza compilata e non deve essere un modello.
////	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then // potrebbe essere un prodotto modello
////		select flag_tipo_report
////		into :ls_flag_tipo_report
////		from tab_flags_configuratore
////		where
////			cod_azienda = :s_cs_xx.cod_azienda and
////			cod_modello = :ls_cod_prodotto;
////			
////		if sqlca.sqlcode = 0 then // è un prodotto modello
////			lb_test = false  // e quindi non stampo
////		else
////			lb_test = true
////			// non è un modello e ha la riga appartenenza = 0; allora non va nel report sfuso
////		end if
////	else
////		lb_test = false
////	end if
//	
//next
//
//
end subroutine

public subroutine wf_get_tipi_ord_ven (ref string as_tipi_ord_ven[]);/**
 * Stefanop
 * 26/11/2011
 *
 * Recupero i tipi di ordini vendita legati al dettaglio offerta impostato dal cliente.
 **/
 
string ls_empty[], ls_cod_tipo_ord_ven, ls_selected
int li_i, li_j
long ll_num_riga_appartenza
boolean lb_find

as_tipi_ord_ven = ls_empty

for li_i = 1 to tab_1.page_3.dw_det_off_ven.rowcount()
	
	lb_find = false
	ls_cod_tipo_ord_ven = tab_1.page_3.dw_det_off_ven.getitemstring(li_i, "cod_tipo_ord_ven")
	
	// EnMe 28/04/2014 se è vuoto il tipo ordine in riga, provo a vedere se ce n'è uno impostato nelle opzioni.
	if isnull(ls_cod_tipo_ord_ven) then
		ls_cod_tipo_ord_ven = tab_1.page_3.dw_det_gen_ord_ven_options.getitemstring(1, "cod_tipo_ord_ven")
	end if
	
	ll_num_riga_appartenza = tab_1.page_3.dw_det_off_ven.getitemnumber(li_i, "num_riga_appartenenza")
	// stefanop: 16/01/2012: prendo in considerazione solo le righe selezionate
	ls_selected = tab_1.page_3.dw_det_off_ven.getitemstring(li_i, "selected")
	
	// controllo solo le righe "padri"
	//if not isnull(ll_num_riga_appartenza) and ll_num_riga_appartenza > 1 and ls_selected = "N" and isnull(ls_cod_tipo_ord_ven) then continue
	if isnull(ls_cod_tipo_ord_ven) or len(ls_cod_tipo_ord_ven) < 1 or ls_selected = "N" or ll_num_riga_appartenza > 0 then continue
		
	tab_1.page_3.dw_det_off_ven.setitem(li_i, "cod_tipo_ord_ven",ls_cod_tipo_ord_ven)

	// è già stato inserito il tipo ordine?
	if not guo_functions.uof_in_array(ls_cod_tipo_ord_ven, as_tipi_ord_ven[]) then
		as_tipi_ord_ven[upperbound(as_tipi_ord_ven) + 1] = ls_cod_tipo_ord_ven
	end if
next
end subroutine

public function integer wf_genera_ordine (string as_cod_tipo_ord_ven, date adt_data_consegna);long			ll_anno_registrazione, ll_num_registrazione, ll_i, ll_anno_offerta, &
				ll_num_offerta, ll_prog_riga_off_ven,ll_num_sequenza, ll_num_riga_appartenenza, ll_j, &
				ll_prog_riga_ord_ven, ll_anno_documento, ll_num_documento, ll_num_revisione, ll_riga
				
string			ls_cod_tipo_ord_ven, ls_cod_tipo_off_ven, ls_nota_testata, ls_cod_cliente, &
				ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, &
				ls_frazione, ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione, &
				ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
				ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, ls_null,&
				ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, &
				ls_cod_tipo_det_ven, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, &
				ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_ven, &
				ls_cod_tipo_analisi_off, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, &
				ls_aspetto_beni, ls_causale_trasporto, ls_flag_riep_bol, ls_flag_riep_fat, &
				ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, & 
				ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione, ls_flag_gen_commessa, & 
				ls_cod_prodotto_variante,ls_flag_esclusione,ls_formula_tempo,ls_des_estesa,ls_flag_materia_prima , &
				ls_db, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det,&
				ls_cod_fornitore,ls_nota_fissa_video, ls_messaggio, ls_prog_riga[], ls_empty[], ls_prog_riga_in, ls_det_sql, &
				ls_cod_documento, ls_numeratore_documento, ls_cod_giro_consegna, ls_cod_operatore_new, ls_errore, &
				ls_flag_prezzo_bloccato, ls_cod_tipo_det_ven_prodotto, ls_cod_tipo_det_ven_addizionali, ls_cod_tipo_det_ven_optionals,&
				ls_cod_tipo_det_ven_prodotto_sost, ls_cod_tipo_det_ven_addizionali_sost, ls_cod_tipo_det_ven_optionals_sost, &
				ls_cod_modello, ls_flag_disgrega_ordini_diversi, ls_testo_formula, ls_risultato
		 
datetime		ldt_data_registrazione, ldt_data_consegna, ldt_data_consegna_det, ldt_data_pronto

dec{4}		ld_sconto, ld_tot_val_offerta, ld_quan_offerta, ld_prezzo_vendita, ld_sconto_1, ld_sconto_2, &
				ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
				ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_peso_netto_riga, &
				ld_provvigione_2, ld_val_riga, ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x, & 
				ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_coef_calcolo, ld_num_confezioni, ld_num_pezzi_confezione, &
				ld_quantita_um, ld_prezzo_um, ldd_dim_opt_x, ldd_dim_opt_y
				
dec{5}		ld_fat_conversione_ven, ld_cambio_ven
integer		li_risposta

uo_funzioni_1								luo_funzioni_1
uo_calcola_documento_euro			luo_calcolo
uo_spese_trasporto						luo_spese_trasporto
s_cs_xx_parametri							l_progressivi
uo_formule_calcolo 						luo_formule

setpointer(hourglass!)

Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

ls_flag_disgrega_ordini_diversi = tab_1.page_3.dw_det_gen_ord_ven_options.getitemstring(1,"flag_disgrega_ordini_diversi")
if isnull(ls_flag_disgrega_ordini_diversi) then ls_flag_disgrega_ordini_diversi = "N"

ll_riga = tab_1.page_2.dw_tes_gen_ord_ven.getrow()
ll_anno_registrazione = f_anno_esercizio()
setnull(ls_null)

if ib_gibus then
	ll_num_registrazione = guo_functions.uof_gen_num_ordine( today() )
else
	select con_ord_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_ord_ven
	where  con_ord_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	ll_num_registrazione ++
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante la lettura della tabella Controllo Ordini Vendita.", sqlca)
		return -1
	end if
	
	update con_ord_ven
	set    con_ord_ven.num_registrazione = :ll_num_registrazione
	where  con_ord_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante la scrittura sulla tabella Controllo Ordini Vendita.", sqlca)
		return -1
	end if
end if

ls_cod_tipo_off_ven = tab_1.page_2.dw_tes_gen_ord_ven.getitemstring(ll_riga, "cod_tipo_off_ven")

select tab_tipi_off_ven.cod_tipo_ord_ven,  
		 tab_tipi_off_ven.cod_tipo_analisi  
into   :ls_cod_tipo_ord_ven,  
		 :ls_cod_tipo_analisi_off
from   tab_tipi_off_ven  
where  tab_tipi_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_off_ven.cod_tipo_off_ven = :ls_cod_tipo_off_ven;

// NUOVO
if len(as_cod_tipo_ord_ven) > 0 then ls_cod_tipo_ord_ven = as_cod_tipo_ord_ven
// ----

if isnull(ls_cod_tipo_ord_ven) then
	g_mb.error("Per questo tipo offerta non è specificato il relativo tipo ordine.", sqlca)
	return -1
end if

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la lettura della tabella Tipi Offerte Vendita.", sqlca)
	return -1
end if

ll_anno_offerta = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(ll_riga, "anno_registrazione")
ll_num_offerta = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(ll_riga, "num_registrazione")

select tes_off_ven.cod_cliente,   
		 tes_off_ven.cod_operatore,   
		 tes_off_ven.cod_des_cliente,   
		 tes_off_ven.rag_soc_1,   
		 tes_off_ven.rag_soc_2,   
		 tes_off_ven.indirizzo,   
		 tes_off_ven.localita,   
		 tes_off_ven.frazione,   
		 tes_off_ven.cap,   
		 tes_off_ven.provincia,   
		 tes_off_ven.cod_deposito,   
		 tes_off_ven.cod_ubicazione,   
		 tes_off_ven.cod_valuta,   
		 tes_off_ven.cambio_ven,   
		 tes_off_ven.cod_tipo_listino_prodotto,   
		 tes_off_ven.cod_pagamento,   
		 tes_off_ven.sconto,   
		 tes_off_ven.cod_agente_1,
		 tes_off_ven.cod_agente_2,
		 tes_off_ven.cod_banca,   
		 tes_off_ven.cod_imballo,   
		 tes_off_ven.aspetto_beni,
		 tes_off_ven.peso_netto,
		 tes_off_ven.peso_lordo,
		 tes_off_ven.num_colli,
		 tes_off_ven.cod_vettore,   
		 tes_off_ven.cod_inoltro,   
		 tes_off_ven.cod_mezzo,
		 tes_off_ven.causale_trasporto,
		 tes_off_ven.cod_porto,   
		 tes_off_ven.cod_resa,   
		 tes_off_ven.nota_testata,   
		 tes_off_ven.nota_piede,   
		 tes_off_ven.flag_riep_bol,
		 tes_off_ven.flag_riep_fat,
		 tes_off_ven.tot_val_offerta,
		 tes_off_ven.data_consegna,   
		 tes_off_ven.cod_banca_clien_for,
		 tes_off_ven.flag_doc_suc_tes, 
		 tes_off_ven.flag_doc_suc_pie,
		 tes_off_ven.cod_documento,
		 tes_off_ven.anno_documento,
		 tes_off_ven.numeratore_documento,
		 tes_off_ven.num_documento,
		 tes_off_ven.num_revisione
 into  :ls_cod_cliente,   
		 :ls_cod_operatore,   
		 :ls_cod_des_cliente,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,   
		 :ls_cod_deposito,   
		 :ls_cod_ubicazione,
		 :ls_cod_valuta,   
		 :ld_cambio_ven,   
		 :ls_cod_tipo_listino_prodotto,   
		 :ls_cod_pagamento,   
		 :ld_sconto_testata,   
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_banca,   
		 :ls_cod_imballo,   
		 :ls_aspetto_beni,
		 :ld_peso_netto,
		 :ld_peso_lordo,
		 :ld_num_colli,
		 :ls_cod_vettore,   
		 :ls_cod_inoltro,   
		 :ls_cod_mezzo,   
		 :ls_causale_trasporto,
		 :ls_cod_porto,
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ls_flag_riep_bol,
		 :ls_flag_riep_fat,
		 :ld_tot_val_offerta,
		 :ldt_data_consegna,   
		 :ls_cod_banca_clien_for,
		 :ls_flag_doc_suc_tes, 
		 :ls_flag_doc_suc_pie,
		 :ls_cod_documento,
		 :ll_anno_documento,
		 :ls_numeratore_documento,
		 :ll_num_documento,
		 :ll_num_revisione
from   tes_off_ven  
where  tes_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_off_ven.anno_registrazione = :ll_anno_offerta and  
		 tes_off_ven.num_registrazione = :ll_num_offerta;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la lettura Testata Offerte Vendita.", sqlca)
	return -1
end if

ldt_data_registrazione = datetime(today())
ldt_data_pronto  = wf_calcola_data_pronto(ldt_data_consegna)
ls_cod_giro_consegna = wf_calcola_giro_consegna(ls_cod_cliente)

// controllo se trasportare la nota testata e piede nel documento successivo.
luo_formule = create uo_formule_calcolo
if not isnull(ls_nota_testata) then
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		into :ls_flag_doc_suc_tes
		from tab_tipi_off_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_off_ven = :ls_cod_tipo_off_ven;
	end if		
	
	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	else	
		// devo convertire la nota di testata e piede da RTF a TESTO NORMALE
		ls_testo_formula = g_str.format("striprtf('$1')",ls_nota_testata)
		if luo_formule.uof_eval_with_datastore(ls_testo_formula, ls_risultato, ls_errore) < 0 then
			g_mb.error("Errore durante il trasporto della nota di testata")
			return -1
		else
			ls_nota_testata = ls_risultato
		end if
	end if			
else
	// se null metto stringa vuota
	ls_nota_testata = ""
end if

if not isnull(ls_nota_piede) then
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		into :ls_flag_doc_suc_pie
		from tab_tipi_off_ven
		where 
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_off_ven = :ls_cod_tipo_off_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	else	
		// devo convertire la nota di testata e piede da RTF a TESTO NORMALE
		ls_testo_formula = g_str.format("striprtf('$1')",ls_nota_piede)
		if luo_formule.uof_eval_with_datastore(ls_testo_formula, ls_risultato, ls_errore) < 0 then
			g_mb.error("Errore durante il trasporto della nota di testata")
			return -1
		else
			ls_nota_piede = ls_risultato
		end if
	end if				
else
		ls_nota_piede = ""
end if
// -------------------------- Creazione della nota fissa durante generazione documenti -------------------
setnull(ls_cod_fornitore)

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_ORD_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_fissa_video ) < 0 then
	g_mb.error(ls_nota_testata)
else
	if len(ls_nota_fissa_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_fissa_video)
end if


// --------------------------------------------------------------------------------------------------------

// metto il riferimento all'offerta
// se ho già scritto qualcosa nella nota di testata vado a capo
if len(ls_nota_testata) > 0 then ls_nota_testata = ls_nota_testata+ "~r~n"
ls_nota_testata = "Rif. Ns. Offerta N. " + ls_cod_documento + "-" + string(ll_anno_documento) + "/" + ls_numeratore_documento + "-" + string(ll_num_documento) + " rev." + string(ll_num_revisione) + "~r~n" + ls_nota_testata
// ----

//prima di fare la insert metti l'operatore collegato per il nuovo documento
if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
	ls_cod_operatore = ls_cod_operatore_new
end if

//####################################################################
//testata uguale per tutti, se poi siamo in gibus segue update
insert into tes_ord_ven (cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	data_registrazione,   
	cod_operatore,   
	cod_tipo_ord_ven,   
	cod_cliente,   
	cod_des_cliente,   
	rag_soc_1,   
	rag_soc_2,   
	indirizzo,   
	localita,   
	frazione,   
	cap,   
	provincia,   
	cod_deposito,   
	cod_ubicazione,
	cod_valuta,   
	cambio_ven,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	sconto,   
	cod_agente_1,
	cod_agente_2,
	cod_banca,   
	num_ord_cliente,   
	data_ord_cliente,   
	cod_imballo,   
	aspetto_beni,
	peso_netto,
	peso_lordo,
	num_colli,
	cod_vettore,   
	cod_inoltro,   
	cod_mezzo,
	causale_trasporto,
	cod_porto,   
	cod_resa,   
	nota_testata,   
	nota_piede,   
	flag_fuori_fido,
	flag_blocco,   
	flag_evasione,  
	flag_riep_bol,
	flag_riep_fat,
	tot_val_evaso,
	tot_val_ordine,   
	data_consegna,   
	cod_banca_clien_for,
	flag_stampato,
	cod_giro_consegna,
	flag_doc_suc_tes,
	flag_st_note_tes,					
	flag_doc_suc_pie,
	flag_st_note_pie
) values (
	:s_cs_xx.cod_azienda,   
	:ll_anno_registrazione,   
	:ll_num_registrazione,   
	:ldt_data_registrazione,   
	:ls_cod_operatore,   
	:ls_cod_tipo_ord_ven,   
	:ls_cod_cliente,   
	:ls_cod_des_cliente,   
	:ls_rag_soc_1,   
	:ls_rag_soc_2,   
	:ls_indirizzo,   
	:ls_localita,   
	:ls_frazione,   
	:ls_cap,   
	:ls_provincia,   
	:ls_cod_deposito,   
	:ls_cod_ubicazione,
	:ls_cod_valuta,   
	:ld_cambio_ven,   
	:ls_cod_tipo_listino_prodotto,   
	:ls_cod_pagamento,   
	:ld_sconto_testata,
	:ls_cod_agente_1,
	:ls_cod_agente_2,
	:ls_cod_banca,   
	null,   
	null,   
	:ls_cod_imballo,   
	:ls_aspetto_beni,
	:ld_peso_netto,
	:ld_peso_lordo,
	:ld_num_colli,
	:ls_cod_vettore,   
	:ls_cod_inoltro,   
	:ls_cod_mezzo,   
	:ls_causale_trasporto,
	:ls_cod_porto,   
	:ls_cod_resa,   
	:ls_nota_testata,   
	:ls_nota_piede,   
	'N',
	'N',   
	'A',
	:ls_flag_riep_bol,
	:ls_flag_riep_fat,
	0,   
	:ld_tot_val_offerta,   
	:ldt_data_consegna,   
	:ls_cod_banca_clien_for,
	'N',
	:ls_cod_giro_consegna,
	'I',
	'I',
	'I',
	'I');
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la creazione della Testata Ordine Vendita.", sqlca)
	return -1
end if

//personalizzazione per GIBUS (campo data_pronto)
if ib_gibus then
	//donato: ottimizzato il codice nel caso Gibus faccio solo un update dei campi personalizzati
	update 	tes_ord_ven
	set 		data_pronto=:ldt_data_pronto
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_registrazione and
				num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante l'aggiornamento data pronto creazione della Testata Ordine Vendita.", sqlca)
		return -1
	end if
end if
//####################################################################

// stefanop : 24/10/2011 : carico solo i dettagli selezionati
ls_prog_riga_in = wf_righe_dettaglio_selezionate(true, ls_cod_tipo_ord_ven, adt_data_consegna)
ll_prog_riga_ord_ven = 0
//

ls_det_sql = "select	det_off_ven.prog_riga_off_ven,"+&
							"det_off_ven.cod_tipo_det_ven,"+&
							"det_off_ven.cod_misura,"+&
							"det_off_ven.cod_prodotto,"+&
							"det_off_ven.des_prodotto,"+&
							"det_off_ven.quan_offerta,"+&
							"det_off_ven.prezzo_vendita,"+&
							"det_off_ven.fat_conversione_ven,"+&
							"det_off_ven.sconto_1,"+&
							"det_off_ven.sconto_2,"+&
							"det_off_ven.provvigione_1,"+&
							"det_off_ven.provvigione_2,"+&
							"det_off_ven.val_riga,"+&
							"det_off_ven.cod_iva,"+&
							"det_off_ven.data_consegna,"+&
							"det_off_ven.nota_dettaglio,"+&
							"det_off_ven.sconto_3,"+&
							"det_off_ven.sconto_4,"+&
							"det_off_ven.sconto_5,"+&
							"det_off_ven.sconto_6,"+&
							"det_off_ven.sconto_7,"+&
							"det_off_ven.sconto_8,"+&
							"det_off_ven.sconto_9,"+&
							"det_off_ven.sconto_10,"+&
							"det_off_ven.cod_centro_costo,"+&
							"det_off_ven.cod_versione,"+&
							"det_off_ven.num_confezioni,"+&
							"det_off_ven.num_pezzi_confezione,"+&
							"det_off_ven.flag_gen_commessa,"+&
							"det_off_ven.num_riga_appartenenza,"+&
							"det_off_ven.flag_doc_suc_det,"+&
							"det_off_ven.quantita_um,"+&
							"det_off_ven.prezzo_um,"+&
							"det_off_ven.flag_prezzo_bloccato,"
							
if ib_gibus then
	ls_det_sql += "det_off_ven.dim_x, det_off_ven.dim_y "
else
	ls_det_sql += "0.00 as dim_x, 0.00 as dim_y "
end if

ls_det_sql += 	"from 	det_off_ven "+&
					"where	det_off_ven.cod_azienda ='" + s_cs_xx.cod_azienda + "' and  "+&
								"det_off_ven.anno_registrazione = " + string(ll_anno_offerta) + " and "+&
								"det_off_ven.num_registrazione = " + string(ll_num_offerta) + " and "+&
								"det_off_ven.prog_riga_off_ven in ("+ ls_prog_riga_in + ")  "
	
// genero ordini diversi per diverse date di consegna
if ls_flag_disgrega_ordini_diversi="S" then
	
	if isnull(adt_data_consegna) then
		ls_det_sql += " AND det_off_ven.data_consegna is null "
	else
		ls_det_sql += " AND det_off_ven.data_consegna = '" +string(adt_data_consegna, s_cs_xx.db_funzioni.formato_data) + "' "
	end if
	
end if
	
ls_det_sql += " order by det_off_ven.prog_riga_off_ven asc"
		
declare cu_det_off_ven dynamic cursor for sqlsa;
prepare sqlsa from :ls_det_sql;
open dynamic cu_det_off_ven;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante l'apertura del cursore dei dettagli offerte vendita", sqlca)
	return -1
end if

do while 0 = 0
	fetch cu_det_off_ven into :ll_prog_riga_off_ven, :ls_cod_tipo_det_ven, 
			:ls_cod_misura, :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_offerta, 
			:ld_prezzo_vendita, :ld_fat_conversione_ven, :ld_sconto_1, :ld_sconto_2, 
			:ld_provvigione_1, :ld_provvigione_2, :ld_val_riga, :ls_cod_iva, 
			:ldt_data_consegna_det, :ls_nota_dettaglio, :ld_sconto_3, :ld_sconto_4, 
			:ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, :ld_sconto_9, 
			:ld_sconto_10, :ls_cod_centro_costo,:ls_cod_versione, :ld_num_confezioni, 
			:ld_num_pezzi_confezione, :ls_flag_gen_commessa, :ll_num_riga_appartenenza, :ls_flag_doc_suc_det,
			:ld_quantita_um, :ld_prezzo_um, :ls_flag_prezzo_bloccato, :ldd_dim_opt_x, :ldd_dim_opt_y;

	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		g_mb.error("Errore durante la lettura Dettagli Offerte Vendita.", sqlca)
		close cu_det_off_ven;
		return -1
	end if
	
	select tab_tipi_det_ven.flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode = -1 then
		g_mb.error("Si è verificato un errore in fase di lettura tipi dettagli.", sqlca)
		close cu_det_off_ven;
		return -1
	end if

//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc
		into :ls_flag_doc_suc_det
		from tab_tipi_det_ven
		where 
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		

	//calcolo la PK della riga (prog_riga_ord_ven) ###############################
	
	//max prog_riga_prog_ven in det_ord_ven
	select max(prog_riga_ord_ven)
	into :ll_prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_registrazione and
				num_registrazione=:ll_num_registrazione;
				
	if sqlca.sqlcode<0 then
		g_mb.error("Si è verificato un errore in lettura max prog.riga.", sqlca)
		close cu_det_off_ven;
		return -1
	end if
	
	if isnull(ll_prog_riga_ord_ven) then ll_prog_riga_ord_ven = 0
	
	
	//in ll_num_riga_appartenenza c'è il valore della riga di offerta
	//a cui è collegata la riga di offerta che si vuole trasformare in riga ordine
	if isnull(ll_num_riga_appartenenza) or ll_num_riga_appartenenza < 1 then
		//riga principale: calcolo prog_riga_ord_ven come valore + alto sommato a 10
		//e poi lo trasformo nel più vicino divisibile per 10
		//es. se MAX( prog_riga_ord_ven) = 35 allora aggiungo 10, ottenendo 45 e poi considero 40
		//     se MAX( prog_riga_ord_ven) = 30 allora aggiungo 10, ottenendo 40 e mi va bene cosi ...
		ll_prog_riga_ord_ven += 10
	
		if mod(ll_prog_riga_ord_ven, 10)<>0 then
			//non divisibile per 10
			//es. 45  -->   45 / 10 = 4.5 -> trasformo in intero ottengo 4  --> 4 * 10 = 40
			ll_prog_riga_ord_ven = long(ll_prog_riga_ord_ven / 10)  * 10
		else
			//mi sta bene il valore presente in ll_prog_riga_ord_ven
		end if
		
		wf_conserva_progressivi(l_progressivi, ll_num_offerta, ll_prog_riga_off_ven, ll_prog_riga_ord_ven)
	else
		//riga collegata: incremento di 1 il prog_riga_ord_ven da dare
		//e poi leggo il corrispondente num riga appartenenza da aassegnare
		ll_prog_riga_ord_ven += 1
		ll_num_riga_appartenenza = wf_leggi_num_appartenenza(l_progressivi, ll_num_offerta, ll_num_riga_appartenenza)
	end if
	//#######################################################


	if ib_gibus then
		ls_flag_prezzo_bloccato = "S"
	end if
	
	if ib_gibus then
		// Verifico il tipo ordine ed eventualmente gestisco la trasformazione del tipo dettaglio per la questione degli ordini di campionatura.
		if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
			select 	cod_tipo_det_ven_prodotto
			into		:ls_cod_tipo_det_ven_prodotto
			from 		tab_flags_configuratore
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_modello = :ls_cod_prodotto;
			if sqlca.sqlcode = 0 then
				select cod_tipo_det_ven_prodotto
				into	:ls_cod_tipo_det_ven_prodotto
				from 	tab_flags_conf_tipi_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						 cod_modello = :ls_cod_prodotto and
						 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
				if sqlca.sqlcode = 0 then
					ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_prodotto
				end if
			end if
		else
			select cod_prodotto
			into   :ls_cod_modello
			from det_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ord_ven = :ll_num_riga_appartenenza;
			if sqlca.sqlcode = 0 then
				select 	cod_tipo_det_ven_addizionali,
							cod_tipo_det_ven_optionals
				into		:ls_cod_tipo_det_ven_addizionali,
							:ls_cod_tipo_det_ven_optionals
				from 		tab_flags_configuratore
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							cod_modello = :ls_cod_modello;
				if sqlca.sqlcode = 0 then
					select 	cod_tipo_det_ven_addizionali,
								cod_tipo_det_ven_optionals
					into		:ls_cod_tipo_det_ven_addizionali_sost,
								:ls_cod_tipo_det_ven_optionals_sost
					from tab_flags_conf_tipi_ord_ven
					where cod_azienda = :s_cs_xx.cod_azienda and
							 cod_modello = :ls_cod_modello and
							 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
					if sqlca.sqlcode = 0 then
						// si tratta di un'addizionale
						if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then
							ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali_sost
						end if
						// si tratta di optional
						if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_optionals then
							ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_optionals_sost
						end if
					end if
				end if
			end if
		end if
	
	end if
	
	// stefanop 06/06/2012: se la data di consegna del dettaglio è vuota imposto quella della testata
	if isnull(ldt_data_consegna_det) then ldt_data_consegna_det = ldt_data_consegna
	// ----
	
	insert into det_ord_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_ord_ven,   
		cod_tipo_det_ven,   
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_ordine,   
		prezzo_vendita,   
		fat_conversione_ven,   
		sconto_1,   
		sconto_2,   
		provvigione_1,
		provvigione_2,
		cod_iva,   
		data_consegna,   
		val_riga,
		quan_in_evasione,   
		quan_evasa,
		flag_evasione,   
		flag_blocco,   
		nota_dettaglio,   
		anno_registrazione_off,   
		num_registrazione_off,   
		prog_riga_off_ven,
		anno_commessa,
		num_commessa,
		cod_centro_costo,
		sconto_3,   
		sconto_4,   
		sconto_5,   
		sconto_6,   
		sconto_7,   
		sconto_8,   
		sconto_9,   
		sconto_10,
		cod_versione,
		num_confezioni,
		num_pezzi_confezione,
		flag_gen_commessa,
		num_riga_appartenenza,
		flag_doc_suc_det,
		flag_st_note_det,
		quantita_um,
		prezzo_um,
		flag_prezzo_bloccato
	) values (
		:s_cs_xx.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		:ll_prog_riga_ord_ven,
		:ls_cod_tipo_det_ven,   
		:ls_cod_misura,   
		:ls_cod_prodotto,   
		:ls_des_prodotto,   
		:ld_quan_offerta,   
		:ld_prezzo_vendita,   
		:ld_fat_conversione_ven,   
		:ld_sconto_1,   
		:ld_sconto_2,  
		:ld_provvigione_1,
		:ld_provvigione_2,
		:ls_cod_iva,   
		:ldt_data_consegna_det,   
		:ld_val_riga,   
		0,
		0,
		'A',   
		'N',   
		:ls_nota_dettaglio,   
		:ll_anno_offerta,   
		:ll_num_offerta,   
		:ll_prog_riga_off_ven,
		null,   
		null,   
		:ls_cod_centro_costo,   
		:ld_sconto_3,   
		:ld_sconto_4,   
		:ld_sconto_5,   
		:ld_sconto_6,   
		:ld_sconto_7,   
		:ld_sconto_8,   
		:ld_sconto_9,   
		:ld_sconto_10,
		:ls_cod_versione,
		:ld_num_confezioni,
		:ld_num_pezzi_confezione,
		:ls_flag_gen_commessa,
		:ll_num_riga_appartenenza,
		'I',
		'I',
		:ld_quantita_um,
		:ld_prezzo_um,
		:ls_flag_prezzo_bloccato);		
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante la scrittura Dettagli Ordini Vendita.", sqlca)
		close cu_det_off_ven;
		return -1
	end if
	
	//solo per gibus, se ci sono dimensioni (optionals configurabili), le inserisco con un update
	if ib_gibus and (ldd_dim_opt_x>0 or ldd_dim_opt_y>0) then
		update det_ord_ven
		set 	dim_x=:ldd_dim_opt_x,
				dim_y=:ldd_dim_opt_y,
				flag_mrp='N'
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ll_anno_registrazione and
					num_registrazione=:ll_num_registrazione and
					prog_riga_ord_ven=:ll_prog_riga_ord_ven;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante la scrittura delle dimensioni (optional configurabile):", sqlca)
			close cu_det_off_ven;
			return -1
		end if
	end if
	//-----------------------------------------------------------------------------------------------------

	if ls_flag_tipo_det_ven = "M" then
		update anag_prodotti  
		set quan_impegnata = quan_impegnata + :ld_quan_offerta
		where 
			anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
			 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.", sqlca)
			close cu_det_off_ven;
			return -1
		end if
	end if				

	ls_tabella_orig = "det_off_ven_stat"
	ls_tabella_des = "det_ord_ven_stat"
	ls_prog_riga_doc_orig = "prog_riga_off_ven"
	ls_prog_riga_doc_des = "prog_riga_ord_ven"

	if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_offerta, ll_num_offerta, ll_prog_riga_off_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven) = -1 then
		close cu_det_off_ven;
		return -1
	end if	

	select tab_tipi_ord_ven.cod_tipo_analisi  
	into :ls_cod_tipo_analisi_ord
	from tab_tipi_ord_ven  
	where
		tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Si è verificato un errore nella lettura della Tabella Tipi Ordini.", sqlca)
		close cu_det_off_ven;
		return -1
	end if

	if ls_cod_tipo_analisi_off <> ls_cod_tipo_analisi_ord then
		if f_crea_distribuzione(ls_cod_tipo_analisi_ord, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_off_ven, 4) = -1 then
			g_mb.error("Si è verificato un errore nella Generazione dei Dettagli Statistici.", sqlca)
			close cu_det_off_ven;
			return -1
		end if
	end if

	// duplicazione delle varianti		
	INSERT INTO varianti_det_ord_ven (
		cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_ord_ven,
		cod_prodotto_padre,
		num_sequenza,
		cod_prodotto_figlio,
		cod_versione_figlio,
		cod_versione,
		cod_misura,
		cod_prodotto,
		quan_tecnica,
		quan_utilizzo,
		dim_x,
		dim_y,
		dim_z,
		dim_t,
		coef_calcolo,
		flag_esclusione,
		formula_tempo,
		des_estesa,
		flag_materia_prima,
		lead_time,
		cod_versione_variante
	) select
		:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		:ll_prog_riga_ord_ven,
		cod_prodotto_padre,
		num_sequenza,
		cod_prodotto_figlio,
		cod_versione_figlio,
		cod_versione,
		cod_misura,
		cod_prodotto,
		quan_tecnica,
		quan_utilizzo,
		dim_x,
		dim_y,
		dim_z,
		dim_t,
		coef_calcolo,
		flag_esclusione,
		formula_tempo,
		des_estesa,
		flag_materia_prima,
		lead_time,
		cod_versione_variante
	from varianti_det_off_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_offerta and
		num_registrazione = :ll_num_offerta and
		prog_riga_off_ven = :ll_prog_riga_off_ven;
	
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la creazione varianti ordine", sqlca)
		return -1
	end if

	// duplicazione delle integrazioni
	insert into integrazioni_det_ord_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_ord_ven,   
		progressivo,   
		cod_prodotto_padre,   
		cod_versione_padre,   
		num_sequenza,   
		cod_prodotto_figlio,   
		cod_versione_figlio,   
		cod_misura,   
		quan_tecnica,   
		quan_utilizzo,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,   
		coef_calcolo,   
		des_estesa,   
		cod_formula_quan_utilizzo,   
		flag_escludibile,   
		flag_materia_prima,   
		flag_arrotonda,
		lead_time
	) select 
		:s_cs_xx.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		:ll_prog_riga_ord_ven,   
		progressivo,   
		cod_prodotto_padre,   
		cod_versione_padre,   
		num_sequenza,   
		cod_prodotto_figlio,   
		cod_versione_figlio,   
		cod_misura,   
		quan_tecnica,   
		quan_utilizzo,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,   
		coef_calcolo,   
		des_estesa,   
		cod_formula_quan_utilizzo,   
		flag_escludibile,   
		flag_materia_prima,   
		flag_arrotonda,
		lead_time
		from  integrazioni_det_off_ven  
		where
			cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_offerta and
			num_registrazione=:ll_num_offerta and
			prog_riga_off_ven=:ll_prog_riga_off_ven;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la creazione integrazioni ordine.", sqlca)
		return -1
	end if
	
	// copio variabili
	  INSERT INTO det_ord_ven_conf_variabili   (
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_ord_ven,   
				  prog_variabile,   
				  cod_variabile,   
				  flag_tipo_dato,   
				  valore_stringa,   
				  valore_numero,   
				  valore_datetime,
				  des_valore,
				  flag_visible_in_stampa
				  )  
			SELECT 	:s_cs_xx.cod_azienda,   
						:ll_anno_registrazione,   
						:ll_num_registrazione,   
						:ll_prog_riga_ord_ven,   
						prog_variabile,   
						cod_variabile,   
						flag_tipo_dato,   
						valore_stringa,   
						valore_numero,   
						valore_datetime,
						des_valore,
				  		flag_visible_in_stampa
			 FROM 	det_off_ven_conf_variabili 
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:ll_anno_offerta and
						num_registrazione=:ll_num_offerta and
						prog_riga_off_ven=:ll_prog_riga_off_ven ;
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la creazione variabili ordine.", sqlca)
		return -1
	end if
	

	// copio varianti
	if ib_gibus then
		
		INSERT INTO comp_det_ord_ven (
			cod_azienda,   
			anno_registrazione,   
			num_registrazione,   
			prog_riga_ord_ven,   
			cod_prod_finito,   
			cod_modello,   
			dim_x,   
			dim_y,   
			dim_z,   
			dim_t,   
			cod_tessuto,   
			cod_non_a_magazzino,   
			cod_tessuto_mant,   
			cod_mant_non_a_magazzino,   
			flag_tipo_mantovana,   
			alt_mantovana,   
			flag_cordolo,   
			flag_passamaneria,   
			cod_verniciatura,   
			cod_comando,   
			pos_comando,   
			alt_asta,   
			cod_comando_tenda_verticale,   
			cod_colore_lastra,   
			cod_tipo_lastra,   
			spes_lastra,   
			cod_tipo_supporto,   
			flag_cappotta_frontale,   
			flag_rollo,   
			flag_kit_laterale,   
			flag_misura_luce_finita,   
			cod_tipo_stampo,   
			cod_mat_sis_stampaggio,   
			rev_stampo,   
			num_gambe,   
			num_campate,   
			flag_autoblocco,   
			num_fili_plisse,   
			num_boccole_plisse,   
			intervallo_punzoni_plisse,   
			num_pieghe_plisse,   
			prof_inf_plisse,   
			prof_sup_plisse,   
			colore_passamaneria,   
			colore_cordolo,   
			num_punzoni_plisse,   
			des_vernice_fc,   
			des_comando_1,   
			cod_comando_2,   
			des_comando_2,   
			flag_addizionale_comando_1,   
			flag_addizionale_comando_2,   
			flag_addizionale_supporto,   
			flag_addizionale_tessuto,   
			flag_addizionale_vernic,   
			flag_fc_comando_1,   
			flag_fc_comando_2,   
			flag_fc_supporto,   
			flag_fc_tessuto,   
			flag_fc_vernic,   
			flag_allegati,   
			data_ora_ar,   
			flag_tessuto_cliente,   
			flag_tessuto_mant_cliente,
			cod_verniciatura_2,
			flag_addizionale_vernic_2,
			flag_fc_vernic_2 ,
			des_vernic_fc_2  ,
			cod_verniciatura_3,
			flag_addizionale_vernic_3,
			flag_fc_vernic_3,
			des_vernic_fc_3,
			flag_azzera_dt )  
		SELECT
			:s_cs_xx.cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   	
			:ll_prog_riga_ord_ven, 
			cod_prod_finito,  
			cod_modello,   
			dim_x,   
			dim_y,   
			dim_z,   
			dim_t,
			cod_tessuto,
			cod_non_a_magazzino,   
			cod_tessuto_mant,      
			cod_mant_non_a_magazzino,
			flag_tipo_mantovana,   
			alt_mantovana,   
			flag_cordolo,   
			flag_passamaneria,   
			cod_verniciatura,   
			cod_comando,   
			pos_comando,   
			alt_asta,   
			cod_comando_tenda_verticale,   
			cod_colore_lastra,   
			cod_tipo_lastra,   
			spes_lastra,   
			cod_tipo_supporto,   
			flag_cappotta_frontale,   
			flag_rollo,   
			flag_kit_laterale,   
			flag_misura_luce_finita,   
			cod_tipo_stampo,   
			cod_mat_sis_stampaggio,   
			rev_stampo,   
			num_gambe,   
			num_campate,   
			flag_autoblocco,   
			num_fili_plisse,   
			num_boccole_plisse,   
			intervallo_punzoni_plisse,   
			num_pieghe_plisse,   
			prof_inf_plisse,   
			prof_sup_plisse,   
			colore_passamaneria,   
			colore_cordolo,   
			num_punzoni_plisse,   
			des_vernice_fc,   
			des_comando_1,   
			cod_comando_2,   
			des_comando_2,   
			flag_addizionale_comando_1,   
			flag_addizionale_comando_2,   
			flag_addizionale_supporto,   
			flag_addizionale_tessuto,   
			flag_addizionale_vernic,   
			flag_fc_comando_1,   
			flag_fc_comando_2,   
			flag_fc_supporto,   
			flag_fc_tessuto,   
			flag_fc_vernic,   
			flag_allegati,   
			data_ora_ar,   
			flag_tessuto_cliente,   
			flag_tessuto_mant_cliente,
			cod_verniciatura_2,
			flag_addizionale_vernic_2,
			flag_fc_vernic_2 ,
			des_vernic_fc_2  ,
			cod_verniciatura_3,
			flag_addizionale_vernic_3,
			flag_fc_vernic_3,
			des_vernic_fc_3,
			flag_azzera_dt
		FROM comp_det_off_ven
		WHERE
			cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_offerta and
			num_registrazione=:ll_num_offerta and
			prog_riga_off_ven=:ll_prog_riga_off_ven;
			
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante la copia della tabella comp_det_off_ven", sqlca)
			return -1
		end if
		
		
		luo_funzioni_1 = create uo_funzioni_1
		luo_funzioni_1.uof_gen_varianti_deposito_ord_ven ( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ref ls_errore )
		destroy luo_funzioni_1
		
	end if
	// ---


	//---------------------------------------------------------------------------------------------------------------------------------------------
	//calcolo il peso netto, senza gestire l'eventuale errore (fare dopo l'inserimento eventuale in comp_det_ord_ven)
	
	//il calocolo della formula viene fatto in base alle dimensioni x,y e a volte con variabile sul codice prodotto
	//se 'è la comp_det_ord_ven allora le dimensioni stanno lì, al limite ono in det_ord_ven (caso OPTIONALS configurabili)
	//se cosi non è la funzione di calcolo imposta l'eventuale peso netto presente in anagrafica prodotti
	luo_funzioni_1 = create uo_funzioni_1
	ld_peso_netto_riga = 0
	luo_funzioni_1.uof_calcola_peso_riga_ordine( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ld_peso_netto_riga, ls_errore)
	destroy luo_funzioni_1
	
	ls_errore = ""
	if isnull(ld_peso_netto_riga) then ld_peso_netto_riga = 0
	
	update det_ord_ven
	set 	peso_netto = :ld_peso_netto_riga
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_registrazione and
				num_registrazione=:ll_num_registrazione and
				prog_riga_ord_ven=:ll_prog_riga_ord_ven;
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//--------------------------------------------
	// stefanop 19/02/2014
	luo_spese_trasporto = create uo_spese_trasporto
	li_risposta = luo_spese_trasporto.uof_imposta_spese_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, false, ref ls_errore)
	destroy luo_spese_trasporto;
	if li_risposta < 0 then
		g_mb.error("Attenzione", ls_errore)
		return -1
	end if
	

loop

close cu_det_off_ven;

int li_r 

li_r = tab_1.page_4.dw_gen_orig_dest.insertrow(0)
tab_1.page_4.dw_gen_orig_dest.setrow(li_r)
tab_1.page_4.dw_gen_orig_dest.setitem(li_r, "anno_registrazione_orig", ll_anno_offerta)
tab_1.page_4.dw_gen_orig_dest.setitem(li_r, "num_registrazione_orig", ll_num_offerta)
tab_1.page_4.dw_gen_orig_dest.setitem(li_r, "anno_registrazione_dest", ll_anno_registrazione)
tab_1.page_4.dw_gen_orig_dest.setitem(li_r, "num_registrazione_dest", ll_num_registrazione)
tab_1.page_4.dw_gen_orig_dest.setitem(li_r, "flag_tipo_riga", "OFFVEN")

luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"ord_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

setpointer(arrow!)
end function

public function string wf_righe_dettaglio_selezionate (boolean ab_righe_collegate, string as_cod_tipo_ord_ven, date adt_data_consegna);/**
 * stefanop
 * 25/10/2011
 *
 * Controllo quali sono le righe di dettaglio da copiare
 *
 * Parametri:
 * 		ab_righe_collegate = TRUE se voglio recupeare anche le righe collegate, FALSE solo quelle di testata
 **/

string ls_prog_righe[], ls_prog_riga,ls_flag_disgrega_ordini_diversi
long ll_i, ll_j, ll_prog_riga, ll_num_riga_appartenenza, ll_rowcount
date ldt_data_consegna

ll_rowcount = tab_1.page_3.dw_det_off_ven.rowcount()
ls_flag_disgrega_ordini_diversi = tab_1.page_3.dw_det_gen_ord_ven_options.getitemstring(1,"flag_disgrega_ordini_diversi")
if isnull(ls_flag_disgrega_ordini_diversi) then ls_flag_disgrega_ordini_diversi = "N"
	
for ll_i = 1 to ll_rowcount
	if tab_1.page_3.dw_det_off_ven.getitemstring(ll_i, "selected") = "S" and tab_1.page_3.dw_det_off_ven.getitemstring(ll_i, "cod_tipo_ord_ven") = as_cod_tipo_ord_ven  then
		ll_prog_riga = tab_1.page_3.dw_det_off_ven.getitemnumber(ll_i, "prog_riga_off_ven")
		ll_num_riga_appartenenza = tab_1.page_3.dw_det_off_ven.getitemnumber(ll_i, "num_riga_appartenenza")
		ldt_data_consegna = date(tab_1.page_3.dw_det_off_ven.getitemdatetime(ll_i, "data_consegna"))
		
		if ls_flag_disgrega_ordini_diversi="S" then
			// considero la data di scadenza
			if ldt_data_consegna <> adt_data_consegna then continue
		end if
		
		ls_prog_righe[upperbound(ls_prog_righe) + 1] = string(ll_prog_riga)
	
		// non voglio recuperare le righe collegate ma solo quelle di testata
		if not ab_righe_collegate then continue

		// recupeo righe collegate
		ll_j = 0
		do
			ll_j = tab_1.page_3.dw_det_off_ven.find("num_riga_appartenenza=" + string(ll_prog_riga) + "", ll_j, ll_rowcount)
				
			// controllo righe collegate
			if ll_j > 0 then 
				ls_prog_righe[upperbound(ls_prog_righe) + 1] = string(tab_1.page_3.dw_det_off_ven.getitemnumber(ll_j, "prog_riga_off_ven"))
				ll_j++
			end if
			
			// se sono arrivato all'ultima riga esco altrimenti il find mi va in loop
			if ll_j = (ll_rowcount + 1) then exit
		loop while ll_j > 0
	end if
next

return guo_functions.uof_implode(ls_prog_righe, ",")
end function

public function string wf_righe_dettaglio_selezionate (boolean ab_righe_collegate, date adt_data_consegna);/**
 * stefanop
 * 25/10/2011
 *
 * Controllo quali sono le righe di dettaglio da copiare
 *
 * Parametri:
 * 		ab_righe_collegate = TRUE se voglio recupeare anche le righe collegate, FALSE solo quelle di testata
 **/

string ls_prog_righe[], ls_prog_riga,ls_flag_disgrega_ordini_diversi
long ll_i, ll_j, ll_prog_riga, ll_num_riga_appartenenza, ll_rowcount
date ldt_data_consegna

ll_rowcount = tab_1.page_3.dw_det_off_ven.rowcount()
ls_flag_disgrega_ordini_diversi = tab_1.page_3.dw_det_gen_ord_ven_options.getitemstring(1,"flag_disgrega_ordini_diversi")
if isnull(ls_flag_disgrega_ordini_diversi) then ls_flag_disgrega_ordini_diversi = "N"
	
for ll_i = 1 to ll_rowcount
	if tab_1.page_3.dw_det_off_ven.getitemstring(ll_i, "selected") = "S" then
		ll_prog_riga = tab_1.page_3.dw_det_off_ven.getitemnumber(ll_i, "prog_riga_off_ven")
		ll_num_riga_appartenenza =tab_1.page_3. dw_det_off_ven.getitemnumber(ll_i, "num_riga_appartenenza")
		ldt_data_consegna = date(tab_1.page_3.dw_det_off_ven.getitemdatetime(ll_i, "data_consegna"))
		
		if ls_flag_disgrega_ordini_diversi="S" then
			// considero la data di scadenza
			if ldt_data_consegna <> adt_data_consegna then continue
		end if
		
		ls_prog_righe[upperbound(ls_prog_righe) + 1] = string(ll_prog_riga)
	
		// non voglio recuperare le righe collegate ma solo quelle di testata
		if not ab_righe_collegate then continue

		// recupeo righe collegate
		ll_j = 0
		do
			ll_j = tab_1.page_3.dw_det_off_ven.find("num_riga_appartenenza=" + string(ll_prog_riga) + "", ll_j, ll_rowcount)
				
			// controllo righe collegate
			if ll_j > 0 then 
				ls_prog_righe[upperbound(ls_prog_righe) + 1] = string(tab_1.page_3.dw_det_off_ven.getitemnumber(ll_j, "prog_riga_off_ven"))
				ll_j++
			end if
			
			// se sono arrivato all'ultima riga esco altrimenti il find mi va in loop
			if ll_j = (ll_rowcount + 1) then exit
		loop while ll_j > 0
		
	end if
next

return guo_functions.uof_implode(ls_prog_righe, ",")
end function

public function datetime wf_calcola_data_pronto (datetime adt_data_consegna);long ll_giorno
datetime ldt_data_pronto

ll_giorno = daynumber(date(adt_data_consegna))
choose case ll_giorno
	case 1    // domenica
		ldt_data_pronto = datetime(relativedate(date(adt_data_consegna), -2), 00:00:00)
	case 2	 // lunedì
		ldt_data_pronto = datetime(relativedate(date(adt_data_consegna), -4), 00:00:00)
	case 3	// martedì
		ldt_data_pronto = datetime(relativedate(date(adt_data_consegna), -4), 00:00:00)
	case 4	// mercoledì
		ldt_data_pronto = datetime(relativedate(date(adt_data_consegna), -2), 00:00:00)
	case 5	// giovedì
		ldt_data_pronto = datetime(relativedate(date(adt_data_consegna), -2), 00:00:00)
	case 6	// venerdì
		ldt_data_pronto = datetime(relativedate(date(adt_data_consegna), -2), 00:00:00)
	case 7	// sabato 
		ldt_data_pronto = datetime(relativedate(date(adt_data_consegna), -2), 00:00:00)
end choose

return ldt_data_pronto

end function

public function string wf_calcola_giro_consegna (string as_cod_cliente);string ls_return
long ll_cont
datetime ldt_oggi

setnull(ls_return)
ldt_oggi = datetime(today(),00:00:00)
				
select count(*)
into   :ll_cont
from   det_giri_consegne, tes_giri_consegne
where  det_giri_consegne.cod_azienda = tes_giri_consegne.cod_azienda and
		 det_giri_consegne.cod_giro_consegna = tes_giri_consegne.cod_giro_consegna and
		 det_giri_consegne.cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :as_cod_cliente and
		 ( (flag_blocco <> 'S') or 
		    (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			 
if ll_cont = 0 then
	setnull(ls_return)
	
elseif ll_cont = 1 then
	select det_giri_consegne.cod_giro_consegna
	into :ls_return
	from	det_giri_consegne, tes_giri_consegne
	where	det_giri_consegne.cod_azienda = tes_giri_consegne.cod_azienda and
				det_giri_consegne.cod_giro_consegna = tes_giri_consegne.cod_giro_consegna and
				det_giri_consegne.cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :as_cod_cliente and
				( (flag_blocco <> 'S') or 
				(flag_blocco = 'S' and data_blocco > :ldt_oggi));
				
	if sqlca.sqlcode <> 0 then setnull(ls_return)

end if

return ls_return
end function

public function integer wf_inserisci_righe_collegate (long al_num_riga_off_appartenenza, long al_num_riga_ord_appartenenza, ref string as_messaggio);long			ll_prog_riga_off_ven, ll_num_riga_appartenenza

string			ls_cod_tipo_det_ven, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_cod_versione, &
				ls_flag_gen_commessa

dec{4}		ld_quan_offerta, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, ld_provvigione_1, ld_provvigione_2, ld_val_riga, ld_sconto_3, &
				ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_num_confezioni, ld_num_pezzi_confezione
				
datetime		ldt_data_consegna_det



//
//for ll_index_1=1 to ll_tot_1
//	//questo ciclo legge solo le righe do offerta principali, cioè con num_riga_appartenenza vuota o zero
//	//------------------------------------------------------------------------------------------------------------------------
//	ll_prog_riga_off_ven				= lds_dati_principali.getitemnumber(			ll_index_1, 1)
//	ls_cod_tipo_det_ven				= lds_dati_principali.getitemstring(			ll_index_1, 2)
//	ls_cod_misura						= lds_dati_principali.getitemstring(			ll_index_1, 3)
//	ls_cod_prodotto					= lds_dati_principali.getitemstring(			ll_index_1, 4)
//	ls_des_prodotto					= lds_dati_principali.getitemstring(			ll_index_1, 5)
//	ld_quan_offerta					= lds_dati_principali.getitemnumber(			ll_index_1, 6)
//	ld_prezzo_vendita					= lds_dati_principali.getitemnumber(			ll_index_1, 7)
//	ld_fat_conversione_ven			= lds_dati_principali.getitemnumber(			ll_index_1, 8)
//	ld_sconto_1							= lds_dati_principali.getitemnumber(			ll_index_1, 9)
//	ld_sconto_2							= lds_dati_principali.getitemnumber(			ll_index_1, 10)
//	ld_provvigione_1					= lds_dati_principali.getitemnumber(			ll_index_1, 11)
//	ld_provvigione_2					= lds_dati_principali.getitemnumber(			ll_index_1, 12)
//	ld_val_riga							= lds_dati_principali.getitemnumber(			ll_index_1, 13)
//	ls_cod_iva							= lds_dati_principali.getitemstring(			ll_index_1, 14)
//	ldt_data_consegna_det			= lds_dati_principali.getitemdatetime(		ll_index_1, 15)
//	ls_nota_dettaglio					= lds_dati_principali.getitemstring(			ll_index_1, 16)
//	ld_sconto_3							= lds_dati_principali.getitemnumber(			ll_index_1, 17)
//	ld_sconto_4							= lds_dati_principali.getitemnumber(			ll_index_1, 18)
//	ld_sconto_5							= lds_dati_principali.getitemnumber(			ll_index_1, 19)
//	ld_sconto_6							= lds_dati_principali.getitemnumber(			ll_index_1, 20)
//	ld_sconto_7							= lds_dati_principali.getitemnumber(			ll_index_1, 21)
//	ld_sconto_8							= lds_dati_principali.getitemnumber(			ll_index_1, 22)
//	ld_sconto_9							= lds_dati_principali.getitemnumber(			ll_index_1, 23)
//	ld_sconto_10						= lds_dati_principali.getitemnumber(			ll_index_1, 24)
//	ls_cod_centro_costo				= lds_dati_principali.getitemstring(			ll_index_1, 25)
//	ls_cod_versione					= lds_dati_principali.getitemstring(			ll_index_1, 26)
//	ld_num_confezioni					= lds_dati_principali.getitemnumber(			ll_index_1, 27)
//	ld_num_pezzi_confezione		= lds_dati_principali.getitemnumber(			ll_index_1, 28)
//	ls_flag_gen_commessa			= lds_dati_principali.getitemstring(			ll_index_1, 29)
//	ll_num_riga_appartenenza		= lds_dati_principali.getitemnumber(			ll_index_1, 30)
//	ls_flag_doc_suc_det				= lds_dati_principali.getitemstring(			ll_index_1, 31)
//	ld_quantita_um						= lds_dati_principali.getitemnumber(			ll_index_1, 32)
//	ld_prezzo_um						= lds_dati_principali.getitemnumber(			ll_index_1, 33)
//	ls_flag_prezzo_bloccato			= lds_dati_principali.getitemstring(			ll_index_1, 34)
//	
//	select tab_tipi_det_ven.flag_tipo_det_ven
//	into   :ls_flag_tipo_det_ven
//	from   tab_tipi_det_ven
//	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
//			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//
//	if sqlca.sqlcode = -1 then
//		g_mb.error("Si è verificato un errore in fase di lettura tipi dettagli.", sqlca)
//		destroy lds_dati_principali
//		destroy lds_dati_completi
//		return -1
//	end if
//	
//	// Modifica Nicola --------------
//	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
//		select flag_doc_suc
//		into :ls_flag_doc_suc_det
//		from tab_tipi_det_ven
//		where 
//			cod_azienda = :s_cs_xx.cod_azienda and
//			cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//	end if		
//	
//	if ls_flag_doc_suc_det = 'N' then
//		setnull(ls_nota_dettaglio)
//	end if				
//	//Fine Modifica ----------
//	
//	if ib_gibus then
//		ls_flag_prezzo_bloccato = "S"
//	end if
//	
//	if ib_gibus then
//		// Verifico il tipo ordine ed eventualmente gestisco la trasformazione del tipo dettaglio per la questione degli ordini di campionatura.
//		if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
//			select 	cod_tipo_det_ven_prodotto
//			into		:ls_cod_tipo_det_ven_prodotto
//			from 		tab_flags_configuratore
//			where 	cod_azienda = :s_cs_xx.cod_azienda and
//						cod_modello = :ls_cod_prodotto;
//			if sqlca.sqlcode = 0 then
//				select cod_tipo_det_ven_prodotto
//				into	:ls_cod_tipo_det_ven_prodotto
//				from 	tab_flags_conf_tipi_ord_ven
//				where cod_azienda = :s_cs_xx.cod_azienda and
//						 cod_modello = :ls_cod_prodotto and
//						 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
//				if sqlca.sqlcode = 0 then
//					ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_prodotto
//				end if
//			end if
//		else
//			select cod_prodotto
//			into   :ls_cod_modello
//			from det_ord_ven
//			where cod_azienda = :s_cs_xx.cod_azienda and
//					 anno_registrazione = :ll_anno_registrazione and
//					 num_registrazione = :ll_num_registrazione and
//					 prog_riga_ord_ven = :ll_num_riga_appartenenza;
//			if sqlca.sqlcode = 0 then
//				select 	cod_tipo_det_ven_addizionali,
//							cod_tipo_det_ven_optionals
//				into		:ls_cod_tipo_det_ven_addizionali,
//							:ls_cod_tipo_det_ven_optionals
//				from 		tab_flags_configuratore
//				where 	cod_azienda = :s_cs_xx.cod_azienda and
//							cod_modello = :ls_cod_modello;
//				if sqlca.sqlcode = 0 then
//					select 	cod_tipo_det_ven_addizionali,
//								cod_tipo_det_ven_optionals
//					into		:ls_cod_tipo_det_ven_addizionali_sost,
//								:ls_cod_tipo_det_ven_optionals_sost
//					from tab_flags_conf_tipi_ord_ven
//					where cod_azienda = :s_cs_xx.cod_azienda and
//							 cod_modello = :ls_cod_modello and
//							 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
//					if sqlca.sqlcode = 0 then
//						// si tratta di un'addizionale
//						if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then
//							ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali_sost
//						end if
//						// si tratta di optional
//						if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_optionals then
//							ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_optionals_sost
//						end if
//					end if
//				end if
//			end if
//		end if
//	
//	end if
//	
//	// stefanop 06/06/2012: se la data di consegna del dettaglio è vuota imposto quella della testata
//	if isnull(ldt_data_consegna_det) then ldt_data_consegna_det = ldt_data_consegna
//	// ----
//	
//	select max(prog_riga_ord_ven)
//	into :ll_prog_riga_ord_ven
//	from det_ord_ven
//	where 	cod_azienda=:s_cs_xx.cod_azienda and
//				anno_registrazione=:ll_anno_registrazione and
//				num_registrazione=:ll_num_registrazione;
//	if sqlca.sqlcode<0 then
//		g_mb.error("Si è verificato un errore in lettura max prog.riga.", sqlca)
//		destroy lds_dati_principali
//		destroy lds_dati_completi
//		return -1
//	end if
//	
//	if isnull(ll_prog_riga_ord_ven) then ll_prog_riga_ord_ven = 0
//	ll_prog_riga_ord_ven += 10
//	
//	if mod(ll_prog_riga_ord_ven, 10)<>0 then
//		//calcolo il progressivo come il + alto divisibile per 10
//		//es. se 35 allora devo ottenere 40
//		ll_prog_riga_ord_ven = (long(ll_prog_riga_ord_ven / 10) + 1) * 10
//	end if
//	
//	insert into det_ord_ven (
//		cod_azienda,   
//		anno_registrazione,   
//		num_registrazione,   
//		prog_riga_ord_ven,   
//		cod_tipo_det_ven,   
//		cod_misura,   
//		cod_prodotto,   
//		des_prodotto,   
//		quan_ordine,   
//		prezzo_vendita,   
//		fat_conversione_ven,   
//		sconto_1,   
//		sconto_2,   
//		provvigione_1,
//		provvigione_2,
//		cod_iva,   
//		data_consegna,   
//		val_riga,
//		quan_in_evasione,   
//		quan_evasa,
//		flag_evasione,   
//		flag_blocco,   
//		nota_dettaglio,   
//		anno_registrazione_off,   
//		num_registrazione_off,   
//		prog_riga_off_ven,
//		anno_commessa,
//		num_commessa,
//		cod_centro_costo,
//		sconto_3,   
//		sconto_4,   
//		sconto_5,   
//		sconto_6,   
//		sconto_7,   
//		sconto_8,   
//		sconto_9,   
//		sconto_10,
//		cod_versione,
//		num_confezioni,
//		num_pezzi_confezione,
//		flag_gen_commessa,
//		num_riga_appartenenza,
//		flag_doc_suc_det,
//		flag_st_note_det,
//		quantita_um,
//		prezzo_um,
//		flag_prezzo_bloccato
//	) values (
//		:s_cs_xx.cod_azienda,   
//		:ll_anno_registrazione,   
//		:ll_num_registrazione,   
//		:ll_prog_riga_ord_ven,
//		:ls_cod_tipo_det_ven,   
//		:ls_cod_misura,   
//		:ls_cod_prodotto,   
//		:ls_des_prodotto,   
//		:ld_quan_offerta,   
//		:ld_prezzo_vendita,   
//		:ld_fat_conversione_ven,   
//		:ld_sconto_1,   
//		:ld_sconto_2,  
//		:ld_provvigione_1,
//		:ld_provvigione_2,
//		:ls_cod_iva,   
//		:ldt_data_consegna_det,   
//		:ld_val_riga,   
//		0,
//		0,
//		'A',   
//		'N',   
//		:ls_nota_dettaglio,   
//		:ll_anno_offerta,   
//		:ll_num_offerta,   
//		:ll_prog_riga_off_ven,
//		null,   
//		null,   
//		:ls_cod_centro_costo,   
//		:ld_sconto_3,   
//		:ld_sconto_4,   
//		:ld_sconto_5,   
//		:ld_sconto_6,   
//		:ld_sconto_7,   
//		:ld_sconto_8,   
//		:ld_sconto_9,   
//		:ld_sconto_10,
//		:ls_cod_versione,
//		:ld_num_confezioni,
//		:ld_num_pezzi_confezione,
//		:ls_flag_gen_commessa,
//		:ll_num_riga_appartenenza,
//		'I',
//		'I',
//		:ld_quantita_um,
//		:ld_prezzo_um,
//		:ls_flag_prezzo_bloccato);		
//		
//	if sqlca.sqlcode <> 0 then
//		g_mb.error("Errore durante la scrittura Dettagli Ordini Vendita.", sqlca)
//		destroy lds_dati_principali
//		destroy lds_dati_completi
//		return -1
//	end if
//
//	if ls_flag_tipo_det_ven = "M" then
//		update anag_prodotti  
//		set quan_impegnata = quan_impegnata + :ld_quan_offerta
//		where 
//			anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
//			 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
//
//		if sqlca.sqlcode = -1 then
//			g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.", sqlca)
//			destroy lds_dati_principali
//			destroy lds_dati_completi
//			return -1
//		end if
//	end if				
//
//	ls_tabella_orig = "det_off_ven_stat"
//	ls_tabella_des = "det_ord_ven_stat"
//	ls_prog_riga_doc_orig = "prog_riga_off_ven"
//	ls_prog_riga_doc_des = "prog_riga_ord_ven"
//
//	if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_offerta, ll_num_offerta, ll_prog_riga_off_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven) = -1 then
//		destroy lds_dati_principali
//		destroy lds_dati_completi
//		return -1
//	end if	
//
//	select tab_tipi_ord_ven.cod_tipo_analisi  
//	into :ls_cod_tipo_analisi_ord
//	from tab_tipi_ord_ven  
//	where
//		tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//		tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
//	
//	if sqlca.sqlcode <> 0 then
//		g_mb.error("Si è verificato un errore nella lettura della Tabella Tipi Ordini.", sqlca)
//		destroy lds_dati_principali
//		destroy lds_dati_completi
//		return -1
//	end if
//
//	if ls_cod_tipo_analisi_off <> ls_cod_tipo_analisi_ord then
//		if f_crea_distribuzione(ls_cod_tipo_analisi_ord, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_off_ven, 4) = -1 then
//			g_mb.error("Si è verificato un errore nella Generazione dei Dettagli Statistici.", sqlca)
//			destroy lds_dati_principali
//			destroy lds_dati_completi
//			return -1
//		end if
//	end if
//
//	// duplicazione delle varianti		
//	INSERT INTO varianti_det_ord_ven (
//		cod_azienda,
//		anno_registrazione,
//		num_registrazione,
//		prog_riga_ord_ven,
//		cod_prodotto_padre,
//		num_sequenza,
//		cod_prodotto_figlio,
//		cod_versione_figlio,
//		cod_versione,
//		cod_misura,
//		cod_prodotto,
//		quan_tecnica,
//		quan_utilizzo,
//		dim_x,
//		dim_y,
//		dim_z,
//		dim_t,
//		coef_calcolo,
//		flag_esclusione,
//		formula_tempo,
//		des_estesa,
//		flag_materia_prima,
//		lead_time,
//		cod_versione_variante
//	) select
//		:s_cs_xx.cod_azienda,
//		:ll_anno_registrazione,
//		:ll_num_registrazione,
//		:ll_prog_riga_ord_ven,
//		cod_prodotto_padre,
//		num_sequenza,
//		cod_prodotto_figlio,
//		cod_versione_figlio,
//		cod_versione,
//		cod_misura,
//		cod_prodotto,
//		quan_tecnica,
//		quan_utilizzo,
//		dim_x,
//		dim_y,
//		dim_z,
//		dim_t,
//		coef_calcolo,
//		flag_esclusione,
//		formula_tempo,
//		des_estesa,
//		flag_materia_prima,
//		lead_time,
//		cod_versione_variante
//	from varianti_det_off_ven
//	where
//		cod_azienda = :s_cs_xx.cod_azienda and
//		anno_registrazione = :ll_anno_offerta and
//		num_registrazione = :ll_num_offerta and
//		prog_riga_off_ven = :ll_prog_riga_off_ven;
//	
//	
//	if sqlca.sqlcode < 0 then
//		g_mb.error("Errore durante la creazione varianti ordine", sqlca)
//		return -1
//	end if
//
//	// duplicazione delle integrazioni
//	insert into integrazioni_det_ord_ven (
//		cod_azienda,   
//		anno_registrazione,   
//		num_registrazione,   
//		prog_riga_ord_ven,   
//		progressivo,   
//		cod_prodotto_padre,   
//		cod_versione_padre,   
//		num_sequenza,   
//		cod_prodotto_figlio,   
//		cod_versione_figlio,   
//		cod_misura,   
//		quan_tecnica,   
//		quan_utilizzo,   
//		dim_x,   
//		dim_y,   
//		dim_z,   
//		dim_t,   
//		coef_calcolo,   
//		des_estesa,   
//		cod_formula_quan_utilizzo,   
//		flag_escludibile,   
//		flag_materia_prima,   
//		flag_arrotonda,
//		lead_time
//	) select 
//		:s_cs_xx.cod_azienda,   
//		:ll_anno_registrazione,   
//		:ll_num_registrazione,   
//		:ll_prog_riga_ord_ven,   
//		progressivo,   
//		cod_prodotto_padre,   
//		cod_versione_padre,   
//		num_sequenza,   
//		cod_prodotto_figlio,   
//		cod_versione_figlio,   
//		cod_misura,   
//		quan_tecnica,   
//		quan_utilizzo,   
//		dim_x,   
//		dim_y,   
//		dim_z,   
//		dim_t,   
//		coef_calcolo,   
//		des_estesa,   
//		cod_formula_quan_utilizzo,   
//		flag_escludibile,   
//		flag_materia_prima,   
//		flag_arrotonda,
//		lead_time
//		from  integrazioni_det_off_ven  
//		where
//			cod_azienda=:s_cs_xx.cod_azienda and
//			anno_registrazione=:ll_anno_offerta and
//			num_registrazione=:ll_num_offerta and
//			prog_riga_off_ven=:ll_prog_riga_off_ven;
//		
//	if sqlca.sqlcode < 0 then
//		g_mb.error("Errore durante la creazione integrazioni ordine.", sqlca)
//		return -1
//	end if
//
//	// copio varianti
//	if ib_gibus then
//		
//		INSERT INTO comp_det_ord_ven (
//			cod_azienda,   
//			anno_registrazione,   
//			num_registrazione,   
//			prog_riga_ord_ven,   
//			cod_prod_finito,   
//			cod_modello,   
//			dim_x,   
//			dim_y,   
//			dim_z,   
//			dim_t,   
//			cod_tessuto,   
//			cod_non_a_magazzino,   
//			cod_tessuto_mant,   
//			cod_mant_non_a_magazzino,   
//			flag_tipo_mantovana,   
//			alt_mantovana,   
//			flag_cordolo,   
//			flag_passamaneria,   
//			cod_verniciatura,   
//			cod_comando,   
//			pos_comando,   
//			alt_asta,   
//			cod_comando_tenda_verticale,   
//			cod_colore_lastra,   
//			cod_tipo_lastra,   
//			spes_lastra,   
//			cod_tipo_supporto,   
//			flag_cappotta_frontale,   
//			flag_rollo,   
//			flag_kit_laterale,   
//			flag_misura_luce_finita,   
//			cod_tipo_stampo,   
//			cod_mat_sis_stampaggio,   
//			rev_stampo,   
//			num_gambe,   
//			num_campate,   
//			flag_autoblocco,   
//			num_fili_plisse,   
//			num_boccole_plisse,   
//			intervallo_punzoni_plisse,   
//			num_pieghe_plisse,   
//			prof_inf_plisse,   
//			prof_sup_plisse,   
//			colore_passamaneria,   
//			colore_cordolo,   
//			num_punzoni_plisse,   
//			des_vernice_fc,   
//			des_comando_1,   
//			cod_comando_2,   
//			des_comando_2,   
//			flag_addizionale_comando_1,   
//			flag_addizionale_comando_2,   
//			flag_addizionale_supporto,   
//			flag_addizionale_tessuto,   
//			flag_addizionale_vernic,   
//			flag_fc_comando_1,   
//			flag_fc_comando_2,   
//			flag_fc_supporto,   
//			flag_fc_tessuto,   
//			flag_fc_vernic,   
//			flag_allegati,   
//			data_ora_ar,   
//			flag_tessuto_cliente,   
//			flag_tessuto_mant_cliente,
//			cod_verniciatura_2,
//			flag_addizionale_vernic_2,
//			flag_fc_vernic_2 ,
//			des_vernic_fc_2  ,
//			cod_verniciatura_3,
//			flag_addizionale_vernic_3,
//			flag_fc_vernic_3,
//			des_vernic_fc_3	)  
//		SELECT
//			:s_cs_xx.cod_azienda,   
//			:ll_anno_registrazione,   
//			:ll_num_registrazione,   	
//			:ll_prog_riga_ord_ven, 
//			cod_prod_finito,  
//			cod_modello,   
//			dim_x,   
//			dim_y,   
//			dim_z,   
//			dim_t,
//			cod_tessuto,
//			cod_non_a_magazzino,   
//			cod_tessuto_mant,      
//			cod_mant_non_a_magazzino,
//			flag_tipo_mantovana,   
//			alt_mantovana,   
//			flag_cordolo,   
//			flag_passamaneria,   
//			cod_verniciatura,   
//			cod_comando,   
//			pos_comando,   
//			alt_asta,   
//			cod_comando_tenda_verticale,   
//			cod_colore_lastra,   
//			cod_tipo_lastra,   
//			spes_lastra,   
//			cod_tipo_supporto,   
//			flag_cappotta_frontale,   
//			flag_rollo,   
//			flag_kit_laterale,   
//			flag_misura_luce_finita,   
//			cod_tipo_stampo,   
//			cod_mat_sis_stampaggio,   
//			rev_stampo,   
//			num_gambe,   
//			num_campate,   
//			flag_autoblocco,   
//			num_fili_plisse,   
//			num_boccole_plisse,   
//			intervallo_punzoni_plisse,   
//			num_pieghe_plisse,   
//			prof_inf_plisse,   
//			prof_sup_plisse,   
//			colore_passamaneria,   
//			colore_cordolo,   
//			num_punzoni_plisse,   
//			des_vernice_fc,   
//			des_comando_1,   
//			cod_comando_2,   
//			des_comando_2,   
//			flag_addizionale_comando_1,   
//			flag_addizionale_comando_2,   
//			flag_addizionale_supporto,   
//			flag_addizionale_tessuto,   
//			flag_addizionale_vernic,   
//			flag_fc_comando_1,   
//			flag_fc_comando_2,   
//			flag_fc_supporto,   
//			flag_fc_tessuto,   
//			flag_fc_vernic,   
//			flag_allegati,   
//			data_ora_ar,   
//			flag_tessuto_cliente,   
//			flag_tessuto_mant_cliente,
//			cod_verniciatura_2,
//			flag_addizionale_vernic_2,
//			flag_fc_vernic_2 ,
//			des_vernic_fc_2  ,
//			cod_verniciatura_3,
//			flag_addizionale_vernic_3,
//			flag_fc_vernic_3,
//			des_vernic_fc_3
//		FROM comp_det_off_ven
//		WHERE
//			cod_azienda=:s_cs_xx.cod_azienda and
//			anno_registrazione=:ll_anno_offerta and
//			num_registrazione=:ll_num_offerta and
//			prog_riga_off_ven=:ll_prog_riga_off_ven;
//			
//		if sqlca.sqlcode < 0 then
//			g_mb.error("Errore durante la copia della tabella comp_det_off_ven", sqlca)
//			return -1
//		end if
//		
//		luo_funzioni_1 = create uo_funzioni_1
//		luo_funzioni_1.uof_gen_varianti_deposito_ord_ven ( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ref ls_errore )
//		destroy luo_funzioni_1
//		
//	end if
//	//------------------------------------------------------------------------------------------------------------------------
//next

return 0
end function

public subroutine wf_conserva_progressivi (ref s_cs_xx_parametri al_progressivi, long al_num_offerta, long al_riga_offerta, long al_riga_ordine);
long ll_index

ll_index = upperbound(al_progressivi.parametro_d_1_a[])
if isnull(ll_index) then ll_index=0

ll_index += 1

//solo per righe offerte principali
al_progressivi.parametro_d_1_a[ll_index] = al_num_offerta
al_progressivi.parametro_d_2_a[ll_index] = al_riga_offerta
al_progressivi.parametro_d_3_a[ll_index] = al_riga_ordine

/*
offerta 1					il vettore sarebbe					ordine generato: 5
1		10					1		10		10						5		10
1		11		10			1		20		20						5		11		10
1		12		10			2		10		30						5		12		10
1		20															5		20		
1		21		20													5		21		20

offerta 2
2		10															5		30
2		11		10													5		31		30
2		12		10													5		32		30
2		13		10													5		33		30

*/


end subroutine

public function long wf_leggi_num_appartenenza (s_cs_xx_parametri al_progressivi, long al_num_offerta, long al_riga_offerta);
long ll_index, ll_num_riga_appartenenza

ll_num_riga_appartenenza = 0

for ll_index=1 to upperbound(al_progressivi.parametro_d_1_a[])
	
	if al_progressivi.parametro_d_1_a[ll_index] = al_num_offerta and al_progressivi.parametro_d_2_a[ll_index] = al_riga_offerta then
		//leggo la riga appartenenza da assegnare alla riga collegata
		ll_num_riga_appartenenza = al_progressivi.parametro_d_3_a[ll_index]
		exit
	end if

next


return ll_num_riga_appartenenza


end function

public subroutine wf_ricerca ();/**
 * 25/06/2014
 *
 * Esegue la ricerca delle righe
 **/
 
string ls_sql, ls_where, ls_value
int li_value
date ldt_value

tab_1.page_1.dw_ricerca.accepttext()
ls_sql = is_default_sql

// stefanop: where base
ls_where = " WHERE tes_off_ven.cod_azienda = anag_clienti.cod_azienda" +  &
				" AND tes_off_ven.cod_cliente = anag_clienti.cod_cliente " + &
				" AND tes_off_ven.cod_azienda ='" + s_cs_xx.cod_azienda + "' " + &
				" AND tes_off_ven.cod_documento is not null " +  &
				" AND tes_off_ven.cod_documento is not null " + &
				" AND tes_off_ven.flag_blocco = 'N' " + &
				" AND tes_off_ven.flag_evasione <> 'E' " + &
				" AND tes_off_ven.cod_cliente is not null " + &
				" AND tes_off_ven.flag_fuori_fido = 'N' " + &
				" AND (tes_off_ven.data_scadenza >='" + string(today(), s_cs_xx.db_funzioni.formato_data) + "' " + &
				" OR tes_off_ven.data_scadenza is null) "
				
// ----------------------------------------------------------------------------------------------------------
// Where dinamica
li_value = tab_1.page_1.dw_ricerca.getitemnumber(1, "anno_registrazione")
if li_value > 1900 then
	ls_where += " AND tes_off_ven.anno_registrazione=" + string(li_value)
end if

li_value = tab_1.page_1.dw_ricerca.getitemnumber(1, "num_registrazione")
if li_value > 0 then
	ls_where += " AND tes_off_ven.num_registrazione=" + string(li_value)
end if

ldt_value = tab_1.page_1.dw_ricerca.getitemdate(1,"data_consegna_da")
if not isnull(ldt_value) then ls_where += " AND tes_off_ven.data_consegna >= '" + string(ldt_value, s_cs_xx.db_funzioni.formato_data) + "' "
ldt_value = tab_1.page_1.dw_ricerca.getitemdate(1,"data_consegna_a")
if not isnull(ldt_value) then ls_where += " AND tes_off_ven.data_consegna <= '" + string(ldt_value, s_cs_xx.db_funzioni.formato_data) + "' "

ldt_value = tab_1.page_1.dw_ricerca.getitemdate(1,"data_registrazione_da")
if not isnull(ldt_value) then ls_where += " AND tes_off_ven.data_registrazione >= '" + string(ldt_value, s_cs_xx.db_funzioni.formato_data) + "' "
ldt_value = tab_1.page_1.dw_ricerca.getitemdate(1,"data_registrazione_a")
if not isnull(ldt_value) then ls_where += " AND tes_off_ven.data_registrazione >= '" + string(ldt_value, s_cs_xx.db_funzioni.formato_data) + "' "

ldt_value = tab_1.page_1.dw_ricerca.getitemdate(1,"data_scadenza_da")
if not isnull(ldt_value) then ls_where += " AND tes_off_ven.data_scadenza >= '" + string(ldt_value, s_cs_xx.db_funzioni.formato_data) + "' "
ldt_value = tab_1.page_1.dw_ricerca.getitemdate(1,"data_scadenza_a")
if not isnull(ldt_value) then ls_where += " AND tes_off_ven.data_scadenza <= '" + string(ldt_value, s_cs_xx.db_funzioni.formato_data) + "' "


// Documento
ls_value = tab_1.page_1.dw_ricerca.getitemstring(1,"cod_cliente")
if not isnull(ls_value) and ls_value<>"" then ls_where += " AND  tes_off_ven.cod_cliente = '" + ls_value + "' "
ls_value = tab_1.page_1.dw_ricerca.getitemstring(1,"cod_documento")
if not isnull(ls_value) and ls_value<>"" then ls_where += " AND  tes_off_ven.cod_documento = '" + ls_value + "' "
li_value = tab_1.page_1.dw_ricerca.getitemnumber(1,"anno_documento")
if not isnull(li_value) and li_value>1900 then ls_where += " AND tes_off_ven.anno_documento =" + string(li_value)
ls_value = tab_1.page_1.dw_ricerca.getitemstring(1,"numeratore_documento")
if not isnull(ls_value) and ls_value<>"" then ls_where += " AND tes_off_ven.numeratore_documento = '" + ls_value + "' "
li_value = tab_1.page_1.dw_ricerca.getitemnumber(1,"num_documento")
if not isnull(li_value) and li_value > 0 then ls_where += " AND tes_off_ven.num_documento=" + string(li_value)

ls_value = tab_1.page_1.dw_ricerca.getitemstring(1,"num_offerta")
if not isnull(ls_value) and ls_value<>"" then ls_where += " AND tes_off_ven.num_offerta = '" + ls_value + "' "

ls_value = tab_1.page_1.dw_ricerca.getitemstring(1,"cod_tipo_off_ven")
if not isnull(ls_value) and ls_value<>"" then ls_where += " AND tes_off_ven.cod_tipo_off_ven = '" + ls_value + "' "
// ----------------------------------------------------------------------------------------------------------
				
// stefanop: aggiungo orderby
ls_sql += ls_where + " ORDER BY tes_off_ven.cod_documento ASC, tes_off_ven.anno_documento ASC, " + &
			 " tes_off_ven.numeratore_documento ASC, tes_off_ven.num_documento ASC"

if tab_1.page_2.dw_tes_gen_ord_ven.setsqlselect(ls_sql) = 1 then
	tab_1.page_2.dw_tes_gen_ord_ven.event pcd_retrieve(1,1)
	tab_1.selecttab(2)
else
	g_mb.error("Errore durante l'impostazione della select per il filtro degli ordini")
end if


end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_flag
windowobject lw_oggetti[]

set_w_options(c_noenablepopup)

// stefanop: 25/06/2014: Allineo DW
tab_1.page_1.dw_ricerca.move(0,0)
tab_1.page_2.dw_tes_gen_ord_ven.move(0,0)
// -----

// stefanop: 25/06/2014: Impostazioni base
tab_1.page_1.dw_ricerca.insertrow(0)
is_default_sql = tab_1.page_2.dw_tes_gen_ord_ven.getsqlselect()
// ----

iuo_fido = create uo_fido_cliente

tab_1.page_2.dw_tes_gen_ord_ven.set_dw_options(sqlca, &
													pcca.null_object, &
													c_nonew + &
													c_nomodify + &
													c_nodelete + &
													c_disablecc + &
													c_disableccinsert + &
													c_noretrieveonopen, &
													c_default)

tab_1.page_3.dw_det_off_ven.settransobject(sqlca)

tab_1.page_4.dw_gen_orig_dest.set_dw_options(sqlca, &
												pcca.null_object, &
												c_nonew + &
												c_nomodify + &
												c_nodelete + &
												c_disablecc + &
												c_disableccinsert, &
												c_viewmodeblack)
												
													 
save_on_close(c_socnosave)

tab_1.page_3.dw_det_gen_ord_ven_options.insertrow(0)

// leggo il flag se è gibus
guo_functions.uof_get_parametro_azienda("GIB", ib_gibus)
//select flag
//into :ls_flag
//from parametri_azienda
//where
//	cod_azienda = :s_cs_xx.cod_azienda and
//	flag_parametro = 'F' and
//	cod_parametro = 'GIB';
//	
//ib_gibus = (ls_flag = 'S')
// ---

end event

on w_tes_gen_ord_ven.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_tes_gen_ord_ven.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setddlb;call super::pc_setddlb;string ls_where, ls_cod_deposito, ls_error

// stefanop: 17/04/2012: Abilito filtro per deposito
// ------------------------------------------------------------------
ls_where = "cod_azienda = '" + s_cs_xx.cod_azienda + "'"
if guo_functions.uof_is_admin(s_cs_xx.cod_utente) or guo_functions.uof_is_supervisore(s_cs_xx.cod_utente) then
	// niente
else
	guo_functions.uof_get_stabilimento_utente(ref ls_cod_deposito, ref ls_error)
	
	if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then 
		ls_where += " AND cod_deposito='" + ls_cod_deposito + "'"
	end if
end if
// ------------------------------------------------------------------

f_po_loaddddw_dw(tab_1.page_3.dw_det_off_ven, &
                 "cod_tipo_ord_ven", &
                 sqlca, &
                 "tab_tipi_ord_ven", &
                 "cod_tipo_ord_ven", &
                 "des_tipo_ord_ven", &
                 ls_where)

f_po_loaddddw_dw(tab_1.page_3.dw_det_gen_ord_ven_options, &
                 "cod_tipo_ord_ven", &
                 sqlca, &
                 "tab_tipi_ord_ven", &
                 "cod_tipo_ord_ven", &
                 "des_tipo_ord_ven", &
                 ls_where)
					  
f_po_loaddddw_dw(tab_1.page_1.dw_ricerca, &
                 "cod_tipo_off_ven", &
                 sqlca, &
                 "tab_tipi_off_ven", &
                 "cod_tipo_off_ven", &
                 "des_tipo_off_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event close;call super::close;

destroy iuo_fido
end event

event resize;/** TOLTO ANCESTOR **/

tab_1.resize(newwidth - 40, newheight - 40)
tab_1.event ue_resize()
end event

type tab_1 from tab within w_tes_gen_ord_ven
event create ( )
event destroy ( )
event ue_resize ( )
integer x = 18
integer y = 20
integer width = 4457
integer height = 2140
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
page_1 page_1
page_2 page_2
page_3 page_3
page_4 page_4
end type

on tab_1.create
this.page_1=create page_1
this.page_2=create page_2
this.page_3=create page_3
this.page_4=create page_4
this.Control[]={this.page_1,&
this.page_2,&
this.page_3,&
this.page_4}
end on

on tab_1.destroy
destroy(this.page_1)
destroy(this.page_2)
destroy(this.page_3)
destroy(this.page_4)
end on

event ue_resize();/*
 */
 
page_1.dw_ricerca.resize(page_1.width, page_1.height)

// Posiziono pagina 2
page_2.cb_seleziona.move(page_2.width - page_2.cb_seleziona.width - 20, page_2.height - page_2.cb_seleziona.height - 20)
page_2.cb_refresh.move(page_2.cb_seleziona.x - page_2.cb_refresh.width - 20, page_2.cb_seleziona.y)
page_2.dw_tes_gen_ord_ven.resize(page_1.width, page_2.cb_seleziona.y - 40)

// Posiziono pagina 3
page_3.cb_gen.move(page_3.width - page_3.cb_gen.width - 20, page_3.height - page_3.cb_gen.height - 20)
page_3.dw_det_gen_ord_ven_options.move(0, page_3.height - page_3.dw_det_gen_ord_ven_options.height - 20)
page_3.dw_det_off_ven.resize(page_3.width, page_3.dw_det_gen_ord_ven_options.y - 40)

// Posiziono pagina 4
page_4.dw_gen_orig_dest.resize(page_1.width, page_1.height)
end event

event selectionchanged;
choose case newindex
	case 1
		tab_1.page_3.enabled = false

	case 2
		tab_1.page_2.enabled = true
		tab_1.page_3.enabled = false
		
	case 3
		tab_1.page_2.enabled = true
		tab_1.page_3.enabled = true
		
		if tab_1.page_2.dw_tes_gen_ord_ven.rowcount() > 0 then
			tab_1.page_3.dw_det_off_ven.triggerevent("ue_retrieve")
		else
			tab_1.page_3.dw_det_off_ven.reset()
		end if
		
end choose
end event

type page_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4421
integer height = 2012
long backcolor = 12632256
string text = "1. Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ricerca dw_ricerca
end type

on page_1.create
this.dw_ricerca=create dw_ricerca
this.Control[]={this.dw_ricerca}
end on

on page_1.destroy
destroy(this.dw_ricerca)
end on

type dw_ricerca from datawindow within page_1
integer x = 27
integer y = 28
integer width = 3749
integer height = 1140
integer taborder = 30
string title = "none"
string dataobject = "d_det_gen_ord_ven_ricerca"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose case dwo.name
		
	case "b_cerca"
		wf_ricerca()
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca, "cod_cliente")
		
		
end choose
end event

type page_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4421
integer height = 2012
boolean enabled = false
long backcolor = 12632256
string text = "2. Lista"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_seleziona cb_seleziona
cb_refresh cb_refresh
dw_tes_gen_ord_ven dw_tes_gen_ord_ven
end type

on page_2.create
this.cb_seleziona=create cb_seleziona
this.cb_refresh=create cb_refresh
this.dw_tes_gen_ord_ven=create dw_tes_gen_ord_ven
this.Control[]={this.cb_seleziona,&
this.cb_refresh,&
this.dw_tes_gen_ord_ven}
end on

on page_2.destroy
destroy(this.cb_seleziona)
destroy(this.cb_refresh)
destroy(this.dw_tes_gen_ord_ven)
end on

type cb_seleziona from commandbutton within page_2
integer x = 3799
integer y = 1228
integer width = 343
integer height = 104
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona"
end type

event clicked;// se ho selezionato almeno una riga allora procedo
if dw_tes_gen_ord_ven.getrow() > 0 then
	tab_1.selecttab(3)
end if
end event

type cb_refresh from commandbutton within page_2
integer x = 3365
integer y = 1228
integer width = 402
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricarica"
end type

event clicked;wf_ricerca()
end event

type dw_tes_gen_ord_ven from uo_cs_xx_dw within page_2
integer x = 5
integer y = 28
integer width = 4206
integer height = 1160
integer taborder = 20
string dataobject = "d_tes_gen_ord_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;/* TOLTO ANCESTOR */

if row > 0 then

	tab_1.selecttab(3)

end if
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_select
long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event rowfocuschanged;/* TOLTO ANCESTOR */

if currentrow > 0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

type page_3 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4421
integer height = 2012
boolean enabled = false
long backcolor = 12632256
string text = "3. Righe"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det_gen_ord_ven_options dw_det_gen_ord_ven_options
cb_gen cb_gen
dw_det_off_ven dw_det_off_ven
end type

on page_3.create
this.dw_det_gen_ord_ven_options=create dw_det_gen_ord_ven_options
this.cb_gen=create cb_gen
this.dw_det_off_ven=create dw_det_off_ven
this.Control[]={this.dw_det_gen_ord_ven_options,&
this.cb_gen,&
this.dw_det_off_ven}
end on

on page_3.destroy
destroy(this.dw_det_gen_ord_ven_options)
destroy(this.cb_gen)
destroy(this.dw_det_off_ven)
end on

type dw_det_gen_ord_ven_options from uo_std_dw within page_3
integer x = 27
integer y = 1168
integer width = 2606
integer height = 200
integer taborder = 40
string dataobject = "d_det_gen_ord_ven_options"
boolean border = false
end type

type cb_gen from commandbutton within page_3
integer x = 3776
integer y = 1208
integer width = 430
integer height = 104
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Genera Ordine"
end type

event clicked;wf_genera_ordine()
end event

type dw_det_off_ven from uo_std_dw within page_3
event ue_retrieve ( )
integer x = -18
integer y = 8
integer width = 4343
integer height = 1140
integer taborder = 20
string dataobject = "d_det_gen_ord_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event ue_retrieve();string ls_cod_tipo_off_ven, ls_cod_tipo_ord_ven
long ll_i

il_anno_registrazione = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(tab_1.page_2.dw_tes_gen_ord_ven.getrow(), "anno_registrazione")
il_num_registrazione = tab_1.page_2.dw_tes_gen_ord_ven.getitemnumber(tab_1.page_2.dw_tes_gen_ord_ven.getrow(), "num_registrazione")

if  retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione) < 0 then
   pcca.error = c_fatal
else
	// tipo ordine da generare
	ls_cod_tipo_off_ven = tab_1.page_2.dw_tes_gen_ord_ven.getitemstring(tab_1.page_2.dw_tes_gen_ord_ven.getrow(), "cod_tipo_off_ven")
	
	select cod_tipo_ord_ven
	into :ls_cod_tipo_ord_ven
	from tab_tipi_off_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_off_ven = :ls_cod_tipo_off_ven;
		
	if len(ls_cod_tipo_ord_ven) < 1 then
		
		g_mb.show("Attenzione, il Tipo Offerta non ha nessun Tipo Ordine associato!")
		
	end if
	
	
	for ll_i = 1 to rowcount()
		// codice standard per tutti i clienti
		setitem(ll_i, "cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
				
		if not isnull(getitemnumber(ll_i, "num_riga_appartenenza")) then
			setitem(ll_i, "selected", "N")
		else
			setitem(ll_i, "selected", "S")
		end if
	next
	
	// gibus ha una modalità diversa per il dettaglio ordine. 
	if ib_gibus then wf_tipo_ord_gibus()
end if
end event

event itemchanged;call super::itemchanged;long ll_find, ll_prog_riga_off_ven

choose case dwo.name
		
	// Applicato il dettglio anche alle righe collegate
	case "cod_tipo_ord_ven"
		if data <> "" then
			
			ll_prog_riga_off_ven = getitemnumber(row, "prog_riga_off_ven")
			ll_find = row
			
			do
				ll_find = find("num_riga_appartenenza=" + string(ll_prog_riga_off_ven), ll_find, rowcount())
				
				if ll_find > 0 then
					setitem(ll_find, "cod_tipo_ord_ven", data)
					ll_find ++
				end if
				
			// stefanop: 16/01/2012: rifaccio la modifica che non si sa come ma si è persa !!!
			loop while (ll_find > 0 AND ll_find <= rowcount())
			
		end if
		
end choose
end event

type page_4 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4421
integer height = 2012
long backcolor = 12632256
string text = "4. Riepilogo"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_gen_orig_dest dw_gen_orig_dest
end type

on page_4.create
this.dw_gen_orig_dest=create dw_gen_orig_dest
this.Control[]={this.dw_gen_orig_dest}
end on

on page_4.destroy
destroy(this.dw_gen_orig_dest)
end on

type dw_gen_orig_dest from uo_cs_xx_dw within page_4
event pcd_retrieve pbm_custom60
integer width = 4251
integer height = 1360
integer taborder = 30
string dataobject = "d_gen_orig_dest"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean ib_dw_detail = true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

event buttonclicked;call super::buttonclicked;s_open_parm lstr_param

choose case dwo.name

	case "b_apri"
		lstr_param.anno_registrazione = getitemnumber(row, "anno_registrazione_dest")
		lstr_param.num_registrazione = long(getitemnumber(row, "num_registrazione_dest"))
		lstr_param.origine = "w_tes_gen_ord_ven"
		
		window_open_parm(w_tes_ord_ven_tv, -1,  lstr_param)
		
end choose
end event

