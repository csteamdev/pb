﻿$PBExportHeader$w_tes_gen_ord_ven_single.srw
$PBExportComments$Finestra Testata Generazione Ordini Vendita
forward
global type w_tes_gen_ord_ven_single from window
end type
type cb_annulla from commandbutton within w_tes_gen_ord_ven_single
end type
type cb_conferma from commandbutton within w_tes_gen_ord_ven_single
end type
type st_titolo from statictext within w_tes_gen_ord_ven_single
end type
type tab_1 from tab within w_tes_gen_ord_ven_single
end type
type det_1 from userobject within tab_1
end type
type dw_1 from datawindow within det_1
end type
type det_1 from userobject within tab_1
dw_1 dw_1
end type
type det_2 from userobject within tab_1
end type
type dw_2 from datawindow within det_2
end type
type det_2 from userobject within tab_1
dw_2 dw_2
end type
type det_3 from userobject within tab_1
end type
type dw_3 from datawindow within det_3
end type
type det_3 from userobject within tab_1
dw_3 dw_3
end type
type det_4 from userobject within tab_1
end type
type dw_4 from datawindow within det_4
end type
type det_4 from userobject within tab_1
dw_4 dw_4
end type
type det_5 from userobject within tab_1
end type
type dw_5 from datawindow within det_5
end type
type det_5 from userobject within tab_1
dw_5 dw_5
end type
type tab_1 from tab within w_tes_gen_ord_ven_single
det_1 det_1
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
end type
end forward

global type w_tes_gen_ord_ven_single from window
integer width = 4745
integer height = 1944
string title = "Generazione ordine di Vendita da Offerta di Vendita"
long backcolor = 12632256
boolean center = true
event ue_postopen ( )
event ue_carica_dropdown ( )
cb_annulla cb_annulla
cb_conferma cb_conferma
st_titolo st_titolo
tab_1 tab_1
end type
global w_tes_gen_ord_ven_single w_tes_gen_ord_ven_single

type variables

integer			ii_anno_offerta

long				il_num_offerta
end variables

forward prototypes
public function integer wf_imposta_testata (ref string as_errore)
public function integer wf_imposta_dettagli (datetime adt_data_cons, ref string as_errore)
public subroutine wf_tipo_det_ven_lista (datawindow fdw_datawindow, string fs_cod_tipo_det_ven)
public subroutine wf_dati_cliente (string as_cod_cliente)
public function integer wf_controlla_dettagli (integer ai_anno, long al_numero, ref string as_errore)
public function integer wf_salva (ref integer ai_anno_ordine, ref long al_num_ordine, ref string as_errore)
public function integer wf_controlla_dati (ref integer ai_anno_ordine, ref long al_num_ordine, ref string as_errore)
public function integer wf_post_save_details (integer ai_anno_ordine, long al_num_ordine, ref string as_errore)
public subroutine wf_rowfocuschg_details (long al_row, string as_cod_tipo_det_ven, ref datawindow adw_data)
public subroutine wf_sel_prodotto (long al_row)
end prototypes

event ue_postopen();
integer			li_ret

string				ls_errore


li_ret = wf_imposta_testata(ls_errore)

if li_ret<0 then
	g_mb.error(ls_errore)
	return
end if
end event

event ue_carica_dropdown();

string ls_select_operatori


if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_tipo_ord_ven", &
                 sqlca, &
                 "tab_tipi_ord_ven", &
                 "cod_tipo_ord_ven", &
                 "des_tipo_ord_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_causale", &
                 sqlca, &
                 "tab_causali_trasp", &
                 "cod_causale", &
                 "des_causale", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_1.det_3.dw_3, &
                 "cod_giro_consegna", &
                 sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_1.det_5.dw_5, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_1.det_5.dw_5, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
end event

public function integer wf_imposta_testata (ref string as_errore);string			ls_cod_cliente, ls_cod_operatore, ls_cod_des_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, &
				ls_cod_deposito, ls_cod_ubicazione, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_imballo, ls_aspetto_beni, &
				ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_cod_banca_clien_for, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_causale_trasporto, &
			 	ls_cod_porto, ls_cod_resa, ls_flag_riep_bol, ls_flag_riep_fat, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_cod_operatore_new, ls_cod_tipo_off_ven, &
				ls_cod_tipo_ord_ven, ls_flag_st_note_tes, ls_flag_st_note_pie

dec{4}		ld_cambio_ven, ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_tot_val_offerta

datetime		ldt_data_consegna

long			ll_new

integer		li_ret


select	 cod_tipo_off_ven,
		 cod_cliente,   
		 cod_operatore,   
		 cod_des_cliente,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,   
		 cod_deposito,   
		 cod_ubicazione,   
		 cod_valuta,   
		 cambio_ven,   
		 cod_tipo_listino_prodotto,   
		 cod_pagamento,   
		 sconto,   
		 cod_agente_1,
		 cod_agente_2,
		 cod_banca,   
		 cod_imballo,   
		 aspetto_beni,
		 peso_netto,
		 peso_lordo,
		 num_colli,
		 cod_vettore,   
		 cod_inoltro,   
		 cod_mezzo,
		 causale_trasporto,
		 cod_porto,   
		 cod_resa,   
		 flag_riep_bol,
		 flag_riep_fat,
		 tot_val_offerta,
		 data_consegna,   
		 cod_banca_clien_for,
		 flag_doc_suc_tes, 
		 flag_doc_suc_pie,
		 flag_st_note_tes,
		 flag_st_note_pie
 into		 :ls_cod_tipo_off_ven, 
 			 :ls_cod_cliente,   
			 :ls_cod_operatore,   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito,   
			 :ls_cod_ubicazione,
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_flag_riep_bol,
			 :ls_flag_riep_fat,
			 :ld_tot_val_offerta,
			 :ldt_data_consegna,   
			 :ls_cod_banca_clien_for,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie,
			 :ls_flag_st_note_tes,
		 	 :ls_flag_st_note_pie
from   tes_off_ven  
where	cod_azienda = :s_cs_xx.cod_azienda and  
			anno_registrazione = :ii_anno_offerta and  
			num_registrazione = :il_num_offerta;
		
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in lettura dati testata offerta di vendita: "+sqlca.sqlerrtext
	return -1
end if

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tab_tipi_off_ven  
where cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_off_ven = :ls_cod_tipo_off_ven;

if ls_cod_tipo_ord_ven="" then setnull(ls_cod_tipo_ord_ven)

ll_new = tab_1.det_1.dw_1.insertrow(0)

tab_1.det_1.dw_1.setitem(ll_new, "data_registrazione", datetime(today(), 00:00:00))

tab_1.det_1.dw_1.setitem(ll_new, "cod_tipo_ord_ven", ls_cod_tipo_ord_ven)

tab_1.det_1.dw_1.setitem(ll_new, "cod_cliente", ls_cod_cliente)

//metti l'operatore collegato per il nuovo documento
if f_get_operatore_doc(ls_cod_operatore_new, as_errore)>0 then
	ls_cod_operatore = ls_cod_operatore_new
end if
as_errore = ""
tab_1.det_1.dw_1.setitem(ll_new, "cod_operatore", ls_cod_operatore)

tab_1.det_1.dw_1.setitem(ll_new, "cod_des_cliente", ls_cod_des_cliente)
tab_1.det_1.dw_1.setitem(ll_new, "rag_soc_1", ls_rag_soc_1)
tab_1.det_1.dw_1.setitem(ll_new, "rag_soc_2", ls_rag_soc_2)
tab_1.det_1.dw_1.setitem(ll_new, "indirizzo", ls_indirizzo)
tab_1.det_1.dw_1.setitem(ll_new, "localita", ls_localita)
tab_1.det_1.dw_1.setitem(ll_new, "frazione", ls_frazione)
tab_1.det_1.dw_1.setitem(ll_new, "cap", ls_cap)
tab_1.det_1.dw_1.setitem(ll_new, "provincia", ls_provincia)
tab_1.det_1.dw_1.setitem(ll_new, "cod_deposito", ls_cod_deposito)
tab_1.det_1.dw_1.setitem(ll_new, "cod_ubicazione", ls_cod_ubicazione)
tab_1.det_1.dw_1.setitem(ll_new, "cod_valuta", ls_cod_valuta)
tab_1.det_1.dw_1.setitem(ll_new, "cambio_ven", ld_cambio_ven)
tab_1.det_1.dw_1.setitem(ll_new, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
tab_1.det_1.dw_1.setitem(ll_new, "cod_pagamento", ls_cod_pagamento)
tab_1.det_1.dw_1.setitem(ll_new, "sconto", ld_sconto_testata)
tab_1.det_1.dw_1.setitem(ll_new, "cod_agente_1", ls_cod_agente_1)
tab_1.det_1.dw_1.setitem(ll_new, "cod_agente_2", ls_cod_agente_2)
tab_1.det_1.dw_1.setitem(ll_new, "cod_banca", ls_cod_banca)
tab_1.det_1.dw_1.setitem(ll_new, "cod_imballo", ls_cod_imballo)
tab_1.det_1.dw_1.setitem(ll_new, "aspetto_beni", ls_aspetto_beni)
tab_1.det_1.dw_1.setitem(ll_new, "peso_netto", ld_peso_netto)
tab_1.det_1.dw_1.setitem(ll_new, "peso_lordo", ld_peso_lordo)
tab_1.det_1.dw_1.setitem(ll_new, "num_colli", ld_num_colli)
tab_1.det_1.dw_1.setitem(ll_new, "cod_vettore", ls_cod_vettore)
tab_1.det_1.dw_1.setitem(ll_new, "cod_inoltro", ls_cod_inoltro)
tab_1.det_1.dw_1.setitem(ll_new, "cod_mezzo", ls_cod_mezzo)
tab_1.det_1.dw_1.setitem(ll_new, "causale_trasporto", ls_causale_trasporto)
tab_1.det_1.dw_1.setitem(ll_new, "cod_porto", ls_cod_porto)
tab_1.det_1.dw_1.setitem(ll_new, "cod_resa", ls_cod_resa)

tab_1.det_1.dw_1.setitem(ll_new, "flag_fuori_fido", "N")
tab_1.det_1.dw_1.setitem(ll_new, "flag_blocco", "N")
tab_1.det_1.dw_1.setitem(ll_new, "flag_evasione", "A")

if isnull(ls_flag_riep_bol) or ls_flag_riep_bol="" then  ls_flag_riep_bol = "N"
tab_1.det_1.dw_1.setitem(ll_new, "flag_riep_bol", ls_flag_riep_bol)

if isnull(ls_flag_riep_fat) or ls_flag_riep_fat="" then  ls_flag_riep_fat = "N"
tab_1.det_1.dw_1.setitem(ll_new, "flag_riep_fat", ls_flag_riep_fat)

tab_1.det_1.dw_1.setitem(ll_new, "flag_stampato", "N")
tab_1.det_1.dw_1.setitem(ll_new, "flag_stampata_bcl", "N")

tab_1.det_1.dw_1.setitem(ll_new, "tot_val_ordine", ld_tot_val_offerta)
tab_1.det_1.dw_1.setitem(ll_new, "data_consegna", ldt_data_consegna)
tab_1.det_1.dw_1.setitem(ll_new, "cod_banca_clien_for", ls_cod_banca_clien_for)

if isnull(ls_flag_doc_suc_tes) or ls_flag_doc_suc_tes="" then ls_flag_doc_suc_tes = "N"
tab_1.det_1.dw_1.setitem(ll_new, "flag_doc_suc_tes", ls_flag_doc_suc_tes)

if isnull(ls_flag_doc_suc_pie) or ls_flag_doc_suc_pie="" then ls_flag_doc_suc_pie = "N"
tab_1.det_1.dw_1.setitem(ll_new, "flag_doc_suc_pie",ls_flag_doc_suc_pie)

if isnull(ls_flag_st_note_tes) or ls_flag_st_note_tes="" then ls_flag_st_note_tes = "N"
tab_1.det_1.dw_1.setitem(ll_new, "flag_st_note_tes", ls_flag_st_note_tes)

if isnull(ls_flag_st_note_pie) or ls_flag_st_note_pie="" then ls_flag_st_note_pie = "N"
tab_1.det_1.dw_1.setitem(ll_new, "flag_st_note_pie", ls_flag_st_note_pie)

tab_1.det_1.dw_1.setitem(ll_new, "flag_controllato", "N")
tab_1.det_1.dw_1.setitem(ll_new, "flag_aut_sblocco_fido", "N")


/*
flag_doc_suc_tes
flag_st_note_tes

flag_doc_suc_pie
flag_st_note_pie
*/

wf_dati_cliente(ls_cod_cliente)


//imposta adesso i dettagli
li_ret = wf_imposta_dettagli(ldt_data_consegna, as_errore)
if li_ret<0 then
	return -1
end if

return 0





end function

public function integer wf_imposta_dettagli (datetime adt_data_cons, ref string as_errore);datastore		lds_data

string				ls_sql, ls_cod_tipo_det_ven, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_cod_versione, &
					ls_flag_gen_commessa, ls_flag_doc_suc_det, ls_flag_prezzo_bloccato, ls_flag_tipo_det_ven

long				ll_index, ll_tot, ll_prog_riga_ord_ven, ll_num_riga_appartenenza, ll_new, ll_null

dec{4}		ld_quan_offerta, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
				ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_provvigione_1, ld_provvigione_2, ld_val_riga, ld_num_confezioni, ld_num_pezzi_confezione, ld_quantita_um, ld_prezzo_um
				
datetime		ldt_data_consegna_det


setnull(ll_null)


ls_sql = "select		prog_riga_off_ven,"+&
						"cod_tipo_det_ven,"+&
						"cod_misura,"+&
						"cod_prodotto,"+&
						"des_prodotto,"+&
						"quan_offerta,"+&
						"prezzo_vendita,"+&
						"fat_conversione_ven,"+&
						"sconto_1,"+&
						"sconto_2,"+&
						"provvigione_1,"+&
						"provvigione_2,"+&
						"val_riga,"+&
						"cod_iva,"+&
						"data_consegna,"+&
						"nota_dettaglio,"+&
						"sconto_3,"+&
						"sconto_4,"+&
						"sconto_5,"+&
						"sconto_6,"+&
						"sconto_7,"+&
						"sconto_8,"+&
						"sconto_9,"+&
						"sconto_10,"+&
						"cod_centro_costo,"+&
						"cod_versione,"+&
						"num_confezioni,"+&
						"num_pezzi_confezione,"+&
						"flag_gen_commessa,"+&
						"flag_doc_suc_det,"+&
						"quantita_um,"+&
						"prezzo_um,"+&
						"flag_prezzo_bloccato "+&
		"from det_off_ven "+&
		"where	cod_azienda ='" + s_cs_xx.cod_azienda + "' and  "+&
					"anno_registrazione = " + string(ii_anno_offerta) + " and "+&
					"num_registrazione = " + string(il_num_offerta) + " "+&
		"order by prog_riga_off_ven asc"
		
ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)

if ll_tot<0 then
	as_errore = "Errore creazione ds dettagli: " + as_errore
	return -1
end if

for ll_index=1 to ll_tot
	ll_prog_riga_ord_ven		= lds_data.getitemnumber(ll_index, 1)
	ls_cod_tipo_det_ven		= lds_data.getitemstring(ll_index, 2)
	ls_cod_misura				= lds_data.getitemstring(ll_index, 3)			
	ls_cod_prodotto			= lds_data.getitemstring(ll_index, 4)
	ls_des_prodotto			= lds_data.getitemstring(ll_index, 5)
	ld_quan_offerta			= lds_data.getitemdecimal(ll_index, 6)
	ld_prezzo_vendita			= lds_data.getitemdecimal(ll_index, 7)
	ld_fat_conversione_ven	= lds_data.getitemdecimal(ll_index, 8)
	ld_sconto_1					= lds_data.getitemdecimal(ll_index, 9)	
	ld_sconto_2					= lds_data.getitemdecimal(ll_index, 10)
	ld_provvigione_1			= lds_data.getitemdecimal(ll_index, 11)
	ld_provvigione_2			= lds_data.getitemdecimal(ll_index, 12)
	ld_val_riga					= lds_data.getitemdecimal(ll_index, 13)
	ls_cod_iva					= lds_data.getitemstring(ll_index, 14)
	ldt_data_consegna_det	= lds_data.getitemdatetime(ll_index, 15)
	ls_nota_dettaglio			= lds_data.getitemstring(ll_index, 16)
	ld_sconto_3					= lds_data.getitemdecimal(ll_index, 17)
	ld_sconto_4					= lds_data.getitemdecimal(ll_index, 18)
	ld_sconto_5					= lds_data.getitemdecimal(ll_index, 19)
	ld_sconto_6					= lds_data.getitemdecimal(ll_index, 20)
	ld_sconto_7					= lds_data.getitemdecimal(ll_index, 21)
	ld_sconto_8					= lds_data.getitemdecimal(ll_index, 22)
	ld_sconto_9					= lds_data.getitemdecimal(ll_index, 23)
	ld_sconto_10				= lds_data.getitemdecimal(ll_index, 24)
	ls_cod_centro_costo		= lds_data.getitemstring(ll_index, 25)
	ls_cod_versione			= lds_data.getitemstring(ll_index, 26)
	ld_num_confezioni			= lds_data.getitemdecimal(ll_index, 27)
	ld_num_pezzi_confezione = lds_data.getitemdecimal(ll_index, 28)
	ls_flag_gen_commessa	= lds_data.getitemstring(ll_index, 29)
	ll_num_riga_appartenenza = 0
	ls_flag_doc_suc_det		= lds_data.getitemstring(ll_index, 30)
	ld_quantita_um				= lds_data.getitemdecimal(ll_index, 31)
	ld_prezzo_um				= lds_data.getitemdecimal(ll_index, 32)
	ls_flag_prezzo_bloccato	= lds_data.getitemstring(ll_index, 33)
	
	if ls_cod_prodotto="" then setnull(ls_cod_prodotto)
	
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode<0 then
		as_errore = "Errore in fase di lettura tipi dettagli: " + sqlca.sqlerrtext
		destroy  lds_data;
		return -1
	end if
	
	//nota dettaglio
	if ls_flag_doc_suc_det = 'I' then 
		select flag_doc_suc
		into :ls_flag_doc_suc_det
		from tab_tipi_det_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

	//Se la data di consegna del dettaglio è vuota imposto quella della testata
	if isnull(ldt_data_consegna_det) then ldt_data_consegna_det = adt_data_cons
	
	//insertrow
	ll_new = tab_1.det_5.dw_5.insertrow(0)
	
	//setitem
	tab_1.det_5.dw_5.setitem(ll_new, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
	tab_1.det_5.dw_5.setitem(ll_new, "cod_tipo_det_ven", ls_cod_tipo_det_ven)
	tab_1.det_5.dw_5.setitem(ll_new, "cod_misura", ls_cod_misura)
	tab_1.det_5.dw_5.setitem(ll_new, "cod_prodotto", ls_cod_prodotto)
	tab_1.det_5.dw_5.setitem(ll_new, "des_prodotto", ls_des_prodotto)
	tab_1.det_5.dw_5.setitem(ll_new, "quan_ordine", ld_quan_offerta)
	tab_1.det_5.dw_5.setitem(ll_new, "prezzo_vendita", ld_prezzo_vendita)
	tab_1.det_5.dw_5.setitem(ll_new, "fat_conversione_ven", ld_fat_conversione_ven)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_1", ld_sconto_1)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_2", ld_sconto_2)
	tab_1.det_5.dw_5.setitem(ll_new, "provvigione_1", ld_provvigione_1)
	tab_1.det_5.dw_5.setitem(ll_new, "provvigione_2", ld_provvigione_2)
	tab_1.det_5.dw_5.setitem(ll_new, "cod_iva", ls_cod_iva)
	tab_1.det_5.dw_5.setitem(ll_new, "data_consegna", ldt_data_consegna_det)
	tab_1.det_5.dw_5.setitem(ll_new, "val_riga", ld_val_riga)
	tab_1.det_5.dw_5.setitem(ll_new, "quan_in_evasione", 0)
	tab_1.det_5.dw_5.setitem(ll_new, "quan_evasa", 0)
	tab_1.det_5.dw_5.setitem(ll_new, "flag_evasione", "A")
	tab_1.det_5.dw_5.setitem(ll_new, "flag_blocco", "N")
	tab_1.det_5.dw_5.setitem(ll_new, "nota_dettaglio", ls_nota_dettaglio)
	tab_1.det_5.dw_5.setitem(ll_new, "anno_registrazione_off", ii_anno_offerta)
	tab_1.det_5.dw_5.setitem(ll_new, "num_registrazione_off", il_num_offerta)
	tab_1.det_5.dw_5.setitem(ll_new, "prog_riga_off_ven", ll_prog_riga_ord_ven)
	tab_1.det_5.dw_5.setitem(ll_new, "anno_commessa", ll_null)
	tab_1.det_5.dw_5.setitem(ll_new, "num_commessa", ll_null)
	tab_1.det_5.dw_5.setitem(ll_new, "cod_centro_costo", ldt_data_consegna_det)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_3", ld_sconto_3)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_4", ld_sconto_4)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_5", ld_sconto_5)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_6", ld_sconto_6)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_7", ld_sconto_7)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_8", ld_sconto_8)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_9", ld_sconto_9)
	tab_1.det_5.dw_5.setitem(ll_new, "sconto_10", ld_sconto_10)
	tab_1.det_5.dw_5.setitem(ll_new, "cod_versione", ls_cod_versione)
	tab_1.det_5.dw_5.setitem(ll_new, "num_confezioni", ld_num_confezioni)
	tab_1.det_5.dw_5.setitem(ll_new, "num_pezzi_confezione", ld_num_pezzi_confezione)
	tab_1.det_5.dw_5.setitem(ll_new, "flag_gen_commessa", ls_flag_gen_commessa)
	tab_1.det_5.dw_5.setitem(ll_new, "num_riga_appartenenza", ll_num_riga_appartenenza)
	tab_1.det_5.dw_5.setitem(ll_new, "flag_doc_suc_det", "I")
	tab_1.det_5.dw_5.setitem(ll_new, "flag_st_note_det", "I")
	tab_1.det_5.dw_5.setitem(ll_new, "quantita_um", ld_quantita_um)
	tab_1.det_5.dw_5.setitem(ll_new, "prezzo_um", ld_prezzo_um)
	//tab_1.det_5.dw_5.setitem(ll_new, "flag_prezzo_bloccato", ls_flag_prezzo_bloccato)
	
	
next

destroy  lds_data;

if ll_tot>0 then
	wf_rowfocuschg_details(1, tab_1.det_5.dw_5.getitemstring(1, "cod_tipo_det_ven"), tab_1.det_5.dw_5)
	tab_1.det_5.dw_5.setrow(1)
end if



return 0
end function

public subroutine wf_tipo_det_ven_lista (datawindow fdw_datawindow, string fs_cod_tipo_det_ven);string				ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio, ls_ret
double			ld_prov_agente_1, ld_prov_agente_2, ld_provv_riga_1, ld_provv_riga_2
long				ll_row

setnull(ls_null)

ll_row = fdw_datawindow.getrow()

if ll_row>0 then
else
	return
end if

if not isnull(fs_cod_tipo_det_ven) and fs_cod_tipo_det_ven<>"" then
	
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_tipo_det_ven = :fs_cod_tipo_det_ven;
	
	if sqlca.sqlcode <> 0 then
		//ls_modify = "cod_prodotto.protect='1'"
		ls_modify = "cod_prodotto.tabsequence=0"
		fdw_datawindow.modify(ls_modify)
		
		ls_modify = "cod_prodotto.background.color='12632256'"
		fdw_datawindow.modify(ls_modify)
		return
		
	end if
	
	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
		//ls_modify = "cod_prodotto.protect='0'"
		ls_modify = "cod_prodotto.tabsequence=11"
		ls_ret = fdw_datawindow.modify(ls_modify)
		
		ls_modify = "cod_prodotto.background.color='16777215'"
		ls_ret = fdw_datawindow.modify(ls_modify)
	else
		if not isnull(fdw_datawindow.getitemstring(ll_row,"cod_prodotto")) then
			fdw_datawindow.setitem(ll_row,"cod_prodotto",ls_null)
		end if
		//ls_modify = "cod_prodotto.protect='1'"
		ls_modify = "cod_prodotto.tabsequence=0"
		ls_ret = fdw_datawindow.modify(ls_modify)
		
		ls_modify = "cod_prodotto.background.color='12632256'"
		ls_ret = fdw_datawindow.modify(ls_modify)
	end if
	
else
	//ls_modify = "cod_prodotto.protect='1'"
	ls_modify = "cod_prodotto.tabsequence=0"
	fdw_datawindow.modify(ls_modify)
	
	ls_modify = "cod_prodotto.background.color='12632256'"
	fdw_datawindow.modify(ls_modify)
end if
end subroutine

public subroutine wf_dati_cliente (string as_cod_cliente);
string			ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_partita_iva, ls_cod_fiscale


select		rag_soc_1,
			rag_soc_2,
			indirizzo,
			frazione,
			cap,
			localita,
			provincia,
			telefono,
			fax,
			telex,
			partita_iva,
			cod_fiscale
into	:ls_rag_soc_1,    
		:ls_rag_soc_2,
		:ls_indirizzo,    
		:ls_frazione,
		:ls_cap,
		:ls_localita,
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_telex,
		:ls_partita_iva,
		:ls_cod_fiscale
	from   anag_clienti
	where	cod_azienda = :s_cs_xx.cod_azienda and 
				cod_cliente = :as_cod_cliente;

if sqlca.sqlcode = 0 then
	if not isnull(as_cod_cliente) then
		tab_1.det_2.dw_2.modify("st_cod_cliente.text='" + as_cod_cliente + "'")
	else
		as_cod_cliente = ""
		tab_1.det_2.dw_2.modify("st_cod_cliente.text=''")
	end if
	if not isnull(ls_rag_soc_1) then
		tab_1.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
	else
		tab_1.det_2.dw_2.modify("st_rag_soc_1.text=''")
	end if
	if not isnull(ls_rag_soc_2) then
		tab_1.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
	else
		tab_1.det_2.dw_2.modify("st_rag_soc_2.text=''")
	end if
	if not isnull(ls_indirizzo) then
		tab_1.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
	else
		tab_1.det_2.dw_2.modify("st_indirizzo.text=''")
	end if
	if not isnull(ls_frazione) then
		tab_1.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
	else
		tab_1.det_2.dw_2.modify("st_frazione.text=''")
	end if
	if not isnull(ls_cap) then
		tab_1.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
	else
		tab_1.det_2.dw_2.modify("st_cap.text=''")
	end if
	if not isnull(ls_localita) then
		tab_1.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
	else
		tab_1.det_2.dw_2.modify("st_localita.text=''")
	end if
	if not isnull(ls_provincia) then
		tab_1.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
	else
		tab_1.det_2.dw_2.modify("st_provincia.text=''")
	end if
	if not isnull(ls_telefono) then
		tab_1.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
	else
		tab_1.det_2.dw_2.modify("st_telefono.text=''")
	end if
	if not isnull(ls_fax) then
		tab_1.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
	else
		tab_1.det_2.dw_2.modify("st_fax.text=''")
	end if
	if not isnull(ls_telex) then
		tab_1.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
	else
		tab_1.det_2.dw_2.modify("st_telex.text=''")
	end if
	if not isnull(ls_partita_iva) then
		tab_1.det_2.dw_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
	else
		tab_1.det_2.dw_2.modify("st_partita_iva.text=''")
	end if
	if not isnull(ls_cod_fiscale) then
		tab_1.det_2.dw_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
	else
		tab_1.det_2.dw_2.modify("st_cod_fiscale.text=''")
	end if

	f_po_loaddddw_dw(tab_1.det_1.dw_1, &
						  "cod_des_cliente", &
						  sqlca, &
						  "anag_des_clienti", &
						  "cod_des_cliente", &
						  "rag_soc_1", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + as_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
else
	tab_1.det_2.dw_2.modify("st_cod_cliente.text=''")
	tab_1.det_2.dw_2.modify("st_rag_soc_1.text=''")
	tab_1.det_2.dw_2.modify("st_rag_soc_2.text=''")
	tab_1.det_2.dw_2.modify("st_indirizzo.text=''")
	tab_1.det_2.dw_2.modify("st_frazione.text=''")
	tab_1.det_2.dw_2.modify("st_cap.text=''")
	tab_1.det_2.dw_2.modify("st_localita.text=''")
	tab_1.det_2.dw_2.modify("st_provincia.text=''")
	tab_1.det_2.dw_2.modify("st_telefono.text=''")
	tab_1.det_2.dw_2.modify("st_fax.text=''")
	tab_1.det_2.dw_2.modify("st_telex.text=''")
	tab_1.det_2.dw_2.modify("st_partita_iva.text=''")
	tab_1.det_2.dw_2.modify("st_cod_fiscale.text=''")
end if
end subroutine

public function integer wf_controlla_dettagli (integer ai_anno, long al_numero, ref string as_errore);long			ll_index, ll_tot, ll_riga

string			ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_null, ls_cod_prodotto

datetime		ldt_data_consegna, ldt_data_consegna_testata


ll_tot = tab_1.det_5.dw_5.rowcount()

setnull(ls_null)

ldt_data_consegna_testata = tab_1.det_1.dw_1.getitemdatetime(1, "data_consegna")


for ll_index = 1 to ll_tot
	tab_1.det_5.dw_5.setitem(ll_index, "cod_azienda", s_cs_xx.cod_azienda)
	tab_1.det_5.dw_5.setitem(ll_index, "anno_registrazione", ai_anno)
	tab_1.det_5.dw_5.setitem(ll_index, "num_registrazione", al_numero)
	ll_riga = tab_1.det_5.dw_5.getitemnumber(ll_index, "prog_riga_ord_ven")

	ls_cod_tipo_det_ven = tab_1.det_5.dw_5.getitemstring(ll_index, "cod_tipo_det_ven")

	if ls_cod_tipo_det_ven="" or isnull(ls_cod_tipo_det_ven) then
		as_errore = "Il tipo dettaglio è obbligatorio: verifica la riga "+string(ai_anno)+"/"+string(al_numero)+"/"+string(ll_riga)+" !"
		return -1
	end if

	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
		ls_cod_prodotto = tab_1.det_5.dw_5.getitemstring(ll_index, "cod_prodotto")

		if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
			as_errore = "Il prodotto alla riga "+string(ai_anno)+"/"+string(al_numero)+"/"+string(ll_riga)+" è obbligatorio per il tipo dettaglio selezionato!"
			return -1
		end if

	else
		tab_1.det_5.dw_5.setitem(ll_index, "cod_prodotto", ls_null)
	end if
	
	ldt_data_consegna = tab_1.det_5.dw_5.getitemdatetime(ll_index, "data_consegna")
	if isnull(ldt_data_consegna) or year(date(ldt_data_consegna)) < 1980 then
		tab_1.det_5.dw_5.setitem(ll_index, "data_consegna", ldt_data_consegna_testata)
	end if
	
next


return 0
end function

public function integer wf_salva (ref integer ai_anno_ordine, ref long al_num_ordine, ref string as_errore);
integer									li_ret, li_anno_ordine

long										ll_num_ordine



//check su testata e dettagli
li_ret = wf_controlla_dati(ai_anno_ordine, al_num_ordine, as_errore)

if li_ret < 0 then
	return -1
end if

//update delle datawindow
li_ret = tab_1.det_1.dw_1.update()
if li_ret<0 then
	as_errore = "Errore in update datawindow di testata!"
	return -1
end if

li_ret = tab_1.det_5.dw_5.update()
if li_ret<0 then
	as_errore = "Errore in update datawindow di dettaglio!"
	return -1
end if

//operazioni successive al salvataggio (es. calcolo documento, creazione statistiche, varianti, integrazioni, ecc...)
li_ret = wf_post_save_details(ai_anno_ordine, al_num_ordine, as_errore)

if li_ret < 0 then
	return -1
end if


//se arrivi fin qui fai il commit all'uscita della funzione
return 0
end function

public function integer wf_controlla_dati (ref integer ai_anno_ordine, ref long al_num_ordine, ref string as_errore);
long				ll_num_reg, ll_anno_reg, ll_max_registrazione

integer			li_ret


tab_1.det_1.dw_1.setitem(1, "cod_azienda", s_cs_xx.cod_azienda)

ll_anno_reg = f_anno_esercizio()
if ll_anno_reg > 0 then
	tab_1.det_1.dw_1.setitem(1, "anno_registrazione", int(ll_anno_reg))
else
	as_errore = "Impostare l'anno di esercizio in parametri aziendali"
	return -1
end if

select num_registrazione
into   :ll_num_reg
from   con_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ll_num_reg) then ll_num_reg = 0

ll_max_registrazione = 0

select max(num_registrazione)
into   :ll_max_registrazione
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg;
		 
if not isnull(ll_max_registrazione) then
	if ll_max_registrazione >= ll_num_reg then ll_num_reg = ll_max_registrazione
end if

ll_num_reg += 1

 tab_1.det_1.dw_1.setitem(1, "num_registrazione", ll_num_reg)
 
update con_ord_ven
set    num_registrazione = :ll_num_reg
where  cod_azienda = :s_cs_xx.cod_azienda;


li_ret = wf_controlla_dettagli(ll_anno_reg, ll_num_reg, as_errore)

if li_ret<0 then
	return -1
end if

ai_anno_ordine = ll_anno_reg
al_num_ordine = ll_num_reg

return 0
end function

public function integer wf_post_save_details (integer ai_anno_ordine, long al_num_ordine, ref string as_errore);long				ll_index, ll_tot, ll_prog_riga, ll_num_ordine

integer			li_anno_ordine, li_ret

dec{4}			ld_quan_ordine

string				ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_cod_tipo_analisi_ord, ls_cod_tipo_off_ven, ls_cod_tipo_analisi_off, ls_cod_tipo_ord_ven, &
					ls_flag_tipo_det_ven, ls_cod_prodotto, ls_cod_tipo_det_ven

uo_calcola_documento_euro luo_calcolo

//N.B.
//il numero riga offerta conciderà con il numero riga ordine ...

//calcolo del documento
luo_calcolo = create uo_calcola_documento_euro
luo_calcolo.ib_salta_esposiz_cliente = true

li_ret = luo_calcolo.uof_calcola_documento(ai_anno_ordine, al_num_ordine, "ord_ven", as_errore)
destroy luo_calcolo

if li_ret <> 0 then
	return -1;
end if
//--------------------------------------------

//leggo tipo analisi legata al tipo ordine vendita
ls_cod_tipo_ord_ven = tab_1.det_1.dw_1.getitemstring(1, "cod_tipo_ord_ven")

select cod_tipo_analisi
into :ls_cod_tipo_analisi_ord
from tab_tipi_ord_ven  
where 	cod_azienda = :s_cs_xx.cod_azienda and  
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in lettura cod_tipo_analisi da Tipi Ordini : " + sqlca.sqlerrtext
	return -1
end if
//---------------------------------------------

//leggo tipo analisi legata al tipo offerta vendita
select cod_tipo_off_ven
into :ls_cod_tipo_off_ven
from tes_off_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ii_anno_offerta and
			num_registrazione=:il_num_offerta;
			
select 	cod_tipo_analisi  
into 	:ls_cod_tipo_analisi_off
from   tab_tipi_off_ven  
where	cod_azienda = :s_cs_xx.cod_azienda and  
			cod_tipo_off_ven = :ls_cod_tipo_off_ven;
//---------------------------------------------

ll_tot = tab_1.det_5.dw_5.rowcount()

for ll_index=1 to ll_tot
	ld_quan_ordine = tab_1.det_5.dw_5.getitemdecimal(ll_index, "quan_ordine")
	ls_cod_prodotto = tab_1.det_5.dw_5.getitemstring(ll_index, "cod_prodotto")
	
	ls_cod_tipo_det_ven = tab_1.det_5.dw_5.getitemstring(ll_index, "cod_tipo_det_ven")

	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if ls_flag_tipo_det_ven = "M" and ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		
		if isnull(ld_quan_ordine) then ld_quan_ordine=0
		
		update anag_prodotti  
		set quan_impegnata = quan_impegnata + :ld_quan_ordine
		where	cod_azienda = :s_cs_xx.cod_azienda and  
					cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode < 0 then
			as_errore = "Errore in fase di aggiornamento impegnato prodotto '"+ls_cod_prodotto+"' : " + sqlca.sqlerrtext
			return -1
		end if
	end if
	
	
	ls_tabella_orig = "det_off_ven_stat"
	ls_tabella_des = "det_ord_ven_stat"
	ls_prog_riga_doc_orig = "prog_riga_off_ven"
	ls_prog_riga_doc_des = "prog_riga_ord_ven"
	
	li_anno_ordine = tab_1.det_5.dw_5.getitemnumber(ll_index, "anno_registrazione")
	ll_num_ordine = tab_1.det_5.dw_5.getitemnumber(ll_index, "num_registrazione")
	ll_prog_riga = tab_1.det_5.dw_5.getitemnumber(ll_index, "prog_riga_ord_ven")
	
	if f_genera_det_stat(	ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
								ii_anno_offerta, il_num_offerta, ll_prog_riga, li_anno_ordine, ll_num_ordine, ll_prog_riga) = -1 then
		return -1
	end if	
	

	if ls_cod_tipo_analisi_off <> ls_cod_tipo_analisi_ord and ls_cod_tipo_analisi_ord<>"" and not isnull(ls_cod_tipo_analisi_ord) then
		if f_crea_distribuzione(ls_cod_tipo_analisi_ord, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_ordine, li_anno_ordine, ll_prog_riga, 4) = -1 then
			as_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici." +  sqlca.sqlerrtext
			return -1
		end if
	end if
	
	
	// duplicazione delle varianti		
	insert into varianti_det_ord_ven (
		cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_ord_ven,
		cod_prodotto_padre,
		num_sequenza,
		cod_prodotto_figlio,
		cod_versione_figlio,
		cod_versione,
		cod_misura,
		cod_prodotto,
		quan_tecnica,
		quan_utilizzo,
		dim_x,
		dim_y,
		dim_z,
		dim_t,
		coef_calcolo,
		flag_esclusione,
		formula_tempo,
		des_estesa,
		flag_materia_prima,
		lead_time,
		cod_versione_variante
	) select
		:s_cs_xx.cod_azienda,
		:li_anno_ordine,
		:ll_num_ordine,
		:ll_prog_riga,
		cod_prodotto_padre,
		num_sequenza,
		cod_prodotto_figlio,
		cod_versione_figlio,
		cod_versione,
		cod_misura,
		cod_prodotto,
		quan_tecnica,
		quan_utilizzo,
		dim_x,
		dim_y,
		dim_z,
		dim_t,
		coef_calcolo,
		flag_esclusione,
		formula_tempo,
		des_estesa,
		flag_materia_prima,
		lead_time,
		cod_versione_variante
	from varianti_det_off_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ii_anno_offerta and
		num_registrazione = :il_num_offerta and
		prog_riga_off_ven = :ll_prog_riga;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore durante la creazione varianti ordine da varianti offerta : " + sqlca.sqlerrtext
		return -1
	end if
	
	
	// duplicazione delle integrazioni
	insert into integrazioni_det_ord_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_ord_ven,   
		progressivo,   
		cod_prodotto_padre,   
		cod_versione_padre,   
		num_sequenza,   
		cod_prodotto_figlio,   
		cod_versione_figlio,   
		cod_misura,   
		quan_tecnica,   
		quan_utilizzo,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,   
		coef_calcolo,   
		des_estesa,   
		cod_formula_quan_utilizzo,   
		flag_escludibile,   
		flag_materia_prima,   
		flag_arrotonda,
		lead_time
	) select 
		:s_cs_xx.cod_azienda,   
		:li_anno_ordine,
		:ll_num_ordine,
		:ll_prog_riga,
		progressivo,   
		cod_prodotto_padre,   
		cod_versione_padre,   
		num_sequenza,   
		cod_prodotto_figlio,   
		cod_versione_figlio,   
		cod_misura,   
		quan_tecnica,   
		quan_utilizzo,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,   
		coef_calcolo,   
		des_estesa,   
		cod_formula_quan_utilizzo,   
		flag_escludibile,   
		flag_materia_prima,   
		flag_arrotonda,
		lead_time
		from  integrazioni_det_off_ven  
		where
			cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione = :ii_anno_offerta and
			num_registrazione = :il_num_offerta and
			prog_riga_off_ven = :ll_prog_riga;
		
	if sqlca.sqlcode < 0 then
		as_errore = "Errore durante la creazione integrazioni ordine da integrazioni offerta : " + sqlca.sqlerrtext
		return -1
	end if

next
	
return 0
end function

public subroutine wf_rowfocuschg_details (long al_row, string as_cod_tipo_det_ven, ref datawindow adw_data);string				ls_modify, ls_cod_iva

ls_modify = "cod_prodotto.tabsequence=11~t"
adw_data.modify(ls_modify)
ls_modify = "cod_prodotto.background.color='16777215'~t"
adw_data.modify(ls_modify)

ls_modify = "des_prodotto.protect='0'~t"
adw_data.modify(ls_modify)
ls_modify = "des_prodotto.background.color='16777215'~t"
adw_data.modify(ls_modify)

adw_data.modify(ls_modify)
ls_modify = "quan_ordine.protect='0'~t"
adw_data.modify(ls_modify)
ls_modify = "quan_ordine.background.color='16777215'~t"
adw_data.modify(ls_modify)
ls_modify = "prezzo_vendita.protect='0'~t"
adw_data.modify(ls_modify)
ls_modify = "prezzo_vendita.background.color='16777215'~t"
adw_data.modify(ls_modify)
ls_modify = "sconto_1.protect='0'~t"
adw_data.modify(ls_modify)
ls_modify = "sconto_1.background.color='16777215'~t"
adw_data.modify(ls_modify)

//ld_datawindow = adw_data
wf_tipo_det_ven_lista(adw_data, as_cod_tipo_det_ven)

if isnull(adw_data.getitemstring(al_row, "cod_iva")) then
	select cod_iva  
	into   :ls_cod_iva  
	from   tab_tipi_det_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :as_cod_tipo_det_ven;
			 
	if sqlca.sqlcode = 0 and ls_cod_iva<>"" then
		adw_data.setitem(al_row, "cod_iva", ls_cod_iva)
	end if
end if
end subroutine

public subroutine wf_sel_prodotto (long al_row);
datawindow			ldw_null
any					la_cod_prodotto[]
string					ls_cod_prodotto, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_des_prodotto


//controlla se puoi selezionare il codice prodotto, in base al tipo dettaglio
ls_cod_tipo_det_ven = tab_1.det_5.dw_5.getitemstring(al_row, "cod_tipo_det_ven")

if ls_cod_tipo_det_ven="" or isnull(ls_cod_tipo_det_ven) then
	g_mb.warning("Seleziona prima il codice Tipo dettaglio di Vendita della riga!")
	return
end if

select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
	//OK alla selezione da anagrafica
else
	g_mb.warning("Il tipo dettaglio della riga NON prevede la selezione del codice dall'anagrafica!")
	return
end if

//se arrivi fin qui è tutto OK
setnull(ldw_null)
guo_ricerca.uof_set_response( )
guo_ricerca.uof_ricerca_prodotto(ldw_null	, "")
guo_ricerca.uof_get_results( la_cod_prodotto[] )

if upperbound(la_cod_prodotto[]) > 0 then
	ls_cod_prodotto = string(la_cod_prodotto[1])
	
	if ls_cod_prodotto="" then setnull(ls_cod_prodotto)
	tab_1.det_5.dw_5.setitem(al_row, "cod_prodotto", ls_cod_prodotto)
	
	if not isnull(ls_cod_prodotto) then
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
		
		tab_1.det_5.dw_5.setitem(al_row, "des_prodotto", ls_des_prodotto)
	end if
	
else
	//probabilmente hai fatto annulla dalla ricerca
	return
end if


return
end subroutine

on w_tes_gen_ord_ven_single.create
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.st_titolo=create st_titolo
this.tab_1=create tab_1
this.Control[]={this.cb_annulla,&
this.cb_conferma,&
this.st_titolo,&
this.tab_1}
end on

on w_tes_gen_ord_ven_single.destroy
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.st_titolo)
destroy(this.tab_1)
end on

event open;
s_cs_xx_parametri			lstr_parametri



lstr_parametri = message.powerobjectparm

ii_anno_offerta = lstr_parametri.parametro_ul_1
il_num_offerta = lstr_parametri.parametro_ul_2

st_titolo.text = "Generazione ordine di Vendita a partire dall'Offerta di Vendita n° "+string(ii_anno_offerta)+"/"+string(il_num_offerta)

tab_1.det_1.dw_1.settransobject(sqlca)
tab_1.det_2.dw_2.settransobject(sqlca)
tab_1.det_3.dw_3.settransobject(sqlca)
tab_1.det_4.dw_4.settransobject(sqlca)
tab_1.det_5.dw_5.settransobject(sqlca)

tab_1.det_1.dw_1.object.b_ricerca_cliente.visible = false
tab_1.det_1.dw_1.object.cod_cliente.protect = 1

tab_1.det_1.dw_1.sharedata(tab_1.det_2.dw_2)
tab_1.det_1.dw_1.sharedata(tab_1.det_3.dw_3)
tab_1.det_1.dw_1.sharedata(tab_1.det_4.dw_4)


event post ue_carica_dropdown()
event post ue_postopen()
end event

type cb_annulla from commandbutton within w_tes_gen_ord_ven_single
integer x = 2734
integer y = 24
integer width = 402
integer height = 84
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;

if not g_mb.confirm("Annullare l'operazione di creazione?") then return

close(parent)
end event

type cb_conferma from commandbutton within w_tes_gen_ord_ven_single
integer x = 3177
integer y = 24
integer width = 402
integer height = 84
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;
integer			li_ret, li_anno_ordine

long				ll_num_ordine

string				as_messaggio


tab_1.det_1.dw_1.accepttext()
tab_1.det_2.dw_2.accepttext()
tab_1.det_3.dw_3.accepttext()
tab_1.det_4.dw_4.accepttext()
tab_1.det_5.dw_5.accepttext()


if not g_mb.confirm("Procedo con la generazione dell'ordine?") then return

li_ret = wf_salva(li_anno_ordine, ll_num_ordine, as_messaggio)

if li_ret < 0 then
	rollback;
	if as_messaggio<>"" and not isnull(as_messaggio) then g_mb.error(as_messaggio)
	return
	
else
	commit;
	g_mb.success("Creato l'ordine n° "+string(li_anno_ordine)+"/"+string(ll_num_ordine))
	close(parent)
	
end if
end event

type st_titolo from statictext within w_tes_gen_ord_ven_single
integer x = 23
integer y = 20
integer width = 2624
integer height = 88
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Generazione ordine di Vendita a partire dall~'Offerta di vendita n°"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type tab_1 from tab within w_tes_gen_ord_ven_single
integer x = 23
integer y = 152
integer width = 4695
integer height = 1768
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
det_1 det_1
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
end type

on tab_1.create
this.det_1=create det_1
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
this.det_5=create det_5
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4,&
this.det_5}
end on

on tab_1.destroy
destroy(this.det_1)
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
destroy(this.det_5)
end on

type det_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4658
integer height = 1640
long backcolor = 12632256
string text = "Testata"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "Destination!"
long picturemaskcolor = 536870912
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
this.Control[]={this.dw_1}
end on

on det_1.destroy
destroy(this.dw_1)
end on

type dw_1 from datawindow within det_1
event ue_data_consegna ( string data )
integer x = 27
integer y = 24
integer width = 3163
integer height = 1508
integer taborder = 20
string title = "none"
string dataobject = "d_tes_ord_ven_det_1"
boolean border = false
boolean livescroll = true
end type

event ue_data_consegna(string data);datetime			ldt_data_consegna, ldt_data_consegna_det
long				ll_index

if data<>"" then
	ldt_data_consegna = tab_1.det_1.dw_1.getitemdatetime(1, "data_consegna")
else
	setnull(ldt_data_consegna)
end if

for ll_index=1 to tab_1.det_5.dw_5.rowcount()
	ldt_data_consegna_det = tab_1.det_5.dw_5.getitemdatetime(ll_index, "data_consegna")
	
	if isnull(ldt_data_consegna_det) or year(date(ldt_data_consegna_det))<= 1980 then
		tab_1.det_5.dw_5.setitem(ll_index, "data_consegna", ldt_data_consegna)
	end if
next
end event

event buttonclicked;if row>0 then
	choose case dwo.name	
//		case "b_ricerca_cliente"
//			guo_ricerca.uof_ricerca_cliente(dw_tes_ord_ven_det_1,"cod_cliente")
			
	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(tab_1.det_1.dw_1,"cod_banca_clien_for")
			
			
	end choose
end if
end event

event itemchanged;datetime			ldt_data_consegna

if row>0 then
	choose case dwo.name	
		case "data_consegna"
			tab_1.det_1.dw_1.event post ue_data_consegna(data)
			
			
	end choose
end if
end event

type det_2 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4658
integer height = 1640
long backcolor = 12632256
string text = "Cliente/Destinazione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "Custom002!"
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from datawindow within det_2
integer x = 37
integer y = 28
integer width = 2935
integer height = 1400
integer taborder = 30
string title = "none"
string dataobject = "d_tes_ord_ven_det_2"
boolean border = false
boolean livescroll = true
end type

type det_3 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4658
integer height = 1640
long backcolor = 12632256
string text = "Trasporto/Totali"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "RunSCC!"
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from datawindow within det_3
integer x = 32
integer y = 32
integer width = 2711
integer height = 1496
integer taborder = 40
string title = "none"
string dataobject = "d_tes_ord_ven_det_3"
boolean border = false
boolean livescroll = true
end type

type det_4 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4658
integer height = 1640
long backcolor = 12632256
string text = "Note"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "Custom072!"
long picturemaskcolor = 536870912
dw_4 dw_4
end type

on det_4.create
this.dw_4=create dw_4
this.Control[]={this.dw_4}
end on

on det_4.destroy
destroy(this.dw_4)
end on

type dw_4 from datawindow within det_4
integer x = 27
integer y = 24
integer width = 3136
integer height = 1208
integer taborder = 40
string title = "none"
string dataobject = "d_tes_ord_ven_det_4"
boolean border = false
boolean livescroll = true
end type

type det_5 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4658
integer height = 1640
long backcolor = 12632256
string text = "Dettagli"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "CreateTable5!"
long picturemaskcolor = 536870912
dw_5 dw_5
end type

on det_5.create
this.dw_5=create dw_5
this.Control[]={this.dw_5}
end on

on det_5.destroy
destroy(this.dw_5)
end on

type dw_5 from datawindow within det_5
event ue_key pbm_dwnkey
integer x = 50
integer y = 56
integer width = 4553
integer height = 1540
integer taborder = 30
string title = "none"
string dataobject = "d_det_ord_ven_gen_off"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;//
//
//choose case this.getcolumnname()
//	case "cod_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			guo_ricerca.uof_ricerca_prodotto(tab_1.det_5.dw_5, "cod_prodotto")
//		end if
//		
//end choose
end event

event itemchanged;string				ls_modify, ls_cod_iva, ls_des_prodotto, ls_cod_misura_mag, ls_cod_misura,ls_flag_decimali, ls_null

datawindow		ld_datawindow

double			ld_fat_conversione

uo_default_prodotto luo_default_prodotto


setnull(ls_null)

if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_tipo_det_ven"
		ls_modify = "cod_prodotto.tabsequence=11~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)

		ls_modify = "des_prodotto.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
  
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "quan_ordine.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "quan_ordine.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "prezzo_vendita.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "prezzo_vendita.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
   
         ld_datawindow = tab_1.det_5.dw_5
         wf_tipo_det_ven_lista(ld_datawindow, data)

         if isnull(this.getitemstring(row, "cod_iva")) then
            select tab_tipi_det_ven.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_ven  
            where  cod_azienda = :s_cs_xx.cod_azienda and  
                   cod_tipo_det_ven = :data;
						 
            if sqlca.sqlcode = 0 and ls_cod_iva<>"" then
               tab_1.det_5.dw_5.setitem(row, "cod_iva", ls_cod_iva)
            end if
         end if
		
	case "cod_prodotto"
		
		if data <> "" then
			 select cod_misura_mag, cod_misura_ven, fat_conversione_ven, flag_decimali, cod_iva, des_prodotto
			into   :ls_cod_misura_mag, :ls_cod_misura, :ld_fat_conversione, :ls_flag_decimali, :ls_cod_iva, :ls_des_prodotto
			from   anag_prodotti
			where	cod_azienda = :s_cs_xx.cod_azienda and 
						cod_prodotto = :data;
			
			if sqlca.sqlcode = 0 then
			
				tab_1.det_5.dw_5.setitem(row, "des_prodotto", ls_des_prodotto)
				tab_1.det_5.dw_5.setitem(row, "cod_misura", ls_cod_misura)
				
				if ld_fat_conversione <> 0 then
					tab_1.det_5.dw_5.setitem(row, "fat_conversione_ven", ld_fat_conversione)
				else
					ld_fat_conversione = 1
					tab_1.det_5.dw_5.setitem(row, "fat_conversione_ven", 1)
				end if
				
				if not isnull(ls_cod_iva) and ls_cod_iva<>"" then tab_1.det_5.dw_5.setitem(row, "cod_iva", ls_cod_iva)
				
				
				f_PO_LoadDDDW_DW(tab_1.det_5.dw_5, 	"cod_versione",sqlca,&
																		"distinta_padri","cod_versione","des_versione",&
																		"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + data + "'")
				
				luo_default_prodotto = CREATE uo_default_prodotto
				if luo_default_prodotto.uof_flag_gen_commessa(data) = 0 then
					
					if luo_default_prodotto.is_cod_versione = "" then setnull(luo_default_prodotto.is_cod_versione)
					
					setitem(row, "cod_versione", luo_default_prodotto.is_cod_versione)
					setitem(row, "flag_gen_commessa", luo_default_prodotto.is_flag_gen_commessa)
					
				end if
				destroy luo_default_prodotto
				
			else
				setitem(row, "cod_versione", ls_null)
				setitem(row, "flag_gen_commessa", "N")
				guo_ricerca.uof_ricerca_prodotto(tab_1.det_5.dw_5,"cod_prodotto")
				
			end if
		else
			setitem(row, "cod_versione", ls_null)
			setitem(row, "flag_gen_commessa", "N")
			guo_ricerca.uof_ricerca_prodotto(tab_1.det_5.dw_5,"cod_prodotto")

		end if


		

end choose
end event

event rowfocuschanged;string				ls_cod_prodotto, ls_cod_tipo_det_ven, ls_modify

datawindow		ld_datawindow


if currentrow>0 then
	
	ls_cod_prodotto = tab_1.det_5.dw_5.getitemstring(currentrow, "cod_prodotto")
	ls_cod_tipo_det_ven = tab_1.det_5.dw_5.getitemstring(currentrow, "cod_tipo_det_ven")
	
	if isnull(ls_cod_prodotto) then  ls_cod_prodotto = ""
	
	f_PO_LoadDDDW_DW(tab_1.det_5.dw_5,"cod_versione",sqlca,&
   	              	  "distinta_padri","cod_versione", "des_versione",&
	  			           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + ls_cod_prodotto+"' ")
								 
								 
	if ls_cod_tipo_det_ven<>"" and not isnull(ls_cod_tipo_det_ven) then
		ls_modify = "cod_prodotto.tabsequence=11"
		//ls_modify = "cod_prodotto.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)

		ls_modify = "des_prodotto.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
  
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "quan_ordine.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "quan_ordine.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "prezzo_vendita.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "prezzo_vendita.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         tab_1.det_5.dw_5.modify(ls_modify)
   
         ld_datawindow = tab_1.det_5.dw_5
         wf_tipo_det_ven_lista(ld_datawindow, ls_cod_tipo_det_ven)
	end if
       
else
	wf_tipo_det_ven_lista(ld_datawindow, "") 
end if




end event

event buttonclicked;long			ll_riga

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_elimina"
		ll_riga = tab_1.det_5.dw_5.getitemnumber(row, "prog_riga_ord_ven")
		if not g_mb.confirm("Eliminare la riga con progressivo "+string(ll_riga) + "?") then return
		
		deleterow(row)
		
	
	case "b_sel_prodotto"
		wf_sel_prodotto(row)
		
		
end choose
end event

