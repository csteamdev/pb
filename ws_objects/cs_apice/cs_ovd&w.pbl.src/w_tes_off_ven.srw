﻿$PBExportHeader$w_tes_off_ven.srw
$PBExportComments$Finestra Gestione Testata Offerte di Vendita
forward
global type w_tes_off_ven from w_cs_xx_principale
end type
type dw_folder from u_folder within w_tes_off_ven
end type
type dw_folder_search from u_folder within w_tes_off_ven
end type
type tv_1 from treeview within w_tes_off_ven
end type
type dw_filtro from uo_std_dw within w_tes_off_ven
end type
type dw_tes_off_ven_det_3 from uo_cs_xx_dw within w_tes_off_ven
end type
type dw_tes_off_ven_det_4 from uo_cs_xx_dw within w_tes_off_ven
end type
type dw_tes_off_ven_det_1 from uo_cs_xx_dw within w_tes_off_ven
end type
type dw_tes_ord_ven from datawindow within w_tes_off_ven
end type
type dw_tes_off_ven_det_2 from uo_cs_xx_dw within w_tes_off_ven
end type
end forward

global type w_tes_off_ven from w_cs_xx_principale
integer width = 4539
integer height = 2792
string title = "Offerte Clienti"
event pc_menu_raccoltadati ( )
event pc_menu_corrispondenze ( )
event pc_menu_note ( )
event pc_menu_duplicadocumento ( )
event pc_menu_calcola ( )
event pc_menu_excel ( )
event pc_menu_stampa ( )
event pc_menu_dettagli ( )
event pc_menu_configuratore ( )
event pc_menu_revisiona ( )
event pc_proforma ( )
event ue_auto_search ( )
event pc_menu_genera_ordine ( )
event ue_marginalita ( )
dw_folder dw_folder
dw_folder_search dw_folder_search
tv_1 tv_1
dw_filtro dw_filtro
dw_tes_off_ven_det_3 dw_tes_off_ven_det_3
dw_tes_off_ven_det_4 dw_tes_off_ven_det_4
dw_tes_off_ven_det_1 dw_tes_off_ven_det_1
dw_tes_ord_ven dw_tes_ord_ven
dw_tes_off_ven_det_2 dw_tes_off_ven_det_2
end type
global w_tes_off_ven w_tes_off_ven

type variables
protected:
	string is_sql_filtro, is_SPD = "N"
	boolean ib_clear_tree = false, ib_new = false, ib_GIB = false
	long il_livello_corrente = 0
	datastore ids_store
	ss_record iss_record
	long ll_max_offerte = 200
	string	is_join_det_ord_ven = " join det_off_ven on det_off_ven.cod_azienda=tes_off_ven.cod_azienda and "+&
																"det_off_ven.anno_registrazione=tes_off_ven.anno_registrazione and "+&
																"det_off_ven.num_registrazione=tes_off_ven.num_registrazione "
	
	// icone
	int TIPO_OFF_ICON, OFF_SENZA_NUM_ICO, OFF_APERTA_ICON, OFF_PARZIALE_ICON, OFF_EVASA_ICON
	int CLIENTE_ICON
	int ANNO_ICON
	int OFFERTA_ICON
	int DEPOSITO_ICON
	int OPERATORE_ICON
	int DATASCAD_ICON
	
	uo_fido_cliente		iuo_fido
	string					is_cod_parametro_blocco = "IOV"
	

	
public:
	// usanto solo da ptenda.
	// inserita la variabili qui così da ridurre la creazione di un ulteriore menu.
	// l'evento pc_menu_configuratore è comunque codificato nella w_tes_off_ven_ptenda
	boolean ib_menu_configuratore = false
end variables

forward prototypes
public subroutine wf_report_excel (long fl_anno_registrazione, long fl_num_registrazione)
public function string uof_nome_cella (long fi_riga, long fi_colonna)
public function integer uof_salva (long fl_anno_registrazione, long fl_num_registrazione, string fs_foglio, ref oleobject myoleobject)
public subroutine wf_imposta_ricerca ()
public subroutine wf_cancella_tree ()
public subroutine wf_imposta_tree ()
public subroutine wf_init_livelli ()
public subroutine wf_inserisci_offerte (long al_handle, long al_anno_registrazione, long al_num_registrazione)
public function integer wf_inserisci_tipi_offerta (long al_handle)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function integer wf_inserisci_anno (long al_handle)
public function integer wf_inserisci_clienti (long al_handle)
public subroutine wf_init_tree_icons ()
public function treeviewitem wf_new_treeviewitem (boolean ab_children, integer ai_icon)
public function long wf_aggiungi_offerta (long al_handle, integer ai_anno_registrazione, long al_num_registrazione)
public function integer wf_inserisci_depositi (long al_handle)
public function integer wf_inserisci_operatori (long al_handle)
public function integer wf_inserisci_datascadenza (long al_handle)
public subroutine wf_assegna_numerazione (integer ai_row)
public subroutine wf_duplica_offerta (boolean ab_revisiona)
public function long wf_aggiungi_offerta (long al_handle, integer ai_anno_registrazione, long al_num_registrazione, string as_cod_documento, integer ai_anno_documento, string as_numeratore_documento, long al_num_documento, long ai_icon)
public function boolean wf_update_note ()
public function boolean wf_duplica_altre_tabelle (long al_anno_registrazione_orig, long al_num_registrazione_orig, long al_anno_registrazione_dupl, long al_num_registrazione_dupl, ref string as_errore)
public function integer wf_carica_campi_extra ()
public function integer wf_get_info_cliente_contatto (long fl_row, string fs_codice, integer fi_tipo_info, ref string fs_errore)
public subroutine wf_pulisci_campi_cliente_contatto (long fl_row, integer fi_tipo)
public function integer wf_retrieve_ordini_vendita (long fl_anno_reg_offerta, long fl_num_reg_offerta)
public subroutine wf_calcola_documento_dw_corrente ()
end prototypes

event pc_menu_raccoltadati();s_cs_xx.parametri.parametro_d_1 = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"num_registrazione")	
	
window_open(w_report_raccolta_dati, -1)
end event

event pc_menu_corrispondenze();s_cs_xx.parametri.parametro_s_1 = "Off_Ven"
window_open_parm(w_acq_ven_corr, -1, dw_tes_off_ven_det_1)
end event

event pc_menu_note();s_cs_xx.parametri.parametro_ul_1 = dw_tes_off_ven_det_1.getitemnumber(1, "anno_registrazione")
s_cs_xx.parametri.parametro_ul_2 = dw_tes_off_ven_det_1.getitemnumber(1, "num_registrazione")

open(w_acq_ven_ole)
end event

event pc_menu_duplicadocumento();wf_duplica_offerta(false)
end event

event pc_menu_calcola();//string ls_messaggio, ls_cod_cliente
		
//long   ll_num_registrazione, ll_anno_registrazione

//uo_calcola_documento_euro luo_calcola_documento_euro


if dw_tes_off_ven_det_1.rowcount() < 1 then
	g_mb.messagebox("Offerte di vendita", "nessun ordine selezionato: calcolo bloccato")
	return
end if

wf_calcola_documento_dw_corrente()
/*
ll_anno_registrazione = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"num_registrazione")
ls_cod_cliente = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(), "cod_cliente")

luo_calcola_documento_euro = create uo_calcola_documento_euro

// stefanop: 18/01/2012: 
//controllo se si è impostato il cliente o un contatto, nel caso di un contatto non devo controllare
//il fido altrimenti non calcola nulla e da errore
if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then 
	luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
end if
// ----

if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"off_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	rollback;
	destroy luo_calcola_documento_euro
	return
else
	commit;
end if

destroy luo_calcola_documento_euro
*/
dw_tes_off_ven_det_1.change_dw_current()

postevent("pc_retrieve")
end event

event pc_menu_excel();long  ll_i,ll_anno, ll_num_registrazione

ll_i = dw_tes_off_ven_det_1.getrow()
ll_anno  = dw_tes_off_ven_det_1.getitemnumber(ll_i,"anno_registrazione")
ll_num_registrazione  = dw_tes_off_ven_det_1.getitemnumber(ll_i,"num_registrazione")

wf_report_excel(ll_anno, ll_num_registrazione)
end event

event pc_menu_stampa();long   		ll_righe_dett
integer		li_ret
string			ls_cod_cliente, ls_messaggio
datetime		ldt_data_reg


//cb_calcola.triggerevent("clicked")
triggerevent("pc_menu_calcola")

if dw_tes_off_ven_det_1.getrow() > 0 then
	
	ls_cod_cliente = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_cliente")
	ldt_data_reg = dw_tes_off_ven_det_1.getitemdatetime(dw_tes_off_ven_det_1.getrow(),"data_registrazione")
	
	li_ret = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente, ldt_data_reg, "SOV", ls_messaggio)
	if li_ret=2 then
		//blocco
		g_mb.error(ls_messaggio)
		return
	elseif li_ret=1 then
		//solo warning
		g_mb.warning(ls_messaggio)
	end if
	
	s_cs_xx.parametri.parametro_d_1 = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"num_registrazione")	
	
	select count(*)
	into   :ll_righe_dett
	from   det_off_ven
	where  cod_azienda        = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione  = :s_cs_xx.parametri.parametro_d_2;
			 
	if ll_righe_dett = 0 or isnull(ll_righe_dett) then
		g_mb.messagebox("Stampa Offerta Vendita","Nessun dettaglio presente in questa offerta: impossibile stampare!",StopSign!)
		return
	end if
	
	window_open(w_report_off_ven_euro, -1)
	
end if

end event

event pc_menu_dettagli();s_cs_xx.parametri.parametro_w_off_ven = this

//if ib_richieste then
//	// provengo dalla finestre delle richieste
//	s_cs_xx.parametri.parametro_s_10 = "richieste"
//	s_cs_xx.parametri.parametro_d_1 = il_anno_reg_richiesta
//	s_cs_xx.parametri.parametro_d_2 = il_num_reg_richiesta
//else
//	setnull(s_cs_xx.parametri.parametro_s_10)
//	s_cs_xx.parametri.parametro_d_1 = 0
//	s_cs_xx.parametri.parametro_d_2 = 0
//end if

window_open_parm(w_det_off_ven, -1, dw_tes_off_ven_det_1)

end event

event pc_menu_configuratore();/**
 * Evento Configuratore menu
 **/
end event

event pc_menu_revisiona();/**
 * stefanop
 * 13/10/2011
 *
 * Revisiono il documento, quindi creo una copia dell'offerta (copresa la numerazione) e incremento 
 * il numero di revisione
 **/
 
string ls_cod_documento

ls_cod_documento = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(), "cod_documento")

if isnull(ls_cod_documento) or ls_cod_documento = "" then
	
	g_mb.error("Per revisionare l'offerta è necessario assegnare una numerazione.")
	dw_tes_off_ven_det_1.setfocus()
	
	return
	
else
	
	wf_duplica_offerta(true)
	
end if
end event

event pc_proforma();string ls_errore, ls_cod_cliente, ls_cod_contatto
long ll_anno_registrazione, ll_num_registrazione, ll_anno_fattura,ll_num_fattura
uo_generazione_documenti luo_generazione_documenti
integer li_ret


ll_anno_registrazione = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

ls_cod_cliente = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_cliente")
ls_cod_contatto = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_contatto")

//if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
//	g_mb.error("Non è possibile attualmente generare fatture proforma da offerte di vendita con cliente, ma solo con contatto!")
//	return
//end if
//
//if isnull(ls_cod_contatto) or ls_cod_contatto="" then
//	g_mb.error("Non è possibile attualmente generare fatture proforma da offerte di vendita senza specificare il contatto!")
//	return
//end if
if isnull(ls_cod_cliente) and ls_cod_cliente<>"" and isnull(ls_cod_contatto) and ls_cod_contatto<>"" then
	g_mb.error("Per generare fatture proforma da offerte di vendita devi selezionare almeno uno fra il Cliente o il Contatto!")
	return
end if


if g_mb.confirm("APICE","Sei sicuro di voler creare la fattura PROFORMA partendo da questa offerta?") then
	luo_generazione_documenti = create uo_generazione_documenti
	
	li_ret = luo_generazione_documenti.uof_crea_fattura_proforma_offerta(ll_anno_registrazione, ll_num_registrazione, ll_anno_fattura, ll_num_fattura, ls_errore)
	
	if li_ret < 0 then
		rollback;
		g_mb.error("Errore in generazione fattura PROFORMA.~r~n" + ls_errore)
		
	elseif li_ret=1 then
		rollback;
		g_mb.warning("Operazione annullata dall'utente!")
		
	else
		//ritorno 0
		commit;
		g_mb.success("Generata con successo la fattura proforma " + string(ll_anno_fattura) + "/" + string(ll_num_fattura))
		
	end if
	destroy luo_generazione_documenti
end if

return

end event

event ue_auto_search();dw_filtro.setitem(dw_filtro.getrow(), "anno_registrazione",  s_cs_xx.parametri.parametro_d_1)
dw_filtro.setitem(dw_filtro.getrow(), "num_registrazione",  s_cs_xx.parametri.parametro_d_2)

setnull( s_cs_xx.parametri.parametro_d_1)
setnull( s_cs_xx.parametri.parametro_d_2)

wf_imposta_ricerca()

if s_cs_xx.parametri.parametro_s_10 = "richiesteM" then
	iuo_dw_main.triggerevent("pcd_modify")
end if
setnull( s_cs_xx.parametri.parametro_s_10)

end event

event pc_menu_genera_ordine();long							ll_i, ll_numero
integer						li_anno, li_ret
string							ls_cod_cliente, ls_messaggio
s_cs_xx_parametri			lstr_parametri
datetime						ldt_oggi


//per gestione personalizzata Eurocablaggi: generazione singolo ordine di vendita da singola offerta di vendita

if is_SPD <> "S" then return

ll_i = dw_tes_off_ven_det_1.getrow()
if ll_i>0 then
	
	ldt_oggi = datetime(today(), 00:00:00)
	
	li_anno = dw_tes_off_ven_det_1.getitemnumber(ll_i, "anno_registrazione")
	ll_numero = dw_tes_off_ven_det_1.getitemnumber(ll_i, "num_registrazione")
	ls_cod_cliente = dw_tes_off_ven_det_1.getitemstring(ll_i, "cod_cliente")
	
	if li_anno>0 and ll_numero>0 then
		
		if ls_cod_cliente="" or isnull(ls_cod_cliente) then
			g_mb.warning("Impossibile generare l'ordine perchè l'offerta selezionata ("+string(li_anno)+"/"+string(ll_numero)+") è senza cliente!")
			return
		end if
		
		//se l'offerta non ha righe segnala e non andare avanti
		select count(*)
		into :ll_i
		from det_off_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno and
					num_registrazione=:ll_numero;
		
		if sqlca.sqlcode<0 then
			g_mb.error("Errore in controllo righe offerta: "+sqlca.sqlerrtext)
			return
			
		elseif ll_i=0 then 
			g_mb.warning("Impossibile generare l'ordine perchè l'offerta selezionata ("+string(li_anno)+"/"+string(ll_numero)+") sembra non avere righe di dettaglio!")
			return
			
		end if
		
		//*********************************************************************
		//controllo parametri blocco
		li_ret = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente, ldt_oggi, "GOC", ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			return
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		//*********************************************************************
		
		
		if not g_mb.confirm("Procedere alla generazione dell'ordine di vendita dall'offerta n° "+string(li_anno)+"/"+string(ll_numero)+"?") then
			return
		end if

		//apri window di generazione
		lstr_parametri.parametro_ul_1 = li_anno
		lstr_parametri.parametro_ul_2 = ll_numero
		
		openwithparm(w_tes_gen_ord_ven_single, lstr_parametri, w_cs_xx_mdi)
	end if

end if

return
end event

event ue_marginalita();integer											li_anno
long 												ll_numero
s_cs_xx_parametri								lstr_parametri
w_report_margine_ordini_sfuso			lw_window


li_anno = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(), "anno_registrazione")
ll_numero = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(), "num_registrazione")

if li_anno>0 and ll_numero>0 then
else
	return
end if

lstr_parametri.parametro_d_1 = li_anno
lstr_parametri.parametro_d_2 = ll_numero
lstr_parametri.parametro_s_1 = "F"

opensheetwithparm(lw_window, lstr_parametri, PCCA.MDI_Frame, 6,  Original!)


return

return
end event

public subroutine wf_report_excel (long fl_anno_registrazione, long fl_num_registrazione);long ll_colonna, ll_riga, ll_errore, ll_i, ll_prima_riga, ll_ultima_riga, ll_ret, ll_controllo
string ls_path, ls_rag_soc_azienda, ls_foglio, ls_descrizione, ls_cod_prodotto, ls_nota, ls_valuta, ls_formato,&
		ls_col_e, ls_col_f, ls_col_g, ls_col_h, ls_prova, ls_prodotto, ls_scontato
dec{4}  ld_importo,  ld_prezzo_listino, ld_quantita, ld_prova, ld_sconto_1, ld_sconto_2, ld_spese_sicurezza
dec{6} ld_x1,  ld_intermedio
dec ld_valore
dec{2} ld_arrotondamento
boolean lb_controllo
datastore lds_det_off_ven
OLEObject myoleobject

ld_valore = 0.790513

g_mb.messagebox("APICE", "Non cliccare sul foglio di lavoro di excel. ~r~nAttendere il termine.")
myoleobject = CREATE OLEObject
ll_colonna = 1
ll_riga = 1

select rag_soc_1 
into :ls_rag_soc_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

select  stringa
into :ls_path
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
cod_parametro = 'LO1' and
flag_parametro = 'S'; 

select spese_sicurezza, cod_valuta
into :ld_spese_sicurezza, :ls_valuta
from tes_off_ven
where cod_azienda =  :s_cs_xx.cod_azienda and
anno_registrazione = :fl_anno_registrazione and 
num_registrazione = :fl_num_registrazione;

select formato
into :ls_formato
from tab_valute
where cod_azienda =  :s_cs_xx.cod_azienda and
cod_valuta = :ls_valuta;

lds_det_off_ven = create datastore

lds_det_off_ven.dataobject = "d_det_off_ven_excel"

if lds_det_off_ven.settransobject(sqlca) < 0 then
		g_mb.messagebox("APICE","Si è verificato un errore nella creazione del datastore lds_det_off_ven ")
		destroy lds_det_off_ven
	return 
end if

if lds_det_off_ven.retrieve(s_cs_xx.cod_azienda, fl_anno_registrazione, fl_num_registrazione ) < 0 then
	g_mb.messagebox("APICE","Si è verificato un errore nella creazione del datastore lds_det_off_ven ")
	destroy lds_det_off_ven
end if
// calcolo il totale dell'importo materiali da listino prezzo  (somma prezzi per q.tà)
ld_importo = 0
for ll_i = 1 to lds_det_off_ven.rowcount()	
		ld_prova = lds_det_off_ven.getitemnumber(ll_i, "prezzo_um") * lds_det_off_ven.getitemnumber(ll_i, "quantita_um")
		if ld_prova = 0 then 
				ld_prova = lds_det_off_ven.getitemnumber(ll_i, "prezzo_vendita") * lds_det_off_ven.getitemnumber(ll_i, "quan_offerta")
		end if
		ld_importo =ld_importo + ld_prova
next

if ld_spese_sicurezza = 0 or ld_importo = 0 then
	//messagebox("APICE","Si è verificato un errore: le spese complessive sicurezza sono uguale a zero oppure~n il totale di tutte le righe sono uguali a zero. ")
	//destroy lds_det_off_ven
	ld_x1 = 0
else
	ld_spese_sicurezza = ld_spese_sicurezza/ld_importo
	ld_intermedio = 1 + ld_spese_sicurezza 
	ld_x1 = ld_valore * ld_spese_sicurezza / ld_intermedio
end if

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	g_mb.messagebox("APICE","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return
end if

ls_foglio = "foglio1"
myoleobject.Visible = True
myoleobject.Workbooks.Add
//myoleobject.sheets("foglio1").Protect
myoleobject.sheets(ls_foglio).PageSetup.PrintGridlines = True
myoleobject.sheets(ls_foglio).PageSetup.orientation = 2 
/// COS ORIZZONTALE 1 X VERTICALE
myoleobject.sheets(ls_foglio).PageSetup.leftmargin = 20
myoleobject.sheets(ls_foglio).PageSetup.rightmargin = 10
myoleobject.sheets(ls_foglio).PageSetup.topmargin = 20
myoleobject.sheets(ls_foglio).PageSetup.bottommargin = 20

myoleobject.sheets(ls_foglio).PageSetup.FooterMargin = 5

//Worksheets(1).PageSetup
//    .LeftMargin = Application.InchesToPoints(0.5)
//    .RightMargin = Application.InchesToPoints(0.75)
//    .TopMargin = Application.InchesToPoints(1.5)
//    .BottomMargin = Application.InchesToPoints(1)
//    .HeaderMargin = Application.InchesToPoints(0.5)
//    .FooterMargin = Application.InchesToPoints(0.5)

//myoleobject.worksheets(ls_foglio).Shapes.SelectAll = true

//
ls_path = s_cs_xx.volume + ls_path

//lb_controllo = FileExists(ls_path)
ll_controllo = FileLength(ls_path)
if ll_controllo > 0 then
	myoleobject.worksheets(ls_foglio).Shapes.AddPicture(ls_path, True, True, 10, 10, 650, 70)
else
	g_mb.messagebox("APICE","Il logo non è presente, impostare correttamente il parametro 'LO1'!")
end if
///INSERISCO IL LOGO E FORMATTO IL FOGLIO


myoleobject.worksheets(ls_foglio).PageSetup.CenterFooter = "&P di &N"
//
//myoleobject.worksheets(ls_foglio).PageSetup.LeftHeader= "&G"
//myoleobject.worksheets(ls_foglio).PageSetup.CentertHeaderPicture.FileName = 'C:\prova.bmp'
//myoleobject.worksheets(ls_foglio).PageSetup.HeaderPicture.FileName = 'C:\prova.bmp'
//myoleobject.worksheets(ls_foglio).PageSetup.HeaderPicture.Height = 275.25
//myoleobject.worksheets(ls_foglio).PageSetup.HeaderPicture.Width = 463.5

myoleobject.sheets(ls_foglio).columns(ll_colonna + 1).wraptext = true
myoleobject.sheets(ls_foglio).cells(8, ll_colonna).value = "DESCRIZIONE INTERVENTO:"
ll_riga = 10
// formatto le celle di intestazione e el relative colonne

myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "cod."
myoleobject.sheets(ls_foglio).columns(ll_colonna).columnwidth = "15"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "descrizione"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 1).columnwidth = "40"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = "prezzo listino ~n (P.L.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 2).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 2).numberformat= "#.###.##0,00"
//ls_formato non va bene
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = "oneri sicurezza ~n (O.S.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 3).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 3).numberformat= "#.###.##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = "prezzo unitario ~n (P.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 4).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 4).numberformat= "#.###.##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = "1% sconto su ~n (P.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna+ 5).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 5).numberformat= "##0,00%"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 6).value = "2% sconto su ~n (P.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna+ 6).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 6).numberformat= "##0,00%"

myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 7).value = "Q.tà"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 7).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 7).numberformat= "##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "Importo oneri ~n sicurezza"
myoleobject.sheets(ls_foglio).columns(ll_colonna+ 8).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 8).numberformat= "#.###.##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "Importo"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 9).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 9).numberformat= "#.###.##0,00"
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value =""
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value
myoleobject.worksheets(ls_foglio).rows(ll_riga).wraptext = true
myoleobject.worksheets(ls_foglio).rows(ll_riga).HorizontalAlignment = "1"
for ll_i = 0 to 9
	myoleobject.sheets(ls_foglio).columns(ll_colonna + ll_i).font.size = "9"
	myoleobject.worksheets(ls_foglio).cells(ll_riga, ll_colonna + ll_i).borders(3).Weight = "3"
	myoleobject.worksheets(ls_foglio).cells(ll_riga, ll_colonna + ll_i).borders(4).Weight = "3"
next
// inserisco il dettaglio delle righe dell'offerta
ll_riga++
ll_prima_riga = ll_riga
for ll_i = 1 to lds_det_off_ven.rowcount()
	ls_cod_prodotto = lds_det_off_ven.getitemstring(ll_i, "cod_prodotto")
	if isnull(ls_cod_prodotto) then
		ls_cod_prodotto = ""
	end if
	myoleobject.worksheets(ls_foglio).cells(ll_riga, ll_colonna).HorizontalAlignment = "3"
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "'"+ls_cod_prodotto
	ls_descrizione = lds_det_off_ven.getitemstring(ll_i, "des_prodotto")
	if isnull(ls_descrizione) then
		ls_descrizione = ""
	end if
	ls_nota = lds_det_off_ven.getitemstring(ll_i, "nota_dettaglio")
		if isnull(ls_nota) then
		ls_nota = ""
	end if
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = ls_descrizione +""+ls_nota
	ld_prezzo_listino = lds_det_off_ven.getitemnumber(ll_i, "prezzo_um")
	if ld_prezzo_listino = 0 then
		ld_prezzo_listino = lds_det_off_ven.getitemnumber(ll_i, "prezzo_vendita")
	end if
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = ld_prezzo_listino
	if ld_x1 = 0 then
		myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = 0
	else
		ld_arrotondamento = ld_prezzo_listino * ld_x1
		myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = ld_arrotondamento
	end if
	ld_arrotondamento = ld_prezzo_listino - (ld_prezzo_listino * ld_x1)
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = ld_arrotondamento
	ld_sconto_1 = lds_det_off_ven.getitemnumber(ll_i, "sconto_1")
	ld_sconto_2 = lds_det_off_ven.getitemnumber(ll_i, "sconto_2")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = ld_sconto_1/100
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 6).value = ld_sconto_2/100
	ld_quantita = lds_det_off_ven.getitemnumber(ll_i, "quantita_um")
	if ld_quantita = 0 then
		ld_quantita = lds_det_off_ven.getitemnumber(ll_i, "quan_offerta")
	
	end if
	
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 7).value = ld_quantita
	ld_arrotondamento = ld_quantita * ld_prezzo_listino * ld_x1
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = ld_arrotondamento
	ls_col_e = uof_nome_cella(ll_riga, ll_colonna + 4)
	ls_col_f = uof_nome_cella(ll_riga, ll_colonna + 5)
	ls_col_g = uof_nome_cella(ll_riga, ll_colonna + 6)
	ls_col_h = uof_nome_cella(ll_riga, ll_colonna + 7)
	ls_prova = "=("+ls_col_e+"-(("+ls_col_e+"*"+ls_col_f+"))-(("+ls_col_e+"-("+ls_col_e+"*"+ls_col_f+"))*"+ls_col_g+"))*"+ls_col_h
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "=("+ls_col_e+"-(("+ls_col_e+"*"+ls_col_f+"))-(("+ls_col_e+"-("+ls_col_e+"*"+ls_col_f+"))*"+ls_col_g+"))*"+ls_col_h
	ll_riga++
next
for ll_i = 0 to 9	
	myoleobject.worksheets(ls_foglio).cells(ll_riga - 1, ll_colonna + ll_i).borders(4).Weight = "3"
next
ll_ultima_riga = ll_riga - 1
ll_riga++

// inserisco i totali finali
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "1)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "SOMMANO MATERIALI"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "=somma("+uof_nome_cella(ll_prima_riga, ll_colonna + 9)+":"+uof_nome_cella(ll_ultima_riga, ll_colonna + 9)+")"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "2)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TRASPORTI E NOLI"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "0"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "3)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERE DI ASSISTENZA MURARIA 0%"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "0"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "4)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "ONERI PER LA SICUREZZA"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "=somma("+uof_nome_cella(ll_prima_riga, ll_colonna + 8)+":"+uof_nome_cella(ll_ultima_riga, ll_colonna + 8)+")"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).borders(4).Weight = "3"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "5)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "IMPORTO COMPLESSIVO MATERIALI"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "=somma("+uof_nome_cella(ll_ultima_riga + 2, ll_colonna + 8)+":"+uof_nome_cella(ll_ultima_riga + 5, ll_colonna + 8)+")"
ll_riga++
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "MANODOPERA"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = "Ore"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = "€/ora"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = "totale"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = "sconto"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "totale scontato"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.1)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TECNICO DI CONCETTO LAUREATO"
ll_i = ll_riga
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.2)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TECNICO DI CONCETTO DIPLOMATO"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.3)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERAIO SPECIALIZZATO V° livello"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.4)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERAIO SPECIALIZZATO IV° livello"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.5)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERAIO COMUNE"

for ll_i = ll_i to ll_riga 
	
	ls_prodotto = "="+uof_nome_cella(ll_i , ll_colonna + 2)+"*"+uof_nome_cella(ll_i , ll_colonna + 3)
	ls_scontato = "="+uof_nome_cella(ll_i , ll_colonna + 4)+"-("+uof_nome_cella(ll_i , ll_colonna + 4)+"*"+uof_nome_cella(ll_i , ll_colonna + 5)+")"
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 2).value = "0"
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 3).value = "0,00"
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 4).value = ls_prodotto
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 5).value = 13.53/100
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 8).value = ls_scontato

next

myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).borders(4).Weight = "3"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TOTALE IMPORTO MANODOPERA"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "=somma("+uof_nome_cella(ll_riga - 5, ll_colonna + 8)+":"+uof_nome_cella(ll_riga -1, ll_colonna + 8)+")"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).borders(4).Weight = "3"
ll_riga++
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "IMPORTO TOTALE"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "="+uof_nome_cella(ll_riga - 2 , ll_colonna + 9)+"+"+uof_nome_cella(ll_riga - 10 , ll_colonna + 9)
myoleobject.sheets(ls_foglio).rows(ll_riga).font.size = "12"
myoleobject.sheets(ls_foglio).rows(ll_riga).Font.Bold = True
ll_riga++

ll_ret = uof_salva(fl_anno_registrazione, fl_num_registrazione, ls_foglio, ref myoleobject)
if ll_ret = -1 then
	return
end if
g_mb.messagebox("APICE","Report allegato alla nota dell'offerta di vendita!")
destroy lds_det_off_ven
return


end subroutine

public function string uof_nome_cella (long fi_riga, long fi_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fi_colonna > 26 then
	ll_resto = mod(fi_colonna, 26)
	ll_i = long(fi_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fi_colonna = ll_resto 
end if

if fi_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fi_colonna] + string(fi_riga)
end if	

return ls_char
end function

public function integer uof_salva (long fl_anno_registrazione, long fl_num_registrazione, string fs_foglio, ref oleobject myoleobject);string ls_path, ls_cod_nota, ls_db, ls_str
blob lb_file
long ll_progressivo,  ll_prog_mimetype, li_risposta, ll_ret, ll_i, ll_controllo
boolean lb_prova

//transaction sqlcb

//fare il salvataggio 
select stringa
into   :ls_path
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_parametro = 'TDD';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Attenzione: inserire il parametro aziendale TDD! " + sqlca.sqlerrtext)	
	myoleobject.quit
	return -1
end if

setnull(ls_str)
ls_str = left(ls_path, 1)
if ls_str <> "\" then ls_path = "\" + ls_path

setnull(ls_str)
ls_str = right(ls_path, 1)
if ls_str <> "\" then ls_path = ls_path + "\"

ls_path = s_cs_xx.volume + ls_path + "preventivo"+string(now(),"hh_mm_ss")+".xls"

// stefanop: ticket 2011/93
// controllo se esiste la cartella altrimenti la creo
if not DirectoryExists(s_cs_xx.volume + ls_path) then
	CreateDirectory(s_cs_xx.volume + ls_path)
end if
// ---

myoleobject.sheets(fs_foglio).SaveAs(ls_path)
myoleobject.quit

lb_file = f_carica_blob(ls_path)
ll_ret = len(lb_file)

// se ll_ret <= 0 allora non è riuscito ad aprire il file perchè excel non è stato ancora chiuso
if ll_ret <= 0 then
	ll_controllo = 0
	//provo per 20 volte ad aprire il file poi lancio errore
	for ll_i = 1 to 40
		lb_file = f_carica_blob(ls_path)
		ll_ret = len(lb_file)
		if ll_ret > 0 then
			ll_controllo = 1
			exit
		end if
	next
	if ll_controllo = 0 then
		g_mb.messagebox( "APICE", "Errore durante l'inserimento del file nel database, ritentare l'elaborazione. "  + sqlca.sqlerrtext)
		rollback;
		FileDelete(ls_path)
		return -1
	end if
end if

FileDelete(ls_path)

ls_db = f_db()

//	//*** so per certo che è un xls, quindi lo cerco
select prog_mimetype
into   :ll_prog_mimetype
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'XLS';

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "APICE", "Errore durante la ricerca del mimetype: " + sqlca.sqlerrtext)
	rollback;
	return -1
elseif sqlca.sqlcode = 100 then  // ** faccio insert
	
	select max(prog_mimetype)
	into   :ll_prog_mimetype
	from   tab_mimetype
	where  cod_azienda = :s_cs_xx.cod_azienda;
	
	if isnull(ll_prog_mimetype) then ll_prog_mimetype = 0
	ll_prog_mimetype = ll_prog_mimetype + 1
	
	INSERT INTO tab_mimetype ( cod_azienda,   
										prog_mimetype,   
										estensione,   
										applicazione,   
										mimetype )  
							VALUES ( :s_cs_xx.cod_azienda,   
										:ll_prog_mimetype,   
										'XLS',   
										'Microsoft excel',   
										'application/msexcel' )  ;		
end if


select cod_nota
into   :ls_cod_nota
from   tes_off_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and 
		 num_registrazione = :fl_num_registrazione and 
		 cod_nota = 'P01' ;

if sqlca.sqlcode = 0 then
	ll_ret = g_mb.messagebox("APICE", "Attenzione il preventivo presente nelle note sarà sostituito da quello appena elaborato.~n Desideri sovrascriverlo? ", Exclamation!, YesNo!, 2)
	//ll_ret = 1 //si
	if ll_ret = 2 then //no
		rollback;
		return -1
	end if
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "APICE", "Errore durante la ricerca del documento: " + sqlca.sqlerrtext)
	rollback;
	return -1
elseif sqlca.sqlcode = 100 then  // ** faccio insert
	
	INSERT INTO tes_off_ven_note ( cod_azienda,   
											anno_registrazione,   
											num_registrazione,   
											cod_nota,
											des_nota, 
											prog_mimetype)  
								VALUES ( :s_cs_xx.cod_azienda,   
											:fl_anno_registrazione,   
											:fl_num_registrazione,   
											'P01'  ,
											'Foglio excel del preventivo',
											:ll_prog_mimetype)  ;		


	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante l'inserimento del documento: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

end if

// stefanop: ticket 2011/93: tolgo il codice per MSSQL che da problemi
//if ls_db = "MSSQL" then
//
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	updateblob tes_off_ven_note
//	set        note_esterne = :lb_file
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//				  anno_registrazione = :fl_anno_registrazione and 
//				  num_registrazione = :fl_num_registrazione and 
//				  cod_nota = 'P01' 
//	using      sqlcb;
//	
//	if sqlca.sqlcode <> 0 then
//		g_mb.messagebox( "APICE", "Errore durante l'inserimento del blod del documento: " + sqlca.sqlerrtext)
//		rollback;
//		destroy sqlcb;
//		return -1
//	end if
//destroy sqlcb;
//
//else

	updateblob tes_off_ven_note
	set        note_esterne = :lb_file
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :fl_anno_registrazione and 
				  num_registrazione = :fl_num_registrazione and 
				  cod_nota = 'P01' ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante l'inserimento del blod del documento: " + sqlca.sqlerrtext)
		rollback;
//		destroy sqlcb;
		return -1
	end if
				  
//end if


commit;



return 1


end function

public subroutine wf_imposta_ricerca ();string			ls_cod_cliente, ls_cod_contatto, ls_cod_tipo_off_ven, ls_cod_valuta, ls_flag_evasione, ls_num_documento, ls_tes, ls_cod_deposito, &
					ls_cod_agente_1, ls_cod_agente_2, ls_cod_documento, ls_numeratore_documento, ls_cod_prodotto, ls_det,ls_flag_offerte_web, &
					ls_cod_tipo_off_web
long				ll_anno_registrazione, ll_num_registrazione, ll_num_revisione, ll_anno_documento, ll_num_documento
date				ldt_data


dw_filtro.accepttext()

ls_tes = "tes_off_ven."
ls_det = "det_off_ven."

is_sql_filtro = ""

ll_anno_registrazione = dw_filtro.getitemnumber(1,"anno_registrazione")
ll_num_registrazione = dw_filtro.getitemnumber(1,"num_registrazione")
ll_num_revisione = dw_filtro.getitemnumber(1,"num_revisione")
ls_cod_cliente = dw_filtro.getitemstring(1,"cod_cliente")
ls_cod_contatto = dw_filtro.getitemstring(1,"cod_contatto")
ls_cod_tipo_off_ven = dw_filtro.getitemstring(1,"cod_tipo_off_ven")
ls_flag_evasione = dw_filtro.getitemstring(1,"flag_evasione")
ls_num_documento = dw_filtro.getitemstring(1,"num_documento")
ls_cod_deposito = dw_filtro.getitemstring(1,"cod_deposito")
ls_cod_agente_1 = dw_filtro.getitemstring(1,"cod_agente_1")
ls_cod_agente_2 = dw_filtro.getitemstring(1,"cod_agente_2")
ls_cod_documento = dw_filtro.getitemstring(1,"cod_documento")
ll_anno_documento = dw_filtro.getitemnumber(1,"anno_documento")
ls_numeratore_documento = dw_filtro.getitemstring(1,"numeratore_documento")
ll_num_documento= dw_filtro.getitemnumber(1,"num_documento")
ls_cod_prodotto = dw_filtro.getitemstring(1,"cod_prodotto")
ls_flag_offerte_web = dw_filtro.getitemstring(1,"flag_offerte_web")

// creo filtro
if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 then
	is_sql_filtro += " and "+ls_tes+"anno_registrazione = " + string(ll_anno_registrazione) + " "
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	is_sql_filtro += " and "+ls_tes+"num_registrazione = " + string(ll_num_registrazione) + " "
end if
if not isnull(ll_num_revisione) and ll_num_revisione>0 then
	is_sql_filtro += " and "+ls_tes+"num_revisione = " + string(ll_num_revisione) + " "
end if
if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_cliente = '" + ls_cod_cliente + "' "
end if
if not isnull(ls_cod_contatto) and ls_cod_contatto<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_contatto = '" + ls_cod_contatto + "' "
end if

choose case ls_flag_offerte_web
	case "S"	
		// visualizza SOLO le offerte web
		is_sql_filtro += " and " + ls_tes + "flag_ipertech = 'S' "
	
	case "N"
		// non visualizzare le offerte web
		if len(ls_cod_tipo_off_ven) > 0 then
			is_sql_filtro += " and "+ls_tes+"cod_tipo_off_ven ='" + ls_cod_tipo_off_ven + "' "
		end if
		
		is_sql_filtro += " and " + ls_tes + "flag_ipertech = 'N' "
end choose



if not isnull(ls_flag_evasione) and ls_flag_evasione<>"" and ls_flag_evasione <> "T" then
	if ls_flag_evasione = "AP" then // Aperte/Parziali
		is_sql_filtro += " and (" + ls_tes + "flag_evasione='A' or " + ls_tes + "flag_evasione='P') "
	else
		is_sql_filtro += " and "+ls_tes+"flag_evasione = '" + ls_flag_evasione + "' "
	end if
end if
if not isnull(ls_num_documento) and ls_num_documento<>"" then
	is_sql_filtro += " and "+ls_tes+"num_documento = '" + ls_num_documento + "' "
end if
if not isnull(ls_cod_deposito) and ls_cod_deposito<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_deposito = '" + ls_cod_deposito + "' "
end if
if not isnull(ls_cod_agente_1) and ls_cod_agente_1<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_agente_1 = '" + ls_cod_agente_1 + "' "
end if
if not isnull(ls_cod_agente_2) and ls_cod_agente_2<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_agente_2 = '" + ls_cod_agente_2 + "' "
end if

ldt_data = dw_filtro.getitemdate(1,"data_consegna")
if not isnull(ldt_data) then
	is_sql_filtro += " AND "+ls_tes+"data_consegna = '" + string(ldt_data, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ldt_data = dw_filtro.getitemdate(1,"data_registrazione")
if not isnull(ldt_data) then
	is_sql_filtro += " AND "+ls_tes+"data_registrazione = '" + string(ldt_data, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ldt_data = dw_filtro.getitemdate(1,"data_scadenza")
if not isnull(ldt_data) then
	is_sql_filtro += " AND "+ls_tes+"data_scadenza = '" + string(ldt_data, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
	is_sql_filtro += " and "+ls_det+"cod_prodotto = '" + ls_cod_prodotto + "' "
end if

// ----
 
// Documento
if not isnull(ls_cod_documento) and ls_cod_documento<>"" then is_sql_filtro += " and "+ls_tes+"cod_documento = '" + ls_cod_documento + "' "
if not isnull(ll_anno_documento) and ll_anno_documento>1900 then is_sql_filtro += " and "+ls_tes+"anno_documento =" + string(ll_anno_documento)
if not isnull(ls_numeratore_documento) and ls_numeratore_documento<>"" then is_sql_filtro += " and "+ls_tes+"numeratore_documento = '" + ls_numeratore_documento + "' "
if not isnull(ll_num_documento) and ll_num_documento > 0 then is_sql_filtro += " and "+ls_tes+"num_documento=" + string(ll_num_documento)
// ----


wf_cancella_tree()

wf_imposta_tree()

tv_1.setfocus()

dw_folder_search.fu_SelectTab(2)
dw_folder.fu_SelectTab(1)
end subroutine

public subroutine wf_cancella_tree ();ib_clear_tree = true
tv_1.deleteitem(0)
ib_clear_tree = false
end subroutine

public subroutine wf_imposta_tree ();long ll_anno_registrazione, ll_num_registrazione

setpointer(HourGlass!)

tv_1.setredraw(false)

ll_anno_registrazione = dw_filtro.getitemnumber(1,"anno_registrazione")
ll_num_registrazione = dw_filtro.getitemnumber(1,"num_registrazione")

// mostro la singola offerta
if ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) and ll_num_registrazione > 0 and not isnull(ll_num_registrazione) then
	wf_inserisci_offerte(0, ll_anno_registrazione, ll_num_registrazione )
	tv_1.setredraw(true)
	return
end if

il_livello_corrente = 1

// controlla cosa c'è al livello successivo
choose case dw_filtro.getitemstring(1, "livello_" + string(il_livello_corrente))
	case "T"
		//caricamento tipo offerte
		wf_inserisci_tipi_offerta(0)
		
	case "A"
		//caricamento anni
		wf_inserisci_anno(0)

	case "C"
		//caricamento clienti
		wf_inserisci_clienti(0)
		
	case "D"
		wf_inserisci_depositi(0)
		
	case "O"
		wf_inserisci_operatori(0)
		
	case "S"
		wf_inserisci_datascadenza(0)
		
	case "N" 
		wf_inserisci_offerte(0, 0,0)
end choose

tv_1.setredraw(true)

setpointer(arrow!)
return
end subroutine

public subroutine wf_init_livelli ();/**
 * stefanop
 * 22/09/2011
 *
 * Inizializzo le drop dei livelli
 **/
 
 

end subroutine

public subroutine wf_inserisci_offerte (long al_handle, long al_anno_registrazione, long al_num_registrazione);/**
 * stefnaop
 * inserisco la singola offerta o tutte le offerte se anno e numero sono a 0
 **/
 
string ls_sql, ls_flag_evasione, ls_cod_documento, ls_numeratore_documento, ls_sql_livello
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_icon, ll_anno_documento, ll_num_documento, ll_count

ls_sql = "select distinct "+&
				"tes_off_ven.anno_registrazione,"+&
				"tes_off_ven.num_registrazione,"+&
				"tes_off_ven.flag_evasione,"+&
				"tes_off_ven.cod_documento,"+&
				"tes_off_ven.anno_documento," + &
				"tes_off_ven.numeratore_documento,"+&
				"tes_off_ven.num_documento "+&
			"from tes_off_ven "
			
ls_sql_livello = ""
wf_leggi_parent(al_handle, ls_sql_livello)

if pos(is_sql_filtro, "det_off_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_off_ven.cod_prodotto")>0 then
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql +=ls_sql_livello
end if

//imposto l'ordinamento per anno e numero
ls_sql += " order by tes_off_ven.anno_registrazione, tes_off_ven.num_registrazione"

if not f_crea_datastore(ids_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_offerte)", sqlca)
	return
end if

ll_count =  ids_store.retrieve()

// stefanop 02/07/2012: aggiungo controllo per il numero massimo di risultati
if ll_count > ll_max_offerte  then
	if not g_mb.confirm("Sono state recuperate " + string(ll_count) + " superando il limite consigliato.~r~nProcedere con la visualizzazione di tutte le offerte?") then
		// Visualizzo solo le richieste nel limite
		ll_count = ll_max_offerte
	end if
end if

for ll_i = 1 to ll_count
	
	
	ll_anno_registrazione = ids_store.getitemnumber(ll_i, 1)
	ll_num_registrazione = ids_store.getitemnumber(ll_i, 2)
	ls_flag_evasione = ids_store.getitemstring(ll_i, 3)
	ls_cod_documento = ids_store.getitemstring(ll_i, 4)
	ll_anno_documento = ids_store.getitemnumber(ll_i, 5)
	ls_numeratore_documento = ids_store.getitemstring(ll_i, 6)
	ll_num_documento = ids_store.getitemnumber(ll_i, 7)
	ll_icon = TIPO_OFF_ICON
	
	if isnull(ls_cod_documento) or ls_cod_documento = "" then
			ll_icon = OFF_SENZA_NUM_ICO
	elseif not isnull(ls_cod_documento) and ls_flag_evasione = "A" then
		ll_icon = TIPO_OFF_ICON
	elseif ls_flag_evasione = "P" then
		ll_icon = OFF_PARZIALE_ICON
	elseif ls_flag_evasione = "A" then
		ll_icon = OFF_APERTA_ICON
	elseif ls_flag_evasione = "E" then
		ll_icon = OFF_EVASA_ICON
	end if
		
	wf_aggiungi_offerta(al_handle, ll_anno_registrazione, ll_num_registrazione, ls_cod_documento, ll_anno_documento, ls_numeratore_documento, ll_num_documento, ll_icon)
	
	
next

setnull(ids_store)


end subroutine

public function integer wf_inserisci_tipi_offerta (long al_handle);/**
 * stefanop
 * 22/09/2011
 *
 * Carico i tipi offerta
 **/
 
string ls_sql, ls_cod_tipo_off, ls_des_tipo_off, ls_sql_livello
long ll_rows, ll_i
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT tes_off_ven.cod_tipo_off_ven, tab_tipi_off_ven.des_tipo_off_ven FROM tes_off_ven &
	LEFT OUTER JOIN tab_tipi_off_ven ON &
		tab_tipi_off_ven.cod_azienda = tes_off_ven.cod_azienda AND &
		tab_tipi_off_ven.cod_tipo_off_ven = tes_off_ven.cod_tipo_off_ven "
		
	//WHERE tes_off_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

ls_sql_livello = ""
wf_leggi_parent(al_handle, ls_sql_livello)

if pos(is_sql_filtro, "det_off_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_off_ven.cod_prodotto")>0 then
	ls_sql += is_join_det_ord_ven
end if
			
ls_sql += "where tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_off_ven.cod_tipo_off_ven asc "


if not f_crea_datastore(ids_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_tipi_offerta)", sqlca)
	return -1
end if

ll_rows = ids_store.retrieve()

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_treeviewitem(true, TIPO_OFF_ICON)
	ss_record lss_record
	
	ls_cod_tipo_off = ids_store.getitemstring(ll_i, 1)
	ls_des_tipo_off = ids_store.getitemstring(ll_i, 2)
	
	lss_record.tipo_livello = "T"
	lss_record.codice = ls_cod_tipo_off
	lss_record.livello = il_livello_corrente
	lss_record.anno_registrazione = 0
	lss_record.num_registrazione = 0
	
	if isnull(ls_cod_tipo_off) or isnull(ls_des_tipo_off) then
	ltvi_item.label = "codice tipo offerta mancante"
	elseif isnull(ls_des_tipo_off) then
	ltvi_item.label = ls_cod_tipo_off
	else 
	ltvi_item.label = ls_cod_tipo_off + " - " + ls_des_tipo_off
	end if
	
	ltvi_item.data = lss_record
	
	tv_1.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
ss_record lss_record

if al_handle = 0 then return 0

do
	
	tv_1.getitem(al_handle, ltv_item)
		
	lss_record = ltv_item.data
	
	choose case lss_record.tipo_livello		
			
		case "T"
			if isnull(lss_record.codice) or lss_record.codice = "" then
				as_sql += " AND tes_off_ven.cod_tipo_off_ven is null "
			else
				as_sql += " AND tes_off_ven.cod_tipo_off_ven = '" + lss_record.codice + "' "
			end if
			
		case "A"
			if isnull(lss_record.codice) or lss_record.codice = "" then
				as_sql += " AND tes_off_ven.anno_registrazione is null "
			else
				as_sql += " AND tes_off_ven.anno_registrazione = " + lss_record.codice + " "
			end if
			
		case "C"
			if isnull(lss_record.codice) or lss_record.codice = "" then
				as_sql += " AND tes_off_ven.cod_cliente is null "
			else
				as_sql += " AND tes_off_ven.cod_cliente = '" + lss_record.codice + "' "
			end if
			
		case "D"
			if isnull(lss_record.codice) or lss_record.codice = "" then
				as_sql += " AND tes_off_ven.cod_deposito is null "
			else
				as_sql += " AND tes_off_ven.cod_deposito = '" + lss_record.codice + "' "
			end if
			
		case "O"
			if isnull(lss_record.codice) or lss_record.codice = "" then
				as_sql += " AND tes_off_ven.cod_operatore is null "
			else
				as_sql += " AND tes_off_ven.cod_operatore = '" + lss_record.codice + "' "
			end if
			
		case "S"
			if isnull(lss_record.codice) or lss_record.codice = "" then
				as_sql += " AND tes_off_ven.data_scadenza is null "
			else
				as_sql += " AND tes_off_ven.data_scadenza = '" + lss_record.codice + " 00:00:00' "
			end if
			
	end choose
	
	al_handle = tv_1.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function integer wf_inserisci_anno (long al_handle);/**
 * stefanop
 * 22/09/2011
 *
 * Carico il livello per anno
 **/
 
string ls_sql, ls_sql_livello
long ll_rows, ll_i, ll_anno
treeviewitem ltvi_item

ls_sql = "select distinct tes_off_ven.anno_registrazione from tes_off_ven "

//"where tes_off_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

ls_sql_livello = ""
//wf_leggi_parent(al_handle, ls_sql)
wf_leggi_parent(al_handle, ls_sql_livello)


if pos(is_sql_filtro, "det_off_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_off_ven.cod_prodotto")>0 then
			ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_off_ven.anno_registrazione desc "


if not f_crea_datastore(ids_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_anno)", sqlca)
	return -1
end if

ll_rows = ids_store.retrieve()

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_treeviewitem(true, ANNO_ICON)
	ss_record lss_record
	
	ll_anno = ids_store.getitemnumber(ll_i, 1)
	
	lss_record.tipo_livello = "A"
	lss_record.codice = string(ll_anno)
	lss_record.livello = il_livello_corrente
	lss_record.anno_registrazione = 0
	lss_record.num_registrazione = 0
	 
	if isnull(ll_anno) then
		ltvi_item.label = "anno mancante"
	else 
		ltvi_item.label = string(ll_anno)
	end if
	
	ltvi_item.data = lss_record
	
	tv_1.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_inserisci_clienti (long al_handle);/**
 * stefanop
 * 22/09/2011
 *
 * Carico i tipi offerta
 **/
 
string ls_sql, ls_cod_cliente, ls_rag_soc_1, ls_sql_livello
long ll_rows, ll_i
treeviewitem ltvi_item

ls_sql = "select distinct anag_clienti.cod_cliente, anag_clienti.rag_soc_1 "+&
			"from tes_off_ven "+&
			"left outer join anag_clienti on anag_clienti.cod_azienda = tes_off_ven.cod_azienda and "+&
								"anag_clienti.cod_cliente = tes_off_ven.cod_cliente "
	
	//WHERE tes_off_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

ls_sql_livello = ""
wf_leggi_parent(al_handle, ls_sql_livello)

if pos(is_sql_filtro, "det_off_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_off_ven.cod_prodotto")>0 then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + " order by anag_clienti.rag_soc_1 asc "

if not f_crea_datastore(ids_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_clienti)", sqlca)
	return -1
end if

ll_rows = ids_store.retrieve()

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_treeviewitem(true, CLIENTE_ICON)
	ss_record lss_record
	
	ls_cod_cliente = ids_store.getitemstring(ll_i, 1)
	ls_rag_soc_1 = ids_store.getitemstring(ll_i, 2)
	
	lss_record.tipo_livello = "C"
	lss_record.codice = ls_cod_cliente
	lss_record.livello = il_livello_corrente
	lss_record.anno_registrazione = 0
	lss_record.num_registrazione = 0
	
	if isnull(ls_cod_cliente) or isnull(ls_rag_soc_1) then
		ltvi_item.label = "codice cliente mancante"
	elseif isnull(ls_rag_soc_1) then
		ltvi_item.label = ls_cod_cliente
	else 
		ltvi_item.label = ls_cod_cliente + " - " + ls_rag_soc_1
	end if
	
	ltvi_item.data = lss_record
	
	tv_1.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public subroutine wf_init_tree_icons ();/**
 * stefanop
 * 22/09/2011
 *
 * Carico icone albero
 **/
 
tv_1.deletepictures()

//1 tipo offerta
TIPO_OFF_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\tipo_documento.png")

// senza numero di assegnazione
OFF_SENZA_NUM_ICO = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\tag_red.png")

// offerta evasa
OFF_EVASA_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\tag_green.png")

// offerta aperta
OFF_APERTA_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\tag_yellow.png")

// offerta parziale
OFF_PARZIALE_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\tag_orange.png")

//2 cliente
CLIENTE_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\cliente.png")

//3 anno
ANNO_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\anno.png")

//4 offerta
OFFERTA_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\offerta.png")

// Deposito
DEPOSITO_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\deposito.png")

// Operatore
OPERATORE_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\operatore.png")

// Data Sacdenza
DATASCAD_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\data_1.png")
end subroutine

public function treeviewitem wf_new_treeviewitem (boolean ab_children, integer ai_icon);treeviewitem ltvi_campo

ltvi_campo.expanded = false
ltvi_campo.selected = false
ltvi_campo.children = ab_children	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ltvi_campo.pictureindex = ai_icon		
ltvi_campo.selectedpictureindex = ai_icon		
//ltvi_campo.overlaypictureindex = 4

return ltvi_campo
end function

public function long wf_aggiungi_offerta (long al_handle, integer ai_anno_registrazione, long al_num_registrazione);/**
 * stefanop
 * 23/09/2011
 *
 * Codice centralizzato per inserie un'offerta all'albero in quanto richiesta in più parti
 **/

return wf_aggiungi_offerta(al_handle, ai_anno_registrazione, al_num_registrazione, "", 0, "",0, OFFERTA_ICON)
end function

public function integer wf_inserisci_depositi (long al_handle);/**
 * stefanop
 * 22/09/2011
 *
 * Carico i depositi
 **/
 
string ls_sql, ls_cod_deposito, ls_des_deposito, ls_sql_livello
long ll_rows, ll_i
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT tes_off_ven.cod_deposito, anag_depositi.des_deposito FROM tes_off_ven &
			LEFT OUTER JOIN anag_depositi ON &
					anag_depositi.cod_azienda = tes_off_ven.cod_azienda AND &
					anag_depositi.cod_deposito = tes_off_ven.cod_deposito "
		
		
	//WHERE tes_off_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

ls_sql_livello = ""
wf_leggi_parent(al_handle, ls_sql_livello)

if pos(is_sql_filtro, "det_off_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_off_ven.cod_prodotto")>0 then
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + " order by tes_off_ven.cod_deposito asc "

if not f_crea_datastore(ids_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_depositi)", sqlca)
	return -1
end if

ll_rows = ids_store.retrieve()

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_treeviewitem(true, DEPOSITO_ICON)
	ss_record lss_record
	
	ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	lss_record.tipo_livello = "D"
	lss_record.codice = ls_cod_deposito
	lss_record.livello = il_livello_corrente
	lss_record.anno_registrazione = 0
	lss_record.num_registrazione = 0
	
	if isnull(ls_cod_deposito) or isnull(ls_des_deposito) then
		ltvi_item.label = "codice deposito mancante"
	elseif isnull(ls_des_deposito) then
		ltvi_item.label = ls_cod_deposito
	else 
		ltvi_item.label = ls_cod_deposito + " - " + ls_des_deposito
	end if
	
	ltvi_item.data = lss_record
	
	tv_1.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_inserisci_operatori (long al_handle);/**
 * stefanop
 * 22/09/2011
 *
 * Carico i depositi
 **/
 
string ls_sql, ls_cod_operatore, ls_des_operatore, ls_sql_livello
long ll_rows, ll_i
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT tes_off_ven.cod_operatore, tab_operatori.des_operatore FROM tes_off_ven &
	LEFT OUTER JOIN tab_operatori ON &
		tab_operatori.cod_azienda = tes_off_ven.cod_azienda AND &
		tab_operatori.cod_operatore = tes_off_ven.cod_operatore "
		
	//WHERE tes_off_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

ls_sql_livello = ""
wf_leggi_parent(al_handle, ls_sql_livello)

if pos(is_sql_filtro, "det_off_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_off_ven.cod_prodotto")>0 then
	ls_sql += is_join_det_ord_ven
end if
			
ls_sql += "where tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_off_ven.cod_operatore asc "


if not f_crea_datastore(ids_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_operatori)", sqlca)
	return -1
end if

ll_rows = ids_store.retrieve()

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_treeviewitem(true, OPERATORE_ICON)
	ss_record lss_record
	
	ls_cod_operatore = ids_store.getitemstring(ll_i, 1)
	ls_des_operatore = ids_store.getitemstring(ll_i, 2)
	
	lss_record.tipo_livello = "O"
	lss_record.codice = ls_cod_operatore
	lss_record.livello = il_livello_corrente
	lss_record.anno_registrazione = 0
	lss_record.num_registrazione = 0
	
	if isnull(ls_cod_operatore) or isnull(ls_des_operatore) then
		ltvi_item.label = "codice operatore mancante"
	elseif isnull(ls_des_operatore) then
		ltvi_item.label = ls_cod_operatore
	else 
		ltvi_item.label = ls_cod_operatore + " - " + ls_des_operatore
	end if
	
	ltvi_item.data = lss_record
	
	tv_1.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_inserisci_datascadenza (long al_handle);/**
 * stefanop
 * 22/09/2011
 *
 * Carico i depositi
 **/
 
string ls_sql, ls_sql_livello
long ll_rows, ll_i
datetime ldt_data_scadenza
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT tes_off_ven.data_scadenza FROM tes_off_ven "


	//WHERE tes_off_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

ls_sql_livello = ""
wf_leggi_parent(al_handle, ls_sql_livello)

if pos(is_sql_filtro, "det_off_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_off_ven.cod_prodotto")>0 then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_off_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + " order by tes_off_ven.data_scadenza desc "


if not f_crea_datastore(ids_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_datascadenza)", sqlca)
	return -1
end if

ll_rows = ids_store.retrieve()

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_treeviewitem(true, DATASCAD_ICON)
	ss_record lss_record
	
	ldt_data_scadenza = ids_store.getitemdatetime(ll_i, 1)
	
	lss_record.tipo_livello = "S"
	lss_record.codice = string(ldt_data_scadenza, s_cs_xx.db_funzioni.formato_data)
	lss_record.livello = il_livello_corrente
	lss_record.anno_registrazione = 0
	lss_record.num_registrazione = 0
	
	if isnull(ldt_data_scadenza) then
		ltvi_item.label = "data scadenza mancante"
	else
		ltvi_item.label = string(ldt_data_scadenza, "dd/mm/yyyy")
	end if

	ltvi_item.data = lss_record
	
	tv_1.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public subroutine wf_assegna_numerazione (integer ai_row);string ls_cod_tipo_off_ven, ls_cod_documento, ls_numeratore_documento,ls_test
long ll_anno_registrazione, ll_num_registrazione, ll_anno_esc, ll_num_documento
datetime ldt_data

ldt_data = datetime(today(), 00:00:00)


ll_anno_registrazione = dw_tes_off_ven_det_1.getitemnumber(ai_row, "anno_registrazione")
ll_num_registrazione = dw_tes_off_ven_det_1.getitemnumber(ai_row, "num_registrazione")
ls_cod_tipo_off_ven = dw_tes_off_ven_det_1.getitemstring(ai_row, "cod_tipo_off_ven")
ls_cod_documento = dw_tes_off_ven_det_1.getitemstring(ai_row, "cod_documento")

if isnull(ll_anno_registrazione) or ll_anno_registrazione < 1900 or isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
	return
end if

if isnull(ls_cod_tipo_off_ven) or ls_cod_tipo_off_ven = "" then
	g_mb.show("Impostare un Tipo Offerta prima di procedere con numerazione.")
	return
end if

if not isnull(ls_cod_documento) and ls_cod_documento <> "" then
	g_mb.show("Il documento presenta già una numerazione valida!")
	return
end if

select cod_documento, numeratore_documento
into :ls_cod_documento, :ls_numeratore_documento
from tab_tipi_off_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_off_ven = :ls_cod_tipo_off_ven;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il recupero del codice documento.", sqlca)
	return
elseif sqlca.sqlcode = 100 then
	g_mb.error("Il Tipo Offerta selezionato non è presente all'interno del database.")
	return
elseif isnull(ls_cod_documento) or ls_cod_documento = "" or isnull(ls_numeratore_documento) or ls_numeratore_documento = "" then
	g_mb.error("Il Codice Documento o il Numerator Documento non sono impostati correttamente nel dettaglio offerta selezionato")
	return
end if

ll_anno_esc = f_anno_esercizio()

// calcolo il progressivo
select max(num_documento)
into :ll_num_documento
from tes_off_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_documento = :ls_cod_documento and
	anno_documento = :ll_anno_esc and
	numeratore_documento = :ls_numeratore_documento;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il calcolo del num_documento.", sqlca)
	return
elseif sqlca.sqlcode = 100 or isnull(ll_num_documento) or ll_num_documento < 1 then
	ll_num_documento = 0
end if

ll_num_documento++

// aggiorno tabella numeratori
select cod_documento
into :ls_test
from numeratori
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_documento = :ls_cod_documento and
	numeratore_documento = :ls_numeratore_documento and
	anno_documento = :ll_anno_esc;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo dei numeratori")
	return
elseif sqlca.sqlcode = 100 then
	g_mb.error("Non è impostato nessun numeratore per il documento " + ls_cod_documento + "-" + string(ll_anno_esc) + "-" + ls_numeratore_documento)
	return
end if

update tes_off_ven
set
	cod_documento = :ls_cod_documento,
	anno_documento = :ll_anno_esc,
	numeratore_documento = :ls_numeratore_documento,
	num_documento = :ll_num_documento,
	data_offerta = :ldt_data,
	num_revisione = 1
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione and
	num_registrazione = :ll_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il salvataggio del numero fiscale.", sqlca)
	rollback;
	return
end if

update numeratori
set num_documento = :ll_num_documento
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_documento = :ls_cod_documento and
	anno_documento = :ll_anno_esc and
	numeratore_documento = :ls_numeratore_documento;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante l'aggiornamento dei numeratori.", sqlca)
	rollback;
	return
end if
// ----

commit;
	
// aggiorno campi DW
dw_tes_off_ven_det_1.setitem(ai_row, "cod_documento", ls_cod_documento)
dw_tes_off_ven_det_1.setitem(ai_row, "anno_documento", ll_anno_esc)
dw_tes_off_ven_det_1.setitem(ai_row, "numeratore_documento", ls_numeratore_documento)
dw_tes_off_ven_det_1.setitem(ai_row, "num_documento", ll_num_documento)
dw_tes_off_ven_det_1.setitem(ai_row, "data_offerta", ldt_data)
dw_tes_off_ven_det_1.setitem(ai_row, "num_revisione", 1)


dw_tes_off_ven_det_1.resetupdate()
//triggerevent("pc_retrieve")
end subroutine

public subroutine wf_duplica_offerta (boolean ab_revisiona);/**
 * stefanop
 * 13/10/2011
 *
 * Duplico il documento in una nuova offerta.
 * Se devo revisionare (ab_revisiona = true) allora copia tutte le informazioni, compresa
 * la numerazione, ed incremento la revisione
 * Se devo duplicare non copio la numerazione e la revisione parte da 0
 **/
 
string ls_num_offerta, ls_cod_documento, ls_numeratore_documento, ls_errore
int li_row
long ll_anno_registrazione, ll_num_registrazione, ll_anno_registrazione_orig, ll_num_registrazione_orig, ll_revisione, ll_anno_documento, &
		ll_num_documento
datetime ldt_oggi, ldt_data_offerta

ll_revisione = 0
li_row = dw_tes_off_ven_det_1.getrow()
ls_num_offerta = dw_tes_off_ven_det_1.getitemstring(li_row, "num_offerta")
ls_cod_documento = dw_tes_off_ven_det_1.getitemstring(li_row, "cod_documento")
ll_anno_documento = dw_tes_off_ven_det_1.getitemnumber(li_row, "anno_documento")
ls_numeratore_documento = dw_tes_off_ven_det_1.getitemstring(li_row, "numeratore_documento")
ll_num_documento = dw_tes_off_ven_det_1.getitemnumber(li_row, "num_documento")
ldt_data_offerta = dw_tes_off_ven_det_1.getitemdatetime(li_row, "data_offerta")
ldt_oggi = datetime(today(), 00:00:00)
ll_anno_registrazione = f_anno_esercizio()

// calcolo nuova revisione
if ab_revisiona then
	
	select max(num_revisione)
	into :ll_revisione
	from tes_off_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_documento = :ls_cod_documento and
		anno_documento = :ll_anno_documento and
		numeratore_documento = :ls_numeratore_documento and
		num_documento = :ll_num_documento;
	 
	if isnull(ll_revisione) or ll_revisione < 1 then
		ll_revisione = 1
	else
		ll_revisione ++
	end if
	
else
	
	 // azzero i valori in caso di una nuova offerta
	setnull(ls_cod_documento)
	setnull(ll_anno_documento)
	setnull(ls_numeratore_documento)
	setnull(ll_num_documento)
	setnull(ldt_data_offerta)
	
end if	

// calcolo nuova registrazione
select max(num_registrazione)
into :ll_num_registrazione
from tes_off_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione;
		 
if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

ll_anno_registrazione_orig = dw_tes_off_ven_det_1.getitemnumber(li_row, "anno_registrazione")
ll_num_registrazione_orig = dw_tes_off_ven_det_1.getitemnumber(li_row, "num_registrazione")

insert into tes_off_ven (
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	cod_cliente,   
	data_registrazione,   
	data_scadenza,   
	cod_operatore,   
	cod_tipo_off_ven,   
	cod_des_cliente,   
	rag_soc_1,   
	rag_soc_2,   
	indirizzo,   
	localita,   
	frazione,   
	cap,   
	provincia,   
	cod_deposito,   
	cod_ubicazione,   
	cod_valuta,   
	cambio_ven,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	sconto,   
	cod_agente_1,   
	cod_agente_2,   
	cod_banca,   
	num_ric_cliente,   
	data_ric_cliente,   
	cod_imballo,   
	aspetto_beni,   
	peso_netto,   
	peso_lordo,   
	num_colli,   
	cod_vettore,   
	cod_inoltro,   
	cod_mezzo,   
	causale_trasporto,   
	cod_porto,   
	cod_resa,   
	nota_testata,   
	nota_piede,   
	flag_fuori_fido,   
	flag_blocco,   
	flag_evasione,   
	flag_riep_bol,   
	flag_riep_fat,   
	tot_val_offerta,   
	cod_contatto,   
	data_consegna,   
	cod_banca_clien_for,
	spese_sicurezza,
	num_offerta,
	num_revisione,
	richiedente_nome,
	richiedente_funzione,
	richiedente_telefono,
	richiedente_email,
	richiedente_cellulare,
	cod_documento,
	anno_documento,
	numeratore_documento,
	num_documento,
	data_offerta,
	flag_pubblica_intranet,
	cod_causale,
	mezzo_ricezione
) select
		tes_off_ven.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		tes_off_ven.cod_cliente,   
		:ldt_oggi,      
		tes_off_ven.data_scadenza,   
		tes_off_ven.cod_operatore,   
		tes_off_ven.cod_tipo_off_ven,   
		tes_off_ven.cod_des_cliente,   
		tes_off_ven.rag_soc_1,   
		tes_off_ven.rag_soc_2,   
		tes_off_ven.indirizzo,   
		tes_off_ven.localita,   
		tes_off_ven.frazione,   
		tes_off_ven.cap,   
		tes_off_ven.provincia,   
		tes_off_ven.cod_deposito,   
		tes_off_ven.cod_ubicazione,   
		tes_off_ven.cod_valuta,   
		tes_off_ven.cambio_ven,   
		tes_off_ven.cod_tipo_listino_prodotto,   
		tes_off_ven.cod_pagamento,   
		tes_off_ven.sconto,   
		tes_off_ven.cod_agente_1,   
		tes_off_ven.cod_agente_2,   
		tes_off_ven.cod_banca,   
		tes_off_ven.num_ric_cliente,   
		tes_off_ven.data_ric_cliente,   
		tes_off_ven.cod_imballo,   
		tes_off_ven.aspetto_beni,   
		tes_off_ven.peso_netto,   
		tes_off_ven.peso_lordo,   
		tes_off_ven.num_colli,   
		tes_off_ven.cod_vettore,   
		tes_off_ven.cod_inoltro,   
		tes_off_ven.cod_mezzo,   
		tes_off_ven.causale_trasporto,   
		tes_off_ven.cod_porto,   
		tes_off_ven.cod_resa,   
		tes_off_ven.nota_testata,   
		tes_off_ven.nota_piede,   
		tes_off_ven.flag_fuori_fido,   
		'N',   
		'A',   
		tes_off_ven.flag_riep_bol,   
		tes_off_ven.flag_riep_fat,   
		tes_off_ven.tot_val_offerta,   
		tes_off_ven.cod_contatto,   
		tes_off_ven.data_consegna,   
		tes_off_ven.cod_banca_clien_for,
		tes_off_ven.spese_sicurezza,
		tes_off_ven.num_offerta,
		:ll_revisione,
		tes_off_ven.richiedente_nome,
		tes_off_ven.richiedente_funzione,
		tes_off_ven.richiedente_telefono,
		tes_off_ven.richiedente_email,
		tes_off_ven.richiedente_cellulare,
		:ls_cod_documento,
		:ll_anno_documento,
		:ls_numeratore_documento,
		:ll_num_documento,
		:ldt_data_offerta,
		tes_off_ven.flag_pubblica_intranet,
		tes_off_ven.cod_causale,
		tes_off_ven.mezzo_ricezione
	from tes_off_ven
	where
		tes_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		tes_off_ven.anno_registrazione = :ll_anno_registrazione_orig and  
		tes_off_ven.num_registrazione = :ll_num_registrazione_orig;

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di duplicazione testata offerte.", sqlca)
	rollback;
	return
end if

insert into det_off_ven (
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	prog_riga_off_ven,   
	cod_tipo_det_ven,   
	cod_misura,   
	cod_prodotto,   
	des_prodotto,   
	quan_offerta,   
	prezzo_vendita,   
	fat_conversione_ven,   
	sconto_1,   
	sconto_2,   
	provvigione_1,   
	provvigione_2,   
	val_riga,   
	cod_iva,   
	data_consegna,   
	nota_dettaglio,   
	sconto_3,   
	sconto_4,   
	sconto_5,   
	sconto_6,   
	sconto_7,   
	sconto_8,   
	sconto_9,   
	sconto_10,   
	cod_centro_costo,
	anno_reg_richiesta,
	num_reg_richiesta,
	num_riga_appartenenza,
	flag_gen_commessa
) select
		det_off_ven.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		det_off_ven.prog_riga_off_ven,   
		det_off_ven.cod_tipo_det_ven,   
		det_off_ven.cod_misura,   
		det_off_ven.cod_prodotto,   
		det_off_ven.des_prodotto,   
		det_off_ven.quan_offerta,   
		det_off_ven.prezzo_vendita,   
		det_off_ven.fat_conversione_ven,   
		det_off_ven.sconto_1,   
		det_off_ven.sconto_2,   
		det_off_ven.provvigione_1,   
		det_off_ven.provvigione_2,   
		det_off_ven.val_riga,   
		det_off_ven.cod_iva,   
		det_off_ven.data_consegna,   
		det_off_ven.nota_dettaglio,   
		det_off_ven.sconto_3,   
		det_off_ven.sconto_4,   
		det_off_ven.sconto_5,   
		det_off_ven.sconto_6,   
		det_off_ven.sconto_7,   
		det_off_ven.sconto_8,   
		det_off_ven.sconto_9,   
		det_off_ven.sconto_10,   
		det_off_ven.cod_centro_costo,
		det_off_ven.anno_reg_richiesta,
		det_off_ven.num_reg_richiesta,
		det_off_ven.num_riga_appartenenza,
		det_off_ven.flag_gen_commessa
	from det_off_ven
	where
		det_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		det_off_ven.anno_registrazione = :ll_anno_registrazione_orig and  
		det_off_ven.num_registrazione = :ll_num_registrazione_orig;

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di duplicazione dettagli offerte.", sqlca)
	rollback;
	return
end if

insert into det_off_ven_stat (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_off_ven,   
		cod_tipo_analisi,   
		progressivo,   
		cod_statistico_1,   
		cod_statistico_2,   
		cod_statistico_3,   
		cod_statistico_4,   
		cod_statistico_5,   
		cod_statistico_6,   
		cod_statistico_7,   
		cod_statistico_8,   
		cod_statistico_9,   
		cod_statistico_10,   
		percentuale,   
		flag_trasferito
) select
		det_off_ven_stat.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		det_off_ven_stat.prog_riga_off_ven,   
		det_off_ven_stat.cod_tipo_analisi,   
		det_off_ven_stat.progressivo,   
		det_off_ven_stat.cod_statistico_1,   
		det_off_ven_stat.cod_statistico_2,   
		det_off_ven_stat.cod_statistico_3,   
		det_off_ven_stat.cod_statistico_4,   
		det_off_ven_stat.cod_statistico_5,   
		det_off_ven_stat.cod_statistico_6,   
		det_off_ven_stat.cod_statistico_7,   
		det_off_ven_stat.cod_statistico_8,   
		det_off_ven_stat.cod_statistico_9,   
		det_off_ven_stat.cod_statistico_10,   
		det_off_ven_stat.percentuale,   
		'N'  
	from det_off_ven_stat  
	where
		det_off_ven_stat.cod_azienda = :s_cs_xx.cod_azienda and   
		det_off_ven_stat.anno_registrazione = :ll_anno_registrazione_orig and   
		det_off_ven_stat.num_registrazione = :ll_num_registrazione_orig;


if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di duplicazione dettagli statistici.", sqlca)
	rollback;
	return
end if

update con_off_ven
set con_off_ven.num_registrazione = :ll_num_registrazione
where con_off_ven.cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di aggiornamento contatore.", sqlca)
	rollback;
	return
end if

if not wf_duplica_altre_tabelle(ll_anno_registrazione_orig, ll_num_registrazione_orig, ll_anno_registrazione, ll_num_registrazione, ls_errore) then
	
	rollback;	
	if not isnull(ls_errore) and len(ls_errore) > 0 then g_mb.error(ls_errore)
	return
	
end if

commit;

long ll_handle
ll_handle = wf_aggiungi_offerta(0, ll_anno_registrazione, ll_num_registrazione)
tv_1.selectitem(ll_handle)
postevent("pc_menu_calcola")

if ab_revisiona then
	g_mb.show("Revisione offerta completata. Generata offerta N. " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
else
	g_mb.show("Generata offerta N. " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
end if

end subroutine

public function long wf_aggiungi_offerta (long al_handle, integer ai_anno_registrazione, long al_num_registrazione, string as_cod_documento, integer ai_anno_documento, string as_numeratore_documento, long al_num_documento, long ai_icon);/**
 * stefanop
 * 23/09/2011
 *
 * Codice centralizzato per inserie un'offerta all'albero in quanto richiesta in più parti
 **/

treeviewitem ltvi_item
ss_record lss_record
dec{4} ld_imponibile_iva, ld_importo_iva, ld_tot_documento
string ls_cod_cliente, ls_cod_contatto, ls_rag_soc_1, ls_localita

ltvi_item = wf_new_treeviewitem(false, ai_icon)

lss_record.anno_registrazione = ai_anno_registrazione
lss_record.num_registrazione = al_num_registrazione
lss_record.livello = il_livello_corrente
lss_record.tipo_livello = "O"


//inseriamo più informazioni nel nodo
select 	imponibile_iva, importo_iva,
			cod_cliente,
			cod_contatto
into   		:ld_imponibile_iva, :ld_importo_iva,
			:ls_cod_cliente,
			:ls_cod_contatto
from   tes_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ai_anno_registrazione and
		 num_registrazione = :al_num_registrazione;

if isnull(ld_imponibile_iva) then ld_imponibile_iva=0
if isnull(ld_importo_iva) then ld_importo_iva=0
ld_tot_documento = ld_imponibile_iva + ld_importo_iva

if len(as_cod_documento) > 0 then
	ltvi_item.label = as_cod_documento + "-" + string(ai_anno_documento) + "/" + as_numeratore_documento + "-" + string(al_num_documento) + " (" + string(ai_anno_registrazione) + "/"+ string(al_num_registrazione) + ")"
else
	ltvi_item.label = string(ai_anno_registrazione) + "/"+ string(al_num_registrazione)
end if

ltvi_item.label += " Val. "+string(ld_tot_documento, "#,###,###,##0.00") 


if not isnull(ls_cod_cliente) then
	//cliente ----------------------------------
	select		rag_soc_1,
				localita
	into		:ls_rag_soc_1,
				:ls_localita
	from   anag_clienti 
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
	
	ltvi_item.label += " -" + ls_rag_soc_1 + " (" + ls_cod_cliente + ")"

	
elseif not isnull(ls_cod_contatto) then
	
	//cliente ----------------------------------
	select		rag_soc_1,
				localita
	into		:ls_rag_soc_1,
				:ls_localita
	from   anag_contatti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_contatto = :ls_cod_contatto;
	
	ltvi_item.label += " -" + ls_rag_soc_1 + " (* " + ls_cod_contatto + " *)"
	
end if

ltvi_item.data = lss_record

return tv_1.insertitemlast(al_handle, ltvi_item)
end function

public function boolean wf_update_note ();/**
 * stefanop
 * 26/11/2011
 *
 * Il campo note_testata e nota_piede sono diventati campi text e per fare l'update
 * bisogna farlo a mano in quanto PB è limitato nella WHERE
 **/
 
string ls_nota
long ll_row, ll_anno_registrazione, ll_num_registrazione

ll_row = dw_tes_off_ven_det_4.getrow()
ll_anno_registrazione = dw_tes_off_ven_det_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_registrazione = dw_tes_off_ven_det_1.getitemnumber(ll_row, "num_registrazione")

// Nota testa
if dw_tes_off_ven_det_4.getitemstatus(ll_row, "nota_testata", Primary!) <> NotModified! then
	

	
	ls_nota = dw_tes_off_ven_det_4.object.nota_testata.copyrtf()  //    getitemstring(ll_row, "nota_testata")
	ls_nota = ls_nota + "~r~n" 
	dw_tes_off_ven_det_4.object.nota_testata.pastertf(ls_nota)

	ls_nota = dw_tes_off_ven_det_4.getitemstring(ll_row, "nota_testata")	
	string ls_nota_rtf
	ls_nota_rtf = dw_tes_off_ven_det_4.object.nota_testata.copyrtf(false)
	
	
	update tes_off_ven
	set nota_testata = :ls_nota
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante il salvataggio delle note fisse.", sqlca)
		rollback;
		return false
	end if
end if

// Nota piede
if dw_tes_off_ven_det_4.getitemstatus(ll_row, "nota_piede", Primary!) <> NotModified! then
	
	ls_nota = dw_tes_off_ven_det_4.getitemstring(ll_row, "nota_piede")
	
	update tes_off_ven
	set 	nota_piede = :ls_nota
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante il salvataggio delle note fisse.", sqlca)
		rollback;
		return false
	end if
	
end if

commit;
return true
end function

public function boolean wf_duplica_altre_tabelle (long al_anno_registrazione_orig, long al_num_registrazione_orig, long al_anno_registrazione_dupl, long al_num_registrazione_dupl, ref string as_errore);/**
 * Funzione da estendere nelle finestre che richiedono la duplicazione di altre tabelle
 **/
 
 return true
end function

public function integer wf_carica_campi_extra ();/**
 * stefanop
 * 15/02/2012
 *
 * Carico le drop di DATA-SCADENZA e TEMPI-CONSEGNA con i valori più usati
 **/
 
string ls_sql, ls_modify, ls_error
datawindowchild ldw_child

// des_validita
ls_sql = "SELECT TOP 10 count(*) as data_column, des_validita as display_column FROM tes_off_ven WHERE des_validita is not null GROUP BY des_validita ORDER BY data_column desc"

dw_tes_off_ven_det_1.getchild("des_validita", ldw_child)

ldw_child.settransobject(sqlca)
ls_modify = "DataWindow.Table.Select='" + ls_sql  + "'"
ls_error   = ldw_child.Modify(ls_modify)
ldw_child.Retrieve()

// des_validiita
ls_sql = "SELECT TOP 10 count(*) as data_column, des_tempi_consegna as display_column FROM tes_off_ven WHERE des_tempi_consegna is not null GROUP BY des_tempi_consegna ORDER BY data_column desc"

dw_tes_off_ven_det_2.getchild("des_tempi_consegna", ldw_child)

ldw_child.settransobject(sqlca)
ls_modify = "DataWindow.Table.Select='" + ls_sql  + "'"
ls_error   = ldw_child.Modify(ls_modify)
ldw_child.Retrieve()
	
return 1
end function

public function integer wf_get_info_cliente_contatto (long fl_row, string fs_codice, integer fi_tipo_info, ref string fs_errore);//questa funzione esegue la select dalla tabella anagrafica
//			anag_clienti			se fi_tipo_info = 0
//			
//			anag_contatti		se fi_tipo_info = 1

string			ls_sql, ls_tabella, ls_colonna
datastore	lds_data
integer		li_ret

string				ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_partita_iva,ls_cod_fiscale ,&
					ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca_clien_for, &
					ls_cod_banca, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_flag_fuori_fido, ls_flag_riep_boll, ls_flag_riep_fatt, &
					ls_cod_contatto, ls_cod_cliente, ls_null, ls_messaggio
									  
dec{4}			ld_sconto, ld_null

datetime			ldt_data_registrazione


setnull(ls_null)
setnull(ld_null)

ldt_data_registrazione = dw_tes_off_ven_det_1.getitemdatetime(fl_row, "data_registrazione")

if fi_tipo_info = 0 then
	ls_tabella = "anag_clienti"
	ls_colonna = "cod_cliente"
	
	//questo controllo solo in caso di cliente
	li_ret = iuo_fido.uof_get_blocco_cliente( fs_codice, ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
	if li_ret=2 then
		//blocco
		//in ls_messaggio il messaggio
		fs_errore = ls_messaggio
		return 1
	elseif li_ret=1 then
		//solo warning
		g_mb.warning(ls_messaggio)
	end if
	
	
else
	ls_tabella = "anag_contatti"
	ls_colonna = "cod_contatto"
end if

ls_sql = "select 		 rag_soc_1,"+&
							"rag_soc_2,"+&
							"indirizzo,"+&
							"frazione,"+&
							"cap,"+&
							"localita,"+&
							"provincia,"+&
							"telefono,"+&
							"fax,"+&
							"telex,"+&
							"partita_iva,"+&
							"cod_fiscale,"+&
							"cod_deposito,"+&
							"cod_valuta,"+&
							"cod_tipo_listino_prodotto,"+&
							"cod_pagamento,"+&
							"sconto,"+&
							"cod_agente_1,"+&
							"cod_agente_2,"+&
							"cod_banca_clien_for,"+&
							"cod_imballo,"+&
							"cod_vettore,"+&
							"cod_inoltro,"+&
							"cod_mezzo,"+&
							"cod_porto,"+&
							"cod_resa,"+&
							"flag_fuori_fido,"+&
							"flag_riep_boll,"+&
							"flag_riep_fatt "
							
//l'ultima colonna selezionata dipende da fi_tipo_info
if fi_tipo_info=0 then
	//anag_clienti
	ls_sql += ", cod_banca "
else
	//anag_contatti
	ls_sql += ", cod_cliente "
end if
							
ls_sql += "from " + ls_tabella + " " +&
			 "where  cod_azienda='"+s_cs_xx.cod_azienda+ "' and "+&
					   ls_colonna + "='" + fs_codice+ "' "

//al + avrà una sola riga
li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)

if li_ret<0 then
	//errore
	return -1
	
elseif li_ret=0 then
	//informazione non trovata!
	
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_deposito", ls_null)
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_valuta", ls_null)
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_tipo_listino_prodotto", ls_null)
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_pagamento", ls_null)
	dw_tes_off_ven_det_1.setitem(fl_row, "sconto", ld_null)
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_agente_1", ls_null)
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_agente_2", ls_null)
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_banca_clien_for", ls_null)
	
	if fi_tipo_info = 0 then
		//anag_clienti
		dw_tes_off_ven_det_1.setitem(fl_row, "cod_banca", ls_null)
	else
		//anag_contatti
	end if
	
	dw_tes_off_ven_det_1.setitem(fl_row, "flag_fuori_fido", "N")
	dw_tes_off_ven_det_1.setitem(fl_row, "flag_riep_bol", "N")
	dw_tes_off_ven_det_1.setitem(fl_row, "flag_riep_fat", "N")
	
	dw_tes_off_ven_det_2.modify("st_cod_cliente.text=''")
	dw_tes_off_ven_det_2.modify("st_cod_contatto.text=''")
	dw_tes_off_ven_det_2.modify("st_rag_soc_1.text=''")
	dw_tes_off_ven_det_2.modify("st_rag_soc_2.text=''")
	dw_tes_off_ven_det_2.modify("st_indirizzo.text=''")
	dw_tes_off_ven_det_2.modify("st_frazione.text=''")
	dw_tes_off_ven_det_2.modify("st_cap.text=''")
	dw_tes_off_ven_det_2.modify("st_localita.text=''")
	dw_tes_off_ven_det_2.modify("st_provincia.text=''")
	dw_tes_off_ven_det_2.modify("st_telefono.text=''")
	dw_tes_off_ven_det_2.modify("st_fax.text=''")
	dw_tes_off_ven_det_2.modify("st_telex.text=''")
	dw_tes_off_ven_det_2.modify("st_partita_iva.text=''")
	dw_tes_off_ven_det_2.modify("st_cod_fiscale.text=''")
	
	dw_tes_off_ven_det_3.setitem(fl_row, "cod_imballo", ls_null)
	dw_tes_off_ven_det_3.setitem(fl_row, "cod_vettore", ls_null)
	dw_tes_off_ven_det_3.setitem(fl_row, "cod_inoltro", ls_null)
	dw_tes_off_ven_det_3.setitem(fl_row, "cod_mezzo", ls_null)
	dw_tes_off_ven_det_3.setitem(fl_row, "cod_porto", ls_null)
	dw_tes_off_ven_det_3.setitem(fl_row, "cod_resa", ls_null)
	
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_des_cliente", ls_null)
	
	if fi_tipo_info=0 then
		//anag_clienti
		fs_errore = "Codice cliente "+fs_codice+" inesistente!"
	else
		//anag_contatti
		fs_errore = "Codice contatto "+fs_codice+" inesistente!"
	end if
	
	return 1
	
end if

//lettura dati dal datastore --------------------------------------------------------------
ls_rag_soc_1							= lds_data.getitemstring(1, 1)
ls_rag_soc_2							= lds_data.getitemstring(1, 2)
ls_indirizzo								= lds_data.getitemstring(1, 3)
ls_frazione								= lds_data.getitemstring(1, 4)
ls_cap									= lds_data.getitemstring(1, 5)
ls_localita								= lds_data.getitemstring(1, 6)
ls_provincia								= lds_data.getitemstring(1, 7)
ls_telefono								= lds_data.getitemstring(1, 8)
ls_fax										= lds_data.getitemstring(1, 9)
ls_telex									= lds_data.getitemstring(1, 10)
ls_partita_iva							= lds_data.getitemstring(1, 11)
ls_cod_fiscale							= lds_data.getitemstring(1, 12)
ls_cod_deposito						= lds_data.getitemstring(1, 13)
ls_cod_valuta							= lds_data.getitemstring(1, 14)
ls_cod_tipo_listino_prodotto			= lds_data.getitemstring(1, 15)
ls_cod_pagamento						= lds_data.getitemstring(1, 16)
ld_sconto								= lds_data.getitemdecimal(1, 17)
ls_cod_agente_1						= lds_data.getitemstring(1, 18)
ls_cod_agente_2						= lds_data.getitemstring(1, 19)
ls_cod_banca_clien_for				= lds_data.getitemstring(1, 20)
ls_cod_imballo							= lds_data.getitemstring(1, 21)
ls_cod_vettore							= lds_data.getitemstring(1, 22)
ls_cod_inoltro							= lds_data.getitemstring(1, 23)
ls_cod_mezzo							= lds_data.getitemstring(1, 24)
ls_cod_porto							= lds_data.getitemstring(1, 25)
ls_cod_resa								= lds_data.getitemstring(1, 26)
ls_flag_fuori_fido						= lds_data.getitemstring(1, 27)
ls_flag_riep_boll						= lds_data.getitemstring(1, 28)
ls_flag_riep_fatt						= lds_data.getitemstring(1, 29)

if fi_tipo_info = 0 then
	//anag_clienti
	ls_cod_banca							= lds_data.getitemstring(1, 30)
else
	//anag_contatti
	ls_cod_cliente							= lds_data.getitemstring(1, 31)
end if

if ls_cod_cliente="" then setnull(ls_cod_cliente)

//-----------------------------------------------------------------------------------------


if not isnull(ls_rag_soc_1) then
	dw_tes_off_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
else
	dw_tes_off_ven_det_2.modify("st_rag_soc_1.text=''")
end if
if not isnull(ls_rag_soc_2) then
	dw_tes_off_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
else
	dw_tes_off_ven_det_2.modify("st_rag_soc_2.text=''")
end if
if not isnull(ls_indirizzo) then
	dw_tes_off_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
else
	dw_tes_off_ven_det_2.modify("st_indirizzo.text=''")
end if
if not isnull(ls_frazione) then
	dw_tes_off_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
else
	dw_tes_off_ven_det_2.modify("st_frazione.text=''")
end if
if not isnull(ls_cap) then
	dw_tes_off_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
else
	dw_tes_off_ven_det_2.modify("st_cap.text=''")
end if
if not isnull(ls_localita) then
	dw_tes_off_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
else
	dw_tes_off_ven_det_2.modify("st_localita.text=''")
end if
if not isnull(ls_provincia) then
	dw_tes_off_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
else
	dw_tes_off_ven_det_2.modify("st_provincia.text=''")
end if
if not isnull(ls_telefono) then
	dw_tes_off_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
else
	dw_tes_off_ven_det_2.modify("st_telefono.text=''")
end if
if not isnull(ls_fax) then
	dw_tes_off_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
else
	dw_tes_off_ven_det_2.modify("st_fax.text=''")
end if
if not isnull(ls_telex) then
	dw_tes_off_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
else
	dw_tes_off_ven_det_2.modify("st_telex.text=''")
end if
if not isnull(ls_partita_iva) then
	dw_tes_off_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
else
	dw_tes_off_ven_det_2.modify("st_partita_iva.text=''")
end if
if not isnull(ls_cod_fiscale) then
	dw_tes_off_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
else
	dw_tes_off_ven_det_2.modify("st_cod_fiscale.text=''")
end if

dw_tes_off_ven_det_1.setitem(fl_row, "cod_deposito", ls_cod_deposito)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_valuta", ls_cod_valuta)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_pagamento", ls_cod_pagamento)
dw_tes_off_ven_det_1.setitem(fl_row, "sconto", ld_sconto)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_agente_1", ls_cod_agente_1)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_agente_2", ls_cod_agente_2)

if ls_cod_banca_clien_for="" then setnull(ls_cod_banca_clien_for)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_banca_clien_for", ls_cod_banca_clien_for)

if fi_tipo_info = 0 then
	//anag_clienti
	if ls_cod_banca="" then setnull(ls_cod_banca)
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_banca", ls_cod_banca)
else
	//anag_contatti
	
	//se il campo cliente è vuoto, metti il cliente del contatto, se ne hai trovato uno ....
	if dw_tes_off_ven_det_1.getitemstring(fl_row, "cod_cliente")="" or isnull(dw_tes_off_ven_det_1.getitemstring(fl_row, "cod_cliente")) then
		dw_tes_off_ven_det_1.setitem(fl_row, "cod_cliente", ls_cod_cliente)
	else
		//lascia il cliente che c'è ...
	end if
	
end if

dw_tes_off_ven_det_3.setitem(fl_row, "cod_imballo", ls_cod_imballo)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_vettore", ls_cod_vettore)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_inoltro", ls_cod_inoltro)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_mezzo", ls_cod_mezzo)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_porto", ls_cod_porto)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_resa", ls_cod_resa)

if isnull(ls_flag_fuori_fido) or ls_flag_fuori_fido="" then ls_flag_fuori_fido = "N"
dw_tes_off_ven_det_1.setitem(fl_row, "flag_fuori_fido", ls_flag_fuori_fido)

if isnull(ls_flag_riep_boll) or ls_flag_riep_boll="" then ls_flag_riep_boll = "N"
dw_tes_off_ven_det_1.setitem(fl_row, "flag_riep_bol", ls_flag_riep_boll)

if isnull(ls_flag_riep_fatt) or ls_flag_riep_fatt="" then ls_flag_riep_fatt = "N"
dw_tes_off_ven_det_1.setitem(fl_row, "flag_riep_fat", ls_flag_riep_fatt)


dw_tes_off_ven_det_1.setitem(fl_row, "cod_des_cliente", ls_null)

if fi_tipo_info = 0 then
	//anag_clienti
	
	if not isnull(fs_codice) then
		dw_tes_off_ven_det_2.modify("st_cod_cliente.text='" + fs_codice + "'")
	else
		dw_tes_off_ven_det_2.modify("st_cod_cliente.text=''")
	end if
	
	f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
					  "cod_des_cliente", &
					  sqlca, &
					  "anag_des_clienti", &
					  "cod_des_cliente", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + fs_codice + &
					  				"' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) "+&
									  "and flag_dest_merce_fat = 'S'")
	
	//leggo l'eventuale contatto associato al cliente
	select anag_contatti.cod_contatto
	into   :ls_cod_contatto
	from   anag_contatti
	where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_contatti.cod_cliente = :fs_codice;

	if ls_cod_contatto="" then setnull(ls_cod_contatto)

	//se il campo contatto è vuoto, metti il contatto del cliente, se ne hai trovato uno ....
	if dw_tes_off_ven_det_1.getitemstring(fl_row, "cod_contatto")="" or isnull(dw_tes_off_ven_det_1.getitemstring(fl_row, "cod_contatto")) then

		if sqlca.sqlcode = 0 then 
			dw_tes_off_ven_det_1.setitem(fl_row, "cod_contatto", ls_cod_contatto)
			dw_tes_off_ven_det_2.modify("st_cod_contatto.text='" + ls_cod_contatto + "'")
		else
			dw_tes_off_ven_det_1.setitem(fl_row, "cod_contatto", ls_null)
			dw_tes_off_ven_det_2.modify("st_cod_contatto.text=''")
		end if
	
	end if
	
else
	//anag_contatti
	
	dw_tes_off_ven_det_2.modify("st_cod_contatto.text=''")

	f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
					  "cod_des_cliente", &
					  sqlca, &
					  "anag_des_clienti", &
					  "cod_des_cliente", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + fs_codice + &
					  				"' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) ")
	
	dw_tes_off_ven_det_2.setitem(fl_row, "rag_soc_1", ls_null)
	dw_tes_off_ven_det_2.setitem(fl_row, "rag_soc_2", ls_null)
	dw_tes_off_ven_det_2.setitem(fl_row, "indirizzo", ls_null)
	dw_tes_off_ven_det_2.setitem(fl_row, "frazione", ls_null)
	dw_tes_off_ven_det_2.setitem(fl_row, "cap", ls_null)
	dw_tes_off_ven_det_2.setitem(fl_row, "localita", ls_null)
	dw_tes_off_ven_det_2.setitem(fl_row, "provincia", ls_null)
	
end if

f_cambio_ven(ls_cod_valuta, ldt_data_registrazione)

return 0
end function

public subroutine wf_pulisci_campi_cliente_contatto (long fl_row, integer fi_tipo);string				ls_null
dec{4}			ld_null

setnull(ls_null)
setnull(ld_null)

dw_tes_off_ven_det_1.setitem(fl_row, "cod_deposito", ls_null)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_valuta", ls_null)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_tipo_listino_prodotto", ls_null)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_pagamento", ls_null)
dw_tes_off_ven_det_1.setitem(fl_row, "sconto", ld_null)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_agente_1", ls_null)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_agente_2", ls_null)
dw_tes_off_ven_det_1.setitem(fl_row, "cod_banca_clien_for", ls_null)

if fi_tipo=0 then
	//anag_clienti
	dw_tes_off_ven_det_1.setitem(fl_row, "cod_banca", ls_null)
else
	//anag_contatti
	//dw_tes_off_ven_det_1.setitem(fl_row, "cod_cliente", ls_null)
end if

dw_tes_off_ven_det_1.setitem(fl_row, "flag_fuori_fido", "N")
dw_tes_off_ven_det_1.setitem(fl_row, "flag_riep_bol", "N")
dw_tes_off_ven_det_1.setitem(fl_row, "flag_riep_fat", "N")

dw_tes_off_ven_det_2.modify("st_cod_cliente.text=''")
dw_tes_off_ven_det_2.modify("st_rag_soc_1.text=''")
dw_tes_off_ven_det_2.modify("st_rag_soc_2.text=''")
dw_tes_off_ven_det_2.modify("st_indirizzo.text=''")
dw_tes_off_ven_det_2.modify("st_frazione.text=''")
dw_tes_off_ven_det_2.modify("st_cap.text=''")
dw_tes_off_ven_det_2.modify("st_localita.text=''")
dw_tes_off_ven_det_2.modify("st_provincia.text=''")
dw_tes_off_ven_det_2.modify("st_telefono.text=''")
dw_tes_off_ven_det_2.modify("st_fax.text=''")
dw_tes_off_ven_det_2.modify("st_telex.text=''")
dw_tes_off_ven_det_2.modify("st_partita_iva.text=''")
dw_tes_off_ven_det_2.modify("st_cod_fiscale.text=''")

dw_tes_off_ven_det_3.setitem(fl_row, "cod_imballo", ls_null)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_vettore", ls_null)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_inoltro", ls_null)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_mezzo", ls_null)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_porto", ls_null)
dw_tes_off_ven_det_3.setitem(fl_row, "cod_resa", ls_null)

if fi_tipo=0 then
	//anag_clienti
	f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
						  "cod_des_cliente", &
						  sqlca, &
						  "anag_des_clienti", &
						  "cod_des_cliente", &
						  "rag_soc_1", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'  and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) "+&
										"and flag_dest_merce_fat = 'S'")
else
	//anag_contatti
	f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
						  "cod_des_cliente", &
						  sqlca, &
						  "anag_des_clienti", &
						  "cod_des_cliente", &
						  "rag_soc_1", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'  and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) ")
end if

dw_tes_off_ven_det_1.setitem(fl_row, "cod_des_cliente", ls_null)
//dw_tes_off_ven_det_1.setitem(fl_row, "cod_contatto", ls_null)
dw_tes_off_ven_det_2.modify("st_cod_contatto.text=''")

   
end subroutine

public function integer wf_retrieve_ordini_vendita (long fl_anno_reg_offerta, long fl_num_reg_offerta);long ll_tot
string ls_flag_visualizza_ordini

ls_flag_visualizza_ordini = dw_filtro.getitemstring(dw_filtro.getrow(), "flag_visualizza_ordini")

if ls_flag_visualizza_ordini<> "S" then return 0

ll_tot = dw_tes_ord_ven.retrieve(s_cs_xx.cod_azienda, fl_anno_reg_offerta, fl_num_reg_offerta)

return ll_tot
end function

public subroutine wf_calcola_documento_dw_corrente ();string ls_messaggio, ls_cod_cliente
		
long   ll_num_registrazione, ll_anno_registrazione

uo_calcola_documento_euro luo_calcola_documento_euro

ll_anno_registrazione = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"num_registrazione")
ls_cod_cliente = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(), "cod_cliente")

luo_calcola_documento_euro = create uo_calcola_documento_euro

// stefanop: 18/01/2012: 
//controllo se si è impostato il cliente o un contatto, nel caso di un contatto non devo controllare
//il fido altrimenti non calcola nulla e da errore
if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then 
	luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
end if
// ----

if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"off_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	rollback;
	destroy luo_calcola_documento_euro
	return
else
	commit;
end if

destroy luo_calcola_documento_euro

end subroutine

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori, ls_select_mansionari
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_tipo_off_ven", &
                 sqlca, &
                 "tab_tipi_off_ven", &
                 "cod_tipo_off_ven", &
                 "des_tipo_off_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_ven_det_3, &
                 "cod_causale", &
                 sqlca, &
                 "tab_causali_trasp", &
                 "cod_causale", &
                 "des_causale", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		
// ricerca
f_po_loaddddw_dw(dw_filtro, &
			"cod_tipo_off_ven", &
			sqlca, &
			"tab_tipi_off_ven", &
			"cod_tipo_off_ven", &
			"des_tipo_off_ven", &
			"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_filtro, &
			"cod_valuta", &
			sqlca, &
			"tab_valute", &
			"cod_valuta", &
			"des_valuta", &
			"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_filtro, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_filtro, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_filtro, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

// Firma Approvazione
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_select_mansionari = "cod_azienda = '" + s_cs_xx.cod_azienda + "' "
ls_select_mansionari += " and cod_operatore in ( select cod_operatore &
		from tab_operatori_utenti  &
			join mansionari on &
				tab_operatori_utenti.cod_azienda = mansionari.cod_azienda and &
				tab_operatori_utenti.cod_utente = mansionari.cod_utente &
			join tab_privilegi_mansionari on &
				tab_privilegi_mansionari.cod_azienda = mansionari.cod_azienda and &
				tab_privilegi_mansionari.cod_mansionario = mansionari.cod_resp_divisione and &
				tab_privilegi_mansionari.cod_privilegio = '" + luo_mansionario.approva_off_ven + "' &
		where tab_operatori_utenti.cod_azienda = '" + s_cs_xx.cod_azienda + "')"
		
f_po_loaddddw_dw(dw_tes_off_ven_det_1, &
                 "cod_operatore_approva", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
                 ls_select_mansionari)
end event

event pc_setwindow;call super::pc_setwindow;boolean				lb_test


windowobject lw_oggetti[], lw_empty[]


iuo_fido = create uo_fido_cliente


dw_tes_off_ven_det_1.set_dw_key("cod_azienda")
dw_tes_off_ven_det_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
dw_tes_off_ven_det_2.set_dw_options(sqlca, dw_tes_off_ven_det_1, c_sharedata + c_scrollparent, c_default)
dw_tes_off_ven_det_3.set_dw_options(sqlca, dw_tes_off_ven_det_1, c_sharedata + c_scrollparent, c_default)
dw_tes_off_ven_det_4.set_dw_options(sqlca, dw_tes_off_ven_det_1, c_sharedata + c_scrollparent, c_default)


lw_oggetti[1] = dw_tes_off_ven_det_2
dw_folder.fu_assigntab(2, "Cliente/Destinazione", lw_oggetti[])
lw_oggetti[1] = dw_tes_off_ven_det_4
dw_folder.fu_assigntab(4, "Note", lw_oggetti[])
lw_oggetti[1] = dw_tes_off_ven_det_3
dw_folder.fu_assigntab(3, "Trasporto/Totali", lw_oggetti[])
lw_oggetti[1] = dw_tes_off_ven_det_1
lw_oggetti[2] = dw_tes_ord_ven
dw_folder.fu_assigntab(1, "Testata", lw_oggetti[])
dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_tes_off_ven_det_1
ib_prima_riga = false

dw_tes_ord_ven.settransobject(sqlca)

dw_filtro.insertrow(0)
dw_filtro.triggerevent("ue_proteggi_campi_insert")
guo_functions.uof_leggi_filtro(dw_filtro, "OFFVEN")
wf_init_tree_icons()

lw_oggetti = lw_empty
lw_oggetti[1] = tv_1
dw_folder_search.fu_assigntab(2, "Registrazioni", lw_oggetti[])
lw_oggetti[1] = dw_filtro
dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)


//---------------------------------------------------------------------------------------------------------------------------------
//per gestione personalizzata Eurocablaggi: generazione singolo ordine di vendita da singola offerta di vendita
guo_functions.uof_get_parametro_azienda("SPD", lb_test)
if lb_test then
	is_SPD = "S"
else
	is_SPD = "N"
end if
//-----------------------------------------------------------------------------------------------------------------------------------

guo_functions.uof_get_parametro_azienda("GIB", ib_GIB)
if isnull(ib_GIB) then ib_GIB = false

dw_filtro.object.b_leggi_filtro.Filename  = guo_functions.uof_risorse("treeview/filter.png")
dw_filtro.object.b_memorizza_filtro.Filename  = guo_functions.uof_risorse("treeview/filter-add.png")
dw_filtro.object.b_cancella_filtro.Filename  = guo_functions.uof_risorse("treeview/filter-delete.png")

//datawindow ordini vendita ##############################################
dw_tes_ord_ven.object.p_tes_off_ven.FileName = s_cs_xx.volume + s_cs_xx.risorse + "\treeview\documento_grigio.png"
dw_tes_ord_ven.object.p_det_off_ven.FileName = s_cs_xx.volume + s_cs_xx.risorse + "\treeview\prodotto.png"



// stefanop: 01/02/2013: se arrivo dalla generazione richieste/offerte allora preparo con l'offerta selezionata
if left(s_cs_xx.parametri.parametro_s_10,9) = "richieste" and s_cs_xx.parametri.parametro_d_1 > 1900 and s_cs_xx.parametri.parametro_d_2 > 0 then 	event post ue_auto_search()

end event

event pc_delete;call super::pc_delete;integer li_i


for li_i = 1 to  dw_tes_off_ven_det_1.deletedcount()
	if dw_tes_off_ven_det_1.getitemstring(li_i, "flag_evasione", delete!, true) <> "A" then
	   g_mb.messagebox("Attenzione", "Offerta non cancellabile! Ha già subito un'evasione.", exclamation!, ok!)
	   dw_tes_off_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
next

// cancello nodo dall'albero
long ll_handle
treeviewitem ltvi_item

ll_handle = tv_1.FindItem(CurrentTreeItem!, 0)
if ll_handle > 0 then
	
	tv_1.getitem(ll_handle, ltvi_item)
	ss_record lss_record
	lss_record = ltvi_item.data
	
	// verifico che sia un nodo "O"fferta
	if lss_record.tipo_livello = "O" then tv_1.deleteitem(ll_handle)
	
end if
end event

on w_tes_off_ven.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_folder_search=create dw_folder_search
this.tv_1=create tv_1
this.dw_filtro=create dw_filtro
this.dw_tes_off_ven_det_3=create dw_tes_off_ven_det_3
this.dw_tes_off_ven_det_4=create dw_tes_off_ven_det_4
this.dw_tes_off_ven_det_1=create dw_tes_off_ven_det_1
this.dw_tes_ord_ven=create dw_tes_ord_ven
this.dw_tes_off_ven_det_2=create dw_tes_off_ven_det_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_folder_search
this.Control[iCurrent+3]=this.tv_1
this.Control[iCurrent+4]=this.dw_filtro
this.Control[iCurrent+5]=this.dw_tes_off_ven_det_3
this.Control[iCurrent+6]=this.dw_tes_off_ven_det_4
this.Control[iCurrent+7]=this.dw_tes_off_ven_det_1
this.Control[iCurrent+8]=this.dw_tes_ord_ven
this.Control[iCurrent+9]=this.dw_tes_off_ven_det_2
end on

on w_tes_off_ven.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_folder_search)
destroy(this.tv_1)
destroy(this.dw_filtro)
destroy(this.dw_tes_off_ven_det_3)
destroy(this.dw_tes_off_ven_det_4)
destroy(this.dw_tes_off_ven_det_1)
destroy(this.dw_tes_ord_ven)
destroy(this.dw_tes_off_ven_det_2)
end on

event pc_modify;call super::pc_modify;if dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(), "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", exclamation!, ok!)
   dw_tes_off_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if


wf_carica_campi_extra()
end event

event pc_new;call super::pc_new;wf_carica_campi_extra()
end event

event pc_close;call super::pc_close;

destroy iuo_fido
end event

type dw_folder from u_folder within w_tes_off_ven
integer x = 1234
integer y = 28
integer width = 3227
integer height = 2628
integer taborder = 0
end type

event itemchanged;call super::itemchanged;dw_tes_off_ven_det_3.settaborder("codice_imballo",5)
end event

type dw_folder_search from u_folder within w_tes_off_ven
integer x = 23
integer y = 28
integer width = 1184
integer height = 2628
integer taborder = 10
end type

type tv_1 from treeview within w_tes_off_ven
integer x = 41
integer y = 128
integer width = 1125
integer height = 2504
integer taborder = 90
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"Custom093!","C:\CS_70\framework\RISORSE\Foglio2_Dim_Orig.bmp","Start!","Picture!","Custom096!","Custom024!"}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event itempopulate;treeviewitem ltv_item
ss_record lss_record

pcca.window_current = parent

if isnull(handle) or handle <= 0 or ib_clear_tree then
	return 0
end if

getitem(handle,ltv_item)

lss_record = ltv_item.data

il_livello_corrente = lss_record.livello

if il_livello_corrente < 3 then
	
	il_livello_corrente++
	// controlla cosa c'è al livello successivo ???
	choose case dw_filtro.getitemstring(1, "livello_" + string(il_livello_corrente))
			
		case "T" // caricamento tipi offerte vendita
			if wf_inserisci_tipi_offerta(handle) = 0 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
	
		case "A" // carico lista anni
			if wf_inserisci_anno(handle) = 0 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if

		case "C"  // carico clienti
			if wf_inserisci_clienti(handle) = 0 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "D"  // carico depositi
			if wf_inserisci_depositi(handle) = 0 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "O"  // carico operatori
			if wf_inserisci_operatori(handle) = 0 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "S"  // carico data scadenza
			if wf_inserisci_datascadenza(handle) = 0 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case else
			wf_inserisci_offerte(handle, 0, 0)
			
	end choose
else
	// inserisco le offerte
	wf_inserisci_offerte(handle, 0, 0)
	
end if
end event

event rightclicked;treeviewitem ltv_item
ss_record lss_record

pcca.window_current = parent

if handle < 1 or dw_tes_off_ven_det_1.getitemnumber(1, "anno_registrazione") < 1900 then return

getitem(handle,ltv_item)
lss_record = ltv_item.data

if lss_record.tipo_livello = "O" then
	
	// non sono im modifica o nuovo
	if not dw_tes_off_ven_det_1.ib_stato_nuovo and not dw_tes_off_ven_det_1.ib_stato_modifica then 
		
		m_tes_off_ven lm_menu
		lm_menu = create m_tes_off_ven	
		
		lm_menu.m_configuratore.visible = ib_menu_configuratore
		
		// visualizzo il pulsante di stampa solo se l'offerta ha una numerazione assegnata
		lm_menu.m_stampa.enabled =not isnull(dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(), "cod_documento"))
		lm_menu.m_revisiona.enabled = lm_menu.m_stampa.enabled
		
		//-----------------------------------------------------------------------------------------------
		//per gestione personalizzata Eurocablaggi: generazione singolo ordine di vendita da singola offerta di vendita
		if is_SPD = "S" then
			lm_menu.m_generaordine.visible = true
		else
			lm_menu.m_generaordine.visible = false
		end if
		//-----------------------------------------------------------------------------------------------

		if ib_GIB then
			lm_menu.m_reportmarginalità.visible = true
		else
			lm_menu.m_reportmarginalità.visible = false
		end if

		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		destroy lm_menu
	
	end if

end if
end event

event selectionchanged;treeviewitem ltv_item
ss_record lss_record

pcca.window_current = parent

if isnull(newhandle) or newhandle <= 0 or ib_clear_tree then
	return 0
end if

getitem(newhandle, ltv_item)
lss_record = ltv_item.data

iss_record = lss_record

pcca.window_currentdw = dw_tes_off_ven_det_1
dw_tes_off_ven_det_1.change_dw_current()
parent.postevent("pc_retrieve")
end event

event key;if key = KeyX! and keyflags=3 and s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	if g_mb.confirm("Ricalcolare tutte le offerte nella lista?") then

		long	ll_handle
		string ls_messaggio
		treeviewitem ltv_item
		ss_record lws_record
		uo_calcola_documento_euro luo_calcola_documento_euro
		
		ll_handle = tv_1.FindItem(CurrentTreeItem!, 0)
		
		setpointer(HourGlass!)
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tv_1.getitem(ll_handle, ltv_item)
			
			lws_record = ltv_item.data
			
			if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then
			
				luo_calcola_documento_euro = create uo_calcola_documento_euro
				
				luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
				
				w_cs_xx_mdi.setmicrohelp("Calcolo Offerta " + string(lws_record.anno_registrazione) + "-"  + string(lws_record.num_registrazione))
				Yield()
				
				if luo_calcola_documento_euro.uof_calcola_documento(lws_record.anno_registrazione, lws_record.num_registrazione,"off_ven",ls_messaggio) <> 0 then
					if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
					rollback;
				else
					commit;
				end if
				destroy luo_calcola_documento_euro
			end if
			
			ll_handle = tv_1.finditem( NextTreeItem! ,ll_handle )

		loop
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if

end if
end event

type dw_filtro from uo_std_dw within w_tes_off_ven
event ue_key pbm_dwnkey
event ue_reset ( )
integer x = 41
integer y = 128
integer width = 1129
integer height = 2492
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_tes_off_ven_ricerca_tv"
boolean border = false
end type

event ue_key;if key = keyenter! then
	wf_imposta_ricerca()
end if

end event

event itemchanged;
choose case dwo.name
	case "livello_1"
		setitem(getrow(),"livello_2", "N")
		setitem(getrow(),"livello_3", "N")
		settaborder("livello_2", 0)
		settaborder("livello_3", 0)
		if data <> "N" then
			settaborder("livello_2", 250)
		end if
		
	case "livello_2"
		setitem(getrow(),"livello_3", "N")
		settaborder("livello_3", 0)
		if data <> "N" then
			settaborder("livello_3", 260)
		end if
				
end choose

end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_cerca"
		wf_imposta_ricerca()
		
	case "b_memorizza_filtro"
		guo_functions.uof_memorizza_filtro(dw_filtro, "OFFVEN")
		
	case "b_leggi_filtro"
		guo_functions.uof_leggi_filtro(dw_filtro, "OFFVEN")
		
	case "b_cancella_filtro"
		guo_functions.uof_cancella_filtro(dw_filtro, "OFFVEN")
		
	case "b_cerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_filtro, "cod_cliente")
		
	case "b_cerca_contatto"
		guo_ricerca.uof_ricerca_contatto(dw_filtro, "cod_contatto")
		
	case "b_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_filtro, "cod_prodotto")
		
end choose
end event

type dw_tes_off_ven_det_3 from uo_cs_xx_dw within w_tes_off_ven
integer x = 1280
integer y = 128
integer width = 2738
integer height = 1380
integer taborder = 100
boolean bringtotop = true
string dataobject = "d_tes_off_ven_det_3"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_aspetto_beni"
		setnull(s_cs_xx.parametri.parametro_s_1)
		window_open(w_aspetto_beni, 0)
		dw_tes_off_ven_det_3.setitem(dw_tes_off_ven_det_3.getrow(), "aspetto_beni", s_cs_xx.parametri.parametro_s_1)
		
end choose
end event

type dw_tes_off_ven_det_4 from uo_cs_xx_dw within w_tes_off_ven
integer x = 1280
integer y = 144
integer width = 3035
integer height = 1328
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_tes_off_ven_det_4"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_nota_fissa

choose case this.getcolumnname()
	
	case "nota_testata"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_cliente")
			s_cs_xx.parametri.parametro_s_2 = "%"
			s_cs_xx.parametri.parametro_s_3 = "S"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "T"
			s_cs_xx.parametri.parametro_data_1 = dw_tes_off_ven_det_1.getitemdatetime(dw_tes_off_ven_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_off_ven_det_4.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then
					if len(this.gettext()) > 0 then
						this.setcolumn("nota_testata")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else
						this.setcolumn("nota_testata")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if
		
	case "nota_piede"
		if key = keyF1!  and keyflags = 1 then		
			s_cs_xx.parametri.parametro_s_1 = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_cliente")
			s_cs_xx.parametri.parametro_s_2 = "%"
			s_cs_xx.parametri.parametro_s_3 = "S"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "P"
			s_cs_xx.parametri.parametro_data_1 = dw_tes_off_ven_det_1.getitemdatetime(dw_tes_off_ven_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_off_ven_det_4.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then	
					if len(this.gettext()) > 0 then					
						this.setcolumn("nota_piede")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else	
						this.setcolumn("nota_piede")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if	
		
end choose

end event

type dw_tes_off_ven_det_1 from uo_cs_xx_dw within w_tes_off_ven
integer x = 1285
integer y = 124
integer width = 3104
integer height = 2108
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_tes_off_ven_det_1"
boolean controlmenu = true
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
				
	string ls_messaggio, ls_flag_dest_merce_fat, ls_cod_valuta, ls_null, ls_cod_cliente, ls_nota_testata, ls_nota_piede, ls_nota_video, ls_nota_old
			
	dec{4} ld_sconto
	
	datetime ldt_data_registrazione
	
	integer	li_ret


   setnull(ls_null)

	choose case i_colname
			
		case "data_registrazione"
				ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
				f_cambio_ven(ls_cod_valuta, datetime(date(data)))
			
		case "cod_cliente"
			
			if i_coltext<>"" and not isnull(i_coltext) then
				
				li_ret = wf_get_info_cliente_contatto(i_rownbr, i_coltext, 0, ls_messaggio)
				if li_ret < 0 or li_ret=1 then
					g_mb.error(ls_messaggio)
					this.setitem(i_rownbr, i_colname, ls_null)
					this.SetColumn ( i_colname )
					return 1
				end if
				
				if f_cerca_destinazione(dw_tes_off_ven_det_2, i_rownbr, data, "C", ls_null, ls_messaggio) = -1 then
					g_mb.error("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
				end if
				
			else
				//pulisci campi
				wf_pulisci_campi_cliente_contatto(i_rownbr, 0)
			end if
			
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			if guo_functions.uof_get_note_fisse(i_coltext, ls_null, ls_null, "OFF_VEN", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if

	//-----------------------------------------------------------
	case "cod_contatto"
		
		if data<>"" and not isnull(data) then
			
			li_ret = wf_get_info_cliente_contatto(i_rownbr, i_coltext, 1, ls_messaggio)
			
			if li_ret < 0 or li_ret=1 then
				g_mb.error(ls_messaggio)
				this.setitem(i_rownbr, i_colname, ls_null)
				this.SetColumn ( i_colname )
				return 1
			end if
		else
			//pulisci campi
			wf_pulisci_campi_cliente_contatto(i_rownbr, 1)
		end if
			
	//-----------------------------------------------------------
	case "cod_des_cliente"
		
         ls_cod_cliente = this.getitemstring(i_rownbr, "cod_cliente")
		if f_cerca_destinazione(dw_tes_off_ven_det_2, i_rownbr, ls_cod_cliente, "C", data, ls_messaggio) = -1 then
			g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
		end if
		
	//-----------------------------------------------------------
      case "cod_valuta"
			
		ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
		f_cambio_ven(data, ldt_data_registrazione)
		
		
   end choose
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   string ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, &
          ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
          ls_partita_iva, ls_cod_fiscale, ls_cod_contatto, ls_flag_dest_merce_fat


   if this.getrow() > 0 then
      ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
      ls_cod_contatto = this.getitemstring(this.getrow(), "cod_contatto")
      select anag_clienti.rag_soc_1,
             anag_clienti.rag_soc_2,
             anag_clienti.indirizzo,
             anag_clienti.frazione,
             anag_clienti.cap,
             anag_clienti.localita,
             anag_clienti.provincia,
             anag_clienti.telefono,
             anag_clienti.fax,
             anag_clienti.telex,
             anag_clienti.partita_iva,
             anag_clienti.cod_fiscale
      into   :ls_rag_soc_1,    
             :ls_rag_soc_2,
             :ls_indirizzo,    
             :ls_frazione,
             :ls_cap,
             :ls_localita,
             :ls_provincia,
             :ls_telefono,
             :ls_fax,
             :ls_telex,
             :ls_partita_iva,
             :ls_cod_fiscale
      from   anag_clienti
      where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_clienti.cod_cliente = :ls_cod_cliente;

      if sqlca.sqlcode = 0 then
         if not isnull(ls_cod_cliente) then
            dw_tes_off_ven_det_2.modify("st_cod_cliente.text='" + ls_cod_cliente + "'")
         else
            ls_cod_cliente = ""
            dw_tes_off_ven_det_2.modify("st_cod_cliente.text=''")
         end if
         if not isnull(ls_rag_soc_1) then
            dw_tes_off_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
         else
            dw_tes_off_ven_det_2.modify("st_rag_soc_1.text=''")
         end if
         if not isnull(ls_rag_soc_2) then
            dw_tes_off_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
         else
            dw_tes_off_ven_det_2.modify("st_rag_soc_2.text=''")
         end if
         if not isnull(ls_indirizzo) then
            dw_tes_off_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
         else
            dw_tes_off_ven_det_2.modify("st_indirizzo.text=''")
         end if
         if not isnull(ls_frazione) then
            dw_tes_off_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
         else
            dw_tes_off_ven_det_2.modify("st_frazione.text=''")
         end if
         if not isnull(ls_cap) then
            dw_tes_off_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
         else
            dw_tes_off_ven_det_2.modify("st_cap.text=''")
         end if
         if not isnull(ls_localita) then
            dw_tes_off_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
         else
            dw_tes_off_ven_det_2.modify("st_localita.text=''")
         end if
         if not isnull(ls_provincia) then
            dw_tes_off_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
         else
            dw_tes_off_ven_det_2.modify("st_provincia.text=''")
         end if
         if not isnull(ls_telefono) then
            dw_tes_off_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
         else
            dw_tes_off_ven_det_2.modify("st_telefono.text=''")
         end if
         if not isnull(ls_fax) then
            dw_tes_off_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
         else
            dw_tes_off_ven_det_2.modify("st_fax.text=''")
         end if
         if not isnull(ls_telex) then
            dw_tes_off_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
         else
            dw_tes_off_ven_det_2.modify("st_telex.text=''")
            end if
         if not isnull(ls_partita_iva) then
            dw_tes_off_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
         else
            dw_tes_off_ven_det_2.modify("st_partita_iva.text=''")
         end if
         if not isnull(ls_cod_fiscale) then
            dw_tes_off_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
         else
            dw_tes_off_ven_det_2.modify("st_cod_fiscale.text=''")
         end if

//---------------------------- Modfica Nicola ------------------------------------------
				ls_flag_dest_merce_fat = "S"
				f_po_loaddddw_dw(this, &
									  "cod_des_cliente", &
									  sqlca, &
									  "anag_des_clienti", &
									  "cod_des_cliente", &
									  "rag_soc_1", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_dest_merce_fat = '" + ls_flag_dest_merce_fat + "'")

//----------------------------- Fine Modifica ------------------------------------------	

         select anag_contatti.cod_contatto
         into   :ls_cod_contatto
         from   anag_contatti
         where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_contatti.cod_cliente = :ls_cod_cliente;
   
         if sqlca.sqlcode = 0 then 
            dw_tes_off_ven_det_2.modify("st_cod_contatto.text='" + ls_cod_contatto + "'")
         else
            dw_tes_off_ven_det_2.modify("st_cod_contatto.text=''")
         end if         
      else
         select anag_contatti.rag_soc_1,
                anag_contatti.rag_soc_2,
                anag_contatti.indirizzo,
                anag_contatti.frazione,
                anag_contatti.cap,
                anag_contatti.localita,
                anag_contatti.provincia,
                anag_contatti.telefono,
                anag_contatti.fax,
                anag_contatti.telex,
                anag_contatti.partita_iva,
                anag_contatti.cod_fiscale
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale
         from   anag_contatti
         where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_contatti.cod_contatto = :ls_cod_contatto;

         dw_tes_off_ven_det_2.modify("st_cod_cliente.text=''")

         if sqlca.sqlcode = 0 then
            if not isnull(ls_cod_contatto) then
               dw_tes_off_ven_det_2.modify("st_cod_contatto.text='" + ls_cod_contatto + "'")
            else
               dw_tes_off_ven_det_2.modify("st_cod_contatto.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               dw_tes_off_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_off_ven_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_off_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_off_ven_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_off_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_off_ven_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_off_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_off_ven_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_off_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_off_ven_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_off_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_off_ven_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_off_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_off_ven_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_off_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_off_ven_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_off_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_off_ven_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_off_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_off_ven_det_2.modify("st_telex.text=''")
               end if
            if not isnull(ls_partita_iva) then
               dw_tes_off_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               dw_tes_off_ven_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               dw_tes_off_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               dw_tes_off_ven_det_2.modify("st_cod_fiscale.text=''")
            end if
         else
            dw_tes_off_ven_det_2.modify("st_cod_cliente.text=''")
            dw_tes_off_ven_det_2.modify("st_cod_contatto.text=''")
            dw_tes_off_ven_det_2.modify("st_rag_soc_1.text=''")
            dw_tes_off_ven_det_2.modify("st_rag_soc_2.text=''")
            dw_tes_off_ven_det_2.modify("st_indirizzo.text=''")
            dw_tes_off_ven_det_2.modify("st_frazione.text=''")
            dw_tes_off_ven_det_2.modify("st_cap.text=''")
            dw_tes_off_ven_det_2.modify("st_localita.text=''")
            dw_tes_off_ven_det_2.modify("st_provincia.text=''")
            dw_tes_off_ven_det_2.modify("st_telefono.text=''")
            dw_tes_off_ven_det_2.modify("st_fax.text=''")
            dw_tes_off_ven_det_2.modify("st_telex.text=''")
            dw_tes_off_ven_det_2.modify("st_partita_iva.text=''")
            dw_tes_off_ven_det_2.modify("st_cod_fiscale.text=''")
         end if
      end if
   end if
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_tes_off_ven_det_1.object.b_ricerca_cliente.enabled = false
	dw_tes_off_ven_det_3.object.b_ricerca_aspetto_beni.enabled = false
	dw_tes_off_ven_det_1.object.b_ricerca_banca.enabled = false
	dw_tes_off_ven_det_1.object.b_ricerca_contatto.enabled = false
	
	ib_new = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_off_ven, ls_cod_operatore, ls_cod_deposito, ls_errore

   select con_off_ven.cod_tipo_off_ven
   into :ls_cod_tipo_off_ven
   from con_off_ven
   where con_off_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode = 0 then
		this.setitem(this.getrow(), "cod_tipo_off_ven", ls_cod_tipo_off_ven)
		this.setitem(this.getrow(), "data_registrazione", datetime(today()))
	end if

	select cod_operatore
	into :ls_cod_operatore
	from tab_operatori_utenti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		flag_default = 'S';
			 
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
		pcca.error = c_fatal
		return
	elseif sqlca.sqlcode = 0 then
		this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
	end if
	
	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
	if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----

	dw_tes_off_ven_det_1.object.b_ricerca_banca.enabled = true
	dw_tes_off_ven_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_off_ven_det_1.object.b_ricerca_contatto.enabled = true
	dw_tes_off_ven_det_3.object.b_ricerca_aspetto_beni.enabled = true
	ib_new = true
	
	setitem(getrow(), "num_revisione", 0)
	dw_tes_off_ven_det_4.setitem(dw_tes_off_ven_det_1.getrow(), "flag_doc_suc_tes", "I")
	dw_tes_off_ven_det_4.setitem(dw_tes_off_ven_det_1.getrow(), "flag_st_note_tes", "I")	
	dw_tes_off_ven_det_4.setitem(dw_tes_off_ven_det_1.getrow(), "flag_doc_suc_pie", "I")
	dw_tes_off_ven_det_4.setitem(dw_tes_off_ven_det_1.getrow(), "flag_st_note_pie", "I")		
	
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if i_insave > 0 then
	
	if this.getrow() > 0 then
		
		if isnull(this.getitemstring(this.getrow(), "cod_contatto")) and isnull(this.getitemstring(this.getrow(), "cod_cliente")) then
			g_mb.error("Attenzione", "Inserire Codice Contatto o Codice Cliente!")
			pcca.error = c_fatal
			return
		end if
		
		if isnull(this.getitemstring(this.getrow(), "cod_deposito")) or this.getitemstring(this.getrow(), "cod_deposito")="" then
			g_mb.error("Attenzione", "Il campo Deposito è obbligatorio!")
			pcca.error = c_fatal
			return
		end if
		
		if isnull(this.getitemstring(this.getrow(), "cod_valuta")) or this.getitemstring(this.getrow(), "cod_valuta")="" then
			g_mb.error("Attenzione", "Il campo Valuta è obbligatorio!")
			pcca.error = c_fatal
			return
		end if
		
	end if
	
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	if pcca.error = c_success then
		dw_tes_off_ven_det_1.object.b_ricerca_cliente.enabled = false
		dw_tes_off_ven_det_3.object.b_ricerca_aspetto_beni.enabled = false
		dw_tes_off_ven_det_1.object.b_ricerca_banca.enabled = false
		dw_tes_off_ven_det_1.object.b_ricerca_contatto.enabled = false

		
		ib_new = true
	end if
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	integer li_i
	string ls_tabella, ls_tot_documento, ls_nome_prog, ls_cod_cliente, ls_nota_testata, ls_nota_piede, ls_nota_old, ls_null, ls_nota_video
	datetime ldt_data_registrazione
	long ll_anno_registrazione, ll_num_registrazione
	uo_log_sistema luo_log
	
	
	
	for li_i = 1 to this.deletedcount()
		ll_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(li_i, "num_registrazione", delete!, true)

		luo_log = create uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca( "CANC_OFV", g_str.format("Cancellazione Testata Offerta Vendita $1-$2",ll_anno_registrazione,ll_num_registrazione))
		destroy luo_log

		ls_tabella = "det_off_ven_stat"
		ls_nome_prog = ""

		if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione,ls_nome_prog,0) = -1 then
			return 1
		end if

		delete varianti_det_off_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione varianti offerte",stopsign!)
			return 1
		end if
		
		delete det_off_ven_note		
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione note dettaglio offerta",stopsign!)
			return 1
		end if
		
		delete det_off_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione corrispondenze dettaglio offerta",stopsign!)
			return 1
		end if
      
		delete iva_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione dati iva dell'offerta." + sqlca.sqlerrtext,stopsign!)
			return 1
		end if

		delete det_off_ven_conf_variabili	
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione variabili configuratore",stopsign!)
			return 1
		end if
		
		delete comp_det_off_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione variabili configuratore",stopsign!)
			return 1
		end if
		
		ls_tabella = "det_off_ven"

		if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
		
   next

   if this.getrow() > 0 then
	ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_registrazione")
	ll_num_registrazione = this.getitemnumber(this.getrow(), "num_registrazione")
	
	if ll_anno_registrazione <> 0 then
		ls_tabella = "det_off_ven"
	
		if this.getitemstring(this.getrow(), "flag_evasione") <> "A" then
			g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", exclamation!, ok!)
			this.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
		if ib_new then
			setnull(ls_null)
			ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			
			if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "OFF_VEN", ls_null, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				
				//ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
				//if isnull(ls_nota_old) then ls_nota_old = ""
				//this.setitem(this.getrow(), "nota_testata", ls_nota_testata + ls_nota_old )
				
				//ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
				//if isnull(ls_nota_old) then ls_nota_old = ""
				//this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
				//testata ------
				ls_nota_old = ""
				dw_tes_off_ven_det_4.setcolumn( "nota_testata" )
				dw_tes_off_ven_det_4.selecttextall()
				ls_nota_old=dw_tes_off_ven_det_4.selectedtext()
				if isnull(ls_nota_old) then ls_nota_old = ""
				dw_tes_off_ven_det_4.replacetext(ls_nota_testata + char(10) + char(13) + ls_nota_old)
				dw_tes_off_ven_det_4.accepttext( )
				
				//piede ------
				ls_nota_old = ""
				dw_tes_off_ven_det_4.setcolumn( "nota_piede" )
				dw_tes_off_ven_det_4.selecttextall()
				ls_nota_old=dw_tes_off_ven_det_4.selectedtext()
				if isnull(ls_nota_old) then ls_nota_old = ""
				dw_tes_off_ven_det_4.replacetext(ls_nota_piede +  char(10) + char(13) + ls_nota_old)
				dw_tes_off_ven_det_4.accepttext( )
			end if
			
			/*
			if f_crea_nota_fissa(ls_cod_cliente, ls_null, "OFF_VEN", ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) <> -1 then
				//testata ------
				ls_nota_old = ""
				dw_tes_off_ven_det_4.setcolumn( "nota_testata" )
				dw_tes_off_ven_det_4.selecttextall()
				ls_nota_old=dw_tes_off_ven_det_4.selectedtext()
				if isnull(ls_nota_old) then ls_nota_old = ""
				dw_tes_off_ven_det_4.replacetext(ls_nota_testata + char(10) + char(13) + ls_nota_old)
				dw_tes_off_ven_det_4.accepttext( )
				
				//piede ------
				ls_nota_old = ""
				dw_tes_off_ven_det_4.setcolumn( "nota_piede" )
				dw_tes_off_ven_det_4.selecttextall()
				ls_nota_old=dw_tes_off_ven_det_4.selectedtext()
				if isnull(ls_nota_old) then ls_nota_old = ""
				dw_tes_off_ven_det_4.replacetext(ls_nota_piede +  char(10) + char(13) + ls_nota_old)
				dw_tes_off_ven_det_4.accepttext( )
			end if
			*/
			// aggiungo nodo all'albero
			wf_aggiungi_offerta(0, ll_anno_registrazione, ll_num_registrazione)
		end if
		
     end if
	  
	//parent.event pc_menu_calcola()
		
   end if
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_tes_off_ven_det_1.object.b_ricerca_banca.enabled = true
	dw_tes_off_ven_det_1.object.b_ricerca_contatto.enabled = true
	dw_tes_off_ven_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_off_ven_det_3.object.b_ricerca_aspetto_beni.enabled = true
	ib_new = false
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione
datetime ldt_data_scadenza


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
		ll_anno_registrazione = f_anno_esercizio()
      if ll_anno_registrazione > 0 then
         this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
      else
			g_mb.messagebox("Offerte Clienti","Impostare l'anno di esercizio in parametri aziendali")
		end if
      select con_off_ven.num_registrazione
      into   :ll_num_registrazione
      from   con_off_ven
      where  con_off_ven.cod_azienda = :s_cs_xx.cod_azienda;
		choose case sqlca.sqlcode
			case 0
				ll_num_registrazione ++
			case 100
				g_mb.messagebox("Offerte di Vendita","Errore in assegnazione numero offerta: verificare di aver impostato i parametri offerte di vendita. ~r~nIl salvataggio verrà effettuato ugualmente")
			case else
				g_mb.messagebox("Offerte di Vendita","Errore in assegnazione numero offerta.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verrà effettuato ugualmente")
		end choose
		
		ll_max_registrazione = 0
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= ll_num_registrazione then ll_num_registrazione = ll_max_registrazione + 1
		end if
		
      this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
      update con_off_ven
      set    con_off_ven.num_registrazione = :ll_num_registrazione
      where  con_off_ven.cod_azienda = :s_cs_xx.cod_azienda;
    end if
	 
	 // stefanop 28/09/2011: funzione 13 spec. Gruppo Gibus - Data scadenza automatica +60G
	 ldt_data_scadenza = this.getitemdatetime(ll_i, "data_scadenza")
	 if isnull(ldt_data_scadenza) then
		
		ldt_data_scadenza = datetime(RelativeDate(date( this.getitemdatetime(ll_i, "data_registrazione")), 60), 00:00:00)
		this.setitem(ll_i, "data_scadenza", ldt_data_scadenza)
		
	 end if
	 // ----
next      
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(iss_record) or isnull(iss_record) then return

ll_errore = retrieve(s_cs_xx.cod_azienda, iss_record.anno_registrazione,iss_record.num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

wf_retrieve_ordini_vendita(iss_record.anno_registrazione, iss_record.num_registrazione)
end event

event buttonclicked;call super::buttonclicked;change_dw_current()

choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_off_ven_det_1, "cod_cliente")
		
	case "b_ricerca_contatto"
		guo_ricerca.uof_ricerca_contatto(dw_tes_off_ven_det_1, "cod_contatto")
		
	case "b_ricerca_banca"
		guo_ricerca.uof_ricerca_banca_clifor(dw_tes_off_ven_det_1, "cod_banca_clien_for")
		
	case "b_assegna_num"
		wf_assegna_numerazione(row)
		
end choose
end event

event updateend;call super::updateend;boolean lb_flag_AOV
long ll_row, ll_num_registrazione
integer li_anno_registrazione
string	ls_cod_tipo_off_ven, ls_cod_documento


wf_update_note()

guo_functions.uof_get_parametro_azienda( "AOV", lb_flag_AOV)

//se lb_flag_AOV è TRUE allora l'offerta deve essere assegnata (se ancora non lo è stata)
//-----------------------------------------------------------------------------------------------------------
if lb_flag_AOV then

	ll_row = getrow()
	if ll_row>0 then
		
		li_anno_registrazione = getitemnumber(ll_row, "anno_registrazione")
		ll_num_registrazione = getitemnumber(ll_row, "num_registrazione")
		ls_cod_tipo_off_ven = getitemstring(ll_row, "cod_tipo_off_ven")
		ls_cod_documento = getitemstring(ll_row, "cod_documento")
		
		if	not isnull(li_anno_registrazione) and li_anno_registrazione > 1900 and not isnull(ll_num_registrazione) and ll_num_registrazione > 0 and &
			not isnull(ls_cod_tipo_off_ven) and ls_cod_tipo_off_ven <> "" and (isnull(ls_cod_documento) or ls_cod_documento="") then
			
			//procedi pure all'assegnazione diretta dell'offerta
			wf_assegna_numerazione(ll_row)
		end if
	end if
end if
//-----------------------------------------------------------------------------------------------------------
if rowsdeleted = 0 then
	wf_calcola_documento_dw_corrente()	
end if


end event

event doubleclicked;call super::doubleclicked;string ls_messaggio, ls_destinatari[], ls_allegati[],ls_errore, ls_oggetto
uo_outlook luo_outlook

choose case dwo.name
	case "richiedente_email"
		
		luo_outlook = create uo_outlook
		ls_destinatari[1] = this.getitemstring(row, "richiedente_email")
		
		if isnull(this.getitemstring(row, "cod_documento")) then
			ls_oggetto = "Preventivo numero " + string( getitemnumber(row, "anno_registrazione") )  + "-" + string( getitemnumber(row, "num_registrazione") ) + " del " + string(  getitemdatetime(row, "data_registrazione"),"dd/mm/yyyy" ) 
		else
			ls_oggetto = "Preventivo numero " +getitemstring(row, "cod_documento") + "-" + string( getitemnumber(row, "anno_documento") ) 
			ls_oggetto += "/" + getitemstring(row, "numeratore_documento") + "-" + string( getitemnumber(row, "num_documento") ) + " rev:" + string( getitemnumber(row, "num_revisione") ) 
			ls_oggetto += " del " + string(  getitemdatetime(row, "data_offerta"),"dd/mm/yyyy" ) 
		end if
		
		if luo_outlook.uof_invio_outlook(0, "M", ls_oggetto, "", ls_destinatari[], ls_allegati[], false, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		end if
		destroy luo_outlook
		
end choose
end event

type dw_tes_ord_ven from datawindow within w_tes_off_ven
integer x = 1257
integer y = 1820
integer width = 3154
integer height = 800
integer taborder = 120
boolean bringtotop = true
string title = "none"
string dataobject = "d_tes_off_ven_ordini_tv"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;s_open_parm	lstr_parm

if row>0 then
	choose case dwo.name
		case "b_ordine"
			
			lstr_parm.anno_registrazione = dw_tes_ord_ven.getitemnumber(row, "det_ord_ven_anno_registrazione")
			lstr_parm.num_registrazione = dw_tes_ord_ven.getitemnumber(row, "det_ord_ven_num_registrazione")
			
			if lstr_parm.anno_registrazione > 0 and lstr_parm.num_registrazione > 0 then
				
				window_open_parm(w_tes_ord_ven_tv,-1,lstr_parm)
//				wf_apri_ordine(ll_anno_reg_ord, ll_num_reg_ord)
			else
				return
			end if
			
	end choose
end if
end event

type dw_tes_off_ven_det_2 from uo_cs_xx_dw within w_tes_off_ven
integer x = 1280
integer y = 120
integer width = 3131
integer height = 1960
integer taborder = 90
string dataobject = "d_tes_off_ven_det_2"
boolean border = false
end type

