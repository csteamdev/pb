﻿$PBExportHeader$w_tes_off_ven_preventivo_excel.srw
forward
global type w_tes_off_ven_preventivo_excel from w_cs_xx_principale
end type
type cb_report from commandbutton within w_tes_off_ven_preventivo_excel
end type
type dw_tes_off_ven_preventivo_excel from uo_cs_xx_dw within w_tes_off_ven_preventivo_excel
end type
end forward

global type w_tes_off_ven_preventivo_excel from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1294
integer height = 1096
string title = "Preventivo Offerte Vendita Excel"
cb_report cb_report
dw_tes_off_ven_preventivo_excel dw_tes_off_ven_preventivo_excel
end type
global w_tes_off_ven_preventivo_excel w_tes_off_ven_preventivo_excel

type variables
long il_num_registrazione, il_anno


OLEObject myoleobject
end variables

forward prototypes
public subroutine wf_report ()
public function string uof_nome_cella (long fi_riga, long fi_colonna)
public function long uof_salva (string fs_foglio)
end prototypes

public subroutine wf_report ();
long ll_colonna, ll_riga, ll_errore, ll_i, ll_prima_riga, ll_ultima_riga, ll_ret
string ls_path, ls_rag_soc_azienda, ls_foglio, ls_descrizione, ls_cod_prodotto, ls_nota, ls_valuta, ls_formato,&
		ls_col_e, ls_col_f, ls_col_g, ls_col_h, ls_prova, ls_prodotto, ls_scontato 
dec{4}  ld_importo,  ld_prezzo_listino, ld_quantita, ld_prova, ld_sconto_1, ld_sconto_2, ld_spese_sicurezza
dec{6} ld_x1,  ld_intermedio
dec ld_valore
dec{2} ld_arrotondamento
boolean lb_controllo
datastore lds_det_off_ven

ld_valore = 0.790513

g_mb.messagebox("APICE", "Non cliccare sul foglio di lavoro di excel. ~r~nAttendere il termine.")
myoleobject = CREATE OLEObject
ll_colonna = 1
ll_riga = 1

select rag_soc_1 
into :ls_rag_soc_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

select  stringa
into :ls_path
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
cod_parametro = 'LO1' and
flag_parametro = 'S'; 

select spese_sicurezza, cod_valuta
into :ld_spese_sicurezza, :ls_valuta
from tes_off_ven
where cod_azienda =  :s_cs_xx.cod_azienda and
anno_registrazione = :il_anno and 
num_registrazione = :il_num_registrazione;

select formato
into :ls_formato
from tab_valute
where cod_azienda =  :s_cs_xx.cod_azienda and
cod_valuta = :ls_valuta;

lds_det_off_ven = create datastore

lds_det_off_ven.dataobject = "d_det_off_ven_excel"

if lds_det_off_ven.settransobject(sqlca) < 0 then
		g_mb.messagebox("APICE","Si è verificato un errore nella creazione del datastore lds_det_off_ven ")
		destroy lds_det_off_ven
	return 
end if

if lds_det_off_ven.retrieve(s_cs_xx.cod_azienda, il_anno, il_num_registrazione ) < 0 then
	g_mb.messagebox("APICE","Si è verificato un errore nella creazione del datastore lds_det_off_ven ")
	destroy lds_det_off_ven
end if
// calcolo il totale dell'importo materiali da listino prezzo  (somma prezzi per q.tà)
ld_importo = 0
for ll_i = 1 to lds_det_off_ven.rowcount()	
		ld_prova = lds_det_off_ven.getitemnumber(ll_i, "prezzo_um") * lds_det_off_ven.getitemnumber(ll_i, "quantita_um")
		if ld_prova = 0 then 
				ld_prova = lds_det_off_ven.getitemnumber(ll_i, "prezzo_vendita") * lds_det_off_ven.getitemnumber(ll_i, "quan_offerta")
		end if
		ld_importo =ld_importo + ld_prova
next

if ld_spese_sicurezza = 0 or ld_importo = 0 then
	g_mb.messagebox("APICE","Si è verificato un errore: le spese complessive sicurezza sono uguale a zero oppure~n il totale di tutte le righe sono uguali a zero. ")
	//destroy lds_det_off_ven
	ld_x1 = 0
else
	ld_spese_sicurezza = ld_spese_sicurezza/ld_importo
	ld_intermedio = 1 + ld_spese_sicurezza 
	ld_x1 = ld_valore * ld_spese_sicurezza / ld_intermedio
end if

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	g_mb.messagebox("APICE","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return
end if

ls_foglio = "foglio1"
myoleobject.Visible = True
myoleobject.Workbooks.Add
//myoleobject.sheets("foglio1").Protect
myoleobject.sheets(ls_foglio).PageSetup.PrintGridlines = True
myoleobject.sheets(ls_foglio).PageSetup.orientation = 2 
/// COSì è ORIZZONTALE 1 X VERTICALE
myoleobject.sheets(ls_foglio).PageSetup.leftmargin = 20
myoleobject.sheets(ls_foglio).PageSetup.rightmargin = 10
myoleobject.sheets(ls_foglio).PageSetup.topmargin = 20
myoleobject.sheets(ls_foglio).PageSetup.bottommargin = 20

myoleobject.sheets(ls_foglio).PageSetup.FooterMargin = 5



//Worksheets(1).PageSetup
//    .LeftMargin = Application.InchesToPoints(0.5)
//    .RightMargin = Application.InchesToPoints(0.75)
//    .TopMargin = Application.InchesToPoints(1.5)
//    .BottomMargin = Application.InchesToPoints(1)
//    .HeaderMargin = Application.InchesToPoints(0.5)
//    .FooterMargin = Application.InchesToPoints(0.5)

//myoleobject.worksheets(ls_foglio).Shapes.SelectAll = true
ls_path = s_cs_xx.volume + ls_path
lb_controllo = FileExists(ls_path)
if lb_controllo then
	myoleobject.worksheets(ls_foglio).Shapes.AddPicture(ls_path, True, True, 10, 10, 650, 70)
else
	g_mb.messagebox("APICE","Il logo non è presente, impostare correttamente il parametro 'LO1'!")
end if
///INSERISCO IL LOGO E FORMATTO IL FOGLIO


myoleobject.worksheets(ls_foglio).PageSetup.CenterFooter = "&P di &N"
//
//myoleobject.worksheets(ls_foglio).PageSetup.LeftHeader= "&G"
//myoleobject.worksheets(ls_foglio).PageSetup.CentertHeaderPicture.FileName = 'C:\prova.bmp'
//myoleobject.worksheets(ls_foglio).PageSetup.HeaderPicture.FileName = 'C:\prova.bmp'
//myoleobject.worksheets(ls_foglio).PageSetup.HeaderPicture.Height = 275.25
//myoleobject.worksheets(ls_foglio).PageSetup.HeaderPicture.Width = 463.5

myoleobject.sheets(ls_foglio).columns(ll_colonna + 1).wraptext = true
myoleobject.sheets(ls_foglio).cells(8, ll_colonna).value = "DESCRIZIONE INTERVENTO:"
ll_riga = 10
// formatto le celle di intestazione e el relative colonne

myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "cod."
myoleobject.sheets(ls_foglio).columns(ll_colonna).columnwidth = "15"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "descrizione"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 1).columnwidth = "40"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = "prezzo listino ~n (P.L.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 2).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 2).numberformat= "#.###.##0,00"
//ls_formato non va bene
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = "oneri sicurezza ~n (O.S.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 3).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 3).numberformat= "#.###.##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = "prezzo unitario ~n (P.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 4).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 4).numberformat= "#.###.##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = "1° sconto su ~n (P.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna+ 5).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 5).numberformat= "##0,00%"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 6).value = "2° sconto su ~n (P.U.)"
myoleobject.sheets(ls_foglio).columns(ll_colonna+ 6).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 6).numberformat= "##0,00%"

myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 7).value = "Q.tà"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 7).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 7).numberformat= "##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "Importo oneri ~n sicurezza"
myoleobject.sheets(ls_foglio).columns(ll_colonna+ 8).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 8).numberformat= "#.###.##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "Importo"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 9).columnwidth = "10"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 9).numberformat= "#.###.##0,00"
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value =""
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value
myoleobject.worksheets(ls_foglio).rows(ll_riga).wraptext = true
myoleobject.worksheets(ls_foglio).rows(ll_riga).HorizontalAlignment = "1"
for ll_i = 0 to 9
	myoleobject.sheets(ls_foglio).columns(ll_colonna + ll_i).font.size = "9"
	myoleobject.worksheets(ls_foglio).cells(ll_riga, ll_colonna + ll_i).borders(3).Weight = "3"
	myoleobject.worksheets(ls_foglio).cells(ll_riga, ll_colonna + ll_i).borders(4).Weight = "3"
next
// inserisco il dettaglio delle righe dell'offerta
ll_riga++
ll_prima_riga = ll_riga
for ll_i = 1 to lds_det_off_ven.rowcount()
	ls_cod_prodotto = lds_det_off_ven.getitemstring(ll_i, "cod_prodotto")
	if isnull(ls_cod_prodotto) then
		ls_cod_prodotto = ""
	end if
	myoleobject.worksheets(ls_foglio).cells(ll_riga, ll_colonna).HorizontalAlignment = "3"
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = ls_cod_prodotto
	ls_descrizione = lds_det_off_ven.getitemstring(ll_i, "des_prodotto")
	if isnull(ls_descrizione) then
		ls_descrizione = ""
	end if
	ls_nota = lds_det_off_ven.getitemstring(ll_i, "nota_dettaglio")
		if isnull(ls_nota) then
		ls_nota = ""
	end if
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = ls_descrizione +""+ls_nota
	ld_prezzo_listino = lds_det_off_ven.getitemnumber(ll_i, "prezzo_um")
	if ld_prezzo_listino = 0 then
		ld_prezzo_listino = lds_det_off_ven.getitemnumber(ll_i, "prezzo_vendita")
	end if
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = ld_prezzo_listino
	if ld_x1 = 0 then
		myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = 0
	else
		ld_arrotondamento = ld_prezzo_listino * ld_x1
		myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = ld_arrotondamento
	end if
	ld_arrotondamento = ld_prezzo_listino - (ld_prezzo_listino * ld_x1)
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = ld_arrotondamento
	ld_sconto_1 = lds_det_off_ven.getitemnumber(ll_i, "sconto_1")
	ld_sconto_2 = lds_det_off_ven.getitemnumber(ll_i, "sconto_2")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = ld_sconto_1/100
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 6).value = ld_sconto_2/100
	ld_quantita = lds_det_off_ven.getitemnumber(ll_i, "quantita_um")
	if ld_quantita = 0 then
		ld_quantita = lds_det_off_ven.getitemnumber(ll_i, "quan_offerta")
	
	end if
	
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 7).value = ld_quantita
	ld_arrotondamento = ld_quantita * ld_prezzo_listino * ld_x1
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = ld_arrotondamento
	ls_col_e = uof_nome_cella(ll_riga, ll_colonna + 4)
	ls_col_f = uof_nome_cella(ll_riga, ll_colonna + 5)
	ls_col_g = uof_nome_cella(ll_riga, ll_colonna + 6)
	ls_col_h = uof_nome_cella(ll_riga, ll_colonna + 7)
	ls_prova = "=("+ls_col_e+"-(("+ls_col_e+"*"+ls_col_f+"))-(("+ls_col_e+"-("+ls_col_e+"*"+ls_col_f+"))*"+ls_col_g+"))*"+ls_col_h
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "=("+ls_col_e+"-(("+ls_col_e+"*"+ls_col_f+"))-(("+ls_col_e+"-("+ls_col_e+"*"+ls_col_f+"))*"+ls_col_g+"))*"+ls_col_h
	ll_riga++
next
for ll_i = 0 to 9	
	myoleobject.worksheets(ls_foglio).cells(ll_riga - 1, ll_colonna + ll_i).borders(4).Weight = "3"
next
ll_ultima_riga = ll_riga - 1
ll_riga++

// inserisco i totali finali
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "1)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "SOMMANO MATERIALI"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "=somma("+uof_nome_cella(ll_prima_riga, ll_colonna + 9)+":"+uof_nome_cella(ll_ultima_riga, ll_colonna + 9)+")"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "2)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TRASPORTI E NOLI"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "0"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "3)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERE DI ASSISTENZA MURARIA 0%"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "0"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "4)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "ONERI PER LA SICUREZZA"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "=somma("+uof_nome_cella(ll_prima_riga, ll_colonna + 8)+":"+uof_nome_cella(ll_ultima_riga, ll_colonna + 8)+")"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).borders(4).Weight = "3"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "5)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "IMPORTO COMPLESSIVO MATERIALI"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "=somma("+uof_nome_cella(ll_ultima_riga + 2, ll_colonna + 8)+":"+uof_nome_cella(ll_ultima_riga + 5, ll_colonna + 8)+")"
ll_riga++
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "MANODOPERA"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = "Ore"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = "€/ora"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = "totale"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = "sconto"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).value = "totale scontato"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.1)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TECNICO DI CONCETTO LAUREATO"
ll_i = ll_riga
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.2)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TECNICO DI CONCETTO DIPLOMATO"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.3)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERAIO SPECIALIZZATO V° livello"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.4)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERAIO SPECIALIZZATO IV° livello"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6.5)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "OPERAIO COMUNE"

for ll_i = ll_i to ll_riga 
	
	ls_prodotto = "="+uof_nome_cella(ll_i , ll_colonna + 2)+"*"+uof_nome_cella(ll_i , ll_colonna + 3)
	ls_scontato = "="+uof_nome_cella(ll_i , ll_colonna + 4)+"-("+uof_nome_cella(ll_i , ll_colonna + 4)+"*"+uof_nome_cella(ll_i , ll_colonna + 5)+")"
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 2).value = "0"
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 3).value = "0,00"
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 4).value = ls_prodotto
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 5).value = 13.53/100
	myoleobject.sheets(ls_foglio).cells(ll_i, ll_colonna + 8).value = ls_scontato

next

myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 8).borders(4).Weight = "3"
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "6)"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "TOTALE IMPORTO MANODOPERA"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "=somma("+uof_nome_cella(ll_riga - 5, ll_colonna + 8)+":"+uof_nome_cella(ll_riga -1, ll_colonna + 8)+")"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).borders(4).Weight = "3"
ll_riga++
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "IMPORTO TOTALE"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 9).value = "="+uof_nome_cella(ll_riga - 2 , ll_colonna + 9)+"+"+uof_nome_cella(ll_riga - 10 , ll_colonna + 9)
myoleobject.sheets(ls_foglio).rows(ll_riga).font.size = "12"
myoleobject.sheets(ls_foglio).rows(ll_riga).Font.Bold = True
ll_riga++

ll_ret = uof_salva(ls_foglio)
if ll_ret = -1 then
	return
end if
g_mb.messagebox("APICE","Report allegato alla nota dell'offerta di vendita!")
destroy lds_det_off_ven
return


end subroutine

public function string uof_nome_cella (long fi_riga, long fi_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fi_colonna > 26 then
	ll_resto = mod(fi_colonna, 26)
	ll_i = long(fi_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fi_colonna = ll_resto 
end if

if fi_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fi_colonna] + string(fi_riga)
end if	

return ls_char
end function

public function long uof_salva (string fs_foglio);

string ls_path, ls_cod_nota, ls_db
blob lb_file
long ll_progressivo,  ll_prog_mimetype, li_risposta, ll_ret, ll_i, ll_controllo
boolean lb_prova

transaction sqlcb

//fare il salvataggio 
select stringa
into   :ls_path
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_parametro = 'TDD';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Attenzione: inserire il parametro aziendale TDD! " + sqlca.sqlerrtext)	
	myoleobject.quit
	return -1
end if

ls_path = s_cs_xx.volume + ls_path + "\preventivo"+string(now(),"hh_mm_ss")+".xls"

myoleobject.sheets(fs_foglio).SaveAs(ls_path)
myoleobject.quit

lb_file = f_carica_blob(ls_path)
ll_ret = len(lb_file)

// se ll_ret <= 0 allora non è riuscito ad aprire il file perchè excel non è stato ancora chiuso
if ll_ret <= 0 then
	ll_controllo = 0
	//provo per 20 volte ad aprire il file poi lancio errore
	for ll_i = 1 to 40
		lb_file = f_carica_blob(ls_path)
		ll_ret = len(lb_file)
		if ll_ret > 0 then
			ll_controllo = 1
			exit
		end if
	next
	if ll_controllo = 0 then
		g_mb.messagebox( "APICE", "Errore durante l'inserimento del file nel database, ritentare l'elaborazione. "  + sqlca.sqlerrtext)
		rollback;
		FileDelete(ls_path)
		return -1
	end if
end if

FileDelete(ls_path)

ls_db = f_db()

//	//*** so per certo che è un xsl, quindi lo cerco
	select prog_mimetype
	into   :ll_prog_mimetype
	from   tab_mimetype
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       estensione = 'XLS';

	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "APICE", "Errore durante la ricerca del mimetype: " + sqlca.sqlerrtext)
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then  // ** faccio insert
		
		select max(prog_mimetype)
		into   :ll_prog_mimetype
		from   tab_mimetype
		where  cod_azienda = :s_cs_xx.cod_azienda;
		
		if isnull(ll_prog_mimetype) then ll_prog_mimetype = 0
		ll_prog_mimetype = ll_prog_mimetype + 1
		
	   INSERT INTO tab_mimetype ( cod_azienda,   
                                 prog_mimetype,   
                                 estensione,   
                                 applicazione,   
                                 mimetype )  
                        VALUES ( :s_cs_xx.cod_azienda,   
                                 :ll_prog_mimetype,   
                                 'XLS',   
                                 'Microsoft excel',   
                                 'application/msexcel' )  ;		
	end if
	
	
	select cod_nota
	into   :ls_cod_nota
	from   tes_off_ven_note
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :il_anno and 
			 num_registrazione = :il_num_registrazione and 
			 cod_nota = 'P01' ;

	if sqlca.sqlcode = 0 then
		ll_ret = g_mb.messagebox("APICE", "Attenzione il preventivo presente nelle note sarà sostituito da quello appena elaborato.~n Desideri sovrascriverlo? ", Exclamation!, YesNo!, 2)
		//ll_ret = 1 //si
		if ll_ret = 2 then //no
			rollback;
			return -1
		end if
	end if
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "APICE", "Errore durante la ricerca del documento: " + sqlca.sqlerrtext)
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then  // ** faccio insert
		
	INSERT INTO tes_off_ven_note ( cod_azienda,   
                                 anno_registrazione,   
                                 num_registrazione,   
                                 cod_nota,
											des_nota, 
											prog_mimetype)  
                        VALUES ( :s_cs_xx.cod_azienda,   
                                 :il_anno,   
                                 :il_num_registrazione,   
                                 'P01'  ,
											'Foglio excel del preventivo',
											:ll_prog_mimetype)  ;		
	
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante l'inserimento del documento: " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
	
	end if

	if ls_db = "MSSQL" then

					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob tes_off_ven_note
					set        note_esterne = :lb_file
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :il_anno and 
								  num_registrazione = :il_num_registrazione and 
								  cod_nota = 'P01' 
					using      sqlcb;
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox( "APICE", "Errore durante l'inserimento del blod del documento: " + sqlca.sqlerrtext)
						rollback;
						destroy sqlcb;
						return -1
					end if
					destroy sqlcb;
					
				else

					updateblob tes_off_ven_note
					set        note_esterne = :lb_file
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :il_anno and 
								  num_registrazione = :il_num_registrazione and 
								  cod_nota = 'P01' ;
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox( "APICE", "Errore durante l'inserimento del blod del documento: " + sqlca.sqlerrtext)
						rollback;
						destroy sqlcb;
						return -1
					end if
								  
				end if


	commit;
	


return 1


end function

on w_tes_off_ven_preventivo_excel.create
int iCurrent
call super::create
this.cb_report=create cb_report
this.dw_tes_off_ven_preventivo_excel=create dw_tes_off_ven_preventivo_excel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report
this.Control[iCurrent+2]=this.dw_tes_off_ven_preventivo_excel
end on

on w_tes_off_ven_preventivo_excel.destroy
call super::destroy
destroy(this.cb_report)
destroy(this.dw_tes_off_ven_preventivo_excel)
end on

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_tes_off_ven_preventivo_excel, &
//                 "anno_registrazione", &
//                 sqlca, &
//                 "tes_off_ven", &
//                 "anno_registrazione", &
//                 "anno_registrazione", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_po_loaddddw_dw(dw_con_off_ven, &
//                 "num_registrazione", &
//                 sqlca, &
//                 "tab_tipi_off_ven", &
//                 "cod_tipo_off_ven", &
//                 "des_tipo_off_ven", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
end event

event pc_setwindow;call super::pc_setwindow;dw_tes_off_ven_preventivo_excel.set_dw_key("cod_azienda")
dw_tes_off_ven_preventivo_excel.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end event

type cb_report from commandbutton within w_tes_off_ven_preventivo_excel
integer x = 731
integer y = 700
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;
long  ll_i

dw_tes_off_ven_preventivo_excel.acceptText()

ll_i = dw_tes_off_ven_preventivo_excel.getrow()
il_anno  = dw_tes_off_ven_preventivo_excel.getitemnumber(ll_i,"anno_registrazione")
il_num_registrazione  = dw_tes_off_ven_preventivo_excel.getitemnumber(ll_i,"num_registrazione")

wf_report()
end event

type dw_tes_off_ven_preventivo_excel from uo_cs_xx_dw within w_tes_off_ven_preventivo_excel
integer y = 20
integer width = 1166
integer height = 660
integer taborder = 10
string dataobject = "d_tes_off_ven_preventivo_excel"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event itemchanged;call super::itemchanged;//string ls_null
//setnull(ls_null)
//if i_extendmode then
//	
//	choose case i_colname
//	
//	case "num_registrazione"
//			
//			setitem( getrow(), "cod_lista_dist", ls_null)
//			
//			f_po_loaddddw_dw( dw_tes_off_ven_preventivo_excel, &
//									"num_registrazione", &
//									sqlca, &
//									"tes_off_ven", &
//									"num_registrazione", &
//									"num_registrazione", &
//									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = '" + i_coltext + "' ")
//	end choose
//	
//end if
//
////
end event

