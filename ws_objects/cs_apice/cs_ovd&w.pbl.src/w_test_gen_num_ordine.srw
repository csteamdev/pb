﻿$PBExportHeader$w_test_gen_num_ordine.srw
forward
global type w_test_gen_num_ordine from w_cs_xx_principale
end type
type plb_1 from picturelistbox within w_test_gen_num_ordine
end type
type dp_1 from datepicker within w_test_gen_num_ordine
end type
type cb_1 from commandbutton within w_test_gen_num_ordine
end type
end forward

global type w_test_gen_num_ordine from w_cs_xx_principale
integer width = 1115
integer height = 1064
plb_1 plb_1
dp_1 dp_1
cb_1 cb_1
end type
global w_test_gen_num_ordine w_test_gen_num_ordine

type variables

end variables

on w_test_gen_num_ordine.create
int iCurrent
call super::create
this.plb_1=create plb_1
this.dp_1=create dp_1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.plb_1
this.Control[iCurrent+2]=this.dp_1
this.Control[iCurrent+3]=this.cb_1
end on

on w_test_gen_num_ordine.destroy
call super::destroy
destroy(this.plb_1)
destroy(this.dp_1)
destroy(this.cb_1)
end on

type plb_1 from picturelistbox within w_test_gen_num_ordine
integer x = 41
integer y = 164
integer width = 1001
integer height = 776
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
end type

type dp_1 from datepicker within w_test_gen_num_ordine
integer x = 480
integer y = 32
integer width = 567
integer height = 100
integer taborder = 20
boolean border = true
borderstyle borderstyle = stylelowered!
date maxdate = Date("2999-12-31")
date mindate = Date("1800-01-01")
datetime value = DateTime(Date("2011-11-10"), Time("12:04:50.000000"))
integer textsize = -10
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
integer calendarfontweight = 400
boolean todaysection = true
boolean todaycircle = true
end type

type cb_1 from commandbutton within w_test_gen_num_ordine
integer x = 27
integer y = 24
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;
plb_1.additem(string(guo_functions.uof_gen_num_ordine(date(dp_1.value))))
end event

