﻿$PBExportHeader$w_report_packing_list.srw
$PBExportComments$Finestra Report Packing list
forward
global type w_report_packing_list from w_cs_xx_principale
end type
type st_1 from statictext within w_report_packing_list
end type
type dw_selezione from uo_cs_xx_dw within w_report_packing_list
end type
type dw_report from uo_cs_xx_dw within w_report_packing_list
end type
type cb_annulla from commandbutton within w_report_packing_list
end type
type cb_report from commandbutton within w_report_packing_list
end type
end forward

global type w_report_packing_list from w_cs_xx_principale
integer width = 3593
integer height = 2132
string title = "Report Packing List"
st_1 st_1
dw_selezione dw_selezione
dw_report dw_report
cb_annulla cb_annulla
cb_report cb_report
end type
global w_report_packing_list w_report_packing_list

event pc_setwindow;call super::pc_setwindow;string ls_des_azienda

dw_report.ib_dw_report = true

select rag_soc_1
  into :ls_des_azienda
  from aziende
 where cod_azienda = :s_cs_xx.cod_azienda;
 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Descrizione Azienda non trovata")
end if

if sqlca.sqlcode = 0 then
	dw_report.object.st_azienda.text = ls_des_azienda
end if	

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
                            
save_on_close(c_socnosave)
iuo_dw_main = dw_report

end event

on w_report_packing_list.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_report
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_report
end on

on w_report_packing_list.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_annulla)
destroy(this.cb_report)
end on

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(		dw_selezione, &
//                 	  "tipo_doc_acq", &
//                 		sqlca, &
//                    "tab_tipi_ord_acq", &
//                    "cod_tipo_ord_acq", &
//                    "des_tipo_ord_acq", &
//                    "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
end event

event clicked;call super::clicked;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type st_1 from statictext within w_report_packing_list
integer x = 3150
integer y = 36
integer width = 379
integer height = 92
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_packing_list
integer y = 20
integer width = 3131
integer height = 360
integer taborder = 40
string dataobject = "d_sel_report_packing_list"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_null

date ld_null

long ll_null

setnull(ls_null)
setnull(ld_null)
setnull(ll_null)

dw_selezione.setitem(1, "cod_cliente", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "data_consegna_da", ld_null)
dw_selezione.setitem(1, "data_consegna_a", ld_null)
dw_selezione.setitem(1, "flag_evasione", 'A')
dw_selezione.setitem(1, "num_pack_list", 0)
end event

event itemchanged;call super::itemchanged;//if i_extendmode then
//	choose case i_colname
//		case "flag_blocco"
//			if i_coltext = "S" then
//				dw_selezione.setitem(dw_selezione.getrow(),"flag_blocco_righe","T")
//				dw_selezione.Object.flag_blocco_righe.Protect = 1
//			else
//				dw_selezione.Object.flag_blocco_righe.Protect = 0
//			end if
//	end choose
//end if
end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_packing_list
integer x = 23
integer y = 396
integer width = 3520
integer height = 1632
integer taborder = 50
string dataobject = "d_righe_report_packing_list"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_annulla from commandbutton within w_report_packing_list
integer x = 3173
integer y = 296
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null

date   ld_null

long   ll_null

setnull(ls_null)
setnull(ld_null)
setnull(ll_null)

//dw_selezione.insertrow(0)
dw_selezione.setitem(1, "cod_cliente", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "data_consegna_da", ld_null)
dw_selezione.setitem(1, "data_consegna_a", ld_null)
dw_selezione.setitem(1, "flag_evasione", 'A')
dw_selezione.setitem(1, "num_pack_list", 0)
end event

type cb_report from commandbutton within w_report_packing_list
integer x = 3173
integer y = 196
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_cliente_sel, ls_consegna_da, ls_consegna_a, ls_provincia, ls_flag_evasione, ls_cod_cliente, ls_des_cliente, ls_flag_evasione_sel, &
       ls_testata, ls_da, ls_a, ls_cod_prodotto, ls_des_prodotto, ls_provincia_spedizione, ls_cod_des_cliente, ls_cliente_appo,ls_flag_spedito
		 
long   ll_prog_riga, ll_num_pack_list, ll_num_collo, ll_i, ll_anno_ordine, ll_a, ll_num_ordine, ll_count, ll_num_pack_list_old, ll_num_pack_list_sel, ll_controllo, ll_contr_test
       
		 
dec{4} ld_quan_pack_list

datetime ldt_data_consegna, ldt_da_data_consegna, ldt_a_data_consegna, &
         ldt_data_da_sel, ldt_data_a_sel, ldt_data_ord_cliente
			
uo_calcola_sconto iuo_calcola_sconto

dw_report.Reset() 
dw_report.setredraw(false)
dw_selezione.accepttext()

ls_cod_cliente_sel = dw_selezione.getitemstring( 1, "cod_cliente")
ldt_data_da_sel = dw_selezione.getitemdatetime( 1, "data_da")
ldt_data_a_sel = dw_selezione.getitemdatetime( 1, "data_a")
ldt_da_data_consegna = dw_selezione.getitemdatetime( 1, "data_consegna_da")
ldt_a_data_consegna = dw_selezione.getitemdatetime( 1, "data_consegna_a")
ls_flag_evasione_sel = dw_selezione.getitemstring( 1, "flag_evasione")
ll_num_pack_list_sel = dw_selezione.getitemnumber( 1, "num_pack_list")
ls_flag_spedito = dw_selezione.getitemstring( 1, "flag_spedito")
if ls_flag_spedito = 'I' then ls_flag_spedito = '%'

ll_i = 0

declare cu_testata dynamic cursor for sqlsa;

ls_testata = " select anno_registrazione, " &
           + "        num_registrazione,  " &
			  + "        data_registrazione, " &
			  + "        data_consegna,      " &
			  + "        cod_cliente,        " &
			  + "        flag_evasione,      " &
			  + "        provincia      " &
			  + " from   tes_ord_ven " &
			  + " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_cliente_sel) then
	ls_testata = ls_testata + " and cod_cliente = '" + ls_cod_cliente_sel + "'"
end if

ls_da = string(ldt_data_da_sel, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_data_da_sel) then
	ls_testata = ls_testata + " and data_ord_cliente >= '" + ls_da + "'"
end if

ls_a = string(ldt_data_a_sel, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_data_a_sel) then
	ls_testata = ls_testata + " and data_ord_cliente <= '" + string(ls_a) + "'"
end if

ls_consegna_da = string(ldt_da_data_consegna, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_da_data_consegna) then
	ls_testata = ls_testata + " and data_consegna >= '" + ls_consegna_da + "'"
end if

ls_consegna_a = string(ldt_a_data_consegna, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_da_data_consegna) then
	ls_testata = ls_testata + " and data_ord_cliente <= '" + string(ls_consegna_a) + "'"
end if

choose case ls_flag_evasione_sel
	case 'A'
		ls_testata += " and flag_evasione = 'A' "
	case 'E'
		ls_testata += " and flag_evasione = 'E' "		
	case 'P'
		ls_testata += " and (flag_evasione = 'P' or flag_evasione = 'A') "		
end choose

ls_testata += " and ( select count(*) " &
            + "       from det_ord_ven_pack_list " &
				+ "       where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " &
				+ "             anno_registrazione = tes_ord_ven.anno_registrazione and " &
				+ "             num_registrazione = tes_ord_ven.num_registrazione "
				
if not isnull(ll_num_pack_list_sel)	and ll_num_pack_list_sel > 0 then
	ls_testata += " and num_pack_list = " + string(ll_num_pack_list_sel) 
end if

ls_testata += " ) > 0 "

ls_testata += " ORDER BY cod_cliente ASC, anno_registrazione ASC, num_registrazione ASC "

prepare sqlsa from :ls_testata;
open dynamic cu_testata;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_testata. " + sqlca.sqlerrtext)
	rollback;
	return
end if
ll_contr_test = 0
ls_cliente_appo = ""
ll_num_pack_list_old = 0

do while 1=1
	
   fetch cu_testata into :ll_anno_ordine, 
								 :ll_num_ordine, 
								 :ldt_data_ord_cliente, 
								 :ldt_data_consegna, 
								 :ls_cod_cliente,
								 :ls_flag_evasione, 
								 :ls_provincia;
								 
	if sqlca.sqlcode = 100 then exit
	
	st_1.text = string(ll_anno_ordine)+"/"+string(ll_num_ordine)
	
	select rag_soc_1
	into   :ls_des_cliente
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
			 
	
			 
	if ls_cliente_appo <> ls_cod_cliente  then
		ll_controllo = 0
		ll_contr_test = 0
		ls_cliente_appo = ls_cod_cliente	
		SELECT count (det_ord_ven.anno_registrazione)
		INTO :ll_controllo
   	FROM   det_ord_ven,   
          	det_ord_ven_pack_list  
   	WHERE ( det_ord_ven.cod_azienda = det_ord_ven_pack_list.cod_azienda ) and  
      	   ( det_ord_ven.anno_registrazione = det_ord_ven_pack_list.anno_registrazione ) and  
        		 ( det_ord_ven.num_registrazione = det_ord_ven_pack_list.num_registrazione ) and  
        	 	( det_ord_ven.prog_riga_ord_ven = det_ord_ven_pack_list.prog_riga_ord_ven )  and
          	 det_ord_ven.anno_registrazione = :ll_anno_ordine and 
			 	 det_ord_ven.num_registrazione = :ll_num_ordine and
			 	 det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			  	det_ord_ven_pack_list.flag_spedito like :ls_flag_spedito;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_dettagli " + sqlca.sqlerrtext)
			rollback;
			return
		end if
	
		
		if ll_controllo <= 0 then continue	  
		
		ll_contr_test = 1
		ll_i = dw_report.insertrow(0)	
		dw_report.setitem(ll_i, "tipo_riga", 100)
		if isnull(ls_des_cliente) then ls_des_cliente = ""
		if isnull(ls_des_prodotto) then ls_des_prodotto = ""
		dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
		dw_report.setitem(ll_i, "des_cliente", ls_des_cliente)
	else
	
			SELECT count (det_ord_ven.anno_registrazione)
		INTO :ll_controllo
   	FROM   det_ord_ven,   
          	det_ord_ven_pack_list  
   	WHERE ( det_ord_ven.cod_azienda = det_ord_ven_pack_list.cod_azienda ) and  
      	   ( det_ord_ven.anno_registrazione = det_ord_ven_pack_list.anno_registrazione ) and  
        		 ( det_ord_ven.num_registrazione = det_ord_ven_pack_list.num_registrazione ) and  
        	 	( det_ord_ven.prog_riga_ord_ven = det_ord_ven_pack_list.prog_riga_ord_ven )  and
          	 det_ord_ven.anno_registrazione = :ll_anno_ordine and 
			 	 det_ord_ven.num_registrazione = :ll_num_ordine and
			 	 det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			  	det_ord_ven_pack_list.flag_spedito like :ls_flag_spedito;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_dettagli " + sqlca.sqlerrtext)
			rollback;
			return
		end if
	
		
		if ll_controllo <= 0 then continue	  
		
		if ll_contr_test <> 1 then
			ll_contr_test = 1 
			ll_i = dw_report.insertrow(0)	
			dw_report.setitem(ll_i, "tipo_riga", 100)
			if isnull(ls_des_cliente) then ls_des_cliente = ""
			if isnull(ls_des_prodotto) then ls_des_prodotto = ""
			dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
			dw_report.setitem(ll_i, "des_cliente", ls_des_cliente)
		end if

	end if	
	//**********modifica Claudia 18/07/05 aggiunto controllo su flag_spedito
	declare cu_pacchi cursor for 
   SELECT det_ord_ven_pack_list.prog_riga_ord_ven, 
          det_ord_ven_pack_list.num_pack_list,   
          det_ord_ven_pack_list.num_collo,   
          det_ord_ven_pack_list.quan_pack_list,   
          det_ord_ven.cod_prodotto  
   FROM   det_ord_ven,   
          det_ord_ven_pack_list  
   WHERE ( det_ord_ven.cod_azienda = det_ord_ven_pack_list.cod_azienda ) and  
         ( det_ord_ven.anno_registrazione = det_ord_ven_pack_list.anno_registrazione ) and  
         ( det_ord_ven.num_registrazione = det_ord_ven_pack_list.num_registrazione ) and  
         ( det_ord_ven.prog_riga_ord_ven = det_ord_ven_pack_list.prog_riga_ord_ven )  and
           det_ord_ven.anno_registrazione = :ll_anno_ordine and 
			  det_ord_ven.num_registrazione = :ll_num_ordine and
			  det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			  det_ord_ven_pack_list.flag_spedito like :ls_flag_spedito	  
   ORDER BY det_ord_ven_pack_list.num_pack_list ASC, cod_prodotto ASC;

	open cu_pacchi;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_dettagli " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	ll_count = 0
	ll_num_pack_list_old = 0

	do while 1=1
		
		fetch cu_pacchi into :ll_prog_riga, 
          						:ll_num_pack_list,   
          						:ll_num_collo,   
                           :ld_quan_pack_list,   
          						:ls_cod_prodotto ;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore nella fetch del cursore cu_dettagli " + sqlca.sqlerrtext)
			rollback;
			return
		end if		
		
	if sqlca.sqlcode = 100 then 
//			if ll_controllo = 0 then
//				ll_a = dw_report.deleterow(ll_i)	
//				ll_controllo = -1
//				if ll_a < 1 then
//					messagebox("APICE","Errore nella creazione del report " )
//					rollback;
//					return
//				end if
//				exit
//			else
				exit
		end if
//		end if
	
		//ll_controllo = 1
		if not isnull(ll_num_pack_list_sel)	and ll_num_pack_list_sel > 0 then
			if ll_num_pack_list_sel <> ll_num_pack_list then continue
		end if	
	
		select des_prodotto
		into   :ls_des_prodotto
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ordine and
				 num_registrazione = :ll_num_ordine and
				 prog_riga_ord_ven = :ll_prog_riga;		
				 
		if ls_flag_evasione = "E" then ls_flag_evasione = "Evaso"
		if ls_flag_evasione = "P" then ls_flag_evasione = "Parziale"
		if ls_flag_evasione = "A" then ls_flag_evasione = "Aperto"		
		
		if isnull(ls_provincia) or ls_provincia = "" then
			
			select cod_des_cliente
			into   :ls_cod_des_cliente
			from   tes_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_ordine and
					 num_registrazione = :ll_num_ordine;
			
			if isnull(ls_cod_des_cliente) or ls_cod_des_cliente = "" then
			
				select provincia
				into   :ls_provincia
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cliente = :ls_cod_cliente;
						
			else
				
				select provincia
				into   :ls_provincia
				from   anag_des_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_cliente = :ls_cod_cliente and
						 cod_des_cliente = :ls_cod_des_cliente;
				
			end if
					 
		end if
		
		if isnull(ll_num_pack_list) then ll_num_pack_list = 0
				
		ll_count ++
		
		ll_i = dw_report.insertrow(0)
		
		if ll_count = 1 then
			ll_num_pack_list_old = ll_num_pack_list
			dw_report.setitem(ll_i, "tipo_riga", 1)
		else
			if ll_num_pack_list <> ll_num_pack_list_old then
				ll_num_pack_list_old = ll_num_pack_list
				dw_report.setitem(ll_i, "tipo_riga", 2)
			else			
				dw_report.setitem(ll_i, "tipo_riga", 3)
			end if										
		end if
				
		if isnull(ls_des_cliente) then ls_des_cliente = ""
		if isnull(ls_des_prodotto) then ls_des_prodotto = ""
		dw_report.setitem(ll_i, "numero_ordine", ll_num_ordine)
		dw_report.setitem(ll_i, "anno_ordine", ll_anno_ordine)
		dw_report.setitem(ll_i, "data_ordine", ldt_data_ord_cliente)
		dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
		dw_report.setitem(ll_i, "des_cliente", ls_des_cliente)
		dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
		dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
		dw_report.setitem(ll_i, "stato_ordine", ls_flag_evasione)
		dw_report.setitem(ll_i, "riga_ordine", ll_prog_riga)
		dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
		dw_report.setitem(ll_i, "num_pack_list", ll_num_pack_list)
		dw_report.setitem(ll_i, "num_collo", ll_num_collo)
		dw_report.setitem(ll_i, "quantita", ld_quan_pack_list)
		dw_report.setitem(ll_i, "provincia_spedizione", ls_provincia)

	loop
	close cu_pacchi;	
	
	
loop
close cu_testata;	
rollback;


dw_report.setredraw(true)

dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.preview.rulers = 'Yes'
st_1.text = ""

dw_report.change_dw_current()
end event

