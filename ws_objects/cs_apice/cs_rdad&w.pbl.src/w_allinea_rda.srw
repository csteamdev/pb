﻿$PBExportHeader$w_allinea_rda.srw
forward
global type w_allinea_rda from window
end type
type sle_azienda from singlelineedit within w_allinea_rda
end type
type mle_log from multilineedit within w_allinea_rda
end type
type hpb_1 from hprogressbar within w_allinea_rda
end type
type cb_1 from commandbutton within w_allinea_rda
end type
end forward

global type w_allinea_rda from window
integer width = 3525
integer height = 2068
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
sle_azienda sle_azienda
mle_log mle_log
hpb_1 hpb_1
cb_1 cb_1
end type
global w_allinea_rda w_allinea_rda

forward prototypes
public function integer wf_elabora ()
public subroutine wf_log (string as_messaggio)
end prototypes

public function integer wf_elabora ();string				ls_sql, ls_cod_azienda, ls_errore, ls_cod_valuta, ls_cod_operaio, ls_sql_2, ls_flag_gen_ord_acq, ls_flag_stato_autorizzato
datastore			lds_data, lds_data_2
long					ll_tot, ll_index, ll_num_reg, ll_tot_2, ll_index_2
integer				li_anno_reg
datetime			ldt_data_richiesta
boolean				lb_almeno_uno_a_N, lb_almeno_uno_a_S


mle_log.text = "Inizio elaborazione ...~r~n"

hpb_1.position = 0

ls_cod_azienda = sle_azienda.text

if ls_cod_azienda="" or isnull(ls_cod_azienda) then
	wf_log("Devi indicare il codice azienda per cui effettuare l'elaborazione!~r~n")
	
	return -1
end if

ls_sql = "select distinct anno_registrazione, num_registrazione "+&
			"from det_rda "+&
			"where cod_azienda='"+ls_cod_azienda+"' "+&
			"order by anno_registrazione asc, num_registrazione asc "

ls_errore = ""

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot<0 then
	wf_log(ls_errore + "~r~n")
	return -1
end if

hpb_1.maxposition = ll_tot

for ll_index=1 to ll_tot
	Yield()
	
	li_anno_reg = lds_data.getitemnumber(ll_index, 1)
	ll_num_reg = lds_data.getitemnumber(ll_index, 2)
	
	hpb_1.stepit()
	
	wf_log("Elaborazione righe della RDA n° "+string(li_anno_reg)+"/"+string(ll_num_reg)+"~r~n")
	
	ls_sql_2 = "select prog_riga_rda, data_richiesta, flag_gen_ord_acq, cod_valuta, cod_operaio " + &
					"from det_rda " + &
					"where cod_azienda='" + ls_cod_azienda + "' and " + &
								"anno_registrazione=" + string(li_anno_reg) + " and " + &
								"num_registrazione=" + string(ll_num_reg) + " " + &
					"order by prog_riga_rda asc "
	
	ls_errore = ""

	ll_tot_2 = guo_functions.uof_crea_datastore(lds_data_2, ls_sql_2, ls_errore)


	if ll_tot_2<0 then
		destroy lds_data
		
		wf_log(ls_errore + "~r~n")
		return -1
	end if
	
	ls_flag_stato_autorizzato = "A"
	lb_almeno_uno_a_N = false
	lb_almeno_uno_a_S = false
	
	for ll_index_2=1 to ll_tot_2
		
		ls_flag_gen_ord_acq = lds_data_2.getitemstring(ll_index_2, 3)
		
		if ll_index_2=1 then
			ldt_data_richiesta = lds_data_2.getitemdatetime(ll_index_2, 2)
			ls_cod_valuta = lds_data_2.getitemstring(ll_index_2, 4)
			ls_cod_operaio = lds_data_2.getitemstring(ll_index_2, 5)
		end if
		
		if ls_flag_gen_ord_acq="S" then
			lb_almeno_uno_a_S = true
		else
			lb_almeno_uno_a_N = true
		end if
		
	next
	
	destroy lds_data_2
	
	if lb_almeno_uno_a_S and lb_almeno_uno_a_N then
		ls_flag_stato_autorizzato = "P"
		
	elseif not lb_almeno_uno_a_N and lb_almeno_uno_a_S then
		ls_flag_stato_autorizzato = "E"
		
	end if
	
	update tes_rda
        set		data_registrazione			=:ldt_data_richiesta,
            		cod_valuta						=:ls_cod_valuta,
            		cod_operaio					=:ls_cod_operaio,
				flag_stato_autorizzato	=:ls_flag_stato_autorizzato
        where 	cod_azienda=:ls_cod_azienda and
                		anno_registrazione = :li_anno_reg and
                		num_registrazione = :ll_num_reg;
	
	if sqlca.sqlcode<0 then
		wf_log("Errore i update data_reg-valuta-operaio: "+ sqlca.sqlerrtext+ "~r~n")
		destroy lds_data
		destroy lds_data_2
		rollback;
		return -1
		
	else
		commit;
	end if
	
	
next

destroy lds_data

wf_log("Elaborazione terminata!")

return 0
end function

public subroutine wf_log (string as_messaggio);

mle_log.text = as_messaggio

mle_log.scroll(mle_log.linecount())

mle_log.SetRedraw(TRUE)

return
end subroutine

on w_allinea_rda.create
this.sle_azienda=create sle_azienda
this.mle_log=create mle_log
this.hpb_1=create hpb_1
this.cb_1=create cb_1
this.Control[]={this.sle_azienda,&
this.mle_log,&
this.hpb_1,&
this.cb_1}
end on

on w_allinea_rda.destroy
destroy(this.sle_azienda)
destroy(this.mle_log)
destroy(this.hpb_1)
destroy(this.cb_1)
end on

type sle_azienda from singlelineedit within w_allinea_rda
integer x = 526
integer y = 40
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "A01"
borderstyle borderstyle = stylelowered!
end type

type mle_log from multilineedit within w_allinea_rda
integer x = 46
integer y = 308
integer width = 3397
integer height = 1624
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "Pronto!"
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
boolean hideselection = false
end type

type hpb_1 from hprogressbar within w_allinea_rda
integer x = 59
integer y = 212
integer width = 3378
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type cb_1 from commandbutton within w_allinea_rda
integer x = 50
integer y = 40
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "ALLINEA RDA"
end type

event clicked;

wf_elabora()
end event

