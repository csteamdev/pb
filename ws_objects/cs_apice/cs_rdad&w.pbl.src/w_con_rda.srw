﻿$PBExportHeader$w_con_rda.srw
$PBExportComments$Finestra Gestione Parametri RDA
forward
global type w_con_rda from w_cs_xx_principale
end type
type dw_con_rda from uo_cs_xx_dw within w_con_rda
end type
end forward

global type w_con_rda from w_cs_xx_principale
integer width = 2816
integer height = 1564
string title = "Parametri RDA"
dw_con_rda dw_con_rda
end type
global w_con_rda w_con_rda

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_rda, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (flag_tipo_det_acq = 'M' or flag_tipo_det_acq = 'N') ")

f_po_loaddddw_dw(dw_con_rda, &
                 "cod_tipo_det_acq_des", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (flag_tipo_det_acq = 'M' or flag_tipo_det_acq = 'N') ")

f_po_loaddddw_dw(dw_con_rda, &
                 "cod_tipo_ord_acq", &
                 sqlca, &
                 "tab_tipi_ord_acq", &
                 "cod_tipo_ord_acq", &
                 "des_tipo_ord_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_con_rda, &
                 "cod_tipo_off_acq", &
                 sqlca, &
                 "tab_tipi_off_acq", &
                 "cod_tipo_off_acq", &
                 "des_tipo_off_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;dw_con_rda.set_dw_key("cod_azienda")
dw_con_rda.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end event

on w_con_rda.create
int iCurrent
call super::create
this.dw_con_rda=create dw_con_rda
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_rda
end on

on w_con_rda.destroy
call super::destroy
destroy(this.dw_con_rda)
end on

type dw_con_rda from uo_cs_xx_dw within w_con_rda
integer width = 2766
integer height = 1440
string dataobject = "d_con_rda"
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

