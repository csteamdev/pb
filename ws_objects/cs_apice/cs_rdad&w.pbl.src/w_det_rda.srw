﻿$PBExportHeader$w_det_rda.srw
forward
global type w_det_rda from w_cs_xx_principale
end type
type tab_1 from tab within w_det_rda
end type
type det_1 from userobject within tab_1
end type
type dw_det_rda_lista from uo_cs_xx_dw within det_1
end type
type det_1 from userobject within tab_1
dw_det_rda_lista dw_det_rda_lista
end type
type det_2 from userobject within tab_1
end type
type dw_det_rda_det from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_1
dw_det_rda_det dw_det_rda_det
end type
type tab_1 from tab within w_det_rda
det_1 det_1
det_2 det_2
end type
type st_2 from statictext within w_det_rda
end type
type dw_ricerca from u_dw_search within w_det_rda
end type
end forward

global type w_det_rda from w_cs_xx_principale
integer width = 4197
integer height = 2856
string title = "Amministrazione R.D.A."
tab_1 tab_1
st_2 st_2
dw_ricerca dw_ricerca
end type
global w_det_rda w_det_rda

type variables
private:
	boolean ib_gibus = false
end variables

forward prototypes
public subroutine wf_tasti_funzione (string fs_nome_colonna)
public subroutine wf_nuova_rda ()
public subroutine wf_deposito ()
public subroutine wf_carica_fornitore (string as_cod_tessuto, string as_colore_tessuto)
end prototypes

public subroutine wf_tasti_funzione (string fs_nome_colonna);st_2.text = ""
choose case fs_nome_colonna
	case "prezzo_acquisto"
		st_2.text = "SHIFT - F1 = Storico Acq."
end choose

end subroutine

public subroutine wf_nuova_rda ();if not g_mb.confirm("Creare una nuova RDA?") then
	return
end if

// pulisco righe
tab_1.det_1.dw_det_rda_lista.reset()

this.postevent('pc_new')
end subroutine

public subroutine wf_deposito ();string ls_cod_operaio, ls_cod_deposito, ls_error

ls_cod_operaio = "" 

ls_cod_operaio = tab_1.det_1.dw_det_rda_lista.getitemstring(tab_1.det_1.dw_det_rda_lista.getrow(), "cod_operaio")

guo_functions.uof_get_stabilimento_operaio(ls_cod_operaio, ls_cod_deposito, ls_error)

 tab_1.det_1.dw_det_rda_lista.setitem(tab_1.det_1.dw_det_rda_lista.getrow(), "cod_deposito", ls_cod_deposito)
end subroutine

public subroutine wf_carica_fornitore (string as_cod_tessuto, string as_colore_tessuto);// se è presente colore tessuto e codice tessuto, allora carico anche il codice fornitore

string				ls_cod_fornitore, ls_cod_parametro

long					ll_row



ll_row = tab_1.det_2.dw_det_rda_det.getrow()

if ll_row>0 then
else
	return
end if

//sarebbe il cod. prodotto
if as_cod_tessuto="" or isnull(as_cod_tessuto) then return


//--------------------------------------------------------------------------------------------------------------------
//se esiste il parametro CSO e hai specificato il colore tessuto ma non ancora il fornitore, cerca il fornitore da tab_tessuti_locale
select cod_parametro
into   :ls_cod_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CSO';


if sqlca.sqlcode=0 and ls_cod_parametro="CSO" then
	//ESISTE il parametro aziendale CSO
	
	//campo des_specifica
	if isnull(as_colore_tessuto ) or trim(as_colore_tessuto)="" then return
	
	select cod_fornitore
	into   :ls_cod_fornitore
	from   tab_tessuti_locale
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tessuto = :as_cod_tessuto and
			 cod_non_a_magazzino = :as_colore_tessuto;
	
else
	//altrimenti prendi il fornitore abituale del prodotto ----------------------------------------------------------
	select cod_fornitore
	into   :ls_cod_fornitore
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :as_cod_tessuto;

end if

if ls_cod_fornitore="" then setnull(ls_cod_fornitore)

//se il fornitore abituale o quello della gestione tab_tessuti_locale è vuoto
//lascia quello presente
if not isnull(ls_cod_fornitore) then
	tab_1.det_2.dw_det_rda_det.setitem(ll_row,"cod_fornitore", ls_cod_fornitore)
end if


return
end subroutine

on w_det_rda.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.st_2=create st_2
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.dw_ricerca
end on

on w_det_rda.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.st_2)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[], ls_flag_amministratore, ls_cod_operaio
windowobject  l_objects[ ]

// stefanop: 19/01/2012: imposto la posizione degli oggetti
dw_ricerca.move(20,20)
tab_1.move(20, dw_ricerca.y + dw_ricerca.height + 40)
tab_1.det_1.dw_det_rda_lista.move(0,0)
tab_1.det_2.dw_det_rda_det.move(0,0)
tab_1.postevent("ue_resize")
// ----

dw_ricerca.setitem( 1, "flag_gen_ord_acq", "N")

tab_1.det_1.dw_det_rda_lista.set_dw_key("cod_azienda")
tab_1.det_1.dw_det_rda_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default + c_noretrieveonopen, &
                                 c_default)
tab_1.det_2.dw_det_rda_det.set_dw_options(sqlca, &
                                tab_1.det_1.dw_det_rda_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

// --------------------  imposto filtro ----------------------------------------- //
l_criteriacolumn[1] = "anno_registrazione"
l_criteriacolumn[2] = "num_registrazione"
l_criteriacolumn[3] = "cod_fornitore"
l_criteriacolumn[4] = "cod_operaio"
l_criteriacolumn[5] = "cod_prodotto"
l_criteriacolumn[6] = "cod_centro_costo"
l_criteriacolumn[7] = "anno_commessa"
l_criteriacolumn[8] = "num_commessa"
l_criteriacolumn[9] = "flag_autorizzato"
l_criteriacolumn[10] = "flag_gen_ord_acq"
l_criteriacolumn[11] = "cod_deposito"

l_searchtable[1] = "det_rda"
l_searchtable[2] = "det_rda"
l_searchtable[3] = "det_rda"
l_searchtable[4] = "det_rda"
l_searchtable[5] = "det_rda"
l_searchtable[6] = "det_rda"
l_searchtable[7] = "det_rda"
l_searchtable[8] = "det_rda"
l_searchtable[9] = "det_rda"
l_searchtable[10] = "det_rda"
l_searchtable[11] = "det_rda"

l_searchcolumn[1] = "anno_registrazione"
l_searchcolumn[2] = "num_registrazione"
l_searchcolumn[3] = "cod_fornitore"
l_searchcolumn[4] = "cod_operaio"
l_searchcolumn[5] = "cod_prodotto"
l_searchcolumn[6] = "cod_centro_costo"
l_searchcolumn[7] = "anno_commessa"
l_searchcolumn[8] = "num_commessa"
l_searchcolumn[9] = "flag_autorizzato"
l_searchcolumn[10] = "flag_gen_ord_acq"
l_searchcolumn[11] = "cod_deposito"

dw_ricerca.fu_wiredw(l_criteriacolumn[], tab_1.det_1.dw_det_rda_lista, l_searchtable[], l_searchcolumn[], sqlca)

select flag_collegato
into :ls_flag_amministratore
from utenti
where cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode <> 0 then ls_flag_amministratore = "N"

ib_gibus = guo_functions.uof_is_gibus()

// Per gibus, richiesto da Beatrice, il codice operaio NON deve essere bloccato
if not ib_gibus then
	if s_cs_xx.cod_utente = "CS_SYSTEM" or ls_flag_amministratore ="S" then
		tab_1.det_2.dw_det_rda_det.Object.cod_operaio.Protect=0
	else
		tab_1.det_2.dw_det_rda_det.Object.cod_operaio.Protect=1
	end if
end if

tab_1.det_1.dw_det_rda_lista.ib_dw_detail = true
tab_1.det_2.dw_det_rda_det.ib_dw_detail = true



end event

event pc_setddlb;call super::pc_setddlb;string ls_db, ls_descrizione

ls_db = f_db()
if ls_db = "ORACLE" then
	ls_descrizione = "nome || ' ' || cognome"
else
	ls_descrizione = "nome + ', ' + cognome" 
end if

f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_operaio", &
							sqlca, &
							"anag_operai", &
							"cod_operaio", &
							ls_descrizione, &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and (data_fine_rapporto > " + s_cs_xx.db_funzioni.oggi + " or data_fine_rapporto is null)")			
							
f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
							"des_deposito", &
							"cod_azienda='" + s_cs_xx.cod_azienda + "' ")
							
f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_deposito_partenza", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
							"des_deposito", &
							"cod_azienda='" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_operaio_autorizzante", &
							sqlca, &
							"anag_operai", &
							"cod_operaio", &
							ls_descrizione, &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and (data_fine_rapporto > " + s_cs_xx.db_funzioni.oggi + " or data_fine_rapporto is null)")

dw_ricerca.fu_loadcode("cod_operaio", &
							"anag_operai", &
							"cod_operaio", &
							ls_descrizione, &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and (data_fine_rapporto > " + s_cs_xx.db_funzioni.oggi + " or data_fine_rapporto is null) order by cognome, nome asc", "(Tutti)" )

f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_centro_costo", &
							sqlca, &
							"tab_centri_costo", &
							"cod_centro_costo", &
							"des_centro_costo", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")

dw_ricerca.fu_loadcode("cod_centro_costo", &
							"tab_centri_costo", &
							"cod_centro_costo", &
							"des_centro_costo", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + ")) order by des_centro_costo asc", "(Tutti)" )

f_po_loaddddw_dw(dw_ricerca, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
							"des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_deposito = 'I' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_valuta", &
							sqlca, &
							"tab_valute", &
							"cod_valuta", &
							"des_valuta", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_tipo_politica_riordino", &
							sqlca, &
							"tab_tipi_politiche_riordino", &
							"cod_tipo_politica_riordino", &
							"des_tipo_politica_riordino", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")


end event

event pc_modify;call super::pc_modify;
tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true

end event

event pc_new;call super::pc_new;
tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true

end event

event pc_save;call super::pc_save;
tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = false
tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = false

end event

event pc_view;call super::pc_view;
tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = false
tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = false

end event

event resize;/* TOLTO ANCESTORE */
tab_1.resize(newwidth - 40, newheight - tab_1.y - 20)

tab_1.event ue_resize()
end event

type tab_1 from tab within w_det_rda
event create ( )
event destroy ( )
event ue_resize ( )
integer x = 27
integer y = 716
integer width = 4101
integer height = 2016
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
det_1 det_1
det_2 det_2
end type

on tab_1.create
this.det_1=create det_1
this.det_2=create det_2
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_1.destroy
destroy(this.det_1)
destroy(this.det_2)
end on

event ue_resize();det_1.dw_det_rda_lista.resize(det_1.width, det_1.height)
det_2.dw_det_rda_det.resize(det_1.width, det_1.height)
end event

type det_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 4064
integer height = 1892
long backcolor = 12632256
string text = "Lista"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det_rda_lista dw_det_rda_lista
end type

on det_1.create
this.dw_det_rda_lista=create dw_det_rda_lista
this.Control[]={this.dw_det_rda_lista}
end on

on det_1.destroy
destroy(this.dw_det_rda_lista)
end on

type dw_det_rda_lista from uo_cs_xx_dw within det_1
integer x = 18
integer y = 8
integer width = 4023
integer height = 1872
integer taborder = 20
string dataobject = "d_det_rda_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
boolean i_allowdelete = true
boolean i_allowdrilldown = true
boolean i_allowmodify = true
boolean i_allownew = true
boolean i_allowquery = true
end type

event updatestart;call super::updatestart;//CLAUDIA 13/09/07 tolto il controllo perchè cancellando l'ultima riga dà errore getitemstatus(ll_i,0,primary!) = new!

long ll_i, ll_righe
dwitemstatus ld_status

if i_extendmode then
	for ll_i = 1 to rowcount()
		ll_righe = rowcount()
		ld_status = getitemstatus(ll_i,0,primary!) 
		
		if getitemstatus(ll_i,0,primary!) = newmodified!  then
			if isnull(getitemdatetime(ll_i,"data_richiesta")) then
				g_mb.messagebox("APICE","Data richiesta obbligatoria !!!",stopsign!)
				return 1
			end if
			
			if isnull(getitemstring(ll_i,"cod_operaio")) then
				g_mb.messagebox("APICE","Richiedente obbligatorio !!!",stopsign!)
				return 1
			end if
			
		end if
		
	next
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true
	
	wf_tasti_funzione(tab_1.det_2.dw_det_rda_det.getcolumnname())

end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	string				ls_str, ls_cod_operaio
	long					ll_row
	datetime			ldt_today
	
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true
	
	select stringa
	into :ls_str
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
		 	 cod_parametro = 'LIR';
			  
	if sqlca.sqlcode <> 0 then
		g_mb.warning("APICE","Parametro LIR non impostato in parametri Azienda")
	else
		setitem(getrow(),"cod_valuta", ls_str)
	end if
	
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM"  then
	
		select cod_operaio
		into :ls_cod_operaio
		from anag_operai
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_utente = :s_cs_xx.cod_utente;
				 
		if sqlca.sqlcode = 0 then
			setitem(getrow(),"cod_operaio",ls_cod_operaio)
			wf_deposito()
		end if
	
	end if
	
	ldt_today = datetime(today(), 00:00:00)
	setitem(getrow(), "data_richiesta", ldt_today)
	setitem(getrow(),"data_disponibilita", ldt_today)
	
	// stefnaop: 19/01/2012: controllo se esiste già una riga e prelevo i campi base da quella
	if rowcount() > 1 then
		ll_row = getrow() -1
		
		setitem(getrow(), "anno_registrazione", getitemnumber(ll_row, "anno_registrazione"))
		setitem(getrow(), "num_registrazione", getitemnumber(ll_row, "num_registrazione"))
	end if
	
	setcolumn("cod_prodotto")
end if

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = false
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = false
end if
end event

event pcd_setkey;call super::pcd_setkey;long 				ll_i, ll_num_registrazione, ll_prog_riga_rda, ll_num_registrazione_2
integer			li_anno_registrazione


for ll_i = 1 to this.rowcount()
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// COD_AZIENDA
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// ANNO_REGISTRAZIONE
	li_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
	if li_anno_registrazione = 0 or  isnull(li_anno_registrazione) then
		li_anno_registrazione = f_anno_esercizio()
		setitem(ll_i, "anno_registrazione", li_anno_registrazione)
	end if
	
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// NUM_REGISTRAZIONE
	ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione")
	
	if ll_num_registrazione = 0 or  isnull(ll_num_registrazione) then
		
		setnull(ll_num_registrazione)
		setnull(ll_num_registrazione_2)
		
		//questo per compatibilità con gestione testata albero e vecchia gestione rda
		// prelevo il massimo dal database det_rda
		select max(num_registrazione)
		into :ll_num_registrazione
		from det_rda
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :li_anno_registrazione;
		
		// prelevo il massimo dal database tes_rda
		select max(num_registrazione)
		into   :ll_num_registrazione_2
		from   tes_rda
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :li_anno_registrazione;		
		
		if isnull(ll_num_registrazione) then ll_num_registrazione=0
		if isnull(ll_num_registrazione_2) then ll_num_registrazione_2=0
		
		//prendo il + grande dei due e lo incremento di uno
		if ll_num_registrazione_2 > ll_num_registrazione then ll_num_registrazione = ll_num_registrazione_2
	
		ll_num_registrazione += 1
		
		setitem(ll_i, "num_registrazione", ll_num_registrazione)
			
		update con_rda
		set num_registrazione = :ll_num_registrazione
		where cod_azienda = :s_cs_xx.cod_azienda;
		
	end if

	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// PROG_RIGA_RDA
	ll_prog_riga_rda = this.getitemnumber(ll_i, "prog_riga_rda")
	if ll_prog_riga_rda < 2 or  isnull(ll_prog_riga_rda) then
		
		if isnull(ll_prog_riga_rda) or ll_prog_riga_rda = 0 then
			// prelevo il massimo dal database
			select max(prog_riga_rda)
			into :ll_prog_riga_rda
			from det_rda
			where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :li_anno_registrazione and
					 num_registrazione = :ll_num_registrazione;
					
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante il calcolo della progressivo riga RDA.", sqlca)
				return -1
			elseif sqlca.sqlcode = 100 or isnull(ll_prog_riga_rda) or ll_prog_riga_rda < 1 then
				ll_prog_riga_rda = 0
			end if			
		end if
		
		ll_prog_riga_rda += 10
		this.setitem(ll_i, "prog_riga_rda", ll_prog_riga_rda)
	end if
 
next      
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = false
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = false
end if
end event

event ue_key;call super::ue_key;string ls_cod_valuta, ls_cod_misura
long   ll_cont
double ld_prezzo_acquisto, ld_sconto_tot, ld_sconti[], ld_fat_conversione
datetime ldt_data_consegna
uo_condizioni_cliente luo_condizioni_cliente

choose case this.getcolumnname()
		
	case "prezzo_acquisto"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = getitemstring(getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "O"
			s_cs_xx.parametri.parametro_s_12 = "%"+ Left (getitemstring(getrow(),"des_prodotto"), 20)+"%"
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
		
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_prodotto( dw_det_rda_lista,"cod_prodotto")
		end if
		
end choose


end event

event itemchanged;call super::itemchanged;string ls_cod_parametro, ls_flag_blocco, ls_where

setnull(ls_cod_parametro)

if i_extendmode then
	
	choose case i_colname
			
		case "cod_prodotto"
			wf_carica_fornitore(i_coltext, getitemstring(i_rownbr, "des_specifica"))
			
			select flag_blocco
			into   :ls_flag_blocco
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode = 0 then
				// 17/06/2008 aggiunto segnalazione blocco prodotto su richiesta di beatrice.
				if ls_flag_blocco = "S" then
					g_mb.warning("APICE","Attenzione: prodotto bloccato da anagrafica prodotti")
				end if
			end if
			
			
			
		case "des_specifica"
			wf_carica_fornitore(getitemstring(i_rownbr, "cod_prodotto"), i_coltext)
			
			
			
		case "cod_operaio"
			wf_deposito()

			
	end choose
end if
end event

type det_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 4064
integer height = 1892
long backcolor = 12632256
string text = "Dettaglio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det_rda_det dw_det_rda_det
end type

on det_2.create
this.dw_det_rda_det=create dw_det_rda_det
this.Control[]={this.dw_det_rda_det}
end on

on det_2.destroy
destroy(this.dw_det_rda_det)
end on

type dw_det_rda_det from uo_cs_xx_dw within det_2
event ue_carica_fornitore ( string as_cod_tessuto,  string as_colore_tessuto )
integer width = 4023
integer height = 1916
integer taborder = 70
string dataobject = "d_det_rda_det"
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_carica_fornitore(string as_cod_tessuto, string as_colore_tessuto);// se è presente colore tessuto e codice tessuto, allora carico anche il codice fornitore

wf_carica_fornitore(as_cod_tessuto, as_colore_tessuto)

//string ls_cod_fornitore
//
//if not isnull(as_cod_tessuto) and len(trim(as_cod_tessuto)) > 0 and not isnull(as_colore_tessuto ) and len(trim(as_colore_tessuto)) > 0 then
//	
//	
//	select cod_fornitore
//	into   :ls_cod_fornitore
//	from   tab_tessuti_locale
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_tessuto = :as_cod_tessuto and
//			 cod_non_a_magazzino = :as_colore_tessuto;
//			 
//	if sqlca.sqlcode = 0 then
//		setitem(getrow(),"cod_fornitore",ls_cod_fornitore)
//	end if
//	
//else
//	select cod_fornitore
//	into   :ls_cod_fornitore
//	from   anag_prodotti
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_prodotto = :as_cod_tessuto;
//			 
//	if ls_cod_fornitore="" then setnull(ls_cod_fornitore)
//	setitem(getrow(),"cod_fornitore",ls_cod_fornitore)
//	
//end if			

return
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_det_rda_det,"cod_fornitore")
		
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_rda_det,"cod_prodotto")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_flag_blocco


if i_extendmode then
	
	choose case i_colname
			
		case "cod_prodotto"
			
			wf_carica_fornitore( i_coltext, getitemstring(i_rownbr, "des_specifica") )
			
			
			select flag_blocco
			into   :ls_flag_blocco
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode = 0 then
				// 17/06/2008 aggiunto segnalazione blocco prodotto su richiesta di beatrice.
				if ls_flag_blocco = "S" then
					g_mb.messagebox("APICE","Attenzione: prodotto bloccato da anagrafica prodotti")
				end if
				
			end if
			
			
			
			
		case "des_specifica"
			wf_carica_fornitore(getitemstring(i_rownbr, "cod_prodotto"), i_coltext )
			
			
			
		case "cod_operaio"
			wf_deposito()

			
	end choose
end if
end event

event pcd_modify;call super::pcd_modify;//if i_extendmode then
//	wf_tasti_funzione(dw_det_rda_det.getcolumnname())
//end if
end event

event pcd_new;call super::pcd_new;

if i_extendmode then
	
	string ls_str, ls_cod_operaio, ls_flag_amministratore
	long ll_row
	datetime ldt_today
	
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true
	
	select stringa
	into :ls_str
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
		 	 cod_parametro = 'LIR';
			  
	if sqlca.sqlcode <> 0 then
		g_mb.warning("APICE","Parametro LIR non impostato in parametri Azienda")
	else
		setitem(getrow(),"cod_valuta", ls_str)
	end if
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM"  then
	
		select cod_operaio
		into :ls_cod_operaio
		from anag_operai
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_utente = :s_cs_xx.cod_utente;
				 
		if sqlca.sqlcode = 0 then
			setitem(getrow(),"cod_operaio",ls_cod_operaio)
			wf_deposito()
		end if
	
	end if
	
	ldt_today = datetime(today(), 00:00:00)
	setitem(getrow(), "data_richiesta", ldt_today)
	setitem(getrow(),"data_disponibilita", ldt_today)
	
	// stefnaop: 19/01/2012: controllo se esiste già una riga e prelevo i campi base da quella
	if rowcount() > 1 then
		ll_row = getrow() -1
		
		setitem(getrow(), "anno_registrazione", getitemnumber(ll_row, "anno_registrazione"))
		setitem(getrow(), "num_registrazione", getitemnumber(ll_row, "num_registrazione"))
	end if
	
	setcolumn("cod_prodotto")
end if

end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
		
	case "prezzo_acquisto"
		
		if key = keyF1!  and keyflags = 1 then
			
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = getitemstring(getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "O"
			s_cs_xx.parametri.parametro_s_11 = "rda"
			
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
			
		end if
		
		
	case "des_specifica"
		// questa opzione è stata aggiunta per PTENDA.
		// quindi solo nel caso di ptenda esiste il parametro CSO (che tra l'altro è usato anche per lo storico ordini
		// se esiste questo parametro, concedo di andare in ricerca tessuti.
		
		string ls_cod_parametro
		
		setnull(ls_cod_parametro)
		
		select cod_parametro
		into   :ls_cod_parametro
		from   parametri_azienda
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_parametro = 'CSO';
		
		if key = keyF1!  and keyflags = 1 and ls_cod_parametro = "CSO" then
		
			s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_rda_det
			s_cs_xx.parametri.parametro_s_1 = "des_specifica"
			s_cs_xx.parametri.parametro_s_2 = "cod_prodotto"
			
			window_type_open(w_cs_xx_risposta, "w_tessuti_ricerca", 0)
			
			dw_det_rda_det.setcolumn("cod_prodotto")
			dw_det_rda_det.settext(s_cs_xx.parametri.parametro_s_1)
			dw_det_rda_det.setcolumn("des_specifica")
			dw_det_rda_det.settext(s_cs_xx.parametri.parametro_s_2)
			dw_det_rda_det.accepttext()
			
		end if 
				
end choose

end event

type st_2 from statictext within w_det_rda
integer x = 1143
integer y = 1436
integer width = 997
integer height = 68
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_ricerca from u_dw_search within w_det_rda
event ue_key pbm_dwnkey
integer x = 18
integer y = 20
integer width = 3538
integer height = 660
integer taborder = 30
string dataobject = "d_det_rda_search"
boolean border = false
end type

event ue_key;//choose case this.getcolumnname()
//	case "rs_cod_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//			s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
//			s_cs_xx.parametri.parametro_s_1 = "rs_cod_prodotto"
//			if not isvalid(w_prodotti_ricerca) then
//				window_open(w_prodotti_ricerca, 0)
//			end if
//			w_prodotti_ricerca.show()
//		end if
//end choose
//
//if key = keyenter! then cb_reset.postevent("clicked")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca, "cod_fornitore")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
		
	case "b_cerca"
		dw_ricerca.fu_buildsearch(TRUE)
		tab_1.det_1.dw_det_rda_lista.change_dw_current()
		parent.triggerevent("pc_retrieve")

	case "b_annulla"
		dw_ricerca.fu_reset()
		
	case "b_nuova_rda"
		wf_nuova_rda()
		
end choose
end event

