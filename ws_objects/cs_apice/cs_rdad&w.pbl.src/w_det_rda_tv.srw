﻿$PBExportHeader$w_det_rda_tv.srw
forward
global type w_det_rda_tv from w_cs_xx_principale
end type
type tab_1 from tab within w_det_rda_tv
end type
type det_1 from userobject within tab_1
end type
type dw_det_rda_lista from uo_cs_xx_dw within det_1
end type
type det_1 from userobject within tab_1
dw_det_rda_lista dw_det_rda_lista
end type
type det_2 from userobject within tab_1
end type
type dw_det_rda_det from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_1
dw_det_rda_det dw_det_rda_det
end type
type tab_1 from tab within w_det_rda_tv
det_1 det_1
det_2 det_2
end type
end forward

global type w_det_rda_tv from w_cs_xx_principale
integer width = 4174
integer height = 2180
string title = "Dettaglio RDA"
event ue_title ( string as_title )
tab_1 tab_1
end type
global w_det_rda_tv w_det_rda_tv

type variables


private:
	boolean			ib_gibus = false
	
	integer			ii_anno_rda
	
	long				il_num_rda
	
	string			is_cod_valuta_rda, ls_cod_operaio_rda
	
	datetime		idt_data_reg_rda
	
end variables

forward prototypes
public subroutine wf_carica_fornitore (string as_cod_tessuto, string as_colore_tessuto)
public subroutine wf_deposito ()
public subroutine wf_nuova_rda ()
public subroutine wf_tasti_funzione (string fs_nome_colonna)
public function integer wf_new (ref datawindow adw_data)
end prototypes

event ue_title(string as_title);
this.title = as_title


return
end event

public subroutine wf_carica_fornitore (string as_cod_tessuto, string as_colore_tessuto);// se è presente colore tessuto e codice tessuto, allora carico anche il codice fornitore

string				ls_cod_fornitore, ls_cod_parametro

long					ll_row



ll_row = tab_1.det_2.dw_det_rda_det.getrow()

if ll_row>0 then
else
	return
end if

//sarebbe il cod. prodotto
if as_cod_tessuto="" or isnull(as_cod_tessuto) then return


//--------------------------------------------------------------------------------------------------------------------
//se esiste il parametro CSO e hai specificato il colore tessuto ma non ancora il fornitore, cerca il fornitore da tab_tessuti_locale
select cod_parametro
into   :ls_cod_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CSO';


if sqlca.sqlcode=0 and ls_cod_parametro="CSO" then
	//ESISTE il parametro aziendale CSO
	
	//campo des_specifica
	if isnull(as_colore_tessuto ) or trim(as_colore_tessuto)="" then return
	
	select cod_fornitore
	into   :ls_cod_fornitore
	from   tab_tessuti_locale
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tessuto = :as_cod_tessuto and
			 cod_non_a_magazzino = :as_colore_tessuto;
	
else
	//altrimenti prendi il fornitore abituale del prodotto ----------------------------------------------------------
	select cod_fornitore
	into   :ls_cod_fornitore
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :as_cod_tessuto;

end if

if ls_cod_fornitore="" then setnull(ls_cod_fornitore)

//se il fornitore abituale o quello della gestione tab_tessuti_locale è vuoto
//lascia quello presente
if not isnull(ls_cod_fornitore) then
	tab_1.det_2.dw_det_rda_det.setitem(ll_row,"cod_fornitore", ls_cod_fornitore)
end if


return
end subroutine

public subroutine wf_deposito ();string ls_cod_operaio, ls_cod_deposito, ls_error

ls_cod_operaio = "" 

ls_cod_operaio = tab_1.det_1.dw_det_rda_lista.getitemstring(tab_1.det_1.dw_det_rda_lista.getrow(), "cod_operaio")

guo_functions.uof_get_stabilimento_operaio(ls_cod_operaio, ls_cod_deposito, ls_error)

 tab_1.det_1.dw_det_rda_lista.setitem(tab_1.det_1.dw_det_rda_lista.getrow(), "cod_deposito", ls_cod_deposito)
end subroutine

public subroutine wf_nuova_rda ();if not g_mb.confirm("Creare una nuova RDA?") then
	return
end if

// pulisco righe
tab_1.det_1.dw_det_rda_lista.reset()

this.postevent('pc_new')
end subroutine

public subroutine wf_tasti_funzione (string fs_nome_colonna);//st_2.text = ""
//choose case fs_nome_colonna
//	case "prezzo_acquisto"
//		st_2.text = "SHIFT - F1 = Storico Acq."
//end choose
//
end subroutine

public function integer wf_new (ref datawindow adw_data);
string			ls_str, ls_cod_operaio
long				ll_row
datetime		ldt_data_reg


tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true

adw_data.setitem(adw_data.getrow(), "anno_registrazione", ii_anno_rda)
adw_data.setitem(adw_data.getrow(), "num_registrazione", il_num_rda)


//---------------------------------------------------------------
//valuta
if is_cod_valuta_rda<>"" and not isnull(is_cod_valuta_rda) then
	//prediligo il cod valuta della testata rda
	adw_data.setitem(adw_data.getrow(),"cod_valuta", is_cod_valuta_rda)

else
	//provo con parametro LIR
	select stringa
	into :ls_str
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'LIR';

	if ls_str<>"" and not isnull(ls_str) then
		adw_data.setitem(adw_data.getrow(),"cod_valuta", ls_str)
		
	else
		//messaggio di avviso, non bloccante
		g_mb.warning("APICE","Per pre-impostare la valuta occorre selezionarla nella testata RDA oppure pre-impostare il parametro aziendale LIR!")
	end if
end if


//---------------------------------------------------------------
//operaio richiedente e relativo deposito
if ls_cod_operaio_rda<>"" and not isnull(ls_cod_operaio_rda) then
	//prediligo il cod operaio della testata rda
	ls_cod_operaio = ls_cod_operaio_rda
	
else
	//provo a impostarlo dalll'operaio legato all'utente
	if s_cs_xx.cod_utente <> "CS_SYSTEM"  then
		
		select cod_operaio
		into :ls_cod_operaio
		from anag_operai
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_utente = :s_cs_xx.cod_utente;
		
		if ls_cod_operaio="" then setnull(ls_cod_operaio)
	else
		setnull(ls_cod_operaio)
	end if
	
end if

adw_data.setitem(adw_data.getrow(),"cod_operaio", ls_cod_operaio)


//deposito dell'operaio richiedente
if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then
	
	wf_deposito()
end if


//data_richiesta e data_disponibilità ----------------------------------------------------
ldt_data_reg = idt_data_reg_rda

if isnull(ldt_data_reg) or year(date(ldt_data_reg)) < 1980 then
	//metto data odierna
	ldt_data_reg = datetime(today(), 00:00:00)
end if

adw_data.setitem(adw_data.getrow(), "data_richiesta", ldt_data_reg)
adw_data.setitem(adw_data.getrow(),"data_disponibilita", ldt_data_reg)


adw_data.setcolumn("cod_prodotto")



return 0
end function

on w_det_rda_tv.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_det_rda_tv.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;string				l_criteriacolumn[], l_searchtable[], l_searchcolumn[], &
						ls_flag_amministratore, ls_cod_operaio

windowobject		l_objects[ ]


tab_1.det_1.dw_det_rda_lista.set_dw_key("cod_azienda")
tab_1.det_1.dw_det_rda_lista.set_dw_key("anno_registrazione")
tab_1.det_1.dw_det_rda_lista.set_dw_key("anno_registrazione")


//##########################################################
//set_w_options(c_noenablepopup + c_closenosave)
//
//tab_1.det_1.dw_det_rda_lista.set_dw_options(		sqlca, &
//																		i_openparm, &
//																		c_scrollparent + c_nonew + c_nodelete + c_nomodify, &
//																		c_default)
//												
//tab_1.det_2.dw_det_rda_det.set_dw_options(		sqlca, &
//																		tab_1.det_1.dw_det_rda_lista, &
//																		c_sharedata + c_scrollparent + c_nonew + c_nodelete + c_nomodify, &
//																		c_default)

tab_1.det_1.dw_det_rda_lista.set_dw_options(		sqlca, &
																		i_openparm, &
																		c_scrollparent, &
																		c_default)
												
tab_1.det_2.dw_det_rda_det.set_dw_options(		sqlca, &
																		tab_1.det_1.dw_det_rda_lista, &
																		c_sharedata + c_scrollparent, &
																		c_default)

iuo_dw_main = tab_1.det_1.dw_det_rda_lista

//##########################################################

select flag_collegato
into :ls_flag_amministratore
from utenti
where cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode <> 0 then ls_flag_amministratore = "N"

ib_gibus = guo_functions.uof_is_gibus()

// Per gibus, richiesto da Beatrice, il codice operaio NON deve essere bloccato
if not ib_gibus then
	if s_cs_xx.cod_utente = "CS_SYSTEM" or ls_flag_amministratore ="S" then
		tab_1.det_2.dw_det_rda_det.Object.cod_operaio.Protect=0
	else
		tab_1.det_2.dw_det_rda_det.Object.cod_operaio.Protect=1
	end if
end if

tab_1.det_1.dw_det_rda_lista.ib_dw_detail = true
tab_1.det_2.dw_det_rda_det.ib_dw_detail = true

tab_1.det_1.dw_det_rda_lista.is_cod_parametro_blocco_prodotto = 'PRA'
tab_1.det_2.dw_det_rda_det.is_cod_parametro_blocco_prodotto = 'PRA'



end event

event resize;/* TOLTO ANCESTOR SCRIPT */
tab_1.resize(newwidth - 40, newheight - tab_1.y - 20)

//tab_1.event ue_resize()
end event

event pc_setddlb;call super::pc_setddlb;string ls_db, ls_descrizione

ls_db = f_db()
if ls_db = "ORACLE" then
	ls_descrizione = "nome || ' ' || cognome"
else
	ls_descrizione = "nome + ', ' + cognome" 
end if

f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_operaio", &
							sqlca, &
							"anag_operai", &
							"cod_operaio", &
							ls_descrizione, &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and (data_fine_rapporto > " + s_cs_xx.db_funzioni.oggi + " or data_fine_rapporto is null)")			
							
f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
							"des_deposito", &
							"cod_azienda='" + s_cs_xx.cod_azienda + "' ")
							
f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_deposito_partenza", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
							"des_deposito", &
							"cod_azienda='" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_operaio_autorizzante", &
							sqlca, &
							"anag_operai", &
							"cod_operaio", &
							ls_descrizione, &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and (data_fine_rapporto > " + s_cs_xx.db_funzioni.oggi + " or data_fine_rapporto is null)")

f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_centro_costo", &
							sqlca, &
							"tab_centri_costo", &
							"cod_centro_costo", &
							"des_centro_costo", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_valuta", &
							sqlca, &
							"tab_valute", &
							"cod_valuta", &
							"des_valuta", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(tab_1.det_2.dw_det_rda_det, &
							"cod_tipo_politica_riordino", &
							sqlca, &
							"tab_tipi_politiche_riordino", &
							"cod_tipo_politica_riordino", &
							"des_tipo_politica_riordino", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")


end event

type tab_1 from tab within w_det_rda_tv
event ue_resize ( )
integer x = 14
integer y = 36
integer width = 4101
integer height = 2016
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
det_1 det_1
det_2 det_2
end type

event ue_resize();det_1.dw_det_rda_lista.resize(det_1.width, det_1.height)
det_2.dw_det_rda_det.resize(det_1.width, det_1.height)
end event

on tab_1.create
this.det_1=create det_1
this.det_2=create det_2
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_1.destroy
destroy(this.det_1)
destroy(this.det_2)
end on

type det_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4064
integer height = 1892
long backcolor = 12632256
string text = "Lista"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det_rda_lista dw_det_rda_lista
end type

on det_1.create
this.dw_det_rda_lista=create dw_det_rda_lista
this.Control[]={this.dw_det_rda_lista}
end on

on det_1.destroy
destroy(this.dw_det_rda_lista)
end on

type dw_det_rda_lista from uo_cs_xx_dw within det_1
integer x = 14
integer y = 56
integer width = 4032
integer height = 1824
integer taborder = 20
string dataobject = "d_det_rda_lista_tv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_parametro, ls_flag_blocco, ls_where
datetime ldt_data_registrazione

setnull(ls_cod_parametro)

if i_extendmode then
	
	choose case i_colname
			
		case "cod_prodotto"
			ldt_data_registrazione = datetime(today(),00:00:00)
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
			
			wf_carica_fornitore(i_coltext, getitemstring(i_rownbr, "des_specifica"))
			
			select flag_blocco
			into   :ls_flag_blocco
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode = 0 then
				// 17/06/2008 aggiunto segnalazione blocco prodotto su richiesta di beatrice.
				if ls_flag_blocco = "S" then
					g_mb.warning("APICE","Attenzione: prodotto bloccato da anagrafica prodotti")
				end if
			end if
			
			
			
		case "des_specifica"
			wf_carica_fornitore(getitemstring(i_rownbr, "cod_prodotto"), i_coltext)
			
			
			
		case "cod_operaio"
			wf_deposito()

			
	end choose
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true
	
	wf_tasti_funzione(tab_1.det_2.dw_det_rda_det.getcolumnname())

end if
end event

event pcd_retrieve;call super::pcd_retrieve;
long 				ll_errore
string			ls_title


ii_anno_rda = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_rda = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

is_cod_valuta_rda = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ls_cod_operaio_rda = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

idt_data_reg_rda = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")


ll_errore = retrieve(s_cs_xx.cod_azienda, ii_anno_rda, il_num_rda)

ls_title = "Dettaglio RDA n° "+string(ii_anno_rda)+"/"+string(il_num_rda)



if ll_errore < 0 then
   pcca.error = c_fatal
end if

w_det_rda_tv.event trigger ue_title(ls_title)
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = false
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = false
end if
end event

event pcd_setkey;call super::pcd_setkey;long 				ll_i, ll_num_registrazione, ll_prog_riga_rda, ll_num_registrazione_2
integer			li_anno_registrazione


for ll_i = 1 to this.rowcount()
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// COD_AZIENDA
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// ANNO_REGISTRAZIONE
	li_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
	if li_anno_registrazione = 0 or  isnull(li_anno_registrazione) then
		setitem(ll_i, "anno_registrazione", ii_anno_rda)
		li_anno_registrazione = ii_anno_rda
	end if
	
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// NUM_REGISTRAZIONE
	ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione")
	if ll_num_registrazione = 0 or  isnull(ll_num_registrazione) then
		setitem(ll_i, "num_registrazione", il_num_rda)
		ll_num_registrazione = il_num_rda
	end if

	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	// PROG_RIGA_RDA
	ll_prog_riga_rda = this.getitemnumber(ll_i, "prog_riga_rda")
	if ll_prog_riga_rda = 0 or  isnull(ll_prog_riga_rda) then
		
		setnull(ll_prog_riga_rda)
		
		// prelevo il massimo raggiunto dalle righe rda per la rda corrente	
		select max(prog_riga_rda)
		into :ll_prog_riga_rda
		from det_rda
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :li_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
				
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante il calcolo della progressivo riga RDA.", sqlca)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_prog_riga_rda) then
			ll_prog_riga_rda = 0
		end if			

		
		ll_prog_riga_rda += 10
		this.setitem(ll_i, "prog_riga_rda", ll_prog_riga_rda)
	end if
 
next      
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = false
	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = false
end if
end event

event ue_key;call super::ue_key;string ls_cod_valuta, ls_cod_misura
long   ll_cont
double ld_prezzo_acquisto, ld_sconto_tot, ld_sconti[], ld_fat_conversione
datetime ldt_data_consegna
uo_condizioni_cliente luo_condizioni_cliente

choose case this.getcolumnname()
		
	case "prezzo_acquisto"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = getitemstring(getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "O"
			s_cs_xx.parametri.parametro_s_12 = "%"+ Left (getitemstring(getrow(),"des_prodotto"), 20)+"%"
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
		
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_prodotto( dw_det_rda_lista,"cod_prodotto")
		end if
		
end choose


end event

event updatestart;call super::updatestart;
long ll_i, ll_righe
dwitemstatus ld_status

if i_extendmode then
	for ll_i = 1 to rowcount()
		ll_righe = rowcount()
		ld_status = getitemstatus(ll_i,0,primary!) 
		
		if getitemstatus(ll_i,0,primary!) = newmodified!  then
			if isnull(getitemdatetime(ll_i,"data_richiesta")) then
				g_mb.messagebox("APICE","Data richiesta obbligatoria !!!",stopsign!)
				return 1
			end if
			
			if isnull(getitemstring(ll_i,"cod_operaio")) then
				g_mb.messagebox("APICE","Richiedente obbligatorio !!!",stopsign!)
				return 1
			end if
			
		end if
		
	next
end if
end event

event pcd_new;call super::pcd_new;

if i_extendmode then
	
		
	wf_new(tab_1.det_1.dw_det_rda_lista)
	
end if



//if i_extendmode then
//	string			ls_str, ls_cod_operaio, ls_cod_valuta, ls_cod_operaio_2
//	long				ll_row//, ll_num_registrazione
//	datetime		ldt_today
//	//integer			li_anno_registrazione
//	
//	tab_1.det_2.dw_det_rda_det.object.b_ricerca_prodotto.enabled = true
//	tab_1.det_2.dw_det_rda_det.object.b_ricerca_fornitore.enabled = true
//	
//	
//	//li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
//	//ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
//	
//	setitem(getrow(), "anno_registrazione", il_anno_rda)
//	setitem(getrow(), "num_registrazione", il_num_rda)
//	
//	//---------------------------------------------------------------
//	//lettura valori da propagare eventualmente dalla testata rda
//	select cod_valuta, cod_operaio, data_registrazione
//	into :ls_cod_valuta, :ls_cod_operaio_2, :ldt_today
//	from tes_rda
//	where 	cod_azienda = :s_cs_xx.cod_azienda and
//				anno_registrazione=:il_anno_rda and
//				num_registrazione=:il_num_rda;
//	
//	
//	//---------------------------------------------------------------
//	//valuta
//	if ls_cod_valuta<>"" and not isnull(ls_cod_valuta) then
//		//prediligo il cod valuta della testata rda
//		setitem(getrow(),"cod_valuta", ls_cod_valuta)
//	
//	else
//		//provo con parametro LIR
//		select stringa
//		into :ls_str
//		from parametri_azienda
//		where cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_parametro = 'LIR';
//	
//		if ls_str<>"" and not isnull(ls_str) then
//			setitem(getrow(),"cod_valuta", ls_str)
//			
//		else
//			//messaggio di avviso, non bloccante
//			g_mb.warning("APICE","Per pre-impostare la valuta occorre selezionarla nella testata RDA oppure pre-impostare il parametro aziendale LIR!")
//		end if
//	end if
//	
//	
//	//---------------------------------------------------------------
//	//operaio richiedente e relativo deposito
//	if ls_cod_operaio_2<>"" and not isnull(ls_cod_operaio_2) then
//		//prediligo il cod operaio della testata rda
//		ls_cod_operaio = ls_cod_operaio_2
//		
//	else
//		//provo a impostarlo dalll'operaio legato all'utente
//		if s_cs_xx.cod_utente <> "CS_SYSTEM"  then
//			
//			select cod_operaio
//			into :ls_cod_operaio
//			from anag_operai
//			where cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_utente = :s_cs_xx.cod_utente;
//			
//			if ls_cod_operaio="" then setnull(ls_cod_operaio)
//		else
//			setnull(ls_cod_operaio)
//		end if
//		
//	end if
//	
//	setitem(getrow(),"cod_operaio", ls_cod_operaio)
//	
//	
//	//deposito dell'operaio richiedente
//	if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then
//		
//		wf_deposito()
//	end if
//	
//
//	//data_richiesta e data_disponibilità ----------------------------------------------------
//	if isnull(ldt_today) or year(date(ldt_today)) < 1980 then
//		//metto data odierna
//		ldt_today = datetime(today(), 00:00:00)
//	end if
//	
//	setitem(getrow(), "data_richiesta", ldt_today)
//	setitem(getrow(),"data_disponibilita", ldt_today)
//	
//	
//	setcolumn("cod_prodotto")
//end if

end event

type det_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4064
integer height = 1892
long backcolor = 12632256
string text = "Dettaglio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det_rda_det dw_det_rda_det
end type

on det_2.create
this.dw_det_rda_det=create dw_det_rda_det
this.Control[]={this.dw_det_rda_det}
end on

on det_2.destroy
destroy(this.dw_det_rda_det)
end on

type dw_det_rda_det from uo_cs_xx_dw within det_2
event ue_carica_fornitore ( string as_cod_tessuto,  string as_colore_tessuto )
integer x = 5
integer y = 24
integer width = 4032
integer height = 1852
integer taborder = 11
string dataobject = "d_det_rda_det_tv"
boolean border = false
end type

event ue_carica_fornitore(string as_cod_tessuto, string as_colore_tessuto);wf_carica_fornitore(as_cod_tessuto, as_colore_tessuto)

return
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_det_rda_det,"cod_fornitore")
		
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_rda_det,"cod_prodotto")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_flag_blocco
datetime ldt_data_registrazione


if i_extendmode then
	
	choose case i_colname
			
		case "cod_prodotto"
			
			ldt_data_registrazione = datetime(today(),00:00:00)
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
			
			wf_carica_fornitore( i_coltext, getitemstring(i_rownbr, "des_specifica") )
			
			
			select flag_blocco
			into   :ls_flag_blocco
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode = 0 then
				// 17/06/2008 aggiunto segnalazione blocco prodotto su richiesta di beatrice.
				if ls_flag_blocco = "S" then
					g_mb.messagebox("APICE","Attenzione: prodotto bloccato da anagrafica prodotti")
				end if
				
			end if
			
			
			
			
		case "des_specifica"
			wf_carica_fornitore(getitemstring(i_rownbr, "cod_prodotto"), i_coltext )
			
			
			
		case "cod_operaio"
			wf_deposito()

			
	end choose
end if
end event

event pcd_new;call super::pcd_new;

if i_extendmode then
	
	wf_new(tab_1.det_1.dw_det_rda_lista)
	
end if
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
		
	case "prezzo_acquisto"
		
		if key = keyF1!  and keyflags = 1 then
			
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = getitemstring(getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "O"
			s_cs_xx.parametri.parametro_s_11 = "rda"
			
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
			
		end if
		
		
	case "des_specifica"
		// questa opzione è stata aggiunta per PTENDA.
		// quindi solo nel caso di ptenda esiste il parametro CSO (che tra l'altro è usato anche per lo storico ordini
		// se esiste questo parametro, concedo di andare in ricerca tessuti.
		
		string ls_cod_parametro
		
		setnull(ls_cod_parametro)
		
		select cod_parametro
		into   :ls_cod_parametro
		from   parametri_azienda
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_parametro = 'CSO';
		
		if key = keyF1!  and keyflags = 1 and ls_cod_parametro = "CSO" then
		
			s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_rda_det
			s_cs_xx.parametri.parametro_s_1 = "des_specifica"
			s_cs_xx.parametri.parametro_s_2 = "cod_prodotto"
			
			window_type_open(w_cs_xx_risposta, "w_tessuti_ricerca", 0)
			
			dw_det_rda_det.setcolumn("cod_prodotto")
			dw_det_rda_det.settext(s_cs_xx.parametri.parametro_s_1)
			dw_det_rda_det.setcolumn("des_specifica")
			dw_det_rda_det.settext(s_cs_xx.parametri.parametro_s_2)
			dw_det_rda_det.accepttext()
			
		end if 
				
end choose

end event

