﻿$PBExportHeader$w_evasione_rda.srw
forward
global type w_evasione_rda from w_cs_xx_principale
end type
type st_1 from statictext within w_evasione_rda
end type
type rb_2 from radiobutton within w_evasione_rda
end type
type rb_1 from radiobutton within w_evasione_rda
end type
type cb_gen_off_acq from commandbutton within w_evasione_rda
end type
type cb_ricerca from commandbutton within w_evasione_rda
end type
type cb_annulla from commandbutton within w_evasione_rda
end type
type cb_gen_ord_acq from commandbutton within w_evasione_rda
end type
type gb_1 from groupbox within w_evasione_rda
end type
type dw_ext_selezione_evas_rda from u_dw_search within w_evasione_rda
end type
type dw_ext_gen_ord_acq_rda from uo_std_dw within w_evasione_rda
end type
type dw_evas_rda_lista_rda from uo_cs_xx_dw within w_evasione_rda
end type
type dw_folder_search from u_folder within w_evasione_rda
end type
end forward

global type w_evasione_rda from w_cs_xx_principale
integer width = 4713
integer height = 2832
string title = "Creazione Offerte / Ordini Acquisto da RDA"
st_1 st_1
rb_2 rb_2
rb_1 rb_1
cb_gen_off_acq cb_gen_off_acq
cb_ricerca cb_ricerca
cb_annulla cb_annulla
cb_gen_ord_acq cb_gen_ord_acq
gb_1 gb_1
dw_ext_selezione_evas_rda dw_ext_selezione_evas_rda
dw_ext_gen_ord_acq_rda dw_ext_gen_ord_acq_rda
dw_evas_rda_lista_rda dw_evas_rda_lista_rda
dw_folder_search dw_folder_search
end type
global w_evasione_rda w_evasione_rda

type variables
private:
	string is_parametro_cso
	string is_cod_deposito_utente = ""
end variables

forward prototypes
public function integer wf_cambio_acq (string fs_cod_valuta, datetime fdt_data)
public function integer wf_note (string fs_cod_fornitore, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore)
public function integer wf_listini_fornitore (string as_cod_prodotto, string as_cod_fornitore, string as_cod_tipo_listino_prodotto, string as_cod_valuta, decimal ad_cambio_acq, date adt_data_consegna, decimal ad_quantita, ref decimal ad_prezzo, ref decimal ad_sconto, ref decimal ad_fat_conversione, ref string as_cod_misura)
public function integer wf_calcola_prezzo_fornitore (string as_cod_prodotto, string as_cod_fornitore, decimal ad_quantita, string as_cod_tipo_det_acq, date adt_data_inizio_val_for, ref decimal ad_prezzo, ref decimal ad_sconto, ref decimal ad_fat_conversione, ref string as_cod_misura)
public function integer wf_carica_ultimo_fornitore (long al_riga, ref string as_ultimo_fornitore)
public subroutine wf_inserisci_riga (long al_riga)
public subroutine wf_carica_drop_fornitore (string as_cod_prodotto)
end prototypes

public function integer wf_cambio_acq (string fs_cod_valuta, datetime fdt_data);long ll_i
dec{4} ld_cambio_acq, ld_valore
datetime ldt_data_cambio

select cambio_acq
into   :ld_cambio_acq 
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :fs_cod_valuta;

if sqlca.sqlcode = 0 then
   declare cu_cambio cursor for select cambio_acq, data_cambio from tab_cambi where cod_azienda = :s_cs_xx.cod_azienda and cod_valuta = :fs_cod_valuta and data_cambio <= :fdt_data order by cod_azienda, cod_valuta, data_cambio desc;
   open cu_cambio;
   ll_i = 0
   do while 0 = 0
      ll_i ++
      fetch cu_cambio into :ld_valore, :ldt_data_cambio;
      if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore in ricerca del cambio~r~n" + sqlca.sqlerrtext)
			close cu_cambio;
         return -1
      end if
		if sqlca.sqlcode = 100 then
			if ll_i = 1 then   		// ha fatto errore subito
				close cu_cambio;
				return ld_cambio_acq
			else
				close cu_cambio;
				return ld_valore
			end if
		end if
   loop

   if sqlca.sqlcode = 0 then
   end if
   close cu_cambio;
else
	return 1
end if
end function

public function integer wf_note (string fs_cod_fornitore, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore);string		ls_cod_fornitore, ls_cod_nota_fissa, ls_des_nota_fissa, ls_nota_fissa, ls_flag_piede_testata
datetime	ldt_oggi
long		ll_count,ll_prog_riga_ord_acq

//	Michela 28/05/2007: cursore per le note fisse

ldt_oggi = datetime(date(today()), 00:00:00)

declare cu_note cursor for
select	  	cod_nota_fissa,   
         	des_nota_fissa,   
         	nota_fissa,   
         	flag_piede_testata,
			cod_fornitore
from		tab_note_fisse  
where		cod_azienda = :s_cs_xx.cod_azienda and
			data_inizio <= :ldt_oggi and
			data_fine >= :ldt_oggi and
			flag_ordine_acq = 'S'
order by 	cod_nota_fissa ASC;

ll_count = 0

open cu_note;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore durante l'apertura del cursore: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_note into 	:ls_cod_nota_fissa,
							:ls_des_nota_fissa,
							:ls_nota_fissa,
							:ls_flag_piede_testata,
							:ls_cod_fornitore;
							
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore durante la fetch del cursore: " + sqlca.sqlerrtext
		close cu_note;
		return -1
	end if

	if not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
		if ls_cod_fornitore <> fs_cod_fornitore then continue
	end if
	
	select max(prog_riga_ordine_acq)
	into   :ll_prog_riga_ord_acq
	from   det_ord_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_registrazione and
			num_registrazione = :fl_num_registrazione;
			
	if isnull(ll_prog_riga_ord_acq) or ll_prog_riga_ord_acq < 1 then
		ll_prog_riga_ord_acq = 10
	else
		ll_prog_riga_ord_acq = ll_prog_riga_ord_acq + 10
	end if
	
	// procedo con la creazione delle righe d'ordine acquisto

  INSERT INTO det_ord_acq  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_ordine_acq,   
           cod_tipo_det_acq,   
           cod_prodotto,   
           cod_misura,   
           des_prodotto,   
           quan_ordinata,   
           prezzo_acquisto,   
           fat_conversione,   
           sconto_1,   
           sconto_2,   
           sconto_3,   
           cod_iva,   
           data_consegna,   
           cod_prod_fornitore,   
           quan_arrivata,   
           val_riga,   
           flag_saldo,   
           flag_blocco,   
           nota_dettaglio,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_off_acq,   
           num_off_acq,   
           prog_riga_off_acq,   
           data_consegna_fornitore,   
           num_confezioni,   
           num_pezzi_confezione,   
           flag_doc_suc_det,   
           flag_st_note_det,   
           imponibile_iva,   
           imponibile_iva_valuta,   
           anno_reg_rda,   
           num_reg_rda,   
           prog_riga_rda )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :fl_anno_registrazione,   
           :fl_num_registrazione,   
           :ll_prog_riga_ord_acq,
           :ls_cod_nota_fissa,   
           null,   
           null,   
           null,   
           0,   
           0,   
           1,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           0,   
           0,   
           'N',   
           'N',   
           :ls_nota_fissa,   
           null,		// anno commessa   
           null,   	// num commessa
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
		  null,
		  null,
           0,   
           0,   
           null,   	// anno reg rda
           null,   	// num reg rda
           null);	// riga rda
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in INSERT della riga ordine.~r~n" + sqlca.sqlerrtext
			close cu_note;
			return -1
		end if	
	
loop

return 0
end function

public function integer wf_listini_fornitore (string as_cod_prodotto, string as_cod_fornitore, string as_cod_tipo_listino_prodotto, string as_cod_valuta, decimal ad_cambio_acq, date adt_data_consegna, decimal ad_quantita, ref decimal ad_prezzo, ref decimal ad_sconto, ref decimal ad_fat_conversione, ref string as_cod_misura);/**
 * stefanop
 * 14/01/2013
 *
 * !!! ATTENZIONE !!!
 * La funzione è stata adattata dal f_listini_fornitori ed è solo di prova.
 * Questa funzione andrebbe incorporata all'interno di uno user object e cambiate tutte le chiamate
 * alla f_listini_fornitori per centralizzare la gestione.
 *
 * USE AT OWN RISK
 **/
  
string ls_cod_misura
decimal{4} ld_prezzo, ld_sconto, ld_quantita[], ld_prezzo_listino[], ld_sconto_listino[], ld_empty[], ld_fat_conversione
date ldt_data_inizio_val

setnull(ad_fat_conversione)
setnull(as_cod_misura)

// 1. LISTINO FORNITORI
ld_quantita = ld_empty
ld_prezzo_listino = ld_empty
ld_sconto_listino = ld_empty

select
	data_inizio_val, 
	quantita_1, prezzo_1, sconto_1, 
	quantita_2, prezzo_2, sconto_2, 
	quantita_3, prezzo_3, sconto_3, 
	quantita_4, prezzo_4, sconto_4, 
	quantita_5, prezzo_5, sconto_5, 
	fat_conversione, cod_misura
into :ldt_data_inizio_val,
	  :ld_quantita[1], :ld_prezzo_listino[1], :ld_sconto_listino[1],
	  :ld_quantita[2], :ld_prezzo_listino[2], :ld_sconto_listino[2],
	  :ld_quantita[3], :ld_prezzo_listino[3], :ld_sconto_listino[3],
	  :ld_quantita[4], :ld_prezzo_listino[4], :ld_sconto_listino[4],
	  :ld_quantita[5], :ld_prezzo_listino[5], :ld_sconto_listino[5],
	  :ld_fat_conversione, :ls_cod_misura
from listini_fornitori
where cod_azienda = :s_cs_xx.cod_azienda  and
		cod_prodotto = :as_cod_prodotto and
		cod_fornitore = :as_cod_fornitore and
		cod_valuta = :as_cod_valuta and
		data_inizio_val <= :adt_data_consegna
order by cod_azienda, cod_prodotto, cod_fornitore, cod_valuta, data_inizio_val desc;

if sqlca.sqlcode = 0 and (ld_prezzo_listino[1] <> 0 or &
								ld_prezzo_listino[2] <> 0 or &
								ld_prezzo_listino[3] <> 0 or &
								ld_prezzo_listino[4] <> 0 or &
								ld_prezzo_listino[5] <> 0) then
								
	if ad_quantita <= ld_quantita[1] then
		ld_prezzo = ld_prezzo_listino[1]
		ld_sconto = ld_sconto_listino[1]
	elseif ad_quantita <= ld_quantita[2] then
		ld_prezzo = ld_prezzo_listino[2]
		ld_sconto = ld_sconto_listino[2]
	elseif ad_quantita <= ld_quantita[3] then
		ld_prezzo = ld_prezzo_listino[3]
		ld_sconto = ld_sconto_listino[3]
	elseif ad_quantita <= ld_quantita[4] then
		ld_prezzo = ld_prezzo_listino[4]
		ld_sconto = ld_sconto_listino[4]
	elseif ad_quantita <= ld_quantita[5] then
		ld_prezzo = ld_prezzo_listino[5]
		ld_sconto = ld_sconto_listino[5]
	end if
	
	ad_fat_conversione = ld_fat_conversione
	as_cod_misura = ls_cod_misura
	ad_prezzo = ld_prezzo
	ad_sconto = ld_sconto
	return 0

end if

// 2. LISTINO PRODOTTI
ld_quantita = ld_empty
ld_prezzo_listino = ld_empty
ld_sconto_listino = ld_empty

select
	data_inizio_val,
	quantita_1, prezzo_1, sconto_1,
	quantita_2, prezzo_2, sconto_2, 
	quantita_3, prezzo_3, sconto_3, 
	quantita_4, prezzo_4, sconto_4, 
	quantita_5, prezzo_5, sconto_5
into :ldt_data_inizio_val,
	  :ld_quantita[1], :ld_prezzo_listino[1], :ld_sconto_listino[1],
	  :ld_quantita[2], :ld_prezzo_listino[2], :ld_sconto_listino[2],
	  :ld_quantita[3], :ld_prezzo_listino[3], :ld_sconto_listino[3],
	  :ld_quantita[4], :ld_prezzo_listino[4], :ld_sconto_listino[4],
	  :ld_quantita[5], :ld_prezzo_listino[5], :ld_sconto_listino[5]
from listini_prodotti
where cod_azienda = :s_cs_xx.cod_azienda  and
		 cod_prodotto = :as_cod_prodotto and
		 cod_tipo_listino_prodotto = :as_cod_tipo_listino_prodotto  and
		 cod_valuta = :as_cod_valuta and
		 data_inizio_val <= :adt_data_consegna
order by cod_azienda, cod_prodotto, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val desc;

if sqlca.sqlcode = 0 and (ld_prezzo_listino[1] <> 0 or &
								ld_prezzo_listino[2] <> 0 or &
								ld_prezzo_listino[3] <> 0 or &
								ld_prezzo_listino[4] <> 0 or &
								ld_prezzo_listino[5] <> 0) then
								
	if ad_quantita <= ld_quantita[1] then
		ld_prezzo = ld_prezzo_listino[1]
		ld_sconto = ld_sconto_listino[1]
	elseif ad_quantita <= ld_quantita[2] then
		ld_prezzo = ld_prezzo_listino[2]
		ld_sconto = ld_sconto_listino[2]
	elseif ad_quantita <= ld_quantita[3] then
		ld_prezzo = ld_prezzo_listino[3]
		ld_sconto = ld_sconto_listino[3]
	elseif ad_quantita <= ld_quantita[4] then
		ld_prezzo = ld_prezzo_listino[4]
		ld_sconto = ld_sconto_listino[4]
	elseif ad_quantita <= ld_quantita[5] then
		ld_prezzo = ld_prezzo_listino[5]
		ld_sconto = ld_sconto_listino[5]
	end if
	
	ad_prezzo = ld_prezzo
	ad_sconto = ld_sconto
	return 0

end if

// 3. anagraFICA PRODOTTI 
select
	anag_prodotti.prezzo_acquisto,
	anag_prodotti.sconto_acquisto
into
	:ld_prezzo,
	:ld_sconto
from anag_prodotti
where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
		 anag_prodotti.cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode = 0 and ld_prezzo <> 0 then
	
	if not isnull(ad_cambio_acq) and ad_cambio_acq > 0 then
		ad_prezzo = ld_prezzo / ad_cambio_acq
	else
		ad_prezzo = ld_prezzo
	end if
	
	ad_sconto = ld_sconto
end if


return 0
end function

public function integer wf_calcola_prezzo_fornitore (string as_cod_prodotto, string as_cod_fornitore, decimal ad_quantita, string as_cod_tipo_det_acq, date adt_data_inizio_val_for, ref decimal ad_prezzo, ref decimal ad_sconto, ref decimal ad_fat_conversione, ref string as_cod_misura);/**
 * stefanop
 * 14/01/2013
 *
 * Calcolo il prezzo del prodotto in base al fornitore e agli sconti impostati
 **/
 
string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_misura
decimal ld_cambio_acq, ld_fat_conversione

select
	cod_tipo_listino_prodotto,
	cod_valuta
into
	:ls_cod_tipo_listino_prodotto,
	:ls_cod_valuta
from anag_fornitori
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_fornitore = :as_cod_fornitore;
		 
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il recupero del tipo listino del fornitore.", sqlca)
	return -1
end if

wf_listini_fornitore(as_cod_prodotto, as_cod_fornitore, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ld_cambio_acq, adt_data_inizio_val_for, ad_quantita, ref ad_prezzo, ref ad_sconto, ref ad_fat_conversione, ref as_cod_misura)

return 0
end function

public function integer wf_carica_ultimo_fornitore (long al_riga, ref string as_ultimo_fornitore);string			ls_errore, ls_cod_prodotto, ls_sql
datastore	lds_data
long			ll_tot

as_ultimo_fornitore = ""
ls_cod_prodotto = dw_evas_rda_lista_rda.getitemstring(al_riga, "cod_prodotto")

if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then return 1

ls_sql =  "select t.cod_fornitore "+&
			"from tes_ord_acq as t "+&
			"join det_ord_acq as d on d.cod_azienda=t.cod_azienda and "+&
											 "d.anno_registrazione=t.anno_registrazione and "+&
											 "d.num_registrazione=t.num_registrazione "+&
			"where   t.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"t.cod_fornitore is not null "
			
if is_cod_deposito_utente <> "" then ls_sql += " and t.cod_deposito='"+is_cod_deposito_utente+"' "

ls_sql += " and d.cod_prodotto='"+ls_cod_prodotto+"' "+&
			"order by t.data_registrazione desc "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot>0 then
	//estraggo il fornitore primo nella lista
	as_ultimo_fornitore = lds_data.getitemstring(al_riga, 1)
end if


return 0
end function

public subroutine wf_inserisci_riga (long al_riga);long				ll_riga_new
string				ls_ultimo_fornitore, ls_sovrascrivi, ls_msg, ls_cod_fornitore, ls_cod_prodotto


//S se deve sovrascrivere sempre e tacitamente
//N se poma di sovrascrivere chiede (se fornitore impostato è diverso da ultimo)
ls_sovrascrivi = dw_ext_selezione_evas_rda.getitemstring(dw_ext_selezione_evas_rda.getrow(), "flag_sovrascrivi_forn")


ll_riga_new = dw_ext_gen_ord_acq_rda.insertrow(0)
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "anno_reg_rda" , dw_evas_rda_lista_rda.getitemnumber(al_riga, "anno_registrazione"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "num_reg_rda" , dw_evas_rda_lista_rda.getitemnumber(al_riga, "num_registrazione"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "prog_riga_rda" ,dw_evas_rda_lista_rda.getitemnumber(al_riga, "prog_riga_rda") )

//GESTIONE ULTIMO FORNITORE DA CUI LO STABILIEMNTO HA ACQUISTATO
//recupero l'ultimo fornitore da cui il deposito dell'utente ha acquistato il prodotto presente sulla riga della RDA
ls_cod_prodotto = dw_evas_rda_lista_rda.getitemstring(al_riga, "cod_prodotto")
ls_cod_fornitore = dw_evas_rda_lista_rda.getitemstring(al_riga, "cod_fornitore")

wf_carica_ultimo_fornitore(al_riga, ls_ultimo_fornitore)

if ls_ultimo_fornitore<>"" and ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	//esiste l'ultimo fornitore del prodotto
	
	if 	ls_cod_fornitore<>"" and not isnull(ls_cod_fornitore) and ls_ultimo_fornitore<>ls_cod_fornitore  then
		
		//esiste anche un fornitore impostato nella RDA ed è diverso dall'ultimo fornitore
		
		if ls_sovrascrivi="S" then
			//sovrascrivi ultimo fornitore senza chiedere niente a nessuno
		else
			ls_msg = "L'ultimo fornitore '"+ls_ultimo_fornitore+" - "+f_des_tabella("anag_fornitori","cod_fornitore = '" +  ls_ultimo_fornitore +"'", "rag_soc_1")+"' "+&
						"del prodotto '"+ls_cod_prodotto+"' è diverso da quello già impostato nella RDA ("+&
									ls_cod_fornitore+" - "+f_des_tabella("anag_fornitori","cod_fornitore = '" +  ls_cod_fornitore +"'", "rag_soc_1")+"). Vuoi sovrascrivere?"
			
			if g_mb.confirm(ls_msg) then
				//sovrascrivi
			else
				//non sovrascrivere
				ls_ultimo_fornitore = ls_cod_fornitore
			end if
		end if
		
	else
		//sovrascrivi tranquillamente a prescindere, tanto il fornitore della RDA non c'è
	end if
	
else
	//NESSUN ultimo fornitore
	//metto come fornitore quello che eventualmente era già stato impostato nella RDA
	ls_ultimo_fornitore = ls_cod_fornitore
end if

dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "cod_fornitore" , ls_ultimo_fornitore)
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "rag_soc_1" , f_des_tabella("anag_fornitori","cod_fornitore = '" +  ls_ultimo_fornitore +"'", "rag_soc_1"))

dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "cod_prodotto" , ls_cod_prodotto)
wf_carica_drop_fornitore(ls_cod_prodotto)

dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "des_prodotto" , dw_evas_rda_lista_rda.getitemstring(al_riga, "des_prodotto"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "cod_valuta" , dw_evas_rda_lista_rda.getitemstring(al_riga, "cod_valuta"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "quan_ordinata" , dw_evas_rda_lista_rda.getitemnumber(al_riga, "quan_richiesta") )
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "prezzo_acquisto" , dw_evas_rda_lista_rda.getitemnumber(al_riga, "prezzo_acquisto") )
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "cod_centro_costo" ,dw_evas_rda_lista_rda.getitemstring(al_riga, "cod_centro_costo") )
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "anno_commessa" , dw_evas_rda_lista_rda.getitemnumber(al_riga, "anno_commessa") )
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "num_commessa" , dw_evas_rda_lista_rda.getitemnumber(al_riga, "num_commessa") )
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "data_disponibilita" , dw_evas_rda_lista_rda.getitemdatetime(al_riga, "data_disponibilita"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "note" , dw_evas_rda_lista_rda.getitemstring(al_riga, "note"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "des_specifica" , dw_evas_rda_lista_rda.getitemstring(al_riga, "det_rda_des_specifica"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "cod_deposito" , dw_evas_rda_lista_rda.getitemstring(al_riga, "cod_deposito"))
dw_ext_gen_ord_acq_rda.setitem(ll_riga_new, "cod_deposito_trasf" , dw_evas_rda_lista_rda.getitemstring(al_riga, "cod_deposito_partenza"))


dw_ext_gen_ord_acq_rda.setrow(ll_riga_new)

return



end subroutine

public subroutine wf_carica_drop_fornitore (string as_cod_prodotto);string				ls_where, ls_cod_deposito_utente, ls_gropuby, ls_order_by
long				ll_anno

	
ll_anno = year(today())	
							
ls_where = 	"det_ord_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and year(tes_ord_acq.data_registrazione)="+string(ll_anno)

if as_cod_prodotto<>"" and not isnull(as_cod_prodotto) then
	ls_where += " and det_ord_acq.cod_prodotto='"+as_cod_prodotto+"'"
end if

if ls_cod_deposito_utente<>"" and not isnull(ls_cod_deposito_utente) then
	ls_where += " and tes_ord_acq.cod_deposito='"+ls_cod_deposito_utente+"'"
end if

ls_gropuby = "tes_ord_acq.cod_fornitore, anag_fornitori.rag_soc_1"
ls_order_by = "count(*) desc"

guo_functions.uof_loaddddw_dw( 	dw_ext_gen_ord_acq_rda, &
											"cod_fornitore", &
											sqlca, &
											"det_ord_acq", &
											"tes_ord_acq.cod_fornitore", &
														"'('+ cast(count(*) as varchar(10)) + ') '+ anag_fornitori.rag_soc_1", &
											"join tes_ord_acq on tes_ord_acq.cod_azienda=det_ord_acq.cod_azienda and "+&
                   													 	"tes_ord_acq.anno_registrazione=det_ord_acq.anno_registrazione and "+&
                    														"tes_ord_acq.num_registrazione=det_ord_acq.num_registrazione "+&
											"join anag_fornitori on anag_fornitori.cod_azienda=tes_ord_acq.cod_azienda and "+&
                       													"anag_fornitori.cod_fornitore=tes_ord_acq.cod_fornitore ",&
											ls_where, ls_gropuby, ls_order_by)		
end subroutine

on w_evasione_rda.create
int iCurrent
call super::create
this.st_1=create st_1
this.rb_2=create rb_2
this.rb_1=create rb_1
this.cb_gen_off_acq=create cb_gen_off_acq
this.cb_ricerca=create cb_ricerca
this.cb_annulla=create cb_annulla
this.cb_gen_ord_acq=create cb_gen_ord_acq
this.gb_1=create gb_1
this.dw_ext_selezione_evas_rda=create dw_ext_selezione_evas_rda
this.dw_ext_gen_ord_acq_rda=create dw_ext_gen_ord_acq_rda
this.dw_evas_rda_lista_rda=create dw_evas_rda_lista_rda
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.rb_2
this.Control[iCurrent+3]=this.rb_1
this.Control[iCurrent+4]=this.cb_gen_off_acq
this.Control[iCurrent+5]=this.cb_ricerca
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.cb_gen_ord_acq
this.Control[iCurrent+8]=this.gb_1
this.Control[iCurrent+9]=this.dw_ext_selezione_evas_rda
this.Control[iCurrent+10]=this.dw_ext_gen_ord_acq_rda
this.Control[iCurrent+11]=this.dw_evas_rda_lista_rda
this.Control[iCurrent+12]=this.dw_folder_search
end on

on w_evasione_rda.destroy
call super::destroy
destroy(this.st_1)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.cb_gen_off_acq)
destroy(this.cb_ricerca)
destroy(this.cb_annulla)
destroy(this.cb_gen_ord_acq)
destroy(this.gb_1)
destroy(this.dw_ext_selezione_evas_rda)
destroy(this.dw_ext_gen_ord_acq_rda)
destroy(this.dw_evas_rda_lista_rda)
destroy(this.dw_folder_search)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[],l_searchtable[],l_searchcolumn[], ls_errore
windowobject l_objects[ ]


dw_evas_rda_lista_rda.set_dw_options(sqlca, &
							pcca.null_object, &
							c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete, &
							c_default)

save_on_close(c_socnosave)


l_criteriacolumn[1] = "cod_prodotto"
l_criteriacolumn[2] = "cod_fornitore"
l_criteriacolumn[3] = "cod_operaio"
l_criteriacolumn[4] = "flag_autorizzato"
l_criteriacolumn[5] = "anno_commessa"
l_criteriacolumn[6] = "num_commessa"
l_criteriacolumn[7] = "cod_deposito"
l_criteriacolumn[8] = "anno_registrazione"
l_criteriacolumn[9] = "num_registrazione"

l_searchtable[1] = "det_rda"
l_searchtable[2] = "det_rda"
l_searchtable[3] = "det_rda"
l_searchtable[4] = "det_rda"
l_searchtable[5] = "det_rda"
l_searchtable[6] = "det_rda"
l_searchtable[7] = "det_rda"
l_searchtable[8] = "det_rda"
l_searchtable[9] = "det_rda"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_fornitore"
l_searchcolumn[3] = "cod_operaio"
l_searchcolumn[4] = "flag_autorizzato"
l_searchcolumn[5] = "anno_commessa"
l_searchcolumn[6]  = "num_commessa"
l_searchcolumn[7]  = "cod_deposito"
l_searchcolumn[8] = "anno_registrazione"
l_searchcolumn[9] = "num_registrazione"

dw_ext_selezione_evas_rda.fu_wiredw(l_criteriacolumn[], &
                     dw_evas_rda_lista_rda, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)


// ---------------  imposto folder di ricerca -------------------------------------
dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_evas_rda_lista_rda
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])
l_objects[1] = dw_ext_selezione_evas_rda
l_objects[2] = cb_ricerca
l_objects[3] = cb_annulla
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)

guo_functions.uof_get_parametro_azienda("CSO", ref is_parametro_cso)


is_cod_deposito_utente = ""

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	guo_functions.uof_get_stabilimento_utente(is_cod_deposito_utente, ls_errore)
	if isnull(is_cod_deposito_utente) or len(is_cod_deposito_utente) < 1 then
		is_cod_deposito_utente = ""
	end if
end if



end event

event pc_setddlb;call super::pc_setddlb;string					ls_where, ls_gropuby, ls_order_by


f_po_loaddddw_dw(dw_ext_selezione_evas_rda, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
							"des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_deposito = 'I' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")
							
							
f_po_loaddddw_dw(dw_ext_gen_ord_acq_rda, &
							"cod_fornitore", &
							sqlca, &
							"anag_fornitori", &
							"cod_fornitore", &
							"rag_soc_1", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
							
							
//ls_where = 	"det_ord_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
//					"det_ord_acq.cod_prodotto='CODICE' and "+&
//					"tes_ord_acq.cod_deposito='CODICE' and "+&
//					"year(tes_ord_acq.data_registrazione)=string(ANNO) "
//guo_functions.uof_loaddddw_dw( 	dw_ext_gen_ord_acq_rda, &
//											"cod_fornitore", &
//											sqlca, &
//											"det_ord_acq", &
//											"tes_ord_acq.cod_fornitore", &
//														"'('+ cast(count(*) as varchar(10)) + ') '+ anag_fornitori.rag_soc_1", &
//											"join tes_ord_acq on tes_ord_acq.cod_azienda=det_ord_acq.cod_azienda and "+&
//                   													 	"tes_ord_acq.anno_registrazione=det_ord_acq.anno_registrazione and "+&
//                    														"tes_ord_acq.num_registrazione=det_ord_acq.num_registrazione "+&
//											"join anag_fornitori on anag_fornitori.cod_azienda=tes_ord_acq.cod_azienda and "+&
//                       													"anag_fornitori.cod_fornitore=tes_ord_acq.cod_fornitore ",&
//											ls_where, ls_gropuby, ls_order_by)							


end event

type st_1 from statictext within w_evasione_rda
integer x = 32
integer y = 1220
integer width = 3072
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 12632256
string text = "DOPPIO CLICK sul numero registrazione per portare sotto l~'intera RDA."
boolean focusrectangle = false
end type

type rb_2 from radiobutton within w_evasione_rda
integer x = 873
integer y = 2584
integer width = 663
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Fornitore Potenziale"
end type

type rb_1 from radiobutton within w_evasione_rda
integer x = 142
integer y = 2584
integer width = 663
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Fornitore Anagrafico"
boolean checked = true
end type

type cb_gen_off_acq from commandbutton within w_evasione_rda
integer x = 3991
integer y = 2612
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen.Offerte"
end type

event clicked;string ls_cod_fornitore, ls_cod_deposito,ls_cod_valuta, ls_cod_tipo_listino_prodotto,ls_cod_pagamento,ls_cod_banca_clien_for,&
       ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_cod_banca,ls_cod_operatore,&
		 ls_flag_blocco,ls_cod_des_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_cap, ls_localita, ls_frazione, ls_telefono, ls_fax, &
		 ls_provincia, ls_nota_testata, ls_stringa, ls_cod_iva, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_centro_costo, &
		 ls_cod_prod_fornitore, ls_cod_tipo_det_acq,ls_cod_tipo_off_acq, ls_flag_tipo_det_acq, ls_cod_iva_det,ls_cod_valuta_rda, &
		 ls_cod_for_pot,ls_cod_tipo_det_acq_des, ls_note,ls_flag_doc_suc_or, ls_flag_st_note_det,ls_flag_doc_suc, ls_flag_st_note
long ll_i,ll_cont, ll_anno_reg[], ll_num_reg[], ll_index, ll_num_reg_rda, ll_anno_reg_rda, ll_prog_riga_rda, &
     ll_anno_commessa, ll_num_commessa,ll_prog_riga_off_acq
dec {4} ld_sconto_testata, ld_cambio_acq, ld_fat_conversione, ld_quan_acquisto, ld_prezzo_acquisto, ld_sconto_1, ld_sconto_2, ld_sconto_3
datetime ldt_oggi, ldt_data_consegna

if dw_ext_gen_ord_acq_rda.rowcount() < 1 then
	g_mb.messagebox("APICE","Nulla da generare; selezionare le righe di RDA dalla lista sopra!")
	return
end if

if g_mb.messagebox("APICE","Sei sicuro di voler generare le offerte di acquisto?",Question!,YesNo!,2) = 2 then return

// ordino le righe per fornitore in modo da creare gli ordini in modo omogeneo
dw_ext_gen_ord_acq_rda.setsort("cod_fornitore A, anno_reg_rda A, num_reg_rda A, prog_riga_rda A")
if dw_ext_gen_ord_acq_rda.sort() < 1 then
	g_mb.messagebox("APICE","Attenzione si è verificato un errore durante l'ordinamento delle righe.")
	return
end if

ll_index = 0

// cerco il tipo di dettaglio e il tipo ordine predefinito
select cod_tipo_det_acq, 
       cod_tipo_det_acq_des, 
		 cod_tipo_off_acq
into   :ls_cod_tipo_det_acq, 
       :ls_cod_tipo_det_acq_des, 
		 :ls_cod_tipo_off_acq
from   con_rda
where  cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo dettaglio acquisti nella tabella Parametri RDA ~r~n" + sqlca.sqlerrtext )
	rollback;
	return
end if
if isnull(ls_cod_tipo_det_acq) then
	g_mb.messagebox("APICE","Tipo dettaglio acquisto non specificato in Parametri RDA")
	rollback;
	return
end if
if isnull(ls_cod_tipo_det_acq_des) then
	g_mb.messagebox("APICE","Tipo dettaglio acquisto DESCRITTIVO non specificato in Parametri RDA")
	rollback;
	return
end if
if isnull(ls_cod_tipo_off_acq) then
	g_mb.messagebox("APICE","Tipo Ordine Acquisto non specificato in Parametri RDA")
	rollback;
	return
end if

select cod_iva,
       flag_doc_suc_or,
		 flag_st_note
into   :ls_cod_iva_det,
       :ls_flag_doc_suc_or,
		 :ls_flag_st_note_det
from   tab_tipi_det_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_det_acq = :ls_cod_tipo_det_acq;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca codice iva nella tabella tipi dettagli acquisto ~r~n" + sqlca.sqlerrtext )
	rollback;
	return
end if

setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_uo_dw_search)

//mancava il setnull pure di questo
setnull(s_cs_xx.parametri.parametro_dw_1)

//il parametro non è s_10 ma s_1
//setnull(s_cs_xx.parametri.parametro_s_10)
setnull(s_cs_xx.parametri.parametro_s_1)

if rb_1.checked then
//	window_open(w_fornitori_ricerca_response, 0)
	
	//il parametro non è s_10 ma s_1
	//ls_cod_fornitore = s_cs_xx.parametri.parametro_s_10
	ls_cod_fornitore = s_cs_xx.parametri.parametro_s_1
	
	setnull(ls_cod_for_pot)
else
	datawindow ldw_empty
	any la_results[]
	setnull(ldw_empty)
	guo_ricerca.uof_set_response()
	guo_ricerca.uof_ricerca_fornitore_potenziale(ldw_empty,"cod_for_pot")
	guo_ricerca.uof_get_results(la_results)
	
	//il parametro non è s_10 ma s_1
	//ls_cod_for_pot = s_cs_xx.parametri.parametro_s_10
//	ls_cod_fornitore = s_cs_xx.parametri.parametro_s_1
	ls_cod_fornitore = la_results[1]
	setnull(ls_cod_fornitore)
end if

//il parametro non è s_10 ma s_1
//if isnull(s_cs_xx.parametri.parametro_s_10) or len(s_cs_xx.parametri.parametro_s_10) < 1 then
if isnull(s_cs_xx.parametri.parametro_s_1) or len(s_cs_xx.parametri.parametro_s_1) < 1 then
	g_mb.messagebox("APICE","Specificare un fornitore o un fornitore potenziale")
	rollback;
	return
end if

// in questo caso procedo alla creazione di una nuova testata ordine di acquisto
if not isnull(ls_cod_fornitore) then
	select cod_deposito,   
		  cod_valuta,   
		  cod_tipo_listino_prodotto,   
		  cod_pagamento,   
		  sconto,   
		  cod_banca_clien_for,   
		  cod_imballo,   
		  cod_vettore,   
		  cod_inoltro,   
		  cod_mezzo,   
		  cod_porto,   
		  cod_resa,   
		  cod_banca
	into :ls_cod_deposito,   
		  :ls_cod_valuta,   
		  :ls_cod_tipo_listino_prodotto,   
		  :ls_cod_pagamento,   
		  :ld_sconto_testata,   
		  :ls_cod_banca_clien_for,   
		  :ls_cod_imballo,   
		  :ls_cod_vettore,   
		  :ls_cod_inoltro,   
		  :ls_cod_mezzo,   
		  :ls_cod_porto,   
		  :ls_cod_resa,   
		  :ls_cod_banca
	from anag_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca del fornitore ~r~n" + sqlca.sqlerrtext)
		return
	end if
	
	// proposta delle destinazioni del fornitore; se c'è una destinazione la scrivo in automatico; se ce n'è più di 1
	// metto una nota nella nota_piede di ricordarsi di specificare il dato.
	setnull(ls_nota_testata)
	setnull(ls_cod_des_fornitore)
	
	select count(*)
	into   :ll_cont
	from   anag_des_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
	if ll_cont = 1 then
		select cod_des_fornitore, rag_soc_1, rag_soc_2, indirizzo, cap, localita, frazione, provincia, telefono, fax
		into   :ls_cod_des_fornitore, :ls_rag_soc_1, :ls_rag_soc_2, :ls_indirizzo, :ls_cap, :ls_localita, :ls_frazione, :ls_provincia, :ls_telefono, :ls_fax
		from   anag_des_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
	else
		if ll_cont > 1 then
			ls_nota_testata = "~r~nATTENZIONE !! Resta da specificare la destinazione della merce."
		end if
	end if
		
else
	setnull(ls_cod_banca)
	select cod_deposito,   
		  cod_valuta,   
		  cod_tipo_listino_prodotto,   
		  cod_pagamento,   
		  sconto,   
		  cod_banca_clien_for,   
		  cod_imballo,   
		  cod_vettore,   
		  cod_inoltro,   
		  cod_mezzo,   
		  cod_porto,   
		  cod_resa
	into :ls_cod_deposito,   
		  :ls_cod_valuta,   
		  :ls_cod_tipo_listino_prodotto,   
		  :ls_cod_pagamento,   
		  :ld_sconto_testata,   
		  :ls_cod_banca_clien_for,   
		  :ls_cod_imballo,   
		  :ls_cod_vettore,   
		  :ls_cod_inoltro,   
		  :ls_cod_mezzo,   
		  :ls_cod_porto,   
		  :ls_cod_resa
	from anag_for_pot
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_for_pot = :ls_cod_for_pot;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca del fornitore ~r~n" + sqlca.sqlerrtext)
		return
	end if
end if	

// impostazione della data di registrazione=oggi 
ldt_oggi = datetime(today(), 00:00:00)

// ricerca del cambio in caso di valuta estera
select stringa
into   :ls_stringa
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LIR';
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca parametro LIR in parametri azienda")
	rollback;
	return
end if
// se la valuta della RDA è impostata la uso, altrimenti prendo quella del fornitore
ls_cod_valuta_rda = dw_ext_gen_ord_acq_rda.getitemstring(1,"cod_valuta")
if not isnull(ls_cod_valuta_rda) then
	ls_cod_valuta = ls_cod_valuta_rda
end if
if ls_cod_valuta <> ls_stringa then
	ld_cambio_acq = wf_cambio_acq(ls_cod_valuta, ldt_oggi)
else
	ld_cambio_acq = 1
end if


// ricerca dell'operatore
select cod_operatore
into :ls_cod_operatore
from tab_operatori_utenti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente;
if sqlca.sqlcode <> 0 then
	setnull(ls_cod_operatore)
end if

// ricerca del numero max + 1 dell'ordine di acquisto
ll_index ++
ll_anno_reg[ll_index] = f_anno_esercizio()

select max(num_registrazione)
into  :ll_num_reg[ll_index]
from  tes_off_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_reg[ll_index];
if isnull(ll_num_reg[ll_index]) or ll_num_reg[ll_index] < 1 then
	ll_num_reg[ll_index] = 1
else
	ll_num_reg[ll_index] ++
end if

select flag_doc_suc,
		 flag_st_note
into   :ls_flag_doc_suc,
		 :ls_flag_st_note
from   tab_tipi_off_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_off_acq = :ls_cod_tipo_off_acq;
		 //cod_tipo_ord_acq = :ls_cod_tipo_off_acq;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo ordine di acquisto " + sqlca.sqlerrtext)
	rollback;
	return
end if

INSERT INTO tes_off_acq  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_tipo_off_acq,   
		  data_registrazione,   
		  cod_operatore,
		  cod_for_pot,
		  cod_fornitore,
		  cod_des_fornitore,
		  rag_soc_1,
		  rag_soc_2,
		  indirizzo,
		  cap,
		  frazione,
		  localita,
		  provincia,
		  cod_deposito,   
		  cod_valuta,   
		  cambio_acq,   
		  cod_tipo_listino_prodotto,   
		  cod_pagamento,   
		  sconto,   
		  cod_banca_clien_for,   
		  cod_imballo,   
		  cod_vettore,   
		  cod_inoltro,   
		  cod_mezzo,   
		  cod_porto,   
		  cod_resa,   
		  flag_blocco,   
		  flag_evasione,   
		  data_consegna,   
		  cod_banca,   
		  flag_doc_suc_tes,   
		  flag_doc_suc_pie,   
		  flag_st_note_tes,   
		  flag_st_note_pie )  
VALUES ( :s_cs_xx.cod_azienda,   
		  :ll_anno_reg[ll_index],   
		  :ll_num_reg[ll_index],   
		  :ls_cod_tipo_off_acq,   
		  :ldt_oggi,   
		  :ls_cod_operatore,
		  :ls_cod_for_pot,
		  :ls_cod_fornitore,   
		  :ls_cod_des_fornitore,
		  :ls_rag_soc_1,
		  :ls_rag_soc_2,
		  :ls_indirizzo,
		  :ls_cap,
		  :ls_frazione,
		  :ls_localita,
		  :ls_provincia,
		  :ls_cod_deposito,   
		  :ls_cod_valuta,   
		  :ld_cambio_acq,   
		  :ls_cod_tipo_listino_prodotto,   
		  :ls_cod_pagamento,   
		  :ld_sconto_testata,   
		  :ls_cod_banca_clien_for,   
		  :ls_cod_imballo,   
		  :ls_cod_vettore,   
		  :ls_cod_inoltro,   
		  :ls_cod_mezzo,   
		  :ls_cod_porto,   
		  :ls_cod_resa,   
		  'N',   
		  'A',   
		  :ldt_data_consegna,   
		  :ls_cod_banca,   
		  :ls_flag_doc_suc,   
		  :ls_flag_doc_suc,   
		  :ls_flag_st_note,   
		  :ls_flag_st_note)  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in INSERT della testata offerte.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
for ll_i = 1 to dw_ext_gen_ord_acq_rda.rowcount()
	
	ls_cod_prodotto = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_prodotto")
	ls_des_prodotto = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"des_prodotto")
	ld_quan_acquisto = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"quan_ordinata")
	ld_prezzo_acquisto = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"prezzo_acquisto")
	ll_anno_reg_rda = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"anno_reg_rda")
	ll_num_reg_rda = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"num_reg_rda")
	ll_prog_riga_rda = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"prog_riga_rda")
	ll_anno_commessa = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"anno_commessa")
	ll_num_commessa = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"num_commessa")
	ls_cod_centro_costo = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_centro_costo")
	ldt_data_consegna = dw_ext_gen_ord_acq_rda.getitemdatetime(ll_i,"data_disponibilita")
	ls_note = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"note")
	
	// leggo dati da anag_prodotti
	if isnull(ls_cod_prodotto) then
		ls_cod_tipo_det_acq = ls_cod_tipo_det_acq_des
		select cod_iva,
				 flag_doc_suc_or,
				 flag_st_note
		into   :ls_cod_iva_det,
				 :ls_flag_doc_suc_or,
				 :ls_flag_st_note_det
		from   tab_tipi_det_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca codice iva nella tabella tipi dettagli acquisto ~r~n" + sqlca.sqlerrtext )
			rollback;
			return
		end if
		
		setnull(ls_cod_prod_fornitore)
		setnull(ls_cod_misura)
		ld_fat_conversione = 1
	else
		select cod_misura_mag,
				 fat_conversione_acq,
				 cod_iva
		into   :ls_cod_misura,
				 :ld_fat_conversione,
				 :ls_cod_iva
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotto " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext )
			rollback;
			return
		end if
		
		if isnull(ls_cod_iva) then
			if isnull(ls_cod_iva_det) then
				g_mb.messagebox("APICE","Il prodotto " + ls_cod_prodotto +" non ha alcuna aliquota iva indicata in anagrafica")
				rollback;
				return
			else
				ls_cod_iva = ls_cod_iva_det
			end if
		end if
			
		// cerco codice prodotto del fornitore
		select cod_prod_fornitore
		into   :ls_cod_prod_fornitore
		from   tab_prod_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 cod_fornitore = :ls_cod_fornitore;
		if sqlca.sqlcode = 100 then setnull(ls_cod_prod_fornitore)
	end if
	// cerco la il prossimo numero di riga ordine
	select max(prog_riga_off_acq)
	into   :ll_prog_riga_off_acq
	from   det_off_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_reg[ll_index] and
			num_registrazione = :ll_num_reg[ll_index];
	if isnull(ll_prog_riga_off_acq) or ll_prog_riga_off_acq < 1 then
		ll_prog_riga_off_acq = 10
	else
		ll_prog_riga_off_acq = ll_prog_riga_off_acq + 10
	end if
	
	// procedo con la creazione delle righe d'ordine acquisto

  INSERT INTO det_off_acq  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_off_acq,   
           cod_tipo_det_acq,   
           cod_prodotto,   
           cod_misura,   
           des_prodotto,   
           quan_ordinata,   
           prezzo_acquisto,   
           fat_conversione,   
           sconto_1,   
           sconto_2,   
           sconto_3,   
           cod_iva,   
           data_consegna,   
           cod_prod_fornitore,   
           val_riga,   
           flag_blocco,   
           nota_dettaglio,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           num_confezioni,   
           num_pezzi_confezione,   
           flag_doc_suc_det,   
           flag_st_note_det,   
           imponibile_iva,   
           imponibile_iva_valuta,   
           anno_reg_rda,   
           num_reg_rda,   
           prog_riga_rda )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_reg[ll_index],   
           :ll_num_reg[ll_index],   
           :ll_prog_riga_off_acq,
           :ls_cod_tipo_det_acq,   
           :ls_cod_prodotto,   
           :ls_cod_misura,   
           :ls_des_prodotto,   
           :ld_quan_acquisto,   
           :ld_prezzo_acquisto,   
           :ld_fat_conversione,   
           :ld_sconto_1,   
           :ld_sconto_2,   
           :ld_sconto_3,   
           :ls_cod_iva,   
           :ldt_data_consegna,   
           :ls_cod_prod_fornitore,   
           0,   
           'N',   
           :ls_note,   
           :ll_anno_commessa,   
           :ll_num_commessa,   
           :ls_cod_centro_costo,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
			  :ls_flag_doc_suc_or,
			  :ls_flag_st_note_det,
           0,   
           0,   
           :ll_anno_reg_rda,   
           :ll_num_reg_rda,   
           :ll_prog_riga_rda);
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in INSERT della riga offerta relativa al prodotto "+ls_cod_prodotto+".~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
next

for ll_i = 1 to ll_index
	ls_stringa = "~r~n" + string(ll_anno_reg[ll_i]) + "/" + string(ll_num_reg[ll_i])
next

g_mb.messagebox("APICE","RDA elaborate correttamente generando le seguenti offerte: " + ls_stringa)
commit;
dw_ext_gen_ord_acq_rda.reset()

cb_ricerca.postevent("clicked")
end event

type cb_ricerca from commandbutton within w_evasione_rda
integer x = 759
integer y = 868
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;// cb_search clicked event
dw_ext_selezione_evas_rda.fu_BuildSearch(TRUE)
dw_evas_rda_lista_rda.change_dw_current()
parent.triggerevent("pc_retrieve")
dw_folder_search.fu_selectTab(1)


end event

type cb_annulla from commandbutton within w_evasione_rda
integer x = 1413
integer y = 868
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_ext_selezione_evas_rda.fu_Reset()
end event

type cb_gen_ord_acq from commandbutton within w_evasione_rda
integer x = 3991
integer y = 2512
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen.Ordini"
end type

event clicked;boolean lb_provenienza_ordine_vendita=false

string ls_cod_fornitore, ls_cod_fornitore_old,ls_cod_deposito,ls_cod_valuta, ls_cod_tipo_listino_prodotto,ls_cod_pagamento,ls_cod_banca_clien_for,&
       ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_cod_banca,ls_cod_operatore,&
		 ls_flag_blocco,ls_cod_des_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_cap, ls_localita, ls_frazione, ls_telefono, ls_fax, &
		 ls_provincia, ls_nota_testata, ls_stringa, ls_cod_iva, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_centro_costo, &
		 ls_cod_prod_fornitore, ls_cod_tipo_det_acq,ls_cod_tipo_ord_acq, ls_flag_tipo_det_acq, ls_cod_iva_det, ls_cod_valuta_rda, &
		 ls_cod_tipo_det_acq_des, ls_note,ls_flag_doc_suc_bl, ls_flag_st_note_det,ls_flag_doc_suc, ls_flag_st_note, ls_cod_tipo_dettaglio, ls_cod_nota_fissa, ls_des_nota_fissa, ls_nota_fissa, ls_flag_piede_testata, &
		 ls_null, ls_nota_piede, ls_nota_video, ls_cod_nota_prodotto, ls_nota_prodotto, ls_des_specifica, ls_cod_misura_for,ls_cod_misura_ven, &
		 ls_cod_deposito_rda, ls_cod_deposito_trasf, ls_flag_copia_prezzo_ord_ven
		 
long ll_i,ll_cont, ll_anno_reg[], ll_num_reg[], ll_index, ll_num_reg_rda, ll_anno_reg_rda, ll_prog_riga_rda, ll_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_prog_riga_ord_ven, &
     ll_anno_commessa, ll_num_commessa,ll_prog_riga_ord_acq, ll_count, ll_num_ordini_presenti, ll_ret, ll_testata, ll_anno_ordine_scelto, ll_num_ordine_scelto
	  
dec {4} ld_sconto_testata, ld_cambio_acq, ld_fat_conversione, ld_quan_acquisto, ld_prezzo_acquisto, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
		  ld_fat_conversione_for, ld_prezzo_vendita, ld_sconto_acquisto_1, ld_prezzo_acquisto_1
		  
dec{5} ld_fat_conversione_ven

datetime ldt_oggi, ldt_data_consegna,ldt_data_inizio_val_for

boolean	lb_note

setnull(ls_null)

ll_testata = 0
setnull(ls_cod_fornitore_old)
if dw_ext_gen_ord_acq_rda.rowcount() < 1 then
	g_mb.messagebox("APICE","Nulla da generare; selezionare le righe di RDA dalla lista sopra!")
	return
end if

if g_mb.messagebox("APICE","Sei sicuro di voler generare gli ordini di acquisto?",Question!,YesNo!,2) = 2 then return

// ordino le righe per fornitore in modo da creare gli ordini in modo omogeneo
dw_ext_gen_ord_acq_rda.setsort("cod_fornitore A, anno_reg_rda A, num_reg_rda A, prog_riga_rda A")
if dw_ext_gen_ord_acq_rda.sort() < 1 then
	g_mb.messagebox("APICE","Attenzione si è verificato un errore durante l'ordimanento delle righe.")
	return
end if

ll_index = 0

// cerco il tipo di dettaglio e il tipo ordine predefinito
select cod_tipo_det_acq, 
       cod_tipo_det_acq_des, 
		 cod_tipo_ord_acq,
		 flag_copia_prezzo_ord_ven
into   :ls_cod_tipo_det_acq, 
       :ls_cod_tipo_det_acq_des, 
		 :ls_cod_tipo_ord_acq,
		 :ls_flag_copia_prezzo_ord_ven
from   con_rda
where  cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo dettaglio acquisti nella tabella Parametri RDA ~r~n" + sqlca.sqlerrtext )
	rollback;
	return
end if

if isnull(ls_cod_tipo_det_acq) then
	g_mb.messagebox("APICE","Tipo dettaglio acquisto non specificato in Parametri RDA")
	rollback;
	return
end if
if isnull(ls_cod_tipo_det_acq_des) then
	g_mb.messagebox("APICE","Tipo dettaglio DESCRITTIVO acquisto non specificato in Parametri RDA")
	rollback;
	return
end if
if isnull(ls_cod_tipo_ord_acq) then
	g_mb.messagebox("APICE","Tipo Ordine Acquisto non specificato in Parametri RDA")
	rollback;
	return
end if

select cod_iva,
       	flag_doc_suc_bl,
		flag_st_note_or
into   	:ls_cod_iva_det,
       	:ls_flag_doc_suc_bl,
		:ls_flag_st_note_det
from   	tab_tipi_det_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       	cod_tipo_det_acq = :ls_cod_tipo_det_acq;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca codice iva nella tabella tipi dettagli acquisto ~r~n" + sqlca.sqlerrtext )
	rollback;
	return
end if

lb_note = false

for ll_i = 1 to dw_ext_gen_ord_acq_rda.rowcount()
	ls_cod_fornitore = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_fornitore")
	ls_cod_deposito_rda = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_deposito")
	ls_cod_deposito_trasf = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_deposito_trasf")
	
	if ls_cod_fornitore <> ls_cod_fornitore_old or isnull(ls_cod_fornitore_old) then
		//----------CLAUDIA 19/07/07 AGGIUNTO IL CONTROLLO SE ESISTONO GIA' ORDINI
		ll_testata = 0
		ll_num_ordini_presenti = 0
		select count(*)
		into :ll_num_ordini_presenti
		from tes_ord_acq
		where cod_azienda =:s_cs_xx.cod_azienda and
				cod_fornitore = :ls_cod_fornitore and
				flag_evasione <> 'E' and
				flag_blocco = 'N';
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca ordini già inseriti per il fornitore con cod: "+ls_cod_fornitore+" ~r~n" + sqlca.sqlerrtext )
			rollback;
			return
		end if
		//---------------------- se esistono già ordini chiedo cosa vuole il cliente
		
		if ll_num_ordini_presenti >  0 then
			ll_ret =  g_mb.messagebox("APICE","Attenzione! Vuoi aggiungere le righe all'ordine esistente?",Question!,YesNo!,2) 
			if ll_ret = 1 then
				//------------ALLORA NE PROPONGO E GLIELI FACCIO SCIEGLIERE
				ll_testata = 1
				//------------apro maschera response
				//s_cs_xx.parametri.parametro_s_1  = ls_cod_tipo_ord_acq
				s_cs_xx.parametri.parametro_s_2 = ls_cod_fornitore
				window_open(w_ord_acq_x_rda, 0)
				ll_anno_ordine_scelto = s_cs_xx.parametri.parametro_i_1
				ll_num_ordine_scelto = s_cs_xx.parametri.parametro_i_2
				if ll_anno_ordine_scelto = 0 and ll_num_ordine_scelto = 0 then
					//-------ci ha ripensato e ne vuole craere uno nuovo?
					ll_testata = 0
					rollback;
					return
				end if
			else
				//----------allora procedo come nulla fosse
				ll_testata = 0
			end if
		end if
		
		// impostazione della data di registrazione=oggi 
		ldt_oggi = datetime(today(), 00:00:00)
		
		if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null, "ORD_ACQ", ls_null, ldt_oggi, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
			ls_nota_testata = ""
			ls_nota_piede = ""
			ls_nota_video = ""
		end if							
		
		// in questo caso procedo alla creazione di una nuova testata ordine di acquisto
		select cod_deposito,   
           cod_valuta,   
           cod_tipo_listino_prodotto,   
           cod_pagamento,   
           sconto,   
           cod_banca_clien_for,   
           cod_imballo,   
           cod_vettore,   
           cod_inoltro,   
           cod_mezzo,   
           cod_porto,   
           cod_resa,   
           cod_banca
		into :ls_cod_deposito,   
           :ls_cod_valuta,   
           :ls_cod_tipo_listino_prodotto,   
           :ls_cod_pagamento,   
           :ld_sconto_testata,   
           :ls_cod_banca_clien_for,   
           :ls_cod_imballo,   
           :ls_cod_vettore,   
           :ls_cod_inoltro,   
           :ls_cod_mezzo,   
           :ls_cod_porto,   
           :ls_cod_resa,   
           :ls_cod_banca
		from anag_fornitori
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_fornitore = :ls_cod_fornitore;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca del fornitore ~r~n" + sqlca.sqlerrtext)
			return
		end if
		
		// proposta delle destinazioni del fornitore; se c'è una destinazione la scrivo in automatico; se ce n'è più di 1
		// metto una nota nella nota_piede di ricordarsi di specificare il dato.
		setnull(ls_nota_testata)
		setnull(ls_cod_des_fornitore)
		
		select 	count(*)
		into   		:ll_cont
		from    	anag_des_fornitori
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		       		cod_fornitore = :ls_cod_fornitore;
						 
		if ll_cont = 1 then
			
			select		cod_des_fornitore, 
						rag_soc_1, 
						rag_soc_2, 
						indirizzo, 
						cap, 
						localita, 
						frazione, 
						provincia, 
						telefono, 
						fax
			into   		:ls_cod_des_fornitore, 
						:ls_rag_soc_1, 
						:ls_rag_soc_2, 
						:ls_indirizzo, 
						:ls_cap, 
						:ls_localita, 
						:ls_frazione, 
						:ls_provincia, 
						:ls_telefono, 
						:ls_fax
			from   	anag_des_fornitori
			where  	cod_azienda = :s_cs_xx.cod_azienda and
					 	cod_fornitore = :ls_cod_fornitore;
						 
		else
			if ll_cont > 1 then
				ls_nota_testata = "~r~nATTENZIONE !! Resta da specificare la destinazione della merce."
			end if
		end if
		
		// ricerca del cambio in caso di valuta estera
		
		select stringa
		into   :ls_stringa
		from   parametri_azienda
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_parametro = 'LIR';
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca parametro LIR in parametri azienda")
			rollback;
			return
		end if
		// se la valuta della RDA è impostata la uso, altrimenti prendo quella del fornitore
		ls_cod_valuta_rda = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_valuta")
		if not isnull(ls_cod_valuta_rda) then
			ls_cod_valuta = ls_cod_valuta_rda
		end if
		
		if ls_cod_valuta <> ls_stringa then
			ld_cambio_acq = wf_cambio_acq(ls_cod_valuta, ldt_oggi)
		else
			ld_cambio_acq = 1
		end if
		
		// ricerca dell'operatore
		select cod_operatore
		into :ls_cod_operatore
		from tab_operatori_utenti
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_utente = :s_cs_xx.cod_utente;
		if sqlca.sqlcode <> 0 then
			setnull(ls_cod_operatore)
		end if
		
		// ricerca del numero max + 1 dell'ordine di acquisto
		ll_index ++
		ll_anno_reg[ll_index] = f_anno_esercizio()
		
		select max(num_registrazione)
		into  :ll_num_reg[ll_index]
		from  tes_ord_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
		      anno_registrazione = :ll_anno_reg[ll_index];
		if isnull(ll_num_reg[ll_index]) or ll_num_reg[ll_index] < 1 then
			ll_num_reg[ll_index] = 1
		else
			ll_num_reg[ll_index] ++
		end if
		
		select flag_doc_suc,
		       flag_st_note
		into   :ls_flag_doc_suc,
		       :ls_flag_st_note
		from   tab_tipi_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca tipo ordine di acquisto " + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		// stefanop RDT
		if not isnull(ls_cod_deposito_trasf) and ls_cod_deposito_trasf <> "" then
			
			// deposito dell'rda non quello del fornitore perchè in questo caso
			// il fornitore è fisso ed è GIBUS
			ls_cod_deposito = ls_cod_deposito_rda 
			
			// Il deposito di prelievo è 400 + la prima lettere del dep di arrivo
			//ls_cod_deposito_trasf = left(ls_cod_deposito_trasf, 2) + left(ls_cod_deposito, 1)
		else
			
			setnull(ls_cod_deposito_trasf)
			
		end if
		
		//-----------claudia 19/07/07 se ha scelto di aggiungere le righe all'ordine già esistente allora questo non glielo faccio fare, ma gli imposto anno, numero dell'ordine
		if ll_testata = 0 then
			INSERT INTO tes_ord_acq  
					( cod_azienda,   
					  anno_registrazione,   
					  num_registrazione,   
					  cod_tipo_ord_acq,   
					  data_registrazione,   
					  cod_operatore,   
					  cod_fornitore,
					  cod_des_fornitore,
					  rag_soc_1,
					  rag_soc_2,
					  indirizzo,
					  cap,
					  frazione,
					  localita,
					  provincia,
					  cod_deposito,   
					  cod_valuta,   
					  cambio_acq,   
					  cod_tipo_listino_prodotto,   
					  cod_pagamento,   
					  sconto,   
					  cod_banca_clien_for,   
					  cod_imballo,   
					  cod_vettore,   
					  cod_inoltro,   
					  cod_mezzo,   
					  cod_porto,   
					  cod_resa,   
					  flag_blocco,   
					  flag_evasione,    
					  cod_banca,      
					  flag_doc_suc_tes,   
					  flag_doc_suc_pie,   
					  flag_st_note_tes,   
					  flag_st_note_pie,
					  nota_testata,
					  nota_piede,
					  data_consegna,
					  data_consegna_fornitore,
					  cod_deposito_trasf)
			VALUES ( :s_cs_xx.cod_azienda,   
					  :ll_anno_reg[ll_index],   
					  :ll_num_reg[ll_index],   
					  :ls_cod_tipo_ord_acq,   
					  :ldt_oggi,   
					  :ls_cod_operatore,   
					  :ls_cod_fornitore,   
					  :ls_cod_des_fornitore,
					  :ls_rag_soc_1,
					  :ls_rag_soc_2,
					  :ls_indirizzo,
					  :ls_cap,
					  :ls_frazione,
					  :ls_localita,
					  :ls_provincia,
					  :ls_cod_deposito,   
					  :ls_cod_valuta,   
					  :ld_cambio_acq,   
					  :ls_cod_tipo_listino_prodotto,   
					  :ls_cod_pagamento,   
					  :ld_sconto_testata,   
					  :ls_cod_banca_clien_for,   
					  :ls_cod_imballo,   
					  :ls_cod_vettore,   
					  :ls_cod_inoltro,   
					  :ls_cod_mezzo,   
					  :ls_cod_porto,   
					  :ls_cod_resa,   
					  'N',   
					  'A',    
					  :ls_cod_banca,
					  :ls_flag_doc_suc,   
					  :ls_flag_doc_suc,   
					  :ls_flag_st_note,   
					  :ls_flag_st_note,
					  :ls_nota_testata,
					  :ls_nota_piede,
					  NULL,
					  NULL,
					  :ls_cod_deposito_trasf);
					  
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in INSERT della testata ordini.~r~n" + sqlca.sqlerrtext)
					rollback;
					return
				end if
			else
				ll_anno_reg[ll_index] = ll_anno_ordine_scelto 
				ll_num_reg[ll_index] = ll_num_ordine_scelto
			end if
				
	end if

	ls_cod_prodotto = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_prodotto")
	ls_des_prodotto = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"des_prodotto")
	ld_quan_acquisto = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"quan_ordinata")
	ld_prezzo_acquisto = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"prezzo_acquisto")
	ll_anno_reg_rda = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"anno_reg_rda")
	ll_num_reg_rda = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"num_reg_rda")
	ll_prog_riga_rda = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"prog_riga_rda")
	ll_anno_commessa = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"anno_commessa")
	ll_num_commessa = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i,"num_commessa")
	ls_cod_centro_costo = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"cod_centro_costo")
	ldt_data_consegna = dw_ext_gen_ord_acq_rda.getitemdatetime(ll_i,"data_disponibilita")
	ls_des_specifica = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"des_specifica")
	ls_note = dw_ext_gen_ord_acq_rda.getitemstring(ll_i,"note")
	
	select anno_reg_ord_ven, num_reg_ord_ven, prog_riga_ord_ven
	into :ll_anno_reg_ord_ven, :ll_num_reg_ord_ven, :ll_prog_riga_ord_ven
	from det_rda
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_reg_rda and
			num_registrazione = :ll_num_reg_rda and
			prog_riga_rda = :ll_prog_riga_rda;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca riga RDA originale~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	// la riga RDA deriva da una riga d'ordine di vendita
	// pertanto mi tengo il prezzo di vendita, ma aplico lo sconto del fornitore
	lb_provenienza_ordine_vendita=false
	
	if not isnull(ll_anno_reg_ord_ven) and ls_flag_copia_prezzo_ord_ven ="S" then
		select prezzo_vendita, 
				fat_conversione_ven,
				cod_misura
		into	:ld_prezzo_vendita,  
				:ld_fat_conversione_ven,
				:ls_cod_misura_ven
		from 	det_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_reg_ord_ven and
				num_registrazione = :ll_num_reg_ord_ven and
				prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode = 0 then
			lb_provenienza_ordine_vendita = true
		end if
		
	end if
	
	if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
		ls_cod_tipo_dettaglio = ls_cod_tipo_det_acq_des
	else
		ls_cod_tipo_dettaglio = ls_cod_tipo_det_acq
	end if
		
	select cod_iva,
			 flag_doc_suc_bl,
			 flag_st_note,
			 cod_nota_prodotto
	into   :ls_cod_iva_det,
			 :ls_flag_doc_suc_bl,
			 :ls_flag_st_note_det,
			 :ls_cod_nota_prodotto
	from   tab_tipi_det_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_det_acq = :ls_cod_tipo_dettaglio;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca codice iva nella tabella tipi dettagli acquisto ~r~n" + sqlca.sqlerrtext )
		rollback;
		return
	end if
	
	if not isnull(ls_cod_prodotto) then
				
		// leggo dati da anag_prodotti
		select cod_misura_mag,
				 fat_conversione_acq,
				 cod_iva
		into   :ls_cod_misura,
				 :ld_fat_conversione,
				 :ls_cod_iva
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotto " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext )
			rollback;
			return
		end if
		
		if isnull(ls_cod_iva) then
			if isnull(ls_cod_iva_det) then
				g_mb.messagebox("APICE","Il prodotto " + ls_cod_prodotto +" non ha alcuna aliquota iva indicata in anagrafica")
				rollback;
				return
			else
				ls_cod_iva = ls_cod_iva_det
			end if
		end if
		
		// cerco codice prodotto del fornitore
		select 	cod_prod_fornitore
		into   	:ls_cod_prod_fornitore
		from   	tab_prod_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 cod_fornitore = :ls_cod_fornitore;
		if sqlca.sqlcode = 100 then setnull(ls_cod_prod_fornitore)
		
		if not isnull(ls_cod_nota_prodotto) and ls_cod_nota_prodotto <> "" then
		
			setnull(ls_nota_prodotto)
		
			select 	nota_prodotto
			into    	:ls_nota_prodotto
			from	anag_prodotti_note
			where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto and
					cod_nota_prodotto = :ls_cod_nota_prodotto;
					
			if isnull(ls_nota_prodotto) then ls_nota_prodotto = ""
			
		end if
		
		
		setnull(ldt_data_inizio_val_for)
		
		select max(data_inizio_val)
		into :ldt_data_inizio_val_for
		from listini_fornitori
		where cod_azienda = :s_cs_xx.cod_azienda and
		       	 cod_fornitore = :ls_cod_fornitore and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in ricerca listino fornitori " + ls_cod_prodotto + ".", sqlca.sqlerrtext )
			rollback;
			return
		end if
		
		if not isnull(ldt_data_inizio_val_for) then
		
			// NUOVO PREZZO
			wf_calcola_prezzo_fornitore(ls_cod_prodotto, ls_cod_fornitore, ld_quan_acquisto, ls_cod_tipo_dettaglio, &
													date(ldt_data_inizio_val_for), ref ld_prezzo_acquisto_1, ref ld_sconto_acquisto_1,ref ld_fat_conversione_for,  ref ls_cod_misura_for)
			
			/*select cod_misura, 
					fat_conversione,
					prezzo_1,
					sconto_1
			into   	:ls_cod_misura_for,
					:ld_fat_conversione_for,
					:ld_prezzo_acquisto_1,
					:ld_sconto_acquisto_1
			from   	listini_fornitori
			where  cod_azienda     = :s_cs_xx.cod_azienda and
					cod_fornitore   = :ls_cod_fornitore and
					cod_prodotto    = :ls_cod_prodotto and
					cod_valuta	  = :ls_cod_valuta and
					data_inizio_val = :ldt_data_inizio_val_for;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in ricerca listino fornitori " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext )
				rollback;
				return
			end if*/
			
			ld_prezzo_acquisto = ld_prezzo_acquisto_1
			ld_sconto_1 = ld_sconto_acquisto_1
			
			if lb_provenienza_ordine_vendita then
				// se provengo dall'ordine di vendita mi tengo il prezzo, ma applico lo sconto fornitore
				ls_cod_misura = ls_cod_misura_ven
				ld_fat_conversione = ld_fat_conversione_ven
				ld_prezzo_acquisto = ld_prezzo_vendita
				ld_sconto_1 = ld_sconto_acquisto_1
			else
				if not isnull(ls_cod_misura_for) then
					ls_cod_misura = ls_cod_misura_for
					ld_fat_conversione = ld_fat_conversione_for
				end if
			end if		
		end if	 
		
	else
		setnull(ls_cod_misura)
		setnull(ls_cod_prod_fornitore)
		ld_fat_conversione = 1
		ls_cod_iva = ls_cod_iva_det
		ls_nota_prodotto = ""
	end if		

	if isnull(ls_note) then ls_note = ""
	ls_note += " " + ls_nota_prodotto
		
	// cerco la il prossimo numero di riga ordine
	select max(prog_riga_ordine_acq)
	into   :ll_prog_riga_ord_acq
	from   det_ord_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_reg[ll_index] and
			num_registrazione = :ll_num_reg[ll_index];
	if isnull(ll_prog_riga_ord_acq) or ll_prog_riga_ord_acq < 1 then
		ll_prog_riga_ord_acq = 10
	else
		ll_prog_riga_ord_acq = ll_prog_riga_ord_acq + 10
	end if
	
	// procedo con la creazione delle righe d'ordine acquisto

	INSERT INTO det_ord_acq  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_ordine_acq,   
           cod_tipo_det_acq,   
           cod_prodotto,   
           cod_misura,   
           des_prodotto,   
           quan_ordinata,   
           prezzo_acquisto,   
           fat_conversione,   
           sconto_1,   
           sconto_2,   
           sconto_3,   
           cod_iva,   
           data_consegna,   
           cod_prod_fornitore,   
           quan_arrivata,   
           val_riga,   
           flag_saldo,   
           flag_blocco,   
           nota_dettaglio,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_off_acq,   
           num_off_acq,   
           prog_riga_off_acq,   
           data_consegna_fornitore,   
           num_confezioni,   
           num_pezzi_confezione,   
           flag_doc_suc_det,   
           flag_st_note_det,   
           imponibile_iva,   
           imponibile_iva_valuta,   
           anno_reg_rda,   
           num_reg_rda,   
           prog_riga_rda,
			  des_specifica)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_reg[ll_index],   
           :ll_num_reg[ll_index],   
           :ll_prog_riga_ord_acq,
           :ls_cod_tipo_dettaglio,   
           :ls_cod_prodotto,   
           :ls_cod_misura,   
           :ls_des_prodotto,   
           :ld_quan_acquisto,   
           :ld_prezzo_acquisto,   
           :ld_fat_conversione,   
           :ld_sconto_1,   
           :ld_sconto_2,   
           :ld_sconto_3,   
           :ls_cod_iva,   
           :ldt_data_consegna,   
           :ls_cod_prod_fornitore,   
           0,   
           0,   
           'N',   
           'N',   
           :ls_note,   
           :ll_anno_commessa,   
           :ll_num_commessa,   
           :ls_cod_centro_costo,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           :ldt_data_consegna,   
           0,   
           0,   
		:ls_flag_doc_suc_bl,
		:ls_flag_st_note_det,
           0,   
           0,   
           :ll_anno_reg_rda,   
           :ll_num_reg_rda,   
           :ll_prog_riga_rda,
			  :ls_des_specifica);
			  
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in INSERT della riga ordine.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	
		// aggiorno magazzino ed evado la RDA
		select flag_tipo_det_acq
		into  :ls_flag_tipo_det_acq
		from  tab_tipi_det_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_tipo_det_acq = :ls_cod_tipo_dettaglio;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca FLAG tipo dettaglio acquisto.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		if ls_flag_tipo_det_acq = "M" then		
			update anag_prodotti
			set quan_ordinata = quan_ordinata + :ld_quan_acquisto
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in aggiornamento quantità ordinata nel prodotto "+ls_cod_prodotto+".~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if		
		
		update det_rda
		set flag_gen_ord_acq = 'S'
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_reg_rda and
				num_registrazione  = :ll_num_reg_rda and
				prog_riga_rda      = :ll_prog_riga_rda;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in aggiornamento flag_generato ordine nella RDA del prodotto "+ls_cod_prodotto+".~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	
	// imposto fornitore per valutare eventuali cambiamenti
	ls_cod_fornitore_old = ls_cod_fornitore
next

ls_stringa = ""
for ll_i = 1 to ll_index
	ls_stringa += "~r~n" + string(ll_anno_reg[ll_i]) + "/" + string(ll_num_reg[ll_i])
next

g_mb.messagebox("APICE","RDA elaborate correttamente generando i seguenti ordini: " + ls_stringa)
commit;
dw_ext_gen_ord_acq_rda.reset()

cb_ricerca.postevent("clicked")
end event

type gb_1 from groupbox within w_evasione_rda
integer x = 27
integer y = 2504
integer width = 1531
integer height = 180
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo di Fornitore"
end type

type dw_ext_selezione_evas_rda from u_dw_search within w_evasione_rda
integer x = 146
integer y = 60
integer width = 2839
integer height = 952
integer taborder = 20
string dataobject = "d_ext_selezione_evas_rda"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;if isvalid(dwo) then
	
	choose case dwo.name
			
		case "b_ricerca_fornitore"
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_fornitore(dw_ext_selezione_evas_rda,"cod_fornitore")
			
		case "b_ricerca_operaio"
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_operaio(dw_ext_selezione_evas_rda,"cod_operaio")
			
		case "b_ricerca_prodotto"
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_prodotto(dw_ext_selezione_evas_rda,"cod_prodotto")
			
	end choose
	
end if
end event

type dw_ext_gen_ord_acq_rda from uo_std_dw within w_evasione_rda
event ue_keydown pbm_dwnkey
integer x = 27
integer y = 1324
integer width = 4608
integer height = 1168
integer taborder = 50
string dataobject = "d_ext_gen_ord_acq_rda"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event doubleclicked;/** TOLTO ANCESTOR **/

string ls_cod_colore_tessuto,ls_des_colore,ls_colname,ls_cod_parametro

//setnull(ls_cod_parametro)
//
//select cod_parametro
//into   :ls_cod_parametro
//from   parametri_azienda
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_parametro = 'CSO';
//	
//if sqlca.sqlcode = 0 and ls_cod_parametro = "CSO" then
if not isnull(is_parametro_cso) and is_parametro_cso <> "" then

	if dwo.name = "des_specifica" then

			ls_colname = dwo.name
			
			ls_cod_colore_tessuto = getitemstring(row,ls_colname)
			
			select des_colore_tessuto
			into   :ls_des_colore
			from   tab_colori_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_colore_tessuto = :ls_cod_colore_tessuto ;
					 
			messagebox("DESCRIZIONE COLORE",ls_des_colore)		 
		else
	
			this.deleterow(row)
			//Reset_DW_Modified(c_ResetChildren)
			
		end if
	
else
	
	this.deleterow(row)
	//Reset_DW_Modified(c_ResetChildren)
	
end if
end event

event itemchanged;call super::itemchanged;
string ls_rag_soc_1
	
choose case dwo.name
		
	case "cod_fornitore"
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :data;
				 
		if sqlca.sqlcode <> 0 then setnull(ls_rag_soc_1)
		
		setitem(getrow(),"rag_soc_1",ls_rag_soc_1)
		
		
end choose

end event

event rowfocuschanged;call super::rowfocuschanged;string			ls_cod_prodotto

if currentrow>0 then
else
	return
	
end if


ls_cod_prodotto = getitemstring(currentrow, "cod_prodotto")

if isnull(ls_cod_prodotto) then ls_cod_prodotto=""
wf_carica_drop_fornitore(ls_cod_prodotto)

end event

event buttonclicked;call super::buttonclicked;

if row < 1 then return 0

choose case dwo.name
	case "b_sel_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ext_gen_ord_acq_rda, "cod_fornitore")
		
end choose
end event

type dw_evas_rda_lista_rda from uo_cs_xx_dw within w_evasione_rda
integer x = 123
integer y = 56
integer width = 4448
integer height = 1084
integer taborder = 10
string dataobject = "d_evas_rda_lista_rda_grid"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG LL_ERRORE

ll_errore = retrieve(s_cs_xx.cod_azienda)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event doubleclicked;call super::doubleclicked;boolean			lb_stop
string				ls_cod_fornitore, ls_ultimo_fornitore, ls_sovrascrivi, ls_msg
long				ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_rda, ll_i, ll_num_rows,ll_riga, ll_riga_new, ll_y, ll_x

if row > 0 then
	
//	//S se deve sovrascrivere sempre e tacitamente
//	//N se poma di sovrascrivere chiede (se fornitore impostato è diverso da ultimo)
//	ls_sovrascrivi = dw_ext_selezione_evas_rda.getitemstring(dw_ext_selezione_evas_rda.getrow(), "flag_sovrascrivi_forn")
	
	if dwo.name <> "num_registrazione" then
	
		ll_anno_registrazione = this.getitemnumber(row, "anno_registrazione")
		ll_num_registrazione = this.getitemnumber(row, "num_registrazione")
		ll_prog_riga_rda = this.getitemnumber(row, "prog_riga_rda")
		
		
		for ll_i = 1 to dw_ext_gen_ord_acq_rda.rowcount()
			if ll_anno_registrazione = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i, "anno_reg_rda") and &
				ll_num_registrazione  = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i, "num_reg_rda") and &
				ll_prog_riga_rda  = dw_ext_gen_ord_acq_rda.getitemnumber(ll_i, "prog_riga_rda") then
					g_mb.messagebox("APICE","Questa riga RDA è già stata spostata nella lista sottostante.",Information!)
					return
			end if
		next
		
		wf_inserisci_riga(row)

	else
		
		ll_anno_registrazione = this.getitemnumber(row, "anno_registrazione")
		ll_num_registrazione = this.getitemnumber(row, "num_registrazione")
		ll_prog_riga_rda = this.getitemnumber(row, "prog_riga_rda")
		
		for ll_y = 1 to rowcount()
			
			if ll_anno_registrazione = this.getitemnumber(ll_y, "anno_registrazione") and ll_num_registrazione = this.getitemnumber(ll_y, "num_registrazione") then
		
				// inserisce effettivamente la riga
				ll_riga = ll_y
				lb_stop = false
				
				for ll_x = 1 to dw_ext_gen_ord_acq_rda.rowcount()
					if dw_evas_rda_lista_rda.getitemnumber(ll_riga, "anno_registrazione") = dw_ext_gen_ord_acq_rda.getitemnumber(ll_x, "anno_reg_rda") and dw_evas_rda_lista_rda.getitemnumber(ll_riga, "num_registrazione")  = dw_ext_gen_ord_acq_rda.getitemnumber(ll_x, "num_reg_rda") and dw_evas_rda_lista_rda.getitemnumber(ll_riga, "prog_riga_rda")  = dw_ext_gen_ord_acq_rda.getitemnumber(ll_x, "prog_riga_rda") then
							g_mb.messagebox("APICE","L'RDA  " + string(dw_evas_rda_lista_rda.getitemnumber(ll_riga, "anno_registrazione")) + " - " + string(dw_evas_rda_lista_rda.getitemnumber(ll_riga, "num_registrazione") ) + " - " + string(dw_evas_rda_lista_rda.getitemnumber(ll_riga, "prog_riga_rda"))+ " è già stata spostata nella lista sottostante.",Information!)
							lb_stop = true
					end if
				next
				
				if lb_stop then continue
				
				wf_inserisci_riga(ll_riga)
		
			end if
		next
	end if
end if

end event

type dw_folder_search from u_folder within w_evasione_rda
integer x = 23
integer y = 20
integer width = 4622
integer height = 1144
integer taborder = 50
end type

