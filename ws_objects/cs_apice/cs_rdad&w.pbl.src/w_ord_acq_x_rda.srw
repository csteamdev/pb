﻿$PBExportHeader$w_ord_acq_x_rda.srw
$PBExportComments$elenca gli ordini di acquisto inerenti un fornitore
forward
global type w_ord_acq_x_rda from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_ord_acq_x_rda
end type
type cb_genera from commandbutton within w_ord_acq_x_rda
end type
type dw_ordini from uo_dw_main within w_ord_acq_x_rda
end type
end forward

global type w_ord_acq_x_rda from w_cs_xx_principale
integer width = 2240
integer height = 1340
string title = "ORDINI DI ACQUISTO"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_annulla cb_annulla
cb_genera cb_genera
dw_ordini dw_ordini
end type
global w_ord_acq_x_rda w_ord_acq_x_rda

type variables
string is_cod_ord_acq, is_cod_fornitore 
end variables

on w_ord_acq_x_rda.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_genera=create cb_genera
this.dw_ordini=create dw_ordini
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_genera
this.Control[iCurrent+3]=this.dw_ordini
end on

on w_ord_acq_x_rda.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_genera)
destroy(this.dw_ordini)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)
//dw_ordini.set_dw_options(sqlca, &
//                                       pcca.null_object, &
//                                       c_nomodify + &
//								   c_noretrieveonopen + &
//                     		            c_nodelete + &
//                                       c_newonopen + &
//                                       c_disableCC, &
//                                       c_noresizedw + &
//                                       c_nohighlightselected + &
//                                       c_nocursorrowpointer +&
//                                       c_nocursorrowfocusrect )
dw_ordini.set_dw_options(sqlca, &
							pcca.null_object, &
							c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete, &
							c_default)
end event

event open;call super::open;long ll_errore

//is_cod_ord_acq = s_cs_xx.parametri.parametro_s_1 
is_cod_fornitore = s_cs_xx.parametri.parametro_s_2
ll_errore = dw_ordini.retrieve(s_cs_xx.cod_azienda, is_cod_fornitore)//,  is_cod_ord_acq)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type cb_annulla from commandbutton within w_ord_acq_x_rda
integer x = 1417
integer y = 1136
integer width = 375
integer height = 84
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Annulla"
boolean cancel = true
end type

event clicked;
s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_i_2 = 0
close(w_ord_acq_x_rda)
end event

type cb_genera from commandbutton within w_ord_acq_x_rda
integer x = 1824
integer y = 1136
integer width = 375
integer height = 84
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "OK"
end type

event clicked;long ll_num_ordine, ll_anno_ordine

s_cs_xx.parametri.parametro_i_1 = dw_ordini.getitemnumber(dw_ordini.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_i_2 = dw_ordini.getitemnumber(dw_ordini.getrow(), "num_registrazione")

close(w_ord_acq_x_rda)
end event

type dw_ordini from uo_dw_main within w_ord_acq_x_rda
integer x = 27
integer y = 24
integer width = 2181
integer height = 1092
integer taborder = 10
string dataobject = "d_ord_acq_x_rda"
boolean vscrollbar = true
end type

