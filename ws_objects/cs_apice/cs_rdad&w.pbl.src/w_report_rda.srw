﻿$PBExportHeader$w_report_rda.srw
forward
global type w_report_rda from w_cs_xx_principale
end type
type dw_report_rda from uo_cs_xx_dw within w_report_rda
end type
type dw_folder from u_folder within w_report_rda
end type
type dw_sel_report_rda from uo_cs_xx_dw within w_report_rda
end type
type cb_aggiorna from commandbutton within w_report_rda
end type
end forward

global type w_report_rda from w_cs_xx_principale
integer width = 3762
integer height = 2240
string title = "Report Richieste di Approvvigionamento"
event ue_setdwcolor ( )
dw_report_rda dw_report_rda
dw_folder dw_folder
dw_sel_report_rda dw_sel_report_rda
cb_aggiorna cb_aggiorna
end type
global w_report_rda w_report_rda

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

windowobject lw_oggetti[]

dw_sel_report_rda.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_sel_report_rda.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

dw_report_rda.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nonew + &
													c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )
													
// *** folder													

lw_oggetti[1] = dw_report_rda
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

lw_oggetti[1] = dw_sel_report_rda
lw_oggetti[2] = cb_aggiorna
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)
													
dw_report_rda.object.datawindow.print.preview = 'Yes'
end event

on w_report_rda.create
int iCurrent
call super::create
this.dw_report_rda=create dw_report_rda
this.dw_folder=create dw_folder
this.dw_sel_report_rda=create dw_sel_report_rda
this.cb_aggiorna=create cb_aggiorna
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_rda
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_sel_report_rda
this.Control[iCurrent+4]=this.cb_aggiorna
end on

on w_report_rda.destroy
call super::destroy
destroy(this.dw_report_rda)
destroy(this.dw_folder)
destroy(this.dw_sel_report_rda)
destroy(this.cb_aggiorna)
end on

type dw_report_rda from uo_cs_xx_dw within w_report_rda
event ue_post_retrieve ( )
integer x = 46
integer y = 140
integer width = 3611
integer height = 1920
integer taborder = 0
string dataobject = "d_report_rda"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;long ll_errore, ll_anno_commessa, ll_num_commessa

dw_sel_report_rda.accepttext()

ll_anno_commessa = dw_sel_report_rda.getitemnumber( 1, "anno_commessa")
ll_num_commessa = dw_sel_report_rda.getitemnumber( 1, "num_commessa")

ll_errore = retrieve( s_cs_xx.cod_azienda, ll_anno_commessa, ll_num_commessa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder from u_folder within w_report_rda
integer x = 23
integer width = 3680
integer height = 2100
integer taborder = 10
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Report"
      SetFocus(dw_report_rda)
		
   CASE "Selezione"
      SetFocus(dw_sel_report_rda)

END CHOOSE

end event

type dw_sel_report_rda from uo_cs_xx_dw within w_report_rda
integer x = 46
integer y = 140
integer width = 3611
integer height = 160
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_ext_selezione_report_rda"
boolean border = false
end type

type cb_aggiorna from commandbutton within w_report_rda
integer x = 3269
integer y = 160
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;dw_report_rda.change_dw_current()
setpointer(hourglass!)
dw_report_rda.setredraw(false)
parent.triggerevent("pc_retrieve")
dw_report_rda.setredraw(true)
dw_folder.fu_selecttab(2)
setpointer(arrow!)
end event

