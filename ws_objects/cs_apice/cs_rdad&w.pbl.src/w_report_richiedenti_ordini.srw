﻿$PBExportHeader$w_report_richiedenti_ordini.srw
$PBExportComments$Finestra Report Ordini di Acquisto
forward
global type w_report_richiedenti_ordini from w_cs_xx_principale
end type
type st_1 from statictext within w_report_richiedenti_ordini
end type
type dw_selezione from uo_cs_xx_dw within w_report_richiedenti_ordini
end type
type dw_report from uo_cs_xx_dw within w_report_richiedenti_ordini
end type
type cb_annulla from commandbutton within w_report_richiedenti_ordini
end type
type cb_report from commandbutton within w_report_richiedenti_ordini
end type
end forward

global type w_report_richiedenti_ordini from w_cs_xx_principale
integer width = 3598
integer height = 2068
string title = "Report Richiedenti Ordini Acquisto"
st_1 st_1
dw_selezione dw_selezione
dw_report dw_report
cb_annulla cb_annulla
cb_report cb_report
end type
global w_report_richiedenti_ordini w_report_richiedenti_ordini

event pc_setwindow;call super::pc_setwindow;string ls_des_azienda

dw_report.ib_dw_report = true

select rag_soc_1
  into :ls_des_azienda
  from aziende
 where cod_azienda = :s_cs_xx.cod_azienda;
 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Descrizione Azienda non trovata")
end if

if sqlca.sqlcode = 0 then
	dw_report.object.st_azienda.text = ls_des_azienda
end if	

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
                            
save_on_close(c_socnosave)
iuo_dw_main = dw_report

end event

on w_report_richiedenti_ordini.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_report
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_report
end on

on w_report_richiedenti_ordini.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_annulla)
destroy(this.cb_report)
end on

type st_1 from statictext within w_report_richiedenti_ordini
integer x = 3150
integer y = 36
integer width = 379
integer height = 92
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_richiedenti_ordini
integer y = 20
integer width = 3141
integer height = 452
integer taborder = 40
string dataobject = "d_sel_report_richiedenti_ordini"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_null
date ld_null

setnull(ls_null)
setnull(ld_null)

//dw_selezione.insertrow(0)

dw_selezione.setitem(1, "cod_fornitore", ls_null)
dw_selezione.setitem(1, "cod_prodotto", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "anno", f_anno_esercizio())
dw_selezione.setitem(1, "num_ordine_da", 1)
dw_selezione.setitem(1, "num_ordine_a", 999999)
dw_selezione.setitem(1, "flag_blocco", 'T')
dw_selezione.setitem(1, "flag_blocco_righe", 'T')
dw_selezione.setitem(1, "flag_evasione", 'A')
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "flag_blocco"
			if i_coltext = "S" then
				dw_selezione.setitem(dw_selezione.getrow(),"flag_blocco_righe","T")
				dw_selezione.Object.flag_blocco_righe.Protect = 1
			else
				dw_selezione.Object.flag_blocco_righe.Protect = 0
			end if
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_richiedenti_ordini
integer x = 23
integer y = 476
integer width = 3520
integer height = 1472
integer taborder = 50
string dataobject = "d_report_richiedenti_ord_acq_rda"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_annulla from commandbutton within w_report_richiedenti_ordini
integer x = 3173
integer y = 376
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null
date ld_null

setnull(ls_null)
setnull(ld_null)

//dw_selezione.insertrow(0)
dw_selezione.setitem(1, "anno", f_anno_esercizio())
dw_selezione.setitem(1, "cod_fornitore", ls_null)
dw_selezione.setitem(1, "cod_prodotto", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "data_consegna_da", ld_null)
dw_selezione.setitem(1, "data_consegna_a", ld_null)
dw_selezione.setitem(1, "num_ordine_da", 1)
dw_selezione.setitem(1, "num_ordine_a", 999999)
dw_selezione.setitem(1, "flag_blocco", 'T')
dw_selezione.setitem(1, "flag_blocco_righe", 'T')
dw_selezione.setitem(1, "flag_evasione", 'A')
end event

type cb_report from commandbutton within w_report_richiedenti_ordini
integer x = 3173
integer y = 276
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_fornitore_sel, ls_cod_prodotto_sel, ls_flag_blocco_sel, ls_flag_evasione_sel, ls_cod_prodotto, ls_des_prodotto, &
       ls_unita_misura, ls_iva, ls_stato, ls_tipo_listino, ls_valuta, ls_pagamento, ls_fornitore, ls_stato_ordine, ls_flag_saldo,&
		 ls_des_pagamento, ls_cod_tipo_det_acq,ls_flag_tipo_det_acq, ls_dettaglio, ls_testata, ls_rag_soc_1, ls_da, ls_a, ls_formato, &
		 ls_flag_blocco_righe_sel, ls_cod_operatore, ls_cod_operaio, ls_des_operaio, ls_des_operatore
		 
long   ll_prog_riga, ll_confezioni, ll_pezzi_x_confezione, ll_num_ordine, ll_anno_ordine, ll_i, ll_anno_sel, ll_num_ordine_da_sel, &
       ll_num_ordine_a_sel, ll_anno_reg_rda, ll_num_reg_rda, ll_prog_riga_rda
		 
dec{4} ld_qta_ordine, ld_residuo_ordine, ld_prezzo_acq, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
		 ld_sconto_4,  ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_valore_riga, ld_sconto, ld_quan_arrivata, ld_sconto_totale, ld_cambio_acq
		 
datetime ldt_data_consegna, ldt_data_consegna_for, ldt_data_registrazione, ldt_da_data_consegna, ldt_a_data_consegna, &
         ldt_data_da_sel, ldt_data_a_sel
			
datastore lds_report
			
uo_calcola_sconto iuo_calcola_sconto

dw_report.Reset() 
dw_report.setredraw(false)
dw_selezione.accepttext()
ls_cod_fornitore_sel = dw_selezione.getitemstring(1, "cod_fornitore")
ls_cod_prodotto_sel = dw_selezione.getitemstring(1, "cod_prodotto")
ls_flag_blocco_sel = dw_selezione.getitemstring(1, "flag_blocco")
ls_flag_blocco_righe_sel = dw_selezione.getitemstring(1, "flag_blocco_righe")
ls_flag_evasione_sel = dw_selezione.getitemstring(1, "flag_evasione")
ldt_data_da_sel = dw_selezione.getitemdatetime(1, "data_da")
ldt_data_a_sel = dw_selezione.getitemdatetime(1, "data_a")
ll_anno_sel = dw_selezione.getitemnumber(1, "anno")
ll_num_ordine_da_sel = dw_selezione.getitemnumber(1, "num_ordine_da")
ll_num_ordine_a_sel = dw_selezione.getitemnumber(1, "num_ordine_a")
ldt_da_data_consegna = dw_selezione.getitemdatetime(1, "data_consegna_da")
ldt_a_data_consegna = dw_selezione.getitemdatetime(1, "data_consegna_a")
if isnull(ldt_da_data_consegna) then ldt_da_data_consegna = datetime(date("01/01/1900"), 00:00:00)
if isnull(ldt_a_data_consegna) then ldt_a_data_consegna = datetime(date("31/12/2099"), 00:00:00)
ll_i = 0

declare cu_testata dynamic cursor for sqlsa;

ls_testata = "select anno_registrazione, num_registrazione, data_registrazione, cod_tipo_listino_prodotto, cod_valuta, cod_pagamento, cod_fornitore, flag_evasione, sconto, cod_operatore from tes_ord_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_fornitore_sel) then
	ls_testata = ls_testata + " and cod_fornitore = '" + ls_cod_fornitore_sel + "'"
end if

if not isnull(ls_flag_blocco_sel) and ls_flag_blocco_sel <> "T" then
	ls_testata = ls_testata + " and flag_blocco = '" + ls_flag_blocco_sel + "'"
end if

ls_da = string(ldt_data_da_sel, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_data_da_sel) then
	ls_testata = ls_testata + " and data_registrazione >= '" + ls_da + "'"
end if

ls_a = string(ldt_data_a_sel, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_data_a_sel) then
	ls_testata = ls_testata + " and data_registrazione <= '" + string(ls_a) + "'"
end if

if not isnull(ll_anno_sel) then
	ls_testata = ls_testata + " and anno_registrazione = " + string(ll_anno_sel) + " "
end if

if not isnull(ll_num_ordine_da_sel) then
	ls_testata = ls_testata + " and num_registrazione >= " + string(ll_num_ordine_da_sel) + " "
end if

if not isnull(ll_num_ordine_a_sel) then
	ls_testata = ls_testata + " and num_registrazione <= " + string(ll_num_ordine_a_sel) + " "
end if

prepare sqlsa from :ls_testata;

open dynamic cu_testata;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_testata. " + sqlca.sqlerrtext)
	rollback;
	return
end if

do while 1=1
   fetch cu_testata into :ll_anno_ordine, :ll_num_ordine, :ldt_data_registrazione, :ls_tipo_listino, :ls_valuta, :ls_pagamento, :ls_fornitore, :ls_stato_ordine, :ld_sconto, :ls_cod_operatore;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		
		select cambio_acq,
				 formato
		into   :ld_cambio_acq,
				 :ls_formato
		from   tab_valute
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_valuta = :ls_valuta;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
			return -1
		end if	
		
		st_1.text = string(ll_anno_ordine)+"/"+string(ll_num_ordine)
		declare cu_dettaglio dynamic cursor for sqlsa;
		ls_dettaglio = "select prog_riga_ordine_acq, cod_tipo_det_acq, cod_prodotto, des_prodotto, cod_misura, quan_ordinata, (quan_ordinata - quan_arrivata), prezzo_acquisto, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9,	 sconto_10, imponibile_iva_valuta, cod_iva, data_consegna, data_consegna_fornitore, flag_saldo, quan_arrivata, anno_reg_rda, num_reg_rda, prog_riga_rda from det_ord_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(ll_anno_ordine) + " and num_registrazione = " + string(ll_num_ordine) + " "
		ls_dettaglio = ls_dettaglio + " and ((data_consegna >= '" + string(ldt_da_data_consegna, s_cs_xx.db_funzioni.formato_data) + "'"
		ls_dettaglio = ls_dettaglio + " and data_consegna <= '" + string(ldt_a_data_consegna, s_cs_xx.db_funzioni.formato_data) + "') or data_consegna is null)"
		if not isnull(ls_cod_prodotto_sel) then
			ls_dettaglio = ls_dettaglio + " and cod_prodotto = '" + ls_cod_prodotto_sel + "'"
		end if
		// flag blocco (aggiunto da EnMe 12/3/2002)
		if not isnull(ls_flag_blocco_righe_sel) and ls_flag_blocco_righe_sel <> "T" then
			ls_dettaglio = ls_dettaglio + " and flag_blocco = '" + ls_flag_blocco_righe_sel + "'"
		end if
		// valuto lo stato della riga dell'ordine
		choose case ls_flag_evasione_sel
			case "P"
				ls_dettaglio = ls_dettaglio + " and quan_arrivata > 0 and flag_saldo = 'N' "
			case "A"
				ls_dettaglio = ls_dettaglio + " and (quan_ordinata - quan_arrivata) > 0 and flag_saldo = 'N' "
			case "E"
				ls_dettaglio = ls_dettaglio + " and ((quan_ordinata - quan_arrivata) <= 0 or flag_saldo = 'S') "
		end choose
		prepare sqlsa from :ls_dettaglio;
		open dynamic cu_dettaglio;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_dettagli " + sqlca.sqlerrtext)
			rollback;
			return
		end if

		do while 1=1
			fetch cu_dettaglio into :ll_prog_riga, :ls_cod_tipo_det_acq, :ls_cod_prodotto, :ls_des_prodotto, :ls_unita_misura, :ld_qta_ordine, :ld_residuo_ordine, :ld_prezzo_acq, :ld_sconto_1, :ld_sconto_2, :ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_valore_riga, :ls_iva, :ldt_data_consegna, :ldt_data_consegna_for, :ls_flag_saldo, :ld_quan_arrivata, :ll_anno_reg_rda, :ll_num_reg_rda, :ll_prog_riga_rda;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = 0 then		
				if not isnull(ll_num_ordine) and not isnull(ll_anno_ordine) then
					
					ld_prezzo_acq = ld_prezzo_acq * ld_cambio_acq
					
					ll_i = dw_report.insertrow(0)					
					dw_report.setitem(ll_i, "num_ordine", ll_num_ordine)
					dw_report.setitem(ll_i, "anno_ordine", ll_anno_ordine)
					dw_report.setitem(ll_i, "data_registrazione", ldt_data_registrazione)
					dw_report.setitem(ll_i, "tipo_listino", ls_tipo_listino)
					dw_report.setitem(ll_i, "valuta", ls_valuta)
					setnull(ls_des_operatore)
					if not isnull(ll_anno_reg_rda) then
						select cod_operaio
						into   :ls_cod_operaio
						from   det_rda
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       anno_registrazione = :ll_anno_reg_rda and
								 num_registrazione = :ll_num_reg_rda and
								 prog_riga_rda = :ll_prog_riga_rda;
						if sqlca.sqlcode = 0 then
							select cognome + ',' + nome
							into   :ls_des_operatore
							from   anag_operai
							where  cod_azienda = :s_cs_xx.cod_azienda and
							       cod_operaio = :ls_cod_operaio;
							if sqlca.sqlcode = 0 then
								ls_cod_operatore = ls_cod_operaio
							else
								g_mb.messagebox("APICE","Errore in ricerca operatore cod. " + ls_cod_operaio + "~r~n" + sqlca.sqlerrtext)
							end if
						else
							g_mb.messagebox("APICE","Errore in ricerca codice operatore richiedente nella RDA " +string(ll_anno_reg_rda)+"/"+string(ll_num_reg_rda)+"/"+string(ll_prog_riga_rda) + "~r~n" + sqlca.sqlerrtext)
						end if
						dw_report.setitem(ll_i, "rda", string(ll_anno_reg_rda)+" - "+string(ll_num_reg_rda)+" - "+string(ll_prog_riga_rda))
					else		// non presente FK con RDA allora uso operatore di testata
						if isnull(ls_cod_operatore) then
							ls_cod_operatore = "---"
							ls_des_operatore = "Operatore non precisato !"
						else
							select des_operatore
							into   :ls_des_operatore
							from   tab_operatori
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_operatore = :ls_cod_operatore;
							if sqlca.sqlcode = 0  then
								g_mb.messagebox("APICE","Errore in ricerca descrizione operatore " + ls_cod_operatore + " in tabella operatori ~r~n" + sqlca.sqlerrtext)
							end if
						end if
					end if
					
					dw_report.setitem(ll_i, "cod_operatore", ls_cod_operatore)
										
					dw_report.setitem(ll_i, "formato", ls_formato)
					
					dw_report.setitem(ll_i, "pagamento", ls_pagamento)
					
					select des_pagamento
					into   :ls_des_pagamento
					from   tab_pagamenti
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_pagamento = :ls_pagamento;
					dw_report.setitem(ll_i, "des_pagamento", ls_des_pagamento)
					
					select rag_soc_1
					  into :ls_rag_soc_1
					  from anag_fornitori
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_fornitore = :ls_fornitore;
					
					dw_report.setitem(ll_i, "fornitore", ls_rag_soc_1)
					
					if ls_stato_ordine = "E" then ls_stato_ordine = "Evaso"
					if ls_stato_ordine = "P" then ls_stato_ordine = "Parziale"
					if ls_stato_ordine = "A" then ls_stato_ordine = "Aperto"				
					
					dw_report.setitem(ll_i, "stato_ordine", ls_stato_ordine)
					dw_report.setitem(ll_i, "sconto", ld_sconto)
					
					dw_report.setitem(ll_i, "prog_riga", ll_prog_riga)
					if isnull(ls_cod_prodotto) then ls_cod_prodotto = ""
					dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
					if isnull(ls_des_prodotto) then ls_des_prodotto = ""
					if ls_des_prodotto = "" and not isnull(ls_cod_prodotto) then
						select des_prodotto
						into   :ls_des_prodotto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_prodotto = :ls_cod_prodotto;
					end if				
					
					dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
					dw_report.setitem(ll_i, "unita_misura", ls_unita_misura)
					dw_report.setitem(ll_i, "qta_ordine", ld_qta_ordine)
					dw_report.setitem(ll_i, "residuo_ordine", ld_residuo_ordine)
					dw_report.setitem(ll_i, "prezzo_acq", ld_prezzo_acq)
					
					iuo_calcola_sconto = create uo_calcola_sconto
					ld_sconto_totale = iuo_calcola_sconto.wf_calcola_sconto(ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10)
					destroy iuo_calcola_sconto					
					
					// le righe di sconto le metto in negativo
					select flag_tipo_det_acq
					into   :ls_flag_tipo_det_acq
					from   tab_tipi_det_acq
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_tipo_det_acq = :ls_cod_tipo_det_acq;
					if not isnull(ls_flag_tipo_det_acq) and ls_flag_tipo_det_acq = "S" then  ld_valore_riga = ld_valore_riga * -1
					
					dw_report.setitem(ll_i, "sconto_1", ld_sconto_totale)
					dw_report.setitem(ll_i, "valore_riga", ld_valore_riga)	
					dw_report.setitem(ll_i, "iva", ls_iva)
					dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
					dw_report.setitem(ll_i, "data_consegna_for", ldt_data_consegna_for)
	
					if ls_flag_saldo = "N" and (ld_quan_arrivata = 0 or isnull(ld_quan_arrivata)) then ls_stato = "A"
					if ls_flag_saldo = "N" and ld_quan_arrivata > 0 then ls_stato = "P"
					if ls_flag_saldo = "S" then ls_stato = "E"
					
					dw_report.setitem(ll_i, "stato", ls_stato)				
					
					
				end if	
			end if
		loop
		close cu_dettaglio;
	end if	
loop
close cu_testata;	
rollback;


dw_report.setsort("fornitore A, anno_ordine A, num_ordine A, prog_riga A")
dw_report.sort()
dw_report.groupcalc()

dw_report.setredraw(true)

dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.preview.rulers = 'Yes'
st_1.text = ""

dw_report.change_dw_current()
end event

