﻿$PBExportHeader$w_report_tes_rda.srw
$PBExportComments$finestra report prodotti venduti movimentati
forward
global type w_report_tes_rda from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_tes_rda
end type
type dw_report from uo_cs_xx_dw within w_report_tes_rda
end type
end forward

global type w_report_tes_rda from w_cs_xx_principale
integer width = 3913
integer height = 2484
string title = "Report RDA"
cb_stampa cb_stampa
dw_report dw_report
end type
global w_report_tes_rda w_report_tes_rda

type variables
integer				il_anno_registrazione

long					il_num_registrazione
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1_a[1]
il_num_registrazione = s_cs_xx.parametri.parametro_d_1_a[2]

setnull(s_cs_xx.parametri.parametro_d_1_a[1])
setnull(s_cs_xx.parametri.parametro_d_1_a[2])


set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

iuo_dw_main = dw_report

end event

on w_report_tes_rda.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_report
end on

on w_report_tes_rda.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

type cb_stampa from commandbutton within w_report_tes_rda
integer x = 3438
integer y = 48
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;

dw_report.print(true, true)
end event

type dw_report from uo_cs_xx_dw within w_report_tes_rda
integer x = 23
integer y = 160
integer width = 3817
integer height = 2200
integer taborder = 50
string dataobject = "d_report_tes_rda"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;

long 				ll_errore
string			ls_title, ls_valore
dec{4}			ld_totale



ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

ls_title = "Report RDA N° "+string(il_anno_registrazione)+"/"+string(il_num_registrazione)



if ll_errore < 0 then
   pcca.error = c_fatal
end if

select sum(isnull(quan_richiesta, 0) * isnull(prezzo_acquisto,0))
into :ld_totale
from det_rda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:il_anno_registrazione and
			num_registrazione=:il_num_registrazione;


if isnull(ld_totale) then ld_totale = 0
ls_valore = string(ld_totale, "###,###,##0.00")

object.valore_rda_t.text = ls_valore

parent.title = ls_title
end event

