﻿$PBExportHeader$w_tes_rda.srw
forward
global type w_tes_rda from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from datawindow within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
end forward

global type w_tes_rda from w_cs_xx_treeview
integer width = 4562
integer height = 2656
string title = "Gestione RDA"
event pc_menu_respingi ( )
event pc_menu_invia ( )
event pc_menu_dettagli ( )
event pc_menu_calcola ( )
event pc_menu_stampa ( )
event pc_menu_sospendi ( )
event pc_menu_desospendi ( )
end type
global w_tes_rda w_tes_rda

type variables
private:
	datastore ids_store
	long il_livello
	string is_sql_det
	
	// icone
	int ICONA_RDA, ICONA_RDA_PARZIALE, ICONA_RDA_EVASA, ICONA_RDA_SEL, ICONA_RDA_PARZIALE_SEL, ICONA_RDA_EVASA_SEL
	int ICONA_OPERATORE, ICONA_TIPO_RDA, ICONA_ANNO, ICONA_DATA_REGISTRAZIONE
	int ICONA_DEPOSITO, ICONA_FORNITORE, ICONA_PRODOTTO
	
	boolean ib_new, ib_modify
	
	long il_max_row = 255
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_treeview_icons ()
public subroutine wf_valori_livelli ()
public function long wf_inserisci_rda (long al_handle)
public function long wf_inserisci_tipo_rda (long al_handle)
public function long wf_inserisci_fornitori (long al_handle)
public function long wf_inserisci_anno (long al_handle)
public function long wf_inserisci_depositi (long al_handle)
public function integer wf_inserisci_singola_rda (long al_handle, long al_anno_registrazione, long al_num_registrazione, datetime adt_data_registrazione, string as_flag_stato_autorizzato, string as_cod_valuta)
public function boolean wf_is_rda_node (ref long al_handle)
public function long wf_inserisci_data_registrazione (long al_handle)
public function long wf_inserisci_prodotti (long al_handle)
public function long wf_inserisci_operai (long al_handle)
public function integer wf_invia_respingi (integer ai_anno_rda, long al_num_rda, string as_invia_o_respingi, ref string as_messaggio)
public function integer wf_carica_destinatari (string as_cod_tipo_lista_dist, string as_cod_lista_dist, long al_num_sequenza_invio, ref string as_cod_destinatari[], ref string as_email_destinatari[], ref string as_errore)
public function integer wf_salva_notifica (integer ai_anno_rda, long al_num_rda, string as_cod_destinatario, string as_cod_mittente, long al_num_sequenza_corrente, long al_num_sequenza_arrivo, string as_note, ref string as_errore)
public function integer wf_carica_destinatari (boolean ab_inserimento_rda, string as_cod_tipo_lista_dist, string as_cod_lista_dist, long al_num_sequenza_corrente, string as_invia_o_respingi, ref long al_num_sequenza_arrivo, ref long al_num_sequenza_invio_mail, ref string as_cod_destinatari[], ref string as_email_destinatari[], ref string as_errore)
public function integer wf_controlla_sequenza (boolean ab_inserimento, long al_num_sequenza_corrente, string as_cod_tipo_lista_dist, ref string as_cod_lista_dist, ref string as_cod_mittente, ref string as_email_mittente, ref string as_errore)
public function integer wf_invia_mail (string as_oggetto, string as_testo, string as_email_destinatari[], ref string as_cod_mittente, ref string as_mail_mittente, ref string as_errore)
public function integer wf_mail_insert_rda (integer ai_anno_rda, long al_num_rda, ref string as_messaggio)
public function integer wf_sospendi_desospendi (integer ai_anno_rda, long al_num_rda, string as_flag_sospesa, ref string as_errore)
end prototypes

event pc_menu_respingi();
integer			li_anno_rda, li_ret

long				ll_num_rda, ll_row

string			ls_messaggio


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
else
	return
end if

li_anno_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

setpointer(Hourglass!)
li_ret =  wf_invia_respingi(li_anno_rda, ll_num_rda, "R", ls_messaggio)
setpointer(Arrow!)

if li_ret<0 then
	rollback;
	g_mb.error(ls_messaggio)
	
elseif li_ret>0 then
	rollback;
	if ls_messaggio<>"" and not isnull(ls_messaggio) then
		g_mb.warning(ls_messaggio)
	end if
	
elseif li_ret=0 then
	commit;
	g_mb.success("RDA respinta alla fase precedente!")
	
	//------------------------------------------------------------------------------------------------------------------------------------------
	//richiamo il metodo retrieve per visualizzare i dati aggiornati
	tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, li_anno_rda, ll_num_rda)
	
	//fare la retrieve pure delle notifiche
	tab_dettaglio.det_2.dw_2.retrieve(s_cs_xx.cod_azienda, li_anno_rda, ll_num_rda)
	
end if

return


end event

event pc_menu_invia();
integer			li_anno_rda, li_ret

long				ll_num_rda, ll_row

string			ls_messaggio


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
else
	return
end if

li_anno_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

setpointer(Hourglass!)
li_ret =  wf_invia_respingi(li_anno_rda, ll_num_rda, "I", ls_messaggio)
setpointer(Arrow!)

if li_ret<0 then
	rollback;
	g_mb.error(ls_messaggio)
	
elseif li_ret>0 then
	rollback;
	if ls_messaggio<>"" and not isnull(ls_messaggio) then
		g_mb.warning(ls_messaggio)
	end if
	
elseif li_ret=0 then
	commit;
	g_mb.success("RDA inviata alla fase successiva!")
	
	//------------------------------------------------------------------------------------------------------------------------------------------
	//richiamo il metodo retrieve per visualizzare i dati aggiornati
	tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, li_anno_rda, ll_num_rda)
	
	//fare la retrieve pure delle notifiche
	tab_dettaglio.det_2.dw_2.retrieve(s_cs_xx.cod_azienda, li_anno_rda, ll_num_rda)
	
end if

return


end event

event pc_menu_dettagli();s_cs_xx.parametri.parametro_w_rda = this

if tab_dettaglio.det_1.dw_1.getitemnumber(1, "anno_registrazione") < 1 then return

window_open_parm(w_det_rda_tv, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_menu_calcola();
string 				ls_cod_valuta, ls_flag_stato_autorizzato

long   				ll_anno_reg, ll_num_reg, ll_row, ll_handle

dec{4}				ld_valore

treeviewitem		ltvi_item

str_treeview		lstr_data

datetime			ldt_data_registrazione



if not wf_is_RDA_node(ll_handle) then return


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
else
	return
end if


tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltvi_item)
lstr_data = ltvi_item.data


ll_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber( ll_row,"anno_registrazione")
ll_num_reg =  tab_dettaglio.det_1.dw_1.getitemnumber( ll_row,"num_registrazione")
ldt_data_registrazione = tab_dettaglio.det_1.dw_1.getitemdatetime( ll_row,"data_registrazione")
ls_cod_valuta = tab_dettaglio.det_1.dw_1.getitemstring( ll_row,"cod_valuta")

if not isnull(ll_anno_reg) and ll_anno_reg <> 0 and not isnull(ll_num_reg) and ll_num_reg <> 0 then

	select sum(isnull(prezzo_acquisto, 0) * isnull(quan_richiesta, 0))
	into :ld_valore
	from det_rda
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg and
				num_registrazione=:ll_num_reg;
	
	if isnull(ld_valore) then ld_valore=0
	
	select flag_stato_autorizzato
	into :ls_flag_stato_autorizzato
	from tes_rda
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg and
				num_registrazione=:ll_num_reg;
				
	
	choose case ls_flag_stato_autorizzato
		case "P"
			ltvi_item = wf_new_item(false, ICONA_RDA_PARZIALE, ICONA_RDA_PARZIALE_SEL)
			
		case "E"
			ltvi_item = wf_new_item(false, ICONA_RDA_EVASA, ICONA_RDA_EVASA_SEL)
			
		case else
			ltvi_item = wf_new_item(false, ICONA_RDA, ICONA_RDA_SEL)
			
	end choose
	
	
	ltvi_item.children = false
	ltvi_item.label = string(ll_anno_reg) + "/"+ string(ll_num_reg)

	if not isnull(ldt_data_registrazione) and year(date(ldt_data_registrazione))>1950 then ltvi_item.label += " del "+string(ldt_data_registrazione, "dd/mm/yy") 
	
	if ls_cod_valuta<>"" and not isnull(ls_cod_valuta) then ltvi_item.label += " " + ls_cod_valuta
	
	ltvi_item.label += " Val. "+string(ld_valore, "#,###,###,##0.00") 
	
	ltvi_item.data = lstr_data
	
	tab_ricerca.selezione.tv_selezione.setitem(ll_handle, ltvi_item)
	
end if	

return
end event

event pc_menu_stampa();integer			li_anno_reg

long				ll_num_reg, ll_righe_dett



if tab_dettaglio.det_1.dw_1.getrow() > 0 then
	
	li_anno_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_registrazione")
	ll_num_reg = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_registrazione")
	
	if li_anno_reg>0 and ll_num_reg>0 then
	else
		return
	end if
	
	s_cs_xx.parametri.parametro_d_1_a[1] = li_anno_reg
	s_cs_xx.parametri.parametro_d_1_a[2] = ll_num_reg
	
	setpointer(Hourglass!)
	
	select count(*)
	into   :ll_righe_dett
	from   det_rda
	where  cod_azienda        = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg and
			 num_registrazione  = :ll_num_reg;
	
	setpointer(Arrow!)
	
	if ll_righe_dett = 0 or isnull(ll_righe_dett) then
		g_mb.warning("Nessun dettaglio presente in questa RDA: inutile stampare!")
		return
	end if
	
	window_open(w_report_tes_rda, -1)
	
end if
end event

event pc_menu_sospendi();integer			li_anno_rda, li_ret

long				ll_num_rda, ll_row

string			ls_messaggio


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
else
	return
end if

li_anno_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

if li_anno_rda>0 and ll_num_rda>0 then
	
	setpointer(Hourglass!)
	li_ret = wf_sospendi_desospendi(li_anno_rda, ll_num_rda, "S", ls_messaggio)
	setpointer(Arrow!)
	
	if li_ret<>0 then
		rollback;
		g_mb.error(ls_messaggio)
	else
		commit;
		g_mb.success("Operazione effettuata con successo!")
		
		//richiamo il metodo retrieve per visualizzare i dati aggiornati
		tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, li_anno_rda, ll_num_rda)
		
	end if

end if

return
end event

event pc_menu_desospendi();
integer			li_anno_rda, li_ret

long				ll_num_rda, ll_row

string			ls_messaggio


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
else
	return
end if

li_anno_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_rda = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

if li_anno_rda>0 and ll_num_rda>0 then
	
	setpointer(Hourglass!)
	li_ret = wf_sospendi_desospendi(li_anno_rda, ll_num_rda, "N", ls_messaggio)
	setpointer(Arrow!)
	
	if li_ret<>0 then
		rollback;
		g_mb.error(ls_messaggio)
	else
		commit;
		g_mb.success("Operazione effettuata con successo!")
		
		//richiamo il metodo retrieve per visualizzare i dati aggiornati
		tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, li_anno_rda, ll_num_rda)
		
	end if

end if

return
end event

public subroutine wf_imposta_ricerca ();
integer				li_anno_registrazione, li_anno_commessa
long					ll_num_registrazione, ll_num_commessa
boolean				lb_sql_det


is_sql_det = ""
is_sql_filtro = ""
lb_sql_det = false


//----------------------------------
li_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_registrazione")

if not isnull(li_anno_registrazione) and li_anno_registrazione > 1900 then
	is_sql_filtro += " AND tes_rda.anno_registrazione=" + string(li_anno_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	is_sql_filtro += " AND tes_rda.num_registrazione=" + string(ll_num_registrazione)
end if
if not isnull(li_anno_registrazione) and li_anno_registrazione>0 and not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	// mi fermo non serve altro, filtro solo la singolo ordine
	return
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione")) and year(date(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione")))>1950 then
	is_sql_filtro += " AND tes_rda.data_registrazione='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione"), s_cs_xx.db_funzioni.formato_data) +"'"
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_rda")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_rda")<>"" then
	is_sql_filtro += " AND tes_rda.cod_tipo_rda='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_rda") +"'"
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio_tes")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio_tes")<>"" then
	is_sql_filtro += " AND tes_rda.cod_operaio='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio_tes") +"'"
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")<>"" then
	is_sql_filtro += " AND det_rda.cod_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore") +"'"
	lb_sql_det = true
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")<>"" then
	is_sql_filtro += " AND det_rda.cod_prodotto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto") +"'"
	lb_sql_det = true
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")<>"" then
	is_sql_filtro += " AND det_rda.cod_deposito='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito") +"'"
	lb_sql_det = true
end if

//----------------------------------
//if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio")<>"" then
//	is_sql_filtro += " AND det_rda.cod_operaio='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio") +"'"
//	lb_sql_det = true
//end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_centro_costo")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_centro_costo")<>"" then
	is_sql_filtro += " AND det_rda.cod_centro_costo='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_centro_costo") +"'"
	lb_sql_det = true
end if

//--------------------------------
li_anno_commessa = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_commessa")
ll_num_commessa = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_commessa")

if not isnull(li_anno_commessa) and li_anno_commessa > 1900 then
	is_sql_filtro += " AND det_rda.anno_commessa=" + string(li_anno_commessa)
	lb_sql_det = true
end if
if not isnull(ll_num_commessa) and ll_num_commessa > 0 then
	is_sql_filtro += " AND det_rda.num_commessa=" + string(ll_num_commessa)
	lb_sql_det = true
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_autorizzato")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_autorizzato")<>"T" then
	is_sql_filtro += " AND det_rda.flag_autorizzato='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_autorizzato") +"'"
	lb_sql_det = true
end if

//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_ord_acq")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_ord_acq")<>"T" then
	is_sql_filtro += " AND det_rda.flag_gen_ord_acq='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_ord_acq") +"'"
	lb_sql_det = true
end if


//---------------------------------
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_stato_autorizzato")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_stato_autorizzato")<>"" then
	if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_stato_autorizzato")="AP" then
		is_sql_filtro += " AND tes_rda.flag_stato_autorizzato IN ('A','P'))"
		
	else
		is_sql_filtro += " AND tes_rda.flag_stato_autorizzato='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_evasione") +"'"
		
	end if
end if

//controllo se devo abilitare la join con la tabella di dettaglio RDA
if lb_sql_det OR wf_has_livello("F")  OR wf_has_livello("P") OR wf_has_livello("D") then
	is_sql_det = " LEFT OUTER JOIN det_rda on det_rda.cod_azienda=tes_rda.cod_azienda and "+&
							"det_rda.anno_registrazione=tes_rda.anno_registrazione and "+&
							"det_rda.num_registrazione=tes_rda.num_registrazione "
end if



end subroutine

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "T"
		return wf_inserisci_tipo_rda(al_handle)	
	
	
	case "F"
		return wf_inserisci_fornitori(al_handle)
		
		
	case "A"
		return wf_inserisci_anno(al_handle)
		
		
	case "D"
		return wf_inserisci_depositi(al_handle)
		
		
	case "O"
		return wf_inserisci_operai(al_handle)
		
		
	case "R"
		return wf_inserisci_data_registrazione(al_handle)
		
	
	case "P"
		return wf_inserisci_prodotti(al_handle)
		
	
	case else
		return wf_inserisci_rda(al_handle)
		
end choose

end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "O"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_rda.cod_operaio is null "
			else
				as_sql += " AND tes_rda.cod_operaio = '" + lstr_data.codice + "' "
			end if
			
			
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_rda.cod_tipo_rda is null "
			else
				as_sql += " AND tes_rda.cod_tipo_rda = '" + lstr_data.codice + "' "
			end if
			
			
		case "A"
			as_sql += " AND tes_rda.anno_registrazione = " + lstr_data.codice
			
			
		case "R"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_rda.data_registrazione is null "
			else
				as_sql += " AND tes_rda.data_registrazione = '" + lstr_data.codice + "' "
			end if
			
			
		case "C"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_ord_acq.data_consegna is null "
			else
				as_sql += " AND tes_ord_acq.data_consegna = '" + lstr_data.codice + "' "
			end if
			
			
		case "D"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND det_rda.cod_deposito is null "
			else
				as_sql += " AND det_rda.cod_deposito = '" + lstr_data.codice + "' "
			end if
			
			
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND det_rda.cod_prodotto is null "
			else
				as_sql += " AND det_rda.cod_prodotto = '" + lstr_data.codice + "' "
			end if


		case "F"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND det_rda.cod_fornitore is null "
			else
				as_sql += " AND det_rda.cod_fornitore = '" + lstr_data.codice + "' "
			end if
			
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public subroutine wf_treeview_icons ();ICONA_RDA = wf_treeview_add_icon("treeview\tipo_documento.png")
ICONA_RDA_PARZIALE = wf_treeview_add_icon("treeview\tag_yellow.png")
ICONA_RDA_EVASA = wf_treeview_add_icon("treeview\tag_red.png")

ICONA_RDA_SEL = wf_treeview_add_icon("treeview\tipo_documento_selected.png")
ICONA_RDA_PARZIALE_SEL = wf_treeview_add_icon("treeview\tag_yellow_selected.png")
ICONA_RDA_EVASA_SEL = wf_treeview_add_icon("treeview\tag_red_selected.png")


ICONA_OPERATORE = wf_treeview_add_icon("treeview\operatore.png")
ICONA_TIPO_RDA = wf_treeview_add_icon("treeview\documento_blu.png")
ICONA_ANNO = wf_treeview_add_icon("treeview\anno.png")
ICONA_DATA_REGISTRAZIONE = wf_treeview_add_icon("treeview\data_1.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_FORNITORE = wf_treeview_add_icon("treeview\cliente.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")


end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Tipo RDA", "T")
wf_add_valore_livello("Fornitore", "F")
wf_add_valore_livello("Anno Registrazione", "A")
wf_add_valore_livello("Depositi", "D")
wf_add_valore_livello("Data Registrazione", "R")
wf_add_valore_livello("Operatore", "O")
wf_add_valore_livello("Prodotto", "P")
end subroutine

public function long wf_inserisci_rda (long al_handle);string				ls_sql, ls_error

long					ll_rows, ll_i

treeviewitem 		ltvi_item


ls_sql = "SELECT	DISTINCT "+&
							"tes_rda.anno_registrazione,"+&
							"tes_rda.num_registrazione,"+&
							"tes_rda.data_registrazione,"+&
							"tes_rda.flag_stato_autorizzato,"+&
							"isnull(tes_rda.cod_valuta,'') "+&
			"FROM tes_rda " + &
			is_sql_det + &
			" WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

//imposto l'ordinamento per anno e numero
ls_sql += " ORDER BY tes_rda.anno_registrazione, tes_rda.num_registrazione"

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

// stefanop 02/07/2012: aggiungo controllo per il numero massimo di risultati
if ll_rows > il_max_row  then
	if not g_mb.confirm("Sono stati recuperate " + string(ll_rows) + " RDA superando il limite consigliato.~r~nProcedere con la visualizzazione di tutte le RDA?") then
		// Visualizzo solo le richieste nel limite
		ll_rows = il_max_row
	end if
end if

for ll_i = 1 to ll_rows
	
	wf_inserisci_singola_rda(	al_handle, &
											ids_store.getitemnumber(ll_i, 1), &
											ids_store.getitemnumber(ll_i, 2), &
											ids_store.getitemdatetime(ll_i, 3), &
											ids_store.getitemstring(ll_i, 4), &
											ids_store.getitemstring(ll_i, 5))

next

return ll_rows
end function

public function long wf_inserisci_tipo_rda (long al_handle);string				ls_sql, ls_label, ls_cod_tipo_rda, ls_des_tipo_rda, ls_error
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview 		lstr_data


ls_sql = "SELECT DISTINCT "+&
					"tab_tipi_rda.cod_tipo_rda,"+&
					"tab_tipi_rda.des_tipo_rda "+&
			"FROM tes_rda  "+&
			"LEFT OUTER JOIN tab_tipi_rda ON tab_tipi_rda.cod_azienda = tes_rda.cod_azienda AND  "+&
															"tab_tipi_rda.cod_tipo_rda = tes_rda.cod_tipo_rda " + &
			is_sql_det +  " "+&
			"WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	 ls_cod_tipo_rda = ids_store.getitemstring(ll_i, 1)
	 ls_des_tipo_rda = ids_store.getitemstring(ll_i, 2)
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	if ls_cod_tipo_rda="" then
		setnull(lstr_data.codice)
	else
		lstr_data.codice = ls_cod_tipo_rda
	end if
	
	
	if isnull(ls_cod_tipo_rda) and isnull(ls_cod_tipo_rda) then
		ls_label = "<Tipo RDA mancante>"
		
	elseif isnull(ls_des_tipo_rda) then
		ls_label = ls_cod_tipo_rda
		
	else
		ls_label = ls_cod_tipo_rda + " - " + ls_des_tipo_rda
	end if
	
	ltvi_item = wf_new_item(true, ICONA_TIPO_RDA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_fornitori (long al_handle);string					ls_sql, ls_label, ls_cod_fornitore, ls_des_fornitore, ls_error
long						ll_rows, ll_i
treeviewitem			ltvi_item


ls_sql = "SELECT DISTINCT "+&
						"anag_fornitori.cod_fornitore,"+&
						"anag_fornitori.rag_soc_1 "+&
			"FROM tes_rda "+&
			is_sql_det + " "+&
			"LEFT OUTER JOIN anag_fornitori ON anag_fornitori.cod_azienda = det_rda.cod_azienda AND "+&
																"anag_fornitori.cod_fornitore = det_rda.cod_fornitore " + &			
			"WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	 ls_cod_fornitore = ids_store.getitemstring(ll_i, 1)
	 ls_des_fornitore = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "F"
	lstr_data.codice = ls_cod_fornitore
	
	if isnull(ls_cod_fornitore) and isnull(ls_des_fornitore) then
		ls_label = "<Fornitore mancante>"
	elseif isnull(ls_des_fornitore) then
		ls_label = ls_cod_fornitore
	else
		ls_label = ls_cod_fornitore + " - " + ls_des_fornitore
	end if

	ltvi_item = wf_new_item(true, ICONA_FORNITORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_anno (long al_handle);string					ls_sql, ls_error
long						ll_rows, ll_i
treeviewitem			ltvi_item


ls_sql = "SELECT DISTINCT "+&
					"tes_rda.anno_registrazione "+&
			"FROM tes_rda " + &
			is_sql_det + &
			" WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "A"
	lstr_data.codice = string(ids_store.getitemnumber(ll_i, 1))
	
	ltvi_item = wf_new_item(true, ICONA_ANNO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = string(ids_store.getitemnumber(ll_i, 1))
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_depositi (long al_handle);
string				ls_sql, ls_label, ls_error, ls_cod_deposito, ls_des_deposito
long					ll_rows, ll_i
treeviewitem		ltvi_item


ls_sql = "SELECT DISTINCT "+&
					"anag_depositi.cod_deposito, anag_depositi.des_deposito "+&
			"FROM tes_rda " + &
			is_sql_det +  " "+&
			"LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_deposito = det_rda.cod_azienda AND  "+&
															"anag_depositi.cod_deposito = det_rda.cod_deposito  "+&
			"WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_cod_deposito) then
		ls_label = "<Deposito mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_singola_rda (long al_handle, long al_anno_registrazione, long al_num_registrazione, datetime adt_data_registrazione, string as_flag_stato_autorizzato, string as_cod_valuta);
dec{4}				ld_tot_rda

treeviewitem		ltvi_item

str_treeview		lstr_data


	
lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = al_anno_registrazione
lstr_data.decimale[2] = al_num_registrazione

choose case as_flag_stato_autorizzato
	case "P"
		ltvi_item = wf_new_item(false, ICONA_RDA_PARZIALE, ICONA_RDA_PARZIALE_SEL)
		
	case "E"
		ltvi_item = wf_new_item(false, ICONA_RDA_EVASA, ICONA_RDA_EVASA_SEL)
		
	case else
		ltvi_item = wf_new_item(false, ICONA_RDA, ICONA_RDA_SEL)
		
end choose



//mettiamo sul nodo le info principali -----------------------------
select 	sum(isnull(prezzo_acquisto, 0) * isnull(quan_richiesta, 0))
into   	:ld_tot_rda
from   det_rda
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;
		 
if isnull(ld_tot_rda) then ld_tot_rda=0.00


ltvi_item.label = string(al_anno_registrazione) + "/"+ string(al_num_registrazione)

if not isnull(adt_data_registrazione) and year(date(adt_data_registrazione))>1950 then ltvi_item.label += " del "+string(adt_data_registrazione, "dd/mm/yy") 

if as_cod_valuta<>"" and not isnull(as_cod_valuta) then ltvi_item.label += " " + as_cod_valuta

ltvi_item.label += " Val. "+string(ld_tot_rda, "#,###,###,##0.00") 

ltvi_item.data = lstr_data

tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)

return 1
end function

public function boolean wf_is_rda_node (ref long al_handle);
treeviewitem 		ltvi_item
str_treeview 		lstr_data



al_handle = wf_get_current_handle()

if al_handle>0 then
else
	return false
end if

tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltvi_item)
lstr_data = ltvi_item.data

//se non è un nodo di una RDA esco subito per evitare problemi
if lstr_data.tipo_livello <> "N" or upperbound(lstr_data.decimale[]) < 2 then
	return false 
end if

if lstr_data.decimale[1]>0 and lstr_data.decimale[2]>0 then
else
	return false
end if

return true


end function

public function long wf_inserisci_data_registrazione (long al_handle);string						ls_sql, ls_error
long							ll_rows, ll_i
treeviewitem				ltvi_item


ls_sql = "SELECT DISTINCT "+&
					"tes_rda.data_registrazione "+&
			"FROM tes_rda " + &
			is_sql_det + &
			" WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "R"
	lstr_data.codice = string(ids_store.getitemdatetime(ll_i, 1), s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA_REGISTRAZIONE)
	
	ltvi_item.data = lstr_data
	if isnull(ids_store.getitemdatetime(ll_i, 1)) then
		ltvi_item.label = "<Data registrazione mancante>"
	else
		ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_prodotti (long al_handle);string						ls_sql, ls_label, ls_error, ls_cod_prodotto, ls_des_prodotto
long							ll_rows, ll_i
treeviewitem				ltvi_item


ls_sql = "SELECT DISTINCT "+&
					"anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto "+&
			"FROM tes_rda " + &
			is_sql_det + " " + &
			"LEFT OUTER JOIN anag_prodotti ON anag_prodotti.cod_azienda = det_rda.cod_azienda AND "+&
															"anag_prodotti.cod_prodotto = det_rda.cod_prodotto "+&
			"WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_prodotto = ids_store.getitemstring(ll_i, 1)
	 ls_des_prodotto = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "P"
	lstr_data.codice = ls_cod_prodotto
	
	if isnull(ls_cod_prodotto) and isnull(ls_des_prodotto) then
		ls_label = "<Prodotto mancante>"
	elseif isnull(ls_des_prodotto) then
		ls_label = ls_cod_prodotto
	else
		ls_label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if

	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_operai (long al_handle);string				ls_sql, ls_label, ls_cod_operaio, ls_des_operaio, ls_error
long					ll_rows, ll_i
treeviewitem		ltvi_item


ls_sql = "SELECT DISTINCT "+&
						"anag_operai.cod_operaio, anag_operai.nome + ' ' + anag_operai.cognome "+&
			"FROM tes_rda "+&
			"LEFT OUTER JOIN anag_operai ON anag_operai.cod_azienda = tes_rda.cod_azienda AND "+&
															"anag_operai.cod_operaio = tes_rda.cod_operaio " + &
			is_sql_det + " "+&
			"WHERE tes_rda.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	 ls_cod_operaio = ids_store.getitemstring(ll_i, 1)
	 ls_des_operaio = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "O"
	lstr_data.codice = ls_cod_operaio
	
	if (isnull(ls_cod_operaio) or ls_cod_operaio="") then
		ls_label = "<Operaio mancante>"
		
	elseif isnull(ls_des_operaio) or ls_des_operaio="" then
		ls_label = ls_cod_operaio
		
	else
		ls_label = ls_cod_operaio + " - " + ls_des_operaio
		
	end if
	
	ltvi_item = wf_new_item(true, ICONA_OPERATORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_invia_respingi (integer ai_anno_rda, long al_num_rda, string as_invia_o_respingi, ref string as_messaggio);string			ls_cod_tipo_rda, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_sospesa, ls_flag_gestione_fasi, ls_cod_destinatari[], &
					ls_oggetto, ls_testo, ls_email_destinatari[], ls_cod_mittente, ls_note, ls_email_mittente

long				ll_row, ll_num_sequenza_corrente, ll_num_sequenza_arrivo, ll_num_sequenza_invio_mail, ll_index

integer			li_ret


as_messaggio = ""


//------------------------------------------------------------------------------------------------------------------------------------------
//controllo riga corrente
ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
else
	as_messaggio = "Nessuna riga selezionata"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------
//se non sei in modalità visualizza non proseguire
if ib_new or ib_modify then return 0


if ai_anno_rda>0 and al_num_rda>0 then
else
	as_messaggio = "Impossibile procedere: devi selezionare una RDA!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------
//lettura tipo RDA
ls_cod_tipo_rda = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_tipo_rda")

if ls_cod_tipo_rda="" or isnull(ls_cod_tipo_rda) then
	as_messaggio = "Impossibile procedere: manca il tipo della RDA!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------
//lettura tipo lista distribuzione e lista distribuzione
select	cod_tipo_lista_dist,
			cod_lista_dist
into	:ls_cod_tipo_lista_dist, 
		:ls_cod_lista_dist
from tab_tipi_rda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_rda=:ls_cod_tipo_rda;

if sqlca.sqlcode<0 then
	as_messaggio = "Errore in lettura tipo e lista distribuzione associata: "+sqlca.sqlerrtext
	return -1
	
elseif isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist="" or &
			isnull(ls_cod_lista_dist) or ls_cod_lista_dist=""	then
	
	as_messaggio = "Impossibile procedere: nel tipo RDA manca il tipo lista ditribuzione e/o la listadistribuzione!"
	return 1
end if

select flag_gestione_fasi
into :ls_flag_gestione_fasi
from tab_tipi_liste_dist
where 	cod_tipo_lista_dist=:ls_cod_tipo_lista_dist;

if ls_flag_gestione_fasi="S" then
else
	as_messaggio = "Impossibile procedere: sono ammessi solo Tipi Lista Distribuzione (associati al tipo RDA) con Gestione Fasi abilitata!"
	return 1
end if
			

//------------------------------------------------------------------------------------------------------------------------------------------
//controllo che il documento non sia sospeso
ls_flag_sospesa = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_sospesa")

if ls_flag_sospesa="S" then
	as_messaggio = "Impossibile procedere: la RDA risulta sospesa!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------
//leggo la fase in cui si trova il documento
ll_num_sequenza_corrente = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_sequenza_corrente")
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0


//------------------------------------------------------------------------------------------------------------------------------------------
//controllo se chi vorrebbe INVIARE è abilitato e ha tutti i parametri in ordine
//se la RDA è stata inserita, è sufficiente che l'utente collegato abbia l'indirizzo email in tabella utenti altrimenti
//l'utente collegato deve essere associato ad un destinatario (tab_ind_dest.cod_utente) della fase successiva a quella
//		in cui si trova il documento
li_ret = wf_controlla_sequenza(	false, ll_num_sequenza_corrente, ls_cod_tipo_lista_dist, ls_cod_lista_dist, &
												ls_cod_mittente, ls_email_mittente, as_messaggio)

if li_ret <> 0 then
	return li_ret
end if


//recupero fase ed elenco destinatari a cui mandare (successiva se INVIA e precedente se RESPINGI)
li_ret = wf_carica_destinatari(		false, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ll_num_sequenza_corrente, as_invia_o_respingi, &
												ll_num_sequenza_arrivo, ll_num_sequenza_invio_mail, ls_cod_destinatari[], ls_email_destinatari[], &
												as_messaggio)

if li_ret <> 0 then
	return li_ret
end if

//------------------------------------------------------------------------------------------------------------------------------------------

//invia i messaggi
if upperbound(ls_email_destinatari[])>0 then
	
	if as_invia_o_respingi="R" then
		ls_oggetto = "Respingimento"
		ls_testo = "Respingimento"
		
		if g_mb.confirm( "Si desidera inserire una descrizione per motivare il Respingimento?") then
			s_cs_xx.parametri.parametro_d_10 = 9999
			openwithparm(w_inserisci_altro_valore, "<Inserire qui la motivazione del respingimento, poi premi INVIO per confermare>")
			
			ls_note = message.stringparm
			
			if ls_note="cs_team_esc_pressed" then ls_note = ""
			
			if ls_note<>"" then	ls_note = " con la seguente motivazione:" + ls_note
			
		end if
		
	else
		ls_note = ""
		ls_oggetto = "Avanzamento"
		ls_testo = "Avanzamento"
	end if
	
	ls_oggetto += " RDA n° "+string(ai_anno_rda)+"/"+string(al_num_rda)
	ls_testo +=  " RDA n° "+string(ai_anno_rda)+"/"+string(al_num_rda) + " " + ls_note
	
	
	if wf_invia_mail(ls_oggetto, ls_testo, ls_email_destinatari[], ls_cod_mittente, ls_email_mittente, as_messaggio) < 0 then
		return -1
	end if

	//salvo notifiche in apposita tabella comunicazioni -----------------------------------------------------------------------------
	for ll_index=1 to upperbound(ls_cod_destinatari[])
		if wf_salva_notifica(	ai_anno_rda, al_num_rda, ls_cod_destinatari[ll_index], ls_cod_mittente, &
										ll_num_sequenza_corrente, ll_num_sequenza_arrivo, ls_note, as_messaggio) < 0 then
										
		end if
	next

end if



//------------------------------------------------------------------------------------------------------------------------------------------
//aggiorno il numero sequenza corrente del documento
update tes_rda
set num_sequenza_corrente = :ll_num_sequenza_arrivo
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_rda and
			num_registrazione=:al_num_rda;

if sqlca.sqlcode<0 then
	as_messaggio = "Errore in aggiornamento sequenza corrente RDA: "+sqlca.sqlerrtext
	return -1
end if

////------------------------------------------------------------------------------------------------------------------------------------------
////richiamo il metodo retrieve per visualizzare i dati aggiornati
//tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, ai_anno_rda, al_num_rda)
//
////fare la retrieve pure delle notifiche
//tab_dettaglio.det_2.dw_2.retrieve(s_cs_xx.cod_azienda, ai_anno_rda, al_num_rda)



return 0
end function

public function integer wf_carica_destinatari (string as_cod_tipo_lista_dist, string as_cod_lista_dist, long al_num_sequenza_invio, ref string as_cod_destinatari[], ref string as_email_destinatari[], ref string as_errore);string					ls_sql, ls_cod_destinatario, ls_des_destinatario, ls_indirizzo

long						ll_index, ll_i


ls_sql = "select a.cod_destinatario,"+&
						"a.des_destinatario,"+&
						"a.indirizzo "+&
				"from tab_ind_dest as a "+&
				"join det_liste_dist_destinatari as b on b.cod_azienda=a.cod_azienda and "+&
															"b.cod_destinatario_mail=a.cod_destinatario "+&
				"where a.cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
						"a.email='S' and "+&
						"b.cod_tipo_lista_dist = '"+as_cod_tipo_lista_dist+"' and "+&
						"b.cod_lista_dist = '"+as_cod_lista_dist+"' and "+&
						"b.num_sequenza = "+string(al_num_sequenza_invio)+" and "+&
						"a.indirizzo<>'' and a.indirizzo is not null "
	
ll_index = 0
	
declare cu_dest dynamic cursor for sqlsa;
	
prepare sqlsa from :ls_sql;
	
open cu_dest;
	
if sqlca.sqlcode <> 0 then
	as_errore = "Errore nella open del cursore destinatari: " + sqlca.sqlerrtext
	return -1
end if
	
do while true
		
	fetch cu_dest
	into  :ls_cod_destinatario,
			:ls_des_destinatario,
			:ls_indirizzo;
				
	if sqlca.sqlcode < 0 then
		as_errore = "Errore nella fetch del cursore destinatari: " + sqlca.sqlerrtext
		close cu_dest;
		return -1
		
	elseif sqlca.sqlcode = 100	then
		close cu_dest;
		exit
	end if

	if pos(ls_indirizzo, "@")>0 then
	else
		continue
	end if

	ll_index += 1

	as_cod_destinatari[ll_index] = ls_cod_destinatario
	as_email_destinatari[ll_index] = ls_indirizzo
loop

if upperbound(as_cod_destinatari[]) = 0 then
	as_errore = "Sembra che non sia presente alcun destinatario a cui inviare email!"
end if


end function

public function integer wf_salva_notifica (integer ai_anno_rda, long al_num_rda, string as_cod_destinatario, string as_cod_mittente, long al_num_sequenza_corrente, long al_num_sequenza_arrivo, string as_note, ref string as_errore);long			ll_prog_riga
datetime	ldt_oggi


if as_cod_mittente="" then setnull(as_cod_mittente)

if isnull(al_num_sequenza_corrente) then al_num_sequenza_corrente = 0

ldt_oggi = datetime(today(), now())

setnull(ll_prog_riga)

select max(prog_riga)
into :ll_prog_riga
from tes_rda_notifiche
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_rda and
			num_registrazione=:al_num_rda;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura max progressivo riga notifica: "+sqlca.sqlerrtext
	return -1
end if

if isnull(ll_prog_riga) then ll_prog_riga = 0
ll_prog_riga += 1

insert into tes_rda_notifiche
(	cod_azienda,
	anno_registrazione,
	num_registrazione,
	prog_riga,
	data_registrazione,
	cod_destinatario,
	cod_mittente,
	num_sequenza_corrente,
	num_sequenza_arrivo,
	note	)
values		(	:s_cs_xx.cod_azienda,
					:ai_anno_rda,
					:al_num_rda,
					:ll_prog_riga,
					:ldt_oggi,
					:as_cod_destinatario,
					:as_cod_mittente,
					:al_num_sequenza_corrente,
					:al_num_sequenza_arrivo,
					:as_note);

if sqlca.sqlcode<0 then
	as_errore = "Errore in salvataggio notifica: "+sqlca.sqlerrtext
	return -1
end if

return 0



end function

public function integer wf_carica_destinatari (boolean ab_inserimento_rda, string as_cod_tipo_lista_dist, string as_cod_lista_dist, long al_num_sequenza_corrente, string as_invia_o_respingi, ref long al_num_sequenza_arrivo, ref long al_num_sequenza_invio_mail, ref string as_cod_destinatari[], ref string as_email_destinatari[], ref string as_errore);
string				ls_vuoto[]


as_cod_destinatari[] = ls_vuoto[]
as_email_destinatari[] = ls_vuoto[]

if as_invia_o_respingi="I" then
	//#######################################################################################
	//si tratta di un INVIO
	
	if al_num_sequenza_corrente=0 then

		select num_sequenza
		into   :al_num_sequenza_arrivo
		from   det_liste_dist
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
					cod_lista_dist = :as_cod_lista_dist and
					(num_sequenza_prec = 0 or num_sequenza_prec is null);

	else
		select num_sequenza
		into   :al_num_sequenza_arrivo
		from   det_liste_dist
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
					cod_lista_dist = :as_cod_lista_dist and
					num_sequenza_prec = :al_num_sequenza_corrente;

	end if
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura sequenza successiva (1): "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=100 or isnull(al_num_sequenza_arrivo) or al_num_sequenza_arrivo=0 then
		as_errore = "Sequenza successiva NON trovata!"
		return 1
	end if
	
	
	if not ab_inserimento_rda then
		//----------------------------------------------------------
		//in realtà la mail deve andare ai destinatari che hanno come numero sequenza di provenienza 
		//quella contenuta nella variabile al_num_sequenza_arrivo
		select num_sequenza
		into   :al_num_sequenza_invio_mail
		from   det_liste_dist
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
					cod_lista_dist = :as_cod_lista_dist and
					num_sequenza_prec = :al_num_sequenza_arrivo;
		
		if sqlca.sqlcode<0 then
			as_errore = "Errore in lettura sequenza successiva (2): "+sqlca.sqlerrtext
			return -1
			
		elseif sqlca.sqlcode=100  or isnull(al_num_sequenza_invio_mail) or al_num_sequenza_invio_mail=0 then
			//sei all'ultimo stadio: nessuna mail di notifica in quanto non ci sono altri destinatari
			//ma il documento deve andare nella fase ultima del ciclo di vita
			setnull(al_num_sequenza_invio_mail)
		end if
		
	else
		//in fase di inserimento l'invio mail va comunque ai desrtinatari della prima fase
		al_num_sequenza_invio_mail = al_num_sequenza_arrivo
		
	end if
	
else
	//#######################################################################################
	//si tratta di un RESPINGI
	//quindi devi mandare una notifica agli operatori del passo precedente, cioè
	//i destinatari che sono nella fase indicata dal num.sequenza corrente, mentre il documento
	//deve andare nella fase precedente a quella indicata dal num.sequenza corrente
	if isnull(al_num_sequenza_corrente) or al_num_sequenza_corrente=0 then
		as_errore = "Impossibile continuare! Non esiste una fase precedente a quella corrente!"
		return 1
	end if
	
	al_num_sequenza_invio_mail = al_num_sequenza_corrente
	
	select num_sequenza_prec
	into   :al_num_sequenza_arrivo
	from   det_liste_dist
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
				cod_lista_dist = :as_cod_lista_dist and
				num_sequenza = :al_num_sequenza_corrente;

end if

if isnull(al_num_sequenza_arrivo) then al_num_sequenza_arrivo=0

//carica gli indirizzi dei destinatari
if not isnull(al_num_sequenza_invio_mail) and al_num_sequenza_invio_mail>0 then
	if wf_carica_destinatari(as_cod_tipo_lista_dist, as_cod_lista_dist, al_num_sequenza_invio_mail, as_cod_destinatari[], as_email_destinatari[], as_errore) < 0 then
		return -1
	end if
end if


return 0
end function

public function integer wf_controlla_sequenza (boolean ab_inserimento, long al_num_sequenza_corrente, string as_cod_tipo_lista_dist, ref string as_cod_lista_dist, ref string as_cod_mittente, ref string as_email_mittente, ref string as_errore);long			ll_tot, ll_num_sequenza_successiva

string		ls_sql

datastore	lds_data



//leggi i dati email e codice del mittente
//N.B. se sei in fase di creazione della RDA il codice del mittente resta vuoto e leggi dalla tabella utenti la m ial associata
//per le fasi successive leggi la mail dalla tabella rubrica dei destinatari e relativo codice mittente


if ab_inserimento then
		//prendi i valori dell'utente collegato
		setnull(as_cod_mittente)
		
		select		e_mail
		into 			:as_email_mittente
		from utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if as_email_mittente="" or isnull(as_email_mittente) then
			as_errore = "Manca email del Mittente: impostare l'indirizzo e-mail per l'utente "+s_cs_xx.cod_utente + " in tabella utenti!"
			return -1
		end if

else
	//leggi da tabella rubrica destinatari
	select		cod_destinatario, indirizzo
	into 			:as_cod_mittente, :as_email_mittente
	from tab_ind_dest
	where 	cod_azienda=:s_cs_xx.cod_azienda and 
				cod_utente = :s_cs_xx.cod_utente and
				email='S';


	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura dati mittente: "+sqlca.sqlerrtext
		return -1
		
	elseif (sqlca.sqlcode = 100 or as_cod_mittente="" or isnull(as_cod_mittente)) then
		as_errore = "Nessun mittente associato con l'utente "+s_cs_xx.cod_utente+" in tabella rubrica destinatari!"
		return -1
		
	end if
end if

if as_email_mittente="" or isnull(as_email_mittente) and not ab_inserimento  then
	as_errore = "Manca email del Mittente: impostare l'indirizzo e-mail il codice "+as_cod_mittente + " in tabella rubrica destinatari!"
	return -1
end if

if ab_inserimento then
	//hai finito
	return 0
end if

//ulteriore controllo per gli invii/respingimenti per le fasi successive alla creazione della RDA
//chi fa l'azione di invio/respingimento deve essere un destinatario della fase successiva a quella in cui si trova il documento
/*
es. di un cuiclo di vita fatto da 
Fase 10
Fase 20
Fase 30
Fase 40 (ultima)

--------------------------------------------------------------------------------------------------------------------------------
STATO IN CUI
SI TROVA
LA RDA			AZIONE						MAIL DI AVVISO									STATO IN CUI VA LA RDA
--------------------------------------------------------------------------------------------------------------------------------
<n.a.>			creazione RDA  			ai destinatari fase 10							<nessuno>
--------------------------------------------------------------------------------------------------------------------------------
<nessuno>	INVIA							ai destinatari fase 20							Fase 10
<nessuno>	RESPINGI					<nessuna mail>									<nessuno>
--------------------------------------------------------------------------------------------------------------------------------
10					INVIA							ai destinatari fase 30							Fase 20
10					RESPINGI					ai destinatari fase 10							<nessuno>
--------------------------------------------------------------------------------------------------------------------------------
20					INVIA							ai destinatari fase 40							Fase 30
20					RESPINGI					ai destinatari fase 20							Fase 10
---------------------------------------------------------------------------------------------------------------------------------
30					APPROVA					<nessuna mail>									Fase 40 (ultima)
30					RESPINGI					ai destinatari fase 30							Fase 20
---------------------------------------------------------------------------------------------------------------------------------

in pratica lo stato in cui va la RDA rappresenta lo fase degli ultimi destinatari che hanno mandato avanti il documento
*/

//adesso mi serve sapere se ci sono fasi successive e qual è il numero sequenza successivo
//allo scopo di verificare se l'utente collegato è presente tra i destinatari di questa fase
ls_sql = "select num_sequenza "+&
			"from det_liste_dist "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_tipo_lista_dist='"+as_cod_tipo_lista_dist+"' and "+&
						"cod_lista_dist='"+as_cod_lista_dist +"'and "+&
						"num_sequenza>"+string(al_num_sequenza_corrente)+" "+&
			"order by num_sequenza asc"

ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_tot<0 then
	return -1
	
elseif ll_tot=0 then
	//sembra non ci sia niente oltre la fase in cui ci si trova
	as_errore = "Nessuna fase successiva a quella corrente è stata trovata nei cicli di vita oppure il ciclo di vita è stato concluso!"
	destroy lds_data
	return 1
	
end if

//leggo dalla prima riga
ll_num_sequenza_successiva = lds_data.getitemnumber(1, 1)

destroy lds_data


//verifico se l'utente collegato è associato ad uno dei destinatari della fase successiva
select count(*)
into :ll_tot
from det_liste_dist_destinatari
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_lista_dist=:as_cod_tipo_lista_dist and
			cod_lista_dist=:as_cod_lista_dist and
			num_sequenza=:ll_num_sequenza_successiva and
			cod_destinatario_mail=:as_cod_mittente;

if sqlca.sqlcode<0 then
	as_errore = "Errore controllo su privilegi invio in fase corrente: "+sqlca.sqlerrtext
	return -1
end if

if isnull(ll_tot) or ll_tot=0 then
	as_errore = "Non autorizzato! (wf_controlla_sequenza)"
	return 1
end if


return 0
end function

public function integer wf_invia_mail (string as_oggetto, string as_testo, string as_email_destinatari[], ref string as_cod_mittente, ref string as_mail_mittente, ref string as_errore);
uo_sendmail					luo_send

string							ls_smtp, ls_usr_smtp, ls_pwd_smtp, ls_email_ind_dest, ls_cod_mittente

long								ll_index

boolean							lb_ret


//---------------------------------------------------------------------------------------------------------
select		smtp_server, smtp_usr, smtp_pwd
into 			:ls_smtp, :ls_usr_smtp, :ls_pwd_smtp
from utenti
where cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura dati e-mail dell'utente collegato: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 100 then
	as_errore = "Utente "+s_cs_xx.cod_utente+" non trovato in tabella!"
	return -1
	
end if

if ls_smtp="" or isnull(ls_smtp) then
	as_errore = "Impostare l'indirizzo del server SMTP per l'utente "+s_cs_xx.cod_utente
	return -1
end if


//---------------------------------------------------------------------------------------------------------
//if as_cod_mittente="" or isnull(as_cod_mittente) then
//
//	select		cod_destinatario, indirizzo
//	into 			:as_cod_mittente, :as_mail_mittente
//	from tab_ind_dest
//	where cod_azienda=:s_cs_xx.cod_azienda and cod_utente = :s_cs_xx.cod_utente;
//	
//	if sqlca.sqlcode<0 then
//		as_errore = "Errore in lettura dati mittente: "+sqlca.sqlerrtext
//		return -1
//		
//	elseif sqlca.sqlcode = 100 or as_cod_mittente="" or isnull(as_cod_mittente) then
//		as_errore = "Nessun mittente associato con l'utente "+s_cs_xx.cod_utente+" in tabella destinatari!"
//		return -1
//		
//	end if
//	
//	if as_mail_mittente="" or isnull(as_mail_mittente) then
//		as_errore = "Manca email del Mittente: impostare l'indirizzo e-mail il codice "+as_cod_mittente + " in tabella destinatari!"
//		return -1
//	end if
//	
//else
//	if as_mail_mittente="" or isnull(as_mail_mittente) then
//		as_errore = "Manca email del Mittente: impostare l'indirizzo e-mail il codice "+as_cod_mittente + " in tabella destinatari (cod. "+as_cod_mittente+" )!"
//		return -1
//	end if
//	
//end if


luo_send = create uo_sendmail

//IMPOSTA MITTENTE ---------------------------------------------------------------------------
luo_send.uof_set_from( as_mail_mittente )

//TODO email del MITTENTE per inviare per conoscenza --------------------------------
luo_send.uof_add_cc( as_mail_mittente)

//DESTINATARI -----------------------------------------------------------------------------------
luo_send.uof_add_to( as_email_destinatari[])

//OGGETTO ---------------------------------------------------------------------------------------
luo_send.uof_set_subject( as_oggetto )

//TESTO -------------------------------------------------------------------------------------------
luo_send.uof_set_message( as_testo )

//IMPOSTA SMTP, UTENTE e PASSWORD ----------------------------------------------------
luo_send.uof_set_smtp( ls_smtp, ls_usr_smtp, ls_pwd_smtp)

//INVIO --------------------------------------------------------------------------------------------
lb_ret = luo_send.uof_send()

if not lb_ret then
	as_errore = luo_send.uof_get_error()
	destroy luo_send
	return -1
end if

destroy luo_send

as_errore = ""

return 0
end function

public function integer wf_mail_insert_rda (integer ai_anno_rda, long al_num_rda, ref string as_messaggio);
long				ll_row, ll_num_sequenza_corrente, ll_num_sequenza_arrivo, ll_num_sequenza_invio_mail, ll_index

string			ls_cod_tipo_rda, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi,  ls_cod_destinatari[], ls_email_destinatari[], &
					ls_oggetto, ls_testo, ls_cod_mittente, ls_email_mittente

integer			li_ret


as_messaggio = ""


//------------------------------------------------------------------------------------------------------------------------------------------
//controllo riga corrente
ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
else
	//as_messaggio = "Nessuna riga selezionata"
	return 1
end if


if ai_anno_rda>0 and al_num_rda>0 then
else
	//as_messaggio = "Impossibile procedere: devi selezionare una RDA!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------
//lettura tipo RDA
ls_cod_tipo_rda = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_tipo_rda")

if ls_cod_tipo_rda="" or isnull(ls_cod_tipo_rda) then
	//as_messaggio = "Impossibile procedere: manca il tipo della RDA!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------
//lettura tipo lista distribuzione e lista distribuzione
select	cod_tipo_lista_dist,
			cod_lista_dist
into	:ls_cod_tipo_lista_dist, 
		:ls_cod_lista_dist
from tab_tipi_rda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_rda=:ls_cod_tipo_rda;

if sqlca.sqlcode<0 then
	as_messaggio = "Errore in lettura tipo e lista distribuzione associata: "+sqlca.sqlerrtext
	return -1
	
elseif isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist="" or &
			isnull(ls_cod_lista_dist) or ls_cod_lista_dist=""	then
	
	//as_messaggio = "Impossibile procedere: nel tipo RDA manca il tipo lista ditribuzione e/o la listadistribuzione!"
	return 1
end if

select flag_gestione_fasi
into :ls_flag_gestione_fasi
from tab_tipi_liste_dist
where 	cod_tipo_lista_dist=:ls_cod_tipo_lista_dist;

if ls_flag_gestione_fasi="S" then
else
	//as_messaggio = "Impossibile procedere: sono ammessi solo Tipi Lista Distribuzione (associati al tipo RDA) con Gestione Fasi abilitata!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------
ll_num_sequenza_corrente =0


//------------------------------------------------------------------------------------------------------------------------------------------
//controllo se chi vuole INVIARE è abilitato e ha tutti i parametri in ordine
//se la RDA è stata inserita, è sufficiente che l'utente collegato abbia l'indirizzo email in tabella utenti altrimenti
//l'utente collegato deve essere associato ad un destinatario (tab_ind_dest.cod_utente) della fase successiva a quella
//		in cui si trova il documento
li_ret = wf_controlla_sequenza(	true, ll_num_sequenza_corrente, ls_cod_tipo_lista_dist, ls_cod_lista_dist, &
												ls_cod_mittente, ls_email_mittente, as_messaggio)

if li_ret <> 0 then
	return li_ret
end if

//recupero fase a cui mandare (successiva se INVIA e precedente se RESPINGI) + elenco destinatari interessati
li_ret = wf_carica_destinatari(		true, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ll_num_sequenza_corrente, "I", &
												ll_num_sequenza_arrivo, ll_num_sequenza_invio_mail, ls_cod_destinatari[], ls_email_destinatari[], &
												as_messaggio)

if li_ret <> 0 then
	//return li_ret
	return 1
end if

//------------------------------------------------------------------------------------------------------------------------------------------
//invia i messaggi

ls_oggetto = "Creazione RDA n° "+string(ai_anno_rda)+"/"+string(al_num_rda)
ls_testo = "E' stata creata la seguente RDA: "+string(ai_anno_rda)+"/"+string(al_num_rda)

if upperbound(ls_email_destinatari[])>0 then
	if wf_invia_mail(ls_oggetto, ls_testo, ls_email_destinatari[], ls_cod_mittente, ls_email_mittente, as_messaggio) < 0 then
		return -1
	end if

	//salvo notifiche in apposita tabella comunicazioni -----------------------------------------------------------------------------
	for ll_index=1 to upperbound(ls_cod_destinatari[])
		if wf_salva_notifica(	ai_anno_rda, al_num_rda, ls_cod_destinatari[ll_index], ls_cod_mittente, &
										ll_num_sequenza_corrente, ll_num_sequenza_arrivo, "", as_messaggio) < 0 then
										
		end if
	next

end if


////------------------------------------------------------------------------------------------------------------------------------------------
////fare la retrieve delle notifiche
//tab_dettaglio.det_2.dw_2.retrieve(s_cs_xx.cod_azienda, ai_anno_rda, al_num_rda)



return 0
end function

public function integer wf_sospendi_desospendi (integer ai_anno_rda, long al_num_rda, string as_flag_sospesa, ref string as_errore);string			ls_cod_utente


if isnull(as_flag_sospesa) or as_flag_sospesa="" then as_flag_sospesa="N"

if as_flag_sospesa="S" then
	ls_cod_utente = s_cs_xx.cod_utente
	
else
	setnull(ls_cod_utente)
end if

update tes_rda
set 	flag_sospesa=:as_flag_sospesa,
		cod_utente_sospesa=:ls_cod_utente
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_rda and
			num_registrazione=:al_num_rda;
			
if sqlca.sqlcode<0 then
	as_errore = "Errore in sospensione/desospensione rda: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "RDA "+string(ai_anno_rda)+"/"+string(al_num_rda) + " non trovata nel database!"
	return 1
	
else 
	return 0
	
end if
end function

on w_tes_rda.create
int iCurrent
call super::create
end on

on w_tes_rda.destroy
call super::destroy
end on

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori, ls_db, ls_descrizione


f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "nome + ' ' +cognome", &
					  ls_select_operatori)
					  
					  
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_tipo_rda", &
                 sqlca, &
                 "tab_tipi_rda", &
                 "cod_tipo_rda", &
                 "des_tipo_rda", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//------------------------------------------------------------------------------------------------------------
//datawindow di ricerca

f_po_loaddddw_dw(	tab_ricerca.ricerca.dw_ricerca, &
								"cod_deposito", &
								sqlca, &
								"anag_depositi", &
								"cod_deposito", &
								"des_deposito", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_deposito = 'I' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(	tab_ricerca.ricerca.dw_ricerca, &
								"cod_centro_costo", &
								sqlca, &
								"tab_centri_costo", &
								"cod_centro_costo", &
								"des_centro_costo", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + ")) " )

f_po_loaddddw_dw(	tab_ricerca.ricerca.dw_ricerca, &
								"cod_tipo_rda", &
								sqlca, &
								"tab_tipi_rda", &
								"cod_tipo_rda", &
								"des_tipo_rda", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

end event

event pc_setwindow;call super::pc_setwindow;


string ls_wizard, ls_prova

is_codice_filtro = "RDA"

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")

//##########################################################
//set_w_options(c_noenablepopup + c_closenosave)
//
//tab_dettaglio.det_1.dw_1.set_dw_options(		sqlca, &
//																	i_openparm, &
//																	c_scrollparent + c_noretrieveonopen +  c_nonew + c_nodelete + c_nomodify, &
//																	c_default)

tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)

//##########################################################

tab_dettaglio.det_1.dw_1.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1

il_livello = 0

tab_dettaglio.det_2.dw_2.settransobject(sqlca)
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_tes_rda
integer x = 1321
integer width = 3182
integer height = 2516
boolean raggedright = false
det_2 det_2
end type

on tab_dettaglio.create
this.det_2=create det_2
call super::create
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3145
integer height = 2392
string text = "Dati Testata"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_tes_rda
integer width = 1298
integer height = 2516
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1262
integer height = 2392
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1243
integer height = 2348
string dataobject = "d_tes_rda_ricerca"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_operatore"
		guo_ricerca.uof_ricerca_operaio(dw_ricerca, "cod_operaio_tes")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca, "cod_fornitore")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1262
integer height = 2392
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer height = 2352
integer weight = 700
end type

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if



end event

event tv_selezione::doubleclicked;call super::doubleclicked;

treeviewitem				ltvi_item
str_treeview				lstr_data
m_ordini_acquisto		lm_menu

if AncestorReturnValue < 0 then return

if not wf_is_valid(handle) then return

pcca.window_current = getwindow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then
	tab_ricerca.selezione.tv_selezione.selectitem(handle)
	getwindow().postevent("pc_menu_dettagli")
	
end if
end event

event tv_selezione::rightclicked;call super::rightclicked;long								ll_row, ll_num_sequenza_corrente
treeviewitem					ltvi_item
str_treeview					lstr_data
m_rda							lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row = tab_dettaglio.det_1.dw_1.getrow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then

	lm_menu = create m_rda
	
	select num_sequenza_corrente
	into :ll_num_sequenza_corrente
	from tes_rda
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:lstr_data.decimale[1] and
				num_registrazione=:lstr_data.decimale[2];
	
	if ll_num_sequenza_corrente > 0 then
		lm_menu.m_respingi.enabled = true
	else
		lm_menu.m_respingi.enabled = false
	end if
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

type dw_1 from uo_cs_xx_dw within det_1
integer x = 18
integer y = 20
integer width = 2665
integer height = 2060
integer taborder = 20
string dataobject = "d_tes_rda_1"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

change_dw_current()


//fare la retrieve pure delle notifiche
tab_dettaglio.det_2.dw_2.retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])
end event

event pcd_new;call super::pcd_new;long			ll_row
string		ls_cod_operaio, ls_cod_valuta


ll_row = this.getrow()

this.setitem(ll_row, "data_registrazione", datetime(today(), 00:00:00))


select cod_operaio
into :ls_cod_operaio
from anag_operai
where		cod_azienda = :s_cs_xx.cod_azienda and
				cod_utente = :s_cs_xx.cod_utente;
			 
if sqlca.sqlcode = 0 and ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then
	this.setitem(ll_row, "cod_operaio", ls_cod_operaio)
end if

ib_new = true
ib_modify = false


select stringa
into :ls_cod_valuta
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LIR';
		  
if ls_cod_valuta<>"" and not isnull(ls_cod_valuta) then
	setitem(ll_row,"cod_valuta", ls_cod_valuta)
end if
end event

event pcd_setkey;call super::pcd_setkey;
long					ll_i, ll_num_registrazione, ll_num_registrazione_2

integer				li_anno_registrazione


ll_i = getrow()

if ll_i>0 then
else
	return
end if
	
if isnull(this.getitemstring(ll_i, "cod_azienda")) then
	this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
end if
	
li_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
	
if li_anno_registrazione <= 0 or isnull(li_anno_registrazione) then
		
	li_anno_registrazione = f_anno_esercizio()
	
	if li_anno_registrazione > 0 then
		this.setitem(this.getrow(), "anno_registrazione", li_anno_registrazione)
	else
		li_anno_registrazione = year(today())
	end if
	
	setnull(ll_num_registrazione)
	setnull(ll_num_registrazione_2)
	
	//per gestione compatibilità iniziale (uso maschera testata)
	
	//max da det_rda
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   det_rda
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_registrazione;
	
	//max da tes_rda
	select max(num_registrazione)
	into   :ll_num_registrazione_2
	from   tes_rda
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_registrazione;
		 
	if isnull(ll_num_registrazione) then ll_num_registrazione=0
	if isnull(ll_num_registrazione_2) then ll_num_registrazione_2=0
	
	//assegno l'incremento +1 del + grande num. reg. tra la tes e la det
	if ll_num_registrazione_2 > ll_num_registrazione then ll_num_registrazione = ll_num_registrazione_2
	
	ll_num_registrazione += 1
	
	this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
	
end if
end event

event pcd_modify;call super::pcd_modify;

ib_new = false
ib_modify = true
end event

event pcd_view;call super::pcd_view;

ib_new = false
ib_modify = false
end event

event pcd_save;call super::pcd_save;
ib_new = false
ib_modify = false
end event

event updatestart;call super::updatestart;if i_extendmode then

	long					ll_num_registrazione, ll_i,ll_handle
	integer				li_anno_registrazione, li_ret
	string				ls_errore

	
	// controllo righe cancellate ed eseguo eventuali cancellazioni di righe referenziate.
	for ll_i = 1 to this.deletedcount()
		
		li_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione", delete!, true)
		
		//cancello comunicazioni
		delete from tes_rda_notifiche
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :li_anno_registrazione and
					num_registrazione = :ll_num_registrazione;
					
		if sqlca.sqlcode < 0 then
			g_mb.error("APICE", "Errore durante la cancellazione delle notifiche relative ai cicli di vita della RDA: "+sqlca.sqlerrtext)
			return 1
		end if
		
		//cancello eventuali dettagli
		if f_del_det("det_rda", li_anno_registrazione, ll_num_registrazione) = -1 then
			set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
		//elimino nodo selezionato
		tab_ricerca.selezione.tv_selezione.findItem(CurrentTreeItem!, ll_handle)
		
		if ll_handle > 0 then tab_ricerca.selezione.tv_selezione.deleteitem(ll_handle)
		
		if DeletedCount() > 0 then
			wf_set_deleted_item_status()
		end if
   next

	//aggiungo l'icona all'albero
	if ib_new then
		wf_inserisci_singola_rda(		0, &
												getitemnumber(getrow(), "anno_registrazione"), &
												getitemnumber(getrow(), "num_registrazione"), &
												getitemdatetime(getrow(), "data_registrazione"), &
												getitemstring(getrow(), "flag_stato_autorizzato"), &
												getitemstring(getrow(), "cod_valuta"))
												
	end if

end if
end event

event updateend;call super::updateend;long					ll_row, ll_num_rda

integer				li_ret, li_anno_rda

string				ls_errore


//inserimento elemento sull'albero se nuovo
ll_row = this.getrow()

if ll_row>0 then
	choose case this.getitemstatus(ll_row, 0, Primary!)
			
		case NewModified!
			
			li_anno_rda = this.getitemnumber( ll_row, "anno_registrazione")
			ll_num_rda = this.getitemnumber(getrow(), "num_registrazione")
			
			li_ret = wf_mail_insert_rda(		li_anno_rda, &
														ll_num_rda, &
														ls_errore)
			if li_ret<0 then
				rollback;
//				
//				if ls_errore<>"" and not isnull(ls_errore) then
//					if li_ret<0 then
//						g_mb.error(ls_errore)
//					else
//						g_mb.warning(ls_errore)
//					end if
//				end if
				
			else
				commit;
				
				//------------------------------------------------------------------------------------------------------------------------------------------
				//fare la retrieve delle notifiche
				tab_dettaglio.det_2.dw_2.retrieve(s_cs_xx.cod_azienda, li_anno_rda, ll_num_rda)
				
			end if
			
		case New!
			
	end choose

end if
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3145
integer height = 2392
long backcolor = 12632256
string text = "Notifiche"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from datawindow within det_2
integer x = 14
integer y = 12
integer width = 3122
integer height = 2360
integer taborder = 40
string title = "none"
string dataobject = "d_tes_rda_notifiche"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

