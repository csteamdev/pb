﻿$PBExportHeader$w_tipi_rda.srw
$PBExportComments$Finestra Gestione Tipi Ordini Acquisti
forward
global type w_tipi_rda from w_cs_xx_principale
end type
type dw_tipi_rda_lista from uo_cs_xx_dw within w_tipi_rda
end type
type dw_tipi_rda_det from uo_cs_xx_dw within w_tipi_rda
end type
end forward

global type w_tipi_rda from w_cs_xx_principale
integer width = 2258
integer height = 1624
string title = "Gestione Tipi RDA"
dw_tipi_rda_lista dw_tipi_rda_lista
dw_tipi_rda_det dw_tipi_rda_det
end type
global w_tipi_rda w_tipi_rda

type variables

end variables

event pc_setwindow;call super::pc_setwindow;
dw_tipi_rda_lista.set_dw_key("cod_azienda")

dw_tipi_rda_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
												 
dw_tipi_rda_det.set_dw_options(sqlca, &
                                   dw_tipi_rda_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
											  
iuo_dw_main = dw_tipi_rda_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_rda_det, &
                 "cod_tipo_off_acq", &
                 sqlca, &
                 "tab_tipi_off_acq", &
                 "cod_tipo_off_acq", &
                 "des_tipo_off_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipi_rda_det, &
                 "cod_tipo_ord_acq", &
                 sqlca, &
                 "tab_tipi_ord_acq", &
                 "cod_tipo_ord_acq", &
                 "des_tipo_ord_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
// stefanop 09/08/2010: cicli di vita
f_po_loaddddw_dw(dw_tipi_rda_det, &
                 "cod_tipo_lista_dist", &
                 sqlca, &
                 "tab_tipi_liste_dist", &
                 "cod_tipo_lista_dist", &
                 "descrizione", "")
					  
f_po_loaddddw_dw(dw_tipi_rda_det, &
                 "cod_lista_dist", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", "")
// ----

f_po_loaddddw_dw(dw_tipi_rda_det, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", "")

end event

on w_tipi_rda.create
int iCurrent
call super::create
this.dw_tipi_rda_lista=create dw_tipi_rda_lista
this.dw_tipi_rda_det=create dw_tipi_rda_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_rda_lista
this.Control[iCurrent+2]=this.dw_tipi_rda_det
end on

on w_tipi_rda.destroy
call super::destroy
destroy(this.dw_tipi_rda_lista)
destroy(this.dw_tipi_rda_det)
end on

type dw_tipi_rda_lista from uo_cs_xx_dw within w_tipi_rda
integer x = 23
integer y = 20
integer width = 2171
integer height = 500
integer taborder = 10
string dataobject = "d_tipi_rda_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_tipi_rda_det from uo_cs_xx_dw within w_tipi_rda
integer x = 23
integer y = 520
integer width = 2171
integer height = 972
integer taborder = 20
string dataobject = "d_tipi_rda_det"
borderstyle borderstyle = styleraised!
boolean ib_dw_detail = true
end type

event itemchanged;call super::itemchanged;string ls_null

choose case dwo.name
	case "cod_tipo_lista_dist"
		setnull(ls_null)
		
		setitem(row, "cod_lista_dist", ls_null)
		
		if not isnull(data) and data <> "" then
			
			f_po_loaddddw_dw(dw_tipi_rda_det, &
						  "cod_lista_dist", &
						  sqlca, &
						  "tes_liste_dist", &
						  "cod_lista_dist", &
						  "des_lista_dist", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_tipo_lista_dist='" + data + "'")
						  
		end if
end choose
end event

