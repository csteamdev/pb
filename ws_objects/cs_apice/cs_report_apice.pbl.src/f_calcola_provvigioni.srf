﻿$PBExportHeader$f_calcola_provvigioni.srf
$PBExportComments$Funzione di calcolo provvigioni in dettaglio fattura
global type f_calcola_provvigioni from function_object
end type

forward prototypes
global function integer f_calcola_provvigioni (string fs_cod_azienda, long fl_num_registrazione, long fl_anno_registrazione, long fl_prog_riga, ref double fd_imponibile_provv_1, ref double fd_imponibile_provv_2, ref double fd_importo_provv_1, ref double fd_importo_provv_2, ref double fd_provv_1, ref double fd_provv_2, ref string fs_messaggio)
end prototypes

global function integer f_calcola_provvigioni (string fs_cod_azienda, long fl_num_registrazione, long fl_anno_registrazione, long fl_prog_riga, ref double fd_imponibile_provv_1, ref double fd_imponibile_provv_2, ref double fd_importo_provv_1, ref double fd_importo_provv_2, ref double fd_provv_1, ref double fd_provv_2, ref string fs_messaggio);// calcolo delle provvigioni di una riga di dettaglio fatture vendita
// ritorna 0 se ok, -1 se errori
// risultati: imponibile provvigioni e importo provvigioni negli argomenti per reference

double ld_quan_documento, ld_prezzo_vendita, ld_aliquota_iva, &
       ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
       ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_pagamento, &
       ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
       ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
       ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
       ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
       ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
       ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
       ld_val_sconto_pagamento, ld_val_riga_sconto_pagamento, &
		 ld_tot_sconti_cassa, ld_tot_sconti_commerciali, ld_tot_imponibile, &
		 ld_tot_iva, ld_tot_fattura, ld_tot_imp_iva_val, ld_tot_iva_val, ld_tot_fattura_val, &
		 ld_tot_imp_provv_1, ld_tot_provv_1, ld_tot_imp_provv_2, ld_tot_provv_2, ld_provv_1, &
		 ld_provv_2, ld_sconto_testata, ld_imponibile_riga, ld_iva_riga, ld_importo_provv_1, &
		 ld_importo_provv_2, ld_val_sconti_riga, ld_cambio_ven
string ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_sql_stringa, ls_lire, ls_cod_gruppo, &
		 ls_flag_sola_iva, ls_cod_pagamento, ls_cod_valuta, ls_cod_iva, ls_cod_tipo_fat_ven, &
		 ls_cod_cliente, ls_flag_tipo_cliente, ls_flag_tipo_fat_ven, ls_flag_calcolo, &
		 ls_flag_cambio_zero, ls_cod_tipo_movimento
datetime ldt_data_registrazione
long ll_i, ll_prog_riga_fat_ven

setnull(fs_messaggio)
ll_prog_riga_fat_ven = fl_prog_riga

if isnull(fl_anno_registrazione) or fl_anno_registrazione < 1 then
	fs_messaggio = "Calcolo provvigioni: anno registrazione non indicato"
	return -1
end if

if isnull(fl_num_registrazione) or fl_num_registrazione < 1 then
	fs_messaggio = "Calcolo provvigioni: numero registrazione non indicato"
	return -1
end if

ls_cod_cliente = ""

select flag_calcolo, 
       flag_cambio_zero,
       cod_valuta,   
       cod_pagamento,   
       sconto,
	 	 cod_tipo_fat_ven,
	 	 cod_cliente,
	 	 data_registrazione,
	 	 cambio_ven
into   :ls_flag_calcolo,
       :ls_flag_cambio_zero,
       :ls_cod_valuta,   
       :ls_cod_pagamento,   
       :ld_sconto_testata,
	 	 :ls_cod_tipo_fat_ven,
	 	 :ls_cod_cliente,
	 	 :ldt_data_registrazione,
	 	 :ld_cambio_ven
from   tes_fat_ven
where  cod_azienda        = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione ;

if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
	fs_messaggio = "Errore in ricerca testata fattura durante calcolo provvigioni"
	return -1
end if

if ls_cod_cliente = "" or isnull(ls_cod_cliente) then
	fs_messaggio = "Errore in ricerca testata fattura:  cliente non letto"
	return -1
end if


select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
	 parametri_azienda.flag_parametro = 'S' and &
	 parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Configurare il codice valuta per le Lire Italiane."
	return -1
end if

if ls_lire = ls_cod_valuta then
	ld_cambio_ven = 1
else
	if ls_flag_cambio_zero = "S" then
		ld_cambio_ven = f_cambio_val_ven( ls_cod_valuta, ldt_data_registrazione )
	end if
end if


ld_tot_sconti_commerciali = 0
ld_tot_imp_provv_1 = 0
ld_tot_imp_provv_2 = 0
ld_tot_provv_1 = 0
ld_tot_provv_2 = 0
ld_tot_imp_iva_val = 0
ld_tot_iva = 0
ld_tot_imponibile = 0
ld_tot_iva_val = 0
ld_tot_fattura = 0
ld_tot_fattura_val = 0

select flag_tipo_cliente
into   :ls_flag_tipo_cliente
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente ;
if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
	fs_messaggio = "Errore durante calcolo fattura"
	return -1
end if

select flag_tipo_fat_ven
into   :ls_flag_tipo_fat_ven
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven ;
if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
	fs_messaggio = "Errore in ricerca tipo fattura in calcolo fattura"
	return -1
end if
		 
if isnull(ld_sconto_testata) then ld_sconto_testata = 0

select tab_pagamenti.sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
		 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	ld_sconto_pagamento = 0
end if

ll_i = fl_prog_riga

select cod_tipo_det_ven , quan_fatturata , prezzo_vendita , sconto_1 ,
			sconto_2 , provvigione_1 , provvigione_2 , cod_iva ,
			cod_tipo_movimento , sconto_3, sconto_4, sconto_5, sconto_6,
			sconto_7, sconto_8, sconto_9, sconto_10
into :ls_cod_tipo_det_ven , :ld_quan_documento , :ld_prezzo_vendita , :ld_sconto_1 ,
			:ld_sconto_2 , :ld_provv_1 , :ld_provv_2 , :ls_cod_iva , :ls_cod_tipo_movimento, 
			:ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7,
			:ld_sconto_8, :ld_sconto_9, :ld_sconto_10
from det_fat_ven
where  cod_azienda        = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 prog_riga_fat_ven = :fl_prog_riga ;
	
fd_provv_1 = ld_provv_1
fd_provv_2 = ld_provv_2

select tab_tipi_det_ven.flag_tipo_det_ven,
		 tab_tipi_det_ven.flag_sola_iva
into   :ls_flag_tipo_det_ven,
	 	 :ls_flag_sola_iva
from   tab_tipi_det_ven
where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
	 	tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Si è verificato un errore in ricerca tipi dettaglio fattura"
	return -1
end if

ld_val_riga = ld_quan_documento * ld_prezzo_vendita
ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
ld_val_sconti_riga = ld_val_riga - ld_val_riga_sconto_10

if isnull(ld_provv_1) then ld_provv_1 = 0
if isnull(ld_provv_2) then ld_provv_2 = 0

if isnull(ls_cod_iva) then
	ld_aliquota_iva = 0
else
	select tab_ive.aliquota
	into   :ld_aliquota_iva
	from   tab_ive
	where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and
			 tab_ive.cod_iva = :ls_cod_iva;
if sqlca.sqlcode <> 0 then ld_aliquota_iva = 0
end if



choose case ls_flag_tipo_det_ven
case "M", "C", "N"
	ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
	ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
	ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
	ld_imponibile_riga = ld_val_riga - &
								ld_val_sconto_testata - &
								ld_val_sconto_pagamento - &
								ld_val_sconti_riga

	ld_imponibile_riga = round(ld_imponibile_riga, 0)
	if ls_flag_sola_iva = "N" then
//		ld_tot_imponibile = ld_tot_imponibile + round(ld_imponibile_riga, 0)
		if ld_provv_1 > 0 then
			fd_importo_provv_1 = (ld_imponibile_riga * ld_provv_1) / 100
			fd_imponibile_provv_1 = round(ld_imponibile_riga, 0)
		end if
		if ld_provv_2 > 0 then
			fd_importo_provv_2 = (ld_imponibile_riga * ld_provv_2) / 100
			fd_imponibile_provv_2 = round(ld_imponibile_riga, 0)
		end if
	end if
case "S"
	ld_imponibile_riga = ld_val_riga
	ld_imponibile_riga = round(ld_imponibile_riga, 0)

	if ls_flag_sola_iva = "N" then
//		ld_tot_imponibile = ld_tot_imponibile - round(ld_imponibile_riga, 0)
		if ld_provv_1 > 0 then
			fd_importo_provv_1 = - (ld_imponibile_riga * ld_provv_1) / 100
			fd_imponibile_provv_1 = - round(ld_imponibile_riga, 0)
		end if
		if ld_provv_2 > 0 then
			fd_importo_provv_2 = - (ld_imponibile_riga * ld_provv_2) / 100
			fd_imponibile_provv_2 = - round(ld_imponibile_riga, 0)
		end if
	end if

case "I"
	ld_imponibile_riga = ld_val_riga
	ld_imponibile_riga = round(ld_imponibile_riga, 0)

	if ls_flag_sola_iva = "N" then
		if ld_provv_1 > 0 then
			fd_importo_provv_1 = (ld_imponibile_riga * ld_provv_1) / 100
			fd_imponibile_provv_1 = round(ld_imponibile_riga, 0)
		end if
		if ld_provv_2 > 0 then
			fd_importo_provv_2 = (ld_imponibile_riga * ld_provv_2) / 100
			fd_imponibile_provv_2 = round(ld_imponibile_riga, 0)
		end if
	end if

case "T"
	ld_imponibile_riga = ld_val_riga
	ld_imponibile_riga = round(ld_imponibile_riga, 0)

	if ls_flag_sola_iva = "N" then
		if ld_provv_1 > 0 then
			fd_importo_provv_1 = (ld_imponibile_riga * ld_provv_1) / 100
			fd_imponibile_provv_1 = round(ld_imponibile_riga, 0)
		end if
		if ld_provv_2 > 0 then
			fd_importo_provv_2 = (ld_imponibile_riga * ld_provv_2) / 100
			fd_imponibile_provv_2 = round(ld_imponibile_riga, 0)
		end if
	end if

case "B"
	ld_imponibile_riga = ld_val_riga
	ld_imponibile_riga = round(ld_imponibile_riga, 0)

	if ls_flag_sola_iva = "N" then
		if ld_provv_1 > 0 then
			fd_importo_provv_1 = (ld_imponibile_riga * ld_provv_1) / 100
			fd_imponibile_provv_1 = round(ld_imponibile_riga, 0)
		end if
		if ld_provv_2 > 0 then
			fd_importo_provv_2 = (ld_imponibile_riga * ld_provv_2) / 100
			fd_imponibile_provv_2 = round(ld_imponibile_riga, 0)
		end if
	end if

case "V"
	ld_imponibile_riga = ld_val_riga
	ld_imponibile_riga = round(ld_imponibile_riga, 0)

	if ls_flag_sola_iva = "N" then
		if ld_provv_1 > 0 then
			fd_importo_provv_1 = (ld_imponibile_riga * ld_provv_1) / 100
			fd_imponibile_provv_1 = round(ld_imponibile_riga, 0)
		end if
		if ld_provv_2 > 0 then
			fd_importo_provv_2 = (ld_imponibile_riga * ld_provv_2) / 100
			fd_imponibile_provv_2 = round(ld_imponibile_riga, 0)
		end if
	end if
	if ld_provv_1 > 0 then
		fd_importo_provv_1 = (ld_imponibile_riga * ld_provv_1) / 100
//		ld_tot_imp_provv_1 = ld_tot_imp_provv_1 + ld_imponibile_riga // cosa è?
		fd_imponibile_provv_1 = round(ld_imponibile_riga, 0)
	end if
	if ld_provv_2 > 0 then
		fd_importo_provv_2 = (ld_imponibile_riga * ld_provv_2) / 100
//		ld_tot_imp_provv_2 = ld_tot_imp_provv_2 + ld_imponibile_riga  // cosa è?
		fd_imponibile_provv_2 = round(ld_imponibile_riga, 0)
	end if
case else
	g_mb.messagebox("Stampa Provvigioni", "Errore: flag_tipo_det_ven non riconosciuto")
end choose
if ls_flag_tipo_fat_ven = 'N' then 
	fd_importo_provv_1 = fd_importo_provv_1 * (- 1)
	fd_importo_provv_2 = fd_importo_provv_2 * (- 1)
	fd_imponibile_provv_2 = fd_imponibile_provv_2 * (- 1)
	fd_imponibile_provv_1 = fd_imponibile_provv_1 * (- 1)
end if

return 0
end function

