﻿$PBExportHeader$uo_analisi.sru
forward
global type uo_analisi from nonvisualobject
end type
end forward

global type uo_analisi from nonvisualobject
end type
global uo_analisi uo_analisi

type variables
/**
 * stefanop
 * 02/05/2012
 *
 * Oggetto per lanciare un'analisi di BI:
 * 
 * 1. uof_set_analisi(CODICE_ANALISI)
 * 2. uof_execute()
 *
 **/
 
 private:
	string is_error
	string is_desktop_folder
	
	// codice dell'analisi da lanciare
	string is_cod_analisi
	
	// percorso del file dell'analisi
	string is_file
	
	uo_formule_calcolo iuo_formule
	uo_log iuo_log
	
	boolean ib_debug = false 
	boolean ib_log = false

end variables

forward prototypes
private function integer uof_check_and_copy_excel (string as_source_file)
public function string uof_get_error ()
public function integer uof_set_analisi (string as_cod_analisi)
public function integer uof_execute ()
public function string uof_get_file ()
public subroutine uof_set_param (string as_key, string as_value)
public subroutine uof_remove_param (string as_key)
public subroutine uof_resert_param ()
public subroutine uof_set_debug (boolean ab_debug_mode)
private subroutine uof_log (string as_message)
private function boolean uof_delete_file ()
public subroutine uof_set_param (string as_key, long al_value)
public subroutine uof_set_param (string as_key, date adt_value)
private function integer uof_get_param_value (string as_cod_analisi, integer ai_num_riga, integer ai_num_colonna, string as_cod_formula, string as_cod_parametro, ref string as_valore, ref string as_errore)
public subroutine uof_set_log (boolean ab_enable)
public subroutine uof_set_file_path (string as_path_file)
end prototypes

private function integer uof_check_and_copy_excel (string as_source_file);/**
 * stefanop
 * 30/04/2012
 *
 * Controllo che il file excel base da usare nell'analisi esista,
 * se esiste ne creo una copia nella cartella locale dell'utente
 * così da non sporcare con dei valori il documento base.
 **/
 
string ls_desktop_file, ls_file_dir, ls_file_name, ls_file_ext
  
if not fileexists(as_source_file) then
	// il file non esiste
	is_error = g_str.format("Attenzione il file template non è stato trovato o non si dispongolo delle autorizzazioni necessario.~r~nPercorso: $1", as_source_file)
	if ib_log then iuo_log.error(is_error)
	return -1
end if

// 1.1 Recupero informazione file
guo_functions.uof_get_file_info(as_source_file, ref ls_file_dir, ref ls_file_name, ref ls_file_ext)


//09/11/2012 Donato
//Se ho impostato io dall'esterno il percorso desiderato del file, usa quello
//altrimenti usa quello predefinito del dektop dell'utente
if is_file="" or isnull(is_file) then
	// TODO: recuperare l'estensione del file origine e usarla nel file di destinazione
	is_file = is_desktop_folder + ls_file_name + string(now(), "_hh_mm_ss") + "." + ls_file_ext
end if
//
//// TODO: recuperare l'estensione del file origine e usarla nel file di destinazione
//is_file = is_desktop_folder + ls_file_name + string(now(), "_hh_mm_ss") + "." + ls_file_ext


if filecopy(as_source_file, is_file, true) < 1 then
	is_error = "Errore durante la copia in locale del file template dell'analisi.~r~nFile: " + is_file
	if ib_log then iuo_log.error(is_error)
	setnull(is_file)
	return -1
end if

return 0
end function

public function string uof_get_error ();return this.is_error
end function

public function integer uof_set_analisi (string as_cod_analisi);/**
 * Imposta il codice dell'analisi, prima controlla che il codice esista
 **/
 
string ls_test

select cod_analisi 
into :ls_test
from tes_analisi
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :as_cod_analisi;
		 
if sqlca.sqlcode < 0 then
	is_error = "Errore durante il controllo dell'analisi.~r~n" + sqlca.sqlerrtext
	setnull(is_cod_analisi)
	return -1
elseif sqlca.sqlcode = 100 then
	is_error  = "Il codice analisi passato non esiste nel database."
	setnull(is_cod_analisi)
	return -1
end if

is_cod_analisi = as_cod_analisi
return 0

end function

public function integer uof_execute ();string  		ls_path_file_excel, ls_sql, ls_cod_formula, ls_select_formula, ls_error, ls_flag_tipo_voce, ls_cod_parametro, la_valore_parametro, &	
		 		ls_worksheet, ls_flag_negativo, ls_formula_return, ls_separator
		 
int 			li_righe_formule, li_formula, li_num_colonna, li_param, li_formula_param, li_ret, li_num_riga, li_num_riga_precedente

long 			ll_ds_rows, ll_ds_column, ll_i, ll_c, ll_row_offset, ll_riga_excel, ll_offset_riga, ll_offset_nuove_righe, ll_offset_nuove_righe_old

decimal 		ld_risultato

datastore 	lds_formule, lds_formule_param

uo_excel 	luo_excel

uo_ds 		luo_risultati_formula

if ib_log then iuo_log.info("Avvio analisi " + is_cod_analisi)

iuo_formule.ib_log = ib_log

if isnull(is_cod_analisi) or is_cod_analisi = "" then
	is_error = "Il codice analisi non risulta impostato!"
	if ib_log then iuo_log.error("Codice analisi non impostato")
	return -1
end if

select path_file_excel, worksheet
into :ls_path_file_excel, :ls_worksheet
from tes_analisi
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :is_cod_analisi;
		 
if sqlca.sqlcode <> 0 then
	is_error = "Errore durante la lettura dei dettagli dell'analisi selezionata." + sqlca.sqlerrtext
	if ib_log then iuo_log.error(g_str.format("Errore durante la lettura dell'analisini. $1", sqlca.sqlerrtext))
	return -1
end if

// 1. controllo il file e ne faccio una copia
if uof_check_and_copy_excel(ls_path_file_excel) = -1 then
	return -1
end if

// 2. Apro il file e inzio l'elaborazione
luo_excel = create uo_excel
luo_excel.uof_open(is_file, ib_log)
luo_excel.uof_set_sheet(ls_worksheet)

ls_sql = "SELECT num_riga, num_colonna, cod_formula, flag_negativo FROM tes_analisi_formule WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_analisi='" + is_cod_analisi + "' " + &
			"ORDER BY  num_riga, num_colonna"
li_righe_formule = guo_functions.uof_crea_datastore(lds_formule, ls_sql, ls_error)

if li_righe_formule < 0 then
	is_error = "Errore durante la creazione del datastore delle formule." + ls_error
	if ib_log then iuo_log.error(is_error)
	uof_delete_file()
	destroy lds_formule
	destroy luo_excel
	return -1
elseif ib_log then
	iuo_log.log(g_str.format("Trovate $1 formule da analizzare", li_righe_formule))
end if

ll_row_offset = 0
li_num_riga_precedente = 0

for li_formula = 1 to li_righe_formule
	
	li_num_riga = lds_formule.getitemnumber(li_formula, 1)
	li_num_colonna = lds_formule.getitemnumber(li_formula, 2)
	ls_cod_formula = lds_formule.getitemstring(li_formula, 3)
	ls_flag_negativo = lds_formule.getitemstring(li_formula, 4)
	setnull(ld_risultato)
	
	if li_num_riga_precedente <> li_num_riga then
		// Ho cambiato riga e quindi aggiungo l'offset di riga
		// Se è a zero allora uso l'offset precedente perchè cmq le righe sono state
		// spostate in giù da una formula datastore.
		if ll_offset_nuove_righe = 0 then
			ll_offset_nuove_righe = ll_offset_nuove_righe_old
		else
			ll_offset_nuove_righe_old = ll_offset_nuove_righe
		end if
		// ---
		
		ll_riga_excel = li_num_riga + ll_offset_nuove_righe
		li_num_riga_precedente = li_num_riga
		ll_offset_nuove_righe = 0
	end if
		
	if ib_log then iuo_log.info(g_str.format("Formula r:$1, c:$2, formula:$3, negativo:$4",li_num_riga, li_num_colonna,ls_cod_formula,ls_flag_negativo))
	
	// recupero le parametri formula
	ls_sql = "SELECT cod_parametro, valore_parametro FROM tes_analisi_formule_parametri " + &
				"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_analisi='" + is_cod_analisi + "' " + &
				" AND num_riga=" + string(li_num_riga) + " AND num_colonna=" + string(li_num_colonna) + &
				" AND cod_formula='" + ls_cod_formula + "'"
			
	li_formula_param = guo_functions.uof_crea_datastore(lds_formule_param, ls_sql, ls_error)
	if li_righe_formule < 0 then
		is_error = "Errore durante la creazione del datastore delle formule." + ls_error
		if ib_log then iuo_log.error(is_error)
	end if
	
	
	// TODO: Prima di settare i parametri per l'analisi potrebbe essere interessante salvare un copia della iuo_cache che contiene
	// i parametri globali per poi reimpostarla al termine del calcolo.
	// Così due parametri con lo stesso nome vengono sovrascritti.
	for li_param = 1 to li_formula_param
		
		uof_set_param(lds_formule_param.getitemstring(li_param, 1), lds_formule_param.getitemstring(li_param, 2))
		if ib_log then iuo_log.info(g_str.format("~t$1 = $2", lds_formule_param.getitemstring(li_param, 1), lds_formule_param.getitemstring(li_param, 2)))
		
	next
	// ----

	if not iuo_formule.uof_is_formula_datastore(ls_cod_formula) then
		// è una formula normale
		li_ret = iuo_formule.uof_calcola_formula(ls_cod_formula, ls_select_formula, ld_risultato, ls_error)
		
		if li_ret < 0 then
		// E' successo un errore
		destroy lds_formule
		destroy luo_excel
		uof_delete_file()
		is_error = "Errore durante l'esequzione della formula " + ls_cod_formula + ".~r~n" + &
						"Riga: " + string(li_num_riga) + " - Colonna: " + string(li_num_colonna) + "~r~n" + ls_error

		if ib_log then iuo_log.error(is_error)
			return -1
		end if
		
		if ls_flag_negativo = "S" then 
			ld_risultato = (ld_risultato) * (-1)
		end if
		
		if ib_log then iuo_log.info("~tRisultato: " + string(ld_risultato))
		
		luo_excel.uof_set(ll_riga_excel, li_num_colonna, ld_risultato)
	
	else
		// è un datastore.
		ls_formula_return = iuo_formule.uof_calcola_formula_datastore(ls_cod_formula, luo_risultati_formula, ls_error)
		
		if ls_formula_return = "OK" then
			// recupero numero di righe della formula
			ll_ds_rows = luo_risultati_formula.rowcount()
			
			if ll_ds_rows > 0 then
				// recupero numero colonne della datastore
				ll_ds_column = luo_risultati_formula.uof_get_column_count()
				ll_offset_riga = 0
															
				for ll_i = 1 to ll_ds_rows
					ls_separator = ""
					uo_string_builder luo_sb
										
					if ll_i >= ll_offset_riga + 2 and ll_i > ll_offset_nuove_righe then
						ll_offset_riga++
						luo_excel.uof_insert_row(ll_riga_excel + ll_offset_riga, uo_excel.INSERT_DOWN)
					elseif   ll_i >= ll_offset_riga + 2 then
						ll_offset_riga++
					end if
					
					// Scrivo le colonne
					luo_sb = g_str.append(ls_separator)
					for ll_c = 1 to ll_ds_column
						luo_excel.uof_set(ll_riga_excel + ll_offset_riga, li_num_colonna + ll_c - 1, luo_risultati_formula.getitemany(ll_i, ll_c))
					next
					// ----
					
				next
				
				ll_offset_nuove_righe = max(ll_offset_nuove_righe, ll_offset_riga)
			
			end if
			
		else
			if ib_log then iuo_log.error("~tErrore: " + ls_error)
			g_mb.error(ls_error)
			exit
		end if
		
	end if
	
next

destroy lds_formule_param
destroy luo_excel

if ib_log then iuo_log.info("Fine elaborazione analisi")
return 0
end function

public function string uof_get_file ();return this.is_file
end function

public subroutine uof_set_param (string as_key, string as_value);/**
 * stefanop
 * 03/05/2012
 *
 * Imposto il paraemtri all'interno dell'analisi
 **/

iuo_formule.iuo_cache.set(as_key, "'"+ as_value +"'" )
end subroutine

public subroutine uof_remove_param (string as_key);/**
 * stefanop
 * 03/05/2012
 *
 * Rimuovo il parametro impostato all'interno della cache delle formule
 **/

iuo_formule.iuo_cache.remove(as_key)
end subroutine

public subroutine uof_resert_param ();/**
 * stefanop
 * 03/05/2012
 *
 * Rimuove tutti i paremtri della formula
 **/

iuo_formule.iuo_cache.removeall()
end subroutine

public subroutine uof_set_debug (boolean ab_debug_mode);this.ib_debug = ab_debug_mode
this.ib_log = ab_debug_mode
end subroutine

private subroutine uof_log (string as_message);/**
 * stefanop
 * 04/05/2012
 *
 * Scrivo un file di log.
 **/
 
if ib_log then

end if
end subroutine

private function boolean uof_delete_file ();if fileexists(is_file) then
	return filedelete(is_file)
end if
end function

public subroutine uof_set_param (string as_key, long al_value);/**
 * stefanop
 * 03/05/2012
 *
 * Imposto il paraemtri all'interno dell'analisi
 **/

iuo_formule.iuo_cache.set(as_key, string(al_value) )
end subroutine

public subroutine uof_set_param (string as_key, date adt_value);/**
 * stefanop
 * 03/05/2012
 *
 * Imposto il paraemtri all'interno dell'analisi
 **/

iuo_formule.iuo_cache.set(as_key, "'" + string(adt_value, s_cs_xx.db_funzioni.formato_data) + "'" )
end subroutine

private function integer uof_get_param_value (string as_cod_analisi, integer ai_num_riga, integer ai_num_colonna, string as_cod_formula, string as_cod_parametro, ref string as_valore, ref string as_errore);select valore_parametro
into :as_valore
from tes_analisi_formule_parametri
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :as_cod_analisi and
		 num_riga = :ai_num_riga and
		 num_colonna = :ai_num_colonna and
		 cod_formula = :as_cod_formula and
		 cod_parametro = :as_cod_parametro;
		 
if sqlca.sqlcode < 0 then
	as_errore = "Errore durante il recupero del parametro " + as_cod_parametro + " per la formula di impresa." + sqlca.sqlerrtext
elseif sqlca.sqlcode = 100 then
	as_errore = "Il parametro " + as_cod_parametro + " per la formula di impresa non è stato trovato."
end if

if sqlca.sqlcode <> 0 then
	as_errore += "~r~nAnalisi: " + as_cod_analisi + "~r~n" + &
				  "Riga: " + string(ai_num_riga) + "~r~n" + &
				  "Colonna:" + string(ai_num_colonna)  + "~r~n" + &
				  "Formula:" + as_cod_formula
				  
	return -1
else
	return 0
end if
			
end function

public subroutine uof_set_log (boolean ab_enable);this.ib_log = ab_enable
iuo_log.open("C:\temp\analisi.log")
end subroutine

public subroutine uof_set_file_path (string as_path_file);
if isnull(as_path_file) then as_path_file = ""

is_file = as_path_file
end subroutine

on uo_analisi.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_analisi.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_formule = create uo_formule_calcolo
iuo_log = create uo_log
is_desktop_folder = guo_functions.uof_get_user_desktop_folder()
end event

event destructor;destroy iuo_formule
end event

