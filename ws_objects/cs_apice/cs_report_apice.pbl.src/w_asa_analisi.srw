﻿$PBExportHeader$w_asa_analisi.srw
forward
global type w_asa_analisi from w_cs_xx_principale
end type
type st_wait from statictext within w_asa_analisi
end type
type cbx_log from checkbox within w_asa_analisi
end type
type dw_parametri from uo_std_dw within w_asa_analisi
end type
type cb_esegui from commandbutton within w_asa_analisi
end type
type dw_analisi from uo_std_dw within w_asa_analisi
end type
end forward

global type w_asa_analisi from w_cs_xx_principale
integer width = 2441
integer height = 1384
string title = "Analisi"
st_wait st_wait
cbx_log cbx_log
dw_parametri dw_parametri
cb_esegui cb_esegui
dw_analisi dw_analisi
end type
global w_asa_analisi w_asa_analisi

type variables
private:
	string is_user_file
	string is_user_desktop
end variables

forward prototypes
public function integer wf_esegui ()
public subroutine wf_start (boolean ab_status)
end prototypes

public function integer wf_esegui ();/**
 * stefanop
 * 30/04/2012
 *
 * Esegui l'analisi impostata
 **/
 
string ls_cod_analisi, ls_valore
int li_result, li_i
uo_analisi luo_analisi
 
luo_analisi = create uo_analisi
 
dw_analisi.accepttext()
dw_parametri.accepttext()
ls_cod_analisi = dw_analisi.getitemstring(1, "cod_analisi")
 
wf_start(true)
 
luo_analisi.uof_set_analisi(ls_cod_analisi)
 
// imposto i parametri
if dw_parametri.rowcount() > 0 then
	for li_i = 1 to dw_parametri.rowcount()
	
	ls_valore = dw_parametri.getitemstring(li_i, "valore_stringa")
	
	if not isnull(ls_valore) and ls_valore <> ""  then
		choose case dw_parametri.getitemstring(li_i, "flag_tipo_parametro")
			case "S"
				luo_analisi.uof_set_param(dw_parametri.getitemstring(li_i, "cod_analisi_parametro"), ls_valore)
				
			case "D"
				luo_analisi.uof_set_param(dw_parametri.getitemstring(li_i, "cod_analisi_parametro"), date(ls_valore))
				
			case "N"
				luo_analisi.uof_set_param(dw_parametri.getitemstring(li_i, "cod_analisi_parametro"), integer(ls_valore))
				
		end choose
		
	end if
	
next
end if
// ---
 
 // eseguo analisi
 luo_analisi.uof_set_log(cbx_log.checked)
 li_result = luo_analisi.uof_execute()
 
 if li_result = 0 then
	g_mb.success("Analisi completata correttamente.")
 else
	g_mb.error(luo_analisi.uof_get_error())
end if
 
destroy luo_analisi
wf_start(false)
 
 return li_result
end function

public subroutine wf_start (boolean ab_status);st_wait.visible = ab_status
dw_parametri.visible = not ab_status
cb_esegui.enabled = not ab_status

if ab_status then
	pcca.mdi_frame.setmicrohelp("Elaboro analisi in corso...")
else
	pcca.mdi_frame.setmicrohelp("")
end if
end subroutine

on w_asa_analisi.create
int iCurrent
call super::create
this.st_wait=create st_wait
this.cbx_log=create cbx_log
this.dw_parametri=create dw_parametri
this.cb_esegui=create cb_esegui
this.dw_analisi=create dw_analisi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_wait
this.Control[iCurrent+2]=this.cbx_log
this.Control[iCurrent+3]=this.dw_parametri
this.Control[iCurrent+4]=this.cb_esegui
this.Control[iCurrent+5]=this.dw_analisi
end on

on w_asa_analisi.destroy
call super::destroy
destroy(this.st_wait)
destroy(this.cbx_log)
destroy(this.dw_parametri)
destroy(this.cb_esegui)
destroy(this.dw_analisi)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_analisi, &
                 "cod_analisi", &
                 sqlca, &
                 "tes_analisi", &
                 "cod_analisi", &
                 "des_analisi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;string ls_cod_analisi
s_cs_xx_parametri lstr_data
 
lstr_data = message.powerobjectparm

setnull(message.powerobjectparm)

dw_analisi.settransobject(sqlca)
dw_analisi.insertrow(0)

dw_parametri.settransobject(sqlca)

is_user_desktop = guo_functions.uof_get_user_desktop_folder()

if isvalid(lstr_data) and not isnull(lstr_data) then
	ls_cod_analisi = lstr_data.parametro_s_1
	
	if g_str.isnotempty(ls_cod_analisi) then
		dw_analisi.post setitem(1, "cod_analisi", ls_cod_analisi)
		dw_parametri.post retrieve(s_cs_xx.cod_azienda, ls_cod_analisi)					
	end if
end if
end event

type st_wait from statictext within w_asa_analisi
boolean visible = false
integer x = 23
integer y = 580
integer width = 2354
integer height = 80
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Elaborazione analisi in corso..."
alignment alignment = center!
boolean focusrectangle = false
end type

type cbx_log from checkbox within w_asa_analisi
integer x = 23
integer y = 1160
integer width = 297
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Log"
end type

type dw_parametri from uo_std_dw within w_asa_analisi
integer x = 23
integer y = 180
integer width = 2354
integer height = 960
integer taborder = 20
string dataobject = "d_asa_tes_analisi_parametri_lista"
end type

type cb_esegui from commandbutton within w_asa_analisi
integer x = 1966
integer y = 1160
integer width = 402
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;wf_esegui()
end event

type dw_analisi from uo_std_dw within w_asa_analisi
integer x = 23
integer y = 20
integer width = 1943
integer height = 160
integer taborder = 10
string dataobject = "d_asa_analisi_lista"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case dwo.name
	case "cod_analisi"
		if isnull(data) then
			dw_parametri.reset()
		else
			dw_parametri.retrieve(s_cs_xx.cod_azienda, data)			
		end if
end choose
end event

