﻿$PBExportHeader$w_asa_grid.srw
forward
global type w_asa_grid from w_cs_xx_treeview
end type
type dw_analisi from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_analisi_parametri from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_analisi_parametri dw_analisi_parametri
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_colonne from uo_dw_grid within det_3
end type
type dw_righe from datawindow within det_3
end type
type ddlb_zoom from dropdownlistbox within det_3
end type
type dw_grid from uo_dw_grid within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_colonne dw_colonne
dw_righe dw_righe
ddlb_zoom ddlb_zoom
dw_grid dw_grid
end type
end forward

global type w_asa_grid from w_cs_xx_treeview
integer width = 4187
integer height = 2028
string title = "Gestione Analisi"
event pc_menu_delete ( )
event pc_menu_taglia ( )
event pc_menu_copia ( )
event pc_menu_incolla ( )
event pc_menu_duplica_analisi ( )
event pc_menu_chiudi_colonna ( )
event pc_menu_add_column_1 ( )
event pc_menu_add_row_1 ( )
event pc_menu_visualizza_colonna ( )
event pc_menu_add_column_5 ( )
event pc_menu_add_column_10 ( )
event pc_menu_add_column_20 ( )
event pc_menu_add_row_5 ( )
event pc_menu_add_row_10 ( )
event pc_menu_add_row_20 ( )
event pc_menu_esegui_analisi ( )
event pc_menu_nuova_analisi ( )
end type
global w_asa_grid w_asa_grid

type variables
private:
	string is_desktop_dir
	
	int ICONA_ANALISI, ICONA_GRIGLIA, ICONA_PARAMETRI
	
	long il_livello, il_grid_row, il_grid_col
	long il_grid_row_copy, il_grid_col_copy
	boolean ib_grid_cut
	
	datastore ids_store
	
	// grid
	int ii_default_columns, ii_default_rows
	
	// codice analisi per copia
	string is_cod_analisi_copy
end variables

forward prototypes
public subroutine wf_treeview_icons ()
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_inserisci_analisi (long al_handle)
public subroutine wf_imposta_ricerca ()
public function long wf_inserisci_nodo_parametri (long al_handle, string as_cod_analisi)
public function long wf_inserisci_nodo_griglia (long al_handle, string as_cod_analisi)
public function long wf_inserisci_nodo_analisi (long al_handle, string as_cod_analisi, string as_des_analisi)
public subroutine wf_change_tab (integer ai_tab, boolean ab_trigger_new)
public subroutine wf_change_tab (string as_tipo_livello, boolean ab_trigger_new)
public subroutine wf_get_worksheet (string as_file_path)
public subroutine wf_set_grid_handle (long ai_row, string as_dwo_name)
public function integer wf_cancella_formula (long al_row, long al_col)
public subroutine wf_prepara_griglia ()
public subroutine wf_label_grid ()
public subroutine wf_imposta_zoom (integer ai_index)
public subroutine wf_cancella_formula ()
public function integer wf_incolla_formula (integer ai_num_riga_origine, integer ai_num_colonna_origine, integer ai_num_riga, integer ai_num_colonna)
public subroutine wf_button_status (boolean ab_status)
public subroutine wf_add_column (integer ai_new_column)
public subroutine wf_add_row (integer ai_new_row)
public function integer wf_cancella_formula (string as_cod_analisi, long al_row, long al_col)
end prototypes

event pc_menu_delete();/**
 * stefanop
 * 10/05/2012
 *
 * Cancello la formula
 **/

wf_cancella_formula()
end event

event pc_menu_taglia();/**
 * stefanop
 * 10/05/2012
 *
 * Taglio la formula
 **/
string ls_null

setnull(ls_null)
if il_grid_row < 1 or il_grid_col < 1 then return

ib_grid_cut = true
is_cod_analisi_copy = istr_data.codice
il_grid_row_copy = il_grid_row
il_grid_col_copy = il_grid_col


end event

event pc_menu_copia();/**
 * stefanop
 * 10/05/2012
 *
 * Taglio la formula
 **/
string ls_null

setnull(ls_null)
if il_grid_row < 1 or il_grid_col < 1 then return

ib_grid_cut = false
is_cod_analisi_copy = istr_data.codice
il_grid_row_copy = il_grid_row
il_grid_col_copy = il_grid_col


end event

event pc_menu_incolla();/**
 * stefanop
 * 10/05/2012
 *
 * Incolla la formula
 **/
string ls_null, ls_cod_formula
int li_rows_selected[], li_i

setnull(ls_null)
if il_grid_row < 1 or il_grid_col < 1 or il_grid_row_copy < 1 or il_grid_col_copy < 1 or g_str.isempty(is_cod_analisi_copy) then return

tab_dettaglio.det_3.dw_grid.uof_get_selected_row(li_rows_selected)

if upperbound(li_rows_selected) > 1 then
	for li_i = 1 to upperbound(li_rows_selected)
		
		wf_incolla_formula(il_grid_row_copy, il_grid_col_copy, li_rows_selected[li_i], il_grid_col)
		
	next
else
	wf_incolla_formula(il_grid_row_copy, il_grid_col_copy, il_grid_row, il_grid_col)
end if
end event

event pc_menu_duplica_analisi();/**
 * stefanop
 * 11/05/2012
 *
 * Duplico l'analisi e tutti i suoi parametri
 **/
 
string ls_cod_analisi_new, ls_cod_analisi_old
long ll_max_analisi, ll_handle
treeviewitem ltv_item
str_treeview lstr_data

ll_handle = tab_ricerca.selezione.tv_selezione.finditem(currenttreeitem!, 0)

if ll_handle < 0 then return
	
tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
lstr_data = ltv_item.data
ls_cod_analisi_old = lstr_data.codice

if not g_mb.confirm("Sicuro di voler duplicare l'analisi selezionata?") then return

select max(cod_analisi)
into :ll_max_analisi
from tes_analisi
where cod_azienda = :s_cs_xx.cod_azienda and
		 isnumeric(cod_analisi) = 1;
		 
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il calcolo del codice analisi." , sqlca)
	return
end if

if sqlca.sqlcode = 100 or isnull(ll_max_analisi) then ll_max_analisi = 0
ll_max_analisi ++

ls_cod_analisi_new = right("000" + string(ll_max_analisi), 3)


// 1. testa
insert into tes_analisi (
	cod_azienda,
	cod_analisi,
	des_analisi,
	path_file_excel,
	offset_riga,
	offset_colonna,
	numero_colonne,
	worksheet)
	select :s_cs_xx.cod_azienda,
		:ls_cod_analisi_new,
		des_analisi,
		path_file_excel,
		offset_riga,
		offset_colonna,
		numero_colonne,
		worksheet
	from tes_analisi
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :ls_cod_analisi_old;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la duplicazione della testata.", sqlca)
	rollback;
	return
end if

// 1.1 tes_analisi_parametri
insert into tes_analisi_parametri (
	cod_azienda,
	cod_analisi,
	cod_analisi_parametro,
	des_analisi_parametro,
	flag_tipo_parametro,
	ordinamento)
	select :s_cs_xx.cod_azienda,
		:ls_cod_analisi_new,
		cod_analisi_parametro,
		des_analisi_parametro,
		flag_tipo_parametro,
		ordinamento
	from tes_analisi_parametri
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :ls_cod_analisi_old;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la duplicazione della testata parametri.", sqlca)
	rollback;
	return
end if

// 2. testata formule
INSERT INTO tes_analisi_formule (
	cod_azienda,   
	cod_analisi,   
	num_riga,   
	num_colonna,   
	cod_formula)  
	SELECT :s_cs_xx.cod_azienda,
		:ls_cod_analisi_new,
		num_riga,
		num_colonna,
		cod_formula
	FROM tes_analisi_formule
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND
			   cod_analisi = :lstr_data.codice;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la duplicazione delle formule.", sqlca)
	rollback;
	return
end if

// 3. testata parametri
INSERT INTO tes_analisi_formule_parametri (
	cod_azienda,   
	cod_analisi,   
	num_riga,   
	num_colonna,   
	cod_formula,   
	cod_parametro,   
	valore_parametro)  
	SELECT :s_cs_xx.cod_azienda,
		:ls_cod_analisi_new,
		num_riga,   
		num_colonna,   
		cod_formula,   
		cod_parametro,   
		valore_parametro
	FROM tes_analisi_formule_parametri
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND
			   cod_analisi = :lstr_data.codice;
		
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la duplicazione dei parametri formule.", sqlca)
	rollback;
	return
end if

commit;
g_mb.success("Analisi duplicata correttamente!")
end event

event pc_menu_chiudi_colonna();/**
 * stefanop
 * 14/05/2012
 *
 * Collassa le colonne selezionate
 **/
 
int li_columns_selected[], li_i

tab_dettaglio.det_3.dw_grid.uof_get_selected_column(ref li_columns_selected)

tab_dettaglio.det_3.dw_grid.setredraw(false)

for li_i = 1 to upperbound(li_columns_selected)
	
	tab_dettaglio.det_3.dw_grid.uof_set_column_width(li_columns_selected[li_i], 0)
	tab_dettaglio.det_3.dw_colonne.uof_set_column_width(li_columns_selected[li_i], 0)

next

tab_dettaglio.det_3.dw_grid.setredraw(true)
end event

event pc_menu_add_column_1();wf_add_column(1)
end event

event pc_menu_add_row_1();wf_add_row(1)
end event

event pc_menu_visualizza_colonna();/**
 * stefanop
 * 28/05/2012
 *
 * Consente di riapire le colonne selezionate
 **/
 
int li_columns_selected[], li_i, li_width

li_width = tab_dettaglio.det_3.dw_grid.uof_get_default_column_width()
tab_dettaglio.det_3.dw_grid.uof_get_selected_column(ref li_columns_selected)

tab_dettaglio.det_3.dw_grid.setredraw(false)

for li_i = 1 to upperbound(li_columns_selected)
	
	tab_dettaglio.det_3.dw_grid.uof_set_column_width(li_columns_selected[li_i], li_width)
	tab_dettaglio.det_3.dw_colonne.uof_set_column_width(li_columns_selected[li_i], li_width)

next

tab_dettaglio.det_3.dw_grid.setredraw(true)
end event

event pc_menu_add_column_5();wf_add_column(5)
end event

event pc_menu_add_column_10();wf_add_column(10)
end event

event pc_menu_add_column_20();wf_add_column(20)
end event

event pc_menu_add_row_5();wf_add_row(5)
end event

event pc_menu_add_row_10();wf_add_row(10)
end event

event pc_menu_add_row_20();wf_add_row(20)
end event

event pc_menu_esegui_analisi();/**
 * stefanop
 * 15/07/2014
 *
 * Eseguo l'analisi selezionata
 **/
 
 
 s_cs_xx_parametri lstr_data
 
 
 lstr_data.parametro_s_1 = istr_data.codice
 
 window_open_parm(w_asa_analisi, 0, lstr_data)
end event

event pc_menu_nuova_analisi();/**
 * stefanop
 * 15/07/2014
 *
 * Creo una nuova analisi
 **/
 
tab_dettaglio.det_1.dw_analisi.reset()
wf_change_tab(1, true)
end event

public subroutine wf_treeview_icons ();ICONA_ANALISI = wf_treeview_add_icon("treeview\documento_blu.png")
ICONA_GRIGLIA = wf_treeview_add_icon("treeview\table.png")
ICONA_PARAMETRI = wf_treeview_add_icon("treeview\cog.png")
//ICONA_SALTO_RIGA = wf_treeview_add_icon("treeview\tag_purple.png")
//
end subroutine

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "A"
		return wf_inserisci_analisi(al_handle)
		
	case "V"
		//return wf_inserisci_analisi_voci(al_handle)
				
	case else
		return 1 //wf_inserisci_ordini_acquisti(al_handle)
		
end choose
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "A"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_analisi.cod_analisi is null "
			else
				as_sql += " AND tes_analisi.cod_analisi = '" + lstr_data.codice + "' "
			end if
			
						
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function long wf_inserisci_analisi (long al_handle);string ls_sql, ls_label, ls_error, ls_cod_analisi, ls_des_analisi
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tes_analisi.cod_analisi, tes_analisi.des_analisi FROM tes_analisi " + &
			"WHERE tes_analisi.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_analisi = ids_store.getitemstring(ll_i, 1)
	 ls_des_analisi = ids_store.getitemstring(ll_i, 2)
	
	wf_inserisci_nodo_analisi(al_handle, ls_cod_analisi, ls_des_analisi)
next

return ll_rows
end function

public subroutine wf_imposta_ricerca ();string ls_cod_analisi, ls_des_analisi

tab_ricerca.ricerca.dw_ricerca.accepttext()

ls_cod_analisi = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_analisi")
ls_des_analisi = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "des_analisi")

is_sql_filtro = ""

if not isnull(ls_cod_analisi) and ls_cod_analisi <> "" then
	is_sql_filtro += " AND tes_analisi.cod_analisi='" + ls_cod_analisi + "' "
end if


if not isnull(ls_des_analisi) and ls_des_analisi <> "" then
	is_sql_filtro += " AND tes_analisi.des_analisi LIKE '%" + ls_des_analisi + "%' "
end if

end subroutine

public function long wf_inserisci_nodo_parametri (long al_handle, string as_cod_analisi);treeviewitem ltv_item
str_treeview lstr_data

ltv_item = wf_new_item(false, ICONA_PARAMETRI)

lstr_data.tipo_livello = "P"
lstr_data.livello = il_livello
lstr_data.codice = as_cod_analisi

ltv_item.label = "Parametri Analisi"
ltv_item.data = lstr_data

return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltv_item)
end function

public function long wf_inserisci_nodo_griglia (long al_handle, string as_cod_analisi);treeviewitem ltv_item
str_treeview lstr_data

ltv_item = wf_new_item(false, ICONA_GRIGLIA)

lstr_data.tipo_livello = "G"
lstr_data.livello = il_livello
lstr_data.codice = as_cod_analisi

ltv_item.label = "Griglia"
ltv_item.data = lstr_data

return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltv_item)
end function

public function long wf_inserisci_nodo_analisi (long al_handle, string as_cod_analisi, string as_des_analisi);string ls_label
long ll_handle_analisi
treeviewitem ltv_item
str_treeview lstr_data

ltv_item = wf_new_item(false, ICONA_ANALISI)

lstr_data.tipo_livello = "A"
lstr_data.livello = il_livello
lstr_data.codice = as_cod_analisi

ls_label = as_cod_analisi

if not isnull(as_des_analisi) and as_des_analisi <> "" then
	ls_label += " - " + as_des_analisi
end if

ltv_item.label = ls_label
ltv_item.data = lstr_data

ll_handle_analisi = tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltv_item)

wf_inserisci_nodo_parametri(ll_handle_analisi, as_cod_analisi)
wf_inserisci_nodo_griglia(ll_handle_analisi, as_cod_analisi)

return ll_handle_analisi
end function

public subroutine wf_change_tab (integer ai_tab, boolean ab_trigger_new);/**
 * stefanop
 * 27/04/2012
 *
 * Cambio la datawindow ed imposto i parametri necessari.
 * Se ab_trigger_new è true allora scateno l'evento PC_NEW della window
 **/
 
int li_i
 
choose case ai_tab
	case 1
		iuo_dw_main = tab_dettaglio.det_1.dw_analisi
		
	case 2
		iuo_dw_main = tab_dettaglio.det_2.dw_analisi_parametri
		
	case 3
		setnull(iuo_dw_main)
		wf_prepara_griglia()
		tab_dettaglio.det_3.dw_grid.postevent("pcd_retrieve")
		//tab_dettaglio.det_3.dw_analisi_voci_formule.event pcd_retrieve()
		//iuo_dw_main = tab_dettaglio.det_3.dw_formule_param
		
			
end choose

// seleziono il tab
tab_dettaglio.selecttab(ai_tab)

// nascondo gli altri tab
for li_i = 1 to upperbound(tab_dettaglio.control)
	
	tab_dettaglio.control[li_i].visible = (li_i = ai_tab)
	
next

// seleziono DW
if isvalid(iuo_dw_main) and not isnull(iuo_dw_main) then
	
	iuo_dw_main.change_dw_current()
	
	if ab_trigger_new then
		
		getwindow().triggerevent("pc_new")
		
	end if
	
end if
end subroutine

public subroutine wf_change_tab (string as_tipo_livello, boolean ab_trigger_new);int li_tab

choose case as_tipo_livello
	case "A"
		li_tab = 1
		
	case "P"
		li_tab = 2
		
	case "G"
		li_tab = 3
		
end choose

wf_change_tab(li_tab, false)
end subroutine

public subroutine wf_get_worksheet (string as_file_path);string ls_worksheet[], ls_values
int li_i
uo_excel luo_excel

if isnull(as_file_path) or as_file_path = "" then return
if not fileexists(as_file_path) then return

luo_excel = create uo_excel
luo_excel.uof_open(as_file_path, false, true)
luo_excel.uof_get_sheets(ref ls_worksheet[])
destroy luo_excel

ls_values = ""
for li_i = 1 to upperbound(ls_worksheet)
	
	ls_values += ls_worksheet[li_i] + "~t" +  ls_worksheet[li_i] + "/"
	
next

tab_dettaglio.det_1.dw_analisi.modify("worksheet.Values='" + ls_values + "'")

end subroutine

public subroutine wf_set_grid_handle (long ai_row, string as_dwo_name);long ll_col, ll_row

if ai_row >= 1 then
	il_grid_row = ai_row
	
	ll_col = tab_dettaglio.det_3.dw_grid.uof_get_col_number(string(as_dwo_name))
	
	if ll_col < 0 then
		il_grid_col = -1
		return
	end if
	
	il_grid_col = ll_col
end if
end subroutine

public function integer wf_cancella_formula (long al_row, long al_col);/**
 * stefanop
 * 11/05/2012
 *
 * Cancello la formula nella cella selezionata e anche i relativi parametri
 **/
 
return wf_cancella_formula(istr_data.codice, al_row, al_col)
end function

public subroutine wf_prepara_griglia ();long ll_num_riga, ll_num_colonna

// Recupero limiti della griglia
select max(num_riga)
into :ll_num_riga
from tes_analisi_formule
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :istr_data.codice;
		 
if sqlca.sqlcode <> 0 or isnull(ll_num_riga) then ll_num_riga = ii_default_rows
		 
select max(num_colonna)
into :ll_num_colonna
from tes_analisi_formule
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :istr_data.codice;
		 
if sqlca.sqlcode <> 0 or isnull(ll_num_colonna) then ll_num_colonna = ii_default_columns
// ----

ll_num_riga += 5
ll_num_colonna += 5

tab_dettaglio.det_3.dw_colonne.uof_init_only_header(ll_num_colonna)
tab_dettaglio.det_3.dw_grid.uof_init(uo_dw_grid.MODE_VIEW_ONLY ,ll_num_riga, ll_num_colonna, false, false)
wf_label_grid()
end subroutine

public subroutine wf_label_grid ();string ls_column
int li_i, li_j, li_k

tab_dettaglio.det_3.dw_righe.reset()
tab_dettaglio.det_3.dw_righe.insertrow(0)

for li_i = 1 to tab_dettaglio.det_3.dw_grid.rowcount()	
	tab_dettaglio.det_3.dw_righe.insertrow(0)
	tab_dettaglio.det_3.dw_righe.setitem(li_i + 1, 1, li_i)
next

li_k = 0

for li_i = 1 to tab_dettaglio.det_3.dw_grid.uof_get_columncount()
	
	ls_column = ""
	
	if li_i > 26 then
		li_k = int(li_i / 26)
		
		ls_column += string(char(64 + li_k))
	end if
	
	li_j = li_i - (26 * li_k)
	ls_column += string(char(64 + li_j))
						
	
	tab_dettaglio.det_3.dw_colonne.setitem(0, li_i, ls_column)
	//tab_dettaglio.det_3.dw_grid.setitem(0, li_i, ls_column)
	
next
end subroutine

public subroutine wf_imposta_zoom (integer ai_index);if ai_index = 0 then
	
	// se passo un valore negativo allora mi devo recuperare l'indice direttamente dalla dropdwon
	ai_index = tab_dettaglio.det_3.ddlb_zoom.finditem(tab_dettaglio.det_3.ddlb_zoom.text, 0)
	
end if

tab_dettaglio.det_3.dw_grid.Modify("DataWindow.Zoom=" + string(ai_index * 10))
tab_dettaglio.det_3.dw_colonne.Modify("DataWindow.Zoom=" + string(ai_index * 10))
tab_dettaglio.det_3.dw_righe.Modify("DataWindow.Zoom=" + string(ai_index * 10))
end subroutine

public subroutine wf_cancella_formula ();string ls_null, ls_cod_formula

setnull(ls_null)
if il_grid_row < 1 or il_grid_col < 1 then return

ls_cod_formula = tab_dettaglio.det_3.dw_grid.getitemstring(il_grid_row, il_grid_col)
if isnull(ls_cod_formula) or ls_cod_formula = "" then return 

if not g_mb.confirm("Sicuri di voler cancellare la formula " + ls_cod_formula + " della cella : " +string(il_grid_row) + ":" + string(il_grid_col) + " ?") then return

if wf_cancella_formula(il_grid_row, il_grid_col) = 0 then
	commit;
	tab_dettaglio.det_3.dw_grid.setitem(il_grid_row, il_grid_col, ls_null)
else
	rollback;
end if
end subroutine

public function integer wf_incolla_formula (integer ai_num_riga_origine, integer ai_num_colonna_origine, integer ai_num_riga, integer ai_num_colonna);/**
 * stefanop
 * 10/05/2012
 *
 * Incolla la formula
 **/
string ls_null, ls_cod_formula, ls_cod_formula_test

setnull(ls_null)
if ai_num_riga_origine < 1 or ai_num_colonna_origine < 1 or ai_num_riga < 1 or ai_num_colonna < 1 then return 0

// stefanop: 15/07/2014: copio la formula tra analisi
//ls_cod_formula = tab_dettaglio.det_3.dw_grid.getitemstring(ai_num_riga_origine, ai_num_colonna_origine)
//
//if isnull(ls_cod_formula) then
//	g_mb.warning("Nessun formula da copiare!")
//	return 0
//end if

select cod_formula
into :ls_cod_formula
from tes_analisi_formule
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :is_cod_analisi_copy and
		 num_riga = :ai_num_riga_origine and
		 num_colonna = :ai_num_colonna_origine;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la copia della formula", sqlca)
	return -1
elseif sqlca.sqlcode = 100 or g_str.isempty(ls_cod_formula) then
	g_mb.warning("Nessun formula da copiare!")
	return 0
end if

// Controllo che non ci sia già un formula
select cod_formula
into :ls_cod_formula_test
from tes_analisi_formule
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :istr_data.codice and
		 num_riga = :ai_num_riga and
		 num_colonna = :ai_num_colonna;

if sqlca.sqlcode = 0 and not isnull(ls_cod_formula_test) and ls_cod_formula_test <> "" then
	
	if not g_mb.confirm("Attenzione, è già presente la formula " + ls_cod_formula_test + " nella riga: " +string(ai_num_riga) + ", colonna: " + string(ai_num_colonna) + "~r~nSostituisco?") then
		return 0
	end if
end if
// -----

// TESTATA
insert into tes_analisi_formule ( cod_azienda, 
	cod_analisi,
	num_riga,
	num_colonna,
	cod_formula,
	flag_negativo
) select :s_cs_xx.cod_azienda,
		:istr_data.codice,
		:ai_num_riga,
		:ai_num_colonna,
		cod_formula,
		flag_negativo
	from tes_analisi_formule
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :is_cod_analisi_copy and
			 num_riga = :ai_num_riga_origine and
			 num_colonna = :ai_num_colonna_origine;
			 
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la copia della formula nella nuova cella.", sqlca)
	rollback;
	return -1
end if

// TESTATA
insert into tes_analisi_formule_parametri ( cod_azienda, 
	cod_analisi,
	num_riga,
	num_colonna,
	cod_formula,
	cod_parametro,
	valore_parametro
) select :s_cs_xx.cod_azienda,
		:istr_data.codice,
		:ai_num_riga,
		:ai_num_colonna,
		cod_formula,
		cod_parametro,
		valore_parametro
	from tes_analisi_formule_parametri
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :is_cod_analisi_copy and
			 num_riga = :ai_num_riga_origine and
			 num_colonna = :ai_num_colonna_origine;
			 
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la copia dei valori della formula nella nuova cella.", sqlca)
	rollback;
	return -1
end if

if ib_grid_cut then
	// Ho fatto taglia, quindi cancello la cella di partenza
	if wf_cancella_formula(is_cod_analisi_copy, ai_num_riga_origine, ai_num_colonna_origine) < 0 then
		rollback;
		return -1
	end if
	
	if is_cod_analisi_copy = istr_data.codice then
		// Sono nella stessa analisi quindi aggiorno la griglia
		tab_dettaglio.det_3.dw_grid.setitem(ai_num_riga_origine, ai_num_colonna_origine, ls_null)
	end if
end if

commit;
tab_dettaglio.det_3.dw_grid.setitem(ai_num_riga, ai_num_colonna, ls_cod_formula)

ib_grid_cut = false
end function

public subroutine wf_button_status (boolean ab_status);tab_dettaglio.det_1.dw_analisi.object.b_ricerca_foglio_excel.enabled = ab_status
tab_dettaglio.det_1.dw_analisi.object.b_fogli.enabled = ab_status
end subroutine

public subroutine wf_add_column (integer ai_new_column);string ls_cod_analisi
int li_column

ls_cod_analisi = istr_data.codice
if isnull(ls_cod_analisi) or ls_cod_analisi ="" or il_grid_col < 1 then return

// Aggiorno colonne
update tes_analisi_formule
set num_colonna = num_colonna + :ai_new_column
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_analisi = :ls_cod_analisi and
	num_colonna >= :il_grid_col;

if sqlca.sqlcode < 0 then 
	g_mb.error("Erorre durante lo spostamento delle formule nelle nuove posizioni", sqlca)
	rollback;
	return
end if

update tes_analisi_formule_parametri
set num_colonna = num_colonna + :ai_new_column
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_analisi = :ls_cod_analisi and
	num_colonna >= :il_grid_col;
	
if sqlca.sqlcode < 0 then 
	g_mb.error("Erorre durante lo spostamento delle formule nelle nuove posizioni", sqlca)
	rollback;
	return
end if

commit;

tab_dettaglio.det_3.dw_grid.uof_add_columns(ai_new_column, true)
tab_dettaglio.det_3.dw_colonne.uof_add_columns(ai_new_column, true)
wf_label_grid()
wf_imposta_zoom(0)
tab_dettaglio.det_3.dw_grid.event pcd_retrieve()
end subroutine

public subroutine wf_add_row (integer ai_new_row);string ls_cod_analisi
long ll_row

ls_cod_analisi =  istr_data.codice
ll_row = tab_dettaglio.det_3.dw_grid.getrow() - 1

if isnull(ls_cod_analisi) or ls_cod_analisi = "" or ll_row < 1 then return

// Sposto eventuali formule 
update tes_analisi_formule
set num_riga = num_riga + :ai_new_row
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_analisi = :ls_cod_analisi and
	num_riga >= :ll_row;
	
if sqlca.sqlcode < 0 then 
	g_mb.error("Erorre durante lo spostamento delle formule nelle nuove posizioni", sqlca)
	rollback;
	return
end if
	
// Sposto eventuali formule parametri
update tes_analisi_formule_parametri
set num_riga = num_riga + :ai_new_row
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_analisi = :ls_cod_analisi and
	num_riga >= :ll_row;
	
if sqlca.sqlcode < 0 then 
	g_mb.error("Erorre durante lo spostamento dei parametri delle formule nelle nuove posizioni", sqlca)
	rollback;
	return
end if

commit;

tab_dettaglio.det_3.dw_grid.uof_add_rows(ai_new_row, true)
wf_label_grid()
tab_dettaglio.det_3.dw_grid.event pcd_retrieve()
wf_imposta_zoom(0)

tab_dettaglio.det_3.dw_grid.scrolltorow(ll_row)
end subroutine

public function integer wf_cancella_formula (string as_cod_analisi, long al_row, long al_col);/**
 * stefanop
 * 11/05/2012
 *
 * Cancello la formula nella cella selezionata e anche i relativi parametri
 **/
 
if  al_row < 1 or  al_col < 1 then return 1

delete from tes_analisi_formule_parametri
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :as_cod_analisi and
		 num_riga = :al_row and
		 num_colonna = :al_col;
		 
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durane la cancellazione dei parametri.", sqlca)
	return -1
end if

delete from tes_analisi_formule
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_analisi = :as_cod_analisi and
		 num_riga = :al_row and
		 num_colonna = :al_col;
		 
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durane la cancellazione della formula.", sqlca)
	return -1
end if

return 0
end function

on w_asa_grid.create
int iCurrent
call super::create
end on

on w_asa_grid.destroy
call super::destroy
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_analisi", &
                 sqlca, &
                 "tes_analisi", &
                 "cod_analisi", &
                 "des_analisi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;tab_dettaglio.det_1.dw_analisi.set_dw_options(sqlca, pcca.null_object,  c_default, c_default)
tab_dettaglio.det_2.dw_analisi_parametri.set_dw_options(sqlca, pcca.null_object,  c_default, c_default)

// Sposto oggetti in posizioni iniziali
tab_dettaglio.det_1.dw_analisi.move(0,0)
tab_dettaglio.det_2.dw_analisi_parametri.move(0,0)
tab_dettaglio.det_3.dw_grid.move(0,0)

// Parametri default della griglia
ii_default_columns = 20
ii_default_rows = 20

// User friendly
is_desktop_dir = guo_functions.uof_get_user_desktop_folder()

tab_dettaglio.det_3.dw_grid.uof_set_default_text_align(2)
tab_dettaglio.det_3.ddlb_zoom.selectitem(10)
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_asa_grid
integer width = 2720
det_2 det_2
det_3 det_3
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
end on

event tab_dettaglio::ue_resize;call super::ue_resize;
det_1.dw_analisi.resize(det_1.width, det_1.height)
det_2.dw_analisi_parametri.resize(det_2.width, det_2.height)

det_3.ddlb_zoom.move(det_3.width - det_3.ddlb_zoom.width - 30, 20)
//det_3.dw_grid.move(0, 152)
//det_3.dw_grid.resize(det_3.width, det_3.height - det_3.dw_grid.y)

det_3.dw_righe.move(0, 152)
det_3.dw_righe.height = det_3.height - det_3.dw_righe.y

det_3.dw_colonne.move(det_3.dw_righe.width, 152)
det_3.dw_colonne.width = det_3.width - det_3.dw_righe.width

det_3.dw_grid.move(det_3.dw_righe.width, det_3.dw_colonne.y + det_3.dw_colonne.height)
det_3.dw_grid.resize(det_3.width - det_3.dw_righe.width, det_3.height - det_3.dw_grid.y)
end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 2683
string text = "Analisi"
dw_analisi dw_analisi
end type

on det_1.create
this.dw_analisi=create dw_analisi
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_analisi
end on

on det_1.destroy
call super::destroy
destroy(this.dw_analisi)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_asa_grid
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
string dataobject = "d_asa_grid_ricerca"
end type

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
event pc_menu_esegui_analisi ( )
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if
end event

event tv_selezione::rightclicked;call super::rightclicked;treeviewitem ltvi_item
m_asa_grid_treeview lm_menu

lm_menu = create m_asa_grid_treeview

if AncestorReturnValue < 0 then 
	lm_menu.m_nuova_analisi.visible = true
	lm_menu.m_duplica_analisi.visible = false
	lm_menu.m_esegui.visible = false
else
	lm_menu.m_nuova_analisi.visible = false

	tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)
	istr_data = ltvi_item.data
	
	lm_menu.m_duplica_analisi.visible = (istr_data.tipo_livello = 'A')
	lm_menu.m_esegui.visible = lm_menu.m_duplica_analisi.visible
end if

	
lm_menu.popmenu(w_cs_xx_mdi.pointerx(), w_cs_xx_mdi.pointery())

destroy lm_menu
end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data

wf_change_tab(istr_data.tipo_livello, false)

getwindow().postevent("pc_retrieve")
end event

type dw_analisi from uo_cs_xx_dw within det_1
integer x = 5
integer y = 12
integer width = 2560
integer height = 640
integer taborder = 30
string dataobject = "d_asa_grid_analisi"
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_file_excel

if not isvalid(istr_data) or isnull(istr_data) then return 0

if retrieve(s_cs_xx.cod_azienda, istr_data.codice) < 0 then
   pcca.error = c_fatal
else	
	ls_file_excel = getitemstring(getrow(), "path_file_excel")
	wf_get_worksheet(ls_file_excel)
end if
end event

event buttonclicked;call super::buttonclicked;string ls_file_excel, ls_files[]

choose case dwo.name
	
	// -- DW_TES_ANALISI --
	case "b_ricerca_foglio_excel"
		
		if GetFileOpenName("Seleziona il file Excel", ls_file_excel, ls_files[], "Excel", "Excel, *.xls;*.xlsx", is_desktop_dir) = 1 then
			
			setitem(row, "path_file_excel", ls_file_excel)
			//wf_get_worksheet(ls_file_excel)
			
		end if
	// ------------------------
	
case "b_fogli"
	ls_file_excel = getitemstring(row, "path_file_excel")
	
	if not isnull(ls_file_excel) and ls_file_excel <> "" then
		wf_get_worksheet(ls_file_excel)
	end if
	
end choose
end event

event pcd_new;call super::pcd_new;wf_button_status(true)
end event

event pcd_modify;call super::pcd_modify;wf_button_status(true)
end event

event pcd_view;call super::pcd_view;wf_button_status(false)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
next
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_i

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_analisi")) then
		g_mb.warning("Impostare un codice analisi valido")
		pcca.error = c_fatal
		return -1
	end if
	
	if isnull(this.getitemstring(ll_i, "worksheet")) then
		g_mb.warning("Impostare un foglio di lavoro per il file excel")
		pcca.error = c_fatal
		return -1
	end if
	
next
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2683
integer height = 1676
long backcolor = 12632256
string text = "Parametri"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_analisi_parametri dw_analisi_parametri
end type

on det_2.create
this.dw_analisi_parametri=create dw_analisi_parametri
this.Control[]={this.dw_analisi_parametri}
end on

on det_2.destroy
destroy(this.dw_analisi_parametri)
end on

type dw_analisi_parametri from uo_cs_xx_dw within det_2
integer x = 5
integer y = 12
integer width = 2651
integer height = 960
integer taborder = 11
string dataobject = "d_asa_gird_analisi_parametri"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;int li_i

for li_i = 1 to rowcount()

	if isnull(getitemstring(li_i, "cod_azienda")) then
		setitem(li_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemstring(li_i, "cod_analisi")) then
		setitem(li_i, "cod_analisi", istr_data.codice)
	end if

next
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_file_excel

if not isvalid(istr_data) or isnull(istr_data) then return 0

if retrieve(s_cs_xx.cod_azienda, istr_data.codice) < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_validate;call super::pcd_validate;int li_i

for li_i = 1 to rowcount()
	
	if isnull(getitemstring(li_i, "cod_analisi_parametro")) then
		g_mb.warning("Il Codice Parametro è un campo obbligatorio.")
		return -1
	end if
	
next
end event

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2683
integer height = 1676
long backcolor = 12632256
string text = "Griglia"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_colonne dw_colonne
dw_righe dw_righe
ddlb_zoom ddlb_zoom
dw_grid dw_grid
end type

on det_3.create
this.dw_colonne=create dw_colonne
this.dw_righe=create dw_righe
this.ddlb_zoom=create ddlb_zoom
this.dw_grid=create dw_grid
this.Control[]={this.dw_colonne,&
this.dw_righe,&
this.ddlb_zoom,&
this.dw_grid}
end on

on det_3.destroy
destroy(this.dw_colonne)
destroy(this.dw_righe)
destroy(this.ddlb_zoom)
destroy(this.dw_grid)
end on

type dw_colonne from uo_dw_grid within det_3
integer x = 279
integer y = 152
integer width = 2377
integer height = 80
integer taborder = 40
boolean hscrollbar = false
boolean vscrollbar = false
boolean border = false
boolean livescroll = false
end type

event ue_column_resize_end;call super::ue_column_resize_end;uof_sync_column_width(dw_grid)

end event

type dw_righe from datawindow within det_3
integer y = 152
integer width = 206
integer height = 1500
integer taborder = 40
string title = "none"
string dataobject = "d_asa_grid_analisi_righe"
boolean border = false
end type

type ddlb_zoom from dropdownlistbox within det_3
integer x = 2016
integer y = 32
integer width = 640
integer height = 360
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean sorted = false
boolean hscrollbar = true
boolean vscrollbar = true
integer limit = 10
string item[] = {"10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;wf_imposta_zoom(index)
end event

type dw_grid from uo_dw_grid within det_3
event pcd_retrieve ( )
event ue_key pbm_dwnkey
integer x = 279
integer y = 292
integer width = 2400
integer height = 1380
integer taborder = 30
boolean border = false
end type

event pcd_retrieve();string ls_sql, ls_cod_formula
long ll_row, ll_i, ll_num_riga, ll_num_colonna

ls_sql = "SELECT num_riga, num_colonna, cod_formula FROM tes_analisi_formule " + &
		   "WHERE cod_azienda ='" + s_cs_xx.cod_azienda +"' AND " + &
		   "cod_analisi='" + istr_data.codice + "'"
			
ll_row = guo_functions.uof_crea_datastore(ids_store, ls_sql)

for ll_i = 1 to ll_row
	
	ll_num_riga = ids_store.getitemnumber(ll_i, 1)
	ll_num_colonna = ids_store.getitemnumber(ll_i, 2)
	ls_cod_formula = ids_store.getitemstring(ll_i, 3)
	
	setitem(ll_num_riga, ll_num_colonna, ls_cod_formula)
next

destroy ids_store
end event

event ue_key;
if key = keydelete! then
	
	wf_cancella_formula()
	
end if
end event

event rbuttondown;call super::rbuttondown;string ls_cod_formula
m_asa_grid lm_menu

wf_set_grid_handle(row, string(dwo.name))

lm_menu = create m_asa_grid

// devo far vedere il menu solo se ho un handle di una cella
if il_grid_row < 1 or il_grid_col < 1 then return 0

ls_cod_formula = getitemstring(il_grid_row, il_grid_col)
if isnull(ls_cod_formula) or ls_cod_formula = "" then
	lm_menu.m_copia.enabled = false
	lm_menu.m_taglia.enabled = false
	lm_menu.m_elimina.enabled = false
end if

// Abilito l'incolla solo se ho fatto copia o incolla
if  il_grid_row_copy < 1 or il_grid_col_copy < 1 then
	lm_menu.m_incolla.enabled = false
end if

lm_menu.popmenu(w_cs_xx_mdi.pointerx(), w_cs_xx_mdi.pointery())

destroy lm_menu
end event

event doubleclicked;call super::doubleclicked;string ls_cod_formula
long ll_col, ll_row
str_asa_grid lstr_data

wf_set_grid_handle(row, string(dwo.name))

if il_grid_row > 0 and il_grid_col > 0 then
	ll_row = row
	
	ll_col = uof_get_col_number(string(dwo.name))
	
	if ll_col < 0 then
		g_mb.error("Errore durante il recupero del numero di colonna")
		return 0
//	else
//		ll_col --
	end if
	
	ls_cod_formula = getitemstring(ll_row, ll_col)
	
	 il_grid_row = ll_row
	 il_grid_col = ll_col
	
	lstr_data.num_riga = ll_row
	lstr_data.num_colonna = ll_col
	lstr_data.cod_formula = ls_cod_formula
	lstr_data.cod_analisi = istr_data.codice
	
	//messagebox(getitemstring(row, ll_col), "riga: " + string(row) + " - colonna: " +string(ll_col) )
	window_open_parm(w_asa_grid_formula, 0, lstr_data)
	
	lstr_data = message.powerobjectparm
	
	if isvalid(lstr_data) and not isnull(lstr_data) then
		
		setitem(ll_row, ll_col, lstr_data.cod_formula)
		
	end if
end if
end event

event ue_left_mouse_clicked;call super::ue_left_mouse_clicked;wf_set_grid_handle(row, string(dwo.name))
end event

event scrollvertical;call super::scrollvertical;dw_righe.Object.datawindow.verticalscrollposition = scrollpos
end event

event scrollhorizontal;call super::scrollhorizontal;dw_colonne.Object.datawindow.horizontalscrollposition = scrollpos
end event

