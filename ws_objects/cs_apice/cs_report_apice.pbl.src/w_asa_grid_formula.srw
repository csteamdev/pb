﻿$PBExportHeader$w_asa_grid_formula.srw
forward
global type w_asa_grid_formula from w_cs_xx_risposta
end type
type cbx_negativo from checkbox within w_asa_grid_formula
end type
type cb_test from commandbutton within w_asa_grid_formula
end type
type dw_formule_parametri from uo_std_dw within w_asa_grid_formula
end type
type cb_cancel from commandbutton within w_asa_grid_formula
end type
type cb_ok from commandbutton within w_asa_grid_formula
end type
type dw_lista from uo_std_dw within w_asa_grid_formula
end type
end forward

global type w_asa_grid_formula from w_cs_xx_risposta
integer width = 2638
integer height = 1420
string title = ""
boolean resizable = false
event we_load_ddddlb ( )
cbx_negativo cbx_negativo
cb_test cb_test
dw_formule_parametri dw_formule_parametri
cb_cancel cb_cancel
cb_ok cb_ok
dw_lista dw_lista
end type
global w_asa_grid_formula w_asa_grid_formula

type variables
private:
	str_asa_grid istr_data
	string is_global_param[]
end variables

forward prototypes
public function integer wf_save ()
public function integer wf_load_param (string as_cod_formula)
public function integer wf_load_param_formula (string as_cod_formula)
public function integer wf_load_global_param (string as_cod_analisi)
end prototypes

event we_load_ddddlb();f_po_loaddddw_dw(dw_lista, &
                 "cod_formula", &
                 sqlca, &
                 "tab_formule_calcolo", &
                 "cod_formula", &
                 "des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
f_po_loaddddw_dw(dw_formule_parametri, &
                 "valore_parametro", &
                 sqlca, &
                 "tes_analisi_parametri", &
                 "cod_analisi_parametro", &
                 "des_analisi_parametro", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_analisi='" + istr_data.cod_analisi  + "'")
end event

public function integer wf_save ();string ls_cod_formula, ls_cod_parametro, ls_valore_parametro, ls_flag_negativo
int li_i

dw_lista.accepttext()
dw_formule_parametri.accepttext()
ls_cod_formula = dw_lista.getitemstring(1, "cod_formula")
if cbx_negativo.checked then
	ls_flag_negativo = "S"
else
	ls_flag_negativo = "N"
end if

if isnull(ls_cod_formula) or ls_cod_formula = "" then
	g_mb.warning("Inserire una formula prima di salvare!")
	return -1
end if

// La formula è cambiata, vanno cancellati i relativi parametri
if not isnull(istr_data.cod_formula) and istr_data.cod_formula <> "" then
	delete from tes_analisi_formule_parametri
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :istr_data.cod_analisi and
			 num_riga = :istr_data.num_riga and
			 num_colonna = :istr_data.num_colonna and
			 cod_formula = :istr_data.cod_formula;
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione dei vecchi parametri.", sqlca)
		return -1
	end if
end if
// ---


// Se cambia la formula devo pulire la testata
if ls_cod_formula <> istr_data.cod_formula then
	delete from tes_analisi_formule
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :istr_data.cod_analisi and
			 num_riga = :istr_data.num_riga and
			 num_colonna = :istr_data.num_colonna and
			 cod_formula = :istr_data.cod_formula;
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione della vecchia formula.", sqlca)
		return -1
	end if
end if

// Se è una nuova formula oppure è cambiata la devo inserire
if isnull(istr_data.cod_formula) or istr_data.cod_formula = "" or ls_cod_formula <> istr_data.cod_formula then

	insert into tes_analisi_formule (cod_azienda,
		cod_analisi,
		num_riga,
		num_colonna,
		cod_formula,
		flag_negativo
	) values (
		:s_cs_xx.cod_azienda,
		:istr_data.cod_analisi,
		:istr_data.num_riga,
		:istr_data.num_colonna,
		:ls_cod_formula,
		:ls_flag_negativo);
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il salvataggio della formula", sqlca)
		return -1
	end if
else
	
	update tes_analisi_formule
	set flag_negativo = :ls_flag_negativo
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :istr_data.cod_analisi and
			 num_riga = :istr_data.num_riga and
			 num_colonna = :istr_data.num_colonna and
			 cod_formula = :istr_data.cod_formula;
end if
// ----

// Aggiorno i parametri, SEMPRE
for li_i = 1 to dw_formule_parametri.rowcount()
	
	ls_cod_parametro = dw_formule_parametri.getitemstring(li_i, "cod_parametro")
	ls_valore_parametro = dw_formule_parametri.getitemstring(li_i, "valore_parametro")
	
	// non salvo i parametri/valori vuoti
	if isnull(ls_cod_parametro) or ls_cod_parametro = ""  or isnull(ls_valore_parametro) or ls_valore_parametro = "" then continue
	
	// stefanop 28/05/2012: controllo che il valore non abbia lo stesso valore del parametro
	if ls_cod_parametro = ls_valore_parametro then
		g_mb.warning("Non è possibile assegnare come valore il nome del parametro.~r~nControllare il parametro " +  ls_cod_parametro)
		return -1
	end if
	// -----
	
	insert into tes_analisi_formule_parametri (cod_azienda,
		cod_analisi,
		num_riga,
		num_colonna,
		cod_formula,
		cod_parametro,
		valore_parametro
	) values (
		:s_cs_xx.cod_azienda,
		:istr_data.cod_analisi,
		:istr_data.num_riga,
		:istr_data.num_colonna,
		:ls_cod_formula,
		:ls_cod_parametro,
		:ls_valore_parametro);
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante l'inserimento del parametro " + ls_cod_parametro, sqlca)
		return -1
	end if
	
next

istr_data.cod_formula = ls_cod_formula
return 0
end function

public function integer wf_load_param (string as_cod_formula);string ls_sql, ls_cod_parametro, ls_valore_parametro
int li_i, li_rows, li_row
datastore lds_store

// prima carico i parametri della formula
wf_load_param_formula(as_cod_formula)

ls_sql = "SELECT cod_parametro, valore_parametro FROM tes_analisi_formule_parametri " + &
			"WHERE cod_azienda='" + s_cs_xx.cod_azienda  + "' AND " + &
			"cod_analisi='" + istr_data.cod_analisi + "' AND " + &
			"num_riga=" + string(istr_data.num_riga) + " AND " + &
			"num_colonna=" + string(istr_data.num_colonna) + " AND " + &
			"cod_formula='" + as_cod_formula + "'"
			
li_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql)

for li_i = 1 to li_rows
	
	ls_cod_parametro = lds_store.getitemstring(li_i, 1)
	ls_valore_parametro = lds_store.getitemstring(li_i, 2)
	
	// primo provo a vedere se il parametro è già stato inserito
	li_row = dw_formule_parametri.find("cod_parametro='" + ls_cod_parametro + "'", 0, dw_formule_parametri.rowcount())
	
	if li_row = 0 then
		// il parametro non è stato inserito, allora procedo con l'inserimento della nuova riga
		li_row = dw_formule_parametri.insertrow(0)
		dw_formule_parametri.setitem(li_row, "cod_parametro", ls_cod_parametro)
	end if
	
	dw_formule_parametri.setitem(li_row, "valore_parametro", ls_valore_parametro)
	
next

return li_rows
end function

public function integer wf_load_param_formula (string as_cod_formula);string ls_variabili[], ls_errore
int li_i, li_row
uo_formule_calcolo luo_formule

luo_formule = create uo_formule_calcolo

dw_formule_parametri.reset()

if luo_formule.uof_lista_variabili(as_cod_formula, ref ls_variabili, ls_errore) < 0 then
	g_mb.warning(ls_errore)	
end if

for li_i = 1 to upperbound(ls_variabili)
	
	if guo_functions.uof_in_array(ls_variabili[li_i], is_global_param) then
		// Il parametro è già tra quelli globali dell'analisi, quindi non occorre che venga inserito!
		continue
	end if
	
	li_row = dw_formule_parametri.insertrow(0)
	dw_formule_parametri.setitem(li_row, "cod_parametro", ls_variabili[li_i])
	
next

destroy uo_formule_calcolo

return 1
end function

public function integer wf_load_global_param (string as_cod_analisi);/**
 * stefanop
 * 15/05/2012
 *
 * Carico i parametri globali dell'analisi in modo da escluderli da quelli necessari per la formula scelta
 **/
 
string ls_sql, ls_cod_parametro
int li_i, li_rows, li_row
datastore lds_store

ls_sql = "SELECT cod_analisi_parametro FROM tes_analisi_parametri " + &
			"WHERE cod_azienda='" + s_cs_xx.cod_azienda  + "' AND " + &
			"cod_analisi='" + istr_data.cod_analisi + "' "
			
li_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql)

for li_i = 1 to li_rows
	
	is_global_param[li_i] = lds_store.getitemstring(li_i, 1)
	
next

destroy lds_store
return li_rows
end function

on w_asa_grid_formula.create
int iCurrent
call super::create
this.cbx_negativo=create cbx_negativo
this.cb_test=create cb_test
this.dw_formule_parametri=create dw_formule_parametri
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_negativo
this.Control[iCurrent+2]=this.cb_test
this.Control[iCurrent+3]=this.dw_formule_parametri
this.Control[iCurrent+4]=this.cb_cancel
this.Control[iCurrent+5]=this.cb_ok
this.Control[iCurrent+6]=this.dw_lista
end on

on w_asa_grid_formula.destroy
call super::destroy
destroy(this.cbx_negativo)
destroy(this.cb_test)
destroy(this.dw_formule_parametri)
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag_negativo
istr_data = message.powerobjectparm

title = "Formula [riga: " + string(istr_data.num_riga) + ", colonna:" + string(istr_data.num_colonna) + "]"

wf_load_global_param(istr_data.cod_analisi)

dw_lista.settransobject(sqlca)
dw_lista.insertrow(0)
dw_lista.setitem(1, "cod_formula", istr_data.cod_formula)
if not isnull( istr_data.cod_formula) and  istr_data.cod_formula <> "" then
	wf_load_param( istr_data.cod_formula)
	
	select flag_negativo
	into :ls_flag_negativo
	from tes_analisi_formule
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_analisi = :istr_data.cod_analisi and
			 num_riga = :istr_data.num_riga and
			 num_colonna = :istr_data.num_colonna;
			 
	if sqlca.sqlcode = 0 then
		cbx_negativo.checked = (ls_flag_negativo = "S")
	end if
end if

dw_formule_parametri.settransobject(sqlca)



event post we_load_ddddlb()
end event

event closequery;call super::closequery;message.powerobjectparm = istr_data
end event

type cbx_negativo from checkbox within w_asa_grid_formula
integer x = 434
integer y = 1240
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Risultato Negativo"
end type

type cb_test from commandbutton within w_asa_grid_formula
integer x = 23
integer y = 1220
integer width = 366
integer height = 92
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Test"
end type

event clicked;string ls_cod_formula, ls_select_formula, ls_error, ls_cod_parametro, ls_valore_parametro
int li_i
decimal ld_risultato
uo_formule_calcolo luo_formule

dw_lista.accepttext()
dw_formule_parametri.accepttext()
ls_cod_formula = dw_lista.getitemstring(1, "cod_formula")

if isnull(ls_cod_formula) or ls_cod_formula = "" then
	g_mb.warning("Insere una formula prima di usare il pulsante Test")
	return
end if

luo_formule = create uo_formule_calcolo

// Imposto parametri
for li_i = 1 to dw_formule_parametri.rowcount()
	
	ls_cod_parametro = dw_formule_parametri.getitemstring(li_i, "cod_parametro")
	ls_valore_parametro = dw_formule_parametri.getitemstring(li_i, "valore_parametro")
	choose case mid(ls_cod_parametro, 2,1)
		case "S"
			ls_valore_parametro = "'" + ls_valore_parametro + "'"
			
		case "D"
			ls_valore_parametro = "'" + ls_valore_parametro + "'"
			
		case "N"
			ls_valore_parametro = ls_valore_parametro
			
	end choose
	
	luo_formule.iuo_cache.set(ls_cod_parametro, ls_valore_parametro)
next

if luo_formule.uof_calcola_formula(ls_cod_formula, ls_select_formula, ld_risultato, ls_error) < 0 then
	g_mb.error(ls_error)
else
	// stefanop 28/05/2012: controllo se devo moltiplicare per -1
	if cbx_negativo.checked then ld_risultato *= -1
	// ----
	
	g_mb.success("Risultato formula: " + string(ld_risultato))
end if

destroy luo_formule
end event

type dw_formule_parametri from uo_std_dw within w_asa_grid_formula
integer x = 23
integer y = 160
integer width = 2583
integer height = 1020
integer taborder = 20
string dataobject = "d_asa_grid_formule_parametri"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;call super::clicked;choose case dwo.name
		
	case "p_delete"
		if g_mb.confirm("Eliminare il parametro selezionato?") then
			deleterow(row)
		end if
		
	case "p_add"
		insertrow(0)
		
end choose
end event

type cb_cancel from commandbutton within w_asa_grid_formula
integer x = 1760
integer y = 1220
integer width = 402
integer height = 92
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_asa_grid_formula
integer x = 2194
integer y = 1220
integer width = 402
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;if wf_save() = 0 then
	commit;
	close(parent)
else
	rollback;
end if
end event

type dw_lista from uo_std_dw within w_asa_grid_formula
integer x = 23
integer y = 20
integer width = 2583
integer height = 120
integer taborder = 10
string dataobject = "d_asa_grid_formula_lista"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case dwo.name
		
	case "cod_formula"
		if isnull(data) or data = "" then
			return 2
		else
			wf_load_param(data)
		end if
		
end choose
end event

