﻿$PBExportHeader$w_banche_fidi.srw
forward
global type w_banche_fidi from w_cs_xx_principale
end type
type cb_banche_impresa from commandbutton within w_banche_fidi
end type
type dw_banche_fidi_det from uo_cs_xx_dw within w_banche_fidi
end type
type dw_banche_fidi_lista from uo_cs_xx_dw within w_banche_fidi
end type
end forward

global type w_banche_fidi from w_cs_xx_principale
integer width = 1728
integer height = 1180
string title = "Fidi Banche"
cb_banche_impresa cb_banche_impresa
dw_banche_fidi_det dw_banche_fidi_det
dw_banche_fidi_lista dw_banche_fidi_lista
end type
global w_banche_fidi w_banche_fidi

on w_banche_fidi.create
int iCurrent
call super::create
this.cb_banche_impresa=create cb_banche_impresa
this.dw_banche_fidi_det=create dw_banche_fidi_det
this.dw_banche_fidi_lista=create dw_banche_fidi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_banche_impresa
this.Control[iCurrent+2]=this.dw_banche_fidi_det
this.Control[iCurrent+3]=this.dw_banche_fidi_lista
end on

on w_banche_fidi.destroy
call super::destroy
destroy(this.cb_banche_impresa)
destroy(this.dw_banche_fidi_det)
destroy(this.dw_banche_fidi_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_banche_fidi_lista.set_dw_key("cod_azienda")
dw_banche_fidi_lista.set_dw_key("cod_banca")
dw_banche_fidi_lista.set_dw_key("cod_fido")
dw_banche_fidi_lista.set_dw_options(sqlca, &
                                       i_openparm, &
                                       c_scrollparent, &
                                       c_default)
dw_banche_fidi_det.set_dw_options(sqlca, &
                                     dw_banche_fidi_lista, &
                                     c_sharedata + c_scrollparent, &
                                     c_default)
iuo_dw_main = dw_banche_fidi_lista
//cb_note_esterne.enabled = false

end event

type cb_banche_impresa from commandbutton within w_banche_fidi
integer x = 1253
integer y = 984
integer width = 402
integer height = 84
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Impresa"
end type

event clicked;window_open_parm(w_banche_fidi_impresa, -1, dw_banche_fidi_lista)
end event

type dw_banche_fidi_det from uo_cs_xx_dw within w_banche_fidi
integer x = 23
integer y = 472
integer width = 1646
integer height = 504
integer taborder = 20
string dataobject = "d_banche_fidi_det"
borderstyle borderstyle = styleraised!
end type

type dw_banche_fidi_lista from uo_cs_xx_dw within w_banche_fidi
integer x = 23
integer y = 20
integer width = 1646
integer height = 440
integer taborder = 10
string dataobject = "d_banche_fidi_lista"
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_banca

ls_cod_banca = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_banca")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_banca")) then
      this.setitem(ll_i, "cod_banca", ls_cod_banca)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_banca


ls_cod_banca = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_banca")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_banca)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_save;call super::pcd_save;cb_banche_impresa.enabled = true
end event

event pcd_new;call super::pcd_new;cb_banche_impresa.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_banche_impresa.enabled = false
end event

event pcd_view;call super::pcd_view;cb_banche_impresa.enabled = true
end event

