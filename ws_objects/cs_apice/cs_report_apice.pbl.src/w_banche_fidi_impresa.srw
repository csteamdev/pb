﻿$PBExportHeader$w_banche_fidi_impresa.srw
forward
global type w_banche_fidi_impresa from w_cs_xx_principale
end type
type dw_banche_fidi_impresa_det from uo_cs_xx_dw within w_banche_fidi_impresa
end type
type dw_banche_fidi_impresa_lista from uo_cs_xx_dw within w_banche_fidi_impresa
end type
end forward

global type w_banche_fidi_impresa from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2382
integer height = 1100
string title = "Fidi Banche Impresa"
dw_banche_fidi_impresa_det dw_banche_fidi_impresa_det
dw_banche_fidi_impresa_lista dw_banche_fidi_impresa_lista
end type
global w_banche_fidi_impresa w_banche_fidi_impresa

type variables
boolean ib_in_new = false
end variables

on w_banche_fidi_impresa.create
int iCurrent
call super::create
this.dw_banche_fidi_impresa_det=create dw_banche_fidi_impresa_det
this.dw_banche_fidi_impresa_lista=create dw_banche_fidi_impresa_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_banche_fidi_impresa_det
this.Control[iCurrent+2]=this.dw_banche_fidi_impresa_lista
end on

on w_banche_fidi_impresa.destroy
call super::destroy
destroy(this.dw_banche_fidi_impresa_det)
destroy(this.dw_banche_fidi_impresa_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_banche_fidi_impresa_lista.set_dw_key("cod_azienda")
dw_banche_fidi_impresa_lista.set_dw_key("cod_banca")
dw_banche_fidi_impresa_lista.set_dw_key("cod_fido")


dw_banche_fidi_impresa_lista.set_dw_options(sqlca, &
                                       i_openparm, &
                                       c_scrollparent, &
                                       c_default)
dw_banche_fidi_impresa_det.set_dw_options(sqlca, &
                                     dw_banche_fidi_impresa_lista, &
                                     c_sharedata + c_scrollparent, &
                                     c_default)
												 
dw_banche_fidi_impresa_det.object.flag_anticipo_fatture.values = 'N'
dw_banche_fidi_impresa_det.object.flag_anticipo_cc.values = 'N'
iuo_dw_main = dw_banche_fidi_impresa_lista
//cb_note_esterne.enabled = false

end event

event pc_setddlb;call super::pc_setddlb;

if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_banche_fidi_impresa_det, &
						  "conto_impresa", &
						  sqlci, &
						  "conto", &
						  "codice", &
						  "descrizione", &
						  "")
end if
end event

type dw_banche_fidi_impresa_det from uo_cs_xx_dw within w_banche_fidi_impresa
integer x = 23
integer y = 480
integer width = 2309
integer height = 500
integer taborder = 20
string dataobject = "d_banche_fidi_impresa_det"
borderstyle borderstyle = styleraised!
end type

type dw_banche_fidi_impresa_lista from uo_cs_xx_dw within w_banche_fidi_impresa
integer x = 23
integer y = 20
integer width = 2309
integer height = 440
integer taborder = 10
string dataobject = "d_banche_fidi_impresa_lista"
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_banca, ls_cod_fido

ls_cod_banca = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_banca")
ls_cod_fido = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fido")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_banca")) then
      this.setitem(ll_i, "cod_banca", ls_cod_banca)
   end if
	if isnull(this.getitemstring(ll_i, "cod_fido")) then
      this.setitem(ll_i, "cod_fido", ls_cod_fido)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_banca, ls_cod_fido


ls_cod_banca = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_banca")
ls_cod_fido = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fido")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_banca, ls_cod_fido)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event updatestart;call super::updatestart;long ll_max
string ls_cod_banca, ls_cod_fido

ls_cod_banca = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_banca")
ls_cod_fido = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fido")

if ib_in_new then
  select max(progressivo)
     into :ll_max
     from anag_banche_fidi_impresa
     where (cod_azienda = :s_cs_xx.cod_azienda) and
	  cod_banca  = :ls_cod_banca and
	  cod_fido =  :ls_cod_fido;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_max) then
      ll_max = 1
   else
      ll_max = ll_max + 1
   end if
	
   dw_banche_fidi_impresa_lista.SetItem (dw_banche_fidi_impresa_lista.GetRow ( ),"progressivo", ll_max)


end if
end event

event pcd_disable;call super::pcd_disable;ib_in_new = false
end event

event pcd_modify;call super::pcd_modify;ib_in_new = false
end event

event pcd_new;call super::pcd_new;ib_in_new = true
end event

