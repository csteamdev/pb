﻿$PBExportHeader$w_buy_report_righe_fat_euro.srw
$PBExportComments$W_Report_Righe_Fattura_Acquisti
forward
global type w_buy_report_righe_fat_euro from w_cs_xx_principale
end type
type cb_ricerca_prodotti_1 from cb_prod_ricerca within w_buy_report_righe_fat_euro
end type
type cb_ricerca_prodotti_2 from cb_prod_ricerca within w_buy_report_righe_fat_euro
end type
type cb_1 from commandbutton within w_buy_report_righe_fat_euro
end type
type cb_annulla from commandbutton within w_buy_report_righe_fat_euro
end type
type cb_report from commandbutton within w_buy_report_righe_fat_euro
end type
type cb_selezione from commandbutton within w_buy_report_righe_fat_euro
end type
type dw_selezione from uo_cs_xx_dw within w_buy_report_righe_fat_euro
end type
type dw_report from uo_cs_xx_dw within w_buy_report_righe_fat_euro
end type
end forward

global type w_buy_report_righe_fat_euro from w_cs_xx_principale
integer width = 4571
integer height = 1988
string title = "Report Fatture Acquisti"
cb_ricerca_prodotti_1 cb_ricerca_prodotti_1
cb_ricerca_prodotti_2 cb_ricerca_prodotti_2
cb_1 cb_1
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_buy_report_righe_fat_euro w_buy_report_righe_fat_euro

forward prototypes
public function integer wf_righe_fattura ()
end prototypes

public function integer wf_righe_fattura ();string   ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_cod_fornitore, ls_rag_soc_1, &
			ls_where, ls_sql_tes, ls_sql_det, code, ls_where_det="",ls_protocollo, ls_num_doc_origine, &
			ls_cod_doc_origine, ls_anno_doc_origine, ls_numeratore_doc_origine, ls_protocollo_completo
 

dec{4}	ld_quan_fatturata, ld_prezzo_acquisto, ld_imponibile_iva_valuta, ld_num_reg_mov_mag, &
			ld_anno_reg_mov_mag, ld_prezzo_acq_netto, ld_imponibile_iva, ld_somma_imponi, ld_somma_iva, ld_somma_tot, & 
			ld_importo_iva, ld_tot_val_fat_acq, ld_perc, ld_sconti[10], ld_tot_sconto_perc
			
		 
datetime		ldt_data_registrazione, ldt_data_protocollo, ldt_data_doc_origine

long     ll_anno_registrazione, ll_num_registrazione, ll_riga, ll_i, ll_anno_prec, ll_num_prec
			

//Varibili-Parametri di ricerca
string ls_tipo_fattura,  ls_cod_prodotto_inf, ls_cod_prodotto_sup, ls_flag_dettaglio
datetime ldt_data_reg_inf, ldt_data_reg_sup, ldt_data_doc_inf, ldt_data_doc_sup


dw_report.reset()

//Assegnazione variabili selezione
ls_tipo_fattura = dw_selezione.getitemstring(1,"cod_tipo_fat_acq")
ls_cod_prodotto_inf = dw_selezione.getitemstring(1,"da_cod_prodotto")
ls_cod_prodotto_sup = dw_selezione.getitemstring(1,"a_cod_prodotto")
ls_cod_fornitore= dw_selezione.getitemstring(1,"cod_fornitore")
ldt_data_reg_inf = datetime(dw_selezione.getitemdate(1,"da_data_registrazione"))
ldt_data_reg_sup = datetime(dw_selezione.getitemdate(1,"a_data_registrazione"))
ldt_data_doc_inf = datetime(dw_selezione.getitemdate(1,"da_data_fattura_fornitore"))
ldt_data_doc_sup = datetime(dw_selezione.getitemdate(1,"a_data_fattura_fornitore"))
ls_flag_dettaglio = dw_selezione.getitemstring(1,"flag_dettaglio")

if ls_flag_dettaglio="F" then 
	dw_report.Object.DataWindow.Detail.Height=0
	dw_report.Object.linea.visible=0
	
	dw_report.Object.t_8.visible=1
	dw_report.Object.t_9.visible=1
	dw_report.Object.t_10.visible=1
	dw_report.Object.t_11.visible=1
	dw_report.Object.t_12.visible=1
	dw_report.Object.t_13.visible=1
	dw_report.Object.t_14.visible=1
	dw_report.Object.t_15.visible=1
	dw_report.Object.t_16.visible=1
	dw_report.Object.t_17.visible=1
	
	dw_report.Object.cod_fornitore_t.visible=0
	dw_report.Object.rag_soc_1_t.visible=0
	dw_report.Object.protocollo_t.visible=0
	dw_report.Object.data_protocollo_t.visible=0
	dw_report.Object.data_registrazione_t.visible=0
	dw_report.Object.num_doc_origine_t.visible=0
	dw_report.Object.data_doc_origine_t.visible=0
	dw_report.Object.tot_merci_t.visible=0
	dw_report.Object.importo_iva_t.visible=0
	dw_report.Object.tot_val_fat_acq_t.visible=0
	
	dw_report.Modify("DataWindow.Header.1.Height=186")
else
	dw_report.Object.DataWindow.Detail.Height=60
	dw_report.Object.linea.visible=1
	
	dw_report.Object.t_8.visible=0
	dw_report.Object.t_9.visible=0
	dw_report.Object.t_10.visible=0
	dw_report.Object.t_11.visible=0
	dw_report.Object.t_12.visible=0
	dw_report.Object.t_13.visible=0
	dw_report.Object.t_14.visible=0
	dw_report.Object.t_15.visible=0
	dw_report.Object.t_16.visible=0
	dw_report.Object.t_17.visible=0
	
	dw_report.Object.cod_fornitore_t.visible=1
	dw_report.Object.rag_soc_1_t.visible=1
	dw_report.Object.protocollo_t.visible=1
	dw_report.Object.data_protocollo_t.visible=1
	dw_report.Object.data_registrazione_t.visible=1
	dw_report.Object.num_doc_origine_t.visible=1
	dw_report.Object.data_doc_origine_t.visible=1
	dw_report.Object.tot_merci_t.visible=1
	dw_report.Object.importo_iva_t.visible=1
	dw_report.Object.tot_val_fat_acq_t.visible=1
	
	dw_report.Modify("DataWindow.Header.1.Height=305")
end if
	


//Creazione WHERE dinamico
ls_where = "where tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "'"

if not(isnull(ls_tipo_fattura)) then
	ls_where = ls_where + " and tes_fat_acq.cod_tipo_fat_acq = '" + ls_tipo_fattura + "'"
end if

if not (ls_cod_fornitore="") then
	ls_where = ls_where + " and tes_fat_acq.cod_fornitore = '" + ls_cod_fornitore + "'"
end if

if NOT ((isnull(ldt_data_reg_inf)) or (isnull(ldt_data_reg_sup))) then
	ls_where = ls_where + " and tes_fat_acq.data_registrazione between '" + string(ldt_data_reg_inf, s_cs_xx.db_funzioni.formato_data) + "' and '" + string(ldt_data_reg_sup, s_cs_xx.db_funzioni.formato_data) + "'"
end if

if NOT ((isnull(ldt_data_doc_inf)) or (isnull(ldt_data_doc_sup))) then
	ls_where = ls_where + " and tes_fat_acq.data_doc_origine between '" + string(ldt_data_doc_inf, s_cs_xx.db_funzioni.formato_data) + "' and '" + string(ldt_data_doc_sup, s_cs_xx.db_funzioni.formato_data) + "'"
end if

if NOT ((isnull(ls_cod_prodotto_inf)) or (isnull(ls_cod_prodotto_sup))) then
	ls_where = ls_where + " and det_fat_acq.cod_prodotto between '" + string(ls_cod_prodotto_inf) + "' and '" + string(ls_cod_prodotto_sup) + "'"
end if

ls_where=ls_where+" and tes_fat_acq.cod_fornitore = anag_fornitori.cod_fornitore"
ls_where=ls_where+" and tes_fat_acq.anno_registrazione = det_fat_acq.anno_registrazione"
ls_where=ls_where+" and tes_fat_acq.num_registrazione = det_fat_acq.num_registrazione"




//Cursore sulla testata delle fatture
declare cu_tes dynamic cursor for sqlsa;

ls_sql_tes = "select "+&
				 "tes_fat_acq.data_registrazione,"+& 
				 "tes_fat_acq.data_protocollo, "+&
				 "tes_fat_acq.protocollo, " +& 
				 "tes_fat_acq.data_doc_origine, "+&
				 "tes_fat_acq.num_doc_origine, "+&
				 "tes_fat_acq.imponibile_iva, " +&
				 "tes_fat_acq.importo_iva, "+&
				 "tes_fat_acq.tot_val_fat_acq, "+&
				 "anag_fornitori.cod_fornitore, " +&
				 "anag_fornitori.rag_soc_1, "+&
				 "tes_fat_acq.anno_registrazione, "+&
				 "tes_fat_acq.num_registrazione, "+&
				 "tes_fat_acq.cod_doc_origine, "+&
				 "tes_fat_acq.anno_doc_origine, "+&
				 "tes_fat_acq.numeratore_doc_origine "+&
				 " from tes_fat_acq, anag_fornitori, det_fat_acq " + ls_where + ""


prepare sqlsa from :ls_sql_tes;

open dynamic cu_tes;

if (sqlca.sqlcode = -1) then
	g_mb.messagebox("Errore","Errore di connessione al DB  01 "+ sqlca.sqlerrtext,stopsign!)
end if



do while 1=1
   fetch cu_tes into :ldt_data_registrazione, 
							:ldt_data_protocollo, 
							:ls_protocollo, 
							:ldt_data_doc_origine, 
							:ls_num_doc_origine, 
							:ld_imponibile_iva, 
							:ld_importo_iva, 
							:ld_tot_val_fat_acq, 
							:ls_cod_fornitore,
							:ls_rag_soc_1,
							:ll_anno_registrazione,
							:ll_num_registrazione,
							:ls_cod_doc_origine,
							:ls_anno_doc_origine,
							:ls_numeratore_doc_origine;

ls_protocollo_completo=ls_cod_doc_origine+"-"+ls_anno_doc_origine+"/"+ls_numeratore_doc_origine+"-"+ls_num_doc_origine 		
	
   if (sqlca.sqlcode = -1) then
		g_mb.messagebox("Errore","Errore di connessione al DB  02 "+ sqlca.sqlerrtext,stopsign!)
		exit
	end if
	
	if (sqlca.sqlcode = 100) then exit

if ((ll_anno_registrazione = ll_anno_prec) and (ll_num_registrazione = ll_num_prec)) then continue	


if NOT ((isnull(ls_cod_prodotto_inf)) or (isnull(ls_cod_prodotto_sup))) then
	ls_where_det =" and det_fat_acq.cod_prodotto between '" + string(ls_cod_prodotto_inf) + "' and '" + string(ls_cod_prodotto_sup) + "'"
else
	ls_where_det=""
end if

	//Cursore sul dettaglio delle fatture	
	declare cu_det dynamic cursor for sqlsa;

			ls_sql_det ="select "+&
							"det_fat_acq.cod_prodotto, "+&
							"det_fat_acq.des_prodotto, "+&
							"det_fat_acq.cod_misura, " +&
				 			"det_fat_acq.quan_fatturata, "+&
							"det_fat_acq.prezzo_acquisto, "+&
							"det_fat_acq.imponibile_iva_valuta, "+&
				 			"det_fat_acq.num_registrazione_mov_mag, "+&
							"det_fat_acq.anno_registrazione_mov_mag, "+&
							"det_fat_acq.prezzo_acq_netto, " +&
							"det_fat_acq.sconto_1, " +&
							"det_fat_acq.sconto_2, " +&
							"det_fat_acq.sconto_3, " +&
							"det_fat_acq.sconto_4, " +&
							"det_fat_acq.sconto_5, " +&
							"det_fat_acq.sconto_6, " +&
							"det_fat_acq.sconto_7, " +&
							"det_fat_acq.sconto_8, " +&
							"det_fat_acq.sconto_9, " +&
							"det_fat_acq.sconto_10 " +&
							"from  det_fat_acq " +&
							"where det_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "'" + &
							" and det_fat_acq.anno_registrazione = " + string(ll_anno_registrazione) + &
							" and det_fat_acq.num_registrazione = " + string(ll_num_registrazione) +" "+ ls_where_det + ""
	
	
	

	prepare sqlsa from :ls_sql_det;
	
	open dynamic cu_det;
	if (sqlca.sqlcode = -1) then g_mb.messagebox("Errore","Errore di connessione al DB 03 "+ sqlca.sqlerrtext,stopsign!)
	
	do while 1=1
   	fetch cu_det into :ls_cod_prodotto,
								:ls_des_prodotto,
								:ls_cod_misura,
								:ld_quan_fatturata,
								:ld_prezzo_acquisto,
								:ld_imponibile_iva_valuta,
								:ld_num_reg_mov_mag,
								:ld_anno_reg_mov_mag,
								:ld_prezzo_acq_netto,
								:ld_sconti[1],
								:ld_sconti[2],
								:ld_sconti[3], 
								:ld_sconti[4], 
								:ld_sconti[5],
								:ld_sconti[6],
								:ld_sconti[7],
								:ld_sconti[8],
								:ld_sconti[9],
								:ld_sconti[10] ;
								
		code=string(sqlca.sqlcode)			
   	if (sqlca.sqlcode = -1) then 
			g_mb.messagebox("Errore","Errore di connessione al DB 04 "+code+" "+sqlca.sqlerrtext,stopsign!)
			exit
		end if	
		
		if (sqlca.sqlcode = 100) then exit

		
	ld_perc = 100
for ll_i = 1 to 10
	if not (isnull(ld_sconti[ll_i])) then ld_perc = ld_perc - ((ld_perc * ld_sconti[ll_i]) / 100)
next

	ld_tot_sconto_perc=100 - ld_perc
	
if isnull(ls_des_prodotto) then
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti
	where cod_prodotto=:ls_cod_prodotto
	and cod_azienda=:s_cs_xx.cod_azienda;
end if
	
dw_report.SetRedraw ( false )
dw_report.Object.datawindow.pointer='hourglass!'
		//Inserimento campi nel report di visualizzazione
		ll_riga=dw_report.InsertRow(0)
		
		dw_report.setitem(ll_riga, "data_registrazione", ldt_data_registrazione)
		dw_report.setitem(ll_riga, "data_protocollo", ldt_data_protocollo)
		dw_report.setitem(ll_riga, "protocollo", ls_protocollo_completo)
		dw_report.setitem(ll_riga, "data_doc_origine", ldt_data_doc_origine)
		dw_report.setitem(ll_riga, "num_doc_origine", ls_protocollo)
		dw_report.setitem(ll_riga, "imponibile_iva", ld_imponibile_iva)
		dw_report.setitem(ll_riga, "importo_iva", ld_importo_iva)
		dw_report.setitem(ll_riga, "tot_val_fat_acq", ld_tot_val_fat_acq)
		
		dw_report.setitem(ll_riga, "cod_fornitore", ls_cod_fornitore)
		dw_report.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		
		dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
		dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
		dw_report.setitem(ll_riga, "cod_misura", ls_cod_misura)
		dw_report.setitem(ll_riga, "quan_fatturata", ld_quan_fatturata)
		dw_report.setitem(ll_riga, "prezzo_acquisto", ld_prezzo_acquisto)
		dw_report.setitem(ll_riga, "imponibile_iva_valuta", ld_imponibile_iva_valuta)
		dw_report.setitem(ll_riga, "num_reg_mov_mag", ld_num_reg_mov_mag)
		dw_report.setitem(ll_riga, "anno_reg_mov_mag", ld_anno_reg_mov_mag)
		dw_report.setitem(ll_riga, "prezzo_acq_netto", ld_prezzo_acq_netto)
		
		dw_report.setitem(ll_riga, "tot_sconto_perc", ld_tot_sconto_perc)
		
		dw_report.setitem(ll_riga, "anno_registrazione", ll_anno_registrazione)
		dw_report.setitem(ll_riga, "num_registrazione", ll_num_registrazione)
		
	
				
	loop
	close cu_det;
   ll_anno_prec=ll_anno_registrazione
	ll_num_prec=ll_num_registrazione
	if not isnull(ld_imponibile_iva) then ld_somma_imponi=ld_somma_imponi+ld_imponibile_iva
	if not isnull(ld_importo_iva) then ld_somma_iva=ld_somma_iva+ld_importo_iva
	if not isnull(ld_tot_val_fat_acq) then ld_somma_tot=ld_somma_tot+ld_tot_val_fat_acq
	dw_report.setitem(ll_riga, "somma_imponibile", ld_somma_imponi)
	dw_report.setitem(ll_riga, "somma_iva", ld_somma_iva)
	dw_report.setitem(ll_riga, "somma_tot", ld_somma_tot)
loop
close cu_tes;


dw_report.groupcalc()
dw_report.SetRedraw ( true )
dw_report.Object.datawindow.pointer='arrow!'

return 0
end function

event pc_setwindow;call super::pc_setwindow;
dw_selezione.insertrow(0)

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


this.x = 641
this.y = 485
this.width = 2600
this.height = 1050



end event

on w_buy_report_righe_fat_euro.create
int iCurrent
call super::create
this.cb_ricerca_prodotti_1=create cb_ricerca_prodotti_1
this.cb_ricerca_prodotti_2=create cb_ricerca_prodotti_2
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ricerca_prodotti_1
this.Control[iCurrent+2]=this.cb_ricerca_prodotti_2
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_report
this.Control[iCurrent+6]=this.cb_selezione
this.Control[iCurrent+7]=this.dw_selezione
this.Control[iCurrent+8]=this.dw_report
end on

on w_buy_report_righe_fat_euro.destroy
call super::destroy
destroy(this.cb_ricerca_prodotti_1)
destroy(this.cb_ricerca_prodotti_2)
destroy(this.cb_1)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_fat_acq",sqlca,&
                 "tab_tipi_fat_acq","cod_tipo_fat_acq","des_tipi_fat_acq",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_ricerca_prodotti_1 from cb_prod_ricerca within w_buy_report_righe_fat_euro
integer x = 2354
integer y = 232
integer width = 73
integer height = 72
integer taborder = 60
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "da_cod_prodotto"
end event

type cb_ricerca_prodotti_2 from cb_prod_ricerca within w_buy_report_righe_fat_euro
integer x = 2354
integer y = 312
integer width = 73
integer height = 72
integer taborder = 30
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "a_cod_prodotto"
end event

type cb_1 from commandbutton within w_buy_report_righe_fat_euro
integer x = 3680
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "stampa"
end type

event clicked;dw_report.print()
end event

type cb_annulla from commandbutton within w_buy_report_righe_fat_euro
integer x = 1760
integer y = 820
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;
dw_selezione.reset()
dw_selezione.InsertRow(0)

end event

type cb_report from commandbutton within w_buy_report_righe_fat_euro
integer x = 2149
integer y = 820
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;
dw_selezione.accepttext()
dw_selezione.hide()

//cb_ricerca_fornitore.hide()
cb_ricerca_prodotti_1.hide()
cb_ricerca_prodotti_2.hide()


parent.x = 60
parent.y = 50
parent.width = 4571
parent.height = 1988
dw_report.show()

wf_righe_fattura()			//funzione per selezionare il listino di vendita


cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_buy_report_righe_fat_euro
integer x = 4137
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;
dw_selezione.show()

//cb_ricerca_fornitore.show()
cb_ricerca_prodotti_1.show()
cb_ricerca_prodotti_2.show()


parent.x = 641
parent.y = 485
parent.width = 2600
parent.height = 1050

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_buy_report_righe_fat_euro
integer x = 23
integer width = 2514
integer height = 760
integer taborder = 20
string dataobject = "d_sel_report_righe_fat_acq"
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

type dw_report from uo_cs_xx_dw within w_buy_report_righe_fat_euro
boolean visible = false
integer x = 23
integer width = 4480
integer height = 1760
integer taborder = 10
string dataobject = "d_report_fatture_acquisti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

