﻿$PBExportHeader$w_giornale_mag.srw
$PBExportComments$Giornale di Magazzino (brogliaccio o ufficiale)
forward
global type w_giornale_mag from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_giornale_mag
end type
type cb_report from commandbutton within w_giornale_mag
end type
type cb_selezione from commandbutton within w_giornale_mag
end type
type dw_selezione from uo_cs_xx_dw within w_giornale_mag
end type
type dw_report from uo_cs_xx_dw within w_giornale_mag
end type
end forward

global type w_giornale_mag from w_cs_xx_principale
int X=60
int Y=289
int Width=3708
int Height=1701
boolean TitleBar=true
string Title="Report Giornale Magazzino"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_giornale_mag w_giornale_mag

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 1052
this.height = 537

end event

on w_giornale_mag.create
int iCurrent
call w_cs_xx_principale::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_annulla
this.Control[iCurrent+2]=cb_report
this.Control[iCurrent+3]=cb_selezione
this.Control[iCurrent+4]=dw_selezione
this.Control[iCurrent+5]=dw_report
end on

on w_giornale_mag.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

type cb_annulla from commandbutton within w_giornale_mag
int X=362
int Y=321
int Width=289
int Height=81
int TabOrder=30
string Text="&Esci"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_giornale_mag
int X=677
int Y=321
int Width=289
int Height=81
int TabOrder=40
string Text="&Report"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_flag_definitivo
datetime ldt_data_stampa, ldt_data_giornale

dw_selezione.accepttext()
ls_flag_definitivo = dw_selezione.getitemString(1,"rs_flag_definitivo")
ldt_data_stampa  = dw_selezione.getitemDatetime(1,"rdt_data_stampa")

if isnull(ls_flag_definitivo) then ls_flag_definitivo = "N"
if isnull(ldt_data_stampa) then  ldt_data_stampa  = datetime(today())



select data_giornale
into :ldt_data_giornale
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda
using sqlca;

if isnull(ldt_data_giornale) then  ldt_data_giornale  = datetime(date("01/01/1900"))

if ldt_data_stampa < ldt_data_giornale then
	g_mb.messagebox("Stampa Giornale Magazzino", "data inserita antecedente all'ultima data di stampa del giornale")
	return
end if

if ls_flag_definitivo = "S" then
	update con_magazzino
		set data_giornale = :ldt_data_giornale
		where cod_azienda = :s_cs_xx.cod_azienda
		using sqlca;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Stampa Giornale Magazzino" , "Errore aggiornamento della data di stampa giornale nella tabella Parametri Magazzino ")
			rollback;
			return 
		end if
end if

dw_selezione.hide()

parent.x = 5
parent.y = 50
parent.width = 3708
parent.height = 1701

dw_report.show()
dw_report.retrieve(s_cs_xx.cod_azienda, ldt_data_giornale, ldt_data_stampa)
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_giornale_mag
int X=3178
int Y=1461
int Width=366
int Height=81
int TabOrder=50
string Text="Selezione"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 1052
parent.height = 537

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_giornale_mag
int X=23
int Y=21
int Width=947
int Height=265
int TabOrder=20
string DataObject="d_dw_selezione_giornale_mag"
BorderStyle BorderStyle=StyleRaised!
end type

type dw_report from uo_cs_xx_dw within w_giornale_mag
int X=23
int Y=21
int Width=3571
int Height=1421
int TabOrder=10
boolean Visible=false
string DataObject="d_dw_giornale_mag"
boolean HScrollBar=true
boolean VScrollBar=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

