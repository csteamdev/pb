﻿$PBExportHeader$w_importi_automatici.srw
$PBExportComments$conti in impresa per calcolo per piano liquidità
forward
global type w_importi_automatici from w_cs_xx_principale
end type
type dw_importi_automatici from uo_cs_xx_dw within w_importi_automatici
end type
end forward

global type w_importi_automatici from w_cs_xx_principale
integer width = 2875
integer height = 1392
string title = "Importi Automatici per Piano Liquidità "
dw_importi_automatici dw_importi_automatici
end type
global w_importi_automatici w_importi_automatici

type variables
boolean ib_in_new = false
end variables

on w_importi_automatici.create
int iCurrent
call super::create
this.dw_importi_automatici=create dw_importi_automatici
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_importi_automatici
end on

on w_importi_automatici.destroy
call super::destroy
destroy(this.dw_importi_automatici)
end on

event pc_setddlb;call super::pc_setddlb;if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_importi_automatici, &
						  "conto_impresa", &
						  sqlci, &
						  "conto", &
						  "codice", &
						  "descrizione", &
						  "")
end if
end event

event pc_setwindow;call super::pc_setwindow;dw_importi_automatici.set_dw_key("cod_azienda")
dw_importi_automatici.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_default, &
                                    c_default)
end event

event closequery;call super::closequery;dw_importi_automatici.acceptText()
end event

type dw_importi_automatici from uo_cs_xx_dw within w_importi_automatici
integer x = 23
integer y = 20
integer width = 2789
integer height = 1260
integer taborder = 30
string dataobject = "d_importi_automatici"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event updatestart;call super::updatestart;long ll_max


if ib_in_new then

  select max(progressivo)
     into :ll_max
     from tab_importi_automatici
     where (cod_azienda = :s_cs_xx.cod_azienda) ;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_max) then
      ll_max = 1
   else
      ll_max = ll_max + 1
   end if
	
   dw_importi_automatici.SetItem (dw_importi_automatici.GetRow ( ),"progressivo", ll_max)


end if
end event

event pcd_disable;call super::pcd_disable;ib_in_new = false
end event

event pcd_modify;call super::pcd_modify;ib_in_new = false

end event

event pcd_new;call super::pcd_new;
ib_in_new = true

end event

event pcd_retrieve;call super::pcd_retrieve;
long   l_errore


l_errore = retrieve(s_cs_xx.cod_azienda)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_max


for ll_i = 1 to this.rowcount()
	
	  if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	if isnull(this.getitemnumber(ll_i, "progressivo")) or &
      this.getitemnumber(ll_i, "progressivo") = 0	then
      this.setitem(ll_i, "progressivo", ll_max)
   end if	
next
end event

