﻿$PBExportHeader$w_importi_periodici.srw
$PBExportComments$window per inserire gli importi periodici per il piano liquidità
forward
global type w_importi_periodici from w_cs_xx_principale
end type
type dw_importi_periodici_dettaglio from uo_cs_xx_dw within w_importi_periodici
end type
type dw_importi_periodici from uo_cs_xx_dw within w_importi_periodici
end type
end forward

global type w_importi_periodici from w_cs_xx_principale
integer width = 1664
integer height = 1724
string title = "Importi Periodici per piano liquidità"
dw_importi_periodici_dettaglio dw_importi_periodici_dettaglio
dw_importi_periodici dw_importi_periodici
end type
global w_importi_periodici w_importi_periodici

type variables
boolean ib_in_new = false
end variables

on w_importi_periodici.create
int iCurrent
call super::create
this.dw_importi_periodici_dettaglio=create dw_importi_periodici_dettaglio
this.dw_importi_periodici=create dw_importi_periodici
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_importi_periodici_dettaglio
this.Control[iCurrent+2]=this.dw_importi_periodici
end on

on w_importi_periodici.destroy
call super::destroy
destroy(this.dw_importi_periodici_dettaglio)
destroy(this.dw_importi_periodici)
end on

event pc_setwindow;call super::pc_setwindow;dw_importi_periodici.set_dw_key("cod_azienda")
dw_importi_periodici.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_default, &
                                    c_default)
dw_importi_periodici_dettaglio.set_dw_options(sqlca, &
                                		dw_importi_periodici, &
                                		c_sharedata + c_scrollparent, &
                                		c_default)
iuo_dw_main = dw_importi_periodici
end event

event pc_setddlb;call super::pc_setddlb;if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_importi_periodici_dettaglio, &
						  "conto_impresa", &
						  sqlci, &
						  "conto", &
						  "codice", &
						  "descrizione", &
						  "")
end if
end event

event closequery;call super::closequery;


//dw_importi_periodici_dettaglio.acceptText()
//dw_importi_periodici.acceptText()
//


end event

type dw_importi_periodici_dettaglio from uo_cs_xx_dw within w_importi_periodici
integer y = 660
integer width = 1600
integer height = 940
integer taborder = 20
string dataobject = "d_importi_periodici_dettaglio"
borderstyle borderstyle = styleraised!
end type

event updatestart;call super::updatestart;//long ll_max
//
//
//if ib_in_new then
//
//  select max(progressivo)
//     into :ll_max
//     from tab_importi_periodici
//     where (cod_azienda = :s_cs_xx.cod_azienda) ;
//
//   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_max) then
//      ll_max = 1
//   else
//      ll_max = ll_max + 1
//   end if
//	
//   dw_importi_periodici_dettaglio.SetItem (dw_importi_periodici_dettaglio.GetRow ( ),"progressivo", ll_max)
//
//
//end if
end event

event pcd_setkey;call super::pcd_setkey;//long ll_i, ll_max
//
//
//for ll_i = 1 to this.rowcount()
//	
//	  if isnull(this.getitemstring(ll_i, "cod_azienda")) then
//      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
//   end if
//	if isnull(this.getitemnumber(ll_i, "progressivo")) or &
//      this.getitemnumber(ll_i, "progressivo") = 0	then
//      this.setitem(ll_i, "progressivo", ll_max)
//   end if	
//next
end event

type dw_importi_periodici from uo_cs_xx_dw within w_importi_periodici
integer x = 23
integer y = 20
integer width = 1577
integer height = 620
integer taborder = 10
string dataobject = "d_importi_periodici"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_max



for ll_i = 1 to this.rowcount()
	
	  if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	if isnull(this.getitemnumber(ll_i, "progressivo")) or &
      this.getitemnumber(ll_i, "progressivo") = 0	then
      this.setitem(ll_i, "progressivo", ll_max)
   end if	
next
end event

event updatestart;call super::updatestart;long ll_max, ll_i
datetime ldt_data, ldt_data_2

dw_importi_periodici_dettaglio.acceptText()

if ib_in_new then

  select max(progressivo)
     into :ll_max
     from tab_importi_periodici
     where (cod_azienda = :s_cs_xx.cod_azienda) ;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_max) then
      ll_max = 1
   else
      ll_max = ll_max + 1
   end if
	
  // dw_importi_periodici_dettaglio.SetItem (dw_importi_periodici_dettaglio.GetRow ( ),"progressivo", ll_max)
 	dw_importi_periodici.SetItem (dw_importi_periodici.GetRow ( ),"progressivo", ll_max)

end if

for ll_i = 1 to dw_importi_periodici.rowcount()	
	  	if isnull(dw_importi_periodici_dettaglio.getitemstring(ll_i, "conto_impresa")) then
      	g_mb.messagebox ( "Apice ", "Inserire il codice del conto in Impresa" , StopSign!)
			return -1
		end if	
		ldt_data = dw_importi_periodici_dettaglio.getitemdatetime(ll_i, "data_inizio")
		if isnull(ldt_data) then
      	g_mb.messagebox ( "Apice ", "Inserire la data di inizio" , StopSign!)
			return -1
		end if
		ldt_data_2 = dw_importi_periodici_dettaglio.getitemdatetime(ll_i, "data_fine")
		if isnull(ldt_data_2) then
      	g_mb.messagebox ( "Apice ", "Inserire la data di fine" , StopSign!)
			return -1
		end if
		
		if ldt_data_2 < ldt_data then
      	g_mb.messagebox ( "Apice ", "Incoerenza nelle date inserite" , StopSign!)
			return -1
		end if	
		
next


end event

event pcd_new;call super::pcd_new;ib_in_new = true
end event

event pcd_modify;call super::pcd_modify;//se ho appena cliccato new e poi modify
if ib_in_new then
	ib_in_new = true
else
	ib_in_new = false
end if
end event

event pcd_delete;call super::pcd_delete;ib_in_new = false
end event

event pcd_disable;call super::pcd_disable;ib_in_new = false
end event

