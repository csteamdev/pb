﻿$PBExportHeader$w_inventario_magazzino_euro.srw
$PBExportComments$Report Inventario del Magazino
forward
global type w_inventario_magazzino_euro from w_cs_xx_principale
end type
type dw_folder from u_folder within w_inventario_magazzino_euro
end type
type dw_report from uo_cs_xx_dw within w_inventario_magazzino_euro
end type
type pb_1 from picturebutton within w_inventario_magazzino_euro
end type
type dw_selezione from uo_cs_xx_dw within w_inventario_magazzino_euro
end type
type dw_cod_deposito from uo_dddw_checkbox within w_inventario_magazzino_euro
end type
type cb_report from commandbutton within w_inventario_magazzino_euro
end type
type cb_annulla from commandbutton within w_inventario_magazzino_euro
end type
type sle_1 from singlelineedit within w_inventario_magazzino_euro
end type
end forward

global type w_inventario_magazzino_euro from w_cs_xx_principale
integer x = 73
integer y = 300
integer width = 4027
integer height = 2488
string title = "Inventario Magazzino"
dw_folder dw_folder
dw_report dw_report
pb_1 pb_1
dw_selezione dw_selezione
dw_cod_deposito dw_cod_deposito
cb_report cb_report
cb_annulla cb_annulla
sle_1 sle_1
end type
global w_inventario_magazzino_euro w_inventario_magazzino_euro

type variables
boolean ib_interrupt
//uo_dddw_checkbox iuo_dddw_depositi

protected:
	boolean			ib_gibus = false
end variables

forward prototypes
public function integer wf_report_raggruppo ()
end prototypes

public function integer wf_report_raggruppo ();string						ls_cod_prodotto_da, ls_cod_cat_mer_1, ls_flag_bloccato, ls_where, ls_error, ls_cod_prodotto, ls_messaggio,&
							ls_cod_misura_mag, ls_cod_cat_mer, ls_flag_blocco, ls_sort, ls_sql, ls_cod_prodotto_a, &
							ls_des_prodotto, ls_flag_det_depositi, ls_cod_deposito, ls_flag_det_stock, &
							ls_flag_visualizza_val, ls_flag_tipo_valorizzazione, ls_chiave[], ls_cod_deposito_selezione, &
							ls_intestazione, ls_des_azienda, ls_flag_visulizza_giac_zero, ls_costo_unitario_t,  ls_pks[], &
							ls_vettore_nullo[],ls_flag_fiscale, ls_flag_classe, ls_flag_escludi_cdf, ls_array[], ls_cod_depositi_array[], ls_flag_lifo, &
							ls_errore_ds, ls_cod_prodotto_raggruppato, ls_des_prodotto_raggruppato

datastore				lds_prodotti

integer					li_return, li_cont
long						ll_i, li_count, ll_newrow, ll_num_righe, ll_num_stock, ll_j, ll_ret, ll_rows, ll_x

dec{4}					ld_saldo_quan_inizio_anno, ld_quant_val[], ld_giacenza, ld_totale_quantita, ld_costo_medio, ld_quan_acq, ld_val_inizio_anno, &
							ld_costo_standard, ld_prezzo_acquisto, ld_costo, ld_nulla, ld_giacenza_stock[], ld_costo_unitario, ld_vettore_nullo[], &
							ld_costo_medio_continuo,ld_costo_medio_continuo_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], &
							ld_fat_conversione_rag_mag, ld_gz_x_cm, ld_gz_fconv, ld_tot_valore
							
datetime					ldt_data_a, ldt_data_blocco, ldt_data_chiusura_annuale

uo_costo_medio_continuo luo_costo_medio
uo_magazzino luo_magazzino


dw_selezione.AcceptText()

//datetime ldt_data_a è la data di inventario
dw_report.reset()
dw_report.setredraw(false)
ib_interrupt = false

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;

ldt_data_a = dw_selezione.getItemDatetime(1,"data_inventario")

if ldt_data_a < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if

setnull(ld_nulla)

select rag_soc_1
into :ls_des_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;
if not isnull(ls_des_azienda) then ls_intestazione = ls_des_azienda

ls_cod_prodotto_da = dw_selezione.getItemString(1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getItemString(1, "cod_prodotto_a")
ls_cod_cat_mer_1 = dw_selezione.getItemString(1, "cod_cat_mer")
ldt_data_a = dw_selezione.getItemDatetime(1, "data_inventario")
ls_flag_det_depositi = dw_selezione.getItemString(1, "flag_det_depositi")
ls_cod_deposito = dw_cod_deposito.uof_get_selected_column("cod_deposito", false)
g_str.explode(ls_cod_deposito, ",", ls_cod_depositi_array)
ls_cod_deposito_selezione = dw_cod_deposito.uof_get_selected_column("cod_deposito", true)

ls_flag_lifo = dw_selezione.getItemString(1, "flag_lifo")
//ls_flag_lifo = ""
//if not ib_gibus then
//	ls_flag_lifo = dw_selezione.getItemString(1, "flag_lifo")
//end if
if ls_flag_lifo = "" or isnull(ls_flag_lifo) then ls_flag_lifo="T"


ls_flag_det_stock = dw_selezione.getItemString(1, "flag_det_stock")
ls_flag_visualizza_val = dw_selezione.getItemString(1, "flag_visualizza_val")
ls_flag_tipo_valorizzazione = dw_selezione.getItemString(1, "flag_tipo_valorizzazione")
ls_flag_visulizza_giac_zero = dw_selezione.getItemString(1, "flag_visulizza_giac_zero")
ls_flag_fiscale = dw_selezione.getItemString(1, "flag_fiscale")
ls_flag_classe = dw_selezione.getItemString(1, "flag_classe")
ls_flag_escludi_cdf = dw_selezione.getItemString(1, "flag_includi_cdf")

if isnull(ls_flag_det_depositi) then ls_flag_det_depositi = "N"
if isnull(ls_flag_det_stock) then ls_flag_det_stock = "N"
if isnull(ls_flag_visulizza_giac_zero) then ls_flag_visulizza_giac_zero = "N"
if isnull(ls_flag_fiscale) or ls_flag_fiscale="" then ls_flag_fiscale = "T"
if isnull(ls_flag_classe) or len(ls_flag_classe) < 1 then ls_flag_classe = "X"


if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then	// devo selezionare ls_cod_prodotto_da e ls_cod_prodotto_a
	if (isnull(ls_cod_prodotto_da) or ls_cod_prodotto_da="") or (isnull(ls_cod_prodotto_a)  or ls_cod_prodotto_a="") then
		g_mb.messagebox("Apice", "Selezionare sia cod. prodotto_da, sia cod. prodotto a")
		return -1
	else
		ls_intestazione = ls_intestazione + "; prodotti da:" + ls_cod_prodotto_da + &
		" a:" + ls_cod_prodotto_a + "; "
	end if
else
	ls_intestazione = ls_intestazione + "; cat. merceologica:" + ls_cod_cat_mer_1 + "; "
end if

if (not isnull(ls_cod_deposito)) and (ls_cod_deposito<>"") then
	ls_intestazione = ls_intestazione + " deposito:" + dw_selezione.getitemstring(1, "cod_deposito") + "; "
end if

if ls_flag_lifo = "S" then
	ls_intestazione += " Lifo: Si;"
elseif ls_flag_lifo="N" then
	ls_intestazione += " Lifo: No;"
end if

choose case ls_flag_visualizza_val
		
	case "N", "X"
		ls_flag_tipo_valorizzazione = "N"
		
	case "S"
		
		CHOOSE CASE ls_flag_tipo_valorizzazione
			CASE "S"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Standard"
				ls_costo_unitario_t = "Costo Std."
			CASE "M"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Medio"
				ls_costo_unitario_t = "Costo Medio"
			CASE "A"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Acquisto"
				ls_costo_unitario_t = "Costo Acq."
			CASE "N"
				ls_costo_unitario_t = "Costo "
			CASE "C"
				ls_costo_unitario_t = "Costo Medio Continuo"
		END CHOOSE
	
end choose

ld_gz_x_cm = 0
ld_gz_fconv = 0

if ls_flag_det_stock ="N" and ls_flag_det_depositi = "N" then
	//############################################################################################
	// caso NO dettaglio_stock e NO dettaglio_deposito
	//############################################################################################
	
	dw_report.DataObject = 'd_dw_report_inventario_euro_r'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
	dw_report.object.linea.visible = 0
	if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
		ls_sql = "select cod_prodotto, des_prodotto, cod_misura_mag, cod_prodotto_raggruppato, fat_conversione_rag_mag " +&
					"from anag_prodotti " +&
					"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
					"cod_prodotto >= '" + ls_cod_prodotto_da + "' and " +&
					"cod_prodotto <= '" + ls_cod_prodotto_a + "' "
	else
		ls_sql = "select cod_prodotto, des_prodotto, cod_misura_mag, cod_prodotto_raggruppato, fat_conversione_rag_mag " +&
					"from anag_prodotti " +&
					"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
					"cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
	end if

	if ls_flag_fiscale="N" or ls_flag_fiscale = "S" then
		ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
	end if
					
	if ls_flag_classe <> "X" then
		ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
	end if
	
	if ls_flag_lifo <> "T" then ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "
	
	//elaboro comunque solo i prodotti in stock --------------------------------
	ls_sql += " and cod_prodotto in (select distinct cod_prodotto from stock " +&
											 " where cod_azienda='"+s_cs_xx.cod_azienda+"')"
											 
	//imposto il filtro per deposito/i eventualmente impostato
	if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
		ls_sql +=" and cod_prodotto in (select distinct cod_prodotto from mov_magazzino " +&
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + ls_cod_deposito_selezione + " )"
	end if
	
	//escludo sempre a -priori i codici che sono di raggruppo, i cui valori devono essere ricavati
	ls_sql +=" and cod_prodotto not in (select distinct cod_prodotto_raggruppato from anag_prodotti " +&
						" where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto_raggruppato is not null and cod_prodotto_raggruppato<>'') "
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++
	DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql;
	OPEN DYNAMIC cur_1 ;
	
	DO while true
		Yield()
		if ib_interrupt then
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
		
		FETCH cur_1 INTO :ls_cod_prodotto, :ls_des_prodotto, :ls_cod_misura_mag, &
								:ls_cod_prodotto_raggruppato, :ld_fat_conversione_rag_mag;
								
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotti:~r~n" + SQLCA.SQLErrText)
			close cur_1;
			rollback;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		if isnull(ls_cod_prodotto_raggruppato) or ls_cod_prodotto_raggruppato="" then
			//vuol dire che non è un codice che si raggruppa in un altro, quindi predispongo
			//come se si raggruppasse con se stesso ...
			ls_cod_prodotto_raggruppato = ls_cod_prodotto
			ls_des_prodotto_raggruppato = ls_des_prodotto
			ld_fat_conversione_rag_mag = 1
		else
			//fattore conversione in raggruppo
			if isnull(ld_fat_conversione_rag_mag) then ld_fat_conversione_rag_mag = 1
			
			//leggi la descrizione del codice raggruppo
			select des_prodotto, cod_misura_mag
			into :ls_des_prodotto_raggruppato, :ls_cod_misura_mag
			from anag_prodotti
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_prodotto=:ls_cod_prodotto_raggruppato;
		end if
	
		ll_newrow = dw_report.insertrow(0) 
		dw_report.setitem(ll_newrow, "rs_cod_prodotto", ls_cod_prodotto)
		dw_report.setitem(ll_newrow, "rs_descrizione", ls_des_prodotto)
		dw_report.setitem(ll_newrow, "cod_misura_mag_raggruppo", ls_cod_misura_mag)
		dw_report.setitem(ll_newrow, "cod_prodotto_raggruppo" , ls_cod_prodotto_raggruppato)
		dw_report.setitem(ll_newrow, "f_conv" , ld_fat_conversione_rag_mag)
		dw_report.setitem(ll_newrow, "des_prod_raggruppo" , ls_des_prodotto_raggruppato)
		
		sle_1.text= "prodotto :" + ls_cod_prodotto
		
		if ls_flag_tipo_valorizzazione = "C" then
			// costo medio continuo (30/12/2002 Realizzato per colombin - EnMe)
			luo_costo_medio = create uo_costo_medio_continuo 
			ll_ret = luo_costo_medio.uof_costo_medio_continuo(		ls_cod_prodotto, ldt_data_a, ref ld_giacenza, ref ld_costo_medio_continuo, ref ls_chiave[], &
																					ref ld_giacenza_stock[], ref ld_costo_medio_continuo_stock[], dw_selezione.getitemstring(1, "path"), ref ls_messaggio)
			destroy luo_costo_medio
			if ll_ret = -1 then
				g_mb.error("Inventario", ls_messaggio)
				close cur_1;
				rollback;
				return -1
			end if	
			
			//in ld_giacenza c'è il valore da inserire nel relativo campo
			
			choose case ls_flag_visualizza_val
				case "S"
					ld_costo = ld_costo_medio_continuo
					
				case "N"	// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					ld_costo = ld_nulla
					
				case "X"
					dw_report.object.valore_t.visible = 0
					dw_report.object.linea.visible = 0
					dw_report.object.t_1.visible = 0
					dw_report.object.totale.visible = 0
					dw_report.object.costo_unitario_t.visible = 0
					dw_report.object.valore_t.visible = 0
					dw_report.object.valore_riga.visible = 0
					dw_report.object.rd_costo_unitario.visible = 0
			end choose
			
		else
			//no costo medio continuo
			for li_cont = 1 to 14
				ld_quant_val[li_cont] = 0
			next
			
			luo_magazzino = CREATE uo_magazzino
			luo_magazzino.is_considera_depositi_fornitori = "I" //ls_flag_escludi_cdf
			
			li_count = dw_cod_deposito.uof_get_selected_column_as_array("cod_deposito", ls_pks, true)
			if li_count > 0 then
				 luo_magazzino.is_cod_depositi_in =  g_str.implode(ls_pks, ",")
			else
				luo_magazzino.is_cod_depositi_in = ""
			end if
			li_return = luo_magazzino.uof_saldo_prod_date_decimal(			ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "N", &
																								ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
			destroy luo_magazzino
			
			if li_return <0 then
				g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
				close cur_1;
				rollback;
				return -1
			end if
			
			ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			
			// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
			// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
			// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
			
			choose case ls_flag_visualizza_val
				case "S"
					if ls_flag_tipo_valorizzazione = "S" then	// costo standard
						select costo_standard
						into :ld_costo_standard
						from anag_prodotti
						where cod_azienda = :s_cs_xx.cod_azienda
						  and cod_prodotto = :ls_cod_prodotto;
						  
						if sqlca.sqlcode<0 then
							g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti : "+sqlca.sqlerrtext)
							CLOSE cur_1 ;
							rollback;
							return -1
						end if
						ld_costo = ld_costo_standard
						
					elseif ls_flag_tipo_valorizzazione = "A" then	//ultimo costo acquisto
						// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
						if ld_quant_val[14] >0 then
							ld_costo = ld_quant_val[14]
						else
							ld_costo = 0
						end if
						
					elseif ls_flag_tipo_valorizzazione = "M" then	//costo medio di acquisto
						if ld_quant_val[12] > 0 then
							ld_costo = ld_quant_val[13] / ld_quant_val[12]
							
						else
							select costo_medio_ponderato
							into   :ld_costo
							from   lifo
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_prodotto and
									 anno_lifo = (select max(anno_lifo)
													  from   lifo
													  where  cod_azienda = :s_cs_xx.cod_azienda and
																cod_prodotto = :ls_cod_prodotto);

							if sqlca.sqlcode < 0 then
								g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
								CLOSE cur_1 ;
								rollback;
								return -1
								
							elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
								ld_costo = 0
								
							end if

						end if
					end if
					
					
				case "N"		// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					ld_costo = ld_nulla
					
				case "X"
					dw_report.object.valore_t.visible = 0
					dw_report.object.linea.visible = 0
					dw_report.object.t_1.visible = 0
					dw_report.object.totale.visible = 0
					dw_report.object.costo_unitario_t.visible = 0
					dw_report.object.valore_t.visible = 0
					dw_report.object.valore_riga.visible = 0
					dw_report.object.rd_costo_unitario.visible = 0
			end choose
		end if
		
		ld_costo_unitario = ld_costo
		
		dw_report.setitem(ll_newrow, "rd_giacenza", ld_giacenza)
		dw_report.setitem(ll_newrow, "rd_costo_unitario", ld_costo_unitario)
		
		//se hai richiesto di non visualizzare le giacenze nulle (o negative)
		//evita di includere nella somma, altrimenti il totale si sballa (in quanto decrementa il valore ma non lo include poi nella lista)
		if ls_flag_visulizza_giac_zero = "N" and (ld_giacenza<0 or ld_costo_unitario<0) then
			//non includere
		else
			ld_gz_x_cm += ld_giacenza * ld_costo_unitario
			ld_gz_fconv += ld_giacenza * ld_fat_conversione_rag_mag
		end if
		
	LOOP
	CLOSE cur_1 ;
	//+++++++++++++++++++++++++++++++++++++++++++++++++

	ls_sort = "rs_cod_prodotto"
//	dw_report.SetSort(ls_sort)
//	dw_report.Sort()
	dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
	dw_report.object.titolo.width = 3223
	
	if ld_gz_fconv<>0 then
		ld_tot_valore = ld_gz_x_cm / ld_gz_fconv
	else
		ld_tot_valore = 0
	end if
	
	dw_report.object.valore_totale_t.text = string(ld_gz_x_cm, "###,###,###,##0.00")
	
end if
// fine caso dettaglio_stock = N e dettaglio_deposito = N


if ls_flag_det_stock ="S" and ls_flag_det_depositi = "N" then
	//NON PREVISTO, PERCHE' NON AVREBBE SENSO
	g_mb.warning("Attivando l'opzione 'Prodotto Raggruppato' non puoi attivare anche l'opzione dettaglio stock!")
	return 0
end if

if ls_flag_det_stock ="N" and ls_flag_det_depositi = "S" then
	//############################################################################################
	// caso dettaglio_stock = N e dettaglio_deposito = S
	//############################################################################################
	
	//piazzo il dataobject con il raggruppo
	dw_report.DataObject = 'd_dw_report_inventario_deposito_euro_r'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
	
	dw_report.object.linea.visible = 0
	dw_report.object.rs_chiave_t.text = "Dep."
	
	if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
		if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
			ls_sql = "select cod_prodotto, des_prodotto, cod_misura_mag, cod_prodotto_raggruppato, fat_conversione_rag_mag " +&
						"from anag_prodotti " +&
						"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						"cod_prodotto >= '" + ls_cod_prodotto_da + "' and " +&
						"cod_prodotto <= '" + ls_cod_prodotto_a + "' "
		else
			ls_sql = "select cod_prodotto, des_prodotto, cod_misura_mag, cod_prodotto_raggruppato, fat_conversione_rag_mag " +&
						"from anag_prodotti " +&
						"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						"cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
		end if
		
		if ls_flag_fiscale="N" or ls_flag_fiscale = "S" then
			ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
		end if
		
		if ls_flag_classe <> "X" then
			ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
		end if
		
		if ls_flag_lifo <> "T" then ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "

	else
		if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
			
			ls_sql = "select cod_prodotto, des_prodotto, cod_misura_mag, cod_prodotto_raggruppato, fat_conversione_rag_mag " +&
						"from anag_prodotti " +&
						"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						"cod_prodotto >= '" + ls_cod_prodotto_da + "' and " +&
						"cod_prodotto <= '" + ls_cod_prodotto_a + "' "

			if ls_flag_fiscale="N" or ls_flag_fiscale = "S" then
				ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
			end if
			
			if ls_flag_classe <> "X" then
				ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
			end if
	
			if ls_flag_lifo <> "T" then ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "

			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto from stock " +&
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

			if ls_cod_deposito <> "" then ls_sql += " AND " + ls_cod_deposito_selezione + " "
			ls_sql += ") "
		else
			ls_sql = "select cod_prodotto, des_prodotto, cod_misura_mag, cod_prodotto_raggruppato, fat_conversione_rag_mag " +&
						"from anag_prodotti " +&
						"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						"cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "

			if ls_flag_fiscale="N" or ls_flag_fiscale = "S" then
				ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
			end if

			
			if ls_flag_classe <> "X" then
				ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
			end if
	
			if ls_flag_lifo <> "T" then ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "
			
			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto from mov_magazzino " +&
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
						
			if ls_cod_deposito <> "" then ls_sql += " AND "+  ls_cod_deposito_selezione + " "
			ls_sql += ") "
		end if
		
	end if
	
	//elaboro comunque solo i prodotti in stock --------------------------------
	ls_sql += " and cod_prodotto in (select distinct cod_prodotto from stock " +&
											 " where cod_azienda='"+s_cs_xx.cod_azienda+"')"
	
	//escludo sempre a -priori i codici che sono di raggruppo, i cui valori devono essere ricavati
	ls_sql +=" and cod_prodotto not in (select distinct cod_prodotto_raggruppato from anag_prodotti " +&
						" where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto_raggruppato is not null and cod_prodotto_raggruppato<>'') "
	
	
	if guo_functions.uof_crea_datastore(ref lds_prodotti, ls_sql, ref ls_errore_ds) < 0 then
		g_mb.error("Errore in create datastore lds_prodotti. ~r~n " + ls_errore_ds)
		rollback;
		return -1
	end if
	
	lds_prodotti.settransobject(sqlca)
	ll_rows = lds_prodotti.retrieve()
	
	
	for ll_x = 1 to ll_rows
		if mod(ll_x, 10) = 0 then Yield()
		if ib_interrupt then
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
		
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next
		
		ls_cod_prodotto = lds_prodotti.getitemstring(ll_x, 1)
		ls_des_prodotto =  lds_prodotti.getitemstring(ll_x, 2)
		ls_cod_misura_mag = lds_prodotti.getitemstring(ll_x, 3)
		
		ls_cod_prodotto_raggruppato = lds_prodotti.getitemstring(ll_x, 4)
		
		if isnull(ls_cod_prodotto_raggruppato) or ls_cod_prodotto_raggruppato="" then
			//vuol dire che non è un codice che si raggruppa in un altro, quindi predispongo
			//come se si raggruppasse con se stesso ...
			ls_cod_prodotto_raggruppato = ls_cod_prodotto
			ls_des_prodotto_raggruppato = ls_des_prodotto
			ld_fat_conversione_rag_mag = 1
			
		else
			//fattore conversione in raggruppo
			ld_fat_conversione_rag_mag = lds_prodotti.getitemdecimal(ll_x, 5)
			if isnull(ld_fat_conversione_rag_mag) then ld_fat_conversione_rag_mag = 1
			
			//leggi la descrizione del codice raggruppo
			select des_prodotto, cod_misura_mag
			into :ls_des_prodotto_raggruppato, :ls_cod_misura_mag
			from anag_prodotti
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_prodotto=:ls_cod_prodotto_raggruppato;
			
		end if
		
		sle_1.text= "prodotto :" + ls_cod_prodotto + " - " + ls_des_prodotto
		
		ls_chiave = ls_vettore_nullo
		ld_giacenza_stock = ld_vettore_nullo
		
		//********************************************************************
		luo_magazzino = CREATE uo_magazzino
		luo_magazzino.is_considera_depositi_fornitori = "I" //ls_flag_escludi_cdf
		
		li_count = dw_cod_deposito.uof_get_selected_column_as_array("cod_deposito", ls_pks, true)
		if li_count > 0 then
			 luo_magazzino.is_cod_depositi_in =  g_str.implode(ls_pks, ",")
		else
			luo_magazzino.is_cod_depositi_in = ""
		end if
		li_return = luo_magazzino.uof_saldo_prod_date_decimal(		ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", &
																						ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
		destroy luo_magazzino
		//********************************************************************
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		
		//ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]	giacenza totale in tutti gli stock
		// ld_quant_val			[1]=quan_inizio_anno,  	[2]=val_inizio_anno,  
		// 							[3]=quan_ultima_chius, 	[4]=qta_entrata, 			[5]=val_entrata,	[6]=qta_uscita, 	[7]=val_uscita, 
		//							[8]=qta_acq, 				[9]=val_acq, 				[10]=qta_ven, 		[11]=val_ven
		choose case ls_flag_visualizza_val
			case "S"
				// costo standard --------------------------------------------------------------------------------------------------------
				if ls_flag_tipo_valorizzazione = "S" then	
					select costo_standard
					into :ld_costo_standard
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda
					  and cod_prodotto = :ls_cod_prodotto;
					if sqlca.sqlcode <>0 then
						g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti")
						return -1
					end if
					ld_costo = ld_costo_standard
				end if
				
				//ultimo costo acquisto -------------------------------------------------------------------------------------------------
				if ls_flag_tipo_valorizzazione = "A" then
					// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
					if ld_quant_val[14] >0 then
						ld_costo = ld_quant_val[14]
					else
						ld_costo = 0
					end if
				end if
				
				//costo medio ----------------------------------------------------------------------------------------------------------
				if ls_flag_tipo_valorizzazione = "M" then	
					if ld_quant_val[12] > 0 then
						ld_costo = ld_quant_val[13] / ld_quant_val[12]
					else
						select costo_medio_ponderato
						into   :ld_costo
						from   lifo
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 anno_lifo = (select max(anno_lifo)
												  from   lifo
												  where  cod_azienda = :s_cs_xx.cod_azienda and
															cod_prodotto = :ls_cod_prodotto);
																
						if sqlca.sqlcode < 0 then
							g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
							return -1
						elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
							ld_costo = 0
						end if
					end if
				end if
				//dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo)
			
			// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti ------------------------------------
			case "N"	
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					//dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
					ld_costo = ld_nulla
			
			//Valore invisibile ---------------------------------------------------------------
			case "X"
				dw_report.object.valore_t.visible = 0
				dw_report.object.linea.visible = 0
				dw_report.object.t_1.visible = 0
				dw_report.object.totale.visible = 0
				dw_report.object.costo_unitario_t.visible = 0
				dw_report.object.valore_t.visible = 0
				dw_report.object.valore_riga.visible = 0
				dw_report.object.rd_costo_unitario.visible = 0
				
				dw_report.object.cf_rd_giacenza.visible = 0
				dw_report.object.cf_costo_unitario.visible = 0
				dw_report.object.cf_valore_gruppo.visible = 0
		end choose
		
		ld_costo_unitario = ld_costo
		
		
		ll_num_stock = upperbound(ls_chiave[])
		
		for ll_j = 1 to ll_num_stock
			
			if isnull(ls_cod_deposito) or ls_cod_deposito = "" or upperbound(ls_cod_depositi_array) = 0 then
				//tutti i depositi recuperati dalla uof_saldo_prod_date_decimal ...
			else
				//solo i depositi specificati dal filtro ...
				if guo_functions.uof_in_array(ls_chiave[ll_j], ls_cod_depositi_array) then
				else
					//non nel filtro
					continue
				end if
			end if
			
			ll_newrow = dw_report.insertrow(0) 
			dw_report.setitem(ll_newrow, "rs_cod_prodotto", ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione", ls_des_prodotto)
			dw_report.setitem(ll_newrow, "cod_misura_mag_raggruppo", ls_cod_misura_mag)
			dw_report.setitem(ll_newrow, "rs_chiave", ls_chiave[ll_j])
			dw_report.setitem(ll_newrow, "rd_giacenza", ld_giacenza_stock[ll_j])
			dw_report.setitem(ll_newrow, "rd_costo_unitario", ld_costo_unitario)
			
			dw_report.setitem(ll_newrow, "cod_prodotto_raggruppo" , ls_cod_prodotto_raggruppato)
			dw_report.setitem(ll_newrow, "f_conv" , ld_fat_conversione_rag_mag)
			dw_report.setitem(ll_newrow, "des_prod_raggruppo" , ls_des_prodotto_raggruppato)
			
			//se hai richiesto di non visualizzare le giacenze nulle (o negative)
			//evita di includere nella somma, altrimenti il totale si sballa (in quanto decrementa il valore ma non lo include poi nella lista)
			if ls_flag_visulizza_giac_zero = "N" and (ld_giacenza_stock[ll_j]<0 or ld_costo_unitario<0) then
				//non includere
			else
				ld_gz_x_cm += ld_giacenza_stock[ll_j] * ld_costo_unitario
				ld_gz_fconv += ld_giacenza_stock[ll_j] * ld_fat_conversione_rag_mag
			end if
		next
	next

	if ld_gz_fconv<>0 then
		ld_tot_valore = ld_gz_x_cm / ld_gz_fconv
	else
		ld_tot_valore = 0
	end if
	
	dw_report.object.valore_totale_t.text = string(ld_gz_x_cm, "###,###,###,##0.00")
	
	
	ls_sort = "rs_chiave, rs_cod_prodotto"

	dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
	dw_report.object.titolo.width = 3223

end if		// fine caso dettaglio_stock = N e dettaglio_deposito = S

string ls_filtro

if ls_flag_visulizza_giac_zero = "N" then
	ls_filtro = "rd_giacenza > 0"
	dw_report.SetFilter(ls_filtro)
	dw_report.Filter()
	ll_num_righe = dw_report.rowcount()
end if

dw_report.object.intestazione.text = ls_intestazione
dw_report.object.costo_unitario_t.text = ls_costo_unitario_t
dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'
dw_report.SetSort(ls_sort)
dw_report.Sort()
dw_report.groupcalc()

dw_folder.fu_selecttab(2)
dw_report.Change_DW_Current()


return 0
end function

on w_inventario_magazzino_euro.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_report=create dw_report
this.pb_1=create pb_1
this.dw_selezione=create dw_selezione
this.dw_cod_deposito=create dw_cod_deposito
this.cb_report=create cb_report
this.cb_annulla=create cb_annulla
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.pb_1
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_cod_deposito
this.Control[iCurrent+6]=this.cb_report
this.Control[iCurrent+7]=this.cb_annulla
this.Control[iCurrent+8]=this.sle_1
end on

on w_inventario_magazzino_euro.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_report)
destroy(this.pb_1)
destroy(this.dw_selezione)
destroy(this.dw_cod_deposito)
destroy(this.cb_report)
destroy(this.cb_annulla)
destroy(this.sle_1)
end on

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW( dw_selezione, &
	"cod_cat_mer", &
	sqlca, &
	"tab_cat_mer", &
	"cod_cat_mer", &
	"des_cat_mer", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


//f_PO_LoadDDDW_DW( dw_selezione, &
//	"cod_deposito", &
//	sqlca, &
//	"anag_depositi", &
//	"cod_deposito", &
//	"des_deposito", &
//	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) AND flag_tipo_deposito<>'C' ")
dw_cod_deposito.uof_set_column("cod_deposito, des_deposito, flag_tipo_deposito", "anag_depositi", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", + &
											"cod_deposito")
//dw_selezione.event itemchanged(dw_selezione.getrow(), dw_selezione.object.flag_includi_cdf, "N") //flag_includi_cdf
dw_cod_deposito.event post losefocus()
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report
//

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_annulla
l_objects[4] = sle_1
l_objects[5] = pb_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_selezione.Object.path.Protect=1

pb_1.picturename = s_cs_xx.risorse + "\MENUVOCA.BMP"
pb_1.disabledname = s_cs_xx.risorse + "\MENUVOCA.BMP"
pb_1.enabled = false

dw_cod_deposito.visible = false
dw_cod_deposito.uof_set_parent_dw( dw_selezione, "cod_deposito" )

end event

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170
end event

type dw_folder from u_folder within w_inventario_magazzino_euro
integer x = 9
integer y = 12
integer width = 3968
integer height = 2352
integer taborder = 90
boolean border = false
end type

type dw_report from uo_cs_xx_dw within w_inventario_magazzino_euro
integer x = 46
integer y = 124
integer width = 3913
integer height = 2232
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_dw_report_inventario_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type pb_1 from picturebutton within w_inventario_magazzino_euro
integer x = 2327
integer y = 828
integer width = 101
integer height = 84
integer taborder = 13
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
string disabledname = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I

value = GetFileOpenName("Selezione", + docname, named, "*.*", "Tutti i tipi (*.*),*.*")
if value < 1 then return

ll_pos = 0
ll_pos_old = 1
do while true
	ll_pos = pos(docname,"\",ll_pos_old)
	if ll_pos < 1 then exit
	ll_pos_old = ll_pos + 1
loop
		
if ll_pos_old > 0 then
	dw_selezione.setitem(dw_selezione.getrow(),"path",left(docname, ll_pos_old - 1))
end if		
end event

type dw_selezione from uo_cs_xx_dw within w_inventario_magazzino_euro
integer x = 41
integer y = 124
integer width = 3835
integer height = 1744
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_dw_selezione_rep_inventario"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_nomecol, ls_nulla, ls_where

setnull(ls_nulla)

choose case i_colname
		
	case "cod_deposito"
//		iuo_dddw_depositi.event post uoe_read_from_parent()
		
	case "cod_prodotto_da"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		if isnull(getitemstring(getrow(), "cod_prodotto_a")) then
			setitem(getrow(), "cod_prodotto_a", i_coltext)
		end if
		
	case "cod_prodotto_a"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		
	case "cod_cat_mer"
		setitem(getrow(), "cod_prodotto_da", ls_nulla)
		setitem(getrow(), "cod_prodotto_a", ls_nulla)
		
	case "flag_det_depositi"
		if i_coltext = "N" then
			setitem(getrow(), "cod_deposito", ls_nulla)
		else
			setitem(getrow(), "flag_det_stock", "N")
		end if
		
	case "flag_visualizza_val"
		if i_coltext = "N" then
			setitem(getrow(), "flag_tipo_valorizzazione", "N")
		end if
		if i_coltext = "S" then
			setitem(getrow(), "flag_tipo_valorizzazione", "S")
		end if

	case "flag_tipo_valorizzazione"
		if i_coltext = "C" then
			dw_selezione.Object.path.Protect=0
			pb_1.enabled=true
			setitem(getrow(),"flag_det_stock","N")
			dw_selezione.Object.flag_det_stock.Protect=1
			setitem(getrow(),"flag_det_depositi","N")
			dw_selezione.Object.flag_det_depositi.Protect=1
		else
			setitem(getrow(), "path", "")
			dw_selezione.Object.path.Protect=1
			pb_1.enabled=false
			dw_selezione.Object.flag_det_stock.Protect=0
			dw_selezione.Object.flag_det_depositi.Protect=0
		end if
		
	case "flag_det_stock"
		if i_coltext = "S" then
			setitem(getrow(), "flag_det_depositi", "N")
		end if
		
	case "flag_includi_cdf"
		dw_cod_deposito.uof_deselect_all_rows( )
		
		choose case data
			case "S"
				ls_where = " flag_tipo_deposito='C'"

			case "N"
				ls_where = " flag_tipo_deposito <> 'C' "
					
			case else
				ls_where = ""
				
		end choose
		
		dw_cod_deposito.setfilter(ls_where)
		dw_cod_deposito.filter()
		dw_cod_deposito.setsort("cod_deposito")
		dw_cod_deposito.sort()
		dw_cod_deposito.uof_select_all_rows()
		dw_cod_deposito.event losefocus()

		
end choose
end event

event pcd_new;call super::pcd_new;dw_selezione.setitem(1,"data_inventario",datetime(today()))
//dw_selezione.setitem(1,"flag_visualizza_val","S")
//dw_selezione.setitem(1,"flag_tipo_valorizzazione","S")



end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "cod_deposito"
		dw_cod_deposito.show()
		
end choose
end event

type dw_cod_deposito from uo_dddw_checkbox within w_inventario_magazzino_euro
integer x = 571
integer y = 744
integer width = 1714
integer height = 696
integer taborder = 50
boolean bringtotop = true
end type

type cb_report from commandbutton within w_inventario_magazzino_euro
integer x = 1641
integer y = 1616
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string				ls_cod_prodotto_da, ls_cod_cat_mer_1, ls_flag_bloccato, ls_where, ls_error, ls_cod_prodotto, ls_messaggio,&
					ls_cod_misura_mag, ls_cod_cat_mer, ls_flag_blocco, ls_sort, ls_sql, ls_cod_prodotto_a, &
					ls_des_prodotto, ls_flag_det_depositi, ls_cod_deposito, ls_flag_det_stock, &
					ls_flag_visualizza_val, ls_flag_tipo_valorizzazione, ls_chiave[], ls_cod_deposito_selezione, &
					ls_intestazione, ls_des_azienda, ls_flag_visulizza_giac_zero, ls_costo_unitario_t,  ls_pks[], &
					ls_vettore_nullo[],ls_flag_fiscale, ls_flag_classe, ls_flag_escludi_cdf, ls_array[], ls_cod_depositi_array[], ls_flag_lifo, &
					ls_flag_attiva_raggruppo, ls_array_elementi[],ls_locale,ls_corsia, ls_scaffale, ls_colonna,ls_ripiano, ls_posizione
					
integer			li_return, li_cont
long				ll_i, li_count, ll_newrow, ll_num_righe, ll_num_stock, ll_j, ll_ret, ll_elemento_prog_stock

dec{4}			ld_saldo_quan_inizio_anno, ld_quant_val[], ld_giacenza, ld_totale_quantita, ld_costo_medio, ld_quan_acq, ld_val_inizio_anno, &
					ld_costo_standard, ld_prezzo_acquisto, ld_costo, ld_nulla, ld_giacenza_stock[], ld_costo_unitario, ld_vettore_nullo[], &
					ld_costo_medio_continuo,ld_costo_medio_continuo_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
					
datetime			ldt_data_a, ldt_data_blocco, ldt_data_chiusura_annuale, ldt_elemento_data_stock

uo_costo_medio_continuo luo_costo_medio
uo_magazzino luo_magazzino

dw_selezione.AcceptText()

ls_flag_attiva_raggruppo = dw_selezione.getItemstring(1,"flag_attiva_raggruppo")

//se hai attivato il report con raggruppamenti per codice raggruppo allora chiama la funzione apposita
//il raggruppio si attiva solo per dettaglio depositi
//if ls_flag_attiva_raggruppo="S" and (ls_flag_det_depositi="S"  or (ls_flag_det_depositi="N" and ls_flag_det_stock="N")) then
if ls_flag_attiva_raggruppo="S" then
	wf_report_raggruppo()
	return
end if
//------------------------------------------------------------------------------------------------------------------


// datetime ldt_data_a è la data di inventario
dw_report.reset()
dw_report.setredraw(false)
ib_interrupt = false


select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;

ldt_data_a = dw_selezione.getItemDatetime(1,"data_inventario")

if ldt_data_a < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if

setnull(ld_nulla)

select rag_soc_1
into :ls_des_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;
if not isnull(ls_des_azienda) then ls_intestazione = ls_des_azienda

ls_cod_prodotto_da = dw_selezione.getItemString(1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getItemString(1, "cod_prodotto_a")
ls_cod_cat_mer_1 = dw_selezione.getItemString(1, "cod_cat_mer")
ldt_data_a = dw_selezione.getItemDatetime(1, "data_inventario")

ls_flag_det_depositi = dw_selezione.getItemString(1, "flag_det_depositi")
ls_flag_det_stock = dw_selezione.getItemString(1, "flag_det_stock")

// stefanop 06/06/2012 - messa drop multipla
//ls_cod_deposito = dw_selezione.getItemString(1, "cod_deposito")
ls_cod_deposito = dw_cod_deposito.uof_get_selected_column("cod_deposito", false)
g_str.explode(ls_cod_deposito, ",", ls_cod_depositi_array)
//iuo_dddw_depositi.uof_get_selected_key(ls_cod_depositi_array)
//// ----
ls_cod_deposito_selezione = dw_cod_deposito.uof_get_selected_column("cod_deposito", true)

// stefanop 02/07/2012: aggiunto flag_lifo beatrice
ls_flag_lifo = dw_selezione.getItemString(1, "flag_lifo")
if ls_flag_lifo="" or isnull(ls_flag_lifo) then ls_flag_lifo = "T"

ls_flag_visualizza_val = dw_selezione.getItemString(1, "flag_visualizza_val")
ls_flag_tipo_valorizzazione = dw_selezione.getItemString(1, "flag_tipo_valorizzazione")
ls_flag_visulizza_giac_zero = dw_selezione.getItemString(1, "flag_visulizza_giac_zero")

ls_flag_fiscale = dw_selezione.getItemString(1, "flag_fiscale")
ls_flag_classe = dw_selezione.getItemString(1, "flag_classe")
ls_flag_escludi_cdf = dw_selezione.getItemString(1, "flag_includi_cdf")
if isnull(ls_flag_det_depositi) then ls_flag_det_depositi = "N"
if isnull(ls_flag_det_stock) then ls_flag_det_stock = "N"
if isnull(ls_flag_visulizza_giac_zero) then ls_flag_visulizza_giac_zero = "N"

if isnull(ls_flag_fiscale) or ls_flag_fiscale="" then ls_flag_fiscale = "T"

if isnull(ls_flag_classe) or len(ls_flag_classe) < 1 then ls_flag_classe = "X"

if (not isnull(ls_cod_deposito)) and (ls_cod_deposito<>"") and ls_flag_det_depositi = "N" then
	g_mb.error("Attenzione; se è stato selezionato uno o più depositi è obbligatorio attivare anche il check 'Dettaglio Depositi' ")
	return -1
end if


if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then	// devo selezionare ls_cod_prodotto_da e ls_cod_prodotto_a
	if (isnull(ls_cod_prodotto_da) or ls_cod_prodotto_da="") or (isnull(ls_cod_prodotto_a)  or ls_cod_prodotto_a="") then
		g_mb.messagebox("Apice", "Selezionare sia cod._prodotto_da, sia cod._prodotto_a")
		return -1
	else
		ls_intestazione = ls_intestazione + "; prodotti da:" + ls_cod_prodotto_da + &
		" a:" + ls_cod_prodotto_a + "; "
	end if
else
	ls_intestazione = ls_intestazione + "; cat. merceologica:" + ls_cod_cat_mer_1 + "; "
end if

if (not isnull(ls_cod_deposito)) and (ls_cod_deposito<>"") then
	ls_intestazione = ls_intestazione + " deposito:" + dw_selezione.getitemstring(1, "cod_deposito") + "; "
end if

if ls_flag_lifo = "S" then
	ls_intestazione += " Lifo: Si;"
elseif ls_flag_lifo = "N" then
	ls_intestazione += " Lifo: No;"
end if

choose case ls_flag_visualizza_val
		
	case "N", "X"
		ls_flag_tipo_valorizzazione = "N"
		
	case "S"
		
		CHOOSE CASE ls_flag_tipo_valorizzazione
			CASE "S"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Standard"
				ls_costo_unitario_t = "Costo Std."
			CASE "M"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Medio"
				ls_costo_unitario_t = "Costo Medio"
			CASE "A"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Acquisto"
				ls_costo_unitario_t = "Costo Acq."
			CASE "N"
				ls_costo_unitario_t = "Costo "
			CASE "C"
				ls_costo_unitario_t = "Costo Medio Continuo"
		END CHOOSE
	
end choose

if ls_flag_det_stock ="N" and ls_flag_det_depositi = "N" then
	// caso dettaglio_stock = N e dettaglio_deposito = N
	
	dw_report.DataObject = 'd_dw_report_inventario_euro'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
	dw_report.object.linea.visible = 0
	if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
		ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
					" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "
	else
		ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
	end if

	if ls_flag_fiscale="N" or ls_flag_fiscale="S" then
		ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
	end if
					
	if ls_flag_classe <> "X" then
		ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
	end if
	
	if ls_flag_lifo="N" or ls_flag_lifo="S" then
		ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "
	end if
	
	ls_sql += " order by cod_prodotto "
	
	DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql;
	OPEN DYNAMIC cur_1 ;
	
	DO while true
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
		FETCH cur_1 INTO :ls_cod_prodotto, :ls_des_prodotto, :ls_cod_misura_mag;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotti:~r~n" + SQLCA.SQLErrText)
			close cur_1;
			rollback;
			return -1
		end if
		
		select count(*)
		into :ll_num_stock
		from stock
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_prodotto = :ls_cod_prodotto;
		if ll_num_stock > 0 then		// elaboro solo prodotti in stock
			ll_newrow = dw_report.insertrow(0) 
			dw_report.ScrollToRow(ll_newrow)
			dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
			dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
		end if
	LOOP
	CLOSE cur_1 ;
	
	ll_num_righe = dw_report.rowcount()
	if ls_flag_tipo_valorizzazione = "C" then	
		luo_costo_medio = CREATE uo_costo_medio_continuo 
	end if
	for ll_i = 1 to ll_num_righe
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
	
		ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
		if ls_flag_tipo_valorizzazione = "C" then		// costo medio continuo (30/12/2002 Realizzato per colombin - EnMe)
			ll_ret = luo_costo_medio.uof_costo_medio_continuo(ls_cod_prodotto, ldt_data_a, ref ld_giacenza, ref ld_costo_medio_continuo, ref ls_chiave[], ref ld_giacenza_stock[], ref ld_costo_medio_continuo_stock[],dw_selezione.getitemstring(1, "path") ,ref ls_messaggio)
			if ll_ret = -1 then
				g_mb.messagebox("Inventario", ls_messaggio)
				destroy luo_costo_medio
				return -1
			end if	
			
			sle_1.text= "prodotto :" + ls_cod_prodotto
			dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
			
			choose case ls_flag_visualizza_val
				case "S"
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo_medio_continuo)
					
				case "N"	// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
					
				case "X"
					dw_report.object.valore_t.visible = 0
					dw_report.object.linea.visible = 0
					dw_report.object.t_1.visible = 0
					dw_report.object.totale.visible = 0
					dw_report.object.costo_unitario_t.visible = 0
					dw_report.object.valore_t.visible = 0
					dw_report.object.valore_riga.visible = 0
					dw_report.object.rd_costo_unitario.visible = 0
			end choose
			
		else
			for li_cont = 1 to 14
				ld_quant_val[li_cont] = 0
			next
			
			sle_1.text= "prodotto :" + ls_cod_prodotto
			
			luo_magazzino = CREATE uo_magazzino
			luo_magazzino.is_considera_depositi_fornitori = "I" //ls_flag_escludi_cdf
			
			li_count = dw_cod_deposito.uof_get_selected_column_as_array("cod_deposito", ls_pks, true)
			if li_count > 0 then
				 luo_magazzino.is_cod_depositi_in =  g_str.implode(ls_pks, ",")
			else
				luo_magazzino.is_cod_depositi_in = ""
			end if
			
			li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
			
			destroy luo_magazzino
			
			if li_return <0 then
				g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
				return -1
			end if
			
			ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
			
			// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
			// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
			// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
			
			choose case ls_flag_visualizza_val
				case "S"
					if ls_flag_tipo_valorizzazione = "S" then	// costo standard
						select costo_standard
						into :ld_costo_standard
						from anag_prodotti
						where cod_azienda = :s_cs_xx.cod_azienda
						  and cod_prodotto = :ls_cod_prodotto;
						if sqlca.sqlcode <>0 then
							g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti")
							return -1
						end if
						ld_costo = ld_costo_standard
					end if
					if ls_flag_tipo_valorizzazione = "A" then	//ultimo costo acquisto
						// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
						if ld_quant_val[14] >0 then
							ld_costo = ld_quant_val[14]
						else
							ld_costo = 0
						end if
					end if
					if ls_flag_tipo_valorizzazione = "M" then	//costo medio di acquisto
						if ld_quant_val[12] > 0 then
							ld_costo = ld_quant_val[13] / ld_quant_val[12]
						else
							
							select costo_medio_ponderato
							into   :ld_costo
							from   lifo
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_prodotto and
									 anno_lifo = (select max(anno_lifo)
													  from   lifo
													  where  cod_azienda = :s_cs_xx.cod_azienda and
																cod_prodotto = :ls_cod_prodotto);
																	
							if sqlca.sqlcode < 0 then
								g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
								return -1
							elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
								ld_costo = 0
							end if
							
						end if
					end if
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo)
					
				case "N"		// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
					
				case "X"
					dw_report.object.valore_t.visible = 0
					dw_report.object.linea.visible = 0
					dw_report.object.t_1.visible = 0
					dw_report.object.totale.visible = 0
					dw_report.object.costo_unitario_t.visible = 0
					dw_report.object.valore_t.visible = 0
					dw_report.object.valore_riga.visible = 0
					dw_report.object.rd_costo_unitario.visible = 0
			end choose
		end if
	next
	if ls_flag_tipo_valorizzazione = "C" then	
		destroy luo_costo_medio
	end if

	ls_sort = "rs_cod_prodotto a"
	dw_report.SetSort(ls_sort)
	dw_report.Sort()
	dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
	dw_report.object.titolo.width = 3223

end if		// fine caso dettaglio_stock = N e dettaglio_deposito = N


if ls_flag_det_stock ="S" and ls_flag_det_depositi = "N" then
	// caso dettaglio_stock = S e dettaglio_deposito = N
	
	dw_report.DataObject = 'd_dw_report_inventario_stock_euro'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
	dw_report.object.linea.visible = 0
	dw_report.object.rs_chiave_t.text = "Stock"
	if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
		ls_sql =  " select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
					" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "
	else
		ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
	end if
					
	if ls_flag_fiscale="N" or ls_flag_fiscale="S" then
		ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
	end if
	
	if ls_flag_classe <> "X" then
		ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
	end if
	
	if ls_flag_lifo="N" or ls_flag_lifo="S" then
		ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "
	end if
	
	DECLARE cur_2 DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql;
	OPEN DYNAMIC cur_2 ;
	
	DO while true
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
		FETCH cur_2 INTO :ls_cod_prodotto, :ls_des_prodotto, :ls_cod_misura_mag;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotti:~r~n" +SQLCA.SQLErrText)
			close cur_2;
			rollback;
			return -1
		end if
		sle_1.text= "prodotto :" + ls_cod_prodotto
		
		select count(*)
		into :ll_num_stock
		from stock
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_prodotto = :ls_cod_prodotto;
		if ll_num_stock > 0 then		// elaboro solo prodotti in stock
			ll_newrow = dw_report.insertrow(0) 
			dw_report.ScrollToRow(ll_newrow)
			dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
			dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
		end if
	LOOP
	CLOSE cur_2 ;
	
	ll_num_righe = dw_report.rowcount()
	for ll_i = 1 to ll_num_righe
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
	
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next
		ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
		ls_des_prodotto = dw_report.getitemstring(ll_i, "rs_descrizione")
		ls_cod_misura_mag = dw_report.getitemstring(ll_i,  "rs_cod_misura_mag")
		sle_1.text= "prodotto :" + ls_cod_prodotto
		
		ls_chiave = ls_vettore_nullo
		ld_giacenza_stock = ld_vettore_nullo
		
		luo_magazzino = CREATE uo_magazzino
		luo_magazzino.is_considera_depositi_fornitori = ls_flag_escludi_cdf
		
		li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "S", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
		
		destroy luo_magazzino
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		
//		ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]	giacenza totale in tutti gli stock
//		dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
		// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
		// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
		// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
		
		choose case ls_flag_visualizza_val
			case "S"
				if ls_flag_tipo_valorizzazione = "S" then	// costo standard
					select costo_standard
					into :ld_costo_standard
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda
					  and cod_prodotto = :ls_cod_prodotto;
					if sqlca.sqlcode <>0 then
						g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti")
						return -1
					end if
					ld_costo = ld_costo_standard
				end if
				if ls_flag_tipo_valorizzazione = "A" then	//ultimo costo acquisto
					// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
					if ld_quant_val[14] >0 then
						ld_costo = ld_quant_val[14]
					else
						ld_costo = 0
					end if
				end if
				if ls_flag_tipo_valorizzazione = "M" then	//costo medio
					if ld_quant_val[12] >0 then
						ld_costo = ld_costo_medio_stock[1]
					else
						
						select costo_medio_ponderato
						into   :ld_costo
						from   lifo
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 anno_lifo = (select max(anno_lifo)
												  from   lifo
												  where  cod_azienda = :s_cs_xx.cod_azienda and
															cod_prodotto = :ls_cod_prodotto);
																
						if sqlca.sqlcode < 0 then
							g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
							return -1
						elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
							ld_costo = 0
						end if
						
					end if
				end if
				dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo)
				
			case "N"		// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti inventario fisico
				dw_report.object.valore_t.text = "Quan. rilevata"
				dw_report.object.linea.visible = 1
				dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
				dw_report.object.rs_posizione.visible = 1
				dw_report.object.rd_costo_unitario.visible = 0
				g_str.explode( ls_chiave[1], "-", ref ls_array_elementi[])
				
				ls_costo_unitario_t = "Posizione Articolo"
				
				ldt_elemento_data_stock = datetime( date(ls_array_elementi[4]), 00:00:00)
				ll_elemento_prog_stock = long(ls_array_elementi[5])
				
				select locale,
						corsia, 
						scaffale, 
						colonna,
						ripiano
				into	:ls_locale,
						:ls_corsia, 
						:ls_scaffale, 
						:ls_colonna,
						:ls_ripiano
				from stock
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_deposito = :ls_array_elementi[1] and
						cod_ubicazione = :ls_array_elementi[2] and
						cod_lotto = :ls_array_elementi[3] and
						data_stock = :ldt_elemento_data_stock and
						prog_stock = :ll_elemento_prog_stock;
						
				if sqlca.sqlcode = 0 then
					if g_str.isempty(ls_locale) then ls_locale = "//"
					if g_str.isempty(ls_corsia) then ls_corsia = "//"
					if g_str.isempty(ls_scaffale) then ls_scaffale = "//"
					if g_str.isempty(ls_colonna) then ls_colonna = "//"
					if g_str.isempty(ls_ripiano) then ls_ripiano = "//"
					ls_posizione = ls_locale + " - " + ls_corsia + " - " + ls_scaffale + " - " + ls_colonna + " - " +  + ls_ripiano
					dw_report.setitem(ll_i, "rs_posizione", ls_posizione)
				end if
				
			case "X"
				dw_report.object.valore_t.visible = 0
				dw_report.object.linea.visible = 0
				dw_report.object.t_1.visible = 0
				dw_report.object.totale.visible = 0
				dw_report.object.costo_unitario_t.visible = 0
				dw_report.object.valore_t.visible = 0
				dw_report.object.valore_riga.visible = 0
				dw_report.object.rd_costo_unitario.visible = 0
			
		end choose
		
		ll_num_stock = upperbound(ls_chiave[])
		dw_report.setitem(ll_i, "rs_chiave", ls_chiave[1])
		dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza_stock[1])
		dw_report.accepttext()
		
		
		ld_costo_unitario = dw_report.getitemnumber(ll_i, "rd_costo_unitario")
		if ll_num_stock > 1 then	// più stock
			for ll_j = 2 to (ll_num_stock)
				ll_newrow = dw_report.insertrow(ll_i + 1) 
				dw_report.ScrollToRow(ll_newrow)
				ll_num_righe = ll_num_righe + 1
				ll_i = ll_i +1
				dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
				dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
				dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
				dw_report.setitem(ll_newrow, "rs_chiave", ls_chiave[ll_j])
				dw_report.setitem(ll_newrow, "rd_giacenza", ld_giacenza_stock[ll_j])
				dw_report.setitem(ll_newrow, "rd_costo_unitario", ld_costo_medio_stock[ll_j])
			next
		end if

		if ls_costo_unitario_t = "Posizione Articolo" then
			ls_sort = "rs_posizione asc, rs_cod_prodotto asc, rs_chiave asc"
		else
			ls_sort = "rs_cod_prodotto a"
		end if
		dw_report.SetSort(ls_sort)
		dw_report.Sort()
		dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
		dw_report.object.titolo.width = 4618

	next
end if		// fine caso dettaglio_stock = S e dettaglio_deposito = N

if ls_flag_det_stock ="N" and ls_flag_det_depositi = "S" then
	// caso dettaglio_stock = N e dettaglio_deposito = S
	
	dw_report.DataObject = 'd_dw_report_inventario_deposito_euro'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
	
	dw_report.object.linea.visible = 0
	dw_report.object.rs_chiave_t.text = "Deposito"
	if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
		if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
			ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
						" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "
		else
			ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
		end if
		
		if ls_flag_fiscale="N" or ls_flag_fiscale="S" then
			ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
		end if
		
		if ls_flag_classe <> "X" then
			ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
		end if
		
		if ls_flag_lifo="N" or ls_flag_lifo="S" then
			ls_sql += " and flag_lifo='"+ls_flag_lifo+" "
		end if
	
	else
		if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
			
			ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
						" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "

			if ls_flag_fiscale="N" or ls_flag_fiscale="S" then
				ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
			end if
			
			if ls_flag_classe <> "X" then
				ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
			end if
	
			if ls_flag_lifo="N" or ls_flag_lifo="S" then
				ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "
			end if

			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto from stock " +&
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
//			if ls_cod_deposito <> "" then ls_sql += " AND cod_deposito IN (" + ls_cod_deposito + ") "
			if ls_cod_deposito <> "" then ls_sql += " AND " + ls_cod_deposito_selezione + " "
			ls_sql += ") "
		else
			ls_sql = " select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "

			if ls_flag_fiscale="N" or ls_flag_fiscale="S" then
				ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
			end if
			
			if ls_flag_classe <> "X" then
				ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
			end if
	
			if ls_flag_lifo="N" or ls_flag_lifo="S" then
				ls_sql += " and flag_lifo='"+ls_flag_lifo+"' "
			end if
			
			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto from mov_magazzino " +&
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
						
//			if ls_cod_deposito <> "" then ls_sql += " AND cod_deposito IN (" + ls_cod_deposito + ") "
			if ls_cod_deposito <> "" then ls_sql += " AND "+  ls_cod_deposito_selezione + " "
			ls_sql += ") "
		end if
		
	end if	

	datastore lds_prodotti
	string ls_errore_ds
	long ll_rows, ll_x
	
	if guo_functions.uof_crea_datastore(ref lds_prodotti, ls_sql, ref ls_errore_ds) < 0 then
		g_mb.error("Errore in create datastore lds_prodotti. ~r~n " + ls_errore_ds)
		rollback;
		return
	end if
	
	lds_prodotti.settransobject(sqlca)
	ll_rows = lds_prodotti.retrieve()
	
	
	for ll_x = 1 to ll_rows
		
		ls_cod_prodotto = lds_prodotti.getitemstring(ll_x,1)
		ls_des_prodotto =  lds_prodotti.getitemstring(ll_x,2)
		ls_cod_misura_mag = lds_prodotti.getitemstring(ll_x,3)
		
		sle_1.text= "prodotto :" + ls_cod_prodotto + " - " + ls_des_prodotto
		
		select count(*)
		into :ll_num_stock
		from stock
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_prodotto = :ls_cod_prodotto;
		if ll_num_stock > 0 then		// elaboro solo prodotti in stock
			ll_newrow = dw_report.insertrow(0) 
			dw_report.ScrollToRow(ll_newrow)
			dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
			dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
		end if
		
	next

	ll_num_righe = dw_report.rowcount()
	for ll_i = 1 to ll_num_righe
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
	
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next
		ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
		ls_des_prodotto = dw_report.getitemstring(ll_i, "rs_descrizione")
		ls_cod_misura_mag = dw_report.getitemstring(ll_i,  "rs_cod_misura_mag")
		sle_1.text= "prodotto :" + ls_cod_prodotto

		ls_chiave = ls_vettore_nullo
		ld_giacenza_stock = ld_vettore_nullo
		
		luo_magazzino = CREATE uo_magazzino
		luo_magazzino.is_considera_depositi_fornitori = "I" //ls_flag_escludi_cdf
		
		li_count = dw_cod_deposito.uof_get_selected_column_as_array("cod_deposito", ls_pks, true)
		if li_count > 0 then
			 luo_magazzino.is_cod_depositi_in =  g_str.implode(ls_pks, ",")
		else
			luo_magazzino.is_cod_depositi_in = ""
		end if
		
		li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

		destroy luo_magazzino
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		
//		ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]	giacenza totale in tutti gli stock
//		dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
		// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
		// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
		// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
		choose case ls_flag_visualizza_val
			case "S"
				if ls_flag_tipo_valorizzazione = "S" then	// costo standard
					select costo_standard
					into :ld_costo_standard
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda
					  and cod_prodotto = :ls_cod_prodotto;
					if sqlca.sqlcode <>0 then
						g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti")
						return -1
					end if
					ld_costo = ld_costo_standard
				end if
				if ls_flag_tipo_valorizzazione = "A" then	//ultimo costo acquisto
					// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
					if ld_quant_val[14] >0 then
						ld_costo = ld_quant_val[14]
					else
						ld_costo = 0
					end if
				end if
				if ls_flag_tipo_valorizzazione = "M" then	//costo medio
					if ld_quant_val[12] > 0 then
						ld_costo = ld_quant_val[13] / ld_quant_val[12]
					else
						
						select costo_medio_ponderato
						into   :ld_costo
						from   lifo
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 anno_lifo = (select max(anno_lifo)
												  from   lifo
												  where  cod_azienda = :s_cs_xx.cod_azienda and
															cod_prodotto = :ls_cod_prodotto);
																
						if sqlca.sqlcode < 0 then
							g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
							return -1
						elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
							ld_costo = 0
						end if
						
					end if
				end if
				dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo)
				
			case "N"	// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
					
			case "X"
				dw_report.object.valore_t.visible = 0
				dw_report.object.linea.visible = 0
				dw_report.object.t_1.visible = 0
				dw_report.object.totale.visible = 0
				dw_report.object.costo_unitario_t.visible = 0
				dw_report.object.valore_t.visible = 0
				dw_report.object.valore_riga.visible = 0
				dw_report.object.rd_costo_unitario.visible = 0
			end choose
		ll_num_stock = upperbound(ls_chiave[])
		// devo scrivere tutti depositi 
		//if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
		if isnull(ls_cod_deposito) or ls_cod_deposito = "" or upperbound(ls_cod_depositi_array) > 1 then
			dw_report.setitem(ll_i, "rs_chiave", ls_chiave[1])
			dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza_stock[1])
			dw_report.accepttext()
			
			ld_costo_unitario = dw_report.getitemnumber(ll_i, "rd_costo_unitario")
			if ll_num_stock > 1 then	// più depositi
				for ll_j = 2 to (ll_num_stock)
					ll_newrow = dw_report.insertrow(ll_i + 1) 
					dw_report.ScrollToRow(ll_newrow)
					ll_num_righe = ll_num_righe + 1
					ll_i = ll_i +1
					dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
					dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
					dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
					dw_report.setitem(ll_newrow, "rs_chiave", ls_chiave[ll_j])
					dw_report.setitem(ll_newrow, "rd_giacenza", ld_giacenza_stock[ll_j])
					dw_report.setitem(ll_newrow, "rd_costo_unitario", ld_costo_unitario)
				next
			end if
		else		// solo deposito indicato
			for ll_j = 1 to ll_num_stock
				// stefanop: 07/06/2012: array di depositi
				if guo_functions.uof_in_array(ls_chiave[ll_j], ls_cod_depositi_array) then
				//if ls_chiave[ll_j] = ls_cod_deposito then
					dw_report.setitem(ll_i, "rs_chiave", ls_chiave[ll_j])
					dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza_stock[ll_j])
				end if
			next
		end if
	next		// next ll_i
	
	ls_sort = "rs_chiave a, rs_cod_prodotto a"
	dw_report.SetSort(ls_sort)
	dw_report.Sort()
	dw_report.groupcalc()
	dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
	dw_report.object.titolo.width = 3223

end if		// fine caso dettaglio_stock = N e dettaglio_deposito = S

string ls_filtro

if ls_flag_visulizza_giac_zero = "N" then
	ls_filtro = "rd_giacenza > 0"
	dw_report.SetFilter(ls_filtro)
	dw_report.Filter()
	ll_num_righe = dw_report.rowcount()
end if

dw_report.object.intestazione.text = ls_intestazione

dw_report.object.costo_unitario_t.text = ls_costo_unitario_t
dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'
dw_report.SetSort(ls_sort)
dw_report.Sort()

dw_folder.fu_selecttab(2)
dw_report.Change_DW_Current()

end event

type cb_annulla from commandbutton within w_inventario_magazzino_euro
integer x = 2030
integer y = 1616
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;ib_interrupt = true

end event

type sle_1 from singlelineedit within w_inventario_magazzino_euro
integer x = 59
integer y = 1700
integer width = 2313
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

