﻿$PBExportHeader$w_mostra_buchi_documenti.srw
forward
global type w_mostra_buchi_documenti from window
end type
type st_2 from statictext within w_mostra_buchi_documenti
end type
type st_1 from statictext within w_mostra_buchi_documenti
end type
type cb_annulla from commandbutton within w_mostra_buchi_documenti
end type
type cb_ok from commandbutton within w_mostra_buchi_documenti
end type
type mle_date from multilineedit within w_mostra_buchi_documenti
end type
type mle_numerazione from multilineedit within w_mostra_buchi_documenti
end type
end forward

global type w_mostra_buchi_documenti from window
integer width = 4233
integer height = 2492
boolean titlebar = true
string title = "Incongruenza Documenti per numerazione o data"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_2 st_2
st_1 st_1
cb_annulla cb_annulla
cb_ok cb_ok
mle_date mle_date
mle_numerazione mle_numerazione
end type
global w_mostra_buchi_documenti w_mostra_buchi_documenti

on w_mostra_buchi_documenti.create
this.st_2=create st_2
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.mle_date=create mle_date
this.mle_numerazione=create mle_numerazione
this.Control[]={this.st_2,&
this.st_1,&
this.cb_annulla,&
this.cb_ok,&
this.mle_date,&
this.mle_numerazione}
end on

on w_mostra_buchi_documenti.destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.mle_date)
destroy(this.mle_numerazione)
end on

event open;s_cs_xx_parametri			lstr_parametri


lstr_parametri = message.powerobjectparm


mle_numerazione.text = lstr_parametri.parametro_s_1_a[1]
mle_date.text = lstr_parametri.parametro_s_1_a[2]
end event

type st_2 from statictext within w_mostra_buchi_documenti
integer x = 2149
integer y = 52
integer width = 2043
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "ELENCO DOCUMENTI CON INCONGRUENZE NELLE DATE"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_mostra_buchi_documenti
integer x = 37
integer y = 52
integer width = 2043
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "ELENCO DOCUMENTI CON BUCHI NELLA NUMERAZIONE FISCALE"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_mostra_buchi_documenti
integer x = 2322
integer y = 2292
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;

closewithreturn(parent, "KO")
end event

type cb_ok from commandbutton within w_mostra_buchi_documenti
integer x = 1399
integer y = 2288
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prosegui"
boolean default = true
end type

event clicked;

closewithreturn(parent, "OK")
end event

type mle_date from multilineedit within w_mostra_buchi_documenti
integer x = 2149
integer y = 128
integer width = 2043
integer height = 2100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type mle_numerazione from multilineedit within w_mostra_buchi_documenti
integer x = 27
integer y = 128
integer width = 2043
integer height = 2100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

