﻿$PBExportHeader$w_registro_clavorazione.srw
forward
global type w_registro_clavorazione from w_cs_xx_principale
end type
type dw_sel_clavorazione_2 from uo_cs_xx_dw within w_registro_clavorazione
end type
type rb_2 from radiobutton within w_registro_clavorazione
end type
type rb_1 from radiobutton within w_registro_clavorazione
end type
type cb_report2 from commandbutton within w_registro_clavorazione
end type
type st_2 from statictext within w_registro_clavorazione
end type
type st_1 from statictext within w_registro_clavorazione
end type
type dw_registro_clavorazione_2 from uo_cs_xx_dw within w_registro_clavorazione
end type
type dw_registro_clavorazione_1 from uo_cs_xx_dw within w_registro_clavorazione
end type
type cb_report from commandbutton within w_registro_clavorazione
end type
type dw_sel_clavorazione_1 from uo_cs_xx_dw within w_registro_clavorazione
end type
type gb_1 from groupbox within w_registro_clavorazione
end type
type gb_2 from groupbox within w_registro_clavorazione
end type
end forward

global type w_registro_clavorazione from w_cs_xx_principale
integer width = 3470
integer height = 2028
string title = "Report Controllo Scarichi"
boolean maxbox = false
boolean resizable = false
dw_sel_clavorazione_2 dw_sel_clavorazione_2
rb_2 rb_2
rb_1 rb_1
cb_report2 cb_report2
st_2 st_2
st_1 st_1
dw_registro_clavorazione_2 dw_registro_clavorazione_2
dw_registro_clavorazione_1 dw_registro_clavorazione_1
cb_report cb_report
dw_sel_clavorazione_1 dw_sel_clavorazione_1
gb_1 gb_1
gb_2 gb_2
end type
global w_registro_clavorazione w_registro_clavorazione

type variables
datetime idt_da, idt_a
end variables

on w_registro_clavorazione.create
int iCurrent
call super::create
this.dw_sel_clavorazione_2=create dw_sel_clavorazione_2
this.rb_2=create rb_2
this.rb_1=create rb_1
this.cb_report2=create cb_report2
this.st_2=create st_2
this.st_1=create st_1
this.dw_registro_clavorazione_2=create dw_registro_clavorazione_2
this.dw_registro_clavorazione_1=create dw_registro_clavorazione_1
this.cb_report=create cb_report
this.dw_sel_clavorazione_1=create dw_sel_clavorazione_1
this.gb_1=create gb_1
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_clavorazione_2
this.Control[iCurrent+2]=this.rb_2
this.Control[iCurrent+3]=this.rb_1
this.Control[iCurrent+4]=this.cb_report2
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.dw_registro_clavorazione_2
this.Control[iCurrent+8]=this.dw_registro_clavorazione_1
this.Control[iCurrent+9]=this.cb_report
this.Control[iCurrent+10]=this.dw_sel_clavorazione_1
this.Control[iCurrent+11]=this.gb_1
this.Control[iCurrent+12]=this.gb_2
end on

on w_registro_clavorazione.destroy
call super::destroy
destroy(this.dw_sel_clavorazione_2)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.cb_report2)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_registro_clavorazione_2)
destroy(this.dw_registro_clavorazione_1)
destroy(this.cb_report)
destroy(this.dw_sel_clavorazione_1)
destroy(this.gb_1)
destroy(this.gb_2)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_noenablepopup)

dw_sel_clavorazione_1.set_dw_options(sqlca, &
													  pcca.null_object, &
													  c_newonopen + c_nomodify + c_nodelete + c_noretrieveonopen, &
													  c_nohighlightselected)
													  
dw_sel_clavorazione_2.set_dw_options(sqlca, &
													  pcca.null_object, &
													  c_newonopen + c_nomodify + c_nodelete + c_noretrieveonopen, &
													  c_nohighlightselected)

dw_registro_clavorazione_1.set_dw_options(sqlca, &
													 		pcca.null_object, &
													  		c_nonew + c_nomodify + c_nodelete + c_noretrieveonopen, &
													  		c_nohighlightselected)

dw_registro_clavorazione_2.set_dw_options(sqlca, &
													 		pcca.null_object, &
													  		c_nonew + c_nomodify + c_nodelete + c_noretrieveonopen, &
													  		c_nohighlightselected)
															  
dw_registro_clavorazione_1.object.datawindow.print.preview = 'Yes'
dw_registro_clavorazione_1.object.datawindow.print.preview.rulers = 'Yes'

dw_registro_clavorazione_2.object.datawindow.print.preview = 'Yes'
dw_registro_clavorazione_2.object.datawindow.print.preview.rulers = 'Yes'
end event

type dw_sel_clavorazione_2 from uo_cs_xx_dw within w_registro_clavorazione
integer x = 1669
integer y = 1040
integer height = 80
integer taborder = 50
string dataobject = "d_sel_clavorazione"
boolean border = false
end type

type rb_2 from radiobutton within w_registro_clavorazione
integer x = 46
integer y = 1052
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA"
borderstyle borderstyle = stylelowered!
end type

event clicked;rb_1.checked = false

rb_2.checked = true

dw_registro_clavorazione_2.change_dw_current()
end event

type rb_1 from radiobutton within w_registro_clavorazione
integer x = 46
integer y = 92
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA"
borderstyle borderstyle = stylelowered!
end type

event clicked;rb_1.checked = true

rb_2.checked = false

dw_registro_clavorazione_1.change_dw_current()
end event

type cb_report2 from commandbutton within w_registro_clavorazione
integer x = 3017
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;dw_sel_clavorazione_2.accepttext()

idt_da = dw_sel_clavorazione_2.getitemdatetime(1,"data_da")

if isnull(idt_da) then
	idt_da = datetime(date("01/01/1900"),00:00:00)
end if

idt_a = dw_sel_clavorazione_2.getitemdatetime(1,"data_a")

if isnull(idt_a) then
	idt_a = datetime(date("31/12/2999"),00:00:00)
end if

rb_1.checked = false

rb_2.checked = true

dw_registro_clavorazione_2.change_dw_current()

parent.triggerevent("pc_retrieve")
end event

type st_2 from statictext within w_registro_clavorazione
integer x = 389
integer y = 1040
integer width = 1280
integer height = 80
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Lavorazioni terzi presso MECO"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_1 from statictext within w_registro_clavorazione
integer x = 389
integer y = 80
integer width = 1280
integer height = 80
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Lavorazioni MECO presso terzi"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type dw_registro_clavorazione_2 from uo_cs_xx_dw within w_registro_clavorazione
integer x = 46
integer y = 1140
integer width = 3360
integer height = 760
integer taborder = 30
string dataobject = "d_registro_clavorazione_2"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,idt_da,idt_a)
end event

event clicked;call super::clicked;rb_1.checked = false

rb_2.checked = true

change_dw_current()
end event

type dw_registro_clavorazione_1 from uo_cs_xx_dw within w_registro_clavorazione
integer x = 46
integer y = 180
integer width = 3360
integer height = 760
integer taborder = 30
string dataobject = "d_registro_clavorazione_1"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,idt_da,idt_a)
end event

event clicked;call super::clicked;rb_1.checked = true

rb_2.checked = false

change_dw_current()
end event

type cb_report from commandbutton within w_registro_clavorazione
integer x = 3017
integer y = 80
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;dw_sel_clavorazione_1.accepttext()

idt_da = dw_sel_clavorazione_1.getitemdatetime(1,"data_da")

if isnull(idt_da) then
	idt_da = datetime(date("01/01/1900"),00:00:00)
end if

idt_a = dw_sel_clavorazione_1.getitemdatetime(1,"data_a")

if isnull(idt_a) then
	idt_a = datetime(date("31/12/2999"),00:00:00)
end if

rb_1.checked = true

rb_2.checked = false

dw_registro_clavorazione_1.change_dw_current()

parent.triggerevent("pc_retrieve")
end event

type dw_sel_clavorazione_1 from uo_cs_xx_dw within w_registro_clavorazione
integer x = 1669
integer y = 80
integer height = 80
integer taborder = 10
string dataobject = "d_sel_clavorazione"
boolean border = false
end type

type gb_1 from groupbox within w_registro_clavorazione
integer x = 23
integer y = 980
integer width = 3406
integer height = 940
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
borderstyle borderstyle = stylelowered!
end type

type gb_2 from groupbox within w_registro_clavorazione
integer x = 23
integer y = 20
integer width = 3406
integer height = 940
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
borderstyle borderstyle = stylelowered!
end type

