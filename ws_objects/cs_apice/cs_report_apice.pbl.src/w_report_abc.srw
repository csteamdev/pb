﻿$PBExportHeader$w_report_abc.srw
$PBExportComments$Report analisi abc prodotti
forward
global type w_report_abc from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_abc
end type
type cb_report from commandbutton within w_report_abc
end type
type cb_selezione from commandbutton within w_report_abc
end type
type dw_selezione from uo_cs_xx_dw within w_report_abc
end type
type dw_report from uo_cs_xx_dw within w_report_abc
end type
type cb_esci from commandbutton within w_report_abc
end type
type sle_1 from singlelineedit within w_report_abc
end type
end forward

global type w_report_abc from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Stampa analisi abc"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
cb_esci cb_esci
sle_1 sle_1
end type
global w_report_abc w_report_abc

type variables
boolean ib_interrupt
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2250
this.height = 670

end event

on w_report_abc.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_esci=create cb_esci
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.cb_esci
this.Control[iCurrent+7]=this.sle_1
end on

on w_report_abc.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_esci)
destroy(this.sle_1)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_cat_mer", &
							sqlca, &
							"tab_cat_mer", &
							"cod_cat_mer", &
                     "des_cat_mer", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type cb_annulla from commandbutton within w_report_abc
integer x = 1417
integer y = 420
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;ib_interrupt = true
end event

type cb_report from commandbutton within w_report_abc
integer x = 1806
integer y = 420
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_cat_mer, ls_intestazione, &
		 ls_sql, ls_cod_prodotto, ls_des_prodotto, ls_where, ls_error, ls_chiave[], &
		 ls_classe, ls_classi[], ls_where_prodotti		 
datetime ldt_data_da, ldt_data_a
long ll_num_stock, ll_newrow, ll_num_righe, ll_i, ll_j, ll_num_classi
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_consumi_1, ld_consumi_2, ld_consumi, &
		 ld_consumi_tot, ld_incidenza, ld_progressivo, ld_classi_da[], ld_classi_a[], &
		 ld_scaglione_da, ld_scaglione_a, ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
integer li_return
uo_magazzino luo_magazzino

dw_selezione.AcceptText()

// datetime ldt_data_a è la data di inventario
dw_report.reset()
ib_interrupt = false

ls_cod_prodotto_da = dw_selezione.getItemString(1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getItemString(1, "cod_prodotto_a")
ls_cod_cat_mer = dw_selezione.getItemString(1, "cod_cat_mer")
ldt_data_da = dw_selezione.getItemDatetime(1, "data_da")
ldt_data_a = dw_selezione.getItemDatetime(1, "data_a")

if isnull(ldt_data_da) or isnull(ldt_data_a) then
	g_mb.messagebox("Dati incompleti","Impostare entrambe le date!") 
	return -1
end if	

ls_intestazione = "Data: da " + string(ldt_data_da, "dd/mm/yyyy") + " a " + &
		string(ldt_data_a, "dd/mm/yyyy")

if not isnull(ls_cod_cat_mer) then
	ls_intestazione = ls_intestazione + " , Cat. merceologica:" + ls_cod_cat_mer + " "
end if

if isnull(ls_cod_prodotto_da) or ls_cod_prodotto_da = "" then
	if isnull(ls_cod_prodotto_a) or ls_cod_prodotto_a = "" then
		ls_where_prodotti = ""
		ls_intestazione = ls_intestazione + " , Prodotti : tutti"
	else
		ls_where_prodotti = " and cod_prodotto <= '" + ls_cod_prodotto_a + "' "		
		ls_intestazione = ls_intestazione + " , Prodotti: tutti fino a " + ls_cod_prodotto_a + " "
	end if	
else
	if isnull(ls_cod_prodotto_a) or ls_cod_prodotto_a = "" then
		ls_where_prodotti = " and cod_prodotto >= '" + ls_cod_prodotto_da + "' "		
		ls_intestazione = ls_intestazione + " , Prodotti: tutti a partire da " + ls_cod_prodotto_da + " "
	else	
		ls_where_prodotti = " and cod_prodotto between '" + ls_cod_prodotto_da + "' and '" + ls_cod_prodotto_a + "' "		
		ls_intestazione = ls_intestazione + " , Prodotti: compresi fra " + ls_cod_prodotto_da + " "
		ls_intestazione = ls_intestazione + " e " + ls_cod_prodotto_a + " "
	end if
end if	

if isnull(ls_cod_cat_mer) or ls_cod_cat_mer = "" then
	ls_sql = "	select cod_prodotto, des_prodotto " +&
				" from anag_prodotti " +&
				" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
				ls_where_prodotti
else
	ls_sql = "	select cod_prodotto, des_prodotto " +&
				" from anag_prodotti " +&
				" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
				" and cod_cat_mer = '" + ls_cod_cat_mer + "' "	
end if
				
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore dinamico cur_1. Dettaglio =" + sqlca.sqlerrtext) 
	return -1
end if

DO while 0=0
	Yield()
	if ib_interrupt then  // var set in other script
		g_mb.messagebox("Inventario Magazzino","Operazione interrotta dall'utente")
		exit
	end if
	FETCH cur_1 INTO :ls_cod_prodotto, :ls_des_prodotto ;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		return -1
	end if
	sle_1.text= "Selez. prodotto :" + ls_cod_prodotto
	
	select count(*)
	into :ll_num_stock
	from stock
	where cod_azienda = :s_cs_xx.cod_azienda
	  and cod_prodotto = :ls_cod_prodotto;
	if ll_num_stock > 0 then		// elaboro solo prodotti in stock
		ll_newrow = dw_report.insertrow(0) 
		dw_report.ScrollToRow(ll_newrow)
		dw_report.setitem(ll_newrow, "cod_prodotto" , ls_cod_prodotto)
		dw_report.setitem(ll_newrow, "des_prodotto"  , ls_des_prodotto)
	end if
LOOP
CLOSE cur_1 ;

ld_consumi_tot = 0
ll_num_righe = dw_report.rowcount()
for ll_i = 1 to ll_num_righe
	Yield()
	if ib_interrupt then  // var set in other script
		g_mb.messagebox("Inventario Magazzino","Operazione interrotta dall'utente")
		exit
	end if

	for ll_j = 1 to 14
		ld_quant_val[ll_j] = 0
	next
	ls_cod_prodotto = dw_report.getitemstring(ll_i, "cod_prodotto")
	
	SELECT saldo_quan_inizio_anno,   
			 val_inizio_anno  
	 INTO :ld_quant_val[1],   
			:ld_quant_val[2]  
	 FROM anag_prodotti  
	WHERE cod_prodotto = :s_cs_xx.cod_azienda AND  
			cod_prodotto = :ls_cod_prodotto ;

	ld_quant_val[2] = ld_quant_val[1] * ld_quant_val[2]

	sle_1.text= "Giac. prodotto :" + ls_cod_prodotto

	luo_magazzino = CREATE uo_magazzino

	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_da, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

	destroy luo_magazzino
	
	if li_return < 0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		return -1
	end if
	ld_consumi_1 = ld_quant_val[6]
	
	for ll_j = 1 to 14
		ld_quant_val[ll_j] = 0
	next
	SELECT saldo_quan_inizio_anno,   
			 val_inizio_anno  
	 INTO :ld_quant_val[1],   
			:ld_quant_val[2]  
	 FROM anag_prodotti  
	WHERE cod_prodotto = :s_cs_xx.cod_azienda AND  
			cod_prodotto = :ls_cod_prodotto ;

	ld_quant_val[2] = ld_quant_val[1] * ld_quant_val[2]
	
	luo_magazzino = CREATE uo_magazzino
	
	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[],ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

	destroy luo_magazzino
	
	if li_return <0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		return -1
	end if
	ld_consumi_2 = ld_quant_val[6]
	
	ld_consumi = ld_consumi_2 - ld_consumi_1
	dw_report.setitem(ll_i, "consumo"  , ld_consumi)
	ld_consumi_tot = ld_consumi_tot + ld_consumi
	

next // ll_i: righe prodotti

// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven

if ld_consumi_tot = 0 then
	g_mb.messagebox("Apice", "consumi totali = 0")
	return
end if

// calcolo incidenza
sle_1.text= "Calcolo incidenza" 
for ll_i = 1 to ll_num_righe
	
	ld_consumi = dw_report.getitemnumber(ll_i, "consumo")
	ld_incidenza = (ld_consumi / ld_consumi_tot) * 100
	ld_incidenza = round(ld_incidenza , 4)
	dw_report.setitem(ll_i, "incidenza"  , ld_incidenza )
next // ll_i: righe prodotti

dw_report.accepttext()

dw_report.setsort("incidenza d")
dw_report.sort()

string ls_filtro

ls_filtro = "consumo > 0"
dw_report.SetFilter(ls_filtro)
dw_report.Filter()
ll_num_righe = dw_report.rowcount()

// leggo classi ls_classi[],  ld_classi_da[], ld_classi_a[]

declare cur_classi cursor for  
select tab_analisi_abc.classe,   
		tab_analisi_abc.scaglione_da,   
		tab_analisi_abc.scaglione_a  
 from tab_analisi_abc  
 where cod_azienda = :s_cs_xx.cod_azienda;
	 
open cur_classi;
ll_j = 1
do while 0=0
	fetch cur_classi into :ls_classe, :ld_scaglione_da, :ld_scaglione_a;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Apice", "Errore nel DB " + SQLCA.SQLErrText)
		return -1
	end if
	
	ls_classi[ll_j]= ls_classe  
	ld_classi_da[ll_j] = ld_scaglione_da
	ld_classi_a[ll_j] = ld_scaglione_a
	ll_j = ll_j + 1
loop
close cur_classi ;
ll_num_classi = upperbound(ls_classi)

// calcolo classe
sle_1.text= "Calcolo classi" 
ld_progressivo = 0
for ll_i = 1 to ll_num_righe
	
	ld_incidenza = dw_report.getitemnumber(ll_i, "incidenza")
	ld_progressivo = ld_progressivo + ld_incidenza
	
	for ll_j = 1 to ll_num_classi
		if ld_progressivo >= ld_classi_da[ll_j] and ld_progressivo <= ld_classi_a[ll_j] then
			dw_report.setitem(ll_i, "classe", ls_classi[ll_j])
			exit
		end if
	next // ll_j: lettura scaglioni
next // ll_i: righe prodotti


dw_selezione.visible=false
parent.x = 74
parent.y = 301
parent.width = 3550
parent.height = 1700

dw_report.object.intestazione.text = ls_intestazione

dw_report.visible=true

cb_selezione.visible = true
cb_annulla.visible = false
cb_esci.visible = false
dw_selezione.object.b_ricerca_prodotto_da.visible = false
dw_selezione.object.b_ricerca_prodotto_a.visible = false
cb_report.visible = false
dw_report.Change_DW_Current()



end event

type cb_selezione from commandbutton within w_report_abc
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_report.reset()
dw_report.hide()
dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2250
parent.height = 670

cb_selezione.visible = false
cb_annulla.visible = true
cb_esci.visible = true
dw_selezione.object.b_ricerca_prodotto_da.visible = true
dw_selezione.object.b_ricerca_prodotto_a.visible = true
cb_report.visible = true


end event

type dw_selezione from uo_cs_xx_dw within w_report_abc
integer x = 23
integer y = 20
integer width = 2171
integer height = 380
integer taborder = 40
string dataobject = "d_selezione_report_abc"
end type

event itemchanged;call super::itemchanged;string ls_nomecol, ls_nulla

setnull(ls_nulla)

ls_nomecol = i_colname
if ls_nomecol = "cod_prodotto_da" then
	setitem(getrow(), "cod_cat_mer", ls_nulla)
end if
if ls_nomecol = "cod_prodotto_a" then
	setitem(getrow(), "cod_cat_mer", ls_nulla)
end if

if ls_nomecol = "cod_cat_mer" then
	setitem(getrow(), "cod_prodotto_da", ls_nulla)
	setitem(getrow(), "cod_prodotto_a", ls_nulla)
end if


end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_abc
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 30
string dataobject = "d_report_abc"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_esci from commandbutton within w_report_abc
integer x = 1097
integer y = 420
integer width = 297
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

event clicked;close(parent)
end event

type sle_1 from singlelineedit within w_report_abc
integer x = 23
integer y = 400
integer width = 1029
integer height = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean autohscroll = false
end type

