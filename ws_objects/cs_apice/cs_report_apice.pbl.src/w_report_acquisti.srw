﻿$PBExportHeader$w_report_acquisti.srw
$PBExportComments$Report prodotti acquistati movimentati
forward
global type w_report_acquisti from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_acquisti
end type
type cb_report from commandbutton within w_report_acquisti
end type
type cb_selezione from commandbutton within w_report_acquisti
end type
type dw_report from uo_cs_xx_dw within w_report_acquisti
end type
type sle_1 from singlelineedit within w_report_acquisti
end type
type dw_selezione from uo_cs_xx_dw within w_report_acquisti
end type
end forward

global type w_report_acquisti from w_cs_xx_principale
integer width = 3566
integer height = 1676
string title = "Report Prodotti Acquistati"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
sle_1 sle_1
dw_selezione dw_selezione
end type
global w_report_acquisti w_report_acquisti

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2291
this.height = 709
cb_selezione.hide()
end event

on w_report_acquisti.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.sle_1=create sle_1
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.sle_1
this.Control[iCurrent+6]=this.dw_selezione
end on

on w_report_acquisti.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.sle_1)
destroy(this.dw_selezione)
end on

event resize;dw_report.move(20,20)
cb_selezione.move(newwidth - 400, newheight - 90)
dw_report.height= newheight - 110
dw_report.width= newwidth - 40
end event

type cb_annulla from commandbutton within w_report_acquisti
integer x = 1463
integer y = 480
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_acquisti
integer x = 1851
integer y = 480
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;datetime ldt_data_da, ldt_data_a, ldt_data_doc_origine, ldt_data_bolla
string ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_fornitore_sel, ls_flag_scelta, &
		 ls_sql, ls_cod_prodotto, ls_cod_doc_origine, ls_numeratore_doc_origine, &
		 ls_cod_fornitore, ls_numeratore_doc, ls_des_prodotto, ls_cod_tipo_bol_acq, &
		 ls_intestazione, ls_rag_soc_1, ls_errore, ls_flag_tipo_fat_acq, ls_flag_tipo_bol_acq
long ll_newrow, ll_num_righe, ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo, &
	  ll_num_registrazione_mov_mag, ll_anno_registrazione_mov_mag, ll_anno_bolla_acq, li_return, ll_cont, ll_i, ll_num_bolla_acq
dec{4} ld_quan_fatturata, ld_prezzo_acquisto, ld_quan_arrivata, ld_val_riga

datastore lds_fatacq, lds_bolacq

dw_selezione.accepttext()
dw_report.reset()

ldt_data_da = dw_selezione.getitemdatetime(1, "data_registrazione_da")
ldt_data_a = dw_selezione.getitemdatetime(1, "data_registrazione_a")
ls_cod_prodotto_da = dw_selezione.getitemstring(1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getitemstring(1, "cod_prodotto_a")
ls_cod_fornitore_sel = dw_selezione.getitemstring(1, "cod_fornitore")
ls_flag_scelta = dw_selezione.getitemstring(1, "flag_scelta")

if isnull(ls_flag_scelta) then ls_flag_scelta = "N"

if isnull(ldt_data_da) or isnull(ldt_data_a) then
	g_mb.messagebox("Apice", "Impostare entrambe le date")
	return
end if

ls_intestazione = "data da: "+ string(ldt_data_da, "dd/mm/yyyy") + " data a: " + string(ldt_data_a, "dd/mm/yyyy") + "; "
if ls_cod_prodotto_da = "" then  setnull(ls_cod_prodotto_da)
if ls_cod_prodotto_a = "" then  setnull(ls_cod_prodotto_a)

if isnull(ls_cod_prodotto_da) or isnull(ls_cod_prodotto_da) then
	li_return = g_mb.messagebox("Apice", "Selezione prodotti non impostati: continuare?", Question!, YesNo! )
	if li_return = 2 then return
end if

if not isnull(ls_cod_prodotto_da) then
	ls_intestazione = ls_intestazione + " prodotto da: " + ls_cod_prodotto_da
end if
if not isnull(ls_cod_prodotto_a) then
	ls_intestazione = ls_intestazione + " prodotto a: " + ls_cod_prodotto_a
end if

if isnull(ls_cod_fornitore_sel) or ls_cod_fornitore_sel= "" then
	ls_cod_fornitore_sel = "%"
end if

ls_intestazione = ls_intestazione + " fornitore: " + ls_cod_fornitore_sel

ls_sql = "select det_fat_acq.cod_prodotto,   " +&
"det_fat_acq.quan_fatturata,   " +&
"det_fat_acq.num_registrazione_mov_mag, " +&  				
"det_fat_acq.anno_registrazione_mov_mag, " +&   				
"tes_fat_acq.cod_doc_origine,   " +&
"tes_fat_acq.numeratore_doc_origine, " +&   
"tes_fat_acq.anno_doc_origine,   " +&
"tes_fat_acq.num_doc_origine,  " +& 
"tes_fat_acq.progressivo,  " +& 
"tes_fat_acq.data_doc_origine, " +&  
"tes_fat_acq.cod_fornitore, " +& 
"anag_prodotti.des_prodotto, " +&  
"mov_magazzino.val_movimento, " + &
"tab_tipi_fat_acq.flag_tipo_fat_acq " + &
" from det_fat_acq " +&
" join tes_fat_acq  on det_fat_acq.cod_azienda=tes_fat_acq.cod_azienda and det_fat_acq.anno_registrazione=tes_fat_acq.anno_registrazione and det_fat_acq.num_registrazione=tes_fat_acq.num_registrazione " +&
" left join anag_prodotti on anag_prodotti.cod_azienda=det_fat_acq.cod_azienda and anag_prodotti.cod_prodotto=det_fat_acq.cod_prodotto " + &
" left join mov_magazzino on mov_magazzino.cod_azienda=tes_fat_acq.cod_azienda and mov_magazzino.anno_registrazione=det_fat_acq.anno_registrazione_mov_mag and mov_magazzino.num_registrazione = det_fat_acq.num_registrazione_mov_mag " + &
" left join tab_tipi_fat_acq on tes_fat_acq.cod_azienda=tab_tipi_fat_acq.cod_azienda and tes_fat_acq.cod_tipo_fat_acq=tab_tipi_fat_acq.cod_tipo_fat_acq " + &
"WHERE det_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "'  and  " +&
"  det_fat_acq.cod_prodotto is not null  and  " +&
"  tes_fat_acq.flag_fat_confermata = 'S'  and  " +&
"  det_fat_acq.num_bolla_acq is null  and  " +&
"  tes_fat_acq.data_registrazione <= '" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' and  " +&
"  tes_fat_acq.data_registrazione >= '" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' and  " +&
"  tes_fat_acq.cod_fornitore like '" + ls_cod_fornitore_sel + "'   "
if not isnull(ls_cod_prodotto_da) then
	ls_sql +=  " and det_fat_acq.cod_prodotto >= '" + ls_cod_prodotto_da + "' "
end if
if not isnull(ls_cod_prodotto_da) then
	ls_sql +=  " and det_fat_acq.cod_prodotto <= '" + ls_cod_prodotto_a + "' "
end if

ll_cont = guo_functions.uof_crea_datastore( lds_fatacq, ls_sql, ref ls_errore)

if ll_cont < 0 then
	g_mb.error(g_str.format("Errore SQL in ricerca fatture. $1", ls_errore))
	return
end if

for ll_i = 1 to ll_cont
	ls_cod_prodotto 			= lds_fatacq.getitemstring(ll_i,1)
	ld_quan_fatturata 			= lds_fatacq.getitemnumber(ll_i,2)
	ll_num_registrazione_mov_mag = lds_fatacq.getitemnumber(ll_i,3)
	ll_anno_registrazione_mov_mag = lds_fatacq.getitemnumber(ll_i,4) 
	ls_cod_doc_origine 		= lds_fatacq.getitemstring(ll_i,5)
	ls_numeratore_doc_origine = lds_fatacq.getitemstring(ll_i,6)
	ll_anno_doc_origine 		= lds_fatacq.getitemnumber(ll_i,7)
	ll_num_doc_origine 		= lds_fatacq.getitemnumber(ll_i,8)
	ll_progressivo 				= lds_fatacq.getitemnumber(ll_i,9)
	ldt_data_doc_origine 		= lds_fatacq.getitemdatetime(ll_i,10)
	ls_cod_fornitore 			= lds_fatacq.getitemstring(ll_i,11)
	ls_des_prodotto			= lds_fatacq.getitemstring(ll_i,12)
	ld_prezzo_acquisto 		= lds_fatacq.getitemnumber(ll_i,13)
	ls_flag_tipo_fat_acq 		= lds_fatacq.getitemstring(ll_i,14)
	
	if ls_flag_tipo_fat_acq = "N" then ld_quan_fatturata = ld_quan_fatturata * -1

	ls_numeratore_doc = g_str.format("$1 $2 - $3 / $4 $5",ls_cod_doc_origine, ll_anno_doc_origine,ls_numeratore_doc_origine,ll_num_doc_origine,ll_progressivo)
	
	sle_1.text = g_str.format("FATTURA: $1 $2", ls_numeratore_doc, ldt_data_doc_origine)
	if isnull(ld_prezzo_acquisto) then ld_prezzo_acquisto = 0

	ll_newrow = dw_report.insertrow(0) 
	dw_report.ScrollToRow(ll_newrow)
	
	if isnull(ls_cod_fornitore) or len(ls_cod_fornitore) < 1 then
		ls_rag_soc_1 = "<non specificato>"
	else
		ls_rag_soc_1 = f_des_tabella("anag_fornitori", "cod_fornitore = '" + ls_cod_fornitore + "'", "rag_soc_1")
	end if
	dw_report.setitem(ll_newrow, "cod_prodotto" , ls_cod_prodotto)
	dw_report.setitem(ll_newrow, "des_prodotto" , ls_des_prodotto)
	dw_report.setitem(ll_newrow, "cod_fornitore" , ls_rag_soc_1)
	dw_report.setitem(ll_newrow, "numeratore_doc", ls_numeratore_doc)
	dw_report.setitem(ll_newrow, "data_doc" , ldt_data_doc_origine)
	dw_report.setitem(ll_newrow, "quantita" , ld_quan_fatturata)
	dw_report.setitem(ll_newrow, "valore_un" , ld_prezzo_acquisto)
next

// leggo dati delle bolle
ls_sql = "SELECT tes_bol_acq.anno_bolla_acq,   " +&
"tes_bol_acq.num_bolla_acq, "   +&
"tes_bol_acq.cod_fornitore,  "  +&
"tes_bol_acq.data_bolla,   " +&
"tes_bol_acq.cod_tipo_bol_acq, "  +&
"det_bol_acq.cod_prodotto,  "  +&
"det_bol_acq.quan_arrivata,  "  +&
"det_bol_acq.val_riga,  " +&         
"det_bol_acq.num_registrazione_mov_mag, " +&   
"det_bol_acq.anno_registrazione_mov_mag, " +&  
"anag_prodotti.des_prodotto, " +&  
"mov_magazzino.val_movimento, " + &
"tab_tipi_bol_acq.tipo_bol_acq " + &
 "FROM det_bol_acq " +&
" join tes_bol_acq on tes_bol_acq.cod_azienda = det_bol_acq.cod_azienda and tes_bol_acq.anno_bolla_acq = det_bol_acq.anno_bolla_acq  and  tes_bol_acq.num_bolla_acq = det_bol_acq.num_bolla_acq   " +&
" left join anag_prodotti on anag_prodotti.cod_azienda=det_bol_acq.cod_azienda and anag_prodotti.cod_prodotto=det_bol_acq.cod_prodotto " + &
" left join mov_magazzino on mov_magazzino.cod_azienda=det_bol_acq.cod_azienda and mov_magazzino.anno_registrazione=det_bol_acq.anno_registrazione_mov_mag and mov_magazzino.num_registrazione = det_bol_acq.num_registrazione_mov_mag " + &
" left join tab_tipi_bol_acq on tes_bol_acq.cod_azienda=tab_tipi_bol_acq.cod_azienda and tes_bol_acq.cod_tipo_bol_acq=tab_tipi_bol_acq.cod_tipo_bol_acq " + &
" WHERE det_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "'  and  " +&
" tes_bol_acq.flag_blocco = 'N'  AND  " +&
" tes_bol_acq.cod_fornitore like '" + ls_cod_fornitore_sel + "'  and  " +&
" tes_bol_acq.data_registrazione >= '" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "'  and  " +&
" tes_bol_acq.data_registrazione <= '" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "'  and  " +&
" det_bol_acq.cod_prodotto is not null  and " +&
" tes_bol_acq.flag_movimenti = 'S'"

if not isnull(ls_cod_prodotto_da) then
	ls_sql +=  " and det_bol_acq.cod_prodotto >= '" + ls_cod_prodotto_da + "' "
end if
if not isnull(ls_cod_prodotto_da) then
	ls_sql +=  " and det_bol_acq.cod_prodotto <= '" + ls_cod_prodotto_a + "' "
end if

ll_cont = guo_functions.uof_crea_datastore( lds_bolacq, ls_sql, ref ls_errore)

if ll_cont < 0 then
	g_mb.error(g_str.format("Errore SQL in ricerca bolle entrata. $1", ls_errore))
	return
end if

for ll_i = 1 to ll_cont

	ll_anno_bolla_acq 		= lds_bolacq.getitemnumber(ll_i,1)
	ll_num_bolla_acq 		= lds_bolacq.getitemnumber(ll_i,2)
	ls_cod_fornitore 		= lds_bolacq.getitemstring(ll_i,3)
	ldt_data_bolla 			= lds_bolacq.getitemdatetime(ll_i,4)
	ls_cod_tipo_bol_acq 	= lds_bolacq.getitemstring(ll_i,5)
	ls_cod_prodotto  		= lds_bolacq.getitemstring(ll_i,6)
	ld_quan_arrivata 		= lds_bolacq.getitemnumber(ll_i,7)
	ld_val_riga				= lds_bolacq.getitemnumber(ll_i,8)
	ll_num_registrazione_mov_mag = lds_bolacq.getitemnumber(ll_i,9)
	ll_anno_registrazione_mov_mag = lds_bolacq.getitemnumber(ll_i,10)
	ls_des_prodotto		= lds_bolacq.getitemstring(ll_i,11)
	ld_prezzo_acquisto	= lds_bolacq.getitemnumber(ll_i,12)
	ls_flag_tipo_bol_acq 		= lds_bolacq.getitemstring(ll_i,13)
	
	if ls_flag_tipo_bol_acq = "R" then ld_quan_arrivata = ld_quan_arrivata * -1
	
	ls_numeratore_doc = g_str.format("$1 $2/$3 - $4",ls_cod_tipo_bol_acq, ll_anno_bolla_acq, ll_num_bolla_acq,ll_progressivo)
	sle_1.text = g_str.format("BOLLA: $1 - $2",ls_numeratore_doc, ldt_data_bolla)
	Yield()
	if isnull(ld_prezzo_acquisto) then ld_prezzo_acquisto = 0

	ll_newrow = dw_report.insertrow(0) 
	dw_report.ScrollToRow(ll_newrow)
	if isnull(ls_cod_fornitore) or len(ls_cod_fornitore) < 1 then
		ls_rag_soc_1 = "<non specificato>"
	else
		ls_rag_soc_1 = f_des_tabella("anag_fornitori", "cod_fornitore = '" + ls_cod_fornitore + "'", "rag_soc_1")
	end if
	dw_report.setitem(ll_newrow, "cod_prodotto" , ls_cod_prodotto)
	dw_report.setitem(ll_newrow, "des_prodotto" , ls_des_prodotto)
	dw_report.setitem(ll_newrow, "cod_fornitore" , ls_rag_soc_1)
	dw_report.setitem(ll_newrow, "numeratore_doc", ls_numeratore_doc)
	dw_report.setitem(ll_newrow, "data_doc" , ldt_data_bolla)
	dw_report.setitem(ll_newrow, "quantita" , ld_quan_arrivata)
	dw_report.setitem(ll_newrow, "valore_un" , ld_prezzo_acquisto)
next

//dw_report.setsort("data_doc a, numeratore_doc a")
//se fa vedere i totali....
if ls_flag_scelta = "N" then 
	dw_report.setsort("data_doc a, numeratore_doc a")
else
	dw_report.setsort("cod_prodotto")//, data_doc a, numeratore_doc a")
end if

dw_report.sort()

dw_report.object.intestazione.text = ls_intestazione

dw_selezione.hide()
dw_selezione.object.b_ricerca_prodotto_da.visible=false
dw_selezione.object.b_ricerca_prodotto_a.visible=false
dw_selezione.object.b_ricerca_fornitore.visible=false
cb_report.hide()
cb_annulla.hide()

parent.x = 10
parent.y = 20
parent.width = 3600
parent.height = 1700

if ls_flag_scelta = "N" then // deve nascondere i totali
	dw_report.object.tot_qta_prodotto_t.visible = 0
	dw_report.object.tot_qta_prodotto.visible = 0
	dw_report.object.tot_val_prodotto_t.visible = 0
	dw_report.object.tot_val_prodotto.visible = 0	
	dw_report.object.t_3.visible = 0
	dw_report.object.compute_3.visible = 0
	dw_report.object.compute_4.visible = 0
else
	dw_report.object.tot_qta_prodotto_t.visible = 1
	dw_report.object.tot_qta_prodotto.visible = 1
	dw_report.object.tot_val_prodotto_t.visible = 1
	dw_report.object.tot_val_prodotto.visible = 1	
	dw_report.object.t_3.visible = 1
	dw_report.object.compute_3.visible = 1
	dw_report.object.compute_4.visible = 1
end if

dw_report.show()

dw_report.groupcalc()

cb_selezione.show()
dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_acquisti
integer x = 3154
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;
dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2291
parent.height = 709

dw_report.hide()
cb_selezione.hide()

dw_selezione.show()
dw_selezione.object.b_ricerca_prodotto_da.visible=true
dw_selezione.object.b_ricerca_prodotto_a.visible=true
dw_selezione.object.b_ricerca_fornitore.visible=true
cb_report.show()
cb_annulla.show()

dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_acquisti
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 50
string dataobject = "d_report_acquisti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type sle_1 from singlelineedit within w_report_acquisti
integer x = 23
integer y = 460
integer width = 1394
integer height = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean enabled = false
boolean border = false
boolean autohscroll = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_acquisti
integer x = 23
integer y = 20
integer width = 2194
integer height = 440
integer taborder = 10
string dataobject = "d_selezione_report_acquisti"
end type

event itemchanged;call super::itemchanged;string ls_descrizione 

if i_colname = "cod_fornitore" then
	ls_descrizione = f_des_tabella("anag_fornitori","cod_fornitore = '" +  i_coltext   + "'","rag_soc_1")
	if ls_descrizione = "< CODICE INESISTENTE >" then return 2
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione, "cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione, "cod_prodotto_a")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione, "cod_fornitore")
end choose
end event

