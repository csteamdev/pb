﻿$PBExportHeader$w_report_assegnato_prodotto.srw
$PBExportComments$Report Situazione Impegnato del Prodotto
forward
global type w_report_assegnato_prodotto from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_assegnato_prodotto
end type
type cb_annulla from commandbutton within w_report_assegnato_prodotto
end type
type cb_cerca from commandbutton within w_report_assegnato_prodotto
end type
type dw_selezione from uo_cs_xx_dw within w_report_assegnato_prodotto
end type
end forward

global type w_report_assegnato_prodotto from w_cs_xx_principale
integer width = 4297
integer height = 2528
string title = "Report Prodotto Assegnato"
dw_report dw_report
cb_annulla cb_annulla
cb_cerca cb_cerca
dw_selezione dw_selezione
end type
global w_report_assegnato_prodotto w_report_assegnato_prodotto

type variables


string				is_cod_prodotto
end variables

on w_report_assegnato_prodotto.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.cb_annulla=create cb_annulla
this.cb_cerca=create cb_cerca
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_cerca
this.Control[iCurrent+4]=this.dw_selezione
end on

on w_report_assegnato_prodotto.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.cb_annulla)
destroy(this.cb_cerca)
destroy(this.dw_selezione)
end on

event pc_setwindow;call super::pc_setwindow;

try
	is_cod_prodotto = message.stringparm
catch (runtimeerror err)
	setnull(is_cod_prodotto)
end try


dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

//if not isnull(i_openparm) then
if is_cod_prodotto<>"" and not isnull(is_cod_prodotto) then
	
//	dw_report.set_dw_options(sqlca, &
//									 i_openparm, &
//									 c_noretrieveonopen + &
//									 c_nomodify + &
//									 c_nodelete + &
//									 c_newonopen + &
//									 c_disableCC, &
//									 c_noresizedw + &
//									 c_nohighlightselected + &
//									 c_nocursorrowpointer +&
//									 c_nocursorrowfocusrect )
									 
	dw_selezione.postevent("ue_selezione")
	
//else
//	dw_report.set_dw_options(sqlca, &
//									 pcca.null_object, &
//									 c_noretrieveonopen + &
//									 c_nomodify + &
//									 c_nodelete + &
//									 c_newonopen + &
//									 c_disableCC, &
//									 c_noresizedw + &
//									 c_nohighlightselected + &
//									 c_nocursorrowpointer +&
//									 c_nocursorrowfocusrect )
	
end if

dw_report.set_dw_options(sqlca, &
									 pcca.null_object, &
									 c_noretrieveonopen + &
									 c_nomodify + &
									 c_nodelete + &
									 c_newonopen + &
									 c_disableCC, &
									 c_noresizedw + &
									 c_nohighlightselected + &
									 c_nocursorrowpointer +&
									 c_nocursorrowfocusrect )


iuo_dw_main = dw_report

dw_report.object.datawindow.print.preview = "yes"

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,	"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_report from uo_cs_xx_dw within w_report_assegnato_prodotto
integer x = 14
integer y = 228
integer width = 4224
integer height = 2164
integer taborder = 60
string dataobject = "d_report_assegnato_prodotto_ord_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto_sel, ls_cod_deposito_sel
LONG  l_Error

dw_selezione.accepttext()

ls_cod_prodotto_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_prodotto")
ls_cod_deposito_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito")

if ls_cod_prodotto_sel="" or isnull(ls_cod_prodotto_sel) then ls_cod_prodotto_sel="%"
if ls_cod_deposito_sel="" or isnull(ls_cod_deposito_sel) then ls_cod_deposito_sel="%"

l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto_sel, ls_cod_deposito_sel)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_annulla from commandbutton within w_report_assegnato_prodotto
integer x = 2949
integer y = 136
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

type cb_cerca from commandbutton within w_report_assegnato_prodotto
integer x = 3346
integer y = 136
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;//dw_report.reset()

//setpointer(hourglass!)

dw_report.postevent("pcd_retrieve")

//setpointer(arrow!)
end event

type dw_selezione from uo_cs_xx_dw within w_report_assegnato_prodotto
event ue_selezione ( )
integer x = 23
integer y = 12
integer width = 3191
integer height = 200
integer taborder = 10
string dataobject = "d_dw_selezione_rep_impegnato_prodotto"
boolean border = false
end type

event ue_selezione();

//setitem(getrow(),"cod_prodotto",dw_report.i_parentdw.getitemstring(dw_report.i_parentdw.getrow(),"cod_prodotto"))
setitem(getrow(),"cod_prodotto", is_cod_prodotto)


cb_cerca.postevent("clicked")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

