﻿$PBExportHeader$w_report_consegna_ordine_prod.srw
$PBExportComments$Report righe ord. raggr. per prod.
forward
global type w_report_consegna_ordine_prod from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_consegna_ordine_prod
end type
type cb_report from commandbutton within w_report_consegna_ordine_prod
end type
type cb_selezione from commandbutton within w_report_consegna_ordine_prod
end type
type dw_report from uo_cs_xx_dw within w_report_consegna_ordine_prod
end type
type dw_selezione from uo_cs_xx_dw within w_report_consegna_ordine_prod
end type
end forward

global type w_report_consegna_ordine_prod from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Consegna/Ordine/Prodotto"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
dw_selezione dw_selezione
end type
global w_report_consegna_ordine_prod w_report_consegna_ordine_prod

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2172
this.height = 633

end event

on w_report_consegna_ordine_prod.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_selezione
end on

on w_report_consegna_ordine_prod.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.dw_selezione)
end on

type cb_annulla from commandbutton within w_report_consegna_ordine_prod
integer x = 1349
integer y = 420
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_consegna_ordine_prod
integer x = 1737
integer y = 420
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_doc_da,ls_doc_a,ls_cod_cliente,ls_prod_da,ls_prod_a,ls_flag_evaso
integer li_riga
double ld_num_da,ld_num_a,ld_doc_da,ld_doc_a,ld_num_anno
datetime ldt_da,ldt_a
//string ls_prodotto, ls_cat_mer
//date   ld_da_data, ld_a_data
//
//
dw_selezione.accepttext()
ld_num_anno=dw_selezione.getitemnumber(1,"n_anno")
if isnull(ld_num_anno) then ld_num_anno=f_anno_esercizio()
ld_num_da=dw_selezione.getitemnumber(1,"n_ord_da")
if isnull(ld_num_da) or ld_num_da <1 then ld_num_da=1
ld_num_a=dw_selezione.getitemnumber(1,"n_ord_a")
if isnull(ld_num_a) then ld_num_a=999999
ls_prod_da=dw_selezione.getitemstring(1,"s_prod_da")
if isnull(ls_prod_da) or ls_prod_da="" then ls_prod_da="000000000000000"
ls_prod_a=dw_selezione.getitemstring(1,"s_prod_a")
if isnull(ls_prod_a) or ls_prod_a="" then ls_prod_a="ZZZZZZZZZZZZZZZ"
ldt_da=datetime(dw_selezione.getitemdate(1,"d_data_da"))
if isnull(ldt_da) then ldt_da=datetime(1900-01-01)
ldt_a=datetime(dw_selezione.getitemdate(1,"d_data_a"))
if isnull(ldt_a) then ldt_a=datetime(2999-12-31)
ls_flag_evaso=dw_selezione.getitemstring(1,"flag_evaso")
if isnull(ls_flag_evaso) or ls_flag_evaso="" then ls_flag_evaso="T"
ls_cod_cliente=dw_selezione.getitemstring(1,"cod_cliente")
if isnull(ls_cod_cliente) or ls_cod_cliente="" then ls_cod_cliente="%"
//ls_prodotto = dw_selezione.getitemstring(1,"rs_cod_prodotto")
//ls_cat_mer  = dw_selezione.getitemstring(1,"rs_cat_prodotto")
//ld_da_data  = dw_selezione.getitemdate(1,"rd_da_data")
//ld_a_data   = dw_selezione.getitemdate(1,"rd_a_data")
//
//if isnull(ls_prodotto) then ls_prodotto = "%"
//if isnull(ls_cat_mer) then  ls_cat_mer  = "%"
//if isnull(ld_da_data) then  ld_da_data  = date("01/01/1900")
//if isnull(ld_a_data)  then  ld_a_data   = date("31/12/2999")
//

dw_selezione.hide()


parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665
//
dw_report.show()
dw_report.retrieve(s_cs_xx.cod_azienda,ldt_da,ldt_a,ld_num_da,ld_num_a,ld_num_anno,ls_prod_da,ls_prod_a,ls_cod_cliente,ls_flag_evaso)
cb_selezione.show()
cb_annulla.hide()
cb_report.hide()
//cb_ricerca_cliente.hide()
//
dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_consegna_ordine_prod
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2172
parent.height = 633

dw_report.hide()
cb_selezione.hide()
cb_report.show()
cb_annulla.show()
//cb_ricerca_prodotto.show()
//cb_ricerca_prodotto_2.show()
//cb_ricerca_cliente.show()
dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_consegna_ordine_prod
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 30
string dataobject = "d_consegna_ordine_prodotto"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_selezione from uo_cs_xx_dw within w_report_consegna_ordine_prod
integer x = 23
integer y = 20
integer width = 2080
integer height = 380
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_cerca_consegna_ordine_prodotto"
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"s_prod_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"s_prod_a")
end choose
		
end event

