﻿$PBExportHeader$w_report_consumi_magazzino.srw
forward
global type w_report_consumi_magazzino from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_report_consumi_magazzino
end type
type dw_report from uo_cs_xx_dw within w_report_consumi_magazzino
end type
type dw_folder from u_folder within w_report_consumi_magazzino
end type
type dw_selezione from uo_cs_xx_dw within w_report_consumi_magazzino
end type
type st_1 from statictext within w_report_consumi_magazzino
end type
end forward

global type w_report_consumi_magazzino from w_cs_xx_principale
integer width = 4279
integer height = 2476
string title = "Giornale di Magazzino"
event ue_reset ( )
event ue_report ( )
hpb_1 hpb_1
dw_report dw_report
dw_folder dw_folder
dw_selezione dw_selezione
st_1 st_1
end type
global w_report_consumi_magazzino w_report_consumi_magazzino

type variables

string								is_cod_prod_modello = ""
end variables

forward prototypes
public function integer wf_giacenza_inizio (uo_magazzino auo_mag, string as_cod_prodotto, string as_cod_deposito, datetime adt_data_rif, ref decimal ad_giacenza)
public function integer wf_costo_medio (uo_magazzino auo_mag, string as_cod_prodotto, string as_cod_deposito, datetime adt_data_rif, ref decimal ad_costo)
end prototypes

event ue_reset();string				ls_null
datetime			ldt_inizio, ldt_fine

setnull(ls_null)

//imposta con ultima chiusura effettuata

select data_chiusura_annuale
into :ldt_inizio
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;

ldt_inizio = datetime(date(ldt_inizio), 00:00:00)
ldt_fine = datetime(today(), 23:59:59)

//data inizio impostata come giorno successivo all'ultima chiusura magazzino effettuata
//infatti i movimenti di apertura sono nel giorno successivo alla chiusura
//es. se chiudo al 31/12/2014 gli INL saranno in data 01/01/2015

if not isnull(ldt_inizio) and year(date(ldt_inizio)) > 1980 then ldt_inizio = datetime(relativedate(date(ldt_inizio), 1), 00:00:00)

dw_selezione.setitem(1, "data_inizio", ldt_inizio)
dw_selezione.setitem(1, "data_fine", ldt_fine)
dw_selezione.setitem(1, "cod_deposito", ls_null)
dw_selezione.setitem(1, "cod_prodotto_inizio", ls_null)
dw_selezione.setitem(1, "cod_prodotto_fine",ls_null)

end event

event ue_report();boolean					lb_1, lb_2, lb_3

string						ls_sql, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_des_prodotto, &
							ls_cod_misura_mag, ls_des_deposito, ls_cod_prodotto_hold, &
							ls_azienda_rag_soc_1, ls_azienda_localita, ls_azienda_provincia, ls_partita_iva, &
							ls_errore, ls_cod_deposito_sel, ls_cod_prodotto_ds, &
							ls_where_movimenti, ls_log, ls_sql_base, &
							ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_flag_std
							
long						ll_riga, ll_i,ll_tipo_operazione,ll_y,ll_err, ll_cont_scartati, ll_z, ll_tot, ll_index

dec{4}					ld_quan_movimento, ld_quan_carico, ld_quan_scarico, ld_giacenza_inizio, ld_giacenza_fine, ld_perc

dec{5}					ld_costo, ld_costo_std

datetime					ldt_inizio, ldt_fine, ldt_data_registrazione

uo_magazzino			luo_magazzino

datastore				lds_movimenti



// ----------------------- impostazioni aspetti grafici del report -----------------------------------------
dw_selezione.accepttext()
dw_report.reset()
dw_report.setredraw(false)
st_1.text = "Attendere la Preparazione del Report; l'operazione potrebbe richiedere parecchi minuti."
// ----------------------- verifica impostazioni e filtri --------------------------------------------------

hpb_1.position = 0

ldt_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_fine = dw_selezione.getitemdatetime(1,"data_fine")

ldt_inizio = datetime(date(ldt_inizio), 00:00:00)
ldt_fine = datetime(date(ldt_fine), 23:59:59)


if isnull(ldt_inizio) then
	g_mb.warning("Impostare la Data Inizio periodo!")
	return
else
	ls_log += " - Dal : " + string(ldt_inizio, "dd/mm/yyyy")
end if

if isnull(ldt_fine) then
	g_mb.warning("Impostare la Data Fine periodo!")
	return
else
	ls_log += " - Fino al : " + string(ldt_fine, "dd/mm/yyyy")
end if

if ldt_inizio > ldt_fine then
	g_mb.warning("La Data di inizio periodo non può essere superiore alla Data fine periodo!")
	return
end if

ls_cod_deposito_sel = dw_selezione.getitemstring(1,"cod_deposito")
ls_cod_prodotto_inizio = dw_selezione.getitemstring(1,"cod_prodotto_inizio")
ls_cod_prodotto_fine = dw_selezione.getitemstring(1,"cod_prodotto_fine")

select rag_soc_1, 
       localita, 
		 provincia, 
		 partita_iva
into   :ls_azienda_rag_soc_1, 
       :ls_azienda_localita, 
		 :ls_azienda_provincia, 
		 :ls_partita_iva
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

try
	dw_report.object.st_azienda.text = ls_azienda_rag_soc_1
//dw_report.object.st_indirizzo.text = ls_azienda_localita + "(" + ls_azienda_provincia + ")"
//dw_report.object.st_piva.text = ls_partita_iva
catch (runtimeerror e)
end try

ll_cont_scartati = 0


ls_sql_base =  "select mov_magazzino.data_registrazione,"+& 
							"mov_magazzino.cod_prodotto,"+&
							"mov_magazzino.cod_deposito,"+&
							"mov_magazzino.quan_movimento,"+&
							"mov_magazzino.cod_tipo_movimento,"+&
							"mov_magazzino.cod_tipo_mov_det,"+&
							"anag_prodotti.cod_misura_mag,"+&
							"anag_prodotti.des_prodotto,"+&
							"anag_prodotti.costo_standard "+&
					"from mov_magazzino "+&
					"join anag_prodotti on anag_prodotti.cod_azienda=mov_magazzino.cod_azienda and " + &
												"anag_prodotti.cod_prodotto=mov_magazzino.cod_prodotto "+&
					"where mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda +"' "
					
ls_where_movimenti = ""

dw_report.object.filtro_t.text = ""

if not isnull(ls_cod_prodotto_inizio) and ls_cod_prodotto_inizio<>"" then
	ls_where_movimenti += "and mov_magazzino.cod_prodotto>='" + ls_cod_prodotto_inizio + "' "
	ls_log += " - Da Prodotto: " + ls_cod_prodotto_inizio
end if	

if not isnull(ls_cod_prodotto_fine) and ls_cod_prodotto_fine<>"" then
	ls_where_movimenti += "and mov_magazzino.cod_prodotto<='" + ls_cod_prodotto_fine + "' "
	ls_log += " - Fino a Prodotto: " + ls_cod_prodotto_fine
end if		

if not isnull(ls_cod_deposito_sel) and ls_cod_deposito_sel<>"" then
	ls_where_movimenti += " and mov_magazzino.cod_deposito='" + ls_cod_deposito_sel + "' "
	ls_log += " - sul Deposito: " + ls_cod_deposito_sel + " " + f_des_tabella("anag_depositi","cod_deposito = '" +  ls_cod_deposito_sel + "'","des_deposito")
end if

//escludo i prodotti MODELLO (tende finite)
if is_cod_prod_modello<>"" then
	ls_sql += " and (anag_prodotti.cod_cat_mer<>'"+is_cod_prod_modello+"' or anag_prodotti.cod_cat_mer is null)"
end if

ldt_fine = datetime(date(ldt_fine), 23:59:59)

ls_where_movimenti +=" and mov_magazzino.data_registrazione>='" + string(ldt_inizio, s_cs_xx.db_funzioni.formato_data) +"' " + &
								"and mov_magazzino.data_registrazione<='" + string(ldt_fine, s_cs_xx.db_funzioni.formato_data) +"' "

ls_sql_base += ls_where_movimenti + " order by mov_magazzino.cod_prodotto, mov_magazzino.data_registrazione"

ll_tot = guo_functions.uof_crea_datastore(lds_movimenti, ls_sql_base, ls_errore)

if ll_tot < 0 then
	g_mb.error("Errore preparazione datastore dei codici da elaborare: " + ls_errore)
	return
elseif ll_tot=0 then
	destroy lds_movimenti
	g_mb.warning("Nessun articolo/movimenti da elaborare con questo filtro impostato!")
	return
end if

dw_report.object.filtro_t.text = ls_log

luo_magazzino = CREATE uo_magazzino

//############################################################
//INIZIO
ls_cod_prodotto_hold = ""
ls_log = ""

hpb_1.maxposition = ll_tot

for ll_index=1 to ll_tot

	Yield()
	ldt_data_registrazione = lds_movimenti.getitemdatetime(ll_index, 1)
	ls_cod_prodotto_ds = lds_movimenti.getitemstring(ll_index, 2)
	
	ld_perc = (ll_index / ll_tot) * 100
	hpb_1.stepit()
	
	//se inizia per spazio saltalo che senno si blocca tutto ....
	if left(ls_cod_prodotto_ds, 1) = " " then continue
	
	ls_log = string(long(ld_perc)) + " % - Elaborazione articolo " +ls_cod_prodotto_ds+" in corso ..."
	st_1.text = ls_log
		
	//leggo gli altri valori del movimento che sto elaborando
	ld_quan_movimento = lds_movimenti.getitemnumber(ll_index, 4)
	ls_cod_tipo_movimento = lds_movimenti.getitemstring(ll_index, 5)
	ls_cod_tipo_mov_det = lds_movimenti.getitemstring(ll_index, 6)
	ls_cod_misura_mag = lds_movimenti.getitemstring(ll_index, 7)
	ls_des_prodotto = lds_movimenti.getitemstring(ll_index, 8)
	ld_costo_std = lds_movimenti.getitemnumber(ll_index, 9)
	
	if ls_cod_prodotto_ds <> ls_cod_prodotto_hold then
		//nuovo prodotto, fai insert row e resetta le variabili
		ll_riga = dw_report.insertrow(0)
		
		ls_cod_prodotto_hold = ls_cod_prodotto_ds
		ld_quan_carico = 0
		ld_quan_scarico = 0
		ld_giacenza_fine = 0
		ls_flag_std = ""
		
		//nuova riga, quindi faccio i setitem comuni
		dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto_ds)
		dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
		dw_report.setitem(ll_riga, "cod_misura_mag", ls_cod_misura_mag)
		
		
		//elaboro giacenza iniziale (quella inizio periodo la calcolo via via che elaboro i movimenti a ritroso)
		wf_giacenza_inizio(luo_magazzino, ls_cod_prodotto_ds, ls_cod_deposito_sel, ldt_inizio, ld_giacenza_inizio)
		
		wf_costo_medio(luo_magazzino, ls_cod_prodotto_ds, ls_cod_deposito_sel, ldt_fine, ld_costo)
		
		ld_giacenza_fine = 0
		
		if isnull(ld_costo) or ld_costo<=0 then
			ld_costo = ld_costo_std
			ls_flag_std = "(*)"
		end if
		
		dw_report.setitem(ll_riga, "giacenza_inizio", ld_giacenza_inizio)
		
		//TODO: valutare il costo medio alla fine del periodo impostato
		//********************************************************
		dw_report.setitem(ll_riga, "costo_medio", ld_costo)
		//********************************************************
		
		dw_report.setitem(ll_riga, "flag_std", ls_flag_std)
		dw_report.setitem(ll_riga, "valore_iniziale", ld_giacenza_inizio * ld_costo)		//<<<<-------------------  valutare il costo medio alla fine del periodo impostato
		
		ld_giacenza_fine = ld_giacenza_inizio
	end if
	
	if date(ldt_data_registrazione) > date(ldt_inizio) then
	
		luo_magazzino.uof_tipo_movimento(ls_cod_tipo_mov_det, ref ll_tipo_operazione, ref lb_1, ref lb_2, ref lb_3)
		
		choose case ll_tipo_operazione
			case 1,5,19,13  		// movimenti di carico
				ld_quan_carico += ld_quan_movimento
				ld_giacenza_fine += ld_quan_movimento
			
			case 2,6,20,14	 		// rettifiche carico
				ld_quan_scarico += ld_quan_movimento
				ld_giacenza_fine -= ld_quan_movimento
				
			case 3,7	 				// movimenti di scarico
				ld_quan_scarico += ld_quan_movimento
				ld_giacenza_fine -= ld_quan_movimento
				
			case 4,8	 				// rettifiche scarico
				ld_quan_carico += ld_quan_movimento
				ld_giacenza_fine += ld_quan_movimento
				
			case else  // 9,15,10,16,11,17,12,18
				// movimenti di rettifica solo valore e quindi salti
				
		end choose
		
		dw_report.setitem(ll_riga, "quan_carico", ld_quan_carico)
		dw_report.setitem(ll_riga, "quan_scarico", ld_quan_scarico)
	end if
	
	dw_report.setitem(ll_riga, "giacenza_fine", ld_giacenza_fine)
	
	if ld_quan_scarico < ld_giacenza_inizio then
		dw_report.setitem(ll_riga, "valore_consumato", ld_quan_scarico * ld_costo)
	else
		dw_report.setitem(ll_riga, "valore_consumato", ld_giacenza_inizio * ld_costo)
	end if
	
	dw_report.setitem(ll_riga, "valore_finale", ld_giacenza_fine * ld_costo)
	dw_report.setitem(ll_riga, "differenza", (ld_giacenza_fine - ld_giacenza_inizio) * ld_costo)
next

destroy luo_magazzino
destroy lds_movimenti
//##########################################################################


st_1.text = "Finito!"

dw_report.setredraw(true)
dw_folder.fu_selecttab(2)

dw_report.change_dw_current()

end event

public function integer wf_giacenza_inizio (uo_magazzino auo_mag, string as_cod_prodotto, string as_cod_deposito, datetime adt_data_rif, ref decimal ad_giacenza);
string					ls_where, ls_error, ls_chiave[], ls_flag_depositi
dec{4}				ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
integer				li_return, li_index

ad_giacenza = 0
ls_flag_depositi = "N"

if g_str.isnotempty(as_cod_deposito) then
	ls_flag_depositi = "D"
	ls_where = "and cod_deposito=~~'"+as_cod_deposito+"~~'"
end if

li_return = auo_mag.uof_saldo_prod_date_decimal(	as_cod_prodotto, adt_data_rif, ls_where, ld_quant_val, ls_error, ls_flag_depositi, &
																	ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

if li_return <0 then
	return -1
end if

if ls_flag_depositi="D" then
	if upperbound(ld_giacenza_stock[]) > 0 then
		ad_giacenza = ld_giacenza_stock[1]
	end if
else
	ad_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]	
end if
if isnull(ad_giacenza) then ad_giacenza = 0
	

return 0
end function

public function integer wf_costo_medio (uo_magazzino auo_mag, string as_cod_prodotto, string as_cod_deposito, datetime adt_data_rif, ref decimal ad_costo);
string					ls_where, ls_error, ls_chiave[], ls_flag_depositi
dec{4}				ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
integer				li_return, li_index

ad_costo = 0
ls_flag_depositi = "N"

if g_str.isnotempty(as_cod_deposito) then
	ls_flag_depositi = "D"
	ls_where = "and cod_deposito=~~'"+as_cod_deposito+"~~'"
end if

li_return = auo_mag.uof_saldo_prod_date_decimal(	as_cod_prodotto, adt_data_rif, ls_where, ld_quant_val, ls_error, ls_flag_depositi, &
																	ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

if li_return <0 then
	return -1
end if

//costo medio
if ld_quant_val[12] > 0 then
	ad_costo = ld_quant_val[13] / ld_quant_val[12]
end if

return 0


end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report Giornale", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,4)
dw_folder.fu_selecttab(1)


select 	cod_cat_mer 
into		:is_cod_prod_modello
from 		tab_cat_mer_tabelle 
where 	cod_azienda = :s_cs_xx.cod_azienda and
      		nome_tabella = 'tab_modelli';
				
if sqlca.sqlcode <> 0 or isnull(is_cod_prod_modello) then
	is_cod_prod_modello = ""
end if

move(600,0)
this.height = w_cs_xx_mdi.mdi_1.height - 50

postevent("resize")
postevent("ue_reset")

end event

on w_report_consumi_magazzino.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_selezione=create dw_selezione
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.st_1
end on

on w_report_consumi_magazzino.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_selezione)
destroy(this.st_1)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(	dw_selezione, &
								"cod_deposito", sqlca, "anag_depositi", "cod_deposito", &
								"des_deposito", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
									"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event resize;
//ancestor script disattivato!!!!!


//sistemo il folder
dw_folder.width = this.width - 100
dw_folder.height = this.height - 150

dw_report.height = dw_folder.height - 140
dw_report.width = dw_folder.width - 100


return
end event

type hpb_1 from hprogressbar within w_report_consumi_magazzino
integer x = 23
integer y = 848
integer width = 3589
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_report from uo_cs_xx_dw within w_report_consumi_magazzino
integer x = 18
integer y = 132
integer width = 4169
integer height = 2156
integer taborder = 10
string dataobject = "d_report_consumi_magazzino"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_consumi_magazzino
integer x = 9
integer y = 20
integer width = 4201
integer height = 2300
integer taborder = 20
boolean border = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_consumi_magazzino
integer x = 23
integer y = 140
integer width = 3589
integer height = 612
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_sel_report_consumi_magazzino"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;integer			li_ret


choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_inizio")
	
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_fine")
		
	case "b_report"
		parent.postevent("ue_report")
		
	case "b_reset"
		parent.postevent("ue_reset")
		
	case "b_esporta"
		li_ret = dw_report.saveas("", Excel!, true)
		
		if li_ret = 1 then
			g_mb.success("Esportazione effettuata!")
		else
			g_mb.warning("Esportazione fallita!")
		end if
		
end choose
end event

event itemchanged;call super::itemchanged;

choose case dwo.name
	case "cod_prodotto_inizio"
		if data <> "" then
			setitem(1, "cod_prodotto_fine", data)
		end if
		
end choose
end event

type st_1 from statictext within w_report_consumi_magazzino
integer x = 23
integer y = 760
integer width = 3589
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean border = true
boolean focusrectangle = false
end type

