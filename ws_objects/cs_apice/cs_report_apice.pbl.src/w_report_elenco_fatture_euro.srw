﻿$PBExportHeader$w_report_elenco_fatture_euro.srw
$PBExportComments$Finestra Report Elenco Fatture con contropartite
forward
global type w_report_elenco_fatture_euro from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_elenco_fatture_euro
end type
type cb_report from commandbutton within w_report_elenco_fatture_euro
end type
type cb_selezione from commandbutton within w_report_elenco_fatture_euro
end type
type dw_selezione from uo_cs_xx_dw within w_report_elenco_fatture_euro
end type
type dw_report from uo_cs_xx_dw within w_report_elenco_fatture_euro
end type
end forward

global type w_report_elenco_fatture_euro from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Elenco Fatture con Contropartite"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_elenco_fatture_euro w_report_elenco_fatture_euro

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2405
this.height = 977

end event

on w_report_elenco_fatture_euro.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_elenco_fatture_euro.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_annulla from commandbutton within w_report_elenco_fatture_euro
integer x = 1600
integer y = 780
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_elenco_fatture_euro
integer x = 1989
integer y = 780
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_documento_inizio, ls_cod_documento_fine, ls_cod_cliente, ls_cod_tipo_fat_ven, ls_cod_agente_1, &
       ls_flag_contabilita
long   ll_fattura_inizio, ll_fattura_fine, ll_anno_documento
date   ldd_data_inizio, ldd_data_fine

// da rifare: la scelta cod_agente_1 like % taglia dalla selezione 
// tutte le fatture con cod_agente_1 = null

// nota: ll_anno_documento non usato nella retrieve
ldd_data_inizio = dw_selezione.getitemdate(1,"da_data_fattura")
ldd_data_fine = dw_selezione.getitemdate(1,"a_data_fattura")
ll_fattura_inizio = dw_selezione.getitemnumber(1,"da_num_documento")
ll_fattura_fine = dw_selezione.getitemnumber(1,"a_num_documento")
ls_cod_documento_inizio = dw_selezione.getitemstring(1,"da_cod_documento")
ls_cod_documento_fine = dw_selezione.getitemstring(1,"a_cod_documento")
// ll_anno_documento = dw_selezione.getitemnumber(1,"anno_documento")
ls_cod_tipo_fat_ven = dw_selezione.getitemstring(1,"cod_tipo_fat_ven")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
// ls_cod_agente_1 = dw_selezione.getitemstring(1,"cod_agente_1")
ls_flag_contabilita = dw_selezione.getitemstring(1,"flag_contabilita")

		
if isnull(ls_cod_tipo_fat_ven) then ls_cod_tipo_fat_ven = "%"
if isnull(ls_cod_cliente) then ls_cod_cliente = "%"
//if isnull(ls_cod_agente_1) then ls_cod_agente_1 = "%"
if isnull(ldd_data_inizio) then ldd_data_inizio = date("01/01/1900")
if isnull(ldd_data_fine) then ldd_data_fine = date("31/12/2999")
if isnull(ls_cod_documento_inizio) then ls_cod_documento_inizio = "0"
if isnull(ls_cod_documento_fine) then ls_cod_documento_fine = "zzz"
if ls_flag_contabilita = "T" then ls_flag_contabilita = "%"
if isnull(ll_fattura_inizio) then ll_fattura_inizio = 0
if isnull(ll_fattura_fine) then ll_fattura_fine = 99999999

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()
//dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_documento_inizio, ls_cod_documento_fine, &
//                   ll_fattura_inizio, ll_fattura_fine, ldd_data_inizio, ldd_data_fine, &
//						 ls_cod_cliente, ls_cod_agente_1, ls_cod_tipo_fat_ven, ls_flag_contabilita)

dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_documento_inizio, ls_cod_documento_fine, &
                   ll_fattura_inizio, ll_fattura_fine, ldd_data_inizio, ldd_data_fine, &
						 ls_cod_cliente, ls_cod_tipo_fat_ven, ls_flag_contabilita)


cb_selezione.show()
//cb_ricerca_cliente.hide()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_elenco_fatture_euro
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()


dw_report.hide()
cb_selezione.hide()
//cb_ricerca_cliente.show()

dw_selezione.change_dw_current()

parent.x = 741
parent.y = 885
parent.width = 2405
parent.height = 977

end event

type dw_selezione from uo_cs_xx_dw within w_report_elenco_fatture_euro
integer y = 20
integer width = 2354
integer height = 680
integer taborder = 20
string dataobject = "d_sel_report_elenco_fat"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	end choose
end event

type dw_report from uo_cs_xx_dw within w_report_elenco_fatture_euro
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_elenco_fatture_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

