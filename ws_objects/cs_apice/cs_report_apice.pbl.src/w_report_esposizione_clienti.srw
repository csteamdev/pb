﻿$PBExportHeader$w_report_esposizione_clienti.srw
forward
global type w_report_esposizione_clienti from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_report_esposizione_clienti
end type
type dw_report from uo_cs_xx_dw within w_report_esposizione_clienti
end type
type dw_sel from u_dw_search within w_report_esposizione_clienti
end type
end forward

global type w_report_esposizione_clienti from w_cs_xx_principale
integer width = 4133
integer height = 2180
string title = "Report Esposizione Clienti"
hpb_1 hpb_1
dw_report dw_report
dw_sel dw_sel
end type
global w_report_esposizione_clienti w_report_esposizione_clienti

type variables
private:
boolean ib_export=false
end variables

forward prototypes
public function integer wf_report ()
public function integer wf_totale_fatturato_cliente (string as_cod_cliente, long al_anno, ref decimal ad_fatturato, ref string as_messaggio)
public function integer wf_tot_ordini_bloccati (string as_cod_cliente, long al_anno, ref decimal ad_imponibile, ref string as_messaggio)
public function integer wf_export ()
end prototypes

public function integer wf_report ();boolean lb_cliente = false, lb_inizio=true

string		ls_sql, ls_cliente_da, ls_cliente_a, ls_cod_cliente, ls_rag_soc, ls_fuori_fido, ls_messaggio, ls_con_fido, &
				ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_cod_agente,ls_tipo_riba, ls_esito_pagamento, &
				ls_scadenze, ls_scadenze_data, ls_scadenze_docs, ls_manageriali, ls_manageriali_data, ls_manageriali_docs, &
				ls_cod_nazione, ls_des_nazione, ls_cod_agente_1, ls_rag_soc_agente_1, ls_cod_agente_2, ls_rag_soc_agente_2, &
				ls_cod_pagamento, ls_des_pagamento, ls_casella_mail, ls_cod_deposito, ls_cod_deposito_cliente,ls_cod_tipo_anagrafica, &
				ls_des_tipo_anagrafica, ls_manageriali_dilaz, ls_manageriali_data_dilaz, ls_manageriali_docs_dilaz

long			ll_riga, ll_ret, ll_riga_det, ll_gg_riba, ll_num_documento,ll_count_row_for_cliente, ll_i, ll_prima_riga_cliente,ll_y,ll_count,ll_index

datetime		ldt_data_rif, ldt_oggi_toll_riba, ldt_oggi, ldt_data_documento
date				ld_manager_data_scadenza, ld_manager_data_registrazione

dec{4}		ld_esposizione, ld_fido, ld_diff, ld_perc, ld_fatturato_cliente_corrente, ld_fatturato_cliente_precedente, ld_totale_ordini_bloccati

uo_fido_cliente luo_fido
s_esposizione_cliente lstr_esposizione



ldt_data_rif = dw_sel.getitemdatetime(1,"data_rif")

if isnull(ldt_data_rif) then
	g_mb.messagebox("APICE","Impostare la data di riferimento!",exclamation!)
	return -1
end if

dw_report.object.t_data_rif.text = "Data riferimento: " + string(date(ldt_data_rif))

ls_cliente_da = dw_sel.getitemstring(1,"cod_cliente_da")

ls_cliente_a = dw_sel.getitemstring(1,"cod_cliente_a")

ls_con_fido = dw_sel.getitemstring(1,"fido")

ls_cod_deposito = dw_sel.getitemstring(1,"cod_deposito")
if ls_cod_deposito="" then setnull(ls_cod_deposito)

//Donato 06-11-2008 aggiunto filtro per agente: specifica gestione_fidi PTENDA
ls_cod_agente = dw_sel.getitemstring(1,"cod_agente")
if ls_cod_agente="" then setnull(ls_cod_agente)

//--------------------------------------------------------------------------------

//lettura parametro RIB
//rappresenta il n° di giorni oltre i quali le RIBA vanno considerate scadute
select numero
into :ll_gg_riba
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'RIB' and flag_parametro = 'N';
	
if sqlca.sqlcode = 0 then
else
	ll_gg_riba = -1
end if
if ll_gg_riba = -1 then
	//nessuna tolleranza per le RIBA
	ldt_oggi_toll_riba = ldt_data_rif
else
	//calcola la data dopo la quale considerare insolute le RIBA
	ldt_oggi_toll_riba = datetime(RelativeDate(date(ldt_data_rif), - ll_gg_riba),00:00:00)
end if



ldt_oggi = datetime(today(),now())
//--------------------------------------------------------------------

dw_sel.object.t_stato.text = "Preparazione dati ..."
hpb_1.position = 0

select count(*)
into :ll_count
from anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	(cod_cliente >= :ls_cliente_da or :ls_cliente_a is null) and
	(cod_cliente <= :ls_cliente_a or :ls_cliente_da is null) and
	(fido > 0 or :ls_con_fido = 'N')
	//Donato 06-11-2008 aggiunto filtro per agente: specifica gestione_fidi PTENDA
	and (cod_agente_1 = :ls_cod_agente or isnull(:ls_cod_agente, null) is null)
	//fine modifica -------------------------------------------------------------------
	and (cod_deposito = :ls_cod_deposito or isnull(:ls_cod_deposito, null) is null);

ll_index = 0


hpb_1.maxposition = ll_count
hpb_1.setstep = 1

declare clienti cursor for
select
	cod_cliente,
	rag_soc_1,
	indirizzo,
	cap,
	localita,
	provincia,
	telefono,
	fax,
	cod_nazione,
	cod_agente_1,
	cod_agente_2,
	cod_pagamento,
	casella_mail,
	cod_deposito,
	cod_tipo_anagrafica
from
	anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	(cod_cliente >= :ls_cliente_da or :ls_cliente_a is null) and
	(cod_cliente <= :ls_cliente_a or :ls_cliente_da is null) and
	(fido > 0 or :ls_con_fido = 'N')
	//Donato 06-11-2008 aggiunto filtro per agente: specifica gestione_fidi PTENDA
	and (cod_agente_1 = :ls_cod_agente or isnull(:ls_cod_agente, null) is null)
	//fine modifica -------------------------------------------------------------------
	and (cod_deposito = :ls_cod_deposito or isnull(:ls_cod_deposito, null) is null)
order by
	cod_cliente ASC;

open clienti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore clienti: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

dw_report.reset()
if ib_export then
	dw_report.setredraw(false)
else
	dw_report.setredraw(true)
end if

luo_fido = create uo_fido_cliente

do while true

	fetch
		clienti
	into
		:ls_cod_cliente,
		:ls_rag_soc,
		:ls_indirizzo,
		:ls_cap,
		:ls_localita,
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_cod_nazione,
		:ls_cod_agente_1,
		:ls_cod_agente_2,
		:ls_cod_pagamento,
		:ls_casella_mail,
		:ls_cod_deposito_cliente,
		:ls_cod_tipo_anagrafica;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in open cursore clienti: " + sqlca.sqlerrtext,stopsign!)
		close clienti;
		destroy luo_fido
		return -1
	elseif sqlca.sqlcode = 100 then
		close clienti;
		destroy luo_fido
		exit
	end if
	
	Yield()
	ll_index += 1
	dw_sel.object.t_stato.text = "Elaborazione cliente "+string(ll_index) + " di " + string(ll_count) + " (codice: " + ls_cod_cliente+")"
	hpb_1.stepit()
	
	lb_cliente = false
	
	if not isnull(ls_indirizzo) and len(trim(ls_indirizzo)) > 0 then
		ls_rag_soc += " " + ls_indirizzo
	end if
	
	if not isnull(ls_cap) and len(trim(ls_cap)) > 0 then
		ls_rag_soc += " " + ls_cap
	end if
	
	if not isnull(ls_localita) and len(trim(ls_localita)) > 0 then
		ls_rag_soc += " " + ls_localita
	end if
	
	if not isnull(ls_provincia) and len(trim(ls_provincia)) > 0 then
		ls_rag_soc += " (" + ls_provincia + ")"
	end if
	
	if not isnull(ls_cod_nazione) then
		select des_nazione
		into :ls_des_nazione
		from tab_nazioni
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_nazione = :ls_cod_nazione;
		
		if sqlca.sqlcode = 0 then ls_rag_soc += " - " + ls_des_nazione
	end if
	
	if not isnull(ls_telefono) and len(trim(ls_telefono)) > 0 then
		ls_rag_soc += " Tel. " + ls_telefono
	end if
	
	if not isnull(ls_fax) and len(trim(ls_fax)) > 0 then
		ls_rag_soc += " Fax " + ls_fax
	end if
	
	if not isnull(ls_casella_mail) and len(trim(ls_casella_mail)) > 0 then
		ls_rag_soc += " Email: " + ls_casella_mail
	end if
	

	if not isnull(ls_cod_agente_1) then
		select rag_soc_1
		into :ls_rag_soc_agente_1
		from anag_agenti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_agente = :ls_cod_agente_1;
		
		if sqlca.sqlcode <> 0 then ls_rag_soc_agente_1 = " <Nessun Agente>"
	else
		ls_rag_soc_agente_1 = " <Nessun Agente>"
	end if
	
	if not isnull(ls_cod_agente_2) then
		select rag_soc_1
		into :ls_rag_soc_agente_2
		from anag_agenti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_agente = :ls_cod_agente_2;
		
		if sqlca.sqlcode <> 0 then ls_rag_soc_agente_2 = " <Nessun Agente>"
	else
		ls_rag_soc_agente_2 = " <Nessun Agente>"
	end if
	
	if not isnull(ls_cod_pagamento) then
		select des_pagamento
		into :ls_des_pagamento
		from tab_pagamenti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_pagamento = :ls_cod_pagamento;
		
		if sqlca.sqlcode <> 0 then ls_des_pagamento = ""
	end if
	
	if not isnull(ls_cod_tipo_anagrafica) then
		select des_tipo_anagrafica
		into :ls_des_tipo_anagrafica
		from tab_tipi_anagrafiche
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_anagrafica = :ls_cod_tipo_anagrafica;
		
		if sqlca.sqlcode <> 0 then ls_des_pagamento = ""
	end if
	
	
	
	if luo_fido.uof_fido_cliente(ls_cod_cliente,ld_fido,ls_fuori_fido,ls_messaggio) <> 0 then
		g_mb.messagebox("APICE",ls_messaggio,stopsign!)
		close clienti;
		destroy luo_fido
		return -1
	end if
	
	if luo_fido.uof_esposizione_cliente_ivato(ls_cod_cliente,ldt_data_rif,lstr_esposizione,ls_messaggio) <> 0 then
		g_mb.messagebox("APICE",ls_messaggio,stopsign!)
		close clienti;
		destroy luo_fido
		return -1
	end if
	
	ld_esposizione = lstr_esposizione.saldo_contabile + &
							lstr_esposizione.fatture_non_contabilizzate + &
							lstr_esposizione.ddt_non_fatturati + &
							lstr_esposizione.ordini_assegnati + lstr_esposizione.ordini_non_spediti + lstr_esposizione.ordini_aperti
	
	ld_diff = ld_fido - ld_esposizione
	
	if ld_fido <> 0 then
		ld_perc = round((ld_diff * 100 / ld_fido),2)
	else
		setnull(ld_perc)
	end if
	
	// --------------- aggiunto da EnMe su richiesta di Beatrice 05-06-2013 --------------------------------------------
	if ib_export then
		
		if wf_totale_fatturato_cliente( ls_cod_cliente, f_anno_esercizio(), ref ld_fatturato_cliente_corrente, ref ls_messaggio) < 0 then
			g_mb.messagebox("APICE",ls_messaggio,stopsign!)
			close clienti;
			destroy luo_fido
			return -1
		end if
			
		if wf_totale_fatturato_cliente( ls_cod_cliente, f_anno_esercizio() - 1, ref ld_fatturato_cliente_precedente, ref ls_messaggio) < 0 then
			g_mb.messagebox("APICE",ls_messaggio,stopsign!)
			close clienti;
			destroy luo_fido
			return -1
		end if
		
		if wf_tot_ordini_bloccati( ls_cod_cliente, f_anno_esercizio() - 1, ld_totale_ordini_bloccati, ls_messaggio) < 0 then
			g_mb.messagebox("APICE",ls_messaggio,stopsign!)
			close clienti;
			destroy luo_fido
			return -1
		end if
		
	else
		// servono solo export, mi risparmio le query
		ld_fatturato_cliente_corrente = 0
		ld_fatturato_cliente_precedente = 0
		ld_totale_ordini_bloccati = 0
		
	end if
	//***************************************//
	
	dec{4} ld_valore
	datetime ldt_data_scadenza		
	
	ll_count_row_for_cliente = 0
	
	//Lettura saldo contabile da IMPRESA (AD_SALDO_CONTABILE)
	if s_cs_xx.parametri.impresa and isvalid(sqlci) then
		
		long		ll_anno	
		string 	ls_cod_impresa, ls_cod_capoconto	
		uo_impresa luo_impresa	
			
		ll_anno = year(date(ldt_data_rif))
		
		select cod_capoconto
		into :ls_cod_capoconto
		from anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in lettura codice capoconto cliente: " + sqlca.sqlerrtext, StopSign!)
			close clienti;
			destroy luo_fido
			return -1
		elseif sqlca.sqlcode = 100 then
			g_mb.messagebox("APICE","Errore in lettura codice capoconto cliente: cliente non trovato" + sqlca.sqlerrtext, StopSign!)
			close clienti;
			destroy luo_fido
			return -1
		end if
		
		luo_impresa = create uo_impresa
		
		ls_cod_impresa = luo_impresa.uof_codice_cli_for("C",ls_cod_capoconto, ls_cod_cliente)
		
		destroy luo_impresa
		
		
		// mi carico un le registrazioni manageriali 
		
		dec{4} ld_manager_dare, ld_manager_avere, ld_manager_saldo
		long ll_manager_num_documento
		datetime ldt_manager_data_documento, ldt_manager_data_scadenza, ldt_manager_data_registrazione
		
		declare cu_manageriali  cursor for
		select  isnull(mov_Scadenza.dare,0), isnull(mov_Scadenza.avere,0), isnull(mov_Scadenza.dare,0) - isnull(mov_Scadenza.avere,0), scadenza.num_documento, scadenza.data_documento, scadenza.data_scadenza, reg_pd.data_registrazione
		from mov_scadenza
			join scadenza  		on mov_Scadenza.id_scadenza = scadenza.id_Scadenza
			join conto				on scadenza.id_conto = conto.id_conto
			join mov_contabile	on mov_scadenza.id_mov_contabile = mov_contabile.id_mov_contabile
			join reg_pd			on mov_contabile.id_reg_pd = reg_pd.id_reg_pd
			join registro			on reg_pd.id_registro = registro.id_registro
			join tipo_pagamento	on tipo_pagamento.id_tipo_pagamento = scadenza.id_tipo_pagamento
		where conto .codice = :ls_cod_impresa and registro.tipo_registro = 'MN'	
		order by scadenza.data_scadenza, scadenza.data_documento, scadenza.num_documento
		using sqlci;
		
		open cu_manageriali;
		if sqlci.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in open cursore cu_manageriali scadenze.~r~n" + sqlci.sqlerrtext,stopsign!)
			close clienti;
			destroy luo_fido
			rollback;
			return -1
		end if		
		
		ls_manageriali = "IMPORTO~r~n"
		ls_manageriali_data = "SCADENZA~r~n"
		ls_manageriali_docs = "RIFERIMENTO DOCUMENTO E DATA REGISTRAZIONE CONTABILE~r~n"
		ls_manageriali_dilaz = "IMPORTO~r~n"
		ls_manageriali_data_dilaz = "SCADENZA~r~n"
		ls_manageriali_docs_dilaz = "RIFERIMENTO DOCUMENTO E DATA REGISTRAZIONE CONTABILE~r~n"
		
		do while true
			fetch cu_manageriali into :ld_manager_dare, :ld_manager_avere, :ld_manager_saldo, :ll_manager_num_documento, :ldt_manager_data_documento, :ldt_manager_data_scadenza, :ldt_manager_data_registrazione;
			if sqlci.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in open cursore caricamento manageriali: " + sqlci.sqlerrtext,stopsign!)
				close clienti;
				close cu_manageriali;
				destroy luo_fido
				return -1
			elseif sqlci.sqlcode = 100 then
				close cu_manageriali;
				exit
			end if
			
			ld_manager_data_scadenza = relativedate(date(ldt_manager_data_scadenza),6)
			ld_manager_data_registrazione = date (ldt_manager_data_registrazione)

			if ld_manager_data_scadenza >= ld_manager_data_registrazione then
				// è la registrazione manageriale originale
				ls_manageriali += string(ld_manager_saldo,"###,##0.00") + "~r~n"
				ls_manageriali_data += string(ldt_manager_data_scadenza,"dd/mm/yyyy") + "~r~n"
				ls_manageriali_docs += string(ll_manager_num_documento) + " del " + string(ldt_manager_data_documento,"dd/mm/yyyy") + " Data Reg. " + string(ldt_manager_data_registrazione,"dd/mm/yyyy")  + "~r~n"
			else
				// vuol dire che è stata concessa una ulteriore dilazione
				ls_manageriali_dilaz += string(ld_manager_saldo,"###,##0.00") + "~r~n"
				ls_manageriali_data_dilaz += string(ldt_manager_data_scadenza,"dd/mm/yyyy") + "~r~n"
				ls_manageriali_docs_dilaz += string(ll_manager_num_documento) + " del " + string(ldt_manager_data_documento,"dd/mm/yyyy") + " Data Reg. " + string(ldt_manager_data_registrazione,"dd/mm/yyyy")  + "~r~n"
			end if
		loop
		
		close cu_manageriali;
		
		
		// ----------  Nuovo cursore per escludere la chiusura scadenze causata dalle manageriali -------------
		
		declare dettaglio cursor for
		select sum(isnull(mov_scadenza.dare,0) - isnull(mov_scadenza.avere,0)) as saldo, scadenza.data_scadenza as data_scadenza, 'nonRIBA' , scadenza.esito_pagamento, scadenza.num_documento as num_documento, scadenza.data_documento as data_documento
		from dba.mov_scadenza
				join scadenza on scadenza.id_scadenza = mov_scadenza.id_scadenza
				join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
				join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
				left outer join mov_contabile on  mov_scadenza.id_mov_contabile =  mov_contabile.id_mov_contabile
				left outer join reg_pd 		on mov_contabile.id_reg_pd = reg_pd.id_reg_pd
				left outer join registro 		on reg_pd.id_registro = registro.id_registro
		where 
				dba.scadenza.tipo_scadenza='A' and
				dba.conto.codice=:ls_cod_impresa
				and dba.tipo_pagamento.codice not in ('RIBA','RB') and 
				(registro.tipo_registro <> 'MN'	or registro.tipo_registro is null)	
		group by
		data_scadenza, scadenza.esito_pagamento,scadenza.num_documento, scadenza.data_documento
		having 
		sum(isnull(mov_scadenza.dare,0) - isnull(mov_scadenza.avere,0)) <> 0 
		//----------
		union all
		//----------insoluti 
		select 
		sum(isnull(mov_scadenza.dare,0) - isnull(mov_scadenza.avere,0)) as saldo, 
		data_scadenza, 
		'RIBA' , 
		'I' ,
		scadenza.num_documento, 
		scadenza.data_documento
				from dba.mov_scadenza
				join scadenza on scadenza.id_scadenza = mov_scadenza.id_scadenza
				join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
				left outer join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
				left outer join mov_contabile on  mov_scadenza.id_mov_contabile =  mov_contabile.id_mov_contabile
				left outer join reg_pd 		on mov_contabile.id_reg_pd = reg_pd.id_reg_pd
				left outer join registro 		on reg_pd.id_registro = registro.id_registro
				where 
		dba.conto.codice= :ls_cod_impresa and 
		dba.tipo_pagamento.codice in ('RIBA','RB')  and
		(registro.tipo_registro <> 'MN'	or registro.tipo_registro is null) and
		scadenza.id_scadenza in (
										select distinct mov_scadenza.id_scadenza 
										from mov_scadenza
										join scadenza on scadenza.id_scadenza = mov_scadenza.id_scadenza
										join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
										where dba.mov_scadenza.esito_pagamento='I' 
										and dba.conto.codice= :ls_cod_impresa
										)
		group by
		 data_scadenza, scadenza.num_documento, scadenza.data_documento
		having 
		sum(isnull(mov_scadenza.dare,0) - isnull(mov_scadenza.avere,0)) <> 0 
		// ---------  RIBA APERTE CON SCANDENZA MINORE DI OGGI
		//----------
		union all
		//----------
		select isnull(dare,0) - isnull(avere,0), scadenza.data_scadenza as data_scadenza, 'RIBA' , esito_pagamento, scadenza.num_documento as num_documento, scadenza.data_documento as data_documento
		from dba.scadenza
				join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
				join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
		where dba.scadenza.tipo_scadenza='A' and dba.scadenza.data_scadenza >=:ldt_oggi_toll_riba
					and dba.conto.codice=:ls_cod_impresa and dba.tipo_pagamento.codice in ('RIBA','RB')
					and isnull(dare,0) - isnull(avere,0) <> 0
					and dba.scadenza.esito_pagamento<>'I'
		order by data_scadenza, data_documento, num_documento
		using sqlci;		
		
		// --------------  fine nuovo curosre
		//#################################################
		open dettaglio;
		if sqlci.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in open cursore dettaglio scadenze.~r~n" + sqlci.sqlerrtext,stopsign!)
			close clienti;
			destroy luo_fido
			rollback;
			return -1
		end if		
		
		ls_scadenze = "IMPORTO~r~n"
		ls_scadenze_data = "SCADENZA~r~n"
		ls_scadenze_docs = "RIFERIMENTO DOCUMENTO~r~n"
		
		lb_inizio = true
		
		do while true
			fetch dettaglio into :ld_valore,:ldt_data_scadenza,	:ls_tipo_riba, :ls_esito_pagamento, :ll_num_documento, :ldt_data_documento	;
			
			if sqlci.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in fetch cursore dettaglio riba: " + sqlci.sqlerrtext,stopsign!)
				close clienti;
				close dettaglio;
				destroy luo_fido
				return -1
			elseif sqlci.sqlcode = 100 then
				close dettaglio;
				exit
			end if
						
			if ls_tipo_riba = "nonRIBA" then
				if ldt_data_scadenza < ldt_data_rif then//ldt_oggi then
					//partita scaduta
					ll_riga = dw_report.insertrow(0)
					if lb_inizio then
						ll_prima_riga_cliente = ll_riga
						lb_inizio = false
					end if
					ll_count_row_for_cliente += 1
					dw_report.setitem(ll_riga, "count_for_clienti", ll_count_row_for_cliente)
					
					dw_report.setitem(ll_riga,"cod_cliente",ls_cod_cliente)
					dw_report.setitem(ll_riga,"rag_soc",ls_rag_soc)
					dw_report.setitem(ll_riga,"agente","Principale: " + ls_rag_soc_agente_1 + " Secondario: " + ls_rag_soc_agente_2 )
					dw_report.setitem(ll_riga,"forma_pagamento",ls_cod_pagamento + " - " + ls_des_pagamento)
					dw_report.setitem(ll_riga,"saldo_contabile",lstr_esposizione.saldo_contabile  )
					dw_report.setitem(ll_riga,"saldo_manageriali",lstr_esposizione.saldo_manageriali * -1 )	
					dw_report.setitem(ll_riga,"saldo_manageriali_dilazionate",lstr_esposizione.saldo_manageriali_dilazionate * -1)
					dw_report.setitem(ll_riga,"saldo_manageriali_normali",lstr_esposizione.saldo_manageriali_normali * -1)
					dw_report.setitem(ll_riga,"tot_fat_ven",lstr_esposizione.fatture_non_contabilizzate )
					dw_report.setitem(ll_riga,"tot_bol_ven",lstr_esposizione.ddt_non_fatturati  )
					dw_report.setitem(ll_riga,"tot_ord_ven_ass",lstr_esposizione.ordini_assegnati  )
					dw_report.setitem(ll_riga,"tot_ord_ven_spe",lstr_esposizione.ordini_non_spediti  )
					dw_report.setitem(ll_riga,"tot_ord_ven_a_p",lstr_esposizione.ordini_aperti  )
					dw_report.setitem(ll_riga,"esposizione",ld_esposizione)
					dw_report.setitem(ll_riga,"fido",ld_fido)
					dw_report.setitem(ll_riga,"diff_valore",ld_diff)
					dw_report.setitem(ll_riga,"diff_perc",ld_perc)
					dw_report.setitem(ll_riga,"fuori_fido",ls_fuori_fido)
					dw_report.setitem(ll_riga, "data_scadenza", ldt_data_scadenza)
					dw_report.setitem(ll_riga, "valore", ld_valore)
					
					dw_report.setitem(ll_riga, "partite_scadute_impresa", ld_valore)
					dw_report.setitem(ll_riga, "scaduto_impresa", 1)
					
					ls_scadenze += string(ld_valore,"###,##0.00") + " ( )~r~n"
					ls_scadenze_data += string(ldt_data_scadenza,"dd/mm/yyyy") + "~r~n"
					ls_scadenze_docs += string(ll_num_documento) + " del " + string(ldt_data_documento,"dd/mm/yyyy") + "~r~n"
					
					// aggiunto per export su file
					dw_report.setitem(ll_riga, "tot_ord_ven_bloccati", ld_totale_ordini_bloccati)
					dw_report.setitem(ll_riga, "tot_fatturato_anno_prec", ld_fatturato_cliente_precedente)
					dw_report.setitem(ll_riga, "tot_fatturato_anno_corr", ld_fatturato_cliente_corrente)
					dw_report.setitem(ll_riga, "cod_deposito", ls_cod_deposito_cliente)
					dw_report.setitem(ll_riga, "provincia", ls_provincia)
					dw_report.setitem(ll_riga, "tipo_anagrafica", ls_des_tipo_anagrafica)
				else
					//partita futura
					ll_riga = dw_report.insertrow(0)
					if lb_inizio then
						ll_prima_riga_cliente = ll_riga
						lb_inizio = false
					end if
					ll_count_row_for_cliente += 1
					dw_report.setitem(ll_riga, "count_for_clienti", ll_count_row_for_cliente)
					
					dw_report.setitem(ll_riga,"cod_cliente",ls_cod_cliente)
					dw_report.setitem(ll_riga,"rag_soc",ls_rag_soc)
					dw_report.setitem(ll_riga,"agente","Principale: " + ls_rag_soc_agente_1 + " Secondario: " + ls_rag_soc_agente_2 )
					dw_report.setitem(ll_riga,"forma_pagamento",ls_cod_pagamento + " - " + ls_des_pagamento)
					dw_report.setitem(ll_riga,"saldo_contabile",lstr_esposizione.saldo_contabile)
					dw_report.setitem(ll_riga,"saldo_manageriali",lstr_esposizione.saldo_manageriali * -1)	
					dw_report.setitem(ll_riga,"saldo_manageriali_dilazionate",lstr_esposizione.saldo_manageriali_dilazionate * -1)
					dw_report.setitem(ll_riga,"saldo_manageriali_normali",lstr_esposizione.saldo_manageriali_normali * -1)
					dw_report.setitem(ll_riga,"tot_fat_ven",lstr_esposizione.fatture_non_contabilizzate)
					dw_report.setitem(ll_riga,"tot_bol_ven",lstr_esposizione.ddt_non_fatturati )
					dw_report.setitem(ll_riga,"tot_ord_ven_ass",lstr_esposizione.ordini_assegnati)
					dw_report.setitem(ll_riga,"tot_ord_ven_spe",lstr_esposizione.ordini_non_spediti)
					dw_report.setitem(ll_riga,"tot_ord_ven_a_p",lstr_esposizione.ordini_aperti)
					dw_report.setitem(ll_riga,"esposizione",ld_esposizione)
					dw_report.setitem(ll_riga,"fido",ld_fido)
					dw_report.setitem(ll_riga,"diff_valore",ld_diff)
					dw_report.setitem(ll_riga,"diff_perc",ld_perc)
					dw_report.setitem(ll_riga,"fuori_fido",ls_fuori_fido)
					dw_report.setitem(ll_riga, "data_scadenza", ldt_data_scadenza)
					dw_report.setitem(ll_riga, "valore", ld_valore)
					
					dw_report.setitem(ll_riga, "partite_future_impresa", ld_valore)
					dw_report.setitem(ll_riga, "scaduto_impresa", 0)
					
					ls_scadenze += string(ld_valore,"###,##0.00") + " ( )~r~n"
					ls_scadenze_data += string(ldt_data_scadenza,"dd/mm/yyyy") + "~r~n"
					ls_scadenze_docs += string(ll_num_documento) + " del " + string(ldt_data_documento,"dd/mm/yyyy") + "~r~n"
					// aggiunto per export su file
					dw_report.setitem(ll_riga, "tot_ord_ven_bloccati", ld_totale_ordini_bloccati)
					dw_report.setitem(ll_riga, "tot_fatturato_anno_prec", ld_fatturato_cliente_precedente)
					dw_report.setitem(ll_riga, "tot_fatturato_anno_corr", ld_fatturato_cliente_corrente)
					dw_report.setitem(ll_riga, "cod_deposito", ls_cod_deposito_cliente)
					dw_report.setitem(ll_riga, "provincia", ls_provincia)
					dw_report.setitem(ll_riga, "tipo_anagrafica", ls_des_tipo_anagrafica)
				end if			
			else
				//nel caso di RIBA se dichiarata insoluta deve essere in rosso
				if ls_esito_pagamento = "I" then
					//partita scaduta
					ll_riga = dw_report.insertrow(0)
					if lb_inizio then
						ll_prima_riga_cliente = ll_riga
						lb_inizio = false
					end if
					ll_count_row_for_cliente += 1
					dw_report.setitem(ll_riga, "count_for_clienti", ll_count_row_for_cliente)
					
					dw_report.setitem(ll_riga,"cod_cliente",ls_cod_cliente)
					dw_report.setitem(ll_riga,"rag_soc",ls_rag_soc)
					dw_report.setitem(ll_riga,"agente","Principale: " + ls_rag_soc_agente_1 + " Secondario: " + ls_rag_soc_agente_2 )
					dw_report.setitem(ll_riga,"forma_pagamento",ls_cod_pagamento + " - " + ls_des_pagamento)
					dw_report.setitem(ll_riga,"saldo_contabile",lstr_esposizione.saldo_contabile)
					dw_report.setitem(ll_riga,"saldo_manageriali",lstr_esposizione.saldo_manageriali * -1)	
					dw_report.setitem(ll_riga,"saldo_manageriali_dilazionate",lstr_esposizione.saldo_manageriali_dilazionate * -1)
					dw_report.setitem(ll_riga,"saldo_manageriali_normali",lstr_esposizione.saldo_manageriali_normali * -1)
					dw_report.setitem(ll_riga,"tot_fat_ven",lstr_esposizione.fatture_non_contabilizzate)
					dw_report.setitem(ll_riga,"tot_bol_ven",lstr_esposizione.ddt_non_fatturati)
					dw_report.setitem(ll_riga,"tot_ord_ven_ass",lstr_esposizione.ordini_assegnati)
					dw_report.setitem(ll_riga,"tot_ord_ven_spe",lstr_esposizione.ordini_non_spediti)
					dw_report.setitem(ll_riga,"tot_ord_ven_a_p",lstr_esposizione.ordini_aperti)
					dw_report.setitem(ll_riga,"esposizione",ld_esposizione)
					dw_report.setitem(ll_riga,"fido",ld_fido)
					dw_report.setitem(ll_riga,"diff_valore",ld_diff)
					dw_report.setitem(ll_riga,"diff_perc",ld_perc)
					dw_report.setitem(ll_riga,"fuori_fido",ls_fuori_fido)
					dw_report.setitem(ll_riga, "data_scadenza", ldt_data_scadenza)
					dw_report.setitem(ll_riga, "valore", ld_valore)
					
					dw_report.setitem(ll_riga, "partite_scadute_impresa", ld_valore)
					dw_report.setitem(ll_riga, "scaduto_impresa", 1)
					ls_scadenze += string(ld_valore,"###,##0.00") + " (I)~r~n"
					ls_scadenze_data += string(ldt_data_scadenza,"dd/mm/yyyy") + "~r~n"
					ls_scadenze_docs += string(ll_num_documento) + " del " + string(ldt_data_documento,"dd/mm/yyyy") + "~r~n"
					// aggiunto per export su file
					dw_report.setitem(ll_riga, "tot_ord_ven_bloccati", ld_totale_ordini_bloccati)
					dw_report.setitem(ll_riga, "tot_fatturato_anno_prec", ld_fatturato_cliente_precedente)
					dw_report.setitem(ll_riga, "tot_fatturato_anno_corr", ld_fatturato_cliente_corrente)
					dw_report.setitem(ll_riga, "cod_deposito", ls_cod_deposito_cliente)
					dw_report.setitem(ll_riga, "provincia", ls_provincia)
					dw_report.setitem(ll_riga, "tipo_anagrafica", ls_des_tipo_anagrafica)
				else
					//altrimenti dipende dalla data e dal periodo finestra, se c'è
					if ldt_data_scadenza < ldt_oggi_toll_riba then //ldt_oggi then
						//partita scaduta
						//dw_report.setitem(ll_riga, "partite_scadute_impresa", ld_valore)
						//dw_report.setitem(ll_riga, "scaduto_impresa", 1)
						
						//...continua elaborazione
					else
						//partita futura
						ll_riga = dw_report.insertrow(0)
						if lb_inizio then
							ll_prima_riga_cliente = ll_riga
							lb_inizio = false
						end if
						ll_count_row_for_cliente += 1
						dw_report.setitem(ll_riga, "count_for_clienti", ll_count_row_for_cliente)
						
						dw_report.setitem(ll_riga,"cod_cliente",ls_cod_cliente)
						dw_report.setitem(ll_riga,"rag_soc",ls_rag_soc)
						dw_report.setitem(ll_riga,"agente","Principale: " + ls_rag_soc_agente_1 + " Secondario: " + ls_rag_soc_agente_2 )
						dw_report.setitem(ll_riga,"forma_pagamento",ls_cod_pagamento + " - " + ls_des_pagamento)
						dw_report.setitem(ll_riga,"saldo_contabile",lstr_esposizione.saldo_contabile)
						dw_report.setitem(ll_riga,"saldo_manageriali",lstr_esposizione.saldo_manageriali * -1)	
						dw_report.setitem(ll_riga,"saldo_manageriali_dilazionate",lstr_esposizione.saldo_manageriali_dilazionate * -1)
						dw_report.setitem(ll_riga,"saldo_manageriali_normali",lstr_esposizione.saldo_manageriali_normali * -1)
						dw_report.setitem(ll_riga,"tot_fat_ven",lstr_esposizione.fatture_non_contabilizzate)
						dw_report.setitem(ll_riga,"tot_bol_ven",lstr_esposizione.ddt_non_fatturati)
						dw_report.setitem(ll_riga,"tot_ord_ven_ass",lstr_esposizione.ordini_assegnati)
						dw_report.setitem(ll_riga,"tot_ord_ven_spe",lstr_esposizione.ordini_non_spediti)
						dw_report.setitem(ll_riga,"tot_ord_ven_a_p",lstr_esposizione.ordini_aperti)
						dw_report.setitem(ll_riga,"esposizione",ld_esposizione)
						dw_report.setitem(ll_riga,"fido",ld_fido)
						dw_report.setitem(ll_riga,"diff_valore",ld_diff)
						dw_report.setitem(ll_riga,"diff_perc",ld_perc)
						dw_report.setitem(ll_riga,"fuori_fido",ls_fuori_fido)
						dw_report.setitem(ll_riga, "data_scadenza", ldt_data_scadenza)
						dw_report.setitem(ll_riga, "valore", ld_valore)
						
						dw_report.setitem(ll_riga, "partite_future_impresa", ld_valore)
						dw_report.setitem(ll_riga, "scaduto_impresa", 0)
						
						ls_scadenze += string(ld_valore,"###,##0.00") + " (R)~r~n"
						ls_scadenze_data += string(ldt_data_scadenza,"dd/mm/yyyy") + "~r~n"
						ls_scadenze_docs += string(ll_num_documento) + " del " + string(ldt_data_documento,"dd/mm/yyyy") + "~r~n"
						// aggiunto per export su file
						dw_report.setitem(ll_riga, "tot_ord_ven_bloccati", ld_totale_ordini_bloccati)
						dw_report.setitem(ll_riga, "tot_fatturato_anno_prec", ld_fatturato_cliente_precedente)
						dw_report.setitem(ll_riga, "tot_fatturato_anno_corr", ld_fatturato_cliente_corrente)
						dw_report.setitem(ll_riga, "cod_deposito", ls_cod_deposito_cliente)
						dw_report.setitem(ll_riga, "provincia", ls_provincia)
						dw_report.setitem(ll_riga, "tipo_anagrafica", ls_des_tipo_anagrafica)
					end if			
				end if				
			end if
			
			for ll_y = ll_prima_riga_cliente to ll_riga
				dw_report.setitem(ll_y, "scadenze", ls_scadenze)
				dw_report.setitem(ll_y, "scadenze_data_scadenza", ls_scadenze_data)
				dw_report.setitem(ll_y, "scadenze_documento", ls_scadenze_docs)
				
				dw_report.setitem(ll_y, "manageriali", ls_manageriali)
				dw_report.setitem(ll_y, "manageriali_data_scadenza", ls_manageriali_data)
				dw_report.setitem(ll_y, "manageriali_documento", ls_manageriali_docs)
				
				dw_report.setitem(ll_y, "manageriali_dilazionate", ls_manageriali_dilaz)
				dw_report.setitem(ll_y, "manageriali_data_scadenza_dilazionate", ls_manageriali_data_dilaz)
				dw_report.setitem(ll_y, "manageriali_documento_dilazionate", ls_manageriali_docs_dilaz)
			next
			lb_cliente = true
		loop
		//-----------------------------------------------------------
		
		if not lb_cliente then
			ll_riga = dw_report.insertrow(0)
			ll_count_row_for_cliente += 1
			dw_report.setitem(ll_riga, "count_for_clienti", ll_count_row_for_cliente)
			
			dw_report.setitem(ll_riga,"cod_cliente",ls_cod_cliente)	
			dw_report.setitem(ll_riga,"rag_soc",ls_rag_soc)	
			dw_report.setitem(ll_riga,"agente","Principale: " + ls_rag_soc_agente_1 + " Secondario: " + ls_rag_soc_agente_2 )
			dw_report.setitem(ll_riga,"forma_pagamento",ls_cod_pagamento + " - " + ls_des_pagamento)
			dw_report.setitem(ll_riga,"saldo_contabile",lstr_esposizione.saldo_contabile)	
			dw_report.setitem(ll_riga,"saldo_manageriali",lstr_esposizione.saldo_manageriali * -1)	
			dw_report.setitem(ll_riga,"saldo_manageriali_dilazionate",lstr_esposizione.saldo_manageriali_dilazionate * -1)
			dw_report.setitem(ll_riga,"saldo_manageriali_normali",lstr_esposizione.saldo_manageriali_normali * -1)
			dw_report.setitem(ll_riga,"tot_fat_ven",lstr_esposizione.fatture_non_contabilizzate)	
			dw_report.setitem(ll_riga,"tot_bol_ven",lstr_esposizione.ddt_non_fatturati)	
			dw_report.setitem(ll_riga,"tot_ord_ven_ass",lstr_esposizione.ordini_assegnati)	
			dw_report.setitem(ll_riga,"tot_ord_ven_spe",lstr_esposizione.ordini_non_spediti)	
			dw_report.setitem(ll_riga,"tot_ord_ven_a_p",lstr_esposizione.ordini_aperti)	
			dw_report.setitem(ll_riga,"esposizione",ld_esposizione)	
			dw_report.setitem(ll_riga,"fido",ld_fido)	
			dw_report.setitem(ll_riga,"diff_valore",ld_diff)	
			dw_report.setitem(ll_riga,"diff_perc",ld_perc)	
			dw_report.setitem(ll_riga,"fuori_fido",ls_fuori_fido)
			setnull(ldt_data_scadenza)
			setnull(ld_valore)			
			dw_report.setitem(ll_riga, "data_scadenza", ldt_data_scadenza)
			dw_report.setitem(ll_riga, "valore", ld_valore)
			
			dw_report.setitem(ll_riga, "partite_future_impresa", ld_valore)
			dw_report.setitem(ll_riga, "partite_scadute_impresa", ld_valore)
			dw_report.setitem(ll_riga, "scadenze", ls_scadenze)
			// aggiunto per export su file
			dw_report.setitem(ll_riga, "tot_ord_ven_bloccati", ld_totale_ordini_bloccati)
			dw_report.setitem(ll_riga, "tot_fatturato_anno_prec", ld_fatturato_cliente_precedente)
			dw_report.setitem(ll_riga, "tot_fatturato_anno_corr", ld_fatturato_cliente_corrente)
			dw_report.setitem(ll_riga, "cod_deposito", ls_cod_deposito_cliente)
			dw_report.setitem(ll_riga, "provincia", ls_provincia)
			dw_report.setitem(ll_riga, "tipo_anagrafica", ls_des_tipo_anagrafica)
		end if
	end if
	//fine gestione dati IMPRESA ..........................................................................................	
	//***************************************
loop

if ll_gg_riba <= 0 then
	dw_report.object.t_nota_giorni_rischio.text = "Il programma non utilizza giorni rischio per liberare le partite in essere."
else
	dw_report.object.t_nota_giorni_rischio.text = "Il programma calcola "+string(ll_gg_riba)+" giorni dallo scaduto (RIBA e altro) per liberare le partite in essere."
end if

if ib_export then
	dw_report.setredraw(false)
else
	dw_report.setredraw(true)
end if

dw_report.groupcalc()
dw_report.sort()

dw_sel.object.t_stato.text = "Report Elaborato Correttamente."

return 0
end function

public function integer wf_totale_fatturato_cliente (string as_cod_cliente, long al_anno, ref decimal ad_fatturato, ref string as_messaggio);dec{4} ls_tot_fatturato, ld_tot_nc

select
	sum(imponibile_iva)
into
	:ls_tot_fatturato
from
	tes_fat_ven
	join tab_tipi_fat_ven on tes_fat_ven.cod_azienda = tab_tipi_fat_ven.cod_azienda and tes_fat_ven.cod_tipo_fat_ven = tab_tipi_fat_ven.cod_tipo_fat_ven 
where
	tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and tes_fat_ven.cod_cliente = :as_cod_cliente and
	tes_fat_ven.cod_documento is not null and tes_fat_ven.numeratore_documento is not null and
	tes_fat_ven.anno_documento is not null and tes_fat_ven.num_documento is not null and
	tes_fat_ven.anno_documento = :al_anno and
	tab_tipi_fat_ven.flag_tipo_fat_ven not in ('P','F','N') ;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale fatture di vendita confermate non contabilizzate: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_tot_fatturato) then
	ls_tot_fatturato = 0
end if

// totalizzo le note di credito
select
	sum(imponibile_iva)
into
	:ld_tot_nc
from
	tes_fat_ven
	join tab_tipi_fat_ven on tes_fat_ven.cod_azienda = tab_tipi_fat_ven.cod_azienda and tes_fat_ven.cod_tipo_fat_ven = tab_tipi_fat_ven.cod_tipo_fat_ven 
where
	tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and tes_fat_ven.cod_cliente = :as_cod_cliente and
	tes_fat_ven.cod_documento is not null and tes_fat_ven.numeratore_documento is not null and
	tes_fat_ven.anno_documento is not null and tes_fat_ven.num_documento is not null and
	tes_fat_ven.flag_contabilita = 'N'  and
	tes_fat_ven.anno_documento = :al_anno and
	tab_tipi_fat_ven.flag_tipo_fat_ven in ('N') ;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale fatture di vendita confermate non contabilizzate: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ld_tot_nc) then
	ld_tot_nc = 0
end if

ad_fatturato = ls_tot_fatturato - ld_tot_nc

return 0
end function

public function integer wf_tot_ordini_bloccati (string as_cod_cliente, long al_anno, ref decimal ad_imponibile, ref string as_messaggio);dec{4} ls_tot_fatturato, ld_tot_nc

ad_imponibile = 0

select
	sum(imponibile_iva)
into
	:ad_imponibile
from
	tes_ord_ven
where
	tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
	tes_ord_ven.cod_cliente = :as_cod_cliente and
	tes_ord_ven.anno_registrazione >= :al_anno and
	tes_ord_ven.flag_blocco = 'S' ;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in calcolo totale ordini bloccati del cliente " + as_cod_cliente + " Dettaglio Errore:" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ad_imponibile) then
	ad_imponibile = 0
end if


return 0
end function

public function integer wf_export ();string ls_cod_cliente, ls_cod_cliente_old, ls_path, ls_file,&
ls_rag_soc_1,ls_indirizzo,ls_localita,ls_cap,ls_provincia,ls_telefono,ls_fax,ls_email_amministrazione,ls_cod_pagamento, &
ls_des_pagamento,ls_cod_agente_1,ls_rag_soc_agente_1, ls_cod_agente_2,ls_rag_soc_agente_2
long ll_i, ll_row, ll_ret
dec{4} ld_scaduto_impresa, ld_partite_future_impresa, ld_scaduto_impresa_sum, ld_partite_future_impresa_sum
datastore lds_export


lds_export = create datastore
lds_export.dataobject = "d_report_esposizione_clienti_4_export"
ll_row = 0
ls_cod_cliente_old = ""

for ll_i = 1 to dw_report.rowcount()
	
	ls_cod_cliente = dw_report.getitemstring(ll_i, "cod_cliente")
	
	pcca.mdi_frame.setmicrohelp("Elaborazione Cliente ..." + ls_cod_cliente)
	
	if ls_cod_cliente_old <> ls_cod_cliente then
		if ls_cod_cliente_old <> "" then
			lds_export.setitem(ll_row, "scaduto_impresa", ld_scaduto_impresa_sum)
			lds_export.setitem(ll_row, "partite_future_impresa", ld_partite_future_impresa_sum)
			ld_scaduto_impresa_sum = 0
			ld_partite_future_impresa_sum = 0	
		end if
		
		ll_row=lds_export.insertrow(0)
		
		lds_export.setitem(ll_row, "cod_cliente", dw_report.getitemstring(ll_i, "cod_cliente")  )		
		
		select anag_clienti.rag_soc_1,
				 anag_clienti.indirizzo,
				 anag_clienti.localita,
				 anag_clienti.cap,
				 anag_clienti.provincia,
				 anag_clienti.telefono,
				 anag_clienti.fax,
				 anag_clienti.email_amministrazione,
				 anag_clienti.cod_pagamento,
				 tab_pagamenti.des_pagamento,
				 anag_clienti.cod_agente_1,
				 anag_agenti_1.rag_soc_1,
				 anag_clienti.cod_agente_2,
				 anag_agenti_2.rag_soc_1
		into	 :ls_rag_soc_1,
				 :ls_indirizzo,
				 :ls_localita,
				 :ls_cap,
				 :ls_provincia,
				 :ls_telefono,
				 :ls_fax,
				 :ls_email_amministrazione,
				 :ls_cod_pagamento,
				 :ls_des_pagamento,
				 :ls_cod_agente_1,
				 :ls_rag_soc_agente_1,
				 :ls_cod_agente_2,
				 :ls_rag_soc_agente_2
		from anag_clienti
		left outer join tab_pagamenti on anag_clienti.cod_azienda = tab_pagamenti.cod_azienda and anag_clienti.cod_pagamento = tab_pagamenti.cod_pagamento
		left outer join anag_agenti  anag_agenti_1 on anag_clienti.cod_azienda = anag_agenti_1.cod_azienda and anag_clienti.cod_agente_1 = anag_agenti_1.cod_agente
		left outer join anag_agenti  anag_agenti_2 on anag_clienti.cod_azienda = anag_agenti_2.cod_azienda and anag_clienti.cod_agente_2 = anag_agenti_1.cod_agente
		where 	anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
					anag_clienti.cod_cliente = :ls_cod_cliente   ;
		
		
		lds_export.setitem(ll_row, "rag_soc",  ls_rag_soc_1 )		
		lds_export.setitem(ll_row, "indirizzo",  ls_indirizzo )		
		lds_export.setitem(ll_row, "localita", ls_localita  )		
		lds_export.setitem(ll_row, "cap", ls_cap  )		
		lds_export.setitem(ll_row, "provincia", ls_provincia )		
		lds_export.setitem(ll_row, "telefono", ls_telefono  )		
		lds_export.setitem(ll_row, "fax",  ls_fax )		
		lds_export.setitem(ll_row, "email_amministrazione", ls_email_amministrazione   )		
		lds_export.setitem(ll_row, "agente_1", ls_rag_soc_agente_1  )		
		lds_export.setitem(ll_row, "agente_2", ls_rag_soc_agente_2  )		
		
		lds_export.setitem(ll_row, "forma_pagamento", dw_report.getitemstring(ll_i, "forma_pagamento")  )		
		lds_export.setitem(ll_row, "cod_deposito", dw_report.getitemstring(ll_i, "cod_deposito")  )		
		lds_export.setitem(ll_row, "tipo_anagrafica", dw_report.getitemstring(ll_i, "tipo_anagrafica")  )		
		lds_export.setitem(ll_row, "fuori_fido", dw_report.getitemstring(ll_i, "fuori_fido")  )		
	
		lds_export.setitem(ll_row, "tot_fat_ven", dw_report.getitemnumber(ll_i, "tot_fat_ven")  )		
		lds_export.setitem(ll_row, "tot_bol_ven", dw_report.getitemnumber(ll_i, "tot_bol_ven")  )		
		lds_export.setitem(ll_row, "tot_ord_ven_ass", dw_report.getitemnumber(ll_i, "tot_ord_ven_ass")  )		
		lds_export.setitem(ll_row, "tot_ord_ven_spe", dw_report.getitemnumber(ll_i, "tot_ord_ven_spe")  )		
		lds_export.setitem(ll_row, "tot_ord_ven_a_p", dw_report.getitemnumber(ll_i, "tot_ord_ven_a_p")  )		
		lds_export.setitem(ll_row, "fido", dw_report.getitemnumber(ll_i, "fido")  )		
		lds_export.setitem(ll_row, "diff_valore", dw_report.getitemnumber(ll_i, "diff_valore")  )		
		lds_export.setitem(ll_row, "tot_ord_ven_bloccati", dw_report.getitemnumber(ll_i, "tot_ord_ven_bloccati")  )		
		lds_export.setitem(ll_row, "tot_fatturato_anno_prec", dw_report.getitemnumber(ll_i, "tot_fatturato_anno_prec")  )		
		lds_export.setitem(ll_row, "tot_fatturato_anno_corr", dw_report.getitemnumber(ll_i, "tot_fatturato_anno_corr")  )		
	
	end if
	
	ld_scaduto_impresa = dw_report.getitemnumber(ll_i, "partite_scadute_impresa")
	if isnull(ld_scaduto_impresa) then ld_scaduto_impresa = 0
	ld_partite_future_impresa = dw_report.getitemnumber(ll_i, "partite_future_impresa")
	if isnull(ld_partite_future_impresa) then ld_partite_future_impresa = 0
	
	ld_scaduto_impresa_sum = ld_scaduto_impresa_sum + ld_scaduto_impresa
	ld_partite_future_impresa_sum = ld_partite_future_impresa_sum + ld_partite_future_impresa
	
	ls_cod_cliente_old = ls_cod_cliente

	// ultimo giro
	if ll_i = dw_report.rowcount() then
		lds_export.setitem(ll_row, "scaduto_impresa", ld_scaduto_impresa_sum)
		lds_export.setitem(ll_row, "partite_future_impresa", ld_partite_future_impresa_sum)
	end if
	
next

ll_ret = GetFileSaveName ( "Selezione file", ls_path, ls_file, "XLS", "All Files (*.*),*.*" , guo_functions.uof_get_user_documents_folder(),  32770)
if ll_ret < 1 then return 0

lds_export.saveas(ls_path,  Excel!, true)

return 0

end function

on w_report_esposizione_clienti.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.dw_report=create dw_report
this.dw_sel=create dw_sel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_sel
end on

on w_report_esposizione_clienti.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.dw_report)
destroy(this.dw_sel)
end on

event pc_setwindow;call super::pc_setwindow;string	ls_logo,  ls_null

dw_report.ib_dw_report = true
set_w_options(c_noenablepopup + c_closenosave)
iuo_dw_main = dw_report
dw_report.change_dw_current()
dw_report.set_dw_options(	sqlca, &
								pcca.null_object, &
								c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete, &
								c_nohighlightselected + c_nocursorrowfocusrect)

select	stringa
into	:ls_logo
from	parametri_azienda
where	cod_azienda = :s_cs_xx.cod_azienda and
		flag_parametro = 'S' and
		cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro aziendale LO5 per logo report: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Parametro aziendale LO5 mancante: il logo non sarà visualizzato",stopsign!)
	return -1
end if

dw_report.object.p_logo.filename = s_cs_xx.volume + ls_logo
//dw_report.object.datawindow.print.preview = 'Yes'


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel, &
                 "cod_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(dw_sel, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

event open;call super::open;dw_sel.postevent("ue_reset")
end event

type hpb_1 from hprogressbar within w_report_esposizione_clienti
integer x = 23
integer y = 568
integer width = 4046
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_report from uo_cs_xx_dw within w_report_esposizione_clienti
integer x = 23
integer y = 652
integer width = 4046
integer height = 1396
integer taborder = 10
string dataobject = "d_report_esposizione_clienti_3"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;setpointer(hourglass!)
wf_report()
groupcalc()
sort()
resetupdate()
setpointer(arrow!)

if ib_export then
	wf_export()
end if
end event

event buttonclicked;call super::buttonclicked;integer li_height
string ls_str
		
choose case dwo.name
	case "b_scadenze"
	
		ls_str = describe("DataWindow.Header.2.Height.AutoSize")
		
		if upper(ls_str) = "NO" then
			ls_str = Modify("DataWindow.Header.2.Height.AutoSize=Yes" )
		else
			ls_str=Modify("DataWindow.Header.2.Height.AutoSize=No" )
			ls_str=Modify("DataWindow.Header.2.height=0" )
		end if		
		
	case "b_manageriali"
	
		ls_str = describe("DataWindow.Header.3.Height.AutoSize")
		
		
		if upper(ls_str) = "NO" then
			ls_str=Modify("DataWindow.Header.3.Height.AutoSize=Yes" )
		else
			ls_str=Modify("DataWindow.Header.3.Height.AutoSize=No" )
			ls_str=Modify("DataWindow.Header.3.height=0" )
		end if		
		
		ls_str = describe("DataWindow.Header.4.Height.AutoSize")
		
		
		if upper(ls_str) = "NO" then
			ls_str=Modify("DataWindow.Header.4.Height.AutoSize=Yes" )
		else
			ls_str=Modify("DataWindow.Header.4.Height.AutoSize=No" )
			ls_str=Modify("DataWindow.Header.4.height=0" )
		end if		
		
end choose

groupcalc()
setredraw(true)
end event

type dw_sel from u_dw_search within w_report_esposizione_clienti
event ue_reset ( )
integer x = 23
integer y = 20
integer width = 4046
integer height = 544
integer taborder = 10
string dataobject = "d_sel_esposizione_clienti"
end type

event ue_reset();string		ls_null
setnull(ls_null)
setitem(1,"cod_cliente_da",ls_null)
setitem(1,"cod_cliente_a",ls_null)
setitem(1,"data_rif",datetime(today(),00:00:00.000000))
setitem(1,"fido","S")	

end event

event itemchanged;call super::itemchanged;if dwo.name = "cod_cliente_da" then
	if not isnull(data) and data <> "" then
		setitem(row, "cod_cliente_a", data)
	end if
end if
end event

event buttonclicked;call super::buttonclicked;if isvalid(dwo)  then
	choose case  dwo.name

		case "b_ric_clienti_da"
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_cliente(dw_sel,"cod_cliente_da")	
			
		case "b_ric_clienti_a"
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_cliente(dw_sel,"cod_cliente_a")		
			
		case "b_annulla"
			string		ls_null
			setnull(ls_null)
			setitem(1,"cod_cliente_da",ls_null)
			setitem(1,"cod_cliente_a",ls_null)
			setitem(1,"data_rif",datetime(today(),00:00:00.000000))
			setitem(1,"fido","S")	
			
		case "b_report"
			accepttext()
			dw_report.change_dw_current()
			ib_export=false
			parent.postevent("pc_retrieve")
			
		case "b_export"
			accepttext()
			dw_report.change_dw_current()
			ib_export=true
			parent.postevent("pc_retrieve")
			
	end choose
end if

end event

