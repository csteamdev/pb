﻿$PBExportHeader$w_report_estrattoconto_agenti.srw
$PBExportComments$Report Estrattoconto Agenti
forward
global type w_report_estrattoconto_agenti from w_cs_xx_principale
end type
type dw_tipi_pag_scadenze from datawindow within w_report_estrattoconto_agenti
end type
type cb_annulla from commandbutton within w_report_estrattoconto_agenti
end type
type cb_report from commandbutton within w_report_estrattoconto_agenti
end type
type dw_selezione from uo_cs_xx_dw within w_report_estrattoconto_agenti
end type
type st_1 from statictext within w_report_estrattoconto_agenti
end type
type dw_report from uo_cs_xx_dw within w_report_estrattoconto_agenti
end type
type dw_folder from u_folder within w_report_estrattoconto_agenti
end type
end forward

global type w_report_estrattoconto_agenti from w_cs_xx_principale
integer x = 663
integer y = 740
integer width = 3904
integer height = 1948
string title = "Report Estrattoconto per Agenti"
boolean maxbox = false
boolean resizable = false
dw_tipi_pag_scadenze dw_tipi_pag_scadenze
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
st_1 st_1
dw_report dw_report
dw_folder dw_folder
end type
global w_report_estrattoconto_agenti w_report_estrattoconto_agenti

type variables
transaction sqlca2
string is_cdazie
uo_impresa iuo_impresa
end variables

forward prototypes
public subroutine wf_report ()
end prototypes

public subroutine wf_report ();integer li_scadenza_lire
string ls_cdente, ls_tkclie, ls_ragcog, ls_ragnom, ls_indir1, ls_indir2, ls_cap, ls_comune, ls_cdprov, ls_cod_agente, &
       ls_desc_agente, ls_rag_soc_1, ls_cod_cliente_corris, ls_cod_cliente_par, ls_causale_1, ls_causale_2, ls_causale_3, &
		 ls_causale_4, ls_agente, ls_cod_cliente, ls_sql,ls_cod_capoconto, ls_indirizzo, ls_localita,ls_provincia,ls_cod_cliente_impresa, &
		 ls_cod_profilo, ls_des_profilo, ls_num_documento, ls_tipo_fattura, ls_cod_giro_consegna, ls_lista_clienti, ls_telefono, &
		 ls_flag_salto_pg
datetime ldt_da_data_registrazione, ldt_a_data_registrazione, ldt_da_data_scadenza, ldt_a_data_scadenza, ldt_data_riferimento, &
			ldt_taddoc, ldt_tadsca, ldt_data_documento, ldt_data_scadenza
integer Net, li_id_conto
long ll_num_prod, ll_i, ll_tandoc, ll_tanpro, ll_x, ll_gg_scaduti,ll_righe, ll_righe_lire, ll_y, ll_j, &
	  ll_id_tipo_pagamento, ll_colore,ll_num_clienti_giro
dec{4} ld_importo_dare, ld_importo_avere, ld_importo_documento, ld_importo_rata, ld_saldo_scadenza
double ld_tarata, ld_taimpo
boolean lb_escludi
datastore lds_scadenzario, lds_scadenzario_lire,lds_clienti_giro

dw_report.reset()
dw_report.SetRedraw ( false )
dw_selezione.acceptText()


ldt_da_data_registrazione  = dw_selezione.getitemdatetime(1,"rdt_da_data_registrazione")
ldt_a_data_registrazione   = dw_selezione.getitemdatetime(1,"rdt_a_data_registrazione")
ldt_da_data_scadenza  = dw_selezione.getitemdatetime(1,"rdt_da_data_scadenza")
ldt_a_data_scadenza   = dw_selezione.getitemdatetime(1,"rdt_a_data_scadenza")
ldt_data_riferimento  = dw_selezione.getitemdatetime(1,"rdt_data_riferimento")
ls_causale_1 = dw_selezione.getitemString(1,"rs_causale_1")
ls_causale_2 = dw_selezione.getitemString(1,"rs_causale_2")
ls_causale_3 = dw_selezione.getitemString(1,"rs_causale_3")
ls_causale_4 = dw_selezione.getitemString(1,"rs_causale_4")
ls_agente = dw_selezione.getitemString(1,"rs_agente")
ls_cod_cliente_par = dw_selezione.getitemString(1,"cod_cliente")
ls_cod_giro_consegna = dw_selezione.getitemString(1,"cod_giro_consegna")
ls_flag_salto_pg = dw_selezione.getitemString(1,"flag_pagina_cliente")

//dw_report.reset()
//if ls_flag_salto_pg = "S" then
//	dw_report.dataobject = 'd_report_estrattoconto_agenti_salto_pg'
//else
//	dw_report.dataobject = 'd_report_estrattoconto_agenti'
//end if
//dw_report.set_dw_options(sqlca, &
//									pcca.null_object, &
//									c_nonew + &
//									c_nomodify + &
//									c_nodelete + &
//									c_noenablenewonopen + &
//									c_noenablemodifyonopen + &
//									c_scrollparent + &
//									c_disablecc, &
//									c_noresizedw + &
//									c_nohighlightselected + &
//									c_nocursorrowfocusrect + &
//									c_nocursorrowpointer)
//
//dw_report.change_dw_current()

if isnull(ls_causale_1) and isnull(ls_causale_2) and isnull(ls_causale_3) and isnull(ls_causale_4) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire almeno una causale")
	  return
end if

if isnull(ldt_da_data_registrazione) and isnull(ldt_a_data_registrazione) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire le date di registrazione ")
	  return
end if

if isnull(ldt_da_data_scadenza) and isnull(ldt_a_data_scadenza) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire le date di scadenza")
	  return
end if

ls_sql = "select cod_cliente, cod_capoconto, rag_soc_1, indirizzo, cap, localita, provincia, cod_agente_1, telefono from anag_clienti where cod_azienda = '"+s_cs_xx.cod_azienda+"'"

if not isnull(ls_agente)  then
	ls_sql = ls_sql + " and cod_agente_1 = '" + ls_agente + "'"
end if

if not isnull(ls_cod_cliente_par) and ls_cod_cliente_par <> "" then 
	ls_sql = ls_sql + " and cod_cliente = '" + ls_cod_cliente_par + "'"
end if

if not isnull(ls_cod_giro_consegna) and len(ls_cod_giro_consegna) > 0 then 
	lds_clienti_giro = create datastore
	lds_clienti_giro.dataobject = 'd_ds_estrattoconto_clienti_giri'
	lds_clienti_giro.settransobject(sqlca)
	ll_num_clienti_giro = lds_clienti_giro.retrieve(s_cs_xx.cod_azienda, ls_cod_giro_consegna)
	ls_lista_clienti = ""
	for ll_i = 1 to ll_num_clienti_giro
		if len(ls_lista_clienti) > 0 then
			ls_lista_clienti = ls_lista_clienti + ", "
		end if
		ls_lista_clienti = ls_lista_clienti + "'" + lds_clienti_giro.getitemstring(ll_i, "cod_cliente") + "'"
	next
	
	if len(ls_lista_clienti) > 0 then
		ls_sql = ls_sql + " and cod_cliente in (" + ls_lista_clienti + ")"
	end if
	
	destroy lds_clienti_giro
end if


if isnull(ldt_da_data_registrazione) then  ldt_da_data_registrazione  = datetime(date("01/01/1900"))
if isnull(ldt_a_data_registrazione) then  ldt_a_data_registrazione  = datetime(date("31/12/2900"))
if isnull(ldt_da_data_scadenza)  then  ldt_da_data_scadenza   = datetime(date("01/01/1900"))
if isnull(ldt_a_data_scadenza)  then ldt_a_data_scadenza   = datetime(date("31/12/2999"))
if isnull(ldt_data_riferimento)  then ldt_data_riferimento   = datetime(today())

select stringa
into   :ls_cod_cliente_corris
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CCC';
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Stampa Estrattoconto","Errore lettura parametro aziendale CCC (Codice Cliente per Corrispettivi)")
	ls_cod_cliente_corris = "Nessuno"	// non filtra niente (questo cod. cliente non esiste poichè è di 7 lettere)
end if	

if sqlca.sqlcode = 0 and not isnull(ls_cod_cliente_corris) and ls_cod_cliente_corris <> "" then 
	ls_sql = ls_sql + " and cod_cliente <> '" + ls_cod_cliente_corris + "'"
end if

dw_report.object.data_scadenza_da.text=string(date(ldt_da_data_scadenza))
dw_report.object.data_scadenza_a.text=string(date(ldt_a_data_scadenza))
dw_report.object.data_doc_da.text=string(date(ldt_da_data_registrazione))
dw_report.object.data_doc_a.text=string(date(ldt_a_data_registrazione))

declare cu_clienti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_clienti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN del cursore clienti.~r~n" + sqlca.sqlerrtext)
	return
end if

// SCADENZARIO CON PARTITE IN LIRE
lds_scadenzario_lire = create datastore
lds_scadenzario_lire.DataObject = 'd_ds_scadenzario_cliente_partite_lire'
lds_scadenzario_lire.SetTransObject (sqlci)

// leggo scadenzario normale
lds_scadenzario= create datastore
lds_scadenzario.DataObject = 'd_scadenzario_cliente'
lds_scadenzario.SetTransObject (sqlci)
	

do while true
   fetch cu_clienti into :ls_cod_cliente, :ls_cod_capoconto, :ls_rag_soc_1, :ls_indirizzo, :ls_cap, :ls_localita, :ls_provincia, :ls_cod_agente, :ls_telefono;
   if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then 
		g_mb.messagebox("APICE","Erroe in lettura cursore clienti:" + sqlca.sqlerrtext)
		exit
	end if
	
	Yield()
	
	if isnull(ls_provincia) then ls_provincia = ""
	if isnull(ls_localita) then ls_localita = ""
	if isnull(ls_indirizzo) then ls_indirizzo = ""
	if isnull(ls_cap) then ls_cap = ""
	if isnull(ls_telefono) then ls_telefono = ""
	if isnull(ls_rag_soc_1) then
		g_mb.messagebox("APICE","Attenzione!! Il cliente codice " +ls_cod_cliente+" non ha la ragione sociale; continuo lo stesso ! ")
		ls_rag_soc_1 = "SENZA RAGIONE SOCIALE"
	end if
	
	ls_cod_cliente_impresa = iuo_impresa.uof_codice_cli_for("C",ls_cod_capoconto, ls_cod_cliente)
	
	select id_conto
	into   :li_id_conto
	from   conto
	where  codice = :ls_cod_cliente_impresa
	using  sqlci;
	
	if sqlci.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca cliente " + ls_cod_cliente + " in impresa; questo cliente verrà saltato !!!")
		continue
	end if
	
	ll_righe = lds_scadenzario.retrieve(li_id_conto)
	for ll_i = 1 to ll_righe
		
		Yield()
		
		if lds_scadenzario.getitemnumber(ll_i, "saldo_scadenza") = 0 then continue
		li_scadenza_lire = lds_scadenzario.getitemnumber(ll_i, "id_scadenza_lire")
		if not isnull(li_scadenza_lire) and li_scadenza_lire > 0 then
			ll_righe_lire = lds_scadenzario_lire.retrieve(li_scadenza_lire)
			ll_y = 1
			ls_cod_profilo = lds_scadenzario_lire.getitemstring(ll_y, "prof_registrazione_codice")
			ls_des_profilo = lds_scadenzario_lire.getitemstring(ll_y, "prof_registrazione_descrizione")
			ls_num_documento = lds_scadenzario_lire.getitemstring(ll_y, "reg_pd_num_documento")
			ldt_data_documento = lds_scadenzario_lire.getitemdatetime(ll_y, "reg_pd_data_documento")
			ld_importo_documento = lds_scadenzario_lire.getitemnumber(ll_y, "reg_pd_importo")
			ls_tipo_fattura = lds_scadenzario_lire.getitemstring(ll_y, "reg_pd_tipo_fattura")
		else
			ls_cod_profilo = lds_scadenzario.getitemstring(ll_i, "prof_registrazione_codice")
			ls_des_profilo = lds_scadenzario.getitemstring(ll_i, "prof_registrazione_descrizione")
			ls_num_documento = lds_scadenzario.getitemstring(ll_i, "reg_pd_num_documento")
			ldt_data_documento = lds_scadenzario.getitemdatetime(ll_i, "reg_pd_data_documento")
			ld_importo_documento = lds_scadenzario.getitemnumber(ll_i, "reg_pd_importo")
			ls_tipo_fattura = lds_scadenzario.getitemstring(ll_i, "reg_pd_tipo_fattura")
		end if
		
		// Modifica Michele per esclusione e imostazione colori delle scadenze in base al tipo pagamento 29/10/2003
		
		ll_id_tipo_pagamento = lds_scadenzario.getitemnumber(ll_i,"scadenza_id_tipo_pagamento")
		
		lb_escludi = false
		
		for ll_j = 1 to dw_tipi_pag_scadenze.rowcount()
			if dw_tipi_pag_scadenze.getitemnumber(ll_j,"id_tipo_pagamento") = ll_id_tipo_pagamento then
				if dw_tipi_pag_scadenze.getitemstring(ll_j,"includi") = "N" then
					lb_escludi = true
					exit
				end if
				ll_colore = dw_tipi_pag_scadenze.getitemnumber(ll_j,"colore")
			end if
		next
		
		if lb_escludi then
			continue
		end if
		
		// Fine Modifica
		
		ldt_data_scadenza = lds_scadenzario.getitemdatetime(ll_i, "scadenza_data_scadenza")
		ld_importo_dare = lds_scadenzario.getitemnumber(ll_i, "mov_scadenza_dare")
		ld_importo_avere = lds_scadenzario.getitemnumber(ll_i, "mov_scadenza_avere")
		if isnull(ld_importo_dare) then ld_importo_dare = 0
		if isnull(ld_importo_avere) then ld_importo_avere = 0
		ld_importo_rata = ld_importo_dare - ld_importo_avere
	
		
	
		if (ldt_data_documento < ldt_da_data_registrazione) or (ldt_data_documento > ldt_a_data_registrazione) then continue
		if (ldt_data_scadenza < ldt_da_data_scadenza) or (ldt_data_scadenza > ldt_a_data_scadenza) then continue
		if lds_scadenzario.getitemnumber(ll_i, "saldo_scadenza") = 0 then continue
		
		st_1.text = "Fattura nr." + ls_num_documento
			
		select rag_soc_1
		into   :ls_desc_agente 
		from   anag_agenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_agente = :ls_cod_agente;
				 
		if ls_tipo_fattura = "C" or ls_tipo_fattura = "A" then
			ld_importo_documento = ld_importo_documento * -1
		end if
		
		ll_x=dw_report.insertrow(0)
		dw_report.setrow(ll_x)
		ll_gg_scaduti = DaysAfter (date(ldt_data_scadenza) , date(ldt_data_riferimento))
		dw_report.setitem(ll_x,"data_riferimento", ldt_data_riferimento) 
		dw_report.setitem(ll_x,"cod_agente" , ls_cod_agente) 
		dw_report.setitem(ll_x,"agenti_rag_soc_1"  , ls_desc_agente )
		dw_report.setitem(ll_x, "cod_cliente", ls_cod_cliente_impresa)
		dw_report.setitem(ll_x, "cod_ente", string(li_id_conto))
		dw_report.setitem(ll_x, "rag_soc_1", ls_rag_soc_1)
		dw_report.setitem(ll_x, "indirizzo", ls_indirizzo)
		dw_report.setitem(ll_x, "cap", ls_cap)
		dw_report.setitem(ll_x, "citta",ls_localita)
		dw_report.setitem(ll_x, "prov", ls_provincia)
		dw_report.setitem(ll_x, "cod_causale",ls_cod_profilo)
		dw_report.setitem(ll_x, "num_protocollo_fattura", long(ls_num_documento))
		dw_report.setitem(ll_x, "data_fattura", ldt_data_documento)
		dw_report.setitem(ll_x, "importo_fattura", ld_importo_documento)
		dw_report.setitem(ll_x, "importo_rata", ld_importo_rata)
		dw_report.setitem(ll_x, "data_scadenza", ldt_data_scadenza)
		dw_report.setitem(ll_x, "num_giorni_scaduto", ll_gg_scaduti)
		dw_report.setitem(ll_x, "colore", ll_colore)
		dw_report.setitem(ll_x, "telefono", ls_telefono)
	next
loop
close cu_clienti;

dw_report.setsort("cod_agente a, rag_soc_1 a, data_fattura a, num_protocollo_fattura d")
dw_report.sort()
dw_report.groupcalc()

dw_folder.fu_selecttab(2)

dw_report.Object.DataWindow.Print.PreView = 'Yes'

dw_report.SetRedraw ( true )


end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

windowobject lw_oggetti[]

lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])
lw_oggetti[1] = dw_selezione
lw_oggetti[2] = dw_tipi_pag_scadenze
lw_oggetti[3] = st_1
lw_oggetti[4] = cb_annulla
lw_oggetti[5] = cb_report
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)

dw_folder.fu_selecttab(1)


set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
								 c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect +&
								 c_ViewModeBorderUnchanged + &
								 c_ViewModeColorUnchanged + &
								 c_InactiveDWColorUnchanged + &
								 c_InactiveTextLineCol )

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
									 c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
iuo_dw_main = dw_report

dw_report.object.datawindow.print.preview = "Yes"

dw_tipi_pag_scadenze.postevent("ue_carica")

end event

on w_report_estrattoconto_agenti.create
int iCurrent
call super::create
this.dw_tipi_pag_scadenze=create dw_tipi_pag_scadenze
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.st_1=create st_1
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_pag_scadenze
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.dw_folder
end on

on w_report_estrattoconto_agenti.destroy
call super::destroy
destroy(this.dw_tipi_pag_scadenze)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.st_1)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event close;call super::close;destroy iuo_impresa

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "rs_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_selezione, &
                 "cod_giro_consegna", &
                 sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event open;call super::open;iuo_impresa = CREATE uo_impresa

end event

type dw_tipi_pag_scadenze from datawindow within w_report_estrattoconto_agenti
event ue_carica ( )
integer x = 69
integer y = 840
integer width = 2377
integer height = 640
integer taborder = 90
string dataobject = "d_tipi_pag_scadenze"
boolean border = false
boolean livescroll = true
end type

event ue_carica();long	 ll_id_tipo_pagamento, ll_riga

string ls_codice, ls_descrizione, ls_tipo


setpointer(hourglass!)

declare pagamenti cursor for
select
	id_tipo_pagamento,
	codice,
	descrizione,
	tipo
from
	tipo_pagamento
order by
	codice ASC, descrizione ASC
using
	sqlci;

open pagamenti;

if sqlci.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox("APICE","Errore in open cursore pagamenti: " + sqlci.sqlerrtext,stopsign!)
	return
end if

do while true
	
	fetch
		pagamenti
	into
		:ll_id_tipo_pagamento,
		:ls_codice,
		:ls_descrizione,
		:ls_tipo;
		
	if sqlci.sqlcode < 0 then
		setpointer(arrow!)
		g_mb.messagebox("APICE","Errore in fetch cursore pagamenti: " + sqlci.sqlerrtext,stopsign!)
		close pagamenti;
		return
	elseif sqlci.sqlcode = 100 then
		close pagamenti;
		exit
	end if
		
	ll_riga = insertrow(0)
	
	setitem(ll_riga,"id_tipo_pagamento",ll_id_tipo_pagamento)
	setitem(ll_riga,"codice",ls_codice)
	setitem(ll_riga,"descrizione",ls_descrizione)
	setitem(ll_riga,"tipo",ls_tipo)
	setitem(ll_riga,"includi","S")
	setitem(ll_riga,"colore",16777215)
	
loop

setpointer(arrow!)
end event

event clicked;if dwo.name = "cf_colore" then
	setitem(row,"colore",f_scegli_colore())
end if
end event

event itemchanged;if dwo.name = "includi" and data = "N" then
	setitem(row,"colore",16777215)
end if
end event

type cb_annulla from commandbutton within w_report_estrattoconto_agenti
integer x = 1687
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_estrattoconto_agenti
integer x = 2075
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_report.change_dw_current()

dw_report.triggerevent("pcd_retrieve")
end event

type dw_selezione from uo_cs_xx_dw within w_report_estrattoconto_agenti
integer x = 69
integer y = 140
integer width = 2377
integer height = 700
integer taborder = 10
string dataobject = "d_selezione_estrattoconto_agenti"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then

	string ls_cod_agente_1, ls_nulla

	setnull(ls_nulla)
	if i_colname = "cod_cliente" then
	
		select cod_agente_1
		into :ls_cod_agente_1
		from anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda
			and cod_cliente =:i_coltext;
		
		if sqlca.sqlcode = 0 then
			setitem(i_rownbr, "rs_agente", ls_cod_agente_1)
		end if
	end if
	if i_colname = "rs_agente" then
		setitem(i_rownbr, "cod_cliente", ls_nulla)
	end if


end if

end event

event pcd_new;call super::pcd_new;string ls_messaggio, ls_causale1, ls_causale2, ls_causale3, ls_causale4

select stringa
into :is_cdazie
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda
   and cod_parametro='CAZ';

select stringa
into :ls_causale1
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda
   and cod_parametro='CA1';
dw_selezione.setitem(1,"rs_causale_1",ls_causale1)

select stringa
into :ls_causale2
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda
   and cod_parametro='CA2';
dw_selezione.setitem(1,"rs_causale_2",ls_causale2)
	
select stringa
into :ls_causale3
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda
   and cod_parametro='CA3';
dw_selezione.setitem(1,"rs_causale_3",ls_causale3)
	
select stringa
into :ls_causale4
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda
   and cod_parametro='CA4';
	
dw_selezione.setitem(1,"rs_causale_4",ls_causale4)

dw_selezione.setitem(1,"rdt_data_riferimento",datetime(today()))

dw_selezione.setitem(1,"rdt_da_data_registrazione",datetime(date("01/01/2001"),00:00:00))
dw_selezione.setitem(1,"rdt_a_data_registrazione",datetime(relativedate(today(),180),00:00:00))
dw_selezione.setitem(1,"rdt_da_data_scadenza",datetime(date("01/01/2001"),00:00:00))
dw_selezione.setitem(1,"rdt_a_data_scadenza",datetime(relativedate(today(),180),00:00:00))


end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type st_1 from statictext within w_report_estrattoconto_agenti
integer x = 69
integer y = 1540
integer width = 1591
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_report_estrattoconto_agenti
integer x = 46
integer y = 140
integer width = 3771
integer height = 1640
integer taborder = 20
string dataobject = "d_report_estrattoconto_agenti"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;//wf_report()
integer li_scadenza_lire
string ls_cdente, ls_tkclie, ls_ragcog, ls_ragnom, ls_indir1, ls_indir2, ls_cap, ls_comune, ls_cdprov, ls_cod_agente, &
       ls_desc_agente, ls_rag_soc_1, ls_cod_cliente_corris, ls_cod_cliente_par, ls_causale_1, ls_causale_2, ls_causale_3, &
		 ls_causale_4, ls_agente, ls_cod_cliente, ls_sql,ls_cod_capoconto, ls_indirizzo, ls_localita,ls_provincia,ls_cod_cliente_impresa, &
		 ls_cod_profilo, ls_des_profilo, ls_num_documento, ls_tipo_fattura, ls_cod_giro_consegna, ls_lista_clienti, ls_telefono, &
		 ls_flag_salto_pg
datetime ldt_da_data_registrazione, ldt_a_data_registrazione, ldt_da_data_scadenza, ldt_a_data_scadenza, ldt_data_riferimento, &
			ldt_taddoc, ldt_tadsca, ldt_data_documento, ldt_data_scadenza
integer Net, li_id_conto
long ll_num_prod, ll_i, ll_tandoc, ll_tanpro, ll_x, ll_gg_scaduti,ll_righe, ll_righe_lire, ll_y, ll_j, &
	  ll_id_tipo_pagamento, ll_colore,ll_num_clienti_giro
dec{4} ld_importo_dare, ld_importo_avere, ld_importo_documento, ld_importo_rata, ld_saldo_scadenza
double ld_tarata, ld_taimpo
boolean lb_escludi
datastore lds_scadenzario, lds_scadenzario_lire,lds_clienti_giro

dw_report.reset()
dw_report.SetRedraw ( false )
dw_selezione.acceptText()


ldt_da_data_registrazione  = dw_selezione.getitemdatetime(1,"rdt_da_data_registrazione")
ldt_a_data_registrazione   = dw_selezione.getitemdatetime(1,"rdt_a_data_registrazione")
ldt_da_data_scadenza  = dw_selezione.getitemdatetime(1,"rdt_da_data_scadenza")
ldt_a_data_scadenza   = dw_selezione.getitemdatetime(1,"rdt_a_data_scadenza")
ldt_data_riferimento  = dw_selezione.getitemdatetime(1,"rdt_data_riferimento")
ls_causale_1 = dw_selezione.getitemString(1,"rs_causale_1")
ls_causale_2 = dw_selezione.getitemString(1,"rs_causale_2")
ls_causale_3 = dw_selezione.getitemString(1,"rs_causale_3")
ls_causale_4 = dw_selezione.getitemString(1,"rs_causale_4")
ls_agente = dw_selezione.getitemString(1,"rs_agente")
ls_cod_cliente_par = dw_selezione.getitemString(1,"cod_cliente")
ls_cod_giro_consegna = dw_selezione.getitemString(1,"cod_giro_consegna")
ls_flag_salto_pg = dw_selezione.getitemString(1,"flag_pagina_cliente")

// -----------
dw_report.reset()
if ls_flag_salto_pg = "S" then
	dw_report.dataobject = 'd_report_estrattoconto_agenti_salto_pg'
else
	dw_report.dataobject = 'd_report_estrattoconto_agenti'
end if

dw_report.change_dw_current()
//---------------------

if isnull(ls_causale_1) and isnull(ls_causale_2) and isnull(ls_causale_3) and isnull(ls_causale_4) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire almeno una causale")
	  return
end if

if isnull(ldt_da_data_registrazione) and isnull(ldt_a_data_registrazione) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire le date di registrazione ")
	  return
end if

if isnull(ldt_da_data_scadenza) and isnull(ldt_a_data_scadenza) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire le date di scadenza")
	  return
end if

ls_sql = "select cod_cliente, cod_capoconto, rag_soc_1, indirizzo, cap, localita, provincia, cod_agente_1, telefono from anag_clienti where cod_azienda = '"+s_cs_xx.cod_azienda+"'"

if not isnull(ls_agente)  then
	ls_sql = ls_sql + " and cod_agente_1 = '" + ls_agente + "'"
end if

if not isnull(ls_cod_cliente_par) and ls_cod_cliente_par <> "" then 
	ls_sql = ls_sql + " and cod_cliente = '" + ls_cod_cliente_par + "'"
end if

if not isnull(ls_cod_giro_consegna) and len(ls_cod_giro_consegna) > 0 then 
	lds_clienti_giro = create datastore
	lds_clienti_giro.dataobject = 'd_ds_estrattoconto_clienti_giri'
	lds_clienti_giro.settransobject(sqlca)
	ll_num_clienti_giro = lds_clienti_giro.retrieve(s_cs_xx.cod_azienda, ls_cod_giro_consegna)
	ls_lista_clienti = ""
	for ll_i = 1 to ll_num_clienti_giro
		if len(ls_lista_clienti) > 0 then
			ls_lista_clienti = ls_lista_clienti + ", "
		end if
		ls_lista_clienti = ls_lista_clienti + "'" + lds_clienti_giro.getitemstring(ll_i, "cod_cliente") + "'"
	next
	
	if len(ls_lista_clienti) > 0 then
		ls_sql = ls_sql + " and cod_cliente in (" + ls_lista_clienti + ")"
	end if
	
	destroy lds_clienti_giro
end if


if isnull(ldt_da_data_registrazione) then  ldt_da_data_registrazione  = datetime(date("01/01/1900"))
if isnull(ldt_a_data_registrazione) then  ldt_a_data_registrazione  = datetime(date("31/12/2900"))
if isnull(ldt_da_data_scadenza)  then  ldt_da_data_scadenza   = datetime(date("01/01/1900"))
if isnull(ldt_a_data_scadenza)  then ldt_a_data_scadenza   = datetime(date("31/12/2999"))
if isnull(ldt_data_riferimento)  then ldt_data_riferimento   = datetime(today())

select stringa
into   :ls_cod_cliente_corris
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CCC';
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Stampa Estrattoconto","Errore lettura parametro aziendale CCC (Codice Cliente per Corrispettivi)")
	ls_cod_cliente_corris = "Nessuno"	// non filtra niente (questo cod. cliente non esiste poichè è di 7 lettere)
end if	

if sqlca.sqlcode = 0 and not isnull(ls_cod_cliente_corris) and ls_cod_cliente_corris <> "" then 
	ls_sql = ls_sql + " and cod_cliente <> '" + ls_cod_cliente_corris + "'"
end if

dw_report.object.data_scadenza_da.text=string(date(ldt_da_data_scadenza))
dw_report.object.data_scadenza_a.text=string(date(ldt_a_data_scadenza))
dw_report.object.data_doc_da.text=string(date(ldt_da_data_registrazione))
dw_report.object.data_doc_a.text=string(date(ldt_a_data_registrazione))

declare cu_clienti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_clienti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN del cursore clienti.~r~n" + sqlca.sqlerrtext)
	return
end if

// SCADENZARIO CON PARTITE IN LIRE
lds_scadenzario_lire = create datastore
lds_scadenzario_lire.DataObject = 'd_ds_scadenzario_cliente_partite_lire'
lds_scadenzario_lire.SetTransObject (sqlci)

// leggo scadenzario normale
lds_scadenzario= create datastore
lds_scadenzario.DataObject = 'd_scadenzario_cliente'
lds_scadenzario.SetTransObject (sqlci)
	

do while true
   fetch cu_clienti into :ls_cod_cliente, :ls_cod_capoconto, :ls_rag_soc_1, :ls_indirizzo, :ls_cap, :ls_localita, :ls_provincia, :ls_cod_agente, :ls_telefono;
   if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then 
		g_mb.messagebox("APICE","Erroe in lettura cursore clienti:" + sqlca.sqlerrtext)
		exit
	end if
	
	Yield()
	
	if isnull(ls_provincia) then ls_provincia = ""
	if isnull(ls_localita) then ls_localita = ""
	if isnull(ls_indirizzo) then ls_indirizzo = ""
	if isnull(ls_cap) then ls_cap = ""
	if isnull(ls_telefono) then ls_telefono = ""
	if isnull(ls_rag_soc_1) then
		g_mb.messagebox("APICE","Attenzione!! Il cliente codice " +ls_cod_cliente+" non ha la ragione sociale; continuo lo stesso ! ")
		ls_rag_soc_1 = "SENZA RAGIONE SOCIALE"
	end if
	
	ls_cod_cliente_impresa = iuo_impresa.uof_codice_cli_for("C",ls_cod_capoconto, ls_cod_cliente)
	
	select id_conto
	into   :li_id_conto
	from   conto
	where  codice = :ls_cod_cliente_impresa
	using  sqlci;
	
	if sqlci.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca cliente " + ls_cod_cliente + " in impresa; questo cliente verrà saltato !!!")
		continue
	end if
	
	ll_righe = lds_scadenzario.retrieve(li_id_conto)
	for ll_i = 1 to ll_righe
		
		Yield()
		
		if lds_scadenzario.getitemnumber(ll_i, "saldo_scadenza") = 0 then continue
		li_scadenza_lire = lds_scadenzario.getitemnumber(ll_i, "id_scadenza_lire")
		if not isnull(li_scadenza_lire) and li_scadenza_lire > 0 then
			ll_righe_lire = lds_scadenzario_lire.retrieve(li_scadenza_lire)
			ll_y = 1
			ls_cod_profilo = lds_scadenzario_lire.getitemstring(ll_y, "prof_registrazione_codice")
			ls_des_profilo = lds_scadenzario_lire.getitemstring(ll_y, "prof_registrazione_descrizione")
			ls_num_documento = lds_scadenzario_lire.getitemstring(ll_y, "reg_pd_num_documento")
			ldt_data_documento = lds_scadenzario_lire.getitemdatetime(ll_y, "reg_pd_data_documento")
			ld_importo_documento = lds_scadenzario_lire.getitemnumber(ll_y, "reg_pd_importo")
			ls_tipo_fattura = lds_scadenzario_lire.getitemstring(ll_y, "reg_pd_tipo_fattura")
		else
			ls_cod_profilo = lds_scadenzario.getitemstring(ll_i, "prof_registrazione_codice")
			ls_des_profilo = lds_scadenzario.getitemstring(ll_i, "prof_registrazione_descrizione")
			ls_num_documento = lds_scadenzario.getitemstring(ll_i, "reg_pd_num_documento")
			ldt_data_documento = lds_scadenzario.getitemdatetime(ll_i, "reg_pd_data_documento")
			ld_importo_documento = lds_scadenzario.getitemnumber(ll_i, "reg_pd_importo")
			ls_tipo_fattura = lds_scadenzario.getitemstring(ll_i, "reg_pd_tipo_fattura")
		end if
		
		// Modifica Michele per esclusione e imostazione colori delle scadenze in base al tipo pagamento 29/10/2003
		
		ll_id_tipo_pagamento = lds_scadenzario.getitemnumber(ll_i,"scadenza_id_tipo_pagamento")
		
		lb_escludi = false
		
		for ll_j = 1 to dw_tipi_pag_scadenze.rowcount()
			if dw_tipi_pag_scadenze.getitemnumber(ll_j,"id_tipo_pagamento") = ll_id_tipo_pagamento then
				if dw_tipi_pag_scadenze.getitemstring(ll_j,"includi") = "N" then
					lb_escludi = true
					exit
				end if
				ll_colore = dw_tipi_pag_scadenze.getitemnumber(ll_j,"colore")
			end if
		next
		
		if lb_escludi then
			continue
		end if
		
		// Fine Modifica
		
		ldt_data_scadenza = lds_scadenzario.getitemdatetime(ll_i, "scadenza_data_scadenza")
		ld_importo_dare = lds_scadenzario.getitemnumber(ll_i, "mov_scadenza_dare")
		ld_importo_avere = lds_scadenzario.getitemnumber(ll_i, "mov_scadenza_avere")
		if isnull(ld_importo_dare) then ld_importo_dare = 0
		if isnull(ld_importo_avere) then ld_importo_avere = 0
		ld_importo_rata = ld_importo_dare - ld_importo_avere
	
		
	
		if (ldt_data_documento < ldt_da_data_registrazione) or (ldt_data_documento > ldt_a_data_registrazione) then continue
		if (ldt_data_scadenza < ldt_da_data_scadenza) or (ldt_data_scadenza > ldt_a_data_scadenza) then continue
		if lds_scadenzario.getitemnumber(ll_i, "saldo_scadenza") = 0 then continue
		
		st_1.text = "Fattura nr." + ls_num_documento
			
		select rag_soc_1
		into   :ls_desc_agente 
		from   anag_agenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_agente = :ls_cod_agente;
				 
		if ls_tipo_fattura = "C" or ls_tipo_fattura = "A" then
			ld_importo_documento = ld_importo_documento * -1
		end if
		
		ll_x=dw_report.insertrow(0)
		dw_report.setrow(ll_x)
		ll_gg_scaduti = DaysAfter (date(ldt_data_scadenza) , date(ldt_data_riferimento))
		dw_report.setitem(ll_x,"data_riferimento", ldt_data_riferimento) 
		dw_report.setitem(ll_x,"cod_agente" , ls_cod_agente) 
		dw_report.setitem(ll_x,"agenti_rag_soc_1"  , ls_desc_agente )
		dw_report.setitem(ll_x, "cod_cliente", ls_cod_cliente_impresa)
		dw_report.setitem(ll_x, "cod_ente", string(li_id_conto))
		dw_report.setitem(ll_x, "rag_soc_1", ls_rag_soc_1)
		dw_report.setitem(ll_x, "indirizzo", ls_indirizzo)
		dw_report.setitem(ll_x, "cap", ls_cap)
		dw_report.setitem(ll_x, "citta",ls_localita)
		dw_report.setitem(ll_x, "prov", ls_provincia)
		dw_report.setitem(ll_x, "cod_causale",ls_cod_profilo)
		dw_report.setitem(ll_x, "num_protocollo_fattura", long(ls_num_documento))
		dw_report.setitem(ll_x, "data_fattura", ldt_data_documento)
		dw_report.setitem(ll_x, "importo_fattura", ld_importo_documento)
		dw_report.setitem(ll_x, "importo_rata", ld_importo_rata)
		dw_report.setitem(ll_x, "data_scadenza", ldt_data_scadenza)
		dw_report.setitem(ll_x, "num_giorni_scaduto", ll_gg_scaduti)
		dw_report.setitem(ll_x, "colore", ll_colore)
		dw_report.setitem(ll_x, "telefono", ls_telefono)
	next
loop
close cu_clienti;

dw_report.setsort("cod_agente a, rag_soc_1 a, data_fattura a, num_protocollo_fattura d")
dw_report.sort()
dw_report.groupcalc()

dw_report.Object.DataWindow.Print.PreView = 'Yes'

dw_report.SetRedraw ( true )

dw_folder.fu_selecttab(2)

dw_report.change_dw_current()

iuo_dw_main = dw_report

end event

type dw_folder from u_folder within w_report_estrattoconto_agenti
integer x = 23
integer y = 20
integer width = 3817
integer height = 1800
integer taborder = 100
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

