﻿$PBExportHeader$w_report_etichette_barcode.srw
forward
global type w_report_etichette_barcode from window
end type
type cb_3 from commandbutton within w_report_etichette_barcode
end type
type cbx_1 from checkbox within w_report_etichette_barcode
end type
type em_11 from singlelineedit within w_report_etichette_barcode
end type
type em_10 from singlelineedit within w_report_etichette_barcode
end type
type em_9 from singlelineedit within w_report_etichette_barcode
end type
type em_53 from editmask within w_report_etichette_barcode
end type
type st_53 from statictext within w_report_etichette_barcode
end type
type em_52 from editmask within w_report_etichette_barcode
end type
type st_52 from statictext within w_report_etichette_barcode
end type
type em_51 from editmask within w_report_etichette_barcode
end type
type st_51 from statictext within w_report_etichette_barcode
end type
type em_50 from editmask within w_report_etichette_barcode
end type
type st_50 from statictext within w_report_etichette_barcode
end type
type ddlb_2 from dropdownlistbox within w_report_etichette_barcode
end type
type st_100 from statictext within w_report_etichette_barcode
end type
type st_9 from statictext within w_report_etichette_barcode
end type
type ddlb_1 from dropdownlistbox within w_report_etichette_barcode
end type
type cb_5 from commandbutton within w_report_etichette_barcode
end type
type cb_4 from commandbutton within w_report_etichette_barcode
end type
type st_13 from statictext within w_report_etichette_barcode
end type
type st_12 from statictext within w_report_etichette_barcode
end type
type st_11 from statictext within w_report_etichette_barcode
end type
type st_10 from statictext within w_report_etichette_barcode
end type
type dw_1 from datawindow within w_report_etichette_barcode
end type
type cb_carica_dati from commandbutton within w_report_etichette_barcode
end type
type st_8 from statictext within w_report_etichette_barcode
end type
type em_8 from editmask within w_report_etichette_barcode
end type
type cb_2 from commandbutton within w_report_etichette_barcode
end type
type cb_1 from commandbutton within w_report_etichette_barcode
end type
type em_7 from editmask within w_report_etichette_barcode
end type
type st_7 from statictext within w_report_etichette_barcode
end type
type em_6 from editmask within w_report_etichette_barcode
end type
type st_6 from statictext within w_report_etichette_barcode
end type
type st_5 from statictext within w_report_etichette_barcode
end type
type em_5 from editmask within w_report_etichette_barcode
end type
type st_4 from statictext within w_report_etichette_barcode
end type
type em_4 from editmask within w_report_etichette_barcode
end type
type em_3 from editmask within w_report_etichette_barcode
end type
type st_3 from statictext within w_report_etichette_barcode
end type
type st_2 from statictext within w_report_etichette_barcode
end type
type st_1 from statictext within w_report_etichette_barcode
end type
type em_2 from editmask within w_report_etichette_barcode
end type
type em_1 from editmask within w_report_etichette_barcode
end type
type r_1 from rectangle within w_report_etichette_barcode
end type
type r_2 from rectangle within w_report_etichette_barcode
end type
end forward

global type w_report_etichette_barcode from window
integer width = 3831
integer height = 2560
boolean titlebar = true
string title = "Stampa Etichette"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cb_3 cb_3
cbx_1 cbx_1
em_11 em_11
em_10 em_10
em_9 em_9
em_53 em_53
st_53 st_53
em_52 em_52
st_52 st_52
em_51 em_51
st_51 st_51
em_50 em_50
st_50 st_50
ddlb_2 ddlb_2
st_100 st_100
st_9 st_9
ddlb_1 ddlb_1
cb_5 cb_5
cb_4 cb_4
st_13 st_13
st_12 st_12
st_11 st_11
st_10 st_10
dw_1 dw_1
cb_carica_dati cb_carica_dati
st_8 st_8
em_8 em_8
cb_2 cb_2
cb_1 cb_1
em_7 em_7
st_7 st_7
em_6 em_6
st_6 st_6
st_5 st_5
em_5 em_5
st_4 st_4
em_4 em_4
em_3 em_3
st_3 st_3
st_2 st_2
st_1 st_1
em_2 em_2
em_1 em_1
r_1 r_1
r_2 r_2
end type
global w_report_etichette_barcode w_report_etichette_barcode

type variables
boolean ib_etichette_vuote=true
end variables

on w_report_etichette_barcode.create
this.cb_3=create cb_3
this.cbx_1=create cbx_1
this.em_11=create em_11
this.em_10=create em_10
this.em_9=create em_9
this.em_53=create em_53
this.st_53=create st_53
this.em_52=create em_52
this.st_52=create st_52
this.em_51=create em_51
this.st_51=create st_51
this.em_50=create em_50
this.st_50=create st_50
this.ddlb_2=create ddlb_2
this.st_100=create st_100
this.st_9=create st_9
this.ddlb_1=create ddlb_1
this.cb_5=create cb_5
this.cb_4=create cb_4
this.st_13=create st_13
this.st_12=create st_12
this.st_11=create st_11
this.st_10=create st_10
this.dw_1=create dw_1
this.cb_carica_dati=create cb_carica_dati
this.st_8=create st_8
this.em_8=create em_8
this.cb_2=create cb_2
this.cb_1=create cb_1
this.em_7=create em_7
this.st_7=create st_7
this.em_6=create em_6
this.st_6=create st_6
this.st_5=create st_5
this.em_5=create em_5
this.st_4=create st_4
this.em_4=create em_4
this.em_3=create em_3
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_2=create em_2
this.em_1=create em_1
this.r_1=create r_1
this.r_2=create r_2
this.Control[]={this.cb_3,&
this.cbx_1,&
this.em_11,&
this.em_10,&
this.em_9,&
this.em_53,&
this.st_53,&
this.em_52,&
this.st_52,&
this.em_51,&
this.st_51,&
this.em_50,&
this.st_50,&
this.ddlb_2,&
this.st_100,&
this.st_9,&
this.ddlb_1,&
this.cb_5,&
this.cb_4,&
this.st_13,&
this.st_12,&
this.st_11,&
this.st_10,&
this.dw_1,&
this.cb_carica_dati,&
this.st_8,&
this.em_8,&
this.cb_2,&
this.cb_1,&
this.em_7,&
this.st_7,&
this.em_6,&
this.st_6,&
this.st_5,&
this.em_5,&
this.st_4,&
this.em_4,&
this.em_3,&
this.st_3,&
this.st_2,&
this.st_1,&
this.em_2,&
this.em_1,&
this.r_1,&
this.r_2}
end on

on w_report_etichette_barcode.destroy
destroy(this.cb_3)
destroy(this.cbx_1)
destroy(this.em_11)
destroy(this.em_10)
destroy(this.em_9)
destroy(this.em_53)
destroy(this.st_53)
destroy(this.em_52)
destroy(this.st_52)
destroy(this.em_51)
destroy(this.st_51)
destroy(this.em_50)
destroy(this.st_50)
destroy(this.ddlb_2)
destroy(this.st_100)
destroy(this.st_9)
destroy(this.ddlb_1)
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.dw_1)
destroy(this.cb_carica_dati)
destroy(this.st_8)
destroy(this.em_8)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.em_7)
destroy(this.st_7)
destroy(this.em_6)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.em_5)
destroy(this.st_4)
destroy(this.em_4)
destroy(this.em_3)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_2)
destroy(this.em_1)
destroy(this.r_1)
destroy(this.r_2)
end on

event open;string ls_cod_parametro

if isvalid(s_cs_xx.parametri.parametro_dw_1) then
	if s_cs_xx.parametri.parametro_dw_1.dataobject <> "d_ext_carico_acq_det" then
		cb_carica_dati.enabled = false
	end if
else
	cb_carica_dati.enabled = false
end if

em_1.text = string(dw_1.object.datawindow.label.columns)
em_2.text = string(dw_1.object.datawindow.label.rows)
em_4.text = string(long(dw_1.object.datawindow.label.rows.spacing) / 1000)
em_3.text = string(long(dw_1.object.datawindow.label.columns.spacing) / 1000)
em_6.text = string(long(dw_1.object.datawindow.label.height) / 1000)
em_5.text = string(long(dw_1.object.datawindow.label.width) / 1000)
em_50.text = string(long(dw_1.object.datawindow.print.Margin.Top) / 1000)
em_51.text = string(long(dw_1.object.datawindow.print.Margin.Bottom) / 1000)
em_52.text = string(long(dw_1.object.datawindow.print.Margin.Right) / 1000)
em_53.text = string(long(dw_1.object.datawindow.print.Margin.Left) / 1000)

dw_1.Modify("compute_1.moveable=1")
dw_1.Modify("des_prodotto.moveable=1")
dw_1.Modify("cod_misura.moveable=1")

dw_1.Modify("compute_1.resizable=1")
dw_1.Modify("des_prodotto.resizable=1")
dw_1.Modify("cod_misura.resizable=1")

dw_1.Modify("des_prodotto.width=" + string(dw_1.object.datawindow.label.width))


select cod_parametro
into   :ls_cod_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'ET1';
		 
if sqlca.sqlcode = 0 then
	em_1.text = "1"
	em_2.text = "1"
	em_6.text = "3,8"
	em_5.text = "9,8"
	dw_1.object.datawindow.print.orientation = 2
	ddlb_1.text = "20"
	ddlb_2.text = "14"
	if PrintSetPrinter ("Zebra_ZM400_1") = 1 then
		messagebox("Stampante","ATTENZIONE! Adesso la stampante predefinita è Zebra_ZM400_1")
	end if
end if

end event

type cb_3 from commandbutton within w_report_etichette_barcode
integer x = 2990
integer y = 544
integer width = 389
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Des. Magaz."
end type

event clicked;string ls_cod_prodotto,ls_des_prootto

ls_cod_prodotto = em_9.text

if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then
	g_mb.messagebox("APICE","Inserire in codice valido")
	return
end if

select des_prodotto
into   :ls_des_prootto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode <>0 then
	g_mb.messagebox("APICE","Error in ricerca descriione prodotto~r~n" + sqlca.sqlerrtext)
	return
else
	em_10.text = ls_des_prootto
end if

end event

type cbx_1 from checkbox within w_report_etichette_barcode
integer x = 1125
integer y = 756
integer width = 393
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "LandScape"
boolean lefttext = true
end type

event clicked;if checked = true then
	dw_1.object.datawindow.print.orientation = 1
else
	dw_1.object.datawindow.print.orientation = 2
end if
end event

type em_11 from singlelineedit within w_report_etichette_barcode
integer x = 2240
integer y = 740
integer width = 320
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type em_10 from singlelineedit within w_report_etichette_barcode
integer x = 2240
integer y = 640
integer width = 1486
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type em_9 from singlelineedit within w_report_etichette_barcode
integer x = 2240
integer y = 540
integer width = 731
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type em_53 from editmask within w_report_etichette_barcode
integer x = 1966
integer y = 340
integer width = 329
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type st_53 from statictext within w_report_etichette_barcode
integer x = 1280
integer y = 340
integer width = 672
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Sinistro Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_52 from editmask within w_report_etichette_barcode
integer x = 1966
integer y = 240
integer width = 329
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type st_52 from statictext within w_report_etichette_barcode
integer x = 1253
integer y = 240
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Destro Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_51 from editmask within w_report_etichette_barcode
integer x = 777
integer y = 340
integer width = 329
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type st_51 from statictext within w_report_etichette_barcode
integer x = 91
integer y = 340
integer width = 672
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Inferiore Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_50 from editmask within w_report_etichette_barcode
integer x = 777
integer y = 240
integer width = 329
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type st_50 from statictext within w_report_etichette_barcode
integer x = 64
integer y = 240
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Superiore Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_2 from dropdownlistbox within w_report_etichette_barcode
integer x = 1257
integer y = 640
integer width = 274
integer height = 300
integer taborder = 180
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean sorted = false
boolean vscrollbar = true
string item[] = {"6","8","10","12","14","16","18","20","22","24","26","28"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;long ll_font

ll_font = long(ddlb_1.text(index))
ll_font = ll_font * -1
dw_1.OBJECT.des_prodotto.font.height = ll_font
dw_1.OBJECT.cod_misura.font.height = ll_font

end event

type st_100 from statictext within w_report_etichette_barcode
integer x = 663
integer y = 660
integer width = 567
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Dimensione Testo:"
boolean focusrectangle = false
end type

type st_9 from statictext within w_report_etichette_barcode
integer x = 663
integer y = 560
integer width = 567
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Dimensione Barcode:"
boolean focusrectangle = false
end type

type ddlb_1 from dropdownlistbox within w_report_etichette_barcode
integer x = 1257
integer y = 540
integer width = 274
integer height = 300
integer taborder = 170
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean sorted = false
boolean vscrollbar = true
string item[] = {"6","8","10","12","14","16","18","20","22","24","26","28"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;long ll_font

ll_font = long(ddlb_1.text(index))
ll_font = ll_font * -1
dw_1.OBJECT.compute_1.font.height = ll_font
end event

type cb_5 from commandbutton within w_report_etichette_barcode
integer x = 411
integer y = 740
integer width = 366
integer height = 80
integer taborder = 220
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;dw_1.reset()
ib_etichette_vuote = true
em_7.enabled = true
end event

type cb_4 from commandbutton within w_report_etichette_barcode
integer x = 3337
integer y = 740
integer width = 389
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
end type

event clicked;long ll_riga

dw_1.setfocus()
dw_1.setredraw(false)
ll_riga = dw_1.insertrow(0)
dw_1.setitem(ll_riga, "cod_prodotto", em_9.text)
dw_1.setitem(ll_riga, "des_prodotto", em_10.text)
dw_1.setitem(ll_riga, "cod_misura",   "U.M.: " + em_11.text)
dw_1.setredraw(true)

end event

type st_13 from statictext within w_report_etichette_barcode
integer x = 2720
integer y = 760
integer width = 581
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 15780518
string text = "Aggiungi Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_12 from statictext within w_report_etichette_barcode
integer x = 1691
integer y = 760
integer width = 512
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Unità di Misura:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_11 from statictext within w_report_etichette_barcode
integer x = 1623
integer y = 660
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Descrizione Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_10 from statictext within w_report_etichette_barcode
integer x = 1691
integer y = 560
integer width = 512
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Codice Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_report_etichette_barcode
event ue_imposta_etichetta ( )
integer x = 23
integer y = 860
integer width = 3726
integer height = 1560
integer taborder = 230
string title = "none"
string dataobject = "d_report_etichette_barcode_ext"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_imposta_etichetta();dw_1.object.datawindow.label.columns = long(em_1.text)
dw_1.object.datawindow.label.rows = long(em_2.text)
dw_1.object.datawindow.print.Margin.Top = long(dec(em_50.text) * 1000)
dw_1.object.datawindow.print.Margin.Bottom = long(dec(em_51.text) * 1000)
dw_1.object.datawindow.label.rows.spacing = long(dec(em_4.text) * 1000)
dw_1.object.datawindow.label.columns.spacing = long(dec(em_3.text) * 1000)
dw_1.object.datawindow.print.Margin.Right = long(dec(em_52.text) * 1000)
dw_1.object.datawindow.print.Margin.Left = long(dec(em_53.text) * 1000)
dw_1.object.datawindow.label.height = long(dec(em_6.text) * 1000)
dw_1.object.datawindow.label.width = long(dec(em_5.text) * 1000)

dw_1.OBJECT.compute_1.font.height = -20
dw_1.OBJECT.des_prodotto.font.height = -12
dw_1.OBJECT.cod_misura.font.height = -12

dw_1.Modify("compute_1.moveable=1")
dw_1.Modify("des_prodotto.moveable=1")
dw_1.Modify("cod_misura.moveable=1")

dw_1.Modify("compute_1.resizable=1")
dw_1.Modify("des_prodotto.resizable=1")
dw_1.Modify("cod_misura.resizable=1")

dw_1.Modify("des_prodotto.width=" + string(dw_1.object.datawindow.label.width))

end event

type cb_carica_dati from commandbutton within w_report_etichette_barcode
integer x = 23
integer y = 540
integer width = 366
integer height = 80
integer taborder = 190
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Dati"
end type

event clicked;string  ls_centro_costo, ls_codice,ls_cod_parametro

dec{4}  ld_num, ld_quan_acquisto

long 	  ll_i, ll_ret, ll_righe, ll_y, ll_num_etichette, ll_riga, ll_anno_reg, ll_num_reg, ll_prog_riga, &
		  ll_anno_commessa, ll_num_commessa


datawindow ldw_etichette


if s_cs_xx.parametri.parametro_dw_1.dataobject = "d_ext_carico_acq_det" then
	
	ll_ret = g_mb.messagebox("APICE","Carico i dati delle etichette dai prodotti in entrata?",Question!,YesNo!,2)
	
	if ll_ret = 1 then
		
		ldw_etichette = s_cs_xx.parametri.parametro_dw_1
		
		ll_righe = ldw_etichette.rowcount()
		
		dw_1.dataobject = 'd_report_etichette_barcode_ext'
		dw_1.triggerevent("ue_imposta_etichetta")
		
		for ll_i = 1 to ll_righe
			
			if isnull(ldw_etichette.getitemstring(ll_i,"cod_prodotto")) or len(ldw_etichette.getitemstring(ll_i,"cod_prodotto")) < 1 then
				
				ll_anno_reg = ldw_etichette.getitemnumber(ll_i,"anno_registrazione")
				
				ll_num_reg = ldw_etichette.getitemnumber(ll_i,"num_registrazione")
				
				ll_prog_riga = ldw_etichette.getitemnumber(ll_i,"prog_riga_ord_acq")
				
				select cod_centro_costo,
						 anno_commessa,
						 num_commessa
				into   :ls_centro_costo,
						 :ll_anno_commessa,
						 :ll_num_commessa
				from   det_ord_acq
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_reg and
						 num_registrazione = :ll_num_reg and
						 prog_riga_ordine_acq = :ll_prog_riga;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in lettura dati riga ordine da det_ord_acq: " + sqlca.sqlerrtext)
					return -1
				end if
				
				if not isnull(ls_centro_costo) then
					ls_codice = "60" + ls_centro_costo
				elseif not isnull(ll_anno_commessa) and not isnull(ll_num_commessa) then
					ls_codice = right(string(ll_num_commessa),3)
					choose case len(ls_codice)
						case 1
							ls_codice = "00" + ls_codice
						case 2
							ls_codice = "0" + ls_codice
					end choose
					ls_codice = "50" + right(string(ll_anno_commessa),2) + ls_codice
				else
					continue
				end if
				
				ll_riga = dw_1.insertrow(0)
				
				dw_1.setitem(ll_riga, "cod_prodotto", ls_codice)
				
			else
				
				ld_quan_acquisto = ldw_etichette.getitemnumber(ll_i,"quan_acquisto")
				
				ll_num_etichette = ceiling(ld_quan_acquisto)
				
				for ll_y = 1 to ll_num_etichette
					ll_riga = dw_1.insertrow(0)
					dw_1.setitem(ll_riga, "cod_prodotto", ldw_etichette.getitemstring(ll_i,"cod_prodotto"))
					dw_1.setitem(ll_riga, "des_prodotto", ldw_etichette.getitemstring(ll_i,"des_prodotto"))
					dw_1.setitem(ll_riga, "cod_misura",   "U.M.: " + ldw_etichette.getitemstring(ll_i,"cod_misura"))
				next
				
			end if
			
		next
		
	end if
	
end if

em_1.text = string(long(dw_1.object.datawindow.label.columns),"##0")
em_2.text = string(long(dw_1.object.datawindow.label.rows),"##0")
em_6.text = string(dec(dw_1.object.datawindow.label.height) / 1000,"###,##0.000")
em_4.text = string(dec(dw_1.object.datawindow.label.rows.spacing) / 1000,"###,##0.000")
em_3.text = string(dec(dw_1.object.datawindow.label.columns.spacing) / 1000,"###,##0.000")
em_5.text = string(dec(dw_1.object.datawindow.label.width) / 1000,"###,##0.000")

select cod_parametro
into   :ls_cod_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'ET1';
		 
if sqlca.sqlcode = 0 then
	em_1.text = "1"
	em_2.text = "1"
	em_6.text = "3,8"
	em_5.text = "9,8"
	dw_1.object.datawindow.print.orientation = 2
	ddlb_1.text = "20"
	ddlb_2.text = "14"
	dw_1.Modify("cod_prodotto.moveable=1")
	dw_1.Modify("des_prodotto.moveable=1")
	dw_1.Modify("cod_misura.moveable=1")
	if PrintSetPrinter ("Zebra_ZM400_1") = 1 then
		messagebox("Stampante","ATTENZIONE! Adesso la stampante predefinita è Zebra_ZM400_1")
	end if
end if

dw_1.Object.DataWindow.Print.preview = 'Yes'
dw_1.Object.DataWindow.Print.preview.rulers = 'Yes'


end event

type st_8 from statictext within w_report_etichette_barcode
integer x = 2743
integer y = 280
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num.Pagine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_8 from editmask within w_report_etichette_barcode
integer x = 3200
integer y = 280
integer width = 251
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
string minmax = "1~~999"
end type

type cb_2 from commandbutton within w_report_etichette_barcode
integer x = 23
integer y = 740
integer width = 366
integer height = 80
integer taborder = 210
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampante"
end type

event clicked;printsetup()
end event

type cb_1 from commandbutton within w_report_etichette_barcode
integer x = 23
integer y = 640
integer width = 366
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long ll_i 


em_7.enabled = false

// inserisco n. etichette vuote all'inizio
if (long(em_7.text) > 0 or not isnull(long(em_7.text))) and ib_etichette_vuote then
	for ll_i = 1 to long(em_7.text)
		dw_1.insertrow(1)
	next
end if

// imposto num.copie
dw_1.Object.DataWindow.Print.Copies = long(em_8.text)

// stampo etichette
dw_1.print()
ib_etichette_vuote = false
end event

type em_7 from editmask within w_report_etichette_barcode
integer x = 3200
integer y = 380
integer width = 251
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
string minmax = "0~~999"
end type

type st_7 from statictext within w_report_etichette_barcode
integer x = 2309
integer y = 380
integer width = 837
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num.Etichette Vuote"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_6 from editmask within w_report_etichette_barcode
integer x = 3200
integer y = 40
integer width = 251
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type st_6 from statictext within w_report_etichette_barcode
integer x = 2711
integer y = 40
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Altezza Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_report_etichette_barcode
integer x = 2629
integer y = 140
integer width = 539
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Larghezza Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_5 from editmask within w_report_etichette_barcode
integer x = 3200
integer y = 140
integer width = 251
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type st_4 from statictext within w_report_etichette_barcode
integer x = 1426
integer y = 40
integer width = 526
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Spazio tra Righe:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_4 from editmask within w_report_etichette_barcode
integer x = 1966
integer y = 40
integer width = 251
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type em_3 from editmask within w_report_etichette_barcode
integer x = 1966
integer y = 140
integer width = 251
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type st_3 from statictext within w_report_etichette_barcode
integer x = 1440
integer y = 140
integer width = 512
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Spazio tra Colonne:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_report_etichette_barcode
integer x = 64
integer y = 140
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num. Etichette in Colonna:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_etichette_barcode
integer x = 160
integer y = 40
integer width = 590
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num. Etichette in Riga:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_2 from editmask within w_report_etichette_barcode
integer x = 777
integer y = 140
integer width = 251
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type em_1 from editmask within w_report_etichette_barcode
integer x = 777
integer y = 40
integer width = 251
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
end type

event modified;dw_1.triggerevent("ue_imposta_etichetta")
end event

type r_1 from rectangle within w_report_etichette_barcode
long fillcolor = 12639424
integer x = 23
integer y = 20
integer width = 3726
integer height = 500
end type

type r_2 from rectangle within w_report_etichette_barcode
long fillcolor = 15780518
integer x = 1554
integer y = 520
integer width = 2194
integer height = 320
end type

