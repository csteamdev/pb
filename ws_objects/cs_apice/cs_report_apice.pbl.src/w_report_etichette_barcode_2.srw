﻿$PBExportHeader$w_report_etichette_barcode_2.srw
forward
global type w_report_etichette_barcode_2 from w_cs_xx_principale
end type
type st_50 from statictext within w_report_etichette_barcode_2
end type
type st_2 from statictext within w_report_etichette_barcode_2
end type
type st_1 from statictext within w_report_etichette_barcode_2
end type
type st_5 from statictext within w_report_etichette_barcode_2
end type
type st_6 from statictext within w_report_etichette_barcode_2
end type
type em_6 from editmask within w_report_etichette_barcode_2
end type
type em_5 from editmask within w_report_etichette_barcode_2
end type
type em_7 from editmask within w_report_etichette_barcode_2
end type
type em_8 from editmask within w_report_etichette_barcode_2
end type
type st_8 from statictext within w_report_etichette_barcode_2
end type
type st_7 from statictext within w_report_etichette_barcode_2
end type
type st_4 from statictext within w_report_etichette_barcode_2
end type
type em_4 from editmask within w_report_etichette_barcode_2
end type
type em_3 from editmask within w_report_etichette_barcode_2
end type
type st_52 from statictext within w_report_etichette_barcode_2
end type
type em_52 from editmask within w_report_etichette_barcode_2
end type
type em_53 from editmask within w_report_etichette_barcode_2
end type
type st_53 from statictext within w_report_etichette_barcode_2
end type
type em_50 from editmask within w_report_etichette_barcode_2
end type
type em_51 from editmask within w_report_etichette_barcode_2
end type
type st_51 from statictext within w_report_etichette_barcode_2
end type
type em_1 from editmask within w_report_etichette_barcode_2
end type
type em_2 from editmask within w_report_etichette_barcode_2
end type
type st_3 from statictext within w_report_etichette_barcode_2
end type
type cb_1 from commandbutton within w_report_etichette_barcode_2
end type
type cb_2 from commandbutton within w_report_etichette_barcode_2
end type
type cb_carica_dati from commandbutton within w_report_etichette_barcode_2
end type
type ddlb_1 from dropdownlistbox within w_report_etichette_barcode_2
end type
type st_9 from statictext within w_report_etichette_barcode_2
end type
type st_100 from statictext within w_report_etichette_barcode_2
end type
type ddlb_2 from dropdownlistbox within w_report_etichette_barcode_2
end type
type cb_5 from commandbutton within w_report_etichette_barcode_2
end type
type cb_3 from commandbutton within w_report_etichette_barcode_2
end type
type dw_2 from uo_cs_xx_dw within w_report_etichette_barcode_2
end type
type tab_1 from tab within w_report_etichette_barcode_2
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tab_1 from tab within w_report_etichette_barcode_2
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
type dw_1 from datawindow within w_report_etichette_barcode_2
end type
type cb_4 from commandbutton within w_report_etichette_barcode_2
end type
end forward

global type w_report_etichette_barcode_2 from w_cs_xx_principale
integer width = 3858
integer height = 2412
string title = "Stampa Etichette Barcode"
event ue_setdwcolor ( )
st_50 st_50
st_2 st_2
st_1 st_1
st_5 st_5
st_6 st_6
em_6 em_6
em_5 em_5
em_7 em_7
em_8 em_8
st_8 st_8
st_7 st_7
st_4 st_4
em_4 em_4
em_3 em_3
st_52 st_52
em_52 em_52
em_53 em_53
st_53 st_53
em_50 em_50
em_51 em_51
st_51 st_51
em_1 em_1
em_2 em_2
st_3 st_3
cb_1 cb_1
cb_2 cb_2
cb_carica_dati cb_carica_dati
ddlb_1 ddlb_1
st_9 st_9
st_100 st_100
ddlb_2 ddlb_2
cb_5 cb_5
cb_3 cb_3
dw_2 dw_2
tab_1 tab_1
dw_1 dw_1
cb_4 cb_4
end type
global w_report_etichette_barcode_2 w_report_etichette_barcode_2

type variables
boolean ib_etichette_vuote
end variables

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

if isvalid(s_cs_xx.parametri.parametro_dw_1) then
	if s_cs_xx.parametri.parametro_dw_1.dataobject <> "d_ext_carico_acq_det" then
		cb_carica_dati.enabled = false
	end if
else
	cb_carica_dati.enabled = false
end if

em_1.text = string(dw_1.object.datawindow.label.columns)
em_2.text = string(dw_1.object.datawindow.label.rows)
em_4.text = string(long(dw_1.object.datawindow.label.rows.spacing) / 1000)
em_3.text = string(long(dw_1.object.datawindow.label.columns.spacing) / 1000)
em_6.text = string(long(dw_1.object.datawindow.label.height) / 1000)
em_5.text = string(long(dw_1.object.datawindow.label.width) / 1000)
em_50.text = string(long(dw_1.object.datawindow.print.Margin.Top) / 1000)
em_51.text = string(long(dw_1.object.datawindow.print.Margin.Bottom) / 1000)
em_52.text = string(long(dw_1.object.datawindow.print.Margin.Right) / 1000)
em_53.text = string(long(dw_1.object.datawindow.print.Margin.Left) / 1000)

st_1.visible = true
em_1.visible = true
st_2.visible = true
em_2.visible = true
st_3.visible = true
em_3.visible = true
st_4.visible = true
em_4.visible = true
st_5.visible = true
em_5.visible = true
st_6.visible = true
em_6.visible = true
st_7.visible = true
em_7.visible = true
st_8.visible = true
em_8.visible = true
st_50.visible = true
em_50.visible = true
st_51.visible = true
em_51.visible = true
st_52.visible = true
em_52.visible = true	
st_53.visible = true
em_53.visible = true	

dw_2.visible = false//cb_ricerca_prodotto.visible = false
cb_4.visible = false
cb_5.visible = false
cb_3.visible = false
//cb_ricerca_prodotto_a.visible = false
//cb_ricerca_prodotto_da.visible = false
	
dw_2.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )
//dw_2.settransobject( sqlca)
//dw_2.reset()
//dw_2.insertrow(0)







end event

on w_report_etichette_barcode_2.create
int iCurrent
call super::create
this.st_50=create st_50
this.st_2=create st_2
this.st_1=create st_1
this.st_5=create st_5
this.st_6=create st_6
this.em_6=create em_6
this.em_5=create em_5
this.em_7=create em_7
this.em_8=create em_8
this.st_8=create st_8
this.st_7=create st_7
this.st_4=create st_4
this.em_4=create em_4
this.em_3=create em_3
this.st_52=create st_52
this.em_52=create em_52
this.em_53=create em_53
this.st_53=create st_53
this.em_50=create em_50
this.em_51=create em_51
this.st_51=create st_51
this.em_1=create em_1
this.em_2=create em_2
this.st_3=create st_3
this.cb_1=create cb_1
this.cb_2=create cb_2
this.cb_carica_dati=create cb_carica_dati
this.ddlb_1=create ddlb_1
this.st_9=create st_9
this.st_100=create st_100
this.ddlb_2=create ddlb_2
this.cb_5=create cb_5
this.cb_3=create cb_3
this.dw_2=create dw_2
this.tab_1=create tab_1
this.dw_1=create dw_1
this.cb_4=create cb_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_50
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.st_5
this.Control[iCurrent+5]=this.st_6
this.Control[iCurrent+6]=this.em_6
this.Control[iCurrent+7]=this.em_5
this.Control[iCurrent+8]=this.em_7
this.Control[iCurrent+9]=this.em_8
this.Control[iCurrent+10]=this.st_8
this.Control[iCurrent+11]=this.st_7
this.Control[iCurrent+12]=this.st_4
this.Control[iCurrent+13]=this.em_4
this.Control[iCurrent+14]=this.em_3
this.Control[iCurrent+15]=this.st_52
this.Control[iCurrent+16]=this.em_52
this.Control[iCurrent+17]=this.em_53
this.Control[iCurrent+18]=this.st_53
this.Control[iCurrent+19]=this.em_50
this.Control[iCurrent+20]=this.em_51
this.Control[iCurrent+21]=this.st_51
this.Control[iCurrent+22]=this.em_1
this.Control[iCurrent+23]=this.em_2
this.Control[iCurrent+24]=this.st_3
this.Control[iCurrent+25]=this.cb_1
this.Control[iCurrent+26]=this.cb_2
this.Control[iCurrent+27]=this.cb_carica_dati
this.Control[iCurrent+28]=this.ddlb_1
this.Control[iCurrent+29]=this.st_9
this.Control[iCurrent+30]=this.st_100
this.Control[iCurrent+31]=this.ddlb_2
this.Control[iCurrent+32]=this.cb_5
this.Control[iCurrent+33]=this.cb_3
this.Control[iCurrent+34]=this.dw_2
this.Control[iCurrent+35]=this.tab_1
this.Control[iCurrent+36]=this.dw_1
this.Control[iCurrent+37]=this.cb_4
end on

on w_report_etichette_barcode_2.destroy
call super::destroy
destroy(this.st_50)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.em_6)
destroy(this.em_5)
destroy(this.em_7)
destroy(this.em_8)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_4)
destroy(this.em_4)
destroy(this.em_3)
destroy(this.st_52)
destroy(this.em_52)
destroy(this.em_53)
destroy(this.st_53)
destroy(this.em_50)
destroy(this.em_51)
destroy(this.st_51)
destroy(this.em_1)
destroy(this.em_2)
destroy(this.st_3)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.cb_carica_dati)
destroy(this.ddlb_1)
destroy(this.st_9)
destroy(this.st_100)
destroy(this.ddlb_2)
destroy(this.cb_5)
destroy(this.cb_3)
destroy(this.dw_2)
destroy(this.tab_1)
destroy(this.dw_1)
destroy(this.cb_4)
end on

type st_50 from statictext within w_report_etichette_barcode_2
integer x = 91
integer y = 348
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Superiore Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_report_etichette_barcode_2
integer x = 91
integer y = 248
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num. Etichette in Colonna:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_etichette_barcode_2
integer x = 187
integer y = 148
integer width = 590
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num. Etichette in Riga:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_report_etichette_barcode_2
integer x = 2656
integer y = 248
integer width = 539
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Larghezza Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_6 from statictext within w_report_etichette_barcode_2
integer x = 2738
integer y = 148
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Altezza Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_6 from editmask within w_report_etichette_barcode_2
integer x = 3227
integer y = 148
integer width = 251
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.height = long(dec(em_6.text) * 1000)

end event

type em_5 from editmask within w_report_etichette_barcode_2
integer x = 3227
integer y = 248
integer width = 251
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.width = long(dec(em_5.text) * 1000)

end event

type em_7 from editmask within w_report_etichette_barcode_2
integer x = 3227
integer y = 488
integer width = 251
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
string minmax = "0~~999"
end type

type em_8 from editmask within w_report_etichette_barcode_2
integer x = 3227
integer y = 388
integer width = 251
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
string minmax = "1~~999"
end type

type st_8 from statictext within w_report_etichette_barcode_2
integer x = 2770
integer y = 388
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num.Pagine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_7 from statictext within w_report_etichette_barcode_2
integer x = 2336
integer y = 488
integer width = 837
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num.Etichette Vuote"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_report_etichette_barcode_2
integer x = 1454
integer y = 148
integer width = 526
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Spazio tra Righe:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_4 from editmask within w_report_etichette_barcode_2
integer x = 1993
integer y = 148
integer width = 251
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.rows.spacing = long(dec(em_4.text) * 1000)

end event

type em_3 from editmask within w_report_etichette_barcode_2
integer x = 1993
integer y = 248
integer width = 251
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.columns.spacing = long(dec(em_3.text) * 1000)

end event

type st_52 from statictext within w_report_etichette_barcode_2
integer x = 1280
integer y = 348
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Destro Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_52 from editmask within w_report_etichette_barcode_2
integer x = 1993
integer y = 348
integer width = 329
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Right = long(dec(em_52.text) * 1000)
end event

type em_53 from editmask within w_report_etichette_barcode_2
integer x = 1993
integer y = 448
integer width = 329
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Left = long(dec(em_53.text) * 1000)
end event

type st_53 from statictext within w_report_etichette_barcode_2
integer x = 1307
integer y = 448
integer width = 672
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Sinistro Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_50 from editmask within w_report_etichette_barcode_2
integer x = 805
integer y = 348
integer width = 329
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Top = long(dec(em_50.text) * 1000)

end event

type em_51 from editmask within w_report_etichette_barcode_2
integer x = 805
integer y = 448
integer width = 329
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Bottom = long(dec(em_51.text) * 1000)

end event

type st_51 from statictext within w_report_etichette_barcode_2
integer x = 119
integer y = 448
integer width = 672
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Inferiore Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_1 from editmask within w_report_etichette_barcode_2
integer x = 805
integer y = 148
integer width = 251
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
end type

event modified;dw_1.object.datawindow.label.columns = long(em_1.text)

end event

type em_2 from editmask within w_report_etichette_barcode_2
integer x = 805
integer y = 248
integer width = 251
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
end type

event modified;dw_1.object.datawindow.label.rows = long(em_2.text)

end event

type st_3 from statictext within w_report_etichette_barcode_2
integer x = 1467
integer y = 248
integer width = 512
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Spazio tra Colonne:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_report_etichette_barcode_2
integer x = 421
integer y = 616
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long ll_i 


em_7.enabled = false

// inserisco n. etichette vuote all'inizio
if (long(em_7.text) > 0 or not isnull(long(em_7.text))) and ib_etichette_vuote then
	for ll_i = 1 to long(em_7.text)
		dw_1.insertrow(1)
	next
end if

// imposto num.copie
dw_1.Object.DataWindow.Print.Copies = long(em_8.text)

// stampo etichette
dw_1.print()
ib_etichette_vuote = false
end event

type cb_2 from commandbutton within w_report_etichette_barcode_2
integer x = 809
integer y = 616
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampante"
end type

event clicked;printsetup()
end event

type cb_carica_dati from commandbutton within w_report_etichette_barcode_2
integer x = 32
integer y = 616
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Dati"
end type

event clicked;string  ls_centro_costo, ls_codice, ls_cod_prodotto, ls_cod_comodo

dec{4}  ld_num, ld_quan_acquisto, ld_prezzo_vendita

long 	  ll_i, ll_ret, ll_righe, ll_y, ll_num_etichette, ll_riga, ll_anno_reg, ll_num_reg, ll_prog_riga, &
		  ll_anno_commessa, ll_num_commessa


datawindow ldw_etichette


if s_cs_xx.parametri.parametro_dw_1.dataobject = "d_ext_carico_acq_det" then
	
	ll_ret = g_mb.messagebox("APICE","Carico i dati delle etichette dai prodotti in entrata?",Question!,YesNo!,2)
	
	if ll_ret = 1 then
		
		ldw_etichette = s_cs_xx.parametri.parametro_dw_1
		
		ll_righe = ldw_etichette.rowcount()
		
		//dw_1.dataobject = 'd_report_etichette_barcode_ext'
		
		for ll_i = 1 to ll_righe
			
			if isnull(ldw_etichette.getitemstring(ll_i,"cod_prodotto")) or len(ldw_etichette.getitemstring(ll_i,"cod_prodotto")) < 1 then
				
				ll_anno_reg = ldw_etichette.getitemnumber(ll_i,"anno_registrazione")
				
				ll_num_reg = ldw_etichette.getitemnumber(ll_i,"num_registrazione")
				
				ll_prog_riga = ldw_etichette.getitemnumber(ll_i,"prog_riga_ord_acq")
				
				select cod_centro_costo,
						 anno_commessa,
						 num_commessa
				into   :ls_centro_costo,
						 :ll_anno_commessa,
						 :ll_num_commessa
				from   det_ord_acq
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_reg and
						 num_registrazione = :ll_num_reg and
						 prog_riga_ordine_acq = :ll_prog_riga;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in lettura dati riga ordine da det_ord_acq: " + sqlca.sqlerrtext)
					return -1
				end if
				
				if not isnull(ls_centro_costo) then
					ls_codice = "60" + ls_centro_costo
				elseif not isnull(ll_anno_commessa) and not isnull(ll_num_commessa) then
					ls_codice = right(string(ll_num_commessa),3)
					choose case len(ls_codice)
						case 1
							ls_codice = "00" + ls_codice
						case 2
							ls_codice = "0" + ls_codice
					end choose
					ls_codice = "50" + right(string(ll_anno_commessa),2) + ls_codice
				else
					continue
				end if
				
				ll_riga = dw_1.insertrow(0)
				
				dw_1.setitem(ll_riga, "cod_prodotto", ls_codice)
				
			else
				
				ld_quan_acquisto = ldw_etichette.getitemnumber(ll_i,"quan_acquisto")
				
				ll_num_etichette = ceiling(ld_quan_acquisto)
				ls_cod_prodotto = ldw_etichette.getitemstring(ll_i,"cod_prodotto")
				select cod_comodo,
						 prezzo_vendita						
				into   :ls_cod_comodo,
						 :ld_prezzo_vendita
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
		 
				
				for ll_y = 1 to ll_num_etichette
					ll_riga = dw_1.insertrow(0)
					dw_1.setitem(ll_riga, "cod_prodotto", ldw_etichette.getitemstring(ll_i,"cod_prodotto"))
					dw_1.setitem(ll_riga, "des_prodotto", ldw_etichette.getitemstring(ll_i,"des_prodotto"))
					dw_1.setitem(ll_riga, "cod_comodo", ls_cod_comodo)
					dw_1.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
				next
				
			end if
			
		next
		
	end if
	
end if

em_1.text = string(long(dw_1.object.datawindow.label.columns),"##0")
em_2.text = string(long(dw_1.object.datawindow.label.rows),"##0")
em_6.text = string(dec(dw_1.object.datawindow.label.height) / 1000,"###,##0.000")
em_4.text = string(dec(dw_1.object.datawindow.label.rows.spacing) / 1000,"###,##0.000")
em_3.text = string(dec(dw_1.object.datawindow.label.columns.spacing) / 1000,"###,##0.000")
em_5.text = string(dec(dw_1.object.datawindow.label.width) / 1000,"###,##0.000")

dw_1.Object.DataWindow.Print.preview = 'Yes'
dw_1.Object.DataWindow.Print.preview.rulers = 'Yes'
end event

type ddlb_1 from dropdownlistbox within w_report_etichette_barcode_2
integer x = 1815
integer y = 616
integer width = 274
integer height = 300
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean sorted = false
boolean vscrollbar = true
string item[] = {"6","8","10","12","14","16","18","20","22","24","26","28","30","32","34","36","38"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;long ll_font

ll_font = long(ddlb_1.text(index))
ll_font = ll_font * -1
dw_1.OBJECT.compute_1.font.height = ll_font
end event

type st_9 from statictext within w_report_etichette_barcode_2
integer x = 1243
integer y = 636
integer width = 567
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Dimensione Barcode:"
boolean focusrectangle = false
end type

type st_100 from statictext within w_report_etichette_barcode_2
integer x = 2112
integer y = 636
integer width = 480
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Dimensione Testo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_2 from dropdownlistbox within w_report_etichette_barcode_2
integer x = 2592
integer y = 616
integer width = 274
integer height = 300
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean sorted = false
boolean vscrollbar = true
string item[] = {"6","8","10","12","14","16","18","20","22","24","26","28"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;long ll_font

ll_font = long(ddlb_1.text(index))
ll_font = ll_font * -1
dw_1.OBJECT.des_prodotto.font.height = ll_font
dw_1.OBJECT.cod_prodotto.font.height = ll_font
dw_1.OBJECT.cod_comodo.font.height = ll_font
dw_1.OBJECT.prezzo_vendita.font.height = ll_font
//dw_1.OBJECT.cod_misura.font.height = ll_font

end event

type cb_5 from commandbutton within w_report_etichette_barcode_2
integer x = 69
integer y = 160
integer width = 754
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichetta Grande"
end type

event clicked;long ll_riga

dw_1.reset()
dw_1.dataobject = "d_report_etichette_barcode_ext_grande"
dw_1.settransobject( sqlca)
dw_1.setfocus()
dw_1.reset()
ll_riga = dw_1.insertrow(0)
//dw_1.setitem(ll_riga, "cod_prodotto", em_cod_prodotto.text)
//dw_1.setitem(ll_riga, "des_prodotto", em_des_prodotto.text)
//dw_1.setitem(ll_riga, "cod_comodo", em_cod_comodo.text)
//dw_1.setitem(ll_riga, "prezzo_vendita", dec(em_prezzo_vendita.text))
//dw_1.setredraw(true)
//dw_1.object.datawindow.print.preview = 'Yes'
//
end event

type cb_3 from commandbutton within w_report_etichette_barcode_2
integer x = 891
integer y = 160
integer width = 754
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichetta Piccola"
end type

event clicked;long ll_riga

dw_1.reset()
dw_1.dataobject = "d_report_etichette_barcode_ext_pic"
dw_1.settransobject( sqlca)
dw_1.setfocus()
dw_1.reset()
ll_riga = dw_1.insertrow(0)
//dw_1.setitem(ll_riga, "cod_prodotto", em_cod_prodotto.text)
//dw_1.setitem(ll_riga, "des_prodotto", em_des_prodotto.text)
//dw_1.setitem(ll_riga, "cod_comodo", em_cod_comodo.text)
//dw_1.setitem(ll_riga, "prezzo_vendita", dec(em_prezzo_vendita.text))
//dw_1.setredraw(true)
//dw_1.object.datawindow.print.preview = 'Yes'
end event

type dw_2 from uo_cs_xx_dw within w_report_etichette_barcode_2
integer x = 69
integer y = 260
integer width = 3611
integer height = 280
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_dati_1"
boolean border = false
end type

event getfocus;/// ** via l'ancestor
end event

event itemchanged;call super::itemchanged;/// ** via l'ancestor

choose case i_colname
	case "cod_prodotto_da"
		setitem( 1, "cod_prodotto_a", i_coltext)
end choose
end event

event losefocus;/// ** via l'ancestor
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_2,"cod_prodotto_da")
		
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_2,"cod_prodotto_a")
		
end choose
end event

type tab_1 from tab within w_report_etichette_barcode_2
integer x = 23
integer y = 20
integer width = 3771
integer height = 580
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

event clicked;if index = 1 then
	st_1.visible = true
	em_1.visible = true
	st_2.visible = true
	em_2.visible = true
	st_3.visible = true
	em_3.visible = true
	st_4.visible = true
	em_4.visible = true
	st_5.visible = true
	em_5.visible = true
	st_6.visible = true
	em_6.visible = true
	st_7.visible = true
	em_7.visible = true
	st_8.visible = true
	em_8.visible = true
	st_50.visible = true
	em_50.visible = true
	st_51.visible = true
	em_51.visible = true
	st_52.visible = true
	em_52.visible = true	
	st_53.visible = true
	em_53.visible = true	
	
	dw_2.visible = false
	cb_4.visible = false
//	cb_ricerca_prodotto_a.visible = false
//	cb_ricerca_prodotto_da.visible = false
	cb_5.visible = false
	cb_3.visible = false
	//cb_ricerca_prodotto.visible = false
else
	st_1.visible = false
	em_1.visible = false
	st_2.visible = false
	em_2.visible = false
	st_3.visible = false
	em_3.visible = false
	st_4.visible = false
	em_4.visible = false
	st_5.visible = false
	em_5.visible = false
	st_6.visible = false
	em_6.visible = false
	st_7.visible = false
	em_7.visible = false
	st_8.visible = false
	em_8.visible = false
	st_50.visible = false
	em_50.visible = false
	st_51.visible = false
	em_51.visible = false
	st_52.visible = false
	em_52.visible = false	
	st_53.visible = false
	em_53.visible = false		
	
dw_2.visible = true
cb_4.visible = true
//cb_ricerca_prodotto_a.visible = true
//cb_ricerca_prodotto_da.visible = true
cb_5.visible = true
cb_3.visible = true
end if
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3735
integer height = 456
long backcolor = 12639424
string text = "Dati"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3735
integer height = 456
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type dw_1 from datawindow within w_report_etichette_barcode_2
integer x = 46
integer y = 720
integer width = 3771
integer height = 1560
integer taborder = 10
string title = "none"
string dataobject = "d_report_etichette_barcode_ext_new"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_4 from commandbutton within w_report_etichette_barcode_2
integer x = 2263
integer y = 480
integer width = 389
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
end type

event clicked;string ls_cod_prodotto, ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_prodotto_c, ls_des_prodotto_c, ls_cod_misura_c, ls_cod_comodo_c
dec    ld_prezzo_vendita
long   ll_riga, ls_caso

dw_2.accepttext()
ls_cod_prodotto = dw_2.getitemstring( 1,"cod_prodotto")
ls_cod_prodotto_da = dw_2.getitemstring( 1,"cod_prodotto_da")
ls_cod_prodotto_a = dw_2.getitemstring( 1,"cod_prodotto_a")

if dw_1.rowcount() = 1 and (dw_1.getitemstring( 1, "cod_prodotto") = "" or isnull(dw_1.getitemstring( 1, "cod_prodotto")) or len(dw_1.getitemstring( 1, "cod_prodotto")) = 0 ) then
	dw_1.reset()
end if

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
	ls_cod_prodotto = ""
else
	if left( ls_cod_prodotto, 1) = "*" then ls_cod_prodotto = "%" + mid(ls_cod_prodotto,2)
	if right( ls_cod_prodotto, 1) = "*" then ls_cod_prodotto = left(ls_cod_prodotto,len(ls_cod_prodotto)-1) + "%"
end if	

if not isnull(ls_cod_prodotto_da) and ls_cod_prodotto_da <> "" and not isnull(ls_cod_prodotto_a) and ls_cod_prodotto_a <> "" then
	
	ls_caso = 1
	
	if ls_cod_prodotto <> "" then
	
		declare cu_prodotti_1 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto like :ls_cod_prodotto and
				 cod_prodotto >= :ls_cod_prodotto_da and
				 cod_prodotto <= :ls_cod_prodotto_a
		order by cod_prodotto ASC;
	else
		
		ls_caso = 5
		
		declare cu_prodotti_5 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto >= :ls_cod_prodotto_da and
				 cod_prodotto <= :ls_cod_prodotto_a
		order by cod_prodotto ASC;		
	end if

elseif (isnull(ls_cod_prodotto_da) or ls_cod_prodotto_da = "") and (isnull(ls_cod_prodotto_a) or ls_cod_prodotto_a = "") then
	
	ls_caso = 2
	
	if ls_cod_prodotto <> "" then
		declare cu_prodotti_2 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto like :ls_cod_prodotto 
		order by cod_prodotto ASC;	
	else
		ls_caso = 6
		
		declare cu_prodotti_6 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda 
		order by cod_prodotto ASC;	
	end if
	
	
elseif not isnull(ls_cod_prodotto_da) and ls_cod_prodotto_da <> "" and ( isnull(ls_cod_prodotto_a) or ls_cod_prodotto_a = "") then
	
	ls_caso = 3
	if ls_cod_prodotto <> "" then
		declare cu_prodotti_3 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto like :ls_cod_prodotto and
				 cod_prodotto >= :ls_cod_prodotto_da
		order by cod_prodotto ASC;
	else
		ls_caso = 7
		
		declare cu_prodotti_7 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto >= :ls_cod_prodotto_da
		order by cod_prodotto ASC;		
	end if
elseif (isnull(ls_cod_prodotto_da) or ls_cod_prodotto_da = "") and not isnull(ls_cod_prodotto_a) and ls_cod_prodotto_a <> "" then
	
	ls_caso = 4
	if ls_cod_prodotto <> "" then
		declare cu_prodotti_4 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto like :ls_cod_prodotto and
				 cod_prodotto <= :ls_cod_prodotto_a
		order by cod_prodotto ASC;
	else
		
		ls_caso = 8
		
		declare cu_prodotti_8 cursor for
		select cod_prodotto,
				 des_prodotto,
				 cod_misura_mag,
				 cod_comodo,
				 prezzo_vendita
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto <= :ls_cod_prodotto_a
		order by cod_prodotto ASC;		
		
	end if
end if

choose case ls_caso
	case 1
		open cu_prodotti_1;
	case 2
		open cu_prodotti_2;
	case 3
		open cu_prodotti_3;
	case 4
		open cu_prodotti_4;
	case 5
		open cu_prodotti_5;
	case 6
		open cu_prodotti_6;
	case 7
		open cu_prodotti_7;
	case 8
		open cu_prodotti_8;		
end choose

do while 1=1
	
	choose case ls_caso
		case 1
			fetch cu_prodotti_1 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;
		case 2
			fetch cu_prodotti_2 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;
		case 3
			fetch cu_prodotti_3 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;
		case 4
			fetch cu_prodotti_4 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;
										  
		case 5
			fetch cu_prodotti_5 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;
										  
		case 6
			fetch cu_prodotti_6 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;
										  
		case 7
			fetch cu_prodotti_7 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;
										  
		case 8
			fetch cu_prodotti_8 into :ls_cod_prodotto_c,
										  :ls_des_prodotto_c,
										  :ls_cod_misura_c,
										  :ls_cod_comodo_c,
										  :ld_prezzo_vendita;										  
	end choose	
	if sqlca.sqlcode <> 0 then exit
	
	ll_riga = dw_1.insertrow(0)
	dw_1.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto_c)
	dw_1.setitem(ll_riga, "des_prodotto", ls_des_prodotto_c)
	dw_1.setitem(ll_riga, "cod_comodo", ls_cod_comodo_c)
	dw_1.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
	
loop

close cu_prodotti_1;
close cu_prodotti_2;
close cu_prodotti_3;
close cu_prodotti_4;
close cu_prodotti_5;
close cu_prodotti_6;
close cu_prodotti_7;
close cu_prodotti_8;
rollback;

dw_1.setredraw(true)
dw_1.object.datawindow.print.preview = 'Yes'

return
//long ll_riga
//
//dw_1.setfocus()
//dw_1.setredraw(false)
//ll_riga = dw_1.insertrow(0)
//dw_1.setitem(ll_riga, "cod_prodotto", em_cod_prodotto.text)
//dw_1.setitem(ll_riga, "des_prodotto", em_des_prodotto.text)
//dw_1.setitem(ll_riga, "cod_misura",   "U.M.: " + em_unita_misura.text)
//dw_1.setitem(ll_riga, "cod_comodo", em_cod_comodo.text)
//dw_1.setitem(ll_riga, "prezzo_vendita", dec(em_prezzo_vendita.text))
//dw_1.setredraw(true)
//dw_1.object.datawindow.print.preview = 'Yes'
//
end event

