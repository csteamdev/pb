﻿$PBExportHeader$w_report_etichette_barcode_new.srw
forward
global type w_report_etichette_barcode_new from window
end type
type cb_3 from commandbutton within w_report_etichette_barcode_new
end type
type em_prezzo_vendita from singlelineedit within w_report_etichette_barcode_new
end type
type st_14 from statictext within w_report_etichette_barcode_new
end type
type em_cod_comodo from singlelineedit within w_report_etichette_barcode_new
end type
type st_cod_comodo from statictext within w_report_etichette_barcode_new
end type
type em_unita_misura from singlelineedit within w_report_etichette_barcode_new
end type
type em_des_prodotto from singlelineedit within w_report_etichette_barcode_new
end type
type em_cod_prodotto from singlelineedit within w_report_etichette_barcode_new
end type
type ddlb_2 from dropdownlistbox within w_report_etichette_barcode_new
end type
type st_100 from statictext within w_report_etichette_barcode_new
end type
type st_9 from statictext within w_report_etichette_barcode_new
end type
type ddlb_1 from dropdownlistbox within w_report_etichette_barcode_new
end type
type cb_5 from commandbutton within w_report_etichette_barcode_new
end type
type cb_4 from commandbutton within w_report_etichette_barcode_new
end type
type st_13 from statictext within w_report_etichette_barcode_new
end type
type st_12 from statictext within w_report_etichette_barcode_new
end type
type st_11 from statictext within w_report_etichette_barcode_new
end type
type st_10 from statictext within w_report_etichette_barcode_new
end type
type cb_carica_dati from commandbutton within w_report_etichette_barcode_new
end type
type cb_2 from commandbutton within w_report_etichette_barcode_new
end type
type cb_1 from commandbutton within w_report_etichette_barcode_new
end type
type st_3 from statictext within w_report_etichette_barcode_new
end type
type em_2 from editmask within w_report_etichette_barcode_new
end type
type em_1 from editmask within w_report_etichette_barcode_new
end type
type st_51 from statictext within w_report_etichette_barcode_new
end type
type em_51 from editmask within w_report_etichette_barcode_new
end type
type em_50 from editmask within w_report_etichette_barcode_new
end type
type st_53 from statictext within w_report_etichette_barcode_new
end type
type em_53 from editmask within w_report_etichette_barcode_new
end type
type em_52 from editmask within w_report_etichette_barcode_new
end type
type st_52 from statictext within w_report_etichette_barcode_new
end type
type em_3 from editmask within w_report_etichette_barcode_new
end type
type em_4 from editmask within w_report_etichette_barcode_new
end type
type st_4 from statictext within w_report_etichette_barcode_new
end type
type st_7 from statictext within w_report_etichette_barcode_new
end type
type st_8 from statictext within w_report_etichette_barcode_new
end type
type em_8 from editmask within w_report_etichette_barcode_new
end type
type em_7 from editmask within w_report_etichette_barcode_new
end type
type em_5 from editmask within w_report_etichette_barcode_new
end type
type em_6 from editmask within w_report_etichette_barcode_new
end type
type st_6 from statictext within w_report_etichette_barcode_new
end type
type st_5 from statictext within w_report_etichette_barcode_new
end type
type st_1 from statictext within w_report_etichette_barcode_new
end type
type st_2 from statictext within w_report_etichette_barcode_new
end type
type st_50 from statictext within w_report_etichette_barcode_new
end type
type dw_1 from datawindow within w_report_etichette_barcode_new
end type
type tab_1 from tab within w_report_etichette_barcode_new
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tab_1 from tab within w_report_etichette_barcode_new
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
end forward

global type w_report_etichette_barcode_new from window
integer width = 3867
integer height = 2552
boolean titlebar = true
string title = "Stampa Etichette"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cb_3 cb_3
em_prezzo_vendita em_prezzo_vendita
st_14 st_14
em_cod_comodo em_cod_comodo
st_cod_comodo st_cod_comodo
em_unita_misura em_unita_misura
em_des_prodotto em_des_prodotto
em_cod_prodotto em_cod_prodotto
ddlb_2 ddlb_2
st_100 st_100
st_9 st_9
ddlb_1 ddlb_1
cb_5 cb_5
cb_4 cb_4
st_13 st_13
st_12 st_12
st_11 st_11
st_10 st_10
cb_carica_dati cb_carica_dati
cb_2 cb_2
cb_1 cb_1
st_3 st_3
em_2 em_2
em_1 em_1
st_51 st_51
em_51 em_51
em_50 em_50
st_53 st_53
em_53 em_53
em_52 em_52
st_52 st_52
em_3 em_3
em_4 em_4
st_4 st_4
st_7 st_7
st_8 st_8
em_8 em_8
em_7 em_7
em_5 em_5
em_6 em_6
st_6 st_6
st_5 st_5
st_1 st_1
st_2 st_2
st_50 st_50
dw_1 dw_1
tab_1 tab_1
end type
global w_report_etichette_barcode_new w_report_etichette_barcode_new

type variables
boolean ib_etichette_vuote=true
end variables

forward prototypes
public function integer wf_scrivi_etichetta (string fs_nome, string fs_valore)
public function boolean wf_salvaformato (ref datawindow dwo)
public function boolean wf_caricaformato (ref datawindow dwo)
end prototypes

public function integer wf_scrivi_etichetta (string fs_nome, string fs_valore);integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vql", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro VQL (registry) indicante volume condiviso documenti.")
	return -1
end if

li_file = fileopen(ls_volume + "\etichette.txt", linemode!, Write!, LockWrite!, Append!)
if li_file = -1 then
	g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
	return -1
end if
ls_messaggio = fs_nome + "=" + fs_valore + "~r"
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0
end function

public function boolean wf_salvaformato (ref datawindow dwo);//***********************************************************
// function: gridSaveFormat returns boolean
// args:
// Datawindow   dwo -- datawindow object whose format we wish to save  passed by reference
// return value: true iff successful, false iff not successful.
//***********************************************************

long column_count       //* number of columns
long i                  //* loop index
string build_str        //* result string
string colname          //* name of current column being processed
string inifile = 'c:\etichetta.ini'  //you can make this an instance variable...

//check if ini file exists
if NOT FileExists ( inifile ) then
	g_mb.messagebox('', 'Can not find ini file: '+inifile)
	Return False
end if

column_count = long(dwo.describe("datawindow.column.count"))
build_str = ''

for i = 1 to column_count
  colname = dwo.describe('#'+string(i)+'.name')
  build_str = build_str + ' C'+string(i)+':'
  build_str = build_str + dwo.describe (colname + ".x") + ':'
  build_str = build_str + dwo.describe (colname + ".width") + ':'
  build_str = build_str + dwo.describe (colname + ".visible") + ':'
next

// c:\my.ini
// [dataobject name]
// colformat = C1:10:100:1: C2:110:100:1: C3:230:100:0:
if SetProfileSTring (inifile, dwo.dataobject, "colformat", build_str) <> 1 then
   return false
end if

return true
end function

public function boolean wf_caricaformato (ref datawindow dwo);long i, j              //* loop index
long pos1, pos2
integer column_count   //* count of columns in dwo
string  colname        //* name of column being formatted
string  value, value2  //* values used to format column
string  result_str     //* used to test success of modify() call
string  loadstr        //* string loaded from .ini file
string  colstring
string  scol[], sxcoord[], swidth[], svisible[]
long    sorder[] //for grid dw you have to do it in order
long    lorder
string  inifile = 'c:\etichetta.ini'  //replace with global variable...
long fnd[]
long shighest, snext

// example of ini file:
// [dataobject name]
// colformat = C1:10:100:1: C2:110:100:1: C3:230:100:0:
loadstr = profileString (inifile, dwo.dataobject,"colformat", '')

if (len(loadstr) = 0) or isNull(loadstr) then
      return false
end if

//get number of columns
column_count = integer(dwo.describe("datawindow.column.count"))

for i = 1 to column_count
  // colNum:xcoordinate:width:visible:
  // colformat = C1:10:100:1: C2:110:100:1: C3:230:100:0:
   pos1 = Pos(loadstr, 'C'+string(i) +':')
   if pos1 = 0 THEN 
		continue	
	end if

	//find the next column ex: C1: starting position
	if i = column_count then
		pos2 = len(loadstr) + 1
	else
		pos2 = Pos(loadstr, 'C'+string(i+1) +':')
	end if
	
		//get the current columns value
	colstring = mid(loadstr, pos1, pos2 - pos1)
	
		//get the column number
	pos1 = pos(colstring, ':', 1)
	scol[i] = mid(colstring, 1, pos1 - 1)
	
	//get the x coordinate
	pos1 = pos1 + 1
	pos2 = pos(colstring, ':', pos1)
	sxcoord[i] = mid(colstring, pos1, pos2 - pos1)
	
	//get the width
	pos1 = pos2 + 1
	pos2 = pos(colstring, ':', pos1)
	swidth[i] = mid(colstring, pos1, pos2 - pos1)
	
	//get the visible attribute
	pos1 = pos2 + 1
	pos2 = pos(colstring, ':', pos1)
	svisible[i] = mid(colstring, pos1, pos2 - pos1)

next

for i = 1 to column_count
	fnd[i] = 0
	sorder[i] = 0
next

for i = 1 to column_count
	shighest = 0
   for j = 1 to column_count
		if fnd[j] = 1 then
			continue
		end if
	   snext = long(sxcoord[ j ])
		if shighest < snext then
	      shighest = snext
			lorder = j
		end if
	next
	sorder[i] = lorder
	fnd[lorder] = 1
next

for i = 1 to column_count
	if fnd[i] = 0 then
		sorder[column_count] = i
	end if
next

for i = 1 to column_count
	j = sorder[i]
	colname = dwo.describe('#'+string(j)+'.name')
	dwo.modify (colname + ".visible=" + svisible[j])
next

for i = 1 to column_count
	for j = 1 to 2
		lorder = sorder[i]
		colname = dwo.describe('#'+string(lorder)+'.name')
		dwo.modify (colname + ".width=" + swidth[lorder])
		dwo.modify (colname + ".x=" + sxcoord[lorder]) 
	next
next

return true


end function

on w_report_etichette_barcode_new.create
this.cb_3=create cb_3
this.em_prezzo_vendita=create em_prezzo_vendita
this.st_14=create st_14
this.em_cod_comodo=create em_cod_comodo
this.st_cod_comodo=create st_cod_comodo
this.em_unita_misura=create em_unita_misura
this.em_des_prodotto=create em_des_prodotto
this.em_cod_prodotto=create em_cod_prodotto
this.ddlb_2=create ddlb_2
this.st_100=create st_100
this.st_9=create st_9
this.ddlb_1=create ddlb_1
this.cb_5=create cb_5
this.cb_4=create cb_4
this.st_13=create st_13
this.st_12=create st_12
this.st_11=create st_11
this.st_10=create st_10
this.cb_carica_dati=create cb_carica_dati
this.cb_2=create cb_2
this.cb_1=create cb_1
this.st_3=create st_3
this.em_2=create em_2
this.em_1=create em_1
this.st_51=create st_51
this.em_51=create em_51
this.em_50=create em_50
this.st_53=create st_53
this.em_53=create em_53
this.em_52=create em_52
this.st_52=create st_52
this.em_3=create em_3
this.em_4=create em_4
this.st_4=create st_4
this.st_7=create st_7
this.st_8=create st_8
this.em_8=create em_8
this.em_7=create em_7
this.em_5=create em_5
this.em_6=create em_6
this.st_6=create st_6
this.st_5=create st_5
this.st_1=create st_1
this.st_2=create st_2
this.st_50=create st_50
this.dw_1=create dw_1
this.tab_1=create tab_1
this.Control[]={this.cb_3,&
this.em_prezzo_vendita,&
this.st_14,&
this.em_cod_comodo,&
this.st_cod_comodo,&
this.em_unita_misura,&
this.em_des_prodotto,&
this.em_cod_prodotto,&
this.ddlb_2,&
this.st_100,&
this.st_9,&
this.ddlb_1,&
this.cb_5,&
this.cb_4,&
this.st_13,&
this.st_12,&
this.st_11,&
this.st_10,&
this.cb_carica_dati,&
this.cb_2,&
this.cb_1,&
this.st_3,&
this.em_2,&
this.em_1,&
this.st_51,&
this.em_51,&
this.em_50,&
this.st_53,&
this.em_53,&
this.em_52,&
this.st_52,&
this.em_3,&
this.em_4,&
this.st_4,&
this.st_7,&
this.st_8,&
this.em_8,&
this.em_7,&
this.em_5,&
this.em_6,&
this.st_6,&
this.st_5,&
this.st_1,&
this.st_2,&
this.st_50,&
this.dw_1,&
this.tab_1}
end on

on w_report_etichette_barcode_new.destroy
destroy(this.cb_3)
destroy(this.em_prezzo_vendita)
destroy(this.st_14)
destroy(this.em_cod_comodo)
destroy(this.st_cod_comodo)
destroy(this.em_unita_misura)
destroy(this.em_des_prodotto)
destroy(this.em_cod_prodotto)
destroy(this.ddlb_2)
destroy(this.st_100)
destroy(this.st_9)
destroy(this.ddlb_1)
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.cb_carica_dati)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.st_3)
destroy(this.em_2)
destroy(this.em_1)
destroy(this.st_51)
destroy(this.em_51)
destroy(this.em_50)
destroy(this.st_53)
destroy(this.em_53)
destroy(this.em_52)
destroy(this.st_52)
destroy(this.em_3)
destroy(this.em_4)
destroy(this.st_4)
destroy(this.st_7)
destroy(this.st_8)
destroy(this.em_8)
destroy(this.em_7)
destroy(this.em_5)
destroy(this.em_6)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_50)
destroy(this.dw_1)
destroy(this.tab_1)
end on

event open;if isvalid(s_cs_xx.parametri.parametro_dw_1) then
	if s_cs_xx.parametri.parametro_dw_1.dataobject <> "d_ext_carico_acq_det" then
		cb_carica_dati.enabled = false
	end if
else
	cb_carica_dati.enabled = false
end if

em_1.text = string(dw_1.object.datawindow.label.columns)
em_2.text = string(dw_1.object.datawindow.label.rows)
em_4.text = string(long(dw_1.object.datawindow.label.rows.spacing) / 1000)
em_3.text = string(long(dw_1.object.datawindow.label.columns.spacing) / 1000)
em_6.text = string(long(dw_1.object.datawindow.label.height) / 1000)
em_5.text = string(long(dw_1.object.datawindow.label.width) / 1000)
em_50.text = string(long(dw_1.object.datawindow.print.Margin.Top) / 1000)
em_51.text = string(long(dw_1.object.datawindow.print.Margin.Bottom) / 1000)
em_52.text = string(long(dw_1.object.datawindow.print.Margin.Right) / 1000)
em_53.text = string(long(dw_1.object.datawindow.print.Margin.Left) / 1000)

st_1.visible = true
em_1.visible = true
st_2.visible = true
em_2.visible = true
st_3.visible = true
em_3.visible = true
st_4.visible = true
em_4.visible = true
st_5.visible = true
em_5.visible = true
st_6.visible = true
em_6.visible = true
st_7.visible = true
em_7.visible = true
st_8.visible = true
em_8.visible = true
st_50.visible = true
em_50.visible = true
st_51.visible = true
em_51.visible = true
st_52.visible = true
em_52.visible = true	
st_53.visible = true
em_53.visible = true	

em_cod_prodotto.visible = false
em_des_prodotto.visible = false
em_unita_misura.visible = false
em_cod_comodo.visible = false
em_prezzo_vendita.visible = false
st_10.visible = false
st_11.visible = false
st_12.visible = false
st_14.visible = false
st_cod_comodo.visible = false
//cb_ricerca_prodotto.visible = false



end event

type cb_3 from commandbutton within w_report_etichette_barcode_new
integer x = 800
integer y = 760
integer width = 754
integer height = 80
integer taborder = 230
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichetta Piccola"
end type

event clicked;long ll_riga

dw_1.reset()
dw_1.dataobject = "d_report_etichette_barcode_ext_pic"
dw_1.settransobject( sqlca)
dw_1.setfocus()
dw_1.setredraw(false)
ll_riga = dw_1.insertrow(0)
dw_1.setitem(ll_riga, "cod_prodotto", em_cod_prodotto.text)
dw_1.setitem(ll_riga, "des_prodotto", em_des_prodotto.text)
dw_1.setitem(ll_riga, "cod_comodo", em_cod_comodo.text)
dw_1.setitem(ll_riga, "prezzo_vendita", dec(em_prezzo_vendita.text))
dw_1.setredraw(true)
dw_1.object.datawindow.print.preview = 'Yes'
end event

type em_prezzo_vendita from singlelineedit within w_report_etichette_barcode_new
integer x = 1829
integer y = 480
integer width = 320
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_14 from statictext within w_report_etichette_barcode_new
integer x = 1303
integer y = 480
integer width = 512
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Prezzo Vendita:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_cod_comodo from singlelineedit within w_report_etichette_barcode_new
integer x = 663
integer y = 380
integer width = 1486
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event modified;string ls_cod_prodotto, ls_cod_comodo, ls_des_prodotto, ls_cod_barre, ls_cod_misura_mag

dec{4} ld_prezzo_vendita

ls_cod_prodotto = this.text

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" or len(trim(ls_cod_prodotto)) = 0 then
	em_des_prodotto.text = ""
	em_unita_misura.text = ""
	return 0
end if

select des_prodotto,
       cod_comodo,
		 prezzo_vendita,
		 cod_barre,
		 cod_misura_mag
into   :ls_des_prodotto,
       :ls_cod_comodo,
		 :ld_prezzo_vendita,
		 :ls_cod_barre,
		 :ls_cod_misura_mag
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	em_des_prodotto.text = ""
	em_unita_misura.text = ""
	return 0
else
	em_des_prodotto.text = ls_des_prodotto
	em_unita_misura.text = ls_cod_misura_mag
	
end if
	
		 

end event

type st_cod_comodo from statictext within w_report_etichette_barcode_new
integer x = 114
integer y = 380
integer width = 512
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Codice Comodo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_unita_misura from singlelineedit within w_report_etichette_barcode_new
integer x = 663
integer y = 480
integer width = 320
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type em_des_prodotto from singlelineedit within w_report_etichette_barcode_new
integer x = 663
integer y = 280
integer width = 1486
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type em_cod_prodotto from singlelineedit within w_report_etichette_barcode_new
integer x = 663
integer y = 180
integer width = 731
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event modified;string ls_cod_prodotto, ls_cod_comodo, ls_des_prodotto, ls_cod_barre, ls_cod_misura_mag

dec{4} ld_prezzo_vendita

ls_cod_prodotto = this.text

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" or len(trim(ls_cod_prodotto)) = 0 then
	em_des_prodotto.text = ""
	em_unita_misura.text = ""
	return 0
end if

select des_prodotto,
       cod_comodo,
		 prezzo_vendita,
		 cod_barre,
		 cod_misura_mag
into   :ls_des_prodotto,
       :ls_cod_comodo,
		 :ld_prezzo_vendita,
		 :ls_cod_barre,
		 :ls_cod_misura_mag
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	em_des_prodotto.text = ""
	em_unita_misura.text = ""
	em_cod_comodo.text = ""
	em_prezzo_vendita.text = ""
	return 0
else
	em_des_prodotto.text = ls_des_prodotto
	em_unita_misura.text = ls_cod_misura_mag
	em_cod_comodo.text = ls_cod_comodo
	em_prezzo_vendita.text = string(ld_prezzo_vendita)
	
end if
	
		 

end event

type ddlb_2 from dropdownlistbox within w_report_etichette_barcode_new
integer x = 2583
integer y = 660
integer width = 274
integer height = 300
integer taborder = 180
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean sorted = false
boolean vscrollbar = true
string item[] = {"6","8","10","12","14","16","18","20","22","24","26","28"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;long ll_font

ll_font = long(ddlb_1.text(index))
ll_font = ll_font * -1
dw_1.OBJECT.des_prodotto.font.height = ll_font
dw_1.OBJECT.cod_prodotto.font.height = ll_font
dw_1.OBJECT.cod_comodo.font.height = ll_font
dw_1.OBJECT.prezzo_vendita.font.height = ll_font
//dw_1.OBJECT.cod_misura.font.height = ll_font

end event

type st_100 from statictext within w_report_etichette_barcode_new
integer x = 2103
integer y = 680
integer width = 480
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Dimensione Testo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_9 from statictext within w_report_etichette_barcode_new
integer x = 1234
integer y = 680
integer width = 567
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Dimensione Barcode:"
boolean focusrectangle = false
end type

type ddlb_1 from dropdownlistbox within w_report_etichette_barcode_new
integer x = 1806
integer y = 660
integer width = 274
integer height = 300
integer taborder = 170
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean sorted = false
boolean vscrollbar = true
string item[] = {"6","8","10","12","14","16","18","20","22","24","26","28"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;long ll_font

ll_font = long(ddlb_1.text(index))
ll_font = ll_font * -1
dw_1.OBJECT.compute_1.font.height = ll_font
end event

type cb_5 from commandbutton within w_report_etichette_barcode_new
integer x = 23
integer y = 760
integer width = 754
integer height = 80
integer taborder = 220
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichetta Grande"
end type

event clicked;long ll_riga

dw_1.reset()
dw_1.dataobject = "d_report_etichette_barcode_ext_grande"
dw_1.settransobject( sqlca)
dw_1.setfocus()
dw_1.setredraw(false)
ll_riga = dw_1.insertrow(0)
dw_1.setitem(ll_riga, "cod_prodotto", em_cod_prodotto.text)
dw_1.setitem(ll_riga, "des_prodotto", em_des_prodotto.text)
dw_1.setitem(ll_riga, "cod_comodo", em_cod_comodo.text)
dw_1.setitem(ll_riga, "prezzo_vendita", dec(em_prezzo_vendita.text))
dw_1.setredraw(true)
dw_1.object.datawindow.print.preview = 'Yes'

end event

type cb_4 from commandbutton within w_report_etichette_barcode_new
integer x = 3406
integer y = 660
integer width = 389
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
end type

event clicked;long ll_riga

dw_1.setfocus()
dw_1.setredraw(false)
ll_riga = dw_1.insertrow(0)
dw_1.setitem(ll_riga, "cod_prodotto", em_cod_prodotto.text)
dw_1.setitem(ll_riga, "des_prodotto", em_des_prodotto.text)
dw_1.setitem(ll_riga, "cod_misura",   "U.M.: " + em_unita_misura.text)
dw_1.setitem(ll_riga, "cod_comodo", em_cod_comodo.text)
dw_1.setitem(ll_riga, "prezzo_vendita", dec(em_prezzo_vendita.text))
dw_1.setredraw(true)
dw_1.object.datawindow.print.preview = 'Yes'

end event

type st_13 from statictext within w_report_etichette_barcode_new
integer x = 2903
integer y = 680
integer width = 503
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Aggiungi Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_12 from statictext within w_report_etichette_barcode_new
integer x = 91
integer y = 480
integer width = 512
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Unità di Misura:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_11 from statictext within w_report_etichette_barcode_new
integer x = 46
integer y = 280
integer width = 571
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Descrizione Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_10 from statictext within w_report_etichette_barcode_new
integer x = 114
integer y = 180
integer width = 512
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 15780518
string text = "Codice Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_carica_dati from commandbutton within w_report_etichette_barcode_new
integer x = 23
integer y = 660
integer width = 366
integer height = 80
integer taborder = 190
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Dati"
end type

event clicked;string  ls_centro_costo, ls_codice, ls_cod_prodotto, ls_cod_comodo

dec{4}  ld_num, ld_quan_acquisto, ld_prezzo_vendita

long 	  ll_i, ll_ret, ll_righe, ll_y, ll_num_etichette, ll_riga, ll_anno_reg, ll_num_reg, ll_prog_riga, &
		  ll_anno_commessa, ll_num_commessa


datawindow ldw_etichette


if s_cs_xx.parametri.parametro_dw_1.dataobject = "d_ext_carico_acq_det" then
	
	ll_ret = g_mb.messagebox("APICE","Carico i dati delle etichette dai prodotti in entrata?",Question!,YesNo!,2)
	
	if ll_ret = 1 then
		
		ldw_etichette = s_cs_xx.parametri.parametro_dw_1
		
		ll_righe = ldw_etichette.rowcount()
		
		//dw_1.dataobject = 'd_report_etichette_barcode_ext'
		
		for ll_i = 1 to ll_righe
			
			if isnull(ldw_etichette.getitemstring(ll_i,"cod_prodotto")) or len(ldw_etichette.getitemstring(ll_i,"cod_prodotto")) < 1 then
				
				ll_anno_reg = ldw_etichette.getitemnumber(ll_i,"anno_registrazione")
				
				ll_num_reg = ldw_etichette.getitemnumber(ll_i,"num_registrazione")
				
				ll_prog_riga = ldw_etichette.getitemnumber(ll_i,"prog_riga_ord_acq")
				
				select cod_centro_costo,
						 anno_commessa,
						 num_commessa
				into   :ls_centro_costo,
						 :ll_anno_commessa,
						 :ll_num_commessa
				from   det_ord_acq
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_reg and
						 num_registrazione = :ll_num_reg and
						 prog_riga_ordine_acq = :ll_prog_riga;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in lettura dati riga ordine da det_ord_acq: " + sqlca.sqlerrtext)
					return -1
				end if
				
				if not isnull(ls_centro_costo) then
					ls_codice = "60" + ls_centro_costo
				elseif not isnull(ll_anno_commessa) and not isnull(ll_num_commessa) then
					ls_codice = right(string(ll_num_commessa),3)
					choose case len(ls_codice)
						case 1
							ls_codice = "00" + ls_codice
						case 2
							ls_codice = "0" + ls_codice
					end choose
					ls_codice = "50" + right(string(ll_anno_commessa),2) + ls_codice
				else
					continue
				end if
				
				ll_riga = dw_1.insertrow(0)
				
				dw_1.setitem(ll_riga, "cod_prodotto", ls_codice)
				
			else
				
				ld_quan_acquisto = ldw_etichette.getitemnumber(ll_i,"quan_acquisto")
				
				ll_num_etichette = ceiling(ld_quan_acquisto)
				ls_cod_prodotto = ldw_etichette.getitemstring(ll_i,"cod_prodotto")
				select cod_comodo,
						 prezzo_vendita						
				into   :ls_cod_comodo,
						 :ld_prezzo_vendita
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
		 
				
				for ll_y = 1 to ll_num_etichette
					ll_riga = dw_1.insertrow(0)
					dw_1.setitem(ll_riga, "cod_prodotto", ldw_etichette.getitemstring(ll_i,"cod_prodotto"))
					dw_1.setitem(ll_riga, "des_prodotto", ldw_etichette.getitemstring(ll_i,"des_prodotto"))
					dw_1.setitem(ll_riga, "cod_comodo", ls_cod_comodo)
					dw_1.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
				next
				
			end if
			
		next
		
	end if
	
end if

em_1.text = string(long(dw_1.object.datawindow.label.columns),"##0")
em_2.text = string(long(dw_1.object.datawindow.label.rows),"##0")
em_6.text = string(dec(dw_1.object.datawindow.label.height) / 1000,"###,##0.000")
em_4.text = string(dec(dw_1.object.datawindow.label.rows.spacing) / 1000,"###,##0.000")
em_3.text = string(dec(dw_1.object.datawindow.label.columns.spacing) / 1000,"###,##0.000")
em_5.text = string(dec(dw_1.object.datawindow.label.width) / 1000,"###,##0.000")

dw_1.Object.DataWindow.Print.preview = 'Yes'
dw_1.Object.DataWindow.Print.preview.rulers = 'Yes'
end event

type cb_2 from commandbutton within w_report_etichette_barcode_new
integer x = 800
integer y = 660
integer width = 366
integer height = 80
integer taborder = 210
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampante"
end type

event clicked;printsetup()
end event

type cb_1 from commandbutton within w_report_etichette_barcode_new
integer x = 411
integer y = 660
integer width = 366
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long ll_i 


em_7.enabled = false

// inserisco n. etichette vuote all'inizio
if (long(em_7.text) > 0 or not isnull(long(em_7.text))) and ib_etichette_vuote then
	for ll_i = 1 to long(em_7.text)
		dw_1.insertrow(1)
	next
end if

// imposto num.copie
dw_1.Object.DataWindow.Print.Copies = long(em_8.text)

// stampo etichette
dw_1.print()
ib_etichette_vuote = false
end event

type st_3 from statictext within w_report_etichette_barcode_new
integer x = 1440
integer y = 280
integer width = 512
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Spazio tra Colonne:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_2 from editmask within w_report_etichette_barcode_new
integer x = 777
integer y = 280
integer width = 251
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
end type

event modified;dw_1.object.datawindow.label.rows = long(em_2.text)

end event

type em_1 from editmask within w_report_etichette_barcode_new
integer x = 777
integer y = 180
integer width = 251
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
end type

event modified;dw_1.object.datawindow.label.columns = long(em_1.text)

end event

type st_51 from statictext within w_report_etichette_barcode_new
integer x = 91
integer y = 480
integer width = 672
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Inferiore Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_51 from editmask within w_report_etichette_barcode_new
integer x = 777
integer y = 480
integer width = 329
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Bottom = long(dec(em_51.text) * 1000)

end event

type em_50 from editmask within w_report_etichette_barcode_new
integer x = 777
integer y = 380
integer width = 329
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Top = long(dec(em_50.text) * 1000)

end event

type st_53 from statictext within w_report_etichette_barcode_new
integer x = 1280
integer y = 480
integer width = 672
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Sinistro Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_53 from editmask within w_report_etichette_barcode_new
integer x = 1966
integer y = 480
integer width = 329
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Left = long(dec(em_53.text) * 1000)
end event

type em_52 from editmask within w_report_etichette_barcode_new
integer x = 1966
integer y = 380
integer width = 329
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,##0.000"
boolean spin = true
string minmax = "0~~"
end type

event modified;dw_1.object.datawindow.print.Margin.Right = long(dec(em_52.text) * 1000)
end event

type st_52 from statictext within w_report_etichette_barcode_new
integer x = 1253
integer y = 380
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Destro Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_3 from editmask within w_report_etichette_barcode_new
integer x = 1966
integer y = 280
integer width = 251
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.columns.spacing = long(dec(em_3.text) * 1000)

end event

type em_4 from editmask within w_report_etichette_barcode_new
integer x = 1966
integer y = 180
integer width = 251
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.rows.spacing = long(dec(em_4.text) * 1000)

end event

type st_4 from statictext within w_report_etichette_barcode_new
integer x = 1426
integer y = 180
integer width = 526
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Spazio tra Righe:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_7 from statictext within w_report_etichette_barcode_new
integer x = 2309
integer y = 520
integer width = 837
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num.Etichette Vuote"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_8 from statictext within w_report_etichette_barcode_new
integer x = 2743
integer y = 420
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num.Pagine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_8 from editmask within w_report_etichette_barcode_new
integer x = 3200
integer y = 420
integer width = 251
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
string minmax = "1~~999"
end type

type em_7 from editmask within w_report_etichette_barcode_new
integer x = 3200
integer y = 520
integer width = 251
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
string minmax = "0~~999"
end type

type em_5 from editmask within w_report_etichette_barcode_new
integer x = 3200
integer y = 280
integer width = 251
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.width = long(dec(em_5.text) * 1000)

end event

type em_6 from editmask within w_report_etichette_barcode_new
integer x = 3200
integer y = 180
integer width = 251
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.000"
end type

event modified;dw_1.object.datawindow.label.height = long(dec(em_6.text) * 1000)

end event

type st_6 from statictext within w_report_etichette_barcode_new
integer x = 2711
integer y = 180
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Altezza Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_report_etichette_barcode_new
integer x = 2629
integer y = 280
integer width = 539
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Larghezza Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_etichette_barcode_new
integer x = 160
integer y = 180
integer width = 590
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num. Etichette in Riga:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_report_etichette_barcode_new
integer x = 64
integer y = 280
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Num. Etichette in Colonna:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_50 from statictext within w_report_etichette_barcode_new
integer x = 64
integer y = 380
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12639424
string text = "Margine Superiore Foglio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_report_etichette_barcode_new
integer x = 23
integer y = 860
integer width = 3726
integer height = 1560
integer taborder = 230
string title = "none"
string dataobject = "d_report_etichette_barcode_ext_new"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type tab_1 from tab within w_report_etichette_barcode_new
integer y = 20
integer width = 3794
integer height = 620
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

event clicked;if index = 1 then
	st_1.visible = true
	em_1.visible = true
	st_2.visible = true
	em_2.visible = true
	st_3.visible = true
	em_3.visible = true
	st_4.visible = true
	em_4.visible = true
	st_5.visible = true
	em_5.visible = true
	st_6.visible = true
	em_6.visible = true
	st_7.visible = true
	em_7.visible = true
	st_8.visible = true
	em_8.visible = true
	st_50.visible = true
	em_50.visible = true
	st_51.visible = true
	em_51.visible = true
	st_52.visible = true
	em_52.visible = true	
	st_53.visible = true
	em_53.visible = true	
	
	em_cod_prodotto.visible = false
	em_des_prodotto.visible = false
	em_unita_misura.visible = false
	em_cod_comodo.visible = false
	em_prezzo_vendita.visible = false
	st_10.visible = false
	st_11.visible = false
	st_12.visible = false
	st_14.visible = false
	st_cod_comodo.visible = false
	//cb_ricerca_prodotto.visible = false
else
	st_1.visible = false
	em_1.visible = false
	st_2.visible = false
	em_2.visible = false
	st_3.visible = false
	em_3.visible = false
	st_4.visible = false
	em_4.visible = false
	st_5.visible = false
	em_5.visible = false
	st_6.visible = false
	em_6.visible = false
	st_7.visible = false
	em_7.visible = false
	st_8.visible = false
	em_8.visible = false
	st_50.visible = false
	em_50.visible = false
	st_51.visible = false
	em_51.visible = false
	st_52.visible = false
	em_52.visible = false	
	st_53.visible = false
	em_53.visible = false		
	
	em_cod_prodotto.visible = true
	em_des_prodotto.visible = true
	em_unita_misura.visible = true
	em_cod_comodo.visible = true
	em_prezzo_vendita.visible = true
	st_10.visible = true
	st_11.visible = true
	st_12.visible = true
	st_14.visible = true
	st_cod_comodo.visible = true
	//cb_ricerca_prodotto.visible = true
end if
end event

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3758
integer height = 496
long backcolor = 12639424
string text = "Stile"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3758
integer height = 496
long backcolor = 15780518
string text = "Dati"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

