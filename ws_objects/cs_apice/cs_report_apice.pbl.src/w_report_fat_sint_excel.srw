﻿$PBExportHeader$w_report_fat_sint_excel.srw
forward
global type w_report_fat_sint_excel from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_fat_sint_excel
end type
type cb_report from commandbutton within w_report_fat_sint_excel
end type
type dw_selezione from uo_cs_xx_dw within w_report_fat_sint_excel
end type
type dw_report from uo_cs_xx_dw within w_report_fat_sint_excel
end type
end forward

global type w_report_fat_sint_excel from w_cs_xx_principale
integer width = 2258
integer height = 744
string title = "Report Sintetico Fatture"
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_fat_sint_excel w_report_fat_sint_excel

on w_report_fat_sint_excel.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
end on

on w_report_fat_sint_excel.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
//cb_ricerca_cliente.Show ( )									 
end event

type cb_annulla from commandbutton within w_report_fat_sint_excel
integer x = 1445
integer y = 544
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_fat_sint_excel
integer x = 1833
integer y = 544
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esporta"
end type

event clicked;string ls_cod_documento_inizio, ls_cod_documento_fine, ls_cod_cliente, ls_cod_tipo_fat_ven, ls_cod_agente_1, &
       ls_flag_contabilita
long   ll_fattura_inizio, ll_fattura_fine, ll_anno_documento, ll_i
date   ldd_data_inizio, ldd_data_fine
string ls_cod_tipo_fat_ven_estratto, ls_cod_cliente_estratto, ls_cod_documento_estratto, ls_numeratore_documento_estratto, &
		 ls_flag_movimenti_estratto, ls_flag_contabilita_estratto, ls_cod_agente_1_estratto, ls_cod_agente_2_estratto, ls_cod_vettore_estratto, ls_cod_inoltro_estratto		  
long	 ll_anno_documento_estratto, ll_num_documento_estratto, ll_importo_iva_estratto, ll_imponibile_iva_estratto, ll_tot_fattura_estratto
datetime	 ldt_data_fattura_estratto
string ls_sql, ls_data_da, ls_data_a
string ls_des_cliente, ls_cod_zona, ls_provincia, ls_cod_nazione, ls_des_zona, ls_des_nazione, &
		 ls_des_agente_1, ls_des_agente_2, ls_des_vettore
string docname, named, ls_stringa_file
integer value, li_filenum		 

ldd_data_inizio = dw_selezione.getitemdate(1,"da_data_fattura")
ldd_data_fine = dw_selezione.getitemdate(1,"a_data_fattura")
ll_fattura_inizio = dw_selezione.getitemnumber(1,"da_num_documento")
ll_fattura_fine = dw_selezione.getitemnumber(1,"a_num_documento")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_flag_contabilita = dw_selezione.getitemstring(1,"flag_contabilita")

if ls_flag_contabilita = "T" then ls_flag_contabilita = "%"

declare cu_tes_fat_ven dynamic cursor for sqlsa;

ls_sql = " select cod_tipo_fat_ven, cod_cliente, cod_documento, numeratore_documento, anno_documento, num_documento, data_fattura, flag_movimenti, flag_contabilita, importo_iva, imponibile_iva, tot_fattura, cod_agente_1, cod_agente_2, cod_vettore, cod_inoltro from tes_fat_ven where (cod_azienda = '" + s_cs_xx.cod_azienda + "') "

if not isnull(ls_cod_cliente) then
	ls_sql = ls_sql + "and cod_cliente like '" + ls_cod_cliente + "' "
end if	

if not isnull(ldd_data_inizio) then
	ls_data_da = string(ldd_data_inizio, "yyyy/mm/dd")
	ls_sql = ls_sql + "and data_fattura >= '" + ls_data_da + "' "
end if	
	
if not isnull(ldd_data_fine) then
	ls_data_a = string(ldd_data_fine, "yyyy/mm/dd")
	ls_sql = ls_sql + "and data_fattura <= '" + ls_data_a + "' "
end if		

if not isnull(ll_fattura_inizio) then
	ls_sql = ls_sql + "and num_documento >= " + string(ll_fattura_inizio) + " "
end if	
	
if not isnull(ll_fattura_fine) then
	ls_sql = ls_sql + "and num_documento <= " + string(ll_fattura_fine) + " "
end if		
	
if not isnull(ls_flag_contabilita) then
	ls_sql = ls_sql + "and flag_contabilita like '" + ls_flag_contabilita + "' "
end if	

ls_sql = ls_sql + "and cod_tipo_fat_ven not in (select cod_tipo_fat_ven from tab_tipi_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and (flag_tipo_fat_ven = 'P' or flag_tipo_fat_ven = 'F')) "	
	
prepare sqlsa from :ls_sql;

open dynamic cu_tes_fat_ven;

ll_i = 1

do while 1 = 1
	fetch cu_tes_fat_ven into :ls_cod_tipo_fat_ven_estratto, :ls_cod_cliente_estratto, :ls_cod_documento_estratto, :ls_numeratore_documento_estratto, :ll_anno_documento_estratto, :ll_num_documento_estratto, :ldt_data_fattura_estratto, :ls_flag_movimenti_estratto, :ls_flag_contabilita_estratto, :ll_importo_iva_estratto, :ll_imponibile_iva_estratto, :ll_tot_fattura_estratto, :ls_cod_agente_1_estratto, :ls_cod_agente_2_estratto, :ls_cod_vettore_estratto, :ls_cod_inoltro_estratto	;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		
		ls_des_cliente = ""
		ls_cod_zona  = ""
		ls_provincia  = ""
		ls_cod_nazione	 = ""	
		
		select rag_soc_1,
				 cod_zona,
				 provincia,
				 cod_nazione
		  into :ls_des_cliente,
		  		 :ls_cod_zona,
				 :ls_provincia, 
				 :ls_cod_nazione
		  from anag_clienti
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_cliente = :ls_cod_cliente_estratto;
		if sqlca.sqlcode = 0 then
			
			ls_des_zona = ""
			
			select des_zona
			  into :ls_des_zona
			  from tab_zone
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_zona = :ls_cod_zona;
				
			ls_des_nazione = ""	
				
			select des_nazione
			  into :ls_des_nazione 
			  from tab_nazioni
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_nazione = :ls_cod_nazione;
				
			ls_des_agente_1 = ""
			
			select rag_soc_1
			  into :ls_des_agente_1
			  from anag_agenti
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_agente = :ls_cod_agente_1_estratto;
		
			ls_des_agente_2 = ""
		
			select rag_soc_1
			  into :ls_des_agente_2
			  from anag_agenti
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_agente = :ls_cod_agente_2_estratto;
				
			ls_des_vettore = ""	
				
			select rag_soc_1
			  into :ls_des_vettore
			  from anag_vettori
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_vettore = :ls_cod_vettore_estratto;			
				
			dw_report.InsertRow(0)	
			dw_report.setitem(ll_i, "tes_fat_ven_cod_tipo_fat_ven", ls_cod_tipo_fat_ven_estratto)
			dw_report.setitem(ll_i, "tes_fat_ven_cod_cliente", ls_cod_cliente_estratto)
			dw_report.setitem(ll_i, "tes_fat_ven_importo_iva", ll_importo_iva_estratto)			
			dw_report.setitem(ll_i, "tes_fat_ven_imponibile_iva", ll_imponibile_iva_estratto)					
			dw_report.setitem(ll_i, "tes_fat_ven_tot_fattura", ll_tot_fattura_estratto)				
			dw_report.setitem(ll_i, "tes_fat_ven_flag_contabilita", ls_flag_contabilita_estratto)		
			dw_report.setitem(ll_i, "tes_fat_ven_flag_movimenti", ls_flag_movimenti_estratto)
			dw_report.setitem(ll_i, "anag_clienti_rag_soc_1", ls_des_cliente)	
			dw_report.setitem(ll_i, "tes_fat_ven_cod_documento", ls_cod_documento_estratto)	
			dw_report.setitem(ll_i, "tes_fat_ven_numeratore_documento", ls_numeratore_documento_estratto)				
			dw_report.setitem(ll_i, "tes_fat_ven_anno_documento", ll_anno_documento_estratto)
			dw_report.setitem(ll_i, "tes_fat_ven_num_documento", ll_num_documento_estratto)
			dw_report.setitem(ll_i, "tes_fat_ven_data_fattura", ldt_data_fattura_estratto)	
			dw_report.setitem(ll_i, "anag_clienti_cod_zona", ls_des_zona)	
			dw_report.setitem(ll_i, "anag_clienti_provincia", ls_provincia)
			dw_report.setitem(ll_i, "anag_clienti_cod_nazione", ls_des_nazione)
			dw_report.setitem(ll_i, "tes_fat_ven_cod_agente_1", ls_des_agente_1)
			dw_report.setitem(ll_i, "tes_fat_ven_cod_agente_2", ls_des_agente_2)
			dw_report.setitem(ll_i, "tes_fat_ven_cod_vettore", ls_des_vettore)
			dw_report.setitem(ll_i, "tes_fat_ven_cod_inoltro", ls_cod_inoltro_estratto)			
			ll_i ++
		end if	
	end if
loop

//dw_report.show()

close cu_tes_fat_ven;

value = GetFileSaveName("Select File",  & 
	docname, named, "XLS",  &
	"XLS Files (*.XLS),*.XLS")
IF value = 1 THEN 
	dw_report.SaveAs(docname, Excel!, true)
end if	
end event

type dw_selezione from uo_cs_xx_dw within w_report_fat_sint_excel
integer x = 23
integer y = 16
integer width = 2194
integer height = 524
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_sel_report_sint_fat_excel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	end choose
end event

type dw_report from uo_cs_xx_dw within w_report_fat_sint_excel
boolean visible = false
integer x = 23
integer y = 20
integer width = 3131
integer height = 1420
integer taborder = 10
string dataobject = "d_report_sint_fat_excel"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

