﻿$PBExportHeader$w_report_fatture_sint_euro.srw
forward
global type w_report_fatture_sint_euro from w_cs_xx_principale
end type
type dw_nazioni from uo_dddw_checkbox within w_report_fatture_sint_euro
end type
type dw_selezione from uo_cs_xx_dw within w_report_fatture_sint_euro
end type
type dw_report from uo_cs_xx_dw within w_report_fatture_sint_euro
end type
type dw_report_conti from uo_cs_xx_dw within w_report_fatture_sint_euro
end type
type dw_report_aliquote from uo_cs_xx_dw within w_report_fatture_sint_euro
end type
type dw_folder from u_folder within w_report_fatture_sint_euro
end type
end forward

global type w_report_fatture_sint_euro from w_cs_xx_principale
integer width = 4059
integer height = 1884
string title = "Report Elenco Sintetico Fatture"
event ue_report_sintetico ( )
event ue_report_conti ( )
event ue_report_aliquote ( )
dw_nazioni dw_nazioni
dw_selezione dw_selezione
dw_report dw_report
dw_report_conti dw_report_conti
dw_report_aliquote dw_report_aliquote
dw_folder dw_folder
end type
global w_report_fatture_sint_euro w_report_fatture_sint_euro

type variables

end variables

event ue_report_sintetico();string ls_cod_documento_inizio, ls_cod_documento_fine, ls_cod_cliente, ls_cod_tipo_fat_ven, ls_cod_agente_1, &
		ls_flag_contabilita, ls_cod_documento, ls_selezione, ls_rag_soc_1, ls_cod_deposito, ls_des_deposito, ls_cod_pagamento, &
		ls_tipo_fattura, ls_des_tipo_fattura, ls_cod_tipo_anagrafica, ls_flag_somma_cliente, ls_cod_agente, ls_cod_zona, &
		ls_cod_nazioni[], ls_empty[]
long   ll_fattura_inizio, ll_fattura_fine, ll_anno_documento, ll_visible
date   ldd_data_inizio, ldd_data_fine
integer li_tutte_le_nazioni, li_nazioni_selezionate


dw_selezione.accepttext()

dw_folder.fu_disabletab(3)
dw_folder.fu_disabletab(4)
dw_folder.fu_enabletab(2)
dw_report.setredraw(true)
dw_report.reset()
li_tutte_le_nazioni = 0

dw_report = dw_report
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "N" then
	dw_report.Object.compute_7.Expression = "'A'"
else
	dw_report.Object.compute_7.Expression = " tes_fat_ven_cod_documento "
end if

ldd_data_inizio = dw_selezione.getitemdate(1,"da_data_fattura")
ldd_data_fine = dw_selezione.getitemdate(1,"a_data_fattura")
ll_fattura_inizio = dw_selezione.getitemnumber(1,"da_num_documento")
ll_fattura_fine = dw_selezione.getitemnumber(1,"a_num_documento")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")
ls_cod_documento = dw_selezione.getitemstring(1,"cod_documento")
ls_flag_contabilita = dw_selezione.getitemstring(1,"flag_contabilita")
ls_cod_pagamento = dw_selezione.getitemstring(1, "cod_pagamento")
li_nazioni_selezionate = dw_nazioni.uof_get_selected_column_as_array("cod_nazione", ref ls_cod_nazioni, false)
	
if isnull(ls_cod_cliente) then ls_cod_cliente = "%"
if isnull(ls_cod_deposito) then ls_cod_deposito = "%"
if isnull(ls_cod_documento) then ls_cod_documento = "%"
if isnull(ldd_data_inizio) then ldd_data_inizio = date("01/01/1900")
if isnull(ldd_data_fine) then ldd_data_fine = date("31/12/2999")
if ls_flag_contabilita = "T" then ls_flag_contabilita = "%"

// ---------------  imposto la descrizione della selezione del report ------------------------------
ls_selezione = "Da data fattura: " + string(ldd_data_inizio,"dd/mm/yyyy") + " A data fattura: " + string(ldd_data_fine,"dd/mm/yyyy") + " - Da fattura nr.:" + string(ll_fattura_inizio) + " A fattura nr.:" + string(ll_fattura_fine)// + "~r~n"
if ls_cod_deposito <> "%" then
	select des_deposito
	into :ls_des_deposito
	from anag_depositi
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_deposito = :ls_cod_deposito;
	ls_selezione = ls_selezione + " - Deposito: " + ls_cod_deposito + ", " + ls_des_deposito
else
	ls_selezione = ls_selezione + " - Tutti i Depositi "
end if
if ls_cod_cliente <> "%" then
	select rag_soc_1
	into :ls_rag_soc_1
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_cliente = :ls_cod_cliente;
	ls_selezione = ls_selezione + " - Cliente: " + ls_cod_cliente + ", " + ls_rag_soc_1
else
	ls_selezione = ls_selezione + " - Tutti i clienti "
end if
if ls_cod_documento <> "%" then
	ls_selezione = ls_selezione + " - Codice Documento: " + ls_cod_documento
else
	ls_selezione = ls_selezione + " - Tutti i tipi di documenti"
end if
//ls_selezione = ls_selezione + "~r~n"
choose case ls_flag_contabilita
	case "%"
		ls_selezione = ls_selezione + " - Fatture Contabilizzate e non Contabilizzate"
	case "C"
		ls_selezione = ls_selezione + " - Solo fatture Contabilizzate"
		ls_flag_contabilita = "S"
	case "N"
		ls_selezione = ls_selezione + " - Solo fatture non Contabilizzate"
end choose
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "S" then
	ls_selezione = ls_selezione + " - Raggruppamento per tipo fattura"
else
	ls_selezione = ls_selezione + " - Senza Raggruppamento per tipo fattura"
end if

//Donato 03/04/2009 aggiunto filtro per tipo fattura
ls_tipo_fattura = dw_selezione.getitemstring(1,"cod_tipo_fat_ven")
if ls_tipo_fattura = "" or isnull(ls_tipo_fattura) then
	setnull(ls_tipo_fattura)
else
	select des_tipo_fat_ven
	into :ls_des_tipo_fattura
	from tab_tipi_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven =:ls_tipo_fattura;
	
	//ls_selezione = ls_selezione + "~r~n"
	ls_selezione = ls_selezione + " - Tipo fattura " + ls_des_tipo_fattura
end if
//fine modifica -----------------------------------------------

//Donato 18/01/2011 aggiunto filtro per cod tipo anagrafica (cliente)
ls_cod_tipo_anagrafica = dw_selezione.getitemstring(1,"cod_tipo_anagrafica")

if ls_cod_tipo_anagrafica = "" then setnull(ls_cod_tipo_anagrafica)

if not isnull(ls_cod_tipo_anagrafica) then ls_selezione +=  " - Tipo Anagrafica " + ls_cod_tipo_anagrafica
//fine modifica -----------------------------------------------

//Donato 22/02/2011 aggiunto filtro per agente (1 e/o 2) e per zona
ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
ls_cod_zona = dw_selezione.getitemstring(1,"cod_zona")

if ls_cod_agente = "" then setnull(ls_cod_agente)
if ls_cod_zona = "" then setnull(ls_cod_zona)

if not isnull(ls_cod_agente) then ls_selezione +=  " - Agente " + ls_cod_agente
if not isnull(ls_cod_zona) then ls_selezione +=  " - Zona " + ls_cod_zona
//fine modifica ----------------------------------------------------------------------------------

// stefanop 21/05/2012
if not isnull(ls_cod_pagamento) then ls_selezione += " - Pagamento " + ls_cod_pagamento


if li_nazioni_selezionate < 1 then
	setnull(ls_cod_nazioni[1])
	ls_selezione+= " - Nazioni TUTTE"
	li_tutte_le_nazioni = 1
else
	ls_selezione += " - Nazioni " + dw_selezione.getitemstring(dw_selezione.getrow(), "cod_nazione")
	li_tutte_le_nazioni = 0
end if

ls_selezione = g_str.replace(ls_selezione, '"', '~~"')
dw_report.Object.t_selezione.text = ls_selezione
// -------------------------------------------------------------------------------------------------

//22/02/2011 visibilità totali per gruppo cliente --------------------
ls_flag_somma_cliente = dw_selezione.getitemstring(1,"flag_somma_cliente")
if ls_flag_somma_cliente="S" then
	ll_visible = 1
else
	ll_visible = 0
end if

dw_report.object.compute_11.visible = ll_visible
dw_report.object.compute_12.visible = ll_visible
dw_report.object.compute_13.visible = ll_visible
dw_report.object.compute_14.visible = ll_visible
// -------------------------------------------------------------------------------------------------------------------------------------------------------

//Donato 03/04/2009 aggiunto ulteriore arg di retrieve "tipo_fattura" e cod_tipo_anagrafica (18/01/2011), e successivamente per agente e zona
dw_report.retrieve(s_cs_xx.cod_azienda, ldd_data_inizio, ldd_data_fine, ll_fattura_inizio, &
								ll_fattura_fine,  ls_cod_cliente, ls_flag_contabilita, ls_cod_documento, &
								ls_tipo_fattura, ls_cod_tipo_anagrafica, ls_cod_agente, ls_cod_zona, ls_cod_deposito, ls_cod_pagamento, &
								ls_cod_nazioni, li_tutte_le_nazioni)


// attenzione; è necesario fare il sort PRIMA del groupcalc
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_ordinamento") = "N" then
	dw_report.setsort(" tes_fat_ven_cod_documento A, tes_fat_ven_numeratore_documento A, tes_fat_ven_anno_documento A, tes_fat_ven_num_documento A")
	dw_report.sort()
else
	//ordinamento per ragione sociale
	if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "N" then
		//NON raggruppare per codice documento
		dw_report.setsort("anag_clienti_rag_soc_1 A, tes_fat_ven_cod_documento A, tes_fat_ven_numeratore_documento A, tes_fat_ven_anno_documento A, tes_fat_ven_num_documento A")
		dw_report.sort()
	else
		//Raggruppa per codice documento
		dw_report.setsort("tes_fat_ven_cod_documento A, anag_clienti_rag_soc_1 A")
		dw_report.sort()
	end if
end if

dw_report.groupcalc()

dw_report.object.datawindow.print.preview = "Yes"

dw_report.setredraw(true)
dw_folder.fu_selecttab(2)
dw_report.change_dw_current()

end event

event ue_report_conti();string ls_cod_documento_inizio, ls_cod_documento_fine, ls_cod_cliente, ls_cod_tipo_fat_ven, ls_cod_agente_1, &
		ls_flag_contabilita, ls_cod_documento, ls_selezione, ls_rag_soc_1, ls_cod_conto, ls_cod_deposito, ls_des_deposito, &
		ls_cod_pagamento, ls_cod_nazioni[], ls_empty[]
long   ll_fattura_inizio, ll_fattura_fine, ll_anno_documento, ll_visible
date   ldd_data_inizio, ldd_data_fine
string ls_tipo_fattura, ls_cod_tipo_anagrafica, ls_flag_somma_cliente, ls_cod_agente, ls_cod_zona
integer li_tutte_le_nazioni,li_nazioni_selezionate

dw_selezione.accepttext()

dw_folder.fu_disabletab(2)
dw_folder.fu_disabletab(4)
dw_folder.fu_enabletab(3)
dw_report_conti.setredraw(true)
dw_report_conti.reset()
li_tutte_le_nazioni = 0

dw_report_conti = dw_report_conti
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "N" then
	dw_report_conti.Object.compute_7.Expression = "'A'"
else
	dw_report_conti.Object.compute_7.Expression = " tes_fat_ven_cod_documento "
end if

ldd_data_inizio = dw_selezione.getitemdate(1,"da_data_fattura")
ldd_data_fine = dw_selezione.getitemdate(1,"a_data_fattura")
ll_fattura_inizio = dw_selezione.getitemnumber(1,"da_num_documento")
ll_fattura_fine = dw_selezione.getitemnumber(1,"a_num_documento")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")
ls_cod_documento = dw_selezione.getitemstring(1,"cod_documento")
ls_cod_conto = dw_selezione.getitemstring(1,"cod_conto")
ls_flag_contabilita = dw_selezione.getitemstring(1,"flag_contabilita")
ls_cod_pagamento = dw_selezione.getitemstring(1,"cod_pagamento")
li_nazioni_selezionate = dw_nazioni.uof_get_selected_column_as_array("cod_nazione", ref ls_cod_nazioni, false)


//Donato 03/04/2009 aggiunto filtro per tipo fattura
ls_tipo_fattura = dw_selezione.getitemstring(1,"cod_tipo_fat_ven")

if ls_tipo_fattura = "" then setnull(ls_tipo_fattura)
//fine modifica -----------------------------------------------

//Donato 18/01/2011 aggiunto filtro per cod tipo anagrafica (cliente)
ls_cod_tipo_anagrafica = dw_selezione.getitemstring(1,"cod_tipo_anagrafica")

if ls_cod_tipo_anagrafica = "" then setnull(ls_cod_tipo_anagrafica)
//fine modifica -----------------------------------------------
	
if isnull(ls_cod_cliente) then ls_cod_cliente = "%"
if isnull(ls_cod_deposito) then ls_cod_deposito = "%"
if isnull(ls_cod_documento) then ls_cod_documento = "%"
if isnull(ls_cod_conto) then ls_cod_conto = "%"
if isnull(ldd_data_inizio) then ldd_data_inizio = date("01/01/1900")
if isnull(ldd_data_fine) then ldd_data_fine = date("31/12/2999")
if ls_flag_contabilita = "T" then ls_flag_contabilita = "%"

// ---------------  imposto la descrizione della selezione del report ------------------------------
ls_selezione = "Da data fattura: " + string(ldd_data_inizio,"dd/mm/yyyy") + " A data fattura: " + string(ldd_data_fine,"dd/mm/yyyy") + " - Da fattura nr.:" + string(ll_fattura_inizio) + " A fattura nr.:" + string(ll_fattura_fine) //+ "~r~n"
if ls_cod_deposito <> "%" then
	select des_deposito
	into :ls_des_deposito
	from anag_depositi
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_deposito = :ls_cod_deposito;
	ls_selezione = ls_selezione + " - Deposito: " + ls_cod_deposito + ", " + ls_des_deposito
else
	ls_selezione = ls_selezione + " - Tutti i depositi "
end if
if ls_cod_cliente <> "%" then
	select rag_soc_1
	into :ls_rag_soc_1
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_cliente = :ls_cod_cliente;
	ls_selezione = ls_selezione + " - Cliente: " + ls_cod_cliente + ", " + ls_rag_soc_1
else
	ls_selezione = ls_selezione + " - Tutti i clienti "
end if
if ls_cod_documento <> "%" then
	ls_selezione = ls_selezione + " - Codice Documento: " + ls_cod_documento
else
	ls_selezione = ls_selezione + " - Tutti i tipi di documenti"
end if
//ls_selezione = ls_selezione + "~r~n"
choose case ls_flag_contabilita
	case "%"
		ls_selezione = ls_selezione + " - Fatture Contabilizzate e non Contabilizzate"
	case "C"
		ls_selezione = ls_selezione + " - Solo fatture Contabilizzate"
		ls_flag_contabilita = "S"
	case "N"
		ls_selezione = ls_selezione + " - Solo fatture non Contabilizzate"
end choose
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "S" then
	ls_selezione = ls_selezione + " - Raggruppamento per tipo fattura"
else
	ls_selezione = ls_selezione + " - Senza Raggruppamento per tipo fattura"
end if


//Donato 22/02/2011 aggiunto filtro per agente (1 e/o 2) e per zona
ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
ls_cod_zona = dw_selezione.getitemstring(1,"cod_zona")

if ls_cod_agente = "" then setnull(ls_cod_agente)
if ls_cod_zona = "" then setnull(ls_cod_zona)

if not isnull(ls_cod_agente) then ls_selezione +=  " - Agente " + ls_cod_agente
if not isnull(ls_cod_zona) then ls_selezione +=  " - Zona " + ls_cod_zona
//fine modifica ----------------------------------------------------------------------------------


// stefanop 25/05/2012
if not isnull(ls_cod_pagamento) then ls_selezione += " - Pagamento " + ls_cod_pagamento

if li_nazioni_selezionate < 1 then
	setnull(ls_cod_nazioni[1])
	ls_selezione+= " - Nazioni TUTTE"
	li_tutte_le_nazioni = 1
else
	ls_selezione += " - Nazioni " + dw_selezione.getitemstring(dw_selezione.getrow(), "cod_nazione")
	li_tutte_le_nazioni = 0
end if

ls_selezione = g_str.replace(ls_selezione, '"', '~~"')
dw_report_conti.Object.t_selezione.text = ls_selezione
// -------------------------------------------------------------------------------------------------

//22/02/2011 visibilità totali per gruppo cliente --------------------
ls_flag_somma_cliente = dw_selezione.getitemstring(1,"flag_somma_cliente")
if ls_flag_somma_cliente="S" then
	ll_visible = 1
else
	ll_visible = 0
end if

dw_report_conti.object.compute_9.visible = ll_visible
dw_report_conti.object.compute_6.visible = ll_visible
// -------------------------------------------------------------------------------------------------------------------------------------------------------


//Donato 03/04/2009 aggiunto ulteriore arg di retrieve "tipo_fattura" e cod_tipo_anagrafica (18/01/2011) e successivamente per agente e zona
dw_report_conti.retrieve(s_cs_xx.cod_azienda, ldd_data_inizio, ldd_data_fine, ll_fattura_inizio, &
										ll_fattura_fine,  ls_cod_cliente, ls_flag_contabilita, ls_cod_documento, &
										ls_cod_conto, ls_tipo_fattura, ls_cod_tipo_anagrafica,ls_cod_agente, ls_cod_zona, ls_cod_deposito, ls_cod_pagamento, &
										ls_cod_nazioni, li_tutte_le_nazioni)

// attenzione; è necesario fare il sort PRIMA del groupcalc
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_ordinamento") = "N" then
	dw_report_conti.setsort("tes_fat_ven_cod_documento A, tes_fat_ven_numeratore_documento A, tes_fat_ven_anno_documento A, tes_fat_ven_num_documento A")
	dw_report_conti.sort()		
else
	if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "N" then
		dw_report_conti.setsort("anag_clienti_rag_soc_1 A, tes_fat_ven_cod_documento A, tes_fat_ven_numeratore_documento A, tes_fat_ven_anno_documento A, tes_fat_ven_num_documento A")
		dw_report_conti.sort()
	else
		dw_report_conti.setsort("tes_fat_ven_cod_documento A, anag_clienti_rag_soc_1 A")
		dw_report_conti.sort()
	end if
end if

dw_report_conti.groupcalc()
dw_report_conti.object.datawindow.print.preview = "Yes"

dw_report_conti.setredraw(true)
dw_folder.fu_selecttab(3)
dw_report_conti.change_dw_current()

end event

event ue_report_aliquote();string ls_cod_documento_inizio, ls_cod_documento_fine, ls_cod_cliente, ls_cod_tipo_fat_ven, ls_cod_agente_1, &
		ls_flag_contabilita, ls_cod_documento, ls_selezione, ls_rag_soc_1, ls_cod_conto, ls_cod_deposito, ls_des_deposito, &
		ls_cod_pagamento, ls_cod_nazioni[], ls_empty[]
int 	li_nazioni_selezionate
long   ll_fattura_inizio, ll_fattura_fine, ll_anno_documento, ll_visible
date   ldd_data_inizio, ldd_data_fine
string ls_tipo_fattura, ls_cod_tipo_anagrafica, ls_flag_somma_cliente, ls_cod_agente, ls_cod_zona
integer li_tutte_le_nazioni

dw_selezione.accepttext()

dw_folder.fu_disabletab(2)
dw_folder.fu_disabletab(3)
dw_folder.fu_enabletab(4)
dw_report_aliquote.setredraw(true)
dw_report_aliquote.reset()
li_tutte_le_nazioni = 0

dw_report_aliquote = dw_report_aliquote
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "N" then
	dw_report_aliquote.Object.compute_7.Expression = "'A'"
else
	dw_report_aliquote.Object.compute_7.Expression = " tes_fat_ven_cod_documento "
end if

ldd_data_inizio = dw_selezione.getitemdate(1,"da_data_fattura")
ldd_data_fine = dw_selezione.getitemdate(1,"a_data_fattura")
ll_fattura_inizio = dw_selezione.getitemnumber(1,"da_num_documento")
ll_fattura_fine = dw_selezione.getitemnumber(1,"a_num_documento")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_documento = dw_selezione.getitemstring(1,"cod_documento")
ls_cod_conto = dw_selezione.getitemstring(1,"cod_conto")
ls_flag_contabilita = dw_selezione.getitemstring(1,"flag_contabilita")
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")
ls_tipo_fattura = dw_selezione.getitemstring(1,"cod_tipo_fat_ven")
ls_cod_pagamento = dw_selezione.getitemstring(1, "cod_pagamento")
li_nazioni_selezionate = dw_nazioni.uof_get_selected_column_as_array("cod_nazione", ref ls_cod_nazioni, false)


if ls_tipo_fattura = "" then setnull(ls_tipo_fattura)
//fine modifica -----------------------------------------------

//Donato 18/01/2011 aggiunto filtro per cod tipo anagrafica (cliente)
ls_cod_tipo_anagrafica = dw_selezione.getitemstring(1,"cod_tipo_anagrafica")

if ls_cod_tipo_anagrafica = "" then setnull(ls_cod_tipo_anagrafica)
//fine modifica -----------------------------------------------

if isnull(ls_cod_cliente) then ls_cod_cliente = "%"
if isnull(ls_cod_deposito) then ls_cod_deposito = "%"
if isnull(ls_cod_documento) then ls_cod_documento = "%"
if isnull(ls_cod_conto) then ls_cod_conto = "%"
if isnull(ldd_data_inizio) then ldd_data_inizio = date("01/01/1900")
if isnull(ldd_data_fine) then ldd_data_fine = date("31/12/2999")
if ls_flag_contabilita = "T" then ls_flag_contabilita = "%"

// ---------------  imposto la descrizione della selezione del report ------------------------------
ls_selezione = "Da data fattura: " + string(ldd_data_inizio,"dd/mm/yyyy") + " A data fattura: " + string(ldd_data_fine,"dd/mm/yyyy") + " - Da fattura nr.:" + string(ll_fattura_inizio) + " A fattura nr.:" + string(ll_fattura_fine) //+ "~r~n"
if ls_cod_deposito <> "%" then
	select des_deposito
	into :ls_des_deposito
	from anag_depositi
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_deposito = :ls_cod_deposito;
	ls_selezione = ls_selezione + " - Deposito: " + ls_cod_deposito + ", " + ls_des_deposito
else
	ls_selezione = ls_selezione + " - Tutti i depositi "
end if
if ls_cod_cliente <> "%" then
	select rag_soc_1
	into :ls_rag_soc_1
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_cliente = :ls_cod_cliente;
	ls_selezione = ls_selezione + " - Cliente: " + ls_cod_cliente + ", " + ls_rag_soc_1
else
	ls_selezione = ls_selezione + " - Tutti i clienti"
end if
if ls_cod_documento <> "%" then
	ls_selezione = ls_selezione + " - Codice Documento: " + ls_cod_documento
else
	ls_selezione = ls_selezione + " - Tutti i tipi di documenti"
end if
//ls_selezione = ls_selezione + "~r~n"
choose case ls_flag_contabilita
	case "%"
		ls_selezione = ls_selezione + " - Fatture Contabilizzate e non Contabilizzate"
	case "C"
		ls_selezione = ls_selezione + " - Solo fatture Contabilizzate"
		ls_flag_contabilita = "S"
	case "N"
		ls_selezione = ls_selezione + " - Solo fatture non Contabilizzate"
end choose
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "S" then
	ls_selezione = ls_selezione + " - Raggruppamento per tipo fattura"
else
	ls_selezione = ls_selezione + " - Senza Raggruppamento per tipo fattura"
end if


//Donato 22/02/2011 aggiunto filtro per agente (1 e/o 2) e per zona
ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
ls_cod_zona = dw_selezione.getitemstring(1,"cod_zona")

if ls_cod_agente = "" then setnull(ls_cod_agente)
if ls_cod_zona = "" then setnull(ls_cod_zona)

if not isnull(ls_cod_agente) then ls_selezione +=  " - Agente " + ls_cod_agente
if not isnull(ls_cod_zona) then ls_selezione +=  " - Zona " + ls_cod_zona
//fine modifica ----------------------------------------------------------------------------------

// stefanop
if not isnull(ls_cod_pagamento) then ls_selezione += " - Pagamento " + ls_cod_pagamento

//ls_selezione += " - Nazioni " + guo_functions.uof_implode(ls_cod_nazioni, ", ")
if li_nazioni_selezionate < 1 then
	setnull(ls_cod_nazioni[1])
	ls_selezione+= " - Nazioni TUTTE"
	li_tutte_le_nazioni = 1
else
	ls_selezione += " - Nazioni " + dw_selezione.getitemstring(dw_selezione.getrow(), "cod_nazione")
	li_tutte_le_nazioni = 0
end if

ls_selezione = g_str.replace(ls_selezione, '"', '~~"')
dw_report_aliquote.Object.t_selezione.text = ls_selezione
// -------------------------------------------------------------------------------------------------


//22/02/2011 visibilità totali per gruppo cliente --------------------
ls_flag_somma_cliente = dw_selezione.getitemstring(1,"flag_somma_cliente")
if ls_flag_somma_cliente="S" then
	ll_visible = 1
else
	ll_visible = 0
end if

dw_report_aliquote.object.compute_14.visible = ll_visible
dw_report_aliquote.object.compute_15.visible = ll_visible
dw_report_aliquote.object.compute_16.visible = ll_visible
dw_report_aliquote.object.compute_13.visible = ll_visible

dw_report_aliquote.object.l_13.visible = ll_visible
dw_report_aliquote.object.l_14.visible = ll_visible
dw_report_aliquote.object.l_15.visible = ll_visible
dw_report_aliquote.object.l_16.visible = ll_visible
dw_report_aliquote.object.l_17.visible = ll_visible

// -------------------------------------------------------------------------------------------------------------------------------------------------------


//Donato 03/04/2009 aggiunto ulteriore arg di retrieve "tipo_fattura" e cod_tipo_anagrafica (18/01/2011) e successivamente per agente e zona
dw_report_aliquote.retrieve(s_cs_xx.cod_azienda, ldd_data_inizio, ldd_data_fine, &
											ll_fattura_inizio, ll_fattura_fine,  ls_cod_cliente, ls_flag_contabilita, &
											ls_cod_documento, &
											ls_tipo_fattura, ls_cod_tipo_anagrafica, ls_cod_agente, ls_cod_zona, ls_cod_deposito, ls_cod_pagamento, &
											ls_cod_nazioni, li_tutte_le_nazioni)


// attenzione; è necesario fare il sort PRIMA del groupcalc
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_ordinamento") = "N" then
	dw_report_aliquote.setsort("tes_fat_ven_cod_documento A, tes_fat_ven_numeratore_documento A, tes_fat_ven_anno_documento A, tes_fat_ven_num_documento A")
	dw_report_aliquote.sort()
else
	if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_raggruppa") = "N" then
		dw_report_aliquote.setsort("anag_clienti_rag_soc_1 A, tes_fat_ven_cod_documento A, tes_fat_ven_numeratore_documento A, tes_fat_ven_anno_documento A, tes_fat_ven_num_documento A")
		dw_report_aliquote.sort()
	else
		dw_report_aliquote.setsort("tes_fat_ven_cod_documento A, anag_clienti_rag_soc_1 A")
		dw_report_aliquote.sort()
	end if
end if

dw_report_aliquote.groupcalc()

dw_report_aliquote.object.datawindow.print.preview = "Yes"

dw_report_aliquote.setredraw(true)

dw_folder.fu_selecttab(4)
dw_report_aliquote.change_dw_current()

end event

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true
dw_report_aliquote.ib_dw_report = true
dw_report_conti.ib_dw_report = true

windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_report_aliquote.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_report_conti.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report Elenco Semplice", l_objects[])
l_objects[1] = dw_report_conti
dw_folder.fu_assigntab(3, "Report Con Conti", l_objects[])
l_objects[1] = dw_report_aliquote
dw_folder.fu_assigntab(4, "Report per Aliquote", l_objects[])
l_objects[1] = dw_selezione
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(4,4)
dw_folder.fu_selecttab(1)


// DDDW nazioni
dw_nazioni.uof_set_column("cod_nazione, des_nazione", "tab_nazioni", "cod_azienda='" + s_cs_xx.cod_azienda + "'", "des_nazione")
dw_nazioni.uof_set_parent_dw(dw_selezione, "cod_nazione")
// -----
end event

on w_report_fatture_sint_euro.create
int iCurrent
call super::create
this.dw_nazioni=create dw_nazioni
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_report_conti=create dw_report_conti
this.dw_report_aliquote=create dw_report_aliquote
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_nazioni
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_report
this.Control[iCurrent+4]=this.dw_report_conti
this.Control[iCurrent+5]=this.dw_report_aliquote
this.Control[iCurrent+6]=this.dw_folder
end on

on w_report_fatture_sint_euro.destroy
call super::destroy
destroy(this.dw_nazioni)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_report_conti)
destroy(this.dw_report_aliquote)
destroy(this.dw_folder)
end on

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_conto", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//Donato 03/04/2009 aggiunto filtro per tipo fattura
f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					 "tab_tipi_fat_ven.flag_tipo_fat_ven <> 'P' and " + &
					 "tab_tipi_fat_ven.flag_tipo_fat_ven <> 'F'")
//fine modifica ----------------------------------------------

f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_anagrafica = 'C'")

//Donato 22/02/2011 aggiunto filtro per agente e zona
f_po_loaddddw_dw(dw_selezione, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_selezione, &
                 "cod_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//----------------------------------------------------------------------------------
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")				
	
// stefanop 21/05/2012
if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_selezione, &
		"cod_pagamento", &
		sqlci, &
		"tipo_pagamento", &
		"codice", &
		"descrizione", &
		"")				  
else
	dw_selezione.modify("cod_pagamento_t.visible=0")
	dw_selezione.modify("cf_cod_pagamento.visible=0")
	dw_selezione.modify("cod_pagamento.visible=0")
	
//	f_po_loaddddw_dw(dw_selezione, &
//		"cod_pagamento", &
//		sqlca, &
//		"tab_pagamenti", &
//		"cod_pagamento", &
//		"des_pagamento", &
//		"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end if
// -----

					  


end event

type dw_nazioni from uo_dddw_checkbox within w_report_fatture_sint_euro
integer x = 759
integer y = 400
integer width = 1509
integer height = 704
integer taborder = 30
boolean bringtotop = true
integer ii_width_percent = 150
end type

type dw_selezione from uo_cs_xx_dw within w_report_fatture_sint_euro
integer x = 64
integer y = 156
integer width = 2240
integer height = 1588
integer taborder = 20
string dataobject = "d_sel_report_sintetico_fatture"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
		
	case "b_report_sintetico"
		parent.triggerevent("ue_report_sintetico")
		
	case "b_report_conti"
		parent.triggerevent("ue_report_conti")
		
	case "b_report_aliquote"
		parent.triggerevent("ue_report_aliquote")
		
end choose
end event

event clicked;call super::clicked;choose case dwo.name

	case "cod_nazione"		
		dw_nazioni.show()
		
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_fatture_sint_euro
integer x = 23
integer y = 132
integer width = 3931
integer height = 1600
integer taborder = 10
string dataobject = "d_report_sint_fatture_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_report_conti from uo_cs_xx_dw within w_report_fatture_sint_euro
integer x = 23
integer y = 132
integer width = 3931
integer height = 1600
integer taborder = 20
string dataobject = "d_report_sint_fatture_euro_conti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_report_aliquote from uo_cs_xx_dw within w_report_fatture_sint_euro
integer x = 23
integer y = 132
integer width = 3931
integer height = 1600
integer taborder = 20
string dataobject = "d_report_sint_fatture_euro_aliquote"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_fatture_sint_euro
integer x = 9
integer y = 20
integer width = 3945
integer height = 1736
integer taborder = 20
boolean border = false
end type

event sqlpreview;call super::sqlpreview;string ls_str 

ls_Str = ""
ls_str = sqlsyntax

end event

