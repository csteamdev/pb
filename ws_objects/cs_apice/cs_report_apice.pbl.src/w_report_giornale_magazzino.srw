﻿$PBExportHeader$w_report_giornale_magazzino.srw
forward
global type w_report_giornale_magazzino from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_report_giornale_magazzino
end type
type dw_report from uo_cs_xx_dw within w_report_giornale_magazzino
end type
type dw_folder from u_folder within w_report_giornale_magazzino
end type
type dw_selezione from uo_cs_xx_dw within w_report_giornale_magazzino
end type
type st_1 from statictext within w_report_giornale_magazzino
end type
type cb_reset from commandbutton within w_report_giornale_magazzino
end type
type cb_report from commandbutton within w_report_giornale_magazzino
end type
end forward

global type w_report_giornale_magazzino from w_cs_xx_principale
integer width = 3703
integer height = 2052
string title = "Giornale di Magazzino"
cb_1 cb_1
dw_report dw_report
dw_folder dw_folder
dw_selezione dw_selezione
st_1 st_1
cb_reset cb_reset
cb_report cb_report
end type
global w_report_giornale_magazzino w_report_giornale_magazzino

type variables
boolean ib_negativi=true
end variables

forward prototypes
public subroutine wf_crea_gruppi (long fl_riga, string fs_cod_prodotto, string fs_des_prodotto, string fs_cod_comodo, string fs_cod_comodo_2, string fs_cod_cat_mer, string fs_cod_deposito, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, datetime fdt_data_registrazione, ref boolean fb_continue)
public function integer wf_prodotto_raggruppo (string as_cod_prodotto_raggruppo, uo_magazzino auo_mag, datetime adt_data_rif, string as_where, ref decimal ad_quant_val[], ref string as_errore, string as_flag_stock, ref string as_chiave[], ref decimal ad_giacenza_stock[], ref decimal ad_costo_medio[], ref decimal ad_quan_costo_medio[])
end prototypes

public subroutine wf_crea_gruppi (long fl_riga, string fs_cod_prodotto, string fs_des_prodotto, string fs_cod_comodo, string fs_cod_comodo_2, string fs_cod_cat_mer, string fs_cod_deposito, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, datetime fdt_data_registrazione, ref boolean fb_continue);fb_continue = false
long ll_i
string ls_gruppo[],ls_des_cat_mer,ls_des_deposito,ls_des_tipo_movimento,ls_des_tipo_mov_det,ls_cod_misura_mag
         
ls_gruppo[1] = dw_selezione.getitemstring(1,"flag_gruppo_1")
ls_gruppo[2] = dw_selezione.getitemstring(1,"flag_gruppo_2")
ls_gruppo[3] = dw_selezione.getitemstring(1,"flag_gruppo_3")
ls_gruppo[4] = dw_selezione.getitemstring(1,"flag_gruppo_4")
ls_gruppo[5] = dw_selezione.getitemstring(1,"flag_gruppo_5")

for ll_i = 1 to 5
	choose case ls_gruppo[ll_i]
		case "1"
				if isnull(fs_cod_comodo) or len(trim(fs_cod_comodo)) < 1 then 
					fb_continue = true
					exit
				end if
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),fs_cod_comodo)
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Codice (C)" + fs_cod_comodo)
				
		case "2"
				if isnull(fs_cod_comodo_2) or len(trim(fs_cod_comodo_2)) < 1 then 
					fb_continue = true
					exit
				end if
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),fs_cod_comodo_2)
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Codice (C2)" + fs_cod_comodo_2)
				
		case "3"
				if isnull(fs_cod_cat_mer) or fs_cod_cat_mer = "" then 
					fs_cod_cat_mer = "Senza Indicazione Categoria"
				else
					select des_cat_mer
					into   :ls_des_cat_mer
					from   tab_cat_mer
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_cat_mer = :fs_cod_cat_mer;
						end if
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),fs_cod_cat_mer)
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Categoria " + fs_cod_cat_mer + ", " + ls_des_cat_mer)
				
		case "4"
				if isnull(fs_cod_deposito) or fs_cod_deposito = "" then 
					fs_cod_deposito = "Senza Indicaz. Deposito"
				else
					select des_deposito
					into   :ls_des_deposito
					from   anag_depositi
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_deposito = :fs_cod_deposito;
				end if
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),fs_cod_deposito)
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Deposito " + fs_cod_deposito + ", " + ls_des_deposito)
				
		case "5"
				select cod_misura_mag
				into   :ls_cod_misura_mag
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto = :fs_cod_prodotto;

				if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),fs_cod_prodotto)
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Prodotto " + fs_cod_prodotto + ", " + fs_des_prodotto + " (" + ls_cod_misura_mag + ")")
				
		case "6"			// tipo movimento
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),fs_cod_tipo_movimento)
				
				select des_tipo_movimento
				into   :ls_des_tipo_movimento
				from   tab_tipi_movimenti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_movimento = :fs_cod_tipo_movimento;
						 
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Tipo Movimento " + fs_cod_tipo_movimento + ", " + ls_des_tipo_movimento)
	
		case "7"			// tipo movimento di dettaglio
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),fs_cod_tipo_mov_det)
				
				select des_tipo_movimento
				into   :ls_des_tipo_mov_det
				from   tab_tipi_movimenti_det
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_mov_det = :fs_cod_tipo_movimento;
						 
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Tipo Movimento Dett." + fs_cod_tipo_mov_det + ", " + ls_des_tipo_mov_det)
	
		case "8"			// data registrazione movimenti
				dw_report.setitem(fl_riga, "gruppo_"+string(ll_i),string(fdt_data_registrazione,"dd/mm/yyyy"))
				dw_report.setitem(fl_riga, "des_gruppo_"+string(ll_i),"Movimenti del " + string(fdt_data_registrazione,"dd/mm/yyyy"))
	end choose	
next	
return
end subroutine

public function integer wf_prodotto_raggruppo (string as_cod_prodotto_raggruppo, uo_magazzino auo_mag, datetime adt_data_rif, string as_where, ref decimal ad_quant_val[], ref string as_errore, string as_flag_stock, ref string as_chiave[], ref decimal ad_giacenza_stock[], ref decimal ad_costo_medio[], ref decimal ad_quan_costo_medio[]);
datastore			lds_prodotti_ragg
string					ls_sql, ls_cod_prodotto_raggruppato, ls_chiave[], ls_null[]
long					ll_tot, ll_index
dec{5}				ld_fat_conv_rag_mag
integer				li_ret, li_y
dec{4}				ld_quant_val[], ld_giacenza_stock[], ld_null[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]


ls_sql = 	"select cod_prodotto, fat_conversione_rag_mag "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_prodotto_raggruppato='"+as_cod_prodotto_raggruppo+"'"

ll_tot = guo_functions.uof_crea_datastore(lds_prodotti_ragg, ls_sql, as_errore)

//reset variabili -----------------------------
for li_y = 1 to 14
	ad_quant_val[li_y] = 0
next
as_chiave[] = ls_null[]
ad_giacenza_stock[] = ld_null[]
ad_costo_medio[] = ld_null[]
ad_quan_costo_medio[] = ld_null[]


for ll_index=1 to ll_tot
	ls_cod_prodotto_raggruppato = lds_prodotti_ragg.getitemstring(ll_index, 1)
	ld_fat_conv_rag_mag = lds_prodotti_ragg.getitemdecimal(ll_index, 2)
	
	//reset variabili -----------------------------
	for li_y = 1 to 14
		ld_quant_val[li_y] = 0
	next
	ls_chiave[] = ls_null[]
	ld_giacenza_stock[] = ld_null[]
	ld_costo_medio_stock[] = ld_null[]
	ld_quan_costo_medio_stock[] = ld_null[]
	
	//tutte le quantita vanno sommate tenendo conto del fattore di conversione del prodotoo raggruppo
	// Q raggruppo   =   Q raggruppato   x   F conv raggruppato
	
	li_ret = auo_mag.uof_saldo_prod_date_decimal(	ls_cod_prodotto_raggruppato, &
																		datetime(date(relativedate(date(adt_data_rif),-1)),00:00:00), &
																		"", ld_quant_val[], &
																		as_errore, &
																		as_flag_stock, &
																		ls_chiave[], &
																		ld_giacenza_stock[], &
																		ld_costo_medio_stock[],&
																		ld_quan_costo_medio_stock[])
	
	if li_ret<0 then
		destroy lds_prodotti_ragg
		return -1
	end if
	
	ad_quant_val[1] += ld_quant_val[1] * ld_fat_conv_rag_mag
	ad_quant_val[4] += ld_quant_val[4] * ld_fat_conv_rag_mag
	ad_quant_val[6] += ld_quant_val[6] * ld_fat_conv_rag_mag
	ad_quant_val[12] += ld_quant_val[12] * ld_fat_conv_rag_mag
	ad_quant_val[13] += ld_quant_val[13] * ld_fat_conv_rag_mag

next

destroy lds_prodotti_ragg

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report Giornale", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = st_1
l_objects[4] = cb_reset
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,4)
dw_folder.fu_selecttab(1)

end event

on w_report_giornale_magazzino.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_selezione=create dw_selezione
this.st_1=create st_1
this.cb_reset=create cb_reset
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_reset
this.Control[iCurrent+7]=this.cb_report
end on

on w_report_giornale_magazzino.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_selezione)
destroy(this.st_1)
destroy(this.cb_reset)
destroy(this.cb_report)
end on

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_1 from commandbutton within w_report_giornale_magazzino
integer x = 3218
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "EXPORT"
end type

event clicked;boolean lb_primo_giro
string ls_movimenti[], ls_gruppo_2, ls_gruppo_3, ls_gruppo_4, ls_gruppo_5,ls_flag_prodotti_movimentati, ls_flag_prodotti_fiscali, &
       ls_flag_classe, ls_sql, ls_cod_prodotto, ls_cod_deposito,ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_des_prodotto, &
		 ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer,ls_flag_dettaglio, ls_cod_misura_mag, ls_des_deposito, ls_des_cat_mer, &
		 ls_azienda_rag_soc_1, ls_azienda_localita, ls_azienda_provincia, ls_partita_iva, ls_des_tipo_movimento, ls_des_tipo_mov_det, &
		 ls_flag_saldo, ls_flag_saldo_iniziale, ls_errore, ls_cod_deposito_sel, ls_cod_prodotto_precedente, &
		 ls_testata,ls_stringa
long ll_file, ll_i, ll_y, ll_ret
dec{4} ld_quan_movimento,ld_conteggio
datetime ldt_inizio, ldt_fine, ldt_data_registrazione, ldt_data_inventario
uo_magazzino luo_magazzino

// ----------------------- impostazioni aspetti grafici del report -----------------------------------------
dw_selezione.accepttext()
dw_report.reset()
dw_report.setredraw(false)
st_1.text = "Attendere la Preparazione del Report; l'operazione potrebbe richiedere parecchi minuti."

// ----------------------- verifica impostazioni e filtri --------------------------------------------------

ldt_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_fine = dw_selezione.getitemdatetime(1,"data_fine")

ldt_data_inventario = datetime(relativedate(date(ldt_inizio), -1), 00:00:00)


if isnull(ldt_inizio) then
	g_mb.messagebox("APICE","Data Inizio non impostata.", StopSign!)
end if
if isnull(ldt_fine) then
	g_mb.messagebox("APICE","Data Fine non impostata.", StopSign!)
end if
if ldt_inizio > ldt_fine then
	g_mb.messagebox("APICE","Date di inizio e fine elaborazione incongruenti fra loro.", StopSign!)
end if


ls_flag_prodotti_movimentati = dw_selezione.getitemstring(1,"flag_prodotti_movimentati")
ls_flag_prodotti_fiscali = dw_selezione.getitemstring(1,"flag_prodotti_fiscali")
ls_flag_classe = dw_selezione.getitemstring(1,"flag_classe")
ls_flag_dettaglio = dw_selezione.getitemstring(1,"flag_dettaglio")
ls_flag_saldo_iniziale = dw_selezione.getitemstring(1,"flag_saldo_iniziale")
ls_flag_saldo = dw_selezione.getitemstring(1,"flag_saldo")
ls_cod_deposito_sel = dw_selezione.getitemstring(1,"cod_deposito")

dw_report.object.st_titolo.text = "GIORNALE DI MAGAZZINO DAL " + string(ldt_inizio,"dd/mm/yyyy") + " AL " + string(ldt_fine,"dd/mm/yyyy")
select rag_soc_1, 
       localita, 
		 provincia, 
		 partita_iva
into   :ls_azienda_rag_soc_1, 
       :ls_azienda_localita, 
		 :ls_azienda_provincia, 
		 :ls_partita_iva
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.object.st_azienda.text = ls_azienda_rag_soc_1
dw_report.object.st_indirizzo.text = ls_azienda_localita + "(" + ls_azienda_provincia + ")"
dw_report.object.st_piva.text = ls_partita_iva

// -------------------------dichiarazione cursore di caricamento movimenti nel report ----------------------
st_1.text = "Attendere la preparazione query; l'operazione potrebbe richiedere parecchi minuti."

declare cu_giornale dynamic cursor for sqlsa;

ls_sql = "(select cod_prodotto, cod_tipo_movimento, sum(quan_movimento) as quantita, count(*) as conteggio from mov_magazzino " + &
			" where mov_magazzino.cod_azienda = '" + s_cs_xx.cod_azienda +"' and " + &
					" mov_magazzino.data_registrazione >= '" + string(ldt_inizio,s_cs_xx.db_funzioni.formato_data) +"' and " + &
					" mov_magazzino.data_registrazione <= '" + string(ldt_fine,s_cs_xx.db_funzioni.formato_data) +"' "

if not isnull(ls_cod_deposito_sel) then
	ls_sql += " and mov_magazzino.cod_deposito = '" + ls_cod_deposito_sel + "') "
end if
					
ls_sql += " group by cod_prodotto, cod_tipo_movimento) "

ls_sql +=" union all " + &
			" (select cod_prodotto, cod_tipo_movimento, 0 as quantita, 0 as conteggio " + &
			" from anag_prodotti, tab_tipi_movimenti group by cod_prodotto, cod_tipo_movimento) "
 		
ls_sql += " order by cod_prodotto, cod_tipo_movimento "

ls_cod_prodotto_precedente = "inizio"

prepare sqlsa from :ls_sql;

// ----------------------------------------------------------------------------------------------- //
open cu_giornale;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in apertura del cursore 'cu_giornale'~r~n" + sqlca.sqlerrtext)
	return
end if

luo_magazzino = CREATE uo_magazzino

ll_file = fileopen("c:\temp\magazzino.xls", linemode!, write!)
ll_i = 0
lb_primo_giro=true

do while true
	fetch cu_giornale into :ls_cod_prodotto, :ls_cod_tipo_movimento, :ld_quan_movimento, :ld_conteggio ;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in scorrimento del cursore 'cu_giornale'~r~n" + sqlca.sqlerrtext)
		destroy luo_magazzino
		rollback;
		return
	end if

	st_1.text = ls_cod_prodotto + " - " + ls_cod_tipo_movimento
	
		
	if ls_cod_prodotto <> ls_cod_prodotto_precedente then
		
		if ls_cod_prodotto_precedente <> "inizio" then 
			if lb_primo_giro then
				ls_testata = "PRODOTTO"
				for ll_y = 1 to upperbound(ls_movimenti)
					ls_testata += "~t" + ls_movimenti[ll_y] + "~t" + ls_movimenti[ll_y]
				next
				
				ll_ret = filewrite(ll_file, ls_testata)
			end if
			lb_primo_giro = false
			ll_ret = filewrite(ll_file, ls_stringa)
		end if
		
		ls_cod_prodotto_precedente = ls_cod_prodotto
		ls_stringa = ls_cod_prodotto
	end if
	
	if lb_primo_giro then
		ll_i ++
		ls_movimenti[ll_i] = ls_cod_tipo_movimento
	end if
	
	ls_stringa += "~t" + string(ld_quan_movimento) + "~t" + string(ld_conteggio)
loop
FILECLOSE(ll_file)
rollback;
end event

type dw_report from uo_cs_xx_dw within w_report_giornale_magazzino
integer x = 18
integer y = 132
integer width = 3607
integer height = 1600
integer taborder = 10
string dataobject = "d_report_giornale_magazzino_gruppi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_giornale_magazzino
integer x = 9
integer y = 20
integer width = 3648
integer height = 1736
integer taborder = 20
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;st_1.text = ""
end event

type dw_selezione from uo_cs_xx_dw within w_report_giornale_magazzino
integer x = 23
integer y = 140
integer width = 3589
integer height = 1080
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_selezione_report_giornale_magazzino"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
	case "flag_gruppo_1"
		dw_selezione.setitem(1,"flag_gruppo_2",ls_null)
		dw_selezione.setitem(1,"flag_gruppo_3",ls_null)
		dw_selezione.setitem(1,"flag_gruppo_4",ls_null)
		dw_selezione.setitem(1,"flag_gruppo_5",ls_null)
		dw_selezione.object.flag_gruppo_2.protect = 0
		dw_selezione.object.flag_gruppo_3.protect = 1
		dw_selezione.object.flag_gruppo_4.protect = 1
		dw_selezione.object.flag_gruppo_5.protect = 1
		
	case "flag_gruppo_2"
		dw_selezione.setitem(1,"flag_gruppo_3",ls_null)
		dw_selezione.setitem(1,"flag_gruppo_4",ls_null)
		dw_selezione.setitem(1,"flag_gruppo_5",ls_null)
		dw_selezione.object.flag_gruppo_3.protect = 0
		dw_selezione.object.flag_gruppo_4.protect = 1
		dw_selezione.object.flag_gruppo_5.protect = 1
		
	case "flag_gruppo_3"
		dw_selezione.setitem(1,"flag_gruppo_4",ls_null)
		dw_selezione.setitem(1,"flag_gruppo_5",ls_null)
		dw_selezione.object.flag_gruppo_4.protect = 0
		dw_selezione.object.flag_gruppo_5.protect = 1
		
	case "flag_gruppo_4"
		dw_selezione.setitem(1,"flag_gruppo_5",ls_null)
		dw_selezione.object.flag_gruppo_5.protect = 0
		
end choose
end event

event ue_key;call super::ue_key;if keyflags=3 and key = keyend! then 
	ib_negativi = true
	dw_selezione.object.t_9.text = "Visualizza Saldo:"
end if

if keyflags=3 and key = keyhome! then
	ib_negativi = false
	dw_selezione.object.t_9.text = "Vis. Saldo(N):"
end if


end event

event buttonclicked;call super::buttonclicked;

choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_inizio")
	
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_fine")
		
end choose
end event

type st_1 from statictext within w_report_giornale_magazzino
integer x = 69
integer y = 1220
integer width = 2606
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_reset from commandbutton within w_report_giornale_magazzino
integer x = 2789
integer y = 1224
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;string ls_null

setnull(ls_null)

dw_selezione.setitem(1,"flag_gruppo_1", ls_null)
dw_selezione.setitem(1,"flag_gruppo_2", ls_null)
dw_selezione.setitem(1,"flag_gruppo_3", ls_null)
dw_selezione.setitem(1,"flag_gruppo_4", ls_null)
dw_selezione.setitem(1,"flag_gruppo_5", ls_null)

dw_selezione.setitem(1,"flag_prodotti_movimentati", "S")
dw_selezione.setitem(1,"flag_classe", "A")
dw_selezione.setitem(1,"flag_prodotti_fiscali", "N")
dw_selezione.setitem(1,"flag_dettaglio", "S")

end event

type cb_report from commandbutton within w_report_giornale_magazzino
integer x = 3177
integer y = 1224
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;boolean					lb_1, lb_2, lb_3, lb_continue=false ,lb_salta_prodotto=false, lb_codice_raggruppo

string						ls_gruppo[5], ls_gruppo_2, ls_gruppo_3, ls_gruppo_4, ls_gruppo_5,ls_flag_prodotti_movimentati, ls_flag_prodotti_fiscali, &
							ls_flag_classe, ls_sql, ls_cod_prodotto, ls_cod_deposito,ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_des_prodotto, &
							ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer,ls_flag_dettaglio, ls_cod_misura_mag, ls_des_deposito, ls_des_cat_mer, &
							ls_azienda_rag_soc_1, ls_azienda_localita, ls_azienda_provincia, ls_partita_iva, ls_des_tipo_movimento, ls_des_tipo_mov_det, &
							ls_flag_saldo, ls_flag_saldo_iniziale, ls_errore, ls_chiave[], ls_null_vett[], ls_cod_deposito_sel, ls_cod_prodotto_ds, &
							ls_prodotti_scartati[], ls_flag_lifo, ls_sql_prodotti, ls_where_prodotti, ls_where_movimenti, ls_log, ls_sql_base, ls_sql_base_raggruppo, &
							ls_cod_prodotto_inizio, ls_cod_prodotto_fine,ls_where_depositi
							
long						ll_riga, ll_i,ll_tipo_operazione,ll_y,ll_err, ll_cont_scartati, ll_z, ll_tot_prodotti, ll_index_prodotti, ll_count_raggruppato

dec{4}					ld_quan_movimento,ld_quant_val[],ld_giacenza,ld_giacenza_stock[],ld_costo_medio_stock[],ld_quan_costo_medio_stock[], ld_null[]

datetime					ldt_inizio, ldt_fine, ldt_data_registrazione, ldt_data_inventario

uo_magazzino			luo_magazzino

datastore				lds_prodotti



// ----------------------- impostazioni aspetti grafici del report -----------------------------------------
dw_selezione.accepttext()
dw_report.reset()
dw_report.setredraw(false)
st_1.text = "Attendere la Preparazione del Report; l'operazione potrebbe richiedere parecchi minuti."
// ----------------------- verifica impostazioni e filtri --------------------------------------------------
ldt_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_fine = dw_selezione.getitemdatetime(1,"data_fine")

ldt_data_inventario = datetime(relativedate(date(ldt_inizio), -1), 00:00:00)


if isnull(ldt_inizio) then
	g_mb.messagebox("APICE","Data Inizio non impostata.", StopSign!)
	return
end if
if isnull(ldt_fine) then
	g_mb.messagebox("APICE","Data Fine non impostata.", StopSign!)
	return
end if
if ldt_inizio > ldt_fine then
	g_mb.messagebox("APICE","Date di inizio e fine elaborazione incongruenti fra loro.", StopSign!)
	return
end if

ls_gruppo[1] = dw_selezione.getitemstring(1,"flag_gruppo_1")
ls_gruppo[2] = dw_selezione.getitemstring(1,"flag_gruppo_2")
ls_gruppo[3] = dw_selezione.getitemstring(1,"flag_gruppo_3")
ls_gruppo[4] = dw_selezione.getitemstring(1,"flag_gruppo_4")
ls_gruppo[5] = dw_selezione.getitemstring(1,"flag_gruppo_5")

ls_flag_prodotti_movimentati = dw_selezione.getitemstring(1,"flag_prodotti_movimentati")
ls_flag_prodotti_fiscali = dw_selezione.getitemstring(1,"flag_prodotti_fiscali")
ls_flag_classe = dw_selezione.getitemstring(1,"flag_classe")
ls_flag_dettaglio = dw_selezione.getitemstring(1,"flag_dettaglio")
ls_flag_saldo_iniziale = dw_selezione.getitemstring(1,"flag_saldo_iniziale")
ls_flag_saldo = dw_selezione.getitemstring(1,"flag_saldo")
ls_cod_deposito_sel = dw_selezione.getitemstring(1,"cod_deposito")
ls_flag_lifo = dw_selezione.getitemstring(1, "flag_lifo")

ls_cod_prodotto_inizio = dw_selezione.getitemstring(1,"cod_prodotto_inizio")
ls_cod_prodotto_fine = dw_selezione.getitemstring(1,"cod_prodotto_fine")

if ls_flag_classe = "N" then
	setnull(ls_flag_classe)
end if

dw_report.object.st_titolo.text = "GIORNALE DI MAGAZZINO DAL " + string(ldt_inizio,"dd/mm/yyyy") + " AL " + string(ldt_fine,"dd/mm/yyyy")
select rag_soc_1, 
       localita, 
		 provincia, 
		 partita_iva
into   :ls_azienda_rag_soc_1, 
       :ls_azienda_localita, 
		 :ls_azienda_provincia, 
		 :ls_partita_iva
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.object.st_azienda.text = ls_azienda_rag_soc_1
dw_report.object.st_indirizzo.text = ls_azienda_localita + "(" + ls_azienda_provincia + ")"
dw_report.object.st_piva.text = ls_partita_iva

ll_cont_scartati = 0


//impostazioni del report
dw_report.Object.cf_gruppo_1.Expression = 'gruppo_1'
dw_report.Object.cf_gruppo_2.Expression = 'gruppo_2'
dw_report.Object.cf_gruppo_3.Expression = 'gruppo_3'
dw_report.Object.cf_gruppo_4.Expression = 'gruppo_4'
dw_report.Object.cf_gruppo_5.Expression = 'gruppo_5'


//se viene impostato il gruppo 1 allora nascondi la sezione di dettaglio
//se invece non viene impostato alcun raggruppamento allora la sezione di dettaglio deve essere aperta per evitare
//di generare un report "vuoto"

if isnull(ls_gruppo[1]) then
	//nessun raggruppamento impostato
	dw_report.modify("datawindow.header.1.height = 0")
	dw_report.modify("datawindow.detail.height = 76")
else
	dw_report.modify("datawindow.header.1.height = 76")
	dw_report.modify("datawindow.detail.height = 0")
end if

if isnull(ls_gruppo[2]) then
	dw_report.modify("datawindow.header.2.height = 0")
else
	dw_report.modify("datawindow.header.2.height = 76")
end if

if isnull(ls_gruppo[3]) then
	dw_report.modify("datawindow.header.3.height = 0")
else
	dw_report.modify("datawindow.header.3.height = 76")
end if

if isnull(ls_gruppo[4]) then
	dw_report.modify("datawindow.header.4.height = 0")
else
	dw_report.modify("datawindow.header.4.height = 76")
end if

if isnull(ls_gruppo[5]) then
	dw_report.modify("datawindow.header.5.height = 0")
else
	dw_report.modify("datawindow.header.5.height = 76")
end if
//
//if ls_flag_dettaglio = "N" then
//	dw_report.modify("datawindow.detail.height = 0")
//else
//	dw_report.modify("datawindow.detail.height = 76")
//end if


ls_sql_base =  "select mov_magazzino.data_registrazione,"+&
							"mov_magazzino.cod_prodotto,"+&
							"mov_magazzino.cod_deposito,"+&
							"mov_magazzino.quan_movimento,"+&
							"mov_magazzino.cod_tipo_movimento,"+&
							"mov_magazzino.cod_tipo_mov_det "+&
					"from mov_magazzino "+&
					"join anag_prodotti on anag_prodotti.cod_azienda=mov_magazzino.cod_azienda and " + &
												"anag_prodotti.cod_prodotto=mov_magazzino.cod_prodotto "+&
					"where mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda +"' "
					
ls_sql_base_raggruppo =  "select mov_magazzino.data_registrazione,"+&
									"mov_magazzino.cod_prodotto,"+&
									"mov_magazzino.cod_deposito,"+&
									"(mov_magazzino.quan_movimento * anag_prodotti.fat_conversione_rag_mag) as quan_movimento,"+&
									"mov_magazzino.cod_tipo_movimento,"+&
									"mov_magazzino.cod_tipo_mov_det "+&
							"from mov_magazzino "+&
							"join anag_prodotti on anag_prodotti.cod_azienda=mov_magazzino.cod_azienda and " + &
														"anag_prodotti.cod_prodotto=mov_magazzino.cod_prodotto "+&
							"where mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda +"' "



//##########################################################################
//definisco un datastore dei prodotti da elaborare
ls_sql_prodotti = 	"select cod_prodotto,"+&
									"des_prodotto,"+&
									"cod_comodo,"+&
									"cod_comodo_2,"+&
									"cod_cat_mer, "+&
									"cod_misura_mag "+&
						"from anag_prodotti "+&
						"where anag_prodotti.cod_azienda='"+s_cs_xx.cod_azienda+"' "

ls_where_prodotti = ""
ls_where_movimenti = ""

//where datastore prodotti da elaborare ......................
if not isnull(ls_flag_classe) and ls_flag_classe<>"" then
	ls_where_prodotti += "and flag_classe_abc='" + ls_flag_classe + "' "
end if	

//se SI allora devi escludere quelli non fiscali = visualizza solo quelli fiscali
if ls_flag_prodotti_fiscali = "S" or ls_flag_prodotti_fiscali="N" then
	ls_where_prodotti += "and flag_articolo_fiscale='"+ls_flag_prodotti_fiscali+"' "
end if

//se SI allora devi far vedere solo quelli LIFO
if ls_flag_lifo = "S" or ls_flag_lifo = "N" then
	ls_where_prodotti += "and anag_prodotti.flag_lifo='"+ls_flag_lifo+"' "
end if

if not isnull(ls_cod_prodotto_inizio) and ls_cod_prodotto_inizio<>"" then
	ls_where_prodotti += "and anag_prodotti.cod_prodotto>='" + ls_cod_prodotto_inizio + "' "
end if	
if not isnull(ls_cod_prodotto_fine) and ls_cod_prodotto_fine<>"" then
	ls_where_prodotti += "and anag_prodotti.cod_prodotto<='" + ls_cod_prodotto_fine + "' "
end if		


//where cursore movimenti .......................................
ls_where_depositi = ""
if not isnull(ls_cod_deposito_sel) and ls_cod_deposito_sel<>"" then
	ls_where_movimenti += " and mov_magazzino.cod_deposito='" + ls_cod_deposito_sel + "' "
	
	ls_where_depositi = "and cod_deposito=~~'"+ls_cod_deposito_sel+"~~'"
end if
ls_where_movimenti +=" and mov_magazzino.data_registrazione>='" + string(ldt_inizio,s_cs_xx.db_funzioni.formato_data) +"' " + &
								"and mov_magazzino.data_registrazione<='" + string(ldt_fine,s_cs_xx.db_funzioni.formato_data) +"' "



ls_sql_prodotti += ls_where_prodotti + " order by cod_prodotto"

ll_tot_prodotti = guo_functions.uof_crea_datastore(lds_prodotti, ls_sql_prodotti, ls_errore)

if ll_tot_prodotti < 0 then
	g_mb.error("Errore preparazione datastore dei codici da elaborare: " + ls_errore)
	return
elseif ll_tot_prodotti=0 then
	g_mb.warning("Nessun codice da elaborare con questo filtro impostato!")
	return
end if

//dichiarazione cursore utilizzato per i movimenti di ciascun prodotto, volta per volta
declare cu_giornale dynamic cursor for sqlsa;

luo_magazzino = CREATE uo_magazzino

//############################################################
//INIZIO DS PRODOTTI
for ll_index_prodotti=1 to ll_tot_prodotti

	Yield()
	ls_cod_prodotto_ds = lds_prodotti.getitemstring(ll_index_prodotti, 1)
	//ls_log = "Elaborazione articolo " +ls_cod_prodotto_ds+" in corso ..."
	ls_log = "(" + string(ll_index_prodotti) + " di " + string(ll_tot_prodotti) + ") - Elaborazione articolo " +ls_cod_prodotto_ds+" in corso ..."
	
	//se inizia per spazio saltalo che senno si blocca tutto ....
	if left(ls_cod_prodotto, 1) = " " then continue
	
	ls_des_prodotto = lds_prodotti.getitemstring(ll_index_prodotti, 2)
	ls_cod_comodo = lds_prodotti.getitemstring(ll_index_prodotti, 3)
	ls_cod_comodo_2 = lds_prodotti.getitemstring(ll_index_prodotti, 4)
	ls_cod_cat_mer = lds_prodotti.getitemstring(ll_index_prodotti, 5)
	ls_cod_misura_mag = lds_prodotti.getitemstring(ll_index_prodotti, 6)
	
	st_1.text = ls_log

	//-----------------------------------------------------------------
	//stabilisco se si tratta di un codice di raggruppo oppure no
	lb_codice_raggruppo=false
	setnull(ll_count_raggruppato)

	select count(*)
	into	:ll_count_raggruppato
	from anag_prodotti
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_raggruppato = :ls_cod_prodotto_ds;
				
	if ll_count_raggruppato>0 then
		lb_codice_raggruppo = true
	end if
	//-----------------------------------------------------------------

	//preparo il cursore dei movimenti del codice da elaborare
	if not lb_codice_raggruppo then
		//prendo i movimenti del codice prodotto corrente "ls_cod_prodotto_ds"
		ls_sql = ls_sql_base + " and mov_magazzino.cod_prodotto='"+ls_cod_prodotto_ds+"' "
	else
		//prendo i movimenti dei codici raggruppati dal prodotto corrente "ls_cod_prodotto_ds"
		ls_sql = 	ls_sql_base_raggruppo + " and mov_magazzino.cod_prodotto IN (	select cod_prodotto "+&
																											"from anag_prodotti "+&
																											"where cod_prodotto_raggruppato='"+ls_cod_prodotto_ds+"') "
	end if
	
	//aggiungo eventuale clausole su deposito selezionato e periodo di tempo impostato)
	ls_sql += ls_where_movimenti
	
	ls_sql += "order by mov_magazzino.data_registrazione"


	//############################################
	//elaborazione movimenti del prodotto
	//############################################
	prepare sqlsa from :ls_sql;
	
	open cu_giornale;
	if sqlca.sqlcode <> 0 then
		destroy lds_prodotti
		destroy luo_magazzino
		g_mb.error("Errore in apertura del cursore 'cu_giornale': " + sqlca.sqlerrtext)
		return
	end if
	
	
	do while true
		fetch cu_giornale into		:ldt_data_registrazione,
										:ls_cod_prodotto,
										:ls_cod_deposito,
										:ld_quan_movimento,
										:ls_cod_tipo_movimento,
										:ls_cod_tipo_mov_det;
										
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore in fetch cursore movimenti prodotto "  + ls_cod_prodotto_ds + " : " + sqlca.sqlerrtext)
			destroy luo_magazzino
			destroy lds_prodotti
			return
		end if
		
		st_1.text = ls_log + " - Data Reg.Mov. "+string(ldt_data_registrazione, "dd/mm/yyyy")
		
		//se non devi visualizzare i prodotti non movimentati escludi i movimenti con quantità zero
		if ls_flag_prodotti_movimentati = "S" and ld_quan_movimento = 0 then continue
		
		
		// aggiungo un movimento di saldo periodo precedente per ogni prodotto coinvolto nel report
		// ma non ancora estratto; lo faccio una sola volta.
		if ls_flag_saldo_iniziale <> "N" then
			//ll_y = dw_report.Find("cod_prodotto = '"+ls_cod_prodotto+ "'", 1, dw_report.rowcount() )
			ll_y = dw_report.Find("cod_prodotto = '"+ls_cod_prodotto_ds+ "'", 1, dw_report.rowcount() )
			
			if ll_y = 0 then 		// prima volta che incontro questo prodotto
				
				choose case ls_flag_saldo_iniziale
					case "S"		// carico il saldo iniziale nella q.ta di carico
			
						for ll_y = 1 to 14
							ld_quant_val[ll_y] = 0
						next
						ls_chiave[] = ls_null_vett[]
						ld_giacenza_stock[] = ld_null[]
						ld_costo_medio_stock[] = ld_null[]
						ld_quan_costo_medio_stock[] = ld_null[]
						
						if not lb_codice_raggruppo then
							ll_err = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_ds, &
																							ldt_data_inventario, &
																							ls_where_depositi, &
																							ld_quant_val[], &
																							ls_errore, &
																							"N", &
																							ls_chiave[], &
																							ld_giacenza_stock[], &
																							ld_costo_medio_stock[],&
																							ld_quan_costo_medio_stock[])
					
						else
							//si tratta di un codice che raggruppa, quindi la giacenza va valutata sommando le giacenze
							//dei codici raggruppati moltiplicate per i fattori di conversione
							ll_err = wf_prodotto_raggruppo(		ls_cod_prodotto_ds,&
																			luo_magazzino,&
																			ldt_data_inventario,&
																			ls_where_depositi,&
																			ld_quant_val[],&
																			ls_errore,&
																			"N",&
																			ls_chiave[],&
																			ld_giacenza_stock[],&
																			ld_costo_medio_stock[],&
																			ld_quan_costo_medio_stock[])
						end if
					
						ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
						
						ll_riga = dw_report.insertrow(0)
						ls_des_cat_mer = ""
						ls_des_deposito = ""
						
						lb_continue = false
						
						//wf_crea_gruppi(ll_riga, ls_cod_prodotto, ls_des_prodotto, ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer, ls_cod_deposito, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ldt_data_registrazione, ref lb_continue )
						wf_crea_gruppi(ll_riga, ls_cod_prodotto_ds, ls_des_prodotto, ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer, ls_cod_deposito, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ldt_data_registrazione, ref lb_continue )
						
						if lb_continue = true then
							dw_report.deleterow(ll_riga)
							continue
						end if
						
						
						dw_report.setitem(ll_riga, "data_registrazione",ldt_inizio)
						//dw_report.setitem(ll_riga, "cod_prodotto",ls_cod_prodotto)
						dw_report.setitem(ll_riga, "cod_prodotto",ls_cod_prodotto_ds)
						dw_report.setitem(ll_riga, "des_prodotto",ls_des_prodotto)
						dw_report.setitem(ll_riga, "flag_dettaglio",ls_flag_dettaglio)
						dw_report.setitem(ll_riga, "cod_misura_mag",ls_cod_misura_mag)
						
						// può essere solo un carico
						dw_report.setitem(ll_riga, "quan_carico", ld_giacenza)
						
						
					case "F"		// carico il saldo iniziale nella q.ta di carico
						
						// verifico se è fra gli scartati
						//////////////////lb_salta_prodotto = false
						
						//////////////for ll_z = 1 to ll_cont_scartati
						////////////////	if ls_cod_prodotto = ls_prodotti_scartati[ll_z] then
							///////////	lb_salta_prodotto = true
							/////////////	exit
							////////////////end if
						/////////////next
						
						////////if lb_salta_prodotto then continue		// salto la riga giornale perchè è un prodotto con saldo di fine periodo negativo
	
						// non è fra gli scartati, quindi vado a vedere saldo di fine periodo
						ldt_data_inventario = ldt_fine
						for ll_y = 1 to 14
							ld_quant_val[ll_y] = 0
						next
						ls_chiave[] = ls_null_vett[]
						ld_giacenza_stock[] = ld_null[]
						ld_costo_medio_stock[] = ld_null[]
						ld_quan_costo_medio_stock[] = ld_null[]
						
						if not lb_codice_raggruppo then
							ll_err = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_ds, &
																							ldt_data_inventario, &
																							ls_where_depositi, &
																							ld_quant_val[], &
																							ls_errore, &
																							"N", &
																							ls_chiave[], &
																							ld_giacenza_stock[], &
																							ld_costo_medio_stock[],&
																							ld_quan_costo_medio_stock[])
						else
							//si tratta di un codice che raggruppa, quindi la giacenza va valutata sommando le giacenze
							//dei codici raggruppati moltiplicate per i fattori di conversione
							ll_err = wf_prodotto_raggruppo(		ls_cod_prodotto_ds,&
																			luo_magazzino,&
																			ldt_data_inventario,&
																			ls_where_depositi,&
																			ld_quant_val[],&
																			ls_errore,&
																			"N",&
																			ls_chiave[],&
																			ld_giacenza_stock[],&
																			ld_costo_medio_stock[],&
																			ld_quan_costo_medio_stock[])
						end if
					
					
						ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
						
						// se giacenza < 0 allora salto la riga e lo includo fra i prodotti scartati
						// così non calcolerò più il saldo.
							//////if ld_giacenza < 0 then
								//////ll_cont_scartati ++
								//////ls_prodotti_scartati[ll_cont_scartati]= ls_cod_prodotto
								//////continue
							///////end if
						
				end choose					
			end if		
		end if
		
		ll_riga = dw_report.insertrow(0)
		ls_des_cat_mer = ""
		ls_des_deposito = ""
		
		lb_continue = false
		
		//wf_crea_gruppi(ll_riga, ls_cod_prodotto, ls_des_prodotto, ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer, ls_cod_deposito, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ldt_data_registrazione, ref lb_continue )
		wf_crea_gruppi(ll_riga, ls_cod_prodotto_ds, ls_des_prodotto, ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer, ls_cod_deposito, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ldt_data_registrazione, ref lb_continue )
		
	
		if lb_continue = true then
			dw_report.deleterow(ll_riga)
			continue
		end if
		
		dw_report.setitem(ll_riga, "data_registrazione",ldt_data_registrazione)
		//dw_report.setitem(ll_riga, "cod_prodotto",ls_cod_prodotto)
		dw_report.setitem(ll_riga, "cod_prodotto",ls_cod_prodotto_ds)
		dw_report.setitem(ll_riga, "des_prodotto",ls_des_prodotto)
		dw_report.setitem(ll_riga, "flag_dettaglio",ls_flag_dettaglio)
		dw_report.setitem(ll_riga, "cod_misura_mag",ls_cod_misura_mag)
		
		luo_magazzino.uof_tipo_movimento(ls_cod_tipo_mov_det, ref ll_tipo_operazione, ref lb_1, ref lb_2, ref lb_3)
		
		choose case ll_tipo_operazione
			case 1,5,19,13  		// movimenti di carico
				dw_report.setitem(ll_riga, "quan_carico", ld_quan_movimento)
				dw_report.setitem(ll_riga, "quan_scarico", 0)
			case 2,6,20,14	 		// rettifiche carico
				dw_report.setitem(ll_riga, "quan_carico", 0)
				dw_report.setitem(ll_riga, "quan_scarico",ld_quan_movimento)
			case 3,7	 				// movimenti si scarico
				dw_report.setitem(ll_riga, "quan_carico", 0)
				dw_report.setitem(ll_riga, "quan_scarico", ld_quan_movimento)
			case 4,8	 				// rettifiche scarico
				dw_report.setitem(ll_riga, "quan_carico",ld_quan_movimento)
				dw_report.setitem(ll_riga, "quan_scarico", 0)
			case else  // 9,15,10,16,11,17,12,18
				// movimenti di rettifica solo valore e quindi salti
				
		end choose
		
		
	loop
	
	close cu_giornale;
	//############################################
	//FINE elaborazione movimenti del prodotto
	//############################################

next

destroy luo_magazzino
destroy lds_prodotti
//FINE DS PRODOTTI
//##########################################################################


st_1.text = "Finito!"


// nascondo le colonne relative al saldo
if ls_flag_saldo = "N" then
	dw_report.object.t_saldo.visible = 0
	dw_report.object.saldo.visible = 0
	dw_report.object.diff_1.visible = 0
	dw_report.object.diff_2.visible = 0
	dw_report.object.diff_3.visible = 0
	dw_report.object.diff_4.visible = 0
	dw_report.object.diff_5.visible = 0
else
	dw_report.object.t_saldo.visible = 1
	dw_report.object.saldo.visible = 1
	dw_report.object.diff_1.visible = 1
	dw_report.object.diff_2.visible = 1
	dw_report.object.diff_3.visible = 1
	dw_report.object.diff_4.visible = 1
	dw_report.object.diff_5.visible = 1
end if
			
dw_report.groupcalc()

if not ib_negativi then
	dw_report.setfilter(" diff_1 >= 0 ")
	dw_report.filter()
else
	dw_report.setfilter("")
	dw_report.filter()
end if

dw_report.setredraw(true)
dw_folder.fu_selecttab(2)

dw_report.change_dw_current()

end event

