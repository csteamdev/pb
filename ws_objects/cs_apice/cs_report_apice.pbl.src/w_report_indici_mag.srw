﻿$PBExportHeader$w_report_indici_mag.srw
$PBExportComments$indici magazzino
forward
global type w_report_indici_mag from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_indici_mag
end type
type cb_selezione from commandbutton within w_report_indici_mag
end type
type dw_selezione from uo_cs_xx_dw within w_report_indici_mag
end type
type dw_report from uo_cs_xx_dw within w_report_indici_mag
end type
type cb_esci from commandbutton within w_report_indici_mag
end type
type sle_1 from singlelineedit within w_report_indici_mag
end type
type cb_report from commandbutton within w_report_indici_mag
end type
end forward

global type w_report_indici_mag from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Indici Magazzino"
cb_annulla cb_annulla
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
cb_esci cb_esci
sle_1 sle_1
cb_report cb_report
end type
global w_report_indici_mag w_report_indici_mag

type variables
boolean ib_interrupt
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2172
this.height = 633

end event

on w_report_indici_mag.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_esci=create cb_esci
this.sle_1=create sle_1
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_selezione
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.cb_esci
this.Control[iCurrent+6]=this.sle_1
this.Control[iCurrent+7]=this.cb_report
end on

on w_report_indici_mag.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_esci)
destroy(this.sle_1)
destroy(this.cb_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_cat_mer", &
							sqlca, &
							"tab_cat_mer", &
							"cod_cat_mer", &
                     "des_cat_mer", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_annulla from commandbutton within w_report_indici_mag
integer x = 1349
integer y = 400
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;ib_interrupt = true
end event

type cb_selezione from commandbutton within w_report_indici_mag
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2172
parent.height = 633

dw_report.hide()
cb_selezione.hide()
cb_annulla.visible = true
cb_esci.visible = true
dw_selezione.object.b_ricerca_prodotto.visible = true
cb_report.visible = true


dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_indici_mag
integer x = 23
integer y = 20
integer width = 2080
integer height = 360
integer taborder = 30
string dataobject = "d_selezione_indici_mag"
end type

event pcd_new;call super::pcd_new;date ldt_oggi

ldt_oggi = today()
dw_selezione.setitem(1, "data_a", ldt_oggi)

dw_selezione.setitem(1, "periodo", "S")



end event

event itemchanged;call super::itemchanged;string ls_nomecol, ls_nulla, ls_descrizione 

setnull(ls_nulla)

ls_nomecol = i_colname
if ls_nomecol = "cod_prodotto" then
	setitem(getrow(), "cod_cat_mer", ls_nulla)
	ls_descrizione = f_des_tabella("anag_prodotti","cod_prodotto = '" +  i_coltext + "'","des_prodotto")
	if ls_descrizione = "< CODICE INESISTENTE >" then return 2

end if
if ls_nomecol = "cod_cat_mer" then
	setitem(getrow(), "cod_prodotto", ls_nulla)
end if


end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_indici_mag
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 20
string dataobject = "d_report_indici_mag"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_esci from commandbutton within w_report_indici_mag
integer x = 1029
integer y = 400
integer width = 297
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

event clicked;close(parent)
end event

type sle_1 from singlelineedit within w_report_indici_mag
integer x = 23
integer y = 380
integer width = 983
integer height = 100
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean autohscroll = false
end type

type cb_report from commandbutton within w_report_indici_mag
integer x = 1737
integer y = 400
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto_sel, ls_des_prodotto, ls_cod_cat_mer, ls_periodo, &
		 ls_sql, ls_cod_prodotto, ls_where, ls_error, ls_chiave[], ls_intestazione
date ldt_data_da, ldt_data_a, ldt_date_intermedie[]
long ll_i, ll_periodo, ll_num_stock, ll_newrow, ll_num_righe, ll_j, ll_k, ll_num_periodi
integer li_cont, li_return
dec{4} ld_quant_val[], ld_giacenza, ld_giacenza_stock[], ld_num_periodi, &
		 ld_prog_cons1, ld_prog_cons2, ld_prog_cons, ld_val_cons1, ld_val_cons2, ld_val_cons, &
		 ld_giac_m_date, ld_giac_media, ld_indice_rot, ld_cons_medio, ld_periodi_scorta, &
		 ld_giac_m_1, ld_giac_m_2, ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_date_inter[]
uo_magazzino luo_magazzino

dw_selezione.accepttext()
dw_report.reset()
ib_interrupt = false

ls_cod_prodotto_sel = dw_selezione.getitemstring(1, "cod_prodotto")
ls_cod_cat_mer = dw_selezione.getitemstring(1, "cod_cat_mer")
ls_periodo = dw_selezione.getitemstring(1, "periodo")

ldt_data_da = dw_selezione.getitemdate(1, "data_da")
ldt_data_a = dw_selezione.getitemdate(1, "data_a")

// controlli preventivi
if isnull(ldt_data_da) or isnull(ldt_data_a) then
	g_mb.messagebox("Apice", "Inserire entrambe le date su cui si vogliono calcolare gli indici")
	return
end if
if ldt_data_da >= ldt_data_a then
	g_mb.messagebox("Apice", "La data fine intervallo deve essere superiore alla data inizio intervallo ! ")
	return
end if

// calcolo date intermedie: inizio
if ls_periodo = "S" then ll_periodo = 7
if ls_periodo = "M" then ll_periodo = 30
if ls_periodo = "A" then ll_periodo = 365

ll_num_periodi = 1
ldt_date_intermedie[1] = ldt_data_da

do while ldt_date_intermedie[ll_num_periodi] < ldt_data_a
	ll_num_periodi = ll_num_periodi + 1
	ldt_date_intermedie[ll_num_periodi] = relativedate( ldt_data_da, ((ll_num_periodi - 1) * ll_periodo )) 
loop
ldt_date_intermedie[ll_num_periodi] = ldt_data_a

for ll_j = 1 to ll_num_periodi	// li converto in datetime
	ldt_date_inter[ll_j] = datetime(ldt_date_intermedie[ll_j])
next
ld_num_periodi = (daysafter(ldt_data_da, ldt_data_a)) / ll_periodo
if ld_num_periodi = 0 then ld_num_periodi = 1
// calcolo date intermedie: fine

ls_intestazione = "Da data:" + string(ldt_data_da,"dd/mm/yyyy") + &
			" a data:" + string(ldt_data_a,"dd/mm/yyyy") + "; n° giorni periodo: " + string(ll_periodo)
if not isnull(ls_cod_prodotto_sel) then  
	ls_intestazione = ls_intestazione + "; prodotto: " + ls_cod_prodotto_sel
end if

if not isnull(ls_cod_cat_mer) then  
	ls_intestazione = ls_intestazione + "; cat. merc.: " + ls_cod_cat_mer
end if




// leggo prodotti
if isnull(ls_cod_cat_mer) or ls_cod_cat_mer = "" then
	if isnull(ls_cod_prodotto_sel) then 
		ls_sql = "	select cod_prodotto, des_prodotto " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and flag_blocco = 'N'"
	else
		ls_sql = "	select cod_prodotto, des_prodotto " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_prodotto like '" + ls_cod_prodotto_sel + "' " +&
					" and flag_blocco = 'N'"
	end if
					
else
	ls_sql = "	select cod_prodotto, des_prodotto " +&
				" from anag_prodotti " +&
				" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
				" and cod_cat_mer = '" + ls_cod_cat_mer + "' "	+&
				" and flag_blocco = 'N'"
end if
				
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;

DO while 0=0
	Yield()
	if ib_interrupt then  // var set in other script
		g_mb.messagebox("Indici Magazzino","Operazione interrotta dall'utente")
		exit
	end if
	FETCH cur_1 INTO :ls_cod_prodotto, :ls_des_prodotto ;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		return -1
	end if

	sle_1.text= "prodotto :" + ls_cod_prodotto
	ll_num_stock = 0
	ll_newrow = 0
	
	select count(*)
	into :ll_num_stock
	from stock
	where cod_azienda = :s_cs_xx.cod_azienda
	  and cod_prodotto = :ls_cod_prodotto;
	  
	if ll_num_stock > 0 then		// elaboro solo prodotti in stock
		ll_newrow = dw_report.insertrow(0) 
		dw_report.ScrollToRow(ll_newrow)
		dw_report.setitem(ll_newrow, "cod_prodotto" ,ls_cod_prodotto)
		dw_report.setitem(ll_newrow, "des_prodotto"  ,ls_des_prodotto)
	end if
LOOP
CLOSE cur_1 ;

ll_num_righe = dw_report.rowcount()
for ll_k = 1 to ll_num_righe
	Yield()
	if ib_interrupt then  // var set in other script
		g_mb.messagebox("Indici Magazzino","Operazione interrotta dall'utente")
		exit
	end if
	ls_cod_prodotto = dw_report.getitemstring(ll_k, "cod_prodotto")
	sle_1.text= "prodotto :" + ls_cod_prodotto
	
	ld_giac_media = 0
	ld_prog_cons = 0 
	ld_val_cons = 0 
	ld_indice_rot = 0 
	ld_periodi_scorta = 0

	for ll_i = 1 to 14
		ld_quant_val[ll_i] = 0
	next
	
	SELECT saldo_quan_inizio_anno,   
			 val_inizio_anno  
	 INTO :ld_quant_val[1],   
			:ld_quant_val[2]  
	 FROM anag_prodotti  
	WHERE cod_prodotto = :s_cs_xx.cod_azienda AND  
			cod_prodotto = :ls_cod_prodotto ;

	ld_quant_val[2] = ld_quant_val[1] * ld_quant_val[2]

	// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
	// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
	// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven

		luo_magazzino = CREATE uo_magazzino

		li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_date_inter[1], ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

		destroy luo_magazzino
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		ld_prog_cons1 =  ld_quant_val[6]
		ld_val_cons1 = ld_quant_val[7]
		ld_giac_m_1 = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]

	
	for ll_j = 2 to ll_num_periodi 	// date intermedie
		for ll_i = 1 to 14
			ld_quant_val[ll_i] = 0
		next
		// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
		// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
		// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven

		SELECT saldo_quan_inizio_anno,   
				 val_inizio_anno  
		 INTO :ld_quant_val[1],   
				:ld_quant_val[2]  
		 FROM anag_prodotti  
		WHERE cod_prodotto = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :ls_cod_prodotto ;
	
		ld_quant_val[2] = ld_quant_val[1] * ld_quant_val[2]

		luo_magazzino = CREATE uo_magazzino
		
		li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_date_inter[ll_j], ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

		destroy luo_magazzino
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if

		ld_giac_m_2 = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
		ld_giac_m_date = (ld_giac_m_1 + ld_giac_m_2)/ 2
		ld_giac_media = ld_giac_media + ld_giac_m_date
		ld_giac_m_1 = ld_giac_m_2
	next 	// ll_j: date intermedie 

	ld_prog_cons2 =  ld_quant_val[6]
	ld_val_cons2 = ld_quant_val[7]
	
	ld_prog_cons = ld_prog_cons2 - ld_prog_cons1
	ld_val_cons = ld_val_cons2 - ld_val_cons1
	
	ld_prog_cons = round(ld_prog_cons, 4)
	ld_val_cons = round(ld_val_cons, 4)
	
	ld_giac_media = ld_giac_media / ld_num_periodi
	ld_giac_media = round(ld_giac_media, 4)

	if ld_giac_media <> 0 then
		ld_indice_rot = ld_prog_cons / ld_giac_media
	else
		ld_indice_rot = 0
	end if
	ld_indice_rot = round(ld_indice_rot, 4)

	ld_cons_medio = ld_prog_cons / ld_num_periodi
	if ld_cons_medio = 0 then 
		ld_periodi_scorta = 0
	else
		ld_periodi_scorta = ld_giac_m_2 / ld_cons_medio
	end if
	ld_periodi_scorta = round(ld_periodi_scorta, 4)
	
	dw_report.setitem(ll_k, "prog_qta_consumi", ld_prog_cons)
	dw_report.setitem(ll_k, "prog_val_consumi", ld_val_cons)
	dw_report.setitem(ll_k, "giacenza_media", ld_giac_media)
	dw_report.setitem(ll_k, "indice_rot", ld_indice_rot)
	dw_report.setitem(ll_k, "periodi_scorta", ld_periodi_scorta)
next // ll_k: righe dw_report


dw_report.object.intestazione.text = ls_intestazione



dw_selezione.visible=false
parent.x = 74
parent.y = 301
parent.width = 3553
parent.height = 1665

dw_report.visible=true
cb_selezione.visible = true
cb_annulla.visible = false
cb_esci.visible = false
dw_selezione.object.b_ricerca_prodotto.visible = false
cb_report.visible = false
dw_report.Change_DW_Current()


end event

