﻿$PBExportHeader$w_report_lista_prelievo.srw
$PBExportComments$Finestra Report Lista Prelievo
forward
global type w_report_lista_prelievo from window
end type
type dw_report from datawindow within w_report_lista_prelievo
end type
type cb_1 from commandbutton within w_report_lista_prelievo
end type
type dw_selezione from datawindow within w_report_lista_prelievo
end type
end forward

global type w_report_lista_prelievo from window
integer x = 1074
integer y = 484
integer width = 3584
integer height = 2148
boolean titlebar = true
string title = "Report Lista Prelievo a Magazzino"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
dw_report dw_report
cb_1 cb_1
dw_selezione dw_selezione
end type
global w_report_lista_prelievo w_report_lista_prelievo

event open;dw_selezione.insertrow(0)
dw_selezione.setitem(1,"anno_registrazione",year(today()))
end event

on w_report_lista_prelievo.create
this.dw_report=create dw_report
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_report,&
this.cb_1,&
this.dw_selezione}
end on

on w_report_lista_prelievo.destroy
destroy(this.dw_report)
destroy(this.cb_1)
destroy(this.dw_selezione)
end on

type dw_report from datawindow within w_report_lista_prelievo
integer x = 23
integer y = 680
integer width = 3406
integer height = 1320
integer taborder = 3
string dataobject = "d_report_lista_prelievo"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_1 from commandbutton within w_report_lista_prelievo
integer x = 2194
integer y = 560
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA"
end type

event clicked;string ls_cod_cliente, ls_rag_soc_1, ls_localita, ls_cap, ls_provincia, ls_telefono, ls_giorno_chiusura, ls_indirizzo, &
       ls_cdente
long   ll_anno_registrazione, ll_num_reg_inizio, ll_num_reg_fine, ll_cont, ll_i, ll_anno_reg_ord_ven, ll_num_reg_ord_ven, &
       ll_righe, ll_y, ll_riga
datetime ldt_data_reg_inizio, ldt_data_reg_fine, ldt_data_cons_inizio, ldt_data_cons_fine
datastore ds_lista, ds_righe

dw_selezione.accepttext()
ds_lista = create datastore
ds_lista.dataobject = 'd_ds_lista_prel_lista_ordini'
ds_lista.settransobject(sqlca)
ds_righe = create datastore
ds_righe.dataobject = 'd_ds_lista_prel_dati_ordine'
ds_righe.settransobject(sqlca)
dw_report.reset()

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
if isnull(ls_cod_cliente) then ls_cod_cliente = "%"
ll_anno_registrazione = dw_selezione.getitemnumber(1,"anno_registrazione")
ldt_data_reg_inizio = dw_selezione.getitemdatetime(1,"data_reg_inizio")
if isnull(ldt_data_reg_inizio) then ldt_data_reg_inizio = datetime(date("01/01/1900"), 00:00:00)
ldt_data_reg_fine = dw_selezione.getitemdatetime(1,"data_reg_fine")
if isnull(ldt_data_reg_fine) then ldt_data_reg_fine = datetime(date("01/01/1900"), 00:00:00)
ldt_data_cons_inizio = dw_selezione.getitemdatetime(1,"data_consegna_inizio")
if isnull(ldt_data_cons_inizio) then ldt_data_cons_inizio = datetime(date("01/01/1900"), 00:00:00)
ldt_data_cons_fine = dw_selezione.getitemdatetime(1,"data_consegna_fine")
if isnull(ldt_data_cons_fine) then ldt_data_cons_fine = datetime(date("01/01/1900"), 00:00:00)
ll_num_reg_inizio = dw_selezione.getitemnumber(1,"num_reg_inizio")
if isnull(ll_num_reg_inizio) or ll_num_reg_inizio = 0 then ll_num_reg_inizio = 1
ll_num_reg_fine = dw_selezione.getitemnumber(1,"num_reg_fine")
if isnull(ll_num_reg_fine) or ll_num_reg_inizio = 0 then ll_num_reg_inizio = 999999

ll_cont = ds_lista.retrieve(s_cs_xx.cod_azienda, ldt_data_reg_inizio, ldt_data_reg_fine, ldt_data_cons_inizio, ldt_data_cons_fine, ls_cod_cliente, ll_num_reg_inizio, ll_num_reg_fine, ll_anno_registrazione)
if ll_cont < 1 then
	g_mb.messagebox("APICE","Nessun ordine trovato con queste selezioni")
	return
end if
dw_report.setredraw(false)
for ll_i = 1 to ll_cont
	ll_anno_reg_ord_ven = ds_lista.getitemnumber(ll_i, "tes_ord_ven_anno_registrazione")
	ll_num_reg_ord_ven = ds_lista.getitemnumber(ll_i, "tes_ord_ven_num_registrazione")
	ll_righe = ds_righe.retrieve(s_cs_xx.cod_azienda, ll_anno_reg_ord_ven, ll_num_reg_ord_ven)
	for ll_y = 1 to ll_righe
		
		ll_riga = dw_report.insertrow(0)
		ls_cod_cliente = ds_righe.getitemstring(ll_y,"tes_ord_ven_cod_cliente")
		dw_report.setitem(ll_riga, "anno_registrazione", ds_righe.getitemnumber(ll_y,"tes_ord_ven_anno_registrazione"))
		dw_report.setitem(ll_riga, "num_registrazione", ds_righe.getitemnumber(ll_y,"tes_ord_ven_num_registrazione"))
		dw_report.setitem(ll_riga, "prog_riga_ord_ven", ds_righe.getitemnumber(ll_y,"det_ord_ven_prog_riga_ord_ven"))
		dw_report.setitem(ll_riga, "sconto_1", ds_righe.getitemnumber(ll_y,"det_ord_ven_sconto_1"))
		dw_report.setitem(ll_riga, "prezzo_vendita", ds_righe.getitemnumber(ll_y,"det_ord_ven_prezzo_vendita"))
		dw_report.setitem(ll_riga, "num_confezioni", ds_righe.getitemnumber(ll_y,"det_ord_ven_num_confezioni"))
		dw_report.setitem(ll_riga, "num_pezzi_cobfezione", ds_righe.getitemnumber(ll_y,"det_ord_ven_num_pezzi_confezione"))
		dw_report.setitem(ll_riga, "num_colli", ds_righe.getitemnumber(ll_y,"tes_ord_ven_num_colli"))
		
		dw_report.setitem(ll_riga, "anag_prodotti_pezzi", ds_righe.getitemnumber(ll_y,"anag_prodotti_pezzi_collo"))
		
		// dati del cliente
		dw_report.setitem(ll_riga, "cod_cliente", ds_righe.getitemstring(ll_y,"tes_ord_ven_cod_cliente"))
		
		// select su anag_prodotti
		select rag_soc_1,   
				 indirizzo,   
				 localita,   
				 cap,   
				 provincia,   
				 telefono  
		 into :ls_rag_soc_1,   
				:ls_indirizzo,   
				:ls_localita,   
				:ls_cap,   
				:ls_provincia,   
				:ls_telefono  
		 from  anag_clienti  
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :ls_cod_cliente;

		dw_report.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		dw_report.setitem(ll_riga, "indirizzo", ls_indirizzo)
		dw_report.setitem(ll_riga, "cap", ls_cap)
		dw_report.setitem(ll_riga, "localita", ls_localita)
		dw_report.setitem(ll_riga, "provincia", ls_provincia)
		dw_report.setitem(ll_riga, "telefono", ls_telefono)
		dw_report.setitem(ll_riga, "giorno_chiusura", "")
		
		// select per indicazione dell'ente del cliente
		select cdente
		into   :ls_cdente
		from   col_clienti_archclie
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cliente = :ls_cod_cliente;
		dw_report.setitem(ll_riga, "cdente", ls_cdente)
			
		
		// destinazione merce
		if not isnull(ds_righe.getitemstring(ll_y,"tes_ord_ven_rag_soc_1")) then
			dw_report.setitem(ll_riga, "anag_des_clienti_rag_soc_1", ds_righe.getitemstring(ll_y,"tes_ord_ven_rag_soc_1"))
		end if
		
		if not isnull(ds_righe.getitemstring(ll_y,"tes_ord_ven_indirizzo")) then
			dw_report.setitem(ll_riga, "anag_des_clienti_indirizzo", ds_righe.getitemstring(ll_y,"tes_ord_ven_indirizzo"))
		end if
		
		if not isnull(ds_righe.getitemstring(ll_y,"tes_ord_ven_localita")) then
			dw_report.setitem(ll_riga, "anag_des_clienti_localita", ds_righe.getitemstring(ll_y,"tes_ord_ven_localita"))
		end if
		
		if not isnull(ds_righe.getitemstring(ll_y,"tes_ord_ven_cap")) then
			dw_report.setitem(ll_riga, "anag_des_clienti_cap", ds_righe.getitemstring(ll_y,"tes_ord_ven_cap"))
		end if
		
		if not isnull(ds_righe.getitemstring(ll_y,"tes_ord_ven_provincia")) then
			dw_report.setitem(ll_riga, "anag_des_clienti_provincia", ds_righe.getitemstring(ll_y,"tes_ord_ven_provincia"))
		end if
		
		// ubicazione del prodotto
		if ds_righe.getitemnumber(ll_y,"det_ord_ven_prog_riga_ord_ven") = 9999 then
			dw_report.setitem(ll_riga, "cod_prodotto", "ZZZZZZZZZZZZZZA")
		else
			dw_report.setitem(ll_riga, "cod_prodotto", ds_righe.getitemstring(ll_y,"anag_prodotti_cod_ubicazione"))
		end if
		
		// nota di dettaglio
		if not isnull(ds_righe.getitemstring(ll_y,"det_ord_ven_nota_dettaglio")) and &
			len(trim(ds_righe.getitemstring(ll_y,"det_ord_ven_nota_dettaglio"))) > 0 then
			dw_report.setitem(ll_riga, "des_prodotto", ds_righe.getitemstring(ll_y,"det_ord_ven_des_prodotto") + &
									"~n" + ds_righe.getitemstring(ll_y,"det_ord_ven_nota_dettaglio") + "~r~n~r~n")
		else
			dw_report.setitem(ll_riga, "des_prodotto", ds_righe.getitemstring(ll_y,"det_ord_ven_des_prodotto") + "~r~n~r~n")
		end if
		
		// date varie
		dw_report.setitem(ll_riga, "data_registrazione", ds_righe.getitemdatetime(ll_y,"tes_ord_ven_data_registrazione"))
		dw_report.setitem(ll_riga, "data_consegna", ds_righe.getitemdatetime(ll_y,"tes_ord_ven_data_consegna"))
		
		if ll_y = ll_righe and not isnull(ds_righe.getitemstring(ll_y,"tes_ord_ven_nota_piede")) and len(ds_righe.getitemstring(ll_y,"tes_ord_ven_nota_piede")) > 0 then
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga, "cod_prodotto", "ZZZZZZZZZZZZZZZ")
			dw_report.setitem(ll_riga, "des_prodotto", ds_righe.getitemstring(ll_y,"tes_ord_ven_nota_piede"))
      end if
		
	next
	
	dw_report.setsort("cod_prodotto A, des_prodotto A")
	dw_report.sort()
	dw_report.print()
	dw_report.reset()
	
	update tes_ord_ven
	set    flag_stampato ='S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_reg_ord_ven and
			 num_registrazione = :ll_num_reg_ord_ven;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento flag stampato ordine " + string(ll_anno_reg_ord_ven) + &
					  "/" + string(ll_num_reg_ord_ven) + ": " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
	
	commit;
	
next

dw_report.setredraw(true)
end event

type dw_selezione from datawindow within w_report_lista_prelievo
integer y = 20
integer width = 2583
integer height = 540
integer taborder = 10
string dataobject = "d_selezione_lista_prelievo"
boolean border = false
boolean livescroll = true
end type

event itemchanged;resetupdate()
end event

event buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

