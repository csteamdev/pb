﻿$PBExportHeader$w_report_listino_ven.srw
$PBExportComments$finestra report listino vendita
forward
global type w_report_listino_ven from w_cs_xx_principale
end type
type cbx_1 from checkbox within w_report_listino_ven
end type
type st_1 from statictext within w_report_listino_ven
end type
type cb_chiudi from commandbutton within w_report_listino_ven
end type
type cb_cancella from commandbutton within w_report_listino_ven
end type
type cb_report from commandbutton within w_report_listino_ven
end type
type dw_report_listino_ven from uo_cs_xx_dw within w_report_listino_ven
end type
type cb_selezione from commandbutton within w_report_listino_ven
end type
type cb_stampa from commandbutton within w_report_listino_ven
end type
type cb_filtro_descrizione from commandbutton within w_report_listino_ven
end type
type cb_filtro_avanzato from commandbutton within w_report_listino_ven
end type
type cb_2 from commandbutton within w_report_listino_ven
end type
type cb_1 from commandbutton within w_report_listino_ven
end type
type st_2 from statictext within w_report_listino_ven
end type
type em_1 from editmask within w_report_listino_ven
end type
type st_3 from statictext within w_report_listino_ven
end type
type em_2 from editmask within w_report_listino_ven
end type
type dw_selezione_listino from uo_cs_xx_dw within w_report_listino_ven
end type
end forward

global type w_report_listino_ven from w_cs_xx_principale
integer x = 0
integer y = 0
integer width = 3534
integer height = 556
string title = "Listini di vendita"
boolean resizable = false
event ue_posiziona_w ( )
event ue_imposta_data ( )
cbx_1 cbx_1
st_1 st_1
cb_chiudi cb_chiudi
cb_cancella cb_cancella
cb_report cb_report
dw_report_listino_ven dw_report_listino_ven
cb_selezione cb_selezione
cb_stampa cb_stampa
cb_filtro_descrizione cb_filtro_descrizione
cb_filtro_avanzato cb_filtro_avanzato
cb_2 cb_2
cb_1 cb_1
st_2 st_2
em_1 em_1
st_3 st_3
em_2 em_2
dw_selezione_listino dw_selezione_listino
end type
global w_report_listino_ven w_report_listino_ven

forward prototypes
public function integer wf_report ()
end prototypes

event ue_posiziona_w;this.x = 0
this.y = 0
end event

event ue_imposta_data;dw_selezione_listino.setitem(1,"data_rif",today())

end event

public function integer wf_report ();datetime ldt_data
string ls_cod_azienda, ls_cod_cliente, ls_cod_tipo_listino_prodotto, ls_cod_valuta,&
       ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_cod_agente_1, ls_cod_misura_mag, &
		 ls_cod_agente_2, ls_sconto, ls_messaggio, ls_prodotto_da, ls_prodotto_a, ls_quan_str, &
		 ls_where, ls_sql, ls_header_prodotti, ls_header_cliente, ls_old_str, ls_new_str, ls_cod_chiave, ls_cod_valore
long ll_riga, ll_i, ll_start_pos
decimal ldd_quantita, ldd_prezzo, ldd_prezzo_mag, ldd_fat_conversione
boolean lb_no_sconti, lb_no_variazioni
uo_condizioni_cliente iuo_condizioni_cliente

dw_report_listino_ven.reset()
dw_report_listino_ven.change_dw_current()
dw_report_listino_ven.setfocus()

ls_where = ""
ls_sql = ""
ldd_quantita = dw_selezione_listino.getitemnumber(1,"quantita")
if ldd_quantita = 0 or isnull(ldd_quantita) then
	g_mb.messagebox("Dati incompleti","Impostare la quantità!")
	return -1	
elseif ldd_quantita < 0 then
	g_mb.messagebox("Dati non validi","La quantità non può essere negativa")
	return -1
end if	
ls_cod_valuta = dw_selezione_listino.getitemstring(1,"compute_2")
if ls_cod_valuta = "" or isnull(ls_cod_valuta) or ls_cod_valuta = "< CODICE INESISTENTE >" then
	g_mb.messagebox("Dati incompleti","Impostare la valuta!")
	return -1
end if	
ls_cod_tipo_listino_prodotto = dw_selezione_listino.getitemstring(1,"compute_3")
if ls_cod_tipo_listino_prodotto = "" or isnull(ls_cod_tipo_listino_prodotto) or ls_cod_tipo_listino_prodotto = "< CODICE INESISTENTE >" then
	g_mb.messagebox("Dati incompleti","Impostare il tipo di listino!")
	return -1
end if
setnull(ls_cod_agente_1)
setnull(ls_cod_agente_2)
ls_cod_azienda = s_cs_xx.cod_azienda
ldt_data = datetime(dw_selezione_listino.getitemdate(1,"data_rif"))
dw_report_listino_ven.object.t_10.text = string(date(ldt_data))
ls_cod_cliente = dw_selezione_listino.getitemstring(1,"cliente")
if ls_cod_cliente <> ""  and not isnull(ls_cod_cliente) and cbx_1.checked = false then
	ls_where = "and cod_cliente = '" + ls_cod_cliente + "' "
else
	ls_where = "and cod_cliente is null "	
end if

ls_header_cliente = dw_selezione_listino.getitemstring(1,"compute_5")

if ls_header_cliente = "" then
	ls_header_cliente = "non specificato"
elseif ls_header_cliente = "< CODICE INESISTENTE >" then
	ls_header_cliente = "inesistente per il codice specificato"
end if	

ls_prodotto_da = dw_selezione_listino.getitemstring(1,"prodotto_da")
ls_prodotto_a = dw_selezione_listino.getitemstring(1,"prodotto_a")
if ls_prodotto_da = "" or isnull(ls_prodotto_da) then
	if ls_prodotto_a = "" or isnull(ls_prodotto_a) then
		ls_header_prodotti = "tutti"
	else
		ls_where = ls_where + "and cod_prodotto <= '" + ls_prodotto_a +"'"
		ls_header_prodotti = "tutti fino a " + ls_prodotto_a
	end if
else
	if ls_prodotto_a = "" or isnull(ls_prodotto_a) then
		ls_where = ls_where + "and cod_prodotto >= '" + ls_prodotto_da +"'"
		ls_header_prodotti = "tutti a partire da " + ls_prodotto_da
	else
		ls_where = ls_where + "and cod_prodotto between '" + ls_prodotto_da +"' and '" + ls_prodotto_a + "'"
		ls_header_prodotti = "compresi fra " + ls_prodotto_da + " e " + ls_prodotto_a
	end if
end if	



dw_report_listino_ven.object.t_12.text = ls_header_prodotti

ls_cod_chiave = dw_selezione_listino.getitemstring(1,"cod_chiave")
if ls_cod_chiave <> "" and  not isnull(ls_cod_chiave) then
	ls_where = ls_where + "and cod_prodotto in ( select cod_prodotto from anag_prodotti_chiavi " +&
	"where cod_azienda = '" + ls_cod_azienda + "'  and cod_chiave = '"+ls_cod_chiave+"' "
end if
	

ls_cod_valore = dw_selezione_listino.getitemstring(1,"compute_6")

if ls_cod_valore <> "" and  not isnull(ls_cod_valore) then
	ls_where = ls_where + "and progressivo = " + string(ls_cod_valore) +")"
elseif  ls_cod_chiave <> "" and  not isnull(ls_cod_chiave) then
	ls_where = ls_where + ")"
end if


ll_start_pos = 1
ls_old_str = char(34)
ls_new_str = char(96)
ll_start_pos = Pos(ls_header_cliente, ls_old_str, ll_start_pos)
do while ll_start_pos > 0
	ls_header_cliente = Replace(ls_header_cliente, ll_start_pos, Len(ls_old_str), ls_new_str)
	ll_start_pos = Pos(ls_header_cliente, ls_old_str, ll_start_pos+Len(ls_new_str))
loop
dw_report_listino_ven.object.t_14.text = ls_header_cliente

ls_sql = "select distinct(cod_prodotto) from listini_vendite " +&
"where data_inizio_val <= '" + string(date(ldt_data),s_cs_xx.db_funzioni.formato_data) + "' and " +&
"cod_azienda = '" + ls_cod_azienda + "' and " +&
"cod_tipo_listino_prodotto = '" + ls_cod_tipo_listino_prodotto + "' and " +&
"cod_valuta = '" + ls_cod_valuta + "' " +&
ls_where +&
" order by cod_prodotto"

declare prodotti dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;
		
open dynamic prodotti;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore prodotti. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

this.title = "Listini di vendita - elaborazione in corso..."

do while true

	fetch prodotti into :ls_cod_prodotto;

	if sqlca.sqlcode = 100 then
		exit
	elseif sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore prodotti. Dettaglio = " + sqlca.sqlerrtext)
		return -1
	end if	
	
	if isnull(ls_cod_prodotto) then continue
	
	iuo_condizioni_cliente = create uo_condizioni_cliente
   iuo_condizioni_cliente.ib_setitem=false
   iuo_condizioni_cliente.ib_setitem_provvigioni=false
	iuo_condizioni_cliente.str_parametri.ldw_oggetto = dw_report_listino_ven
	iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
	iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
	iuo_condizioni_cliente.str_parametri.cambio_ven = 1
	iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data
	if isnull(ls_cod_cliente) or ls_cod_cliente = "" then
		ls_cod_cliente = "XXXXXX"
	end if	
	iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
	iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
	iuo_condizioni_cliente.str_parametri.dim_1 = 0
	iuo_condizioni_cliente.str_parametri.dim_2 = 0
	iuo_condizioni_cliente.str_parametri.quantita = ldd_quantita
	iuo_condizioni_cliente.str_parametri.valore = 0
	iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
	iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
	iuo_condizioni_cliente.wf_condizioni_cliente()

   lb_no_sconti = false
   lb_no_variazioni = false

   if upperbound(iuo_condizioni_cliente.str_output.variazioni) = 0 then
		lb_no_variazioni = true
	else
		ldd_prezzo=iuo_condizioni_cliente.str_output.variazioni[upperbound(iuo_condizioni_cliente.str_output.variazioni)]
	end if
	
	ls_sconto = ""
	
	if upperbound(iuo_condizioni_cliente.str_output.sconti) = 0 then
		lb_no_sconti = true
	else		
		for ll_i = 1 to upperbound(iuo_condizioni_cliente.str_output.sconti)
			if iuo_condizioni_cliente.str_output.sconti[ll_i] > 0 and not isnull(iuo_condizioni_cliente.str_output.sconti[ll_i]) then
				if ll_i > 1 then
					ls_sconto = ls_sconto + "+ "
				end if
				ls_sconto = ls_sconto + string(iuo_condizioni_cliente.str_output.sconti[ll_i]) + "% "
			end if	
		next
	end if	
	
	if lb_no_sconti = true and lb_no_variazioni = true then continue

	select des_prodotto, cod_misura_ven, fat_conversione_ven, cod_misura_mag
	into :ls_des_prodotto, :ls_cod_misura, :ldd_fat_conversione, :ls_cod_misura_mag
	from anag_prodotti
	where cod_azienda = :ls_cod_azienda and
			cod_prodotto = :ls_cod_prodotto;
		
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di anag_prodotti. Dettaglio = " + sqlca.sqlerrtext)
		return -1
	end if
	 
	 //prezzo in unità magazzino
	ldd_prezzo_mag = ldd_prezzo / ldd_fat_conversione
	
	if iuo_condizioni_cliente.uof_arrotonda_prezzo_decimal(ldd_prezzo_mag,ls_cod_valuta,ldd_prezzo_mag,ls_messaggio) = -1 then
		g_mb.messagebox("APICE","Errore in arrotondamento prezzo magazzino.~n" + ls_messaggio)
		return -1
	end if
	
   ll_riga = dw_report_listino_ven.insertrow(0)
	dw_report_listino_ven.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
	dw_report_listino_ven.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
	
	//dw_report_listino_ven.setitem(ll_riga, "unita_misura", ls_cod_misura)
	dw_report_listino_ven.setitem(ll_riga, "unita_misura", ls_cod_misura_mag + "/" + ls_cod_misura)
	
	dw_report_listino_ven.setitem(ll_riga, "quantita", ldd_quantita)
	
	ls_quan_str = string(ldd_quantita, "#####0.00") + "/" + string(ldd_quantita * ldd_fat_conversione, "#####0.00")
	dw_report_listino_ven.setitem(ll_riga, "qta_str", ls_quan_str)
	
	
	if ldd_prezzo <> 0 then
		//in UM mag
		dw_report_listino_ven.setitem(ll_riga, "prezzo", ldd_prezzo)
	end if
	
	//ui UM vendita
	dw_report_listino_ven.setitem(ll_riga, "prezzo_mag", ldd_prezzo_mag)
	dw_report_listino_ven.setitem(ll_riga, "sconto", ls_sconto)
	st_1.text = "elemento: " + ls_cod_prodotto + "   " + ls_des_prodotto

	destroy iuo_condizioni_cliente
	
loop	

close prodotti;	

this.title = "Listini di vendita"
this.postevent("ue_posiziona_w")

dw_selezione_listino.object.b_ricerca_prodotto_da.visible=false
dw_selezione_listino.object.b_ricerca_prodotto_a.visible=false
dw_selezione_listino.object.b_ricerca_cliente.visible=false
cb_report.hide()
cb_cancella.hide()
cb_chiudi.hide()
st_1.text = ""
st_1.hide()
dw_selezione_listino.hide()
dw_report_listino_ven.resetupdate()
dw_report_listino_ven.show()
cb_selezione.show()
cb_stampa.show()
cb_1.show()
cb_2.show()
cbx_1.hide()

cb_filtro_avanzato.show()
cb_filtro_descrizione.show()
st_2.show()
st_3.show()
em_1.text = ""
em_1.show()
em_2.text = ""
em_2.show()

dw_report_listino_ven.object.datawindow.print.preview = 'Yes'
dw_report_listino_ven.object.datawindow.print.preview.rulers = 'Yes'

this.width = 3529
this.height = 2000

return 0
end function

on w_report_listino_ven.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.st_1=create st_1
this.cb_chiudi=create cb_chiudi
this.cb_cancella=create cb_cancella
this.cb_report=create cb_report
this.dw_report_listino_ven=create dw_report_listino_ven
this.cb_selezione=create cb_selezione
this.cb_stampa=create cb_stampa
this.cb_filtro_descrizione=create cb_filtro_descrizione
this.cb_filtro_avanzato=create cb_filtro_avanzato
this.cb_2=create cb_2
this.cb_1=create cb_1
this.st_2=create st_2
this.em_1=create em_1
this.st_3=create st_3
this.em_2=create em_2
this.dw_selezione_listino=create dw_selezione_listino
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.cb_cancella
this.Control[iCurrent+5]=this.cb_report
this.Control[iCurrent+6]=this.dw_report_listino_ven
this.Control[iCurrent+7]=this.cb_selezione
this.Control[iCurrent+8]=this.cb_stampa
this.Control[iCurrent+9]=this.cb_filtro_descrizione
this.Control[iCurrent+10]=this.cb_filtro_avanzato
this.Control[iCurrent+11]=this.cb_2
this.Control[iCurrent+12]=this.cb_1
this.Control[iCurrent+13]=this.st_2
this.Control[iCurrent+14]=this.em_1
this.Control[iCurrent+15]=this.st_3
this.Control[iCurrent+16]=this.em_2
this.Control[iCurrent+17]=this.dw_selezione_listino
end on

on w_report_listino_ven.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.st_1)
destroy(this.cb_chiudi)
destroy(this.cb_cancella)
destroy(this.cb_report)
destroy(this.dw_report_listino_ven)
destroy(this.cb_selezione)
destroy(this.cb_stampa)
destroy(this.cb_filtro_descrizione)
destroy(this.cb_filtro_avanzato)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.st_2)
destroy(this.em_1)
destroy(this.st_3)
destroy(this.em_2)
destroy(this.dw_selezione_listino)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_report_listino_ven.ib_dw_report = true
dw_selezione_listino.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_report_listino_ven.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

dw_selezione_listino.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
													
select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_listino_ven.modify(ls_modify)

this.postevent("ue_imposta_data")

dw_selezione_listino.change_dw_current()
dw_selezione_listino.setfocus()
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione_listino, &
                 "des_chiave", &
                 sqlca, &
                 "tab_chiavi", &
                 "cod_chiave", &
                 "des_chiave", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW (dw_selezione_listino,"des_valuta",sqlca,&
                 "tab_valute","cod_valuta","des_valuta",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_selezione_listino,"des_listino",sqlca,&
                 "tab_tipi_listini_prodotti","cod_tipo_listino_prodotto","des_tipo_listino_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  

end event

type cbx_1 from checkbox within w_report_listino_ven
integer x = 1577
integer y = 376
integer width = 745
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Cliente come intestazione"
end type

type st_1 from statictext within w_report_listino_ven
integer x = 23
integer y = 376
integer width = 1531
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_chiudi from commandbutton within w_report_listino_ven
integer x = 2354
integer y = 376
integer width = 366
integer height = 80
integer taborder = 114
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&hiudi"
end type

event clicked;close(parent)
end event

type cb_cancella from commandbutton within w_report_listino_ven
integer x = 2743
integer y = 376
integer width = 366
integer height = 80
integer taborder = 104
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cancella"
end type

event clicked;dw_selezione_listino.setitem(1,"des_valuta","")
dw_selezione_listino.setitem(1,"des_listino","")
dw_selezione_listino.setitem(1,"prodotto_da","")
dw_selezione_listino.setitem(1,"prodotto_a","")
dw_selezione_listino.setitem(1,"cliente","")
dw_selezione_listino.setitem(1,"quantita",1)
dw_selezione_listino.setitem(1,"data_rif",today())
end event

type cb_report from commandbutton within w_report_listino_ven
integer x = 3131
integer y = 376
integer width = 366
integer height = 80
integer taborder = 94
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_selezione_listino.accepttext()
wf_report()
end event

type dw_report_listino_ven from uo_cs_xx_dw within w_report_listino_ven
boolean visible = false
integer x = 23
integer y = 220
integer width = 3474
integer height = 1680
integer taborder = 0
string dataobject = "d_report_listino_ven"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type cb_selezione from commandbutton within w_report_listino_ven
boolean visible = false
integer x = 3131
integer y = 20
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "S&elezione"
end type

event clicked;integer li_larghezza,li_altezza,li_x,li_y

li_larghezza = w_cs_xx_mdi.WorkSpaceWidth ( )
li_altezza = w_cs_xx_mdi.WorkSpaceheight ( )

cb_stampa.hide()

dw_report_listino_ven.hide()
cb_selezione.hide()
cb_1.hide()
cb_2.hide()
cb_filtro_avanzato.hide()
cb_filtro_descrizione.hide()
st_2.hide()
st_3.hide()
em_1.hide()
em_2.hide()

dw_selezione_listino.show()
cb_report.show()
cb_cancella.show()
cb_chiudi.show()
dw_selezione_listino.object.b_ricerca_prodotto_da.visible=true
dw_selezione_listino.object.b_ricerca_prodotto_a.visible=true
dw_selezione_listino.object.b_ricerca_cliente.visible=true
st_1.show()
cbx_1.show()


parent.width = 3529
parent.height = 552
li_x = (li_larghezza - parent.width)/2
li_y = (li_altezza - parent.height)/2

if li_x < 0 then li_x = 0
if li_y < 0 then li_y = 0

parent.move(li_x,li_y)

dw_selezione_listino.change_dw_current()
dw_selezione_listino.setfocus()
end event

type cb_stampa from commandbutton within w_report_listino_ven
boolean visible = false
integer x = 2747
integer y = 20
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;dw_report_listino_ven.print()
end event

type cb_filtro_descrizione from commandbutton within w_report_listino_ven
boolean visible = false
integer x = 2743
integer y = 120
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Filtra Desc."
end type

event clicked;string ls_des_inizio, ls_des_fine, ls_filtro
integer ll_ret

ls_des_inizio = em_1.text
ls_des_fine   = em_2.text

ls_des_inizio = ls_des_inizio+ fill(" ",40 - len(ls_des_inizio))
ls_des_fine = ls_des_fine + fill("Z",40 - len(ls_des_fine))

ls_filtro = "des_prodotto >= '" + ls_des_inizio + "!' and des_prodotto <= '" + ls_des_fine + "'"
ll_ret = dw_report_listino_ven.setfilter(ls_filtro)
if ll_ret = 1 then
	dw_report_listino_ven.filter()
else
	g_mb.messagebox("APICE","Si è verificato un errore durante l'impostazione del filtro di ricerca;.~r~nProvare a reimpostarlo")
end if



end event

type cb_filtro_avanzato from commandbutton within w_report_listino_ven
boolean visible = false
integer x = 3131
integer y = 120
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Filtro Avanz."
end type

event clicked;string ls_null

setnull(ls_null)

if dw_report_listino_ven.setfilter(ls_null) = 1 then
	dw_report_listino_ven.filter()
end if

end event

type cb_2 from commandbutton within w_report_listino_ven
boolean visible = false
integer x = 411
integer y = 20
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ord.Descr."
end type

event clicked;dw_report_listino_ven.setsort("des_prodotto")
dw_report_listino_ven.sort()
end event

type cb_1 from commandbutton within w_report_listino_ven
boolean visible = false
integer x = 23
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ord.Codice"
end type

event clicked;dw_report_listino_ven.setsort("cod_prodotto")
dw_report_listino_ven.sort()

end event

type st_2 from statictext within w_report_listino_ven
boolean visible = false
integer x = 23
integer y = 120
integer width = 416
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Da Descrizione:"
boolean focusrectangle = false
end type

type em_1 from editmask within w_report_listino_ven
boolean visible = false
integer x = 457
integer y = 120
integer width = 891
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type st_3 from statictext within w_report_listino_ven
boolean visible = false
integer x = 1394
integer y = 120
integer width = 393
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "A Descrizione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_2 from editmask within w_report_listino_ven
boolean visible = false
integer x = 1806
integer y = 120
integer width = 891
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type dw_selezione_listino from uo_cs_xx_dw within w_report_listino_ven
integer x = 23
integer y = 20
integer width = 3474
integer height = 352
integer taborder = 0
string dataobject = "d_selezione_listino"
end type

event itemchanged;call super::itemchanged;//long ll_null//, ll_i
string ls_cod_chiave, ls_null
dw_selezione_listino.accepttext()
setnull(ls_null)
//ll_i = dw_selezione_listino.getrow()
//if ll_i <= 0 then return -1
//
ls_cod_chiave = dw_selezione_listino.getitemstring(1,"cod_chiave")


choose case i_colname
	case "des_chiave"
		if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" then
			
			
			f_po_loaddddw_dw(dw_selezione_listino, &
								  "valore", &
								  sqlca, &
								  "tab_chiavi_valori", &
								  "progressivo", &
								  "valore", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_chiave = '" + ls_cod_chiave + "' ")		
			dw_selezione_listino.setitem(1,"valore", ls_null)
			dw_selezione_listino.setitem(1,"compute_6", ls_null)
			
					  
			//setitem(ll_i,"progressivo", ll_null)
			
		else
			
			f_po_loaddddw_dw(dw_selezione_listino, &
								  "valore", &
								  sqlca, &
								  "tab_chiavi_valori", &
								  "progressivo", &
								  "valore", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
								  
			//setitem(ll_i,"progressivo", ll_null)
			
		end if
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione_listino,"cliente")
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione_listino,"prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione_listino,"prodotto_a")
end choose
end event

