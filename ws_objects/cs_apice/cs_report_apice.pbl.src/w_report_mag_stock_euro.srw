﻿$PBExportHeader$w_report_mag_stock_euro.srw
$PBExportComments$Finestra Report Situazione Magazzino per Stock
forward
global type w_report_mag_stock_euro from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_mag_stock_euro
end type
type cb_report from commandbutton within w_report_mag_stock_euro
end type
type cb_selezione from commandbutton within w_report_mag_stock_euro
end type
type dw_report from uo_cs_xx_dw within w_report_mag_stock_euro
end type
type dw_selezione from uo_cs_xx_dw within w_report_mag_stock_euro
end type
end forward

global type w_report_mag_stock_euro from w_cs_xx_principale
integer width = 3557
integer height = 1680
string title = "Report Materiali Accettati"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
dw_selezione dw_selezione
end type
global w_report_mag_stock_euro w_report_mag_stock_euro

type variables
string is_original_select
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 641
this.y = 485
this.width = 2437
this.height = 1213

end event

on w_report_mag_stock_euro.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_selezione
end on

on w_report_mag_stock_euro.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.dw_selezione)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_prodotto", &
//							sqlca, &
//							"anag_prodotti", &
//							"cod_prodotto", &
//                     "des_prodotto", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
							
f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito_da", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito_a", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_cliente_da", &
//							sqlca, &
//							"anag_clienti", &
//							"cod_cliente", &
//                     "rag_soc_1", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_cliente_a", &
//							sqlca, &
//							"anag_clienti", &
//							"cod_cliente", &
//                     "rag_soc_1", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_fornitore_da", &
//							sqlca, &
//							"anag_fornitori", &
//							"cod_fornitore", &
//                     "rag_soc_1", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_fornitore_a", &
//							sqlca, &
//							"anag_fornitori", &
//							"cod_fornitore", &
//                     "rag_soc_1", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//
//
end event

event open;call super::open;is_original_select =  &
	dw_report.Describe("DataWindow.Table.Select")
cb_report.enabled=false	

end event

type cb_annulla from commandbutton within w_report_mag_stock_euro
integer x = 1623
integer y = 1000
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_mag_stock_euro
integer x = 2011
integer y = 1000
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto, ls_cod_deposito_da, ls_cod_deposito_a, ls_cod_ubicazione_da, ls_cod_ubicazione_a
string ls_cod_lotto_da, ls_cod_lotto_a, ls_cod_cliente_da, ls_cod_cliente_a
string ls_cod_fornitore_da, ls_cod_fornitore_a
string ls_where_clause, ls_mod_string, ls_rc
date   ld_data_stock_da, ld_data_stock_a

ls_where_clause=""
ls_cod_prodotto = dw_selezione.getitemstring(1,"cod_prodotto")
ls_cod_deposito_da = dw_selezione.getitemstring(1,"cod_deposito_da")
ls_cod_deposito_a = dw_selezione.getitemstring(1,"cod_deposito_a")
ls_cod_ubicazione_da = dw_selezione.getitemstring(1,"cod_ubicazione_da")
ls_cod_ubicazione_a = dw_selezione.getitemstring(1,"cod_ubicazione_a")
ls_cod_lotto_da = dw_selezione.getitemstring(1,"cod_lotto_da")
ls_cod_lotto_a = dw_selezione.getitemstring(1,"cod_lotto_a")
ls_cod_cliente_da = dw_selezione.getitemstring(1,"cod_cliente_da")
ls_cod_cliente_a = dw_selezione.getitemstring(1,"cod_cliente_a")
ls_cod_fornitore_da = dw_selezione.getitemstring(1,"cod_fornitore_da")
ls_cod_fornitore_a = dw_selezione.getitemstring(1,"cod_fornitore_a")

ld_data_stock_da = date(dw_selezione.getitemdatetime(1,"data_stock_da"))
ld_data_stock_a = date(dw_selezione.getitemdatetime(1,"data_stock_a"))

if isnull(ls_cod_deposito_da) then  ls_cod_deposito_da  = "0"
if isnull(ls_cod_deposito_a) then ls_cod_deposito_a = "zzz"
if isnull(ls_cod_ubicazione_da) then ls_cod_ubicazione_da = "0"
if isnull(ls_cod_ubicazione_a) then ls_cod_ubicazione_a = "zzz"
if isnull(ls_cod_lotto_da) then ls_cod_lotto_da = "0"
if isnull(ls_cod_lotto_a) then ls_cod_lotto_a = "zzz"

if isnull(ld_data_stock_da) then  ld_data_stock_da  = date("01/01/1900")
if isnull(ld_data_stock_a)  then  ld_data_stock_a   = date("31/12/2999")

if ( isnull(ls_cod_cliente_da)  and isnull(ls_cod_cliente_a) ) then
	ls_cod_cliente_da="0"
	ls_cod_cliente_a="zzzzzz"
	ls_where_clause = ls_where_clause + " and ( (stock.cod_cliente >= :rs_cliente_da ) AND (stock.cod_cliente <= :rs_cliente_a  ) or stock.cod_cliente is null)"
else
	if isnull(ls_cod_cliente_da) then ls_cod_cliente_da = "0" 
	if isnull(ls_cod_cliente_a) then ls_cod_cliente_a = "zzzzzz" 
	ls_where_clause=ls_where_clause + " and ( (stock.cod_cliente >= :rs_cliente_da ) AND (stock.cod_cliente <= :rs_cliente_a ) )" // non seleziona i null
end if

if ( isnull(ls_cod_fornitore_da)  and isnull(ls_cod_fornitore_a) ) then
	ls_cod_fornitore_da="0"
	ls_cod_fornitore_a="zzzzzz"
	ls_where_clause =ls_where_clause + " and ( (stock.cod_fornitore >= :rs_fornitore_da ) AND (stock.cod_fornitore <= :rs_fornitore_a) or stock.cod_fornitore is null)"
else
	if isnull(ls_cod_fornitore_da) then ls_cod_fornitore_da = "0"
	if isnull(ls_cod_fornitore_a) then ls_cod_fornitore_a = "zzzzzz"
	ls_where_clause=ls_where_clause + " and ( (stock.cod_fornitore >= :rs_fornitore_da ) AND (stock.cod_fornitore <= :rs_fornitore_a))" // non seleziona i null
end if

ls_mod_string = "DataWindow.Table.Select='"  &
	+ is_original_select + ls_where_clause + "'"


dw_selezione.hide()

parent.x = 20
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()

ls_rc = dw_report.Modify(ls_mod_string)
IF ls_rc = "" THEN
	dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ls_cod_deposito_da, ls_cod_deposito_a, &
					ls_cod_ubicazione_da, ls_cod_ubicazione_a,ls_cod_lotto_da, ls_cod_lotto_a, &
					ld_data_stock_da, ld_data_stock_a, ls_cod_cliente_da, ls_cod_cliente_a, &
					ls_cod_fornitore_da, ls_cod_fornitore_a)
ELSE
	g_mb.messagebox("Status", "Modify Failed" + ls_rc)
END IF



cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_mag_stock_euro
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()

parent.x = 641
parent.y = 485
parent.width = 2437
parent.height = 1213

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_mag_stock_euro
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 50
string dataobject = "d_report_mag_stock_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_selezione from uo_cs_xx_dw within w_report_mag_stock_euro
integer x = 23
integer y = 20
integer width = 2354
integer height = 960
integer taborder = 60
string dataobject = "d_selezione_report_mag_stock"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;string ls_nome_col, ls_cod_prec
if not isnull(data) then 
	ls_nome_col=dwo.Name
	cb_report.enabled=true
	CHOOSE CASE ls_nome_col
		CASE "cod_deposito_a"
			ls_cod_prec=dw_selezione.getitemstring(1,"cod_deposito_da")
			IF ls_cod_prec>data THEN
				g_mb.messagebox("Magazzino per Deposito", "Selezionare un magazzino in ordine sucessivo a " + ls_cod_prec)
				dw_selezione.SetItem ( 1, "cod_deposito_a", "" )
				RETURN 2
			END IF

	END CHOOSE
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione, "cod_prodotto")
	case "b_ricerca_cliente_da"
		guo_ricerca.uof_ricerca_cliente(dw_selezione, "cod_cliente_da")
	case "b_ricerca_cliente_a"
		guo_ricerca.uof_ricerca_cliente(dw_selezione, "cod_cliente_a")
	case "b_ricerca_fornitore_da"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione, "cod_fornitore_da")
	case "b_ricerca_fornitore_a"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione, "cod_fornitore_a")
end choose
end event

