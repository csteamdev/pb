﻿$PBExportHeader$w_report_matricole_ass_fasi.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_matricole_ass_fasi from w_cs_xx_principale
end type
type dw_fasi from uo_cs_xx_dw within w_report_matricole_ass_fasi
end type
end forward

global type w_report_matricole_ass_fasi from w_cs_xx_principale
integer width = 3461
integer height = 2268
string title = "Fasi di Lavorazione"
boolean resizable = false
dw_fasi dw_fasi
end type
global w_report_matricole_ass_fasi w_report_matricole_ass_fasi

forward prototypes
public function integer wf_fasi_lavorazione (long fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, decimal fdd_quantita_richiesta, ref string fs_errore)
end prototypes

public function integer wf_fasi_lavorazione (long fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, decimal fdd_quantita_richiesta, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2,ls_flag_materia_prima,ls_cod_prodotto_inserito,&
			 ls_cod_prodotto_variante,ls_flag_materia_prima_variante, ls_cod_versione_figlio, ls_cod_versione_variante, ls_cod_versione_inserito
long      ll_num_righe,ll_num_figli
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2,ldd_quan_prodotta, & 
			 ldd_quan_utilizzo_variante,ldd_tempo_attrezzaggio_prev,ldd_tempo_attrezzaggio_commessa_prev, & 
			 ldd_tempo_lavorazione_prev, ldd_tempo_risorsa_umana_prev,ldd_tempo_totale_prev,ldd_costo_prev, & 
			 ldd_quan_prodotta_fase, ldd_costo_totale_2_prev

datastore lds_righe_distinta

select quan_prodotta
into   :ldd_quan_prodotta
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :fi_anno_commessa and
		 num_commessa = :fl_num_commessa;

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve( s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	select cod_prodotto,
	       cod_versione_variante,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
	       :ls_cod_versione_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :fi_anno_commessa  and    
			 num_commessa = :fl_num_commessa	   and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :fs_cod_versione and
			 cod_versione_figlio = :ls_cod_versione_figlio;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante * fdd_quantita_richiesta
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	if ls_flag_materia_prima = 'N' then
		
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda  and 	 
		       cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		if sqlca.sqlcode = 100 then continue
	else
		continue
	end if

//*************************
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio and
			 cod_versione = :ls_cod_versione_figlio;
	
	if sqlca.sqlcode = 100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 cod_cat_attrezzature_2,
			 tempo_lavorazione,
			 tempo_attrezzaggio,
			 tempo_attrezzaggio_commessa,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_lavorazione_prev,
			 :ldd_tempo_attrezzaggio_prev,
			 :ldd_tempo_attrezzaggio_commessa_prev,
			 :ldd_tempo_risorsa_umana_prev
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  AND   
	      cod_prodotto = :ls_cod_prodotto_figlio and
			cod_versione = :ls_cod_versione_figlio;

	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature_2;

   dw_fasi.insertrow(1)
   dw_fasi.setitem( 1, "cod_prodotto", ls_cod_prodotto_figlio)
 	dw_fasi.setitem( 1, "des_prodotto", ls_des_prodotto)
 	dw_fasi.setitem( 1, "cod_lavorazione", ls_cod_lavorazione)
	dw_fasi.setitem( 1, "cod_cat_attrezzature", ls_cod_cat_attrezzature)
	dw_fasi.setitem( 1, "des_cat_attrezzature", ls_des_cat_attrezzature)
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda  and	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio and
			 cod_versione = :ls_cod_versione_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_fasi_lavorazione( fi_anno_commessa, fl_num_commessa, ls_cod_prodotto_figlio, ls_cod_versione_figlio, ldd_quan_utilizzo, ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_fasi.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_fasi.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw )								 


							 
iuo_dw_main = dw_fasi
end event

on w_report_matricole_ass_fasi.create
int iCurrent
call super::create
this.dw_fasi=create dw_fasi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fasi
end on

on w_report_matricole_ass_fasi.destroy
call super::destroy
destroy(this.dw_fasi)
end on

type dw_fasi from uo_cs_xx_dw within w_report_matricole_ass_fasi
integer x = 23
integer y = 20
integer width = 3406
integer height = 2140
integer taborder = 30
string dataobject = "d_report_matricole_assistenza_fasi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;//long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret
//
//ll_anno_registrazione = dw_report.getitemnumber( dw_report.getrow(), "anno_registrazione")
//ll_num_registrazione = dw_report.getitemnumber( dw_report.getrow(), "num_registrazione")
//ll_prog_riga_ord_ven = dw_report.getitemnumber( dw_report.getrow(), "prog_riga_ord_ven")
//
//ll_ret = Retrieve( s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
//
//IF ll_ret < 0 THEN
//	PCCA.Error = c_Fatal
//END IF	
//
end event

