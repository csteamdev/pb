﻿$PBExportHeader$w_report_matricole_ass_mat_prime.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_matricole_ass_mat_prime from w_cs_xx_principale
end type
type dw_mp from uo_cs_xx_dw within w_report_matricole_ass_mat_prime
end type
end forward

global type w_report_matricole_ass_mat_prime from w_cs_xx_principale
integer width = 2432
integer height = 2288
string title = "Elenco Materie Prime"
boolean resizable = false
dw_mp dw_mp
end type
global w_report_matricole_ass_mat_prime w_report_matricole_ass_mat_prime

forward prototypes
public function integer wf_materie_prime (long fl_anno_commessa, long fl_num_commessa)
end prototypes

public function integer wf_materie_prime (long fl_anno_commessa, long fl_num_commessa);string  ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione
double  ldd_quan_utilizzo[],ldd_quan_prodotta,ldd_quantita_totale,ldd_costo_materia_prima, &
		  ldd_costo_totale,ldd_costo_totale_mp, ldd_costo_totale_lav,ldd_costo_totale_ru, &
		  ldd_quan_utilizzata,ldd_quan_scarto,ldd_costo_utilizzo,ldd_costo_scarto,ldd_quan_spedita,ldd_costo_preventivo, & 
		  ldd_costo_totale_preventivo,ldd_costo_totale_lav_prev,ldd_costo_totale_ru_prev
long    ll_t,ll_num_commessa, ll_ret


select cod_prodotto,
       cod_versione,
		 quan_prodotta
into   :ls_cod_prodotto,
       :ls_cod_versione,
		 :ldd_quan_prodotta
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :fl_anno_commessa and
		 num_commessa = :fl_num_commessa;

ll_ret = f_trova_mat_prima(ls_cod_prodotto, ls_cod_versione, ls_mat_prima[], ldd_quan_utilizzo[], ldd_quan_prodotta, fl_anno_commessa, fl_num_commessa)

for ll_t = 1 to upperbound(ls_mat_prima)

	select sum(quan_utilizzata),
			 sum(quan_scarto)
	into   :ldd_quan_utilizzata,
			 :ldd_quan_scarto
	from   mat_prime_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and		
	       anno_commessa = :fl_anno_commessa 	and    
			 num_commessa = :fl_num_commessa	   and    
			 cod_prodotto = :ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "Sep","Errore nel DB:"+ sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	if isnull(ldd_quan_utilizzata) then ldd_quan_utilizzata = 0
	if isnull(ldd_quan_scarto) then ldd_quan_scarto = 0
	
	select sum(quan_spedita)
	into   :ldd_quan_spedita
	from   prod_bolle_out_com
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
	       anno_commessa = :fl_anno_commessa 	   and    
			 num_commessa = :fl_num_commessa	      and    
			 cod_prodotto_spedito = :ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore nel DB"+ sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	if isnull(ldd_quan_spedita) then 
		ldd_quan_spedita = 0
	else
		ldd_quan_utilizzata = ldd_quan_spedita
	end if
	
	ldd_quantita_totale = ldd_quan_utilizzata + ldd_quan_scarto
	
	select des_prodotto
	into	 :ls_des_prodotto
	from 	 anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_mat_prima[ll_t];

	dw_mp.insertrow(1)
	dw_mp.setitem( 1, "cod_prodotto", ls_mat_prima[ll_t])
	dw_mp.setitem( 1, "des_prodotto", ls_des_prodotto)
	dw_mp.setitem( 1, "quan_utilizzo", ldd_quan_utilizzata)  

next

//li_risposta=wf_fasi_lavorazione(li_anno_commessa,ll_num_commessa,ls_cod_prodotto,ls_cod_versione,1,ls_errore)

return 0

end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_mp.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_mp.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw )								 


							 
iuo_dw_main = dw_mp
end event

on w_report_matricole_ass_mat_prime.create
int iCurrent
call super::create
this.dw_mp=create dw_mp
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_mp
end on

on w_report_matricole_ass_mat_prime.destroy
call super::destroy
destroy(this.dw_mp)
end on

type dw_mp from uo_cs_xx_dw within w_report_matricole_ass_mat_prime
integer x = 23
integer y = 40
integer width = 2377
integer height = 2140
integer taborder = 30
string dataobject = "d_report_matricole_assistenza_mp"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;//long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret
//
//ll_anno_registrazione = dw_report.getitemnumber( dw_report.getrow(), "anno_registrazione")
//ll_num_registrazione = dw_report.getitemnumber( dw_report.getrow(), "num_registrazione")
//ll_prog_riga_ord_ven = dw_report.getitemnumber( dw_report.getrow(), "prog_riga_ord_ven")
//
//ll_ret = Retrieve( s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
//
//IF ll_ret < 0 THEN
//	PCCA.Error = c_Fatal
//END IF	
//
end event

