﻿$PBExportHeader$w_report_matricole_assistenza.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_matricole_assistenza from w_cs_xx_principale
end type
type dw_bolle from uo_cs_xx_dw within w_report_matricole_assistenza
end type
type cb_1 from commandbutton within w_report_matricole_assistenza
end type
type cb_report from commandbutton within w_report_matricole_assistenza
end type
type dw_selezione from uo_cs_xx_dw within w_report_matricole_assistenza
end type
type dw_ordine from uo_cs_xx_dw within w_report_matricole_assistenza
end type
type dw_report from uo_cs_xx_dw within w_report_matricole_assistenza
end type
type dw_folder from u_folder within w_report_matricole_assistenza
end type
type dw_commessa from uo_cs_xx_dw within w_report_matricole_assistenza
end type
end forward

global type w_report_matricole_assistenza from w_cs_xx_principale
integer width = 3781
integer height = 2288
string title = "Controllo Matricole Assistenza"
boolean resizable = false
dw_bolle dw_bolle
cb_1 cb_1
cb_report cb_report
dw_selezione dw_selezione
dw_ordine dw_ordine
dw_report dw_report
dw_folder dw_folder
dw_commessa dw_commessa
end type
global w_report_matricole_assistenza w_report_matricole_assistenza

type variables
long il_anno_commessa, il_num_commessa
end variables

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_commessa.ib_dw_report = true
dw_ordine.ib_dw_report = true
dw_bolle.ib_dw_report = true
dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_commessa.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw )

dw_ordine.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw )
								 
dw_bolle.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw )								 


dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report



l_objects[1] = dw_commessa
dw_folder.fu_assigntab(3, "Commessa", l_objects[])

l_objects[1] = dw_report
dw_folder.fu_assigntab(1, "Risultati", l_objects[])

l_objects[1] = dw_ordine
l_objects[2] = dw_bolle
dw_folder.fu_assigntab(2, "Ordine", l_objects[])

dw_folder.fu_foldercreate(3,3)
dw_folder.fu_selecttab(1)

end event

on w_report_matricole_assistenza.create
int iCurrent
call super::create
this.dw_bolle=create dw_bolle
this.cb_1=create cb_1
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_ordine=create dw_ordine
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_commessa=create dw_commessa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_bolle
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_ordine
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_commessa
end on

on w_report_matricole_assistenza.destroy
call super::destroy
destroy(this.dw_bolle)
destroy(this.cb_1)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_ordine)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_commessa)
end on

type dw_bolle from uo_cs_xx_dw within w_report_matricole_assistenza
integer x = 594
integer y = 1480
integer width = 2537
integer height = 560
integer taborder = 30
string dataobject = "d_report_matricole_assistenza_bolla"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret

ll_anno_registrazione = dw_report.getitemnumber( dw_report.getrow(), "anno_registrazione")
ll_num_registrazione = dw_report.getitemnumber( dw_report.getrow(), "num_registrazione")
ll_prog_riga_ord_ven = dw_report.getitemnumber( dw_report.getrow(), "prog_riga_ord_ven")

ll_ret = Retrieve( s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)

IF ll_ret < 0 THEN
	PCCA.Error = c_Fatal
END IF	

end event

type cb_1 from commandbutton within w_report_matricole_assistenza
integer x = 3291
integer y = 440
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string   ls_null
datetime ldt_null

setnull(ls_null)

dw_selezione.setitem( 1, "matricola", ls_null)
dw_selezione.setitem( 1, "cod_cliente", ls_null)
dw_selezione.setitem( 1, "cod_prodotto", ls_null)
dw_selezione.setitem( 1, "data_produzione_da", ldt_null)
dw_selezione.setitem( 1, "data_produzione_a", ldt_null)
end event

type cb_report from commandbutton within w_report_matricole_assistenza
integer x = 2903
integer y = 440
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;string   ls_matricola, ls_cod_cliente, ls_cod_prodotto, ls_sql, ls_prima, ls_seconda, ls_where, ls_new
datetime ldt_data_da, ldt_data_a
boolean  lb_matricola
long     ll_index, ll_ret

dw_selezione.accepttext()

ls_matricola = dw_selezione.getitemstring( 1, "matricola")
ls_cod_cliente = dw_selezione.getitemstring( 1, "cod_cliente")
ls_cod_prodotto = dw_selezione.getitemstring( 1, "cod_prodotto")
ldt_data_da = dw_selezione.getitemdatetime( 1, "data_produzione_da")
ldt_data_a = dw_selezione.getitemdatetime( 1, "data_produzione_a")

lb_matricola = false

ls_where = " WHERE tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			
if not isnull(ls_matricola) and ls_matricola <> "" then
	ls_prima = mid( ls_matricola, 1, 1)
	ls_seconda = mid( ls_matricola, 2, 1)
	
	choose case ls_prima
		case "A"
			ls_prima = "1"
		case "B"
			ls_prima = "2"
		case "C"
			ls_prima = "3"
		case "D"
			ls_prima = "4"
		case "E"
			ls_prima = "5"
		case "F"
			ls_prima = "6"
		case "G"
			ls_prima = "7"
		case "H"
			ls_prima = "8"
		case "I"
			ls_prima = "9"
	end choose
	
	choose case ls_seconda
		case "A"
			ls_seconda = "1"
		case "B"
			ls_seconda = "2"
		case "C"
			ls_seconda = "3"
		case "D"
			ls_seconda = "4"
		case "E"
			ls_seconda = "5"
		case "F"
			ls_seconda = "6"
		case "G"
			ls_seconda = "7"
		case "H"
			ls_seconda = "8"
		case "I"
			ls_seconda = "9"
	end choose	
	
	ls_matricola = ls_prima + ls_seconda + mid(ls_matricola, 3)
	
	ls_where += " AND det_ord_ven.num_commessa = " + ls_matricola

	lb_matricola = true

end if

if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	
	ls_where += " AND tes_ord_ven.cod_cliente = '" + ls_cod_cliente + "' "
	
end if

if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
	
	ls_where += " AND det_ord_ven.cod_prodotto = '" + ls_cod_prodotto + "' "
	
end if

if not isnull(ldt_data_da) then
	ls_where += " AND anag_commesse.data_produzione >= '" + string( ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_a) then
	ls_where += " AND anag_commesse.data_produzione <= '" + string( ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' "
end if


ls_where += " ORDER BY det_ord_ven.anno_registrazione ASC, det_ord_ven.num_registrazione ASC, det_ord_ven.prog_riga_ord_ven ASC "

ls_sql = upper(dw_report.GETSQLSELECT())

ll_index = pos( ls_sql, "WHERE")
if ll_index = 0 then
	ls_new = dw_report.GETSQLSELECT() + ls_where
else	
	ls_new = left( ls_sql, ll_index - 1) + ls_where
end if

ll_ret = dw_report.SetSQLSelect(ls_new)

if ll_ret < 0 then 
	g_mb.messagebox("OMNIA","Errore nel filtro di selezione")
	pcca.error = c_fatal
else
	
	dw_selezione.resetupdate()
	
	iuo_dw_main = dw_report
	dw_report.change_dw_current()

	parent.postevent("pc_retrieve")
	
end if






//string ls_da_attrezzatura, ls_a_attrezzatura, ls_cat_attrezzature, ls_cod_divisione, ls_cod_area_aziendale_s, ls_cod_reparto_s, &
//		 ls_cod_azienda, ls_cod_attrezzatura, ls_descrizione, ls_fabbricante, ls_modello, ls_num_matricola, ls_unita_tempo, &
//		 ls_utilizzo_scadenza, ls_cod_inventario, ls_cod_utente, ls_reperibilita, ls_cod_area_aziendale, &
//		 ls_cod_reparto, ls_cod_area_aziendale_r[], ls_des_reparto, ls_des_area, ls_rag_soc_1, ls_rag_soc_2, ls_null, &
//		 ls_cod_cat_attrezzatura, ls_des_cat_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_periodicita, &
//		 ls_modalita_esecuzione
//datetime ldt_data_acquisto
//decimal ld_periodicita_revisione
//long ll_i, ll_riga, ll_cont, ll_frequenza
//
//dw_report.setredraw(false)
//dw_selezione.accepttext()
//
//declare cu_tipi_manutenzioni cursor for
//	select cod_tipo_manutenzione,
//	       des_tipo_manutenzione,
//	       periodicita,
//			 frequenza,
//			 modalita_esecuzione
//	from   tab_tipi_manutenzione
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_attrezzatura = :ls_cod_attrezzatura and
//			 flag_blocco = 'N';
//
//
//declare cu_aree cursor for
// select cod_area_aziendale
//	from tab_aree_aziendali
//  where cod_azienda = :s_cs_xx.cod_azienda
//	 and (cod_divisione like :ls_cod_divisione or cod_divisione is null);
//
//declare cu_cursore cursor for
//select cod_azienda,
//		 cod_attrezzatura,
//		 descrizione,
//		 fabbricante,
//		 modello,
//		 num_matricola,
//		 data_acquisto,
//		 periodicita_revisione,
//		 unita_tempo,
//		 utilizzo_scadenza,
//		 cod_reparto,
//		 cod_inventario,
//		 cod_utente,
//		 reperibilita,
//		 cod_area_aziendale,
//		 cod_reparto,
//		 cod_cat_attrezzature
//  from anag_attrezzature
// where cod_azienda = :s_cs_xx.cod_azienda
//	and cod_attrezzatura >= :ls_da_attrezzatura
//	and cod_attrezzatura <= :ls_a_attrezzatura
//	and (cod_cat_attrezzature like :ls_cat_attrezzature or cod_cat_attrezzature is null)
//	and (cod_area_aziendale like :ls_cod_area_aziendale_r[ll_i] or cod_area_aziendale is null)
//	and (cod_reparto like :ls_cod_reparto_s or cod_reparto is null);	 
//	
//setnull(ls_null)
//dw_report.reset()
//
//ls_da_attrezzatura = dw_selezione.getitemstring(1,"rs_da_cod_attrezzatura")
//ls_a_attrezzatura = dw_selezione.getitemstring(1,"rs_a_cod_attrezzatura")
//ls_cat_attrezzature = dw_selezione.getitemstring(1,"rs_cod_cat_attrezzatura")
//ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")
//ls_cod_area_aziendale_s = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
//ls_cod_reparto_s = dw_selezione.getitemstring(1,"rs_cod_reparto")
//
//if isnull(ls_da_attrezzatura) then ls_da_attrezzatura = "!"
//if isnull(ls_a_attrezzatura) then  ls_a_attrezzatura  = "ZZZZZZ"
//if isnull(ls_cat_attrezzature) then  ls_cat_attrezzature  = "%"
//if isnull(ls_cod_divisione) then  ls_cod_divisione  = "%"
//if isnull(ls_cod_area_aziendale_s) then  ls_cod_area_aziendale_s  = "%"
//if isnull(ls_cod_reparto_s) then  ls_cod_reparto_s  = "%"
//
//ll_i = 1
//ls_cod_area_aziendale_r[1] = ls_null
////st_1.text = "Preparazione dei dati in corso ..."
//if ls_cod_area_aziendale_s = "%" and ls_cod_divisione <> "%" then
//	
//	open cu_aree;
//	if sqlca.sqlcode <> 0 then
//		messagebox("Omnia", "Errore in apertura cursore cu_aree " + sqlca.sqlerrtext)
//		return
//	end if
//	
//	do while 1 = 1
//		fetch cu_aree into :ls_cod_area_aziendale_r[ll_i];
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia", "Errore in lettura dati da tabella tab_aree_aziendali " + sqlca.sqlerrtext)
//			return
//		end if		
//		if sqlca.sqlcode = 100 then exit
//		
//		ll_i ++
//	loop
//	close cu_aree;
//end if	
//
//if ls_cod_area_aziendale_s = "%" and ls_cod_divisione = "%" then ls_cod_area_aziendale_r[1] = "%"
//if ls_cod_area_aziendale_s <> "%" and ls_cod_divisione <> "%" then ls_cod_area_aziendale_r[1] = ls_cod_area_aziendale_s
//if ls_cod_area_aziendale_s <> "%" and ls_cod_divisione = "%" then ls_cod_area_aziendale_r[1] = ls_cod_area_aziendale_s
//
//for ll_i = 1 to UpperBound(ls_cod_area_aziendale_r)
//
//	open cu_cursore;
//	if sqlca.sqlcode <> 0 then
//		messagebox("Omnia", "Errore in apertura cursore cu_cursore " + sqlca.sqlerrtext)
//		return
//	end if	
//	
//	do while 1 = 1
//		fetch cu_cursore into :ls_cod_azienda,
//									 :ls_cod_attrezzatura,
//									 :ls_descrizione,
//									 :ls_fabbricante,
//									 :ls_modello,
//									 :ls_num_matricola,
//									 :ldt_data_acquisto,
//									 :ld_periodicita_revisione,
//									 :ls_unita_tempo,
//									 :ls_utilizzo_scadenza,
//									 :ls_cod_reparto,
//									 :ls_cod_inventario,
//									 :ls_cod_utente,
//									 :ls_reperibilita,
//									 :ls_cod_area_aziendale,
//									 :ls_cod_reparto,
//									 :ls_cod_cat_attrezzatura;
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia", "Errore in lettura tabella anag_attrezzature " + sqlca.sqlerrtext)
//			return
//		end if									 
//
//		if sqlca.sqlcode = 100 then exit		
//		ll_riga = dw_report.insertrow(0)
//		dw_report.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
//		dw_report.setitem(ll_riga, "descrizione", ls_descrizione)
//		dw_report.setitem(ll_riga, "fabbricante", ls_fabbricante)		
//		dw_report.setitem(ll_riga, "modello", ls_modello)
//		dw_report.setitem(ll_riga, "num_matricola", ls_num_matricola)		
//		dw_report.setitem(ll_riga, "data_acquisto", ldt_data_acquisto)
//		dw_report.setitem(ll_riga, "periodicita_revisione", ld_periodicita_revisione)				
//		dw_report.setitem(ll_riga, "unita_tempo", ls_unita_tempo)
//		dw_report.setitem(ll_riga, "utilizzo_scadenza", ls_utilizzo_scadenza)		
//		dw_report.setitem(ll_riga, "cod_reparto", ls_cod_reparto)
//		dw_report.setitem(ll_riga, "cod_inventario", ls_cod_inventario)						
//		dw_report.setitem(ll_riga, "cod_utente", ls_cod_utente)
//		dw_report.setitem(ll_riga, "reperibilita", ls_reperibilita)				
//		dw_report.setitem(ll_riga, "cod_area_aziendale", ls_cod_area_aziendale)
//		dw_report.setitem(ll_riga, "cod_cat_attrezzature", ls_cod_cat_attrezzatura)
//		
//		select des_reparto
//		  into :ls_des_reparto
//		  from anag_reparti
//		 where cod_azienda = :s_cs_xx.cod_azienda
//			and cod_reparto = :ls_cod_reparto;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia", "Errore in lettura tabella anag_reparti " + sqlca.sqlerrtext)
//			return
//		end if									 		
//		
//		dw_report.setitem(ll_riga, "anag_reparti_des_reparto", ls_des_reparto)	
//
//		select des_area
//		  into :ls_des_area
//		  from tab_aree_aziendali
//		 where cod_azienda = :s_cs_xx.cod_azienda
//			and cod_area_aziendale = :ls_cod_area_aziendale;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia", "Errore in lettura tabella tab_aree_aziendali " + sqlca.sqlerrtext)
//			return
//		end if									 				
//		
//		dw_report.setitem(ll_riga, "tab_aree_aziendali_des_area", ls_des_area)
//		
//		select des_cat_attrezzature
//		  into :ls_des_cat_attrezzatura
//		  from tab_cat_attrezzature
//		 where cod_azienda = :s_cs_xx.cod_azienda
//			and cod_cat_attrezzature = :ls_cod_cat_attrezzatura;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia", "Errore in lettura tabella tab_cat_attrezzature " + sqlca.sqlerrtext)
//			return
//		end if									 				
//		
//		dw_report.setitem(ll_riga, "des_cat_attrezzature", ls_des_cat_attrezzatura)
//		
//		select rag_soc_1,
//				 rag_soc_2
//		  into :ls_rag_soc_1,
//				 :ls_rag_soc_2
//		  from aziende
//		 where cod_azienda = :s_cs_xx.cod_azienda;
//		 
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia", "Errore in lettura tabella aziende " + sqlca.sqlerrtext)
//			return
//		end if									 						 
//		
//		dw_report.setitem(ll_riga, "aziende_rag_soc_1", ls_rag_soc_1)		
//		dw_report.setitem(ll_riga, "aziende_rag_soc_2", ls_rag_soc_2)				
//
//		open cu_tipi_manutenzioni;
//		if sqlca.sqlcode <> 0 then
//			messagebox("Omnia", "Errore in apertura del cursore cu_tipi_manutenzioni " + sqlca.sqlerrtext)
//			rollback;
//			return
//		end if	
//		ll_cont = 1
//		do while true	
//			fetch cu_tipi_manutenzioni into :ls_cod_tipo_manutenzione,:ls_des_tipo_manutenzione, :ls_periodicita, :ll_frequenza, :ls_modalita_esecuzione;
//			if sqlca.sqlcode = 100 then exit
//			if sqlca.sqlcode < 0 then
//				messagebox("Omnia", "Errore in fecth cursore cu_tipi_manutenzioni " + sqlca.sqlerrtext)
//				rollback;
//				return
//			end if	
////			st_1.text = ls_cod_attrezzatura + "  " + ls_descrizione + "/ " + ls_cod_tipo_manutenzione
//			if ll_cont > 1 then
//				ll_riga = dw_report.insertrow(0)
//				dw_report.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
//				dw_report.setitem(ll_riga, "descrizione", ls_descrizione)
//				dw_report.setitem(ll_riga, "fabbricante", ls_fabbricante)		
//				dw_report.setitem(ll_riga, "modello", ls_modello)
//				dw_report.setitem(ll_riga, "num_matricola", ls_num_matricola)		
//				dw_report.setitem(ll_riga, "data_acquisto", ldt_data_acquisto)
//				dw_report.setitem(ll_riga, "periodicita_revisione", ld_periodicita_revisione)				
//				dw_report.setitem(ll_riga, "unita_tempo", ls_unita_tempo)
//				dw_report.setitem(ll_riga, "utilizzo_scadenza", ls_utilizzo_scadenza)		
//				dw_report.setitem(ll_riga, "cod_reparto", ls_cod_reparto)
//				dw_report.setitem(ll_riga, "cod_inventario", ls_cod_inventario)						
//				dw_report.setitem(ll_riga, "cod_utente", ls_cod_utente)
//				dw_report.setitem(ll_riga, "reperibilita", ls_reperibilita)				
//				dw_report.setitem(ll_riga, "cod_area_aziendale", ls_cod_area_aziendale)
//				dw_report.setitem(ll_riga, "cod_cat_attrezzature", ls_cod_cat_attrezzatura)
//				dw_report.setitem(ll_riga, "anag_reparti_des_reparto", ls_des_reparto)	
//				dw_report.setitem(ll_riga, "tab_aree_aziendali_des_area", ls_des_area)
//				dw_report.setitem(ll_riga, "des_cat_attrezzature", ls_des_cat_attrezzatura)
//				dw_report.setitem(ll_riga, "aziende_rag_soc_1", ls_rag_soc_1)		
//				dw_report.setitem(ll_riga, "aziende_rag_soc_2", ls_rag_soc_2)				
//			end if
//			dw_report.setitem(ll_riga, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
//			dw_report.setitem(ll_riga, "des_tipo_manutenzione", ls_des_tipo_manutenzione)
//			dw_report.setitem(ll_riga, "periodicita", ls_periodicita)
//			dw_report.setitem(ll_riga, "frequenza", ll_frequenza)
//			dw_report.setitem(ll_riga, "modalita_esecuzione", ls_modalita_esecuzione + "~r~n-")
//				
//			ll_cont ++
//		loop
//		close cu_tipi_manutenzioni;
//	loop
//	close cu_cursore;
//next	
//
//dw_report.setredraw(true)
//
//dw_report.groupcalc()
//dw_report.Object.DataWindow.Print.Preview = 'Yes'
//dw_folder.fu_selecttab(2)
////st_1.text = ""
//
//dw_report.change_dw_current()
//
//rollback;
end event

type dw_selezione from uo_cs_xx_dw within w_report_matricole_assistenza
integer x = 23
integer y = 20
integer width = 3680
integer height = 540
integer taborder = 20
string dataobject = "d_sel_report_matricole_assistenza"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_ordine from uo_cs_xx_dw within w_report_matricole_assistenza
integer x = 571
integer y = 740
integer width = 2674
integer height = 740
integer taborder = 20
string dataobject = "d_report_matricole_assistenza_ordine"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret

ll_anno_registrazione = dw_report.getitemnumber( dw_report.getrow(), "anno_registrazione")
ll_num_registrazione = dw_report.getitemnumber( dw_report.getrow(), "num_registrazione")
ll_prog_riga_ord_ven = dw_report.getitemnumber( dw_report.getrow(), "prog_riga_ord_ven")

ll_ret = Retrieve( s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)

IF ll_ret < 0 THEN
	PCCA.Error = c_Fatal
END IF	

end event

type dw_report from uo_cs_xx_dw within w_report_matricole_assistenza
integer x = 69
integer y = 740
integer width = 3589
integer height = 1380
integer taborder = 10
string dataobject = "d_report_matricole_assistenza_grid"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_ret

ll_ret = Retrieve( )
IF ll_ret < 0 THEN
	PCCA.Error = c_Fatal
END IF	
end event

event rowfocuschanged;call super::rowfocuschanged;if currentrow < 1 or isnull(currentrow) then return

//iuo_dw_main = dw_ordine
dw_ordine.Change_DW_Current( )
dw_ordine.triggerevent("pcd_retrieve")

dw_bolle.Change_DW_Current( )
dw_bolle.triggerevent("pcd_retrieve")

dw_commessa.Change_DW_Current( )
dw_commessa.triggerevent("pcd_retrieve")

end event

type dw_folder from u_folder within w_report_matricole_assistenza
integer x = 46
integer y = 600
integer width = 3639
integer height = 1552
integer taborder = 40
boolean border = false
end type

type dw_commessa from uo_cs_xx_dw within w_report_matricole_assistenza
integer x = 526
integer y = 900
integer width = 2720
integer height = 1060
integer taborder = 30
string dataobject = "d_report_matricole_assistenza_commessa"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret, ll_anno_commessa, ll_num_commessa

ll_anno_registrazione = dw_report.getitemnumber( dw_report.getrow(), "anno_registrazione")
ll_num_registrazione = dw_report.getitemnumber( dw_report.getrow(), "num_registrazione")
ll_prog_riga_ord_ven = dw_report.getitemnumber( dw_report.getrow(), "prog_riga_ord_ven")

select anno_commessa,
       num_commessa
into   :ll_anno_commessa,
       :ll_num_commessa
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione and
		 prog_riga_ord_ven = :ll_prog_riga_ord_ven;

ll_ret = Retrieve( s_cs_xx.cod_azienda, ll_anno_commessa, ll_num_commessa)

IF ll_ret < 0 THEN
	PCCA.Error = c_Fatal
END IF	

if ll_ret <= 0 then
	this.Object.b_distinta.Enabled = false
	il_anno_commessa = 0
	il_num_commessa = 0
else
	this.Object.b_distinta.Enabled = true
	il_anno_commessa = ll_anno_commessa
	il_num_commessa = ll_num_commessa
end if
	

end event

event clicked;call super::clicked;string ls_appo, ls_cod_prodotto, ls_cod_versione, ls_errore
dec{4} ld_quan_prodotta

choose case dwo.name
		
	case "b_distinta"
		
		s_cs_xx.parametri.parametro_dw_1 = dw_commessa
		window_open(w_varianti_commesse,-1)		
		
	case "b_mat_prime"
		
		if il_anno_commessa > 0 and not isnull(il_anno_commessa) and il_num_commessa > 0 and not isnull(il_num_commessa) then
			if not isvalid(w_report_matricole_ass_mat_prime) then		
				window_open(w_report_matricole_ass_mat_prime, -1)
			end if
			
			w_report_matricole_ass_mat_prime.wf_materie_prime( il_anno_commessa, il_num_commessa)
		end if
		
	case "b_fasi"
		
		if il_anno_commessa > 0 and not isnull(il_anno_commessa) and il_num_commessa > 0 and not isnull(il_num_commessa) then
			
			if not isvalid(w_report_matricole_ass_fasi) then		
				window_open(w_report_matricole_ass_fasi, -1)
			end if
			
			select cod_prodotto,
			       cod_versione,
					 quan_prodotta
			into   :ls_cod_prodotto,
			       :ls_cod_versione,
					 :ld_quan_prodotta
			from   anag_commesse
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_commessa = :il_anno_commessa and
					 num_commessa = :il_num_commessa;
			
			w_report_matricole_ass_fasi.wf_fasi_lavorazione( il_anno_commessa, il_num_commessa, ls_cod_prodotto, ls_cod_versione, ld_quan_prodotta, ls_errore)

		end if		
		
end choose
end event

