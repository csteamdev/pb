﻿$PBExportHeader$w_report_movimenti.srw
$PBExportComments$Report movimenti di magazzino (dettaglio)
forward
global type w_report_movimenti from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_report_movimenti
end type
type st_log from statictext within w_report_movimenti
end type
type cb_annulla from commandbutton within w_report_movimenti
end type
type cb_report from commandbutton within w_report_movimenti
end type
type dw_selezione from uo_cs_xx_dw within w_report_movimenti
end type
type dw_report from uo_cs_xx_dw within w_report_movimenti
end type
type dw_folder from u_folder within w_report_movimenti
end type
end forward

global type w_report_movimenti from w_cs_xx_principale
integer width = 3826
integer height = 1896
string title = "Report Movimenti Magazzino"
boolean resizable = false
hpb_1 hpb_1
st_log st_log
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_movimenti w_report_movimenti

type variables
string is_sql_base = ""
end variables

forward prototypes
public function integer wf_report (string fs_sql, string fs_sql_count, ref string fs_errore)
end prototypes

public function integer wf_report (string fs_sql, string fs_sql_count, ref string fs_errore);integer				li_anno_registrazione_cu
long					ll_num_registrazione_cu, ll_num_documento_cu, ll_count, ll_index, ll_perc
datetime				ldt_data_registrazione_cu, ldt_data_documento_cu
string					ls_cod_tipo_movimento_cu, ls_cod_prodotto_cu, ls_referenza_cu, ls_cod_tipo_mov_det_cu
decimal				ld_quan_movimento_cu, ld_val_movimento_cu
uo_ref_mov_mag	luo_ref_mov_mag

string					ls_cod_fornitore_filtro
integer				li_ret
long					ll_new


st_log.text = "Preparazione cursore in corso ..."
ls_cod_fornitore_filtro = dw_selezione.getitemstring(1,"cod_fornitore")
ll_index = 0

//conteggio -----------------------------------------------------------------------
declare cu_rep_mov_count dynamic cursor for sqlsa;
prepare sqlsa from :fs_sql_count;
open dynamic cu_rep_mov_count;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore cu_rep_mov_count: " + sqlca.sqlerrtext
	return -1
end if

fetch cu_rep_mov_count into	:ll_count;
close cu_rep_mov_count;

if isnull(ll_count) then ll_count = 0

hpb_1.position = 0
hpb_1.maxposition = ll_count

if ll_count > 0 then
else
	return 0
end if

//------------------------------------------------------------------------------------


declare cu_rep_mov dynamic cursor for sqlsa;
prepare sqlsa from :fs_sql;
open dynamic cu_rep_mov;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore cu_rep_mov: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_rep_mov into	:li_anno_registrazione_cu,   
									:ll_num_registrazione_cu,   
									:ldt_data_registrazione_cu,   
									:ls_cod_tipo_movimento_cu,   
									:ls_cod_prodotto_cu,   
									:ld_quan_movimento_cu,   
									:ld_val_movimento_cu,   
									:ll_num_documento_cu,   
									:ldt_data_documento_cu,   
									:ls_referenza_cu,   
									:ls_cod_tipo_mov_det_cu;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nella fetch del cursore cu_rep_mov: " + sqlca.sqlerrtext
		close cu_rep_mov;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	Yield()
	hpb_1.stepit( )
	ll_index += 1
	
	ll_perc = (ll_index / ll_count) * 100
	
	st_log.text = "("+string(ll_perc) + "%)  Elaborazione movimento del "+string(ldt_data_registrazione_cu, "dd/mm/yyyy")+" - n°"+string(li_anno_registrazione_cu)+"/"+string(ll_num_registrazione_cu)+" ..."
	
	if ls_cod_fornitore_filtro<>"" and not isnull(ls_cod_fornitore_filtro) then
		luo_ref_mov_mag = create uo_ref_mov_mag
		li_ret = luo_ref_mov_mag.uof_movimenti_per_cli_for(li_anno_registrazione_cu, ll_num_registrazione_cu, "", ls_cod_fornitore_filtro, fs_errore)
		destroy luo_ref_mov_mag;
		
		if li_ret<0 then
			//in fs_errore il messaggio
			close cu_rep_mov;
			return -1
		end if
		
		if li_ret=0 then continue		//salta il movimento
		
	end if
	
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new, "anno_registrazione", li_anno_registrazione_cu)
	dw_report.setitem(ll_new, "num_registrazione", ll_num_registrazione_cu)
	dw_report.setitem(ll_new, "data_registrazione", ldt_data_registrazione_cu)
	dw_report.setitem(ll_new, "cod_tipo_movimento", ls_cod_tipo_movimento_cu)
	dw_report.setitem(ll_new, "cod_prodotto", ls_cod_prodotto_cu)
	dw_report.setitem(ll_new, "quan_movimento", ld_quan_movimento_cu)
	dw_report.setitem(ll_new, "val_movimento", ld_val_movimento_cu)
	dw_report.setitem(ll_new, "num_documento", ll_num_documento_cu)
	dw_report.setitem(ll_new, "data_documento", ldt_data_documento_cu)
	dw_report.setitem(ll_new, "referenza", ls_referenza_cu)
	dw_report.setitem(ll_new, "cod_tipo_mov_det", ls_cod_tipo_mov_det_cu)
	
	
loop


close cu_rep_mov;

return 0
end function

event pc_setwindow;call super::pc_setwindow;
windowobject l_objects[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])

l_objects[1] = dw_selezione
l_objects[2] = cb_annulla
l_objects[3] = cb_report
l_objects[4] = st_log
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

is_sql_base = dw_report.getsqlselect( )

dw_report.object.datawindow.print.preview = "yes"


end event

on w_report_movimenti.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.st_log=create st_log
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.st_log
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.dw_selezione
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.dw_folder
end on

on w_report_movimenti.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.st_log)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_movimento_det",sqlca,&
                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_selezione,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  

end event

type hpb_1 from hprogressbar within w_report_movimenti
integer x = 46
integer y = 956
integer width = 3707
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type st_log from statictext within w_report_movimenti
integer x = 46
integer y = 812
integer width = 2158
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_report_movimenti
integer x = 1463
integer y = 1072
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_movimenti
integer x = 1851
integer y = 1072
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string			ls_cod_tipo_mov_det, ls_cod_prodotto, ls_cod_prodotto_a, &
				ls_flag_scelta, ls_cod_deposito, ls_intestazione, ls_cod_fornitore, ls_sql, ls_ricerca_cli_for, ls_cod_cliente, ls_errore, ls_sql_count

date   		ld_da_data, ld_a_data

setpointer(Hourglass!)

st_log.text = "Preparazione query in corso ..."

dw_selezione.accepttext()
ls_ricerca_cli_for = ""
ls_intestazione = ""
hpb_1.position = 0

ls_sql = is_sql_base
ls_sql_count = "select count(*) from mov_magazzino"

ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"
ls_sql_count += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

ls_cod_tipo_mov_det = dw_selezione.getitemstring(1,"cod_tipo_movimento_det")
ld_da_data = date(dw_selezione.getitemdatetime(1,"data_da"))
ld_a_data = date(dw_selezione.getitemdatetime(1,"data_a"))
ls_cod_prodotto = dw_selezione.getitemstring(1,"cod_prodotto")
ls_cod_prodotto_a = dw_selezione.getitemstring(1,"cod_prodotto_a")
ls_flag_scelta = dw_selezione.getitemstring(1,"flag_scelta")
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")

ls_cod_fornitore = dw_selezione.getitemstring(1,"cod_fornitore")
ls_cod_cliente = ""   //serve in futuro se si attiverà la ricerca per cliente ...

if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	//ricerca per cliente
	if not isnull(ls_cod_fornitore) and ls_cod_fornitore<>"" then
		//ricerca anche per fornitore: segnala che non puoi
		g_mb.warning("Attenzione!", "Puoi filtrare o per cliente o per fornitore, ma non  per entrambi!")
		return
	else
		ls_ricerca_cli_for = "C"
	end if
else
	if not isnull(ls_cod_fornitore) and ls_cod_fornitore<>"" then
		//ricerca per fornitore
		ls_ricerca_cli_for = "F"
	end if
end if

//ricerca per data da - a -------------------------------------------------------------------------
if not isnull(ld_da_data) and year(ld_da_data) > 1950 then
	ls_sql += " and data_registrazione>='"+string(ld_da_data, s_cs_xx.db_funzioni.formato_data)+"'"
	ls_sql_count += " and data_registrazione>='"+string(ld_da_data, s_cs_xx.db_funzioni.formato_data)+"'"
	
	ls_intestazione += "Data registrazione: da " + string(ld_da_data, "yyyy/mm/dd")
end if

if not isnull(ld_a_data) and year(ld_a_data) > 1950 then
	ls_sql += " and data_registrazione<='"+string(ld_a_data, s_cs_xx.db_funzioni.formato_data)+"'"
	ls_sql_count += " and data_registrazione<='"+string(ld_a_data, s_cs_xx.db_funzioni.formato_data)+"'"
	
	if ls_intestazione="" then
		//metti in intestazione Data registrazione
		ls_intestazione += "Data registrazione: a " + string(ld_a_data, "yyyy/mm/dd") + ";"
	else
		ls_intestazione +=" a " + string(ld_a_data, "yyyy/mm/dd") + ";" 
	end if
else
	if ls_intestazione<>"" then ls_intestazione += ";"
end if

if not isnull(ls_cod_tipo_mov_det) and ls_cod_tipo_mov_det<>"" then
	ls_sql += " and cod_tipo_mov_det='"+ls_cod_tipo_mov_det+"'"
	ls_sql_count += " and cod_tipo_mov_det='"+ls_cod_tipo_mov_det+"'"
	ls_intestazione += "Tipo mov.: " + ls_cod_tipo_mov_det + ";"
	
else
	//no tipo mov det specificato
	dw_selezione.setitem(1, "flag_scelta", "N")
	ls_flag_scelta = "N"
end if

//stampa totali
if isnull(ls_cod_tipo_mov_det) or ls_cod_tipo_mov_det="" then 
	ls_cod_tipo_mov_det = "%"
	dw_selezione.setitem(1, "flag_scelta", "N")
	ls_flag_scelta = "N"
end if

if isnull(ls_flag_scelta) then ls_flag_scelta="N"
ls_intestazione = "Stampa totali: " + ls_flag_scelta+";"

if not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
	ls_intestazione += " Fornitore: " + ls_cod_fornitore + ";"
end if


if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
	ls_sql += " and cod_deposito='"+ls_cod_deposito+"'"
	ls_sql_count += " and cod_deposito='"+ls_cod_deposito+"'"
	ls_intestazione +=" Deposito: " + ls_cod_deposito + ";" 
end if

if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	ls_sql += " and cod_prodotto>='"+ls_cod_prodotto+"'"
	ls_sql_count += " and cod_prodotto>='"+ls_cod_prodotto+"'"
	ls_intestazione += "da Prodotto :" + ls_cod_prodotto //+ " a: " + ls_cod_prodotto_a + ";"
end if

if ls_cod_prodotto_a<>"" and not isnull(ls_cod_prodotto_a) then
	ls_sql += " and cod_prodotto<='"+ls_cod_prodotto_a+"'" // ticket 2013/27
	ls_sql_count += " and cod_prodotto<='"+ls_cod_prodotto_a+"'" // ticket 2013/27
	ls_intestazione += " fino a Prodotto: " + ls_cod_prodotto_a + ";"
end if

if isnull(ls_flag_scelta) then ls_flag_scelta = "N"


dw_report.object.intestazione.text = ls_intestazione
dw_report.reset()

ls_sql += " order by data_registrazione"

//dw_report.setsqlselect(ls_sql)

//dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_mov_det, ld_da_data, ld_a_data, ls_cod_prodotto, ls_cod_prodotto_a, ls_cod_deposito)
//dw_report.retrieve()

if wf_report(ls_sql, ls_sql_count, ls_errore)<0 then
	setpointer(Arrow!)
	st_log.text = "Elaborazione terminata con errori!"
	g_mb.error(ls_errore)
	return -1
end if

dw_report.setsort("cod_prodotto a, data_registrazione a")
dw_report.sort()
dw_report.groupcalc()

st_log.text = "Pronto!"

if ls_flag_scelta = "N" then // deve nascondere i totali
	dw_report.object.tot_qta_prodotto_t.visible = 0
	dw_report.object.tot_qta_prodotto.visible = 0
	dw_report.object.tot_val_prodotto_t.visible = 0
	dw_report.object.tot_val_prodotto.visible = 0	
else
	dw_report.object.tot_qta_prodotto_t.visible = 1
	dw_report.object.tot_qta_prodotto.visible = 1
	dw_report.object.tot_val_prodotto_t.visible = 1
	dw_report.object.tot_val_prodotto.visible = 1	

end if

dw_folder.fu_selecttab(2)
dw_report.change_dw_current( )

setpointer(Arrow!)
end event

type dw_selezione from uo_cs_xx_dw within w_report_movimenti
integer x = 23
integer y = 124
integer width = 2217
integer height = 804
integer taborder = 30
string dataobject = "d_selezione_movimenti"
boolean border = false
end type

event itemchanged;call super::itemchanged;//string ls_nomecol, ls_nulla
//
//setnull(ls_nulla)
//
//ls_nomecol = i_colname
//if ls_nomecol = "cod_prodotto" then
//	setitem(getrow(), "cod_deposito", ls_nulla)
//end if
//if ls_nomecol = "cod_prodotto_a" then
//	setitem(getrow(), "cod_deposito", ls_nulla)
//end if
//
//if ls_nomecol = "cod_deposito" then
//	setitem(getrow(), "cod_prodotto", ls_nulla)
//	setitem(getrow(), "cod_prodotto_a", ls_nulla)
//end if
//
//
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione, "cod_fornitore")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_movimenti
boolean visible = false
integer x = 23
integer y = 124
integer width = 3753
integer height = 1644
integer taborder = 20
string dataobject = "d_report_movimenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_movimenti
integer x = 9
integer y = 12
integer width = 3785
integer height = 1780
integer taborder = 50
end type

