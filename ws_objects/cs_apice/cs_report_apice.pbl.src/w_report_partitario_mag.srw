﻿$PBExportHeader$w_report_partitario_mag.srw
$PBExportComments$Report Partitario di Magazzino
forward
global type w_report_partitario_mag from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_partitario_mag
end type
type cb_report from commandbutton within w_report_partitario_mag
end type
type cb_selezione from commandbutton within w_report_partitario_mag
end type
type dw_report from uo_cs_xx_dw within w_report_partitario_mag
end type
type dw_selezione from uo_cs_xx_dw within w_report_partitario_mag
end type
end forward

global type w_report_partitario_mag from w_cs_xx_principale
integer x = 46
integer y = 260
integer width = 3570
integer height = 1672
string title = "Report Partitario Magazzino per Prodotto"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
dw_selezione dw_selezione
end type
global w_report_partitario_mag w_report_partitario_mag

type variables
string is_original_select
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report

this.x = 46
this.y = 261
this.width = 2583
this.height = 685

end event

on w_report_partitario_mag.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_selezione
end on

on w_report_partitario_mag.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.dw_selezione)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW( dw_selezione, &
//							"rs_cod_prodotto", &
//							sqlca, &
//							"anag_prodotti", &
//							"cod_prodotto", &
//                     "des_prodotto", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//							
//f_PO_LoadDDDW_DW( dw_selezione, &
//							"rs_cod_cliente", &
//							sqlca, &
//							"anag_clienti", &
//							"cod_cliente", &
//                     "rag_soc_1", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_PO_LoadDDDW_DW( dw_selezione, &
//							"rs_cod_fornitore", &
//							sqlca, &
//							"anag_fornitori", &
//							"cod_fornitore", &
//                     "rag_soc_1", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//
end event

event open;call super::open;cb_report.enabled=false	
is_original_select =  &
	dw_report.Describe("DataWindow.Table.Select")
dw_selezione.SetColumn ("rs_cod_prodotto")
end event

type cb_annulla from commandbutton within w_report_partitario_mag
integer x = 1472
integer y = 476
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_partitario_mag
integer x = 1861
integer y = 476
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;
string ls_where_clause, ls_mod_string, ls_rc, ls_error, ls_saldo_iniz, &
		 ls_saldo_fin, ls_cod_prodotto, ls_cod_cliente, ls_cod_fornitore, &
		 ls_cod_cat_mer, ls_cod_tipo_movimento_det , ls_cod_tipo_movimento, &
		 ls_chiave[] 
boolean lb_calcola_saldo, lb_cli_for
datetime ldt_data_registrazione_da,ldt_data_registrazione_a, ldt_data_chiusura_annuale
dec{4} ld_saldo_iniz, ld_saldo_fin, ld_quan_val[], ld_quan_movimento, ld_val_movimento, &
		 ld_saldo_iniz_val, ld_saldo_fin_val, ld_giacenza[], ld_quan_terzista, ld_costo_medio_stock[], &
		 ld_quant_val_stock[], ld_quan_costo_medio_stock[]
integer li_ret_fun
long ll_cont, ll_num_mov_prod
uo_magazzino luo_magazzino


dw_selezione.AcceptText()

lb_calcola_saldo = true
lb_cli_for = false
ls_where_clause=""
ls_cod_prodotto = dw_selezione.getitemstring(1,"rs_cod_prodotto")
ls_cod_cliente = dw_selezione.getitemstring(1,"rs_cod_cliente")
ls_cod_fornitore = dw_selezione.getitemstring(1,"rs_cod_fornitore")

ldt_data_registrazione_da= dw_selezione.getitemdatetime(1,"rdt_data_registrazione_da")
ldt_data_registrazione_a = dw_selezione.getitemdatetime(1,"rdt_data_registrazione_a")

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1990"))

if isnull(ldt_data_registrazione_da) then  
	ldt_data_registrazione_da  = ldt_data_chiusura_annuale
end if

if isnull(ldt_data_registrazione_a)  then  ldt_data_registrazione_a = datetime(today())

if ldt_data_registrazione_a < ldt_data_registrazione_da then
	g_mb.messagebox("Errore parametri Partitario","Data Registrazione a minore di Data Registrazione da")
	return
end if


if (ldt_data_registrazione_da < ldt_data_chiusura_annuale) or (ldt_data_registrazione_a < ldt_data_chiusura_annuale) then
	g_mb.messagebox("Errore parametri Partitario","Non si possono calcolare le quantità per movimenti antecedenti l'ultima chiusura annuale")
	lb_calcola_saldo=false 
end if

if not isnull(ls_cod_cliente) then
	ls_where_clause = ls_where_clause + " and (mov_magazzino.cod_cliente= :rs_cod_cliente ) "
	lb_cli_for = true
end if

if not isnull(ls_cod_fornitore) then
	ls_where_clause = ls_where_clause + " and (mov_magazzino.cod_fornitore = :rs_cod_fornitore )"
	lb_cli_for = true
end if

ld_saldo_iniz=0
ld_saldo_fin=0

ls_mod_string = "DataWindow.Table.Select='"  &
	+ is_original_select + ls_where_clause + "'"


dw_selezione.hide()

parent.x = 20
parent.y = 50
parent.width = 3649
parent.height = 1665

dw_report.show()

ls_rc = dw_report.Modify(ls_mod_string)
IF ls_rc = "" THEN
	dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ldt_data_registrazione_da, &
		ldt_data_registrazione_a, ls_cod_cliente, ls_cod_fornitore)
ELSE
	g_mb.messagebox("Status", "Modify Failed" + ls_rc)
END IF


dw_report.setsort("mov_magazzino_data_registrazione a, mov_magazzino_num_registrazione a")
dw_report.sort()

for ll_cont=1 to 14
	ld_quan_val[ll_cont]=0
next
if lb_calcola_saldo=true and lb_cli_for=false then
	// saldo iniziale
	SELECT saldo_quan_inizio_anno,   
			 val_inizio_anno  
	 INTO :ld_quan_val[1],   
			:ld_quan_val[2]  
	 FROM anag_prodotti  
	WHERE cod_prodotto = :s_cs_xx.cod_azienda AND  
			cod_prodotto = :ls_cod_prodotto ;

	ld_quan_val[2] = ld_quan_val[1] * ld_quan_val[2]
	
	luo_magazzino = CREATE uo_magazzino
	
	li_ret_fun = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_registrazione_da, ls_where_clause, ld_quan_val, ls_error, "N", ls_chiave[], ld_giacenza[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
	
	ld_saldo_iniz = ld_quan_val[1] + ld_quan_val[4] - ld_quan_val[6]
	
	if ld_quan_val[12] <> 0 then 
		ld_saldo_iniz_val = ld_quan_val[13] / ld_quan_val[12]
	else
		ld_saldo_iniz_val = 0
	end if
	
//	ls_saldo_iniz = " Giacenza:" + string(ld_saldo_iniz, "###,#") + "   costo medio:" + string(ld_saldo_iniz_val, "###,#") + &
//						 " costo ultimo:" + string(ld_quan_val[14], "###,#") 
	ls_saldo_iniz = " Giacenza:" + string(ld_saldo_iniz, "###,#")
	dw_report.Object.saldo_iniz.text = ls_saldo_iniz
end if

// fd_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven; alla data ldt_data_registrazione_da

// calcolo i progressivi dei movimenti selezionati

ll_num_mov_prod=dw_report.rowcount()  // numero di movimenti selezionati fra le due date

for ll_cont=1 to 14
	ld_quan_val[ll_cont]=0
next

if ll_num_mov_prod > 0 then
	
	for ll_cont = 1 to ll_num_mov_prod
		
		ld_quan_terzista = 0
		ls_cod_tipo_movimento_det = dw_report.getItemString(ll_cont,"cod_tipo_mov_det")
		ls_cod_tipo_movimento = dw_report.getItemString(ll_cont,"cod_tipo_movimento")
		ld_quan_movimento = dw_report.GetItemNumber(ll_cont,"quan_movimento")
		ld_val_movimento = dw_report.GetItemNumber(ll_cont,"val_movimento")
		
		luo_magazzino = CREATE uo_magazzino		
		
		li_ret_fun = luo_magazzino.uof_single_mov_mag_decimal(ls_cod_tipo_movimento_det, ld_quan_movimento,ld_val_movimento, ld_quan_val, ld_quant_val_stock, ls_error, ls_cod_tipo_movimento, ld_quan_terzista)

		destroy luo_magazzino
		
		if li_ret_fun < 0 then 
			g_mb.messagebox("Partitario Magazzino", ls_error)
			return
		end if
		
	next
	
end if

ld_saldo_fin = ld_saldo_iniz + ld_quan_val[1] +ld_quan_val[4] - ld_quan_val[6]

if ld_quan_val[12] <> 0 then 
	ld_saldo_fin_val = ld_quan_val[13] / ld_quan_val[12]
else
	ld_saldo_fin_val = 0
end if


// ls_saldo_fin = " Giacenza:" + string(ld_saldo_fin, "###,#") + "   costo medio:" + string(ld_saldo_fin_val, "###,#") + &
//					" costo ultimo:" + string(ld_quan_val[14], "###,#") 
ls_saldo_fin = " Giacenza:" + string(ld_saldo_fin, "###,#")
dw_report.Object.saldo_fin.text = ls_saldo_fin
dw_report.groupcalc()

cb_selezione.show()
dw_selezione.object.b_ricerca_cliente.visible=false
dw_selezione.object.b_ricerca_prodotto.visible=false

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_partitario_mag
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_report.Object.saldo_fin.text = "0.000.000.000.000.000,0000"
dw_report.Object.saldo_iniz.text = "0.000.000.000.000.000,0000"

dw_selezione.show()

parent.x = 46
parent.y = 261
parent.width = 2583
parent.height = 685

dw_report.reset()
dw_report.hide()
cb_selezione.hide()
dw_selezione.object.b_ricerca_cliente.visible=true
dw_selezione.object.b_ricerca_prodotto.visible=true

dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_partitario_mag
boolean visible = false
integer x = 23
integer y = 20
integer width = 3566
integer height = 1420
integer taborder = 80
string dataobject = "d_report_partitario_mag"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_selezione from uo_cs_xx_dw within w_report_partitario_mag
integer x = 23
integer y = 20
integer width = 2203
integer height = 428
integer taborder = 40
string dataobject = "d_selezione_report_partitario_mag"
end type

event losefocus;call super::losefocus;// this.AcceptText()
end event

event editchanged;call super::editchanged;string ls_nome

ls_nome = dwo.name
if ls_nome = "rdt_data_registrazione_da" or ls_nome = "rdt_data_registrazione_a" then
	cb_report.enabled=true
end if



end event

event itemchanged;call super::itemchanged;string ls_descrizione

if i_colname = "rs_cod_cliente" then
	ls_descrizione = f_des_tabella("anag_clienti","cod_cliente = '" +  i_coltext + "'","rag_soc_1")
	if ls_descrizione = "< CODICE INESISTENTE >" then return 2
end if


if i_colname = "rs_cod_fornitore" then
	ls_descrizione = f_des_tabella("anag_fornitori","cod_fornitore = '" +  i_coltext   + "'","rag_soc_1")
	if ls_descrizione = "< CODICE INESISTENTE >" then return 2
end if

if i_colname = "rs_cod_prodotto" then
	ls_descrizione = f_des_tabella("anag_prodotti","cod_prodotto = '" +  i_coltext + "'","des_prodotto")
	if ls_descrizione = "< CODICE INESISTENTE >" then return 2
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"rs_cod_cliente")
case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"rs_cod_prodotto")
case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"rs_cod_fornitore")
end choose
end event

