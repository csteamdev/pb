﻿$PBExportHeader$w_report_prodotti_foto.srw
$PBExportComments$Finestra Report Foto Prodotti
forward
global type w_report_prodotti_foto from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_prodotti_foto
end type
type cb_report from commandbutton within w_report_prodotti_foto
end type
type cb_selezione from commandbutton within w_report_prodotti_foto
end type
type dw_selezione from uo_cs_xx_dw within w_report_prodotti_foto
end type
type dw_report from uo_cs_xx_dw within w_report_prodotti_foto
end type
end forward

global type w_report_prodotti_foto from w_cs_xx_principale
integer x = 677
integer y = 700
integer width = 3465
integer height = 1708
string title = "Foto Prodotti"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_prodotti_foto w_report_prodotti_foto

type variables
string original_select
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 677
this.y = 589
this.width = 2579
this.height =841
cb_selezione.hide()
end event

on w_report_prodotti_foto.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_prodotti_foto.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"rs_prodotto", &
							sqlca, &
							"anag_prodotti", &
							"cod_prodotto", &
                     "des_prodotto", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW( dw_selezione, &
							"rs_cat_mer", &
							sqlca, &
							"tab_categorie", &
							"cod_categoria", &
                     "des_categoria", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW( dw_selezione, &
							"rs_responsabile", &
							sqlca, &
							"tab_responsabili", &
							"cod_responsabile", &
                     "des_responsabile", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
									  
f_PO_LoadDDDW_DW( dw_selezione, &
							"rs_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event open;call super::open;original_select =  &
	dw_report.Describe("DataWindow.Table.Select")
	
end event

type cb_annulla from commandbutton within w_report_prodotti_foto
integer x = 1783
integer y = 520
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_prodotti_foto
integer x = 2167
integer y = 516
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_prodotto, ls_cat_mer, ls_responsabile, ls_deposito, ls_path
string ls_where_clause, ls_mod_string, ls_rc, ls_vql,ls_pti, ls_cod_azienda

ls_prodotto = dw_selezione.getitemstring(1,"rs_prodotto")
ls_cat_mer  = dw_selezione.getitemstring(1,"rs_cat_mer")
ls_responsabile = dw_selezione.getitemstring(1,"rs_responsabile")
ls_deposito  = dw_selezione.getitemstring(1,"rs_deposito")

ls_where_clause=""

if not isnull(ls_deposito) then
	ls_where_clause = ls_where_clause + " and (anag_prodotti.cod_deposito = :rs_cod_deposito )"
end if

if not isnull(ls_responsabile) then
	ls_where_clause = ls_where_clause + " and (anag_prodotti.cod_responsabile = :rs_cod_responsabile )"
end if

if not isnull(ls_cat_mer) then
	ls_where_clause = ls_where_clause + " and (anag_prodotti.cod_cat_mer = :rs_cod_cat_mer )"
end if

if isnull(ls_prodotto) then ls_prodotto = "%"

if isnull(ls_cat_mer) then  ls_cat_mer  = "%"
if isnull(ls_responsabile) then ls_responsabile = "%"
if isnull(ls_deposito) then  ls_deposito  = "%"

ls_mod_string = "DataWindow.Table.Select='"  &
	+ original_select + ls_where_clause + "'"

ls_cod_azienda=s_cs_xx.cod_azienda

select parametri.stringa 
into :ls_vql
from parametri 
where (parametri.flag_parametro='S' and parametri.cod_parametro='VQL')
using sqlca;

select parametri_azienda.stringa 
into :ls_pti
from parametri_azienda 
where (parametri_azienda.flag_parametro='S' and parametri_azienda.cod_parametro='PTI' and cod_azienda=:ls_cod_azienda)
using sqlca;


ls_path=ls_vql+ls_pti+"\"

dw_selezione.hide()
parent.x = 100
parent.y = 50
parent.width = 3500
parent.height = 1733

dw_report.show()


ls_rc = dw_report.Modify(ls_mod_string)
IF ls_rc = "" THEN
	dw_report.retrieve(ls_cod_azienda, ls_prodotto, ls_deposito, ls_cat_mer, ls_responsabile, ls_path)
ELSE
	g_mb.messagebox("Status", "Modify Failed" + ls_rc)
END IF



cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_prodotti_foto
integer x = 3040
integer y = 1500
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()

parent.x = 677
parent.y = 589
parent.width = 2579
parent.height = 841

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_prodotti_foto
integer x = 23
integer y = 20
integer width = 2514
integer height = 480
integer taborder = 20
string dataobject = "d_selezione_report_prodotti_foto"
borderstyle borderstyle = stylelowered!
end type

type dw_report from uo_cs_xx_dw within w_report_prodotti_foto
boolean visible = false
integer x = 23
integer y = 20
integer width = 3383
integer height = 1440
integer taborder = 10
string dataobject = "d_report_prodotti_foto"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

