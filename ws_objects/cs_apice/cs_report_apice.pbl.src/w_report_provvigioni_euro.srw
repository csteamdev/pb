﻿$PBExportHeader$w_report_provvigioni_euro.srw
$PBExportComments$REPORT PROVVIGIONI AGENTI
forward
global type w_report_provvigioni_euro from w_cs_xx_principale
end type
type dw_folder from u_folder within w_report_provvigioni_euro
end type
type dw_report from uo_cs_xx_dw within w_report_provvigioni_euro
end type
type dw_ext_sel_provvigioni from uo_cs_xx_dw within w_report_provvigioni_euro
end type
end forward

global type w_report_provvigioni_euro from w_cs_xx_principale
integer width = 3991
integer height = 2032
string title = "Report Provvigioni"
dw_folder dw_folder
dw_report dw_report
dw_ext_sel_provvigioni dw_ext_sel_provvigioni
end type
global w_report_provvigioni_euro w_report_provvigioni_euro

forward prototypes
public function integer wf_calcola_provvigioni (string fs_cod_agente, string fs_cod_documento, datetime fd_data_inizio, datetime fd_data_fine, long fn_num_doc_inizio, long fn_num_doc_fine, string fs_flag_provv_zero, string fs_cod_cliente, string fs_flag_tipo_report)
public subroutine wf_report ()
public subroutine wf_reset ()
end prototypes

public function integer wf_calcola_provvigioni (string fs_cod_agente, string fs_cod_documento, datetime fd_data_inizio, datetime fd_data_fine, long fn_num_doc_inizio, long fn_num_doc_fine, string fs_flag_provv_zero, string fs_cod_cliente, string fs_flag_tipo_report);string					ls_sql_1, ls_sql_2, ls_cod_cliente, ls_cod_valuta, ls_cod_documento, ls_numeratore_documento, &
						ls_cod_agente_1, ls_cod_agente_2, ls_cod_prodotto, ls_des_prodotto, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
						ls_cod_tipo_fat_ven, ls_rag_soc_cliente, ls_rag_soc_agente, ls_flag_tipo_fat_ven, ls_formato, ls_cod_pagamento, ls_cod_misura_mag, ls_cod_misura_ven
						
long					ll_anno_registrazione, ll_num_registrazione,ll_anno_documento,ll_num_documento, ll_riga, ll_precisione

dec{4}				ld_cambio_ven, ld_quan_fatturata, ld_imponibile_iva, ld_prog_riga_fat_ven, &
						ld_provvigione_1,ld_provvigione_2, ld_imponibile_riga, ld_importo_provvigione, ld_perc_provvigione, ld_precisione, &
						ld_sconto_pagamento, ld_prezzo_vendita, ld_sconto_1
						
datetime				ldt_data_fattura

string					ls_rag_soc_cliente_f, ls_cod_cliente_where

integer				li_flag_gruppo = 0



if isnull(fs_cod_documento) then fs_cod_documento = "%"

if fs_cod_cliente <> "" then
	select rag_soc_1
	into :ls_rag_soc_cliente_f
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :fs_cod_cliente;
	
	ls_cod_cliente_where = fs_cod_cliente + "%"
else
	ls_cod_cliente_where = "%"
	ls_rag_soc_cliente_f = ""
end if

//---------------------------------------------------------------------------------------------------------
//cursore testata fatture vendita
declare cu_tes_fat_ven cursor for 
select		anno_registrazione,
			num_registrazione,
			cod_cliente,
			cambio_ven,
			cod_valuta,
			cod_documento, 
			numeratore_documento, 
			anno_documento, 
			num_documento,
			data_fattura,
			cod_agente_1,
			cod_agente_2,
			cod_tipo_fat_ven,
			cod_pagamento
from   tes_fat_ven
where	cod_azienda = :s_cs_xx.cod_azienda and 
			(cod_agente_1 = :fs_cod_agente or cod_agente_2 = :fs_cod_agente) and
			data_fattura between :fd_data_inizio and :fd_data_fine and
			num_documento >= :fn_num_doc_inizio and num_documento <= :fn_num_doc_fine and
			cod_documento is not null and cod_documento like :fs_cod_documento and
			num_documento > 0 and cod_cliente like :ls_cod_cliente_where
order by cod_documento, num_documento;


//---------------------------------------------------------------------------------------------------------
//cursore dettaglio fatture vendita
declare cu_det_fat_ven cursor for 
select		det_fat_ven.cod_prodotto,
			det_fat_ven.des_prodotto,
			det_fat_ven.cod_tipo_det_ven,
			det_fat_ven.quan_fatturata,
			det_fat_ven.imponibile_iva,
			det_fat_ven.provvigione_1,
			det_fat_ven.provvigione_2,
			det_fat_ven.prog_riga_fat_ven,
			det_fat_ven.prezzo_vendita,
			det_fat_ven.sconto_1,
			anag_prodotti.cod_misura_mag,
			det_fat_ven.cod_misura
from    det_fat_ven
left outer join anag_prodotti on 	anag_prodotti.cod_azienda=det_fat_ven.cod_azienda and
											anag_prodotti.cod_prodotto=det_fat_ven.cod_prodotto
where	det_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			det_fat_ven.anno_registrazione = :ll_anno_registrazione and
			det_fat_ven.num_registrazione = :ll_num_registrazione
order by det_fat_ven.prog_riga_fat_ven;


//----------------------------------------------------------------------------------------------------------
//apertura cursore testata fatture
open cu_tes_fat_ven;


if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in OPEN cursore cu_tes_fat_ven " + sqlca.sqlerrtext)
	return -1
end if


//----------------------------------------------------------------------------------------------------------
//ciclo cursore testata fatture
do while 1=1
	fetch cu_tes_fat_ven into		:ll_anno_registrazione, :ll_num_registrazione, :ls_cod_cliente, :ld_cambio_ven, :ls_cod_valuta, 
										:ls_cod_documento, :ls_numeratore_documento, :ll_anno_documento, :ll_num_documento, :ldt_data_fattura, 
										:ls_cod_agente_1, :ls_cod_agente_2, :ls_cod_tipo_fat_ven, :ls_cod_pagamento;
										
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		g_mb.error("Errore in lettura testata fatture di vendita. Dettaglio errore: " + sqlca.sqlerrtext)
		close cu_tes_fat_ven;
		exit
	end if
	
	dw_ext_sel_provvigioni.object.log_t.text = "Elaborazione fattura n° " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " in corso ..."
	
	select flag_tipo_fat_ven
	into   :ls_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in ricerca tipo documento in tabella tab_tipi_fat_ven: dettaglio errore: " + sqlca.sqlerrtext)
		close cu_tes_fat_ven;
		exit
	end if
	
	select sconto
	into :ld_sconto_pagamento
	from tab_pagamenti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_pagamento = :ls_cod_pagamento;
	if sqlca.sqlcode <> 0 then ld_sconto_pagamento = 0
		
	
	select precisione, formato
	into   :ld_precisione, :ls_formato
	from   tab_valute
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_valuta = :ls_cod_valuta;
			 
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in select da tabella valute: "+sqlca.sqlerrtext)
		close cu_tes_fat_ven;
		exit
	end if
	
	choose case ld_precisione
		case 1
			ll_precisione = 0
		case 0.1
			ll_precisione = 1
		case 0.01 
			ll_precisione = 2
		case 0.001 
			ll_precisione = 3
		case 0.0001 
			ll_precisione = 4
		case 0.00001 
			ll_precisione = 5
		case else
			ll_precisione = 0
	end choose
	
	if ls_flag_tipo_fat_ven <> "P" and ls_flag_tipo_fat_ven <> "F" then
		
		if li_flag_gruppo = 0 then
			li_flag_gruppo = 1
		else
			li_flag_gruppo = 0
		end if
		
		
		//----------------------------------------------------------------------------------------------------------
		//apertura cursore dettaglio fattura
		open cu_det_fat_ven;
		
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore in OPEN cursore cu_det_fat_ven " + sqlca.sqlerrtext)
			close cu_tes_fat_ven;
			return -1
		end if
		
		
		//----------------------------------------------------------------------------------------------------------
		//ciclo cursore dettaglio fattura
		do while 1=1
			fetch cu_det_fat_ven into	:ls_cod_prodotto, :ls_des_prodotto, :ls_cod_tipo_det_ven, :ld_quan_fatturata, &
												:ld_imponibile_iva, :ld_provvigione_1,:ld_provvigione_2, :ld_prog_riga_fat_ven, &
												:ld_prezzo_vendita, :ld_sconto_1, :ls_cod_misura_mag, :ls_cod_misura_ven;
												
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode = -1 then
				g_mb.error("Errore in lettura dettaglio fatture di vendita. Dettaglio errore: " + sqlca.sqlerrtext)
				close cu_det_fat_ven;
				close cu_tes_fat_ven;
				exit
			end if
			
			select flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.error("APICE","Errore in lettura tipi dettaglio di vendita. Dettaglio errore: " + sqlca.sqlerrtext)
				close cu_det_fat_ven;
				close cu_tes_fat_ven;
				exit
			end if
			
			
			// stefanop: 10/01/2013: In DB è già memorizzato con il valore meno, qundi faccio il controllo che sia 
			// uno sconto e che sia maggiore di 0
			if ls_flag_tipo_det_ven = "S" and ld_imponibile_iva > 0 then
				ld_imponibile_riga = ld_imponibile_iva * -1
			else
				ld_imponibile_riga = ld_imponibile_iva
			end if

			if isnull(ls_cod_misura_mag) or ls_cod_misura_mag="" then ls_cod_misura_mag = ls_cod_misura_ven

			if fs_cod_agente = ls_cod_agente_1 then

				ld_importo_provvigione = (ld_imponibile_riga * ld_provvigione_1) / 100
				ld_perc_provvigione = ld_provvigione_1
				if fs_flag_provv_zero = "N" and ld_importo_provvigione = 0 then continue
			end if
			
			if fs_cod_agente = ls_cod_agente_2 then
				ld_importo_provvigione = (ld_imponibile_riga * ld_provvigione_2) / 100
				ld_perc_provvigione = ld_provvigione_2
				if fs_flag_provv_zero = "N" and ld_importo_provvigione = 0 then continue
			end if
			
			ld_importo_provvigione = round(ld_importo_provvigione, ll_precisione)
	
			select rag_soc_1
			into   :ls_rag_soc_cliente
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_cliente = :ls_cod_cliente;
					 
			select rag_soc_1
			into   :ls_rag_soc_agente
			from   anag_agenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_agente = :fs_cod_agente;
	
	
			// inserisci riga nel report
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga, "titolo", "REPORT PROVVIGIONI AGENTE")
			dw_report.setitem(ll_riga, "formato", ls_formato)
			dw_report.setitem(ll_riga, "cod_agente", fs_cod_agente)
			dw_report.setitem(ll_riga, "rag_soc_agente", ls_rag_soc_agente)
			dw_report.setitem(ll_riga, "data_inizio", fd_data_inizio)
			dw_report.setitem(ll_riga, "data_fine", fd_data_fine)
			
			if fs_cod_cliente <> "" and not isnull(fs_cod_cliente) then
				dw_report.setitem(ll_riga, "cod_cliente_f", fs_cod_cliente)
				dw_report.setitem(ll_riga, "des_cliente_f", ls_rag_soc_cliente_f)			
			end if			
			
			dw_report.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
			dw_report.setitem(ll_riga, "rag_soc_cliente", ls_rag_soc_cliente)
			dw_report.setitem(ll_riga, "cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
			dw_report.setitem(ll_riga, "cod_documento", ls_cod_documento)
			dw_report.setitem(ll_riga, "numeratore_documento", ls_numeratore_documento)
			dw_report.setitem(ll_riga, "anno_documento", ll_anno_documento)
			dw_report.setitem(ll_riga, "num_documento", ll_num_documento)
			dw_report.setitem(ll_riga, "data_fattura", ldt_data_fattura)
			dw_report.setitem(ll_riga, "prog_riga_fat_ven", ld_prog_riga_fat_ven)
			dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
			dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
			
			if ls_flag_tipo_fat_ven = "N" then
				dw_report.setitem(ll_riga, "imponibile_provv", ld_imponibile_riga * -1)
				dw_report.setitem(ll_riga, "perc_provv", ld_perc_provvigione * -1)
				dw_report.setitem(ll_riga, "importo_provv", ld_importo_provvigione* -1)
			else
				dw_report.setitem(ll_riga, "imponibile_provv", ld_imponibile_riga)
				dw_report.setitem(ll_riga, "perc_provv", ld_perc_provvigione)
				dw_report.setitem(ll_riga, "importo_provv", ld_importo_provvigione)
			end if
			
			
			if fs_flag_tipo_report = "A" then
				try
					//se analitico metti prezzo, quantita, unità misura e sconto 1
					//metto dentro try/catch perchè alcune dw di questo report non hanno questi campi
					dw_report.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
					dw_report.setitem(ll_riga, "quantita", ld_quan_fatturata)
					dw_report.setitem(ll_riga, "um_vendita", ls_cod_misura_mag)
					dw_report.setitem(ll_riga, "sconto_1", ld_sconto_1)
					
					dw_report.setitem(ll_riga, "flag_gruppo", li_flag_gruppo)
				catch (runtimeerror err)
				end try
				
			end if
			
		loop
		//fine ciclo cursore dettaglio fattura
		//----------------------------------------------------------------------------------------------------------
		
		close cu_det_fat_ven;
	end if
loop
//fine ciclo cursore testata fatture
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
//chiusura cursore testata fatture
close cu_tes_fat_ven;

dw_folder.fu_selecttab(2)
dw_report.change_dw_current()

return 0
end function

public subroutine wf_report ();string ls_cod_agente, ls_flag_tipo_report, ls_flag_provv_zero, ls_cod_documento
long ll_fattura_inizio, ll_fattura_fine
datetime ldt_inizio, ldt_fine
string ls_cod_cliente

dw_ext_sel_provvigioni.accepttext()

ls_cod_agente 					= dw_ext_sel_provvigioni.getitemstring(1,"cod_agente")
ls_cod_documento 			= dw_ext_sel_provvigioni.getitemstring(1,"cod_documento")
ls_flag_tipo_report 			= dw_ext_sel_provvigioni.getitemstring(1,"flag_tipo_report")
ls_flag_provv_zero 			= dw_ext_sel_provvigioni.getitemstring(1,"flag_provv_zero")
ll_fattura_inizio 				= dw_ext_sel_provvigioni.getitemnumber(1,"fattura_inizio")
ll_fattura_fine 					= dw_ext_sel_provvigioni.getitemnumber(1,"fattura_fine")
ldt_inizio 						= datetime(dw_ext_sel_provvigioni.getitemdate(1,"data_inizio"), 00:00:00)
ldt_fine 							= datetime(dw_ext_sel_provvigioni.getitemdate(1,"data_fine"), 00:00:00)

ls_cod_cliente = dw_ext_sel_provvigioni.getitemstring(1,"cod_cliente")
if isnull(ls_cod_cliente)  then
	ls_cod_cliente = ""
end if

if isnull(ls_cod_agente) or len(ls_cod_agente) < 1 then
	g_mb.messagebox("APICE","Indicare un codice agente valido")
	return
end if

choose case ls_flag_tipo_report
	case "R"
		//raggruppato per aliquote per ogni fattura ----------------------------------------
		dw_report.dataobject = 'd_report_provvigioni_aliquote_euro'
		dw_report.object.datawindow.print.orientation = 2

	case "T"
		//totali per aliquote provvigioni ------------------------------------------------------
		dw_report.dataobject = 'd_report_provvigioni_tot_aliquote_euro'
		dw_report.object.datawindow.print.orientation = 2

	case "A"
		//analitico ------------------------------------------------------------------------------
		dw_report.dataobject = 'd_report_provvigioni_dettaglio_euro'
		dw_report.object.datawindow.print.orientation = 1

	case "S"
		//sintetico ------------------------------------------------------------------------------
		dw_report.dataobject = 'd_report_provvigioni_sintetico_euro'
		dw_report.object.datawindow.print.orientation = 2

	case else
		g_mb.warning("APICE","Tipo report non previsto")
		return
end choose

dw_ext_sel_provvigioni.object.log_t.text = "Elaborazione report in corso... attendere !"
dw_report.reset()
dw_report.setredraw(false)

wf_calcola_provvigioni (ls_cod_agente, ls_cod_documento, ldt_inizio, ldt_fine, &
								ll_fattura_inizio, ll_fattura_fine, ls_flag_provv_zero, &
								ls_cod_cliente, ls_flag_tipo_report)

choose case ls_flag_tipo_report
	case "R"
		dw_report.setsort("cod_documento, numeratore_documento, anno_documento, num_documento, perc_provv")
		dw_report.sort()
		dw_report.groupcalc()

	case "T"
		dw_report.setsort("perc_provv")
		dw_report.sort()
		dw_report.groupcalc()

	case else
		dw_report.setsort("cod_documento, numeratore_documento, anno_documento, num_documento, prog_riga_fat_ven")
		dw_report.sort()

end choose

dw_report.object.datawindow.print.preview = 'Yes'

dw_report.setredraw(true)
dw_report.change_dw_current()
dw_ext_sel_provvigioni.object.log_t.text = ""


end subroutine

public subroutine wf_reset ();string				ls_null
datetime			ldt_null


setnull(ls_null)
setnull(ldt_null)

//------------------------------------------
dw_ext_sel_provvigioni.setitem(1, "cod_agente", ls_null)
dw_ext_sel_provvigioni.setitem(1, "cod_documento", ls_null)
//------------------------------------------
dw_ext_sel_provvigioni.setitem(1, "data_inizio", ldt_null)
dw_ext_sel_provvigioni.setitem(1, "data_fine", ldt_null)
//------------------------------------------
dw_ext_sel_provvigioni.setitem(1, "fattura_inizio", 1)
dw_ext_sel_provvigioni.setitem(1, "fattura_fine", 999999)
//------------------------------------------
dw_ext_sel_provvigioni.setitem(1, "flag_tipo_report", "S")
dw_ext_sel_provvigioni.setitem(1, "flag_provv_zero", "N")
dw_ext_sel_provvigioni.setitem(1, "cod_cliente", ls_null)
//------------------------------------------

return
end subroutine

on w_report_provvigioni_euro.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_report=create dw_report
this.dw_ext_sel_provvigioni=create dw_ext_sel_provvigioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_ext_sel_provvigioni
end on

on w_report_provvigioni_euro.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_report)
destroy(this.dw_ext_sel_provvigioni)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_sel_provvigioni,"cod_agente",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1", &
                 "(anag_agenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_agenti.flag_blocco <> 'S') or (anag_agenti.flag_blocco = 'S' and anag_agenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )

f_PO_LoadDDDW_DW(dw_ext_sel_provvigioni,"cod_documento",sqlca,&
                 "tab_documenti","cod_documento","des_documento", &
                 "(tab_documenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_documenti.flag_blocco <> 'S') or (tab_documenti.flag_blocco = 'S' and tab_documenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )
end event

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_ext_sel_provvigioni.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])

l_objects[1] = dw_ext_sel_provvigioni
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

move(600,0)
this.height = w_cs_xx_mdi.mdi_1.height - 50

postevent("resize")


end event

event resize;

//script ancestor disattivato !!!!!


//sistemo il folder
dw_folder.width = this.width - 100
dw_folder.height = this.height - 150

dw_report.height = dw_folder.height - 150
dw_report.width = dw_folder.width - 100


return
end event

type dw_folder from u_folder within w_report_provvigioni_euro
integer x = 5
integer y = 4
integer width = 3945
integer height = 1920
integer taborder = 10
boolean border = false
end type

type dw_report from uo_cs_xx_dw within w_report_provvigioni_euro
integer x = 23
integer y = 108
integer width = 3913
integer height = 1752
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_report_provvigioni_sintetico_euro"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_ext_sel_provvigioni from uo_cs_xx_dw within w_report_provvigioni_euro
integer x = 23
integer y = 108
integer width = 3451
integer height = 804
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_ext_sel_provvigioni_euro"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;


choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ext_sel_provvigioni,"cod_cliente")
		
	case "b_report"
		wf_report()
		
	case "b_reset"
		wf_reset()
		
end choose
end event

