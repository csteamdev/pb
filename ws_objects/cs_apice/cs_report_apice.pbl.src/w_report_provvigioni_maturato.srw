﻿$PBExportHeader$w_report_provvigioni_maturato.srw
$PBExportComments$REPORT PROVVIGIONI AGENTI
forward
global type w_report_provvigioni_maturato from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_report_provvigioni_maturato
end type
type dw_sel_storico from datawindow within w_report_provvigioni_maturato
end type
type pb_1 from picturebutton within w_report_provvigioni_maturato
end type
type pb_2 from picturebutton within w_report_provvigioni_maturato
end type
type cb_stampa_provvisorio from commandbutton within w_report_provvigioni_maturato
end type
type dw_report from uo_cs_xx_dw within w_report_provvigioni_maturato
end type
type dw_folder from u_folder within w_report_provvigioni_maturato
end type
type dw_ext_sel_provvigioni from uo_cs_xx_dw within w_report_provvigioni_maturato
end type
end forward

global type w_report_provvigioni_maturato from w_cs_xx_principale
integer width = 4155
integer height = 2344
string title = "Report Provvigioni"
cb_1 cb_1
dw_sel_storico dw_sel_storico
pb_1 pb_1
pb_2 pb_2
cb_stampa_provvisorio cb_stampa_provvisorio
dw_report dw_report
dw_folder dw_folder
dw_ext_sel_provvigioni dw_ext_sel_provvigioni
end type
global w_report_provvigioni_maturato w_report_provvigioni_maturato

type variables
private:
	datetime idt_inizio, idt_fine
end variables

forward prototypes
public function decimal wf_calcola_riga (string fs_flag_tipo_det_ven, decimal fd_quantita, decimal fd_prezzo, string fs_cod_valuta, decimal fd_cambio, long fl_precisione, decimal fd_sconto_1, decimal fd_sconto_2, decimal fd_sconto_3, decimal fd_sconto_4, decimal fd_sconto_5, decimal fd_sconto_6, decimal fd_sconto_7, decimal fd_sconto_8, decimal fd_sconto_9, decimal fd_sconto_10)
public subroutine wf_imposta_report ()
public subroutine wf_log_dw (string as_message)
public function boolean wf_storicizza ()
public subroutine wf_carica_dw_storico ()
public subroutine wf_stampa_storico (long al_anno_registrazione, long al_num_registrazione)
public function boolean wf_controlla_periodo_report ()
public function integer wf_calcola_provvigioni (string fs_cod_agente, string fs_cod_documento, datetime fd_data_inizio, datetime fd_data_fine, datetime fd_data_maturazione, long fn_num_doc_inizio, long fn_num_doc_fine, string fs_flag_provv_zero, string fs_cod_cliente, datetime fdt_data_scadenza_inizio, datetime fdt_data_scadenza_fine, string fs_cod_tipo_anagrafica)
public subroutine wf_get_sqlquery (integer ai_querycount, ref string as_sqlquery)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_des_report ()
public function decimal wf_negativo (string as_flag_tipo_fat_ven, decimal ad_number)
end prototypes

public function decimal wf_calcola_riga (string fs_flag_tipo_det_ven, decimal fd_quantita, decimal fd_prezzo, string fs_cod_valuta, decimal fd_cambio, long fl_precisione, decimal fd_sconto_1, decimal fd_sconto_2, decimal fd_sconto_3, decimal fd_sconto_4, decimal fd_sconto_5, decimal fd_sconto_6, decimal fd_sconto_7, decimal fd_sconto_8, decimal fd_sconto_9, decimal fd_sconto_10);dec{4} ld_perc, ld_sconto_tot, ld_prezzo_scontato, ld_valore_riga

ld_perc = 100
ld_perc = ld_perc - ((ld_perc * fd_sconto_1) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_2) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_3) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_4) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_5) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_6) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_7) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_8) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_9) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_10) / 100)

ld_sconto_tot = 100 - ld_perc

ld_prezzo_scontato = fd_prezzo - ((fd_prezzo * ld_sconto_tot) / 100)

ld_valore_riga = ld_prezzo_scontato * fd_quantita
ld_valore_riga = round(ld_valore_riga,fl_precisione)

if fs_flag_tipo_det_ven = "S" then
	ld_valore_riga = ld_valore_riga * -1
end if
return ld_valore_riga
end function

public subroutine wf_imposta_report ();string ls_cod_agente, ls_flag_tipo_report, ls_flag_provv_zero, ls_cod_documento, ls_cod_tipo_anagrafica
long ll_fattura_inizio, ll_fattura_fine
datetime ldt_data_maturazione, ldt_data_scadenza_inizio, ldt_data_scadenza_fine
string ls_cod_cliente

dw_ext_sel_provvigioni.accepttext()

ls_cod_agente = dw_ext_sel_provvigioni.getitemstring(1,"cod_agente")
ls_cod_documento = dw_ext_sel_provvigioni.getitemstring(1,"cod_documento")
ls_flag_tipo_report = dw_ext_sel_provvigioni.getitemstring(1,"flag_tipo_report")
ls_flag_provv_zero = dw_ext_sel_provvigioni.getitemstring(1,"flag_provv_zero")
ll_fattura_inizio = dw_ext_sel_provvigioni.getitemnumber(1,"fattura_inizio")
ll_fattura_fine = dw_ext_sel_provvigioni.getitemnumber(1,"fattura_fine")
idt_inizio = datetime(dw_ext_sel_provvigioni.getitemdate(1,"data_inizio"), 00:00:00)
idt_fine = datetime(dw_ext_sel_provvigioni.getitemdate(1,"data_fine"), 00:00:00)
ldt_data_maturazione = datetime(dw_ext_sel_provvigioni.getitemdate(1,"data_maturazione"), 00:00:00)
ldt_data_scadenza_inizio = dw_ext_sel_provvigioni.getitemdatetime(1,"data_scadenza_inizio")
ldt_data_scadenza_fine = dw_ext_sel_provvigioni.getitemdatetime(1,"data_scadenza_fine")
ls_cod_cliente = dw_ext_sel_provvigioni.getitemstring(1,"cod_cliente")
ls_cod_tipo_anagrafica = dw_ext_sel_provvigioni.getitemstring(1, "cod_tipo_anagrafica")

//if isnull(ls_cod_cliente)  then
//	ls_cod_cliente = ""
//end if

if isnull(ldt_data_scadenza_inizio) then ldt_data_scadenza_inizio = datetime('01-01-1900 00:00:000')
if isnull(ldt_data_scadenza_fine) then ldt_data_scadenza_inizio = datetime('31-12-2099 00:00:000')
if isnull(ldt_data_maturazione) then ldt_data_scadenza_inizio = ldt_data_scadenza_fine


if isnull(idt_inizio) then idt_inizio = datetime('01-01-1900 00:00:000')
if isnull(idt_fine) then idt_fine = datetime('31-12-2099 00:00:000')

if isnull(ls_cod_agente) or len(ls_cod_agente) < 1 then
	g_mb.messagebox("APICE","Indicare un codice agente valido")
	return
end if

if not wf_controlla_periodo_report() then
	return
end if

wf_log_dw("Elaborazione report in corso... attendere !")
dw_report.reset()
dw_report.setredraw(false)

wf_calcola_provvigioni (ls_cod_agente, ls_cod_documento, idt_inizio, idt_fine, ldt_data_maturazione,ll_fattura_inizio, ll_fattura_fine, ls_flag_provv_zero, ls_cod_cliente,ldt_data_scadenza_inizio,ldt_data_scadenza_fine, ls_cod_tipo_anagrafica)

dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.orientation = '1'

dw_report.setredraw(true)

// se stampa definitiva salvo i dati nel database
if dw_ext_sel_provvigioni.getitemstring(1, "flag_definitiva") = "S" then
	
	dw_report.modify("t_stampa_def.Text='Stampa consolidata'")
	
	if wf_storicizza() then
		commit;
		wf_carica_dw_storico()
	else
		rollback;
	end if
else
	dw_report.modify("t_stampa_def.Text=''")
end if
// ----

wf_log_dw("")


end subroutine

public subroutine wf_log_dw (string as_message);/**
 * stefanop
 * 11/05/2010
 *
 * Il log va inserito nella colonna log della dw della ricerca
 **/
 
dw_ext_sel_provvigioni.setitem(1, "log", as_message)
dw_ext_sel_provvigioni.setredraw(true)
end subroutine

public function boolean wf_storicizza ();/**
 * stefanop
 * 11/05/2011
 *
 * Storicizza il report appena calcolato
 **/

string ls_des_periodo, ls_agente_rag_soc_1, ls_periodo_report, ls_des_cliente, ls_cod_cliente, ls_rag_soc_1, ls_tipo_documento, ls_esito_pagamento, &
		ls_cod_agente
long ll_anno_registrazione, ll_num_registrazione, ll_row, ll_progressivo, ll_anno_documento, ll_num_documento, ll_count, ll_id_scadenza_impresa
datetime ldt_today, ldt_data_documento, ldt_scadenza, ldt_data_rif_maturazione
decimal{4} ld_imponibile_documento, ld_importo_provvigioni, ld_importo_scadenza, ld_importo_provvigione, &
ld_totale_documento, ld_imponibile_provv_scadenza

ldt_today = datetime(today(), now())
ll_anno_registrazione = f_anno_esercizio()

// chiedo la descrizione  per la storicizzazione
if not g_mb.prompt(ls_des_periodo, "Descrizione periodo", 250) then
	g_mb.show("Inserire una descrizione periodo valida per poter storicizzare il report")
	return false
end if

ls_cod_agente = dw_ext_sel_provvigioni.getitemstring(1, "cod_agente")

// calcolo progressivo
select max(num_registrazione) + 1
into :ll_num_registrazione
from tes_storico_provvigioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il calcolo del progressivo testata.~r~n" + sqlca.sqlerrtext)
	return false
elseif isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
end if
// ----

// Creo testata
insert into tes_storico_provvigioni (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	data_creazione_report,
	ora_creazione_report,
	cod_utente,
	data_inizio,
	data_fine,
	des_periodo
) values (
	:s_cs_xx.cod_azienda,
	:ll_anno_registrazione,
	:ll_num_registrazione,
	:ldt_today,
	:ldt_today,
	:s_cs_xx.cod_utente,
	:idt_inizio,
	:idt_fine,
	:ls_des_periodo);
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la creazione della testata.~r~n" + sqlca.sqlerrtext)
	return false
end if
	
// creo dettaglio
for ll_row = 1 to dw_report.rowcount()

	ls_agente_rag_soc_1 = dw_report.getitemstring(ll_row, "agente_rag_soc_1")
	ls_periodo_report = dw_report.getitemstring(ll_row, "periodo_report")
	ls_des_cliente = dw_report.getitemstring(ll_row, "des_cliente")
	ls_cod_cliente = dw_report.getitemstring(ll_row, "cod_cliente")
	ls_rag_soc_1 = dw_report.getitemstring(ll_row, "rag_soc_1")
	ls_tipo_documento = dw_report.getitemstring(ll_row, "tipo_documento")
	ll_anno_documento = dw_report.getitemnumber(ll_row, "anno_documento")
	ll_num_documento = dw_report.getitemnumber(ll_row, "num_documento")
	ldt_data_documento = dw_report.getitemdatetime(ll_row, "data_documento")
	ld_imponibile_documento = dw_report.getitemnumber(ll_row, "imponibile_documento")
	ld_importo_provvigioni = dw_report.getitemnumber(ll_row, "importo_provvigioni")
	ldt_scadenza = dw_report.getitemdatetime(ll_row, "scadenza")
	ld_importo_scadenza = dw_report.getitemnumber(ll_row, "importo_scadenza")
	ld_imponibile_provv_scadenza = dw_report.getitemnumber(ll_row, "imponibile_provv_scadenza")
	ld_importo_provvigione = dw_report.getitemnumber(ll_row, "importo_provvigione")
	ldt_data_rif_maturazione = dw_report.getitemdatetime(ll_row, "data_rif_maturazione")
	ls_esito_pagamento = dw_report.getitemstring(ll_row, "esito_pagamento")
	ld_totale_documento = dw_report.getitemnumber(ll_row, "totale_documento")
	ll_id_scadenza_impresa = dw_report.getitemnumber(ll_row, "id_scadenza_impresa")

	select max(progressivo) + 1
	into :ll_progressivo
	from det_storico_provvigioni
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il calcolo del progressivo dettaglio", sqlca)
		return false
	elseif isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	end if

	insert into det_storico_provvigioni (
		cod_azienda,
		anno_registrazione,
		num_registrazione,
		progressivo,
		cod_agente,
		des_agente,
		periodo_report,
		cod_cliente,
		des_cliente,
		rag_soc_1,
		tipo_documento,
		anno_documento,
		num_documento,
		data_documento,
		imponibile_documento,
		importo_provvigioni,
		scadenza,
		importo_scadenza,
		imponibile_provv_scadenza,
		importo_provvigione,
		data_rif_maturazione,
		esito_pagamento,
		totale_documento,
		id_scadenza_impresa
	) values (
		:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione, 
		:ll_progressivo, 
		:ls_cod_agente,
		:ls_agente_rag_soc_1, 
		:ls_periodo_report, 
		:ls_cod_cliente,
		:ls_des_cliente, 
		:ls_rag_soc_1,
		:ls_tipo_documento,
		:ll_anno_documento,
		:ll_num_documento,
		:ldt_data_documento,
		:ld_imponibile_documento,
		:ld_importo_provvigioni,
		:ldt_scadenza,
		:ld_importo_scadenza,
		:ld_imponibile_provv_scadenza,
		:ld_importo_provvigione,
		:ldt_data_rif_maturazione,
		:ls_esito_pagamento,
		:ld_totale_documento,
		:ll_id_scadenza_impresa
	);
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante l'inserimento della riga di dettaglio.", sqlca)
		return false
	end if
	
	// controllo tabella scadenze forzate e aggiorno le referenze se necessario
	select count(*)
	into :ll_count
	from scad_fat_ven_impresa
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		id_scadenza = :ll_id_scadenza_impresa;
		//data_scadenza = :ldt_data_documento and
	
	if sqlca.sqlcode <0 then
		g_mb.error("Errore durante il controllo delle date forzate", sqlca)
		return false
	elseif sqlca.sqlcode = 0 and ll_count > 0 then
		
		update scad_fat_ven_impresa
		set 
			anno_reg_det_storico_prov = :ll_anno_registrazione,
			num_reg_det_storico_prov = :ll_num_registrazione,
			prog_reg_det_storico_prov = :ll_progressivo
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			id_scadenza = :ll_id_scadenza_impresa;
			//data_scadenza = :ldt_data_documento and
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante l'aggiornamento delle data forzata", sqlca)
			return false
		end if
		
	end if
	// ---

next

return true
end function

public subroutine wf_carica_dw_storico ();dw_sel_storico.retrieve(s_cs_xx.cod_azienda)
end subroutine

public subroutine wf_stampa_storico (long al_anno_registrazione, long al_num_registrazione);/**
 * Crea il report usando i dati dello storico
 **/

string ls_cod_agente, ls_des_agente, ls_periodo_report, ls_cod_cliente, ls_des_cliente, ls_tipo_documento, ls_esito_pagamento, ls_rag_soc_1, ls_flag_tipo_riga
long ll_progressivo, ll_anno_documento, ll_num_documento, ll_row
datetime ldt_data_documento, ldt_scadenza, ldt_data_rif_maturazione
decimal{4} ld_imponibile_documento, ld_importo_provvigioni, ld_importo_scadenza, ld_imponibile_provv_scadenza, ld_totale_documento, ld_importo_provvigione


wf_log_dw("Elaborazione report in corso... attendere !")
dw_report.reset()
dw_report.setredraw(false)

declare cu_det_storico_provvigioni cursor for 
select 
	progressivo,
	cod_agente,
	des_agente,
	periodo_report,
	cod_cliente,
	des_cliente,
	rag_soc_1,
	tipo_documento,
	anno_documento,
	num_documento,
	data_documento,
	imponibile_documento,
	importo_provvigioni,
	scadenza,
	importo_scadenza,
	imponibile_provv_scadenza,
	importo_provvigione,
	data_rif_maturazione,
	esito_pagamento,
	totale_documento,
	flag_tipo_riga
from det_storico_provvigioni
where
	cod_azienda = :s_Cs_xx.cod_azienda and
	anno_registrazione = :al_anno_registrazione and
	num_registrazione = :al_num_registrazione
order by cod_cliente;
  
open cu_det_storico_provvigioni;
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in OPEN cursore cu_det_storico_provvigioni", sqlca)
	return
end if


do while true
	fetch cu_det_storico_provvigioni into :ll_progressivo, :ls_cod_agente, :ls_des_agente, :ls_periodo_report, :ls_cod_cliente, :ls_des_cliente, :ls_rag_soc_1, 
													:ls_tipo_documento, :ll_anno_documento, :ll_num_documento, :ldt_data_documento, :ld_imponibile_documento, 
													:ld_importo_provvigioni, :ldt_scadenza, :ld_importo_scadenza, :ld_imponibile_provv_scadenza, :ld_importo_provvigione,
													:ldt_data_rif_maturazione, :ls_esito_pagamento, :ld_totale_documento, :ls_flag_tipo_riga;

	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.error("Errore in lettura dettaglio storico.", sqlca)
		exit
	end if
	

	ll_row = dw_report.insertrow(0)
	dw_report.setitem(ll_row, "agente_rag_soc_1", ls_des_agente)
	dw_report.setitem(ll_row, "periodo_report", ls_periodo_report)
	dw_report.setitem(ll_row, "des_cliente", ls_des_cliente)
	dw_report.setitem(ll_row, "cod_cliente", ls_cod_cliente)
	dw_report.setitem(ll_row, "rag_soc_1", ls_rag_soc_1)
	dw_report.setitem(ll_row, "tipo_documento", ls_tipo_documento)
	dw_report.setitem(ll_row, "anno_documento", ll_anno_documento)
	dw_report.setitem(ll_row, "num_documento", ll_num_documento)
	dw_report.setitem(ll_row, "data_documento", ldt_data_documento)
	dw_report.setitem(ll_row, "imponibile_documento", ld_imponibile_documento)
	dw_report.setitem(ll_row, "importo_provvigioni", ld_importo_provvigioni)
	dw_report.setitem(ll_row, "scadenza", ldt_scadenza)
	dw_report.setitem(ll_row, "importo_scadenza", ld_importo_scadenza)
	dw_report.setitem(ll_row, "imponibile_provv_scadenza", ld_imponibile_provv_scadenza)
	dw_report.setitem(ll_row, "importo_provvigione", ld_importo_provvigione)
	dw_report.setitem(ll_row, "data_rif_maturazione", ldt_data_rif_maturazione)
	dw_report.setitem(ll_row, "esito_pagamento", ls_esito_pagamento)
	dw_report.setitem(ll_row, "totale_documento", ld_totale_documento)
	dw_report.setitem(ll_row, "flag_tipo_riga", ls_flag_tipo_riga)
	
	
loop

close cu_det_storico_provvigioni;

dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.orientation = '1'
dw_report.modify("t_stampa_def.Text='Stampa consolidata'")

dw_report.setredraw(true)

dw_folder.fu_selecttab(2)
dw_report.change_dw_current()
end subroutine

public function boolean wf_controlla_periodo_report ();/**
 * stefanop
 * 31/05/2011
 *
 * Controlla il periodo del report inserito che non deve essere già storicizzato
 * supponiamo di aver storicizzato dal 01/10/2010 al 31/12/2010 il report potrà essere fatto dal 01/01/2011 in poi
 **/
 
string ls_cod_agente, ls_des_agente, ls_des_periodo
long ll_count
date ldt_data_fine, ldt_data_inizio

ls_cod_agente = dw_ext_sel_provvigioni.getitemstring(1, "cod_agente")
ldt_data_inizio =dw_ext_sel_provvigioni.getitemdate(1, "data_inizio")
	
if dw_ext_sel_provvigioni.getitemstring(1, "flag_definitiva") = "S" then
	
	// avviso se non esiste almeno uno storicizzazione
	select count(*)
	into :ll_count
	from tes_storico_provvigioni
		left outer join det_storico_provvigioni on 
			det_storico_provvigioni.cod_azienda = tes_storico_provvigioni.cod_azienda and
			det_storico_provvigioni.anno_registrazione = tes_storico_provvigioni.anno_registrazione and
			det_storico_provvigioni.num_registrazione = tes_storico_provvigioni.num_registrazione
	where
		tes_storico_provvigioni.cod_azienda = :s_cs_xx.cod_azienda and
		tes_storico_provvigioni.data_inizio <= :ldt_data_inizio and
		det_storico_provvigioni.cod_agente = :ls_cod_agente;
	
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo degli storici per l'agente selezionato.", sqlca)
		return false
	elseif sqlca.sqlcode = 100 or (sqlca.sqlcode = 0 and (isnull(ll_count) or ll_count < 1)) then
		if not g_mb.confirm("Attenzione, l'agente selezionato non ha nessun storico, procedo lo stesso?") then
			return false
		end if
	end if
	// ---
	
	// stefanop : 04/01/2012
	// Ricommento questo codice, perchè già stsato fatto ma si è perso chissa dove.
	// NON bigosna fare nessun controllo sulle date, altrimenti non è possibile considerare
	// le vecchie fattura pagate successivamente al periodo già storicizzato
	
	/*select distinct data_fine, des_periodo
	into :ldt_data_fine, :ls_des_periodo
	from tes_storico_provvigioni
		join det_storico_provvigioni on
			det_storico_provvigioni.cod_azienda = tes_storico_provvigioni.cod_azienda and
			det_storico_provvigioni.anno_registrazione = tes_storico_provvigioni.anno_registrazione and
			det_storico_provvigioni.num_registrazione =tes_storico_provvigioni.num_registrazione
	where
		tes_storico_provvigioni.cod_azienda = :s_cs_xx.cod_azienda and
		det_storico_provvigioni.cod_agente = :ls_cod_agente;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo dei limiti del periodo", sqlca)
		return false
	elseif ldt_data_fine > ldt_data_inizio then
		g_mb.show("Il periodo impostato per l'agente corrente è già stato storicizzato.~r~nInserire come data inzio una data superiore al " + string(ldt_data_fine, "dd/mm/yyyy"))
		return false
	end if*/
		
end if

return true
end function

public function integer wf_calcola_provvigioni (string fs_cod_agente, string fs_cod_documento, datetime fd_data_inizio, datetime fd_data_fine, datetime fd_data_maturazione, long fn_num_doc_inizio, long fn_num_doc_fine, string fs_flag_provv_zero, string fs_cod_cliente, datetime fdt_data_scadenza_inizio, datetime fdt_data_scadenza_fine, string fs_cod_tipo_anagrafica);string ls_sql_1, ls_cod_cliente,ls_cod_valuta,ls_cod_documento,ls_numeratore_documento,ls_rag_soc_1,ls_cod_capoconto, ls_cod_cliente_impresa, &
         ls_cod_agente_1,ls_cod_agente_2, ls_cod_tipo_fat_ven,ls_rag_soc_cliente,ls_rag_soc_agente, ls_flag_tipo_fat_ven, ls_formato,ls_flag_esito, &
		ls_rag_soc_cliente_f, ls_cod_cliente_where, ls_cod_cliente_old, ls_query, ls_des_report
		
long   ll_anno_registrazione, ll_num_registrazione,ll_anno_documento,ll_num_documento, ll_precisione, ll_riga, ll_ret, ll_i, ll_rows, ll_cont_scadenza, &
		ll_query_count, ll_id_scadenza, ll_find, ll_id_scadenza_origine,ll_id_scadenza_padr, ll_id_scadenza_impresa
		
dec{4}    ld_cambio_ven, ld_precisione, ld_imponibile_iva, ld_importo_provvigioni_1, ld_importo_scadenza, ld_imponibile_provv_scadenza, ld_importo_provvigione,&
		 	ld_totale_documento,ld_imponibile_provvigioni_1, ld_percentuale_incidenza, ld_imponibile_provvigione_scadenza, ld_provvigione_scadenza,&
			ld_importo_scadenza_dare, ld_importo_scadenza_avere, ld_saldo_scadenza,  &
			ld_importo_provvigioni_2, ld_imponibile_provvigioni_2, ld_importo_provvigioni_agente, ld_imponibile_provvigioni_agente, &
			ld_importo_scadenza_impresa
			
datetime ldt_data_fattura, ldt_scadenza, ldt_data_rif_maturato, ldt_data_scadenza, ldt_data_forzato_pagato

datastore lds_scadenzario, lds_saldo_scadenze_fattura

uo_impresa luo_impresa





ls_des_report = dw_ext_sel_provvigioni.getitemstring(1, "des_report")
if isnull(fs_cod_documento) then fs_cod_documento = "%"

if fs_cod_cliente <> "" then
	select rag_soc_1
	into :ls_rag_soc_cliente_f
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :fs_cod_cliente;
	
	ls_cod_cliente_where = "'" + fs_cod_cliente + "%'"
else
	ls_cod_cliente_where = "'%'"
	ls_rag_soc_cliente_f = ""
end if

select rag_soc_1
into  :ls_rag_soc_agente
from anag_agenti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_agente = :fs_cod_agente;

if isnull(ls_rag_soc_agente) then ls_rag_soc_agente = "Nessun Agente Peimpostato"

DECLARE cu_tes_fat_ven DYNAMIC CURSOR FOR SQLSA ;

// faccio un ciclo for per eseguire il controllo su due tabelle diverse
// faccio un for per recuperare tutto il codice senza doverlo scrivere due volte
for ll_query_count = 1 to 2
	
	// imposto la prima query (originale)
	wf_get_sqlquery(ll_query_count, ls_query)
	
	if not isnull(fs_cod_agente) and fs_cod_agente <> "" then
		ls_query += " AND (tes_fat_ven.cod_agente_1 = '" + fs_cod_agente + "'or tes_fat_ven.cod_agente_2 ='"+fs_cod_agente+"')  "
	end if
	
	ls_query += " AND tes_fat_ven.data_fattura between " + f_data_sql(fd_data_inizio) +" and " + f_data_sql(fd_data_fine)
	ls_query += " AND tes_fat_ven.num_documento > 0"
	
	if not isnull(fn_num_doc_inizio) then
		ls_query += " AND tes_fat_ven.num_documento >= " + string(fn_num_doc_inizio)
	end if
	
	if not isnull(fn_num_doc_fine) then
		ls_query += " AND tes_fat_ven.num_documento <= " + string(fn_num_doc_fine)
	end if
	
	if not isnull(fs_cod_documento) and fs_cod_documento <> "" then
		ls_query += " AND tes_fat_ven.cod_documento is not null and tes_fat_ven.cod_documento like '%" + fs_cod_documento + "%'"
	end if
	
	if not isnull(ls_cod_cliente_where) and ls_cod_cliente_where <> "" then
		ls_query += " AND tes_fat_ven.cod_cliente like " + ls_cod_cliente_where
	end if
	
	if not isnull(fs_cod_tipo_anagrafica) and fs_cod_tipo_anagrafica <> "" then
		ls_query += " AND anag_clienti.cod_tipo_anagrafica ='" + fs_cod_tipo_anagrafica + "'"
	end if
			
	if ll_query_count = 2 then
		
		//return -1
		ls_query += " AND scad_fat_ven_impresa.data_forzato_pagato between " + f_data_sql(fd_data_inizio) +" and " + f_data_sql(fd_data_fine)
		
	end if
	
	ls_query += " ORDER BY tes_fat_ven.cod_cliente, anno_documento, num_documento"
			
	// preparo il cursore
	close cu_tes_fat_ven;
	PREPARE SQLSA FROM :ls_query;
	OPEN DYNAMIC cu_tes_fat_ven;
	
	//open cu_tes_fat_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in OPEN cursore cu_tes_fat_ven " + sqlca.sqlerrtext)
		return -1
	end if
	
	lds_scadenzario = create datastore
	lds_scadenzario.dataobject = 'd_ds_report_provvigioni_maturato'
	lds_scadenzario.settransobject(sqlci)
	ls_cod_cliente_old = '-'
	
	do while true
		if ll_query_count = 1 then
			
			fetch cu_tes_fat_ven into :ll_anno_registrazione, :ll_num_registrazione, :ls_cod_cliente, :ld_cambio_ven, :ls_cod_valuta, 
											:ls_cod_documento, :ls_numeratore_documento, :ll_anno_documento, :ll_num_documento, :ldt_data_fattura, 
											:ls_cod_agente_1, :ls_cod_agente_2, :ls_cod_tipo_fat_ven, :ld_imponibile_iva, :ld_imponibile_provvigioni_1, 
											:ld_importo_provvigioni_1, :ld_imponibile_provvigioni_2, :ld_importo_provvigioni_2, :ld_totale_documento;
		
		elseif ll_query_count = 2 then
			
			fetch cu_tes_fat_ven into :ll_anno_registrazione, :ll_num_registrazione, :ls_cod_cliente, :ld_cambio_ven, :ls_cod_valuta, 
											:ls_cod_documento, :ls_numeratore_documento, :ll_anno_documento, :ll_num_documento, :ldt_data_fattura, 
											:ls_cod_agente_1, :ls_cod_agente_2, :ls_cod_tipo_fat_ven, :ld_imponibile_iva, :ld_imponibile_provvigioni_1, 
											:ld_importo_provvigioni_1, :ld_imponibile_provvigioni_2, :ld_importo_provvigioni_2, :ld_totale_documento, :ll_id_scadenza;
			
		end if
		
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore in lettura testata fatture di vendita. Dettaglio errore: " + sqlca.sqlerrtext)
			exit
		end if
		
		if isnull(ld_importo_provvigioni_1) then ld_importo_provvigioni_1 = 0
		if isnull(ld_importo_provvigioni_2) then ld_importo_provvigioni_2 = 0
		if isnull(ld_totale_documento) then ld_totale_documento = 0
		if isnull(ld_imponibile_provvigioni_1) then ld_imponibile_provvigioni_1 = 0
		if isnull(ld_imponibile_provvigioni_2) then ld_imponibile_provvigioni_2 = 0
		if isnull(ld_imponibile_iva) then ld_imponibile_iva = 0
		
		ld_importo_provvigioni_agente = 0
		ld_imponibile_provvigioni_agente = 0
		
		if fs_cod_agente = ls_cod_agente_1 then
			ld_importo_provvigioni_agente = ld_importo_provvigioni_1
			ld_imponibile_provvigioni_agente = ld_imponibile_provvigioni_1
		else
			ld_importo_provvigioni_agente = ld_importo_provvigioni_2
			ld_imponibile_provvigioni_agente = ld_imponibile_provvigioni_2
		end if
		
		wf_log_dw(string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		
		select flag_tipo_fat_ven
		into   :ls_flag_tipo_fat_ven
		from   tab_tipi_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca tipo documento in tabella tab_tipi_fat_ven; proseguo ugualmente.~r~n" + sqlca.sqlerrtext)
			ls_flag_tipo_fat_ven = "I"
		end if
		
		select precisione, formato
		into   :ld_precisione, :ls_formato
		from   tab_valute
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_valuta = :ls_cod_valuta;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in select da tabella valute")
			close cu_tes_fat_ven;
			return 0
		end if
		
		choose case ld_precisione
			case 1
				ll_precisione = 0
			case 0.1
				ll_precisione = 1
			case 0.01 
				ll_precisione = 2
			case 0.001 
				ll_precisione = 3
			case 0.0001 
				ll_precisione = 4
			case 0.00001 
				ll_precisione = 5
			case else
				ll_precisione = 0
		end choose
		
		
		select rag_soc_1, cod_capoconto
		into    :ls_rag_soc_1, :ls_cod_capoconto
		from  anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :ls_cod_cliente;
				
		
		// prelevo i dati delle scadenze maturate da impresa		 
		luo_impresa = create uo_impresa
		ls_cod_cliente_impresa = luo_impresa.uof_codice_cli_for("C", ls_cod_capoconto, ls_cod_cliente)
		destroy luo_impresa
		
		
		lds_saldo_scadenze_fattura = create datastore
		
		if ll_query_count = 1 then
			lds_saldo_scadenze_fattura.dataobject = 'd_ds_report_provvigioni_maturato_saldo_scad'
			
			lds_saldo_scadenze_fattura.settransobject(sqlci)
		
			ll_rows = lds_saldo_scadenze_fattura.retrieve(ls_cod_cliente_impresa, fd_data_maturazione,string(ll_num_documento),ldt_data_fattura)
		
		elseif ll_query_count = 2 then
			lds_saldo_scadenze_fattura.dataobject = 'd_ds_report_provvigioni_maturato_saldo_scad_2'
			
			lds_saldo_scadenze_fattura.settransobject(sqlci)
		
			ll_rows = lds_saldo_scadenze_fattura.retrieve(ll_id_scadenza)
		end if
		
	
		if ll_rows < 0 then
			g_mb.messagebox("APICE","Errore in generazione vista - lds_saldo_scadenze_fattura ")
			close cu_tes_fat_ven;
			destroy lds_saldo_scadenze_fattura
			return -1
		end if
	
		for ll_i = 1 to ll_rows
			
			ldt_data_scadenza = lds_saldo_scadenze_fattura.getitemdatetime(ll_i, 1)
			ld_importo_scadenza_dare = lds_saldo_scadenze_fattura.getitemnumber(ll_i, 2)
			ld_importo_scadenza_avere = lds_saldo_scadenze_fattura.getitemnumber(ll_i, 3)
			ld_saldo_scadenza = lds_saldo_scadenze_fattura.getitemnumber(ll_i, 4)
			ll_id_scadenza_impresa =  lds_saldo_scadenze_fattura.getitemnumber(ll_i, 5)
			ll_id_scadenza_origine = lds_saldo_scadenze_fattura.getitemnumber(ll_i, 6)
			
			if isnull(ld_importo_scadenza_dare) then ld_importo_scadenza_dare = 0
			if isnull(ld_importo_scadenza_avere) then ld_importo_scadenza_avere = 0
			if isnull(ld_saldo_scadenza) then ld_saldo_scadenza = 0

			// controllo che la scadenza non sia ri-emessa cioè che esista un'altra scadeza con id_scadenza_padre uguale a id_scadenza in attuale
			// per il controllo uso il datastore, che ha già tutte le righe delle scadenze caricate, eliminando query aggiuntive
			ll_find = lds_saldo_scadenze_fattura.find("id_scadenza_origine=" + string(ll_id_scadenza_impresa), 0, lds_saldo_scadenze_fattura.rowcount())
			if ll_find > 0 then
				continue
			end if
			
			// --- Passata questa fase devo calcolare il saldo della scadenza togliendo eventuali saldi dare/avere derivanti da movimenti manageriali
			// 		Richiesta da AntoB 06/07/2012: le manageriali non devono influenzare le provvigioni.
			dec{4} ld_dare_manageriali, ld_avere_manageriali, ld_saldo_manageriali
			
			select sum(isnull(mov_Scadenza.dare,0) ), sum( isnull(mov_Scadenza.avere,0) ), sum(isnull(mov_Scadenza.dare,0) ) - sum( isnull(mov_Scadenza.avere,0) ) as SALDO_MANAGER
			into	:ld_dare_manageriali, :ld_avere_manageriali, :ld_saldo_manageriali
			from mov_scadenza
				join scadenza
				on mov_Scadenza.id_scadenza = scadenza.id_Scadenza
				join conto
				on scadenza.id_conto = conto.id_conto
				join mov_contabile
				on mov_scadenza.id_mov_contabile = mov_contabile.id_mov_contabile
				join reg_pd
				on mov_contabile.id_reg_pd = reg_pd.id_reg_pd
				join registro
				on reg_pd.id_registro = registro.id_registro
			where 
				registro.tipo_registro = 'MN'	 and scadenza.id_scadenza=:ll_id_scadenza_impresa 
			using sqlci;
			
			if sqlci.sqlcode <> 0 then
				rollback;
				g_mb.error("ERRORE in fase di ricerca saldo movimenti MANAGERIALI in mov_scadenza~r~n" + sqlci.sqlerrtext)
				return -1
			end if
			
			//				and mov_scadenza.data <= :fd_data_maturazione
			
			if isnull(ld_dare_manageriali) then ld_dare_manageriali = 0
			if isnull(ld_avere_manageriali) then  ld_avere_manageriali = 0
			
			ld_importo_scadenza_dare -= ld_dare_manageriali
			ld_importo_scadenza_avere -= ld_avere_manageriali
			ld_saldo_scadenza = ld_importo_scadenza_dare - ld_importo_scadenza_avere
// --
			// considero solo le scadenze con saldo ZERO
			if ld_saldo_scadenza <> 0 then continue
			
			// attenzione, il controllo sulle date va fatto solo il primo giro, quando si va a controllare i "veri" re ord all'inrterno delle fatture
			// la seconda volta si controlla quelle forzate e quindi il controllo delle date salta sempe!
			
			if ll_query_count = 1 then
				if ldt_data_scadenza < fdt_data_scadenza_inizio  then continue
			
				if ldt_data_scadenza > fdt_data_scadenza_fine   then continue
			
			end if
			
			// EnMe 06/07/2012
			// ----------------------------------
			select sum( isnull(scadenza.dare,0) -  isnull(scadenza.avere,0) )
			into :ld_importo_scadenza_impresa
			from scadenza
			where id_scadenza = :ll_id_scadenza_impresa
			using sqlci;
			
			if sqlci.sqlcode <> 0 then
				rollback;
				g_mb.error("ERRORE in fase di ricerca saldo scadenza in tabella scadenza~r~n" + sqlci.sqlerrtext)
				return -1
			end if
			
			// stefanop 26/06/2012 controllo se è stata impostata una data scadenza forzata
			// -----------------------------------
			select data_forzato_pagato
			into :ldt_data_forzato_pagato
			from scad_fat_ven_impresa
			where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						id_scadenza = :ll_id_scadenza_impresa and
						anno_reg_det_storico_prov is null and
						num_reg_det_storico_prov is null;
						
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante il controllo delle date forzate", sqlca)
				return -1
			elseif sqlca.sqlcode = 0 and not isnull(ldt_data_forzato_pagato) then
				
				if ldt_data_forzato_pagato < fdt_data_scadenza_inizio  then continue
		
				if ldt_data_forzato_pagato > fdt_data_scadenza_fine   then continue
				
				// controllo che la scadenza non sia ri-emessa cioè che esista un'altra scadeza con id_scadenza_padre uguale a id_scadenza in attuale
				// per il controllo uso il datastore, che ha già tutte le righe delle scadenze caricate, eliminando query aggiuntive
				ll_find = dw_report.find("id_scadenza_impresa=" + string(ll_id_scadenza_impresa), 1, dw_report.rowcount())
				if ll_find > 0 then
					continue
				end if
				// ---
				
			end if
			// -----------------------------------
			
			if fs_flag_provv_zero ="N" and ld_importo_provvigioni_agente = 0 then continue
			
			// Controllo che la scandenza non sia già stata portata in un report precedente!
			// EnMe 06-07-2012: devo controllare anche l'agente perchè potrebbe essere che la scadenza vada pagata a 2 agenti come provv_1 e provv_2
			select count(*)
			into :ll_cont_scadenza
			from det_storico_provvigioni
			where cod_azienda = :s_cs_xx.cod_azienda and 
					cod_agente = :fs_cod_agente and
					id_scadenza_impresa = :ll_id_scadenza_impresa;
				
			if ll_cont_scadenza > 0 then continue
			// ----
			
			ll_riga = dw_report.insertrow(0)	
			
			if not isnull(ls_des_report) and  ls_des_report <> "" then
				dw_report.setitem(ll_riga, "periodo_report", ls_des_report)
			else
				dw_report.setitem(ll_riga, "periodo_report", "PERIODO RIFERIMENTO " + string(fd_data_inizio,"dd/mm/yyyy") + ' - ' + string(fd_data_fine,"dd/mm/yyyy") )
			end if
			
			dw_report.setitem(ll_riga, "agente_rag_soc_1", "AGENTE: " + ls_rag_soc_agente  )
			dw_report.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
			dw_report.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
			
			//if ls_cod_cliente <> ls_cod_cliente_old then
				dw_report.setitem(ll_riga, "des_cliente", ls_cod_cliente + " - " + ls_rag_soc_1)
			//end if
			
			ls_cod_cliente_old = ls_cod_cliente
			
			dw_report.setitem(ll_riga, "tipo_documento", ls_flag_tipo_fat_ven)
			dw_report.setitem(ll_riga, "anno_documento", ll_anno_documento)
			dw_report.setitem(ll_riga, "num_documento", ll_num_documento)
			dw_report.setitem(ll_riga, "data_documento", ldt_data_fattura)
			dw_report.setitem(ll_riga, "totale_documento", wf_negativo(ls_flag_tipo_fat_ven, ld_totale_documento))
			dw_report.setitem(ll_riga, "imponibile_documento", wf_negativo(ls_flag_tipo_fat_ven, ld_imponibile_iva))
			dw_report.setitem(ll_riga, "importo_provvigioni", wf_negativo(ls_flag_tipo_fat_ven, ld_importo_provvigioni_agente))
			
			dw_report.setitem(ll_riga, "scadenza", ldt_data_scadenza)
//			ld_importo_scadenza = ld_importo_scadenza_dare - ld_importo_scadenza_avere
			ld_importo_scadenza =ld_importo_scadenza_impresa
			// Importo ne
			dw_report.setitem(ll_riga, "importo_scadenza", ld_importo_scadenza )
			
			// calcolo l'imponibile provvigioni come proporzione su imponibile_fatt / 
			// per prima cosa calcolo l'incidenza percentuale della scadenza sulla fattura

			// EnMe 09/07/2012; caso in cui il documento è ZERO.
			if ld_totale_documento <> 0 then
				ld_percentuale_incidenza = ld_importo_scadenza * 100 / ld_totale_documento
				ld_imponibile_provvigione_scadenza = round(ld_imponibile_provvigioni_agente / 100 * ld_percentuale_incidenza, 2)
				dw_report.setitem(ll_riga, "imponibile_provv_scadenza", ld_imponibile_provvigione_scadenza )
				
				// poi con la stessa logica spartisco la provvigione
				ld_provvigione_scadenza = round(ld_importo_provvigioni_agente / 100 * ld_percentuale_incidenza, 2)
				dw_report.setitem(ll_riga, "importo_provvigione", ld_provvigione_scadenza )
				dw_report.setitem(ll_riga, "id_scadenza_impresa", ll_id_scadenza_impresa)
			else
				// caso documento a zero
				dw_report.setitem(ll_riga, "imponibile_provv_scadenza", ld_imponibile_provvigioni_agente )
				dw_report.setitem(ll_riga, "importo_provvigione", ld_importo_provvigioni_agente )
				dw_report.setitem(ll_riga, "id_scadenza_impresa", ll_id_scadenza_impresa)
			end if
			
			dw_report.setitem(ll_riga, "flag_tipo_riga", 'N')
		next
		
		destroy lds_saldo_scadenze_fattura
	loop
		
	destroy lds_scadenzario
	close cu_tes_fat_ven;

next

dw_report.sort()
dw_report.groupcalc()

dw_folder.fu_selecttab(2)
dw_report.change_dw_current()

return 0
end function

public subroutine wf_get_sqlquery (integer ai_querycount, ref string as_sqlquery);

as_sqlquery = "select  &
		tes_fat_ven.anno_registrazione, &
		tes_fat_ven.num_registrazione, &
		tes_fat_ven.cod_cliente, &
		tes_fat_ven.cambio_ven, &
		tes_fat_ven.cod_valuta, &
		tes_fat_ven.cod_documento,  &
		tes_fat_ven.numeratore_documento,  &
		tes_fat_ven.anno_documento,  &
		tes_fat_ven.num_documento, &
		tes_fat_ven.data_fattura, &
		tes_fat_ven.cod_agente_1, &
		tes_fat_ven.cod_agente_2, &
		tes_fat_ven.cod_tipo_fat_ven, &
		tes_fat_ven.imponibile_iva, &
		tes_fat_ven.imponibile_provvigioni_1, &
		tes_fat_ven.tot_provvigioni_1, &
		tes_fat_ven.imponibile_provvigioni_2, &
		tes_fat_ven.tot_provvigioni_2, &
		tes_fat_ven.tot_fattura "
if ai_querycount = 2 then
	as_sqlquery += ", scad_fat_ven_impresa.id_scadenza "
end if

as_sqlquery += " from tes_fat_ven &
		left outer join anag_clienti on  &
			anag_clienti.cod_azienda = tes_fat_ven.cod_azienda and &
			anag_clienti.cod_cliente = tes_fat_ven.cod_cliente "

// se sono al secondo giro devo includere in join la tabella delle scadenze forzate
if ai_querycount = 2 then
	as_sqlquery += " left outer join scad_fat_ven_impresa on &
		scad_fat_ven_impresa.cod_azienda = tes_fat_ven.cod_azienda and &
		scad_fat_ven_impresa.anno_registrazione = tes_fat_ven.anno_registrazione and &
		scad_fat_ven_impresa.num_registrazione = tes_fat_ven.num_registrazione " 
end if

as_sqlquery += " where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "'"
end subroutine

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if not g_mb.confirm("Memorizza l'attuale impostazione dei filtro come predefinito?") then return

ls_colcount = dw_ext_sel_provvigioni.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_ext_sel_provvigioni.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_ext_sel_provvigioni.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_ext_sel_provvigioni.getitemdatetime(1, ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_ext_sel_provvigioni.getitemstring(1, ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_ext_sel_provvigioni.getitemnumber(1, ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'PROVV';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'PROVV';
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore in memorizzazione impostazioni predefinite filtri di ricerca.", sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'PROVV');
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore in memorizzazione impostazioni predefinite filtri di ricerca", sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_ext_sel_provvigioni.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'PROVV';
if sqlca.sqlcode = 100 then
	//g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_ext_sel_provvigioni.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_ext_sel_provvigioni.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_ext_sel_provvigioni.setitem(dw_ext_sel_provvigioni.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_ext_sel_provvigioni.setitem(dw_ext_sel_provvigioni.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_ext_sel_provvigioni.setitem(dw_ext_sel_provvigioni.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public subroutine wf_des_report ();string ls_cod_agente, ls_des_agente, ls_des_report
date ldt_data_maturazione

dw_ext_sel_provvigioni.accepttext()
ls_cod_agente = dw_ext_sel_provvigioni.getitemstring(1, "cod_agente")
ldt_data_maturazione = dw_ext_sel_provvigioni.getitemdate(1, "data_maturazione")

ls_des_report = ""

if not isnull(ldt_data_maturazione) then
	ls_des_report += "PERIODO MATURAZIONE: " + string(ldt_data_maturazione, "dd/mm/yyyy")
end if 

if not isnull(ls_cod_agente) and ls_cod_agente <> "" then
	ls_des_agente = f_des_tabella("anag_agenti", "cod_agente='" + ls_cod_agente + "'", "rag_soc_1")
	
	if ls_des_report <> "" then ls_des_report += " - "
	
	ls_des_report += "AGENTE: " + ls_des_agente
end if

dw_ext_sel_provvigioni.setitem(1, "des_report", ls_des_report)
end subroutine

public function decimal wf_negativo (string as_flag_tipo_fat_ven, decimal ad_number);if as_flag_tipo_fat_ven = "N" then
	return ad_number * -1
end if

return ad_number
	
end function

on w_report_provvigioni_maturato.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_sel_storico=create dw_sel_storico
this.pb_1=create pb_1
this.pb_2=create pb_2
this.cb_stampa_provvisorio=create cb_stampa_provvisorio
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_ext_sel_provvigioni=create dw_ext_sel_provvigioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_sel_storico
this.Control[iCurrent+3]=this.pb_1
this.Control[iCurrent+4]=this.pb_2
this.Control[iCurrent+5]=this.cb_stampa_provvisorio
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_ext_sel_provvigioni
end on

on w_report_provvigioni_maturato.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_sel_storico)
destroy(this.pb_1)
destroy(this.pb_2)
destroy(this.cb_stampa_provvisorio)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_ext_sel_provvigioni)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_sel_provvigioni,"cod_agente",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1", &
                 "(anag_agenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_agenti.flag_blocco <> 'S') or (anag_agenti.flag_blocco = 'S' and anag_agenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )

f_PO_LoadDDDW_DW(dw_ext_sel_provvigioni,"cod_documento",sqlca,&
                 "tab_documenti","cod_documento","des_documento", &
                 "(tab_documenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_documenti.flag_blocco <> 'S') or (tab_documenti.flag_blocco = 'S' and tab_documenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )
					  
					  
// stefanop 05/05/2011
f_po_loaddddw_dw(dw_ext_sel_provvigioni, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_anagrafica = 'C' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
// ----
end event

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

windowobject l_objects[], lwo_empty[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_ext_sel_provvigioni.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_noresizedw + c_nohighlightselected + c_cursorrowpointer)

dw_sel_storico.settransobject(sqlca)

iuo_dw_main = dw_report

l_objects[1] = dw_ext_sel_provvigioni
l_objects[2] = dw_sel_storico
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

l_objects[] = lwo_empty[]
l_objects[1] = dw_report
l_objects[2] = pb_1
l_objects[3] = pb_2
l_objects[4] = cb_stampa_provvisorio
dw_folder.fu_assigntab(2, "Report", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_sel_storico.visible = false

wf_leggi_filtro()

wf_carica_dw_storico()
end event

event resize;/** TOLTO ANCESTOR SCRPIT **/

dw_folder.move(20,20)
dw_folder.resize(newwidth - 20, newheight - 20)

dw_ext_sel_provvigioni.move(dw_folder.x + 40, dw_folder.y + 120)
dw_sel_storico.move(dw_ext_sel_provvigioni.x, dw_ext_sel_provvigioni.y + dw_ext_sel_provvigioni.height + 40)
pb_2.y = dw_ext_sel_provvigioni.y
pb_1.y = dw_ext_sel_provvigioni.y
cb_stampa_provvisorio.y = dw_ext_sel_provvigioni.y
dw_report.move(dw_ext_sel_provvigioni.x, cb_stampa_provvisorio.y + cb_stampa_provvisorio.height + 20)

dw_report.resize(dw_folder.width - dw_folder.x - 80, dw_folder.height - cb_stampa_provvisorio.y - 140)
end event

type cb_1 from commandbutton within w_report_provvigioni_maturato
integer x = 137
integer y = 840
integer width = 402
integer height = 112
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

type dw_sel_storico from datawindow within w_report_provvigioni_maturato
integer x = 46
integer y = 1000
integer width = 3451
integer height = 880
integer taborder = 60
string title = "none"
string dataobject = "d_sel_provvigioni_storico"
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event clicked;choose case dwo.name
		
	case "b_report"
		wf_stampa_storico(getitemnumber(row, "anno_registrazione"), getitemnumber(row, "num_registrazione"))
		
end choose
end event

type pb_1 from picturebutton within w_report_provvigioni_maturato
integer x = 183
integer y = 180
integer width = 101
integer height = 84
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
boolean originalsize = true
string picturename = "c:\cs_70\framework\risorse\suc.bmp"
alignment htextalign = left!
end type

event clicked;dw_report.scrollnextpage()
end event

type pb_2 from picturebutton within w_report_provvigioni_maturato
integer x = 69
integer y = 180
integer width = 101
integer height = 84
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
boolean originalsize = true
string picturename = "c:\cs_70\framework\risorse\prec.bmp"
alignment htextalign = left!
end type

event clicked;dw_report.scrollpriorpage()
end event

type cb_stampa_provvisorio from commandbutton within w_report_provvigioni_maturato
integer x = 297
integer y = 180
integer width = 366
integer height = 84
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_report.print()

end event

type dw_report from uo_cs_xx_dw within w_report_provvigioni_maturato
integer x = 23
integer y = 300
integer width = 4050
integer height = 1868
integer taborder = 50
string dataobject = "d_report_provvigioni_maturato"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_folder from u_folder within w_report_provvigioni_maturato
integer x = 23
integer y = 40
integer width = 4078
integer height = 2176
integer taborder = 10
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;if i_SelectedTab = 1 then
	dw_sel_storico.visible = (dw_ext_sel_provvigioni.getitemstring(1, "flag_visualizza_storico") = "S")
end if
end event

type dw_ext_sel_provvigioni from uo_cs_xx_dw within w_report_provvigioni_maturato
integer x = 46
integer y = 160
integer width = 3451
integer height = 820
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_ext_sel_provvigioni_maturato"
end type

event clicked;/** TOLTO ANCESTOR SCRIPT, MENO COSE FA IL FRAMEWORK MEGLIO E :D **/

choose case dwo.name
	case "b_report"
		wf_imposta_report()
		
	case "b_cliente"
		dw_ext_sel_provvigioni.change_dw_current()
		guo_ricerca.uof_ricerca_cliente(dw_ext_sel_provvigioni,"cod_cliente")
		
	case "b_imposta_filtro"
		wf_memorizza_filtro()
		
	case "b_leggi_filtro"
		wf_leggi_filtro()
		
	
end choose
end event

event itemchanged;call super::itemchanged;string ls_null

choose case dwo.name
		
	case "flag_visualizza_storico"
		dw_sel_storico.visible = (data = "S")
		
		
	case "flag_definitiva"
		if data = "S" then
			setnull(ls_null)
			
			setitem(row, "cod_documento", ls_null)
			setitem(row, "cod_cliente", ls_null)
			setitem(row, "cod_tipo_anagrafica", ls_null)
			setitem(row, "fattura_inizio", ls_null)
			setitem(row, "fattura_fine", ls_null)
			setitem(row, "cod_documento", ls_null)
			
		end if
		
	case "data_inizio"
		setitem(row, "data_scadenza_inizio", date(data))
		
	case "data_fine"
		setitem(row, "data_scadenza_fine", date(data))
		setitem(row, "data_maturazione", date(data))
		wf_des_report()
				
				
	case "cod_agente", "data_maturazione"
		wf_des_report()

end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ext_sel_provvigioni,"cod_cliente")
end choose
end event

