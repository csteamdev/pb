﻿$PBExportHeader$w_report_righe_bolla_euro.srw
$PBExportComments$Report righe bolle ord. per data o cliente
forward
global type w_report_righe_bolla_euro from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_righe_bolla_euro
end type
type cb_report from commandbutton within w_report_righe_bolla_euro
end type
type cb_selezione from commandbutton within w_report_righe_bolla_euro
end type
type dw_selezione from uo_cs_xx_dw within w_report_righe_bolla_euro
end type
type dw_report from uo_cs_xx_dw within w_report_righe_bolla_euro
end type
end forward

global type w_report_righe_bolla_euro from w_cs_xx_principale
integer width = 3547
integer height = 1664
string title = "Report Righe Bolla Analitico"
boolean resizable = false
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_righe_bolla_euro w_report_righe_bolla_euro

type variables
datawindow idw_righe_bolle
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2442
this.height = 681

end event

on w_report_righe_bolla_euro.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_righe_bolla_euro.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_tipo_bol_ven", &
							sqlca, &
							"tab_tipi_bol_ven", &
							"cod_tipo_bol_ven", &
                     "des_tipo_bol_ven", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_annulla from commandbutton within w_report_righe_bolla_euro
integer x = 1623
integer y = 480
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_righe_bolla_euro
integer x = 2011
integer y = 480
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_doc_da,ls_doc_a,ls_cod_cliente, ls_cod_tipo_bol_ven, &
		 ls_original_select, ls_cod_fornitore, ls_mod_string, ls_where_clause, &
		 ls_risp
integer li_riga
double ld_num_da,ld_num_a,ld_doc_da,ld_doc_a
datetime ldt_da,ldt_a

dw_selezione.accepttext()

ld_num_da = dw_selezione.getitemnumber(1,"n_doc_da")
if isnull(ld_num_da) or ld_num_da <1 then ld_num_da=1

ld_num_a = dw_selezione.getitemnumber(1,"n_doc_a")
if isnull(ld_num_a) then ld_num_a=999999

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_fornitore = dw_selezione.getitemstring(1,"cod_fornitore")

ls_cod_tipo_bol_ven = dw_selezione.getitemstring(1,"cod_tipo_bol_ven")
if isnull(ls_cod_tipo_bol_ven) or ls_cod_tipo_bol_ven = ""  then ls_cod_tipo_bol_ven = '%'

ldt_da = datetime(dw_selezione.getitemdate(1,"d_data_da"))
if isnull(ldt_da) then ldt_da = datetime(1900-01-01)
ldt_a = datetime(dw_selezione.getitemdate(1,"d_data_a"))
if isnull(ldt_a) then ldt_a = datetime(2999-12-31)

ls_original_select = dw_report.Describe("DataWindow.Table.Select")

ls_where_clause = "" 
if (not isnull(ls_cod_cliente)) then 
	ls_where_clause = "and          ( tes_bol_ven.cod_cliente = ~~'" + ls_cod_cliente + "~~' ) "
end if
if (not isnull(ls_cod_fornitore)) then 
	ls_where_clause = "and          ( tes_bol_ven.cod_fornitore = ~~'" + ls_cod_fornitore + "~~' ) "
end if

ls_mod_string = "DataWindow.Table.Select='" + ls_original_select + ls_where_clause + "'"

ls_risp = dw_report.modify(ls_mod_string)
IF ls_risp <> "" THEN
	g_mb.messagebox("Status", "Modify Failed " + ls_risp)
	return
END IF


dw_selezione.hide()


parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()
dw_report.retrieve(ldt_da, ldt_a, ld_num_da, ld_num_a, s_cs_xx.cod_azienda, ls_cod_tipo_bol_ven)

cb_selezione.show()
cb_annulla.hide()
cb_report.hide()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_righe_bolla_euro
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()
dw_report.reset()

parent.x = 741
parent.y = 885
parent.width = 2442
parent.height = 681

dw_report.hide()
cb_selezione.hide()
cb_report.show()
cb_annulla.show()
dw_selezione.object.b_ricerca_cliente.visible=true
dw_selezione.object.b_ricerca_fornitore.visible=true
dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_righe_bolla_euro
integer x = 23
integer y = 20
integer width = 2354
integer height = 420
integer taborder = 30
string dataobject = "d_cerca_righe_bolle"
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_flag_tipo_bol_ven, ls_nulla
	setnull(ls_nulla)
   choose case i_colname
		case "cod_tipo_bol_ven"
			if isnull(i_coltext) or i_coltext = "" then
				dw_selezione.object.b_ricerca_cliente.visible=true
				setitem(getrow(), "cod_cliente", ls_nulla)
				this.object.cod_cliente_t.visible=true
				this.object.cf_rag_soc_cliente.visible=true
				this.object.cod_cliente.visible=true
				dw_selezione.object.b_ricerca_fornitore.visible=true
				this.object.cod_fornitore_t.visible=true
				setitem(getrow(), "cod_fornitore", ls_nulla)
				this.object.cf_rag_soc_fornitore.visible=true
				this.object.cod_fornitore.visible=true
				
			else
			
				select tab_tipi_bol_ven.flag_tipo_bol_ven
				into   :ls_flag_tipo_bol_ven   
				from   tab_tipi_bol_ven  
				where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
						 tab_tipi_bol_ven.cod_tipo_bol_ven = :i_coltext;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi Bolle Vendita.", &
								  exclamation!, ok!)
					return
				end if
				
				if ls_flag_tipo_bol_ven = 'V' then
					dw_selezione.object.b_ricerca_cliente.visible=true
					this.object.cod_cliente_t.visible=true
					this.object.cf_rag_soc_cliente.visible=true
					this.object.cod_cliente.visible=true
					dw_selezione.object.b_ricerca_fornitore.visible=false
					this.object.cod_fornitore_t.visible=false
					setitem(getrow(), "cod_fornitore", ls_nulla)
					this.object.cf_rag_soc_fornitore.visible=false
					this.object.cod_fornitore.visible=false
				elseif ls_flag_tipo_bol_ven = 'R' then
					dw_selezione.object.b_ricerca_cliente.visible=false
					this.object.cod_cliente_t.visible=false
					setitem(getrow(), "cod_cliente", ls_nulla)
					this.object.cf_rag_soc_cliente.visible=false
					this.object.cod_cliente.visible=false
					dw_selezione.object.b_ricerca_fornitore.visible=true
					this.object.cod_fornitore_t.visible=true
					this.object.cf_rag_soc_fornitore.visible=true
					this.object.cod_fornitore.visible=true
				elseif ls_flag_tipo_bol_ven = 'T' then
					dw_selezione.object.b_ricerca_cliente.visible=false
					this.object.cod_cliente_t.visible=false
					setitem(getrow(), "cod_cliente", ls_nulla)
					this.object.cf_rag_soc_cliente.visible=false
					this.object.cod_cliente.visible=false
					dw_selezione.object.b_ricerca_fornitore.visible=false
					this.object.cod_fornitore_t.visible=false
					setitem(getrow(), "cod_fornitore", ls_nulla)
					this.object.cf_rag_soc_fornitore.visible=false
					this.object.cod_fornitore.visible=false
				elseif ls_flag_tipo_bol_ven = 'C' then
					dw_selezione.object.b_ricerca_cliente.visible=false
					this.object.cod_cliente_t.visible=false
					setitem(getrow(), "cod_cliente", ls_nulla)
					this.object.cf_rag_soc_cliente.visible=false
					this.object.cod_cliente.visible=false
					dw_selezione.object.b_ricerca_fornitore.visible=true
					this.object.cod_fornitore_t.visible=true
					this.object.cf_rag_soc_fornitore.visible=true
					this.object.cod_fornitore.visible=true
				end if
			end if
			
		case "cod_cliente"
			setitem(getrow(), "cod_fornitore", ls_nulla)
		case "cod_fornitore"
			setitem(getrow(), "cod_cliente", ls_nulla)

	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_righe_bolla_euro
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 20
string dataobject = "d_report_righe_bolla_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

