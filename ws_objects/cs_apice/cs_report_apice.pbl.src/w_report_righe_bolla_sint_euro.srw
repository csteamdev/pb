﻿$PBExportHeader$w_report_righe_bolla_sint_euro.srw
$PBExportComments$Finestra Report Righe Bolla Sintetico
forward
global type w_report_righe_bolla_sint_euro from w_cs_xx_principale
end type
type tab_1 from tab within w_report_righe_bolla_sint_euro
end type
type det_1 from userobject within tab_1
end type
type dw_selezione from uo_cs_xx_dw within det_1
end type
type det_1 from userobject within tab_1
dw_selezione dw_selezione
end type
type det_2 from userobject within tab_1
end type
type dw_report from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_1
dw_report dw_report
end type
type tab_1 from tab within w_report_righe_bolla_sint_euro
det_1 det_1
det_2 det_2
end type
end forward

global type w_report_righe_bolla_sint_euro from w_cs_xx_principale
integer width = 3982
integer height = 2272
string title = "Report Righe Bolla"
boolean resizable = false
tab_1 tab_1
end type
global w_report_righe_bolla_sint_euro w_report_righe_bolla_sint_euro

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string  ls_doc_da,ls_doc_a,ls_cod_cliente, ls_cod_tipo_bol_ven, ls_flag_gen_fat, ls_stringa, ls_formato
integer li_riga
dec{4}  ld_num_da,ld_num_a,ld_doc_da,ld_doc_a
datetime ldt_da,ldt_a
string ls_cod_agente
datawindow dw_selezione

dw_selezione = tab_1.det_1.dw_selezione
dw_selezione.accepttext()
ld_num_da=dw_selezione.getitemnumber(1,"n_doc_da")
if isnull(ld_num_da) or ld_num_da <1 then ld_num_da=1

ld_num_a=dw_selezione.getitemnumber(1,"n_doc_a")
if isnull(ld_num_a) then ld_num_a = 999999

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
if isnull(ls_cod_cliente) or ls_cod_cliente=""  then ls_cod_cliente='%'

ls_cod_tipo_bol_ven = dw_selezione.getitemstring(1,"cod_tipo_bol_ven")
if isnull(ls_cod_tipo_bol_ven) or ls_cod_tipo_bol_ven = ""  then ls_cod_tipo_bol_ven = '%'


ldt_da = datetime(dw_selezione.getitemdate(1,"d_data_da"))
if isnull(ldt_da) then ldt_da = datetime(DATE("01/01/1900"),00:00:00)
ldt_a = datetime(dw_selezione.getitemdate(1,"d_data_a"))
if isnull(ldt_a) then ldt_a=datetime(DATE("31/12/2999"),00:00:00)

ls_flag_gen_fat = dw_selezione.getitemstring(1,"flag_fat_gen")
if ls_flag_gen_fat = "T"  then ls_flag_gen_fat ='%'

//Donato 06/04/2009 aggiunto filtro per agente
ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
if ls_cod_agente = "" then setnull(ls_cod_agente)
//fine modifica -----------------------------------------

//Donato 06/04/2009 aggiunto filtro per agente
tab_1.det_2.dw_report.retrieve( ldt_da, ldt_a, ls_cod_cliente, ld_num_da, ld_num_a, &
							s_cs_xx.cod_azienda, ls_cod_tipo_bol_ven, ls_flag_gen_fat, &
							ls_cod_agente)
//dw_report.retrieve(ldt_da,ldt_a,ls_cod_cliente,ld_num_da,ld_num_a,s_cs_xx.cod_azienda, ls_cod_tipo_bol_ven, ls_flag_gen_fat)

return 1
end function

event pc_setwindow;call super::pc_setwindow;tab_1.det_2.dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

tab_1.det_2.dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
tab_1.det_1.dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = tab_1.det_2.dw_report

end event

on w_report_righe_bolla_sint_euro.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_report_righe_bolla_sint_euro.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_1.det_1.dw_selezione, &
	"cod_tipo_bol_ven", &
	sqlca, &
	"tab_tipi_bol_ven", &
	"cod_tipo_bol_ven", &
	"des_tipo_bol_ven", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
							
//Donato 06/04/2009 aggiunto filtro per agente
f_PO_LoadDDDW_DW(tab_1.det_1.dw_selezione, &
	"cod_agente", &
	sqlca,&
	"anag_agenti","cod_agente","rag_soc_1", &
	"(anag_agenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
	"((anag_agenti.flag_blocco <> 'S') or (anag_agenti.flag_blocco = 'S' and anag_agenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )
//fine modifica ---------------------------

end event

event resize;call super::resize;tab_1.resize(newwidth - 20, newheight - 40)

tab_1.event ue_resize()
end event

type tab_1 from tab within w_report_righe_bolla_sint_euro
event create ( )
event destroy ( )
event ue_resize ( )
integer x = 18
integer y = 16
integer width = 3913
integer height = 2144
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
det_1 det_1
det_2 det_2
end type

on tab_1.create
this.det_1=create det_1
this.det_2=create det_2
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_1.destroy
destroy(this.det_1)
destroy(this.det_2)
end on

event ue_resize();det_2.dw_report.resize(det_2.width, det_2.height)
end event

type det_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3877
integer height = 2016
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_selezione dw_selezione
end type

on det_1.create
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_selezione}
end on

on det_1.destroy
destroy(this.dw_selezione)
end on

type dw_selezione from uo_cs_xx_dw within det_1
integer x = 18
integer y = 32
integer width = 2267
integer height = 688
integer taborder = 30
string dataobject = "d_cerca_righe_bolle_sintetico"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
	case "b_cerca"
		if wf_report() > 0 then
			tab_1.selecttab(2)
		end if
end choose
end event

event pcd_new;call super::pcd_new;dw_selezione.setitem(1, "flag_fat_gen", 'T')
end event

type det_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3877
integer height = 2016
long backcolor = 12632256
string text = "Report"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_report dw_report
end type

on det_2.create
this.dw_report=create dw_report
this.Control[]={this.dw_report}
end on

on det_2.destroy
destroy(this.dw_report)
end on

type dw_report from uo_cs_xx_dw within det_2
integer width = 3474
integer height = 1420
integer taborder = 60
string dataobject = "d_report_righe_bolla_sint_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

