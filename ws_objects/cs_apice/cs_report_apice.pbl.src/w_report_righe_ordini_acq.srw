﻿$PBExportHeader$w_report_righe_ordini_acq.srw
$PBExportComments$Finestra Report Ordini di Acquisto
forward
global type w_report_righe_ordini_acq from w_cs_xx_principale
end type
type st_1 from statictext within w_report_righe_ordini_acq
end type
type dw_selezione from uo_cs_xx_dw within w_report_righe_ordini_acq
end type
type cb_annulla from commandbutton within w_report_righe_ordini_acq
end type
type cb_report from commandbutton within w_report_righe_ordini_acq
end type
type dw_report from uo_cs_xx_dw within w_report_righe_ordini_acq
end type
type dw_folder from u_folder within w_report_righe_ordini_acq
end type
end forward

global type w_report_righe_ordini_acq from w_cs_xx_principale
integer width = 5083
integer height = 2588
string title = "Report Righe Ordini Acquisto"
st_1 st_1
dw_selezione dw_selezione
cb_annulla cb_annulla
cb_report cb_report
dw_report dw_report
dw_folder dw_folder
end type
global w_report_righe_ordini_acq w_report_righe_ordini_acq

type variables
private:
	string is_sql_select_gruppo, is_sql_groupy, is_sql_rda
end variables

forward prototypes
public function integer wf_report_riepilogo_prod (string as_where, ref string as_errore)
public subroutine wf_imposta_sql_group ()
public function integer wf_situazione_prodotto (string as_cod_prodotto, long al_row, ref string as_errore)
public function integer wf_report_riepilogo_prod_2 (string as_where, ref string as_errore)
end prototypes

public function integer wf_report_riepilogo_prod (string as_where, ref string as_errore);

string					ls_sql, ls_cod_prodotto

long					ll_index, ll_tot


ls_sql = is_sql_select_gruppo + " and det_ord_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' " + as_where + " " + is_sql_groupy

dw_report.setsqlselect(ls_sql)

ll_tot = dw_report.retrieve()

if ll_tot < 0 then
	as_errore = "Errore durante la retrieve dei dati!"
	return -1
end if

//rielaborazione righe per determinare la giacenza totale, l'ordinato al fornitore e la disponibilità teorica
for ll_index=1 to ll_tot
	
	ls_cod_prodotto = dw_report.getitemstring(ll_index, "cod_prodotto")
	
//	if wf_situazione_prodotto(ls_cod_prodotto, ll_index, as_errore) < 0 then
//		return -1
//	end if
next


return 0
end function

public subroutine wf_imposta_sql_group ();
datastore 			lds_data
long					ll_pos


lds_data = create datastore
lds_data.dataobject = "d_righe_report_ord_acq_riepilogo_prod"
lds_data.settransobject(sqlca)

is_sql_select_gruppo = lds_data.getsqlselect()
destroy lds_data

ll_pos = pos(is_sql_select_gruppo, "group by")
if ll_pos > 0 then
	is_sql_groupy = " " + mid(is_sql_select_gruppo, ll_pos, len(is_sql_select_gruppo))
	is_sql_select_gruppo = mid(is_sql_select_gruppo, 1, ll_pos -1) + " "
end if

lds_data = create datastore
lds_data.dataobject = "d_righe_report_ord_acq_riepilogo_prod_2"
lds_data.settransobject(sqlca)

is_sql_rda = lds_data.getsqlselect()

destroy lds_data
end subroutine

public function integer wf_situazione_prodotto (string as_cod_prodotto, long al_row, ref string as_errore);double				ld_saldo_quan_inizio_anno, ld_prog_quan_entrata, ld_prog_quan_uscita, &
						ld_quan_ordinata, 	ld_quan_impegnata, ld_quan_assegnata, ld_quan_in_spedizione, ld_quan_anticipi, &
						ld_reale, ld_teorica, ld_giacenza

select			saldo_quan_inizio_anno, prog_quan_entrata, prog_quan_uscita,  
				quan_ordinata, quan_impegnata, quan_assegnata, quan_in_spedizione, quan_anticipi  
into			:ld_saldo_quan_inizio_anno, :ld_prog_quan_entrata, :ld_prog_quan_uscita,
				:ld_quan_ordinata, :ld_quan_impegnata, :ld_quan_assegnata, :ld_quan_in_spedizione, :ld_quan_anticipi  
from		anag_prodotti
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in lettura situazione del prodotto '"+as_cod_prodotto+"': "+sqlca.sqlerrtext
	return -1
end if


//IMPEGNATO
dw_report.setitem(al_row, "tot_impegnato", ld_quan_impegnata)

//GIACENZA
ld_giacenza = ld_saldo_quan_inizio_anno + ld_prog_quan_entrata - ld_prog_quan_uscita
dw_report.setitem(al_row, "giacenza_totale", ld_giacenza)

//DISPONIBILITA'
ld_reale =ld_giacenza - ld_quan_impegnata - ld_quan_anticipi - ld_quan_assegnata - ld_quan_in_spedizione

//DISPONIBILITA' TEORICA
ld_teorica = ld_reale + ld_quan_ordinata //+ ld_quan_in_spedizione
dw_report.setitem(al_row, "disp_teorica", ld_teorica)

return 0

end function

public function integer wf_report_riepilogo_prod_2 (string as_where, ref string as_errore);

string					ls_sql, ls_cod_prodotto

long					ll_index, ll_tot


ls_sql = is_sql_rda + " and det_ord_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' " + as_where + " "

dw_report.setsqlselect(ls_sql)

ll_tot = dw_report.retrieve()

if ll_tot < 0 then
	as_errore = "Errore durante la retrieve dei dati!"
	return -1
end if


return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]
string ls_des_azienda

select rag_soc_1
  into :ls_des_azienda
  from aziende
 where cod_azienda = :s_cs_xx.cod_azienda;
 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Descrizione Azienda non trovata")
end if

if sqlca.sqlcode = 0 then
	dw_report.object.st_azienda.text = ls_des_azienda
end if	

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
                            
save_on_close(c_socnosave)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_annulla
l_objects[4] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)


wf_imposta_sql_group()


end event

on w_report_righe_ordini_acq.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_selezione=create dw_selezione
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
end on

on w_report_righe_ordini_acq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_selezione)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(		dw_selezione, &
                 	  "tipo_doc_acq", &
                 		sqlca, &
                    "tab_tipi_ord_acq", &
                    "cod_tipo_ord_acq", &
                    "des_tipo_ord_acq", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						  
						  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
						  
end event

type st_1 from statictext within w_report_righe_ordini_acq
integer x = 114
integer y = 988
integer width = 1806
integer height = 92
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_righe_ordini_acq
integer x = 46
integer y = 160
integer width = 3131
integer height = 820
integer taborder = 40
string dataobject = "d_sel_righe_report_ordini_acq"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_null
date ld_null

setnull(ls_null)
setnull(ld_null)

//dw_selezione.insertrow(0)

dw_selezione.setitem(1, "cod_fornitore", ls_null)
dw_selezione.setitem(1, "cod_prodotto", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "anno", f_anno_esercizio())
dw_selezione.setitem(1, "num_ordine_da", 1)
dw_selezione.setitem(1, "num_ordine_a", 999999)

//	Michela 28/05/2007: messo a N il default dei flag bloccati
dw_selezione.setitem(1, "flag_blocco", 'N')
dw_selezione.setitem(1, "flag_blocco_righe", 'N')
//	fine modifica

dw_selezione.setitem(1, "flag_evasione", 'A')
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "flag_blocco"
			if i_coltext = "S" then
				dw_selezione.setitem(dw_selezione.getrow(),"flag_blocco_righe","T")
				dw_selezione.Object.flag_blocco_righe.Protect = 1
			else
				dw_selezione.Object.flag_blocco_righe.Protect = 0
			end if
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

type cb_annulla from commandbutton within w_report_righe_ordini_acq
integer x = 2400
integer y = 988
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null
date ld_null

setnull(ls_null)
setnull(ld_null)

//dw_selezione.insertrow(0)
dw_selezione.setitem(1, "anno", f_anno_esercizio())
dw_selezione.setitem(1, "cod_fornitore", ls_null)
dw_selezione.setitem(1, "cod_prodotto", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "data_consegna_da", ld_null)
dw_selezione.setitem(1, "data_consegna_a", ld_null)
dw_selezione.setitem(1, "num_ordine_da", 1)
dw_selezione.setitem(1, "num_ordine_a", 999999)
dw_selezione.setitem(1, "flag_blocco", 'T')
dw_selezione.setitem(1, "flag_blocco_righe", 'T')
dw_selezione.setitem(1, "flag_evasione", 'A')
end event

type cb_report from commandbutton within w_report_righe_ordini_acq
integer x = 2811
integer y = 988
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string					ls_cod_fornitore_sel, ls_cod_prodotto_sel, ls_flag_blocco_sel, ls_flag_evasione_sel, ls_cod_prodotto, ls_des_prodotto, &
						ls_unita_misura, ls_iva, ls_stato, ls_tipo_listino, ls_valuta, ls_pagamento, ls_fornitore, ls_stato_ordine, ls_flag_saldo,&
						ls_des_pagamento, ls_cod_tipo_det_acq,ls_flag_tipo_det_acq, ls_dettaglio, ls_testata, ls_rag_soc_1, ls_da, ls_a, ls_formato, &
						ls_flag_blocco_righe_sel,ls_cod_misura_acq, ls_tipo_doc_acq, ls_cod_tipo_ord_acq, ls_des_tipo_acq, ls_flag_note, &
						ls_flag_commessa, ls_nota_dettaglio, ls_cod_deposito, ls_flag_residuo, ls_flag_barcode, ls_flag_des_specifica, ls_des_specifica, &
						ls_flag_vis_reparto, ls_cod_reparto, ls_des_reparto, ls_nota_testata, ls_cod_deposito_ordine, &
						ls_cod_deposito_trasf_ordine, ls_flag_deposito_trasf, ls_tipo_report, ls_where, ls_errore, ls_cod_tipo_politica_riordino
		 
long					ll_prog_riga, ll_confezioni, ll_pezzi_x_confezione, ll_num_ordine, ll_anno_ordine, ll_i, ll_anno_sel, ll_num_ordine_da_sel, &
						ll_num_ordine_a_sel, ll_anno_commessa, ll_num_commessa
		 
dec{4}				ld_qta_ordine, ld_residuo_ordine, ld_prezzo_acq, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
						ld_sconto_4,  ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
						ld_valore_riga, ld_sconto, ld_quan_arrivata, ld_sconto_totale, ld_cambio_acq
						
dec{5}				ld_fat_conversione
		 
datetime				ldt_data_consegna, ldt_data_consegna_for, ldt_data_registrazione, ldt_da_data_consegna, ldt_a_data_consegna, &
						ldt_data_da_sel, ldt_data_a_sel
						
integer				li_ret
						
			
uo_calcola_sconto iuo_calcola_sconto

dw_report.Reset() 
dw_report.setredraw(false)
dw_selezione.accepttext()


ls_tipo_report = dw_selezione.getitemstring(1, "tipo_report")

ls_cod_fornitore_sel = dw_selezione.getitemstring(1, "cod_fornitore")
ls_cod_prodotto_sel = dw_selezione.getitemstring(1, "cod_prodotto")
ls_flag_blocco_sel = dw_selezione.getitemstring(1, "flag_blocco")
ls_flag_blocco_righe_sel = dw_selezione.getitemstring(1, "flag_blocco_righe")
ls_flag_evasione_sel = dw_selezione.getitemstring(1, "flag_evasione")
ldt_data_da_sel = dw_selezione.getitemdatetime(1, "data_da")
ldt_data_a_sel = dw_selezione.getitemdatetime(1, "data_a")
ll_anno_sel = dw_selezione.getitemnumber(1, "anno")
ll_num_ordine_da_sel = dw_selezione.getitemnumber(1, "num_ordine_da")
ll_num_ordine_a_sel = dw_selezione.getitemnumber(1, "num_ordine_a")
ldt_da_data_consegna = dw_selezione.getitemdatetime(1, "data_consegna_da")
ldt_a_data_consegna = dw_selezione.getitemdatetime(1, "data_consegna_a")
ls_tipo_doc_acq = dw_selezione.getitemstring(1, "tipo_doc_acq")
ls_flag_note = dw_selezione.getitemstring(1, "flag_note_det")
ls_flag_commessa = dw_selezione.getitemstring(1, "flag_rif_commessa")
ls_cod_deposito = dw_selezione.getitemstring( 1, "cod_deposito")
ls_flag_residuo = dw_selezione.getitemstring( 1, "flag_residuo")
ls_flag_des_specifica = dw_selezione.getitemstring( 1, "flag_des_specifica")

ls_flag_vis_reparto = dw_selezione.getitemstring( 1, "flag_reparto")

//###################################################################
//Donato 24/07/2013
//ma porco cazzo: la datawindow "d_righe_report_ordini_acq_bc" NON ESISTE IN ALCUN REPOSITORY !!!!
//ma come cazzo si fa?
//quindi commento sta parte che altrimenti darebbe errore durante l'elaborazione e rendo 
//invisibile wsto radio button del cazzo sulla dw di ricerca

//// *** Michela Mantoan 22/11/2007: scelgo il tipo di report (se standard o quello col barcode)
//ls_flag_barcode = dw_selezione.getitemstring( 1, "flag_barcode")
//if isnull(ls_flag_barcode) or ls_flag_barcode = "" then ls_flag_barcode = "N"
//if ls_flag_barcode = "S" then
//	dw_report.dataobject = "d_righe_report_ordini_acq_bc"
//else
//	dw_report.dataobject = "d_righe_report_ordini_acq"
//end if

//fine modifica del cazzo ......
//###################################################################

if isnull(ldt_da_data_consegna) then ldt_da_data_consegna = datetime(date("01/01/1900"), 00:00:00)
if isnull(ldt_a_data_consegna) then ldt_a_data_consegna = datetime(date("31/12/2099"), 00:00:00)

dw_report.dataobject = "d_righe_report_ordini_acq"
dw_report.settransobject( sqlca)

//inizio gestione NUOVO LAYOUT ------------------------
if ls_tipo_report = "R" or ls_tipo_report = "V" then
	//report layout diverso: raggruppamento per prodotto SR Controllo_struttura per Dekora (funzione 4)
	//+ report marginalità vendite (valore "V")
	st_1.text = "Preparazione query in corso ..."
	
	//cambio dataobject
	if ls_tipo_report = "R" then
		dw_report.dataobject = "d_righe_report_ord_acq_riepilogo_prod"
	else
		dw_report.dataobject = "d_righe_report_ord_acq_riepilogo_prod_2"
	end if
	dw_report.settransobject( sqlca)
	
	if ls_cod_fornitore_sel<>"" and not isnull(ls_cod_fornitore_sel) then ls_where += "and tes_ord_acq.cod_fornitore='"+ls_cod_fornitore_sel+"' "
	if ls_cod_prodotto_sel<>"" and not isnull(ls_cod_prodotto_sel) then ls_where += "and det_ord_acq.cod_prodotto='"+ls_cod_prodotto_sel+"' "
	if not isnull(ls_flag_blocco_sel) and ls_flag_blocco_sel <> "T" then ls_where += " and tes_ord_acq.flag_blocco='"+ls_flag_blocco_sel+"' "
	if not isnull(ls_flag_blocco_righe_sel) and ls_flag_blocco_righe_sel <> "T" then ls_where += "and det_ord_acq.flag_blocco='" + ls_flag_blocco_righe_sel + "' "
	
	choose case ls_flag_evasione_sel
		case "P"			//parziale
			ls_where += "and det_ord_acq.quan_arrivata > 0 and det_ord_acq.flag_saldo = 'N' "
		case "A"			//aperto
			ls_where += "and (det_ord_acq.quan_ordinata - det_ord_acq.quan_arrivata) > 0 and det_ord_acq.flag_saldo = 'N' "
		case "E"			//evaso
			ls_where += "and ((det_ord_acq.quan_ordinata - det_ord_acq.quan_arrivata) <= 0 or flag_saldo = 'S') "
	end choose
	
	if not isnull(ldt_data_da_sel) then ls_where += "and tes_ord_acq.data_registrazione>='" + string(ldt_data_da_sel, s_cs_xx.db_funzioni.formato_data) + "' "
	if not isnull(ldt_data_a_sel) then ls_where += "and tes_ord_acq.data_registrazione <= '" + string(ldt_data_a_sel, s_cs_xx.db_funzioni.formato_data) + "'" 
	if not isnull(ll_anno_sel) and ll_anno_sel > 0 then ls_where += "and tes_ord_acq.anno_registrazione=" + string(ll_anno_sel) + " "
	if not isnull(ldt_da_data_consegna) then ls_where += "and tes_ord_acq.data_consegna>='" + string(ldt_da_data_consegna, s_cs_xx.db_funzioni.formato_data) + "' "
	if not isnull(ldt_a_data_consegna) then ls_where += "and tes_ord_acq.data_consegna<='" + string(ldt_a_data_consegna, s_cs_xx.db_funzioni.formato_data) + "' "
	if not isnull(ll_num_ordine_da_sel) then ls_where += "and tes_ord_acq.num_registrazione>=" + string(ll_num_ordine_da_sel) + " "
	if not isnull(ll_num_ordine_a_sel) then ls_where += "and tes_ord_acq.num_registrazione<=" + string(ll_num_ordine_a_sel) + " "
	if not isnull(ls_tipo_doc_acq) and ls_tipo_doc_acq<"" then ls_where += "and tes_ord_acq.cod_tipo_ord_acq='" + ls_tipo_doc_acq + "' "
	if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then ls_where += "and tes_ord_acq.cod_deposito='" + string(ls_cod_deposito) + "' "
	
	ls_cod_tipo_politica_riordino = dw_selezione.getitemstring(1,"cod_tipo_politica_riordino")
	if ls_cod_tipo_politica_riordino<>"" and not isnull(ls_cod_tipo_politica_riordino) then
		ls_where += " and anag_prodotti.cod_tipo_politica_riordino='"+ls_cod_tipo_politica_riordino+"' "
	end if
	
	if ls_tipo_report = "R" then
		li_ret = wf_report_riepilogo_prod(ls_where, ls_errore)
	else
		li_ret = wf_report_riepilogo_prod_2(ls_where, ls_errore)
	end if
	
	if li_ret < 0 then
		g_mb.error(ls_errore)
		dw_report.setredraw(true)
		st_1.text = "Elaborazione terminata con errori!"
		return
	end if
	st_1.text = "Pronto"
	dw_report.setredraw(true)
	dw_folder.fu_selecttab(2)
	dw_report.change_dw_current()
	
	//quindi esci perchè hai generato il report secondo questo layout e noj quello classico
	return
end if
//fine gestione NUOVO LAYOUT --------------------------

ll_i = 0

declare cu_testata dynamic cursor for sqlsa;

ls_testata = "select anno_registrazione," + & 
							"num_registrazione," + & 
							"data_registrazione," + & 
							"cod_tipo_listino_prodotto," + & 
							"cod_valuta," + & 
							"cod_pagamento," + & 
							"cod_fornitore," + & 
							"flag_evasione," + & 
							"sconto," + & 
							"cod_tipo_ord_acq," + & 
							"cod_deposito,"+&
							"cod_deposito_trasf,"+&
							"nota_testata "+&
				"from tes_ord_acq  " + & 
				"where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_fornitore_sel) then
	ls_testata = ls_testata + " and cod_fornitore = '" + ls_cod_fornitore_sel + "'"
end if

if not isnull(ls_flag_blocco_sel) and ls_flag_blocco_sel <> "T" then
	ls_testata = ls_testata + " and flag_blocco = '" + ls_flag_blocco_sel + "'"
end if

ls_da = string(ldt_data_da_sel, s_cs_xx.db_funzioni.formato_data)
if not isnull(ldt_data_da_sel) then
	ls_testata = ls_testata + " and data_registrazione >= '" + ls_da + "'"
end if

ls_a = string(ldt_data_a_sel, s_cs_xx.db_funzioni.formato_data)
if not isnull(ldt_data_a_sel) then
	ls_testata = ls_testata + " and data_registrazione <= '" + string(ls_a) + "'"
end if

if not isnull(ll_anno_sel) and ll_anno_sel > 0 then
	ls_testata = ls_testata + " and anno_registrazione = " + string(ll_anno_sel) + " "
end if

//----------------------------------------
if not isnull(ldt_da_data_consegna) then
	ls_testata = ls_testata + " and data_consegna >= '" + string(ldt_da_data_consegna, s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ldt_a_data_consegna) then
	ls_testata = ls_testata + " and data_consegna <= '" + string(ldt_a_data_consegna, s_cs_xx.db_funzioni.formato_data) + "'"
end if
//----------------------------------------

if not isnull(ll_num_ordine_da_sel) then
	ls_testata = ls_testata + " and num_registrazione >= " + string(ll_num_ordine_da_sel) + " "
end if

if not isnull(ll_num_ordine_a_sel) then
	ls_testata = ls_testata + " and num_registrazione <= " + string(ll_num_ordine_a_sel) + " "
end if

if not isnull(ls_tipo_doc_acq) then
	ls_testata = ls_testata + " and cod_tipo_ord_acq = '" + string(ls_tipo_doc_acq) + "' "
end if

if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
	ls_testata += " and cod_deposito = '" + string(ls_cod_deposito) + "' "
end if

prepare sqlsa from :ls_testata;

open dynamic cu_testata;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_testata. " + sqlca.sqlerrtext)
	rollback;
	return
end if

do while 1=1
	
   fetch cu_testata into 	:ll_anno_ordine, :ll_num_ordine, :ldt_data_registrazione, :ls_tipo_listino, &
									:ls_valuta, :ls_pagamento, :ls_fornitore, :ls_stato_ordine, :ld_sconto, :ls_cod_tipo_ord_acq, &
									:ls_cod_deposito_ordine, :ls_cod_deposito_trasf_ordine, :ls_nota_testata;
	
		if sqlca.sqlcode = 100 then exit
	
		if sqlca.sqlcode = 0 then
		
		if ls_valuta<>"" and not isnull(ls_valuta) then
			select cambio_acq,
					 formato
			into   :ld_cambio_acq,
					 :ls_formato
			from   tab_valute
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_valuta = :ls_valuta;
					 
			if sqlca.sqlcode < 0 then
				g_mb.error("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
				return -1
			end if	
		else
			g_mb.warning("APICE",	"Nell'ordine "+string(ll_anno_ordine)+"/"+string(ll_num_ordine) + " non è statp selezionato il codice valuta. "+&
												"Sarà escluso dal report!")
			continue
		end if
		
		
		ls_flag_deposito_trasf = "N"
		if isnull(ls_cod_deposito_trasf_ordine) or ls_cod_deposito_trasf_ordine="" then
			ls_cod_deposito_trasf_ordine=ls_cod_deposito_ordine
		else
			ls_flag_deposito_trasf = "S"
		end if
		
		if not isnull(ls_cod_deposito_trasf_ordine) and ls_cod_deposito_trasf_ordine <>"" then
			ls_cod_deposito_trasf_ordine = ls_cod_deposito_trasf_ordine + " - " + &
															f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_trasf_ordine + "'", "des_deposito")
		else
			ls_cod_deposito_trasf_ordine = ""												
		end if
		
		
		st_1.text = string(ll_anno_ordine)+"/"+string(ll_num_ordine)
		declare cu_dettaglio dynamic cursor for sqlsa;
		ls_dettaglio = "select 	prog_riga_ordine_acq,"+&
										"cod_tipo_det_acq,"+&
										"cod_prodotto,"+&
										"des_prodotto,"+&
										"des_specifica,"+&
										"quan_ordinata,"+&
										"(quan_ordinata - quan_arrivata),"+&
										"prezzo_acquisto,"+&
										"sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10,"+&
										"imponibile_iva_valuta, cod_iva, data_consegna, data_consegna_fornitore, flag_saldo, quan_arrivata, fat_conversione,"+&
										"cod_misura, nota_dettaglio, anno_commessa, num_commessa,"+&
										"cod_reparto "+&
							"from det_ord_acq "+&
							"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
										"anno_registrazione = " + string(ll_anno_ordine) + " and " + &
										"num_registrazione = " + string(ll_num_ordine) + " and " +&
										"((data_consegna>='" + string(ldt_da_data_consegna, s_cs_xx.db_funzioni.formato_data) + "' and "+&
										"data_consegna<='" + string(ldt_a_data_consegna, s_cs_xx.db_funzioni.formato_data) + "') or data_consegna is null) "
		
		if not isnull(ls_cod_prodotto_sel) then
			ls_dettaglio = ls_dettaglio + " and cod_prodotto = '" + ls_cod_prodotto_sel + "'"
		end if
		// flag blocco (aggiunto da EnMe 12/3/2002)
		if not isnull(ls_flag_blocco_righe_sel) and ls_flag_blocco_righe_sel <> "T" then
			ls_dettaglio = ls_dettaglio + " and flag_blocco = '" + ls_flag_blocco_righe_sel + "'"
		end if
		// valuto lo stato della riga dell'ordine
		choose case ls_flag_evasione_sel
			case "P"
				ls_dettaglio = ls_dettaglio + " and quan_arrivata > 0 and flag_saldo = 'N' "
			case "A"
				ls_dettaglio = ls_dettaglio + " and (quan_ordinata - quan_arrivata) > 0 and flag_saldo = 'N' "
			case "E"
				ls_dettaglio = ls_dettaglio + " and ((quan_ordinata - quan_arrivata) <= 0 or flag_saldo = 'S') "
		end choose
		prepare sqlsa from :ls_dettaglio;
		open dynamic cu_dettaglio;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nell'apertura del cursore cu_dettagli " + sqlca.sqlerrtext)
			rollback;
			return
		end if

		do while 1=1
			
			fetch cu_dettaglio into :ll_prog_riga, 
											:ls_cod_tipo_det_acq, 
											:ls_cod_prodotto, 
											:ls_des_prodotto, 
											:ls_des_specifica,
											:ld_qta_ordine, 
											:ld_residuo_ordine, 
											:ld_prezzo_acq, 
											:ld_sconto_1, 
											:ld_sconto_2, 
											:ld_sconto_3, 
											:ld_sconto_4, 
											:ld_sconto_5, 
											:ld_sconto_6, 
											:ld_sconto_7, 
											:ld_sconto_8, 
											:ld_sconto_9, 
											:ld_sconto_10, 
											:ld_valore_riga, 
											:ls_iva, 
											:ldt_data_consegna, 
											:ldt_data_consegna_for, 
											:ls_flag_saldo, 
											:ld_quan_arrivata, 
											:ld_fat_conversione, 
											:ls_cod_misura_acq, 
											:ls_nota_dettaglio,
											:ll_anno_commessa, 
											:ll_num_commessa,
											:ls_cod_reparto;
			
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode = 0 then
				
				if mod(ll_i, 30) = 0 then Yield()
				
				if not isnull(ll_num_ordine) and not isnull(ll_anno_ordine) then
					
					ld_prezzo_acq = ld_prezzo_acq * ld_cambio_acq
					
					ll_i = dw_report.insertrow(0)					
					dw_report.setitem(ll_i, "num_ordine", ll_num_ordine)
					dw_report.setitem(ll_i, "anno_ordine", ll_anno_ordine)
					dw_report.setitem(ll_i, "data_registrazione", ldt_data_registrazione)
					dw_report.setitem(ll_i, "tipo_listino", ls_tipo_listino)
					dw_report.setitem(ll_i, "valuta", ls_valuta)
										
					dw_report.setitem(ll_i, "formato", ls_formato)
					
					dw_report.setitem(ll_i, "pagamento", ls_pagamento)
					
					select des_pagamento
					into   :ls_des_pagamento
					from   tab_pagamenti
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_pagamento = :ls_pagamento;
					dw_report.setitem(ll_i, "des_pagamento", ls_des_pagamento)
					
					select rag_soc_1
					  into :ls_rag_soc_1
					  from anag_fornitori
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_fornitore = :ls_fornitore;
					
					dw_report.setitem(ll_i, "fornitore", ls_rag_soc_1)
					
					if ls_stato_ordine = "E" then ls_stato_ordine = "Evaso"
					if ls_stato_ordine = "P" then ls_stato_ordine = "Parziale"
					if ls_stato_ordine = "A" then ls_stato_ordine = "Aperto"				
					
					dw_report.setitem(ll_i, "stato_ordine", ls_stato_ordine)
					
					select des_tipo_ord_acq
					into :ls_des_tipo_acq
					from tab_tipi_ord_acq
					where cod_azienda = :s_cs_xx.cod_azienda and
					 		cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
					
					
					dw_report.setitem(ll_i, "cod_tipo_ordine", ls_des_tipo_acq)
					
					dw_report.setitem(ll_i, "sconto", ld_sconto)
					
					dw_report.setitem(ll_i, "prog_riga", ll_prog_riga)
					if isnull(ls_cod_prodotto) then ls_cod_prodotto = ""
					dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
					if isnull(ls_des_prodotto) then ls_des_prodotto = ""
					if ls_des_prodotto = "" and not isnull(ls_cod_prodotto) then
						select des_prodotto
						into   :ls_des_prodotto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_prodotto = :ls_cod_prodotto;
					end if				
					
					dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
					
					if not isnull(ls_cod_prodotto) then
						select cod_misura_mag
						into   :ls_unita_misura
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
					end if
					
					dw_report.setitem(ll_i, "unita_misura_acq", ls_cod_misura_acq)
					dw_report.setitem(ll_i, "unita_misura", ls_unita_misura)
					dw_report.setitem(ll_i, "qta_ordine", ld_qta_ordine)
					dw_report.setitem(ll_i, "residuo_ordine", ld_residuo_ordine)
					dw_report.setitem(ll_i, "prezzo_acq", ld_prezzo_acq)
					dw_report.setitem(ll_i, "fat_conversione", ld_fat_conversione)
					
					iuo_calcola_sconto = create uo_calcola_sconto
					ld_sconto_totale = iuo_calcola_sconto.wf_calcola_sconto(ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10)
					destroy iuo_calcola_sconto					
					
					// le righe di sconto le metto in negativo
					select flag_tipo_det_acq
					into   :ls_flag_tipo_det_acq
					from   tab_tipi_det_acq
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_tipo_det_acq = :ls_cod_tipo_det_acq;
							 
					if not isnull(ls_flag_tipo_det_acq) and ls_flag_tipo_det_acq = "S" then  ld_valore_riga = ld_valore_riga * -1
					
					dw_report.setitem(ll_i, "sconto_1", ld_sconto_totale)
					
					if not isnull(ls_flag_residuo) and ls_flag_residuo = "S" then
						
						if isnull(ld_sconto_totale) then ld_sconto_totale = 0
						
						ld_valore_riga = ld_prezzo_acq * ld_residuo_ordine
						ld_valore_riga = ld_valore_riga - ( ld_valore_riga * ld_sconto_totale / 100 )
						dw_report.setitem( ll_i, "valore_riga", ld_valore_riga)
						
					else
						dw_report.setitem(ll_i, "valore_riga", ld_valore_riga)	
					end if
					dw_report.setitem(ll_i, "iva", ls_iva)
					dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
					dw_report.setitem(ll_i, "data_consegna_for", ldt_data_consegna_for)
	
					if ls_flag_saldo = "N" and (ld_quan_arrivata = 0 or isnull(ld_quan_arrivata)) then ls_stato = "A"
					if ls_flag_saldo = "N" and ld_quan_arrivata > 0 then ls_stato = "P"
					if ls_flag_saldo = "S" then ls_stato = "E"
					
					dw_report.setitem(ll_i, "stato", ls_stato)	
					
					if ls_flag_des_specifica = "S" then
						try
							dw_report.setitem(ll_i, "des_specifica", ls_des_specifica)
						catch (throwable err)
						end try
					end if
					
					if ls_flag_note = "S" then
						dw_report.setitem(ll_i, "nota_dettaglio", ls_nota_dettaglio)
					end if
					
					if ls_flag_commessa = "S" then
						dw_report.setitem(ll_i, "anno_commessa", ll_anno_commessa)	
						dw_report.setitem(ll_i, "num_commessa", ll_num_commessa)	
					end if
					
					
					//deposito o deposito trasf.
					dw_report.setitem(ll_i, "flag_deposito_trasf", ls_flag_deposito_trasf)
					dw_report.setitem(ll_i, "deposito", ls_cod_deposito_trasf_ordine)
					
					//nota testata
					dw_report.setitem(ll_i, "nota_testata", ls_nota_testata)
										
					//reparto della riga
					if ls_flag_vis_reparto = "S" then
						
						if ls_cod_reparto<>"" and not isnull(ls_cod_reparto) then
							
							select des_reparto
							into :ls_des_reparto
							from anag_reparti
							where 	cod_azienda = :s_cs_xx.cod_azienda and
										cod_reparto= :ls_cod_reparto;
							
							dw_report.setitem(ll_i, "reparto", "REPARTO: " + ls_cod_reparto + " - " + ls_des_reparto)
						end if
						
						
					end if
					
				end if	
				
			end if
			
		loop
		
		close cu_dettaglio;
		
	end if	
	
loop

close cu_testata;	
rollback;


dw_report.setsort("fornitore A, anno_ordine A, num_ordine A, prog_riga A")
dw_report.sort()

dw_report.groupcalc()

dw_report.setredraw(true)

dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.preview.rulers = 'Yes'
st_1.text = ""

dw_folder.fu_selecttab(2)
dw_report.change_dw_current()

end event

type dw_report from uo_cs_xx_dw within w_report_righe_ordini_acq
integer x = 46
integer y = 176
integer width = 4955
integer height = 2260
integer taborder = 50
string dataobject = "d_righe_report_ordini_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_righe_ordini_acq
integer x = 23
integer y = 20
integer width = 5019
integer height = 2436
integer taborder = 60
boolean border = false
end type

