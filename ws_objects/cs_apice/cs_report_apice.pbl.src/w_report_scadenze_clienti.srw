﻿$PBExportHeader$w_report_scadenze_clienti.srw
$PBExportComments$Report Estrattoconto Agenti
forward
global type w_report_scadenze_clienti from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_scadenze_clienti
end type
type cb_report from commandbutton within w_report_scadenze_clienti
end type
type dw_selezione from uo_cs_xx_dw within w_report_scadenze_clienti
end type
type st_1 from statictext within w_report_scadenze_clienti
end type
type dw_folder from u_folder within w_report_scadenze_clienti
end type
type dw_report from uo_cs_xx_dw within w_report_scadenze_clienti
end type
end forward

global type w_report_scadenze_clienti from w_cs_xx_principale
integer x = 663
integer y = 740
integer width = 3899
integer height = 1948
string title = "Report Scadenze Clienti"
boolean maxbox = false
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
st_1 st_1
dw_folder dw_folder
dw_report dw_report
end type
global w_report_scadenze_clienti w_report_scadenze_clienti

type variables
transaction sqlca2
string is_cdazie

end variables

forward prototypes
public subroutine wf_report1 ()
end prototypes

public subroutine wf_report1 ();integer 	li_scadenza_lire
string 	ls_cdente, ls_ragcog, ls_ragnom, ls_indir1, ls_indir2, ls_cap, ls_comune, ls_cdprov, ls_cod_agente, &
			ls_desc_agente, ls_rag_soc_1, ls_cod_cliente_par, ls_agente, ls_cod_cliente, ls_sql,ls_cod_capoconto, &
			ls_indirizzo, ls_localita,ls_provincia, ls_num_documento, ls_cod_tipo_fat_ven, ls_lista_clienti, ls_telefono, &
			ls_flag_salto_pg,ls_flag_pagate, ls_flag_tipo_fat_ven, ls_flag_visualizza_scaduto, ls_cod_pagamento, ls_des_pagamento
datetime ldt_da_data_registrazione, ldt_a_data_registrazione, ldt_da_data_scadenza, ldt_a_data_scadenza, ldt_data_riferimento, &
			ldt_taddoc, ldt_tadsca, ldt_data_documento, ldt_data_scadenza
integer 	Net, li_id_conto
long 		ll_i,  ll_x, ll_gg_scaduti,ll_righe,  ll_y, ll_j
dec{4} 	ld_importo_dare, ld_importo_avere, ld_importo_documento, ld_importo_rata, ld_saldo_scadenza
boolean 	lb_escludi
datastore lds_scadenzario


dw_report.reset()
dw_report.SetRedraw ( false )
dw_selezione.acceptText()
dw_report.change_dw_current()

ldt_da_data_registrazione  = dw_selezione.getitemdatetime(1,"rdt_da_data_registrazione")
ldt_a_data_registrazione   = dw_selezione.getitemdatetime(1,"rdt_a_data_registrazione")
ldt_da_data_scadenza  = dw_selezione.getitemdatetime(1,"rdt_da_data_scadenza")
ldt_a_data_scadenza   = dw_selezione.getitemdatetime(1,"rdt_a_data_scadenza")
ldt_data_riferimento  = dw_selezione.getitemdatetime(1,"rdt_data_riferimento")
ls_agente = dw_selezione.getitemString(1,"rs_agente")
ls_cod_cliente_par = dw_selezione.getitemString(1,"cod_cliente")
ls_flag_salto_pg = dw_selezione.getitemString(1,"flag_pagina_cliente")
ls_flag_pagate = dw_selezione.getitemString(1,"flag_pagate")
ls_flag_visualizza_scaduto = dw_selezione.getitemString(1,"flag_visualizza_scaduto")

if ls_flag_salto_pg = "S" then
	dw_report.dataobject = 'd_report_scadenze_clienti_cliente_newpage'
else
	dw_report.dataobject = 'd_report_scadenze_clienti'
end if

if isnull(ldt_da_data_registrazione) and isnull(ldt_a_data_registrazione) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire le date di registrazione ")
	  return
end if

if isnull(ldt_da_data_scadenza) and isnull(ldt_a_data_scadenza) then
	  g_mb.messagebox("Estratto Conto Clienti","Inserire le date di scadenza")
	  return
end if

ls_sql = "select cod_cliente, cod_capoconto, rag_soc_1, indirizzo, cap, localita, provincia, cod_agente_1, telefono from anag_clienti where cod_azienda = '"+s_cs_xx.cod_azienda+"'"

if not isnull(ls_agente)  then
	ls_sql = ls_sql + " and cod_agente_1 = '" + ls_agente + "'"
end if

if not isnull(ls_cod_cliente_par) and ls_cod_cliente_par <> "" then 
	ls_sql = ls_sql + " and cod_cliente = '" + ls_cod_cliente_par + "'"
end if

if isnull(ldt_da_data_registrazione) then  ldt_da_data_registrazione  = datetime(date("01/01/1900"))
if isnull(ldt_a_data_registrazione) then  ldt_a_data_registrazione  = datetime(date("31/12/2900"))
if isnull(ldt_da_data_scadenza)  then  ldt_da_data_scadenza   = datetime(date("01/01/1900"))
if isnull(ldt_a_data_scadenza)  then ldt_a_data_scadenza   = datetime(date("31/12/2999"))
if isnull(ldt_data_riferimento)  then ldt_data_riferimento   = datetime(today())

dw_report.object.data_scadenza_da.text=string(date(ldt_da_data_scadenza))
dw_report.object.data_scadenza_a.text=string(date(ldt_a_data_scadenza))
dw_report.object.data_doc_da.text=string(date(ldt_da_data_registrazione))
dw_report.object.data_doc_a.text=string(date(ldt_a_data_registrazione))

declare cu_clienti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_clienti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN del cursore clienti.~r~n" + sqlca.sqlerrtext)
	return
end if


do while true
   fetch cu_clienti into :ls_cod_cliente, :ls_cod_capoconto, :ls_rag_soc_1, :ls_indirizzo, :ls_cap, :ls_localita, :ls_provincia, :ls_cod_agente, :ls_telefono;
   if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then 
		g_mb.messagebox("APICE","Erroe in lettura cursore clienti:" + sqlca.sqlerrtext)
		exit
	end if
	
	Yield()
	
	if isnull(ls_provincia) then ls_provincia = ""
	if isnull(ls_localita) then ls_localita = ""
	if isnull(ls_indirizzo) then ls_indirizzo = ""
	if isnull(ls_cap) then ls_cap = ""
	if isnull(ls_telefono) then ls_telefono = ""
	if isnull(ls_rag_soc_1) then
		g_mb.messagebox("APICE","Attenzione!! Il cliente codice " +ls_cod_cliente+" non ha la ragione sociale; continuo lo stesso ! ")
		ls_rag_soc_1 = "SENZA RAGIONE SOCIALE"
	end if
	
	// estrazione delle scadenze
	ls_sql = 		" SELECT tes_fat_ven.cod_tipo_fat_ven,   " + &
					" tes_fat_ven.cod_cliente,   " + &
					" tes_fat_ven.cod_pagamento,   " + &
					" tes_fat_ven.cod_agente_1,   " + &
					" tes_fat_ven.cod_documento,   " + &
					" tes_fat_ven.numeratore_documento,   " + &
					" tes_fat_ven.anno_documento,   " + &
					" tes_fat_ven.num_documento,   " + &
					" tes_fat_ven.data_fattura,   " + &
					" tes_fat_ven.importo_iva,   " + &
					" tes_fat_ven.imponibile_iva,   " + &
					" tes_fat_ven.tot_fattura,   " + &
					" scad_fat_ven.data_scadenza,   " + &
					" scad_fat_ven.prog_scadenza,   " + &
					" scad_fat_ven.imp_rata,   " + &
					" scad_fat_ven.flag_pagata,   " + &
					" tab_tipi_fat_ven.flag_tipo_fat_ven, " + &
					" anag_agenti.rag_soc_1, " + &
					" tes_fat_ven.cod_pagamento   " + &
					" FROM tes_fat_ven LEFT OUTER JOIN scad_fat_ven ON tes_fat_ven.cod_azienda = scad_fat_ven.cod_azienda AND tes_fat_ven.anno_registrazione = scad_fat_ven.anno_registrazione AND tes_fat_ven.num_registrazione = scad_fat_ven.num_registrazione "  + &
											" LEFT OUTER JOIN tab_tipi_fat_ven ON tes_fat_ven.cod_azienda = tab_tipi_fat_ven.cod_azienda AND tes_fat_ven.cod_tipo_fat_ven = tab_tipi_fat_ven.cod_tipo_fat_ven " 	+ &
											" LEFT OUTER JOIN anag_agenti ON tes_fat_ven.cod_azienda = anag_agenti.cod_azienda AND tes_fat_ven.cod_agente_1 = anag_agenti.cod_agente " 	+ &
					" WHERE tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and tes_fat_ven.cod_documento is not null and tes_fat_ven.cod_cliente = '" + ls_cod_cliente + "' "

					ls_sql += " and tes_fat_ven.data_fattura >=  '" + string(ldt_da_data_registrazione, s_cs_xx.db_funzioni.formato_data) + "' and  tes_fat_ven.data_fattura <=  '" + string(ldt_a_data_registrazione, s_cs_xx.db_funzioni.formato_data) + "'"

					ls_sql += " and scad_fat_ven.data_scadenza >=  '" + string(ldt_da_data_scadenza, s_cs_xx.db_funzioni.formato_data) + "' and  scad_fat_ven.data_scadenza <=  '" + string(ldt_a_data_scadenza, s_cs_xx.db_funzioni.formato_data) + "'"
					
					if ls_flag_visualizza_scaduto = "S" then
						ls_sql += " and scad_fat_ven.data_scadenza <= '" + string(ldt_data_riferimento, s_cs_xx.db_funzioni.formato_data) + "'"
					end if

	ll_righe = guo_functions.uof_crea_datastore( lds_scadenzario, ls_sql)
	// fine SQL estrazione scadenze	

	
	for ll_i = 1 to ll_righe
		
		// non visualizzare le scadenze pagate
		if ls_flag_pagate = "N" and lds_scadenzario.getitemstring(ll_i,16) = "S"  then continue
		
		choose case lds_scadenzario.getitemstring(ll_i,17) 
			case "N"		// nota di credito
				ld_importo_avere = lds_scadenzario.getitemnumber(ll_i, 15)
				ld_importo_dare = 0		
			case "D", "I"		// fattura Differita / Immediata
				ld_importo_dare = lds_scadenzario.getitemnumber(ll_i, 15)
				ld_importo_avere = 0
			case else		// altro documento lascio stare (ad esempio le proforma)
				continue
		end choose

		ls_num_documento = g_str.format("$1/$2 - $3/$4", 	lds_scadenzario.getitemstring(ll_i, 5), &
																			lds_scadenzario.getitemstring(ll_i, 6), &
																			lds_scadenzario.getitemnumber(ll_i, 7), &
																			lds_scadenzario.getitemnumber(ll_i, 8))
		
		st_1.text = "Fattura nr." + ls_num_documento
		Yield()

		ldt_data_documento = lds_scadenzario.getitemdatetime(ll_i, 9)
		ld_importo_documento = lds_scadenzario.getitemnumber(ll_i, 12)
		ls_cod_tipo_fat_ven = lds_scadenzario.getitemstring(ll_i, 1)
		
		// Fine Modifica
		
		ldt_data_scadenza = lds_scadenzario.getitemdatetime(ll_i, 13)
			
		if isnull(ld_importo_dare) then ld_importo_dare = 0
		if isnull(ld_importo_avere) then ld_importo_avere = 0
		ld_importo_rata = ld_importo_dare - ld_importo_avere
	
		//if (ldt_data_documento < ldt_da_data_registrazione) or (ldt_data_documento > ldt_a_data_registrazione) then continue
		//if (ldt_data_scadenza < ldt_da_data_scadenza) or (ldt_data_scadenza > ldt_a_data_scadenza) then continue
		
			
		ls_desc_agente =  lds_scadenzario.getitemstring(ll_i, 18)
		
		ls_cod_pagamento =  lds_scadenzario.getitemstring(ll_i, 19)
		if isnull(ls_cod_pagamento) then
			ls_des_pagamento = "<non impostato>"
		else
			ls_des_pagamento = f_des_tabella("tab_pagamenti","cod_pagamento = '" + ls_cod_pagamento + "'", "des_pagamento")
		end if
		
		ll_x=dw_report.insertrow(0)
		dw_report.setrow(ll_x)
		ll_gg_scaduti = DaysAfter (date(ldt_data_scadenza) , date(ldt_data_riferimento))
		dw_report.setitem(ll_x,"data_riferimento", ldt_data_riferimento) 
		dw_report.setitem(ll_x,"cod_agente" , ls_cod_agente) 
		dw_report.setitem(ll_x,"agenti_rag_soc_1"  , ls_desc_agente )
		dw_report.setitem(ll_x, "cod_cliente", ls_cod_cliente)
		dw_report.setitem(ll_x, "rag_soc_1", ls_rag_soc_1)
		dw_report.setitem(ll_x, "indirizzo", ls_indirizzo)
		dw_report.setitem(ll_x, "cap", ls_cap)
		dw_report.setitem(ll_x, "citta",ls_localita)
		dw_report.setitem(ll_x, "prov", ls_provincia)
		dw_report.setitem(ll_x, "num_fattura", ls_num_documento)
		dw_report.setitem(ll_x, "data_fattura", ldt_data_documento)
		dw_report.setitem(ll_x, "importo_fattura", ld_importo_documento)
		dw_report.setitem(ll_x, "importo_rata", ld_importo_rata)
		dw_report.setitem(ll_x, "data_scadenza", ldt_data_scadenza)
		dw_report.setitem(ll_x, "num_giorni_scaduto", ll_gg_scaduti)
		dw_report.setitem(ll_x, "telefono", ls_telefono)
		dw_report.setitem(ll_x, "flag_pagato", lds_scadenzario.getitemstring(ll_i,16))
		dw_report.setitem(ll_x, "pagamento", ls_des_pagamento)
	next
	
	destroy lds_scadenzario
	
loop

close cu_clienti;

dw_report.setsort("cod_agente a, rag_soc_1 a, data_fattura a, num_protocollo_fattura d")
dw_report.sort()
dw_report.groupcalc()

dw_report.Object.DataWindow.Print.PreView = 'Yes'

dw_report.SetRedraw ( true )

dw_folder.fu_selecttab(2)

iuo_dw_main = dw_report


end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin +  c_NoEnablePopup)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_report

lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])
lw_oggetti[1] = dw_selezione
lw_oggetti[2] = st_1
lw_oggetti[3] = cb_annulla
lw_oggetti[4] = cb_report
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)

dw_folder.fu_selecttab(1)

dw_selezione.postevent("ue_imposta_date")
end event

on w_report_scadenze_clienti.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.st_1=create st_1
this.dw_folder=create dw_folder
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_report
end on

on w_report_scadenze_clienti.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.st_1)
destroy(this.dw_folder)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "rs_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170
end event

type cb_annulla from commandbutton within w_report_scadenze_clienti
integer x = 1673
integer y = 696
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_scadenze_clienti
integer x = 2043
integer y = 696
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;wf_report1()
end event

type dw_selezione from uo_cs_xx_dw within w_report_scadenze_clienti
event ue_imposta_date ( )
integer x = 78
integer y = 140
integer width = 2377
integer height = 700
integer taborder = 10
string dataobject = "d_selezione_scadenze_clienti_1"
boolean border = false
end type

event ue_imposta_date();string		ls_primo_gg_mese
datetime ldt_data_oggi

long ll_mese_corrente, ll_anno_corrente

ldt_data_oggi = datetime(today(), 00:00:00)
dw_selezione.setitem(1,"rdt_data_riferimento", ldt_data_oggi)

ll_mese_corrente = month( date(ldt_data_oggi) )
ll_anno_corrente = year( date(ldt_data_oggi) )

ls_primo_gg_mese = "01/"+string(ll_mese_corrente)+"/"+string(ll_anno_corrente)

dw_selezione.setitem(1,"rdt_da_data_registrazione",datetime(relativedate(today(),-180),00:00:00))
dw_selezione.setitem(1,"rdt_a_data_registrazione",datetime(relativedate(today(),180),00:00:00))
dw_selezione.setitem(1,"rdt_da_data_scadenza",datetime(relativedate(today(),-180),00:00:00))
dw_selezione.setitem(1,"rdt_a_data_scadenza",datetime(relativedate(today(),180),00:00:00))



end event

event itemchanged;call super::itemchanged;if i_extendmode then

	string ls_cod_agente_1, ls_nulla

	setnull(ls_nulla)
	if i_colname = "cod_cliente" then
	
		select cod_agente_1
		into :ls_cod_agente_1
		from anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda
			and cod_cliente =:i_coltext;
		
		if sqlca.sqlcode = 0 then
			setitem(i_rownbr, "rs_agente", ls_cod_agente_1)
		end if
	end if
	if i_colname = "rs_agente" then
		setitem(i_rownbr, "cod_cliente", ls_nulla)
	end if


end if

end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type st_1 from statictext within w_report_scadenze_clienti
integer x = 96
integer y = 700
integer width = 1568
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type dw_folder from u_folder within w_report_scadenze_clienti
integer x = 23
integer y = 20
integer width = 3817
integer height = 1800
integer taborder = 100
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_report from uo_cs_xx_dw within w_report_scadenze_clienti
integer x = 69
integer y = 148
integer width = 3739
integer height = 1652
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_scadenze_clienti"
boolean vscrollbar = true
boolean livescroll = true
end type

