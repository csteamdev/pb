﻿$PBExportHeader$w_report_scheda_magazzino.srw
forward
global type w_report_scheda_magazzino from w_cs_xx_principale
end type
type st_1 from statictext within w_report_scheda_magazzino
end type
type cb_reset from commandbutton within w_report_scheda_magazzino
end type
type cb_report from commandbutton within w_report_scheda_magazzino
end type
type dw_selezione from uo_cs_xx_dw within w_report_scheda_magazzino
end type
type dw_report from uo_cs_xx_dw within w_report_scheda_magazzino
end type
type dw_folder from u_folder within w_report_scheda_magazzino
end type
end forward

global type w_report_scheda_magazzino from w_cs_xx_principale
integer width = 3703
integer height = 2068
string title = "Schede di Magazzino"
st_1 st_1
cb_reset cb_reset
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_scheda_magazzino w_report_scheda_magazzino

forward prototypes
public subroutine wf_report ()
public function integer wf_prodotto_raggruppo (string as_cod_prodotto_raggruppo, uo_magazzino auo_mag, datetime adt_data_rif, string as_where, ref decimal ad_quant_val[], ref string as_errore, string as_flag_stock, ref string as_chiave[], ref decimal ad_giacenza_stock[], ref decimal ad_costo_medio[], ref decimal ad_quan_costo_medio[])
end prototypes

public subroutine wf_report ();boolean lb_1, lb_2, lb_3, lb_continue=false, lb_salta_negativi=false

string ls_gruppo[5], ls_gruppo_2, ls_gruppo_3, ls_gruppo_4, ls_gruppo_5,ls_flag_prodotti_movimentati, ls_flag_fiscale, &
       ls_sql, ls_cod_prodotto, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_des_prodotto, &
		 ls_cod_comodo,  ls_cod_cat_mer,ls_flag_dettaglio, ls_cod_misura_mag, ls_des_deposito, ls_des_cat_mer, &
		 ls_azienda_rag_soc_1, ls_azienda_localita, ls_azienda_provincia, ls_partita_iva, ls_des_tipo_movimento, &
		 ls_des_tipo_mov_det,ls_flag_riepilogo_mensile,ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_comodo_2, &
		 ls_cod_deposito, ls_flag_classe, ls_flag_valorizza, ls_cod_prodotto_mov, ls_des_movimento, ls_errore, &
		 ls_null, ls_chiave[], ls_path_logo_1, ls_modify, ls_des_azienda,ls_mese,ls_null_vett[], ls_flag_dett_mov, ls_flag_lifo

long ll_riga, ll_tipo_operazione,ll_return, ll_i, ll_j,ll_mese,ll_y,ll_mese_inizio, ll_anno, ll_ind_mese

dec{4} ld_quantita, ld_carico_tot, ld_scarico_tot, ld_saldo_tot, ld_riporto, ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], &
          ld_quan_costo_medio_stock[], ld_costo, ld_quan_movimento,ld_null[],ld_saldo_finale

datetime  ldt_inizio, ldt_fine, ldt_data_registrazione, ldt_data_chiusura_periodica, ldt_data_chiusura_annuale, ldt_null, &
          ldt_date_fine_mese[],ldt_fine_mese, ldt_date_start, ldt_date_end

uo_magazzino luo_magazzino
datastore lds_prodotti, lds_movimenti


dw_selezione.accepttext()

setnull(ldt_null)
setnull(ls_null)

ldt_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_fine = dw_selezione.getitemdatetime(1,"data_fine")
ls_cod_prodotto_da = dw_selezione.getitemstring(1,"cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getitemstring(1,"cod_prodotto_a")
ls_cod_comodo_2 = dw_selezione.getitemstring(1,"cod_comodo_2")
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")
ls_flag_classe = dw_selezione.getitemstring(1,"flag_classe")
ls_flag_valorizza = dw_selezione.getitemstring(1,"flag_valorizza")
ls_flag_fiscale = dw_selezione.getitemstring(1,"flag_fiscale")
ls_flag_riepilogo_mensile = dw_selezione.getitemstring(1,"flag_riepilogo_mensile")
ls_flag_dett_mov = dw_selezione.getitemstring(1,"flag_dett_mov")
ls_flag_lifo = dw_selezione.getitemstring(1, "flag_lifo")

if ls_flag_riepilogo_mensile ="N" then
	ls_flag_dett_mov = "S"
end if

if ls_flag_riepilogo_mensile ="S" then
	ls_flag_valorizza = "N"
//	ls_flag_dett_mov = "S"
end if

if ls_flag_riepilogo_mensile ="X" then
	ls_flag_valorizza = "N"
	ls_flag_riepilogo_mensile = "S"
	lb_salta_negativi = true
	
	ll_mese_inizio = month(date(ldt_inizio))
	ll_mese = ll_mese_inizio
	ll_i = 0
	
	// calcolo le date di ogni fine mese
	do while true
		
		if ll_mese < ll_mese_inizio then 
			ll_anno ++
			ll_mese = 1
		else
			ll_anno = year(date(ldt_inizio))
		end if
		
		choose case ll_mese
			case 1,3,5,7,8,10,12
				ldt_fine_mese = datetime(date("31/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
			case 2
				if mod(ll_anno,4) = 0 then		// anno bisestile
					ldt_fine_mese = datetime(date("29/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
				else
					ldt_fine_mese = datetime(date("28/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
				end if		
			case else
				ldt_fine_mese = datetime(date("30/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
				
		end choose
		
		if ldt_fine_mese > ldt_fine then exit
		
		ll_i ++
		ldt_date_fine_mese[ll_i] = ldt_fine_mese
		ll_mese ++
		if ll_mese > 12 then ll_mese = 0
	loop	
	
end if

if isnull(ldt_inizio) then
	g_mb.messagebox("APICE","Data Inizio non impostata.", StopSign!)
	return
end if
if isnull(ldt_fine) then
	g_mb.messagebox("APICE","Data Fine non impostata.", StopSign!)
	return
end if
if ldt_inizio > ldt_fine then
	g_mb.messagebox("APICE","Date di inizio e fine elaborazione incongruenti fra loro.", StopSign!)
	return
end if

if ls_cod_prodotto_da = "" then setnull(ls_cod_prodotto_da)
if ls_cod_prodotto_a = "" then setnull(ls_cod_prodotto_a)
if ls_cod_comodo_2 = "" then setnull(ls_cod_comodo_2)
if ls_flag_classe = "" then setnull(ls_flag_classe)
if ls_flag_valorizza = "" then setnull(ls_flag_valorizza)

dw_report.reset()

if ls_flag_riepilogo_mensile ="S" then
	dw_report.dataobject = 'd_report_scheda_magazzino_mensile'
end if

dw_report.setredraw(false)
st_1.text = "Attendere la Preparazione del Report; l'operazione potrebbe richiedere parecchi minuti."

select rag_soc_1, 
       localita, 
		 provincia, 
		 partita_iva
into   :ls_azienda_rag_soc_1, 
       :ls_azienda_localita, 
		 :ls_azienda_provincia, 
		 :ls_partita_iva
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.object.st_azienda.text = ls_azienda_rag_soc_1
dw_report.object.st_indirizzo.text = ls_azienda_localita + "(" + ls_azienda_provincia + ")"
dw_report.object.st_piva.text = ls_partita_iva

dw_report.object.st_movimenti.text = "MOVIMENTI DAL " + string(ldt_inizio,"dd/mm/yyyy") + " AL " + string(ldt_fine,"dd/mm/yyyy")
if not isnull(ls_cod_prodotto_da) and not isnull(ls_cod_prodotto_a) then
	dw_report.object.st_prodotto.text = "DAL PRODOTTO " + ls_cod_prodotto_da + " AL PRODOTTO " + ls_cod_prodotto_a
elseif not isnull(ls_cod_prodotto_da) then
	dw_report.object.st_prodotto.text = "DAL PRODOTTO " + ls_cod_prodotto_da + " AL PRODOTTO " + ls_cod_prodotto_da
elseif not isnull(ls_cod_prodotto_a) then
	dw_report.object.st_prodotto.text = "DAL PRODOTTO " + ls_cod_prodotto_a + " AL PRODOTTO " + ls_cod_prodotto_a
else
	dw_report.object.st_prodotto.text = ""
end if
if not isnull(ls_cod_comodo_2) then
	dw_report.object.st_comodo.text = "COD COMODO " + ls_cod_comodo_2
else
	dw_report.object.st_comodo.text = ""
end if
if not isnull(ls_flag_classe) then
	dw_report.object.st_classe.text = "PRODOTTI DI CLASSE " + ls_flag_classe
else
	dw_report.object.st_classe.text = ""	
end if
if not isnull(ls_cod_deposito) then
	
	select des_deposito
	into   :ls_des_deposito
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_deposito = :ls_cod_deposito;
		
	dw_report.object.st_deposito.text = "VALIDO PER IL DEPOSITO " + ls_des_deposito
else
	dw_report.object.st_deposito.text = "VALIDO PER TUTTI I DEPOSITI"
end if
// prendo le date di chiusura e controllo che la stampa del magazzino sia successiva

select data_chiusura_periodica,
       data_chiusura_annuale
into   :ldt_data_chiusura_periodica,
       :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;


if ls_flag_lifo <> "S" then setnull(ls_flag_lifo)

// *** filtro i prodotti

lds_prodotti = create datastore
lds_prodotti.dataobject = "d_ds_report_scheda_magazzino_prodotti"
lds_prodotti.settransobject( sqlca)
ll_return = lds_prodotti.retrieve( s_cs_xx.cod_azienda, ls_cod_prodotto_da, ls_cod_prodotto_a, ls_flag_fiscale, ls_flag_classe, ls_cod_comodo_2, ls_flag_lifo)

if ll_return < 0 then
	g_mb.messagebox( "APICE", "Errore durante la ricerca dei prodotti iniziali: " + sqlca.sqlerrtext)
	destroy lds_prodotti
	return
end if

for ll_i = 1 to lds_prodotti.rowcount()       // ** filtro con gli altri dati
	
	ls_cod_prodotto_mov = lds_prodotti.getitemstring( ll_i, 1)
	
	select des_prodotto,
	       cod_misura_mag
	into   :ls_des_prodotto,
	       :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto_mov;

	if isnull(ls_cod_prodotto_mov) then ls_cod_prodotto_mov = ""
	if isnull(ls_des_prodotto) then ls_des_prodotto = ""
	
	st_1.text = "Prodotto: " + ls_cod_prodotto_mov + " " + ls_des_prodotto
	
	// Arrivo fino al mese che va in negativo, poi salto tutto
	if lb_salta_negativi then
		
		for ll_ind_mese = 1 to upperbound(ldt_date_fine_mese)
		
			// lancio un inventario con data = data fine
			for ll_y = 1 to 14
				ld_quant_val[ll_y] = 0
			next
			
			ls_chiave[] = ls_null_vett[]
			ld_giacenza_stock[] = ld_null[]
			ld_costo_medio_stock[] = ld_null[]
			ld_quan_costo_medio_stock[] = ld_null[]
			
			luo_magazzino = CREATE uo_magazzino			
					
			ll_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_mov, ldt_date_fine_mese[ll_ind_mese], "", ld_quant_val, ls_errore, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
			
			destroy luo_magazzino
			
			if ll_return <0 then
				g_mb.messagebox( "APICE", "Impossibile calcolare il riporto: " + ls_errore)
			end if
			
			ld_saldo_finale = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			
			// passo ad altro prodotto
			if ld_saldo_finale < 0 then
				if ll_ind_mese = 1 then 
					continue // la scheda è negativa dal primo mese
				else
					// arrivo fino al mes eprima del negativo
					ldt_fine = ldt_date_fine_mese[ll_ind_mese - 1]
					exit
				end if
			else
				ldt_fine = ldt_date_fine_mese[ll_ind_mese]
			end if
			
		next		
		
	end if
	
	// ---------------------------------------------------------------------------
	
	if ls_flag_riepilogo_mensile = "N" then
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga",1)
		dw_report.setitem( ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
		dw_report.setitem( ll_riga, "des_prodotto", ls_des_prodotto)
		dw_report.setitem( ll_riga, "codice_um", ls_cod_misura_mag)
	end if
		
	lds_movimenti = create datastore
	lds_movimenti.dataobject = "d_ds_report_scheda_magazzino_mov"
	lds_movimenti.settransobject( sqlca)
	ll_return = lds_movimenti.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto_mov, ldt_inizio, ldt_fine, ls_cod_deposito)

	if ll_return < 0 then
		g_mb.messagebox( "APICE", "Errore durante la ricerca dei movimenti dei prodotti iniziali: " + sqlca.sqlerrtext)
		destroy lds_movimenti
		destroy lds_prodotti
		return
	end if
	
	ld_carico_tot = 0
	ld_scarico_tot = 0
	ld_saldo_tot = 0
	
	if lds_movimenti.rowcount() < 1 then
		
		if ls_flag_riepilogo_mensile = "N" then
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
		end if			
		// **** in questo punto se la data inizio periodo è maggiore alla (data chiusura periodica + 1) oppure è maggiore
		//      della data chiusura annuale allora devo mettere il riporto dell'anno precedente.
		if ldt_inizio > ldt_data_chiusura_periodica and date(ldt_inizio) > relativedate(date(ldt_data_chiusura_annuale),1) then
				
				
			luo_magazzino = CREATE uo_magazzino			

			for ll_y = 1 to 14
				ld_quant_val[ll_y] = 0
			next
			ls_chiave[] = ls_null_vett[]
			ld_giacenza_stock[] = ld_null[]
			ld_costo_medio_stock[] = ld_null[]
			ld_quan_costo_medio_stock[] = ld_null[]
					
			ll_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_mov, datetime(date(relativedate(date(ldt_inizio),-1)),00:00:00), "", ld_quant_val, ls_errore, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
			
			destroy luo_magazzino
			
			if ll_return <0 then
				g_mb.messagebox( "APICE", "Impossibile calcolare il riporto: " + ls_errore)
			end if
			
			ld_riporto = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
		
			// *** inserisco la riga del riporto
			ld_saldo_tot += ld_riporto
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
			dw_report.setitem( ll_riga, "mese", "RIPORTO")
			dw_report.setitem( ll_riga, "data_registrazione", ldt_null)
			dw_report.setitem( ll_riga, "cod_movimento", ls_null)
			dw_report.setitem( ll_riga, "des_movimento", "RIPORTO al " + string(relativedate(date(ldt_inizio),-1), "dd/mm/yyyy") )		
			dw_report.setitem( ll_riga, "carico", 0)
			dw_report.setitem( ll_riga, "scarico", 0)
			
			if ls_flag_riepilogo_mensile ="S" then
				dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
				dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
				dw_report.setitem(ll_riga, "codice_um", ls_cod_misura_mag)
				dw_report.setitem(ll_riga, "carico", ld_saldo_tot)
			else		
				dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)
			end if
			destroy luo_magazzino				
				
		end if		
		
		if not isnull(ls_flag_valorizza) and ls_flag_valorizza <> "" and ls_flag_valorizza = "S" then
			
			for ll_return = 1 to 14
				ld_quant_val[ll_return] = 0
			next
			
			ls_chiave[] = ls_null_vett[]
			ld_giacenza_stock[] = ld_null[]
			ld_costo_medio_stock[] = ld_null[]
			ld_quan_costo_medio_stock[] = ld_null[]

			luo_magazzino = CREATE uo_magazzino
			ll_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_mov, ldt_fine, "", ld_quant_val, ls_errore, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
			destroy luo_magazzino
				
			if ll_return <0 then
				g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto_mov, ls_errore)
				continue
			end if
				
			if ld_quant_val[12] > 0 then
				ld_costo = ld_quant_val[13] / ld_quant_val[12]
			else						
				select costo_medio_ponderato
				into   :ld_costo
				from   lifo
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto_mov and
						 anno_lifo = (select max(anno_lifo)
										  from   lifo
										  where  cod_azienda = :s_cs_xx.cod_azienda and
													cod_prodotto = :ls_cod_prodotto_mov);
																	
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto_mov, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
					continue
				elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
					ld_costo = 0
				end if
							
			end if
			
			if ls_flag_riepilogo_mensile = "N" then
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem( ll_riga, "flag_tipo_riga", 5)
				dw_report.setitem(ll_riga, "costo_medio", ld_costo)		
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem( ll_riga, "flag_tipo_riga", 6)
				dw_report.setitem(ll_riga, "totale", round(ld_costo * ld_saldo_tot,2))				
			end if
			
		end if		
		
		continue
	end if
	
	for ll_j = 1 to lds_movimenti.rowcount()
		
		if ll_j = 1 then
			if ls_flag_riepilogo_mensile = "N" then
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
			end if			
			// **** in questo punto se la data inizio periodo è maggiore alla (data chiusura periodica + 1) oppure è maggiore
			//      della data chiusura annuale allora devo mettere il riporto dell'anno precedente.
			if ldt_inizio > ldt_data_chiusura_periodica and date(ldt_inizio) > relativedate(date(ldt_data_chiusura_annuale),1) then
				
				for ll_return = 1 to 14
					ld_quant_val[ll_return] = 0
				next				
				ls_chiave[] = ls_null_vett[]
				ld_giacenza_stock[] = ld_null[]
				ld_costo_medio_stock[] = ld_null[]
				ld_quan_costo_medio_stock[] = ld_null[]
				luo_magazzino = CREATE uo_magazzino			
				ll_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_mov, datetime(date(relativedate(date(ldt_inizio),-1)),00:00:00), "", ld_quant_val, ls_errore, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
				destroy luo_magazzino
				
				if ll_return <0 then
					g_mb.messagebox( "APICE", "Impossibile calcolare il riporto: " + ls_errore)
				end if
				
				ld_riporto = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			
				// *** inserisco la riga del riporto
				ld_saldo_tot += ld_riporto
				ll_riga = dw_report.insertrow(0)
						
				dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
				dw_report.setitem( ll_riga, "mese", "0 - RIPORTO")// DA ERRORE....
				//dw_report.setitem( ll_riga, "des_movimento", "0 - RIPORTO")
				dw_report.setitem( ll_riga, "data_registrazione", ldt_null)
				dw_report.setitem( ll_riga, "cod_movimento", ls_null)
				dw_report.setitem( ll_riga, "des_movimento", "RIPORTO al " + string(relativedate(date(ldt_inizio),-1), "dd/mm/yyyy") )		
				dw_report.setitem( ll_riga, "carico", 0)
				dw_report.setitem( ll_riga, "scarico", 0)
				
				if ls_flag_riepilogo_mensile ="S" then
					dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
					dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
					dw_report.setitem(ll_riga, "codice_um", ls_cod_misura_mag)
					dw_report.setitem(ll_riga, "carico", ld_saldo_tot)
				else		
					dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)
				end if
				
			//dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)				
			end if			
			
		end if
		
		ldt_data_registrazione = lds_movimenti.getitemdatetime( ll_j, "data_registrazione")
		ls_cod_tipo_movimento = lds_movimenti.getitemstring( ll_j, "cod_tipo_mov_det")
		ld_quantita = lds_movimenti.getitemnumber( ll_j, "quantita")
		
		select des_tipo_movimento
		into   :ls_des_movimento
		from   tab_tipi_movimenti_det
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_mov_det = :ls_cod_tipo_movimento;
		
		// *** trovo se è di scarico o di carico
		luo_magazzino = create uo_magazzino
		luo_magazzino.uof_tipo_movimento(ls_cod_tipo_movimento, ref ll_tipo_operazione, ref lb_1, ref lb_2, ref lb_3)
		destroy luo_magazzino
		
		//
		
		ll_riga = dw_report.insertrow(0)
		
		if ls_flag_riepilogo_mensile = "S" then
			dw_report.setitem( ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
			dw_report.setitem( ll_riga, "des_prodotto", ls_des_prodotto)
			dw_report.setitem( ll_riga, "codice_um", ls_cod_misura_mag)
			ll_mese = month(date(ldt_data_registrazione))
			ll_anno = year(date(ldt_data_registrazione))
			choose case ll_mese
				case 1
					ls_mese = "GENNAIO " + string(ll_anno)
				case 2
					ls_mese = "FEBBRAIO " + string(ll_anno)
				case 3
					ls_mese = "MARZO " + string(ll_anno)
				case 4
					ls_mese = "APRILE " + string(ll_anno)
				case 5
					ls_mese = "MAGGIO " + string(ll_anno)
				case 6
					ls_mese = "GIUGNO " + string(ll_anno)
				case 7
					ls_mese = "LUGLIO " + string(ll_anno)
				case 8
					ls_mese = "AGOSTO " + string(ll_anno)
				case 9
					ls_mese = "SETTEMBRE " + string(ll_anno)
				case 10
					ls_mese = "OTTOBRE " + string(ll_anno)
				case 11
					ls_mese = "NOVEMBRE " + string(ll_anno)
				case 12
					ls_mese = "DICEMBRE " + string(ll_anno)
			end choose
			
			dw_report.setitem( ll_riga, "mese", ls_mese)
			dw_report.setitem( ll_riga, "num_mese", (ll_anno * 100) + ll_mese)
		end if
		
		dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
		dw_report.setitem( ll_riga, "data_registrazione", ldt_data_registrazione)
		dw_report.setitem( ll_riga, "cod_movimento", ls_cod_tipo_movimento)
		dw_report.setitem( ll_riga, "des_movimento", ls_des_movimento)
		
		choose case ll_tipo_operazione
			case 1,5,19,13  		// movimenti di carico
				dw_report.setitem(ll_riga, "carico", ld_quantita)
				dw_report.setitem(ll_riga, "scarico", 0)
				ld_carico_tot += ld_quantita
			case 2,6,20,14	 		// rettifiche carico
				dw_report.setitem(ll_riga, "carico", 0)
				dw_report.setitem(ll_riga, "scarico",ld_quantita)
				ld_scarico_tot += ld_quantita
			case 3,7	 				// movimenti si scarico
				dw_report.setitem(ll_riga, "carico", 0)
				dw_report.setitem(ll_riga, "scarico", ld_quantita)
				ld_scarico_tot += ld_quantita
			case 4,8	 				// rettifiche scarico
				dw_report.setitem(ll_riga, "carico",ld_quantita)
				dw_report.setitem(ll_riga, "scarico", 0)
				ld_carico_tot += ld_quantita
			case else  // 9,15,10,16,11,17,12,18
				// movimenti di rettifica solo valore e quindi salti
				dw_report.setitem(ll_riga, "carico",0)
				dw_report.setitem(ll_riga, "scarico", 0)			
		end choose		
		ld_saldo_tot = ld_saldo_tot + ld_carico_tot - ld_scarico_tot
		dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)
		ld_scarico_tot = 0
		ld_carico_tot = 0
		
	next
	
	destroy lds_movimenti
	
	if ls_flag_riepilogo_mensile = "N" then
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga", 4)
		dw_report.setitem(ll_riga, "saldo_totale", ld_saldo_tot)
	end if
	
	if not isnull(ls_flag_valorizza) and ls_flag_valorizza <> "" and ls_flag_valorizza = "S" then
		
		for ll_return = 1 to 14
			ld_quant_val[ll_return] = 0
		next
		
		ls_chiave[] = ls_null_vett[]
		ld_giacenza_stock[] = ld_null[]
		ld_costo_medio_stock[] = ld_null[]
		ld_quan_costo_medio_stock[] = ld_null[]
		luo_magazzino = CREATE uo_magazzino
		ll_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_mov, ldt_fine, "", ld_quant_val, ls_errore, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
		destroy luo_magazzino
			
		if ll_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto_mov, ls_errore)
			continue
		end if
			
		if ld_quant_val[12] > 0 then
			ld_costo = ld_quant_val[13] / ld_quant_val[12]
		else						
			select costo_medio_ponderato
			into   :ld_costo
			from   lifo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_mov and
					 anno_lifo = (select max(anno_lifo)
									  from   lifo
									  where  cod_azienda = :s_cs_xx.cod_azienda and
												cod_prodotto = :ls_cod_prodotto);
																
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
				continue
			elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
				ld_costo = 0
			end if
						
		end if
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga", 5)
		dw_report.setitem(ll_riga, "costo_medio", ld_costo)		
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga", 6)
		dw_report.setitem(ll_riga, "totale", round(ld_costo * ld_saldo_tot,2))				
		
	end if
	
next

destroy lds_prodotti
rollback;

if ls_flag_riepilogo_mensile = "S" then
	dw_report.setsort("cod_prodotto A, num_mese A, cod_movimento A")
	dw_report.sort()
end if

if ls_flag_dett_mov = "N" then
	dw_report.modify("datawindow.trailer.3.height = 0")
end if

dw_report.groupcalc()
dw_report.setredraw(true)
dw_folder.fu_selecttab(2)
dw_report.change_dw_current()
dw_report.object.datawindow.print.preview = true
return

end subroutine

public function integer wf_prodotto_raggruppo (string as_cod_prodotto_raggruppo, uo_magazzino auo_mag, datetime adt_data_rif, string as_where, ref decimal ad_quant_val[], ref string as_errore, string as_flag_stock, ref string as_chiave[], ref decimal ad_giacenza_stock[], ref decimal ad_costo_medio[], ref decimal ad_quan_costo_medio[]);
datastore			lds_prodotti_ragg
string					ls_sql, ls_cod_prodotto_raggruppato, ls_chiave[], ls_null[]
long					ll_tot, ll_index
dec{5}				ld_fat_conv_rag_mag
integer				li_ret, li_y
dec{4}				ld_quant_val[], ld_giacenza_stock[], ld_null[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]


ls_sql = 	"select cod_prodotto, fat_conversione_rag_mag "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_prodotto_raggruppato='"+as_cod_prodotto_raggruppo+"'"

ll_tot = guo_functions.uof_crea_datastore(lds_prodotti_ragg, ls_sql, as_errore)

//reset variabili -----------------------------
for li_y = 1 to 14
	ad_quant_val[li_y] = 0
next
as_chiave[] = ls_null[]
ad_giacenza_stock[] = ld_null[]
ad_costo_medio[] = ld_null[]
ad_quan_costo_medio[] = ld_null[]


for ll_index=1 to ll_tot
	ls_cod_prodotto_raggruppato = lds_prodotti_ragg.getitemstring(ll_index, 1)
	ld_fat_conv_rag_mag = lds_prodotti_ragg.getitemdecimal(ll_index, 2)
	
	//reset variabili -----------------------------
	for li_y = 1 to 14
		ld_quant_val[li_y] = 0
	next
	ls_chiave[] = ls_null[]
	ld_giacenza_stock[] = ld_null[]
	ld_costo_medio_stock[] = ld_null[]
	ld_quan_costo_medio_stock[] = ld_null[]
	
	//tutte le quantita vanno sommate tenendo conto del fattore di conversione del prodotoo raggruppo
	// Q raggruppo   =   Q raggruppato   x   F conv raggruppato
	
	li_ret = auo_mag.uof_saldo_prod_date_decimal(	ls_cod_prodotto_raggruppato, &
																		datetime(date(relativedate(date(adt_data_rif),-1)),00:00:00), &
																		"", ld_quant_val[], &
																		as_errore, &
																		as_flag_stock, &
																		ls_chiave[], &
																		ld_giacenza_stock[], &
																		ld_costo_medio_stock[],&
																		ld_quan_costo_medio_stock[])
	
	if li_ret<0 then
		destroy lds_prodotti_ragg
		return -1
	end if
	
	ad_quant_val[1] += ld_quant_val[1] * ld_fat_conv_rag_mag
	ad_quant_val[4] += ld_quant_val[4] * ld_fat_conv_rag_mag
	ad_quant_val[6] += ld_quant_val[6] * ld_fat_conv_rag_mag
	ad_quant_val[12] += ld_quant_val[12] * ld_fat_conv_rag_mag
	ad_quant_val[13] += ld_quant_val[13] * ld_fat_conv_rag_mag

next

destroy lds_prodotti_ragg

return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = st_1
l_objects[4] = cb_reset
//l_objects[5] = cb_ricerca_prodotto
//l_objects[6] = cb_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,4)
dw_folder.fu_selecttab(1)

windowstate = maximized!

end event

on w_report_scheda_magazzino.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_reset=create cb_reset
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_reset
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
end on

on w_report_scheda_magazzino.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_reset)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170
end event

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_selezione, &
//                 "cod_documento", &
//                 sqlca, &
//                 "tab_documenti", &
//                 "cod_documento", &
//                 "des_documento", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

end event

type st_1 from statictext within w_report_scheda_magazzino
integer x = 69
integer y = 860
integer width = 2606
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_reset from commandbutton within w_report_scheda_magazzino
integer x = 2789
integer y = 860
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string   ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)

dw_selezione.setitem(1,"cod_prodotto_da", ls_null)
dw_selezione.setitem(1,"cod_prodotto_a", ls_null)
dw_selezione.setitem(1,"cod_comodo_2", ls_null)
dw_selezione.setitem(1,"cod_deposito", ls_null)
dw_selezione.setitem(1,"flag_classe", ls_null)
dw_selezione.setitem(1,"flag_valorizza", "N")
dw_selezione.setitem(1,"data_inizio", ldt_null)
dw_selezione.setitem(1,"data_fine", ldt_null)

end event

type cb_report from commandbutton within w_report_scheda_magazzino
integer x = 3177
integer y = 860
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;//parent.triggerevent("pc_retrieve")
wf_report()
end event

type dw_selezione from uo_cs_xx_dw within w_report_scheda_magazzino
integer x = 23
integer y = 140
integer width = 3589
integer height = 820
integer taborder = 20
string dataobject = "d_selezione_report_scheda_magazzino"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
	case "cod_prodotto_da"
		if not isnull(i_coltext) and i_coltext <> "" then
			dw_selezione.setitem( 1, "cod_prodotto_a", i_coltext)
		end if
	
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_scheda_magazzino
integer x = 18
integer y = 132
integer width = 3607
integer height = 1600
integer taborder = 10
string dataobject = "d_report_scheda_magazzino"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_scheda_magazzino
integer x = 9
integer y = 20
integer width = 3648
integer height = 1736
integer taborder = 20
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;st_1.text = ""
end event

