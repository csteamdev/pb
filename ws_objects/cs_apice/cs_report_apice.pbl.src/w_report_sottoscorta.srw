﻿$PBExportHeader$w_report_sottoscorta.srw
$PBExportComments$Report prodotti sottoscorta con ordini acquisto (da finire)
forward
global type w_report_sottoscorta from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_sottoscorta
end type
type cb_report from commandbutton within w_report_sottoscorta
end type
type cb_selezione from commandbutton within w_report_sottoscorta
end type
type dw_selezione from uo_cs_xx_dw within w_report_sottoscorta
end type
type dw_report from uo_cs_xx_dw within w_report_sottoscorta
end type
type sle_1 from singlelineedit within w_report_sottoscorta
end type
type cb_stampa from commandbutton within w_report_sottoscorta
end type
end forward

global type w_report_sottoscorta from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Prodotti Sottoscorta"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
sle_1 sle_1
cb_stampa cb_stampa
end type
global w_report_sottoscorta w_report_sottoscorta

type variables
datetime idt_data_consegna
end variables

forward prototypes
public function integer wf_ordinato_al (string fs_cod_prodotto, datetime fdt_data_rif, ref double fd_quan_ordinata, ref string fs_messaggio)
public function integer wf_cambio_acq (string as_cod_valuta, datetime adt_data_cambio, ref double fd_cambio_acq)
public function integer wf_prezzo_un (string fs_cod_prodotto, string fs_cod_fornitore, datetime fdt_data_consegna, double ad_quantita, ref double fd_prezzo_un, ref double fd_sconto_1, ref string fs_messaggio)
public function integer wf_calcola_quan_ordinare (string fs_cod_prodotto, double fd_quan_ordinata, double fd_disp, ref double fd_disp_teor, ref double fd_quan_ordinare, ref string fs_messaggio)
public function integer wf_controlla_quan (string fs_cod_prodotto, double fd_quan_ordinare, ref string fs_messaggio)
end prototypes

public function integer wf_ordinato_al (string fs_cod_prodotto, datetime fdt_data_rif, ref double fd_quan_ordinata, ref string fs_messaggio);// funzione che ritorna la quantità ordinata del prodotto fs_cod_prodotto negli ordini acq.
// non ancora chiusi, con data consegna <= fdt_data_rif
// ritorna = se ok

double ld_quan_ordinata

select sum(quan_ordinata)
into :ld_quan_ordinata   
from det_ord_acq   
where cod_azienda = :s_cs_xx.cod_azienda
  and cod_prodotto = :fs_cod_prodotto
  and data_consegna is not null
  and data_consegna <= :fdt_data_rif
  and quan_ordinata > quan_arrivata
  and flag_saldo = 'N';

//		fat_conversione,   
//		data_consegna,   
//		quan_arrivata,   
//		flag_saldo,   
//		data_consegna_fornitore  
//


return 0
end function

public function integer wf_cambio_acq (string as_cod_valuta, datetime adt_data_cambio, ref double fd_cambio_acq);double ld_cambio_acq
datetime ldt_data_cambio
long ll_i

select tab_valute.cambio_acq
into   :ld_cambio_acq 
from   tab_valute
where  tab_valute.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_valute.cod_valuta = :as_cod_valuta;

if sqlca.sqlcode = 0 then
	fd_cambio_acq = ld_cambio_acq

   declare cu_cambio cursor for select cambio_acq, data_cambio 
				from tab_cambi 
				where cod_azienda = :s_cs_xx.cod_azienda 
				and cod_valuta = :as_cod_valuta 
				and data_cambio <= :adt_data_cambio 
				order by cod_azienda, cod_valuta, data_cambio desc;
   open cu_cambio;
   ll_i = 0
   do while 0 = 0
      ll_i ++
      fetch cu_cambio into :ld_cambio_acq, :ldt_data_cambio;

      if sqlca.sqlcode <> 0 or ll_i = 1 then
         exit
      end if
   loop

   if sqlca.sqlcode = 0 then
      fd_cambio_acq = ld_cambio_acq
   end if
   close cu_cambio;
else
	fd_cambio_acq = 0
end if

return 0
end function

public function integer wf_prezzo_un (string fs_cod_prodotto, string fs_cod_fornitore, datetime fdt_data_consegna, double ad_quantita, ref double fd_prezzo_un, ref double fd_sconto_1, ref string fs_messaggio);// funzione al posto di f_listini_fornitori
// f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)
// legge prezzo e sconto_1 : prima in anag_prodotti, poi in listini_prodotti, poi in listini_fornitori 


double ld_prezzo_1, ld_prezzo_2, ld_prezzo_3, ld_prezzo_4, ld_prezzo_5, &
       ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, &
       ld_quantita_1, ld_quantita_2, ld_quantita_3, ld_quantita_4, ld_quantita_5, &
       ld_fat_conversione, ld_cambio_acq, ld_prezzo_1_prod, ld_sconto_1_prod
string ls_flag_tipo_det_acq, ls_flag_for_pref, ls_cod_misura, ls_cod_tipo_listino_prodotto, &
		 ls_cod_valuta  
datetime ldt_data_inizio_val
long ll_i

setnull(ld_prezzo_1)
setnull(ld_sconto_1)
fd_prezzo_un = 0
fd_sconto_1 = 0

if isnull(fs_cod_prodotto) then return 0
if isnull(fs_cod_fornitore) then return 0	// nota: il fornitore serve anche per la valuta ed il cambio di acquisto

select cod_tipo_listino_prodotto, cod_valuta  
into :ls_cod_tipo_listino_prodotto, :ls_cod_valuta  
from anag_fornitori
where cod_azienda = :s_cs_xx.cod_azienda
  and cod_fornitore = :fs_cod_fornitore;

if not isnull(ls_cod_valuta) then
	this.wf_cambio_acq ( ls_cod_valuta, fdt_data_consegna, ld_cambio_acq )
else
	fs_messaggio = "Definire la valuta per il fornitore " + fs_cod_fornitore
	return -1
end if

if ld_cambio_acq = 0 then 
	fs_messaggio = "Cambio di acquisto per il fornitore " + fs_cod_fornitore + " non definito"
	return -1
end if
select anag_prodotti.prezzo_acquisto,
       anag_prodotti.sconto_acquisto
into   :ld_prezzo_1,
       :ld_sconto_1
from   anag_prodotti
where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_prodotti.cod_prodotto = :fs_cod_prodotto;
   
if sqlca.sqlcode = 0 and ld_prezzo_1 <> 0 then
   ld_prezzo_1 = ld_prezzo_1 / ld_cambio_acq
   fd_prezzo_un = ld_prezzo_1
	ld_prezzo_1_prod = ld_prezzo_1
   fd_sconto_1 = ld_sconto_1
	ld_sconto_1_prod = ld_sconto_1
end if

if isnull(ls_cod_tipo_listino_prodotto) then
	fs_messaggio = "Definire il listino per il fornitore " + fs_cod_fornitore
	return -1
end if


//declare cu_listini_prodotti cursor for 
//	select   data_inizio_val, quantita_1, prezzo_1, sconto_1, quantita_2, prezzo_2, sconto_2, 
//		quantita_3, prezzo_3, sconto_3, quantita_4, prezzo_4, sconto_4, quantita_5, 
//		prezzo_5, sconto_5 
//	from listini_prodotti 
//	where cod_azienda = :s_cs_xx.cod_azienda 
//	and cod_prodotto = :fs_cod_prodotto 
//	and cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto 
//	and cod_valuta = :ls_cod_valuta 
//	and data_inizio_val <= :fdt_data_consegna 
//   order by cod_azienda, cod_prodotto, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val desc;
//
// open cu_listini_prodotti;
//ll_i = 0
//do while 0 = 0
//   ll_i = ll_i +1
//   fetch cu_listini_prodotti into :ldt_data_inizio_val, :ld_quantita_1, :ld_prezzo_1, 
//		:ld_sconto_1, :ld_quantita_2, :ld_prezzo_2, :ld_sconto_2, :ld_quantita_3, 
//		:ld_prezzo_3, :ld_sconto_3, :ld_quantita_4, :ld_prezzo_4, :ld_sconto_4, 
//		:ld_quantita_5, :ld_prezzo_5, :ld_sconto_5;
//
//   if sqlca.sqlcode <> 0 or ll_i = 1 then exit
//loop
//
//if sqlca.sqlcode = 0 and (ld_prezzo_1 <> 0 or &
//                          ld_prezzo_2 <> 0 or &
//                          ld_prezzo_3 <> 0 or &
//                          ld_prezzo_4 <> 0 or &
//                          ld_prezzo_5 <> 0) then
//   if ad_quantita <= ld_quantita_1 then
//      fd_prezzo_un = ld_prezzo_1
//		fd_sconto_1 = ld_sconto_1
//   elseif ad_quantita <= ld_quantita_2 then
//      fd_prezzo_un = ld_prezzo_2
//		fd_sconto_1 = ld_sconto_2
//   elseif ad_quantita <= ld_quantita_3 then
//      fd_prezzo_un = ld_prezzo_3
//		fd_sconto_1 = ld_sconto_3
//   elseif ad_quantita <= ld_quantita_4 then
//      fd_prezzo_un = ld_prezzo_4
//		fd_sconto_1 = ld_sconto_4
//   elseif ad_quantita <= ld_quantita_5 then
//      fd_prezzo_un = ld_prezzo_5
//		fd_sconto_1 = ld_sconto_5
//   end if
//end if
//close cu_listini_prodotti;
//if sqlca.sqlcode < 0 then
//	fs_messaggio = "Errore lettura listini acquisto prodotti"
//	return -1
//end if

setnull(ld_prezzo_1)

declare cu_nome cursor for 
	select data_inizio_val, quantita_1, prezzo_1, sconto_1, quantita_2, prezzo_2, 
		sconto_2, quantita_3, prezzo_3, sconto_3, quantita_4, prezzo_4, sconto_4, 
		quantita_5, prezzo_5, sconto_5, flag_for_pref, fat_conversione, cod_misura 
	from listini_fornitori 
	where cod_azienda = :s_cs_xx.cod_azienda 
	and cod_prodotto = :fs_cod_prodotto 
	and cod_fornitore = :fs_cod_fornitore 
	and cod_valuta = :ls_cod_valuta 
	and data_inizio_val <= :fdt_data_consegna &
   order by cod_azienda, cod_prodotto, cod_fornitore, cod_valuta, data_inizio_val desc;
open cu_nome;
ll_i = 0
do while 0 = 0
   ll_i = ll_i + 1
   fetch cu_nome into :ldt_data_inizio_val, :ld_quantita_1, :ld_prezzo_1, :ld_sconto_1, 
		:ld_quantita_2, :ld_prezzo_2, :ld_sconto_2, :ld_quantita_3, :ld_prezzo_3, 
		:ld_sconto_3, :ld_quantita_4, :ld_prezzo_4, :ld_sconto_4, :ld_quantita_5, 
		:ld_prezzo_5, :ld_sconto_5, :ls_flag_for_pref, :ld_fat_conversione, :ls_cod_misura;

   if sqlca.sqlcode <> 0 or ll_i = 1 then exit
loop

if sqlca.sqlcode = 0 and (ld_prezzo_1 <> 0 or &
                          ld_prezzo_2 <> 0 or &
                          ld_prezzo_3 <> 0 or &
                          ld_prezzo_4 <> 0 or &
                          ld_prezzo_5 <> 0) then
   if ad_quantita <= ld_quantita_1 then
      fd_prezzo_un = ld_prezzo_1
		fd_sconto_1 = ld_sconto_1
   elseif ad_quantita <= ld_quantita_2 then
      fd_prezzo_un = ld_prezzo_2
		fd_sconto_1 = ld_sconto_2
   elseif ad_quantita <= ld_quantita_3 then
      fd_prezzo_un = ld_prezzo_3
		fd_sconto_1 = ld_sconto_3
   elseif ad_quantita <= ld_quantita_4 then
      fd_prezzo_un = ld_prezzo_4
		fd_sconto_1 = ld_sconto_4
   elseif ad_quantita <= ld_quantita_5 then
      fd_prezzo_un = ld_prezzo_5
		fd_sconto_1 = ld_sconto_5
   end if


//   if ls_flag_for_pref = 'N' then
//      messagebox("Attenzione", "Fornitore non Preferenziale.", exclamation!, ok!)
//   end if
end if
close cu_nome;
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore lettura listini acquisto fornitori"
	return -1
end if

if isnull(ld_prezzo_1) then 
	if isnull(ld_prezzo_1_prod) then
		fd_prezzo_un = 0
		fd_sconto_1 = 0
	else
		fd_prezzo_un = ld_prezzo_1_prod
		fd_sconto_1 = ld_sconto_1_prod
	end if
end if

return 0
end function

public function integer wf_calcola_quan_ordinare (string fs_cod_prodotto, double fd_quan_ordinata, double fd_disp, ref double fd_disp_teor, ref double fd_quan_ordinare, ref string fs_messaggio);// funzione che ritorna la quantità da ordinare per il prodotto fs_cod_prodotto
double ld_disp, ld_disp_teor, ld_saldo_quan_inizio_anno, ld_prog_quan_entrata, ld_prog_quan_uscita, &
		ld_quan_impegnata, ld_quan_assegnata, ld_quan_anticipi, ld_quan_in_spedizione, &
		ld_scorta_massima, ld_quan_minima, ld_inc_ordine, ld_quan_massima, ld_lotto_economico, &
		ld_quan_ordine, ld_num_lotti

select scorta_massima,   
		quan_minima,   
		inc_ordine,   
		quan_massima,   
		lotto_economico  
into  :ld_scorta_massima,   
		:ld_quan_minima,   
		:ld_inc_ordine,   
		:ld_quan_massima,   
		:ld_lotto_economico      
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda
  and cod_prodotto = :fs_cod_prodotto;

//ld_disp = ld_saldo_quan_inizio_anno + ld_prog_quan_entrata - ld_prog_quan_uscita - &
//			ld_quan_impegnata - ld_quan_assegnata - ld_quan_anticipi - ld_quan_in_spedizione
//fd_disp = ld_disp

ld_disp_teor = fd_disp + fd_quan_ordinata
fd_disp_teor = ld_disp_teor

if not isnull(ld_scorta_massima) and ld_scorta_massima > 0 then
	ld_quan_ordine = ld_scorta_massima - ld_disp_teor
else
	fd_quan_ordinare = abs(ld_disp_teor)
	fs_messaggio = "Errore in anagrafica prodotti: la scorta massima deve essere non nulla"
	return -1
end if

if (not isnull(ld_quan_minima)) and ld_quan_minima > 0 and ld_quan_ordine < ld_quan_minima then
	ld_quan_ordine = ld_quan_minima
end if
if (not isnull(ld_quan_massima)) and ld_quan_massima > 0 and ld_quan_ordine > ld_quan_massima then
	ld_quan_ordine = ld_quan_massima
end if
if (not isnull(ld_inc_ordine)) and ld_inc_ordine > 0 then
   if mod(ld_quan_ordine, ld_inc_ordine) <> 0 then
		ld_num_lotti = mod(ld_quan_ordine, ld_inc_ordine) + 1
		ld_quan_ordine = ld_num_lotti * ld_inc_ordine
	end if
end if

fd_quan_ordinare = ld_quan_ordine

return 0
end function

public function integer wf_controlla_quan (string fs_cod_prodotto, double fd_quan_ordinare, ref string fs_messaggio);// funzione che controlla che la quantità da ordinare rispetti i vincoli posti in anag_prodotti, scheda acquisto

double ld_scorta_massima, ld_quan_minima, ld_inc_ordine, ld_quan_massima, ld_lotto_economico, &
		 ld_quan_ordine, ld_num_lotti

select scorta_massima,   
		quan_minima,   
		inc_ordine,   
		quan_massima,   
		lotto_economico  
into  :ld_scorta_massima,   
		:ld_quan_minima,   
		:ld_inc_ordine,   
		:ld_quan_massima,   
		:ld_lotto_economico      
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda
  and cod_prodotto = :fs_cod_prodotto;

if (not isnull(ld_quan_minima)) and ld_quan_minima > 0 and fd_quan_ordinare < ld_quan_minima then
	fs_messaggio = "Quantità minima da ordinare pari a " + string (ld_quan_minima)
	return -1
end if
if (not isnull(ld_quan_massima)) and ld_quan_massima > 0 and fd_quan_ordinare > ld_quan_massima then
	fs_messaggio = "Quantità massima da ordinare pari a " + string (ld_quan_massima)
	return -1
end if
if (not isnull(ld_inc_ordine)) and ld_inc_ordine > 0 then
   if mod(fd_quan_ordinare, ld_inc_ordine) <> 0 then
		ld_num_lotti = int(fd_quan_ordinare / ld_inc_ordine) + 1
		ld_quan_ordine = ld_num_lotti * ld_inc_ordine
		fs_messaggio = "La quantità da ordinare deve essere un multiplo di " + &
			string(ld_inc_ordine) + " : ordinare una quantità pari a " + string(ld_quan_ordine)
		return -1
	end if
end if

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

idt_data_consegna = datetime(relativedate(today(), 30))

this.x = 741
this.y = 885
this.width = 2163
this.height = 570

end event

on w_report_sottoscorta.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.sle_1=create sle_1
this.cb_stampa=create cb_stampa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.sle_1
this.Control[iCurrent+7]=this.cb_stampa
end on

on w_report_sottoscorta.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.sle_1)
destroy(this.cb_stampa)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_report, &
                 "cod_tipo_ord_acq", &
                 sqlca, &
                 "tab_tipi_ord_acq", &
                 "cod_tipo_ord_acq", &
                 "des_tipo_ord_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_annulla from commandbutton within w_report_sottoscorta
integer x = 1326
integer y = 340
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_sottoscorta
integer x = 1714
integer y = 340
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Trova"
end type

event clicked;string ls_tipo_scortamin_livriord, ls_sql, ls_where, ls_cod_prodotto, ls_des_prodotto, &
		 ls_cod_misura_mag, ls_flag_sotto_scorta, ls_cod_fornitore, ls_cod_misura_acq, &
		 ls_messaggio
datetime ldt_data_ordinato
double ld_giacenza, ld_disp, ld_saldo_quan_inizio_anno, ld_prog_quan_entrata, &
		 ld_prog_quan_uscita, ld_quan_impegnata, ld_quan_assegnata, ld_quan_in_spedizione, &
		 ld_quan_anticipi, ld_lotto_economico, ld_livello_riordino, ld_scorta_minima, &
		 ld_scorta_massima, ld_fat_conversione_acq,  ld_prezzo_acquisto, ld_quan_minima, &
       ld_inc_ordine, ld_quan_massima, ld_tempo_app, ld_quan_ordinata, ld_disp_teor, &
		 ld_quan_ordinare, ld_sconto_1, ld_prezzo_un
long ll_riga, ll_prima_riga, ll_risp

ll_prima_riga = 1


ls_tipo_scortamin_livriord = dw_selezione.getitemstring(1,"tipo_scortamin_livriord")
ldt_data_ordinato = dw_selezione.getitemdatetime(1,"data_ordinato")

// ls_tipo_scortamin_livriord = { "LR", "SN"}, cioè Livello Riordino, Scorta miNima
if isnull(ls_tipo_scortamin_livriord) then ls_tipo_scortamin_livriord = "LR"

if isnull(ldt_data_ordinato)  then 
	g_mb.messagebox("Sottoscorta", "Errore inizializzazione data ordinato")
	return
end if


ls_sql = " select cod_prodotto, des_prodotto, cod_misura_mag, flag_sotto_scorta, " + &
         "saldo_quan_inizio_anno, prog_quan_entrata, prog_quan_uscita, quan_impegnata, " + &
			"quan_assegnata, quan_in_spedizione, quan_anticipi, lotto_economico, " + &
         "livello_riordino, scorta_minima, scorta_massima, cod_misura_acq, " + &
         "fat_conversione_acq, cod_fornitore, prezzo_acquisto, quan_minima, " + &
         "inc_ordine, quan_massima, tempo_app,  " + &
			"  ((saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita) " + &
			"- quan_impegnata - quan_anticipi - quan_assegnata -  quan_in_spedizione ) as disp " + &
    "from anag_prodotti " + &
   "where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

ls_where = "  and disp <>0 and disp < "

if ls_tipo_scortamin_livriord = "LR" then
	ls_sql = ls_sql + " and livello_riordino is not null and livello_riordino > 0 "
	ls_where = ls_where + "livello_riordino "
else
	ls_sql = ls_sql + "and scorta_minima is not null and scorta_minima > 0 "
	ls_where = ls_where + " scorta_minima "
end if

ls_sql = ls_sql + ls_where

// messagebox("debug", ls_sql)


declare cur_prod dynamic cursor for SQLSA ;
prepare SQLSA from :ls_sql;
open dynamic cur_prod;



ll_riga = 1

open cur_prod;
do while 0=0
	fetch cur_prod into  :ls_cod_prodotto, :ls_des_prodotto, :ls_cod_misura_mag, 
				:ls_flag_sotto_scorta, :ld_saldo_quan_inizio_anno, :ld_prog_quan_entrata, 
				:ld_prog_quan_uscita, :ld_quan_impegnata, :ld_quan_assegnata, 
				:ld_quan_in_spedizione, :ld_quan_anticipi, :ld_lotto_economico, 
         :ld_livello_riordino, :ld_scorta_minima, :ld_scorta_massima, :ls_cod_misura_acq, 
         :ld_fat_conversione_acq, :ls_cod_fornitore, :ld_prezzo_acquisto, :ld_quan_minima, 
         :ld_inc_ordine, :ld_quan_massima, :ld_tempo_app, :ld_disp ;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
		exit
	end if
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB " , SQLCA.SQLErrText)
		return
	end if
//	ld_disp = ((ld_saldo_quan_inizio_anno + ld_prog_quan_entrata - ld_prog_quan_uscita) &
//					- ld_quan_impegnata - ld_quan_anticipi - ld_quan_assegnata -  ld_quan_in_spedizione )

	ll_risp = wf_ordinato_al (ls_cod_prodotto, ldt_data_ordinato, ld_quan_ordinata, ls_messaggio)
	ll_risp = wf_calcola_quan_ordinare (ls_cod_prodotto, ld_quan_ordinata, ld_disp, ld_disp_teor, ld_quan_ordinare, ls_messaggio )
	ll_risp = wf_prezzo_un (ls_cod_prodotto, ls_cod_fornitore, ldt_data_ordinato, ld_quan_ordinare, ld_prezzo_un, ld_sconto_1, ls_messaggio )
	if ll_risp = -1 then 
		ld_prezzo_un = 0
		ld_sconto_1 = 0
		g_mb.messagebox("Errore calcolo prezzo unitario", ls_messaggio)
	end if
	sle_1.text = "prodotto " + ls_cod_prodotto + ": " + string(ld_disp_teor)

	if ll_prima_riga = 0 then ll_riga = dw_report.insertrow(0)
	dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
	dw_report.setitem(ll_riga, "cod_misura_mag", ls_cod_misura_mag)
	dw_report.setitem(ll_riga, "disponibilita_att", ld_disp)
	dw_report.setitem(ll_riga, "cod_fornitore", ls_cod_fornitore)
	dw_report.setitem(ll_riga, "cod_misura_acq", ls_cod_misura_acq)
	dw_report.setitem(ll_riga, "fat_conversione_acq", ld_fat_conversione_acq)
	dw_report.setitem(ll_riga, "livello_riordino", ld_livello_riordino)
	dw_report.setitem(ll_riga, "scorta_minima", ld_scorta_minima)
	dw_report.setitem(ll_riga, "scorta_massima", ld_scorta_massima)
	dw_report.setitem(ll_riga, "quan_minima", ld_quan_minima)
	dw_report.setitem(ll_riga, "inc_ordine", ld_inc_ordine)
	dw_report.setitem(ll_riga, "quan_massima", ld_quan_massima)
	dw_report.setitem(ll_riga, "quan_ordine", ld_quan_ordinare)
	dw_report.setitem(ll_riga, "ordinato_for", ld_quan_ordinata)
	dw_report.setitem(ll_riga, "disponibilita_teorica", ld_disp_teor)
	dw_report.setitem(ll_riga, "prezzo_acquisto", ld_prezzo_un)
	ll_prima_riga = 0
loop
close cur_prod ;

dw_report.setsort( "cod_fornitore A, cod_prodotto A")
dw_report.sort()




dw_selezione.hide()

parent.x = 50
parent.y = 50
parent.width = 3553
parent.height = 1665


dw_report.show()
cb_selezione.show()
dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_sottoscorta
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;
dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2163
parent.height = 570

dw_report.hide()
dw_report.reset()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_sottoscorta
integer x = 23
integer y = 20
integer width = 2080
integer height = 220
integer taborder = 20
string dataobject = "d_selezione_sottoscorta"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_tipo_scortamin_livriord
datetime ldt_data_ordinato

setitem(1, "tipo_scortamin_livriord", "LR")
ldt_data_ordinato = datetime(relativedate(today(), 30))
setitem(1, "data_ordinato", ldt_data_ordinato)


end event

type dw_report from uo_cs_xx_dw within w_report_sottoscorta
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_sottoscorta"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_key;call super::ue_key;//string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
//       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
//long ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
//double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo
//datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "cod_fornitore"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_fornitore(dw_report,"cod_fornitore")		
		end if
end choose


end event

event itemchanged;call super::itemchanged;if i_extendmode then
  string ls_cod_prodotto, ls_flag_decimali, ls_messaggio, ls_cod_fornitore
  double ld_prezzo_un, ld_sconto_1, ld_quan_ordine, ld_verifica
  
  choose case i_colname
      case "quan_ordine"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
			
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_ordine", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if
			if wf_controlla_quan ( ls_cod_prodotto, double(i_coltext), ls_messaggio) < 0 then
				g_mb.messagebox("Stampa Sottoscorta Attenzione:", ls_messaggio)
				return 2
			end if
			if wf_prezzo_un ( ls_cod_prodotto, ls_cod_fornitore, idt_data_consegna, double(i_coltext), ld_prezzo_un, ld_sconto_1, ls_messaggio ) < 0 then
				g_mb.messagebox("Errore Stampa Sottoscorta", ls_messaggio)
				return 2
			else
				this.setitem(i_rownbr, "prezzo_acquisto", ld_prezzo_un)
			end if
			
      case "cod_fornitore"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quan_ordine = this.getitemnumber(i_rownbr, "quan_ordine")
			select count(*)
			into :ld_verifica
			from anag_fornitori
			where cod_azienda = :s_cs_xx.cod_azienda
			  and cod_fornitore = :i_coltext;
			if ld_verifica = 0 then
				g_mb.messagebox("Stampa Sottoscorta", "Fornitore non presente in anagrafica fornitori")
				return 2
			end if
			
			if wf_prezzo_un ( ls_cod_prodotto, i_coltext, idt_data_consegna, ld_quan_ordine, ld_prezzo_un, ld_sconto_1, ls_messaggio ) < 0 then
				g_mb.messagebox("Errore Stampa Sottoscorta", ls_messaggio)
				this.setitem(i_rownbr, "prezzo_acquisto", 0)
			else
				this.setitem(i_rownbr, "prezzo_acquisto", ld_prezzo_un)
			end if
			
			
	end choose
	 accepttext()
	 resetupdate()
	 Reset_DW_Modified(c_ResetChildren)
end if
end event

type sle_1 from singlelineedit within w_report_sottoscorta
integer x = 23
integer y = 240
integer width = 2057
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean autohscroll = false
end type

type cb_stampa from commandbutton within w_report_sottoscorta
integer x = 2743
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Filtra"
end type

event clicked;
dw_report.setfilter("flag_ordine ='S'")
dw_report.Filter()



end event

