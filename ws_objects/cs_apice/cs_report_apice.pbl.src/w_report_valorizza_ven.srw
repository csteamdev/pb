﻿$PBExportHeader$w_report_valorizza_ven.srw
$PBExportComments$Report Valorizzzione del venduto
forward
global type w_report_valorizza_ven from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_valorizza_ven
end type
type cb_report from commandbutton within w_report_valorizza_ven
end type
type cb_selezione from commandbutton within w_report_valorizza_ven
end type
type st_1 from statictext within w_report_valorizza_ven
end type
type dw_selezione from uo_cs_xx_dw within w_report_valorizza_ven
end type
type dw_report from uo_cs_xx_dw within w_report_valorizza_ven
end type
end forward

global type w_report_valorizza_ven from w_cs_xx_principale
integer x = 37
integer y = 236
integer width = 3566
integer height = 1768
string title = "Report Valorizzazione Costo Venduto"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
st_1 st_1
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_valorizza_ven w_report_valorizza_ven

forward prototypes
public function integer wf_calcola_riga_fat_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_fat_ven, decimal fd_sconto, string fs_cod_pagamento, ref decimal fd_imponibile_iva, ref string fs_messaggio)
end prototypes

public function integer wf_calcola_riga_fat_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_fat_ven, decimal fd_sconto, string fs_cod_pagamento, ref decimal fd_imponibile_iva, ref string fs_messaggio);// wf_calcola_riga_fat_ven ( long fl_anno_registrazione, long fl_num_registrazione, 
// long fl_prog_riga_fat_ven, double fd_sconto, string fs_cod_pagamento, double fd_imponibile_iva, 
// string fs_messaggio )

// funzione che calcola l'imponibile iva di una riga del dettaglio fattura di vendita
// al netto degli sconti, il cui cod_prodotto non è null

decimal ld_sconto_pagamento, ld_quan_documento, ld_prezzo_vendita, ld_sconto_1, ld_sconto_2, &
		ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
		ld_sconto_9, ld_sconto_10, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		ld_val_sconto_2 , ld_val_riga_sconto_2 , ld_val_sconto_3, ld_val_riga_sconto_3, &
		ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconti_riga, ld_val_sconto_testata, &
		ld_val_riga_sconto_testata, ld_tot_sconti_commerciali, ld_val_sconto_pagamento, &
		ld_tot_sconti_cassa, ld_imponibile_riga 


select tab_pagamenti.sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
		 tab_pagamenti.cod_pagamento = :fs_cod_pagamento;

select quan_fatturata, prezzo_vendita, sconto_1,   sconto_2,   sconto_3,   sconto_4,   
		 sconto_5,   sconto_6,   sconto_7,   sconto_8,   sconto_9,   sconto_10  
into :ld_quan_documento, :ld_prezzo_vendita, :ld_sconto_1, :ld_sconto_2, :ld_sconto_3,   
         :ld_sconto_4, :ld_sconto_5,   :ld_sconto_6,   :ld_sconto_7,   :ld_sconto_8,   
         :ld_sconto_9, :ld_sconto_10  
    FROM det_fat_ven 
where cod_azienda = :s_cs_xx.cod_azienda
  and anno_registrazione = :fl_anno_registrazione
  and num_registrazione = :fl_num_registrazione
  and prog_riga_fat_ven = :fl_prog_riga_fat_ven;

ld_val_riga = ld_quan_documento * ld_prezzo_vendita
ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
ld_val_sconti_riga = ld_val_riga - ld_val_riga_sconto_10


ld_val_sconto_testata = (ld_val_riga_sconto_10 * fd_sconto) / 100
ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
ld_tot_sconti_commerciali = ld_val_sconti_riga + ld_val_sconto_testata
ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
ld_tot_sconti_cassa = ld_tot_sconti_cassa + ld_val_sconto_pagamento
ld_imponibile_riga = ld_val_riga - &
		ld_val_sconto_testata - &
		ld_val_sconto_pagamento - &
		ld_val_sconti_riga

ld_imponibile_riga = round(ld_imponibile_riga, 2)

fd_imponibile_iva = ld_imponibile_riga

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report


this.width = 1985
this.height = 689

end event

on w_report_valorizza_ven.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.st_1=create st_1
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_selezione
this.Control[iCurrent+6]=this.dw_report
end on

on w_report_valorizza_ven.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.st_1)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_cat_mer", &
							sqlca, &
							"tab_cat_mer", &
							"cod_cat_mer", &
                     "des_cat_mer", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_agente", &
							sqlca, &
							"anag_agenti", &
							"cod_agente", &
                     "rag_soc_1", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_annulla from commandbutton within w_report_valorizza_ven
integer x = 1166
integer y = 480
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_valorizza_ven
integer x = 1554
integer y = 480
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;integer  li_risp

long     ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, ll_num_documento, &
		   ll_i, ll_pos

string   ls_cod_prodotto, ls_cod_cat_mer, ls_cod_cliente, ls_cod_agente, ls_select, &
		   ls_cod_pagamento, ls_messaggio, ls_des_prodotto, ls_data, ls_selezione, &
		   ls_des_cat_mer, ls_rag_soc_1_cli, ls_rag_soc_1_age, ls_cod_valuta, ls_formato
		 
dec{4}   ld_quan_fatturata, ld_sconto, ld_imponibile_iva, ld_prezzo_acquisto, ld_percdiff, &
			ld_provvigione_1, ld_prezzo_vendita

datetime ldt_data_fattura_da, ldt_data_fattura_a, ldt_data_fattura

dw_selezione.accepttext()
st_1.text = ""
ls_cod_prodotto = dw_selezione.getitemstring(1,"cod_prodotto")
ls_cod_cat_mer = dw_selezione.getitemstring(1,"cod_cat_mer")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
ldt_data_fattura_da= dw_selezione.getitemdatetime(1,"data_fattura_da")
ldt_data_fattura_a   = dw_selezione.getitemdatetime(1,"data_fattura_a")

ls_selezione = "data fatura da: " + string(ldt_data_fattura_da, "dd-mm-yyyy") + &
	" a " +	string(ldt_data_fattura_a, "dd-mm-yyyy") + " , "
	
if isnull(ls_cod_prodotto) and isnull(ls_cod_cat_mer) and isnull(ls_cod_cliente) and isnull(ls_cod_agente) then
	li_risp = g_mb.messagebox("Valorizzazione venduto", "Attenzione: la selezione caricherà molti dati.~r~n" + &
					"Si vuole proseguire?", question!, yesNo!,2)
	if li_risp = 2 then return
end if

ls_select = "select det_fat_ven.prog_riga_fat_ven, det_fat_ven.cod_prodotto, " + & 
	"det_fat_ven.quan_fatturata,tes_fat_ven.num_documento, tes_fat_ven.data_fattura, " + &
	"tes_fat_ven.anno_registrazione, tes_fat_ven.num_registrazione, " + &
	" tes_fat_ven.sconto, tes_fat_ven.cod_pagamento, det_fat_ven.provvigione_1, " + &
	" det_fat_ven.prezzo_vendita, tes_fat_ven.cod_valuta " +&
"from det_fat_ven, tes_fat_ven " + &
"where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
	"tes_fat_ven.data_fattura <= '" + string(ldt_data_fattura_a, "yyyy-mm-dd") +"' and " + &
	"tes_fat_ven.data_fattura >= '" + string(ldt_data_fattura_da, "yyyy-mm-dd") + "' and " + &
	"(det_fat_ven.cod_azienda = tes_fat_ven.cod_azienda and " + & 
	"det_fat_ven.anno_registrazione = tes_fat_ven.anno_registrazione and " + &
	"det_fat_ven.num_registrazione = tes_fat_ven.num_registrazione) and " + &
	"det_fat_ven.cod_prodotto is not null "

if not isnull(ls_cod_prodotto) then 	
	ls_select = ls_select + " and det_fat_ven.cod_prodotto = '" + ls_cod_prodotto +"' " 
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :ls_cod_prodotto;
		
	ls_selezione = ls_selezione + "prodotto = " + ls_cod_prodotto +" "
	if not isnull(ls_des_prodotto) then ls_selezione = ls_selezione + ls_des_prodotto +", "
end if

if not isnull(ls_cod_cat_mer) then 	
	ls_select = ls_select + &
	" and det_fat_ven.cod_prodotto in (select cod_prodotto from anag_prodotti  " + &
				" where anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + &
				"' and  cod_cat_mer = '" + ls_cod_cat_mer + "') "
	select des_cat_mer
	into :ls_des_cat_mer
	from tab_cat_mer
	where cod_azienda = :s_cs_xx.cod_azienda and cod_cat_mer = :ls_cod_cat_mer;
	
	ls_selezione = ls_selezione + "categoria mer. = " + ls_cod_cat_mer +" "
	if not isnull(ls_des_cat_mer) then ls_selezione = ls_selezione + ls_des_cat_mer +", "
end if

if not isnull(ls_cod_cliente) then 	
	ls_select = ls_select + " and tes_fat_ven.cod_cliente = '" + ls_cod_cliente +"' " 
	select rag_soc_1
	into :ls_rag_soc_1_cli
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :ls_cod_cliente;
	
	ls_selezione = ls_selezione + "cliente = " + ls_cod_cliente + " "
	if not isnull(ls_rag_soc_1_cli) then ls_selezione = ls_selezione + ls_rag_soc_1_cli
end if

if not isnull(ls_cod_agente) then 
	ls_select = ls_select + &
	" and tes_fat_ven.cod_cliente in (select cod_cliente from anag_clienti  " + &
	" where anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
	"	( cod_agente_1 = '" + ls_cod_agente +"' or cod_agente_2 = '" + ls_cod_agente +"')) "
	ls_selezione = ls_selezione + "agente = " + ls_cod_agente
	
	select rag_soc_1
	into :ls_rag_soc_1_age
	from anag_agenti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_agente = :ls_cod_agente;
	if not isnull(ls_rag_soc_1_age) then ls_selezione = ls_selezione + " " + ls_rag_soc_1_age
	
end if

SetPointer (HourGlass!)

DECLARE my_cursor DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_select;

OPEN DYNAMIC my_cursor;

DO while 0=0
	FETCH my_cursor INTO :ll_prog_riga_fat_ven, :ls_cod_prodotto, :ld_quan_fatturata,
			:ll_num_documento, :ldt_data_fattura, :ll_anno_registrazione, :ll_num_registrazione,
			:ld_sconto, 	:ls_cod_pagamento, :ld_provvigione_1, :ld_prezzo_vendita, :ls_cod_valuta;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore Stampa Listini","Errore nel DB "+SQLCA.SQLErrText)
		return -1
	end if
	
	 li_risp = wf_calcola_riga_fat_ven ( ll_anno_registrazione, ll_num_registrazione, &
			 ll_prog_riga_fat_ven, ld_sconto, ls_cod_pagamento, ld_imponibile_iva, ls_messaggio)
	if li_risp < 0 then 
		g_mb.messagebox("Valorizzazione venduto", "Errore in calcolo fattura " + &
				string(ll_num_registrazione) + " del " + string(ldt_data_fattura))
		ld_imponibile_iva = -1
	end if
	
	select des_prodotto, prezzo_acquisto
	into   :ls_des_prodotto, :ld_prezzo_acquisto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
	       cod_prodotto =:ls_cod_prodotto;
	
	select formato
	into   :ls_formato
	from   tab_valute
	where  cod_azienda =:s_cs_xx.cod_azienda and
	       cod_valuta = :ls_cod_valuta;
			 
	ll_i = dw_report.insertrow(0)
	st_1.text = ls_cod_prodotto
	
	if (ld_quan_fatturata = 0) or (ld_prezzo_acquisto=0) then 
		ld_percdiff =0
	else
		ld_percdiff = ((ld_imponibile_iva/ld_quan_fatturata) - ld_prezzo_acquisto)*100 / ld_prezzo_acquisto
	end if
	
	dw_report.setitem(ll_i,"data_fattura", ldt_data_fattura )
	dw_report.setitem(ll_i,"num_documento", ll_num_documento )
	dw_report.setitem(ll_i,"cod_prodotto", ls_cod_prodotto )
	dw_report.setitem(ll_i,"des_prodotto", ls_des_prodotto )
	dw_report.setitem(ll_i,"quan_fatturata", ld_quan_fatturata )
	dw_report.setitem(ll_i,"imponibile_riga", ld_imponibile_iva)
	dw_report.setitem(ll_i,"costo_venduto", ld_prezzo_acquisto)
	dw_report.setitem(ll_i,"perc_differenza", ld_percdiff)
	dw_report.setitem(ll_i,"perc_provvigione", ld_provvigione_1)
	dw_report.setitem(ll_i,"prezzo_unitario", ld_prezzo_vendita)
	dw_report.setitem(ll_i,"formato", ls_formato)
	
	
LOOP

CLOSE my_cursor ;

dw_report.setsort("cod_prodotto A")
dw_report.sort()

ll_pos = 1

do
	
   ll_pos = pos(ls_selezione,"'",ll_pos)
	
   if ll_pos <> 0 then
      ls_selezione = replace(ls_selezione,ll_pos,1,"~'")	
      ll_pos = ll_pos + 2
   end if	
	
loop while ll_pos <> 0

ll_pos = 1

do
	
   ll_pos = pos(ls_selezione,"~"",ll_pos)
	
   if ll_pos <> 0 then
      ls_selezione = replace(ls_selezione,ll_pos,1,"~~~"")	
      ll_pos = ll_pos + 2
   end if	
	
loop while ll_pos <> 0

dw_report.object.selezione.text = ls_selezione

SetPointer (Arrow!)
dw_selezione.hide()
dw_selezione.object.b_ricerca_cliente.visible=false
dw_selezione.object.b_ricerca_prodotto.visible=false

//parent.x = 50
//parent.y = 50
parent.width = 3566
parent.height = 1769

dw_report.show()

cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_valorizza_ven
integer x = 3131
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_report.RowsDiscard (1, dw_report.rowcount(), primary!)
dw_selezione.show()
dw_selezione.object.b_ricerca_cliente.visible=true
dw_selezione.object.b_ricerca_prodotto.visible=true

parent.x = 633
parent.y = 265
parent.width = 1985
parent.height = 689

dw_report.hide()
cb_selezione.hide()
dw_selezione.change_dw_current()
end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type st_1 from statictext within w_report_valorizza_ven
integer x = 46
integer y = 480
integer width = 1070
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_valorizza_ven
integer x = 23
integer y = 20
integer width = 1897
integer height = 440
integer taborder = 10
string dataobject = "d_valorizza_ven_selezione"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;if i_extendmode then

	string ls_null
	dw_selezione.accepttext()
	setnull(ls_null)
	choose case i_colname
		case "cod_cliente"
			this.setitem(1,"cod_agente", ls_null)
		case "cod_agente"
			this.setitem(1,"cod_cliente",ls_null)
		case "cod_prodotto"
			this.setitem(1,"cod_cat_mer", ls_null)
		case "cod_cat_mer"
			this.setitem(1,"cod_prodotto", ls_null)
	end choose
	
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_valorizza_ven
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1520
integer taborder = 30
string dataobject = "d_report_valorizza_costo"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

