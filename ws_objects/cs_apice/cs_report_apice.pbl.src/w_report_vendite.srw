﻿$PBExportHeader$w_report_vendite.srw
$PBExportComments$finestra report prodotti venduti movimentati
forward
global type w_report_vendite from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_vendite
end type
type cb_report from commandbutton within w_report_vendite
end type
type cb_selezione from commandbutton within w_report_vendite
end type
type dw_report from uo_cs_xx_dw within w_report_vendite
end type
type sle_1 from singlelineedit within w_report_vendite
end type
type dw_selezione from uo_cs_xx_dw within w_report_vendite
end type
end forward

global type w_report_vendite from w_cs_xx_principale
integer width = 3566
integer height = 1676
string title = "Report Prodotti venduti"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
sle_1 sle_1
dw_selezione dw_selezione
end type
global w_report_vendite w_report_vendite

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2291
this.height = 709

end event

on w_report_vendite.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.sle_1=create sle_1
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.sle_1
this.Control[iCurrent+6]=this.dw_selezione
end on

on w_report_vendite.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.sle_1)
destroy(this.dw_selezione)
end on

type cb_annulla from commandbutton within w_report_vendite
integer x = 1463
integer y = 480
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_vendite
integer x = 1851
integer y = 480
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;
datetime					ldt_data_da, ldt_data_a, ldt_data_doc_origine, ldt_data_bolla, ldt_data_fattura

string						ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_cliente_sel, ls_flag_scelta, &
							ls_sql, ls_cod_prodotto, ls_cod_doc_origine, ls_numeratore_doc_origine, &
							ls_cod_fornitore, ls_numeratore_doc, ls_des_prodotto, ls_cod_tipo_bol_acq, &
							ls_num_bolla_acq, ls_cod_cliente, ls_cod_documento, ls_numeratore_documento, &
							ls_intestazione
							
integer					li_return, li_flag_ordinamento

long						ll_newrow, ll_num_righe, ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo, &
							ll_num_registrazione_mov_mag, ll_anno_registrazione, ll_anno_bolla_acq, &
							ll_anno_documento, ll_num_documento, ll_anno_registrazione_mov_mag
							
double					ld_quan_fatturata, ld_prezzo_unitario, ld_quan_arrivata, ld_val_riga, ld_quan_consegnata


dw_selezione.accepttext()
dw_report.reset()

ldt_data_da = dw_selezione.getitemdatetime(1, "data_registrazione_da")
ldt_data_a = dw_selezione.getitemdatetime(1, "data_registrazione_a")
ls_cod_prodotto_da = dw_selezione.getitemstring(1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getitemstring(1, "cod_prodotto_a")
ls_cod_cliente_sel = dw_selezione.getitemstring(1, "cod_cliente")
ls_flag_scelta = dw_selezione.getitemstring(1, "flag_scelta")
li_flag_ordinamento = dw_selezione.getitemnumber(1, "flag_ordinamento")


if isnull(ls_flag_scelta) then ls_flag_scelta = "N"

if isnull(ldt_data_da) or isnull(ldt_data_a) then
	g_mb.warning("Apice", "Impostare entrambe le date")
	return
end if

if ls_cod_prodotto_da = "" then  setnull(ls_cod_prodotto_da)
if ls_cod_prodotto_a = "" then  setnull(ls_cod_prodotto_a)

if isnull(ls_cod_prodotto_da) or isnull(ls_cod_prodotto_da) then
	li_return = g_mb.messagebox("Apice", "Selezione prodotti non impostati: continuare?", Question!, YesNo! )
	if li_return = 2 then return
end if

if isnull(ls_cod_cliente_sel) or ls_cod_cliente_sel= "" then
	ls_cod_cliente_sel = "%"
end if

ls_intestazione = "data da: "+ string(ldt_data_da, "dd/mm/yyyy") + " data a: " + string(ldt_data_a, "dd/mm/yyyy") + "; "
if not isnull(ls_cod_prodotto_da) then
	ls_intestazione = ls_intestazione + " prodotto da: " + ls_cod_prodotto_da
end if
if not isnull(ls_cod_prodotto_a) then
	ls_intestazione = ls_intestazione + " prodotto a: " + ls_cod_prodotto_a
end if
ls_intestazione = ls_intestazione + " cliente: " + ls_cod_cliente_sel



if isnull(ls_cod_prodotto_da) or isnull(ls_cod_prodotto_da) then
	ls_sql = "SELECT tes_fat_ven.cod_cliente,   " +&
				"tes_fat_ven.cod_documento,   " +&
				"tes_fat_ven.numeratore_documento, " +&  
				"tes_fat_ven.anno_documento,   " +&
				"tes_fat_ven.num_documento,   " +&
				"tes_fat_ven.data_fattura,   " +&
				"det_fat_ven.cod_prodotto,   " +&
				"det_fat_ven.quan_fatturata,   " +&
				"det_fat_ven.num_registrazione_mov_mag,   " +&
				"det_fat_ven.anno_registrazione_mov_mag  " +&
		 "FROM det_fat_ven,   " +&
				"tes_fat_ven,   " +&
				"tab_tipi_fat_ven  " +&
		"WHERE ( tes_fat_ven.cod_azienda = det_fat_ven.cod_azienda ) and  " +&
				"( tes_fat_ven.anno_registrazione = det_fat_ven.anno_registrazione ) and  " +&
				"( tes_fat_ven.num_registrazione = det_fat_ven.num_registrazione ) and  " +&
				"( tab_tipi_fat_ven.cod_azienda = tes_fat_ven.cod_azienda ) and  " +&
				"( tab_tipi_fat_ven.cod_tipo_fat_ven = tes_fat_ven.cod_tipo_fat_ven ) and  " +&
				"( ( det_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " +&
				"( det_fat_ven.cod_prodotto is not null ) AND  " +&
				"( det_fat_ven.num_registrazione_bol_ven is null ) AND  " +&
				"( tes_fat_ven.flag_blocco = 'N' ) AND  " +&
				"( tes_fat_ven.flag_movimenti = 'S' ) AND  " +&
				"( tab_tipi_fat_ven.flag_tipo_fat_ven = 'I' ) AND  " +&
				"( tes_fat_ven.data_registrazione >= '" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' ) AND " +&  
				"( tes_fat_ven.data_registrazione <= '" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' ) AND " +& 
				"( tes_fat_ven.cod_cliente like '" + ls_cod_cliente_sel + "' )) "
else
	ls_sql = "SELECT tes_fat_ven.cod_cliente,   " +&
				"tes_fat_ven.cod_documento,   " +&
				"tes_fat_ven.numeratore_documento, " +&  
				"tes_fat_ven.anno_documento,   " +&
				"tes_fat_ven.num_documento,   " +&
				"tes_fat_ven.data_fattura,   " +&
				"det_fat_ven.cod_prodotto,   " +&
				"det_fat_ven.quan_fatturata,   " +&
				"det_fat_ven.num_registrazione_mov_mag,   " +&
				"det_fat_ven.anno_registrazione_mov_mag  " +&
		 "FROM det_fat_ven,   " +&
				"tes_fat_ven,   " +&
				"tab_tipi_fat_ven  " +&
		"WHERE ( tes_fat_ven.cod_azienda = det_fat_ven.cod_azienda ) and  " +&
				"( tes_fat_ven.anno_registrazione = det_fat_ven.anno_registrazione ) and  " +&
				"( tes_fat_ven.num_registrazione = det_fat_ven.num_registrazione ) and  " +&
				"( tab_tipi_fat_ven.cod_azienda = tes_fat_ven.cod_azienda ) and  " +&
				"( tab_tipi_fat_ven.cod_tipo_fat_ven = tes_fat_ven.cod_tipo_fat_ven ) and  " +&
				"( ( det_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " +&
				"( det_fat_ven.cod_prodotto is not null ) AND  " +&
				"( det_fat_ven.num_registrazione_bol_ven is null ) AND  " +&
				"( tes_fat_ven.flag_blocco = 'N' ) AND  " +&
				"( tes_fat_ven.flag_movimenti = 'S' ) AND  " +&
				"( tab_tipi_fat_ven.flag_tipo_fat_ven = 'I' ) AND  " +&
				"( tes_fat_ven.data_registrazione >= '" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' ) AND " +&  
				"( tes_fat_ven.data_registrazione <= '" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' ) AND " +& 
				"( tes_fat_ven.cod_cliente like '" + ls_cod_cliente_sel + "' ) AND  " +&
				"( det_fat_ven.cod_prodotto >= '" + ls_cod_prodotto_da + "' ) AND  " +&
				"( det_fat_ven.cod_prodotto <= '" + ls_cod_prodotto_a + "' ) )  "
end if
DECLARE cur_fat_ven DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_fat_ven ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "errore apertura cursore cur_fat_ven " + sqlca.sqlerrtext)
	g_mb.messagebox("Apice: errore apertura cursore cur_fat_ven ", ls_sql)
	return
end if

DO while 0=0
	FETCH cur_fat_ven 
	INTO 	:ls_cod_cliente, 
			:ls_cod_documento,  
			:ls_numeratore_documento, 
			:ll_anno_documento,  
			:ll_num_documento, 
			:ldt_data_fattura,  
			:ls_cod_prodotto, 
			:ld_quan_fatturata,  
			:ll_num_registrazione_mov_mag, 
			:ll_anno_registrazione_mov_mag ;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_fat_ven;
		return -1
	end if
	
	ls_numeratore_doc =ls_cod_documento + "  " + string(ll_anno_documento) + 	"-" + ls_numeratore_documento + &
			" / " + string(ll_num_documento)
	
	sle_1.text = "doc: " + ls_numeratore_doc + " " + string(ldt_data_fattura, "dd/mm/yyyy")
	
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti 
	where cod_azienda = :s_cs_xx.cod_azienda
	  and cod_prodotto = :ls_cod_prodotto;

	if isnull(ll_num_registrazione_mov_mag) then // prodotti non collegati a magazzino: valorizzo a zero
		ld_prezzo_unitario = 0
	else
		select val_movimento
		into :ld_prezzo_unitario
		from mov_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda
		  and anno_registrazione = :ll_anno_registrazione_mov_mag
		  and num_registrazione = :ll_num_registrazione_mov_mag;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Mov. magazzino " + string(ll_anno_registrazione_mov_mag) + "/" + string(ll_num_registrazione_mov_mag), &
					"Errore lettura " + sqlca.sqlerrtext)
			ld_prezzo_unitario = 0
		end if
	end if

	ll_newrow = dw_report.insertrow(0) 
	dw_report.ScrollToRow(ll_newrow)
	dw_report.setitem(ll_newrow, "cod_prodotto" , ls_cod_prodotto)
	dw_report.setitem(ll_newrow, "des_prodotto" , ls_des_prodotto)
	dw_report.setitem(ll_newrow, "cod_cliente" , ls_cod_cliente)
	dw_report.setitem(ll_newrow, "numeratore_doc", ls_numeratore_doc)
	dw_report.setitem(ll_newrow, "data_doc" , ldt_data_fattura)
	dw_report.setitem(ll_newrow, "quantita" , ld_quan_fatturata)
	dw_report.setitem(ll_newrow, "valore_un" , ld_prezzo_unitario)

LOOP
CLOSE cur_fat_ven;


// leggo dati delle bolle

if isnull(ls_cod_prodotto_da) or isnull(ls_cod_prodotto_da) then
  ls_sql = "SELECT tes_bol_ven.cod_cliente,  " +& 
         "tes_bol_ven.cod_documento,   " +&
         "tes_bol_ven.numeratore_documento, " +&  
         "tes_bol_ven.anno_documento,   " +&
         "tes_bol_ven.num_documento,   " +&
         "tes_bol_ven.data_bolla,   " +&
         "det_bol_ven.cod_prodotto,   " +&
         "det_bol_ven.quan_consegnata,   " +&
         "det_bol_ven.num_registrazione_mov_mag,   " +&
         "det_bol_ven.anno_registrazione_mov_mag  " +&
    "FROM det_bol_ven,   " +&
         "tes_bol_ven,   " +&
         "tab_tipi_bol_ven  " +&
   "WHERE ( tes_bol_ven.cod_azienda = det_bol_ven.cod_azienda ) and  " +&
         "( tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione ) and  " +&
         "( tes_bol_ven.num_registrazione = det_bol_ven.num_registrazione ) and  " +&
         "( tab_tipi_bol_ven.cod_azienda = tes_bol_ven.cod_azienda ) and  " +&
         "( tab_tipi_bol_ven.cod_tipo_bol_ven = tes_bol_ven.cod_tipo_bol_ven ) and  " +&
         "( ( det_bol_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " +&
         "( det_bol_ven.cod_prodotto is not null ) AND  " +&
         "( tes_bol_ven.flag_blocco = 'N' ) AND  " +&
         "( tes_bol_ven.flag_movimenti = 'S' ) AND  " +&
         "( tab_tipi_bol_ven.flag_tipo_bol_ven = 'V' ) AND  " +&
         "( tes_bol_ven.data_registrazione >= '" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' ) AND  " +&
         "( tes_bol_ven.data_registrazione <= '" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' ) AND  " +&
         "( tes_bol_ven.cod_cliente like '" + ls_cod_cliente_sel + "' ) )  "
else
  ls_sql = "SELECT tes_bol_ven.cod_cliente,  " +& 
         "tes_bol_ven.cod_documento,   " +&
         "tes_bol_ven.numeratore_documento, " +&  
         "tes_bol_ven.anno_documento,   " +&
         "tes_bol_ven.num_documento,   " +&
         "tes_bol_ven.data_bolla,   " +&
         "det_bol_ven.cod_prodotto,   " +&
         "det_bol_ven.quan_consegnata,   " +&
         "det_bol_ven.num_registrazione_mov_mag,   " +&
         "det_bol_ven.anno_registrazione_mov_mag  " +&
    "FROM det_bol_ven,   " +&
         "tes_bol_ven,   " +&
         "tab_tipi_bol_ven  " +&
   "WHERE ( tes_bol_ven.cod_azienda = det_bol_ven.cod_azienda ) and  " +&
         "( tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione ) and  " +&
         "( tes_bol_ven.num_registrazione = det_bol_ven.num_registrazione ) and  " +&
         "( tab_tipi_bol_ven.cod_azienda = tes_bol_ven.cod_azienda ) and  " +&
         "( tab_tipi_bol_ven.cod_tipo_bol_ven = tes_bol_ven.cod_tipo_bol_ven ) and  " +&
         "( ( det_bol_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " +&
         "( det_bol_ven.cod_prodotto is not null ) AND  " +&
         "( tes_bol_ven.flag_blocco = 'N' ) AND  " +&
         "( tes_bol_ven.flag_movimenti = 'S' ) AND  " +&
         "( tab_tipi_bol_ven.flag_tipo_bol_ven = 'V' ) AND  " +&
         "( tes_bol_ven.data_registrazione >= '" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' ) AND  " +&
         "( tes_bol_ven.data_registrazione <= '" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' ) AND  " +&
         "( tes_bol_ven.cod_cliente like '" + ls_cod_cliente_sel + "' ) AND  " +&
         "( det_bol_ven.cod_prodotto >= '" + ls_cod_prodotto_da + "' ) AND  " +&
         "( det_bol_ven.cod_prodotto <= '" + ls_cod_prodotto_a + "' ) ) "
end if

DECLARE cur_bol_ven DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_bol_ven ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "errore apertura cursore cur_bol_ven " + sqlca.sqlerrtext)
	g_mb.messagebox("Apice: errore apertura cursore cur_bol_ven ", ls_sql)
	return
end if

DO while true

	FETCH cur_bol_ven 
	INTO 	:ls_cod_cliente, 
			:ls_cod_documento,  
			:ls_numeratore_documento, 
			:ll_anno_documento,  
			:ll_num_documento, 
			:ldt_data_bolla,  
			:ls_cod_prodotto, 
			:ld_quan_consegnata,  
			:ll_num_registrazione_mov_mag, 
			:ll_anno_registrazione_mov_mag ;
			
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_bol_ven;
		return -1
	end if
	
	ls_numeratore_doc =ls_cod_documento + "  " + string(ll_anno_documento) + 	"-" + ls_numeratore_documento + &
			" / " + string(ll_num_documento)
	
	sle_1.text = "doc: " + ls_numeratore_doc + " " + string(ldt_data_bolla, "dd/mm/yyyy")
	
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti 
	where cod_azienda = :s_cs_xx.cod_azienda
	  and cod_prodotto = :ls_cod_prodotto;
	
	if isnull(ll_num_registrazione_mov_mag) then // prodotti non collegati a magazzino: valorizzo a zero
		ld_prezzo_unitario = 0
	else
		select val_movimento
		into :ld_prezzo_unitario
		from mov_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda
		  and anno_registrazione = :ll_anno_registrazione_mov_mag
		  and num_registrazione = :ll_num_registrazione_mov_mag;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Mov. magazzino " + string(ll_anno_registrazione_mov_mag) + "/" + string(ll_num_registrazione_mov_mag), &
					"Errore lettura " + sqlca.sqlerrtext)
			ld_prezzo_unitario = 0
		end if
	end if

	ll_newrow = dw_report.insertrow(0) 
	dw_report.ScrollToRow(ll_newrow)
	dw_report.setitem(ll_newrow, "cod_prodotto" , ls_cod_prodotto)
	dw_report.setitem(ll_newrow, "des_prodotto" , ls_des_prodotto)
	dw_report.setitem(ll_newrow, "cod_cliente" , ls_cod_cliente)
	dw_report.setitem(ll_newrow, "numeratore_doc", ls_numeratore_doc)
	dw_report.setitem(ll_newrow, "data_doc" , ldt_data_bolla)
	dw_report.setitem(ll_newrow, "quantita" , ld_quan_consegnata)
	dw_report.setitem(ll_newrow, "valore_un" , ld_prezzo_unitario)

LOOP
CLOSE cur_bol_ven;


//if ls_flag_scelta = "N" then 
//	dw_report.setsort("data_doc a, numeratore_doc a")
//else
//	dw_report.setsort("cod_prodotto")//, data_doc a, numeratore_doc a")
//end if
if li_flag_ordinamento = 1 then
	//per prodotto
	dw_report.setsort("cod_prodotto, data_doc a, numeratore_doc a")
else
	//per data documento
	dw_report.setsort("data_doc a, numeratore_doc a, cod_prodotto")
end if

dw_report.sort()

dw_report.object.intestazione.text = ls_intestazione


dw_selezione.hide()
dw_selezione.object.b_ricerca_prodotto_da.visible=false
dw_selezione.object.b_ricerca_prodotto_a.visible=false
dw_selezione.object.b_ricerca_cliente.visible=false
cb_report.hide()
cb_annulla.hide()

parent.x = 10
parent.y = 20
parent.width = 3600
parent.height = 1700

if ls_flag_scelta = "N" then // deve nascondere i totali
	dw_report.object.tot_qta_prodotto_t.visible = 0
	dw_report.object.tot_qta_prodotto.visible = 0
	dw_report.object.tot_val_prodotto_t.visible = 0
	dw_report.object.tot_val_prodotto.visible = 0
	dw_report.object.t_3.visible = 0
	dw_report.object.compute_3.visible = 0
	dw_report.object.compute_4.visible = 0
else
	dw_report.object.tot_qta_prodotto_t.visible = 1
	dw_report.object.tot_qta_prodotto.visible = 1
	dw_report.object.tot_val_prodotto_t.visible = 1
	dw_report.object.tot_val_prodotto.visible = 1
	dw_report.object.t_3.visible = 1
	dw_report.object.compute_3.visible = 1
	dw_report.object.compute_4.visible = 1

end if

dw_report.show()

dw_report.groupcalc()

cb_selezione.show()
dw_report.change_dw_current()

end event

type cb_selezione from commandbutton within w_report_vendite
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;
dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2291
parent.height = 709

dw_report.hide()
cb_selezione.hide()

dw_selezione.show()
dw_selezione.object.b_ricerca_prodotto_da.visible=true
dw_selezione.object.b_ricerca_prodotto_a.visible=true
dw_selezione.object.b_ricerca_cliente.visible=true
cb_report.show()
cb_annulla.show()

dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_vendite
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 50
string dataobject = "d_report_vendite"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type sle_1 from singlelineedit within w_report_vendite
integer x = 23
integer y = 460
integer width = 1394
integer height = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean enabled = false
boolean border = false
boolean autohscroll = false
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_selezione from uo_cs_xx_dw within w_report_vendite
integer x = 23
integer y = 20
integer width = 2194
integer height = 440
integer taborder = 10
string dataobject = "d_selezione_report_vendite"
end type

event itemchanged;call super::itemchanged;string ls_nomecol, ls_nulla, ls_descrizione

setnull(ls_nulla)
ls_nomecol = i_colname
if ls_nomecol = "cod_cliente" then
	ls_descrizione = f_des_tabella("anag_clienti","cod_cliente = '" +  i_coltext + "'","rag_soc_1")
	if ls_descrizione = "< CODICE INESISTENTE >" then return 2
end if



end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

