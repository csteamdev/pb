﻿$PBExportHeader$w_report_vendite_operatore.srw
$PBExportComments$Report Statistiche: fatturato per operatore
forward
global type w_report_vendite_operatore from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_report_vendite_operatore
end type
type cb_annulla from commandbutton within w_report_vendite_operatore
end type
type cb_report from commandbutton within w_report_vendite_operatore
end type
type cb_selezione from commandbutton within w_report_vendite_operatore
end type
type dw_selezione from uo_cs_xx_dw within w_report_vendite_operatore
end type
type dw_report from uo_cs_xx_dw within w_report_vendite_operatore
end type
type sle_1 from singlelineedit within w_report_vendite_operatore
end type
end forward

global type w_report_vendite_operatore from w_cs_xx_principale
integer width = 4443
integer height = 2204
string title = "Stampa statistiche: vendite per operatore"
cb_1 cb_1
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
sle_1 sle_1
end type
global w_report_vendite_operatore w_report_vendite_operatore

forward prototypes
public function integer wf_scrivi_riga (long fl_riga_cli, string fs_cod_cliente, string fs_rag_soc_1, double fd_imponib[], string fs_cod_agente, ref string fs_error)
end prototypes

public function integer wf_scrivi_riga (long fl_riga_cli, string fs_cod_cliente, string fs_rag_soc_1, double fd_imponib[], string fs_cod_agente, ref string fs_error);// 	li_risp = wf_scrivi_riga(ll_riga_cli, ls_cliente_corrente, ls_rag_soc_1, imponib, fs_cod_agente, ls_error)
// scrive i campi di una riga del report
string ls_rag_soc_1_ag, ls_rag_soc_1, ls_cod_agente_1, ls_cod_agente_2

if fl_riga_cli = 0 then return 0		// deve ancora leggere il primo cliente

select rag_soc_1, cod_agente_1, cod_agente_2
into :ls_rag_soc_1, :ls_cod_agente_1, :ls_cod_agente_2
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_cliente = :fs_cod_cliente
using sqlca;


if (not isnull(ls_cod_agente_1) or ls_cod_agente_1 <> "") and ls_cod_agente_1 = fs_cod_agente then 
	select rag_soc_1
	into :ls_rag_soc_1_ag
	from anag_agenti
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_agente = :ls_cod_agente_1
	using sqlca;
else 
	ls_rag_soc_1_ag = " "
end if

if (not isnull(ls_cod_agente_2) or ls_cod_agente_2 <> "") and ls_cod_agente_2 = fs_cod_agente then 
	select rag_soc_1
	into :ls_rag_soc_1_ag
	from anag_agenti
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_agente = :ls_cod_agente_2
	using sqlca;
else 
	ls_rag_soc_1_ag = " "
end if

// ------------ modificato da Alessio il 19/06/2000: 
// ------------ scrive sempre rag. soc. in anag. clienti, perchè intestatario della fattura
//if  not isnull(fs_rag_soc_1) then 
//	ls_rag_soc_1 = fs_rag_soc_1
//end if
//




dw_report.setitem(fl_riga_cli, "rs_cod_cliente", fs_cod_cliente)
dw_report.setitem(fl_riga_cli, "rs_rag_soc_1", ls_rag_soc_1)
dw_report.setitem(fl_riga_cli, "rs_cod_agente_1", fs_cod_agente)
dw_report.setitem(fl_riga_cli, "rs_rag_soc_1_ag", ls_rag_soc_1_ag)
dw_report.setitem(fl_riga_cli, "rd_imponib_1", fd_imponib[1])
dw_report.setitem(fl_riga_cli, "rd_imponib_2", fd_imponib[2])
dw_report.setitem(fl_riga_cli, "rd_imponib_3", fd_imponib[3])
dw_report.setitem(fl_riga_cli, "rd_imponib_4", fd_imponib[4])
dw_report.setitem(fl_riga_cli, "rd_imponib_5", fd_imponib[5])
dw_report.setitem(fl_riga_cli, "rd_imponib_6", fd_imponib[6])
dw_report.setitem(fl_riga_cli, "rd_imponib_7", fd_imponib[7])
dw_report.setitem(fl_riga_cli, "rd_imponib_8", fd_imponib[8])
dw_report.setitem(fl_riga_cli, "rd_imponib_9", fd_imponib[9])
dw_report.setitem(fl_riga_cli, "rd_imponib_10", fd_imponib[10])
dw_report.setitem(fl_riga_cli, "rd_imponib_11", fd_imponib[11])
dw_report.setitem(fl_riga_cli, "rd_imponib_12", fd_imponib[12])


return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
								 c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

//set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)
								 


//dw_selezione.set_dw_options(sqlca, &
//                            pcca.null_object, &
//                            c_nodelete + &
//                            c_newonopen + &
//                            c_disableCC +&
//									 c_TriggerRefresh, &
//                            c_noresizedw + &
//                            c_nohighlightselected + &
//                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x =673
this.y = 265
this.width = 2050
this.height = 540
dw_selezione.setitem(1, "data_fine", today())
dw_selezione.setitem(1, "data_inizio", relativedate(today(), -365))

end event

on w_report_vendite_operatore.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_selezione
this.Control[iCurrent+5]=this.dw_selezione
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.sle_1
end on

on w_report_vendite_operatore.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.sle_1)
end on

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori, ls_cod, ls_des
long ll_conta
//if s_cs_xx.cod_utente <> "CS_SYSTEM" then
//	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
//					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
//						  s_cs_xx.cod_utente + "')"
//else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
//end if

f_po_loaddddw_dw(dw_selezione, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
					  
f_PO_LoadDDDW_DW (dw_selezione,&
						"cod_tipo_listino",&
						sqlca,&
                 "tab_tipi_listini_prodotti",&
					  "cod_tipo_listino_prodotto",&
					  "des_tipo_listino_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
	select count(*)
	into :ll_conta
	from tab_tipi_listini_prodotti
	where cod_azienda = : s_cs_xx.cod_azienda;
	
	
	
	if ll_conta = 1 then
		select cod_tipo_listino_prodotto, des_tipo_listino_prodotto
		into :ls_cod, :ls_des
		from tab_tipi_listini_prodotti
		where cod_azienda = : s_cs_xx.cod_azienda;
		dw_selezione.setitem(1,"cod_tipo_listino", ls_des)
		dw_selezione.setitem(1,"compute_2", ls_cod)
	end if

end event

type cb_1 from commandbutton within w_report_vendite_operatore
integer x = 2720
integer y = 1680
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
//long ll_num_righe
//
//ll_num_righe= dw_report.rowcount()
//dw_report.RowsDiscard (1, ll_num_righe, primary!)
//
//dw_report.object.rd_imponib_1_t.text = "Gennaio"
//dw_report.object.rd_imponib_2_t.text = "Febbraio"
//dw_report.object.rd_imponib_3_t.text = "Marzo"
//dw_report.object.rd_imponib_4_t.text = "Aprile"
//dw_report.object.rd_imponib_5_t.text = "Maggio"
//dw_report.object.rd_imponib_6_t.text = "Giugno"
//dw_report.object.rd_imponib_7_t.text = "Luglio"
//dw_report.object.rd_imponib_8_t.text = "Agosto"
//dw_report.object.rd_imponib_9_t.text = "Settembre"
//dw_report.object.rd_imponib_10_t.text = "Ottobre"
//dw_report.object.rd_imponib_11_t.text = "Novembre"
//dw_report.object.rd_imponib_12_t.text = "Dicembre"
//
//
//
//
//
//
//dw_selezione.show()
//
//parent.x = 673
//parent.y = 265
//parent.width = 2050
//parent.height = 540
//
//dw_report.hide()
//cb_selezione.hide()
//
//dw_selezione.change_dw_current()
dw_report.print()
end event

type cb_annulla from commandbutton within w_report_vendite_operatore
integer x = 1257
integer y = 320
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_vendite_operatore
integer x = 1646
integer y = 320
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string  ls_cod_cliente, ls_cod_operatore_attuale, ls_des_operatore, &
		 ls_cliente_corrente , ls_error, ls_cod_operatore, ls_sqlstatement, &
		 ls_clausola, ls_mese, ls_cod_prodotto, ls_errore, ls_chiave[], &	
		 ls_cod_tipo_listino, ls_cod_tipo_listino_ddt
datetime ldt_data_rif_da, ldt_data_rif_a, ldt_data_fattura, ldd_data_inizio, ldd_data_fine
integer li_mese,li_anno, li_i, li_risp, li_mese_1, li_cont
dec{4} ld_imponibile_iva,  ld_prezzo_listino, ld_qta, ld_cmp, ld_quan[],&
		ld_giacenza_stock[],ld_costo_medio_stock[],ld_quan_costo_medio_stock[], ld_costo, &
		ld_quan_costo_medio, ldd_prezzo
dec{2} ld_valore
long ll_riga, ll_i, ll_ret
string ls_mesi[], ls_cod_notacred, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven
uo_condizioni_cliente iuo_condizioni_cliente
uo_magazzino luo_mag



dw_selezione.accepttext()
ls_cod_operatore = dw_selezione.getitemstring(1,"cod_operatore")
ls_cod_tipo_listino = dw_selezione.getitemstring(1,"cod_tipo_listino")
ldd_data_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldd_data_fine = dw_selezione.getitemdatetime(1,"data_fine")
if isnull(ls_cod_operatore) then ls_cod_operatore = "%"
//is isnull(ldd_data_inizio) then ldd_data_inizio = 



dw_report.retrieve(s_cs_xx.cod_azienda, ldd_data_inizio, ldd_data_fine, ls_cod_operatore)

ll_riga = dw_report.rowcount()
for ll_i = 1 to ll_riga
	dw_report.setitem(ll_i, "num_righe", ll_riga)
	ls_cod_prodotto = dw_report.getitemstring(ll_i , "det_bol_ven_cod_prodotto")
	 //det_bol_ven_imponibile_iva  /  det_bol_ven_quantita_um 
	ls_cod_cliente = dw_report.getitemstring(ll_i , "tes_bol_ven_cod_cliente")
	ld_qta = dw_report.getitemnumber(ll_i , "det_bol_ven_quantita_um")
	ls_cod_operatore_attuale = dw_report.getitemstring(ll_i , "tes_bol_ven_cod_operatore")

	ld_imponibile_iva = dw_report.getitemnumber(ll_i , "det_bol_ven_imponibile_iva")
	ls_cod_tipo_listino_ddt = dw_report.getitemstring(ll_i ,"tes_bol_ven_cod_tipo_listino_prodotto")
	ld_imponibile_iva = ld_imponibile_iva / ld_qta
	select des_operatore
	into :ls_des_operatore
	from tab_operatori
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_operatore = :ls_cod_operatore_attuale;
	if sqlca.sqlcode <>0 then
		g_mb.messagebox("Apice", "Errore lettura descrizione operatore " + sqlca.sqlerrtext)
		return 	
	end if
	ls_cod_tipo_listino_ddt = ""
	dw_report.setitem(ll_i, "des_operatore", ls_des_operatore)
	if isnull(ls_cod_tipo_listino_ddt) or ls_cod_tipo_listino_ddt = "" then
		//****cerco il tipo listino associato al cliente
		select cod_tipo_listino_prodotto
		into :ls_cod_tipo_listino_ddt
		from anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda
		and cod_cliente = :ls_cod_cliente;
		if sqlca.sqlcode <>0 then
			g_mb.messagebox("Apice", "Errore lettura tipo listino " + sqlca.sqlerrtext)
			return 	
		end if
	end if
	//******calcolo il valore del listino
	if isnull(ls_cod_tipo_listino_ddt) or ls_cod_tipo_listino_ddt = "" then
		ls_cod_tipo_listino_ddt = ls_cod_tipo_listino
	end if
	ls_cod_tipo_listino = ls_cod_tipo_listino_ddt
	iuo_condizioni_cliente = create uo_condizioni_cliente
   iuo_condizioni_cliente.ib_setitem=false
   iuo_condizioni_cliente.ib_setitem_provvigioni=false
	iuo_condizioni_cliente.str_parametri.ldw_oggetto = dw_report
	iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino
	iuo_condizioni_cliente.str_parametri.cod_valuta = dw_report.getitemstring(ll_i,"tes_bol_ven_cod_valuta")
	iuo_condizioni_cliente.str_parametri.cambio_ven = 1
	iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(today(), 00:00:00)
	if isnull(ls_cod_cliente) or ls_cod_cliente = "" then
		ls_cod_cliente = "XXXXXX"
	end if	
	iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
	iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
	iuo_condizioni_cliente.str_parametri.dim_1 = 0
	iuo_condizioni_cliente.str_parametri.dim_2 = 0
	iuo_condizioni_cliente.str_parametri.quantita = ld_qta
	iuo_condizioni_cliente.str_parametri.valore = 0
	//iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
	//iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
	iuo_condizioni_cliente.wf_condizioni_cliente()
	
	ldd_prezzo=iuo_condizioni_cliente.str_output.variazioni[upperbound(iuo_condizioni_cliente.str_output.variazioni)]
	dw_report.setitem(ll_i, "prezzo_listino", ldd_prezzo)
	
	destroy iuo_condizioni_cliente
				
	//******faccio la differenza
	//det_bol_ven_prezzo_um
	ld_prezzo_listino = dw_report.getitemnumber(ll_i , "prezzo_listino")
		
	if ld_prezzo_listino = 0 then
		ld_valore = 0
	else
		//ld_valore =((ld_imponibile_iva - ld_prezzo_listino) *100)/ld_imponibile_iva
		//ld_imponibile_iva = dw_report.getitemnumber(ll_i , "det_bol_ven_prezzo_um")
		//---modificato su descrizione di marco
		ld_valore =round(((ld_imponibile_iva - ld_prezzo_listino) *100)/ld_prezzo_listino,2)
		
	end if
	dw_report.setitem(ll_i, "diff_listino", ld_valore)
	//*********totale
	ld_valore =ld_imponibile_iva * ld_qta
	dw_report.setitem(ll_i, "totale", ld_valore)
	//**********cmp
	luo_mag = create uo_magazzino
	ll_ret = luo_mag.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldd_data_fine, "", ld_quan[], ls_errore, "N", ls_chiave[], ld_giacenza_stock[],ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	destroy luo_mag
	if ld_quan[12] > 0 then
					ld_costo = ld_quan[13] / ld_quan[12]
				else
					
					select costo_medio_ponderato
					into   :ld_costo
					from   lifo
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto and
							 anno_lifo = (select max(anno_lifo)
											  from   lifo
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
															
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("APICE","Errore in ricerca del costo medio del prodotto " + ls_cod_prodotto+ " in tabella LIFO~r~n" + sqlca.sqlerrtext)
						continue
					elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
						ld_costo = 0
					end if
					
				end if
				
				//ld_quan_costo_medio = ld_quan[12]
		dw_report.setitem(ll_i, "cmp", ld_costo)		
				
				
	//****diff ricarico calcolare il costo medio ponderato di acquisto
	ld_costo = dw_report.getitemnumber(ll_i, "cmp")
	if ld_costo = 0 then
		ld_valore = 0
	else	
		ld_valore =((ld_imponibile_iva - ld_costo)*100)/ld_costo
	end if
	dw_report.setitem(ll_i, "diff_ricairco", ld_valore)		
	//*margine
	if ld_imponibile_iva = 0 then
		ld_valore = 0
	else	
		ld_valore =((ld_imponibile_iva - ld_costo)*100)/ld_imponibile_iva
	end if
	dw_report.setitem(ll_i, "margine", ld_valore)		
next

//****faccio i totali
		



dw_selezione.hide()

parent.x = 10
parent.y = 100
parent.width = 3600
parent.height = 1884

dw_report.show()
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_vendite_operatore
integer x = 3109
integer y = 1680
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;
//long ll_num_righe
//
//ll_num_righe= dw_report.rowcount()
//dw_report.RowsDiscard (1, ll_num_righe, primary!)
//
//dw_report.object.rd_imponib_1_t.text = "Gennaio"
//dw_report.object.rd_imponib_2_t.text = "Febbraio"
//dw_report.object.rd_imponib_3_t.text = "Marzo"
//dw_report.object.rd_imponib_4_t.text = "Aprile"
//dw_report.object.rd_imponib_5_t.text = "Maggio"
//dw_report.object.rd_imponib_6_t.text = "Giugno"
//dw_report.object.rd_imponib_7_t.text = "Luglio"
//dw_report.object.rd_imponib_8_t.text = "Agosto"
//dw_report.object.rd_imponib_9_t.text = "Settembre"
//dw_report.object.rd_imponib_10_t.text = "Ottobre"
//dw_report.object.rd_imponib_11_t.text = "Novembre"
//dw_report.object.rd_imponib_12_t.text = "Dicembre"
//
//
//
//
//
//
dw_selezione.show()

parent.x = 673
parent.y = 265
parent.width = 2050
parent.height = 540

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_vendite_operatore
integer x = 23
integer y = 20
integer width = 1989
integer height = 280
integer taborder = 20
string dataobject = "d_report_vendite_operatore_sel"
end type

event pcd_new;call super::pcd_new;dw_selezione.setitem(1, "data_fine", today())
dw_selezione.setitem(1, "data_inizio", relativedate(today(), -365))
end event

type dw_report from uo_cs_xx_dw within w_report_vendite_operatore
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1620
integer taborder = 10
string dataobject = "d_report_elenco_bolle_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type sle_1 from singlelineedit within w_report_vendite_operatore
integer x = 183
integer y = 320
integer width = 1001
integer height = 92
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean autohscroll = false
end type

