﻿$PBExportHeader$w_rif_ord_in_ft.srw
$PBExportComments$procedura per inserire nelle fatture il riferimento all'ordine
forward
global type w_rif_ord_in_ft from window
end type
type st_3 from statictext within w_rif_ord_in_ft
end type
type st_2 from statictext within w_rif_ord_in_ft
end type
type st_1 from statictext within w_rif_ord_in_ft
end type
type anno from singlelineedit within w_rif_ord_in_ft
end type
type cb_1 from commandbutton within w_rif_ord_in_ft
end type
end forward

global type w_rif_ord_in_ft from window
integer width = 1495
integer height = 544
boolean titlebar = true
string title = "Riferimento ordine in fattura acquisto"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
st_3 st_3
st_2 st_2
st_1 st_1
anno anno
cb_1 cb_1
end type
global w_rif_ord_in_ft w_rif_ord_in_ft

on w_rif_ord_in_ft.create
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.anno=create anno
this.cb_1=create cb_1
this.Control[]={this.st_3,&
this.st_2,&
this.st_1,&
this.anno,&
this.cb_1}
end on

on w_rif_ord_in_ft.destroy
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.anno)
destroy(this.cb_1)
end on

type st_3 from statictext within w_rif_ord_in_ft
integer x = 46
integer y = 100
integer width = 1234
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "all~'ordine nelle fatture di acquisto"
boolean focusrectangle = false
end type

type st_2 from statictext within w_rif_ord_in_ft
integer x = 46
integer y = 40
integer width = 2149
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Procedura che serve per inserire il riferimento "
boolean focusrectangle = false
end type

type st_1 from statictext within w_rif_ord_in_ft
integer x = 69
integer y = 220
integer width = 251
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Anno:"
boolean focusrectangle = false
end type

type anno from singlelineedit within w_rif_ord_in_ft
integer x = 320
integer y = 200
integer width = 229
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "2006"
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_rif_ord_in_ft
integer x = 846
integer y = 200
integer width = 411
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "esegui"
end type

event clicked;long ll_ret, ll_i , ll_num_ordine, ll_anno_ordine, ll_num_riga_ordine
long ll_anno
datastore lds_dati

//ll_anno = anno.getext
ll_anno = long(anno.text)
lds_dati= create datastore
lds_dati.dataobject = "d_ordini_fatture_bolle"
lds_dati.settransobject(sqlca)
ll_ret = lds_dati.retrieve(s_cs_xx.cod_azienda, ll_anno)

if ll_ret <=0 then
	destroy lds_dati
	  	g_mb.messagebox("Procedura","Errore non ci sono fatture presenti")
	  return

else
	
	for ll_i = 1 to lds_dati.rowcount()
		
	ll_num_ordine = lds_dati.getitemnumber(ll_i, "det_bol_acq_num_registrazione")
	ll_anno_ordine = lds_dati.getitemnumber(ll_i, "det_bol_acq_anno_registrazione")
	ll_num_riga_ordine = lds_dati.getitemnumber(ll_i, "det_bol_acq_prog_riga_ordine_acq")
	if not isnull(ll_num_ordine) and not isnull(ll_anno_ordine) and not isnull(ll_num_riga_ordine) then
		lds_dati.setitem(ll_i, "det_fat_acq_anno_reg_ord_acq", ll_anno_ordine)
		lds_dati.setitem(ll_i, "det_fat_acq_num_reg_ord_acq", ll_num_ordine)
		lds_dati.setitem(ll_i, "det_fat_acq_prog_riga_ordine_acq", ll_num_riga_ordine)
//	else
//		lds_dati.setitem(ll_i, "det_fat_acq_anno_reg_ord_acq", setnull(ll_anno_ordine))
//		lds_dati.setitem(ll_i, "det_fat_acq_num_reg_ord_acq", NULL)
//		lds_dati.setitem(ll_i, "det_fat_acq_prog_riga_ordine_acq", NULL)

	end if
	next
		
	ll_ret = lds_dati.update()
	
	if ll_ret <0 then
		
		destroy lds_dati
	  	g_mb.messagebox("Procedura","Errore non eseguito update, contattare l'assistenza!")
		return
		
	end if
end if

destroy lds_dati
  	g_mb.messagebox("Procedura","OK TERMINATA!")
	
return

end event

