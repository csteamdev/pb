﻿$PBExportHeader$w_righe_ordini_cliente_euro.srw
$PBExportComments$Report righe da ordini cliente
forward
global type w_righe_ordini_cliente_euro from w_cs_xx_principale
end type
type dw_selezione from uo_cs_xx_dw within w_righe_ordini_cliente_euro
end type
type dw_report from uo_cs_xx_dw within w_righe_ordini_cliente_euro
end type
type dw_folder from u_folder within w_righe_ordini_cliente_euro
end type
type cb_annulla from commandbutton within w_righe_ordini_cliente_euro
end type
type cb_report from commandbutton within w_righe_ordini_cliente_euro
end type
end forward

global type w_righe_ordini_cliente_euro from w_cs_xx_principale
integer x = 5
integer y = 4
integer width = 3799
integer height = 2228
string title = "Report Righe Ordini Clienti"
event ue_posizione ( )
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
cb_annulla cb_annulla
cb_report cb_report
end type
global w_righe_ordini_cliente_euro w_righe_ordini_cliente_euro

event ue_posizione();//this.x = 20
//this.y = 12
//this.width = 3703
//this.height = 2032
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
save_on_close(c_socnosave)
iuo_dw_main = dw_report
dw_selezione.resetupdate()

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_annulla
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)


postevent("ue_posizione")
end event

on w_righe_ordini_cliente_euro.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_report
end on

on w_righe_ordini_cliente_euro.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.cb_annulla)
destroy(this.cb_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(		dw_selezione, &
                 	  "cod_tipo_ord_ven", &
                 		sqlca, &
                    "tab_tipi_ord_ven", &
                    "cod_tipo_ord_ven", &
                    "des_tipo_ord_ven", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						  
f_po_loaddddw_dw(		dw_selezione, &
                 	  "cod_tipo_det_ven", &
                 		sqlca, &
                    "tab_tipi_det_ven", &
                    "cod_tipo_det_ven", &
                    "des_tipo_det_ven", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "'")						  
end event

type dw_selezione from uo_cs_xx_dw within w_righe_ordini_cliente_euro
integer x = 46
integer y = 160
integer width = 3383
integer height = 1160
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_selezione_righe_ordini_cliente"
boolean border = false
end type

event pcd_new;call super::pcd_new;setitem(getrow(),"n_anno",f_anno_esercizio())
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_report from uo_cs_xx_dw within w_righe_ordini_cliente_euro
integer x = 23
integer y = 180
integer width = 3703
integer height = 1920
integer taborder = 30
string dataobject = "d_righe_ordini_clienti_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_flag_evaso, ls_flag_evasione_1, ls_flag_evasione_2, ls_flag_evasione_3, &
		 ls_flag_blocco, ls_cod_prodotto, ls_cod_tipo_ord_ven, ls_cod_tipo_det_ven, ls_flag_residuo

long   ll_ordine_inizio, ll_ordine_fine, ll_errore, ll_anno_ordine

date   ld_data_inizio, ld_data_fine, ld_data_ordine_da, ld_data_ordine_a


dw_selezione.accepttext()

dw_report.setredraw( false)

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_prodotto = dw_selezione.getitemstring(1,"cod_prodotto")
ls_flag_evaso = dw_selezione.getitemstring(1,"flag_evaso")
ld_data_inizio = dw_selezione.getitemdate(1,"d_data_da")
ld_data_fine = dw_selezione.getitemdate(1,"d_data_a")
ld_data_ordine_da = dw_selezione.getitemdate( 1, "data_ordine_da")
ld_data_ordine_a = dw_selezione.getitemdate( 1, "data_ordine_a")
ll_anno_ordine = dw_selezione.getitemnumber(1,"n_anno")
ll_ordine_inizio = dw_selezione.getitemnumber(1,"n_ord_da")
ll_ordine_fine = dw_selezione.getitemnumber(1,"n_ord_a")
ls_flag_blocco = dw_selezione.getitemstring(1,"flag_blocco")
ls_cod_tipo_ord_ven = dw_selezione.getitemstring( 1, "cod_tipo_ord_ven")
ls_cod_tipo_det_ven = dw_selezione.getitemstring( 1, "cod_tipo_det_ven")
ls_flag_residuo = dw_selezione.getitemstring( 1, "flag_residuo")

if not isnull(ls_flag_residuo) and ls_flag_residuo = "S" then
	dw_report.dataobject = "d_righe_ordini_clienti_euro_residuo"	
	dw_report.settransobject( sqlca)	
else
	dw_report.dataobject = "d_righe_ordini_clienti_euro"	
	dw_report.settransobject( sqlca)		
end if

if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
	ls_cod_cliente ="%"
end if

if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then
	ls_cod_prodotto ="%"
end if

choose case ls_flag_evaso
	case "A"
		ls_flag_evasione_1="A"
		ls_flag_evasione_2="A"
		ls_flag_evasione_3="A"
	case "P"
		ls_flag_evasione_1="P"
		ls_flag_evasione_2="P"
		ls_flag_evasione_3="P"	
	case "E"
		ls_flag_evasione_1="E"
		ls_flag_evasione_2="E"
		ls_flag_evasione_3="E"
	case "I"
		ls_flag_evasione_1="A"
		ls_flag_evasione_2="P"
		ls_flag_evasione_3="P"
	case "T"
		ls_flag_evasione_1="A"
		ls_flag_evasione_2="P"
		ls_flag_evasione_3="E"
end choose

if isnull(ld_data_inizio) then
	ld_data_inizio = date("01/01/1900")
end if	

if isnull(ld_data_fine) then
	ld_data_fine = date("31/12/2999")
end if	

if isnull(ld_data_ordine_da) then
	ld_data_ordine_da = date("01/01/1900")
end if	

if isnull(ld_data_ordine_a) then
	ld_data_ordine_a = date("31/12/2999")
end if	

if ll_anno_ordine = 0 then
	setnull(ll_anno_ordine)
end if

if isnull(ll_ordine_inizio) or ll_ordine_inizio = 0 then
	ll_ordine_inizio = 1
end if

if isnull(ll_ordine_fine) or ll_ordine_fine = 0 then
	ll_ordine_inizio = 999999
end if	

if isnull(ls_flag_blocco) then
	ls_flag_blocco = 'N'
end if	

if isnull(ls_cod_tipo_ord_ven) or len(ls_cod_tipo_ord_ven) < 1 then
	ls_cod_tipo_ord_ven = "%"
end if

if isnull(ls_cod_tipo_det_ven) or len(ls_cod_tipo_det_ven) < 1 then
	ls_cod_tipo_det_ven = "%"
end if

ll_errore = dw_report.retrieve(s_cs_xx.cod_azienda, ll_anno_ordine, ld_data_inizio, ld_data_fine, ll_ordine_inizio, ll_ordine_fine, ls_flag_evasione_1, ls_flag_evasione_2, ls_flag_evasione_3, ls_cod_cliente, ls_flag_blocco, ls_cod_prodotto, ls_cod_tipo_ord_ven, ls_cod_tipo_det_ven, ld_data_ordine_da, ld_data_ordine_a)

dw_report.groupcalc()

dw_report.setredraw( true)

dw_report.change_dw_current()

iuo_dw_main = dw_report

dw_folder.fu_selecttab(2)
end event

type dw_folder from u_folder within w_righe_ordini_cliente_euro
integer x = 23
integer y = 40
integer width = 3730
integer height = 2072
integer taborder = 40
boolean border = false
end type

type cb_annulla from commandbutton within w_righe_ordini_cliente_euro
integer x = 2537
integer y = 980
integer width = 366
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null
date ld_null
long ll_null

setnull(ld_null)
setnull(ll_null)
setnull(ls_null)

dw_selezione.setitem(dw_selezione.getrow(), "d_data_da", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_a", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_cliente", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_prodotto", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_da", 1)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_a", 999999)
dw_selezione.setitem(dw_selezione.getrow(),"n_anno",f_anno_esercizio())
dw_selezione.setitem(dw_selezione.getrow(), "flag_evaso", "A")
dw_selezione.setitem(dw_selezione.getrow(), "flag_blocco", "N")
dw_selezione.setitem(dw_selezione.getrow(), "cod_tipo_ord_ven", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_tipo_det_ven", ls_null)
end event

type cb_report from commandbutton within w_righe_ordini_cliente_euro
integer x = 2949
integer y = 980
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_report.triggerevent("pcd_retrieve")
end event

