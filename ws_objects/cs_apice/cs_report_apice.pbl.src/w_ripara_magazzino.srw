﻿$PBExportHeader$w_ripara_magazzino.srw
$PBExportComments$Report Inventario del Magazino
forward
global type w_ripara_magazzino from w_cs_xx_principale
end type
type pb_2 from picturebutton within w_ripara_magazzino
end type
type sle_file2 from singlelineedit within w_ripara_magazzino
end type
type st_4 from statictext within w_ripara_magazzino
end type
type cb_1 from commandbutton within w_ripara_magazzino
end type
type em_data_ini from editmask within w_ripara_magazzino
end type
type st_2 from statictext within w_ripara_magazzino
end type
type dw_report from uo_cs_xx_dw within w_ripara_magazzino
end type
type em_da_data from editmask within w_ripara_magazzino
end type
type st_3 from statictext within w_ripara_magazzino
end type
type st_1 from statictext within w_ripara_magazzino
end type
type sle_file from singlelineedit within w_ripara_magazzino
end type
type pb_1 from picturebutton within w_ripara_magazzino
end type
type cb_report from commandbutton within w_ripara_magazzino
end type
type ln_1 from line within w_ripara_magazzino
end type
type ln_2 from line within w_ripara_magazzino
end type
type ln_3 from line within w_ripara_magazzino
end type
type ln_4 from line within w_ripara_magazzino
end type
end forward

global type w_ripara_magazzino from w_cs_xx_principale
integer x = 73
integer y = 300
integer width = 3721
integer height = 2124
string title = "Inventario Magazzino"
pb_2 pb_2
sle_file2 sle_file2
st_4 st_4
cb_1 cb_1
em_data_ini em_data_ini
st_2 st_2
dw_report dw_report
em_da_data em_da_data
st_3 st_3
st_1 st_1
sle_file sle_file
pb_1 pb_1
cb_report cb_report
ln_1 ln_1
ln_2 ln_2
ln_3 ln_3
ln_4 ln_4
end type
global w_ripara_magazzino w_ripara_magazzino

type variables
boolean ib_interrupt
end variables

forward prototypes
public function integer wf_opera (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_cod_misura[], string fs_quantita[], datetime fdt_da_data)
public function integer wf_aggiorna_ini (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_quantita[], datetime fdt_da_data)
end prototypes

public function integer wf_opera (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_cod_misura[], string fs_quantita[], datetime fdt_da_data);string ls_cod_cat_mer_1, ls_flag_bloccato, ls_where, ls_error, ls_messaggio,&
       ls_cod_misura_mag, ls_cod_cat_mer, ls_flag_blocco, ls_sort, ls_sql, ls_cod_prodotto_a, &
		 ls_des_prodotto, ls_flag_det_depositi, ls_cod_deposito, ls_flag_det_stock, &
		 ls_flag_visualizza_val, ls_flag_tipo_valorizzazione, ls_chiave[], &
		 ls_intestazione, ls_des_azienda, ls_flag_visulizza_giac_zero, ls_costo_unitario_t, &
		 ls_vettore_nullo[], ls_cod_prodotto, ls_trovato[]
integer li_return, li_cont, ll_i
long ll_newrow, ll_num_righe, ll_num_stock, ll_j,ll_ret
dec{4} ld_saldo_quan_inizio_anno, ld_quant_val[], ld_giacenza, ld_totale_quantita, ld_costo_medio, ld_quan_acq, ld_val_inizio_anno, &
       ld_costo_standard, ld_prezzo_acquisto, ld_costo, ld_nulla, ld_giacenza_stock[], ld_costo_unitario, ld_vettore_nullo[], &
		 ld_costo_medio_continuo,ld_costo_medio_continuo_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_a, ldt_data_blocco, ldt_data_chiusura_annuale
uo_costo_medio_continuo luo_costo_medio
uo_magazzino luo_magazzino

// datetime ldt_data_a è la data di inventario
dw_report.reset()
dw_report.setredraw(false)

select data_chiusura_annuale
into   :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

ldt_data_a = fdt_da_data

if ldt_data_a < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if


ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
			"  from   anag_prodotti " +&
			"  where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			"  order by cod_prodotto " 
					
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;
	
DO while 0 = 0

	FETCH cur_1 INTO :ls_cod_prodotto, 
						  :ls_des_prodotto, 
						  :ls_cod_misura_mag;
						  
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		return -1
	end if
	
	select count(*)
	into   :ll_num_stock
	from   stock
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_prodotto = :ls_cod_prodotto;
			 
	if ll_num_stock > 0 then		// elaboro solo prodotti in stock
		ll_newrow = dw_report.insertrow(0) 
		dw_report.ScrollToRow(ll_newrow)
		dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
		dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
		dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)		
	end if
	
LOOP

CLOSE cur_1 ;
	
ls_trovato = fs_cod_prodotto	
	
ll_num_righe = dw_report.rowcount()

for ll_i = 1 to ll_num_righe
	
	ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
	
	for li_cont = 1 to 14
		ld_quant_val[li_cont] = 0
	next
		
	luo_magazzino = CREATE uo_magazzino
	
	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
		
	if li_return <0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		return -1
	end if	
		
	ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
	dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
	
	
	for ll_j = 1 to upperbound( fs_cod_prodotto)
		
		if fs_cod_prodotto[ll_j] = ls_cod_prodotto then
			
			dw_report.setitem(ll_i, "rs_cod_prodotto_2", fs_cod_prodotto[ll_j])
			dw_report.setitem(ll_i, "rs_descrizione_2", fs_des_prodotto[ll_j])
			dw_report.setitem(ll_i, "rs_cod_misura_mag", fs_cod_misura[ll_j])
			dw_report.setitem(ll_i, "rd_giacenza_2", fs_quantita[ll_j])
			ls_trovato[ll_j] = "ok"
			exit
			
		end if
		
	next
	
next

integer li_FileNum

li_FileNum = FileOpen("C:\prodotti_non_considerati.TXT", LineMode!, Write!, LockWrite!, Append!)

for ll_i = 1 to upperbound( fs_cod_prodotto)
	if ls_trovato[ll_i] <> "ok" then
		
		FileWrite(li_FileNum, fs_cod_prodotto[ll_i] + "~t" + fs_des_prodotto[ll_i] + "~t" + fs_cod_misura[ll_i] + "~t" + fs_quantita[ll_i] + "~r")
		
	end if
next

fileclose( li_filenum)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'

dw_report.Change_DW_Current()


return 0
end function

public function integer wf_aggiorna_ini (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_quantita[], datetime fdt_da_data);string		 ls_sql, ls_cod_prodotto, ls_trovato[]
long         ll_anno_registrazione, ll_num_registrazione, ll_j, li_1, li_2
boolean      lb_trovato = false
dec{4}       ld_giacenza, ld_giacenza_2
datetime ldt_data_a, ldt_data_blocco, ldt_data_ini
uo_magazzino luo_magazzino

ldt_data_ini = fdt_da_data

ls_sql = "	select anno_registrazione, num_registrazione, cod_prodotto, quan_movimento FROM mov_magazzino " + &
			"  where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_movimento = 'INI' and data_registrazione = '" + string( ldt_data_ini, s_cs_xx.db_funzioni.formato_data) + "' " + &
			"  order by cod_prodotto " 
					
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "APICE", "Errore durante l'apertura del cursore cur_1: " + sqlca.sqlerrtext)
	return -1
end if

li_1 = FileOpen("C:\prodotti_aggiornati.TXT", LineMode!, Write!, LockWrite!, Append!)
li_2 = FileOpen("C:\prodotti_non_aggiornati.TXT", LineMode!, Write!, LockWrite!, Append!)

ls_trovato = fs_cod_prodotto

DO while 0 = 0

	FETCH cur_1 INTO :ll_anno_registrazione, 
						  :ll_num_registrazione, 
						  :ls_cod_prodotto,
						  :ld_giacenza;
						  
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		rollback;
		return -1
	end if
	
	lb_trovato = false
	
	for ll_j = 1 to upperbound( fs_cod_prodotto)
		
		if fs_cod_prodotto[ll_j] = ls_cod_prodotto then
			
			if ls_trovato[ll_j] = "ok" then
				lb_trovato = false   // ini doppi
				exit
				
			else
				ls_trovato[ll_j] = "ok"
				ld_giacenza_2 = dec(fs_quantita[ll_j])
				
				update mov_magazzino 
				set    quan_movimento = :ld_giacenza_2
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox( "APICE", "Errore durante l'aggiornamento del movimento di magazzino: " + sqlca.sqlerrtext)
					g_mb.messagebox( "APICE", "prodotto: " + ls_cod_prodotto + " giacenza: " + string(ld_giacenza))
					close cur_1;
					rollback;
					return -1
				end if
				
				filewrite(li_1, ls_cod_prodotto + "~t" + string(ld_giacenza))
				
				lb_trovato = true	
				exit
			end if
		end if
		
	next
	
	if not lb_trovato then
		
		update mov_magazzino 
		set    quan_movimento = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "APICE", "Errore durante l'aggiornamento del movimento di magazzino: " + sqlca.sqlerrtext)
			g_mb.messagebox( "APICE", "prodotto: " + ls_cod_prodotto + " giacenza: 0 ")
			close cur_1;
			rollback;
			return -1
		end if				 
				 
		filewrite(li_2, ls_cod_prodotto + "~t0")
		
	end if
	
LOOP

CLOSE cur_1 ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "", "Errore durante la chiusura del cursore: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

fileclose( li_1)
fileclose( li_2)
	

integer li_FileNum

li_FileNum = FileOpen("C:\prodotti_non_considerati_INI.TXT", LineMode!, Write!, LockWrite!, Append!)

for ll_j = 1 to upperbound( fs_cod_prodotto)
	if ls_trovato[ll_j] <> "ok" then
		
		FileWrite(li_FileNum, fs_cod_prodotto[ll_j] + "~t" + fs_des_prodotto[ll_j] + "~t" + fs_quantita[ll_j] + "~r")
		
	end if
next

fileclose( li_filenum)

return 0
end function

on w_ripara_magazzino.create
int iCurrent
call super::create
this.pb_2=create pb_2
this.sle_file2=create sle_file2
this.st_4=create st_4
this.cb_1=create cb_1
this.em_data_ini=create em_data_ini
this.st_2=create st_2
this.dw_report=create dw_report
this.em_da_data=create em_da_data
this.st_3=create st_3
this.st_1=create st_1
this.sle_file=create sle_file
this.pb_1=create pb_1
this.cb_report=create cb_report
this.ln_1=create ln_1
this.ln_2=create ln_2
this.ln_3=create ln_3
this.ln_4=create ln_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.pb_2
this.Control[iCurrent+2]=this.sle_file2
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.em_data_ini
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.dw_report
this.Control[iCurrent+8]=this.em_da_data
this.Control[iCurrent+9]=this.st_3
this.Control[iCurrent+10]=this.st_1
this.Control[iCurrent+11]=this.sle_file
this.Control[iCurrent+12]=this.pb_1
this.Control[iCurrent+13]=this.cb_report
this.Control[iCurrent+14]=this.ln_1
this.Control[iCurrent+15]=this.ln_2
this.Control[iCurrent+16]=this.ln_3
this.Control[iCurrent+17]=this.ln_4
end on

on w_ripara_magazzino.destroy
call super::destroy
destroy(this.pb_2)
destroy(this.sle_file2)
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.em_data_ini)
destroy(this.st_2)
destroy(this.dw_report)
destroy(this.em_da_data)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.sle_file)
destroy(this.pb_1)
destroy(this.cb_report)
destroy(this.ln_1)
destroy(this.ln_2)
destroy(this.ln_3)
destroy(this.ln_4)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report



end event

type pb_2 from picturebutton within w_ripara_magazzino
integer x = 2491
integer y = 1760
integer width = 101
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
string disabledname = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I

value = GetFileOpenName("Selezione", docname, named, "*.*", "Tutti i tipi (*.*),*.*")
if value < 1 then return

sle_file2.text = docname

	
end event

type sle_file2 from singlelineedit within w_ripara_magazzino
integer x = 983
integer y = 1760
integer width = 1486
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_ripara_magazzino
integer x = 91
integer y = 1760
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documento con lista Prodotti:"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_ripara_magazzino
integer x = 2606
integer y = 1760
integer width = 366
integer height = 80
integer taborder = 23
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;string   ls_cod_prodotto[], ls_appo, ls_des_prodotto[], ls_cod_misura_mag[], ls_quantita[]
long     ll_file, ll_ret, ll_i
datetime ldt_da_data, ldt_a_data
integer li_tab

ldt_da_data = datetime( date( em_data_ini.text), 00:00:00)


ll_file = fileopen(sle_file2.text, linemode!, Read!)

ll_ret = 0
ll_i = 0

do while ll_ret >= 0
	
	ll_ret = fileread( ll_file, ls_appo)
	
	if isnull(ll_ret) then ll_ret = -1
	
	if ll_ret < 0 then exit
	
	if not isnull(ls_appo) and ls_appo <> "" then
		ll_i = ll_i + 1
		


		li_tab = Pos( ls_appo, "~t", 1)
		ls_cod_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_des_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )		
		ls_cod_misura_mag[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_quantita[ll_i] = Mid( ls_appo, li_tab + 1)

	end if	
	
loop

fileclose(ll_file)

wf_aggiorna_ini( ls_cod_prodotto, ls_des_prodotto, ls_quantita, ldt_da_data)


g_mb.messagebox( "APICE", "PROCEDURA TERMINATA!")
end event

type em_data_ini from editmask within w_ripara_magazzino
integer x = 983
integer y = 1860
integer width = 1486
integer height = 80
integer taborder = 33
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "01/01/2004"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
end type

type st_2 from statictext within w_ripara_magazzino
integer x = 91
integer y = 1860
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "Data Aggiorna INI:"
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_ripara_magazzino
integer x = 23
integer y = 220
integer width = 3634
integer height = 1440
integer taborder = 90
string dataobject = "d_dw_ripara_magazzino"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type em_da_data from editmask within w_ripara_magazzino
integer x = 914
integer y = 120
integer width = 1486
integer height = 80
integer taborder = 33
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "31/12/2004"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
end type

type st_3 from statictext within w_ripara_magazzino
integer x = 23
integer y = 120
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Data Inventario:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_ripara_magazzino
integer x = 23
integer y = 20
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documento con lista Prodotti:"
boolean focusrectangle = false
end type

type sle_file from singlelineedit within w_ripara_magazzino
integer x = 914
integer y = 20
integer width = 1486
integer height = 80
integer taborder = 23
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type pb_1 from picturebutton within w_ripara_magazzino
integer x = 2423
integer y = 20
integer width = 101
integer height = 84
integer taborder = 13
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
string disabledname = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I

value = GetFileOpenName("Selezione", docname, named, "*.*", "Tutti i tipi (*.*),*.*")
if value < 1 then return

sle_file.text = docname

	
end event

type cb_report from commandbutton within w_ripara_magazzino
integer x = 2537
integer y = 20
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;string   ls_cod_prodotto[], ls_appo, ls_des_prodotto[], ls_cod_misura_mag[], ls_quantita[]
long     ll_file, ll_ret, ll_i
datetime ldt_da_data, ldt_a_data
integer li_tab

ldt_da_data = datetime( date( em_da_data.text), 00:00:00)


ll_file = fileopen(sle_file.text, linemode!, Read!)

ll_ret = 0
ll_i = 0

do while ll_ret >= 0
	
	ll_ret = fileread( ll_file, ls_appo)
	
	if isnull(ll_ret) then ll_ret = -1
	
	if ll_ret < 0 then exit
	
	if not isnull(ls_appo) and ls_appo <> "" then
		ll_i = ll_i + 1
		


		li_tab = Pos( ls_appo, "~t", 1)
		ls_cod_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_des_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )		
		ls_cod_misura_mag[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_quantita[ll_i] = Mid( ls_appo, li_tab + 1)

	end if	
	
loop

fileclose(ll_file)

wf_opera( ls_cod_prodotto, ls_des_prodotto, ls_cod_misura_mag, ls_quantita, ldt_da_data)


g_mb.messagebox( "APICE", "PROCEDURA TERMINATA!")
end event

type ln_1 from line within w_ripara_magazzino
long linecolor = 255
integer linethickness = 12
integer beginx = 23
integer beginy = 1720
integer endx = 3634
integer endy = 1720
end type

type ln_2 from line within w_ripara_magazzino
long linecolor = 255
integer linethickness = 12
integer beginx = 23
integer beginy = 1980
integer endx = 3634
integer endy = 1980
end type

type ln_3 from line within w_ripara_magazzino
long linecolor = 255
integer linethickness = 12
integer beginx = 23
integer beginy = 1720
integer endx = 23
integer endy = 1980
end type

type ln_4 from line within w_ripara_magazzino
long linecolor = 255
integer linethickness = 12
integer beginx = 3634
integer beginy = 1720
integer endx = 3634
integer endy = 1980
end type

