﻿$PBExportHeader$w_ripara_magazzino_con_deposito.srw
$PBExportComments$Report Inventario del Magazino
forward
global type w_ripara_magazzino_con_deposito from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_ripara_magazzino_con_deposito
end type
type st_num_prod from statictext within w_ripara_magazzino_con_deposito
end type
type hpb_1 from hprogressbar within w_ripara_magazzino_con_deposito
end type
type rb_solo_scarichi from radiobutton within w_ripara_magazzino_con_deposito
end type
type rb_solo_carichi from radiobutton within w_ripara_magazzino_con_deposito
end type
type rb_tutti from radiobutton within w_ripara_magazzino_con_deposito
end type
type st_5 from statictext within w_ripara_magazzino_con_deposito
end type
type dw_selezione from uo_cs_xx_dw within w_ripara_magazzino_con_deposito
end type
type st_prodotto from statictext within w_ripara_magazzino_con_deposito
end type
type pb_2 from picturebutton within w_ripara_magazzino_con_deposito
end type
type sle_file2 from singlelineedit within w_ripara_magazzino_con_deposito
end type
type st_4 from statictext within w_ripara_magazzino_con_deposito
end type
type cb_1 from commandbutton within w_ripara_magazzino_con_deposito
end type
type em_data_ini from editmask within w_ripara_magazzino_con_deposito
end type
type st_2 from statictext within w_ripara_magazzino_con_deposito
end type
type dw_report from uo_cs_xx_dw within w_ripara_magazzino_con_deposito
end type
type em_da_data from editmask within w_ripara_magazzino_con_deposito
end type
type st_3 from statictext within w_ripara_magazzino_con_deposito
end type
type st_1 from statictext within w_ripara_magazzino_con_deposito
end type
type sle_file from singlelineedit within w_ripara_magazzino_con_deposito
end type
type pb_1 from picturebutton within w_ripara_magazzino_con_deposito
end type
type cb_report from commandbutton within w_ripara_magazzino_con_deposito
end type
type ln_1 from line within w_ripara_magazzino_con_deposito
end type
type ln_2 from line within w_ripara_magazzino_con_deposito
end type
type ln_3 from line within w_ripara_magazzino_con_deposito
end type
type ln_4 from line within w_ripara_magazzino_con_deposito
end type
type r_1 from rectangle within w_ripara_magazzino_con_deposito
end type
end forward

global type w_ripara_magazzino_con_deposito from w_cs_xx_principale
integer x = 73
integer y = 300
integer width = 4338
integer height = 2460
string title = "Inventario Magazzino"
cb_2 cb_2
st_num_prod st_num_prod
hpb_1 hpb_1
rb_solo_scarichi rb_solo_scarichi
rb_solo_carichi rb_solo_carichi
rb_tutti rb_tutti
st_5 st_5
dw_selezione dw_selezione
st_prodotto st_prodotto
pb_2 pb_2
sle_file2 sle_file2
st_4 st_4
cb_1 cb_1
em_data_ini em_data_ini
st_2 st_2
dw_report dw_report
em_da_data em_da_data
st_3 st_3
st_1 st_1
sle_file sle_file
pb_1 pb_1
cb_report cb_report
ln_1 ln_1
ln_2 ln_2
ln_3 ln_3
ln_4 ln_4
r_1 r_1
end type
global w_ripara_magazzino_con_deposito w_ripara_magazzino_con_deposito

type variables
boolean ib_interrupt
end variables

forward prototypes
public function integer wf_opera (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_cod_misura[], string fs_quantita[], datetime fdt_da_data)
public function integer wf_aggiorna_ini (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_quantita[], datetime fdt_da_data)
public function integer wf_opera_con_deposito (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_cod_misura[], string fs_quantita[], string fs_cod_deposito, datetime fdt_da_data)
end prototypes

public function integer wf_opera (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_cod_misura[], string fs_quantita[], datetime fdt_da_data);string ls_cod_cat_mer_1, ls_flag_bloccato, ls_where, ls_error, ls_messaggio,&
       ls_cod_misura_mag, ls_cod_cat_mer, ls_flag_blocco, ls_sort, ls_sql, ls_cod_prodotto_a, &
		 ls_des_prodotto, ls_flag_det_depositi, ls_cod_deposito, ls_flag_det_stock, &
		 ls_flag_visualizza_val, ls_flag_tipo_valorizzazione, ls_chiave[], &
		 ls_intestazione, ls_des_azienda, ls_flag_visulizza_giac_zero, ls_costo_unitario_t, &
		 ls_vettore_nullo[], ls_cod_prodotto, ls_trovato[]
integer li_return, li_cont, ll_i
long ll_newrow, ll_num_righe, ll_num_stock, ll_j,ll_ret
dec{4} ld_saldo_quan_inizio_anno, ld_quant_val[], ld_giacenza, ld_totale_quantita, ld_costo_medio, ld_quan_acq, ld_val_inizio_anno, &
       ld_costo_standard, ld_prezzo_acquisto, ld_costo, ld_nulla, ld_giacenza_stock[], ld_costo_unitario, ld_vettore_nullo[], &
		 ld_costo_medio_continuo,ld_costo_medio_continuo_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_a, ldt_data_blocco, ldt_data_chiusura_annuale
uo_costo_medio_continuo luo_costo_medio
uo_magazzino luo_magazzino

// datetime ldt_data_a è la data di inventario
dw_report.reset()
dw_report.setredraw(false)

select data_chiusura_annuale
into   :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

ldt_data_a = fdt_da_data

if ldt_data_a < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if


ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
			"  from   anag_prodotti " +&
			"  where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			"  order by cod_prodotto " 
					
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;
	
DO while 0 = 0

	FETCH cur_1 INTO :ls_cod_prodotto, 
						  :ls_des_prodotto, 
						  :ls_cod_misura_mag;
						  
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		return -1
	end if
	
	select count(*)
	into   :ll_num_stock
	from   stock
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_prodotto = :ls_cod_prodotto;
			 
	if ll_num_stock > 0 then		// elaboro solo prodotti in stock
		ll_newrow = dw_report.insertrow(0) 
		dw_report.ScrollToRow(ll_newrow)
		dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
		dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
		dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)		
	end if
	
LOOP

CLOSE cur_1 ;
	
ls_trovato = fs_cod_prodotto	
	
ll_num_righe = dw_report.rowcount()

for ll_i = 1 to ll_num_righe
	
	ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
	
	for li_cont = 1 to 14
		ld_quant_val[li_cont] = 0
	next
		
	luo_magazzino = CREATE uo_magazzino
	
	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
		
	if li_return <0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		return -1
	end if	
		
	ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
	dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
	
	
	for ll_j = 1 to upperbound( fs_cod_prodotto)
		
		if fs_cod_prodotto[ll_j] = ls_cod_prodotto then
			
			dw_report.setitem(ll_i, "rs_cod_prodotto_2", fs_cod_prodotto[ll_j])
			dw_report.setitem(ll_i, "rs_descrizione_2", fs_des_prodotto[ll_j])
			dw_report.setitem(ll_i, "rs_cod_misura_mag", fs_cod_misura[ll_j])
			dw_report.setitem(ll_i, "rd_giacenza_2", fs_quantita[ll_j])
			ls_trovato[ll_j] = "ok"
			exit
			
		end if
		
	next
	
next

integer li_FileNum

li_FileNum = FileOpen("C:\prodotti_non_considerati.TXT", LineMode!, Write!, LockWrite!, Append!)

for ll_i = 1 to upperbound( fs_cod_prodotto)
	if ls_trovato[ll_i] <> "ok" then
		
		FileWrite(li_FileNum, fs_cod_prodotto[ll_i] + "~t" + fs_des_prodotto[ll_i] + "~t" + fs_cod_misura[ll_i] + "~t" + fs_quantita[ll_i] + "~r")
		
	end if
next

fileclose( li_filenum)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'

dw_report.Change_DW_Current()


return 0
end function

public function integer wf_aggiorna_ini (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_quantita[], datetime fdt_da_data);string		 ls_sql, ls_cod_prodotto, ls_trovato[]
long         ll_anno_registrazione, ll_num_registrazione, ll_j, li_1, li_2
boolean      lb_trovato = false
dec{4}       ld_giacenza, ld_giacenza_2
datetime ldt_data_a, ldt_data_blocco, ldt_data_ini
uo_magazzino luo_magazzino

ldt_data_ini = fdt_da_data

ls_sql = "	select anno_registrazione, num_registrazione, cod_prodotto, quan_movimento FROM mov_magazzino " + &
			"  where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_movimento = 'INI' and data_registrazione = '" + string( ldt_data_ini, s_cs_xx.db_funzioni.formato_data) + "' " + &
			"  order by cod_prodotto " 
					
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "APICE", "Errore durante l'apertura del cursore cur_1: " + sqlca.sqlerrtext)
	return -1
end if

li_1 = FileOpen("C:\prodotti_aggiornati.TXT", LineMode!, Write!, LockWrite!, Append!)
li_2 = FileOpen("C:\prodotti_non_aggiornati.TXT", LineMode!, Write!, LockWrite!, Append!)

ls_trovato = fs_cod_prodotto

DO while 0 = 0

	FETCH cur_1 INTO :ll_anno_registrazione, 
						  :ll_num_registrazione, 
						  :ls_cod_prodotto,
						  :ld_giacenza;
						  
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		rollback;
		return -1
	end if
	
	lb_trovato = false
	
	for ll_j = 1 to upperbound( fs_cod_prodotto)
		
		if fs_cod_prodotto[ll_j] = ls_cod_prodotto then
			
			if ls_trovato[ll_j] = "ok" then
				lb_trovato = false   // ini doppi
				exit
				
			else
				ls_trovato[ll_j] = "ok"
				ld_giacenza_2 = dec(fs_quantita[ll_j])
				
				update mov_magazzino 
				set    quan_movimento = :ld_giacenza_2
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox( "APICE", "Errore durante l'aggiornamento del movimento di magazzino: " + sqlca.sqlerrtext)
					g_mb.messagebox( "APICE", "prodotto: " + ls_cod_prodotto + " giacenza: " + string(ld_giacenza))
					close cur_1;
					rollback;
					return -1
				end if
				
				filewrite(li_1, ls_cod_prodotto + "~t" + string(ld_giacenza))
				
				lb_trovato = true	
				exit
			end if
		end if
		
	next
	
	if not lb_trovato then
		
		update mov_magazzino 
		set    quan_movimento = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "APICE", "Errore durante l'aggiornamento del movimento di magazzino: " + sqlca.sqlerrtext)
			g_mb.messagebox( "APICE", "prodotto: " + ls_cod_prodotto + " giacenza: 0 ")
			close cur_1;
			rollback;
			return -1
		end if				 
				 
		filewrite(li_2, ls_cod_prodotto + "~t0")
		
	end if
	
LOOP

CLOSE cur_1 ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "", "Errore durante la chiusura del cursore: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

fileclose( li_1)
fileclose( li_2)
	

integer li_FileNum

li_FileNum = FileOpen("C:\prodotti_non_considerati_INI.TXT", LineMode!, Write!, LockWrite!, Append!)

for ll_j = 1 to upperbound( fs_cod_prodotto)
	if ls_trovato[ll_j] <> "ok" then
		
		FileWrite(li_FileNum, fs_cod_prodotto[ll_j] + "~t" + fs_des_prodotto[ll_j] + "~t" + fs_quantita[ll_j] + "~r")
		
	end if
next

fileclose( li_filenum)

return 0
end function

public function integer wf_opera_con_deposito (string fs_cod_prodotto[], string fs_des_prodotto[], string fs_cod_misura[], string fs_quantita[], string fs_cod_deposito, datetime fdt_da_data);string ls_cod_cat_mer_1, ls_flag_bloccato, ls_where, ls_error, ls_messaggio,&
       ls_cod_misura_mag, ls_cod_cat_mer, ls_flag_blocco, ls_sort, ls_sql, ls_cod_prodotto_a, &
		 ls_des_prodotto, ls_flag_det_depositi, ls_cod_deposito, ls_flag_det_stock, &
		 ls_flag_visualizza_val, ls_flag_tipo_valorizzazione, ls_chiave[], &
		 ls_intestazione, ls_des_azienda, ls_flag_visulizza_giac_zero, ls_costo_unitario_t, &
		 ls_vettore_nullo[], ls_cod_prodotto, ls_trovato[]
integer li_return, li_cont, ll_i
long ll_newrow, ll_num_righe, ll_num_stock, ll_j,ll_ret
dec{4} ld_saldo_quan_inizio_anno, ld_quant_val[], ld_giacenza, ld_totale_quantita, ld_costo_medio, ld_quan_acq, ld_val_inizio_anno, &
       ld_costo_standard, ld_prezzo_acquisto, ld_costo, ld_nulla, ld_giacenza_stock[], ld_costo_unitario, ld_vettore_nullo[], &
		 ld_costo_medio_continuo,ld_costo_medio_continuo_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_a, ldt_data_blocco, ldt_data_chiusura_annuale
uo_costo_medio_continuo luo_costo_medio
uo_magazzino luo_magazzino
datastore lds_ripara_magazzino_con_deposito
long ll_rows_to_compute, ll_row_compute

dec{4} ld_giacenza_v[]
long ll_indice
boolean ld_deposito_trovato = false
boolean ld_prodotto_trovato = false
dec{4} ld_giacenza_1, ld_giacenza_2

// datetime ldt_data_a è la data di inventario
dw_report.reset()
//dw_report.setredraw(false)

rb_tutti.checked = true
rb_solo_carichi.checked = false
rb_solo_scarichi.checked = false
dw_report.setfilter("")
dw_report.filter()

select data_chiusura_annuale
into   :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

ldt_data_a = fdt_da_data

if ldt_data_a < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if

st_prodotto.text = ""
hpb_1.position = 0
st_num_prod.text = ""

//vedo quanti prodotti dovrò elaborare
lds_ripara_magazzino_con_deposito = create datastore

//lds_ripara_magazzino_con_deposito.dataobject = "d_ds_ripara_magazzino_con_deposito"
lds_ripara_magazzino_con_deposito.dataobject = "d_ds_ripara_magazzino_con_deposito_stock"

lds_ripara_magazzino_con_deposito.settransobject(sqlca)

//ll_rows_to_compute = lds_ripara_magazzino_con_deposito.retrieve(s_cs_xx.cod_azienda)
ll_rows_to_compute = lds_ripara_magazzino_con_deposito.retrieve(s_cs_xx.cod_azienda, fs_cod_deposito)

/*
ls_sql = "	select anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto, anag_prodotti.cod_misura_mag " +&
			"  from   anag_prodotti " +&
			"  where  anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			"  order by anag_prodotti.cod_prodotto " 
*/

//prendo tutti i prodotti nella anag_prodotti con almeno uno stock nel deposito in esame
ls_sql = "	select distinct anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto, anag_prodotti.cod_misura_mag " +&
			"  from   anag_prodotti " +&
			" join stock on stock.cod_prodotto=anag_prodotti.cod_prodotto "+ &
			"  where  anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			" and stock.cod_deposito = '" + fs_cod_deposito + "' " +&
			"  order by anag_prodotti.cod_prodotto " 

//----------------------------
					
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;

hpb_1.maxposition = ll_rows_to_compute
ll_row_compute = 0

DO while 0 = 0
	ld_deposito_trovato = false
	ld_prodotto_trovato = false
	
	FETCH cur_1 INTO :ls_cod_prodotto, 
						  :ls_des_prodotto, 
						  :ls_cod_misura_mag;
						  
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then continue
	
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		return -1
	end if
	
	//visualizzo il prodotto in esame
	st_prodotto.text = ls_cod_prodotto
	ll_row_compute += 1
	//st_num_prod.text = string(ll_row_compute) + " di " + string(ll_rows_to_compute)
	st_num_prod.text = string((ll_row_compute / ll_rows_to_compute) * 100, "##0.00") + " %"
	hpb_1.stepit()
	
	// seleziono solo i prodotti in stock del deposito in esame
	/*
	select count(*)
	into   :ll_num_stock
	from   stock
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_prodotto = :ls_cod_prodotto and cod_deposito = :fs_cod_deposito;
	*/
	ll_num_stock = 1
		
	if ll_num_stock > 0 then
		//se entro nell'if vuol dire che si tratta di un prodotto in stock del deposito in esame
		
		ll_newrow = dw_report.insertrow(0) 
		dw_report.ScrollToRow(ll_newrow)
		
		//scrivo i valori dati da apice
		dw_report.setitem(ll_newrow, "rs_cod_deposito" ,fs_cod_deposito)
		dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
		dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
		dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
		
		//reset dell'array della qta
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next
		
		luo_magazzino = CREATE uo_magazzino
		
		//calcolo la giacenza per deposito
		li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
		destroy luo_magazzino
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if	
	
		//mi servono i dati sulla giacenza del solo deposito corrente
		ll_num_stock = upperbound(ls_chiave[])
		ld_giacenza = 0
		for ll_indice = 1 to ll_num_stock
			if ls_chiave[ll_indice] = fs_cod_deposito then
				ld_giacenza = ld_giacenza_stock[ll_indice]
				ld_deposito_trovato = true
			end if
		next
	
		//set della giacenza del deposito letta da apice
		if isnull(ld_giacenza) then ld_giacenza = 0
		dw_report.setitem(ll_newrow, "rd_giacenza", ld_giacenza)
		
		//metto prima una giacenza nulla per quanto riguarda quella fornita dal cliente
		//se poi becca il prodotto allora metterà la giacenza corretta, altrimenti restera a zero
		//quindi vuol dire che deve essere portata a zero dalla procedura da lanciare successivamente
		dw_report.setitem(ll_newrow, "rd_giacenza_2", "0")
		//---------------------------------------------------------------------------
		
		//cerco il prodotto in esame nell'array pre-caricato datomi dal cliente
		for ll_j = 1 to upperbound( fs_cod_prodotto)
			
			if fs_cod_prodotto[ll_j] = ls_cod_prodotto then
				//prodotto trovato
				dw_report.setitem(ll_newrow, "rs_cod_prodotto_2", fs_cod_prodotto[ll_j])
				dw_report.setitem(ll_newrow, "rs_descrizione_2", fs_des_prodotto[ll_j])
				dw_report.setitem(ll_newrow, "rs_cod_misura_mag_2", fs_cod_misura[ll_j])
				dw_report.setitem(ll_newrow, "rd_giacenza_2", fs_quantita[ll_j])
				ls_trovato[ll_j] = "ok"
				ld_prodotto_trovato = true				
				exit
				
			end if
			
		next
		
		//se non è stato trovato
		if not ld_prodotto_trovato then
			//riporto il codice, la descrizione e la UM di Apice, mentre la quantità sarà 0
			dw_report.setitem(ll_newrow, "rs_cod_prodotto_2", ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione_2", ls_des_prodotto)
			dw_report.setitem(ll_newrow, "rs_cod_misura_mag_2", ls_cod_misura_mag)
		end if
		ld_prodotto_trovato = false
		
		ld_giacenza_1 = dw_report.getitemnumber(ll_newrow, "rd_giacenza")
		if isnull(ld_giacenza_1) then ld_giacenza_1 = 0
		
		ld_giacenza_2 = dec(dw_report.getitemstring(ll_newrow, "rd_giacenza_2"))
		if isnull(ld_giacenza_2) then ld_giacenza_2 = 0
		
		ld_giacenza_2 = ld_giacenza_2 - ld_giacenza_1
		if ld_giacenza_2 > 0 then
			//il cliente vuole una qta superiore a quella registrata in apice: fare CARICO
			dw_report.setitem(ll_newrow, "rs_tipo_riga", "C")
		elseif ld_giacenza_2 < 0 then
			//il cliente vuole una qta inferiore a quella registrata in apice: fare SCARICO
			dw_report.setitem(ll_newrow, "rs_tipo_riga", "S")
		else
			//=0
			dw_report.setitem(ll_newrow, "rs_tipo_riga", "N")
		end if
		
	end if

LOOP

CLOSE cur_1 ;

dw_report.Object.DataWindow.Print.Preview	='Yes'
//dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'

//dw_report.Change_DW_Current()

return 0

//VECCHIO CODICE CHE NON CONSIDERAVA I VARI DEPOSITI
/*
for ll_i = 1 to ll_num_righe
	
	ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
	st_prodotto.text = ls_cod_prodotto
	
	for li_cont = 1 to 14
		ld_quant_val[li_cont] = 0
	next
		
	luo_magazzino = CREATE uo_magazzino
	
	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
		
	if li_return <0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		return -1
	end if	
	
	//mi servono i dati sulla giacenza del solo deposito corrente
	ll_num_stock = upperbound(ls_chiave[])
	ld_giacenza_v[ll_i] = 0
	for ll_indice = 1 to ll_num_stock
		if ls_chiave[ll_indice] = fs_cod_deposito then
			ld_giacenza_v[ll_i] = ld_giacenza_stock[ll_indice]
		end if
	next
	
	//ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
	ld_giacenza = ld_giacenza_v[ll_i]
	
	dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
	
	//metto prima una giacenza nulla per quanto riguarda quella fornita dal cliente
	//se poi becca il prodotto allora metterà la giacenza corretta
	dw_report.setitem(ll_i, "rd_giacenza_2", 0)
	//---------------------------------------------------------------------------
	
	for ll_j = 1 to upperbound( fs_cod_prodotto)
		
		if fs_cod_prodotto[ll_j] = ls_cod_prodotto then
			
			dw_report.setitem(ll_i, "rs_cod_prodotto_2", fs_cod_prodotto[ll_j])
			dw_report.setitem(ll_i, "rs_descrizione_2", fs_des_prodotto[ll_j])
			dw_report.setitem(ll_i, "rs_cod_misura_mag", fs_cod_misura[ll_j])
			dw_report.setitem(ll_i, "rd_giacenza_2", fs_quantita[ll_j])
			ls_trovato[ll_j] = "ok"
			exit
			
		end if
		
	next
	
	hpb_1.stepit()
next

integer li_FileNum

li_FileNum = FileOpen("C:\prodotti_non_considerati.TXT", LineMode!, Write!, LockWrite!, Append!)

for ll_i = 1 to upperbound( fs_cod_prodotto)
	if ls_trovato[ll_i] <> "ok" then
		
		FileWrite(li_FileNum, fs_cod_prodotto[ll_i] + "~t" + fs_des_prodotto[ll_i] + "~t" + fs_cod_misura[ll_i] + "~t" + fs_quantita[ll_i] + "~r")
		
	end if
next

fileclose( li_filenum)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'

dw_report.Change_DW_Current()


return 0
*/
end function

on w_ripara_magazzino_con_deposito.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.st_num_prod=create st_num_prod
this.hpb_1=create hpb_1
this.rb_solo_scarichi=create rb_solo_scarichi
this.rb_solo_carichi=create rb_solo_carichi
this.rb_tutti=create rb_tutti
this.st_5=create st_5
this.dw_selezione=create dw_selezione
this.st_prodotto=create st_prodotto
this.pb_2=create pb_2
this.sle_file2=create sle_file2
this.st_4=create st_4
this.cb_1=create cb_1
this.em_data_ini=create em_data_ini
this.st_2=create st_2
this.dw_report=create dw_report
this.em_da_data=create em_da_data
this.st_3=create st_3
this.st_1=create st_1
this.sle_file=create sle_file
this.pb_1=create pb_1
this.cb_report=create cb_report
this.ln_1=create ln_1
this.ln_2=create ln_2
this.ln_3=create ln_3
this.ln_4=create ln_4
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.st_num_prod
this.Control[iCurrent+3]=this.hpb_1
this.Control[iCurrent+4]=this.rb_solo_scarichi
this.Control[iCurrent+5]=this.rb_solo_carichi
this.Control[iCurrent+6]=this.rb_tutti
this.Control[iCurrent+7]=this.st_5
this.Control[iCurrent+8]=this.dw_selezione
this.Control[iCurrent+9]=this.st_prodotto
this.Control[iCurrent+10]=this.pb_2
this.Control[iCurrent+11]=this.sle_file2
this.Control[iCurrent+12]=this.st_4
this.Control[iCurrent+13]=this.cb_1
this.Control[iCurrent+14]=this.em_data_ini
this.Control[iCurrent+15]=this.st_2
this.Control[iCurrent+16]=this.dw_report
this.Control[iCurrent+17]=this.em_da_data
this.Control[iCurrent+18]=this.st_3
this.Control[iCurrent+19]=this.st_1
this.Control[iCurrent+20]=this.sle_file
this.Control[iCurrent+21]=this.pb_1
this.Control[iCurrent+22]=this.cb_report
this.Control[iCurrent+23]=this.ln_1
this.Control[iCurrent+24]=this.ln_2
this.Control[iCurrent+25]=this.ln_3
this.Control[iCurrent+26]=this.ln_4
this.Control[iCurrent+27]=this.r_1
end on

on w_ripara_magazzino_con_deposito.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.st_num_prod)
destroy(this.hpb_1)
destroy(this.rb_solo_scarichi)
destroy(this.rb_solo_carichi)
destroy(this.rb_tutti)
destroy(this.st_5)
destroy(this.dw_selezione)
destroy(this.st_prodotto)
destroy(this.pb_2)
destroy(this.sle_file2)
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.em_data_ini)
destroy(this.st_2)
destroy(this.dw_report)
destroy(this.em_da_data)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.sle_file)
destroy(this.pb_1)
destroy(this.cb_report)
destroy(this.ln_1)
destroy(this.ln_2)
destroy(this.ln_3)
destroy(this.ln_4)
destroy(this.r_1)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report

dw_selezione.insertrow(0)



end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_deposito", &
//							sqlca, &
//							"anag_depositi", &
//							"cod_deposito", &
//                     "des_deposito", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_2 from commandbutton within w_ripara_magazzino_con_deposito
integer x = 480
integer y = 160
integer width = 114
integer height = 60
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "none"
end type

event clicked;string   ls_cod_prodotto[], ls_appo
long     ll_file, ll_ret, ll_i
datetime ldt_da_data, ldt_a_data
integer li_tab

string ls_cod_deposito[], ls_qta[]
string ls_cod_prodotto_tmp, ls_des_prodotto_tmp, ls_cod_misura_mag_tmp, ls_quantita_tmp, ls_cod_deposito_tmp
string ls_cod_deposito_corrente
long ll_indice

ldt_da_data = datetime( date( em_da_data.text), 00:00:00)

ls_cod_deposito_corrente ='003'

ll_file = fileopen(sle_file.text, linemode!, Read!)

ll_ret = 0
ll_i = 0
st_prodotto.text = "preparazione in corso..."
do while ll_ret >= 0
	
	ll_ret = fileread( ll_file, ls_appo)
	
	if isnull(ll_ret) then ll_ret = -1
	
	if ll_ret < 0 then exit
	
	if not isnull(ls_appo) and ls_appo <> "" then
		li_tab = Pos( ls_appo, "~t", 1)
		ls_cod_prodotto_tmp = Left( ls_appo, li_tab - 1)
		
		ls_quantita_tmp = Mid( ls_appo, li_tab + 1)
		
		//in alcuni casi potrei trovare il carattere TAB alla fine della riga
		//rimuovere eventuale TAB alla fine della stringa
		li_tab = Pos( ls_quantita_tmp, "~t", 1 )
		
		if li_tab > 0 then
			ls_quantita_tmp = left(ls_quantita_tmp, li_tab - 1)			
		end if

		ll_i = ll_i + 1
		ls_cod_prodotto[ll_i] = ls_cod_prodotto_tmp
		ls_qta[ll_i] = ls_quantita_tmp
	end if	
loop

fileclose(ll_file)

//cicla il vettore: ogni cod prodotto ha almeno uno stock in '003'
//se no allora scrivo nel log
string ls_file_error
integer li_file_error

//----------------------------
ls_file_error = "C:\prodotti_senza_stock.log"
Filedelete(ls_file_error)
li_file_error = FileOpen(ls_file_error,LineMode!, Write!, LockWrite!, Append!)
if li_file_error < -1 then
	g_mb.messagebox( "APICE", "Errore creazione file errori!")
	return
end if
fileclose(li_file_error)
//----------------------------
st_prodotto.text = ""
string ls_cod_prodotto_appo

for ll_i = 1 to upperbound(ls_cod_prodotto)	
	ls_cod_prodotto_tmp = ls_cod_prodotto[ll_i]
	ls_quantita_tmp = ls_qta[ll_i]
	
	st_prodotto.text = ls_cod_prodotto_tmp
	
	setnull(ll_ret)
	
	select count(*)
	into :ll_ret
	from   anag_prodotti
	join stock on stock.cod_prodotto=anag_prodotti.cod_prodotto
	where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda
		and stock.cod_deposito = '003' and stock.cod_prodotto=:ls_cod_prodotto_tmp;
	
	if sqlca.sqlcode = 0 and ll_ret > 0 then
		//stock trovato
	else
		//stock non trovato: scrivo log
		//-----------------------------------------------
		li_file_error = FileOpen(ls_file_error,LineMode!, Write!, LockWrite!, Append!)
		filewrite(li_file_error, ls_cod_prodotto_tmp + "~t" + ls_quantita_tmp)//+"~r~n")
		fileclose(li_file_error)
		//------------------------------------------------
	end if
next

g_mb.messagebox( "APICE", "PROCEDURA TERMINATA!")
end event

type st_num_prod from statictext within w_ripara_magazzino_con_deposito
integer x = 937
integer y = 260
integer width = 357
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 32768
long backcolor = 12632256
boolean border = true
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_ripara_magazzino_con_deposito
integer x = 1303
integer y = 260
integer width = 2949
integer height = 60
unsignedinteger maxposition = 100
integer setstep = 1
end type

type rb_solo_scarichi from radiobutton within w_ripara_magazzino_con_deposito
integer x = 3269
integer y = 140
integer width = 498
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Solo gli Scarichi"
end type

event clicked;rb_tutti.checked = false
rb_solo_carichi.checked = false

dw_report.setfilter('rs_tipo_riga="S"')
dw_report.filter()
end event

type rb_solo_carichi from radiobutton within w_ripara_magazzino_con_deposito
integer x = 2766
integer y = 140
integer width = 421
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Solo i Carichi"
end type

event clicked;rb_tutti.checked = false
rb_solo_scarichi.checked = false

dw_report.setfilter('rs_tipo_riga="C"')
dw_report.filter()
end event

type rb_tutti from radiobutton within w_ripara_magazzino_con_deposito
integer x = 2423
integer y = 140
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tutti"
boolean checked = true
end type

event clicked;rb_solo_carichi.checked = false
rb_solo_scarichi.checked = false

dw_report.setfilter("")
dw_report.filter()
end event

type st_5 from statictext within w_ripara_magazzino_con_deposito
integer x = 3177
integer y = 20
integer width = 1115
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "<- In genere fine dell~'anno da aggiustare"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_selezione from uo_cs_xx_dw within w_ripara_magazzino_con_deposito
integer x = 617
integer y = 120
integer width = 1669
integer height = 120
integer taborder = 90
string dataobject = "d_ripara_mag_sel_deposito"
boolean border = false
end type

type st_prodotto from statictext within w_ripara_magazzino_con_deposito
integer x = 46
integer y = 260
integer width = 869
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 32768
long backcolor = 12632256
boolean border = true
boolean focusrectangle = false
end type

type pb_2 from picturebutton within w_ripara_magazzino_con_deposito
integer x = 2491
integer y = 2100
integer width = 101
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
string disabledname = "C:\CS_70\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I

value = GetFileOpenName("Selezione", docname, named, "*.*", "Tutti i tipi (*.*),*.*")
if value < 1 then return

sle_file2.text = docname

	
end event

type sle_file2 from singlelineedit within w_ripara_magazzino_con_deposito
integer x = 983
integer y = 2100
integer width = 1486
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_ripara_magazzino_con_deposito
integer x = 91
integer y = 2100
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documento con lista Prodotti:"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_ripara_magazzino_con_deposito
integer x = 2606
integer y = 2100
integer width = 366
integer height = 80
integer taborder = 23
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;string   ls_cod_prodotto[], ls_appo, ls_des_prodotto[], ls_cod_misura_mag[], ls_quantita[]
long     ll_file, ll_ret, ll_i
datetime ldt_da_data, ldt_a_data
integer li_tab

string ls_cod_deposito_corrente, ls_cod_prodotto_tmp, ls_des_prodotto_tmp
string ls_cod_deposito_tmp, ls_cod_misura_mag_tmp, ls_quantita_tmp, ls_cod_deposito[]
long ll_indice

ldt_da_data = datetime( date( em_data_ini.text), 00:00:00)

////gestione selezione deposito --------------------------------------------------------------
//ls_cod_deposito_corrente = sle_deposito.text
//if ls_cod_deposito_corrente = "" or isnull(ls_cod_deposito_corrente) then
//	g_mb.messagebox("APICE", "Specificare il Deposito!")
//	return
//end if
//
//select count(*)
//into :ll_indice
//from anag_depositi
//where cod_azienda=:s_cs_xx.cod_azienda and cod_deposito=:ls_cod_deposito_corrente;
//
//if sqlca.sqlcode <> 0 then
//	g_mb.messagebox("APICE", "Deposito non trovato oppure si è verificato un errore durante la lettura del deposito!!")
//	return
//end if
////----------------------------------------------------------------------------------------------------

ls_cod_deposito_corrente = dw_selezione.getitemstring(1, "cod_deposito")
if ls_cod_deposito_corrente = "" or isnull(ls_cod_deposito_corrente) then
	g_mb.messagebox("APICE", "Specificare il Deposito!")
	return
end if

ll_file = fileopen(sle_file2.text, linemode!, Read!)

ll_ret = 0
ll_i = 0

do while ll_ret >= 0
	
	ll_ret = fileread( ll_file, ls_appo)
	
	if isnull(ll_ret) then ll_ret = -1
	
	if ll_ret < 0 then exit
	
	if not isnull(ls_appo) and ls_appo <> "" then
		ll_i = ll_i + 1
		
		/*
		li_tab = Pos( ls_appo, "~t", 1)
		ls_cod_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_des_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )		
		ls_cod_misura_mag[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_quantita[ll_i] = Mid( ls_appo, li_tab + 1)
		*/
		
		li_tab = Pos( ls_appo, "~t", 1)
		ls_cod_prodotto_tmp = Left( ls_appo, li_tab - 1)
		//ls_cod_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_des_prodotto_tmp = Left( ls_appo, li_tab - 1)
		//ls_des_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_cod_misura_mag_tmp = Left( ls_appo, li_tab - 1)
		//ls_cod_misura_mag[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_quantita_tmp = Left( ls_appo, li_tab - 1)
		//ls_quantita[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_cod_deposito_tmp = Mid( ls_appo, li_tab + 1)
		//ls_cod_deposito[ll_i] = Mid( ls_appo, li_tab + 1)
		
		//riempio il vettore solo se il prodotto letto dal file del cliente è del deposito corrente
		if ls_cod_deposito_tmp = ls_cod_deposito_corrente then
			ls_cod_prodotto[ll_i] = ls_cod_prodotto_tmp
			ls_des_prodotto[ll_i] = ls_des_prodotto_tmp
			ls_cod_misura_mag[ll_i] = ls_cod_misura_mag_tmp
			ls_quantita[ll_i] = ls_quantita_tmp
			ls_cod_deposito[ll_i] = ls_cod_deposito_tmp
		end if
		
		
	end if	
	
loop

fileclose(ll_file)

wf_aggiorna_ini( ls_cod_prodotto, ls_des_prodotto, ls_quantita, ldt_da_data)


g_mb.messagebox( "APICE", "PROCEDURA TERMINATA!")
end event

type em_data_ini from editmask within w_ripara_magazzino_con_deposito
integer x = 983
integer y = 2200
integer width = 1486
integer height = 80
integer taborder = 33
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "01/01/2004"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
end type

type st_2 from statictext within w_ripara_magazzino_con_deposito
integer x = 91
integer y = 2200
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "Data Aggiorna INI:"
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_ripara_magazzino_con_deposito
integer x = 23
integer y = 340
integer width = 4251
integer height = 1700
integer taborder = 90
string dataobject = "d_dw_ripara_magazzino_con_deposito"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type em_da_data from editmask within w_ripara_magazzino_con_deposito
integer x = 2811
integer y = 20
integer width = 366
integer height = 80
integer taborder = 33
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "31/12/2007"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
end type

type st_3 from statictext within w_ripara_magazzino_con_deposito
integer x = 2377
integer y = 20
integer width = 434
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Data Inventario:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_ripara_magazzino_con_deposito
integer x = 23
integer y = 20
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Documento con lista Prodotti:"
boolean focusrectangle = false
end type

type sle_file from singlelineedit within w_ripara_magazzino_con_deposito
integer x = 914
integer y = 20
integer width = 1211
integer height = 80
integer taborder = 23
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type pb_1 from picturebutton within w_ripara_magazzino_con_deposito
integer x = 2126
integer y = 20
integer width = 101
integer height = 84
integer taborder = 13
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
alignment htextalign = left!
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I

value = GetFileOpenName("Selezione", docname, named, "*.*", "Tutti i tipi (*.*),*.*")
if value < 1 then return

sle_file.text = docname

	
end event

type cb_report from commandbutton within w_ripara_magazzino_con_deposito
integer x = 91
integer y = 120
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
boolean default = true
end type

event clicked;string   ls_cod_prodotto[], ls_appo, ls_des_prodotto[], ls_cod_misura_mag[], ls_quantita[]
long     ll_file, ll_ret, ll_i
datetime ldt_da_data, ldt_a_data
integer li_tab

string ls_cod_deposito[]
string ls_cod_prodotto_tmp, ls_des_prodotto_tmp, ls_cod_misura_mag_tmp, ls_quantita_tmp, ls_cod_deposito_tmp
string ls_cod_deposito_corrente
long ll_indice

ldt_da_data = datetime( date( em_da_data.text), 00:00:00)

ls_cod_deposito_corrente = dw_selezione.getitemstring(1, "cod_deposito")
if ls_cod_deposito_corrente = "" or isnull(ls_cod_deposito_corrente) then
	g_mb.messagebox("APICE", "Specificare un Deposito!")
	return
end if

ll_file = fileopen(sle_file.text, linemode!, Read!)

ll_ret = 0
ll_i = 0

do while ll_ret >= 0
	
	ll_ret = fileread( ll_file, ls_appo)
	
	if isnull(ll_ret) then ll_ret = -1
	
	if ll_ret < 0 then exit
	
	if not isnull(ls_appo) and ls_appo <> "" then
		//ll_i = ll_i + 1
		
		li_tab = Pos( ls_appo, "~t", 1)
		ls_cod_prodotto_tmp = Left( ls_appo, li_tab - 1)
		//ls_cod_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_des_prodotto_tmp = Left( ls_appo, li_tab - 1)
		//ls_des_prodotto[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_cod_misura_mag_tmp = Left( ls_appo, li_tab - 1)
		//ls_cod_misura_mag[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_appo = Mid( ls_appo, li_tab + 1)
		li_tab = Pos( ls_appo, "~t", 1 )
		ls_quantita_tmp = Left( ls_appo, li_tab - 1)
		//ls_quantita[ll_i] = Left( ls_appo, li_tab - 1)
		
		ls_cod_deposito_tmp = Mid( ls_appo, li_tab + 1)
		//ls_cod_deposito[ll_i] = Mid( ls_appo, li_tab + 1)
		
		//inn alcuni casi potrei trovare il carattere TAB alla fine della riga
		//rimuovere eventuale TAB alla fine della stringa
		li_tab = Pos( ls_cod_deposito_tmp, "~t", 1 )
		
		if li_tab > 0 then
			ls_cod_deposito_tmp = left(ls_cod_deposito_tmp, li_tab - 1)			
		end if
		
		//riempio il vettore solo se il prodotto letto dal file del cliente è del deposito corrente
		if ls_cod_deposito_tmp = ls_cod_deposito_corrente then
			ll_i = ll_i + 1
			
			ls_cod_prodotto[ll_i] = ls_cod_prodotto_tmp
			ls_des_prodotto[ll_i] = ls_des_prodotto_tmp
			ls_cod_misura_mag[ll_i] = ls_cod_misura_mag_tmp
			ls_quantita[ll_i] = ls_quantita_tmp
			ls_cod_deposito[ll_i] = ls_cod_deposito_tmp
		end if
	end if	
	
loop

fileclose(ll_file)

//wf_opera( ls_cod_prodotto, ls_des_prodotto, ls_cod_misura_mag, ls_quantita, ldt_da_data)
wf_opera_con_deposito( ls_cod_prodotto, ls_des_prodotto, ls_cod_misura_mag, ls_quantita, ls_cod_deposito_corrente, ldt_da_data)


g_mb.messagebox( "APICE", "PROCEDURA TERMINATA!")

dw_selezione.setfocus()
end event

type ln_1 from line within w_ripara_magazzino_con_deposito
long linecolor = 255
integer linethickness = 12
integer beginx = 23
integer beginy = 2060
integer endx = 3634
integer endy = 2060
end type

type ln_2 from line within w_ripara_magazzino_con_deposito
long linecolor = 255
integer linethickness = 12
integer beginx = 23
integer beginy = 2320
integer endx = 3634
integer endy = 2320
end type

type ln_3 from line within w_ripara_magazzino_con_deposito
long linecolor = 255
integer linethickness = 12
integer beginx = 23
integer beginy = 2060
integer endx = 23
integer endy = 2320
end type

type ln_4 from line within w_ripara_magazzino_con_deposito
long linecolor = 255
integer linethickness = 12
integer beginx = 3634
integer beginy = 2060
integer endx = 3634
integer endy = 2320
end type

type r_1 from rectangle within w_ripara_magazzino_con_deposito
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 2309
integer y = 120
integer width = 1577
integer height = 100
end type

