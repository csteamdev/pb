﻿$PBExportHeader$w_sel_report_righe_fat_euro.srw
$PBExportComments$W Report Righe Fattura Vendita
forward
global type w_sel_report_righe_fat_euro from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_sel_report_righe_fat_euro
end type
type cb_report from commandbutton within w_sel_report_righe_fat_euro
end type
type cb_selezione from commandbutton within w_sel_report_righe_fat_euro
end type
type dw_selezione from uo_cs_xx_dw within w_sel_report_righe_fat_euro
end type
type dw_report from uo_cs_xx_dw within w_sel_report_righe_fat_euro
end type
end forward

global type w_sel_report_righe_fat_euro from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Righe Fattura"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_sel_report_righe_fat_euro w_sel_report_righe_fat_euro

forward prototypes
public function integer wf_righe_fattura ()
end prototypes

public function integer wf_righe_fattura ();string   ls_da_cod_documento, ls_a_cod_documento, ls_cod_cliente, ls_rag_soc_1, ls_cod_tipo_fat_ven, &
		   ls_cod_agente_1, ls_rag_soc_agente, ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_cod_iva, &
		   ls_flag_contabilita, ls_des_tipo_fat_ven, ls_sql_tes, ls_sql_det, ls_where, &
	  	   ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_numeratore_doc, ls_nota_dettaglio, ls_cod_tipo_det_ven
		 
long     ll_riga, ll_anno_reg_bol_ven, ll_num_reg_bol_ven, ll_anno_reg_ord_ven, ll_num_reg_ord_ven, &
		   ll_anno_documento, ll_num_documento, ll_da_num_documento, ll_a_num_documento, &
			ll_anno_registrazione, ll_num_registrazione, ll_num_registrazione_mov_mag, ll_num_registrazione_bol_ven, ll_y, ll_max   
			
double   ld_valore_sconto, ld_sconto, ld_prezzo_vendita, ld_sconti[]			
			
dec{4}	ld_quan_fatturata, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, &
			ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_valore_riga, &
			ld_provvigione_1, ld_provvigione_2, ld_fat_conversione_ven, ld_cambio, ld_perc			
		 
datetime ldt_da_data_fattura,ldt_a_data_fattura, ldt_data_registrazione, ldt_data_fattura

string   ls_cod_tipo_fat_ven_sel, ls_where_tipo_fat, ls_cod_documento, ls_numeratore_documento, ls_cod_prodotto_sel


dw_report.reset()

dw_report.setredraw( false)

//Assegnazione variabili selezione

ll_anno_documento = dw_selezione.getitemnumber(1,"anno_documento")

ll_da_num_documento = dw_selezione.getitemnumber(1,"da_num_documento")

ll_a_num_documento = dw_selezione.getitemnumber(1,"a_num_documento")

ls_da_cod_documento = dw_selezione.getitemstring(1,"da_cod_documento")

ls_a_cod_documento = dw_selezione.getitemstring(1,"a_cod_documento")

ldt_da_data_fattura = datetime(dw_selezione.getitemdate(1,"da_data_fattura"))

ldt_a_data_fattura = datetime(dw_selezione.getitemdate(1,"a_data_fattura"))

ls_flag_contabilita = dw_selezione.getitemstring(1,"flag_contabilita")

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")

ls_cod_agente_1 = dw_selezione.getitemstring(1,"cod_agente_1")

ls_cod_tipo_fat_ven_sel = dw_selezione.getitemstring(1,"cod_tipo_fat_ven")

ls_cod_prodotto_sel = dw_selezione.getitemstring( 1, "cod_prodotto")

if ls_cod_prodotto_sel = "" then setnull(ls_cod_prodotto_sel)

/////////////////////Estrazione dati ed assegnazione alla DW del report///////////////////


//Creazione WHERE dinamico
ls_where = "where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "'"

if not(isnull(ll_anno_documento)) then
	ls_where = ls_where + " and tes_fat_ven.anno_documento = '" + string(ll_anno_documento) + "'"
end if

if not (isnull(ll_da_num_documento)) then
	ls_where = ls_where + " and tes_fat_ven.num_documento >= '" + string(ll_da_num_documento) + "'"
end if

if not (isnull(ll_a_num_documento)) then
	ls_where = ls_where + " and tes_fat_ven.num_documento <= '" + string(ll_a_num_documento) + "'"
end if

if not (isnull(ls_da_cod_documento)) then
	ls_where = ls_where + " and tes_fat_ven.cod_documento >= '" + ls_da_cod_documento + "'"
end if

if not (isnull(ls_a_cod_documento)) then
	ls_where = ls_where + " and tes_fat_ven.cod_documento <= '" + ls_a_cod_documento + "'"
end if

if not ((isnull(ldt_da_data_fattura)) and (isnull(ldt_a_data_fattura))) then
	ls_where = ls_where + " and tes_fat_ven.data_fattura between '" + string(ldt_da_data_fattura, s_cs_xx.db_funzioni.formato_data) + "' and '" + string(ldt_a_data_fattura, s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not (isnull(ls_cod_cliente)) then
	ls_where = ls_where + " and tes_fat_ven.cod_cliente = '" + ls_cod_cliente + "'"
end if

if not (isnull(ls_cod_agente_1)) then
	ls_where = ls_where + " and tes_fat_ven.cod_agente_1 = '" + ls_cod_agente_1 + "'"
end if

if not (isnull(ls_cod_tipo_fat_ven_sel)) then
	ls_where = ls_where + " and tes_fat_ven.cod_tipo_fat_ven = '" + ls_cod_tipo_fat_ven_sel + "'"
end if

//Se il flag è impostato a "TUTTE" allora non si controlla il flag
if ls_flag_contabilita <> "T" then
	ls_where = ls_where + " and tes_fat_ven.flag_contabilita = '" + ls_flag_contabilita + "'"
end if


//Cursore sulla testata delle fatture
declare cu_tes dynamic cursor for sqlsa;

ls_sql_tes = "select tes_fat_ven.num_registrazione, tes_fat_ven.anno_registrazione, tes_fat_ven.cod_tipo_fat_ven, " + &
"tes_fat_ven.cod_tipo_listino_prodotto, tes_fat_ven.cod_cliente, tes_fat_ven.cod_valuta, tes_fat_ven.data_registrazione, " + &
" tes_fat_ven.cod_agente_1, tes_fat_ven.cod_documento, tes_fat_ven.numeratore_documento, tes_fat_ven.anno_documento, " + &
" tes_fat_ven.num_documento, tes_fat_ven.data_fattura " + &
" from tes_fat_ven " + ls_where + " order by tes_fat_ven.cod_documento, tes_fat_ven.numeratore_documento, tes_fat_ven.anno_documento, tes_fat_ven.num_documento "

prepare sqlsa from :ls_sql_tes;

open dynamic cu_tes;

do while 1=1
fetch cu_tes into :ll_num_registrazione, 
						:ll_anno_registrazione, 
						:ls_cod_tipo_fat_ven, 
						:ls_cod_tipo_listino_prodotto, 
						:ls_cod_cliente, 
						:ls_cod_valuta, 
						:ldt_data_registrazione,
						:ls_cod_agente_1, 
						:ls_cod_documento, 
						:ls_numeratore_documento, 
						:ll_anno_documento, 
						:ll_num_documento, 
						:ldt_data_fattura;
						
	
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	ls_numeratore_doc = ls_cod_documento + " " + string(ll_anno_documento) + " " + ls_numeratore_documento + " " + string(ll_num_documento) + " " + string(ldt_data_fattura, "dd/mm/yyyy")
	
	if isnull(ls_numeratore_doc) then 
		ls_numeratore_doc = string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
	end if
	
	//Estrarre la descrizione dell'agente
	select rag_soc_1
	into   :ls_rag_soc_agente
	from   anag_agenti  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_agente = :ls_cod_agente_1;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_rag_soc_agente)
	end if
	
	//Estrarre la descrizione del tipo_fattura
	select des_tipo_fat_ven
	into   :ls_des_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_des_tipo_fat_ven)
	end if
	
	//Estrarre la descrizione del cliente
	select rag_soc_1
	into   :ls_rag_soc_1
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		setnull(ls_cod_cliente)
	end if
		
	//Cursore sul dettaglio delle fatture
	declare cu_det dynamic cursor for sqlsa;
	//---claudia 18/04/07 aggiunto l'ordinamento
	//-claudia 18/04/07  ls_nota_dettaglio inserita per le desacrizioni in nota
	ls_sql_det = "select det_fat_ven.cod_prodotto, det_fat_ven.des_prodotto, det_fat_ven.cod_misura, " + &
	" det_fat_ven.quan_fatturata, det_fat_ven.prezzo_vendita, det_fat_ven.sconto_1," + &
	" det_fat_ven.sconto_2, det_fat_ven.sconto_3, det_fat_ven.sconto_4, det_fat_ven.sconto_5, " + &
	" det_fat_ven.sconto_6, det_fat_ven.sconto_7, det_fat_ven.sconto_8, det_fat_ven.sconto_9, " + &
	" det_fat_ven.sconto_10, det_fat_ven.cod_iva, det_fat_ven.provvigione_1, det_fat_ven.provvigione_2, " + &
	" det_fat_ven.num_registrazione_mov_mag, det_fat_ven.num_registrazione_bol_ven, det_fat_ven.fat_conversione_ven," + &
	" det_fat_ven.anno_registrazione_bol_ven, det_fat_ven.anno_reg_ord_ven, det_fat_ven.num_reg_ord_ven,  "+&
	" det_fat_ven.nota_dettaglio, det_fat_ven.cod_tipo_det_ven  " + &
	" from det_fat_ven" + &
	" where det_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "'" + &
	" and det_fat_ven.anno_registrazione = " + string(ll_anno_registrazione) + &
	" and det_fat_ven.num_registrazione = " + string(ll_num_registrazione) +&
	"order by cod_azienda, " +&
	"anno_registrazione, " +&
	"num_registrazione," +&
	"prog_riga_fat_ven"
	


	prepare sqlsa from :ls_sql_det;
	
	open dynamic cu_det;
	
	do while 1=1
	fetch cu_det into   :ls_cod_prodotto, 
							:ls_des_prodotto, 
							:ls_cod_misura, 
							:ld_quan_fatturata, 
							:ld_prezzo_vendita, 
							:ld_sconto_1, 
							:ld_sconto_2, 
							:ld_sconto_3, 
							:ld_sconto_4, 
							:ld_sconto_5, 
							:ld_sconto_6, 
							:ld_sconto_7, 
							:ld_sconto_8, 
							:ld_sconto_9, 
							:ld_sconto_10, 
							:ls_cod_iva, 
							:ld_provvigione_1, 
							:ld_provvigione_2, 
							:ll_num_registrazione_mov_mag, 
							:ll_num_registrazione_bol_ven, 
							:ld_fat_conversione_ven,
							:ll_anno_reg_bol_ven, 
							:ll_anno_reg_ord_ven, 
							:ll_num_reg_ord_ven,
							:ls_nota_dettaglio,
							:ls_cod_tipo_det_ven;
								
   	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		
		if not isnull(ls_cod_prodotto_sel) then
			
			if isnull(ls_cod_prodotto) then continue
			
			if ls_cod_prodotto = "" then continue
			
			if ls_cod_prodotto_sel <> ls_cod_prodotto then continue

		end if		
		
		//Calcolo della qtà effettiva
		ld_quan_fatturata = ld_quan_fatturata * ld_fat_conversione_ven

		//Calcolo del prezzo effettivo unitario
		ld_prezzo_vendita = ld_prezzo_vendita / ld_fat_conversione_ven

		//Calcolo dello sconto tot		
      	ld_sconti[1] = ld_sconto_1
		ld_sconti[2] = ld_sconto_2
		ld_sconti[3] = ld_sconto_3
		ld_sconti[4] = ld_sconto_4
		ld_sconti[5] = ld_sconto_5
		ld_sconti[6] = ld_sconto_6
		ld_sconti[7] = ld_sconto_7
		ld_sconti[8] = ld_sconto_8
		ld_sconti[9] = ld_sconto_9
		ld_sconti[10] = ld_sconto_10
		
		ld_perc = 100
		ll_max=upperbound(ld_sconti)
		
		for ll_y = 1 to ll_max
			ld_perc = ld_perc - ((ld_perc * ld_sconti[ll_y]) / 100)
		next

		ld_sconto = 100 - ld_perc
		ld_valore_sconto = (ld_prezzo_vendita * ld_sconto) / 100
		
		//Calcolo del valore effettivo della riga = (prezzo-sconto tot)*qtà		
		ld_valore_riga = (ld_prezzo_vendita - ld_valore_sconto) * ld_quan_fatturata
		
		select cambio_ven
		into   :ld_cambio
		from   tab_valute
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_valuta = :ls_cod_valuta;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
			return -1
		end if
		
		//Conversione in euro di valore riga e prezzo vendita
		ld_valore_riga = round( ld_valore_riga * ld_cambio , 2 )
		ld_prezzo_vendita = round( ld_prezzo_vendita * ld_cambio , 2 )

		//Inserimento campi nel report di visualizzazione e stampa
		ll_riga = dw_report.insertrow(0)
		
		dw_report.setitem(ll_riga, "da_data_fattura", ldt_da_data_fattura)
		dw_report.setitem(ll_riga, "a_data_fattura", ldt_a_data_fattura)
		dw_report.setitem(ll_riga, "anno_documento", ll_anno_documento)
		if ls_flag_contabilita = "S" then ls_flag_contabilita = "Contabilizzate"
		if ls_flag_contabilita = "N" then ls_flag_contabilita = "Non Contabilizzate"
		if ls_flag_contabilita = "T" then ls_flag_contabilita = "Tutte"
		dw_report.setitem(ll_riga, "flag_contabilita",ls_flag_contabilita)
		dw_report.setitem(ll_riga, "da_num_documento", ll_da_num_documento)
		dw_report.setitem(ll_riga, "a_num_documento", ll_a_num_documento)
		dw_report.setitem(ll_riga, "da_cod_documento", ls_da_cod_documento)
		dw_report.setitem(ll_riga, "a_cod_documento", ls_a_cod_documento)
		dw_report.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
		dw_report.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		dw_report.setitem(ll_riga, "cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
		dw_report.setitem(ll_riga, "des_tipo_fat_ven", ls_des_tipo_fat_ven)
		dw_report.setitem(ll_riga, "cod_agente_1", ls_cod_agente_1)
		dw_report.setitem(ll_riga, "rag_soc_agente", ls_rag_soc_agente)
						
		dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
		//claudia18/04/07
		if not isnull(ls_des_prodotto) then	
			dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
		else
			select des_tipo_det_ven
			into   :ls_nota_dettaglio
			from   tab_tipi_det_ven
			where tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			dw_report.setitem(ll_riga, "des_prodotto", ls_nota_dettaglio)
			
		end if
		dw_report.setitem(ll_riga, "cod_misura", ls_cod_misura)
		dw_report.setitem(ll_riga, "quan_fatturata", ld_quan_fatturata)				
//------------------------ modifica Nicola ------------------------
		ls_where_tipo_fat = "cod_tipo_fat_ven = '" +  ls_cod_tipo_fat_ven_sel + "'"

		if f_flag_tipo_fatt("tab_tipi_fat_ven", ls_where_tipo_fat, "flag_tipo_fat_ven") = "N" then
			ld_prezzo_vendita = ld_prezzo_vendita * (-1)
			ld_valore_riga = ld_valore_riga * (-1)
		end if	
//------------------------ fine modifica --------------------------		
		dw_report.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
		dw_report.setitem(ll_riga, "sconto_tot", ld_valore_sconto)
		dw_report.setitem(ll_riga, "val_riga", ld_valore_riga)
		dw_report.setitem(ll_riga, "cod_iva", ls_cod_iva)
		dw_report.setitem(ll_riga, "provvigione_1", ld_provvigione_1)
		dw_report.setitem(ll_riga, "provvigione_2", ld_provvigione_2)
		dw_report.setitem(ll_riga, "num_registrazione_mov_mag", ll_num_registrazione_mov_mag)
		dw_report.setitem(ll_riga, "tes_fat_ven_cod_tipo_listino", ls_cod_tipo_listino_prodotto)
		dw_report.setitem(ll_riga, "tes_fat_ven_cod_valuta", ls_cod_valuta)
		dw_report.setitem(ll_riga, "tes_fat_ven_data_registrazione", ldt_data_registrazione)
		dw_report.setitem(ll_riga, "tes_fat_ven_cod_cliente", ls_cod_cliente)
		dw_report.setitem(ll_riga, "det_fat_ven_anno_reg_bol_ven", ll_anno_reg_bol_ven)
		dw_report.setitem(ll_riga, "det_fat_ven_num_reg_bol_ven", ll_num_registrazione_bol_ven)
		dw_report.setitem(ll_riga, "det_fat_ven_anno_reg_ord_ven", ll_anno_reg_ord_ven)
		dw_report.setitem(ll_riga, "det_fat_ven_num_reg_ord_ven", ll_num_reg_ord_ven)
		dw_report.setitem(ll_riga, "num_registrazione_mov_mag", ll_num_registrazione_mov_mag)
		dw_report.setitem(ll_riga, "tes_fat_ven_numeratore_doc", ls_numeratore_doc)
//		dw_report.setitem(ll_riga, "det_fat_ven_maggiorazione", ld_num_registrazione_mov_mag)
		dw_report.setitem(ll_riga, "tes_fat_ven_anno_registrazione", ll_anno_registrazione)
		dw_report.setitem(ll_riga, "tes_fat_ven_num_registrazione", ll_num_registrazione)

// f_leggi_maggiorazione( tes_fat_ven_cod_tipo_listino, tes_fat_ven_cod_valuta, tes_fat_ven_data_registrazione, tes_fat_ven_cod_cliente, cod_prodotto, quan_fatturata, det_fat_ven_anno_reg_bol_ven, det_fat_ven_num_reg_bol_ven, det_fat_ven_anno_reg_ord_ven, det_fat_ven_num_reg_ord_ven)				
				
	loop
	close cu_det;

loop
close cu_tes;

dw_report.setsort("tes_fat_ven_anno_registrazione A, tes_fat_ven_num_registrazione A")

dw_report.sort()

dw_report.groupcalc()

dw_report.setredraw( true)

return 0
end function

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.ib_dw_report=true

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_selezione.setitem(1,"flag_contabilita","T")

iuo_dw_main = dw_report


this.x = 641
this.y = 485
this.width = 2500
this.height = 930



end event

on w_sel_report_righe_fat_euro.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_sel_report_righe_fat_euro.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_fat_ven",sqlca,&
                 "tab_tipi_fat_ven","cod_tipo_fat_ven","des_tipo_fat_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_annulla from commandbutton within w_sel_report_righe_fat_euro
integer x = 1691
integer y = 720
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_sel_report_righe_fat_euro
integer x = 2080
integer y = 720
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_selezione.accepttext()

dw_selezione.hide()

//cb_ricerca_cliente.hide()

//cb_ricerca_prodotto.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()

wf_righe_fattura()			//funzione per selezionare il listino di vendita


cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_sel_report_righe_fat_euro
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;
dw_selezione.show()

//cb_ricerca_cliente.show()

//cb_ricerca_prodotto.show()

parent.x = 641
parent.y = 485
parent.width = 2500
parent.height = 930

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_sel_report_righe_fat_euro
integer x = 23
integer y = 20
integer width = 2469
integer height = 680
integer taborder = 20
string dataobject = "d_sel_report_righe_fat"
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_report from uo_cs_xx_dw within w_sel_report_righe_fat_euro
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_righe_fattura_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

