﻿$PBExportHeader$w_sel_report_tot_fatt_euro.srw
$PBExportComments$W selezione report totali di fatturazione
forward
global type w_sel_report_tot_fatt_euro from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_sel_report_tot_fatt_euro
end type
type cb_report from commandbutton within w_sel_report_tot_fatt_euro
end type
type dw_selezione from uo_cs_xx_dw within w_sel_report_tot_fatt_euro
end type
type dw_report from uo_cs_xx_dw within w_sel_report_tot_fatt_euro
end type
type dw_folder from u_folder within w_sel_report_tot_fatt_euro
end type
end forward

global type w_sel_report_tot_fatt_euro from w_cs_xx_principale
integer width = 3840
integer height = 2228
string title = "Stampa Totali Fatturazione"
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_sel_report_tot_fatt_euro w_sel_report_tot_fatt_euro

forward prototypes
public function integer wf_tot_fatturazione ()
end prototypes

public function integer wf_tot_fatturazione ();string 	ls_cod_cliente, ls_rag_soc_1, ls_cod_agente_1, ls_rag_soc_agente_1, ls_cod_agente_2, ls_rag_soc_agente_2, &
		 	ls_cod_tipo_fat_ven, ls_des_tipo_fat_ven, ls_cod_zona, ls_des_zona, ls_cod_valuta, ls_des_valuta, &
		 	ls_cod_pagamento, ls_des_pagamento, ls_sql, ls_where, ls_stringa_lire, ls_stringa_euro, ls_dicitura_std
		 
long     ll_riga

double   ld_da_num_documento, ld_a_num_documento, ld_anno_documento, ld_anno_fattura, ld_num_documento, ld_cambio_ven

dec{4}   ld_imponibile_euro, ld_importo_euro, ld_totale_euro, ld_imponibile_valuta, ld_importo_valuta,&
         ld_totale_valuta

datetime ldt_da_data_fattura,ldt_a_data_fattura, ldt_data_fattura


dw_report.reset()

//Assegnazione variabili selezione
ld_anno_documento = dw_selezione.getitemnumber(1,"anno_documento")
ld_da_num_documento = dw_selezione.getitemnumber(1,"da_num_documento")
ld_a_num_documento = dw_selezione.getitemnumber(1,"a_num_documento")
ldt_da_data_fattura = datetime(dw_selezione.getitemdate(1,"da_data_fattura"))
ldt_a_data_fattura = datetime(dw_selezione.getitemdate(1,"a_data_fattura"))
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_agente_1 = dw_selezione.getitemstring(1,"cod_agente_1")
ls_cod_agente_2 = dw_selezione.getitemstring(1,"cod_agente_2")
ls_cod_tipo_fat_ven = dw_selezione.getitemstring(1,"cod_tipo_fat_ven")
ls_cod_zona = dw_selezione.getitemstring(1,"cod_zona")
ls_cod_valuta = dw_selezione.getitemstring(1,"cod_valuta")
ls_cod_pagamento = dw_selezione.getitemstring(1,"cod_pagamento")


/////////////////////Estrazione dati ed assegnazione alla DW del report///////////////////


//Creazione WHERE dinamico
ls_where = " where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "'"

if not(isnull(ld_anno_documento)) then
	ls_where = ls_where + " and tes_fat_ven.anno_documento = " + string(ld_anno_documento) + " "
end if

if not (isnull(ld_da_num_documento)) and ld_da_num_documento > 0 then
	ls_where = ls_where + " and tes_fat_ven.num_documento >= " + string(ld_da_num_documento) + " "
end if

if not (isnull(ld_a_num_documento)) and ld_a_num_documento > 0 then
	ls_where = ls_where + " and tes_fat_ven.num_documento <= " + string(ld_a_num_documento) + " "
end if

if not (isnull(ls_cod_zona)) and ls_cod_zona <> "" then
	ls_where += " AND tes_fat_ven.cod_cliente IN ( SELECT cod_cliente FROM anag_clienti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_zona = '" + ls_cod_zona + "' ) "
end if

if not (isnull(ls_cod_valuta)) and ls_cod_valuta <> "" then
	ls_where = ls_where + " and tes_fat_ven.cod_valuta = '" + ls_cod_valuta + "'"
end if

if not (isnull(ls_cod_pagamento)) and ls_cod_pagamento <> "" then
	ls_where = ls_where + " and tes_fat_ven.cod_pagamento = '" + ls_cod_pagamento + "'"
end if

if not isnull( ldt_da_data_fattura) then
	ls_where += " AND tes_fat_ven.data_fattura >= '" + string(ldt_da_data_fattura, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull( ldt_a_data_fattura) then
	ls_where += " AND tes_fat_ven.data_fattura <= '" + string(ldt_a_data_fattura, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not (isnull(ls_cod_cliente)) and ls_cod_cliente <> "" then
	ls_where = ls_where + " and tes_fat_ven.cod_cliente = '" + ls_cod_cliente + "'"
end if

if not (isnull(ls_cod_agente_1)) and ls_cod_agente_1 <> "" then
	ls_where = ls_where + " and tes_fat_ven.cod_agente_1 = '" + ls_cod_agente_1 + "'"
end if

if not (isnull(ls_cod_agente_2)) and ls_cod_agente_2 <> "" then
	ls_where = ls_where + " and tes_fat_ven.cod_agente_2 = '" + ls_cod_agente_2 + "'"
end if

if not (isnull(ls_cod_tipo_fat_ven)) and ls_cod_tipo_fat_ven <> "" then
	ls_where = ls_where + " and tes_fat_ven.cod_tipo_fat_ven = '" + ls_cod_tipo_fat_ven + "'"
end if


//Selezione della descrizione valuta in LIRE
select parametri_azienda.stringa  
into  :ls_stringa_lire  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
		parametri_azienda.flag_parametro = 'S' and  
		parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_lire)
end if

//Selezione della descrizione valuta in EURO
select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
		parametri_azienda.flag_parametro = 'S' and  
		parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if
	

//Cursore sulla testata delle fatture
declare cu_tes dynamic cursor for sqlsa;

ls_sql = "select tes_fat_ven.imponibile_iva_valuta,  " + & 
			"		  tes_fat_ven.importo_iva_valuta, " + & 
			"		  tes_fat_ven.imponibile_iva,  " + & 
			"		  tes_fat_ven.importo_iva,  " + & 
			"		  tes_fat_ven.tot_fattura," + & 
			"		  tes_fat_ven.tot_fattura_valuta,  " + & 
			"		  tes_fat_ven.cod_valuta,  " + & 
			"		  tes_fat_ven.data_fattura," + &
			"		  tes_fat_ven.anno_documento,  " + & 
			"		  tes_fat_ven.num_documento,  " + & 
			"		  tes_fat_ven.cod_cliente, " + &
			"		  tes_fat_ven.cod_agente_1,  " + & 
			"		  tes_fat_ven.cod_agente_2,  " + & 
			"		  tes_fat_ven.cod_pagamento," + &
			"		  tes_fat_ven.cod_tipo_fat_ven " + &
			" from  tes_fat_ven " + &
"" + ls_where + " order by tes_fat_ven.data_fattura"

prepare sqlsa from :ls_sql;

open dynamic cu_tes;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "APICE", "Errore durante l'apertura del cursore cu_tes:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	return -1
end if

do while true
	
   fetch cu_tes
	into  :ld_imponibile_valuta,
			:ld_importo_valuta,			
			:ld_imponibile_euro,
			:ld_importo_euro,
			:ld_totale_euro,
			:ld_totale_valuta,
			:ls_cod_valuta,
			:ldt_data_fattura,
			:ld_anno_fattura,
			:ld_num_documento,
			:ls_cod_cliente,
			:ls_cod_agente_1,
			:ls_cod_agente_2,
			:ls_cod_pagamento,
			:ls_cod_tipo_fat_ven;
	
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		
	//Estrarre la descrizione dell'agente_1
	select rag_soc_1
	into  :ls_rag_soc_agente_1
	from  anag_agenti  
	where cod_azienda = :s_cs_xx.cod_azienda and  
			cod_agente = :ls_cod_agente_1;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_rag_soc_agente_1)
	end if
	
	//Estrarre la descrizione dell'agente_2
	select rag_soc_1
	into  :ls_rag_soc_agente_2
	from  anag_agenti  
	where cod_azienda = :s_cs_xx.cod_azienda and  
			cod_agente = :ls_cod_agente_2;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_rag_soc_agente_2)
	end if
	
	//Estrarre la descrizione del tipo_fattura
	select des_tipo_fat_ven
	into  :ls_des_tipo_fat_ven
	from  tab_tipi_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and  
			cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_des_tipo_fat_ven)
	end if
	
	//Estrarre la descrizione del cliente
	select rag_soc_1, cod_zona
	into  :ls_rag_soc_1, :ls_cod_zona
	from  anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and  
			cod_cliente = :ls_cod_cliente;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_rag_soc_1)
		setnull(ls_cod_zona)
	end if
	
	//Estrarre la descrizione della zona
	select des_zona
	into  :ls_des_zona
	from  tab_zone
	where cod_azienda = :s_cs_xx.cod_azienda and  
			cod_zona = :ls_cod_zona;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_des_zona)
	end if
	
	//Estrarre la descrizione del pagamento
	select des_pagamento
	into  :ls_des_pagamento
	from  tab_pagamenti
	where cod_azienda = :s_cs_xx.cod_azienda and  
			cod_pagamento = :ls_cod_pagamento;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_des_pagamento)
	end if
	
	//Estrarre la descrizione della valuta
	select des_valuta
	into  :ls_des_valuta
	from  tab_valute
	where cod_azienda = :s_cs_xx.cod_azienda and  
			cod_valuta = :ls_cod_valuta;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_des_valuta)
	end if
	
	//Inserimento campi nel report di visualizzazione e stampa
	ll_riga = dw_report.insertrow(0)
	
	dw_report.setitem(ll_riga, "data_fattura", ldt_data_fattura)
	dw_report.setitem(ll_riga, "anno_documento", ld_anno_fattura)
	dw_report.setitem(ll_riga, "num_documento", ld_num_documento)
	dw_report.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
	dw_report.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
	dw_report.setitem(ll_riga, "cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	dw_report.setitem(ll_riga, "des_tipo_fat_ven", ls_des_tipo_fat_ven)
	dw_report.setitem(ll_riga, "cod_agente_1", ls_cod_agente_1)
	dw_report.setitem(ll_riga, "rag_soc_agente_1", ls_rag_soc_agente_1)
	dw_report.setitem(ll_riga, "cod_agente_2", ls_cod_agente_2)
	dw_report.setitem(ll_riga, "rag_soc_agente_2", ls_rag_soc_agente_2)
	dw_report.setitem(ll_riga, "cod_valuta", ls_cod_valuta)
	dw_report.setitem(ll_riga, "des_valuta", ls_des_valuta)
	dw_report.setitem(ll_riga, "cod_pagamento", ls_cod_pagamento)
	dw_report.setitem(ll_riga, "des_pagamento", ls_des_pagamento)
	dw_report.setitem(ll_riga, "cod_zona", ls_cod_zona)
	dw_report.setitem(ll_riga, "des_zona", ls_des_zona)
			
	dw_report.setitem(ll_riga, "impon_euro", ld_imponibile_euro)
	dw_report.setitem(ll_riga, "impon_valuta", ld_imponibile_valuta)			
	dw_report.setitem(ll_riga, "imposta_euro", ld_importo_euro)
	dw_report.setitem(ll_riga, "imposta_valuta", ld_importo_valuta)			
	dw_report.setitem(ll_riga, "totale_euro", ld_totale_euro)
	dw_report.setitem(ll_riga, "totale_valuta", ld_totale_valuta)			
	
loop
close cu_tes;

commit;
return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.ib_dw_report = true

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_annulla
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)
end event

on w_sel_report_tot_fatt_euro.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_sel_report_tot_fatt_euro.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_agente_1",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_agente_2",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_fat_ven",sqlca,&
                 "tab_tipi_fat_ven","cod_tipo_fat_ven","des_tipo_fat_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_zona",sqlca,&
                 "tab_zone","cod_zona","des_zona",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_valuta",sqlca,&
                 "tab_valute","cod_valuta","des_valuta",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_pagamento",sqlca,&
                 "tab_pagamenti","cod_pagamento","des_pagamento",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_annulla from commandbutton within w_sel_report_tot_fatt_euro
integer x = 1463
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string	ls_null
date     ldt_null

setnull( ls_null)
setnull( ldt_null)

dw_selezione.setitem( dw_selezione.getrow(), "anno_documento", year(today()))
dw_selezione.setitem( dw_selezione.getrow(), "da_num_documento", 0)
dw_selezione.setitem( dw_selezione.getrow(), "a_num_documento", 0)
dw_selezione.setitem( dw_selezione.getrow(), "da_data_fattura", ldt_null)
dw_selezione.setitem( dw_selezione.getrow(), "a_data_fattura", ldt_null)
dw_selezione.setitem( dw_selezione.getrow(), "cod_cliente", ls_null)
dw_selezione.setitem( dw_selezione.getrow(), "cod_agente_1", ls_null)
dw_selezione.setitem( dw_selezione.getrow(), "cod_agente_2", ls_null)
dw_selezione.setitem( dw_selezione.getrow(), "cod_tipo_fat_ven", ls_null)
dw_selezione.setitem( dw_selezione.getrow(), "cod_zona", ls_null)
dw_selezione.setitem( dw_selezione.getrow(), "cod_valuta", ls_null)
dw_selezione.setitem( dw_selezione.getrow(), "cod_pagamento", ls_null)
end event

type cb_report from commandbutton within w_sel_report_tot_fatt_euro
integer x = 1851
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_selezione.accepttext()
dw_selezione.hide()

wf_tot_fatturazione()			//funzione per selezionare il listino di vendita

dw_report.change_dw_current()

dw_folder.fu_selecttab(2)

dw_report.change_dw_current()
end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_selezione from uo_cs_xx_dw within w_sel_report_tot_fatt_euro
integer x = 46
integer y = 160
integer width = 2217
integer height = 840
integer taborder = 20
string dataobject = "d_sel_report_tot_fatturazione"
boolean border = false
end type

event pcd_new;call super::pcd_new;setitem( getrow(), "anno_documento", year(today()))
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type dw_report from uo_cs_xx_dw within w_sel_report_tot_fatt_euro
boolean visible = false
integer x = 46
integer y = 160
integer width = 3680
integer height = 1900
integer taborder = 10
string dataobject = "d_report_tot_fatturazione_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_sel_report_tot_fatt_euro
integer x = 23
integer y = 20
integer width = 3730
integer height = 2072
integer taborder = 60
boolean border = false
end type

