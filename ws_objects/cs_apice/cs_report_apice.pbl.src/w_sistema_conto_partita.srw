﻿$PBExportHeader$w_sistema_conto_partita.srw
forward
global type w_sistema_conto_partita from window
end type
type cb_4 from commandbutton within w_sistema_conto_partita
end type
type cb_3 from commandbutton within w_sistema_conto_partita
end type
type sle_3 from singlelineedit within w_sistema_conto_partita
end type
type st_3 from statictext within w_sistema_conto_partita
end type
type sle_2 from singlelineedit within w_sistema_conto_partita
end type
type st_2 from statictext within w_sistema_conto_partita
end type
type sle_1 from singlelineedit within w_sistema_conto_partita
end type
type st_1 from statictext within w_sistema_conto_partita
end type
type cb_2 from commandbutton within w_sistema_conto_partita
end type
type dw_2 from datawindow within w_sistema_conto_partita
end type
type cb_1 from commandbutton within w_sistema_conto_partita
end type
type dw_1 from datawindow within w_sistema_conto_partita
end type
end forward

global type w_sistema_conto_partita from window
integer width = 3246
integer height = 960
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cb_4 cb_4
cb_3 cb_3
sle_3 sle_3
st_3 st_3
sle_2 sle_2
st_2 st_2
sle_1 sle_1
st_1 st_1
cb_2 cb_2
dw_2 dw_2
cb_1 cb_1
dw_1 dw_1
end type
global w_sistema_conto_partita w_sistema_conto_partita

event open;dw_1.settransobject(sqlci)

dw_2.settransobject(sqlci)

end event

on w_sistema_conto_partita.create
this.cb_4=create cb_4
this.cb_3=create cb_3
this.sle_3=create sle_3
this.st_3=create st_3
this.sle_2=create sle_2
this.st_2=create st_2
this.sle_1=create sle_1
this.st_1=create st_1
this.cb_2=create cb_2
this.dw_2=create dw_2
this.cb_1=create cb_1
this.dw_1=create dw_1
this.Control[]={this.cb_4,&
this.cb_3,&
this.sle_3,&
this.st_3,&
this.sle_2,&
this.st_2,&
this.sle_1,&
this.st_1,&
this.cb_2,&
this.dw_2,&
this.cb_1,&
this.dw_1}
end on

on w_sistema_conto_partita.destroy
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.sle_3)
destroy(this.st_3)
destroy(this.sle_2)
destroy(this.st_2)
destroy(this.sle_1)
destroy(this.st_1)
destroy(this.cb_2)
destroy(this.dw_2)
destroy(this.cb_1)
destroy(this.dw_1)
end on

type cb_4 from commandbutton within w_sistema_conto_partita
integer x = 1783
integer y = 460
integer width = 480
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CARICA REG"
end type

event clicked;long ll_righe, ll_i, ll_id_reg_pd, ll_id_conto, ll_id_conto_iva, ll_id_prof_nc
decimal ld_importo

if isnull(sle_1.text) or len(sle_1.text) < 1 then
	g_mb.messagebox("","Specificare ID CONTO IVA")
	return
end if
if isnull(sle_3.text) or len(sle_3.text) < 1 then
	g_mb.messagebox("","Specificare ID PROFILO FT")
	return
end if
setnull(ll_id_conto_iva)
ll_id_conto_iva = long(sle_1.text)
if isnull(ll_id_conto_iva) or ll_id_conto_iva < 1 then
	g_mb.messagebox("","conto iva non valido " + string(ll_id_conto_iva) )
	return
end if
setnull(ll_id_prof_nc)
ll_id_prof_nc = long(sle_3.text)
if isnull(ll_id_prof_nc) or ll_id_prof_nc < 1 then
	g_mb.messagebox("","PROFILO NC NON VALIDO " + string(ll_id_prof_nc) )
	return
end if

dw_2.retrieve(ll_id_conto_iva, ll_id_prof_nc)

end event

type cb_3 from commandbutton within w_sistema_conto_partita
integer x = 823
integer y = 460
integer width = 480
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CARICA REG"
end type

event clicked;long ll_righe, ll_i, ll_id_reg_pd, ll_id_conto, ll_id_conto_iva, ll_id_prof_ft
decimal ld_importo

if isnull(sle_1.text) or len(sle_1.text) < 1 then
	g_mb.messagebox("","Specificare ID CONTO IVA")
	return
end if
if isnull(sle_2.text) or len(sle_2.text) < 1 then
	g_mb.messagebox("","Specificare ID PROFILO FT")
	return
end if

setnull(ll_id_conto_iva)
ll_id_conto_iva = long(sle_1.text)
if isnull(ll_id_conto_iva) or ll_id_conto_iva < 1 then
	g_mb.messagebox("","conto iva non valido " + string(ll_id_conto_iva) )
	return
end if
setnull(ll_id_prof_ft)
ll_id_prof_ft = long(sle_2.text)
if isnull(ll_id_prof_ft) or ll_id_prof_ft < 1 then
	g_mb.messagebox("","PROFILO FT NON VALIDO " + string(ll_id_prof_ft) )
	return
end if

dw_1.retrieve(ll_id_conto_iva, ll_id_prof_ft)

end event

type sle_3 from singlelineedit within w_sistema_conto_partita
integer x = 1463
integer y = 260
integer width = 402
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_sistema_conto_partita
integer x = 1029
integer y = 280
integer width = 407
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "ID PROFILO NC:"
boolean focusrectangle = false
end type

type sle_2 from singlelineedit within w_sistema_conto_partita
integer x = 1463
integer y = 140
integer width = 402
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_sistema_conto_partita
integer x = 1029
integer y = 160
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "ID PROFILO FT:"
boolean focusrectangle = false
end type

type sle_1 from singlelineedit within w_sistema_conto_partita
integer x = 1463
integer y = 20
integer width = 402
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_sistema_conto_partita
integer x = 1029
integer y = 40
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "ID CONTO IVA:"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_sistema_conto_partita
integer x = 1783
integer y = 620
integer width = 480
integer height = 140
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "NC"
end type

event clicked;long ll_righe, ll_i, ll_id_reg_pd, ll_id_conto, ll_id_conto_iva, ll_id_prof_nc
decimal ld_importo

if isnull(sle_1.text) or len(sle_1.text) < 1 then
	g_mb.messagebox("","Specificare ID CONTO IVA")
	return
end if
if isnull(sle_3.text) or len(sle_3.text) < 1 then
	g_mb.messagebox("","Specificare ID PROFILO FT")
	return
end if
setnull(ll_id_conto_iva)
ll_id_conto_iva = long(sle_1.text)
if isnull(ll_id_conto_iva) or ll_id_conto_iva < 1 then
	g_mb.messagebox("","conto iva non valido " + string(ll_id_conto_iva) )
	return
end if
setnull(ll_id_prof_nc)
ll_id_prof_nc = long(sle_3.text)
if isnull(ll_id_prof_nc) or ll_id_prof_nc < 1 then
	g_mb.messagebox("","PROFILO NC NON VALIDO " + string(ll_id_prof_nc) )
	return
end if

dw_2.retrieve(34, 72)
ll_righe = dw_2.rowcount()
for ll_i = 1 to ll_righe
	ll_id_reg_pd = dw_2.getitemnumber(ll_i, "id_reg_pd")
	ll_id_reg_pd = dw_2.getitemnumber(ll_i, "id_reg_pd")
	
	select mov_contabile.id_conto,mov_contabile.dare 
	into :ll_id_conto, :ld_importo
  FROM {oj "mov_contabile"  LEFT OUTER JOIN "reg_pd"  ON "mov_contabile"."id_reg_pd" = "reg_pd"."id_reg_pd"}  
 where ((mov_contabile.avere is not null) or (mov_contabile.avere  > 0)) and
		 reg_pd.id_prof_registrazione = :ll_id_prof_nc and
		 reg_pd.id_conto = :ll_id_conto_iva and
		 reg_pd.id_reg_pd = :ll_id_reg_pd
	using sqlci;
	if sqlci.sqlcode <> 0 then
		g_mb.messagebox("Errore select", sqlci.sqlerrtext)
	end if
	
	
	update reg_pd
	set id_conto = :ll_id_conto, importo = :ld_importo
	where id_reg_pd = :ll_id_reg_pd
	using sqlci;
	if sqlci.sqlcode <> 0 then
		g_mb.messagebox("Errore update", sqlci.sqlerrtext)
	end if
	commit using sqlci;
next
commit using sqlci;

dw_2.retrieve()
end event

type dw_2 from datawindow within w_sistema_conto_partita
integer x = 2423
integer y = 20
integer width = 754
integer height = 820
integer taborder = 10
string title = "none"
string dataobject = "d_sistema_conto_testata_nc"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_sistema_conto_partita
integer x = 823
integer y = 620
integer width = 480
integer height = 140
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "FATTURE"
end type

event clicked;long ll_righe, ll_i, ll_id_reg_pd, ll_id_conto, ll_id_conto_iva, ll_id_prof_ft
decimal ld_importo

if isnull(sle_1.text) or len(sle_1.text) < 1 then
	g_mb.messagebox("","Specificare ID CONTO IVA")
	return
end if
if isnull(sle_2.text) or len(sle_2.text) < 1 then
	g_mb.messagebox("","Specificare ID PROFILO FT")
	return
end if

setnull(ll_id_conto_iva)
ll_id_conto_iva = long(sle_1.text)
if isnull(ll_id_conto_iva) or ll_id_conto_iva < 1 then
	g_mb.messagebox("","conto iva non valido " + string(ll_id_conto_iva) )
	return
end if
setnull(ll_id_prof_ft)
ll_id_prof_ft = long(sle_2.text)
if isnull(ll_id_prof_ft) or ll_id_prof_ft < 1 then
	g_mb.messagebox("","PROFILO FT NON VALIDO " + string(ll_id_prof_ft) )
	return
end if

dw_1.retrieve(ll_id_conto_iva, ll_id_prof_ft)
ll_righe = dw_1.rowcount()
for ll_i = 1 to ll_righe
	ll_id_reg_pd = dw_1.getitemnumber(ll_i, "id_reg_pd")
	ll_id_reg_pd = dw_1.getitemnumber(ll_i, "id_reg_pd")
	
	select mov_contabile.id_conto,mov_contabile.dare 
	into :ll_id_conto, :ld_importo
  FROM {oj "mov_contabile"  LEFT OUTER JOIN "reg_pd"  ON "mov_contabile"."id_reg_pd" = "reg_pd"."id_reg_pd"}  
 where ((mov_contabile.dare is not null) or (mov_contabile.dare  > 0)) and
		 reg_pd.id_prof_registrazione = :ll_id_prof_ft and
		 reg_pd.id_conto = :ll_id_conto_iva and
		 reg_pd.id_reg_pd = :ll_id_reg_pd
	using sqlci;
	if sqlci.sqlcode <> 0 then
		g_mb.messagebox("Errore update", sqlci.sqlerrtext)
	end if
	
	
	update reg_pd
	set id_conto = :ll_id_conto, importo = :ld_importo
	where id_reg_pd = :ll_id_reg_pd
	using sqlci;
	if sqlci.sqlcode <> 0 then
		g_mb.messagebox("Errore update", sqlci.sqlerrtext)
	end if
	commit using sqlci;
next
commit using sqlci;

dw_1.retrieve()
end event

type dw_1 from datawindow within w_sistema_conto_partita
integer y = 20
integer width = 754
integer height = 820
integer taborder = 10
string title = "none"
string dataobject = "d_sistema_conto_testata"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

