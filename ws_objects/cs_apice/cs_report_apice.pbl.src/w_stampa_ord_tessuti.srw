﻿$PBExportHeader$w_stampa_ord_tessuti.srw
forward
global type w_stampa_ord_tessuti from w_cs_xx_principale
end type
type tab_1 from tab within w_stampa_ord_tessuti
end type
type page_1 from userobject within tab_1
end type
type dw_ricerca from datawindow within page_1
end type
type page_1 from userobject within tab_1
dw_ricerca dw_ricerca
end type
type page_2 from userobject within tab_1
end type
type cb_stampa_tessuti from commandbutton within page_2
end type
type dw_lista from datawindow within page_2
end type
type page_2 from userobject within tab_1
cb_stampa_tessuti cb_stampa_tessuti
dw_lista dw_lista
end type
type tab_1 from tab within w_stampa_ord_tessuti
page_1 page_1
page_2 page_2
end type
end forward

global type w_stampa_ord_tessuti from w_cs_xx_principale
integer width = 4731
integer height = 2216
string title = "Stampa Ordini Tessuti"
tab_1 tab_1
end type
global w_stampa_ord_tessuti w_stampa_ord_tessuti

type variables
constant long COLOR_TRASPARENT = 536870912
constant long COLOR_SUCCESS = 14155735
constant long COLOR_SKIPPED = 14286847
constant long COLOR_ERROR = 5592575




private:
	// Cartella dove salvare i file excel generati dalla funzione wf_invia_files
	string is_user_excel_folder
	
	// Cartella dove recupera il file di template per creare i file
	string is_path_excel_mag_tessuti
	
	// Cartella remota dove copiare i file excel
	string is_remote_folder_to_copy_excel
	
	// Lista dei file excel generato
	string is_files_generated[]
end variables

forward prototypes
public function integer wf_ricerca ()
public subroutine wf_seleziona (string as_column, string as_flag)
public subroutine wf_colora_riga (long al_row, long al_color)
public function boolean wf_precheck_risorse ()
public subroutine wf_crea_excel (s_invia_tessuti_righe astr_righe[])
public function integer wf_elabora_ordini_in_files ()
public subroutine wf_crea_ascii (s_invia_tessuti_righe astr_righe[])
public function integer wf_dim_calcolate (integer ai_anno, long al_numero, long al_prog_riga, ref decimal ad_dim_x, ref decimal ad_dim_y)
end prototypes

public function integer wf_ricerca ();/**
 * stefanop
 * 24/09/2014
 *
 * Esegue la ricerca con i parametri importati
 **/
 
string ls_cod_cliente_filtro, ls_sql, ls_error, ls_cod_cliente, ls_cod_tipo_ord_ven, ls_cod_operatore, ls_cod_deposito_origine, ls_operatore_ds, ls_des_cliente
int li_anno_ordine_filtro
long ll_num_ordine_filtro, ll_rows, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_row
datetime ldt_da_data, ldt_a_data, ldt_da_data_reg, ldt_a_data_reg, ldt_data_consegna, ldt_data_registrazione
datastore lds_store

setpointer(Hourglass!)

tab_1.page_2.enabled = false

// Leggo filtri
tab_1.page_1.dw_ricerca.accepttext()
li_anno_ordine_filtro = tab_1.page_1.dw_ricerca.getitemnumber(1,"anno_ordine")
ll_num_ordine_filtro = tab_1.page_1.dw_ricerca.getitemnumber(1,"num_ordine")
ldt_da_data =  tab_1.page_1.dw_ricerca.getitemdatetime(1,"data_consegna_inizio")
ldt_a_data =  tab_1.page_1.dw_ricerca.getitemdatetime(1,"data_consegna_fine")
ldt_da_data_reg = tab_1.page_1.dw_ricerca.getitemdatetime(1,"data_registrazione_inizio")
ldt_a_data_reg = tab_1.page_1.dw_ricerca.getitemdatetime(1,"data_registrazione_fine")
ls_cod_cliente_filtro = tab_1.page_1.dw_ricerca.getitemstring(1,"cod_cliente")

// Preparo la ricerca
ls_sql = "select tes_ord_ven.anno_registrazione, " & 
			+ "tes_ord_ven.num_registrazione, " &
			+ "tes_ord_ven.data_consegna, " &
			+ "tes_ord_ven.data_registrazione, " & 
			+ "tes_ord_ven.cod_cliente, " &
			+ "anag_clienti.rag_soc_1, " &
			+ "tes_ord_ven.cod_tipo_ord_ven " &
		+ "from tes_ord_ven " &
		+ "join anag_clienti on anag_clienti.cod_azienda=tes_ord_ven.cod_azienda and anag_clienti.cod_cliente=tes_ord_ven.cod_cliente " &
		+ "where tes_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' and " &
			+ "tes_ord_ven.flag_evasione<>'E' and " &
			+ "tes_ord_ven.data_registrazione>='" + string(ldt_da_data_reg, s_cs_xx.db_funzioni.formato_data) + "' and " &
			+ "tes_ord_ven.data_registrazione<='" + string(ldt_a_data_reg, s_cs_xx.db_funzioni.formato_data) + "' " &

if not isnull(ldt_da_data) then
	ls_sql += " and tes_ord_ven.data_consegna>='" + string(ldt_da_data, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_da_data) then
	ls_sql += " and tes_ord_ven.data_consegna<='" + string(ldt_a_data, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if li_anno_ordine_filtro>0 then
	ls_sql += " and tes_ord_ven.anno_registrazione="+string(li_anno_ordine_filtro)
end if
if ll_num_ordine_filtro>0 then
	ls_sql += " and tes_ord_ven.num_registrazione="+string(ll_num_ordine_filtro)
end if

ls_sql += " order by tes_ord_ven.anno_registrazione asc, tes_ord_ven.num_registrazione asc"

// Eseguo ricerca
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
if ll_rows < 0 then
	g_mb.error(ls_error)
	destroy lds_store
	return -1
end if

for ll_i = 1 to ll_rows
	ll_anno_registrazione = lds_store.getitemnumber(ll_i, 1)
	ll_num_registrazione = lds_store.getitemnumber(ll_i, 2)
	ldt_data_consegna = lds_store.getitemdatetime(ll_i, 3)
	ldt_data_registrazione = lds_store.getitemdatetime(ll_i, 4)
	ls_cod_cliente = lds_store.getitemstring(ll_i, 5)
	ls_des_cliente = lds_store.getitemstring(ll_i, 6)
	ls_cod_tipo_ord_ven = lds_store.getitemstring(ll_i, 7)
	
	pcca.mdi_frame.setmicrohelp("Elaboraz. Ordine " + string(ll_anno_registrazione)+"/"+string(ll_num_registrazione) + " - " + string(ll_i) + " di " + string(ll_rows))
	Yield()
	
	ll_row = tab_1.page_2.dw_lista.insertrow(0)
	tab_1.page_2.dw_lista.setitem(ll_row, "anno_registrazione", ll_anno_registrazione)
	tab_1.page_2.dw_lista.setitem(ll_row, "num_registrazione", ll_num_registrazione)
	tab_1.page_2.dw_lista.setitem(ll_row, "cod_cliente", ls_cod_cliente)
	tab_1.page_2.dw_lista.setitem(ll_row, "des_cliente", ls_des_cliente)
	tab_1.page_2.dw_lista.setitem(ll_row, "cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
	tab_1.page_2.dw_lista.setitem(ll_row, "data_consegna", ldt_data_consegna)
	tab_1.page_2.dw_lista.setitem(ll_row, "data_registrazione", ldt_data_registrazione)
	
next

pcca.mdi_frame.setmicrohelp("Pronto")
Yield()
	
// Cambio i tab
tab_1.page_2.enabled = true
tab_1.selecttab(2)

return 0
end function

public subroutine wf_seleziona (string as_column, string as_flag);/**
 * imposta il valore in tutte
 * le righe
 **/

long ll_i

for ll_i = 1 to tab_1.page_2.dw_lista.rowcount()
	tab_1.page_2.dw_lista.setitem(ll_i, as_column, as_flag)
next
end subroutine

public subroutine wf_colora_riga (long al_row, long al_color);tab_1.page_2.dw_lista.setitem(al_row, "color", al_color)
end subroutine

public function boolean wf_precheck_risorse ();/**
 * stefanop
 * 25/09/2014
 *
 * Controllo che ci siano tutte le risorse impostate
 * prima di procedere con l'invio
 *
 * Ritorna: true se tutto ok, false se error
 **/
 
// 1. Controllo file di template

//is_path_excel_mag_tessuti = s_cs_xx.volume + s_cs_xx.risorse + "mag_tessuti\import.xls"
//if not fileexists(is_path_excel_mag_tessuti) then
//	g_mb.warning("Attenzione: Manca il file 'import.xls' nella cartella 'mag_tessuti' nelle risorse")
//	return false
//end if

// 2. Cartella temporanei
if g_str.isempty(is_user_excel_folder) then
	
	is_user_excel_folder = guo_functions.uof_get_user_documents_folder() + "csteam_temp_excel\"
	
	if not directoryexists(is_user_excel_folder) then
		if createdirectory(is_user_excel_folder) < 0 then
			g_mb.warning("Attenzione: Errore durante la creazione della cartella temporanea per i file excel nella cartella dell'utente.")
			return false
		end if
	end if
end if

// 3. Cartella remota dove copiare i file
guo_functions.uof_get_parametro_azienda("RFM", is_remote_folder_to_copy_excel)
if g_str.isempty(is_remote_folder_to_copy_excel) then
	g_mb.warning("Attenzione: Non è impostata la cartella remota dove copiare i file excel.~r~nImpostare il parametro aziendale RFM.")
	return false
end if

return true
end function

public subroutine wf_crea_excel (s_invia_tessuti_righe astr_righe[]);/**
 * stefanop
 * 25/06/2014
 *
 * Elaboro le righe dei tessuti e preparo il foglio excel
 * In caso di errore lancio l'eccezione che viene recuperata
 * dalla funzione chiamante (wf_invia_files)
 **/
 
string ls_file_name, ls_ordine
int li_i, li_count, li_riga_excel
uo_excel luo_excel
RuntimeError ex

li_count = upperbound(astr_righe)
if li_count < 1 then return


ex = create RuntimeError

try
	luo_excel = create uo_excel
	
	ls_file_name = astr_righe[1].cod_tessuto + "-" + astr_righe[1].cod_colore
	ls_file_name = guo_functions.uof_sanatize_filename(ls_file_name)
	ls_file_name = is_user_excel_folder + ls_file_name + ".xls"
	
	if FileCopy(is_path_excel_mag_tessuti, ls_file_name, true) < 0 then
		ex.setmessage("Errore durante la copia del file excel dal percorso: " + g_str.safe(is_path_excel_mag_tessuti) + " al percorso: " + g_str.safe(ls_file_name))
		throw ex
	end if
	
	luo_excel.uof_open(ls_file_name  , false)
	li_riga_excel = 2
	
	for li_i = 1 to upperbound(astr_righe)
		li_riga_excel++
		
		ls_ordine = string(astr_righe[li_i].anno_registrazione) + "/" + string(astr_righe[li_i].num_registrazione) + "/" + string(astr_righe[li_i].prog_riga_ord_ven)
		
		luo_excel.uof_set(li_riga_excel, "A", li_i)
		luo_excel.uof_set(li_riga_excel, "B", astr_righe[li_i].quantita)			// Quantità
		luo_excel.uof_set(li_riga_excel, "C", astr_righe[li_i].cod_tessuto)		// tessuto
		luo_excel.uof_set(li_riga_excel, "I", "mm")									// UM
		luo_excel.uof_set(li_riga_excel, "J", astr_righe[li_i].dim_x) 				// dim x
		luo_excel.uof_set(li_riga_excel, "K", astr_righe[li_i].dim_y) 				// dim y
		luo_excel.uof_set(li_riga_excel, "Q", "NO") 									// Rotazinoe possibile?
		luo_excel.uof_set(li_riga_excel, "Z", ls_ordine) 								// Ordine

	next
	
	is_files_generated[upperbound(is_files_generated) + 1] = ls_file_name
catch(RuntimeError e)
	throw e
finally
	destroy luo_excel
end try
end subroutine

public function integer wf_elabora_ordini_in_files ();/**
 * stefanop
 * 25/09/2014
 *
 * Elaboro le righe e invio i files nella cartella
 **/

string ls_sql, ls_error, ls_cod_colore, ls_cod_tessuto, ls_empty[], ls_cod_cliente, ls_sql_formule
int li_index, li_prog_riga_ord_ven
long ll_i, ll_rows, ll_anno_registrazione, ll_num_registrazione, ll_rows_comp, ll_i_comp, li_i_tessuto, ll_j
decimal{4} ld_dim_x, ld_dim_y, ld_quantita, ld_bobina_with, ld_dim_temp
datastore lds_store
uo_map luo_map_tessuti
uo_map luo_map_colori
uo_file luo_file
RuntimeError ex
s_invia_tessuti_righe lstr_riga, lstr_righe[], lstr_empty[]

is_files_generated = ls_empty
tab_1.page_2.dw_lista.accepttext()
tab_1.page_2.dw_lista.setredraw(false)
ll_rows = tab_1.page_2.dw_lista.rowcount()
luo_map_tessuti = create uo_map
luo_file = create uo_file
ex = create RuntimeError

try 
	for ll_i = 0 to ll_rows
		ll_i = tab_1.page_2.dw_lista.find("selezionato='S'", ll_i,  ll_rows)
		if ll_i = 0 or ll_i > ll_rows then exit
		
		// Ciclo tutti gli ordini selezionati dall'operatore
		ll_anno_registrazione = tab_1.page_2.dw_lista.getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione = tab_1.page_2.dw_lista.getitemnumber(ll_i, "num_registrazione")
		ls_cod_cliente = tab_1.page_2.dw_lista.getitemstring(ll_i, "cod_cliente")
		
		pcca.mdi_frame.setmicrohelp("Elaboraz. Ordine " + string(ll_anno_registrazione)+"/"+string(ll_num_registrazione))
		wf_colora_riga(ll_i, COLOR_TRASPARENT)
		
		ls_sql = "select prog_riga_ord_ven, cod_tessuto, cod_non_a_magazzino, dim_x, dim_y " &
				+ "from comp_det_ord_ven " &
				+ "where cod_azienda='" + s_cs_xx.cod_azienda + "' and " &
				+ "anno_registrazione=" + string(ll_anno_registrazione) + " and " &
				+ "num_registrazione=" + string(ll_num_registrazione) &
				+ "and cod_tessuto is not null and cod_non_a_magazzino is not null"
		
		ll_rows_comp = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
		if ll_rows_comp < 0 then
			ex.setmessage("Errore durante la creazione del datastore.~r~n" + g_str.safe(ls_error))
			throw ex
		elseif ll_rows_comp = 0 then
			// nessuna riga nel datastore da controllare
			wf_colora_riga(ll_i, COLOR_SKIPPED)
			continue
		end if
		
		// Ciclo tutte le righe della comp_det_ord_ven. Questo implica che sto controllando solo
		// ordini che arrivano dal configuratore
		for ll_i_comp = 1 to ll_rows_comp
			ls_cod_tessuto = lds_store.getitemstring(ll_i_comp, "cod_tessuto")
			ls_cod_colore = lds_store.getitemstring(ll_i_comp, "cod_non_a_magazzino")
			li_prog_riga_ord_ven = lds_store.getitemnumber(ll_i_comp, "prog_riga_ord_ven")
			ld_dim_x = lds_store.getitemnumber(ll_i_comp, "dim_x")
			ld_dim_y = lds_store.getitemnumber(ll_i_comp, "dim_y")
			
			// Recupero dimensioni da formule
			wf_dim_calcolate(ll_anno_registrazione, ll_num_registrazione, li_prog_riga_ord_ven, ld_dim_x, ld_dim_y)
			
			// Richiesto da Walter; se la dimensione della bobina supera la dimensione del tessuto
			// allora giro la dim_x in dim_y
			// Controllo dimensioni bobbina e nel caso giro il tessuto
			select c_width
			into :ld_bobina_with
			from tab_tessuti_parametri
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :ls_cod_tessuto;
					 
			if sqlca.sqlcode = 0 then
				// Walter vuole un offset di 60 mm perchè la macchina di taglio ha bisogno di
				// questo spazio per il laser.
				if ld_bobina_with > 60 then
					if ld_dim_x > (ld_bobina_with - 60) then
						ld_dim_temp = ld_dim_x
						ld_dim_x = ld_dim_y
						ld_dim_y = ld_dim_temp
					end if
				end if
			end if
			
			
			// recupero quantita riga
			select quan_ordine
			into :ld_quantita
			from det_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ord_ven = :li_prog_riga_ord_ven;
					 
			if sqlca.sqlcode < 0 then
				ex.setmessage(g_str.format("Errore durante il recupero della quantità per l'ordine $1/$2/$3.~r~n$4", &
						ll_anno_registrazione, ll_num_registrazione, li_prog_riga_ord_ven, g_str.safe(sqlca.sqlerrtext)))
				throw ex
			elseif sqlca.sqlcode = 100 or isnull(ld_quantita) then
				ld_quantita = 0
			end if
			
			/**
			Praticamente i dati vengono salvati in questo modo
			COD_TESSUTO (uo_map)
				|- COD_COLORE (uo_map)
				|	|- Struttura righe ordine (struttura)
				|	|- Struttura righe ordine
			**/
			// Recupero tessuto
			if not luo_map_tessuti.has_key(ls_cod_tessuto) then
				luo_map_colori = create uo_map
				luo_map_tessuti.add(ls_cod_tessuto, luo_map_colori)
			end if
			
			// Recupero colore
			luo_map_colori = luo_map_tessuti.get(ls_cod_tessuto)
			if not luo_map_colori.has_key(ls_cod_colore) then
				luo_map_colori.add(ls_cod_colore, lstr_empty)
			end if
			
			lstr_righe = luo_map_colori.get(ls_cod_colore)
			
			li_index = upperbound(lstr_righe) + 1
			lstr_righe[li_index].anno_registrazione = ll_anno_registrazione
			lstr_righe[li_index].num_registrazione = ll_num_registrazione
			lstr_righe[li_index].prog_riga_ord_ven = li_prog_riga_ord_ven
			lstr_righe[li_index].cod_tessuto = ls_cod_tessuto
			lstr_righe[li_index].cod_colore = ls_cod_colore
			lstr_righe[li_index].dim_x = ld_dim_x
			lstr_righe[li_index].dim_y = ld_dim_y
			lstr_righe[li_index].quantita = ld_quantita
			lstr_righe[li_index].cod_cliente = ls_cod_cliente
			
			luo_map_colori.put(ls_cod_colore, lstr_righe)
		next
		
		wf_colora_riga(ll_i, COLOR_SUCCESS)
	next
	
	// Ho popalto le strutture; procedo con la creazione dei file
	ll_rows = luo_map_tessuti.size( )
	
	if ll_rows > 0 then
		tab_1.page_2.dw_lista.setredraw(true)
		pcca.mdi_frame.setmicrohelp("Inizio creazione dei file")
		
		for ll_i = 1 to ll_rows
			luo_map_colori = luo_map_tessuti.get(ll_i)
			li_index = luo_map_colori.size()
			for ll_j = 1 to li_index
				pcca.mdi_frame.setmicrohelp("Elaborazione file " + string(ll_i) + "/" + string(ll_rows) + " - Tessuto: " + string(ll_j) + "/" + string(li_index))
				
				lstr_righe = luo_map_colori.get(ll_j)
				wf_crea_ascii(lstr_righe)
			next
		next
	end if
	
	// Copio i file nella cartella remota
	pcca.mdi_frame.setmicrohelp("Copia dei file nella cartella remota")
	if luo_file.copy_to_folder(is_files_generated, is_remote_folder_to_copy_excel, ls_error) < 0 then
		ex.setmessage(ls_error)
		throw ex
	end if
	
	g_mb.success("Elaborazione completata")
	
catch(RuntimeError e)
	wf_colora_riga(ll_i, COLOR_ERROR)
	g_mb.error(e.getMessage())
	return -1
finally
	// Pulizia variabili
	destroy lds_store
	destroy luo_map_tessuti
	destroy luo_map_colori
	destroy luo_file
	tab_1.page_2.dw_lista.setredraw(true)
end try
 
 pcca.mdi_frame.setmicrohelp("Pronto")
return 1
end function

public subroutine wf_crea_ascii (s_invia_tessuti_righe astr_righe[]);/**
 * stefanop
 * 25/06/2014
 *
 * Elaboro le righe dei tessuti e preparo il foglio ascii
 * In caso di errore lancio l'eccezione che viene recuperata
 * dalla funzione chiamante (wf_invia_files)
 **/
 
string ls_file_name, ls_ordine, ls_cod_sagoma
int li_i, li_count, li_riga_excel
uo_ascii luo_ascii
RuntimeError ex

li_count = upperbound(astr_righe)
if li_count < 1 then return


ex = create RuntimeError

try
	luo_ascii = create uo_ascii
	
	ls_file_name = astr_righe[1].cod_tessuto + "-" + astr_righe[1].cod_colore
	ls_file_name = guo_functions.uof_sanatize_filename(ls_file_name)
	
	// dovrebbe essere .asc ma metto .r così in produzione non devono
	// cambiare ogni volta il formato del file
	ls_file_name = is_user_excel_folder + ls_file_name + ".r"
	
	if upperbound(astr_righe) > 0 then
		
		luo_ascii.open(ls_file_name)
		luo_ascii.append( astr_righe[1].cod_tessuto, 32)
		luo_ascii.append(string(today(), "ddmmyyyy"), 8)
		luo_ascii.append("", 8)
		luo_ascii.append(right("00000000" + string(upperbound(astr_righe)), 8), 8)
		luo_ascii.append("V4", 2)
		luo_ascii.new_line()
		luo_ascii.append(right("00000000" + string(upperbound(astr_righe)), 8), 8)
		
		for li_i = 1 to upperbound(astr_righe)
			
			// Creo sagoma
			//wf_crea_sagoma(astr_righe[li_i], ls_cod_sagoma)
			
			
		
			ls_ordine = string(astr_righe[li_i].anno_registrazione) + "/" + string(astr_righe[li_i].num_registrazione) + "/" + string(astr_righe[li_i].prog_riga_ord_ven)
		
			luo_ascii.new_line()
			luo_ascii.append(astr_righe[li_i].cod_tessuto, 16)
			luo_ascii.append(right("00000" + string(li_i), 5), 5)	// position number
			luo_ascii.append(astr_righe[li_i].cod_cliente, 12)		// Customer code
			luo_ascii.append(string(astr_righe[li_i].anno_registrazione) + "/" + string(astr_righe[li_i].num_registrazione), 12)								// Ordine
			luo_ascii.append("########", 8)										// Shape
			luo_ascii.append("000000.0")								// Grinding Dimensions
			luo_ascii.append("000")										// Priority
			luo_ascii.append("N")											// Rotation
			luo_ascii.append(right("00000000" + string(int(astr_righe[li_i].quantita)), 8)) // Quantita
			luo_ascii.append("", 8)
			luo_ascii.append(right("000000" + string(int(astr_righe[li_i].dim_x)) + ".0", 8))
			luo_ascii.append(right("000000" + string(int(astr_righe[li_i].dim_y)) + ".0", 8))
			luo_ascii.append("", 8)
			luo_ascii.append(ls_ordine, 32)
			luo_ascii.append("", 152)
			
			// Sagoma
			ls_cod_sagoma = string(now(), "hhmmssff")
			luo_ascii.new_line()
			luo_ascii.append("059 2")
			luo_ascii.new_line().append("H " +  string(int(astr_righe[li_i].dim_x)) + ".0")
			luo_ascii.new_line().append("W " +  string(int(astr_righe[li_i].dim_y)) + ".0")
			luo_ascii.new_line().append(ls_cod_sagoma).append(" 0.0 0.0 0.0 0.0")
			
	//		li_riga_excel++
	//		
	//		
	//		luo_excel.uof_set(li_riga_excel, "A", li_i)
	//		luo_excel.uof_set(li_riga_excel, "B", astr_righe[li_i].quantita)			// Quantità
	//		luo_excel.uof_set(li_riga_excel, "C", astr_righe[li_i].cod_tessuto)		// tessuto
	//		luo_excel.uof_set(li_riga_excel, "I", "mm")									// UM
	//		luo_excel.uof_set(li_riga_excel, "J", astr_righe[li_i].dim_x) 				// dim x
	//		luo_excel.uof_set(li_riga_excel, "K", astr_righe[li_i].dim_y) 				// dim y
	//		luo_excel.uof_set(li_riga_excel, "Q", "NO") 									// Rotazinoe possibile?
	//		luo_excel.uof_set(li_riga_excel, "Z", ls_ordine) 								// Ordine
	//
		next
		
		luo_ascii.new_line()
	
		is_files_generated[upperbound(is_files_generated) + 1] = ls_file_name
	end if
catch(RuntimeError e)
	throw e
finally
	destroy luo_ascii
end try
end subroutine

public function integer wf_dim_calcolate (integer ai_anno, long al_numero, long al_prog_riga, ref decimal ad_dim_x, ref decimal ad_dim_y);/**
 * s
 **/
 
string ls_sql, ls_des
decimal ld_misura
int li_count,li_i
datastore lds_store

ls_sql = "SELECT DISTINCT a.des_distinta_taglio, dt.misura &
	FROM distinte_taglio_calcolate as dt &
	join tab_distinte_taglio a on  &
	a.cod_azienda=dt.cod_azienda and  &
	a.cod_prodotto_padre=dt.cod_prodotto_padre and  &
	a.num_sequenza=dt.num_sequenza and &
	a.cod_prodotto_figlio=dt.cod_prodotto_figlio and  &
	a.cod_versione=dt.cod_versione and  &
	a.cod_versione_figlio=dt.cod_versione_figlio and &
	a.progressivo = dt.progressivo &
	where dt.anno_registrazione=" + string(ai_anno) +" and dt.num_registrazione=" + string(al_numero) + " &
	and dt.prog_riga_ord_ven=" + string(al_prog_riga)
	
li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)

for li_i = 1 to li_count
	ls_des = lds_store.getitemstring(li_i, 1)
	ld_misura = lds_store.getitemnumber(li_i, 2)
	
	ls_des = upper(ls_des)

	if g_str.contains(ls_des, "LARGHEZZA") then
		ad_dim_x = ld_misura
	elseif g_str.contains(ls_des, "ALTEZZA") then
		ad_dim_y = ld_misura
	end if
next

destroy lds_store
return li_count
end function

on w_stampa_ord_tessuti.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_stampa_ord_tessuti.destroy
call super::destroy
destroy(this.tab_1)
end on

event resize;/**
 *
 **/
 
tab_1.resize(newwidth - 40, newheight - 40)

tab_1.event ue_resize()
end event

event pc_setwindow;call super::pc_setwindow;

tab_1.page_1.dw_ricerca.insertrow(0)
end event

type tab_1 from tab within w_stampa_ord_tessuti
event ue_resize ( )
integer x = 18
integer y = 20
integer width = 3429
integer height = 1720
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
page_1 page_1
page_2 page_2
end type

event ue_resize();
// Pagina 2
page_2.cb_stampa_tessuti.move(page_2.width - page_2.cb_stampa_tessuti.width - 20, page_2.height - page_2.cb_stampa_tessuti.height - 40)
page_2.dw_lista.resize(page_2.width - 40, page_2.cb_stampa_tessuti.y - 60)
end event

on tab_1.create
this.page_1=create page_1
this.page_2=create page_2
this.Control[]={this.page_1,&
this.page_2}
end on

on tab_1.destroy
destroy(this.page_1)
destroy(this.page_2)
end on

type page_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 3392
integer height = 1592
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ricerca dw_ricerca
end type

on page_1.create
this.dw_ricerca=create dw_ricerca
this.Control[]={this.dw_ricerca}
end on

on page_1.destroy
destroy(this.dw_ricerca)
end on

type dw_ricerca from datawindow within page_1
integer x = 32
integer y = 28
integer width = 2789
integer height = 1000
integer taborder = 20
string title = "none"
string dataobject = "d_stampa_ord_tes_ricerca"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;choose case dwo.name
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca)
		
	case "b_ricerca"
		wf_ricerca()
		
	case "b_cancella"
		tab_1.page_2.dw_lista.reset()
		
end choose
end event

type page_2 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 3392
integer height = 1592
boolean enabled = false
long backcolor = 12632256
string text = "Ordini"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_stampa_tessuti cb_stampa_tessuti
dw_lista dw_lista
end type

on page_2.create
this.cb_stampa_tessuti=create cb_stampa_tessuti
this.dw_lista=create dw_lista
this.Control[]={this.cb_stampa_tessuti,&
this.dw_lista}
end on

on page_2.destroy
destroy(this.cb_stampa_tessuti)
destroy(this.dw_lista)
end on

type cb_stampa_tessuti from commandbutton within page_2
integer x = 2843
integer y = 1448
integer width = 507
integer height = 112
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia Files"
end type

event clicked;
setpointer(Hourglass!)

if wf_precheck_risorse() then
	if wf_elabora_ordini_in_files() < 0 then
		
	end if
end if

setpointer(Arrow!)

end event

type dw_lista from datawindow within page_2
integer x = 9
integer y = 8
integer width = 3360
integer height = 1280
integer taborder = 30
string title = "none"
string dataobject = "d_stampa_ord_tes_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event buttonclicked;choose case dwo.name
		
	case "b_sel_tutti"
		wf_seleziona("selezionato", "S")
		
	case "b_sel_nessuno"
		wf_seleziona("selezionato", "N")
		
end choose
end event

event rowfocuschanged;selectrow(0, false)
selectrow(currentrow, true)
end event

