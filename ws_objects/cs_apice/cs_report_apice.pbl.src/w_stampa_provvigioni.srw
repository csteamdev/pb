﻿$PBExportHeader$w_stampa_provvigioni.srw
$PBExportComments$finestra Report Provvigioni per agente : analitico e sintetico
forward
global type w_stampa_provvigioni from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_stampa_provvigioni
end type
type cb_report from commandbutton within w_stampa_provvigioni
end type
type cb_selezione from commandbutton within w_stampa_provvigioni
end type
type dw_selezione from uo_cs_xx_dw within w_stampa_provvigioni
end type
type dw_report from uo_cs_xx_dw within w_stampa_provvigioni
end type
type sle_1 from singlelineedit within w_stampa_provvigioni
end type
end forward

global type w_stampa_provvigioni from w_cs_xx_principale
int X=60
int Y=313
int Width=3557
int Height=1929
boolean TitleBar=true
string Title="Report Provvigioni agenti"
boolean Resizable=false
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
sle_1 sle_1
end type
global w_stampa_provvigioni w_stampa_provvigioni

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 60
this.y = 313
this.width = 2007
this.height = 537

end event

on w_stampa_provvigioni.create
int iCurrent
call w_cs_xx_principale::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_annulla
this.Control[iCurrent+2]=cb_report
this.Control[iCurrent+3]=cb_selezione
this.Control[iCurrent+4]=dw_selezione
this.Control[iCurrent+5]=dw_report
this.Control[iCurrent+6]=sle_1
end on

on w_stampa_provvigioni.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.sle_1)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_agente", &
							sqlca, &
							"anag_agenti", &
							"cod_agente", &
                     "rag_soc_1", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_annulla from commandbutton within w_stampa_provvigioni
int X=1212
int Y=341
int Width=366
int Height=81
int TabOrder=30
string Text="&Esci"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_stampa_provvigioni
int X=1601
int Y=341
int Width=366
int Height=81
int TabOrder=40
string Text="&Report"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_agente , ls_flag_movimenti, ls_flag_contabilita, ls_flag_sint_anal, ls_messaggio
string ls_cod_agente_1, ls_cod_agente_2, ls_rag_soc_1, ls_flag_tipo_fat_ven, ls_cod_tipo_fat_ven
datetime   ldt_data_fat_da, ldt_data_fat_a
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, ll_num_righe, ll_i
double ld_imponibile_provv_1, ld_imponibile_provv_2, ld_importo_provv_1, ld_importo_provv_2
double ld_provv_1, ld_provv_2, ld_imp_fattura
integer li_risp
dw_selezione.accepttext()

ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
// ls_flag_movimenti = dw_selezione.getitemstring(1,"flag_movimenti")
ls_flag_contabilita = dw_selezione.getitemstring(1,"flag_contabilita")
ls_flag_sint_anal = dw_selezione.getitemstring(1,"flag_sint_anal")
ldt_data_fat_da  = dw_selezione.getitemdatetime(1,"data_fat_da")
ldt_data_fat_a   = dw_selezione.getitemdatetime(1,"data_fat_a")

if isnull(ls_cod_agente) then
	g_mb.messagebox("Stampa Provvigioni", "Selezionare l'agente")	
	return
end if

// if isnull(ls_flag_movimenti) then ls_flag_movimenti = "N"
if isnull(ls_flag_contabilita ) then ls_flag_contabilita = "N"
if isnull(ls_flag_sint_anal) then ls_flag_sint_anal = "S"	// "S"intetico o "A"nalitico

if isnull(ldt_data_fat_da) then  
	g_mb.messagebox("Stampa Provvigioni", "Selezionare la data fattura da")	
	return
end if

if isnull(ldt_data_fat_a)  then  
	g_mb.messagebox("Stampa Provvigioni", "Selezionare la data fattura a")	
	return
end if

dw_selezione.hide()


if ls_flag_sint_anal = "S" then // Sintetico
	dw_report.DataObject = 'd_stampa_provv'
	dw_report.settransobject(sqlca)
	ll_num_righe = dw_report.retrieve(s_cs_xx.cod_azienda, ldt_data_fat_da, ldt_data_fat_a, ls_cod_agente, "S", ls_flag_contabilita )
//	for ll_i = 1 to ll_num_righe
//		ls_cod_tipo_fat_ven = dw_report.getitemstring(ll_i,"tes_fat_ven_cod_tipo_fat_ven")
//		select flag_tipo_fat_ven
//		into   :ls_flag_tipo_fat_ven
//		from   tab_tipi_fat_ven
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven ;
//		if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
//			messagebox("Stampa provvigioni", "Errore in ricerca tipo fattura ")
//			return -1
//		end if
//		if ls_flag_tipo_fat_ven = 'N' then
//			ld_imp_fattura = dw_report.getitemnumber(ll_i,"tes_fat_ven_imponibile_iva")
//			dw_report.setitem(ll_i, "tes_fat_ven_imponibile_iva", - ld_imp_fattura)
//			ld_imponibile_provv_1 = dw_report.getitemnumber(ll_i,"imponibile_provvigione")
//			dw_report.setitem(ll_i, "imponibile_provvigione", ld_imponibile_provv_1*(- 1))
//			ld_importo_provv_1 = dw_report.getitemnumber(ll_i,"totale_provvigione")
//			dw_report.setitem(ll_i, "totale_provvigione", ld_importo_provv_1*(- 1))
//			
//		end if
//	next
end if

if ls_flag_sint_anal = "A" then // Analitico
	dw_report.DataObject = 'd_stampa_provv_anal'
	dw_report.settransobject(sqlca)
	// utilizzo le colonne det_fat_ven_prezzo_vendita, det_fat_ven_provvigione_1, det_fat_ven_quan_fatturata
	// della d_stampa_provv_anal come contenitore per scriverci l'imponibile, la % provv., l'importo provv.
	ll_num_righe = dw_report.retrieve(s_cs_xx.cod_azienda, date(ldt_data_fat_da), date(ldt_data_fat_a), ls_cod_agente, "S", ls_flag_contabilita )
	for ll_i = 1 to ll_num_righe
		ld_imponibile_provv_1 = 0
		ld_imponibile_provv_2 = 0
		ld_importo_provv_1 = 0
		ld_importo_provv_2 = 0
		ld_provv_1 = 0
		ld_provv_2 = 0
		ls_messaggio = " "
		ll_anno_registrazione = dw_report.getitemnumber(ll_i, "det_fat_ven_anno_registrazione")
		ll_num_registrazione = dw_report.getitemnumber(ll_i, "cnum_registrazione")
		ll_prog_riga_fat_ven = dw_report.getitemnumber(ll_i, "det_fat_ven_prog_riga_fat_ven")
      ls_cod_agente_1 = dw_report.getitemstring(ll_i, "anag_clienti_cod_agente_1")
      ls_cod_agente_2 = dw_report.getitemstring(ll_i, "anag_clienti_cod_agente_2")

		li_risp = f_calcola_provvigioni(s_cs_xx.cod_azienda, ll_num_registrazione, &
				ll_anno_registrazione, ll_prog_riga_fat_ven, &
				ld_imponibile_provv_1, ld_imponibile_provv_2, ld_importo_provv_1, &
				ld_importo_provv_2, ld_provv_1, ld_provv_2, ls_messaggio)
//		messagebox("debug", string(ll_num_registrazione) + " - " + &
//				string(ll_prog_riga_fat_ven)+ " ; imponibile1: " + &
//				string(ld_imponibile_provv_1)+ " ; imponibile2: " + &
//				string(ld_imponibile_provv_2)+ ", importo1: " +string(ld_importo_provv_1) + &
//				 ", importo2: " +string(ld_importo_provv_2) + &
//				" , %1: " + string(ld_provv_1) + " , %2: " + string(ld_provv_2))
//
		sle_1.text = "Fatt. n° reg. " + string(ll_num_registrazione) + " - " + string(ll_anno_registrazione)
		if ls_cod_agente_1 = ls_cod_agente then
			dw_report.setitem(ll_i, "det_fat_ven_prezzo_vendita", ld_imponibile_provv_1)
			dw_report.setitem(ll_i, "det_fat_ven_provvigione_1", ld_provv_1)
			dw_report.setitem(ll_i, "det_fat_ven_quan_fatturata", ld_importo_provv_1)
		else
			dw_report.setitem(ll_i, "det_fat_ven_prezzo_vendita", ld_imponibile_provv_2)
			dw_report.setitem(ll_i, "det_fat_ven_provvigione_1", ld_provv_2)
			dw_report.setitem(ll_i, "det_fat_ven_quan_fatturata", ld_importo_provv_2)
		end if

	next
	
	
end if

select rag_soc_1
into :ls_rag_soc_1
from anag_agenti
where cod_azienda = :s_cs_xx.cod_azienda
  and cod_agente = :ls_cod_agente;
dw_report.object.agente.text = "Agente: " + ls_rag_soc_1 +" (" + ls_cod_agente + ")"

parent.x = 60
parent.y = 113
parent.width = 3553
parent.height = 1929

dw_report.show()
cb_selezione.show()
dw_report.change_dw_current()

end event

type cb_selezione from commandbutton within w_stampa_provvigioni
int X=3132
int Y=1741
int Width=366
int Height=81
int TabOrder=50
string Text="Selezione"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_prodotto, ls_cat_mer

// dw_report.RowsDiscard (1, dw_report.rowcount(), primary! )
dw_report.reset()
dw_selezione.show()

parent.x = 60
parent.y = 313
parent.width = 2007
parent.height = 537

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_stampa_provvigioni
int X=23
int Y=21
int Width=1943
int Height=301
int TabOrder=20
string DataObject="d_stampa_provvigioni_selezione"
BorderStyle BorderStyle=StyleLowered!
end type

type dw_report from uo_cs_xx_dw within w_stampa_provvigioni
int X=23
int Y=21
int Width=3475
int Height=1681
int TabOrder=10
boolean Visible=false
string DataObject="d_stampa_provv"
boolean HScrollBar=true
boolean VScrollBar=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

type sle_1 from singlelineedit within w_stampa_provvigioni
int X=23
int Y=341
int Width=1162
int Height=73
int TabOrder=21
boolean BringToTop=true
boolean Border=false
boolean AutoHScroll=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

