﻿$PBExportHeader$w_tab_analisi_abc.srw
$PBExportComments$finestra definizione scaglioni per analisi abc
forward
global type w_tab_analisi_abc from w_cs_xx_principale
end type
type dw_tab_analisi_abc from uo_cs_xx_dw within w_tab_analisi_abc
end type
end forward

global type w_tab_analisi_abc from w_cs_xx_principale
int Width=2401
int Height=853
boolean TitleBar=true
string Title="Gestione Analisi ABC"
dw_tab_analisi_abc dw_tab_analisi_abc
end type
global w_tab_analisi_abc w_tab_analisi_abc

on w_tab_analisi_abc.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_analisi_abc=create dw_tab_analisi_abc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_analisi_abc
end on

on w_tab_analisi_abc.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_analisi_abc)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_analisi_abc.set_dw_key("cod_azienda")
dw_tab_analisi_abc.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)

end event

type dw_tab_analisi_abc from uo_cs_xx_dw within w_tab_analisi_abc
int X=23
int Y=21
int Width=2309
int Height=681
string DataObject="d_tab_analisi_abc"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

