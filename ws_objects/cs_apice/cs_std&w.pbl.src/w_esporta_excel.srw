﻿$PBExportHeader$w_esporta_excel.srw
$PBExportComments$Window  esporta_excel
forward
global type w_esporta_excel from w_cs_xx_principale
end type
type st_3 from statictext within w_esporta_excel
end type
type st_1 from statictext within w_esporta_excel
end type
type ddlb_cod_tipo_analisi from dropdownlistbox within w_esporta_excel
end type
type cb_path_righe from commandbutton within w_esporta_excel
end type
type cb_path_stat_1 from commandbutton within w_esporta_excel
end type
type cb_path_stat_2 from commandbutton within w_esporta_excel
end type
type cb_path_stat_3 from commandbutton within w_esporta_excel
end type
type cb_path_stat_4 from commandbutton within w_esporta_excel
end type
type cb_path_stat_5 from commandbutton within w_esporta_excel
end type
type cb_path_stat_6 from commandbutton within w_esporta_excel
end type
type cb_path_stat_7 from commandbutton within w_esporta_excel
end type
type cb_path_stat_8 from commandbutton within w_esporta_excel
end type
type cb_path_stat_9 from commandbutton within w_esporta_excel
end type
type cb_path_stat_10 from commandbutton within w_esporta_excel
end type
type cb_esporta from commandbutton within w_esporta_excel
end type
type cbx_ord_acq from checkbox within w_esporta_excel
end type
type cbx_off_ven from checkbox within w_esporta_excel
end type
type cbx_bol_ven from checkbox within w_esporta_excel
end type
type cbx_off_acq from checkbox within w_esporta_excel
end type
type gb_1 from groupbox within w_esporta_excel
end type
type cbx_fat_acq from checkbox within w_esporta_excel
end type
type em_data_fine from editmask within w_esporta_excel
end type
type st_2 from statictext within w_esporta_excel
end type
type dw_tab_path_esportazioni from uo_cs_xx_dw within w_esporta_excel
end type
type cbx_ord_ven from checkbox within w_esporta_excel
end type
type cbx_bol_acq from checkbox within w_esporta_excel
end type
type cbx_fat_ven from checkbox within w_esporta_excel
end type
end forward

global type w_esporta_excel from w_cs_xx_principale
int Width=3027
int Height=1461
boolean TitleBar=true
string Title="Esportazioni Verso Fogli di Calcolo"
st_3 st_3
st_1 st_1
ddlb_cod_tipo_analisi ddlb_cod_tipo_analisi
cb_path_righe cb_path_righe
cb_path_stat_1 cb_path_stat_1
cb_path_stat_2 cb_path_stat_2
cb_path_stat_3 cb_path_stat_3
cb_path_stat_4 cb_path_stat_4
cb_path_stat_5 cb_path_stat_5
cb_path_stat_6 cb_path_stat_6
cb_path_stat_7 cb_path_stat_7
cb_path_stat_8 cb_path_stat_8
cb_path_stat_9 cb_path_stat_9
cb_path_stat_10 cb_path_stat_10
cb_esporta cb_esporta
cbx_ord_acq cbx_ord_acq
cbx_off_ven cbx_off_ven
cbx_bol_ven cbx_bol_ven
cbx_off_acq cbx_off_acq
gb_1 gb_1
cbx_fat_acq cbx_fat_acq
em_data_fine em_data_fine
st_2 st_2
dw_tab_path_esportazioni dw_tab_path_esportazioni
cbx_ord_ven cbx_ord_ven
cbx_bol_acq cbx_bol_acq
cbx_fat_ven cbx_fat_ven
end type
global w_esporta_excel w_esporta_excel

forward prototypes
public function integer wf_esporta_off_acq ()
public function integer wf_esporta_off_ven ()
public function integer wf_esporta_ord_acq ()
public function integer wf_esporta_bol_ven ()
public function integer wf_esporta_fat_acq ()
public function integer wf_esporta_bol_acq ()
public function integer wf_esporta_ord_ven ()
public function integer wf_esporta_fat_ven ()
end prototypes

public function integer wf_esporta_off_acq ();string  ls_cod_pagamento, ls_cod_tipo_off_acq, ls_cod_tipo_det_acq, &
        ls_flag_tipo_det_acq, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_off_acq,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
end if

declare righe_tes_off_acq cursor for 
select  tes_off_acq.cod_tipo_off_acq,
		  tes_off_acq.data_registrazione,
		  tes_off_acq.cod_pagamento,
		  tes_off_acq.sconto,
		  tes_off_acq.anno_registrazione,
		  tes_off_acq.num_registrazione,
		  det_off_acq.val_riga,
		  det_off_acq.cod_tipo_det_acq,
		  det_off_acq.prog_riga_off_acq,
		  det_off_acq_stat.cod_statistico_1,
		  det_off_acq_stat.cod_statistico_2,
		  det_off_acq_stat.cod_statistico_3,
		  det_off_acq_stat.cod_statistico_4,
		  det_off_acq_stat.cod_statistico_5,
		  det_off_acq_stat.cod_statistico_6,
		  det_off_acq_stat.cod_statistico_7,
		  det_off_acq_stat.cod_statistico_8,
		  det_off_acq_stat.cod_statistico_9,
		  det_off_acq_stat.cod_statistico_10,		
		  det_off_acq_stat.percentuale,
		  det_off_acq_stat.progressivo
from	  tes_off_acq,det_off_acq,det_off_acq_stat
where	  tes_off_acq.cod_azienda=:s_cs_xx.cod_azienda
and     tes_off_acq.cod_azienda = det_off_acq.cod_azienda
and     tes_off_acq.anno_registrazione = det_off_acq.anno_registrazione
and 	  tes_off_acq.num_registrazione = det_off_acq.num_registrazione
and  	  tes_off_acq.cod_azienda = det_off_acq_stat.cod_azienda
and     tes_off_acq.anno_registrazione = det_off_acq_stat.anno_registrazione
and 	  tes_off_acq.num_registrazione = det_off_acq_stat.num_registrazione
and     det_off_acq_stat.flag_trasferito='N'			
and     tes_off_acq.data_registrazione <= :ldt_data_fine
and     det_off_acq_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta off_acq: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_off_acq;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta off_acq: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_off_acq into :ls_cod_tipo_off_acq, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_acq, &
										  :ll_prog_riga_off_acq, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;
   
   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_acq
   	into   :ls_flag_tipo_det_acq
	   from   tab_tipi_det_acq
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	   if ls_flag_tipo_det_acq = "M" or &
   	   ls_flag_tipo_det_acq = "C" or &
      	ls_flag_tipo_det_acq = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_acq = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="A" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_off_acq) + fill('A',4 - len(string(ll_prog_riga_off_acq))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_off_acq;
			     return -1
			  end if

			  UPDATE det_off_acq_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_registrazione = :li_anno_registrazione 
			  AND    num_registrazione = :ll_num_registrazione 
			  AND    prog_riga_off_acq = :ll_prog_riga_off_acq 
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_off_acq;
			     return -1
			  end if

loop

close righe_tes_off_acq;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

public function integer wf_esporta_off_ven ();string  ls_cod_pagamento, ls_cod_tipo_off_ven, ls_cod_tipo_det_ven, &
        ls_flag_tipo_det_ven, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_off_ven,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
	return -1
end if

declare righe_tes_off_ven cursor for 
select  tes_off_ven.cod_tipo_off_ven,
		  tes_off_ven.data_registrazione,
		  tes_off_ven.cod_pagamento,
		  tes_off_ven.sconto,
		  tes_off_ven.anno_registrazione,
		  tes_off_ven.num_registrazione,
		  det_off_ven.val_riga,
		  det_off_ven.cod_tipo_det_ven,
		  det_off_ven.prog_riga_off_ven,
		  det_off_ven_stat.cod_statistico_1,
		  det_off_ven_stat.cod_statistico_2,
		  det_off_ven_stat.cod_statistico_3,
		  det_off_ven_stat.cod_statistico_4,
		  det_off_ven_stat.cod_statistico_5,
		  det_off_ven_stat.cod_statistico_6,
		  det_off_ven_stat.cod_statistico_7,
		  det_off_ven_stat.cod_statistico_8,
		  det_off_ven_stat.cod_statistico_9,
		  det_off_ven_stat.cod_statistico_10,		
		  det_off_ven_stat.percentuale,
		  det_off_ven_stat.progressivo
from	  tes_off_ven,det_off_ven,det_off_ven_stat
where	  tes_off_ven.cod_azienda=:s_cs_xx.cod_azienda
and     tes_off_ven.cod_azienda = det_off_ven.cod_azienda
and     tes_off_ven.anno_registrazione = det_off_ven.anno_registrazione
and 	  tes_off_ven.num_registrazione = det_off_ven.num_registrazione
and  	  tes_off_ven.cod_azienda = det_off_ven_stat.cod_azienda
and     tes_off_ven.anno_registrazione = det_off_ven_stat.anno_registrazione
and 	  tes_off_ven.num_registrazione = det_off_ven_stat.num_registrazione
and     det_off_ven_stat.flag_trasferito='N'			
and     tes_off_ven.data_registrazione <= :ldt_data_fine
and     det_off_ven_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta off_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_off_ven;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta off_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_off_ven into :ls_cod_tipo_off_ven, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_ven, &
										  :ll_prog_riga_off_ven, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;
   
   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_ven
   	into   :ls_flag_tipo_det_ven
	   from   tab_tipi_det_ven
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	   if ls_flag_tipo_det_ven = "M" or &
   	   ls_flag_tipo_det_ven = "C" or &
      	ls_flag_tipo_det_ven = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_ven = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="E" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_off_ven) + fill('A',4 - len(string(ll_prog_riga_off_ven))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;
			  
			  if sqlca.sqlcode <> 0 then
				  close righe_tes_off_ven;
			     return -1
			  end if
			  
			  UPDATE det_off_ven_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_registrazione = :li_anno_registrazione 
			  AND    num_registrazione = :ll_num_registrazione 
			  AND    prog_riga_off_ven = :ll_prog_riga_off_ven
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_off_ven;
			     return -1
			  end if

loop
close righe_tes_off_ven;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

public function integer wf_esporta_ord_acq ();string  ls_cod_pagamento, ls_cod_tipo_ord_acq, ls_cod_tipo_det_acq, &
        ls_flag_tipo_det_acq, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_ord_acq,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
end if

declare righe_tes_ord_acq cursor for 
select  tes_ord_acq.cod_tipo_ord_acq,
		  tes_ord_acq.data_registrazione,
		  tes_ord_acq.cod_pagamento,
		  tes_ord_acq.sconto,
		  tes_ord_acq.anno_registrazione,
		  tes_ord_acq.num_registrazione,
		  det_ord_acq.val_riga,
		  det_ord_acq.cod_tipo_det_acq,
		  det_ord_acq.prog_riga_ordine_acq,
		  det_ord_acq_stat.cod_statistico_1,
		  det_ord_acq_stat.cod_statistico_2,
		  det_ord_acq_stat.cod_statistico_3,
		  det_ord_acq_stat.cod_statistico_4,
		  det_ord_acq_stat.cod_statistico_5,
		  det_ord_acq_stat.cod_statistico_6,
		  det_ord_acq_stat.cod_statistico_7,
		  det_ord_acq_stat.cod_statistico_8,
		  det_ord_acq_stat.cod_statistico_9,
		  det_ord_acq_stat.cod_statistico_10,		
		  det_ord_acq_stat.percentuale,
		  det_ord_acq_stat.progressivo
from	  tes_ord_acq,det_ord_acq,det_ord_acq_stat
where	  tes_ord_acq.cod_azienda =:s_cs_xx.cod_azienda
and     tes_ord_acq.cod_azienda = det_ord_acq.cod_azienda
and     tes_ord_acq.anno_registrazione = det_ord_acq.anno_registrazione
and 	  tes_ord_acq.num_registrazione = det_ord_acq.num_registrazione
and  	  tes_ord_acq.cod_azienda = det_ord_acq_stat.cod_azienda
and     tes_ord_acq.anno_registrazione = det_ord_acq_stat.anno_registrazione
and 	  tes_ord_acq.num_registrazione = det_ord_acq_stat.num_registrazione
and     det_ord_acq_stat.flag_trasferito='N'			
and     tes_ord_acq.data_registrazione <= :ldt_data_fine
and     det_ord_acq_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta ord_acq: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_ord_acq;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta ord_acq: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_ord_acq into :ls_cod_tipo_ord_acq, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_acq, &
										  :ll_prog_riga_ord_acq, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;   

   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_acq
   	into   :ls_flag_tipo_det_acq
	   from   tab_tipi_det_acq
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	   if ls_flag_tipo_det_acq = "M" or &
   	   ls_flag_tipo_det_acq = "C" or &
      	ls_flag_tipo_det_acq = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_acq = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="B" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_ord_acq) + fill('A',4 - len(string(ll_prog_riga_ord_acq))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_ord_acq;
			     return -1
			  end if
			  
			  UPDATE det_ord_acq_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_registrazione = :li_anno_registrazione 
			  AND    num_registrazione = :ll_num_registrazione 
			  AND    prog_riga_ordine_acq = :ll_prog_riga_ord_acq 
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_ord_acq;
			     return -1
			  end if

loop
close righe_tes_ord_acq;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

public function integer wf_esporta_bol_ven ();string  ls_cod_pagamento, ls_cod_tipo_bol_ven, ls_cod_tipo_det_ven, &
        ls_flag_tipo_det_ven, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_bol_ven,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
end if

declare righe_tes_bol_ven cursor for 
select  tes_bol_ven.cod_tipo_bol_ven,
		  tes_bol_ven.data_registrazione,
		  tes_bol_ven.cod_pagamento,
		  tes_bol_ven.sconto,
		  tes_bol_ven.anno_registrazione,
		  tes_bol_ven.num_registrazione,
		  det_bol_ven.quan_consegnata,
		  det_bol_ven.cod_tipo_det_ven,
		  det_bol_ven.prog_riga_bol_ven,
		  det_bol_ven_stat.cod_statistico_1,
		  det_bol_ven_stat.cod_statistico_2,
		  det_bol_ven_stat.cod_statistico_3,
		  det_bol_ven_stat.cod_statistico_4,
		  det_bol_ven_stat.cod_statistico_5,
		  det_bol_ven_stat.cod_statistico_6,
		  det_bol_ven_stat.cod_statistico_7,
		  det_bol_ven_stat.cod_statistico_8,
		  det_bol_ven_stat.cod_statistico_9,
		  det_bol_ven_stat.cod_statistico_10,		
		  det_bol_ven_stat.percentuale,
		  det_bol_ven_stat.progressivo
from	  tes_bol_ven,det_bol_ven,det_bol_ven_stat
where	  tes_bol_ven.cod_azienda=:s_cs_xx.cod_azienda
and     tes_bol_ven.cod_azienda = det_bol_ven.cod_azienda
and     tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione
and 	  tes_bol_ven.num_registrazione = det_bol_ven.num_registrazione
and  	  tes_bol_ven.cod_azienda = det_bol_ven_stat.cod_azienda
and     tes_bol_ven.anno_registrazione = det_bol_ven_stat.anno_registrazione
and 	  tes_bol_ven.num_registrazione = det_bol_ven_stat.num_registrazione
and     det_bol_ven_stat.flag_trasferito='N'			
and     tes_bol_ven.data_registrazione <= :ldt_data_fine
and     det_bol_ven_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta bol_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_bol_ven;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta bol_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_bol_ven into :ls_cod_tipo_bol_ven, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_ven, &
										  :ll_prog_riga_bol_ven, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;
   
   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_ven
   	into   :ls_flag_tipo_det_ven
	   from   tab_tipi_det_ven
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	   if ls_flag_tipo_det_ven = "M" or &
   	   ls_flag_tipo_det_ven = "C" or &
      	ls_flag_tipo_det_ven = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_ven = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="G" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_bol_ven) + fill('A',4 - len(string(ll_prog_riga_bol_ven))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_bol_ven;
			     return -1
			  end if

			  UPDATE det_bol_ven_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_registrazione = :li_anno_registrazione 
			  AND    num_registrazione = :ll_num_registrazione 
			  AND    prog_riga_bol_ven = :ll_prog_riga_bol_ven
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_bol_ven;
			     return -1
			  end if

loop

close righe_tes_bol_ven;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

public function integer wf_esporta_fat_acq ();string  ls_cod_pagamento, ls_cod_tipo_fat_acq, ls_cod_tipo_det_acq, &
        ls_flag_tipo_det_acq, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_fat_acq,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
end if

declare righe_tes_fat_acq cursor for 
select  tes_fat_acq.cod_tipo_fat_acq,
		  tes_fat_acq.data_registrazione,
		  tes_fat_acq.cod_pagamento,
		  tes_fat_acq.sconto,
		  tes_fat_acq.anno_doc_origine,
		  tes_fat_acq.numeratore_doc_origine,
		  det_fat_acq.quan_fatturata,
		  det_fat_acq.cod_tipo_det_acq,
		  det_fat_acq.prog_riga_fat_acq,
		  det_fat_acq_stat.cod_statistico_1,
		  det_fat_acq_stat.cod_statistico_2,
		  det_fat_acq_stat.cod_statistico_3,
		  det_fat_acq_stat.cod_statistico_4,
		  det_fat_acq_stat.cod_statistico_5,
		  det_fat_acq_stat.cod_statistico_6,
		  det_fat_acq_stat.cod_statistico_7,
		  det_fat_acq_stat.cod_statistico_8,
		  det_fat_acq_stat.cod_statistico_9,
		  det_fat_acq_stat.cod_statistico_10,		
		  det_fat_acq_stat.percentuale,
		  det_fat_acq_stat.progressivo
from	  tes_fat_acq,det_fat_acq,det_fat_acq_stat
where	  tes_fat_acq.cod_azienda =:s_cs_xx.cod_azienda
and     tes_fat_acq.cod_azienda = det_fat_acq.cod_azienda
and     tes_fat_acq.anno_doc_origine = det_fat_acq.anno_doc_origine
and 	  tes_fat_acq.numeratore_doc_origine = det_fat_acq.numeratore_doc_origine
and  	  tes_fat_acq.cod_azienda = det_fat_acq_stat.cod_azienda
and     tes_fat_acq.anno_doc_origine = det_fat_acq_stat.anno_doc_origine
and 	  tes_fat_acq.numeratore_doc_origine = det_fat_acq_stat.numeratore_doc_origine
and     det_fat_acq_stat.flag_trasferito='N'			
and     tes_fat_acq.data_registrazione <= :ldt_data_fine
and     det_fat_acq_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta fat_acq: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_fat_acq;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta fat_acq: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_fat_acq into :ls_cod_tipo_fat_acq, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_acq, &
										  :ll_prog_riga_fat_acq, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;   

   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_acq
   	into   :ls_flag_tipo_det_acq
	   from   tab_tipi_det_acq
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	   if ls_flag_tipo_det_acq = "M" or &
   	   ls_flag_tipo_det_acq = "C" or &
      	ls_flag_tipo_det_acq = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_acq = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="D" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_fat_acq) + fill('A',4 - len(string(ll_prog_riga_fat_acq))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_fat_acq;
			     return -1
			  end if
			  
			  UPDATE det_fat_acq_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_doc_origine = :li_anno_registrazione 
			  AND    numeratore_doc_origine = :ll_num_registrazione 
			  AND    prog_riga_ordine_acq = :ll_prog_riga_fat_acq 
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_fat_acq;
			     return -1
			  end if

loop
close righe_tes_fat_acq;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

public function integer wf_esporta_bol_acq ();string  ls_cod_pagamento, ls_cod_tipo_bol_acq, ls_cod_tipo_det_acq, &
        ls_flag_tipo_det_acq, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_bol_acq,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
end if

declare righe_tes_bol_acq cursor for 
select  tes_bol_acq.cod_tipo_bol_acq,
		  tes_bol_acq.data_registrazione,
		  tes_bol_acq.cod_pagamento,
		  tes_bol_acq.sconto,
		  tes_bol_acq.anno_bolla_acq,
		  tes_bol_acq.num_bolla_acq,
		  det_bol_acq.quan_arrivata,
		  det_bol_acq.cod_tipo_det_acq,
		  det_bol_acq.prog_riga_bolla_acq,
		  det_bol_acq_stat.cod_statistico_1,
		  det_bol_acq_stat.cod_statistico_2,
		  det_bol_acq_stat.cod_statistico_3,
		  det_bol_acq_stat.cod_statistico_4,
		  det_bol_acq_stat.cod_statistico_5,
		  det_bol_acq_stat.cod_statistico_6,
		  det_bol_acq_stat.cod_statistico_7,
		  det_bol_acq_stat.cod_statistico_8,
		  det_bol_acq_stat.cod_statistico_9,
		  det_bol_acq_stat.cod_statistico_10,		
		  det_bol_acq_stat.percentuale,
		  det_bol_acq_stat.progressivo
from	  tes_bol_acq,det_bol_acq,det_bol_acq_stat
where	  tes_bol_acq.cod_azienda=:s_cs_xx.cod_azienda
and     tes_bol_acq.cod_azienda = det_bol_acq.cod_azienda
and     tes_bol_acq.anno_bolla_acq = det_bol_acq.anno_bolla_acq
and 	  tes_bol_acq.num_bolla_acq = det_bol_acq.num_bolla_acq
and  	  tes_bol_acq.cod_azienda = det_bol_acq_stat.cod_azienda
and     tes_bol_acq.anno_bolla_acq = det_bol_acq_stat.anno_bolla_acq
and 	  tes_bol_acq.num_bolla_acq = det_bol_acq_stat.num_bolla_acq
and     det_bol_acq_stat.flag_trasferito='N'			
and     tes_bol_acq.data_registrazione <= :ldt_data_fine
and     det_bol_acq_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Esporta bol_acq Errore: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_bol_acq;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Esporta bol_acq Errore: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_bol_acq into :ls_cod_tipo_bol_acq, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_acq, &
										  :ll_prog_riga_bol_acq, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;
   
   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_acq
   	into   :ls_flag_tipo_det_acq
	   from   tab_tipi_det_acq
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	   if ls_flag_tipo_det_acq = "M" or &
   	   ls_flag_tipo_det_acq = "C" or &
      	ls_flag_tipo_det_acq = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_acq = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="C" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_bol_acq) + fill('A',4 - len(string(ll_prog_riga_bol_acq))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_bol_acq;
			     return -1
			  end if

			  UPDATE det_bol_acq_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_bolla_acq = :li_anno_registrazione 
			  AND    num_bolla_acq = :ll_num_registrazione 
			  AND    prog_riga_bol_acq = :ll_prog_riga_bol_acq
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_bol_acq;
			     return -1
			  end if

loop

close righe_tes_bol_acq;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

public function integer wf_esporta_ord_ven ();string  ls_cod_pagamento, ls_cod_tipo_ord_ven, ls_cod_tipo_det_ven, &
        ls_flag_tipo_det_ven, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_ord_ven,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
end if

declare righe_tes_ord_ven cursor for 
select  tes_ord_ven.cod_tipo_ord_ven,
		  tes_ord_ven.data_registrazione,
		  tes_ord_ven.cod_pagamento,
		  tes_ord_ven.sconto,
		  tes_ord_ven.anno_registrazione,
		  tes_ord_ven.num_registrazione,
		  det_ord_ven.val_riga,
		  det_ord_ven.cod_tipo_det_ven,
		  det_ord_ven.prog_riga_ord_ven,
		  det_ord_ven_stat.cod_statistico_1,
		  det_ord_ven_stat.cod_statistico_2,
		  det_ord_ven_stat.cod_statistico_3,
		  det_ord_ven_stat.cod_statistico_4,
		  det_ord_ven_stat.cod_statistico_5,
		  det_ord_ven_stat.cod_statistico_6,
		  det_ord_ven_stat.cod_statistico_7,
		  det_ord_ven_stat.cod_statistico_8,
		  det_ord_ven_stat.cod_statistico_9,
		  det_ord_ven_stat.cod_statistico_10,		
		  det_ord_ven_stat.percentuale,
		  det_ord_ven_stat.progressivo
from	  tes_ord_ven,det_ord_ven,det_ord_ven_stat
where	  tes_ord_ven.cod_azienda =:s_cs_xx.cod_azienda
and     tes_ord_ven.cod_azienda = det_ord_ven.cod_azienda
and     tes_ord_ven.anno_registrazione = det_ord_ven.anno_registrazione
and 	  tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione
and  	  tes_ord_ven.cod_azienda = det_ord_ven_stat.cod_azienda
and     tes_ord_ven.anno_registrazione = det_ord_ven_stat.anno_registrazione
and 	  tes_ord_ven.num_registrazione = det_ord_ven_stat.num_registrazione
and     det_ord_ven_stat.flag_trasferito='N'			
and     tes_ord_ven.data_registrazione <= :ldt_data_fine
and     det_ord_ven_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta ord_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_ord_ven;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta ord_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_ord_ven into :ls_cod_tipo_ord_ven, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_ven, &
										  :ll_prog_riga_ord_ven, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;   

   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_ven
   	into   :ls_flag_tipo_det_ven
	   from   tab_tipi_det_ven
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	   if ls_flag_tipo_det_ven = "M" or &
   	   ls_flag_tipo_det_ven = "C" or &
      	ls_flag_tipo_det_ven = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_ven = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="F" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_ord_ven) + fill('A',4 - len(string(ll_prog_riga_ord_ven))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_ord_ven;
			     return -1
			  end if
			  
			  UPDATE det_ord_ven_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_registrazione = :li_anno_registrazione 
			  AND    num_registrazione = :ll_num_registrazione 
			  AND    prog_riga_ordine_ven = :ll_prog_riga_ord_ven 
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_ord_ven;
			     return -1
			  end if

loop
close righe_tes_ord_ven;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

public function integer wf_esporta_fat_ven ();string  ls_cod_pagamento, ls_cod_tipo_fat_ven, ls_cod_tipo_det_ven, &
        ls_flag_tipo_det_ven, ls_lire,ls_cod_asa, &
	     ls_cod_tipo_analisi,ls_test, &
		  ls_cod_statistico_1, &
		  ls_cod_statistico_2, &
		  ls_cod_statistico_3, &
		  ls_cod_statistico_4, &
		  ls_cod_statistico_5, &
		  ls_cod_statistico_6, &
		  ls_cod_statistico_7, &
		  ls_cod_statistico_8, &
		  ls_cod_statistico_9, &
		  ls_cod_statistico_10
long    ll_i,ll_num_registrazione,ll_prog_riga_fat_ven,ll_progressivo
double  ldd_val_riga, ldd_sconto_testata, ldd_sconto_pagamento, &
        ldd_val_sconto_testata, ldd_val_riga_sconto_testata, &
        ldd_val_sconto_pagamento, ldd_val_riga_netto, ldd_tot_val_documento,ldd_valore
integer li_anno_registrazione,li_percentuale,li_fine,li_prog_stat
datetime    ldt_data_registrazione,ldt_data_fine

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
ldt_data_fine = datetime(date(em_data_fine.text))

select  max(progressivo)
into    :ll_progressivo
from 	  tab_asa
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_analisi=:ls_cod_tipo_analisi;

if isnull(ll_progressivo) then ll_progressivo=1

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", &
              exclamation!, ok!)
end if

declare righe_tes_fat_ven cursor for 
select  tes_fat_ven.cod_tipo_fat_ven,
		  tes_fat_ven.data_registrazione,
		  tes_fat_ven.cod_pagamento,
		  tes_fat_ven.sconto,
		  tes_fat_ven.anno_registrazione,
		  tes_fat_ven.num_registrazione,
		  det_fat_ven.quan_fatturata,
		  det_fat_ven.cod_tipo_det_ven,
		  det_fat_ven.prog_riga_fat_ven,
		  det_fat_ven_stat.cod_statistico_1,
		  det_fat_ven_stat.cod_statistico_2,
		  det_fat_ven_stat.cod_statistico_3,
		  det_fat_ven_stat.cod_statistico_4,
		  det_fat_ven_stat.cod_statistico_5,
		  det_fat_ven_stat.cod_statistico_6,
		  det_fat_ven_stat.cod_statistico_7,
		  det_fat_ven_stat.cod_statistico_8,
		  det_fat_ven_stat.cod_statistico_9,
		  det_fat_ven_stat.cod_statistico_10,		
		  det_fat_ven_stat.percentuale,
		  det_fat_ven_stat.progressivo
from	  tes_fat_ven,det_fat_ven,det_fat_ven_stat
where	  tes_fat_ven.cod_azienda=:s_cs_xx.cod_azienda
and     tes_fat_ven.cod_azienda = det_fat_ven.cod_azienda
and     tes_fat_ven.anno_registrazione = det_fat_ven.anno_registrazione
and 	  tes_fat_ven.num_registrazione = det_fat_ven.num_registrazione
and  	  tes_fat_ven.cod_azienda = det_fat_ven_stat.cod_azienda
and     tes_fat_ven.anno_registrazione = det_fat_ven_stat.anno_registrazione
and 	  tes_fat_ven.num_registrazione = det_fat_ven_stat.num_registrazione
and     det_fat_ven_stat.flag_trasferito='N'			
and     tes_fat_ven.data_registrazione <= :ldt_data_fine
and     det_fat_ven_stat.cod_tipo_analisi=:ls_cod_tipo_analisi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta fat_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

open righe_tes_fat_ven;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore Esporta fat_ven: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while 1 = 1
	fetch righe_tes_fat_ven into :ls_cod_tipo_fat_ven, & 
										  :ldt_data_registrazione, & 
										  :ls_cod_pagamento, &
										  :ldd_sconto_testata, &
									     :li_anno_registrazione, &
										  :ll_num_registrazione, &
										  :ldd_val_riga, &
										  :ls_cod_tipo_det_ven, &
										  :ll_prog_riga_fat_ven, &
										  :ls_cod_statistico_1, &
										  :ls_cod_statistico_2, &
										  :ls_cod_statistico_3, &
										  :ls_cod_statistico_4, &
										  :ls_cod_statistico_5, &
										  :ls_cod_statistico_6, &
										  :ls_cod_statistico_7, &
										  :ls_cod_statistico_8, &
										  :ls_cod_statistico_9, &
										  :ls_cod_statistico_10, &
										  :li_percentuale, &
										  :li_prog_stat;
   
   if sqlca.sqlcode <> 0 then exit
								
	if isnull(ldd_sconto_testata) then ldd_sconto_testata = 0

	select tab_pagamenti.sconto
	into   :ldd_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
	  	    tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
  		ldd_sconto_pagamento = 0
	end if	

	   select flag_tipo_det_ven
   	into   :ls_flag_tipo_det_ven
	   from   tab_tipi_det_ven
   	where  cod_azienda = :s_cs_xx.cod_azienda and 
      	    cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	   if ls_flag_tipo_det_ven = "M" or &
   	   ls_flag_tipo_det_ven = "C" or &
      	ls_flag_tipo_det_ven = "N" then
	      ldd_val_sconto_testata = (ldd_val_riga * ldd_sconto_testata) / 100
   	   ldd_val_riga_sconto_testata = ldd_val_riga - ldd_val_sconto_testata
      	ldd_val_sconto_pagamento = (ldd_val_riga_sconto_testata * ldd_sconto_pagamento) / 100
	      ldd_val_riga_netto = ldd_val_riga_sconto_testata - ldd_val_sconto_pagamento
   	elseif ls_flag_tipo_det_ven = "S" then
      	ldd_val_riga_netto = ldd_val_riga * -1
	   else
   	   ldd_val_riga_netto = ldd_val_riga
	   end if
		
			ll_progressivo++

			ldd_valore=(ldd_val_riga_netto*li_percentuale)/100
			ls_cod_asa="H" + string(li_anno_registrazione) + string(ll_num_registrazione) + fill('A',6 - len(string(ll_num_registrazione))) + string(ll_prog_riga_fat_ven) + fill('A',4 - len(string(ll_prog_riga_fat_ven))) + string(li_prog_stat) + fill('A',3 - len(string(li_prog_stat))) + "00"
			INSERT INTO tab_asa  
      	   		  (cod_azienda,   
		   	         cod_tipo_analisi,   
       			      progressivo,   
		         	   cod_asa,   
       		      	data_reg,   
			            data_scad,   
			            valore,   
			            cod_statistico_1,   
		   	         cod_statistico_2,   
       			      cod_statistico_3,   
		         	   cod_statistico_4,   
		            	cod_statistico_5,   
	       		      cod_statistico_6,   
			            cod_statistico_7,   
			            cod_statistico_8,   
       			      cod_statistico_9,   
		      	      cod_statistico_10 )  
		   VALUES ( :s_cs_xx.cod_azienda,   
   		         :ls_cod_tipo_analisi,   
      		      :ll_progressivo,   
         		   :ls_cod_asa,   
	         	   :ldt_data_registrazione,   
     	         	:ldt_data_registrazione,   
	      	      :ldd_valore,   
   	      	   :ls_cod_statistico_1,   
	   	         :ls_cod_statistico_2,   
   	   	      :ls_cod_statistico_3,   
      	   	   :ls_cod_statistico_4,   
         	   	:ls_cod_statistico_5,   
		            :ls_cod_statistico_6,   
   		         :ls_cod_statistico_7,   
      		      :ls_cod_statistico_8,   
         		   :ls_cod_statistico_9,   
	         	   :ls_cod_statistico_10 )  ;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_fat_ven;
			     return -1
			  end if

			  UPDATE det_fat_ven_stat
     		  SET    flag_trasferito = 'S'  
		     WHERE  cod_azienda = :s_cs_xx.cod_azienda  
			  AND    anno_registrazione = :li_anno_registrazione 
			  AND    num_registrazione = :ll_num_registrazione 
			  AND    prog_riga_fat_ven = :ll_prog_riga_fat_ven
			  AND    cod_tipo_analisi = :ls_cod_tipo_analisi 
			  AND    progressivo = :ll_progressivo;

			  if sqlca.sqlcode <> 0 then
				  close righe_tes_fat_ven;
			     return -1
			  end if

loop

close righe_tes_fat_ven;

UPDATE tab_tipi_analisi
SET    progressivo = :ll_progressivo  
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_tipo_analisi = :ls_cod_tipo_analisi;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 0
end function

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_cod_tipo_analisi, &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

on w_esporta_excel.create
int iCurrent
call w_cs_xx_principale::create
this.st_3=create st_3
this.st_1=create st_1
this.ddlb_cod_tipo_analisi=create ddlb_cod_tipo_analisi
this.cb_path_righe=create cb_path_righe
this.cb_path_stat_1=create cb_path_stat_1
this.cb_path_stat_2=create cb_path_stat_2
this.cb_path_stat_3=create cb_path_stat_3
this.cb_path_stat_4=create cb_path_stat_4
this.cb_path_stat_5=create cb_path_stat_5
this.cb_path_stat_6=create cb_path_stat_6
this.cb_path_stat_7=create cb_path_stat_7
this.cb_path_stat_8=create cb_path_stat_8
this.cb_path_stat_9=create cb_path_stat_9
this.cb_path_stat_10=create cb_path_stat_10
this.cb_esporta=create cb_esporta
this.cbx_ord_acq=create cbx_ord_acq
this.cbx_off_ven=create cbx_off_ven
this.cbx_bol_ven=create cbx_bol_ven
this.cbx_off_acq=create cbx_off_acq
this.gb_1=create gb_1
this.cbx_fat_acq=create cbx_fat_acq
this.em_data_fine=create em_data_fine
this.st_2=create st_2
this.dw_tab_path_esportazioni=create dw_tab_path_esportazioni
this.cbx_ord_ven=create cbx_ord_ven
this.cbx_bol_acq=create cbx_bol_acq
this.cbx_fat_ven=create cbx_fat_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_3
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=ddlb_cod_tipo_analisi
this.Control[iCurrent+4]=cb_path_righe
this.Control[iCurrent+5]=cb_path_stat_1
this.Control[iCurrent+6]=cb_path_stat_2
this.Control[iCurrent+7]=cb_path_stat_3
this.Control[iCurrent+8]=cb_path_stat_4
this.Control[iCurrent+9]=cb_path_stat_5
this.Control[iCurrent+10]=cb_path_stat_6
this.Control[iCurrent+11]=cb_path_stat_7
this.Control[iCurrent+12]=cb_path_stat_8
this.Control[iCurrent+13]=cb_path_stat_9
this.Control[iCurrent+14]=cb_path_stat_10
this.Control[iCurrent+15]=cb_esporta
this.Control[iCurrent+16]=cbx_ord_acq
this.Control[iCurrent+17]=cbx_off_ven
this.Control[iCurrent+18]=cbx_bol_ven
this.Control[iCurrent+19]=cbx_off_acq
this.Control[iCurrent+20]=gb_1
this.Control[iCurrent+21]=cbx_fat_acq
this.Control[iCurrent+22]=em_data_fine
this.Control[iCurrent+23]=st_2
this.Control[iCurrent+24]=dw_tab_path_esportazioni
this.Control[iCurrent+25]=cbx_ord_ven
this.Control[iCurrent+26]=cbx_bol_acq
this.Control[iCurrent+27]=cbx_fat_ven
end on

on w_esporta_excel.destroy
call w_cs_xx_principale::destroy
destroy(this.st_3)
destroy(this.st_1)
destroy(this.ddlb_cod_tipo_analisi)
destroy(this.cb_path_righe)
destroy(this.cb_path_stat_1)
destroy(this.cb_path_stat_2)
destroy(this.cb_path_stat_3)
destroy(this.cb_path_stat_4)
destroy(this.cb_path_stat_5)
destroy(this.cb_path_stat_6)
destroy(this.cb_path_stat_7)
destroy(this.cb_path_stat_8)
destroy(this.cb_path_stat_9)
destroy(this.cb_path_stat_10)
destroy(this.cb_esporta)
destroy(this.cbx_ord_acq)
destroy(this.cbx_off_ven)
destroy(this.cbx_bol_ven)
destroy(this.cbx_off_acq)
destroy(this.gb_1)
destroy(this.cbx_fat_acq)
destroy(this.em_data_fine)
destroy(this.st_2)
destroy(this.dw_tab_path_esportazioni)
destroy(this.cbx_ord_ven)
destroy(this.cbx_bol_acq)
destroy(this.cbx_fat_ven)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_path_esportazioni.set_dw_key("cod_azienda")
dw_tab_path_esportazioni.set_dw_key("cod_tipo_analisi")

dw_tab_path_esportazioni.set_dw_options(sqlca, c_nulldw,c_noretrieveonopen + c_nonew + c_nodelete,c_default)

iuo_dw_main = dw_tab_path_esportazioni

cb_path_righe.enabled=false
cb_path_stat_1.enabled=false
cb_path_stat_2.enabled=false
cb_path_stat_3.enabled=false
cb_path_stat_4.enabled=false
cb_path_stat_5.enabled=false
cb_path_stat_6.enabled=false
cb_path_stat_7.enabled=false
cb_path_stat_8.enabled=false
cb_path_stat_9.enabled=false
cb_path_stat_10.enabled=false

em_data_fine.text=string(today(),"dd/mm/yyyy")
end event

type st_3 from statictext within w_esporta_excel
int X=412
int Y=21
int Width=389
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_esporta_excel
int X=92
int Y=21
int Width=321
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Tipo Analisi:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_cod_tipo_analisi from dropdownlistbox within w_esporta_excel
event selectionchanged pbm_cbnselchange
int X=801
int Y=21
int Width=1235
int Height=1541
int TabOrder=140
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;string ls_label_livello_1, &
		 ls_label_livello_2, &
		 ls_label_livello_3, &
		 ls_label_livello_4, &
		 ls_label_livello_5, &
		 ls_label_livello_6, &
		 ls_label_livello_7, &
		 ls_label_livello_8, &
		 ls_label_livello_9, &
		 ls_label_livello_10
integer li_num_livelli

st_3.text = f_po_selectddlb(ddlb_cod_tipo_analisi)

parent.triggerevent("pc_retrieve")

select label_livello_1,
		 label_livello_2,
		 label_livello_3,
		 label_livello_4,
		 label_livello_5,
		 label_livello_6,
		 label_livello_7,
		 label_livello_8,
		 label_livello_9,
		 label_livello_10,
		 num_liv_gestiti
into   :ls_label_livello_1, 
		 :ls_label_livello_2, 
		 :ls_label_livello_3, 
		 :ls_label_livello_4, 
		 :ls_label_livello_5, 
		 :ls_label_livello_6, 
		 :ls_label_livello_7, 
		 :ls_label_livello_8, 
		 :ls_label_livello_9, 
		 :ls_label_livello_10,
		 :li_num_livelli
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_analisi=:st_3.text;

if isnull(ls_label_livello_1) then ls_label_livello_1=""
if isnull(ls_label_livello_2) then ls_label_livello_2=""
if isnull(ls_label_livello_3) then ls_label_livello_3=""
if isnull(ls_label_livello_4) then ls_label_livello_4=""
if isnull(ls_label_livello_5) then ls_label_livello_5=""
if isnull(ls_label_livello_6) then ls_label_livello_6=""
if isnull(ls_label_livello_7) then ls_label_livello_7=""
if isnull(ls_label_livello_8) then ls_label_livello_8=""
if isnull(ls_label_livello_9) then ls_label_livello_9=""
if isnull(ls_label_livello_10) then ls_label_livello_10=""

dw_tab_path_esportazioni.object.path_statistica_1_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_2_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_3_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_4_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_5_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_6_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_7_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_8_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_9_t.visible=0
dw_tab_path_esportazioni.object.path_statistica_10_t.visible=0

dw_tab_path_esportazioni.object.path_statistica_1.visible=0
dw_tab_path_esportazioni.object.path_statistica_2.visible=0
dw_tab_path_esportazioni.object.path_statistica_3.visible=0
dw_tab_path_esportazioni.object.path_statistica_4.visible=0
dw_tab_path_esportazioni.object.path_statistica_5.visible=0
dw_tab_path_esportazioni.object.path_statistica_6.visible=0
dw_tab_path_esportazioni.object.path_statistica_7.visible=0
dw_tab_path_esportazioni.object.path_statistica_8.visible=0
dw_tab_path_esportazioni.object.path_statistica_9.visible=0
dw_tab_path_esportazioni.object.path_statistica_10.visible=0

cb_path_stat_1.visible=false
cb_path_stat_2.visible=false
cb_path_stat_3.visible=false
cb_path_stat_4.visible=false
cb_path_stat_5.visible=false
cb_path_stat_6.visible=false
cb_path_stat_7.visible=false
cb_path_stat_8.visible=false
cb_path_stat_9.visible=false
cb_path_stat_10.visible=false

choose case li_num_livelli
	case 1
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		cb_path_stat_1.visible=true
	case 2
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
	case 3
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
	case 4
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
		cb_path_stat_4.visible=true
	case 5
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
		cb_path_stat_4.visible=true
		cb_path_stat_5.visible=true
	case 6
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
		cb_path_stat_4.visible=true
		cb_path_stat_5.visible=true
		cb_path_stat_6.visible=true
	case 7
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
		cb_path_stat_4.visible=true
		cb_path_stat_5.visible=true
		cb_path_stat_6.visible=true
		cb_path_stat_7.visible=true
	case 8
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_8_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7.visible=1
		dw_tab_path_esportazioni.object.path_statistica_8.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
		cb_path_stat_4.visible=true
		cb_path_stat_5.visible=true
		cb_path_stat_6.visible=true
		cb_path_stat_7.visible=true
		cb_path_stat_8.visible=true
	case 9
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_8_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_9_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7.visible=1
		dw_tab_path_esportazioni.object.path_statistica_8.visible=1
		dw_tab_path_esportazioni.object.path_statistica_9.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
		cb_path_stat_4.visible=true
		cb_path_stat_5.visible=true
		cb_path_stat_6.visible=true
		cb_path_stat_7.visible=true
		cb_path_stat_8.visible=true
		cb_path_stat_9.visible=true
	case 10
		dw_tab_path_esportazioni.object.path_statistica_1_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_8_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_9_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_10_t.visible=1
		dw_tab_path_esportazioni.object.path_statistica_1.visible=1
		dw_tab_path_esportazioni.object.path_statistica_2.visible=1
		dw_tab_path_esportazioni.object.path_statistica_3.visible=1
		dw_tab_path_esportazioni.object.path_statistica_4.visible=1
		dw_tab_path_esportazioni.object.path_statistica_5.visible=1
		dw_tab_path_esportazioni.object.path_statistica_6.visible=1
		dw_tab_path_esportazioni.object.path_statistica_7.visible=1
		dw_tab_path_esportazioni.object.path_statistica_8.visible=1
		dw_tab_path_esportazioni.object.path_statistica_9.visible=1
		dw_tab_path_esportazioni.object.path_statistica_10.visible=1
		cb_path_stat_1.visible=true
		cb_path_stat_2.visible=true
		cb_path_stat_3.visible=true
		cb_path_stat_4.visible=true
		cb_path_stat_5.visible=true
		cb_path_stat_6.visible=true
		cb_path_stat_7.visible=true
		cb_path_stat_8.visible=true
		cb_path_stat_9.visible=true
		cb_path_stat_10.visible=true
end choose

dw_tab_path_esportazioni.object.path_statistica_1_t.text=ls_label_livello_1
dw_tab_path_esportazioni.object.path_statistica_2_t.text=ls_label_livello_2
dw_tab_path_esportazioni.object.path_statistica_3_t.text=ls_label_livello_3
dw_tab_path_esportazioni.object.path_statistica_4_t.text=ls_label_livello_4
dw_tab_path_esportazioni.object.path_statistica_5_t.text=ls_label_livello_5
dw_tab_path_esportazioni.object.path_statistica_6_t.text=ls_label_livello_6
dw_tab_path_esportazioni.object.path_statistica_7_t.text=ls_label_livello_7
dw_tab_path_esportazioni.object.path_statistica_8_t.text=ls_label_livello_8
dw_tab_path_esportazioni.object.path_statistica_9_t.text=ls_label_livello_9
dw_tab_path_esportazioni.object.path_statistica_10_t.text=ls_label_livello_10
end event

type cb_path_righe from commandbutton within w_esporta_excel
int X=2881
int Y=141
int Width=69
int Height=81
int TabOrder=130
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_righe",ls_path)
end if

end event

type cb_path_stat_1 from commandbutton within w_esporta_excel
int X=2881
int Y=221
int Width=69
int Height=81
int TabOrder=120
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_1",ls_path)
end if

end event

type cb_path_stat_2 from commandbutton within w_esporta_excel
int X=2881
int Y=301
int Width=69
int Height=81
int TabOrder=110
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_2",ls_path)
end if

end event

type cb_path_stat_3 from commandbutton within w_esporta_excel
int X=2881
int Y=381
int Width=69
int Height=81
int TabOrder=100
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_3",ls_path)
end if

end event

type cb_path_stat_4 from commandbutton within w_esporta_excel
int X=2881
int Y=461
int Width=69
int Height=81
int TabOrder=90
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_4",ls_path)
end if

end event

type cb_path_stat_5 from commandbutton within w_esporta_excel
int X=2881
int Y=541
int Width=69
int Height=81
int TabOrder=80
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_5",ls_path)
end if

end event

type cb_path_stat_6 from commandbutton within w_esporta_excel
int X=2881
int Y=621
int Width=69
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_6",ls_path)
end if

end event

type cb_path_stat_7 from commandbutton within w_esporta_excel
int X=2881
int Y=701
int Width=69
int Height=81
int TabOrder=70
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_7",ls_path)
end if

end event

type cb_path_stat_8 from commandbutton within w_esporta_excel
int X=2881
int Y=781
int Width=69
int Height=81
int TabOrder=60
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_8",ls_path)
end if

end event

type cb_path_stat_9 from commandbutton within w_esporta_excel
int X=2881
int Y=861
int Width=69
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_9",ls_path)
end if

end event

type cb_path_stat_10 from commandbutton within w_esporta_excel
int X=2881
int Y=941
int Width=69
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "wks", "Lotus (*.wks),*.wks")

IF li_valore = 1 THEN 
	dw_tab_path_esportazioni.setitem(dw_tab_path_esportazioni.getrow(),"path_statistica_10",ls_path)
end if

end event

type cb_esporta from commandbutton within w_esporta_excel
int X=2606
int Y=1261
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Esporta"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;datastore lds_stat_1, &
			 lds_stat_2, &
			 lds_stat_3, &
			 lds_stat_4, &
			 lds_stat_5, &
			 lds_stat_6, &
			 lds_stat_7, &
			 lds_stat_8, &
			 lds_stat_9, &
			 lds_stat_10, &
			 lds_tab_asa
long ll_num_righe,ll_progressivo
string ls_cod_tipo_analisi
integer li_risposta

ls_cod_tipo_analisi=f_po_selectddlb(ddlb_cod_tipo_analisi)

if isnull(ls_cod_tipo_analisi) or ls_cod_tipo_analisi="" then
	g_mb.messagebox("Apice","Attenzione! Selezionare un tipo analisi.",exclamation!)
	return
end if

if not(cbx_off_acq.checked) and not(cbx_off_ven.checked) and not(cbx_ord_acq.checked) and not(cbx_bol_ven.checked) and not(cbx_fat_acq.checked) then
	g_mb.messagebox("Apice","Attenzione! Selezionare almeno una gestione da esportare.",exclamation!)
	return
end if

setpointer(hourglass!)

dw_tab_path_esportazioni.triggerevent("pcd_retrieve")

select progressivo
into   :ll_progressivo
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_analisi=:ls_cod_tipo_analisi;

if cbx_off_acq.checked=true then
	li_risposta = wf_esporta_off_acq()
end if

if cbx_ord_acq.checked=true then
	li_risposta = wf_esporta_ord_acq()
end if

if cbx_bol_acq.checked=true then
	li_risposta = wf_esporta_bol_acq()
end if

if cbx_fat_acq.checked=true then
	li_risposta = wf_esporta_fat_acq()
end if

if cbx_off_ven.checked=true then
	li_risposta = wf_esporta_off_ven()
end if

if cbx_ord_ven.checked=true then
	li_risposta = wf_esporta_ord_ven()
end if

if cbx_bol_ven.checked=true then
	li_risposta = wf_esporta_bol_ven()
end if

if cbx_fat_ven.checked=true then
	li_risposta = wf_esporta_fat_ven()
end if

lds_stat_1 = Create DataStore
lds_stat_2 = Create DataStore
lds_stat_3 = Create DataStore
lds_stat_4 = Create DataStore
lds_stat_5 = Create DataStore
lds_stat_6 = Create DataStore
lds_stat_7 = Create DataStore
lds_stat_8 = Create DataStore
lds_stat_9 = Create DataStore
lds_stat_10 = Create DataStore
lds_tab_asa = Create DataStore

lds_stat_1.DataObject = "d_tab_statistica_1_esporta"
lds_stat_2.DataObject = "d_tab_statistica_2_esporta"
lds_stat_3.DataObject = "d_tab_statistica_3_esporta"
lds_stat_4.DataObject = "d_tab_statistica_4_esporta"
lds_stat_5.DataObject = "d_tab_statistica_5_esporta"
lds_stat_6.DataObject = "d_tab_statistica_6_esporta"
lds_stat_7.DataObject = "d_tab_statistica_7_esporta"
lds_stat_8.DataObject = "d_tab_statistica_8_esporta"
lds_stat_9.DataObject = "d_tab_statistica_9_esporta"
lds_stat_10.DataObject = "d_tab_statistica_10_esporta"
lds_tab_asa.DataObject = "d_tab_asa"

lds_stat_1.SetTransObject(sqlca)
lds_stat_2.SetTransObject(sqlca)
lds_stat_3.SetTransObject(sqlca)
lds_stat_4.SetTransObject(sqlca)
lds_stat_5.SetTransObject(sqlca)
lds_stat_6.SetTransObject(sqlca)
lds_stat_7.SetTransObject(sqlca)
lds_stat_8.SetTransObject(sqlca)
lds_stat_9.SetTransObject(sqlca)
lds_stat_10.SetTransObject(sqlca)
lds_tab_asa.SetTransObject(sqlca)

ll_num_righe = lds_stat_1.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_2.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_3.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_4.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_5.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_6.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_7.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_8.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_9.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_stat_10.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)
ll_num_righe = lds_tab_asa.Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi,ll_progressivo)

lds_stat_1.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_1"),wks!, FALSE)
lds_stat_2.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_2"),wks!, FALSE)
lds_stat_3.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_3"),wks!, FALSE)
lds_stat_4.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_4"),wks!, FALSE)
lds_stat_5.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_5"),wks!, FALSE)
lds_stat_6.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_6"),wks!, FALSE)
lds_stat_7.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_7"),wks!, FALSE)
lds_stat_8.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_8"),wks!, FALSE)
lds_stat_9.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_9"),wks!, FALSE)
lds_stat_10.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_statistica_10"),wks!, FALSE)
lds_tab_asa.SaveAs(dw_tab_path_esportazioni.getitemstring(dw_tab_path_esportazioni.getrow(),"path_righe"),wks!, FALSE)

setpointer(arrow!)
end event

type cbx_ord_acq from checkbox within w_esporta_excel
int X=595
int Y=1121
int Width=508
int Height=77
string Text="Ordini Acquisto"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_off_ven from checkbox within w_esporta_excel
int X=46
int Y=1221
int Width=508
int Height=77
string Text="Offerte Vendita"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_bol_ven from checkbox within w_esporta_excel
int X=1212
int Y=1221
int Width=508
int Height=77
string Text="Bolle Vendita"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_off_acq from checkbox within w_esporta_excel
int X=46
int Y=1121
int Width=508
int Height=77
string Text="Offerte Acquisto"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_esporta_excel
int X=23
int Y=1061
int Width=2332
int Height=281
int TabOrder=30
string Text="Gestioni Esportabili"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_fat_acq from checkbox within w_esporta_excel
int X=1806
int Y=1121
int Width=508
int Height=77
boolean BringToTop=true
string Text="Fatture Acquisto"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_data_fine from editmask within w_esporta_excel
int X=2583
int Y=1161
int Width=389
int Height=81
int TabOrder=31
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="D"
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_esporta_excel
int X=2378
int Y=1081
int Width=595
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Data Fine Esportazione"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_tab_path_esportazioni from uo_cs_xx_dw within w_esporta_excel
int X=23
int Y=121
int Width=2949
int Height=921
int TabOrder=150
string DataObject="d_tab_path_esportazioni"
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_tipo_analisi,ls_etichetta

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_path_righe.enabled=true
	cb_path_stat_1.enabled=true
	cb_path_stat_2.enabled=true
	cb_path_stat_3.enabled=true
	cb_path_stat_4.enabled=true
	cb_path_stat_5.enabled=true
	cb_path_stat_6.enabled=true
	cb_path_stat_7.enabled=true
	cb_path_stat_8.enabled=true
	cb_path_stat_9.enabled=true
	cb_path_stat_10.enabled=true
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	cb_path_righe.enabled=false
	cb_path_stat_1.enabled=false
	cb_path_stat_2.enabled=false
	cb_path_stat_3.enabled=false
	cb_path_stat_4.enabled=false
	cb_path_stat_5.enabled=false
	cb_path_stat_6.enabled=false
	cb_path_stat_7.enabled=false
	cb_path_stat_8.enabled=false
	cb_path_stat_9.enabled=false
	cb_path_stat_10.enabled=false
end if

end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_path_righe.enabled=false
	cb_path_stat_1.enabled=false
	cb_path_stat_2.enabled=false
	cb_path_stat_3.enabled=false
	cb_path_stat_4.enabled=false
	cb_path_stat_5.enabled=false
	cb_path_stat_6.enabled=false
	cb_path_stat_7.enabled=false
	cb_path_stat_8.enabled=false
	cb_path_stat_9.enabled=false
	cb_path_stat_10.enabled=false
end if
end event

type cbx_ord_ven from checkbox within w_esporta_excel
int X=595
int Y=1221
int Width=503
int Height=81
boolean BringToTop=true
string Text="Ordini Vendita"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_bol_acq from checkbox within w_esporta_excel
int X=1212
int Y=1121
int Width=503
int Height=81
boolean BringToTop=true
string Text="Bolle Acquisto"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_fat_ven from checkbox within w_esporta_excel
int X=1806
int Y=1221
int Width=503
int Height=81
boolean BringToTop=true
string Text="Fatture Vendita"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

