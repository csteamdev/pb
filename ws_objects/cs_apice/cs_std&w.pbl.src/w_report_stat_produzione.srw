﻿$PBExportHeader$w_report_stat_produzione.srw
forward
global type w_report_stat_produzione from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_report_stat_produzione
end type
type st_4 from statictext within w_report_stat_produzione
end type
type st_message from statictext within w_report_stat_produzione
end type
type dw_selezione from uo_std_dw within w_report_stat_produzione
end type
type dw_elenco_prodotti_stat_prod from uo_std_dw within w_report_stat_produzione
end type
type st_2 from statictext within w_report_stat_produzione
end type
type ddlb_flag_evasione from dropdownlistbox within w_report_stat_produzione
end type
type cb_resetta from commandbutton within w_report_stat_produzione
end type
type dw_elenco_prodotti_stat from uo_cs_xx_dw within w_report_stat_produzione
end type
type cb_stampa from commandbutton within w_report_stat_produzione
end type
type ddlb_cod_stat_10_10 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_9_9 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_8_8 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_7_7 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_6_6 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_5_5 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_4_4 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_3_3 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_2_2 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_1_1 from dropdownlistbox within w_report_stat_produzione
end type
type cb_ricerca from commandbutton within w_report_stat_produzione
end type
type em_a_data from editmask within w_report_stat_produzione
end type
type st_6 from statictext within w_report_stat_produzione
end type
type em_da_data from editmask within w_report_stat_produzione
end type
type st_5 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_7 from dropdownlistbox within w_report_stat_produzione
end type
type et7 from statictext within w_report_stat_produzione
end type
type et10 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_10 from dropdownlistbox within w_report_stat_produzione
end type
type et9 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_9 from dropdownlistbox within w_report_stat_produzione
end type
type et8 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_8 from dropdownlistbox within w_report_stat_produzione
end type
type et6 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_6 from dropdownlistbox within w_report_stat_produzione
end type
type et5 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_5 from dropdownlistbox within w_report_stat_produzione
end type
type et4 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_4 from dropdownlistbox within w_report_stat_produzione
end type
type et3 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_3 from dropdownlistbox within w_report_stat_produzione
end type
type ddlb_cod_stat_2 from dropdownlistbox within w_report_stat_produzione
end type
type et2 from statictext within w_report_stat_produzione
end type
type ddlb_cod_stat_1 from dropdownlistbox within w_report_stat_produzione
end type
type et1 from statictext within w_report_stat_produzione
end type
type st_1 from statictext within w_report_stat_produzione
end type
type ddlb_cod_tipo_analisi from dropdownlistbox within w_report_stat_produzione
end type
type st_3 from statictext within w_report_stat_produzione
end type
end forward

global type w_report_stat_produzione from w_cs_xx_principale
integer width = 3950
integer height = 2956
string title = "Report Statistiche Produzione"
cb_1 cb_1
st_4 st_4
st_message st_message
dw_selezione dw_selezione
dw_elenco_prodotti_stat_prod dw_elenco_prodotti_stat_prod
st_2 st_2
ddlb_flag_evasione ddlb_flag_evasione
cb_resetta cb_resetta
dw_elenco_prodotti_stat dw_elenco_prodotti_stat
cb_stampa cb_stampa
ddlb_cod_stat_10_10 ddlb_cod_stat_10_10
ddlb_cod_stat_9_9 ddlb_cod_stat_9_9
ddlb_cod_stat_8_8 ddlb_cod_stat_8_8
ddlb_cod_stat_7_7 ddlb_cod_stat_7_7
ddlb_cod_stat_6_6 ddlb_cod_stat_6_6
ddlb_cod_stat_5_5 ddlb_cod_stat_5_5
ddlb_cod_stat_4_4 ddlb_cod_stat_4_4
ddlb_cod_stat_3_3 ddlb_cod_stat_3_3
ddlb_cod_stat_2_2 ddlb_cod_stat_2_2
ddlb_cod_stat_1_1 ddlb_cod_stat_1_1
cb_ricerca cb_ricerca
em_a_data em_a_data
st_6 st_6
em_da_data em_da_data
st_5 st_5
ddlb_cod_stat_7 ddlb_cod_stat_7
et7 et7
et10 et10
ddlb_cod_stat_10 ddlb_cod_stat_10
et9 et9
ddlb_cod_stat_9 ddlb_cod_stat_9
et8 et8
ddlb_cod_stat_8 ddlb_cod_stat_8
et6 et6
ddlb_cod_stat_6 ddlb_cod_stat_6
et5 et5
ddlb_cod_stat_5 ddlb_cod_stat_5
et4 et4
ddlb_cod_stat_4 ddlb_cod_stat_4
et3 et3
ddlb_cod_stat_3 ddlb_cod_stat_3
ddlb_cod_stat_2 ddlb_cod_stat_2
et2 et2
ddlb_cod_stat_1 ddlb_cod_stat_1
et1 et1
st_1 st_1
ddlb_cod_tipo_analisi ddlb_cod_tipo_analisi
st_3 st_3
end type
global w_report_stat_produzione w_report_stat_produzione

type variables
decimal	id_tot_minuti_attrezzaggio, id_tot_minuti_attrezzaggio_commessa,id_tot_minuti_lavorazione, & 
			id_tot_minuti_risorsa_umana 
			
			
datetime idt_da_data, idt_a_data

string   is_1, is_2, is_3, is_4, is_5, is_6, is_7, is_8, is_9, is_10
end variables

forward prototypes
public function integer wf_calcola_tempi_fase (string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto, string fs_cod_operaio, datetime fdt_da_data, datetime fdt_a_data, ref decimal fd_minuti_attrezzaggio, ref decimal fd_minuti_attrezzaggio_commessa, ref decimal fd_minuti_lavorazione, ref decimal fd_minuti_risorsa_umana)
end prototypes

public function integer wf_calcola_tempi_fase (string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto, string fs_cod_operaio, datetime fdt_da_data, datetime fdt_a_data, ref decimal fd_minuti_attrezzaggio, ref decimal fd_minuti_attrezzaggio_commessa, ref decimal fd_minuti_lavorazione, ref decimal fd_minuti_risorsa_umana);string   ls_giorno,ls_giorno_it,ls_problemi_produzione, & 
			ls_cognome,ls_errore,ls_cod_prodotto_test,ls_cod_operaio
datetime ldt_data_corrente,lt_orario_attrezzaggio,lt_orario_attrezzaggio_commessa, &
			lt_orario_lavorazione,lt_orario_risorsa_umana,lt_orario_attrezzaggio_fine,lt_orario_attrezzaggio_commessa_fine, &
			lt_orario_lavorazione_fine,lt_orario_risorsa_umana_fine
decimal  ld_secondi,ld_minuti_risorsa_umana,ld_ore,ld_quan_prodotta,ld_minuti_lavorazione,ld_minuti_attrezzaggio, & 
			ld_minuti_attrezzaggio_commessa,ld_ore_lavorazione,ld_ore_attrezzaggio,ld_ore_attrezzaggio_commessa, & 
			ld_somma_tempo_attrezzaggio,ld_somma_tempo_attrezzaggio_commessa,ld_somma_tempo_lavorazione
integer  li_risposta
long     ll_num_anno_commessa,ll_num_num_commessa,ll_num_commesse,ll_num_orari

select count(*)
into   :ll_num_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
		
if ll_num_orari = 0 then return 1

		
ldt_data_corrente = fdt_da_data

do while ldt_data_corrente <= fdt_a_data
	
	declare r_det_orari cursor for 
	select orario_attrezzaggio,
			 orario_attrezzaggio_commessa,
			 orario_lavorazione,
			 orario_risorsa_umana,
			 cod_operaio
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    data_giorno = :ldt_data_corrente
	and    flag_inizio = 'S'
	and    cod_reparto = :fs_cod_reparto
	and    cod_lavorazione=:fs_cod_lavorazione
	and    cod_prodotto=:fs_cod_prodotto
	and    cod_operaio like :fs_cod_operaio
	order by prog_orari;
	
	open r_det_orari;
	
	do while 1 = 1 
	
		fetch r_det_orari
		into  :lt_orario_attrezzaggio,
				:lt_orario_attrezzaggio_commessa,
				:lt_orario_lavorazione,
				:lt_orario_risorsa_umana,
				:ls_cod_operaio;

		
		if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_orari;
			return -1
		end if
	
		if time(lt_orario_attrezzaggio) <> 00:00:00 then

			declare r_d1 cursor for 
			select orario_attrezzaggio
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_attrezzaggio is not null
			and    orario_attrezzaggio > :lt_orario_attrezzaggio
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto
			order by orario_attrezzaggio;
			
			open r_d1;
			
			fetch r_d1
			into  :lt_orario_attrezzaggio_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d1;
				close r_det_orari;
				return -1
			end if
			
			close r_d1;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio),time(lt_orario_attrezzaggio_fine))
			ld_minuti_attrezzaggio = ld_minuti_attrezzaggio + ld_secondi/60
			ld_secondi=0
		end if	

		if time(lt_orario_attrezzaggio_commessa) <> 00:00:00 then
			declare r_d2 cursor for 
			select orario_attrezzaggio_commessa
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_attrezzaggio_commessa is not null
			and    orario_attrezzaggio_commessa > :lt_orario_attrezzaggio_commessa
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto

			order by orario_attrezzaggio_commessa;
			
			open r_d2;
			
			fetch r_d2
			into  :lt_orario_attrezzaggio_commessa_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d2;
				close r_det_orari;
				return -1
			end if
			
			close r_d2;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio_commessa),time(lt_orario_attrezzaggio_commessa_fine))
			ld_minuti_attrezzaggio_commessa = ld_minuti_attrezzaggio_commessa + ld_secondi/60
			ld_secondi = 0
		end if	

		if time(lt_orario_lavorazione) <> 00:00:00  then
			declare r_d3 cursor for 
			select orario_lavorazione,
					 problemi_produzione
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_lavorazione is not null
			and    orario_lavorazione > :lt_orario_lavorazione
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto

			order by orario_lavorazione;
			
			open r_d3;
			
			fetch r_d3
			into  :lt_orario_lavorazione_fine,
					:ls_problemi_produzione;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d3;
				close r_det_orari;
				return -1
			end if 
			
			close r_d3;
			if ls_problemi_produzione ='N' or isnull(ls_problemi_produzione) then
				ld_secondi = SecondsAfter ( time(lt_orario_lavorazione),time(lt_orario_lavorazione_fine))
				ld_minuti_lavorazione = ld_minuti_lavorazione + ld_secondi/60			
			end if
		end if	

		if time(lt_orario_risorsa_umana) <> 00:00:00 then
			declare r_d4 cursor for 
			select orario_risorsa_umana
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio = :ls_cod_operaio
			and    orario_risorsa_umana is not null
			and    orario_risorsa_umana >= :lt_orario_risorsa_umana
			order by orario_risorsa_umana;
			
			open r_d4;
			
			fetch r_d4
			into  :lt_orario_risorsa_umana_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d4;
				close r_det_orari;
				return -1
			end if
			
			close r_d4;
			ld_secondi = SecondsAfter ( time(lt_orario_risorsa_umana),time(lt_orario_risorsa_umana_fine))
			ld_minuti_risorsa_umana = ld_minuti_risorsa_umana + ld_secondi/60			
		
		end if	

	loop
	
	
	close r_det_orari;


	ldt_data_corrente = datetime(RelativeDate ( date(ldt_data_corrente) , 1 ),00:00:00)
		
loop

select sum(quan_prodotta)
into   :ld_quan_prodotta
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select count(distinct anno_commessa)
into   :ll_num_anno_commessa
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select count(distinct num_commessa)
into   :ll_num_num_commessa
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if ll_num_num_commessa > ll_num_anno_commessa then
	ll_num_commesse = ll_num_num_commessa
else
	ll_num_commesse = ll_num_anno_commessa
end if

if ll_num_commesse>0 then
	fd_minuti_attrezzaggio = round(ld_minuti_attrezzaggio/ll_num_commesse,4)
	fd_minuti_attrezzaggio_commessa = round(ld_minuti_attrezzaggio_commessa/ll_num_commesse,4)
end if

if ld_quan_prodotta > 0 then
	fd_minuti_lavorazione = round(ld_minuti_lavorazione/ld_quan_prodotta,4)
	fd_minuti_risorsa_umana = round(ld_minuti_risorsa_umana/ld_quan_prodotta,4)
end if



return 0
end function

on w_report_stat_produzione.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.st_4=create st_4
this.st_message=create st_message
this.dw_selezione=create dw_selezione
this.dw_elenco_prodotti_stat_prod=create dw_elenco_prodotti_stat_prod
this.st_2=create st_2
this.ddlb_flag_evasione=create ddlb_flag_evasione
this.cb_resetta=create cb_resetta
this.dw_elenco_prodotti_stat=create dw_elenco_prodotti_stat
this.cb_stampa=create cb_stampa
this.ddlb_cod_stat_10_10=create ddlb_cod_stat_10_10
this.ddlb_cod_stat_9_9=create ddlb_cod_stat_9_9
this.ddlb_cod_stat_8_8=create ddlb_cod_stat_8_8
this.ddlb_cod_stat_7_7=create ddlb_cod_stat_7_7
this.ddlb_cod_stat_6_6=create ddlb_cod_stat_6_6
this.ddlb_cod_stat_5_5=create ddlb_cod_stat_5_5
this.ddlb_cod_stat_4_4=create ddlb_cod_stat_4_4
this.ddlb_cod_stat_3_3=create ddlb_cod_stat_3_3
this.ddlb_cod_stat_2_2=create ddlb_cod_stat_2_2
this.ddlb_cod_stat_1_1=create ddlb_cod_stat_1_1
this.cb_ricerca=create cb_ricerca
this.em_a_data=create em_a_data
this.st_6=create st_6
this.em_da_data=create em_da_data
this.st_5=create st_5
this.ddlb_cod_stat_7=create ddlb_cod_stat_7
this.et7=create et7
this.et10=create et10
this.ddlb_cod_stat_10=create ddlb_cod_stat_10
this.et9=create et9
this.ddlb_cod_stat_9=create ddlb_cod_stat_9
this.et8=create et8
this.ddlb_cod_stat_8=create ddlb_cod_stat_8
this.et6=create et6
this.ddlb_cod_stat_6=create ddlb_cod_stat_6
this.et5=create et5
this.ddlb_cod_stat_5=create ddlb_cod_stat_5
this.et4=create et4
this.ddlb_cod_stat_4=create ddlb_cod_stat_4
this.et3=create et3
this.ddlb_cod_stat_3=create ddlb_cod_stat_3
this.ddlb_cod_stat_2=create ddlb_cod_stat_2
this.et2=create et2
this.ddlb_cod_stat_1=create ddlb_cod_stat_1
this.et1=create et1
this.st_1=create st_1
this.ddlb_cod_tipo_analisi=create ddlb_cod_tipo_analisi
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.st_4
this.Control[iCurrent+3]=this.st_message
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_elenco_prodotti_stat_prod
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.ddlb_flag_evasione
this.Control[iCurrent+8]=this.cb_resetta
this.Control[iCurrent+9]=this.dw_elenco_prodotti_stat
this.Control[iCurrent+10]=this.cb_stampa
this.Control[iCurrent+11]=this.ddlb_cod_stat_10_10
this.Control[iCurrent+12]=this.ddlb_cod_stat_9_9
this.Control[iCurrent+13]=this.ddlb_cod_stat_8_8
this.Control[iCurrent+14]=this.ddlb_cod_stat_7_7
this.Control[iCurrent+15]=this.ddlb_cod_stat_6_6
this.Control[iCurrent+16]=this.ddlb_cod_stat_5_5
this.Control[iCurrent+17]=this.ddlb_cod_stat_4_4
this.Control[iCurrent+18]=this.ddlb_cod_stat_3_3
this.Control[iCurrent+19]=this.ddlb_cod_stat_2_2
this.Control[iCurrent+20]=this.ddlb_cod_stat_1_1
this.Control[iCurrent+21]=this.cb_ricerca
this.Control[iCurrent+22]=this.em_a_data
this.Control[iCurrent+23]=this.st_6
this.Control[iCurrent+24]=this.em_da_data
this.Control[iCurrent+25]=this.st_5
this.Control[iCurrent+26]=this.ddlb_cod_stat_7
this.Control[iCurrent+27]=this.et7
this.Control[iCurrent+28]=this.et10
this.Control[iCurrent+29]=this.ddlb_cod_stat_10
this.Control[iCurrent+30]=this.et9
this.Control[iCurrent+31]=this.ddlb_cod_stat_9
this.Control[iCurrent+32]=this.et8
this.Control[iCurrent+33]=this.ddlb_cod_stat_8
this.Control[iCurrent+34]=this.et6
this.Control[iCurrent+35]=this.ddlb_cod_stat_6
this.Control[iCurrent+36]=this.et5
this.Control[iCurrent+37]=this.ddlb_cod_stat_5
this.Control[iCurrent+38]=this.et4
this.Control[iCurrent+39]=this.ddlb_cod_stat_4
this.Control[iCurrent+40]=this.et3
this.Control[iCurrent+41]=this.ddlb_cod_stat_3
this.Control[iCurrent+42]=this.ddlb_cod_stat_2
this.Control[iCurrent+43]=this.et2
this.Control[iCurrent+44]=this.ddlb_cod_stat_1
this.Control[iCurrent+45]=this.et1
this.Control[iCurrent+46]=this.st_1
this.Control[iCurrent+47]=this.ddlb_cod_tipo_analisi
this.Control[iCurrent+48]=this.st_3
end on

on w_report_stat_produzione.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.st_4)
destroy(this.st_message)
destroy(this.dw_selezione)
destroy(this.dw_elenco_prodotti_stat_prod)
destroy(this.st_2)
destroy(this.ddlb_flag_evasione)
destroy(this.cb_resetta)
destroy(this.dw_elenco_prodotti_stat)
destroy(this.cb_stampa)
destroy(this.ddlb_cod_stat_10_10)
destroy(this.ddlb_cod_stat_9_9)
destroy(this.ddlb_cod_stat_8_8)
destroy(this.ddlb_cod_stat_7_7)
destroy(this.ddlb_cod_stat_6_6)
destroy(this.ddlb_cod_stat_5_5)
destroy(this.ddlb_cod_stat_4_4)
destroy(this.ddlb_cod_stat_3_3)
destroy(this.ddlb_cod_stat_2_2)
destroy(this.ddlb_cod_stat_1_1)
destroy(this.cb_ricerca)
destroy(this.em_a_data)
destroy(this.st_6)
destroy(this.em_da_data)
destroy(this.st_5)
destroy(this.ddlb_cod_stat_7)
destroy(this.et7)
destroy(this.et10)
destroy(this.ddlb_cod_stat_10)
destroy(this.et9)
destroy(this.ddlb_cod_stat_9)
destroy(this.et8)
destroy(this.ddlb_cod_stat_8)
destroy(this.et6)
destroy(this.ddlb_cod_stat_6)
destroy(this.et5)
destroy(this.ddlb_cod_stat_5)
destroy(this.et4)
destroy(this.ddlb_cod_stat_4)
destroy(this.et3)
destroy(this.ddlb_cod_stat_3)
destroy(this.ddlb_cod_stat_2)
destroy(this.et2)
destroy(this.ddlb_cod_stat_1)
destroy(this.et1)
destroy(this.st_1)
destroy(this.ddlb_cod_tipo_analisi)
destroy(this.st_3)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_prodotti_stat.ib_dw_report = true
dw_elenco_prodotti_stat_prod.ib_dw_report = true

//dw_elenco_prodotti_stat.set_dw_options(sqlca, pcca.null_object,c_nonew + c_nodelete + c_nomodify + c_noretrieveonopen,c_default)
dw_elenco_prodotti_stat.SetTrans(sqlca)
dw_elenco_prodotti_stat_prod.setTrans(sqlca)

//iuo_dw_main = dw_elenco_prodotti_stat_prod

em_da_data.text = string(today())
//em_da_data.text = string(relativedate(today(),-365))
em_a_data.text = string(relativedate(today(),30))

dw_selezione.insertrow(0)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_cod_tipo_analisi, &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")

//f_po_loaddddw_dw(dw_tab_des_variabili_lista, &
//                 "cod_variabile", &
//                 sqlca, &
//                 "tab_variabili_formule", &
//                 "cod_variabile", &
//                 "isnull(nome_campo_database, cod_variabile) ", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_po_loaddddw_dw ( dw_selezione, "as_reparto_1", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_2", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_3", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_4", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_5", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

end event

type cb_1 from commandbutton within w_report_stat_produzione
integer x = 3497
integer y = 1580
integer width = 366
integer height = 80
integer taborder = 260
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "PDF"
end type

event clicked;string ls_path
long ll_ret
uo_shellexecute luo_exec

ls_path = guo_functions.uof_get_user_documents_folder( ) + guo_functions.uof_get_random_filename( ) + ".pdf"
dw_elenco_prodotti_stat_prod.saveas(  ls_path , PDF!, true)
g_mb.show( "Il file è stato salvato nella cartella DOCUMENTI dell'utente.~r~n" + ls_path)

end event

type st_4 from statictext within w_report_stat_produzione
integer x = 2377
integer y = 1360
integer width = 937
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
string text = "Vuoto=Solo Ordini Aperti e Parziali"
boolean focusrectangle = false
end type

type st_message from statictext within w_report_stat_produzione
integer x = 23
integer y = 2740
integer width = 3863
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type dw_selezione from uo_std_dw within w_report_stat_produzione
integer x = 23
integer y = 1440
integer width = 3200
integer height = 520
integer taborder = 240
string dataobject = "d_report_stat_produzione_selezione"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event itemchanged;call super::itemchanged;choose case dwo.name
		
		
	case "b_attrezzatura"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione, "cod_attrezzatura")
		
		

end choose
end event

type dw_elenco_prodotti_stat_prod from uo_std_dw within w_report_stat_produzione
integer x = 23
integer y = 1980
integer width = 3863
integer height = 740
integer taborder = 240
string dataobject = "d_ext_prodotti_stat_prod_st"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event doubleclicked;
if not isnull(dwo) then
	if dwo.name="num_commessa" then
	
		long	ll_anno_commessa, ll_num_commessa
		s_cs_xx_parametri				lstr_param
		
		ll_anno_commessa = getitemnumber(row, "anno_commessa")
		ll_num_commessa = getitemnumber(row, "num_commessa")
		
		lstr_param.parametro_ul_1=ll_anno_commessa
		lstr_param.parametro_ul_2=ll_num_commessa
		
		window_open_parm(w_commesse_produzione_tv, -1, lstr_param)
		
	elseif left(dwo.name,13) = "quan_reparto_" then
		
			s_cs_xx.parametri.parametro_i_1 = getitemnumber(row, "anno_commessa")
			s_cs_xx.parametri.parametro_dec4_1 = getitemnumber(row, "num_commessa")
			s_cs_xx.parametri.parametro_i_2 = 1
			
			window_open(w_report_bl_attivazione,-1)
		
	else
		if ib_dw_report then
			event ue_anteprima()
		end if
	end if	
end if

end event

type st_2 from statictext within w_report_stat_produzione
integer y = 1360
integer width = 229
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Da data"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_flag_evasione from dropdownlistbox within w_report_stat_produzione
integer x = 1806
integer y = 1340
integer width = 549
integer height = 360
integer taborder = 260
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean sorted = false
boolean vscrollbar = true
string item[] = {"","Aperto","Parziale","Evaso"}
end type

type cb_resetta from commandbutton within w_report_stat_produzione
integer x = 2286
integer y = 20
integer width = 366
integer height = 80
integer taborder = 240
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;ddlb_cod_stat_1.reset()
ddlb_cod_stat_1.text=""

ddlb_cod_stat_2.reset()
ddlb_cod_stat_2.text=""

ddlb_cod_stat_3.reset()
ddlb_cod_stat_3.text=""

ddlb_cod_stat_4.reset()
ddlb_cod_stat_4.text=""

ddlb_cod_stat_5.reset()
ddlb_cod_stat_5.text=""

ddlb_cod_stat_6.reset()
ddlb_cod_stat_6.text=""

ddlb_cod_stat_7.reset()
ddlb_cod_stat_7.text=""

ddlb_cod_stat_8.reset()
ddlb_cod_stat_8.text=""

ddlb_cod_stat_9.reset()
ddlb_cod_stat_9.text=""

ddlb_cod_stat_10.reset()
ddlb_cod_stat_10.text=""


ddlb_cod_stat_1_1.reset()
ddlb_cod_stat_1_1.text=""

ddlb_cod_stat_2_2.reset()
ddlb_cod_stat_2_2.text=""

ddlb_cod_stat_3_3.reset()
ddlb_cod_stat_3_3.text=""

ddlb_cod_stat_4_4.reset()
ddlb_cod_stat_4_4.text=""

ddlb_cod_stat_5_5.reset()
ddlb_cod_stat_5_5.text=""

ddlb_cod_stat_6_6.reset()
ddlb_cod_stat_6_6.text=""

ddlb_cod_stat_7_7.reset()
ddlb_cod_stat_7_7.text=""

ddlb_cod_stat_8_8.reset()
ddlb_cod_stat_8_8.text=""

ddlb_cod_stat_9_9.reset()
ddlb_cod_stat_9_9.text=""

ddlb_cod_stat_10_10.reset()
ddlb_cod_stat_10_10.text=""

ddlb_cod_tipo_analisi.reset()
ddlb_cod_tipo_analisi.text=""

f_po_loadddlb(ddlb_cod_tipo_analisi, &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")


dw_elenco_prodotti_stat_prod.reset()
dw_elenco_prodotti_stat_prod.Object.des_statistico_1_t.Text = ""
dw_elenco_prodotti_stat_prod.Object.des_statistico_2_t.Text = ""
dw_elenco_prodotti_stat_prod.Object.des_statistico_3_t.Text = ""
dw_elenco_prodotti_stat_prod.Object.des_statistico_4_t.Text = ""
dw_elenco_prodotti_stat_prod.Object.des_statistico_5_t.Text = ""
dw_elenco_prodotti_stat_prod.Object.des_statistico_6_t.Text = ""

et1.text = ""
et2.text = ""
et3.text = ""
et4.text = ""
et5.text = ""
et6.text = ""
et7.text = ""
et8.text = ""
et9.text = ""
et10.text = ""

em_da_data.text = string(today())
em_a_data.text = string(relativedate(today(),30))

dw_elenco_prodotti_stat.reset()
end event

type dw_elenco_prodotti_stat from uo_cs_xx_dw within w_report_stat_produzione
event pcd_retrieve pbm_custom60
integer x = 3246
integer y = 20
integer width = 640
integer height = 1292
string title = "none"
string dataobject = "d_prodotti_stat"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve();long l_error


l_error = dw_elenco_prodotti_stat.retrieve(s_cs_xx.cod_azienda,idt_da_data,idt_a_data)


IF l_error < 0 THEN
   PCCA.Error = c_Fatal
	return
END IF
end event

type cb_stampa from commandbutton within w_report_stat_produzione
integer x = 3497
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 250
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;dw_elenco_prodotti_stat_prod.print()
end event

type ddlb_cod_stat_10_10 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 1220
integer width = 1303
integer height = 1260
integer taborder = 210
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_9_9 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 1100
integer width = 1303
integer height = 1260
integer taborder = 190
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_8_8 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 980
integer width = 1303
integer height = 1260
integer taborder = 170
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_7_7 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 860
integer width = 1303
integer height = 1260
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_6_6 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 740
integer width = 1303
integer height = 1260
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_5_5 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 620
integer width = 1303
integer height = 1260
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_4_4 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 500
integer width = 1303
integer height = 1260
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_3_3 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 380
integer width = 1303
integer height = 1260
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_2_2 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 260
integer width = 1303
integer height = 1260
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_1_1 from dropdownlistbox within w_report_stat_produzione
integer x = 1920
integer y = 140
integer width = 1303
integer height = 1260
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type cb_ricerca from commandbutton within w_report_stat_produzione
integer x = 3497
integer y = 1380
integer width = 366
integer height = 80
integer taborder = 240
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ricerca"
end type

event clicked;integer li_risposta,li_stato_fase, li_i
string ls_cod_operaio, ls_cod_reparto_scelto,ls_cod_lavorazione,ls_cod_prodotto,ls_cod_prodotto_finito,ls_cod_versione, &
		 ls_cod_reparto,ls_cod_versione_scelta, ls_cod_attrezzatura, &
		 ls_tipo_analisi, ls_stat_1, ls_stat_2, ls_stat_3, ls_stat_4, ls_stat_5, ls_stat_6, ls_stat_7, ls_stat_8, ls_stat_9, ls_stat_10, &
		 ls_stat_1_1, ls_stat_2_2, ls_stat_3_3, ls_stat_4_4, ls_stat_5_5, ls_stat_6_6, ls_stat_7_7, ls_stat_8_8, ls_stat_9_9, ls_stat_10_10, &
		 ls_flag_evasione, ls_sql, ls_c_1, ls_c_2, ls_c_3, ls_c_4, ls_c_5, ls_c_6, ls_c_7, ls_c_8, ls_c_9, ls_c_10, ls_d_1,ls_d_2,ls_d_3, &
		 ls_d_4,ls_d_5,ls_d_6,ls_d_7,ls_d_8,ls_d_9,ls_d_10, ls_t_an, ls_sel_reparto[5]
datetime ldt_da_data,ldt_a_data, ldt_data_consegna
dec{4} ld_quan_ordine, ld_quan_evasa, ls_quan_prodotta_reparto
long ll_riga,ll_i,ll_num_prodotti, l_error, ll_anno_commessa, ll_num_commessa, ll_righe, ll_y, ll_cont_attrezzatura

//Donato 13/10/2008
//Modifica per filtrare gli ordini per stato
string ls_sintassi, ls_errore, ls_desc_evasione, ls_sintax
integer li_ret

datastore lds_ricerca


setpointer (hourglass!)

// leggo i vari criteri di scelta.


// intervallo data di consegna obbligatoria!!

ldt_da_data = datetime(date(em_da_data.text),00:00:00)
ldt_a_data = datetime(date(em_a_data.text),00:00:00)

if isnull(ldt_da_data) or isnull(ldt_a_data) or ldt_da_data = datetime(date("01/01/1900"),00:00:00) or ldt_a_data = datetime(date("01/01/1900"),00:00:00) then
	g_mb.messagebox("Sep","Attenzione! Selezionare L'intervallo della data di consegna!",stopsign!)
	return
end if

ls_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)

if isnull(ls_tipo_analisi) or ls_tipo_analisi = "" then
	g_mb.messagebox("Sep","Attenzione! Selezionare il tipo di analisi!",stopsign!)
	return	
end if

ls_stat_1 = f_po_selectddlb(ddlb_cod_stat_1)
ls_stat_2 = f_po_selectddlb(ddlb_cod_stat_2)
ls_stat_3 = f_po_selectddlb(ddlb_cod_stat_3)
ls_stat_4 = f_po_selectddlb(ddlb_cod_stat_4)
ls_stat_5 = f_po_selectddlb(ddlb_cod_stat_5)
ls_stat_6 = f_po_selectddlb(ddlb_cod_stat_6)
ls_stat_7 = f_po_selectddlb(ddlb_cod_stat_7)
ls_stat_8 = f_po_selectddlb(ddlb_cod_stat_8)
ls_stat_9 = f_po_selectddlb(ddlb_cod_stat_9)
ls_stat_10 = f_po_selectddlb(ddlb_cod_stat_10)

ls_stat_1_1 = f_po_selectddlb(ddlb_cod_stat_1_1)
ls_stat_2_2 = f_po_selectddlb(ddlb_cod_stat_2_2)
ls_stat_3_3 = f_po_selectddlb(ddlb_cod_stat_3_3)
ls_stat_4_4 = f_po_selectddlb(ddlb_cod_stat_4_4)
ls_stat_5_5 = f_po_selectddlb(ddlb_cod_stat_5_5)
ls_stat_6_6 = f_po_selectddlb(ddlb_cod_stat_6_6)
ls_stat_7_7 = f_po_selectddlb(ddlb_cod_stat_7_7)
ls_stat_8_8 = f_po_selectddlb(ddlb_cod_stat_8_8)
ls_stat_9_9 = f_po_selectddlb(ddlb_cod_stat_9_9)
ls_stat_10_10 = f_po_selectddlb(ddlb_cod_stat_10_10)

ls_sel_reparto[1] = dw_selezione.getitemstring(1,"as_reparto_1")
ls_sel_reparto[2] = dw_selezione.getitemstring(1,"as_reparto_2")
ls_sel_reparto[3] = dw_selezione.getitemstring(1,"as_reparto_3")
ls_sel_reparto[4] = dw_selezione.getitemstring(1,"as_reparto_4")
ls_sel_reparto[5] = dw_selezione.getitemstring(1,"as_reparto_5")
ls_cod_attrezzatura = dw_selezione.getitemstring(1,"cod_attrezzatura")

idt_da_data = ldt_da_data
idt_a_data = ldt_a_data
//dw_elenco_prodotti_stat.change_dw_current()
dw_elenco_prodotti_stat.triggerevent("pcd_retrieve")


// carico la seconda dw!!
// ***** carico gli ordini in base alla azienda e all'intervallo della consegna

dw_elenco_prodotti_stat_prod.reset()


dw_elenco_prodotti_stat_prod.Object.des_statistico_1_t.Text = is_1
dw_elenco_prodotti_stat_prod.Object.des_statistico_2_t.Text = is_2
dw_elenco_prodotti_stat_prod.Object.des_statistico_3_t.Text = is_3
dw_elenco_prodotti_stat_prod.Object.des_statistico_4_t.Text = is_4
dw_elenco_prodotti_stat_prod.Object.des_statistico_5_t.Text = is_5
dw_elenco_prodotti_stat_prod.Object.des_statistico_6_t.Text = is_6
dw_elenco_prodotti_stat_prod.Object.reparto_1.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_2.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_3.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_4.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_5.Text = ""

for ll_y = 1 to 5
	if not isnull(ls_sel_reparto[ll_y]) then 
		dw_elenco_prodotti_stat_prod.modify("reparto_"+string(ll_y)+".Text = '" + ls_sel_reparto[ll_y] + "'")
	else
		dw_elenco_prodotti_stat_prod.modify("reparto_"+string(ll_y)+".Text = ''")
	end if
next

DECLARE cu_prodotti_stat DYNAMIC CURSOR FOR SQLSA ;

//Donato 13/10/2008 ------------------------------------------------------------------
//Modifica per filtrare in base allo stato dell'ordine
ls_desc_evasione = upper(ddlb_flag_evasione.text)

ls_sintax = 	"SELECT det_ord_ven.anno_registrazione," + &
			"det_ord_ven.num_registrazione," + &
			"det_ord_ven.cod_prodotto," + &
			"det_ord_ven.quan_ordine," + &
			"det_ord_ven.data_consegna," + &
			"det_ord_ven.quan_evasa," + &
			"det_ord_ven.flag_evasione," + &
			"det_ord_ven.num_commessa," + &
			"anag_clienti.rag_soc_1, " + &
			"det_ord_ven.anno_commessa" + &
	" FROM det_ord_ven " + &
	" LEFT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda "+&
				"AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione "+&
				"AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione "+&
	" LEFT OUTER JOIN anag_clienti ON anag_clienti.cod_azienda = tes_ord_ven.cod_azienda "+&
				"AND anag_clienti.cod_cliente = tes_ord_ven.cod_cliente " + &
	" WHERE det_ord_ven.cod_azienda = '"+ s_cs_xx.cod_azienda+ "' " + &
			" AND det_ord_ven.data_consegna >= '"+string(ldt_da_data, s_cs_xx.db_funzioni.formato_data)+"' " + &
			" AND det_ord_ven.data_consegna <= '"+string(ldt_a_data, s_cs_xx.db_funzioni.formato_data)+"' "
			
choose case ls_desc_evasione
	case "APERTO"
		ls_sintax += " AND det_ord_ven.flag_evasione='A'  "
		
	case "PARZIALE"
		ls_sintax += " AND det_ord_ven.flag_evasione='P'  "
		
	case "EVASO"
		ls_sintax += " AND det_ord_ven.flag_evasione='E'  "
		
	case else
		ls_sintax += " AND det_ord_ven.flag_evasione in ('A','P') "
			
end choose

li_risposta = guo_functions.uof_crea_datastore( lds_ricerca, ls_sintax, ls_errore)

if li_risposta = -1 then
	destroy lds_ricerca;
	g_mb.messagebox("SEP",g_str.format("Errore nel caricamento dei dati! $1", ls_errore) )
	return		
end if

if li_risposta > 0 then
	
	ll_i = 0
	ll_righe = 0
	for ll_i = 1 to lds_ricerca.rowcount()
		
		ls_cod_prodotto 		= lds_ricerca.getitemstring(ll_i,3)
		ll_anno_commessa 	= lds_ricerca.getitemnumber(ll_i,10)
		ll_num_commessa 	= lds_ricerca.getitemnumber(ll_i,8)
		
		if not isnull(ls_cod_attrezzatura) then
		
			select count(*)
			into	:ll_cont_attrezzatura
			from det_orari_produzione
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :ll_anno_commessa and
					num_commessa = :ll_num_commessa and
					cod_attrezzatura = :ls_cod_attrezzatura;
			
			if ll_cont_attrezzatura < 1 then continue 
					
		end if		
		st_message.text = g_str.format("Commessa $1-$2 Prodotto $3",ll_anno_commessa,ll_num_commessa,ls_cod_prodotto)
		Yield()
		
		if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then
			
			ls_sql = "select cod_tipo_analisi,cod_statistico_1,cod_statistico_2, cod_statistico_3, " + &
			          " cod_statistico_4, cod_statistico_5, cod_statistico_6, cod_statistico_7, " + &
						 " cod_statistico_8, cod_statistico_9, cod_statistico_10 " + &
						 " from tab_stat_prodotti "  + &
						 " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "' "
		
			if ls_tipo_analisi <> "" then
				ls_sql = ls_sql + " and cod_tipo_analisi = '" + ls_tipo_analisi + "' "
				if ls_stat_1_1 <> "" and ls_stat_1 <> "" then
					ls_sql = ls_sql + " and cod_statistico_1 between '" + ls_stat_1 + "' and '" + ls_stat_1_1 + "' "
				end if
				
				if ls_stat_2_2 <> "" and ls_stat_2 <> "" then
					ls_sql = ls_sql + " and cod_statistico_2 between '" + ls_stat_2 + "' and '" + ls_stat_2_2 + "' "
				end if
				
				if ls_stat_3_3 <> "" and ls_stat_3 <> "" then
					ls_sql = ls_sql + " and cod_statistico_3 between '" + ls_stat_3 + "' and '" + ls_stat_3_3 + "' "
				end if
				
				if ls_stat_4_4 <> "" and ls_stat_4 <> "" then
					ls_sql = ls_sql + " and cod_statistico_4 between '" + ls_stat_4 + "' and '" + ls_stat_4_4 + "' "
				end if
				
				if ls_stat_5_5 <> "" and ls_stat_5 <> "" then
					ls_sql = ls_sql + " and cod_statistico_5 between '" + ls_stat_5 + "' and '" + ls_stat_5_5 + "' "
				end if
				
				if ls_stat_6_6 <> "" and ls_stat_6 <> "" then
					ls_sql = ls_sql + " and cod_statistico_6 between '" + ls_stat_6 + "' and '" + ls_stat_6_6 + "' "
				end if
			
				if ls_stat_7_7 <> "" and ls_stat_7 <> "" then
					ls_sql = ls_sql + " and cod_statistico_7 between '" + ls_stat_7 + "' and '" + ls_stat_7_7 + "' "
				end if
				
				if ls_stat_8_8 <> "" and ls_stat_8 <> "" then
					ls_sql = ls_sql + " and cod_statistico_8 between '" + ls_stat_8 + "' and '" + ls_stat_8_8 + "' "
				end if
				
				if ls_stat_9_9 <> "" and ls_stat_9 <> "" then
					ls_sql = ls_sql + " and cod_statistico_9 between '" + ls_stat_9 + "' and '" + ls_stat_9_9 + "' "
				end if
				
				if ls_stat_10_10 <> "" and ls_stat_10 <> "" then
					ls_sql = ls_sql + " and cod_statistico_10 between '" + ls_stat_10 + "' and '" + ls_stat_10_10 + "' "
				end if		
		
			end if		
			ls_sql = ls_sql + " order by cod_statistico_1, cod_statistico_2, cod_statistico_3 "
			
		
			PREPARE SQLSA FROM :ls_sql;
			OPEN DYNAMIC cu_prodotti_stat;
			
			do while true
				FETCH cu_prodotti_stat INTO 	:ls_t_an,
													 	:ls_c_1,
				                            				:ls_c_2,
													 	:ls_c_3,
													 	:ls_c_4,
													 	:ls_c_5,
													 	:ls_c_6,
													 	:ls_c_7,
													 	:ls_c_8,
													 	:ls_c_9,
													 	:ls_c_10;
				if sqlca.sqlcode <> 0 then exit
				Yield()
				
				ll_righe = dw_elenco_prodotti_stat_prod.insertrow(ll_righe)
				dw_elenco_prodotti_stat_prod.SetItem(ll_righe,"cod_prodotto",ls_cod_prodotto)
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"rag_soc_1",lds_ricerca.getitemstring(ll_i,9))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"quan_ordine",lds_ricerca.getitemdecimal(ll_i,4))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"quan_evasa",lds_ricerca.getitemdecimal(ll_i,6))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"flag_evasione",lds_ricerca.getitemstring(ll_i,7))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"num_commessa",ll_num_commessa)
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"anno_commessa",ll_anno_commessa)
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_1",ls_c_1)
	
				select des_statistico_2
				into   :ls_d_2
				from   tab_statistica_2
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_2 = :ls_c_2;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_2",ls_d_2)
				
				select des_statistico_3
				into   :ls_d_3
				from   tab_statistica_3
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_3 = :ls_c_3;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_3",ls_d_3)
				
				select des_statistico_4
				into   :ls_d_4
				from   tab_statistica_4
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_4 = :ls_c_4;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_4",ls_d_4)
				
				select des_statistico_5
				into   :ls_d_5
				from   tab_statistica_5
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_5 = :ls_c_5;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_5",ls_d_5)
				
				select des_statistico_6
				into   :ls_d_6
				from   tab_statistica_6
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_6 = :ls_c_6;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_6",ls_d_6)
				
				select des_statistico_7
				into   :ls_d_7
				from   tab_statistica_7
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_7 = :ls_c_7;
				
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"data_consegna",lds_ricerca.getitemdatetime(ll_i,5))			
				
				for ll_y = 1 to 5
					if isnull(ls_sel_reparto[ll_y]) or len(ls_sel_reparto[ll_y]) < 1 then exit
					
					select sum(quan_prodotta)
					into	:ls_quan_prodotta_reparto
					from	avan_produzione_com
					where cod_azienda = :s_cs_xx.cod_azienda and
							anno_commessa = :ll_anno_commessa and
							num_commessa = :ll_num_commessa and
							cod_reparto = :ls_sel_reparto[ll_y];
							
					if isnull(ls_quan_prodotta_reparto) then ls_quan_prodotta_reparto = 0
					
					dw_elenco_prodotti_stat_prod.setitem(ll_righe,"quan_reparto_" + string(ll_y),ls_quan_prodotta_reparto)
					
					st_message.text = g_str.format("$1  Reparto $2  Quan-prodotta $3)",st_message.text, ls_sel_reparto[ll_y], ls_quan_prodotta_reparto)
					Yield()
				next
				
				ll_righe = ll_righe + 1
				
				
			loop

			CLOSE cu_prodotti_stat;
		
		end if		
	next
	
end if
dw_elenco_prodotti_stat_prod.setfocus()
dw_elenco_prodotti_stat_prod.SetSort("des_statistico_1 A ,des_statistico_2 A, des_statistico_3 A, data_consegna A ")

dw_elenco_prodotti_stat_prod.Sort()
setpointer (arrow!)

st_message.text = "Pronto!"


end event

type em_a_data from editmask within w_report_stat_produzione
integer x = 891
integer y = 1360
integer width = 411
integer height = 60
integer taborder = 230
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_6 from statictext within w_report_stat_produzione
integer x = 686
integer y = 1360
integer width = 183
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "A data"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_da_data from editmask within w_report_stat_produzione
integer x = 251
integer y = 1360
integer width = 411
integer height = 60
integer taborder = 220
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_5 from statictext within w_report_stat_produzione
integer x = 1417
integer y = 1360
integer width = 366
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stato Ordine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_cod_stat_7 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 860
integer width = 1303
integer height = 1260
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et7 from statictext within w_report_stat_produzione
integer x = 23
integer y = 860
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type et10 from statictext within w_report_stat_produzione
integer x = 23
integer y = 1220
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_10 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 1220
integer width = 1303
integer height = 1260
integer taborder = 200
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et9 from statictext within w_report_stat_produzione
integer x = 23
integer y = 1100
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_9 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 1100
integer width = 1303
integer height = 1260
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et8 from statictext within w_report_stat_produzione
integer x = 23
integer y = 980
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_8 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 980
integer width = 1303
integer height = 1260
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et6 from statictext within w_report_stat_produzione
integer x = 23
integer y = 740
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_6 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 740
integer width = 1303
integer height = 1260
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et5 from statictext within w_report_stat_produzione
integer x = 23
integer y = 620
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_5 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 620
integer width = 1303
integer height = 1260
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et4 from statictext within w_report_stat_produzione
integer x = 23
integer y = 500
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_4 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 500
integer width = 1303
integer height = 1260
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et3 from statictext within w_report_stat_produzione
integer x = 23
integer y = 380
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_3 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 380
integer width = 1303
integer height = 1260
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_2 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 260
integer width = 1303
integer height = 1260
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et2 from statictext within w_report_stat_produzione
integer x = 23
integer y = 260
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_1 from dropdownlistbox within w_report_stat_produzione
integer x = 594
integer y = 140
integer width = 1303
integer height = 1260
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et1 from statictext within w_report_stat_produzione
integer x = 23
integer y = 140
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_stat_produzione
integer x = 23
integer y = 20
integer width = 320
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Tipo Analisi:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_cod_tipo_analisi from dropdownlistbox within w_report_stat_produzione
integer x = 937
integer y = 20
integer width = 1303
integer height = 1260
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;string ls_cod_tipo_analisi,ls_label_1,ls_label_2,ls_label_3,ls_label_4,ls_label_5,ls_label_6,ls_label_7,ls_label_8, & 
		 ls_label_9,ls_label_10
		 
ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
st_3.text =  ls_cod_tipo_analisi 

select label_livello_1,
       label_livello_2,
   	 label_livello_3,
		 label_livello_4,
		 label_livello_5,
		 label_livello_6,
		 label_livello_7,
		 label_livello_8,
		 label_livello_9,
		 label_livello_10
into   :ls_label_1,
		 :ls_label_2,
		 :ls_label_3,
		 :ls_label_4,
		 :ls_label_5,
		 :ls_label_6,
		 :ls_label_7,
		 :ls_label_8,
		 :ls_label_9,
		 :ls_label_10
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_analisi=:ls_cod_tipo_analisi;

is_1 = ls_label_1
is_2 = ls_label_2
is_3 = ls_label_3
is_4 = ls_label_4
is_5 = ls_label_5
is_6 = ls_label_6
is_7 = ls_label_7
is_8 = ls_label_8
is_9 = ls_label_9
is_10 = ls_label_10


dw_elenco_prodotti_stat_prod.reset()
dw_elenco_prodotti_stat_prod.Modify("des_statistico_1_t.Text='" + is_1 + "'")
dw_elenco_prodotti_stat_prod.Object.des_statistico_2_t.Text = is_2
dw_elenco_prodotti_stat_prod.Object.des_statistico_3_t.Text = is_3
dw_elenco_prodotti_stat_prod.Object.des_statistico_4_t.Text = is_4
dw_elenco_prodotti_stat_prod.Object.des_statistico_5_t.Text = is_5
dw_elenco_prodotti_stat_prod.Object.des_statistico_6_t.Text = is_6
//dw_elenco_prodotti_stat_prod.Object.des_statistico_7_t.Text = is_7
//dw_elenco_prodotti_stat_prod.Object.des_statistico_8_t.Text = is_8
//dw_elenco_prodotti_stat_prod.Object.des_statistico_9_t.Text = is_9
//dw_elenco_prodotti_stat_prod.Object.des_statistico_10_t.Text = is_10

et1.text = ls_label_1
et2.text = ls_label_2
et3.text = ls_label_3
et4.text = ls_label_4
et5.text = ls_label_5
et6.text = ls_label_6
et7.text = ls_label_7
et8.text = ls_label_8
et9.text = ls_label_9
et10.text = ls_label_10

ddlb_cod_stat_1.text =""
ddlb_cod_stat_2.text =""
ddlb_cod_stat_3.text =""
ddlb_cod_stat_4.text =""
ddlb_cod_stat_5.text =""
ddlb_cod_stat_6.text =""
ddlb_cod_stat_7.text =""
ddlb_cod_stat_8.text =""
ddlb_cod_stat_9.text =""
ddlb_cod_stat_10.text =""
ddlb_cod_stat_1_1.text =""
ddlb_cod_stat_2_2.text =""
ddlb_cod_stat_3_3.text =""
ddlb_cod_stat_4_4.text =""
ddlb_cod_stat_5_5.text =""
ddlb_cod_stat_6_6.text =""
ddlb_cod_stat_7_7.text =""
ddlb_cod_stat_8_8.text =""
ddlb_cod_stat_9_9.text =""
ddlb_cod_stat_10_10.text =""
ddlb_cod_stat_1.reset()
ddlb_cod_stat_2.reset()
ddlb_cod_stat_3.reset()
ddlb_cod_stat_4.reset()
ddlb_cod_stat_5.reset()
ddlb_cod_stat_6.reset()
ddlb_cod_stat_7.reset()
ddlb_cod_stat_8.reset()
ddlb_cod_stat_9.reset()
ddlb_cod_stat_10.reset()
ddlb_cod_stat_1_1.reset()
ddlb_cod_stat_2_2.reset()
ddlb_cod_stat_3_3.reset()
ddlb_cod_stat_4_4.reset()
ddlb_cod_stat_5_5.reset()
ddlb_cod_stat_6_6.reset()
ddlb_cod_stat_7_7.reset()
ddlb_cod_stat_8_8.reset()
ddlb_cod_stat_9_9.reset()
ddlb_cod_stat_10_10.reset()

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)

// modifiche del 31/01/03: metto codice al posto della descrizione nella prima statistica
f_po_loadddlb(ddlb_cod_stat_1, &
                 sqlca, &
                 "tab_statistica_1", &
                 "cod_statistico_1", &
                 "cod_statistico_1", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_2, &
                 sqlca, &
                 "tab_statistica_2", &
                 "cod_statistico_2", &
                 "des_statistico_2", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_3, &
                 sqlca, &
                 "tab_statistica_3", &
                 "cod_statistico_3", &
                 "des_statistico_3", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_4, &
                 sqlca, &
                 "tab_statistica_4", &
                 "cod_statistico_4", &
                 "des_statistico_4", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_5, &
                 sqlca, &
                 "tab_statistica_5", &
                 "cod_statistico_5", &
                 "des_statistico_5", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_6, &
                 sqlca, &
                 "tab_statistica_6", &
                 "cod_statistico_6", &
                 "des_statistico_6", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_7, &
                 sqlca, &
                 "tab_statistica_7", &
                 "cod_statistico_7", &
                 "des_statistico_7", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_8, &
                 sqlca, &
                 "tab_statistica_8", &
                 "cod_statistico_8", &
                 "des_statistico_8", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_9, &
                 sqlca, &
                 "tab_statistica_9", &
                 "cod_statistico_9", &
                 "des_statistico_9", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_10, &
                 sqlca, &
                 "tab_statistica_10", &
                 "cod_statistico_10", &
                 "des_statistico_10", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")



f_po_loadddlb(ddlb_cod_stat_1_1, &
                 sqlca, &
                 "tab_statistica_1", &
                 "cod_statistico_1", &
                 "cod_statistico_1", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_2_2, &
                 sqlca, &
                 "tab_statistica_2", &
                 "cod_statistico_2", &
                 "des_statistico_2", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_3_3, &
                 sqlca, &
                 "tab_statistica_3", &
                 "cod_statistico_3", &
                 "des_statistico_3", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_4_4, &
                 sqlca, &
                 "tab_statistica_4", &
                 "cod_statistico_4", &
                 "des_statistico_4", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_5_5, &
                 sqlca, &
                 "tab_statistica_5", &
                 "cod_statistico_5", &
                 "des_statistico_5", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_6_6, &
                 sqlca, &
                 "tab_statistica_6", &
                 "cod_statistico_6", &
                 "des_statistico_6", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_7_7, &
                 sqlca, &
                 "tab_statistica_7", &
                 "cod_statistico_7", &
                 "des_statistico_7", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_8_8, &
                 sqlca, &
                 "tab_statistica_8", &
                 "cod_statistico_8", &
                 "des_statistico_8", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_9_9, &
                 sqlca, &
                 "tab_statistica_9", &
                 "cod_statistico_9", &
                 "des_statistico_9", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_10_10, &
                 sqlca, &
                 "tab_statistica_10", &
                 "cod_statistico_10", &
                 "des_statistico_10", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")


parent.triggerevent("pc_retrieve")
end event

type st_3 from statictext within w_report_stat_produzione
integer x = 343
integer y = 20
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

