﻿$PBExportHeader$w_ricerca_prodotto_chiavi.srw
$PBExportComments$Ricerca i prodotti in base alle chiavi scelte
forward
global type w_ricerca_prodotto_chiavi from w_cs_xx_principale
end type
type st_2 from statictext within w_ricerca_prodotto_chiavi
end type
type cb_ricerca from commandbutton within w_ricerca_prodotto_chiavi
end type
type dw_ricerca_prodotto_chiavi from uo_cs_xx_dw within w_ricerca_prodotto_chiavi
end type
end forward

global type w_ricerca_prodotto_chiavi from w_cs_xx_principale
integer width = 2711
integer height = 1504
string title = "Ricerca Prodotti Per Chiavi"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
st_2 st_2
cb_ricerca cb_ricerca
dw_ricerca_prodotto_chiavi dw_ricerca_prodotto_chiavi
end type
global w_ricerca_prodotto_chiavi w_ricerca_prodotto_chiavi

on w_ricerca_prodotto_chiavi.create
int iCurrent
call super::create
this.st_2=create st_2
this.cb_ricerca=create cb_ricerca
this.dw_ricerca_prodotto_chiavi=create dw_ricerca_prodotto_chiavi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.dw_ricerca_prodotto_chiavi
end on

on w_ricerca_prodotto_chiavi.destroy
call super::destroy
destroy(this.st_2)
destroy(this.cb_ricerca)
destroy(this.dw_ricerca_prodotto_chiavi)
end on

event pc_setwindow;call super::pc_setwindow;//dw_ricerca_prodotto_chiavi.set_dw_key("cod_azienda")
//dw_prodotti_chiavi_valori.set_dw_key("cod_prodotto")
set_w_options(c_closenosave + c_autoposition + c_noresizewin)
dw_ricerca_prodotto_chiavi.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )
													
													

//
//												//i_openparm, &
//dw_ricerca_prodotto_chiavi.set_dw_options(sqlca, &
//											i_openparm, &
//											c_scrollparent, &
//											c_default)
////iuo_dw_main = dw_ricerca_prodotto_chiavi



end event

event pc_setddlb;call super::pc_setddlb;	f_po_loaddddw_dw( dw_ricerca_prodotto_chiavi, &
						  "cod_chiave", &
						  sqlca, &
						  "tab_chiavi", &
						  "cod_chiave", &
						  "des_chiave", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

end event

type st_2 from statictext within w_ricerca_prodotto_chiavi
integer x = 46
integer y = 1340
integer width = 2208
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_ricerca from commandbutton within w_ricerca_prodotto_chiavi
integer x = 2322
integer y = 1332
integer width = 370
integer height = 84
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricerca"
end type

event clicked;string ls_sql, ls_stringa,ls_db, ls_str1, ls_str2, ls_cod_chiave, ls_sql_chiavi
long ll_i, ll_y, ll_ret, ll_pos, ll_progressivo, ll_controllo
integer li_risposta
//s_cs_xx.parametri.parametro_ds_1

//s_cs_xx.parametri.parametro_ds_1
li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

choose case ls_db
	case "SYBASE_ASE"
		ls_sql = "select cod_prodotto, des_prodotto, cod_comodo, cod_comodo_2 from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'"
	case else
		ls_sql = "select cod_prodotto, des_prodotto, cod_comodo, cod_comodo_2 from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( flag_blocco = 'N' or ( flag_blocco = 'S' and data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "' ))"
end choose

// *** carico tutti i prodotti con le chiavi

dw_ricerca_prodotto_chiavi.accepttext()

ls_sql_chiavi = ""
ll_controllo = 1
for ll_i = 1 to dw_ricerca_prodotto_chiavi.rowcount()
	
	ls_cod_chiave = dw_ricerca_prodotto_chiavi.getitemstring( ll_i, "cod_chiave")
	ll_progressivo = dw_ricerca_prodotto_chiavi.getitemnumber( ll_i, "progressivo")
	//*la prima riga 
	if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and not isnull(ll_progressivo) and ll_progressivo > 0 and ll_i =1 then
		
		ls_sql_chiavi += " and (cod_prodotto IN ( select cod_prodotto from anag_prodotti_chiavi where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_chiave = '" + ls_cod_chiave + "' and progressivo = " + string(ll_progressivo) + " )) "
		ll_controllo ++
		continue
	end if	
	if  not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and ( isnull(ll_progressivo) or ll_progressivo <= 0) and ll_i =1 then
		
		ls_sql_chiavi += " and (cod_prodotto IN ( select cod_prodotto from anag_prodotti_chiavi where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_chiave = '" + ls_cod_chiave + "' )) "		
		ll_controllo ++
		continue
	end if
	
	//*le altre righe prima riga 
	if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and not isnull(ll_progressivo) and ll_progressivo > 0 and ll_i >1 then
		
		ls_sql_chiavi += " and (cod_prodotto IN ( select cod_prodotto from anag_prodotti_chiavi where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_chiave = '" + ls_cod_chiave + "' and progressivo = " + string(ll_progressivo) + " ))) "
		//ll_controllo ++
		continue
	end if	
	if  not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and ( isnull(ll_progressivo) or ll_progressivo <= 0) and ll_i >1 then
		
		ls_sql_chiavi += " and (cod_prodotto IN ( select cod_prodotto from anag_prodotti_chiavi where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_chiave = '" + ls_cod_chiave + "' ))) "		
		//ll_controllo ++
		continue
	end if
	
	//*l'ultima 
	if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and not isnull(ll_progressivo) and ll_progressivo > 0 and ll_i = dw_ricerca_prodotto_chiavi.rowcount()-1 and ll_controllo <1 then
		
		ls_sql_chiavi += " and (cod_prodotto IN ( select cod_prodotto from anag_prodotti_chiavi where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_chiave = '" + ls_cod_chiave + "' and progressivo = " + string(ll_progressivo) + " )))) "
		ll_controllo ++
		continue	
	end if	
	if  not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and ( isnull(ll_progressivo) or ll_progressivo <= 0) and  ll_i = dw_ricerca_prodotto_chiavi.rowcount() and ll_controllo <1  then
		
		ls_sql_chiavi += " and (cod_prodotto IN ( select cod_prodotto from anag_prodotti_chiavi where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_chiave = '" + ls_cod_chiave + "' )))) "		
		ll_controllo ++
		continue	
	end if
	
next
if ll_controllo = 2 then
	ls_sql_chiavi += ")"
end if
if ls_sql_chiavi <> "" then 
	ls_sql += " " + ls_sql_chiavi
end if

ls_sql = ls_sql + " order by cod_prodotto"

ll_ret = s_cs_xx.parametri.parametro_ds_1.setsqlselect(ls_sql)


if ll_ret < 1 then
	g_mb.messagebox("APICE", "Errore nella assegnazione sintassi SQL di ricerca con setsqlselect: comunicare l'errore all'amministratore del sistema")
	return
end if
ll_y =s_cs_xx.parametri.parametro_ds_1.retrieve()//


if ll_y > 0 then
	// se tutto va bene resetto la datawindow di ricerca
//	dw_ricerca_prodotto_chiavi.reset()
//	dw_ricerca_prodotto_chiavi.set_dw_options(sqlca, &
//														pcca.null_object, &
//														c_nomodify + &
//														c_noretrieveonopen + &
//														c_nodelete + &
//														c_newonopen + &
//														c_disableCC, &
//														c_noresizedw + &
//														c_nohighlightselected + &
//														c_nocursorrowpointer +&
//														c_nocursorrowfocusrect )
//	dw_ricerca_prodotto_chiavi.insertrow(0)
	//
	//st_2.text = "TROVATI " + string(ll_y) + " PRODOTTI"
	//call w_ricerca_prodotto_chiavi.super::event close()
	//w_ricerca_prodotto_chiavi.triggerEvent("close")
		//w_ricerca_prodotto_chiavi.postevent("close")
			close(parent)
else
	
	st_2.text = "NESSUN PRODOTTO TROVATO"
//	dw_stringa_ricerca_prodotti.setfocus()
end if

//dw_folder.fu_selectTab(1)
end event

type dw_ricerca_prodotto_chiavi from uo_cs_xx_dw within w_ricerca_prodotto_chiavi
integer width = 2697
integer height = 1316
integer taborder = 10
string dataobject = "d_anag_prodotti_chiavi_ricerca"
boolean maxbox = true
end type

event itemchanged;call super::itemchanged;long ll_null, ll_i, ll_progressivo
string ls_cod_chiave

setnull(ll_null)
ll_i = dw_ricerca_prodotto_chiavi.getrow()
if ll_i <= 0 then return -1
//i_parentdw.retrieve()
choose case i_colname
	case "cod_chiave"
			
		if not isnull(i_coltext) and i_coltext <> "" then
			
			f_po_loaddddw_dw(dw_ricerca_prodotto_chiavi, &
								  "progressivo", &
								  sqlca, &
								  "tab_chiavi_valori", &
								  "progressivo", &
								  "valore", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_chiave = '" + i_coltext + "' ")		
								  
			setitem(ll_i,"progressivo", ll_null)
			//aggiungo una riga alla fine della dw solo se cod_chiave non è nulla in quella presente
			if dw_ricerca_prodotto_chiavi.rowcount() = ll_i then
				//se l'ultima riga corrisponde con quella che sto modificando
				insertrow(0)
			else
				// se l'ultima riga non è quella che sto modificando
				ls_cod_chiave = dw_ricerca_prodotto_chiavi.getitemstring(dw_ricerca_prodotto_chiavi.rowcount(),"cod_chiave")
				if ls_cod_chiave <> ""  and not isnull(ls_cod_chiave) then
					insertrow(0)
				end if
			
			end if
//			ls_cod_chiave = dw_ricerca_prodotto_chiavi.getitemstring(dw_ricerca_prodotto_chiavi.rowcount(),"cod_chiave")
//			if ls_cod_chiave <> ""  and not isnull(ls_cod_chiave) then
//				insertrow(0)
//			else
//				if ll_i = 1   then insertrow(0)
//			end if
		else
			
			f_po_loaddddw_dw(dw_ricerca_prodotto_chiavi, &
								  "progressivo", &
								  sqlca, &
								  "tab_chiavi_valori", &
								  "progressivo", &
								  "valore", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
								  
			setitem(ll_i,"progressivo", ll_null)
			//aggiungo una riga alla fine della dw solo se cod_chiave non è nulla in quella presente
//			if dw_ricerca_prodotto_chiavi.getitemstring(dw_ricerca_prodotto_chiavi.rowcount(),"cod_chiave")<>""  or not isnull(dw_ricerca_prodotto_chiavi.getitemstring(dw_ricerca_prodotto_chiavi.rowcount(),"cod_chiave")) then
//				insertrow(0)
//			end if
		end if
		
		
	case "progressivo"
		
		if rowcount() = row then
			
			ls_cod_chiave = getitemstring( row, "cod_chiave")
			ll_progressivo = long(i_coltext)
			
//			if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" then//and not isnull(ll_progressivo) and ll_progressivo > 0 then
//				insertrow(0)
//			end if			
			
		end if
end choose
end event

