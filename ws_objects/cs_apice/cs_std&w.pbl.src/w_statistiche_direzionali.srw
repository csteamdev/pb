﻿$PBExportHeader$w_statistiche_direzionali.srw
forward
global type w_statistiche_direzionali from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_statistiche_direzionali
end type
type et20 from statictext within w_statistiche_direzionali
end type
type st_4 from statictext within w_statistiche_direzionali
end type
type st_23 from statictext within w_statistiche_direzionali
end type
type st_2 from statictext within w_statistiche_direzionali
end type
type ddlb_reparti from dropdownlistbox within w_statistiche_direzionali
end type
type sle_versione from singlelineedit within w_statistiche_direzionali
end type
type cb_media_tempi from commandbutton within w_statistiche_direzionali
end type
type st_11 from statictext within w_statistiche_direzionali
end type
type st_9 from statictext within w_statistiche_direzionali
end type
type st_lavorazione from statictext within w_statistiche_direzionali
end type
type st_attrezzaggio from statictext within w_statistiche_direzionali
end type
type st_10 from statictext within w_statistiche_direzionali
end type
type st_8 from statictext within w_statistiche_direzionali
end type
type st_risorsa_umana from statictext within w_statistiche_direzionali
end type
type st_attrezzaggio_com from statictext within w_statistiche_direzionali
end type
type cbx_tutti from checkbox within w_statistiche_direzionali
end type
type ddlb_operai from dropdownlistbox within w_statistiche_direzionali
end type
type em_a_data from editmask within w_statistiche_direzionali
end type
type st_6 from statictext within w_statistiche_direzionali
end type
type em_da_data from editmask within w_statistiche_direzionali
end type
type st_5 from statictext within w_statistiche_direzionali
end type
type dw_elenco_prodotti_stat from uo_cs_xx_dw within w_statistiche_direzionali
end type
type ddlb_cod_stat_7 from dropdownlistbox within w_statistiche_direzionali
end type
type et7 from statictext within w_statistiche_direzionali
end type
type et10 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_10 from dropdownlistbox within w_statistiche_direzionali
end type
type et9 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_9 from dropdownlistbox within w_statistiche_direzionali
end type
type et8 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_8 from dropdownlistbox within w_statistiche_direzionali
end type
type et6 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_6 from dropdownlistbox within w_statistiche_direzionali
end type
type et5 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_5 from dropdownlistbox within w_statistiche_direzionali
end type
type et4 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_4 from dropdownlistbox within w_statistiche_direzionali
end type
type et3 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_3 from dropdownlistbox within w_statistiche_direzionali
end type
type ddlb_cod_stat_2 from dropdownlistbox within w_statistiche_direzionali
end type
type et2 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_stat_1 from dropdownlistbox within w_statistiche_direzionali
end type
type et1 from statictext within w_statistiche_direzionali
end type
type st_1 from statictext within w_statistiche_direzionali
end type
type ddlb_cod_tipo_analisi from dropdownlistbox within w_statistiche_direzionali
end type
type st_3 from statictext within w_statistiche_direzionali
end type
type gb_1 from groupbox within w_statistiche_direzionali
end type
end forward

global type w_statistiche_direzionali from w_cs_xx_principale
integer width = 3625
integer height = 2048
string title = "Statistiche Direzionali"
cb_reset cb_reset
et20 et20
st_4 st_4
st_23 st_23
st_2 st_2
ddlb_reparti ddlb_reparti
sle_versione sle_versione
cb_media_tempi cb_media_tempi
st_11 st_11
st_9 st_9
st_lavorazione st_lavorazione
st_attrezzaggio st_attrezzaggio
st_10 st_10
st_8 st_8
st_risorsa_umana st_risorsa_umana
st_attrezzaggio_com st_attrezzaggio_com
cbx_tutti cbx_tutti
ddlb_operai ddlb_operai
em_a_data em_a_data
st_6 st_6
em_da_data em_da_data
st_5 st_5
dw_elenco_prodotti_stat dw_elenco_prodotti_stat
ddlb_cod_stat_7 ddlb_cod_stat_7
et7 et7
et10 et10
ddlb_cod_stat_10 ddlb_cod_stat_10
et9 et9
ddlb_cod_stat_9 ddlb_cod_stat_9
et8 et8
ddlb_cod_stat_8 ddlb_cod_stat_8
et6 et6
ddlb_cod_stat_6 ddlb_cod_stat_6
et5 et5
ddlb_cod_stat_5 ddlb_cod_stat_5
et4 et4
ddlb_cod_stat_4 ddlb_cod_stat_4
et3 et3
ddlb_cod_stat_3 ddlb_cod_stat_3
ddlb_cod_stat_2 ddlb_cod_stat_2
et2 et2
ddlb_cod_stat_1 ddlb_cod_stat_1
et1 et1
st_1 st_1
ddlb_cod_tipo_analisi ddlb_cod_tipo_analisi
st_3 st_3
gb_1 gb_1
end type
global w_statistiche_direzionali w_statistiche_direzionali

type variables
decimal	id_tot_minuti_attrezzaggio, id_tot_minuti_attrezzaggio_commessa,id_tot_minuti_lavorazione, & 
			id_tot_minuti_risorsa_umana 
end variables

forward prototypes
public function integer wf_calcola_tempi_fase (string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto, string fs_cod_operaio, datetime fdt_da_data, datetime fdt_a_data, ref decimal fd_minuti_attrezzaggio, ref decimal fd_minuti_attrezzaggio_commessa, ref decimal fd_minuti_lavorazione, ref decimal fd_minuti_risorsa_umana)
public function integer wf_esplodi_fasi (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_corrente, ref integer fi_stato_fase)
end prototypes

public function integer wf_calcola_tempi_fase (string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto, string fs_cod_operaio, datetime fdt_da_data, datetime fdt_a_data, ref decimal fd_minuti_attrezzaggio, ref decimal fd_minuti_attrezzaggio_commessa, ref decimal fd_minuti_lavorazione, ref decimal fd_minuti_risorsa_umana);string   ls_giorno,ls_giorno_it,ls_problemi_produzione, & 
			ls_cognome,ls_errore,ls_cod_prodotto_test,ls_cod_operaio
datetime ldt_data_corrente,lt_orario_attrezzaggio,lt_orario_attrezzaggio_commessa, &
			lt_orario_lavorazione,lt_orario_risorsa_umana,lt_orario_attrezzaggio_fine,lt_orario_attrezzaggio_commessa_fine, &
			lt_orario_lavorazione_fine,lt_orario_risorsa_umana_fine
decimal  ld_secondi,ld_minuti_risorsa_umana,ld_ore,ld_quan_prodotta,ld_minuti_lavorazione,ld_minuti_attrezzaggio, & 
			ld_minuti_attrezzaggio_commessa,ld_ore_lavorazione,ld_ore_attrezzaggio,ld_ore_attrezzaggio_commessa, & 
			ld_somma_tempo_attrezzaggio,ld_somma_tempo_attrezzaggio_commessa,ld_somma_tempo_lavorazione
integer  li_risposta
long     ll_num_anno_commessa,ll_num_num_commessa,ll_num_commesse,ll_num_orari

select count(*)
into   :ll_num_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
		
if ll_num_orari = 0 then return 1

		
ldt_data_corrente = fdt_da_data

do while ldt_data_corrente <= fdt_a_data
	
	declare r_det_orari cursor for 
	select orario_attrezzaggio,
			 orario_attrezzaggio_commessa,
			 orario_lavorazione,
			 orario_risorsa_umana,
			 cod_operaio
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    data_giorno = :ldt_data_corrente
	and    flag_inizio = 'S'
	and    cod_reparto = :fs_cod_reparto
	and    cod_lavorazione=:fs_cod_lavorazione
	and    cod_prodotto=:fs_cod_prodotto
	and    cod_operaio like :fs_cod_operaio
	order by prog_orari;
	
	open r_det_orari;
	
	do while 1 = 1 
	
		fetch r_det_orari
		into  :lt_orario_attrezzaggio,
				:lt_orario_attrezzaggio_commessa,
				:lt_orario_lavorazione,
				:lt_orario_risorsa_umana,
				:ls_cod_operaio;

		
		if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_orari;
			return -1
		end if
	
		if time(lt_orario_attrezzaggio) <> 00:00:00 then

			declare r_d1 cursor for 
			select orario_attrezzaggio
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_attrezzaggio is not null
			and    orario_attrezzaggio > :lt_orario_attrezzaggio
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto
			order by orario_attrezzaggio;
			
			open r_d1;
			
			fetch r_d1
			into  :lt_orario_attrezzaggio_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d1;
				close r_det_orari;
				return -1
			end if
			
			close r_d1;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio),time(lt_orario_attrezzaggio_fine))
			ld_minuti_attrezzaggio = ld_minuti_attrezzaggio + ld_secondi/60
			ld_secondi=0
		end if	

		if time(lt_orario_attrezzaggio_commessa) <> 00:00:00 then
			declare r_d2 cursor for 
			select orario_attrezzaggio_commessa
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_attrezzaggio_commessa is not null
			and    orario_attrezzaggio_commessa > :lt_orario_attrezzaggio_commessa
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto

			order by orario_attrezzaggio_commessa;
			
			open r_d2;
			
			fetch r_d2
			into  :lt_orario_attrezzaggio_commessa_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d2;
				close r_det_orari;
				return -1
			end if
			
			close r_d2;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio_commessa),time(lt_orario_attrezzaggio_commessa_fine))
			ld_minuti_attrezzaggio_commessa = ld_minuti_attrezzaggio_commessa + ld_secondi/60
			ld_secondi = 0
		end if	

		if time(lt_orario_lavorazione) <> 00:00:00  then
			declare r_d3 cursor for 
			select orario_lavorazione,
					 problemi_produzione
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_lavorazione is not null
			and    orario_lavorazione > :lt_orario_lavorazione
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto

			order by orario_lavorazione;
			
			open r_d3;
			
			fetch r_d3
			into  :lt_orario_lavorazione_fine,
					:ls_problemi_produzione;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d3;
				close r_det_orari;
				return -1
			end if 
			
			close r_d3;
			if ls_problemi_produzione ='N' or isnull(ls_problemi_produzione) then
				ld_secondi = SecondsAfter ( time(lt_orario_lavorazione),time(lt_orario_lavorazione_fine))
				ld_minuti_lavorazione = ld_minuti_lavorazione + ld_secondi/60			
			end if
		end if	

		if time(lt_orario_risorsa_umana) <> 00:00:00 then
			declare r_d4 cursor for 
			select orario_risorsa_umana
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio = :ls_cod_operaio
			and    orario_risorsa_umana is not null
			and    orario_risorsa_umana >= :lt_orario_risorsa_umana
			order by orario_risorsa_umana;
			
			open r_d4;
			
			fetch r_d4
			into  :lt_orario_risorsa_umana_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d4;
				close r_det_orari;
				return -1
			end if
			
			close r_d4;
			ld_secondi = SecondsAfter ( time(lt_orario_risorsa_umana),time(lt_orario_risorsa_umana_fine))
			ld_minuti_risorsa_umana = ld_minuti_risorsa_umana + ld_secondi/60			
		
		end if	

	loop
	
	
	close r_det_orari;


	ldt_data_corrente = datetime(RelativeDate ( date(ldt_data_corrente) , 1 ),00:00:00)
		
loop

select sum(quan_prodotta)
into   :ld_quan_prodotta
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select count(distinct anno_commessa)
into   :ll_num_anno_commessa
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select count(distinct num_commessa)
into   :ll_num_num_commessa
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if ll_num_num_commessa > ll_num_anno_commessa then
	ll_num_commesse = ll_num_num_commessa
else
	ll_num_commesse = ll_num_anno_commessa
end if

if ll_num_commesse>0 then
	fd_minuti_attrezzaggio = round(ld_minuti_attrezzaggio/ll_num_commesse,4)
	fd_minuti_attrezzaggio_commessa = round(ld_minuti_attrezzaggio_commessa/ll_num_commesse,4)
end if

if ld_quan_prodotta > 0 then
	fd_minuti_lavorazione = round(ld_minuti_lavorazione/ld_quan_prodotta,4)
	fd_minuti_risorsa_umana = round(ld_minuti_risorsa_umana/ld_quan_prodotta,4)
end if



return 0
end function

public function integer wf_esplodi_fasi (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_corrente, ref integer fi_stato_fase);string    ls_cod_prodotto_figlio, ls_flag_esterna, ls_cod_operaio,ls_cod_reparto_scelto,& 		   
		    ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, ls_cod_prodotto_inserito, & 
			 ls_cod_lavorazione,ls_cod_tipo_mov_reso,ls_cod_tipo_mov_sfrido,ls_test, & 
			 ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante
long      ll_num_figli,ll_num_righe,ll_ordinamento
integer   li_risposta
double    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione,ldd_quan_utilizzo_variante
datetime  ldt_da_data,ldt_a_data
decimal   ld_minuti_attrezzaggio,ld_minuti_attrezzaggio_commessa,ld_minuti_lavorazione,ld_minuti_risorsa_umana
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe
	ls_cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")

	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	

	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio

	
	if ls_flag_materia_prima = 'N' then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda 
		and 	 cod_prodotto_padre=:ls_cod_prodotto_inserito
		and    cod_versione=:fs_cod_versione;
		
		if sqlca.sqlcode=100 then continue
	else
		continue
	end if


	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_inserito
	and    cod_versione=:fs_cod_versione;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=wf_esplodi_fasi(ls_cod_prodotto_inserito,fs_cod_versione, fl_num_livello_corrente + 1,fi_stato_fase)
		if li_risposta = -1 then
			return -1
		end if
		
	end if

   SELECT cod_reparto,   
          cod_lavorazione,
			 flag_esterna
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_flag_esterna
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  
	AND   cod_prodotto = :ls_cod_prodotto_inserito;

	if sqlca.sqlcode=100 then
		
		return -1
	end if

	ld_minuti_attrezzaggio = 0
	ld_minuti_attrezzaggio_commessa = 0
	ld_minuti_lavorazione = 0
	ld_minuti_risorsa_umana = 0

	ls_cod_operaio = f_po_selectddlb(ddlb_operai)
	if cbx_tutti.checked = true then ls_cod_operaio = "%"

	ldt_da_data = datetime(date(em_da_data.text),00:00:00)
	ldt_a_data = datetime(date(em_a_data.text),00:00:00)

	ls_cod_reparto_scelto = f_po_selectddlb(ddlb_reparti)
	
	if isnull(ls_cod_reparto_scelto) or ls_cod_reparto_scelto="" then

		li_risposta = wf_calcola_tempi_fase(ls_cod_reparto, &
														ls_cod_lavorazione,&
														ls_cod_prodotto_inserito,&
														ls_cod_operaio,&
														ldt_da_data,&
														ldt_a_data,&
														ld_minuti_attrezzaggio,&
														ld_minuti_attrezzaggio_commessa,&
														ld_minuti_lavorazione,&
														ld_minuti_risorsa_umana)
	
	else
		if ls_cod_reparto_scelto = ls_cod_reparto then
	
			li_risposta = wf_calcola_tempi_fase(ls_cod_reparto, &
															ls_cod_lavorazione,&
															ls_cod_prodotto_inserito,&
															ls_cod_operaio,&
															ldt_da_data,&
															ldt_a_data,&
															ld_minuti_attrezzaggio,&
															ld_minuti_attrezzaggio_commessa,&
															ld_minuti_lavorazione,&
															ld_minuti_risorsa_umana)
															
			
		end if
	end if
	
	if li_risposta = 1 then fi_stato_fase = 1
		
	id_tot_minuti_attrezzaggio = id_tot_minuti_attrezzaggio + ld_minuti_attrezzaggio
	id_tot_minuti_attrezzaggio_commessa = id_tot_minuti_attrezzaggio_commessa +ld_minuti_attrezzaggio_commessa
	id_tot_minuti_lavorazione = id_tot_minuti_lavorazione + ld_minuti_lavorazione
	id_tot_minuti_risorsa_umana = id_tot_minuti_risorsa_umana+ ld_minuti_risorsa_umana
	
next

return 0
end function

on w_statistiche_direzionali.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.et20=create et20
this.st_4=create st_4
this.st_23=create st_23
this.st_2=create st_2
this.ddlb_reparti=create ddlb_reparti
this.sle_versione=create sle_versione
this.cb_media_tempi=create cb_media_tempi
this.st_11=create st_11
this.st_9=create st_9
this.st_lavorazione=create st_lavorazione
this.st_attrezzaggio=create st_attrezzaggio
this.st_10=create st_10
this.st_8=create st_8
this.st_risorsa_umana=create st_risorsa_umana
this.st_attrezzaggio_com=create st_attrezzaggio_com
this.cbx_tutti=create cbx_tutti
this.ddlb_operai=create ddlb_operai
this.em_a_data=create em_a_data
this.st_6=create st_6
this.em_da_data=create em_da_data
this.st_5=create st_5
this.dw_elenco_prodotti_stat=create dw_elenco_prodotti_stat
this.ddlb_cod_stat_7=create ddlb_cod_stat_7
this.et7=create et7
this.et10=create et10
this.ddlb_cod_stat_10=create ddlb_cod_stat_10
this.et9=create et9
this.ddlb_cod_stat_9=create ddlb_cod_stat_9
this.et8=create et8
this.ddlb_cod_stat_8=create ddlb_cod_stat_8
this.et6=create et6
this.ddlb_cod_stat_6=create ddlb_cod_stat_6
this.et5=create et5
this.ddlb_cod_stat_5=create ddlb_cod_stat_5
this.et4=create et4
this.ddlb_cod_stat_4=create ddlb_cod_stat_4
this.et3=create et3
this.ddlb_cod_stat_3=create ddlb_cod_stat_3
this.ddlb_cod_stat_2=create ddlb_cod_stat_2
this.et2=create et2
this.ddlb_cod_stat_1=create ddlb_cod_stat_1
this.et1=create et1
this.st_1=create st_1
this.ddlb_cod_tipo_analisi=create ddlb_cod_tipo_analisi
this.st_3=create st_3
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.et20
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.st_23
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.ddlb_reparti
this.Control[iCurrent+7]=this.sle_versione
this.Control[iCurrent+8]=this.cb_media_tempi
this.Control[iCurrent+9]=this.st_11
this.Control[iCurrent+10]=this.st_9
this.Control[iCurrent+11]=this.st_lavorazione
this.Control[iCurrent+12]=this.st_attrezzaggio
this.Control[iCurrent+13]=this.st_10
this.Control[iCurrent+14]=this.st_8
this.Control[iCurrent+15]=this.st_risorsa_umana
this.Control[iCurrent+16]=this.st_attrezzaggio_com
this.Control[iCurrent+17]=this.cbx_tutti
this.Control[iCurrent+18]=this.ddlb_operai
this.Control[iCurrent+19]=this.em_a_data
this.Control[iCurrent+20]=this.st_6
this.Control[iCurrent+21]=this.em_da_data
this.Control[iCurrent+22]=this.st_5
this.Control[iCurrent+23]=this.dw_elenco_prodotti_stat
this.Control[iCurrent+24]=this.ddlb_cod_stat_7
this.Control[iCurrent+25]=this.et7
this.Control[iCurrent+26]=this.et10
this.Control[iCurrent+27]=this.ddlb_cod_stat_10
this.Control[iCurrent+28]=this.et9
this.Control[iCurrent+29]=this.ddlb_cod_stat_9
this.Control[iCurrent+30]=this.et8
this.Control[iCurrent+31]=this.ddlb_cod_stat_8
this.Control[iCurrent+32]=this.et6
this.Control[iCurrent+33]=this.ddlb_cod_stat_6
this.Control[iCurrent+34]=this.et5
this.Control[iCurrent+35]=this.ddlb_cod_stat_5
this.Control[iCurrent+36]=this.et4
this.Control[iCurrent+37]=this.ddlb_cod_stat_4
this.Control[iCurrent+38]=this.et3
this.Control[iCurrent+39]=this.ddlb_cod_stat_3
this.Control[iCurrent+40]=this.ddlb_cod_stat_2
this.Control[iCurrent+41]=this.et2
this.Control[iCurrent+42]=this.ddlb_cod_stat_1
this.Control[iCurrent+43]=this.et1
this.Control[iCurrent+44]=this.st_1
this.Control[iCurrent+45]=this.ddlb_cod_tipo_analisi
this.Control[iCurrent+46]=this.st_3
this.Control[iCurrent+47]=this.gb_1
end on

on w_statistiche_direzionali.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.et20)
destroy(this.st_4)
destroy(this.st_23)
destroy(this.st_2)
destroy(this.ddlb_reparti)
destroy(this.sle_versione)
destroy(this.cb_media_tempi)
destroy(this.st_11)
destroy(this.st_9)
destroy(this.st_lavorazione)
destroy(this.st_attrezzaggio)
destroy(this.st_10)
destroy(this.st_8)
destroy(this.st_risorsa_umana)
destroy(this.st_attrezzaggio_com)
destroy(this.cbx_tutti)
destroy(this.ddlb_operai)
destroy(this.em_a_data)
destroy(this.st_6)
destroy(this.em_da_data)
destroy(this.st_5)
destroy(this.dw_elenco_prodotti_stat)
destroy(this.ddlb_cod_stat_7)
destroy(this.et7)
destroy(this.et10)
destroy(this.ddlb_cod_stat_10)
destroy(this.et9)
destroy(this.ddlb_cod_stat_9)
destroy(this.et8)
destroy(this.ddlb_cod_stat_8)
destroy(this.et6)
destroy(this.ddlb_cod_stat_6)
destroy(this.et5)
destroy(this.ddlb_cod_stat_5)
destroy(this.et4)
destroy(this.ddlb_cod_stat_4)
destroy(this.et3)
destroy(this.ddlb_cod_stat_3)
destroy(this.ddlb_cod_stat_2)
destroy(this.et2)
destroy(this.ddlb_cod_stat_1)
destroy(this.et1)
destroy(this.st_1)
destroy(this.ddlb_cod_tipo_analisi)
destroy(this.st_3)
destroy(this.gb_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_prodotti_stat.set_dw_options(sqlca, pcca.null_object,c_nonew + c_nodelete + c_nomodify + c_noretrieveonopen,c_default)

iuo_dw_main = dw_elenco_prodotti_stat

em_a_data.text = string(today())
em_da_data.text = string(relativedate(today(),-365))
end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_cod_tipo_analisi, &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")


f_po_loadddlb(ddlb_reparti, &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
					  
					  
f_PO_LoadDDLB(ddlb_operai, sqlca, "anag_operai", &	
				  "cod_operaio", "cognome", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
	
end event

type cb_reset from commandbutton within w_statistiche_direzionali
integer x = 1806
integer y = 1560
integer width = 91
integer height = 60
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "R."
end type

event clicked;ddlb_reparti.reset()
ddlb_reparti.text=""
et20.text=""

f_po_loadddlb(ddlb_reparti, &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
					  
end event

type et20 from statictext within w_statistiche_direzionali
integer x = 709
integer y = 1460
integer width = 251
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type st_4 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 1360
integer width = 517
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Operaio"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_23 from statictext within w_statistiche_direzionali
integer x = 503
integer y = 1460
integer width = 206
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Reparto"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 1460
integer width = 183
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ver."
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_reparti from dropdownlistbox within w_statistiche_direzionali
integer x = 960
integer y = 1460
integer width = 937
integer height = 1260
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
et20.text =  f_po_selectddlb(ddlb_reparti)

end event

type sle_versione from singlelineedit within w_statistiche_direzionali
integer x = 206
integer y = 1460
integer width = 251
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
end type

type cb_media_tempi from commandbutton within w_statistiche_direzionali
integer x = 1897
integer y = 1840
integer width = 462
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola Tempi"
end type

event clicked;integer li_risposta,li_stato_fase
string ls_cod_operaio, ls_cod_reparto_scelto,ls_cod_lavorazione,ls_cod_prodotto,ls_cod_prodotto_finito,ls_cod_versione, &
		 ls_cod_reparto,ls_cod_versione_scelta
datetime ldt_da_data,ldt_a_data
decimal ld_minuti_lavorazione,ld_minuti_attrezzaggio,ld_minuti_attrezzaggio_commessa,ld_minuti_risorsa_umana
long ll_riga,ll_i,ll_num_prodotti

setpointer (hourglass!)

st_attrezzaggio.text = "0"
st_attrezzaggio_com.text = "0"
st_lavorazione.text = "0"
st_risorsa_umana.text = "0"

id_tot_minuti_attrezzaggio = 0
id_tot_minuti_attrezzaggio_commessa = 0
id_tot_minuti_lavorazione = 0
id_tot_minuti_risorsa_umana = 0
	
ls_cod_operaio = f_po_selectddlb(ddlb_operai)
if cbx_tutti.checked = true then ls_cod_operaio = "%"

ls_cod_reparto_scelto = f_po_selectddlb(ddlb_reparti)

ls_cod_versione_scelta =sle_versione.text

ldt_da_data = datetime(date(em_da_data.text),00:00:00)
ldt_a_data = datetime(date(em_a_data.text),00:00:00)

if isnull(ls_cod_operaio) or ls_cod_operaio="" or ls_cod_operaio ="none" then
	g_mb.messagebox("Sep","Attenzione! Selezionare almeno un operaio oppure l'opzione tutti.",stopsign!)
	return
end if

ll_i = 0

for ll_riga = 1 to dw_elenco_prodotti_stat.rowcount()
	ls_cod_prodotto_finito = dw_elenco_prodotti_stat.getitemstring(ll_riga,"cod_prodotto")
	
	
	
	
	if isnull(ls_cod_versione_scelta) or ls_cod_versione_scelta ="" then
		declare r_distinta_padri cursor for
		select  cod_versione
		from    distinta_padri
		where   cod_azienda=:s_cs_xx.cod_azienda
		and     cod_prodotto=:ls_cod_prodotto_finito;
		
		open  r_distinta_padri;
		
		do while 1=1
			fetch r_distinta_padri 
			into  :ls_cod_versione;
			
			if sqlca.sqlcode = 100 then exit
			
			li_stato_fase = 0
			li_risposta = wf_esplodi_fasi(ls_cod_prodotto_finito,ls_cod_versione,1,li_stato_fase)
		
		loop
		
		close r_distinta_padri;
		
	else
		li_stato_fase = 0
		li_risposta = wf_esplodi_fasi(ls_cod_prodotto_finito,ls_cod_versione_scelta,1,li_stato_fase)
	end if
	
	if li_stato_fase = 1 then ll_i++
next									


ll_num_prodotti = dw_elenco_prodotti_stat.rowcount() - ll_i

if ll_num_prodotti = 0 then 
	g_mb.messagebox("Apice","Attenzione! nessuno dei prodotti della lista ha dei dettagli orari, pertanto non è possibile effettuare il calcolo.",information!)
	return
end if

st_attrezzaggio.text = string(round(id_tot_minuti_attrezzaggio/ll_num_prodotti,4))
st_attrezzaggio_com.text = string(round(id_tot_minuti_attrezzaggio_commessa/ll_num_prodotti,4))
st_lavorazione.text = string(round(id_tot_minuti_lavorazione/ll_num_prodotti,4))
st_risorsa_umana.text = string(round(id_tot_minuti_risorsa_umana/ll_num_prodotti,4))
 
setpointer (arrow!)


end event

type st_11 from statictext within w_statistiche_direzionali
integer x = 46
integer y = 1740
integer width = 343
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attrezzaggio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_9 from statictext within w_statistiche_direzionali
integer x = 46
integer y = 1820
integer width = 343
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Lavorazione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_lavorazione from statictext within w_statistiche_direzionali
integer x = 389
integer y = 1820
integer width = 389
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_attrezzaggio from statictext within w_statistiche_direzionali
integer x = 389
integer y = 1740
integer width = 389
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_10 from statictext within w_statistiche_direzionali
integer x = 974
integer y = 1740
integer width = 343
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attrez. Com.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_8 from statictext within w_statistiche_direzionali
integer x = 891
integer y = 1820
integer width = 425
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Risorsa Umana:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_risorsa_umana from statictext within w_statistiche_direzionali
integer x = 1326
integer y = 1820
integer width = 389
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_attrezzaggio_com from statictext within w_statistiche_direzionali
integer x = 1326
integer y = 1740
integer width = 389
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cbx_tutti from checkbox within w_statistiche_direzionali
integer x = 1646
integer y = 1340
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tutti"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

type ddlb_operai from dropdownlistbox within w_statistiche_direzionali
integer x = 571
integer y = 1340
integer width = 1051
integer height = 1260
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type em_a_data from editmask within w_statistiche_direzionali
integer x = 914
integer y = 1580
integer width = 411
integer height = 60
integer taborder = 170
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_6 from statictext within w_statistiche_direzionali
integer x = 731
integer y = 1580
integer width = 183
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "A data"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_da_data from editmask within w_statistiche_direzionali
integer x = 229
integer y = 1580
integer width = 411
integer height = 60
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_5 from statictext within w_statistiche_direzionali
integer y = 1580
integer width = 229
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Da data"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_elenco_prodotti_stat from uo_cs_xx_dw within w_statistiche_direzionali
integer x = 1920
integer y = 140
integer width = 1646
integer height = 1640
integer taborder = 180
string dataobject = "d_elenco_prodotti_stat"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_tipo_analisi,ls_cod_statistico_1,ls_cod_statistico_2,ls_cod_statistico_3,ls_cod_statistico_4,ls_cod_statistico_5, &
		 ls_cod_statistico_6,ls_cod_statistico_7,ls_cod_statistico_8,ls_cod_statistico_9,ls_cod_statistico_10,ls_sql

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)

ls_cod_statistico_1 = f_po_selectddlb(ddlb_cod_stat_1)
ls_cod_statistico_2 = f_po_selectddlb(ddlb_cod_stat_2)
ls_cod_statistico_3 = f_po_selectddlb(ddlb_cod_stat_3)
ls_cod_statistico_4 = f_po_selectddlb(ddlb_cod_stat_4)
ls_cod_statistico_5 = f_po_selectddlb(ddlb_cod_stat_5)
ls_cod_statistico_6 = f_po_selectddlb(ddlb_cod_stat_6)
ls_cod_statistico_7 = f_po_selectddlb(ddlb_cod_stat_7)
ls_cod_statistico_8 = f_po_selectddlb(ddlb_cod_stat_8)
ls_cod_statistico_9 = f_po_selectddlb(ddlb_cod_stat_9)
ls_cod_statistico_10 = f_po_selectddlb(ddlb_cod_stat_10)

ls_sql = "Select cod_prodotto from tab_stat_prodotti where cod_tipo_analisi='" + ls_cod_tipo_analisi + "'"

if not (isnull(ls_cod_statistico_1)) and ls_cod_statistico_1 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_1 ='" + ls_cod_statistico_1 + "'"
end if

if not (isnull(ls_cod_statistico_2)) and ls_cod_statistico_2 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_2 ='" + ls_cod_statistico_2 + "'"
end if

if not (isnull(ls_cod_statistico_3)) and ls_cod_statistico_3 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_3 ='" + ls_cod_statistico_3 + "'"
end if

if not (isnull(ls_cod_statistico_4)) and ls_cod_statistico_4 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_4 ='" + ls_cod_statistico_4 + "'"
end if

if not (isnull(ls_cod_statistico_5)) and ls_cod_statistico_5 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_5 ='" + ls_cod_statistico_5 + "'"
end if

if not (isnull(ls_cod_statistico_6)) and ls_cod_statistico_6 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_6 ='" + ls_cod_statistico_6 + "'"
end if

if not (isnull(ls_cod_statistico_7)) and ls_cod_statistico_7 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_7 ='" + ls_cod_statistico_7 + "'"
end if

if not (isnull(ls_cod_statistico_8)) and ls_cod_statistico_8 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_8 ='" + ls_cod_statistico_8 + "'"
end if

if not (isnull(ls_cod_statistico_9)) and ls_cod_statistico_9 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_9 ='" + ls_cod_statistico_9 + "'"
end if

if not (isnull(ls_cod_statistico_10)) and ls_cod_statistico_10 <> "" then 
	ls_sql = ls_sql + " and cod_statistico_10 ='" + ls_cod_statistico_10 + "'"
end if

dw_elenco_prodotti_stat.setsqlselect(ls_sql)

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type ddlb_cod_stat_7 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 860
integer width = 1303
integer height = 1260
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et7 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 860
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type et10 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 1220
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_10 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 1220
integer width = 1303
integer height = 1260
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et9 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 1100
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_9 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 1100
integer width = 1303
integer height = 1260
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et8 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 980
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_8 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 980
integer width = 1303
integer height = 1260
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et6 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 740
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_6 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 740
integer width = 1303
integer height = 1260
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et5 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 620
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_5 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 620
integer width = 1303
integer height = 1260
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et4 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 500
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_4 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 500
integer width = 1303
integer height = 1260
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et3 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 380
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_3 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 380
integer width = 1303
integer height = 1260
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type ddlb_cod_stat_2 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 260
integer width = 1303
integer height = 1260
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et2 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 260
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_cod_stat_1 from dropdownlistbox within w_statistiche_direzionali
integer x = 594
integer y = 140
integer width = 1303
integer height = 1260
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;
parent.triggerevent("pc_retrieve")
end event

type et1 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 140
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_statistiche_direzionali
integer x = 23
integer y = 20
integer width = 320
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Tipo Analisi:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_cod_tipo_analisi from dropdownlistbox within w_statistiche_direzionali
integer x = 937
integer y = 20
integer width = 1303
integer height = 1260
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;string ls_cod_tipo_analisi,ls_label_1,ls_label_2,ls_label_3,ls_label_4,ls_label_5,ls_label_6,ls_label_7,ls_label_8, & 
		 ls_label_9,ls_label_10
		 
ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)
st_3.text =  ls_cod_tipo_analisi 

select label_livello_1,
       label_livello_2,
   	 label_livello_3,
		 label_livello_4,
		 label_livello_5,
		 label_livello_6,
		 label_livello_7,
		 label_livello_8,
		 label_livello_9,
		 label_livello_10
into   :ls_label_1,
		 :ls_label_2,
		 :ls_label_3,
		 :ls_label_4,
		 :ls_label_5,
		 :ls_label_6,
		 :ls_label_7,
		 :ls_label_8,
		 :ls_label_9,
		 :ls_label_10
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_analisi=:ls_cod_tipo_analisi;

et1.text = ls_label_1
et2.text = ls_label_2
et3.text = ls_label_3
et4.text = ls_label_4
et5.text = ls_label_5
et6.text = ls_label_6
et7.text = ls_label_7
et8.text = ls_label_8
et9.text = ls_label_9
et10.text = ls_label_10

ddlb_cod_stat_1.text =""
ddlb_cod_stat_2.text =""
ddlb_cod_stat_3.text =""
ddlb_cod_stat_4.text =""
ddlb_cod_stat_5.text =""
ddlb_cod_stat_6.text =""
ddlb_cod_stat_7.text =""
ddlb_cod_stat_8.text =""
ddlb_cod_stat_9.text =""
ddlb_cod_stat_10.text =""
ddlb_cod_stat_1.reset()
ddlb_cod_stat_2.reset()
ddlb_cod_stat_3.reset()
ddlb_cod_stat_4.reset()
ddlb_cod_stat_5.reset()
ddlb_cod_stat_6.reset()
ddlb_cod_stat_7.reset()
ddlb_cod_stat_8.reset()
ddlb_cod_stat_9.reset()
ddlb_cod_stat_10.reset()

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)

f_po_loadddlb(ddlb_cod_stat_1, &
                 sqlca, &
                 "tab_statistica_1", &
                 "cod_statistico_1", &
                 "des_statistico_1", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_2, &
                 sqlca, &
                 "tab_statistica_2", &
                 "cod_statistico_2", &
                 "des_statistico_2", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_3, &
                 sqlca, &
                 "tab_statistica_3", &
                 "cod_statistico_3", &
                 "des_statistico_3", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_4, &
                 sqlca, &
                 "tab_statistica_4", &
                 "cod_statistico_4", &
                 "des_statistico_4", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_5, &
                 sqlca, &
                 "tab_statistica_5", &
                 "cod_statistico_5", &
                 "des_statistico_5", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_6, &
                 sqlca, &
                 "tab_statistica_6", &
                 "cod_statistico_6", &
                 "des_statistico_6", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_7, &
                 sqlca, &
                 "tab_statistica_7", &
                 "cod_statistico_7", &
                 "des_statistico_7", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_8, &
                 sqlca, &
                 "tab_statistica_8", &
                 "cod_statistico_8", &
                 "des_statistico_8", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_9, &
                 sqlca, &
                 "tab_statistica_9", &
                 "cod_statistico_9", &
                 "des_statistico_9", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")

f_po_loadddlb(ddlb_cod_stat_10, &
                 sqlca, &
                 "tab_statistica_10", &
                 "cod_statistico_10", &
                 "des_statistico_10", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'","")


parent.triggerevent("pc_retrieve")
end event

type st_3 from statictext within w_statistiche_direzionali
integer x = 343
integer y = 20
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_statistiche_direzionali
integer x = 23
integer y = 1660
integer width = 1783
integer height = 260
integer taborder = 190
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Medie dei tempi per periodo in minuti"
borderstyle borderstyle = stylelowered!
end type

