﻿$PBExportHeader$w_statistiche_vendita_det.srw
forward
global type w_statistiche_vendita_det from w_cs_xx_risposta
end type
type dw_tes_statistiche_det from uo_cs_xx_dw within w_statistiche_vendita_det
end type
type cb_salva from commandbutton within w_statistiche_vendita_det
end type
type cb_annulla from commandbutton within w_statistiche_vendita_det
end type
end forward

global type w_statistiche_vendita_det from w_cs_xx_risposta
integer width = 3113
integer height = 956
string title = "Dettagli Tipo Statistica"
dw_tes_statistiche_det dw_tes_statistiche_det
cb_salva cb_salva
cb_annulla cb_annulla
end type
global w_statistiche_vendita_det w_statistiche_vendita_det

type variables
boolean ib_modificato_dati, ib_modificato_codice
end variables

on w_statistiche_vendita_det.create
int iCurrent
call super::create
this.dw_tes_statistiche_det=create dw_tes_statistiche_det
this.cb_salva=create cb_salva
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_statistiche_det
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.cb_annulla
end on

on w_statistiche_vendita_det.destroy
call super::destroy
destroy(this.dw_tes_statistiche_det)
destroy(this.cb_salva)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;ib_modificato_dati = false

ib_modificato_codice = false

choose case s_cs_xx.parametri.parametro_s_1

	case "nuovo"
	
		dw_tes_statistiche_det.set_dw_options(sqlca, &
                            					  pcca.null_object, &
                            					  c_nomodify + c_nodelete + &                            
                            					  c_disableCC, &
                            					  c_noresizedw + &
                            					  c_nohighlightselected + &
                            					  c_cursorrowpointer)
		
		save_on_close(c_socnosave)
		
		dw_tes_statistiche_det.postevent("pcd_new")
		
	case "modifica"
		
		dw_tes_statistiche_det.set_dw_options(sqlca, &
                            					  pcca.null_object, &
                            					  c_nonew + c_nodelete + &                            
                            					  c_disableCC, &
                            					  c_noresizedw + &
                            					  c_nohighlightselected + &
                            					  c_cursorrowpointer)		
		
		save_on_close(c_socnosave)
		
		dw_tes_statistiche_det.postevent("pcd_modify")

end choose
end event

type dw_tes_statistiche_det from uo_cs_xx_dw within w_statistiche_vendita_det
integer x = 23
integer y = 20
integer width = 2994
integer height = 676
integer taborder = 10
string dataobject = "d_tes_statistiche_det"
end type

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
next	
end event

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_2)
end event

event editchanged;call super::editchanged;choose case s_cs_xx.parametri.parametro_s_1

	case "nuovo"
	
		if dwo.name = "cod_statistica" then
			
			if isnull(data) or data = "" then
				cb_salva.enabled = false
				ib_modificato_codice = false
			else
				cb_salva.enabled = true
				ib_modificato_codice = true
			end if	
			
		else
			
			if isnull(data) or data = "" then
				ib_modificato_dati = false	
			else
				ib_modificato_dati = true
			end if	
			
		end if	
		
	case "modifica"
		
		cb_salva.enabled = true
		
		ib_modificato_dati = true
	
end choose
end event

type cb_salva from commandbutton within w_statistiche_vendita_det
integer x = 2651
integer y = 720
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Salva"
end type

event clicked;dw_tes_statistiche_det.accepttext()

if s_cs_xx.parametri.parametro_s_1 = "nuovo" then		
	
	dw_tes_statistiche_det.setitem(dw_tes_statistiche_det.getrow(),"data_creazione",today())
		
	dw_tes_statistiche_det.setitem(dw_tes_statistiche_det.getrow(),"cod_utente",s_cs_xx.cod_utente)

end if		

dw_tes_statistiche_det.triggerevent("pcd_save")

close(w_statistiche_vendita_det)
end event

type cb_annulla from commandbutton within w_statistiche_vendita_det
integer x = 2263
integer y = 720
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;choose case s_cs_xx.parametri.parametro_s_1

	case "nuovo"
	
		if ib_modificato_codice = true or ib_modificato_dati = true then
			
			if g_mb.messagebox("Annulla modifiche","Ignorare le modifiche apportate al record?",question!,yesno!,2) = 2 then
				return
			end if
			
		end if
		
	case "modifica"
		
		if ib_modificato_dati = true then
			
			if g_mb.messagebox("Annulla modifiche","Ignorare le modifiche apportate al record?",question!,yesno!,2) = 2 then
				return
			end if
			
		end if
	
end choose

close(w_statistiche_vendita_det)
end event

