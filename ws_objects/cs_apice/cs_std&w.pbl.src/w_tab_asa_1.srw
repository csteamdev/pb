﻿$PBExportHeader$w_tab_asa_1.srw
$PBExportComments$Window gestione tabella ASA
forward
global type w_tab_asa_1 from w_cs_xx_principale
end type
type dw_tab_asa_1 from uo_cs_xx_dw within w_tab_asa_1
end type
type ddlb_cod_tipo_analisi from dropdownlistbox within w_tab_asa_1
end type
type st_cod_tipo_analisi from statictext within w_tab_asa_1
end type
type ddlb_gestione from dropdownlistbox within w_tab_asa_1
end type
type em_da_data from editmask within w_tab_asa_1
end type
type st_2 from statictext within w_tab_asa_1
end type
type em_a_data from editmask within w_tab_asa_1
end type
type st_3 from statictext within w_tab_asa_1
end type
type st_gestioni from statictext within w_tab_asa_1
end type
end forward

global type w_tab_asa_1 from w_cs_xx_principale
int Width=3489
int Height=1505
boolean TitleBar=true
string Title="Tipi Analisi"
dw_tab_asa_1 dw_tab_asa_1
ddlb_cod_tipo_analisi ddlb_cod_tipo_analisi
st_cod_tipo_analisi st_cod_tipo_analisi
ddlb_gestione ddlb_gestione
em_da_data em_da_data
st_2 st_2
em_a_data em_a_data
st_3 st_3
st_gestioni st_gestioni
end type
global w_tab_asa_1 w_tab_asa_1

on w_tab_asa_1.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_asa_1=create dw_tab_asa_1
this.ddlb_cod_tipo_analisi=create ddlb_cod_tipo_analisi
this.st_cod_tipo_analisi=create st_cod_tipo_analisi
this.ddlb_gestione=create ddlb_gestione
this.em_da_data=create em_da_data
this.st_2=create st_2
this.em_a_data=create em_a_data
this.st_3=create st_3
this.st_gestioni=create st_gestioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_asa_1
this.Control[iCurrent+2]=ddlb_cod_tipo_analisi
this.Control[iCurrent+3]=st_cod_tipo_analisi
this.Control[iCurrent+4]=ddlb_gestione
this.Control[iCurrent+5]=em_da_data
this.Control[iCurrent+6]=st_2
this.Control[iCurrent+7]=em_a_data
this.Control[iCurrent+8]=st_3
this.Control[iCurrent+9]=st_gestioni
end on

on w_tab_asa_1.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_asa_1)
destroy(this.ddlb_cod_tipo_analisi)
destroy(this.st_cod_tipo_analisi)
destroy(this.ddlb_gestione)
destroy(this.em_da_data)
destroy(this.st_2)
destroy(this.em_a_data)
destroy(this.st_3)
destroy(this.st_gestioni)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_asa_1.set_dw_key("cod_azienda")
dw_tab_asa_1.set_dw_key("cod_tipo_analisi")
dw_tab_asa_1.set_dw_key("progressivo")

dw_tab_asa_1.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_default,c_default)

iuo_dw_main = dw_tab_asa_1

em_da_data.text="01/01/1900"
em_a_data.text="01/01/2999"

end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_cod_tipo_analisi, &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

type dw_tab_asa_1 from uo_cs_xx_dw within w_tab_asa_1
int X=23
int Y=221
int Width=3406
int Height=1161
int TabOrder=10
string DataObject="d_tab_asa_1"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_tipo_analisi,ls_cod_asa
datetime ld_da_data,ld_a_data

ls_cod_tipo_analisi = st_cod_tipo_analisi.text
ls_cod_asa=st_gestioni.text + "%"
ld_da_data=datetime(date(em_da_data.text))
ld_a_data=datetime(date(em_a_data.text))

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi,ls_cod_asa,ld_da_data,ld_a_data)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	ddlb_cod_tipo_analisi.enabled=false
	ddlb_gestione.enabled=false
	em_a_data.enabled=false
	em_da_data.enabled=false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_test	
	long l_idx
	long ll_progressivo

	ddlb_cod_tipo_analisi.enabled=false
	ddlb_gestione.enabled=false
	em_a_data.enabled=false
	em_da_data.enabled=false

	select max(progressivo)
	into   :ll_progressivo
	from   tab_asa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_analisi=:st_cod_tipo_analisi.text;

	FOR l_Idx = 1 TO RowCount()
		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
			ll_progressivo++
	   	SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
			SetItem(l_Idx, "cod_tipo_analisi", st_cod_tipo_analisi.text)
			SetItem(l_Idx, "progressivo", ll_progressivo)
			SetItem(getrow(), "data_reg", date(String(Today( ), "dd/mm/yyyy")))
			setItem(getrow(), "data_scad", date(String(Today( ), "dd/mm/yyyy")))
			setItem(getrow(), "cod_asa", st_gestioni.text)
	  	end if
	NEXT
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	ddlb_cod_tipo_analisi.enabled=true
	ddlb_gestione.enabled=true
	em_a_data.enabled=true
	em_da_data.enabled=true
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	ddlb_cod_tipo_analisi.enabled=true
	ddlb_gestione.enabled=true
	em_a_data.enabled=true
	em_da_data.enabled=true
end if
end event

event pcd_validaterow;call super::pcd_validaterow;integer li_num_livelli
string ls_cod_statistico_1, &
		 ls_cod_statistico_2, &
 		 ls_cod_statistico_3, &
		 ls_cod_statistico_4, &
		 ls_cod_statistico_5, &
		 ls_cod_statistico_6, &
		 ls_cod_statistico_7, &
		 ls_cod_statistico_8, &
		 ls_cod_statistico_9, &
		 ls_cod_statistico_10, &
		 ls_cod_tipo_analisi
long  ll_i,ll_num_righe
boolean lb_1,lb_2,lb_3,lb_4,lb_5,lb_6,lb_7,lb_8,lb_9,lb_10
	
lb_1=false
lb_2=false
lb_3=false
lb_4=false
lb_5=false
lb_6=false
lb_7=false
lb_8=false
lb_9=false
lb_10=false
	
ll_i=getrow()
ll_num_righe = rowcount()
ll_i=ll_num_righe
ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)

select num_liv_gestiti
into   :li_num_livelli
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and	 cod_tipo_analisi=:ls_cod_tipo_analisi;

choose case li_num_livelli
	case 1
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_2=true
		lb_3=true
		lb_4=true
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 2
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_3=true
		lb_4=true
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 3
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_4=true
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 4
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(ll_i,"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_5=true
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 5
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(ll_i,"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(ll_i,"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_6=true
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 6
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(ll_i,"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(ll_i,"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(ll_i,"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_7=true
		lb_8=true
		lb_9=true
		lb_10=true

	case 7
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(ll_i,"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(ll_i,"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(ll_i,"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(ll_i,"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_8=true
		lb_9=true
		lb_10=true

	case 8
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(ll_i,"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(ll_i,"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(ll_i,"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(ll_i,"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_8 = getitemstring(ll_i,"cod_statistico_8")
		if ls_cod_statistico_8= "" or isnull(ls_cod_statistico_8) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 8.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_9=true
		lb_10=true

	case 9
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(ll_i,"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(ll_i,"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(ll_i,"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(ll_i,"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_8 = getitemstring(ll_i,"cod_statistico_8")
		if ls_cod_statistico_8= "" or isnull(ls_cod_statistico_8) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 8.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_9 = getitemstring(ll_i,"cod_statistico_9")
		if ls_cod_statistico_9= "" or isnull(ls_cod_statistico_9) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 9.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		lb_10=true

	case 10
		ls_cod_statistico_1 = getitemstring(ll_i,"cod_statistico_1")
		if ls_cod_statistico_1= "" or isnull(ls_cod_statistico_1) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 1.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_2 = getitemstring(ll_i,"cod_statistico_2")
		if ls_cod_statistico_2= "" or isnull(ls_cod_statistico_2) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 2.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_3 = getitemstring(ll_i,"cod_statistico_3")
		if ls_cod_statistico_3= "" or isnull(ls_cod_statistico_3) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 3.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_4 = getitemstring(ll_i,"cod_statistico_4")
		if ls_cod_statistico_4= "" or isnull(ls_cod_statistico_4) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 4.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_5 = getitemstring(ll_i,"cod_statistico_5")
		if ls_cod_statistico_5= "" or isnull(ls_cod_statistico_5) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 5.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_6 = getitemstring(ll_i,"cod_statistico_6")
		if ls_cod_statistico_6= "" or isnull(ls_cod_statistico_6) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 6.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_7 = getitemstring(ll_i,"cod_statistico_7")
		if ls_cod_statistico_7= "" or isnull(ls_cod_statistico_7) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 7.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_8 = getitemstring(ll_i,"cod_statistico_8")
		if ls_cod_statistico_8= "" or isnull(ls_cod_statistico_8) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 8.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_9 = getitemstring(ll_i,"cod_statistico_9")
		if ls_cod_statistico_9= "" or isnull(ls_cod_statistico_9) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 9.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
		ls_cod_statistico_10 = getitemstring(ll_i,"cod_statistico_10")
		if ls_cod_statistico_10= "" or isnull(ls_cod_statistico_10) then
			g_mb.messagebox("Apice","Attenzione! Manca il livello 10.",exclamation!)
			pcca.error=c_valfailed
			return
		end if
end choose
end event

type ddlb_cod_tipo_analisi from dropdownlistbox within w_tab_asa_1
int X=412
int Y=21
int Width=915
int Height=1321
int TabOrder=20
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;string ls_cod_tipo_analisi,ls_test, & 
		 ls_label_livello_1, &
		 ls_label_livello_2, &
		 ls_label_livello_3, &
		 ls_label_livello_4, &
		 ls_label_livello_5, &
		 ls_label_livello_6, &
		 ls_label_livello_7, &
		 ls_label_livello_8, &
		 ls_label_livello_9, &
		 ls_label_livello_10

integer li_num_liv_gestiti

ls_cod_tipo_analisi = f_po_selectddlb(ddlb_cod_tipo_analisi)


st_cod_tipo_analisi.text=ls_cod_tipo_analisi


if (not isnull(ls_cod_tipo_analisi)) and ls_cod_tipo_analisi <> "" then

	select num_liv_gestiti,
			 label_livello_1,
			 label_livello_2,
			 label_livello_3,
			 label_livello_4,
			 label_livello_5,
			 label_livello_6,
			 label_livello_7,
			 label_livello_8,
			 label_livello_9,
			 label_livello_10
	into   :li_num_liv_gestiti,
			 :ls_label_livello_1,
	 		 :ls_label_livello_2,
		 	 :ls_label_livello_3,
			 :ls_label_livello_4,
			 :ls_label_livello_5,
			 :ls_label_livello_6,
			 :ls_label_livello_7,
			 :ls_label_livello_8,
			 :ls_label_livello_9,
			 :ls_label_livello_10
	from   tab_tipi_analisi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_analisi=:ls_cod_tipo_analisi;

	if isnull(ls_label_livello_1) then ls_label_livello_1=""
	if isnull(ls_label_livello_2) then ls_label_livello_2=""
	if isnull(ls_label_livello_3) then ls_label_livello_3=""
	if isnull(ls_label_livello_4) then ls_label_livello_4=""
	if isnull(ls_label_livello_5) then ls_label_livello_5=""
	if isnull(ls_label_livello_6) then ls_label_livello_6=""
	if isnull(ls_label_livello_7) then ls_label_livello_7=""
	if isnull(ls_label_livello_8) then ls_label_livello_8=""
	if isnull(ls_label_livello_9) then ls_label_livello_9=""
	if isnull(ls_label_livello_10) then ls_label_livello_10=""

			choose case li_num_liv_gestiti
				case 1
					dw_tab_asa_1.Object.cod_statistico_1_t.Text = ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 0
					dw_tab_asa_1.Object.cod_statistico_3.visible = 0
					dw_tab_asa_1.Object.cod_statistico_4.visible = 0
					dw_tab_asa_1.Object.cod_statistico_5.visible = 0
					dw_tab_asa_1.Object.cod_statistico_6.visible = 0
					dw_tab_asa_1.Object.cod_statistico_7.visible = 0
					dw_tab_asa_1.Object.cod_statistico_8.visible = 0
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 2
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 0
					dw_tab_asa_1.Object.cod_statistico_4.visible = 0
					dw_tab_asa_1.Object.cod_statistico_5.visible = 0
					dw_tab_asa_1.Object.cod_statistico_6.visible = 0
					dw_tab_asa_1.Object.cod_statistico_7.visible = 0
					dw_tab_asa_1.Object.cod_statistico_8.visible = 0
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 3
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 0
					dw_tab_asa_1.Object.cod_statistico_5.visible = 0
					dw_tab_asa_1.Object.cod_statistico_6.visible = 0
					dw_tab_asa_1.Object.cod_statistico_7.visible = 0
					dw_tab_asa_1.Object.cod_statistico_8.visible = 0
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 4
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_4_t.Text =ls_label_livello_4
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 1
					dw_tab_asa_1.Object.cod_statistico_5.visible = 0
					dw_tab_asa_1.Object.cod_statistico_6.visible = 0
					dw_tab_asa_1.Object.cod_statistico_7.visible = 0
					dw_tab_asa_1.Object.cod_statistico_8.visible = 0
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 5
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_4_t.Text =ls_label_livello_4
					dw_tab_asa_1.Object.cod_statistico_5_t.Text =ls_label_livello_5
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 1
					dw_tab_asa_1.Object.cod_statistico_5.visible = 1
					dw_tab_asa_1.Object.cod_statistico_6.visible = 0
					dw_tab_asa_1.Object.cod_statistico_7.visible = 0
					dw_tab_asa_1.Object.cod_statistico_8.visible = 0
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 6
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_4_t.Text =ls_label_livello_4
					dw_tab_asa_1.Object.cod_statistico_5_t.Text =ls_label_livello_5
					dw_tab_asa_1.Object.cod_statistico_6_t.Text =ls_label_livello_6
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 1
					dw_tab_asa_1.Object.cod_statistico_5.visible = 1
					dw_tab_asa_1.Object.cod_statistico_6.visible = 1
					dw_tab_asa_1.Object.cod_statistico_7.visible = 0
					dw_tab_asa_1.Object.cod_statistico_8.visible = 0
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 7
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_4_t.Text =ls_label_livello_4
					dw_tab_asa_1.Object.cod_statistico_5_t.Text =ls_label_livello_5
					dw_tab_asa_1.Object.cod_statistico_6_t.Text =ls_label_livello_6
					dw_tab_asa_1.Object.cod_statistico_7_t.Text =ls_label_livello_7
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 1
					dw_tab_asa_1.Object.cod_statistico_5.visible = 1
					dw_tab_asa_1.Object.cod_statistico_6.visible = 1
					dw_tab_asa_1.Object.cod_statistico_7.visible = 1
					dw_tab_asa_1.Object.cod_statistico_8.visible = 0
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 8
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_4_t.Text =ls_label_livello_4
					dw_tab_asa_1.Object.cod_statistico_5_t.Text =ls_label_livello_5
					dw_tab_asa_1.Object.cod_statistico_6_t.Text =ls_label_livello_6
					dw_tab_asa_1.Object.cod_statistico_7_t.Text =ls_label_livello_7
			   	dw_tab_asa_1.Object.cod_statistico_8_t.Text =ls_label_livello_8
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 1
					dw_tab_asa_1.Object.cod_statistico_5.visible = 1
					dw_tab_asa_1.Object.cod_statistico_6.visible = 1
					dw_tab_asa_1.Object.cod_statistico_7.visible = 1
					dw_tab_asa_1.Object.cod_statistico_8.visible = 1
					dw_tab_asa_1.Object.cod_statistico_9.visible = 0
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_8",sqlca,&
               "tab_statistica_8","cod_statistico_8","des_statistico_8","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

				case 9
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_4_t.Text =ls_label_livello_4
					dw_tab_asa_1.Object.cod_statistico_5_t.Text =ls_label_livello_5
					dw_tab_asa_1.Object.cod_statistico_6_t.Text =ls_label_livello_6
					dw_tab_asa_1.Object.cod_statistico_7_t.Text =ls_label_livello_7
			   	dw_tab_asa_1.Object.cod_statistico_8_t.Text =ls_label_livello_8
					dw_tab_asa_1.Object.cod_statistico_9_t.Text =ls_label_livello_9
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 1
					dw_tab_asa_1.Object.cod_statistico_5.visible = 1
					dw_tab_asa_1.Object.cod_statistico_6.visible = 1
					dw_tab_asa_1.Object.cod_statistico_7.visible = 1
					dw_tab_asa_1.Object.cod_statistico_8.visible = 1
					dw_tab_asa_1.Object.cod_statistico_9.visible = 1
					dw_tab_asa_1.Object.cod_statistico_10.visible = 0
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_8",sqlca,&
               "tab_statistica_8","cod_statistico_8","des_statistico_8","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_9",sqlca,&
               "tab_statistica_9","cod_statistico_9","des_statistico_9","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
				case 10
					dw_tab_asa_1.Object.cod_statistico_1_t.Text =ls_label_livello_1
					dw_tab_asa_1.Object.cod_statistico_2_t.Text =ls_label_livello_2
					dw_tab_asa_1.Object.cod_statistico_3_t.Text =ls_label_livello_3
					dw_tab_asa_1.Object.cod_statistico_4_t.Text =ls_label_livello_4
					dw_tab_asa_1.Object.cod_statistico_5_t.Text =ls_label_livello_5
					dw_tab_asa_1.Object.cod_statistico_6_t.Text =ls_label_livello_6
					dw_tab_asa_1.Object.cod_statistico_7_t.Text =ls_label_livello_7
			   	dw_tab_asa_1.Object.cod_statistico_8_t.Text =ls_label_livello_8
					dw_tab_asa_1.Object.cod_statistico_9_t.Text =ls_label_livello_9
					dw_tab_asa_1.Object.cod_statistico_10_t.Text =ls_label_livello_10
					dw_tab_asa_1.Object.cod_statistico_1.visible = 1
					dw_tab_asa_1.Object.cod_statistico_2.visible = 1
					dw_tab_asa_1.Object.cod_statistico_3.visible = 1
					dw_tab_asa_1.Object.cod_statistico_4.visible = 1
					dw_tab_asa_1.Object.cod_statistico_5.visible = 1
					dw_tab_asa_1.Object.cod_statistico_6.visible = 1
					dw_tab_asa_1.Object.cod_statistico_7.visible = 1
					dw_tab_asa_1.Object.cod_statistico_8.visible = 1
					dw_tab_asa_1.Object.cod_statistico_9.visible = 1
					dw_tab_asa_1.Object.cod_statistico_10.visible = 1
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_1",sqlca,&
               "tab_statistica_1","cod_statistico_1","des_statistico_1","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_2",sqlca,&
               "tab_statistica_2","cod_statistico_2","des_statistico_2","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_3",sqlca,&
               "tab_statistica_3","cod_statistico_3","des_statistico_3","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_4",sqlca,&
               "tab_statistica_4","cod_statistico_4","des_statistico_4","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_5",sqlca,&
               "tab_statistica_5","cod_statistico_5","des_statistico_5","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_6",sqlca,&
               "tab_statistica_6","cod_statistico_6","des_statistico_6","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_7",sqlca,&
               "tab_statistica_7","cod_statistico_7","des_statistico_7","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_8",sqlca,&
               "tab_statistica_8","cod_statistico_8","des_statistico_8","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_9",sqlca,&
               "tab_statistica_9","cod_statistico_9","des_statistico_9","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	
	         	f_PO_LoadDDDW_DW(dw_tab_asa_1,"cod_statistico_10",sqlca,&
               "tab_statistica_10","cod_statistico_10","des_statistico_10","cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi='" + ls_cod_tipo_analisi + "'")	

			end choose
		parent.triggerevent("pc_retrieve")

end if
end event

type st_cod_tipo_analisi from statictext within w_tab_asa_1
int X=23
int Y=21
int Width=389
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
long BorderColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_gestione from dropdownlistbox within w_tab_asa_1
int X=412
int Y=121
int Width=915
int Height=781
int TabOrder=50
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
string Item[]={"Offerte Acquisto",&
"Offerte Vendita",&
"Ordini Acquisto",&
"Ordini Vendita",&
"Fatture Acquisto",&
"Bolle Vendita"}
end type

event selectionchanged;choose case ddlb_gestione.text
	case "Offerte Acquisto"
		st_gestioni.text="A"

	case "Offerte Vendita"
		st_gestioni.text="E"

	case "Ordini Acquisto"
		st_gestioni.text="B"

	case "Ordini Vendita"
		st_gestioni.text="F"

	case "Fatture Acquisto"
		st_gestioni.text="D"

	case "Bolle Vendita"
		st_gestioni.text="G"

end choose

parent.triggerevent("pc_retrieve")
end event

type em_da_data from editmask within w_tab_asa_1
int X=1966
int Y=21
int Width=389
int Height=81
int TabOrder=30
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;parent.triggerevent("pc_retrieve")
end event

type st_2 from statictext within w_tab_asa_1
int X=1349
int Y=21
int Width=613
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Da Data Registrazione:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_a_data from editmask within w_tab_asa_1
int X=1966
int Y=121
int Width=389
int Height=81
int TabOrder=40
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;parent.triggerevent("pc_retrieve")
end event

type st_3 from statictext within w_tab_asa_1
int X=1349
int Y=141
int Width=618
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="A Data Registrazione:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_gestioni from statictext within w_tab_asa_1
int X=23
int Y=121
int Width=389
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
long BorderColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

