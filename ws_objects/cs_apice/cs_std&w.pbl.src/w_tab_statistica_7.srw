﻿$PBExportHeader$w_tab_statistica_7.srw
$PBExportComments$Window w_tab_statistica_7
forward
global type w_tab_statistica_7 from w_cs_xx_principale
end type
type dw_tab_statistica_7_lista from uo_cs_xx_dw within w_tab_statistica_7
end type
type dw_tab_statistica_7_dett from uo_cs_xx_dw within w_tab_statistica_7
end type
end forward

global type w_tab_statistica_7 from w_cs_xx_principale
int Width=1815
int Height=877
boolean TitleBar=true
string Title="Statistica 1"
dw_tab_statistica_7_lista dw_tab_statistica_7_lista
dw_tab_statistica_7_dett dw_tab_statistica_7_dett
end type
global w_tab_statistica_7 w_tab_statistica_7

on w_tab_statistica_7.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_statistica_7_lista=create dw_tab_statistica_7_lista
this.dw_tab_statistica_7_dett=create dw_tab_statistica_7_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_statistica_7_lista
this.Control[iCurrent+2]=dw_tab_statistica_7_dett
end on

on w_tab_statistica_7.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_statistica_7_lista)
destroy(this.dw_tab_statistica_7_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_statistica_7_lista.set_dw_key("cod_azienda")
dw_tab_statistica_7_lista.set_dw_key("cod_tipo_analisi")

dw_tab_statistica_7_lista.set_dw_options(sqlca, i_openparm, c_scrollparent,c_default)
dw_tab_statistica_7_dett.set_dw_options(sqlca,dw_tab_statistica_7_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_statistica_7_lista
end event

type dw_tab_statistica_7_lista from uo_cs_xx_dw within w_tab_statistica_7
int X=23
int Y=21
int Width=1738
int Height=441
int TabOrder=10
string DataObject="d_tab_statistica_7_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_tipo_analisi,ls_etichetta

ls_cod_tipo_analisi = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_analisi")
l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_analisi)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

ls_etichetta=i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "label_livello_7")
if isnull(ls_etichetta) then ls_etichetta=""
dw_tab_statistica_7_dett.object.cod_statistico_7_t.Text = "Cod."+ls_etichetta
dw_tab_statistica_7_lista.object.cod_statistico_7_t.Text = "Cod."+ls_etichetta

parent.title=ls_etichetta
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long l_idx
	string ls_cod_tipo_analisi

	ls_cod_tipo_analisi = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_analisi")

	FOR l_Idx = 1 TO RowCount()
		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
   		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		   SetItem(l_Idx, "cod_tipo_analisi", ls_cod_tipo_analisi)
  		end if
	NEXT
end if
end event

type dw_tab_statistica_7_dett from uo_cs_xx_dw within w_tab_statistica_7
int X=23
int Y=481
int Width=1738
int Height=281
int TabOrder=20
string DataObject="d_tab_statistica_7_dett"
BorderStyle BorderStyle=StyleRaised!
end type

