﻿$PBExportHeader$w_tab_tipi_analisi.srw
$PBExportComments$Window  tab_tipi_analisi
forward
global type w_tab_tipi_analisi from w_cs_xx_principale
end type
type dw_tab_tipi_analisi_lista from uo_cs_xx_dw within w_tab_tipi_analisi
end type
type cb_liv_1 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_2 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_3 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_4 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_5 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_6 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_7 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_8 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_9 from commandbutton within w_tab_tipi_analisi
end type
type cb_liv_10 from commandbutton within w_tab_tipi_analisi
end type
type dw_tab_tipi_analisi_dett from uo_cs_xx_dw within w_tab_tipi_analisi
end type
end forward

global type w_tab_tipi_analisi from w_cs_xx_principale
int Width=2529
int Height=1385
boolean TitleBar=true
string Title="Tipi Analisi"
dw_tab_tipi_analisi_lista dw_tab_tipi_analisi_lista
cb_liv_1 cb_liv_1
cb_liv_2 cb_liv_2
cb_liv_3 cb_liv_3
cb_liv_4 cb_liv_4
cb_liv_5 cb_liv_5
cb_liv_6 cb_liv_6
cb_liv_7 cb_liv_7
cb_liv_8 cb_liv_8
cb_liv_9 cb_liv_9
cb_liv_10 cb_liv_10
dw_tab_tipi_analisi_dett dw_tab_tipi_analisi_dett
end type
global w_tab_tipi_analisi w_tab_tipi_analisi

on w_tab_tipi_analisi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_tipi_analisi_lista=create dw_tab_tipi_analisi_lista
this.cb_liv_1=create cb_liv_1
this.cb_liv_2=create cb_liv_2
this.cb_liv_3=create cb_liv_3
this.cb_liv_4=create cb_liv_4
this.cb_liv_5=create cb_liv_5
this.cb_liv_6=create cb_liv_6
this.cb_liv_7=create cb_liv_7
this.cb_liv_8=create cb_liv_8
this.cb_liv_9=create cb_liv_9
this.cb_liv_10=create cb_liv_10
this.dw_tab_tipi_analisi_dett=create dw_tab_tipi_analisi_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_tipi_analisi_lista
this.Control[iCurrent+2]=cb_liv_1
this.Control[iCurrent+3]=cb_liv_2
this.Control[iCurrent+4]=cb_liv_3
this.Control[iCurrent+5]=cb_liv_4
this.Control[iCurrent+6]=cb_liv_5
this.Control[iCurrent+7]=cb_liv_6
this.Control[iCurrent+8]=cb_liv_7
this.Control[iCurrent+9]=cb_liv_8
this.Control[iCurrent+10]=cb_liv_9
this.Control[iCurrent+11]=cb_liv_10
this.Control[iCurrent+12]=dw_tab_tipi_analisi_dett
end on

on w_tab_tipi_analisi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_tipi_analisi_lista)
destroy(this.cb_liv_1)
destroy(this.cb_liv_2)
destroy(this.cb_liv_3)
destroy(this.cb_liv_4)
destroy(this.cb_liv_5)
destroy(this.cb_liv_6)
destroy(this.cb_liv_7)
destroy(this.cb_liv_8)
destroy(this.cb_liv_9)
destroy(this.cb_liv_10)
destroy(this.dw_tab_tipi_analisi_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_tipi_analisi_lista.set_dw_key("cod_azienda")
dw_tab_tipi_analisi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_tipi_analisi_dett.set_dw_options(sqlca,dw_tab_tipi_analisi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_tipi_analisi_lista
end event

type dw_tab_tipi_analisi_lista from uo_cs_xx_dw within w_tab_tipi_analisi
int X=23
int Y=21
int Width=2446
int Height=441
int TabOrder=100
string DataObject="d_tab_tipi_analisi_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if rowcount()=0 then
	cb_liv_1.enabled=false
	cb_liv_2.enabled=false
	cb_liv_3.enabled=false
	cb_liv_4.enabled=false
	cb_liv_5.enabled=false
	cb_liv_6.enabled=false
	cb_liv_7.enabled=false
	cb_liv_8.enabled=false
	cb_liv_9.enabled=false
	cb_liv_10.enabled=false
end if

end event

event pcd_setkey;call super::pcd_setkey;long l_idx

FOR l_Idx = 1 TO RowCount()
	IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
   	SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
  	end if
NEXT
end event

event pcd_new;call super::pcd_new;if i_extendmode then	
	cb_liv_1.enabled=false
	cb_liv_2.enabled=false
	cb_liv_3.enabled=false
	cb_liv_4.enabled=false
	cb_liv_5.enabled=false
	cb_liv_6.enabled=false
	cb_liv_7.enabled=false
	cb_liv_8.enabled=false
	cb_liv_9.enabled=false
	cb_liv_10.enabled=false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then	
	string ls_test,ls_cod_tipo_analisi
	
	ls_cod_tipo_analisi=getitemstring(getrow(),"cod_tipo_analisi")
	cb_liv_1.enabled=true
	cb_liv_2.enabled=true
	cb_liv_3.enabled=true
	cb_liv_4.enabled=true
	cb_liv_5.enabled=true
	cb_liv_6.enabled=true
	cb_liv_7.enabled=true
	cb_liv_8.enabled=true
	cb_liv_9.enabled=true
	cb_liv_10.enabled=true

	select cod_azienda
	into   :ls_test
	from   tab_path_esportazioni
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_analisi=:ls_cod_tipo_analisi;
	
	if sqlca.sqlcode=100 then

	   INSERT INTO tab_path_esportazioni  
   	      		(cod_azienda,   
      	    	 	 cod_tipo_analisi,   
         	  		 path_righe,   
           			 path_statistica_1,   
           			 path_statistica_2,   
	           		 path_statistica_3,   
   	        		 path_statistica_4,   
      	     		 path_statistica_5,   
         	  		 path_statistica_6,   
           			 path_statistica_7,   
           			 path_statistica_8,   
	           		 path_statistica_9,   
   	        		 path_statistica_10)  
	   VALUES ( :s_cs_xx.cod_azienda,   
   	         :ls_cod_tipo_analisi,   
      	      null,   
         	   null,   
            	null,   
	            null,   
   	         null,   
      	      null,   
         	   null,   
            	null,   
	            null,   
   	         null,   
      	      null )  ;
	end if

end if

end event

event pcd_view;call super::pcd_view;if i_extendmode then	
	cb_liv_1.enabled=true
	cb_liv_2.enabled=true
	cb_liv_3.enabled=true
	cb_liv_4.enabled=true
	cb_liv_5.enabled=true
	cb_liv_6.enabled=true
	cb_liv_7.enabled=true
	cb_liv_8.enabled=true
	cb_liv_9.enabled=true
	cb_liv_10.enabled=true
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then	
	cb_liv_1.enabled=false
	cb_liv_2.enabled=false
	cb_liv_3.enabled=false
	cb_liv_4.enabled=false
	cb_liv_5.enabled=false
	cb_liv_6.enabled=false
	cb_liv_7.enabled=false
	cb_liv_8.enabled=false
	cb_liv_9.enabled=false
	cb_liv_10.enabled=false
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	string ls_cod_tipo_analisi
	long ll_righe
	
	for ll_righe=1 to deletedcount()	
		ls_cod_tipo_analisi=getitemstring(ll_righe,"cod_tipo_analisi",delete!,true)

		delete from tab_path_esportazioni
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_tipo_analisi=:ls_cod_tipo_analisi;

	next

end if
end event

type cb_liv_1 from commandbutton within w_tab_tipi_analisi
int X=1166
int Y=709
int Width=92
int Height=81
int TabOrder=90
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_1, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_2 from commandbutton within w_tab_tipi_analisi
int X=1166
int Y=789
int Width=92
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_2, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_3 from commandbutton within w_tab_tipi_analisi
int X=1166
int Y=869
int Width=92
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_3, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_4 from commandbutton within w_tab_tipi_analisi
int X=1166
int Y=949
int Width=92
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_4, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_5 from commandbutton within w_tab_tipi_analisi
int X=1166
int Y=1029
int Width=92
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_5, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_6 from commandbutton within w_tab_tipi_analisi
int X=2355
int Y=709
int Width=92
int Height=81
int TabOrder=70
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_6, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_7 from commandbutton within w_tab_tipi_analisi
int X=2355
int Y=789
int Width=92
int Height=81
int TabOrder=80
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_7, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_8 from commandbutton within w_tab_tipi_analisi
int X=2355
int Y=869
int Width=92
int Height=81
int TabOrder=60
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_8, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_9 from commandbutton within w_tab_tipi_analisi
int X=2355
int Y=949
int Width=92
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_9, -1, dw_tab_tipi_analisi_dett)
end event

type cb_liv_10 from commandbutton within w_tab_tipi_analisi
int X=2355
int Y=1029
int Width=92
int Height=81
int TabOrder=21
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_tab_statistica_10, -1, dw_tab_tipi_analisi_dett)
end event

type dw_tab_tipi_analisi_dett from uo_cs_xx_dw within w_tab_tipi_analisi
int X=23
int Y=481
int Width=2446
int Height=781
int TabOrder=110
string DataObject="d_tab_tipi_analisi_dett"
BorderStyle BorderStyle=StyleRaised!
end type

