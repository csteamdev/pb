﻿$PBExportHeader$uo_impresa.sru
$PBExportComments$UserObject  connessione con impresa
forward
global type uo_impresa from nonvisualobject
end type
type str_centro_costo from structure within uo_impresa
end type
type str_dati_centro_costo from structure within uo_impresa
end type
end forward

type str_centro_costo from structure
	string		cod_centro_costo
	long		id_centro_costo
	decimal { 4 }		percentuale
	decimal { 4 }		importo_dare
	decimal { 4 }		importo_avere
end type

type str_dati_centro_costo from structure
	long		id_conto
	str_centro_costo		sstr_centro_costo[]
end type

global type uo_impresa from nonvisualobject
end type
global uo_impresa uo_impresa

type variables
transaction			i_impresa
string					i_messaggio, is_gestione_cc, is_mov_intra = "", is_provincia_azienda=""
long					i_db_errore
boolean				ib_MVI = false
boolean				ib_XXX = false


private:
	string				is_tipi_det_acq_trasp="", is_tipi_det_ven_trasp=""


end variables

forward prototypes
public function long uof_transaction (boolean ib_commit)
public function string uof_codice_cli_for (string fs_tipo_anagrafica, string fs_cod_capoconto, string fs_codice)
public function long uof_contabilizza_fat_acq (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_id_reg_pd)
public function long uof_contabilizza_fat_ven (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_id_reg_pd)
public function integer uof_con_pagamento (string fs_cod_pagamento, string fs_azione, ref string fs_errore)
public function integer uof_app_pagamento (string fs_cod_pagamento, string fs_azione, ref string fs_errore)
public function integer uof_saldo_conto (string fs_conto, datetime fdt_data_riferimento, ref decimal fd_saldo, ref string fs_error)
public function integer uof_saldo_conto_centro_costo (string fs_conto, string fs_piano_centro_costo, string fs_centro_costo, datetime fdt_data_riferimento, ref decimal fd_saldo, ref string fs_error)
public function long uof_tab_iva (string flag_tipo_operazione, string fs_cod_iva, string fs_des_iva, string fs_des_estesa, double fd_perc_iva, double fd_perc_detraibile)
public function integer uof_saldo_conto_periodo (string fs_conto, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_saldo, ref string fs_errore)
public function integer uof_saldo_conto_cc_periodo (string fs_conto, string fs_piano_centro_costo, string fs_centro_costo, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_saldo, ref string fs_errore)
public function integer uof_flag_predefiniti (string fs_tipo_soggetto, string fs_tipo_pagamento, string fs_modo_uso_banca, ref string fs_flag_banca_predefinita, ref string fs_flag_docfin_predefinita)
public function integer uof_reg_ivacee (long al_id_prof_registrazione, ref long al_id_conta_iva_acq_cee)
public function string ouf_saldo_conto_impresa (ref decimal ad_saldo, string as_cod_conto_impresa, ref string as_nome_conto)
public function decimal uof_saldo_partite_aperte (string as_tipo_anagrafica, string as_codice, string as_flag_italia_estero_ue, date ad_data_rif)
public function integer uof_top_credit (long al_num_elementi, string as_flag_tipo_scadenza, string as_flag_italia_estero_ue, datetime adt_data_scadenza, ref datastore ads_data, ref string as_errore)
public function long uof_abicab (string fs_tipo_operazione, long fi_id_anagrafica, long fi_id_con_pagamento, string fs_tipo_anagrafica, string fs_cod_abi, string fs_des_abi, string fs_cod_cab, string fs_des_cab, string fs_cin, string fs_des_banca, string fs_tipo_banca, string fs_iban, string fs_swift, ref long fi_id_banca_appoggio, ref long fi_id_banca_sog_commerciale)
public function long uof_crea_cli_for (string fs_tipo_anagrafica, string fs_codice, string fs_cod_capoconto, string fs_partita_iva, string fs_cod_fiscale, string fs_rag_soc_1, string fs_rag_soc_2, string fs_indirizzo, string fs_localita, string fs_cap, string fs_provincia, string fs_frazione, string fs_cod_abi, string fs_cod_cab, string fs_cin, string fs_cod_abi_1, string fs_cod_cab_1, string fs_cin_1, string fs_cod_pagamento, string fs_cod_valuta, string fs_cod_nazione, long fl_ggmm_esclusione_1_da, long fl_ggmm_esclusione_1_a, long fl_ggmm_esclusione_2_da, long fl_ggmm_esclusione_2_a, long fl_data_sost_1, long fl_data_sost_2, string fs_flag_raggruppo_scadenze, string fs_telefono, string fs_fax, string fs_telex, string fs_email, string fs_cod_agente_1, string fs_iban, string fs_swift, long fl_id_doc_fic_rating, long fl_id_doc_fic_voce_fin, string fs_flag_tipo_cli_for, ref long fi_id_anagrafica, ref long fi_id_sog_commerciale)
public function integer uof_mov_intra (long al_id_reg_pd, integer ai_anno_doc, long al_num_doc, string as_tipo_doc)
public function integer uof_crea_nota_contabilizzazione (long al_id_reg_pd, integer ai_anno_doc, long al_num_doc, string as_tipo_doc)
public function integer uof_nazione_intra (long al_id_reg_pd, integer ai_anno_doc, long al_num_doc, string as_tipo_doc)
public function integer uof_dati_aggiuntivi_intra (long al_id_reg_pd, string as_cod_mezzo, string as_cod_porto, string as_cod_natura_intra)
public function integer uof_prepare_ds_mov_intra (integer ai_anno_doc, long al_num_doc, string as_tipo_doc, ref s_cs_xx_parametri astr_data[])
public function decimal uof_get_unita_supplettiva (string as_cod_prodotto, decimal ad_qta)
public function string uof_get_tipo_det_acq_trasp ()
public function string uof_get_tipo_det_ven_trasp ()
public function integer uof_get_imponibile_trasporto (string as_tipo_doc, integer ai_anno_doc, long al_num_doc, ref decimal ad_imponibile, ref decimal ad_imponibile_valuta)
public function string uof_tipi_det_ven_trasporti ()
public function string uof_tipi_det_acq_trasporti ()
public subroutine uof_set_tipo_det_trasporto ()
public function integer uof_get_identity (ref long al_identity)
end prototypes

public function long uof_transaction (boolean ib_commit);if ib_commit then
	commit using i_impresa;
else
	rollback using i_impresa;
end if
return 0
end function

public function string uof_codice_cli_for (string fs_tipo_anagrafica, string fs_cod_capoconto, string fs_codice);string ls_mastro, ls_codice_cliente, ls_str, ls_cod_conto
long ll_lunghezza_codice
// ******************* aggiunto per nostra gestione interna del codice ***************
//if fs_tipo_anagrafica = "C" then
//	select cod_conto
//	into   :ls_cod_conto
//	from   anag_clienti
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_clienteo = :fs_codice;
//	if sqlca.sqlcode = 0 and not isnull(ls_cod_conto) and len(ls_cod_conto) > 0 then
//		return ls_cod_conto
//	end if
//end if

// ***********************************************************************************


if fs_tipo_anagrafica = "C" then
	select stringa
	into   :ls_str
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'MCL';
	if sqlca.sqlcode <> 0 then
		ls_mastro = "CL"
	else
		ls_mastro = ls_str
	end if
else
	select stringa
	into   :ls_str
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'MFO';
	if sqlca.sqlcode <> 0 then
		ls_mastro = "FO"
	else
		ls_mastro = ls_str
	end if
end if

// leggo parametro generale per lunghezza codice
select numero
into   	:ll_lunghezza_codice
from   parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LCI';
if sqlca.sqlcode <> 0 then
	ll_lunghezza_codice = 4
end if


if len(fs_codice) < ll_lunghezza_codice then 
	if isnull(fs_cod_capoconto) or len(fs_cod_capoconto) < 1 then fs_cod_capoconto = "01"
end if

if ll_lunghezza_codice = 6 then
	// impostazione che dipende dal parametro LCI
	
	choose case len(fs_codice)
		case 6
			if isnull(fs_cod_capoconto) or len(fs_cod_capoconto) < 1 then 
				ls_codice_cliente = ls_mastro + "00" + fs_codice
			else
				ls_codice_cliente = ls_mastro + fs_cod_capoconto + fs_codice
			end if
		case 5
			if isnull(fs_cod_capoconto) or len(fs_cod_capoconto) < 1 then 
				ls_codice_cliente = ls_mastro + "0" + fs_codice
			else
				ls_codice_cliente = ls_mastro + fs_cod_capoconto + "0" + fs_codice
			end if
		case 4
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + "00" + fs_codice
		case 3
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + "000" + fs_codice
		case 2
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + "0000" + fs_codice
		case 1
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + "00000" + fs_codice
	end choose				
	
else	
	// impostazione libera lunghezza
	
	choose case len(fs_codice)
		case 6
			if isnull(fs_cod_capoconto) or len(fs_cod_capoconto) < 1 then 
				ls_codice_cliente = ls_mastro + fs_codice
			else
				ls_codice_cliente = ls_mastro + fs_cod_capoconto + right(fs_codice,4)
			end if
		case 5
			if isnull(fs_cod_capoconto) or len(fs_cod_capoconto) < 1 then 
				ls_codice_cliente = ls_mastro + "0" + fs_codice
			else
				ls_codice_cliente = ls_mastro + fs_cod_capoconto + right(fs_codice,4)
			end if
		case 4
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + fs_codice
		case 3
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + "0" + fs_codice
		case 2
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + "00" + fs_codice
		case 1
			ls_codice_cliente = ls_mastro + fs_cod_capoconto + "000" + fs_codice
	end choose	
	
end if

return ls_codice_cliente
end function

public function long uof_contabilizza_fat_acq (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_id_reg_pd);u_reg_pd_base lu_reg_pd

lu_reg_pd = CREATE u_reg_pd_base
lu_reg_pd.it_tran = sqlci
lu_reg_pd.uf_set_transaction(sqlci)


// ------------- LEGGO DATI FATTURA DI ACQUISTO -----------------------------------------------
boolean	lb_commit = false, lb_conto_iva[], lb_conto_iva_alt[], lb_ivacee

char		ls_dare_avere[]

integer	li_anno_partita

string		ls_cod_tipo_fat_acq, ls_cod_fornitore, ls_cod_valuta, ls_cod_pagamento, ls_cod_valuta_impresa, ls_des_contabile[],&
			ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_flag_movimenti, ls_flag_contabilita, ls_rag_soc_1,&
			ls_cod_banca_clien_for, ls_flag_blocco, ls_cod_capoconto, ls_flag_tipo_pagamento, ls_flag_bolli, ls_iban, &
			ls_flag_partenza, ls_flag_iva_prima_rata, ls_flag_cal_commerciale, ls_null, ls_cod_iva, ls_sql, ls_messaggio, &
			ls_segno_contropartita, ls_flag_dare_avere, ls_cod_conto, ls_valuta_default, ls_flag_tipo_fat_acq, ls_segno_partita, &
			ls_num_documento, ls_protocollo, ls_cin, ls_profilo_impresa, ls_esercizio,ls_conto_impresa, ls_codice_fornitore, &
			ls_cod_abi, ls_cod_cab, ls_cod_tipo_pagamento, ls_cod_pag_impresa, ls_scad_raggruppate, ls_des_banca,ls_des_abi,ls_des_cab, &
			ls_tipo_banca,ls_flag_chiusa_forzatamente, ls_cod_centro_costo, ls_conto_impresa_cc,ls_des_prof_registrazione, ls_cod_banca_scadenza, ls_swift, &
			ls_errore_reg_pd, ls_num_partita
		 
long		ll_ret, li_id_valuta,li_id_prof_registrazione, li_id_esercizio, li_id_conto_iva,li_id_tab_iva[], ll_cont, &
			li_id_conto[], li_id_sog_commerciale, li_id_tipo_pagamento, li_id_conto_fornitore, li_id_con_pagamento, &
			li_id_banca_sog_commerciale, li_return,li_id_banca_appoggio, li_id_anagrafica,li_id_banca_azienda, ll_righe_scadenza
		 
long		ll_giorno_fisso_scadenza, ll_mese_escluso1, ll_mese_escluso2, ll_giorno_sost1, ll_giorno_sost2, ll_int_partenza, &
			ll_num_rate_com, ll_perc_prima_rata, ll_anno_documento, ll_num_gior_prima_rata, ll_num_gior_int_rata, &
			ll_perc_ult_rata, ll_num_gior_ult_rata, ll_num_documento, ll_i, ll_ll_numerorecord, ll_numerorecord, ll_cont_cc, &
			ll_id_mov_contabile, ll_id_cc_conto, ll_id_conto_cc, ll_id_conto_iva_iv
		 
dec{4}	ld_cambio_acq, ld_tot_merci, ld_tot_spese_trasporto, ld_tot_spese_imballo, ld_tot_spese_bolli, ld_tot_spese_varie, &
			ld_tot_sconto_cassa, ld_tot_sconti_commerciali, ld_imponibile_provvigioni_1, ld_imponibile_provvigioni_2, &
			ld_tot_provvigioni_1, ld_tot_provvigioni_2, ld_importo_iva, ld_imponibile_iva, ld_importo_iva_valuta, ld_importo_ctp[], &
			ld_imponibile_iva_valuta, ld_tot_fattura, ld_tot_fattura_valuta, ld_imponibile[], ld_imposta[], ld_iva_importo, &
			ld_perc_iva, ld_imp_riga, ld_salva_protocollo, ld_rata_scadenza, ld_rata_scadenza_val, ld_dare_scadenza, &
			ld_avere_scadenza, ld_dare_valuta_scadenza, ld_avere_valuta_scadenza, ld_cambio,  ld_saldo,	ld_saldo_valuta, &
			ld_controllo_quadratura_cc, ld_percentuale_cc, ld_importo_cc, ld_cambio_alla_data_documento
		 
datetime ldt_data_registrazione, ldt_data_fattura, ldt_salva_data, ldt_oggi, ldt_data_scadenza, ldt_data_protocollo

datastore	lds_scadenze

str_dati_centro_costo					lstr_dati_cc[]
str_centro_costo						lstr_cc[], lstr_vuoto[]
s_reg_pd_cc_mov_contabile			lstr_centri_costo[]

//  Come riempire gli array se la registrazione è ivacee  


// 1. da par_sistema prendere conto iva vendite id_conto_iva_iv
// 2. testata imp_valuta = imposta + imponibile, importo = imponibile
// 3 righe 
//    per la riga del fornitore ldc_importo_mc[ll_l] = ldc_importo_mc[ll_l] - uf_get_imposta()
//									   ldc_importo_val_mc[ll_l] = ldc_importo_val_mc[ll_l] - uf_get_imposta_val()
// 4 dopo aver inserito il conto iva

select cod_tipo_fat_acq,   
       cod_fornitore,   
       cod_valuta,   
       cambio_acq,   
       cod_pagamento,   
       cod_banca,   
       flag_fat_confermata,   
       flag_contabilita,   
       tot_merci,   
       tot_spese_trasporto,   
       tot_spese_imballo,   
       tot_spese_bolli,   
       tot_spese_varie,   
       tot_sconti_commerciali,   
       imponibile_provvigioni_1,   
       imponibile_provvigioni_2,   
       tot_provvigioni_1,   
       tot_provvigioni_2,   
       importo_iva,   
       imponibile_iva,   
       importo_valuta_iva,   
       imponibile_valuta_iva,   
       tot_val_fat_acq,   
       tot_valuta_fat_acq,   
       cod_banca_clien_for,   
		 anno_doc_origine,
		 data_doc_origine,
		 num_doc_origine,
		 data_registrazione,
		 protocollo,
		 data_protocollo
into   :ls_cod_tipo_fat_acq,   
       :ls_cod_fornitore,   
       :ls_cod_valuta,   
       :ld_cambio_acq,   
       :ls_cod_pagamento,   
       :ls_cod_banca,   
       :ls_flag_movimenti,   
       :ls_flag_contabilita,   
       :ld_tot_merci,   
       :ld_tot_spese_trasporto,   
       :ld_tot_spese_imballo,   
       :ld_tot_spese_bolli,   
       :ld_tot_spese_varie,   
       :ld_tot_sconti_commerciali,   
       :ld_imponibile_provvigioni_1,   
       :ld_imponibile_provvigioni_2,   
       :ld_tot_provvigioni_1,   
       :ld_tot_provvigioni_2,   
       :ld_importo_iva,   
       :ld_imponibile_iva,   
       :ld_importo_iva_valuta,   
       :ld_imponibile_iva_valuta,   
       :ld_tot_fattura,   
       :ld_tot_fattura_valuta,   
       :ls_cod_banca_clien_for,   
		 :ll_anno_documento,
		 :ldt_data_fattura,
		 :ll_num_documento,
		 :ldt_data_registrazione,
		 :ls_protocollo,
		 :ldt_data_protocollo
from   tes_fat_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore durante ricerca fattura da contabilizzare." + sqlca.sqlerrtext
	i_db_errore    = sqlca.sqlcode
	return -1
end if

// ---------------- Eseguo controllo del cambio ------------------------------ //

guo_functions.uof_get_parametro_azienda( "LIR", ls_valuta_default)

// se valuta diversa da valuta default
if ls_valuta_default <> ls_cod_valuta then
	ld_cambio_alla_data_documento = f_cambio_val_acq(ls_cod_valuta, ldt_data_protocollo)
	if ld_cambio_alla_data_documento <> ld_cambio_acq then
		i_messaggio = "ATTENZIONE: la fattura in valuta presenta un cambio non coerente con la data del documento. "
		i_db_errore    = 0
		return -1
	end if
end if

select	cod_conto,
			rag_soc_1,
			swift
into	:ls_cod_capoconto, 
		:ls_rag_soc_1,
		:ls_swift
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_fornitore = :ls_cod_fornitore;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore durante ricerca capoconto in anagrafica fornitori." + sqlca.sqlerrtext
	i_db_errore    = sqlca.sqlcode
	return -1
end if

select codice_impresa
into   :ls_cod_valuta_impresa
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta  = :ls_cod_valuta;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore durante ricerca codice valuta impresa in tabella valute di APICE" + sqlca.sqlerrtext
	i_db_errore    = sqlca.sqlcode
	return -1
end if
		 
select id_valuta
into   :li_id_valuta
from   valuta
where  codice = :ls_cod_valuta_impresa
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca ID della valuta " + ls_cod_valuta_impresa + " in IMPRESA. " + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

// ----------------------------  memo dati testata -----------------------------------------------------------

lu_reg_pd.uf_reset()
lu_reg_pd.uf_displaymode(0)
lu_reg_pd.uf_autocommit(false)

//select id_conto_iva_iv
//into :ll_id_conto_iva_iv
//from par_sistema
//using sqlci;
//if sqlci.sqlcode <> 0 then
//	i_messaggio = "Errore in ricerca conto IVA VENDITE CEE in parametri sistema " + ls_profilo_impresa + " in IMPRESA. " + sqlci.sqlerrtext
//	i_db_errore = sqlci.sqlcode
//	return -1
//end if

select profilo_impresa, flag_tipo_fat_acq
into   :ls_profilo_impresa, :ls_flag_tipo_fat_acq
from   tab_tipi_fat_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca profilo registrazione e tipo fattura in tabella tipi fatture" + sqlca.sqlerrtext
	i_db_errore = sqlca.sqlcode
	return -1
end if

if isnull(ls_profilo_impresa) or len(ls_profilo_impresa) < 1 then
	i_messaggio = "Nel tipo fattura non è stato impostato il profilo registrazione di impresa."
	i_db_errore = 0
	return -1
end if

select id_prof_registrazione, id_conto_iva, descrizione
into   :li_id_prof_registrazione, :li_id_conto_iva, :ls_des_prof_registrazione
from   prof_registrazione
where  codice = :ls_profilo_impresa
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca ID profilo e conto iva nel profilo " + ls_profilo_impresa + " in IMPRESA. " + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

if ls_flag_tipo_fat_acq <> "N" then
	ls_segno_partita = "A"					// è una fattura allora metto la partita in dare e le ctp in avere	
	ls_segno_contropartita = "D"
else
	ls_segno_partita = "D"					// è una nc allora metto la partita in dare e le ctp in avere	
	ls_segno_contropartita = "A"
end if

lu_reg_pd.uf_id_prof_registrazione(li_id_prof_registrazione)

// EnMe 25/06/2014 carico variabile a valore TRUE se registrazione IVACEE e prelevo anche il conto iva per acquisti CEE
ll_ret = uof_reg_ivacee(li_id_prof_registrazione, ref ll_id_conto_iva_iv)

choose case ll_ret
	case 1
		lb_ivacee = true
		lu_reg_pd.uf_doc_intra("S")
	case 0
		lb_ivacee = false
		lu_reg_pd.uf_doc_intra("N")
	case else
		return -1
end choose

lu_reg_pd.uf_attiva_wizard( lb_ivacee )
lu_reg_pd.il_id_wizard_ivacee = 5

// se è una fattura CEE allora il totale documento per la quadratura è l'imponibile, ma il totale fattura è sempre IVA inclusa.
lu_reg_pd.uf_importo(ld_tot_fattura)
lu_reg_pd.uf_imp_valuta(ld_tot_fattura_valuta)
if lb_ivacee then
	ld_tot_fattura = ld_imponibile_iva
	ld_tot_fattura_valuta = ld_imponibile_iva_valuta
end if

select stringa
into   :ls_esercizio
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ECI';
if sqlca.sqlcode = 100 then
	ls_esercizio = string(ll_anno_documento)
elseif sqlca.sqlcode <> 0 then
	if sqlca.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca profilo registrazione e tipo fattura in tabella tipi fatture" + sqlca.sqlerrtext
		i_db_errore = sqlca.sqlcode
		return -1
	end if
end if	
	
select id_esercizio
into   :li_id_esercizio
from   esercizio
where  codice = :ls_esercizio
using  sqlci;
lu_reg_pd.uf_id_esercizio(li_id_esercizio)

lu_reg_pd.uf_data_registrazione(date(ldt_data_fattura))
lu_reg_pd.uf_num_documento(ls_protocollo)
lu_reg_pd.uf_num_progressivo(ll_num_documento)
lu_reg_pd.uf_data_documento(date(ldt_data_protocollo))

lu_reg_pd.uf_descrizione("")
if ls_flag_tipo_fat_acq = "N" then lu_reg_pd.uf_tipo_fattura("C")


//   --------------------------------- IMPOSTAZIONE DEI MOVIMENTI CONTABILI ------------------------

// -------------------------------------- carico struttura registro iva ----------------------------
// EnMe da sistemare per IVACEE: caricare i record iva
declare cu_iva dynamic cursor for sqlsa ;
ls_sql = "select cod_iva, imponibile_iva, importo_iva, perc_iva " + &
			"from   iva_fat_acq " + &
			"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			"anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
			"num_registrazione = " + string(fl_num_registrazione)

ll_i = 0
prepare sqlsa from :ls_sql using sqlca;
open dynamic cu_iva;
do while 1=1
   fetch cu_iva into :ls_cod_iva, :ld_imponibile_iva, :ld_iva_importo, :ld_perc_iva;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	
	select id_tab_iva
	into   :li_id_tab_iva[ll_i]
	from   tab_iva
	where  codice = :ls_cod_iva
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca aliquota iva " + ls_cod_iva + ". " + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	
	ld_imponibile[ll_i] = ld_imponibile_iva
	ld_imposta[ll_i] = ld_iva_importo
loop
close cu_iva;

if ll_i > 0 then
	lu_reg_pd.uf_imponibile(ld_imponibile)
	lu_reg_pd.uf_id_tab_iva(li_id_tab_iva)
	lu_reg_pd.uf_imposta(ld_imposta)
end if	


// -------------------------- carico struttura contropartite contabili --------------------

declare cu_conti dynamic cursor for sqlsa;
ls_sql = "SELECT cod_conto, " + &
                "importo_riga, " + &
        			 "flag_dare_avere " + &
			"from   cont_fat_acq " + &
			"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			       "anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
			       "num_registrazione = " + string(fl_num_registrazione)

declare cu_conti_cc cursor for
select cod_centro_costo,   
       percentuale,   
       importo  
from   cont_fat_acq_cc
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_conto = :ls_cod_conto;

ll_i = 0

ls_codice_fornitore = uof_codice_cli_for("F", ls_cod_capoconto, ls_cod_fornitore)

select id_conto
into   :li_id_sog_commerciale
from   conto
where  codice = :ls_codice_fornitore
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca codice soggetto commerciale in contabilità " + ls_codice_fornitore + ". " + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

// Enme 04.11.2015: controllo che non esista stesso documento per lo stesso fornitore
select count(*)
into	:ll_cont
from	reg_pd
where num_documento = :ls_protocollo and
		data_documento = :ldt_data_protocollo and
		id_conto = :li_id_sog_commerciale
using sqlci;
if ll_cont > 0 then
	i_messaggio = "Il documento esiste già. Rif documento acquisti numero:" + ls_protocollo + "  data:" + string(ldt_data_protocollo) + "   fornitore:" + ls_codice_fornitore + ". " + sqlci.sqlerrtext
	i_db_errore = 0
	return -1
end if

// EnMe 04.11.2015 (fine)

lu_reg_pd.uf_id_conto(li_id_sog_commerciale)

//  creazione della partita fornitore
ll_i = ll_i + 1
lb_conto_iva[ll_i] = false
lb_conto_iva_alt[ll_i] = false
li_id_conto[ll_i] = li_id_sog_commerciale
ls_dare_avere[ll_i] = ls_segno_partita
ld_importo_ctp[ll_i] = ld_tot_fattura
	
// creazione della contropartita iva
ll_i = ll_i + 1
lb_conto_iva[ll_i] = true
lb_conto_iva_alt[ll_i] = true
li_id_conto[ll_i] = li_id_conto_iva
ls_dare_avere[ll_i] = ls_segno_contropartita
ld_importo_ctp[ll_i] = ld_importo_iva
// descrizione contabile della contropartita.
ls_des_contabile[ll_i] = left(ls_des_prof_registrazione + " " + ls_rag_soc_1, 80)

// --------------------  EnMe registrazione iva cee con segno opposto   -----------
IF lb_ivacee = TRUE THEN
		ll_i ++
		
		lb_conto_iva[ll_i] = false
		lb_conto_iva_alt[ll_i] = true
		li_id_conto[ll_i] = ll_id_conto_iva_iv
		
		IF ls_segno_contropartita = "D" THEN
			ls_dare_avere[ll_i] = "A"
		ELSE
			ls_dare_avere[ll_i] = "D"
		END IF
		
		ld_importo_ctp[ll_i] = ld_importo_iva
		//ldc_importo_val_mc[ll_l] = uf_get_imposta_val()
		
		//ld_data_inizio_competenza[ll_l] = uf_get_data_in_comp_imposta()
		//ld_data_fine_competenza[ll_l] = uf_get_data_fn_comp_imposta()
		
END IF

// --------------------------------------------------------------

prepare sqlsa from :ls_sql  using sqlca;
open dynamic cu_conti;

do while true
   fetch cu_conti into :ls_cod_conto, :ld_imp_riga, :ls_flag_dare_avere;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	
	select conto_impresa
	into   :ls_conto_impresa
	from   anag_piano_conti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_conto   = :ls_cod_conto;
	if sqlca.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca conto " + ls_cod_conto + " in piano dei conti" + sqlca.sqlerrtext
		i_db_errore = sqlca.sqlcode
		return -1
	end if
	
	if isnull(ls_conto_impresa) then
		i_messaggio = "Conto contabile di corrispondenza non impostato nella tabella piano dei conti di APICE"
		i_db_errore = sqlca.sqlcode
		return -1
	end if
	
	select id_conto
	into   :li_id_conto[ll_i]
	from   conto
	where  codice = :ls_conto_impresa
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca codice conto contabile " + ls_conto_impresa + ". " + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	
//	ls_dare_avere[ll_i] = ls_flag_dare_avere
	ls_dare_avere[ll_i] = ls_segno_contropartita
	ld_importo_ctp[ll_i] = ld_imp_riga
	lb_conto_iva[ll_i] = false
	
	// descrizione contabile della contropartita.
	ls_des_contabile[ll_i] = ls_des_prof_registrazione + " " + ls_rag_soc_1
	
	if is_gestione_cc = "S" then
	
		open cu_conti_cc;
		
		if sqlca.sqlcode <> 0 then
			i_messaggio = "Errore in OPEN cursore cu_conti_CC" + sqlca.sqlerrtext
			i_db_errore = sqlca.sqlcode
			return -1
		end if
		
		lstr_dati_cc[ll_i].id_conto = li_id_conto[ll_i]
		lstr_cc = lstr_vuoto
		ld_controllo_quadratura_cc = 0
		ll_cont_cc = 0
		
		do while true
			fetch cu_conti_cc into :ls_cod_centro_costo, :ld_percentuale_cc, :ld_importo_cc;
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode <> 0 then
				i_messaggio = "Errore in FETCH cursore cu_conti_CC" + sqlca.sqlerrtext
				i_db_errore = sqlca.sqlcode
				return -1
			end if
			
			ll_cont_cc ++
			
			lstr_cc[ll_cont_cc].cod_centro_costo = ls_cod_centro_costo
			lstr_cc[ll_cont_cc].percentuale      = ld_percentuale_cc
			
			if ls_segno_contropartita = "D" then
				if ld_importo_cc >= 0 then
					lstr_cc[ll_cont_cc].importo_dare  = ld_importo_cc
					lstr_cc[ll_cont_cc].importo_avere = 0
				else
					lstr_cc[ll_cont_cc].importo_dare  = 0
					lstr_cc[ll_cont_cc].importo_avere = ld_importo_cc
				end if
			else
				if ld_importo_cc >= 0 then
					lstr_cc[ll_cont_cc].importo_avere = ld_importo_cc
					lstr_cc[ll_cont_cc].importo_dare  = 0
				else
					lstr_cc[ll_cont_cc].importo_avere = 0
					lstr_cc[ll_cont_cc].importo_dare  = ld_importo_cc
				end if
			end if
			
			ld_controllo_quadratura_cc += ld_importo_cc
			
			lstr_dati_cc[ll_i].sstr_centro_costo = lstr_cc
		
			//----- aggiunto Enme 09/07/2012 per nuovo oggetto contabile revisionato
			// cerco il codice del centro di costo di impresa
			
			select codice_impresa
			into   :ls_conto_impresa_cc
			from   tab_centri_costo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_centro_costo = :lstr_cc[ll_cont_cc].cod_centro_costo
			using sqlca;
					
			if sqlca.sqlcode <> 0 then
				i_messaggio = "Errore in ricerca conto centri di costo di impresa in tabella centri di costo = ~r~n" + sqlca.sqlerrtext
				i_db_errore = sqlca.sqlcode
				return -1
			end if
			
			// cerco il codice che è chiave primaria in impresa
			select id_cc_conto
			into   :ll_id_cc_conto
			from   cc_conto
			where  codice = :ls_conto_impresa_cc
			using sqlci;
			
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in ricerca conto centri di costo di impresa in tabella cc_conto di impresa = " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
					
			lstr_centri_costo[ll_i].l_id_cc_conto[ll_cont_cc] = ll_id_cc_conto
			lstr_centri_costo[ll_i].l_num_riga[ll_cont_cc] = ll_cont_cc
			lstr_centri_costo[ll_i].dc_dare[ll_cont_cc] = lstr_cc[ll_cont_cc].importo_dare
			lstr_centri_costo[ll_i].dc_avere[ll_cont_cc] = lstr_cc[ll_cont_cc].importo_avere
			lstr_centri_costo[ll_i].dc_percentuale[ll_cont_cc] = lstr_cc[ll_cont_cc].percentuale
			lstr_centri_costo[ll_i].s_bloccato[ll_cont_cc] = "S"
			
			//----- fine aggiunta Enme 09/07/2012
		loop
		
		close cu_conti_cc;
		
		if ld_controllo_quadratura_cc <> ld_imp_riga then
			i_messaggio = "Non vi è la quadratura fra imponibile fattura e centri di costo: VERIFICARE !!!" 
			i_db_errore = 0
			return -1
		end if		
		
	end if
	
loop
close cu_conti;

/*
// EnMe: se ivacee allora il tot fattura è solo l'imponibile
lu_reg_pd.uf_importo(ld_tot_fattura)
lu_reg_pd.uf_imp_valuta(ld_tot_fattura)
*/

if not lb_ivacee then
// -- aggiunto per gestione valuta EnMe 04.09.2014 ----
	if not isnull(li_id_valuta) then
		if ld_cambio_acq <> 0 then
			lu_reg_pd.uf_cambio( round(1 / ld_cambio_acq,4) )
		else
			lu_reg_pd.uf_cambio(0)
		end if
		lu_reg_pd.uf_id_valuta(li_id_valuta)
		lu_reg_pd.uf_imp_valuta(ld_tot_fattura_valuta)
	end if
end if
// --------------------------------------------------------------------


for ll_i = 1 to upperbound(ls_des_contabile)
	if ls_des_contabile[ll_i] = "" then setnull(ls_des_contabile[ll_i])
next

if ll_i > 0 then
	lu_reg_pd.uf_des_contabile(ls_des_contabile)
	lu_reg_pd.uf_id_conto_mc(li_id_conto)
	lu_reg_pd.uf_segno_mc(ls_dare_avere)
	lu_reg_pd.uf_importo_mc(ld_importo_ctp)
	lu_reg_pd.uf_mc_iva(lb_conto_iva)
	lu_reg_pd.uf_id_cc_mov_contabile( lstr_centri_costo )
end if	

// ------------------------  LANCIO LA CREAZIONE DELLE REGISTRAZIONI CONTABILI -------------------------------------

// EnMe 09/07/2012; se la fattura ha l'importo a ZERO è necessario andare a chiamare una specifica funzione
if ld_tot_fattura = 0 then
	lu_reg_pd.uf_zero_mov_contabile(TRUE)
else
	lu_reg_pd.uf_zero_mov_contabile(false)
end if


fl_id_reg_pd = lu_reg_pd.uf_crea()

ls_errore_reg_pd = lu_reg_pd.is_info

if isnull(fl_id_reg_pd) then
	
	if ls_errore_reg_pd<>"" and not isnull(ls_errore_reg_pd) then
		i_messaggio = "Errore in creazione registrazione contabile: " + ls_errore_reg_pd
	else
		//se  la variabile transazione di IMPRESA contiene un errore, accodalo
		if not isnull(lu_reg_pd.it_tran.sqlerrtext) and lu_reg_pd.it_tran.sqlerrtext<>"" then
			i_messaggio = "Errore generico in creazione registrazione contabile! " + lu_reg_pd.it_tran.sqlerrtext
		else
			i_messaggio = "Errore generico in creazione registrazione contabile!"
		end if
	end if
	//i_messaggio = "Errore generico durante la creazione delle registrazioni"
	return -1


elseif fl_id_reg_pd <= 0 then				// valutazione dell'errore
	if ls_errore_reg_pd<>"" and not isnull(ls_errore_reg_pd) then
		i_messaggio = "Errore in creazione registrazione contabile: " + ls_errore_reg_pd
		
		//se  la variabile transazione di IMPRESA contiene un errore, accodalo
		if not isnull(lu_reg_pd.it_tran.sqlerrtext) and lu_reg_pd.it_tran.sqlerrtext<>"" then
			i_messaggio = ": " + lu_reg_pd.it_tran.sqlerrtext
		end if
	else
		i_messaggio = "Errore generico in creazione registrazione contabile (" + string(fl_id_reg_pd) + ")"
	end if
	//i_messaggio = "Errore generico durante la creazione delle registrazioni (" + string(fl_id_reg_pd) + ")"
	return -1
	
end if


// ----------------------------  CREAZIIONE DELLE SCADENZE ---------------------------------------------------------------
// -------------------------- carico struttura scadenze della fattura  ----------------------------

select flag_tipo_pagamento, codice_impresa
into   :ls_flag_tipo_pagamento, :ls_cod_pag_impresa
from   tab_pagamenti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;
if sqlca.sqlcode = -1 then
	i_messaggio = "Errore in ricerca tipo pagamento in APICE; dettaglio = " + sqlci.sqlerrtext
	i_db_errore = sqlca.sqlcode
	return -1
end if
		 
select id_con_pagamento
into   :li_id_con_pagamento
from   con_pagamento
where  codice = :ls_cod_pag_impresa
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca ID condizione pagamento " + ls_cod_pag_impresa + "in IMPRESA" + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

// ---- 26/02/2001   inserisco a mano la condizione di pagamento in impresa perchè loro non lo fanno (KZ)------

if li_id_con_pagamento > 0 and not isnull(li_id_con_pagamento) then
	update reg_pd
	set    id_con_pagamento = :li_id_con_pagamento
	where  id_reg_pd = :fl_id_reg_pd
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in inserimento ID condizione pagamento in prima nota nella registrazione corrente in impresa" + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
end if

// -------------------------------------------------------------------------------------------------------------

select id_conto
into   :li_id_conto_fornitore
from   conto
where  codice = :ls_codice_fornitore
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca ID conto del fornitore  " + ls_codice_fornitore + "in tabella conti in IMPRESA" + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

declare cu_scadenze dynamic cursor for sqlsa ;
ls_sql = " SELECT data_scadenza, imp_rata, imp_rata_valuta, cod_tipo_pagamento" + &
			" from scad_fat_acq " + &
			" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			      "anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
			      "num_registrazione = " + string(fl_num_registrazione) + &
			" order by data_scadenza "

//ll_i = 0

ll_righe_scadenza = guo_functions.uof_crea_datastore(lds_scadenze, ls_sql)

choose case ll_righe_scadenza
	case is < 0
		// errore
		i_messaggio = "Errore in scorrimento scadenze della fattura; dettaglio = " + sqlci.sqlerrtext
		i_db_errore = sqlca.sqlcode
		return -1
	case 0
		// non ci sono scadenze
		i_messaggio = "Errore in scorrimento scadenze della fattura; la fattura non ha alcuna scadenza associata"
		i_db_errore = 0
		return -1
	case else
		// OK trovate scadenze: le inserisco.
		for ll_i = 1 to ll_righe_scadenza
			
			ldt_data_scadenza = lds_scadenze.getitemdatetime(ll_i, 1)
			ld_rata_scadenza = lds_scadenze.getitemnumber(ll_i, 2)
			ld_rata_scadenza_val = lds_scadenze.getitemnumber(ll_i, 3)
			ls_cod_tipo_pagamento = lds_scadenze.getitemstring(ll_i, 4)
		//prepare sqlsa from :ls_sql  using sqlca;
		//open dynamic cu_scadenze ;
		//do while true
		//   fetch cu_scadenze into :ldt_data_scadenza, :ld_rata_scadenza, :ld_rata_scadenza_val, :ls_cod_tipo_pagamento;
		//	if sqlca.sqlcode = -1 then
		//		i_messaggio = "Errore in scorrimento scadenze della fattura; dettaglio = " + sqlci.sqlerrtext
		//		i_db_errore = sqlca.sqlcode
		//		return -1
		//	end if
		//   if sqlca.sqlcode = 100  then
		//		if ll_i = 0 then
		//			ldt_data_scadenza = ldt_data_fattura
		//			ld_rata_scadenza = ld_tot_fattura
		//			ld_rata_scadenza_val = ld_tot_fattura_valuta
		//		else
		//			exit
		//		end if
		//	end if
		//	ll_i = ll_i + 1
		
			// ----------  modificato da EnMe 28/1/2002  per spostare il tipo pagamento sulle righe di scadenza -----
			select id_tipo_pagamento
			into   :li_id_tipo_pagamento
			from   tipo_pagamento
			where  codice = :ls_cod_tipo_pagamento
			using  sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in ricerca ID tipo pagamento " + ls_cod_tipo_pagamento + "in IMPRESA" + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			// -------------------------------------------------------------------------------------------------------------------
			ld_dare_scadenza = 0
			ld_dare_valuta_scadenza = 0
			ld_avere_scadenza = 0
			ld_avere_valuta_scadenza = 0
			setnull(li_id_banca_azienda)
			
			if ls_cod_tipo_pagamento = "CPR" then
				// se è caparra allora metto chiusa forzatament = S
				ls_flag_chiusa_forzatamente = "S"
				if ls_segno_partita = "D" then
					ld_avere_scadenza = ld_rata_scadenza
					ld_avere_valuta_scadenza = ld_rata_scadenza_val
				else
					ld_dare_scadenza = ld_rata_scadenza
					ld_dare_valuta_scadenza = ld_rata_scadenza_val
				end if
			else
				ls_flag_chiusa_forzatamente = "N"
				if ls_segno_partita = "D" then
					ld_dare_scadenza = ld_rata_scadenza
					ld_dare_valuta_scadenza = ld_rata_scadenza_val
				else
					ld_avere_scadenza = ld_rata_scadenza
					ld_avere_valuta_scadenza = ld_rata_scadenza_val
				end if
			end if
		
			if ld_cambio_acq <> 0 then
				ld_cambio = round(1 / ld_cambio_acq, 4)
			else
				ld_cambio = 0
			end if
			ld_saldo = ld_tot_fattura
			ld_saldo_valuta = ld_tot_fattura_valuta
		
			ls_num_documento = string(ll_num_documento)
			
			select scad_raggruppate
			into   :ls_scad_raggruppate
			from   sog_commerciale
			where  codice = :ls_codice_fornitore
			using  sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in flag_scadenze raggruppate nel soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			// aggiunto su richiesta di A.Bellin 26/4/2005
			// Se un fornitore ha RIBA allora la banca della scadenza deve essere la banca di appoggio
			// del fornitore.
		
			choose case ls_flag_tipo_pagamento
					
				case "R"		// RIBA
					
					if not isnull(ls_cod_banca) then
						// nella fattura è stata indicata una banca dell'azienda; verifico se la banca è da creare.
						
						select cod_abi,
								 cod_cab,
								 des_banca,
								 cin,
								 iban
						into   :ls_cod_abi,   
								 :ls_cod_cab,
								 :ls_des_banca,
								 :ls_cin,
								 :ls_iban
						from   anag_banche
						where  cod_azienda = :s_cs_xx.cod_azienda and  
								 cod_banca = :ls_cod_banca;
						if sqlca.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca banca appoggio fornitore; codice banca fornitore=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						if isnull(ls_cod_abi) or isnull(ls_cod_cab) or len(ls_cod_abi) < 1 or len(ls_cod_cab) < 1 then
							// se codice abi e cab non sono impostati allora segnalo errore ed esco dalla contabilizzazione.
							i_messaggio = "Nella banca di appoggio del fornitore mancano ABI e CAB; non sarà quindi possibile assegnare tale banca alla scadenza."
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						select des_abi
						into   :ls_des_abi
						from   tab_abi
						where  cod_abi = :ls_cod_abi;
						if sqlca.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca banca appoggio fornitore; codice banca fornitore=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						select agenzia
						into   :ls_des_cab
						from   tab_abicab
						where  cod_abi = :ls_cod_abi and cod_cab = :ls_cod_cab;
						if sqlca.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca banca appoggio fornitore; codice banca fornitore=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						select id_anagrafica
						into   :li_id_anagrafica
						from   sog_commerciale
						where  codice = :ls_codice_fornitore
						using  sqlci;
						if sqlci.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca codice anagrafico del soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
					
						if isnull(ls_des_banca) or len(ls_des_banca) < 1 then ls_des_banca = ls_des_abi + "," + ls_des_cab
						ls_tipo_banca = "N"
						
						//niente swift per banca nostra
						li_return = uof_abicab(	"M", li_id_anagrafica, li_id_con_pagamento, "F", &
														ls_cod_abi, ls_des_abi, ls_cod_cab, ls_des_cab, ls_cin, ls_des_banca, ls_tipo_banca, &
														ls_iban, "", li_id_banca_appoggio, li_id_banca_sog_commerciale )
						if li_return <> 0 then
							i_messaggio = "Errore in creazione banca appoggio fornitore. " + i_messaggio 
							return -1
						end if
						
						select id_banca_azienda
						into   :li_id_banca_azienda
						from   banca_sog_commerciale
						where  id_banca_sog_commerciale =:li_id_banca_sog_commerciale
						using  sqlci;
						if sqlci.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca codice anagrafico del soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
					else
						setnull(li_id_banca_sog_commerciale)
						setnull(li_id_banca_azienda)
						i_messaggio = "Manca la banca nostra per pagamento RIBA del fornitore."
						
		//				Enrico X Antonella Bellin 20.06.2014: Se nella fattura non è indicata alcuna banca, procedo lo stesso, ma poi metto il warning
		//
		//				select id_banca_sog_commerciale
		//				into   :li_id_banca_sog_commerciale
		//				from   sog_commerciale
		//				where  codice = :ls_codice_fornitore
		//				using  sqlci;
		//				if sqlci.sqlcode <> 0 then
		//					i_messaggio = "Errore in ricerca banca del soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
		//					i_db_errore = sqlci.sqlcode
		//					return -1
		//				end if
						
					end if	
					
				case else  // in caso non sia una RIBA
					
					if not isnull(ls_cod_banca_clien_for) then
						// nella fattura è stata indicata una banca di appoggio; verifico se la banca è da creare.
						
						select cod_abi,
								 cod_cab,
								 des_banca,
								 cin,
								 iban
						into   :ls_cod_abi,   
								 :ls_cod_cab,
								 :ls_des_banca,
								 :ls_cin,
								 :ls_iban
						from   anag_banche_clien_for  
						where  cod_azienda = :s_cs_xx.cod_azienda and  
								 cod_banca_clien_for = :ls_cod_banca_clien_for;
						if sqlca.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca banca appoggio fornitore; codice banca fornitore=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						if isnull(ls_cod_abi) or isnull(ls_cod_cab) or len(ls_cod_abi) < 1 or len(ls_cod_cab) < 1 then
							// se codice abi e cab non sono impostati allora segnalo errore ed esco dalla contabilizzazione.
							i_messaggio = "Nella banca di appoggio del fornitore mancano ABI e CAB; non sarà quindi possibile assegnare tale banca alla scadenza."
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						select des_abi
						into   :ls_des_abi
						from   tab_abi
						where  cod_abi = :ls_cod_abi;
						if sqlca.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca banca appoggio fornitore; codice banca fornitore=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						select agenzia
						into   :ls_des_cab
						from   tab_abicab
						where  cod_abi = :ls_cod_abi and cod_cab = :ls_cod_cab;
						if sqlca.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca banca appoggio fornitore; codice banca fornitore=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						select id_anagrafica
						into   :li_id_anagrafica
						from   sog_commerciale
						where  codice = :ls_codice_fornitore
						using  sqlci;
						if sqlci.sqlcode <> 0 then
							i_messaggio = "Errore in ricerca codice anagrafico del soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
							i_db_errore = sqlci.sqlcode
							return -1
						end if
						
						if isnull(ls_des_banca) or len(ls_des_banca) < 1 then ls_des_banca = ls_des_abi + "," + ls_des_cab
						ls_tipo_banca = "S"
						li_return = uof_abicab(	"M", li_id_anagrafica, li_id_con_pagamento,"F",ls_cod_abi, &
														ls_des_abi, ls_cod_cab, ls_des_cab, ls_cin, ls_des_banca, ls_tipo_banca, &
														ls_iban, ls_swift, li_id_banca_appoggio, li_id_banca_sog_commerciale )
						if li_return <> 0 then
							i_messaggio = "Errore in creazione banca appoggio fornitore. " + i_messaggio 
							return -1
						end if
		
						
					else
		
						setnull(li_id_banca_sog_commerciale)
						setnull(li_id_banca_azienda)
						i_messaggio = "Manca la banca di appoggio del fornitore"
						
		//				Enrico X Antonella Bellin 20.06.2014: Se nella fattura non è indicata alcuna banca, procedo lo stesso, ma poi metto il warning
						
		//				select id_banca_sog_commerciale
		//				into   :li_id_banca_sog_commerciale
		//				from   sog_commerciale
		//				where  codice = :ls_codice_fornitore
		//				using  sqlci;
		//				if sqlci.sqlcode <> 0 then
		//					i_messaggio = "Errore in ricerca banca del soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
		//					i_db_errore = sqlci.sqlcode
		//					return -1
		//				end if
						
					end if	
						
			end choose
		
			
			if ld_dare_scadenza = 0 then setnull(ld_dare_scadenza)
			if ld_avere_scadenza = 0 then setnull(ld_avere_scadenza)
			if ld_dare_valuta_scadenza = 0 then setnull(ld_dare_valuta_scadenza)
			if ld_avere_valuta_scadenza = 0 then setnull(ld_avere_valuta_scadenza)
			
			if not isnull(ldt_data_protocollo) then
				li_anno_partita = year( date(ldt_data_protocollo))
			else
				li_anno_partita = year( date(ldt_data_fattura))
			end if	
			
			insert into scadenza
							(id_azienda,
							 id_reg_pd,
							 id_conto,
							 id_valuta,
							 id_tipo_pagamento,
							 id_banca_sog_commerciale,
							 data_scadenza,
							 dare,
							 avere,
							 dare_valuta,
							 avere_valuta,
							 cambio,
							 saldo,
							 saldo_valuta,
							 tipo_scadenza,
							 num_documento,
							 data_documento,
							 chiusa_forzatamente,
							 imp_varie,
							 bloccata,
							 scadenza_da_raggruppare,
							 tot_documento,
							 id_banca_azienda,
							 num_partita,
							 anno_partita)
				values   (1,
							:fl_id_reg_pd,
							:li_id_conto_fornitore,
							:li_id_valuta,
							:li_id_tipo_pagamento,
							:li_id_banca_sog_commerciale,
							:ldt_data_scadenza,
							:ld_dare_scadenza,
							:ld_avere_scadenza,
							:ld_dare_valuta_scadenza,
							:ld_avere_valuta_scadenza,
							:ld_cambio,
							:ld_saldo,
							:ld_saldo_valuta,
							'P',
							:ls_protocollo,
							:ldt_data_protocollo,
							:ls_flag_chiusa_forzatamente,
							 0,
							'N',
							:ls_scad_raggruppate,
							:ld_tot_fattura,
							:li_id_banca_azienda,
							:ls_protocollo,
							:li_anno_partita)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento scadenze della fattura " +string(ll_num_documento) + " - " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			// ------------ aggiunto il 03/05/2012 su richiesta di Samuele --------------------------	
			update scadenza
			set num_mov_contabile = (		  select numero
													  from mov_contabile
													  where mov_contabile.id_reg_pd = scadenza.id_reg_pd and
															  mov_contabile.id_conto = scadenza.id_conto 	 )
			where num_mov_contabile is null and id_reg_pd = :fl_id_reg_pd
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in allineamento scadenza con movimento contabile " +string(ll_num_documento) + " - " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
		//loop
		//close cu_scadenze;
		next
end choose
//-----------------------------------------------------------------------------------------------------
//inserisco nella tabella note testata del documento una riga con informazioni su chi ha contabilizzato, quando e con quale ID_REG_PD è stato generato
if uof_crea_nota_contabilizzazione(fl_id_reg_pd, fl_anno_registrazione, fl_num_registrazione, "FATACQ") < 0 then
	//in i_messaggio il messaggio di errore
	return -1
end if
//-----------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------
//gestione movimenti INTRA
//solo se il cliente è CEE ed è attivo il parametro multiaziendale MVI (mov_intra in IMPRESA)
is_mov_intra = ""
if uof_mov_intra(fl_id_reg_pd, fl_anno_registrazione, fl_num_registrazione, "FATACQ") < 0 then
	//in i_messaggio il messaggio di errore
	return -1
end if
//-----------------------------------------------------------------------------------------------------


return 0
end function

public function long uof_contabilizza_fat_ven (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_id_reg_pd);u_reg_pd_base lu_reg_pd

lu_reg_pd = CREATE u_reg_pd_base
lu_reg_pd.it_tran = sqlci
lu_reg_pd.uf_set_transaction(sqlci)


// ------------- LEGGO DATI FATTURA DI VENDITA -----------------------------------------------
boolean			lb_commit = false, lb_conto_iva[]

char				ls_dare_avere[]

string				ls_cod_tipo_fat_ven, ls_cod_cliente, ls_cod_valuta, ls_cod_pagamento, ls_cod_valuta_impresa,&
					ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_flag_movimenti, ls_flag_contabilita, &
					ls_cod_banca_clien_for, ls_flag_calcolo, ls_flag_blocco, ls_cod_capoconto, ls_rag_soc_1, &
					ls_flag_tipo_pagamento, ls_flag_bolli, ls_flag_partenza, ls_flag_iva_prima_rata, ls_des_contabile[],&
					ls_flag_cal_commerciale, ls_null, ls_cod_iva, ls_sql, ls_messaggio, ls_segno_contropartita, &
					ls_flag_dare_avere, ls_cod_conto, ls_valuta_default, ls_flag_tipo_fat_ven, ls_segno_partita, &
					ls_num_documento, ls_cin,ls_cod_abi, ls_cod_cab, ls_cod_tipo_pagamento, ls_cod_pag_impresa, &
					ls_scad_raggruppate, ls_des_banca,ls_des_abi,ls_des_cab, ls_tipo_banca,ls_flag_chiusa_forzatamente, &
					ls_conto_impresa, ls_codice_cliente, ls_profilo_impresa, ls_esercizio, ls_cod_centro_costo, &
					ls_conto_impresa_cc, ls_des_prof_registrazione, ls_iban, ls_num_partita, ls_swift,ls_errore_reg_pd, &
					ls_flag_tipo_cliente, ls_profilo_impresa_cee, ls_profilo_impresa_excee
		 
long				li_id_valuta, li_id_tipo_pagamento, li_id_conto_cliente, li_id_con_pagamento, li_id_banca_sog_commerciale, &
					li_return, li_id_banca_appoggio, li_id_anagrafica, li_id_conto[], li_id_sog_commerciale, &
					li_id_prof_registrazione, li_id_esercizio, li_id_conto_iva, li_id_tab_iva[]
		  
long				ll_giorno_fisso_scadenza, ll_mese_escluso1, ll_mese_escluso2, ll_giorno_sost1, &
					ll_giorno_sost2, ll_int_partenza, ll_num_rate_com, ll_perc_prima_rata, ll_anno_documento, &
					ll_num_gior_prima_rata, ll_num_gior_int_rata, ll_perc_ult_rata, ll_num_gior_ult_rata, &
					ll_num_documento, ll_i, ll_ll_numerorecord, ll_numerorecord,ll_cont_cc, ll_id_mov_contabile, &
					ll_id_conto_cc,ll_id_cc_conto
		 
dec{4}			ld_cambio_ven, ld_tot_merci, ld_tot_spese_trasporto, ld_tot_spese_imballo, &
					ld_tot_spese_bolli, ld_tot_spese_varie, ld_tot_sconto_cassa, ld_tot_sconti_commerciali, &
					ld_imponibile_provvigioni_1, ld_imponibile_provvigioni_2, ld_tot_provvigioni_1, &
					ld_tot_provvigioni_2, ld_importo_iva, ld_imponibile_iva, ld_importo_iva_valuta, &
					ld_imponibile_iva_valuta, ld_tot_fattura, ld_tot_fattura_valuta, ld_perc_iva, ld_imp_riga, &
					ld_salva_protocollo, ld_rata_scadenza, ld_rata_scadenza_val, ld_dare_scadenza, ld_avere_scadenza, &
					ld_dare_valuta_scadenza, ld_avere_valuta_scadenza, ld_cambio,  ld_saldo,	ld_saldo_valuta, ld_importo_ctp[], &
					ld_imponibile[], ld_imposta[], ld_iva_importo, ld_percentuale_cc, ld_importo_cc, ld_controllo_quadratura_cc, ld_bollo_fattura

datetime			ldt_data_registrazione, ldt_data_fattura, ldt_salva_data, ldt_oggi, ldt_data_scadenza, ldt_data_decorrenza_pagamento, ldt_data_neutra

date				ld_data

str_dati_centro_costo				lstr_dati_cc[]
str_centro_costo					lstr_cc[], lstr_vuoto[]
s_reg_pd_cc_mov_contabile		lstr_centri_costo[]




ldt_data_neutra = datetime(date("01/01/1900"),time("00:00:00") )


select cod_tipo_fat_ven,   
       cod_cliente,   
       cod_valuta,   
       cambio_ven,   
       cod_pagamento,   
       cod_agente_1,   
       cod_agente_2,   
       cod_banca,   
       flag_movimenti,   
       flag_contabilita,   
       tot_merci,   
       tot_spese_trasporto,   
       tot_spese_imballo,   
       tot_spese_bolli,   
       tot_spese_varie,   
       tot_sconto_cassa,   
       tot_sconti_commerciali,   
       imponibile_provvigioni_1,   
       imponibile_provvigioni_2,   
       tot_provvigioni_1,   
       tot_provvigioni_2,   
       importo_iva,   
       imponibile_iva,   
       importo_iva_valuta,   
       imponibile_iva_valuta,   
       tot_fattura,   
       tot_fattura_valuta,   
       cod_banca_clien_for,   
       flag_calcolo,   
       flag_blocco,
		 anno_documento,
		 data_fattura,
		 num_documento,
		 data_registrazione,
		 data_decorrenza_pagamento
into   :ls_cod_tipo_fat_ven,   
		:ls_cod_cliente,   
		:ls_cod_valuta,   
		:ld_cambio_ven,   
		:ls_cod_pagamento,   
		:ls_cod_agente_1,   
		:ls_cod_agente_2,   
		:ls_cod_banca,   
		:ls_flag_movimenti,   
		:ls_flag_contabilita,   
		:ld_tot_merci,   
		:ld_tot_spese_trasporto,   
		:ld_tot_spese_imballo,   
		:ld_tot_spese_bolli,   
		:ld_tot_spese_varie,   
		:ld_tot_sconto_cassa,   
		:ld_tot_sconti_commerciali,   
		:ld_imponibile_provvigioni_1,   
		:ld_imponibile_provvigioni_2,   
		:ld_tot_provvigioni_1,   
		:ld_tot_provvigioni_2,   
		:ld_importo_iva,   
		:ld_imponibile_iva,   
		:ld_importo_iva_valuta,   
		:ld_imponibile_iva_valuta,   
		:ld_tot_fattura,   
		:ld_tot_fattura_valuta,   
		:ls_cod_banca_clien_for,   
		:ls_flag_calcolo,   
		:ls_flag_blocco,
		:ll_anno_documento,
		:ldt_data_fattura,
		:ll_num_documento,
		:ldt_data_registrazione,
		:ldt_data_decorrenza_pagamento
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore durante ricerca fattura da contabilizzare." + sqlca.sqlerrtext
	i_db_errore    = sqlca.sqlcode
	return -1
end if

select		cod_capoconto,
			rag_soc_1,
			swift,
			flag_tipo_cliente
into		:ls_cod_capoconto, 
			:ls_rag_soc_1,
			:ls_swift,
			:ls_flag_tipo_cliente
from		anag_clienti
where	cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore durante ricerca capoconto in anagrafica clienti." + sqlca.sqlerrtext
	i_db_errore    = sqlca.sqlcode
	return -1
end if

select codice_impresa
into   :ls_cod_valuta_impresa
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta  = :ls_cod_valuta;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore durante ricerca codice valuta impresa in tabella valute di APICE" + sqlca.sqlerrtext
	i_db_errore    = sqlca.sqlcode
	return -1
end if
		 
select id_valuta
into   :li_id_valuta
from   valuta
where  codice = :ls_cod_valuta_impresa
using  sqlci;

if sqlci.sqlcode <> 0 then
	if isnull(ls_cod_valuta_impresa) or ls_cod_valuta_impresa="" then
		i_messaggio = "Non è stato assegnato il codice valuta di impresa alla valuta APICE  " + ls_cod_valuta + "!"
	else
		i_messaggio = "Errore in ricerca ID della valuta " + ls_cod_valuta_impresa + " in IMPRESA. " + sqlci.sqlerrtext
	end if
	
	i_db_errore = sqlci.sqlcode
	return -1
end if
//
// ----------------------------  memo dati testata -----------------------------------------------------------
//
lu_reg_pd.uf_reset()
lu_reg_pd.uf_displaymode(0)
lu_reg_pd.uf_autocommit(false)

select 	profilo_impresa, 
			flag_tipo_fat_ven,
			profilo_impresa_cee,
			profilo_impresa_excee
into   		:ls_profilo_impresa, 
			:ls_flag_tipo_fat_ven,
			:ls_profilo_impresa_cee,
			:ls_profilo_impresa_excee			
from   	tab_tipi_fat_ven
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca profilo registrazione e tipo fattura in tabella tipi fatture" + sqlca.sqlerrtext
	i_db_errore = sqlca.sqlcode
	return -1
end if

// sostituisco il profilo in base al cliente INTRA o EXTRACEE
choose case ls_flag_tipo_cliente
	case "C" 
		ls_profilo_impresa = ls_profilo_impresa_cee
	case "E"
		ls_profilo_impresa = ls_profilo_impresa_excee
end choose

if isnull(ls_profilo_impresa) or len(ls_profilo_impresa) < 1 then
	i_messaggio = "Nel tipo fattura non è stato impostato il profilo registrazione di impresa."
	i_db_errore = 0
	return -1
end if

select id_prof_registrazione, id_conto_iva, descrizione
into   :li_id_prof_registrazione, :li_id_conto_iva, :ls_des_prof_registrazione
from   prof_registrazione
where  codice = :ls_profilo_impresa
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca ID profilo e conto iva nel profilo " + ls_profilo_impresa + " in IMPRESA. " + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

if ls_flag_tipo_fat_ven <> "N" then
	ls_segno_partita = "D"					// è una fattura allora metto la partita in dare e le ctp in avere	
	ls_segno_contropartita = "A"
else
	ls_segno_partita = "A"					// è una nc allora metto la partita in dare e le ctp in avere	
	ls_segno_contropartita = "D"
end if

lu_reg_pd.uf_id_prof_registrazione(li_id_prof_registrazione)

select stringa
into   :ls_esercizio
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ECI';
		 
if sqlca.sqlcode = 100 then
	ls_esercizio = string(ll_anno_documento)
elseif sqlca.sqlcode <> 0 then
	if sqlca.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca profilo registrazione e tipo fattura in tabella tipi fatture" + sqlca.sqlerrtext
		i_db_errore = sqlca.sqlcode
		return -1
	end if
end if	
	
select id_esercizio
into   :li_id_esercizio
from   esercizio
where  codice = :ls_esercizio
using  sqlci;
lu_reg_pd.uf_id_esercizio(li_id_esercizio)

lu_reg_pd.uf_data_documento(date(ldt_data_fattura))
lu_reg_pd.uf_data_registrazione(date(ldt_data_fattura))
lu_reg_pd.uf_num_documento(string(ll_num_documento))
lu_reg_pd.uf_num_progressivo(ll_num_documento)

lu_reg_pd.uf_descrizione("")
if ls_flag_tipo_fat_ven = "N" then lu_reg_pd.uf_tipo_fattura("C")


// --------------------------------- IMPOSTAZIONE DEI MOVIMENTI CONTABILI ------------------------//

// -------------------------------------- carico struttura registro iva ----------------------------//

declare cu_iva dynamic cursor for sqlsa ;
ls_sql = "select cod_iva, imponibile_iva, importo_iva, perc_iva " + &
			"from   iva_fat_ven " + &
			"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			"anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
			"num_registrazione = " + string(fl_num_registrazione)

ll_i = 0
prepare sqlsa from :ls_sql using sqlca;
open dynamic cu_iva;
do while 1=1
   fetch cu_iva into :ls_cod_iva, :ld_imponibile_iva, :ld_iva_importo, :ld_perc_iva;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	
	select id_tab_iva
	into   :li_id_tab_iva[ll_i]
	from   tab_iva
	where  codice = :ls_cod_iva
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca aliquota iva " + ls_cod_iva + ". " + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	
	ld_imponibile[ll_i] = ld_imponibile_iva
	ld_imposta[ll_i] = ld_iva_importo
loop
close cu_iva;


// -------------------------- EnMe 23/07/2019 aggiunta iva per bolli fatture elettroniche ----------- //

select cod_iva
into	:ls_cod_iva
from	tab_tipi_det_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_det_ven = 'SPB';
if sqlca.sqlcode = 0 then
	// ha senso solo se siste il tipo dettaglio fisso con codice SPB
	select sum(imponibile_iva)
	into	:ld_bollo_fattura
	from	det_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_registrazione and
			num_registrazione = :fl_num_registrazione and
			cod_tipo_det_ven = 'SPB';
	if sqlca.sqlcode = 100 then
		ld_bollo_fattura = 0
	elseif sqlca.sqlcode <> 0 then
		if sqlca.sqlcode <> 0 then
			i_messaggio = "Errore in somma totale bolli tipo SPB (bollo fattura esente)" + sqlca.sqlerrtext
			i_db_errore = sqlca.sqlcode
			return -1
		end if
	end if	
	if isnull(ld_bollo_fattura) then ld_bollo_fattura = 0
	
	if ld_bollo_fattura > 0 then
		ll_i = ll_i + 1

		select id_tab_iva
		into   :li_id_tab_iva[ll_i]
		from   tab_iva
		where  codice = :ls_cod_iva
		using  sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in ricerca aliquota iva " + ls_cod_iva + ". " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
		ld_imponibile[ll_i] = ld_bollo_fattura
		ld_imposta[ll_i] = 0
		
	end if
end if

if ll_i > 0 then
	lu_reg_pd.uf_imponibile(ld_imponibile)
	lu_reg_pd.uf_id_tab_iva(li_id_tab_iva)
	lu_reg_pd.uf_imposta(ld_imposta)
end if	

// -------------------------- carico struttura contropartite contabili --------------------

declare cu_conti dynamic cursor for sqlsa;
ls_sql = "SELECT cod_conto, " + &
                "imp_riga, " + &
        			 "flag_dare_avere " + &
			"from   cont_fat_ven " + &
			"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			       "anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
			       "num_registrazione = " + string(fl_num_registrazione)


declare cu_conti_cc cursor for
select cod_centro_costo,   
       percentuale,   
       importo  
from   cont_fat_ven_cc
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_conto = :ls_cod_conto;



ll_i = 0

ls_codice_cliente = uof_codice_cli_for("C", ls_cod_capoconto, ls_cod_cliente)

select id_conto
into   :li_id_sog_commerciale
from   conto
where  codice = :ls_codice_cliente
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca codice soggetto commerciale in contabilità " + ls_codice_cliente + ". " + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

lu_reg_pd.uf_id_conto(li_id_sog_commerciale)

		//  creazione della partita cliente
//if ld_tot_fattura > 0 then
	ll_i = ll_i + 1
	lb_conto_iva[ll_i] = false
	li_id_conto[ll_i] = li_id_sog_commerciale
	ls_dare_avere[ll_i] = ls_segno_partita
	ld_importo_ctp[ll_i] = ld_tot_fattura
//end if	
		// creazione della contropartita iva
//if ld_importo_iva > 0 then
	ll_i = ll_i + 1
	lb_conto_iva[ll_i] = true
	li_id_conto[ll_i] = li_id_conto_iva
	ls_dare_avere[ll_i] = ls_segno_contropartita
	ld_importo_ctp[ll_i] = ld_importo_iva
	
	// descrizione contabile della contropartita.
	ls_des_contabile[ll_i] = left(ls_des_prof_registrazione + " " + ls_rag_soc_1, 80)
//end if

prepare sqlsa from :ls_sql  using sqlca;

open dynamic cu_conti;

if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore in OPEN cursore cu_conti" + sqlca.sqlerrtext
	i_db_errore = sqlca.sqlcode
	return -1
end if

ll_cont_cc = 0

do while true
   fetch cu_conti into :ls_cod_conto, :ld_imp_riga, :ls_flag_dare_avere;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	
	select conto_impresa
	into   :ls_conto_impresa
	from   anag_piano_conti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_conto   = :ls_cod_conto;
	if sqlca.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca conto " + ls_cod_conto + " in piano dei conti" + sqlca.sqlerrtext
		i_db_errore = sqlca.sqlcode
		return -1
	end if
	
	if isnull(ls_conto_impresa) then
		i_messaggio = "Conto contabile di corrispondenza non impostato nella tabella piano dei conti di APICE"
		i_db_errore = sqlca.sqlcode
		return -1
	end if
	
	select id_conto
	into   :li_id_conto[ll_i]
	from   conto
	where  codice = :ls_conto_impresa
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca codice conto contabile " + ls_codice_cliente + ". " + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	
//	ls_dare_avere[ll_i] = ls_flag_dare_avere
	ls_dare_avere[ll_i] = ls_segno_contropartita
	ld_importo_ctp[ll_i] = ld_imp_riga
	lb_conto_iva[ll_i] = false
	
	// descrizione contabile della contropartita.
	ls_des_contabile[ll_i] = ls_des_prof_registrazione + " " + ls_rag_soc_1
	
	// procedo con memorizzazione dati CENTRI DI COSTO
	
	if is_gestione_cc = "S" then
	
		open cu_conti_cc;
		
		if sqlca.sqlcode <> 0 then
			i_messaggio = "Errore in OPEN cursore cu_conti_CC" + sqlca.sqlerrtext
			i_db_errore = sqlca.sqlcode
			return -1
		end if
		
		lstr_dati_cc[ll_i].id_conto = li_id_conto[ll_i]
		lstr_cc = lstr_vuoto
		ld_controllo_quadratura_cc = 0
		ll_cont_cc = 0
		
		do while true
			fetch cu_conti_cc into :ls_cod_centro_costo, :ld_percentuale_cc, :ld_importo_cc;
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode <> 0 then
				i_messaggio = "Errore in FETCH cursore cu_conti_CC" + sqlca.sqlerrtext
				i_db_errore = sqlca.sqlcode
				return -1
			end if
			
			ll_cont_cc ++
			
			lstr_cc[ll_cont_cc].cod_centro_costo = ls_cod_centro_costo
			lstr_cc[ll_cont_cc].percentuale      = ld_percentuale_cc
			
			if ls_segno_contropartita = "D" then
				if ld_importo_cc >= 0 then
					lstr_cc[ll_cont_cc].importo_dare  = ld_importo_cc
					lstr_cc[ll_cont_cc].importo_avere = 0
				else
					lstr_cc[ll_cont_cc].importo_dare  = 0
					lstr_cc[ll_cont_cc].importo_avere = ld_importo_cc * -1
				end if							
			else
				if ld_importo_cc >= 0 then
					lstr_cc[ll_cont_cc].importo_avere = ld_importo_cc
					lstr_cc[ll_cont_cc].importo_dare  = 0
				else
					lstr_cc[ll_cont_cc].importo_avere = 0
					lstr_cc[ll_cont_cc].importo_dare  = ld_importo_cc * -1
				end if
			end if
			
			ld_controllo_quadratura_cc += ld_importo_cc
			
			lstr_dati_cc[ll_i].sstr_centro_costo = lstr_cc
			
			
			//----- aggiunto Enme 09/07/2012 per nuovo oggetto contabile revisionato
			// cerco il codice del centro di costo di impresa
			
			select codice_impresa
			into   :ls_conto_impresa_cc
			from   tab_centri_costo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_centro_costo = :lstr_cc[ll_cont_cc].cod_centro_costo
			using sqlca;
					
			if sqlca.sqlcode <> 0 then
				i_messaggio = "Errore in ricerca conto centri di costo di impresa in tabella centri di costo = ~r~n" + sqlca.sqlerrtext
				i_db_errore = sqlca.sqlcode
				return -1
			end if
			
			// cerco il codice che è chiave primaria in impresa
			select id_cc_conto
			into   :ll_id_cc_conto
			from   cc_conto
			where  codice = :ls_conto_impresa_cc
			using sqlci;
			
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in ricerca conto centri di costo di impresa in tabella cc_conto di impresa = " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
					
			lstr_centri_costo[ll_i].l_id_cc_conto[ll_cont_cc] = ll_id_cc_conto
			lstr_centri_costo[ll_i].l_num_riga[ll_cont_cc] = ll_cont_cc
			lstr_centri_costo[ll_i].dc_dare[ll_cont_cc] = lstr_cc[ll_cont_cc].importo_dare
			lstr_centri_costo[ll_i].dc_avere[ll_cont_cc] = lstr_cc[ll_cont_cc].importo_avere
			lstr_centri_costo[ll_i].dc_percentuale[ll_cont_cc] = lstr_cc[ll_cont_cc].percentuale
			lstr_centri_costo[ll_i].s_bloccato[ll_cont_cc] = "S"
			
			//----- fine aggiunta Enme 09/07/2012
		loop
		
		close cu_conti_cc;
		
		if ld_controllo_quadratura_cc <> ld_imp_riga then
			i_messaggio = "Non vi è la quadratura fra imponibile fattura e centri di costo: VERIFICARE !!!" 
			i_db_errore = 0
			return -1
		end if		
		
	end if	
	
loop

close cu_conti;

lu_reg_pd.uf_importo(ld_tot_fattura)

// -- aggiunto per gestione valuta EnMe 04.09.2014 ----
//-- ERA STATO DISABILITATO, ORA RIATTIVATO.
if not isnull(li_id_valuta) then
	if ld_cambio_ven <> 1 and ld_cambio_ven <> 0 then
		ld_cambio = ROUND(1 / ld_cambio_ven,4)
	end if
	lu_reg_pd.uf_cambio(ld_cambio)
	lu_reg_pd.uf_id_valuta(li_id_valuta)
	lu_reg_pd.uf_imp_valuta(ld_tot_fattura_valuta)
end if
// --------------------------------------------------------------------
for ll_i = 1 to upperbound(ls_des_contabile)
	if ls_des_contabile[ll_i] = "" then setnull(ls_des_contabile[ll_i])
next

if ll_i > 0 then
	lu_reg_pd.uf_des_contabile(ls_des_contabile)
	lu_reg_pd.uf_id_conto_mc(li_id_conto)
	lu_reg_pd.uf_segno_mc(ls_dare_avere)
	lu_reg_pd.uf_importo_mc(ld_importo_ctp)
	lu_reg_pd.uf_mc_iva(lb_conto_iva)
	lu_reg_pd.uf_crea_cc_mov_contabile(true)
	lu_reg_pd.uf_id_cc_mov_contabile( lstr_centri_costo )
	
	// EnMe 07/08/2017 siccome la riconversione degli importi creerebbe problemi, con il seguente script potrei imporre che gli importi delle righe registrazione vengano registrati lo stesso in euro
	//lu_reg_pd.uf_imp_valuta_mc( ld_importo_ctp )

end if	

// ------------------------  LANCIO LA CREAZIONE DELLE REGISTRAZIONI CONTABILI -------------------------------------

// EnMe 09/07/2012; se la fattura ha l'importo a ZERO è necessario andare a chiamare una specifica funzione
if ld_tot_fattura = 0 then
	lu_reg_pd.uf_zero_mov_contabile(TRUE)
else
	lu_reg_pd.uf_zero_mov_contabile(false)
end if

fl_id_reg_pd = lu_reg_pd.uf_crea()
ls_errore_reg_pd = lu_reg_pd.is_info


choose case fl_id_reg_pd				// valutazione dell'errore
	case is <= 0
		//C'E' UN PROBLEMA QUINDI DEVE COMUNQUE TORNARE -1
		if ls_errore_reg_pd<>"" and not isnull(ls_errore_reg_pd) then
			i_messaggio = "Errore in creazione registrazione contabile: " + ls_errore_reg_pd
			
			//se c'è un errore nella transazione di IMPRESA accoda il messaggio
			if not isnull(lu_reg_pd.it_tran.sqlerrtext) and lu_reg_pd.it_tran.sqlerrtext<>"" then
				i_messaggio += " : " + lu_reg_pd.it_tran.sqlerrtext
				i_db_errore = -1
			end if
			
		else
			i_messaggio = "Errore generico in creazione registrazione contabile (" + string(fl_id_reg_pd) + ")"
			
			//se c'è un errore nella transazione di IMPRESA accoda il messaggio
			if not isnull(lu_reg_pd.it_tran.sqlerrtext) and lu_reg_pd.it_tran.sqlerrtext<>"" then
				i_messaggio += " : " + lu_reg_pd.it_tran.sqlerrtext
				i_db_errore = -1
			end if
			
		end if
		
		//return fl_id_reg_pd
		return -1
		
	case else
		
		// EnMe 15/02/2012; metto la data decorrenza nella testata fattura; la metto sempre tanto non fa male.
		// reg_pd.data_dec_pagamento		
		
		if not isnull(ldt_data_decorrenza_pagamento) and ldt_data_decorrenza_pagamento > ldt_data_neutra then
			ld_data = date(ldt_data_decorrenza_pagamento)
		else
			ld_data = date(ldt_data_fattura)
		end if
		
		update reg_pd
		set data_dec_pagamento = :ld_data
		where id_reg_pd = :fl_id_reg_pd
		using sqlci ;
		
		if sqlci.sqlcode < 0 then
			i_messaggio = "Errore SQL durante aggiornamento della data di decorrenza del pagamento " + sqlca.sqlerrtext
			return -1
		end if
		
end choose




// ==========================  CARICO I DATI DEI CENTRI DI COSTO NELLA RELATIVA TABELLA =========================== //
/*
if is_gestione_cc = "S" then
	
	declare cu_mov_contabile_cc cursor for  
	select id_mov_contabile,   
			id_conto  
	from mov_contabile  
	where id_reg_pd = :fl_id_reg_pd
	order by id_mov_contabile using sqlci;
	
	open cu_mov_contabile_cc;
	if sqlci.sqlcode = -1 then
		i_messaggio = "Errore OPEN cursore cu_mov_contabile_cc (movimenti contabili per centri costo)~r~n" + sqlci.sqlerrtext
		i_db_errore = sqlca.sqlcode
		return -1
	end if
	
	do while true
		fetch cu_mov_contabile_cc into :ll_id_mov_contabile, :ll_id_conto_cc;
	
		if sqlci.sqlcode = 100 then exit
		
		if sqlci.sqlcode = -1 then
			i_messaggio = "Errore FETCH cursore cu_mov_contabile_cc (movimenti contabili per centri costo)~r~n" + sqlci.sqlerrtext
			i_db_errore = sqlca.sqlcode
			return -1
		end if
	
		for ll_i = 1 to upperbound(lstr_dati_cc)
			if lstr_dati_cc[ll_i].id_conto = ll_id_conto_cc then
				
				lstr_cc = lstr_dati_cc[ll_i].sstr_centro_costo
				
				for ll_cont_cc = 1 to upperbound(lstr_cc)
				
					// cerco il codice del centro di costo di impresa
					
					select codice_impresa
					into   :ls_conto_impresa_cc
					from   tab_centri_costo
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_centro_costo = :lstr_cc[ll_cont_cc].cod_centro_costo
					using sqlca;
					
					if sqlca.sqlcode <> 0 then
						i_messaggio = "Errore in ricerca conto centri di costo di impresa in tabella centri di costo = ~r~n" + sqlca.sqlerrtext
						i_db_errore = sqlca.sqlcode
						return -1
					end if
					
					// cerco il codice che è chiave primaria in impresa
					select id_cc_conto
					into   :ll_id_cc_conto
					from   cc_conto
					where  codice = :ls_conto_impresa_cc
					using sqlci;
					
					if sqlci.sqlcode <> 0 then
						i_messaggio = "Errore in ricerca conto centri di costo di impresa in tabella cc_conto di impresa = " + sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
					
					
					
					INSERT INTO cc_mov_contabile  
							( id_cc_conto,   
							  id_mov_contabile,   
							  num_riga,   
							  dare,   
							  avere,   
							  note,   
							  id_materiale,   
							  id_ubicazione,   
							  id_risorsa,   
							  id_cantiere,   
							  percentuale,   
							  bloccato )  
					VALUES ( :ll_id_cc_conto,   
							  :ll_id_mov_contabile,   
							  :ll_cont_cc,   
							  :lstr_cc[ll_cont_cc].importo_dare,   
							  :lstr_cc[ll_cont_cc].importo_avere,   
							  null,   
							  null,   
							  null,   
							  null,   
							  null,   
							  :lstr_cc[ll_cont_cc].percentuale,   
							  'S' ) 
					using sqlci;
					
					if sqlci.sqlcode <> 0 then
						i_messaggio = "Errore in inserimento movimenti centri di costo in impresa = " + sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
					
				
				next
			end if
		
		next
	loop
	close cu_mov_contabile_cc; 
	
end if
*/
// ----------------------------  CREAZIIONE DELLE SCADENZE ---------------------------------------------------------------
// -------------------------- carico struttura scadenze della fattura  ----------------------------

select flag_tipo_pagamento, codice_impresa
into   :ls_flag_tipo_pagamento, :ls_cod_pag_impresa
from   tab_pagamenti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;
if sqlca.sqlcode = -1 then
	i_messaggio = "Errore in ricerca tipo pagamento in APICE; dettaglio = " + sqlci.sqlerrtext
	i_db_errore = sqlca.sqlcode
	return -1
end if

if isnull(ls_cod_pag_impresa) or ls_cod_pag_impresa="" then
	i_messaggio = "non è stato specificato il tipo pagamento di IMPRESA corrispondente al codice pagamento "+ls_cod_pagamento+" di APICE!"
	i_db_errore = 0
	return -1
end if
		 
select id_con_pagamento
into   :li_id_con_pagamento
from   con_pagamento
where  codice = :ls_cod_pag_impresa
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca ID condizione pagamento " + ls_cod_pag_impresa + "in IMPRESA" + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

// ---- 26/02/2001   inserisco a mano la condizione di pagamento in impresa perchè loro non lo fanno (KZ)------

if li_id_con_pagamento > 0 and not isnull(li_id_con_pagamento) then
	update reg_pd
	set    id_con_pagamento = :li_id_con_pagamento
	where  id_reg_pd = :fl_id_reg_pd
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in inserimento ID condizione pagamento in prima nota nella registrazione corrente in impresa" + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
end if

// -------------------------------------------------------------------------------------------------------------

select id_conto
into   :li_id_conto_cliente
from   conto
where  codice = :ls_codice_cliente
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca ID conto del cliente  " + ls_codice_cliente + "in tabella conti in IMPRESA" + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

declare cu_scadenze cursor for 
select data_scadenza, imp_rata, imp_rata_valuta, cod_tipo_pagamento
from scad_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione
		order by data_scadenza;

ll_i = 0
open cu_scadenze ;
if sqlca.sqlcode <> 0 then
	i_messaggio = "Errore in OPEN cursore cu_scadenze; dettaglio = " + sqlci.sqlerrtext
	i_db_errore = sqlca.sqlcode
	return -1
end if

do while true
   fetch cu_scadenze into :ldt_data_scadenza, :ld_rata_scadenza, :ld_rata_scadenza_val, :ls_cod_tipo_pagamento;
	if sqlca.sqlcode = -1 then
		i_messaggio = "Errore in scorrimento scadenze della fattura; dettaglio = " + sqlci.sqlerrtext
		i_db_errore = sqlca.sqlcode
		return -1
	end if
   if sqlca.sqlcode = 100  then
		if ll_i = 0 then
			ldt_data_scadenza = ldt_data_fattura
			ld_rata_scadenza = ld_tot_fattura
			ld_rata_scadenza_val = ld_tot_fattura_valuta
		else
			exit
		end if
	end if
	ll_i = ll_i + 1

	// ----------  modificato da EnMe 28/1/2002  per spostare il tipo pagamento sulle righe di scadenza -----
	select id_tipo_pagamento
	into   :li_id_tipo_pagamento
	from   tipo_pagamento
	where  codice = :ls_cod_tipo_pagamento
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in ricerca ID tipo pagamento " + ls_cod_tipo_pagamento + "in IMPRESA" + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	// -------------------------------------------------------------------------------------------------------------------
	ld_dare_scadenza = 0
	ld_dare_valuta_scadenza = 0
	ld_avere_scadenza = 0
	ld_avere_valuta_scadenza = 0
	if ls_cod_tipo_pagamento = "CPR" then
		// se è caparra allora metto chiusa forzatament = S
		ls_flag_chiusa_forzatamente = "S"
		if ls_segno_partita = "D" then
			ld_avere_scadenza = ld_rata_scadenza
			ld_avere_valuta_scadenza = ld_rata_scadenza_val
		else
			ld_dare_scadenza = ld_rata_scadenza
			ld_dare_valuta_scadenza = ld_rata_scadenza_val
		end if
	else
		ls_flag_chiusa_forzatamente = "N"
		if ls_segno_partita = "D" then
			ld_dare_scadenza = ld_rata_scadenza
			ld_dare_valuta_scadenza = ld_rata_scadenza_val
		else
			ld_avere_scadenza = ld_rata_scadenza
			ld_avere_valuta_scadenza = ld_rata_scadenza_val
		end if
	end if

	ld_cambio = ROUND(1 / ld_cambio_ven,4)
	ld_saldo = ld_tot_fattura
	ld_saldo_valuta = ld_tot_fattura_valuta

	ls_num_documento = string(ll_num_documento)
	
	select scad_raggruppate
	into   :ls_scad_raggruppate
	from   sog_commerciale
	where  codice = :ls_codice_cliente
	using  sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in flag_scadenze raggruppate nel soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if

	if not isnull(ls_cod_banca_clien_for) then
		// nella fattura è stata indicata una banca di appoggio; verifico se la banca è da creare.
		
		select cod_abi,
				 cod_cab,
				 des_banca,
				 cin,
				 iban
		into   :ls_cod_abi,   
				 :ls_cod_cab,
				 :ls_des_banca,
				 :ls_cin,
				 :ls_iban
		from   anag_banche_clien_for  
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_banca_clien_for = :ls_cod_banca_clien_for;
		if sqlca.sqlcode <> 0 then
			i_messaggio = "Errore in ricerca banca appoggio cliente; codice banca cliente=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
		if isnull(ls_cod_abi) or isnull(ls_cod_cab) or len(ls_cod_abi) < 1 or len(ls_cod_cab) < 1 then
			// se codice abi e cab non sono impostati allora segnalo errore ed esco dalla contabilizzazione.
			i_messaggio = "Nella banca di appoggio del cliente mancano ABI e CAB; non sarà quindi possibile assegnare tale banca alla scadenza."
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
		select des_abi
		into   :ls_des_abi
		from   tab_abi
		where  cod_abi = :ls_cod_abi;
		if sqlca.sqlcode <> 0 then
			i_messaggio = "Errore in ricerca banca appoggio cliente; codice banca cliente=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
		select agenzia
		into   :ls_des_cab
		from   tab_abicab
		where  cod_abi = :ls_cod_abi and cod_cab = :ls_cod_cab;
		if sqlca.sqlcode <> 0 then
			i_messaggio = "Errore in ricerca banca appoggio cliente; codice banca cliente=" + string(ls_cod_banca_clien_for) + "Dettaglio errore=" + sqlca.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
		select id_anagrafica
		into   :li_id_anagrafica
		from   sog_commerciale
		where  codice = :ls_codice_cliente
		using  sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in ricerca codice anagrafico del soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
		if isnull(ls_des_banca) or len(ls_des_banca) < 1 then ls_des_banca = ls_des_abi + "," + ls_des_cab
		ls_tipo_banca = "S"
		li_return = uof_abicab(	"M", li_id_anagrafica, li_id_tipo_pagamento, "C" ,&
										ls_cod_abi, ls_des_abi, ls_cod_cab, ls_des_cab, ls_cin, ls_des_banca, ls_tipo_banca, &
										ls_iban, ls_swift, li_id_banca_appoggio, li_id_banca_sog_commerciale )
		if li_return <> 0 then
			i_messaggio = "Errore in creazione banca appoggio cliente. " + i_messaggio 
			return -1
		end if
		
	else
		// in fattura non è stata indicata alcuna banca del soggetto commerciale (banca appoggio);
		// allora vado a vedere se c'è nel soggetto commerciale.
		select id_banca_sog_commerciale
		into   :li_id_banca_sog_commerciale
		from   sog_commerciale
		where  codice = :ls_codice_cliente
		using  sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in ricerca banca del soggetto commerciale; ID sogg commerciale=" + string(li_id_sog_commerciale) + "Dettaglio errore=" + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
	end if	
	
	if ld_dare_scadenza = 0 then setnull(ld_dare_scadenza)
	if ld_avere_scadenza = 0 then setnull(ld_avere_scadenza)
	if ld_dare_valuta_scadenza = 0 then setnull(ld_dare_valuta_scadenza)
	if ld_avere_valuta_scadenza = 0 then setnull(ld_avere_valuta_scadenza)
	
	ls_num_partita = string(ll_anno_documento,"0000") + string(ll_num_documento,"000000")
	
	insert into scadenza
	            (id_azienda,
					 id_reg_pd,
					 id_conto,
					 id_valuta,
					 id_tipo_pagamento,
					 id_banca_sog_commerciale,
					 data_scadenza,
					 dare,
					 avere,
					 dare_valuta,
					 avere_valuta,
					 cambio,
					 saldo,
					 saldo_valuta,
					 tipo_scadenza,
					 num_documento,
					 data_documento,
					 chiusa_forzatamente,
					 imp_varie,
					 bloccata,
					 scadenza_da_raggruppare,
					 tot_documento,
					 anno_partita,
					 num_partita)
		values   (1,
					:fl_id_reg_pd,
					:li_id_conto_cliente,
					:li_id_valuta,
					:li_id_tipo_pagamento,
					:li_id_banca_sog_commerciale,
					:ldt_data_scadenza,
					:ld_dare_scadenza,
					:ld_avere_scadenza,
					:ld_dare_valuta_scadenza,
					:ld_avere_valuta_scadenza,
					:ld_cambio,
					:ld_saldo,
					:ld_saldo_valuta,
					'A',
					:ls_num_documento,
					:ldt_data_fattura,
					:ls_flag_chiusa_forzatamente,
					 0,
					'N',
					:ls_scad_raggruppate,
					:ld_tot_fattura,
					:ll_anno_documento,
					:ls_num_partita)
	using sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in inserimento scadenze della fattura " +string(ll_num_documento) + " - " + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	
	// ------------ aggiunto il 03/05/2012 su richiesta di Samuele --------------------------	
	update scadenza
	set num_mov_contabile = (		  select numero
											  from mov_contabile
											  where mov_contabile.id_reg_pd = scadenza.id_reg_pd and
													  mov_contabile.id_conto = scadenza.id_conto 	 )
	where num_mov_contabile is null and id_reg_pd = :fl_id_reg_pd
	using sqlci;
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in allineamento scadenza con movimento contabile " +string(ll_num_documento) + " - " + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	
loop
close cu_scadenze;


//-----------------------------------------------------------------------------------------------------
//inserisco nella tabella note testata del documento una riga con informazioni su chi ha contabilizzato, quando e con quale ID_REG_PD è stato generato
if uof_crea_nota_contabilizzazione(fl_id_reg_pd, fl_anno_registrazione, fl_num_registrazione, "FATVEN") < 0 then
	//in i_messaggio il messaggio di errore
	return -1
end if
//-----------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------
//gestione movimenti INTRA
//solo se il cliente è CEE ed è attivo il parametro multiaziendale MVI (mov_intra in IMPRESA)
is_mov_intra = ""
if uof_mov_intra(fl_id_reg_pd, fl_anno_registrazione, fl_num_registrazione, "FATVEN") < 0 then
	//in i_messaggio il messaggio di errore
	return -1
end if
//-----------------------------------------------------------------------------------------------------

return 0
end function

public function integer uof_con_pagamento (string fs_cod_pagamento, string fs_azione, ref string fs_errore);string			ls_codice_impresa, ls_descrizione, ls_sql, ls_errore
datetime		ld_data_oggi
decimal		ld_sconto_cassa
long			ll_id_con_pagamento
long			ll_ret
boolean		ib_esiste=false
datastore	lds_data

ld_data_oggi = datetime(today(), now())

//il codice impresa coincide con il codice APICE
//poi Impresa comunque metterà l'autoincrementale su id_con_pagamento
//mentre nel campo codice metto lo stesso codice di APICE
//ls_codice_impresa = fs_cod_pagamento

select codice_impresa
into 	:ls_codice_impresa
from 	tab_pagamenti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_pagamento=:fs_cod_pagamento
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura cod.pagam. di collegamento ad IMPRESA: "+sqlca.sqlerrtext
	rollback using sqlci;
	return -1
end if

if isnull(ls_codice_impresa) or ls_codice_impresa="" then
	ls_codice_impresa = fs_cod_pagamento 
end if



//verifico se il codice già esiste in impresa
select		id_con_pagamento
into		:ll_id_con_pagamento
from con_pagamento
where codice=:ls_codice_impresa
using sqlci;

if ll_id_con_pagamento>0 then
	ib_esiste=true
else
	//non esiste
	ib_esiste=false
end if


//---------------------------------------------------------------------
//recupero le info da APICE relative al pagamento
ls_sql = "select codice_impresa,"+&
				"des_pagamento,"+&
				"sconto "+&
			"from tab_pagamenti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_pagamento='"+fs_cod_pagamento+"' "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, sqlca, fs_errore)
if ll_ret=1 then
	//mi aspetto una riga
	ls_descrizione = lds_data.getitemstring(1, "des_pagamento")
	
	ld_sconto_cassa = lds_data.getitemdecimal(1, "sconto")
	if isnull(ld_sconto_cassa) then ld_sconto_cassa=0
	
elseif ll_ret=0 then
	fs_errore = "Non è stato possibile recuperare le info sulla riga del pagamento in APICE!"
	rollback using sqlci;
	return -1
	
else
	//c'è stato un errore (in fs_errore)
	rollback using sqlci;
	return -1
	
end if
//---------------------------------------------------------------------

// stefanop: 19/10/2012: aggiunto campo obbligatorio che non ha initial nel database
if not ib_esiste then
	insert into con_pagamento(
		codice,
		descrizione,
		sconto_cassa,
		data_inserimento,
		tipo_pag_scontrino )
	values (
		:ls_codice_impresa,
		:ls_descrizione,
		:ld_sconto_cassa,
		:ld_data_oggi,
		'X')
	using sqlci;
	
	if sqlci.sqlcode<0 then
		fs_errore = "Errore in inserimento tabella con_pagamento IMPRESA: "+sqlci.sqlerrtext
		rollback using sqlci;
		return -1
	end if
	
//	guo_functions.uof_get_identity(sqlci, ll_id_con_pagamento)
	uof_get_identity(ref ll_id_con_pagamento)

else
	update con_pagamento
	set 	codice=:ls_codice_impresa,
			descrizione=:ls_descrizione,
			sconto_cassa=:ld_sconto_cassa,
			data_modifica=:ld_data_oggi
	where id_con_pagamento=:ll_id_con_pagamento
	using sqlci;
	
	if sqlci.sqlcode<0 then
		fs_errore = "Errore in aggiornamento tabella con_pagamento IMPRESA: "+sqlci.sqlerrtext
		rollback using sqlci;
		return -1
	end if
	
end if

//	per il momento lasciamo questa funzione commentata.
//	IN TEORIA QUESTO PEZZO DI SCRIPT NON SERVE PERCHE' LE RATE DEL PAGAMENTO DOVREBBERO 
//	GENERARSI DA SOLE ALL'ATTO DELLA CREAZIONE / MODIFICA DEL PAGAMENTO.
// 	procedo con la creazione delle rate con la specifica funzione
//ll_ret = uof_app_pagamento(fs_cod_pagamento, "", ref ls_errore)
//
//if ll_ret < 0 then
//	fs_errore = "Errore in creazione RATE pagamento in impresa; " + ls_errore
//	rollback using sqlci;
//	return -1
//end if

// se è tutto andato bene allora aggiorno il collegamento fra APICE e IMPRESA

update tab_pagamenti
set 	  codice_impresa = :fs_cod_pagamento
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_pagamento = :fs_cod_pagamento;
if sqlca.sqlcode<0 then
	fs_errore = "Errore in nel collegamento PAGAMENTO fra APICE e IMPRESA: "+sqlca.sqlerrtext
	rollback using sqlci;
	return -1
end if
	
return ll_id_con_pagamento
end function

public function integer uof_app_pagamento (string fs_cod_pagamento, string fs_azione, ref string fs_errore);string			ls_codice_impresa, ls_tipo_scadenza, ls_cod_tipo_pagamento, ls_sql
decimal		ld_ordine_rata, ld_giorni_decorrenza, ld_numero_rate, ld_giorni_tra_rate, ld_giorno_fisso, ld_perc_iva, ld_perc_imponibile
long			ll_id_con_pagamento, ll_id_tipo_pagamento, ll_ret, ll_index
boolean		ib_esiste=false
datastore	lds_data


select codice_impresa
into :ls_codice_impresa
from tab_pagamenti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_pagamento=:fs_cod_pagamento
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura cod.pagam. di collegamento ad IMPRESA: "+sqlca.sqlerrtext
	rollback using sqlci;
	return -1
end if

if isnull(ls_codice_impresa) or ls_codice_impresa="" then
	//fs_errore = "Non è stato indicato il codice pagamento di collegamento ad IMPRESA!"
	//rollback using sqlci;
	//return -1
	ls_codice_impresa = fs_cod_pagamento
end if

//verifico se il codice già esiste in impresa: in questo caso il codice pagamento impresa deve già esistere ....
select		id_con_pagamento
into		:ll_id_con_pagamento
from 		con_pagamento
where 	codice=:ls_codice_impresa
using 		sqlci;

if ll_id_con_pagamento>0 then
	//esiste, OK
else
	//non esiste
	fs_errore = "Il codice pagamento di collegamento ad IMPRESA '"+ls_codice_impresa+"' non esiste!"
	rollback using sqlci;
	return -1
end if


//---------------------------------------------------------------------
//recupero le info da APICE relative alle rate di pagamento
ls_sql = "select "+&
				"ordine_rata,"+&
				"giorni_decorrenza,"+&
				"numero_rate,"+&
				"giorni_tra_rate,"+&
				"tipo_scadenza,"+&
				"giorno_fisso,"+&
				"perc_iva,"+&
				"perc_imponibile,"+&
				"cod_tipo_pagamento "+&
			"from tab_pagamenti_rate "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_pagamento='"+fs_cod_pagamento+"' "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, sqlca, fs_errore)

if ll_ret<0 then
	//c'è stato un errore (in fs_errore)
	rollback using sqlci;
	return -1
end if

delete from app_pagamento
where id_con_pagamento=:ll_id_con_pagamento
using sqlci;

if sqlci.sqlcode<0 then
	//errore
	fs_errore = "Errore in pre-cancellazione da app_pagamento: "+sqlci.sqlerrtext
	rollback using sqlci;
	return -1
end if

for ll_index=1 to ll_ret
	ld_ordine_rata = lds_data.getitemdecimal(ll_index, "ordine_rata")
	ld_giorni_decorrenza = lds_data.getitemdecimal(ll_index, "giorni_decorrenza")
	ld_numero_rate = lds_data.getitemdecimal(ll_index, "numero_rate")
	ld_giorni_tra_rate = lds_data.getitemdecimal(ll_index, "giorni_tra_rate")
	ls_tipo_scadenza = lds_data.getitemstring(ll_index, "tipo_scadenza")
	ld_giorno_fisso = lds_data.getitemdecimal(ll_index, "giorno_fisso")
	ld_perc_iva = lds_data.getitemdecimal(ll_index, "perc_iva")
	ld_perc_imponibile = lds_data.getitemdecimal(ll_index, "perc_imponibile")
	ls_cod_tipo_pagamento = lds_data.getitemstring(ll_index, "cod_tipo_pagamento")
	
	//leggo il tipo pagamento di impresa
	select id_tipo_pagamento
	into :ll_id_tipo_pagamento
	from tipo_pagamento
	where codice=:ls_cod_tipo_pagamento
	using sqlci;
	
	if ll_id_tipo_pagamento>0 then
	else
		fs_errore = "Tipo pagamento'"+ls_cod_tipo_pagamento+"' non trovato in IMPRESA!"
		rollback using sqlci;
		return -1
	end if
	
	// 29-12-2011 sistemato da EnMe; se gg_decorrenza è zero, in impresa va messo null
	if isnull(ld_giorni_decorrenza) then ld_giorni_decorrenza = 0
	if ld_giorno_fisso = 0 then setnull(ld_giorno_fisso)
	

	insert into app_pagamento
		(	id_con_pagamento,
			num_ordine,
			id_tipo_pagamento,
			giorni_data_decorrenza,
			num_rate,
			giorni_rate,
			giorno_scadenza,
			giorno_fisso,
			per_iva,
			per_imponibile)
	values
		(	:ll_id_con_pagamento,
			:ld_ordine_rata,
			:ll_id_tipo_pagamento,
			:ld_giorni_decorrenza,
			:ld_numero_rate,
			:ld_giorni_tra_rate,
			:ls_tipo_scadenza,
			:ld_giorno_fisso,
			:ld_perc_iva,
			:ld_perc_imponibile)
	using sqlci;
	
	if sqlci.sqlcode<0 then
		fs_errore = "Errore in aggiornamento app_pagamento IMPRESA: "+sqlci.sqlerrtext
		rollback using sqlci;
		return -1
	end if
	
next
//---------------------------------------------------------------------

//se arrivi fin qui commit su IMPRESA
commit using sqlci;


return 0
end function

public function integer uof_saldo_conto (string fs_conto, datetime fdt_data_riferimento, ref decimal fd_saldo, ref string fs_error);long li_id_conto

fs_error = ""
fd_saldo = 0

select id_conto
into :li_id_conto
from conto
where codice = :fs_conto
using sqlci;

if sqlci.sqlcode = 100 then
	fs_error = "Il conto richiesto "+fs_conto+" è inesistente!"
	return -1
end if

select isnull(sum( isnull(dba.mov_contabile.dare,0) - isnull(dba.mov_contabile.avere,0) ),0)
into :fd_saldo
from dba.mov_contabile
                join dba.reg_pd
                    on dba.mov_contabile.id_reg_pd = dba.reg_pd.id_reg_pd
                join dba.registro
                    on dba.reg_pd.id_registro = dba.registro.id_registro
                join dba.mod_registro
                    on dba.registro.id_mod_registro = dba.mod_registro.id_mod_registro
                join dba.azienda
                    on dba.registro.id_azienda = dba.azienda.id_azienda
                join dba.esercizio
                    on dba.azienda.id_esercizio = dba.esercizio.id_esercizio
where 
                dba.mov_contabile.id_conto = :li_id_conto and
                dba.registro.tipo_registro <> 'PZ' and
			  dba.registro.tipo_registro <> 'MN' and 
			  dba.mod_registro.fiscale = 'N' and
                dba.reg_pd.data_registrazione >= dba.esercizio.data_inizio and
                dba.reg_pd.data_registrazione <= :fdt_data_riferimento
using sqlci;

if sqlci.sqlcode < 0 then
	fs_error = "Errore in calcolo saldo del conto " + fs_conto + ": " + sqlci.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_saldo_conto_centro_costo (string fs_conto, string fs_piano_centro_costo, string fs_centro_costo, datetime fdt_data_riferimento, ref decimal fd_saldo, ref string fs_error);string ls_cod_centro_costo
long li_id_conto, li_id_cc_conto, li_id_cc_piano

fs_error = ""
fd_saldo = 0

select id_conto
into :li_id_conto
from conto
where codice = :fs_conto
using sqlci;

if sqlci.sqlcode = 100 then
	fs_error = "Il conto richiesto " + fs_conto + " è inesistente!"
	return -1
end if

// controllo piano centro cost
select id_cc_piano
into :li_id_cc_piano
from cc_piano
where codice = :fs_piano_centro_costo
using sqlci;

if sqlci.sqlcode <> 0 then
	fs_error = "Il  piano centro di costo richiesto " + string(fs_piano_centro_costo) + " è inesistente!"
	return -1
end if

select id_cc_conto
into :li_id_cc_conto
from cc_conto
where codice = :fs_centro_costo and
		 id_cc_piano = :li_id_cc_piano
using sqlci;

if sqlci.sqlcode <> 0 then
	fs_error = "Il centro di costo richiesto " + string(fs_centro_costo) + " è inesistente!"
	return -1
end if

select isnull(sum( isnull(dba.cc_mov_contabile.dare,0) - isnull(dba.cc_mov_contabile.avere,0) ),0)
into :fd_saldo
from dba.cc_mov_contabile
join dba.cc_conto
	on dba.cc_mov_contabile.id_cc_conto = dba.cc_conto.id_cc_conto
join dba.mov_contabile
	on dba.mov_contabile.id_mov_contabile = dba.cc_mov_contabile.id_mov_contabile           
join dba.reg_pd
	on dba.mov_contabile.id_reg_pd = dba.reg_pd.id_reg_pd
join dba.registro
	on dba.reg_pd.id_registro = dba.registro.id_registro
join dba.mod_registro
	on dba.registro.id_mod_registro = dba.mod_registro.id_mod_registro
join dba.azienda
	on dba.registro.id_azienda = dba.azienda.id_azienda
join dba.esercizio
	on dba.azienda.id_esercizio = dba.esercizio.id_esercizio
where
	dba.mov_contabile.id_conto = :li_id_conto and
	dba.cc_conto.id_cc_conto = :li_id_cc_conto and
	dba.registro.tipo_registro <> 'PZ' and
	dba.registro.tipo_registro <> 'MN' and
	dba.mod_registro.fiscale = 'N' and
	dba.reg_pd.data_registrazione >= dba.esercizio.data_inizio and
	dba.reg_pd.data_registrazione <= :fdt_data_riferimento
using sqlci;

if sqlci.sqlcode < 0 then
	fs_error = "Errore in calcolo saldo del conto " + fs_conto + "  nel centro di costo " + string(fs_centro_costo) + "~r~n" + sqlci.sqlerrtext
	return -1
end if

return 0
end function

public function long uof_tab_iva (string flag_tipo_operazione, string fs_cod_iva, string fs_des_iva, string fs_des_estesa, double fd_perc_iva, double fd_perc_detraibile);

//se la des_estesa è vuota metto nel campo la des_iva
if isnull(fs_des_estesa) or fs_des_estesa="" then fs_des_estesa = fs_des_iva
//------------------------------------------------------------------------------------

choose case flag_tipo_operazione
	case "I"
		if not isnull(fs_cod_iva) and len(fs_cod_iva) > 0 then
		  INSERT INTO tab_iva  
					( des_breve,   
					  des_estesa,   
					  tipo_iva,   
					  per_aliquota,   
					  per_detrazione,   
					  codice )  
		  VALUES ( :fs_des_iva,   
					  :fs_des_estesa,   
					  'N',   
					  :fd_perc_iva,   
					  100,   
					  :fs_cod_iva )  
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento nuova aliquota iva in contabilità. Dettaglio errore " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if
	case "M"
		update tab_iva
		set    des_breve = :fs_des_iva,
		       des_estesa = :fs_des_estesa,
				 per_aliquota = :fd_perc_iva
		where  codice = :fs_cod_iva
		using  sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in aggiornamento della aliquota iva in contabilità. Dettaglio errore " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
	case "D"
		delete from tab_iva
		where  codice = :fs_cod_iva
		using  sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in cancellazione della aliquota iva in contabilità. Dettaglio errore " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
end choose

return 0
end function

public function integer uof_saldo_conto_periodo (string fs_conto, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_saldo, ref string fs_errore);string ls_errore
long ll_ret
dec{4} ld_saldo_inizio, ld_saldo_fine
date ld_data
datetime ldt_data_inizio

ld_data = date(fdt_data_inizio)
ld_data = relativedate(ld_data, -1)
ldt_data_inizio = datetime(ld_data, 00:00:00)

ll_ret = uof_saldo_conto( fs_conto, fdt_data_fine, ld_saldo_fine, ls_errore)
if ll_ret < 0 then
	fs_errore = ls_errore
	return -1
end if

if isnull(ld_saldo_fine) then ld_saldo_fine = 0

ll_ret = uof_saldo_conto( fs_conto, ldt_data_inizio, ld_saldo_inizio, ls_errore)
if ll_ret < 0 then
	fs_errore = ls_errore
	return -1
end if

if isnull(ld_saldo_inizio) then ld_saldo_inizio = 0

fd_saldo = ld_saldo_fine - ld_saldo_inizio


return 0
end function

public function integer uof_saldo_conto_cc_periodo (string fs_conto, string fs_piano_centro_costo, string fs_centro_costo, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_saldo, ref string fs_errore);string ls_errore
long ll_ret
dec{4} ld_saldo_inizio, ld_saldo_fine
date ld_data
datetime ldt_data_inizio

ld_data = date(fdt_data_inizio)
ld_data = relativedate(ld_data, -1)
ldt_data_inizio = datetime(ld_data, 00:00:00)

ll_ret = uof_saldo_conto_centro_costo(fs_conto, fs_piano_centro_costo, fs_centro_costo, fdt_data_fine, ref ld_saldo_fine, ref ls_errore)
if ll_ret < 0 then
	fs_errore = ls_errore
	return -1
end if

if isnull(ld_saldo_fine) then ld_saldo_fine = 0

ll_ret = uof_saldo_conto_centro_costo(fs_conto, fs_piano_centro_costo, fs_centro_costo, ldt_data_inizio, ref ld_saldo_inizio, ref ls_errore)
if ll_ret < 0 then
	fs_errore = ls_errore
	return -1
end if

if isnull(ld_saldo_inizio) then ld_saldo_inizio = 0

fd_saldo = ld_saldo_fine - ld_saldo_inizio


return 0
end function

public function integer uof_flag_predefiniti (string fs_tipo_soggetto, string fs_tipo_pagamento, string fs_modo_uso_banca, ref string fs_flag_banca_predefinita, ref string fs_flag_docfin_predefinita);fs_flag_banca_predefinita = "N"
fs_flag_docfin_predefinita = "N"

choose case fs_tipo_soggetto

	case "C" 		// cliente
		if fs_modo_uso_banca = "S" then
			// banca del soggetto
			fs_flag_docfin_predefinita = "S"
			
			if fs_tipo_pagamento = "RIBA" or fs_tipo_pagamento = "RB" then
				fs_flag_banca_predefinita = "S"
			else
				fs_flag_banca_predefinita = "N"
			end if
		else
			// banca nostra
			fs_flag_docfin_predefinita = "N"
			if fs_tipo_pagamento = "RIBA" or fs_tipo_pagamento = "RB" then
				fs_flag_banca_predefinita = "N"
			else
				fs_flag_banca_predefinita = "S"
			end if
		end if

	case "F" 		// fornitore
		if fs_modo_uso_banca = "S" then
			// banca del soggetto
			fs_flag_docfin_predefinita = "S"
			
			if fs_tipo_pagamento = "RIBA" or fs_tipo_pagamento = "RB"  then
				fs_flag_banca_predefinita = "N"
			else
				fs_flag_banca_predefinita = "S"
			end if
		else
			// banca nostra
			fs_flag_docfin_predefinita = "N"
			if fs_tipo_pagamento = "RIBA" or fs_tipo_pagamento = "RB"then
				fs_flag_banca_predefinita = "S"
			else
				fs_flag_banca_predefinita = "N"
			end if
		end if
end choose

return 0
end function

public function integer uof_reg_ivacee (long al_id_prof_registrazione, ref long al_id_conta_iva_acq_cee);// EnMe 25/06/2014
// Questa funzione verifica se un certo profilo di registrazione prevede la registrazione IVACEE
integer	li_retval
string	ls_valore
long 		ll_id_wizard

li_retval = 0

IF al_id_prof_registrazione > 0 THEN
	SetNull(ll_id_wizard)

	SELECT 	wizard.id_wizard, 
				par_wizard.valore
	INTO 		:ll_id_wizard,
				:ls_valore
	FROM 	prof_registrazione,
				wiz_profilo,
				wizard,
				par_wizard
	WHERE 	prof_registrazione.id_prof_registrazione = wiz_profilo.id_prof_registrazione AND
				wizard.id_wizard = wiz_profilo.id_wizard AND
				wizard.id_wizard = par_wizard.id_wizard AND
				prof_registrazione.id_prof_registrazione = :al_id_prof_registrazione AND
				wizard.tipo = 'IVACEE' AND
				wiz_profilo.utilizzabile = 'S'
	USING 	sqlci;
	
	if sqlci.sqlcode < 0 then
		i_messaggio = "Errore in ricerca wizard IVACEE dal profilo id=" + string(al_id_prof_registrazione) + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
	
	setnull(al_id_conta_iva_acq_cee)
		
	if ll_id_wizard > 0 then 
		li_retval = 1
		
		select 	id_conto_iva
		into   	:al_id_conta_iva_acq_cee
		from   	prof_registrazione
		where  	codice = :ls_valore
		using  	sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in ricerca conto IVA vendite per acquisti CEE nel profilo " + ls_valore + " in IMPRESA. " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
	end if
	
END IF

RETURN li_retval

end function

public function string ouf_saldo_conto_impresa (ref decimal ad_saldo, string as_cod_conto_impresa, ref string as_nome_conto);
/*
	Funzione che dato un codice di impresa e una data di termine restituisce
il saldo di quel conto fino al mese indicato nella data di riferimento
	se ritorna errore => errore
	(-1 potrebbe essere un saldo di qualche conto)
	attenzione vale solo per i conti di tipo generico
*/
/* claudia 16/01/06  fa un calcolo errato perchè non riprende i saldi dell'anno prima
	

*/
	long		ll_anno, ll_mese
	
	string 	ls_cod_impresa, ls_cod_capoconto, ls_anno_mese, ls_messaggio
	
	decimal {2} ld_saldo_contabile
	datetime ldt_data_inizio_anno
//Verifica parametri ricevuti
if isnull(as_cod_conto_impresa) or isnull(as_cod_conto_impresa) then
	ls_messaggio = "Specificare il conto e data di riferimento"
	return ls_messaggio
end if

//Lettura saldo contabile da IMPRESA (AD_SALDO_CONTABILE)
if s_cs_xx.parametri.impresa and isvalid(sqlci) then
	
	SELECT dba.esercizio.data_inizio
	INTO   :ldt_data_inizio_anno
	FROM   dba.esercizio
			 JOIN dba.azienda ON dba.azienda.id_esercizio = dba.esercizio.id_esercizio	
	USING  sqlci;
		
	if sqlci.sqlcode <> 0 then
		ls_messaggio = "Errore in lettura dell'anno di apertura conto in IMPRESA: " + sqlci.sqlerrtext
		return ls_messaggio
	end if
	
	ll_anno = year(date(ldt_data_inizio_anno))
	
	SELECT
		Sum(dba.saldo_mensile.dare) - Sum(dba.saldo_mensile.avere) AS saldo, dba.conto.descrizione
	INTO
		:ld_saldo_contabile,
		:as_nome_conto
	FROM
		dba.saldo_mensile
		JOIN dba.conto ON dba.saldo_mensile.id_conto = dba.conto.id_conto

	WHERE	
		dba.saldo_mensile.anno >= :ll_anno AND
		dba.conto.codice = :as_cod_conto_impresa
	GROUP BY
		dba.conto.codice,
		dba.conto.descrizione
	USING
		sqlci;

	if sqlci.sqlcode < 0 then
		ls_messaggio = "Errore in lettura saldo contabile del conto da IMPRESA: " + sqlci.sqlerrtext
		return ls_messaggio
	elseif sqlci.sqlcode = 100 then
		ld_saldo_contabile = 0
	end if
	
	if isnull(ld_saldo_contabile) then
		ld_saldo_contabile = 0
	end if
	
else
	ld_saldo_contabile = 0
end if

ad_saldo = ld_saldo_contabile

return "ok"
end function

public function decimal uof_saldo_partite_aperte (string as_tipo_anagrafica, string as_codice, string as_flag_italia_estero_ue, date ad_data_rif);string	ls_cod_capoconto, ls_codice_impresa, ls_flag_prov_contabile, ls_errore, ls_sql
long 		ll_ret
dec{4} 	ld_importo
datetime ldt_data_riferimento
datastore lds_data

ldt_data_riferimento = datetime(ad_data_rif,00:00:00)

as_tipo_anagrafica = "'" + as_tipo_anagrafica + "'"

if as_codice = "%" or as_codice = "*" then
	ls_codice_impresa = "'%'" 
else
	
	choose case as_tipo_anagrafica
		case "C"
			select cod_capoconto
			into	:ls_cod_capoconto
			from	anag_clienti
			where cod_azienda = :s_cs_xx.cod_azienda and
						cod_cliente = :as_codice;
						
			
		case "F"
			select cod_conto
			into	:ls_cod_capoconto
			from	anag_fornitori
			where cod_azienda = :s_cs_xx.cod_azienda and
						cod_fornitore = :as_codice;
						
	end choose
	
	ls_codice_impresa = uof_codice_cli_for(as_tipo_anagrafica, ls_cod_capoconto, as_codice)
end if

choose case as_flag_italia_estero_ue
	case "I"		// italia
		ls_flag_prov_contabile = "'T'"		
	case "C" 	// intracomunicario + san marino
		ls_flag_prov_contabile = "'I','S'"
	case "E"		// estero
		ls_flag_prov_contabile = "'E'"
	case else
		ls_flag_prov_contabile = "'T','I','S','E'"		// tutti
end choose


ls_sql = "select sum(importo) from dba.vs_apice_scadenze_aperte  where conto_codice like " + ls_codice_impresa + " 	and dba.vs_apice_scadenze_aperte.provenienza_contabile in ("+ ls_flag_prov_contabile +") and dba.vs_apice_scadenze_aperte.tipo_soggetto like " + as_tipo_anagrafica  + " and dba.vs_apice_scadenze_aperte.data_scadenza <= '" + string(ldt_data_riferimento,s_cs_xx.db_funzioni.formato_data) + "'"

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, sqlci, ls_errore)

if ll_ret > 0 then
	ld_importo = lds_data.getitemnumber(1,1)
	destroy lds_data
	return ld_importo
else
	ls_errore = "Errore SQL database Impresa funzione (uof_saldo_partite_aperte). " + ls_errore
	return -1
end if

if isnull(ld_importo) then ld_importo = 0
			
return ld_importo
			
end function

public function integer uof_top_credit (long al_num_elementi, string as_flag_tipo_scadenza, string as_flag_italia_estero_ue, datetime adt_data_scadenza, ref datastore ads_data, ref string as_errore);string ls_flag_prov_contabile, ls_sql
long 	ll_ret


choose case as_flag_italia_estero_ue
	case "I"		// italia
		ls_flag_prov_contabile = "'T'"		
	case "C" 	// intracomunicario + san marino
		ls_flag_prov_contabile = "'I','S'"
	case "E"		// estero
		ls_flag_prov_contabile = "'E'"
	case else
		ls_flag_prov_contabile = "'T','I','S','E'"		// tutti
end choose

if g_str.isempty(as_flag_tipo_scadenza) then
	as_flag_tipo_scadenza = "%"
end if

ls_sql = ""
if not isnull(al_num_elementi) and al_num_elementi > 0 then
	ls_sql += "select top " + string(al_num_elementi)
end if
ls_sql += 	" conto_codice, conto_descrizione, tipo_scadenza, SUM(importo) from dba.vs_apice_scadenze_aperte where	scadenza_attiva_passiva like '" + as_flag_tipo_scadenza + "' and " + &
				" provenienza_contabile in (" + ls_flag_prov_contabile + ") and data_scadenza <= '" + string(adt_data_scadenza,s_cs_xx.db_funzioni.formato_data) + "' group by conto_codice, conto_descrizione, tipo_scadenza order by SUM(importo) desc "

ll_ret = guo_functions.uof_crea_datastore( ads_data, ls_sql, sqlci, as_errore)

return ll_ret

end function

public function long uof_abicab (string fs_tipo_operazione, long fi_id_anagrafica, long fi_id_con_pagamento, string fs_tipo_anagrafica, string fs_cod_abi, string fs_des_abi, string fs_cod_cab, string fs_des_cab, string fs_cin, string fs_des_banca, string fs_tipo_banca, string fs_iban, string fs_swift, ref long fi_id_banca_appoggio, ref long fi_id_banca_sog_commerciale);string ls_flag_predefinita_docfinance, ls_tipo_pagamento, ls_flag_banca_predefinita
long li_id_banca_azienda, ll_id_tipo_pagamento, ll_num_ordine

if not isnull(fi_id_con_pagamento) then
	
	ll_num_ordine = 0
	
	select min(num_ordine)
	into	:ll_num_ordine
	from	app_pagamento
	where id_con_pagamento = :fi_id_con_pagamento
	using sqlci;
	
	if ll_num_ordine > 0 then
		select id_tipo_pagamento
		into	:ll_id_tipo_pagamento
		from	app_pagamento
		where id_con_pagamento = :fi_id_con_pagamento and num_ordine = :ll_num_ordine
		using sqlci;
		
		if not isnull(ll_id_tipo_pagamento) then
			select codice
			into	:ls_tipo_pagamento
			from	tipo_pagamento
			where id_tipo_pagamento = :ll_id_tipo_pagamento
			using sqlci;
			
			if sqlca.sqlcode <> 0 then ls_tipo_pagamento = ''
		end if
	else
		ls_tipo_pagamento = ''
	end if
	
else
	ls_tipo_pagamento = ''
	
end if

choose case fs_tipo_operazione
	case "I" 
		
		select id_banca_appoggio
		into :fi_id_banca_appoggio
		from  banca_appoggio
		where cod_abi = :fs_cod_abi and cod_cab = :fs_cod_cab
		using sqlci;
		
		if sqlci.sqlcode = 100 then
			insert into banca_appoggio (cod_abi, den_abi, cod_cab, den_cab, des_estesa)
			values (:fs_cod_abi, :fs_des_abi, :fs_cod_cab, :fs_des_cab, :fs_des_banca)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			uof_get_identity(fi_id_banca_appoggio)
//			guo_functions.uof_get_identity(sqlci, fi_id_banca_appoggio)
//			SELECT @@identity INTO :fi_id_banca_appoggio from sys.dummy using sqlci;
		end if
		
		uof_flag_predefiniti(fs_tipo_anagrafica, ls_tipo_pagamento, fs_tipo_banca, ref ls_flag_banca_predefinita, ref ls_flag_predefinita_docfinance)
		
		if ls_flag_banca_predefinita = "S" then
			update 	banca_sog_commerciale
			set 		predefinita = 'N'
			where 	id_anagrafica = :fi_id_anagrafica
			using 	sqlci;
			if sqlci.sqlcode = -1 then
				i_messaggio = sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if		
			
		if ls_flag_predefinita_docfinance = "S" then
			update 	banca_sog_commerciale
			set 		pred_docfinance= 'N'
			where 	id_anagrafica = :fi_id_anagrafica
			using 	sqlci;
			if sqlci.sqlcode = -1 then
				i_messaggio = sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if

		if fs_tipo_banca = "N" then
			// EnMe con Samuele: se banca nostra niente CIN e IBAN; serve per DocF 11/7/2014
			setnull(fs_cin)
			setnull(fs_iban)
			
			setnull(fs_swift)
		else
			fs_swift = left(fs_swift, 40)
		end if
		
		insert into banca_sog_commerciale (id_anagrafica, id_banca_appoggio,  modo_uso, cin, iban, predefinita, pred_docfinance, codice_swift)  
		values    (:fi_id_anagrafica, :fi_id_banca_appoggio, :fs_tipo_banca, :fs_cin, :fs_iban, :ls_flag_banca_predefinita, :ls_flag_predefinita_docfinance, :fs_swift)
		using sqlci;
		
		if sqlci.sqlcode <> 0 then
			i_messaggio = sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		uof_get_identity(ref fi_id_banca_sog_commerciale)
//		guo_functions.uof_get_identity(sqlci, fi_id_banca_sog_commerciale)
//		SELECT @@identity INTO :fi_id_banca_sog_commerciale from sys.dummy using sqlci;
		
		
		// aggiunto per gestione banca azienda (segnalazione A.Bellin Ptenda 24/2/03) (ENME)
		setnull(li_id_banca_azienda)
		
		//caso "banca nostra" per fornitore
		if fs_tipo_anagrafica = "F" and fs_tipo_banca = "N" then
			select id_banca_azienda
			into   :li_id_banca_azienda  
			from   banca_azienda  
			where  id_banca_appoggio = :fi_id_banca_appoggio
			using sqlci;
			
			if sqlci.sqlcode = 100 then
				//i_messaggio = sqlci.sqlerrtext
				//i_db_errore = sqlci.sqlcode
				
				i_messaggio = "La Banca Nostra selezionata per il Fornitore non è presente in tabella banche Azienda di IMPRESA."
				i_db_errore = -1
				
				return -1
			end if
			
			update banca_sog_commerciale
			set id_banca_azienda = :li_id_banca_azienda
			where id_banca_sog_commerciale = :fi_id_banca_sog_commerciale
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if
		// ^^^  fine modifica
		
	case "M"
		
		select id_banca_appoggio
		into   :fi_id_banca_appoggio
		from   banca_appoggio
		where  cod_abi = :fs_cod_abi and cod_cab = :fs_cod_cab
		using sqlci;
		
		if sqlci.sqlcode = 100 then
			
			insert into banca_appoggio (cod_abi, den_abi, cod_cab, den_cab, des_estesa)
			values (:fs_cod_abi, :fs_des_abi, :fs_cod_cab, :fs_des_cab, :fs_des_banca)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			// leggo l'ID generato e lo memorizzo in una var locale
			uof_get_identity(fi_id_banca_appoggio)
			//guo_functions.uof_get_identity(sqlci, fi_id_banca_appoggio)
			//SELECT @@identity INTO :fi_id_banca_appoggio from sys.dummy using sqlci;
			
			// inserisco anche  la banca del soggetto commerciale
		
			uof_flag_predefiniti(fs_tipo_anagrafica, ls_tipo_pagamento, fs_tipo_banca, ref ls_flag_banca_predefinita, ref ls_flag_predefinita_docfinance)
			
			if ls_flag_banca_predefinita = "S" then
				update 	banca_sog_commerciale
				set 		predefinita ='N'
				where 	id_anagrafica = :fi_id_anagrafica
				using 	sqlci;
				if sqlci.sqlcode = -1 then
					i_messaggio = sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if
			end if		
				
			if ls_flag_predefinita_docfinance = "S" then
				update 	banca_sog_commerciale
				set 		pred_docfinance= 'N'
				where 	id_anagrafica = :fi_id_anagrafica
				using 	sqlci;
				if sqlci.sqlcode = -1 then
					i_messaggio = sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if
			end if		

			if fs_tipo_banca = "N" then
				// EnMe con Samuele: se banca nostra niente CIN e IBAN; serve per DocF 11/7/2014
				setnull(fs_cin)
				setnull(fs_iban)
				
				setnull(fs_swift)
			else
				fs_swift = left(fs_swift, 40)
			end if
			
			insert into banca_sog_commerciale (id_anagrafica, id_banca_appoggio,  modo_uso, cin, iban, predefinita,pred_docfinance, codice_swift)  
			values    (:fi_id_anagrafica, :fi_id_banca_appoggio, :fs_tipo_banca, :fs_cin, :fs_iban, :ls_flag_banca_predefinita, :ls_flag_predefinita_docfinance, :fs_swift) 
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			// leggo l'ID generato e lo memorizzo in una var locale
			uof_get_identity(ref fi_id_banca_sog_commerciale)
//			guo_functions.uof_get_identity(sqlci, fi_id_banca_sog_commerciale)
			//SELECT @@identity INTO :fi_id_banca_sog_commerciale from sys.dummy using sqlci;

			// aggiunto per gestione banca azienda (segnalazione A.Bellin Ptenda 24/2/03) (ENME)
			setnull(li_id_banca_azienda)
			
			//caso "banca nostra" per fornitore
			if fs_tipo_anagrafica = "F" and fs_tipo_banca = "N" then
				select id_banca_azienda
				into   :li_id_banca_azienda  
				from   banca_azienda  
				where  id_banca_appoggio = :fi_id_banca_appoggio
				using sqlci;
				
				if sqlci.sqlcode = 100 then
					//i_messaggio = sqlci.sqlerrtext
					//i_db_errore = sqlci.sqlcode
					
					i_messaggio = "La Banca Nostra selezionata per il Fornitore non è presente in tabella banche Azienda di IMPRESA."
					i_db_errore = -1
					return -1
				end if
				
				update banca_sog_commerciale
				set id_banca_azienda = :li_id_banca_azienda
				where id_banca_sog_commerciale = :fi_id_banca_sog_commerciale
				using sqlci;
				if sqlci.sqlcode = 100 then
					i_messaggio = sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if
			end if
			// ^^^  fine modifica
			
		elseif sqlci.sqlcode <> 0 then
			
			i_messaggio = "Errore in ricerca abi-cab banca appoggio " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
			
			
		else	// modifico la banca esistente.
			select id_banca_appoggio
			into   :fi_id_banca_appoggio
			from   banca_appoggio
			where  cod_abi = :fs_cod_abi and cod_cab = :fs_cod_cab
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in ricerca banca appoggio" + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		
			update banca_appoggio 
			set den_abi    = :fs_des_abi, 
				 den_cab    = :fs_des_cab, 
				 des_estesa = :fs_des_banca
			where cod_abi = :fs_cod_abi and cod_cab = :fs_cod_cab 
			using sqlci;
			if sqlci.sqlcode <> 0 or sqlci.sqlnrows <= 0  then    			// non esiste la banca di appoggio; tento un insert
				i_messaggio = "Errore in ricerca banca appoggio" + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if

			// eseguito update su banca appoggio; adesso eseguo update o insert su banca_sog_commerciale		
			fi_id_banca_sog_commerciale = 0
			
			select max(id_banca_sog_commerciale)
			into   :fi_id_banca_sog_commerciale
			from   banca_sog_commerciale
			where  id_anagrafica = :fi_id_anagrafica and id_banca_appoggio = :fi_id_banca_appoggio
			using sqlci;
			
			if isnull(fi_id_banca_sog_commerciale) or fi_id_banca_sog_commerciale = 0 then
				// non c'e' la banca del soggetto commerciale per questa banca;

				uof_flag_predefiniti(fs_tipo_anagrafica, ls_tipo_pagamento, fs_tipo_banca, ref ls_flag_banca_predefinita, ref ls_flag_predefinita_docfinance)
				
				if ls_flag_banca_predefinita = "S" then
					update 	banca_sog_commerciale
					set 		predefinita = 'N'
					where 	id_anagrafica = :fi_id_anagrafica
					using 	sqlci;
					if sqlci.sqlcode = -1 then
						i_messaggio = sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
				end if		
					
				if ls_flag_predefinita_docfinance = "S" then
					update 	banca_sog_commerciale
					set 		pred_docfinance= 'N'
					where 	id_anagrafica = :fi_id_anagrafica
					using 	sqlci;
					if sqlci.sqlcode = -1 then
						i_messaggio = sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
				end if		
				
				if fs_tipo_banca = "N" then
					// EnMe con Samuele: se banca nostra niente CIN e IBAN; serve per DocF 11/7/2014
					setnull(fs_cin)
					setnull(fs_iban)
					
					setnull(fs_swift)
				else
					fs_swift = left(fs_swift, 40)
				end if
				
				insert into banca_sog_commerciale (id_anagrafica, id_banca_appoggio,  modo_uso, cin, iban, predefinita, pred_docfinance, codice_swift)  
				values    (:fi_id_anagrafica, :fi_id_banca_appoggio, :fs_tipo_banca, :fs_cin, :fs_iban, :ls_flag_banca_predefinita, :ls_flag_predefinita_docfinance, :fs_swift) 
				using sqlci;
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in creazione banca del soggetto commerciale " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if
				// leggo l'ID generato e lo memorizzo in una var locale
				uof_get_identity(ref fi_id_banca_sog_commerciale)
				//guo_functions.uof_get_identity(sqlci, fi_id_banca_sog_commerciale)
				//SELECT @@identity INTO :fi_id_banca_sog_commerciale from sys.dummy using sqlci;

				// aggiunto per gestione banca azienda (segnalazione A.Bellin Ptenda 24/2/03) (ENME)
				setnull(li_id_banca_azienda)
				
				//caso "banca nostra" per fornitore
				if fs_tipo_anagrafica = "F" and fs_tipo_banca = "N" then
					select id_banca_azienda
					into   :li_id_banca_azienda  
					from   banca_azienda  
					where  id_banca_appoggio = :fi_id_banca_appoggio
					using sqlci;
					
					if sqlci.sqlcode = 100 then
						//i_messaggio = sqlci.sqlerrtext
						//i_db_errore = sqlci.sqlcode
						
						i_messaggio = "La Banca Nostra selezionata per il Fornitore non è presente in tabella banche Azienda di IMPRESA."
						i_db_errore = -1
						return -1
					end if
					
					update banca_sog_commerciale
					set id_banca_azienda = :li_id_banca_azienda
					where id_banca_sog_commerciale = :fi_id_banca_sog_commerciale
					using sqlci;
					if sqlci.sqlcode = 100 then
						i_messaggio = sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
					
				end if
				// ^^^  fine modifica
				
			else
				// imposto la banca attuale come predefinita

				uof_flag_predefiniti(fs_tipo_anagrafica, ls_tipo_pagamento, fs_tipo_banca, ref ls_flag_banca_predefinita, ref ls_flag_predefinita_docfinance)
				
				if ls_flag_banca_predefinita = "S" then
					update 	banca_sog_commerciale
					set 		predefinita = 'N'
					where  	id_anagrafica = :fi_id_anagrafica
					using 	sqlci;
					if sqlci.sqlcode = -1 then
						i_messaggio = sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
				end if		
					
				if ls_flag_predefinita_docfinance = "S" then
					update 	banca_sog_commerciale
					set 		pred_docfinance ='N'
					where  	id_anagrafica = :fi_id_anagrafica
					using 	sqlci;
					if sqlci.sqlcode = -1 then
						i_messaggio = sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
				end if		
				
				if fs_tipo_banca = "N" then
					// EnMe con Samuele: se banca nostra niente CIN e IBAN; serve per DocF 11/7/2014
					setnull(fs_cin)
					setnull(fs_iban)
					
					setnull(fs_swift)
				else
					fs_swift = left(fs_swift, 40)
				end if
				
				update 	banca_sog_commerciale
				set 		predefinita = :ls_flag_banca_predefinita,
							pred_docfinance = :ls_flag_predefinita_docfinance,
						 	iban = :fs_iban,
							cin = :fs_cin,
							modo_uso = :fs_tipo_banca,
							codice_swift = :fs_swift
				where  id_anagrafica = :fi_id_anagrafica and id_banca_appoggio = :fi_id_banca_appoggio
				using sqlci;
				if sqlci.sqlcode = -1 then
					i_messaggio = sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if


				// aggiunto per gestione banca azienda (segnalazione A.Bellin Ptenda 24/2/03) (ENME)
				setnull(li_id_banca_azienda)
				
				//caso "banca nostra" per fornitore
				if fs_tipo_anagrafica = "F" and fs_tipo_banca = "N" then
					select id_banca_azienda
					into   :li_id_banca_azienda  
					from   banca_azienda  
					where  id_banca_appoggio = :fi_id_banca_appoggio
					using sqlci;
					if sqlci.sqlcode = 100 then
						//i_messaggio = sqlci.sqlerrtext
						//i_db_errore = sqlci.sqlcode
						
						i_messaggio = "La Banca Nostra selezionata per il Fornitore non è presente in tabella banche Azienda di IMPRESA."
						i_db_errore = -1
						return -1
					end if

					update banca_sog_commerciale
					set id_banca_azienda = :li_id_banca_azienda
					where id_banca_sog_commerciale = :fi_id_banca_sog_commerciale
					using sqlci;
					if sqlci.sqlcode = 100 then
						i_messaggio = sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if

				end if
				// ^^^  fine modifica
				
	 		end if
		end if
end choose
return 0

end function

public function long uof_crea_cli_for (string fs_tipo_anagrafica, string fs_codice, string fs_cod_capoconto, string fs_partita_iva, string fs_cod_fiscale, string fs_rag_soc_1, string fs_rag_soc_2, string fs_indirizzo, string fs_localita, string fs_cap, string fs_provincia, string fs_frazione, string fs_cod_abi, string fs_cod_cab, string fs_cin, string fs_cod_abi_1, string fs_cod_cab_1, string fs_cin_1, string fs_cod_pagamento, string fs_cod_valuta, string fs_cod_nazione, long fl_ggmm_esclusione_1_da, long fl_ggmm_esclusione_1_a, long fl_ggmm_esclusione_2_da, long fl_ggmm_esclusione_2_a, long fl_data_sost_1, long fl_data_sost_2, string fs_flag_raggruppo_scadenze, string fs_telefono, string fs_fax, string fs_telex, string fs_email, string fs_cod_agente_1, string fs_iban, string fs_swift, long fl_id_doc_fic_rating, long fl_id_doc_fic_voce_fin, string fs_flag_tipo_cli_for, ref long fi_id_anagrafica, ref long fi_id_sog_commerciale);boolean				lb_trovato_soggetto=false

string					ls_messaggio_1, ls_messaggio_2, ls_des_nazione, ls_codice, ls_des_abi, ls_des_cab, ls_des_banca, ls_codice_cliente, ls_sottomastro, &
						ls_mastro, ls_des_sottomastro, ls_codice_impresa, ls_tipo_banca, ls_descrizione, ls_errore, ls_codice_esterno, ls_codice_esterno_for, &
						ls_codice_esterno_soggetto, ls_iban, ls_flag_prov_contabile, ls_flag_tipo_pagamento, ls_cod_nazione_iso
						
long					li_errore_1,  li_id_valuta, li_id_con_pagamento,li_id_banca_appoggio, li_id_banca_sog_commerciale, &
						li_return, li_id_sog_commerciale, li_id_sottomastro, li_id_mastro, li_id_banca_appoggio_sog_commerciale, &
						li_giorno_inizio_periodo, li_mese_inizio_periodo,li_giorno_fine_periodo,li_mese_fine_periodo, li_giorno_applicazione,&
						li_mese_applicazione, ll_ret, ll_cont_soggetti, li_id_nazione, ll_id_conto

long ll_cont
		  
//Donato 03-11-2008 gestione telefono, fax e cellulare, agente (quest'ultimo solo per i clienti)
long ll_id_telefono_tipo, ll_id_telefono
string ls_cod_agente_1_impresa, ls_flag_persona_fisica
long ll_id_gruppo_agenti, ll_id_capoarea

// EnMe 15-1-2012: da ora in poi non sincronizzo più pre partita iva, ma memorizzo il mio codice in tabella angrafica in modo da avere una corrispondenza uno ad uno
// questa modalità è stata concordata con Giorgio Papagno della OfficeGroup

//Donato 16/11/2012
//Modificata per gestire 2 periodi di esclusione date scadenze dal-al 1 e dal-al 2

li_errore_1 = 100
setnull(ls_codice_esterno)
setnull(ls_codice_esterno_for)
ls_cod_nazione_iso = left(fs_cod_nazione,2)
ls_flag_persona_fisica = "N"

choose case fs_tipo_anagrafica

	case "C"
		ls_codice_esterno = fs_codice
		ls_codice_esterno_soggetto = fs_codice
		
		if fs_flag_tipo_cli_for = "E" or fs_flag_tipo_cli_for = "C"  or fs_flag_tipo_cli_for = "M" then setnull(fs_provincia)
		 
		
	case "F"
		ls_codice_esterno_for = fs_codice
		ls_codice_esterno_soggetto = fs_codice

 		if fs_flag_tipo_cli_for = "E" or fs_flag_tipo_cli_for = "C" or fs_flag_tipo_cli_for = "M" then setnull(fs_provincia)

end choose

choose case fs_flag_tipo_cli_for
	case "S","P","F"
		ls_flag_prov_contabile = "T"
		ls_cod_nazione_iso = "IT"
		if fs_flag_tipo_cli_for="F" then ls_flag_persona_fisica="S"
	case "C"
		ls_flag_prov_contabile = "I"
	case "E"
		ls_flag_prov_contabile = "E"
		setnull(ls_cod_nazione_iso)
	case "M"
		ls_flag_prov_contabile = "S"
		ls_cod_nazione_iso = "IT"
	case else
		ls_flag_prov_contabile = "T"
		ls_cod_nazione_iso = "IT"
end choose

select	id_anagrafica, 
			id_sog_commerciale,
			codice
into   	:fi_id_anagrafica,
			:fi_id_sog_commerciale,
			:ls_codice_cliente
from   	sog_commerciale
where 	codice_esterno = :ls_codice_esterno_soggetto and tipo = :fs_tipo_anagrafica
using  	sqlci;


// creo il codice cliente IMPRESA


// NEL CASO DEL SOGGETTO CHE E' SIA CLIENTE CHE FORNITORE potrebbe esistere l'anagrafica e non il soggetto commerciale
// uso quindi il codice parallelo (nuovo valore passato nella funzione) che mi indica da APICE l'esistenza di questo caso.

ls_codice_cliente = uof_codice_cli_for(fs_tipo_anagrafica, fs_cod_capoconto, fs_codice)

if sqlci.sqlcode = 100 then
	
	// Intanto provo a vedere se esiste una anagrafica direttamente con il codice (improbabile).

	choose case fs_tipo_anagrafica
	
		case "C"
			
			select id_anagrafica
			into :fi_id_anagrafica
			from anagrafica
			where codice_esterno = :ls_codice_esterno
			using sqlci;
			
			if sqlci.sqlcode = 0 then
				li_errore_1 = 0
			end if
			
			
		case "F"
			
			select id_anagrafica
			into :fi_id_anagrafica
			from anagrafica
			where codice_esterno_for = :ls_codice_esterno_for
			using sqlci;
			
			if sqlci.sqlcode = 0 then
				li_errore_1 = 0
			end if
			
	end choose
	
	// se ancora non l'ho trovato
	if li_errore_1 = 100 then
	
		// Adesso provo con la partita iva
		if not isnull(fs_partita_iva) and len(fs_partita_iva) > 0 then
			
			select id_anagrafica  
			into   :fi_id_anagrafica  
			from   anagrafica
			where  par_iva = :fs_partita_iva
			using  sqlci;
			
			if sqlci.sqlcode = 0 then
				li_errore_1 = 0
			else
				// ci provo anche col codice fiscale
				if not isnull(fs_cod_fiscale) and len(fs_cod_fiscale) > 0 then
					select id_anagrafica  
					into   :fi_id_anagrafica  
					from   anagrafica
					where  cod_fiscale = :fs_cod_fiscale
					using  sqlci;
					
					if sqlci.sqlcode = 0 then
						li_errore_1 = 0
					else
						li_errore_1 = 100
					end if
					
				end if
				
			end if
			
		end if
		
	end if
	
else
	li_errore_1 = 0
	lb_trovato_soggetto = true
end if

ls_sottomastro = mid(ls_codice_cliente, 3,2)
ls_mastro = left(ls_codice_cliente,2)


select id_mastro
into   :li_id_mastro
from   mastro
where  codice = :ls_mastro
using  sqlci;
if sqlci.sqlcode <> 0 then
	i_messaggio = "Errore in ricerca del mastro "+ls_mastro+" in contabilità. " + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
end if

select id_sottomastro
into   :li_id_sottomastro
from   sottomastro
where  id_mastro = :li_id_mastro and codice = :ls_sottomastro
using sqlci;

if sqlci.sqlcode = -1 then
	i_messaggio = "Errore in ricerca del sottomastro "+ls_sottomastro+" in contabilità. " + sqlci.sqlerrtext
	i_db_errore = sqlci.sqlcode
	return -1
elseif sqlci.sqlcode = 100 then
	if fs_tipo_anagrafica = "C" then
		ls_des_sottomastro = "Clienti"
	else
		ls_des_sottomastro = "Fornitori"
	end if
	
	INSERT INTO sottomastro  
			( codice,   
			  descrizione,   
			  uso,   
			  id_azienda,   
			  id_mastro)  
	VALUES (:ls_sottomastro,   
			  :ls_des_sottomastro,   
			  :fs_tipo_anagrafica,   
			  1,   
			  :li_id_mastro)
	using sqlci;
	
	if sqlci.sqlcode <> 0 then
		i_messaggio = "Errore in inserimento del sottomastro "+ls_sottomastro+" in contabilità. " + sqlci.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if

//	guo_functions.uof_get_identity(sqlci, li_id_sottomastro)
	uof_get_identity(ref li_id_sottomastro)

end if

// Codice Valuta del cliente
select codice_impresa
into   :ls_codice
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :fs_cod_valuta;
if sqlca.sqlcode <> 0 then
	setnull(ls_codice)
	setnull(li_id_valuta)
else
	select id_valuta
	into   :li_id_valuta
	from   valuta
	where  codice = :ls_codice
	using sqlci;
	if sqlci.sqlcode <> 0 then setnull(li_id_valuta)
end if
	


// Carico la nazione nell'anagrafica

if isnull(fs_cod_nazione) or len(fs_cod_nazione) < 1 then
	
	setnull(ls_des_nazione)
	setnull(li_id_nazione)
else
	
	select des_nazione
	into   :ls_des_nazione
	from   tab_nazioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_nazione = :fs_cod_nazione;
	if sqlca.sqlcode <> 0 then
		setnull(ls_des_nazione)
		setnull(li_id_nazione)
	else
		
		select id_nazione
		into :li_id_nazione
		from nazione
		where descrizione = :ls_des_nazione
		using sqlci;
		
		if sqlci.sqlcode = 100 then
		
			insert into nazione
			(codice,
			descrizione,
			id_valuta,
			id_lingua)
			values
			(:fs_cod_nazione,
			:ls_des_nazione,
			:li_id_valuta,
			3)
			using sqlci;
			
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimentonazione in tabella nazioni. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			if sqlci.sqlcode = 0 then
//				guo_functions.uof_get_identity(sqlci, li_id_nazione)
				uof_get_identity(ref  li_id_nazione)
			end if
			
		end if
		
	end if
	
end if
// fine gestione nazione


// --------------  Verifico esistenza codice di pagamento ed eventualmente lo creo
if not isnull(fs_cod_pagamento) then
	ll_ret = uof_con_pagamento(fs_cod_pagamento, "", ls_errore)
	//se torna <0 allora c'è stato un errore (rollback in IMPRESA già fatto nell'oggetto)
	//se invece torna 1 allora il commit in IMPRESA è stato fatto
	//se torna ZERO allora probabilmente l'utente ha annullato di proposito l'operazione
	if ll_ret < 0 then
		i_messaggio = "Errore in verifica/creazione pagamento: " + ls_errore
		i_db_errore = sqlci.sqlcode
		return -1
	else
		li_id_con_pagamento = ll_ret
	end if
else
	setnull(li_id_con_pagamento)
end if

if not isnull(fs_cod_pagamento) then 
	select 	flag_tipo_pagamento
	into 		:ls_flag_tipo_pagamento
	from 		tab_pagamenti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_pagamento = :fs_cod_pagamento;
	if sqlca.sqlcode < 0 then
		i_messaggio = "Errore ricerca codice pagamento: " + sqlca.sqlerrtext
		i_db_errore = sqlci.sqlcode
		return -1
	end if
end if

// ------------------------  ( fine gestione pagamenti ) ---------------------------------------

//se abilitata la INTRA e il soggetto è CEE allora se la partita iva è vuota inietto il cod_fiscale nel campo partita_iva, altrimenti il file INTRA viene senza partita iva
//inoltre metto un parametro multiaziendale temporaneo XXX per abilitare questa cosa qui, poi a regime verrà tolto dall'IF ed eliminato dalla tabella parametri
if		ib_MVI and &
		fs_flag_tipo_cli_for = "C" and &
		(fs_partita_iva="" or isnull(fs_partita_iva)) and &
		ib_XXX then
	
	fs_partita_iva = fs_cod_fiscale
end if


if li_errore_1 = 100 then   // non esiste ne cercando per codice fiscale ne per partita iva ne per codice esterno

		select max(id_anagrafica) into :ll_cont from anagrafica using sqlci;

		INSERT INTO anagrafica 
				( id_titolo,   
				  id_valuta,   
				  rag_soc_1,   
				  rag_soc_2,   
				  indirizzo,   
				  cap,   
				  localita,   
				  provincia,   
				  nazione,   
				  cod_fiscale,   
				  par_iva,   
				  id_azienda,
				  codice_esterno,
				  codice_esterno_for,
				  id_nazione,
				  prov_contabile,
				  codice_iso,
				  persona_fisica)  
		VALUES ( 1,   
				  :li_id_valuta,   
				  :fs_rag_soc_1,   
				  :fs_rag_soc_2,   
				  :fs_indirizzo,   
				  :fs_cap,   
				  :fs_localita,   
				  :fs_provincia,   
				  :ls_des_nazione,   
				  :fs_cod_fiscale,   
				  :fs_partita_iva,   
				  1,
				  :ls_codice_esterno,
				  :ls_codice_esterno_for,
				  :li_id_nazione,
				  :ls_flag_prov_contabile,
				  :ls_cod_nazione_iso,
				  :ls_flag_persona_fisica) 
		using sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in inserimento anagrafica cliente. " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
//		Ho commentato perchè sul PC di Paolo da Errore. Non capito perchè 23/03/2021
//		guo_functions.uof_get_identity(sqlci, fi_id_anagrafica)
		uof_get_identity(ref fi_id_anagrafica)
		
	
		setnull(li_id_banca_appoggio_sog_commerciale)
		if not isnull(fs_cod_abi_1) and not isnull(fs_cod_cab_1) then			// inserisco banca di appoggio (COD_BANCA)
			select 	des_abi
			into   		:ls_des_abi
			from   	tab_abi
			where  	cod_abi = :fs_cod_abi_1;
			
			select 	agenzia
			into   		:ls_des_cab
			from   	tab_abicab
			where  	cod_abi = :fs_cod_abi_1 and cod_cab = :fs_cod_cab_1;
			
			select 	des_banca
			into   		:ls_des_banca
			from   	anag_banche
			where  	cod_azienda = :s_cs_xx.cod_azienda and
			       		cod_abi = :fs_cod_abi_1 and
					 	cod_cab = :fs_cod_cab_1;
			
			if isnull(ls_des_banca) or len(ls_des_banca) < 1 then ls_des_banca = ls_des_abi + " " + ls_des_cab
			ls_tipo_banca = "N"
			li_return = uof_abicab(	"I", fi_id_anagrafica, li_id_con_pagamento, fs_tipo_anagrafica,fs_cod_abi_1, ls_des_abi, &
											fs_cod_cab_1, ls_des_cab, fs_cin_1, ls_des_banca, ls_tipo_banca, &
											fs_iban, fs_swift, li_id_banca_appoggio, li_id_banca_sog_commerciale )
			if li_return <> 0 then
				i_messaggio = "Errore in creazione banca appoggio cliente. " + i_messaggio 
				return -1
			end if
			
			if ls_flag_tipo_pagamento = "R" then
				if fs_tipo_anagrafica = "F" then	
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				else
					//originale
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				end if
			else
				if fs_tipo_anagrafica = "F" then	
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				else
					//originale
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_appoggio
				end if
			end if
			
		end if
		if not isnull(fs_cod_abi) and not isnull(fs_cod_cab) then			// inserisco banca di appoggio (banca_clien_for)
			select des_abi
			into   :ls_des_abi
			from   tab_abi
			where  cod_abi = :fs_cod_abi;
			
			select agenzia
			into   :ls_des_cab
			from   tab_abicab
			where  cod_abi = :fs_cod_abi and cod_cab = :fs_cod_cab;
			
			select des_banca
			into   :ls_des_banca
			from   anag_banche_clien_for
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_abi = :fs_cod_abi and
					 cod_cab = :fs_cod_cab;
			
			if isnull(ls_des_banca) or len(ls_des_banca) < 1 then ls_des_banca = ls_des_abi + " " + ls_des_cab
			ls_tipo_banca = "S"
			li_return = uof_abicab(	"I", fi_id_anagrafica, li_id_con_pagamento, fs_tipo_anagrafica,&
											fs_cod_abi, ls_des_abi, fs_cod_cab, ls_des_cab, fs_cin, ls_des_banca, ls_tipo_banca, &
											fs_iban, fs_swift, li_id_banca_appoggio, li_id_banca_sog_commerciale )
			if li_return <> 0 then
				i_messaggio = "Errore in creazione banca appoggio cliente. " + i_messaggio 
				return -1
			end if
//			if fs_tipo_anagrafica = "C" then	li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
			
			if ls_flag_tipo_pagamento = "R" then
				if fs_tipo_anagrafica = "F" then	
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_appoggio
				else
					//originale
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				end if
			else
				if fs_tipo_anagrafica = "F" then	
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				else
					//originale
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_appoggio
				end if
			end if
			
			
		else
			setnull(li_id_banca_sog_commerciale)
		end if
		
		//Donato 03-11-2008 modifica per gestione agente
		if not isnull(fs_cod_agente_1) and fs_cod_agente_1 <> "" then
			
			//recupero il codice impresa dell'agente
			select codice_impresa
			into :ls_cod_agente_1_impresa
			from anag_agenti
			where cod_azienda =:s_cs_xx.cod_azienda and cod_agente = :fs_cod_agente_1;
			
			if sqlca.sqlcode = 100 then
				i_messaggio = "Attenzione, l'agente codice " + ls_cod_agente_1_impresa + " non esiste in Impresa"
				i_db_errore = sqlci.sqlcode
				return -1
			elseif sqlca.sqlcode < 0 then
				i_messaggio = "Errore in lettura del codice impresa dell'agente in APICE. " + sqlca.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			//se non impostato imposto io a null id_gruppo_agenti e id_capoarea
			if isnull(ls_cod_agente_1_impresa) or ls_cod_agente_1_impresa = "" then
				setnull(ll_id_gruppo_agenti)
				setnull(ll_id_capoarea)
			else
				//leggo da gruppo_agenti i 2 campi che mi interessano (id_gruppo_agenti, id_sog_com_capo_area)
				select id_gruppo_agenti, id_sog_com_capo_area
				into :ll_id_gruppo_agenti, :ll_id_capoarea
				from gruppo_agenti
				where codice = :ls_cod_agente_1_impresa
				using sqlci;
				
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in lettura id_gruppo_agenti e  id_sog_com_capo_area da gruppo_agenti. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if
				
				if ll_id_gruppo_agenti <= 0 then setnull(ll_id_gruppo_agenti)
				if ll_id_capoarea <= 0 then setnull(ll_id_capoarea)				
			end if
		else
			setnull(ll_id_gruppo_agenti)
			setnull(ll_id_capoarea)
		end if
		
		select max(id_sog_commerciale) into :ll_cont from sog_commerciale using sqlci;
		
		INSERT INTO sog_commerciale  
				( id_anagrafica,   
				  id_banca_sog_commerciale,   
				  id_con_pagamento,   
				  codice,   
				  tipo,  
				  scad_raggruppate,
				  id_azienda,
				  id_gruppo_agenti,
				  id_capoarea,
				  codice_esterno,
				  id_doc_fin_rating,
				  id_doc_fin_voce_fin)  
		VALUES (:fi_id_anagrafica,   
				  :li_id_banca_appoggio_sog_commerciale,   
				  :li_id_con_pagamento,   
				  :ls_codice_cliente,   
				  :fs_tipo_anagrafica,  
				  :fs_flag_raggruppo_scadenze,
				  1,
				  :ll_id_gruppo_agenti,
				  :ll_id_capoarea,
				  :ls_codice_esterno_soggetto,
				  :fl_id_doc_fic_rating,
				  :fl_id_doc_fic_voce_fin)
		using sqlci;
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in inserimento soggetto commerciale contabile. " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
//		guo_functions.uof_get_identity(sqlci, fi_id_sog_commerciale)
		uof_get_identity(ref  fi_id_sog_commerciale)
	
		
		select max(id_sog_commerciale) into :ll_cont from sog_commerciale using sqlci;
		
		
		//Donato 16/11/2012 gestione casella mail
		//MAIL
		if not isnull(fs_email) and fs_email <> "" then			
			//poichè il sog_commerciale è stato inserito, inseriamo anche l'indirizzo email
			insert into telefono
				(	id_anagrafica,
					tipo,
					num_riferimento,
					id_azienda,
					id_sog_commerciale,
					pred_per_invio_doc,
					pred_per_invio_lettere
				)
				values 
				(	:fi_id_anagrafica,
					'E',
					:fs_email,
					1,
					:fi_id_sog_commerciale,
					'S',
					'S'
				)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				//i_messaggio = "Errore in inserimento e-mail. " + sqlci.sqlerrtext
				i_messaggio = "Errore in inserimento e-mail ("
				i_messaggio += "id_anagrafica="
				if not isnull(fi_id_anagrafica) then
					i_messaggio += string(fi_id_anagrafica)
				else
					i_messaggio += "<NULL>"
				end if
				
				i_messaggio += ") , id_sog_commerciale="
				if not isnull(fi_id_sog_commerciale) then
					i_messaggio += string(fi_id_sog_commerciale)
				else
					i_messaggio += "<NULL>"
				end if
				i_messaggio += ") "+ sqlci.sqlerrtext
				
				i_db_errore = sqlci.sqlcode
				return -1
			end if			
		end if
		//------------------------------------------
		
		//Donato 03-11-2008 gestione telefono, fax e cellulare
		//TELEFONO
		if not isnull(fs_telefono) and fs_telefono <> "" then			
			//poichè il sog_commerciale è stato inserito, inseriamo anche il telefono
			insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc
				)
				values 
				(	:fi_id_anagrafica
					,'T'
					,:fs_telefono
					,1
					,:fi_id_sog_commerciale
					,'N'
				)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento telefono. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if			
		end if
		
		//FAX
		if not isnull(fs_fax) and fs_fax <> "" then
			//poichè il sog_commerciale è stato inserito, inseriamo anche il fax
			insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc
				)
				values(	:fi_id_anagrafica
					,'F'
					,:fs_fax
					,1
					,:fi_id_sog_commerciale
					,'N'
				)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento fax. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if			
		end if
		
		//CELLULARE
		if not isnull(fs_telex) and fs_telex <> "" then
			//poichè il sog_commerciale è stato inserito, inseriamo anche il cellulare
			insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc
				)
				values(	:fi_id_anagrafica
					,'C'
					,:fs_telex
					,1
					,:fi_id_sog_commerciale
					,'N'
					)
				using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento cellulare. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if			
		end if
		//fine modifica ------------------------------------------------
		
		// aggiunto da Enrico il 18/11 per la gestione delle date di scadenza posticipate
		
		// gestione prima data posticipata
		//if fl_mese_escluso_1 > 0 and not isnull(fl_mese_escluso_1) then
		if fl_ggmm_esclusione_1_da>0 and not isnull(fl_ggmm_esclusione_1_da) and fl_ggmm_esclusione_1_a >0 and not isnull(fl_ggmm_esclusione_1_a) then
			li_giorno_applicazione = int(fl_data_sost_1 / 100)
			li_mese_applicazione   = fl_data_sost_1 - (li_giorno_applicazione * 100)
			li_giorno_inizio_periodo = int(fl_ggmm_esclusione_1_da / 100)  		//1
			li_giorno_fine_periodo = int(fl_ggmm_esclusione_1_a / 100)		//31
			li_mese_inizio_periodo = integer(right(string(fl_ggmm_esclusione_1_da), 2))		 //fl_mese_escluso_1
			li_mese_fine_periodo = integer(right(string(fl_ggmm_esclusione_1_a), 2))			//fl_mese_escluso_1
			ls_descrizione = ""
			
			insert into con_spec_scadenza
					(id_azienda,
					id_anagrafica,
					giorno_inizio_periodo,
					mese_inizio_periodo,
					giorno_fine_periodo,
					mese_fine_periodo,
					descrizione,
					giorno_applicazione,
					mese_applicazione,
					id_sog_commerciale)
					values
					(1,
					:fi_id_anagrafica,
					:li_giorno_inizio_periodo,
					:li_mese_inizio_periodo,
					:li_giorno_fine_periodo,
					:li_mese_fine_periodo,
					:ls_descrizione,
					:li_giorno_applicazione,
					:li_mese_applicazione,
					:fi_id_sog_commerciale)
					using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento condizioni speciali scadenza. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if
		
		// gestione seconda data posticipata
		//if fl_mese_escluso_2 > 0 and not isnull(fl_mese_escluso_2) then
		if fl_ggmm_esclusione_2_da>0 and not isnull(fl_ggmm_esclusione_2_da) and fl_ggmm_esclusione_2_a >0 and not isnull(fl_ggmm_esclusione_2_a) then
			li_giorno_applicazione = int(fl_data_sost_2 / 100)
			li_mese_applicazione   = fl_data_sost_2 - (li_giorno_applicazione * 100)
			li_giorno_inizio_periodo = int(fl_ggmm_esclusione_2_da / 100)		//1
			li_giorno_fine_periodo = int(fl_ggmm_esclusione_2_a / 100)			//31
			li_mese_inizio_periodo = integer(right(string(fl_ggmm_esclusione_2_da), 2))			//fl_mese_escluso_2
			li_mese_fine_periodo = integer(right(string(fl_ggmm_esclusione_2_a), 2))			//fl_mese_escluso_2
			ls_descrizione = ""
			
			insert into con_spec_scadenza
			(id_azienda,
			id_anagrafica,
			giorno_inizio_periodo,
			mese_inizio_periodo,
			giorno_fine_periodo,
			mese_fine_periodo,
			descrizione,
			giorno_applicazione,
			mese_applicazione,
			id_sog_commerciale)
			values
			(1,
			:fi_id_anagrafica,
			:li_giorno_inizio_periodo,
			:li_mese_inizio_periodo,
			:li_giorno_fine_periodo,
			:li_mese_fine_periodo,
			:ls_descrizione,
			:li_giorno_applicazione,
			:li_mese_applicazione,
			:fi_id_sog_commerciale)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento condizioni speciali scadenza. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if
		

elseif fi_id_anagrafica > 0 then						  // trovato
		/* 	se esiste l'anagrafica non è detto che esista il soggetto commerciale. Quindi dopo l'update dell'anagrafica
			faccio una ricerca del soggetto commerciale e verifico se fare insert  o update */
			
		// devo distinguere fra update di cliente e di fornitore per imporre il corretto codice esterno
		
		choose case fs_tipo_anagrafica
			case "C"
				update anagrafica
				set    id_valuta = :li_id_valuta,
						  rag_soc_1= :fs_rag_soc_1,
						  rag_soc_2= :fs_rag_soc_2 ,  
						  indirizzo= :fs_indirizzo,   
						  cap= :fs_cap   ,
						  localita= :fs_localita  ,
						  provincia= :fs_provincia   ,
						  nazione= :ls_des_nazione   ,
						  cod_fiscale= :fs_cod_fiscale   ,
						  par_iva= :fs_partita_iva,
						  codice_esterno = :ls_codice_esterno,
						  id_nazione = :li_id_nazione,
						  prov_contabile = :ls_flag_prov_contabile,
						  codice_iso = :ls_cod_nazione_iso,
						  persona_fisica = :ls_flag_persona_fisica
				where   id_anagrafica = :fi_id_anagrafica
				using sqlci;
				
			case "F"	
				update anagrafica
				set    id_valuta = :li_id_valuta,
						  rag_soc_1= :fs_rag_soc_1,
						  rag_soc_2= :fs_rag_soc_2 ,  
						  indirizzo= :fs_indirizzo,   
						  cap= :fs_cap   ,
						  localita= :fs_localita  ,
						  provincia= :fs_provincia   ,
						  nazione= :ls_des_nazione   ,
						  cod_fiscale= :fs_cod_fiscale   ,
						  par_iva= :fs_partita_iva,
						  codice_esterno_for =:ls_codice_esterno_for,
						  id_nazione = :li_id_nazione,
						  prov_contabile = :ls_flag_prov_contabile,
						  codice_iso = :ls_cod_nazione_iso,
						  persona_fisica = :ls_flag_persona_fisica
				where   id_anagrafica = :fi_id_anagrafica
				using sqlci;
		end choose
		
		if sqlci.sqlcode <> 0 then
			i_messaggio = "Errore in aggiornamento anagrafica contabile cliente. " + sqlci.sqlerrtext
			i_db_errore = sqlci.sqlcode
			return -1
		end if
		
		setnull(li_id_banca_appoggio_sog_commerciale)
		if not isnull(fs_cod_abi_1) and not isnull(fs_cod_cab_1) then			// inserisco banca di appoggio (COD_BANCA)
			select des_abi
			into   :ls_des_abi
			from   tab_abi
			where  cod_abi = :fs_cod_abi_1;
			
			select agenzia
			into   :ls_des_cab
			from   tab_abicab
			where  cod_abi = :fs_cod_abi_1 and cod_cab = :fs_cod_cab_1;
			
			select des_banca, iban
			into   :ls_des_banca, :ls_iban
			from   anag_banche
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_abi = :fs_cod_abi_1 and
					 cod_cab = :fs_cod_cab_1;
			
			if isnull(ls_des_banca) or len(ls_des_banca) < 1 then ls_des_banca = ls_des_abi + " " + ls_des_cab
			ls_tipo_banca = "N"
			// attenzione in questa chiamata va passato l'iban della banca nostra, preos dalla tabella anag_banche
			li_return = uof_abicab(	"M", fi_id_anagrafica, li_id_con_pagamento, fs_tipo_anagrafica, &
											fs_cod_abi_1, ls_des_abi, fs_cod_cab_1, ls_des_cab, fs_cin_1, ls_des_banca, ls_tipo_banca, &
											ls_iban, fs_swift, li_id_banca_appoggio, li_id_banca_sog_commerciale )
			if li_return <> 0 then
				i_messaggio = "Errore in creazione banca appoggio cliente. " + i_messaggio 
				return -1
			end if
			//if fs_tipo_anagrafica = "F" then	li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
			
			if ls_flag_tipo_pagamento = "R" then
				if fs_tipo_anagrafica = "F" then	
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				else
					//originale
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				end if
			else
				if fs_tipo_anagrafica = "F" then	
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				else
					//originale
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_appoggio
				end if
			end if
		end if
		
		if not isnull(fs_cod_abi) and not isnull(fs_cod_cab) then			// inserisco banca di appoggio (BANCA_CLIEN_FOR)
			select des_abi
			into   :ls_des_abi
			from   tab_abi
			where  cod_abi = :fs_cod_abi;
			
			select agenzia
			into   :ls_des_cab
			from   tab_abicab
			where  cod_abi = :fs_cod_abi and cod_cab = :fs_cod_cab;
			
			select des_banca
			into   :ls_des_banca
			from   anag_banche_clien_for
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_abi = :fs_cod_abi and
					 cod_cab = :fs_cod_cab;
			
			if isnull(ls_des_banca) or len(ls_des_banca) < 1 then ls_des_banca = ls_des_abi + " " + ls_des_cab
			ls_tipo_banca = "S"
			li_return = uof_abicab(	"M", fi_id_anagrafica, li_id_con_pagamento, fs_tipo_anagrafica, &
											fs_cod_abi, ls_des_abi, fs_cod_cab, ls_des_cab, fs_cin, ls_des_banca, ls_tipo_banca, &
											fs_iban, fs_swift, li_id_banca_appoggio, li_id_banca_sog_commerciale )
			if li_return <> 0 then
				i_messaggio = "Errore in creazione banca appoggio cliente. " + i_messaggio 
				return -1
			end if
//			if fs_tipo_anagrafica = "C" then	li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale

			if ls_flag_tipo_pagamento = "R" then
				if fs_tipo_anagrafica = "F" then	
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_appoggio
				else
					//originale
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				end if
			else
				if fs_tipo_anagrafica = "F" then	
					li_id_banca_appoggio_sog_commerciale = li_id_banca_sog_commerciale
				else
					//originale
					//li_id_banca_appoggio_sog_commerciale = li_id_banca_appoggio
				end if
			end if
			
		else
			setnull(li_id_banca_sog_commerciale)
		end if
		
		if not lb_trovato_soggetto then
			// se non avevo già trovato l'id esatto delsoggetto commerciale
			select id_sog_commerciale
			into   :li_id_sog_commerciale
			from   sog_commerciale
			where  codice = :ls_codice_cliente
			using sqlci;
			
			// se ancora non lo trovo vado a vedere se ci sono dei soggetti commerciali con lo stesso id_anagrafica per lo stesso tipo e poi problema all'utente.
			select count(*)
			into :ll_cont_soggetti
			from sog_commerciale
			where id_anagrafica = :fi_id_anagrafica and tipo = :fs_tipo_anagrafica
			using sqlci;
			
			choose case ll_cont_soggetti
				case 0
					// non c'è
					 sqlci.sqlcode = 100
					lb_trovato_soggetto = false
				case 1
					// trovato 1
					select id_sog_commerciale
					into :li_id_sog_commerciale
					from sog_commerciale
					where id_anagrafica = :fi_id_anagrafica and tipo = :fs_tipo_anagrafica
					using sqlci;
				case else
					// più di 1: situazione confusa
					i_messaggio = "ATTENZIONE: è stato trovato più di un soggetto commerciale per la stessa angrafica ID=" + string(fs_tipo_anagrafica) + " TIPO=" + fs_tipo_anagrafica + ". VERIFICARE MANUALMENTE IL PROBLEMA IN IMPRESA."
					i_db_errore = 100
					return -1
			end choose
			
		else
			// siccome l'ho trovato, faccio in modo che vada in update
			sqlci.sqlcode = 0
			li_id_sog_commerciale = fi_id_sog_commerciale
		end if
		
		if sqlci.sqlcode = 100 then				// soggetto commerciale inesistente: lo inserisco
			
			//Donato 03-11-2008: gestione agente			
			if not isnull(fs_cod_agente_1) and fs_cod_agente_1 <> "" then				
				//recupero il codice impresa dell'agente
				select codice_impresa
				into :ls_cod_agente_1_impresa
				from anag_agenti
				where cod_azienda =:s_cs_xx.cod_azienda and cod_agente = :fs_cod_agente_1;
				
				if sqlca.sqlcode <> 0 then
					i_messaggio = "Errore in lettura del codice impresa dell'agente. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if
				
				//se non impostato imposto io a null id_gruppo_agenti e id_capoarea
				if isnull(ls_cod_agente_1_impresa) or ls_cod_agente_1_impresa = "" then
					setnull(ll_id_gruppo_agenti)
					setnull(ll_id_capoarea)
				else
					//leggo da gruppo_agenti i 2 campi che mi interessano (id_gruppo_agenti, id_sog_com_capo_area)
					select id_gruppo_agenti, id_sog_com_capo_area
					into :ll_id_gruppo_agenti, :ll_id_capoarea
					from gruppo_agenti
					where codice = :ls_cod_agente_1_impresa
					using sqlci;
					
					if sqlci.sqlcode <> 0 then
						i_messaggio = "Errore in lettura id_gruppo_agenti e  id_sog_com_capo_area da gruppo_agenti. " + sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
					
					if ll_id_gruppo_agenti <= 0 then setnull(ll_id_gruppo_agenti)
					if ll_id_capoarea <= 0 then setnull(ll_id_capoarea)				
				end if
			else
				setnull(ll_id_gruppo_agenti)
				setnull(ll_id_capoarea)
			end if
		
			INSERT INTO sog_commerciale  
					( id_anagrafica,   
					  id_banca_sog_commerciale,   
					  id_con_pagamento,   
					  codice,   
					  tipo,   
					  scad_raggruppate,
					  id_azienda,
					  id_gruppo_agenti,
					  id_capoarea,
					  codice_esterno,
					  id_doc_fin_rating,
					  id_doc_fin_voce_fin)  
			VALUES (:fi_id_anagrafica,   
					  :li_id_banca_appoggio_sog_commerciale,   
					  :li_id_con_pagamento,   
					  :ls_codice_cliente,   
					  :fs_tipo_anagrafica,  
					  :fs_flag_raggruppo_scadenze,
					  1,
					  :ll_id_gruppo_agenti,
					  :ll_id_capoarea,
					  :ls_codice_esterno_soggetto,
					  :fl_id_doc_fic_rating,
					  :fl_id_doc_fic_voce_fin)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento soggetto commerciale contabile. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
//			guo_functions.uof_get_identity(sqlci, fi_id_sog_commerciale)
			uof_get_identity(ref fi_id_sog_commerciale)
			
			//##############################################################
			
			//Donato 16/11/2012 gestione casella mail
			//MAIL
			//fare insert in telefono
			if not isnull(fs_email) and fs_email <> "" then			
				//insert
				insert into telefono
					(	id_anagrafica,
						tipo,
						num_riferimento,
						id_azienda,
						id_sog_commerciale,
						pred_per_invio_doc,
						pred_per_invio_lettere
					)
					values 
					(	:fi_id_anagrafica,
						'E',
						:fs_email,
						1,
						:fi_id_sog_commerciale,
						'S',
						'S'
					)
				using sqlci;
				if sqlci.sqlcode <> 0 then
					//i_messaggio = "Errore in inserimento e-mail. " + sqlci.sqlerrtext
					i_messaggio = "Errore in inserimento e-mail ("
					i_messaggio += "id_anagrafica="
					if not isnull(fi_id_anagrafica) then
						i_messaggio += string(fi_id_anagrafica)
					else
						i_messaggio += "<NULL>"
					end if
					
					i_messaggio += ") , id_sog_commerciale="
					if not isnull(fi_id_sog_commerciale) then
						i_messaggio += string(fi_id_sog_commerciale)
					else
						i_messaggio += "<NULL>"
					end if
					i_messaggio += ") "+ sqlci.sqlerrtext
					
					i_db_errore = sqlci.sqlcode
					return -1
				end if			
			end if
			//------------------------------------------
			
			//Donato 03-11-2008 gestione telefono, fax e cellulare
			//TELEFONO					
			//fare insert in telefono
			if not isnull(fs_telefono) and fs_telefono <> "" then
				//insert
				insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc)
				values(	:fi_id_anagrafica
					,'T'
					,:fs_telefono
					,1
					,:fi_id_sog_commerciale
					,'N')
				using sqlci;
					
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in inserimento telefono. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if				
			end if									
			
			//FAX		
			//fare insert in telefono
			if not isnull(fs_fax) and fs_fax <> "" then
				//insert
				insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc)
				values(	:fi_id_anagrafica
					,'F'
					,:fs_fax
					,1
					,:fi_id_sog_commerciale
					,'N')
				using sqlci;
					
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in inserimento fax. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			end if							
			
			//CELLULARE		
			//fare insert in telefono
			if not isnull(fs_telex) and fs_telex <> "" then
				//insert
				insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc)
				values(	:fi_id_anagrafica
					,'C'
					,:fs_telex
					,1
					,:fi_id_sog_commerciale
					,'N')
				using sqlci;
					
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in inserimento cellulare. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			end if							
			//fine modifica ------------------------------------------------
			//##############################################################
			li_id_sog_commerciale = fi_id_sog_commerciale
									
		else												// trovato anche il soggetto commerciale: lo aggiorno con i dati giusti
			
			//Donato 03-11-2008: gestione agente			
			if not isnull(fs_cod_agente_1) and fs_cod_agente_1 <> "" then				
				//recupero il codice impresa dell'agente
				select codice_impresa
				into :ls_cod_agente_1_impresa
				from anag_agenti
				where cod_azienda =:s_cs_xx.cod_azienda and cod_agente = :fs_cod_agente_1;
				
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in lettura del codice impresa dell'agente. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if
				
				//se non impostato imposto io a null id_gruppo_agenti e id_capoarea
				if isnull(ls_cod_agente_1_impresa) or ls_cod_agente_1_impresa = "" then
					setnull(ll_id_gruppo_agenti)
					setnull(ll_id_capoarea)
				else
					//leggo da gruppo_agenti i 2 campi che mi interessano (id_gruppo_agenti, id_sog_com_capo_area)
					select id_gruppo_agenti, id_sog_com_capo_area
					into :ll_id_gruppo_agenti, :ll_id_capoarea
					from gruppo_agenti
					where codice = :ls_cod_agente_1_impresa
					using sqlci;
					
					if sqlci.sqlcode <> 0 then
						i_messaggio = "Errore in lettura id_gruppo_agenti e  id_capoarea da gruppo_agenti. " + sqlci.sqlerrtext
						i_db_errore = sqlci.sqlcode
						return -1
					end if
					
					if ll_id_gruppo_agenti <= 0 then setnull(ll_id_gruppo_agenti)
					if ll_id_capoarea <= 0 then setnull(ll_id_capoarea)				
				end if
			else
				setnull(ll_id_gruppo_agenti)
				setnull(ll_id_capoarea)
			end if
			
			// EnMe 8/11/2012 
			// Necessario eseguire update per non creare conti doppi nel piano dei conti
			
			select id_conto
			into :ll_id_conto
			from 	sog_commerciale
			where  id_sog_commerciale = :li_id_sog_commerciale
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in ricerca conto contabile dalla tabella CONTO. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			update conto
			set 	codice = :ls_codice_cliente, 
					descrizione = :fs_rag_soc_1, 
					id_sottomastro = :li_id_sottomastro
			where id_conto = :ll_id_conto
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in aggiornamento record in tabella CONTO. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			update sog_commerciale  
			set    id_banca_sog_commerciale = :li_id_banca_appoggio_sog_commerciale,
					 id_con_pagamento = :li_id_con_pagamento,
					 tipo = :fs_tipo_anagrafica,
					 scad_raggruppate = :fs_flag_raggruppo_scadenze,
					 id_gruppo_agenti = :ll_id_gruppo_agenti,
					 id_capoarea = :ll_id_capoarea,
					 codice_esterno = :fs_codice,
					 codice = :ls_codice_cliente,
					 id_doc_fin_rating = :fl_id_doc_fic_rating,
					 id_doc_fin_voce_fin = :fl_id_doc_fic_voce_fin
			where  id_sog_commerciale = :li_id_sog_commerciale
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in aggiornamento soggetto commerciale contabile. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
			
			//##############################################################
			
			
			//Donato 16-11-2012 gestione casella mail
			//MAIL					
			//verificare se fare insert o update in telefono
			setnull(ll_id_telefono)
			select id_telefono
			into :ll_id_telefono
			from telefono
			where id_anagrafica = :fi_id_anagrafica and tipo = 'E' 
			and id_sog_commerciale =:li_id_sog_commerciale
			using sqlci;
			
			if isnull(ll_id_telefono) then ll_id_telefono = 0
			
			if ll_id_telefono = 0 and not isnull(fs_email) and fs_email <> "" then
				//insert
				insert into telefono
				(	id_anagrafica,
					tipo,
					num_riferimento,
					id_azienda,
					id_sog_commerciale,
					pred_per_invio_doc,
					pred_per_invio_lettere )
				values(	:fi_id_anagrafica,
							'E',
							:fs_email,
							1,
							:li_id_sog_commerciale,
							'S',
							'S')
				using sqlci;
					
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in inserimento email. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			else
				//update
				update telefono
				set num_riferimento = :fs_email
				where id_telefono = :ll_id_telefono
				using sqlci;
				
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in aggiornamento email. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			end if			
			
			//Donato 03-11-2008 gestione telefono, fax e cellulare
			//TELEFONO					
			//verificare se fare insert o update in telefono
			setnull(ll_id_telefono)
			select id_telefono
			into :ll_id_telefono
			from telefono
			where id_anagrafica = :fi_id_anagrafica and tipo = 'T' 
			and id_sog_commerciale =:li_id_sog_commerciale
			using sqlci;
			
			if isnull(ll_id_telefono) then ll_id_telefono = 0
			
			if ll_id_telefono = 0 and not isnull(fs_telefono) and fs_telefono <> "" then
				//insert
				insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc)
				values(	:fi_id_anagrafica
					,'T'
					,:fs_telefono
					,1
					,:li_id_sog_commerciale
					,'N')
				using sqlci;
					
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in inserimento telefono. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			else
				//update
				update telefono
				set num_riferimento = :fs_telefono
				where id_telefono = :ll_id_telefono
				using sqlci;
				
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in aggiornamento telefono. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			end if									
			
			//FAX		
			//verificare se fare insert o update in telefono
			setnull(ll_id_telefono)
			select id_telefono
			into :ll_id_telefono
			from telefono
			where id_anagrafica = :fi_id_anagrafica and tipo = 'F'
			and id_sog_commerciale =:li_id_sog_commerciale
			using sqlci;
			
			if isnull(ll_id_telefono) then ll_id_telefono = 0
			
			if ll_id_telefono = 0 and not isnull(fs_fax) and fs_fax <> "" then
				//insert
				insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc)
				values(	:fi_id_anagrafica
					,'F'
					,:fs_fax
					,1
					,:li_id_sog_commerciale
					,'N')
				using sqlci;
					
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in inserimento fax. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			else
				//update
				update telefono
				set num_riferimento = :fs_fax
				where id_telefono = :ll_id_telefono
				using sqlci; 
				
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in aggiornamento fax. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			end if							
			
			//CELLULARE		
			//verificare se fare insert o update in telefono
			setnull(ll_id_telefono)
			select id_telefono
			into :ll_id_telefono
			from telefono
			where id_anagrafica = :fi_id_anagrafica and tipo = 'C'
			and id_sog_commerciale =:li_id_sog_commerciale
			using sqlci;
			
			if isnull(ll_id_telefono) then ll_id_telefono = 0
			
			if ll_id_telefono = 0 and not isnull(fs_telex) and fs_telex <> "" then
				//insert
				insert into telefono
				(	 id_anagrafica
					,tipo
					,num_riferimento
					,id_azienda
					,id_sog_commerciale
					,pred_per_invio_doc)
				values(	:fi_id_anagrafica
					,'C'
					,:fs_telex
					,1
					,:li_id_sog_commerciale
					,'N')
				using sqlci;
					
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in inserimento cellulare. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			else
				//update
				update telefono
				set num_riferimento = :fs_telex
				where id_telefono = :ll_id_telefono
				using sqlci; 
				
				if sqlci.sqlcode <> 0 then
					i_messaggio = "Errore in aggiornamento cellulare. " + sqlci.sqlerrtext
					i_db_errore = sqlci.sqlcode
					return -1
				end if	
			end if							
			//fine modifica ------------------------------------------------
			//##############################################################
			
		end if
			
		// aggiunto da Enrico il 18/11 per la gestione delle date di scadenza posticipate
		// se sono in modifica dell'anagrafica cancello e poi reinserisco il dato.
		delete con_spec_scadenza
		where	id_azienda = 1 and
					id_anagrafica = :fi_id_anagrafica and
					id_sog_commerciale=:li_id_sog_commerciale
		using sqlci;
				
		// gestione prima data posticipata
		//if fl_mese_escluso_1 > 0 and not isnull(fl_mese_escluso_1) then
		if fl_ggmm_esclusione_1_da>0 and not isnull(fl_ggmm_esclusione_1_da) and fl_ggmm_esclusione_1_a>0 and not isnull(fl_ggmm_esclusione_1_a) then
			li_giorno_applicazione = int(fl_data_sost_1 / 100)
			li_mese_applicazione   = fl_data_sost_1 - (li_giorno_applicazione * 100)
			li_giorno_inizio_periodo = int(fl_ggmm_esclusione_1_da / 100)			//1
			li_giorno_fine_periodo = int(fl_ggmm_esclusione_1_a / 100)			//31
			li_mese_inizio_periodo = integer(right(string(fl_ggmm_esclusione_1_da), 2))			//fl_mese_escluso_1
			li_mese_fine_periodo = integer(right(string(fl_ggmm_esclusione_1_a), 2))			//fl_mese_escluso_1
			ls_descrizione = ""
			
			insert into con_spec_scadenza
					(id_azienda,
					id_anagrafica,
					giorno_inizio_periodo,
					mese_inizio_periodo,
					giorno_fine_periodo,
					mese_fine_periodo,
					descrizione,
					giorno_applicazione,
					mese_applicazione,
					id_sog_commerciale)
					values
					(1,
					:fi_id_anagrafica,
					:li_giorno_inizio_periodo,
					:li_mese_inizio_periodo,
					:li_giorno_fine_periodo,
					:li_mese_fine_periodo,
					:ls_descrizione,
					:li_giorno_applicazione,
					:li_mese_applicazione,
					:li_id_sog_commerciale)
					using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento condizioni speciali scadenza. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if
		
		// gestione seconda data posticipata
		//if fl_mese_escluso_2 > 0 and not isnull(fl_mese_escluso_2) then
		if fl_ggmm_esclusione_2_da>0 and not isnull(fl_ggmm_esclusione_2_da) and fl_ggmm_esclusione_2_a>0 and not isnull(fl_ggmm_esclusione_2_a) then
			li_giorno_applicazione = int(fl_data_sost_2 / 100)
			li_mese_applicazione   = fl_data_sost_2 - (li_giorno_applicazione * 100)
			li_giorno_inizio_periodo = int(fl_ggmm_esclusione_2_da / 100)				//1
			li_giorno_fine_periodo = int(fl_ggmm_esclusione_2_a / 100)					//31
			li_mese_inizio_periodo = integer(right(string(fl_ggmm_esclusione_2_da), 2))			//fl_mese_escluso_2
			li_mese_fine_periodo = integer(right(string(fl_ggmm_esclusione_2_a), 2))				//fl_mese_escluso_2
			ls_descrizione = ""
			
			insert into con_spec_scadenza
			(id_azienda,
			id_anagrafica,
			giorno_inizio_periodo,
			mese_inizio_periodo,
			giorno_fine_periodo,
			mese_fine_periodo,
			descrizione,
			giorno_applicazione,
			mese_applicazione,
			id_sog_commerciale)
			values
			(1,
			:fi_id_anagrafica,
			:li_giorno_inizio_periodo,
			:li_mese_inizio_periodo,
			:li_giorno_fine_periodo,
			:li_mese_fine_periodo,
			:ls_descrizione,
			:li_giorno_applicazione,
			:li_mese_applicazione,
			:li_id_sog_commerciale)
			using sqlci;
			if sqlci.sqlcode <> 0 then
				i_messaggio = "Errore in inserimento condizioni speciali scadenza. " + sqlci.sqlerrtext
				i_db_errore = sqlci.sqlcode
				return -1
			end if
		end if
		
end if
return 0
end function

public function integer uof_mov_intra (long al_id_reg_pd, integer ai_anno_doc, long al_num_doc, string as_tipo_doc);//tabella mov_intra di impresa
// un record per ogni nomenclatura interessata con il totale imponibile e totale peso netto defgli articoli della nomenclatura
//		id_mov_intra					<- valore autoincrementale
//		id_nomenclatura_intra		<- codice nomenclatura (quello di IMPRESA)
//		massa_netta					<- totale peso netto articoli della nomenclatura
//		importo							<- totale imponibile articoli della nomenclatura
//		id_reg_pd						<- identificativo registrazione contabile del documento

datastore				lds_data, lds_nomenclature
string						ls_sql, ls_tabella_tes, ls_tabella_det, ls_colonna_anno, ls_colonna_numero, ls_colonna_riga, ls_flag_tipo, ls_documento, ls_cod_mezzo, ls_des_mezzo, &
							ls_soggetto, ls_msg, ls_cod_nomenclatura, ls_des_nomenclatura, ls_codice, ls_riga_spaziatrice, ls_capitolo, ls_des_nomenclatura_impresa, &
							ls_cod_porto, ls_cod_natura_intra, ls_msg_trasporto

uo_log_sistema			luo_log
long						ll_index, ll_tot, ll_id_nomenclatura_intra, ll_tot2, ll_count_mov_intra, ll_id_trasporto_intra, ll_index_importo_piu_alto
dec{4}					ld_massa_netta, ld_importo, ld_importo_valuta, ld_unita_supplettiva, ld_imponibile_trasporto, ld_imponibile_trasporto_valuta, ld_importo_piu_alto
s_cs_xx_parametri		lstr_data[]
integer					li_ret


ll_count_mov_intra = 0
ls_riga_spaziatrice = "........................................................................................"

//------------------------------------------------------------------------------------------------------------------------------------
//controlla se il parametro MVI è attivo
if not ib_MVI then
	//parametro non attivo, quindi no movimentazioni INTRA
	return 0
end if


//------------------------------------------------------------------------------------------------------------------------------------
//controlla se il soggetto (cliente o fornitore è di tipo CEE)
if as_tipo_doc="FATVEN" then
	
	//controlla se il cliente è CEE
	select a.flag_tipo_cliente, t.cod_cliente, t.cod_mezzo, t.cod_porto, t.cod_natura_intra
	into :ls_flag_tipo, :ls_codice, :ls_cod_mezzo, :ls_cod_porto, :ls_cod_natura_intra
	from tes_fat_ven as t
	join anag_clienti as a on 	a.cod_azienda=t.cod_azienda and
										a.cod_cliente=t.cod_cliente
	where	t.cod_azienda=:s_cs_xx.cod_azienda and
				t.anno_registrazione=:ai_anno_doc and
				t.num_registrazione=:al_num_doc
	using sqlca;
	
	ls_tabella_tes = "tes_fat_ven"
	ls_tabella_det = "det_fat_ven"
	ls_colonna_anno = "anno_registrazione"
	ls_colonna_numero = "num_registrazione"
	ls_colonna_riga = "prog_riga_fat_ven"
	ls_soggetto = "cliente"
	ls_documento = "Fatt. Vendita N. interno " + string(ai_anno_doc)+"-"+string(al_num_doc)
	
else
	//FATACQ
	
	//controlla se il fornitore è CEE
	select a.flag_tipo_fornitore, t.cod_fornitore, t.cod_mezzo, t.cod_porto, t.cod_natura_intra
	into :ls_flag_tipo, :ls_codice, :ls_cod_mezzo, :ls_cod_porto, :ls_cod_natura_intra
	from tes_fat_acq as t
	join anag_fornitori as a on 	a.cod_azienda=t.cod_azienda and
										a.cod_fornitore=t.cod_fornitore
	where	t.cod_azienda=:s_cs_xx.cod_azienda and
				t.anno_registrazione=:ai_anno_doc and
				t.num_registrazione=:al_num_doc
	using sqlca;
	
	ls_tabella_tes = "tes_fat_acq"
	ls_tabella_det = "det_fat_acq"
	ls_colonna_anno = "anno_registrazione"
	ls_colonna_numero = "num_registrazione"
	ls_colonna_riga = "prog_riga_fat_acq"
	ls_soggetto = "fornitore"
	ls_documento = "Fatt. Acquisto N. interno " + string(ai_anno_doc)+"-"+string(al_num_doc)
	
end if

if ls_flag_tipo="C" then
else
	luo_log = create  uo_log_sistema
	luo_log.uof_write_log_sistema_not_sqlca("MOVINTRA_"+as_tipo_doc, ls_documento+ " no MOVINTRA perchè la forma giuridica del "+ls_soggetto+ " '" +ls_codice+ "' NON è CEE!")
	
	is_mov_intra = ls_riga_spaziatrice + "~r~n"+&
						"Nessun movimento Intra eseguito con ID-REG.N. "+string(al_id_reg_pd) + " in quanto la forma giuridica del " + ls_soggetto + " '"+ls_codice+"' " + " NON è CEE~r~n"+&
						ls_riga_spaziatrice
	
	destroy luo_log
	return 0
end if

//mezzo consegna
if isnull(ls_cod_mezzo) or ls_cod_mezzo="" then
	i_messaggio = "Non è stato inserito il codice del mezzo nella fattura!"
	i_db_errore = -1
	return -1
end if

//natura intra
if isnull(ls_cod_natura_intra) or ls_cod_natura_intra="" then
	i_messaggio = "Non è stato inserito il codice Natura della transazione intra nella fattura!"
	i_db_errore = -1
	return -1
end if

//porto, ovvero condizioni di consegna
if isnull(ls_cod_porto) or ls_cod_porto="" then
	i_messaggio = "Non è stato inserito il codice Porto (Condizioni Consegna intra) nella fattura!"
	i_db_errore = -1
	return -1
end if

//------------------------------------------------------------------------------------------------------------------------------------
//segnala eventuali articoli nel documento senza il peso netto unitario impostato
ls_sql = "select d."+ls_colonna_riga+", d.cod_prodotto "+&
			"from "+ls_tabella_det+" as d "+&
			"join anag_prodotti as p on	p.cod_azienda=d.cod_azienda and "+&
										"p.cod_prodotto=d.cod_prodotto "+&
			"where d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					 "d."+ls_colonna_anno+"="+string(ai_anno_doc)+" and "+&
					 "d."+ls_colonna_numero+"="+string(al_num_doc) +" and " +&
					 "(p.peso_netto is null or p.peso_netto=0) "+&
			"order by d."+ls_colonna_riga

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_msg)

if ll_tot<0 then
	i_messaggio = "Errore creazione ds per  articoli senza nomenclatura: " + ls_msg
	i_db_errore = -1
	return -1
	
elseif ll_tot>0 then
	//ci sono righe con articoli senza peso netto unitario, segnala e non contabilizzare la fattura
	ls_msg = "Ci sono righe con articoli senza peso netto unitario assegnato!~r~n"
	for ll_index=1 to ll_tot
		ls_msg += "Riga "+string(lds_data.getitemnumber(ll_index, 1)) + " - Articolo " + lds_data.getitemstring(ll_index, 2) + "~r~n"
	next
	
	i_messaggio = ls_msg
	i_db_errore = -1
	destroy lds_data
	return -1
end if

destroy lds_data




//------------------------------------------------------------------------------------------------------------------------------------
//segnala eventuali righe del documento con articoli senza nomenclatura
ls_sql = "select d."+ls_colonna_riga+", d.cod_prodotto "+&
			"from "+ls_tabella_det+" as d "+&
			"join anag_prodotti as p on	p.cod_azienda=d.cod_azienda and "+&
										"p.cod_prodotto=d.cod_prodotto "+&
			"where d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					 "d."+ls_colonna_anno+"="+string(ai_anno_doc)+" and "+&
					 "d."+ls_colonna_numero+"="+string(al_num_doc) +" and " +&
					 "p.cod_nomenclatura is null "+&
			"order by d."+ls_colonna_riga

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_msg)

if ll_tot<0 then
	i_messaggio = "Errore creazione ds per  articoli senza nomenclatura: " + ls_msg
	i_db_errore = -1
	return -1
	
elseif ll_tot>0 then
	//ci sono righe con articoli senza nomenclatura, segnala e non contabilizzare la fattura
	ls_msg = "Ci sono righe con articoli senza codice nomenclatura assegnato!~r~n"
	for ll_index=1 to ll_tot
		ls_msg += "Riga "+string(lds_data.getitemnumber(ll_index, 1)) + " - Articolo " + lds_data.getitemstring(ll_index, 2) + "~r~n"
	next
	
	i_messaggio = ls_msg
	i_db_errore = -1
	destroy lds_data
	return -1
end if

destroy lds_data

//------------------------------------------------------------------------------------------------------------------------------------
//segnala eventuali righe del documento con articoli il cui codice nomenclatura assegnato in APICE non esiste in IMPRESA (tabella nomenclatura_intra)
ls_sql = "select d."+ls_colonna_riga+", d.cod_prodotto, p.cod_nomenclatura, a.des_nomenclatura "+&
			"from "+ls_tabella_det+" as d "+&
			"join anag_prodotti as p on	p.cod_azienda=d.cod_azienda and "+&
										"p.cod_prodotto=d.cod_prodotto "+&
			"join tab_nomenclature as a on	a.cod_azienda=p.cod_azienda and "+&
										"a.cod_nomenclatura=p.cod_nomenclatura "+&
			"where d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					 "d."+ls_colonna_anno+"="+string(ai_anno_doc)+" and "+&
					 "d."+ls_colonna_numero+"="+string(al_num_doc) +" " +&
			"order by d."+ls_colonna_riga

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_msg)

if ll_tot<0 then
	i_messaggio = "Errore creazione ds righe documento con codici nomenclatura apice-impresa: " + ls_msg
	i_db_errore = -1
	return -1
else
	
	ls_msg = ""
	
	for ll_index=1 to ll_tot
		//leggi il codice nomenclatura
		ls_cod_nomenclatura = lds_data.getitemstring(ll_index, 3)
		ls_des_nomenclatura = lds_data.getitemstring(ll_index, 4)
		
		if isnull(ls_des_nomenclatura) then ls_des_nomenclatura = ""
		
		//verifica che esiste in IMPRESA
		select count(*)
		into :ll_tot2
		from nomenclatura_intra
		where codice=:ls_cod_nomenclatura
		using sqlci;
		
		if sqlci.sqlcode<0 then
			i_messaggio = "Errore controllo esistenza nomenclatura apice in impresa: " + sqlci.sqlerrtext
			i_db_errore = -1
			return -1
		else
			
			if ll_tot2>0 then
				//la nomenclatura di APICE esiste in IMPRESA, quindi prosegui
				continue
			else
				
				//la nomenclatura non esiste in IMPRESA, quindi la inserisco
				ls_capitolo = left(ls_cod_nomenclatura, 2)
				ls_des_nomenclatura_impresa = left(ls_des_nomenclatura, 80)
				
				insert into nomenclatura_intra
				(	codice,
					capitolo,
					descrizione,
					descrizione_completa,
					servizio,
					calc_unita_suppletiva,
					utilizzabile)
				values	(	:ls_cod_nomenclatura,
								:ls_capitolo,
								:ls_des_nomenclatura_impresa,
								:ls_des_nomenclatura,
								'N',
								'N',
								'S')
				using sqlci;
				
				if sqlci.sqlcode<0 then
					i_messaggio = "Errore in inserimento nomenclatura apice in impresa: " + sqlci.sqlerrtext
					i_db_errore = -1
					return -1
				end if
				
				continue
			end if
		end if
	next
end if

if ls_msg<>"" then
	//hai trovato righe con articolo il cui codice nomenclatura APICE non esiste in IMPRESA
	i_messaggio = ls_msg
	i_db_errore = -1
	destroy lds_data
	return -1
end if


//#################################################################
//nazione provenienza (se FATVEN) o nazione origine (se FATACQ)
//aggiorno la tabella reg_pd con le informazioni circa
//	- paese provenienza/origine						reg_pd.id_nazione_provenienza  (se VENDITA)     reg_pd.id_nazione_origine  (se ACQUISTI)
//	- mezzo di consegna								reg_pd.id_trasporto_intra
if uof_nazione_intra(al_id_reg_pd, ai_anno_doc, al_num_doc, as_tipo_doc) < 0 then
	//i_messaggio e i_db_errore già valorizzate dalla funzione
	return -1
end if


//#################################################################
//campi codice trasporto, natura transazione e condizioni consegna
if uof_dati_aggiuntivi_intra(al_id_reg_pd, ls_cod_mezzo, ls_cod_porto, ls_cod_natura_intra) < 0 then
	//i_messaggio e i_db_errore già valorizzate dalla funzione
	return -1
end if
////################################################################


//se arrivi a questo punto procedi con la query di calcolo

if uof_prepare_ds_mov_intra(ai_anno_doc, al_num_doc, as_tipo_doc, lstr_data[]) < 0 then
	//i_messaggio e i_db_errore già valorizzate dalla funzione
	return -1
end if

ll_tot = upperbound(lstr_data[])

is_mov_intra = ""
ls_msg_trasporto = ""
ld_importo_piu_alto = 0
ll_index_importo_piu_alto = 0

if ll_tot<0 then
	i_messaggio = "Errore creaz. ds per salvataggio in mov_intra: " + ls_msg
	i_db_errore = -1
	return -1
else
	//#################################################################################################
	//21/07/2015 modifica fatta per Simona Rossi - Lupo Solution
	//EVENTUALE IMPORTO RELATIVO A COSTO TRASPORTO IN FATTURA VA IMPUTATO ALL'IMPONIBILE PIU' ALTO FRA LE NOMENCLATURE
	//torna 	1 se non è stato specificato il parametro dei tipi dettaglio trasporto
	//			2 se non ci sono righe di trasporto (in accordo ai tipi dettaglio trasporto specificati dai parametri)
	//			0 se ha calcolato il valore del trasporto
	//			-1 in caso di errore critico
	li_ret = uof_get_imponibile_trasporto(as_tipo_doc, ai_anno_doc, al_num_doc, ld_imponibile_trasporto, ld_imponibile_trasporto_valuta)
	//-------------------------------------------------
	if li_ret = 1 then
		//no tipi dettaglio trasporto specificati in parametro aziendale DTV/DTA
		ls_msg_trasporto = "Eventuali spese trasporto non considerate in quanto i parametri DTA o DTV non esistono o non sono stati valorizzati"
		
	elseif li_ret = 2 then
		//no righe con tipo dettaglio trasporto specificato nel parametro DTV/DTA oppure trasporto pari a zero
		ls_msg_trasporto = "Eventuali spese trasporto non considerate in quanto non ci sono righe di trasporto nel documento "+&
									"con tipo dettaglio specificato dai parametri DTA o DTV oppure, se esistono, l'imponibile del trasporto è pari a zero"
		
	elseif li_ret < 0 then
		//in i_messaggio il messaggio di errore
		i_db_errore = -1
		return -1
	else
		//OK, trova la nomenclatura con imponibile più alto
		//ciclo per rincarare delle spese di trasporto
		for ll_index=1 to ll_tot
			
			ls_cod_nomenclatura = lstr_data[ll_index].parametro_s_1
			ld_importo =  lstr_data[ll_index].parametro_dec4_1
			ld_importo_valuta = lstr_data[ll_index].parametro_dec4_2
			
			if ld_importo > ld_importo_piu_alto then
				ld_importo_piu_alto = ld_importo
				ll_index_importo_piu_alto = ll_index
			end if
			
		next

		//in "ll_index_importo_piu_alto" l'indice relativo alla nomenclatura con più alto imponibile
		if ll_index_importo_piu_alto > 0 then
			lstr_data[ll_index_importo_piu_alto].parametro_dec4_1 += ld_imponibile_trasporto
			lstr_data[ll_index_importo_piu_alto].parametro_dec4_2 += ld_imponibile_trasporto_valuta
			
			ls_msg_trasporto = "Attribuito l'imponibile del trasporto pari a " + string(ld_imponibile_trasporto, "###,###,##0.00") + " alla nomeclatura " + lstr_data[ll_index_importo_piu_alto].parametro_s_1 + &
									  " in quanto risulta con imponibile più alto."
		end if
	end if
	//#################################################################################################
	
	
	for ll_index=1 to ll_tot
		
		ls_cod_nomenclatura = lstr_data[ll_index].parametro_s_1
		ld_importo =  lstr_data[ll_index].parametro_dec4_1
		ld_importo_valuta = lstr_data[ll_index].parametro_dec4_2
		ld_massa_netta =  lstr_data[ll_index].parametro_dec4_3
		ld_unita_supplettiva =  lstr_data[ll_index].parametro_dec4_4
		
		//in caso di più nomenclature corrispondenti allo stesso codice
		//prendo la prima della lista (anche se è un fatto anomalo)
		ls_sql = "select id_nomenclatura_intra from nomenclatura_intra where codice='"+ls_cod_nomenclatura+"' "
		
		ll_tot2 = guo_functions.uof_crea_datastore(lds_nomenclature, ls_sql, sqlci, ls_msg)
		if isnull(ll_tot2) then ll_tot2 = 0
		
		if ll_tot2<0 then
			i_messaggio = "Errore creaz. ds lettura id_nomenclatura da IMPRESA (per mov_intra): " + ls_msg
			i_db_errore = -1
			destroy lds_nomenclature
			destroy lds_data
			return -1
			
		elseif ll_tot2=0 then
			i_messaggio = "Non è stato possibile recuperare id_nomenclatura da IMPRESA (per mov_intra)"
			i_db_errore = -1
			destroy lds_nomenclature
			destroy lds_data
			return -1
		else
			//prendo la prima
			ll_id_nomenclatura_intra = lds_nomenclature.getitemnumber(1, 1)
			destroy lds_nomenclature
		end if
		
		//inserimento in mov_intra (la PK, id_mov_intra è autoincrementale)
		//questa INSERT inserisce una riga in mov_intra recuperando id_nomenclatura_intra
		//dalla tabella nomenclatura_intra mediante il codice nomenclatura di APICE (già controllato sopra che i codici apice-impresa coincidono)
		insert into mov_intra
		(	id_nomenclatura_intra,
			importo,
			importo_valuta,
			massa_netta,
			id_reg_pd,
			servizio,
			unita_supplettiva)
		values	(	:ll_id_nomenclatura_intra,
						:ld_importo,
						:ld_importo_valuta,
						:ld_massa_netta,
						:al_id_reg_pd,
						'N',
						:ld_unita_supplettiva)
		using sqlci;
		
		if sqlci.sqlcode<0 then
			i_messaggio = "Errore inserimento in mov_intra degli importi per la nomenclatura "+ls_cod_nomenclatura+" : " + sqlci.sqlerrtext
			i_db_errore = -1
			destroy lds_data
			return -1
		end if
		
		ll_count_mov_intra += 1
		is_mov_intra += ls_cod_nomenclatura + " " + f_des_tabella("tab_nomenclature","cod_nomenclatura = '" +  ls_cod_nomenclatura +"'", "des_nomenclatura") + "~r~n"
		is_mov_intra += "Importo: " + string(ld_importo, "###,###,##0.00") + " €          Massa Netta:" +  string(ld_massa_netta, "###,###,##0.00") + " KG          "
		is_mov_intra += "Unità Suppl.: " + string(ld_unita_supplettiva, "###,###,##0.00") + " mq"
		
		if ll_index<ll_tot then
			//Ci sono ancora righe da inserire...
			is_mov_intra += "~r~n~r~n"
		end if
		
	next
	destroy lds_data
	
	if ll_count_mov_intra>0 then
		is_mov_intra = ls_riga_spaziatrice+"~r~n"+&
							"INSERITI "+string(ll_count_mov_intra)+" MOVIMENTI INTRA con ID-REG.N. "+string(al_id_reg_pd)+" IN IMPRESA~r~n" + &
							ls_riga_spaziatrice+"~r~n"+&
							is_mov_intra + "~r~n"
							
		if ls_msg_trasporto<>"" then
			is_mov_intra += "~r~n" + ls_msg_trasporto + "~r~n"
		end if
							
		is_mov_intra += ls_riga_spaziatrice
							
		luo_log = create  uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca("MOVINTRA_"+as_tipo_doc, ls_documento+ " : inseriti N. "+string(ll_count_mov_intra)+" MOVINTRA con ID-REG.N."+string(al_id_reg_pd)+" in IMPRESA")					
		destroy luo_log
	end if
	
	
end if

return 0
end function

public function integer uof_crea_nota_contabilizzazione (long al_id_reg_pd, integer ai_anno_doc, long al_num_doc, string as_tipo_doc);string					ls_cod_nota, ls_des_nota, ls_note
integer				li_ret


setnull(ls_cod_nota)

if as_tipo_doc="FATVEN" then
	select max(cod_nota)
	into   :ls_cod_nota
	from   tes_fat_ven_note
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ai_anno_doc and
			 num_registrazione = :al_num_doc and
			 cod_nota like 'C%';
else
	//FATACQ
	select max(cod_nota)
	into   :ls_cod_nota
	from   tes_fat_acq_note
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ai_anno_doc and
			 num_registrazione = :al_num_doc and
			 cod_nota like 'C%';
end if


if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
	ls_cod_nota = "C01"
else
	ls_cod_nota = right(ls_cod_nota, 2)
	li_ret = long(ls_cod_nota)
	li_ret += 1
	ls_cod_nota = "C" + string(li_ret, "00")
end if

ls_des_nota = "Contabilizzazione Fattura"
ls_note =	"Fattura contabilizzata dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
				"ID registrazione Contabilità: " + string(al_id_reg_pd) + "~r~n" + &
				"Data contabilizzazione:" + string(today(), "dd/mm/yyyy") + "  Ora contabilizzazione:" + string(now(), "hh:mm:ss")


if as_tipo_doc="FATVEN" then
	insert into tes_fat_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
		  :ai_anno_doc,   
		  :al_num_doc,   
		  :ls_cod_nota,   
		  :ls_des_nota,   
		  :ls_note,
		  null );
else
	//FATACQ
	insert into tes_fat_acq_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
		  :ai_anno_doc,   
		  :al_num_doc,   
		  :ls_cod_nota,   
		  :ls_des_nota,   
		  :ls_note,
		  null );
end if

if sqlca.sqlcode<0 then
	i_messaggio = "Errore in salvataggio ID_REG_PD in tabella note fattura: " + sqlca.sqlerrtext
	i_db_errore = -1
	return -1
end if



return 0
end function

public function integer uof_nazione_intra (long al_id_reg_pd, integer ai_anno_doc, long al_num_doc, string as_tipo_doc);string					ls_cod_soggetto, ls_cod_nazione, ls_sog, ls_cod_iso, ls_cod_iso_impresa, ls_des_nazione
integer				li_ret
long					ll_id_nazione, ll_id_valuta_euro


//SE ACQUISTI, valorizzo
//	- dba.reg_pd.id_nazione_origine
//	- dba.reg_pd.provincia_intra 					(con il codice ISO della nazione del fornitore)
//	- dba.reg_pd.provincia_destinazione			(con la sigla della provincia dell'azienda)  			-> is_provincia_azienda


//SE VENDITE, valorizzo
//	- dba.reg_pd.id_nazione_provenienza
//	- dba.reg_pd.provincia_intra 					(con la sigla della provincia dell'azienda)				-> is_provincia_azienda
//	- dba.reg_pd.provincia_destinazione			(con il codice ISO della nazione del cliente)



if as_tipo_doc="FATVEN" then
	ls_sog = "cliente"
	
	select cod_cliente
	into   :ls_cod_soggetto
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ai_anno_doc and
			 num_registrazione = :al_num_doc;
	
	select cod_nazione
	into :ls_cod_nazione
	from anag_clienti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_cliente=:ls_cod_soggetto;
else
	//FATACQ
	
	ls_sog = "fornitore"
	
	select cod_fornitore
	into   :ls_cod_soggetto
	from   tes_fat_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ai_anno_doc and
			 num_registrazione = :al_num_doc;
	
	select cod_nazione
	into :ls_cod_nazione
	from anag_fornitori
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_fornitore=:ls_cod_soggetto;
end if


if isnull(ls_cod_nazione) or len(ls_cod_nazione) < 1 then
	//soggetto senza nazione impostata
	i_messaggio = "Il "+ls_sog+"della fattura non ha la nazione impostata! Impostare la relativa nazione in anagrafica " + ls_sog + " !"
	i_db_errore = -1
	return -1
end if

//controlla se è stato impostato il codice iso nella nazione
select cod_iso, des_nazione
into :ls_cod_iso, :ls_des_nazione
from tab_nazioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_nazione=:ls_cod_nazione;

if sqlca.sqlcode<0 then
	i_messaggio = "Errore in lettura codice ISO della nazione del "+ls_sog+" della fattura : "+sqlca.sqlerrtext
	i_db_errore = -1
	return -1
	
elseif ls_cod_iso="" or isnull(ls_cod_iso) then
	i_messaggio = "Manca l'impostazione del codice ISO della nazione '"+ls_cod_nazione
	
	if not isnull(ls_des_nazione) and ls_des_nazione<>"" then i_messaggio+= " " + ls_des_nazione
	
	i_messaggio += "' del "+ls_sog+" della fattura! Impostare il codice ISO in anagrafica nazioni di APICE!"
	
	i_db_errore = -1
	return -1
end if

//cerca la nazione in IMPRESA, se non esiste inseriscila
select id_nazione, codice_iso
into :ll_id_nazione, :ls_cod_iso_impresa
from nazione
where codice=:ls_cod_nazione
using sqlci;

if sqlci.sqlcode<0 then
	i_messaggio = "Errore in lettura nazione da IMPRESA : "+sqlci.sqlerrtext
	i_db_errore = -1
	return -1
	
elseif isnull(ll_id_nazione) or ll_id_nazione<=0 or sqlci.sqlcode=100 then
	//la nazione non esiste in IMPRESA, inseriscila
	
	//recupero il codice della valuta EURO da IMPRESA
	select id_valuta
	into :ll_id_valuta_euro
	from valuta
	where codice like 'EUR%'
	using sqlci;
	
	if ll_id_valuta_euro<=0 then setnull(ll_id_valuta_euro)
	
	insert into nazione (
				codice_iso,
				nazione_cee,
				id_lingua,
				codice,
				descrizione,
				id_valuta)
     values	(	:ls_cod_iso,
          			'S',
					null,
					:ls_cod_nazione,
					:ls_des_nazione,
					:ll_id_valuta_euro)
	using sqlci;
	
	if sqlci.sqlcode<0 then
		i_messaggio = "Errore in creazione nazione in IMPRESA : "+sqlci.sqlerrtext
		i_db_errore = -1
		return -1
	end if
	
//	guo_functions.uof_get_identity(sqlci, ll_id_nazione)
	uof_get_identity(ref ll_id_nazione)
	
else
	//nazione trovata in IMPRESA
	//verifica se è stato impostato il codice ISO, se no aggiornalo con quello della nazione di APICE
	if ls_cod_iso_impresa="" or isnull(ls_cod_iso_impresa) then
		update nazione
		set codice_iso = :ls_cod_iso
		where id_nazione=:ll_id_nazione
		using sqlci;
	end if
end if

//se arrivi fin qui in ll_id_nazione c'è l'ID della nazione da memorizzare in reg_pd
if as_tipo_doc="FATVEN" then
	//VENDITE
	update reg_pd
	set		id_nazione_provenienza=:ll_id_nazione,
			provincia_intra=:is_provincia_azienda,
			provincia_destinazione=:ls_cod_iso
	where id_reg_pd=:al_id_reg_pd
	using sqlci;
else
	//FATACQ
	//ACQUISTI
	update reg_pd
	set		id_nazione_origine=:ll_id_nazione,
			provincia_intra=:ls_cod_iso,
			provincia_destinazione=:is_provincia_azienda
	where id_reg_pd=:al_id_reg_pd
	using sqlci;
end if

if sqlci.sqlcode<0 then
	i_messaggio = "Errore in aggiornamento nazione e provinvia in tabella reg_pd di IMPRESA : "+sqlci.sqlerrtext
	i_db_errore = -1
	return -1
end if


return 0
end function

public function integer uof_dati_aggiuntivi_intra (long al_id_reg_pd, string as_cod_mezzo, string as_cod_porto, string as_cod_natura_intra);/*
codice del trasporto
natura intra
condizioni consegna (porto)
*/

long			ll_id_trasporto_intra, ll_count, ll_id_natura_intra, ll_id_cond_consegna
string			ls_descrizione



//#################################################################
//mezzo consegna
//la variabile "as_cod_mezzo" è stata già controllata qualora fosse vuota ...

//controlla che sia presente il codice di impresa in anagrafica mezzi
select codice_impresa, des_mezzo
into :ll_id_trasporto_intra, :ls_descrizione
from tab_mezzi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_mezzo=:as_cod_mezzo;
			
if sqlca.sqlcode<0 then
	i_messaggio = "Errore controllo presenza codice impresa in tab_mezzi: " + sqlca.sqlerrtext
	i_db_errore = -1
	return -1
end if
	
if isnull(ll_id_trasporto_intra) then
	i_messaggio = "Manca il codice di mappatura (APICE-IMPRESA) del mezzo di consegna '"+as_cod_mezzo+"'"
	if ls_descrizione<>"" and not isnull(ls_descrizione) then i_messaggio += " " + ls_descrizione
	i_messaggio += " indicato nella fattura!"
	i_db_errore = -1
	return -1
end if
	
//controllo che il codice impresa indicato in APICE esista effettivamente in IMPRESA
select count(*)
into :ll_count
from trasporto_intra
where id_trasporto_intra=:ll_id_trasporto_intra
using sqlci;
	
if sqlci.sqlcode<0 then
	i_messaggio = "Errore controllo esistenza trasporto_intra in impresa: " + sqlci.sqlerrtext
	i_db_errore = -1
	return -1
	
elseif isnull(ll_count) or ll_count<=0 then
	//il trasporto intra NON esiste in IMPRESA
	i_messaggio = "Il codice di mappatura "+string(ll_id_trasporto_intra)+" (APICE-IMPRESA) del mezzo di consegna '"+as_cod_mezzo+"'"
	if ls_descrizione<>"" and not isnull(ls_descrizione) then i_messaggio += " " + ls_descrizione
	i_messaggio += " indicato nella fattura non è effettivamente presente in IMPRESA! Inserirlo in IMPRESA!"
	i_db_errore = -1
	return -1
end if
	
//aggiorno reg_pd con il mezzo di trasporto indicato
update reg_pd
set id_trasporto_intra = :ll_id_trasporto_intra
where id_reg_pd=:al_id_reg_pd
using sqlci;

if sqlci.sqlcode<0 then
	i_messaggio = "Errore aggiornamento id_trasporto_intra in tabella reg_pd : " + sqlci.sqlerrtext
	i_db_errore = -1
	return -1
end if
//#################################################################


//#################################################################
//natura intra
//la variabile "as_cod_natura_intra" è stata già controllata qualora fosse vuota ...

//controlla che sia presente il codice di impresa in anagrafica mezzi
select id_impresa, descrizione
into :ll_id_natura_intra, :ls_descrizione
from tab_natura_intra
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_natura_intra=:as_cod_natura_intra;
			
if sqlca.sqlcode<0 then
	i_messaggio = "Errore controllo presenza codice impresa in tab_natura_intra: " + sqlca.sqlerrtext
	i_db_errore = -1
	return -1
end if
	
if isnull(ll_id_natura_intra) then
	i_messaggio = "Manca il codice di mappatura (APICE-IMPRESA) della natura transazione intra '"+as_cod_natura_intra+"'"
	if ls_descrizione<>"" and not isnull(ls_descrizione) then i_messaggio += " " + ls_descrizione
	i_messaggio += " indicato nella fattura!"
	i_db_errore = -1
	return -1
end if
	
//controllo che il codice impresa indicato in APICE esista effettivamente in IMPRESA
select count(*)
into :ll_count
from natura_intra
where id_natura_intra=:ll_id_natura_intra
using sqlci;
	
if sqlci.sqlcode<0 then
	i_messaggio = "Errore controllo esistenza natura_intra in impresa: " + sqlci.sqlerrtext
	i_db_errore = -1
	return -1
	
elseif isnull(ll_count) or ll_count<=0 then
	//la natura intra NON esiste in IMPRESA
	i_messaggio = "Il codice di mappatura "+string(ll_id_natura_intra)+" (APICE-IMPRESA) della natura transazione intra '"+as_cod_natura_intra+"'"
	if ls_descrizione<>"" and not isnull(ls_descrizione) then i_messaggio += " " + ls_descrizione
	i_messaggio += " indicato nella fattura non è effettivamente presente in IMPRESA! Inserirlo in IMPRESA!"
	i_db_errore = -1
	return -1
end if
	
//aggiorno reg_pd con la natura intra indicata
update reg_pd
set id_natura_intra = :ll_id_natura_intra
where id_reg_pd=:al_id_reg_pd
using sqlci;

if sqlci.sqlcode<0 then
	i_messaggio = "Errore aggiornamento id_natura_intra in tabella reg_pd : " + sqlci.sqlerrtext
	i_db_errore = -1
	return -1
end if
//#################################################################

//#################################################################
//condizioni consegna intra
//la variabile "as_cod_porto" è stata già controllata qualora fosse vuota ...

//controlla che sia presente il codice di impresa in tab_porti
select id_impresa, des_porto
into :ll_id_cond_consegna, :ls_descrizione
from tab_porti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_porto=:as_cod_porto;
			
if sqlca.sqlcode<0 then
	i_messaggio = "Errore controllo presenza codice impresa in tab_porti: " + sqlca.sqlerrtext
	i_db_errore = -1
	return -1
end if
	
if isnull(ll_id_cond_consegna) then
	i_messaggio = "Manca il codice di mappatura (APICE-IMPRESA) della condizione consegna intra (codice porto) '"+as_cod_porto+"'"
	if ls_descrizione<>"" and not isnull(ls_descrizione) then i_messaggio += " " + ls_descrizione
	i_messaggio += " indicato nella fattura!"
	i_db_errore = -1
	return -1
end if
	
//controllo che il codice impresa indicato in APICE esista effettivamente in IMPRESA
select count(*)
into :ll_count
from cond_consegna_intra
where id_cond_consegna_intra=:ll_id_cond_consegna
using sqlci;
	
if sqlci.sqlcode<0 then
	i_messaggio = "Errore controllo esistenza condizione consegna intra in impresa: " + sqlci.sqlerrtext
	i_db_errore = -1
	return -1
	
elseif isnull(ll_count) or ll_count<=0 then
	//la condizione consegna intra NON esiste in IMPRESA
	i_messaggio = "Il codice di mappatura "+string(ll_id_cond_consegna)+" (APICE-IMPRESA) della condizione consegna intra (codice porto) '"+as_cod_porto+"'"
	if ls_descrizione<>"" and not isnull(ls_descrizione) then i_messaggio += " " + ls_descrizione
	i_messaggio += " indicato nella fattura non è effettivamente presente in IMPRESA! Inserirlo in IMPRESA!"
	i_db_errore = -1
	return -1
end if
	
//aggiorno reg_pd con la condizione di consegna indicata
update reg_pd
set id_cond_consegna_intra = :ll_id_cond_consegna
where id_reg_pd=:al_id_reg_pd
using sqlci;

if sqlci.sqlcode<0 then
	i_messaggio = "Errore aggiornamento id_cond_consegna in tabella reg_pd : " + sqlci.sqlerrtext
	i_db_errore = -1
	return -1
end if
//#################################################################


return 0
end function

public function integer uof_prepare_ds_mov_intra (integer ai_anno_doc, long al_num_doc, string as_tipo_doc, ref s_cs_xx_parametri astr_data[]);
string					ls_tabella_det, ls_colonna_anno, ls_colonna_numero, ls_soggetto, ls_documento, ls_sql, ls_msg, &
						ls_cod_nomenclatura, ls_cod_nomenclatura_old, ls_cod_prodotto

datastore			lds_data

long					ll_tot, ll_index, ll_new

dec{4}				ld_importo, ld_importo_valuta, ld_unità_supplettiva, ld_quantita




if as_tipo_doc="FATVEN" then
	ls_tabella_det = "det_fat_ven"
	ls_colonna_anno = "anno_registrazione"
	ls_colonna_numero = "num_registrazione"
	ls_soggetto = "cliente"
	ls_documento = "Fatt. Vendita N. interno " + string(ai_anno_doc)+"-"+string(al_num_doc)
	
else
	//FATACQ
	ls_tabella_det = "det_fat_acq"
	ls_colonna_anno = "anno_registrazione"
	ls_colonna_numero = "num_registrazione"
	ls_soggetto = "fornitore"
	ls_documento = "Fatt. Acquisto N. interno " + string(ai_anno_doc)+"-"+string(al_num_doc)
	
end if

//il distinct è importante perchè altrimenti la left join con tab_tessuti potrebbe moltiplicare le righe
//(caso di presenza dello stesso tessuto in combinazione con più colori tessuto)
ls_sql = "select	"+&
					"p.cod_nomenclatura, "+&
					"p.cod_prodotto,"+&
					"isnull(d.imponibile_iva,0) as importo,"+&
					"isnull(d.imponibile_iva_valuta,0) as importo_valuta,"+&
					"isnull(p.peso_netto, 0) * isnull(d.quan_fatturata, 0) as massa_netta,"+&
					"isnull(d.quan_fatturata, 0) "+&
			"from "+ls_tabella_det+" as d "+&
			"join anag_prodotti as p on	p.cod_azienda=d.cod_azienda and "+&
											    "p.cod_prodotto=d.cod_prodotto "+&
			"where d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					 "d."+ls_colonna_anno+"="+string(ai_anno_doc)+" and d."+ls_colonna_numero+"="+string(al_num_doc)+" and "+&
						"d.cod_prodotto is not null and p.cod_nomenclatura is not null "+&
			"order by p.cod_nomenclatura"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_msg)

if ll_tot<0 then
	i_messaggio = "Errore creaz. ds per preparazione dati mov_intra: " + ls_msg
	i_db_errore = -1
	return -1
else
	
	//conteggiamo la somma degli importi, della massa e della unità suppletiva per nomenclatura
	ls_cod_nomenclatura_old = ""
	ll_new = 0
	
	for ll_index=1 to ll_tot
		
		ls_cod_nomenclatura = lds_data.getitemstring(ll_index, 1)
		ls_cod_prodotto = lds_data.getitemstring(ll_index, 2)
		ld_quantita = lds_data.getitemdecimal(ll_index, 6)
		ld_unità_supplettiva = uof_get_unita_supplettiva(ls_cod_prodotto, ld_quantita)
		
		if ls_cod_nomenclatura <> ls_cod_nomenclatura_old then
			//nuova nomenclatura, inserisci in struttura
			ls_cod_nomenclatura_old = ls_cod_nomenclatura
			
			ll_new += 1
			astr_data[ll_new].parametro_s_1 = ls_cod_nomenclatura										//cod_nomenclatura
			astr_data[ll_new].parametro_dec4_1 = lds_data.getitemdecimal(ll_index, 3)				//imponibile
			astr_data[ll_new].parametro_dec4_2 = lds_data.getitemdecimal(ll_index, 4)				//imponibile_valuta
			astr_data[ll_new].parametro_dec4_3 = lds_data.getitemdecimal(ll_index, 5)				//massa_netta
			astr_data[ll_new].parametro_dec4_4 = ld_unità_supplettiva										//unita suppletiva
			
		else
			//incrementa quello che c'è
			astr_data[ll_new].parametro_dec4_1 += lds_data.getitemdecimal(ll_index, 3)				//imponibile
			astr_data[ll_new].parametro_dec4_2 += lds_data.getitemdecimal(ll_index, 4)				//imponibile_valuta
			astr_data[ll_new].parametro_dec4_3 += lds_data.getitemdecimal(ll_index, 5)				//massa_netta
			astr_data[ll_new].parametro_dec4_4 += ld_unità_supplettiva									//unita suppletiva
		end if
		
	next
end if

destroy lds_data

return 0
end function

public function decimal uof_get_unita_supplettiva (string as_cod_prodotto, decimal ad_qta);string					ls_sql, ls_errore
datastore			lds_data
dec{4}				ld_valore

ld_valore = 0

ls_sql = "select	alt_tessuto "+&
			"from tab_tessuti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_tessuto='"+as_cod_prodotto+"'"


//attualmente calcoliamo la unità suppletiva come quantità totale in metri quadri (caso tessuti)
//quindi se l'articolo non è un tessuto o nel db non esiste la tabella tessuti imporremo coma unità suppletiva il valore NULLO

if guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore) > 0 then
	//vuol dire che l'articolo è un tessuto
	//leggo la prima riga, infatti se il tessuto è previsto in più colori ci saranno più righe nel datastore
	
	//***************************************************************************
	//IMPORTANTE
	//***************************************************************************
	//in caso di acquisto o vendita codice tessuto la unità suppletiva è rappresentata dal totale metri quadrati
	//poichè la um è ML è sufficiente moltiplicare la quantità fatturata per l'altezza del tessuto in tabella tab_tessuti
	//inoltre ricordarsi che il fattore 1000 è dovuto al fatto che il cliente LUPO (per ora l'unico ad usare la intrastat) usa memorizzare l'altezza tessuto in millimetri
	//per altri futuri clienti potrebbe non essere cosi ... quindi tale procedura andrà integrata
	
	ld_valore = lds_data.getitemdecimal(1, 1)
	if isnull(ld_valore) then ld_valore = 0
	
	ld_valore = (ld_valore/1000) * ad_qta
end if

if isvalid(lds_data) then destroy lds_data

if isnull(ld_valore) then ld_valore = 0

return ld_valore
end function

public function string uof_get_tipo_det_acq_trasp ();
if isnull(is_tipi_det_acq_trasp) then is_tipi_det_acq_trasp = ""

return is_tipi_det_acq_trasp
end function

public function string uof_get_tipo_det_ven_trasp ();
if isnull(is_tipi_det_ven_trasp) then is_tipi_det_ven_trasp = ""

return is_tipi_det_ven_trasp
end function

public function integer uof_get_imponibile_trasporto (string as_tipo_doc, integer ai_anno_doc, long al_num_doc, ref decimal ad_imponibile, ref decimal ad_imponibile_valuta);
string					ls_tabella_det,ls_colonna_anno, ls_colonna_numero, ls_colonna_tipo_det, ls_tipi_dettaglio_previsti, ls_sql, ls_msg

datastore			lds_data

long					ll_tot, ll_index

dec{4}				ld_valore


ad_imponibile = 0
ad_imponibile_valuta = 0

if as_tipo_doc="FATVEN" then
	ls_tabella_det = "det_fat_ven"
	ls_colonna_anno = "anno_registrazione"
	ls_colonna_numero = "num_registrazione"
	ls_colonna_tipo_det = "cod_tipo_det_ven"
	ls_tipi_dettaglio_previsti = is_tipi_det_ven_trasp
else
	//FATACQ
	ls_tabella_det = "det_fat_acq"
	ls_colonna_anno = "anno_registrazione"
	ls_colonna_numero = "num_registrazione"
	ls_colonna_tipo_det = "cod_tipo_det_acq"
	ls_tipi_dettaglio_previsti = is_tipi_det_acq_trasp
end if

if ls_tipi_dettaglio_previsti="" or isnull(ls_tipi_dettaglio_previsti) then
	//tipi dettaglio non indicati nel parametro aziendale corrispondente
	return 1
end if

//il distinct è importante perchè altrimenti la left join con tab_tessuti potrebbe moltiplicare le righe
//(caso di presenza dello stesso tessuto in combinazione con più colori tessuto)
ls_sql = "select	"+&
					"sum(isnull(d.imponibile_iva,0)) as importo,"+&
					"sum(isnull(d.imponibile_iva_valuta,0)) as importo_valuta "+&
			"from "+ls_tabella_det+" as d "+&
			"where d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					 "d."+ls_colonna_anno+"="+string(ai_anno_doc)+" and d."+ls_colonna_numero+"="+string(al_num_doc)+" and "+&
						"d."+ls_colonna_tipo_det+" in ("+ls_tipi_dettaglio_previsti+") "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_msg)

if ll_tot<0 then
	i_messaggio = "Errore creaz. ds per preparazione dati trasporto mov_intra: " + ls_msg
	i_db_errore = -1
	return -1
	
elseif ll_tot = 0 then
	//no righe trasporto
	return 2
end if

ld_valore = lds_data.getitemdecimal(1, 1)
if isnull(ld_valore) then ld_valore = 0

if ld_valore = 0 then
	ad_imponibile = 0
	ad_imponibile_valuta = 0
	return 2
end if

ad_imponibile = ld_valore

ld_valore = lds_data.getitemdecimal(1, 2)
ad_imponibile_valuta = ld_valore

return 0
end function

public function string uof_tipi_det_ven_trasporti ();string				ls_DTV, as_array[]
long				ll_index


guo_functions.uof_get_parametro_azienda("DTV", ls_DTV)

if isnull(ls_DTV) then ls_DTV = ""


g_str.explode(ls_DTV, ",", as_array[])

ls_DTV = ""

for ll_index=1 to upperbound(as_array[])
	if ll_index > 1 then ls_DTV += ","
	ls_DTV += "'" + as_array[ll_index] + "'"
next

return ls_DTV
end function

public function string uof_tipi_det_acq_trasporti ();string				ls_DTA, as_array[]
long				ll_index


guo_functions.uof_get_parametro_azienda("DTA", ls_DTA)

if isnull(ls_DTA) then ls_DTA = ""


g_str.explode(ls_DTA, ",", as_array[])

ls_DTA = ""

for ll_index=1 to upperbound(as_array[])
	if ll_index > 1 then ls_DTA += ","
	ls_DTA += "'" + as_array[ll_index] + "'"
next

return ls_DTA
end function

public subroutine uof_set_tipo_det_trasporto ();string ls_DTV, ls_DTA

ls_DTV = uof_tipi_det_ven_trasporti()
ls_DTA = uof_tipi_det_acq_trasporti()

if isnull(ls_DTV) then ls_DTV = ""
if isnull(ls_DTA) then ls_DTA = ""

is_tipi_det_acq_trasp = ls_DTA
is_tipi_det_ven_trasp = ls_DTV

return
end subroutine

public function integer uof_get_identity (ref long al_identity);string ls_db
long li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype_impresa", ls_db)
if li_risposta < 0 or trim(ls_db)="" or isnull(ls_db) then
	li_risposta = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)
	if li_risposta < 0 then return -1
end if


choose case ls_db
	case "SYBASE_ASA"
		select @@identity into :al_identity from sys.dummy using sqlci;
		
	case "ORACLE"
		// caso non previsto dal momento che impresa non gira su ORACLE
		
	case "SYBASE_ASE", "MSSQL"
		DECLARE my_cursor DYNAMIC CURSOR FOR SQLSA;
		PREPARE SQLSA FROM "SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]"  using sqlci ;
		OPEN DYNAMIC my_cursor ;
		if sqlca.sqlcode = -1 then return -1
		FETCH my_cursor INTO :al_identity ;
		CLOSE my_cursor ;	

end  choose

return 0

end function

on uo_impresa.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_impresa.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;select flag
into :is_gestione_cc
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_parametro = 'GCC';
		
if sqlca.sqlcode <> 0 then
	is_gestione_cc = "N"
end if

//parametro di attivazione scritture movimentazioni INTRA
guo_functions.uof_get_parametro("MVI", ib_MVI)
if isnull(ib_MVI) then ib_MVI = false


//parametro temporaneo
guo_functions.uof_get_parametro("XXX", ib_XXX)
if isnull(ib_XXX) then ib_XXX = false

select provincia
into :is_provincia_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(is_provincia_azienda) then is_provincia_azienda=""


end event

event destructor;//disconnect using i_impresa;
//destroy i_impresa
end event

