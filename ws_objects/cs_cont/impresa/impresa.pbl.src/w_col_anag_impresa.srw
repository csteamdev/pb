﻿$PBExportHeader$w_col_anag_impresa.srw
$PBExportComments$Finestra Creazione / Aggiornamenti Anagrafiche in Contabilità
forward
global type w_col_anag_impresa from w_cs_xx_principale
end type
type cb_verifica from commandbutton within w_col_anag_impresa
end type
type st_1 from statictext within w_col_anag_impresa
end type
type st_2 from statictext within w_col_anag_impresa
end type
type cb_1 from commandbutton within w_col_anag_impresa
end type
type cb_2 from commandbutton within w_col_anag_impresa
end type
type dw_clienti_lista from uo_cs_xx_dw within w_col_anag_impresa
end type
type dw_fornitori_lista from uo_cs_xx_dw within w_col_anag_impresa
end type
type dw_folder from u_folder within w_col_anag_impresa
end type
type cb_allinea_clienti_doc_fin from commandbutton within w_col_anag_impresa
end type
type cb_allinea_fornitori_doc_fin from commandbutton within w_col_anag_impresa
end type
type dw_verifica from datawindow within w_col_anag_impresa
end type
type dw_log from uo_std_dw within w_col_anag_impresa
end type
type st_4 from statictext within w_col_anag_impresa
end type
type st_3 from statictext within w_col_anag_impresa
end type
end forward

global type w_col_anag_impresa from w_cs_xx_principale
integer width = 3707
integer height = 2268
string title = "Aggiornamento Anagrafiche Contabilità"
cb_verifica cb_verifica
st_1 st_1
st_2 st_2
cb_1 cb_1
cb_2 cb_2
dw_clienti_lista dw_clienti_lista
dw_fornitori_lista dw_fornitori_lista
dw_folder dw_folder
cb_allinea_clienti_doc_fin cb_allinea_clienti_doc_fin
cb_allinea_fornitori_doc_fin cb_allinea_fornitori_doc_fin
dw_verifica dw_verifica
dw_log dw_log
st_4 st_4
st_3 st_3
end type
global w_col_anag_impresa w_col_anag_impresa

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_log.ib_dw_report=true

dw_clienti_lista.set_dw_options(sqlca, &
										  pcca.null_object, &
										  c_multiselect + &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete, &
										  c_InactiveDWColorUnchanged + &
										  c_ViewModeColorUnchanged  )

dw_fornitori_lista.set_dw_options(sqlca, &
										    pcca.null_object, &
										    c_multiselect + &
										    c_nonew + &
										    c_nomodify + &
										    c_nodelete, &
										  c_InactiveDWColorUnchanged + &
										  c_ViewModeColorUnchanged	)
											 
dw_verifica.settransobject(sqlca)




l_objects[1] = dw_log
dw_folder.fu_AssignTab(3, "LOG", l_Objects[])

l_objects[1] = cb_allinea_clienti_doc_fin
l_objects[2] = cb_allinea_fornitori_doc_fin
dw_folder.fu_AssignTab(5, "Utility", l_Objects[])

l_objects[1] = dw_verifica
l_objects[2] = cb_verifica
dw_folder.fu_AssignTab(4, "Verifica", l_Objects[])

l_objects[1] = dw_clienti_lista
l_objects[2] = cb_1
l_objects[3] = st_3
l_objects[4] = st_4
dw_folder.fu_AssignTab(1, "Clienti", l_Objects[])

l_objects[1] = dw_fornitori_lista
l_objects[2] = cb_2
l_objects[3] = st_1
l_objects[4] = st_2
dw_folder.fu_AssignTab(2, "Fornitori", l_Objects[])

dw_folder.fu_FolderCreate(5, 5)
                          // il primo parametri indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)



end event

on w_col_anag_impresa.create
int iCurrent
call super::create
this.cb_verifica=create cb_verifica
this.st_1=create st_1
this.st_2=create st_2
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_clienti_lista=create dw_clienti_lista
this.dw_fornitori_lista=create dw_fornitori_lista
this.dw_folder=create dw_folder
this.cb_allinea_clienti_doc_fin=create cb_allinea_clienti_doc_fin
this.cb_allinea_fornitori_doc_fin=create cb_allinea_fornitori_doc_fin
this.dw_verifica=create dw_verifica
this.dw_log=create dw_log
this.st_4=create st_4
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_verifica
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.cb_2
this.Control[iCurrent+6]=this.dw_clienti_lista
this.Control[iCurrent+7]=this.dw_fornitori_lista
this.Control[iCurrent+8]=this.dw_folder
this.Control[iCurrent+9]=this.cb_allinea_clienti_doc_fin
this.Control[iCurrent+10]=this.cb_allinea_fornitori_doc_fin
this.Control[iCurrent+11]=this.dw_verifica
this.Control[iCurrent+12]=this.dw_log
this.Control[iCurrent+13]=this.st_4
this.Control[iCurrent+14]=this.st_3
end on

on w_col_anag_impresa.destroy
call super::destroy
destroy(this.cb_verifica)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_clienti_lista)
destroy(this.dw_fornitori_lista)
destroy(this.dw_folder)
destroy(this.cb_allinea_clienti_doc_fin)
destroy(this.cb_allinea_fornitori_doc_fin)
destroy(this.dw_verifica)
destroy(this.dw_log)
destroy(this.st_4)
destroy(this.st_3)
end on

type cb_verifica from commandbutton within w_col_anag_impresa
integer x = 3195
integer y = 2000
integer width = 402
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Verifica"
end type

event clicked;string ls_codice_cliente, ls_codice, ls_tipo_anagrafica, ls_rag_soc_1, ls_des_conto
long ll_i,li_id_anagrafica,li_id_sog_commerciale
uo_impresa luo_impresa

luo_impresa = create uo_impresa
dw_verifica.retrieve()

for ll_i = 1 to dw_verifica.rowcount()
	
	ls_codice = dw_verifica.getitemstring(ll_i, "cod_cliente")
	ls_tipo_anagrafica = dw_verifica.getitemstring(ll_i, "tipo_anagrafica")

	select	id_anagrafica, 
			id_sog_commerciale,
			codice
	into   	:li_id_anagrafica,
			:li_id_sog_commerciale,
			:ls_codice_cliente
	from   sog_commerciale
	where codice_esterno = :ls_codice and tipo = :ls_tipo_anagrafica
	using  sqlci;
	
	select 	rag_soc_1
	into 		:ls_rag_soc_1
	from 		anagrafica
	where 	id_anagrafica = :li_id_anagrafica
	using 		sqlci;
	
	select 	descrizione
	into 		:ls_des_conto
	from 		conto
	where 	codice = :ls_codice_cliente
	using 		sqlci;
	
	dw_verifica.setitem(ll_i, "rag_soc_impresa", ls_rag_soc_1)
	
	dw_verifica.setitem(ll_i, "codice_impresa", ls_codice_cliente)
	
	dw_verifica.setitem(ll_i, "des_conto_contabile", ls_des_conto)
	
next
end event

type st_1 from statictext within w_col_anag_impresa
integer x = 59
integer y = 2024
integer width = 261
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Fornitore:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_col_anag_impresa
integer x = 334
integer y = 2024
integer width = 754
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_col_anag_impresa
integer x = 2318
integer y = 2008
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string				ls_cod_cliente, ls_messaggio, ls_cod_capoconto, ls_cod_abi, ls_cod_cab, ls_cod_banca_clien_for, &
					ls_cod_abi_1, ls_cod_cab_1, ls_cod_banca, ls_cin, ls_cin_1
long				ll_id_anagrafica, ll_id_sog_commerciale, ll_selezionate[], ll_num_righe, ll_riga, ll_i, ll_row,ll_id_doc_fin_rating, ll_id_doc_fin_voce_fin
uo_impresa		luo_impresa

//Donato 03-11-2008 gestione telefono, fax e cellulare e cod_agente_1
string ls_telefono, ls_fax, ls_telex, ls_cod_agente_1,ls_iban


dw_clienti_lista.get_selected_rows(ll_selezionate[]) 

luo_impresa = CREATE uo_impresa
ll_num_righe = upperbound(ll_selezionate)

dw_log.reset()

dw_folder.fu_SelectTab(3)

for ll_i = 1 to ll_num_righe
	ll_riga = ll_selezionate[ll_i]

	if s_cs_xx.parametri.impresa then
		ls_cod_capoconto = dw_clienti_lista.getitemstring(ll_riga, "cod_capoconto")
		ls_cod_cliente = dw_clienti_lista.getitemstring(ll_riga, "cod_cliente")
		
		st_4.text = ls_cod_cliente
		Yield()
		
		if isnull(ls_cod_capoconto) and len(ls_cod_cliente) < 4 then
			g_mb.messagebox("APICE","Codice capoconto obbligatorio !")
			return 1
		end if
		ls_cod_banca_clien_for = dw_clienti_lista.getitemstring(ll_riga, "cod_banca_clien_for")
		if isnull(ls_cod_banca_clien_for) then
			setnull(ls_cod_abi)
			setnull(ls_cod_cab)
			setnull(ls_cin)
		else
			select cod_abi, cod_cab, cin
			into   :ls_cod_abi, :ls_cod_cab, :ls_cin
			from   anag_banche_clien_for
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_banca_clien_for = :ls_cod_banca_clien_for;
		end if			
		ls_cod_banca = dw_clienti_lista.getitemstring(ll_riga, "cod_banca")
		if isnull(ls_cod_banca) then
			setnull(ls_cod_abi_1)
			setnull(ls_cod_cab_1)
			setnull(ls_cin_1)
		else
			select cod_abi, cod_cab, cin
			into   :ls_cod_abi_1, :ls_cod_cab_1, :ls_cin_1
			from   anag_banche
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_banca = :ls_cod_banca;
		end if			
		
		//Donato 03-11-2008 Modifica per gestione telefono, fax, cellullare
		ls_telefono = dw_clienti_lista.getitemstring(ll_riga, "telefono")
		ls_fax = dw_clienti_lista.getitemstring(ll_riga, "fax")
		ls_telex = dw_clienti_lista.getitemstring(ll_riga, "telex")
		ls_cod_agente_1 = dw_clienti_lista.getitemstring(ll_riga, "cod_agente_1")
		ls_iban = dw_clienti_lista.getitemstring(ll_riga, "iban")
		ll_id_doc_fin_rating =  dw_clienti_lista.getitemnumber(ll_riga, "id_doc_fin_rating_impresa")
		ll_id_doc_fin_voce_fin =  dw_clienti_lista.getitemnumber(ll_riga, "id_doc_fin_voce_fin_impresa")
		
		if luo_impresa.uof_crea_cli_for("C", &
										ls_cod_cliente, &
										ls_cod_capoconto, &
										dw_clienti_lista.getitemstring(ll_riga, "partita_iva"),&
										dw_clienti_lista.getitemstring(ll_riga, "cod_fiscale"), &
										dw_clienti_lista.getitemstring(ll_riga, "rag_soc_1"),&
										dw_clienti_lista.getitemstring(ll_riga, "rag_soc_2"), &
										dw_clienti_lista.getitemstring(ll_riga, "indirizzo"),&
										dw_clienti_lista.getitemstring(ll_riga, "localita"), &
										dw_clienti_lista.getitemstring(ll_riga, "cap"), &
										dw_clienti_lista.getitemstring(ll_riga, "provincia"), &
										dw_clienti_lista.getitemstring(ll_riga, "frazione"), &
										ls_cod_abi, &
										ls_cod_cab, &
										ls_cin, &
										ls_cod_abi_1, &
										ls_cod_cab_1, &
										ls_cin_1, &
										dw_clienti_lista.getitemstring(ll_riga, "cod_pagamento"), &
										dw_clienti_lista.getitemstring(ll_riga, "cod_valuta"), &
										dw_clienti_lista.getitemstring(ll_riga, "cod_nazione"), &
										dw_clienti_lista.getitemnumber(ll_riga, "ggmm_esclusione_1_da"), &
										dw_clienti_lista.getitemnumber(ll_riga, "ggmm_esclusione_1_a"), &
										dw_clienti_lista.getitemnumber(ll_riga, "ggmm_esclusione_2_da"), &
										dw_clienti_lista.getitemnumber(ll_riga, "ggmm_esclusione_2_a"), &
										dw_clienti_lista.getitemnumber(ll_riga, "data_sostituzione_1"), &
										dw_clienti_lista.getitemnumber(ll_riga, "data_sostituzione_2"), &
										dw_clienti_lista.getitemstring(ll_riga, "flag_raggruppo_scadenze"), &
										ls_telefono, ls_fax, ls_telex, &
										dw_clienti_lista.getitemstring(ll_riga, "email_amministrazione"), &
										ls_cod_agente_1, &
										ls_iban , &
										dw_clienti_lista.getitemstring(ll_riga, "swift"), &
										ll_id_doc_fin_rating , &
										ll_id_doc_fin_voce_fin , &
										dw_clienti_lista.getitemstring(ll_riga, "flag_tipo_cliente"), &
										ref ll_id_anagrafica, &
										ref ll_id_sog_commerciale ) = -1 then 
			g_mb.messagebox("APICE",luo_impresa.i_messaggio)
			rollback using sqlci;
			rollback using sqlca;
			ll_row = dw_log.insertrow(0)
			dw_log.setitem(ll_row, "note", "Cliente: " + ls_cod_cliente + " Errore di sincronizzazione con IMPRESA:~r~n" + luo_impresa.i_messaggio)
			dw_log.scrolltorow(ll_row)
			dw_clienti_lista.setitem(ll_riga,"flag_elaborato","E")
			dw_clienti_lista.resetupdate()
			Yield()
		else
			commit using sqlci;
			commit using sqlca;
			ll_row = dw_log.insertrow(0)
			dw_log.setitem(ll_row, "note", "Cliente: " + ls_cod_cliente + " correttamente sincronizzato in IMPRESA con id_anagrafica=" + string(ll_id_anagrafica) + " id_soggetto=" + string(ll_id_sog_commerciale) )
			dw_log.scrolltorow(ll_row)
			dw_clienti_lista.setitem(ll_riga,"flag_elaborato","S")
			dw_clienti_lista.resetupdate()
			Yield()
		end if
	end if
next
commit using sqlci;
destroy luo_impresa

g_mb.messagebox("APICE","Elaborazione terminata; controllare il LOG")

end event

type cb_2 from commandbutton within w_col_anag_impresa
event clicked pbm_bnclicked
integer x = 2318
integer y = 2012
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_cod_fornitore, ls_messaggio, ls_cod_conto, ls_cod_banca_clien_for, ls_cod_abi, ls_cod_cab, &
       ls_cod_banca, ls_cod_abi_1, ls_cod_cab_1, ls_cin, ls_cin_1
long   ll_id_anagrafica, ll_id_sog_commerciale, ll_selezionate[], ll_num_righe, ll_riga, ll_i, ll_row, ll_ret,ll_id_doc_fin_rating, ll_id_doc_fin_voce_fin
uo_impresa luo_impresa

//Donato 03-11-2008 gestione telefono, fax e cellulare
string ls_telefono, ls_fax, ls_telex, ls_iban

dw_fornitori_lista.get_selected_rows(ll_selezionate[]) 
luo_impresa = CREATE uo_impresa
ll_num_righe = upperbound(ll_selezionate)

dw_log.reset()

dw_folder.fu_SelectTab(3)

for ll_i = 1 to ll_num_righe
	ll_riga = ll_selezionate[ll_i]

	if s_cs_xx.parametri.impresa then
		ls_cod_conto = dw_fornitori_lista.getitemstring(ll_riga, "cod_conto")
		ls_cod_fornitore = dw_fornitori_lista.getitemstring(ll_riga, "cod_fornitore")
		
		st_2.text = ls_cod_fornitore
		Yield()
		
		if isnull(ls_cod_conto) and len(ls_cod_fornitore) < 4 then
			g_mb.messagebox("APICE","Codice capoconto obbligatorio !")
			return 1
		end if
		ls_cod_banca_clien_for = dw_fornitori_lista.getitemstring(ll_riga, "cod_banca_clien_for")
		if isnull(ls_cod_banca_clien_for) then
			setnull(ls_cod_abi)
			setnull(ls_cod_cab)
			setnull(ls_cin)
		else
			select cod_abi, cod_cab, cin
			into   :ls_cod_abi, :ls_cod_cab, :ls_cin
			from   anag_banche_clien_for
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_banca_clien_for = :ls_cod_banca_clien_for;
		end if			
		ls_cod_banca = dw_fornitori_lista.getitemstring(ll_riga, "cod_banca")
		if isnull(ls_cod_banca) then
			setnull(ls_cod_abi_1)
			setnull(ls_cod_cab_1)
			setnull(ls_cin_1)
		else
			select cod_abi, cod_cab, cin
			into   :ls_cod_abi_1, :ls_cod_cab_1, :ls_cin_1
			from   anag_banche
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_banca = :ls_cod_banca;
		end if
		
		//Donato 03-11-2008 Modifica per gestione telefono, fax, cellulare
		//a cod_agente_1 passiamo sempre ""
		ls_telefono = dw_fornitori_lista.getitemstring(ll_riga, "telefono")
		ls_fax = dw_fornitori_lista.getitemstring(ll_riga, "fax")
		ls_telex = dw_fornitori_lista.getitemstring(ll_riga, "telex")
		ls_iban = dw_fornitori_lista.getitemstring(ll_riga, "iban")
		ll_id_doc_fin_rating =  dw_fornitori_lista.getitemnumber(ll_riga, "id_doc_fin_rating_impresa")	
		ll_id_doc_fin_voce_fin = dw_fornitori_lista.getitemnumber(ll_riga, "id_doc_fin_voce_fin_impresa")
		
		if luo_impresa.uof_crea_cli_for("F", &
										ls_cod_fornitore, &
										ls_cod_conto, &
										dw_fornitori_lista.getitemstring(ll_riga, "partita_iva"),&
										dw_fornitori_lista.getitemstring(ll_riga, "cod_fiscale"), &
										dw_fornitori_lista.getitemstring(ll_riga, "rag_soc_1"),&
										dw_fornitori_lista.getitemstring(ll_riga, "rag_soc_2"), &
										dw_fornitori_lista.getitemstring(ll_riga, "indirizzo"),&
										dw_fornitori_lista.getitemstring(ll_riga, "localita"), &
										dw_fornitori_lista.getitemstring(ll_riga, "cap"), &
										dw_fornitori_lista.getitemstring(ll_riga, "provincia"), &
										dw_fornitori_lista.getitemstring(ll_riga, "frazione"), &
										ls_cod_abi, &
										ls_cod_cab, &
										ls_cin, &
										ls_cod_abi_1, &
										ls_cod_cab_1, &
										ls_cin_1, &
										dw_fornitori_lista.getitemstring(ll_riga, "cod_pagamento"), &
										dw_fornitori_lista.getitemstring(ll_riga, "cod_valuta"), &
										dw_fornitori_lista.getitemstring(ll_riga, "cod_nazione"), &
										dw_fornitori_lista.getitemnumber(ll_riga, "ggmm_esclusione_1_da"), &
										dw_fornitori_lista.getitemnumber(ll_riga, "ggmm_esclusione_1_a"), &
										dw_fornitori_lista.getitemnumber(ll_riga, "ggmm_esclusione_2_da"), &
										dw_fornitori_lista.getitemnumber(ll_riga, "ggmm_esclusione_2_a"), &
										dw_fornitori_lista.getitemnumber(ll_riga, "data_sostituzione_1"), &
										dw_fornitori_lista.getitemnumber(ll_riga, "data_sostituzione_2"), &
										"N",&
										ls_telefono, ls_fax, ls_telex, &
										dw_fornitori_lista.getitemstring(ll_riga, "casella_mail"), &
										"", &
										ls_iban, &
										dw_fornitori_lista.getitemstring(ll_riga, "swift"), &
										ll_id_doc_fin_rating, &
										ll_id_doc_fin_voce_fin, &
										dw_fornitori_lista.getitemstring(ll_riga, "flag_tipo_fornitore"), &
										ref ll_id_anagrafica, &
										ref ll_id_sog_commerciale ) = -1 then 
			g_mb.messagebox("APICE",luo_impresa.i_messaggio)
			rollback using sqlci;
			rollback using sqlca;
			ll_row = dw_log.insertrow(0)
			dw_log.setitem(ll_row, "note", "Fornitore: " + ls_cod_fornitore + " Errore di sincronizzazione con IMPRESA:~r~n" + luo_impresa.i_messaggio)
			dw_log.setrow(ll_row)
			dw_log.scrolltorow(ll_row)
			dw_fornitori_lista.setitem(ll_riga,"flag_elaborato","E")
			dw_fornitori_lista.resetupdate()
			Yield()
		else
			commit using sqlci;
			commit using sqlca;
			ll_row = dw_log.insertrow(0)
			dw_log.setitem(ll_row, "note", "Fornitore: " + ls_cod_fornitore + " correttamente sincronizzato in IMPRESA con id_anagrafica=" + string(ll_id_anagrafica) + " id_soggetto=" + string(ll_id_sog_commerciale) )
			dw_log.scrolltorow(ll_row)
			dw_fornitori_lista.setitem(ll_riga,"flag_elaborato","S")
			dw_fornitori_lista.resetupdate()
			Yield()
		end if
		
	end if
next
commit using sqlci;
destroy luo_impresa

dw_folder.fu_SelectTab(3)

g_mb.messagebox("APICE","Elaborazione terminata; controllare il LOG")

end event

type dw_clienti_lista from uo_cs_xx_dw within w_col_anag_impresa
integer x = 46
integer y = 120
integer width = 2615
integer height = 1868
integer taborder = 40
string dataobject = "d_col_anag_impresa_clienti"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_fornitori_lista from uo_cs_xx_dw within w_col_anag_impresa
event pcd_retrieve pbm_custom60
integer x = 41
integer y = 120
integer width = 2633
integer height = 1860
integer taborder = 30
string dataobject = "d_col_anag_impresa_fornitori"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_folder from u_folder within w_col_anag_impresa
integer x = 23
integer y = 20
integer width = 3625
integer height = 2116
integer taborder = 10
end type

type cb_allinea_clienti_doc_fin from commandbutton within w_col_anag_impresa
integer x = 251
integer y = 232
integer width = 2935
integer height = 156
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allineamento Voce Fin + Rating CLIENTI (IMPRESA -> APICE)"
end type

event clicked;string ls_cod_cliente
long ll_selezionate[],ll_num_righe,ll_i,ll_riga,ll_id_doc_fin_rating,ll_id_doc_fin_voce_fin, ll_row

dw_clienti_lista.get_selected_rows(ll_selezionate[]) 

ll_num_righe = upperbound(ll_selezionate)
dw_folder.fu_SelectTab(3)
ll_row = dw_log.insertrow(0)
dw_log.setitem(ll_row, "note", "INIZIO PROCEDURA ALLINEAMENTO IMPRESA -> APICE DEI DATI DOCFINANCE.")
dw_log.scrolltorow(ll_row)
Yield()

for ll_i = 1 to ll_num_righe
	
	ll_riga = ll_selezionate[ll_i]
	ls_cod_cliente = dw_clienti_lista.getitemstring(ll_riga, "cod_cliente")
	
	if s_cs_xx.parametri.impresa then
	
		select 	id_doc_fin_rating,
					id_doc_fin_voce_fin
		into 		:ll_id_doc_fin_rating,
					:ll_id_doc_fin_voce_fin
		from 		sog_commerciale
		where 	codice_esterno = :ls_cod_cliente and tipo = 'C'
		using 	sqlci ;
		
		if sqlci.sqlcode = 0 then
			
			update 	anag_clienti
			set 		id_doc_fin_rating_impresa = :ll_id_doc_fin_rating,
						id_doc_fin_voce_fin_impresa = :ll_id_doc_fin_voce_fin
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_cliente = :ls_cod_cliente;
			
			if sqlca.sqlcode < 0 then
				ll_row = dw_log.insertrow(0)
				dw_log.setitem(ll_row, "note", "Cliente: " + ls_cod_cliente + " Errore allineamento dati doc finance:~r~n" + sqlca.sqlerrtext)
				dw_log.scrolltorow(ll_row)
				Yield()
				rollback using sqlca;
				rollback using sqlci;
			else
				// tutto ok lo scrivo nel log
				ll_row = dw_log.insertrow(0)
				dw_log.setitem(ll_row, "note", "Cliente: " + ls_cod_cliente + " dati DOC FINANCE allineati con successo.")
				dw_log.scrolltorow(ll_row)
				Yield()
			end if				
			
		else
			dw_log.setitem(ll_row, "note", "Cliente: " + ls_cod_cliente + " non trovato in IMPRESA ")
			dw_log.scrolltorow(ll_row)
			Yield()
		end if
	end if
next
ll_row = dw_log.insertrow(0)
dw_log.setitem(ll_row, "note", "FINE PROCEDURA ALLINEAMENTO IMPRESA -> APICE DEI DATI DOCFINANCE.")
dw_log.scrolltorow(ll_row)

rollback using sqlci;
commit using sqlca;

end event

type cb_allinea_fornitori_doc_fin from commandbutton within w_col_anag_impresa
integer x = 251
integer y = 404
integer width = 2935
integer height = 156
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allineamento Voce Fin + Rating FORNITORI (IMPRESA -> APICE)"
end type

event clicked;string ls_cod_fornitore
long ll_selezionate[],ll_num_righe,ll_i,ll_riga,ll_id_doc_fin_rating,ll_id_doc_fin_voce_fin, ll_row

dw_fornitori_lista.get_selected_rows(ll_selezionate[]) 

ll_num_righe = upperbound(ll_selezionate)
dw_folder.fu_SelectTab(3)
ll_row = dw_log.insertrow(0)
dw_log.setitem(ll_row, "note", "INIZIO PROCEDURA ALLINEAMENTO IMPRESA -> APICE DEI DATI DOCFINANCE.")
dw_log.scrolltorow(ll_row)
Yield()

for ll_i = 1 to ll_num_righe
	
	ll_riga = ll_selezionate[ll_i]
	ls_cod_fornitore = dw_fornitori_lista.getitemstring(ll_riga, "cod_fornitore")
	
	if s_cs_xx.parametri.impresa then
	
		select 	id_doc_fin_rating,
					id_doc_fin_voce_fin
		into 		:ll_id_doc_fin_rating,
					:ll_id_doc_fin_voce_fin
		from 		sog_commerciale
		where 	codice_esterno = :ls_cod_fornitore and tipo = 'F'
		using 	sqlci ;
		
		if sqlci.sqlcode = 0 then
			
			update 	anag_fornitori
			set 		id_doc_fin_rating_impresa = :ll_id_doc_fin_rating,
						id_doc_fin_voce_fin_impresa = :ll_id_doc_fin_voce_fin
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_fornitore = :ls_cod_fornitore;
			
			if sqlca.sqlcode < 0 then
				ll_row = dw_log.insertrow(0)
				dw_log.setitem(ll_row, "note", "Fornitore: " + ls_cod_fornitore + " Errore allineamento dati doc finance:~r~n" + sqlca.sqlerrtext)
				dw_log.scrolltorow(ll_row)
				Yield()
				rollback using sqlca;
				rollback using sqlci;
			else
				// tutto ok lo scrivo nel log
				ll_row = dw_log.insertrow(0)
				dw_log.setitem(ll_row, "note", "Fornitore: " + ls_cod_fornitore + " dati DOC FINANCE allineati con successo.")
				dw_log.scrolltorow(ll_row)
				Yield()
			end if				
			
		else
			dw_log.setitem(ll_row, "note", "Fornitore: " + ls_cod_fornitore + " non trovato in IMPRESA ")
			dw_log.scrolltorow(ll_row)
			Yield()
		end if
	end if
next
ll_row = dw_log.insertrow(0)
dw_log.setitem(ll_row, "note", "FINE PROCEDURA ALLINEAMENTO IMPRESA -> APICE DEI DATI DOCFINANCE.")
dw_log.scrolltorow(ll_row)

rollback using sqlci;
commit using sqlca;

end event

type dw_verifica from datawindow within w_col_anag_impresa
integer x = 50
integer y = 124
integer width = 3547
integer height = 1860
integer taborder = 60
string title = "none"
string dataobject = "d_col_anag_impresa_verifica"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_log from uo_std_dw within w_col_anag_impresa
integer x = 50
integer y = 124
integer width = 3547
integer height = 1860
integer taborder = 50
string dataobject = "d_col_anag_impresa_log"
boolean border = false
boolean hsplitscroll = true
borderstyle borderstyle = stylebox!
boolean righttoleft = true
end type

type st_4 from statictext within w_col_anag_impresa
integer x = 329
integer y = 2020
integer width = 754
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type st_3 from statictext within w_col_anag_impresa
integer x = 55
integer y = 2020
integer width = 261
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Cliente:"
boolean focusrectangle = false
end type

