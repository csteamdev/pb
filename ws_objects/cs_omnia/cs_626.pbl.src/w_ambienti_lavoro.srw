﻿$PBExportHeader$w_ambienti_lavoro.srw
$PBExportComments$Finestra Ambienti di Lavoro
forward
global type w_ambienti_lavoro from w_cs_xx_principale
end type
type dw_ambienti_lavoro_lista from uo_cs_xx_dw within w_ambienti_lavoro
end type
type cb_documento from commandbutton within w_ambienti_lavoro
end type
type dw_ambienti_lavoro_det from uo_cs_xx_dw within w_ambienti_lavoro
end type
end forward

global type w_ambienti_lavoro from w_cs_xx_principale
integer width = 2619
integer height = 1640
string title = "Ambienti Lavoro"
dw_ambienti_lavoro_lista dw_ambienti_lavoro_lista
cb_documento cb_documento
dw_ambienti_lavoro_det dw_ambienti_lavoro_det
end type
global w_ambienti_lavoro w_ambienti_lavoro

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_ambienti_lavoro_lista.set_dw_key("cod_azienda")
dw_ambienti_lavoro_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_ambienti_lavoro_det.set_dw_options(sqlca,dw_ambienti_lavoro_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_ambienti_lavoro_lista


end event

on w_ambienti_lavoro.create
int iCurrent
call super::create
this.dw_ambienti_lavoro_lista=create dw_ambienti_lavoro_lista
this.cb_documento=create cb_documento
this.dw_ambienti_lavoro_det=create dw_ambienti_lavoro_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ambienti_lavoro_lista
this.Control[iCurrent+2]=this.cb_documento
this.Control[iCurrent+3]=this.dw_ambienti_lavoro_det
end on

on w_ambienti_lavoro.destroy
call super::destroy
destroy(this.dw_ambienti_lavoro_lista)
destroy(this.cb_documento)
destroy(this.dw_ambienti_lavoro_det)
end on

type dw_ambienti_lavoro_lista from uo_cs_xx_dw within w_ambienti_lavoro
integer x = 23
integer y = 20
integer width = 2149
integer height = 500
integer taborder = 10
string dataobject = "d_ambienti_lavoro_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_view;call super::pcd_view;cb_documento.enabled = true

end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event pcd_new;call super::pcd_new;cb_documento.enabled = false

end event

event pcd_modify;call super::pcd_modify;cb_documento.enabled = false

end event

type cb_documento from commandbutton within w_ambienti_lavoro
event clicked pbm_bnclicked
integer x = 2194
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_ambiente, ls_db, ls_doc
integer li_risposta
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)

ls_cod_ambiente = dw_ambienti_lavoro_lista.getitemstring(dw_ambienti_lavoro_lista.getrow(), "cod_ambiente")

select prog_mimetype
into :ll_prog_mimetype
from tab_ambienti_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_ambiente = :ls_cod_ambiente;
	
selectblob note_esterne
into :lbl_blob
from tab_ambienti_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_ambiente = :ls_cod_ambiente;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_ambienti_lavoro
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_ambiente = :ls_cod_ambiente;
	else
		updateblob tab_ambienti_lavoro
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_ambiente = :ls_cod_ambiente;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update tab_ambienti_lavoro
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_ambiente = :ls_cod_ambiente;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//
//	li_risposta = f_crea_sqlcb(sqlcb)
//
//	selectblob note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_ambienti_lavoro
//	where      cod_azienda = :s_cs_xx.cod_azienda
//	and		  cod_ambiente = :ls_cod_ambiente
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_ambienti_lavoro
//	where      cod_azienda = :s_cs_xx.cod_azienda
//	and		  cod_ambiente = :ls_cod_ambiente;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//window_open(w_ole, 0)
//
//setpointer(hourglass!)
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tab_ambienti_lavoro
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda
//		and		  cod_ambiente = :ls_cod_ambiente 
//		using      sqlcb;
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob tab_ambienti_lavoro
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda
//		and		  cod_ambiente = :ls_cod_ambiente ;
//		
//	end if
//	commit;
//
//end if
setpointer(arrow!)
end event

type dw_ambienti_lavoro_det from uo_cs_xx_dw within w_ambienti_lavoro
integer x = 23
integer y = 540
integer width = 2537
integer height = 980
integer taborder = 30
string dataobject = "d_ambienti_lavoro_det"
borderstyle borderstyle = styleraised!
end type

