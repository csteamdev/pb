﻿$PBExportHeader$w_cat_disp_protezione.srw
$PBExportComments$Finestra Tabella Categorie Dispositivi Protezione
forward
global type w_cat_disp_protezione from w_cs_xx_principale
end type
type dw_cat_disp_protezione_lista from uo_cs_xx_dw within w_cat_disp_protezione
end type
type dw_cat_disp_protezione_det from uo_cs_xx_dw within w_cat_disp_protezione
end type
end forward

global type w_cat_disp_protezione from w_cs_xx_principale
int Width=2305
int Height=1045
boolean TitleBar=true
string Title="Cat. Dispositivi Protezione"
dw_cat_disp_protezione_lista dw_cat_disp_protezione_lista
dw_cat_disp_protezione_det dw_cat_disp_protezione_det
end type
global w_cat_disp_protezione w_cat_disp_protezione

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_cat_disp_protezione_lista.set_dw_key("cod_azienda")
dw_cat_disp_protezione_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_cat_disp_protezione_det.set_dw_options(sqlca,dw_cat_disp_protezione_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_cat_disp_protezione_lista


end event

on w_cat_disp_protezione.create
int iCurrent
call w_cs_xx_principale::create
this.dw_cat_disp_protezione_lista=create dw_cat_disp_protezione_lista
this.dw_cat_disp_protezione_det=create dw_cat_disp_protezione_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_cat_disp_protezione_lista
this.Control[iCurrent+2]=dw_cat_disp_protezione_det
end on

on w_cat_disp_protezione.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_cat_disp_protezione_lista)
destroy(this.dw_cat_disp_protezione_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_cat_disp_protezione_det,"cod_parte_corpo",sqlca,&
                 "tab_parti_corpo","cod_parte_corpo","des_parte_corpo",&
                 "(tab_parti_corpo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_parti_corpo.flag_blocco <> 'S') or (tab_parti_corpo.flag_blocco = 'S' and tab_parti_corpo.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

end event

type dw_cat_disp_protezione_lista from uo_cs_xx_dw within w_cat_disp_protezione
int X=23
int Y=21
int Width=2218
int Height=501
int TabOrder=10
string DataObject="d_cat_disp_protezione_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_cat_disp_protezione_det from uo_cs_xx_dw within w_cat_disp_protezione
int X=23
int Y=541
int Width=2218
int Height=381
int TabOrder=20
string DataObject="d_cat_disp_protezione_det"
BorderStyle BorderStyle=StyleRaised!
end type

