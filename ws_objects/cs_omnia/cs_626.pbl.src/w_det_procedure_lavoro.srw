﻿$PBExportHeader$w_det_procedure_lavoro.srw
$PBExportComments$Finestra Dettaglio Procedure Lavoro
forward
global type w_det_procedure_lavoro from w_cs_xx_principale
end type
type dw_det_procedure_lavoro_lista from uo_cs_xx_dw within w_det_procedure_lavoro
end type
type cb_1 from commandbutton within w_det_procedure_lavoro
end type
type cb_documento from commandbutton within w_det_procedure_lavoro
end type
type dw_det_procedure_lavoro_det_1 from uo_cs_xx_dw within w_det_procedure_lavoro
end type
type dw_folder from u_folder within w_det_procedure_lavoro
end type
type dw_det_procedure_lavoro_det_2 from uo_cs_xx_dw within w_det_procedure_lavoro
end type
end forward

global type w_det_procedure_lavoro from w_cs_xx_principale
int Width=2190
int Height=1529
boolean TitleBar=true
string Title="Dettaglio Procedure Lavoro"
dw_det_procedure_lavoro_lista dw_det_procedure_lavoro_lista
cb_1 cb_1
cb_documento cb_documento
dw_det_procedure_lavoro_det_1 dw_det_procedure_lavoro_det_1
dw_folder dw_folder
dw_det_procedure_lavoro_det_2 dw_det_procedure_lavoro_det_2
end type
global w_det_procedure_lavoro w_det_procedure_lavoro

type variables
boolean ib_new=FALSE
long il_new_row
end variables

event pc_setwindow;call super::pc_setwindow;string ls_flag_amb_sic
windowobject l_objects[ ]

l_objects[1] = dw_det_procedure_lavoro_det_1
dw_folder.fu_AssignTab(1, "Dati", l_Objects[])
l_objects[1] = dw_det_procedure_lavoro_det_2
dw_folder.fu_AssignTab(2, "Valutazione", l_Objects[])

dw_folder.fu_FolderCreate(2,4)
dw_folder.fu_SelectTab(1)

dw_det_procedure_lavoro_lista.set_dw_key("cod_azienda")
dw_det_procedure_lavoro_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_det_procedure_lavoro_det_1.set_dw_options(sqlca,dw_det_procedure_lavoro_lista,c_sharedata+c_scrollparent,c_default)
dw_det_procedure_lavoro_det_2.set_dw_options(sqlca,dw_det_procedure_lavoro_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_det_procedure_lavoro_lista



end event

on w_det_procedure_lavoro.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_procedure_lavoro_lista=create dw_det_procedure_lavoro_lista
this.cb_1=create cb_1
this.cb_documento=create cb_documento
this.dw_det_procedure_lavoro_det_1=create dw_det_procedure_lavoro_det_1
this.dw_folder=create dw_folder
this.dw_det_procedure_lavoro_det_2=create dw_det_procedure_lavoro_det_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_procedure_lavoro_lista
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_documento
this.Control[iCurrent+4]=dw_det_procedure_lavoro_det_1
this.Control[iCurrent+5]=dw_folder
this.Control[iCurrent+6]=dw_det_procedure_lavoro_det_2
end on

on w_det_procedure_lavoro.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_procedure_lavoro_lista)
destroy(this.cb_1)
destroy(this.cb_documento)
destroy(this.dw_det_procedure_lavoro_det_1)
destroy(this.dw_folder)
destroy(this.dw_det_procedure_lavoro_det_2)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_procedure_lavoro_det_1,"cod_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo", &
                 "(tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_paragrafi_iso.flag_blocco <> 'S') or (tab_paragrafi_iso.flag_blocco = 'S' and tab_paragrafi_iso.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_det_procedure_lavoro_det_1,"cod_disp_protezione",sqlca,&
                 "tab_disp_protezione","cod_disp_protezione","des_disp_protezione",&
                 "(tab_disp_protezione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_disp_protezione.flag_blocco <> 'S') or (tab_disp_protezione.flag_blocco = 'S' and tab_disp_protezione.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_det_procedure_lavoro_det_1,"cod_parte_corpo",sqlca,&
                 "tab_parti_corpo","cod_parte_corpo","des_parte_corpo",&
                 "(tab_parti_corpo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_parti_corpo.flag_blocco <> 'S') or (tab_parti_corpo.flag_blocco = 'S' and tab_parti_corpo.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_det_procedure_lavoro_det_1,"cod_tipo_rischio",sqlca,&
                 "tab_tipi_rischi","cod_tipo_rischio","des_tipo_rischio",&
                 "tab_tipi_rischi.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
f_PO_LoadDDDW_DW(dw_det_procedure_lavoro_det_1,"cod_misure_prev",sqlca,&
                 "tab_misure_prevenzioni","cod_misure_prev","des_misure_prev",&
                 "tab_misure_prevenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
f_PO_LoadDDDW_DW(dw_det_procedure_lavoro_det_1,"cod_tipo_impatto",sqlca,&
                 "tab_tipi_impatti","cod_tipo_impatto","des_tipo_impatto",&
                 "tab_tipi_impatti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					  

end event

event open;call super::open;string ls_flag_amb_sic

ls_flag_amb_sic = dw_det_procedure_lavoro_lista.i_parentdw.getitemstring(dw_det_procedure_lavoro_lista.i_parentdw.i_selectedrows[1], "flag_ambiente_sic")

if ls_flag_amb_sic = 'N' then
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione_t.Text = "Dispositivi Prot.:"
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo_t.Text = "Parte Corpo:"
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Visible = 0	
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.TabSequence = 0	
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Background.Color = RGB(188, 188, 188)
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Background.Color = RGB(188, 188, 188)	
	dw_det_procedure_lavoro_det_1.Object.cf_cod_tipo_imp.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cf_cod_parte_corpo.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cf_cod_disp_protezione.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cf_cod_misure_prev.Visible = 0	
else
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione_t.Text = "Misure Prev.:"
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo_t.Text = "Tipo Impatto:"
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Visible = 1	
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Background.Color = RGB(188, 188, 188)
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Background.Color = RGB(188, 188, 188)		
	dw_det_procedure_lavoro_det_1.Object.cf_cod_tipo_imp.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cf_cod_parte_corpo.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cf_cod_disp_protezione.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cf_cod_misure_prev.Visible = 1		
end if

end event

type dw_det_procedure_lavoro_lista from uo_cs_xx_dw within w_det_procedure_lavoro
int X=23
int Y=21
int Width=1715
int Height=501
int TabOrder=20
string DataObject="d_det_procedure_lavoro_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_procedura
long   ll_num_edizione, ll_num_revisione,l_Idx


ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_revisione")
ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_procedura")

for l_idx = 1 to rowcount()
   if isnull(getItemstring(l_idx, "cod_azienda")) then
      setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getitemnumber(l_idx, "num_edizione")) or getitemnumber(l_idx, "num_edizione") < 1 then
      setitem(l_idx, "num_edizione", ll_num_edizione)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getitemnumber(l_idx, "num_revisione")) or getitemnumber(l_idx, "num_revisione") < 1 then
      setitem(l_idx, "num_revisione", ll_num_revisione)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getItemstring(l_idx, "cod_procedura")) then
      setitem(l_idx, "cod_procedura", ls_cod_procedura)
   end if
next


end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_procedura
long   l_Error, ll_num_edizione, ll_num_revisione

ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_revisione")
ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_procedura")

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ls_cod_procedura, &
						 ll_num_edizione, &
						 ll_num_revisione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event updatestart;call super::updatestart;string ls_cod_procedura
long ll_num_registrazione, ll_num_edizione, ll_num_revisione, ll_minuti, ll_i, ll_rows
double ld_gravita, ld_probabilita, ld_indice_rischio
datetime ldt_tempo_esposizione


ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_procedura")
ll_num_edizione  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_revisione")

if ib_new then
	select max(det_procedure_lavoro.prog_riga_operazione)
   into   :ll_num_registrazione
   from   det_procedure_lavoro
   where  det_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda and
	       det_procedure_lavoro.cod_procedura = :ls_cod_procedura and
			 det_procedure_lavoro.num_edizione = :ll_num_edizione and
			 det_procedure_lavoro.num_revisione = :ll_num_revisione;
	
	select max(det_procedure_lavoro.prog_riga_operazione)
   into   :ll_num_registrazione
   from   det_procedure_lavoro
   where  det_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 10
   else
      ll_num_registrazione = ll_num_registrazione + 10
   end if
   this.setitem (il_new_row,"prog_riga_operazione", ll_num_registrazione)

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
      ll_num_registrazione = 10
      this.setitem(il_new_row, "prog_riga_operazione", ll_num_registrazione)
   end if
   ib_new = false
end if

ll_rows = rowcount()
if ll_rows > 0 then
	for ll_i = 1 to ll_rows
		if this.getitemstatus(ll_i, 0, primary!) = newmodified! or this.getitemstatus(ll_i, 0, primary!) = datamodified!  then
			ld_gravita = this.getitemnumber(ll_i, "gravita_rischio")
			ld_probabilita = this.getitemnumber(ll_i, "probabilita_rischio")
			ldt_tempo_esposizione = this.getitemdatetime(ll_i, "tempo_esposizione")
			ll_minuti = minute(time(ldt_tempo_esposizione)) + ( hour(time(ldt_tempo_esposizione)) * 60 )
			ld_indice_rischio =  ld_gravita * ld_probabilita * ll_minuti
			setitem(ll_i, "indice_rischio", ld_indice_rischio)
		end if
	next
end if

end event

event pcd_new;call super::pcd_new;string ls_flag_amb_sic

ib_new = true
il_new_row = this.getrow()

ls_flag_amb_sic = dw_det_procedure_lavoro_lista.i_parentdw.getitemstring(dw_det_procedure_lavoro_lista.i_parentdw.i_selectedrows[1], "flag_ambiente_sic")

if ls_flag_amb_sic = 'N' then
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.TabSequence = 20
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.TabSequence = 30
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione_t.Text = "Dispositivi Prot.:"
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo_t.Text = "Parte Corpo:"
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Visible = 0	
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.TabSequence = 0	
else
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione_t.Text = "Misure Prev.:"
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo_t.Text = "Tipo Impatto:"
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Visible = 1	
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.TabSequence = 20
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.TabSequence = 30
end if

end event

event updateend;call super::updateend;string ls_cod_procedura
long   ll_num_registrazione, ll_num_edizione, ll_num_revisione, ll_i, ll_rows,&
       ll_minuti, ll_tot_minuti
double ld_gravita, ld_probabilita, ld_indice_rischio
datetime   ldt_tempo_esposizione

ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_revisione")
ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_procedura")

ll_rows = this.rowcount()
ld_indice_rischio = 0
ll_tot_minuti = 0
if ll_rows > 0 then
	for ll_i = 1 to ll_rows
		ld_gravita = this.getitemnumber(ll_i, "gravita_rischio")
		ld_probabilita = this.getitemnumber(ll_i, "probabilita_rischio")
		ldt_tempo_esposizione = this.getitemdatetime(ll_i, "tempo_esposizione")
		ll_minuti = minute(time(ldt_tempo_esposizione)) + ( hour(time(ldt_tempo_esposizione)) * 60 )
		ll_tot_minuti = ll_tot_minuti + ll_minuti
		ld_indice_rischio =  ld_indice_rischio + (ld_gravita * ld_probabilita * ll_minuti)
	next
	
	ld_indice_rischio = ld_indice_rischio / ll_tot_minuti
	
	if ld_indice_rischio > 0 then
		update tes_procedure_lavoro
		set    indice_rischio_tot = :ld_indice_rischio
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_procedura = :ls_cod_procedura and
				 num_edizione = :ll_num_edizione and
				 num_revisione = :ll_num_revisione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("SIC","Errore durante l'aggiornamento dell'indice di rischio. Errore sul DB:" + sqlca.sqlerrtext, Information!)
		end if
	end if
end if			 
end event

event pcd_modify;call super::pcd_modify;string ls_flag_amb_sic

ls_flag_amb_sic = dw_det_procedure_lavoro_lista.i_parentdw.getitemstring(dw_det_procedure_lavoro_lista.i_parentdw.i_selectedrows[1], "flag_ambiente_sic")

if ls_flag_amb_sic = 'N' then
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.TabSequence = 20
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.TabSequence = 30
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione_t.Text = "Dispositivi Prot.:"
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo_t.Text = "Parte Corpo:"
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Visible = 0	
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.TabSequence = 0	
else
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Visible = 0
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.TabSequence = 0
	dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione_t.Text = "Misure Prev.:"
	dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo_t.Text = "Tipo Impatto:"
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Visible = 1
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Visible = 1	
	dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.TabSequence = 20
	dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.TabSequence = 30
end if

end event

event pcd_save;call super::pcd_save;dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Background.Color = RGB(188, 188, 188)
dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Background.Color = RGB(188, 188, 188)	
dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Background.Color = RGB(188, 188, 188)
dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Background.Color = RGB(188, 188, 188)	
end event

event pcd_view;call super::pcd_view;dw_det_procedure_lavoro_det_1.Object.cod_disp_protezione.Background.Color = RGB(188, 188, 188)
dw_det_procedure_lavoro_det_1.Object.cod_parte_corpo.Background.Color = RGB(188, 188, 188)	
dw_det_procedure_lavoro_det_1.Object.cod_misure_prev.Background.Color = RGB(188, 188, 188)
dw_det_procedure_lavoro_det_1.Object.cod_tipo_impatto.Background.Color = RGB(188, 188, 188)	
end event

type cb_1 from commandbutton within w_det_procedure_lavoro
int X=1761
int Y=21
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Prod.Peric."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_det_procedure_lavoro_prod, -1, dw_det_procedure_lavoro_lista)
end event

type cb_documento from commandbutton within w_det_procedure_lavoro
event clicked pbm_bnclicked
int X=1761
int Y=121
int Width=362
int Height=81
int TabOrder=22
string Text="&Note Est."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_det_procedure_lavoro_blob, -1, dw_det_procedure_lavoro_lista)

end event

type dw_det_procedure_lavoro_det_1 from uo_cs_xx_dw within w_det_procedure_lavoro
int X=46
int Y=641
int Width=2012
int Height=721
int TabOrder=50
string DataObject="d_det_procedure_lavoro_det_1"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_folder from u_folder within w_det_procedure_lavoro
int X=23
int Y=541
int Width=2103
int Height=861
int TabOrder=10
end type

type dw_det_procedure_lavoro_det_2 from uo_cs_xx_dw within w_det_procedure_lavoro
int X=69
int Y=641
int Width=1943
int Height=721
int TabOrder=40
string DataObject="d_det_procedure_lavoro_det_2"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

