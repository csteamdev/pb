﻿$PBExportHeader$w_det_procedure_lavoro_blob.srw
$PBExportComments$Finestra Documenti Esterni Dettaglio Procedure Lavoro
forward
global type w_det_procedure_lavoro_blob from w_cs_xx_principale
end type
type dw_det_procedure_lavoro_blob from uo_cs_xx_dw within w_det_procedure_lavoro_blob
end type
type cb_note_esterne from commandbutton within w_det_procedure_lavoro_blob
end type
end forward

global type w_det_procedure_lavoro_blob from w_cs_xx_principale
integer width = 1751
integer height = 744
string title = "Allegati"
dw_det_procedure_lavoro_blob dw_det_procedure_lavoro_blob
cb_note_esterne cb_note_esterne
end type
global w_det_procedure_lavoro_blob w_det_procedure_lavoro_blob

on pc_delete;call w_cs_xx_principale::pc_delete;cb_note_esterne.enabled = false

end on

event pc_setwindow;call super::pc_setwindow;dw_det_procedure_lavoro_blob.set_dw_key("cod_azienda")
dw_det_procedure_lavoro_blob.set_dw_key("cod_procedura")
dw_det_procedure_lavoro_blob.set_dw_key("num_edizione")
dw_det_procedure_lavoro_blob.set_dw_key("num_revisione")
dw_det_procedure_lavoro_blob.set_dw_key("prog_riga_operazione")
dw_det_procedure_lavoro_blob.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)

iuo_dw_main=dw_det_procedure_lavoro_blob
dw_det_procedure_lavoro_blob.ib_proteggi_chiavi = false
cb_note_esterne.enabled = false

end event

on w_det_procedure_lavoro_blob.create
int iCurrent
call super::create
this.dw_det_procedure_lavoro_blob=create dw_det_procedure_lavoro_blob
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_procedure_lavoro_blob
this.Control[iCurrent+2]=this.cb_note_esterne
end on

on w_det_procedure_lavoro_blob.destroy
call super::destroy
destroy(this.dw_det_procedure_lavoro_blob)
destroy(this.cb_note_esterne)
end on

type dw_det_procedure_lavoro_blob from uo_cs_xx_dw within w_det_procedure_lavoro_blob
integer x = 23
integer y = 20
integer width = 1669
integer height = 500
integer taborder = 10
string dataobject = "d_det_procedure_lavoro_blob"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_view;call super::pcd_view;if i_extendmode then
      cb_note_esterne.enabled = true
end if
end event

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_num_edizione, ll_num_revisione, ll_prog_riga
string ls_cod_procedura

ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_procedura")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_revisione")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_operazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

   if isnull(this.getitemstring(ll_i, "cod_procedura")) then
      this.setitem(ll_i, "cod_procedura", ls_cod_procedura)
   end if

   if isnull(this.getitemnumber(ll_i, "num_edizione")) or this.getitemnumber(ll_i, "num_edizione") < 1 then
      this.setitem(ll_i, "num_edizione", ll_num_edizione)
   end if

   if isnull(this.getitemnumber(ll_i, "num_revisione"))  or this.getitemnumber(ll_i, "num_revisione") < 1 then
      this.setitem(ll_i, "num_revisione", ll_num_revisione)
   end if

   if isnull(this.getitemnumber(ll_i, "prog_riga_operazione"))  or this.getitemnumber(ll_i, "prog_riga_operazione") < 1 then
      this.setitem(ll_i, "prog_riga_operazione", ll_prog_riga)
   end if

next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_i, ll_num_edizione, ll_num_revisione, ll_prog_riga, ll_errore
string ls_cod_procedura

ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_procedura")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_revisione")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_operazione")

ll_errore = retrieve(s_cs_xx.cod_azienda,&
                     ls_cod_procedura, &
							ll_num_edizione, &
							ll_num_revisione, &
							ll_prog_riga)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type cb_note_esterne from commandbutton within w_det_procedure_lavoro_blob
integer x = 1326
integer y = 540
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_procedura, ls_cod_blob, ls_db
long ll_num_edizione, ll_num_revisione, ll_prog_riga
integer li_i, li_risposta

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_det_procedure_lavoro_blob.getrow()
ls_cod_procedura = dw_det_procedure_lavoro_blob.getitemstring(li_i, "cod_procedura")
ll_num_edizione  =  dw_det_procedure_lavoro_blob.getitemnumber(li_i, "num_edizione")
ll_num_revisione =  dw_det_procedure_lavoro_blob.getitemnumber(li_i, "num_revisione")
ll_prog_riga =  dw_det_procedure_lavoro_blob.getitemnumber(li_i, "prog_riga_operazione")
ls_cod_blob = dw_det_procedure_lavoro_blob.getitemstring(li_i, "cod_blob")

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)

	selectblob det_procedure_lavoro_blob.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_procedure_lavoro_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_procedura = :ls_cod_procedura and 
	           num_edizione = :ll_num_edizione and 
	           num_revisione = :ll_num_revisione and 
	           prog_riga_operazione = :ll_prog_riga and 
	           cod_blob = :ls_cod_blob
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob det_procedure_lavoro_blob.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_procedure_lavoro_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_procedura = :ls_cod_procedura and 
	           num_edizione = :ll_num_edizione and 
	           num_revisione = :ll_num_revisione and 
	           prog_riga_operazione = :ll_prog_riga and 
	           cod_blob = :ls_cod_blob;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then

		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob det_procedure_lavoro_blob
	   set        note_esterne  = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda   = :s_cs_xx.cod_azienda and
					  cod_procedura = :ls_cod_procedura and 
					  num_edizione  = :ll_num_edizione and 
					  num_revisione = :ll_num_revisione and 
					  prog_riga_operazione = :ll_prog_riga and 
					  cod_blob      = :ls_cod_blob
		using      sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob det_procedure_lavoro_blob
	   set        note_esterne  = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda   = :s_cs_xx.cod_azienda and
					  cod_procedura = :ls_cod_procedura and 
					  num_edizione  = :ll_num_edizione and 
					  num_revisione = :ll_num_revisione and 
					  prog_riga_operazione = :ll_prog_riga and 
					  cod_blob      = :ls_cod_blob;

	end if
   commit;
end if
end event

