﻿$PBExportHeader$w_det_procedure_lavoro_prod.srw
$PBExportComments$Finestra Prodotti Pericolosi Associati al Dettaglio Procedure Lavoro
forward
global type w_det_procedure_lavoro_prod from w_cs_xx_principale
end type
type dw_det_procedure_lavoro_prod_lista from uo_cs_xx_dw within w_det_procedure_lavoro_prod
end type
type dw_det_procedure_lavoro_prod_det from uo_cs_xx_dw within w_det_procedure_lavoro_prod
end type
end forward

global type w_det_procedure_lavoro_prod from w_cs_xx_principale
int Width=2254
int Height=1565
boolean TitleBar=true
string Title="Prodotti Pericolosi"
dw_det_procedure_lavoro_prod_lista dw_det_procedure_lavoro_prod_lista
dw_det_procedure_lavoro_prod_det dw_det_procedure_lavoro_prod_det
end type
global w_det_procedure_lavoro_prod w_det_procedure_lavoro_prod

type variables
boolean ib_new=FALSE
long il_new_row
end variables

event pc_setwindow;call super::pc_setwindow;dw_det_procedure_lavoro_prod_lista.set_dw_key("cod_azienda")
dw_det_procedure_lavoro_prod_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_det_procedure_lavoro_prod_det.set_dw_options(sqlca,dw_det_procedure_lavoro_prod_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_det_procedure_lavoro_prod_lista


end event

on w_det_procedure_lavoro_prod.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_procedure_lavoro_prod_lista=create dw_det_procedure_lavoro_prod_lista
this.dw_det_procedure_lavoro_prod_det=create dw_det_procedure_lavoro_prod_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_procedure_lavoro_prod_lista
this.Control[iCurrent+2]=dw_det_procedure_lavoro_prod_det
end on

on w_det_procedure_lavoro_prod.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_procedure_lavoro_prod_lista)
destroy(this.dw_det_procedure_lavoro_prod_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_procedure_lavoro_prod_det,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "(anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_prodotti.flag_blocco <> 'S') or (anag_prodotti.flag_blocco = 'S' and anag_prodotti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +")) and " + &
					  "(anag_prodotti.cod_prodotto in (select prod_pericolosi.cod_prodotto  " + &
					                                  "from   prod_pericolosi " + &
																 "where  prod_pericolosi.cod_azienda = '" + s_cs_xx.cod_azienda + "' ))" )


end event

type dw_det_procedure_lavoro_prod_lista from uo_cs_xx_dw within w_det_procedure_lavoro_prod
int X=23
int Y=21
int Width=2172
int Height=501
int TabOrder=10
string DataObject="d_det_procedure_lavoro_prod_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_procedura
long   l_Idx, ll_prog_riga, ll_num_edizione, ll_num_revisione


ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_revisione")
ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_procedura")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_operazione")

for l_idx = 1 to rowcount()
   if isnull(getItemstring(l_idx, "cod_procedura")) then
      setitem(l_idx, "cod_procedura", ls_cod_procedura)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getItemstring(l_idx, "cod_azienda")) then
      setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getitemnumber(l_idx, "num_edizione")) or getitemnumber(l_idx, "num_edizione") < 1 then
      setitem(l_idx, "num_edizione", ll_num_edizione)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getitemnumber(l_idx, "num_revisione")) or getitemnumber(l_idx, "num_revisione") < 1 then
      setitem(l_idx, "num_revisione", ll_num_revisione)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getItemnumber(l_idx, "prog_riga_operazione")) or getItemnumber(l_idx, "prog_riga_operazione") < 1 then
      setitem(l_idx, "prog_riga_operazione", ll_prog_riga)
   end if
next


end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_procedura
long   l_Error, ll_prog_riga, ll_num_edizione, ll_num_revisione

ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_edizione")
ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_revisione")
ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_procedura")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_operazione")

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ls_cod_procedura, &
						 ll_num_edizione, &
						 ll_num_revisione, &
						 ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event updatestart;call super::updatestart;if ib_new then
	string ls_cod_procedura
   long ll_num_registrazione, ll_num_edizione, ll_num_revisione, ll_prog_riga

	ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_edizione")
	ll_num_revisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_revisione")
	ls_cod_procedura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_procedura")
	ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_operazione")

   select max(det_procedure_lavoro_prod.progressivo)
   into   :ll_num_registrazione
   from   det_procedure_lavoro_prod
   where  det_procedure_lavoro_prod.cod_azienda = :s_cs_xx.cod_azienda and
	       det_procedure_lavoro_prod.cod_procedura = :ls_cod_procedura and
			 det_procedure_lavoro_prod.num_edizione = :ll_num_edizione and
			 det_procedure_lavoro_prod.num_revisione = :ll_num_revisione and
			 det_procedure_lavoro_prod.prog_riga_operazione = :ll_prog_riga;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 10
   else
      ll_num_registrazione = ll_num_registrazione + 10
   end if
   this.setitem (il_new_row,"progressivo", ll_num_registrazione)

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
      ll_num_registrazione = 10
      this.setitem(il_new_row, "progressivo", ll_num_registrazione)
   end if
   ib_new = false
end if

end event

event pcd_new;call super::pcd_new;ib_new = true
il_new_row = this.getrow()
end event

type dw_det_procedure_lavoro_prod_det from uo_cs_xx_dw within w_det_procedure_lavoro_prod
int X=23
int Y=541
int Width=2172
int Height=901
int TabOrder=20
string DataObject="d_det_procedure_lavoro_prod_det"
BorderStyle BorderStyle=StyleRaised!
end type

