﻿$PBExportHeader$w_frasi_rischio_s.srw
$PBExportComments$Finestra Codifica Frasi Rischio TipoS
forward
global type w_frasi_rischio_s from w_cs_xx_principale
end type
type dw_frasi_rischio_s from uo_cs_xx_dw within w_frasi_rischio_s
end type
end forward

global type w_frasi_rischio_s from w_cs_xx_principale
int Width=2282
int Height=1149
boolean TitleBar=true
string Title="Frasi S"
dw_frasi_rischio_s dw_frasi_rischio_s
end type
global w_frasi_rischio_s w_frasi_rischio_s

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_frasi_rischio_s.set_dw_key("cod_azienda")
dw_frasi_rischio_s.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
iuo_dw_main = dw_frasi_rischio_s
dw_frasi_rischio_s.ib_proteggi_chiavi = false


end event

on w_frasi_rischio_s.create
int iCurrent
call w_cs_xx_principale::create
this.dw_frasi_rischio_s=create dw_frasi_rischio_s
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_frasi_rischio_s
end on

on w_frasi_rischio_s.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_frasi_rischio_s)
end on

type dw_frasi_rischio_s from uo_cs_xx_dw within w_frasi_rischio_s
int X=23
int Y=21
int Width=2195
int Height=1001
int TabOrder=10
string DataObject="d_frasi_rischio_s"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

