﻿$PBExportHeader$w_grafici_indici.srw
$PBExportComments$Finestra Indici di Rischio
forward
global type w_grafici_indici from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_grafici_indici
end type
type dw_graph from uo_cs_graph within w_grafici_indici
end type
type rb_4 from radiobutton within w_grafici_indici
end type
type rb_3 from radiobutton within w_grafici_indici
end type
type rb_2 from radiobutton within w_grafici_indici
end type
type rb_1 from radiobutton within w_grafici_indici
end type
type gb_1 from groupbox within w_grafici_indici
end type
type cb_esporta from commandbutton within w_grafici_indici
end type
end forward

global type w_grafici_indici from w_cs_xx_principale
integer width = 2962
integer height = 1664
string title = "Grafici Indici Rischio"
cb_stampa cb_stampa
dw_graph dw_graph
rb_4 rb_4
rb_3 rb_3
rb_2 rb_2
rb_1 rb_1
gb_1 gb_1
cb_esporta cb_esporta
end type
global w_grafici_indici w_grafici_indici

type variables

end variables

forward prototypes
public subroutine wf_procedure ()
public subroutine wf_attrezzature ()
public subroutine wf_postazioni ()
public subroutine wf_ambienti ()
end prototypes

public subroutine wf_procedure ();string ls_descrizione, ls_sql
long ll_indice, ll_i
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_graf_external"
lds_dati.settransobject(sqlca)

declare cu_procedure dynamic cursor for sqlsa;

ls_sql = "select tes_procedure_lavoro.des_procedura, " + &
         "tes_procedure_lavoro.indice_rischio_tot " + &
			"from tes_procedure_lavoro " + &
			"where tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			      "tes_procedure_lavoro.flag_valido = 'S'"
prepare sqlsa from :ls_sql;

open dynamic cu_procedure;

do while 1=1
   fetch cu_procedure into :ls_descrizione, :ll_indice;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	/*
	ll_i = dw_grafico.insertrow(0)
	dw_grafico.setitem(ll_i, "descrizione_1", ls_descrizione)
	dw_grafico.setitem(ll_i, "descrizione_2", "XX")
	dw_grafico.setitem(ll_i, "valore", ll_indice)
	dw_grafico.setitemstatus(ll_i,0,primary!,notmodified!)
	*/
	ll_i = lds_dati.insertrow(0)
	lds_dati.setitem(ll_i, "descrizione_1", ls_descrizione)
	lds_dati.setitem(ll_i, "descrizione_2", "XX")
	lds_dati.setitem(ll_i, "valore", ll_indice)
	lds_dati.setitemstatus(ll_i,0,primary!,notmodified!)
loop
close cu_procedure;

/*
dw_grafico.object.gr_1.title = 'Indice di Rischio per Procedure'
dw_grafico.object.gr_1.category.Label = 'Procedure'
dw_grafico.object.gr_1.values.Label = 'Indice Rischio'
*/
//costruzione grafico --------------------------------------------------------------------------------
dw_graph.ids_data = lds_dati
dw_graph.ib_datastore = true

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graph.ii_tipo_grafico = 9

dw_graph.is_titolo = "Indice di Rischio per Procedure"
dw_graph.is_label_categoria = "Procedure"
dw_graph.is_label_valori = "Indice Rischio"

dw_graph.is_str[1] = "descrizione_1"
dw_graph.is_str[2] = "descrizione_2"
dw_graph.is_num[1] = "valore"

dw_graph.is_source_categoria_col = "descrizione_1"

dw_graph.is_categoria_col = "str_1"
dw_graph.is_exp_valore = "num_1"
dw_graph.is_serie_col = "str_2"

dw_graph.uof_retrieve( )
//----------------------------------------------------------------------------------------------------------

destroy lds_dati
end subroutine

public subroutine wf_attrezzature ();string ls_descrizione[], ls_sql, ls_attrezzatura, ls_procedura, ls_old_attrezzatura, ls_str
long ll_indice[], ll_i, ll_y, ll_gravita, ll_probabilita, ll_tot_minuti
datetime ldt_tempo
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_graf_external"
lds_dati.settransobject(sqlca)

declare cu_procedure dynamic cursor for sqlsa;

ls_sql = "SELECT tes_procedure_lavoro.cod_attrezzatura, " + &
					 "det_procedure_lavoro.cod_procedura, " + &
					 "det_procedure_lavoro.gravita_rischio, " + &
					 "det_procedure_lavoro.probabilita_rischio, " + &
					 "det_procedure_lavoro.tempo_esposizione " + &
			"FROM   det_procedure_lavoro, " + &
					 "tes_procedure_lavoro " + &
			"WHERE  (tes_procedure_lavoro.cod_azienda   = det_procedure_lavoro.cod_azienda ) and  " + &
					 "(tes_procedure_lavoro.cod_procedura = det_procedure_lavoro.cod_procedura ) and " + & 
					 "(tes_procedure_lavoro.num_edizione  = det_procedure_lavoro.num_edizione ) and " + & 
					 "(tes_procedure_lavoro.num_revisione = det_procedure_lavoro.num_revisione ) and " + & 
					 "((det_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) ) "  + & 
			"ORDER BY tes_procedure_lavoro.cod_attrezzatura ASC, " + &  
					 "det_procedure_lavoro.cod_procedura ASC,  "  + &
					 "det_procedure_lavoro.gravita_rischio ASC, "   + &
					 "det_procedure_lavoro.probabilita_rischio ASC,  "  + &
					 "det_procedure_lavoro.tempo_esposizione ASC "  

ls_old_attrezzatura = " "
ll_i = 0
prepare sqlsa from :ls_sql;
open dynamic cu_procedure;
do while 1=1
   fetch cu_procedure into :ls_attrezzatura, :ls_procedura, :ll_gravita, :ll_probabilita, :ldt_tempo;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
		if ll_i > 0 then ll_indice[ll_i] = ll_indice[ll_i] / ll_tot_minuti
		exit
	end if
	if ls_attrezzatura <> ls_old_attrezzatura then
		if ll_i > 0 then ll_indice[ll_i] = ll_indice[ll_i] / ll_tot_minuti
		ll_i = ll_i + 1
		ll_tot_minuti = 0
		ll_indice[ll_i] = 0
	end if
	ll_tot_minuti = ll_tot_minuti + (minute(time(ldt_tempo)) + (hour(time(ldt_tempo)) * 60))
	ll_indice[ll_i] = ll_indice[ll_i] + ( ll_gravita * ll_probabilita * ( minute(time(ldt_tempo)) + (hour(time(ldt_tempo)) * 60) ) )
	ls_descrizione[ll_i] = ls_attrezzatura
	ls_old_attrezzatura = ls_attrezzatura
loop
close cu_procedure;

if ll_i < 1 then return

//dw_grafico.reset()
//dw_grafico.setredraw(false)
for ll_i = 1 to upperbound(ll_indice)
	select anag_attrezzature.descrizione
	into   :ls_str
	from   anag_attrezzature
	where  anag_attrezzature.cod_azienda      = :s_cs_xx.cod_azienda and
			 anag_attrezzature.cod_attrezzatura = :ls_descrizione[ll_i] ;

	if sqlca.sqlcode = 0 then 	ls_descrizione[ll_i] = ls_str
	/*
	ll_y = dw_grafico.insertrow(0)
	dw_grafico.setitem(ll_y, "descrizione_1", ls_descrizione[ll_i])
	dw_grafico.setitem(ll_y, "descrizione_2", "XX")
	dw_grafico.setitem(ll_i, "valore", ll_indice[ll_i])
	dw_grafico.setitemstatus(ll_i,0,primary!,notmodified!)
	*/
	ll_y = lds_dati.insertrow(0)
	lds_dati.setitem(ll_y, "descrizione_1", ls_descrizione[ll_i])
	lds_dati.setitem(ll_y, "descrizione_2", "XX")
	lds_dati.setitem(ll_i, "valore", ll_indice[ll_i])
	lds_dati.setitemstatus(ll_i,0,primary!,notmodified!)
next

/*
dw_grafico.object.gr_1.title = 'Indice di Rischio per Attrezzature'
dw_grafico.object.gr_1.category.Label = 'Attrezzature'
dw_grafico.object.gr_1.values.Label = 'Indice Rischio'
*/
//costruzione grafico --------------------------------------------------------------------------------
dw_graph.ids_data = lds_dati
dw_graph.ib_datastore = true

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graph.ii_tipo_grafico = 9

dw_graph.is_titolo = "Indice di Rischio per Attrezzature"
dw_graph.is_label_categoria = "Attrezzature"
dw_graph.is_label_valori = "Indice Rischio"

dw_graph.is_str[1] = "descrizione_1"
dw_graph.is_str[2] = "descrizione_2"
dw_graph.is_num[1] = "valore"

dw_graph.is_source_categoria_col = "descrizione_1"

dw_graph.is_categoria_col = "str_1"
dw_graph.is_exp_valore = "num_1"
dw_graph.is_serie_col = "str_2"

dw_graph.uof_retrieve( )
//----------------------------------------------------------------------------------------------------------

//dw_grafico.setredraw(true)
destroy lds_dati
end subroutine

public subroutine wf_postazioni ();string ls_des_post[], ls_des_amb[], ls_sql, ls_ambiente, ls_postazione, ls_procedura, &
       ls_old_post, ls_old_amb, ls_str, ls_str_1
long ll_indice[], ll_i, ll_y, ll_gravita, ll_probabilita, ll_tot_minuti, ll_soglia_rischio
datetime ldt_tempo
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_graf_external"
lds_dati.settransobject(sqlca)

declare cu_procedure dynamic cursor for sqlsa;

ls_sql = "SELECT tes_procedure_lavoro.cod_ambiente, " + &
                "tes_procedure_lavoro.cod_postazione, " + &
					 "det_procedure_lavoro.cod_procedura, " + &
					 "det_procedure_lavoro.gravita_rischio, " + &
					 "det_procedure_lavoro.probabilita_rischio, " + &
					 "det_procedure_lavoro.tempo_esposizione " + &
			"FROM   det_procedure_lavoro, " + &
					 "tes_procedure_lavoro " + &
			"WHERE  (tes_procedure_lavoro.cod_azienda   = det_procedure_lavoro.cod_azienda ) and  " + &
					 "(tes_procedure_lavoro.cod_procedura = det_procedure_lavoro.cod_procedura ) and " + & 
					 "(tes_procedure_lavoro.num_edizione  = det_procedure_lavoro.num_edizione ) and " + & 
					 "(tes_procedure_lavoro.num_revisione = det_procedure_lavoro.num_revisione ) and " + & 
					 "((det_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) ) "  + & 
			"ORDER BY tes_procedure_lavoro.cod_ambiente ASC, " + &  
			       "tes_procedure_lavoro.cod_postazione ASC, " + &  
					 "det_procedure_lavoro.cod_procedura ASC,  "  + &
					 "det_procedure_lavoro.gravita_rischio ASC, "   + &
					 "det_procedure_lavoro.probabilita_rischio ASC,  "  + &
					 "det_procedure_lavoro.tempo_esposizione ASC "  

ls_old_post = " "
ls_old_amb  = " "
ll_i = 0
prepare sqlsa from :ls_sql;
open dynamic cu_procedure;
do while 1=1
   fetch cu_procedure into :ls_ambiente, :ls_postazione, :ls_procedura, :ll_gravita, :ll_probabilita, :ldt_tempo;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
		if ll_i > 0 then ll_indice[ll_i] = ll_indice[ll_i] / ll_tot_minuti
		exit
	end if
	if (ls_ambiente <> ls_old_amb) or (ls_postazione <> ls_old_post) then
		if ll_i > 0 then ll_indice[ll_i] = ll_indice[ll_i] / ll_tot_minuti
		ll_i = ll_i + 1
		ll_tot_minuti = 0
		ll_indice[ll_i] = 0
	end if
	ll_tot_minuti     = ll_tot_minuti + (minute(time(ldt_tempo)) + (hour(time(ldt_tempo)) * 60))
	ll_indice[ll_i]   = ll_indice[ll_i] + ( ll_gravita * ll_probabilita * ( minute(time(ldt_tempo)) + (hour(time(ldt_tempo)) * 60) ) )
	ls_des_amb[ll_i]  = ls_ambiente
	ls_des_post[ll_i] = ls_postazione
	ls_old_amb  = ls_ambiente
	ls_old_post = ls_postazione
loop
close cu_procedure;

if ll_i < 1 then return

//dw_grafico.reset()
//dw_grafico.setredraw(false)
for ll_i = 1 to upperbound(ll_indice)
	ls_str_1 = ls_des_amb[ll_i]
	select tab_ambienti_lavoro.des_ambiente
	into   :ls_str
	from   tab_ambienti_lavoro
	where  tab_ambienti_lavoro.cod_azienda  = :s_cs_xx.cod_azienda and
			 tab_ambienti_lavoro.cod_ambiente = :ls_des_amb[ll_i] ;
	if sqlca.sqlcode = 0 then 	ls_des_amb[ll_i] = ls_str

	select tab_postazioni_lavoro.des_postazione, tab_postazioni_lavoro.liv_soglia_rischio
	into   :ls_str, :ll_soglia_rischio
	from   tab_postazioni_lavoro
	where  tab_postazioni_lavoro.cod_azienda  = :s_cs_xx.cod_azienda and
			 tab_postazioni_lavoro.cod_ambiente = :ls_str_1 and
			 tab_postazioni_lavoro.cod_postazione = :ls_des_post[ll_i] ;
	if sqlca.sqlcode = 0 then 	ls_des_post[ll_i] = ls_str

	if ll_indice[ll_i] > ll_soglia_rischio then   //////  pericoloso
	/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", ll_indice[ll_i])
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "YY")
		dw_grafico.setitem(ll_y, "valore", 0)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", ll_indice[ll_i])
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "YY")
		lds_dati.setitem(ll_y, "valore", 0)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
	else
		/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "YY")
		dw_grafico.setitem(ll_y, "valore", ll_indice[ll_i])
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", 0)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "YY")
		lds_dati.setitem(ll_y, "valore", ll_indice[ll_i])
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i] + " / " + ls_des_post[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", 0)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
	end if
next

/*
dw_grafico.object.gr_1.title = 'Indice di Rischio per Postazioni'
dw_grafico.object.gr_1.category.Label = 'Postazioni'
dw_grafico.object.gr_1.values.Label = 'Indice Rischio'
*/
//costruzione grafico --------------------------------------------------------------------------------
dw_graph.ids_data = lds_dati
dw_graph.ib_datastore = true

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graph.ii_tipo_grafico = 9

dw_graph.is_titolo = "Indice di Rischio per Postazioni"
dw_graph.is_label_categoria = "Postazioni"
dw_graph.is_label_valori = "Indice Rischio"

dw_graph.is_str[1] = "descrizione_1"
dw_graph.is_str[2] = "descrizione_2"
dw_graph.is_num[1] = "valore"

dw_graph.is_source_categoria_col = "descrizione_1"

dw_graph.is_categoria_col = "str_1"
dw_graph.is_exp_valore = "num_1"
dw_graph.is_serie_col = "str_2"

dw_graph.uof_retrieve( )
//----------------------------------------------------------------------------------------------------------

//dw_grafico.setredraw(true)
destroy lds_dati

end subroutine

public subroutine wf_ambienti ();string ls_des_amb[], ls_sql, ls_ambiente, ls_postazione, ls_procedura, &
       ls_old_amb, ls_str, ls_str_1
long ll_indice[], ll_i, ll_y, ll_gravita, ll_probabilita, ll_tot_minuti, ll_soglia_rischio
datetime ldt_tempo
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_graf_external"
lds_dati.settransobject(sqlca)

declare cu_procedure dynamic cursor for sqlsa;

ls_sql = "SELECT tes_procedure_lavoro.cod_ambiente, " + &
                "tes_procedure_lavoro.cod_postazione, " + &
					 "det_procedure_lavoro.cod_procedura, " + &
					 "det_procedure_lavoro.gravita_rischio, " + &
					 "det_procedure_lavoro.probabilita_rischio, " + &
					 "det_procedure_lavoro.tempo_esposizione " + &
			"FROM   det_procedure_lavoro, " + &
					 "tes_procedure_lavoro " + &
			"WHERE  (tes_procedure_lavoro.cod_azienda   = det_procedure_lavoro.cod_azienda ) and  " + &
					 "(tes_procedure_lavoro.cod_procedura = det_procedure_lavoro.cod_procedura ) and " + & 
					 "(tes_procedure_lavoro.num_edizione  = det_procedure_lavoro.num_edizione ) and " + & 
					 "(tes_procedure_lavoro.num_revisione = det_procedure_lavoro.num_revisione ) and " + & 
					 "((det_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) ) "  + & 
			"ORDER BY tes_procedure_lavoro.cod_ambiente ASC, " + &  
			       "tes_procedure_lavoro.cod_postazione ASC, " + &  
					 "det_procedure_lavoro.cod_procedura ASC,  "  + &
					 "det_procedure_lavoro.gravita_rischio ASC, "   + &
					 "det_procedure_lavoro.probabilita_rischio ASC,  "  + &
					 "det_procedure_lavoro.tempo_esposizione ASC "  

ls_old_amb  = " "
ll_i = 0
prepare sqlsa from :ls_sql;
open dynamic cu_procedure;
do while 1=1
   fetch cu_procedure into :ls_ambiente, :ls_postazione, :ls_procedura, :ll_gravita, :ll_probabilita, :ldt_tempo;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
		if ll_i > 0 then ll_indice[ll_i] = ll_indice[ll_i] / ll_tot_minuti
		exit
	end if
	if ls_ambiente <> ls_old_amb then
		if ll_i > 0 then ll_indice[ll_i] = ll_indice[ll_i] / ll_tot_minuti
		ll_i = ll_i + 1
		ll_tot_minuti = 0
		ll_indice[ll_i] = 0
	end if
	ll_tot_minuti     = ll_tot_minuti + (minute(time(ldt_tempo)) + (hour(time(ldt_tempo)) * 60))
	ll_indice[ll_i]   = ll_indice[ll_i] + ( ll_gravita * ll_probabilita * ( minute(time(ldt_tempo)) + (hour(time(ldt_tempo)) * 60) ) )
	ls_des_amb[ll_i]  = ls_ambiente
	ls_old_amb  = ls_ambiente
loop
close cu_procedure;

if ll_i < 1 then return

//dw_grafico.reset()
//dw_grafico.setredraw(false)
for ll_i = 1 to upperbound(ll_indice)
	select tab_ambienti_lavoro.des_ambiente, tab_ambienti_lavoro.liv_soglia_rischio
	into   :ls_str, :ll_soglia_rischio
	from   tab_ambienti_lavoro
	where  tab_ambienti_lavoro.cod_azienda  = :s_cs_xx.cod_azienda and
			 tab_ambienti_lavoro.cod_ambiente = :ls_des_amb[ll_i] ;
	if sqlca.sqlcode = 0 then 	ls_des_amb[ll_i] = ls_str

	if ll_indice[ll_i] > ll_soglia_rischio then
		/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", ll_indice[ll_i])
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "YY")
		dw_grafico.setitem(ll_y, "valore", 0)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", ll_indice[ll_i])
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "YY")
		lds_dati.setitem(ll_y, "valore", 0)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
	else
		/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "YY")
		dw_grafico.setitem(ll_y, "valore", ll_indice[ll_i])
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", 0)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "YY")
		lds_dati.setitem(ll_y, "valore", ll_indice[ll_i])
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_des_amb[ll_i])
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", 0)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
	end if
next

/*
dw_grafico.object.gr_1.title = 'Indice di Rischio per Ambiente'
dw_grafico.object.gr_1.category.Label = 'Ambienti'
dw_grafico.object.gr_1.values.Label = 'Indice Rischio'
*/
//costruzione grafico --------------------------------------------------------------------------------
dw_graph.ids_data = lds_dati
dw_graph.ib_datastore = true

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graph.ii_tipo_grafico = 9

dw_graph.is_titolo = "Indice di Rischio per Ambiente"
dw_graph.is_label_categoria = "Ambienti"
dw_graph.is_label_valori = "Indice Rischio"

dw_graph.is_str[1] = "descrizione_1"
dw_graph.is_str[2] = "descrizione_2"
dw_graph.is_num[1] = "valore"

dw_graph.is_source_categoria_col = "descrizione_1"

dw_graph.is_categoria_col = "str_1"
dw_graph.is_exp_valore = "num_1"
dw_graph.is_serie_col = "str_2"

dw_graph.uof_retrieve( )
//----------------------------------------------------------------------------------------------------------

//dw_grafico.setredraw(true)
destroy lds_dati

end subroutine

event pc_setwindow;call super::pc_setwindow;//dw_grafico.set_dw_options(sqlca, &
//                          pcca.null_object, &
//								  c_noretrieveonopen + &
//								  c_disableCC + &
//								  c_disableccinsert, &
//								  c_default)
//
//iuo_dw_main = dw_grafico
//save_on_close(c_socnosave)
rb_1.checked = true
rb_2.checked = false
rb_3.checked = false
rb_4.checked = false
rb_1.triggerevent("clicked")

end event

on w_grafici_indici.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_graph=create dw_graph
this.rb_4=create rb_4
this.rb_3=create rb_3
this.rb_2=create rb_2
this.rb_1=create rb_1
this.gb_1=create gb_1
this.cb_esporta=create cb_esporta
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_graph
this.Control[iCurrent+3]=this.rb_4
this.Control[iCurrent+4]=this.rb_3
this.Control[iCurrent+5]=this.rb_2
this.Control[iCurrent+6]=this.rb_1
this.Control[iCurrent+7]=this.gb_1
this.Control[iCurrent+8]=this.cb_esporta
end on

on w_grafici_indici.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_graph)
destroy(this.rb_4)
destroy(this.rb_3)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.gb_1)
destroy(this.cb_esporta)
end on

type cb_stampa from commandbutton within w_grafici_indici
integer x = 105
integer y = 576
integer width = 352
integer height = 80
integer taborder = 31
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long job

job = PrintOpen( ) 
PrintDataWindow(job, dw_graph) 
PrintClose(job)
end event

type dw_graph from uo_cs_graph within w_grafici_indici
integer x = 571
integer y = 20
integer width = 2336
integer height = 1528
integer taborder = 20
end type

type rb_4 from radiobutton within w_grafici_indici
integer x = 69
integer y = 360
integer width = 448
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Procedura"
end type

event clicked;
//dw_grafico.reset()
//dw_grafico.setredraw(false)
wf_procedure()
//dw_grafico.setredraw(true)

end event

type rb_3 from radiobutton within w_grafici_indici
integer x = 69
integer y = 280
integer width = 448
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Attrezzatura"
end type

event clicked;wf_attrezzature()


end event

type rb_2 from radiobutton within w_grafici_indici
integer x = 69
integer y = 200
integer width = 448
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Postazione"
end type

event clicked;//dw_grafico.reset()
//dw_grafico.setredraw(false)
wf_postazioni()
//dw_grafico.setredraw(true)

end event

type rb_1 from radiobutton within w_grafici_indici
integer x = 69
integer y = 120
integer width = 448
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Ambiente"
end type

event clicked;
//dw_grafico.reset()
//dw_grafico.setredraw(false)
wf_ambienti()
//dw_grafico.setredraw(true)

end event

type gb_1 from groupbox within w_grafici_indici
integer x = 23
integer y = 20
integer width = 526
integer height = 440
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo Indice"
end type

type cb_esporta from commandbutton within w_grafici_indici
integer x = 101
integer y = 480
integer width = 366
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Excel"
end type

event clicked;string ls_nome, ls_path
integer li_ritorno

ls_path = "C:"
li_ritorno = getfileopenname("Select File", ls_path, ls_nome, "XLS", "Excel (*.XLS),*.XLS,")

if li_ritorno < 1 then
	return
end if

//dw_grafico.saveas(ls_path, excel!, false)
dw_graph.saveas(ls_path, excel!, false)
end event

