﻿$PBExportHeader$w_grafici_rischio.srw
$PBExportComments$Finestra Grafici Rischi per Operai e Postazioni
forward
global type w_grafici_rischio from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_grafici_rischio
end type
type dw_graph from uo_cs_graph within w_grafici_rischio
end type
type rb_2 from radiobutton within w_grafici_rischio
end type
type rb_1 from radiobutton within w_grafici_rischio
end type
type gb_1 from groupbox within w_grafici_rischio
end type
type cb_esporta from commandbutton within w_grafici_rischio
end type
type em_data_inizio from editmask within w_grafici_rischio
end type
type st_1 from statictext within w_grafici_rischio
end type
type em_data_fine from editmask within w_grafici_rischio
end type
type st_2 from statictext within w_grafici_rischio
end type
end forward

global type w_grafici_rischio from w_cs_xx_principale
integer width = 2693
integer height = 1676
string title = "Grafici Indici Rischio"
cb_stampa cb_stampa
dw_graph dw_graph
rb_2 rb_2
rb_1 rb_1
gb_1 gb_1
cb_esporta cb_esporta
em_data_inizio em_data_inizio
st_1 st_1
em_data_fine em_data_fine
st_2 st_2
end type
global w_grafici_rischio w_grafici_rischio

type variables

end variables

forward prototypes
public subroutine wf_postazioni ()
public subroutine wf_operai ()
end prototypes

public subroutine wf_postazioni ();string   ls_sql, ls_ambiente, ls_postazione, ls_data_1, ls_data_2, ls_cod_procedura
long     ll_i, ll_y, ll_tot_rischio, ll_tot_minuti, ll_indice_rischio, ll_edizione, ll_revisione, &
         ll_rischio_procedura
datetime ldt_data_inizio, ldt_data_fine
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_graf_external"
lds_dati.settransobject(sqlca)

declare cu_procedure dynamic cursor for sqlsa;

ldt_data_inizio = datetime(date(em_data_inizio.text))
ldt_data_fine = datetime(date(em_data_fine.text))
ls_data_1 = string(date(ldt_data_inizio),s_cs_xx.db_funzioni.formato_data)
ls_data_2 = string(date(ldt_data_fine),s_cs_xx.db_funzioni.formato_data)
ls_sql = "select registro_operazioni.cod_ambiente, " + &
                "registro_operazioni.cod_postazione, " + &
         		 "sum(registro_operazioni.minuti_lavoro) " + &
		 			 "from registro_operazioni " + &
		          "where registro_operazioni.cod_azienda ='" + s_cs_xx.cod_azienda + "' and " + &
		                "registro_operazioni.data_operazione between '" + ls_data_1 + "' and '" + ls_data_2 + "' " + &
                "group by cod_ambiente, cod_postazione "

ll_i = 0
prepare sqlsa from :ls_sql;
open dynamic cu_procedure;
do while 1=1
   fetch cu_procedure into :ls_ambiente, :ls_postazione, :ll_tot_minuti;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	select indice_rischio_tot,
	       cod_procedura,
			 num_edizione, 
			 num_revisione
	into   :ll_indice_rischio,
	       :ls_cod_procedura,
			 :ll_edizione, 
			 :ll_revisione
	from   tes_procedure_lavoro
	where  tes_procedure_lavoro.cod_azienda    = :s_cs_xx.cod_azienda and
	       tes_procedure_lavoro.cod_ambiente   = :ls_ambiente and
			 tes_procedure_lavoro.cod_postazione = :ls_postazione and
			 tes_procedure_lavoro.flag_valido    = 'S';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SIC","Attenzione: potrebbero esistere più procedure per una postazione")
		destroy lds_dati
		return
	end if
	
   ll_rischio_procedura = 0
	select sum(indice_rischio)
	into   :ll_rischio_procedura
	from   det_procedure_lavoro
	where  det_procedure_lavoro.cod_azienda    = :s_cs_xx.cod_azienda and
	       det_procedure_lavoro.cod_procedura  = :ls_cod_procedura and
			 det_procedure_lavoro.num_edizione   = :ll_edizione and
			 det_procedure_lavoro.num_revisione  = :ll_revisione;
   if isnull(ll_rischio_procedura) or ll_rischio_procedura = 0 then
		ll_rischio_procedura = 0
	end if
	
	ll_tot_rischio = ll_indice_rischio * ll_tot_minuti
	if ll_tot_rischio > ll_rischio_procedura then
		/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", ll_tot_rischio)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		dw_grafico.setitem(ll_y, "descrizione_2", "YY")
		dw_grafico.setitem(ll_y, "valore", 0)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", ll_tot_rischio)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		lds_dati.setitem(ll_y, "descrizione_2", "YY")
		lds_dati.setitem(ll_y, "valore", 0)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
	else
		/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		dw_grafico.setitem(ll_y, "descrizione_2", "YY")
		dw_grafico.setitem(ll_y, "valore", ll_tot_rischio)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", 0)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		lds_dati.setitem(ll_y, "descrizione_2", "YY")
		lds_dati.setitem(ll_y, "valore", ll_tot_rischio)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_ambiente + " / " + ls_postazione)
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", 0)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
	end if

loop
close cu_procedure;
/*
dw_grafico.object.gr_1.title = 'Rischio per Postazioni'
dw_grafico.object.gr_1.category.Label = 'Postazioni'
dw_grafico.object.gr_1.values.Label = 'Rischio'
*/
//costruzione grafico --------------------------------------------------------------------------------
dw_graph.ids_data = lds_dati
dw_graph.ib_datastore = true

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graph.ii_tipo_grafico = 9

dw_graph.is_titolo = "Rischio per Postazioni"
dw_graph.is_label_categoria = "Postazioni"
dw_graph.is_label_valori = "Rischio"

dw_graph.is_str[1] = "descrizione_1"
dw_graph.is_str[2] = "descrizione_2"
dw_graph.is_num[1] = "valore"

dw_graph.is_source_categoria_col = "descrizione_1"

dw_graph.is_categoria_col = "str_1"
dw_graph.is_exp_valore = "num_1"
dw_graph.is_serie_col = "str_2"

dw_graph.uof_retrieve( )
//----------------------------------------------------------------------------------------------------------

//dw_grafico.setredraw(true)
destroy lds_dati

end subroutine

public subroutine wf_operai ();string ls_sql, ls_ambiente, ls_postazione, ls_data_1, ls_data_2, ls_cod_procedura, ls_operaio, &
       ls_nome, ls_cognome, ls_old_operaio
long   ll_i, ll_y, ll_tot_rischio, ll_tot_minuti, ll_indice_rischio, ll_edizione, ll_revisione, &
       ll_rischio_procedura
datetime   ldt_data_inizio, ldt_data_fine
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_graf_external"
lds_dati.settransobject(sqlca)

declare cu_procedure dynamic cursor for sqlsa;

ldt_data_inizio = datetime(date(em_data_inizio.text))
ldt_data_fine = datetime(date(em_data_fine.text))
ls_data_1 = string(date(ldt_data_inizio),s_cs_xx.db_funzioni.formato_data)
ls_data_2 = string(date(ldt_data_fine),s_cs_xx.db_funzioni.formato_data)
ls_sql = "select registro_operazioni.cod_ambiente, " + &
                "registro_operazioni.cod_postazione, " + &
                "registro_operazioni.cod_operaio, " + &
         		 "sum(registro_operazioni.minuti_lavoro) " + &
		 			 "from registro_operazioni " + &
		          "where registro_operazioni.cod_azienda ='" + s_cs_xx.cod_azienda + "' and " + &
		                "registro_operazioni.data_operazione between '" + ls_data_1 + "' and '" + ls_data_2 + "' " + &
                "group by cod_ambiente, cod_postazione, cod_operaio " + &
                "order by cod_operaio, cod_ambiente, cod_postazione"

ll_i = 0
ls_old_operaio = " "
ll_tot_rischio = 0
prepare sqlsa from :ls_sql;
open dynamic cu_procedure;
do while 1=1
   fetch cu_procedure into :ls_ambiente, :ls_postazione, :ls_operaio, :ll_tot_minuti;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
		/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_operaio)
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", ll_tot_rischio)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_operaio)
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", ll_tot_rischio)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
		
	   	ls_old_operaio = ls_operaio
		ll_tot_rischio = 0
		exit
	end if	
	select indice_rischio_tot,
	       cod_procedura,
			 num_edizione, 
			 num_revisione
	into   :ll_indice_rischio,
	       :ls_cod_procedura,
			 :ll_edizione, 
			 :ll_revisione
	from   tes_procedure_lavoro
	where  tes_procedure_lavoro.cod_azienda    = :s_cs_xx.cod_azienda and
	       tes_procedure_lavoro.cod_ambiente   = :ls_ambiente and
			 tes_procedure_lavoro.cod_postazione = :ls_postazione and
			 tes_procedure_lavoro.flag_valido    = 'S';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SIC","Attenzione: potrebbero esistere più procedure per una postazione")
		destroy lds_dati
		return
	end if
	
   ll_rischio_procedura = 0
	select sum(indice_rischio)
	into   :ll_rischio_procedura
	from   det_procedure_lavoro
	where  det_procedure_lavoro.cod_azienda    = :s_cs_xx.cod_azienda and
	       det_procedure_lavoro.cod_procedura  = :ls_cod_procedura and
			 det_procedure_lavoro.num_edizione   = :ll_edizione and
			 det_procedure_lavoro.num_revisione  = :ll_revisione;
   if isnull(ll_rischio_procedura) or ll_rischio_procedura = 0 then
		ll_rischio_procedura = 0
	end if
	
	select nome, cognome
	into   :ls_nome, :ls_cognome
	from   anag_operai
	where  anag_operai.cod_azienda = :s_cs_xx.cod_azienda and
	       anag_operai.cod_operaio = :ls_operaio;
	ls_operaio = ls_cognome + ", " + ls_nome
	if ls_old_operaio = " " then ls_old_operaio = ls_operaio
	
	ll_tot_rischio = ll_tot_rischio + (ll_indice_rischio * ll_tot_minuti)
	if ls_old_operaio <> ls_operaio then
		/*
		ll_y = dw_grafico.insertrow(0)
		dw_grafico.setitem(ll_y, "descrizione_1", ls_old_operaio)
		dw_grafico.setitem(ll_y, "descrizione_2", "XX")
		dw_grafico.setitem(ll_y, "valore", ll_tot_rischio)
		dw_grafico.setitemstatus(ll_y,0,primary!,notmodified!)
		*/
		ll_y = lds_dati.insertrow(0)
		lds_dati.setitem(ll_y, "descrizione_1", ls_old_operaio)
		lds_dati.setitem(ll_y, "descrizione_2", "XX")
		lds_dati.setitem(ll_y, "valore", ll_tot_rischio)
		lds_dati.setitemstatus(ll_y,0,primary!,notmodified!)
	   	ls_old_operaio = ls_operaio
		ll_tot_rischio = 0
	end if

loop
close cu_procedure;
/*
dw_grafico.object.gr_1.title = 'Rischio per Postazione/Operaio'
dw_grafico.object.gr_1.category.Label = 'Postazioni/Operai'
dw_grafico.object.gr_1.values.Label = 'Rischio'
*/
//costruzione grafico --------------------------------------------------------------------------------
dw_graph.ids_data = lds_dati
dw_graph.ib_datastore = true

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graph.ii_tipo_grafico = 9

dw_graph.is_titolo = "Rischio per Postazione/Operaio"
dw_graph.is_label_categoria = "Postazioni/Operai"
dw_graph.is_label_valori = "Rischio"

dw_graph.is_str[1] = "descrizione_1"
dw_graph.is_str[2] = "descrizione_2"
dw_graph.is_num[1] = "valore"

dw_graph.is_source_categoria_col = "descrizione_1"

dw_graph.is_categoria_col = "str_1"
dw_graph.is_exp_valore = "num_1"
dw_graph.is_serie_col = "str_2"

dw_graph.uof_retrieve( )
//----------------------------------------------------------------------------------------------------------

//dw_grafico.setredraw(true)
destroy lds_dati

end subroutine

event pc_setwindow;call super::pc_setwindow;//dw_grafico.set_dw_options(sqlca, &
//                          pcca.null_object, &
//								  c_noretrieveonopen + &
//								  c_disableCC + &
//								  c_disableccinsert, &
//								  c_default)
//
//iuo_dw_main = dw_grafico
//save_on_close(c_socnosave)
rb_1.checked = true
rb_2.checked = false

em_data_inizio.text = "01/01/1900"
em_data_fine.text   = "31/12/2999"

end event

on w_grafici_rischio.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_graph=create dw_graph
this.rb_2=create rb_2
this.rb_1=create rb_1
this.gb_1=create gb_1
this.cb_esporta=create cb_esporta
this.em_data_inizio=create em_data_inizio
this.st_1=create st_1
this.em_data_fine=create em_data_fine
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_graph
this.Control[iCurrent+3]=this.rb_2
this.Control[iCurrent+4]=this.rb_1
this.Control[iCurrent+5]=this.gb_1
this.Control[iCurrent+6]=this.cb_esporta
this.Control[iCurrent+7]=this.em_data_inizio
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.em_data_fine
this.Control[iCurrent+10]=this.st_2
end on

on w_grafici_rischio.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_graph)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.gb_1)
destroy(this.cb_esporta)
destroy(this.em_data_inizio)
destroy(this.st_1)
destroy(this.em_data_fine)
destroy(this.st_2)
end on

type cb_stampa from commandbutton within w_grafici_rischio
integer x = 2263
integer y = 1484
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long job

job = PrintOpen( ) 
PrintDataWindow(job, dw_graph) 
PrintClose(job)
end event

type dw_graph from uo_cs_graph within w_grafici_rischio
integer x = 32
integer y = 32
integer width = 2601
integer height = 1352
integer taborder = 10
end type

type rb_2 from radiobutton within w_grafici_rischio
integer x = 389
integer y = 1460
integer width = 370
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Postazione"
end type

event clicked;//long ll_i
//
//dw_grafico.reset()
//dw_grafico.setredraw(false)
wf_postazioni()
//dw_grafico.setredraw(true)
//dw_grafico.object.gr_1.legend = 0
end event

type rb_1 from radiobutton within w_grafici_rischio
integer x = 46
integer y = 1460
integer width = 329
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Operaio"
end type

event clicked;//long ll_i
//
//dw_grafico.reset()
//dw_grafico.setredraw(false)
wf_operai()
//dw_grafico.setredraw(true)
//dw_grafico.object.gr_1.legend = 4

end event

type gb_1 from groupbox within w_grafici_rischio
integer y = 1400
integer width = 823
integer height = 160
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo Indice"
end type

type cb_esporta from commandbutton within w_grafici_rischio
integer x = 2263
integer y = 1400
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Excel"
end type

event clicked;string ls_nome, ls_path
integer li_ritorno

ls_path = "C:"
li_ritorno = getfileopenname("Select File", ls_path, ls_nome, "XLS", "Excel (*.XLS),*.XLS,")

if li_ritorno < 1 then
	return
end if

//dw_grafico.saveas(ls_path, excel!, false)
dw_graph.saveas(ls_path, excel!, false)
end event

type em_data_inizio from editmask within w_grafici_rischio
integer x = 1120
integer y = 1440
integer width = 434
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
string minmax = "01/01/1900~~31/12/2999"
end type

type st_1 from statictext within w_grafici_rischio
integer x = 846
integer y = 1440
integer width = 251
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Da Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_fine from editmask within w_grafici_rischio
integer x = 1806
integer y = 1440
integer width = 434
integer height = 80
integer taborder = 41
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
string minmax = "01/01/1900~~31/12/2999"
end type

type st_2 from statictext within w_grafici_rischio
integer x = 1577
integer y = 1440
integer width = 206
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "A Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

