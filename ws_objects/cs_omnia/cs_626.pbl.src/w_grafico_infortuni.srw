﻿$PBExportHeader$w_grafico_infortuni.srw
forward
global type w_grafico_infortuni from w_cs_xx_principale
end type
type tab_1 from tab within w_grafico_infortuni
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tab_1 from tab within w_grafico_infortuni
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
type dw_grafico_periodo from datawindow within w_grafico_infortuni
end type
type dw_periodo_infortuni from datawindow within w_grafico_infortuni
end type
type cb_calcola from commandbutton within w_grafico_infortuni
end type
type dw_grafico_periodo_2 from datawindow within w_grafico_infortuni
end type
type dw_grafico_infortuni from datawindow within w_grafico_infortuni
end type
type dw_grafico_infortuni_2 from datawindow within w_grafico_infortuni
end type
end forward

global type w_grafico_infortuni from w_cs_xx_principale
integer width = 4183
integer height = 2172
string title = "Statistiche infortuni"
boolean maxbox = false
tab_1 tab_1
dw_grafico_periodo dw_grafico_periodo
dw_periodo_infortuni dw_periodo_infortuni
cb_calcola cb_calcola
dw_grafico_periodo_2 dw_grafico_periodo_2
dw_grafico_infortuni dw_grafico_infortuni
dw_grafico_infortuni_2 dw_grafico_infortuni_2
end type
global w_grafico_infortuni w_grafico_infortuni

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string ls_sql

dec{4} ld_mid_giorni_m, ld_mid_giorni_o

long   ll_riga, ll_anno, ll_tot_infortuni, ll_tot_giorni_m, ll_tot_giorni_o


ls_sql = &
"select anno_registrazione, count(*), sum(giorni_malattia), sum(giorni_ospedale) " + &
"from   registro_infortuni " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
"group by anno_registrazione " + &
"order by anno_registrazione "

declare infortuni dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic infortuni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SAFE","Errore in open cursore infortuni: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while true
	
	fetch infortuni
	into  :ll_anno,
			:ll_tot_infortuni,
			:ll_tot_giorni_m,
			:ll_tot_giorni_o;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SAFE","Errore in fetch cursore infortuni: " + sqlca.sqlerrtext,stopsign!)
		close infortuni;
		return -1
	elseif sqlca.sqlcode = 100 then
		close infortuni;
		exit
	end if
	
	if isnull(ll_tot_giorni_m) then
		ll_tot_giorni_m = 0
	end if
	
	if isnull(ll_tot_giorni_o) then
		ll_tot_giorni_o = 0
	end if
	
	if ll_tot_infortuni > 0 then
		ld_mid_giorni_m = round(ll_tot_giorni_m / ll_tot_infortuni,2)
		ld_mid_giorni_o = round(ll_tot_giorni_o / ll_tot_infortuni,2)
	else
		ld_mid_giorni_m = 0
		ld_mid_giorni_o = 0
	end if
	
	ll_riga = dw_grafico_infortuni.insertrow(0)
	
	dw_grafico_infortuni.setitem(ll_riga,"anno",ll_anno)
	
	dw_grafico_infortuni.setitem(ll_riga,"tipo_dato","1. Totale Infortuni")
	
	dw_grafico_infortuni.setitem(ll_riga,"valore",ll_tot_infortuni)
	
	ll_riga = dw_grafico_infortuni_2.insertrow(0)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"anno",ll_anno)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"tipo_dato","2. Totale Giorni Malattia")
	
	dw_grafico_infortuni_2.setitem(ll_riga,"valore",ll_tot_giorni_m)
	
	ll_riga = dw_grafico_infortuni_2.insertrow(0)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"anno",ll_anno)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"tipo_dato","3. Media Giorni Malattia")
	
	dw_grafico_infortuni_2.setitem(ll_riga,"valore",ld_mid_giorni_m)
	
	ll_riga = dw_grafico_infortuni_2.insertrow(0)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"anno",ll_anno)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"tipo_dato","4. Totale Giorni Ospedale")
	
	dw_grafico_infortuni_2.setitem(ll_riga,"valore",ll_tot_giorni_o)
	
	ll_riga = dw_grafico_infortuni_2.insertrow(0)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"anno",ll_anno)
	
	dw_grafico_infortuni_2.setitem(ll_riga,"tipo_dato","5. Media Giorni Ospedale")
	
	dw_grafico_infortuni_2.setitem(ll_riga,"valore",ld_mid_giorni_o)
	
loop

return 0
end function

on w_grafico_infortuni.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.dw_grafico_periodo=create dw_grafico_periodo
this.dw_periodo_infortuni=create dw_periodo_infortuni
this.cb_calcola=create cb_calcola
this.dw_grafico_periodo_2=create dw_grafico_periodo_2
this.dw_grafico_infortuni=create dw_grafico_infortuni
this.dw_grafico_infortuni_2=create dw_grafico_infortuni_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.dw_grafico_periodo
this.Control[iCurrent+3]=this.dw_periodo_infortuni
this.Control[iCurrent+4]=this.cb_calcola
this.Control[iCurrent+5]=this.dw_grafico_periodo_2
this.Control[iCurrent+6]=this.dw_grafico_infortuni
this.Control[iCurrent+7]=this.dw_grafico_infortuni_2
end on

on w_grafico_infortuni.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.dw_grafico_periodo)
destroy(this.dw_periodo_infortuni)
destroy(this.cb_calcola)
destroy(this.dw_grafico_periodo_2)
destroy(this.dw_grafico_infortuni)
destroy(this.dw_grafico_infortuni_2)
end on

event pc_setwindow;call super::pc_setwindow;dw_periodo_infortuni.insertrow(0)

setpointer(hourglass!)

dw_grafico_infortuni.setredraw(false)

dw_grafico_infortuni_2.setredraw(false)

wf_report()

dw_grafico_infortuni.setredraw(true)

dw_grafico_infortuni_2.setredraw(true)

setpointer(arrow!)

tab_1.selecttab(1)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_periodo_infortuni,"cod_procedura",sqlca,"tes_procedure_lavoro","cod_procedura","des_procedura",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_valido = 'S'")
end event

type tab_1 from tab within w_grafico_infortuni
integer x = 23
integer y = 20
integer width = 4091
integer height = 2020
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

event selectionchanged;choose case newindex
		
	case 1
		
		dw_grafico_periodo.show()
		dw_grafico_periodo_2.show()
		dw_periodo_infortuni.show()
		cb_calcola.show()
		
		dw_grafico_infortuni.hide()
		dw_grafico_infortuni_2.hide()
		
	case 2
		
		dw_grafico_periodo.hide()
		dw_grafico_periodo_2.hide()
		dw_periodo_infortuni.hide()
		cb_calcola.hide()
		
		dw_grafico_infortuni.show()
		dw_grafico_infortuni_2.show()
		
end choose
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4055
integer height = 1896
long backcolor = 12632256
string text = "Dati infortuni su periodo specifico"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4055
integer height = 1896
long backcolor = 12632256
string text = "Dati infortuni per anno"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type dw_grafico_periodo from datawindow within w_grafico_infortuni
integer x = 69
integer y = 600
integer width = 1097
integer height = 1400
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_grafico_infortuni"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_periodo_infortuni from datawindow within w_grafico_infortuni
integer x = 69
integer y = 140
integer width = 4000
integer height = 420
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_periodo_infortuni"
boolean border = false
boolean livescroll = true
end type

event itemfocuschanged;selecttext(1,len(gettext()))
end event

type cb_calcola from commandbutton within w_grafico_infortuni
integer x = 3657
integer y = 180
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;datetime ldt_da, ldt_a

string 	ls_cod_procedura, ls_sql

dec{4}	ld_mid_malattia, ld_mid_ospedale

long		ll_tot_infortuni, ll_tot_malattia, ll_tot_ospedale, ll_riga


dw_periodo_infortuni.accepttext()

ldt_da = dw_periodo_infortuni.getitemdatetime(1,"data_da")

ldt_a = dw_periodo_infortuni.getitemdatetime(1,"data_a")

ls_cod_procedura = dw_periodo_infortuni.getitemstring(1,"cod_procedura")

dw_periodo_infortuni.setitem(1,"infortuni",0)

dw_periodo_infortuni.setitem(1,"malattia",0)

dw_periodo_infortuni.setitem(1,"media_malattia",0)

dw_periodo_infortuni.setitem(1,"ospedale",0)

dw_periodo_infortuni.setitem(1,"media_ospedale",0)

ls_sql = &
"select count(*), sum(giorni_malattia), sum(giorni_ospedale) " + &
"from   registro_infortuni " + &
"where	 cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_procedura) then
	ls_sql += "and cod_procedura = '" + ls_cod_procedura + "' "
end if

if not isnull(ldt_da) then
	ls_sql += "and data_registrazione >= '" + string(ldt_da,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_a) then
	ls_sql += "and data_registrazione <= '" + string(ldt_a,s_cs_xx.db_funzioni.formato_data) + "' "
end if
		 
declare infortuni dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic infortuni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SAFE","Errore in open cursore infortuni: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

fetch infortuni
into	:ll_tot_infortuni,
		:ll_tot_malattia,
		:ll_tot_ospedale;
		
if sqlca.sqlcode < 0 then
	g_mb.messagebox("SAFE","Errore in fetch cursore infortuni: " + sqlca.sqlerrtext,stopsign!)
	close infortuni;
	return -1
end if

close infortuni;

if ll_tot_infortuni > 0 then
	ld_mid_malattia = round(ll_tot_malattia / ll_tot_infortuni , 2)
else
	ld_mid_malattia = 0
end if

if ll_tot_infortuni > 0 then
	ld_mid_ospedale = round(ll_tot_ospedale / ll_tot_infortuni , 2)
else
	ld_mid_ospedale = 0
end if

dw_periodo_infortuni.setitem(1,"infortuni",ll_tot_infortuni)

dw_periodo_infortuni.setitem(1,"malattia",ll_tot_malattia)

dw_periodo_infortuni.setitem(1,"media_malattia",ld_mid_malattia)

dw_periodo_infortuni.setitem(1,"ospedale",ll_tot_ospedale)

dw_periodo_infortuni.setitem(1,"media_ospedale",ld_mid_ospedale)

dw_grafico_periodo.reset()

dw_grafico_periodo_2.reset()

dw_grafico_periodo.setredraw(false)

dw_grafico_periodo_2.setredraw(false)

ll_riga = dw_grafico_periodo.insertrow(0)
	
dw_grafico_periodo.setitem(ll_riga,"anno",0)

dw_grafico_periodo.setitem(ll_riga,"tipo_dato","1. Totale Infortuni")

dw_grafico_periodo.setitem(ll_riga,"valore",ll_tot_infortuni)

ll_riga = dw_grafico_periodo_2.insertrow(0)
	
dw_grafico_periodo_2.setitem(ll_riga,"anno",0)

dw_grafico_periodo_2.setitem(ll_riga,"tipo_dato","2. Giorni Malattia")

dw_grafico_periodo_2.setitem(ll_riga,"valore",ll_tot_malattia)

ll_riga = dw_grafico_periodo_2.insertrow(0)
	
dw_grafico_periodo_2.setitem(ll_riga,"anno",0)

dw_grafico_periodo_2.setitem(ll_riga,"tipo_dato","3. Media Malattia")

dw_grafico_periodo_2.setitem(ll_riga,"valore",ld_mid_malattia)

ll_riga = dw_grafico_periodo_2.insertrow(0)
	
dw_grafico_periodo_2.setitem(ll_riga,"anno",0)

dw_grafico_periodo_2.setitem(ll_riga,"tipo_dato","4. Giorni Ospedale")

dw_grafico_periodo_2.setitem(ll_riga,"valore",ll_tot_ospedale)

ll_riga = dw_grafico_periodo_2.insertrow(0)
	
dw_grafico_periodo_2.setitem(ll_riga,"anno",0)

dw_grafico_periodo_2.setitem(ll_riga,"tipo_dato","5. Media Ospedale")

dw_grafico_periodo_2.setitem(ll_riga,"valore",ld_mid_ospedale)

dw_grafico_periodo.setredraw(true)

dw_grafico_periodo_2.setredraw(true)
end event

type dw_grafico_periodo_2 from datawindow within w_grafico_infortuni
integer x = 1189
integer y = 600
integer width = 2880
integer height = 1400
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_grafico_infortuni"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_grafico_infortuni from datawindow within w_grafico_infortuni
integer x = 69
integer y = 160
integer width = 1143
integer height = 1840
integer taborder = 10
string dataobject = "d_grafico_infortuni"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_grafico_infortuni_2 from datawindow within w_grafico_infortuni
integer x = 1234
integer y = 160
integer width = 2834
integer height = 1840
integer taborder = 30
string dataobject = "d_grafico_infortuni"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

