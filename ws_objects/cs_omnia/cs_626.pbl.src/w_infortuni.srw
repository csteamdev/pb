﻿$PBExportHeader$w_infortuni.srw
$PBExportComments$Finestra Gestione Tabella Infortuni
forward
global type w_infortuni from w_cs_xx_principale
end type
type dw_infortuni_lista from uo_cs_xx_dw within w_infortuni
end type
type dw_infortuni_det from uo_cs_xx_dw within w_infortuni
end type
end forward

global type w_infortuni from w_cs_xx_principale
int Width=2533
int Height=1545
boolean TitleBar=true
string Title="Infortuni"
dw_infortuni_lista dw_infortuni_lista
dw_infortuni_det dw_infortuni_det
end type
global w_infortuni w_infortuni

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_infortuni_lista.set_dw_key("cod_azienda")
dw_infortuni_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_infortuni_det.set_dw_options(sqlca,dw_infortuni_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_infortuni_lista


end event

on w_infortuni.create
int iCurrent
call w_cs_xx_principale::create
this.dw_infortuni_lista=create dw_infortuni_lista
this.dw_infortuni_det=create dw_infortuni_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_infortuni_lista
this.Control[iCurrent+2]=dw_infortuni_det
end on

on w_infortuni.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_infortuni_lista)
destroy(this.dw_infortuni_det)
end on

type dw_infortuni_lista from uo_cs_xx_dw within w_infortuni
int X=23
int Y=21
int Width=2446
int Height=501
int TabOrder=10
string DataObject="d_infortuni_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_infortuni_det from uo_cs_xx_dw within w_infortuni
int X=23
int Y=541
int Width=2446
int Height=881
int TabOrder=20
string DataObject="d_infortuni_det"
BorderStyle BorderStyle=StyleRaised!
end type

