﻿$PBExportHeader$w_parti_corpo.srw
$PBExportComments$Finestra Tabella Parti del Corpo
forward
global type w_parti_corpo from w_cs_xx_principale
end type
type dw_parti_corpo from uo_cs_xx_dw within w_parti_corpo
end type
end forward

global type w_parti_corpo from w_cs_xx_principale
integer width = 2327
integer height = 1144
string title = "Parti Corpo"
dw_parti_corpo dw_parti_corpo
end type
global w_parti_corpo w_parti_corpo

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_parti_corpo.set_dw_key("cod_azienda")
dw_parti_corpo.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
iuo_dw_main = dw_parti_corpo
dw_parti_corpo.ib_proteggi_chiavi = false


// prova prova prova

end event

on w_parti_corpo.create
int iCurrent
call super::create
this.dw_parti_corpo=create dw_parti_corpo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parti_corpo
end on

on w_parti_corpo.destroy
call super::destroy
destroy(this.dw_parti_corpo)
end on

type dw_parti_corpo from uo_cs_xx_dw within w_parti_corpo
integer x = 23
integer y = 20
integer width = 2240
integer height = 1000
integer taborder = 10
string dataobject = "d_parti_corpo"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

