﻿$PBExportHeader$w_porte.srw
$PBExportComments$Finestra Gestione Tabella Porte
forward
global type w_porte from w_cs_xx_principale
end type
type dw_porte_lista from uo_cs_xx_dw within w_porte
end type
type dw_porte_det from uo_cs_xx_dw within w_porte
end type
end forward

global type w_porte from w_cs_xx_principale
int Width=2044
int Height=1249
boolean TitleBar=true
string Title="Porte"
dw_porte_lista dw_porte_lista
dw_porte_det dw_porte_det
end type
global w_porte w_porte

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_porte_lista.set_dw_key("cod_azienda")
dw_porte_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_porte_det.set_dw_options(sqlca,dw_porte_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_porte_lista


end event

on w_porte.create
int iCurrent
call w_cs_xx_principale::create
this.dw_porte_lista=create dw_porte_lista
this.dw_porte_det=create dw_porte_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_porte_lista
this.Control[iCurrent+2]=dw_porte_det
end on

on w_porte.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_porte_lista)
destroy(this.dw_porte_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_porte_det,"cod_ambiente",sqlca,&
                 "tab_ambienti_lavoro","cod_ambiente","des_ambiente",&
                 "(tab_ambienti_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_ambienti_lavoro.flag_blocco <> 'S') or (tab_ambienti_lavoro.flag_blocco = 'S' and tab_ambienti_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_porte_det,"verso_ambiente",sqlca,&
                 "tab_ambienti_lavoro","cod_ambiente","des_ambiente",&
                 "(tab_ambienti_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_ambienti_lavoro.flag_blocco <> 'S') or (tab_ambienti_lavoro.flag_blocco = 'S' and tab_ambienti_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")


end event

type dw_porte_lista from uo_cs_xx_dw within w_porte
int X=23
int Y=21
int Width=1966
int Height=501
int TabOrder=10
string DataObject="d_porte_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_porte_det from uo_cs_xx_dw within w_porte
int X=23
int Y=541
int Width=1966
int Height=581
int TabOrder=20
string DataObject="d_porte_det"
BorderStyle BorderStyle=StyleRaised!
end type

