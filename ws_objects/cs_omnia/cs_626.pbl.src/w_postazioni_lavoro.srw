﻿$PBExportHeader$w_postazioni_lavoro.srw
$PBExportComments$Finestra Tabella Postazioni Lavoro
forward
global type w_postazioni_lavoro from w_cs_xx_principale
end type
type dw_postazioni_lavoro_lista from uo_cs_xx_dw within w_postazioni_lavoro
end type
type dw_postazioni_lavoro_det from uo_cs_xx_dw within w_postazioni_lavoro
end type
end forward

global type w_postazioni_lavoro from w_cs_xx_principale
int Width=2145
int Height=1129
boolean TitleBar=true
string Title="Postazioni Lavoro"
dw_postazioni_lavoro_lista dw_postazioni_lavoro_lista
dw_postazioni_lavoro_det dw_postazioni_lavoro_det
end type
global w_postazioni_lavoro w_postazioni_lavoro

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_postazioni_lavoro_lista.set_dw_key("cod_azienda")
dw_postazioni_lavoro_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_postazioni_lavoro_det.set_dw_options(sqlca,dw_postazioni_lavoro_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_postazioni_lavoro_lista


end event

on w_postazioni_lavoro.create
int iCurrent
call w_cs_xx_principale::create
this.dw_postazioni_lavoro_lista=create dw_postazioni_lavoro_lista
this.dw_postazioni_lavoro_det=create dw_postazioni_lavoro_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_postazioni_lavoro_lista
this.Control[iCurrent+2]=dw_postazioni_lavoro_det
end on

on w_postazioni_lavoro.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_postazioni_lavoro_lista)
destroy(this.dw_postazioni_lavoro_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_postazioni_lavoro_det,"cod_ambiente",sqlca,&
                 "tab_ambienti_lavoro","cod_ambiente","des_ambiente",&
                 "(tab_ambienti_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_ambienti_lavoro.flag_blocco <> 'S') or (tab_ambienti_lavoro.flag_blocco = 'S' and tab_ambienti_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")


end event

type dw_postazioni_lavoro_lista from uo_cs_xx_dw within w_postazioni_lavoro
int X=23
int Y=21
int Width=2058
int Height=501
int TabOrder=10
string DataObject="d_postazioni_lavoro_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_postazioni_lavoro_det from uo_cs_xx_dw within w_postazioni_lavoro
int X=23
int Y=541
int Width=2058
int Height=461
int TabOrder=20
string DataObject="d_postazioni_lavoro_det"
BorderStyle BorderStyle=StyleRaised!
end type

