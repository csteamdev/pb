﻿$PBExportHeader$w_prod_pericolosi.srw
$PBExportComments$Finestra Gestione Prodotti Pericolosi
forward
global type w_prod_pericolosi from w_cs_xx_principale
end type
type dw_prodotti_pericolosi_lista from uo_cs_xx_dw within w_prod_pericolosi
end type
type cb_corsi from commandbutton within w_prod_pericolosi
end type
type cb_frasi_r from commandbutton within w_prod_pericolosi
end type
type cb_frasi_s from commandbutton within w_prod_pericolosi
end type
type cb_note from commandbutton within w_prod_pericolosi
end type
type cb_documenti from commandbutton within w_prod_pericolosi
end type
type dw_prodotti_pericolosi_det from uo_cs_xx_dw within w_prod_pericolosi
end type
end forward

global type w_prod_pericolosi from w_cs_xx_principale
integer width = 2647
integer height = 2040
string title = "Prodotti Pericolosi"
dw_prodotti_pericolosi_lista dw_prodotti_pericolosi_lista
cb_corsi cb_corsi
cb_frasi_r cb_frasi_r
cb_frasi_s cb_frasi_s
cb_note cb_note
cb_documenti cb_documenti
dw_prodotti_pericolosi_det dw_prodotti_pericolosi_det
end type
global w_prod_pericolosi w_prod_pericolosi

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_prodotti_pericolosi_lista.set_dw_key("cod_azienda")
dw_prodotti_pericolosi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_prodotti_pericolosi_det.set_dw_options(sqlca,dw_prodotti_pericolosi_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_prodotti_pericolosi_lista
dw_prodotti_pericolosi_lista.ib_proteggi_chiavi = false


end event

on w_prod_pericolosi.create
int iCurrent
call super::create
this.dw_prodotti_pericolosi_lista=create dw_prodotti_pericolosi_lista
this.cb_corsi=create cb_corsi
this.cb_frasi_r=create cb_frasi_r
this.cb_frasi_s=create cb_frasi_s
this.cb_note=create cb_note
this.cb_documenti=create cb_documenti
this.dw_prodotti_pericolosi_det=create dw_prodotti_pericolosi_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_pericolosi_lista
this.Control[iCurrent+2]=this.cb_corsi
this.Control[iCurrent+3]=this.cb_frasi_r
this.Control[iCurrent+4]=this.cb_frasi_s
this.Control[iCurrent+5]=this.cb_note
this.Control[iCurrent+6]=this.cb_documenti
this.Control[iCurrent+7]=this.dw_prodotti_pericolosi_det
end on

on w_prod_pericolosi.destroy
call super::destroy
destroy(this.dw_prodotti_pericolosi_lista)
destroy(this.cb_corsi)
destroy(this.cb_frasi_r)
destroy(this.cb_frasi_s)
destroy(this.cb_note)
destroy(this.cb_documenti)
destroy(this.dw_prodotti_pericolosi_det)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_prodotti_pericolosi_det,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "(anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//					  "((anag_prodotti.flag_blocco <> 'S') or (anag_prodotti.flag_blocco = 'S' and anag_prodotti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
//
end event

type dw_prodotti_pericolosi_lista from uo_cs_xx_dw within w_prod_pericolosi
integer x = 23
integer y = 20
integer width = 2103
integer height = 500
integer taborder = 70
string dataobject = "d_prodotti_pericolosi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then

	string ls_cod_misura_mag, ls_cod_prodotto, ls_cod_prodotto_comodo
	double ld_saldo_quan_inizio_anno, ld_prog_quan_entrata, ld_prog_quan_uscita, ld_giacenza
			 
	select cod_prodotto
	  into :ls_cod_prodotto_comodo
	  from prod_pericolosi
	 where cod_azienda = :s_cs_xx.cod_azienda; 
	
	if not isnull(ls_cod_prodotto_comodo) and ls_cod_prodotto_comodo <> "" then
		if dw_prodotti_pericolosi_lista.RowCount( ) > 0 then
			ls_cod_prodotto = dw_prodotti_pericolosi_lista.getitemstring(dw_prodotti_pericolosi_lista.getrow(), "cod_prodotto")
			if isnull(ls_cod_prodotto) then
				ls_cod_prodotto = ls_cod_prodotto_comodo
			end if
		end if
		
		select saldo_quan_inizio_anno,
				 prog_quan_entrata,
				 prog_quan_uscita,
				 cod_misura_mag
		  into :ld_saldo_quan_inizio_anno,
				 :ld_prog_quan_entrata,
				 :ld_prog_quan_uscita,
				 :ls_cod_misura_mag
		  from anag_prodotti
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_prodotto = :ls_cod_prodotto;
			
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Safe", "Errore in lettura dati dalla tabella ANAG_PRODOTTI")
			g_mb.messagebox("Safe", sqlca.sqlerrtext)
			return
		end if
		
		if sqlca.sqlcode = 0 then 
			ld_giacenza = ld_saldo_quan_inizio_anno + ld_prog_quan_entrata - ld_prog_quan_uscita
			dw_prodotti_pericolosi_det.Object.st_um_1.Text = ls_cod_misura_mag
			dw_prodotti_pericolosi_det.Object.st_um_2.Text = ls_cod_misura_mag
			dw_prodotti_pericolosi_det.Object.st_giacenza.Text = string(ld_giacenza)
		end if	
		
	end if

end if	
end event

event pcd_new;call super::pcd_new;dw_prodotti_pericolosi_det.setfocus()
dw_prodotti_pericolosi_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_view;call super::pcd_view;dw_prodotti_pericolosi_det.object.b_ricerca_prodotto.enabled=false
end event

event pcd_modify;call super::pcd_modify;dw_prodotti_pericolosi_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_delete;call super::pcd_delete;dw_prodotti_pericolosi_det.object.b_ricerca_prodotto.enabled=false
end event

type cb_corsi from commandbutton within w_prod_pericolosi
integer x = 2217
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Corsi"
end type

event clicked;window_open_parm(w_prod_pericolosi_corsi, -1, dw_prodotti_pericolosi_lista)
end event

type cb_frasi_r from commandbutton within w_prod_pericolosi
integer x = 2217
integer y = 120
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Frasi R"
end type

event clicked;window_open_parm(w_prod_pericolosi_frasi_r, -1, dw_prodotti_pericolosi_lista)

end event

type cb_frasi_s from commandbutton within w_prod_pericolosi
integer x = 2217
integer y = 220
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Frasi S"
end type

event clicked;window_open_parm(w_prod_pericolosi_frasi_s, -1, dw_prodotti_pericolosi_lista)

end event

type cb_note from commandbutton within w_prod_pericolosi
integer x = 2217
integer y = 320
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Note"
end type

event clicked;window_open_parm(w_prod_pericolosi_note, -1, dw_prodotti_pericolosi_lista)

end event

type cb_documenti from commandbutton within w_prod_pericolosi
event clicked pbm_bnclicked
integer x = 2217
integer y = 420
integer width = 375
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;window_open_parm(w_prodotti_blob, -1, dw_prodotti_pericolosi_lista)

end event

type dw_prodotti_pericolosi_det from uo_cs_xx_dw within w_prod_pericolosi
event pcd_retrieve pbm_custom60
event pcd_setkey pbm_custom68
integer x = 23
integer y = 540
integer width = 2560
integer height = 1376
integer taborder = 10
string dataobject = "d_prodotti_pericolosi_det"
borderstyle borderstyle = styleraised!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_validate;call super::pcd_validate;string ls_cod_azienda, ls_cod_prodotto
	
if isnull(dw_prodotti_pericolosi_det.getitemstring(dw_prodotti_pericolosi_lista.getrow(), "cod_prodotto")) then
	g_mb.messagebox("Safe", "Codice Prodotto obbligatorio")
	pcca.error = c_fatal
	return
end if	

//ls_cod_prodotto = dw_prodotti_pericolosi_det.getitemstring(dw_prodotti_pericolosi_lista.getrow(), "cod_prodotto")
//
//select cod_azienda
//  into :ls_cod_azienda
//  from prod_pericolosi
// where cod_azienda = :s_cs_xx.cod_azienda
//	and cod_prodotto = :ls_cod_prodotto;
//
//if sqlca.sqlcode = 0 then
//	messagebox("Safe", "Il Codice Prodotto inserito è già esistente")
//	pcca.error = c_fatal
//	return
//end if	
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_prodotti_pericolosi_det,"cod_prodotto")
end choose
end event

