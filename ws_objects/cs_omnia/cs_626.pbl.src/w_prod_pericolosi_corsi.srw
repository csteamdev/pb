﻿$PBExportHeader$w_prod_pericolosi_corsi.srw
$PBExportComments$Finestra Aggencio Corsi a Prodotti Pericolosi
forward
global type w_prod_pericolosi_corsi from w_cs_xx_principale
end type
type dw_prod_pericolosi_corsi from uo_cs_xx_dw within w_prod_pericolosi_corsi
end type
end forward

global type w_prod_pericolosi_corsi from w_cs_xx_principale
int Width=1518
int Height=1149
boolean TitleBar=true
string Title="Corsi per Prodotti Pericolosi"
dw_prod_pericolosi_corsi dw_prod_pericolosi_corsi
end type
global w_prod_pericolosi_corsi w_prod_pericolosi_corsi

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_prod_pericolosi_corsi.set_dw_key("cod_azienda")
dw_prod_pericolosi_corsi.set_dw_key("cod_prodotto")
dw_prod_pericolosi_corsi.set_dw_options(sqlca,i_openparm,c_default,c_default)
iuo_dw_main = dw_prod_pericolosi_corsi
dw_prod_pericolosi_corsi.ib_proteggi_chiavi = false


end event

on w_prod_pericolosi_corsi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_prod_pericolosi_corsi=create dw_prod_pericolosi_corsi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_prod_pericolosi_corsi
end on

on w_prod_pericolosi_corsi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_prod_pericolosi_corsi)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_prod_pericolosi_corsi,"cod_corso",sqlca,&
                 "tab_corsi","cod_corso","des_corso",&
                 "tab_corsi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_prod_pericolosi_corsi from uo_cs_xx_dw within w_prod_pericolosi_corsi
int X=23
int Y=21
int Width=1441
int Height=1001
int TabOrder=10
string DataObject="d_prod_pericolosi_corsi"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto
LONG  l_Idx

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto")) THEN
      SetItem(l_Idx, "cod_prodotto", ls_cod_prodotto)
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto
LONG  l_Error

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto")
l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

