﻿$PBExportHeader$w_prod_pericolosi_note.srw
$PBExportComments$Finestra Note su Prodotti Pericolosi
forward
global type w_prod_pericolosi_note from w_cs_xx_principale
end type
type dw_prod_pericolosi_note_lista from uo_cs_xx_dw within w_prod_pericolosi_note
end type
type dw_prod_pericolosi_note_det from uo_cs_xx_dw within w_prod_pericolosi_note
end type
end forward

global type w_prod_pericolosi_note from w_cs_xx_principale
int Width=2231
int Height=1385
boolean TitleBar=true
string Title="Note Prodotti Pericolosi"
dw_prod_pericolosi_note_lista dw_prod_pericolosi_note_lista
dw_prod_pericolosi_note_det dw_prod_pericolosi_note_det
end type
global w_prod_pericolosi_note w_prod_pericolosi_note

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_prod_pericolosi_note_lista.set_dw_key("cod_azienda")
dw_prod_pericolosi_note_lista.set_dw_key("cod_prodotto")
dw_prod_pericolosi_note_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_prod_pericolosi_note_det.set_dw_options(sqlca,dw_prod_pericolosi_note_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_prod_pericolosi_note_lista


end event

on w_prod_pericolosi_note.create
int iCurrent
call w_cs_xx_principale::create
this.dw_prod_pericolosi_note_lista=create dw_prod_pericolosi_note_lista
this.dw_prod_pericolosi_note_det=create dw_prod_pericolosi_note_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_prod_pericolosi_note_lista
this.Control[iCurrent+2]=dw_prod_pericolosi_note_det
end on

on w_prod_pericolosi_note.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_prod_pericolosi_note_lista)
destroy(this.dw_prod_pericolosi_note_det)
end on

type dw_prod_pericolosi_note_lista from uo_cs_xx_dw within w_prod_pericolosi_note
int X=23
int Y=21
int Width=2149
int Height=501
int TabOrder=10
string DataObject="d_prod_pericolosi_note_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto
LONG  l_Idx

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto")) THEN
      SetItem(l_Idx, "cod_prodotto", ls_cod_prodotto)
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto
LONG  l_Error

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto")
l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_prod_pericolosi_note_det from uo_cs_xx_dw within w_prod_pericolosi_note
int X=23
int Y=541
int Width=2149
int Height=721
int TabOrder=20
string DataObject="d_prod_pericolosi_note_det"
BorderStyle BorderStyle=StyleRaised!
end type

