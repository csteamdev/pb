﻿$PBExportHeader$w_reg_consegna_dispositivi.srw
$PBExportComments$Finestra Registro Consegna Dispositivi di Protezione
forward
global type w_reg_consegna_dispositivi from w_cs_xx_principale
end type
type dw_ricerca from u_dw_search within w_reg_consegna_dispositivi
end type
type cb_documento from commandbutton within w_reg_consegna_dispositivi
end type
type dw_reg_consegna_dispositivi_lista from uo_cs_xx_dw within w_reg_consegna_dispositivi
end type
type dw_reg_consegna_dispositivi_det from uo_cs_xx_dw within w_reg_consegna_dispositivi
end type
type dw_folder_search from u_folder within w_reg_consegna_dispositivi
end type
end forward

global type w_reg_consegna_dispositivi from w_cs_xx_principale
integer width = 2368
integer height = 1836
string title = "Reg. Consegna Disp. Protezione"
dw_ricerca dw_ricerca
cb_documento cb_documento
dw_reg_consegna_dispositivi_lista dw_reg_consegna_dispositivi_lista
dw_reg_consegna_dispositivi_det dw_reg_consegna_dispositivi_det
dw_folder_search dw_folder_search
end type
global w_reg_consegna_dispositivi w_reg_consegna_dispositivi

type variables
boolean ib_new=FALSE
long il_new_row

string is_sql_principale
end variables

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


dw_reg_consegna_dispositivi_lista.set_dw_key("cod_azienda")
dw_reg_consegna_dispositivi_lista.set_dw_key("cod_azienda")
dw_reg_consegna_dispositivi_lista.set_dw_key("cod_azienda")
dw_reg_consegna_dispositivi_lista.set_dw_options(sqlca,pcca.null_object,c_default + c_noretrieveonopen,c_default)
dw_reg_consegna_dispositivi_det.set_dw_options(sqlca,dw_reg_consegna_dispositivi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_reg_consegna_dispositivi_lista





lw_oggetti[1] = dw_reg_consegna_dispositivi_lista
dw_folder_search.fu_assigntab(1, "Lista", lw_oggetti[])

lw_oggetti[1] = dw_ricerca
dw_folder_search.fu_assigntab(2, "Ricerca", lw_oggetti[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)


is_sql_principale = dw_reg_consegna_dispositivi_lista.getsqlselect()

end event

on w_reg_consegna_dispositivi.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.cb_documento=create cb_documento
this.dw_reg_consegna_dispositivi_lista=create dw_reg_consegna_dispositivi_lista
this.dw_reg_consegna_dispositivi_det=create dw_reg_consegna_dispositivi_det
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.cb_documento
this.Control[iCurrent+3]=this.dw_reg_consegna_dispositivi_lista
this.Control[iCurrent+4]=this.dw_reg_consegna_dispositivi_det
this.Control[iCurrent+5]=this.dw_folder_search
end on

on w_reg_consegna_dispositivi.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.cb_documento)
destroy(this.dw_reg_consegna_dispositivi_lista)
destroy(this.dw_reg_consegna_dispositivi_det)
destroy(this.dw_folder_search)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_reg_consegna_dispositivi_det,"cod_disp_protezione",sqlca,&
                 "tab_disp_protezione","cod_disp_protezione","des_disp_protezione",&
                 "(tab_disp_protezione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_disp_protezione.flag_blocco <> 'S') or (tab_disp_protezione.flag_blocco = 'S' and tab_disp_protezione.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

//f_PO_LoadDDDW_DW(dw_reg_consegna_dispositivi_det,"cod_operaio",sqlca,&
//                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
//                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_ricerca,"cod_disp_protezione",sqlca,&
                 "tab_disp_protezione","cod_disp_protezione","des_disp_protezione",&
                 "(tab_disp_protezione.cod_azienda = '" + s_cs_xx.cod_azienda + "') ")


end event

event pc_new;call super::pc_new;dw_reg_consegna_dispositivi_det.setitem(dw_reg_consegna_dispositivi_det.getrow(), "data_consegna", datetime(today()))
end event

type dw_ricerca from u_dw_search within w_reg_consegna_dispositivi
integer x = 27
integer y = 132
integer width = 2263
integer height = 428
integer taborder = 40
string dataobject = "d_reg_consegna_dispositivi_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_null


choose case dwo.name
	case "b_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_ricerca, "cod_operaio")
	
	case "b_ricerca"
		dw_reg_consegna_dispositivi_lista.change_dw_current()
		parent.postevent("pc_retrieve")
	
	case "b_annulla"
		setnull(ls_null)
		dw_ricerca.setitem(1, "cod_operaio", ls_null)
		dw_ricerca.setitem(1, "cod_disp_protezione", ls_null)
		
end choose
end event

type cb_documento from commandbutton within w_reg_consegna_dispositivi
integer x = 1897
integer y = 1632
integer width = 411
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_reg_consegna_dispositivi_det.accepttext()

if (isnull(dw_reg_consegna_dispositivi_det.getrow()) or dw_reg_consegna_dispositivi_det.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato alcun infortunio!")
	return
end if	

if isnull(dw_reg_consegna_dispositivi_det.getitemnumber(dw_reg_consegna_dispositivi_det.getrow(), "anno_registrazione")) or &
	dw_reg_consegna_dispositivi_det.getitemnumber(dw_reg_consegna_dispositivi_det.getrow(), "anno_registrazione") < 2 then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato alcun infortunio!")
	return
end if	

s_cs_xx.parametri.parametro_d_10 = dw_reg_consegna_dispositivi_det.getitemnumber(dw_reg_consegna_dispositivi_det.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_d_11 = dw_reg_consegna_dispositivi_det.getitemnumber(dw_reg_consegna_dispositivi_det.getrow(), "num_registrazione")

window_open_parm(w_det_reg_consegna_disp, -1, dw_reg_consegna_dispositivi_det)
end event

type dw_reg_consegna_dispositivi_lista from uo_cs_xx_dw within w_reg_consegna_dispositivi
integer x = 27
integer y = 132
integer width = 2263
integer height = 928
integer taborder = 10
string dataobject = "d_reg_consegna_dispositivi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_operaio, ls_cod_disp_protezione, ls_sql

dw_ricerca.accepttext()

ls_sql += is_sql_principale + " where reg_consegna_dispositivi.cod_azienda='"+s_cs_xx.cod_azienda+"' "

ls_cod_operaio = dw_ricerca.getitemstring(1, "cod_operaio")
if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then ls_sql += " and reg_consegna_dispositivi.cod_operaio='"+ls_cod_operaio+"' "

ls_cod_disp_protezione = dw_ricerca.getitemstring(1, "cod_disp_protezione")
if ls_cod_disp_protezione<>"" and not isnull(ls_cod_disp_protezione) then ls_sql += " and reg_consegna_dispositivi.cod_disp_protezione='"+ls_cod_disp_protezione+"' "

this.setsqlselect(ls_sql)
l_Error = this.Retrieve()

dw_folder_search.fu_selectTab(1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event updatestart;call super::updatestart;if ib_new then
   long ll_anno,ll_num_registrazione

   this.SetItem (il_new_row,"anno_registrazione", f_anno_esercizio() )
   ll_anno = this.getitemnumber(il_new_row, "anno_registrazione" )
   select max(reg_consegna_dispositivi.num_registrazione)
     into :ll_num_registrazione
     from reg_consegna_dispositivi
     where (reg_consegna_dispositivi.cod_azienda = :s_cs_xx.cod_azienda) and
           (reg_consegna_dispositivi.anno_registrazione = :ll_anno);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (il_new_row,"num_registrazione", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      ll_num_registrazione = 10
      this.SetItem(il_new_row, "num_registrazione", ll_num_registrazione)
   end if
   ib_new = false
end if

end event

event pcd_new;call super::pcd_new;ib_new = true
il_new_row = this.getrow()
end event

type dw_reg_consegna_dispositivi_det from uo_cs_xx_dw within w_reg_consegna_dispositivi
integer x = 32
integer y = 1120
integer width = 2277
integer height = 500
integer taborder = 20
string dataobject = "d_reg_consegna_dispositivi_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if

choose case dwo.name
	case "b_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_reg_consegna_dispositivi_det, "cod_operaio")
		
end choose
end event

type dw_folder_search from u_folder within w_reg_consegna_dispositivi
integer x = 18
integer y = 12
integer width = 2299
integer height = 1080
integer taborder = 40
end type

