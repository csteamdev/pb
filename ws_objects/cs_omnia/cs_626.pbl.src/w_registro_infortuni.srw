﻿$PBExportHeader$w_registro_infortuni.srw
$PBExportComments$Finestra Registro degli Infortuni
forward
global type w_registro_infortuni from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_registro_infortuni
end type
type cb_operaio_ricerca from cb_operai_ricerca within w_registro_infortuni
end type
type dw_registro_infortuni_lista from uo_cs_xx_dw within w_registro_infortuni
end type
type dw_registro_infortuni_det from uo_cs_xx_dw within w_registro_infortuni
end type
end forward

global type w_registro_infortuni from w_cs_xx_principale
integer width = 2149
integer height = 1936
string title = "Registro Infortuni"
cb_documento cb_documento
cb_operaio_ricerca cb_operaio_ricerca
dw_registro_infortuni_lista dw_registro_infortuni_lista
dw_registro_infortuni_det dw_registro_infortuni_det
end type
global w_registro_infortuni w_registro_infortuni

type variables
boolean ib_new=FALSE
long il_new_row
end variables

event pc_setwindow;call super::pc_setwindow;dw_registro_infortuni_lista.set_dw_key("cod_azienda")
dw_registro_infortuni_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_registro_infortuni_det.set_dw_options(sqlca,dw_registro_infortuni_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_registro_infortuni_lista

end event

on w_registro_infortuni.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.cb_operaio_ricerca=create cb_operaio_ricerca
this.dw_registro_infortuni_lista=create dw_registro_infortuni_lista
this.dw_registro_infortuni_det=create dw_registro_infortuni_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.cb_operaio_ricerca
this.Control[iCurrent+3]=this.dw_registro_infortuni_lista
this.Control[iCurrent+4]=this.dw_registro_infortuni_det
end on

on w_registro_infortuni.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.cb_operaio_ricerca)
destroy(this.dw_registro_infortuni_lista)
destroy(this.dw_registro_infortuni_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_registro_infortuni_det,"cod_infortunio",sqlca,&
                 "tab_infortuni","cod_infortunio","des_infortunio",&
                 "tab_infortuni.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_registro_infortuni_det,"cod_procedura",sqlca,&
                 "tes_procedure_lavoro","cod_procedura","des_procedura",&
                 "(tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "(tes_procedure_lavoro.flag_valido = 'S')")
					  
					  
f_PO_LoadDDDW_DW(dw_registro_infortuni_det,"cod_parte_corpo",sqlca,&
                 "tab_parti_corpo","cod_parte_corpo","des_parte_corpo",&
                 "tab_parti_corpo.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

type cb_documento from commandbutton within w_registro_infortuni
integer x = 1691
integer y = 1740
integer width = 411
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_registro_infortuni_lista.accepttext()

if (isnull(dw_registro_infortuni_lista.getrow()) or dw_registro_infortuni_lista.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato alcun infortunio!")
	return
end if	

if isnull(dw_registro_infortuni_lista.getitemnumber(dw_registro_infortuni_lista.getrow(), "anno_registrazione")) or &
	dw_registro_infortuni_lista.getitemnumber(dw_registro_infortuni_lista.getrow(), "anno_registrazione") < 2 then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato alcun infortunio!")
	return
end if	

s_cs_xx.parametri.parametro_d_10 = dw_registro_infortuni_lista.getitemnumber(dw_registro_infortuni_lista.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_d_11 = dw_registro_infortuni_lista.getitemnumber(dw_registro_infortuni_lista.getrow(), "num_registrazione")

window_open_parm(w_det_reg_infortuni, -1, dw_registro_infortuni_lista)
end event

type cb_operaio_ricerca from cb_operai_ricerca within w_registro_infortuni
integer x = 1950
integer y = 1312
integer width = 73
integer height = 72
integer taborder = 30
boolean enabled = false
end type

event getfocus;call super::getfocus;dw_registro_infortuni_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_operaio"

end event

type dw_registro_infortuni_lista from uo_cs_xx_dw within w_registro_infortuni
integer x = 23
integer y = 20
integer width = 2080
integer height = 500
integer taborder = 10
string dataobject = "d_registro_infortuni_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event itemchanged;call super::itemchanged;if i_extendmode then
long ll_edizione, ll_revisione
	
	choose case i_colname
		case "cod_procedura"
			if not isnull(i_coltext) then
				select tes_procedure_lavoro.num_edizione,   
					    tes_procedure_lavoro.num_revisione
				into   :ll_edizione,   
					    :ll_revisione  
				from   tes_procedure_lavoro
				where  tes_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda  and
					    tes_procedure_lavoro.cod_procedura = :i_coltext and
					    tes_procedure_lavoro.flag_valido = 'S'   ;
				if sqlca.sqlcode = 0 then
					setitem(row, "num_edizione", ll_edizione)
					setitem(row, "num_revisione", ll_revisione)
				end if
			end if
	end choose
end if


end event

event updatestart;call super::updatestart;if ib_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (il_new_row,"anno_registrazione", f_anno_esercizio() )
   ll_anno = this.getitemnumber(il_new_row, "anno_registrazione" )
   select max(registro_infortuni.num_registrazione)
     into :ll_num_registrazione
     from registro_infortuni
     where (registro_infortuni.cod_azienda = :s_cs_xx.cod_azienda) and
           (registro_infortuni.anno_registrazione = :ll_anno);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (il_new_row,"num_registrazione", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      ll_num_registrazione = 10
      this.SetItem(il_new_row, "num_registrazione", ll_num_registrazione)
   end if
   ib_new = false
end if

end event

event pcd_new;call super::pcd_new;ib_new = true
il_new_row = this.getrow()

cb_operaio_ricerca.enabled = true
cb_documento.enabled = false
end event

event pcd_view;call super::pcd_view;ib_new = false

cb_operaio_ricerca.enabled = false
cb_documento.enabled = true
end event

event pcd_modify;call super::pcd_modify;cb_operaio_ricerca.enabled = true
cb_documento.enabled = false

end event

event pcd_save;call super::pcd_save;cb_operaio_ricerca.enabled = false
cb_documento.enabled = true
end event

type dw_registro_infortuni_det from uo_cs_xx_dw within w_registro_infortuni
integer x = 23
integer y = 540
integer width = 2080
integer height = 1180
integer taborder = 20
string dataobject = "d_registro_infortuni_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
long ll_edizione, ll_revisione, ll_null
	
	choose case i_colname
		case "cod_procedura"
			if not isnull(i_coltext) then
				select tes_procedure_lavoro.num_edizione,   
					    tes_procedure_lavoro.num_revisione
				into   :ll_edizione,   
					    :ll_revisione  
				from   tes_procedure_lavoro
				where  tes_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda  and
					    tes_procedure_lavoro.cod_procedura = :i_coltext and
					    tes_procedure_lavoro.flag_valido = 'S'   ;
				if sqlca.sqlcode = 0 then
					setitem(row, "num_edizione", ll_edizione)
					setitem(row, "num_revisione", ll_revisione)
				end if
			else
				setnull(ll_null)
				setitem(row, "num_edizione", ll_null)
				setitem(row, "num_revisione", ll_null)
			end if
	end choose
end if


end event

