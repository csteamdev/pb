﻿$PBExportHeader$w_registro_operazioni.srw
$PBExportComments$Finestra Registro Operazioni su Postazioni
forward
global type w_registro_operazioni from w_cs_xx_principale
end type
type dw_reg_operazione_lista from uo_cs_xx_dw within w_registro_operazioni
end type
type dw_reg_operazione_det from uo_cs_xx_dw within w_registro_operazioni
end type
end forward

global type w_registro_operazioni from w_cs_xx_principale
integer width = 2007
integer height = 1120
string title = "Registro Operazioni"
dw_reg_operazione_lista dw_reg_operazione_lista
dw_reg_operazione_det dw_reg_operazione_det
end type
global w_registro_operazioni w_registro_operazioni

type variables
boolean ib_new=FALSE
long il_new_row
end variables

event pc_setwindow;call super::pc_setwindow;dw_reg_operazione_lista.set_dw_key("cod_azienda")
dw_reg_operazione_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_reg_operazione_det.set_dw_options(sqlca,dw_reg_operazione_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_reg_operazione_lista


end event

on w_registro_operazioni.create
int iCurrent
call super::create
this.dw_reg_operazione_lista=create dw_reg_operazione_lista
this.dw_reg_operazione_det=create dw_reg_operazione_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_reg_operazione_lista
this.Control[iCurrent+2]=this.dw_reg_operazione_det
end on

on w_registro_operazioni.destroy
call super::destroy
destroy(this.dw_reg_operazione_lista)
destroy(this.dw_reg_operazione_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_reg_operazione_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_reg_operazione_det,"cod_ambiente",sqlca,&
                 "tab_ambienti_lavoro","cod_ambiente","des_ambiente",&
                 "(tab_ambienti_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_ambienti_lavoro.flag_blocco <> 'S') or (tab_ambienti_lavoro.flag_blocco = 'S' and tab_ambienti_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_reg_operazione_det,"cod_postazione",sqlca,&
                 "tab_postazioni_lavoro","cod_postazione","des_postazione",&
                 "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")


end event

type dw_reg_operazione_lista from uo_cs_xx_dw within w_registro_operazioni
integer x = 23
integer y = 20
integer width = 1920
integer height = 500
integer taborder = 10
string dataobject = "d_reg_operazione_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event rowfocuschanged;call super::rowfocuschanged;string ls_ambiente

if currentrow > 0 then
	ls_ambiente = this.getitemstring(currentrow,"cod_ambiente")
	f_PO_LoadDDDW_DW(dw_reg_operazione_det,"cod_postazione",sqlca,&
		  "tab_postazioni_lavoro","cod_postazione","des_postazione",&
		  "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
		  "(tab_postazioni_lavoro.cod_ambiente = '" + ls_ambiente + "') and " + &
		  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

end if

end event

event pcd_new;call super::pcd_new;ib_new = true
il_new_row = this.getrow()
end event

event pcd_view;call super::pcd_view;ib_new = false
end event

event updatestart;call super::updatestart;if ib_new then
	string ls_cod_ambiente, ls_cod_postazione, ls_cod_operaio
	long ll_progressivo
	datetime ldt_data_operazione

	ll_progressivo = 0
	ls_cod_ambiente     = this.getitemstring(il_new_row, "cod_ambiente")
	ls_cod_postazione   = this.getitemstring(il_new_row, "cod_postazione")
	ls_cod_operaio      = this.getitemstring(il_new_row, "cod_operaio")
	ldt_data_operazione = this.getitemdatetime(il_new_row, "data_operazione")
	select max(registro_operazioni.progressivo)
	into   :ll_progressivo
	from   registro_operazioni
	where  registro_operazioni.cod_azienda     = :s_cs_xx.cod_azienda and
			 registro_operazioni.cod_ambiente    = :ls_cod_ambiente and
			 registro_operazioni.cod_postazione  = :ls_cod_postazione and
			 registro_operazioni.cod_operaio     = :ls_cod_operaio and
			 registro_operazioni.data_operazione = :ldt_data_operazione;
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	this.setitem(il_new_row, "progressivo", ll_progressivo)	
end if
end event

type dw_reg_operazione_det from uo_cs_xx_dw within w_registro_operazioni
integer x = 23
integer y = 540
integer width = 1920
integer height = 460
integer taborder = 20
string dataobject = "d_reg_operazione_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "cod_ambiente"
			if not isnull(i_coltext) then
				f_PO_LoadDDDW_DW(dw_reg_operazione_det,"cod_postazione",sqlca,&
                 "tab_postazioni_lavoro","cod_postazione","des_postazione",&
                 "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                 "(tab_postazioni_lavoro.cod_ambiente = '" + i_coltext + "') and " + &
					  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

			end if
	end choose
end if

end event

