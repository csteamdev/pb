﻿$PBExportHeader$w_reparti_postazioni.srw
$PBExportComments$Finestra Reparti Postazioni
forward
global type w_reparti_postazioni from w_cs_xx_principale
end type
type dw_reparti_postazioni_lista from uo_cs_xx_dw within w_reparti_postazioni
end type
type dw_reparti_postazioni_det from uo_cs_xx_dw within w_reparti_postazioni
end type
end forward

global type w_reparti_postazioni from w_cs_xx_principale
int Width=2094
int Height=965
boolean TitleBar=true
string Title="Reparti e Postazioni"
dw_reparti_postazioni_lista dw_reparti_postazioni_lista
dw_reparti_postazioni_det dw_reparti_postazioni_det
end type
global w_reparti_postazioni w_reparti_postazioni

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_reparti_postazioni_lista.set_dw_key("cod_azienda")
dw_reparti_postazioni_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_reparti_postazioni_det.set_dw_options(sqlca,dw_reparti_postazioni_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_reparti_postazioni_lista


end event

on w_reparti_postazioni.create
int iCurrent
call w_cs_xx_principale::create
this.dw_reparti_postazioni_lista=create dw_reparti_postazioni_lista
this.dw_reparti_postazioni_det=create dw_reparti_postazioni_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_reparti_postazioni_lista
this.Control[iCurrent+2]=dw_reparti_postazioni_det
end on

on w_reparti_postazioni.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_reparti_postazioni_lista)
destroy(this.dw_reparti_postazioni_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_reparti_postazioni_det,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_reparti.flag_blocco <> 'S') or (anag_reparti.flag_blocco = 'S' and anag_reparti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_reparti_postazioni_det,"cod_ambiente",sqlca,&
                 "tab_ambienti_lavoro","cod_ambiente","des_ambiente",&
                 "(tab_ambienti_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_ambienti_lavoro.flag_blocco <> 'S') or (tab_ambienti_lavoro.flag_blocco = 'S' and tab_ambienti_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")


end event

type dw_reparti_postazioni_lista from uo_cs_xx_dw within w_reparti_postazioni
int X=23
int Y=21
int Width=2012
int Height=501
int TabOrder=10
string DataObject="d_reparti_postazioni_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_reparti_postazioni_det from uo_cs_xx_dw within w_reparti_postazioni
int X=23
int Y=541
int Width=2012
int Height=301
int TabOrder=20
string DataObject="d_reparti_postazioni_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "cod_ambiente"
			if not isnull(i_coltext) then
				f_PO_LoadDDDW_DW(dw_reparti_postazioni_det,"cod_postazione",sqlca,&
                 "tab_postazioni_lavoro","cod_postazione","des_postazione",&
                 "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                 "(tab_postazioni_lavoro.cod_ambiente = '" + i_coltext + "') and " + &
					  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

			end if
	end choose
end if

end event

