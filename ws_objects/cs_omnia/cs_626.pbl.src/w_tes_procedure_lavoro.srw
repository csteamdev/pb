﻿$PBExportHeader$w_tes_procedure_lavoro.srw
$PBExportComments$Finestra Testata Procedure Lavoro
forward
global type w_tes_procedure_lavoro from w_cs_xx_principale
end type
type dw_tes_procedure_lavoro_lista from uo_cs_xx_dw within w_tes_procedure_lavoro
end type
type cb_dettagli from commandbutton within w_tes_procedure_lavoro
end type
type rb_ultime_edizioni from radiobutton within w_tes_procedure_lavoro
end type
type rb_in_creazione from radiobutton within w_tes_procedure_lavoro
end type
type gb_amb_sic from groupbox within w_tes_procedure_lavoro
end type
type gb_2 from groupbox within w_tes_procedure_lavoro
end type
type rb_approva from radiobutton within w_tes_procedure_lavoro
end type
type rb_nuova_edizione from radiobutton within w_tes_procedure_lavoro
end type
type cb_esegui from commandbutton within w_tes_procedure_lavoro
end type
type gb_1 from groupbox within w_tes_procedure_lavoro
end type
type rb_1 from radiobutton within w_tes_procedure_lavoro
end type
type dw_tes_procedure_lavoro_det_2 from uo_cs_xx_dw within w_tes_procedure_lavoro
end type
type dw_folder from u_folder within w_tes_procedure_lavoro
end type
type dw_tes_procedure_lavoro_det_1 from uo_cs_xx_dw within w_tes_procedure_lavoro
end type
type rb_ambiente from radiobutton within w_tes_procedure_lavoro
end type
type rb_sicurezza from radiobutton within w_tes_procedure_lavoro
end type
end forward

global type w_tes_procedure_lavoro from w_cs_xx_principale
integer width = 2761
integer height = 1984
string title = "Procedure Lavoro"
dw_tes_procedure_lavoro_lista dw_tes_procedure_lavoro_lista
cb_dettagli cb_dettagli
rb_ultime_edizioni rb_ultime_edizioni
rb_in_creazione rb_in_creazione
gb_amb_sic gb_amb_sic
gb_2 gb_2
rb_approva rb_approva
rb_nuova_edizione rb_nuova_edizione
cb_esegui cb_esegui
gb_1 gb_1
rb_1 rb_1
dw_tes_procedure_lavoro_det_2 dw_tes_procedure_lavoro_det_2
dw_folder dw_folder
dw_tes_procedure_lavoro_det_1 dw_tes_procedure_lavoro_det_1
rb_ambiente rb_ambiente
rb_sicurezza rb_sicurezza
end type
global w_tes_procedure_lavoro w_tes_procedure_lavoro

type variables
boolean ib_new=false, ib_new_rec=false
long ll_new_lista
long il_tipo_operazione
string is_flag_valido, is_flag_in_prova
string is_tipo_records
datetime idt_da_data, idt_a_data
string is_flag_amb_sic
end variables

forward prototypes
public function integer wf_nuova_edizione ()
public function integer wf_approva ()
end prototypes

public function integer wf_nuova_edizione ();string   ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
string   ls_null, ls_str, ls_cod_procedura
long     ll_edizione, ll_revisione, ll_riga_corrente, ll_new_revisione, ll_new_edizione, ll_max_edizioni, ll_nuova_edizione
datetime ldt_scadenza_validazione, ldt_oggi, ldt_null

setnull(ls_null)
setnull(ldt_null)
if is_tipo_records <> "E" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Si possono creare nuove edizioni solo da procedure approvate", StopSign!)
   return -1
end if

if isnull(dw_tes_procedure_lavoro_lista.getitemstring(dw_tes_procedure_lavoro_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Approvazione Procedura di Lavoro","Procdura di Lavoro non ancora APPROVATA", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_tes_procedure_lavoro_det_1.getrow()
ls_cod_procedura = dw_tes_procedure_lavoro_det_1.getitemstring(ll_riga_corrente,"cod_procedura")
ll_edizione = dw_tes_procedure_lavoro_det_1.getitemnumber(ll_riga_corrente,"num_edizione")
ll_revisione = dw_tes_procedure_lavoro_det_1.getitemnumber(ll_riga_corrente,"num_revisione")
ll_nuova_edizione = ll_revisione + 1 

commit;
select tes_procedure_lavoro.emesso_da
into   :ls_str
from   tes_procedure_lavoro
where  ( tes_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda ) and
		 ( tes_procedure_lavoro.cod_procedura = :ls_cod_procedura ) and 
		 ( tes_procedure_lavoro.num_edizione = :ll_edizione ) and
		 ( tes_procedure_lavoro.num_revisione = :ll_nuova_edizione )   ;
if sqlca.sqlcode = 0 then
	g_mb.messagebox("Nuova Edizione Procedura","INTERROMPO ELABORAZIONE: è già esistente una revisione di questa lista")
	ROLLBACK;
	return -1
end if

insert into tes_procedure_lavoro  
           (cod_azienda,   
				cod_procedura,   
				num_edizione,   
				num_revisione,   
				des_procedura,   
				cod_attrezzatura,   
				cod_ambiente,   
				cod_postazione,   
				emesso_il,   
				emesso_da,   
				approvato_il,   
				approvato_da,   
				indice_rischio_tot,   
				flag_valido,   
				note,
				flag_in_prova)  
     select tes_procedure_lavoro.cod_azienda,   
            tes_procedure_lavoro.cod_procedura,   
            tes_procedure_lavoro.num_edizione,   
            tes_procedure_lavoro.num_revisione + 1,   
            tes_procedure_lavoro.des_procedura,   
            tes_procedure_lavoro.cod_attrezzatura,   
            tes_procedure_lavoro.cod_ambiente,   
            tes_procedure_lavoro.cod_postazione,   
            tes_procedure_lavoro.emesso_il,   
            tes_procedure_lavoro.emesso_da,   
            tes_procedure_lavoro.approvato_il,   
            tes_procedure_lavoro.approvato_da,   
            tes_procedure_lavoro.indice_rischio_tot,   
            tes_procedure_lavoro.flag_valido,   
            tes_procedure_lavoro.note,
				tes_procedure_lavoro.flag_in_prova
     from   tes_procedure_lavoro
     where (tes_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda) and
           (tes_procedure_lavoro.cod_procedura = :ls_cod_procedura) and
           (tes_procedure_lavoro.num_edizione = :ll_edizione) and
           (tes_procedure_lavoro.num_revisione = :ll_revisione);
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Procedura di Lavoro",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

   select mansionari.cod_resp_divisione  
   into   :ls_cod_resp_divisione  
   from   mansionari  
   where  (mansionari.cod_azienda = :s_cs_xx.cod_azienda) and
          (mansionari.cod_utente = :s_cs_xx.cod_utente)   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

	setnull(ldt_scadenza_validazione)
	ldt_oggi = datetime(today())
	update tes_procedure_lavoro
	set    flag_valido = 'N' ,
			 flag_in_prova = 'S',
			 emesso_da = :ls_cod_resp_divisione,
			 emesso_il = :ldt_oggi,
			 approvato_il = :ldt_null
   where (tes_procedure_lavoro.cod_azienda   = :s_cs_xx.cod_azienda)  and
         (tes_procedure_lavoro.cod_procedura = :ls_cod_procedura) and
         (tes_procedure_lavoro.num_edizione  = :ll_edizione) and
         (tes_procedure_lavoro.num_revisione = :ll_nuova_edizione);
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Procedura di Lavoro",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

	update tes_procedure_lavoro
   set    approvato_da  = null
   where (tes_procedure_lavoro.cod_azienda   = :s_cs_xx.cod_azienda )  and
         (tes_procedure_lavoro.cod_procedura = :ls_cod_procedura ) and
         (tes_procedure_lavoro.num_edizione  = :ll_edizione ) and
         (tes_procedure_lavoro.num_revisione = :ll_nuova_edizione);
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if


	insert into det_procedure_lavoro
				  (cod_azienda,   
					cod_procedura,   
					num_edizione,   
					num_revisione,   
					prog_riga_operazione,   
					des_operazione,   
					cod_paragrafo,   
					cod_disp_protezione,   
					cod_parte_corpo,   
					cod_tipo_rischio,   
					des_valutazione_rischio,   
					gravita_rischio,   
					probabilita_rischio,   
					tempo_esposizione,   
					indice_rischio)  
		  select det_procedure_lavoro.cod_azienda,   
					det_procedure_lavoro.cod_procedura,   
					det_procedure_lavoro.num_edizione,   
					det_procedure_lavoro.num_revisione + 1,   
					det_procedure_lavoro.prog_riga_operazione,   
					det_procedure_lavoro.des_operazione,   
					det_procedure_lavoro.cod_paragrafo,   
					det_procedure_lavoro.cod_disp_protezione,   
					det_procedure_lavoro.cod_parte_corpo,   
					det_procedure_lavoro.cod_tipo_rischio,   
					det_procedure_lavoro.des_valutazione_rischio,   
					det_procedure_lavoro.gravita_rischio,   
					det_procedure_lavoro.probabilita_rischio,   
					det_procedure_lavoro.tempo_esposizione,   
					det_procedure_lavoro.indice_rischio  
		 from		det_procedure_lavoro
		 where	(det_procedure_lavoro.cod_azienda   = :s_cs_xx.cod_azienda) and
					(det_procedure_lavoro.cod_procedura = :ls_cod_procedura) and
					(det_procedure_lavoro.num_edizione  = :ll_edizione) and
					(det_procedure_lavoro.num_revisione = :ll_revisione);

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Nuova Edizione Procedura Lavoro",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
   ROLLBACK;
   return -1
end if


insert into det_procedure_lavoro_prod
           (cod_azienda,   
				cod_procedura,   
				num_edizione,   
				num_revisione,   
				prog_riga_operazione,
				progressivo,
				cod_prodotto,
				note)
     select det_procedure_lavoro_prod.cod_azienda,   
            det_procedure_lavoro_prod.cod_procedura,   
            det_procedure_lavoro_prod.num_edizione,   
            det_procedure_lavoro_prod.num_revisione + 1,   
            det_procedure_lavoro_prod.prog_riga_operazione,   
            det_procedure_lavoro_prod.progressivo,   
            det_procedure_lavoro_prod.cod_prodotto,   
            det_procedure_lavoro_prod.note
     from   det_procedure_lavoro_prod
     where (det_procedure_lavoro_prod.cod_azienda   = :s_cs_xx.cod_azienda) and
           (det_procedure_lavoro_prod.cod_procedura = :ls_cod_procedura) and
           (det_procedure_lavoro_prod.num_edizione  = :ll_edizione) and
           (det_procedure_lavoro_prod.num_revisione = :ll_revisione)   ;

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Nuova Edizione Procedura Lavoro",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
   ROLLBACK;
   return -1
end if

ll_max_edizioni = f_num_max_edi()

if (ll_revisione > ll_max_edizioni ) and (ll_max_edizioni <> 0) then
   ll_new_revisione = 0 
   ll_new_edizione = ll_edizione + 1

	select tes_procedure_lavoro.emesso_da
   into   :ls_str
	from   tes_procedure_lavoro
	where (tes_procedure_lavoro.cod_azienda   = :s_cs_xx.cod_azienda) and  
			(tes_procedure_lavoro.cod_procedura = :ls_cod_procedura) and
			(tes_procedure_lavoro.num_edizione  = :ll_new_edizione) and  
			(tes_procedure_lavoro.num_revisione = :ll_new_revisione)   ;
   if sqlca.sqlcode = 0 then
      g_mb.messagebox("Nuova Edizione Procedura di Lavoro","INTERROMPO ELABORAZIONE: è già esistente una revisione di questa lista")
      ROLLBACK;
      return -1
   end if
	
	
	update tes_procedure_lavoro
   set    num_edizione = :ll_new_edizione,  
          num_revisione = :ll_new_revisione
   where (tes_procedure_lavoro.cod_azienda   = :s_cs_xx.cod_azienda) and
         (tes_procedure_lavoro.cod_procedura = :ls_cod_procedura) and
         (tes_procedure_lavoro.num_edizione  = :ll_edizione) and
         (tes_procedure_lavoro.num_revisione = :ll_revisione);
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

   update det_procedure_lavoro
   set    num_edizione = :ll_new_edizione,  
          num_revisione = :ll_new_revisione
   where (det_procedure_lavoro.cod_azienda   = :s_cs_xx.cod_azienda) and
         (det_procedure_lavoro.cod_procedura = :ls_cod_procedura) and
         (det_procedure_lavoro.num_edizione  = :ll_edizione) and
         (det_procedure_lavoro.num_revisione = :ll_revisione);
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Procedura di Lavoro",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if
end if

COMMIT;
return 0
end function

public function integer wf_approva ();string   ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, &
         ls_flag_autorizzazione, ls_flag_validazione, ls_cod_procedura
long     ll_edizione, ll_revisione, ll_riga_corrente
datetime ldt_oggi
if is_tipo_records <> "C" then
   g_mb.messagebox("Approvazione Lista di Controllo","Si possono approvare solo le liste in fase di CREAZIONE", StopSign!)
   return -1
end if
if not(isnull(dw_tes_procedure_lavoro_lista.getitemstring(dw_tes_procedure_lavoro_lista.getrow(),"approvato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già APPROVATA", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_tes_procedure_lavoro_det_1.getrow()
ls_cod_procedura = dw_tes_procedure_lavoro_det_1.getitemstring(ll_riga_corrente,"cod_procedura")
ll_edizione = dw_tes_procedure_lavoro_det_1.getitemnumber(ll_riga_corrente,"num_edizione")
ll_revisione = dw_tes_procedure_lavoro_det_1.getitemnumber(ll_riga_corrente,"num_revisione")

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale 
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_approvazione <> "S" then
   g_mb.messagebox("Approvazione Lista di Controllo","L'utente non possiede il PRIVILEGIO di APPROVAZIONE: verificare mansionario", StopSign!)
   return -1
end if 

update tes_procedure_lavoro 
set    flag_valido = 'N'
where  ( tes_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_procedure_lavoro.cod_procedura = :ls_cod_procedura );
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione Procedura di Lavoro",sqlca.sqlerrtext)
   return -1
end if

ldt_oggi = datetime(today())

update tes_procedure_lavoro
set    approvato_da  = :ls_cod_resp_divisione,
       approvato_il  = :ldt_oggi,
		 flag_valido   = 'S',
		 flag_in_prova = 'N'
where  ( tes_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda ) and
       ( tes_procedure_lavoro.cod_procedura = :ls_cod_procedura ) and 
       ( tes_procedure_lavoro.num_edizione  = :ll_edizione ) and
       ( tes_procedure_lavoro.num_revisione  = :ll_revisione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

return 0

end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

is_flag_valido = "S"
is_flag_in_prova = "%"
is_flag_amb_sic = "S"


l_objects[1] = dw_tes_procedure_lavoro_det_1
dw_folder.fu_AssignTab(1, "&Dati Procedura", l_Objects[])
l_objects[1] = dw_tes_procedure_lavoro_det_2
dw_folder.fu_AssignTab(2, "&Note", l_Objects[])

dw_folder.fu_FolderCreate(2,4)
dw_folder.fu_SelectTab(1)

dw_tes_procedure_lavoro_lista.set_dw_key("cod_azienda")
dw_tes_procedure_lavoro_lista.set_dw_options(sqlca,pcca.null_object, c_default, c_default)
dw_tes_procedure_lavoro_det_1.set_dw_options(sqlca,dw_tes_procedure_lavoro_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_procedure_lavoro_det_2.set_dw_options(sqlca,dw_tes_procedure_lavoro_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_procedure_lavoro_lista

rb_ultime_edizioni.checked = true
rb_approva.enabled = false


rb_nuova_edizione.enabled = true
cb_esegui.enabled = false
rb_ultime_edizioni.postevent("clicked")

rb_ambiente.checked = true
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"cod_ambiente",sqlca,&
                 "tab_ambienti_lavoro","cod_ambiente","des_ambiente",&
                 "(tab_ambienti_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_ambienti_lavoro.flag_blocco <> 'S') or (tab_ambienti_lavoro.flag_blocco = 'S' and tab_ambienti_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"emesso_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"approvato_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")				
					  
f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"cod_postazione",sqlca,&
					  "tab_postazioni_lavoro","cod_postazione","des_postazione",&
					  "tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"cod_attrezzatura",sqlca,&
					  "anag_attrezzature","cod_attrezzatura","descrizione",&
					  "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

					  
end event

on w_tes_procedure_lavoro.create
int iCurrent
call super::create
this.dw_tes_procedure_lavoro_lista=create dw_tes_procedure_lavoro_lista
this.cb_dettagli=create cb_dettagli
this.rb_ultime_edizioni=create rb_ultime_edizioni
this.rb_in_creazione=create rb_in_creazione
this.gb_amb_sic=create gb_amb_sic
this.gb_2=create gb_2
this.rb_approva=create rb_approva
this.rb_nuova_edizione=create rb_nuova_edizione
this.cb_esegui=create cb_esegui
this.gb_1=create gb_1
this.rb_1=create rb_1
this.dw_tes_procedure_lavoro_det_2=create dw_tes_procedure_lavoro_det_2
this.dw_folder=create dw_folder
this.dw_tes_procedure_lavoro_det_1=create dw_tes_procedure_lavoro_det_1
this.rb_ambiente=create rb_ambiente
this.rb_sicurezza=create rb_sicurezza
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_procedure_lavoro_lista
this.Control[iCurrent+2]=this.cb_dettagli
this.Control[iCurrent+3]=this.rb_ultime_edizioni
this.Control[iCurrent+4]=this.rb_in_creazione
this.Control[iCurrent+5]=this.gb_amb_sic
this.Control[iCurrent+6]=this.gb_2
this.Control[iCurrent+7]=this.rb_approva
this.Control[iCurrent+8]=this.rb_nuova_edizione
this.Control[iCurrent+9]=this.cb_esegui
this.Control[iCurrent+10]=this.gb_1
this.Control[iCurrent+11]=this.rb_1
this.Control[iCurrent+12]=this.dw_tes_procedure_lavoro_det_2
this.Control[iCurrent+13]=this.dw_folder
this.Control[iCurrent+14]=this.dw_tes_procedure_lavoro_det_1
this.Control[iCurrent+15]=this.rb_ambiente
this.Control[iCurrent+16]=this.rb_sicurezza
end on

on w_tes_procedure_lavoro.destroy
call super::destroy
destroy(this.dw_tes_procedure_lavoro_lista)
destroy(this.cb_dettagli)
destroy(this.rb_ultime_edizioni)
destroy(this.rb_in_creazione)
destroy(this.gb_amb_sic)
destroy(this.gb_2)
destroy(this.rb_approva)
destroy(this.rb_nuova_edizione)
destroy(this.cb_esegui)
destroy(this.gb_1)
destroy(this.rb_1)
destroy(this.dw_tes_procedure_lavoro_det_2)
destroy(this.dw_folder)
destroy(this.dw_tes_procedure_lavoro_det_1)
destroy(this.rb_ambiente)
destroy(this.rb_sicurezza)
end on

type dw_tes_procedure_lavoro_lista from uo_cs_xx_dw within w_tes_procedure_lavoro
integer x = 23
integer y = 20
integer width = 2103
integer height = 500
integer taborder = 50
string dataobject = "d_tes_procedure_lavoro_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   is_flag_valido, &
                   is_flag_in_prova, &
						 is_flag_amb_sic)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event updatestart;call super::updatestart;long   ll_i, ll_revisione, ll_edizione
string ls_cod_resp_divisione, ls_cod_procedura, ls_str, ls_cod_ambiente, ls_cod_postazione


if dw_tes_procedure_lavoro_lista.deletedcount() > 0 then
	for ll_i = 1 to dw_tes_procedure_lavoro_lista.deletedcount()
		ls_cod_procedura = this.getitemstring(ll_i, "cod_procedura", delete!, true)
		ll_edizione = this.getitemnumber(ll_i, "num_edizione", delete!, true)
		ll_revisione = this.getitemnumber(ll_i, "num_revisione", delete!, true)
		delete from  det_procedure_lavoro_blob
				 where (det_procedure_lavoro_blob.cod_azienda   = :s_cs_xx.cod_azienda) and
						 (det_procedure_lavoro_blob.cod_procedura = :ls_cod_procedura) and
						 (det_procedure_lavoro_blob.num_edizione  = :ll_edizione) and
						 (det_procedure_lavoro_blob.num_revisione = :ll_revisione);
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sic","Impossibile eliminare il record selezionato", Information!)
			return 1
		end if
	
		delete from  det_procedure_lavoro_prod
				 where (det_procedure_lavoro_prod.cod_azienda   = :s_cs_xx.cod_azienda) and
						 (det_procedure_lavoro_prod.cod_procedura = :ls_cod_procedura) and
						 (det_procedure_lavoro_prod.num_edizione  = :ll_edizione) and
						 (det_procedure_lavoro_prod.num_revisione = :ll_revisione);
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sic","Impossibile eliminare il record selezionato", Information!)
			return 1
		end if
		
		delete from  det_procedure_lavoro
				 where (det_procedure_lavoro.cod_azienda   = :s_cs_xx.cod_azienda) and
						 (det_procedure_lavoro.cod_procedura = :ls_cod_procedura) and
						 (det_procedure_lavoro.num_edizione  = :ll_edizione) and
						 (det_procedure_lavoro.num_revisione = :ll_revisione);
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sic","Impossibile eliminare il record selezionato", Information!)
			return 1
		end if
	next
end if

if ib_new then
	
	ls_cod_ambiente   = this.getitemstring(this.getrow(),"cod_ambiente")
	ls_cod_postazione = this.getitemstring(this.getrow(),"cod_postazione")
	
	select tes_procedure_lavoro.cod_procedura
	into   :ls_str
	from   tes_procedure_lavoro
	where  tes_procedure_lavoro.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_procedure_lavoro.cod_ambiente = :ls_cod_ambiente and
			 tes_procedure_lavoro.cod_postazione = :ls_cod_postazione and
			 ( tes_procedure_lavoro.flag_valido   = 'S') or 
			 ( tes_procedure_lavoro.flag_in_prova = 'S') ;
	if sqlca.sqlcode = 0 then
		g_mb.messagebox("SIC","Attenzione: è già stata creata una procedura per la postazione indicata",information!)
		return 1
	end if
	
   select mansionari.cod_resp_divisione  
   into   :ls_cod_resp_divisione  
   from   mansionari  
   where  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Lista","Attenzione!! all'utente corrispondono più mansionari, oppure non esiste il mansionario dell'utente", StopSign!)
      pcca.error = c_fatal
      ib_new = false
      return
   end if
   setitem(getrow(), "emesso_da", ls_cod_resp_divisione)
   setitem(getrow(),"num_edizione", 1)
   setitem(getrow(),"num_revisione", 0)
   setitem(getrow(),"flag_valido", "N")
   setitem(getrow(),"flag_in_prova", "S")
   ib_new = false
   ib_new_rec = true
end if   

end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

string   ls_data
long   ll_valido_per

   if isnull(getitemdatetime(getrow(), "emesso_il")) or (date(getitemdatetime(getrow(), "emesso_il")) < date("01/01/1901") )then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione data di creazione lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "emesso_da")) or len(getitemstring(getrow(), "emesso_da")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if
end if			//  di extendmode
end event

event pcd_new;call super::pcd_new;string ls_cod_resp_divisione

cb_dettagli.enabled = false
ib_new =true


if ib_new then
   SELECT mansionari.cod_resp_divisione  
   INTO   :ls_cod_resp_divisione  
   FROM   mansionari  
   WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Lista","Attenzione!! all'utente corrispondono più mansionari, oppure non esiste il mansionario dell'utente", StopSign!)
      pcca.error = c_fatal
   end if
   setitem(getrow(), "emesso_da", ls_cod_resp_divisione )
   setitem(getrow(),"emesso_il", today() )
end if   

dw_tes_procedure_lavoro_det_1.setitem(dw_tes_procedure_lavoro_lista.getrow(), "flag_ambiente_sic", "S")
end event

event pcd_view;call super::pcd_view;
cb_dettagli.enabled = true

if ib_new_rec then
   rb_in_creazione.checked = true
   rb_in_creazione.postevent("Clicked")
   ib_new_rec = false
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_delete;call super::pcd_delete;cb_dettagli.enabled = false

end event

event pcd_modify;call super::pcd_modify;cb_dettagli.enabled = false

end event

event rowfocuschanged;call super::rowfocuschanged;if currentrow > 0 then
	f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"cod_postazione",sqlca,&
						  "tab_postazioni_lavoro","cod_postazione","des_postazione",&
						  "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
						  "(tab_postazioni_lavoro.cod_ambiente = '" + getitemstring(currentrow,"cod_ambiente") + "') and " + &
						  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )
end if
end event

type cb_dettagli from commandbutton within w_tes_procedure_lavoro
integer x = 2149
integer y = 1060
integer width = 370
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Dettaglio"
end type

event clicked;if dw_tes_procedure_lavoro_lista.i_numselected < 1 then
   g_mb.messagebox("Dettaglio Procedure di Lavoro","Selezionare prima la procedura nella quale entrare in dettaglio",StopSign!)
   return
end if
if not isvalid(w_det_procedure_lavoro) then
	window_open_parm(w_det_procedure_lavoro, -1, dw_tes_procedure_lavoro_lista)
end if
end event

type rb_ultime_edizioni from radiobutton within w_tes_procedure_lavoro
integer x = 2171
integer y = 100
integer width = 494
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Ultime Revisioni"
end type

event clicked;rb_approva.enabled = false
rb_nuova_edizione.enabled = true
cb_esegui.enabled = false

is_flag_valido = "S"
is_flag_in_prova = "%"

is_tipo_records = "E"

dw_tes_procedure_lavoro_lista.setfilter("")
dw_tes_procedure_lavoro_lista.filter()

dw_tes_procedure_lavoro_lista.change_dw_current( )
parent.triggerevent("pc_retrieve")
end event

type rb_in_creazione from radiobutton within w_tes_procedure_lavoro
integer x = 2171
integer y = 180
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "In Creazione"
end type

event clicked;rb_approva.enabled = true
rb_nuova_edizione.enabled = false
cb_esegui.enabled = false

is_flag_valido = "N"
is_flag_in_prova = "S"
is_tipo_records = "C"

dw_tes_procedure_lavoro_lista.setfilter("")
dw_tes_procedure_lavoro_lista.filter()

dw_tes_procedure_lavoro_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
end event

type gb_amb_sic from groupbox within w_tes_procedure_lavoro
integer x = 2149
integer y = 660
integer width = 549
integer height = 260
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Situazione"
end type

type gb_2 from groupbox within w_tes_procedure_lavoro
integer x = 2149
integer y = 20
integer width = 549
integer height = 340
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza"
end type

type rb_approva from radiobutton within w_tes_procedure_lavoro
integer x = 2171
integer y = 460
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Approva"
end type

on clicked;il_tipo_operazione = 2
cb_esegui.enabled = true
end on

type rb_nuova_edizione from radiobutton within w_tes_procedure_lavoro
integer x = 2171
integer y = 540
integer width = 507
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Nuova Revisione"
end type

on clicked;il_tipo_operazione = 4
cb_esegui.enabled = true
end on

type cb_esegui from commandbutton within w_tes_procedure_lavoro
integer x = 2149
integer y = 960
integer width = 370
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;long ll_riga_corrente


ll_riga_corrente = dw_tes_procedure_lavoro_lista.getrow()
choose case il_tipo_operazione
case 2
   if wf_approva() = 0 then parent.triggerevent("pc_retrieve")
   dw_tes_procedure_lavoro_lista.setrow(ll_riga_corrente)
   rb_in_creazione.triggerevent("clicked")
   setpointer(arrow!) 
case 4
   if wf_nuova_edizione() = 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo","La nuova edizione si trova fra le liste IN CREAZIONE",Information!)
   end if   
   setpointer(arrow!)
end choose
end event

type gb_1 from groupbox within w_tes_procedure_lavoro
integer x = 2149
integer y = 380
integer width = 549
integer height = 260
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Operazioni"
end type

type rb_1 from radiobutton within w_tes_procedure_lavoro
integer x = 2171
integer y = 260
integer width = 270
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Storico"
end type

event clicked;rb_approva.enabled = false
rb_nuova_edizione.enabled = false
cb_esegui.enabled = false

is_flag_valido = "N"
is_flag_in_prova = "N"

is_tipo_records = "T"

dw_tes_procedure_lavoro_lista.setfilter("")
dw_tes_procedure_lavoro_lista.filter()

dw_tes_procedure_lavoro_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type dw_tes_procedure_lavoro_det_2 from uo_cs_xx_dw within w_tes_procedure_lavoro
event pcd_validatecol pbm_custom73
event pcd_validaterow pbm_custom74
integer x = 46
integer y = 680
integer width = 2034
integer height = 936
integer taborder = 20
string dataobject = "d_tes_procedure_lavoro_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_tes_procedure_lavoro
integer x = 23
integer y = 540
integer width = 2103
integer height = 1320
integer taborder = 80
end type

type dw_tes_procedure_lavoro_det_1 from uo_cs_xx_dw within w_tes_procedure_lavoro
integer x = 69
integer y = 680
integer width = 2011
integer height = 1160
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_tes_procedure_lavoro_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "cod_ambiente"
			if not isnull(i_coltext) then
				f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"cod_postazione",sqlca,&
						  "tab_postazioni_lavoro","cod_postazione","des_postazione",&
						  "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
						  "(tab_postazioni_lavoro.cod_ambiente = '" + i_coltext + "') and " + &
						  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )
			end if																							
		case "cod_postazione"
			if not isnull(i_coltext) then
				f_PO_LoadDDDW_DW(dw_tes_procedure_lavoro_det_1,"cod_attrezzatura",sqlca,&
									  "anag_attrezzature","cod_attrezzatura","descrizione",&
									  "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
			end if																							
	end choose
end if


end event

type rb_ambiente from radiobutton within w_tes_procedure_lavoro
integer x = 2171
integer y = 740
integer width = 329
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Ambiente"
end type

event clicked;is_flag_amb_sic = "S"

dw_tes_procedure_lavoro_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type rb_sicurezza from radiobutton within w_tes_procedure_lavoro
integer x = 2171
integer y = 820
integer width = 338
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Sicurezza"
end type

event clicked;is_flag_amb_sic = "N"

dw_tes_procedure_lavoro_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

