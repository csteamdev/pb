﻿$PBExportHeader$w_tipi_rischi.srw
$PBExportComments$Finestra Gestione Tabella Tipi Rischi
forward
global type w_tipi_rischi from w_cs_xx_principale
end type
type dw_tipi_rischi from uo_cs_xx_dw within w_tipi_rischi
end type
end forward

global type w_tipi_rischi from w_cs_xx_principale
int Width=3123
int Height=1145
boolean TitleBar=true
string Title="Tipi Rischi"
dw_tipi_rischi dw_tipi_rischi
end type
global w_tipi_rischi w_tipi_rischi

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_tipi_rischi.set_dw_key("cod_azienda")
dw_tipi_rischi.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
iuo_dw_main = dw_tipi_rischi
dw_tipi_rischi.ib_proteggi_chiavi = false


end event

on w_tipi_rischi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_rischi=create dw_tipi_rischi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_rischi
end on

on w_tipi_rischi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_rischi)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tipi_rischi,"cod_parte_corpo",sqlca,&
                 "tab_parti_corpo","cod_parte_corpo","des_parte_corpo",&
                 "(tab_parti_corpo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_parti_corpo.flag_blocco <> 'S') or (tab_parti_corpo.flag_blocco = 'S' and tab_parti_corpo.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

end event

type dw_tipi_rischi from uo_cs_xx_dw within w_tipi_rischi
int X=23
int Y=21
int Width=3041
int Height=1001
int TabOrder=10
string DataObject="d_tipi_rischi"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

