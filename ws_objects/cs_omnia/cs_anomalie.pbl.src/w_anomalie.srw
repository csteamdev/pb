﻿$PBExportHeader$w_anomalie.srw
$PBExportComments$Window Anomalie
forward
global type w_anomalie from w_cs_xx_principale
end type
type cb_tempo_medio_risoluzione from commandbutton within w_anomalie
end type
type sle_data_soluzione from u_sle_search within w_anomalie
end type
type sle_data_registrazione from u_sle_search within w_anomalie
end type
type st_data_soluzione from statictext within w_anomalie
end type
type st_data_registrazione from statictext within w_anomalie
end type
type dw_anomalie_det from uo_cs_xx_dw within w_anomalie
end type
type cb_doc_anomalia from commandbutton within w_anomalie
end type
type cb_doc_risposta from commandbutton within w_anomalie
end type
type cb_reset from commandbutton within w_anomalie
end type
type cb_ricerca from commandbutton within w_anomalie
end type
type cb_1 from commandbutton within w_anomalie
end type
type cb_report from commandbutton within w_anomalie
end type
type cb_genera_nc from commandbutton within w_anomalie
end type
type dw_ricerca from u_dw_search within w_anomalie
end type
type dw_folder_search from u_folder within w_anomalie
end type
type dw_anomalie_lista from uo_cs_xx_dw within w_anomalie
end type
end forward

global type w_anomalie from w_cs_xx_principale
integer width = 3278
integer height = 2328
string title = "Registro Anomalie"
event ue_cerca_riga ( )
cb_tempo_medio_risoluzione cb_tempo_medio_risoluzione
sle_data_soluzione sle_data_soluzione
sle_data_registrazione sle_data_registrazione
st_data_soluzione st_data_soluzione
st_data_registrazione st_data_registrazione
dw_anomalie_det dw_anomalie_det
cb_doc_anomalia cb_doc_anomalia
cb_doc_risposta cb_doc_risposta
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_1 cb_1
cb_report cb_report
cb_genera_nc cb_genera_nc
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
dw_anomalie_lista dw_anomalie_lista
end type
global w_anomalie w_anomalie

type variables
long il_anno, il_num
end variables

event ue_cerca_riga();long    ll_row

boolean lb_trovato


if isnull(il_anno) then
	il_anno = f_anno_esercizio()
end if

dw_ricerca.setitem(dw_ricerca.getrow(),"anno_registrazione",il_anno)

cb_reset.triggerevent("clicked")

for ll_row = 1 to dw_anomalie_lista.rowcount()
	if dw_anomalie_lista.getitemnumber(ll_row,"anno_registrazione") = il_anno and &
		dw_anomalie_lista.getitemnumber(ll_row,"num_registrazione") = il_num then
		lb_trovato = true
		exit
	end if
next

if lb_trovato then
	dw_anomalie_lista.setrow(ll_row)
	dw_anomalie_lista.scrolltorow(ll_row)
end if
end event

on w_anomalie.create
int iCurrent
call super::create
this.cb_tempo_medio_risoluzione=create cb_tempo_medio_risoluzione
this.sle_data_soluzione=create sle_data_soluzione
this.sle_data_registrazione=create sle_data_registrazione
this.st_data_soluzione=create st_data_soluzione
this.st_data_registrazione=create st_data_registrazione
this.dw_anomalie_det=create dw_anomalie_det
this.cb_doc_anomalia=create cb_doc_anomalia
this.cb_doc_risposta=create cb_doc_risposta
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_1=create cb_1
this.cb_report=create cb_report
this.cb_genera_nc=create cb_genera_nc
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_anomalie_lista=create dw_anomalie_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_tempo_medio_risoluzione
this.Control[iCurrent+2]=this.sle_data_soluzione
this.Control[iCurrent+3]=this.sle_data_registrazione
this.Control[iCurrent+4]=this.st_data_soluzione
this.Control[iCurrent+5]=this.st_data_registrazione
this.Control[iCurrent+6]=this.dw_anomalie_det
this.Control[iCurrent+7]=this.cb_doc_anomalia
this.Control[iCurrent+8]=this.cb_doc_risposta
this.Control[iCurrent+9]=this.cb_reset
this.Control[iCurrent+10]=this.cb_ricerca
this.Control[iCurrent+11]=this.cb_1
this.Control[iCurrent+12]=this.cb_report
this.Control[iCurrent+13]=this.cb_genera_nc
this.Control[iCurrent+14]=this.dw_ricerca
this.Control[iCurrent+15]=this.dw_folder_search
this.Control[iCurrent+16]=this.dw_anomalie_lista
end on

on w_anomalie.destroy
call super::destroy
destroy(this.cb_tempo_medio_risoluzione)
destroy(this.sle_data_soluzione)
destroy(this.sle_data_registrazione)
destroy(this.st_data_soluzione)
destroy(this.st_data_registrazione)
destroy(this.dw_anomalie_det)
destroy(this.cb_doc_anomalia)
destroy(this.cb_doc_risposta)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_1)
destroy(this.cb_report)
destroy(this.cb_genera_nc)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_anomalie_lista)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject l_objects[ ]
long ll_anno


il_anno = s_cs_xx.parametri.parametro_d_1

il_num = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

dw_anomalie_lista.set_dw_options(sqlca,pcca.null_object, c_noretrieveonopen,c_default)
dw_anomalie_det.set_dw_options(sqlca,dw_anomalie_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_anomalie_lista

l_criteriacolumn[1] = "cod_prodotto"
l_criteriacolumn[2] = "anno_registrazione"
l_criteriacolumn[3] = "flag_risolta"
l_criteriacolumn[4] = "cod_operaio_registrazione"
l_criteriacolumn[5] = "cod_cliente"
l_criteriacolumn[6] = "nome_particolare"
l_criteriacolumn[7] = "cod_operaio_risolve"
l_criteriacolumn[8] = "flag_altre_richieste"

l_searchtable[1] = "tab_anomalie"
l_searchtable[2] = "tab_anomalie"
l_searchtable[3] = "tab_anomalie"
l_searchtable[4] = "tab_anomalie"
l_searchtable[5] = "tab_anomalie"
l_searchtable[6] = "tab_anomalie"
l_searchtable[7] = "tab_anomalie"
l_searchtable[8] = "tab_anomalie"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "anno_registrazione"
l_searchcolumn[3] = "flag_risolta"
l_searchcolumn[4] = "cod_operaio_registrazione"
l_searchcolumn[5] = "cod_cliente"
l_searchcolumn[6] = "nome_particolare"
l_searchcolumn[7] = "cod_operaio_risolve"
l_searchcolumn[8] = "flag_altre_richieste"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_anomalie_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)


sle_data_registrazione.fu_WireDW(dw_anomalie_lista,"tab_anomalie","data_registrazione",SQLCA)													 
sle_data_soluzione.fu_WireDW(dw_anomalie_lista,"tab_anomalie","data_risoluzione",SQLCA)


dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)
l_objects[1] = dw_anomalie_lista
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
l_objects[4] = st_data_registrazione
l_objects[5] = st_data_soluzione
l_objects[6] = sle_data_registrazione
l_objects[7] = sle_data_soluzione

dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)

select numero 
  into :ll_anno
  from parametri_azienda
 where cod_azienda = :s_cs_xx.cod_azienda
   and flag_parametro = 'N'
	and cod_parametro = 'ESC';
	
if not isnull(ll_anno) then
	dw_ricerca.setitem(1, "anno_registrazione", ll_anno)
end if	
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_anomalie_det, &
                 "cod_operaio_registra", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_anomalie_det, &
                 "cod_operaio_risolve", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_anomalie_det, &
                 "cod_resp_ver_efficacia", &
                 sqlca, &
                 "mansionari", &
                 "cod_resp_divisione", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  


f_po_loaddddw_dw(dw_ricerca, &
                 "cod_operaio_registra", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_ricerca, &
                 "cod_operaio_risolve", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event open;call super::open;//modifica michela: non voglio che carichi tuttto!!
//postevent("ue_cerca_riga")
end event

type cb_tempo_medio_risoluzione from commandbutton within w_anomalie
integer x = 1257
integer y = 2132
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "T.medio ris."
end type

event clicked;datetime ldt_data_registrazione,ldt_data_risoluzione
long  ll_giorni_risoluzione,ll_totale_anomalie,ll_anno_registrazione,ll_num_registrazione
decimal{4} ld_tempo_medio

declare r_anomalie cursor for
select anno_registrazione,
		 num_registrazione,
		 data_registrazione,
		 data_risoluzione
from   tab_anomalie
where  cod_azienda=:s_cs_xx.cod_azienda
and    flag_risolta='S';
// and    data_registrazione between '20041001' and '20050930';

open r_anomalie;

do while 1=1
	fetch r_anomalie
	into  :ll_anno_registrazione,
			:ll_num_registrazione,
			:ldt_data_registrazione,
		   :ldt_data_risoluzione;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		exit
	end if
	
	if not isnull(ldt_data_registrazione) and not isnull(ldt_data_risoluzione) then
		ll_totale_anomalie++
		ll_giorni_risoluzione = ll_giorni_risoluzione + DaysAfter ( date(ldt_data_registrazione), date(ldt_data_risoluzione) )
		//if DaysAfter ( date(ldt_data_registrazione), date(ldt_data_risoluzione) ) > 50 then 	messagebox("OMNIA","controllare anomalie anno_reg/num_reg"  + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione),information!)	
	end if
	
loop

close r_anomalie;

if ll_totale_anomalie > 0 then
	ld_tempo_medio = ll_giorni_risoluzione/ll_totale_anomalie
end if

g_mb.messagebox("OMNIA","Il tempo medio per la risoluzione delle anomalie risolte è di " + string(ld_tempo_medio) + " giorni.",information!)

end event

type sle_data_soluzione from u_sle_search within w_anomalie
integer x = 1257
integer y = 80
integer width = 571
integer height = 80
integer taborder = 60
end type

type sle_data_registrazione from u_sle_search within w_anomalie
integer x = 2171
integer y = 80
integer width = 571
integer height = 80
integer taborder = 60
end type

type st_data_soluzione from statictext within w_anomalie
integer x = 891
integer y = 92
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Data Risoluz.:"
boolean focusrectangle = false
end type

type st_data_registrazione from statictext within w_anomalie
integer x = 1851
integer y = 92
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Data Regis.:"
boolean focusrectangle = false
end type

event getfocus;call super::getfocus;dw_anomalie_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_anomalie_det from uo_cs_xx_dw within w_anomalie
integer x = 18
integer y = 632
integer width = 3200
integer height = 1480
integer taborder = 140
string dataobject = "d_anomalie_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_anomalie_det,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_anomalie_det,"cod_prodotto")
end choose
end event

event pcd_delete;call super::pcd_delete;dw_anomalie_det.object.b_ricerca_prodotto.enabled=false
end event

event pcd_modify;call super::pcd_modify;dw_anomalie_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_new;call super::pcd_new;dw_anomalie_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_view;call super::pcd_view;dw_anomalie_det.object.b_ricerca_prodotto.enabled=false
end event

type cb_doc_anomalia from commandbutton within w_anomalie
integer x = 2414
integer y = 2132
integer width = 389
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Doc.&Anomalia"
end type

event clicked;if dw_anomalie_lista.rowcount()>0 then
	if isnull(dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"anno_registrazione")) then return
	
	//window_open_parm(w_blob_anomalie, -1, dw_anomalie_lista)
	s_cs_xx.parametri.parametro_ul_1 = dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_ul_2 = dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"num_registrazione")
	open(w_anomalie_ole)
end if
end event

type cb_doc_risposta from commandbutton within w_anomalie
integer x = 2825
integer y = 2132
integer width = 389
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Doc.&Risposta"
end type

event clicked;if dw_anomalie_lista.rowcount()>0 then
	if isnull(dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"anno_registrazione")) then return
	//window_open_parm(w_blob_risposte, -1, dw_anomalie_lista)
	s_cs_xx.parametri.parametro_ul_1 = dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_ul_2 = dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"num_registrazione")
	open(w_risposte_ole)
end if
end event

type cb_reset from commandbutton within w_anomalie
integer x = 2789
integer y = 380
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
sle_data_registrazione.fu_BuildSearch(false)
sle_data_soluzione.fu_BuildSearch(false)
dw_folder_search.fu_selecttab(1)
dw_anomalie_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_anomalie
integer x = 2789
integer y = 480
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;long ll_anno

dw_ricerca.fu_reset()

select numero 
  into :ll_anno
  from parametri_azienda
 where cod_azienda = :s_cs_xx.cod_azienda
   and flag_parametro = 'N'
	and cod_parametro = 'ESC';
	
if not isnull(ll_anno) then
	dw_ricerca.setitem(1, "anno_registrazione", ll_anno)
end if	



end event

type cb_1 from commandbutton within w_anomalie
integer x = 2025
integer y = 2132
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

type cb_report from commandbutton within w_anomalie
integer x = 2025
integer y = 2132
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = ""
s_cs_xx.parametri.parametro_s_1 = dw_anomalie_lista.getsqlselect()
window_open(w_report_anomalie_nc, -1)
end event

type cb_genera_nc from commandbutton within w_anomalie
integer x = 1637
integer y = 2132
integer width = 366
integer height = 80
integer taborder = 101
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. N.C."
end type

event clicked;string ls_cod_prodotto, ls_cod_operaio,ls_cod_cliente, ls_des_anomalia_1, ls_flag_prod_codificato
long ll_anno_registrazione, ll_num_registrazione,ll_anno_anomalia, ll_num_anomalia
datetime ldt_data_registrazione

if dw_anomalie_lista.getrow() < 1 then return
if dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"anno_non_conformita") > 0 or not isnull(dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"anno_non_conformita")) then
	g_mb.messagebox("OMNIA","E' già stata generata una NC per questa anomalia")
	return
end if

ll_anno_anomalia = dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"anno_registrazione")
ll_num_anomalia  = dw_anomalie_lista.getitemnumber(dw_anomalie_lista.getrow(),"num_registrazione")

setnull(ll_num_registrazione)
ll_anno_registrazione = f_anno_esercizio()
select max(num_non_conf) + 1
into   :ll_num_registrazione
from   non_conformita
where  cod_azienda =:s_cs_xx.cod_azienda and
       anno_non_conf = :ll_anno_registrazione;
if isnull(ll_num_registrazione) then
	ll_num_registrazione = 1
end if

ls_cod_prodotto = dw_anomalie_lista.getitemstring(dw_anomalie_lista.getrow(),"cod_prodotto")
ls_cod_operaio = dw_anomalie_lista.getitemstring(dw_anomalie_lista.getrow(),"cod_operaio_registra")
ldt_data_registrazione = datetime(today(), 00:00:00)
ls_cod_cliente = dw_anomalie_lista.getitemstring(dw_anomalie_lista.getrow(),"cod_cliente")
ls_des_anomalia_1 = dw_anomalie_lista.getitemstring(dw_anomalie_lista.getrow(),"des_anomalia_1")
if isnull(ls_cod_prodotto) then
	ls_flag_prod_codificato = "N"
else
	ls_flag_prod_codificato = "S"
end if

insert into non_conformita
		(cod_azienda,
		anno_non_conf,
		num_non_conf,
		flag_tipo_nc,
		cod_operaio,
		data_creazione,
		data_scadenza,
		cod_cliente,
		flag_prod_codificato,
		cod_prodotto)
values
		(:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		'V',
		:ls_cod_operaio,
		:ldt_data_registrazione,
		null,
		:ls_cod_cliente,
		:ls_flag_prod_codificato,
		:ls_cod_prodotto);
if sqlca.sqlcode <> 0  then
	g_mb.messagebox("OMNIA","Errore in fase di creazione N.C. Dettaglio errore: " + sqlca.sqlerrtext)
	return
	rollback;
end if

update tab_anomalie
set anno_non_conformita = :ll_anno_registrazione, num_non_conformita = :ll_num_registrazione
where cod_azienda        = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_anomalia and
		num_registrazione  = :ll_num_anomalia;
if sqlca.sqlcode <> 0  then
	g_mb.messagebox("OMNIA","Errore in fase di aggiornamento anomalia selezionata con il numero della N.C. generata. Dettaglio errore: " + sqlca.sqlerrtext)
	return
	rollback;
end if

g_mb.messagebox("OMNIA","E' stata generata con successo la Non Conformità " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))

end event

type dw_ricerca from u_dw_search within w_anomalie
event ue_key pbm_dwnkey
integer x = 160
integer y = 60
integer width = 3040
integer height = 520
integer taborder = 50
string dataobject = "d_anomalie_ricerca"
boolean border = false
end type

event ue_key;call super::ue_key;//choose case this.getcolumnname()
//	case "rs_cod_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//			s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
//			s_cs_xx.parametri.parametro_s_1 = "rs_cod_prodotto"
//			if not isvalid(w_prodotti_ricerca) then
//				window_open(w_prodotti_ricerca, 0)
//			end if
//			w_prodotti_ricerca.show()
//		end if
//end choose
//
//if key = keyenter! then cb_reset.postevent("clicked")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")
end choose
end event

type dw_folder_search from u_folder within w_anomalie
integer x = 23
integer y = 20
integer width = 3200
integer height = 600
integer taborder = 60
end type

type dw_anomalie_lista from uo_cs_xx_dw within w_anomalie
integer x = 160
integer y = 60
integer width = 3040
integer height = 520
integer taborder = 130
string dataobject = "d_anomalie_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,li_valido_per,ll_num_registrazione
integer li_anno_esercizio


li_anno_esercizio = f_anno_esercizio()

select max(num_registrazione)
into   :ll_num_registrazione
from   tab_anomalie
where  cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione = :li_anno_esercizio;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then 
	ll_num_registrazione = 1
else 
	ll_num_registrazione++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
	   SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "anno_registrazione", li_anno_esercizio)
		SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
		ll_num_registrazione++
   END IF
NEXT
end event

event updatestart;call super::updatestart;long ll_i,ll_anno_registrazione,ll_num_registrazione

for ll_i = 1 to this.deletedcount()
	ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione", delete!, true)
	ll_num_registrazione = getitemnumber(ll_i, "num_registrazione", delete!, true)

	delete blob_anomalie
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sulla cancellazione documenti-anomalie. Errore DB:" + sqlca.sqlerrtext,stopsign!)
		return
		rollback;
	end if
	
	delete blob_risposte
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sulla cancellazione documenti-risposte. Errore DB:" + sqlca.sqlerrtext,stopsign!)
		return
		rollback;
	end if
	
next
end event

event pcd_view;call super::pcd_view;dw_anomalie_det.object.b_ricerca_cliente.enabled=false
dw_anomalie_det.object.b_ricerca_prodotto.enabled=false
end event

event pcd_new;call super::pcd_new;dw_anomalie_det.object.b_ricerca_cliente.enabled=true
dw_anomalie_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_modify;call super::pcd_modify;dw_anomalie_det.object.b_ricerca_cliente.enabled=true
dw_anomalie_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_delete;call super::pcd_delete;dw_anomalie_det.object.b_ricerca_cliente.enabled=false
dw_anomalie_det.object.b_ricerca_prodotto.enabled=false
end event

