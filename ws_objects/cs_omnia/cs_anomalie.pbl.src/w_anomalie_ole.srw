﻿$PBExportHeader$w_anomalie_ole.srw
forward
global type w_anomalie_ole from w_ole_v2
end type
end forward

global type w_anomalie_ole from w_ole_v2
end type
global w_anomalie_ole w_anomalie_ole

forward prototypes
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name)
public subroutine wf_load_documents ()
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
end prototypes

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);delete from blob_anomalie
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	prog_blob = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);selectblob blob
into :ab_file_blob
from blob_anomalie
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	prog_blob = :astr_data.progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la lettura del documento " + alv_item.label + ". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name);update blob_anomalie
set des_blob = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	prog_blob = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	// errore durante il salvataggio
	return false
else
	// tutto ok
	return true
end if
end function

public subroutine wf_load_documents ();string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

ls_sql = "SELECT &
	des_blob, &
	prog_blob, &
	prog_mimetype &
FROM blob_anomalie &
WHERE &
	cod_azienda ='" + s_cs_xx.cod_azienda +"' and &
	anno_registrazione =" + string(il_anno_registrazione) + " and &
	num_registrazione =" + string(il_num_registrazione) + " &
ORDER BY &
	cod_azienda ASC, &
	anno_registrazione ASC, &
	num_registrazione ASC "
	
if not f_crea_datastore(ref lds_documenti, ls_sql) then
	return 
end if

li_rows = lds_documenti.retrieve()
for li_i = 1 to li_rows
	lstr_data.anno_registrazione = il_anno_registrazione
	lstr_data.num_registrazione = il_num_registrazione
	lstr_data.progressivo = lds_documenti.getitemnumber(li_i, "prog_blob")
	lstr_data.prog_mimetype = lds_documenti.getitemnumber(li_i, "prog_mimetype")
	
	wf_add_document(lds_documenti.getitemstring(li_i, "des_blob"), lstr_data)
next
end subroutine

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);long ll_progressivo
str_ole lstr_data

// Calcolo progressivo
select max(prog_blob)
into :ll_progressivo
from blob_anomalie
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il calcolo del progressivo per il file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) or ll_progressivo < 1 then
	ll_progressivo = 0
end if

ll_progressivo ++
	
// Inserimento nuova riga
insert into blob_anomalie (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	prog_blob,
	des_blob,
	prog_mimetype)
values (
	:s_cs_xx.cod_azienda,
	:il_anno_registrazione,
	:il_num_registrazione,
	:ll_progressivo,
	:as_file_name,
	:al_prog_mimetype);
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Aggiornamento campo blob
updateblob blob_anomalie
set blob = :ab_file_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	prog_blob = :ll_progressivo;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Creo struttura e aggiongo icona
lstr_data.anno_registrazione = il_anno_registrazione
lstr_data.num_registrazione = il_num_registrazione
lstr_data.progressivo = ll_progressivo
lstr_data.prog_mimetype = al_prog_mimetype
// aggiungere informazioni alla struttura se necessario

wf_add_document(as_file_name, lstr_data)

return true
end function

on w_anomalie_ole.create
call super::create
end on

on w_anomalie_ole.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;il_anno_registrazione = s_cs_xx.parametri.parametro_ul_1
il_num_registrazione = s_cs_xx.parametri.parametro_ul_2
end event

type st_loading from w_ole_v2`st_loading within w_anomalie_ole
end type

type cb_cancella from w_ole_v2`cb_cancella within w_anomalie_ole
end type

type lv_documenti from w_ole_v2`lv_documenti within w_anomalie_ole
end type

type ddlb_style from w_ole_v2`ddlb_style within w_anomalie_ole
end type

type cb_sfoglia from w_ole_v2`cb_sfoglia within w_anomalie_ole
end type

