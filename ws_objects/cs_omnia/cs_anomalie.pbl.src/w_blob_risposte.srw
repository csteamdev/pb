﻿$PBExportHeader$w_blob_risposte.srw
$PBExportComments$Window blob per risposta
forward
global type w_blob_risposte from w_cs_xx_principale
end type
type dw_blob_risposte from uo_cs_xx_dw within w_blob_risposte
end type
type cb_note_esterne from commandbutton within w_blob_risposte
end type
end forward

global type w_blob_risposte from w_cs_xx_principale
integer width = 2683
integer height = 1304
string title = "Documenti di Risposta"
dw_blob_risposte dw_blob_risposte
cb_note_esterne cb_note_esterne
end type
global w_blob_risposte w_blob_risposte

on w_blob_risposte.create
int iCurrent
call super::create
this.dw_blob_risposte=create dw_blob_risposte
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_blob_risposte
this.Control[iCurrent+2]=this.cb_note_esterne
end on

on w_blob_risposte.destroy
call super::destroy
destroy(this.dw_blob_risposte)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;dw_blob_risposte.set_dw_key("cod_azienda")
dw_blob_risposte.set_dw_key("anno_registrazione")
dw_blob_risposte.set_dw_key("num_registrazione")
dw_blob_risposte.set_dw_key("prog_blob")
dw_blob_risposte.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)


iuo_dw_main = dw_blob_risposte

cb_note_esterne.enabled = false







end event

type dw_blob_risposte from uo_cs_xx_dw within w_blob_risposte
integer x = 23
integer y = 20
integer width = 2606
integer height = 1060
integer taborder = 20
string dataobject = "d_blob_risposte"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore,ll_num_registrazione
integer li_anno_registrazione



li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_registrazione,ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_num_registrazione
integer li_anno_registrazione,ll_prog_blob

li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

select max(prog_blob)
into   :ll_prog_blob
from   blob_risposte
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

if ll_prog_blob = 0 or isnull(ll_prog_blob) then
	ll_prog_blob = 1
else
	ll_prog_blob++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
	   SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "anno_registrazione", li_anno_registrazione)
		SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
		SetItem(l_Idx, "prog_blob", ll_prog_blob)
		ll_prog_blob++
   END IF
NEXT
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
		if not isnull(getitemnumber(getrow(), "anno_registrazione")) then
			cb_note_esterne.enabled = true
		else
			cb_note_esterne.enabled = false
		end if
	end if
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
		if not isnull(getitemnumber(getrow(), "anno_registrazione")) then
			cb_note_esterne.enabled = true
		else
			cb_note_esterne.enabled = false
		end if
	end if
end if
end event

type cb_note_esterne from commandbutton within w_blob_risposte
integer x = 2263
integer y = 1100
integer width = 366
integer height = 80
integer taborder = 21
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;integer li_anno_registrazione, li_risposta
long ll_prog_blob,ll_num_registrazione,ll_i
blob lbl_null
string ls_db
transaction sqlcb

if dw_blob_risposte.getrow() > 0 then
	setnull(lbl_null)
	
	ll_i = dw_blob_risposte.getrow()
	li_anno_registrazione = dw_blob_risposte.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = dw_blob_risposte.getitemnumber(ll_i, "num_registrazione")
	ll_prog_blob = dw_blob_risposte.getitemnumber(ll_i, "prog_blob")
	
// 11-07-2002 modifiche Michela: controllo l'enginetype

	ls_db = f_db()
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)

		selectblob blob
		into       :s_cs_xx.parametri.parametro_bl_1
		from       blob_risposte
		where      cod_azienda = :s_cs_xx.cod_azienda 
		and        anno_registrazione = :li_anno_registrazione
		and        num_registrazione = :ll_num_registrazione
		and        prog_blob=:ll_prog_blob
		using      sqlcb;
	
		if sqlcb.sqlcode <> 0 then
			s_cs_xx.parametri.parametro_bl_1 = lbl_null
		end if
		
		destroy sqlcb;
		
	else
		
		selectblob blob
		into       :s_cs_xx.parametri.parametro_bl_1
		from       blob_risposte
		where      cod_azienda = :s_cs_xx.cod_azienda 
		and        anno_registrazione = :li_anno_registrazione
		and        num_registrazione = :ll_num_registrazione
		and        prog_blob=:ll_prog_blob;
	
		if sqlca.sqlcode <> 0 then
			s_cs_xx.parametri.parametro_bl_1 = lbl_null
		end if
	
	end if
	
	window_open(w_ole, 0)
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob blob_risposte
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda 
			and        anno_registrazione = :li_anno_registrazione
			and        num_registrazione = :ll_num_registrazione
			and        prog_blob=:ll_prog_blob
			using      sqlcb;
			
			destroy sqlcb;
			
		else
	
			updateblob blob_risposte
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda 
			and        anno_registrazione = :li_anno_registrazione
			and        num_registrazione = :ll_num_registrazione
			and        prog_blob=:ll_prog_blob;
	
		end if
		
		commit;
	end if
end if
end event

