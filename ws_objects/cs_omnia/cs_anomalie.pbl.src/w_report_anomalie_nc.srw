﻿$PBExportHeader$w_report_anomalie_nc.srw
$PBExportComments$Finestra report anomalie e NC
forward
global type w_report_anomalie_nc from w_cs_xx_principale
end type
type dw_report_anomalie_nc from uo_cs_xx_dw within w_report_anomalie_nc
end type
end forward

global type w_report_anomalie_nc from w_cs_xx_principale
integer width = 3259
integer height = 1752
dw_report_anomalie_nc dw_report_anomalie_nc
end type
global w_report_anomalie_nc w_report_anomalie_nc

event pc_setwindow;call super::pc_setwindow;
dw_report_anomalie_nc.ib_dw_report = true

set_w_options(c_noresizewin)
dw_report_anomalie_nc.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_disablecc, &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)


end event

on w_report_anomalie_nc.create
int iCurrent
call super::create
this.dw_report_anomalie_nc=create dw_report_anomalie_nc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_anomalie_nc
end on

on w_report_anomalie_nc.destroy
call super::destroy
destroy(this.dw_report_anomalie_nc)
end on

type dw_report_anomalie_nc from uo_cs_xx_dw within w_report_anomalie_nc
integer x = 23
integer y = 20
integer width = 3177
integer height = 1600
string dataobject = "d_report_anomalie_nc"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;dw_report_anomalie_nc.retrieve(s_cs_xx.cod_azienda)
end event

