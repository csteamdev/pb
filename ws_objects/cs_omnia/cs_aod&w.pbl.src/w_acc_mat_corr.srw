﻿$PBExportHeader$w_acc_mat_corr.srw
$PBExportComments$Finestra Gestione Corrispondenze Accettazione Materiali
forward
global type w_acc_mat_corr from w_cs_xx_principale
end type
type dw_acc_mat_corr_lista from uo_cs_xx_dw within w_acc_mat_corr
end type
type cb_note_esterne from commandbutton within w_acc_mat_corr
end type
type cb_controllo from commandbutton within w_acc_mat_corr
end type
type dw_acc_mat_corr_det_1 from uo_cs_xx_dw within w_acc_mat_corr
end type
type dw_acc_mat_corr_det_2 from uo_cs_xx_dw within w_acc_mat_corr
end type
type dw_folder from u_folder within w_acc_mat_corr
end type
end forward

global type w_acc_mat_corr from w_cs_xx_principale
integer width = 2418
integer height = 1544
string title = "Corrispondenze Acc. Materiali"
dw_acc_mat_corr_lista dw_acc_mat_corr_lista
cb_note_esterne cb_note_esterne
cb_controllo cb_controllo
dw_acc_mat_corr_det_1 dw_acc_mat_corr_det_1
dw_acc_mat_corr_det_2 dw_acc_mat_corr_det_2
dw_folder dw_folder
end type
global w_acc_mat_corr w_acc_mat_corr

type variables
boolean ib_in_new
integer ii_new_row
end variables

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_acc_mat_corr_det_1
dw_folder.fu_AssignTab(1, "&Dettaglio", l_Objects[])

l_objects[1] = dw_acc_mat_corr_det_2
dw_folder.fu_AssignTab(2, "&Agenda", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_acc_mat_corr_lista.set_dw_key("cod_azienda")
dw_acc_mat_corr_lista.set_dw_key("anno_acc_materiali")
dw_acc_mat_corr_lista.set_dw_key("num_acc_materiali")
dw_acc_mat_corr_lista.set_dw_key("cod_corrispondenza")
dw_acc_mat_corr_lista.set_dw_key("data_corrispondenza")
dw_acc_mat_corr_lista.set_dw_key("ora_corrispondenza")
dw_acc_mat_corr_lista.set_dw_key("prog_corrispondenza")
dw_acc_mat_corr_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)

dw_acc_mat_corr_det_1.set_dw_options(sqlca,dw_acc_mat_corr_lista,c_sharedata+c_scrollparent,c_default)
dw_acc_mat_corr_det_2.set_dw_options(sqlca,dw_acc_mat_corr_lista,c_sharedata+c_scrollparent,c_default)

dw_folder.fu_SelectTab(1)
iuo_dw_main = dw_acc_mat_corr_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_acc_mat_corr_det_1,"cod_corrispondenza",sqlca,&
                 "tab_corrispondenze","cod_corrispondenza","des_corrispondenza",&
                 "tab_corrispondenze.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_acc_mat_corr_det_1,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )
					  
f_PO_LoadDDDW_DW(dw_acc_mat_corr_det_2,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_acc_mat_corr_det_2,"cod_tipo_agenda",sqlca,&
                 "tab_tipi_agende","cod_tipo_agenda","des_tipo_agenda",&
                 "tab_tipi_agende.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDLB_DW(dw_acc_mat_corr_det_2,"cod_utente",sqlca,&
                 "utenti","cod_utente","cod_utente",&
                 "utenti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  + "))")

end event

on w_acc_mat_corr.create
int iCurrent
call super::create
this.dw_acc_mat_corr_lista=create dw_acc_mat_corr_lista
this.cb_note_esterne=create cb_note_esterne
this.cb_controllo=create cb_controllo
this.dw_acc_mat_corr_det_1=create dw_acc_mat_corr_det_1
this.dw_acc_mat_corr_det_2=create dw_acc_mat_corr_det_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_acc_mat_corr_lista
this.Control[iCurrent+2]=this.cb_note_esterne
this.Control[iCurrent+3]=this.cb_controllo
this.Control[iCurrent+4]=this.dw_acc_mat_corr_det_1
this.Control[iCurrent+5]=this.dw_acc_mat_corr_det_2
this.Control[iCurrent+6]=this.dw_folder
end on

on w_acc_mat_corr.destroy
call super::destroy
destroy(this.dw_acc_mat_corr_lista)
destroy(this.cb_note_esterne)
destroy(this.cb_controllo)
destroy(this.dw_acc_mat_corr_det_1)
destroy(this.dw_acc_mat_corr_det_2)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;ib_in_new = false
integer li_row
li_row = dw_acc_mat_corr_lista.getrow()
if li_row <= 0 or dw_acc_mat_corr_lista.GetItemnumber(li_row,"prog_corrispondenza") = 0 then
	cb_note_esterne.enabled = false
	cb_controllo.enabled = false
else
	cb_note_esterne.enabled = true
	cb_controllo.enabled = true
end if
end event

type dw_acc_mat_corr_lista from uo_cs_xx_dw within w_acc_mat_corr
integer x = 23
integer y = 20
integer width = 2331
integer height = 500
integer taborder = 10
string dataobject = "d_acc_mat_corr_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_acc_materiali, ll_num_acc_materiali

ll_anno_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_acc_materiali")
ll_num_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_acc_materiali")
l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_acc_materiali, ll_num_acc_materiali)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;integer li_anno_acc_materiali
long ll_num_acc_materiali, l_Idx

li_anno_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_acc_materiali")
ll_num_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_acc_materiali")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(getitemnumber(l_Idx, "anno_acc_materiali")) or getitemnumber(l_Idx, "anno_acc_materiali") = 0 THEN
      SetItem(l_Idx, "anno_acc_materiali", li_anno_acc_materiali)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(getitemnumber(l_Idx, "num_acc_materiali")) or getitemnumber(l_Idx, "num_acc_materiali") = 0 THEN
      SetItem(l_Idx, "num_acc_materiali", ll_num_acc_materiali)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
cb_controllo.enabled = false
ib_in_new = false
end event

event pcd_new;call super::pcd_new;string ls_str
long ll_anno_acc_materiali, ll_num_acc_materiali
datetime ldt_oggi
time lt_ora

ll_anno_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_acc_materiali")
ll_num_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_acc_materiali")

dw_acc_mat_corr_lista.setitem(getrow(),"anno_acc_materiali",ll_anno_acc_materiali)
dw_acc_mat_corr_lista.setitem(getrow(),"num_acc_materiali",ll_num_acc_materiali)

cb_note_esterne.enabled = false
cb_controllo.enabled = false
ib_in_new = true
ii_new_row = getrow()

ls_str = string(now(),"hh:mm")
ldt_oggi = datetime(date(s_cs_xx.db_funzioni.data_neutra), time(ls_str))
this.setitem(this.getrow(),"ora_corrispondenza",ldt_oggi)

ldt_oggi = datetime(today())
this.setitem(this.getrow(),"data_corrispondenza",ldt_oggi)

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not GetItemnumber(li_row,"prog_corrispondenza") = 0 then
	cb_note_esterne.enabled = true
	cb_controllo.enabled = true
else
	cb_note_esterne.enabled = false
	cb_controllo.enabled = false
end if
ib_in_new = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not GetItemnumber(li_row,"prog_corrispondenza") = 0 then
	cb_note_esterne.enabled = true
	cb_controllo.enabled = true
else
	cb_note_esterne.enabled = false
	cb_controllo.enabled = false
end if
ib_in_new = false
end event

event updatestart;call super::updatestart;integer li_anno_acc_materiali, li_i
long ll_num_acc_materiali, ll_prog_corrispondenza
string ls_cod_corrispondenza, ls_cod_tipo_agenda, ls_cod_utente
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza, ldt_data_agenda, ldt_ora_agenda

if ib_in_new then
	li_anno_acc_materiali = dw_acc_mat_corr_det_1.getitemnumber( dw_acc_mat_corr_det_1.getrow(), "anno_acc_materiali" )
	ll_num_acc_materiali = dw_acc_mat_corr_det_1.getitemnumber( dw_acc_mat_corr_det_1.getrow(), "num_acc_materiali" )
	ls_cod_corrispondenza = dw_acc_mat_corr_det_1.getitemstring( dw_acc_mat_corr_det_1.getrow(), "cod_corrispondenza" )
	ldt_data_corrispondenza = dw_acc_mat_corr_det_1.getitemdatetime( dw_acc_mat_corr_det_1.getrow(), "data_corrispondenza" )
	ldt_ora_corrispondenza = dw_acc_mat_corr_det_1.getitemdatetime( dw_acc_mat_corr_det_1.getrow(), "ora_corrispondenza" )
   
   select max(acc_materiali_corrispondenze.prog_corrispondenza)
		  into :ll_prog_corrispondenza
		  from acc_materiali_corrispondenze
		  where (acc_materiali_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
			  (acc_materiali_corrispondenze.anno_acc_materiali = :li_anno_acc_materiali) and
			  (acc_materiali_corrispondenze.num_acc_materiali = :ll_num_acc_materiali) and
			  (acc_materiali_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
			  (acc_materiali_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
			  (acc_materiali_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_prog_corrispondenza) then
      ll_prog_corrispondenza = 1
   else
      ll_prog_corrispondenza = ll_prog_corrispondenza + 1
   end if
   this.SetItem (this.GetRow ( ),"anno_acc_materiali", li_anno_acc_materiali)
   this.SetItem (this.GetRow ( ),"num_acc_materiali", ll_num_acc_materiali)
   this.SetItem (this.GetRow ( ),"cod_corrispondenza", ls_cod_corrispondenza)
   this.SetItem (this.GetRow ( ),"data_corrispondenza", ldt_data_corrispondenza)
   this.SetItem (this.GetRow ( ),"ora_corrispondenza", ldt_ora_corrispondenza)
   this.SetItem (this.GetRow ( ),"prog_corrispondenza", ll_prog_corrispondenza)

   ib_in_new = false
end if

end event

event updateend;call super::updateend;boolean lb_nuovo_appuntamento = false
string ls_cod_tipo_agenda, ls_cod_utente, ls_sql, ls_where, ls_cod_corrispondenza
long ll_i, ll_rows, ll_anno_acc_materiali, ll_num_acc_materiali
datetime ldt_data, ldt_data_old, ldt_ora, ldt_ora_old, ldt_data_corrispondenza, &
         ldt_ora_corrispondenza, ldt_data_agenda, ldt_ora_agenda
time lt_1

ll_rows = rowcount()
if ll_rows < 1 then return
for ll_i = 1 to ll_rows
	ldt_data = getitemdatetime(ll_i,"data_pros_corrispondenza")
	ldt_data_old = getitemdatetime(ll_i,"data_pros_corrispondenza",primary!,true)
	lt_1 = time(getitemdatetime(ll_i,"ora_pros_corrispondenza"))
	ldt_ora = datetime(date(s_cs_xx.db_funzioni.data_neutra), lt_1)
	lt_1 = time(getitemdatetime(ll_i,"ora_pros_corrispondenza",primary!,true))
	ldt_ora_old = datetime(date(s_cs_xx.db_funzioni.data_neutra), lt_1)
	ll_anno_acc_materiali = getitemnumber(ll_i, "anno_acc_materiali")
	ll_num_acc_materiali = getitemnumber(ll_i, "num_acc_materiali")
	ls_cod_corrispondenza = getitemstring(ll_i, "cod_corrispondenza")
	ldt_data_corrispondenza = getitemdatetime(ll_i,"data_corrispondenza")
	ldt_ora_corrispondenza = getitemdatetime(ll_i,"ora_corrispondenza")

	if ll_i = ii_new_row then
		if f_crea_appuntamento(ldt_data, ldt_ora, "", getitemstring(ll_i,"note"), ls_cod_tipo_agenda, ls_cod_utente) = 0 then
			ls_where   = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + & 
							 "      anno_acc_materiali = " + string(ll_anno_acc_materiali) + " and " + & 
							 "      num_acc_materiali = " + string(ll_num_acc_materiali) + " and " + & 
							 "      cod_corrispondenza = '" + ls_cod_corrispondenza + "' and " + & 
							 "      data_corrispondenza = '" + string(ldt_data_corrispondenza, s_cs_xx.db_funzioni.formato_data) + "' and " + & 
							 "      ora_corrispondenza = '" + string(ldt_ora_corrispondenza, "yyyy-mm-dd hh:mm") + "' " 		
			ls_sql = "update acc_materiali_corrispondenze" + &
						" set cod_tipo_agenda = '" + ls_cod_tipo_agenda + "'," + &
						"    cod_utente = '" + ls_cod_utente + "'," + &
						"    data_agenda = '" + string(ldt_data,s_cs_xx.db_funzioni.formato_data) + "'," + &
						"    ora_agenda = '" + string(ldt_ora, "yyyy-mm-dd hh:mm") + "'" + &
						ls_where			
			EXECUTE IMMEDIATE :ls_sql ;								  
		end if	
	end if	
	
	if not(isnull(ldt_data)) or not(isnull(ldt_data_old)) then
		if not isnull(ldt_data_old) then
			if ldt_data_old <> ldt_data then
				// elimina appuntamento segnalando e verificando la nota se è uguale
			end if
		end if
		if not isnull(ldt_data) then
			if ldt_data_old <> ldt_data then
				if f_crea_appuntamento(ldt_data, ldt_ora, "",getitemstring(ll_i,"note"), ls_cod_tipo_agenda, ls_cod_utente) = 0 then
					ls_where=" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + & 
								"      anno_acc_materiali = " + string(ll_anno_acc_materiali) + " and " + & 
								"      num_acc_materiali = " + string(ll_num_acc_materiali) + " and " + & 
								"      cod_corrispondenza = '" + ls_cod_corrispondenza + "' and " + & 
								"      data_corrispondenza = '" + string(ldt_data_corrispondenza, s_cs_xx.db_funzioni.formato_data) + "' and " + & 
								"      ora_corrispondenza = '" + string(ldt_ora_corrispondenza, "yyyy-mm-dd hh:mm") + "' " 		
					ls_sql = "update acc_materiali_corrispondenze" + &
								" set cod_tipo_agenda = '" + ls_cod_tipo_agenda + "'," + &
								"    cod_utente = '" + ls_cod_utente + "'," + &
								"    data_agenda = '" + string(ldt_data,s_cs_xx.db_funzioni.formato_data) + "'," + &
								"    ora_agenda = '" + string(ldt_ora, "yyyy-mm-dd hh:mm") + "'" + &
								ls_where			
					EXECUTE IMMEDIATE :ls_sql ;								  
				end if
			else
				if ldt_ora_old <> ldt_ora then
				// cambia orario
			end if
			end if
		end if
	end if
next

setnull(ii_new_row)

for ll_i = 1 to this.deletedcount()
	ls_cod_tipo_agenda = this.getitemstring(ll_i, "cod_tipo_agenda", delete!, true)
	ls_cod_utente = this.getitemstring(ll_i, "cod_utente", delete!, true)
	ldt_data_agenda= this.getitemdatetime(ll_i, "data_agenda", delete!, true)		
	ldt_ora_agenda= this.getitemdatetime(ll_i, "ora_agenda", delete!, true)		
	if not isnull(ls_cod_tipo_agenda) then
	  delete
	  from  agende_corrispondenze  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende_note  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende_utenti
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	end if
next

end event

type cb_note_esterne from commandbutton within w_acc_mat_corr
event clicked pbm_bnclicked
integer x = 1577
integer y = 1340
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;integer li_anno_acc_materiali, li_risposta
long ll_num_acc_materiali, l_Idx, ll_prog_mimetype
string ls_cod_corrispondenza, ls_db, ls_doc
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza
blob lbl_null, lbl_blob
transaction sqlcb

	
li_anno_acc_materiali = dw_acc_mat_corr_det_1.getitemnumber( dw_acc_mat_corr_det_1.getrow(), "anno_acc_materiali" )
ll_num_acc_materiali = dw_acc_mat_corr_det_1.getitemnumber( dw_acc_mat_corr_det_1.getrow(), "num_acc_materiali" )
ls_cod_corrispondenza = dw_acc_mat_corr_det_1.getitemstring( dw_acc_mat_corr_det_1.getrow(), "cod_corrispondenza" )
ldt_data_corrispondenza = dw_acc_mat_corr_det_1.getitemdatetime( dw_acc_mat_corr_det_1.getrow(), "data_corrispondenza" )
ldt_ora_corrispondenza = dw_acc_mat_corr_det_1.getitemdatetime( dw_acc_mat_corr_det_1.getrow(), "ora_corrispondenza" )

setnull(lbl_null)

selectblob acc_materiali_corrispondenze.note_esterne
into :lbl_blob
from acc_materiali_corrispondenze
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_acc_materiali = :li_anno_acc_materiali and 
	num_acc_materiali = :ll_num_acc_materiali and 
	cod_corrispondenza = :ls_cod_corrispondenza and
	data_corrispondenza = :ldt_data_corrispondenza and
	ora_corrispondenza = :ldt_ora_corrispondenza;
	
select prog_mimetype
into :ll_prog_mimetype
from acc_materiali_corrispondenze
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_acc_materiali = :li_anno_acc_materiali and 
	num_acc_materiali = :ll_num_acc_materiali and 
	cod_corrispondenza = :ls_cod_corrispondenza and
	data_corrispondenza = :ldt_data_corrispondenza and
	ora_corrispondenza = :ldt_ora_corrispondenza;
	
ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update acc_materiali_corrispondenze
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_acc_materiali = :li_anno_acc_materiali and 
			num_acc_materiali = :ll_num_acc_materiali and 
			cod_corrispondenza = :ls_cod_corrispondenza and
			data_corrispondenza = :ldt_data_corrispondenza and
			ora_corrispondenza = :ldt_ora_corrispondenza;
			  
	else
		updateblob acc_materiali_corrispondenze
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_acc_materiali = :li_anno_acc_materiali and 
			num_acc_materiali = :ll_num_acc_materiali and 
			cod_corrispondenza = :ls_cod_corrispondenza and
			data_corrispondenza = :ldt_data_corrispondenza and
			ora_corrispondenza = :ldt_ora_corrispondenza;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update acc_materiali_corrispondenze
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_acc_materiali = :li_anno_acc_materiali and 
		num_acc_materiali = :ll_num_acc_materiali and 
		cod_corrispondenza = :ls_cod_corrispondenza and
		data_corrispondenza = :ldt_data_corrispondenza and
		ora_corrispondenza = :ldt_ora_corrispondenza;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
end if

// 11-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//
//if ls_db = "MSSQL" then
//	
//	li_risposta = f_crea_sqlcb(sqlcb)
//
//	selectblob acc_materiali_corrispondenze.note_esterne
//		into       :s_cs_xx.parametri.parametro_bl_1
//		from       acc_materiali_corrispondenze
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_acc_materiali = :li_anno_acc_materiali and 
//					  num_acc_materiali = :ll_num_acc_materiali and 
//					  cod_corrispondenza = :ls_cod_corrispondenza and
//					  data_corrispondenza = :ldt_data_corrispondenza and
//					  ora_corrispondenza = :ldt_ora_corrispondenza
//		using      sqlcb;
//
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob acc_materiali_corrispondenze.note_esterne
//		into       :s_cs_xx.parametri.parametro_bl_1
//		from       acc_materiali_corrispondenze
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_acc_materiali = :li_anno_acc_materiali and 
//					  num_acc_materiali = :ll_num_acc_materiali and 
//					  cod_corrispondenza = :ls_cod_corrispondenza and
//					  data_corrispondenza = :ldt_data_corrispondenza and
//					  ora_corrispondenza = :ldt_ora_corrispondenza;
//
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//
//		updateblob acc_materiali_corrispondenze
//		set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_acc_materiali = :li_anno_acc_materiali and 
//					  num_acc_materiali = :ll_num_acc_materiali and 
//					  cod_corrispondenza = :ls_cod_corrispondenza and
//					  data_corrispondenza = :ldt_data_corrispondenza and
//					  ora_corrispondenza = :ldt_ora_corrispondenza
//		using      sqlcb;		
//		
//		destroy sqlcb;
//		
//	else
//		
//		updateblob acc_materiali_corrispondenze
//		set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_acc_materiali = :li_anno_acc_materiali and 
//					  num_acc_materiali = :ll_num_acc_materiali and 
//					  cod_corrispondenza = :ls_cod_corrispondenza and
//					  data_corrispondenza = :ldt_data_corrispondenza and
//					  ora_corrispondenza = :ldt_ora_corrispondenza;
//					  
//	end if
//	
//// fine modifiche	
//	
//	commit;
//end if
end event

type cb_controllo from commandbutton within w_acc_mat_corr
event clicked pbm_bnclicked
integer x = 1989
integer y = 1340
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&ontrollo"
end type

event clicked;long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_num_acc_materiali
integer li_anno_acc_materiali
string ls_cod_corrispondenza
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza
   
ll_i[1] = dw_acc_mat_corr_lista.getrow()
if isnull(dw_acc_mat_corr_det_1.getitemnumber(ll_i[1], "num_reg_lista")) or &
   	dw_acc_mat_corr_det_1.getitemnumber(ll_i[1], "num_reg_lista") = 0 then
      g_mb.messagebox("Schede Intervento","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione interventi", StopSign!)
      return
end if

ll_num_reg_lista = dw_acc_mat_corr_det_1.getitemnumber(ll_i[1],"num_reg_lista") 
ll_prog_liste_con_comp = dw_acc_mat_corr_det_1.getitemnumber(ll_i[1],"prog_liste_con_comp") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

	li_anno_acc_materiali = dw_acc_mat_corr_det_1.getitemnumber( dw_acc_mat_corr_det_1.getrow(), "anno_acc_materiali" )
	ll_num_acc_materiali = dw_acc_mat_corr_det_1.getitemnumber( dw_acc_mat_corr_det_1.getrow(), "num_acc_materiali" )
	ls_cod_corrispondenza = dw_acc_mat_corr_det_1.getitemstring( dw_acc_mat_corr_det_1.getrow(), "cod_corrispondenza" )
	ldt_data_corrispondenza = dw_acc_mat_corr_det_1.getitemdatetime( dw_acc_mat_corr_det_1.getrow(), "data_corrispondenza" )
	ldt_ora_corrispondenza = dw_acc_mat_corr_det_1.getitemdatetime( dw_acc_mat_corr_det_1.getrow(), "ora_corrispondenza" )

	update acc_materiali_corrispondenze
	set    acc_materiali_corrispondenze.num_versione = :ll_num_versione,
			 acc_materiali_corrispondenze.num_edizione = :ll_num_edizione,
			 acc_materiali_corrispondenze.num_reg_lista = :ll_num_reg_lista,
			 acc_materiali_corrispondenze.prog_liste_con_comp = :ll_prog_liste_con_comp
	where (acc_materiali_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
		  (acc_materiali_corrispondenze.anno_acc_materiali = :li_anno_acc_materiali) and
		  (acc_materiali_corrispondenze.num_acc_materiali = :ll_num_acc_materiali) and
		  (acc_materiali_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
		  (acc_materiali_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
		  (acc_materiali_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		  
   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_acc_mat_corr_lista.triggerevent("pcd_retrieve")
   dw_acc_mat_corr_lista.set_selected_rows(1, &
                                                ll_i[], &
                                                c_ignorechanges, &
                                                c_refreshchildren, &
                                                c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_acc_mat_corr_lista)
end event

type dw_acc_mat_corr_det_1 from uo_cs_xx_dw within w_acc_mat_corr
integer x = 46
integer y = 640
integer width = 2240
integer height = 656
integer taborder = 30
string dataobject = "d_acc_mat_corr_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_str, ls_des_corrispondenza, ls_oggetto, ls_note, ls_gestione, ls_cod_corrispondenza
	long ll_anno_acc_materiali, ll_num_acc_materiali

	if i_colname = "cod_corrispondenza" then
		ls_cod_corrispondenza = i_coltext
	else
		ls_cod_corrispondenza = getitemstring(row, "cod_corrispondenza")
	end if

	setnull(ls_des_corrispondenza)
	if not isnull(ls_cod_corrispondenza) then
		select des_corrispondenza
		into   :ls_des_corrispondenza
		from   tab_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_corrispondenza = :ls_cod_corrispondenza;
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_corrispondenza)
		end if
	end if
	
	if i_colname = "oggetto" then
		ls_oggetto = i_coltext
	else
		ls_oggetto = getitemstring(row, "oggetto")
	end if
	
	ll_anno_acc_materiali = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_acc_materiali")
	ll_num_acc_materiali  = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_acc_materiali")
	
	if isnull(ls_des_corrispondenza) then
		ls_des_corrispondenza = ""
	else
		ls_des_corrispondenza = ls_des_corrispondenza + "~r~n"
	end if
	if isnull(ls_oggetto) then
		ls_oggetto = ""
	else
		ls_oggetto = ls_oggetto + "~r~n"
	end if

	ls_gestione = "Accettazione Materiali  "
	
ls_str = ls_note + &
			ls_des_corrispondenza + &
			ls_oggetto + &
			ls_gestione + &
			string(ll_anno_acc_materiali) + "/" + string(ll_num_acc_materiali)

setitem(row, "note", ls_str)
end if
end event

type dw_acc_mat_corr_det_2 from uo_cs_xx_dw within w_acc_mat_corr
integer x = 46
integer y = 640
integer width = 2240
integer height = 656
integer taborder = 20
string dataobject = "d_acc_mat_corr_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_acc_mat_corr
integer x = 23
integer y = 540
integer width = 2331
integer height = 780
integer taborder = 0
boolean border = false
end type

