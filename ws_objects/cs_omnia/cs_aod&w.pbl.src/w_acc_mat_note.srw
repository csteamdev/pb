﻿$PBExportHeader$w_acc_mat_note.srw
$PBExportComments$Finestra Note di Accettazione Materiali
forward
global type w_acc_mat_note from w_cs_xx_principale
end type
type dw_acc_mat_note_lista from uo_cs_xx_dw within w_acc_mat_note
end type
type dw_acc_mat_note_det from uo_cs_xx_dw within w_acc_mat_note
end type
type cb_note_esterne from commandbutton within w_acc_mat_note
end type
end forward

global type w_acc_mat_note from w_cs_xx_principale
integer width = 2455
integer height = 1388
string title = "Note Accettazione Materiali"
dw_acc_mat_note_lista dw_acc_mat_note_lista
dw_acc_mat_note_det dw_acc_mat_note_det
cb_note_esterne cb_note_esterne
end type
global w_acc_mat_note w_acc_mat_note

type variables
boolean ib_in_new
end variables

on w_acc_mat_note.create
int iCurrent
call super::create
this.dw_acc_mat_note_lista=create dw_acc_mat_note_lista
this.dw_acc_mat_note_det=create dw_acc_mat_note_det
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_acc_mat_note_lista
this.Control[iCurrent+2]=this.dw_acc_mat_note_det
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_acc_mat_note.destroy
call super::destroy
destroy(this.dw_acc_mat_note_lista)
destroy(this.dw_acc_mat_note_det)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;dw_acc_mat_note_lista.set_dw_key("cod_azienda")
dw_acc_mat_note_lista.set_dw_key("anno_acc_materiali")
dw_acc_mat_note_lista.set_dw_key("num_acc_materiali")
dw_acc_mat_note_lista.set_dw_key("cod_nota")
dw_acc_mat_note_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)

dw_acc_mat_note_det.set_dw_options(sqlca,dw_acc_mat_note_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_acc_mat_note_lista
end event

event pc_delete;call super::pc_delete;ib_in_new = false
integer li_row
li_row = dw_acc_mat_note_lista.getrow()
if li_row <= 0 or isnull(dw_acc_mat_note_lista.GetItemstring(li_row,"cod_nota")) then
	cb_note_esterne.enabled = false
else
	cb_note_esterne.enabled = true
end if
end event

type dw_acc_mat_note_lista from uo_cs_xx_dw within w_acc_mat_note
integer x = 23
integer y = 20
integer width = 1989
integer height = 500
integer taborder = 10
string dataobject = "d_acc_mat_note_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;integer li_anno_acc_materiali
long ll_num_acc_materiali

li_anno_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_acc_materiali")
ll_num_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_acc_materiali")

dw_acc_mat_note_lista.setitem(getrow(),"anno_acc_materiali",li_anno_acc_materiali)
dw_acc_mat_note_lista.setitem(getrow(),"num_acc_materiali",ll_num_acc_materiali)

cb_note_esterne.enabled = false
ib_in_new = true
end event

event pcd_retrieve;call super::pcd_retrieve;integer li_anno_acc_materiali
long ll_num_acc_materiali, l_ERROR

li_anno_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_acc_materiali")
ll_num_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_acc_materiali")

l_Error = Retrieve(s_cs_xx.cod_azienda, li_anno_acc_materiali, ll_num_acc_materiali )

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;integer li_anno_acc_materiali
long ll_num_acc_materiali, l_Idx

li_anno_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_acc_materiali")
ll_num_acc_materiali = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_acc_materiali")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(getitemnumber(l_Idx, "anno_acc_materiali")) or getitemnumber(l_Idx, "anno_acc_materiali") = 0 THEN
      SetItem(l_Idx, "anno_acc_materiali", li_anno_acc_materiali)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(getitemnumber(l_Idx, "num_acc_materiali")) or getitemnumber(l_Idx, "num_acc_materiali") = 0 THEN
      SetItem(l_Idx, "num_acc_materiali", ll_num_acc_materiali)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
ib_in_new = false
end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_nota")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_in_new = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_nota")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_in_new = false
end event

type dw_acc_mat_note_det from uo_cs_xx_dw within w_acc_mat_note
integer x = 23
integer y = 540
integer width = 2377
integer height = 720
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_acc_mat_note_det"
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from commandbutton within w_acc_mat_note
event clicked pbm_bnclicked
integer x = 2034
integer y = 20
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;integer li_anno_acc_materiali, li_risposta
long ll_num_acc_materiali, l_Idx
string ls_cod_nota, ls_db
blob lbl_null
transaction sqlcb

setnull(lbl_null)
li_anno_acc_materiali = dw_acc_mat_note_lista.getitemnumber(dw_acc_mat_note_lista.getrow(), "anno_acc_materiali")
ll_num_acc_materiali = dw_acc_mat_note_lista.getitemnumber(dw_acc_mat_note_lista.getrow(), "num_acc_materiali")
ls_cod_nota = dw_acc_mat_note_lista.getitemstring(dw_acc_mat_note_lista.getrow(), "cod_nota")

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob acc_materiali_note.note_esterne
		into       :s_cs_xx.parametri.parametro_bl_1
		from       acc_materiali_note
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_acc_materiali = :li_anno_acc_materiali and 
					  num_acc_materiali = :ll_num_acc_materiali and 
					  cod_nota = :ls_cod_nota
		using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if	
	
	destroy sqlcb;
	
else

	selectblob acc_materiali_note.note_esterne
		into       :s_cs_xx.parametri.parametro_bl_1
		from       acc_materiali_note
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_acc_materiali = :li_anno_acc_materiali and 
					  num_acc_materiali = :ll_num_acc_materiali and 
					  cod_nota = :ls_cod_nota;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)

		updateblob acc_materiali_note
		set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_acc_materiali = :li_anno_acc_materiali and 
					  num_acc_materiali = :ll_num_acc_materiali and 
					  cod_nota = :ls_cod_nota
		using      sqlcb;		
		
		destroy    sqlcb;		
		
	else
		
		updateblob acc_materiali_note
		set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_acc_materiali = :li_anno_acc_materiali and 
					  num_acc_materiali = :ll_num_acc_materiali and 
					  cod_nota = :ls_cod_nota;

   end if
	
// fine modifiche	
	
   commit;
end if

end event

