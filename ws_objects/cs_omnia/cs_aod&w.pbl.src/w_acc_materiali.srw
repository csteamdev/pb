﻿$PBExportHeader$w_acc_materiali.srw
$PBExportComments$Gestione Accettazione Materiali
forward
global type w_acc_materiali from w_cs_xx_principale
end type
type cb_nc from commandbutton within w_acc_materiali
end type
type cb_report_camp from commandbutton within w_acc_materiali
end type
type cb_annulla from commandbutton within w_acc_materiali
end type
type cb_ricerca from commandbutton within w_acc_materiali
end type
type cb_dati_tecnici from commandbutton within w_acc_materiali
end type
type cb_eti_acc from commandbutton within w_acc_materiali
end type
type cb_etic_att from commandbutton within w_acc_materiali
end type
type cb_1 from commandbutton within w_acc_materiali
end type
type cb_corrispondenze from commandbutton within w_acc_materiali
end type
type cb_note from commandbutton within w_acc_materiali
end type
type cb_ric_stock from cb_stock_ricerca within w_acc_materiali
end type
type cb_tes_campionamenti from commandbutton within w_acc_materiali
end type
type cb_doc_acc from cb_documenti_compilati within w_acc_materiali
end type
type cb_doc_comp from cb_documenti_compilati within w_acc_materiali
end type
type cb_cancella_camp from commandbutton within w_acc_materiali
end type
type st_semaforo from statictext within w_acc_materiali
end type
type dw_ricerca from u_dw_search within w_acc_materiali
end type
type dw_folder from u_folder within w_acc_materiali
end type
type dw_acc_materiali_lista from uo_cs_xx_dw within w_acc_materiali
end type
type dw_acc_materiali_3 from uo_cs_xx_dw within w_acc_materiali
end type
type tab_folder from tab within w_acc_materiali
end type
type tabpage_1 from userobject within tab_folder
end type
type tabpage_1 from userobject within tab_folder
end type
type tabpage_2 from userobject within tab_folder
end type
type tabpage_2 from userobject within tab_folder
end type
type tabpage_3 from userobject within tab_folder
end type
type tabpage_3 from userobject within tab_folder
end type
type tab_folder from tab within w_acc_materiali
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
type dw_acc_materiali_2 from uo_cs_xx_dw within w_acc_materiali
end type
type dw_acc_materiali_1 from uo_cs_xx_dw within w_acc_materiali
end type
end forward

global type w_acc_materiali from w_cs_xx_principale
integer width = 2889
integer height = 1856
string title = "Accettazione Materiali"
cb_nc cb_nc
cb_report_camp cb_report_camp
cb_annulla cb_annulla
cb_ricerca cb_ricerca
cb_dati_tecnici cb_dati_tecnici
cb_eti_acc cb_eti_acc
cb_etic_att cb_etic_att
cb_1 cb_1
cb_corrispondenze cb_corrispondenze
cb_note cb_note
cb_ric_stock cb_ric_stock
cb_tes_campionamenti cb_tes_campionamenti
cb_doc_acc cb_doc_acc
cb_doc_comp cb_doc_comp
cb_cancella_camp cb_cancella_camp
st_semaforo st_semaforo
dw_ricerca dw_ricerca
dw_folder dw_folder
dw_acc_materiali_lista dw_acc_materiali_lista
dw_acc_materiali_3 dw_acc_materiali_3
tab_folder tab_folder
dw_acc_materiali_2 dw_acc_materiali_2
dw_acc_materiali_1 dw_acc_materiali_1
end type
global w_acc_materiali w_acc_materiali

type variables
boolean ib_in_new=false
boolean stato=false
boolean ib_ord_acq=false, ib_bol_acq=false, ib_fat_acq=false
end variables

forward prototypes
public function integer wf_semaforo (string ws_cod_fornitore)
public function integer wf_genera_campionamento (ref long fl_anno_reg_campionamento, ref long fl_num_reg_campionamento)
end prototypes

public function integer wf_semaforo (string ws_cod_fornitore);string ls_flag_omologato, ls_procedure_speciali, ls_flag_certificato, ls_flag_approvato

if isnull(ws_cod_fornitore) then return 0

select anag_fornitori.flag_omologato,
       anag_fornitori.flag_procedure_speciali, 
		 anag_fornitori.flag_certificato, 
		 anag_fornitori.flag_approvato
into   :ls_flag_omologato, 
       :ls_procedure_speciali, 
		 :ls_flag_certificato, 
		 :ls_flag_approvato
from   anag_fornitori
where  (anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
		 (anag_fornitori.cod_fornitore = :ws_cod_fornitore) ;
		 
if sqlca.sqlcode <> 0 then return -1		 
		 
if ls_flag_certificato = "S" then st_semaforo.backcolor = 65280
if ls_procedure_speciali = "S" then st_semaforo.backcolor = 65535
if (ls_flag_approvato = "N") or (ls_flag_omologato = "N") then st_semaforo.backcolor = 255
return 0
end function

public function integer wf_genera_campionamento (ref long fl_anno_reg_campionamento, ref long fl_num_reg_campionamento);integer  li_row
string   ls_cod_prodotto, ls_cod_piano_campionamento, ls_cod_deposito, ls_cod_ubicazione, &
		   ls_cod_lotto, ls_note
datetime ldt_data_stock, ldt_data_validita
long     ll_prog_stock, ll_num_reg_campionamenti, ll_anno_reg_campionamenti, ll_anno_acc_materiali, &
	      ll_num_acc_materiali,ll_prog_piano_campionamento

li_row = dw_acc_materiali_lista.getrow()
ls_cod_prodotto = dw_acc_materiali_lista.getitemstring(li_row, "cod_prodotto")

declare cur_piani_camp cursor for
  select tes_piani_campionamento.cod_piano_campionamento, 
		   tes_piani_campionamento.data_validita,
			tes_piani_campionamento.prog_piano_campionamento
    from tes_piani_campionamento,
         det_piani_campionamento
   where ( det_piani_campionamento.cod_azienda = tes_piani_campionamento.cod_azienda ) and
         ( det_piani_campionamento.cod_piano_campionamento = tes_piani_campionamento.cod_piano_campionamento ) and
         ( det_piani_campionamento.data_validita = tes_piani_campionamento.data_validita ) and
         ( det_piani_campionamento.prog_piano_campionamento = tes_piani_campionamento.prog_piano_campionamento ) and
         ( ( tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda ) and
         ( det_piani_campionamento.cod_prodotto = :ls_cod_prodotto ) )   ;

open cur_piani_camp;		

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Errore durante open cursore cur_piani_camp~r~n" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if	

fetch cur_piani_camp into :ls_cod_piano_campionamento, :ldt_data_validita, :ll_prog_piano_campionamento;
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Nessun Piano di Campionamento legato al Prodotto Corrente")
	close cur_piani_camp;
	return -1
elseif sqlca.sqlcode = -1 then
	g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Errore durante l'estrazione del Piano di Campionamento:" + sqlca.sqlerrtext,stopsign!)
	close cur_piani_camp;
	return -1
end if

close cur_piani_camp;

ls_cod_deposito = dw_acc_materiali_lista.getitemstring(li_row, "cod_deposito")
ls_cod_ubicazione = dw_acc_materiali_lista.getitemstring(li_row, "cod_ubicazione")
ls_cod_lotto = dw_acc_materiali_lista.getitemstring(li_row, "cod_lotto")
ldt_data_stock = dw_acc_materiali_lista.getitemdatetime(li_row, "data_stock")
ll_prog_stock = dw_acc_materiali_lista.getitemnumber(li_row, "prog_stock")

if isnull(ls_cod_deposito) or isnull(ls_cod_ubicazione) or isnull(ls_cod_lotto) or &
	isnull(ldt_data_stock) or isnull(ll_prog_stock) then
	g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Definire lo STOCK!")
	return -1
end if

ll_anno_reg_campionamenti = f_anno_esercizio()

select max(num_reg_campionamenti) 
into   :ll_num_reg_campionamenti 
from   tes_campionamenti 
where  cod_azienda=:s_cs_xx.cod_azienda  and
		 anno_reg_campionamenti=:ll_anno_reg_campionamenti; 

if isnull(ll_num_reg_campionamenti) then
	ll_num_reg_campionamenti = 1
else
	ll_num_reg_campionamenti++
end if

ll_anno_acc_materiali = dw_acc_materiali_lista.getitemnumber(li_row, "anno_acc_materiali")
ll_num_acc_materiali = dw_acc_materiali_lista.getitemnumber(li_row, "num_acc_materiali")
ls_note = "Rif.Acc.Mat.: " + string(ll_anno_acc_materiali) + "/" + string(ll_num_acc_materiali)

window_open(w_ext_dati_campionamento, 0)

if	isnull(s_cs_xx.parametri.parametro_data_1) then
	g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Annullata l'Operazione")
	return -1
end if

insert into tes_campionamenti
		( cod_azienda,
		  anno_reg_campionamenti,
		  num_reg_campionamenti,
		  des_campionamento,
		  data_registrazione,
		  cod_piano_campionamento,
		  data_validita,
		  prog_piano_campionamento,
		  cod_prodotto,
		  cod_deposito,
		  cod_ubicazione,
		  cod_lotto,
		  data_stock,
		  prog_stock,
		  cod_operaio,
		  livello_significativita,
		  note )  
values (:s_cs_xx.cod_azienda,   
		  :ll_anno_reg_campionamenti,   
		  :ll_num_reg_campionamenti,   
		  :s_cs_xx.parametri.parametro_s_1,   
		  :s_cs_xx.parametri.parametro_data_1,   
		  :ls_cod_piano_campionamento,   
		  :ldt_data_validita,   
		  :ll_prog_piano_campionamento,
		  :ls_cod_prodotto,   
		  :ls_cod_deposito,   
		  :ls_cod_ubicazione,   
		  :ls_cod_lotto,   
		  :ldt_data_stock,   
		  :ll_prog_stock,   
		  :s_cs_xx.parametri.parametro_s_2,   
		  0,   
		  :ls_note )  ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Errore Durante l'Inserimento:" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

update acc_materiali
  set anno_reg_campionamenti = :ll_anno_reg_campionamenti,   
		num_reg_campionamenti = :ll_num_reg_campionamenti,
		flag_esito_campionamento = 'A'
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_acc_materiali = :ll_anno_acc_materiali and
		num_acc_materiali = :ll_num_acc_materiali;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Errore Durante l'Aggiornamento Accettazione:" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_data_1)
setnull(s_cs_xx.parametri.parametro_data_2)

g_mb.messagebox("Creazione Campionamento da Acc. Materiali", "Creato Campionamento: " + &
				string(ll_anno_reg_campionamenti) + "/" + string(ll_num_reg_campionamenti))

fl_anno_reg_campionamento = ll_anno_reg_campionamenti
fl_num_reg_campionamento = ll_num_reg_campionamenti
return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_flag_rif_ord, ls_flag_rif_bol, ls_flag_rif_fat,l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[]

dw_acc_materiali_lista.set_dw_key("cod_azienda")
dw_acc_materiali_lista.set_dw_key("anno_acc_materiali")
dw_acc_materiali_lista.set_dw_key("num_acc_materiali")
dw_acc_materiali_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_acc_materiali_1.set_dw_options(sqlca,dw_acc_materiali_lista,c_sharedata+c_scrollparent,c_default)
dw_acc_materiali_2.set_dw_options(sqlca,dw_acc_materiali_lista,c_sharedata+c_scrollparent,c_default)
dw_acc_materiali_3.set_dw_options(sqlca,dw_acc_materiali_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_acc_materiali_lista

// -------------------------  imposto folder ricerca e filtro ---------------------------------------
l_criteriacolumn[1] = "anno_registrazione"
l_criteriacolumn[2] = "num_registrazione"
l_criteriacolumn[3] = "cod_fornitore"
l_criteriacolumn[4] = "cod_prodotto"
l_criteriacolumn[5] = "cod_deposito"
l_criteriacolumn[6] = "cod_lotto"
l_criteriacolumn[7] = "cod_area_aziendale"

l_searchtable[1] = "acc_materiali"
l_searchtable[2] = "acc_materiali"
l_searchtable[3] = "acc_materiali"
l_searchtable[4] = "acc_materiali"
l_searchtable[5] = "acc_materiali"
l_searchtable[6] = "acc_materiali"
l_searchtable[7] = "acc_materiali"

l_searchcolumn[1] = "anno_acc_materiali"
l_searchcolumn[2] = "num_acc_materiali"
l_searchcolumn[3] = "cod_fornitore"
l_searchcolumn[4] = "cod_prodotto"
l_searchcolumn[5] = "cod_deposito"
l_searchcolumn[6] = "cod_lotto"
l_searchcolumn[7] = "cod_area_aziendale"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_acc_materiali_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)
dw_folder.fu_folderoptions(dw_folder.c_defaultheight, &
                           dw_folder.c_foldertableft)
							
lw_oggetti[1] = dw_acc_materiali_lista
dw_folder.fu_assigntab(2, "L", lw_oggetti[])
lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = cb_ricerca
lw_oggetti[3] = cb_annulla
dw_folder.fu_assigntab(1, "R", lw_oggetti[])
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)
							
// --------------------------------------------------------------------------------------------------


SELECT parametri_omnia.flag  
INTO   :ls_flag_rif_ord  
FROM   parametri_omnia
WHERE  ( parametri_omnia.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( parametri_omnia.flag_parametro = 'F' ) AND  
       ( parametri_omnia.cod_parametro = 'GOA' )   ;
choose case sqlca.sqlcode
   case 100
   g_mb.messagebox("ERRORE", "Attenzione!! Non è stato caricato il parametro GOA in parametri omnia", StopSign!)   
case -1
   g_mb.messagebox("ERRORE", sqlca.sqlerrtext + "DURANTE RICERCA PARAMETRO GOA IN PARAMETRI OMNIA", StopSign!)   
end choose

if ls_flag_rif_ord = "S" then
   ib_ord_acq = true
else
   ib_ord_acq = false
end if

SELECT parametri_omnia.flag  
INTO   :ls_flag_rif_bol
FROM   parametri_omnia  
WHERE  ( parametri_omnia.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( parametri_omnia.flag_parametro = 'F' ) AND  
       ( parametri_omnia.cod_parametro = 'GBA' )   ;
choose case sqlca.sqlcode
   case 100
   g_mb.messagebox("ERRORE", "Attenzione!! Non è stato caricato il parametro GBA in parametri omnia", StopSign!)   
case -1
   g_mb.messagebox("ERRORE", sqlca.sqlerrtext + "DURANTE RICERCA PARAMETRO GBA IN PARAMETRI OMNIA", StopSign!)   
end choose

if ls_flag_rif_bol = "S" then
   ib_bol_acq = true
else
   ib_bol_acq = false
end if

SELECT parametri_omnia.flag  
INTO   :ls_flag_rif_fat
FROM   parametri_omnia
WHERE  ( parametri_omnia.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( parametri_omnia.flag_parametro = 'F' ) AND  
       ( parametri_omnia.cod_parametro = 'GFA' )   ;
choose case sqlca.sqlcode
   case 100
   g_mb.messagebox("ERRORE", "Attenzione!! Non è stato caricato il parametro GFA in parametri omnia", StopSign!)   
case -1
   g_mb.messagebox("ERRORE", sqlca.sqlerrtext + "DURANTE RICERCA PARAMETRO GFA IN PARAMETRI OMNIA", StopSign!)   
end choose

if ls_flag_rif_fat = "S" then
   ib_fat_acq = true
else
   ib_fat_acq = false
end if

if ib_ord_acq  then
   dw_acc_materiali_2.modify("st20.visible = 0")
   dw_acc_materiali_2.modify("rif_ord_acq.visible = 0")
else  
   dw_acc_materiali_2.modify("st1.visible = 0")
   dw_acc_materiali_2.modify("st2.visible = 0")
   dw_acc_materiali_2.modify("st3.visible = 0")
   dw_acc_materiali_2.modify("anno_registrazione.visible = 0")
   dw_acc_materiali_2.modify("num_registrazione.visible = 0")
   dw_acc_materiali_2.modify("prog_riga_ordine_acq.visible = 0")
end if

if ib_bol_acq  then
   dw_acc_materiali_2.modify("st21.visible = 0")
   dw_acc_materiali_2.modify("rif_bol_acq.visible = 0")
else  
   dw_acc_materiali_2.modify("st4.visible = 0")
   dw_acc_materiali_2.modify("st5.visible = 0")
   dw_acc_materiali_2.modify("st6.visible = 0")
   dw_acc_materiali_2.modify("anno_bolla_acq.visible = 0")
   dw_acc_materiali_2.modify("num_bolla_acq.visible = 0")
   dw_acc_materiali_2.modify("progressivo.visible = 0")
   dw_acc_materiali_2.modify("prog_riga_bolla_acq.visible = 0")
end if

if ib_fat_acq  then
   dw_acc_materiali_2.modify("st22.visible = 0")
   dw_acc_materiali_2.modify("rif_fat_acq.visible = 0")
else  
//   dw_acc_materiali_2.modify("st7.visible = 0")
//   dw_acc_materiali_2.modify("st8.visible = 0")
//   dw_acc_materiali_2.modify("st9.visible = 0")
//   dw_acc_materiali_2.modify("st10.visible = 0")
//   dw_acc_materiali_2.modify("st11.visible = 0")
//   dw_acc_materiali_2.modify("cf_cod_doc_origine.visible = 0")
//   dw_acc_materiali_2.modify("cod_doc_origine.visible = 0")
//   dw_acc_materiali_2.modify("numeratore_doc_origine.visible = 0")
//   dw_acc_materiali_2.modify("anno_doc_origine.visible = 0")
//   dw_acc_materiali_2.modify("num_doc_origine.visible = 0")
//   dw_acc_materiali_2.modify("progressivo_fat_acq.visible = 0")
//   dw_acc_materiali_2.modify("prog_riga_fat_acq.visible = 0")
end if

dw_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())

end event

event pc_setddlb;call super::pc_setddlb;dw_ricerca.fu_loadcode("cod_deposito", &
							  "anag_depositi", &
							  "cod_deposito", &
							  "des_deposito", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' order by cod_deposito asc", "(Tutti)" )

f_PO_LoadDDDW_DW(dw_ricerca,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_acc_materiali_lista,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_fornitore",sqlca,&
//                 "anag_fornitori","cod_fornitore","rag_soc_1","anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDLB_DW(dw_acc_materiali_2,"anno_bolla_acq",sqlca,&
                 "tes_bol_acq","anno_bolla_acq","anno_bolla_acq",&
                 "(tes_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                  (tes_bol_acq.cod_fornitore = '"+ s_cs_xx.parametri.parametro_s_1 + "')")

f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event open;call super::open;//ib_x = false
end event

on w_acc_materiali.create
int iCurrent
call super::create
this.cb_nc=create cb_nc
this.cb_report_camp=create cb_report_camp
this.cb_annulla=create cb_annulla
this.cb_ricerca=create cb_ricerca
this.cb_dati_tecnici=create cb_dati_tecnici
this.cb_eti_acc=create cb_eti_acc
this.cb_etic_att=create cb_etic_att
this.cb_1=create cb_1
this.cb_corrispondenze=create cb_corrispondenze
this.cb_note=create cb_note
this.cb_ric_stock=create cb_ric_stock
this.cb_tes_campionamenti=create cb_tes_campionamenti
this.cb_doc_acc=create cb_doc_acc
this.cb_doc_comp=create cb_doc_comp
this.cb_cancella_camp=create cb_cancella_camp
this.st_semaforo=create st_semaforo
this.dw_ricerca=create dw_ricerca
this.dw_folder=create dw_folder
this.dw_acc_materiali_lista=create dw_acc_materiali_lista
this.dw_acc_materiali_3=create dw_acc_materiali_3
this.tab_folder=create tab_folder
this.dw_acc_materiali_2=create dw_acc_materiali_2
this.dw_acc_materiali_1=create dw_acc_materiali_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_nc
this.Control[iCurrent+2]=this.cb_report_camp
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_ricerca
this.Control[iCurrent+5]=this.cb_dati_tecnici
this.Control[iCurrent+6]=this.cb_eti_acc
this.Control[iCurrent+7]=this.cb_etic_att
this.Control[iCurrent+8]=this.cb_1
this.Control[iCurrent+9]=this.cb_corrispondenze
this.Control[iCurrent+10]=this.cb_note
this.Control[iCurrent+11]=this.cb_ric_stock
this.Control[iCurrent+12]=this.cb_tes_campionamenti
this.Control[iCurrent+13]=this.cb_doc_acc
this.Control[iCurrent+14]=this.cb_doc_comp
this.Control[iCurrent+15]=this.cb_cancella_camp
this.Control[iCurrent+16]=this.st_semaforo
this.Control[iCurrent+17]=this.dw_ricerca
this.Control[iCurrent+18]=this.dw_folder
this.Control[iCurrent+19]=this.dw_acc_materiali_lista
this.Control[iCurrent+20]=this.dw_acc_materiali_3
this.Control[iCurrent+21]=this.tab_folder
this.Control[iCurrent+22]=this.dw_acc_materiali_2
this.Control[iCurrent+23]=this.dw_acc_materiali_1
end on

on w_acc_materiali.destroy
call super::destroy
destroy(this.cb_nc)
destroy(this.cb_report_camp)
destroy(this.cb_annulla)
destroy(this.cb_ricerca)
destroy(this.cb_dati_tecnici)
destroy(this.cb_eti_acc)
destroy(this.cb_etic_att)
destroy(this.cb_1)
destroy(this.cb_corrispondenze)
destroy(this.cb_note)
destroy(this.cb_ric_stock)
destroy(this.cb_tes_campionamenti)
destroy(this.cb_doc_acc)
destroy(this.cb_doc_comp)
destroy(this.cb_cancella_camp)
destroy(this.st_semaforo)
destroy(this.dw_ricerca)
destroy(this.dw_folder)
destroy(this.dw_acc_materiali_lista)
destroy(this.dw_acc_materiali_3)
destroy(this.tab_folder)
destroy(this.dw_acc_materiali_2)
destroy(this.dw_acc_materiali_1)
end on

type cb_nc from commandbutton within w_acc_materiali
integer x = 2469
integer y = 940
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Non Conf."
end type

event clicked;datetime ldt_data_stock

long     ll_row, ll_anno, ll_num, ll_anno_nc, ll_num_nc, ll_prog_stock, ll_prog_lotto

string   ls_cod_resp, ls_cod_fornitore, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto


select cod_resp_divisione
into   :ls_cod_resp
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_apertura_nc = 'S';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_resp) then
	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato all'apertura delle non conformità")
	return -1
end if

ll_anno_nc = dw_acc_materiali_lista.getitemnumber(dw_acc_materiali_lista.getrow(),"anno_non_conf")

ll_num_nc = dw_acc_materiali_lista.getitemnumber(dw_acc_materiali_lista.getrow(),"num_non_conf")

ls_cod_fornitore = dw_acc_materiali_lista.getitemstring(dw_acc_materiali_lista.getrow(),"cod_fornitore")

ls_cod_prodotto = dw_acc_materiali_lista.getitemstring(dw_acc_materiali_lista.getrow(),"cod_prodotto")

ls_cod_deposito = dw_acc_materiali_lista.getitemstring(dw_acc_materiali_lista.getrow(),"cod_deposito")

ls_cod_ubicazione = dw_acc_materiali_lista.getitemstring(dw_acc_materiali_lista.getrow(),"cod_ubicazione")

ls_cod_lotto = dw_acc_materiali_lista.getitemstring(dw_acc_materiali_lista.getrow(),"cod_lotto")

ll_prog_stock = dw_acc_materiali_lista.getitemnumber(dw_acc_materiali_lista.getrow(),"prog_stock")

ldt_data_stock = dw_acc_materiali_lista.getitemdatetime(dw_acc_materiali_lista.getrow(),"data_stock")

if isnull(ll_anno_nc) and isnull(ll_num_nc) then
	
	ll_anno_nc = year(today())
	
	select max(num_non_conf)
	into   :ll_num_nc
	from   non_conformita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_non_conf = :ll_anno_nc;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select di non_conformita: " + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 or isnull(ll_num_nc) then
		ll_num_nc = 0
	end if
	
	ll_num_nc++
	
	/*
	insert
	into   non_conformita
			 (cod_azienda,
			 anno_non_conf,
			 num_non_conf,
			 flag_tipo_nc,
			 cod_fornitore,
			 cod_prodotto,
			 cod_deposito,
			 cod_ubicazione,
			 cod_lotto,
			 prog_stock,
			 data_stock)
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_nc,
			 :ll_num_nc,
			 'A',
			 :ls_cod_fornitore,
			 :ls_cod_prodotto,
			 :ls_cod_deposito,
			 :ls_cod_ubicazione,
			 :ls_cod_lotto,
			 :ll_prog_stock,
			 :ldt_data_stock);
	*/
	insert
	into   non_conformita
			 (cod_azienda,
			 anno_non_conf,
			 num_non_conf,
			 flag_tipo_nc,
			 cod_fornitore,
			 cod_prodotto)
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_nc,
			 :ll_num_nc,
			 'A',
			 :ls_cod_fornitore,
			 :ls_cod_prodotto);
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella insert di non_conformita: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	//inserimento stock
	if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) and &
		ls_cod_deposito <> "" and not isnull(ls_cod_deposito) and &
		ls_cod_ubicazione <> "" and not isnull(ls_cod_ubicazione) and &
		ls_cod_lotto <> "" and not isnull(ls_cod_lotto) and &
		not isnull(ll_prog_stock) and not isnull(ldt_data_stock) then
		
		insert into non_conformita_stock
				(cod_azienda,
				anno_non_conf,
				num_non_conf,
				progressivo,
				cod_prodotto,
				cod_deposito,
			 	cod_ubicazione,
			 	cod_lotto,
			 	prog_stock,
			 	data_stock)
		values (:s_cs_xx.cod_azienda,
			 		:ll_anno_nc,
			 		:ll_num_nc,
					 1,
					 :ls_cod_prodotto,
					 :ls_cod_deposito,
					 :ls_cod_ubicazione,
					 :ls_cod_lotto,
					 :ll_prog_stock,
					 :ldt_data_stock);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella insert di non_conformita_stock: " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
		
	end if
	
	ll_anno = dw_acc_materiali_lista.getitemnumber(dw_acc_materiali_lista.getrow(),"anno_acc_materiali")
	
	ll_num = dw_acc_materiali_lista.getitemnumber(dw_acc_materiali_lista.getrow(),"num_acc_materiali")
	
	update acc_materiali
	set    anno_non_conf = :ll_anno_nc,
			 num_non_conf = :ll_num_nc
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_acc_materiali = :ll_anno and
			 num_acc_materiali = :ll_num;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella update di acc_materiali: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	commit;
			 
	ll_row = dw_acc_materiali_lista.getrow()
	
	dw_acc_materiali_lista.triggerevent("pcd_retrieve")
	
	dw_acc_materiali_lista.setrow(ll_row)
	
	dw_acc_materiali_lista.scrolltorow(ll_row)
	
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno_nc

s_cs_xx.parametri.parametro_d_2 = ll_num_nc

window_open(w_non_conformita_std_app,-1)
end event

type cb_report_camp from commandbutton within w_acc_materiali
integer x = 2469
integer y = 800
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Rep. Camp."
end type

event clicked;long ll_row, ll_anno, ll_num


ll_row = dw_acc_materiali_lista.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare un'accettazione materiali prima di continuare!",exclamation!)
	return -1
end if

ll_anno = dw_acc_materiali_lista.getitemnumber(ll_row,"anno_acc_materiali")

ll_num = dw_acc_materiali_lista.getitemnumber(ll_row,"num_acc_materiali")

if isnull(ll_anno) or isnull(ll_num) then
	g_mb.messagebox("OMNIA","Selezionare un'accettazione materiali prima di continuare!",exclamation!)
	return -1
end if

if not isvalid(w_report_esecuzione_campionamenti) then
	s_cs_xx.parametri.parametro_d_1 = ll_anno
	s_cs_xx.parametri.parametro_d_2 = ll_num
   window_open(w_report_esecuzione_campionamenti, -1)
end if
end event

event getfocus;
call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"


end event

type cb_annulla from commandbutton within w_acc_materiali
integer x = 1655
integer y = 512
integer width = 361
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_ricerca.fu_Reset()
end event

type cb_ricerca from commandbutton within w_acc_materiali
integer x = 2043
integer y = 512
integer width = 361
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_ricerca.fu_BuildSearch(TRUE)
dw_folder.fu_SelectTab(2)
dw_acc_materiali_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_dati_tecnici from commandbutton within w_acc_materiali
integer x = 2469
integer y = 460
integer width = 361
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dati Prodotto"
end type

on clicked;if not isvalid(w_tes_prodotti_qualita) then
       window_open(w_tes_prodotti_qualita, -1)
end if

end on

type cb_eti_acc from commandbutton within w_acc_materiali
integer x = 2469
integer y = 20
integer width = 361
integer height = 80
integer taborder = 190
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etic.Accett."
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = dw_acc_materiali_1.getitemnumber(dw_acc_materiali_1.getrow(), "anno_acc_materiali") 
s_cs_xx.parametri.parametro_d_2 = dw_acc_materiali_1.getitemnumber(dw_acc_materiali_1.getrow(), "num_acc_materiali") 

if not(isnull(s_cs_xx.parametri.parametro_d_1)) and not(isnull(s_cs_xx.parametri.parametro_d_2)) then
   window_open(w_etichette_acc_materiali, 0)
end if

end event

type cb_etic_att from commandbutton within w_acc_materiali
integer x = 2469
integer y = 120
integer width = 361
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etic.Attesa"
end type

on clicked;s_cs_xx.parametri.parametro_d_1 = dw_acc_materiali_1.getitemnumber(dw_acc_materiali_1.getrow(), "anno_acc_materiali") 
s_cs_xx.parametri.parametro_d_2 = dw_acc_materiali_1.getitemnumber(dw_acc_materiali_1.getrow(), "num_acc_materiali") 

if not(isnull(s_cs_xx.parametri.parametro_d_1)) and not(isnull(s_cs_xx.parametri.parametro_d_2)) then
   window_open(w_etichette_attesa_controllo, 0)
end if
end on

type cb_1 from commandbutton within w_acc_materiali
integer x = 2469
integer y = 700
integer width = 361
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

on clicked;if not isvalid(w_report_acc_mat_1) then
   window_open(w_report_acc_mat_1, -1)
end if
end on

type cb_corrispondenze from commandbutton within w_acc_materiali
event clicked pbm_bnclicked
integer x = 2469
integer y = 260
integer width = 361
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;window_open_parm(w_acc_mat_corr, -1, dw_acc_materiali_1)

end event

type cb_note from commandbutton within w_acc_materiali
event clicked pbm_bnclicked
integer x = 2469
integer y = 360
integer width = 375
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;//window_open_parm(w_acc_mat_note, -1, dw_acc_materiali_1)

if dw_acc_materiali_lista.getrow() < 1 then return

s_cs_xx.parametri.parametro_ul_1 = dw_acc_materiali_lista.getitemnumber(dw_acc_materiali_lista.getrow(), "anno_acc_materiali")
s_cs_xx.parametri.parametro_ul_2 = dw_acc_materiali_lista.getitemnumber(dw_acc_materiali_lista.getrow(), "num_acc_materiali")
open(w_acc_mat_note_ole)

end event

type cb_ric_stock from cb_stock_ricerca within w_acc_materiali
integer x = 1678
integer y = 1360
integer width = 361
integer height = 76
integer taborder = 60
boolean bringtotop = true
string text = "Ric.Stock"
end type

event clicked;call super::clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

s_cs_xx.parametri.parametro_s_10 = dw_acc_materiali_1.getitemstring(dw_acc_materiali_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_acc_materiali_1.getitemstring(dw_acc_materiali_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_acc_materiali_1.getitemstring(dw_acc_materiali_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_acc_materiali_1.getitemstring(dw_acc_materiali_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_acc_materiali_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"
s_cs_xx.parametri.parametro_s_7 = "quan_arrivata"
s_cs_xx.parametri.parametro_s_8 = "prezzo_acquisto"

window_open(w_ricerca_stock, 0)
end event

type cb_tes_campionamenti from commandbutton within w_acc_materiali
event clicked pbm_bnclicked
integer x = 2469
integer y = 560
integer width = 361
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Campionam."
end type

event clicked;long ll_num_reg_campionamenti, ll_anno_reg_campionamenti, ll_row[]
string  ls_cod_fornitore

ll_row[1] = dw_acc_materiali_lista.getrow()
ll_anno_reg_campionamenti = dw_acc_materiali_lista.getitemnumber(ll_row[1], "anno_reg_campionamenti")
ll_num_reg_campionamenti = dw_acc_materiali_lista.getitemnumber(ll_row[1], "num_reg_campionamenti")
ls_cod_fornitore = dw_acc_materiali_lista.getitemstring(ll_row[1], "cod_fornitore")

if isnull(ll_anno_reg_campionamenti) or ll_anno_reg_campionamenti = 0 or &
	isnull(ll_num_reg_campionamenti) or ll_num_reg_campionamenti = 0 then
	if wf_genera_campionamento(ref ll_anno_reg_campionamenti, ref ll_num_reg_campionamenti) <> 0 then return
	dw_acc_materiali_lista.triggerevent("pcd_retrieve")
end if
//s_cs_xx.parametri.parametro_dw_1 = dw_acc_materiali_1
//s_cs_xx.parametri.parametro_s_1 = "acc_materiali"
window_open(w_det_campionamenti,-1)
w_det_campionamenti.ib_accettazioni = true
w_det_campionamenti.ii_anno_reg_campionamenti = ll_anno_reg_campionamenti
w_det_campionamenti.il_num_reg_campionamenti = ll_num_reg_campionamenti
w_det_campionamenti.is_cod_fornitore = ls_cod_fornitore
dw_acc_materiali_lista.set_selected_rows(1, &
													ll_row[], &
													c_ignorechanges, &
													c_refreshchildren, &
													c_refreshsame)

end event

type cb_doc_acc from cb_documenti_compilati within w_acc_materiali
integer x = 2203
integer y = 1140
integer width = 69
integer height = 76
integer taborder = 40
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;long ll_anno_reg, ll_num_reg, ll_riga[]
string ls_ritorno

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "nome_doc_cq"
s_cs_xx.parametri.parametro_s_2 = "DQ6"
s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(),s_cs_xx.parametri.parametro_s_1)

if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)

   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
		ll_anno_reg = dw_acc_materiali_3.getitemnumber(dw_acc_materiali_3.getrow(),"anno_acc_materiali")
		ll_num_reg  = dw_acc_materiali_3.getitemnumber(dw_acc_materiali_3.getrow(),"num_acc_materiali")
		update acc_materiali
		set    nome_doc_cq = :ls_ritorno
		where  acc_materiali.cod_azienda        = :s_cs_xx.cod_azienda and
		       acc_materiali.anno_acc_materiali = :ll_anno_reg and
		       acc_materiali.num_acc_materiali  = :ll_num_reg;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore: il documento potrebbe non essere registrato",Information!)
		end if
   end if      
else
   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
end if

ll_riga[1] = dw_acc_materiali_3.getrow()
dw_acc_materiali_lista.triggerevent("pcd_retrieve")
dw_acc_materiali_lista.Set_Selected_Rows(1, ll_riga, &
                                            c_CheckForChanges, &
                                            c_RefreshChildren, &
                                            c_RefreshView)

end event

type cb_doc_comp from cb_documenti_compilati within w_acc_materiali
integer x = 2203
integer y = 980
integer width = 69
integer height = 76
integer taborder = 50
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;long ll_anno_reg, ll_num_reg, ll_riga[]
string ls_ritorno

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "nome_doc_compilato"
s_cs_xx.parametri.parametro_s_2 = "DQ5"
s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)

if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)

   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
		ll_anno_reg = dw_acc_materiali_3.getitemnumber(dw_acc_materiali_3.getrow(),"anno_acc_materiali")
		ll_num_reg  = dw_acc_materiali_3.getitemnumber(dw_acc_materiali_3.getrow(),"num_acc_materiali")
		update acc_materiali
		set    nome_doc_compilato = :ls_ritorno
		where  acc_materiali.cod_azienda        = :s_cs_xx.cod_azienda and
		       acc_materiali.anno_acc_materiali = :ll_anno_reg and
		       acc_materiali.num_acc_materiali  = :ll_num_reg;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore: il documento potrebbe non essere registrato",Information!)
		end if
   end if      
else
   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
end if
ll_riga[1] = dw_acc_materiali_3.getrow()
dw_acc_materiali_lista.triggerevent("pcd_retrieve")
dw_acc_materiali_lista.Set_Selected_Rows(1, ll_riga, &
                                            c_CheckForChanges, &
                                            c_RefreshChildren, &
                                            c_RefreshView)


end event

type cb_cancella_camp from commandbutton within w_acc_materiali
integer x = 1230
integer y = 1224
integer width = 69
integer height = 72
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "C"
end type

event clicked;long ll_null


setnull(ll_null)

dw_acc_materiali_3.setitem(dw_acc_materiali_3.getrow(),"anno_reg_campionamenti",ll_null)

dw_acc_materiali_3.setitem(dw_acc_materiali_3.getrow(),"num_reg_campionamenti",ll_null)
end event

type st_semaforo from statictext within w_acc_materiali
integer x = 87
integer y = 996
integer width = 247
integer height = 72
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 65280
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_ricerca from u_dw_search within w_acc_materiali
integer x = 137
integer y = 48
integer width = 2290
integer height = 524
integer taborder = 200
string dataobject = "d_acc_materiali_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_acc_materiali
integer x = 23
integer y = 20
integer width = 2423
integer height = 600
integer taborder = 10
end type

type dw_acc_materiali_lista from uo_cs_xx_dw within w_acc_materiali
integer x = 137
integer y = 40
integer width = 2286
integer height = 560
integer taborder = 130
string dataobject = "d_acc_materiali_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_view;call super::pcd_view;stato = false
dw_acc_materiali_1.object.b_ricerca_fornitore.enabled=false
dw_acc_materiali_1.object.b_ricerca_prodotto.enabled=false
cb_doc_comp.enabled = false
cb_doc_acc.enabled = false
cb_eti_acc.enabled = true
cb_etic_att.enabled = true
cb_ric_stock.enabled = false
cb_note.enabled = true
cb_corrispondenze.enabled = true
cb_tes_campionamenti.enabled = true

cb_cancella_camp.enabled = false

cb_nc.enabled = true
end event

event pcd_validaterow;call super::pcd_validaterow;date ldd_data

ldd_data = date(this.getitemdatetime(this.getrow(),"data_prev_consegna"))
if isnull(ldd_data) or (ldd_data <= date("01/01/1900")) then
   g_mb.messagebox("Accettazione Materiali","ATTENZIONE! la data prevista consegna risulta mancante",Exclamation!)
   pcca.error = c_fatal
   return
end if

ldd_data = date(this.getitemdatetime(this.getrow(),"data_eff_consegna"))
if isnull(ldd_data) or (ldd_data <= date("01/01/1900")) then
   g_mb.messagebox("Accettazione Materiali","ATTENZIONE! data effettiva consegna non impostata",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
   g_mb.messagebox("Accettazione Materiali","ATTENZIONE! Manca l'indicazione del fornitore",Exclamation!)
   pcca.error = c_fatal
   return
end if

//Donato 10/06/2009 La valutaz. dei Forn. è estesa anche ai fornitori di servizi
//l'indicazione del prodotto, quantità e stock è dunque non più obbligatorio
/*
if isnull(this.getitemstring(this.getrow(), "cod_prodotto")) then
   g_mb.messagebox("Accettazione Materiali","ATTENZIONE! Manca l'indicazione del prodotto",Exclamation!)
   pcca.error = c_fatal
   return
end if

if this.getitemnumber(this.getrow(), "quan_arrivata") <= 0 then
   g_mb.messagebox("Accettazione Materiali","ATTENZIONE ! Manca l'indicazione della quantità del prodotto",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(), "cod_misura")) and (this.getitemnumber(this.getrow(), "quan_arrivata") > 0) then
   g_mb.messagebox("Accettazione Materiali","Codice misura obbligatorio in presenza di quantità",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(),"cod_deposito")) then
   g_mb.messagebox("Accettazione Materiali","Manca l'indicazione dello STOCK",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(),"cod_ubicazione")) then
   g_mb.messagebox("Accettazione Materiali","Manca l'indicazione dell' UBICAZIONE",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(),"cod_lotto")) then
   g_mb.messagebox("Accettazione Materiali","Manca l'indicazione dell' LOTTO",Exclamation!)
   pcca.error = c_fatal
   return
end if
*/



end event

event pcd_new;call super::pcd_new;ib_in_new = true
dw_acc_materiali_1.object.b_ricerca_fornitore.enabled=true
dw_acc_materiali_1.object.b_ricerca_prodotto.enabled=true
stato = true
cb_doc_comp.enabled = true
cb_doc_acc.enabled = true
cb_eti_acc.enabled = false
cb_etic_att.enabled = false
cb_ric_stock.enabled = true
cb_note.enabled = false
cb_corrispondenze.enabled = false
cb_tes_campionamenti.enabled = false

cb_cancella_camp.enabled = true

cb_nc.enabled = false

setitem(getrow(),"flag_esito_campionamento","D")
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event updatestart;call super::updatestart;//string ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_prodotto, ls_parametro_csa
//long ll_prog_stock, ll_count,ll_anno,ll_num_registrazione, ll_i,ll_cont
//datetime ldt_data_stock
//
//// Enrico x Sintexcal 18/03/2005
//// Creazione Stock in Accettazione
//// Questo parametro stabilisce se fare una insert per creare lo stock automaticamente.
//select flag
//into   :ls_parametro_csa
//from   parametri_omnia
//where  cod_azienda = :s_cs_xx.cod_azienda and
//       cod_parametro = 'CSA';
//if sqlca.sqlcode <> 0 then
//	ls_parametro_csa = "N"
//end if
//
//for ll_i = 1 to rowcount()
//	
//	if getitemstatus(ll_i, 0, primary!) = NewModified!	or getitemstatus(ll_i, 0, primary!) = New! or getitemstatus(ll_i, 0, primary!) = DataModified!	 then
//		
//		ll_anno = getitemnumber(ll_i, "anno_acc_materiali")
//		ll_num_registrazione = getitemnumber(ll_i, "num_acc_materiali")
//		ls_cod_prodotto = getitemstring(ll_i, "cod_prodotto")
//		ls_cod_deposito = getitemstring(ll_i, "cod_deposito")
//		ls_cod_ubicazione = getitemstring(getrow(), "cod_ubicazione")
//		ls_cod_lotto = getitemstring(getrow(), "cod_lotto")
//		ldt_data_stock = getitemdatetime(getrow(), "data_stock")
//		ll_prog_stock = getitemnumber(getrow(), "prog_stock")
//		
//		/*
//		
//		if isnull(ls_cod_prodotto) then
//			g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione di un prodotto di magazzino è obbligatorio!")
//			return 1
//		end if
//		
//		if isnull(ls_cod_deposito) then
//			if ls_parametro_csa = "S" then
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione dell'area aziendale è obbligatorio!")
//			else
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione del deposito dello stock è obbligatorio!")
//			end if
//			return 1
//		end if
//		
//		if isnull(ls_cod_ubicazione) then
//			if ls_parametro_csa = "S" then
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione del fornitore della bolla è obbligatorio!")
//			else
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione dell'ubicazione dello stock è obbligatorio!")
//			end if
//			return 1
//		end if
//		
//		if isnull(ls_cod_lotto) then
//			if ls_parametro_csa = "S" then
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione del numero DDT è obbligatorio!")
//			else
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione del lotto dello stock è obbligatorio!")
//			end if
//			return 1
//		end if
//		
//		if isnull(ldt_data_stock) or ldt_data_stock <= datetime(date("01/01/1900"),00:00:00) then
//			if ls_parametro_csa = "S" then
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione della data data di riceziione/data bolla è obbligatorio!")
//			else
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione),"L'indicazione della data dello stock è obbligatorio!")
//			end if
//			return 1
//		end if
//		
//		*/
//		
//		if getitemstatus(ll_i, 0, primary!) <> DataModified! then
//		
//			select count(*)
//			into :ll_count
//			from acc_materiali
//			where cod_azienda = :s_cs_xx.cod_azienda and
//					cod_prodotto = :ls_cod_prodotto and
//					cod_deposito = :ls_cod_deposito and
//					cod_ubicazione = :ls_cod_ubicazione and
//					cod_lotto = :ls_cod_lotto and
//					data_stock = :ldt_data_stock and
//					prog_stock = :ll_prog_stock and
//					(anno_acc_materiali <> :ll_anno or
//					num_acc_materiali <> :ll_num_registrazione);
//						
//			if sqlca.sqlcode <> 0 then
//				g_mb.messagebox("OMNIA", "Errore in fase di verifica esistenza stock~r~n"+sqlca.sqlerrtext)
//				return 1
//			end if
//			
//			if ll_count > 0 then
//				g_mb.messagebox("Registrazione " + string(ll_anno) + "/" + string(ll_num_registrazione), "Impossibile Eseguire la registrazione in quanto lo stock già presente in un'altra accettazione")
//				return 1
//			end if
//			
//		end if
//		
//		if ls_parametro_csa = "S" then
//			
//			select count(*)
//			into   :ll_cont
//			from   stock
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_prodotto = :ls_cod_prodotto and
//					 cod_deposito = :ls_cod_deposito and
//					 cod_ubicazione = :ls_cod_ubicazione and
//					 cod_lotto = :ls_cod_lotto and
//					 data_stock = :ldt_data_stock and
//					 prog_stock = :ll_prog_stock;
//					 
//			if isnull(ll_cont) or ll_cont = 0 then
//			
//				INSERT INTO stock  
//						( cod_azienda,   
//						  cod_prodotto,   
//						  cod_deposito,   
//						  cod_ubicazione,   
//						  cod_lotto,   
//						  data_stock,   
//						  prog_stock,   
//						  giacenza_stock,   
//						  quan_assegnata,   
//						  quan_in_spedizione,   
//						  costo_medio )  
//				VALUES ( :s_cs_xx.cod_azienda,   
//						  :ls_cod_prodotto,   
//						  :ls_cod_deposito,   
//						  :ls_cod_ubicazione,   
//						  :ls_cod_lotto,   
//						  :ldt_data_stock,   
//						  :ll_prog_stock,   
//						  0,   
//						  0,   
//						  0,   
//						  0)  ;
//						  
//				if sqlca.sqlcode < 0 then
//					g_mb.messagebox("OMNIA","Errore in fase di creazione automatica stock!~r~n"+sqlca.sqlerrtext)
//					return 1
//				end if
//			end if			
//		end if
//		
//	end if	
//next
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_anno_registrazione, ll_num_registrazione

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
	if isnull(GetItemnumber(l_Idx, "anno_acc_materiali")) or GetItemnumber(l_Idx, "anno_acc_materiali") = 0 then
		SetItem(l_Idx, "anno_acc_materiali", f_anno_esercizio())
	end if	

	if isnull(GetItemnumber(l_Idx, "num_acc_materiali")) or GetItemnumber(l_Idx, "num_acc_materiali") = 0 then
		ll_anno_registrazione = f_anno_esercizio()
		ll_num_registrazione = 0
		
		select max(num_acc_materiali)
		  into :ll_num_registrazione
		  from acc_materiali
		  where (cod_azienda = :s_cs_xx.cod_azienda) and 
		        (anno_acc_materiali = :ll_anno_registrazione);
						  
		if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione = ll_num_registrazione + 1
		end if
		setitem(this.getrow(),"num_acc_materiali", ll_num_registrazione)
	end if	
NEXT

end event

event pcd_modify;call super::pcd_modify;stato = true
dw_acc_materiali_1.object.b_ricerca_fornitore.enabled=true
dw_acc_materiali_1.object.b_ricerca_prodotto.enabled=true
cb_doc_comp.enabled = true
cb_doc_acc.enabled = true
cb_eti_acc.enabled = false
cb_etic_att.enabled = false
cb_ric_stock.enabled = true
cb_note.enabled = false
cb_corrispondenze.enabled = false
cb_tes_campionamenti.enabled = false

cb_cancella_camp.enabled = true

cb_nc.enabled = false
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if not isnull(this.getitemstring(getrow(),"cod_fornitore")) then
		wf_semaforo(this.getitemstring(getrow(),"cod_fornitore"))
	end if
end if
end event

event updateend;call super::updateend;cb_nc.enabled = true

cb_cancella_camp.enabled = false
end event

event pcd_delete;call super::pcd_delete;dw_acc_materiali_1.object.b_ricerca_fornitore.enabled=false
dw_acc_materiali_1.object.b_ricerca_prodotto.enabled=false
cb_doc_comp.visible=false
cb_doc_acc.visible=false
end event

type dw_acc_materiali_3 from uo_cs_xx_dw within w_acc_materiali
integer x = 46
integer y = 760
integer width = 2377
integer height = 800
integer taborder = 120
string dataobject = "d_acc_materiali_4"
boolean border = false
end type

event itemchanged;call super::itemchanged;date ld_data

if i_extendmode then
	choose case i_colname
		case "data_eff_consegna"
			ld_data = date(left(data,10))
			if isnull(ld_data) or not( ld_data > date("01-01-1900")) then
				g_mb.messagebox("Accettazione Materiali","Data di ricezione merce obbligatoria", StopSign!)
				pcca.error = c_valfailed
			end if
		case "flag_esito_campionamento"
			if isnull(getitemnumber(row,"anno_reg_campionamenti")) or isnull(getitemnumber(row,"num_reg_campionamenti")) then
				g_mb.messagebox("OMNIA","Nessun campionamento presente!")
				return 1
			end if
	end choose
end if
end event

type tab_folder from tab within w_acc_materiali
integer x = 23
integer y = 640
integer width = 2423
integer height = 1100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type

event selectionchanged;choose case newindex
	case 1
		dw_acc_materiali_1.show()
		dw_acc_materiali_2.hide()
		dw_acc_materiali_3.hide()
		dw_acc_materiali_1.object.b_ricerca_fornitore.visible=true
		dw_acc_materiali_1.object.b_ricerca_prodotto.visible=true
		cb_ric_stock.show()
		cb_doc_acc.hide()
		cb_doc_comp.hide()
		st_semaforo.show()
		cb_cancella_camp.hide()
	case 2
		dw_acc_materiali_1.hide()
		dw_acc_materiali_2.show()
		dw_acc_materiali_3.hide()
		dw_acc_materiali_1.object.b_ricerca_fornitore.visible=false
		dw_acc_materiali_1.object.b_ricerca_prodotto.visible=false
		cb_ric_stock.hide()
		cb_doc_acc.hide()
		cb_doc_comp.hide()
		st_semaforo.hide()
		cb_cancella_camp.hide()
	case 3
		dw_acc_materiali_1.hide()
		dw_acc_materiali_2.hide()
		dw_acc_materiali_3.show()
		cb_doc_acc.show()
		cb_doc_comp.show()
		dw_acc_materiali_1.object.b_ricerca_fornitore.visible=false
		dw_acc_materiali_1.object.b_ricerca_prodotto.visible=false
		cb_ric_stock.hide()
		st_semaforo.hide()
		cb_cancella_camp.show()
end choose	
end event

on tab_folder.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on tab_folder.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

type tabpage_1 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 2386
integer height = 976
long backcolor = 12632256
string text = "Anagrafici"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
end type

type tabpage_2 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 2386
integer height = 976
long backcolor = 12632256
string text = "Riferimenti"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

type tabpage_3 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 2386
integer height = 976
long backcolor = 12632256
string text = "Date / Documenti"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

type dw_acc_materiali_2 from uo_cs_xx_dw within w_acc_materiali
integer x = 41
integer y = 776
integer width = 1797
integer height = 956
integer taborder = 110
string dataobject = "d_acc_materiali_3"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_cod_prod_fornitore, ls_prodotto, ls_fornitore, ls_null
string ls_str1, ls_str2, ls_str3, ls_str4, ls_str5
long   ll_null
datetime   ld_null
double ld_progressivo

setnull(ls_null)
setnull(ll_null)
setnull(ld_null)


choose case i_colname


case "anno_bolla_acq"

	if ib_bol_acq then
		ls_str1 = dw_acc_materiali_1.getitemstring(dw_acc_materiali_1.getrow(), "cod_fornitore")
		if isnull(ls_str1) then ls_str1 = "%"
		this.setitem(this.getrow(), "num_bolla_acq", ll_null)
		this.setitem(this.getrow(), "prog_riga_bolla_acq", ll_null)
		f_PO_LoadDDLB_DW(dw_acc_materiali_2,"num_bolla_acq",sqlca,&
					  "tes_bol_acq","num_bolla_acq","num_bolla_acq",&
					  "(tes_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "(tes_bol_acq.anno_bolla_acq = " + i_coltext + ")  and " +  &
					  "(tes_bol_acq.cod_fornitore like '"+ls_str1+"')" )
	end if

case "num_bolla_acq"
	if ib_bol_acq then
		ls_str1 = dw_acc_materiali_1.getitemstring(dw_acc_materiali_1.getrow(), "cod_fornitore")
		if isnull(ls_str1) then ls_str1 = "%"
		ls_str2 = string(int(this.getitemnumber(this.getrow(), "anno_bolla_acq")))
		SELECT tes_bol_acq.progressivo
		INTO   :ld_progressivo
		FROM   tes_bol_acq
		WHERE  (tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda) and
						(tes_bol_acq.anno_bolla_acq = :ls_str2) and
						(tes_bol_acq.cod_fornitore like :ls_str1) and
						(tes_bol_acq.num_bolla_acq = :i_coltext) ;
	
		ls_str3 = string(int(ld_progressivo))
		this.setitem(this.getrow(), "prog_riga_bolla", ll_null)
		f_PO_LoadDDLB_DW(dw_acc_materiali_2,"prog_riga_bolla",sqlca,&
					  "det_bol_acq","prog_riga_bolla_acq","prog_riga_bolla_acq",&
					  "(det_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "(det_bol_acq.anno_bolla_acq = " + ls_str2 + ")  and " +&
					  "(det_bol_acq.progressivo = " + ls_str3 + ")  and " +&
					  "(det_bol_acq.num_bolla_acq = " + i_coltext + ") ")
	end if

//case "prog_riga_bolla"
//   if not isnull(i_coltext) then
//      ls_str1 = string(int(this.getitemnumber(this.getrow(),"anno_bolla_acq")))
//      ls_str2 = string(int(this.getitemnumber(this.getrow(),"num_bolla_acq")))
//      SELECT det_bol_acq.cod_prodotto
//      INTO   :ls_str3
//      FROM   det_bol_acq
//      WHERE  (det_bol_acq.cod_azienda=:s_cs_xx.cod_azienda) and
//             (det_bol_acq.anno_registrazione = :ls_str1) and
//             (det_bol_acq.num_registrazione  = :ls_str2);
//      if sqlca.sqlcode = 0 then sle_cod_prodotto_bolla_acq.text = ls_str3
//   end if   


case "anno_registrazione"
   ls_str1 = dw_acc_materiali_1.getitemstring(dw_acc_materiali_1.getrow(), "cod_fornitore")
   if isnull(ls_str1) then ls_str1 = "%"
   this.setitem(this.getrow(), "num_registrazione", ll_null)
   this.setitem(this.getrow(), "prog_riga_ordine_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_acc_materiali_2,"num_registrazione",sqlca,&
              "tes_ord_acq","num_registrazione","num_registrazione",&
              "(tes_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " +&
              "(tes_ord_acq.anno_registrazione = " + i_coltext + ")  and " +&
              "(tes_ord_acq.cod_fornitore like '" + ls_str1 + "')" )


case "num_registrazione"
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "anno_registrazione")))
   this.setitem(this.getrow(), "prog_riga_ordine_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_acc_materiali_2,"prog_riga_ordine_acq",sqlca,&
              "det_ord_acq","prog_riga_ordine_acq","prog_riga_ordine_acq",&
              "(det_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(det_ord_acq.anno_registrazione = "+ls_str2+")  and " + &
              "(det_ord_acq.num_registrazione = "+i_coltext+") " )


//case "prog_riga_ordine_acq"
//   if not isnull(i_coltext) then
//      ls_str1 = string(int(this.getitemnumber(this.getrow(),"anno_registrazione_ord_acq")))
//      ls_str2 = string(int(this.getitemnumber(this.getrow(),"num_registrazione")))
//      SELECT det_ord_acq.cod_prodotto
//      INTO   :ls_str3
//      FROM   det_ord_acq
//      WHERE  (det_ord_acq.cod_azienda=:s_cs_xx.cod_azienda) and
//             (det_ord_acq.anno_registrazione = :ls_str1) and
//             (det_ord_acq.num_registrazione  = :ls_str2);
//      if sqlca.sqlcode = 0 then sle_cod_prodotto_ord_acq.text = ls_str3
//   end if   


case "cod_doc_origine"
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "cod_fornitore")))
   if isnull(ls_str1) then ls_str1 = "%"
   this.setitem(this.getrow(), "numeratore_doc_origine", ll_null)
   this.setitem(this.getrow(), "anno_doc_origine", ll_null)
   this.setitem(this.getrow(), "num_doc_origine", ll_null)
   this.setitem(this.getrow(), "progressivo_fat_acq", ll_null)
   this.setitem(this.getrow(), "prog_riga_fat_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_acc_materiali_2,"numeratore_doc_origine",sqlca,&
              "tes_fat_acq","numeratore_doc_origine","numeratore_doc_origine",&
              "(tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(tes_fat_acq.cod_fornitore = '" + ls_str2 + "')  and " + &
              "(tes_fat_acq.cod_doc_origine = '" + i_coltext + "') " )

case "numeratore_doc_origine"
   ls_str1 = string(int(this.getitemnumber(this.getrow(), "cod_fornitore")))
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "cod_doc_origine")))
   if isnull(ls_str1) then ls_str1 = "%"
   if isnull(ls_str2) then ls_str2 = "%"
   this.setitem(this.getrow(), "anno_doc_origine", ll_null)
   this.setitem(this.getrow(), "num_doc_origine", ll_null)
   this.setitem(this.getrow(), "progressivo_fat_acq", ll_null)
   this.setitem(this.getrow(), "prog_riga_fat_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_acc_materiali_2,"anno_doc_origine",sqlca,&
              "tes_fat_acq","anno_doc_origine","anno_doc_origine",&
              "(tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(tes_fat_acq.cod_fornitore = '"+ls_str2+"')  and " + &
              "(tes_fat_acq.cod_doc_origine = '"+ls_str3+"') and " + &
              "(tes_fat_acq.numratore_doc_origine = '"+i_coltext+"')" )

case "anno_doc_origine"
   ls_str1 = string(int(this.getitemnumber(this.getrow(), "cod_fornitore")))
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "cod_doc_origine")))
   ls_str3 = string(int(this.getitemnumber(this.getrow(), "numeratore_doc_origine")))
   if isnull(ls_str1) then ls_str1 = "%"
   if isnull(ls_str2) then ls_str2 = "%"
   if isnull(ls_str3) then ls_str3 = "%"
   this.setitem(this.getrow(), "num_doc_origine", ll_null)
   this.setitem(this.getrow(), "progressivo_fat_acq", ll_null)
   this.setitem(this.getrow(), "prog_riga_fat_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_acc_materiali_2,"num_doc_origine",sqlca,&
              "tes_fat_acq","num_doc_origine","num_doc_origine",&
              "(tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(tes_fat_acq.cod_fornitore = '"+ls_str1+"')  and " + &
              "(tes_fat_acq.cod_doc_origine = '"+ls_str2+"')  and " +&
              "(tes_fat_acq.numeratore_doc_origine = "+ls_str3+") and " +&
              "(tes_fat_acq.anno_doc_origine = "+i_coltext+") " )

case "num_doc_origine"
   ls_str1 = string(int(this.getitemnumber(this.getrow(), "cod_fornitore")))
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "cod_doc_origine")))
   ls_str3 = string(int(this.getitemnumber(this.getrow(), "numeratore_doc_origine")))
   ls_str4 = string(int(this.getitemnumber(this.getrow(), "anno_doc_origine")))
   if isnull(ls_str1) then ls_str1 = "%"
   if isnull(ls_str2) then ls_str2 = "%"
   if isnull(ls_str3) then ls_str3 = "%"
   this.setitem(this.getrow(), "progressivo_fat_acq", ll_null)
   this.setitem(this.getrow(), "prog_riga_fat_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_acc_materiali_2,"progressivo_fat_acq",sqlca,&
              "tes_fat_acq","progressivo_fat_acq","progressivo_fat_acq",&
              "(tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " +&
              "(tes_fat_acq.cod_fornitore = '"+ls_str1+"')  and " +&
              "(tes_fat_acq.cod_doc_origine = '"+ls_str2+"')  and " +&
              "(tes_fat_acq.numeratore_doc_origine = "+ls_str3+") and " +&
              "(tes_fat_acq.anno_doc_origine = "+ls_str4+")  and " + &
              "(tes_fat_acq.num_doc_origine = "+i_coltext+")" )

case "progressivo_fat_acq"
   ls_str1 = string(int(this.getitemnumber(this.getrow(), "cod_fornitore")))
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "cod_doc_origine")))
   ls_str3 = string(int(this.getitemnumber(this.getrow(), "numeratore_doc_origine")))
   ls_str4 = string(int(this.getitemnumber(this.getrow(), "anno_doc_origine")))
   ls_str5 = string(int(this.getitemnumber(this.getrow(), "num_doc_origine")))
   if isnull(ls_str1) then ls_str1 = "%"
   if isnull(ls_str2) then ls_str2 = "%"
   if isnull(ls_str3) then ls_str3 = "%"
   this.setitem(this.getrow(), "prog_riga_fat_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_acc_materiali_2,"prog_riga_fat_acq",sqlca,&
              "tes_fat_acq","prog_riga_fat_acq","prog_riga_fat_acq",&
              "(tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(tes_fat_acq.cod_fornitore = '"+ls_str1+"')  and " + &
              "(tes_fat_acq.cod_doc_origine = '"+ls_str2+"')  and " + &
              "(tes_fat_acq.numeratore_doc_origine = "+ls_str3+") and " + &
              "(tes_fat_acq.anno_doc_origine = "+ls_str4+")  and " + &
              "(tes_fat_acq.num_doc_origine = "+ls_str5+")  and " + &
              "(tes_fat_acq.progresivo_fat_acq = "+i_coltext+")" )

end choose


end if   // si riferisce alla condizione iniziale di test della variabile i_extendmode
end event

type dw_acc_materiali_1 from uo_cs_xx_dw within w_acc_materiali
integer x = 37
integer y = 756
integer width = 2368
integer height = 940
integer taborder = 100
string dataobject = "d_acc_materiali_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string   ls_cod_prod_fornitore, ls_prodotto, ls_fornitore, ls_null, ls_cod_piano_campionamento
string   ls_str1, ls_str2, ls_str3, ls_cod_misura, ls_rag_soc_1, ls_des_piano_campionamento
long     ll_null
datetime ld_null, ld_data_validita
double   ld_progressivo


setnull(ls_null)
setnull(ll_null)
setnull(ld_null)


choose case i_colname

case "cod_prodotto"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   if isnull(ls_prodotto) then ls_prodotto = " "
   if (i_coltext <> ls_prodotto) and (not isnull(this.getitemstring(this.getrow(),"cod_deposito")))  then
      g_mb.messagebox("Accettazione Materiali","Prodotto variato: devi reimpostare stock !",Exclamation!)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "cod_deposito", ls_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "cod_ubicazione", ls_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "cod_lotto", ls_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "data_stock", ld_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "prog_stock", ll_null)
   end if
   ls_prodotto = i_coltext
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")

   SELECT anag_prodotti.cod_misura_mag  
   INTO   :ls_cod_misura  
   FROM   anag_prodotti  
   WHERE  (anag_prodotti.cod_azienda  = :s_cs_xx.cod_azienda ) AND  
          (anag_prodotti.cod_prodotto = :i_coltext )   ;
   if (sqlca.sqlcode = 0) and (not isnull(ls_cod_misura)) then
      setitem(getrow(), "cod_misura", ls_cod_misura)
   end if


case "cod_fornitore"
   ls_str1 = i_coltext
   if isnull(ls_str1) then ls_str1 = "%"
   if ib_bol_acq then
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "anno_bolla_acq", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "num_bolla_acq", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "progressivo", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "prog_riga_bolla_acq", ll_null)
      f_PO_LoadDDLB_DW(dw_acc_materiali_2,"anno_bolla_acq",sqlca,&
                 "tes_bol_acq","anno_bolla_acq","anno_bolla_acq",&
                 "(tes_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                  (tes_bol_acq.cod_fornitore like '"+ ls_str1 + "')")
   end if

   if ib_ord_acq then
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "anno_registrazione", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "num_registrazione", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "prog_riga_ordine_acq", ll_null)
      f_PO_LoadDDLB_DW(dw_acc_materiali_2,"anno_registrazione",sqlca,&
                 "tes_ord_acq","anno_registrazione","anno_registrazione",&
                 "(tes_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                  (tes_ord_acq.cod_fornitore like '"+ ls_str1 + "')")
   end if

   if ib_fat_acq then
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "cod_doc_origine", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "numeratore_doc_origine", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "anno_doc_origine", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "num_doc_origine", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "progressivo_fat_acq", ll_null)
      dw_acc_materiali_2.setitem(dw_acc_materiali_2.getrow(), "prog_riga_fat_acq", ll_null)
      f_PO_LoadDDLB_DW(dw_acc_materiali_2,"cod_doc_origine",sqlca,&
                 "tes_fat_acq","cod_doc_origine","cod_doc_origine",&
                 "(tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                  (tes_fat_acq.cod_fornitore like '"+ ls_str1 + "')")
   end if

//	if not isnull(i_coltext) then
//		SELECT anag_fornitori.rag_soc_1,   
//				 anag_fornitori.cod_piano_campionamento 
//		INTO   :ls_rag_soc_1,   
//				 :ls_cod_piano_campionamento  
//		FROM 	 anag_fornitori
//		WHERE  ( anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//				 ( anag_fornitori.cod_fornitore = :i_coltext )   ;
//
//		SELECT tes_piani_campionamento.des_piano_campionamento,   
//				 max(tes_piani_campionamento.data_validita)  
//		INTO   :ls_des_piano_campionamento,   
//				 :ld_data_validita  
//		FROM   tes_piani_campionamento  
//		WHERE  ( tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//				 ( tes_piani_campionamento.cod_piano_campionamento = :ls_cod_piano_campionamento )
//		GROUP BY tes_piani_campionamento.des_piano_campionamento		 ;
//
//		
//		if sqlca.sqlcode = 0 then
//			if isnull(ls_rag_soc_1) then ls_rag_soc_1 = " "
//			tab_folder.tabpage_3.st_2.text = ls_cod_piano_campionamento
//			tab_folder.tabpage_3.st_3.text = ls_des_piano_campionamento
//		end if	
//	end if
	ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =i_coltext

case "cod_prod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")


case "cod_deposito"
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_acc_materiali_1, &
                       "cod_ubicazione", &
                       SQLCA, &
                       "stock", &
                       "cod_ubicazione" , &
                       "cod_ubicazione" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (stock.cod_deposito = '" + i_coltext + "')")
   end if

case "cod_ubicazione"
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_acc_materiali_1, &
                       "cod_lotto", &
                       SQLCA, &
                       "stock", &
                       "cod_lotto" , &
                       "cod_lotto" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (stock.cod_deposito = '" + this.getitemstring(this.getrow(),"cod_ubicazione") + "') and &
                        (stock.cod_ubicazione = '" + i_coltext + "')")
   end if
end choose


if ( (i_colname = "cod_prod_fornitore") or (i_colname = "cod_prodotto") or (i_colname = "cod_fornitore") ) and &
   (not(isnull(ls_fornitore))) and &
   (not(isnull(ls_prodotto)))  then

   select tab_prod_fornitori.cod_prod_fornitore
   into   :ls_cod_prod_fornitore
   from   tab_prod_fornitori
   where  (tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
          (tab_prod_fornitori.cod_prodotto = :ls_prodotto) and
          (tab_prod_fornitori.cod_fornitore = :ls_fornitore) ;

 	if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
   else
      this.setitem(this.getrow(), "cod_prod_fornitore", "Non Indicato")
   end if
end if

end if   // si riferisce alla condizione iniziale di test della variabile i_extendmode
end event

on pcd_save;call uo_cs_xx_dw::pcd_save;//ib_x = false
end on

event itemchanged;call super::itemchanged;if i_extendmode then
	if i_colname = "cod_fornitore" then
		wf_semaforo(i_coltext)
	end if
end if
end event

on itemfocuschanged;call uo_cs_xx_dw::itemfocuschanged;//if not(ib_x) then return
//
//string ls_prodotto, ls_fornitore, ls_null, ls_colonna, ls_cod_prod_fornitore
//
//if this.getrow() < 1 then return
//
//setnull(ls_null)
//ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
//ls_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
//ls_colonna = this.getcolumnname()
//
//if (this.getcolumnname() = "cod_prod_fornitore") or (this.getcolumnname() = "cod_prodotto") or (this.getcolumnname() = "cod_fornitore") then
//
//   if (isnull(ls_fornitore)) or (isnull(ls_prodotto))  then
//         f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_prod_fornitore",sqlca,&
//                          "tab_prod_fornitori","cod_prod_fornitore","des_prod_fornitore", &
//                          "(tab_prod_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                          (tab_prod_fornitori.cod_prodotto = '" + ls_prodotto + "') and&
//                          (tab_prod_fornitori.cod_fornitore = '" + ls_fornitore + "')" )
//         this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
//   else
//        select tab_prod_fornitori.cod_prod_fornitore
//        into   :ls_cod_prod_fornitore
//        from   tab_prod_fornitori
//        where  (tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
//               (tab_prod_fornitori.cod_prodotto = :ls_prodotto) and
//               (tab_prod_fornitori.cod_fornitore = :ls_fornitore) ;
//        if sqlca.sqlcode = 0 then
//           f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_prod_fornitore",sqlca,&
//                             "tab_prod_fornitori","cod_prod_fornitore","des_prod_fornitore", &
//                             "(tab_prod_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                             (tab_prod_fornitori.cod_prodotto = '" + ls_prodotto + "') and&
//                             (tab_prod_fornitori.cod_fornitore = '" + ls_fornitore + "')" )
//           this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
//        else
//           this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
//        end if
//   end if
//end if
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;//ib_x = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;//ib_x = true
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;//ib_x = true
end on

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_acc_materiali_1,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_acc_materiali_1, "cod_fornitore")
end choose
end event

