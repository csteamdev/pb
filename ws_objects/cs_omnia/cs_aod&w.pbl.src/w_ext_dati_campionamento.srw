﻿$PBExportHeader$w_ext_dati_campionamento.srw
$PBExportComments$Finestra Richiesta Dati Campionamento
forward
global type w_ext_dati_campionamento from w_cs_xx_risposta
end type
type dw_ext_dati_campionamento from uo_cs_xx_dw within w_ext_dati_campionamento
end type
type cb_1 from commandbutton within w_ext_dati_campionamento
end type
type cb_annulla from commandbutton within w_ext_dati_campionamento
end type
end forward

global type w_ext_dati_campionamento from w_cs_xx_risposta
integer width = 2368
integer height = 604
string title = "Dati Indentificati Campionamento"
dw_ext_dati_campionamento dw_ext_dati_campionamento
cb_1 cb_1
cb_annulla cb_annulla
end type
global w_ext_dati_campionamento w_ext_dati_campionamento

on w_ext_dati_campionamento.create
int iCurrent
call super::create
this.dw_ext_dati_campionamento=create dw_ext_dati_campionamento
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ext_dati_campionamento
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_annulla
end on

on w_ext_dati_campionamento.destroy
call super::destroy
destroy(this.dw_ext_dati_campionamento)
destroy(this.cb_1)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;dw_ext_dati_campionamento.set_dw_options(sqlca, &
                                 pcca.null_object,&
											c_nodelete + &
											c_newonopen + &
											c_disablecc, &
											c_default)

save_on_close(c_socnosave)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_dati_campionamento,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_ext_dati_campionamento,"cod_tipologia_campionamento",sqlca,&
                 "tab_rese","cod_resa","des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

type dw_ext_dati_campionamento from uo_cs_xx_dw within w_ext_dati_campionamento
integer x = 23
integer y = 20
integer width = 2286
integer height = 360
integer taborder = 20
string dataobject = "d_ext_dati_campionamento"
end type

event pcd_new;call super::pcd_new;this.setitem(1, "data_registrazione", datetime(today()))
end event

type cb_1 from commandbutton within w_ext_dati_campionamento
integer x = 1920
integer y = 400
integer width = 375
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;dw_ext_dati_campionamento.accepttext()

s_cs_xx.parametri.parametro_s_1 = dw_ext_dati_campionamento.getitemstring(1, "des_campionamento")
s_cs_xx.parametri.parametro_s_2 = dw_ext_dati_campionamento.getitemstring(1, "cod_operaio")
s_cs_xx.parametri.parametro_data_1 = datetime(dw_ext_dati_campionamento.getitemdatetime(1, "data_registrazione"))
s_cs_xx.parametri.parametro_s_3 = dw_ext_dati_campionamento.getitemstring(1, "cod_tipologia_campionamento")

close(parent)
end event

type cb_annulla from commandbutton within w_ext_dati_campionamento
event clicked pbm_bnclicked
integer x = 1531
integer y = 400
integer width = 375
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_data_1)

close(parent)
end event

