﻿$PBExportHeader$w_gestione_nc.srw
$PBExportComments$Finestra Gestione Vista e Modifica NC di Sistema da Piani Miglioramento
forward
global type w_gestione_nc from w_cs_xx_principale
end type
type dw_non_conformita_lista from uo_cs_xx_dw within w_gestione_nc
end type
type cb_dettagli from commandbutton within w_gestione_nc
end type
type dw_non_conformita_std_1 from uo_cs_xx_dw within w_gestione_nc
end type
type dw_non_conformita_std_4 from uo_cs_xx_dw within w_gestione_nc
end type
type dw_folder from u_folder within w_gestione_nc
end type
type cb_doc_aut_deroga from cb_documenti_compilati within w_gestione_nc
end type
type dw_non_conformita_std_2 from uo_cs_xx_dw within w_gestione_nc
end type
type dw_non_conformita_std_3 from uo_cs_xx_dw within w_gestione_nc
end type
end forward

global type w_gestione_nc from w_cs_xx_principale
integer width = 3342
integer height = 1428
string title = "Non Conformità Sistema Qualità"
dw_non_conformita_lista dw_non_conformita_lista
cb_dettagli cb_dettagli
dw_non_conformita_std_1 dw_non_conformita_std_1
dw_non_conformita_std_4 dw_non_conformita_std_4
dw_folder dw_folder
cb_doc_aut_deroga cb_doc_aut_deroga
dw_non_conformita_std_2 dw_non_conformita_std_2
dw_non_conformita_std_3 dw_non_conformita_std_3
end type
global w_gestione_nc w_gestione_nc

type variables
boolean ib_delete=true, ib_in_new=false
string is_prodotto, is_tipo_requisito, is_agg_tipo_nc
long i_anno_nc, i_num_nc
end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_non_conformita_std_3
dw_folder.fu_AssignTab(3, "Correz./Cause", l_Objects[])
l_objects[1] = dw_non_conformita_std_4
dw_folder.fu_AssignTab(4, "Prevenz./Note", l_Objects[])
l_objects[1] = dw_non_conformita_std_1
dw_folder.fu_AssignTab(1, "Generici", l_Objects[])
l_objects[1] = dw_non_conformita_std_2
l_objects[2] = cb_doc_aut_deroga
dw_folder.fu_AssignTab(2, "Qta/Decisioni", l_Objects[])

dw_folder.fu_FolderCreate(4,4)
dw_folder.fu_SelectTab(1)

dw_non_conformita_lista.set_dw_key("cod_azienda")
dw_non_conformita_lista.set_dw_key("flag_tipo_nc")
if s_cs_xx.parametri.parametro_s_1 = "NEW" then
   dw_non_conformita_lista.set_dw_options(sqlca,pcca.null_object,c_default, c_default)
   dw_non_conformita_std_1.set_dw_options(sqlca,dw_non_conformita_lista,c_sharedata+c_scrollparent,c_default)
   dw_non_conformita_std_2.set_dw_options(sqlca,dw_non_conformita_lista,c_sharedata+c_scrollparent,c_default)
   dw_non_conformita_std_3.set_dw_options(sqlca,dw_non_conformita_lista,c_sharedata+c_scrollparent,c_default)
   dw_non_conformita_std_4.set_dw_options(sqlca,dw_non_conformita_lista,c_sharedata+c_scrollparent,c_default)
else
   i_anno_nc = s_cs_xx.parametri.parametro_d_1
   i_num_nc = s_cs_xx.parametri.parametro_d_2
   dw_non_conformita_lista.set_dw_options(sqlca,pcca.null_object,c_viewonopen+c_nomodify+c_nodelete+c_nonew, c_default)
   dw_non_conformita_std_1.set_dw_options(sqlca,dw_non_conformita_lista,c_viewonopen+c_nomodify+c_nodelete+c_nonew+c_sharedata+c_scrollparent,c_default)
   dw_non_conformita_std_2.set_dw_options(sqlca,dw_non_conformita_lista,c_viewonopen+c_nomodify+c_nodelete+c_nonew+c_sharedata+c_scrollparent,c_default)
   dw_non_conformita_std_3.set_dw_options(sqlca,dw_non_conformita_lista,c_viewonopen+c_nomodify+c_nodelete+c_nonew+c_sharedata+c_scrollparent,c_default)
   dw_non_conformita_std_4.set_dw_options(sqlca,dw_non_conformita_lista,c_viewonopen+c_nomodify+c_nodelete+c_nonew+c_sharedata+c_scrollparent,c_default)
end if

is_agg_tipo_nc = s_cs_xx.parametri.parametro_s_5
iuo_dw_main = dw_non_conformita_lista

if s_cs_xx.parametri.parametro_s_1 = "NEW" then postevent("pc_new")

end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_trattamento",sqlca,&
                 "tab_trattamenti","cod_trattamento","des_trattamento", &
                 "tab_trattamenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1", &
                 "anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"tipo_prodotto",sqlca,&
                 "tab_tipi_prodotto","tipo_prodotto","desc_prodotto", &
                 "tab_tipi_prodotto.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita", &
                 "tab_difformita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_gestione_nc.create
int iCurrent
call super::create
this.dw_non_conformita_lista=create dw_non_conformita_lista
this.cb_dettagli=create cb_dettagli
this.dw_non_conformita_std_1=create dw_non_conformita_std_1
this.dw_non_conformita_std_4=create dw_non_conformita_std_4
this.dw_folder=create dw_folder
this.cb_doc_aut_deroga=create cb_doc_aut_deroga
this.dw_non_conformita_std_2=create dw_non_conformita_std_2
this.dw_non_conformita_std_3=create dw_non_conformita_std_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_non_conformita_lista
this.Control[iCurrent+2]=this.cb_dettagli
this.Control[iCurrent+3]=this.dw_non_conformita_std_1
this.Control[iCurrent+4]=this.dw_non_conformita_std_4
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.cb_doc_aut_deroga
this.Control[iCurrent+7]=this.dw_non_conformita_std_2
this.Control[iCurrent+8]=this.dw_non_conformita_std_3
end on

on w_gestione_nc.destroy
call super::destroy
destroy(this.dw_non_conformita_lista)
destroy(this.cb_dettagli)
destroy(this.dw_non_conformita_std_1)
destroy(this.dw_non_conformita_std_4)
destroy(this.dw_folder)
destroy(this.cb_doc_aut_deroga)
destroy(this.dw_non_conformita_std_2)
destroy(this.dw_non_conformita_std_3)
end on

type dw_non_conformita_lista from uo_cs_xx_dw within w_gestione_nc
integer x = 2514
integer y = 20
integer width = 777
integer height = 1180
integer taborder = 80
string dataobject = "d_non_conformita_sistema_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione, ll_anno_nc, ll_num_nc

   dw_non_conformita_lista.SetItem (dw_non_conformita_lista.GetRow ( ),&
                                 "anno_non_conf", year(today()) )
   ll_anno = dw_non_conformita_lista.GetItemNumber(dw_non_conformita_lista.GetRow ( ),&
                                 "anno_non_conf" )
   select max(non_conformita.num_non_conf)
     into :ll_num_registrazione
     from non_conformita
     where (non_conformita.cod_azienda = :s_cs_xx.cod_azienda) and (:ll_anno = non_conformita.anno_non_conf);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   dw_non_conformita_lista.SetItem (dw_non_conformita_lista.GetRow ( ),&
                                    "num_non_conf", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      dw_non_conformita_lista.SetItem (dw_non_conformita_lista.GetRow ( ),&
                                   "num_non_conf", 1)
   end if

end if

end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "flag_tipo_nc")) THEN
      SetItem(l_Idx, "flag_tipo_nc", "S")
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, i_anno_nc, i_num_nc)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_saveafter;call uo_cs_xx_dw::pcd_saveafter;if i_extendmode then

   long ll_anno_nc, ll_num_nc
   
   ll_anno_nc = dw_non_conformita_lista.getItemnumber (dw_non_conformita_lista.GetRow ( ), "anno_non_conf")
   ll_num_nc  = dw_non_conformita_lista.getItemnumber (dw_non_conformita_lista.GetRow ( ), "num_non_conf")

   choose case is_agg_tipo_nc
    case "A"
      UPDATE piani_miglioramento  
         SET anno_non_conf = :ll_anno_nc,   
             num_non_conf  = :ll_num_nc  
       WHERE ( piani_miglioramento.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( piani_miglioramento.cod_piano_miglioramento = :s_cs_xx.parametri.parametro_s_2 )   ;
    case "P"
      UPDATE piani_miglioramento  
         SET anno_nc_precedente = :ll_anno_nc,   
             num_nc_precedente  = :ll_num_nc  
       WHERE ( piani_miglioramento.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( piani_miglioramento.cod_piano_miglioramento = :s_cs_xx.parametri.parametro_s_2 )   ;
   end choose
end if
end on

type cb_dettagli from commandbutton within w_gestione_nc
integer x = 2926
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettaglio"
end type

on clicked;if not isvalid(w_gestione_nc_det) then
//   s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
//   s_cs_xx.parametri.parametro_d_2 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")
//   parent.hide()
   window_open_parm(w_gestione_nc_det, -1, dw_non_conformita_lista)
end if

end on

type dw_non_conformita_std_1 from uo_cs_xx_dw within w_gestione_nc
integer x = 69
integer y = 120
integer width = 2377
integer height = 1120
integer taborder = 40
string dataobject = "d_non_conformita_std_1"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_fornitore(dw_non_conformita_std_1,"cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_non_conformita_std_1,"cod_fornitore")
end choose
end event

type dw_non_conformita_std_4 from uo_cs_xx_dw within w_gestione_nc
integer x = 69
integer y = 120
integer width = 2331
integer height = 1120
integer taborder = 50
string dataobject = "d_non_conformita_std_4"
boolean border = false
end type

type dw_folder from u_folder within w_gestione_nc
integer x = 23
integer y = 20
integer width = 2469
integer height = 1280
integer taborder = 90
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Generici"
      SetFocus(dw_non_conformita_std_1)
   CASE "Qta/Decisioni"
      SetFocus(dw_non_conformita_std_2)
   CASE "Correz./Cause"
      SetFocus(dw_non_conformita_std_3)
   CASE "Prevenz./Note"
      SetFocus(dw_non_conformita_std_4)
END CHOOSE



end on

type cb_doc_aut_deroga from cb_documenti_compilati within w_gestione_nc
integer x = 2331
integer y = 460
integer width = 69
integer height = 80
integer taborder = 10
string text = "..."
end type

on clicked;call cb_documenti_compilati::clicked;string ls_ritorno

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "nome_doc_aut_deroga"
s_cs_xx.parametri.parametro_s_2 = "DQ4"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_std_2.getitemstring(dw_non_conformita_std_2.getrow(), "nome_doc_aut_deroga")

if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)

   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
   end if      
else
   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
end if


end on

type dw_non_conformita_std_2 from uo_cs_xx_dw within w_gestione_nc
integer x = 69
integer y = 120
integer width = 2377
integer height = 1140
integer taborder = 70
string dataobject = "d_non_conformita_std_2"
boolean border = false
end type

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;double ld_quantita

ld_quantita = this.getitemnumber(this.getrow(), "quan_non_conformita")

choose case i_colname
case "quan_derogata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_derogata") + long(i_coltext)
case "quan_recuperata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_recuperata") + long(i_coltext)
case "quan_accettabile"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_accettabile") + long(i_coltext)
case "quan_resa"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_resa") + long(i_coltext)
case "quan_rottamata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_rottamata") + long(i_coltext)
case "quan_carico"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_carico") + long(i_coltext)
end choose

this.setitem(this.getrow(),"quan_non_conformita", ld_quantita)


end on

type dw_non_conformita_std_3 from uo_cs_xx_dw within w_gestione_nc
integer x = 69
integer y = 120
integer width = 2354
integer height = 1100
integer taborder = 60
string dataobject = "d_non_conformita_std_3"
boolean border = false
end type

