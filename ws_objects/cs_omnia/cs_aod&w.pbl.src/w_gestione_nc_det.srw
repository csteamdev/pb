﻿$PBExportHeader$w_gestione_nc_det.srw
$PBExportComments$Finestra Gestione Vista e Modifica Dettaglio NC di Sistema da Piani Miglioramento
forward
global type w_gestione_nc_det from w_cs_xx_principale
end type
type cb_prod_ric from cb_prod_ricerca within w_gestione_nc_det
end type
type cb_ric_lavorazione from cb_lavorazione_ricerca within w_gestione_nc_det
end type
type dw_non_conformita_sistema from uo_cs_xx_dw within w_gestione_nc_det
end type
type dw_folder from u_folder within w_gestione_nc_det
end type
end forward

global type w_gestione_nc_det from w_cs_xx_principale
int Width=2350
int Height=949
boolean TitleBar=true
string Title="Dati Specifici Non Conformità Sistema Qualità"
cb_prod_ric cb_prod_ric
cb_ric_lavorazione cb_ric_lavorazione
dw_non_conformita_sistema dw_non_conformita_sistema
dw_folder dw_folder
end type
global w_gestione_nc_det w_gestione_nc_det

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_non_conformita_sistema,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_sistema,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

dw_non_conformita_sistema.set_dw_key("cod_azienda")
dw_non_conformita_sistema.set_dw_key("anno_non_conf")
dw_non_conformita_sistema.set_dw_key("num_non_conf")
dw_non_conformita_sistema.set_dw_options(sqlca,i_openparm,c_modifyonopen+c_nonew+c_nodelete,c_default)
iuo_dw_main = dw_non_conformita_sistema

l_objects[1] = dw_non_conformita_sistema
l_objects[2] = cb_prod_ric
l_objects[3] = cb_ric_lavorazione
dw_folder.fu_AssignTab(1, "Sistema Qualità", l_Objects[])
dw_folder.fu_FolderCreate(1,4)
                          // il primo parametri indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)


end on

on w_gestione_nc_det.create
int iCurrent
call w_cs_xx_principale::create
this.cb_prod_ric=create cb_prod_ric
this.cb_ric_lavorazione=create cb_ric_lavorazione
this.dw_non_conformita_sistema=create dw_non_conformita_sistema
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_prod_ric
this.Control[iCurrent+2]=cb_ric_lavorazione
this.Control[iCurrent+3]=dw_non_conformita_sistema
this.Control[iCurrent+4]=dw_folder
end on

on w_gestione_nc_det.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_prod_ric)
destroy(this.cb_ric_lavorazione)
destroy(this.dw_non_conformita_sistema)
destroy(this.dw_folder)
end on

type cb_prod_ric from cb_prod_ricerca within w_gestione_nc_det
int X=2126
int Y=161
int TabOrder=20
end type

on getfocus;call cb_prod_ricerca::getfocus;dw_non_conformita_sistema.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"

end on

type cb_ric_lavorazione from cb_lavorazione_ricerca within w_gestione_nc_det
int X=2126
int Y=241
int TabOrder=10
end type

on clicked;call cb_lavorazione_ricerca::clicked;dw_non_conformita_sistema.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_lavorazione"
s_cs_xx.parametri.parametro_s_2 = "cod_reparto"
s_cs_xx.parametri.parametro_s_3 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_sistema.getitemstring(dw_non_conformita_sistema.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_sistema.getitemstring(dw_non_conformita_sistema.getrow(),"cod_reparto")
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Lavorazioni","Prodotto Mancante: la ricerca verrà effettuata su un numero elevato di elementi",Information!)
end if
window_open(w_ricerca_lavorazione, 0)
end on

type dw_non_conformita_sistema from uo_cs_xx_dw within w_gestione_nc_det
int X=46
int Y=141
int Width=2195
int Height=641
int TabOrder=30
string DataObject="d_non_conformita_sistema"
boolean Border=false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error, ll_anno_nc, ll_num_nc

ll_anno_nc = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_non_conf")
ll_num_nc  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_non_conf")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_nc, ll_num_nc)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
else

if this.getrow() > 0 then
if len(this.getitemstring(this.getrow(), "cod_reparto")) > 0 then
   f_PO_LoadDDLB_DW(dw_non_conformita_sistema, &
                    "cod_lavorazione", &
                    SQLCA, &
                    "tes_fasi_lavorazione", &
                    "cod_lavorazione" , &
                    "cod_lavorazione" , &
                    "(tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                     (tes_fasi_lavorazione.cod_prodotto = '" + dw_non_conformita_sistema.getitemstring(dw_non_conformita_sistema.getrow(), "cod_prodotto") + "') and &
                     (tes_fasi_lavorazione.cod_reparto = '" + this.getitemstring(this.getrow(), "cod_reparto") + "')" )
end if

if len(this.getitemstring(this.getrow(), "cod_prodotto")) > 0 then
   f_PO_LoadDDDW_DW(dw_non_conformita_sistema, &
                    "cod_reparto", &
                    SQLCA, &
                    "anag_reparti", &
                    "cod_reparto" , &
                    "des_reparto" , &
                    "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                     (anag_reparti.cod_reparto in (SELECT tes_fasi_lavorazione.cod_reparto FROM   tes_fasi_lavorazione WHERE  (tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_fasi_lavorazione.cod_prodotto = '" + this.getitemstring(this.getrow(), "cod_prodotto") + "')))")
end if
end if
end if
end on

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;if i_extendmode then

string ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_null, ls_str

setnull(ls_null)

choose case i_colname
case "cod_prodotto"
   ls_cod_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   if isnull(ls_cod_prodotto) then ls_cod_prodotto = " "
   if (i_coltext <> ls_cod_prodotto) then     ////   and (not isnull(i_coltext))
      g_mb.messagebox("Non Conformita Sistema Qualità", "Prodotto variato: devi reimpostare reparto e lavorazione !", Information!)
      this.setitem(this.getrow(), "cod_reparto", ls_null)
      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
      f_PO_LoadDDDW_DW(dw_non_conformita_sistema, &
                       "cod_reparto", &
                       SQLCA, &
                       "anag_reparti", &
                       "cod_reparto" , &
                       "des_reparto" , &
                       "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (anag_reparti.cod_reparto in (SELECT tes_fasi_lavorazione.cod_reparto FROM   tes_fasi_lavorazione WHERE  (tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_fasi_lavorazione.cod_prodotto = '" + i_coltext + "')))")
   end if

case "cod_reparto"
   ls_cod_reparto = this.getitemstring(this.getrow(), "cod_reparto")
   if isnull(ls_cod_reparto) then ls_cod_reparto = " "
   if (i_coltext <> ls_cod_reparto) or isnull(i_coltext) then
      g_mb.messagebox("Non Conformita Sistema Qualità", "Reparto variato: devi reimpostare la lavorazione !", Information!)
      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
      f_PO_LoadDDLB_DW(dw_non_conformita_sistema, &
                       "cod_lavorazione", &
                       SQLCA, &
                       "tes_fasi_lavorazione", &
                       "cod_lavorazione" , &
                       "cod_lavorazione" , &
                       "(tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (tes_fasi_lavorazione.cod_prodotto = '" + this.getitemstring(this.getrow(), "cod_prodotto") + "') and &
                        (tes_fasi_lavorazione.cod_reparto = '" + i_coltext + "')" )
   end if
end choose

end if  // non toccare : si riferisce al test della variabile i_extendmode

end on

type dw_folder from u_folder within w_gestione_nc_det
int X=23
int Y=21
int Width=2263
int Height=801
int TabOrder=40
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Sistema Qualità"
      SetFocus(dw_non_conformita_sistema)
END CHOOSE

end on

