﻿$PBExportHeader$w_piani_miglioramento.srw
$PBExportComments$Finestra Gestione Piani Miglioramento
forward
global type w_piani_miglioramento from w_cs_xx_principale
end type
type em_data_fine from editmask within w_piani_miglioramento
end type
type em_data_inizio from editmask within w_piani_miglioramento
end type
type cb_analisi_azioni from commandbutton within w_piani_miglioramento
end type
type cb_analisi_nc from commandbutton within w_piani_miglioramento
end type
type cb_apri_nc_1 from commandbutton within w_piani_miglioramento
end type
type cb_apri_nc_2 from commandbutton within w_piani_miglioramento
end type
type cb_confronti from commandbutton within w_piani_miglioramento
end type
type st_2 from statictext within w_piani_miglioramento
end type
type st_3 from statictext within w_piani_miglioramento
end type
type st_5 from statictext within w_piani_miglioramento
end type
type cb_report from commandbutton within w_piani_miglioramento
end type
type dw_piano_miglioramento_lista from uo_cs_xx_dw within w_piani_miglioramento
end type
type gb_report from groupbox within w_piani_miglioramento
end type
type em_data_riferimento from editmask within w_piani_miglioramento
end type
type cb_annulla from commandbutton within w_piani_miglioramento
end type
type dw_piano_miglioramento_det_1 from uo_cs_xx_dw within w_piani_miglioramento
end type
type dw_piano_miglioramento_det_2 from uo_cs_xx_dw within w_piani_miglioramento
end type
type dw_piano_miglioramento_det_3 from uo_cs_xx_dw within w_piani_miglioramento
end type
type dw_folder from u_folder within w_piani_miglioramento
end type
end forward

global type w_piani_miglioramento from w_cs_xx_principale
integer width = 2277
integer height = 1600
string title = "Piani Miglioramento Sistema Qualità"
em_data_fine em_data_fine
em_data_inizio em_data_inizio
cb_analisi_azioni cb_analisi_azioni
cb_analisi_nc cb_analisi_nc
cb_apri_nc_1 cb_apri_nc_1
cb_apri_nc_2 cb_apri_nc_2
cb_confronti cb_confronti
st_2 st_2
st_3 st_3
st_5 st_5
cb_report cb_report
dw_piano_miglioramento_lista dw_piano_miglioramento_lista
gb_report gb_report
em_data_riferimento em_data_riferimento
cb_annulla cb_annulla
dw_piano_miglioramento_det_1 dw_piano_miglioramento_det_1
dw_piano_miglioramento_det_2 dw_piano_miglioramento_det_2
dw_piano_miglioramento_det_3 dw_piano_miglioramento_det_3
dw_folder dw_folder
end type
global w_piani_miglioramento w_piani_miglioramento

type variables
string is_tipo_report
end variables

forward prototypes
public function integer wf_calcola_date ()
end prototypes

public function integer wf_calcola_date ();long     ll_anno_nc, ll_num_nc, ll_anno_nc_prec, ll_num_nc_prec
datetime ld_data_inizio, ld_data_riferimento
datetime ldt_data

if is_tipo_report = "confronti" then
   ll_anno_nc = dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"anno_non_conf")
   ll_num_nc = dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"num_non_conf")

   ll_anno_nc_prec = dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"anno_nc_precedente")
   ll_num_nc_prec = dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"num_nc_precedente")

   if isnull(ll_anno_nc) or (ll_anno_nc < 1) or isnull(ll_num_nc) or (ll_num_nc < 1)then
      g_mb.messagebox("Piani di Miglioramento","Attenzione : Azione Correttiva di riferimento mancante")
      goto fuori
   end if

   if isnull(ll_anno_nc_prec) or (ll_anno_nc_prec < 1) or isnull(ll_num_nc_prec) or (ll_num_nc_prec < 1)then
      g_mb.messagebox("Piani di Miglioramento","Attenzione : Azione Correttiva iniziale mancante")
      goto fuori
   end if

   SELECT non_conformita.data_creazione  
   INTO   :ldt_data  
   FROM   non_conformita  
   WHERE  ( non_conformita.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( non_conformita.anno_non_conf = :ll_anno_nc_prec ) AND  
          ( non_conformita.num_non_conf = :ll_num_nc_prec )   ;
	ld_data_inizio = ldt_data
   SELECT non_conformita.data_creazione  
   INTO   :ldt_data  
   FROM   non_conformita  
   WHERE  ( non_conformita.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( non_conformita.anno_non_conf = :ll_anno_nc ) AND  
          ( non_conformita.num_non_conf = :ll_num_nc )   ;
	ld_data_inizio = ldt_data
   if isnull(ld_data_inizio) then
      g_mb.messagebox("Piani di Miglioramento","Data Creazione inesistente nella Non Conformita "+string(ll_anno_nc_prec)+"/"+string(ll_num_nc_prec))
      goto fuori
   end if

   em_data_inizio.text = string(date(ld_data_inizio), s_cs_xx.db_funzioni.formato_data)
   em_data_riferimento.text = string(date(ld_data_riferimento), s_cs_xx.db_funzioni.formato_data)
end if

dw_piano_miglioramento_lista.visible = false

em_data_fine.visible = true
em_data_inizio.visible = true
em_data_riferimento.visible = true
st_2.visible = true
st_3.visible = true
st_5.visible = true
cb_report.visible = true
cb_annulla.visible = true
cb_analisi_azioni.enabled = true
cb_confronti.enabled = true
cb_analisi_nc.enabled = true
return 0


fuori:
cb_analisi_azioni.enabled = true
cb_confronti.enabled = true
cb_analisi_nc.enabled = true
return -1

end function

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_piano_miglioramento_det_1
dw_folder.fu_AssignTab(1, "&Anagrafici", l_Objects[])

l_objects[1] = dw_piano_miglioramento_det_2
dw_folder.fu_AssignTab(3, "&Annotazioni", l_Objects[])

l_objects[1] = dw_piano_miglioramento_det_3
l_objects[2] = cb_apri_nc_1
l_objects[3] = cb_apri_nc_2
dw_folder.fu_AssignTab(2, "&Selezioni", l_Objects[])

dw_folder.fu_FolderCreate(3,4)

dw_piano_miglioramento_lista.set_dw_key("cod_azienda")
dw_piano_miglioramento_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_piano_miglioramento_det_1.set_dw_options(sqlca,dw_piano_miglioramento_lista,c_sharedata+c_scrollparent,c_default)
dw_piano_miglioramento_det_2.set_dw_options(sqlca,dw_piano_miglioramento_lista,c_sharedata+c_scrollparent,c_default)
dw_piano_miglioramento_det_3.set_dw_options(sqlca,dw_piano_miglioramento_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_piano_miglioramento_lista
dw_folder.fu_SelectTab(1)

em_data_fine.visible = false
em_data_inizio.visible = false
em_data_riferimento.visible = false
st_2.visible = false
st_3.visible = false
st_5.visible = false
cb_report.visible = false
end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_piano_miglioramento_det_1,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome","mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_piano_miglioramento_det_1,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on w_piani_miglioramento.create
int iCurrent
call super::create
this.em_data_fine=create em_data_fine
this.em_data_inizio=create em_data_inizio
this.cb_analisi_azioni=create cb_analisi_azioni
this.cb_analisi_nc=create cb_analisi_nc
this.cb_apri_nc_1=create cb_apri_nc_1
this.cb_apri_nc_2=create cb_apri_nc_2
this.cb_confronti=create cb_confronti
this.st_2=create st_2
this.st_3=create st_3
this.st_5=create st_5
this.cb_report=create cb_report
this.dw_piano_miglioramento_lista=create dw_piano_miglioramento_lista
this.gb_report=create gb_report
this.em_data_riferimento=create em_data_riferimento
this.cb_annulla=create cb_annulla
this.dw_piano_miglioramento_det_1=create dw_piano_miglioramento_det_1
this.dw_piano_miglioramento_det_2=create dw_piano_miglioramento_det_2
this.dw_piano_miglioramento_det_3=create dw_piano_miglioramento_det_3
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_data_fine
this.Control[iCurrent+2]=this.em_data_inizio
this.Control[iCurrent+3]=this.cb_analisi_azioni
this.Control[iCurrent+4]=this.cb_analisi_nc
this.Control[iCurrent+5]=this.cb_apri_nc_1
this.Control[iCurrent+6]=this.cb_apri_nc_2
this.Control[iCurrent+7]=this.cb_confronti
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.st_3
this.Control[iCurrent+10]=this.st_5
this.Control[iCurrent+11]=this.cb_report
this.Control[iCurrent+12]=this.dw_piano_miglioramento_lista
this.Control[iCurrent+13]=this.gb_report
this.Control[iCurrent+14]=this.em_data_riferimento
this.Control[iCurrent+15]=this.cb_annulla
this.Control[iCurrent+16]=this.dw_piano_miglioramento_det_1
this.Control[iCurrent+17]=this.dw_piano_miglioramento_det_2
this.Control[iCurrent+18]=this.dw_piano_miglioramento_det_3
this.Control[iCurrent+19]=this.dw_folder
end on

on w_piani_miglioramento.destroy
call super::destroy
destroy(this.em_data_fine)
destroy(this.em_data_inizio)
destroy(this.cb_analisi_azioni)
destroy(this.cb_analisi_nc)
destroy(this.cb_apri_nc_1)
destroy(this.cb_apri_nc_2)
destroy(this.cb_confronti)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_5)
destroy(this.cb_report)
destroy(this.dw_piano_miglioramento_lista)
destroy(this.gb_report)
destroy(this.em_data_riferimento)
destroy(this.cb_annulla)
destroy(this.dw_piano_miglioramento_det_1)
destroy(this.dw_piano_miglioramento_det_2)
destroy(this.dw_piano_miglioramento_det_3)
destroy(this.dw_folder)
end on

event pc_new;call super::pc_new;dw_piano_miglioramento_det_1.setitem(dw_piano_miglioramento_det_1.getrow(), "data_registrazione", datetime(today()))
dw_piano_miglioramento_det_1.setitem(dw_piano_miglioramento_det_1.getrow(), "data_validita", datetime(today()))

end event

type em_data_fine from editmask within w_piani_miglioramento
integer x = 594
integer y = 340
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "01/01/1900~~31/12/2999"
end type

type em_data_inizio from editmask within w_piani_miglioramento
integer x = 594
integer y = 240
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "01/01/1900~~31/12/2999"
end type

type cb_analisi_azioni from commandbutton within w_piani_miglioramento
integer x = 1851
integer y = 120
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Anal. Azioni"
end type

on clicked;is_tipo_report = "analisi_azioni"
//cb_agg_date.triggerevent("clicked")
cb_analisi_azioni.enabled = false
cb_analisi_nc.enabled = false
cb_confronti.enabled = false
wf_calcola_date()
end on

type cb_analisi_nc from commandbutton within w_piani_miglioramento
integer x = 1851
integer y = 20
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Analisi NC"
end type

on clicked;is_tipo_report = "analisi_nc"
//cb_agg_date.triggerevent("clicked")
cb_analisi_azioni.enabled = false
cb_analisi_nc.enabled = false
cb_confronti.enabled = false
wf_calcola_date()
end on

type cb_apri_nc_1 from commandbutton within w_piani_miglioramento
integer x = 1097
integer y = 1100
integer width = 69
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;long ll_anno_nc, ll_num_nc, ll_risposta, ll_risposta_2
string ls_str

if not isvalid(w_gestione_nc) then
   ll_anno_nc =dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"anno_non_conf")
   ll_num_nc =dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"num_non_conf")
   s_cs_xx.parametri.parametro_s_1 = ""
   ll_risposta = g_mb.messagebox("Piani Miglioramento","Procedo alla creazione di una nuova NC di sistema?",Question!, YesNo!, 1)
   if ll_risposta = 1 then
      if (ll_anno_nc > 0) or (ll_num_nc > 0) then
         ll_risposta_2 = g_mb.messagebox("Piani Miglioramento","Sposto la Non Conformità indicata in precedente?",Question!, YesNo!, 1)
         if ll_risposta_2 = 1 then
            dw_piano_miglioramento_det_3.setitem(dw_piano_miglioramento_det_3.getrow(),"anno_nc_precedente", dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"anno_non_conf"))
            dw_piano_miglioramento_det_3.setitem(dw_piano_miglioramento_det_3.getrow(),"num_nc_precedente",  dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"num_non_conf"))
            parent.triggerevent("pc_save")
         else
            return
         end if
      end if
      s_cs_xx.parametri.parametro_s_1 = "NEW"
      s_cs_xx.parametri.parametro_s_2 = dw_piano_miglioramento_lista.getitemstring(dw_piano_miglioramento_lista.getrow(),"cod_piano_miglioramento")
      s_cs_xx.parametri.parametro_s_3 = "anno_non_conf"
      s_cs_xx.parametri.parametro_s_4 = "num_non_conf"
      s_cs_xx.parametri.parametro_s_5 = "A"
      window_open(w_gestione_nc, -1)
   else
      if (isnull(ll_anno_nc)) or (ll_anno_nc = 0) or (isnull(ll_num_nc)) or (ll_num_nc = 0) then
         return
      else
         s_cs_xx.parametri.parametro_d_1 = ll_anno_nc
         s_cs_xx.parametri.parametro_d_2 = ll_num_nc
         SELECT non_conformita.flag_tipo_nc  
         INTO   :ls_str  
         FROM   non_conformita  
         WHERE ( non_conformita.cod_azienda = :s_cs_xx.cod_azienda ) AND  
               ( non_conformita.anno_non_conf = :ll_anno_nc ) AND  
               ( non_conformita.num_non_conf = :ll_num_nc )   ;
         if sqlca.sqlcode = 0 then
            window_open(w_gestione_nc, -1)
         else
            g_mb.messagebox("Piani Miglioramento","La Non Conformità richiesta risulta inesistente", StopSign!)
         end if
     end if
  end if
end if

end on

type cb_apri_nc_2 from commandbutton within w_piani_miglioramento
integer x = 1097
integer y = 1280
integer width = 69
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;long ll_anno_nc, ll_num_nc, ll_risposta
string ls_str

if not isvalid(w_gestione_nc) then
   ll_anno_nc =dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"anno_nc_precedente")
   ll_num_nc =dw_piano_miglioramento_det_3.getitemnumber(dw_piano_miglioramento_det_3.getrow(),"num_nc_precedente")
   s_cs_xx.parametri.parametro_s_1 = ""
   if (isnull(ll_anno_nc)) or (ll_anno_nc = 0) or (isnull(ll_num_nc)) or (ll_num_nc = 0) then
      return
   else
      s_cs_xx.parametri.parametro_d_1 = ll_anno_nc
      s_cs_xx.parametri.parametro_d_2 = ll_num_nc

      SELECT non_conformita.flag_tipo_nc  
      INTO   :ls_str  
      FROM   non_conformita  
      WHERE ( non_conformita.cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( non_conformita.anno_non_conf = :ll_anno_nc ) AND  
            ( non_conformita.num_non_conf = :ll_num_nc )   ;
      if sqlca.sqlcode = 0 then
         window_open(w_gestione_nc, -1)
      else
         g_mb.messagebox("Piani Miglioramento","La Non Conformità richiesta risulta inesistente", StopSign!)
      end if
   end if
end if

end on

type cb_confronti from commandbutton within w_piani_miglioramento
integer x = 1851
integer y = 220
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Confronti"
end type

on clicked;is_tipo_report = "confronti"
//cb_agg_date.triggerevent("clicked")
cb_analisi_azioni.enabled = false
cb_analisi_nc.enabled = false
cb_confronti.enabled = false
wf_calcola_date()
end on

type st_2 from statictext within w_piani_miglioramento
integer x = 274
integer y = 260
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Inizio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_piani_miglioramento
integer x = 320
integer y = 360
integer width = 274
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Fine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_piani_miglioramento
integer x = 133
integer y = 160
integer width = 462
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Riferimento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_piani_miglioramento
integer x = 1166
integer y = 340
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report >>>"
end type

event clicked;integer li_len, li_i
string  ls_str, ls_cod_prodotto, ls_area, ls_raggruppo, ls_frequenza


choose case is_tipo_report

case "confronti"
s_cs_xx.parametri.parametro_s_1 = dw_piano_miglioramento_det_1.getitemstring(dw_piano_miglioramento_det_1.getrow(), "cod_prodotto")
if isnull(s_cs_xx.parametri.parametro_s_1)then s_cs_xx.parametri.parametro_s_1="%"

s_cs_xx.parametri.parametro_data_1 = datetime(em_data_inizio.text)
if isnull(s_cs_xx.parametri.parametro_data_1) or (s_cs_xx.parametri.parametro_data_1 < datetime("01/01/1900")) then
   g_mb.messagebox("Selezioni Piano Miglioramento", "Digitare una data inizio valida")
   goto fine
end if

s_cs_xx.parametri.parametro_data_2 = datetime(em_data_fine.text)
if isnull(s_cs_xx.parametri.parametro_data_2) or (s_cs_xx.parametri.parametro_data_2 < datetime("01/01/1900")) then
   g_mb.messagebox("Selezioni Piano Miglioramento", "Digitare una data fine valida")
   goto fine
end if

s_cs_xx.parametri.parametro_data_3 = datetime(em_data_riferimento.text)
if isnull(s_cs_xx.parametri.parametro_data_3) or (s_cs_xx.parametri.parametro_data_3 < datetime("01/01/1900")) then
   g_mb.messagebox("Selezioni Piano Miglioramento", "Digitare una data di riferimento valida")
   goto fine
end if

if s_cs_xx.parametri.parametro_data_1 >= s_cs_xx.parametri.parametro_data_2 then
   g_mb.messagebox("Selezioni Piano Miglioramento", "ATTENZIONE! La data di inizio non può essere uguale o maggiore della di riferimento")
   goto fine
end if
if s_cs_xx.parametri.parametro_data_3 >= s_cs_xx.parametri.parametro_data_2 then
   g_mb.messagebox("Selezioni Piano Miglioramento", "ATTENZIONE! La data di riferimento non può essere uguale o maggiore della di fine")
   goto fine
end if

ls_area =      dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_area_pm")
ls_raggruppo = dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_raggruppo")
ls_frequenza = dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_tipo_pm")

//if (ls_area = "A") and (ls_raggruppo = "R") and (ls_frequenza = "E") then
//   if not isvalid(w_piano_migl_for_difetto_nc) then
//      window_open(w_piano_migl_for_difetto_nc, -1)
//   end if
//end if

if (ls_area = "A") and (ls_raggruppo = "E") and (ls_frequenza = "R") then
   if not isvalid(w_piano_migl_difetto_for_conf) then
      window_open(w_piano_migl_difetto_for_conf, -1)
   end if
end if

case "analisi_nc"

s_cs_xx.parametri.parametro_s_1 = dw_piano_miglioramento_det_1.getitemstring(dw_piano_miglioramento_det_1.getrow(), "cod_prodotto")
if isnull(s_cs_xx.parametri.parametro_s_1)then s_cs_xx.parametri.parametro_s_1="%"

s_cs_xx.parametri.parametro_data_1 = datetime(em_data_inizio.text)
if isnull(s_cs_xx.parametri.parametro_data_1) or (s_cs_xx.parametri.parametro_data_1 < datetime("01/01/1900")) then
   g_mb.messagebox("Selezioni Piano Miglioramento", "Digitare una data inizio valida")
   goto fine
end if
s_cs_xx.parametri.parametro_data_2 = datetime(em_data_fine.text)
if isnull(s_cs_xx.parametri.parametro_data_2) or (s_cs_xx.parametri.parametro_data_2 < datetime("01/01/1900")) then
   g_mb.messagebox("Selezioni Piano Miglioramento", "Digitare una data fine valida")
   goto fine
end if

if s_cs_xx.parametri.parametro_data_1 >= s_cs_xx.parametri.parametro_data_2 then
   g_mb.messagebox("Selezioni Piano Miglioramento", "ATTENZIONE! La data di inizio non può essere uguale o maggiore della di riferimento")
   goto fine
end if

ls_area =      dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_area_pm")
ls_raggruppo = dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_raggruppo")
ls_frequenza = dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_tipo_pm")

if (ls_area = "A") and (ls_raggruppo = "R") and (ls_frequenza = "E") then
   if not isvalid(w_piano_migl_for_difetto) then
      window_open(w_piano_migl_for_difetto, -1)
   end if
end if

if (ls_area = "A") and (ls_raggruppo = "E") and (ls_frequenza = "R") then
   if not isvalid(w_piano_migl_difetto_for) then
      window_open(w_piano_migl_difetto_for, -1)
   end if
end if

case "analisi_azioni"
s_cs_xx.parametri.parametro_s_1 = dw_piano_miglioramento_det_1.getitemstring(dw_piano_miglioramento_det_1.getrow(), "cod_prodotto")
if isnull(s_cs_xx.parametri.parametro_s_1)then s_cs_xx.parametri.parametro_s_1="%"

s_cs_xx.parametri.parametro_data_1 = datetime(em_data_inizio.text)
if isnull(s_cs_xx.parametri.parametro_data_1) or (s_cs_xx.parametri.parametro_data_1 < datetime("01/01/1900")) then
   g_mb.messagebox("Selezioni Piano Miglioramento", "Digitare una data inizio valida")
   goto fine
end if
s_cs_xx.parametri.parametro_data_2 = datetime(em_data_fine.text)
if isnull(s_cs_xx.parametri.parametro_data_2) or (s_cs_xx.parametri.parametro_data_2 < datetime("01/01/1900")) then
   g_mb.messagebox("Selezioni Piano Miglioramento", "Digitare una data fine valida")
   goto fine
end if

if s_cs_xx.parametri.parametro_data_1 >= s_cs_xx.parametri.parametro_data_2 then
   g_mb.messagebox("Selezioni Piano Miglioramento", "ATTENZIONE! La data di inizio non può essere uguale o maggiore della di riferimento")
   goto fine
end if

ls_area =      dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_area_pm")
ls_raggruppo = dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_raggruppo")
ls_frequenza = dw_piano_miglioramento_det_3.getitemstring(dw_piano_miglioramento_det_3.getrow(), "flag_tipo_pm")

//if (ls_area = "A") and (ls_raggruppo = "R") and (ls_frequenza = "E") then
//   if not isvalid(w_piano_migl_for_difetto_nc) then
//      window_open(w_piano_migl_for_difetto_nc, -1)
//   end if
//end if

if (ls_area = "A") and (ls_raggruppo = "E") and (ls_frequenza = "R") then
   if not isvalid(w_piano_migl_difetto_for_nc) then
      window_open(w_piano_migl_difetto_for_nc, -1)
   end if
end if

end choose


fine:
em_data_fine.hide()
em_data_inizio.hide()
em_data_riferimento.hide()
st_2.hide()
st_3.hide()
st_5.hide()
cb_report.hide()
cb_analisi_azioni.enabled = true
cb_confronti.enabled = true
cb_analisi_nc.enabled = true
dw_piano_miglioramento_lista.visible = true
end event

type dw_piano_miglioramento_lista from uo_cs_xx_dw within w_piani_miglioramento
integer x = 23
integer y = 20
integer width = 1806
integer height = 500
integer taborder = 10
string dataobject = "d_piano_miglioramento_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_analisi_azioni.enabled = false
cb_confronti.enabled = false
cb_analisi_nc.enabled = false
cb_apri_nc_1.enabled = false
cb_apri_nc_2.enabled = false

end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_analisi_azioni.enabled = false
cb_confronti.enabled = false
cb_analisi_nc.enabled = false
cb_apri_nc_1.enabled = false
cb_apri_nc_2.enabled = false

end on

on pcd_view;call uo_cs_xx_dw::pcd_view;cb_analisi_azioni.enabled = true
cb_confronti.enabled = true
cb_analisi_nc.enabled = true
cb_apri_nc_1.enabled = true
cb_apri_nc_2.enabled = true

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_saveafter;call uo_cs_xx_dw::pcd_saveafter;parent.triggerevent("pc_setddlb")
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type gb_report from groupbox within w_piani_miglioramento
integer x = 23
integer y = 40
integer width = 1600
integer height = 480
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Selezioni Report Piani Miglioramento"
borderstyle borderstyle = stylelowered!
end type

type em_data_riferimento from editmask within w_piani_miglioramento
integer x = 594
integer y = 140
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "01/01/1900~~31/12/2999"
end type

type cb_annulla from commandbutton within w_piani_miglioramento
integer x = 1166
integer y = 240
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

on clicked;em_data_fine.hide()
em_data_inizio.hide()
em_data_riferimento.hide()
st_2.hide()
st_3.hide()
st_5.hide()
cb_report.hide()
cb_analisi_azioni.enabled = true
cb_confronti.enabled = true
cb_analisi_nc.enabled = true
dw_piano_miglioramento_lista.visible = true
end on

type dw_piano_miglioramento_det_1 from uo_cs_xx_dw within w_piani_miglioramento
integer x = 69
integer y = 640
integer width = 2126
integer height = 760
integer taborder = 140
string dataobject = "d_piano_miglioramento_det_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;string   ls_cod_divisione, ls_str
datetime ld_data

if i_extendmode then
ld_data = datetime(i_coltext)
ls_str = i_coltext

choose case i_colname

case "data_validita"
if len(ls_str) < 1 then
   g_mb.messagebox("Piani Miglioramento", "Inserire una data di validità")
   pcca.error = c_fatal 
   return
end if
if ld_data < getitemdatetime(getrow(), "data_registrazione")then
   g_mb.messagebox("Piani Miglioramento", "ATTENZIONE: data validità minore della data registrazione")
   pcca.error = c_fatal 
   return
end if

case "data_registrazione"
if isnull(i_coltext) then
   g_mb.messagebox("Piani Miglioramento", "Inserire una data di registrazione")
   pcca.error = c_fatal 
   return
end if
if getitemdatetime(getrow(), "data_validita") < datetime(i_coltext) then
   g_mb.messagebox("Piani Miglioramento", "ATTENZIONE: data validità minore della data registrazione")
   pcca.error = c_fatal 
   return
end if

case "cod_resp_divisione"
   if isnull(i_coltext) then
      g_mb.messagebox("Piani di Miglioramento", "Codice Responsabile Obbligatorio", stopsign!)
      pcca.error = c_fatal
   end if

end choose 

end if   ////  relativo al controllo i_extendmode

end event

event pcd_validaterow;call super::pcd_validaterow;string   ls_cod_divisione
datetime ld_data

if i_extendmode then

ld_data = this.getitemdatetime(this.getrow(), "data_validita")
if isnull(string(ld_data)) then
   g_mb.messagebox("Piani Miglioramento", "Inserire una data di validità")
   pcca.error = c_fatal 
   return
end if

if getitemdatetime(getrow(), "data_validita") < getitemdatetime(getrow(), "data_registrazione")then
   g_mb.messagebox("Piani Miglioramento", "ATTENZIONE: data validità minore della data registrazione")
   pcca.error = c_fatal 
   return
end if

if isnull(getitemstring(getrow(), "cod_resp_divisione")) then
      g_mb.messagebox("Piani di Miglioramento", "Codice Responsabile Obbligatorio", stopsign!)
      pcca.error = c_fatal
      return
end if

end if

end event

type dw_piano_miglioramento_det_2 from uo_cs_xx_dw within w_piani_miglioramento
integer x = 46
integer y = 640
integer width = 2103
integer height = 820
integer taborder = 160
string dataobject = "d_piano_miglioramento_det_2"
boolean border = false
end type

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;if i_extendmode then

string ls_cod_divisione, ls_cod_utente
choose case i_colname

case "cod_area_aziendale"
   select tab_aree_aziendali.cod_divisione
   into :ls_cod_divisione
   from tab_aree_aziendali
   where (tab_aree_aziendali.cod_azienda = :s_cs_xx.cod_azienda) and
         (tab_aree_aziendali.cod_area_aziendale = :i_coltext);
   if ls_cod_divisione <> this.getitemstring(this.getrow(), "cod_divisione") then
      g_mb.messagebox("Anagrafica Mansionari", "Il codice divisione inserito non è valido", stopsign!)
      pcca.error = c_fatal
   end if

case "cod_utente"
   if isnull(i_coltext) then
      g_mb.messagebox("Anagrafica Mansionari", "Codice Utente Obbligatorio", stopsign!)
      pcca.error = c_fatal
   else
      select mansionari.cod_utente
      into :ls_cod_utente
      from mansionari
      where (mansionari.cod_azienda = :s_cs_xx.cod_azienda) and
            (mansionari.cod_utente = :i_coltext);
      if sqlca.sqlcode = 0 then
         g_mb.messagebox("Anagrafica Mansionari", "Questo codice utente è già presente in altri mansionari", stopsign!)
         pcca.error = c_fatal
      end if         

   end if

end choose 

end if   ////  relativo al controllo i_extendmode

end on

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;//if i_extendmode then
//
//string ls_cod_divisione, ls_cod_utente
//choose case i_colname
//
//case "cod_area_aziendale"
//   select tab_aree_aziendali.cod_divisione
//   into :ls_cod_divisione
//   from tab_aree_aziendali
//   where (tab_aree_aziendali.cod_azienda = :s_cs_xx.cod_azienda) and
//         (tab_aree_aziendali.cod_area_aziendale = :i_coltext);
//   if sqlca.sqlcode = 0 then
//      if isnull(ls_cod_divisione) then  g_mb.messagebox("Anagrafica Mansionari", "Non hai indicato nessuna area di apparteneza nell'area "+i_colname, StopSign!)
//      this.setitem(this.getrow(), "cod_divisione", ls_cod_divisione)
//   end if
//case "cod_utente"
//   if isnull(i_coltext) then
//      g_mb.messagebox("Anagrafica Mansionari", "Codice Utente Obbligatorio", stopsign!)
//      pcca.error = c_fatal
//   else
//      select mansionari.cod_utente
//      into :ls_cod_utente
//      from mansionari
//      where (mansionari.cod_azienda = :s_cs_xx.cod_azienda) and
//            (mansionari.cod_utente = :i_coltext);
//      if sqlca.sqlcode = 0 then
//         g_mb.messagebox("Anagrafica Mansionari", "Questo codice utente è già presente in altri mansionari", stopsign!)
//         pcca.error = c_fatal
//      end if         
//
//   end if
//end choose
//
//end if
//
end on

type dw_piano_miglioramento_det_3 from uo_cs_xx_dw within w_piani_miglioramento
integer x = 69
integer y = 640
integer width = 2103
integer height = 820
integer taborder = 150
string dataobject = "d_piano_miglioramento_det_3"
boolean border = false
end type

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;if i_extendmode then

string ls_tipo_raggruppamento, ls_tipo_frequenza

ls_tipo_raggruppamento = this.getitemstring(this.getrow(), "flag_raggruppo")
ls_tipo_frequenza  = this.getitemstring(this.getrow(), "flag_tipo_pm")

if ls_tipo_raggruppamento = ls_tipo_frequenza then
   g_mb.messagebox("Piani Miglioramento", "Tipo di raggruppamento e tipo frequenza non possono coincidere")
   pcca.error = c_fatal
   return
end if

end if

end on

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;if i_extendmode then

string ls_tipo_raggruppamento, ls_tipo_frequenza
ls_tipo_raggruppamento = this.getitemstring(this.getrow(), "flag_raggruppo")
ls_tipo_frequenza  = this.getitemstring(this.getrow(), "flag_tipo_pm")

choose case i_colname

case "flag_raggruppo"
if i_coltext = ls_tipo_frequenza then
   g_mb.messagebox("Piani Miglioramento", "Tipo di raggruppamento e tipo frequenza non possono coincidere")
   pcca.error = c_valfailed
   return
end if

case "flag_tipo_pm"
if ls_tipo_raggruppamento = i_coltext then
   g_mb.messagebox("Piani Miglioramento", "Tipo di raggruppamento e tipo frequenza non possono coincidere")
   pcca.error = c_valfailed
   return
end if
end choose

end if

end on

type dw_folder from u_folder within w_piani_miglioramento
integer x = 23
integer y = 540
integer width = 2194
integer height = 940
integer taborder = 130
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Anagrafici"
      SetFocus(dw_piano_miglioramento_det_1)
   CASE "&Selezioni"
      SetFocus(dw_piano_miglioramento_det_3)
   CASE "&Annotazioni"
      SetFocus(dw_piano_miglioramento_det_2)
//   CASE "&Report"
//      em_data_fine.visible = false
//      em_data_inizio.visible = false
//      em_data_riferimento.visible = false
//      st_2.visible = false
//      st_3.visible = false
//      st_5.visible = false
//      cb_agg_date.visible = false
//      cb_report.visible = false
END CHOOSE

end on

