﻿$PBExportHeader$w_piano_migl_difetto_for_nc.srw
$PBExportComments$Report Vista Non Conformita che hanno un certo difetto
forward
global type w_piano_migl_difetto_for_nc from w_cs_xx_principale
end type
type dw_piano_migl_difetto_for_nc from uo_cs_xx_dw within w_piano_migl_difetto_for_nc
end type
end forward

global type w_piano_migl_difetto_for_nc from w_cs_xx_principale
int Width=3557
int Height=1649
boolean TitleBar=true
string Title="Report Piano di Miglioramento"
dw_piano_migl_difetto_for_nc dw_piano_migl_difetto_for_nc
end type
global w_piano_migl_difetto_for_nc w_piano_migl_difetto_for_nc

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_piano_migl_difetto_for_nc.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nomodify+ &
                                         c_nodelete+c_disableCC+c_disableCCinsert,c_default)

iuo_dw_main = dw_piano_migl_difetto_for_nc
end on

on w_piano_migl_difetto_for_nc.create
int iCurrent
call w_cs_xx_principale::create
this.dw_piano_migl_difetto_for_nc=create dw_piano_migl_difetto_for_nc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_piano_migl_difetto_for_nc
end on

on w_piano_migl_difetto_for_nc.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_piano_migl_difetto_for_nc)
end on

type dw_piano_migl_difetto_for_nc from uo_cs_xx_dw within w_piano_migl_difetto_for_nc
int X=23
int Y=21
int Width=3475
int Height=1501
string DataObject="r_piano_migl_difetto_for_nc"
boolean HScrollBar=true
boolean VScrollBar=true
boolean Resizable=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,  &
                   "A", &
                   s_cs_xx.parametri.parametro_s_1, &
                   s_cs_xx.parametri.parametro_data_1, &
                   s_cs_xx.parametri.parametro_data_2)
//l_Error = Retrieve(s_cs_xx.cod_azienda,  &
//                   "A", &
//                   s_cs_xx.parametri.parametro_s_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

