﻿$PBExportHeader$w_report_acc_mat_1.srw
$PBExportComments$Report Materiali Accettati
forward
global type w_report_acc_mat_1 from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_acc_mat_1
end type
type cb_report from commandbutton within w_report_acc_mat_1
end type
type cb_selezione from commandbutton within w_report_acc_mat_1
end type
type dw_sel_report_acc_mat_1 from uo_cs_xx_dw within w_report_acc_mat_1
end type
type dw_report_acc_mat_1 from uo_cs_xx_dw within w_report_acc_mat_1
end type
end forward

global type w_report_acc_mat_1 from w_cs_xx_principale
integer width = 3936
integer height = 2204
string title = "Report Materiali Accettati"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_sel_report_acc_mat_1 dw_sel_report_acc_mat_1
dw_report_acc_mat_1 dw_report_acc_mat_1
end type
global w_report_acc_mat_1 w_report_acc_mat_1

event pc_setwindow;call super::pc_setwindow;dw_report_acc_mat_1.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_acc_mat_1.set_dw_options(sqlca, &
                                   pcca.null_object, &
                                   c_nomodify + &
                                   c_nodelete + &
                                   c_newonopen + &
                                   c_disableCC, &
                                   c_noresizedw + &
                                   c_nohighlightselected + &
                                   c_nocursorrowpointer +&
                                   c_nocursorrowfocusrect )

dw_sel_report_acc_mat_1.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
                                       c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_cursorrowpointer)
iuo_dw_main = dw_report_acc_mat_1

this.x = 741
this.y = 885
this.width = 2172
this.height = 633

end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_sel_report_acc_mat_1,"rs_cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_sel_report_acc_mat_1,"rs_cat_prodotto",sqlca,&
                 "tab_cat_mer","cod_cat_mer","des_cat_mer", &
                 "tab_cat_mer.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

on w_report_acc_mat_1.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_sel_report_acc_mat_1=create dw_sel_report_acc_mat_1
this.dw_report_acc_mat_1=create dw_report_acc_mat_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_sel_report_acc_mat_1
this.Control[iCurrent+5]=this.dw_report_acc_mat_1
end on

on w_report_acc_mat_1.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_sel_report_acc_mat_1)
destroy(this.dw_report_acc_mat_1)
end on

event getfocus;call super::getfocus;dw_sel_report_acc_mat_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "rs_cod_fornitore"

end event

type cb_annulla from commandbutton within w_report_acc_mat_1
integer x = 1349
integer y = 440
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_acc_mat_1
integer x = 1737
integer y = 440
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string   ls_prodotto, ls_cat_mer, ls_cod_fornitore
datetime ld_da_data, ld_a_data

dw_sel_report_acc_mat_1.accepttext()

ls_prodotto = dw_sel_report_acc_mat_1.getitemstring(1,"rs_cod_prodotto")
ls_cat_mer  = dw_sel_report_acc_mat_1.getitemstring(1,"rs_cat_prodotto")
ld_da_data  = dw_sel_report_acc_mat_1.getitemdatetime(1,"rd_da_data")
ld_a_data   = dw_sel_report_acc_mat_1.getitemdatetime(1,"rd_a_data")
ls_cod_fornitore = dw_sel_report_acc_mat_1.getitemstring(1,"rs_cod_fornitore")

if isnull(ls_prodotto) then ls_prodotto = "%"
if isnull(ls_cat_mer) then  ls_cat_mer  = "%"
if isnull(ld_da_data) then  ld_da_data  = datetime(date("01/01/1900"))
if isnull(ld_a_data)  then  ld_a_data   = datetime(date("31/12/2999"))

if ls_cod_fornitore = "" then  setnull(ls_cod_fornitore)

dw_sel_report_acc_mat_1.hide()

parent.x = 100
parent.y = 50
parent.width = 3900 //3553
parent.height = 2150 //1665

dw_report_acc_mat_1.show()
dw_report_acc_mat_1.retrieve(s_cs_xx.cod_azienda, ls_prodotto, ld_da_data, ld_a_data, ls_cat_mer, ls_cod_fornitore)
cb_selezione.show()

dw_report_acc_mat_1.object.datawindow.print.preview = "yes"

dw_report_acc_mat_1.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_acc_mat_1
integer x = 3392
integer y = 1948
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string   ls_prodotto, ls_cat_mer
datetime ld_da_data, ld_a_data

dw_sel_report_acc_mat_1.show()

parent.x = 741
parent.y = 885
parent.width = 2172
parent.height = 633

dw_report_acc_mat_1.hide()
cb_selezione.hide()
dw_sel_report_acc_mat_1.object.b_ricerca_prodotto.visible=true
dw_sel_report_acc_mat_1.object.b_ricerca_fornitore.visible=true

dw_sel_report_acc_mat_1.change_dw_current()
end event

type dw_sel_report_acc_mat_1 from uo_cs_xx_dw within w_report_acc_mat_1
integer x = 23
integer y = 20
integer width = 2080
integer height = 380
integer taborder = 20
string dataobject = "d_sel_report_acc_mat_1"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;string ls_cod_cat_mer, ls_null

choose case i_colname
	case "rs_cod_prodotto"
		if not isnull(i_coltext) then
			SELECT anag_prodotti.cod_cat_mer  
			INTO   :ls_cod_cat_mer  
			FROM   anag_prodotti  
			WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			       ( anag_prodotti.cod_prodotto = :i_coltext )   ;
			if sqlca.sqlcode = 0 then
				setitem(1,"rs_cat_prodotto", ls_cod_cat_mer)
			end if
		end if
	case "rs_cat_prodotto"
		setnull(ls_null)
		setitem(1,"rs_cod_prodotto", ls_null)
end choose	
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_sel_report_acc_mat_1,"rs_cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_sel_report_acc_mat_1,"rs_cod_fornitore")
end choose
end event

type dw_report_acc_mat_1 from uo_cs_xx_dw within w_report_acc_mat_1
boolean visible = false
integer x = 23
integer y = 20
integer width = 3771
integer height = 1896
integer taborder = 10
string dataobject = "d_report_acc_mat_1"
boolean hscrollbar = true
boolean vscrollbar = true
end type

