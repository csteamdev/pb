﻿$PBExportHeader$w_report_esecuzione_campionamenti.srw
$PBExportComments$Finestra Report Elenco Fatture con contropartite
forward
global type w_report_esecuzione_campionamenti from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_esecuzione_campionamenti
end type
end forward

global type w_report_esecuzione_campionamenti from w_cs_xx_principale
integer width = 3758
integer height = 1900
string title = "Report Esecuzione Campionamenti"
dw_report dw_report
end type
global w_report_esecuzione_campionamenti w_report_esecuzione_campionamenti

forward prototypes
public subroutine wf_report (decimal fd_anno_registrazione, decimal fd_num_registrazione)
end prototypes

public subroutine wf_report (decimal fd_anno_registrazione, decimal fd_num_registrazione);string ls_rif_bol_acq, ls_cod_fornitore, ls_cod_prodotto, ls_flag_eseguito_campionamento,ls_cod_piano_campionamento, &
       ls_fornitore_old, ls_prodotto_old, ls_rag_soc_1, ls_des_prodotto, ls_azienda,ls_cod_deposito,ls_cod_ubicazione, &
		 ls_cod_lotto, ls_cod_test, ls_des_test,ls_des_piano_campionamento,ls_cod_tipologia_campionamento, &
		 ls_des_tipologia_campionamento,ls_des_campionamento
long ll_riga, ll_prog_stock, ll_anno_reg_campionamento, ll_num_reg_campionamento, ll_cont, &
     ll_prog_piano_campionamento
dec{4} ld_quan_arrivata, ld_quan_cumulativa,ld_quan_limite_campionamento
datetime ldt_data_eff_consegna, ldt_data_validita,ldt_data_stock
 
SELECT rif_bol_acq,   
		 data_eff_consegna,   
		 anno_reg_campionamenti,   
		 num_reg_campionamenti,   
		 quan_arrivata,   
		 cod_fornitore,   
		 cod_prodotto,
		 cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 data_stock,
		 prog_stock,
		 nome_doc_cq
into   :ls_rif_bol_acq, 
       :ldt_data_eff_consegna, 
		 :ll_anno_reg_campionamento, 
		 :ll_num_reg_campionamento, 
		 :ld_quan_arrivata,
		 :ls_cod_fornitore, 
		 :ls_cod_prodotto,
		 :ls_cod_deposito,
		 :ls_cod_ubicazione,
		 :ls_cod_lotto,
		 :ldt_data_stock,
		 :ll_prog_stock,
		 :ls_cod_tipologia_campionamento
FROM   acc_materiali  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
       anno_acc_materiali = :fd_anno_registrazione and
		 num_acc_materiali = :fd_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore ricerca della registrazione di acctetazione materiali." + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ll_anno_reg_campionamento) or ll_anno_reg_campionamento = 0 then
	g_mb.messagebox("OMNIA","A questa accettazione non è stato associato alcun campionamento.")
	return
end if

dw_report.reset()

select rag_soc_1
into   :ls_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;
	
select rag_soc_1
into   :ls_rag_soc_1
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_fornitore = :ls_cod_fornitore;
			 
select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;
			 
select des_resa
into   :ls_des_tipologia_campionamento
from   tab_rese
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_resa = :ls_cod_tipologia_campionamento;
			 
setnull(ls_cod_piano_campionamento)

select cod_piano_campionamento
into   :ls_cod_piano_campionamento
from   tes_piani_campionamento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_fornitore = :ls_cod_fornitore
group by cod_piano_campionamento;
if isnull(ls_cod_piano_campionamento) then
	g_mb.messagebox("OMNIA","Per il fornitore " + ls_cod_fornitore + " non c'è alcun piano di campionamento.")
end if

select max(data_validita)
into   :ldt_data_validita
from   tes_piani_campionamento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_piano_campionamento = :ls_cod_piano_campionamento and
		 cod_fornitore = :ls_cod_fornitore;
	
select max(prog_piano_campionamento)
into   :ll_prog_piano_campionamento
from   tes_piani_campionamento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_piano_campionamento = :ls_cod_piano_campionamento and
		 cod_fornitore = :ls_cod_fornitore and
		 data_validita = :ldt_data_validita;
	
select des_piano_campionamento
into   :ls_des_piano_campionamento
from   tes_piani_campionamento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_piano_campionamento = :ls_cod_piano_campionamento and
		 data_validita = :ldt_data_validita and 
		 prog_piano_campionamento = :ll_prog_piano_campionamento;
		 
//if sqlca.sqlcode <> 0 then
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore "+string(sqlca.sqlcode)+" in ricerca della descrizione del campionamento nei piani di campionamento.~r~n" + sqlca.sqlerrtext + "~r~nQuesta accettazione verrà saltata!")
	return
end if

//declare cu_piani_camp cursor for  
//select  cod_test
// from   det_piani_campionamento
//where   cod_azienda = :s_cs_xx.cod_azienda and  
//		  cod_piano_campionamento = :ls_cod_piano_campionamento and  
//		  data_validita = :ldt_data_validita and  
//		  cod_prodotto = :ls_cod_prodotto ;
//open cu_piani_camp;
//if sqlca.sqlcode <> 0 then
//	messagebox("OMNIA","Errore nella open del curosre cu_piani_camp~r~n "+string(sqlca.sqlerrtext))
//	return
//end if
//
//ll_cont = 0
//do while true
	
	ll_riga = dw_report.insertrow(0)
	dw_report.setitem(ll_riga,"azienda", ls_azienda)
	dw_report.setitem(ll_riga,"cod_fornitore", ls_cod_fornitore)
	dw_report.setitem(ll_riga,"rag_soc_1", ls_rag_soc_1)
	dw_report.setitem(ll_riga,"cod_prodotto", ls_cod_prodotto)
	dw_report.setitem(ll_riga,"des_prodotto", ls_des_prodotto)
	dw_report.setitem(ll_riga,"ddt", ls_rif_bol_acq)
	dw_report.setitem(ll_riga,"data_ddt", ldt_data_eff_consegna)
	dw_report.setitem(ll_riga,"cod_deposito", ls_cod_deposito)
	dw_report.setitem(ll_riga,"cod_ubicazione", ls_cod_ubicazione)
	dw_report.setitem(ll_riga,"cod_lotto", ls_cod_lotto)
	dw_report.setitem(ll_riga,"data_stock", ldt_data_stock)
	dw_report.setitem(ll_riga,"prog_stock", ll_prog_stock)
	dw_report.setitem(ll_riga,"cod_piano_campionamento", ls_cod_piano_campionamento)
	dw_report.setitem(ll_riga,"des_piano_campionamento", ls_des_piano_campionamento)
	dw_report.setitem(ll_riga,"anno_reg_campionamento", ll_anno_reg_campionamento)
	dw_report.setitem(ll_riga,"num_reg_campionamento", ll_num_reg_campionamento)
	dw_report.setitem(ll_riga,"des_tipologia_campionamento", ls_cod_tipologia_campionamento + " " + ls_des_tipologia_campionamento)
//	dw_report.setitem(ll_riga,"cod_test", "")
//	dw_report.setitem(ll_riga,"des_test", "")
	
//	select des_campionamento
//	into   :ls_des_campionamento
//	from  tes_campionamenti
//	where cod_azienda = :s_cs_xx.cod_azienda and
//	      anno_reg_campionamenti = :ll_anno_reg_campionamento and
//	      num_reg_campionamenti = :ll_num_reg_campionamento;
//			
//	dw_report.setitem(ll_riga,"des_campionamento", ls_des_campionamento)
//	
//	fetch cu_piani_camp into :ls_cod_test;
//	if sqlca.sqlcode = 100 then
//		if ll_cont = 0 then
//			messagebox("OMNIA","Non è stato trovato alcun test per questo prodotto nei piani di campionamento!")
//			rollback;
//			dw_report.Object.DataWindow.Print.preview = 'Yes'
//			return
//		end if
//		exit
//	end if
//	if sqlca.sqlcode = -1 then
//		messagebox("OMNIA","Errore inatteso durante la fetch del cursore cu_piani_camp.~r~n" + sqlca.sqlerrtext)
//		rollback;
//		return
//	end if
//	
//	ll_cont ++
//	
//	select des_test
//	into   :ls_des_test
//	from   tab_test
//	where  cod_test = :ls_cod_test;
//	if sqlca.sqlcode <> 0 then
//		messagebox("OMNIA","Errore inatteso durante la ricerca della descrizione del test in tabella tab_test.~r~n" + sqlca.sqlerrtext)
//		rollback;
//		return
//	end if
	
//	ll_riga = dw_report.insertrow(0)
//	dw_report.setitem(ll_riga,"azienda", ls_azienda)
//	dw_report.setitem(ll_riga,"cod_fornitore", ls_cod_fornitore)
//	dw_report.setitem(ll_riga,"rag_soc_1", ls_rag_soc_1)
//	dw_report.setitem(ll_riga,"cod_prodotto", ls_cod_prodotto)
//	dw_report.setitem(ll_riga,"des_prodotto", ls_des_prodotto)
//	dw_report.setitem(ll_riga,"ddt", ls_rif_bol_acq)
//	dw_report.setitem(ll_riga,"data_ddt", ldt_data_eff_consegna)
//	dw_report.setitem(ll_riga,"cod_deposito", ls_cod_deposito)
//	dw_report.setitem(ll_riga,"cod_ubicazione", ls_cod_ubicazione)
//	dw_report.setitem(ll_riga,"cod_lotto", ls_cod_lotto)
//	dw_report.setitem(ll_riga,"data_stock", ldt_data_stock)
//	dw_report.setitem(ll_riga,"prog_stock", ll_prog_stock)
//	dw_report.setitem(ll_riga,"cod_piano_campionamento", ls_cod_piano_campionamento)
//	dw_report.setitem(ll_riga,"des_piano_campionamento", ls_des_piano_campionamento)
//	dw_report.setitem(ll_riga,"cod_test", ls_cod_test)
//	dw_report.setitem(ll_riga,"des_test", ls_des_test)
//	dw_report.setitem(ll_riga,"anno_reg_campionamento", ll_anno_reg_campionamento)
//	dw_report.setitem(ll_riga,"num_reg_campionamento", ll_num_reg_campionamento)
//	dw_report.setitem(ll_riga,"des_tipologia_campionamento", ls_cod_tipologia_campionamento + " " + ls_des_tipologia_campionamento)
//loop
//
//rollback;

dw_report.Object.DataWindow.Print.preview = 'Yes'

return



end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect + &
								 c_ViewModeColorUnchanged + &
								 c_InactiveDWColorUnchanged + &
								 c_InactiveTextLineCol)
iuo_dw_main = dw_report

wf_report(s_cs_xx.parametri.parametro_d_1,s_cs_xx.parametri.parametro_d_2)
end event

on w_report_esecuzione_campionamenti.create
int iCurrent
call super::create
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
end on

on w_report_esecuzione_campionamenti.destroy
call super::destroy
destroy(this.dw_report)
end on

type dw_report from uo_cs_xx_dw within w_report_esecuzione_campionamenti
integer x = 23
integer y = 20
integer width = 3675
integer height = 1756
integer taborder = 10
string dataobject = "d_report_esecuzione_campionamento"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

