﻿$PBExportHeader$w_report_materiali_accettati.srw
$PBExportComments$Finestra Report Elenco Fatture con contropartite
forward
global type w_report_materiali_accettati from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_materiali_accettati
end type
type dw_selezione from uo_cs_xx_dw within w_report_materiali_accettati
end type
type dw_folder from u_folder within w_report_materiali_accettati
end type
type cb_report from commandbutton within w_report_materiali_accettati
end type
end forward

global type w_report_materiali_accettati from w_cs_xx_principale
integer width = 3579
integer height = 1676
string title = "Report Materiali Accettati / Campionamenti"
dw_report dw_report
dw_selezione dw_selezione
dw_folder dw_folder
cb_report cb_report
end type
global w_report_materiali_accettati w_report_materiali_accettati

forward prototypes
public function integer wf_report (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_area, datetime fdt_data_inizio, datetime fdt_data_fine)
public function integer wf_report_scadenze_camp (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_area, datetime fdt_data_inizio, datetime fdt_data_fine)
public function integer wf_report_old (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_area, datetime fdt_data_inizio, datetime fdt_data_fine)
end prototypes

public function integer wf_report (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_area, datetime fdt_data_inizio, datetime fdt_data_fine);datetime	ldt_data_eff_consegna, ldt_data_validita, ldt_today, ldt_null, ldt_ultimo_campionamento, ldt_campionamento_prec

long			ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_riga, ll_count, ll_prog_piano_campionamento

dec{4}		ld_quan_arrivata, ld_quan_cumulativa, ld_quan_limite_campionamento, ld_null, ld_quan_prec

string		ls_rif_bol_acq, ls_cod_fornitore, ls_cod_prodotto, ls_flag_eseguito_campionamento, ls_cod_piano_campionamento, &
       			ls_fornitore_old, ls_prodotto_old, ls_rag_soc_1, ls_des_prodotto, ls_azienda, ls_null, ls_fam, ls_supervisore, ls_area, &
				ls_area_turno, ls_turno_campionamento


setnull(ls_null)

setnull(ld_null)

setnull(ldt_null)

ldt_today = datetime(today(),00:00:00)

dw_report.dataobject = 'd_report_materiali_accettati'

select
	flag
into
	:ls_fam
from
	parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'FAM';
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura parametro aziendale FAM: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_fam) or ls_fam <> "S" then
	ls_fam = "N"
else
	
	select
		flag_supervisore,
		cod_area_aziendale
	into
		:ls_supervisore,
		:ls_area
	from
		mansionari
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: nessun mansionario associato",stopsign!)
		return -1
	elseif isnull(ls_supervisore) or ls_supervisore <> "S" then
		
		ls_fam = "S"
		
		if isnull(ls_area) then
			g_mb.messagebox("OMNIA","Il mansionario dell'utente corrente non è associato ad alcuna area aziendale",stopsign!)
			return -1
		end if
		
	else
		ls_fam = "N"
	end if
	
end if
 
DECLARE cu_acc_materiali CURSOR FOR  
SELECT rif_bol_acq,   
		 data_eff_consegna,   
		 anno_reg_campionamenti,   
		 num_reg_campionamenti,   
		 quan_arrivata,   
		 cod_fornitore,   
		 cod_prodotto  
 FROM  acc_materiali  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		 data_eff_consegna between :fdt_data_inizio and :fdt_data_fine
ORDER BY cod_fornitore ASC, cod_prodotto ASC, data_eff_consegna ASC ;

open cu_acc_materiali;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in open del cursore cu_acc_materiali." + sqlca.sqlerrtext,stopsign!)
	return -1
end if
dw_report.reset()
ld_quan_cumulativa = 0
ls_fornitore_old = ""
ls_prodotto_old = ""

select rag_soc_1
into   :ls_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

do while true
	fetch cu_acc_materiali into :ls_rif_bol_acq, :ldt_data_eff_consegna, :ll_anno_reg_campionamenti, :ll_num_reg_campionamenti, :ld_quan_arrivata,:ls_cod_fornitore, :ls_cod_prodotto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in fetch del cursore cu_acc_materiali." + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	if not isnull(fs_cod_prodotto) then
		if fs_cod_prodotto <> ls_cod_prodotto then continue
	end if
	
	if not isnull(fs_cod_fornitore) then
		if fs_cod_fornitore <> ls_cod_fornitore then continue
	end if
	
	if not isnull(fs_cod_area) then
		
		select
			count(*)
		into
			:ll_count
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_area_aziendale = :fs_cod_area;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in verifica fornitore: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_count) then
			ll_count = 0
		end if
			
		if ll_count < 1 then
			continue
		end if
		
	end if
	
	if ls_fornitore_old <> ls_cod_fornitore or ls_prodotto_old <> ls_cod_prodotto then
		
		select
			max(data_eff_consegna)
		into
			:ldt_ultimo_campionamento
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_prodotto = :ls_cod_prodotto and
			anno_reg_campionamenti is not null and
			num_reg_campionamenti is not null;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura data ultimo campionamento: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ldt_ultimo_campionamento) then
			ldt_ultimo_campionamento = datetime(date("01/01/1900"),00:00:00)
		end if
		
		if ls_fam = "S" then
			
			select
				min(cod_area_aziendale)
			into
				:ls_area_turno
			from
				acc_materiali
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_fornitore = :ls_cod_fornitore and
				cod_prodotto = :ls_cod_prodotto and
				data_eff_consegna > :ldt_ultimo_campionamento and
				cod_area_aziendale > (select
													max(cod_area_aziendale)
												 from
													acc_materiali
												 where
													cod_azienda = :s_cs_xx.cod_azienda and
													cod_fornitore = :ls_cod_fornitore and
													cod_prodotto = :ls_cod_prodotto and
													data_eff_consegna = :ldt_ultimo_campionamento and
													anno_reg_campionamenti is not null and
													num_reg_campionamenti is not null);
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in lettura area prossimo campionamento: " + sqlca.sqlerrtext,stopsign!)
				close cu_acc_materiali;
				return -1
			elseif sqlca.sqlcode = 100 or isnull(ls_area_turno) then
				
				select
					min(cod_area_aziendale)
				into
					:ls_area_turno
				from
					acc_materiali
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :ls_cod_fornitore and
					cod_prodotto = :ls_cod_prodotto and
					data_eff_consegna > :ldt_ultimo_campionamento;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("OMNIA","Errore in lettura area prossimo campionamento: " + sqlca.sqlerrtext,stopsign!)
					close cu_acc_materiali;
					return -1
				elseif sqlca.sqlcode = 100 then
					setnull(ls_area_turno)
				end if
				
			end if
			
			if isnull(ls_area_turno) or ls_area_turno <> ls_area then
				ls_turno_campionamento = "N"
			else
				ls_turno_campionamento = "S"
			end if
			
		else
			
			ls_turno_campionamento = "S"
			
		end if
		
		select
			max(data_eff_consegna)
		into
			:ldt_campionamento_prec
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_prodotto = :ls_cod_prodotto and
			anno_reg_campionamenti is not null and
			num_reg_campionamenti is not null and
			data_eff_consegna <= :fdt_data_inizio;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura data ultimo campionamento: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ldt_campionamento_prec) then
			ldt_campionamento_prec = datetime(date("01/01/1900"),00:00:00)
		end if
		
		select
			sum(quan_arrivata)
		into
			:ld_quan_prec
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_prodotto = :ls_cod_prodotto and
			data_eff_consegna > :ldt_campionamento_prec and
			data_eff_consegna < :ldt_data_eff_consegna;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in verifica quantità precedente: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ld_quan_prec) then
			ld_quan_prec = 0
		end if
		
		ld_quan_cumulativa = ld_quan_prec
		
		ls_fornitore_old = ls_cod_fornitore
		ls_prodotto_old = ls_cod_prodotto
		
	end if
	
	select rag_soc_1
	into   :ls_rag_soc_1
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_fornitore = :ls_cod_fornitore;
			 
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
			 
	
	ls_flag_eseguito_campionamento = "N"
	ld_quan_limite_campionamento = 0
	setnull(ls_cod_piano_campionamento)
	setnull(ldt_data_validita)

	// controllo se per questa accettazione è stato fatto un campionamento
	if not isnull(ll_anno_reg_campionamenti) and ll_anno_reg_campionamenti > 0 then
		ls_flag_eseguito_campionamento = "S"
		
		select cod_piano_campionamento,
		       data_validita
		into   :ls_cod_piano_campionamento,
		       :ldt_data_validita
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_reg_campionamenti = :ll_anno_reg_campionamenti and
				 num_reg_campionamenti = :ll_num_reg_campionamenti ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore "+string(sqlca.sqlcode)+" in select tes_campionamenti dall'accettazione materiali.~r~n" + sqlca.sqlerrtext + "~r~nQuesta accettazione verrà saltata!")
			continue
		end if
	end if
	

	// ***** script prima della specifica "mod_piani_campionamento"
//	select cod_piano_campionamento,
//			 data_validita
//	into   :ls_cod_piano_campionamento,
//			 :ldt_data_validita
//	from   tes_piani_campionamento
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_fornitore = :ls_cod_fornitore and
//			 data_validita = (select max(data_validita)
//			 						from   tes_piani_campionamento
//									where  cod_azienda = :s_cs_xx.cod_azienda and
//			 								 cod_fornitore = :ls_cod_fornitore and
//											 data_validita <= :ldt_data_eff_consegna);
	
	select cod_piano_campionamento,
			 data_validita,
			 prog_piano_campionamento
	into   :ls_cod_piano_campionamento,
			 :ldt_data_validita,
			 :ll_prog_piano_campionamento
	from   tes_piani_campionamento
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore and
			 data_validita = (select max(tes_piani_campionamento.data_validita)
			 						from   tes_piani_campionamento, det_piani_campionamento
									where  tes_piani_campionamento.cod_azienda = det_piani_campionamento.cod_azienda and
									       tes_piani_campionamento.cod_piano_campionamento = det_piani_campionamento.cod_piano_campionamento and
									       tes_piani_campionamento.data_validita = det_piani_campionamento.data_validita and
									       tes_piani_campionamento.prog_piano_campionamento = det_piani_campionamento.prog_piano_campionamento and
									       tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda and
			 								 tes_piani_campionamento.cod_fornitore = :ls_cod_fornitore and
			 								 det_piani_campionamento.cod_prodotto = :ls_cod_prodotto and
											 tes_piani_campionamento.data_validita <= :ldt_data_eff_consegna);

	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura dati piano campionamento da tes_piani_campionamento: " + sqlca.sqlerrtext)
		close cu_acc_materiali;
		return -1
	elseif sqlca.sqlcode = 100 then
		f_scrivi_log("Per il fornitore " + ls_cod_fornitore + " non c'è alcun piano di campionamento valido; le sue accettazioni vengono saltate!")
		continue
	end if
	
//******************************** Fine Modifica ********************************************
	
	// cerco la quantità limite del campionamento in piani_campionamento
	
	select quan_limite_campionamento
	into   :ld_quan_limite_campionamento
	from   det_piani_campionamento
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_piano_campionamento = :ls_cod_piano_campionamento and
			 data_validita = :ldt_data_validita and
			 cod_prodotto = :ls_cod_prodotto and
			 prog_piano_campionamento = :ll_prog_piano_campionamento
	group by quan_limite_campionamento;
	
		if ld_quan_prec > 0 then
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga,"azienda",ls_azienda)
			dw_report.setitem(ll_riga,"cod_fornitore",ls_cod_fornitore)
			dw_report.setitem(ll_riga,"rag_soc_1",ls_rag_soc_1)
			dw_report.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
			dw_report.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
			dw_report.setitem(ll_riga,"num_ddt","Quantità precedente")
			dw_report.setitem(ll_riga,"data_ddt",ldt_null)
			dw_report.setitem(ll_riga,"flag_eseguito_campionamento",ls_null)
			dw_report.setitem(ll_riga,"quan_consegnata",ld_quan_prec)
			dw_report.setitem(ll_riga,"quan_consegnata_cumulata",ld_quan_cumulativa)
			dw_report.setitem(ll_riga,"quan_limite_campionamento",ld_null)
			ld_quan_prec = 0
		end if
		
		if ls_flag_eseguito_campionamento = "S" then
			ld_quan_cumulativa = 0
		else
			ld_quan_cumulativa = ld_quan_cumulativa + ld_quan_arrivata
		end if
		
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem(ll_riga,"azienda", ls_azienda)
		dw_report.setitem(ll_riga,"cod_fornitore", ls_cod_fornitore)
		dw_report.setitem(ll_riga,"rag_soc_1", ls_rag_soc_1)
		dw_report.setitem(ll_riga,"cod_prodotto", ls_cod_prodotto)
		dw_report.setitem(ll_riga,"des_prodotto", ls_des_prodotto)
		dw_report.setitem(ll_riga,"num_ddt", ls_rif_bol_acq)
		dw_report.setitem(ll_riga,"data_ddt", ldt_data_eff_consegna)
		dw_report.setitem(ll_riga,"flag_eseguito_campionamento", ls_flag_eseguito_campionamento)
		dw_report.setitem(ll_riga,"quan_consegnata", ld_quan_arrivata)
		dw_report.setitem(ll_riga,"quan_consegnata_cumulata", ld_quan_cumulativa)
		dw_report.setitem(ll_riga,"quan_limite_campionamento",ld_quan_limite_campionamento)
		dw_report.setitem(ll_riga,"flag_turno_campionamento",ls_turno_campionamento)
loop
close cu_acc_materiali;
dw_report.groupcalc()

commit;

return 0
end function

public function integer wf_report_scadenze_camp (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_area, datetime fdt_data_inizio, datetime fdt_data_fine);long			ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_ret, ll_cont, ll_pos,ll_riga, &
            ll_frequenza

dec{4}		ld_quan_arrivata, ld_quan_cumulativa, ld_quan_limite_campionamento, ld_null, ld_quan_prec

string		ls_cod_fornitore, ls_cod_prodotto, ls_fornitore_old, ls_prodotto_old, ls_rag_soc_1, &
            ls_des_prodotto, ls_azienda, ls_null, ls_area, ls_area_turno, ls_turno_campionamento, ls_fam, &
				ls_supervisore,ls_periodicita
				
string      ls_sql, ls_sql_order

date  		ld_data_scadenza
datetime	   ldt_today, ldt_null, ldt_ultimo_campionamento,ldt_data_confronto


datastore lds_piani_camp

setnull(ls_null)

setnull(ld_null)

setnull(ldt_null)

ldt_today = datetime(today(),00:00:00)

dw_report.reset()

dw_report.dataobject = 'd_report_materiali_accettati_tempo'

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect + &
								 c_ViewModeColorUnchanged + &
								 c_InactiveDWColorUnchanged + &
								 c_InactiveTextLineCol)


select flag
into :ls_fam
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
		flag_parametro = 'F' and
		cod_parametro = 'FAM';
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura parametro aziendale FAM: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_fam) or ls_fam <> "S" then
	ls_fam = "N"
else
	
//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//	select flag_supervisore,
//			 cod_area_aziendale
//	into   :ls_supervisore,
//		    :ls_area
//	from   mansionari
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_utente = :s_cs_xx.cod_utente;

	select cod_area_aziendale
	into :ls_area
	from   mansionari
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente = :s_cs_xx.cod_utente;

	uo_mansionario luo_mansionario
	luo_mansionario = create uo_mansionario
	
	ls_supervisore = 'N'
	
	if luo_mansionario.uof_get_privilege(luo_mansionario.supervisore) then ls_supervisore = 'S'
	//--- Fine modifica Giulio
	
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: nessun mansionario associato",stopsign!)
		return -1
	elseif isnull(ls_supervisore) or ls_supervisore <> "S" then
		
		ls_fam = "S"
		
		if isnull(ls_area) then
			g_mb.messagebox("OMNIA","Il mansionario dell'utente corrente non è associato ad alcuna area aziendale",stopsign!)
			return -1
		end if
		
	else
		ls_fam = "N"
	end if
	
end if
 
// ricerco il piano di campionamento in cui il prodotto compare con la data più recente

lds_piani_camp = CREATE datastore
lds_piani_camp.dataobject = 'd_ds_report_acc_materiali_tempo'
lds_piani_camp.settransobject(sqlca)

ls_sql = lds_piani_camp.getsqlselect()
if ls_sql = "" then
	g_mb.messagebox("OMNIA","Errore nell'impostazione della query piani_campionamento.")
	return -1
end if

ll_pos = pos(lower(ls_sql), "order by") 

ls_sql_order = mid(ls_sql, ll_pos)

ls_sql = left(ls_sql, ll_pos - 1)

ls_sql +=  "where tes_piani_campionamento.cod_azienda = '" + s_cs_xx.cod_azienda + "' and tes_piani_campionamento.flag_tipo_scad_campionamento = 'T' "

if not isnull(fs_cod_prodotto) then
	ls_sql += " and det_piani_campionamento.cod_prodotto = '" + fs_cod_prodotto + "' "
end if

if not isnull(fs_cod_fornitore) then
	ls_sql += " and tes_piani_campionamento.cod_fornitore = '" + fs_cod_fornitore + "' "
end if

ls_sql += ls_sql_order

ll_ret = lds_piani_camp.setsqlselect(ls_sql)

ll_ret = lds_piani_camp.retrieve()

ls_fornitore_old = ""
ls_prodotto_old = ""

for ll_cont = 1 to lds_piani_camp.rowcount()
	
	// CONTROLLO SELEZIONI
	
	if isnull(lds_piani_camp.getitemstring(ll_cont, "cod_fornitore")) then continue
	
	if isnull(lds_piani_camp.getitemstring(ll_cont, "cod_prodotto")) then continue
	
	if ls_fornitore_old <> lds_piani_camp.getitemstring(ll_cont, "cod_fornitore") or ls_prodotto_old <> lds_piani_camp.getitemstring(ll_cont, "cod_prodotto") then
		
		ls_cod_fornitore = lds_piani_camp.getitemstring(ll_cont, "cod_fornitore")
		ls_cod_prodotto = lds_piani_camp.getitemstring(ll_cont, "cod_prodotto")
		
		// trovo la data ultimo campionamento
		
		setnull(ldt_ultimo_campionamento)
		
		select max(data_eff_consegna)
		into   :ldt_ultimo_campionamento
		from   acc_materiali
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore and
				 cod_prodotto  = :ls_cod_prodotto and
				 anno_reg_campionamenti is not null and
				 num_reg_campionamenti is not null;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura data ultimo campionamento: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		if isnull(ldt_ultimo_campionamento) or ldt_ultimo_campionamento = datetime(date("01/01/1900"),00:00:00) then
			ldt_data_confronto = datetime(date("01/01/1990"),00:00:00)
		else
			ldt_data_confronto = ldt_ultimo_campionamento
		end if
		
		// faccio apparire la scadenza SOLO all'area di competenza
		
		if ls_fam = "S" then
			
			select
				min(cod_area_aziendale)
			into
				:ls_area_turno
			from
				acc_materiali
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_fornitore = :ls_cod_fornitore and
				cod_prodotto = :ls_cod_prodotto and
				data_eff_consegna > :ldt_data_confronto and
				cod_area_aziendale > (select
												max(cod_area_aziendale)
											 from
												acc_materiali
											 where
												cod_azienda = :s_cs_xx.cod_azienda and
												cod_fornitore = :ls_cod_fornitore and
												cod_prodotto = :ls_cod_prodotto and
												data_eff_consegna = :ldt_data_confronto and
												anno_reg_campionamenti is not null and
												num_reg_campionamenti is not null);
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in lettura area prossimo campionamento: " + sqlca.sqlerrtext,stopsign!)
				return -1
			elseif sqlca.sqlcode = 100 or isnull(ls_area_turno) then
				
				select
					min(cod_area_aziendale)
				into
					:ls_area_turno
				from
					acc_materiali
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :ls_cod_fornitore and
					cod_prodotto = :ls_cod_prodotto and
					data_eff_consegna > :ldt_data_confronto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("OMNIA","Errore in lettura area prossimo campionamento: " + sqlca.sqlerrtext,stopsign!)
					return -1
				elseif sqlca.sqlcode = 100 then
					setnull(ls_area_turno)
				end if
				
			end if
			 
			if isnull(ls_area_turno) or ls_area_turno <> ls_area then continue
			
		end if
		
		
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_fornitore = :ls_cod_fornitore;
		if sqlca.sqlcode <> 0 then
			ls_rag_soc_1 = "< FORNITORE NON TROVATO >"
		end if
		
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			ls_rag_soc_1 = "< PRODOTTO NON TROVATO >"
		end if
		
		ls_periodicita = lds_piani_camp.getitemstring(ll_cont, "periodicita")
		ll_frequenza   = lds_piani_camp.getitemnumber(ll_cont, "frequenza")


		if isnull(ldt_ultimo_campionamento) or ldt_ultimo_campionamento = datetime(date("01/01/1900"),00:00:00) then
			ld_data_scadenza = date(ldt_today)
		else

			choose case ls_periodicita
				case "G"   // giorni
					ld_data_scadenza = relativedate(date(ldt_ultimo_campionamento), ll_frequenza)
					
				case "S"   // settimane
					ld_data_scadenza = relativedate(date(ldt_ultimo_campionamento), ll_frequenza * 7)
					
				case "E" 	// mesi
					ld_data_scadenza = relativedate(date(ldt_ultimo_campionamento), ll_frequenza * 30)
					
			end choose
			
		end if
		
		if not isnull(fdt_data_inizio) then
			if ld_data_scadenza < date(fdt_data_inizio) then continue
		end if
		
		if not isnull(fdt_data_fine) then
			if ld_data_scadenza > date(fdt_data_fine) then continue
		end if
		
		ll_riga = dw_report.insertrow(0)
		
		dw_report.setitem(ll_riga,"azienda",ls_azienda)
		dw_report.setitem(ll_riga,"cod_fornitore", ls_cod_fornitore)
		dw_report.setitem(ll_riga,"rag_soc_1",ls_rag_soc_1)
		dw_report.setitem(ll_riga,"cod_prodotto", ls_cod_prodotto)
		dw_report.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
		
		dw_report.setitem(ll_riga, "data_scadenza", ld_data_scadenza)

		ls_fornitore_old = lds_piani_camp.getitemstring(ll_cont, "cod_fornitore")
		ls_prodotto_old = lds_piani_camp.getitemstring(ll_cont, "cod_prodotto")

	end if
next
	
destroy lds_piani_camp

return 0
end function

public function integer wf_report_old (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_area, datetime fdt_data_inizio, datetime fdt_data_fine);datetime	ldt_data_eff_consegna, ldt_data_validita, ldt_today, ldt_null, ldt_ultimo_campionamento, ldt_campionamento_prec

long			ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_riga, ll_count

dec{4}		ld_quan_arrivata, ld_quan_cumulativa, ld_quan_limite_campionamento, ld_null, ld_quan_prec

string		ls_rif_bol_acq, ls_cod_fornitore, ls_cod_prodotto, ls_flag_eseguito_campionamento, ls_cod_piano_campionamento, &
       			ls_fornitore_old, ls_prodotto_old, ls_rag_soc_1, ls_des_prodotto, ls_azienda, ls_null, ls_fam, ls_supervisore, ls_area, &
				ls_area_turno, ls_turno_campionamento


setnull(ls_null)

setnull(ld_null)

setnull(ldt_null)

ldt_today = datetime(today(),00:00:00)

select
	flag
into
	:ls_fam
from
	parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'FAM';
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura parametro aziendale FAM: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_fam) or ls_fam <> "S" then
	ls_fam = "N"
else
	
	select
		flag_supervisore,
		cod_area_aziendale
	into
		:ls_supervisore,
		:ls_area
	from
		mansionari
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: nessun mansionario associato",stopsign!)
		return -1
	elseif isnull(ls_supervisore) or ls_supervisore <> "S" then
		
		ls_fam = "S"
		
		if isnull(ls_area) then
			g_mb.messagebox("OMNIA","Il mansionario dell'utente corrente non è associato ad alcuna area aziendale",stopsign!)
			return -1
		end if
		
	else
		ls_fam = "N"
	end if
	
end if
 
DECLARE cu_acc_materiali CURSOR FOR  
SELECT rif_bol_acq,   
		 data_eff_consegna,   
		 anno_reg_campionamenti,   
		 num_reg_campionamenti,   
		 quan_arrivata,   
		 cod_fornitore,   
		 cod_prodotto  
 FROM  acc_materiali  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		 data_eff_consegna between :fdt_data_inizio and :fdt_data_fine
ORDER BY cod_fornitore ASC, cod_prodotto ASC, data_eff_consegna ASC ;

open cu_acc_materiali;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in open del cursore cu_acc_materiali." + sqlca.sqlerrtext,stopsign!)
	return -1
end if
dw_report.reset()
ld_quan_cumulativa = 0
ls_fornitore_old = ""
ls_prodotto_old = ""

select rag_soc_1
into   :ls_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

do while true
	fetch cu_acc_materiali into :ls_rif_bol_acq, :ldt_data_eff_consegna, :ll_anno_reg_campionamenti, :ll_num_reg_campionamenti, :ld_quan_arrivata,:ls_cod_fornitore, :ls_cod_prodotto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in fetch del cursore cu_acc_materiali." + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	if not isnull(fs_cod_prodotto) then
		if fs_cod_prodotto <> ls_cod_prodotto then continue
	end if
	
	if not isnull(fs_cod_fornitore) then
		if fs_cod_fornitore <> ls_cod_fornitore then continue
	end if
	
	if not isnull(fs_cod_area) then
		
		select
			count(*)
		into
			:ll_count
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_area_aziendale = :fs_cod_area;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in verifica fornitore: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_count) then
			ll_count = 0
		end if
			
		if ll_count < 1 then
			continue
		end if
		
	end if
	
	if ls_fornitore_old <> ls_cod_fornitore or ls_prodotto_old <> ls_cod_prodotto then
		
		select
			max(data_eff_consegna)
		into
			:ldt_ultimo_campionamento
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_prodotto = :ls_cod_prodotto and
			anno_reg_campionamenti is not null and
			num_reg_campionamenti is not null;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura data ultimo campionamento: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ldt_ultimo_campionamento) then
			ldt_ultimo_campionamento = datetime(date("01/01/1900"),00:00:00)
		end if
		
		if ls_fam = "S" then
			
			select
				min(cod_area_aziendale)
			into
				:ls_area_turno
			from
				acc_materiali
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_fornitore = :ls_cod_fornitore and
				cod_prodotto = :ls_cod_prodotto and
				data_eff_consegna > :ldt_ultimo_campionamento and
				cod_area_aziendale > (select
													max(cod_area_aziendale)
												 from
													acc_materiali
												 where
													cod_azienda = :s_cs_xx.cod_azienda and
													cod_fornitore = :ls_cod_fornitore and
													cod_prodotto = :ls_cod_prodotto and
													data_eff_consegna = :ldt_ultimo_campionamento and
													anno_reg_campionamenti is not null and
													num_reg_campionamenti is not null);
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in lettura area prossimo campionamento: " + sqlca.sqlerrtext,stopsign!)
				close cu_acc_materiali;
				return -1
			elseif sqlca.sqlcode = 100 or isnull(ls_area_turno) then
				
				select
					min(cod_area_aziendale)
				into
					:ls_area_turno
				from
					acc_materiali
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :ls_cod_fornitore and
					cod_prodotto = :ls_cod_prodotto and
					data_eff_consegna > :ldt_ultimo_campionamento;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("OMNIA","Errore in lettura area prossimo campionamento: " + sqlca.sqlerrtext,stopsign!)
					close cu_acc_materiali;
					return -1
				elseif sqlca.sqlcode = 100 then
					setnull(ls_area_turno)
				end if
				
			end if
			
			if isnull(ls_area_turno) or ls_area_turno <> ls_area then
				ls_turno_campionamento = "N"
			else
				ls_turno_campionamento = "S"
			end if
			
		else
			
			ls_turno_campionamento = "S"
			
		end if
		
		select
			max(data_eff_consegna)
		into
			:ldt_campionamento_prec
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_prodotto = :ls_cod_prodotto and
			anno_reg_campionamenti is not null and
			num_reg_campionamenti is not null and
			data_eff_consegna <= :fdt_data_inizio;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura data ultimo campionamento: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ldt_campionamento_prec) then
			ldt_campionamento_prec = datetime(date("01/01/1900"),00:00:00)
		end if
		
		select
			sum(quan_arrivata)
		into
			:ld_quan_prec
		from
			acc_materiali
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_prodotto = :ls_cod_prodotto and
			data_eff_consegna > :ldt_campionamento_prec and
			data_eff_consegna < :ldt_data_eff_consegna;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in verifica quantità precedente: " + sqlca.sqlerrtext,stopsign!)
			close cu_acc_materiali;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ld_quan_prec) then
			ld_quan_prec = 0
		end if
		
		ld_quan_cumulativa = ld_quan_prec
		
		ls_fornitore_old = ls_cod_fornitore
		ls_prodotto_old = ls_cod_prodotto
		
	end if
	
	select rag_soc_1
	into   :ls_rag_soc_1
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_fornitore = :ls_cod_fornitore;
			 
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
			 
	
	ls_flag_eseguito_campionamento = "N"
	ld_quan_limite_campionamento = 0
	setnull(ls_cod_piano_campionamento)
	setnull(ldt_data_validita)

	// controllo se per questa accettazione è stato fatto un campionamento
	if not isnull(ll_anno_reg_campionamenti) and ll_anno_reg_campionamenti > 0 then
		ls_flag_eseguito_campionamento = "S"
		
		select cod_piano_campionamento,
		       data_validita
		into   :ls_cod_piano_campionamento,
		       :ldt_data_validita
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_reg_campionamenti = :ll_anno_reg_campionamenti and
				 num_reg_campionamenti = :ll_num_reg_campionamenti ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore "+string(sqlca.sqlcode)+" in select tes_campionamenti dall'accettazione materiali.~r~n" + sqlca.sqlerrtext + "~r~nQuesta accettazione verrà saltata!")
			continue
		end if
	end if
	
//************************* Modifica Michele 21/01/2003 *************************************
		
//	select cod_piano_campionamento
//	into   :ls_cod_piano_campionamento
//	from   tes_piani_campionamento
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_fornitore = :ls_cod_fornitore
//	group by cod_piano_campionamento;
//	if isnull(ls_cod_piano_campionamento) then
//		messagebox("OMNIA","Per il fornitore " + ls_cod_fornitore + " non c'è alcun piano di campionamento; le sue accettazioni vengono saltate!")
//		continue
//	end if
//	
//	select max(data_validita)
//	into   :ldt_data_validita
//	from   tes_piani_campionamento
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_piano_campionamento = :ls_cod_piano_campionamento ;

	select cod_piano_campionamento,
			 data_validita
	into   :ls_cod_piano_campionamento,
			 :ldt_data_validita
	from   tes_piani_campionamento
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore and
			 data_validita = (select max(data_validita)
			 						from   tes_piani_campionamento
									where  cod_azienda = :s_cs_xx.cod_azienda and
			 								 cod_fornitore = :ls_cod_fornitore and
											 data_validita <= :ldt_data_eff_consegna);
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura dati piano campionamento da tes_piani_campionamento: " + sqlca.sqlerrtext)
		close cu_acc_materiali;
		return -1
	elseif sqlca.sqlcode = 100 then
		f_scrivi_log("Per il fornitore " + ls_cod_fornitore + " non c'è alcun piano di campionamento valido; le sue accettazioni vengono saltate!")
		continue
	end if
	
//******************************** Fine Modifica ********************************************
	
	// cerco la quantità limite del campionamento in piani_campionamento
	
	select quan_limite_campionamento
	into   :ld_quan_limite_campionamento
	from   det_piani_campionamento
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_piano_campionamento = :ls_cod_piano_campionamento and
			 data_validita = :ldt_data_validita and
			 cod_prodotto = :ls_cod_prodotto
	group by quan_limite_campionamento;
	
		if ld_quan_prec > 0 then
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga,"azienda",ls_azienda)
			dw_report.setitem(ll_riga,"cod_fornitore",ls_cod_fornitore)
			dw_report.setitem(ll_riga,"rag_soc_1",ls_rag_soc_1)
			dw_report.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
			dw_report.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
			dw_report.setitem(ll_riga,"num_ddt","Quantità precedente")
			dw_report.setitem(ll_riga,"data_ddt",ldt_null)
			dw_report.setitem(ll_riga,"flag_eseguito_campionamento",ls_null)
			dw_report.setitem(ll_riga,"quan_consegnata",ld_quan_prec)
			dw_report.setitem(ll_riga,"quan_consegnata_cumulata",ld_quan_cumulativa)
			dw_report.setitem(ll_riga,"quan_limite_campionamento",ld_null)
			ld_quan_prec = 0
		end if
		
		if ls_flag_eseguito_campionamento = "S" then
			ld_quan_cumulativa = 0
		else
			ld_quan_cumulativa = ld_quan_cumulativa + ld_quan_arrivata
		end if
		
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem(ll_riga,"azienda", ls_azienda)
		dw_report.setitem(ll_riga,"cod_fornitore", ls_cod_fornitore)
		dw_report.setitem(ll_riga,"rag_soc_1", ls_rag_soc_1)
		dw_report.setitem(ll_riga,"cod_prodotto", ls_cod_prodotto)
		dw_report.setitem(ll_riga,"des_prodotto", ls_des_prodotto)
		dw_report.setitem(ll_riga,"num_ddt", ls_rif_bol_acq)
		dw_report.setitem(ll_riga,"data_ddt", ldt_data_eff_consegna)
		dw_report.setitem(ll_riga,"flag_eseguito_campionamento", ls_flag_eseguito_campionamento)
		dw_report.setitem(ll_riga,"quan_consegnata", ld_quan_arrivata)
		dw_report.setitem(ll_riga,"quan_consegnata_cumulata", ld_quan_cumulativa)
		dw_report.setitem(ll_riga,"quan_limite_campionamento",ld_quan_limite_campionamento)
		dw_report.setitem(ll_riga,"flag_turno_campionamento",ls_turno_campionamento)
loop
close cu_acc_materiali;
dw_report.groupcalc()

commit;

return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_report.ib_dw_report  = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect + &
								 c_ViewModeColorUnchanged + &
								 c_InactiveDWColorUnchanged + &
								 c_InactiveTextLineCol)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
//l_objects[3] = cb_ricerca_fornitore
//l_objects[3] = cb_ricerca_prodotto
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)
end event

on w_report_materiali_accettati.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_selezione=create dw_selezione
this.dw_folder=create dw_folder
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.cb_report
end on

on w_report_materiali_accettati.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_selezione)
destroy(this.dw_folder)
destroy(this.cb_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
							"des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_report from uo_cs_xx_dw within w_report_materiali_accettati
integer x = 32
integer y = 124
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_materiali_accettati"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_selezione from uo_cs_xx_dw within w_report_materiali_accettati
integer x = 55
integer y = 136
integer width = 2423
integer height = 644
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_selezione_report_materiali_accettati"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null


setnull(ls_null)

choose case dwo.name
	case "cod_fornitore"
		if not isnull(data) then
			setitem(row,"cod_area_aziendale",ls_null)
		end if
	case "cod_area_aziendale"
		if not isnull(data) then
			setitem(row,"cod_fornitore",ls_null)
		end if
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_report_materiali_accettati
integer x = 9
integer y = 8
integer width = 3520
integer height = 1556
integer taborder = 40
end type

type cb_report from commandbutton within w_report_materiali_accettati
integer x = 2080
integer y = 668
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_fornitore, ls_cod_prodotto, ls_cod_area, ls_flag_tipo_scadenza
datetime ldd_data_inizio, ldd_data_fine

dw_selezione.accepttext()
ldd_data_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldd_data_fine = dw_selezione.getitemdatetime(1,"data_fine")
ls_cod_prodotto = dw_selezione.getitemstring(1,"cod_prodotto")
ls_cod_fornitore = dw_selezione.getitemstring(1,"cod_fornitore")
ls_cod_area = dw_selezione.getitemstring(1,"cod_area_aziendale")
ls_flag_tipo_scadenza = dw_selezione.getitemstring(1,"flag_tipo_scadenza")

if isnull(ldd_data_inizio) then ldd_data_inizio = datetime(date("01/01/1900"),00:00:00)
if isnull(ldd_data_fine) then ldd_data_fine = datetime(date("31/12/2999"),00:00:00)

dw_report.setredraw(false)

choose case ls_flag_tipo_scadenza 
	case "Q"
		
		if wf_report(ls_cod_prodotto, &
		             ls_cod_fornitore, &
						 ls_cod_area, &
						 ldd_data_inizio, &
						 ldd_data_fine) = -1 then 
						 
						 	return
		end if
		
	case "T"
		
		if wf_report_scadenze_camp(ls_cod_prodotto, &
											ls_cod_fornitore, &
											ls_cod_area, &
											ldd_data_inizio, &
											ldd_data_fine) = -1 then 
						 
						 	return
		end if
		
	case "N"
		
		g_mb.messagebox("Omnia", "Funzione non ancora supportata dal report.", Exclamation!)
		
end choose

//dw_report.change_dw_current()

dw_report.setredraw(true)

dw_folder.fu_selecttab(2)
end event

