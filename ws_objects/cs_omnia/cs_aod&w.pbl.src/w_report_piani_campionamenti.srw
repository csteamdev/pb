﻿$PBExportHeader$w_report_piani_campionamenti.srw
$PBExportComments$Finestra Report Elenco Fatture con contropartite
forward
global type w_report_piani_campionamenti from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_piani_campionamenti
end type
type dw_selezione from uo_cs_xx_dw within w_report_piani_campionamenti
end type
type dw_folder from u_folder within w_report_piani_campionamenti
end type
type cb_report from commandbutton within w_report_piani_campionamenti
end type
end forward

global type w_report_piani_campionamenti from w_cs_xx_principale
integer width = 3579
integer height = 1676
string title = "Report Materiali Accettati / Campionamenti"
dw_report dw_report
dw_selezione dw_selezione
dw_folder dw_folder
cb_report cb_report
end type
global w_report_piani_campionamenti w_report_piani_campionamenti

forward prototypes
public function integer wf_report (string fs_cod_prodotto, string fs_cod_fornitore, datetime fdt_data_inizio)
end prototypes

public function integer wf_report (string fs_cod_prodotto, string fs_cod_fornitore, datetime fdt_data_inizio);long			ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_ret, ll_cont, ll_pos,ll_riga, &
            ll_frequenza, ll_prog_piano_campionamento

dec{4}		ld_quan_limite_campionamento

string		ls_cod_fornitore, ls_cod_prodotto, ls_rag_soc_1, ls_des_prodotto, ls_azienda, ls_null, &
            ls_periodicita,ls_cod_piano_campionamento,ls_sql, ls_sql_order, ls_flag_tipo_scad_campionamento,&
				ls_des_tipo_piano_campionamento, ls_des_campionamento, ls_fornitore_old, ls_prodotto_old, &
				ls_des_piano_campionamento, ls_tempo
				
datetime		ldt_data_validita  

datastore lds_piani_camp

setnull(ls_null)

dw_report.reset()

 
select rag_soc_1
into   :ls_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

// ricerco il piano di campionamento in cui il prodotto compare con la data più recente

lds_piani_camp = CREATE datastore
lds_piani_camp.dataobject = 'd_ds_report_acc_materiali_tempo'
lds_piani_camp.settransobject(sqlca)

ls_sql = lds_piani_camp.getsqlselect()
if ls_sql = "" then
	g_mb.messagebox("OMNIA","Errore nell'impostazione della query piani_campionamento.")
	return -1
end if

ll_pos = pos(lower(ls_sql), "order by") 

ls_sql_order = mid(ls_sql, ll_pos)

ls_sql = left(ls_sql, ll_pos - 1)

ls_sql +=  "where tes_piani_campionamento.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(fs_cod_prodotto) then
	ls_sql += " and det_piani_campionamento.cod_prodotto = '" + fs_cod_prodotto + "' "
end if

if not isnull(fs_cod_fornitore) then
	ls_sql += " and tes_piani_campionamento.cod_fornitore = '" + fs_cod_fornitore + "' "
end if

if not isnull(fdt_data_inizio) then
	ls_sql += " and tes_piani_campionamento.data_validita <= '" + string(fdt_data_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += ls_sql_order

ll_ret = lds_piani_camp.setsqlselect(ls_sql)

ll_ret = lds_piani_camp.retrieve()

ls_fornitore_old = ""
ls_prodotto_old = ""

for ll_cont = 1 to lds_piani_camp.rowcount()
	
	// CONTROLLO SELEZIONI
	
	if isnull(lds_piani_camp.getitemstring(ll_cont, "cod_fornitore")) then continue
	
	if isnull(lds_piani_camp.getitemstring(ll_cont, "cod_prodotto")) then continue
	
	if ls_fornitore_old <> lds_piani_camp.getitemstring(ll_cont, "cod_fornitore") or ls_prodotto_old <> lds_piani_camp.getitemstring(ll_cont, "cod_prodotto") then
		
		ls_cod_fornitore = lds_piani_camp.getitemstring(ll_cont, "cod_fornitore")
		ls_cod_prodotto = lds_piani_camp.getitemstring(ll_cont, "cod_prodotto")
		
		ls_cod_piano_campionamento = lds_piani_camp.getitemstring(ll_cont, "cod_piano_campionamento")
		ldt_data_validita = lds_piani_camp.getitemdatetime(ll_cont, "data_validita")
		ll_prog_piano_campionamento = lds_piani_camp.getitemnumber(ll_cont, "prog_piano_campionamento")
		
		ls_periodicita = lds_piani_camp.getitemstring(ll_cont, "periodicita")
		ll_frequenza   = lds_piani_camp.getitemnumber(ll_cont, "frequenza")

		ls_flag_tipo_scad_campionamento = lds_piani_camp.getitemstring(ll_cont, "flag_tipo_scad_campionamento")
		ld_quan_limite_campionamento    = lds_piani_camp.getitemnumber(ll_cont, "quan_limite_campionamento")



		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_fornitore = :ls_cod_fornitore;
		if sqlca.sqlcode <> 0 then
			ls_rag_soc_1 = "< FORNITORE NON TROVATO >"
		end if
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			ls_rag_soc_1 = "< PRODOTTO NON TROVATO >"
		end if
			
		select des_piano_campionamento  
		into   :ls_des_tipo_piano_campionamento  
		from   tab_tipi_piani_campionamento  
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_piano_campionamento = :ls_cod_piano_campionamento ;
		if sqlca.sqlcode <> 0 then
			ls_des_tipo_piano_campionamento = ""
		end if
		
		select des_piano_campionamento  
		into   :ls_des_piano_campionamento  
		from   tes_piani_campionamento  
		where  cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_piano_campionamento = :ls_cod_piano_campionamento AND  
				data_validita = :ldt_data_validita AND  
				prog_piano_campionamento = :ll_prog_piano_campionamento ;
		if sqlca.sqlcode <> 0 then
			ls_des_piano_campionamento = ""
		end if
		
		if len(ls_des_piano_campionamento) > 0 then
			ls_des_piano_campionamento += "~r~n" + ls_des_tipo_piano_campionamento
		else
			ls_des_piano_campionamento = ls_des_tipo_piano_campionamento
		end if
		
		ll_riga = dw_report.insertrow(0)
		
		dw_report.setitem(ll_riga,"azienda",ls_azienda)
		dw_report.setitem(ll_riga,"cod_piano_campionamento", ls_cod_piano_campionamento)
		dw_report.setitem(ll_riga,"des_piano_campionamento", ls_des_piano_campionamento)
		dw_report.setitem(ll_riga,"data_validita", ldt_data_validita)
		dw_report.setitem(ll_riga,"prog_piano_campionamento", ll_prog_piano_campionamento)
		dw_report.setitem(ll_riga,"cod_fornitore", ls_cod_fornitore)
		dw_report.setitem(ll_riga,"rag_soc_1",ls_rag_soc_1)
		dw_report.setitem(ll_riga,"cod_prodotto", ls_cod_prodotto)
		dw_report.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
		dw_report.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
		dw_report.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
		dw_report.setitem(ll_riga,"flag_tipo_scadenza",ls_flag_tipo_scad_campionamento)
		dw_report.setitem(ll_riga,"quan_limite_campionamento",ld_quan_limite_campionamento)
		
		choose case ls_periodicita
			case "E"
				if ll_frequenza > 1 then
					ls_tempo = string(ll_frequenza) + " Mesi"
				else
					ls_tempo = string(ll_frequenza) + " Mese"
				end if
				
			case "S"
				if ll_frequenza > 1 then
					ls_tempo = string(ll_frequenza) + " Settimane"
				else
					ls_tempo = string(ll_frequenza) + " Settimana"
				end if
				
			case "G"
				if ll_frequenza > 1 then
					ls_tempo = string(ll_frequenza) + " Giorni"
				else
					ls_tempo = string(ll_frequenza) + " Giorno"
				end if
		end choose
				
		dw_report.setitem(ll_riga,"periodicita", ls_tempo)
		
		ls_fornitore_old = ls_cod_fornitore
		ls_prodotto_old = ls_cod_prodotto
		
	end if
next
	
destroy lds_piani_camp

return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect + &
								 c_ViewModeColorUnchanged + &
								 c_InactiveDWColorUnchanged + &
								 c_InactiveTextLineCol)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
//l_objects[3] = cb_ricerca_fornitore
//l_objects[3] = cb_ricerca_prodotto
dw_folder.fu_assigntab(1, "Selezione", l_objects[])


dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)
end event

on w_report_piani_campionamenti.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_selezione=create dw_selezione
this.dw_folder=create dw_folder
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.cb_report
end on

on w_report_piani_campionamenti.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_selezione)
destroy(this.dw_folder)
destroy(this.cb_report)
end on

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_selezione,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
//							"des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_report from uo_cs_xx_dw within w_report_piani_campionamenti
integer x = 32
integer y = 124
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_piani_campionamento"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_selezione from uo_cs_xx_dw within w_report_piani_campionamenti
integer x = 55
integer y = 136
integer width = 2423
integer height = 280
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_selezione_report_piani_campionamento"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null


setnull(ls_null)

choose case dwo.name
	case "cod_fornitore"
		if not isnull(data) then
			setitem(row,"cod_area_aziendale",ls_null)
		end if
	case "cod_area_aziendale"
		if not isnull(data) then
			setitem(row,"cod_fornitore",ls_null)
		end if
end choose
end event

event pcd_new;call super::pcd_new;datetime ldt_today

if i_extendmode then
	ldt_today = datetime(today(),00:00:00)

	dw_selezione.setitem(dw_selezione.getrow(),"data_inizio",ldt_today)
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_report_piani_campionamenti
integer x = 9
integer y = 8
integer width = 3520
integer height = 1556
integer taborder = 40
end type

type cb_report from commandbutton within w_report_piani_campionamenti
integer x = 2062
integer y = 324
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_fornitore, ls_cod_prodotto
datetime ldd_data_inizio

dw_selezione.accepttext()
ldd_data_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ls_cod_prodotto = dw_selezione.getitemstring(1,"cod_prodotto")
ls_cod_fornitore = dw_selezione.getitemstring(1,"cod_fornitore")

if isnull(ldd_data_inizio) then 
	g_mb.messagebox("OMNIA","Impostare una data inizio valida!")
	return
end if

dw_report.setredraw(false)

wf_report(ls_cod_prodotto, ls_cod_fornitore, ldd_data_inizio)

dw_report.Object.DataWindow.Print.preview = 'Yes'
		
dw_report.setredraw(true)

dw_report.groupcalc()

dw_folder.fu_selecttab(2)
end event

