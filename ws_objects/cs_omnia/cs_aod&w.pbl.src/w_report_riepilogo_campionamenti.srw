﻿$PBExportHeader$w_report_riepilogo_campionamenti.srw
$PBExportComments$Finestra Report Riepilogo Campionamenti
forward
global type w_report_riepilogo_campionamenti from w_cs_xx_principale
end type
type st_1 from statictext within w_report_riepilogo_campionamenti
end type
type cb_report from commandbutton within w_report_riepilogo_campionamenti
end type
type dw_selezione from uo_cs_xx_dw within w_report_riepilogo_campionamenti
end type
type dw_report from uo_cs_xx_dw within w_report_riepilogo_campionamenti
end type
type dw_folder from u_folder within w_report_riepilogo_campionamenti
end type
end forward

global type w_report_riepilogo_campionamenti from w_cs_xx_principale
integer width = 3776
integer height = 2048
string title = "Report Riepilogo Campionamenti"
boolean resizable = false
st_1 st_1
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_riepilogo_campionamenti w_report_riepilogo_campionamenti

type variables
string is_sql_originale
end variables

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report.modify(ls_modify)

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = st_1
////l_objects[4] = cb_ricerca_fornitore
//l_objects[4] = cb_ricerca_prodotto
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

is_sql_originale = dw_report.getsqlselect()
end event

on w_report_riepilogo_campionamenti.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_riepilogo_campionamenti.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW (dw_selezione,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_selezione,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale", "des_area",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type st_1 from statictext within w_report_riepilogo_campionamenti
integer x = 41
integer y = 648
integer width = 2048
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_report_riepilogo_campionamenti
integer x = 2098
integer y = 648
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_sql, ls_cod_prodotto, ls_cod_fornitore, ls_cod_operaio, ls_flag_esito,ls_azienda, &
       ls_cod_area_aziendale
long ll_anno, ll_ret

dw_selezione.accepttext()

dw_report.reset()
dw_report.setredraw(false)
ls_sql = ""

ll_anno = dw_selezione.getitemnumber(dw_selezione.getrow(),"anno")
ls_cod_prodotto = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_prodotto")
ls_cod_fornitore = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_fornitore")
ls_cod_operaio = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_operaio")
ls_flag_esito = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_esito_campionamento")
ls_cod_area_aziendale = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_area_aziendale")

ls_sql = lower(is_sql_originale)

if pos(ls_sql,"where",1) > 0 then
	ls_sql += " and "
else
	ls_sql += " where "
end if

ls_sql += " ( acc_materiali.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "

if ll_anno > 0 and not isnull(ll_anno) then
	ls_sql += " and ( acc_materiali.anno_reg_campionamenti = " + string(ll_anno) + " ) "
end if

if not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
	ls_sql += " and ( acc_materiali.cod_prodotto = '" + ls_cod_prodotto + "' ) "
end if	

if not isnull(ls_cod_fornitore) and len(trim(ls_cod_fornitore)) > 0 then
	ls_sql += " and ( acc_materiali.cod_fornitore = '" + ls_cod_fornitore + "' ) "
end if	

if not isnull(ls_cod_operaio) and len(trim(ls_cod_operaio)) > 0 then
	ls_sql += " and ( tes_campionamenti.cod_operaio = '" + ls_cod_operaio + "' ) "
end if	

if not isnull(ls_flag_esito) and len(trim(ls_flag_esito)) > 0 then
	ls_sql += " and ( acc_materiali.flag_esito_campionamento = '" + ls_flag_esito + "' ) "
end if	

if not isnull(ls_cod_area_aziendale) and len(trim(ls_cod_area_aziendale)) > 0 then
	ls_sql += " and ( acc_materiali.cod_area_aziendale = '" + ls_cod_area_aziendale + "' ) "
end if	

ll_ret = dw_report.setsqlselect(ls_sql)

if ll_ret = -1 then
	g_mb.messagebox("OMNIA","Errore nell'impostazione della select SQL; contattare l'amministratore del sistema.")
	return
end if

dw_folder.fu_selecttab(2)

dw_report.retrieve()

select rag_soc_1
into   :ls_azienda 
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.object.azienda.text = ls_azienda


dw_report.Object.DataWindow.Print.preview = 'Yes'

dw_report.setredraw(true)
end event

type dw_selezione from uo_cs_xx_dw within w_report_riepilogo_campionamenti
event ue_carica_tipologie ( )
event ue_cod_attr ( )
integer x = 23
integer y = 120
integer width = 2606
integer height = 620
integer taborder = 20
string dataobject = "d_sel_report_riepilogo_campionamenti"
boolean border = false
end type

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"rs_da_cod_attrezzatura")

ls_attr_a = getitemstring(getrow(),"rs_a_cod_attrezzatura")

ls_reparto = getitemstring(getrow(),"rs_cod_reparto")

ls_categoria = getitemstring(getrow(),"rs_cod_cat_attrezzatura")

ls_area = getitemstring(getrow(),"rs_cod_area_aziendale")

setnull(ls_null)

setitem(getrow(),"rs_cod_tipo_manut",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"rs_cod_tipo_manut",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_cod_attr();if not isnull(getitemstring(getrow(),"rs_da_cod_attrezzatura")) then
	setitem(getrow(),"rs_a_cod_attrezzatura",getitemstring(getrow(),"rs_da_cod_attrezzatura"))
end if
end event

event pcd_new;call super::pcd_new;setitem(getrow(),"anno",f_anno_esercizio())
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_riepilogo_campionamenti
integer x = 37
integer y = 112
integer width = 3680
integer height = 1808
integer taborder = 10
string dataobject = "d_report_riepilogo_campionamenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_riepilogo_campionamenti
integer x = 9
integer y = 12
integer width = 3730
integer height = 1932
integer taborder = 40
boolean border = false
end type

