﻿$PBExportHeader$w_det_analisi_cause_effetti.srw
$PBExportComments$Finestra Dettaglio Gestione Cause/Effetti
forward
global type w_det_analisi_cause_effetti from w_cs_xx_principale
end type
type cb_cancella from commandbutton within w_det_analisi_cause_effetti
end type
type cb_inserisci from commandbutton within w_det_analisi_cause_effetti
end type
type cb_salva from commandbutton within w_det_analisi_cause_effetti
end type
type tv_1 from treeview within w_det_analisi_cause_effetti
end type
type cb_stampa from commandbutton within w_det_analisi_cause_effetti
end type
type dw_report_analisi_cause_effetti from uo_cs_xx_dw within w_det_analisi_cause_effetti
end type
type st_1 from statictext within w_det_analisi_cause_effetti
end type
type st_2 from statictext within w_det_analisi_cause_effetti
end type
end forward

type wstr_modifiche from structure
	integer		cod_causa_effetto
	string		desc_causa_effetto
	integer		cod_padre
	character		tipo_modifica
end type

global type w_det_analisi_cause_effetti from w_cs_xx_principale
int Width=2803
int Height=1753
WindowType WindowType=response!
boolean TitleBar=true
string Title="Dettaglio Analisi Cause / Effetti"
boolean MinBox=false
boolean MaxBox=false
boolean Resizable=false
event ue_endeditlabel pbm_custom04
cb_cancella cb_cancella
cb_inserisci cb_inserisci
cb_salva cb_salva
tv_1 tv_1
cb_stampa cb_stampa
dw_report_analisi_cause_effetti dw_report_analisi_cause_effetti
st_1 st_1
st_2 st_2
end type
global w_det_analisi_cause_effetti w_det_analisi_cause_effetti

type variables
Long	il_drag_source, il_drag_parent, il_drop_target
DataStore	ids_source
integer ii_ultimo_codice
wstr_modifiche istr_modifiche[]
string is_old_label = ""
long il_handle_chg_label
boolean ib_modified = False
end variables

forward prototypes
public subroutine wf_popola (long handle_tvi, integer num_figli)
public function integer wf_inserisci ()
public function integer wf_cancella (boolean conferma)
public function integer wf_salva (boolean conferma)
public function integer wf_cancella_figli (integer ai_codice)
end prototypes

event ue_endeditlabel;call super::ue_endeditlabel;treeviewitem ltvi_cau_eff
integer li_num_modifica
long ll_handle

tv_1.GetItem ( il_handle_chg_label, ltvi_cau_eff )
if ltvi_cau_eff.label <> is_old_label then
	li_num_modifica = UpperBound(istr_modifiche) + 1
	istr_modifiche[li_num_modifica].cod_causa_effetto = integer(ltvi_cau_eff.Data)
	istr_modifiche[li_num_modifica].desc_causa_effetto = ltvi_cau_eff.Label
	istr_modifiche[li_num_modifica].tipo_modifica = "D"
	ib_modified = True
end if
end event

public subroutine wf_popola (long handle_tvi, integer num_figli);long ll_handle[]
integer li_cnt, li_rows, li_codici[]

TreeViewItem	ltvi_cau_eff

ltvi_cau_eff.pictureindex = 4
ltvi_cau_eff.selectedpictureindex = 5
For li_Cnt = 1 To num_figli
	li_codici[li_cnt] = ids_Source.Object.num_reg_causa_effetto[li_Cnt]
	ltvi_cau_eff.label = ids_Source.Object.des_causa_effetto[li_Cnt]
	ltvi_cau_eff.data = ids_Source.Object.num_reg_causa_effetto[li_Cnt]
	ll_handle[li_cnt] = tv_1.InsertItemSort(handle_tvi, ltvi_cau_eff)	
next
For li_Cnt = 1 To num_figli
	li_Rows = ids_Source.Retrieve(s_cs_xx.cod_azienda, li_codici[li_cnt])	
	if li_rows > 0 then
		wf_popola(ll_handle[li_cnt], li_rows)
	end if
next

end subroutine

public function integer wf_inserisci ();string ls_codici[]
long ll_handle[]
integer li_cnt, li_rows, li_num_modifica
long ll_cur_handle, ll_new_handle
TreeViewItem	ltvi_cau_eff

ll_cur_handle = tv_1.FindItem(CurrentTreeItem! , 0)
tv_1.GetItem( ll_cur_handle, ltvi_cau_eff )

li_num_modifica = UpperBound(istr_modifiche) + 1
istr_modifiche[li_num_modifica].cod_padre = integer(ltvi_cau_eff.data)

if ltvi_cau_eff.level = 1 then
	ltvi_cau_eff.pictureindex = 2
	ltvi_cau_eff.selectedpictureindex = 3
else
	ltvi_cau_eff.pictureindex = 4
	ltvi_cau_eff.selectedpictureindex = 5
end if

ii_ultimo_codice ++
ltvi_cau_eff.label = "Descrizione"
ltvi_cau_eff.data = ii_ultimo_codice
ll_new_handle = tv_1.InsertItemSort(ll_cur_handle, ltvi_cau_eff)	

istr_modifiche[li_num_modifica].cod_causa_effetto = integer(ltvi_cau_eff.Data)
istr_modifiche[li_num_modifica].desc_causa_effetto = ltvi_cau_eff.label
istr_modifiche[li_num_modifica].tipo_modifica = "I" // Inserimento

ib_modified = True
tv_1.ExpandItem( ll_cur_handle )
tv_1.SelectItem( ll_new_handle )
tv_1.SetFocus()
return 0
end function

public function integer wf_cancella (boolean conferma);long ll_cur_handle
TreeViewItem	ltvi_cau_eff
integer li_num_modifica

ll_cur_handle = tv_1.FindItem(CurrentTreeItem! , 0)
tv_1.GetItem( ll_cur_handle, ltvi_cau_eff )	
If ltvi_cau_eff.Level <> 1 Then
	if conferma then
		if g_mb.messagebox("OMNIA", "Vuoi Cancellare la Causa/Effetto Corrente e i suoi Figli?", &
						Question!, YesNo!, 2) = 2 then return -1
	end if					
	li_num_modifica = UpperBound(istr_modifiche) + 1
	istr_modifiche[li_num_modifica].cod_causa_effetto = integer(ltvi_cau_eff.data)
	istr_modifiche[li_num_modifica].tipo_modifica = "C"

	ib_modified = True
	tv_1.DeleteItem( ll_cur_handle )
	tv_1.SetFocus()
else
	return -1
end if
return 0
end function

public function integer wf_salva (boolean conferma);Integer	li_Cnt, li_num_modifiche

if conferma then
	if g_mb.messagebox("OMNIA", "Vuoi Salvare le Modifiche Apportate?", &
					Question!, YesNo!, 2) = 2 then return -1
end if					

li_num_modifiche = UpperBound(istr_modifiche)

For li_Cnt = 1 To li_num_modifiche

	choose case istr_modifiche[li_Cnt].tipo_modifica
		case "I"  // Inserimento
		  INSERT INTO tab_analisi_cause_effetti  
	         ( cod_azienda,
				  num_reg_causa_effetto,   
	           des_causa_effetto,   
	           num_reg_causa_eff_padre )  
		  VALUES (:s_cs_xx.cod_azienda, 
		          :istr_modifiche[li_Cnt].cod_causa_effetto,   
	             :istr_modifiche[li_Cnt].desc_causa_effetto,   
	             :istr_modifiche[li_Cnt].cod_padre )  ;
		case "P"  // Modifica Padre
			UPDATE "tab_analisi_cause_effetti"
		      SET "num_reg_causa_eff_padre" = :istr_modifiche[li_Cnt].cod_padre 
			 WHERE "tab_analisi_cause_effetti"."num_reg_causa_effetto" = :istr_modifiche[li_Cnt].cod_causa_effetto AND
			 		 "tab_analisi_cause_effetti"."cod_azienda" = :s_cs_xx.cod_azienda;
		case "D"  // Modifica Descrizione
			UPDATE "tab_analisi_cause_effetti"
		      SET "des_causa_effetto" = :istr_modifiche[li_Cnt].desc_causa_effetto
			 WHERE "tab_analisi_cause_effetti"."num_reg_causa_effetto" = :istr_modifiche[li_Cnt].cod_causa_effetto AND
			 		 "tab_analisi_cause_effetti"."cod_azienda" = :s_cs_xx.cod_azienda;
		case "C"  // Cancellazione
			wf_cancella_figli(istr_modifiche[li_cnt].cod_causa_effetto)
	      DELETE FROM tab_analisi_cause_effetti
			 WHERE tab_analisi_cause_effetti.num_reg_causa_effetto = :istr_modifiche[li_cnt].cod_causa_effetto AND
			 		 "tab_analisi_cause_effetti"."cod_azienda" = :s_cs_xx.cod_azienda;
	end choose

	If sqlca.sqlcode <> 0 Then
		g_mb.messagebox("OMNIA", "Errore durante il Salvataggio: " + &
						istr_modifiche[li_Cnt].desc_causa_effetto, Exclamation!)
		Rollback;
		return -1
	else
		istr_modifiche[li_Cnt].tipo_modifica = 'S' // Salvato
End If
Next
Commit;
return 0
end function

public function integer wf_cancella_figli (integer ai_codice);// ai_codice è il codice del padre delle righe da cencellare

integer li_cur_figlio, li_num_figlio, li_figli[], li_index

DECLARE cur_num_reg_cau_eff CURSOR FOR  
  SELECT "tab_analisi_cause_effetti"."num_reg_causa_effetto"  
    FROM "tab_analisi_cause_effetti"
 	 WHERE "tab_analisi_cause_effetti"."num_reg_causa_eff_padre" = :ai_codice and
		    "tab_analisi_cause_effetti"."cod_azienda" = :s_cs_xx.cod_azienda;

OPEN cur_num_reg_cau_eff;

FETCH cur_num_reg_cau_eff INTO :li_cur_figlio;

DO WHILE SQLCA.sqlcode = 0
	li_num_figlio = UpperBound(li_figli) + 1
	li_figli[li_num_figlio] = li_cur_figlio
	FETCH cur_num_reg_cau_eff INTO :li_cur_figlio;
LOOP

CLOSE cur_num_reg_cau_eff;

for li_index = 1 to UpperBound(li_figli)
	wf_cancella_figli(li_figli[li_index])

	DELETE FROM "tab_analisi_cause_effetti"  
		 WHERE "tab_analisi_cause_effetti"."num_reg_causa_eff_padre" = :ai_codice;
	next

return UpperBound(li_figli)
end function

on w_det_analisi_cause_effetti.create
int iCurrent
call w_cs_xx_principale::create
this.cb_cancella=create cb_cancella
this.cb_inserisci=create cb_inserisci
this.cb_salva=create cb_salva
this.tv_1=create tv_1
this.cb_stampa=create cb_stampa
this.dw_report_analisi_cause_effetti=create dw_report_analisi_cause_effetti
this.st_1=create st_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_cancella
this.Control[iCurrent+2]=cb_inserisci
this.Control[iCurrent+3]=cb_salva
this.Control[iCurrent+4]=tv_1
this.Control[iCurrent+5]=cb_stampa
this.Control[iCurrent+6]=dw_report_analisi_cause_effetti
this.Control[iCurrent+7]=st_1
this.Control[iCurrent+8]=st_2
end on

on w_det_analisi_cause_effetti.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_cancella)
destroy(this.cb_inserisci)
destroy(this.cb_salva)
destroy(this.tv_1)
destroy(this.cb_stampa)
destroy(this.dw_report_analisi_cause_effetti)
destroy(this.st_1)
destroy(this.st_2)
end on

event open;call super::open;integer li_cnt, li_rows, li_max_codice
long ll_handle_root
TreeViewItem	ltvi_cau_eff

SELECT max(tab_analisi_cause_effetti.num_reg_causa_effetto)
  INTO :ii_ultimo_codice  
  FROM tab_analisi_cause_effetti  ;
	 
ids_source = Create DataStore

ids_source.DataObject = 'd_ana_cause_effetti'
ids_source.SetTransObject(sqlca)

ltvi_cau_eff.pictureindex = 1
ltvi_cau_eff.selectedpictureindex = 1
ltvi_cau_eff.children = True
ltvi_cau_eff.label = s_cs_xx.parametri.parametro_s_1 
ltvi_cau_eff.data = s_cs_xx.parametri.parametro_i_1
ll_handle_root = tv_1.InsertItemSort(0, ltvi_cau_eff)	

li_Rows = ids_source.Retrieve( s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_i_1 )

if li_rows > 0 then
	wf_popola(ll_handle_root, li_rows)
end if
tv_1.SetLevelPictures ( 2, 2, 3, 3, 3 )

tv_1.ExpandAll ( ll_handle_root )
end event

event close;call super::close;if ib_modified then
	wf_salva(True)
end if	
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_toolbarnone + c_noenablepopup + c_toolbarhidetext )

dw_report_analisi_cause_effetti.set_dw_options(sqlca, &
                                   pcca.null_object, &
                                   c_nomodify + &
                                   c_nodelete + &
                                   c_nonew + &
                                   c_noquery + &
                                   c_disableCC, &
                                   c_noresizedw + &
                                   c_nohighlightselected + &
                                   c_nocursorrowpointer +&
                                   c_nocursorrowfocusrect )

iuo_dw_main = dw_report_analisi_cause_effetti

end event

type cb_cancella from commandbutton within w_det_analisi_cause_effetti
int X=2012
int Y=1561
int Width=366
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="&Cancella"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;wf_cancella(True)
end event

type cb_inserisci from commandbutton within w_det_analisi_cause_effetti
int X=1623
int Y=1561
int Width=366
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="&Inserisci"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;wf_inserisci()
end event

type cb_salva from commandbutton within w_det_analisi_cause_effetti
int X=1235
int Y=1561
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Salva"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if ib_modified then
	if wf_salva(False) = 0 then ib_modified = False
else
	g_mb.messagebox("OMNIA", "Nessuna Modifica da Salvare!")
end if
tv_1.SetFocus()
end event

type tv_1 from treeview within w_det_analisi_cause_effetti
event begindrag pbm_tvnbegindrag
event beginlabeledit pbm_tvnbeginlabeledit
event doubleclicked pbm_tvndoubleclicked
event dragdrop pbm_tvndragdrop
event dragwithin pbm_tvndragwithin
event endlabeledit pbm_tvnendlabeledit
event itempopulate pbm_tvnitempopulate
int X=23
int Y=21
int Width=2743
int Height=1521
int TabOrder=10
boolean DragAuto=true
BorderStyle BorderStyle=StyleLowered!
boolean DisableDragDrop=false
boolean EditLabels=true
boolean LinesAtRoot=true
string PictureName[]={"Globals!",&
"MoveMode!",&
"RetrieveCancel!",&
"Custom039!",&
"Custom050!"}
long PictureMaskColor=12632256
long StatePictureMaskColor=553648127
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event begindrag;TreeViewItem		ltvi_source

GetItem(handle, ltvi_source)

If ltvi_source.Level = 1 Then
	This.Drag(Cancel!)
Else
	il_drag_source = handle
	il_drag_parent = FindItem(ParentTreeItem!, handle)
End If

end event

event beginlabeledit;treeviewitem ltvi_cau_eff

GetItem ( handle, ltvi_cau_eff )
is_old_label = ltvi_cau_eff.label
il_handle_chg_label = handle
end event

event dragdrop;Integer				li_num_modifica, li_rows, ll_handle_parent
Long					ll_new_item
TreeViewItem		ltvi_target, ltvi_source, ltvi_parent, ltvi_new

If GetItem(il_drop_target, ltvi_target) = -1 Then Return
If GetItem(il_drag_source, ltvi_source) = -1 Then Return

ll_handle_parent = il_drop_target
do while ll_handle_parent <> 1 and ll_handle_parent <> il_drag_source
	ll_handle_parent = FindItem(ParentTreeItem!, ll_handle_parent)
loop
if ll_handle_parent = il_drag_source then
	g_mb.messagebox("OMNIA", "Non si può spostare una Causa/Effetto su un figlio!" )
	return
end if

GetItem(il_drag_parent, ltvi_parent)
If g_mb.messagebox("OMNIA", "Vuoi spostare " + &
						ltvi_source.Label + " da " + ltvi_parent.label + " a " + ltvi_target.label + &
						"?", Question!, YesNo!) = 1 Then

	li_num_modifica = UpperBound(istr_modifiche) + 1

	istr_modifiche[li_num_modifica].cod_causa_effetto = integer(ltvi_source.Data)
	istr_modifiche[li_num_modifica].cod_padre = integer(ltvi_target.Data)
	istr_modifiche[li_num_modifica].tipo_modifica = "P"
	ib_modified = True

	SetNull(ltvi_source.ItemHandle)

	if ltvi_target.level = 1 then
		ltvi_source.PictureIndex = 2
		ltvi_source.SelectedPictureIndex = 3
	else
		ltvi_source.PictureIndex = 4
		ltvi_source.SelectedPictureIndex = 5
	end if

	ll_new_item = InsertItemSort(il_drop_target, ltvi_source)

	li_rows = ids_Source.Retrieve(s_cs_xx.cod_azienda, ltvi_source.Data)

	if li_rows > 0 then
		wf_popola(ll_new_item, li_rows)
	end if

	tv_1.ExpandAll ( ll_new_item )

	DeleteItem(il_drag_source)
	SelectItem(ll_new_item)
end if
end event

event dragwithin;TreeViewItem	ltvi_over

If GetItem(handle, ltvi_over) = -1 Then
	SetDropHighlight(0)
	il_drop_target = 0
	Return
End If

If handle <> il_drag_parent and handle <> il_drag_source Then
	SetDropHighlight(handle)
	il_drop_target = handle
Else
	SetDropHighlight(0)
	il_drop_target = 0
End If

end event

event endlabeledit;parent.postevent ("ue_endeditlabel")
end event

event rightclicked;TreeViewItem		ltvi_cau_eff

GetItem(handle, ltvi_cau_eff)

If ltvi_cau_eff.Level <> 1 Then
	tv_1.EditLabel(handle)
end if	
end event

type cb_stampa from commandbutton within w_det_analisi_cause_effetti
event clicked pbm_bnclicked
int X=2401
int Y=1561
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Report"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_cur_handle
TreeViewItem	ltvi_cau_eff

if text = "&Report" then
	ll_cur_handle = tv_1.FindItem(CurrentTreeItem! , 0)
	tv_1.GetItem( ll_cur_handle, ltvi_cau_eff )	
	tv_1.visible = false
	dw_report_analisi_cause_effetti.Retrieve(s_cs_xx.cod_azienda, integer(ltvi_cau_eff.data))
	dw_report_analisi_cause_effetti.visible = true
	text = "&Grafico"
	cb_cancella.enabled = False
	cb_inserisci.enabled = False
	st_1.visible = False
	st_2.visible = False
	cb_salva.enabled = False
else
	tv_1.visible = true
	dw_report_analisi_cause_effetti.visible = false
	text = "&Report"
	cb_cancella.enabled = True
	cb_inserisci.enabled = True
	cb_salva.enabled = True
	st_1.visible = True
	st_2.visible = True
	tv_1.SetFocus()
end if
end event

type dw_report_analisi_cause_effetti from uo_cs_xx_dw within w_det_analisi_cause_effetti
int X=23
int Y=21
int Width=2743
int Height=1521
int TabOrder=2
boolean Visible=false
string DataObject="d_report_analisi_cause_effetti"
boolean HScrollBar=true
boolean VScrollBar=true
end type

type st_1 from statictext within w_det_analisi_cause_effetti
int X=23
int Y=1541
int Width=1153
int Height=49
boolean Enabled=false
boolean BringToTop=true
string Text="Cliccare col tasto destro del mouse in prossimità"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-6
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_det_analisi_cause_effetti
int X=23
int Y=1589
int Width=1153
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="della descrizione che si desidera modificare"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-6
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

