﻿$PBExportHeader$w_det_analisi_stat.srw
$PBExportComments$Finestra Dettaglio Analisi Statistiche
forward
global type w_det_analisi_stat from w_cs_xx_principale
end type
type dw_det_analisi_stat_lista from uo_cs_xx_dw within w_det_analisi_stat
end type
type cb_foglio_calcolo from commandbutton within w_det_analisi_stat
end type
type cb_search_prodotto from cb_prod_ricerca within w_det_analisi_stat
end type
type cb_stock from cb_stock_ricerca within w_det_analisi_stat
end type
type dw_det_analisi_stat_det_1 from uo_cs_xx_dw within w_det_analisi_stat
end type
type dw_det_analisi_stat_det_2 from uo_cs_xx_dw within w_det_analisi_stat
end type
type dw_folder from u_folder within w_det_analisi_stat
end type
end forward

global type w_det_analisi_stat from w_cs_xx_principale
integer width = 2505
integer height = 1588
string title = "Dettaglio Analisi Statistica"
dw_det_analisi_stat_lista dw_det_analisi_stat_lista
cb_foglio_calcolo cb_foglio_calcolo
cb_search_prodotto cb_search_prodotto
cb_stock cb_stock
dw_det_analisi_stat_det_1 dw_det_analisi_stat_det_1
dw_det_analisi_stat_det_2 dw_det_analisi_stat_det_2
dw_folder dw_folder
end type
global w_det_analisi_stat w_det_analisi_stat

type variables
boolean ib_new=False
end variables

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_analisi_stat_det_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_analisi_stat_det_1,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "(anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_prodotti.flag_blocco <> 'S') or (anag_prodotti.flag_blocco = 'S' and anag_prodotti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_det_analisi_stat_det_2,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_det_analisi_stat_det_2
l_objects[2] = cb_stock
dw_folder.fu_AssignTab(2, "&Gestione Stock", l_Objects[])

l_objects[1] = dw_det_analisi_stat_det_1
l_objects[2] = cb_search_prodotto
dw_folder.fu_AssignTab(1, "&Dati Analisi", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_det_analisi_stat_lista.set_dw_key("cod_azienda")
dw_det_analisi_stat_lista.set_dw_key("anno_registrazione")
dw_det_analisi_stat_lista.set_dw_key("num_registrazione")
dw_det_analisi_stat_lista.set_dw_key("prog_riga_analisi")
dw_det_analisi_stat_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_det_analisi_stat_det_1.set_dw_options(sqlca,dw_det_analisi_stat_lista,c_sharedata+c_scrollparent,c_default)
dw_det_analisi_stat_det_2.set_dw_options(sqlca,dw_det_analisi_stat_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_det_analisi_stat_lista

dw_folder.fu_SelectTab(1)
end event

on w_det_analisi_stat.create
int iCurrent
call super::create
this.dw_det_analisi_stat_lista=create dw_det_analisi_stat_lista
this.cb_foglio_calcolo=create cb_foglio_calcolo
this.cb_search_prodotto=create cb_search_prodotto
this.cb_stock=create cb_stock
this.dw_det_analisi_stat_det_1=create dw_det_analisi_stat_det_1
this.dw_det_analisi_stat_det_2=create dw_det_analisi_stat_det_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_analisi_stat_lista
this.Control[iCurrent+2]=this.cb_foglio_calcolo
this.Control[iCurrent+3]=this.cb_search_prodotto
this.Control[iCurrent+4]=this.cb_stock
this.Control[iCurrent+5]=this.dw_det_analisi_stat_det_1
this.Control[iCurrent+6]=this.dw_det_analisi_stat_det_2
this.Control[iCurrent+7]=this.dw_folder
end on

on w_det_analisi_stat.destroy
call super::destroy
destroy(this.dw_det_analisi_stat_lista)
destroy(this.cb_foglio_calcolo)
destroy(this.cb_search_prodotto)
destroy(this.cb_stock)
destroy(this.dw_det_analisi_stat_det_1)
destroy(this.dw_det_analisi_stat_det_2)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;if not (dw_det_analisi_stat_lista.RowCount() > 0 and dw_det_analisi_stat_lista.GetItemNumber(dw_det_analisi_stat_lista.GetRow(),"num_registrazione") <> 0) then
	cb_foglio_calcolo.enabled = False
end if
end event

event pc_save;call super::pc_save;if not (dw_det_analisi_stat_lista.RowCount() > 0 and dw_det_analisi_stat_lista.GetItemNumber(dw_det_analisi_stat_lista.GetRow(),"num_registrazione") <> 0) then
	cb_foglio_calcolo.enabled = False
else
	cb_foglio_calcolo.enabled = True	
end if
end event

event pc_modify;call super::pc_modify;cb_foglio_calcolo.enabled = False
end event

event pc_new;call super::pc_new;cb_foglio_calcolo.enabled = False
end event

event pc_view;call super::pc_view;if not (dw_det_analisi_stat_lista.RowCount() > 0 and dw_det_analisi_stat_lista.GetItemNumber(dw_det_analisi_stat_lista.GetRow(),"num_registrazione") <> 0) then
	cb_foglio_calcolo.enabled = False
else
	cb_foglio_calcolo.enabled = True	
end if

end event

type dw_det_analisi_stat_lista from uo_cs_xx_dw within w_det_analisi_stat
integer x = 23
integer y = 20
integer width = 2034
integer height = 500
integer taborder = 40
string dataobject = "d_det_analisi_stat_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "anno_registrazione")) THEN
      SetItem(l_Idx, "anno_registrazione", 1)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "num_registrazione")) THEN
      SetItem(l_Idx, "num_registrazione", 1)
   END IF
NEXT
//FOR l_Idx = 1 TO RowCount()
//   IF IsNull(GetItemnumber(l_Idx, "prog_riga_analisi")) THEN
//      SetItem(l_Idx, "prog_riga_analisi", 1)
//   END IF
//NEXT
//
end event

event updatestart;call super::updatestart;long ll_prog_riga_analisi, ll_num_registrazione, ll_anno_registrazione

if ib_new then
	ll_num_registrazione = i_parentdw.getitemnumber( i_parentdw.getrow(), "num_registrazione" )
	ll_anno_registrazione = i_parentdw.getitemnumber( i_parentdw.getrow(), "anno_registrazione" )
   select max(det_analisi_stat.prog_riga_analisi)
     into :ll_prog_riga_analisi
     from det_analisi_stat
     where (det_analisi_stat.cod_azienda = :s_cs_xx.cod_azienda) and 
	  		  (det_analisi_stat.anno_registrazione = :ll_anno_registrazione) and
 	  		  (det_analisi_stat.num_registrazione = :ll_num_registrazione);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_prog_riga_analisi) then
      ll_prog_riga_analisi = 1
   else
      ll_prog_riga_analisi = ll_prog_riga_analisi + 1
   end if
   this.SetItem (this.GetRow ( ),"anno_registrazione", ll_anno_registrazione)
   this.SetItem (this.GetRow ( ),"num_registrazione", ll_num_registrazione)
   this.SetItem (this.GetRow ( ),"prog_riga_analisi", ll_prog_riga_analisi)

   ib_new = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_stock.enabled = true
	cb_search_prodotto.enabled = true
	ib_new = true
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_num_registrazione, ll_anno_registrazione, l_Error
	
//ll_num_registrazione = i_parentdw.getitemnumber( i_parentdw.getrow(), "num_registrazione" )
//ll_anno_registrazione = i_parentdw.getitemnumber( i_parentdw.getrow(), "anno_registrazione" )
ll_num_registrazione = i_parentdw.getitemnumber( i_parentdw.i_selectedrows[1], "num_registrazione" )
ll_anno_registrazione = i_parentdw.getitemnumber( i_parentdw.i_selectedrows[1], "anno_registrazione" )

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione )

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if l_Error = 0 then
	cb_foglio_calcolo.enabled = False
end if	
end event

event pcd_modify;call super::pcd_modify;cb_stock.enabled = true
cb_search_prodotto.enabled = true

end event

event pcd_view;call super::pcd_view;cb_stock.enabled = false
cb_search_prodotto.enabled = false

end event

type cb_foglio_calcolo from commandbutton within w_det_analisi_stat
integer x = 2080
integer y = 20
integer width = 361
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "F. &Calcolo"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = dw_det_analisi_stat_lista.GetItemNumber( dw_det_analisi_stat_lista.GetRow(), "anno_registrazione" )
s_cs_xx.parametri.parametro_d_1 = dw_det_analisi_stat_lista.GetItemNumber( dw_det_analisi_stat_lista.GetRow(), "num_registrazione" )
s_cs_xx.parametri.parametro_d_2 = dw_det_analisi_stat_lista.GetItemNumber( dw_det_analisi_stat_lista.GetRow(), "prog_riga_analisi" )
Open (w_blob_analisi_stat)

end event

type cb_search_prodotto from cb_prod_ricerca within w_det_analisi_stat
integer x = 2309
integer y = 1040
integer width = 73
integer height = 80
integer taborder = 20
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_det_analisi_stat_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type cb_stock from cb_stock_ricerca within w_det_analisi_stat
integer x = 1687
integer y = 920
integer width = 73
integer height = 80
integer taborder = 10
end type

event clicked;call super::clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

string ls_cod_prodotto
ls_cod_prodotto = dw_det_analisi_stat_det_1.getitemstring(dw_det_analisi_stat_det_1.getrow(), "cod_prodotto")

if isnull (ls_cod_prodotto) then
	ls_cod_prodotto = dw_det_analisi_stat_lista.i_parentdw.getitemstring(dw_det_analisi_stat_lista.i_parentdw.getrow(),"cod_prodotto")
	if not isnull(ls_cod_prodotto) then
		dw_det_analisi_stat_det_1.setitem(dw_det_analisi_stat_det_1.getrow(),"cod_prodotto", ls_cod_prodotto)
	end if
end if

s_cs_xx.parametri.parametro_s_10 = dw_det_analisi_stat_det_1.getitemstring(dw_det_analisi_stat_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_det_analisi_stat_det_2.getitemstring(dw_det_analisi_stat_det_2.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_det_analisi_stat_det_2.getitemstring(dw_det_analisi_stat_det_2.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_det_analisi_stat_det_2.getitemstring(dw_det_analisi_stat_det_2.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0

dw_det_analisi_stat_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end event

type dw_det_analisi_stat_det_1 from uo_cs_xx_dw within w_det_analisi_stat
integer x = 41
integer y = 660
integer width = 2377
integer height = 776
integer taborder = 70
string dataobject = "d_det_analisi_stat_det_1"
boolean border = false
end type

type dw_det_analisi_stat_det_2 from uo_cs_xx_dw within w_det_analisi_stat
integer x = 41
integer y = 660
integer width = 2354
integer height = 776
integer taborder = 30
string dataobject = "d_det_analisi_stat_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_det_analisi_stat
integer x = 23
integer y = 540
integer width = 2423
integer height = 916
integer taborder = 60
boolean border = false
end type

