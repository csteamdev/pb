﻿$PBExportHeader$w_quick_select.srw
$PBExportComments$Finestra per la Creazione di Scripts SQL
forward
global type w_quick_select from Window
end type
type st_17 from statictext within w_quick_select
end type
type st_11 from statictext within w_quick_select
end type
type st_3 from statictext within w_quick_select
end type
type st_1 from statictext within w_quick_select
end type
type dw_tables from datawindow within w_quick_select
end type
type cb_addall from commandbutton within w_quick_select
end type
type st_16 from statictext within w_quick_select
end type
type st_15 from statictext within w_quick_select
end type
type st_14 from statictext within w_quick_select
end type
type st_13 from statictext within w_quick_select
end type
type st_12 from statictext within w_quick_select
end type
type mle_comments from multilineedit within w_quick_select
end type
type st_10 from statictext within w_quick_select
end type
type dw_columns from datawindow within w_quick_select
end type
type st_9 from statictext within w_quick_select
end type
type st_8 from statictext within w_quick_select
end type
type cb_cancel from commandbutton within w_quick_select
end type
type cb_ok from commandbutton within w_quick_select
end type
type st_7 from statictext within w_quick_select
end type
type st_6 from statictext within w_quick_select
end type
type st_5 from statictext within w_quick_select
end type
type st_4 from statictext within w_quick_select
end type
type st_2 from statictext within w_quick_select
end type
type gb_1 from groupbox within w_quick_select
end type
type dw_criteria from datawindow within w_quick_select
end type
end forward

global type w_quick_select from Window
int X=193
int Y=17
int Width=2113
int Height=1789
boolean TitleBar=true
string Title="Costruzione Selezione"
long BackColor=79741120
ToolBarAlignment ToolBarAlignment=AlignAtLeft!
WindowType WindowType=response!
st_17 st_17
st_11 st_11
st_3 st_3
st_1 st_1
dw_tables dw_tables
cb_addall cb_addall
st_16 st_16
st_15 st_15
st_14 st_14
st_13 st_13
st_12 st_12
mle_comments mle_comments
st_10 st_10
dw_columns dw_columns
st_9 st_9
st_8 st_8
cb_cancel cb_cancel
cb_ok cb_ok
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_2 st_2
gb_1 gb_1
dw_criteria dw_criteria
end type
global w_quick_select w_quick_select

type variables
integer il_HighlightedTable, il_HighlightedColumn
String is_visiblecolumns[], is_TableName



end variables

forward prototypes
public function string wf_replace_underscores_with_spaces (string a_instring)
public function integer wf_parse_object (ref datawindow dw_arg, ref string obj_list[], string obj_type, string band)
public function string wf_create_dw ()
end prototypes

public function string wf_replace_underscores_with_spaces (string a_instring);// string Function wf_replace_underscores_with_spaces (string a_instring)

// Returns value of a_instring with each underscore replaced by a space character

int		p
string	s_in, s_out

s_in = a_instring
p = Pos ( s_in, '_' )

do while p > 0
	s_out =	s_out + Left ( s_in, p -1 ) + ' '
	s_in   = Mid ( s_in, p +1)	
	p = Pos (s_in, '_' )
loop

s_out = s_out + s_in

return  s_out

end function

public function integer wf_parse_object (ref datawindow dw_arg, ref string obj_list[], string obj_type, string band);string obj_string, obj_holder
int obj_count, start_pos=1, tab_pos, count = 0

obj_string = dw_arg.Describe("datawindow.objects")

tab_pos =  Pos(obj_string,"~t",start_pos)
DO WHILE tab_pos > 0
	obj_holder = Mid(obj_string,start_pos,(tab_pos - start_pos))
	IF (dw_arg.Describe(obj_holder+".type") = obj_type or obj_type = "*") AND &
		 (dw_arg.Describe(obj_holder+".band") = band or band = "*") THEN
			count = count + 1
			obj_list[count] = obj_holder
	END IF
start_pos = tab_pos + 1
tab_pos =  Pos(obj_string,"~t",start_pos)	
LOOP 
obj_holder = Mid(obj_string,start_pos,Len(obj_string))
IF (dw_arg.Describe(obj_holder+".type") = obj_type or obj_type = "*") AND &
		 (dw_arg.Describe(obj_holder+".band") = band or band = "*") THEN
	count = count + 1
	obj_list[count] = obj_holder
END IF

Return count
end function

public function string wf_create_dw ();/*************************************************************************
		Make sure that the Quick Select window is open,  if it is then
		use the syntax in the grid datawindow (dw_criteria) as the source 
		for the DataWindow control in this window.  If the Quick Select 
		window is not open then give a message and default with a blank 
		DW Control and disable the capability of saving the DataWindow.

		The syntax for the datawindow will have to be modified to discover
		the visible fields, and then to build the Where statement by taking
		values out of the Grid DataWindow.  The Existing Grid DataWindow 
		Selects all columns from the table and then works depending on the 
		visible attributes.
*************************************************************************/
int 			li_createreturncode, li_arraybound, li_index, li_colcount
String 		ls_dwselectstmt, ls_whereclause, ls_criteriaselectstmt, ls_rc
String		ls_orderclause, ls_column, ls_report_type
long		ll_start, ll_end, ll_rc

	//*********************************************************************
	//		Build the Select statement Based on the Visible columns
	//		Copy the Grid DW from the Quick Select window to the local
	//		DW here and the instance array of visible column names.
	//*********************************************************************	

	ls_criteriaselectstmt = dw_Criteria.getsqlselect()
 	is_VisibleColumns = is_VisibleColumns
	is_TableName	= is_TableName

	//*********************************************************************
	//		Build the Select Statement
	//*********************************************************************
	ls_dwselectstmt = "Select " 
	li_arraybound = UpperBound(is_VisibleColumns)
	For li_index = 1 to li_arraybound
			If is_VisibleColumns[li_index] <> "" then
				ls_dwselectstmt = ls_dwselectstmt + is_VisibleColumns[li_index]
				If li_index <> li_arraybound then
					ls_dwselectstmt = ls_dwselectstmt + ", "
				End If
			End If
	end for	

	// add the FROM clause 
	ls_dwselectstmt = ls_dwselectstmt + " From " + is_TableName

	////////////////////////////////////////////////////////////////////////////////////
	//build the where clause from the querymode datawindow
	///////////////////////////////////////////////////////////////////////////////////
	ll_start = pos(lower(ls_criteriaselectstmt)," where ")
	
	If ll_start > 0 Then
	//order of stmts after where are groub by, having, order by 
	//however, in this example, only ORDER BY can occur
	//check if this occurs, if so, this is the end of the where clause
		ll_end = Pos(lower(ls_criteriaselectstmt)," order by ")

		If ll_end = 0 Then 	// no order by
			ll_end = len (ls_criteriaselectstmt)
		End If

		ls_whereclause = Mid ( ls_criteriaselectstmt, ll_start, ll_end - ll_start +1)
		ls_dwselectstmt = ls_dwselectstmt + ls_whereclause	

	End If 	//Get Where Clause


	////////////////////////////////////////////////////////////////////////////////////
	//build the order by clause from the querymode datawindow
	//This routine will parse through the criteria order by statement
	//converting the column #'s to database column names.
	//This is because the column #'s do not match the new
	//datawindow's column numbers.
	///////////////////////////////////////////////////////////////////////////////////
	ll_start = pos(lower(ls_criteriaselectstmt)," order by ")
	
	If ll_start > 0 Then
		// there is an order by, now parse it out
		ll_end = len (ls_criteriaselectstmt)
	
		ls_orderclause = " Order By "
	
		ll_start = ll_start + 11 // skip past the order by
		ll_end = pos(ls_criteriaselectstmt, " ", ll_start)
		do while ll_end > 0
			If Lower(Mid(ls_criteriaselectstmt, ll_start, 1)) = "a" or  &
			   Lower(Mid(ls_criteriaselectstmt, ll_start, 1)) = "d"  Then
				//This is the asc or desc stmt, just copy it
				ll_rc = pos (ls_criteriaselectstmt, ",", ll_end) + 1   // adjust for space after order sequence 
				If ll_rc > 1 Then ll_end = ll_rc    //no need to adjust if this was the last asc/desc 
				ls_orderclause = ls_orderclause + mid(ls_criteriaselectstmt, ll_start, ll_end - ll_start + 1)
	      Else  			// must be a column # --- convert it to a column name
				ls_column = mid (ls_criteriaselectstmt, ll_start, ll_end - ll_start +1)
				ls_orderclause = ls_orderclause + &
					dw_Criteria.describe ("#" + ls_column + ".dbname") + " "
	      End If				

			//move start to next token
			ll_start = ll_end + 1
			ll_end = pos(ls_criteriaselectstmt, " ", ll_start)
		loop
		
		//build it into main select statement
		ls_dwselectstmt = ls_dwselectstmt + ls_orderclause	

	End If 	//Get Order Clause
return ls_dwselectstmt
end function

event open;string ls_sql
// Open script for w_quick_select

dw_tables.SetTransObject (sqlca)
dw_columns.SetTransObject (sqlca)

// Controllo il DBMS a cui sono attaccato

choose case Upper(Left(sqlca.dbms,3) )

	case "ODB"		// ODBC
		ls_sql = 'SELECT DISTINCT "DBA"."PBCATTBL"."PBT_TNAM", "DBA"."PBCATTBL"."PBT_CMNT" ' &  
			           + ' FROM "DBA"."PBCATTBL" ' &  
				        + ' order by 1 ASC'   ;
						  
	case "ORA", "OR6", "OR7"	// ORACLE
		ls_sql = "SELECT DISTINCT SYSTEM.PBCATTBL.PBT_TNAM, SYSTEM.PBCATTBL.PBT_CMNT " &  
			   		  + " FROM SYSTEM.PBCATTBL " &  
				        + " order by 1 ASC"   ;
	case "O72"	// ORACLE 7.2
//		ls_sql = "SELECT DISTINCT SYSTEM.TAB.TNAME " &  
//			   		  + " FROM SYSTEM.TAB " &  
//				        + " order by 1 ASC"   ;
		ls_sql = "SELECT DISTINCT SYS.CATALOG.TNAME " &  
			   		  + " FROM SYS.CATALOG " &  
						  + " WHERE SYS.CATALOG.CREATOR = 'CS_XX' AND " &
						  + " SYS.CATALOG.TABLETYPE = 'TABLE' " &
				        + " order by 1 ASC"   ;
				
case else
		g_mb.messagebox ("OMNIA", "DataBase Sconosciuto!")
end choose

dw_tables.SetSQLSelect(ls_sql)
dw_tables.Retrieve ()
end event

on w_quick_select.create
this.st_17=create st_17
this.st_11=create st_11
this.st_3=create st_3
this.st_1=create st_1
this.dw_tables=create dw_tables
this.cb_addall=create cb_addall
this.st_16=create st_16
this.st_15=create st_15
this.st_14=create st_14
this.st_13=create st_13
this.st_12=create st_12
this.mle_comments=create mle_comments
this.st_10=create st_10
this.dw_columns=create dw_columns
this.st_9=create st_9
this.st_8=create st_8
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_2=create st_2
this.gb_1=create gb_1
this.dw_criteria=create dw_criteria
this.Control[]={ this.st_17,&
this.st_11,&
this.st_3,&
this.st_1,&
this.dw_tables,&
this.cb_addall,&
this.st_16,&
this.st_15,&
this.st_14,&
this.st_13,&
this.st_12,&
this.mle_comments,&
this.st_10,&
this.dw_columns,&
this.st_9,&
this.st_8,&
this.cb_cancel,&
this.cb_ok,&
this.st_7,&
this.st_6,&
this.st_5,&
this.st_4,&
this.st_2,&
this.gb_1,&
this.dw_criteria}
end on

on w_quick_select.destroy
destroy(this.st_17)
destroy(this.st_11)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.dw_tables)
destroy(this.cb_addall)
destroy(this.st_16)
destroy(this.st_15)
destroy(this.st_14)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.mle_comments)
destroy(this.st_10)
destroy(this.dw_columns)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.gb_1)
destroy(this.dw_criteria)
end on

type st_17 from statictext within w_quick_select
int X=1049
int Y=212
int Width=843
int Height=68
boolean Enabled=false
string Text="trascinalo nel box ~"Commenti~"."
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_11 from statictext within w_quick_select
int X=40
int Y=1041
int Width=491
int Height=49
boolean Enabled=false
string Text="Colonne &Selezionate:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_quick_select
int X=65
int Y=151
int Width=914
int Height=68
boolean Enabled=false
string Text="3.  (Opzionale) inserisci i criteri di selezione"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_quick_select
int X=65
int Y=39
int Width=754
int Height=68
boolean Enabled=false
string Text="1.  Seleziona una tabella."
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_tables from datawindow within w_quick_select
event clicked pbm_dwnlbuttonclk
event rbuttondown pbm_dwnrbuttondown
event rbuttonup pbm_rbuttonup
int X=57
int Y=337
int Width=964
int Height=481
int TabOrder=10
string DataObject="d_table_list"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;Integer		li_index, li_num_of_gobs
String		ls_name, ls_tname , ls_sql_syntax , ls_dw_syntax , ls_errors, ls_sql
String		dw_style , ls_hdgs[] , ls_cols[] , cols_no_underscores[]
String 		ls_emptyarray [], ls_col_name, ls_col_comment

/******************************************************************************************************
	Clear out DataObject name (if any), in case user has already selected a table, and
	now is selecting a different one
 ******************************************************************************************************/

dw_criteria.DataObject = ''
is_visiblecolumns[] = ls_emptyarray[]
/******************************************************************************************************
	Note that the Repository definitions for fonts prevail over the parameters specified
	in dw_style, below.  Thus, the several Modify commands after the Create, below.
******************************************************************************************************/
dw_style =	'style(Type=grid) ' + &
				'Text(font.face="MS Sans Serif"  font.Height=12  font.weight=400 font.family=2' + &
						'font.pitch=2 font.charset=0) ' + &
				'Column(font.face="MS Sans Serif"  font.Height=12  font.weight=400 font.family=2' + &
						'font.pitch=2 font.charset=0) ' 

SetPointer ( HourGlass! )
	
// Highlight this row
selectRow (0, False )			
selectRow (row, True )
il_HighlightedTable = Row

// Get table name
ls_tname = This.Object.tname[row]		
is_TableName = ls_tname
mle_comments.Text = '~tSto estraendo le colonne relative alla Tabella Selezionata ' + Lower ( ls_tname ) + '...'

dw_columns.SetRedraw ( False )

// Verifico il DBMS a cui sono connesso

choose case Upper(Left(sqlca.dbms,3) )

	case "ODB"		// ODBC
		ls_sql = "Select PBC_CNAM, PBC_CMNT " &
				+ "from PBCATCOL " &
				+ "where PBC_TNAM = '" + Upper(is_TableName) + &
				+ "' and pbc_edit <> 'es_cx" + &
				+ "' order by 1 ASC"

	case "ORA", "OR6", "OR7"	// ORACLE
		ls_sql = "Select PBC_CNAM, PBC_CMNT " &
				+ "from system.PBCATCOL " &
				+ "where PBC_TNAM = '" + Upper(is_TableName) &
				+ "' and pbc_edit <> 'es_cx" + &
				+ "' order by 1 ASC"
				
	case "O72"	// ORACLE 7.2
		ls_sql = "Select CNAME " &
				+ "from sys.COL " &
				+ "where TNAME = '" + Upper(is_TableName) &
				+ "' and pbc_edit <> 'es_cx" + &
				+ "' order by 1 ASC"

end choose

dw_columns.SetSQLSelect(ls_sql)

dw_columns.Retrieve()

//	Build generic SELECT statement and generate DataWindow syntax
ls_sql_syntax = 'SELECT '

declare cu_cols dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_cols;
do while true
	fetch cu_cols into :ls_col_name, :ls_col_comment;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ls_sql_syntax = ls_sql_syntax + ls_col_name + ", "
loop
close cu_cols;
ls_sql_syntax = trim (ls_sql_syntax)
ls_sql_syntax = left(ls_sql_syntax, len(ls_sql_syntax)-1) //levo l'ultima virgola
ls_sql_syntax = ls_sql_syntax + ' FROM ' + ls_tname

ls_dw_syntax = SQLCA.SyntaxFromSQL( ls_sql_syntax, dw_style, ls_errors)

// If no ls_errors (there shouldn't be any), create the Criteria grid-style DataWindow
If Len(ls_errors) = 0 Then
	Parent.SetRedraw ( False )			/*****  Prevent "flash"	*****/
	cb_addAll.enabled = True

	If dw_criteria.Create (ls_dw_syntax) > 0 Then
		dw_criteria.Modify ('datawindow.Detail.Height=70 ')

		//	Get names of heading-text items into hdgs[ ] array, 
		// and names of column items into cols[ ] array.
		li_num_of_gobs= wf_parse_object(dw_criteria, ls_hdgs, "text", "header")
		li_num_of_gobs= wf_parse_object(dw_criteria, ls_cols, "column", "detail")

		//	For each column (and its heading), set font and other attributes.  Also, make
		//	each of them invisible for now.
 
		For li_index = 1 to li_num_of_gobs
			//	Substitute spaces for underscores in column names.
			cols_no_underscores[li_index] = wf_replace_underscores_with_spaces( ls_cols[li_index] )

			//	Build the Modify command string
			ls_name = 'datawindow.header.Height=70 ' + &
			ls_hdgs[li_index] + '.Text="***~tWordcap(~'' + cols_no_underscores[li_index] + '~' )" ' + &
			ls_hdgs[li_index] + '.Font.Face="MS Sans Serif" ' + &
			ls_hdgs[li_index] + '.Font.Height="60" ' + &
			ls_hdgs[li_index] + '.Height="60" ' + &
			ls_hdgs[li_index] + '.Font.Weight="400" ' + &
			ls_hdgs[li_index] + '.Font.Family="0" ' + &
			ls_hdgs[li_index] + '.Font.Pitch="2" ' + &
			ls_hdgs[li_index] + '.Font.Charset="0" ' + &
			ls_cols[li_index]  + '.y="4" ' + &
			ls_cols[li_index] + '.Font.Face="MS Sans Serif" ' + &
			ls_cols[li_index] + '.Font.Height="52" ' + &
			ls_cols[li_index] + '.Height="60" ' + &
			ls_cols[li_index] + '.Font.Weight="400" ' + &
			ls_cols[li_index] + '.Font.Family="0" ' + &
			ls_cols[li_index] + '.Font.Pitch="2" ' + &
			ls_cols[li_index] + '.Font.Charset="0" ' + &
			ls_cols[li_index] + '.Visible="0" ' + &
			ls_cols[li_index] + '.EditMask.Mask="" ' + &
			' '
			dw_criteria.Modify (ls_name)
		Next

		For li_index = 1 to 6						
			dw_criteria.InsertRow ( 0 )
		Next

	End If
End if
Parent.SetRedraw ( True )		//  Now, get the window ready to show criteria	

mle_comments.Text = ''
dw_criteria.setTransObject(sqlca)
dw_columns.SetRedraw ( True )

end event

event rbuttondown;/***********************************************************************
		Get the row that has been 'clicked' and take the value out of 
		pbt_cmnt column for comments.  If there is no value in this column
		then display the default message - 'There are no comments for this
		table."
************************************************************************/
	setpointer(hourglass!)

	string 	ls_objectatpointer, ls_strrow, ls_comments

	ls_objectatpointer = this.GetObjectAtPointer() 

	If ls_objectatpointer = "" then return   // all done white space clicked

	ls_strrow = Mid(ls_objectatpointer, Pos(ls_objectatpointer, "~t") + 1, 2)
//**********************************************************************
//		Comments for the Table DataWindow are in column two pbt_cmnt
//**********************************************************************
	ls_comments = this.Object.pbt_cmnt[ integer(ls_strrow) ]

	this.setrowfocusindicator(FocusRect!)
	this.setrow(integer(ls_strrow))
	
	If Trim(ls_comments) = "" or IsNull(ls_comments)  then
		mle_comments.text = "This table has no comments."
	else
		mle_comments.text = ls_comments
	end if

	this.setredraw(TRUE)
end event

on rbuttonup;/*************************************************************************
		When the Minor mouse button is released:
			Remove the FocusIndicator
			Reset the current row to the proper table
			Force the repaint of the datawindow
			Clear out the comments field
**************************************************************************/

setrowfocusindicator(Off!)
setrow(il_HighLightedTable)
setredraw(TRUE)
mle_comments.text = ""

end on

type cb_addall from commandbutton within w_quick_select
int X=801
int Y=1601
int Width=363
int Height=81
int TabOrder=60
boolean Enabled=false
string Text="A&ggiungi Tutte"
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;/************************************************************************
	If the current column was previously selected, Skip it.
	If the Current column was not previoustly selected, selected it and 
		make it visible in the criteria grid.
*************************************************************************/
int			li_current_visibility, li_new_visibility
int			li_DWColumnsRows
string		ls_name, ls_colname, ls_moderr
long		ll_row

SetPointer(HourGlass!)

li_DWColumnsRows = dw_columns.RowCount()

For ll_row = 1 To li_DWColumnsRows

	il_HighLightedColumn = ll_row
	ls_colname = dw_columns.Object.cname[ll_row]		/*****  Get name of column that was clicked	*****/
	ls_name = ls_colname + '.visible'

	/************************************************************************
		Find out if this column had already been chosen or not, by seeing 
		if it is presently visible in the Criteria grid.
	*************************************************************************/
	li_current_visibility = Integer ( dw_criteria.Describe ( ls_name ) )

	/************************************************************************
		Set New Visibility to the opposite of Current Visibility
	*************************************************************************/
	li_new_visibility = 1 - li_current_visibility

	/************************************************************************
		Select or deselect this column's name in the column list.
	*************************************************************************/
	If li_new_visibility = 1 Then
		dw_columns.SelectRow ( ll_row, TRUE )
		il_HighLightedColumn = ll_row
		if cb_ok.enabled = FALSE then cb_ok.enabled = TRUE
		is_VisibleColumns[UpperBound(is_VisibleColumns) + 1 ] = ls_colname
	End If

	/************************************************************************
		Make this column visible if it is not already in the Criteria grid.
	*************************************************************************/
	if li_new_visibility > 0 then
		ls_name = ls_colname + '.visible="' + String ( li_new_visibility ) + '"'
	end if
	ls_moderr = dw_criteria.Modify ( ls_name )
	//dw_criteria.Object.datawindow.QuerySort = 'YES'
	dw_criteria.Modify("DataWindow.QuerySort=yes")	
next

end event

type st_16 from statictext within w_quick_select
int X=82
int Y=1453
int Width=193
int Height=68
boolean Enabled=false
string Text="Oppure:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_15 from statictext within w_quick_select
int X=82
int Y=1380
int Width=193
int Height=71
boolean Enabled=false
string Text="Oppure:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_14 from statictext within w_quick_select
int X=82
int Y=1313
int Width=193
int Height=77
boolean Enabled=false
string Text="Oppure:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_13 from statictext within w_quick_select
int X=82
int Y=1249
int Width=193
int Height=68
boolean Enabled=false
string Text="Criterio:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_12 from statictext within w_quick_select
int X=82
int Y=1175
int Width=193
int Height=68
boolean Enabled=false
string Text="Ordina:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type mle_comments from multilineedit within w_quick_select
int X=54
int Y=884
int Width=1970
int Height=116
BorderStyle BorderStyle=StyleLowered!
boolean AutoVScroll=true
boolean DisplayOnly=true
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_10 from statictext within w_quick_select
int X=54
int Y=820
int Width=289
int Height=61
boolean Enabled=false
string Text="Commenti:"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_columns from datawindow within w_quick_select
event rbuttonup pbm_rbuttonup
int X=1049
int Y=337
int Width=964
int Height=481
int TabOrder=20
string DataObject="d_column_list"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on rbuttonup;/*************************************************************************
		When the Minor mouse button is released:
			Remove the FocusIndicator
			Reset the current row to the proper table
			Force the repaint of the datawindow to remove the focus indicator
			Clear out the comments field
**************************************************************************/
SetRowFocusIndicator(Off!)
setrow(il_HighLightedColumn)

setredraw(TRUE)
mle_comments.text = ""

end on

event clicked;/************************************************************************
	If the clicked column was previously selected, deselect it and make it 
		invisible in the criteria grid.
	If the clicked column was previoustly not selected, selected it and 
		make it visible in the criteria grid.
*************************************************************************/

Long		ll_ReturnCode
Integer	li_current_visibility, li_new_visibility, li_Index, li_ArrayBound
String	ls_name, ls_colname, ls_moderr

il_HighLightedColumn = Row
ls_colname = This.Object.cname[row]		/*****  Get name of column that was clicked	*****/
ls_name = ls_colname + '.visible'

/************************************************************************
	Find out If this column had already been chosen or not, by seeing 
	If it is presently visible in the Criteria grid.
*************************************************************************/
li_current_visibility = Integer ( dw_criteria.Describe ( ls_name ) )

/************************************************************************
	Set New Visibility to the opposite of Current Visibility
*************************************************************************/
li_new_visibility = 1 - li_current_visibility

/************************************************************************
	Select or deselect this column's name in the column list.
*************************************************************************/
If li_new_visibility = 1 Then
	selectRow ( Row , True )
	il_HighLightedColumn = Row
	If cb_ok.enabled = False Then cb_ok.enabled = True
	is_VisibleColumns[UpperBound(is_VisibleColumns) + 1 ] = ls_colname
Else
	selectRow ( row, False )
	il_HighLightedColumn = 0
	ll_returncode = GetSelectedRow(0)
	If ll_returncode < 1 Then cb_ok.enabled = False
	li_ArrayBound = UpperBound(is_VisibleColumns)
	For li_Index = 1 to li_ArrayBound
		If is_VisibleColumns[li_Index] = ls_colname Then
			is_VisibleColumns[li_Index] = ""
		End If 
	Next
End If

/************************************************************************
	Make this column visible or invisible in the Criteria grid.
*************************************************************************/
ls_name = ls_colname + '.visible="' + String ( li_new_visibility ) + '"'

ls_moderr = dw_criteria.Modify ( ls_name )
//dw_criteria.Object.DataWindow.QuerySort = 'YES'
dw_criteria.Modify("DataWindow.QuerySort=yes")	

end event

event rbuttondown;/***********************************************************************
		Get the row that has been 'clicked' and take the value out of 
		pbc_cmnt column for comments.  If there is no value in this column
		then display the default message - 'TThis column has no comments"

************************************************************************/

	integer lCurrentRow
	string 	lObjectAtPointer, lStrRow, lComments

	lObjectAtPointer = this.GetObjectAtPointer()

	If lObjectAtPointer = "" then return   // all done white space clicked

	lStrRow = Mid(lObjectATPointer, Pos(lObjectAtPointer, "~t") + 1, 2)
//**********************************************************************
//		Comments for the Column DataWindow are in Column 3 pbcatcol pbc_cmnt
//**********************************************************************
	lComments = this.Object.pbc_cmnt[integer(lStrRow)]

	this.setrowfocusindicator(FocusRect!)
	this.setrow(integer(lStrRow))
	
	If Trim(lComments) = "" or IsNull(lComments)  then
		mle_comments.text = "This column has no comments."
	else
		mle_comments.text = lComments
	end if

	this.setredraw(TRUE)
end event

type st_9 from statictext within w_quick_select
int X=1049
int Y=279
int Width=274
int Height=68
boolean Enabled=false
string Text="&Colonne:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_8 from statictext within w_quick_select
int X=65
int Y=279
int Width=235
int Height=68
boolean Enabled=false
string Text="&Tabelle:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_cancel from commandbutton within w_quick_select
int X=409
int Y=1601
int Width=363
int Height=81
int TabOrder=50
string Text="&Annulla"
boolean Cancel=true
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;// Clicked script for cb_cancel

s_cs_xx.parametri.parametro_s_1 = ""
Close (Parent)

end event

type cb_ok from commandbutton within w_quick_select
int X=22
int Y=1601
int Width=363
int Height=81
int TabOrder=40
boolean Enabled=false
string Text="&OK"
boolean Default=true
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;/*************************************************************************
		When this button is clicked open up the window to test the
		DataWindow.  Open a new sheet in the frame to create
		the datawindow in.
*************************************************************************/

SetPointer(HourGlass!)

int	li_rc
li_rc = dw_criteria.accepttext( )

If li_rc = 1 Then
	s_cs_xx.parametri.parametro_s_1 = wf_create_dw()
End If

Close (Parent)
end event

type st_7 from statictext within w_quick_select
int X=1049
int Y=151
int Width=843
int Height=68
boolean Enabled=false
string Text="del mouse ed eventualmente"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_quick_select
int X=1049
int Y=97
int Width=843
int Height=68
boolean Enabled=false
string Text="tabelle o colonne, premi il tasto destro"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_quick_select
int X=1049
int Y=39
int Width=843
int Height=68
boolean Enabled=false
string Text="Per visualizzare i commenti di"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_quick_select
int X=65
int Y=212
int Width=897
int Height=68
boolean Enabled=false
string Text="nel box ~"Colonne Selezionate~""
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_quick_select
int X=65
int Y=100
int Width=1206
int Height=68
boolean Enabled=false
string Text="2.  Seleziona una o più colonne."
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_quick_select
int X=22
int Width=2034
int Height=1021
BorderStyle BorderStyle=StyleLowered!
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_criteria from datawindow within w_quick_select
int X=296
int Y=1101
int Width=1750
int Height=468
int TabOrder=30
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean LiveScroll=true
end type

