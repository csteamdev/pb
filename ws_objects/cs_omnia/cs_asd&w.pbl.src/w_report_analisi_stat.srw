﻿$PBExportHeader$w_report_analisi_stat.srw
$PBExportComments$Report Analisi Statistica
forward
global type w_report_analisi_stat from w_cs_xx_principale
end type
type cb_report from commandbutton within w_report_analisi_stat
end type
type dw_report_analisi_stat from uo_cs_xx_dw within w_report_analisi_stat
end type
end forward

global type w_report_analisi_stat from w_cs_xx_principale
integer width = 2898
integer height = 1908
string title = "Report Materiali Accettati (Zoom 80%)"
cb_report cb_report
dw_report_analisi_stat dw_report_analisi_stat
end type
global w_report_analisi_stat w_report_analisi_stat

event pc_setwindow;call super::pc_setwindow;dw_report_analisi_stat.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_analisi_stat.set_dw_options(sqlca, &
                                   pcca.null_object, &
                                   c_nomodify + &
                                   c_nodelete + &
                                   c_newonopen + &
                                   c_disableCC, &
                                   c_noresizedw + &
                                   c_nohighlightselected + &
                                   c_nocursorrowpointer +&
                                   c_nocursorrowfocusrect )
iuo_dw_main = dw_report_analisi_stat

end event

on w_report_analisi_stat.create
int iCurrent
call super::create
this.cb_report=create cb_report
this.dw_report_analisi_stat=create dw_report_analisi_stat
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report
this.Control[iCurrent+2]=this.dw_report_analisi_stat
end on

on w_report_analisi_stat.destroy
call super::destroy
destroy(this.cb_report)
destroy(this.dw_report_analisi_stat)
end on

event open;call super::open;integer li_anno_registrazione, li_num_registrazione, li_prog_riga_analisi, li_cur_row

li_cur_row = w_det_analisi_stat.dw_det_analisi_stat_lista.getrow()
li_anno_registrazione = w_det_analisi_stat.dw_det_analisi_stat_lista.getitemnumber( li_cur_row, "anno_registrazione" )
li_num_registrazione = w_det_analisi_stat.dw_det_analisi_stat_lista.getitemnumber( li_cur_row, "num_registrazione" )
li_prog_riga_analisi = w_det_analisi_stat.dw_det_analisi_stat_lista.getitemnumber( li_cur_row, "prog_riga_analisi" )

dw_report_analisi_stat.retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, li_num_registrazione, li_prog_riga_analisi )
dw_report_analisi_stat.Object.DataWindow.Zoom = 80
end event

event close;call super::close;pcca.mdi_frame.hide()
w_blob_analisi_stat.show()

end event

type cb_report from commandbutton within w_report_analisi_stat
integer x = 2469
integer y = 1700
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;integer li_job
dw_report_analisi_stat.Object.DataWindow.Zoom = 100
dw_report_analisi_stat.Print()
choose case s_cs_xx.parametri.parametro_s_1
	case "Chart"
		w_blob_analisi_stat.ole_chart.object.PrintChart()
	case "Book"
		w_blob_analisi_stat.ole_book.object.FilePrint(False)
end choose
close(parent)
end event

type dw_report_analisi_stat from uo_cs_xx_dw within w_report_analisi_stat
integer x = 23
integer y = 20
integer width = 2811
integer height = 1660
integer taborder = 10
string dataobject = "d_report_analisi_stat"
boolean vscrollbar = true
end type

