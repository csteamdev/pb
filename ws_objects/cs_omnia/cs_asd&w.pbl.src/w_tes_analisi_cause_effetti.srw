﻿$PBExportHeader$w_tes_analisi_cause_effetti.srw
$PBExportComments$Finestra Testata Analisi Cause Effetti
forward
global type w_tes_analisi_cause_effetti from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_tes_analisi_cause_effetti
end type
type cb_2 from commandbutton within w_tes_analisi_cause_effetti
end type
type dw_analisi_causa_eff_lista from uo_cs_xx_dw within w_tes_analisi_cause_effetti
end type
type dw_analisi_causa_eff_dettaglio from uo_cs_xx_dw within w_tes_analisi_cause_effetti
end type
end forward

global type w_tes_analisi_cause_effetti from w_cs_xx_principale
integer width = 2281
integer height = 1380
string title = "Testata Analisi Cause / Effetti"
cb_1 cb_1
cb_2 cb_2
dw_analisi_causa_eff_lista dw_analisi_causa_eff_lista
dw_analisi_causa_eff_dettaglio dw_analisi_causa_eff_dettaglio
end type
global w_tes_analisi_cause_effetti w_tes_analisi_cause_effetti

type variables
boolean ib_new = false
end variables

forward prototypes
public function integer wf_cancella_figli (integer ai_codice)
end prototypes

public function integer wf_cancella_figli (integer ai_codice);// ai_codice è il codice del padre delle righe da cencellare

integer li_cur_figlio, li_num_figlio, li_figli[], li_index

DECLARE cur_num_reg_cau_eff CURSOR FOR  
  SELECT "tab_analisi_cause_effetti"."num_reg_causa_effetto"  
    FROM "tab_analisi_cause_effetti"
 	 WHERE "tab_analisi_cause_effetti"."num_reg_causa_eff_padre" = :ai_codice and
		    "tab_analisi_cause_effetti"."cod_azienda" = :s_cs_xx.cod_azienda;

OPEN cur_num_reg_cau_eff;

FETCH cur_num_reg_cau_eff INTO :li_cur_figlio;

DO WHILE SQLCA.sqlcode = 0
	li_num_figlio = UpperBound(li_figli) + 1
	li_figli[li_num_figlio] = li_cur_figlio
	FETCH cur_num_reg_cau_eff INTO :li_cur_figlio;
LOOP

CLOSE cur_num_reg_cau_eff;

for li_index = 1 to UpperBound(li_figli)
	wf_cancella_figli(li_figli[li_index])

	DELETE FROM "tab_analisi_cause_effetti"  
		 WHERE "tab_analisi_cause_effetti"."num_reg_causa_eff_padre" = :ai_codice;
	next

return UpperBound(li_figli)
end function

on w_tes_analisi_cause_effetti.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_analisi_causa_eff_lista=create dw_analisi_causa_eff_lista
this.dw_analisi_causa_eff_dettaglio=create dw_analisi_causa_eff_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.dw_analisi_causa_eff_lista
this.Control[iCurrent+4]=this.dw_analisi_causa_eff_dettaglio
end on

on w_tes_analisi_cause_effetti.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_analisi_causa_eff_lista)
destroy(this.dw_analisi_causa_eff_dettaglio)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_analisi_causa_eff_dettaglio,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;dw_analisi_causa_eff_lista.set_dw_key("cod_azienda")
dw_analisi_causa_eff_lista.set_dw_key("num_reg_causa_effetto")
dw_analisi_causa_eff_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_analisi_causa_eff_dettaglio.set_dw_options(sqlca,dw_analisi_causa_eff_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_analisi_causa_eff_lista
end event

event pc_modify;call super::pc_modify;cb_1.enabled = False
cb_2.enabled = False
end event

event pc_new;call super::pc_new;cb_1.enabled = False
cb_2.enabled = False
dw_analisi_causa_eff_dettaglio.SetItem (dw_analisi_causa_eff_dettaglio.GetRow(), "data_creazione", datetime(today()) )
end event

event pc_save;call super::pc_save;cb_1.enabled = True
cb_2.enabled = True
end event

event pc_view;call super::pc_view;cb_1.enabled = True
cb_2.enabled = True
end event

type cb_1 from commandbutton within w_tes_analisi_cause_effetti
event clicked pbm_bnclicked
integer x = 1851
integer y = 20
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;if dw_analisi_causa_eff_lista.RowCount() > 0 and dw_analisi_causa_eff_lista.GetItemNumber(dw_analisi_causa_eff_lista.GetRow(),"num_reg_causa_effetto") <> 0 then
	s_cs_xx.parametri.parametro_i_1 = dw_analisi_causa_eff_lista.GetItemNumber(dw_analisi_causa_eff_lista.GetRow(),"num_reg_causa_effetto")
	s_cs_xx.parametri.parametro_s_1 = dw_analisi_causa_eff_lista.GetItemString(dw_analisi_causa_eff_lista.GetRow(),"des_causa_effetto")
	window_open_parm( w_det_analisi_cause_effetti, -1, dw_analisi_causa_eff_lista )
else
	g_mb.messagebox( "OMNIA", "Non c'è alcuna Analisi da Dettagliare!" )
end if	

end event

type cb_2 from commandbutton within w_tes_analisi_cause_effetti
event clicked pbm_bnclicked
integer x = 1851
integer y = 120
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Do&cumento"
end type

event clicked;long ll_num_registrazione
string ls_cod_blob, ls_db
blob lbl_null
integer li_risposta
transaction sqlcb

setnull(lbl_null)

ll_num_registrazione = dw_analisi_causa_eff_lista.getitemnumber(dw_analisi_causa_eff_lista.getrow(), "num_reg_causa_effetto")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tab_analisi_cause_effetti
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  num_reg_causa_effetto = :ll_num_registrazione
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if	
	
	destroy sqlcb;
	
else
	
	selectblob note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tab_analisi_cause_effetti
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  num_reg_causa_effetto = :ll_num_registrazione;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)

   	updateblob tab_analisi_cause_effetti
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda
		and		  num_reg_causa_effetto = :ll_num_registrazione
		using      sqlcb;		
		
		destroy sqlcb;
		
	else

   	updateblob tab_analisi_cause_effetti
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda
		and		  num_reg_causa_effetto = :ll_num_registrazione;
	
	end if
	
// fine modifiche	
	
	commit;

end if

setpointer(arrow!)
end event

type dw_analisi_causa_eff_lista from uo_cs_xx_dw within w_tes_analisi_cause_effetti
integer x = 23
integer y = 20
integer width = 1806
integer height = 500
integer taborder = 10
string dataobject = "d_analisi_causa_eff_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event updatestart;call super::updatestart;long ll_num_registrazione

if ib_new then
   select max(tab_analisi_cause_effetti.num_reg_causa_effetto)
     into :ll_num_registrazione
     from tab_analisi_cause_effetti
     where tab_analisi_cause_effetti.cod_azienda = :s_cs_xx.cod_azienda;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_causa_effetto", ll_num_registrazione)
   this.SetItem (this.GetRow ( ),"num_reg_causa_eff_padre", 0)
	
   ib_new = false
end if

if i_extendmode then
	
	integer  li_i, li_num_registrazione

	for li_i = 1 to this.deletedcount()
		li_num_registrazione= this.getitemnumber(li_i, "num_reg_causa_effetto", delete!, true)
		wf_cancella_figli(li_num_registrazione)
	next

end if
end event

event pcd_new;call super::pcd_new;ib_new = True
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "num_reg_causa_effetto")) THEN
      SetItem(l_Idx, "num_reg_causa_effetto", 1)
   END IF
NEXT

end event

type dw_analisi_causa_eff_dettaglio from uo_cs_xx_dw within w_tes_analisi_cause_effetti
integer x = 23
integer y = 540
integer width = 2194
integer height = 716
integer taborder = 20
string dataobject = "d_analisi_causa_eff_dettaglio"
borderstyle borderstyle = styleraised!
end type

