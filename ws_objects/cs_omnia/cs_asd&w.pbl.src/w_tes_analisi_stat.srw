﻿$PBExportHeader$w_tes_analisi_stat.srw
$PBExportComments$Finestra Testata Analisi Statistiche
forward
global type w_tes_analisi_stat from w_cs_xx_principale
end type
type dw_tes_analisi_stat_lista from uo_cs_xx_dw within w_tes_analisi_stat
end type
type cb_1 from commandbutton within w_tes_analisi_stat
end type
type cb_2 from commandbutton within w_tes_analisi_stat
end type
type dw_tes_analisi_stat_det_2 from uo_cs_xx_dw within w_tes_analisi_stat
end type
type dw_tes_analisi_stat_det_1 from uo_cs_xx_dw within w_tes_analisi_stat
end type
type dw_folder from u_folder within w_tes_analisi_stat
end type
end forward

global type w_tes_analisi_stat from w_cs_xx_principale
integer width = 2341
integer height = 1676
string title = "Analisi Statistica"
dw_tes_analisi_stat_lista dw_tes_analisi_stat_lista
cb_1 cb_1
cb_2 cb_2
dw_tes_analisi_stat_det_2 dw_tes_analisi_stat_det_2
dw_tes_analisi_stat_det_1 dw_tes_analisi_stat_det_1
dw_folder dw_folder
end type
global w_tes_analisi_stat w_tes_analisi_stat

type variables
boolean ib_new=False
end variables

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_tes_analisi_stat_det_1
dw_folder.fu_AssignTab(1, "&Tabelle", l_Objects[])

l_objects[1] = dw_tes_analisi_stat_det_2
dw_folder.fu_AssignTab(2, "&Anagrafiche", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_tes_analisi_stat_lista.set_dw_key("cod_azienda")
dw_tes_analisi_stat_lista.set_dw_key("anno_registrazione")
dw_tes_analisi_stat_lista.set_dw_key("num_registrazione")
dw_tes_analisi_stat_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_analisi_stat_det_1.set_dw_options(sqlca,dw_tes_analisi_stat_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_analisi_stat_det_2.set_dw_options(sqlca,dw_tes_analisi_stat_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_tes_analisi_stat_lista

dw_folder.fu_SelectTab(1)

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_1,"cod_attrezzatura",sqlca,&
					  "anag_attrezzature","cod_attrezzatura","descrizione",&
					  "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_1,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_reparti.flag_blocco <> 'S') or (anag_reparti.flag_blocco = 'S' and anag_reparti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_1,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "anag_divisioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_2,"cod_cliente",sqlca,&
                 "anag_clienti","cod_cliente","rag_soc_1",&
                 "(anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_clienti.flag_blocco <> 'S') or (anag_clienti.flag_blocco = 'S' and anag_clienti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_2,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1",&
                 "(anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_fornitori.flag_blocco <> 'S') or (anag_fornitori.flag_blocco = 'S' and anag_fornitori.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_tes_analisi_stat_det_2,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "(anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_prodotti.flag_blocco <> 'S') or (anag_prodotti.flag_blocco = 'S' and anag_prodotti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")


end event

on w_tes_analisi_stat.create
int iCurrent
call super::create
this.dw_tes_analisi_stat_lista=create dw_tes_analisi_stat_lista
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_tes_analisi_stat_det_2=create dw_tes_analisi_stat_det_2
this.dw_tes_analisi_stat_det_1=create dw_tes_analisi_stat_det_1
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_analisi_stat_lista
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.dw_tes_analisi_stat_det_2
this.Control[iCurrent+5]=this.dw_tes_analisi_stat_det_1
this.Control[iCurrent+6]=this.dw_folder
end on

on w_tes_analisi_stat.destroy
call super::destroy
destroy(this.dw_tes_analisi_stat_lista)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_tes_analisi_stat_det_2)
destroy(this.dw_tes_analisi_stat_det_1)
destroy(this.dw_folder)
end on

event pc_new;call super::pc_new;cb_1.enabled = False
cb_2.enabled = False
end event

event pc_modify;call super::pc_modify;cb_1.enabled = False
cb_2.enabled = False
end event

event pc_view;call super::pc_view;if dw_tes_analisi_stat_lista.RowCount() > 0 and dw_tes_analisi_stat_lista.GetItemNumber(dw_tes_analisi_stat_lista.GetRow(),"num_registrazione") <> 0 then
	cb_1.enabled = True
	cb_2.enabled = True
else
	cb_1.enabled = False
	cb_2.enabled = False
end if

end event

event pc_save;call super::pc_save;if dw_tes_analisi_stat_lista.RowCount() > 0 and dw_tes_analisi_stat_lista.GetItemNumber(dw_tes_analisi_stat_lista.GetRow(),"num_registrazione") <> 0 then
	cb_1.enabled = True
	cb_2.enabled = True
else
	cb_1.enabled = False
	cb_2.enabled = False
end if

end event

event pc_delete;call super::pc_delete;if not (dw_tes_analisi_stat_lista.RowCount() > 0 and dw_tes_analisi_stat_lista.GetItemNumber(dw_tes_analisi_stat_lista.GetRow(),"num_registrazione") <> 0) then
	cb_1.enabled = False
	cb_2.enabled = False
end if
end event

type dw_tes_analisi_stat_lista from uo_cs_xx_dw within w_tes_analisi_stat
integer x = 23
integer y = 20
integer width = 1874
integer height = 500
integer taborder = 10
string dataobject = "d_tes_analisi_stat_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if l_Error = 0 then
	cb_1.enabled = False
	cb_2.enabled = False
end if	
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "anno_registrazione")) THEN
      SetItem(l_Idx, "anno_registrazione", 1)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "num_registrazione")) THEN
      SetItem(l_Idx, "num_registrazione", 1)
   END IF
NEXT

end event

event updatestart;call super::updatestart;integer  li_i, li_anno_registrazione, li_num_registrazione
long ll_num_registrazione, ll_anno_registrazione

if i_extendmode then
	if ib_new  then
		ll_anno_registrazione = f_anno_esercizio()
		select max(tes_analisi_stat.num_registrazione)
		  into :ll_num_registrazione
		  from tes_analisi_stat
		  where (tes_analisi_stat.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_analisi_stat.anno_registrazione = :ll_anno_registrazione);
	
		if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
			ll_num_registrazione = 1
		else
			ll_num_registrazione = ll_num_registrazione + 1
		end if
		this.SetItem (this.GetRow ( ),"anno_registrazione", ll_anno_registrazione)
		this.SetItem (this.GetRow ( ),"num_registrazione", ll_num_registrazione)
		
		ib_new = false
	end if

	for li_i = 1 to this.deletedcount()
	   li_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
	   li_num_registrazione= this.getitemnumber(li_i, "num_registrazione", delete!, true)

	   delete from det_analisi_stat
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	anno_registrazione=:li_anno_registrazione
		and 	num_registrazione=:li_num_registrazione;

	next

end if

end event

event pcd_new;call super::pcd_new;dw_tes_analisi_stat_det_2.object.b_ricerca_cliente.enabled = true
dw_tes_analisi_stat_det_2.object.b_ricerca_prodotto.enabled = true
dw_tes_analisi_stat_det_2.object.b_ricerca_fornitore.enabled = true
ib_new = true

end event

event pcd_modify;call super::pcd_modify;dw_tes_analisi_stat_det_2.object.b_ricerca_prodotto.enabled = true
dw_tes_analisi_stat_det_2.object.b_ricerca_cliente.enabled = true
dw_tes_analisi_stat_det_2.object.b_ricerca_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;dw_tes_analisi_stat_det_2.object.b_ricerca_prodotto.enabled = false
dw_tes_analisi_stat_det_2.object.b_ricerca_cliente.enabled = false
dw_tes_analisi_stat_det_2.object.b_ricerca_fornitore.enabled = false

end event

type cb_1 from commandbutton within w_tes_analisi_stat
integer x = 1920
integer y = 20
integer width = 361
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;window_open_parm( w_det_analisi_stat, -1, dw_tes_analisi_stat_lista )

end event

type cb_2 from commandbutton within w_tes_analisi_stat
event clicked pbm_bnclicked
integer x = 1920
integer y = 120
integer width = 361
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Do&cumento"
end type

event clicked;long ll_num_registrazione, ll_anno_registrazione
string ls_cod_blob, ls_db
blob lbl_null
integer li_risposta
transaction sqlcb

setnull(lbl_null)

ll_anno_registrazione = dw_tes_analisi_stat_lista.getitemnumber(dw_tes_analisi_stat_lista.getrow(), "anno_registrazione") 
ll_num_registrazione = dw_tes_analisi_stat_lista.getitemnumber(dw_tes_analisi_stat_lista.getrow(), "num_registrazione")  

s_cs_xx.parametri.parametro_bl_1 = lbl_null

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tes_analisi_stat
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  anno_registrazione = :ll_anno_registrazione
	and		  num_registrazione = :ll_num_registrazione
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if	
	
	destroy sqlcb;
	
else

	selectblob note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tes_analisi_stat
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  anno_registrazione = :ll_anno_registrazione
	and		  num_registrazione = :ll_num_registrazione;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob tes_analisi_stat
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda
		and		  anno_registrazione = :ll_anno_registrazione
		and		  num_registrazione = :ll_num_registrazione
		using      sqlcb;		
		
		destroy sqlcb;
		
	else
		
	   updateblob tes_analisi_stat
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda
		and		  anno_registrazione = :ll_anno_registrazione
		and		  num_registrazione = :ll_num_registrazione;
		
	end if
	
// fine modifiche	
	
   commit;

end if

setpointer(arrow!)
end event

type dw_tes_analisi_stat_det_2 from uo_cs_xx_dw within w_tes_analisi_stat
integer x = 41
integer y = 640
integer width = 2217
integer height = 900
integer taborder = 30
string dataobject = "d_tes_analisi_stat_det_2"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_analisi_stat_det_2,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tes_analisi_stat_det_2,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_analisi_stat_det_2,"cod_fornitore")
end choose
end event

type dw_tes_analisi_stat_det_1 from uo_cs_xx_dw within w_tes_analisi_stat
integer x = 41
integer y = 640
integer width = 2217
integer height = 900
integer taborder = 20
string dataobject = "d_tes_analisi_stat_det_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

	string ls_cod_divisione
	
	if i_colname = "cod_area_aziendale" then
	   select tab_aree_aziendali.cod_divisione
	   into :ls_cod_divisione
	   from tab_aree_aziendali
	   where (tab_aree_aziendali.cod_azienda = :s_cs_xx.cod_azienda) and
	         (tab_aree_aziendali.cod_area_aziendale = :i_coltext);
	   if sqlca.sqlcode = 0 then
	      if isnull(ls_cod_divisione) then  g_mb.messagebox("Testata Analisi Statistica", "Non hai indicato nessuna area di apparteneza nell'area "+i_coltext, StopSign!)
	      this.setitem(this.getrow(), "cod_divisione", ls_cod_divisione)
	   end if
	end if
end if
end event

type dw_folder from u_folder within w_tes_analisi_stat
integer x = 23
integer y = 540
integer width = 2263
integer height = 1020
integer taborder = 0
boolean border = false
end type

