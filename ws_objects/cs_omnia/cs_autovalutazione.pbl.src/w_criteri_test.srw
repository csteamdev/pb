﻿$PBExportHeader$w_criteri_test.srw
forward
global type w_criteri_test from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_criteri_test
end type
type dw_criteri_lista from uo_cs_xx_dw within w_criteri_test
end type
type r_1 from rectangle within w_criteri_test
end type
type r_2 from rectangle within w_criteri_test
end type
type dw_criteri_det from uo_cs_xx_dw within w_criteri_test
end type
end forward

global type w_criteri_test from w_cs_xx_principale
integer width = 2277
integer height = 1656
string title = "Definizione Criteri di Autovalutazione"
cb_1 cb_1
dw_criteri_lista dw_criteri_lista
r_1 r_1
r_2 r_2
dw_criteri_det dw_criteri_det
end type
global w_criteri_test w_criteri_test

on w_criteri_test.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_criteri_lista=create dw_criteri_lista
this.r_1=create r_1
this.r_2=create r_2
this.dw_criteri_det=create dw_criteri_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_criteri_lista
this.Control[iCurrent+3]=this.r_1
this.Control[iCurrent+4]=this.r_2
this.Control[iCurrent+5]=this.dw_criteri_det
end on

on w_criteri_test.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_criteri_lista)
destroy(this.r_1)
destroy(this.r_2)
destroy(this.dw_criteri_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_criteri_lista.set_dw_key("cod_azienda")
dw_criteri_lista.set_dw_key("cod_test")
dw_criteri_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default) 
                                    
dw_criteri_det.set_dw_options(sqlca, &
                                    dw_criteri_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
iuo_dw_main = dw_criteri_lista




end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_criteri_det,"cod_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo",&
                 "tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
end event

type cb_1 from commandbutton within w_criteri_test
integer x = 1829
integer y = 1460
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sottocriteri"
end type

event clicked;window_open_parm(w_sottocriteri, -1, dw_criteri_lista)
end event

type dw_criteri_lista from uo_cs_xx_dw within w_criteri_test
integer x = 23
integer y = 20
integer width = 2171
integer height = 500
integer taborder = 10
string dataobject = "d_criteri_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_num_edizione, ll_num_versione
string ls_cod_tipo_trattativa, ls_cod_azienda 
ls_cod_azienda = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_azienda")
ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_trattativa,ll_num_versione,ll_num_edizione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_tipo_trattativa

ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_test")) then
      this.setitem(ll_i, "cod_test", ls_cod_tipo_trattativa)
   end if
next
end event

event pcd_new;call super::pcd_new;string ls_cod_test
long ll_progressivo, ll_num_edizione, ll_num_versione

ls_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")


select max(progressivo)
into :ll_progressivo
from det_autovalutazione_criteri
where cod_azienda = :s_cs_xx.cod_azienda
  and cod_test = :ls_cod_test;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore lettura dettaglio test" + sqlca.sqlerrtext)
	return
end if

if isnull(ll_progressivo) then ll_progressivo = 0

ll_progressivo = ll_progressivo + 1
setitem(getrow(), "progressivo", ll_progressivo )
setitem(getrow(), "cod_test", ls_cod_test)
setitem(getrow(), "num_versione", ll_num_versione)
setitem(getrow(), "num_edizione", ll_num_edizione)




end event

type r_1 from rectangle within w_criteri_test
long linecolor = 8388608
integer linethickness = 1
long fillcolor = 16777215
integer x = 69
integer y = 680
integer width = 165
integer height = 144
end type

type r_2 from rectangle within w_criteri_test
long linecolor = 8388608
integer linethickness = 1
long fillcolor = 16777215
integer x = 137
integer y = 700
integer width = 165
integer height = 144
end type

type dw_criteri_det from uo_cs_xx_dw within w_criteri_test
integer x = 23
integer y = 520
integer width = 2194
integer height = 920
integer taborder = 20
string dataobject = "d_criteri_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_colname = "peso" then
	  if long(i_coltext) < 1 OR long(i_coltext) > 10 or long (i_coltext) = 0 then
		  g_mb.messagebox("Definizione criteri", "Inserire un valore compreso tra 1 e 10")
		  return 1;
	end if
end if	
end event

