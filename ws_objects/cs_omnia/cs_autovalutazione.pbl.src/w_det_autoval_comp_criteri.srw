﻿$PBExportHeader$w_det_autoval_comp_criteri.srw
forward
global type w_det_autoval_comp_criteri from w_cs_xx_principale
end type
type cb_4 from commandbutton within w_det_autoval_comp_criteri
end type
type cb_2 from commandbutton within w_det_autoval_comp_criteri
end type
type cb_3 from commandbutton within w_det_autoval_comp_criteri
end type
type cb_1 from commandbutton within w_det_autoval_comp_criteri
end type
type dw_det_autoval_comp_criteri_lista from uo_cs_xx_dw within w_det_autoval_comp_criteri
end type
end forward

global type w_det_autoval_comp_criteri from w_cs_xx_principale
integer width = 3762
integer height = 1240
string title = "Compilazione Criteri di Autovlutazione"
cb_4 cb_4
cb_2 cb_2
cb_3 cb_3
cb_1 cb_1
dw_det_autoval_comp_criteri_lista dw_det_autoval_comp_criteri_lista
end type
global w_det_autoval_comp_criteri w_det_autoval_comp_criteri

type variables
long il_anno_registrazione, il_num_registrazione, il_num_edizione, il_num_versione 
string il_cod_test

end variables

on w_det_autoval_comp_criteri.create
int iCurrent
call super::create
this.cb_4=create cb_4
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_1=create cb_1
this.dw_det_autoval_comp_criteri_lista=create dw_det_autoval_comp_criteri_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_4
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_3
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.dw_det_autoval_comp_criteri_lista
end on

on w_det_autoval_comp_criteri.destroy
call super::destroy
destroy(this.cb_4)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_1)
destroy(this.dw_det_autoval_comp_criteri_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_autoval_comp_criteri_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default) 
                                    

iuo_dw_main = dw_det_autoval_comp_criteri_lista

il_cod_test = dw_det_autoval_comp_criteri_lista.i_parentdw.getitemstring(dw_det_autoval_comp_criteri_lista.i_parentdw.i_selectedrows[1],"cod_test")
il_anno_registrazione = dw_det_autoval_comp_criteri_lista.i_parentdw.getitemnumber(dw_det_autoval_comp_criteri_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_registrazione =dw_det_autoval_comp_criteri_lista.i_parentdw.getitemnumber(dw_det_autoval_comp_criteri_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
il_num_edizione =dw_det_autoval_comp_criteri_lista.i_parentdw.getitemnumber(dw_det_autoval_comp_criteri_lista.i_parentdw.i_selectedrows[1], "num_edizione")
il_num_versione =dw_det_autoval_comp_criteri_lista.i_parentdw.getitemnumber(dw_det_autoval_comp_criteri_lista.i_parentdw.i_selectedrows[1], "num_versione")





end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_autoval_comp_criteri_lista,"cod_operatore",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
                 

end event

type cb_4 from commandbutton within w_det_autoval_comp_criteri
integer x = 3319
integer y = 1040
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sottocriteri"
end type

event clicked;window_open_parm(w_det_autoval_comp_sottocrit, -1, dw_det_autoval_comp_criteri_lista)
end event

type cb_2 from commandbutton within w_det_autoval_comp_criteri
integer x = 2907
integer y = 1040
integer width = 379
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Carica test"
end type

event clicked;  long ll_num_righe
  
  SELECT COUNT(*) 
  INTO   :ll_num_righe
  FROM   det_autoval_comp_criteri
  WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
         anno_registrazione = :il_anno_registrazione AND
			num_registrazione = :il_num_registrazione;
			if ll_num_righe = 0 then
			  
		
  INSERT INTO det_autoval_comp_criteri
              (cod_azienda,
				  anno_registrazione,
				  num_registrazione,
				  prog_riga_criterio,
				  des_criterio,
				  peso_criterio,
				  cod_paragrafo)
  SELECT 	  :s_cs_xx.cod_azienda,
  				  :il_anno_registrazione,
				  :il_num_registrazione,
				  progressivo,
				  des_criterio,
				  peso,
				  cod_paragrafo
	FROM det_autovalutazione_criteri
	
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND
			cod_test = :il_cod_test AND
			num_edizione = :il_num_edizione AND
			num_versione = :il_num_versione
	
	;
	
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore inserimento in det_autoval_comp_criteri: " + sqlca.sqlerrtext )
		rollback;
	else
		commit;
	end if
	/*caricamento sottocriteri */
	INSERT INTO det_autoval_comp_sottocrit
              (cod_azienda,
				  anno_registrazione,
				  num_registrazione,
				  prog_riga_criterio,
				  prog_riga_sottoscriterio,
				  des_sottocriterio,
				  peso_sottocriterio,
				  cod_paragrafo)
  SELECT 	  :s_cs_xx.cod_azienda,
  				  :il_anno_registrazione,
				  :il_num_registrazione,
				  progressivo,
				  prog_riga_sottocriterio,
				  des_sottocriterio,
				  peso,
				  cod_paragrafo
	FROM det_autovalutazione_sottocrit
	
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND
			cod_test = :il_cod_test AND
			num_edizione = :il_num_edizione AND
			num_versione = :il_num_versione
	
	;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore inserimento in det_autoval_comp_criteri: " + sqlca.sqlerrtext )
		rollback;
	else
		commit;
	end if

END IF
	
dw_det_autoval_comp_criteri_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")






			  
		
 
end event

type cb_3 from commandbutton within w_det_autoval_comp_criteri
integer x = 23
integer y = 1340
integer width = 393
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica test"
end type

type cb_1 from commandbutton within w_det_autoval_comp_criteri
integer x = 1600
integer y = 1340
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sottocriteri"
end type

type dw_det_autoval_comp_criteri_lista from uo_cs_xx_dw within w_det_autoval_comp_criteri
integer y = 20
integer width = 3707
integer height = 1000
integer taborder = 10
string dataobject = "d_det_autoval_comp_criteri_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore,ll_anno_registrazione,ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_registrazione")
ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
	 if isnull(this.getitemnumber(ll_i, "num_registrazione")) then
      this.setitem(ll_i, "anno_registrazione", ll_num_registrazione)
   end if
next
end event

event pcd_new;call super::pcd_new;long il_anno_registrazione, ll_num_registrazione 
string il_cod_test
il_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_test")
il_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")


setitem(getrow(), "anno_registrazione", il_anno_registrazione)
setitem(getrow(), "num_registrazione", il_num_registrazione)




end event

