﻿$PBExportHeader$w_det_autoval_comp_sottocrit.srw
forward
global type w_det_autoval_comp_sottocrit from w_cs_xx_principale
end type
type dw_autoval_comp_sottoscrit_lista from uo_cs_xx_dw within w_det_autoval_comp_sottocrit
end type
end forward

global type w_det_autoval_comp_sottocrit from w_cs_xx_principale
integer width = 3067
integer height = 1232
string title = "Compilazione Sottocriteri di Autovalutazione"
dw_autoval_comp_sottoscrit_lista dw_autoval_comp_sottoscrit_lista
end type
global w_det_autoval_comp_sottocrit w_det_autoval_comp_sottocrit

event pc_setwindow;call super::pc_setwindow;dw_autoval_comp_sottoscrit_lista.set_dw_key("cod_azienda")
dw_autoval_comp_sottoscrit_lista.set_dw_key("anno_registrazione")
dw_autoval_comp_sottoscrit_lista.set_dw_key("num_registrazione")
dw_autoval_comp_sottoscrit_lista.set_dw_key("prog_riga_criterio")

dw_autoval_comp_sottoscrit_lista.set_dw_options(sqlca, &
																i_openparm, &
																c_default + &
																c_scrollparent, &
																c_default)
end event

on w_det_autoval_comp_sottocrit.create
int iCurrent
call super::create
this.dw_autoval_comp_sottoscrit_lista=create dw_autoval_comp_sottoscrit_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_autoval_comp_sottoscrit_lista
end on

on w_det_autoval_comp_sottocrit.destroy
call super::destroy
destroy(this.dw_autoval_comp_sottoscrit_lista)
end on

type dw_autoval_comp_sottoscrit_lista from uo_cs_xx_dw within w_det_autoval_comp_sottocrit
integer x = 23
integer y = 20
integer width = 2994
integer height = 1100
integer taborder = 10
string dataobject = "d_autoval_comp_sottoscrit_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_registrazione")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_criterio")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga)

if ll_errore < 0 then
   pcca.error = c_fatal
end if



end event

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_anno_registrazione,ll_num_registrazione,ll_prog_riga

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(),"numo_registrazione")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.getrow(),"prog_riga_criterio")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
	 if isnull(this.getitemnumber(ll_i, "num_registrazione")) then
      this.setitem(ll_i, "anno_registrazione", ll_num_registrazione)
	end if
   if isnull(this.getitemnumber(ll_i, "prog_riga_criterio")) then
      this.setitem(ll_i, "prog_riga_criterio", ll_prog_riga)
   end if
  
next
end event

