﻿$PBExportHeader$w_grafico_autoval.srw
forward
global type w_grafico_autoval from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_grafico_autoval
end type
type cb_elabora from commandbutton within w_grafico_autoval
end type
type dw_grafico from uo_cs_xx_dw within w_grafico_autoval
end type
type dw_sel from u_dw_search within w_grafico_autoval
end type
end forward

global type w_grafico_autoval from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2578
integer height = 2012
string title = "Grafico Autovalutazione"
cb_annulla cb_annulla
cb_elabora cb_elabora
dw_grafico dw_grafico
dw_sel dw_sel
end type
global w_grafico_autoval w_grafico_autoval

forward prototypes
public function integer wf_elabora ()
end prototypes

public function integer wf_elabora ();datetime	ldt_inizio, ldt_fine

dec{4}		ld_punteggio, ld_media

long			ll_anno, ll_mese, ll_periodi, ll_durata, ll_i, ll_riga, ll_media, ll_prog_criterio

string		ls_periodo, ls_cod_area, ls_cod_test, ls_test_old, ls_area_old, ls_sc, ls_sql


dw_sel.accepttext()

ls_cod_test = dw_sel.getitemstring(1,"cod_test")

if isnull(ls_cod_test) then
	g_mb.messagebox("OMNIA","Selezionare un test!",exclamation!)
	return -1
end if

ls_cod_area = dw_sel.getitemstring(1,"cod_area")

ll_anno = dw_sel.getitemnumber(1,"anno_inizio")

if isnull(ll_anno) or ll_anno < 1000 or ll_anno > 2999 then
	g_mb.messagebox("OMNIA","Inserire un anno valido!",exclamation!)
	return -1
end if

ll_mese = dw_sel.getitemnumber(1,"mese_inizio")

if isnull(ll_mese) or ll_mese < 1 or ll_mese > 12 then
	g_mb.messagebox("OMNIA","Inserire un mese valido!",exclamation!)
	return -1
end if

ll_periodi = dw_sel.getitemnumber(1,"num_periodi")

if isnull(ll_periodi) or ll_periodi < 1 then
	g_mb.messagebox("OMNIA","Selezionare il numero di periodi!",exclamation!)
	return -1
end if

ll_durata = dw_sel.getitemnumber(1,"periodicita")

if isnull(ll_durata) or ll_durata < 1 then
	g_mb.messagebox("OMNIA","Selezionare la durata di ogni periodo!",exclamation!)
	return -1
end if

ls_sc = dw_sel.getitemstring(1,"suddividi_criteri")

dw_grafico.reset()

ldt_inizio = datetime(date("01/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)

ll_mese += (ll_periodi * ll_durata)
	
if ll_mese > 12 then
	ll_anno += int(ll_mese/12)
	ll_mese = mod(ll_mese,12)
end if

ldt_fine = datetime(date("01/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)

ldt_fine = datetime(relativedate(date(ldt_fine),-1),00:00:00)

declare check cursor for
select distinct
	num_edizione,
	num_versione
from
	tes_autoval_comp
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_test = :ls_cod_test and
	data_registrazione > :ldt_inizio and
	data_registrazione < :ldt_fine;

open check;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in open cursore check: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 0 and sqlca.sqlnrows > 1 then
	if g_mb.messagebox("OMNIA","ATTENZIONE: nel periodo specificato sono state utilizzate differenti versioni del test indicato.~nContinuare?",exclamation!,yesno!,2) = 2 then
		close check;
		return -1
	end if
end if

close check;

for ll_i  = 1 to ll_periodi
	
	ll_anno = year(date(ldt_inizio))
	
	ll_mese = month(date(ldt_inizio))
	
	ll_mese += (ll_durata)
	
	if ll_mese > 12 then
		ll_anno += int(ll_mese/12)
		ll_mese = mod(ll_mese,12)
	end if
	
	ldt_fine = datetime(date("01/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
	
	ldt_fine = datetime(relativedate(date(ldt_fine),-1),00:00:00)
	
	ls_periodo = string(date(ldt_inizio),"dd/mm/yy") + " - " + string(date(ldt_fine),"dd/mm/yy")
	
	if ls_sc = "N" then
		ls_sql = &
		"select distinct " + &
		"	cod_test " + &
		"from " + &
		"	tes_autovalutazione " + &
		"where " + &
		"	cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
		"	cod_test = '" + ls_cod_test + "'"
	else
		ls_sql = &
		"select distinct " + &
		"	progressivo " + &
		"from " + &
		"	det_autovalutazione_criteri " + &
		"where " + &
		"	cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"	cod_test = '" + ls_cod_test + "'"
	end if
	
	declare test dynamic cursor for sqlsa;
	
	prepare sqlsa from :ls_sql;
	
	open dynamic test;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in open cursore test: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	do while true
		
		if ls_sc = "N" then
			fetch
				test
			into
				:ls_cod_test;
		else
			fetch
				test
			into
				:ll_prog_criterio;
		end if	
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in fetch cursore test: " + sqlca.sqlerrtext,stopsign!)
			close test;
			return -1
		elseif sqlca.sqlcode = 100 then
			close test;
			exit
		end if
		
		ll_riga = dw_grafico.insertrow(0)
	
		dw_grafico.setitem(ll_riga,"periodo",ls_periodo)
		
		if ls_sc = "N" then
			dw_grafico.setitem(ll_riga,"test",ls_cod_test)
		else
			dw_grafico.setitem(ll_riga,"test",string(ll_prog_criterio))
		end if
		
		if ls_sc = "N" then
			ls_sql = &
			"select " + &
			"	cod_area_aziendale, " + &
			"	punteggio " + &
			"from " + &
			"	tes_autoval_comp " + &
			"where " + &
			"	cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			"	cod_test = '" + ls_cod_test + "' and " + &
			"	data_registrazione >= '" + string(ldt_inizio,s_cs_xx.db_funzioni.formato_data) + "'  and " + &
			"	data_registrazione <= '" + string(ldt_fine,s_cs_xx.db_funzioni.formato_data) + "' "
			if not isnull(ls_cod_area) then
				ls_sql += " and cod_area_aziendale = '" + ls_cod_area + "' "
			end if
			ls_sql += &
			"order by " + &
			"	cod_area_aziendale ASC, " + &
			"	data_registrazione DESC "
		else
			ls_sql = &
			"select " + &
			"	a.cod_area_aziendale, " + &
			"	b.punteggio_criterio " + &
			"from " + &
			"	tes_autoval_comp a, " + &
			"	det_autoval_comp_criteri b " + &
			"where " + &
			"	b.cod_azienda = a.cod_azienda and " + &
			"	b.anno_registrazione = a.anno_registrazione and " + &
			"	b.num_registrazione = a.num_registrazione and " + &
			"	a.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			"	cod_test = '" + ls_cod_test + "' and " + &
			"	b.prog_riga_criterio = " + string(ll_prog_criterio) + " and " + &
			"	data_registrazione >= '" + string(ldt_inizio,s_cs_xx.db_funzioni.formato_data) + "'  and " + &
			"	data_registrazione <= '" + string(ldt_fine,s_cs_xx.db_funzioni.formato_data) + "' "
			if not isnull(ls_cod_area) then
				ls_sql += " and cod_area_aziendale = '" + ls_cod_area + "' "
			end if
			ls_sql += &
			"order by " + &
			"	cod_area_aziendale ASC, " + &
			"	data_registrazione DESC "
		end if
		
		declare autoval dynamic cursor for sqlsa;
	
		prepare sqlsa from :ls_sql;
		
		open dynamic autoval;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in open cursore autoval: " + sqlca.sqlerrtext,stopsign!)
			close test;
			return -1
		end if
		
		setnull(ls_area_old)
		
		ll_media = 0
		
		ld_media = 0
		
		do while true
			
			fetch
				autoval
			into
				:ls_cod_area,
				:ld_punteggio;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in fetch cursore autoval: " + sqlca.sqlerrtext,stopsign!)
				close autoval;
				close test;
				return -1
			elseif sqlca.sqlcode = 100 then
				close autoval;
				exit
			end if
			
			if isnull(ld_punteggio) then
				ld_punteggio = 0
			end if
			
			if not isnull(ls_area_old) and ls_cod_area = ls_area_old then
				continue
			else
				ls_area_old = ls_cod_area
				ll_media ++
				ld_media += ld_punteggio
			end if
			
		loop
		
		if ll_media > 0 then
			ld_media = round((ld_media / ll_media),2)
		else
			ld_media = 0
		end if
		
		dw_grafico.setitem(ll_riga,"valore",ld_media)
		
	loop
	
	ldt_inizio = datetime(relativedate(date(ldt_fine),1),00:00:00)
	
next

return 0
end function

on w_grafico_autoval.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_elabora=create cb_elabora
this.dw_grafico=create dw_grafico
this.dw_sel=create dw_sel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_elabora
this.Control[iCurrent+3]=this.dw_grafico
this.Control[iCurrent+4]=this.dw_sel
end on

on w_grafico_autoval.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_elabora)
destroy(this.dw_grafico)
destroy(this.dw_sel)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_grafico

dw_grafico.change_dw_current()

dw_grafico.set_dw_options(sqlca, &
										pcca.null_object, &
										c_nonew + c_nomodify + c_nodelete + c_noretrieveonopen, &
										c_nohighlightselected + c_nocursorrowfocusrect)

dw_sel.insertrow(0)

cb_annulla.postevent("clicked")
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel,"cod_area",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel,"cod_test",sqlca,"tes_autovalutazione","cod_test","des_test","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_annulla from commandbutton within w_grafico_autoval
integer x = 2149
integer y = 200
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string ls_null


setnull(ls_null)

dw_sel.setitem(1,"cod_area",ls_null)

dw_sel.setitem(1,"cod_test",ls_null)

dw_sel.setitem(1,"anno_inizio",f_anno_esercizio())

dw_sel.setitem(1,"mese_inizio",1)

dw_sel.setitem(1,"num_periodi",1)

dw_sel.setitem(1,"periodicita",1)

dw_sel.setitem(1,"suddividi_criteri","N")
end event

type cb_elabora from commandbutton within w_grafico_autoval
integer x = 2149
integer y = 300
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;dw_grafico.change_dw_current()

parent.postevent("pc_retrieve")
end event

type dw_grafico from uo_cs_xx_dw within w_grafico_autoval
integer x = 23
integer y = 400
integer width = 2491
integer height = 1480
integer taborder = 20
string dataobject = "d_grafico_autoval"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;setpointer(hourglass!)

wf_elabora()

resetupdate()

setpointer(arrow!)
end event

type dw_sel from u_dw_search within w_grafico_autoval
integer x = 23
integer y = 20
integer width = 2103
integer height = 360
integer taborder = 10
string dataobject = "d_sel_grafico_autoval"
end type

