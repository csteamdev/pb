﻿$PBExportHeader$w_report_autovalutazione_comp.srw
forward
global type w_report_autovalutazione_comp from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_autovalutazione_comp
end type
type dw_report from uo_cs_xx_dw within w_report_autovalutazione_comp
end type
end forward

global type w_report_autovalutazione_comp from w_cs_xx_principale
integer width = 3424
integer height = 2280
string title = "Report Autovalutazione Compilata"
cb_stampa cb_stampa
dw_report dw_report
end type
global w_report_autovalutazione_comp w_report_autovalutazione_comp

on w_report_autovalutazione_comp.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_report
end on

on w_report_autovalutazione_comp.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

//dw_report_finale.set_dw_options(sqlca, &
//                         pcca.null_object, &
//                         c_nomodify + &
//                         c_nodelete + &
//                         c_newonopen + &
//                         c_disableCC, &
//                         c_noresizedw + &
//                         c_nohighlightselected + &
//                         c_nocursorrowpointer +&
//                         c_nocursorrowfocusrect )
//iuo_dw_main = dw_report_finale
//
//
dw_report.settransobject(sqlca)
dw_report.retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_i_1,s_cs_xx.parametri.parametro_i_2)


end event

type cb_stampa from commandbutton within w_report_autovalutazione_comp
integer x = 2994
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;DW_REPORT.PRINT()
end event

type dw_report from uo_cs_xx_dw within w_report_autovalutazione_comp
integer x = 23
integer y = 20
integer width = 3337
integer height = 2040
integer taborder = 10
string dataobject = "d_report_autovalutazione_comp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

