﻿$PBExportHeader$w_report_trend_autovalutazione.srw
$PBExportComments$Finestra Report Autovalutazione_statistiche
forward
global type w_report_trend_autovalutazione from w_cs_xx_principale
end type
type cb_report from commandbutton within w_report_trend_autovalutazione
end type
type cb_selezione from commandbutton within w_report_trend_autovalutazione
end type
type dw_report from uo_cs_xx_dw within w_report_trend_autovalutazione
end type
type cb_esci from commandbutton within w_report_trend_autovalutazione
end type
type sle_1 from singlelineedit within w_report_trend_autovalutazione
end type
type dw_selezione from uo_cs_xx_dw within w_report_trend_autovalutazione
end type
end forward

global type w_report_trend_autovalutazione from w_cs_xx_principale
integer width = 3561
integer height = 1676
string title = "Stampa Statistiche Test Autovalutazione"
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
cb_esci cb_esci
sle_1 sle_1
dw_selezione dw_selezione
end type
global w_report_trend_autovalutazione w_report_trend_autovalutazione

type variables
boolean ib_interrupt
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report
dw_report.change_dw_current()

this.x = 741
this.y = 885
this.width = 2263
this.height = 773

end event

on w_report_trend_autovalutazione.create
int iCurrent
call super::create
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.cb_esci=create cb_esci
this.sle_1=create sle_1
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report
this.Control[iCurrent+2]=this.cb_selezione
this.Control[iCurrent+3]=this.dw_report
this.Control[iCurrent+4]=this.cb_esci
this.Control[iCurrent+5]=this.sle_1
this.Control[iCurrent+6]=this.dw_selezione
end on

on w_report_trend_autovalutazione.destroy
call super::destroy
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.cb_esci)
destroy(this.sle_1)
destroy(this.dw_selezione)
end on

event pc_setddlb;call super::pc_setddlb;string ls_select, ls_modify, ls_return

long   ll_pos, ll_start

datawindowchild ldwc_1


ls_select = "select distinct(cod_test), " + &
				"des_test from tes_autovalutazione " + &
				"where cod_azienda = '" + s_cs_xx.cod_azienda + &
				"' and approvato_da is not null " + &
				"order by cod_test"

dw_selezione.getchild("cod_test",ldwc_1)

ldwc_1.settransobject(sqlca)

ll_start = 1

do
	
	ll_pos = pos(ls_select,"'",ll_start)
	
	if ll_pos <> 0 then
		ls_select = replace(ls_select,ll_pos,1,"~~'")	
		ll_start = ll_pos + 2
	end if	
	
loop while ll_pos <> 0	

ls_modify = "DataWindow.Table.Select='" + ls_select + "'"

ls_return = ldwc_1.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("Errore","Errore nella modifica della select della dropdown datawindow: " + ls_return)
	return -1
end if

if ldwc_1.retrieve() = -1 then
	g_mb.messagebox("Errore","Errore nella retrieve della dropdown datawindow")
	return -1
end if
end event

type cb_report from commandbutton within w_report_trend_autovalutazione
integer x = 1806
integer y = 560
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string   ls_cod_test		 
datetime ldt_data_da, ldt_data_a
long     ll_num_edizione, ll_num_versione


dw_selezione.AcceptText()

ls_cod_test = dw_selezione.getitemstring(1, "cod_test")
ll_num_edizione = (dw_selezione.getitemnumber(1, "num_edizione"))
ll_num_versione = (dw_selezione.getitemnumber(1, "num_versione"))

if isnull(ls_cod_test) or isnull(ll_num_edizione) or isnull(ll_num_versione) then
	g_mb.messagebox("OMNIA","Selezionare codice, edizione e versione prima di continuare!")
	return -1
end if

ldt_data_da = dw_selezione.getItemDatetime(1, "data_registrazione_da")
ldt_data_a =  dw_selezione.getItemDatetime(1, "data_registrazione_a")
if isnull(ldt_data_da) or isnull(ldt_data_a)  then
	g_mb.messagebox("OMNIA","Selezionare le date prima di continuare!")
	return -1
end if
// RETRIEVE DEI DATI
dw_report.retrieve(s_cs_xx.cod_azienda,ls_cod_test, ll_num_edizione, ll_num_versione, ldt_data_da, ldt_data_a)

// VISUALIZZAZIONE DEL REPORT
dw_selezione.visible=false
parent.x = 74
parent.y = 301
parent.width = 3562
parent.height = 1677

dw_report.visible=true

cb_selezione.visible = true
cb_esci.visible = false
cb_report.visible = false
dw_report.Change_DW_Current()
dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.preview.rulers = 'Yes'
end event

type cb_selezione from commandbutton within w_report_trend_autovalutazione
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_report.reset()
dw_report.hide()
dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2263
parent.height = 773

cb_selezione.visible = false

cb_esci.visible = true
cb_report.visible = true



end event

type dw_report from uo_cs_xx_dw within w_report_trend_autovalutazione
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_trend_autovalutazione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_esci from commandbutton within w_report_trend_autovalutazione
integer x = 1486
integer y = 560
integer width = 297
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

event clicked;close(parent)
end event

type sle_1 from singlelineedit within w_report_trend_autovalutazione
integer x = 23
integer y = 540
integer width = 1029
integer height = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean autohscroll = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_trend_autovalutazione
integer x = 23
integer y = 20
integer width = 2171
integer height = 520
integer taborder = 20
string dataobject = "d_report_autovalutazione_selezione_m"
end type

event itemchanged;call super::itemchanged;string ls_select, ls_modify, ls_return, ls_cod_test, ls_where

long   ll_pos, ll_start, ll_num_edizione, ll_null

datawindowchild ldwc_1


setnull(ll_null)



choose case i_colname
		
	case "cod_test"
		
		ls_select = "select distinct(num_edizione), " + &
						"num_edizione from tes_autovalutazione " + &
						"where cod_azienda = '" + s_cs_xx.cod_azienda + &
						"' and approvato_da is not null" + &
						" and cod_test = '" + i_coltext + "' " + &
						"order by num_edizione"

		dw_selezione.getchild("num_edizione",ldwc_1)

		ldwc_1.settransobject(sqlca)

		ll_start = 1

		do
	
			ll_pos = pos(ls_select,"'",ll_start)
	
			if ll_pos <> 0 then
				ls_select = replace(ls_select,ll_pos,1,"~~'")	
				ll_start = ll_pos + 2
			end if	
	
		loop while ll_pos <> 0	

		ls_modify = "DataWindow.Table.Select='" + ls_select + "'"

		ls_return = ldwc_1.modify(ls_modify)

		if ls_return <> "" then
			g_mb.messagebox("Errore","Errore nella modifica della select della dropdown datawindow: " + ls_return)
			return -1
		end if

		if ldwc_1.retrieve() = -1 then
			g_mb.messagebox("Errore","Errore nella retrieve della dropdown datawindow")
			return -1
		end if
		
		dw_selezione.setitem(1,"num_edizione", ll_null)
		
		ll_num_edizione = getitemnumber(1,"num_edizione")
		
		ls_where = "cod_azienda = '" + s_cs_xx.cod_azienda + &
					  "' and cod_test = '" + i_coltext + "' " + &
					  " and num_edizione = " + string(ll_num_edizione)
		
		f_po_loaddddw_dw(dw_selezione,"num_versione",sqlca,"tes_autovalutazione",&
								"num_versione","num_versione",ls_where)
								
		dw_selezione.setitem(1,"num_versione", ll_null)
		
	case "num_edizione"
		
		ls_cod_test = getitemstring(1,"cod_test")
		
		ls_where = "cod_azienda = '" + s_cs_xx.cod_azienda + &
					  "' and cod_test = '" + ls_cod_test + "' " + &
					  " and num_edizione = " + i_coltext
		
		f_po_loaddddw_dw(dw_selezione,"num_versione",sqlca,"tes_autovalutazione",&
								"num_versione","num_versione",ls_where)
								
		dw_selezione.setitem(1,"num_versione", ll_null)		
	
 
		
		
		
end choose		
end event

