﻿$PBExportHeader$w_sottocriteri.srw
forward
global type w_sottocriteri from w_cs_xx_principale
end type
type dw_sottocriteri_det from uo_cs_xx_dw within w_sottocriteri
end type
type dw_sottocriteri_lista from uo_cs_xx_dw within w_sottocriteri
end type
type r_1 from rectangle within w_sottocriteri
end type
type r_2 from rectangle within w_sottocriteri
end type
end forward

global type w_sottocriteri from w_cs_xx_principale
integer width = 2318
integer height = 1512
string title = "Definizione Sottocriteri di Autovalutazione"
dw_sottocriteri_det dw_sottocriteri_det
dw_sottocriteri_lista dw_sottocriteri_lista
r_1 r_1
r_2 r_2
end type
global w_sottocriteri w_sottocriteri

on w_sottocriteri.create
int iCurrent
call super::create
this.dw_sottocriteri_det=create dw_sottocriteri_det
this.dw_sottocriteri_lista=create dw_sottocriteri_lista
this.r_1=create r_1
this.r_2=create r_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sottocriteri_det
this.Control[iCurrent+2]=this.dw_sottocriteri_lista
this.Control[iCurrent+3]=this.r_1
this.Control[iCurrent+4]=this.r_2
end on

on w_sottocriteri.destroy
call super::destroy
destroy(this.dw_sottocriteri_det)
destroy(this.dw_sottocriteri_lista)
destroy(this.r_1)
destroy(this.r_2)
end on

event pc_setwindow;call super::pc_setwindow;dw_sottocriteri_lista.set_dw_key("cod_azienda")
dw_sottocriteri_lista.set_dw_key("cod_test")
dw_sottocriteri_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)
dw_sottocriteri_det.set_dw_options(sqlca, &
                                    dw_sottocriteri_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
iuo_dw_main = dw_sottocriteri_lista



end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_sottocriteri_det,"cod_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo",&
                 "tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
end event

type dw_sottocriteri_det from uo_cs_xx_dw within w_sottocriteri
integer x = 23
integer y = 500
integer width = 2240
integer height = 900
integer taborder = 20
string dataobject = "d_sottocriteri_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_colname = "peso" then
	  if long(i_coltext) < 1 OR long(i_coltext) > 10 or long (i_coltext) = 0 then
		  g_mb.messagebox("Definizione criteri", "Inserire un valore compreso tra 1 e 10")
		  return 1;
	end if
end if	
end event

type dw_sottocriteri_lista from uo_cs_xx_dw within w_sottocriteri
integer x = 23
integer width = 2240
integer height = 480
integer taborder = 10
string dataobject = "d_sottocriteri_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_num_edizione, ll_num_versione, ll_num_progressivo
string ls_cod_tipo_trattativa, ls_cod_azienda 
ls_cod_azienda = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_azienda")
ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
ll_num_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")


ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_trattativa,ll_num_edizione,ll_num_versione,ll_num_progressivo)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_tipo_trattativa

ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_test")) then
      this.setitem(ll_i, "cod_test", ls_cod_tipo_trattativa)
   end if
next
end event

event pcd_new;call super::pcd_new;string ls_cod_test
long ll_progressivo, ll_num_edizione, ll_num_versione, ll_num_progressivo

ls_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
ll_num_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")



select max(prog_riga_sottocriterio)
into :ll_progressivo
from det_autovalutazione_sottocrit
where cod_azienda = :s_cs_xx.cod_azienda
   and cod_test = :ls_cod_test
	and num_edizione = :ll_num_edizione
	and num_versione = :ll_num_versione
	and progressivo = :ll_num_progressivo;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore lettura dettaglio test" + sqlca.sqlerrtext)
	return
end if

if isnull(ll_progressivo) then ll_progressivo = 0

ll_progressivo = ll_progressivo + 1
setitem(getrow(), "progressivo", ll_num_progressivo)
setitem(getrow(), "cod_test", ls_cod_test)
setitem(getrow(), "num_versione", ll_num_versione)
setitem(getrow(), "num_edizione", ll_num_edizione)
setitem(getrow(), "prog_riga_sottocriterio", ll_progressivo)



end event

type r_1 from rectangle within w_sottocriteri
long linecolor = 8388608
integer linethickness = 1
long fillcolor = 16777215
integer x = 46
integer y = 740
integer width = 160
integer height = 80
end type

type r_2 from rectangle within w_sottocriteri
long linecolor = 8388608
integer linethickness = 1
long fillcolor = 16777215
integer x = 69
integer y = 700
integer width = 160
integer height = 140
end type

