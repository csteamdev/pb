﻿$PBExportHeader$w_tes_autoval_comp.srw
forward
global type w_tes_autoval_comp from w_cs_xx_principale
end type
type cb_cerca from commandbutton within w_tes_autoval_comp
end type
type cb_annulla from commandbutton within w_tes_autoval_comp
end type
type dw_sel from u_dw_search within w_tes_autoval_comp
end type
type cb_report_comp from commandbutton within w_tes_autoval_comp
end type
type cb_report_gen from commandbutton within w_tes_autoval_comp
end type
type cb_calcola from commandbutton within w_tes_autoval_comp
end type
type cb_criteri from commandbutton within w_tes_autoval_comp
end type
type dw_tes_autoval_comp_lista from uo_cs_xx_dw within w_tes_autoval_comp
end type
type dw_tes_autoval_comp_det from uo_cs_xx_dw within w_tes_autoval_comp
end type
type dw_folder from u_folder within w_tes_autoval_comp
end type
end forward

global type w_tes_autoval_comp from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2354
integer height = 1648
string title = "Compilazione Autovalutazione"
cb_cerca cb_cerca
cb_annulla cb_annulla
dw_sel dw_sel
cb_report_comp cb_report_comp
cb_report_gen cb_report_gen
cb_calcola cb_calcola
cb_criteri cb_criteri
dw_tes_autoval_comp_lista dw_tes_autoval_comp_lista
dw_tes_autoval_comp_det dw_tes_autoval_comp_det
dw_folder dw_folder
end type
global w_tes_autoval_comp w_tes_autoval_comp

type variables
long il_anno_registrazione, il_num_registrazione, il_riga_corrente, tot_criterio
string is_cod_test
end variables

on w_tes_autoval_comp.create
int iCurrent
call super::create
this.cb_cerca=create cb_cerca
this.cb_annulla=create cb_annulla
this.dw_sel=create dw_sel
this.cb_report_comp=create cb_report_comp
this.cb_report_gen=create cb_report_gen
this.cb_calcola=create cb_calcola
this.cb_criteri=create cb_criteri
this.dw_tes_autoval_comp_lista=create dw_tes_autoval_comp_lista
this.dw_tes_autoval_comp_det=create dw_tes_autoval_comp_det
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cerca
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_sel
this.Control[iCurrent+4]=this.cb_report_comp
this.Control[iCurrent+5]=this.cb_report_gen
this.Control[iCurrent+6]=this.cb_calcola
this.Control[iCurrent+7]=this.cb_criteri
this.Control[iCurrent+8]=this.dw_tes_autoval_comp_lista
this.Control[iCurrent+9]=this.dw_tes_autoval_comp_det
this.Control[iCurrent+10]=this.dw_folder
end on

on w_tes_autoval_comp.destroy
call super::destroy
destroy(this.cb_cerca)
destroy(this.cb_annulla)
destroy(this.dw_sel)
destroy(this.cb_report_comp)
destroy(this.cb_report_gen)
destroy(this.cb_calcola)
destroy(this.cb_criteri)
destroy(this.dw_tes_autoval_comp_lista)
destroy(this.dw_tes_autoval_comp_det)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]


dw_tes_autoval_comp_lista.set_dw_key("cod_azienda")

dw_tes_autoval_comp_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)

dw_tes_autoval_comp_det.set_dw_options(sqlca,dw_tes_autoval_comp_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_tes_autoval_comp_lista

dw_sel.insertrow(0)

dw_folder.fu_folderoptions(dw_folder.c_defaultheight,dw_folder.c_foldertableft)

l_objects[1] = dw_tes_autoval_comp_lista

dw_folder.fu_assigntab(1,"L.", l_objects)

l_objects[1] = dw_sel
l_objects[2] = cb_cerca
l_objects[3] = cb_annulla

dw_folder.fu_assigntab(2,"R.",l_objects)

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(2)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_autoval_comp_det,"cod_test",sqlca,"tes_autovalutazione","cod_test","des_test", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_ultima_revisione = 'S'")

f_po_loaddddw_dw(dw_tes_autoval_comp_det,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel,"cod_test",sqlca,"tes_autovalutazione","cod_test","des_test", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_ultima_revisione = 'S'")

f_po_loaddddw_dw(dw_sel,"cod_area",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_cerca from commandbutton within w_tes_autoval_comp
integer x = 1874
integer y = 320
integer width = 366
integer height = 81
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_folder.fu_selecttab(1)

dw_tes_autoval_comp_lista.change_dw_current()

parent.postevent("pc_retrieve")
end event

type cb_annulla from commandbutton within w_tes_autoval_comp
integer x = 1486
integer y = 320
integer width = 366
integer height = 81
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string		ls_null

datetime	ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_sel.setitem(1,"comp_da",ldt_null)

dw_sel.setitem(1,"comp_a",ldt_null)

dw_sel.setitem(1,"cod_test",ls_null)

dw_sel.setitem(1,"cod_area",ls_null)
end event

type dw_sel from u_dw_search within w_tes_autoval_comp
integer x = 160
integer y = 40
integer width = 2103
integer height = 280
integer taborder = 20
string dataobject = "d_tes_autoval_comp_sel"
boolean border = false
end type

type cb_report_comp from commandbutton within w_tes_autoval_comp
integer x = 1531
integer y = 1440
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report comp."
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = dw_tes_autoval_comp_lista.getitemnumber(dw_tes_autoval_comp_lista.getrow(),"anno_registrazione")

s_cs_xx.parametri.parametro_i_2 = dw_tes_autoval_comp_lista.getitemnumber(dw_tes_autoval_comp_lista.getrow(),"num_registrazione")

window_open(w_report_autovalutazione_comp, -1)
end event

type cb_report_gen from commandbutton within w_tes_autoval_comp
integer x = 1143
integer y = 1440
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report Trend"
end type

event clicked;/*s_cs_xx.parametri.parametro_d_1 = il_anno_registrazione
s_cs_xx.parametri.parametro_d_2 = il_num_registrazione*/  
window_open(w_report_trend_autovalutazione, -1)

end event

type cb_calcola from commandbutton within w_tes_autoval_comp
integer x = 754
integer y = 1440
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;/*dec{2} ld_tot_test*/
long  ll_tot_criterio, ll_tot_criterio_massimo
long ll_punteggio_risposta, ll_punteggio_risposta_massimo
long ll_riga_corrente
long ll_risposta, ll_peso_sottocriterio
dec{4}  ll_tot_test,ll_prog_riga_criterio, ll_peso_criterio, ld_tot_criterio_centesimi, val_criterio_centesimi
dec{4}  ll_tot_test_massimo,  val_criterio_centesimi_massimo

ll_riga_corrente = dw_tes_autoval_comp_det.getrow()
is_cod_test = dw_tes_autoval_comp_det.getitemstring(ll_riga_corrente,"cod_test")
il_anno_registrazione = dw_tes_autoval_comp_det.getitemnumber(ll_riga_corrente,"anno_registrazione")
il_num_registrazione = dw_tes_autoval_comp_det.getitemnumber(ll_riga_corrente,"num_registrazione")

ll_tot_test = 0
ll_tot_test_massimo = 0
declare criteri cursor for
select  prog_riga_criterio, peso_criterio
from    det_autoval_comp_criteri
where   cod_azienda = :s_cs_xx.cod_azienda and
		  anno_registrazione = :il_anno_registrazione and
		  num_registrazione = :il_num_registrazione;
		 
open criteri;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Compilazione test", "Errore nell'apertura del cursore")
	close criteri;
	return -1
end if
do while true
	
	fetch criteri
	into  :ll_prog_riga_criterio,
	      :ll_peso_criterio;
	
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Compilazione test", "Errore nella fetch del cursore criteri : " + sqlca.sqlerrtext)
		close criteri;
		return -1
	elseif sqlca.sqlcode = 100	then
		close criteri;
		exit 
	end if
	
 ll_tot_criterio = 0
 ll_tot_criterio_massimo = 0
	declare sottocriteri cursor for
	select  flag_val_sottocriterio,
           peso_sottocriterio
	from    det_autoval_comp_sottocrit
	where   cod_azienda = :s_cs_xx.cod_azienda and
           anno_registrazione = :il_anno_registrazione and
           num_registrazione  = :il_num_registrazione and
			  prog_riga_criterio = :ll_prog_riga_criterio;
	
	open sottocriteri;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("compilazione test", "Errore nell'aperatura del cursore sottocriteri")
	close sottocriteri;
	return -1
end if
	
	do while true
		
	 fetch sottocriteri
	 into :ll_risposta,
	 		:ll_peso_sottocriterio;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Compilazione test", "Errore nella fetch di sottocriteri: " + sqlca.sqlerrtext)
		close sottocriteri;
		return -1
	elseif ll_peso_sottocriterio = 0 or isnull(ll_peso_sottocriterio) then	
		g_mb.messagebox("ciao", "Il peso è zero oppure non è stato inserito")
		close sottocriteri;
		return -1
	elseif sqlca.sqlcode = 100	then
		close sottocriteri;
		exit
	end if
			 
    choose case ll_risposta 
	 	case 1 
    		ll_punteggio_risposta = ll_peso_sottocriterio * 1
    	case 2 
    		ll_punteggio_risposta = ll_peso_sottocriterio * 2
    	case 3 
    		ll_punteggio_risposta = ll_peso_sottocriterio * 3
	 	case 4
    		ll_punteggio_risposta = ll_peso_sottocriterio * 4  
    	case 5
    		ll_punteggio_risposta = ll_peso_sottocriterio * 5
    end choose
	 
	 ll_punteggio_risposta_massimo = ll_peso_sottocriterio * 5 
	 ll_tot_criterio_massimo = ll_tot_criterio_massimo + ll_punteggio_risposta_massimo  
    ll_tot_criterio = ll_tot_criterio + ll_punteggio_risposta
    loop 	
	 
	 ld_tot_criterio_centesimi = ll_tot_criterio / ll_tot_criterio_massimo
	 val_criterio_centesimi  = ld_tot_criterio_centesimi * ll_peso_criterio
	 val_criterio_centesimi_massimo = ll_peso_criterio 
	 ll_tot_test = ll_tot_test + val_criterio_centesimi
	 ll_tot_test_massimo = ll_tot_test_massimo + val_criterio_centesimi_massimo
	 UPDATE det_autoval_comp_criteri  
    SET    punteggio_criterio = :ld_tot_criterio_centesimi
    WHERE  ( det_autoval_comp_criteri.cod_azienda = :s_cs_xx.cod_azienda ) AND  
           ( det_autoval_comp_criteri.anno_registrazione = :il_anno_registrazione ) AND  
           ( det_autoval_comp_criteri.num_registrazione  = :il_num_registrazione ) AND
			  ( det_autoval_comp_criteri.prog_riga_criterio = :ll_prog_riga_criterio)                                                                  ;  
       
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Compilazione test","Errore nell'aggiornamento della tabella comp_test: " + sqlca.sqlerrtext)
	rollback;
   return -1
end if

	 
 	close sottocriteri;
	 
	  
	
	  
	
loop	

close criteri;
ll_tot_test = ll_tot_test / ll_tot_test_massimo 
UPDATE tes_autoval_comp  
SET    punteggio = :ll_tot_test
WHERE  ( tes_autoval_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_autoval_comp.anno_registrazione = :il_anno_registrazione ) AND  
       ( tes_autoval_comp.num_registrazione  = :il_num_registrazione ) ;  
       
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Compilazione test","Errore nell'aggiornamento della tabella comp_test: " + sqlca.sqlerrtext)
	rollback;
   return -1
end if

g_mb.messagebox("Compilazione test","Aggiornamento effettuato")
commit;
dw_tes_autoval_comp_lista.triggerevent("pcd_retrieve")

end event

type cb_criteri from commandbutton within w_tes_autoval_comp
integer x = 1920
integer y = 1440
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Criteri"
end type

event clicked;window_open_parm(w_det_autoval_comp_criteri, -1, dw_tes_autoval_comp_lista )

end event

type dw_tes_autoval_comp_lista from uo_cs_xx_dw within w_tes_autoval_comp
integer x = 137
integer y = 40
integer width = 2126
integer height = 540
integer taborder = 10
string dataobject = "d_tes_autoval_comp_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long			ll_error

datetime	ldt_da, ldt_a

string		ls_cod_test, ls_cod_area


ldt_da = dw_sel.getitemdatetime(1,"comp_da")

ldt_a = dw_sel.getitemdatetime(1,"comp_a")

ls_cod_test = dw_sel.getitemstring(1,"cod_test")

ls_cod_area = dw_sel.getitemstring(1,"cod_area")

ll_error = retrieve(s_cs_xx.cod_azienda,ldt_da,ldt_a,ls_cod_test,ls_cod_area)

if ll_error < 0 then
	pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i,"cod_azienda"))then
		this.setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
next
end event

event pcd_new;call super::pcd_new;long ll_progressivo_corrente, ll_riga_corrente

setitem(getrow(), "data_registrazione",datetime(today()))
setitem(getrow(), "anno_registrazione",f_anno_esercizio())
select max(num_registrazione)
into :ll_progressivo_corrente
from tes_autoval_comp
where cod_azienda = :s_cs_xx.cod_azienda;
  
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore lettura dettaglio test" + sqlca.sqlerrtext)
	return
end if



if isnull(ll_progressivo_corrente) then ll_progressivo_corrente = 0

ll_progressivo_corrente = ll_progressivo_corrente + 1
setitem(getrow(), "num_registrazione", ll_progressivo_corrente )


end event

type dw_tes_autoval_comp_det from uo_cs_xx_dw within w_tes_autoval_comp
integer x = 23
integer y = 620
integer width = 2263
integer height = 800
integer taborder = 20
string dataobject = "d_tes_autoval_comp_det"
end type

event itemchanged;call super::itemchanged;long ls_num_versione_cambiato, ls_num_edizione_cambiato
if i_colname = "cod_test" then 
	
SELECT tes_autovalutazione.num_edizione,
       tes_autovalutazione.num_versione	
INTO   :ls_num_edizione_cambiato,
	    :ls_num_versione_cambiato 
	
	FROM  tes_autovalutazione 
	WHERE (tes_autovalutazione.cod_azienda = :s_cs_xx.cod_azienda ) and
         (tes_autovalutazione.cod_test = : i_coltext) and
			(tes_autovalutazione.flag_ultima_revisione = 'S');

setitem(getrow(), "num_edizione", ls_num_edizione_cambiato)
setitem(getrow(), "num_versione", ls_num_versione_cambiato)
end if
end event

type dw_folder from u_folder within w_tes_autoval_comp
integer x = 23
integer y = 20
integer width = 2263
integer height = 580
integer taborder = 30
end type

