﻿$PBExportHeader$w_tes_autovalutazione.srw
forward
global type w_tes_autovalutazione from w_cs_xx_principale
end type
type rb_storico from radiobutton within w_tes_autovalutazione
end type
type cb_dettagli from commandbutton within w_tes_autovalutazione
end type
type cb_esegui from commandbutton within w_tes_autovalutazione
end type
type rb_in_creazione from radiobutton within w_tes_autovalutazione
end type
type rb_ultimerevisioni from radiobutton within w_tes_autovalutazione
end type
type rb_nuova_revisione from radiobutton within w_tes_autovalutazione
end type
type rb_approva from radiobutton within w_tes_autovalutazione
end type
type rb_verifica from radiobutton within w_tes_autovalutazione
end type
type dw_testata_det from uo_cs_xx_dw within w_tes_autovalutazione
end type
type dw_testata_lista from uo_cs_xx_dw within w_tes_autovalutazione
end type
type gb_1 from groupbox within w_tes_autovalutazione
end type
type gb_2 from groupbox within w_tes_autovalutazione
end type
end forward

global type w_tes_autovalutazione from w_cs_xx_principale
integer width = 3099
integer height = 1816
string title = "Autovalutazione"
rb_storico rb_storico
cb_dettagli cb_dettagli
cb_esegui cb_esegui
rb_in_creazione rb_in_creazione
rb_ultimerevisioni rb_ultimerevisioni
rb_nuova_revisione rb_nuova_revisione
rb_approva rb_approva
rb_verifica rb_verifica
dw_testata_det dw_testata_det
dw_testata_lista dw_testata_lista
gb_1 gb_1
gb_2 gb_2
end type
global w_tes_autovalutazione w_tes_autovalutazione

type variables
long il_tipo_operazione
string is_flag_valido 
string is_tipo_records
string filtrocreazione, filtroultimerevisioni, filtrostorico
date idt_da_data, idt_a_data
boolean ib_delete=true, ib_new=false, ib_new_rec=false

end variables

forward prototypes
public function integer wf_autorizza ()
public function integer wf_approva ()
public function integer wf_nuova_edizione ()
end prototypes

public function integer wf_autorizza ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
string ls_cod_test
string ls_approvato_da, ls_autorizzato_da
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente
datetime ldt_oggi

/*if is_tipo_records <> "C" then
   messagebox("Autorizzazione Lista di Controllo","Si possono autorizzare solo le liste in fase di CREAZIONE", StopSign!)
   return -1
end if */

if isnull(dw_testata_lista.getitemstring(dw_testata_lista.getrow(),"autorizzato_da")) then
   g_mb.messagebox("Approvazione test","Test non ancora verificato", StopSign!)
   return -1
end if

if not(isnull(dw_testata_lista.getitemstring(dw_testata_lista.getrow(),"approvato_da"))) then
   g_mb.messagebox("Approvazione test","Test già approvato", StopSign!)
   return -1
end if


setpointer(HourGlass!)

ll_riga_corrente = dw_testata_det.getrow()
ls_cod_test = dw_testata_det.getitemstring(ll_riga_corrente,"cod_test")
ll_versione = dw_testata_det.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_testata_det.getitemnumber(ll_riga_corrente,"num_edizione")


//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
      	   mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
      	 :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
      		 ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio
		 

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione test",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_autorizzazione <> "S" then
   g_mb.messagebox("Approvazione test","L'utente non possiede il PRIVILEGIO di approvazione: verificare mansionario", StopSign!)
   return -1
end if 

ldt_oggi = datetime(today())
UPDATE tes_autovalutazione  
SET    flag_ultima_revisione = 'N'
WHERE  ( tes_autovalutazione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_autovalutazione.cod_test = :ls_cod_test );




UPDATE tes_autovalutazione  
SET    approvato_da = :ls_cod_resp_divisione,
       data_approvazione = :ldt_oggi,
	    flag_ultima_revisione = 'S'	 
WHERE  ( tes_autovalutazione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_autovalutazione.cod_test = :ls_cod_test ) AND  
       ( tes_autovalutazione.num_versione  = :ll_versione ) AND  
       ( tes_autovalutazione.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione test",sqlca.sqlerrtext)
   return -1
end if
if sqlca.sqlcode = 0 then 
	g_mb.messagebox("Approvazione test","APPROVAZIONE EFFETTUATA")
end if

COMMIT;

return 0
end function

public function integer wf_approva ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione, ls_cod_test
long ll_versione, ll_edizione, ll_riga_corrente
datetime ldt_oggi
if is_tipo_records <> "C" then
   g_mb.messagebox("Approvazione Test","Si possono approvare solo i test in fase di CREAZIONE", StopSign!)
   return -1
end if
if not(isnull(dw_testata_lista.getitemstring(dw_testata_lista.getrow(),"autorizzato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già VERIFICATA", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_testata_det.getrow()
ls_cod_test = dw_testata_det.getitemstring(ll_riga_corrente,"cod_test")
ll_versione = dw_testata_det.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_testata_det.getitemnumber(ll_riga_corrente,"num_edizione")

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
		 
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Verifica test","Errore nellA RICERCA DEL'UTENTE: " + sqlca.sqlerrtext)
   return -1
end if

if ls_flag_approvazione <> "S" then
   g_mb.messagebox("Verifica test","L'utente non possiede il PRIVILEGIO di VERIFICA: verificare mansionario", StopSign!)
   return -1
end if 

ldt_oggi = datetime(today())    

UPDATE tes_autovalutazione  
SET    data_autorizzazione = :ldt_oggi,
       autorizzato_da = :s_cs_xx.cod_utente
		 
WHERE  ( tes_autovalutazione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_autovalutazione.cod_test = :ls_cod_test ) AND  
       ( tes_autovalutazione.num_versione  = :ll_versione ) AND  
       ( tes_autovalutazione.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Verifica test","Errore nell'aggiornamento di tes_autovalutazione: " + sqlca.sqlerrtext)
	rollback;
   return -1
end if

g_mb.messagebox("Verifica test","VERIFICA EFFETTUATA")

commit;

return 0
end function

public function integer wf_nuova_edizione ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
string ls_null, ls_str,ll_num_reg
long  ll_versione, ll_edizione, ll_riga_corrente, ll_new_edizione, ll_new_versione, ll_max_edizioni, ll_nuova_versione, ll_nuova_edizione
long  ll_num
datetime ldt_scadenza_validazione, ldt_oggi, ldt_null

setnull(ls_null)
setnull(ldt_null)
/*if is_tipo_records <> "E" then
   messagebox("Autorizzazione Lista di Controllo","Si possono creare nuove edizioni solo da liste verificate/approvate", StopSign!)
   return -1
end if*/

if isnull(dw_testata_lista.getitemstring(dw_testata_lista.getrow(),"autorizzato_da")) then
   g_mb.messagebox("Nuova edizione","Test non ancora VERIFICATO", StopSign!)
   return -1
end if

if isnull(dw_testata_lista.getitemstring(dw_testata_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Nuova edizione","Test non ancora APPROVATO", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_testata_det.getrow()
ll_num_reg = dw_testata_det.getitemstring(ll_riga_corrente,"cod_test")
ll_versione = dw_testata_det.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_testata_det.getitemnumber(ll_riga_corrente,"num_edizione")

SELECT parametri_azienda.numero
  INTO :ll_num
  FROM parametri_azienda
  WHERE (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda) and
 		 (parametri_azienda.flag_parametro = 'N') and
       (parametri_azienda.cod_parametro = 'NED');
		if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione","INTERROMPO ELABORAZIONE: ERRORE NEL CERCARE IL NUM MAX DI VERSIONI")
      ROLLBACK;
      return -1
   end if
	
	if ll_versione = ll_num then
		ll_new_versione = 0
		ll_new_edizione = ll_edizione + 1
	else		
		ll_new_versione = ll_versione + 1
		ll_new_edizione = ll_edizione	
	end if
	
	SELECT mansionari.cod_resp_divisione  
   INTO   :ls_cod_resp_divisione  
   FROM   mansionari  
   WHERE  (mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          (mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
			 
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if
	
	ldt_oggi = datetime(today())
	
   INSERT INTO tes_autovalutazione  
         ( cod_azienda,   
           cod_test,   
           num_versione,   
           num_edizione,   
           des_test,   
           cod_area_aziendale,   
           emesso_da,   
           approvato_da,   
           autorizzato_da,   
           data_autorizzazione,
			  data_emissione,
           note)  
     SELECT tes_autovalutazione.cod_azienda,   
            tes_autovalutazione.cod_test,   
            :ll_new_versione,   
            :ll_new_edizione,   
            tes_autovalutazione.des_test,   
            tes_autovalutazione.cod_area_aziendale,   
            :ls_cod_resp_divisione,   
            :ls_null,
				:ls_null,
				:ldt_null,
				:ldt_oggi,
            tes_autovalutazione.note          
		FROM tes_autovalutazione  
      WHERE ( tes_autovalutazione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( tes_autovalutazione.cod_test = :ll_num_reg ) AND  
            ( tes_autovalutazione.num_versione = :ll_versione ) AND  
            ( tes_autovalutazione.num_edizione = :ll_edizione )   ;
   
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if  
  
  INSERT INTO det_autovalutazione_criteri  
         ( cod_azienda,   
           cod_test,   
           num_versione,   										
           num_edizione,   
           progressivo,   
           des_criterio,
			  cod_paragrafo,
			  note,
			  flag_blocco,
			  data_blocco,
			  peso
			  )   
            
     SELECT det_autovalutazione_criteri.cod_azienda,   
            det_autovalutazione_criteri.cod_test,   
            :ll_new_versione,   
            :ll_new_edizione,   
            det_autovalutazione_criteri.progressivo,   
            det_autovalutazione_criteri.des_criterio,
				det_autovalutazione_criteri.cod_paragrafo,
				det_autovalutazione_criteri.note,
				det_autovalutazione_criteri.flag_blocco,
				det_autovalutazione_criteri.data_blocco,
				det_autovalutazione_criteri.peso
				
     FROM   det_autovalutazione_criteri      
              
     WHERE ( det_autovalutazione_criteri.cod_azienda = :s_cs_xx.cod_azienda ) AND  
           ( det_autovalutazione_criteri.cod_test = :ll_num_reg ) AND  
           ( det_autovalutazione_criteri.num_versione = :ll_versione ) AND  
           ( det_autovalutazione_criteri.num_edizione = :ll_edizione );

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Nuova Edizione",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
   ROLLBACK;
   return -1
end if
INSERT INTO det_autovalutazione_sottocrit 
         ( cod_azienda,   
           cod_test,   
           num_versione,   
           num_edizione,   
           progressivo,
			  prog_riga_sottocriterio,
           des_sottocriterio,
			  cod_paragrafo,
			  note,
			  flag_blocco,              
			  data_blocco,               
			  peso)   
            
     SELECT det_autovalutazione_sottocrit.cod_azienda,   
            det_autovalutazione_sottocrit.cod_test,   
            :ll_new_versione,   
            :ll_new_edizione,   
            det_autovalutazione_sottocrit.progressivo,
				det_autovalutazione_sottocrit.prog_riga_sottocriterio,
            det_autovalutazione_sottocrit.des_sottocriterio,
				det_autovalutazione_sottocrit.cod_paragrafo,
				det_autovalutazione_sottocrit.note,
				det_autovalutazione_sottocrit.flag_blocco,
				det_autovalutazione_sottocrit.data_blocco,
				det_autovalutazione_sottocrit.peso
				
     FROM   det_autovalutazione_sottocrit      
              
     WHERE ( det_autovalutazione_sottocrit.cod_azienda = :s_cs_xx.cod_azienda ) AND  
           ( det_autovalutazione_sottocrit.cod_test = :ll_num_reg ) AND  
           ( det_autovalutazione_sottocrit.num_versione = :ll_versione ) AND  
           ( det_autovalutazione_sottocrit.num_edizione = :ll_edizione );

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Nuova Edizione",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
   ROLLBACK;
   return -1
end if 


COMMIT;
dw_testata_lista.postevent("pcd_retrieve")
return 0
end function

on w_tes_autovalutazione.create
int iCurrent
call super::create
this.rb_storico=create rb_storico
this.cb_dettagli=create cb_dettagli
this.cb_esegui=create cb_esegui
this.rb_in_creazione=create rb_in_creazione
this.rb_ultimerevisioni=create rb_ultimerevisioni
this.rb_nuova_revisione=create rb_nuova_revisione
this.rb_approva=create rb_approva
this.rb_verifica=create rb_verifica
this.dw_testata_det=create dw_testata_det
this.dw_testata_lista=create dw_testata_lista
this.gb_1=create gb_1
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_storico
this.Control[iCurrent+2]=this.cb_dettagli
this.Control[iCurrent+3]=this.cb_esegui
this.Control[iCurrent+4]=this.rb_in_creazione
this.Control[iCurrent+5]=this.rb_ultimerevisioni
this.Control[iCurrent+6]=this.rb_nuova_revisione
this.Control[iCurrent+7]=this.rb_approva
this.Control[iCurrent+8]=this.rb_verifica
this.Control[iCurrent+9]=this.dw_testata_det
this.Control[iCurrent+10]=this.dw_testata_lista
this.Control[iCurrent+11]=this.gb_1
this.Control[iCurrent+12]=this.gb_2
end on

on w_tes_autovalutazione.destroy
call super::destroy
destroy(this.rb_storico)
destroy(this.cb_dettagli)
destroy(this.cb_esegui)
destroy(this.rb_in_creazione)
destroy(this.rb_ultimerevisioni)
destroy(this.rb_nuova_revisione)
destroy(this.rb_approva)
destroy(this.rb_verifica)
destroy(this.dw_testata_det)
destroy(this.dw_testata_lista)
destroy(this.gb_1)
destroy(this.gb_2)
end on

event pc_setwindow;call super::pc_setwindow;dw_testata_lista.set_dw_key("cod_azienda")

dw_testata_lista.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_default, &
                         c_default)
dw_testata_det.set_dw_options(sqlca, &
                               dw_testata_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
iuo_dw_main = dw_testata_lista


//  MICHELA 22/11/2005: IMPOSTO COME PUNTO DI PARTENZA iN CREAZIONE

rb_in_creazione.checked = true
rb_verifica.enabled = true
rb_approva.enabled = true
rb_nuova_revisione.enabled = FALSE
cb_esegui.enabled = TRUE
is_tipo_records = "C"
filtrocreazione = "(isnull (approvato_da))"

dw_testata_lista.setfilter(filtrocreazione)
dw_testata_lista.filter()

dw_testata_lista.Change_DW_Current( )
this.triggerevent("pc_retrieve")


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_testata_det,"emesso_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_testata_det,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
f_PO_LoadDDDW_DW(dw_testata_det,"cod_divisione",sqlca,&
                 "anag_divisioni", "cod_divisione", "des_divisione", &
                 "anag_divisioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'" )
					  
f_po_loaddddw_dw(dw_testata_det, &
                 "cod_reparto", &
                 sqlca, &
                 "tab_reparti_dip", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_PO_LoadDDDW_DW(dw_testata_det,"approvato_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  

end event

type rb_storico from radiobutton within w_tes_autovalutazione
integer x = 1943
integer y = 320
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Storico"
end type

event clicked;rb_verifica.enabled = false
rb_approva.enabled = false
rb_nuova_revisione.enabled = false
cb_esegui.enabled = false

is_flag_valido = "N"
is_tipo_records = "T"

filtrostorico= "( not  ( isnull(approvato_da) ) ) and ( flag_ultima_revisione = 'N' )" 

/*dw_testata_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")*/

dw_testata_lista.setfilter(filtrostorico)
dw_testata_lista.filter()
dw_testata_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

end event

type cb_dettagli from commandbutton within w_tes_autovalutazione
integer x = 2491
integer y = 440
integer width = 549
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Criteri"
end type

event clicked;window_open_parm(w_criteri_test, -1, dw_testata_lista)


end event

type cb_esegui from commandbutton within w_tes_autovalutazione
integer x = 1920
integer y = 440
integer width = 549
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Esegui"
end type

event clicked;long ll_riga_corrente

ll_riga_corrente = dw_testata_lista.getrow()
choose case il_tipo_operazione
case 1
   if wf_approva() = 0 then parent.triggerevent("pc_retrieve")
   dw_testata_lista.setrow(ll_riga_corrente)
   rb_in_creazione.triggerevent("clicked")
   setpointer(arrow!)	
case 2
   if wf_autorizza() = 0 then parent.triggerevent("pc_retrieve")
   dw_testata_lista.setrow(ll_riga_corrente)
   rb_in_creazione.triggerevent("clicked")
   setpointer(arrow!) 
case 4
   if wf_nuova_edizione() = 0 then parent.triggerevent("pc_retrieve")
   g_mb.messagebox("Nuova Edizione","La nuova edizione si trova fra le liste IN CREAZIONE",Information!)
	/*dw_testata_lista.setrow(ll_riga_corrente)
   rb_in_creazione.triggerevent("clicked")*/
   setpointer(arrow!)
end choose
end event

type rb_in_creazione from radiobutton within w_tes_autovalutazione
integer x = 1943
integer y = 220
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "In Creazione"
end type

event clicked;rb_verifica.enabled = true
rb_approva.enabled = true
rb_nuova_revisione.enabled = FALSE
cb_esegui.enabled = TRUE
is_tipo_records = "C"
filtrocreazione = "(isnull (approvato_da))"

dw_testata_lista.setfilter(filtrocreazione)
dw_testata_lista.filter()

dw_testata_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

end event

type rb_ultimerevisioni from radiobutton within w_tes_autovalutazione
integer x = 1943
integer y = 120
integer width = 494
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Ultime Revisioni"
end type

event clicked;rb_verifica.enabled = false
rb_approva.enabled = false
rb_nuova_revisione.enabled = true
cb_esegui.enabled = true

is_tipo_records = "E" 
filtroultimerevisioni = "( not  ( isnull(approvato_da) ) ) and ( flag_ultima_revisione = 'S' )"  

dw_testata_lista.setfilter(filtroultimerevisioni)
dw_testata_lista.filter()

dw_testata_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")


end event

type rb_nuova_revisione from radiobutton within w_tes_autovalutazione
integer x = 2514
integer y = 320
integer width = 503
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nuova Revisione"
end type

event clicked;il_tipo_operazione = 4
cb_esegui.enabled = true
end event

type rb_approva from radiobutton within w_tes_autovalutazione
integer x = 2514
integer y = 220
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Approva"
end type

event clicked;il_tipo_operazione = 2
cb_esegui.enabled = true
end event

type rb_verifica from radiobutton within w_tes_autovalutazione
integer x = 2514
integer y = 120
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Verifica"
end type

event clicked;il_tipo_operazione = 1
cb_esegui.enabled = true
end event

type dw_testata_det from uo_cs_xx_dw within w_tes_autovalutazione
integer y = 540
integer width = 3040
integer height = 1160
integer taborder = 20
string dataobject = "d_tes_autovalutazione_det"
borderstyle borderstyle = styleraised!
end type

type dw_testata_lista from uo_cs_xx_dw within w_tes_autovalutazione
integer x = 23
integer y = 20
integer width = 1874
integer height = 500
integer taborder = 10
string dataobject = "d_tes_autovalutazione_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)
                   
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;long ll_i
for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i,"cod_azienda"))then
		this.setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
next
end event

event pcd_new;call super::pcd_new;STRING   ls_cod_resp_divisione

SELECT mansionari.cod_resp_divisione   
INTO   :ls_cod_resp_divisione 
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Nuova Lista","Attenzione!! all'utente corrispondono più mansionari, oppure non esiste il mansionario dell'utente", StopSign!)
	pcca.error = c_fatal
end if

setitem(getrow(), "emesso_da" , ls_cod_resp_divisione)
setitem(getrow(), "data_emissione", datetime(today()))
end event

type gb_1 from groupbox within w_tes_autovalutazione
integer x = 2491
integer width = 549
integer height = 420
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Operazioni"
end type

type gb_2 from groupbox within w_tes_autovalutazione
integer x = 1920
integer width = 549
integer height = 420
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza"
end type

