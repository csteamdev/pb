﻿$PBExportHeader$w_seleziona_non_conformita.srw
forward
global type w_seleziona_non_conformita from w_cs_xx_risposta
end type
type em_a_data from editmask within w_seleziona_non_conformita
end type
type em_da_data from editmask within w_seleziona_non_conformita
end type
type st_4 from statictext within w_seleziona_non_conformita
end type
type cb_conferma from commandbutton within w_seleziona_non_conformita
end type
type cb_annulla from commandbutton within w_seleziona_non_conformita
end type
type cb_visualizza from commandbutton within w_seleziona_non_conformita
end type
type dw_elenco_nc_az_corr from uo_dw_main within w_seleziona_non_conformita
end type
type st_1 from statictext within w_seleziona_non_conformita
end type
type st_2 from statictext within w_seleziona_non_conformita
end type
type st_3 from statictext within w_seleziona_non_conformita
end type
type dw_elenco_nc_az_corr_sel from datawindow within w_seleziona_non_conformita
end type
end forward

global type w_seleziona_non_conformita from w_cs_xx_risposta
integer width = 2779
integer height = 1744
string title = "Seleziona Non Conformita"
em_a_data em_a_data
em_da_data em_da_data
st_4 st_4
cb_conferma cb_conferma
cb_annulla cb_annulla
cb_visualizza cb_visualizza
dw_elenco_nc_az_corr dw_elenco_nc_az_corr
st_1 st_1
st_2 st_2
st_3 st_3
dw_elenco_nc_az_corr_sel dw_elenco_nc_az_corr_sel
end type
global w_seleziona_non_conformita w_seleziona_non_conformita

on w_seleziona_non_conformita.create
int iCurrent
call super::create
this.em_a_data=create em_a_data
this.em_da_data=create em_da_data
this.st_4=create st_4
this.cb_conferma=create cb_conferma
this.cb_annulla=create cb_annulla
this.cb_visualizza=create cb_visualizza
this.dw_elenco_nc_az_corr=create dw_elenco_nc_az_corr
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.dw_elenco_nc_az_corr_sel=create dw_elenco_nc_az_corr_sel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_a_data
this.Control[iCurrent+2]=this.em_da_data
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.cb_conferma
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.cb_visualizza
this.Control[iCurrent+7]=this.dw_elenco_nc_az_corr
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.st_2
this.Control[iCurrent+10]=this.st_3
this.Control[iCurrent+11]=this.dw_elenco_nc_az_corr_sel
end on

on w_seleziona_non_conformita.destroy
call super::destroy
destroy(this.em_a_data)
destroy(this.em_da_data)
destroy(this.st_4)
destroy(this.cb_conferma)
destroy(this.cb_annulla)
destroy(this.cb_visualizza)
destroy(this.dw_elenco_nc_az_corr)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.dw_elenco_nc_az_corr_sel)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_nc_az_corr.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)


end event

event open;call super::open;em_a_data.text = string(today())
em_da_data.text = string(RelativeDate(Today(),-30))
end event

type em_a_data from editmask within w_seleziona_non_conformita
integer x = 891
integer y = 40
integer width = 334
integer height = 64
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type em_da_data from editmask within w_seleziona_non_conformita
integer x = 251
integer y = 40
integer width = 334
integer height = 64
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_4 from statictext within w_seleziona_non_conformita
integer x = 686
integer y = 40
integer width = 224
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "A data:"
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_seleziona_non_conformita
integer x = 2354
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;long ll_t,ll_num_righe


ll_num_righe = dw_elenco_nc_az_corr_sel.rowcount() 

for ll_t = 1 to ll_num_righe
	s_cs_xx.parametri.parametro_d_1_a[ll_t] = dw_elenco_nc_az_corr_sel.getitemnumber(ll_t,"anno_non_conf")
	s_cs_xx.parametri.parametro_d_2_a[ll_t] = dw_elenco_nc_az_corr_sel.getitemnumber(ll_t,"num_non_conf")
next

 
close(parent)
end event

type cb_annulla from commandbutton within w_seleziona_non_conformita
integer x = 1966
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(parent)
end event

type cb_visualizza from commandbutton within w_seleziona_non_conformita
integer x = 1326
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Visualizza"
end type

event clicked;parent.triggerevent("pc_retrieve")
end event

type dw_elenco_nc_az_corr from uo_dw_main within w_seleziona_non_conformita
integer x = 23
integer y = 240
integer width = 1691
integer height = 1280
integer taborder = 30
string dataobject = "d_azioni_corr_non_conf"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ldt_da_data, ldt_a_data


if isnull(parent.em_da_data.text) or (parent.em_da_data.text = "") then
	g_mb.messagebox("OMNIA","Attenzione: impostare la data di partenza!",stopsign!)
	return -1
end if

if isnull(parent.em_a_data.text) or (parent.em_a_data.text = "") then
	g_mb.messagebox("OMNIA","Attenzione: impostare la data di finale!",stopsign!)
	return -1
end if

ldt_da_data = datetime(date(em_da_data.text),time(00:00:00))
ldt_a_data = datetime(date(em_a_data.text),time(00:00:00))

l_Error = Retrieve(s_cs_xx.cod_azienda, ldt_da_data, ldt_a_data)
end event

event doubleclicked;call super::doubleclicked;long ll_num_non_conf, ll_riga
integer li_anno_non_conf

li_anno_non_conf = getitemnumber(row,"anno_non_conf")
ll_num_non_conf = getitemnumber(row,"num_non_conf")

ll_riga = dw_elenco_nc_az_corr_sel.insertrow(0)

dw_elenco_nc_az_corr_sel.setitem(ll_riga,"anno_non_conf",li_anno_non_conf)
dw_elenco_nc_az_corr_sel.setitem(ll_riga,"num_non_conf",ll_num_non_conf)


end event

type st_1 from statictext within w_seleziona_non_conformita
integer x = 23
integer y = 40
integer width = 224
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Da data:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_seleziona_non_conformita
integer x = 23
integer y = 160
integer width = 946
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Elenco complessivo Non Conformità"
boolean focusrectangle = false
end type

type st_3 from statictext within w_seleziona_non_conformita
integer x = 1760
integer y = 160
integer width = 914
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Elenco Non Conf. selezionate"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_elenco_nc_az_corr_sel from datawindow within w_seleziona_non_conformita
integer x = 1760
integer y = 240
integer width = 960
integer height = 1280
integer taborder = 20
string title = "none"
string dataobject = "d_azioni_corr_non_conf_sel"
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;DeleteRow(row) 
end event

