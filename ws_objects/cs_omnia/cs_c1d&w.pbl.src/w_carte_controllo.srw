﻿$PBExportHeader$w_carte_controllo.srw
$PBExportComments$Window carte_controllo
forward
global type w_carte_controllo from w_cs_xx_principale
end type
type dw_carte_controllo_lista from uo_cs_xx_dw within w_carte_controllo
end type
type cb_2 from cb_apri_manuale within w_carte_controllo
end type
type dw_carte_controllo_dettaglio from uo_cs_xx_dw within w_carte_controllo
end type
type cb_formule from commandbutton within w_carte_controllo
end type
end forward

global type w_carte_controllo from w_cs_xx_principale
integer width = 2158
integer height = 1884
string title = "Carte di Controllo"
dw_carte_controllo_lista dw_carte_controllo_lista
cb_2 cb_2
dw_carte_controllo_dettaglio dw_carte_controllo_dettaglio
cb_formule cb_formule
end type
global w_carte_controllo w_carte_controllo

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_carte_controllo_dettaglio,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
					  "and anag_prodotti.cod_prodotto in(select cod_prodotto from tab_test_prodotti)")

f_PO_LoadDDDW_DW(dw_carte_controllo_dettaglio,"cod_test",sqlca,&
				    							   	    "tab_test","cod_test","des_test","")
end on

event pc_setwindow;call super::pc_setwindow;dw_carte_controllo_lista.set_dw_key("cod_azienda")
dw_carte_controllo_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_carte_controllo_dettaglio.set_dw_options(sqlca,dw_carte_controllo_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_carte_controllo_lista



end event

on w_carte_controllo.create
int iCurrent
call super::create
this.dw_carte_controllo_lista=create dw_carte_controllo_lista
this.cb_2=create cb_2
this.dw_carte_controllo_dettaglio=create dw_carte_controllo_dettaglio
this.cb_formule=create cb_formule
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carte_controllo_lista
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.dw_carte_controllo_dettaglio
this.Control[iCurrent+4]=this.cb_formule
end on

on w_carte_controllo.destroy
call super::destroy
destroy(this.dw_carte_controllo_lista)
destroy(this.cb_2)
destroy(this.dw_carte_controllo_dettaglio)
destroy(this.cb_formule)
end on

event pc_new;call super::pc_new;dw_carte_controllo_lista.setitem(dw_carte_controllo_lista.getrow(),"data_carta",datetime(today()))
end event

type dw_carte_controllo_lista from uo_cs_xx_dw within w_carte_controllo
integer x = 23
integer y = 20
integer width = 2080
integer height = 360
integer taborder = 20
string dataobject = "d_carte_controllo_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_view;call super::pcd_view;dw_carte_controllo_dettaglio.object.b_ricerca_prodotto.enabled=false
end event

event pcd_new;call super::pcd_new;dw_carte_controllo_dettaglio.object.b_ricerca_prodotto.enabled=true
end event

event pcd_modify;call super::pcd_modify;dw_carte_controllo_dettaglio.object.b_ricerca_prodotto.enabled=true
end event

event pcd_delete;call super::pcd_delete;dw_carte_controllo_dettaglio.object.b_ricerca_prodotto.enabled=false
end event

type cb_2 from cb_apri_manuale within w_carte_controllo
integer x = 2446
integer y = 20
integer height = 80
integer taborder = 30
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLB")
end on

type dw_carte_controllo_dettaglio from uo_cs_xx_dw within w_carte_controllo
integer x = 23
integer y = 400
integer width = 2080
integer height = 1260
integer taborder = 40
string dataobject = "d_carte_controllo_dett"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
	
	choose case i_colname
		
		case "cod_prodotto" 
			string ls_tipo_carta
			
			ls_tipo_carta = getitemstring(getrow(),"tipo_carta")
			
			dw_carte_controllo_dettaglio.change_dw_current()
			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_s_1 = "cod_test"
			s_cs_xx.parametri.parametro_s_2 = "data_validita"
			s_cs_xx.parametri.parametro_s_10 = i_coltext
			s_cs_xx.parametri.parametro_s_11 = ls_tipo_carta			
			window_open(w_scelta_test_prodotto,0)

		case "tipo_carta" 
			setitem(getrow(),"cod_prodotto","")
			setitem(getrow(),"cod_test","")
			g_mb.messagebox("Omnia","Attenzione, reinserire il prodotto e il test",exclamation!)
	end choose		
end if
end event

event buttonclicked;call super::buttonclicked;dw_carte_controllo_dettaglio.object.b_ricerca_prodotto
choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto( dw_carte_controllo_dettaglio,"cod_prodotto")
end choose
end event

type cb_formule from commandbutton within w_carte_controllo
integer x = 1737
integer y = 1680
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Formule"
end type

event clicked;s_cs_xx.parametri.parametro_s_15 = "CARTE_CONTROLLO"
s_cs_xx.parametri.parametro_dw_1 = dw_carte_controllo_lista
window_open(w_formule,-1)

end event

