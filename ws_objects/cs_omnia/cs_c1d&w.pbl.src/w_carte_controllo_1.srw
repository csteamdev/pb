﻿$PBExportHeader$w_carte_controllo_1.srw
$PBExportComments$Window carte di controllo limitata al prodotto e al test predefinito dal dettaglio campionamenti
forward
global type w_carte_controllo_1 from w_cs_xx_risposta
end type
type dw_carte_controllo_lista from uo_cs_xx_dw within w_carte_controllo_1
end type
type dw_carte_controllo_dettaglio from uo_cs_xx_dw within w_carte_controllo_1
end type
type cb_1 from commandbutton within w_carte_controllo_1
end type
type cb_2 from commandbutton within w_carte_controllo_1
end type
type cb_3 from commandbutton within w_carte_controllo_1
end type
type cb_4 from commandbutton within w_carte_controllo_1
end type
type cb_procedure from cb_apri_manuale within w_carte_controllo_1
end type
end forward

global type w_carte_controllo_1 from w_cs_xx_risposta
int Width=2204
int Height=1741
boolean TitleBar=true
string Title="Carte di Controllo Test-Prodotto"
dw_carte_controllo_lista dw_carte_controllo_lista
dw_carte_controllo_dettaglio dw_carte_controllo_dettaglio
cb_1 cb_1
cb_2 cb_2
cb_3 cb_3
cb_4 cb_4
cb_procedure cb_procedure
end type
global w_carte_controllo_1 w_carte_controllo_1

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_carte_controllo_lista.set_dw_options(sqlca,c_nulldw,c_scrollparent,c_default)
dw_carte_controllo_dettaglio.set_dw_options(sqlca,dw_carte_controllo_lista,c_sharedata+c_scrollparent,c_default)


end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_carte_controllo_dettaglio,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_carte_controllo_dettaglio,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

end on

on w_carte_controllo_1.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_carte_controllo_lista=create dw_carte_controllo_lista
this.dw_carte_controllo_dettaglio=create dw_carte_controllo_dettaglio
this.cb_1=create cb_1
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_4=create cb_4
this.cb_procedure=create cb_procedure
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_carte_controllo_lista
this.Control[iCurrent+2]=dw_carte_controllo_dettaglio
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=cb_2
this.Control[iCurrent+5]=cb_3
this.Control[iCurrent+6]=cb_4
this.Control[iCurrent+7]=cb_procedure
end on

on w_carte_controllo_1.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_carte_controllo_lista)
destroy(this.dw_carte_controllo_dettaglio)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_4)
destroy(this.cb_procedure)
end on

type dw_carte_controllo_lista from uo_cs_xx_dw within w_carte_controllo_1
int X=23
int Y=21
int Width=2126
int Height=361
int TabOrder=10
string DataObject="d_carte_controllo_lista_1"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
	
   string ls_flag_calcolata
	
	ls_flag_calcolata = getitemstring(getrow(),"flag_calcolata")
	
	if  ls_flag_calcolata="S" then
		cb_1.enabled = false
		cb_2.enabled = true
	else
		cb_1.enabled = true
		cb_2.enabled = false
	end if

end if
end on

event pcd_retrieve;call super::pcd_retrieve;LONG     l_Error
string   ls_cod_prodotto,ls_cod_test
datetime ld_data_validita
	
ld_data_validita = datetime(today())
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
ls_cod_test = s_cs_xx.parametri.parametro_s_2

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto,ls_cod_test,ld_data_validita)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_carte_controllo_dettaglio from uo_cs_xx_dw within w_carte_controllo_1
int X=1
int Y=381
int Width=2149
int Height=1141
int TabOrder=30
string DataObject="d_carte_controllo_dett_1"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_1 from commandbutton within w_carte_controllo_1
int X=1006
int Y=1541
int Width=366
int Height=81
int TabOrder=40
string Text="Calcola &Lim."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_num_sottogruppi,ll_num_osservazioni,ll_quantita,ll_quantita_effettiva
long ll_inizio_num_registrazione,ll_fine_num_registrazione,ll_anno_reg_campionamenti
long ll_num_reg_campionamenti,ll_prog_riga_campionamenti
long ll_num_sottogruppi_effettivo, ll_t, ll_inf,ll_fif,ll_grandezza_totale_campione
double ldd_sommatoria,ldd_media_sottogruppo[],ldd_valore_max,ldd_valore_min,ldd_escursione_sottogruppo[]
double ldd_sommatoria_medie,ldd_sommatoria_escursioni,ldd_media_generale,ldd_media_escursioni
double ldd_a2,ldd_d3,ldd_d4,ldd_d2m,ldd_limite_inferiore_medie,ldd_limite_superiore_medie,ldd_limite_superiore_escursioni,ldd_limite_inferiore_escursioni
double ldd_valore_centrale_xmr,ldd_limite_inferiore_xmr,ldd_limite_superiore_xmr
double ldd_sommatoria_xmr,ldd_escursione_xmr_sottogruppo[],ldd_valore_xmr[],ldd_sommatoria_escursioni_xmr
double ldd_p_medio,ldd_lscnp,ldd_lcnp,ldd_licnp
double ldd_d_medio,ldd_lscd,ldd_lcd,ldd_licd
datetime ld_data_carta
string ls_cod_carta,ls_tipo_carta

setpointer(hourglass!)

ll_num_sottogruppi = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"num_sottogruppi")
ll_num_osservazioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"num_osservazioni")
ld_data_carta = dw_carte_controllo_dettaglio.getitemdatetime(dw_carte_controllo_dettaglio.getrow(),"data_carta")
ls_cod_carta = dw_carte_controllo_dettaglio.getitemstring(dw_carte_controllo_dettaglio.getrow(),"cod_carta")
ls_tipo_carta = dw_carte_controllo_dettaglio.getitemstring(dw_carte_controllo_dettaglio.getrow(),"tipo_carta")

select coefficiente_1 into:ldd_a2
from tab_costanti
where strumento_test = 'A2'
and valore_rif = :ll_num_osservazioni;

select coefficiente_1 into:ldd_d3
from tab_costanti
where strumento_test = 'D3'
and valore_rif = :ll_num_osservazioni;

select coefficiente_1 into:ldd_d4
from tab_costanti
where strumento_test = 'D4'
and valore_rif = :ll_num_osservazioni;

select coefficiente_1 into:ldd_d2m
from tab_costanti
where strumento_test = 'D2M'
and valore_rif = :ll_num_osservazioni;

ll_quantita = ll_num_sottogruppi * ll_num_osservazioni
ll_quantita_effettiva = long(s_cs_xx.parametri.parametro_s_3)

if ll_quantita > ll_quantita_effettiva then
	g_mb.messagebox("Omnia","La numerosità campionaria è inferiore a quella richiesta dalla carta di controllo")
end if	

setpointer(hourglass!)

ll_anno_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_4)
ll_num_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_5)
ll_prog_riga_campionamenti = long(s_cs_xx.parametri.parametro_s_6)

select max(num_registrazione) into:ll_fine_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

select min(num_registrazione) into:ll_inizio_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

ll_num_sottogruppi_effettivo = int(ll_quantita_effettiva/ll_num_osservazioni)
g_mb.messagebox("Omnia","I limiti sono riferiti a " + string(ll_num_sottogruppi_effettivo) + " su " + string(ll_num_sottogruppi) + " sottogruppi")

setpointer(hourglass!)

ll_inf = ll_inizio_num_registrazione 
ll_fif = ll_inizio_num_registrazione + ll_num_osservazioni -1

UPDATE collaudi  
SET data_carta = :ld_data_carta,   
    cod_carta  = :ls_cod_carta  
WHERE collaudi.cod_azienda=:s_cs_xx.cod_azienda   
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
and 	num_registrazione  between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;


choose case ls_tipo_carta

	case "M","S"
		for ll_t = 1 to ll_num_sottogruppi_effettivo
	
			select sum(risultato) into:ldd_sommatoria
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			ldd_media_sottogruppo[ll_t] = ldd_sommatoria/ll_num_osservazioni
			
			select max(risultato) into:ldd_valore_max
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			select min(risultato) into:ldd_valore_min
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;

			ldd_escursione_sottogruppo[ll_t] = ldd_valore_max - ldd_valore_min
		
			ll_inf = ll_fif + 1
			ll_fif = ll_fif + ll_num_osservazioni
	
		next

	case "X"
		select sum(risultato) into:ldd_sommatoria_xmr
		from 	collaudi 
		where cod_azienda=:s_cs_xx.cod_azienda 
		and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
		and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
		and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
		and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;

		for ll_t = ll_inizio_num_registrazione to ll_fine_num_registrazione

			select risultato into:ldd_valore_xmr[ll_t - ll_inizio_num_registrazione + 1]
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione = :ll_t;

			if ll_t = ll_inizio_num_registrazione then
				ldd_escursione_xmr_sottogruppo[ll_t - ll_inizio_num_registrazione + 1] = 0
			else
				ldd_escursione_xmr_sottogruppo[ll_t - ll_inizio_num_registrazione + 1] = abs(ldd_valore_xmr[ll_t - ll_inizio_num_registrazione + 1] - ldd_valore_xmr[ll_t - ll_inizio_num_registrazione])
			end if

		next

	case "G"
		for ll_t = 1 to ll_num_sottogruppi_effettivo
	
			select sum(risultato) into:ldd_sommatoria
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			ldd_media_sottogruppo[ll_t] = ldd_sommatoria/ll_num_osservazioni
			
			select max(risultato) into:ldd_valore_max
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			select min(risultato) into:ldd_valore_min
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;

			ldd_escursione_sottogruppo[ll_t] = ldd_valore_max - ldd_valore_min

			if ll_t = 1 then
				ldd_escursione_xmr_sottogruppo[ll_t] = 0
			else
				ldd_escursione_xmr_sottogruppo[ll_t] = abs(ldd_media_sottogruppo[ll_t] - ldd_media_sottogruppo[ll_t - 1])
			end if

			ll_inf = ll_fif + 1
			ll_fif = ll_fif + ll_num_osservazioni
		next	

	case "P"
		select count(*) into:ldd_sommatoria
		from 	collaudi
		where cod_azienda=:s_cs_xx.cod_azienda 
		and   flag_esito ='N'
		and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
		and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
		and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
		and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione ;
	
	case "D"
		select count(*) into:ldd_sommatoria
		from 	collaudi
		where cod_azienda=:s_cs_xx.cod_azienda 
		and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
		and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
		and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
		and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione 
		and   cod_errore is not null;

		select count(distinct cod_pezzo) into:ll_grandezza_totale_campione
		from 	collaudi
		where cod_azienda=:s_cs_xx.cod_azienda 
		and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
		and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
		and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
		and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;
				
end choose	

	ldd_sommatoria_medie = 0
	ldd_sommatoria_escursioni = 0

	
choose case ls_tipo_carta

	case "M"
		for ll_t = 1 to ll_num_sottogruppi_effettivo
			ldd_sommatoria_medie = ldd_sommatoria_medie + ldd_media_sottogruppo[ll_t]	
			ldd_sommatoria_escursioni = ldd_sommatoria_escursioni + ldd_escursione_sottogruppo[ll_t]
		next

		ldd_media_generale = ldd_sommatoria_medie/ll_num_sottogruppi_effettivo
		ldd_media_escursioni = ldd_sommatoria_escursioni/ll_num_sottogruppi_effettivo
		ldd_limite_superiore_medie = ldd_media_generale + ldd_a2*ldd_media_escursioni
		ldd_limite_inferiore_medie = ldd_media_generale - ldd_a2*ldd_media_escursioni
		ldd_limite_superiore_escursioni = ldd_d4 * ldd_media_escursioni
		ldd_limite_inferiore_escursioni = ldd_d3 * ldd_media_escursioni
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie",ldd_media_generale)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie",ldd_limite_inferiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie",ldd_limite_superiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_escursioni",ldd_media_escursioni)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_escursioni",ldd_limite_inferiore_escursioni)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_escursioni",ldd_limite_superiore_escursioni)

	case "S"
		for ll_t = 1 to ll_num_sottogruppi_effettivo
			ldd_sommatoria_medie = ldd_sommatoria_medie + ldd_media_sottogruppo[ll_t]	
			ldd_sommatoria_escursioni = ldd_sommatoria_escursioni + ldd_escursione_sottogruppo[ll_t]
		next
		ldd_media_generale = ldd_sommatoria_medie/ll_num_sottogruppi_effettivo
		ldd_media_escursioni = ldd_sommatoria_escursioni/ll_num_sottogruppi_effettivo
		ldd_limite_superiore_medie = ldd_media_generale + 3*ldd_media_escursioni/ldd_d2m
		ldd_limite_inferiore_medie = ldd_media_generale - 3*ldd_media_escursioni/ldd_d2m
		ldd_limite_superiore_escursioni = ldd_d4 * ldd_media_escursioni
		ldd_limite_inferiore_escursioni = ldd_d3 * ldd_media_escursioni
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie",ldd_media_generale)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie",ldd_limite_inferiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie",ldd_limite_superiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_escursioni",ldd_media_escursioni)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_escursioni",ldd_limite_inferiore_escursioni)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_escursioni",ldd_limite_superiore_escursioni)

	case "X"
		select coefficiente_1 into:ldd_a2
		from tab_costanti
		where strumento_test = 'A2'
		and valore_rif = 2;

		select coefficiente_1 into:ldd_d3
		from tab_costanti
		where strumento_test = 'D3'
		and valore_rif = 2;

		select coefficiente_1 into:ldd_d4
		from tab_costanti
		where strumento_test = 'D4'
		and valore_rif = 2;

		select coefficiente_1 into:ldd_d2m
		from tab_costanti
		where strumento_test = 'D2M'
		and valore_rif = 2;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
			ldd_sommatoria_escursioni_xmr = ldd_sommatoria_escursioni_xmr + ldd_escursione_xmr_sottogruppo[ll_t]
		next

		ldd_media_generale = ldd_sommatoria_xmr/ll_num_sottogruppi_effettivo
		ldd_valore_centrale_xmr = ldd_sommatoria_escursioni_xmr/(ll_num_sottogruppi_effettivo - 1)
		ldd_limite_superiore_medie = ldd_media_generale + 3*ldd_valore_centrale_xmr/ldd_d2m
		ldd_limite_inferiore_medie = ldd_media_generale - 3*ldd_valore_centrale_xmr/ldd_d2m
		ldd_limite_superiore_xmr = ldd_d4 * ldd_valore_centrale_xmr
		ldd_limite_inferiore_xmr = 0

		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie",ldd_media_generale)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie",ldd_limite_inferiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie",ldd_limite_superiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_xmr",ldd_valore_centrale_xmr)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_xmr",ldd_limite_inferiore_xmr)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_xmr",ldd_limite_superiore_xmr)			

	case "G"
		select coefficiente_1 into:ldd_d4
		from tab_costanti
		where strumento_test = 'D4'
		and valore_rif = 2;

		select coefficiente_1 into:ldd_d2m
		from tab_costanti
		where strumento_test = 'D2M'
		and valore_rif = 2;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
			ldd_sommatoria_escursioni_xmr = ldd_sommatoria_escursioni_xmr + ldd_escursione_xmr_sottogruppo[ll_t]
			ldd_sommatoria_medie = ldd_sommatoria_medie + ldd_media_sottogruppo[ll_t]	
			ldd_sommatoria_escursioni = ldd_sommatoria_escursioni + ldd_escursione_sottogruppo[ll_t]
		next
				
		ldd_media_generale = ldd_sommatoria_medie/ll_num_sottogruppi_effettivo
		ldd_media_escursioni = ldd_sommatoria_escursioni/ll_num_sottogruppi_effettivo
		ldd_valore_centrale_xmr = ldd_sommatoria_escursioni_xmr/(ll_num_sottogruppi_effettivo - 1)
		ldd_limite_superiore_medie = ldd_media_generale + 3*ldd_valore_centrale_xmr/ldd_d2m
		ldd_limite_inferiore_medie = ldd_media_generale - 3*ldd_valore_centrale_xmr/ldd_d2m
		ldd_limite_superiore_xmr = ldd_d4 * ldd_valore_centrale_xmr
		ldd_limite_inferiore_xmr = 0
		
		select coefficiente_1 into:ldd_d4
		from tab_costanti
		where strumento_test = 'D4'
		and valore_rif = :ll_num_osservazioni;

		ldd_limite_superiore_escursioni = ldd_d4 * ldd_media_escursioni
		ldd_limite_inferiore_escursioni = ldd_d3 * ldd_media_escursioni
		
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie",ldd_media_generale)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie",ldd_limite_inferiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie",ldd_limite_superiore_medie)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_escursioni",ldd_media_escursioni)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_escursioni",ldd_limite_inferiore_escursioni)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_escursioni",ldd_limite_superiore_escursioni)		
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_xmr",ldd_valore_centrale_xmr)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_xmr",ldd_limite_inferiore_xmr)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_xmr",ldd_limite_superiore_xmr)			

	case "P"
		ldd_p_medio = ldd_sommatoria/ll_quantita_effettiva

		ldd_lcnp = ll_num_osservazioni*ldd_p_medio
		ldd_lscnp = ldd_lcnp + 3*sqrt(ldd_lcnp*(1 - ldd_p_medio))		
		ldd_licnp = ldd_lcnp - 3*sqrt(ldd_lcnp*(1 - ldd_p_medio))		

      if ldd_licnp <0 then ldd_licnp = 0

		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie",ldd_lcnp)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie",ldd_licnp)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie",ldd_lscnp)

	case "D"
		ldd_d_medio = ldd_sommatoria/ll_grandezza_totale_campione
		
		ldd_lcd = ldd_d_medio
		ldd_lscd = ldd_lcd + 3*sqrt(ldd_lcd)		
		ldd_licd = ldd_lcd - 3*sqrt(ldd_lcd)		
      
      if ldd_licd <0 then ldd_licd=0
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie",ldd_lcd)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie",ldd_licd)
		dw_carte_controllo_dettaglio.setitem(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie",ldd_lscd)


end choose

dw_carte_controllo_lista.setitem(dw_carte_controllo_lista.getrow(),"flag_calcolata","S")

cb_2.enabled = true
cb_1.enabled = false

setpointer(arrow!)
end event

type cb_2 from commandbutton within w_carte_controllo_1
int X=1395
int Y=1541
int Width=366
int Height=81
int TabOrder=60
string Text="&Grafici Stat."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_num_sottogruppi,ll_num_osservazioni,ll_quantita,ll_quantita_effettiva
long ll_inizio_num_registrazione,ll_fine_num_registrazione,ll_anno_reg_campionamenti
long ll_num_reg_campionamenti,ll_prog_riga_campionamenti,ll_prog
long ll_num_sottogruppi_effettivo, ll_t, ll_inf,ll_fif,ll_num_pezzo[],ll_num_pezzi
double ldd_sommatoria,ldd_media_sottogruppo[],ldd_valore_max,ldd_valore_min,ldd_escursione_sottogruppo[]
double ldd_sommatoria_medie,ldd_sommatoria_escursioni,ldd_media_generale,ldd_media_escursioni
double ldd_a2,ldd_d3,ldd_d4,ldd_d2m,ldd_limite_inferiore_medie,ldd_limite_superiore_medie,ldd_limite_superiore_escursioni,ldd_limite_inferiore_escursioni
double ldd_valore_centrale_xmr,ldd_limite_inferiore_xmr,ldd_limite_superiore_xmr
double ldd_sommatoria_xmr,ldd_escursione_xmr_sottogruppo[],ldd_valore_xmr[],ldd_sommatoria_escursioni_xmr
double ldd_p[],ldd_d[]
datetime ld_data_carta
string ls_cod_carta,ls_tipo_carta,ls_cod_errore

setpointer(hourglass!)

ll_num_sottogruppi = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"num_sottogruppi")
ll_num_osservazioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"num_osservazioni")
ld_data_carta = dw_carte_controllo_dettaglio.getitemdatetime(dw_carte_controllo_dettaglio.getrow(),"data_carta")
ls_cod_carta = dw_carte_controllo_dettaglio.getitemstring(dw_carte_controllo_dettaglio.getrow(),"cod_carta")
ls_tipo_carta = dw_carte_controllo_dettaglio.getitemstring(dw_carte_controllo_dettaglio.getrow(),"tipo_carta")

ll_quantita = ll_num_sottogruppi * ll_num_osservazioni
ll_quantita_effettiva = long(s_cs_xx.parametri.parametro_s_3)

if ll_quantita > ll_quantita_effettiva then
	g_mb.messagebox("Omnia","La numerosità campionaria è inferiore a quella richiesta dalla carta di controllo")
end if	

setpointer(hourglass!)

ll_anno_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_4)
ll_num_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_5)
ll_prog_riga_campionamenti = long(s_cs_xx.parametri.parametro_s_6)

select max(num_registrazione) into:ll_fine_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

select min(num_registrazione) into:ll_inizio_num_registrazione 
from 	 collaudi 
where  cod_azienda=:s_cs_xx.cod_azienda 
and 	 anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	 num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	 prog_riga_campionamenti=:ll_prog_riga_campionamenti;

ll_num_sottogruppi_effettivo = int(ll_quantita_effettiva/ll_num_osservazioni)
g_mb.messagebox("Omnia","I limiti sono riferiti a " + string(ll_num_sottogruppi_effettivo) + " su " + string(ll_num_sottogruppi) + " sottogruppi")

setpointer(hourglass!)

ll_inf = ll_inizio_num_registrazione 
ll_fif = ll_inizio_num_registrazione + ll_num_osservazioni -1

choose case ls_tipo_carta

	case "M","S"
		for ll_t = 1 to ll_num_sottogruppi_effettivo
	
			select sum(risultato) into:ldd_sommatoria
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			ldd_media_sottogruppo[ll_t] = ldd_sommatoria/ll_num_osservazioni
			
			select max(risultato) into:ldd_valore_max
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			select min(risultato) into:ldd_valore_min
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;

			ldd_escursione_sottogruppo[ll_t] = ldd_valore_max - ldd_valore_min
		
			ll_inf = ll_fif + 1
			ll_fif = ll_fif + ll_num_osservazioni
	
		next

	case "X"
		select sum(risultato) into:ldd_sommatoria_xmr
		from 	collaudi 
		where cod_azienda=:s_cs_xx.cod_azienda 
		and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
		and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
		and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
		and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;

		for ll_t = ll_inizio_num_registrazione to ll_fine_num_registrazione

			select risultato into:ldd_valore_xmr[ll_t - ll_inizio_num_registrazione + 1]
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione = :ll_t;

			if ll_t = ll_inizio_num_registrazione then
				ldd_escursione_xmr_sottogruppo[ll_t - ll_inizio_num_registrazione + 1] = 0
			else
				ldd_escursione_xmr_sottogruppo[ll_t - ll_inizio_num_registrazione + 1] = abs(ldd_valore_xmr[ll_t - ll_inizio_num_registrazione + 1] - ldd_valore_xmr[ll_t - ll_inizio_num_registrazione])
			end if

		next

	case "G"
		for ll_t = 1 to ll_num_sottogruppi_effettivo
	
			select sum(risultato) into:ldd_sommatoria
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			ldd_media_sottogruppo[ll_t] = ldd_sommatoria/ll_num_osservazioni
			
			select max(risultato) into:ldd_valore_max
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			select min(risultato) into:ldd_valore_min
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;

			ldd_escursione_sottogruppo[ll_t] = ldd_valore_max - ldd_valore_min
				
			if ll_t = 1 then
				ldd_escursione_xmr_sottogruppo[ll_t] = 0
			else
				ldd_escursione_xmr_sottogruppo[ll_t] = abs(ldd_media_sottogruppo[ll_t] - ldd_media_sottogruppo[ll_t - 1])
			end if
		
			ll_inf = ll_fif + 1
			ll_fif = ll_fif + ll_num_osservazioni
	
		next

	case "P"
		for ll_t = 1 to ll_num_sottogruppi_effettivo
			select count(*) into:ldd_p[ll_t]
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and	flag_esito='N'
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inf and :ll_fif;
	
			ll_inf = ll_fif + 1
			ll_fif = ll_fif + ll_num_osservazioni
	
		next
	
	case "D"
		ll_prog = 1
		
      select count(distinct cod_pezzo) into:ll_num_pezzi
		from 	collaudi
		where cod_azienda=:s_cs_xx.cod_azienda 
		and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
		and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
		and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
		and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;

      ll_num_pezzo[ll_num_pezzi] = 0
      
		declare righe_pezzi cursor for select count(*) from collaudi &
		where cod_azienda = :s_cs_xx.cod_azienda &
		and   anno_reg_campionamenti  = :ll_anno_reg_campionamenti &
	   and   num_reg_campionamenti   = :ll_num_reg_campionamenti &
		and   prog_riga_campionamenti = :ll_prog_riga_campionamenti &
		and   num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione & 
      and   cod_errore is not null
		group by cod_pezzo;
   	
      open righe_pezzi;

  	   do while 1 = 1
		  fetch righe_pezzi into :ll_num_pezzo[ll_prog];
   	  
		  if (sqlca.sqlcode = 100) or (sqlca.sqlcode=-1) then exit
		  
        if sqlca.sqlcode<>0 then
			 g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)
		  end if
		  
        ll_prog++		

		loop

		close righe_pezzi;
		 
end choose	

	ldd_sommatoria_medie = 0
	ldd_sommatoria_escursioni = 0


choose case ls_tipo_carta

	case "M","S"
		ldd_media_generale = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie")
		ldd_limite_inferiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie")
		ldd_limite_superiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie")
		ldd_media_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_escursioni")
		ldd_limite_inferiore_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_escursioni")
		ldd_limite_superiore_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_escursioni")

		delete  from tab_grafi;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Generale',   
		           :ldd_media_generale )  ;
	
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_media_sottogruppo[ll_t] )  ;

		next

		delete  from tab_grafi_2;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi_2  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Esc.',   
		           :ldd_media_escursioni )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_escursioni )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_escursioni )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_escursione_sottogruppo[ll_t] )  ;

		next
	
		setpointer(arrow!)
		window_open(w_graficocc_1,0)

	case "X"
		ldd_media_generale = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie")
		ldd_limite_inferiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie")
		ldd_limite_superiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie")
		ldd_valore_centrale_xmr = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_xmr")
		ldd_limite_inferiore_xmr = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_xmr")
		ldd_limite_superiore_xmr = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_xmr")

		delete  from tab_grafi;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Generale',   
		           :ldd_media_generale )  ;
	
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_valore_xmr[ll_t] )  ;

		next

		delete  from tab_grafi_2;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi_2  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Esc.',   
		           :ldd_valore_centrale_xmr )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_xmr )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_xmr )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_escursione_xmr_sottogruppo[ll_t] )  ;

		next
	
		setpointer(arrow!)
		window_open(w_graficocc_1,0)

	case "G"
		ldd_media_generale = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie")
		ldd_limite_inferiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie")
		ldd_limite_superiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie")
		ldd_media_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_escursioni")
		ldd_limite_inferiore_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_escursioni")
		ldd_limite_superiore_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_escursioni")
		ldd_valore_centrale_xmr = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_xmr")
		ldd_limite_inferiore_xmr = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_xmr")
		ldd_limite_superiore_xmr = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_xmr")
		
		delete  from tab_grafi;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Generale',   
		           :ldd_media_generale );
	
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_medie );

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_medie );

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_media_sottogruppo[ll_t] )  ;

		next

		delete  from tab_grafi_2;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Esc.',   
		           :ldd_media_escursioni )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_escursioni )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_escursioni )  ;

		  INSERT INTO tab_grafi_2
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_escursione_sottogruppo[ll_t] )  ;

		next

		delete  from tab_grafi_3;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi_3
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Esc.',   
		           :ldd_valore_centrale_xmr );

		  INSERT INTO tab_grafi_3
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_xmr );

		  INSERT INTO tab_grafi_3
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_xmr );

		  INSERT INTO tab_grafi_3
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_escursione_xmr_sottogruppo[ll_t] );

		next
	
		setpointer(arrow!)
		window_open(w_graficocc_2,0)

	case "P"
		ldd_media_generale = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie")
		ldd_limite_inferiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie")
		ldd_limite_superiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie")

		delete  from tab_grafi;

		for ll_t = 1 to ll_num_sottogruppi_effettivo
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Generale',   
		           :ldd_media_generale )  ;
	
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ldd_p[ll_t] )  ;

		next
		setpointer(arrow!)
		window_open(w_graficocc_3,0)

	case "D"
		ldd_media_generale = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie")
		ldd_limite_inferiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie")
		ldd_limite_superiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie")

		delete  from tab_grafi;

		ll_prog = ll_prog -1 

		for ll_t = 1 to ll_num_pezzi
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Media Generale',   
		           :ldd_media_generale )  ;
	
		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Sup.',   
		           :ldd_limite_superiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Lim. Inf.',   
		           :ldd_limite_inferiore_medie )  ;

		  INSERT INTO tab_grafi  
      		   ( cod_valore,   
		           des_valore,   
      		     valore )  
		  VALUES ( :ll_t,   
      		     'Valore',   
		           :ll_num_pezzo[ll_t] )  ;

		next
		setpointer(arrow!)
		window_open(w_graficocc_3,0)

end choose
end event

type cb_3 from commandbutton within w_carte_controllo_1
int X=1783
int Y=1541
int Width=366
int Height=81
int TabOrder=70
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type cb_4 from commandbutton within w_carte_controllo_1
int X=618
int Y=1541
int Width=366
int Height=81
int TabOrder=50
string Text="&Associa C."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti
long ll_inizio_num_registrazione,ll_fine_num_registrazione
datetime ld_data_carta
string ls_cod_carta,ls_tipo_carta

setpointer(hourglass!)

ll_anno_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_4)
ll_num_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_5)
ll_prog_riga_campionamenti = long(s_cs_xx.parametri.parametro_s_6)

ld_data_carta = dw_carte_controllo_dettaglio.getitemdatetime(dw_carte_controllo_dettaglio.getrow(),"data_carta")
ls_cod_carta = dw_carte_controllo_dettaglio.getitemstring(dw_carte_controllo_dettaglio.getrow(),"cod_carta")

select max(num_registrazione) into:ll_fine_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

select min(num_registrazione) into:ll_inizio_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

update collaudi  
set    data_carta = :ld_data_carta,   
       cod_carta  = :ls_cod_carta  
where  collaudi.cod_azienda=:s_cs_xx.cod_azienda   
and 	 anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	 num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	 prog_riga_campionamenti=:ll_prog_riga_campionamenti
and 	 num_registrazione  between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;

commit;

setpointer(arrow!)
end event

type cb_procedure from cb_apri_manuale within w_carte_controllo_1
int X=229
int Y=1541
int TabOrder=20
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLB")
end on

