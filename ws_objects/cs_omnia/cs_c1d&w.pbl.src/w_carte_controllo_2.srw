﻿$PBExportHeader$w_carte_controllo_2.srw
$PBExportComments$Window carte di controllo limitata al prodotto e al test predefinito dal dettaglio campionamenti
forward
global type w_carte_controllo_2 from w_cs_xx_risposta
end type
type dw_carte_controllo_lista from uo_cs_xx_dw within w_carte_controllo_2
end type
type dw_carte_controllo_dettaglio from uo_cs_xx_dw within w_carte_controllo_2
end type
type cb_2 from commandbutton within w_carte_controllo_2
end type
type cb_3 from commandbutton within w_carte_controllo_2
end type
type cb_procedure from cb_apri_manuale within w_carte_controllo_2
end type
end forward

global type w_carte_controllo_2 from w_cs_xx_risposta
int Width=2186
int Height=1741
boolean TitleBar=true
string Title="Carte di Controllo Test-Prodotto"
dw_carte_controllo_lista dw_carte_controllo_lista
dw_carte_controllo_dettaglio dw_carte_controllo_dettaglio
cb_2 cb_2
cb_3 cb_3
cb_procedure cb_procedure
end type
global w_carte_controllo_2 w_carte_controllo_2

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_carte_controllo_lista.set_dw_options(sqlca,c_nulldw,c_nonew + c_nomodify + c_nodelete,c_default)
dw_carte_controllo_dettaglio.set_dw_options(sqlca,dw_carte_controllo_lista,c_sharedata+c_scrollparent,c_default)


end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_carte_controllo_dettaglio,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_carte_controllo_dettaglio,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

end on

on w_carte_controllo_2.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_carte_controllo_lista=create dw_carte_controllo_lista
this.dw_carte_controllo_dettaglio=create dw_carte_controllo_dettaglio
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_procedure=create cb_procedure
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_carte_controllo_lista
this.Control[iCurrent+2]=dw_carte_controllo_dettaglio
this.Control[iCurrent+3]=cb_2
this.Control[iCurrent+4]=cb_3
this.Control[iCurrent+5]=cb_procedure
end on

on w_carte_controllo_2.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_carte_controllo_lista)
destroy(this.dw_carte_controllo_dettaglio)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_procedure)
end on

type dw_carte_controllo_lista from uo_cs_xx_dw within w_carte_controllo_2
int X=1
int Y=1
int Width=2126
int Height=361
int TabOrder=10
string DataObject="d_carte_controllo_lista_2"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_carta
datetime ld_data_carta
	
ls_cod_carta = s_cs_xx.parametri.parametro_s_7
ld_data_carta = s_cs_xx.parametri.parametro_data_1

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_carta,ld_data_carta)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_carte_controllo_dettaglio from uo_cs_xx_dw within w_carte_controllo_2
int X=1
int Y=381
int Width=2126
int Height=1141
int TabOrder=30
string DataObject="d_carte_controllo_dett_1"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_2 from commandbutton within w_carte_controllo_2
int X=1372
int Y=1541
int Width=366
int Height=81
int TabOrder=40
string Text="&Grafici Stat."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_num_sottogruppi,ll_num_osservazioni,ll_quantita,ll_quantita_effettiva
long ll_inizio_num_registrazione,ll_fine_num_registrazione,ll_anno_reg_campionamenti
long ll_num_sottogruppi_effettivo, ll_t, ll_inf,ll_fif,ll_num_reg_campionamenti,ll_prog_riga_campionamenti
double ld_sommatoria,ld_media_sottogruppo[],ld_valore_max,ld_valore_min,ld_escursione_sottogruppo[]
double ld_sommatoria_medie,ld_sommatoria_escursioni,ld_media_generale,ld_media_escursioni
double ld_a2,ld_d3,ld_d4,ld_limite_inferiore_medie,ld_limite_superiore_medie,ld_limite_superiore_escursioni,ld_limite_inferiore_escursioni
datetime ld_data_carta
string ls_cod_carta
setpointer(hourglass!)

ll_num_sottogruppi = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"num_sottogruppi")
ll_num_osservazioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"num_osservazioni")
ld_data_carta = dw_carte_controllo_dettaglio.getitemdatetime(dw_carte_controllo_dettaglio.getrow(),"data_carta")
ls_cod_carta = dw_carte_controllo_dettaglio.getitemstring(dw_carte_controllo_dettaglio.getrow(),"cod_carta")


ll_quantita = ll_num_sottogruppi * ll_num_osservazioni
ll_quantita_effettiva = long(s_cs_xx.parametri.parametro_s_3)

if ll_quantita > ll_quantita_effettiva then
	g_mb.messagebox("Omnia","La numerosità campionaria è inferiore a quella richiesta dalla carta di controllo")
end if	

setpointer(hourglass!)

ll_anno_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_4)
ll_num_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_5)
ll_prog_riga_campionamenti = long(s_cs_xx.parametri.parametro_s_6)

select max(num_registrazione) into:ll_fine_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;


select min(num_registrazione) into:ll_inizio_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

ll_num_sottogruppi_effettivo = int(ll_quantita_effettiva/ll_num_osservazioni)
g_mb.messagebox("Omnia","I limiti sono riferiti a "+string(ll_num_sottogruppi_effettivo)+" su " + string(ll_num_sottogruppi) + " sottogruppi")

setpointer(hourglass!)

ll_inf = ll_inizio_num_registrazione 
ll_fif = ll_inizio_num_registrazione + ll_num_osservazioni -1

for ll_t = 1 to ll_num_sottogruppi_effettivo
	
	select sum(risultato) into:ld_sommatoria
	from 	collaudi 
	where cod_azienda=:s_cs_xx.cod_azienda 
	and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
	and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
	and 	num_registrazione between :ll_inf and :ll_fif;
	
	ld_media_sottogruppo[ll_t] = ld_sommatoria/ll_num_osservazioni
	
	select max(risultato) into:ld_valore_max
	from 	collaudi 
	where cod_azienda=:s_cs_xx.cod_azienda 
	and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
	and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
	and 	num_registrazione between :ll_inf and :ll_fif;
	
	select min(risultato) into:ld_valore_min
	from 	collaudi 
	where cod_azienda=:s_cs_xx.cod_azienda 
	and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
	and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
	and 	num_registrazione between :ll_inf and :ll_fif;

	ld_escursione_sottogruppo[ll_t] = ld_valore_max - ld_valore_min
	ll_inf = ll_fif + 1
	ll_fif = ll_fif + ll_num_osservazioni
	
next


ld_media_generale = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_medie")
ld_limite_inferiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_medie")
ld_limite_superiore_medie = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_medie")
ld_media_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"valore_centrale_escursioni")
ld_limite_inferiore_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_inferiore_escursioni")
ld_limite_superiore_escursioni = dw_carte_controllo_dettaglio.getitemnumber(dw_carte_controllo_dettaglio.getrow(),"limite_superiore_escursioni")

delete  from tab_grafi;

for ll_t = 1 to ll_num_sottogruppi_effettivo
  INSERT INTO tab_grafi  
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Media Generale',   
           :ld_media_generale )  ;

  INSERT INTO tab_grafi  
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Lim. Sup.',   
           :ld_limite_superiore_medie )  ;

  INSERT INTO tab_grafi  
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Lim. Inf.',   
           :ld_limite_inferiore_medie )  ;

  INSERT INTO tab_grafi  
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Valore',   
           :ld_media_sottogruppo[ll_t] )  ;

next

delete  from tab_grafi_2;

for ll_t = 1 to ll_num_sottogruppi_effettivo
  INSERT INTO tab_grafi_2  
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Media Esc.',   
           :ld_media_escursioni )  ;

  INSERT INTO tab_grafi_2
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Lim. Sup.',   
           :ld_limite_superiore_escursioni )  ;

  INSERT INTO tab_grafi_2
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Lim. Inf.',   
           :ld_limite_inferiore_escursioni )  ;

  INSERT INTO tab_grafi_2
         ( cod_valore,   
           des_valore,   
           valore )  
  VALUES ( :ll_t,   
           'Valore',   
           :ld_escursione_sottogruppo[ll_t] )  ;

next
setpointer(arrow!)
window_open(w_graficocc_1,0)
end event

type cb_3 from commandbutton within w_carte_controllo_2
int X=1761
int Y=1541
int Width=366
int Height=81
int TabOrder=50
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type cb_procedure from cb_apri_manuale within w_carte_controllo_2
int X=983
int Y=1541
int TabOrder=20
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLB")
end on

