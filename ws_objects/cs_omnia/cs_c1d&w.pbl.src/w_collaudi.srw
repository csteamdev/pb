﻿$PBExportHeader$w_collaudi.srw
$PBExportComments$Window Collaudi
forward
global type w_collaudi from w_cs_xx_principale
end type
type dw_collaudi_lista from uo_cs_xx_dw within w_collaudi
end type
type dw_tab_test_prodotti_dettaglio_1 from uo_cs_xx_dw within w_collaudi
end type
type mle_1 from multilineedit within w_collaudi
end type
type cb_2 from cb_stock_ricerca within w_collaudi
end type
type dw_collaudi_dettaglio from uo_cs_xx_dw within w_collaudi
end type
type cb_formule from commandbutton within w_collaudi
end type
end forward

global type w_collaudi from w_cs_xx_principale
integer width = 3145
integer height = 1964
string title = "Collaudi"
dw_collaudi_lista dw_collaudi_lista
dw_tab_test_prodotti_dettaglio_1 dw_tab_test_prodotti_dettaglio_1
mle_1 mle_1
cb_2 cb_2
dw_collaudi_dettaglio dw_collaudi_dettaglio
cb_formule cb_formule
end type
global w_collaudi w_collaudi

type variables
string is_cod_prodotto
string is_cod_test
end variables

event pc_setwindow;call super::pc_setwindow;dw_collaudi_lista.set_dw_key("cod_azienda")
dw_collaudi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_collaudi_dettaglio.set_dw_options(sqlca,dw_collaudi_lista,c_sharedata+c_scrollparent,c_default)
dw_tab_test_prodotti_dettaglio_1.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)

iuo_dw_main = dw_collaudi_lista

cb_2.enabled = false
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_collaudi_dettaglio,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

f_PO_LoadDDDW_DW(dw_collaudi_dettaglio,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_collaudi_dettaglio,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_collaudi_dettaglio,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_collaudi_dettaglio,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita",&
                 "tab_difformita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on key;call w_cs_xx_principale::key;if keydown(keyf4!) then
	ShowHelp("\cs_xx\help\cs_help.hlp", Index!)
end if
end on

on w_collaudi.create
int iCurrent
call super::create
this.dw_collaudi_lista=create dw_collaudi_lista
this.dw_tab_test_prodotti_dettaglio_1=create dw_tab_test_prodotti_dettaglio_1
this.mle_1=create mle_1
this.cb_2=create cb_2
this.dw_collaudi_dettaglio=create dw_collaudi_dettaglio
this.cb_formule=create cb_formule
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_collaudi_lista
this.Control[iCurrent+2]=this.dw_tab_test_prodotti_dettaglio_1
this.Control[iCurrent+3]=this.mle_1
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.dw_collaudi_dettaglio
this.Control[iCurrent+6]=this.cb_formule
end on

on w_collaudi.destroy
call super::destroy
destroy(this.dw_collaudi_lista)
destroy(this.dw_tab_test_prodotti_dettaglio_1)
destroy(this.mle_1)
destroy(this.cb_2)
destroy(this.dw_collaudi_dettaglio)
destroy(this.cb_formule)
end on

event pc_new;call super::pc_new;dw_collaudi_lista.setitem(dw_collaudi_lista.getrow(),"data_registrazione",datetime(today()))
end event

type dw_collaudi_lista from uo_cs_xx_dw within w_collaudi
integer x = 23
integer y = 20
integer width = 3063
integer height = 360
integer taborder = 30
string dataobject = "d_collaudi_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx, ll_num_registrazione, ll_anno

ll_anno = year(today())

select max(num_registrazione) into:ll_num_registrazione from collaudi where cod_azienda=:s_cs_xx.cod_azienda and anno_registrazione=:ll_anno;

if isnull(ll_num_registrazione) then
	ll_num_registrazione = 1
else
	ll_num_registrazione++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
      SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
		SetItem(l_Idx, "anno_registrazione", year(today()))
   END IF
NEXT

end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
	cb_2.enabled = false
end if
end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
	cb_2.enabled = false
end if
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
	cb_2.enabled = true
end if
end on

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
	is_cod_prodotto=getitemstring(getrow(),"cod_prodotto")
	is_cod_test=getitemstring(getrow(),"cod_test")
	dw_tab_test_prodotti_dettaglio_1.change_dw_focus(dw_tab_test_prodotti_dettaglio_1)
	parent.triggerevent("pc_retrieve")
end if
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
	cb_2.enabled = true
end if
end on

type dw_tab_test_prodotti_dettaglio_1 from uo_cs_xx_dw within w_collaudi
integer x = 2354
integer y = 580
integer width = 731
integer height = 1160
integer taborder = 20
string dataobject = "d_tab_test_prodotti_dettaglio_1"
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,is_cod_prodotto,is_cod_test)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type mle_1 from multilineedit within w_collaudi
integer x = 2354
integer y = 400
integer width = 731
integer height = 160
integer taborder = 50
integer textsize = -7
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
string text = "Riepilogo delle caratteristiche della sessione di  collaudo del prodotto  selezionato"
alignment alignment = center!
end type

type cb_2 from cb_stock_ricerca within w_collaudi
integer x = 2194
integer y = 820
integer width = 73
integer height = 80
integer taborder = 10
boolean bringtotop = true
end type

event clicked;call cb_stock_ricerca::clicked;s_cs_xx.parametri.parametro_s_10 = dw_collaudi_dettaglio.getitemstring(dw_collaudi_dettaglio.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_collaudi_dettaglio.getitemstring(dw_collaudi_dettaglio.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_collaudi_dettaglio.getitemstring(dw_collaudi_dettaglio.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_collaudi_dettaglio.getitemstring(dw_collaudi_dettaglio.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_collaudi_dettaglio.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end event

type dw_collaudi_dettaglio from uo_cs_xx_dw within w_collaudi
integer x = 23
integer y = 400
integer width = 2309
integer height = 1440
integer taborder = 40
string dataobject = "d_collaudi_dettaglio"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then

	is_cod_test=""
	is_cod_prodotto=""
	
	choose case i_colname

		case "cod_test" 
			string ls_nl
			setnull(ls_nl)
			setitem(getrow(),"cod_prodotto",ls_nl)
			f_PO_LoadDDDW_DW(dw_collaudi_dettaglio,"cod_prodotto",sqlca,&
   			              "anag_prodotti","cod_prodotto","des_prodotto",&
         			        "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in(select cod_prodotto from tab_test_prodotti where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_test='" + i_coltext + "')")
			dw_tab_test_prodotti_dettaglio_1.change_dw_focus(dw_tab_test_prodotti_dettaglio_1)
			parent.triggerevent("pc_retrieve")
	
		case "cod_prodotto" 
			string ls_cod_misura
			is_cod_test = dw_collaudi_dettaglio.GetItemString(dw_collaudi_dettaglio.getrow(), "cod_test")
			is_cod_prodotto = i_coltext
			dw_tab_test_prodotti_dettaglio_1.change_dw_focus(dw_tab_test_prodotti_dettaglio_1)
			parent.triggerevent("pc_retrieve")
		
			select cod_misura into: ls_cod_misura
			from tab_test_prodotti
			where cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:i_coltext and
					cod_test=:is_cod_test;
					
			if ls_cod_misura = "" then
				g_mb.messagebox("Omnia","Non esiste il test selezionato per il prodotto corrente.")
			else
				setitem(getrow(),"cod_misura",ls_cod_misura)		
			end if

	end choose		
end if
end event

type cb_formule from commandbutton within w_collaudi
integer x = 2720
integer y = 1760
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Formule"
end type

event clicked;s_cs_xx.parametri.parametro_s_15 = "collaudi"
s_cs_xx.parametri.parametro_dw_1 = dw_collaudi_lista
window_open(w_formule,-1)
end event

