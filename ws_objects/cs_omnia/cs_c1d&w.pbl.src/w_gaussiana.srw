﻿$PBExportHeader$w_gaussiana.srw
$PBExportComments$Window Grafico Gaussiana Normalizzata
forward
global type w_gaussiana from w_cs_xx_risposta
end type
type gr_1 from graph within w_gaussiana
end type
type st_1 from statictext within w_gaussiana
end type
type st_2 from statictext within w_gaussiana
end type
type st_3 from statictext within w_gaussiana
end type
type st_4 from statictext within w_gaussiana
end type
type cb_1 from commandbutton within w_gaussiana
end type
type st_5 from statictext within w_gaussiana
end type
type st_6 from statictext within w_gaussiana
end type
end forward

global type w_gaussiana from w_cs_xx_risposta
int Width=3507
int Height=1721
boolean TitleBar=true
string Title="Grafico Gaussiana"
gr_1 gr_1
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
cb_1 cb_1
st_5 st_5
st_6 st_6
end type
global w_gaussiana w_gaussiana

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;integer li_test,li_series_nbr_1,li_series_nbr_2,li_series_nbr_3,li_series_nbr_4,li_series_nbr_5
double ldd_x,ldd_y,ldd_mu,ldd_sigma, ldd_costante

st_1.text= string(s_cs_xx.parametri.parametro_d_1)
st_4.text= string(s_cs_xx.parametri.parametro_d_2)

if s_cs_xx.parametri.parametro_d_1 > 5 then
	g_mb.messagebox("Omnia","Attenzione! Il valore della statistica test non rientra nei limiti del grafico, pertanto la rappresentazione non è proporzionale",Exclamation!)
end if

if s_cs_xx.parametri.parametro_d_1 > s_cs_xx.parametri.parametro_d_2 then
	st_6.text = "Test fallito"
else
	st_6.text = "Test superato"
end if

ldd_mu = 5
ldd_sigma = 1
ldd_costante = ldd_sigma*sqrt(2*3.141592654)

li_series_nbr_1 = gr_1.AddSeries("1N")

gr_1.SetSeriesStyle ( "1N", noSymbol!)
gr_1.SetSeriesStyle ( "1N", linecolor! , rgb(0,0,0) )
gr_1.SetSeriesStyle ( "1N", dot!, 1)

for ldd_x = 0 to 10 step .05
	ldd_y = ((ldd_x - ldd_mu)^2)/((2*ldd_sigma)^2)
	ldd_y = exp(-ldd_y)
	ldd_y = ldd_y/ldd_costante
	li_test = gr_1.addData ( li_series_nbr_1, ldd_x, ldd_y)
next


if s_cs_xx.parametri.parametro_s_1 <> "B" then

	li_series_nbr_2 = gr_1.AddSeries("Calcolato")
	li_series_nbr_3 = gr_1.AddSeries("Riferimento")

	gr_1.SetSeriesStyle ( "Riferimento", SymbolSolidCircle!	)
	gr_1.SetSeriesStyle ( "Riferimento", linecolor! , rgb(255,0,0))
	
	gr_1.SetSeriesStyle ( "Calcolato", Symbolstar!)
	gr_1.SetSeriesStyle ( "Calcolato", linecolor! , rgb(0,255,0))
	
	li_test = gr_1.addData ( li_series_nbr_3, s_cs_xx.parametri.parametro_d_2 + 5, 0)
	li_test = gr_1.addData ( li_series_nbr_2, s_cs_xx.parametri.parametro_d_1 + 5, 0)

else

	li_series_nbr_2 = gr_1.AddSeries("Calc_1")
	li_series_nbr_3 = gr_1.AddSeries("Calc_2")
	li_series_nbr_4 = gr_1.AddSeries("Rif_1")
	li_series_nbr_5 = gr_1.AddSeries("Rif_2")

	gr_1.SetSeriesStyle ( "Rif_1", SymbolSolidCircle!	)
	gr_1.SetSeriesStyle ( "Rif_1", Foreground!  , rgb(255,0,0))

	gr_1.SetSeriesStyle ( "Calc_1", Symbolstar!)
	gr_1.SetSeriesStyle ( "Calc_1", Foreground! , rgb(0,255,0))

	gr_1.SetSeriesStyle ( "Rif_2", SymbolSolidCircle!)
	gr_1.SetSeriesStyle ( "Rif_2", Foreground!  , rgb(0,0,255))

	gr_1.SetSeriesStyle ( "Calc_2", Symbolstar!)
	gr_1.SetSeriesStyle ( "Calc_2", Foreground!  , rgb(255,255,0))

	li_test = gr_1.addData ( li_series_nbr_4, s_cs_xx.parametri.parametro_d_2 + 5, 0)
	li_test = gr_1.addData ( li_series_nbr_2, s_cs_xx.parametri.parametro_d_1 + 5, 0)

	li_test = gr_1.addData ( li_series_nbr_3, 5 - s_cs_xx.parametri.parametro_d_1, 0)
	li_test = gr_1.addData ( li_series_nbr_5, 5 - s_cs_xx.parametri.parametro_d_2, 0)

	
end if
end on

on w_gaussiana.create
int iCurrent
call w_cs_xx_risposta::create
this.gr_1=create gr_1
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.cb_1=create cb_1
this.st_5=create st_5
this.st_6=create st_6
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=gr_1
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=st_3
this.Control[iCurrent+5]=st_4
this.Control[iCurrent+6]=cb_1
this.Control[iCurrent+7]=st_5
this.Control[iCurrent+8]=st_6
end on

on w_gaussiana.destroy
call w_cs_xx_risposta::destroy
destroy(this.gr_1)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.st_5)
destroy(this.st_6)
end on

type gr_1 from graph within w_gaussiana
int X=10
int Y=17
int Width=3443
int Height=1485
boolean Enabled=false
grGraphType GraphType=ScatterGraph!
grLegendType Legend=AtBottom!
grSortType SeriesSort=Ascending!
grSortType CategorySort=Ascending!
string Title="Gaussiana Normalizzata"
boolean Border=true
int Depth=100
int Elevation=20
boolean FocusRectangle=false
int Perspective=2
int Rotation=-20
int Spacing=100
long TextColor=8388608
long BackColor=12632256
long ShadeColor=8355711
end type

on gr_1.create
TitleDispAttr = create grDispAttr
LegendDispAttr = create grDispAttr
PieDispAttr = create grDispAttr
Series = create grAxis
Series.DispAttr = create grDispAttr
Series.LabelDispAttr = create grDispAttr
Category = create grAxis
Category.DispAttr = create grDispAttr
Category.LabelDispAttr = create grDispAttr
Values = create grAxis
Values.DispAttr = create grDispAttr
Values.LabelDispAttr = create grDispAttr
TitleDispAttr.Weight=700
TitleDispAttr.FaceName="Arial"
TitleDispAttr.FontFamily=Swiss!
TitleDispAttr.FontPitch=Variable!
TitleDispAttr.Alignment=Center!
TitleDispAttr.TextColor=8388608
TitleDispAttr.BackColor=536870912
TitleDispAttr.Format="[General]"
TitleDispAttr.DisplayExpression="title"
TitleDispAttr.AutoSize=true
Category.Label="x"
Category.ShadeBackEdge=true
Category.MaximumValue=10
Category.MinorDivisions=3
Category.SecondaryLine=transparent!
Category.MajorGridLine=transparent!
Category.MinorGridLine=transparent!
Category.DropLines=transparent!
Category.MajorTic=Straddle!
Category.MinorTic=Outside!
Category.DataType=adtDouble!
Category.DispAttr.Weight=400
Category.DispAttr.FaceName="Arial"
Category.DispAttr.FontFamily=Swiss!
Category.DispAttr.FontPitch=Variable!
Category.DispAttr.Alignment=Center!
Category.DispAttr.TextColor=12632256
Category.DispAttr.BackColor=553648127
Category.DispAttr.Format="[General]"
Category.DispAttr.DisplayExpression="category"
Category.DispAttr.AutoSize=true
Category.LabelDispAttr.Weight=400
Category.LabelDispAttr.FaceName="Arial"
Category.LabelDispAttr.FontFamily=Swiss!
Category.LabelDispAttr.FontPitch=Variable!
Category.LabelDispAttr.Alignment=Center!
Category.LabelDispAttr.TextColor=255
Category.LabelDispAttr.BackColor=553648127
Category.LabelDispAttr.Format="[General]"
Category.LabelDispAttr.DisplayExpression="~"~""
Category.LabelDispAttr.AutoSize=true
Values.AutoScale=true
Values.MinorDivisions=5
Values.PrimaryLine=transparent!
Values.SecondaryLine=transparent!
Values.MajorGridLine=transparent!
Values.MinorGridLine=transparent!
Values.DropLines=transparent!
Values.MajorTic=Outside!
Values.MinorTic=Outside!
Values.DataType=adtDouble!
Values.DispAttr.Weight=400
Values.DispAttr.FaceName="Arial"
Values.DispAttr.FontFamily=Swiss!
Values.DispAttr.FontPitch=Variable!
Values.DispAttr.Alignment=Right!
Values.DispAttr.TextColor=8388608
Values.DispAttr.BackColor=536870912
Values.DispAttr.Format="[General]"
Values.DispAttr.DisplayExpression="value"
Values.DispAttr.AutoSize=true
Values.LabelDispAttr.Weight=400
Values.LabelDispAttr.FaceName="Arial"
Values.LabelDispAttr.FontFamily=Swiss!
Values.LabelDispAttr.FontPitch=Variable!
Values.LabelDispAttr.Alignment=Center!
Values.LabelDispAttr.TextColor=8388608
Values.LabelDispAttr.BackColor=553648127
Values.LabelDispAttr.Format="[General]"
Values.LabelDispAttr.DisplayExpression=" valuesaxislabel "
Values.LabelDispAttr.AutoSize=true
Values.LabelDispAttr.Escapement=900
Series.Label="(None)"
Series.AutoScale=true
Series.SecondaryLine=transparent!
Series.MajorGridLine=transparent!
Series.MinorGridLine=transparent!
Series.DropLines=transparent!
Series.OriginLine=transparent!
Series.MajorTic=Outside!
Series.DataType=adtText!
Series.DispAttr.Weight=400
Series.DispAttr.FaceName="Arial"
Series.DispAttr.FontFamily=Swiss!
Series.DispAttr.FontPitch=Variable!
Series.DispAttr.TextColor=8388608
Series.DispAttr.BackColor=536870912
Series.DispAttr.Format="[General]"
Series.DispAttr.DisplayExpression="series"
Series.DispAttr.AutoSize=true
Series.LabelDispAttr.Weight=400
Series.LabelDispAttr.FaceName="Arial"
Series.LabelDispAttr.FontFamily=Swiss!
Series.LabelDispAttr.FontPitch=Variable!
Series.LabelDispAttr.Alignment=Center!
Series.LabelDispAttr.TextColor=8388608
Series.LabelDispAttr.BackColor=536870912
Series.LabelDispAttr.Format="[General]"
Series.LabelDispAttr.DisplayExpression="seriesaxislabel"
Series.LabelDispAttr.AutoSize=true
LegendDispAttr.Weight=400
LegendDispAttr.FaceName="Arial"
LegendDispAttr.FontFamily=Swiss!
LegendDispAttr.FontPitch=Variable!
LegendDispAttr.TextColor=8388608
LegendDispAttr.BackColor=553648127
LegendDispAttr.Format="[General]"
LegendDispAttr.DisplayExpression="series"
LegendDispAttr.AutoSize=true
PieDispAttr.Weight=400
PieDispAttr.FaceName="Arial"
PieDispAttr.FontFamily=Swiss!
PieDispAttr.FontPitch=Variable!
PieDispAttr.TextColor=8388608
PieDispAttr.BackColor=536870912
PieDispAttr.Format="[General]"
PieDispAttr.DisplayExpression="if(seriescount > 1, series,string(percentofseries,~"0.00%~"))"
PieDispAttr.AutoSize=true
end on

on gr_1.destroy
destroy TitleDispAttr
destroy LegendDispAttr
destroy PieDispAttr
destroy Series.DispAttr
destroy Series.LabelDispAttr
destroy Series
destroy Category.DispAttr
destroy Category.LabelDispAttr
destroy Category
destroy Values.DispAttr
destroy Values.LabelDispAttr
destroy Values
end on

type st_1 from statictext within w_gaussiana
int X=421
int Y=1517
int Width=622
int Height=73
boolean Enabled=false
string Text="none"
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_gaussiana
int X=1130
int Y=1517
int Width=531
int Height=73
boolean Enabled=false
string Text="limite zona di rifiuto:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_gaussiana
int X=19
int Y=1517
int Width=398
int Height=73
boolean Enabled=false
string Text="statistica test :"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_gaussiana
int X=1674
int Y=1517
int Width=540
int Height=73
boolean Enabled=false
string Text="none"
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_gaussiana
int X=3086
int Y=1509
int Width=366
int Height=89
int TabOrder=1
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type st_5 from statictext within w_gaussiana
int X=2282
int Y=1517
int Width=261
int Height=73
boolean Enabled=false
string Text="Risultato:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_gaussiana
int X=2556
int Y=1517
int Width=375
int Height=73
boolean Enabled=false
string Text="......."
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

