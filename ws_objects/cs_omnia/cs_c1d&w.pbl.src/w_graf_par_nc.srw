﻿$PBExportHeader$w_graf_par_nc.srw
$PBExportComments$Finestra Grafico di Pareto non Conformità Approvvigionamneti
forward
global type w_graf_par_nc from w_graf_pareto
end type
type em_a_data from editmask within w_graf_par_nc
end type
type em_da_data from editmask within w_graf_par_nc
end type
type gb_1 from groupbox within w_graf_par_nc
end type
type rb_1 from radiobutton within w_graf_par_nc
end type
type rb_2 from radiobutton within w_graf_par_nc
end type
type rb_3 from radiobutton within w_graf_par_nc
end type
type rb_4 from radiobutton within w_graf_par_nc
end type
type st_1 from statictext within w_graf_par_nc
end type
type st_2 from statictext within w_graf_par_nc
end type
type tab_1 from tab within w_graf_par_nc
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tab_1 from tab within w_graf_par_nc
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
end forward

global type w_graf_par_nc from w_graf_pareto
integer width = 3401
integer height = 2248
string title = "Grafici Non Conformità"
em_a_data em_a_data
em_da_data em_da_data
gb_1 gb_1
rb_1 rb_1
rb_2 rb_2
rb_3 rb_3
rb_4 rb_4
st_1 st_1
st_2 st_2
tab_1 tab_1
end type
global w_graf_par_nc w_graf_par_nc

type variables
string is_flag_tipo_nc
end variables

forward prototypes
public subroutine wf_aggiorna ()
end prototypes

public subroutine wf_aggiorna ();string ls_sql_child, ls_da_data, ls_a_data

ls_da_data=string( date( em_da_data.text ), s_cs_xx.db_funzioni.formato_data )
ls_a_data=string( date ( em_a_data.text ), s_cs_xx.db_funzioni.formato_data )

ls_sql_child = "SELECT count(tab_difformita.des_difformita), tab_difformita.des_difformita " + &
			      "FROM non_conformita, " + &
			         "tab_difformita " + &
			      "WHERE ( tab_difformita.cod_azienda = non_conformita.cod_azienda ) and " + &
			         "( tab_difformita.cod_errore = non_conformita.cod_errore ) and " + &
			         "( ( non_conformita.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND " + &
			         "( non_conformita.flag_tipo_nc = '" + is_flag_tipo_nc + "' ) AND " + &
			         "( non_conformita.data_creazione between '" + ls_da_data + "' and '" + ls_a_data + "' ) ) " + &
					"group by tab_difformita.des_difformita ORDER BY 1 DESC"

wf_pareto_init (ls_sql_child )


end subroutine

event open;call super::open;is_graf1_titolo = "Pareto Difformità su Non Conformità"
is_graf1_des_asse_x = "Tipi Difformità"
is_graf1_des_asse_y = "Numero Casi"
is_graf2_titolo = "Istrogramma Cumulato"
is_graf2_des_asse_x = "Comulata (20% Difformità)"
is_graf2_des_asse_y = "% Incidenza"

if s_cs_xx.parametri.parametro_s_10 = "" or isnull(s_cs_xx.parametri.parametro_s_10) then
	is_flag_tipo_nc = "A"
else
	is_flag_tipo_nc = s_cs_xx.parametri.parametro_s_10
	setnull(s_cs_xx.parametri.parametro_s_10)
end if
choose case is_flag_tipo_nc
	case "A" // Appovvigionamenti
		rb_1.checked = true
	case "V" // Vendita/Post Vendita
		rb_2.checked = true
	case "S" // Sistema
		rb_3.checked = true		
	case "P" // Interna o Produzione
		rb_4.checked = true
end choose
//is_flag_tipo_nc = "A"
//rb_1.Checked = true
em_da_data.text="01/01/1990"
em_a_data.text=string(today(),"dd/mm/yyyy")

wf_aggiorna()
end event

on w_graf_par_nc.create
int iCurrent
call super::create
this.em_a_data=create em_a_data
this.em_da_data=create em_da_data
this.gb_1=create gb_1
this.rb_1=create rb_1
this.rb_2=create rb_2
this.rb_3=create rb_3
this.rb_4=create rb_4
this.st_1=create st_1
this.st_2=create st_2
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_a_data
this.Control[iCurrent+2]=this.em_da_data
this.Control[iCurrent+3]=this.gb_1
this.Control[iCurrent+4]=this.rb_1
this.Control[iCurrent+5]=this.rb_2
this.Control[iCurrent+6]=this.rb_3
this.Control[iCurrent+7]=this.rb_4
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.st_2
this.Control[iCurrent+10]=this.tab_1
end on

on w_graf_par_nc.destroy
call super::destroy
destroy(this.em_a_data)
destroy(this.em_da_data)
destroy(this.gb_1)
destroy(this.rb_1)
destroy(this.rb_2)
destroy(this.rb_3)
destroy(this.rb_4)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.tab_1)
end on

type uo_grafico from w_graf_pareto`uo_grafico within w_graf_par_nc
integer x = 23
integer y = 140
integer width = 3314
integer height = 1780
end type

type dw_pareto_guasti from w_graf_pareto`dw_pareto_guasti within w_graf_par_nc
end type

type uo_graf_cumulata from w_graf_pareto`uo_graf_cumulata within w_graf_par_nc
integer x = 23
integer y = 140
integer width = 3314
integer height = 1780
integer taborder = 30
end type

type dw_guasti_cumulata from w_graf_pareto`dw_guasti_cumulata within w_graf_par_nc
integer taborder = 60
end type

type em_a_data from editmask within w_graf_par_nc
event modified pbm_enmodified
integer x = 2971
integer y = 2036
integer width = 389
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean autoskip = true
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "01/01/0000~~31/12/9999"
end type

event modified;wf_aggiorna()
end event

type em_da_data from editmask within w_graf_par_nc
event modified pbm_enmodified
integer x = 2263
integer y = 2036
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean autoskip = true
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "01/01/0000~~31/12/9999"
end type

event modified;wf_aggiorna()
end event

type gb_1 from groupbox within w_graf_par_nc
integer x = 23
integer y = 1960
integer width = 1737
integer height = 180
integer taborder = 21
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tipo non Conformità"
borderstyle borderstyle = stylelowered!
end type

type rb_1 from radiobutton within w_graf_par_nc
event clicked pbm_bnclicked
integer x = 69
integer y = 2040
integer width = 594
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Approvvigionamenti"
borderstyle borderstyle = stylelowered!
end type

event clicked;is_flag_tipo_nc = "A"

wf_aggiorna()
end event

type rb_2 from radiobutton within w_graf_par_nc
event clicked pbm_bnclicked
integer x = 681
integer y = 2040
integer width = 279
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Vendite"
borderstyle borderstyle = stylelowered!
end type

event clicked;is_flag_tipo_nc = "V"

wf_aggiorna()

end event

type rb_3 from radiobutton within w_graf_par_nc
event clicked pbm_bnclicked
integer x = 1051
integer y = 2040
integer width = 297
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Sistema"
borderstyle borderstyle = stylelowered!
end type

event clicked;is_flag_tipo_nc = "S"

wf_aggiorna()
end event

type rb_4 from radiobutton within w_graf_par_nc
event clicked pbm_bnclicked
integer x = 1440
integer y = 2040
integer width = 265
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Interne"
borderstyle borderstyle = stylelowered!
end type

event clicked;is_flag_tipo_nc = "I"

wf_aggiorna()
end event

type st_1 from statictext within w_graf_par_nc
integer x = 1989
integer y = 2040
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "da data"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_graf_par_nc
integer x = 2697
integer y = 2040
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "a data"
alignment alignment = right!
boolean focusrectangle = false
end type

type tab_1 from tab within w_graf_par_nc
integer y = 20
integer width = 3360
integer height = 1920
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

event selectionchanged;choose case newindex
		
	case 1
		
		uo_grafico.show()
		uo_graf_cumulata.hide()
		
	case 2
		
		uo_grafico.hide()
		uo_graf_cumulata.show()
		
end choose
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3323
integer height = 1796
long backcolor = 79741120
string text = "Pareto"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3323
integer height = 1796
long backcolor = 79741120
string text = "Cumulata"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

