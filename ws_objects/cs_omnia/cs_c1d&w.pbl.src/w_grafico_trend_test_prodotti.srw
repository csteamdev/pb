﻿$PBExportHeader$w_grafico_trend_test_prodotti.srw
$PBExportComments$Window grafico trend test prodotti
forward
global type w_grafico_trend_test_prodotti from w_response
end type
type st_1 from statictext within w_grafico_trend_test_prodotti
end type
type em_1 from editmask within w_grafico_trend_test_prodotti
end type
type st_2 from statictext within w_grafico_trend_test_prodotti
end type
type em_2 from editmask within w_grafico_trend_test_prodotti
end type
type cb_2 from commandbutton within w_grafico_trend_test_prodotti
end type
type cb_1 from uo_cb_close within w_grafico_trend_test_prodotti
end type
type uo_grafico from uo_std_chart within w_grafico_trend_test_prodotti
end type
end forward

global type w_grafico_trend_test_prodotti from w_response
int Width=3534
int Height=1625
boolean TitleBar=true
string Title="Trend Test-Prodotti"
st_1 st_1
em_1 em_1
st_2 st_2
em_2 em_2
cb_2 cb_2
cb_1 cb_1
uo_grafico uo_grafico
end type
global w_grafico_trend_test_prodotti w_grafico_trend_test_prodotti

event pc_setwindow;call super::pc_setwindow;em_1.text = "01/01/1990"
em_2.text = String( today(), "dd/mm/yyyy" )

set_w_options(c_noenablepopup + &
              c_nosaveposition)
cb_2.TriggerEvent ( Clicked! )
end event

on w_grafico_trend_test_prodotti.create
int iCurrent
call w_response::create
this.st_1=create st_1
this.em_1=create em_1
this.st_2=create st_2
this.em_2=create em_2
this.cb_2=create cb_2
this.cb_1=create cb_1
this.uo_grafico=create uo_grafico
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=em_1
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=em_2
this.Control[iCurrent+5]=cb_2
this.Control[iCurrent+6]=cb_1
this.Control[iCurrent+7]=uo_grafico
end on

on w_grafico_trend_test_prodotti.destroy
call w_response::destroy
destroy(this.st_1)
destroy(this.em_1)
destroy(this.st_2)
destroy(this.em_2)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.uo_grafico)
end on

type st_1 from statictext within w_grafico_trend_test_prodotti
int X=23
int Y=1421
int Width=270
int Height=81
boolean Enabled=false
string Text="Data inizio"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_1 from editmask within w_grafico_trend_test_prodotti
int X=298
int Y=1421
int Width=389
int Height=81
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_grafico_trend_test_prodotti
int X=778
int Y=1421
int Width=252
int Height=81
boolean Enabled=false
string Text="Data Fine"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_2 from editmask within w_grafico_trend_test_prodotti
int X=1052
int Y=1421
int Width=389
int Height=81
int TabOrder=30
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="Ä"
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_2 from commandbutton within w_grafico_trend_test_prodotti
int X=2721
int Y=1421
int Width=366
int Height=81
int TabOrder=40
string Text="&Grafico"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_test,ls_cod_prodotto
datetime ld_data_inizio,ld_data_fine
long  l_Error
integer li_cont = 0, li_prog_riga_campionamenti
decimal ldec_coll_risultato

//
// Dichiarazione delle caratteristiche grafiche del grafico
//

ls_cod_test = s_cs_xx.parametri.parametro_s_1
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_2
ld_data_inizio = datetime(date(em_1.text),00:00:00)
ld_data_fine = datetime(date(em_2.text),00:00:00)

DECLARE cur_Graph CURSOR FOR  
SELECT collaudi.prog_riga_campionamenti,avg(collaudi.risultato)
FROM collaudi
WHERE cod_azienda = :s_cs_xx.cod_azienda AND
      cod_test = :ls_cod_test  AND
      cod_prodotto = :ls_cod_prodotto  AND
		prog_riga_campionamenti is not Null  AND
		collaudi.risultato is not Null  AND
      data_registrazione between :ld_data_inizio and :ld_data_fine 
GROUP BY collaudi.prog_riga_campionamenti 
ORDER BY collaudi.prog_riga_campionamenti;

OPEN cur_Graph;

FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ldec_coll_risultato;

li_cont = 0
DO WHILE SQLCA.sqlcode = 0
	li_cont ++
	FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ldec_coll_risultato;
loop	

close cur_Graph;

if li_cont > 0 then
	uo_grafico.of_init(3, 0, "Trend Risultati Test per Campionamenti", "Numero di Campionamenti", "Media Risultato", &
						 "Asse_Z", "A1:B" + string(li_Cont) , "", True, False, False)
	uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).SeriesMarker.Auto = False
	uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).SeriesMarker.Show = True

	For li_cont = 1 To uo_grafico.ole_Graph.object.rowCount
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.visible = True
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.size = 8
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.style = 14
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.fillcolor.blue = 255
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.fillcolor.red = 140
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.fillcolor.green = 70
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).DataPointLabel.locationType = 1
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).DataPointLabel.Component = 1
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).DataPointLabel.VtFont.Size = 8		
	next

//
// Adesso carico dati ed etichette nell'oggetto DataGrid
//

	OPEN cur_Graph;

	FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ldec_coll_risultato;

	li_cont = 0
	DO WHILE SQLCA.sqlcode = 0
		li_cont ++
		uo_grafico.ole_Graph.object.datagrid.RowLabel(li_Cont, 1, string(li_prog_riga_campionamenti))
		uo_grafico.ole_graph.object.datagrid.setdata(li_cont, 1,ldec_coll_risultato,False)	
		FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ldec_coll_risultato;
	loop	

	close cur_Graph;
else
	uo_grafico.of_init(3, 0, "Trend Risultati Test per Campionamenti", "Numero di Campionamenti", "Media Risultato", &
						 "Asse_Z", "" , "", False, False, False)						 
	g_mb.messagebox("Omnia"," Non ci sono dati nell' intervallo di date selezionato ! ",Exclamation!)
end if	
end event

type cb_1 from uo_cb_close within w_grafico_trend_test_prodotti
int X=3109
int Y=1421
int Width=366
int Height=81
int TabOrder=50
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type uo_grafico from uo_std_chart within w_grafico_trend_test_prodotti
event destroy ( )
int X=23
int Y=21
int Width=3452
int Height=1381
int TabOrder=10
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
end type

on uo_grafico.destroy
call uo_std_chart::destroy
end on

