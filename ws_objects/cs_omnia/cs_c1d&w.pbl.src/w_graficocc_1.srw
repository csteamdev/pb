﻿$PBExportHeader$w_graficocc_1.srw
$PBExportComments$Window grafico carta controllo 1
forward
global type w_graficocc_1 from w_cs_xx_risposta
end type
type dw_graficocc_1 from uo_cs_xx_dw within w_graficocc_1
end type
type dw_graficocc_2 from uo_cs_xx_dw within w_graficocc_1
end type
end forward

global type w_graficocc_1 from w_cs_xx_risposta
int Width=3562
int Height=1837
boolean TitleBar=true
string Title="Grafico Carte di Controllo"
dw_graficocc_1 dw_graficocc_1
dw_graficocc_2 dw_graficocc_2
end type
global w_graficocc_1 w_graficocc_1

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;double ldd_valore_max
double ldd_valore_min
string ls_return

dw_graficocc_1.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
dw_graficocc_2.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)

select max(valore) into:ldd_valore_max from tab_grafi;
select min(valore) into:ldd_valore_min from tab_grafi;

ldd_valore_max = int(ldd_valore_max + (ldd_valore_max/100)*2)
ldd_valore_min = int(ldd_valore_min - (ldd_valore_max/100)*2)

ls_return = dw_graficocc_1.Modify("gr_1.series.AutoScale=0")
ls_return = dw_graficocc_1.modify("gr_1.values.maximumvalue="+string(ldd_valore_max))
ls_return = dw_graficocc_1.modify("gr_1.values.minimumvalue="+string(ldd_valore_min))

select max(valore) into:ldd_valore_max from tab_grafi_2;
select min(valore) into:ldd_valore_min from tab_grafi_2;

ldd_valore_max = int(ldd_valore_max + (ldd_valore_max/100)*2)
ldd_valore_min = int(ldd_valore_min - (ldd_valore_max/100)*2)

ls_return = dw_graficocc_2.Modify("gr_1.series.AutoScale=0")
ls_return = dw_graficocc_2.modify("gr_1.values.maximumvalue="+string(ldd_valore_max))
ls_return = dw_graficocc_2.modify("gr_1.values.minimumvalue="+string(ldd_valore_min))
end on

on w_graficocc_1.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_graficocc_1=create dw_graficocc_1
this.dw_graficocc_2=create dw_graficocc_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_graficocc_1
this.Control[iCurrent+2]=dw_graficocc_2
end on

on w_graficocc_1.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_graficocc_1)
destroy(this.dw_graficocc_2)
end on

type dw_graficocc_1 from uo_cs_xx_dw within w_graficocc_1
event ue_imposta_grafico pbm_dwngraphcreate
int X=19
int Y=17
int Width=3489
int Height=845
int TabOrder=10
string DataObject="d_graficocc_1"
end type

on ue_imposta_grafico;call uo_cs_xx_dw::ue_imposta_grafico;dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Sup.", NoSymbol!		)
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Inf.", NoSymbol!		)
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Media Generale", NoSymbol!		)
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Valore", SymbolSolidCircle!)
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Sup.", Foreground! , rgb(0,0,0) )
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Inf.", Foreground! , rgb(0,0,0) )
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Media Generale", Foreground! , rgb(255,0,0) )
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Valore", Foreground! , rgb(0,255,0) )
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_graficocc_2 from uo_cs_xx_dw within w_graficocc_1
event ue_imposta_grafico pbm_dwngraphcreate
int X=19
int Y=873
int Width=3489
int Height=845
int TabOrder=20
string DataObject="d_graficocc_2"
end type

on ue_imposta_grafico;call uo_cs_xx_dw::ue_imposta_grafico;dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Sup.", NoSymbol!		)
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Inf.", NoSymbol!		)
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Media Esc.", NoSymbol!		)
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Valore", SymbolStar!	)

dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Sup.", Foreground! , rgb(0,0,0) )
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Inf.", Foreground! , rgb(0,0,0) )
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Media Esc.", Foreground! , rgb(255,0,0) )
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Valore", Foreground! , rgb(0,0,255) )

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

