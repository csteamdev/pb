﻿$PBExportHeader$w_graficocc_2.srw
$PBExportComments$Window grafico carta controllo per i 3 grafici
forward
global type w_graficocc_2 from w_cs_xx_risposta
end type
type dw_graficocc_1 from uo_cs_xx_dw within w_graficocc_2
end type
type dw_graficocc_2 from uo_cs_xx_dw within w_graficocc_2
end type
type dw_graficocc_3 from uo_cs_xx_dw within w_graficocc_2
end type
end forward

global type w_graficocc_2 from w_cs_xx_risposta
int Width=3287
int Height=2237
boolean TitleBar=true
string Title="Grafico Carte di Controllo"
dw_graficocc_1 dw_graficocc_1
dw_graficocc_2 dw_graficocc_2
dw_graficocc_3 dw_graficocc_3
end type
global w_graficocc_2 w_graficocc_2

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;double ldd_valore_max,ldd_valore_min
string ls_return

dw_graficocc_1.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
dw_graficocc_2.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
dw_graficocc_3.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)

select max(valore) into:ldd_valore_max from tab_grafi;
select min(valore) into:ldd_valore_min from tab_grafi;

ldd_valore_max = int(ldd_valore_max + (ldd_valore_max/100)*2)
ldd_valore_min = int(ldd_valore_min - (ldd_valore_max/100)*2)

ls_return = dw_graficocc_1.Modify("gr_1.series.AutoScale=0")
ls_return = dw_graficocc_1.modify("gr_1.values.maximumvalue="+string(ldd_valore_max))
ls_return = dw_graficocc_1.modify("gr_1.values.minimumvalue="+string(ldd_valore_min))

select max(valore) into:ldd_valore_max from tab_grafi_2;
select min(valore) into:ldd_valore_min from tab_grafi_2;

ldd_valore_max = int(ldd_valore_max + (ldd_valore_max/100)*2)
ldd_valore_min = int(ldd_valore_min - (ldd_valore_max/100)*2)

ls_return = dw_graficocc_2.Modify("gr_1.series.AutoScale=0")
ls_return = dw_graficocc_2.modify("gr_1.values.maximumvalue="+string(ldd_valore_max))
ls_return = dw_graficocc_2.modify("gr_1.values.minimumvalue="+string(ldd_valore_min))

select max(valore) into:ldd_valore_max from tab_grafi_3;
select min(valore) into:ldd_valore_min from tab_grafi_3;

ldd_valore_max = int(ldd_valore_max + (ldd_valore_max/100)*2)
ldd_valore_min = int(ldd_valore_min - (ldd_valore_max/100)*2)

ls_return = dw_graficocc_3.Modify("gr_1.series.AutoScale=0")
ls_return = dw_graficocc_3.modify("gr_1.values.maximumvalue="+string(ldd_valore_max))
ls_return = dw_graficocc_3.modify("gr_1.values.minimumvalue="+string(ldd_valore_min))
end on

on w_graficocc_2.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_graficocc_1=create dw_graficocc_1
this.dw_graficocc_2=create dw_graficocc_2
this.dw_graficocc_3=create dw_graficocc_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_graficocc_1
this.Control[iCurrent+2]=dw_graficocc_2
this.Control[iCurrent+3]=dw_graficocc_3
end on

on w_graficocc_2.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_graficocc_1)
destroy(this.dw_graficocc_2)
destroy(this.dw_graficocc_3)
end on

type dw_graficocc_1 from uo_cs_xx_dw within w_graficocc_2
event ue_imposta_grafico pbm_dwngraphcreate
int X=19
int Y=17
int Width=3210
int Height=689
int TabOrder=10
string DataObject="d_graficocc_1"
end type

on ue_imposta_grafico;call uo_cs_xx_dw::ue_imposta_grafico;dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Sup.", NoSymbol!		)
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Inf.", NoSymbol!		)
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Media Generale", NoSymbol!		)
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Valore", SymbolSolidCircle!)

dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Sup.", Foreground! , rgb(0,0,0) )
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Lim. Inf.", Foreground! , rgb(0,0,0) )
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Media Generale", Foreground! , rgb(255,0,0) )
dw_graficocc_1.SetSeriesStyle ( "gr_1", "Valore", Foreground! , rgb(0,255,0) )

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_graficocc_2 from uo_cs_xx_dw within w_graficocc_2
event ue_imposta_grafico pbm_dwngraphcreate
int X=19
int Y=1425
int Width=3210
int Height=689
int TabOrder=20
string DataObject="d_graficocc_2"
end type

on ue_imposta_grafico;call uo_cs_xx_dw::ue_imposta_grafico;dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Sup.", NoSymbol!		)
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Inf.", NoSymbol!		)
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Media Esc.", NoSymbol!		)
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Valore", SymbolStar!	)

dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Sup.", Foreground! , rgb(0,0,0) )
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Lim. Inf.", Foreground! , rgb(0,0,0) )
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Media Esc.", Foreground! , rgb(255,0,0) )
dw_graficocc_2.SetSeriesStyle ( "gr_1", "Valore", Foreground! , rgb(0,0,255) )

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_graficocc_3 from uo_cs_xx_dw within w_graficocc_2
event ue_imposta_grafico pbm_dwngraphcreate
int X=19
int Y=721
int Width=3210
int Height=689
int TabOrder=30
string DataObject="d_graficocc_3"
end type

on ue_imposta_grafico;call uo_cs_xx_dw::ue_imposta_grafico;dw_graficocc_3.SetSeriesStyle ( "gr_1", "Lim. Sup.", NoSymbol!		)
dw_graficocc_3.SetSeriesStyle ( "gr_1", "Lim. Inf.", NoSymbol!		)
dw_graficocc_3.SetSeriesStyle ( "gr_1", "Media Esc.", NoSymbol!		)
dw_graficocc_3.SetSeriesStyle ( "gr_1", "Valore", SymbolStar!	)

dw_graficocc_3.SetSeriesStyle ( "gr_1", "Lim. Sup.", Foreground! , rgb(0,0,0) )
dw_graficocc_3.SetSeriesStyle ( "gr_1", "Lim. Inf.", Foreground! , rgb(0,0,0) )
dw_graficocc_3.SetSeriesStyle ( "gr_1", "Media Esc.", Foreground! , rgb(255,0,0) )
dw_graficocc_3.SetSeriesStyle ( "gr_1", "Valore", Foreground! , rgb(0,255,255) )

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

