﻿$PBExportHeader$w_lista_collaudi_1.srw
$PBExportComments$se quella sopra (w_lista_collaudi) va bene allora  *CANCELLARE*
forward
global type w_lista_collaudi_1 from w_cs_xx_risposta
end type
type dw_lista_collaudi from uo_cs_xx_dw within w_lista_collaudi_1
end type
type cb_conferma from commandbutton within w_lista_collaudi_1
end type
type cb_annulla from commandbutton within w_lista_collaudi_1
end type
end forward

global type w_lista_collaudi_1 from w_cs_xx_risposta
int Width=3438
int Height=1541
boolean TitleBar=true
string Title="Selezionare i collaudi da associare al campionamento"
dw_lista_collaudi dw_lista_collaudi
cb_conferma cb_conferma
cb_annulla cb_annulla
end type
global w_lista_collaudi_1 w_lista_collaudi_1

type variables
boolean ib_conferma
end variables

event pc_setwindow;call super::pc_setwindow;dw_lista_collaudi.set_dw_options(sqlca,pcca.null_object,c_multiselect + c_nonew + c_nomodify + c_nodelete,c_default)

ib_conferma = false
end event

on w_lista_collaudi_1.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_lista_collaudi=create dw_lista_collaudi
this.cb_conferma=create cb_conferma
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_collaudi
this.Control[iCurrent+2]=cb_conferma
this.Control[iCurrent+3]=cb_annulla
end on

on w_lista_collaudi_1.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_lista_collaudi)
destroy(this.cb_conferma)
destroy(this.cb_annulla)
end on

event close;call super::close;if ib_conferma = false then
	s_cs_xx.parametri.parametro_i_1 = -1  //annulla l'operazione
else
	s_cs_xx.parametri.parametro_i_1 = 0   //conferma l'operazione
end if
end event

type dw_lista_collaudi from uo_cs_xx_dw within w_lista_collaudi_1
int X=23
int Y=21
int Width=3361
int Height=1301
int TabOrder=20
string DataObject="d_lista_collaudi"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG   l_Error,ll_prog_stock
string   ls_cod_test,ls_cod_prodotto,ls_cod_lotto,ls_cod_deposito,ls_cod_ubicazione
datetime ld_da_data,ld_a_data,ld_data_stock


ll_prog_stock = s_cs_xx.parametri.parametro_d_1 
ls_cod_deposito = s_cs_xx.parametri.parametro_s_3  
ls_cod_ubicazione = s_cs_xx.parametri.parametro_s_4 
ld_data_stock = s_cs_xx.parametri.parametro_data_3 


ls_cod_test = s_cs_xx.parametri.parametro_s_1
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_2
ls_cod_lotto = s_cs_xx.parametri.parametro_s_5
ld_da_data = s_cs_xx.parametri.parametro_data_1 
ld_a_data = s_cs_xx.parametri.parametro_data_2

l_Error = Retrieve(s_cs_xx.cod_azienda,ld_da_data,ld_a_data,ls_cod_test,ls_cod_prodotto,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto,ld_data_stock,ll_prog_stock)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type cb_conferma from commandbutton within w_lista_collaudi_1
int X=3018
int Y=1341
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Conferma"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long     ll_num_righe,ll_i,ll_anno_registrazione,ll_num_registrazione,ll_selected[]
long     ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti

ll_anno_reg_campionamenti = s_cs_xx.parametri.parametro_ul_1
ll_num_reg_campionamenti = s_cs_xx.parametri.parametro_ul_2
ll_prog_riga_campionamenti = s_cs_xx.parametri.parametro_ul_3

dw_lista_collaudi.get_selected_rows(ll_selected[])
ll_num_righe = upperbound(ll_selected[])

if ll_num_righe=0 then
	g_mb.messagebox("Omnia","Attenzione! Selezionare almeno un collaudo",information!)
	return
end if

setpointer(hourglass!)

for ll_i = 1 to ll_num_righe

	ll_anno_registrazione = dw_lista_collaudi.getitemnumber(ll_selected[ll_i],"anno_registrazione")
	ll_num_registrazione = dw_lista_collaudi.getitemnumber(ll_selected[ll_i],"num_registrazione")
		
	window_open(w_lista_collaudi,0)
		
	UPDATE collaudi  
   SET    anno_reg_campionamenti = :ll_anno_reg_campionamenti,   
          num_reg_campionamenti = :ll_num_reg_campionamenti,   
          prog_riga_campionamenti = :ll_prog_riga_campionamenti  
 	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	and    anno_registrazione =: ll_anno_registrazione 
	and 	 num_registrazione =: ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if

next 

setpointer(arrow!)

ib_conferma = true
close (parent)
end event

type cb_annulla from commandbutton within w_lista_collaudi_1
int X=2629
int Y=1341
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;ib_conferma = false
close (parent)
end event

