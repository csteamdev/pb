﻿$PBExportHeader$w_riepiloghi_test_prodotti.srw
$PBExportComments$Window di riepilogo test prodotti
forward
global type w_riepiloghi_test_prodotti from w_cs_xx_principale
end type
type lb_1 from listbox within w_riepiloghi_test_prodotti
end type
type lb_2 from listbox within w_riepiloghi_test_prodotti
end type
type cb_1 from commandbutton within w_riepiloghi_test_prodotti
end type
type cb_2 from commandbutton within w_riepiloghi_test_prodotti
end type
type cb_3 from commandbutton within w_riepiloghi_test_prodotti
end type
type cb_5 from commandbutton within w_riepiloghi_test_prodotti
end type
type cb_6 from commandbutton within w_riepiloghi_test_prodotti
end type
type mle_1 from multilineedit within w_riepiloghi_test_prodotti
end type
end forward

global type w_riepiloghi_test_prodotti from w_cs_xx_principale
int Width=2666
int Height=1605
boolean TitleBar=true
string Title="Riepiloghi Test-Prodotti"
lb_1 lb_1
lb_2 lb_2
cb_1 cb_1
cb_2 cb_2
cb_3 cb_3
cb_5 cb_5
cb_6 cb_6
mle_1 mle_1
end type
global w_riepiloghi_test_prodotti w_riepiloghi_test_prodotti

on w_riepiloghi_test_prodotti.create
int iCurrent
call w_cs_xx_principale::create
this.lb_1=create lb_1
this.lb_2=create lb_2
this.cb_1=create cb_1
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_5=create cb_5
this.cb_6=create cb_6
this.mle_1=create mle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=lb_1
this.Control[iCurrent+2]=lb_2
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=cb_2
this.Control[iCurrent+5]=cb_3
this.Control[iCurrent+6]=cb_5
this.Control[iCurrent+7]=cb_6
this.Control[iCurrent+8]=mle_1
end on

on w_riepiloghi_test_prodotti.destroy
call w_cs_xx_principale::destroy
destroy(this.lb_1)
destroy(this.lb_2)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_5)
destroy(this.cb_6)
destroy(this.mle_1)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup)
end event

type lb_1 from listbox within w_riepiloghi_test_prodotti
int X=23
int Y=361
int Width=1258
int Height=1021
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on doubleclicked;long ll_progressivo
string ls_codice[],ls_cod_prodotto

setpointer(hourglass!)

ll_progressivo = f_po_selectlb(lb_1,ls_codice[])
ls_cod_prodotto = ls_codice[ll_progressivo]

lb_2.reset()

f_po_loadlb(lb_2, &
                 sqlca, &
                 "tab_test", &
                 "cod_test", &
                 "des_test", &
                 "cod_test in (select cod_test from tab_test_prodotti where cod_prodotto = '" + ls_cod_prodotto + "' and cod_azienda = '" + s_cs_xx.cod_azienda + "') order by des_test","")

setpointer(arrow!)
end on

type lb_2 from listbox within w_riepiloghi_test_prodotti
int X=1340
int Y=357
int Width=1258
int Height=1021
int TabOrder=70
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on doubleclicked;long ll_progressivo
string ls_codice[],ls_cod_test

setpointer(hourglass!)

ll_progressivo = f_po_selectlb(lb_2,ls_codice[])
ls_cod_test = ls_codice[ll_progressivo]

lb_1.reset()

f_po_loadlb(lb_1, &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda ='" + s_cs_xx.cod_azienda + "' and cod_prodotto in (select cod_prodotto from tab_test_prodotti where cod_test='" + ls_cod_test + "' and cod_azienda='" + s_cs_xx.cod_azienda + "') order by des_prodotto","")

setpointer(arrow!)
end on

type cb_1 from commandbutton within w_riepiloghi_test_prodotti
int X=1441
int Y=1401
int Width=366
int Height=81
int TabOrder=40
string Text="Gr. &Trend"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_progressivo
string ls_codice[],ls_cod_test,ls_cod_prodotto

ll_progressivo = f_po_selectlb(lb_2,ls_codice[])
ls_cod_test = ls_codice[ll_progressivo]

if isnull(ls_cod_test) or ls_cod_test="" then
	g_mb.messagebox("Omnia","Attenzione! Selezionare un test!",stopsign!)
	return
end if

s_cs_xx.parametri.parametro_s_1=ls_cod_test

ll_progressivo = f_po_selectlb(lb_1,ls_codice[])
ls_cod_prodotto = ls_codice[ll_progressivo]
if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
	g_mb.messagebox("Omnia","Attenzione! Selezionare un prodotto!",stopsign!)
	return
end if

s_cs_xx.parametri.parametro_s_2=ls_cod_prodotto

window_open(w_grafico_trend_test_prodotti,0)
end event

type cb_2 from commandbutton within w_riepiloghi_test_prodotti
int X=1829
int Y=1401
int Width=366
int Height=81
int TabOrder=50
string Text="Gr. Ca&pacità"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long ll_progressivo
string ls_codice[],ls_cod_test,ls_cod_prodotto

ll_progressivo = f_po_selectlb(lb_2,ls_codice[])
ls_cod_test = ls_codice[ll_progressivo]
s_cs_xx.parametri.parametro_s_1=ls_cod_test

ll_progressivo = f_po_selectlb(lb_1,ls_codice[])
ls_cod_prodotto = ls_codice[ll_progressivo]
s_cs_xx.parametri.parametro_s_2=ls_cod_prodotto

window_open(w_grafico_capacita_processo,0)
end on

type cb_3 from commandbutton within w_riepiloghi_test_prodotti
int X=2218
int Y=1401
int Width=366
int Height=81
int TabOrder=60
string Text="Gr.Cent&rat."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long ll_progressivo
string ls_codice[],ls_cod_test,ls_cod_prodotto

ll_progressivo = f_po_selectlb(lb_2,ls_codice[])
ls_cod_test = ls_codice[ll_progressivo]
s_cs_xx.parametri.parametro_s_1=ls_cod_test

ll_progressivo = f_po_selectlb(lb_1,ls_codice[])
ls_cod_prodotto = ls_codice[ll_progressivo]
s_cs_xx.parametri.parametro_s_2=ls_cod_prodotto

window_open(w_grafico_centratura_processo,0)
end on

type cb_5 from commandbutton within w_riepiloghi_test_prodotti
int X=23
int Y=261
int Width=366
int Height=81
int TabOrder=20
string Text="Lista &Prod."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;setpointer(hourglass!)

lb_2.reset()
lb_1.reset()

f_po_loadlb(lb_1, &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda ='" + s_cs_xx.cod_azienda + "' and cod_prodotto in (select cod_prodotto from tab_test_prodotti where cod_azienda='" + s_cs_xx.cod_azienda + "') order by des_prodotto","")

setpointer(arrow!)


end on

type cb_6 from commandbutton within w_riepiloghi_test_prodotti
int X=1349
int Y=261
int Width=366
int Height=81
int TabOrder=30
string Text="Lista &Test"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;setpointer(hourglass!)

lb_2.reset()
lb_1.reset()

f_po_loadlb(lb_2, &
                 sqlca, &
                 "tab_test", &
                 "cod_test", &
                 "des_test", &
                 "cod_test in (select cod_test from tab_test_prodotti where cod_azienda ='"+ s_cs_xx.cod_azienda + "') order by des_test","")

setpointer(arrow!)
end on

type mle_1 from multilineedit within w_riepiloghi_test_prodotti
int X=23
int Y=21
int Width=2583
int Height=221
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleRaised!
string Text="Dopo aver caricato una delle due liste (bottone <lista prodotti> e bottone <lista test>) fare doppio click sul prodotto o sul test desiderato, l'altra lista mostrerà i relativi test o prodotti selezionare il test o il prodotto ottenendo così la combinazione test-prodotto, attivare quindi il grafico desiderato"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

