﻿$PBExportHeader$w_scelta_test_prodotto.srw
$PBExportComments$Window Scelta test prodotto
forward
global type w_scelta_test_prodotto from w_cs_xx_risposta
end type
type dw_scelta_test_prodotto from uo_cs_xx_dw within w_scelta_test_prodotto
end type
type cb_2 from commandbutton within w_scelta_test_prodotto
end type
end forward

global type w_scelta_test_prodotto from w_cs_xx_risposta
int Width=2437
int Height=1241
boolean TitleBar=true
string Title="Selezione Test Prodotto"
dw_scelta_test_prodotto dw_scelta_test_prodotto
cb_2 cb_2
end type
global w_scelta_test_prodotto w_scelta_test_prodotto

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_scelta_test_prodotto.set_dw_options(sqlca,pcca.null_object,c_nodelete + c_nonew + c_nomodify,c_default)
end on

on w_scelta_test_prodotto.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_scelta_test_prodotto=create dw_scelta_test_prodotto
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_scelta_test_prodotto
this.Control[iCurrent+2]=cb_2
end on

on w_scelta_test_prodotto.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_scelta_test_prodotto)
destroy(this.cb_2)
end on

type dw_scelta_test_prodotto from uo_cs_xx_dw within w_scelta_test_prodotto
int X=23
int Y=21
int Width=2355
int Height=1001
int TabOrder=10
string DataObject="d_scelta_test_prodotto"
BorderStyle BorderStyle=StyleLowered!
end type

on doubleclicked;call uo_cs_xx_dw::doubleclicked;if i_extendmode then
	cb_2.triggerevent("clicked")
end if
end on

event pcd_retrieve;call super::pcd_retrieve;long   l_Error
string ls_tipo_valutazione
datetime ldt_data_oggi

choose case s_cs_xx.parametri.parametro_s_11

	case "M","S","X","G"
		ls_tipo_valutazione = "M"

	case "P","D"
		ls_tipo_valutazione = "A"

end choose

ldt_data_oggi = datetime(today())

l_Error = retrieve(s_cs_xx.cod_azienda, &
                   s_cs_xx.parametri.parametro_s_10, &
						 ldt_data_oggi, &
						 ls_tipo_valutazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_2 from commandbutton within w_scelta_test_prodotto
int X=2012
int Y=1041
int Width=366
int Height=81
int TabOrder=20
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)

if dw_scelta_test_prodotto.rowcount()>0 then
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_scelta_test_prodotto.getitemstring(dw_scelta_test_prodotto.getrow(),"cod_test"))
end if

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)

if dw_scelta_test_prodotto.rowcount()>0 then
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, dw_scelta_test_prodotto.getitemdatetime(dw_scelta_test_prodotto.getrow(),"data_validita"))
end if

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)

parent.triggerevent("pc_close")
end event

