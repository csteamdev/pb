﻿$PBExportHeader$w_selezione_piano_campionamento.srw
$PBExportComments$Window che permette la selezione di un piano di campionamento del prodotto corrente, è usata dalla window w_tes_campionamenti
forward
global type w_selezione_piano_campionamento from w_cs_xx_principale
end type
type dw_selezione_piano_campionamento from uo_cs_xx_dw within w_selezione_piano_campionamento
end type
type cb_1 from commandbutton within w_selezione_piano_campionamento
end type
type st_prog_riga_piani from statictext within w_selezione_piano_campionamento
end type
type cb_2 from commandbutton within w_selezione_piano_campionamento
end type
end forward

global type w_selezione_piano_campionamento from w_cs_xx_principale
int Width=2698
int Height=1165
boolean TitleBar=true
string Title="Selezione Test di Campionamento"
dw_selezione_piano_campionamento dw_selezione_piano_campionamento
cb_1 cb_1
st_prog_riga_piani st_prog_riga_piani
cb_2 cb_2
end type
global w_selezione_piano_campionamento w_selezione_piano_campionamento

type variables
string is_cod_prodotto
string is_cod_test
end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_selezione_piano_campionamento.set_dw_options(sqlca, &
                                 				   i_openparm, &
				                                    c_scrollparent + c_nonew + c_nomodify + c_nodelete, &
            				                        c_default)

iuo_dw_main = dw_selezione_piano_campionamento

st_prog_riga_piani.visible = false
end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione_piano_campionamento,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

end on

on w_selezione_piano_campionamento.create
int iCurrent
call w_cs_xx_principale::create
this.dw_selezione_piano_campionamento=create dw_selezione_piano_campionamento
this.cb_1=create cb_1
this.st_prog_riga_piani=create st_prog_riga_piani
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_selezione_piano_campionamento
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=st_prog_riga_piani
this.Control[iCurrent+4]=cb_2
end on

on w_selezione_piano_campionamento.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_selezione_piano_campionamento)
destroy(this.cb_1)
destroy(this.st_prog_riga_piani)
destroy(this.cb_2)
end on

type dw_selezione_piano_campionamento from uo_cs_xx_dw within w_selezione_piano_campionamento
int X=23
int Y=21
int Width=2615
int Height=929
int TabOrder=10
string DataObject="d_selezione_piano_campionamento"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error
string ls_cod_prodotto, ls_cod_piano_campionamento,ls_flag_collaudo_finale

ls_cod_piano_campionamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_piano_campionamento")
ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_flag_collaudo_finale = s_cs_xx.parametri.parametro_s_1

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_piano_campionamento,ls_cod_prodotto,ls_flag_collaudo_finale)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on doubleclicked;call uo_cs_xx_dw::doubleclicked;cb_1.triggerevent(clicked!)

end on

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
	if getrow() > 0 then
		st_prog_riga_piani.text = string(getitemnumber(getrow(),"prog_riga_piani"))
	end if
end if
end on

type cb_1 from commandbutton within w_selezione_piano_campionamento
int X=1875
int Y=961
int Width=366
int Height=81
int TabOrder=20
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;window_open_parm(w_det_campionamenti, -1, dw_selezione_piano_campionamento.i_parentdw)
end on

type st_prog_riga_piani from statictext within w_selezione_piano_campionamento
int X=453
int Y=965
int Width=700
int Height=73
boolean Enabled=false
string Text="prog_riga_campionamenti"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_2 from commandbutton within w_selezione_piano_campionamento
int X=2263
int Y=961
int Width=366
int Height=81
int TabOrder=30
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)

end on

