﻿$PBExportHeader$cb_piani_campionamento_ricerca.sru
forward
global type cb_piani_campionamento_ricerca from commandbutton
end type
end forward

global type cb_piani_campionamento_ricerca from commandbutton
integer width = 73
integer height = 80
integer taborder = 1
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type
global cb_piani_campionamento_ricerca cb_piani_campionamento_ricerca

event clicked;if not isvalid(w_piani_campionamento_ricerca) then
   window_open(w_piani_campionamento_ricerca, 0)
end if

w_piani_campionamento_ricerca.show()
end event

on cb_piani_campionamento_ricerca.create
end on

on cb_piani_campionamento_ricerca.destroy
end on

