﻿$PBExportHeader$w_consulta_test_prodotto.srw
forward
global type w_consulta_test_prodotto from window
end type
type dw_consulta from datawindow within w_consulta_test_prodotto
end type
end forward

global type w_consulta_test_prodotto from window
integer width = 2149
integer height = 1876
boolean titlebar = true
string title = "Consulta Test/Prodotto"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_consulta dw_consulta
end type
global w_consulta_test_prodotto w_consulta_test_prodotto

on w_consulta_test_prodotto.create
this.dw_consulta=create dw_consulta
this.Control[]={this.dw_consulta}
end on

on w_consulta_test_prodotto.destroy
destroy(this.dw_consulta)
end on

event open;s_test_prodotti ls_test_prodotti
long ll_i, ll_backcolor

ls_test_prodotti = Message.powerobjectparm

ll_backcolor = 12632256 //silver

dw_consulta.SettransObject(SQLCA)

dw_consulta.retrieve(s_cs_xx.cod_azienda, &
								ls_test_prodotti.s_cod_prodotto, &
								ls_test_prodotti.s_cod_test, &
								ls_test_prodotti.dt_validita)
								
dw_consulta.Object.cod_prodotto.Protect = 1
dw_consulta.Object.cod_test.Protect = 1
dw_consulta.Object.data_validita.Protect = 1
dw_consulta.Object.des_test.Protect = 1
dw_consulta.Object.cod_cat_attrezzature.Protect = 1
dw_consulta.Object.cod_attrezzatura.Protect = 1
dw_consulta.Object.tol_minima.Protect = 1
dw_consulta.Object.tol_massima.Protect = 1
dw_consulta.Object.val_medio.Protect = 1
dw_consulta.Object.cod_misura.Protect = 1
dw_consulta.Object.flag_aql.Protect = 1
dw_consulta.Object.scostamento.Protect = 1
dw_consulta.Object.varianza_popolazione.Protect = 1
dw_consulta.Object.media_popolazione.Protect = 1
dw_consulta.Object.flag_vincolante.Protect = 1
dw_consulta.Object.flag_tipo_ipotesi.Protect = 1
dw_consulta.Object.cod_aql.Protect = 1
dw_consulta.Object.flag_chiusura.Protect = 1
dw_consulta.Object.flag_tipologia_durata.Protect = 1
dw_consulta.Object.valore_durata.Protect = 1
dw_consulta.Object.note.Protect = 1

dw_consulta.Object.cod_prodotto.Background.Color = ll_backcolor
dw_consulta.Object.cod_test.Background.Color = ll_backcolor
dw_consulta.Object.data_validita.Background.Color = ll_backcolor
dw_consulta.Object.des_test.Background.Color = ll_backcolor
dw_consulta.Object.cod_cat_attrezzature.Background.Color = ll_backcolor
dw_consulta.Object.cod_attrezzatura.Background.Color = ll_backcolor
dw_consulta.Object.tol_minima.Background.Color = ll_backcolor
dw_consulta.Object.tol_massima.Background.Color = ll_backcolor
dw_consulta.Object.val_medio.Background.Color = ll_backcolor
dw_consulta.Object.cod_misura.Background.Color = ll_backcolor
dw_consulta.Object.scostamento.Background.Color = ll_backcolor
dw_consulta.Object.varianza_popolazione.Background.Color = ll_backcolor
dw_consulta.Object.media_popolazione.Background.Color = ll_backcolor
dw_consulta.Object.flag_tipo_ipotesi.Background.Color = ll_backcolor
dw_consulta.Object.cod_aql.Background.Color = ll_backcolor
dw_consulta.Object.flag_tipologia_durata.Background.Color = ll_backcolor
dw_consulta.Object.valore_durata.Background.Color = ll_backcolor
dw_consulta.Object.note.Background.Color = ll_backcolor

//f_PO_LoadDDDW_DW(dw_consulta,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_consulta,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

f_PO_LoadDDDW_DW(dw_consulta,"cod_tipo_test",sqlca,&
                 "tab_tipo_test","cod_tipo_test","des_tipo_test","")

f_PO_LoadDDDW_DW(dw_consulta,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_consulta,"cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_consulta,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_consulta,"cod_aql",sqlca,&
                 "tes_aql","cod_aql","des_aql",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_consulta from datawindow within w_consulta_test_prodotto
integer x = 23
integer y = 20
integer width = 2103
integer height = 1720
string title = "none"
string dataobject = "d_tab_test_prodotti_tv"
boolean border = false
boolean livescroll = true
end type

