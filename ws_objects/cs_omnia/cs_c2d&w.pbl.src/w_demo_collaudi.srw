﻿$PBExportHeader$w_demo_collaudi.srw
$PBExportComments$Window det_campionamenti
forward
global type w_demo_collaudi from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_demo_collaudi
end type
type cb_1 from commandbutton within w_demo_collaudi
end type
type dw_demo_collaudi_2 from uo_cs_xx_dw within w_demo_collaudi
end type
type dw_demo_collaudi_1 from uo_cs_xx_dw within w_demo_collaudi
end type
type dw_demo_collaudi_3 from datawindow within w_demo_collaudi
end type
end forward

global type w_demo_collaudi from w_cs_xx_principale
integer width = 2395
integer height = 1448
string title = "Gestione Campionamenti"
event ue_retrieve_2 ( )
cb_2 cb_2
cb_1 cb_1
dw_demo_collaudi_2 dw_demo_collaudi_2
dw_demo_collaudi_1 dw_demo_collaudi_1
dw_demo_collaudi_3 dw_demo_collaudi_3
end type
global w_demo_collaudi w_demo_collaudi

type variables
string is_cod_prodotto, is_cod_test
end variables

forward prototypes
public function integer wf_calcola_aql ()
public function integer wf_calcola_aql_c ()
public function integer wf_esito_campionamento ()
end prototypes

event ue_retrieve_2();dw_demo_collaudi_2.triggerevent("retrieve")
end event

public function integer wf_calcola_aql ();//string   ls_cod_aql_n,ls_cod_fornitore,ls_cod_fornitore_stock,ls_cod_reparto, ls_flag_aql,& 
//		   ls_cod_lavorazione,ls_cod_prodotto,ls_cod_test,ls_flag_tipo_campionamento, & 
//			ls_flag_esito,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto
//double   ldd_num_camp_prec_alzare,ldd_num_test_non_sup,ldd_num_cam_prec_abbassare, & 
//		   ldd_num_test_sup,ldd_lim_inferiore_n,ldd_lim_superiore_n, & 
//		   ldd_num_accettazione_n,ldd_num_rifiuto_n, & 
//		   ldd_num_campionaria_n,ldd_numerosita_stock,ldd_giacenza_stock
//integer  li_liv_protezione_n,ll_num_campionamenti_uguali_prec, & 
//			li_anno_reg_camp_max,li_liv_protezione_max,li_liv_protezione_min, & 
//			li_anno_reg_campionamenti
//long     ll_prog_riga_n,ll_num_campionamenti_superati_prec, & 
//			ll_num_reg_camp_max,ll_prog_riga_camp_max,ll_num_cam_prec_alzare, ll_num_test_non_sup, &
//			ll_num_cam_prec_abbassare,ll_num_test_sup,ll_conteggio_tot_abbassare,ll_conteggio_sup, &
//			ll_conteggio_tot_alzare,ll_conteggio_non_sup,ll_prog_riga,ll_prog_riga_confronto, & 
//			ll_prog_stock,ll_num_reg_campionamenti
//datetime ld_oggi,ld_data_validita,ldt_data_stock
//
//tab_1.tabpage_3.st_lim_inferiore_stock_n.text = "0"
//tab_1.tabpage_3.st_lim_superiore_stock_n.text = "0"
//tab_1.tabpage_3.st_num_accettazione_n.text = "0"
//tab_1.tabpage_3.st_num_rifiuto_n.text = "0"
//tab_1.tabpage_3.st_num_campionaria_n.text = "0"
//tab_1.tabpage_3.st_liv_protezione_n.text = "0"
//tab_1.tabpage_3.st_prog_riga_n.text = "0"
//tab_1.tabpage_3.st_num_campionamenti_superati_prec.text = "0"
//tab_1.tabpage_3.st_num_campionamenti_uguali_prec.text = "0"
//
//
//ld_oggi = datetime(today())
//ls_cod_prodotto = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(),"cod_prodotto")
//ls_cod_test = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(),"cod_test")
//
//select max(data_validita),flag_aql,cod_aql
//into   :ld_data_validita,
//		 :ls_flag_aql,
//		 :ls_cod_aql_n
//from   tab_test_prodotti
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_prodotto=:ls_cod_prodotto
//and    data_validita<=:ld_oggi
//and    cod_test=:ls_cod_test
//group by flag_aql,cod_aql;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//	return -1
//end if
//
//if ls_flag_aql = 'N' then return 1
//
//em_quantita.text = "0"
//
//if dw_selezione_piano_campionamento.rowcount() = 0 then return -1
//
//li_anno_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(),"anno_reg_campionamenti")
//ll_num_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(),"num_reg_campionamenti")
//
//select cod_deposito,
//		 cod_ubicazione,
//		 cod_lotto,
//		 data_stock,
//		 prog_stock
//into   :ls_cod_deposito,
//		 :ls_cod_ubicazione,
//		 :ls_cod_lotto,
//		 :ldt_data_stock,
//		 :ll_prog_stock
//from   tes_campionamenti
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_reg_campionamenti=:li_anno_reg_campionamenti
//and    num_reg_campionamenti=:ll_num_reg_campionamenti;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//	return -1
//end if
//
//select giacenza_stock,
//		 cod_fornitore
//into   :ldd_giacenza_stock,
//		 :ls_cod_fornitore_stock
//from   stock
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_prodotto=:ls_cod_prodotto
//and    cod_deposito=:ls_cod_deposito
//and    cod_ubicazione=:ls_cod_ubicazione
//and    cod_lotto=:ls_cod_lotto
//and    data_stock=:ldt_data_stock
//and    prog_stock=:ll_prog_stock;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//	return -1
//end if
//
//ldd_numerosita_stock = double(em_numerosita_stock.text)
//
//if ldd_numerosita_stock = 0 then
//	ldd_numerosita_stock = ldd_giacenza_stock
//end if
//
//if isnull(ldd_numerosita_stock) or ldd_numerosita_stock=0 then
//	messagebox("Omnia","Inserire la numerosità dello stock!",information!)
//	return -1
//end if
//
//choose case st_tipo_piano.text
//	case 'Accettazione'
//		ls_cod_fornitore = ls_cod_fornitore_stock
//
//		if isnull(ls_cod_fornitore) then
//			ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore")
//		end if
//
//		if isnull(ls_cod_fornitore) then 
//			messagebox("Omnia","Selezionare un Fornitore!",information!)
//			return -1
//		end if
//
//		select max(anno_reg_campionamenti)
//		into   :li_anno_reg_camp_max
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_fornitore=:ls_cod_fornitore
//		and    cod_aql=:ls_cod_aql_n;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//		
//		select max(num_reg_campionamenti)
//		into   :ll_num_reg_camp_max
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_fornitore=:ls_cod_fornitore
//		and    cod_aql=:ls_cod_aql_n
//		and    anno_reg_campionamenti=:li_anno_reg_camp_max;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//		
//		select max(prog_riga_campionamenti)
//		into   :ll_prog_riga_camp_max
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_fornitore=:ls_cod_fornitore
//		and    cod_aql=:ls_cod_aql_n
//		and    anno_reg_campionamenti=:li_anno_reg_camp_max
//		and    num_reg_campionamenti=:ll_num_reg_camp_max;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//
//		select prog_riga 
//		into   :ll_prog_riga_n
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_fornitore=:ls_cod_fornitore
//		and    cod_aql=:ls_cod_aql_n
//		and    anno_reg_campionamenti=:li_anno_reg_camp_max
//		and    num_reg_campionamenti=:ll_num_reg_camp_max
//		and    prog_riga_campionamenti=:ll_prog_riga_camp_max;
//
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//
//	case 'Produzione'
//		ls_cod_reparto = dw_scelta_fase_lavorazione_lista.getitemstring(dw_scelta_fase_lavorazione_lista.getrow(),"cod_reparto")
//		ls_cod_lavorazione = dw_scelta_fase_lavorazione_lista.getitemstring(dw_scelta_fase_lavorazione_lista.getrow(),"cod_lavorazione")
//
//		if isnull(ls_cod_reparto) or isnull(ls_cod_lavorazione) then 
//			messagebox("Omnia","Selezionare una fase di lavoro!",information!)
//			return -1
//		end if
//
//		select max(anno_reg_campionamenti)
//		into   :li_anno_reg_camp_max
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_reparto=:ls_cod_reparto
//		and    cod_lavorazione=:ls_cod_lavorazione
//		and    cod_aql=:ls_cod_aql_n;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//		
//		select max(num_reg_campionamenti)
//		into   :ll_num_reg_camp_max
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_reparto=:ls_cod_reparto
//		and    cod_lavorazione=:ls_cod_lavorazione
//		and    cod_aql=:ls_cod_aql_n
//		and    anno_reg_campionamenti=:li_anno_reg_camp_max;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//		
//		select max(prog_riga_campionamenti)
//		into   :ll_prog_riga_camp_max
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_reparto=:ls_cod_reparto
//		and    cod_lavorazione=:ls_cod_lavorazione
//		and    cod_aql=:ls_cod_aql_n
//		and    anno_reg_campionamenti=:li_anno_reg_camp_max
//		and    num_reg_campionamenti=:ll_num_reg_camp_max;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//		
//		select prog_riga 
//		into   :ll_prog_riga_n
//		from   det_campionamenti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_test=:ls_cod_test
//		and    cod_prodotto=:ls_cod_prodotto
//		and    cod_reparto=:ls_cod_reparto
//		and    cod_lavorazione=:ls_cod_lavorazione
//		and    cod_aql=:ls_cod_aql_n
//		and    anno_reg_campionamenti=:li_anno_reg_camp_max
//		and    num_reg_campionamenti=:ll_num_reg_camp_max
//		and    prog_riga_campionamenti=:ll_prog_riga_camp_max;
//
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			return -1
//		end if
//		
//	case 'Finale'
//		ls_flag_tipo_campionamento = "F"
//
//	case else
//		messagebox("Omnia","Test prodotto non identificato!",information!)
//		return -1
//
//end choose
//
//tab_1.tabpage_3.st_cod_aql_n.text = string(ls_cod_aql_n)
//
//if sqlca.sqlcode = 100 then 
//
//	messagebox("Omnia","Verrà utilizzato il livello di protezione predefinito poichè non esistono campionamenti precedenti simili a questo per poter fare i raffronti.",information!)
//	
//	select liv_protezione,
//			 lim_inferiore_stock,
//			 lim_superiore_stock,
//			 num_accettazione,
//			 num_rifiuto,
//			 num_campionaria,
//			 prog_riga
//	into   :li_liv_protezione_n,
//			 :ldd_lim_inferiore_n,
//			 :ldd_lim_superiore_n,
//			 :ldd_num_accettazione_n,
//			 :ldd_num_rifiuto_n,
//			 :ldd_num_campionaria_n,
//			 :ll_prog_riga
//	from   det_aql
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    cod_aql=:ls_cod_aql_n
//	and    flag_predefinito='S'
//	and    lim_inferiore_stock <= :ldd_numerosita_stock
//	and    lim_superiore_stock >= :ldd_numerosita_stock;
//
//	if sqlca.sqlcode < 0 then
//		messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//		return -1
//	end if
//	
//	if sqlca.sqlcode = 100 then
//		messagebox("Omnia","Non è stato definito nessun livello di protezione predefinito con il codice AQL=" + ls_cod_aql_n + ". Non sarà quindi possibile creare un nuovo campionamento con questo AQL", Information!)
//		return -1
//	end if
//
//	tab_1.tabpage_3.st_lim_inferiore_stock_n.text = string(ldd_lim_inferiore_n)
//	tab_1.tabpage_3.st_lim_superiore_stock_n.text = string(ldd_lim_superiore_n)
//	tab_1.tabpage_3.st_num_accettazione_n.text = string(ldd_num_accettazione_n)
//	tab_1.tabpage_3.st_num_rifiuto_n.text = string(ldd_num_rifiuto_n)
//	tab_1.tabpage_3.st_num_campionaria_n.text = string(ldd_num_campionaria_n)
//	tab_1.tabpage_3.st_liv_protezione_n.text = string(li_liv_protezione_n)
//	tab_1.tabpage_3.st_prog_riga_n.text = string(ll_prog_riga)
//	em_quantita.text = string(ldd_num_campionaria_n)
//
//else
//	select num_cam_prec_alzare,
//			 num_test_non_sup,
//			 num_cam_prec_abbassare,
//			 num_test_sup
//	into   :ll_num_cam_prec_alzare,
//			 :ll_num_test_non_sup,	
//			 :ll_num_cam_prec_abbassare,	
//			 :ll_num_test_sup
//	from   tes_aql
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    cod_aql=:ls_cod_aql_n;
//
//	if sqlca.sqlcode < 0 then
//		messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//		return -1
//	end if
//
//	ll_num_campionamenti_uguali_prec=0
//
//	declare righe_conteggio cursor for 
//	select  flag_esito,
//			  prog_riga
//	from    det_campionamenti
//	where   cod_azienda=:s_cs_xx.cod_azienda
//	and     cod_test = :ls_cod_test
//	and     cod_aql=:ls_cod_aql_n
//	and     cod_fornitore=:ls_cod_fornitore
//	order   by anno_reg_campionamenti,num_reg_campionamenti,prog_riga_campionamenti desc;
//	
//	open righe_conteggio;
//
//	do while 1 = 1 
//		fetch righe_conteggio
//		into  :ls_flag_esito,
//				:ll_prog_riga_confronto;
//
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//			close righe_conteggio;
//			return -1
//		end if
//
//		if sqlca.sqlcode = 100 then exit
//		if ll_prog_riga_confronto <> ll_prog_riga_n then exit
//
//		if ll_num_cam_prec_alzare > ll_conteggio_tot_alzare then
//			ll_conteggio_tot_alzare++
//			if ls_flag_esito = 'N' then ll_conteggio_non_sup++		
//		end if
//
//		if ll_num_cam_prec_abbassare > ll_conteggio_tot_abbassare then
//			ll_conteggio_tot_abbassare++
//			if ls_flag_esito = 'S' then ll_conteggio_sup++		
//		end if
//
//		ll_num_campionamenti_uguali_prec++				
//
//	loop
//
//	close righe_conteggio;
//
//	select max(liv_protezione),
//			 min(liv_protezione)
//	into   :li_liv_protezione_max,
//			 :li_liv_protezione_min
//	from   det_aql
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    cod_aql=:ls_cod_aql_n
//	and    lim_inferiore_stock <= :ldd_numerosita_stock
//	and    lim_superiore_stock >= :ldd_numerosita_stock;
//
//	if sqlca.sqlcode < 0 then
//		messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//		return -1
//	end if
//		
//	select liv_protezione
//	into   :li_liv_protezione_n
//	from   det_aql
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    cod_aql=:ls_cod_aql_n
//	and    prog_riga=:ll_prog_riga_n;
//
//	if sqlca.sqlcode < 0 then
//		messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//		return -1
//	end if
//
//	if ll_conteggio_non_sup >= ll_num_test_non_sup then
//		messagebox("Omnia","Il nuovo campionamento utilizzerà un livello di protezione più alto rispetto all'ultimo campionamento uguale effettuato.",information!)
//		li_liv_protezione_n = li_liv_protezione_n + 1
//	else
//		li_liv_protezione_n = li_liv_protezione_n
//	end if
//
//	if li_liv_protezione_n > li_liv_protezione_max then li_liv_protezione_n = li_liv_protezione_max
//
//	if ll_conteggio_sup >= ll_num_test_sup then
//		li_liv_protezione_n = li_liv_protezione_n -1
//		messagebox("Omnia","Il nuovo campionamento utilizzerà un livello di protezione più basso rispetto all'ultimo campionamento uguale effettuato.",information!)
//	else
//		li_liv_protezione_n = li_liv_protezione_n
//	end if
//	
//	if li_liv_protezione_n < li_liv_protezione_min then li_liv_protezione_n = li_liv_protezione_min
//	
//	select lim_inferiore_stock,
//			 lim_superiore_stock,
//			 num_accettazione,
//			 num_rifiuto,
//			 num_campionaria,
//			 prog_riga
//	into   :ldd_lim_inferiore_n,
//			 :ldd_lim_superiore_n,
//			 :ldd_num_accettazione_n,
//			 :ldd_num_rifiuto_n,
//			 :ldd_num_campionaria_n,
//			 :ll_prog_riga
//	from   det_aql
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    cod_aql=:ls_cod_aql_n
//	and	 liv_protezione = :li_liv_protezione_n
//	and    lim_inferiore_stock <= :ldd_numerosita_stock
//	and    lim_superiore_stock >= :ldd_numerosita_stock;
//
//
//	if sqlca.sqlcode < 0 then
//		messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//		return -1
//	end if
//
//	if sqlca.sqlcode = 100 then
//		messagebox("Omnia","Non esiste nessun livello di protezione per questa numerosità di stock: cambiare la numerosità dello stock oppure definirne un livello di protezione in tabella AQL.", stopsign!)
//		return -1
//	end if
//
//	tab_1.tabpage_3.st_lim_inferiore_stock_n.text = string(ldd_lim_inferiore_n)
//	tab_1.tabpage_3.st_lim_superiore_stock_n.text = string(ldd_lim_superiore_n)
//	tab_1.tabpage_3.st_num_accettazione_n.text = string(ldd_num_accettazione_n)
//	tab_1.tabpage_3.st_num_rifiuto_n.text = string(ldd_num_rifiuto_n)
//	tab_1.tabpage_3.st_num_campionaria_n.text = string(ldd_num_campionaria_n)
//	tab_1.tabpage_3.st_liv_protezione_n.text = string(li_liv_protezione_n)
//	tab_1.tabpage_3.st_prog_riga_n.text = string(ll_prog_riga)
//	em_quantita.text = string(ldd_num_campionaria_n)
//	tab_1.tabpage_3.st_num_campionamenti_superati_prec.text = string(ll_conteggio_sup)
//	tab_1.tabpage_3.st_num_campionamenti_uguali_prec.text = string(ll_num_campionamenti_uguali_prec)
//	
////	ll_conteggio_tot_abbassare = 0
////	ll_conteggio_sup = 0
////	ll_conteggio_tot_alzare = 0
////	ll_conteggio_non_sup = 0
//
//end if
//	
return 2
end function

public function integer wf_calcola_aql_c ();//string   ls_cod_aql_c
//double   ldd_lim_inferiore_c,ldd_lim_superiore_c, & 
//		   ldd_num_accettazione_c,ldd_num_rifiuto_c, ldd_num_campionaria_c
//integer  li_liv_protezione_c
//long     ll_prog_riga_c
//
//ls_cod_aql_c = dw_det_campionamenti_lista.getitemstring(dw_det_campionamenti_lista.getrow(),"cod_aql")
//ll_prog_riga_c	= dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"prog_riga")
//
//select liv_protezione,
//		 lim_inferiore_stock,
//		 lim_superiore_stock,
//		 num_accettazione,
//		 num_rifiuto,
//		 num_campionaria
//into   :li_liv_protezione_c,
//		 :ldd_lim_inferiore_c,
//		 :ldd_lim_superiore_c,
//		 :ldd_num_accettazione_c,
//		 :ldd_num_rifiuto_c,
//		 :ldd_num_campionaria_c
//from   det_aql
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_aql=:ls_cod_aql_c
//and    prog_riga=:ll_prog_riga_c;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
//	return 0
//end if
//
//if sqlca.sqlcode = 100 then
//	tab_1.tabpage_2.st_liv_protezione_c.text = "0"
//	tab_1.tabpage_2.st_lim_inferiore_stock_c.text = "0"
//	tab_1.tabpage_2.st_lim_superiore_stock_c.text = "0"
//	tab_1.tabpage_2.st_num_accettazione_c.text = "0"
//	tab_1.tabpage_2.st_num_rifiuto_c.text = "0"
//	tab_1.tabpage_2.st_num_campionaria_c.text = "0"
//else
//	tab_1.tabpage_2.st_liv_protezione_c.text = string(li_liv_protezione_c)
//	tab_1.tabpage_2.st_lim_inferiore_stock_c.text = string(ldd_lim_inferiore_c)
//	tab_1.tabpage_2.st_lim_superiore_stock_c.text = string(ldd_lim_superiore_c)
//	tab_1.tabpage_2.st_num_accettazione_c.text = string(ldd_num_accettazione_c)
//	tab_1.tabpage_2.st_num_rifiuto_c.text = string(ldd_num_rifiuto_c)
//	tab_1.tabpage_2.st_num_campionaria_c.text = string(ldd_num_campionaria_c)
//end if
//
return 0
end function

public function integer wf_esito_campionamento ();//long    ll_num_rifiuto,ll_num_accettazione,ll_num_reg_campionamenti,ll_prog_riga_campionamenti, & 
//	     ll_num_collaudi_non_superati
//integer li_anno_reg_campionamenti
//string  ls_flag_esito,ls_flag_accettazione_ok
//
//ll_num_rifiuto = long(tab_1.tabpage_3.st_num_rifiuto_n.text)
//ll_num_accettazione = long(tab_1.tabpage_3.st_num_accettazione_n.text)
//
//if dw_det_campionamenti_lista.rowcount() > 0 then
//	li_anno_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber( dw_det_campionamenti_lista.getrow(),"anno_reg_campionamenti")
//	ll_num_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"num_reg_campionamenti")
//	ll_prog_riga_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"prog_riga_campionamenti")
//end if
//
//// il campionamento ha esito positivo se il numero di collaudi associati non superati è 
//// minore uguale al numero accettazione 
//// ha esito negativo se il numero di collaudi associati non superati è maggiore uguale al
//// numero rifiuto
//// se, invece, il numero di collaudi associati non superati è compreso tra NA e NR allora
//// il campionamento viene superato, tuttavia il prossimo campionamento utilizzerà un livello
//// di protezione più alto (nella rivista è scritto che si dovrebbe usare il livello di protezione
//// predefinito, secondo me è sbagliato, perchè supponiamo di utilizzare un AQL con 5 livelli 
//// di protezione e come livello predefinito poniamo il tre, se sto già campionando con il 
//// livello 5 e l'esito è negativo, è errato usare il livello di protezione predefinito  che
//// è il 3, dovrò usare almeno il livello 5).
//
//ll_num_collaudi_non_superati = 0
//
//select count(*)
//into   :ll_num_collaudi_non_superati
//from   collaudi
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_reg_campionamenti=:li_anno_reg_campionamenti
//and    num_reg_campionamenti=:ll_num_reg_campionamenti
//and    prog_riga_campionamenti=:ll_prog_riga_campionamenti
//and    flag_esito ='N';
//
//if sqlca.sqlcode < 0 then
//	messagebox("Omnia","Attenzione errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//	return 0
//end if
//
//if ll_num_collaudi_non_superati <= ll_num_accettazione then
//	ls_flag_esito='S'
//end if
//
//if ll_num_collaudi_non_superati >= ll_num_rifiuto then
//	ls_flag_esito = 'N'
//end if
//
//if ll_num_collaudi_non_superati > ll_num_accettazione and ll_num_collaudi_non_superati < ll_num_rifiuto then
//	ls_flag_esito ='S'
//end if
//
//update det_campionamenti
//set    flag_esito =:ls_flag_esito
//where  cod_azienda=:s_cs_xx.cod_Azienda
//and    anno_reg_campionamenti=:li_anno_reg_campionamenti
//and    num_reg_campionamenti=:ll_num_reg_campionamenti
//and    prog_riga_campionamenti=:ll_prog_riga_campionamenti;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Omnia","Attenzione errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//	return 0
//end if
//
//// qui bisognerà agganciare lo stock per poter stabilire in base all'esito del campionamento
//// se bloccare l'utilizzo dello stock oppure se validarne l'utilizzo
//// Esempio: se il numero di collaudi non superati è compreso tra NA e NR allora lo stock
//// potrà essere usato, tuttavia, l'esito del campionamento è negativo, questo per poter 
//// aumentare il livello di protezione del sucessivo campionamento effettuato nelle stesse
//// condizioni
//
return 0
end function

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_test",sqlca,&
//                 "tab_test","cod_test","des_test","")
//
//f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_misura",sqlca,&
//                 "tab_misure","cod_misura","des_misura",&
//                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_det_campionamenti_dett_1,"cod_reparto",sqlca,&
//                 "anag_reparti","cod_reparto","des_reparto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_det_campionamenti_dett_1,"cod_fornitore",sqlca,&
//                 "anag_fornitori","cod_fornitore","rag_soc_1",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
f_PO_LoadDDDW_DW(dw_demo_collaudi_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio"," cognome + ' ' + nome ",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_demo_collaudi.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_demo_collaudi_2=create dw_demo_collaudi_2
this.dw_demo_collaudi_1=create dw_demo_collaudi_1
this.dw_demo_collaudi_3=create dw_demo_collaudi_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_demo_collaudi_2
this.Control[iCurrent+4]=this.dw_demo_collaudi_1
this.Control[iCurrent+5]=this.dw_demo_collaudi_3
end on

on w_demo_collaudi.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_demo_collaudi_2)
destroy(this.dw_demo_collaudi_1)
destroy(this.dw_demo_collaudi_3)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_demo_collaudi_1.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_nomodify + &
											 c_noretrieveonopen + &
                     				 c_nodelete + &
                                  c_newonopen + &
                                  c_disableCC, &
                                  c_noresizedw + &
                                  c_nohighlightselected + &
                                  c_nocursorrowpointer +&
                                  c_nocursorrowfocusrect )
											 
dw_demo_collaudi_2.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
dw_demo_collaudi_3.settransobject(sqlca)
dw_demo_collaudi_3.insertrow(0)
dw_demo_collaudi_3.setitem( 1, "progressivo", 1)
iuo_dw_main = dw_demo_collaudi_1											 
end event

type cb_2 from commandbutton within w_demo_collaudi
integer x = 1966
integer y = 1240
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(parent)

end event

type cb_1 from commandbutton within w_demo_collaudi
integer x = 1577
integer y = 1240
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Con&ferma"
end type

type dw_demo_collaudi_2 from uo_cs_xx_dw within w_demo_collaudi
integer x = 23
integer y = 360
integer width = 983
integer height = 960
integer taborder = 20
string dataobject = "d_demo_collaudi_2"
borderstyle borderstyle = styleraised!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,is_cod_prodotto,is_cod_test)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_demo_collaudi_1 from uo_cs_xx_dw within w_demo_collaudi
integer x = 23
integer y = 20
integer width = 2327
integer height = 320
integer taborder = 20
string dataobject = "d_demo_collaudi_1"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_cod_prodotto, ls_null

is_cod_test=""

is_cod_prodotto=""

choose case i_colname
		
	case "cod_prodotto"
		
		setnull(ls_null)
		setitem( getrow(), "cod_test", ls_null)
		//setitem( getrow(), "cod_piano_campionamento", ls_null)
		
		f_PO_LoadDDDW_DW( dw_demo_collaudi_1, &
		                  "cod_test", &
								sqlca,&
  			               "tab_test_prodotti", &
								"cod_test", &
								"des_test", &
        			         "tab_test_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + i_coltext + "'")

//		f_PO_LoadDDDW_DW( dw_demo_collaudi_1, &
//		                  "cod_piano_campionamento", &
//								sqlca, &
//                        "tab_tipi_piani_campionamento", &
//								"cod_piano_campionamento", &
//								"des_piano_campionamento", &
//					 			"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento in (select cod_piano_campionamento from det_piani_campionamento where cod_azienda = '" + s_cs_xx.cod_azienda + "' and data_validita <= '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "' and cod_prodotto = '" + i_coltext + "' )")		
//		
//	case "cod_piano_campionamento"
//		
//		f_PO_LoadDDDW_DW( dw_demo_collaudi_1, &
//		                  "cod_test", &
//								sqlca,&
//  			               "tab_test_prodotti", &
//								"cod_test", &
//								"des_test", &
//        			         "tab_test_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + i_coltext + "'")
//		is_cod_prodotto = i_coltext
//		is_cod_test = ls_null
//		dw_demo_collaudi_2.change_dw_focus(dw_demo_collaudi_2)
//		parent.triggerevent("pc_retrieve")								

	case "cod_test" 
		
		is_cod_prodotto = getitemstring( getrow(), "cod_prodotto")
		is_cod_test = i_coltext
		dw_demo_collaudi_2.change_dw_focus(dw_demo_collaudi_2)
		parent.triggerevent("pc_retrieve")				
				
end choose		
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_demo_collaudi_1,"cod_prodotto")
end choose
end event

type dw_demo_collaudi_3 from datawindow within w_demo_collaudi
event ue_dwnkey pbm_dwnprocessenter
integer x = 1029
integer y = 360
integer width = 1303
integer height = 860
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_demo_collaudi_3"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dwnkey;long ll_riga

ll_riga = insertrow(0)
setitem( ll_riga, "progressivo", ll_riga)
setitem( ll_riga, "valutato", "N")


end event

event itemchanged;string ls_cod_prodotto,ls_cod_test
double ldd_tol_minima,ldd_tol_massima,ldd_risultato
datetime ldt_oggi

ls_cod_prodotto = is_cod_prodotto 
ls_cod_test = is_cod_test

SELECT tol_minima,
	    tol_massima
INTO 	 :ldd_tol_minima,
	    :ldd_tol_massima
FROM 	 tab_test_prodotti  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
	 	 cod_prodotto = :ls_cod_prodotto AND  
	 	 cod_test = :ls_cod_test ;
	
if sqlca.sqlcode < 0 then
	messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if
	
ldd_risultato = double(data)
	
if ldd_risultato > ldd_tol_minima and ldd_risultato < ldd_tol_massima then 
	setitem( row, "esito", "S")
else
	setitem( row, "esito", "N")
end if

setitem( row, "valutato", "S")
end event

