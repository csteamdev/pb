﻿$PBExportHeader$w_det_aql.srw
$PBExportComments$Window det_aql
forward
global type w_det_aql from w_cs_xx_principale
end type
type dw_det_aql_lista from uo_cs_xx_dw within w_det_aql
end type
type dw_det_aql_det from uo_cs_xx_dw within w_det_aql
end type
end forward

global type w_det_aql from w_cs_xx_principale
int Width=1427
int Height=1189
boolean TitleBar=true
string Title="Dettaglio Piani di Campionamento"
dw_det_aql_lista dw_det_aql_lista
dw_det_aql_det dw_det_aql_det
end type
global w_det_aql w_det_aql

event pc_setwindow;call super::pc_setwindow;dw_det_aql_lista.set_dw_key("cod_azienda")
dw_det_aql_lista.set_dw_key("cod_aql")

dw_det_aql_lista.set_dw_options(sqlca, &
                                i_openparm, &
                                c_scrollparent, &
                                c_default)

dw_det_aql_det.set_dw_options(sqlca,dw_det_aql_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main =dw_det_aql_lista
	

end event

on w_det_aql.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_aql_lista=create dw_det_aql_lista
this.dw_det_aql_det=create dw_det_aql_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_aql_lista
this.Control[iCurrent+2]=dw_det_aql_det
end on

on w_det_aql.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_aql_lista)
destroy(this.dw_det_aql_det)
end on

type dw_det_aql_lista from uo_cs_xx_dw within w_det_aql
int X=1
int Y=1
int Width=1372
int Height=361
int TabOrder=10
string DataObject="d_det_aql_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_new;call super::pcd_new;//if i_extendmode then
//	string ls_modify,ls_test
//	
//
//	ls_modify = "percentuale_campionaria.protect='0~tif(flag_tipo_quantita<>~~'P~~',1,0)'~t"
//	ls_modify = ls_modify + "percentuale_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'P~~',12632256,16777215)'~t"
//	ls_modify = ls_modify + "numerosita_campionaria.protect='0~tif(flag_tipo_quantita<>~~'N~~',1,0)'~t"
//	ls_modify = ls_modify + "numerosita_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'N~~',12632256,16777215)'~t"
//	ls_test = dw_det_piani_campionamenti_dett.modify(ls_modify)
//	setitem(getrow(), "flag_coll_finale",'N')
//	cb_1.enabled = true
//	
//end if
end event

event pcd_setkey;call super::pcd_setkey;LONG     l_Idx
string   ls_cod_aql

ls_cod_aql = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_aql")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_aql", ls_cod_aql)
	end if
NEXT
end event

event pcd_modify;call super::pcd_modify;//if i_extendmode then
//	string ls_modify,ls_test
//	
//	ls_modify = "percentuale_campionaria.protect='0~tif(flag_tipo_quantita<>~~'P~~',1,0)'~t"
//	ls_modify = ls_modify + "percentuale_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'P~~',12632256,16777215)'~t"
//	ls_modify = ls_modify + "numerosita_campionaria.protect='0~tif(flag_tipo_quantita<>~~'N~~',1,0)'~t"
//	ls_modify = ls_modify + "numerosita_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'N~~',12632256,16777215)'~t"
//	ls_test = dw_det_piani_campionamenti_dett.modify(ls_modify)
//
//	cb_1.enabled = true
//end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG     l_Error
string   ls_cod_aql

ls_cod_aql = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_aql")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_aql)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_det_aql_det from uo_cs_xx_dw within w_det_aql
int X=23
int Y=381
int Height=681
int TabOrder=20
string DataObject="d_det_aql_det"
BorderStyle BorderStyle=StyleRaised!
end type

