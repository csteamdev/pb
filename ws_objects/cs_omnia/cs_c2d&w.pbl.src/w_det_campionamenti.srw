﻿$PBExportHeader$w_det_campionamenti.srw
$PBExportComments$Window det_campionamenti
forward
global type w_det_campionamenti from w_cs_xx_principale
end type
type cb_carte_controllo from commandbutton within w_det_campionamenti
end type
type cb_grafici from commandbutton within w_det_campionamenti
end type
type em_a_data from editmask within w_det_campionamenti
end type
type em_da_data from editmask within w_det_campionamenti
end type
type em_quantita from editmask within w_det_campionamenti
end type
type rb_2 from radiobutton within w_det_campionamenti
end type
type st_1 from statictext within w_det_campionamenti
end type
type st_2 from statictext within w_det_campionamenti
end type
type st_3 from statictext within w_det_campionamenti
end type
type gb_3 from groupbox within w_det_campionamenti
end type
type cb_indici from commandbutton within w_det_campionamenti
end type
type cb_formule from commandbutton within w_det_campionamenti
end type
type st_tipo_piano from statictext within w_det_campionamenti
end type
type st_tipo_test_prodotto from statictext within w_det_campionamenti
end type
type cb_tes_campionamenti from commandbutton within w_det_campionamenti
end type
type dw_det_campionamenti_dett_2 from uo_cs_xx_dw within w_det_campionamenti
end type
type rb_1 from radiobutton within w_det_campionamenti
end type
type st_18 from statictext within w_det_campionamenti
end type
type st_19 from statictext within w_det_campionamenti
end type
type em_numerosita_stock from editmask within w_det_campionamenti
end type
type st_23 from statictext within w_det_campionamenti
end type
type gb_1 from groupbox within w_det_campionamenti
end type
type tab_1 from tab within w_det_campionamenti
end type
type tabpage_1 from userobject within tab_1
end type
type st_cod_aql from statictext within tabpage_1
end type
type st_num_cam_prec_alzare from statictext within tabpage_1
end type
type st_num_test_non_sup from statictext within tabpage_1
end type
type st_num_cam_prec_abbassare from statictext within tabpage_1
end type
type st_num_test_sup from statictext within tabpage_1
end type
type st_21 from statictext within tabpage_1
end type
type st_22 from statictext within tabpage_1
end type
type st_24 from statictext within tabpage_1
end type
type st_26 from statictext within tabpage_1
end type
type st_27 from statictext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
st_cod_aql st_cod_aql
st_num_cam_prec_alzare st_num_cam_prec_alzare
st_num_test_non_sup st_num_test_non_sup
st_num_cam_prec_abbassare st_num_cam_prec_abbassare
st_num_test_sup st_num_test_sup
st_21 st_21
st_22 st_22
st_24 st_24
st_26 st_26
st_27 st_27
end type
type tabpage_2 from userobject within tab_1
end type
type st_liv_protezione_c from statictext within tabpage_2
end type
type st_28 from statictext within tabpage_2
end type
type st_lim_inferiore_stock_c from statictext within tabpage_2
end type
type st_lim_superiore_stock_c from statictext within tabpage_2
end type
type st_num_accettazione_c from statictext within tabpage_2
end type
type st_num_rifiuto_c from statictext within tabpage_2
end type
type st_num_campionaria_c from statictext within tabpage_2
end type
type st_31 from statictext within tabpage_2
end type
type st_32 from statictext within tabpage_2
end type
type st_33 from statictext within tabpage_2
end type
type st_34 from statictext within tabpage_2
end type
type st_35 from statictext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
st_liv_protezione_c st_liv_protezione_c
st_28 st_28
st_lim_inferiore_stock_c st_lim_inferiore_stock_c
st_lim_superiore_stock_c st_lim_superiore_stock_c
st_num_accettazione_c st_num_accettazione_c
st_num_rifiuto_c st_num_rifiuto_c
st_num_campionaria_c st_num_campionaria_c
st_31 st_31
st_32 st_32
st_33 st_33
st_34 st_34
st_35 st_35
end type
type tabpage_3 from userobject within tab_1
end type
type st_liv_protezione_n from statictext within tabpage_3
end type
type st_36 from statictext within tabpage_3
end type
type st_37 from statictext within tabpage_3
end type
type st_lim_inferiore_stock_n from statictext within tabpage_3
end type
type st_38 from statictext within tabpage_3
end type
type st_lim_superiore_stock_n from statictext within tabpage_3
end type
type st_39 from statictext within tabpage_3
end type
type st_num_accettazione_n from statictext within tabpage_3
end type
type st_40 from statictext within tabpage_3
end type
type st_num_rifiuto_n from statictext within tabpage_3
end type
type st_41 from statictext within tabpage_3
end type
type st_num_campionaria_n from statictext within tabpage_3
end type
type st_29 from statictext within tabpage_3
end type
type st_num_campionamenti_uguali_prec from statictext within tabpage_3
end type
type st_30 from statictext within tabpage_3
end type
type st_num_campionamenti_superati_prec from statictext within tabpage_3
end type
type st_42 from statictext within tabpage_3
end type
type st_cod_aql_n from statictext within tabpage_3
end type
type st_prog_riga_n from statictext within tabpage_3
end type
type st_43 from statictext within tabpage_3
end type
type tabpage_3 from userobject within tab_1
st_liv_protezione_n st_liv_protezione_n
st_36 st_36
st_37 st_37
st_lim_inferiore_stock_n st_lim_inferiore_stock_n
st_38 st_38
st_lim_superiore_stock_n st_lim_superiore_stock_n
st_39 st_39
st_num_accettazione_n st_num_accettazione_n
st_40 st_40
st_num_rifiuto_n st_num_rifiuto_n
st_41 st_41
st_num_campionaria_n st_num_campionaria_n
st_29 st_29
st_num_campionamenti_uguali_prec st_num_campionamenti_uguali_prec
st_30 st_30
st_num_campionamenti_superati_prec st_num_campionamenti_superati_prec
st_42 st_42
st_cod_aql_n st_cod_aql_n
st_prog_riga_n st_prog_riga_n
st_43 st_43
end type
type tab_1 from tab within w_det_campionamenti
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
type cb_verifica from commandbutton within w_det_campionamenti
end type
type st_4 from statictext within w_det_campionamenti
end type
type dw_det_campionamenti_lista from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_det_campionamenti_dett from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_folder from u_folder within w_det_campionamenti
end type
type dw_lista_collaudi_campionamenti from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_det_campionamenti_dett_1 from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_scelta_fornitore from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_scelta_fase_lavorazione_lista from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_selezione_piano_campionamento from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_scelta_fase_lavorazione_det from uo_cs_xx_dw within w_det_campionamenti
end type
type dw_folder_1 from u_folder within w_det_campionamenti
end type
end forward

global type w_det_campionamenti from w_cs_xx_principale
integer width = 3410
integer height = 1924
string title = "Dettaglio Campionamenti"
cb_carte_controllo cb_carte_controllo
cb_grafici cb_grafici
em_a_data em_a_data
em_da_data em_da_data
em_quantita em_quantita
rb_2 rb_2
st_1 st_1
st_2 st_2
st_3 st_3
gb_3 gb_3
cb_indici cb_indici
cb_formule cb_formule
st_tipo_piano st_tipo_piano
st_tipo_test_prodotto st_tipo_test_prodotto
cb_tes_campionamenti cb_tes_campionamenti
dw_det_campionamenti_dett_2 dw_det_campionamenti_dett_2
rb_1 rb_1
st_18 st_18
st_19 st_19
em_numerosita_stock em_numerosita_stock
st_23 st_23
gb_1 gb_1
tab_1 tab_1
cb_verifica cb_verifica
st_4 st_4
dw_det_campionamenti_lista dw_det_campionamenti_lista
dw_det_campionamenti_dett dw_det_campionamenti_dett
dw_folder dw_folder
dw_lista_collaudi_campionamenti dw_lista_collaudi_campionamenti
dw_det_campionamenti_dett_1 dw_det_campionamenti_dett_1
dw_tes_campionamenti_lista dw_tes_campionamenti_lista
dw_scelta_fornitore dw_scelta_fornitore
dw_scelta_fase_lavorazione_lista dw_scelta_fase_lavorazione_lista
dw_selezione_piano_campionamento dw_selezione_piano_campionamento
dw_scelta_fase_lavorazione_det dw_scelta_fase_lavorazione_det
dw_folder_1 dw_folder_1
end type
global w_det_campionamenti w_det_campionamenti

type variables
boolean ib_accettazioni = false
integer ii_anno_reg_campionamenti
long il_num_reg_campionamenti
string is_cod_fornitore
end variables

forward prototypes
public function integer wf_calcola_aql ()
public function integer wf_calcola_aql_c ()
public function integer wf_esito_campionamento ()
end prototypes

public function integer wf_calcola_aql ();string   ls_cod_aql_n,ls_cod_fornitore,ls_cod_fornitore_stock,ls_cod_reparto, ls_flag_aql,& 
		   ls_cod_lavorazione,ls_cod_prodotto,ls_cod_test,ls_flag_tipo_campionamento, & 
			ls_flag_esito,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto
double   ldd_num_camp_prec_alzare,ldd_num_test_non_sup,ldd_num_cam_prec_abbassare, & 
		   ldd_num_test_sup,ldd_lim_inferiore_n,ldd_lim_superiore_n, & 
		   ldd_num_accettazione_n,ldd_num_rifiuto_n, & 
		   ldd_num_campionaria_n,ldd_numerosita_stock,ldd_giacenza_stock
integer  li_liv_protezione_n,ll_num_campionamenti_uguali_prec, & 
			li_anno_reg_camp_max,li_liv_protezione_max,li_liv_protezione_min, & 
			li_anno_reg_campionamenti
long     ll_prog_riga_n,ll_num_campionamenti_superati_prec, & 
			ll_num_reg_camp_max,ll_prog_riga_camp_max,ll_num_cam_prec_alzare, ll_num_test_non_sup, &
			ll_num_cam_prec_abbassare,ll_num_test_sup,ll_conteggio_tot_abbassare,ll_conteggio_sup, &
			ll_conteggio_tot_alzare,ll_conteggio_non_sup,ll_prog_riga,ll_prog_riga_confronto, & 
			ll_prog_stock,ll_num_reg_campionamenti
datetime ld_oggi,ld_data_validita,ldt_data_stock

tab_1.tabpage_3.st_lim_inferiore_stock_n.text = "0"
tab_1.tabpage_3.st_lim_superiore_stock_n.text = "0"
tab_1.tabpage_3.st_num_accettazione_n.text = "0"
tab_1.tabpage_3.st_num_rifiuto_n.text = "0"
tab_1.tabpage_3.st_num_campionaria_n.text = "0"
tab_1.tabpage_3.st_liv_protezione_n.text = "0"
tab_1.tabpage_3.st_prog_riga_n.text = "0"
tab_1.tabpage_3.st_num_campionamenti_superati_prec.text = "0"
tab_1.tabpage_3.st_num_campionamenti_uguali_prec.text = "0"


ld_oggi = datetime(today())
ls_cod_prodotto = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(),"cod_prodotto")
ls_cod_test = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(),"cod_test")

select max(data_validita),flag_aql,cod_aql
into   :ld_data_validita,
		 :ls_flag_aql,
		 :ls_cod_aql_n
from   tab_test_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto
and    data_validita<=:ld_oggi
and    cod_test=:ls_cod_test
group by flag_aql,cod_aql;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if

if ls_flag_aql = 'N' then return 1

em_quantita.text = "0"

if dw_selezione_piano_campionamento.rowcount() = 0 then return -1

li_anno_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(),"anno_reg_campionamenti")
ll_num_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(),"num_reg_campionamenti")

select cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 data_stock,
		 prog_stock
into   :ls_cod_deposito,
		 :ls_cod_ubicazione,
		 :ls_cod_lotto,
		 :ldt_data_stock,
		 :ll_prog_stock
from   tes_campionamenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_reg_campionamenti=:li_anno_reg_campionamenti
and    num_reg_campionamenti=:ll_num_reg_campionamenti;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if

select giacenza_stock,
		 cod_fornitore
into   :ldd_giacenza_stock,
		 :ls_cod_fornitore_stock
from   stock
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto
and    cod_deposito=:ls_cod_deposito
and    cod_ubicazione=:ls_cod_ubicazione
and    cod_lotto=:ls_cod_lotto
and    data_stock=:ldt_data_stock
and    prog_stock=:ll_prog_stock;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if

ldd_numerosita_stock = double(em_numerosita_stock.text)

if ldd_numerosita_stock = 0 then
	ldd_numerosita_stock = ldd_giacenza_stock
end if

if isnull(ldd_numerosita_stock) or ldd_numerosita_stock=0 then
	g_mb.messagebox("Omnia","Inserire la numerosità dello stock!",information!)
	return -1
end if

choose case st_tipo_piano.text
	case 'Accettazione'
		ls_cod_fornitore = ls_cod_fornitore_stock

		if isnull(ls_cod_fornitore) then
			ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore")
		end if

		if isnull(ls_cod_fornitore) then 
			g_mb.messagebox("Omnia","Selezionare un Fornitore!",information!)
			return -1
		end if

		select max(anno_reg_campionamenti)
		into   :li_anno_reg_camp_max
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_fornitore=:ls_cod_fornitore
		and    cod_aql=:ls_cod_aql_n;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		select max(num_reg_campionamenti)
		into   :ll_num_reg_camp_max
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_fornitore=:ls_cod_fornitore
		and    cod_aql=:ls_cod_aql_n
		and    anno_reg_campionamenti=:li_anno_reg_camp_max;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		select max(prog_riga_campionamenti)
		into   :ll_prog_riga_camp_max
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_fornitore=:ls_cod_fornitore
		and    cod_aql=:ls_cod_aql_n
		and    anno_reg_campionamenti=:li_anno_reg_camp_max
		and    num_reg_campionamenti=:ll_num_reg_camp_max;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if

		select prog_riga 
		into   :ll_prog_riga_n
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_fornitore=:ls_cod_fornitore
		and    cod_aql=:ls_cod_aql_n
		and    anno_reg_campionamenti=:li_anno_reg_camp_max
		and    num_reg_campionamenti=:ll_num_reg_camp_max
		and    prog_riga_campionamenti=:ll_prog_riga_camp_max;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if

	case 'Produzione'
		ls_cod_reparto = dw_scelta_fase_lavorazione_lista.getitemstring(dw_scelta_fase_lavorazione_lista.getrow(),"cod_reparto")
		ls_cod_lavorazione = dw_scelta_fase_lavorazione_lista.getitemstring(dw_scelta_fase_lavorazione_lista.getrow(),"cod_lavorazione")

		if isnull(ls_cod_reparto) or isnull(ls_cod_lavorazione) then 
			g_mb.messagebox("Omnia","Selezionare una fase di lavoro!",information!)
			return -1
		end if

		select max(anno_reg_campionamenti)
		into   :li_anno_reg_camp_max
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_reparto=:ls_cod_reparto
		and    cod_lavorazione=:ls_cod_lavorazione
		and    cod_aql=:ls_cod_aql_n;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		select max(num_reg_campionamenti)
		into   :ll_num_reg_camp_max
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_reparto=:ls_cod_reparto
		and    cod_lavorazione=:ls_cod_lavorazione
		and    cod_aql=:ls_cod_aql_n
		and    anno_reg_campionamenti=:li_anno_reg_camp_max;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		select max(prog_riga_campionamenti)
		into   :ll_prog_riga_camp_max
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_reparto=:ls_cod_reparto
		and    cod_lavorazione=:ls_cod_lavorazione
		and    cod_aql=:ls_cod_aql_n
		and    anno_reg_campionamenti=:li_anno_reg_camp_max
		and    num_reg_campionamenti=:ll_num_reg_camp_max;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		select prog_riga 
		into   :ll_prog_riga_n
		from   det_campionamenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_test=:ls_cod_test
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_reparto=:ls_cod_reparto
		and    cod_lavorazione=:ls_cod_lavorazione
		and    cod_aql=:ls_cod_aql_n
		and    anno_reg_campionamenti=:li_anno_reg_camp_max
		and    num_reg_campionamenti=:ll_num_reg_camp_max
		and    prog_riga_campionamenti=:ll_prog_riga_camp_max;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
	case 'Finale'
		ls_flag_tipo_campionamento = "F"

	case else
		g_mb.messagebox("Omnia","Test prodotto non identificato!",information!)
		return -1

end choose

tab_1.tabpage_3.st_cod_aql_n.text = string(ls_cod_aql_n)

if sqlca.sqlcode = 100 then 

	g_mb.messagebox("Omnia","Verrà utilizzato il livello di protezione predefinito poichè non esistono campionamenti precedenti simili a questo per poter fare i raffronti.",information!)
	
	select liv_protezione,
			 lim_inferiore_stock,
			 lim_superiore_stock,
			 num_accettazione,
			 num_rifiuto,
			 num_campionaria,
			 prog_riga
	into   :li_liv_protezione_n,
			 :ldd_lim_inferiore_n,
			 :ldd_lim_superiore_n,
			 :ldd_num_accettazione_n,
			 :ldd_num_rifiuto_n,
			 :ldd_num_campionaria_n,
			 :ll_prog_riga
	from   det_aql
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_aql=:ls_cod_aql_n
	and    flag_predefinito='S'
	and    lim_inferiore_stock <= :ldd_numerosita_stock
	and    lim_superiore_stock >= :ldd_numerosita_stock;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("Omnia","Non è stato definito nessun livello di protezione predefinito con il codice AQL=" + ls_cod_aql_n + ". Non sarà quindi possibile creare un nuovo campionamento con questo AQL", Information!)
		return -1
	end if

	tab_1.tabpage_3.st_lim_inferiore_stock_n.text = string(ldd_lim_inferiore_n)
	tab_1.tabpage_3.st_lim_superiore_stock_n.text = string(ldd_lim_superiore_n)
	tab_1.tabpage_3.st_num_accettazione_n.text = string(ldd_num_accettazione_n)
	tab_1.tabpage_3.st_num_rifiuto_n.text = string(ldd_num_rifiuto_n)
	tab_1.tabpage_3.st_num_campionaria_n.text = string(ldd_num_campionaria_n)
	tab_1.tabpage_3.st_liv_protezione_n.text = string(li_liv_protezione_n)
	tab_1.tabpage_3.st_prog_riga_n.text = string(ll_prog_riga)
	em_quantita.text = string(ldd_num_campionaria_n)

else
	select num_cam_prec_alzare,
			 num_test_non_sup,
			 num_cam_prec_abbassare,
			 num_test_sup
	into   :ll_num_cam_prec_alzare,
			 :ll_num_test_non_sup,	
			 :ll_num_cam_prec_abbassare,	
			 :ll_num_test_sup
	from   tes_aql
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_aql=:ls_cod_aql_n;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	ll_num_campionamenti_uguali_prec=0

	declare righe_conteggio cursor for 
	select  flag_esito,
			  prog_riga
	from    det_campionamenti
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_test = :ls_cod_test
	and     cod_aql=:ls_cod_aql_n
	and     cod_fornitore=:ls_cod_fornitore
	order   by anno_reg_campionamenti,num_reg_campionamenti,prog_riga_campionamenti desc;
	
	open righe_conteggio;

	do while 1 = 1 
		fetch righe_conteggio
		into  :ls_flag_esito,
				:ll_prog_riga_confronto;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			close righe_conteggio;
			return -1
		end if

		if sqlca.sqlcode = 100 then exit
		if ll_prog_riga_confronto <> ll_prog_riga_n then exit

		if ll_num_cam_prec_alzare > ll_conteggio_tot_alzare then
			ll_conteggio_tot_alzare++
			if ls_flag_esito = 'N' then ll_conteggio_non_sup++		
		end if

		if ll_num_cam_prec_abbassare > ll_conteggio_tot_abbassare then
			ll_conteggio_tot_abbassare++
			if ls_flag_esito = 'S' then ll_conteggio_sup++		
		end if

		ll_num_campionamenti_uguali_prec++				

	loop

	close righe_conteggio;

	select max(liv_protezione),
			 min(liv_protezione)
	into   :li_liv_protezione_max,
			 :li_liv_protezione_min
	from   det_aql
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_aql=:ls_cod_aql_n
	and    lim_inferiore_stock <= :ldd_numerosita_stock
	and    lim_superiore_stock >= :ldd_numerosita_stock;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
		
	select liv_protezione
	into   :li_liv_protezione_n
	from   det_aql
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_aql=:ls_cod_aql_n
	and    prog_riga=:ll_prog_riga_n;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	if ll_conteggio_non_sup >= ll_num_test_non_sup then
		g_mb.messagebox("Omnia","Il nuovo campionamento utilizzerà un livello di protezione più alto rispetto all'ultimo campionamento uguale effettuato.",information!)
		li_liv_protezione_n = li_liv_protezione_n + 1
	else
		li_liv_protezione_n = li_liv_protezione_n
	end if

	if li_liv_protezione_n > li_liv_protezione_max then li_liv_protezione_n = li_liv_protezione_max

	if ll_conteggio_sup >= ll_num_test_sup then
		li_liv_protezione_n = li_liv_protezione_n -1
		g_mb.messagebox("Omnia","Il nuovo campionamento utilizzerà un livello di protezione più basso rispetto all'ultimo campionamento uguale effettuato.",information!)
	else
		li_liv_protezione_n = li_liv_protezione_n
	end if
	
	if li_liv_protezione_n < li_liv_protezione_min then li_liv_protezione_n = li_liv_protezione_min
	
	select lim_inferiore_stock,
			 lim_superiore_stock,
			 num_accettazione,
			 num_rifiuto,
			 num_campionaria,
			 prog_riga
	into   :ldd_lim_inferiore_n,
			 :ldd_lim_superiore_n,
			 :ldd_num_accettazione_n,
			 :ldd_num_rifiuto_n,
			 :ldd_num_campionaria_n,
			 :ll_prog_riga
	from   det_aql
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_aql=:ls_cod_aql_n
	and	 liv_protezione = :li_liv_protezione_n
	and    lim_inferiore_stock <= :ldd_numerosita_stock
	and    lim_superiore_stock >= :ldd_numerosita_stock;


	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	if sqlca.sqlcode = 100 then
		g_mb.messagebox("Omnia","Non esiste nessun livello di protezione per questa numerosità di stock: cambiare la numerosità dello stock oppure definirne un livello di protezione in tabella AQL.", stopsign!)
		return -1
	end if

	tab_1.tabpage_3.st_lim_inferiore_stock_n.text = string(ldd_lim_inferiore_n)
	tab_1.tabpage_3.st_lim_superiore_stock_n.text = string(ldd_lim_superiore_n)
	tab_1.tabpage_3.st_num_accettazione_n.text = string(ldd_num_accettazione_n)
	tab_1.tabpage_3.st_num_rifiuto_n.text = string(ldd_num_rifiuto_n)
	tab_1.tabpage_3.st_num_campionaria_n.text = string(ldd_num_campionaria_n)
	tab_1.tabpage_3.st_liv_protezione_n.text = string(li_liv_protezione_n)
	tab_1.tabpage_3.st_prog_riga_n.text = string(ll_prog_riga)
	em_quantita.text = string(ldd_num_campionaria_n)
	tab_1.tabpage_3.st_num_campionamenti_superati_prec.text = string(ll_conteggio_sup)
	tab_1.tabpage_3.st_num_campionamenti_uguali_prec.text = string(ll_num_campionamenti_uguali_prec)
	
//	ll_conteggio_tot_abbassare = 0
//	ll_conteggio_sup = 0
//	ll_conteggio_tot_alzare = 0
//	ll_conteggio_non_sup = 0

end if
	
return 2
end function

public function integer wf_calcola_aql_c ();string   ls_cod_aql_c
double   ldd_lim_inferiore_c,ldd_lim_superiore_c, & 
		   ldd_num_accettazione_c,ldd_num_rifiuto_c, ldd_num_campionaria_c
integer  li_liv_protezione_c
long     ll_prog_riga_c

ls_cod_aql_c = dw_det_campionamenti_lista.getitemstring(dw_det_campionamenti_lista.getrow(),"cod_aql")
ll_prog_riga_c	= dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"prog_riga")

select liv_protezione,
		 lim_inferiore_stock,
		 lim_superiore_stock,
		 num_accettazione,
		 num_rifiuto,
		 num_campionaria
into   :li_liv_protezione_c,
		 :ldd_lim_inferiore_c,
		 :ldd_lim_superiore_c,
		 :ldd_num_accettazione_c,
		 :ldd_num_rifiuto_c,
		 :ldd_num_campionaria_c
from   det_aql
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_aql=:ls_cod_aql_c
and    prog_riga=:ll_prog_riga_c;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return 0
end if

if sqlca.sqlcode = 100 then
	tab_1.tabpage_2.st_liv_protezione_c.text = "0"
	tab_1.tabpage_2.st_lim_inferiore_stock_c.text = "0"
	tab_1.tabpage_2.st_lim_superiore_stock_c.text = "0"
	tab_1.tabpage_2.st_num_accettazione_c.text = "0"
	tab_1.tabpage_2.st_num_rifiuto_c.text = "0"
	tab_1.tabpage_2.st_num_campionaria_c.text = "0"
else
	tab_1.tabpage_2.st_liv_protezione_c.text = string(li_liv_protezione_c)
	tab_1.tabpage_2.st_lim_inferiore_stock_c.text = string(ldd_lim_inferiore_c)
	tab_1.tabpage_2.st_lim_superiore_stock_c.text = string(ldd_lim_superiore_c)
	tab_1.tabpage_2.st_num_accettazione_c.text = string(ldd_num_accettazione_c)
	tab_1.tabpage_2.st_num_rifiuto_c.text = string(ldd_num_rifiuto_c)
	tab_1.tabpage_2.st_num_campionaria_c.text = string(ldd_num_campionaria_c)
end if

return 0
end function

public function integer wf_esito_campionamento ();long    ll_num_rifiuto,ll_num_accettazione,ll_num_reg_campionamenti,ll_prog_riga_campionamenti, & 
	     ll_num_collaudi_non_superati
integer li_anno_reg_campionamenti
string  ls_flag_esito,ls_flag_accettazione_ok

ll_num_rifiuto = long(tab_1.tabpage_3.st_num_rifiuto_n.text)
ll_num_accettazione = long(tab_1.tabpage_3.st_num_accettazione_n.text)

if dw_det_campionamenti_lista.rowcount() > 0 then
	li_anno_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber( dw_det_campionamenti_lista.getrow(),"anno_reg_campionamenti")
	ll_num_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"num_reg_campionamenti")
	ll_prog_riga_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"prog_riga_campionamenti")
end if

// il campionamento ha esito positivo se il numero di collaudi associati non superati è 
// minore uguale al numero accettazione 
// ha esito negativo se il numero di collaudi associati non superati è maggiore uguale al
// numero rifiuto
// se, invece, il numero di collaudi associati non superati è compreso tra NA e NR allora
// il campionamento viene superato, tuttavia il prossimo campionamento utilizzerà un livello
// di protezione più alto (nella rivista è scritto che si dovrebbe usare il livello di protezione
// predefinito, secondo me è sbagliato, perchè supponiamo di utilizzare un AQL con 5 livelli 
// di protezione e come livello predefinito poniamo il tre, se sto già campionando con il 
// livello 5 e l'esito è negativo, è errato usare il livello di protezione predefinito  che
// è il 3, dovrò usare almeno il livello 5).

ll_num_collaudi_non_superati = 0

select count(*)
into   :ll_num_collaudi_non_superati
from   collaudi
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_reg_campionamenti=:li_anno_reg_campionamenti
and    num_reg_campionamenti=:ll_num_reg_campionamenti
and    prog_riga_campionamenti=:ll_prog_riga_campionamenti
and    flag_esito ='N';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Attenzione errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return 0
end if

if ll_num_collaudi_non_superati <= ll_num_accettazione then
	ls_flag_esito='S'
end if

if ll_num_collaudi_non_superati >= ll_num_rifiuto then
	ls_flag_esito = 'N'
end if

if ll_num_collaudi_non_superati > ll_num_accettazione and ll_num_collaudi_non_superati < ll_num_rifiuto then
	ls_flag_esito ='S'
end if

update det_campionamenti
set    flag_esito =:ls_flag_esito
where  cod_azienda=:s_cs_xx.cod_Azienda
and    anno_reg_campionamenti=:li_anno_reg_campionamenti
and    num_reg_campionamenti=:ll_num_reg_campionamenti
and    prog_riga_campionamenti=:ll_prog_riga_campionamenti;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Attenzione errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return 0
end if

// qui bisognerà agganciare lo stock per poter stabilire in base all'esito del campionamento
// se bloccare l'utilizzo dello stock oppure se validarne l'utilizzo
// Esempio: se il numero di collaudi non superati è compreso tra NA e NR allora lo stock
// potrà essere usato, tuttavia, l'esito del campionamento è negativo, questo per poter 
// aumentare il livello di protezione del sucessivo campionamento effettuato nelle stesse
// condizioni

return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects_1[]
windowobject l_objects_2[]
windowobject l_objects_3[]
windowobject l_objects_4[]
windowobject l_objects_5[]

l_objects_1[1] = dw_det_campionamenti_lista
dw_folder.fu_AssignTab(1, "Campionamenti", l_Objects_1[])
l_objects_2[1] = dw_det_campionamenti_dett
dw_folder.fu_AssignTab(2, "Dettaglio &1", l_Objects_2[])
l_objects_3[1] = dw_det_campionamenti_dett_1
dw_folder.fu_AssignTab(3, "Dettaglio &2", l_Objects_3[])
l_objects_4[1] = dw_det_campionamenti_dett_2
dw_folder.fu_AssignTab(4, "&A.Q.L.", l_Objects_4[])
l_objects_5[1] = dw_lista_collaudi_campionamenti
dw_folder.fu_AssignTab(5, "&Collaudi", l_Objects_5[])


l_objects_1[1] = dw_tes_campionamenti_lista
l_objects_1[2] = cb_tes_campionamenti
dw_folder_1.fu_AssignTab(1, "&Scelta Campion.", l_Objects_1[])
l_objects_2[1] = dw_selezione_piano_campionamento
dw_folder_1.fu_AssignTab(2, "&Test Prodotto", l_Objects_2[])
l_objects_3[1] = dw_scelta_fornitore
dw_folder_1.fu_AssignTab(3, "&Fornitore", l_Objects_3[])
l_objects_4[1] = dw_scelta_fase_lavorazione_lista
l_objects_4[2] = dw_scelta_fase_lavorazione_det
dw_folder_1.fu_AssignTab(4, "Fasi &Lavoro", l_Objects_4[])


dw_tes_campionamenti_lista.set_dw_options(sqlca,pcca.null_object,c_nomodify + c_nonew + & 
													   c_nomodify,c_default)

dw_selezione_piano_campionamento.set_dw_options(sqlca,pcca.null_object,c_nodelete + & 
																c_nonew + c_nomodify,c_default)

dw_det_campionamenti_lista.set_dw_key("cod_azienda")
dw_det_campionamenti_lista.set_dw_key("anno_reg_campionamenti")
dw_det_campionamenti_lista.set_dw_key("num_reg_campionamenti")
dw_det_campionamenti_lista.set_dw_key("prog_riga_campionamenti")

dw_det_campionamenti_lista.set_dw_options(sqlca, &
		                                    pcca.null_object, &
      		                              c_scrollparent, &
            		                        c_default)

dw_det_campionamenti_dett.set_dw_options(sqlca,dw_det_campionamenti_lista,c_sharedata + & 
													  c_scrollparent,c_default)

dw_det_campionamenti_dett_1.set_dw_options(sqlca,dw_det_campionamenti_lista,c_sharedata + & 
													  c_scrollparent,c_default)

dw_det_campionamenti_dett_2.set_dw_options(sqlca,dw_det_campionamenti_lista,c_sharedata + & 
													  c_scrollparent,c_default)

dw_lista_collaudi_campionamenti.set_dw_options(sqlca,pcca.null_object, c_nonew + c_nomodify + &
															   c_nodelete,c_default)

dw_scelta_fornitore.set_dw_options(sqlca,pcca.null_object, c_nonew + c_modifyonopen +  & 
											  c_nodelete + c_disableccinsert + c_disablecc,c_default)

dw_scelta_fase_lavorazione_lista.set_dw_options(sqlca,pcca.null_object, c_nonew + & 
																c_nomodify + c_nodelete ,c_default)

dw_scelta_fase_lavorazione_det.set_dw_options(sqlca,dw_scelta_fase_lavorazione_lista, & 
															 c_scrollparent + c_sharedata,c_default)

iuo_dw_main = dw_det_campionamenti_lista

dw_folder.fu_FolderCreate(5,5)
dw_folder.fu_SelectTab(1)

dw_folder_1.fu_FolderCreate(4,4)
dw_folder_1.fu_SelectTab(1)

rb_1.Checked=true
rb_2.Checked=false

em_da_data.text = string(today(),"dd/mm/yyyy")
em_a_data.text = string(today(),"dd/mm/yyyy")

em_da_data.enabled=true
em_a_data.enabled=true
em_quantita.enabled=false

dw_scelta_fornitore.insertrow(0)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_campionamenti_dett_1,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_campionamenti_dett_1,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_scelta_fornitore,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_det_campionamenti.create
int iCurrent
call super::create
this.cb_carte_controllo=create cb_carte_controllo
this.cb_grafici=create cb_grafici
this.em_a_data=create em_a_data
this.em_da_data=create em_da_data
this.em_quantita=create em_quantita
this.rb_2=create rb_2
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.gb_3=create gb_3
this.cb_indici=create cb_indici
this.cb_formule=create cb_formule
this.st_tipo_piano=create st_tipo_piano
this.st_tipo_test_prodotto=create st_tipo_test_prodotto
this.cb_tes_campionamenti=create cb_tes_campionamenti
this.dw_det_campionamenti_dett_2=create dw_det_campionamenti_dett_2
this.rb_1=create rb_1
this.st_18=create st_18
this.st_19=create st_19
this.em_numerosita_stock=create em_numerosita_stock
this.st_23=create st_23
this.gb_1=create gb_1
this.tab_1=create tab_1
this.cb_verifica=create cb_verifica
this.st_4=create st_4
this.dw_det_campionamenti_lista=create dw_det_campionamenti_lista
this.dw_det_campionamenti_dett=create dw_det_campionamenti_dett
this.dw_folder=create dw_folder
this.dw_lista_collaudi_campionamenti=create dw_lista_collaudi_campionamenti
this.dw_det_campionamenti_dett_1=create dw_det_campionamenti_dett_1
this.dw_tes_campionamenti_lista=create dw_tes_campionamenti_lista
this.dw_scelta_fornitore=create dw_scelta_fornitore
this.dw_scelta_fase_lavorazione_lista=create dw_scelta_fase_lavorazione_lista
this.dw_selezione_piano_campionamento=create dw_selezione_piano_campionamento
this.dw_scelta_fase_lavorazione_det=create dw_scelta_fase_lavorazione_det
this.dw_folder_1=create dw_folder_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_carte_controllo
this.Control[iCurrent+2]=this.cb_grafici
this.Control[iCurrent+3]=this.em_a_data
this.Control[iCurrent+4]=this.em_da_data
this.Control[iCurrent+5]=this.em_quantita
this.Control[iCurrent+6]=this.rb_2
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.st_3
this.Control[iCurrent+10]=this.gb_3
this.Control[iCurrent+11]=this.cb_indici
this.Control[iCurrent+12]=this.cb_formule
this.Control[iCurrent+13]=this.st_tipo_piano
this.Control[iCurrent+14]=this.st_tipo_test_prodotto
this.Control[iCurrent+15]=this.cb_tes_campionamenti
this.Control[iCurrent+16]=this.dw_det_campionamenti_dett_2
this.Control[iCurrent+17]=this.rb_1
this.Control[iCurrent+18]=this.st_18
this.Control[iCurrent+19]=this.st_19
this.Control[iCurrent+20]=this.em_numerosita_stock
this.Control[iCurrent+21]=this.st_23
this.Control[iCurrent+22]=this.gb_1
this.Control[iCurrent+23]=this.tab_1
this.Control[iCurrent+24]=this.cb_verifica
this.Control[iCurrent+25]=this.st_4
this.Control[iCurrent+26]=this.dw_det_campionamenti_lista
this.Control[iCurrent+27]=this.dw_det_campionamenti_dett
this.Control[iCurrent+28]=this.dw_folder
this.Control[iCurrent+29]=this.dw_lista_collaudi_campionamenti
this.Control[iCurrent+30]=this.dw_det_campionamenti_dett_1
this.Control[iCurrent+31]=this.dw_tes_campionamenti_lista
this.Control[iCurrent+32]=this.dw_scelta_fornitore
this.Control[iCurrent+33]=this.dw_scelta_fase_lavorazione_lista
this.Control[iCurrent+34]=this.dw_selezione_piano_campionamento
this.Control[iCurrent+35]=this.dw_scelta_fase_lavorazione_det
this.Control[iCurrent+36]=this.dw_folder_1
end on

on w_det_campionamenti.destroy
call super::destroy
destroy(this.cb_carte_controllo)
destroy(this.cb_grafici)
destroy(this.em_a_data)
destroy(this.em_da_data)
destroy(this.em_quantita)
destroy(this.rb_2)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.gb_3)
destroy(this.cb_indici)
destroy(this.cb_formule)
destroy(this.st_tipo_piano)
destroy(this.st_tipo_test_prodotto)
destroy(this.cb_tes_campionamenti)
destroy(this.dw_det_campionamenti_dett_2)
destroy(this.rb_1)
destroy(this.st_18)
destroy(this.st_19)
destroy(this.em_numerosita_stock)
destroy(this.st_23)
destroy(this.gb_1)
destroy(this.tab_1)
destroy(this.cb_verifica)
destroy(this.st_4)
destroy(this.dw_det_campionamenti_lista)
destroy(this.dw_det_campionamenti_dett)
destroy(this.dw_folder)
destroy(this.dw_lista_collaudi_campionamenti)
destroy(this.dw_det_campionamenti_dett_1)
destroy(this.dw_tes_campionamenti_lista)
destroy(this.dw_scelta_fornitore)
destroy(this.dw_scelta_fase_lavorazione_lista)
destroy(this.dw_selezione_piano_campionamento)
destroy(this.dw_scelta_fase_lavorazione_det)
destroy(this.dw_folder_1)
end on

event activate;call super::activate;
//	dw_tes_campionamenti_lista.change_dw_current()
//	triggerevent("pc_retrieve")

end event

type cb_carte_controllo from commandbutton within w_det_campionamenti
integer x = 3008
integer y = 1620
integer width = 334
integer height = 80
integer taborder = 190
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Carte Ctrl."
end type

event clicked;string ls_cod_prodotto

ls_cod_prodotto = dw_det_campionamenti_dett.getitemstring(dw_det_campionamenti_dett.getrow(),"cod_prodotto")

if not isnull(ls_cod_prodotto) then

	string ls_cod_test,ls_cod_carta,ls_test_cod_carta
	long ll_quantita,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti
	datetime ld_data_carta
	
	ls_cod_prodotto = dw_det_campionamenti_dett.getitemstring(dw_det_campionamenti_dett.getrow(),"cod_prodotto")
	ls_cod_test = dw_det_campionamenti_dett.getitemstring(dw_det_campionamenti_dett.getrow(),"cod_test")
	ll_quantita = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"quantita")
	ll_anno_reg_campionamenti = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"anno_reg_campionamenti")
	ll_num_reg_campionamenti = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"num_reg_campionamenti")
	ll_prog_riga_campionamenti = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"prog_riga_campionamenti")

	SELECT cod_carta  
	INTO   :ls_test_cod_carta  
	FROM   carte_controllo  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_test = :ls_cod_test
	AND    cod_prodotto = :ls_cod_prodotto;

	if  isnull(ls_test_cod_carta) or ls_test_cod_carta = "" then
		g_mb.messagebox("Omnia","Attenzione. Non esiste nessuna carta di controllo per questa coppia test-prodotto.",Exclamation!)
		return
	end if

	SELECT collaudi.data_carta,   
   	    collaudi.cod_carta  
	INTO   :ld_data_carta,   
   	    :ls_cod_carta  
	FROM   collaudi  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    anno_reg_campionamenti = :ll_anno_reg_campionamenti
	AND    num_reg_campionamenti = :ll_num_reg_campionamenti 
	AND    prog_riga_campionamenti = :ll_prog_riga_campionamenti 
	AND    cod_test = :ls_cod_test
	AND    cod_prodotto = :ls_cod_prodotto;

	if  ls_cod_carta = "" then
		g_mb.messagebox("Omnia","Attenzione. Non esistono collaudi associati al campionamento. Eseguire dei collaudi prima di invocare la carta di controllo",Exclamation!)
		return
	end if

	if isnull(ls_cod_carta) then
		s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_2 = ls_cod_test
		s_cs_xx.parametri.parametro_s_3 = string(ll_quantita)
		s_cs_xx.parametri.parametro_s_4 = string(ll_anno_reg_campionamenti)
		s_cs_xx.parametri.parametro_s_5 = string(ll_num_reg_campionamenti)
		s_cs_xx.parametri.parametro_s_6 = string(ll_prog_riga_campionamenti)
		window_open(w_carte_controllo_1,0)

	else
		s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_2 = ls_cod_test
		s_cs_xx.parametri.parametro_s_3 = string(ll_quantita)
		s_cs_xx.parametri.parametro_s_4 = string(ll_anno_reg_campionamenti)
		s_cs_xx.parametri.parametro_s_5 = string(ll_num_reg_campionamenti)
		s_cs_xx.parametri.parametro_s_6 = string(ll_prog_riga_campionamenti)
		s_cs_xx.parametri.parametro_s_7 = ls_cod_carta
		s_cs_xx.parametri.parametro_data_1 = ld_data_carta
		window_open(w_carte_controllo_1,0)

	end if	

else
	g_mb.messagebox("Omnia","Attenzione. Non c'è nessun dettaglio campionamento. Creare un dettaglio, associando dei collaudi e poi comandare la carta di controllo.",Exclamation!)

end if
end event

type cb_grafici from commandbutton within w_det_campionamenti
integer x = 3008
integer y = 1716
integer width = 334
integer height = 80
integer taborder = 210
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Grafici"
end type

event clicked;string ls_cod_test,ls_cod_prodotto,ls_flag_direzione
long ll_quantita,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti
long ll_t,ll_inizio_num_registrazione,ll_fine_num_registrazione
double ldd_sommatoria,ldd_media,ldd_sommatoria_1,ldd_z,ldd_livello_significativita,ldd_valore
double ldd_media_popolazione,ldd_varianza_popolazione,ldd_media_stimata,ldd_scarto,ldd_z_cfr
datetime ld_data_validita

ld_data_validita = datetime(today())
ls_cod_prodotto = dw_det_campionamenti_dett.getitemstring(dw_det_campionamenti_dett.getrow(),"cod_prodotto")
ls_cod_test = dw_det_campionamenti_dett.getitemstring(dw_det_campionamenti_dett.getrow(),"cod_test")
ls_flag_direzione = dw_det_campionamenti_dett.getitemstring(dw_det_campionamenti_dett.getrow(),"flag_direzione")
ll_quantita = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"quantita")
ll_anno_reg_campionamenti = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"anno_reg_campionamenti")
ll_num_reg_campionamenti = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"num_reg_campionamenti")
ll_prog_riga_campionamenti = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"prog_riga_campionamenti")
ldd_livello_significativita = dw_det_campionamenti_dett.getitemnumber(dw_det_campionamenti_dett.getrow(),"livello_significativita")

if ll_quantita < 30 then
	g_mb.messagebox("Omnia","Attenzione, la numerosita delle osservazioni e inferiore a 30",Information!)
end if

select min(num_registrazione) into:ll_inizio_num_registrazione 
from 	collaudi 
where cod_azienda=:s_cs_xx.cod_azienda 
and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;
	
if (sqlca.sqlcode = 100) or isnull(ll_inizio_num_registrazione) then
	g_mb.messagebox("Omnia","Attenzione, non esistono campioni collegati al campionamento",Exclamation!)
	return
else
	select max(num_registrazione) into:ll_fine_num_registrazione 
	from 	collaudi 
	where cod_azienda=:s_cs_xx.cod_azienda 
	and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
	and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;
	
	select media_popolazione,varianza_popolazione,val_medio
	into   :ldd_media_popolazione,:ldd_varianza_popolazione,:ldd_media_stimata
	from   tab_test_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda
	and  	 cod_prodotto = :ls_cod_prodotto
	and    cod_test = :ls_cod_test
	and    data_validita <= :ld_data_validita;

	select sum(risultato) into:ldd_sommatoria
	from 	collaudi 
	where cod_azienda=:s_cs_xx.cod_azienda 
	and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
	and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
	and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;
			

	if (ll_quantita = 0) or (ldd_varianza_popolazione = 0) then
		g_mb.messagebox("Omnia","Attenzione! Non è possibile ottenenere il grafico poichè la quantità o la varianza popolazione sono uguali a 0. Verificare la varianza popolazione in Tabella test prodotti.",exclamation!)
		return
	end if

	ldd_media = ldd_sommatoria/ll_quantita

	if (ldd_media_popolazione <> 0) and (ldd_varianza_popolazione <> 0) then
		ldd_z = abs((ldd_media - ldd_media_popolazione)/(ldd_varianza_popolazione/sqrt(ll_quantita)))
	end if

	if (ldd_media_popolazione = 0) and (ldd_varianza_popolazione <> 0) then
		if ldd_media_stimata <> 0 then
			ldd_z = abs((ldd_media - ldd_media_stimata)/(ldd_varianza_popolazione/sqrt(ll_quantita)))
		else
			g_mb.messagebox("Omnia","Attenzione, manca la media stimata e la media popolazione, introdurre pertanto uno dei valori in tabella Test per Prodotto.",exclamation!)
			return
		end if
	end if
	
	if (ldd_media_popolazione = 0) and (ldd_varianza_popolazione = 0) then
		ldd_sommatoria_1 = 0
		for ll_t = ll_inizio_num_registrazione to ll_fine_num_registrazione

				select risultato into:ldd_valore
				from 	 collaudi 
				where  cod_azienda=:s_cs_xx.cod_azienda 
				and 	 anno_reg_campionamenti=:ll_anno_reg_campionamenti
				and 	 num_reg_campionamenti=:ll_num_reg_campionamenti 
				and 	 prog_riga_campionamenti=:ll_prog_riga_campionamenti
				and 	 num_registrazione=:ll_t;

			   ldd_sommatoria_1 = ldd_sommatoria_1	+ (ldd_valore - ldd_media)^2								

		next				
		ldd_scarto = sqrt(ldd_sommatoria_1)/ll_quantita
		ldd_z = abs((ldd_media - ldd_media_stimata)/(ldd_varianza_popolazione/sqrt(ll_quantita)))
	end if
end if

select valore_rif 
into   :ldd_z_cfr
from   tab_costanti
where  strumento_test='FZ'
and	 coefficiente_1=:ldd_livello_significativita;

if ls_flag_direzione="B" then
	ldd_z_cfr=ldd_z_cfr/2
	s_cs_xx.parametri.parametro_s_1="B"
end if

s_cs_xx.parametri.parametro_d_1 = ldd_z
s_cs_xx.parametri.parametro_d_2 = ldd_z_cfr

window_open(w_gaussiana, 0)
end event

type em_a_data from editmask within w_det_campionamenti
integer x = 814
integer y = 1700
integer width = 384
integer height = 80
integer taborder = 250
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "01/01/0000~~31/12/9999"
end type

type em_da_data from editmask within w_det_campionamenti
integer x = 242
integer y = 1700
integer width = 384
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "01/01/0000~~31/12/9999"
end type

type em_quantita from editmask within w_det_campionamenti
integer x = 1477
integer y = 1700
integer width = 357
integer height = 80
integer taborder = 240
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = right!
string mask = "#####"
boolean spin = true
double increment = 1
string minmax = "2~~9999"
end type

type rb_2 from radiobutton within w_det_campionamenti
integer x = 1225
integer y = 1636
integer width = 288
integer height = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Manuale"
end type

on clicked;em_da_data.enabled=false
em_a_data.enabled=false
em_quantita.enabled=true
end on

type st_1 from statictext within w_det_campionamenti
integer x = 37
integer y = 1700
integer width = 197
integer height = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Da data"
boolean focusrectangle = false
end type

type st_2 from statictext within w_det_campionamenti
integer x = 654
integer y = 1700
integer width = 160
integer height = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "A data"
boolean focusrectangle = false
end type

type st_3 from statictext within w_det_campionamenti
integer x = 1225
integer y = 1716
integer width = 242
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Nr.collaudi:"
boolean focusrectangle = false
end type

type gb_3 from groupbox within w_det_campionamenti
integer x = 2528
integer y = 20
integer width = 814
integer height = 196
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Tipo Campionamento"
borderstyle borderstyle = stylelowered!
end type

type cb_indici from commandbutton within w_det_campionamenti
integer x = 2642
integer y = 1716
integer width = 334
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Indici"
end type

event clicked;string ls_cod_test,ls_cod_prodotto,ls_tipo_carta,ls_cod_carta
long ll_quantita,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti,ll_num_osservazioni
long ll_num_sottogruppi_effettivo,ll_inf,ll_fif,ll_t,ll_inizio_num_registrazione,ll_fine_num_registrazione
double ldd_sommatoria,ldd_sommatoria_escursioni,ldd_media,ldd_valore_max,ldd_valore_min
double ldd_escursione,ldd_media_escursione,ldd_sommatoria_escursione,ldd_d2m
double ldd_cp,ldd_cpk,ldd_tol_minima,ldd_tol_massima,ldd_zs,ldd_zi
double ldd_escursione_xmr_sottogruppo[],ldd_valore_xmr[],ldd_media_sottogruppo[]
datetime ld_data_carta,ld_data_validita

setpointer(hourglass!)

ld_data_validita = datetime(today())
ls_cod_prodotto = dw_det_campionamenti_lista.getitemstring(dw_det_campionamenti_lista.getrow(),"cod_prodotto")
ls_cod_test = dw_det_campionamenti_lista.getitemstring(dw_det_campionamenti_lista.getrow(),"cod_test")
ll_quantita = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"quantita")
ll_anno_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"anno_reg_campionamenti")
ll_num_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"num_reg_campionamenti")
ll_prog_riga_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(),"prog_riga_campionamenti")

select tol_minima,tol_massima
into :ldd_tol_minima,:ldd_tol_massima
from tab_test_prodotti
where cod_azienda = :s_cs_xx.cod_azienda
and	cod_prodotto = :ls_cod_prodotto
and   cod_test = :ls_cod_test
and   data_validita <= :ld_data_validita;

declare righe_dettaglio cursor for 
select cod_carta,
		 data_carta 
from   collaudi 
where  collaudi.cod_azienda = :s_cs_xx.cod_azienda   
and    cod_prodotto = :ls_cod_prodotto
and    cod_test = :ls_cod_test
and 	 anno_reg_campionamenti  = :ll_anno_reg_campionamenti
and 	 num_reg_campionamenti   = :ll_num_reg_campionamenti 
and 	 prog_riga_campionamenti = :ll_prog_riga_campionamenti; // eventualmente togliere per 
// il discorso dell'associazione della carta di controllo con la testata campionamenti

open righe_dettaglio;

fetch righe_dettaglio into :ls_cod_carta, :ld_data_carta;

if (sqlca.sqlcode = 100) or isnull(ld_data_carta) or isnull(ls_cod_carta) then 
	g_mb.messagebox("Omnia","Il campionamento non è stato associato a nessuna carta di controllo, pertanto non sarà possibile il calcolo degli indici.",information!)
	close righe_dettaglio;
	return
else 
	select tipo_carta,num_osservazioni 
	into   :ls_tipo_carta,:ll_num_osservazioni
	from   carte_controllo
	where  cod_carta  = :ls_cod_carta
	and    data_carta = :ld_data_carta;
 
	if ll_num_osservazioni=0 then
		g_mb.messagebox("Omnia","Attenzione! La carta di controllo associata: " + ls_cod_carta + " con data " + string(ld_data_carta) + " ha il numero di oservazioni impostato a zero, verificare.", exclamation!)
		close righe_dettaglio;
		return
	end if

	select coefficiente_1 into:ldd_d2m
	from tab_costanti
	where strumento_test = 'D2M'
	and valore_rif = :ll_num_osservazioni;

	if sqlca.sqlcode = 100 then
		g_mb.messagebox("Omnia","Attenzione! Nella tabella costanti non esiste nessun coefficiente_1 con strumento test = 'D2M' che abbia come valore di riferimento il numero di osservazioni necessario:" + string(ll_num_osservazioni), exclamation!)
		close righe_dettaglio;
		return
	end if

	select min(num_registrazione) into:ll_inizio_num_registrazione 
	from 	collaudi 
	where cod_azienda=:s_cs_xx.cod_azienda 
	and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
	and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

	select max(num_registrazione) into:ll_fine_num_registrazione 
	from 	collaudi 
	where cod_azienda=:s_cs_xx.cod_azienda 
	and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
	and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;

	if ll_num_osservazioni = 0 then
		g_mb.messagebox("Omnia","Attenzione! Il numero delle osservazioni (nella carta di controllo) è uguale a zero, pertanto non sarà possibile il calcolo dell'indice.",exclamation!)
		close righe_dettaglio;
		return
	end if

	ll_num_sottogruppi_effettivo = Int(ll_quantita/ll_num_osservazioni)

	ll_inf = ll_inizio_num_registrazione 
	ll_fif = ll_inizio_num_registrazione + ll_num_osservazioni -1
	

	choose case ls_tipo_carta
		
		case "M","S"
			for ll_t = 1 to ll_num_sottogruppi_effettivo

				select max(risultato) into:ldd_valore_max
				from 	 collaudi 
				where  cod_azienda=:s_cs_xx.cod_azienda 
				and 	 anno_reg_campionamenti=:ll_anno_reg_campionamenti
				and 	 num_reg_campionamenti=:ll_num_reg_campionamenti 
				and    prog_riga_campionamenti=:ll_prog_riga_campionamenti
				and    num_registrazione between :ll_inf and :ll_fif;
	
				select min(risultato) into:ldd_valore_min
				from 	collaudi 
				where cod_azienda=:s_cs_xx.cod_azienda 
				and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
				and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
				and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
				and 	num_registrazione between :ll_inf and :ll_fif;	

				ldd_escursione = ldd_valore_max - ldd_valore_min
				ldd_sommatoria_escursione = ldd_sommatoria_escursione + ldd_escursione
				ll_inf = ll_fif + 1
				ll_fif = ll_fif + ll_num_osservazioni
	
			next				

			select sum(risultato) into:ldd_sommatoria
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;
					
			case "X"
				for ll_t = ll_inizio_num_registrazione to ll_fine_num_registrazione

					select risultato into:ldd_valore_xmr[ll_t - ll_inizio_num_registrazione + 1]
					from 	collaudi 
					where cod_azienda=:s_cs_xx.cod_azienda 
					and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
					and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
					and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
					and 	num_registrazione = :ll_t;

					if ll_t = ll_inizio_num_registrazione then
						ldd_escursione_xmr_sottogruppo[ll_t - ll_inizio_num_registrazione + 1] = 0
					else
						ldd_escursione_xmr_sottogruppo[ll_t - ll_inizio_num_registrazione + 1] = abs(ldd_valore_xmr[ll_t - ll_inizio_num_registrazione + 1] - ldd_valore_xmr[ll_t - ll_inizio_num_registrazione])
					end if
						ldd_sommatoria_escursione = ldd_sommatoria_escursione + ldd_escursione_xmr_sottogruppo[ll_t - ll_inizio_num_registrazione + 1]

				next

				select sum(risultato) into:ldd_sommatoria
				from 	collaudi 
				where cod_azienda=:s_cs_xx.cod_azienda 
				and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
				and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
				and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
				and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;

		case "G"
			for ll_t = 1 to ll_num_sottogruppi_effettivo
				select sum(risultato) into:ldd_sommatoria
				from 	collaudi 
				where cod_azienda=:s_cs_xx.cod_azienda 
				and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
				and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
				and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
				and 	num_registrazione between :ll_inf and :ll_fif;
	
				ldd_media_sottogruppo[ll_t] = ldd_sommatoria/ll_num_osservazioni

				if ll_t = 1 then
					ldd_escursione_xmr_sottogruppo[ll_t] = 0
				else
					ldd_escursione_xmr_sottogruppo[ll_t] = abs(ldd_media_sottogruppo[ll_t] - ldd_media_sottogruppo[ll_t - 1])
				end if

				ll_inf = ll_fif + 1
				ll_fif = ll_fif + ll_num_osservazioni
			next	

			select sum(risultato) into:ldd_sommatoria
			from 	collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and 	anno_reg_campionamenti=:ll_anno_reg_campionamenti
			and 	num_reg_campionamenti=:ll_num_reg_campionamenti 
			and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti
			and 	num_registrazione between :ll_inizio_num_registrazione and :ll_fine_num_registrazione;

		case "P","D"
			g_mb.messagebox("Omnia","Il campionamento è stato associato a una carta di controllo Binomiale o di Poisson, pertanto non ha senso il calcolo degli indici.",information!)	
			close righe_dettaglio;
			setpointer(arrow!)
			close righe_dettaglio;
			return
	end choose

	if ll_quantita = 0 then
		g_mb.messagebox("Omnia","Attenzione! Non è possibile il calcolo dell'indice, poichè la quantità del campionamento è uguale a zero.",exclamation!)
		close righe_dettaglio;
		return
	end if

	ldd_media = ldd_sommatoria/ll_quantita

	if (ll_num_sottogruppi_effettivo = 0) or (ldd_d2m = 0) then
		g_mb.messagebox("Omnia","Attenzione! Non è possibile il calcolo dell'indice, poichè il numero di sottogruppi effettivo, o  il coefficiente_1 (strumento test D2M della tabella costanti) sono uguali a zero.",exclamation!)
		close righe_dettaglio;
		return
	end if

	ldd_media_escursione = ldd_sommatoria_escursione/ll_num_sottogruppi_effettivo

	if (ldd_media_escursione = 0) then
		g_mb.messagebox("Omnia","Attenzione! Non è possibile il calcolo dell'indice, poichè la media escursione e uguale a zero.",exclamation!)
		close righe_dettaglio;
		return
	end if

	ldd_cp = (ldd_tol_massima - ldd_tol_minima)/(6*ldd_media_escursione/ldd_d2m)
	ldd_zs = (ldd_tol_massima - ldd_media)*ldd_d2m/ldd_media_escursione
	ldd_zi = (ldd_media - ldd_tol_minima)*ldd_d2m/ldd_media_escursione

	if (ldd_zs<0) or (ldd_zi)<0 then
		g_mb.messagebox("Omnia","Attenzione! Il processo non rientra nei limiti impostati, pertanto non è possibile stimarne la centratura",Exclamation!)
		close righe_dettaglio;
		setpointer(arrow!)
		return
	end if
	ldd_cpk = min(ldd_zs,ldd_zi)
	dw_det_campionamenti_dett.setitem(dw_det_campionamenti_dett.getrow(),"capacita_processo",ldd_cp)
	dw_det_campionamenti_dett.setitem(dw_det_campionamenti_dett.getrow(),"centratura_processo",ldd_cpk)

	
end if

close righe_dettaglio;
setpointer(arrow!)
end event

type cb_formule from commandbutton within w_det_campionamenti
integer x = 2642
integer y = 1620
integer width = 334
integer height = 80
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Formule"
end type

event clicked;s_cs_xx.parametri.parametro_s_15 = "DET_CAMPIONAMENTI"
s_cs_xx.parametri.parametro_dw_1 = dw_det_campionamenti_lista
window_open(w_formule,-1)
end event

type st_tipo_piano from statictext within w_det_campionamenti
integer x = 2784
integer y = 80
integer width = 544
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "Accettazione"
boolean focusrectangle = false
end type

type st_tipo_test_prodotto from statictext within w_det_campionamenti
integer x = 2784
integer y = 140
integer width = 544
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "Test di Ipotesi"
boolean focusrectangle = false
end type

type cb_tes_campionamenti from commandbutton within w_det_campionamenti
event clicked pbm_bnclicked
integer x = 1454
integer y = 540
integer width = 334
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&ampionam."
end type

event clicked;window_open_parm(w_tes_campionamenti,-1,dw_tes_campionamenti_lista)


end event

type dw_det_campionamenti_dett_2 from uo_cs_xx_dw within w_det_campionamenti
event itemchanged pbm_dwnitemchange
integer x = 64
integer y = 836
integer width = 960
integer height = 196
integer taborder = 70
string dataobject = "d_det_campionamenti_dett_2"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	if i_colname="cod_reparto" then
		f_PO_LoadDDDW_DW(dw_det_campionamenti_dett_1,"cod_lavorazione",sqlca,&
   	   	           "tab_lavorazioni","cod_lavorazione","des_lavorazione", &
							  "cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
							  "cod_lavorazione in ( select cod_lavorazione from tes_fasi_lavorazione " + & 
							  " where cod_reparto ='" + i_coltext + "' and cod_prodotto='" + getitemstring(row,"cod_prodotto") + "')")
	end if

end if
end event

type rb_1 from radiobutton within w_det_campionamenti
integer x = 37
integer y = 1636
integer width = 384
integer height = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Da Collaudi"
end type

on clicked;em_da_data.enabled=true
em_a_data.enabled=true
em_quantita.enabled=false
end on

type st_18 from statictext within w_det_campionamenti
integer x = 2560
integer y = 80
integer width = 197
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Luogo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_19 from statictext within w_det_campionamenti
integer x = 2560
integer y = 140
integer width = 197
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Tipo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_numerosita_stock from editmask within w_det_campionamenti
integer x = 2208
integer y = 1636
integer width = 384
integer height = 80
integer taborder = 230
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###.00"
string displaydata = ""
end type

type st_23 from statictext within w_det_campionamenti
integer x = 1801
integer y = 1636
integer width = 407
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Numerosità Stock:"
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_det_campionamenti
integer x = 14
integer y = 1580
integer width = 2597
integer height = 220
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Dati del Campionam."
borderstyle borderstyle = stylelowered!
end type

type tab_1 from tab within w_det_campionamenti
integer x = 2528
integer y = 240
integer width = 814
integer height = 1340
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean raggedright = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 777
integer height = 1216
long backcolor = 79741120
string text = "Tab.AQL"
long tabtextcolor = 16711680
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
st_cod_aql st_cod_aql
st_num_cam_prec_alzare st_num_cam_prec_alzare
st_num_test_non_sup st_num_test_non_sup
st_num_cam_prec_abbassare st_num_cam_prec_abbassare
st_num_test_sup st_num_test_sup
st_21 st_21
st_22 st_22
st_24 st_24
st_26 st_26
st_27 st_27
end type

on tabpage_1.create
this.st_cod_aql=create st_cod_aql
this.st_num_cam_prec_alzare=create st_num_cam_prec_alzare
this.st_num_test_non_sup=create st_num_test_non_sup
this.st_num_cam_prec_abbassare=create st_num_cam_prec_abbassare
this.st_num_test_sup=create st_num_test_sup
this.st_21=create st_21
this.st_22=create st_22
this.st_24=create st_24
this.st_26=create st_26
this.st_27=create st_27
this.Control[]={this.st_cod_aql,&
this.st_num_cam_prec_alzare,&
this.st_num_test_non_sup,&
this.st_num_cam_prec_abbassare,&
this.st_num_test_sup,&
this.st_21,&
this.st_22,&
this.st_24,&
this.st_26,&
this.st_27}
end on

on tabpage_1.destroy
destroy(this.st_cod_aql)
destroy(this.st_num_cam_prec_alzare)
destroy(this.st_num_test_non_sup)
destroy(this.st_num_cam_prec_abbassare)
destroy(this.st_num_test_sup)
destroy(this.st_21)
destroy(this.st_22)
destroy(this.st_24)
destroy(this.st_26)
destroy(this.st_27)
end on

type st_cod_aql from statictext within tabpage_1
integer x = 453
integer y = 148
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_num_cam_prec_alzare from statictext within tabpage_1
integer x = 453
integer y = 252
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_num_test_non_sup from statictext within tabpage_1
integer x = 453
integer y = 352
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_num_cam_prec_abbassare from statictext within tabpage_1
integer x = 453
integer y = 452
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_num_test_sup from statictext within tabpage_1
integer x = 453
integer y = 548
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_21 from statictext within tabpage_1
integer x = 14
integer y = 148
integer width = 425
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Cod. AQL:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_22 from statictext within tabpage_1
integer x = 14
integer y = 252
integer width = 425
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Camp.prec.[+]:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_24 from statictext within tabpage_1
integer x = 14
integer y = 352
integer width = 425
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Camp.non sup.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_26 from statictext within tabpage_1
integer x = 14
integer y = 452
integer width = 425
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Camp.prec. [-]:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_27 from statictext within tabpage_1
integer x = 14
integer y = 548
integer width = 425
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Camp.superati:"
alignment alignment = right!
boolean focusrectangle = false
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 777
integer height = 1216
long backcolor = 79741120
string text = "Corrente"
long tabtextcolor = 16711680
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
st_liv_protezione_c st_liv_protezione_c
st_28 st_28
st_lim_inferiore_stock_c st_lim_inferiore_stock_c
st_lim_superiore_stock_c st_lim_superiore_stock_c
st_num_accettazione_c st_num_accettazione_c
st_num_rifiuto_c st_num_rifiuto_c
st_num_campionaria_c st_num_campionaria_c
st_31 st_31
st_32 st_32
st_33 st_33
st_34 st_34
st_35 st_35
end type

on tabpage_2.create
this.st_liv_protezione_c=create st_liv_protezione_c
this.st_28=create st_28
this.st_lim_inferiore_stock_c=create st_lim_inferiore_stock_c
this.st_lim_superiore_stock_c=create st_lim_superiore_stock_c
this.st_num_accettazione_c=create st_num_accettazione_c
this.st_num_rifiuto_c=create st_num_rifiuto_c
this.st_num_campionaria_c=create st_num_campionaria_c
this.st_31=create st_31
this.st_32=create st_32
this.st_33=create st_33
this.st_34=create st_34
this.st_35=create st_35
this.Control[]={this.st_liv_protezione_c,&
this.st_28,&
this.st_lim_inferiore_stock_c,&
this.st_lim_superiore_stock_c,&
this.st_num_accettazione_c,&
this.st_num_rifiuto_c,&
this.st_num_campionaria_c,&
this.st_31,&
this.st_32,&
this.st_33,&
this.st_34,&
this.st_35}
end on

on tabpage_2.destroy
destroy(this.st_liv_protezione_c)
destroy(this.st_28)
destroy(this.st_lim_inferiore_stock_c)
destroy(this.st_lim_superiore_stock_c)
destroy(this.st_num_accettazione_c)
destroy(this.st_num_rifiuto_c)
destroy(this.st_num_campionaria_c)
destroy(this.st_31)
destroy(this.st_32)
destroy(this.st_33)
destroy(this.st_34)
destroy(this.st_35)
end on

type st_liv_protezione_c from statictext within tabpage_2
integer x = 453
integer y = 148
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_28 from statictext within tabpage_2
integer y = 148
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Liv.Protezione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_lim_inferiore_stock_c from statictext within tabpage_2
integer x = 453
integer y = 252
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_lim_superiore_stock_c from statictext within tabpage_2
integer x = 453
integer y = 352
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_num_accettazione_c from statictext within tabpage_2
integer x = 453
integer y = 452
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_num_rifiuto_c from statictext within tabpage_2
integer x = 453
integer y = 548
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_num_campionaria_c from statictext within tabpage_2
integer x = 453
integer y = 652
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_31 from statictext within tabpage_2
integer y = 252
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Lim.Inf.stock:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_32 from statictext within tabpage_2
integer y = 352
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Lim.Sup.stock:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_33 from statictext within tabpage_2
integer y = 452
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nr.Accettazione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_34 from statictext within tabpage_2
integer y = 548
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nr.Rifiuto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_35 from statictext within tabpage_2
integer y = 652
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nr.campionaria:"
alignment alignment = right!
boolean focusrectangle = false
end type

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 777
integer height = 1216
long backcolor = 79741120
string text = "Nuovo"
long tabtextcolor = 16711680
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
st_liv_protezione_n st_liv_protezione_n
st_36 st_36
st_37 st_37
st_lim_inferiore_stock_n st_lim_inferiore_stock_n
st_38 st_38
st_lim_superiore_stock_n st_lim_superiore_stock_n
st_39 st_39
st_num_accettazione_n st_num_accettazione_n
st_40 st_40
st_num_rifiuto_n st_num_rifiuto_n
st_41 st_41
st_num_campionaria_n st_num_campionaria_n
st_29 st_29
st_num_campionamenti_uguali_prec st_num_campionamenti_uguali_prec
st_30 st_30
st_num_campionamenti_superati_prec st_num_campionamenti_superati_prec
st_42 st_42
st_cod_aql_n st_cod_aql_n
st_prog_riga_n st_prog_riga_n
st_43 st_43
end type

on tabpage_3.create
this.st_liv_protezione_n=create st_liv_protezione_n
this.st_36=create st_36
this.st_37=create st_37
this.st_lim_inferiore_stock_n=create st_lim_inferiore_stock_n
this.st_38=create st_38
this.st_lim_superiore_stock_n=create st_lim_superiore_stock_n
this.st_39=create st_39
this.st_num_accettazione_n=create st_num_accettazione_n
this.st_40=create st_40
this.st_num_rifiuto_n=create st_num_rifiuto_n
this.st_41=create st_41
this.st_num_campionaria_n=create st_num_campionaria_n
this.st_29=create st_29
this.st_num_campionamenti_uguali_prec=create st_num_campionamenti_uguali_prec
this.st_30=create st_30
this.st_num_campionamenti_superati_prec=create st_num_campionamenti_superati_prec
this.st_42=create st_42
this.st_cod_aql_n=create st_cod_aql_n
this.st_prog_riga_n=create st_prog_riga_n
this.st_43=create st_43
this.Control[]={this.st_liv_protezione_n,&
this.st_36,&
this.st_37,&
this.st_lim_inferiore_stock_n,&
this.st_38,&
this.st_lim_superiore_stock_n,&
this.st_39,&
this.st_num_accettazione_n,&
this.st_40,&
this.st_num_rifiuto_n,&
this.st_41,&
this.st_num_campionaria_n,&
this.st_29,&
this.st_num_campionamenti_uguali_prec,&
this.st_30,&
this.st_num_campionamenti_superati_prec,&
this.st_42,&
this.st_cod_aql_n,&
this.st_prog_riga_n,&
this.st_43}
end on

on tabpage_3.destroy
destroy(this.st_liv_protezione_n)
destroy(this.st_36)
destroy(this.st_37)
destroy(this.st_lim_inferiore_stock_n)
destroy(this.st_38)
destroy(this.st_lim_superiore_stock_n)
destroy(this.st_39)
destroy(this.st_num_accettazione_n)
destroy(this.st_40)
destroy(this.st_num_rifiuto_n)
destroy(this.st_41)
destroy(this.st_num_campionaria_n)
destroy(this.st_29)
destroy(this.st_num_campionamenti_uguali_prec)
destroy(this.st_30)
destroy(this.st_num_campionamenti_superati_prec)
destroy(this.st_42)
destroy(this.st_cod_aql_n)
destroy(this.st_prog_riga_n)
destroy(this.st_43)
end on

type st_liv_protezione_n from statictext within tabpage_3
integer x = 453
integer y = 352
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_36 from statictext within tabpage_3
integer y = 352
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Liv.Protezione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_37 from statictext within tabpage_3
integer y = 452
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Lim.Inf.stock:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_lim_inferiore_stock_n from statictext within tabpage_3
integer x = 453
integer y = 452
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_38 from statictext within tabpage_3
integer y = 548
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Lim.Sup.stock:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_lim_superiore_stock_n from statictext within tabpage_3
integer x = 453
integer y = 548
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_39 from statictext within tabpage_3
integer y = 652
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nr.Accettazione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_num_accettazione_n from statictext within tabpage_3
integer x = 453
integer y = 652
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_40 from statictext within tabpage_3
integer y = 752
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nr.Rifiuto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_num_rifiuto_n from statictext within tabpage_3
integer x = 453
integer y = 752
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_41 from statictext within tabpage_3
integer y = 852
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nr.campionaria:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_num_campionaria_n from statictext within tabpage_3
integer x = 453
integer y = 852
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_29 from statictext within tabpage_3
integer y = 948
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Camp.prec.uguali:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_num_campionamenti_uguali_prec from statictext within tabpage_3
integer x = 453
integer y = 948
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_30 from statictext within tabpage_3
integer y = 1052
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "C.prec.superati.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_num_campionamenti_superati_prec from statictext within tabpage_3
integer x = 453
integer y = 1052
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_42 from statictext within tabpage_3
integer y = 148
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Cod.AQL:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_cod_aql_n from statictext within tabpage_3
integer x = 453
integer y = 148
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_prog_riga_n from statictext within tabpage_3
integer x = 453
integer y = 252
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_43 from statictext within tabpage_3
integer y = 252
integer width = 448
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Prog.Riga:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_verifica from commandbutton within w_det_campionamenti
integer x = 2528
integer y = 1716
integer width = 64
integer height = 60
integer taborder = 220
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
string text = "V"
end type

event clicked;wf_calcola_aql()
end event

type st_4 from statictext within w_det_campionamenti
integer x = 1920
integer y = 1716
integer width = 608
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
string text = "Verifica nuovo campionam."
boolean focusrectangle = false
end type

type dw_det_campionamenti_lista from uo_cs_xx_dw within w_det_campionamenti
integer x = 64
integer y = 836
integer width = 1317
integer height = 700
integer taborder = 40
string dataobject = "d_det_campionamenti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then	
	long l_Idx,ll_prog_riga_campionamenti,ll_anno_reg_campionamenti,ll_anno_registrazione
	long ll_num_reg_campionamenti,ll_prog_riga_piani,ll_num_record,ll_t,ll_num_registrazione	
	long ll_prog_stock,ll_prog_riga
	string ls_oggetto_test,ls_cod_test,ls_cod_prodotto,ls_cod_misura,ls_flag_tipo_quantita, ls_cod_fornitore
	string ls_modalita_campionamento,ls_flag_direzione,ls_cod_piano_campionamento,ls_cod_lotto
	string ls_cod_deposito,ls_cod_ubicazione,ls_cod_operaio,ls_cod_tipo_test,ls_tipo_valutazione
	string ls_tipo_attributo,ls_tipo_piano,ls_cod_reparto,ls_cod_lavorazione,ls_cod_aql
	double ldd_numerosita_campionaria,ldd_percentuale_campionaria,ldd_livello_significativita,ldd_quantita
	datetime ld_data_validita, ld_da_data,ld_a_data,ld_data_stock,ld_data_registrazione, & 
				ld_data_validita_piano,ld_oggi
	integer li_risposta



	gb_1.enabled = false
	rb_1.enabled = false
	rb_2.enabled = false
	cb_formule.enabled = false
	cb_grafici.enabled = false	
	cb_carte_controllo.enabled = false	
	cb_tes_campionamenti.enabled = false
	em_da_data.enabled = false
	em_a_data.enabled = false

	setnull(ls_cod_aql)
	setnull(ll_prog_riga)

	li_risposta = wf_calcola_aql()

	if li_risposta = -1 then 
		triggerevent("pcd_delete")
	   triggerevent("pcd_save")
		return
	end if

	if li_risposta = 2 then
		ls_cod_aql = tab_1.tabpage_3.st_cod_aql_n.text
		ll_prog_riga = long (tab_1.tabpage_3.st_prog_riga_n.text)
	end if

	if dw_selezione_piano_campionamento.rowcount()>0 then
		ll_prog_riga_piani = dw_selezione_piano_campionamento.getitemnumber(dw_selezione_piano_campionamento.getrow(), "prog_riga_piani")
	else
		g_mb.messagebox("Omnia","Atttenzione, nessun test-prodotto disponibile per questo piano di campionamento",exclamation!)
		triggerevent("pcd_delete")
	   triggerevent("pcd_save")
		return
	end if

	ls_cod_piano_campionamento = dw_tes_campionamenti_lista.getitemstring(dw_tes_campionamenti_lista.getrow(), "cod_piano_campionamento")
	ls_cod_lotto = dw_tes_campionamenti_lista.getitemstring(dw_tes_campionamenti_lista.getrow(), "cod_lotto")
	ll_num_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "num_reg_campionamenti")
	ll_anno_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "anno_reg_campionamenti")
		
	ll_anno_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "anno_reg_campionamenti") 
   ll_num_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "num_reg_campionamenti")
	ls_cod_deposito = dw_tes_campionamenti_lista.getitemstring(dw_tes_campionamenti_lista.getrow(), "cod_deposito")
	ls_cod_ubicazione = dw_tes_campionamenti_lista.getitemstring(dw_tes_campionamenti_lista.getrow(), "cod_ubicazione")
	ld_data_stock = dw_tes_campionamenti_lista.getitemdatetime(dw_tes_campionamenti_lista.getrow(), "data_stock")
	ll_prog_stock = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "prog_stock")	
	ls_cod_operaio = dw_tes_campionamenti_lista.getitemstring(dw_tes_campionamenti_lista.getrow(), "cod_operaio")	
	ld_data_validita_piano = dw_tes_campionamenti_lista.getitemdatetime(dw_tes_campionamenti_lista.getrow(), "data_validita")
	ld_da_data = datetime(date(em_da_data.text))
	ld_a_data = datetime(date(em_a_data.text))
	ll_anno_registrazione = year(today())
	ld_data_registrazione = datetime(today())

	setnull(ls_cod_fornitore)
	setnull(ls_cod_reparto)
	setnull(ls_cod_lavorazione)

//	choose case st_tipo_piano.text
//		case 'Accettazione'
//			ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore")
//			if isnull(ls_cod_fornitore) then 
//				messagebox("Omnia","Per Fare un campionamento Mil.Std. in accettazione bisogna selezionare il fornitore. Selezionarlo dall'apposito folder!",exclamation!)
//				triggerevent("pcd_delete")
//				triggerevent("pcd_save")
//				return
//			end if	
//
//		case 'Produzione'
//			ls_cod_reparto = dw_scelta_fase_lavorazione_lista.getitemstring(dw_scelta_fase_lavorazione_lista.getrow(),"cod_reparto")
//			ls_cod_lavorazione = dw_scelta_fase_lavorazione_lista.getitemstring(dw_scelta_fase_lavorazione_lista.getrow(),"cod_lavorazione")
//			if isnull(ls_cod_reparto) or isnull(ls_cod_lavorazione) then 
//				messagebox("Omnia","Per Fare un campionamento Mil.Std. in produzione bisogna selezionare una fase di lavoro. Selezionarla dall'apposito folder!",exclamation!)
//				triggerevent("pcd_delete")
//				triggerevent("pcd_save")
//				return
//			end if	
//
//		case 'Finale'
//
//	end choose
//
// il controllo effetuato dal case sopra è imlementato anche in wf_calcola aql, eventualmente
// prima di cancellare il commento verificare

   SELECT oggetto_test,   
          cod_test,   
          cod_prodotto, 
	   	 cod_misura,   
          flag_tipo_quantita,   
          numerosita_campionaria,   
          percentuale_campionaria,   
          livello_significativita,   
          modalita_campionamento,   
          flag_direzione
   INTO   :ls_oggetto_test,   
          :ls_cod_test,   
          :ls_cod_prodotto,   
			 :ls_cod_misura,   
          :ls_flag_tipo_quantita,   
          :ldd_numerosita_campionaria,   
          :ldd_percentuale_campionaria,   
          :ldd_livello_significativita,   
          :ls_modalita_campionamento,   
          :ls_flag_direzione         
   FROM 	 det_piani_campionamento  
   WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
          cod_piano_campionamento = :ls_cod_piano_campionamento AND  
			 data_validita = :ld_data_validita_piano and
          prog_riga_piani = :ll_prog_riga_piani;
	
	if sqlca.sqlcode<0 then
		g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	   triggerevent("pcd_delete")
	   triggerevent("pcd_save")
		return
	end if


	if sqlca.sqlcode=100 then
		g_mb.messagebox("Omnia","Attenzione, nessun test-prodotto disponibile per questo piano di campionamento",exclamation!)
		return
	end if

	ld_oggi = datetime(today())

   SELECT data_validita  
   INTO 	 :ld_data_validita  
   FROM 	 tab_test_prodotti  
   WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
          cod_prodotto = :ls_cod_prodotto AND  
          cod_test = :ls_cod_test and
			 data_validita <= :ld_oggi;

	if sqlca.sqlcode<0 then
		g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	   triggerevent("pcd_delete")
	   triggerevent("pcd_save")
		return
	end if

   if isnull(ld_data_validita) then
     g_mb.messagebox("Omnia","Attenzione! Non esiste l'associazione test-prodotto corrente nella tabella test-prodotti. Assicurarsi che la tabella test-prodotti contenga le informazioni necessarie.", Information!)
	  triggerevent("pcd_delete")
	  triggerevent("pcd_save")
	  return
   end if
     
   select max(prog_riga_campionamenti) into:ll_prog_riga_campionamenti 
	from det_campionamenti 
	where cod_azienda=:s_cs_xx.cod_azienda
	and anno_reg_campionamenti=:ll_anno_reg_campionamenti
	and num_reg_campionamenti=:ll_num_reg_campionamenti;

	if sqlca.sqlcode<0 then
		g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	   triggerevent("pcd_delete")
	   triggerevent("pcd_save")
		return
	end if
	
   if isnull(ll_prog_riga_campionamenti) then
     ll_prog_riga_campionamenti = 1
   else
	  ll_prog_riga_campionamenti++
   end if

	if rb_1.checked = true then
	
		select flag_tipo_piano
		into   :ls_tipo_piano
		from   tes_piani_campionamento
		where  cod_azienda=:s_cs_xx.cod_Azienda
		and    cod_piano_campionamento=:ls_cod_piano_campionamento
		and    data_validita=:ld_data_validita;

		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			triggerevent("pcd_delete")
			triggerevent("pcd_save")
			return
		end if

		// se il tipo piano è in produzione (processo K'zzz) allora non utilizza lo stock 
		// i campionamenti in produzione vengono effettuati sui semilavorati i quali non 
		// appartengono a nessuno stock).

		ldd_quantita = 0

		if ls_tipo_piano='P' then

			select count(*) 
			into :ldd_quantita
			from collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda
			and   cod_test=:ls_cod_test
			and 	cod_prodotto=:ls_cod_prodotto
			and   data_registrazione between:ld_da_data and :ld_a_data;

		else
			select count(*) 
			into :ldd_quantita
			from collaudi 
			where cod_azienda=:s_cs_xx.cod_azienda
			and   cod_test=:ls_cod_test
			and 	cod_prodotto=:ls_cod_prodotto
			and   cod_deposito like :ls_cod_deposito
			and   cod_ubicazione like :ls_cod_ubicazione
			and   cod_lotto like :ls_cod_lotto
			and   data_stock like :ld_data_stock
			and   prog_stock like :ll_prog_stock
			and   data_registrazione between:ld_da_data and :ld_a_data;
	
		end if

		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return
		end if

		if isnull(ldd_quantita) or ldd_quantita = 0 then
			g_mb.messagebox("Omnia","Attenzione! Non vi sono collaudi presenti, tra le date indicate, per questo prodotto con questo test nello stock indicato nel campionamento. Aggiungere dei collaudi dalla gestione collaudi, oppure, inserire il campionamento manualmente, oppure allargare il periodo da considerare.", Information!)			
			triggerevent("pcd_delete")
			triggerevent("pcd_save")
			return			
		end if

		if ldd_quantita < ldd_numerosita_campionaria	then
			g_mb.messagebox("Omnia","Attenzione! Il numero di collaudi è inferiore a quello richiesto dal piano di campionamento",Exclamation!)
		end if
		
		s_cs_xx.parametri.parametro_ul_1 = ll_anno_reg_campionamenti
		s_cs_xx.parametri.parametro_ul_2 = ll_num_reg_campionamenti
		s_cs_xx.parametri.parametro_ul_3 = ll_prog_riga_campionamenti
		s_cs_xx.parametri.parametro_d_1 = ll_prog_stock
		s_cs_xx.parametri.parametro_s_1 = ls_cod_test
		s_cs_xx.parametri.parametro_s_2 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_3 = ls_cod_deposito
		s_cs_xx.parametri.parametro_s_4 = ls_cod_ubicazione
		s_cs_xx.parametri.parametro_s_5 = ls_cod_lotto
		s_cs_xx.parametri.parametro_data_1 = ld_da_data
		s_cs_xx.parametri.parametro_data_2 = ld_a_data
		s_cs_xx.parametri.parametro_data_3 = ld_data_stock

   	SetItem(getrow(), "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(getrow(), "anno_reg_campionamenti", ll_anno_reg_campionamenti)
	 	SetItem(getrow(), "num_reg_campionamenti", ll_num_reg_campionamenti)
		setItem(getrow(), "prog_riga_campionamenti",ll_prog_riga_campionamenti)
		SetItem(getrow(), "oggetto_test",ls_oggetto_test)
		SetItem(getrow(), "cod_test",ls_cod_test)
		SetItem(getrow(), "cod_prodotto",ls_cod_prodotto)
		SetItem(getrow(), "data_validita",ld_data_validita)
		SetItem(getrow(), "cod_misura",ls_cod_misura)
		SetItem(getrow(), "flag_tipo_quantita",ls_flag_tipo_quantita)
		SetItem(getrow(), "numerosita_campionaria",ldd_numerosita_campionaria)
		SetItem(getrow(), "percentuale_campionaria",ldd_percentuale_campionaria)
		SetItem(getrow(), "livello_significativita",ldd_livello_significativita)
		SetItem(getrow(), "modalita_campionamento",ls_modalita_campionamento)
		SetItem(getrow(), "flag_direzione",ls_flag_direzione)
		SetItem(getrow(), "quantita",ldd_quantita)
		SetItem(getrow(), "flag_esito",'N')
		SetItem(getrow(), "flag_collaudo",'S')
		SetItem(getrow(), "cod_fornitore",ls_cod_fornitore)
		SetItem(getrow(), "cod_lavorazione",ls_cod_lavorazione)
		SetItem(getrow(), "cod_reparto",ls_cod_reparto)
		SetItem(getrow(), "cod_aql",ls_cod_aql)
		SetItem(getrow(), "prog_riga",ll_prog_riga)

		
//		triggerevent("pcd_save")

		window_open(w_lista_collaudi,0)
		
		if s_cs_xx.parametri.parametro_i_1 = -1 then
			dw_det_campionamenti_lista.change_dw_current()
			dw_det_campionamenti_lista.resetupdate()
			triggerevent("pcd_delete")
			triggerevent("pcd_save")
			return	
		end iF

	else
		ldd_quantita = long(em_quantita.text)
		if ldd_quantita < 2 then
			g_mb.messagebox("Omnia","Inserire nell'apposito spazio il numero di collaudi che si desidera effettuare.", Information!)			
			triggerevent("pcd_delete")
			triggerevent("pcd_save")
			return
		end if
	
		ll_num_record = int(ldd_quantita)
	
		select max(num_registrazione) into:ll_num_registrazione 
		from collaudi
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	anno_registrazione=:ll_anno_registrazione;

		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		if isnull(ll_num_registrazione) then
			ll_num_registrazione = 1
		else
			ll_num_registrazione++
		end if
		
		s_cs_xx.parametri.parametro_s_2 = string(ll_num_registrazione)
		
		SetItem(getrow(), "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(getrow(), "anno_reg_campionamenti", ll_anno_reg_campionamenti)
		SetItem(getrow(), "num_reg_campionamenti", ll_num_reg_campionamenti)
		SetItem(getrow(), "prog_riga_campionamenti",ll_prog_riga_campionamenti)
		SetItem(getrow(), "oggetto_test",ls_oggetto_test)
		SetItem(getrow(), "cod_test",ls_cod_test)
		SetItem(getrow(), "cod_prodotto",ls_cod_prodotto)
		SetItem(getrow(), "data_validita",ld_data_validita)
		SetItem(getrow(), "cod_misura",ls_cod_misura)
		SetItem(getrow(), "flag_tipo_quantita",ls_flag_tipo_quantita)
		SetItem(getrow(), "numerosita_campionaria",ldd_numerosita_campionaria)
		SetItem(getrow(), "percentuale_campionaria",ldd_percentuale_campionaria)
		SetItem(getrow(), "livello_significativita",ldd_livello_significativita)
		SetItem(getrow(), "modalita_campionamento",ls_modalita_campionamento)
		SetItem(getrow(), "flag_direzione",ls_flag_direzione)
		SetItem(getrow(), "quantita",ldd_quantita)
		SetItem(getrow(), "flag_esito",'N')
		SetItem(getrow(), "flag_collaudo",'N')
		SetItem(getrow(), "cod_fornitore",ls_cod_fornitore)
		SetItem(getrow(), "cod_lavorazione",ls_cod_lavorazione)
		SetItem(getrow(), "cod_reparto",ls_cod_reparto)
		SetItem(getrow(), "cod_aql",ls_cod_aql)
		SetItem(getrow(), "prog_riga",ll_prog_riga)

		this.triggerevent("pcd_save")		
				
		select cod_tipo_test 
		into   :ls_cod_tipo_test
		from	 tab_test
		where  cod_test=:ls_cod_test;

		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			triggerevent("pcd_delete")
			triggerevent("pcd_save")
			return
		end if

		select tipo_valutazione,flag_tipo_attributo 
		into   :ls_tipo_valutazione,
			    :ls_tipo_attributo
		from	 tab_tipo_test
		where cod_tipo_test=:ls_cod_tipo_test;

		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			triggerevent("pcd_delete")
			triggerevent("pcd_save")
			return
		end if

		if ls_tipo_valutazione = "M" or (ls_tipo_valutazione="A" and ls_tipo_attributo =	"P") then

			for ll_t= 1 to ll_num_record
				INSERT INTO collaudi  
      	   				(cod_azienda,   
					         anno_registrazione,   
         					num_registrazione,   
         					data_carta,   
           					cod_carta,   
           					anno_reg_campionamenti,   
	           				num_reg_campionamenti,   
   	        				prog_riga_campionamenti,   
      	     				cod_test,   
         	  				data_validita,   
           					data_registrazione,   
           					cod_prodotto,   
           					cod_deposito,   
          			   	cod_ubicazione,   
	           				cod_lotto,   
   	        				data_stock,   
      	     				prog_stock,   
         	  				cod_operaio,   
           					risultato,   
           					cod_misura,   
           					cod_errore,   
           					flag_esito,   
	           				note,
								cod_pezzo)
  				VALUES 	  (:s_cs_xx.cod_azienda,   
         	 			  :ll_anno_registrazione,   
            	        :ll_num_registrazione,   
               	     null,   
                  	  null,   
	           			  :ll_anno_reg_campionamenti,   
   	        			  :ll_num_reg_campionamenti,   
      	     			  :ll_prog_riga_campionamenti,   
         	  			  :ls_cod_test,   
            	        :ld_data_validita,   
               	     :ld_data_registrazione,   
                  	  :ls_cod_prodotto,   
	                    :ls_cod_deposito,   
   	                 :ls_cod_ubicazione,   
      	              :ls_cod_lotto,   
         	  			  :ld_data_stock,   
           				  :ll_prog_stock,   
           				  :ls_cod_operaio,   
           				  0,   
	           			  :ls_cod_misura,   
   	        			  null,   
      	     			  'S',   
         	  			  null,
							  null )  ;

				if sqlca.sqlcode<0 then
					g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
					rollback;
					return
				end if

		   		
				ll_num_registrazione++
			next
				
			s_cs_xx.parametri.parametro_s_3 = string(ll_num_registrazione)

		else
			s_cs_xx.parametri.parametro_s_3 = string(ll_num_registrazione + ll_num_record)

		end if

		s_cs_xx.parametri.parametro_s_1 = string(ll_anno_registrazione)

		choose case ls_tipo_valutazione
			case "M"
			  s_cs_xx.parametri.parametro_s_4 = ls_cod_prodotto
			  s_cs_xx.parametri.parametro_s_5 = ls_cod_test
			  window_open(w_input_manuale_collaudi_num,0)
  			  if s_cs_xx.parametri.parametro_s_15 = "Annulla" then 
			    triggerevent("pcd_retrieve")			
				 return
			  end if

			case "A"
			  s_cs_xx.parametri.parametro_s_4 = ls_cod_prodotto
			  s_cs_xx.parametri.parametro_s_5 = ls_cod_test

   		  if ls_tipo_attributo = "P" then
	 			  window_open(w_input_manuale_collaudi_attr_b,0)

			  else
				  s_cs_xx.parametri.parametro_s_6 = string(ll_anno_reg_campionamenti)	
				  s_cs_xx.parametri.parametro_s_7 = string(ll_num_reg_campionamenti)	
				  s_cs_xx.parametri.parametro_s_8 = string(ll_prog_riga_campionamenti)	
				  s_cs_xx.parametri.parametro_data_1 = ld_data_validita
				  s_cs_xx.parametri.parametro_s_9 = ls_cod_deposito
				  s_cs_xx.parametri.parametro_s_10 =ls_cod_ubicazione
				  s_cs_xx.parametri.parametro_s_11 = ls_cod_lotto
  				  s_cs_xx.parametri.parametro_data_2 = ld_data_stock
				  s_cs_xx.parametri.parametro_s_12 = string(ll_prog_stock)				
				  s_cs_xx.parametri.parametro_s_13 = ls_cod_operaio				
  				  s_cs_xx.parametri.parametro_s_14 = ls_cod_misura

				  window_open(w_input_manuale_collaudi_attr_p,0)

			  end if

		end choose		
	end if
end if

wf_esito_campionamento()

dw_lista_collaudi_campionamenti.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
end event

event pcd_view;call super::pcd_view;if i_extendmode then	
	gb_1.enabled = true
	rb_1.enabled = true
	rb_2.enabled = true
	cb_carte_controllo.enabled = true
	cb_formule.enabled = true	
	cb_grafici.enabled = true	
	cb_tes_campionamenti.enabled = true
	em_da_data.enabled= true
	em_a_data.enabled= true
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then	
	gb_1.enabled = true
	rb_1.enabled = true
	rb_2.enabled = true
	cb_carte_controllo.enabled = true
	cb_formule.enabled = true	
	cb_grafici.enabled = true	
	cb_tes_campionamenti.enabled = true
	em_da_data.enabled= true
	em_a_data.enabled= true
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_reg_campionamenti,ll_num_reg_campionamenti
string ls_cod_test,ls_cod_prodotto

if dw_tes_campionamenti_lista.rowcount() > 0 then
	dw_lista_collaudi_campionamenti.reset()
	ll_num_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "num_reg_campionamenti")
	ll_anno_reg_campionamenti = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "anno_reg_campionamenti")
	
	if dw_selezione_piano_campionamento.rowcount()>0 then
		ls_cod_prodotto = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(),"cod_prodotto")
		ls_cod_test = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(),"cod_test")
	else
		g_mb.messagebox("Omnia","Atttenzione, nessun test-prodotto disponibile per questo piano di campionamento",exclamation!)
		return
	end if

	l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ls_cod_prodotto,ls_cod_test)

	IF l_Error < 0 THEN
   	PCCA.Error = c_Fatal
	END IF
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then	
	gb_1.enabled = false
	rb_1.enabled = false
	rb_2.enabled = false
	cb_carte_controllo.enabled = false
	cb_formule.enabled = false	
	cb_grafici.enabled = false	
	cb_tes_campionamenti.enabled = false
	em_da_data.enabled= false
	em_a_data.enabled= false
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	integer li_i,li_anno_reg_campionamenti
	long ll_num_reg_campionamenti,ll_prog_riga_campionamenti,ll_null
	string ls_cod_prodotto

	setnull(ll_null)

	for li_i = 1 to this.deletedcount()
      li_anno_reg_campionamenti = getitemnumber(li_i, "anno_reg_campionamenti", delete!, true)
      ll_num_reg_campionamenti = getitemnumber(li_i, "num_reg_campionamenti", delete!, true)
		ll_prog_riga_campionamenti = getitemnumber(li_i, "prog_riga_campionamenti", delete!, true)

		update collaudi
		set    anno_reg_campionamenti=:ll_null,
				 num_reg_campionamenti=:ll_null,	
				 prog_riga_campionamenti=:ll_null
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_reg_campionamenti=:li_anno_reg_campionamenti
		and    num_reg_campionamenti=:ll_num_reg_campionamenti
		and    prog_riga_campionamenti=:ll_prog_riga_campionamenti;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
		
	next

end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendMode then
	f_PO_LoadDDDW_DW(dw_det_campionamenti_dett_1,"cod_lavorazione",sqlca,&
      	           "tab_lavorazioni","cod_lavorazione","des_lavorazione", &
						  "cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
						  "cod_lavorazione in ( select cod_lavorazione from tes_fasi_lavorazione " + & 
						  " where cod_reparto ='" + i_coltext + "' and cod_prodotto='" + getitemstring(currentrow,"cod_prodotto") + "')")
	

	if rowcount() > 0 then	
		dw_lista_collaudi_campionamenti.Change_DW_Current( )
		parent.triggerevent("pc_retrieve")
		wf_calcola_aql_c()
	end if


end if
end event

type dw_det_campionamenti_dett from uo_cs_xx_dw within w_det_campionamenti
integer x = 64
integer y = 836
integer width = 1888
integer height = 700
integer taborder = 80
string dataobject = "d_det_campionamenti_dett"
boolean hscrollbar = true
borderstyle borderstyle = styleraised!
end type

type dw_folder from u_folder within w_det_campionamenti
event po_tabclicked pbm_custom75
integer y = 720
integer width = 2505
integer height = 860
integer taborder = 20
end type

type dw_lista_collaudi_campionamenti from uo_cs_xx_dw within w_det_campionamenti
event rowfocuschanged pbm_dwnrowchange
event updatestart pbm_dwnupdatestart
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
event pcd_save pbm_custom62
event pcd_view pbm_custom75
integer x = 64
integer y = 836
integer width = 2322
integer height = 700
integer taborder = 50
string dataobject = "d_lista_collaudi_campionamenti"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti
string ls_cod_test,ls_cod_prodotto

if dw_det_campionamenti_lista.rowcount() > 0 then
	ll_num_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(), "num_reg_campionamenti")
	ll_anno_reg_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(), "anno_reg_campionamenti")
	ll_prog_riga_campionamenti = dw_det_campionamenti_lista.getitemnumber(dw_det_campionamenti_lista.getrow(), "prog_riga_campionamenti")	

	l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti)

	IF l_Error < 0 THEN
   	PCCA.Error = c_Fatal
	END IF
end if
end event

type dw_det_campionamenti_dett_1 from uo_cs_xx_dw within w_det_campionamenti
integer x = 64
integer y = 836
integer width = 1888
integer height = 700
integer taborder = 60
string dataobject = "d_det_campionamenti_dett_1"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	if i_colname="cod_reparto" then
		f_PO_LoadDDDW_DW(dw_det_campionamenti_dett_1,"cod_lavorazione",sqlca,&
   	   	           "tab_lavorazioni","cod_lavorazione","des_lavorazione", &
							  "cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
							  "cod_lavorazione in ( select cod_lavorazione from tes_fasi_lavorazione " + & 
							  " where cod_reparto ='" + i_coltext + "' and cod_prodotto='" + getitemstring(row,"cod_prodotto") + "')")
	end if

end if
end event

type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_det_campionamenti
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
event pcd_save pbm_custom62
event pcd_view pbm_custom75
integer x = 82
integer y = 140
integer width = 1317
integer height = 500
integer taborder = 100
string dataobject = "d_tes_campionamenti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
	s_cs_xx.parametri.parametro_dw_1 = dw_tes_campionamenti_lista
	window_open_parm(w_tes_campionamenti,-1,dw_tes_campionamenti_lista)
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if ib_accettazioni then
	long ll_rows[]

	ll_rows[1] = this.find(	"anno_reg_campionamenti = " + string(ii_anno_reg_campionamenti) + &
									" and num_reg_campionamenti = " + string(il_num_reg_campionamenti), &
									1, this.rowcount( ))

	this.set_selected_rows(1, ll_rows, c_CheckForChanges, &
                                      c_RefreshChildren, &
                                      c_RefreshView)
	ib_accettazioni = false
	dw_scelta_fornitore.setitem(1, "cod_fornitore", is_cod_fornitore)
	dw_scelta_fornitore.Reset_DW_Modified(c_ResetChildren)
end if
end event

event doubleclicked;call super::doubleclicked;//s_cs_xx.parametri.parametro_dw_1 = dw_tes_campionamenti_lista
//window_open_parm(w_tes_campionamenti,-1,dw_tes_campionamenti_lista)
end event

event clicked;call super::clicked;dw_selezione_piano_campionamento.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

dw_det_campionamenti_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")


dw_scelta_fase_lavorazione_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_cod_piano_campionamento,ls_flag_tipo_piano
	datetime ld_data_validita
	
	if rowcount() > 0 then
		ls_cod_piano_campionamento = getitemstring(getrow(),"cod_piano_campionamento")
		ld_data_validita = getitemdatetime(getrow(),"data_validita")

		select flag_tipo_piano
		into   :ls_flag_tipo_piano
		from   tes_piani_campionamento
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_piano_campionamento=:ls_cod_piano_campionamento
		and    data_validita=:ld_data_validita;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return
		end if
	
		choose case ls_flag_tipo_piano
			case 'A'
				st_tipo_piano.text = "Accettazione"
	
			case 'P'
				st_tipo_piano.text = "Produzione"
	
			case 'F'
				st_tipo_piano.text = "Finale"
	
			case else
				st_tipo_piano.text = ""
	
		end choose
	end if
end if
 
end event

type dw_scelta_fornitore from uo_cs_xx_dw within w_det_campionamenti
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
event rowfocuschanged pbm_dwnrowchange
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
event pcd_modify pbm_custom51
event pcd_save pbm_custom62
event pcd_view pbm_custom75
integer x = 82
integer y = 196
integer width = 2185
integer height = 116
integer taborder = 120
string dataobject = "d_scelta_fornitore"
boolean hscrollbar = true
end type

event losefocus;call super::losefocus;dw_scelta_fornitore.Reset_DW_Modified(c_ResetChildren)
end event

type dw_scelta_fase_lavorazione_lista from uo_cs_xx_dw within w_det_campionamenti
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
event rowfocuschanged pbm_dwnrowchange
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
event pcd_modify pbm_custom51
event pcd_save pbm_custom62
event pcd_view pbm_custom75
integer x = 82
integer y = 140
integer width = 1225
integer height = 500
integer taborder = 110
string dataobject = "d_scelta_fase_lavorazione_lista"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_prodotto

if dw_tes_campionamenti_lista.rowcount() > 0 then
	ls_cod_prodotto = dw_tes_campionamenti_lista.getitemstring(dw_tes_campionamenti_lista.getrow(), "cod_prodotto")

	l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto)
	IF l_Error < 0 THEN
   	PCCA.Error = c_Fatal
	END IF

end if
end event

type dw_selezione_piano_campionamento from uo_cs_xx_dw within w_det_campionamenti
event clicked pbm_dwnlbuttonclk
event rowfocuschanged pbm_dwnrowchange
event pcd_retrieve pbm_custom60
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_save pbm_custom62
event pcd_view pbm_custom75
integer x = 82
integer y = 140
integer width = 2185
integer height = 500
integer taborder = 10
string dataobject = "d_selezione_piano_campionamento"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;call super::clicked;dw_det_campionamenti_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

dw_scelta_fase_lavorazione_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_cod_test,ls_flag_aql,ls_cod_prodotto,ls_cod_aql
	datetime ld_oggi,ld_data_validita
	double ldd_num_camp_prec_alzare,ldd_num_test_non_sup,ldd_num_cam_prec_abbassare,ldd_num_test_sup
	
	if rowcount() > 0 then
			ld_oggi = datetime(today())
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
			ls_cod_test = getitemstring(getrow(),"cod_test")

			em_numerosita_stock.enabled = false

			tab_1.tabpage_1.st_num_cam_prec_abbassare.text ="0"
			tab_1.tabpage_1.st_num_cam_prec_alzare.text = "0"
			tab_1.tabpage_1.st_num_test_non_sup.text ="0"
			tab_1.tabpage_1.st_num_test_sup.text ="0"
			tab_1.tabpage_1.st_cod_aql.text = ""
			
			select max(data_validita)
			into   :ld_data_validita
			from   tab_test_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto
			and    data_validita<=:ld_oggi
			and    cod_test=:ls_cod_test;
		
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
				return
			end if
		
		
			select flag_aql,
					 cod_aql
			into   :ls_flag_aql,
					 :ls_cod_aql
			from   tab_test_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto
			and    data_validita=:ld_data_validita
			and    cod_test=:ls_cod_test;

			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
				return
			end if
		
			choose case ls_flag_aql
				case 'N'
					st_tipo_test_prodotto.text = "Test di Ipotesi"
		
				case 'S'
					em_numerosita_stock.enabled = true
					st_tipo_test_prodotto.text = "AQL/Mil.Std."
					
					select num_cam_prec_alzare,
							 num_test_non_sup,
							 num_cam_prec_abbassare,
							 num_test_sup
					into   :ldd_num_camp_prec_alzare,
							 :ldd_num_test_non_sup,
							 :ldd_num_cam_prec_abbassare,
							 :ldd_num_test_sup
					from   tes_aql
					where  cod_azienda=:s_cs_xx.cod_azienda
					and    cod_aql=:ls_cod_aql;
						
					if sqlca.sqlcode < 0 then 
						g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
						return 
					end if
	
					tab_1.tabpage_1.st_num_cam_prec_abbassare.text = string(ldd_num_cam_prec_abbassare)
					tab_1.tabpage_1.st_num_cam_prec_alzare.text = string(ldd_num_camp_prec_alzare) 
					tab_1.tabpage_1.st_num_test_non_sup.text =string(ldd_num_test_non_sup)
					tab_1.tabpage_1.st_num_test_sup.text =string(ldd_num_test_sup)
					tab_1.tabpage_1.st_cod_aql.text = ls_cod_aql
			
				case else
					st_tipo_test_prodotto.text = ""
		end choose
	end if
end if
 
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_prodotto, ls_cod_piano_campionamento,ls_flag_collaudo_finale
datetime ld_data_validita

if dw_tes_campionamenti_lista.rowcount() > 0 then
	ls_cod_piano_campionamento = dw_tes_campionamenti_lista.getitemstring(dw_tes_campionamenti_lista.getrow(), "cod_piano_campionamento")
	ld_data_validita = dw_tes_campionamenti_lista.getitemdatetime(dw_tes_campionamenti_lista.getrow(), "data_validita")
	ls_flag_collaudo_finale = "N"

	l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_piano_campionamento,ld_data_validita)

	IF l_Error < 0 THEN
   	PCCA.Error = c_Fatal
	END IF
end if
end event

type dw_scelta_fase_lavorazione_det from uo_cs_xx_dw within w_det_campionamenti
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
event rowfocuschanged pbm_dwnrowchange
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
event pcd_modify pbm_custom51
event pcd_save pbm_custom62
event pcd_view pbm_custom75
integer x = 1317
integer y = 160
integer width = 1088
integer height = 480
integer taborder = 130
string dataobject = "d_scelta_fase_lavorazione_det"
boolean hscrollbar = true
boolean border = false
end type

type dw_folder_1 from u_folder within w_det_campionamenti
event po_tabclicked pbm_custom75
integer x = 14
integer y = 20
integer width = 2482
integer height = 676
integer taborder = 30
end type

