﻿$PBExportHeader$w_det_piani_campionamento.srw
$PBExportComments$Window det_piani_campionamento
forward
global type w_det_piani_campionamento from w_cs_xx_principale
end type
type dw_det_piani_campionamenti_lista from uo_cs_xx_dw within w_det_piani_campionamento
end type
type cb_1 from cb_prod_ricerca within w_det_piani_campionamento
end type
type dw_det_piani_campionamenti_dett from uo_cs_xx_dw within w_det_piani_campionamento
end type
end forward

global type w_det_piani_campionamento from w_cs_xx_principale
integer width = 2405
integer height = 1792
string title = "Dettaglio Piani di Campionamento"
dw_det_piani_campionamenti_lista dw_det_piani_campionamenti_lista
cb_1 cb_1
dw_det_piani_campionamenti_dett dw_det_piani_campionamenti_dett
end type
global w_det_piani_campionamento w_det_piani_campionamento

event pc_setddlb;call super::pc_setddlb;string ls_oggi

ls_oggi=string(today(),s_cs_xx.db_funzioni.formato_data)

//f_PO_LoadDDDW_DW(dw_det_piani_campionamenti_dett,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in(select cod_prodotto from tab_test_prodotti where data_validita <='" + ls_oggi + "')")

f_PO_LoadDDDW_DW(dw_det_piani_campionamenti_dett,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_det_piani_campionamenti_lista.set_dw_key("cod_azienda")
dw_det_piani_campionamenti_lista.set_dw_key("cod_piano_campionamento")
dw_det_piani_campionamenti_lista.set_dw_key("prog_riga_piani")

dw_det_piani_campionamenti_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)

dw_det_piani_campionamenti_dett.set_dw_options(sqlca,dw_det_piani_campionamenti_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_det_piani_campionamenti_lista

cb_1.enabled = false
	

end on

on w_det_piani_campionamento.create
int iCurrent
call super::create
this.dw_det_piani_campionamenti_lista=create dw_det_piani_campionamenti_lista
this.cb_1=create cb_1
this.dw_det_piani_campionamenti_dett=create dw_det_piani_campionamenti_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_piani_campionamenti_lista
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_det_piani_campionamenti_dett
end on

on w_det_piani_campionamento.destroy
call super::destroy
destroy(this.dw_det_piani_campionamenti_lista)
destroy(this.cb_1)
destroy(this.dw_det_piani_campionamenti_dett)
end on

type dw_det_piani_campionamenti_lista from uo_cs_xx_dw within w_det_piani_campionamento
integer x = 23
integer y = 20
integer width = 2327
integer height = 500
integer taborder = 20
string dataobject = "d_det_piani_campionamento_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_modify,ls_test
	

	ls_modify = "percentuale_campionaria.protect='0~tif(flag_tipo_quantita<>~~'P~~',1,0)'~t"
	ls_modify = ls_modify + "percentuale_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'P~~',12632256,16777215)'~t"
	ls_modify = ls_modify + "numerosita_campionaria.protect='0~tif(flag_tipo_quantita<>~~'N~~',1,0)'~t"
	ls_modify = ls_modify + "numerosita_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'N~~',12632256,16777215)'~t"
	ls_test = dw_det_piani_campionamenti_dett.modify(ls_modify)
	setitem(getrow(), "flag_coll_finale",'N')
	cb_1.enabled = true
	
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG     l_Idx,ll_prog_riga_piani,ll_prog_piano_campionamento
string   ls_cod_piano_campionamento
datetime ld_data_validita

ls_cod_piano_campionamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_piano_campionamento")
ld_data_validita = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_validita")
ll_prog_piano_campionamento = i_parentdw.GetItemnumber(i_parentdw.i_selectedrows[1], "prog_piano_campionamento")

select max(prog_riga_piani) 
into   :ll_prog_riga_piani 
from   det_piani_campionamento 
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_piano_campionamento = :ls_cod_piano_campionamento and 
		 data_validita = :ld_data_validita; 

if isnull(ll_prog_riga_piani) then
	ll_prog_riga_piani = 1
else
	ll_prog_riga_piani++
end if

FOR l_Idx = 1 TO RowCount()
	
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull (GetItemstring(l_Idx, "cod_piano_campionamento")) then
		SetItem(l_Idx, "cod_piano_campionamento", ls_cod_piano_campionamento)
	end if
	
	if isnull (GetItemnumber(l_Idx, "prog_piano_campionamento")) or GetItemnumber(l_Idx, "prog_piano_campionamento") < 1 then
		SetItem(l_Idx, "prog_piano_campionamento", ll_prog_piano_campionamento)
	end if
	
	if isnull (GetItemdatetime(l_Idx, "data_validita")) then
		SetItem(l_Idx, "data_validita", ld_data_validita)
		SetItem(l_Idx, "prog_riga_piani",ll_prog_riga_piani)
	end if
	
NEXT
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	string ls_modify,ls_test
	
	ls_modify = "percentuale_campionaria.protect='0~tif(flag_tipo_quantita<>~~'P~~',1,0)'~t"
	ls_modify = ls_modify + "percentuale_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'P~~',12632256,16777215)'~t"
	ls_modify = ls_modify + "numerosita_campionaria.protect='0~tif(flag_tipo_quantita<>~~'N~~',1,0)'~t"
	ls_modify = ls_modify + "numerosita_campionaria.background.color='16777215~tif(flag_tipo_quantita<>~~'N~~',12632256,16777215)'~t"
	ls_test = dw_det_piani_campionamenti_dett.modify(ls_modify)

	cb_1.enabled = true
end if
end event

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
	cb_1.enabled = false
end if
end on

event pcd_retrieve;call super::pcd_retrieve;LONG     l_Error,ll_prog_piano_campionamento
string   ls_cod_piano_campionamento
datetime ld_data_validita

ls_cod_piano_campionamento  = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_piano_campionamento")
ld_data_validita            = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_validita")
ll_prog_piano_campionamento = i_parentdw.GetItemnumber(i_parentdw.i_selectedrows[1], "prog_piano_campionamento")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_piano_campionamento,ld_data_validita,ll_prog_piano_campionamento )

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
	cb_1.enabled = false
end if
end on

type cb_1 from cb_prod_ricerca within w_det_piani_campionamento
integer x = 2240
integer y = 652
integer width = 69
integer height = 68
integer taborder = 10
end type

on getfocus;call cb_prod_ricerca::getfocus;dw_det_piani_campionamenti_dett.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end on

type dw_det_piani_campionamenti_dett from uo_cs_xx_dw within w_det_piani_campionamento
integer x = 23
integer y = 536
integer width = 2327
integer height = 1140
integer taborder = 30
string dataobject = "d_det_piani_campionamento_dett"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendMode then
	long ll_cont, ll_i
	string ls_nl, ls_cod_prodotto,ls_cod_misura,ls_oggi,ls_flag_direzione, ls_cod_fornitore, ls_cod_piano
	dec{4} ld_quan_limite,ld_quan_test
	datetime ld_data_validita, ldt_validita
	boolean  lb_trovato

	choose case i_colname
		case  "cod_prodotto" 
			setnull(ls_nl)
			ls_oggi=string(today(),s_cs_xx.db_funzioni.formato_data)
			f_PO_LoadDDDW_DW(dw_det_piani_campionamenti_dett,"cod_test",sqlca,&
	      	           "tab_test","cod_test","des_test", &
							  "tab_test.cod_test in ( select tab_test_prodotti.cod_test from tab_test_prodotti where tab_test_prodotti.cod_prodotto ='" + i_coltext + "' and tab_test_prodotti.data_validita <='" + ls_oggi + "')")

			setitem(getrow(),"cod_test",ls_nl)
			setitem(getrow(),"cod_misura",ls_nl)
			setitem(getrow(),"cod_prodotto",ls_nl)
			ls_cod_fornitore = i_parentdw.i_parentdw.getitemstring(i_parentdw.i_parentdw.i_selectedrows[1],"cod_fornitore")
			
			ll_cont = 0
			select count(*)
			into   :ll_cont
			from   tes_piani_campionamento, det_piani_campionamento
			where  (tes_piani_campionamento.cod_azienda = det_piani_campionamento.cod_azienda and
			       tes_piani_campionamento.cod_piano_campionamento = det_piani_campionamento.cod_piano_campionamento and
					 tes_piani_campionamento.data_validita = det_piani_campionamento.data_validita) and
					 (tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda) and
					 (tes_piani_campionamento.cod_fornitore = :ls_cod_fornitore) and
					 (det_piani_campionamento.cod_prodotto = :ls_cod_prodotto);
			if ll_cont > 0 then
				g_mb.messagebox("OMNIA","Attenzione esiste già un piano di campionamento per questo fornitore / prodotto")
				return 2
			end if
			        
		case "cod_test" 
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")

			select cod_misura,flag_tipo_ipotesi
		   into:  ls_cod_misura,:ls_flag_direzione
			from   tab_test_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda and
					 cod_prodotto=:ls_cod_prodotto and
					 cod_test=:i_coltext;
					
			
			if ls_cod_misura = "" then
				g_mb.messagebox("Omnia","Non esiste nessun test per il prodotto selezionato.")
			else
				setitem(getrow(),"cod_misura",ls_cod_misura)		
				setitem(getrow(),"flag_direzione",ls_flag_direzione)
			end if
		
		case "flag_tipo_quantita"
			setitem(getrow(),"numerosita_campionaria",0)			
			setitem(getrow(),"percentuale_campionaria",0)			

		case "quan_limite_campionamento"
			ld_quan_limite = dec(i_coltext)
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
			if isnull(ls_cod_prodotto) then
				g_mb.messagebox("OMNIA","Prima di inserire la quantità inserire il prodotto e il relativo test")
				return 2
			end if
			ls_cod_piano = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_piano_campionamento")
			ld_quan_limite = dec(i_coltext)
			
//************************** Modifica Michele 21/01/2003 *********************************

			ldt_validita = i_parentdw.getitemdatetime(i_parentdw.getrow(),"data_validita")
			
			lb_trovato = false
			
			for ll_i = 1 to rowcount()
				if ll_i <> row and getitemstring(ll_i,"cod_prodotto") = ls_cod_prodotto then
					ld_quan_test = getitemnumber(ll_i,"quan_limite_campionamento")
					if (isnull(ld_quan_test) and not isnull(ld_quan_limite)) or &
						(not isnull(ld_quan_test) and isnull(ld_quan_limite)) or &
					 	ld_quan_test <> ld_quan_limite then
						lb_trovato = true
						exit
					end if
				end if
			next
			
			if lb_trovato then
				if g_mb.messagebox("OMNIA","Attenzione: altri test dello stesso prodotto " + &
								  "hanno quantità limite diversa.~nSarà necessario modificare la quantità " + &
								  "limite anche degli altri test.~nContinuare?",question!,yesno!,2) = 2 then
					return 1
				else
					for ll_i = 1 to rowcount()
						if ll_i <> row and getitemstring(ll_i,"cod_prodotto") = ls_cod_prodotto then
							setitem(ll_i,"quan_limite_campionamento",ld_quan_limite)
						end if
					next
				end if
			end if
			
//			select count(distinct (quan_limite_campionamento))
//			into   :ll_cont
//			from   det_piani_campionamento
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//			       cod_piano_campionamento = :ls_cod_piano and
//					 cod_prodotto = :ls_cod_prodotto and
//					 quan_limite_campionamento > 0 and
//					 quan_limite_campionamento is not null;
//			if sqlca.sqlcode <> 0 then
//				messagebox("OMNIA","Errore in verifica quantità limite per campionamento")
//				return
//			end if
//			
//			choose case ll_cont
//				case 0 
//					// non c'è niente caricato, avanti senza segnalare nulla
//				case 1
//					select COUNT(DISTINCT (quan_limite_campionamento)),quan_limite_campionamento
//					into   :ll_cont, :ld_quan
//					from   det_piani_campionamento
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 cod_piano_campionamento = :ls_cod_piano and
//							 cod_prodotto = :ls_cod_prodotto and
//							 quan_limite_campionamento > 0 and
//							 quan_limite_campionamento is not null
//					group by quan_limite_campionamento;
//					if ld_quan <> 	ld_quan_limite then
//						messagebox("OMNIA","Attenzione: la quantità limite per altri test dello stesso prodotto è diversa da quella inserita.")
//						return 1
//					end if
//				case is > 1		
//					messagebox("OMNIA","Attenzione: la quantità limite per altri test dello stesso prodotto è diversa da quella inserita.")
//					return 1
//			end choose
			
//************************** Modifica Michele 21/01/2003 *********************************
			
	end choose

end if
end event

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendMode then

	string ls_cod_prodotto	

	if getrow() > 0 then
		ls_cod_prodotto=getitemstring(getrow(),"cod_prodotto")	
		f_PO_LoadDDDW_DW(dw_det_piani_campionamenti_dett,"cod_test",sqlca,&
      	           "tab_test","cod_test","des_test", &
						  "tab_test.cod_test in(select cod_test from tab_test_prodotti where cod_prodotto ='" + ls_cod_prodotto + "')")

	end if

end if
end on

