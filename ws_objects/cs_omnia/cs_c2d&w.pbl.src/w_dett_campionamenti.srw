﻿$PBExportHeader$w_dett_campionamenti.srw
$PBExportComments$Window det_campionamenti
forward
global type w_dett_campionamenti from w_cs_xx_principale
end type
type dw_det_campionamenti_lista from uo_cs_xx_dw within w_dett_campionamenti
end type
type dw_det_campionamenti_dett from uo_cs_xx_dw within w_dett_campionamenti
end type
type dw_selezione_piano_campionamento from uo_cs_xx_dw within w_dett_campionamenti
end type
end forward

global type w_dett_campionamenti from w_cs_xx_principale
integer width = 2162
integer height = 2060
string title = "Dettaglio Campionamento"
dw_det_campionamenti_lista dw_det_campionamenti_lista
dw_det_campionamenti_dett dw_det_campionamenti_dett
dw_selezione_piano_campionamento dw_selezione_piano_campionamento
end type
global w_dett_campionamenti w_dett_campionamenti

type variables
long il_prog_riga_piano_campionamento

string is_cod_test
end variables

event pc_setwindow;call super::pc_setwindow;dw_det_campionamenti_lista.set_dw_key("cod_azienda")

dw_det_campionamenti_lista.set_dw_key("anno_reg_campionamenti")

dw_det_campionamenti_lista.set_dw_key("num_reg_campionamenti")

dw_det_campionamenti_lista.set_dw_key("prog_riga_campionamenti")

dw_selezione_piano_campionamento.set_dw_options( sqlca, &
                                          i_openparm, &
														c_scrollparent + c_nodelete + c_nonew + c_nomodify, &
														c_default)

dw_det_campionamenti_lista.set_dw_options( sqlca, &
                                           pcca.null_object, &
														 c_nodelete + c_nonew + c_nomodify, &
														 c_default)

dw_det_campionamenti_dett.set_dw_options( sqlca, &
                                          dw_det_campionamenti_lista, &
														c_sharedata + c_scrollparent, &
														c_default)
								
iuo_dw_main = dw_det_campionamenti_lista



dw_det_campionamenti_dett.modify("altro_valore.visible=~"0~tif(flag_altro_valore='S',1,0)~"")


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_valore",sqlca,&
                 "tab_test_valori","cod_valore","des_valore","")


f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test"," cod_test IN (select cod_test from det_piani_campionamento) ")


f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW( dw_selezione_piano_campionamento, &
                  "cod_test", &
						sqlca, &
                  "tab_test", &
						"cod_test", &
						"des_test", &
						"")
						


end event

on w_dett_campionamenti.create
int iCurrent
call super::create
this.dw_det_campionamenti_lista=create dw_det_campionamenti_lista
this.dw_det_campionamenti_dett=create dw_det_campionamenti_dett
this.dw_selezione_piano_campionamento=create dw_selezione_piano_campionamento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_campionamenti_lista
this.Control[iCurrent+2]=this.dw_det_campionamenti_dett
this.Control[iCurrent+3]=this.dw_selezione_piano_campionamento
end on

on w_dett_campionamenti.destroy
call super::destroy
destroy(this.dw_det_campionamenti_lista)
destroy(this.dw_det_campionamenti_dett)
destroy(this.dw_selezione_piano_campionamento)
end on

type dw_det_campionamenti_lista from uo_cs_xx_dw within w_dett_campionamenti
integer x = 14
integer y = 460
integer width = 2094
integer height = 440
integer taborder = 30
string dataobject = "d_det_campionamenti_lista_new"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event updatestart;call super::updatestart;integer li_i, li_anno_reg_campionamenti

long    ll_num_reg_campionamenti, ll_prog_riga_campionamenti, ll_null, ll_riga

string  ls_cod_prodotto

if i_extendmode then
	
	for li_i = 1 to this.deletedcount()
				
		li_anno_reg_campionamenti = getitemnumber(li_i, "anno_reg_campionamenti", delete!, true)				
		ll_num_reg_campionamenti = getitemnumber(li_i, "num_reg_campionamenti", delete!, true)				
		ll_prog_riga_campionamenti = getitemnumber(li_i, "prog_riga_campionamenti", delete!, true)
						
		update collaudi
		set    anno_reg_campionamenti = :ll_null,
				 num_reg_campionamenti = :ll_null,	
				 prog_riga_campionamenti = :ll_null
		where  cod_azienda = :s_cs_xx.cod_azienda
		and 	 anno_reg_campionamenti = :li_anno_reg_campionamenti
		and    num_reg_campionamenti = :ll_num_reg_campionamenti
		and    prog_riga_campionamenti = :ll_prog_riga_campionamenti;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
				
	next	
	

	
			
end if
end event

event pcd_new;call super::pcd_new;long     ll_prog_riga_piani, ll_prog_stock, ll_prog_riga_campionamenti, ll_prog_riga

string   ls_cod_piano_campionamento, ls_cod_lotto, ls_cod_deposito, ls_cod_ubicazione, &
         ls_oggetto_test, ls_cod_test, ls_cod_prodotto, ls_cod_misura, ls_flag_tipo_quantita, &
			ls_modalita_campionamento, ls_flag_direzione, ls_cod_tipo_test, ls_tipo_valutazione, &
			ls_tipo_attributo, ls_cod_fornitore, ls_val_default

datetime ld_data_stock, ld_data_validita_piano, ld_oggi, ld_data_validita

double   ldd_numerosita_campionaria, ldd_percentuale_campionaria, ldd_livello_significativita, &
         ldd_quantita

if i_extendmode then

	if not isnull(il_prog_riga_piano_campionamento) and il_prog_riga_piano_campionamento > 0 then
	
		ls_cod_piano_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemstring(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "cod_piano_campionamento")			
		ld_data_validita_piano = dw_selezione_piano_campionamento.i_parentdw.getitemdatetime(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "data_validita")					
							
		SELECT oggetto_test,   
				 cod_test,   
				 cod_prodotto, 
				 cod_misura,   
				 flag_tipo_quantita,   
				 numerosita_campionaria,   
				 percentuale_campionaria,   
				 livello_significativita,   
				 modalita_campionamento,   
				 flag_direzione
		INTO   :ls_oggetto_test,   
				 :ls_cod_test,   
				 :ls_cod_prodotto,   
				 :ls_cod_misura,   
				 :ls_flag_tipo_quantita,   
				 :ldd_numerosita_campionaria,   
				 :ldd_percentuale_campionaria,   
				 :ldd_livello_significativita,   
				 :ls_modalita_campionamento,   
				 :ls_flag_direzione         
		FROM 	 det_piani_campionamento  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
				 cod_piano_campionamento = :ls_cod_piano_campionamento AND  
				 data_validita = :ld_data_validita_piano AND
	          prog_riga_piani = :il_prog_riga_piano_campionamento;
		
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			
			triggerevent("pcd_delete")
			
			triggerevent("pcd_save")
			
			return
			
		end if							
		
		SetItem(getrow(), "oggetto_test", ls_oggetto_test)		
		SetItem(getrow(), "cod_test", ls_cod_test)
		SetItem(getrow(), "cod_prodotto", ls_cod_prodotto)
		SetItem(getrow(), "data_validita", ld_data_validita)
		SetItem(getrow(), "cod_misura", ls_cod_misura)
		SetItem(getrow(), "flag_tipo_quantita", ls_flag_tipo_quantita)
		SetItem(getrow(), "numerosita_campionaria", ldd_numerosita_campionaria)
		SetItem(getrow(), "percentuale_campionaria", ldd_percentuale_campionaria)
		SetItem(getrow(), "livello_significativita", ldd_livello_significativita)
		SetItem(getrow(), "modalita_campionamento", ls_modalita_campionamento)
		SetItem(getrow(), "flag_direzione", ls_flag_direzione)
		SetItem(getrow(), "quantita", ldd_quantita)
		SetItem(getrow(), "flag_esito", 'N')
		SetItem(getrow(), "flag_collaudo", 'N')
		SetItem(getrow(), "cod_fornitore", ls_cod_fornitore)
		
		//Donato 28-10-2008: modifica per impostare il default al cod_valore --------------------------
		setnull(ls_val_default)
	
		//recupera il valore di default (se c'è) e lo imposta sulla datawindow
		select cod_valore
		into :ls_val_default
		from tab_test_valori
		where cod_test = :ls_cod_test and flag_default = "S";
		
		if sqlca.sqlcode = -1 then		
			g_mb.messagebox( "OMNIA", "Errore durante il recupero del valore di default:" + sqlca.sqlerrtext)			
		end if
		
		if not isnull(ls_val_default) and ls_val_default <> "" then
			//imposta il valore
			SetItem(getrow(), "cod_valore", ls_val_default)
		end if	
		//fine modifica ---------------------------------------------------------------------------------
	end if

end if





end event

event pcd_retrieve;call super::pcd_retrieve;LONG   l_Error, ll_anno_reg_campionamento, ll_num_reg_campionamento, ll_riga, ll_prog_piano,ll_prog_riga_piano
string ls_cod_test, ls_cod_prodotto

ll_anno_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "anno_reg_campionamenti")

ll_num_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "num_reg_campionamenti")

ls_cod_prodotto = dw_selezione_piano_campionamento.i_parentdw.getitemstring(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "cod_prodotto")

ll_riga = dw_selezione_piano_campionamento.getrow()

if ll_riga < 1 then return

ll_prog_piano = dw_selezione_piano_campionamento.getitemnumber(dw_selezione_piano_campionamento.getrow(), "prog_piano_campionamento")

ll_prog_riga_piano = dw_selezione_piano_campionamento.getitemnumber(dw_selezione_piano_campionamento.getrow(), "prog_riga_piani")

ls_cod_test = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(), "cod_test")

l_Error = Retrieve( s_cs_xx.cod_azienda, ll_anno_reg_campionamento, ll_num_reg_campionamento, ls_cod_prodotto, ls_cod_test)

IF l_Error < 0 THEN
	
  	PCCA.Error = c_Fatal
	  
END IF

end event

event itemchanged;call super::itemchanged;//
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_test

if currentrow < 1 then return

ls_cod_test = getitemstring( currentrow, "cod_test")

if isnull(ls_cod_test) or ls_cod_test = "" then return

f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_valore",sqlca,&
                 "tab_test_valori","cod_valore","des_valore"," cod_test = '" + ls_cod_test + "' ")
	
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_campionamenti, ll_anno_reg_campionamento, ll_num_reg_campionamento

ll_anno_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "anno_reg_campionamenti")

ll_num_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "num_reg_campionamenti")

FOR l_Idx = 1 TO RowCount()
	
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	if isnull(getitemnumber(l_Idx, "anno_reg_campionamenti")) or getitemnumber(l_Idx, "anno_reg_campionamenti") = 0 THEN
		setitem( l_Idx, "anno_reg_campionamenti", ll_anno_reg_campionamento)			
	end if
	if isnull(getitemnumber(l_Idx, "num_reg_campionamenti")) or getitemnumber(l_Idx, "num_reg_campionamenti") = 0 THEN
		setitem( l_Idx, "num_reg_campionamenti", ll_num_reg_campionamento)			
	end if	
		
	if isnull(getitemnumber(l_Idx, "prog_riga_campionamenti")) or getitemnumber(l_Idx, "prog_riga_campionamenti") = 0 THEN
	
		select max(prog_riga_campionamenti) 
		into   :ll_prog_riga_campionamenti 
		from   det_campionamenti 
		where  cod_azienda = :s_cs_xx.cod_azienda	and 
				 anno_reg_campionamenti = :ll_anno_reg_campionamento and 
				 num_reg_campionamenti = :ll_num_reg_campionamento;
	
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			
			rollback;
			
			return
			
		end if
			
		if isnull(ll_prog_riga_campionamenti) then
			
			ll_prog_riga_campionamenti = 1
			
		else
			
			ll_prog_riga_campionamenti++
			
		end if
		
		setitem( l_Idx, "prog_riga_campionamenti", ll_prog_riga_campionamenti)
		
	end if	

NEXT


end event

type dw_det_campionamenti_dett from uo_cs_xx_dw within w_dett_campionamenti
integer y = 900
integer width = 2117
integer height = 1048
integer taborder = 30
string dataobject = "d_dett_campionamenti_dett"
boolean hscrollbar = true
borderstyle borderstyle = styleraised!
end type

event pcd_new;call super::pcd_new;long    	ll_prog_riga_piani, ll_prog_stock, ll_prog_riga_campionamenti, ll_prog_riga, ll_num_giorni, &
			ll_anno_reg_campionamento,ll_num_reg_campionamento

string   ls_cod_piano_campionamento, ls_cod_lotto, ls_cod_deposito, ls_cod_ubicazione, &
         ls_oggetto_test, ls_cod_test, ls_cod_prodotto, ls_cod_misura, ls_flag_tipo_quantita, &
			ls_modalita_campionamento, ls_flag_direzione, ls_cod_tipo_test, ls_tipo_valutazione, &
			ls_tipo_attributo, ls_cod_fornitore, ls_val_default

date		ld_data
datetime ld_data_stock, ld_data_validita_piano, ld_oggi, ld_data_validita, ldt_data_registrazione_campionamento

double   ldd_numerosita_campionaria, ldd_percentuale_campionaria, ldd_livello_significativita, &
         ldd_quantita

if i_extendmode then

	if not isnull(il_prog_riga_piano_campionamento) and il_prog_riga_piano_campionamento > 0 then
	
		ld_data_validita_piano = dw_selezione_piano_campionamento.i_parentdw.getitemdatetime(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "data_validita")					
		ls_cod_piano_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemstring(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "cod_piano_campionamento")
							
		SELECT oggetto_test,   
				 cod_test,   
				 cod_prodotto, 
				 cod_misura,   
				 flag_tipo_quantita,   
				 numerosita_campionaria,   
				 percentuale_campionaria,   
				 livello_significativita,   
				 modalita_campionamento,   
				 flag_direzione
		INTO   :ls_oggetto_test,   
				 :ls_cod_test,   
				 :ls_cod_prodotto,   
				 :ls_cod_misura,   
				 :ls_flag_tipo_quantita,   
				 :ldd_numerosita_campionaria,   
				 :ldd_percentuale_campionaria,   
				 :ldd_livello_significativita,   
				 :ls_modalita_campionamento,   
				 :ls_flag_direzione         
		FROM 	 det_piani_campionamento  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
				 cod_piano_campionamento = :ls_cod_piano_campionamento AND  
				 data_validita = :ld_data_validita_piano AND
	          prog_riga_piani = :il_prog_riga_piano_campionamento;
		
		if sqlca.sqlcode < 0 then			
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return			
		end if							
		
		select MAX(data_validita)
		into   :ld_data_validita
		from   tab_test_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto and
				 cod_test = :ls_cod_test;
		
		SetItem(getrow(), "oggetto_test", ls_oggetto_test)
		
		SetItem(getrow(), "cod_test", ls_cod_test)
		
		SetItem(getrow(), "cod_prodotto", ls_cod_prodotto)
		
		SetItem(getrow(), "data_validita", ld_data_validita)
		
		SetItem(getrow(), "cod_misura", ls_cod_misura)
			
		SetItem(getrow(), "flag_tipo_quantita", ls_flag_tipo_quantita)
		
		SetItem(getrow(), "numerosita_campionaria", ldd_numerosita_campionaria)
			
		SetItem(getrow(), "percentuale_campionaria", ldd_percentuale_campionaria)
		
		SetItem(getrow(), "livello_significativita", ldd_livello_significativita)
		
		SetItem(getrow(), "modalita_campionamento", ls_modalita_campionamento)
		
		SetItem(getrow(), "flag_direzione", ls_flag_direzione)
		
		SetItem(getrow(), "quantita", ldd_quantita)
		
		SetItem(getrow(), "flag_esito", 'I')
		
		SetItem(getrow(), "flag_collaudo", 'N')	
		
		f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_valore",sqlca,&
							  "tab_test_valori","cod_valore","des_valore"," cod_test = '" + ls_cod_test + "' ")
							  
		//Donato 28-10-2008: modifica per impostare il default al cod_valore --------------------------
		setnull(ls_val_default)
	
		//recupera il valore di default (se c'è) e lo imposta sulla datawindow
		select cod_valore
		into :ls_val_default
		from tab_test_valori
		where cod_test = :ls_cod_test and flag_default = 'S';
		
		if sqlca.sqlcode = -1 then		
			g_mb.messagebox( "OMNIA", "Errore durante il recupero del valore di default:" + sqlca.sqlerrtext)			
		end if
		
		if not isnull(ls_val_default) and ls_val_default <> "" then
			//imposta il valore
			SetItem(getrow(), "cod_valore", ls_val_default)
		end if	
		//fine modifica ---------------------------------------------------------------------------------
		
		
		// ----- Aggiunto da EnMe 24-04-2018 per MARR (specifica "implementazione_analisi")
		if not isnull(ls_cod_test) then
			select 	num_giorni
			into		:ll_num_giorni
			from 		tab_test
			where	cod_test = :ls_cod_test;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore nella ricerca dei dati del test.")
			else
				if isnull(ll_num_giorni) then ll_num_giorni = 0
				ll_anno_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "anno_reg_campionamenti")
				ll_num_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "num_reg_campionamenti")
				
				select 	data_registrazione
				into		:ldt_data_registrazione_campionamento
				from		tes_campionamenti
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							anno_reg_campionamenti = :ll_anno_reg_campionamento and
							num_reg_campionamenti = :ll_num_reg_campionamento;
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia","Errore in ricerca data registrazione campionamento~r~n:" + sqlca.sqlerrtext,stopsign!)
					return			
				end if		
				
				ld_data = date(ldt_data_registrazione_campionamento)
				if isnull(ld_data) then ld_data = today()
				
				ld_data = relativedate(ld_data, ll_num_giorni)
		
				ldt_data_registrazione_campionamento = datetime(ld_data, 00:00:00)
		
				setitem(getrow(),"data_fine_analisi",ldt_data_registrazione_campionamento)
			end if
		end if
	// -----------------------  fine EnMe 24-04-2018
	end if	

end if






end event

event updatestart;call super::updatestart;integer li_i, li_anno_reg_campionamenti

long    ll_num_reg_campionamenti, ll_prog_riga_campionamenti, ll_null

string  ls_cod_prodotto

if i_extendmode then
	
//	choose case i_CurrentMode	
//			
//		case c_ModeNew	
//	
//			setnull(ll_null)
//		
//			select max(prog_riga_campionamenti) 
//			into   :ll_prog_riga_campionamenti 
//			from   det_campionamenti 
//			where  cod_azienda = :s_cs_xx.cod_azienda	and 
//					 anno_reg_campionamenti = :il_anno_reg_campionamento and 
//					 num_reg_campionamenti = :il_num_reg_campionamento;
//		
//			if sqlca.sqlcode < 0 then
//				
//				messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//				
//				rollback;
//				
//				return
//				
//			end if
//			
//			if isnull(ll_prog_riga_campionamenti) then
//				
//				ll_prog_riga_campionamenti = 1
//				
//			else
//				
//				ll_prog_riga_campionamenti++
//				
//			end if
//			
//			setitem(getrow(), "cod_azienda", s_cs_xx.cod_azienda)
//			
//			setitem(getrow(), "anno_reg_campionamenti", il_anno_reg_campionamento)
//			
//			setitem(getrow(), "num_reg_campionamenti", il_num_reg_campionamento)
//			
//			setitem(getrow(), "prog_riga_campionamenti", ll_prog_riga_campionamenti)
			
//			
//		case else
//
			for li_i = 1 to this.deletedcount()
				
				li_anno_reg_campionamenti = getitemnumber(li_i, "anno_reg_campionamenti", delete!, true)
				
				ll_num_reg_campionamenti = getitemnumber(li_i, "num_reg_campionamenti", delete!, true)
				
				ll_prog_riga_campionamenti = getitemnumber(li_i, "prog_riga_campionamenti", delete!, true)
				
		
				update collaudi
				set    anno_reg_campionamenti = :ll_null,
						 num_reg_campionamenti = :ll_null,	
						 prog_riga_campionamenti = :ll_null
				where  cod_azienda = :s_cs_xx.cod_azienda
				and 	 anno_reg_campionamenti = :li_anno_reg_campionamenti
				and    num_reg_campionamenti = :ll_num_reg_campionamenti
				and    prog_riga_campionamenti = :ll_prog_riga_campionamenti;
			
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
					return 1
				end if
				
			next
			
//	end choose
	
end if
end event

type dw_selezione_piano_campionamento from uo_cs_xx_dw within w_dett_campionamenti
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_save pbm_custom62
event pcd_view pbm_custom75
integer x = 14
integer y = 20
integer width = 2094
integer height = 440
integer taborder = 40
string dataobject = "d_selezione_piano_campionamento_prodotto"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;dw_det_campionamenti_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")



end event

event rowfocuschanged;call super::rowfocuschanged;LONG   l_Error, ll_anno_reg_campionamento, ll_num_reg_campionamento

string ls_cod_test, ls_cod_prodotto

if i_extendmode then
	
	if rowcount() > 0 then
			
			il_prog_riga_piano_campionamento = getitemnumber( getrow(), "prog_riga_piani")

			is_cod_test = dw_selezione_piano_campionamento.getitemstring(dw_selezione_piano_campionamento.getrow(), "cod_test")

			f_PO_LoadDDDW_DW(dw_det_campionamenti_dett,"cod_valore",sqlca,&
								  "tab_test_valori","cod_valore","des_valore"," cod_test = '" + is_cod_test + "' ")

	end if
	
end if
 
end event

event pcd_retrieve;call super::pcd_retrieve;LONG     l_Error, ll_prog_piano_campionamento, ll_anno_reg_campionamento, ll_num_reg_campionamento

string   ls_cod_piano_campionamento,ls_flag_collaudo_finale, ls_cod_prodotto

datetime ld_data_validita

ls_cod_piano_campionamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_piano_campionamento")

ld_data_validita = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_validita")

ll_prog_piano_campionamento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_piano_campionamento")

ls_flag_collaudo_finale = "N"

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_piano_campionamento, ld_data_validita, ls_cod_prodotto, ll_prog_piano_campionamento)

IF l_Error < 0 THEN
  	PCCA.Error = c_Fatal
END IF

ll_anno_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "anno_reg_campionamenti")

ll_num_reg_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemnumber(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "num_reg_campionamenti")

ls_cod_piano_campionamento = dw_selezione_piano_campionamento.i_parentdw.getitemstring(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "cod_piano_campionamento")

ls_cod_prodotto = dw_selezione_piano_campionamento.i_parentdw.getitemstring(dw_selezione_piano_campionamento.i_parentdw.i_selectedrows[1], "cod_prodotto")

parent.title = "Dettaglio Campionamento " + string(ll_anno_reg_campionamento) + "/" + string(ll_num_reg_campionamento)


end event

