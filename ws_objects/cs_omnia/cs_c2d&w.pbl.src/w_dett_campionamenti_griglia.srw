﻿$PBExportHeader$w_dett_campionamenti_griglia.srw
forward
global type w_dett_campionamenti_griglia from window
end type
type cb_annulla from commandbutton within w_dett_campionamenti_griglia
end type
type cb_salva from commandbutton within w_dett_campionamenti_griglia
end type
type cb_nuovo from commandbutton within w_dett_campionamenti_griglia
end type
type dw_det_campionamenti_griglia from datawindow within w_dett_campionamenti_griglia
end type
end forward

global type w_dett_campionamenti_griglia from window
integer width = 3593
integer height = 2176
boolean titlebar = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
cb_salva cb_salva
cb_nuovo cb_nuovo
dw_det_campionamenti_griglia dw_det_campionamenti_griglia
end type
global w_dett_campionamenti_griglia w_dett_campionamenti_griglia

type variables
s_campionamento_per_griglia is_campionamento_per_griglia
boolean ib_salva = false
end variables

forward prototypes
public function integer wf_controlla_tolleranza (string fs_cod_prodotto, string fs_cod_test, datetime fdt_data_validita, decimal fd_valore)
public subroutine wf_reset_prog_riga_camp (long fl_max_prog_iniziale)
public function integer wf_salva ()
end prototypes

public function integer wf_controlla_tolleranza (string fs_cod_prodotto, string fs_cod_test, datetime fdt_data_validita, decimal fd_valore);//funzione per determinare se il valore numerico immesso rientra nei limiti della tolleranza
//torna 0 se tutto a posto oppure nessun limite impostato
//torna 1 se il valore è più piccolo dell'estremo inferiore
//torna 2 se  il valore è più grande dell'estremo superiore

decimal ldc_tol_minima, ldc_tol_massima

select
	 tol_minima
	,tol_massima
into 
	 :ldc_tol_minima
	,:ldc_tol_massima
from tab_test_prodotti
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_prodotto = :fs_cod_prodotto  
	and cod_test = :fs_cod_test  
	and data_validita = :fdt_data_validita
		;

if isnull(ldc_tol_minima) or ldc_tol_minima = 0 then
	//forse non è stato impostato l'estremo inferiore
	if isnull(ldc_tol_massima) or ldc_tol_massima = 0 then
		//non è stato impostato neppure l'estremo superiore: non fare nessun controllo		
	else
		//è stato impostato l'estremo superiore
		if fd_valore > ldc_tol_massima then return 2
	end if
else
	//estremo inferiore impostato
	if isnull(ldc_tol_massima) or ldc_tol_massima = 0 then
		//non è stato impostato neppure l'estremo superiore, ma quello inferiore si
		if fd_valore < ldc_tol_minima then return 1
	else
		//è stato impostato l'estremo superiore e l'estremo inferiore
		if fd_valore > ldc_tol_massima then return 2
		if fd_valore < ldc_tol_minima then return 1
	end if
end if

return 0
end function

public subroutine wf_reset_prog_riga_camp (long fl_max_prog_iniziale);//long ll_index, ll_rows
//
//if ll_max_prog_riga_campionamenti = -1 then return
//
//ll_rows = dw_det_campionamenti_griglia.rowcount()
//
//for ll_index = 1 to
end subroutine

public function integer wf_salva ();//torna 0 se ha salvato ed è tutto a posto, altrimenti torna 1 (è abbinata anche all'evento close query)
long ll_rows, ll_index, ll_prog_riga_campionamenti, ll_max_prog_riga_campionamenti, ll_ret
boolean lb_primo_null_trovato = true
long ll_max_prog_iniziale
dwItemStatus l_status

if not ib_salva then return 0

ll_max_prog_riga_campionamenti = -1

if g_mb.messagebox("OMNIA", "Confermi il salvataggio dei valori !", Question!, YesNo!, 2) = 1 then
	//ciclare le righe per assegnare eventuali valori null della colonna prog_riga_campionamenti
	ll_rows = dw_det_campionamenti_griglia.rowcount()
	
	for ll_index = 1 to ll_rows
		//ll_prog_riga_campionamenti = dw_det_campionamenti_griglia.getitemnumber(ll_index, "prog_riga_campionamenti")		
		//if isnull(ll_prog_riga_campionamenti) or ll_prog_riga_campionamenti <= 0 then
		
		//verifica lo stato della riga
		l_status = dw_det_campionamenti_griglia.GetItemStatus(ll_index, 0, Primary!)
		if l_status = NewModified! then
			if lb_primo_null_trovato then
				lb_primo_null_trovato = false
				
				//assegnare il max + 1 del prog_riga_campionamenti
				setnull(ll_max_prog_riga_campionamenti)
				
				select max(prog_riga_campionamenti)
				into :ll_max_prog_riga_campionamenti
				from det_campionamenti
				where cod_azienda = :s_cs_xx.cod_azienda
					and anno_reg_campionamenti = :is_campionamento_per_griglia.l_anno_reg_campionamento
					and num_reg_campionamenti = :is_campionamento_per_griglia.l_num_reg_campionamento
				;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("OMNIA", "Errore durante la selezione del progressivo del test!", StopSign!)
					//non chiudere la finestra
					ib_salva = false
					return 1
				end if
				if isnull(ll_max_prog_riga_campionamenti) or ll_max_prog_riga_campionamenti <=0 then
					ll_max_prog_riga_campionamenti = 1
				end if
				
				ll_max_prog_riga_campionamenti += 1
				ll_max_prog_iniziale = ll_max_prog_riga_campionamenti
			else
				ll_max_prog_iniziale += 1
			end if
			
			dw_det_campionamenti_griglia.setitem(ll_index, "prog_riga_campionamenti",ll_max_prog_iniziale)
		end if
	next
	
	//fare update
	ll_ret = dw_det_campionamenti_griglia.update()
	if ll_ret = 1 then
		commit;
		if sqlca.sqlcode = 0 then
			//chiudere la finestra
			ib_salva = false
			return 0
		else
			g_mb.messagebox("OMNIA","Errore durante la conferma dei risultati dei test! Dettaglio:"+sqlca.sqlerrtext)
			rollback;
			//non chiudere la finestra
			ib_salva = false
			return 1
		end if
	else
		g_mb.messagebox("OMNIA","Errore durante l'aggiornamento dei risultati dei test! Dettaglio:"+sqlca.sqlerrtext)
		rollback;
		//non chiudere la finestra
		ib_salva = false
		return 1
	end if
else
	//chiudere la finestra
	ib_salva = false
	return 0
end if
end function

on w_dett_campionamenti_griglia.create
this.cb_annulla=create cb_annulla
this.cb_salva=create cb_salva
this.cb_nuovo=create cb_nuovo
this.dw_det_campionamenti_griglia=create dw_det_campionamenti_griglia
this.Control[]={this.cb_annulla,&
this.cb_salva,&
this.cb_nuovo,&
this.dw_det_campionamenti_griglia}
end on

on w_dett_campionamenti_griglia.destroy
destroy(this.cb_annulla)
destroy(this.cb_salva)
destroy(this.cb_nuovo)
destroy(this.dw_det_campionamenti_griglia)
end on

event open;long ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_rows, ll_index
string ls_path, ls_cod_prodotto, ls_cod_test
decimal ldc_valore
datetime ldt_data_validita
integer li_return

is_campionamento_per_griglia = message.powerobjectparm

ll_anno_reg_campionamenti = is_campionamento_per_griglia.l_anno_reg_campionamento
ll_num_reg_campionamenti = is_campionamento_per_griglia.l_num_reg_campionamento
ls_cod_prodotto = is_campionamento_per_griglia.s_cod_prodotto

dw_det_campionamenti_griglia.SetRowFocusIndicator(Hand!)
dw_det_campionamenti_griglia.settransobject(sqlca)
ll_rows = dw_det_campionamenti_griglia.retrieve(s_cs_xx.cod_azienda, &
										ll_anno_reg_campionamenti, &
										ll_num_reg_campionamenti, &
										ls_cod_prodotto)
										
for ll_index = 1 to ll_rows
	ls_cod_prodotto = dw_det_campionamenti_griglia.getitemstring(ll_index, "cod_prodotto")
	ls_cod_test = dw_det_campionamenti_griglia.getitemstring(ll_index, "cod_test")
	ldt_data_validita = dw_det_campionamenti_griglia.getitemdatetime(ll_index, "data_validita")
	ldc_valore = dw_det_campionamenti_griglia.getitemdecimal(ll_index, "quantita")
	
	li_return = wf_controlla_tolleranza(ls_cod_prodotto, ls_cod_test, ldt_data_validita, ldc_valore)
	
	//test sul ritorno: se 0 OK, se 1 è inferiore al MIN, se 2 è superiore al MAX
	if li_return <> 0 then
		dw_det_campionamenti_griglia.setitem(ll_index, "fl", "S")
	else
		dw_det_campionamenti_griglia.setitem(ll_index, "fl", "N")
	end if
	dw_det_campionamenti_griglia.setitemstatus(ll_index, "fl", primary!, NotModified!)
next
										
dw_det_campionamenti_griglia.groupcalc()

if ll_rows > 0 then
	cb_salva.enabled = true
else
	cb_salva.enabled = false
end if	

//f_po_loaddddw_dw(dw_det_campionamenti_griglia,"cod_test", &
//                 sqlca,&
//                 "tab_test", &
//                 "cod_test", &
//                 "des_test", &
//                 "")

this.title = "Risultati Campionamento " + string(ll_anno_reg_campionamenti) + "/" + string(ll_num_reg_campionamenti)

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "icn_search.bmp"//"VIS.BMP"
dw_det_campionamenti_griglia.object.b_consulta.FileName = ls_path
dw_det_campionamenti_griglia.object.p_consulta.FileName = ls_path

end event

event closequery;integer li_ret

if ib_salva then
	return wf_salva()
else
	//chiudi la finestra
	return 0
end if



end event

type cb_annulla from commandbutton within w_dett_campionamenti_griglia
integer x = 823
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
boolean cancel = true
end type

event clicked;dw_det_campionamenti_griglia.accepttext()

if dw_det_campionamenti_griglia.modifiedcount() > 0 then
	if g_mb.messagebox("OMNIA", "Alcune righe risultano modificate. Si desidera salvare?", Question!,YesNo!, 2) = 2 then
		//l'utente ha risposto di non salvare niente
		ib_salva = false
	else
		//chiudere e salvare
		ib_salva = true
	end if
else
	//comunque non ci sono righe da salvare
	ib_salva = false
end if

close(parent)
end event

type cb_salva from commandbutton within w_dett_campionamenti_griglia
integer x = 434
integer y = 40
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;dw_det_campionamenti_griglia.Accepttext()

if dw_det_campionamenti_griglia.modifiedcount() > 0 then
	ib_salva = true
	wf_salva()
end if
end event

type cb_nuovo from commandbutton within w_dett_campionamenti_griglia
integer x = 46
integer y = 40
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nuovo"
end type

event clicked;long ll_row, ll_null, ll_index
long ll_anno_reg_campionamenti, ll_num_reg_campionamenti
s_campionamento_aut ls_campionamento_aut
s_seleziona_i_test ls_seleziona_i_test
string ls_cod_piano_campionamento
datetime ldt_data_validita
long ll_prog_piano_campionamento

string ls_oggetto_test, ls_cod_test, ls_cod_misura, ls_cod_prodotto, ls_flag_tipo_quantita
string ls_modalita_campionamento, ls_flag_direzione, ls_desc_test, ls_cod_val_default, ls_des_val_default
decimal ldc_numerosita_campionaria, ldc_percentuale_campionaria, ldc_livello_significativita

ll_anno_reg_campionamenti = is_campionamento_per_griglia.l_anno_reg_campionamento
ll_num_reg_campionamenti = is_campionamento_per_griglia.l_num_reg_campionamento
setnull(ll_null)

select
	 cod_piano_campionamento
	,data_validita
	,prog_piano_campionamento
	,cod_prodotto
into
	 :ls_cod_piano_campionamento
	,:ldt_data_validita
	,:ll_prog_piano_campionamento
	,:ls_cod_prodotto
from tes_campionamenti
where cod_azienda = :s_cs_xx.cod_azienda
	and anno_reg_campionamenti = :ll_anno_reg_campionamenti
	and num_reg_campionamenti = :ll_num_reg_campionamenti
;

if sqlca.sqlcode = 0 then
	ls_campionamento_aut.s_cod_piano_campionamento = ls_cod_piano_campionamento
	ls_campionamento_aut.dt_data_validita = ldt_data_validita
	ls_campionamento_aut.l_prog_piano_campionamento = ll_prog_piano_campionamento
	ls_campionamento_aut.s_cod_prodotto = ls_cod_prodotto
	
	openwithparm(w_seleziona_i_test, ls_campionamento_aut)
	ls_seleziona_i_test = message.powerobjectparm
	
	if ls_seleziona_i_test.s_ok = "S" then
		for ll_index = 1 to upperbound(ls_seleziona_i_test.s_oggetto_test)
			//acquisire i valori dalla struttura arrivata
			ls_oggetto_test 							= ls_seleziona_i_test.s_oggetto_test[ll_index]
			ls_cod_test 								= ls_seleziona_i_test.s_cod_test[ll_index]
			ls_cod_misura 							= ls_seleziona_i_test.s_cod_misura[ll_index]
			ls_cod_prodotto 						= ls_seleziona_i_test.s_cod_prodotto[ll_index]
			ls_flag_tipo_quantita 					= ls_seleziona_i_test.s_flag_tipo_quantita[ll_index]
			ls_modalita_campionamento 	= ls_seleziona_i_test.s_modalita_campionamento[ll_index]
			ls_flag_direzione 						= ls_seleziona_i_test.s_flag_direzione[ll_index]
			ls_desc_test 								= ls_seleziona_i_test.s_des_test[ll_index]
			ldc_numerosita_campionaria 	= ls_seleziona_i_test.dc_numerosita_campionaria[ll_index]
			ldc_percentuale_campionaria 	= ls_seleziona_i_test.dc_percentuale_campionaria[ll_index]
			ldc_livello_significativita 				= ls_seleziona_i_test.dc_livello_significativita[ll_index]
			
			//inserire nella dw la riga per il test		
			ll_row = dw_det_campionamenti_griglia.insertrow(0)
			
			//setta la chiave dalla tabella padre tes_campionamenti
			dw_det_campionamenti_griglia.setitem(ll_row, "cod_azienda", s_cs_xx.cod_azienda)
			dw_det_campionamenti_griglia.setitem(ll_row, "anno_reg_campionamenti", ll_anno_reg_campionamenti)
			dw_det_campionamenti_griglia.setitem(ll_row, "num_reg_campionamenti", ll_num_reg_campionamenti)
			//messo a null pechè sara calcolato un'attimo prima di fare l'update
			dw_det_campionamenti_griglia.setitem(ll_row, "prog_riga_campionamenti", ll_null)
			
			dw_det_campionamenti_griglia.setitem(ll_row, "oggetto_test", ls_oggetto_test)
			dw_det_campionamenti_griglia.setitem(ll_row, "cod_test", ls_cod_test)
			dw_det_campionamenti_griglia.setitem(ll_row, "des_test", ls_desc_test)
			dw_det_campionamenti_griglia.setitem(ll_row, "data_validita", ldt_data_validita)
			dw_det_campionamenti_griglia.setitem(ll_row, "cod_misura", ls_cod_misura)
			dw_det_campionamenti_griglia.setitem(ll_row, "cod_prodotto", ls_cod_prodotto)
			dw_det_campionamenti_griglia.setitem(ll_row, "flag_tipo_quantita", ls_flag_tipo_quantita)
			dw_det_campionamenti_griglia.setitem(ll_row, "numerosita_campionaria", ldc_numerosita_campionaria)
			dw_det_campionamenti_griglia.setitem(ll_row, "percentuale_campionaria", ldc_percentuale_campionaria)
			dw_det_campionamenti_griglia.setitem(ll_row, "livello_significativita", ldc_livello_significativita)
			dw_det_campionamenti_griglia.setitem(ll_row, "modalita_campionamento", ls_modalita_campionamento)
			dw_det_campionamenti_griglia.setitem(ll_row, "flag_direzione", ls_flag_direzione)
			
			dw_det_campionamenti_griglia.setitem(ll_row, "flag_esito", "I")
			dw_det_campionamenti_griglia.setitem(ll_row, "flag_collaudo", "N")
			dw_det_campionamenti_griglia.setitem(ll_row, "flag_altro_valore", "N")
			
			dw_det_campionamenti_griglia.setitem(ll_row, "fl", "N")
			
			//Donato 28-10-2008: modifica per impostare il default al cod_valore --------------------------
			setnull(ls_cod_val_default)
			setnull(ls_des_val_default)
		
			//recupera il valore di default (se c'è) e lo imposta sulla datawindow
			select cod_valore, des_valore
			into :ls_cod_val_default, :ls_des_val_default
			from tab_test_valori
			where cod_test = :ls_cod_test and flag_default = "S";
			
			if sqlca.sqlcode = -1 then		
				g_mb.messagebox( "OMNIA", "Errore durante il recupero del valore di default:" + sqlca.sqlerrtext)			
			end if
			
			if not isnull(ls_cod_val_default) and ls_cod_val_default <> "" then
				//imposta il valore
				dw_det_campionamenti_griglia.setItem(ll_row, "cod_valore", ls_cod_val_default)
				dw_det_campionamenti_griglia.setItem(ll_row, "des_valore", ls_des_val_default)
			end if	
			//fine modifica ---------------------------------------------------------------------------------
			
			cb_salva.enabled = true
		next
		dw_det_campionamenti_griglia.groupcalc()
	end if
end if








end event

type dw_det_campionamenti_griglia from datawindow within w_dett_campionamenti_griglia
integer x = 23
integer y = 140
integer width = 3543
integer height = 1940
integer taborder = 10
string title = "none"
string dataobject = "d_det_campionamenti_griglia"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;string ls_flag_altro_valore, ls_altro_valore, ls_cod_test,ls_cod_prodotto, ls_null
s_tab_test_valori_griglia ls_tab_test_valori_griglia
decimal ld_null
datetime ldt_data_validita
s_test_prodotti ls_test_prodotti

setnull(ls_null)
setnull(ld_null)

if row > 0 then
	this.setrow(row)
	AcceptText()
	choose case dwo.name
		case "b_consulta"						
			ls_cod_prodotto = getitemstring(row,"cod_prodotto")
			ls_cod_test = getitemstring(row,"cod_test")
			ldt_data_validita = getitemdatetime(row, "data_validita")
			
			ls_test_prodotti.s_cod_prodotto = ls_cod_prodotto
			ls_test_prodotti.s_cod_test = ls_cod_test
			ls_test_prodotti.dt_validita = ldt_data_validita
			openwithparm(w_consulta_test_prodotto, ls_test_prodotti)
			
		case "b_cod_test"
			//recuperare il test e tutte le info scritte nella corrispondente det_piani_campionamento
			
			
		case "b_cod_valore"
			ls_cod_test = getitemstring(row,"cod_test")
			ls_tab_test_valori_griglia.s_cod_test = ls_cod_test
			ls_tab_test_valori_griglia.s_cod_valore = ""
			ls_tab_test_valori_griglia.s_desc_valore = ""
			openwithparm(w_inserisci_valori_per_test, ls_tab_test_valori_griglia)
			
			ls_tab_test_valori_griglia = Message.PowerObjectParm
			if ls_tab_test_valori_griglia.s_cod_valore = "cs_team_esc_pressed" then				
			else
				setitem(row, "cod_valore", ls_tab_test_valori_griglia.s_cod_valore)
				setitem(row, "des_valore", ls_tab_test_valori_griglia.s_desc_valore)
				//annulla gli altri valori
				setitem(row,"flag_esito", "I")
				setitem(row,"quantita",ld_null)
				setitem(row,"flag_altro_valore","N")
				setitem(row,"altro_valore","")
				setitem(row, "fl", "N")
			end if
			
		case "flag_altro_valore"
			ls_flag_altro_valore = getitemstring(row,"flag_altro_valore")
			if ls_flag_altro_valore = "N" then setitem(row,"altro_valore","")
			
		case "b_altro"
			ls_flag_altro_valore = getitemstring(row,"flag_altro_valore")
			if ls_flag_altro_valore = "N" then 
				if g_mb.messagebox("OMNIA", "Inserire un altro valore?", Question!, YesNo!, 1) = 1 then
					setitem(row,"flag_altro_valore","S")
					ls_flag_altro_valore = "S"
				else
					ls_flag_altro_valore = "N"
				end if
			end if
			
			if ls_flag_altro_valore = "S" then 	
				ls_altro_valore = getitemstring(row,"altro_valore")
				if isnull(ls_altro_valore) then ls_altro_valore = ""
				openwithparm(w_inserisci_altro_valore,ls_altro_valore)
				
				ls_altro_valore = Message.StringParm
				if ls_altro_valore <> "cs_team_esc_pressed" then 
					setitem(row,"altro_valore",ls_altro_valore)
					//annulla tutti gli altri valori
					setitem(row,"flag_esito", "I")
					setitem(row,"cod_valore",ls_null)
					setitem(row,"des_valore",ls_null)
					setitem(row,"quantita",ld_null)
					setitem(row, "fl", "N")
				end if
			end if
	end choose
end if
end event

event itemchanged;decimal ld_null, ldc_tol_minima, ldc_tol_massima
string ls_null, ls_cod_prodotto, ls_cod_test
datetime ldt_data_validita
integer li_return

setnull(ld_null)
setnull(ls_null)

if row > 0 then this.setrow(row)

choose case dwo.name
	case "quantita"
		//annulla gli altri valori
		setitem(row,"flag_esito", "I")
		setitem(row,"cod_valore",ls_null)
		setitem(row,"des_valore",ls_null)
		//setitem(row,"quantita",ld_null)
		setitem(row,"flag_altro_valore","N")
		setitem(row,"altro_valore","")
		
		//controlla gli eventuali limiti
		ls_cod_prodotto = getitemstring(row,"cod_prodotto")
		ls_cod_test = getitemstring(row,"cod_test")
		ldt_data_validita = getitemdatetime(row, "data_validita")
		li_return = wf_controlla_tolleranza(ls_cod_prodotto, ls_cod_test, ldt_data_validita, dec(data))
		//test sul ritorno: se 0 OK, se 1 è inferiore al MIN, se 2 è superiore al MAX
		if li_return <> 0 then
			setitem(row, "fl", "S")
		else
			setitem(row, "fl", "N")
		end if
		
	case "flag_esito"		
		if data <> "I" then
			//annulla gli altri valori
			setitem(row,"quantita",ld_null)
			setitem(row,"cod_valore",ls_null)
			setitem(row,"des_valore",ls_null)
			setitem(row,"flag_altro_valore","N")
			setitem(row,"altro_valore","")
			setitem(row, "fl", "N")
		end if
				
end choose
end event

