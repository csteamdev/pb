﻿$PBExportHeader$w_grafico_centratura_processo.srw
$PBExportComments$Window grafico centratura processo
forward
global type w_grafico_centratura_processo from w_response
end type
type cb_2 from commandbutton within w_grafico_centratura_processo
end type
type cb_1 from uo_cb_close within w_grafico_centratura_processo
end type
type uo_grafico from uo_std_chart within w_grafico_centratura_processo
end type
end forward

global type w_grafico_centratura_processo from w_response
int Width=3534
int Height=1761
boolean TitleBar=true
string Title="Indice Centratura Processo"
cb_2 cb_2
cb_1 cb_1
uo_grafico uo_grafico
end type
global w_grafico_centratura_processo w_grafico_centratura_processo

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup + &
              c_nosaveposition)

cb_2.TriggerEvent ( Clicked! )
end event

on w_grafico_centratura_processo.create
int iCurrent
call w_response::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.uo_grafico=create uo_grafico
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_2
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=uo_grafico
end on

on w_grafico_centratura_processo.destroy
call w_response::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.uo_grafico)
end on

type cb_2 from commandbutton within w_grafico_centratura_processo
int X=2721
int Y=1561
int Width=366
int Height=81
int TabOrder=20
string Text="&Grafico"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_test,ls_cod_prodotto
long  l_Error
decimal ld_centr_processo
integer li_cont = 0, li_prog_riga_campionamenti

//
// Dichiarazione delle caratteristiche grafiche del grafico
//

ls_cod_test = s_cs_xx.parametri.parametro_s_1
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_2

DECLARE cur_Graph CURSOR FOR  
  SELECT det_campionamenti.prog_riga_campionamenti,   
         det_campionamenti.centratura_processo  
    FROM det_campionamenti  
   WHERE ( det_campionamenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( det_campionamenti.cod_test = :ls_cod_test ) AND  
         ( det_campionamenti.cod_prodotto = :ls_cod_prodotto ) AND
			( det_campionamenti.prog_riga_campionamenti is not null ) AND
         ( det_campionamenti.centratura_processo is not null ) ;

OPEN cur_Graph;

FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ld_centr_processo ;

li_cont = 0
DO WHILE SQLCA.sqlcode = 0
	li_cont ++
	FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ld_centr_processo ;
loop	

close cur_Graph;

if li_cont > 0 then
	uo_grafico.of_init(3, 0, "Centratura Processo", "Progressivo Campionamento", "Indice Centratura", &
						 "Asse_Z", "A1:B" + string(li_Cont) , "", True, False, False)
	uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).SeriesMarker.Auto = False
	uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).SeriesMarker.Show = True

	For li_cont = 1 To uo_grafico.ole_Graph.object.rowCount
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.visible = True
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.size = 8
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.style = 14
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.fillcolor.blue = 255
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.fillcolor.red = 140
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).Marker.fillcolor.green = 70
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).DataPointLabel.locationType = 1
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).DataPointLabel.Component = 1
		uo_grafico.ole_Graph.object.Plot.SeriesCollection.Item(1).DataPoints.Item(li_cont).DataPointLabel.VtFont.Size = 8
	next

//
// Adesso carico dati ed etichette nell'oggetto DataGrid
//

	OPEN cur_Graph;

	FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ld_centr_processo ;

	li_cont = 0
	DO WHILE SQLCA.sqlcode = 0
		li_cont ++
		uo_grafico.ole_Graph.object.datagrid.RowLabel(li_Cont, 1, string(li_prog_riga_campionamenti))
		uo_grafico.ole_graph.object.datagrid.setdata(li_cont, 1,ld_centr_processo,False)	
		FETCH cur_Graph INTO :li_prog_riga_campionamenti, :ld_centr_processo ;
	loop	

	close cur_Graph;
else
	uo_grafico.of_init(3, 0, "Centratura Processo", "Progressivo Campionamento", "Indice Centratura", &
						 "Asse_Z", "" , "", False, False, False)
	g_mb.messagebox("Omnia"," Non ci Sono Dati per la Relativa Selezione! ",Exclamation!)
end if	
end event

type cb_1 from uo_cb_close within w_grafico_centratura_processo
int X=3109
int Y=1561
int Width=366
int Height=81
int TabOrder=30
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type uo_grafico from uo_std_chart within w_grafico_centratura_processo
int X=23
int Y=21
int Width=3452
int Height=1521
int TabOrder=10
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
end type

on uo_grafico.destroy
call uo_std_chart::destroy
end on

