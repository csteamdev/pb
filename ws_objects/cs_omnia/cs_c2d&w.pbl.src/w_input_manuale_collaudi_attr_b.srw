﻿$PBExportHeader$w_input_manuale_collaudi_attr_b.srw
$PBExportComments$Window input manuale collaudi attributi per carte Binomiali
forward
global type w_input_manuale_collaudi_attr_b from w_cs_xx_risposta
end type
type dw_input_manuale_collaudi_attr from uo_cs_xx_dw within w_input_manuale_collaudi_attr_b
end type
type cb_1 from uo_cb_save within w_input_manuale_collaudi_attr_b
end type
type cb_2 from commandbutton within w_input_manuale_collaudi_attr_b
end type
type ddlb_1 from dropdownlistbox within w_input_manuale_collaudi_attr_b
end type
type st_1 from statictext within w_input_manuale_collaudi_attr_b
end type
end forward

global type w_input_manuale_collaudi_attr_b from w_cs_xx_risposta
int Width=1290
int Height=1669
boolean TitleBar=true
string Title="Input Manuale Collaudi"
dw_input_manuale_collaudi_attr dw_input_manuale_collaudi_attr
cb_1 cb_1
cb_2 cb_2
ddlb_1 ddlb_1
st_1 st_1
end type
global w_input_manuale_collaudi_attr_b w_input_manuale_collaudi_attr_b

type variables
long il_num_osservazioni
end variables

event pc_setddlb;call super::pc_setddlb;string ls_oggi

ls_oggi = string(today(),s_cs_xx.db_funzioni.formato_data)

f_po_loadddlb(ddlb_1, &
                 sqlca, &
                 "carte_controllo", &
                 "cod_carta", &
                 "cod_carta", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + s_cs_xx.parametri.parametro_s_4 +"' and cod_test='" + s_cs_xx.parametri.parametro_s_5 + "' and tipo_carta='P' and data_validita <= '" + ls_oggi + "'","")

end event

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_input_manuale_collaudi_attr.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete,c_default)

delete from tab_input_sottogruppi where cod_azienda=:s_cs_xx.cod_azienda;


end on

on w_input_manuale_collaudi_attr_b.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_input_manuale_collaudi_attr=create dw_input_manuale_collaudi_attr
this.cb_1=create cb_1
this.cb_2=create cb_2
this.ddlb_1=create ddlb_1
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_input_manuale_collaudi_attr
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_2
this.Control[iCurrent+4]=ddlb_1
this.Control[iCurrent+5]=st_1
end on

on w_input_manuale_collaudi_attr_b.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_input_manuale_collaudi_attr)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.ddlb_1)
destroy(this.st_1)
end on

type dw_input_manuale_collaudi_attr from uo_cs_xx_dw within w_input_manuale_collaudi_attr_b
int X=19
int Y=17
int Width=1212
int Height=1329
int TabOrder=10
string DataObject="d_input_sottogruppo"
BorderStyle BorderStyle=StyleRaised!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end on

type cb_1 from uo_cb_save within w_input_manuale_collaudi_attr_b
int X=481
int Y=1461
int Width=366
int Height=81
int TabOrder=30
string Text="&Conferma"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;call uo_cb_save::clicked;long ll_anno_registrazione,ll_inizio,ll_fine,ll_inf,ll_fif
long ll_num_sottogruppi,ll_num_scarti,ll_t,ll_puntatore

w_input_manuale_collaudi_attr_b.triggerevent("pc_save")

ll_anno_registrazione = long(s_cs_xx.parametri.parametro_s_1)
ll_inizio = long(s_cs_xx.parametri.parametro_s_2)
ll_fine = long(s_cs_xx.parametri.parametro_s_3)

select count(*) into:ll_num_sottogruppi from tab_input_sottogruppi;

ll_inf = ll_inizio
ll_fif = ll_inizio + il_num_osservazioni - 1

for ll_t = 1 to ll_num_sottogruppi
	
	select num_scarti into:ll_num_scarti 
	from tab_input_sottogruppi
	where cod_azienda=:s_cs_xx.cod_azienda
	and num_sottogruppo=:ll_t;

	ll_puntatore = ll_inf + ll_num_scarti - 1 
	
	update collaudi  
   set flag_esito = 'N'  
   where collaudi.cod_azienda = :s_cs_xx.cod_azienda
   and   collaudi.anno_registrazione = :ll_anno_registrazione  
   and   collaudi.num_registrazione between :ll_inf and :ll_puntatore;
	
	ll_inf = ll_fif + 1
	ll_fif = ll_fif + il_num_osservazioni

next

close(parent)
w_det_campionamenti.setfocus()
end on

type cb_2 from commandbutton within w_input_manuale_collaudi_attr_b
int X=869
int Y=1461
int Width=366
int Height=81
int TabOrder=20
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long ll_anno_registrazione,ll_inizio,ll_fine,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti

ll_anno_registrazione = long(s_cs_xx.parametri.parametro_s_1)
ll_inizio = long(s_cs_xx.parametri.parametro_s_2)
ll_fine = long(s_cs_xx.parametri.parametro_s_3)

SELECT collaudi.anno_reg_campionamenti,   
       collaudi.num_reg_campionamenti,   
       collaudi.prog_riga_campionamenti  
INTO   :ll_anno_reg_campionamenti,   
       :ll_num_reg_campionamenti,   
       :ll_prog_riga_campionamenti  
FROM   collaudi  
WHERE  collaudi.cod_azienda = :s_cs_xx.cod_azienda
AND    collaudi.anno_registrazione = :ll_anno_registrazione 
AND    collaudi.num_registrazione = :ll_inizio;
	
delete from collaudi
where cod_azienda=:s_cs_xx.cod_azienda
and	anno_registrazione=:ll_anno_registrazione
and 	num_registrazione between :ll_inizio and :ll_fine;
	
delete from det_campionamenti
where cod_azienda=:s_cs_xx.cod_azienda
and	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;
		
close(parent)

w_det_campionamenti.setfocus()
w_det_campionamenti.dw_det_campionamenti_lista.triggerevent("pcd_retrieve")

delete from tab_input_sottogruppi where cod_azienda=:s_cs_xx.cod_azienda;
end on

type ddlb_1 from dropdownlistbox within w_input_manuale_collaudi_attr_b
int X=485
int Y=1349
int Width=746
int Height=745
int TabOrder=40
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;string ls_cod_prodotto,ls_cod_test,ls_cod_carta
long ll_num_sottogruppi,ll_t
datetime ld_data_validita

ld_data_validita = datetime(today())
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_4
ls_cod_test = s_cs_xx.parametri.parametro_s_5 
ls_cod_carta = f_po_selectddlb(ddlb_1)

select num_sottogruppi,num_osservazioni
into   :ll_num_sottogruppi,
		 :il_num_osservazioni
from   carte_controllo  
where  carte_controllo.cod_azienda = :s_cs_xx.cod_azienda  
and    carte_controllo.cod_carta = :ls_cod_carta
and    carte_controllo.cod_prodotto = :ls_cod_prodotto
and    carte_controllo.cod_test = :ls_cod_test 
and    carte_controllo.data_validita >= :ld_data_validita;

delete from tab_input_sottogruppi where cod_azienda=:s_cs_xx.cod_azienda;

for ll_t = 1 to ll_num_sottogruppi
  insert into tab_input_sottogruppi  
         ( cod_azienda,
			  num_sottogruppo,   
           num_scarti )  
  values ( :s_cs_xx.cod_azienda,
			  :ll_t,   
           null )  ;
next 

w_input_manuale_collaudi_attr_b.triggerevent("pc_retrieve")
w_input_manuale_collaudi_attr_b.triggerevent("pc_modify")
end event

type st_1 from statictext within w_input_manuale_collaudi_attr_b
int X=10
int Y=1361
int Width=458
int Height=73
boolean Enabled=false
string Text="Carte Binomiali"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

