﻿$PBExportHeader$w_input_manuale_collaudi_attr_p.srw
$PBExportComments$Window input manuale collaudi attributi per carte Poisson
forward
global type w_input_manuale_collaudi_attr_p from w_cs_xx_risposta
end type
type dw_input_manuale_collaudi_attr from uo_cs_xx_dw within w_input_manuale_collaudi_attr_p
end type
type cb_2 from commandbutton within w_input_manuale_collaudi_attr_p
end type
type st_1 from statictext within w_input_manuale_collaudi_attr_p
end type
type cb_3 from commandbutton within w_input_manuale_collaudi_attr_p
end type
type ddlb_1 from dropdownlistbox within w_input_manuale_collaudi_attr_p
end type
type cb_1 from commandbutton within w_input_manuale_collaudi_attr_p
end type
end forward

global type w_input_manuale_collaudi_attr_p from w_cs_xx_risposta
int Width=2757
int Height=1749
boolean TitleBar=true
string Title="Input Manuale Collaudi"
dw_input_manuale_collaudi_attr dw_input_manuale_collaudi_attr
cb_2 cb_2
st_1 st_1
cb_3 cb_3
ddlb_1 ddlb_1
cb_1 cb_1
end type
global w_input_manuale_collaudi_attr_p w_input_manuale_collaudi_attr_p

type variables

end variables

event pc_setddlb;call super::pc_setddlb;string ls_oggi

ls_oggi = string(today(),s_cs_xx.db_funzioni.formato_data)

f_PO_LoadDDDW_DW(dw_input_manuale_collaudi_attr,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita",&
                 "tab_difformita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loadddlb(ddlb_1, &
                 sqlca, &
                 "carte_controllo", &
                 "cod_carta", &
                 "cod_carta", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + s_cs_xx.parametri.parametro_s_4 +"' and cod_test='" + s_cs_xx.parametri.parametro_s_5 + "' and tipo_carta='D' and data_validita <='" + ls_oggi + "'","")

end event

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_input_manuale_collaudi_attr.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete,c_default)

delete from tab_input_poisson where cod_azienda=:s_cs_xx.cod_azienda;

cb_1.enabled = false
cb_3.enabled = false


end on

on w_input_manuale_collaudi_attr_p.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_input_manuale_collaudi_attr=create dw_input_manuale_collaudi_attr
this.cb_2=create cb_2
this.st_1=create st_1
this.cb_3=create cb_3
this.ddlb_1=create ddlb_1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_input_manuale_collaudi_attr
this.Control[iCurrent+2]=cb_2
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=cb_3
this.Control[iCurrent+5]=ddlb_1
this.Control[iCurrent+6]=cb_1
end on

on w_input_manuale_collaudi_attr_p.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_input_manuale_collaudi_attr)
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.cb_3)
destroy(this.ddlb_1)
destroy(this.cb_1)
end on

type dw_input_manuale_collaudi_attr from uo_cs_xx_dw within w_input_manuale_collaudi_attr_p
int X=19
int Y=21
int Width=2675
int Height=1489
int TabOrder=10
string DataObject="d_input_poisson"
BorderStyle BorderStyle=StyleRaised!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end on

type cb_2 from commandbutton within w_input_manuale_collaudi_attr_p
int X=2332
int Y=1541
int Width=366
int Height=81
int TabOrder=40
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long ll_anno_registrazione,ll_inizio,ll_fine,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti

ll_anno_registrazione = long(s_cs_xx.parametri.parametro_s_1)
ll_inizio = long(s_cs_xx.parametri.parametro_s_2)
ll_fine = long(s_cs_xx.parametri.parametro_s_3)

SELECT collaudi.anno_reg_campionamenti,   
       collaudi.num_reg_campionamenti,   
       collaudi.prog_riga_campionamenti  
INTO   :ll_anno_reg_campionamenti,   
       :ll_num_reg_campionamenti,   
       :ll_prog_riga_campionamenti  
FROM   collaudi  
WHERE  collaudi.cod_azienda = :s_cs_xx.cod_azienda
AND    collaudi.anno_registrazione = :ll_anno_registrazione 
AND    collaudi.num_registrazione = :ll_inizio;
	
delete from collaudi
where cod_azienda=:s_cs_xx.cod_azienda
and	anno_registrazione=:ll_anno_registrazione
and 	num_registrazione between :ll_inizio and :ll_fine;
	
delete from det_campionamenti
where cod_azienda=:s_cs_xx.cod_azienda
and	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;
		
close(parent)
w_det_campionamenti.setfocus()
w_det_campionamenti.dw_det_campionamenti_lista.triggerevent("pcd_retrieve")
end on

type st_1 from statictext within w_input_manuale_collaudi_attr_p
int X=10
int Y=1533
int Width=636
int Height=73
boolean Enabled=false
string Text="Input per carte Poisson"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_3 from commandbutton within w_input_manuale_collaudi_attr_p
int X=1578
int Y=1541
int Width=366
int Height=81
int TabOrder=50
string Text="&Inserisci"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long ll_num_registrazione


w_input_manuale_collaudi_attr_p.triggerevent("pc_save")

select max(num_registrazione) into:ll_num_registrazione
from tab_input_poisson
where cod_azienda =:s_cs_xx.cod_azienda;

ll_num_registrazione++

insert into tab_input_poisson  
         ( cod_azienda,   
           num_registrazione,   
           cod_pezzo,   
           cod_errore,   
           flag_esito )  
values   ( :s_cs_xx.cod_azienda,   
           :ll_num_registrazione,   
           null,   
           null,   
           'S' )  ;

w_input_manuale_collaudi_attr_p.triggerevent("pc_retrieve")
w_input_manuale_collaudi_attr_p.triggerevent("pc_modify")
end on

type ddlb_1 from dropdownlistbox within w_input_manuale_collaudi_attr_p
int X=645
int Y=1525
int Width=819
int Height=745
int TabOrder=20
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;string ls_cod_prodotto,ls_cod_test,ls_cod_carta
long ll_num_sottogruppi,ll_t,ll_anno_registrazione,ll_inizio,ll_fine,ll_num_record,ll_num_osservazioni
datetime ld_data_validita

ld_data_validita = datetime(today())
ll_anno_registrazione = long(s_cs_xx.parametri.parametro_s_1)
ll_inizio = long(s_cs_xx.parametri.parametro_s_2)
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_4
ls_cod_test = s_cs_xx.parametri.parametro_s_5 
ls_cod_carta = f_po_selectddlb(ddlb_1)

select num_sottogruppi,num_osservazioni
into   :ll_num_sottogruppi,
		 :ll_num_osservazioni
from   carte_controllo  
where  carte_controllo.cod_azienda = :s_cs_xx.cod_azienda  
and    carte_controllo.cod_carta = :ls_cod_carta
and    carte_controllo.cod_prodotto = :ls_cod_prodotto
and    carte_controllo.cod_test = :ls_cod_test 
and    carte_controllo.data_validita <= :ld_data_validita;

ll_num_record = ll_num_sottogruppi * ll_num_osservazioni
ll_fine = ll_inizio + ll_num_record - 1

s_cs_xx.parametri.parametro_s_3 = string(ll_fine)

delete from tab_input_poisson where cod_azienda=:s_cs_xx.cod_azienda;

for ll_t = ll_inizio to ll_fine

  insert into tab_input_poisson
         ( cod_azienda,
			  num_registrazione,   
           cod_pezzo,
			  cod_errore,
			  flag_esito )  
  values ( :s_cs_xx.cod_azienda,
			  :ll_t,
			  null,
			  null,	   
			  'S' );
next 

cb_3.enabled = true
cb_1.enabled = true

w_input_manuale_collaudi_attr_p.triggerevent("pc_retrieve")
w_input_manuale_collaudi_attr_p.triggerevent("pc_modify")
end event

type cb_1 from commandbutton within w_input_manuale_collaudi_attr_p
int X=1957
int Y=1541
int Width=366
int Height=81
int TabOrder=30
string Text="&Conferma"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto,ls_cod_test,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto,ls_cod_operaio
string ls_cod_misura,ls_cod_pezzo,ls_cod_errore,ls_flag_esito,ls_test,ls_st
long ll_num_sottogruppi,ll_t,ll_anno_registrazione,ll_inizio,ll_fine,ll_anno_reg_campionamenti
long ll_num_reg_campionamenti,ll_prog_riga_campionamenti,ll_prog_stock,ll_num_record
datetime ld_data_validita,ld_data_registrazione,ld_data_stock

w_input_manuale_collaudi_attr_p.triggerevent("pc_save")

ld_data_registrazione = datetime(today())
ll_anno_registrazione = long(s_cs_xx.parametri.parametro_s_1)
ll_inizio = long(s_cs_xx.parametri.parametro_s_2)
ll_fine = long(s_cs_xx.parametri.parametro_s_3)
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_4
ls_cod_test = s_cs_xx.parametri.parametro_s_5 
ll_anno_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_6)
ll_num_reg_campionamenti = long(s_cs_xx.parametri.parametro_s_7)
ll_prog_riga_campionamenti = long(s_cs_xx.parametri.parametro_s_8)
ls_cod_deposito = s_cs_xx.parametri.parametro_s_9 
ls_cod_ubicazione = s_cs_xx.parametri.parametro_s_10 
ls_cod_lotto = s_cs_xx.parametri.parametro_s_11
ll_prog_stock = long(s_cs_xx.parametri.parametro_s_12)
ls_cod_operaio = s_cs_xx.parametri.parametro_s_13
ls_cod_misura = s_cs_xx.parametri.parametro_s_14
ld_data_validita = s_cs_xx.parametri.parametro_data_1
ld_data_stock = s_cs_xx.parametri.parametro_data_2
ls_st = ""

select cod_errore into :ls_test 
from   tab_input_poisson
where  cod_azienda=:s_cs_xx.cod_azienda
and    (cod_pezzo =:ls_st or cod_pezzo is null);

if sqlca.sqlcode <> 100 then
	g_mb.messagebox("Omnia","Ad alcune osservazioni non è stato assegnato un codice pezzo, tali osservazioni" + &
                       "saranno ugualmente utilizzate per calcolare i limiti della carta di controllo assegnata",information!)
end if


select count(*) into:ll_num_record 
from   tab_input_poisson
where  cod_azienda=:s_cs_xx.cod_azienda;

ll_fine = ll_inizio + ll_num_record - 1 

for ll_t = ll_inizio to ll_fine

     select cod_pezzo,   
            cod_errore,   
            flag_esito  
     into   :ls_cod_pezzo,   
            :ls_cod_errore,   
            :ls_flag_esito  
     from  tab_input_poisson  
	  where cod_azienda =: s_cs_xx.cod_azienda
	  and   num_registrazione =: ll_t;
   
	  if ls_cod_pezzo = "" or isnull(ls_cod_pezzo) then 
	    ls_cod_pezzo = "@_" + string(ll_t)
	  end if

	  insert into collaudi  
     	   ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           data_carta,   
           cod_carta,   
           anno_reg_campionamenti,   
           num_reg_campionamenti,   
           prog_riga_campionamenti,   
           cod_test,   
           data_validita,   
           data_registrazione,   
           cod_prodotto,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           data_stock,   
           prog_stock,   
           cod_operaio,   
           risultato,   
           cod_misura,   
           cod_errore,   
           flag_esito,   
           note,   
           cod_pezzo )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_registrazione,   
           :ll_t,   
           null,   
           null,   
           :ll_anno_reg_campionamenti,   
           :ll_num_reg_campionamenti,   
           :ll_prog_riga_campionamenti,   
           :ls_cod_test,   
           :ld_data_validita,   
           :ld_data_registrazione,   
           :ls_cod_prodotto,   
           :ls_cod_deposito,   
           :ls_cod_ubicazione,   
           :ls_cod_lotto,   
           :ld_data_stock,   
           :ll_prog_stock,   
           :ls_cod_operaio,   
           null,   
           :ls_cod_misura,   
           :ls_cod_errore,   
           :ls_flag_esito,   
           null,   
           :ls_cod_pezzo )  ;

next 

commit;

close(parent)

w_det_campionamenti.setfocus()

end event

