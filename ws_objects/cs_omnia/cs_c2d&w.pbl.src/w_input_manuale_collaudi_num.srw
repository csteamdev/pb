﻿$PBExportHeader$w_input_manuale_collaudi_num.srw
$PBExportComments$Window input manuale collaudi numerici
forward
global type w_input_manuale_collaudi_num from w_cs_xx_risposta
end type
type dw_input_manuale_collaudi_num from uo_cs_xx_dw within w_input_manuale_collaudi_num
end type
type cb_1 from uo_cb_save within w_input_manuale_collaudi_num
end type
type cb_2 from commandbutton within w_input_manuale_collaudi_num
end type
type st_1 from statictext within w_input_manuale_collaudi_num
end type
type st_2 from statictext within w_input_manuale_collaudi_num
end type
type st_min from statictext within w_input_manuale_collaudi_num
end type
type st_max from statictext within w_input_manuale_collaudi_num
end type
end forward

global type w_input_manuale_collaudi_num from w_cs_xx_risposta
int Width=1290
int Height=1725
boolean TitleBar=true
string Title="Input Manuale Collaudi"
dw_input_manuale_collaudi_num dw_input_manuale_collaudi_num
cb_1 cb_1
cb_2 cb_2
st_1 st_1
st_2 st_2
st_min st_min
st_max st_max
end type
global w_input_manuale_collaudi_num w_input_manuale_collaudi_num

event pc_setwindow;call super::pc_setwindow;string ls_cod_prodotto,ls_cod_test
double ldd_tol_minima,ldd_tol_massima
datetime ldt_oggi

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_4 
ls_cod_test = s_cs_xx.parametri.parametro_s_5
ldt_oggi = datetime(today())

SELECT tol_minima,
		 tol_massima
INTO 	 :ldd_tol_minima,
		 :ldd_tol_massima
FROM 	 tab_test_prodotti  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
		 cod_prodotto = :ls_cod_prodotto AND  
		 cod_test = :ls_cod_test and 
		 data_validita <= :ldt_oggi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

st_min.text = string(ldd_tol_minima)
st_max.text = string(ldd_tol_massima)

dw_input_manuale_collaudi_num.set_dw_options(sqlca,pcca.null_object,c_modifyonopen + c_nonew + c_nodelete,c_default)
end event

on w_input_manuale_collaudi_num.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_input_manuale_collaudi_num=create dw_input_manuale_collaudi_num
this.cb_1=create cb_1
this.cb_2=create cb_2
this.st_1=create st_1
this.st_2=create st_2
this.st_min=create st_min
this.st_max=create st_max
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_input_manuale_collaudi_num
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_2
this.Control[iCurrent+4]=st_1
this.Control[iCurrent+5]=st_2
this.Control[iCurrent+6]=st_min
this.Control[iCurrent+7]=st_max
end on

on w_input_manuale_collaudi_num.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_input_manuale_collaudi_num)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_min)
destroy(this.st_max)
end on

type dw_input_manuale_collaudi_num from uo_cs_xx_dw within w_input_manuale_collaudi_num
int X=1
int Y=1
int Width=1235
int Height=1421
int TabOrder=10
string DataObject="d_input_manuale_collaudi_num"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error,ll_anno_registrazione,ll_inizio,ll_fine

ll_anno_registrazione = long(s_cs_xx.parametri.parametro_s_1)
ll_inizio = long(s_cs_xx.parametri.parametro_s_2)
ll_fine = long(s_cs_xx.parametri.parametro_s_3)

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_registrazione,ll_inizio,ll_fine)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end on

event itemchanged;call super::itemchanged;string ls_cod_prodotto,ls_cod_test
double ldd_tol_minima,ldd_tol_massima,ldd_risultato
datetime ldt_oggi

	ls_cod_prodotto = s_cs_xx.parametri.parametro_s_4 
	ls_cod_test = s_cs_xx.parametri.parametro_s_5
	ldt_oggi = datetime(today())

	SELECT tol_minima,
		    tol_massima
	INTO 	 :ldd_tol_minima,
		    :ldd_tol_massima
	FROM 	 tab_test_prodotti  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
		 	 cod_prodotto = :ls_cod_prodotto AND  
		 	 cod_test = :ls_cod_test and 
			 data_validita <= :ldt_oggi;
	
	if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
	end if
	
	ldd_risultato = double(data)
	
	if ldd_risultato > ldd_tol_minima and ldd_risultato < ldd_tol_massima then 
	setitem(row,"flag_esito","S")
	else
	setitem(row,"flag_esito","N")
	end if
	

end event

type cb_1 from uo_cb_save within w_input_manuale_collaudi_num
int X=481
int Y=1521
int Width=366
int Height=81
int TabOrder=20
string Text="&Conferma"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_15 = "Conferma"
close(parent)

end event

type cb_2 from commandbutton within w_input_manuale_collaudi_num
int X=869
int Y=1521
int Width=366
int Height=81
int TabOrder=30
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_anno_registrazione,ll_inizio,ll_fine,ll_anno_reg_campionamenti,ll_num_reg_campionamenti,ll_prog_riga_campionamenti

ll_anno_registrazione = long(s_cs_xx.parametri.parametro_s_1)
ll_inizio = long(s_cs_xx.parametri.parametro_s_2)
ll_fine = long(s_cs_xx.parametri.parametro_s_3)

SELECT collaudi.anno_reg_campionamenti,   
       collaudi.num_reg_campionamenti,   
       collaudi.prog_riga_campionamenti  
INTO   :ll_anno_reg_campionamenti,   
       :ll_num_reg_campionamenti,   
       :ll_prog_riga_campionamenti  
FROM   collaudi  
WHERE  collaudi.cod_azienda = :s_cs_xx.cod_azienda
AND    collaudi.anno_registrazione = :ll_anno_registrazione 
AND    collaudi.num_registrazione = :ll_inizio;
	
delete from collaudi
where cod_azienda=:s_cs_xx.cod_azienda
and	anno_registrazione=:ll_anno_registrazione
and 	num_registrazione between :ll_inizio and :ll_fine;
	
delete from det_campionamenti
where cod_azienda=:s_cs_xx.cod_azienda
and	anno_reg_campionamenti=:ll_anno_reg_campionamenti
and 	num_reg_campionamenti=:ll_num_reg_campionamenti
and 	prog_riga_campionamenti=:ll_prog_riga_campionamenti;
		
close(parent)
s_cs_xx.parametri.parametro_s_15 = "Annulla"
end event

type st_1 from statictext within w_input_manuale_collaudi_num
int X=23
int Y=1441
int Width=138
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Min.:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_input_manuale_collaudi_num
int X=686
int Y=1441
int Width=161
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Max.:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_min from statictext within w_input_manuale_collaudi_num
int X=161
int Y=1441
int Width=366
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_max from statictext within w_input_manuale_collaudi_num
int X=846
int Y=1441
int Width=366
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

