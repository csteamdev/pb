﻿$PBExportHeader$w_inserisci_altro_valore.srw
forward
global type w_inserisci_altro_valore from window
end type
type cb_annulla from commandbutton within w_inserisci_altro_valore
end type
type cb_ok from commandbutton within w_inserisci_altro_valore
end type
type mle_altro_valore from multilineedit within w_inserisci_altro_valore
end type
end forward

global type w_inserisci_altro_valore from window
integer width = 1051
integer height = 504
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
cb_ok cb_ok
mle_altro_valore mle_altro_valore
end type
global w_inserisci_altro_valore w_inserisci_altro_valore

type variables
string is_altro_valore
end variables

on w_inserisci_altro_valore.create
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.mle_altro_valore=create mle_altro_valore
this.Control[]={this.cb_annulla,&
this.cb_ok,&
this.mle_altro_valore}
end on

on w_inserisci_altro_valore.destroy
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.mle_altro_valore)
end on

event open;is_altro_valore = Message.StringParm

if s_cs_xx.parametri.parametro_d_10 = 9999 then
	mle_altro_valore.limit = 1000
	
elseif s_cs_xx.parametri.parametro_d_10 = 9998 then
	mle_altro_valore.limit = 255
end if

 s_cs_xx.parametri.parametro_d_10 = 0


mle_altro_valore.text = is_altro_valore


if s_cs_xx.parametri.parametro_d_11 = 6666 then
	//	ridimensiona la finestra
	//	sposta il pulsante OK e 
	//			togli la proprietà ACCEPT dal pulsante 
	//			per abilitare l'accettazione dell'invio per "a capo"
	this.height = 612
	this.width = 1920
	
	cb_ok.y = 500
	cb_ok.default = false
	
	cb_annulla.y = cb_ok.y
	
	mle_altro_valore.width = 1856
	
	
	s_cs_xx.parametri.parametro_d_11 = 0
end if
end event

type cb_annulla from commandbutton within w_inserisci_altro_valore
integer x = 503
integer y = 280
integer width = 402
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;closewithreturn(parent,"cs_team_esc_pressed")
end event

type cb_ok from commandbutton within w_inserisci_altro_valore
integer x = 46
integer y = 280
integer width = 402
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
boolean default = true
end type

event clicked;closewithreturn(parent,mle_altro_valore.text)
end event

type mle_altro_valore from multilineedit within w_inserisci_altro_valore
integer x = 23
integer y = 40
integer width = 1006
integer height = 440
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
integer limit = 40
end type

