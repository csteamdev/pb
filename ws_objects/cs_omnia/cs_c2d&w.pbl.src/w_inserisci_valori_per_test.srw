﻿$PBExportHeader$w_inserisci_valori_per_test.srw
forward
global type w_inserisci_valori_per_test from window
end type
type dw_cod_valori from datawindow within w_inserisci_valori_per_test
end type
type cb_esc from commandbutton within w_inserisci_valori_per_test
end type
end forward

global type w_inserisci_valori_per_test from window
integer width = 1632
integer height = 1132
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_cod_valori dw_cod_valori
cb_esc cb_esc
end type
global w_inserisci_valori_per_test w_inserisci_valori_per_test

type variables
s_tab_test_valori_griglia is_tab_test_valori_griglia
end variables

on w_inserisci_valori_per_test.create
this.dw_cod_valori=create dw_cod_valori
this.cb_esc=create cb_esc
this.Control[]={this.dw_cod_valori,&
this.cb_esc}
end on

on w_inserisci_valori_per_test.destroy
destroy(this.dw_cod_valori)
destroy(this.cb_esc)
end on

event open;is_tab_test_valori_griglia = Message.PowerObjectParm

dw_cod_valori.SetTransObject(SQLCA)
dw_cod_valori.retrieve(is_tab_test_valori_griglia.s_cod_test)

if dw_cod_valori.RowCount() > 0 then
	dw_cod_valori.object.t_titolo.Text = "Lista valori disponibili"
end if
end event

type dw_cod_valori from datawindow within w_inserisci_valori_per_test
integer y = 60
integer width = 1691
integer height = 1080
integer taborder = 10
string title = "none"
string dataobject = "d_cod_valori_test_griglia"
boolean vscrollbar = true
boolean border = false
end type

event buttonclicked;string ls_cod_valore, ls_des_valore

if row > 0 then	
	if dwo.name = "b_ok" then
		ls_cod_valore = getitemstring(row,"cod_valore")
		ls_des_valore = getitemstring(row,"des_valore")
		
		is_tab_test_valori_griglia.s_cod_valore = ls_cod_valore
		is_tab_test_valori_griglia.s_desc_valore = ls_des_valore
		closewithreturn(parent, is_tab_test_valori_griglia)
	end if
end if
end event

type cb_esc from commandbutton within w_inserisci_valori_per_test
integer x = 480
integer y = 560
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "ESC"
boolean cancel = true
end type

event clicked;is_tab_test_valori_griglia.s_cod_valore = "cs_team_esc_pressed"

closewithreturn(parent, is_tab_test_valori_griglia)
end event

