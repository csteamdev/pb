﻿$PBExportHeader$w_notifica_messaggi_lab.srw
forward
global type w_notifica_messaggi_lab from w_cs_xx_principale
end type
type sle_testo from multilineedit within w_notifica_messaggi_lab
end type
type cb_altri from commandbutton within w_notifica_messaggi_lab
end type
type dw_liste from datawindow within w_notifica_messaggi_lab
end type
type dw_destinatari from datawindow within w_notifica_messaggi_lab
end type
type st_2 from statictext within w_notifica_messaggi_lab
end type
type cb_invia from commandbutton within w_notifica_messaggi_lab
end type
type cbx_tutte from checkbox within w_notifica_messaggi_lab
end type
type cbx_multipla from checkbox within w_notifica_messaggi_lab
end type
type cb_tutti from commandbutton within w_notifica_messaggi_lab
end type
type cb_azzera from commandbutton within w_notifica_messaggi_lab
end type
type cb_seleziona from commandbutton within w_notifica_messaggi_lab
end type
type st_3 from statictext within w_notifica_messaggi_lab
end type
type st_allegato from statictext within w_notifica_messaggi_lab
end type
type cb_elimina from commandbutton within w_notifica_messaggi_lab
end type
type cb_annulla from commandbutton within w_notifica_messaggi_lab
end type
type st_4 from statictext within w_notifica_messaggi_lab
end type
type sle_oggetto from singlelineedit within w_notifica_messaggi_lab
end type
type st_5 from statictext within w_notifica_messaggi_lab
end type
type gb_2 from groupbox within w_notifica_messaggi_lab
end type
type gb_4 from groupbox within w_notifica_messaggi_lab
end type
type gb_5 from groupbox within w_notifica_messaggi_lab
end type
type gb_6 from groupbox within w_notifica_messaggi_lab
end type
type gb_8 from groupbox within w_notifica_messaggi_lab
end type
type gb_9 from groupbox within w_notifica_messaggi_lab
end type
type r_1 from rectangle within w_notifica_messaggi_lab
end type
type st_1 from statictext within w_notifica_messaggi_lab
end type
type r_2 from rectangle within w_notifica_messaggi_lab
end type
end forward

global type w_notifica_messaggi_lab from w_cs_xx_principale
integer x = 0
integer y = 0
integer width = 3671
integer height = 2076
string title = "Gestione Invio Messaggi"
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
sle_testo sle_testo
cb_altri cb_altri
dw_liste dw_liste
dw_destinatari dw_destinatari
st_2 st_2
cb_invia cb_invia
cbx_tutte cbx_tutte
cbx_multipla cbx_multipla
cb_tutti cb_tutti
cb_azzera cb_azzera
cb_seleziona cb_seleziona
st_3 st_3
st_allegato st_allegato
cb_elimina cb_elimina
cb_annulla cb_annulla
st_4 st_4
sle_oggetto sle_oggetto
st_5 st_5
gb_2 gb_2
gb_4 gb_4
gb_5 gb_5
gb_6 gb_6
gb_8 gb_8
gb_9 gb_9
r_1 r_1
st_1 st_1
r_2 r_2
end type
global w_notifica_messaggi_lab w_notifica_messaggi_lab

type variables
long    il_selezione = 0, il_liste = 0, il_num_sequenza = 0, il_num_sequenza_invio = 0

string  is_origine, is_where, is_tipo_lista, is_cod_lista, is_tipo_ultima = "", is_cod_ultima = "", is_messaggio

boolean ib_inviato = false, ib_log = true, ib_esci, ib_successiva = true, ib_fasi = false

uo_outlook iuo_outlook

long    il_anno_reclamo, il_numero_reclamo, il_anno_non_conf, il_num_non_conf, il_progressivo_budget

string  is_tipo_invio
end variables

forward prototypes
public function integer wf_carica_mansionari (string as_where)
public function integer wf_carica_liste (string as_tipo_lista, string as_cod_lista)
public function integer wf_elimina_destinatari (string as_tipo_lista, string as_cod_lista)
public function integer wf_unica (string as_tipo_lista, string as_cod_lista, boolean ab_elimina)
public function integer wf_carica_destinatari (string as_tipo_lista, string as_cod_lista)
public function integer wf_carica_destinatari_fasi (string as_tipo_lista, string as_cod_lista)
public subroutine wf_scrivi_stato (string fs_codici[])
end prototypes

public function integer wf_carica_mansionari (string as_where);long   ll_riga

string ls_select, ls_destinatario, ls_indirizzo


// Impostazione della select per la lettura dei destinatari dalla tabella mansionari

ls_select = &
"select cod_resp_divisione, " + &
"		  ind_casella_post " + &
"from	  mansionari " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

// Se è stata specificata una condizione per limitare la ricerca viene aggiornata la select

if not isnull(as_where) then
	ls_select = ls_select + " and " + as_where
end if

declare destinatari dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open destinatari;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore destinatari: " + sqlca.sqlerrtext)
	return -1
end if

dw_destinatari.reset()

do while true
	
	fetch destinatari
	into  :ls_destinatario,
			:ls_indirizzo;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore destinatari: " + sqlca.sqlerrtext)
		close destinatari;
		return -1
	elseif sqlca.sqlcode = 100	then
		close destinatari;
		exit
	end if
	
	// Il destinatario e il relativo indirizzo di posta elettronica vengono inseriti nell'elenco
	// Il destinatario è inizialmente deselezionato
	
	ll_riga = dw_destinatari.insertrow(0)
	dw_destinatari.setitem(ll_riga,"descrizione",ls_destinatario)
	dw_destinatari.setitem(ll_riga,"indirizzo",ls_indirizzo)
	dw_destinatari.setitem(ll_riga,"selezione","N")
	
loop

return 0
end function

public function integer wf_carica_liste (string as_tipo_lista, string as_cod_lista);boolean lb_trovato

long    ll_riga, ll_i

string  ls_select, ls_tipo_lista, ls_cod_lista, ls_des_lista, ls_flag_gestione_fasi


if as_tipo_lista = "" then
	setnull(as_tipo_lista)
end if

if as_cod_lista = "" then
	setnull(as_cod_lista)
end if

// Viene impostata la select per la lettura delle liste di distribuzione

ls_select = &
"select cod_tipo_lista_dist, " + &
"		  cod_lista_dist, " + &
"		  des_lista_dist " + &
"from	  tes_liste_dist " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

// Se è stata specificata una lista predefinita la select viene modificata per limitare la ricerca

if not isnull(as_tipo_lista) then
	ls_select = ls_select + " and cod_tipo_lista_dist = '" + as_tipo_lista + "' "
end if

if not isnull(as_cod_lista) then
	ls_select = ls_select + " and cod_lista_dist = '" + as_cod_lista + "' "
end if

declare liste dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open liste;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore liste: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch liste
	into  :ls_tipo_lista,
			:ls_cod_lista,
			:ls_des_lista;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore liste: " + sqlca.sqlerrtext)
		close liste;
		return -1
	elseif sqlca.sqlcode = 100	then
		close liste;
		exit
	end if
	
	lb_trovato = false
	
	// Si controlla che la lista appena letta non sia già presente nell'elenco
	
	for ll_i = 1 to dw_liste.rowcount()		
		if dw_liste.getitemstring(ll_i,"tipo_lista") = ls_tipo_lista and &
			dw_liste.getitemstring(ll_i,"cod_lista") = ls_cod_lista then
			lb_trovato = true
			exit
		end if
	next
	
	// Se è già presente la si salta per non resettare la selezione della lista e dei relativi destinatari
	
	if lb_trovato then
		continue
	end if
	
	// La lista letta viene inserita nell'elenco
	
	ll_riga = dw_liste.insertrow(0)
	dw_liste.setitem(ll_riga,"tipo_lista",ls_tipo_lista)
	dw_liste.setitem(ll_riga,"cod_lista",ls_cod_lista)
	dw_liste.setitem(ll_riga,"descrizione",ls_des_lista)
	
	// Se la lista letta è quella predefinita viene selezionata, altrimenti sarà inizialmente deselezionata
	
	if not isnull(as_tipo_lista) and not isnull(as_cod_lista) then
		dw_liste.setitem(ll_riga,"selezione","S")
		il_liste ++
		// *** se la gestione delle fasi è consentita allora carico i destinatari delle fasi
	
		if ib_fasi then
		
			wf_carica_destinatari_fasi(ls_tipo_lista,ls_cod_lista)
			
		else
			
			wf_carica_destinatari(ls_tipo_lista,ls_cod_lista)
			
		end if
			
	else
		dw_liste.setitem(ll_riga,"selezione","N")
	end if
	
loop

return 0
end function

public function integer wf_elimina_destinatari (string as_tipo_lista, string as_cod_lista);long ll_i, ll_count


// La funzione elimina dall'elenco dei destinatari tutti i destinatari appartenenti ad una certa lista di distribuzione

ll_count = dw_destinatari.rowcount()

for ll_i = dw_destinatari.rowcount() to 1 step -1
	
	// Si controlla se la lista di appartenenza del destinatario è quella indicata alla chiamata della funzione
	
	if dw_destinatari.getitemstring(ll_i,"tipo_lista") <> as_tipo_lista or &
		dw_destinatari.getitemstring(ll_i,"cod_lista") <> as_cod_lista then
		continue
	end if
	
	// Il destinatario viene eliminato
	
	if dw_destinatari.getitemstring(ll_i,"selezione") = "S" then
		
		il_selezione --		
			
		if il_selezione = 0 then
				
			cb_azzera.enabled = false
				
			cb_invia.enabled = false
				
		end if
		
	end if
	
	ll_count --
		
	if il_selezione < ll_count then
		
		cb_tutti.enabled = true
		
	end if
	
	dw_destinatari.deleterow(ll_i)
	
next

return 0
end function

public function integer wf_unica (string as_tipo_lista, string as_cod_lista, boolean ab_elimina);long   ll_i

string ls_tipo_lista, ls_cod_lista, ls_selezione


// La funzione elimina o deseleziona tutte le liste caricate nell'elenco eccetto quella indicata

for ll_i = dw_liste.rowcount() to 1 step -1

	ls_tipo_lista = dw_liste.getitemstring(ll_i,"tipo_lista")
	
	ls_cod_lista = dw_liste.getitemstring(ll_i,"cod_lista")
	
	ls_selezione = dw_liste.getitemstring(ll_i,"selezione")
	
	// Se il parametro ab_elimina è a TRUE la lista viene eliminata, altrimenti solo deselezionata
	
	if ls_tipo_lista <> as_tipo_lista and ls_cod_lista <> as_cod_lista then
	
		if ab_elimina then
			
			if ls_selezione = "S" then
				il_liste --
			end if
		
			dw_liste.deleterow(ll_i)
				
		else
		
			if ls_selezione = "S" then
				il_liste --
				dw_liste.setitem(ll_i,"selezione","N")
			end if
				
		end if
		
		// Se la lista viene deselezionata o eliminata, i relativi destinatari devono essere eliminati dall'elenco
		
		wf_elimina_destinatari(ls_tipo_lista,ls_cod_lista)
		
	end if

next

return 0
end function

public function integer wf_carica_destinatari (string as_tipo_lista, string as_cod_lista);long   ll_riga, ll_sequenza_invio

string ls_select, ls_destinatario, ls_indirizzo, ls_cod_destinatario

ls_select = &
"select cod_destinatario, " + &
"       des_destinatario, " + &
"		  indirizzo " + &
"from	  tab_ind_dest " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"       email = 'S' and " + &
"       cod_destinatario in ( select cod_destinatario " + &
"										from   det_liste_dist " + &
"									   where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"                                    cod_tipo_lista_dist = '" + as_tipo_lista + "' and " +&
"                                    cod_lista_dist = '" + as_cod_lista + "' ) "

declare destinatari dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open destinatari;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore destinatari: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch destinatari
	into  :ls_cod_destinatario,
	      :ls_destinatario,
			:ls_indirizzo;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore destinatari: " + sqlca.sqlerrtext)
		close destinatari;
		return -1
	elseif sqlca.sqlcode = 100	then
		close destinatari;
		exit
	end if
	
	// Ogni destinatario trovato viene inserito nell'elenco e sarà inizialmente deselezionato
	// Per identificazione la lista di appartenenza di ogni destinatario vengono riportati tipo e codice della lista
	
	ll_riga = dw_destinatari.insertrow(0)
	dw_destinatari.setitem(ll_riga,"tipo_lista",as_tipo_lista)
	dw_destinatari.setitem(ll_riga,"cod_lista",as_cod_lista)
	dw_destinatari.setitem(ll_riga,"codice",ls_cod_destinatario)
	dw_destinatari.setitem(ll_riga,"descrizione",ls_destinatario)
	dw_destinatari.setitem(ll_riga,"indirizzo",ls_indirizzo)
	dw_destinatari.setitem(ll_riga,"selezione","N")
	
loop

return 0
end function

public function integer wf_carica_destinatari_fasi (string as_tipo_lista, string as_cod_lista);long   ll_riga, ll_sequenza_invio, ll_sequenza_destinatari

string ls_select, ls_destinatario, ls_indirizzo, ls_cod_destinatario, ls_cod_destinatario_sel, ls_flag_tipo_dist_multipla

// ** utente fittizio per la fase

select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca del parametro PCU: " + sqlca.sqlerrtext)
	return -1
end if

//// leggo se devo prendere la fase successiva o quella corrente
//
choose case ib_successiva
		
	case true
		
		select num_sequenza
		into   :ll_sequenza_invio
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :as_tipo_lista and
				 cod_lista_dist = :as_cod_lista and
				 num_sequenza_prec = :il_num_sequenza;
				 
	case false
		
		ll_sequenza_invio = il_num_sequenza
		
end choose

if isnull(ll_sequenza_invio) or ll_sequenza_invio = 0 then
	
	select num_sequenza
	into   :ll_sequenza_invio
	from   det_liste_dist
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    cod_tipo_lista_dist = :as_tipo_lista and
			 cod_lista_dist = :as_cod_lista and
			 cod_destinatario = :ls_cod_destinatario and
	       (num_sequenza_prec = 0 or num_sequenza_prec is null);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Attenzione, errore durante la ricerca della sequenza di partenza: " + sqlca.sqlerrtext)
		return -1		
	end if
	
end if

il_num_sequenza_invio = ll_sequenza_invio

// *** michela 18/04/2005: devo prendere i destinatari della successiva a cui intendo inviare

select num_sequenza
into   :ll_sequenza_destinatari
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_lista_dist = :as_tipo_lista and
		 cod_lista_dist = :as_cod_lista and
		 num_sequenza_prec = :ll_sequenza_invio;
		 
choose case ib_successiva
	case false
		ll_sequenza_destinatari = ll_sequenza_invio
end choose	

if isnull(ll_sequenza_destinatari) or ll_sequenza_destinatari = 0 then   // in questo caso sono all'ultima fase, quindi avverto l'utente che non ci sono più destinatari
	g_mb.messagebox( "OMNIA", "Attenzione: non ci sono altre fasi nel ciclo di vita! Procedere con la chiusura del documento.")
	return -1
end if

// ** controllo
select flag_tipo_dist_multipla
into   :ls_flag_tipo_dist_multipla
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_lista_dist = :as_tipo_lista and 
		 cod_lista_dist = :as_cod_lista and
		 cod_destinatario = :ls_cod_destinatario and
		 num_sequenza = :ll_sequenza_destinatari;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca del tipo di distribuzione: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ls_flag_tipo_dist_multipla) then ls_flag_tipo_dist_multipla = ""
				 
ls_select = &
"select cod_destinatario, " + &
"       des_destinatario, " + &
"		  indirizzo " + &
"from	  tab_ind_dest " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"       email = 'S' and " + &
"       cod_destinatario in ( select cod_destinatario_mail " + &
"										from   det_liste_dist_destinatari " + &
"									   where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"                                    cod_tipo_lista_dist = '" + as_tipo_lista + "' and " +&
"                                    cod_lista_dist = '" + as_cod_lista + "' and " +&
"                                    cod_destinatario = '" + ls_cod_destinatario + "' and " +&
"												 num_sequenza = " + string(ll_sequenza_destinatari) 

choose case ls_flag_tipo_dist_multipla
		
	case "A"
		
		if s_cs_xx.parametri.parametro_s_9 <> "" then
			
			ls_select += " and ( cod_area_aziendale = '" + s_cs_xx.parametri.parametro_s_9 + "'  or cod_area_aziendale is null ) "
			
		end if
		
	case "C"
		
		if s_cs_xx.parametri.parametro_s_10 <> "" then
			
			ls_select += " and ( cod_cat_mer = '" + s_cs_xx.parametri.parametro_s_10 + "'  or cod_cat_mer is null ) "
			ls_select += " and ( cod_area_aziendale = '" + s_cs_xx.parametri.parametro_s_9 + "'  or cod_area_aziendale is null ) "
			
		end if		
		
end choose

ls_select += " ) "

declare destinatari dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open destinatari;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore destinatari: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch destinatari
	into  :ls_cod_destinatario_sel,
	      :ls_destinatario,
			:ls_indirizzo;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore destinatari: " + sqlca.sqlerrtext)
		close destinatari;
		return -1
	elseif sqlca.sqlcode = 100	then
		close destinatari;
		exit
	end if
	
	// Ogni destinatario trovato viene inserito nell'elenco e sarà inizialmente deselezionato
	// Per identificazione la lista di appartenenza di ogni destinatario vengono riportati tipo e codice della lista
	
	ll_riga = dw_destinatari.insertrow(0)
	dw_destinatari.setitem(ll_riga,"tipo_lista",as_tipo_lista)
	dw_destinatari.setitem(ll_riga,"cod_lista",as_cod_lista)
	dw_destinatari.setitem(ll_riga,"codice",ls_cod_destinatario_sel)	
	dw_destinatari.setitem(ll_riga,"descrizione",ls_destinatario)
	dw_destinatari.setitem(ll_riga,"indirizzo",ls_indirizzo)
	dw_destinatari.setitem(ll_riga,"selezione","S")
	
loop
cb_invia.enabled = true
return 0
end function

public subroutine wf_scrivi_stato (string fs_codici[]);string   ls_cod_destinatario

long     ll_i, ll_anno_registrazione, ll_num_registrazione

datetime ldt_data_invio, ldt_ora_invio

date		ldt_null

time     lt_tempo

setnull(ldt_null)

ll_anno_registrazione = year(today())

ldt_data_invio = datetime( date(today()), 00:00:00)

lt_tempo = now()

ldt_ora_invio = datetime( date(today()), lt_tempo)

for ll_i = 1 to Upperbound(fs_codici)
	
	ls_cod_destinatario = fs_codici[ll_i]
	
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   det_liste_dist_fasi_com
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la ricerca del numero di registrazione: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if isnull(ll_num_registrazione) then ll_num_registrazione = 0
	
	ll_num_registrazione = ll_num_registrazione + 1
	
	insert into det_liste_dist_fasi_com (cod_azienda,
	                                     anno_registrazione,
													 num_registrazione,
													 anno_non_conf,
													 num_non_conf,
													 anno_reg_intervento,
													 num_reg_intervento,
													 cod_tipo_lista_dist,
													 cod_lista_dist,
													 num_sequenza,
													 cod_utente,
													 cod_destinatario,
													 data_invio,
													 ora_invio,
													 progressivo)
								values         (:s_cs_xx.cod_azienda,
								                :ll_anno_registrazione,
													 :ll_num_registrazione,
													 :il_anno_non_conf,
													 :il_num_non_conf,
													 :il_anno_reclamo,
													 :il_numero_reclamo,
													 :is_tipo_lista,
													 :is_cod_lista,
													 :il_num_sequenza_invio,
													 :s_cs_xx.cod_utente,
													 :ls_cod_destinatario,
													 :ldt_data_invio,
													 :ldt_ora_invio,
													 :il_progressivo_budget);
													 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento di un destinatario: " + sqlca.sqlerrtext)
		rollback;
		return
	end if

	commit;
	
next
end subroutine

on w_notifica_messaggi_lab.create
int iCurrent
call super::create
this.sle_testo=create sle_testo
this.cb_altri=create cb_altri
this.dw_liste=create dw_liste
this.dw_destinatari=create dw_destinatari
this.st_2=create st_2
this.cb_invia=create cb_invia
this.cbx_tutte=create cbx_tutte
this.cbx_multipla=create cbx_multipla
this.cb_tutti=create cb_tutti
this.cb_azzera=create cb_azzera
this.cb_seleziona=create cb_seleziona
this.st_3=create st_3
this.st_allegato=create st_allegato
this.cb_elimina=create cb_elimina
this.cb_annulla=create cb_annulla
this.st_4=create st_4
this.sle_oggetto=create sle_oggetto
this.st_5=create st_5
this.gb_2=create gb_2
this.gb_4=create gb_4
this.gb_5=create gb_5
this.gb_6=create gb_6
this.gb_8=create gb_8
this.gb_9=create gb_9
this.r_1=create r_1
this.st_1=create st_1
this.r_2=create r_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_testo
this.Control[iCurrent+2]=this.cb_altri
this.Control[iCurrent+3]=this.dw_liste
this.Control[iCurrent+4]=this.dw_destinatari
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.cb_invia
this.Control[iCurrent+7]=this.cbx_tutte
this.Control[iCurrent+8]=this.cbx_multipla
this.Control[iCurrent+9]=this.cb_tutti
this.Control[iCurrent+10]=this.cb_azzera
this.Control[iCurrent+11]=this.cb_seleziona
this.Control[iCurrent+12]=this.st_3
this.Control[iCurrent+13]=this.st_allegato
this.Control[iCurrent+14]=this.cb_elimina
this.Control[iCurrent+15]=this.cb_annulla
this.Control[iCurrent+16]=this.st_4
this.Control[iCurrent+17]=this.sle_oggetto
this.Control[iCurrent+18]=this.st_5
this.Control[iCurrent+19]=this.gb_2
this.Control[iCurrent+20]=this.gb_4
this.Control[iCurrent+21]=this.gb_5
this.Control[iCurrent+22]=this.gb_6
this.Control[iCurrent+23]=this.gb_8
this.Control[iCurrent+24]=this.gb_9
this.Control[iCurrent+25]=this.r_1
this.Control[iCurrent+26]=this.st_1
this.Control[iCurrent+27]=this.r_2
end on

on w_notifica_messaggi_lab.destroy
call super::destroy
destroy(this.sle_testo)
destroy(this.cb_altri)
destroy(this.dw_liste)
destroy(this.dw_destinatari)
destroy(this.st_2)
destroy(this.cb_invia)
destroy(this.cbx_tutte)
destroy(this.cbx_multipla)
destroy(this.cb_tutti)
destroy(this.cb_azzera)
destroy(this.cb_seleziona)
destroy(this.st_3)
destroy(this.st_allegato)
destroy(this.cb_elimina)
destroy(this.cb_annulla)
destroy(this.st_4)
destroy(this.sle_oggetto)
destroy(this.st_5)
destroy(this.gb_2)
destroy(this.gb_4)
destroy(this.gb_5)
destroy(this.gb_6)
destroy(this.gb_8)
destroy(this.gb_9)
destroy(this.r_1)
destroy(this.st_1)
destroy(this.r_2)
end on

event pc_setwindow;call super::pc_setwindow;long   ll_return

string ls_test, ls_flag_gestione_fasi, ls_1, ls_2


// Lettura origine dei destinatari:
// 	L = liste di distribuzione
// 	M = mansionari

is_origine = s_cs_xx.parametri.parametro_s_1

// Se is_origine = L saranno necessari tipo e codice della lista di distribuzione da caricare:
// tali campi sono in chiave nella tabella delle liste di distribuzione e servono ad individuare la lista predefinita

is_tipo_lista = s_cs_xx.parametri.parametro_s_2

is_cod_lista = s_cs_xx.parametri.parametro_s_3

ib_fasi = false

if not isnull(is_tipo_lista) then
	
	select flag_gestione_fasi
	into   :ls_flag_gestione_fasi
	from   tab_tipi_liste_dist
	where  cod_tipo_lista_dist = :is_tipo_lista;
			 
	if ls_flag_gestione_fasi = "S" then ib_fasi = true
	
end if

// Se is_origine = M è possibile ridurre il campo di ricerca della select sulla tabella mansionari usando is_where:
// la condizione deve essere un stringa del tipo "nome_colonna = valore ". X esempio: " flag_chiusura_nc = 'S' "

is_where = s_cs_xx.parametri.parametro_s_4

// Oggetto del messaggio

sle_oggetto.text = s_cs_xx.parametri.parametro_s_5

// Testo del messaggio

sle_testo.text = s_cs_xx.parametri.parametro_s_6

// Testo del messaggio da utilizzare per la messagebox di conferma dell'invio del messaggio
// Ad esempio: "Inviare un messaggio ai destinatari della lista di distribuzione collegata?"

is_messaggio = s_cs_xx.parametri.parametro_s_7

// nel parametro 8 metto l'allegato

if not isnull(s_cs_xx.parametri.parametro_s_8) and s_cs_xx.parametri.parametro_s_8 <> "" and left(s_cs_xx.parametri.parametro_s_8, 3) = "c:\" then
	st_allegato.text = s_cs_xx.parametri.parametro_s_8
end if

// leggo anno, numero, sequenza reclamo

il_anno_reclamo = long(s_cs_xx.parametri.parametro_s_11)

il_numero_reclamo = long(s_cs_xx.parametri.parametro_s_12)

il_anno_non_conf = long(s_cs_xx.parametri.parametro_s_13)

il_num_non_conf = long(s_cs_xx.parametri.parametro_s_14)

il_progressivo_budget = long(s_cs_xx.parametri.parametro_d_1)

if s_cs_xx.parametri.parametro_s_15 = "N" then
	setnull(il_anno_reclamo)
	setnull(il_numero_reclamo)
	setnull(il_progressivo_budget)
elseif s_cs_xx.parametri.parametro_s_15 = "R" then
	setnull(il_anno_non_conf)
	setnull(il_num_non_conf)	
	setnull(il_progressivo_budget)
elseif s_cs_xx.parametri.parametro_s_15 = "B" then
	setnull(il_anno_reclamo)
	setnull(il_numero_reclamo)
	setnull(il_anno_non_conf)
	setnull(il_num_non_conf)		
else
	setnull(il_anno_reclamo)
	setnull(il_numero_reclamo)
	setnull(il_anno_non_conf)
	setnull(il_num_non_conf)	
	setnull(il_progressivo_budget)
end if

il_num_sequenza = s_cs_xx.parametri.parametro_ul_3

ib_successiva = s_cs_xx.parametri.parametro_b_1

// Viene richiamata la funzione che controlla se è abilitato l'uso delle email e richiede la conferma dell'utente

iuo_outlook = create uo_outlook

ll_return = iuo_outlook.uof_controllo(ref is_messaggio,true)

if ll_return = 100 then
	// Mail non abilitate
	g_mb.messagebox("OMNIA",is_messaggio)
	destroy iuo_outlook
	ib_esci = true
	return -1
elseif ll_return = 50 then
	// L'utente ha deciso di non continuare
	destroy iuo_outlook
	ib_esci = true
	return -1
else
	// Autorizzazione a procedere
	ib_esci = false
end if

destroy iuo_outlook

// Distinzione del tipo origine destinatari
cb_altri.enabled = false

choose case is_origine
		
	case "L"
		
		// Caricamento della lista di distribuzione predefinita
		
		wf_carica_liste(is_tipo_lista,is_cod_lista)
		
		if ib_fasi then
			
			cb_altri.enabled = true
			cbx_tutte.enabled = false		
			cbx_multipla.enabled = false
			
		else
			
			cb_altri.enabled = true
			
		end if
		
	case "M"
		
		// Caricamento dei mansionari che corrispondono alla condizione indicata
		// Le checkbox per la visualizzazione di tutte le liste e la selezione multipla delle liste vengono disabilitate
		
		cbx_tutte.enabled = false
		
		cbx_multipla.enabled = false
		
		wf_carica_mansionari(is_where)
		
	case else
		
		g_mb.messagebox("OMNIA","Tipo origine destinatari specificato non valido",stopsign!)
		postevent("close")
		
end choose

// Controllo per l'abilitazione della registrtazione su file LOG

select stringa
into   :ls_test
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LOG';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Funzione LOG INVIO MESSAGGI disabilitata")
	ib_log = false
end if	
end event

event closequery;call super::closequery;if ib_inviato = false then
	if g_mb.messagebox("OMNIA","Uscire senza inviare il messaggio?",question!,yesno!,2) = 2 then
		return 1
	end if
end if
end event

event open;call super::open;if ib_esci then
	ib_inviato = true
	cb_annulla.postevent("clicked")
end if

setnull(s_cs_xx.parametri.parametro_ul_1)
end event

type sle_testo from multilineedit within w_notifica_messaggi_lab
integer x = 297
integer y = 1260
integer width = 3305
integer height = 360
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_altri from commandbutton within w_notifica_messaggi_lab
integer x = 1787
integer y = 1876
integer width = 347
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Altri Dest."
end type

event clicked;string ls_flag_blocco, ls_cod_destinatario, ls_destinatari[]

long   ll_i

setnull(s_cs_xx.parametri.parametro_uo_dw_1)

s_cs_xx.parametri.parametro_dw_1 = dw_destinatari

window_open(w_destinatari_ricerca, 0)

end event

type dw_liste from datawindow within w_notifica_messaggi_lab
integer x = 23
integer y = 140
integer width = 1783
integer height = 900
integer taborder = 10
string dataobject = "d_invio_messaggi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;string ls_tipo_lista, ls_cod_lista, ls_nome, ls_flag_gestione_fasi


if dwo.name = "selezione" then
	
	ls_tipo_lista = getitemstring(row,"tipo_lista")
	
	ls_cod_lista = getitemstring(row,"cod_lista")
	
	choose case data
			
		case "S"
			
			// Una lista è stata selezionata, se non è attiva la selezione multipla l'ultima esclude le altre
			
			if il_liste = 1 and cbx_multipla.checked = false then				
				wf_unica(ls_tipo_lista,ls_cod_lista,false)
			end if
			
			il_liste ++
			
			is_tipo_ultima = ls_tipo_lista
			
			is_cod_ultima = ls_cod_lista
			
			// Vengono caricati nell'elenco i destinatari della lista selezionata
			if ib_fasi then
			
				wf_carica_destinatari_fasi(ls_tipo_lista,ls_cod_lista)
			
			else
				
				wf_carica_destinatari(ls_tipo_lista,ls_cod_lista)
				
			end if
			
		case "N"
			
			// Lista deselezionata, i destinatari della lista deselezionata vengono eliminati dall'elenco
			
			il_liste --
			
			wf_elimina_destinatari(ls_tipo_lista,ls_cod_lista)
			
	end choose
	
end if
end event

type dw_destinatari from datawindow within w_notifica_messaggi_lab
event ue_inserisci_destinatari ( )
event ue_controlla ( )
integer x = 1851
integer y = 140
integer width = 1783
integer height = 900
integer taborder = 10
string dataobject = "d_invio_messaggi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_inserisci_destinatari();string ls_flag_blocco, ls_cod_destinatario, ls_des_destinatario, ls_indirizzo

long   ll_i, ll_riga

if UpperBound(s_cs_xx.parametri.parametro_s_1_a) < 1 then return 

for ll_i = 1 to UpperBound(s_cs_xx.parametri.parametro_s_1_a)
	
	ls_cod_destinatario = s_cs_xx.parametri.parametro_s_1_a[ll_i]
	
	select flag_blocco,
	       des_destinatario,
			 indirizzo
	into   :ls_flag_blocco,
	       :ls_des_destinatario,
			 :ls_indirizzo
	from   tab_ind_dest
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_destinatario = :ls_cod_destinatario;
	
	if ls_flag_blocco = "S" then continue
	
	ll_riga = dw_destinatari.insertrow(0)
	dw_destinatari.setitem(ll_riga, "tipo_lista", is_tipo_lista)
	dw_destinatari.setitem(ll_riga, "cod_lista", is_cod_lista)
	dw_destinatari.setitem(ll_riga, "codice", ls_cod_destinatario)	
	dw_destinatari.setitem(ll_riga, "descrizione", ls_des_destinatario)
	dw_destinatari.setitem(ll_riga, "indirizzo", ls_indirizzo)
	dw_destinatari.setitem(ll_riga, "selezione", "S")							 	
	
next	

cb_invia.enabled = true

return



end event

event ue_controlla();long	ll_i, ll_cont

ll_cont = 0

for ll_i = 1 to dw_destinatari.rowcount()
	if getitemstring( ll_i, "selezione") = "S" then ll_cont ++
next

if ll_cont = 0 then
	cb_azzera.enabled = false
	cb_invia.enabled = false
else
	cb_azzera.enabled = true
	cb_invia.enabled = true	
end if
end event

event itemchanged;long ll_count


ll_count = dw_destinatari.rowcount()

if dwo.name = "selezione" then
	
	choose case data
			
		case "S"
			
			// Nuovo destinatario selezionato
			
			il_selezione ++
			
			cb_invia.enabled = true
			
			cb_azzera.enabled = true
			
			if il_selezione = ll_count then
				
				cb_tutti.enabled = false
				
			end if
			
		case "N"
			
			// Un destinatario è stato deselezionato
			
			il_selezione --
			
			cb_tutti.enabled = true
			
			if il_selezione = 0 then
				
				cb_azzera.enabled = false
				
				cb_invia.enabled = false
				
			end if				
			
	end choose
	
end if

postevent("ue_controlla")
end event

type st_2 from statictext within w_notifica_messaggi_lab
integer x = 1874
integer y = 40
integer width = 1737
integer height = 60
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Destinatari"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_invia from commandbutton within w_notifica_messaggi_lab
integer x = 3259
integer y = 1872
integer width = 343
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Invia"
end type

event clicked;boolean lb_conferma

long    ll_i, ll_j, ll_return, ll_num_sequenza_respingi

string  ls_destinatari[], ls_allegati[1], ls_messaggio, ls_codici[], ls_1, ls_2, ls_3, ls_oggetto, ls_testo


// Lettura file allegato

ls_allegati[1] = st_allegato.text

ls_oggetto = sle_oggetto.text

ls_testo = sle_testo.text

if isnull(ls_oggetto) then ls_oggetto = ""

if isnull(ls_testo) then ls_testo = ""


//commentato perchè servivano nella gestione delle NC *************************************************
/*
// ** se esiste il parametro omnia IPI = indirizzo pagina intranet allora lo aggiungo al messaggio
// **                              IPN = indirizzo pagina non conformita
// **                              IPR = indirizzo pagina reclami

select stringa
into   :ls_1
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'IPI';
		 
if not isnull(ls_1) and ls_1 <> "" then
	
	select stringa
	into   :ls_2
	from   parametri_omnia
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'IPN';
		
	select stringa
	into   :ls_3
	from   parametri_omnia
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'IPR';
				
	if (not isnull(ls_2) and ls_2 <> "") or ( not isnull(ls_3) and ls_3 <> "" ) then
		
		if ( not isnull(il_anno_reclamo) and not isnull(il_numero_reclamo) ) or ( not isnull(il_anno_non_conf) and not isnull(il_num_non_conf) ) then
		
			if not isnull(il_anno_reclamo) and not isnull(il_numero_reclamo) and not isnull(ls_3) and ls_3 <> "" then
				
				ls_1 = "    " + ls_1 + "?pagina=" + ls_3 + "&anno=" + string(il_anno_reclamo) + "&numero=" + string(il_numero_reclamo)
				
				ls_testo = ls_testo + ls_1
			elseif not isnull(il_anno_non_conf) and not isnull(il_num_non_conf) and not isnull(ls_2) and ls_2 <> ""  then
				
				ls_1 = "    " + ls_1 + "?pagina=" + ls_2 + "&anno=" + string(il_anno_non_conf) + "&numero=" + string(il_num_non_conf)

				ls_testo = ls_testo + ls_1
			end if
			
		end if	
	end if
	
end if
//--*************************************
*/

// Lettura destinatari
	
ll_j = 0
	
for ll_i = 1 to dw_destinatari.rowcount()
		
	if dw_destinatari.getitemstring(ll_i,"selezione") = "S" then
		ll_j ++
		ls_codici[ll_j] = dw_destinatari.getitemstring( ll_i, "codice")
		ls_destinatari[ll_j] = dw_destinatari.getitemstring(ll_i,"indirizzo")
	end if
	
next

// Invio messaggio

iuo_outlook = create uo_outlook

if iuo_outlook.uof_outlook(0,"A", ls_oggetto, ls_testo,ls_destinatari,ls_allegati,false,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy iuo_outlook
	return -1
else
	g_mb.messagebox("OMNIA","Messaggio inviato!", Information!)
end if

destroy iuo_outlook

// *** michela 25/03/2005: se ho la gestione delle fasi, allora memorizzo tutto sull'apposita tabella
//                         in base alla specifica mod_nc_resi_reclami 03 di marr

if ib_fasi then

	wf_scrivi_stato( ls_codici )
	
	if not ib_successiva then    ///*** se ho fatto un respingi, devo decrementare la fase, torno alla precedente
	
		select num_sequenza_prec
		into   :ll_num_sequenza_respingi
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :is_tipo_lista and
				 cod_lista_dist = :is_cod_lista and
				 num_sequenza = :il_num_sequenza_invio;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "OMNIA", "Errore durante la ricerca della sequenza precedente da pulsante respingi: " + sqlca.sqlerrtext)
		else
			if isnull(ll_num_sequenza_respingi) or ll_num_sequenza_respingi = 0 then
				ll_num_sequenza_respingi = -1
			end if
			il_num_sequenza_invio = ll_num_sequenza_respingi
		end if
	end if
	
	s_cs_xx.parametri.parametro_ul_1 = il_num_sequenza_invio

end if

// *** fine modifica

// Eventuale registrazione su file LOG

if ib_log then
	for ll_i = 1 to upperbound(ls_destinatari)
		f_scrivi_log("Messaggio ~"" + sle_oggetto.text + "~" inviato a: " + ls_destinatari[ll_i])
	next
end if

ib_inviato = true

close(parent)
end event

type cbx_tutte from checkbox within w_notifica_messaggi_lab
integer x = 46
integer y = 1880
integer width = 855
integer height = 76
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Mostra tutte le liste disponibili"
end type

event clicked;if checked then
	
	// L'utente ha scelto di visualizzare tutte le liste di disttribuzione presenti nel DB

	wf_carica_liste("","")
	
else
	
	// L'utente ha scelto di visualizzare solo la lista predefinita già proposta all'apertura della maschera
	
	if g_mb.messagebox("OMNIA","Eliminare dall'elenco tutte le liste eccetto quella predefinita?",question!,yesno!,2) = 2 then
		checked = true
		return
	end if
	
	wf_unica(is_tipo_lista,is_cod_lista,true)
	
end if
end event

type cbx_multipla from checkbox within w_notifica_messaggi_lab
integer x = 937
integer y = 1880
integer width = 791
integer height = 76
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Consenti selezione multipla"
end type

event clicked;long ll_count, ll_i


if checked = false then
	
	ll_count = 0
	
	for ll_i = 1 to dw_liste.rowcount()		
		if dw_liste.getitemstring(ll_i,"selezione") = "S" then
			ll_count ++
		end if		
	next
	
	if ll_count < 2 then
		return
	end if
	
	// Se si disabilita la selezione multipla e sono selezionate più di una lista allora bisogna tenerne solo una
	
	if g_mb.messagebox("OMNIA","Eliminare la selezione di tutte le liste eccetto l'ultima?",question!,yesno!,2) = 2 then
		checked = true
		return
	end if
	
	wf_unica(is_tipo_ultima,is_cod_ultima,false)
	
end if
end event

type cb_tutti from commandbutton within w_notifica_messaggi_lab
integer x = 2528
integer y = 1872
integer width = 343
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Tutti"
end type

event clicked;long ll_i


for ll_i = 1 to dw_destinatari.rowcount()	
	dw_destinatari.setitem(ll_i,"selezione","S")	
next

enabled = false

cb_azzera.enabled = true

cb_invia.enabled = true
end event

type cb_azzera from commandbutton within w_notifica_messaggi_lab
integer x = 2162
integer y = 1872
integer width = 343
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Azzera"
end type

event clicked;long ll_i


for ll_i = 1 to dw_destinatari.rowcount()	
	dw_destinatari.setitem(ll_i,"selezione","N")	
next

enabled = false

cb_tutti.enabled = true

cb_invia.enabled = false
end event

type cb_seleziona from commandbutton within w_notifica_messaggi_lab
integer x = 2894
integer y = 1712
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Seleziona"
end type

event clicked;string ls_filename


if st_allegato.text <> "" then
	if g_mb.messagebox("OMNIA","Sostituire l'allegato attualmente selezionato?",question!,yesno!,2) = 2 then
		return
	end if
end if

getfileopenname("Selezione Allegato",st_allegato.text,ls_filename,"*","Tutti i file (*.*),*.*")

if st_allegato.text <> "" then
	cb_elimina.enabled = true
end if
end event

type st_3 from statictext within w_notifica_messaggi_lab
integer x = 46
integer y = 1724
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Allegato:"
boolean focusrectangle = false
end type

type st_allegato from statictext within w_notifica_messaggi_lab
integer x = 297
integer y = 1712
integer width = 2560
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_elimina from commandbutton within w_notifica_messaggi_lab
integer x = 3259
integer y = 1712
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Elimina"
end type

event clicked;if g_mb.messagebox("OMNIA","Eliminare l'allegato attualmente selezionato?",question!,yesno!,2) = 2 then
	return
end if

st_allegato.text = ""

enabled = false
end event

type cb_annulla from commandbutton within w_notifica_messaggi_lab
integer x = 2894
integer y = 1872
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(parent)
end event

type st_4 from statictext within w_notifica_messaggi_lab
integer x = 41
integer y = 1104
integer width = 233
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Oggetto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_oggetto from singlelineedit within w_notifica_messaggi_lab
integer x = 297
integer y = 1088
integer width = 3305
integer height = 92
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_5 from statictext within w_notifica_messaggi_lab
integer x = 41
integer y = 1264
integer width = 233
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Testo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_2 from groupbox within w_notifica_messaggi_lab
integer x = 1760
integer y = 1820
integer width = 1874
integer height = 160
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_4 from groupbox within w_notifica_messaggi_lab
integer x = 23
integer y = 1660
integer width = 3611
integer height = 160
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_5 from groupbox within w_notifica_messaggi_lab
integer x = 914
integer y = 1820
integer width = 846
integer height = 160
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_6 from groupbox within w_notifica_messaggi_lab
integer x = 23
integer y = 1820
integer width = 891
integer height = 160
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_8 from groupbox within w_notifica_messaggi_lab
integer x = 23
integer y = 1200
integer width = 3611
integer height = 440
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_9 from groupbox within w_notifica_messaggi_lab
integer x = 23
integer y = 1040
integer width = 3611
integer height = 160
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type r_1 from rectangle within w_notifica_messaggi_lab
integer linethickness = 1
long fillcolor = 12632256
integer x = 23
integer y = 20
integer width = 1783
integer height = 104
end type

type st_1 from statictext within w_notifica_messaggi_lab
integer x = 46
integer y = 40
integer width = 1737
integer height = 60
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Liste di Distribuzione"
alignment alignment = center!
boolean focusrectangle = false
end type

type r_2 from rectangle within w_notifica_messaggi_lab
integer linethickness = 1
long fillcolor = 12632256
integer x = 1851
integer y = 20
integer width = 1783
integer height = 104
end type

