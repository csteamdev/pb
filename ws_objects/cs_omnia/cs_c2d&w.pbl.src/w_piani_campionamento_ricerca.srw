﻿$PBExportHeader$w_piani_campionamento_ricerca.srw
forward
global type w_piani_campionamento_ricerca from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_piani_campionamento_ricerca
end type
type sle_descrizione from singlelineedit within w_piani_campionamento_ricerca
end type
type cb_annulla from uo_cb_close within w_piani_campionamento_ricerca
end type
type cb_ok from commandbutton within w_piani_campionamento_ricerca
end type
type dw_piani_ricerca from uo_cs_xx_dw within w_piani_campionamento_ricerca
end type
end forward

global type w_piani_campionamento_ricerca from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2555
integer height = 1380
string title = "Ricerca Piani Campionamento"
cb_1 cb_1
sle_descrizione sle_descrizione
cb_annulla cb_annulla
cb_ok cb_ok
dw_piani_ricerca dw_piani_ricerca
end type
global w_piani_campionamento_ricerca w_piani_campionamento_ricerca

type variables
long il_row
end variables

event deactivate;call super::deactivate;this.hide()
end event

event pc_setwindow;call super::pc_setwindow;
dw_piani_ricerca.set_dw_key("cod_azienda")
dw_piani_ricerca.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_retrieveasneeded + c_selectonrowfocuschange, &
                                c_default)



dw_piani_ricerca.setfocus()

end event

on w_piani_campionamento_ricerca.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.sle_descrizione=create sle_descrizione
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_piani_ricerca=create dw_piani_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.sle_descrizione
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_ok
this.Control[iCurrent+5]=this.dw_piani_ricerca
end on

on w_piani_campionamento_ricerca.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.sle_descrizione)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_piani_ricerca)
end on

type cb_1 from commandbutton within w_piani_campionamento_ricerca
integer x = 2377
integer y = 20
integer width = 114
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;parent.triggerevent("pc_retrieve")
end event

type sle_descrizione from singlelineedit within w_piani_campionamento_ricerca
integer x = 23
integer y = 20
integer width = 2331
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_annulla from uo_cb_close within w_piani_campionamento_ricerca
integer x = 1737
integer y = 1180
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type cb_ok from commandbutton within w_piani_campionamento_ricerca
integer x = 2126
integer y = 1180
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;long     ll_riga, ll_prog_piano

datetime ldt_data_validita

string   ls_cod_piano_campionamento

ll_riga = dw_piani_ricerca.getrow()

if ll_riga < 1 then return

if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
//	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("cod_piano_campionamento")
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), "cod_piano_campionamento", dw_piani_ricerca.getitemstring( ll_riga, "cod_piano_campionamento"))
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), "data_validita", dw_piani_ricerca.getitemdatetime( ll_riga, "data_validita"))
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), "prog_piano_campionamento", dw_piani_ricerca.getitemnumber( ll_riga, "prog_piano_campionamento"))

	ls_cod_piano_campionamento = dw_piani_ricerca.getitemstring( ll_riga, "cod_piano_campionamento")
	
	ldt_data_validita = dw_piani_ricerca.getitemdatetime( ll_riga, "data_validita")
	
	ll_prog_piano = dw_piani_ricerca.getitemnumber( ll_riga, "prog_piano_campionamento")
	
	f_PO_LoadDDDW_DW(s_cs_xx.parametri.parametro_uo_dw_2, &
	                 "cod_prodotto", &
						  sqlca,&
                    "anag_prodotti", &
						  "cod_prodotto", &
						  "des_prodotto",&
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( select cod_prodotto from det_piani_campionamento where cod_piano_campionamento = '" + ls_cod_piano_campionamento + "' and data_validita = '" + string( ldt_data_validita, "yyyymmdd") + "' and prog_piano_campionamento = " + string(ll_prog_piano) + " )")	
	
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
else
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn("prog_piano_campionamento")
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), "prog_piano_campionamento", dw_piani_ricerca.getitemnumber( ll_riga, "prog_piano_campionamento"))
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn("cod_piano_campionamento")
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), "cod_piano_campionamento", dw_piani_ricerca.getitemstring( ll_riga, "cod_piano_campionamento"))	
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn("data_validita")
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), "data_validita", dw_piani_ricerca.getitemdatetime( ll_riga, "data_validita"))	
	s_cs_xx.parametri.parametro_uo_dw_search.triggerevent(itemchanged!)
end if	

//
//s_cs_xx.parametri.parametro_s_1 = dw_piani_ricerca.getitemstring( ll_riga, "cod_piano_campionamento")
//
//s_cs_xx.parametri.parametro_data_1 = dw_piani_ricerca.getitemdatetime( ll_riga, )
//
//s_cs_xx.parametri.parametro_ul_1 = dw_piani_ricerca.getitemnumber( ll_riga, )

parent.hide()
end event

type dw_piani_ricerca from uo_cs_xx_dw within w_piani_campionamento_ricerca
integer x = 23
integer y = 120
integer width = 2469
integer height = 1040
integer taborder = 50
string dataobject = "d_piani_campionamento_ricerca"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;call super::doubleclicked;if row > 0 then
	if i_extendmode then
		cb_ok.postevent(clicked!)
	end if
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long     ll_errore

datetime ldd_oggi

string   ls_descrizione

ldd_oggi = datetime(today())

ls_descrizione = parent.sle_descrizione.text

if not isnull(ls_descrizione) and ls_descrizione <> "" then
	ls_descrizione = "%" + ls_descrizione + "%"
else
	ls_descrizione = "%"
end if

ll_errore = retrieve( s_cs_xx.cod_azienda, ldd_oggi, ls_descrizione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event ue_key;call super::ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = this.getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < this.rowcount() then
			il_row = this.getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = this.getrow() - 1
		end if
END CHOOSE

end event

