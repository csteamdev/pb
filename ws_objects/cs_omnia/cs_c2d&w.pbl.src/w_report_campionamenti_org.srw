﻿$PBExportHeader$w_report_campionamenti_org.srw
$PBExportComments$report analisi organolettiche
forward
global type w_report_campionamenti_org from w_cs_xx_principale
end type
type dw_report_campionamenti from uo_cs_xx_dw within w_report_campionamenti_org
end type
end forward

global type w_report_campionamenti_org from w_cs_xx_principale
integer width = 3918
integer height = 2272
string title = "Report Campionamenti"
dw_report_campionamenti dw_report_campionamenti
end type
global w_report_campionamenti_org w_report_campionamenti_org

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();long      ll_riga, ll_anno, ll_numero, ll_prog_stock

string    ls_cod_prodotto, ls_cod_fornitore, ls_rag_soc_fornitore, ls_des_prodotto, ls_cod_cat_mer, &
          ls_des_cat_mer, ls_oggetto_test, ls_cod_misura, ls_modalita_campionamento, ls_note, ls_flag_conforme, &
			 ls_cod_test, ls_des_test, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_des_deposito, &
			 ls_cod_area_aziendale, ls_marchio, ls_confezione, ls_inviato_a, ls_condizione_prelievo, &
			 ls_des_area, ls_flag_esito, ls_flag_commercializzazione, ls_flag_fornitura, ls_des_fornitura, ls_flag_reso, &
			 ls_des_reso, ls_flag_archiviazione, ls_des_archiviazione, ls_flag_confronto, ls_des_confronto, ls_cod_valore, ls_flag_altro_valore, ls_altro_valore, ls_des_valore, &
			 ls_cod_campione, ls_des_campione, ls_cod_prodotto_definitivo, ls_des_prodotto_definitivo, ls_num_campionamento, ls_note_area

datetime  ldt_data_prelievo, ldt_data_scadenza, ldt_data_stock, ldt_data_consegna, ldt_data_registrazione

dec{4}    ld_quantita, ld_val_medio

ll_anno = s_cs_xx.parametri.parametro_d_1

ll_numero = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

select data_registrazione,
       data_prelievo,
       cod_prodotto,
		 cod_fornitore,
		 cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 prog_stock,
		 data_stock,		 
		 data_scadenza_stock,
		 note,
		 flag_conforme,
		 cod_area_aziendale,
		 marchio,
		 confezione,
		 inviato_a,
		 data_consegna,
		 condizione_prelievo,
		 flag_commercializzazione,
		 flag_fornitura,
		 des_fornitura,
		 flag_reso,
		 des_reso,
		 flag_archiviazione,
		 des_archiviazione,
		 flag_confronto,
		 des_confronto,
		 cod_campione,
		 num_campionamento
into   :ldt_data_registrazione,
       :ldt_data_prelievo,
       :ls_cod_prodotto,
		 :ls_cod_fornitore,
		 :ls_cod_deposito,
		 :ls_cod_ubicazione,
		 :ls_cod_lotto,
		 :ll_prog_stock,
		 :ldt_data_stock,		 		 
		 :ldt_data_scadenza,
		 :ls_note,
		 :ls_flag_conforme,
		 :ls_cod_area_aziendale,
		 :ls_marchio,
		 :ls_confezione,
		 :ls_inviato_a,
		 :ldt_data_consegna,
		 :ls_condizione_prelievo,
		 :ls_flag_commercializzazione,
		 :ls_flag_fornitura,
		 :ls_des_fornitura,
		 :ls_flag_reso,
		 :ls_des_reso,
		 :ls_flag_archiviazione,
		 :ls_des_archiviazione,
		 :ls_flag_confronto,
		 :ls_des_confronto,
		 :ls_cod_campione,
		 :ls_num_campionamento
from   tes_campionamenti
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_reg_campionamenti = :ll_anno and
		 num_reg_campionamenti = :ll_numero;
		 
// ** fornitore
		 
select rag_soc_1
into   :ls_rag_soc_fornitore
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_fornitore = :ls_cod_fornitore;
		 
// ** prodotto		 
		 
select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
		 
select des_prodotto
into   :ls_des_campione
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_campione;
		 
if not isnull(ls_cod_campione) and ls_cod_campione <> "" then
	ls_cod_prodotto_definitivo = ls_cod_campione
	ls_des_prodotto_definitivo = ls_des_campione	
else
	ls_cod_prodotto_definitivo = ls_cod_prodotto
	ls_des_prodotto_definitivo = ls_des_prodotto
end if
		 
// ** area	 
		 
select des_area,
       note
into   :ls_des_area,
	    :ls_note_area
from   tab_aree_aziendali
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_area_aziendale = :ls_cod_area_aziendale;
		 
if isnull(ls_cod_area_aziendale) then ls_des_area = ""


// ** categoria merceologica		 
		 
select cod_cat_mer
into   :ls_cod_cat_mer
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
		 
select des_cat_mer
into   :ls_des_cat_mer
from   tab_cat_mer
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cat_mer = :ls_cod_cat_mer;

// ** deposito

select des_deposito
into   :ls_des_deposito
from   anag_depositi
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""

if isnull(ls_cod_deposito) then ls_cod_deposito = ""

ls_des_deposito = ls_cod_deposito + " " + ls_des_deposito
		 
// ** righe di dettaglio		 
		 
declare cu_campionamenti cursor for
select   cod_test,
			oggetto_test,
         cod_misura,
		   modalita_campionamento,
			flag_esito,
			quantita,
			cod_valore,
			flag_altro_valore,
			altro_valore
from     det_campionamenti
where    cod_azienda = :s_cs_xx.cod_azienda and
         anno_reg_campionamenti = :ll_anno and
		   num_reg_campionamenti = :ll_numero
order by anno_reg_campionamenti ASC, num_reg_campionamenti ASC;

open cu_campionamenti;

if sqlca.sqlcode < 0 then 

	g_mb.messagebox("OMNIA", "Errore durante l'apertura di cu_campionamenti: " + sqlca.sqlerrtext)
	
	close cu_campionamenti;
	
	return -1
	
end if

ll_riga = 0

do while 1 = 1
	
	fetch cu_campionamenti into :ls_cod_test,
	                            :ls_oggetto_test,
	                            :ls_cod_misura,
										 :ls_modalita_campionamento,
										 :ls_flag_esito,
										 :ld_quantita,
										 :ls_cod_valore,
										 :ls_flag_altro_valore,
										 :ls_altro_valore;
										 
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_campionamenti: " + sqlca.sqlerrtext)
		
		close cu_campionamenti;
		
		rollback;
		
		return -1
		
	end if
	
	setnull(ls_des_test)
	
	select des_test
	into   :ls_des_test
	from   tab_test
	where  cod_test = :ls_cod_test;
	
	if isnull(ls_des_test) then ls_des_test = ""
	
	if ls_des_test = "" then ls_des_test = ls_cod_test
	
	ll_riga = dw_report_campionamenti.insertrow(0)
	
	dw_report_campionamenti.setitem( ll_riga, "data_prelievo", ldt_data_prelievo)
	
	dw_report_campionamenti.setitem( ll_riga, "data_creazione", ldt_data_registrazione)

	dw_report_campionamenti.setitem( ll_riga, "categoria", ls_cod_cat_mer + "   " + ls_des_cat_mer)
	
	dw_report_campionamenti.setitem( ll_riga, "prodotto", ls_cod_prodotto_definitivo + "   " + ls_des_prodotto_definitivo)

	dw_report_campionamenti.setitem( ll_riga, "deposito", ls_des_deposito)

	dw_report_campionamenti.setitem( ll_riga, "ubicazione", ls_cod_ubicazione)

	dw_report_campionamenti.setitem( ll_riga, "lotto", ls_cod_lotto)

	dw_report_campionamenti.setitem( ll_riga, "data_stock", ldt_data_stock)

	dw_report_campionamenti.setitem( ll_riga, "prog_stock", ll_prog_stock)
	
	dw_report_campionamenti.setitem( ll_riga, "scadenza_stock", ldt_data_scadenza)	
	
	dw_report_campionamenti.setitem( ll_riga, "fornitore", ls_cod_fornitore + "   " + ls_rag_soc_fornitore)
	
	dw_report_campionamenti.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
	
	dw_report_campionamenti.setitem( ll_riga, "parametri", ls_des_test)

	dw_report_campionamenti.setitem( ll_riga, "um", ls_cod_misura)
	
	dw_report_campionamenti.setitem( ll_riga, "metodi", ls_modalita_campionamento)
	
	dw_report_campionamenti.setitem( ll_riga, "cod_area_aziendale", ls_cod_area_aziendale)
	
	dw_report_campionamenti.setitem( ll_riga, "marchio", ls_marchio)
	
	dw_report_campionamenti.setitem( ll_riga, "confezione", ls_confezione)
	
	dw_report_campionamenti.setitem( ll_riga, "inviato_a", ls_inviato_a)
	
	dw_report_campionamenti.setitem( ll_riga, "data_consegna", ldt_data_consegna)
	
	dw_report_campionamenti.setitem( ll_riga, "condizione_prelievo", ls_condizione_prelievo)
	
	dw_report_campionamenti.setitem( ll_riga, "des_area_aziendale", ls_des_area)
	
	dw_report_campionamenti.setitem( ll_riga, "note_area", ls_note_area)
	
	dw_report_campionamenti.setitem( ll_riga, "flag_commercializzazione", ls_flag_commercializzazione)	
	
	dw_report_campionamenti.setitem( ll_riga, "flag_fornitura", ls_flag_fornitura)	
	
	dw_report_campionamenti.setitem( ll_riga, "des_fornitura", ls_des_fornitura)
	
	dw_report_campionamenti.setitem( ll_riga, "flag_reso", ls_flag_reso)
	
	dw_report_campionamenti.setitem( ll_riga, "des_reso", ls_des_reso)
	
	dw_report_campionamenti.setitem( ll_riga, "flag_archiviazione", ls_flag_archiviazione)
	
	dw_report_campionamenti.setitem( ll_riga, "des_archiviazione", ls_des_archiviazione)
	
	dw_report_campionamenti.setitem( ll_riga, "flag_confronto", ls_flag_confronto)
	
	dw_report_campionamenti.setitem( ll_riga, "des_confronto", ls_des_confronto)	
	
	dw_report_campionamenti.setitem( ll_riga, "num_campionamento", ls_num_campionamento)
	
	if ls_flag_altro_valore <> "S" then
		
		select des_valore
		into   :ls_des_valore
		from   tab_test_valori
		where  cod_test = :ls_cod_test and
		       cod_valore = :ls_cod_valore;
				 
		dw_report_campionamenti.setitem( ll_riga, "valore", ls_des_valore)
	else
		dw_report_campionamenti.setitem( ll_riga, "valore", ls_altro_valore)
	end if

	choose case ls_flag_conforme
			
		case 'S'
			
			ls_flag_conforme = "Conforme"
			
		case 'N'
			
			ls_flag_conforme = "Non Conforme"
			
		case 'I'
			
			ls_flag_conforme = "In attesa di esito"
			
	end choose
	
	dw_report_campionamenti.setitem( ll_riga, "stato", ls_flag_conforme)
	
	dw_report_campionamenti.setitem( ll_riga, "motivo", ls_note)	
	
	choose case ls_flag_conforme
			
		case 'S'
			
			ls_flag_conforme = "Conforme"
			
		case 'N'
			
			ls_flag_conforme = "Non Conforme"
			
		case 'I'
			
			ls_flag_conforme = "In attesa di esito"
			
	end choose
	
	dw_report_campionamenti.setitem( ll_riga, "stato", ls_flag_conforme)
	
	dw_report_campionamenti.setitem( ll_riga, "motivo", ls_note)		
	
	choose case ls_flag_esito
			
		case "S"
			
			dw_report_campionamenti.setitem( ll_riga, "valori", "PRESENTE")		
			
		case "N"
			
			dw_report_campionamenti.setitem( ll_riga, "valori", "ASSENTE")		
			
		case "I"
			
			if ( not isnull(ls_cod_valore) and ls_cod_valore <> "") or ( not isnull(ls_altro_valore) and ls_altro_valore <> "") then
				
				if not isnull(ls_cod_valore) and ls_cod_valore <> "" then
					dw_report_campionamenti.setitem( ll_riga, "valori", ls_des_valore)					
				elseif not isnull(ls_altro_valore) and ls_altro_valore <> "" then
					dw_report_campionamenti.setitem( ll_riga, "valori", ls_altro_valore)
				end if
			
			else
			
				select val_medio
				into   :ld_val_medio
				from   tab_test_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 cod_test = :ls_cod_test;
						 
				if sqlca.sqlcode <> 0 or isnull(ld_val_medio) or ld_val_medio = 0 then
					dw_report_campionamenti.setitem( ll_riga, "valori", string(ld_quantita,"###,###,##0.0000") )		
				else
					if ld_quantita < ld_val_medio then
						dw_report_campionamenti.setitem( ll_riga, "valori", "< " + string(ld_val_medio,"###,###,##0.0000") )		
					else
						dw_report_campionamenti.setitem( ll_riga, "valori", "> " + string(ld_val_medio,"###,###,##0.0000") )		
					end if
				end if					
				
			end if
			
	end choose	
			
	
loop

close cu_campionamenti;

if sqlca.sqlcode < 0 then 

	g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_campionamenti: " + sqlca.sqlerrtext)
	
	close cu_campionamenti;
	
	return -1
	
end if

if ll_riga = 0 then
	
	ll_riga = dw_report_campionamenti.insertrow(0)
	
	dw_report_campionamenti.setitem( ll_riga, "data_prelievo", ldt_data_prelievo)
	
	dw_report_campionamenti.setitem( ll_riga, "categoria", ls_cod_cat_mer + "   " + ls_des_cat_mer)
	
	dw_report_campionamenti.setitem( ll_riga, "prodotto", ls_cod_prodotto_definitivo + "   " + ls_des_prodotto_definitivo)
	
	dw_report_campionamenti.setitem( ll_riga, "deposito", ls_des_deposito)

	dw_report_campionamenti.setitem( ll_riga, "ubicazione", ls_cod_ubicazione)

	dw_report_campionamenti.setitem( ll_riga, "lotto", ls_cod_lotto)

	dw_report_campionamenti.setitem( ll_riga, "data_stock", ldt_data_stock)

	dw_report_campionamenti.setitem( ll_riga, "prog_stock", ll_prog_stock)
	
	dw_report_campionamenti.setitem( ll_riga, "scadenza_stock", ldt_data_scadenza)		
	
	dw_report_campionamenti.setitem( ll_riga, "note_area", ls_note_area)
	
	dw_report_campionamenti.setitem( ll_riga, "fornitore", ls_cod_fornitore + "   " + ls_rag_soc_fornitore)
	
	dw_report_campionamenti.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
	
	
end if

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_report_campionamenti.ib_dw_report = true

set_w_options(c_noresizewin)

save_on_close(c_socnosave)

dw_report_campionamenti.set_dw_options(sqlca, &
									pcca.null_object, &
									c_nonew + &
									c_nomodify + &
									c_nodelete + &
									c_noenablenewonopen + &
									c_noenablemodifyonopen + &
									c_scrollparent + &
									c_disablecc, &
									c_noresizedw + &
									c_nohighlightselected + &
									c_nocursorrowfocusrect + &
									c_nocursorrowpointer)
									
select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_campionamenti.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo in basso a sinistra.",stopsign!)

ls_modify = "logobis.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_campionamenti.modify(ls_modify)

dw_report_campionamenti.object.datawindow.print.preview = 'Yes'
dw_report_campionamenti.object.datawindow.print.preview.rulers = 'No'



end event

on w_report_campionamenti_org.create
int iCurrent
call super::create
this.dw_report_campionamenti=create dw_report_campionamenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_campionamenti
end on

on w_report_campionamenti_org.destroy
call super::destroy
destroy(this.dw_report_campionamenti)
end on

type dw_report_campionamenti from uo_cs_xx_dw within w_report_campionamenti_org
integer x = 23
integer y = 20
integer width = 3845
integer height = 2132
integer taborder = 50
string dataobject = "d_report_campionamenti_det_org"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

