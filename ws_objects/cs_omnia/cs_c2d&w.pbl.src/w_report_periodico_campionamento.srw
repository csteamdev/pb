﻿$PBExportHeader$w_report_periodico_campionamento.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_periodico_campionamento from w_cs_xx_principale
end type
type st_1 from statictext within w_report_periodico_campionamento
end type
type cb_report from commandbutton within w_report_periodico_campionamento
end type
type dw_selezione from uo_cs_xx_dw within w_report_periodico_campionamento
end type
type dw_report from uo_cs_xx_dw within w_report_periodico_campionamento
end type
type dw_folder from u_folder within w_report_periodico_campionamento
end type
end forward

global type w_report_periodico_campionamento from w_cs_xx_principale
integer width = 3744
integer height = 2048
string title = "Report Periodico Campionamento"
boolean resizable = false
st_1 st_1
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_periodico_campionamento w_report_periodico_campionamento

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string    ls_null, ls_cod_fornitore, ls_cod_cliente, ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_valore, ls_altro_valore, &
          ls_cod_cat_mer, ls_flag_controllo, ls_sql, ls_cod_prodotto, ls_test[], ls_vuoto[], ls_visible, ls_text, &
			 ls_cod_test_cu[], ls_note, ls_cod_test, ls_des_valore, ls_des_prodotto, ls_flag_altro_valore, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
			 ls_cod_fornitore_cu, ls_des_test,ls_des_deposito, ls_num_campionamento,ls_cod_area_aziendale, ls_marchio,  &
			 ls_confezione, ls_oggetto_test, ls_cod_misura, ls_modalita_campionamento, ls_flag_esito, ls_rag_soc_fornitore, &
			 ls_des_area,ls_note_piede, ls_cod_area_aziendale_sel, ls_tipo_analisi, ls_cod_campione, ls_des_campione, ls_cod_prodotto_definitivo, ls_des_prodotto_definitivo, ls_des_misura, ls_flag_tipo_analisi

long      ll_anno_registrazione, ll_num_registrazione_da, ll_num_registrazione_a, ll_anno_reg_campionamenti_cu, ll_num_reg_campionamenti_cu, &
          ll_i, ll_riga, ll_prog_stock
          
dec{4}    ld_quantita,ld_val_medio, ld_minima, ld_massima, ld_valore_rif_laboratorio
datetime  ldt_data_registrazione_da, ldt_data_registrazione_a, ldt_data_prelievo_da, ldt_data_prelievo_a, &
          ldt_data_scadenza_stock, ldt_data_registrazione_cu, ldt_data_stock, ldt_data_prelievo, ldt_data_consegna

boolean   lb_testata

st_1.text = "Elaborazione in corso ... attendere!"

setpointer(HourGlass!)

dw_selezione.accepttext()

dw_report.reset()

// loghi del report
string ls_logos, ls_modify

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)


setnull(ls_null)

ll_anno_registrazione = dw_selezione.getitemnumber( 1, "anno_registrazione")

ll_num_registrazione_da = dw_selezione.getitemnumber( 1, "num_registrazione_da")

ll_num_registrazione_a = dw_selezione.getitemnumber( 1, "num_registrazione_a")

ldt_data_registrazione_da = dw_selezione.getitemdatetime( 1, "data_registrazione_da")

ldt_data_registrazione_a = dw_selezione.getitemdatetime( 1, "data_registrazione_a")

ldt_data_prelievo_da = dw_selezione.getitemdatetime( 1, "data_prelievo_da")

ldt_data_prelievo_a = dw_selezione.getitemdatetime( 1, "data_prelievo_a")

ls_cod_fornitore = dw_selezione.getitemstring( 1, "cod_fornitore")

ls_cod_cliente = dw_selezione.getitemstring( 1, "cod_cliente")

ls_cod_prodotto_da = dw_selezione.getitemstring( 1, "cod_prodotto_da")

ls_cod_prodotto_a = dw_selezione.getitemstring( 1, "cod_prodotto_a")

ls_cod_cat_mer = dw_selezione.getitemstring( 1, "cod_cat_mer")

ldt_data_scadenza_stock = dw_selezione.getitemdatetime( 1, "data_scadenza_stock")

ls_flag_controllo = dw_selezione.getitemstring( 1, "flag_controllo")

ls_note_piede = dw_selezione.getitemstring( 1, "note")

ls_cod_area_aziendale_sel = dw_selezione.getitemstring( 1, "cod_area_aziendale")

ls_tipo_analisi = dw_selezione.getitemstring( 1, "flag_tipo_analisi")

ls_sql = " SELECT anno_reg_campionamenti, " + &
         "        num_reg_campionamenti, " + &
			"        data_registrazione, " + &
			"        cod_prodotto, " + &
			"        cod_deposito, " + &
			"        cod_ubicazione, " + &
			"        cod_lotto, " + &
			"        data_stock, " + &
			"        prog_stock, " + &
			"        data_scadenza_stock, " + &			
			"        data_prelievo, " + &
			"        cod_fornitore, " + &
			"        num_campionamento, " + &
			"        cod_area_aziendale, " + &
			"        marchio, " + &
			"        confezione, " + &
			"        data_consegna, " + &
			"        flag_tipo_analisi, " + &			
			"        cod_campione " + &
			" FROM   tes_campionamenti " + &
			" WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			
if ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) then
	
	ls_sql = ls_sql + " AND anno_reg_campionamenti = " + string(ll_anno_registrazione)
	
end if

if ll_num_registrazione_da > 0 and not isnull(ll_num_registrazione_da) then
	
	ls_sql = ls_sql + " AND num_reg_campionamenti >= " + string(ll_num_registrazione_da)
	
end if
		
if ll_num_registrazione_a > 0 and not isnull(ll_num_registrazione_a) then
	
	ls_sql = ls_sql + " AND num_reg_campionamenti <= " + string(ll_num_registrazione_a)
	
end if		
		
if not isnull(ldt_data_registrazione_da) then
	
	ls_sql = ls_sql + " AND data_registrazione >= '" + string(ldt_data_registrazione_da, s_cs_xx.db_funzioni.formato_data) + "' "

end if

if not isnull(ldt_data_registrazione_a) then
	
	ls_sql = ls_sql + " AND data_registrazione <= '" + string(ldt_data_registrazione_a, s_cs_xx.db_funzioni.formato_data) + "' "

end if

if not isnull(ldt_data_prelievo_da) then
	
	ls_sql = ls_sql + " AND data_prelievo >= '" + string(ldt_data_prelievo_da, s_cs_xx.db_funzioni.formato_data) + "' "

end if

if not isnull(ldt_data_prelievo_a) then
	
	ls_sql = ls_sql + " AND data_prelievo <= '" + string(ldt_data_prelievo_a, s_cs_xx.db_funzioni.formato_data) + "' "

end if

if ls_cod_fornitore <> "" and not isnull(ls_cod_fornitore) then
	
	ls_sql = ls_sql + " AND cod_fornitore = '" + ls_cod_fornitore + "' "
	
end if

if ls_cod_cliente <> "" and not isnull(ls_cod_cliente) then
	
	ls_sql = ls_sql + " AND cod_cliente = '" + ls_cod_cliente + "' "
	
end if

if ls_cod_prodotto_da <> "" and not isnull(ls_cod_prodotto_da) then
	
	ls_sql = ls_sql + " AND cod_prodotto >= '" + ls_cod_prodotto_da + "' "
	
end if

if ls_cod_prodotto_a <> "" and not isnull(ls_cod_prodotto_a) then
	
	ls_sql = ls_sql + " AND cod_prodotto <= '" + ls_cod_prodotto_a + "' "
	
end if

if ls_cod_cat_mer <> "" and not isnull(ls_cod_cat_mer) then
	
	ls_sql = ls_sql + " AND cod_prodotto IN ( SELECT cod_prodotto FROM anag_prodotti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_cat_mer = '" + ls_cod_cat_mer + "' ) "

end if

if not isnull(ldt_data_scadenza_stock) then
	
	ls_sql = ls_sql + " AND data_scadenza_stock = " + string(ldt_data_scadenza_stock, s_cs_xx.db_funzioni.formato_data)

end if

if ls_flag_controllo <> "" and not isnull(ls_flag_controllo) then
	
	ls_sql = ls_sql + " AND flag_conforme = '" + ls_flag_controllo + "' "
	
end if

if ls_cod_area_aziendale_sel <> "" and not isnull(ls_cod_area_aziendale_sel) then
		
	ls_sql = ls_sql + " AND cod_area_aziendale = '" + ls_cod_area_aziendale_sel + "' "
	
end if

if ls_tipo_analisi <> "" and not isnull(ls_tipo_analisi) then
		
	ls_sql = ls_sql + " AND flag_tipo_analisi = '" + ls_tipo_analisi + "' "
	
end if

DECLARE cu_campionamenti DYNAMIC CURSOR FOR SQLSA ;

PREPARE SQLSA FROM :ls_sql ;

OPEN DYNAMIC cu_campionamenti ;

if sqlca.sqlcode < 0 then
	
	g_mb.messagebox("OMNIA", "Errore durante l'apertura di cu_campionamenti: " + sqlca.sqlerrtext)

	setpointer(Arrow!)
	
	return -1
	
end if

ll_riga = 0

do while true
	
	FETCH cu_campionamenti INTO :ll_anno_reg_campionamenti_cu,
										 :ll_num_reg_campionamenti_cu,
										 :ldt_data_registrazione_cu,
										 :ls_cod_prodotto,
										 :ls_cod_deposito,
										 :ls_cod_ubicazione,
										 :ls_cod_lotto,
										 :ldt_data_stock,
										 :ll_prog_stock,
										 :ldt_data_scadenza_stock,
										 :ldt_data_prelievo,
										 :ls_cod_fornitore_cu,
										 :ls_num_campionamento,
										 :ls_cod_area_aziendale,
										 :ls_marchio,
										 :ls_confezione,
										 :ldt_data_consegna,
										 :ls_flag_tipo_analisi,
										 :ls_cod_campione;
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
	
		g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_campionamenti: " + sqlca.sqlerrtext)
	
		close cu_campionamenti;
	
		setpointer(Arrow!)
		
		return -1
			
	end if
	
	ll_riga = dw_report.insertrow(0)
	
	setnull(ls_des_prodotto)
	setnull(ls_des_deposito)
	setnull(ls_des_area)
		
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
		
	if isnull(ls_des_prodotto) then ls_des_prodotto = ""
	
	select des_prodotto
	into   :ls_des_campione
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_campione;
			 
	if isnull(ls_des_campione) then ls_des_campione = ""
	
	if not isnull(ls_cod_campione) and ls_cod_campione <> "" then
		ls_cod_prodotto_definitivo = ls_cod_campione
		ls_des_prodotto_definitivo = ls_des_campione
	else
		ls_cod_prodotto_definitivo = ls_cod_prodotto
		ls_des_prodotto_definitivo = ls_des_prodotto
	end if
	
	select des_deposito
	into   :ls_des_deposito
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_deposito = :ls_cod_deposito;
			 
	if isnull(ls_des_deposito) then ls_des_deposito = ""
		
	select des_area
	into   :ls_des_area
	from   tab_aree_aziendali
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_aziendale = :ls_cod_area_aziendale;
			 
	if isnull(ls_des_area) then ls_des_area = ""
	
	select rag_soc_1
	into   :ls_rag_soc_fornitore
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_fornitore = :ls_cod_fornitore_cu;
			 
	if isnull(ls_rag_soc_fornitore) then ls_rag_soc_fornitore = ""			 
		
	dw_report.setitem( ll_riga, "cod_area_aziendale", ls_cod_area_aziendale + " " + ls_des_area)		

	dw_report.setitem( ll_riga, "marchio", ls_marchio)		
	
	dw_report.setitem( ll_riga, "confezione", ls_confezione)		
	
	dw_report.setitem( ll_riga, "num_campionamento", ls_num_campionamento)		
	
	dw_report.setitem( ll_riga, "data_consegna", ldt_data_consegna)		
	
	dw_report.setitem( ll_riga, "note", ls_note_piede)		
	
	dw_report.setitem( ll_riga, "num_registrazione", ll_num_reg_campionamenti_cu)
		
	dw_report.setitem( ll_riga, "data_registrazione", ldt_data_registrazione_cu)
		
	dw_report.setitem( ll_riga, "prodotto", ls_cod_prodotto_definitivo + " " + ls_des_prodotto_definitivo)		
	
	dw_report.setitem( ll_riga, "deposito", ls_cod_deposito + " " + ls_des_deposito)
	
	dw_report.setitem( ll_riga, "ubicazione", ls_cod_ubicazione)
	
	dw_report.setitem( ll_riga, "lotto", ls_cod_lotto)
	
	dw_report.setitem( ll_riga, "data_stock", ldt_data_stock)
	
	dw_report.setitem( ll_riga, "prog_stock", ll_prog_stock)		
	
	dw_report.setitem( ll_riga, "scadenza_stock", ldt_data_scadenza_stock)	
	
	dw_report.setitem( ll_riga, "fornitore", ls_cod_fornitore_cu + " " + ls_rag_soc_fornitore)	
	
	dw_report.setitem( ll_riga, "data_prelievo", ldt_data_prelievo)					
	
	dw_report.setitem( ll_riga, "flag_tipo_riga", 0)
	
	declare cu_det_campionamenti cursor for
	
	select cod_test,
			 oggetto_test,
			 cod_misura,
			 modalita_campionamento,
			 flag_esito,
			 quantita,
			 cod_valore,
			 flag_altro_valore,
			 altro_valore,
			 valore_rif_laboratorio
	from   det_campionamenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_reg_campionamenti = :ll_anno_reg_campionamenti_cu and
			 num_reg_campionamenti = :ll_num_reg_campionamenti_cu
	order by cod_test;
	
	open cu_det_campionamenti;
	
	do while true
		
		fetch cu_det_campionamenti into :ls_cod_test,
											 	  :ls_oggetto_test,
											     :ls_cod_misura,
											     :ls_modalita_campionamento,
											     :ls_flag_esito,
											     :ld_quantita,
												  :ls_cod_valore,
												  :ls_flag_altro_valore,
												  :ls_altro_valore,
												  :ld_valore_rif_laboratorio;
		
		if sqlca.sqlcode = 100 then exit				

		if sqlca.sqlcode < 0 then
		
			g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_campionamenti: " + sqlca.sqlerrtext)
		
			close cu_campionamenti;
		
			setpointer(Arrow!)
			
			return -1
				
		end if
	
		ll_riga = dw_report.insertrow(0)
		
		select des_misura
		into   :ls_des_misura
		from   tab_misure
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_misura = :ls_cod_misura;

		dw_report.setitem( ll_riga, "flag_tipo_riga", 1)
		
		dw_report.setitem( ll_riga, "note", ls_note_piede)
		
		dw_report.setitem( ll_riga, "campo_3", ls_des_misura)
		
		select des_test
		into   :ls_des_test
		from   tab_test
		where  cod_test = :ls_cod_test;
		
		if isnull(ls_des_test) then ls_des_test = ""
		
		if ls_des_test = "" then ls_des_test = ls_cod_test
	
		dw_report.setitem( ll_riga, "campo1", ls_cod_test + " " + ls_des_test)		
		

		choose case ls_flag_esito
				
			case "S"
				
				dw_report.setitem( ll_riga, "campo2", "PRESENTE")		
				
			case "N"
				
				dw_report.setitem( ll_riga, "campo2", "ASSENTE")		
				
			case "I"
				
				
				if ls_flag_altro_valore = "S" then
		
					dw_report.setitem( ll_riga, "campo2", ls_altro_valore)		
		
				else
				
				
				
					choose case ls_flag_tipo_analisi
							
						case "M"
									
							select val_medio,
									 tol_minima,
									 tol_massima
							into   :ld_val_medio,
									 :ld_minima,
									 :ld_massima
							from   tab_test_prodotti
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_prodotto and
									 cod_test = :ls_cod_test;
									
							if not isnull(ld_minima) and not isnull(ld_massima) and ld_massima > 0 then
								
								if ld_quantita = 0 and ld_valore_rif_laboratorio <> 0 then
									dw_report.setitem( ll_riga, "campo2", "< " + string(ld_valore_rif_laboratorio,"###,###,##0"))
								else				
									if (ld_quantita < ld_minima) or (ld_quantita > ld_massima) then
										if ld_valore_rif_laboratorio <> 0 then
											dw_report.setitem( ll_riga, "campo2", "> " + string(ld_valore_rif_laboratorio,"###,###,##0") + "*" )
										else
											dw_report.setitem( ll_riga, "campo2", string(ld_quantita,"###,###,##0") + " *" )
										end if
									else
										dw_report.setitem( ll_riga, "campo2", string(ld_quantita,"###,###,##0"))
									end if
								end if
								
							elseif not isnull(ld_val_medio) and ld_val_medio > 0 then
								
								if ld_quantita < ld_val_medio then
									dw_report.setitem( ll_riga, "campo2", "< " + string(ld_val_medio,"###,###,##0") )		
								elseif ld_quantita = ld_val_medio then
									dw_report.setitem( ll_riga, "campo2", string(ld_val_medio,"###,###,##0") )							
								elseif ld_quantita > ld_val_medio then
									dw_report.setitem( ll_riga, "campo2", "> " + string(ld_val_medio,"###,###,##0") + " *" ) 
								end if								
								
							else
								
								dw_report.setitem( ll_riga, "campo2", string(ld_quantita,"###,###,##0") )
								
							end if								
							
				
						case "O"
							
							
							if ls_flag_altro_valore <> "S" then
								
								select des_valore
								into   :ls_des_valore
								from   tab_test_valori
								where  cod_test = :ls_cod_test and
										 cod_valore = :ls_cod_valore;
										 
							end if				
											
							if ( not isnull(ls_cod_valore) and ls_cod_valore <> "") or ( not isnull(ls_altro_valore) and ls_altro_valore <> "") then
							
								if not isnull(ls_cod_valore) and ls_cod_valore <> "" then
									dw_report.setitem( ll_riga, "campo2", ls_des_valore)					
								elseif not isnull(ls_altro_valore) and ls_altro_valore <> "" then
									dw_report.setitem( ll_riga, "campo2", ls_altro_valore)
								end if
						
							else
						
								select val_medio
								into   :ld_val_medio
								from   tab_test_prodotti
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 cod_prodotto = :ls_cod_prodotto and
										 cod_test = :ls_cod_test;
									 
								if sqlca.sqlcode <> 0 or isnull(ld_val_medio) or ld_val_medio = 0 then
									if ld_quantita = 0 then
										dw_report.setitem( ll_riga, "campo2", "")
									else
										dw_report.setitem( ll_riga, "campo2", string(ld_quantita,"###,###,##0.0000") )		
									end if
								else
									if ld_quantita < ld_val_medio then
										dw_report.setitem( ll_riga, "campo2", "< " + string(ld_val_medio,"###,###,##0.0000") )		
									else
										dw_report.setitem( ll_riga, "campo2", "> " + string(ld_val_medio,"###,###,##0.0000") )		
									end if
								end if									
							end if										
					
					end choose

//				select val_medio
//				into   :ld_val_medio
//				from   tab_test_prodotti
//				where  cod_azienda = :s_cs_xx.cod_azienda and
//						 cod_prodotto = :ls_cod_prodotto and
//						 cod_test = :ls_cod_test;
//						 
//				if sqlca.sqlcode <> 0 or isnull(ld_val_medio) or ld_val_medio = 0 then
//					dw_report.setitem( ll_riga, "campo2", string(ld_quantita,"###,###,##0.0000") )		
//				else
//					if ld_quantita < ld_val_medio then
//						dw_report.setitem( ll_riga, "campo2", "< " + string(ld_val_medio,"###,###,##0.0000") )		
//					else
//						dw_report.setitem( ll_riga, "campo2", "> " + string(ld_val_medio,"###,###,##0.0000") )		
//					end if
//				end if		

			end if
				
		end choose	

	loop
	
	close cu_det_campionamenti;	
	
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_det_campionamenti: " + sqlca.sqlerrtext)
	
		setpointer(Arrow!)
		
		return -1
		
	end if	
	
loop

CLOSE cu_campionamenti ;	

if sqlca.sqlcode < 0 then
	
	g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_campionamenti: " + sqlca.sqlerrtext)

	setpointer(Arrow!)
	
	return -1
	
end if

dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview = 'Yes'

dw_folder.fu_selecttab(2)

st_1.text = ""

dw_report.change_dw_current()

setpointer(Arrow!)

rollback;

return 0


end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report.modify(ls_modify)

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)



end event

on w_report_periodico_campionamento.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_periodico_campionamento.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
                  "cod_area_aziendale", &
						sqlca, &
						"tab_aree_aziendali", &
						"cod_area_aziendale", &
						"des_area", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")		
						
f_PO_LoadDDDW_DW( dw_selezione, &
                  "cod_cat_mer", &
						sqlca, &
					   "tab_cat_mer", &
						"cod_cat_mer", &
						"des_cat_mer", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")							
end event

type st_1 from statictext within w_report_periodico_campionamento
integer x = 41
integer y = 1528
integer width = 2254
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_report_periodico_campionamento
integer x = 2309
integer y = 1528
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;wf_report()
end event

type dw_selezione from uo_cs_xx_dw within w_report_periodico_campionamento
integer x = 41
integer y = 132
integer width = 2661
integer height = 1396
integer taborder = 20
string dataobject = "d_sel_report_periodico_campionamento"
boolean border = false
end type

event itemchanged;call super::itemchanged;//string ls_cod_divisione, ls_cod_area_aziendale, ls_cod_cat_mer
//
//dw_selezione.accepttext()
//
//choose case i_colname
//		
//	case "cod_prodotto_da"
//		
//		if isnull(i_coltext) then
//			f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_mer",sqlca,&
//						  "tab_cat_mer","cod_cat_mer","des_cat_mer", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
//		else
//			
//			select cod_cat_mer
//			into   :ls_cod_cat_mer
//			from   anag_prodotti
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//			       cod_prodotto = :i_coltext;
//			
//			f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_mer",sqlca,&
//						  "tab_cat_mer","cod_cat_mer","des_cat_mer", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer = '" + ls_cod_cat_mer + "'")					  					  
//		end if				  
//		
//	
//end choose
//
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_periodico_campionamento
integer x = 23
integer y = 128
integer width = 3680
integer height = 1808
integer taborder = 10
string dataobject = "d_report_campionamenti_periodico"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_periodico_campionamento
integer width = 3707
integer height = 1932
integer taborder = 40
boolean border = false
end type

