﻿$PBExportHeader$w_report_registro_analisi.srw
$PBExportComments$Finestra Report registro analisi effettuate
forward
global type w_report_registro_analisi from w_cs_xx_principale
end type
type st_1 from statictext within w_report_registro_analisi
end type
type cb_report from commandbutton within w_report_registro_analisi
end type
type dw_selezione from uo_cs_xx_dw within w_report_registro_analisi
end type
type dw_report from uo_cs_xx_dw within w_report_registro_analisi
end type
type dw_folder from u_folder within w_report_registro_analisi
end type
end forward

global type w_report_registro_analisi from w_cs_xx_principale
integer width = 3744
integer height = 2048
string title = "Report Registro Analisi Effettuate"
boolean resizable = false
st_1 st_1
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_registro_analisi w_report_registro_analisi

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string    ls_null, ls_cod_fornitore, ls_cod_prodotto_da, ls_cod_prodotto_a, ls_flag_laboratorio, ls_cod_area_aziendale, ls_sql, &
          ls_cod_prodotto, ls_cod_fornitore_cu, ls_num_campionamento, ls_cod_area_aziendale_cu, ls_des_prodotto, ls_des_area, &
			 ls_flag_tipo_analisi_cu, ls_flag_tipo_laboratorio_cu, ls_des_fornitore, ls_cod_campione, ls_des_campione, ls_cod_prodotto_definitivo, ls_des_prodotto_definitivo, ls_flag_tipo_analisi, &
			 ls_num_campionamento_cu

long      ll_anno_registrazione, ll_num_registrazione_da, ll_num_registrazione_a, ll_anno_reg_campionamenti_cu, ll_num_reg_campionamenti_cu, &
          ll_i, ll_riga, ll_prog_stock, ll_count
          
dec{4}    ld_quantita,ld_val_medio
datetime  ldt_data_registrazione_da, ldt_data_registrazione_a, ldt_data_prelievo_da, ldt_data_prelievo_a, &
          ldt_data_scadenza_stock, ldt_data_registrazione_cu, ldt_data_stock, ldt_data_prelievo, ldt_data_consegna

boolean   lb_testata

st_1.text = "Elaborazione in corso ... attendere!"

setpointer(HourGlass!)

dw_selezione.accepttext()

dw_report.reset()

// loghi del report
string ls_logos, ls_modify, ls_appo

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOW';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)

setnull(ls_null)

ldt_data_registrazione_da = dw_selezione.getitemdatetime( 1, "data_registrazione_da")

ldt_data_registrazione_a = dw_selezione.getitemdatetime( 1, "data_registrazione_a")

ls_appo = "Periodo dal " + string(ldt_data_registrazione_da, "dd/mm/yyyy") + " al " + string(ldt_data_registrazione_a, "dd/mm/yyyy")

ls_modify = "t_periodo.text='" + ls_appo + "'"

dw_report.modify(ls_modify)

ls_cod_fornitore = dw_selezione.getitemstring( 1, "cod_fornitore")
ls_cod_prodotto_da = dw_selezione.getitemstring( 1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getitemstring( 1, "cod_prodotto_a")
ls_flag_laboratorio = dw_selezione.getitemstring( 1, "flag_laboratorio")
ls_cod_area_aziendale = dw_selezione.getitemstring( 1, "cod_area_aziendale")
ls_flag_tipo_analisi = dw_selezione.getitemstring( 1, "flag_tipo_analisi")

ls_sql = " SELECT anno_reg_campionamenti, " + &
         "        num_reg_campionamenti, " + &
			"        num_campionamento, " + &
			"        data_registrazione, " + &
			"        cod_prodotto, " + &
			"        cod_fornitore, " + &
			"        num_campionamento, " + &
			"        cod_area_aziendale, " + &
			"        flag_tipo_analisi, " + &
			"        flag_tipo_laboratorio, " + &
			"        cod_campione      " + &
			" FROM   tes_campionamenti " + &
			" WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			
if not isnull(ldt_data_registrazione_da) then
	ls_sql = ls_sql + " AND data_registrazione >= '" + string(ldt_data_registrazione_da, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_registrazione_a) then
	ls_sql = ls_sql + " AND data_registrazione <= '" + string(ldt_data_registrazione_a, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if ls_cod_fornitore <> "" and not isnull(ls_cod_fornitore) then
	ls_sql = ls_sql + " AND cod_fornitore = '" + ls_cod_fornitore + "' "
end if

if ls_cod_prodotto_da <> "" and not isnull(ls_cod_prodotto_da) then
	ls_sql = ls_sql + " AND cod_prodotto >= '" + ls_cod_prodotto_da + "' "
end if

if ls_cod_prodotto_a <> "" and not isnull(ls_cod_prodotto_a) then
	ls_sql = ls_sql + " AND cod_prodotto <= '" + ls_cod_prodotto_a + "' "
end if

if ls_cod_area_aziendale <> "" and not isnull(ls_cod_area_aziendale) then
	ls_sql = ls_sql + " AND cod_area_aziendale = '" + ls_cod_area_aziendale + "' "
end if

choose case ls_flag_tipo_analisi
	case "M"
		ls_sql = ls_sql + " AND flag_tipo_analisi = 'M' "
	case "O"
		ls_sql = ls_sql + " AND flag_tipo_analisi = 'O' "
end choose

choose case ls_flag_laboratorio 
	case "I" 	
		ls_sql = ls_sql + " AND flag_tipo_laboratorio = 'S' "
	case "E"
		ls_sql = ls_sql + " AND flag_tipo_laboratorio = 'N' "
end choose

ls_sql = ls_sql + " ORDER BY data_registrazione ASC "

//Donato 10/09/2009: tolto il cursore e usato il datastore dinamico
//DECLARE cu_campionamenti DYNAMIC CURSOR FOR SQLSA ;
//PREPARE SQLSA FROM :ls_sql ;
//OPEN DYNAMIC cu_campionamenti ;
//if sqlca.sqlcode < 0 then
//	g_mb.messagebox("OMNIA", "Errore durante l'apertura di cu_campionamenti: " + sqlca.sqlerrtext
//	setpointer(Arrow!)
//	return -1
//end if
//----------------------------------------------------------------------------
string ls_sintassi, ls_errore
long ll_righe
datastore lds_dati

ls_sintassi = sqlca.syntaxfromsql( ls_sql, "style(type=grid)", ls_errore)
if Len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
	setpointer(Arrow!)
	return -1
end if

lds_dati = create datastore

lds_dati.Create( ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	destroy lds_dati;
	setpointer(Arrow!)
	return -1
end if

lds_dati.settransobject(sqlca)

ll_righe = lds_dati.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe del datastore:" + sqlca.sqlerrtext)
	destroy lds_dati;
	setpointer(Arrow!)
	return -1
end if

ll_riga = 0
ll_count = 0

//do while true
for ll_i = 1 to ll_righe
//	FETCH cu_campionamenti INTO :ll_anno_reg_campionamenti_cu,
//										 :ll_num_reg_campionamenti_cu,
//										 :ls_num_campionamento_cu,
//										 :ldt_data_registrazione_cu,
//										 :ls_cod_prodotto,
//										 :ls_cod_fornitore_cu,
//										 :ls_num_campionamento,
//										 :ls_cod_area_aziendale_cu,
//										 :ls_flag_tipo_analisi_cu,
//										 :ls_flag_tipo_laboratorio_cu,
//										 :ls_cod_campione;
//										 
//	if sqlca.sqlcode = 100 then exit
	
//	if sqlca.sqlcode < 0 then
//		g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_campionamenti: " + sqlca.sqlerrtext)
//		close cu_campionamenti;
//		setpointer(Arrow!)
//		return -1
//	end if
	
	//recupero variabili
	ll_anno_reg_campionamenti_cu	= lds_dati.getitemnumber(ll_i,1)
	ll_num_reg_campionamenti_cu		= lds_dati.getitemnumber(ll_i,2)
	ls_num_campionamento_cu			= lds_dati.getitemstring(ll_i,3)
	ldt_data_registrazione_cu			= lds_dati.getitemdatetime(ll_i,4)
	ls_cod_prodotto						= lds_dati.getitemstring(ll_i,5)
	ls_cod_fornitore_cu					= lds_dati.getitemstring(ll_i,6)
	ls_num_campionamento				= lds_dati.getitemstring(ll_i,7)
	ls_cod_area_aziendale_cu			= lds_dati.getitemstring(ll_i,8)
	ls_flag_tipo_analisi_cu				= lds_dati.getitemstring(ll_i,9)
	ls_flag_tipo_laboratorio_cu			= lds_dati.getitemstring(ll_i,10)
	ls_cod_campione						= lds_dati.getitemstring(ll_i,11)
	
	ll_count = ll_count + 1
	ll_riga = dw_report.insertrow(0)
	
	st_1.text = "Elaborazione riga "+string(ll_i)+" di "+string(ll_righe)
	
	setnull(ls_des_prodotto)
	setnull(ls_des_area)
		
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
		
	if isnull(ls_des_prodotto) then ls_des_prodotto = ""
	
	select des_prodotto
	into   :ls_des_campione
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_campione;
			 
	if isnull(ls_des_campione) then ls_des_campione = ""
	
	if not isnull(ls_cod_campione) and ls_cod_campione <> "" then
		ls_cod_prodotto_definitivo = ls_cod_campione
		ls_des_prodotto_definitivo = ls_des_campione
	else
		ls_cod_prodotto_definitivo = ls_cod_prodotto
		ls_des_prodotto_definitivo = ls_des_prodotto
	end if
	
	select des_area
	into   :ls_des_area
	from   tab_aree_aziendali
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_aziendale = :ls_cod_area_aziendale_cu;
			 
	if isnull(ls_des_area) then ls_des_area = ""
	
	ls_des_fornitore = ""
	
	select rag_soc_1
	into   :ls_des_fornitore
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_fornitore = :ls_cod_fornitore_cu;
			 
	if isnull(ls_des_fornitore) then ls_des_fornitore = ""
	
	dw_report.setitem( ll_riga, "anno_registrazione", ll_anno_reg_campionamenti_cu)		
	dw_report.setitem( ll_riga, "num_registrazione", ll_num_reg_campionamenti_cu)
	dw_report.setitem( ll_riga, "num_campionamento", ls_num_campionamento_cu)
	dw_report.setitem( ll_riga, "data_registrazione", ldt_data_registrazione_cu)
	dw_report.setitem( ll_riga, "area", ls_des_area)		
	dw_report.setitem( ll_riga, "prodotto", ls_des_prodotto_definitivo)	
	dw_report.setitem( ll_riga, "fornitore", ls_des_fornitore)	
	dw_report.setitem( ll_riga, "tipo_analisi", ls_flag_tipo_analisi_cu)
	dw_report.setitem( ll_riga, "tipo_laboratorio", ls_flag_tipo_laboratorio_cu)	
	
//loop
next

//CLOSE cu_campionamenti ;	
//if sqlca.sqlcode < 0 then
//	g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_campionamenti: " + sqlca.sqlerrtext)
//	setpointer(Arrow!)
//	return -1
//end if

dw_report.setredraw(true)
dw_report.Object.DataWindow.Print.Preview = 'Yes'
dw_folder.fu_selecttab(2)

st_1.text = ""
dw_report.change_dw_current()
setpointer(Arrow!)

rollback;
return 0


end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report.modify(ls_modify)

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)



end event

on w_report_registro_analisi.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_registro_analisi.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
                  "cod_area_aziendale", &
						sqlca, &
                  "tab_aree_aziendali", &
						"cod_area_aziendale", &
						"des_area", &
                  "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type st_1 from statictext within w_report_registro_analisi
integer x = 69
integer y = 740
integer width = 2126
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_report_registro_analisi
integer x = 2313
integer y = 740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;wf_report()
end event

type dw_selezione from uo_cs_xx_dw within w_report_registro_analisi
integer x = 23
integer y = 120
integer width = 2674
integer height = 760
integer taborder = 20
string dataobject = "d_sel_report_registro_analisi"
boolean border = false
end type

event itemchanged;call super::itemchanged;//string ls_cod_divisione, ls_cod_area_aziendale, ls_cod_cat_mer
//
//dw_selezione.accepttext()
//
//choose case i_colname
//		
//	case "cod_prodotto_da"
//		
//		if isnull(i_coltext) then
//			f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_mer",sqlca,&
//						  "tab_cat_mer","cod_cat_mer","des_cat_mer", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
//		else
//			
//			select cod_cat_mer
//			into   :ls_cod_cat_mer
//			from   anag_prodotti
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//			       cod_prodotto = :i_coltext;
//			
//			f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_mer",sqlca,&
//						  "tab_cat_mer","cod_cat_mer","des_cat_mer", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer = '" + ls_cod_cat_mer + "'")					  					  
//		end if				  
//		
//	
//end choose
//
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_registro_analisi
integer x = 23
integer y = 128
integer width = 3680
integer height = 1808
integer taborder = 10
string dataobject = "d_report_registro_analisi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_registro_analisi
integer width = 3707
integer height = 1932
integer taborder = 40
boolean border = false
end type

