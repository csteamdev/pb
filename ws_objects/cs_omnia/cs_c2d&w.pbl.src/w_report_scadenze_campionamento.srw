﻿$PBExportHeader$w_report_scadenze_campionamento.srw
$PBExportComments$Finestra report scadenze analisi campionamento
forward
global type w_report_scadenze_campionamento from w_cs_xx_principale
end type
type st_1 from statictext within w_report_scadenze_campionamento
end type
type cb_report from commandbutton within w_report_scadenze_campionamento
end type
type dw_selezione from uo_cs_xx_dw within w_report_scadenze_campionamento
end type
type dw_report from uo_cs_xx_dw within w_report_scadenze_campionamento
end type
type dw_folder from u_folder within w_report_scadenze_campionamento
end type
end forward

global type w_report_scadenze_campionamento from w_cs_xx_principale
integer width = 3744
integer height = 2048
string title = "Report Scadenze Campionamenti"
boolean resizable = false
st_1 st_1
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_scadenze_campionamento w_report_scadenze_campionamento

forward prototypes
public function integer wf_report ()
public function datetime wf_prossima_scadenza (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza)
public function integer wf_report_2 ()
end prototypes

public function integer wf_report ();boolean   ib_insert_fornitore = false
string    ls_null, ls_cod_fornitore_cu, ls_cod_cliente, ls_cod_prodotto, ls_sql, ls_cod_prodotto_cu, ls_cod_piano_campionamento_cu, &
          ls_cod_fornitore, ls_periodicita_cu, ls_des_fornitore_cu, ls_cod_test_cu, ls_cod_prodotto_prec, ls_des_prodotto_cu, &
			 ls_des_test_cu,ls_cat_mer_da,ls_cod_cat_mer,ls_des_cat_mer,ls_cod_cat_mer_prodotto

datetime  ldt_data_scadenza_da, ldt_data_scadenza_a, ldt_data_validita_cu, ldt_data_scadenza, ldt_max_data

long      ll_riga, ll_n_campionamenti, ll_n_prodotti, ll_anno_reg_campionamento_cu, ll_num_reg_campionamento_cu, ll_prog_piano_campionamento_cu

boolean   lb_testata

decimal   ld_frequenza_cu



declare cu_cat_mer cursor for
select  cod_cat_mer, des_cat_mer
from    tab_cat_mer
where   cod_azienda = :s_Cs_xx.cod_azienda and
        cod_cat_mer like :ls_cat_mer_da;


st_1.text = "Elaborazione in corso ... attendere!"

setpointer(HourGlass!)

dw_selezione.accepttext()

dw_report.reset()

// loghi del report
string ls_logos, ls_modify

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)


setnull(ls_null)

ldt_data_scadenza_da = dw_selezione.getitemdatetime( 1, "data_prelievo_da")

ldt_data_scadenza_a = dw_selezione.getitemdatetime( 1, "data_prelievo_a")

ls_cod_fornitore = dw_selezione.getitemstring( 1, "cod_fornitore")

ls_cod_prodotto = dw_selezione.getitemstring( 1, "cod_prodotto_da")

ls_cat_mer_da = dw_selezione.getitemstring( 1, "cod_cat_mer")

if len(ls_cat_mer_da) < 1 or isnull(ls_cat_mer_da) then
	ls_cat_mer_da = "%"
end if

open cu_cat_mer;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN del cursore su categorie merc.~r~n"+sqlca.sqlerrtext)
	rollback;
	return -1
end if

do while true
	
	fetch cu_cat_mer into :ls_cod_cat_mer, 
								 :ls_des_cat_mer;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in FETCH del cursore su categorie merc.~r~n"+sqlca.sqlerrtext)
		close cu_cat_mer;
		rollback;
		return -1
	end if
	
	if isnull(ls_cod_cat_mer) or len(ls_cod_cat_mer) < 1 then continue

	ll_riga = dw_report.insertrow(0)

	dw_report.setitem( ll_riga, "flag_tipo_riga", 3)

	dw_report.setitem( ll_riga, "campo3", ls_cod_cat_mer + " " + ls_des_cat_mer )		
	
	ls_sql = " SELECT cod_piano_campionamento, " + &
				"        data_validita, " + &
				"        prog_piano_campionamento, " + &			
				"        cod_fornitore, " + &			
				"        periodicita, " + &			
				"        frequenza " + &						
				" FROM   tes_piani_campionamento " + &
				" WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "'  and " + &
				"        flag_tipo_scad_campionamento = 'T' "
				
	if not isnull(ldt_data_scadenza_da) then		
		ls_sql = ls_sql + " AND data_validita >= '" + string(ldt_data_scadenza_da, s_cs_xx.db_funzioni.formato_data) + "' "
	end if
	
	if not isnull(ldt_data_scadenza_a) then
		ls_sql = ls_sql + " AND data_validita <= '" + string(ldt_data_scadenza_a, s_cs_xx.db_funzioni.formato_data) + "' "
	end if
	
	if ls_cod_fornitore <> "" and not isnull(ls_cod_fornitore) then
		ls_sql = ls_sql + " AND cod_fornitore = '" + ls_cod_fornitore + "' "
	end if
	
	ls_sql = ls_sql + " ORDER BY cod_fornitore "
	
	DECLARE cu_tes_piani DYNAMIC CURSOR FOR SQLSA ;
	
	PREPARE SQLSA FROM :ls_sql ;
	
	OPEN DYNAMIC cu_tes_piani ;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore durante l'apertura di cu_tes_piani: " + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_cat_mer;		
		return -1
	end if
	
	ll_riga = 0
	
	do while 1 = 1
		
		FETCH cu_tes_piani INTO :ls_cod_piano_campionamento_cu,
										:ldt_data_validita_cu,
										:ll_prog_piano_campionamento_cu,
										:ls_cod_fornitore_cu,
										:ls_periodicita_cu,
										:ld_frequenza_cu;
																	 
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_tes_piani: " + sqlca.sqlerrtext)
			close cu_tes_piani;
			close cu_cat_mer;		
			setpointer(Arrow!)
			return -1
		end if
	
		// *** controllo in questo punto il prodotto
		
		if ls_cod_prodotto <> "" and ls_cod_prodotto <> "%" and not isnull(ls_cod_prodotto) then
			
			SELECT COUNT(*)
			INTO   :ll_n_prodotti
			FROM   det_piani_campionamento
			WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
					 cod_prodotto = :ls_cod_prodotto AND
					 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
					 data_validita = :ldt_data_validita_cu and
					 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
					 
			if isnull(ll_n_prodotti) then ll_n_prodotti = 0
			
			if ll_n_prodotti = 0 then continue
			
		end if	
		
		
		// *** controllo se ha dei campionamenti.
		
		select count(*)
		into   :ll_n_campionamenti
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
				 data_validita = :ldt_data_validita_cu and
				 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore durante la ricerca di campionamenti: " + sqlca.sqlerrtext)
			close cu_tes_piani;
			close cu_cat_mer;		
			setpointer(Arrow!)
			return -1
		end if
		
		if isnull(ll_n_campionamenti) then ll_n_campionamenti = 0
		
		if ll_n_campionamenti = 0 then
			
			ldt_data_scadenza = ldt_data_validita_cu
			
		else
			
			select MAX(data_registrazione)
			into   :ldt_max_data
			from   tes_campionamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
					 data_validita = :ldt_data_validita_cu and
					 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
					 
			ldt_data_scadenza = wf_prossima_scadenza( ldt_max_data, ls_periodicita_cu, ld_frequenza_cu)				
			
		end if
		
		select rag_soc_1
		into   :ls_des_fornitore_cu
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore_cu;
				 
		if isnull(ls_des_fornitore_cu) then ls_des_fornitore_cu = ""
		
		if isnull(ls_cod_fornitore_cu) then ls_cod_fornitore_cu = ""
		
		if sqlca.sqlcode = 100 then 
			ls_des_fornitore_cu = "Ragione Sociale non trovata in anagrafica"
		end if
		
		ib_insert_fornitore = true
//		ll_riga = dw_report.insertrow(0)
			
//		dw_report.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
//			
//		dw_report.setitem( ll_riga, "fornitore", ls_cod_fornitore_cu + " " + ls_des_fornitore_cu)	
//		
//		dw_report.setitem( ll_riga, "flag_tipo_riga", 0)
//		
		if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then	ls_cod_prodotto = "%"
		
		declare cu_det_piani cursor for
		select cod_test,
				 cod_prodotto
		from   det_piani_campionamento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
				 data_validita = :ldt_data_validita_cu and 
				 prog_piano_campionamento = :ll_prog_piano_campionamento_cu and
				 cod_prodotto like :ls_cod_prodotto
		order by cod_prodotto ASC;	
		
		open cu_det_piani;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore in OPEN di cu_det_piani: " + sqlca.sqlerrtext)
			close cu_tes_piani;
			setpointer(Arrow!)
			return -1
		end if	
		
		ls_cod_prodotto_cu = ""
		
		ls_cod_prodotto_prec = ""
		
		do while 1 = 1		
			
			fetch cu_det_piani into :ls_cod_test_cu,
											:ls_cod_prodotto_cu;
			
			if sqlca.sqlcode = 100 then exit				
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA", "Errore in FETCH di cu_det_piani: " + sqlca.sqlerrtext)
				close cu_tes_piani;
				close cu_cat_mer;
				setpointer(Arrow!)
				return -1
			end if	
			
			select des_prodotto, cod_cat_mer
			into   :ls_des_prodotto_cu, :ls_cod_cat_mer_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_cu;

			// salto il prodotto per  passare al prodotto
			if ls_cod_cat_mer_prodotto <> ls_cod_cat_mer then continue
			
			// *************
			if ib_insert_fornitore then
			
				ll_riga = dw_report.insertrow(0)			
				
				dw_report.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
					
				dw_report.setitem( ll_riga, "fornitore", ls_cod_fornitore_cu + " " + ls_des_fornitore_cu)	
				
				dw_report.setitem( ll_riga, "flag_tipo_riga", 0)
				
				ib_insert_fornitore = false
			
			end if			
			// *************
			if ls_cod_prodotto_cu <> ls_cod_prodotto_prec then
				
				ls_cod_prodotto_prec = ls_cod_prodotto_cu
				
						 
				if isnull(ls_cod_prodotto_cu) then ls_cod_prodotto_cu = ""
				
				if isnull(ls_des_prodotto_cu) then ls_des_prodotto_cu = ""
				
				ll_riga = dw_report.insertrow(0)
	
				dw_report.setitem( ll_riga, "flag_tipo_riga", 1)
	
				dw_report.setitem( ll_riga, "prodotto", ls_cod_prodotto_cu + " " + ls_des_prodotto_cu )		
			
			end if
			
			select des_test
			into   :ls_des_test_cu
			from   tab_test
			where  cod_test = :ls_cod_test_cu;
			
			if isnull(ls_cod_test_cu) then ls_cod_test_cu = ""
			
			if isnull(ls_des_test_cu) then ls_des_test_cu = ""
			
			ll_riga = dw_report.insertrow(0)
	
			dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
	
			dw_report.setitem( ll_riga, "campo1", ls_cod_test_cu + " " + ls_des_test_cu )				
					
		loop
		
		close cu_det_piani;	
		
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_det_piani: " + sqlca.sqlerrtext)
		
			setpointer(Arrow!)
			
			return -1
			
		end if	
		
	loop
	
	CLOSE cu_tes_piani ;	
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_tes_piani: " + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_cat_mer;
		return -1
	end if

loop

close cu_cat_mer;

//dw_report.setsort(" prodotto A, data_scadenza A")

dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview = 'Yes'

dw_folder.fu_selecttab(2)

st_1.text = ""

dw_report.change_dw_current()

setpointer(Arrow!)

rollback;

return 0



end function

public function datetime wf_prossima_scadenza (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza);string 	ls_festivi

datetime ldt_prossima

time 		lt_reg, lt_prossima

long 		ll_gg, ll_freq, ll_giorno, ll_mese, ll_anno, ll_ultimo

boolean  lb_bisestile


if fs_periodicita = 'M' then
	ll_freq = fd_frequenza * 3600
elseif fs_periodicita = 'O' then
	ll_freq = fd_frequenza * 60
elseif fs_periodicita = 'S' then
	ll_freq = fd_frequenza * 7
elseif fs_periodicita = 'E' then
	ll_freq = fd_frequenza
elseif fs_periodicita = 'A' then
	ll_freq = fd_frequenza * 12
else
	ll_freq = fd_frequenza
end if

if fs_periodicita='M' or fs_periodicita='O' then
	lt_prossima=time(fdt_data_registrazione)
	if secondsafter(time("23:59:59"),lt_prossima) < ll_freq then
		lt_prossima=relativetime(lt_prossima,ll_freq -secondsafter(time("23:59:59"),lt_prossima))
		ll_gg=1
	else
		ll_gg=0
		lt_prossima=relativetime(lt_prossima,ll_freq)
	end if
	ll_freq=ll_gg
end if

select flag_includi_festivi
into   :ls_festivi
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_festivi) then
	ls_festivi = "N"
end if

ldt_prossima = fdt_data_registrazione

if (fs_periodicita = 'G' or fs_periodicita = 'S') or ll_gg = 1 then
	ldt_prossima=datetime(relativedate(date(ldt_prossima),ll_freq))
	choose case daynumber(date(ldt_prossima))
		case 1
			if ls_festivi = "N" then
				ldt_prossima=datetime(relativedate(date(ldt_prossima),1))
			end if
		case 7
			if ls_festivi = "N" then
				ldt_prossima=datetime(relativedate(date(ldt_prossima),2))
			end if
	end choose
	ldt_prossima=datetime(date(ldt_prossima),time(fdt_data_registrazione))
end if

if fs_periodicita = 'E' or fs_periodicita = "A" then
	
	ll_giorno = day(date(ldt_prossima))
	
	ll_mese = month(date(ldt_prossima)) + ll_freq
	
	ll_anno = year(date(ldt_prossima))
	
	if ll_mese > 12 and mod(ll_mese,12) > 0 then
		ll_anno = ll_anno + int(ll_mese/12)
		ll_mese = mod(ll_mese,12)
	end if
	
	if ll_mese > 12 and mod(ll_mese,12) = 0 then
		ll_anno = ll_anno + int(ll_mese/12) - 1
		ll_mese = 12
	end if

	choose case ll_mese
		case 4,6,9,11
			ll_ultimo = 30
		case 1,3,5,7,8,10,12
			ll_ultimo = 31
		case 2
			if mod(ll_anno,4) = 0 and mod(ll_anno,100) <> 0 or mod(ll_anno,400) = 0 then
				ll_ultimo = 29
			else
				ll_ultimo = 28
			end if
	end choose
	
	if ll_giorno > ll_ultimo then
		ll_giorno = ll_ultimo
	end if
	
	ldt_prossima = datetime(date(string(ll_giorno) + "/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
	
end if

return ldt_prossima
end function

public function integer wf_report_2 ();boolean   ib_insert_fornitore = false
string    ls_null, ls_cod_fornitore_cu, ls_cod_cliente, ls_cod_prodotto, ls_sql, ls_cod_prodotto_cu, ls_cod_piano_campionamento_cu, &
          ls_cod_fornitore, ls_periodicita_cu, ls_des_fornitore_cu, ls_cod_test_cu, ls_cod_prodotto_prec, ls_des_prodotto_cu, &
			 ls_des_test_cu,ls_cat_mer_da,ls_cod_cat_mer,ls_des_cat_mer,ls_cod_cat_mer_prodotto, ls_cod_piano_campionamento_old

datetime  ldt_data_scadenza_da, ldt_data_scadenza_a, ldt_data_validita_cu, ldt_data_scadenza, &
			 ldt_max_data, ldt_data_validita_old, ldt_data_null

long      ll_i, ll_riga, ll_n_campionamenti, ll_n_prodotti, ll_anno_reg_campionamento_cu, ll_num_reg_campionamento_cu, ll_prog_piano_campionamento_cu, ll_tot, ll_prog_piano_campionamento_old

boolean   lb_testata

decimal   ld_frequenza_cu

datastore lds_campionamenti

declare cu_cat_mer cursor for
select  cod_cat_mer, des_cat_mer
from    tab_cat_mer
where   cod_azienda = :s_Cs_xx.cod_azienda and
        cod_cat_mer like :ls_cat_mer_da;


st_1.text = "Elaborazione in corso ... attendere!"

setpointer(HourGlass!)

dw_selezione.accepttext()

dw_report.reset()

// loghi del report
string ls_logos, ls_modify

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)


setnull(ls_null)

ldt_data_scadenza_da = dw_selezione.getitemdatetime( 1, "data_prelievo_da")
ldt_data_scadenza_a = dw_selezione.getitemdatetime( 1, "data_prelievo_a")
ls_cod_fornitore = dw_selezione.getitemstring( 1, "cod_fornitore")
ls_cod_prodotto = dw_selezione.getitemstring( 1, "cod_prodotto_da")
ls_cat_mer_da = dw_selezione.getitemstring( 1, "cod_cat_mer")

if ls_cod_fornitore = "" then setnull(ls_cod_fornitore)
if ls_cod_prodotto = "" then setnull(ls_cod_prodotto)
if ls_cat_mer_da = "" then setnull(ls_cat_mer_da)

if len(ls_cat_mer_da) < 1 or isnull(ls_cat_mer_da) then
	ls_cat_mer_da = "%"
end if

open cu_cat_mer;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN del cursore su categorie merc.~r~n"+sqlca.sqlerrtext)
	rollback;
	return -1
end if

do while true
	
	fetch cu_cat_mer into :ls_cod_cat_mer, 
								 :ls_des_cat_mer;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in FETCH del cursore su categorie merc.~r~n"+sqlca.sqlerrtext)
		close cu_cat_mer;
		rollback;
		return -1
	end if
	
	if isnull(ls_cod_cat_mer) or len(ls_cod_cat_mer) < 1 then continue

	lds_campionamenti = create datastore
	lds_campionamenti.dataobject = 'd_report_campionamenti_scadenze_new'
	lds_campionamenti.settransobject(sqlca)
	//****claudia modifica perchè prende le scadenze in base alla validità per cui metto la ricerca senza data e
	//poi seleziono quelle all'interno delle scadenze impostate
	//******
	setnull(ldt_data_null)
	ll_tot = lds_campionamenti.retrieve( s_cs_xx.cod_azienda, ls_cod_fornitore, ldt_data_null, ldt_data_null, ls_cod_prodotto)
	
	if ll_tot < 0 then
		g_mb.messagebox("OMNIA", "Errore durante la retrieve del datastore: " + sqlca.sqlerrtext)
		close cu_cat_mer;		
		setpointer(Arrow!)
		return -1
	end if		

	ll_riga = dw_report.insertrow(0)
	dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
	dw_report.setitem( ll_riga, "campo3", ls_cod_cat_mer + " " + ls_des_cat_mer )		
	
	ls_cod_piano_campionamento_old = ""
	setnull(ldt_data_validita_old)
	ll_prog_piano_campionamento_old = 0
	ls_cod_prodotto_prec = ""
	
	for ll_i = 1 to ll_tot
		
		ls_cod_piano_campionamento_cu = lds_campionamenti.getitemstring( ll_i, "cod_piano_campionamento")
		ldt_data_validita_cu = lds_campionamenti.getitemdatetime( ll_i, "data_validita")
		ll_prog_piano_campionamento_cu = lds_campionamenti.getitemnumber( ll_i, "prog_piano_campionamento")
		
		// *** se cambio campionamento devo inserire le testate
		if ls_cod_piano_campionamento_cu <> ls_cod_piano_campionamento_old then
			ib_insert_fornitore = true
			ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
			ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
			ldt_data_validita_old = ldt_data_validita_cu
			ls_cod_prodotto_prec = ""
		else
			if ll_prog_piano_campionamento_cu <> ll_prog_piano_campionamento_old then
				ib_insert_fornitore = true
				ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
				ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
				ldt_data_validita_old = ldt_data_validita_cu
				ls_cod_prodotto_prec = ""
			else
				if ldt_data_validita_old <> ldt_data_validita_cu then
					ib_insert_fornitore = true
					ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
					ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
					ldt_data_validita_old = ldt_data_validita_cu
					ls_cod_prodotto_prec = ""
				end if
			end if				
		end if
		
		ls_cod_fornitore_cu = lds_campionamenti.getitemstring( ll_i, "cod_fornitore")
		ls_periodicita_cu = lds_campionamenti.getitemstring( ll_i, "periodicita")
		ld_frequenza_cu = lds_campionamenti.getitemnumber( ll_i, "frequenza")
		ls_cod_prodotto_cu = lds_campionamenti.getitemstring( ll_i, "cod_prodotto")
		ls_cod_test_cu = lds_campionamenti.getitemstring( ll_i, "cod_test")
																	 
		if ls_cod_prodotto <> "" and ls_cod_prodotto <> "%" and not isnull(ls_cod_prodotto) then
			if ls_cod_prodotto_cu <> ls_cod_prodotto then continue
		end if	
		
		select des_prodotto, 
				 cod_cat_mer
		into   :ls_des_prodotto_cu, 
				 :ls_cod_cat_mer_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_cu;

		// salto il prodotto per  passare al prodotto
		if isnull(ls_cod_cat_mer_prodotto) then ls_cod_cat_mer_prodotto = ""
		if ls_cod_cat_mer_prodotto <> ls_cod_cat_mer then 	continue		
		
		// *** controllo se ha dei campionamenti.
		
		select count(*)
		into   :ll_n_campionamenti
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
				 data_validita = :ldt_data_validita_cu and
				 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore durante la ricerca di campionamenti: " + sqlca.sqlerrtext)
			close cu_cat_mer;		
			setpointer(Arrow!)
			return -1
		end if
		
		if isnull(ll_n_campionamenti) then ll_n_campionamenti = 0
		
		if ll_n_campionamenti = 0 then
			
			ldt_data_scadenza = ldt_data_validita_cu
			
		else
			
			select MAX(data_registrazione)
			into   :ldt_max_data
			from   tes_campionamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
					 data_validita = :ldt_data_validita_cu and
					 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
					 
			ldt_data_scadenza = wf_prossima_scadenza( ldt_max_data, ls_periodicita_cu, ld_frequenza_cu)				
			
		end if
		
		if not isnull(ldt_data_scadenza_a) and ldt_data_scadenza > ldt_data_scadenza_a  then 	continue	
		if not isnull(ldt_data_scadenza_da) and ldt_data_scadenza < ldt_data_scadenza_da then 	continue	
		
		select rag_soc_1
		into   :ls_des_fornitore_cu
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore_cu;
				 
		if isnull(ls_des_fornitore_cu) then ls_des_fornitore_cu = ""
		
		if isnull(ls_cod_fornitore_cu) then ls_cod_fornitore_cu = ""
		
		if sqlca.sqlcode = 100 then 
			ls_des_fornitore_cu = "Ragione Sociale non trovata in anagrafica"
		end if
		
		if ib_insert_fornitore then
			
			ll_riga = dw_report.insertrow(0)			
				
			dw_report.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
				
			dw_report.setitem( ll_riga, "fornitore", ls_cod_fornitore_cu + " " + ls_des_fornitore_cu)	
				
			dw_report.setitem( ll_riga, "flag_tipo_riga", 0)
				
			ib_insert_fornitore = false
	
		end if			

		// *************
		if ls_cod_prodotto_cu <> ls_cod_prodotto_prec then
				
			ls_cod_prodotto_prec = ls_cod_prodotto_cu
										 
			if isnull(ls_cod_prodotto_cu) then ls_cod_prodotto_cu = ""
				
			if isnull(ls_des_prodotto_cu) then ls_des_prodotto_cu = ""
				
			ll_riga = dw_report.insertrow(0)
	
			dw_report.setitem( ll_riga, "flag_tipo_riga", 1)
	
			dw_report.setitem( ll_riga, "prodotto", ls_cod_prodotto_cu + " " + ls_des_prodotto_cu )		
			
		end if
			
		select des_test
		into   :ls_des_test_cu
		from   tab_test
		where  cod_test = :ls_cod_test_cu;
		
		if isnull(ls_cod_test_cu) then ls_cod_test_cu = ""
			
		if isnull(ls_des_test_cu) then ls_des_test_cu = ""
			
		ll_riga = dw_report.insertrow(0)
	
		dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
		dw_report.setitem( ll_riga, "campo1", ls_cod_test_cu + " " + ls_des_test_cu )				
	
	
	next

loop

close cu_cat_mer;

if ls_cat_mer_da = "%" then
	
	lds_campionamenti = create datastore
	lds_campionamenti.dataobject = 'd_report_campionamenti_scadenze_new'
	lds_campionamenti.settransobject(sqlca)

	ll_tot = lds_campionamenti.retrieve( s_cs_xx.cod_azienda, ls_cod_fornitore, ldt_data_scadenza_da, ldt_data_scadenza_a, ls_cod_prodotto)
	
	if ll_tot < 0 then
		g_mb.messagebox("OMNIA", "Errore durante la retrieve del datastore: " + sqlca.sqlerrtext)
		close cu_cat_mer;		
		setpointer(Arrow!)
		return -1
	end if		

	ll_riga = dw_report.insertrow(0)
	dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
	dw_report.setitem( ll_riga, "campo3", "PRODOTTI SENZA CATEGORIA" )		
	
	ls_cod_piano_campionamento_old = ""
	setnull(ldt_data_validita_old)
	ll_prog_piano_campionamento_old = 0
	ls_cod_prodotto_prec = ""
	
	for ll_i = 1 to ll_tot
		
		ls_cod_piano_campionamento_cu = lds_campionamenti.getitemstring( ll_i, "cod_piano_campionamento")
		ldt_data_validita_cu = lds_campionamenti.getitemdatetime( ll_i, "data_validita")
		ll_prog_piano_campionamento_cu = lds_campionamenti.getitemnumber( ll_i, "prog_piano_campionamento")
		
		// *** se cambio campionamento devo inserire le testate
		if ls_cod_piano_campionamento_cu <> ls_cod_piano_campionamento_old then
			ib_insert_fornitore = true
			ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
			ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
			ldt_data_validita_old = ldt_data_validita_cu
			ls_cod_prodotto_prec = ""
		else
			if ll_prog_piano_campionamento_cu <> ll_prog_piano_campionamento_old then
				ib_insert_fornitore = true
				ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
				ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
				ldt_data_validita_old = ldt_data_validita_cu
				ls_cod_prodotto_prec = ""
			else
				if ldt_data_validita_old <> ldt_data_validita_cu then
					ib_insert_fornitore = true
					ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
					ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
					ldt_data_validita_old = ldt_data_validita_cu
					ls_cod_prodotto_prec = ""
				end if
			end if				
		end if
		
		ls_cod_fornitore_cu = lds_campionamenti.getitemstring( ll_i, "cod_fornitore")
		ls_periodicita_cu = lds_campionamenti.getitemstring( ll_i, "periodicita")
		ld_frequenza_cu = lds_campionamenti.getitemnumber( ll_i, "frequenza")
		ls_cod_prodotto_cu = lds_campionamenti.getitemstring( ll_i, "cod_prodotto")
		ls_cod_test_cu = lds_campionamenti.getitemstring( ll_i, "cod_test")
																	 
		if ls_cod_prodotto <> "" and ls_cod_prodotto <> "%" and not isnull(ls_cod_prodotto) then
			if ls_cod_prodotto_cu <> ls_cod_prodotto then continue
		end if	
		
		select des_prodotto, 
				 cod_cat_mer
		into   :ls_des_prodotto_cu, 
				 :ls_cod_cat_mer_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_cu;

		// qui voglio stampare tutti i prodotti con categoria nulla
		if isnull(ls_cod_cat_mer_prodotto)  then ls_cod_cat_mer_prodotto = ""
		if not isnull(ls_cod_cat_mer_prodotto) and len(ls_cod_cat_mer_prodotto) > 0 then continue
		
		// *** controllo se ha dei campionamenti.
		
		select count(*)
		into   :ll_n_campionamenti
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
				 data_validita = :ldt_data_validita_cu and
				 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore durante la ricerca di campionamenti: " + sqlca.sqlerrtext)
			close cu_cat_mer;		
			setpointer(Arrow!)
			return -1
		end if
		
		if isnull(ll_n_campionamenti) then ll_n_campionamenti = 0
		
		if ll_n_campionamenti = 0 then
			
			ldt_data_scadenza = ldt_data_validita_cu
			
		else
			
			select MAX(data_registrazione)
			into   :ldt_max_data
			from   tes_campionamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
					 data_validita = :ldt_data_validita_cu and
					 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
					 
			ldt_data_scadenza = wf_prossima_scadenza( ldt_max_data, ls_periodicita_cu, ld_frequenza_cu)				
			
		end if
		
		select rag_soc_1
		into   :ls_des_fornitore_cu
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore_cu;
				 
		if isnull(ls_des_fornitore_cu) then ls_des_fornitore_cu = ""
		
		if isnull(ls_cod_fornitore_cu) then ls_cod_fornitore_cu = ""
		
		if sqlca.sqlcode = 100 then 
			ls_des_fornitore_cu = "Ragione Sociale non trovata in anagrafica"
		end if
		
		if ib_insert_fornitore then
			
			ll_riga = dw_report.insertrow(0)			
				
			dw_report.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
				
			dw_report.setitem( ll_riga, "fornitore", ls_cod_fornitore_cu + " " + ls_des_fornitore_cu)	
				
			dw_report.setitem( ll_riga, "flag_tipo_riga", 0)
				
			ib_insert_fornitore = false
	
		end if			

		// *************
		if ls_cod_prodotto_cu <> ls_cod_prodotto_prec then
				
			ls_cod_prodotto_prec = ls_cod_prodotto_cu
										 
			if isnull(ls_cod_prodotto_cu) then ls_cod_prodotto_cu = ""
				
			if isnull(ls_des_prodotto_cu) then ls_des_prodotto_cu = ""
				
			ll_riga = dw_report.insertrow(0)
	
			dw_report.setitem( ll_riga, "flag_tipo_riga", 1)
	
			dw_report.setitem( ll_riga, "prodotto", ls_cod_prodotto_cu + " " + ls_des_prodotto_cu )		
			
		end if
			
		select des_test
		into   :ls_des_test_cu
		from   tab_test
		where  cod_test = :ls_cod_test_cu;
		
		if isnull(ls_cod_test_cu) then ls_cod_test_cu = ""
			
		if isnull(ls_des_test_cu) then ls_des_test_cu = ""
			
		ll_riga = dw_report.insertrow(0)
	
		dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
		dw_report.setitem( ll_riga, "campo1", ls_cod_test_cu + " " + ls_des_test_cu )				
	
	
	next	
	
end if

dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview = 'Yes'

dw_folder.fu_selecttab(2)

st_1.text = ""

dw_report.change_dw_current()

setpointer(Arrow!)

rollback;

return 0



end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

//select parametri_azienda.stringa
//into   :ls_path_logo
//from   parametri_azienda
//where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//       parametri_azienda.flag_parametro = 'S' and &
//       parametri_azienda.cod_parametro = 'LO5';
//
//if sqlca.sqlcode < 0 then messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
//
//ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
//dw_report.modify(ls_modify)


string ls_logos

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_report.modify(ls_modify)

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)



end event

on w_report_scadenze_campionamento.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_scadenze_campionamento.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_mer",sqlca,&
                 "tab_cat_mer","cod_cat_mer","des_cat_mer",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type st_1 from statictext within w_report_scadenze_campionamento
integer x = 41
integer y = 504
integer width = 2254
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_report_scadenze_campionamento
integer x = 2304
integer y = 504
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;wf_report_2()
end event

type dw_selezione from uo_cs_xx_dw within w_report_scadenze_campionamento
integer x = 41
integer y = 132
integer width = 2843
integer height = 356
integer taborder = 20
string dataobject = "d_sel_report_scadenze_campionamento"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_scadenze_campionamento
integer x = 23
integer y = 128
integer width = 3657
integer height = 1788
integer taborder = 10
string dataobject = "d_report_campionamenti_scadenze"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_scadenze_campionamento
integer width = 3707
integer height = 1932
integer taborder = 40
boolean border = false
end type

