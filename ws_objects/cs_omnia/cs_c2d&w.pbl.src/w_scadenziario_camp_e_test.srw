﻿$PBExportHeader$w_scadenziario_camp_e_test.srw
$PBExportComments$Window tab_test_prodotti
forward
global type w_scadenziario_camp_e_test from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_scadenziario_camp_e_test
end type
type hpb_1 from hprogressbar within w_scadenziario_camp_e_test
end type
type st_1 from statictext within w_scadenziario_camp_e_test
end type
type cb_cerca from commandbutton within w_scadenziario_camp_e_test
end type
type cb_refresh_campionamenti from commandbutton within w_scadenziario_camp_e_test
end type
type cb_refresh_test from commandbutton within w_scadenziario_camp_e_test
end type
type cb_reset from commandbutton within w_scadenziario_camp_e_test
end type
type cb_salva_test from commandbutton within w_scadenziario_camp_e_test
end type
type cb_stampa_campionamenti from commandbutton within w_scadenziario_camp_e_test
end type
type cb_stampa_test from commandbutton within w_scadenziario_camp_e_test
end type
type dw_test_report from datawindow within w_scadenziario_camp_e_test
end type
type dw_filtro from u_dw_search within w_scadenziario_camp_e_test
end type
type dw_campionamenti from datawindow within w_scadenziario_camp_e_test
end type
type dw_folder from u_folder within w_scadenziario_camp_e_test
end type
type dw_test from datawindow within w_scadenziario_camp_e_test
end type
end forward

global type w_scadenziario_camp_e_test from w_cs_xx_principale
integer width = 3630
integer height = 2924
string title = "Scadenziario Campionamenti e Test"
boolean resizable = false
cb_1 cb_1
hpb_1 hpb_1
st_1 st_1
cb_cerca cb_cerca
cb_refresh_campionamenti cb_refresh_campionamenti
cb_refresh_test cb_refresh_test
cb_reset cb_reset
cb_salva_test cb_salva_test
cb_stampa_campionamenti cb_stampa_campionamenti
cb_stampa_test cb_stampa_test
dw_test_report dw_test_report
dw_filtro dw_filtro
dw_campionamenti dw_campionamenti
dw_folder dw_folder
dw_test dw_test
end type
global w_scadenziario_camp_e_test w_scadenziario_camp_e_test

type variables
//datastore ids_prodotti, ids_test
//treeviewitem tvi_campo
//long         il_handle
//string is_cod_prodotto, is_cod_test, is_tipo
//datetime idt_validita
//boolean ib_delete = false
//boolean ib_new = false
//boolean ib_modify = false
end variables

forward prototypes
public subroutine wf_elabora_test ()
public function datetime wf_prossima_scadenza (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza)
public subroutine wf_elabora_campionamenti ()
public function integer wf_controlla_tolleranza (string fs_cod_prodotto, string fs_cod_test, datetime fdt_data_validita, decimal fd_valore)
public function datetime wf_prossima_scadenza2 (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza)
public subroutine wf_elabora_campionamenti_2 ()
public subroutine wf_elabora_test_2 ()
public subroutine wf_elabora_test_3 ()
end prototypes

public subroutine wf_elabora_test ();string ls_cod_test, ls_cod_valore, ls_flag_altro_valore, ls_altro_valore, ls_cod_prodotto,ls_flag_tipologia_durata
string ls_cod_piano_campionamento, ls_des_piano_campionamento, ls_des_prodotto, ls_des_test, ls_cod_cat_mer
long ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_prog_riga_campionamenti, ll_prog_piano_campionamento
long ll_CountRowToProcessing, ll_CurRow, ll_valore_durata, ll_CurRow2
double lb_quantita
datetime ldt_data_validita, ldt_data_prelievo, ldt_data_scadenza
datetime ldt_data_prelievo_da_filter, ldt_data_prelievo_a_filter, ldt_null
string ls_cod_fornitore_filter, ls_cod_prodotto_filter, ls_cod_cat_mer_filter

// loghi del report (dw_test_report) -------------------
string ls_logos, ls_modify

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_test_report.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_test_report.modify(ls_modify)
//----------------------------------

setnull(ldt_null)
ldt_data_prelievo_da_filter = dw_filtro.getitemdatetime(1,"data_prelievo_da")
ldt_data_prelievo_a_filter = dw_filtro.getitemdatetime(1,"data_prelievo_a")
ls_cod_fornitore_filter = dw_filtro.getitemstring(1,"cod_fornitore")
ls_cod_prodotto_filter = dw_filtro.getitemstring(1,"cod_prodotto")
ls_cod_cat_mer_filter = dw_filtro.getitemstring(1,"cod_cat_mer")

if ls_cod_fornitore_filter = "" then setnull(ls_cod_fornitore_filter)
if ls_cod_prodotto_filter = "" then setnull(ls_cod_prodotto_filter)
if ls_cod_cat_mer_filter = "" then setnull(ls_cod_cat_mer_filter)


SELECT  
		 count(det_campionamenti.anno_reg_campionamenti)
INTO :ll_CountRowToProcessing
FROM det_campionamenti, tes_campionamenti
WHERE  tes_campionamenti.cod_azienda = det_campionamenti.cod_azienda
	AND tes_campionamenti.anno_reg_campionamenti = det_campionamenti.anno_reg_campionamenti
	AND tes_campionamenti.num_reg_campionamenti = det_campionamenti.num_reg_campionamenti
	AND det_campionamenti.cod_azienda = :s_cs_xx.cod_azienda
	AND (det_campionamenti.cod_valore IS NULL or det_campionamenti.cod_valore='')
	
	AND (det_campionamenti.flag_esito='I')
	AND (det_campionamenti.quantita IS NULL OR det_campionamenti.quantita=0)
	/*
	AND (det_campionamenti.quantita IS NULL OR (det_campionamenti.quantita=0
																		AND det_campionamenti.flag_esito="I"))
	*/
	AND (det_campionamenti.flag_altro_valore='N' OR 
									(det_campionamenti.flag_altro_valore='S' AND 
											(det_campionamenti.altro_valore IS NULL
											 OR det_campionamenti.altro_valore='')
									)
			)
	AND (tes_campionamenti.cod_fornitore = :ls_cod_fornitore_filter OR :ls_cod_fornitore_filter IS NULL)
	AND (tes_campionamenti.cod_prodotto = :ls_cod_prodotto_filter OR :ls_cod_prodotto_filter IS NULL)
;

dw_test.reset()
dw_test_report.reset()

if ll_CountRowToProcessing > 0 then	
else
	return
end if

st_1.text = "Elaborazione Test in scadenza in corso ... attendere!"
hpb_1.maxposition = ll_CountRowToProcessing
hpb_1.setstep = 1
hpb_1.position = 0

setpointer(HourGlass!)

//cursore definito per scovare i test registrati e non ancora con un valore
//successivamente saranno presi in considerazione olo quelli che soddisfano
//il criterio di ricerca eventualmente impostato
DECLARE cu_test CURSOR FOR
SELECT  
		 tes_campionamenti.anno_reg_campionamenti
		,tes_campionamenti.num_reg_campionamenti
		,det_campionamenti.prog_riga_campionamenti
		,tes_campionamenti.cod_piano_campionamento
		,tes_campionamenti.prog_piano_campionamento
		,tes_campionamenti.data_prelievo
		,tes_campionamenti.data_validita
		,det_campionamenti.cod_test
		,det_campionamenti.cod_prodotto
		,det_campionamenti.data_validita
		,det_campionamenti.cod_valore
		,det_campionamenti.quantita
		,det_campionamenti.flag_altro_valore
		,det_campionamenti.altro_valore
FROM det_campionamenti, tes_campionamenti
WHERE  tes_campionamenti.cod_azienda = det_campionamenti.cod_azienda
	AND tes_campionamenti.anno_reg_campionamenti = det_campionamenti.anno_reg_campionamenti
	AND tes_campionamenti.num_reg_campionamenti = det_campionamenti.num_reg_campionamenti
	AND det_campionamenti.cod_azienda = :s_cs_xx.cod_azienda
	AND (det_campionamenti.cod_valore IS NULL or det_campionamenti.cod_valore='')
	
	AND (det_campionamenti.flag_esito='I')
	AND (det_campionamenti.quantita IS NULL OR det_campionamenti.quantita=0)
	/*
	AND (det_campionamenti.quantita IS NULL OR (det_campionamenti.quantita=0
																		AND det_campionamenti.flag_esito="I"))
	*/
	/*
	AND (det_campionamenti.quantita IS NULL OR (det_campionamenti.quantita=0
																		AND det_campionamenti.flag_esito="I"))
	*/
	AND (det_campionamenti.flag_altro_valore='N' OR 
									(det_campionamenti.flag_altro_valore='S' AND 
											(det_campionamenti.altro_valore IS NULL
											 OR det_campionamenti.altro_valore='')
									)
			)
	AND (tes_campionamenti.cod_fornitore = :ls_cod_fornitore_filter OR :ls_cod_fornitore_filter IS NULL)
	AND (tes_campionamenti.cod_prodotto = :ls_cod_prodotto_filter OR :ls_cod_prodotto_filter IS NULL)
;


OPEN cu_test;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore dei test. Dettaglio = " + sqlca.sqlerrtext)
	hpb_1.position = 0
	st_1.text = ""
	setpointer(Arrow!)
	return
end if


DO WHILE TRUE
	FETCH cu_test INTO 
			 :ll_anno_reg_campionamenti
			,:ll_num_reg_campionamenti
			,:ll_prog_riga_campionamenti
			,:ls_cod_piano_campionamento
			,:ll_prog_piano_campionamento
			,:ldt_data_prelievo
			,:ldt_data_validita
			,:ls_cod_test
			,:ls_cod_prodotto
			,:ldt_data_validita
			,:ls_cod_valore
			,:lb_quantita
			,:ls_flag_altro_valore
			,:ls_altro_valore
	;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore dei test. Dettaglio = " + sqlca.sqlerrtext)
		close cu_test;
		rollback;
		hpb_1.position = 0
		st_1.text = ""
		setpointer(Arrow!)
		return
	elseif sqlca.sqlcode = 100 then
		
		exit
	end if	
	
	//prima verifica:
	//se il test in esame ha flag_tipologia_durata posto a "N" saltalo
	//verifica questo nella tab_test_prodotti (fissato cod_azienda,cod_prodotto, cod_test, data_validita)
	setnull(ls_flag_tipologia_durata)
	setnull(ll_valore_durata)
	SELECT
		 tab_test_prodotti.flag_tipologia_durata
		,tab_test_prodotti.valore_durata
	INTO
		 :ls_flag_tipologia_durata
		,:ll_valore_durata
	FROM tab_test_prodotti
	WHERE tab_test_prodotti.cod_azienda = :s_cs_xx.cod_azienda
		AND tab_test_prodotti.cod_prodotto = :ls_cod_prodotto
		AND tab_test_prodotti.cod_test = :ls_cod_test
		AND tab_test_prodotti.data_validita = :ldt_data_validita
	;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select sulla durata dei test. Dettaglio = " + sqlca.sqlerrtext)
		close cu_test;
		rollback;
		hpb_1.position = 0
		st_1.text = ""
		setpointer(Arrow!)
		return
	end if	
	
	//scadenza per ORE(H) GIORNI (G) MESI (M)
	IF (ls_flag_tipologia_durata = "H" OR ls_flag_tipologia_durata = "G" OR ls_flag_tipologia_durata = "M") THEN		
		//eliminare i test che non soddisfano il criterio di ricerca (solo per quanto riguarda la categoria merceologica, il resto è stato già filtrato)
		
		//recupero la descrizione del prodotto e il codice della categoria merceologica
		setnull(ls_des_prodotto)
		SELECT anag_prodotti.des_prodotto, anag_prodotti.cod_cat_mer
		INTO :ls_des_prodotto, :ls_cod_cat_mer
		FROM anag_prodotti
		WHERE anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda
			AND anag_prodotti.cod_prodotto = :ls_cod_prodotto
		;
		
		//verifica se è impostato il filtro per categoria merceologica e se necessario filtra
		if not isnull(ls_cod_cat_mer_filter) then
			//filtro impostato: verifica la condizione
			if ls_cod_cat_mer_filter <> ls_cod_cat_mer_filter then continue
		end if
		
		//continua a processare calcolando la scadenza del test
		//utilizzo la stessa funzione che viene adoperata per il calcolod ella scadenza dei campionamenti
		//a patto di cambiare la lettera di alcuni flag
		if ls_flag_tipologia_durata = "H" then 
			ls_flag_tipologia_durata = "O"
		elseif ls_flag_tipologia_durata="M" then
			ls_flag_tipologia_durata = "E"
		end if			
		ldt_data_scadenza = wf_prossima_scadenza2(ldt_data_prelievo,ls_flag_tipologia_durata,ll_valore_durata)
		
		//filtra includendo nel periodo di scadenza eventualmente anche gli estremi
		if not isnull(ldt_data_prelievo_a_filter) and ldt_data_scadenza > ldt_data_prelievo_a_filter  then continue	
		if not isnull(ldt_data_prelievo_da_filter) and ldt_data_scadenza < ldt_data_prelievo_da_filter then 	continue	
		

		ll_CurRow = dw_test.insertrow(0)
		ll_CurRow2 = dw_test_report.insertrow(0)
		
		dw_test.setitem(ll_CurRow,"anno_reg_campionamenti",ll_anno_reg_campionamenti)
		dw_test.setitem(ll_CurRow,"num_reg_campionamenti",ll_num_reg_campionamenti)
		dw_test.setitem(ll_CurRow,"cod_prodotto",ls_cod_prodotto)
		dw_test.setitem(ll_CurRow,"cod_test",ls_cod_test)
		
		dw_test.setitem(ll_CurRow,"data_scadenza",ldt_data_scadenza)
		dw_test.setitem(ll_CurRow,"data_validita",ldt_data_validita)

		dw_test.setitem(ll_CurRow,"prog_riga_campionamenti",ll_prog_riga_campionamenti)
		dw_test.setitem(ll_CurRow,"cod_piano_campionamento",ls_cod_piano_campionamento)
		//---
		dw_test_report.setitem(ll_CurRow2,"anno_reg_campionamenti",ll_anno_reg_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"num_reg_campionamenti",ll_num_reg_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"cod_prodotto",ls_cod_prodotto)
		dw_test_report.setitem(ll_CurRow2,"cod_test",ls_cod_test)
		
		dw_test_report.setitem(ll_CurRow2,"data_scadenza",ldt_data_scadenza)
		dw_test_report.setitem(ll_CurRow2,"data_validita",ldt_data_validita)

		dw_test_report.setitem(ll_CurRow2,"prog_riga_campionamenti",ll_prog_riga_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"cod_piano_campionamento",ls_cod_piano_campionamento)
		//---

		//recupero des_campionamento
		setnull(ls_des_piano_campionamento)
		SELECT des_piano_campionamento
		INTO :ls_des_piano_campionamento
		FROM tes_piani_campionamento
		WHERE tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda
			AND tes_piani_campionamento.cod_piano_campionamento = :ls_cod_piano_campionamento
			AND tes_piani_campionamento.data_validita = :ldt_data_validita
			AND tes_piani_campionamento.prog_piano_campionamento = :ll_prog_piano_campionamento
		;
		dw_test.setitem(ll_CurRow,"des_piano_campionamento",ls_des_piano_campionamento)
		
		dw_test.setitem(ll_CurRow,"cod_prodotto",ls_cod_prodotto)
		dw_test.setitem(ll_CurRow,"des_prodotto",ls_des_prodotto)
		//---------
		dw_test_report.setitem(ll_CurRow2,"des_piano_campionamento",ls_des_piano_campionamento)
		
		dw_test_report.setitem(ll_CurRow2,"cod_prodotto",ls_cod_prodotto)
		dw_test_report.setitem(ll_CurRow2,"des_prodotto",ls_des_prodotto)
		//--------
	
		//recupero la descrizione del test
		setnull(ls_des_test)
		SELECT tab_test.des_test
		INTO :ls_des_test
		FROM tab_test
		WHERE tab_test.cod_test = :ls_cod_test
		;
		dw_test.setitem(ll_CurRow,"des_test",ls_des_test)
		//------
		dw_test_report.setitem(ll_CurRow2,"des_test",ls_des_test)
		//-----

	END IF
	hpb_1.stepit()
loop

dw_test.SetRowFocusIndicator(Hand!)
dw_test.groupcalc( )
//----
dw_test_report.groupcalc( )
dw_test_report.Object.DataWindow.Print.Preview = 'Yes'
//----

hpb_1.position = 0
st_1.text = ""
setpointer(Arrow!)

close cu_test;

end subroutine

public function datetime wf_prossima_scadenza (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza);string 	ls_festivi

datetime ldt_prossima

time 		lt_reg, lt_prossima, lt_relative_time
date ld_relative_date

long 		ll_gg, ll_freq, ll_giorno, ll_mese, ll_anno, ll_ultimo, ll_seconds_after

boolean  lb_bisestile


if fs_periodicita = 'M' then
//	ll_freq = fd_frequenza * 3600
	ll_freq = fd_frequenza * 60
elseif fs_periodicita = 'O' then
	//ll_freq = fd_frequenza * 60
	ll_freq = fd_frequenza * 3600
elseif fs_periodicita = 'S' then
	ll_freq = fd_frequenza * 7
elseif fs_periodicita = 'E' then
	ll_freq = fd_frequenza
elseif fs_periodicita = 'A' then
	ll_freq = fd_frequenza * 12
else
	ll_freq = fd_frequenza
end if

if fs_periodicita='M' or fs_periodicita='O' then
	lt_prossima=time(fdt_data_registrazione)
//	ll_seconds_after = secondsafter(time("23:59:59"),lt_prossima)
	ll_seconds_after = secondsafter(lt_prossima, time("23:59:59"))
	if ll_seconds_after < ll_freq then
//		lt_relative_time = relativetime(lt_prossima,ll_freq -secondsafter(time("23:59:59"),lt_prossima))
		lt_relative_time = relativetime(lt_prossima,ll_freq -ll_seconds_after)
		lt_prossima=lt_relative_time
		ll_gg=1
	else
		ll_gg=0
		lt_relative_time = relativetime(lt_prossima,ll_freq)
		lt_prossima=lt_relative_time
	end if
	ll_freq=ll_gg
end if

select flag_includi_festivi
into   :ls_festivi
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_festivi) then
	ls_festivi = "N"
end if

ldt_prossima = fdt_data_registrazione

if (fs_periodicita = 'G' or fs_periodicita = 'S') or ll_gg = 1 then
	ld_relative_date = relativedate(date(ldt_prossima),ll_freq)
	ldt_prossima=datetime(ld_relative_date)
	choose case daynumber(date(ldt_prossima))
		case 1
			if ls_festivi = "N" then
				ld_relative_date = relativedate(date(ldt_prossima),1)
				ldt_prossima=datetime(ld_relative_date)
			end if
		case 7
			if ls_festivi = "N" then
				ld_relative_date = relativedate(date(ldt_prossima),2)
				ldt_prossima=datetime(ld_relative_date)
			end if
	end choose	
	ldt_prossima=datetime(date(ldt_prossima),time(fdt_data_registrazione))
end if

if fs_periodicita = 'E' or fs_periodicita = "A" then
	
	ll_giorno = day(date(ldt_prossima))
	
	ll_mese = month(date(ldt_prossima)) + ll_freq
	
	ll_anno = year(date(ldt_prossima))
	
	if ll_mese > 12 and mod(ll_mese,12) > 0 then
		ll_anno = ll_anno + int(ll_mese/12)
		ll_mese = mod(ll_mese,12)
	end if
	
	if ll_mese > 12 and mod(ll_mese,12) = 0 then
		ll_anno = ll_anno + int(ll_mese/12) - 1
		ll_mese = 12
	end if

	choose case ll_mese
		case 4,6,9,11
			ll_ultimo = 30
		case 1,3,5,7,8,10,12
			ll_ultimo = 31
		case 2
			if mod(ll_anno,4) = 0 and mod(ll_anno,100) <> 0 or mod(ll_anno,400) = 0 then
				ll_ultimo = 29
			else
				ll_ultimo = 28
			end if
	end choose
	
	if ll_giorno > ll_ultimo then
		ll_giorno = ll_ultimo
	end if
	
	ldt_prossima = datetime(date(string(ll_giorno) + "/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
	
end if

return ldt_prossima
end function

public subroutine wf_elabora_campionamenti ();boolean   ib_insert_fornitore = false
string    ls_null, ls_cod_fornitore_cu, ls_cod_cliente, ls_cod_prodotto, ls_sql, ls_cod_prodotto_cu, ls_cod_piano_campionamento_cu, &
          ls_cod_fornitore, ls_periodicita_cu, ls_des_fornitore_cu, ls_cod_test_cu, ls_cod_prodotto_prec, ls_des_prodotto_cu, &
			 ls_des_test_cu,ls_cat_mer_da,ls_cod_cat_mer,ls_des_cat_mer,ls_cod_cat_mer_prodotto, ls_cod_piano_campionamento_old

datetime  ldt_data_scadenza_da, ldt_data_scadenza_a, ldt_data_validita_cu, ldt_data_scadenza, &
			 ldt_max_data, ldt_data_validita_old, ldt_data_null

long      ll_i, ll_riga, ll_n_campionamenti, ll_n_prodotti, ll_anno_reg_campionamento_cu, ll_num_reg_campionamento_cu, ll_prog_piano_campionamento_cu, ll_tot, ll_prog_piano_campionamento_old
long ll_counter_pb
boolean   lb_testata

decimal   ld_frequenza_cu

datastore lds_campionamenti

string ls_cat_mer_da2
ls_cat_mer_da2 = dw_filtro.getitemstring( 1, "cod_cat_mer")
if len(ls_cat_mer_da2) < 1 or isnull(ls_cat_mer_da2) then ls_cat_mer_da2 = "%"

select  count(cod_cat_mer)
into :ll_counter_pb
from    tab_cat_mer
where   cod_azienda = :s_Cs_xx.cod_azienda and
        cod_cat_mer like :ls_cat_mer_da2;

declare cu_cat_mer cursor for
select  cod_cat_mer, des_cat_mer
from    tab_cat_mer
where   cod_azienda = :s_Cs_xx.cod_azienda and
        cod_cat_mer like :ls_cat_mer_da;


st_1.text = "Elaborazione Campionamenti in scadenza in corso ... attendere!"
hpb_1.maxposition = ll_counter_pb
hpb_1.setstep = 1
hpb_1.position = 0

setpointer(HourGlass!)

dw_filtro.accepttext()

dw_campionamenti.reset()

// loghi del report
string ls_logos, ls_modify

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_campionamenti.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_campionamenti.modify(ls_modify)

setnull(ls_null)

ldt_data_scadenza_da = dw_filtro.getitemdatetime( 1, "data_prelievo_da")
ldt_data_scadenza_a = dw_filtro.getitemdatetime( 1, "data_prelievo_a")
ls_cod_fornitore = dw_filtro.getitemstring( 1, "cod_fornitore")
ls_cod_prodotto = dw_filtro.getitemstring( 1, "cod_prodotto")
ls_cat_mer_da = dw_filtro.getitemstring( 1, "cod_cat_mer")

if ls_cod_fornitore = "" then setnull(ls_cod_fornitore)
if ls_cod_prodotto = "" then setnull(ls_cod_prodotto)
if ls_cat_mer_da = "" then setnull(ls_cat_mer_da)

if len(ls_cat_mer_da) < 1 or isnull(ls_cat_mer_da) then
	ls_cat_mer_da = "%"
end if

open cu_cat_mer;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN del cursore su categorie merc.~r~n"+sqlca.sqlerrtext)
	rollback;
	hpb_1.position = 0
	st_1.text = ""
	return
end if

do while true	
	
	fetch cu_cat_mer into :ls_cod_cat_mer, 
								 :ls_des_cat_mer;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in FETCH del cursore su categorie merc.~r~n"+sqlca.sqlerrtext)
		close cu_cat_mer;
		hpb_1.position = 0
		setpointer(Arrow!)
		st_1.text = ""
		rollback;
		return
	end if
	
	if isnull(ls_cod_cat_mer) or len(ls_cod_cat_mer) < 1 then continue

	lds_campionamenti = create datastore
	lds_campionamenti.dataobject = 'd_report_campionamenti_scadenze_new'
	lds_campionamenti.settransobject(sqlca)
	//****claudia modifica perchè prende le scadenze in base alla validità per cui metto la ricerca senza data e
	//poi seleziono quelle all'interno delle scadenze impostate
	//******
	setnull(ldt_data_null)
	ll_tot = lds_campionamenti.retrieve( s_cs_xx.cod_azienda, ls_cod_fornitore, ldt_data_null, ldt_data_null, ls_cod_prodotto)
	
	if ll_tot < 0 then
		g_mb.messagebox("OMNIA", "Errore durante la retrieve del datastore: " + sqlca.sqlerrtext)
		hpb_1.position = 0
		close cu_cat_mer;		
		setpointer(Arrow!)
		st_1.text = ""
		return
	end if		

	ll_riga = dw_campionamenti.insertrow(0)
	dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 3)
	dw_campionamenti.setitem( ll_riga, "campo3", ls_cod_cat_mer + " " + ls_des_cat_mer )		
	
	ls_cod_piano_campionamento_old = ""
	setnull(ldt_data_validita_old)
	ll_prog_piano_campionamento_old = 0
	ls_cod_prodotto_prec = ""
	
	for ll_i = 1 to ll_tot
		
		ls_cod_piano_campionamento_cu = lds_campionamenti.getitemstring( ll_i, "cod_piano_campionamento")
		ldt_data_validita_cu = lds_campionamenti.getitemdatetime( ll_i, "data_validita")
		ll_prog_piano_campionamento_cu = lds_campionamenti.getitemnumber( ll_i, "prog_piano_campionamento")
		
		// *** se cambio campionamento devo inserire le testate
		if ls_cod_piano_campionamento_cu <> ls_cod_piano_campionamento_old then
			ib_insert_fornitore = true
			ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
			ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
			ldt_data_validita_old = ldt_data_validita_cu
			ls_cod_prodotto_prec = ""
		else
			if ll_prog_piano_campionamento_cu <> ll_prog_piano_campionamento_old then
				ib_insert_fornitore = true
				ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
				ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
				ldt_data_validita_old = ldt_data_validita_cu
				ls_cod_prodotto_prec = ""
			else
				if ldt_data_validita_old <> ldt_data_validita_cu then
					ib_insert_fornitore = true
					ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
					ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
					ldt_data_validita_old = ldt_data_validita_cu
					ls_cod_prodotto_prec = ""
				end if
			end if				
		end if
		
		ls_cod_fornitore_cu = lds_campionamenti.getitemstring( ll_i, "cod_fornitore")
		ls_periodicita_cu = lds_campionamenti.getitemstring( ll_i, "periodicita")
		ld_frequenza_cu = lds_campionamenti.getitemnumber( ll_i, "frequenza")
		ls_cod_prodotto_cu = lds_campionamenti.getitemstring( ll_i, "cod_prodotto")
		ls_cod_test_cu = lds_campionamenti.getitemstring( ll_i, "cod_test")
																	 
		if ls_cod_prodotto <> "" and ls_cod_prodotto <> "%" and not isnull(ls_cod_prodotto) then
			if ls_cod_prodotto_cu <> ls_cod_prodotto then continue
		end if	
		
		select des_prodotto, 
				 cod_cat_mer
		into   :ls_des_prodotto_cu, 
				 :ls_cod_cat_mer_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_cu;

		// salto il prodotto per  passare al prodotto
		if isnull(ls_cod_cat_mer_prodotto) then ls_cod_cat_mer_prodotto = ""
		if ls_cod_cat_mer_prodotto <> ls_cod_cat_mer then 	continue		
		
		// *** controllo se ha dei campionamenti.
		
		select count(*)
		into   :ll_n_campionamenti
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
				 data_validita = :ldt_data_validita_cu and
				 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore durante la ricerca di campionamenti: " + sqlca.sqlerrtext)
			close cu_cat_mer;		
			hpb_1.position = 0
			setpointer(Arrow!)
			st_1.text = ""
			return
		end if
		
		if isnull(ll_n_campionamenti) then ll_n_campionamenti = 0
		
		if ll_n_campionamenti = 0 then
			ldt_data_scadenza = ldt_data_validita_cu
		else
			select MAX(data_registrazione)
			into   :ldt_max_data
			from   tes_campionamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
					 data_validita = :ldt_data_validita_cu and
					 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
					 
			ldt_data_scadenza = wf_prossima_scadenza( ldt_max_data, ls_periodicita_cu, ld_frequenza_cu)				
		end if
		
		if not isnull(ldt_data_scadenza_a) and ldt_data_scadenza > ldt_data_scadenza_a  then 	continue	
		if not isnull(ldt_data_scadenza_da) and ldt_data_scadenza < ldt_data_scadenza_da then 	continue	
		
		select rag_soc_1
		into   :ls_des_fornitore_cu
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore_cu;
				 
		if isnull(ls_des_fornitore_cu) then ls_des_fornitore_cu = ""
		
		if isnull(ls_cod_fornitore_cu) then ls_cod_fornitore_cu = ""
		
		if sqlca.sqlcode = 100 then 
			ls_des_fornitore_cu = "Ragione Sociale non trovata in anagrafica"
		end if
		
		if ib_insert_fornitore then
			
			ll_riga = dw_campionamenti.insertrow(0)			
//#############################
			dw_campionamenti.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
			dw_campionamenti.setitem( ll_riga, "cod_piano_campionamento", ls_cod_piano_campionamento_cu)
			dw_campionamenti.setitem( ll_riga, "prog_piano_campionamento", ll_prog_piano_campionamento_cu)
			dw_campionamenti.setitem( ll_riga, "data_validita", ldt_data_validita_cu)
				
			dw_campionamenti.setitem( ll_riga, "fornitore", ls_cod_fornitore_cu + " " + ls_des_fornitore_cu)	
				
			dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 0)
				
			ib_insert_fornitore = false
	
		end if			

		// *************
		if ls_cod_prodotto_cu <> ls_cod_prodotto_prec then
				
			ls_cod_prodotto_prec = ls_cod_prodotto_cu
										 
			if isnull(ls_cod_prodotto_cu) then ls_cod_prodotto_cu = ""
				
			if isnull(ls_des_prodotto_cu) then ls_des_prodotto_cu = ""
				
			ll_riga = dw_campionamenti.insertrow(0)
	
			dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 1)
	
			dw_campionamenti.setitem( ll_riga, "prodotto", ls_cod_prodotto_cu + " " + ls_des_prodotto_cu )		
			
		end if
			
		select des_test
		into   :ls_des_test_cu
		from   tab_test
		where  cod_test = :ls_cod_test_cu;
		
		if isnull(ls_cod_test_cu) then ls_cod_test_cu = ""
			
		if isnull(ls_des_test_cu) then ls_des_test_cu = ""
			
		ll_riga = dw_campionamenti.insertrow(0)
	
		dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 2)
		dw_campionamenti.setitem( ll_riga, "campo1", ls_cod_test_cu + " " + ls_des_test_cu )
	next
	hpb_1.stepit( )
loop

close cu_cat_mer;

if ls_cat_mer_da = "%" then
	
	lds_campionamenti = create datastore
	lds_campionamenti.dataobject = 'd_report_campionamenti_scadenze_new'
	lds_campionamenti.settransobject(sqlca)

	ll_tot = lds_campionamenti.retrieve( s_cs_xx.cod_azienda, ls_cod_fornitore, ldt_data_scadenza_da, ldt_data_scadenza_a, ls_cod_prodotto)
	
	if ll_tot < 0 then
		g_mb.messagebox("OMNIA", "Errore durante la retrieve del datastore: " + sqlca.sqlerrtext)
		close cu_cat_mer;		
		hpb_1.position = 0
		setpointer(Arrow!)
		st_1.text = ""
		return
	end if		

	ll_riga = dw_campionamenti.insertrow(0)
	dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 3)
	dw_campionamenti.setitem( ll_riga, "campo3", "PRODOTTI SENZA CATEGORIA" )		
	
	ls_cod_piano_campionamento_old = ""
	setnull(ldt_data_validita_old)
	ll_prog_piano_campionamento_old = 0
	ls_cod_prodotto_prec = ""
	
	for ll_i = 1 to ll_tot
		
		ls_cod_piano_campionamento_cu = lds_campionamenti.getitemstring( ll_i, "cod_piano_campionamento")
		ldt_data_validita_cu = lds_campionamenti.getitemdatetime( ll_i, "data_validita")
		ll_prog_piano_campionamento_cu = lds_campionamenti.getitemnumber( ll_i, "prog_piano_campionamento")
		
		// *** se cambio campionamento devo inserire le testate
		if ls_cod_piano_campionamento_cu <> ls_cod_piano_campionamento_old then
			ib_insert_fornitore = true
			ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
			ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
			ldt_data_validita_old = ldt_data_validita_cu
			ls_cod_prodotto_prec = ""
		else
			if ll_prog_piano_campionamento_cu <> ll_prog_piano_campionamento_old then
				ib_insert_fornitore = true
				ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
				ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
				ldt_data_validita_old = ldt_data_validita_cu
				ls_cod_prodotto_prec = ""
			else
				if ldt_data_validita_old <> ldt_data_validita_cu then
					ib_insert_fornitore = true
					ls_cod_piano_campionamento_old = ls_cod_piano_campionamento_cu
					ll_prog_piano_campionamento_old = ll_prog_piano_campionamento_cu
					ldt_data_validita_old = ldt_data_validita_cu
					ls_cod_prodotto_prec = ""
				end if
			end if				
		end if
		
		ls_cod_fornitore_cu = lds_campionamenti.getitemstring( ll_i, "cod_fornitore")
		ls_periodicita_cu = lds_campionamenti.getitemstring( ll_i, "periodicita")
		ld_frequenza_cu = lds_campionamenti.getitemnumber( ll_i, "frequenza")
		ls_cod_prodotto_cu = lds_campionamenti.getitemstring( ll_i, "cod_prodotto")
		ls_cod_test_cu = lds_campionamenti.getitemstring( ll_i, "cod_test")
																	 
		if ls_cod_prodotto <> "" and ls_cod_prodotto <> "%" and not isnull(ls_cod_prodotto) then
			if ls_cod_prodotto_cu <> ls_cod_prodotto then continue
		end if	
		
		select des_prodotto, 
				 cod_cat_mer
		into   :ls_des_prodotto_cu, 
				 :ls_cod_cat_mer_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_cu;

		// qui voglio stampare tutti i prodotti con categoria nulla
		if isnull(ls_cod_cat_mer_prodotto)  then ls_cod_cat_mer_prodotto = ""
		if not isnull(ls_cod_cat_mer_prodotto) and len(ls_cod_cat_mer_prodotto) > 0 then continue
		
		// *** controllo se ha dei campionamenti.
		
		select count(*)
		into   :ll_n_campionamenti
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
				 data_validita = :ldt_data_validita_cu and
				 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore durante la ricerca di campionamenti: " + sqlca.sqlerrtext)
			close cu_cat_mer;		
			hpb_1.position = 0
			setpointer(Arrow!)
			st_1.text = ""
			return
		end if
		
		if isnull(ll_n_campionamenti) then ll_n_campionamenti = 0
		
		if ll_n_campionamenti = 0 then
			
			ldt_data_scadenza = ldt_data_validita_cu
			
		else
			
			select MAX(data_registrazione)
			into   :ldt_max_data
			from   tes_campionamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_piano_campionamento = :ls_cod_piano_campionamento_cu and
					 data_validita = :ldt_data_validita_cu and
					 prog_piano_campionamento = :ll_prog_piano_campionamento_cu;
					 
			ldt_data_scadenza = wf_prossima_scadenza( ldt_max_data, ls_periodicita_cu, ld_frequenza_cu)				
			
		end if
		
		select rag_soc_1
		into   :ls_des_fornitore_cu
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore_cu;
				 
		if isnull(ls_des_fornitore_cu) then ls_des_fornitore_cu = ""
		
		if isnull(ls_cod_fornitore_cu) then ls_cod_fornitore_cu = ""
		
		if sqlca.sqlcode = 100 then 
			ls_des_fornitore_cu = "Ragione Sociale non trovata in anagrafica"
		end if
		
		if ib_insert_fornitore then
			
			ll_riga = dw_campionamenti.insertrow(0)			
//###########################				
			dw_campionamenti.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
			dw_campionamenti.setitem( ll_riga, "cod_piano_campionamento", ls_cod_piano_campionamento_cu)
			dw_campionamenti.setitem( ll_riga, "prog_piano_campionamento", ll_prog_piano_campionamento_cu)
			dw_campionamenti.setitem( ll_riga, "data_validita", ldt_data_validita_cu)
				
				
			dw_campionamenti.setitem( ll_riga, "fornitore", ls_cod_fornitore_cu + " " + ls_des_fornitore_cu)	
				
			dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 0)
				
			ib_insert_fornitore = false
	
		end if			

		// *************
		if ls_cod_prodotto_cu <> ls_cod_prodotto_prec then
				
			ls_cod_prodotto_prec = ls_cod_prodotto_cu
										 
			if isnull(ls_cod_prodotto_cu) then ls_cod_prodotto_cu = ""
				
			if isnull(ls_des_prodotto_cu) then ls_des_prodotto_cu = ""
				
			ll_riga = dw_campionamenti.insertrow(0)
	
			dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 1)
	
			dw_campionamenti.setitem( ll_riga, "prodotto", ls_cod_prodotto_cu + " " + ls_des_prodotto_cu )		
			
		end if
			
		select des_test
		into   :ls_des_test_cu
		from   tab_test
		where  cod_test = :ls_cod_test_cu;
		
		if isnull(ls_cod_test_cu) then ls_cod_test_cu = ""
			
		if isnull(ls_des_test_cu) then ls_des_test_cu = ""
			
		ll_riga = dw_campionamenti.insertrow(0)
	
		dw_campionamenti.setitem( ll_riga, "flag_tipo_riga", 2)
		dw_campionamenti.setitem( ll_riga, "campo1", ls_cod_test_cu + " " + ls_des_test_cu)
	next	
	
end if

dw_campionamenti.setredraw(true)

dw_campionamenti.Object.DataWindow.Print.Preview = 'Yes'

//dw_folder.fu_selecttab(2)

st_1.text = ""
hpb_1.position = 0

//dw_campionamenti.change_dw_current()

setpointer(Arrow!)

rollback;

return



end subroutine

public function integer wf_controlla_tolleranza (string fs_cod_prodotto, string fs_cod_test, datetime fdt_data_validita, decimal fd_valore);//funzione per determinare se il valore numerico immesso rientra nei limiti della tolleranza
//torna 0 se tutto a posto oppure nessun limite impostato
//torna 1 se il valore è più piccolo dell'estremo inferiore
//torna 2 se  il valore è più grande dell'estremo superiore

decimal ldc_tol_minima, ldc_tol_massima

select
	 tol_minima
	,tol_massima
into 
	 :ldc_tol_minima
	,:ldc_tol_massima
from tab_test_prodotti
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_prodotto = :fs_cod_prodotto  
	and cod_test = :fs_cod_test  
	and data_validita = :fdt_data_validita
		;

if isnull(ldc_tol_minima) or ldc_tol_minima = 0 then
	//forse non è stato impostato l'estremo inferiore
	if isnull(ldc_tol_massima) or ldc_tol_massima = 0 then
		//non è stato impostato neppure l'estremo superiore: non fare nessun controllo		
	else
		//è stato impostato l'estremo superiore
		if fd_valore > ldc_tol_massima then return 2
	end if
else
	//estremo inferiore impostato
	if isnull(ldc_tol_massima) or ldc_tol_massima = 0 then
		//non è stato impostato neppure l'estremo superiore, ma quello inferiore si
		if fd_valore < ldc_tol_minima then return 1
	else
		//è stato impostato l'estremo superiore e l'estremo inferiore
		if fd_valore > ldc_tol_massima then return 2
		if fd_valore < ldc_tol_minima then return 1
	end if
end if

return 0
end function

public function datetime wf_prossima_scadenza2 (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza);string 	ls_festivi
datetime ldt_prossima, ldt_null
time 		lt_reg, lt_prossima, lt_relative_time
date ld_relative_date
long 		ll_gg, ll_freq, ll_giorno, ll_mese, ll_anno, ll_ultimo, ll_seconds_after
boolean  lb_bisestile
integer li_hour, li_mod_ore

setnull(ldt_null)
if isnull(fdt_data_registrazione) then return ldt_null

if fs_periodicita = 'M' then	//minuti
	ll_freq = fd_frequenza * 3600
elseif fs_periodicita = 'O' then
	//ll_freq = fd_frequenza * 60
	ll_freq = fd_frequenza * 3600
elseif fs_periodicita = 'S' then
	ll_freq = fd_frequenza * 7
elseif fs_periodicita = 'E' then
	ll_freq = fd_frequenza
elseif fs_periodicita = 'A' then
	ll_freq = fd_frequenza * 12
else
	ll_freq = fd_frequenza
end if

//minuti ------------------------------------------------------------------------------
if fs_periodicita='M' then //or fs_periodicita='O' then
	lt_prossima=time(fdt_data_registrazione)
	ll_seconds_after = secondsafter(time("23:59:59"),lt_prossima)
	if ll_seconds_after < ll_freq then
		lt_relative_time = relativetime(lt_prossima,ll_freq -secondsafter(time("23:59:59"),lt_prossima))
		lt_prossima=lt_relative_time
		ll_gg=1
	else
		ll_gg=0
		lt_relative_time = relativetime(lt_prossima,ll_freq)
		lt_prossima=lt_relative_time
	end if
	ll_freq=ll_gg
end if

//ore ------------------------------------------------------------------------------
if fs_periodicita='O' then
	ll_freq = integer(fd_frequenza / 24)
	if ll_freq = 0 then
		//durata inferiore a 24 ore
		
		//ricavo l'ora dal tempo della data reg. o data prel.
		li_hour = hour(time(fdt_data_registrazione))
		if (fd_frequenza + li_hour)  >= 24 then
			//cadiamo nel giorno successivo
			ldt_prossima = datetime(relativedate(date(fdt_data_registrazione), 1))
		else
			//cadiamo nello stesso giorno
			ldt_prossima = fdt_data_registrazione
		end if
	else
		ldt_prossima = datetime(relativedate(date(fdt_data_registrazione), ll_freq))
		
		//in ll_freq, cioè integer(fd_frequenza / 24), c'è il numero di giorni relativi a fd_frequenza
		//calcolo le ore restanti
		li_mod_ore = mod(fd_frequenza,24)
		
		//ricavo l'ora dal tempo della data reg. o data prel.
		li_hour = hour(time(fdt_data_registrazione))
		if (li_mod_ore + li_hour)  >= 24 then
			//cadiamo nel giorno successivo
			ldt_prossima = datetime(relativedate(date(ldt_prossima), 1))
		else
			//cadiamo nello stesso giorno: non fare niente, già calcolato in :ldt_prossima
		end if		
	end if
	
	select flag_includi_festivi
	into   :ls_festivi
	from   parametri_manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 or isnull(ls_festivi) then
		ls_festivi = "N"
	end if
	
	if ls_festivi = "N" then
		choose case daynumber(date(ldt_prossima))
			case 1 //domenica
				ld_relative_date = relativedate(date(ldt_prossima),1)
				ldt_prossima=datetime(ld_relative_date)
			case 7 //sabato
				ld_relative_date = relativedate(date(ldt_prossima),2)
				ldt_prossima=datetime(ld_relative_date)
		end choose
	end if
	
	return ldt_prossima
end if

select flag_includi_festivi
into   :ls_festivi
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_festivi) then
	ls_festivi = "N"
end if
ldt_prossima = fdt_data_registrazione
//------------------------------------------------------------------------------

//giorni e/o settimane
if (fs_periodicita = 'G' or fs_periodicita = 'S') or ll_gg = 1 then
	ld_relative_date = relativedate(date(ldt_prossima),ll_freq)
	ldt_prossima=datetime(ld_relative_date)
	choose case daynumber(date(ldt_prossima))
		case 1
			if ls_festivi = "N" then
				ld_relative_date = relativedate(date(ldt_prossima),1)
				ldt_prossima=datetime(ld_relative_date)
			end if
		case 7
			if ls_festivi = "N" then
				ld_relative_date = relativedate(date(ldt_prossima),2)
				ldt_prossima=datetime(ld_relative_date)
			end if
	end choose	
	ldt_prossima=datetime(date(ldt_prossima),time(fdt_data_registrazione))
end if
//------------------------------------------------------------------------------

//mesi e/o anni ----------------------------------------------------------
if fs_periodicita = 'E' or fs_periodicita = "A" then
	ll_giorno = day(date(ldt_prossima))
	ll_mese = month(date(ldt_prossima)) + ll_freq
	ll_anno = year(date(ldt_prossima))
	
	if ll_mese > 12 and mod(ll_mese,12) > 0 then
		ll_anno = ll_anno + int(ll_mese/12)
		ll_mese = mod(ll_mese,12)
	end if
	
	if ll_mese > 12 and mod(ll_mese,12) = 0 then
		ll_anno = ll_anno + int(ll_mese/12) - 1
		ll_mese = 12
	end if

	choose case ll_mese
		case 4,6,9,11
			ll_ultimo = 30
		case 1,3,5,7,8,10,12
			ll_ultimo = 31
		case 2
			if mod(ll_anno,4) = 0 and mod(ll_anno,100) <> 0 or mod(ll_anno,400) = 0 then
				ll_ultimo = 29
			else
				ll_ultimo = 28
			end if
	end choose
	
	if ll_giorno > ll_ultimo then
		ll_giorno = ll_ultimo
	end if
	
	ldt_prossima = datetime(date(string(ll_giorno) + "/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
end if
//-------------------------------------------------------------------------------------------

return ldt_prossima
end function

public subroutine wf_elabora_campionamenti_2 ();long ll_counter_pb, ll_prog_piano_campionamento_el, ll_n_campionamenti, ll_tot, ll_i, ll_riga
string ls_null, ls_modify, ls_cod_piano_campionamento_el, ls_cod_prodotto_el
string ls_cod_fornitore, ls_cod_prodotto, ls_cat_mer_da, ls_cod_test_el, ls_des_fornitore_el
string ls_periodicita_el, ls_cod_fornitore_el, ls_des_prodotto_el, ls_cod_cat_mer_el
string ls_cod_test, ls_des_test, ls_des_cat_mer_el
decimal ld_frequenza_el
datetime ldt_data_validita_el, ldt_data_scadenza, ldt_max_data
datetime ldt_data_scadenza_da, ldt_data_scadenza_a, ldt_data_null
datastore lds_campionamenti

st_1.text = "Elaborazione Campionamenti in scadenza in corso ... attendere!"
hpb_1.maxposition = ll_counter_pb
hpb_1.setstep = 1
hpb_1.position = 0

setpointer(HourGlass!)

dw_filtro.accepttext()

dw_campionamenti.setredraw(false)

dw_campionamenti.reset()

// loghi del report -------------------------------------------------------------
string ls_logos

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_campionamenti.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_campionamenti.modify(ls_modify)
//fine loghi del report -------------------------------------------------------

setnull(ls_null)
//acquisisci i valori del filtro ----------------------------------------------
ldt_data_scadenza_da = dw_filtro.getitemdatetime( 1, "data_prelievo_da")
ldt_data_scadenza_a = dw_filtro.getitemdatetime( 1, "data_prelievo_a")
ls_cod_fornitore = dw_filtro.getitemstring( 1, "cod_fornitore")
ls_cod_prodotto = dw_filtro.getitemstring( 1, "cod_prodotto")
ls_cat_mer_da = dw_filtro.getitemstring( 1, "cod_cat_mer")

if ls_cod_fornitore = "" then setnull(ls_cod_fornitore)
if ls_cod_prodotto = "" then setnull(ls_cod_prodotto)
if ls_cat_mer_da = "" then setnull(ls_cat_mer_da)


lds_campionamenti = create datastore
lds_campionamenti.dataobject = 'd_report_campionamenti_scadenze_new'
lds_campionamenti.settransobject(sqlca)
//******
setnull(ldt_data_null)
ll_tot = lds_campionamenti.retrieve( s_cs_xx.cod_azienda, ls_cod_fornitore, ldt_data_null, ldt_data_null, ls_cod_prodotto)
	
if ll_tot < 0 then
	g_mb.messagebox("OMNIA", "Errore durante la retrieve del datastore: " + sqlca.sqlerrtext)
	hpb_1.position = 0
	setpointer(Arrow!)
	st_1.text = ""
	return
end if

for ll_i = 1 to ll_tot
	ls_cod_piano_campionamento_el = lds_campionamenti.getitemstring(ll_i, "cod_piano_campionamento")
	ldt_data_validita_el = lds_campionamenti.getitemdatetime(ll_i, "data_validita")
	ll_prog_piano_campionamento_el = lds_campionamenti.getitemnumber(ll_i, "prog_piano_campionamento")
	
	// *** controllo se ha dei campionamenti
	select count(*)
	into   :ll_n_campionamenti
	from   tes_campionamenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_piano_campionamento = :ls_cod_piano_campionamento_el and
			 data_validita = :ldt_data_validita_el and
			 prog_piano_campionamento = :ll_prog_piano_campionamento_el;

	 if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore durante la ricerca di campionamenti: " + sqlca.sqlerrtext)
		hpb_1.position = 0
		setpointer(Arrow!)
		st_1.text = ""
		return
	end if
	
	ls_cod_fornitore_el = lds_campionamenti.getitemstring( ll_i, "cod_fornitore")
	ls_periodicita_el = lds_campionamenti.getitemstring( ll_i, "periodicita")
	ld_frequenza_el = lds_campionamenti.getitemnumber( ll_i, "frequenza")
	ls_cod_prodotto_el = lds_campionamenti.getitemstring( ll_i, "cod_prodotto")
	ls_cod_test_el = lds_campionamenti.getitemstring( ll_i, "cod_test")
	
	if not isnull(ls_cod_prodotto) then
		if ls_cod_prodotto_el <> ls_cod_prodotto then continue
	end if
	if not isnull(ls_cod_fornitore) then
		if ls_cod_fornitore_el <> ls_cod_fornitore then continue
	end if
	
	if isnull(ll_n_campionamenti) then ll_n_campionamenti = 0			 
	
	if ll_n_campionamenti = 0 then
		ldt_data_scadenza = ldt_data_validita_el
	else
		select max(data_registrazione)
		into   :ldt_max_data
		from   tes_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_piano_campionamento = :ls_cod_piano_campionamento_el and
				 data_validita = :ldt_data_validita_el and
				 prog_piano_campionamento = :ll_prog_piano_campionamento_el;
				 
		ldt_data_scadenza = wf_prossima_scadenza2( ldt_max_data, ls_periodicita_el, ld_frequenza_el)				
	end if
	
	if not isnull(ldt_data_scadenza_da) then
		if ldt_data_scadenza_da > ldt_data_scadenza then continue
	end if
	if not isnull(ldt_data_scadenza_a) then
		if ldt_data_scadenza_a < ldt_data_scadenza then continue
	end if
	
	select des_prodotto, 
			 cod_cat_mer
	into   :ls_des_prodotto_el, 
			 :ls_cod_cat_mer_el
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_el;
			 
	if ls_cod_cat_mer_el = "" or isnull(ls_cod_cat_mer_el) then 
		ls_cod_cat_mer_el = ""
		ls_des_cat_mer_el = "PRODOTTI SENZA CATEGORIA"
	else
		select des_cat_mer
		into :ls_des_cat_mer_el
		from tab_cat_mer
		where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cat_mer = :ls_cod_cat_mer_el;
	end if
			 
	if not isnull(ls_cat_mer_da) then
		if ls_cod_cat_mer_el <> ls_cat_mer_da then continue
	end if
	
	select rag_soc_1
	into   :ls_des_fornitore_el
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore_el;
	
	if sqlca.sqlcode = 100 then 
		ls_cod_fornitore_el = ""
		ls_des_fornitore_el = "Ragione Sociale non trovata in anagrafica"
	end if
	
	ls_cod_test =  lds_campionamenti.getitemstring( ll_i, "cod_test")
	
	select des_test
	into   :ls_des_test
	from   tab_test
	where  cod_test = :ls_cod_test;
	
	if isnull(ls_des_test) then  ls_des_test =  ""
	
	//se arrivi fin qui vuol dire che si può procedere all'inserimento
	ll_riga = dw_campionamenti.insertrow(0)			

	dw_campionamenti.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
	dw_campionamenti.setitem( ll_riga, "cod_piano_campionamento", ls_cod_piano_campionamento_el)
	dw_campionamenti.setitem( ll_riga, "prog_piano_campionamento", ll_prog_piano_campionamento_el)
	dw_campionamenti.setitem( ll_riga, "data_validita", ldt_data_validita_el)
	
	dw_campionamenti.setitem( ll_riga, "prodotto", ls_cod_prodotto_el + " " + ls_des_prodotto_el)
	
	dw_campionamenti.setitem( ll_riga, "categoria", ls_cod_cat_mer_el + " " + ls_des_cat_mer_el)
	
	dw_campionamenti.setitem( ll_riga, "fornitore", ls_cod_fornitore_el + " " + ls_des_fornitore_el)	
	dw_campionamenti.setitem( ll_riga, "cod_test", ls_cod_test)
	dw_campionamenti.setitem( ll_riga, "des_test", ls_des_test)
	
	dw_campionamenti.groupcalc()
	dw_campionamenti.sort()
	
	hpb_1.stepit()
next

dw_campionamenti.groupcalc()
dw_campionamenti.sort()
dw_campionamenti.setredraw(true)
dw_campionamenti.Object.DataWindow.Print.Preview = 'Yes'
st_1.text = ""
hpb_1.position = 0
setpointer(Arrow!)

return

		
				 
		
		
		
		
		

end subroutine

public subroutine wf_elabora_test_2 ();string ls_cod_test, ls_cod_valore, ls_flag_altro_valore, ls_altro_valore, ls_cod_prodotto,ls_flag_tipologia_durata
string ls_cod_piano_campionamento, ls_des_piano_campionamento, ls_des_prodotto, ls_des_test, ls_cod_cat_mer
long ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_prog_riga_campionamenti, ll_prog_piano_campionamento
long ll_CountRowToProcessing, ll_CurRow, ll_valore_durata, ll_CurRow2
double lb_quantita
datetime ldt_data_validita, ldt_data_prelievo, ldt_data_scadenza
datetime ldt_data_prelievo_da_filter, ldt_data_prelievo_a_filter, ldt_null
string ls_cod_fornitore_filter, ls_cod_prodotto_filter, ls_cod_cat_mer_filter

// loghi del report (dw_test_report) -------------------
string ls_logos, ls_modify

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_test_report.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_test_report.modify(ls_modify)
//----------------------------------

setnull(ldt_null)
ldt_data_prelievo_da_filter = dw_filtro.getitemdatetime(1,"data_prelievo_da")
ldt_data_prelievo_a_filter = dw_filtro.getitemdatetime(1,"data_prelievo_a")
ls_cod_fornitore_filter = dw_filtro.getitemstring(1,"cod_fornitore")
ls_cod_prodotto_filter = dw_filtro.getitemstring(1,"cod_prodotto")
ls_cod_cat_mer_filter = dw_filtro.getitemstring(1,"cod_cat_mer")

if ls_cod_fornitore_filter = "" then setnull(ls_cod_fornitore_filter)
if ls_cod_prodotto_filter = "" then setnull(ls_cod_prodotto_filter)
if ls_cod_cat_mer_filter = "" then setnull(ls_cod_cat_mer_filter)


SELECT  
		 count(det_campionamenti.anno_reg_campionamenti)
INTO :ll_CountRowToProcessing
FROM det_campionamenti, tes_campionamenti
WHERE  tes_campionamenti.cod_azienda = det_campionamenti.cod_azienda
	AND tes_campionamenti.anno_reg_campionamenti = det_campionamenti.anno_reg_campionamenti
	AND tes_campionamenti.num_reg_campionamenti = det_campionamenti.num_reg_campionamenti
	AND det_campionamenti.cod_azienda = :s_cs_xx.cod_azienda
	AND (det_campionamenti.cod_valore IS NULL or det_campionamenti.cod_valore='')
	
	AND (det_campionamenti.flag_esito='I')
	AND (det_campionamenti.quantita IS NULL OR det_campionamenti.quantita=0)
	/*
	AND (det_campionamenti.quantita IS NULL OR (det_campionamenti.quantita=0
																		AND det_campionamenti.flag_esito="I"))
	*/
	AND (det_campionamenti.flag_altro_valore='N' OR 
									(det_campionamenti.flag_altro_valore='S' AND 
											(det_campionamenti.altro_valore IS NULL
											 OR det_campionamenti.altro_valore='')
									)
			)
	AND (tes_campionamenti.cod_fornitore = :ls_cod_fornitore_filter OR :ls_cod_fornitore_filter IS NULL)
	AND (tes_campionamenti.cod_prodotto = :ls_cod_prodotto_filter OR :ls_cod_prodotto_filter IS NULL)
;

dw_test.reset()
dw_test_report.reset()

if ll_CountRowToProcessing > 0 then	
else
	return
end if

st_1.text = "Elaborazione Test in scadenza in corso ... attendere!"
hpb_1.maxposition = ll_CountRowToProcessing
hpb_1.setstep = 1
hpb_1.position = 0

setpointer(HourGlass!)

//cursore definito per scovare i test registrati e non ancora con un valore
//successivamente saranno presi in considerazione olo quelli che soddisfano
//il criterio di ricerca eventualmente impostato
string ls_sql_dynamic

ls_sql_dynamic = 'SELECT  ' + &
		 'tes_campionamenti.anno_reg_campionamenti' + &
		',tes_campionamenti.num_reg_campionamenti' + &
		',det_campionamenti.prog_riga_campionamenti' + &
		',tes_campionamenti.cod_piano_campionamento' + &
		',tes_campionamenti.prog_piano_campionamento' + &
		',tes_campionamenti.data_prelievo' + &
		',tes_campionamenti.data_validita' + &
		',det_campionamenti.cod_test' + &
		',det_campionamenti.cod_prodotto' + &
		',det_campionamenti.data_validita' + &
		',det_campionamenti.cod_valore' + &
		',det_campionamenti.quantita' + &
		',det_campionamenti.flag_altro_valore' + &
		',det_campionamenti.altro_valore ' + &
'FROM det_campionamenti, tes_campionamenti ' + &
'WHERE  tes_campionamenti.cod_azienda = det_campionamenti.cod_azienda ' + &
	'AND tes_campionamenti.anno_reg_campionamenti = det_campionamenti.anno_reg_campionamenti ' + &
	'AND tes_campionamenti.num_reg_campionamenti = det_campionamenti.num_reg_campionamenti ' + &
	"AND det_campionamenti.cod_azienda = '"+ s_cs_xx.cod_azienda +"' " + &
	"AND (det_campionamenti.cod_valore IS NULL or det_campionamenti.cod_valore='') " + &	
	"AND (det_campionamenti.flag_esito='I') " + &
	'AND (det_campionamenti.quantita IS NULL OR det_campionamenti.quantita=0) ' + &
	"AND (det_campionamenti.flag_altro_valore='N' OR " + &
									"(det_campionamenti.flag_altro_valore='S' AND  " + &
											'(det_campionamenti.altro_valore IS NULL ' + &
											 "OR det_campionamenti.altro_valore='') " + &
									') ' + &
			') '
			
if not isnull(ls_cod_fornitore_filter) and ls_cod_fornitore_filter<>"" then
	ls_sql_dynamic +="AND tes_campionamenti.cod_fornitore = '"+ls_cod_fornitore_filter+"' "
end if

if not isnull(ls_cod_prodotto_filter) and ls_cod_prodotto_filter<>"" then
	ls_sql_dynamic += "AND tes_campionamenti.cod_prodotto = '"+ls_cod_prodotto_filter +"' "
end if

declare cu_test dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_dynamic;
open dynamic cu_test;


if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore dei test. Dettaglio = " + sqlca.sqlerrtext)
	hpb_1.position = 0
	st_1.text = ""
	setpointer(Arrow!)
	return
end if


DO WHILE TRUE
	FETCH cu_test INTO 
			 :ll_anno_reg_campionamenti
			,:ll_num_reg_campionamenti
			,:ll_prog_riga_campionamenti
			,:ls_cod_piano_campionamento
			,:ll_prog_piano_campionamento
			,:ldt_data_prelievo
			,:ldt_data_validita
			,:ls_cod_test
			,:ls_cod_prodotto
			,:ldt_data_validita
			,:ls_cod_valore
			,:lb_quantita
			,:ls_flag_altro_valore
			,:ls_altro_valore
	;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore dei test. Dettaglio = " + sqlca.sqlerrtext)
		close cu_test;
		rollback;
		hpb_1.position = 0
		st_1.text = ""
		setpointer(Arrow!)
		return
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	//prima verifica:
	//se il test in esame ha flag_tipologia_durata posto a "N" saltalo
	//verifica questo nella tab_test_prodotti (fissato cod_azienda,cod_prodotto, cod_test, data_validita)
	setnull(ls_flag_tipologia_durata)
	setnull(ll_valore_durata)
	SELECT
		 tab_test_prodotti.flag_tipologia_durata
		,tab_test_prodotti.valore_durata
	INTO
		 :ls_flag_tipologia_durata
		,:ll_valore_durata
	FROM tab_test_prodotti
	WHERE tab_test_prodotti.cod_azienda = :s_cs_xx.cod_azienda
		AND tab_test_prodotti.cod_prodotto = :ls_cod_prodotto
		AND tab_test_prodotti.cod_test = :ls_cod_test
		AND tab_test_prodotti.data_validita = :ldt_data_validita
	;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select sulla durata dei test. Dettaglio = " + sqlca.sqlerrtext)
		close cu_test;
		rollback;
		hpb_1.position = 0
		st_1.text = ""
		setpointer(Arrow!)
		return
	end if	
	
	//scadenza per ORE(H) GIORNI (G) MESI (M)
	IF (ls_flag_tipologia_durata = "H" OR ls_flag_tipologia_durata = "G" OR ls_flag_tipologia_durata = "M") THEN		
		//eliminare i test che non soddisfano il criterio di ricerca (solo per quanto riguarda la categoria merceologica, il resto è stato già filtrato)
		
		//recupero la descrizione del prodotto e il codice della categoria merceologica
		setnull(ls_des_prodotto)
		SELECT anag_prodotti.des_prodotto, anag_prodotti.cod_cat_mer
		INTO :ls_des_prodotto, :ls_cod_cat_mer
		FROM anag_prodotti
		WHERE anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda
			AND anag_prodotti.cod_prodotto = :ls_cod_prodotto
		;
		
		//verifica se è impostato il filtro per categoria merceologica e se necessario filtra
		if not isnull(ls_cod_cat_mer_filter) then
			//filtro impostato: verifica la condizione
			if ls_cod_cat_mer_filter <> ls_cod_cat_mer_filter then continue
		end if
		
		//continua a processare calcolando la scadenza del test
		//utilizzo la stessa funzione che viene adoperata per il calcolod ella scadenza dei campionamenti
		//a patto di cambiare la lettera di alcuni flag
		if ls_flag_tipologia_durata = "H" then 
			ls_flag_tipologia_durata = "O"
		elseif ls_flag_tipologia_durata="M" then
			ls_flag_tipologia_durata = "E"
		end if			
		ldt_data_scadenza = wf_prossima_scadenza2(ldt_data_prelievo,ls_flag_tipologia_durata,ll_valore_durata)
		
		//filtra includendo nel periodo di scadenza eventualmente anche gli estremi
		if not isnull(ldt_data_prelievo_a_filter) and ldt_data_scadenza > ldt_data_prelievo_a_filter  then continue	
		if not isnull(ldt_data_prelievo_da_filter) and ldt_data_scadenza < ldt_data_prelievo_da_filter then 	continue	
		

		ll_CurRow = dw_test.insertrow(0)
		ll_CurRow2 = dw_test_report.insertrow(0)
		
		dw_test.setitem(ll_CurRow,"anno_reg_campionamenti",ll_anno_reg_campionamenti)
		dw_test.setitem(ll_CurRow,"num_reg_campionamenti",ll_num_reg_campionamenti)
		dw_test.setitem(ll_CurRow,"cod_prodotto",ls_cod_prodotto)
		dw_test.setitem(ll_CurRow,"cod_test",ls_cod_test)
		
		dw_test.setitem(ll_CurRow,"data_scadenza",ldt_data_scadenza)
		dw_test.setitem(ll_CurRow,"data_validita",ldt_data_validita)

		dw_test.setitem(ll_CurRow,"prog_riga_campionamenti",ll_prog_riga_campionamenti)
		dw_test.setitem(ll_CurRow,"cod_piano_campionamento",ls_cod_piano_campionamento)
		//---
		dw_test_report.setitem(ll_CurRow2,"anno_reg_campionamenti",ll_anno_reg_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"num_reg_campionamenti",ll_num_reg_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"cod_prodotto",ls_cod_prodotto)
		dw_test_report.setitem(ll_CurRow2,"cod_test",ls_cod_test)
		
		dw_test_report.setitem(ll_CurRow2,"data_scadenza",ldt_data_scadenza)
		dw_test_report.setitem(ll_CurRow2,"data_validita",ldt_data_validita)

		dw_test_report.setitem(ll_CurRow2,"prog_riga_campionamenti",ll_prog_riga_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"cod_piano_campionamento",ls_cod_piano_campionamento)
		//---

		//recupero des_campionamento
		setnull(ls_des_piano_campionamento)
		SELECT des_piano_campionamento
		INTO :ls_des_piano_campionamento
		FROM tes_piani_campionamento
		WHERE tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda
			AND tes_piani_campionamento.cod_piano_campionamento = :ls_cod_piano_campionamento
			AND tes_piani_campionamento.data_validita = :ldt_data_validita
			AND tes_piani_campionamento.prog_piano_campionamento = :ll_prog_piano_campionamento
		;
		dw_test.setitem(ll_CurRow,"des_piano_campionamento",ls_des_piano_campionamento)
		
		dw_test.setitem(ll_CurRow,"cod_prodotto",ls_cod_prodotto)
		dw_test.setitem(ll_CurRow,"des_prodotto",ls_des_prodotto)
		//---------
		dw_test_report.setitem(ll_CurRow2,"des_piano_campionamento",ls_des_piano_campionamento)
		
		dw_test_report.setitem(ll_CurRow2,"cod_prodotto",ls_cod_prodotto)
		dw_test_report.setitem(ll_CurRow2,"des_prodotto",ls_des_prodotto)
		//--------
	
		//recupero la descrizione del test
		setnull(ls_des_test)
		SELECT tab_test.des_test
		INTO :ls_des_test
		FROM tab_test
		WHERE tab_test.cod_test = :ls_cod_test
		;
		dw_test.setitem(ll_CurRow,"des_test",ls_des_test)
		//------
		dw_test_report.setitem(ll_CurRow2,"des_test",ls_des_test)
		//-----

	END IF
	hpb_1.stepit()
loop

dw_test.SetRowFocusIndicator(Hand!)
dw_test.groupcalc( )
//----
dw_test_report.groupcalc( )
dw_test_report.Object.DataWindow.Print.Preview = 'Yes'
//----

hpb_1.position = 0
st_1.text = ""
setpointer(Arrow!)

close cu_test;

end subroutine

public subroutine wf_elabora_test_3 ();string ls_cod_test, ls_cod_valore, ls_flag_altro_valore, ls_altro_valore, ls_cod_prodotto,ls_flag_tipologia_durata
string ls_cod_piano_campionamento, ls_des_piano_campionamento, ls_des_prodotto, ls_des_test, ls_cod_cat_mer,ls_sintassi,ls_errore
long ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_prog_riga_campionamenti, ll_prog_piano_campionamento
long ll_CountRowToProcessing, ll_CurRow, ll_valore_durata, ll_CurRow2, ll_index
double lb_quantita
datetime ldt_data_validita, ldt_data_prelievo, ldt_data_scadenza
datetime ldt_data_prelievo_da_filter, ldt_data_prelievo_a_filter, ldt_null
string ls_cod_fornitore_filter, ls_cod_prodotto_filter, ls_cod_cat_mer_filter
datastore lds_test

// loghi del report (dw_test_report) -------------------
string ls_logos, ls_modify

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOS';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOS in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logos.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_test_report.modify(ls_modify)

select parametri_azienda.stringa
into   :ls_logos
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOC';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOC in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logoc.filename='" + s_cs_xx.volume + ls_logos + "'"
dw_test_report.modify(ls_modify)
//----------------------------------

setnull(ldt_null)
ldt_data_prelievo_da_filter = dw_filtro.getitemdatetime(1,"data_prelievo_da")
ldt_data_prelievo_a_filter = dw_filtro.getitemdatetime(1,"data_prelievo_a")
ls_cod_fornitore_filter = dw_filtro.getitemstring(1,"cod_fornitore")
ls_cod_prodotto_filter = dw_filtro.getitemstring(1,"cod_prodotto")
ls_cod_cat_mer_filter = dw_filtro.getitemstring(1,"cod_cat_mer")

if ls_cod_fornitore_filter = "" then setnull(ls_cod_fornitore_filter)
if ls_cod_prodotto_filter = "" then setnull(ls_cod_prodotto_filter)
if ls_cod_cat_mer_filter = "" then setnull(ls_cod_cat_mer_filter)


//SELECT  
//		 count(det_campionamenti.anno_reg_campionamenti)
//INTO :ll_CountRowToProcessing
//FROM det_campionamenti, tes_campionamenti
//WHERE  tes_campionamenti.cod_azienda = det_campionamenti.cod_azienda
//	AND tes_campionamenti.anno_reg_campionamenti = det_campionamenti.anno_reg_campionamenti
//	AND tes_campionamenti.num_reg_campionamenti = det_campionamenti.num_reg_campionamenti
//	AND det_campionamenti.cod_azienda = :s_cs_xx.cod_azienda
//	AND (det_campionamenti.cod_valore IS NULL or det_campionamenti.cod_valore='')
//	
//	AND (det_campionamenti.flag_esito='I')
//	AND (det_campionamenti.quantita IS NULL OR det_campionamenti.quantita=0)
//	/*
//	AND (det_campionamenti.quantita IS NULL OR (det_campionamenti.quantita=0
//																		AND det_campionamenti.flag_esito="I"))
//	*/
//	AND (det_campionamenti.flag_altro_valore='N' OR 
//									(det_campionamenti.flag_altro_valore='S' AND 
//											(det_campionamenti.altro_valore IS NULL
//											 OR det_campionamenti.altro_valore='')
//									)
//			)
//	AND (tes_campionamenti.cod_fornitore = :ls_cod_fornitore_filter OR :ls_cod_fornitore_filter IS NULL)
//	AND (tes_campionamenti.cod_prodotto = :ls_cod_prodotto_filter OR :ls_cod_prodotto_filter IS NULL)
//;

dw_test.reset()
dw_test_report.reset()

setpointer(HourGlass!)

//cursore definito per scovare i test registrati e non ancora con un valore
//successivamente saranno presi in considerazione olo quelli che soddisfano
//il criterio di ricerca eventualmente impostato
string ls_sql_dynamic

ls_sql_dynamic = 'SELECT  ' + &
		 'tes_campionamenti.anno_reg_campionamenti' + &
		',tes_campionamenti.num_reg_campionamenti' + &
		',det_campionamenti.prog_riga_campionamenti' + &
		',tes_campionamenti.cod_piano_campionamento' + &
		',tes_campionamenti.prog_piano_campionamento' + &
		',tes_campionamenti.data_prelievo' + &
		',tes_campionamenti.data_validita' + &
		',det_campionamenti.cod_test' + &
		',det_campionamenti.cod_prodotto' + &
		',det_campionamenti.cod_valore' + &
		',det_campionamenti.quantita' + &
		',det_campionamenti.flag_altro_valore' + &
		',det_campionamenti.altro_valore ' + &
'FROM det_campionamenti, tes_campionamenti ' + &
'WHERE  tes_campionamenti.cod_azienda = det_campionamenti.cod_azienda ' + &
	'AND tes_campionamenti.anno_reg_campionamenti = det_campionamenti.anno_reg_campionamenti ' + &
	'AND tes_campionamenti.num_reg_campionamenti = det_campionamenti.num_reg_campionamenti ' + &
	"AND det_campionamenti.cod_azienda = '"+ s_cs_xx.cod_azienda +"' " + &
	"AND (det_campionamenti.cod_valore IS NULL or det_campionamenti.cod_valore='') " + &	
	"AND (det_campionamenti.flag_esito='I') " + &
	'AND (det_campionamenti.quantita IS NULL OR det_campionamenti.quantita=0) ' + &
	"AND (det_campionamenti.flag_altro_valore='N' OR " + &
									"(det_campionamenti.flag_altro_valore='S' AND  " + &
											'(det_campionamenti.altro_valore IS NULL ' + &
											 "OR det_campionamenti.altro_valore='') " + &
									') ' + &
			') '
			
if not isnull(ls_cod_fornitore_filter) and ls_cod_fornitore_filter<>"" then
	ls_sql_dynamic +="AND tes_campionamenti.cod_fornitore = '"+ls_cod_fornitore_filter+"' "
end if

if not isnull(ls_cod_prodotto_filter) and ls_cod_prodotto_filter<>"" then
	ls_sql_dynamic += "AND tes_campionamenti.cod_prodotto = '"+ls_cod_prodotto_filter +"' "
end if

//declare cu_test dynamic cursor for sqlsa;
//prepare sqlsa from :ls_sql_dynamic;
//open dynamic cu_test;
ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql_dynamic, "style(type=grid)", ls_errore)

if Len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore dei test:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql_dynamic)
	hpb_1.position = 0
	st_1.text = ""
	setpointer(Arrow!)
   return
end if

lds_test = create datastore
lds_test.Create( ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	destroy lds_test;
	hpb_1.position = 0
	st_1.text = ""
	setpointer(Arrow!)
   return
end if

lds_test.settransobject( sqlca)

ll_CountRowToProcessing = lds_test.retrieve()

if ll_CountRowToProcessing < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe dei test:" + sqlca.sqlerrtext)
	destroy lds_test;
	hpb_1.position = 0
	st_1.text = ""
	setpointer(Arrow!)
	return 
end if

if ll_CountRowToProcessing > 0 then	
else
	return
end if

st_1.text = "Elaborazione Test in scadenza in corso ... attendere!"
hpb_1.maxposition = ll_CountRowToProcessing
hpb_1.setstep = 1
hpb_1.position = 0

for ll_index = 1 to ll_CountRowToProcessing
	ll_anno_reg_campionamenti		= lds_test.getitemdecimal(ll_index,"tes_campionamenti_anno_reg_campionamenti")
	ll_num_reg_campionamenti		= lds_test.getitemdecimal(ll_index,"tes_campionamenti_num_reg_campionamenti")
	ll_prog_riga_campionamenti		= lds_test.getitemnumber(ll_index,"det_campionamenti_prog_riga_campionamenti")
	ls_cod_piano_campionamento	= lds_test.getitemstring(ll_index,"tes_campionamenti_cod_piano_campionamento")
	ll_prog_piano_campionamento	= lds_test.getitemnumber(ll_index,"tes_campionamenti_prog_piano_campionamento")
	ldt_data_prelievo						= lds_test.getitemdatetime(ll_index,"tes_campionamenti_data_prelievo")
	ldt_data_validita						= lds_test.getitemdatetime(ll_index,"tes_campionamenti_data_validita")
	ls_cod_test								= lds_test.getitemstring(ll_index,"det_campionamenti_cod_test")
	ls_cod_prodotto						= lds_test.getitemstring(ll_index,"det_campionamenti_cod_prodotto")
	ls_cod_valore							= lds_test.getitemstring(ll_index,"det_campionamenti_cod_valore")
	lb_quantita								= lds_test.getitemdecimal(ll_index,"det_campionamenti_quantita")
	ls_flag_altro_valore					= lds_test.getitemstring(ll_index,"det_campionamenti_flag_altro_valore")
	ls_altro_valore						= lds_test.getitemstring(ll_index,"det_campionamenti_altro_valore")
	
	//prima verifica:
	//se il test in esame ha flag_tipologia_durata posto a "N" saltalo
	//verifica questo nella tab_test_prodotti (fissato cod_azienda,cod_prodotto, cod_test, data_validita)
	setnull(ls_flag_tipologia_durata)
	setnull(ll_valore_durata)
	SELECT
		 tab_test_prodotti.flag_tipologia_durata
		,tab_test_prodotti.valore_durata
	INTO
		 :ls_flag_tipologia_durata
		,:ll_valore_durata
	FROM tab_test_prodotti
	WHERE tab_test_prodotti.cod_azienda = :s_cs_xx.cod_azienda
		AND tab_test_prodotti.cod_prodotto = :ls_cod_prodotto
		AND tab_test_prodotti.cod_test = :ls_cod_test
		AND tab_test_prodotti.data_validita = :ldt_data_validita
	;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select sulla durata dei test. Dettaglio = " + sqlca.sqlerrtext)		
		rollback;
		destroy lds_test;
		hpb_1.position = 0
		st_1.text = ""
		setpointer(Arrow!)
		return
	end if
	
	//scadenza per ORE(H) GIORNI (G) MESI (M)
	IF (ls_flag_tipologia_durata = "H" OR ls_flag_tipologia_durata = "G" OR ls_flag_tipologia_durata = "M") THEN		
		//eliminare i test che non soddisfano il criterio di ricerca (solo per quanto riguarda la categoria merceologica, il resto è stato già filtrato)
		
		//recupero la descrizione del prodotto e il codice della categoria merceologica
		setnull(ls_des_prodotto)
		SELECT anag_prodotti.des_prodotto, anag_prodotti.cod_cat_mer
		INTO :ls_des_prodotto, :ls_cod_cat_mer
		FROM anag_prodotti
		WHERE anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda
			AND anag_prodotti.cod_prodotto = :ls_cod_prodotto
		;
		
		//verifica se è impostato il filtro per categoria merceologica e se necessario filtra
		if not isnull(ls_cod_cat_mer_filter) then
			//filtro impostato: verifica la condizione
			if ls_cod_cat_mer_filter <> ls_cod_cat_mer_filter then continue
		end if
		
		//continua a processare calcolando la scadenza del test
		//utilizzo la stessa funzione che viene adoperata per il calcolod ella scadenza dei campionamenti
		//a patto di cambiare la lettera di alcuni flag
		if ls_flag_tipologia_durata = "H" then 
			ls_flag_tipologia_durata = "O"
		elseif ls_flag_tipologia_durata="M" then
			ls_flag_tipologia_durata = "E"
		end if			
		ldt_data_scadenza = wf_prossima_scadenza2(ldt_data_prelievo,ls_flag_tipologia_durata,ll_valore_durata)
		
		//filtra includendo nel periodo di scadenza eventualmente anche gli estremi
		if not isnull(ldt_data_prelievo_a_filter) and ldt_data_scadenza > ldt_data_prelievo_a_filter  then continue	
		if not isnull(ldt_data_prelievo_da_filter) and ldt_data_scadenza < ldt_data_prelievo_da_filter then 	continue	
		

		ll_CurRow = dw_test.insertrow(0)
		ll_CurRow2 = dw_test_report.insertrow(0)
		
		dw_test.setitem(ll_CurRow,"anno_reg_campionamenti",ll_anno_reg_campionamenti)
		dw_test.setitem(ll_CurRow,"num_reg_campionamenti",ll_num_reg_campionamenti)
		dw_test.setitem(ll_CurRow,"cod_prodotto",ls_cod_prodotto)
		dw_test.setitem(ll_CurRow,"cod_test",ls_cod_test)
		
		dw_test.setitem(ll_CurRow,"data_scadenza",ldt_data_scadenza)
		dw_test.setitem(ll_CurRow,"data_validita",ldt_data_validita)

		dw_test.setitem(ll_CurRow,"prog_riga_campionamenti",ll_prog_riga_campionamenti)
		dw_test.setitem(ll_CurRow,"cod_piano_campionamento",ls_cod_piano_campionamento)
		//---
		dw_test_report.setitem(ll_CurRow2,"anno_reg_campionamenti",ll_anno_reg_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"num_reg_campionamenti",ll_num_reg_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"cod_prodotto",ls_cod_prodotto)
		dw_test_report.setitem(ll_CurRow2,"cod_test",ls_cod_test)
		
		dw_test_report.setitem(ll_CurRow2,"data_scadenza",ldt_data_scadenza)
		dw_test_report.setitem(ll_CurRow2,"data_validita",ldt_data_validita)

		dw_test_report.setitem(ll_CurRow2,"prog_riga_campionamenti",ll_prog_riga_campionamenti)
		dw_test_report.setitem(ll_CurRow2,"cod_piano_campionamento",ls_cod_piano_campionamento)
		//---

		//recupero des_campionamento
		setnull(ls_des_piano_campionamento)
		SELECT des_piano_campionamento
		INTO :ls_des_piano_campionamento
		FROM tes_piani_campionamento
		WHERE tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda
			AND tes_piani_campionamento.cod_piano_campionamento = :ls_cod_piano_campionamento
			AND tes_piani_campionamento.data_validita = :ldt_data_validita
			AND tes_piani_campionamento.prog_piano_campionamento = :ll_prog_piano_campionamento
		;
		dw_test.setitem(ll_CurRow,"des_piano_campionamento",ls_des_piano_campionamento)
		
		dw_test.setitem(ll_CurRow,"cod_prodotto",ls_cod_prodotto)
		dw_test.setitem(ll_CurRow,"des_prodotto",ls_des_prodotto)
		//---------
		dw_test_report.setitem(ll_CurRow2,"des_piano_campionamento",ls_des_piano_campionamento)
		
		dw_test_report.setitem(ll_CurRow2,"cod_prodotto",ls_cod_prodotto)
		dw_test_report.setitem(ll_CurRow2,"des_prodotto",ls_des_prodotto)
		//--------
	
		//recupero la descrizione del test
		setnull(ls_des_test)
		SELECT tab_test.des_test
		INTO :ls_des_test
		FROM tab_test
		WHERE tab_test.cod_test = :ls_cod_test
		;
		dw_test.setitem(ll_CurRow,"des_test",ls_des_test)
		//------
		dw_test_report.setitem(ll_CurRow2,"des_test",ls_des_test)
		//-----

	END IF
	hpb_1.stepit()
	
next

dw_test.SetRowFocusIndicator(Hand!)
dw_test.groupcalc( )
//----
dw_test_report.groupcalc( )
dw_test_report.Object.DataWindow.Print.Preview = 'Yes'
//----

hpb_1.position = 0
st_1.text = ""
setpointer(Arrow!)
destroy lds_test;

end subroutine

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_filtro,"cod_test",sqlca,&
//                 "tab_test","cod_test","des_test","")

f_PO_LoadDDDW_DW(dw_filtro,"cod_cat_mer",sqlca,&
                 "tab_cat_mer","cod_cat_mer","des_cat_mer",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//f_PO_LoadDDDW_DW(dw_test,"cod_valore",sqlca,&
//                 "tab_test_valori","cod_valore","des_valore","")

end event

event pc_setwindow;call super::pc_setwindow;datetime ldt_oggi, ldt_oggi_a_x_gg
integer li_Imposta_scadenza = 7
windowobject lw_oggetti[]
string ls_path

//dw_filtro.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)
//dw_campionamenti.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)
//dw_test.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)

//  ------------------------  imposto folder di ricerca ---------------------------

dw_filtro.insertrow(0)

lw_oggetti[1] = dw_campionamenti
lw_oggetti[2] = cb_stampa_campionamenti
lw_oggetti[3] = cb_refresh_campionamenti
dw_folder.fu_assigntab(2, "Campionamenti in scadenza", lw_oggetti[])

lw_oggetti[1] = dw_test
lw_oggetti[2] = cb_salva_test
lw_oggetti[3] = cb_refresh_test
lw_oggetti[4] = cb_stampa_test
dw_folder.fu_assigntab(3, "Test in scadenza", lw_oggetti[])

lw_oggetti[1] = dw_filtro
lw_oggetti[2] = cb_cerca
lw_oggetti[3] = cb_reset
lw_oggetti[4] = hpb_1
lw_oggetti[5] = st_1
dw_folder.fu_assigntab(1, "Filtro", lw_oggetti[])

dw_folder.fu_foldercreate(3, 3)
dw_folder.fu_selecttab(1)


//ldt_oggi = datetime(today(),time("00:00:00"))
//ldt_oggi_a_x_gg = datetime(RelativeDate(date(ldt_oggi), li_Imposta_scadenza), time("00:00:00"))
//
//dw_filtro.setitem(1,"data_prelievo_da",ldt_oggi)
//dw_filtro.setitem(1,"data_prelievo_a",ldt_oggi_a_x_gg)

setnull(ldt_oggi)
dw_filtro.setitem(1,"data_prelievo_da",ldt_oggi)
dw_filtro.setitem(1,"data_prelievo_a",ldt_oggi)

//imposta le immagini per i bottoni e la legenda della dw_test
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "icn_search.bmp" //"VIS.BMP"
dw_test.object.b_consulta.FileName = ls_path
dw_test.object.p_consulta.FileName = ls_path

//imposta il cursore in alcune colonne della dw_campionamenti
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "hibeam.cur"
dw_campionamenti.object.cod_test.pointer = ls_path
dw_campionamenti.object.des_test.pointer = ls_path





end event

on w_scadenziario_camp_e_test.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.hpb_1=create hpb_1
this.st_1=create st_1
this.cb_cerca=create cb_cerca
this.cb_refresh_campionamenti=create cb_refresh_campionamenti
this.cb_refresh_test=create cb_refresh_test
this.cb_reset=create cb_reset
this.cb_salva_test=create cb_salva_test
this.cb_stampa_campionamenti=create cb_stampa_campionamenti
this.cb_stampa_test=create cb_stampa_test
this.dw_test_report=create dw_test_report
this.dw_filtro=create dw_filtro
this.dw_campionamenti=create dw_campionamenti
this.dw_folder=create dw_folder
this.dw_test=create dw_test
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.hpb_1
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_cerca
this.Control[iCurrent+5]=this.cb_refresh_campionamenti
this.Control[iCurrent+6]=this.cb_refresh_test
this.Control[iCurrent+7]=this.cb_reset
this.Control[iCurrent+8]=this.cb_salva_test
this.Control[iCurrent+9]=this.cb_stampa_campionamenti
this.Control[iCurrent+10]=this.cb_stampa_test
this.Control[iCurrent+11]=this.dw_test_report
this.Control[iCurrent+12]=this.dw_filtro
this.Control[iCurrent+13]=this.dw_campionamenti
this.Control[iCurrent+14]=this.dw_folder
this.Control[iCurrent+15]=this.dw_test
end on

on w_scadenziario_camp_e_test.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.hpb_1)
destroy(this.st_1)
destroy(this.cb_cerca)
destroy(this.cb_refresh_campionamenti)
destroy(this.cb_refresh_test)
destroy(this.cb_reset)
destroy(this.cb_salva_test)
destroy(this.cb_stampa_campionamenti)
destroy(this.cb_stampa_test)
destroy(this.dw_test_report)
destroy(this.dw_filtro)
destroy(this.dw_campionamenti)
destroy(this.dw_folder)
destroy(this.dw_test)
end on

type cb_1 from commandbutton within w_scadenziario_camp_e_test
integer x = 2377
integer y = 260
integer width = 402
integer height = 112
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

type hpb_1 from hprogressbar within w_scadenziario_camp_e_test
integer x = 46
integer y = 720
integer width = 2537
integer height = 60
boolean bringtotop = true
unsignedinteger maxposition = 100
integer setstep = 1
end type

type st_1 from statictext within w_scadenziario_camp_e_test
integer x = 46
integer y = 640
integer width = 2514
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_cerca from commandbutton within w_scadenziario_camp_e_test
integer x = 1851
integer y = 520
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;dw_filtro.AcceptText()

wf_elabora_campionamenti_2()
//wf_elabora_test()
wf_elabora_test_2()

dw_folder.fu_selecttab(2)

end event

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_filtro
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type cb_refresh_campionamenti from commandbutton within w_scadenziario_camp_e_test
integer x = 434
integer y = 180
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Refresh"
end type

event clicked;dw_folder.fu_selecttab(1)
wf_elabora_campionamenti_2()
dw_folder.fu_selecttab(2)
end event

type cb_refresh_test from commandbutton within w_scadenziario_camp_e_test
integer x = 434
integer y = 180
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Refresh"
end type

event clicked;dw_folder.fu_selecttab(1)
wf_elabora_test_3()
dw_folder.fu_selecttab(3)
end event

type cb_reset from commandbutton within w_scadenziario_camp_e_test
integer x = 1486
integer y = 520
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;datetime ldt_oggi
datetime ldt_oggi_a_x_gg
string ls_null
integer li_Imposta_scadenza = 7

setnull(ls_null)
setnull(ldt_oggi)

dw_filtro.setitem(1,"cod_fornitore",ls_null)
dw_filtro.setitem(1,"cod_prodotto",ls_null)
dw_filtro.setitem(1,"cod_test",ls_null)
dw_filtro.setitem(1,"cod_cat_mer",ls_null)

//ldt_oggi = datetime(today(),time("00:00:00"))
//ldt_oggi_a_x_gg = datetime(RelativeDate(date(ldt_oggi), li_Imposta_scadenza), time("00:00:00"))
//
//dw_filtro.setitem(1,"data_prelievo_da",ldt_oggi)
//dw_filtro.setitem(1,"data_prelievo_a",ldt_oggi_a_x_gg)
dw_filtro.setitem(1,"data_prelievo_da",ldt_oggi)
dw_filtro.setitem(1,"data_prelievo_a",ldt_oggi)
end event

type cb_salva_test from commandbutton within w_scadenziario_camp_e_test
integer x = 823
integer y = 180
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_TotRows, ll_Row
long ll_anno_reg_campionamenti, ll_num_reg_campionamenti, ll_prog_riga_campionamenti
decimal ld_quantita
string ls_flag_esito, ls_cod_valore, ls_flag_altro_valore, ls_altro_valore
boolean lb_commit = true

if g_mb.messagebox("OMNIA", &
							"Vuoi salvare i risultati dei test?",&
							Question!, YesNo!, 1) = 1 then

	dw_test.AcceptText()
	ll_TotRows = dw_test.RowCount()
	for ll_Row = 1 to ll_TotRows
		//elabora la dw external
		//acquisisci i campi chiave
		ll_anno_reg_campionamenti = dw_test.getitemnumber(ll_Row,"anno_reg_campionamenti")
		ll_num_reg_campionamenti = dw_test.getitemnumber(ll_Row,"num_reg_campionamenti")
		ll_prog_riga_campionamenti = dw_test.getitemnumber(ll_Row,"prog_riga_campionamenti")
		//acquisisci i valori
		ld_quantita = dw_test.getitemdecimal(ll_Row,"quantita")
		ls_flag_esito = dw_test.getitemstring(ll_Row,"flag_esito")
		ls_cod_valore = dw_test.getitemstring(ll_Row,"cod_valore")
		ls_flag_altro_valore = dw_test.getitemstring(ll_Row,"flag_altro_valore")
		ls_altro_valore = dw_test.getitemstring(ll_Row,"altro_valore")
		
		if ls_flag_altro_valore = "" then
			setnull(ls_altro_valore)
		end if
		
		UPDATE det_campionamenti
		SET
			 quantita 				= :ld_quantita
			,flag_esito 			= :ls_flag_esito
			,cod_valore 			= :ls_cod_valore
			,flag_altro_valore 	= :ls_flag_altro_valore
			,altro_valore 			= :ls_altro_valore
		WHERE cod_azienda = :s_cs_xx.cod_azienda
			AND anno_reg_campionamenti = :ll_anno_reg_campionamenti
			AND num_reg_campionamenti = :ll_num_reg_campionamenti
			AND prog_riga_campionamenti = :ll_prog_riga_campionamenti
		;
		
		if SQLCA.SQLCode <> 0 then
			g_mb.messagebox("OMNIA", &
											"Errore durante il salvataggio dei dati. Dettaglio:"+ SQLCA.SQLErrtext, &
											StopSign!)
			rollback;
			lb_commit = false
			exit
		end if
	next
	
	if lb_commit then 
		COMMIT;
		
		if SQLCA.SQLCode = 0 then
			g_mb.messagebox("OMNIA", &
											"Salvataggio eseguito con successo!", &
											Information!)		
		else
			g_mb.messagebox("OMNIA", &
											"Errore durante la conferma dei dati. Dettaglio:"+ SQLCA.SQLErrtext, &
											StopSign!)
		end if
	end if
end if
end event

type cb_stampa_campionamenti from commandbutton within w_scadenziario_camp_e_test
integer x = 46
integer y = 180
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_campionamenti.print(true, true)
end event

type cb_stampa_test from commandbutton within w_scadenziario_camp_e_test
integer x = 46
integer y = 180
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_test_report.print(true, true)
end event

type dw_test_report from datawindow within w_scadenziario_camp_e_test
boolean visible = false
integer x = 2377
integer y = 1640
integer width = 869
integer height = 780
integer taborder = 40
boolean bringtotop = true
string title = "none"
string dataobject = "d_report_test_con_scadenza"
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_filtro from u_dw_search within w_scadenziario_camp_e_test
integer x = 23
integer y = 140
integer width = 2583
integer height = 660
integer taborder = 70
string dataobject = "d_scadenziario_camp_e_test_filtro"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_filtro,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_filtro,"cod_prodotto")
end choose
end event

type dw_campionamenti from datawindow within w_scadenziario_camp_e_test
integer x = 23
integer y = 280
integer width = 3566
integer height = 2540
integer taborder = 40
string title = "none"
string dataobject = "d_report_campionamenti_scadenze_aut2"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event doubleclicked;s_campionamento_aut ls_campionamento_aut
datetime ldt_data_validita
long ll_prog_piano_campionamento
string ls_cod_piano_campionamento

if row > 0 then
	ls_cod_piano_campionamento = getitemstring(row,"cod_piano_campionamento")
	if isnull(ls_cod_piano_campionamento) or ls_cod_piano_campionamento="" then return
	
	ldt_data_validita = getitemdatetime(row,"data_validita")
	if isnull(ldt_data_validita) then return
	
	ll_prog_piano_campionamento = getitemnumber(row,"prog_piano_campionamento")
	if isnull(ll_prog_piano_campionamento) or ll_prog_piano_campionamento <=0 then return
	
	ls_campionamento_aut.s_cod_piano_campionamento = ls_cod_piano_campionamento
	ls_campionamento_aut.dt_data_validita = ldt_data_validita
	ls_campionamento_aut.l_prog_piano_campionamento = ll_prog_piano_campionamento
	
	openwithparm(w_test_campionamenti_aut, ls_campionamento_aut)
end if
end event

type dw_folder from u_folder within w_scadenziario_camp_e_test
integer width = 3589
integer height = 2840
integer taborder = 20
end type

type dw_test from datawindow within w_scadenziario_camp_e_test
integer x = 23
integer y = 280
integer width = 3566
integer height = 2540
integer taborder = 60
string title = "none"
string dataobject = "d_griglia_test_con_scadenza"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;string ls_flag_altro_valore, ls_altro_valore, ls_cod_test,ls_cod_prodotto, ls_null
s_tab_test_valori_griglia ls_tab_test_valori_griglia
decimal ld_null
datetime ldt_data_validita
s_test_prodotti ls_test_prodotti

setnull(ls_null)
setnull(ld_null)

if row > 0 then
	AcceptText()
	this.setrow(row)
	choose case dwo.name
		case "b_consulta"						
			ls_cod_prodotto = getitemstring(row,"cod_prodotto")
			ls_cod_test = getitemstring(row,"cod_test")
			ldt_data_validita = getitemdatetime(row, "data_validita")
			
			ls_test_prodotti.s_cod_prodotto = ls_cod_prodotto
			ls_test_prodotti.s_cod_test = ls_cod_test
			ls_test_prodotti.dt_validita = ldt_data_validita
			openwithparm(w_consulta_test_prodotto, ls_test_prodotti)
			
		case "b_cod_valore"
			ls_cod_test = getitemstring(row,"cod_test")
			ls_tab_test_valori_griglia.s_cod_test = ls_cod_test
			ls_tab_test_valori_griglia.s_cod_valore = ""
			ls_tab_test_valori_griglia.s_desc_valore = ""
			openwithparm(w_inserisci_valori_per_test, ls_tab_test_valori_griglia)
			
			ls_tab_test_valori_griglia = Message.PowerObjectParm
			if ls_tab_test_valori_griglia.s_cod_valore = "cs_team_esc_pressed" then				
			else
				setitem(row, "cod_valore", ls_tab_test_valori_griglia.s_cod_valore)
				setitem(row, "des_valore", ls_tab_test_valori_griglia.s_desc_valore)
				//annulla gli altri valori
				setitem(row,"flag_esito", "I")
				setitem(row,"quantita",ld_null)
				setitem(row,"flag_altro_valore","N")
				setitem(row,"altro_valore","")
				setitem(row, "fl", "N")
			end if
			
		case "flag_altro_valore"
			ls_flag_altro_valore = getitemstring(row,"flag_altro_valore")
			if ls_flag_altro_valore = "N" then setitem(row,"altro_valore","")
			
		case "b_altro"
			ls_flag_altro_valore = getitemstring(row,"flag_altro_valore")
			if ls_flag_altro_valore = "N" then 
				if g_mb.messagebox("OMNIA", "Inserire un altro valore?", Question!, YesNo!, 1) = 1 then
					setitem(row,"flag_altro_valore","S")
					ls_flag_altro_valore = "S"
				else
					ls_flag_altro_valore = "N"
				end if
			end if
			
			if ls_flag_altro_valore = "S" then 	
				ls_altro_valore = getitemstring(row,"altro_valore")
				if isnull(ls_altro_valore) then ls_altro_valore = ""
				openwithparm(w_inserisci_altro_valore,ls_altro_valore)
				
				ls_altro_valore = Message.StringParm
				if ls_altro_valore <> "cs_team_esc_pressed" then 
					setitem(row,"altro_valore",ls_altro_valore)
					//annulla tutti gli altri valori
					setitem(row,"flag_esito", "I")
					setitem(row,"cod_valore",ls_null)
					setitem(row,"des_valore",ls_null)
					setitem(row,"quantita",ld_null)
					setitem(row, "fl", "N")
				end if
			end if
	end choose
end if
end event

event itemchanged;decimal ld_null, ldc_tol_minima, ldc_tol_massima
string ls_null, ls_cod_prodotto, ls_cod_test
datetime ldt_data_validita
integer li_return

setnull(ld_null)
setnull(ls_null)

if row > 0 then this.setrow(row)

choose case dwo.name
	case "quantita"
		//annulla gli altri valori
		setitem(row,"flag_esito", "I")
		setitem(row,"cod_valore",ls_null)
		setitem(row,"des_valore",ls_null)
		//setitem(row,"quantita",ld_null)
		setitem(row,"flag_altro_valore","N")
		setitem(row,"altro_valore","")
		
		//controlla gli eventuali limiti
		ls_cod_prodotto = getitemstring(row,"cod_prodotto")
		ls_cod_test = getitemstring(row,"cod_test")
		ldt_data_validita = getitemdatetime(row, "data_validita")
		li_return = wf_controlla_tolleranza(ls_cod_prodotto, ls_cod_test, ldt_data_validita, dec(data))
		//test sul ritorno: se 0 OK, se 1 è inferiore al MIN, se 2 è superiore al MAX
		if li_return <> 0 then
			setitem(row, "fl", "S")
		else
			setitem(row, "fl", "N")
		end if
		
	case "flag_esito"		
		if data <> "I" then
			//annulla gli altri valori
			setitem(row,"quantita",ld_null)
			setitem(row,"cod_valore",ls_null)
			setitem(row,"des_valore",ls_null)
			setitem(row,"flag_altro_valore","N")
			setitem(row,"altro_valore","")
			setitem(row, "fl", "N")
		end if
				
end choose
end event

