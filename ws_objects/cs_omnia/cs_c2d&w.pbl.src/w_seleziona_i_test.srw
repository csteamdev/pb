﻿$PBExportHeader$w_seleziona_i_test.srw
forward
global type w_seleziona_i_test from window
end type
type dw_test from datawindow within w_seleziona_i_test
end type
type cb_esci from commandbutton within w_seleziona_i_test
end type
type cb_ok from commandbutton within w_seleziona_i_test
end type
type cb_deseleziona_tutti from commandbutton within w_seleziona_i_test
end type
type cb_seleziona_tutti from commandbutton within w_seleziona_i_test
end type
end forward

global type w_seleziona_i_test from window
integer width = 2720
integer height = 2012
boolean titlebar = true
string title = "Seleziona tra i test disponibili"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_test dw_test
cb_esci cb_esci
cb_ok cb_ok
cb_deseleziona_tutti cb_deseleziona_tutti
cb_seleziona_tutti cb_seleziona_tutti
end type
global w_seleziona_i_test w_seleziona_i_test

type variables
s_campionamento_aut is_campionamento_aut
end variables

on w_seleziona_i_test.create
this.dw_test=create dw_test
this.cb_esci=create cb_esci
this.cb_ok=create cb_ok
this.cb_deseleziona_tutti=create cb_deseleziona_tutti
this.cb_seleziona_tutti=create cb_seleziona_tutti
this.Control[]={this.dw_test,&
this.cb_esci,&
this.cb_ok,&
this.cb_deseleziona_tutti,&
this.cb_seleziona_tutti}
end on

on w_seleziona_i_test.destroy
destroy(this.dw_test)
destroy(this.cb_esci)
destroy(this.cb_ok)
destroy(this.cb_deseleziona_tutti)
destroy(this.cb_seleziona_tutti)
end on

event open;string ls_cod_piano_campionamento, ls_cod_prodotto
datetime ldt_data_validita
long ll_prog_piano_campionamento, ll_rows
is_campionamento_aut = message.powerobjectparm

ls_cod_piano_campionamento = is_campionamento_aut.s_cod_piano_campionamento
ldt_data_validita = is_campionamento_aut.dt_data_validita
ll_prog_piano_campionamento = is_campionamento_aut.l_prog_piano_campionamento
ls_cod_prodotto = is_campionamento_aut.s_cod_prodotto

dw_test.settransobject(sqlca)
ll_rows = dw_test.retrieve(s_cs_xx.cod_azienda, ls_cod_piano_campionamento, ldt_data_validita, ll_prog_piano_campionamento,ls_cod_prodotto)

if ll_rows > 0 then
	cb_seleziona_tutti.enabled = true
	cb_deseleziona_tutti.enabled = true
	cb_ok.enabled = true
	cb_esci.enabled = true
else
	cb_seleziona_tutti.enabled = false
	cb_deseleziona_tutti.enabled = false
	cb_ok.enabled = false
	cb_esci.enabled = true
end if
end event

type dw_test from datawindow within w_seleziona_i_test
integer x = 23
integer y = 140
integer width = 2674
integer height = 1780
integer taborder = 40
string title = "none"
string dataobject = "d_seleziona_i_test"
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_esci from commandbutton within w_seleziona_i_test
integer x = 1326
integer y = 40
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;s_seleziona_i_test ls_seleziona_i_test

ls_seleziona_i_test.s_ok = "N"
closewithreturn(parent, ls_seleziona_i_test)
end event

type cb_ok from commandbutton within w_seleziona_i_test
integer x = 937
integer y = 40
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
boolean default = true
end type

event clicked;s_seleziona_i_test ls_seleziona_i_test
long ll_index, ll_rows, ll_array
string ls_sel
string ls_almeno_uno = "N"

ll_rows = dw_test.rowcount()
dw_test.accepttext()
ll_array = 0
for ll_index = 1 to ll_rows
	ls_sel = dw_test.getitemstring(ll_index, "sel")
	if ls_sel = "S" then
		ll_array += 1
		ls_almeno_uno = "S"
		ls_seleziona_i_test.s_cod_test[ll_array] = dw_test.getitemstring(ll_index, "cod_test")
		ls_seleziona_i_test.s_oggetto_test[ll_array] = dw_test.getitemstring(ll_index, "oggetto_test")
		ls_seleziona_i_test.s_cod_misura[ll_array] = dw_test.getitemstring(ll_index, "cod_misura")
		ls_seleziona_i_test.s_cod_prodotto[ll_array] = dw_test.getitemstring(ll_index, "cod_prodotto")
		ls_seleziona_i_test.s_flag_tipo_quantita[ll_array] = dw_test.getitemstring(ll_index, "flag_tipo_quantita")
		ls_seleziona_i_test.dc_numerosita_campionaria[ll_array] = dw_test.getitemdecimal(ll_index, "numerosita_campionaria")
		ls_seleziona_i_test.dc_percentuale_campionaria[ll_array] = dw_test.getitemdecimal(ll_index, "percentuale_campionaria")
		ls_seleziona_i_test.dc_livello_significativita[ll_array] = dw_test.getitemdecimal(ll_index, "livello_significativita")
		ls_seleziona_i_test.s_modalita_campionamento[ll_array] = dw_test.getitemstring(ll_index, "modalita_campionamento")
		ls_seleziona_i_test.s_flag_direzione[ll_array] = dw_test.getitemstring(ll_index, "flag_direzione")
		ls_seleziona_i_test.s_des_test[ll_array] = dw_test.getitemstring(ll_index, "des_test")
	end if
next
ls_seleziona_i_test.s_ok = ls_almeno_uno

closewithreturn(parent, ls_seleziona_i_test)
end event

type cb_deseleziona_tutti from commandbutton within w_seleziona_i_test
integer x = 549
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nessuno"
end type

event clicked;long ll_rows, ll_index

ll_rows = dw_test.rowcount()

for ll_index = 1 to ll_rows
	dw_test.setitem(ll_index, "sel", "N")
next
end event

type cb_seleziona_tutti from commandbutton within w_seleziona_i_test
integer x = 160
integer y = 40
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;long ll_rows, ll_index

ll_rows = dw_test.rowcount()

for ll_index = 1 to ll_rows
	dw_test.setitem(ll_index, "sel", "S")
next
end event

