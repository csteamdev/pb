﻿$PBExportHeader$w_statistiche_laboratorio.srw
forward
global type w_statistiche_laboratorio from w_cs_xx_principale
end type
type st_val_atteso from statictext within w_statistiche_laboratorio
end type
type st_val_medio from statictext within w_statistiche_laboratorio
end type
type st_tol_max from statictext within w_statistiche_laboratorio
end type
type st_tol_min from statictext within w_statistiche_laboratorio
end type
type dw_graph_fuori_limite from uo_cs_graph within w_statistiche_laboratorio
end type
type dw_graph_analitico from uo_cs_graph within w_statistiche_laboratorio
end type
type st_status from statictext within w_statistiche_laboratorio
end type
type hpb_1 from hprogressbar within w_statistiche_laboratorio
end type
type dw_folder from u_folder within w_statistiche_laboratorio
end type
type dw_folder_search from u_folder within w_statistiche_laboratorio
end type
type dw_test from datawindow within w_statistiche_laboratorio
end type
type dw_prodotti_validita from datawindow within w_statistiche_laboratorio
end type
type dw_filtro from u_dw_search within w_statistiche_laboratorio
end type
type cb_reset from commandbutton within w_statistiche_laboratorio
end type
type cb_cerca from commandbutton within w_statistiche_laboratorio
end type
type dw_graph_popolazione from uo_cs_graph within w_statistiche_laboratorio
end type
end forward

global type w_statistiche_laboratorio from w_cs_xx_principale
integer width = 3305
integer height = 3336
string title = "Statistiche di Laboratorio"
event ue_graph_create pbm_dwngraphcreate
st_val_atteso st_val_atteso
st_val_medio st_val_medio
st_tol_max st_tol_max
st_tol_min st_tol_min
dw_graph_fuori_limite dw_graph_fuori_limite
dw_graph_analitico dw_graph_analitico
st_status st_status
hpb_1 hpb_1
dw_folder dw_folder
dw_folder_search dw_folder_search
dw_test dw_test
dw_prodotti_validita dw_prodotti_validita
dw_filtro dw_filtro
cb_reset cb_reset
cb_cerca cb_cerca
dw_graph_popolazione dw_graph_popolazione
end type
global w_statistiche_laboratorio w_statistiche_laboratorio

type variables
boolean ib_ricerca = true
end variables

forward prototypes
public function long wf_ricerca ()
public function long wf_analitico (string fs_cod_prodotto, string fs_cod_test, datetime fd_data_validita)
public function long wf_retrieve_test (string fs_cod_prodotto, datetime fd_data_validita, ref string fs_cod_test)
end prototypes

event ue_graph_create;/*
These statements in the Clicked event of the DataWindow control dw_employees 
store the style of the series under the pointer in the graph gr_depts in the variable style_type. 
If the style of the series is overlay (true), the script changes the style to normal (false):
*/
string ls_SeriesName
integer li_SeriesNbr, li_Data_Point, li_ret
boolean lb_overlay_style

grObjectType MouseHit

//MouseHit = dw_graph_popolazione.ObjectAtPointer("gr_cs_graph", ls_SeriesNbr, li_Data_Point)
//
//if MouseHit = TypeSeries! then
//	ls_SeriesName = dw_graph_popolazione.SeriesName("gr_cs_graph",li_SeriesNbr)
//	dw_graph_popolazione.GetSeriesStyle("gr_cs_graph", ls_SeriesName, lb_overlay_style)
//
//	if overlay_style then 
//            dw_graph_popolazione.SetSeriesStyle("gr_cs_graph", ls_SeriesName, false)
//	end if
//
//end  if

ls_SeriesName = "A"
li_ret = dw_graph_popolazione.SetSeriesStyle("gr_cs_graph", ls_SeriesName, true)

li_ret = 0
end event

public function long wf_ricerca ();string ls_cod_prodotto_ricerca, ls_cod_test_ricerca, ls_cod_fornitore_ricerca
datetime ldt_data_inizio_ricerca, ldt_data_fine_ricerca
string ls_sql, ls_errore, ls_sintassi
datastore lds_prodotti
long ll_righe, ll_index, ll_riga, ll_ret

setpointer(HourGlass!)

//POPOLAMENTO DEI PRODOTTI #################################
ls_sql = "SELECT DISTINCT " +&
				 "tab_test_prodotti.cod_prodotto" + &
				",tab_test_prodotti.data_validita" + &
				",anag_prodotti.des_prodotto" + &				
			" " + &
        		"FROM tab_test_prodotti " + &
        		"JOIN anag_prodotti ON anag_prodotti.cod_azienda=tab_test_prodotti.cod_azienda " + &
				  										" AND anag_prodotti.cod_prodotto=tab_test_prodotti.cod_prodotto " + &
			"JOIN tes_campionamenti ON tes_campionamenti.cod_azienda=tab_test_prodotti.cod_azienda " + &
				  										" AND tes_campionamenti.cod_prodotto=tab_test_prodotti.cod_prodotto " + &
			"JOIN det_campionamenti ON det_campionamenti.cod_azienda=tes_campionamenti.cod_azienda " + &
				  										" AND det_campionamenti.anno_reg_campionamenti=tes_campionamenti.anno_reg_campionamenti " + &
														  " AND det_campionamenti.num_reg_campionamenti=tes_campionamenti.num_reg_campionamenti " + &
														  " AND det_campionamenti.cod_azienda=tab_test_prodotti.cod_azienda " + &
														  " AND det_campionamenti.cod_prodotto=tab_test_prodotti.cod_prodotto " + &
														  " AND det_campionamenti.cod_test=tab_test_prodotti.cod_test " + &
        	"  WHERE tab_test_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			  
ls_cod_prodotto_ricerca = dw_filtro.getitemstring(1, "rs_cod_prodotto")
if not isnull(ls_cod_prodotto_ricerca) and ls_cod_prodotto_ricerca<>"" then
	ls_sql += " AND tab_test_prodotti.cod_prodotto = '"+ls_cod_prodotto_ricerca+"' "
end if

ls_cod_test_ricerca = dw_filtro.getitemstring(1, "rs_cod_test")
if not isnull(ls_cod_test_ricerca) and ls_cod_test_ricerca<>"" then
	ls_sql += " AND tab_test_prodotti.cod_test = '"+ls_cod_test_ricerca+"' "
end if

ls_cod_fornitore_ricerca = dw_filtro.getitemstring(1, "rs_cod_fornitore")
if not isnull(ls_cod_fornitore_ricerca) and ls_cod_fornitore_ricerca<>"" then
	ls_sql += " AND tes_campionamenti.cod_fornitore = '"+ls_cod_fornitore_ricerca+"' "
end if
			
ldt_data_inizio_ricerca = dw_filtro.getitemdatetime(1, "rdt_data_inizio")
ldt_data_fine_ricerca = dw_filtro.getitemdatetime(1, "rdt_data_fine")
if not isnull(ldt_data_inizio_ricerca) then
	ls_sql += " AND tab_test_prodotti.data_validita >= '" + string( ldt_data_inizio_ricerca, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_fine_ricerca) then
	ls_sql += " AND tab_test_prodotti.data_validita <= '" + string( ldt_data_fine_ricerca, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += " ORDER BY tab_test_prodotti.cod_prodotto,tab_test_prodotti.data_validita DESC "

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

if Len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
	setpointer(Arrow!)
   return -1
end if

lds_prodotti = create datastore

lds_prodotti.Create( ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	destroy lds_prodotti;
	setpointer(Arrow!)
   return -1
end if

lds_prodotti.settransobject(sqlca)

ll_righe = lds_prodotti.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe dei prodotti:" + sqlca.sqlerrtext)
	destroy lds_prodotti;
	setpointer(Arrow!)
	return -1
end if

dw_prodotti_validita.settransobject(SQLCA)
dw_prodotti_validita.reset()
for ll_index = 1 to ll_righe
	//inserisci la riga nella dw external dei prodotti
	ll_riga = dw_prodotti_validita.insertrow(0)
	dw_prodotti_validita.setitem(ll_riga, "cod_prodotto", lds_prodotti.getitemstring(ll_index, 1)) //tab_test_prodotti_cod_prodotto
	dw_prodotti_validita.setitem(ll_riga, "data_validita", lds_prodotti.getitemdatetime(ll_index, 2)) //tab_test_prodotti_data_validita
	dw_prodotti_validita.setitem(ll_riga, "des_prodotto", lds_prodotti.getitemstring(ll_index, 3)) //anag_prodotti_des_prodotto
next
destroy lds_prodotti;
//######################################################################

//POPOLAMENTO DEI TEST ################################################
dw_test.settransobject(SQLCA)
dw_test.reset()
if ll_righe > 0 then
	dw_prodotti_validita.selectrow(0, false)
	dw_prodotti_validita.selectrow(1, true)
	
	ll_ret = wf_retrieve_test(dw_prodotti_validita.getitemstring(1,"cod_prodotto"),dw_prodotti_validita.getitemdatetime(1,"data_validita"), ls_cod_test_ricerca)
end if
//######################################################################


setpointer(arrow!)
return ll_ret
end function

public function long wf_analitico (string fs_cod_prodotto, string fs_cod_test, datetime fd_data_validita);string  ls_row_number, ls_cod_fornitore
string ls_sql, ls_errore, ls_sintassi, ls_des_prodotto, ld_des_test
long ll_righe, ll_index, ll_riga, ll_rows
decimal ld_valore, ld_valore_medio
decimal ld_min, ld_max, ld_ampiezza
decimal ld_indice_min, ld_indice_max, ld_indice
decimal ld_dev_std, ld_vettore_conteggio[]
string ls_vettore_ampiezze[], ls_ampiezza, ls_tipo
long ll_index2, ll_count_fuori_limite, ll_count_conforme
decimal ld_toll_max, ld_toll_min
long ll_anno_campione_da, ll_num_campione_da, ll_anno_campione_a, ll_num_campione_a
datastore lds_test, ldw_analitico, lds_gauss

datastore lds_graph_analitico, lds_graph_popolazione, lds_graph_fuori_limite

setpointer(HourGlass!)

dw_graph_analitico.reset()
dw_graph_popolazione.reset()
dw_graph_fuori_limite.reset()

hpb_1.Position = 0
st_status.text = "preparazione della Query in corso ..."

ls_sql = "SELECT " +&
				 "tes_campionamenti.anno_reg_campionamenti" + &
				",tes_campionamenti.num_reg_campionamenti" + &
				",det_campionamenti.prog_riga_campionamenti" + &
				",tab_test_prodotti.cod_prodotto" + &
				",tab_test_prodotti.data_validita" + &
				",anag_prodotti.des_prodotto " +&
				",tab_test.des_test " +&
				",det_campionamenti.quantita" + &
				",tab_test_prodotti.tol_minima" + &
				",tab_test_prodotti.tol_massima" + &
				",tab_test_prodotti.val_medio" + &
			" " + &
        		"FROM det_campionamenti " + &
        		"JOIN tes_campionamenti ON tes_campionamenti.cod_azienda=det_campionamenti.cod_azienda " + &
				  										" AND tes_campionamenti.anno_reg_campionamenti=det_campionamenti.anno_reg_campionamenti " + &
														  " AND tes_campionamenti.num_reg_campionamenti=det_campionamenti.num_reg_campionamenti " + &
			"JOIN anag_prodotti ON anag_prodotti.cod_azienda= tes_campionamenti.cod_azienda " + &
														" AND anag_prodotti.cod_prodotto=tes_campionamenti.cod_prodotto " + &
			"JOIN tab_test_prodotti ON tab_test_prodotti.cod_prodotto=tes_campionamenti.cod_prodotto " + &
														" AND tab_test_prodotti.cod_test=det_campionamenti.cod_test " + &
														" AND tab_test_prodotti.data_validita='" + string(fd_data_validita, s_cs_xx.db_funzioni.formato_data) + "' " + &
			"JOIN tab_test ON tab_test.cod_test= tab_test_prodotti.cod_test " + &
        	"  WHERE det_campionamenti.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			  " AND tes_campionamenti.cod_prodotto='"+fs_cod_prodotto+"' " + &
			  " AND det_campionamenti.cod_test='"+fs_cod_test+"' " + &
			  " AND tab_test_prodotti.data_validita='" + string(fd_data_validita, s_cs_xx.db_funzioni.formato_data) + "' "+ &
			  " AND det_campionamenti.quantita<>0 AND det_campionamenti.quantita IS NOT NULL "


ls_cod_fornitore = dw_filtro.getitemstring(1, "rs_cod_fornitore")
if not isnull(ls_cod_fornitore) and ls_cod_fornitore<>"" then
	ls_sql += " AND tes_campionamenti.cod_fornitore = '"+ls_cod_fornitore+"' "
end if

ll_anno_campione_da = dw_filtro.getitemnumber(1, "rl_anno_campione_da")
ll_anno_campione_a = dw_filtro.getitemnumber(1, "rl_anno_campione_a")
ll_num_campione_da = dw_filtro.getitemnumber(1, "rl_num_campione_da")
ll_num_campione_a = dw_filtro.getitemnumber(1, "rl_num_campione_a")

if ll_anno_campione_da > 0 then
	ls_sql += " AND tes_campionamenti.anno_reg_campionamenti >= " + string(ll_anno_campione_da)
end if
if ll_anno_campione_a > 0 then
	ls_sql += " AND tes_campionamenti.anno_reg_campionamenti <= " + string(ll_anno_campione_a)
end if
if ll_num_campione_da > 0 then
	ls_sql += " AND tes_campionamenti.num_reg_campionamenti >= " + string(ll_num_campione_da)
end if
if ll_num_campione_a > 0 then
	ls_sql += " AND tes_campionamenti.num_reg_campionamenti <= " + string(ll_num_campione_a)
end if

ls_sql += " ORDER BY tes_campionamenti.anno_reg_campionamenti,tes_campionamenti.num_reg_campionamenti,det_campionamenti.prog_riga_campionamenti "

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

if Len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
	st_status.text = ""
	setpointer(Arrow!)
   return -1
end if

lds_test = create datastore

lds_test.create( ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	st_status.text = ""
	destroy lds_test;
	setpointer(Arrow!)
   return -1
end if

lds_test.settransobject(sqlca)

ll_righe = lds_test.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe dei test:" + sqlca.sqlerrtext)
	st_status.text = ""
	destroy lds_test;
	setpointer(Arrow!)
	return -1
end if

lds_graph_analitico = create datastore
lds_graph_popolazione = create datastore
lds_graph_fuori_limite = create datastore

lds_graph_analitico.dataobject = "d_grafico_test_statistiche"
lds_graph_popolazione.dataobject = "d_gaussiana"
lds_graph_fuori_limite.dataobject = "d_ext_fuorilimite_statistiche"

lds_gauss = create datastore
lds_gauss.dataobject = "d_ds_popolazione_statistica"
lds_gauss.settransobject(SQLCA)

st_status.text = "Inizio elaborazione Dati ..."
//calcola il valore medio
ld_valore_medio = 0
for ll_index = 1 to ll_righe
	ld_valore_medio += lds_test.getitemdecimal(ll_index, 8)//"det_campionamenti_quantita")
next
if ll_righe > 0 then ld_valore_medio = ld_valore_medio/ll_righe

ll_count_fuori_limite = 0
ll_count_conforme = 0
hpb_1.maxposition = ll_righe
hpb_1.setstep = 1

for ll_index = 1 to ll_righe
	ls_des_prodotto = lds_test.getitemstring(ll_index, 6) //"anag_prodotti_des_prodotto")
	ld_des_test = lds_test.getitemstring(ll_index, 7) //"tab_test_des_test")
	
	//VALORE-------------------------------------------------------------
	ll_riga = lds_graph_analitico.insertrow(0)
	
	//ls_row_number del tipo YYYY/nnnnnn-NN
	/*
	ls_row_number = string(lds_test.getitemnumber(ll_index,"tes_campionamenti_anno_reg_campionamenti")) + &
								"/" + &
								right( &
										"000000" + &
										string(lds_test.getitemnumber(ll_index,"tes_campionamenti_num_reg_campionamenti")) &
								,6) + &
								"-" + &
								string(lds_test.getitemnumber(ll_index,"det_campionamenti_prog_riga_campionamenti"))
	*/
	ls_row_number = string(lds_test.getitemnumber(ll_index, 1)) + &
								"/" + &
								right( &
										"000000" + &
										string(lds_test.getitemnumber(ll_index, 2)) &
								,6) + &
								"-" + &
								string(lds_test.getitemnumber(ll_index, 3))
	
	lds_graph_analitico.setitem(ll_riga, "prodotto", ls_des_prodotto)
	lds_graph_analitico.setitem(ll_riga, "test", ld_des_test)
	
	ld_valore = lds_test.getitemdecimal(ll_index, 8) //"det_campionamenti_quantita")
	lds_graph_analitico.setitem(ll_riga, "quantita", ld_valore)
	lds_graph_analitico.setitem(ll_riga, "tipo", "Test")
	lds_graph_analitico.setitem(ll_riga, "rownumber", ls_row_number)	
	
	//VAL.MEDIO-------------------------------------------------------------
	ll_riga = lds_graph_analitico.insertrow(0)		
	lds_graph_analitico.setitem(ll_riga, "prodotto", ls_des_prodotto)
	lds_graph_analitico.setitem(ll_riga, "test", ld_des_test)
	
	if isnull(ld_valore_medio) then ld_valore_medio = 0
	lds_graph_analitico.setitem(ll_riga, "quantita", ld_valore_medio)
	if ld_valore_medio > 0 then 
		st_val_medio.text = "Val.Medio: "+ string(round(ld_valore_medio, 4))
	else
		st_val_medio.text = "Val.Medio:"
	end if
	lds_graph_analitico.setitem(ll_riga, "tipo", "Val.Medio")
	lds_graph_analitico.setitem(ll_riga, "rownumber", ls_row_number)	
	
	//MIN-------------------------------------------------------------
	ll_riga = lds_graph_analitico.insertrow(0)		
	lds_graph_analitico.setitem(ll_riga, "prodotto", ls_des_prodotto)
	lds_graph_analitico.setitem(ll_riga, "test", ld_des_test)
	
	ld_valore = lds_test.getitemdecimal(ll_index, 9) //"tab_test_prodotti_tol_minima")
	if isnull(ld_valore) then ld_valore = 0
	lds_graph_analitico.setitem(ll_riga, "quantita", ld_valore)
	if ld_valore > 0 then 
		//dw_analitico.object.t_min.text = "Val.Min: "+ string(round(ld_valore, 4))
		st_tol_min.text = "Val.Min: "+ string(round(ld_valore, 4))
	else
		//dw_analitico.object.t_min.text = "Val.Min:"
		st_tol_min.text = "Val.Min: "
	end if	
	lds_graph_analitico.setitem(ll_riga, "tipo", "Val.Min.")
	lds_graph_analitico.setitem(ll_riga, "rownumber", ls_row_number)	
	
	//MAX-------------------------------------------------------------
	ll_riga = lds_graph_analitico.insertrow(0)		
	lds_graph_analitico.setitem(ll_riga, "prodotto", ls_des_prodotto)
	lds_graph_analitico.setitem(ll_riga, "test", ld_des_test)
	
	ld_valore = lds_test.getitemdecimal(ll_index, 10) //"tab_test_prodotti_tol_massima")
	if isnull(ld_valore) then ld_valore = 0
	lds_graph_analitico.setitem(ll_riga, "quantita", ld_valore)
	if ld_valore > 0 then 
		st_tol_max.text = "Val.Max: "+ string(round(ld_valore, 4))
	else
		st_tol_max.text = "Val.Max: "
	end if	
	lds_graph_analitico.setitem(ll_riga, "tipo", "Val.Max")
	lds_graph_analitico.setitem(ll_riga, "rownumber", ls_row_number)
	
	//VAL.ATTESO-------------------------------------------------------------
	ll_riga = lds_graph_analitico.insertrow(0)		
	lds_graph_analitico.setitem(ll_riga, "prodotto", ls_des_prodotto)
	lds_graph_analitico.setitem(ll_riga, "test", ld_des_test)
	
	ld_valore = lds_test.getitemdecimal(ll_index, 11)// "tab_test_prodotti_val_medio")
	if isnull(ld_valore) then ld_valore = 0
	lds_graph_analitico.setitem(ll_riga, "quantita", ld_valore)
	if ld_valore > 0 then 
		st_val_atteso.text = "Val.Atteso: "+ string(round(ld_valore, 4))
	else
		st_val_atteso.text = "Val.Atteso:"
	end if	
	lds_graph_analitico.setitem(ll_riga, "tipo", "Val.Atteso")
	lds_graph_analitico.setitem(ll_riga, "rownumber", ls_row_number)	
	
	//datastore per il grafico della popolazione
	ld_valore = lds_test.getitemdecimal(ll_index, 8)//"det_campionamenti_quantita")
	ll_riga = lds_gauss.insertrow(0)
	lds_gauss.setitem(ll_riga, "row_number", ll_riga)
	lds_gauss.setitem(ll_riga, "row_value", ld_valore)
	
	//conteggio per il grafico dei fuori limite ------------------------------------------------
	ld_valore = lds_test.getitemdecimal(ll_index, 8)//"det_campionamenti_quantita")
	ld_toll_max = lds_test.getitemdecimal(ll_index, 10)//"tab_test_prodotti_tol_massima")
	ld_toll_min = lds_test.getitemdecimal(ll_index, 9)//"tab_test_prodotti_tol_minima")
	if not isnull(ld_toll_max) and ld_toll_max>0 then
		//definito il limite superiore
		if not isnull(ld_toll_min) and ld_toll_min > 0 then
			//definito anche il limite inferiore			
			if (ld_valore >= ld_toll_min) and (ld_valore <= ld_toll_max) then
				//conforme
				ll_count_conforme += 1
			else
				//non conforme
				ll_count_fuori_limite += 1
			end if
		else
			//definito solo il limite superiore
			if (ld_valore <= ld_toll_max) then
				//conforme
				ll_count_conforme += 1
			else
				//non conforme
				ll_count_fuori_limite += 1
			end if
		end if
	else
		if not isnull(ld_toll_min) and ld_toll_min > 0 then
			//definito solo il limite inferiore
			if (ld_valore >= ld_toll_min) then
				//conforme
				ll_count_conforme += 1
			else
				//non conforme
				ll_count_fuori_limite += 1
			end if
		else
			//nessun limite definito: conforme
			ll_count_conforme += 1
		end if
	end if	
	//-------------------------------------------------------------
	hpb_1.Stepit()
next

st_status.text = ""
hpb_1.position = 0

if ll_count_fuori_limite + ll_count_conforme > 0 then
	//preparo il grafico a torta dei fuori limite
	ll_riga = lds_graph_fuori_limite.insertrow(0)
	lds_graph_fuori_limite.setitem(ll_riga, "prodotto", ls_des_prodotto)
	lds_graph_fuori_limite.setitem(ll_riga, "test", ld_des_test)
	lds_graph_fuori_limite.setitem(ll_riga, "data_validita", fd_data_validita)
	lds_graph_fuori_limite.setitem(ll_riga, "categoria", "1.Non Conformi")
	lds_graph_fuori_limite.setitem(ll_riga, "valore", ll_count_fuori_limite)
	lds_graph_fuori_limite.setitem(ll_riga, "totale", ll_count_fuori_limite + ll_count_conforme)
	lds_graph_fuori_limite.setitem(ll_riga, "tipo", 'a) Fuori limite')	
		
	ll_riga = lds_graph_fuori_limite.insertrow(0)
	lds_graph_fuori_limite.setitem(ll_riga, "prodotto", ls_des_prodotto)
	lds_graph_fuori_limite.setitem(ll_riga, "test", ld_des_test)
	lds_graph_fuori_limite.setitem(ll_riga, "data_validita", fd_data_validita)
	lds_graph_fuori_limite.setitem(ll_riga, "categoria", "2.Conformi")
	lds_graph_fuori_limite.setitem(ll_riga, "valore", ll_count_conforme)
	lds_graph_fuori_limite.setitem(ll_riga, "totale", ll_count_fuori_limite + ll_count_conforme)
	lds_graph_fuori_limite.setitem(ll_riga, "tipo", 'b) Conformi')
	
//	ll_riga = lds_graph_fuori_limite.insertrow(0)
//	lds_graph_fuori_limite.setitem(ll_riga, "prodotto", ls_des_prodotto)
//	lds_graph_fuori_limite.setitem(ll_riga, "test", ld_des_test)
//	lds_graph_fuori_limite.setitem(ll_riga, "data_validita", fd_data_validita)
//	lds_graph_fuori_limite.setitem(ll_riga, "categoria", "1")
//	lds_graph_fuori_limite.setitem(ll_riga, "valore", ll_count_fuori_limite + ll_count_conforme)
//	lds_graph_fuori_limite.setitem(ll_riga, "totale", ll_count_fuori_limite + ll_count_conforme)
//	lds_graph_fuori_limite.setitem(ll_riga, "tipo", 'c) Totali')
	//----------------------------------------------------
end if

ll_rows = lds_gauss.rowcount()
if ll_rows > 0 then
	st_status.text = "Elaborazione Dati ..."
	
	lds_gauss.setsort("row_value asc")
	lds_gauss.sort()
	
	//calcolo min e max
	ld_min = lds_gauss.getitemdecimal(1, "row_value")
	ld_max = lds_gauss.getitemdecimal(ll_rows, "row_value")
	
	hpb_1.maxposition = ld_max
	hpb_1.setstep = 1
	hpb_1.position = 0
	
	//calcolo della deviazione standard
	ld_dev_std = 0
	if ll_rows = 1 then 
		ld_dev_std = 0
		//calcolo ampiezza delle classi
		ld_ampiezza = truncate(1, 0)
	else
			for ll_index = 1 to ll_rows
			ld_valore = lds_gauss.getitemdecimal(ll_index, "row_value")
			ld_dev_std += (ld_valore - ld_valore_medio)^2
		next
		ld_dev_std = ld_dev_std / (ll_rows - 1)
		ld_dev_std = sqrt(ld_dev_std)
		
		//calcolo ampiezza delle classi
		/*
		secondo una formula trovata su internet
		A = (VALmax - VALmin) / (1 + 3.32 *log10 (NUMvalori) )
		
		Esempio:
		Valori: 10 25 12 12 14 13 5 12 10
		
		VALmax=25		VALmin=5			NUMvalori=9
		Calcolare l'ampiezza di ogni classe

		A = (25 - 5) / (1+3.32*log10(9)) = 4.798...  e quindi poniamo 4
		
		tra 5 e 9			(che sarebbe 5+A)		-> 1
		tra 9 e 13		(che sarebbe 9+A)		-> 4
		tra 13 e 17		(che sarebbe 5+A)		-> 1
		tra 17 e 21		(che sarebbe 5+A)		-> 0
		tra 21 e 25		(che sarebbe 5+A)		-> 0
		oltre 25											-> 1
		*/
		ld_ampiezza = truncate((ld_max - ld_min)/(1 + 3.32 * logten(ll_rows)), 0)
	end if

	if ld_ampiezza = 0 or isnull(ld_ampiezza) then ld_ampiezza = 1

	//preparo il primo intervallo
	ld_indice = truncate(ld_min, 0)
	ld_indice_max = ld_indice + ld_ampiezza
	ll_index2 = 1
	
	//inizio ciclo del conteggio della popolazione ----------------------------------------
	DO
		ld_vettore_conteggio[ll_index2] = 0
		ls_vettore_ampiezze[ll_index2] = ""
		for ll_index = 1 to ll_rows
			//conteggio
			ld_valore = lds_gauss.getitemdecimal(ll_index, "row_value")
			
			//se siamo sulla prima classe allora l'intervallo deve essere chiuso a dx e sx
			if ll_index2 = 1 then
				if (ld_valore>=ld_indice) and (ld_valore<=ld_indice_max) then ld_vettore_conteggio[ll_index2] += 1
			else
				if (ld_valore>ld_indice) and (ld_valore<=ld_indice_max) then ld_vettore_conteggio[ll_index2] += 1
			end if		
		next
		
		//esprimo l'intervallo
		if ll_index2 = 1 then 
			ls_vettore_ampiezze[ll_index2] = "[ "
		else
			ls_vettore_ampiezze[ll_index2] = "] "
		end if
		
		decimal ld_indice_appo, ld_indice_max_appo
		string ls_indice_appo, ls_indice_max_appo
		
		if ld_indice > 1000000 then
			ld_indice_appo = ld_indice /1000000
			ls_indice_appo = string(ld_indice_appo, "#####0.00")+" x10^6"
		else
			ld_indice_appo = ld_indice
			ls_indice_appo = string(ld_indice_appo)
		end if
		if ld_indice_max > 1000000 then
			ld_indice_max_appo = ld_indice_max /1000000
			ls_indice_max_appo = string(ld_indice_max_appo, "#####0.00")+" x10^6"
		else
			ld_indice_max_appo = ld_indice_max
			ls_indice_max_appo = string(ld_indice_max_appo)
		end if
		
		//ls_vettore_ampiezze[ll_index2] += string(ld_indice)+" ÷ "+string(ld_indice_max)+" ]"
		ls_vettore_ampiezze[ll_index2] += ls_indice_appo+" ÷ "+ls_indice_max_appo+" ]"
		
		//a ciclo finito passo all'altro intervallo
		ld_indice = ld_indice_max
		ld_indice_max = ld_indice + ld_ampiezza
		ll_index2 += 1
		
		hpb_1.Stepit()
		
	LOOP UNTIL ld_indice >= ld_max
	//--------------------------------------------------------------------------------------------------
	
	//proseguo con l'elaborazione del grafico della popolazione ------------------
	st_status.text = "Elaborazione Dati per grafico distribuzione dei Valori..."
	hpb_1.maxposition = upperbound(ld_vettore_conteggio)
	hpb_1.setstep = 1
	hpb_1.position = 0
	
	for ll_index = 1 to upperbound(ld_vettore_conteggio)
		ls_ampiezza = ls_vettore_ampiezze[ll_index]
		ls_ampiezza = "i" + string(ll_index) + ": " + ls_ampiezza
		
		ls_tipo = "C" //conteggio
		ld_valore = ld_vettore_conteggio[ll_index]
		
		ll_riga = lds_graph_popolazione.insertrow(0)
		lds_graph_popolazione.setitem(ll_riga, "ampiezza", ls_ampiezza)
		lds_graph_popolazione.setitem(ll_riga, "valore", ld_valore)
		lds_graph_popolazione.setitem(ll_riga, "tipo", ls_tipo)
		
//		ll_riga = lds_graph_popolazione.insertrow(0)
//		lds_graph_popolazione.setitem(ll_riga, "ampiezza", ls_ampiezza)
//		lds_graph_popolazione.setitem(ll_riga, "valore", ll_index)
//		lds_graph_popolazione.setitem(ll_riga, "tipo", "A")
		
		hpb_1.Stepit()
	next
	//-------------------------------------------------------------------------------------------------	
	//dw_popolazione.object.gr_1.category.dispattr.font.escapement = 450

end if

hpb_1.maxposition = 1
hpb_1.setstep = 1
hpb_1.position = 0
st_status.text = ""

//ANALITICO ------------------------------------------------------------
dw_graph_analitico.ib_datastore = true
dw_graph_analitico.ids_data = lds_graph_analitico

dw_graph_analitico.is_str[1] = "prodotto"
dw_graph_analitico.is_str[2] = "test"
dw_graph_analitico.is_str[3] = "tipo"
dw_graph_analitico.is_str[4] = "rownumber"
dw_graph_analitico.is_num[1] = "quantita"

dw_graph_analitico.is_source_categoria_col = "rownumber"

dw_graph_analitico.is_categoria_col = "str_4"
dw_graph_analitico.is_exp_valore = "num_1"
dw_graph_analitico.is_serie_col = "str_3"

dw_graph_analitico.is_titolo = ""
dw_graph_analitico.is_label_categoria = "Campioni (anno/numero - progr.)"
dw_graph_analitico.is_label_valori = "Valori"
dw_graph_analitico.il_backcolor = 12632256

dw_graph_analitico.uof_retrieve( )
//------------------------------------------------------------------------
//POPOLAZIONE ------------------------------------------------------------
string ls_SeriesName
integer li_ret


dw_graph_popolazione.ib_datastore = true
dw_graph_popolazione.ids_data = lds_graph_popolazione

dw_graph_popolazione.is_str[1] = "ampiezza"
dw_graph_popolazione.is_str[2] = "tipo"
dw_graph_popolazione.is_num[1] = "valore"

dw_graph_popolazione.is_source_categoria_col = "ampiezza"

dw_graph_popolazione.is_categoria_col = "str_1"
dw_graph_popolazione.is_exp_valore = "num_1"
dw_graph_popolazione.is_serie_col = "str_2"

dw_graph_popolazione.is_titolo = ""
dw_graph_popolazione.is_label_categoria = "Distribuzione dei valori"
dw_graph_popolazione.is_label_valori = "N° Campioni"
dw_graph_popolazione.il_backcolor = 12632256
dw_graph_popolazione.ii_tipo_grafico = 7 //Colonne

ls_SeriesName = "A"
li_ret = dw_graph_popolazione.SetSeriesStyle("gr_cs_graph", ls_SeriesName, true)

dw_graph_popolazione.uof_retrieve( )
//------------------------------------------------------------------------
//FUORI LIMITE ------------------------------------------------------------
dw_graph_fuori_limite.ib_datastore = true
dw_graph_fuori_limite.ids_data = lds_graph_fuori_limite

dw_graph_fuori_limite.is_str[1] = "prodotto"
dw_graph_fuori_limite.is_str[2] = "test"
dw_graph_fuori_limite.is_str[3] = "tipo"
dw_graph_fuori_limite.is_str[4] = "categoria"
dw_graph_fuori_limite.is_dtm[1] = "data_validita"
dw_graph_fuori_limite.is_num[1] = "valore"
dw_graph_fuori_limite.is_num[2] = "totale"

dw_graph_fuori_limite.is_source_categoria_col = "categoria"

dw_graph_fuori_limite.is_categoria_col = "str_4"
dw_graph_fuori_limite.is_exp_valore = "num_1"//"(num_1/num_2)*100"
dw_graph_fuori_limite.is_serie_col = "" //"str_3"

dw_graph_fuori_limite.is_titolo = ""
dw_graph_fuori_limite.is_label_categoria = ""
dw_graph_fuori_limite.is_label_valori = "%"
dw_graph_fuori_limite.il_backcolor = 12632256
dw_graph_fuori_limite.ii_tipo_grafico = 7 //Colonne

dw_graph_fuori_limite.uof_retrieve( )
//------------------------------------------------------------------------

destroy lds_graph_analitico
destroy lds_graph_popolazione
destroy lds_graph_fuori_limite

setpointer(arrow!)
return ll_righe
end function

public function long wf_retrieve_test (string fs_cod_prodotto, datetime fd_data_validita, ref string fs_cod_test);long ll_ret

if fs_cod_test = "" then setnull(fs_cod_test)

dw_test.settransobject(sqlca)
dw_test.reset()

ll_ret = dw_test.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fd_data_validita, fs_cod_test)
//string ls_pippo
//datetime ldt_pippo
//
//ls_pippo = string(fd_data_validita, "dd-mm-yyyy hh:mm:ss.fff")
//ldt_pippo = datetime(ls_pippo)
//
//ls_pippo = dw_test.dataobject
//ll_ret = dw_test.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, ldt_pippo, fs_cod_test)

return ll_ret
end function

on w_statistiche_laboratorio.create
int iCurrent
call super::create
this.st_val_atteso=create st_val_atteso
this.st_val_medio=create st_val_medio
this.st_tol_max=create st_tol_max
this.st_tol_min=create st_tol_min
this.dw_graph_fuori_limite=create dw_graph_fuori_limite
this.dw_graph_analitico=create dw_graph_analitico
this.st_status=create st_status
this.hpb_1=create hpb_1
this.dw_folder=create dw_folder
this.dw_folder_search=create dw_folder_search
this.dw_test=create dw_test
this.dw_prodotti_validita=create dw_prodotti_validita
this.dw_filtro=create dw_filtro
this.cb_reset=create cb_reset
this.cb_cerca=create cb_cerca
this.dw_graph_popolazione=create dw_graph_popolazione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_val_atteso
this.Control[iCurrent+2]=this.st_val_medio
this.Control[iCurrent+3]=this.st_tol_max
this.Control[iCurrent+4]=this.st_tol_min
this.Control[iCurrent+5]=this.dw_graph_fuori_limite
this.Control[iCurrent+6]=this.dw_graph_analitico
this.Control[iCurrent+7]=this.st_status
this.Control[iCurrent+8]=this.hpb_1
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.dw_folder_search
this.Control[iCurrent+11]=this.dw_test
this.Control[iCurrent+12]=this.dw_prodotti_validita
this.Control[iCurrent+13]=this.dw_filtro
this.Control[iCurrent+14]=this.cb_reset
this.Control[iCurrent+15]=this.cb_cerca
this.Control[iCurrent+16]=this.dw_graph_popolazione
end on

on w_statistiche_laboratorio.destroy
call super::destroy
destroy(this.st_val_atteso)
destroy(this.st_val_medio)
destroy(this.st_tol_max)
destroy(this.st_tol_min)
destroy(this.dw_graph_fuori_limite)
destroy(this.dw_graph_analitico)
destroy(this.st_status)
destroy(this.hpb_1)
destroy(this.dw_folder)
destroy(this.dw_folder_search)
destroy(this.dw_test)
destroy(this.dw_prodotti_validita)
destroy(this.dw_filtro)
destroy(this.cb_reset)
destroy(this.cb_cerca)
destroy(this.dw_graph_popolazione)
end on

event pc_setwindow;call super::pc_setwindow;datetime ldt_oggi
windowobject lw_oggetti[]
windowobject lw_oggetti_graph[]

//dw_filtro.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)
//dw_campionamenti.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)
//dw_test.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)

//  ------------------------  imposto folder di ricerca ---------------------------
dw_filtro.insertrow(0)

lw_oggetti[1] = dw_prodotti_validita
lw_oggetti[2] = dw_test
dw_folder_search.fu_assigntab(2, "Lista Prodotti e Test associati", lw_oggetti[])

lw_oggetti[1] = dw_filtro
//lw_oggetti[2] = cb_fornitori_ricerca
//lw_oggetti[3] = cb_prodotti_ricerca
lw_oggetti[2] = cb_reset
lw_oggetti[3] = cb_cerca
dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti[])

dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

ldt_oggi = datetime(today(),time("00:00:00"))
dw_filtro.setitem(1,"rdt_data_fine",ldt_oggi)


//  ------------------------  imposto folder dei grafici ---------------------------
lw_oggetti_graph[1] = dw_graph_popolazione
dw_folder.fu_assigntab(2, "Distribuzione Valori", lw_oggetti_graph[])

lw_oggetti_graph[1] = dw_graph_fuori_limite
dw_folder.fu_assigntab(3, "Conformi/Non Conformi", lw_oggetti_graph[])

lw_oggetti_graph[1] = dw_graph_analitico
lw_oggetti_graph[2] = st_tol_min
lw_oggetti_graph[3] = st_tol_max
lw_oggetti_graph[4] = st_val_atteso
lw_oggetti_graph[5] = st_val_medio
dw_folder.fu_assigntab(1, "Andamento", lw_oggetti_graph[])


dw_folder.fu_foldercreate(3, 3)
dw_folder.fu_selecttab(1)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_filtro,"rs_cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")
end event

type st_val_atteso from statictext within w_statistiche_laboratorio
integer x = 1193
integer y = 3092
integer width = 864
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Val.Atteso:"
boolean focusrectangle = false
end type

type st_val_medio from statictext within w_statistiche_laboratorio
integer x = 1207
integer y = 3012
integer width = 864
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Val.Medio:"
boolean focusrectangle = false
end type

type st_tol_max from statictext within w_statistiche_laboratorio
integer x = 2345
integer y = 3012
integer width = 864
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Toll.Max.:"
boolean focusrectangle = false
end type

type st_tol_min from statictext within w_statistiche_laboratorio
integer x = 46
integer y = 3012
integer width = 864
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Toll.Min.:"
boolean focusrectangle = false
end type

type dw_graph_fuori_limite from uo_cs_graph within w_statistiche_laboratorio
integer x = 46
integer y = 948
integer width = 3168
integer height = 2232
integer taborder = 60
string dataobject = "d_cs_graph_pie"
boolean ib_pie = true
long il_backcolor = 12632256
end type

type dw_graph_analitico from uo_cs_graph within w_statistiche_laboratorio
integer x = 46
integer y = 948
integer width = 3168
integer height = 2236
integer taborder = 40
boolean hscrollbar = true
long il_backcolor = 12632256
end type

event buttonclicked;call super::buttonclicked;st_tol_min.backcolor = il_backcolor_run
st_val_medio.backcolor = il_backcolor_run
st_tol_max.backcolor = il_backcolor_run
st_val_atteso.backcolor = il_backcolor_run
end event

type st_status from statictext within w_statistiche_laboratorio
integer x = 1691
integer y = 740
integer width = 1554
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_statistiche_laboratorio
integer x = 23
integer y = 740
integer width = 1646
integer height = 60
unsignedinteger maxposition = 100
integer setstep = 10
end type

type dw_folder from u_folder within w_statistiche_laboratorio
integer x = 23
integer y = 820
integer width = 3223
integer height = 2380
integer taborder = 30
end type

type dw_folder_search from u_folder within w_statistiche_laboratorio
integer x = 23
integer y = 20
integer width = 3223
integer height = 700
integer taborder = 30
end type

type dw_test from datawindow within w_statistiche_laboratorio
integer x = 1783
integer y = 120
integer width = 1463
integer height = 600
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_test_per_statistiche"
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;string ls_cod_prodotto, ls_cod_test
datetime ldt_data_validita
long ll_rows

dw_graph_analitico.reset()
dw_graph_popolazione.reset()
dw_graph_fuori_limite.reset()

if currentrow > 0 then
	if ib_ricerca then return

	selectrow(0, false)
	selectrow(currentrow, true)
	ls_cod_prodotto = getitemstring(currentrow, "cod_prodotto")
	ls_cod_test = getitemstring(currentrow, "cod_test")
	ldt_data_validita = getitemdatetime(currentrow, "data_validita")
	
	wf_analitico(ls_cod_prodotto, ls_cod_test, ldt_data_validita)
end if
end event

type dw_prodotti_validita from datawindow within w_statistiche_laboratorio
integer x = 23
integer y = 120
integer width = 1737
integer height = 600
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_ext_prodotti_statistiche"
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;string ls_cod_prodotto, ls_cod_test
datetime ldt_data_validita
long ll_rows

if currentrow > 0 then
	if ib_ricerca then return

	selectrow(0, false)
	selectrow(currentrow, true)
	ls_cod_prodotto = getitemstring(currentrow, "cod_prodotto")
	ldt_data_validita = getitemdatetime(currentrow, "data_validita")
	
	ls_cod_test = dw_filtro.getitemstring(1, "rs_cod_test")
	
	ll_rows = wf_retrieve_test(ls_cod_prodotto, ldt_data_validita, ls_cod_test)
//	if ll_rows > 0 then
//		dw_test.selectrow(0, false)
//		dw_test.selectrow(1, true)
//	end if
end if
end event

type dw_filtro from u_dw_search within w_statistiche_laboratorio
integer x = 23
integer y = 120
integer width = 3223
integer height = 600
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_statistiche_lab_filtro"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_filtro,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_filtro,"cod_fornitore")
end choose
end event

type cb_reset from commandbutton within w_statistiche_laboratorio
integer x = 1211
integer y = 620
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;datetime ldt_oggi
string ls_null

setnull(ls_null)
setnull(ldt_oggi)

dw_filtro.setitem(1,"rs_cod_prodotto",ls_null)
dw_filtro.setitem(1,"rs_cod_test",ls_null)
dw_filtro.setitem(1,"rs_cod_fornitore",ls_null)

dw_filtro.setitem(1,"rdt_data_inizio",ldt_oggi)

ldt_oggi = datetime(today(),time("00:00:00"))
dw_filtro.setitem(1,"rdt_data_fine",ldt_oggi)

dw_filtro.setitem(1,"rl_anno_campione_da",0)
dw_filtro.setitem(1,"rl_num_campione_da",0)
dw_filtro.setitem(1,"rl_anno_campione_a",0)
dw_filtro.setitem(1,"rl_num_campione_a",0)
end event

type cb_cerca from commandbutton within w_statistiche_laboratorio
integer x = 1600
integer y = 620
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;long ll_ret
string ls_cod_prodotto, ls_cod_test
datetime ldt_data_validita

dw_filtro.AcceptText()
ib_ricerca = true
ll_ret = wf_ricerca()
ib_ricerca = false
dw_folder_search.fu_selecttab(2)

if dw_test.rowcount() > 0 then
	dw_test.selectrow(0, false)
	dw_test.selectrow(1, true)
	ls_cod_prodotto = dw_test.getitemstring(1, "cod_prodotto")
	ls_cod_test = dw_test.getitemstring(1, "cod_test")
	ldt_data_validita = dw_test.getitemdatetime(1, "data_validita")
	wf_analitico(ls_cod_prodotto, ls_cod_test, ldt_data_validita)
end if
end event

type dw_graph_popolazione from uo_cs_graph within w_statistiche_laboratorio
integer x = 46
integer y = 948
integer width = 3168
integer height = 2240
integer taborder = 50
boolean bringtotop = true
long il_backcolor = 12632256
end type

