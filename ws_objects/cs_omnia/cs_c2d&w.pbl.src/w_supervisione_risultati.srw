﻿$PBExportHeader$w_supervisione_risultati.srw
forward
global type w_supervisione_risultati from w_cs_xx_principale
end type
type dw_supervisione_test from datawindow within w_supervisione_risultati
end type
type cb_refresh from commandbutton within w_supervisione_risultati
end type
type dw_folder from u_folder within w_supervisione_risultati
end type
type dw_supervisione_camp from datawindow within w_supervisione_risultati
end type
type dw_filtro from u_dw_search within w_supervisione_risultati
end type
type cb_cerca from commandbutton within w_supervisione_risultati
end type
type cb_reset from commandbutton within w_supervisione_risultati
end type
end forward

global type w_supervisione_risultati from w_cs_xx_principale
integer width = 3730
integer height = 2984
string title = "Supervisione Risultati"
dw_supervisione_test dw_supervisione_test
cb_refresh cb_refresh
dw_folder dw_folder
dw_supervisione_camp dw_supervisione_camp
dw_filtro dw_filtro
cb_cerca cb_cerca
cb_reset cb_reset
end type
global w_supervisione_risultati w_supervisione_risultati

type variables
decimal idc_sec_timer
boolean ib_timer = false
end variables

forward prototypes
public function integer wf_retrieve_test (long fl_row)
public function integer wf_controlla_tolleranza (string fs_cod_prodotto, string fs_cod_test, datetime fdt_data_validita, decimal fd_valore)
public subroutine wf_ricerca ()
end prototypes

public function integer wf_retrieve_test (long fl_row);long ll_ret, ll_anno_reg_camp, ll_num_reg_camp, ll_index, li_return
string ls_cod_prodotto, ls_cod_test
datetime ldt_data_validita
decimal ldc_valore

ll_anno_reg_camp = dw_supervisione_camp.getitemnumber(fl_row, "anno_reg_campionamenti")
ll_num_reg_camp = dw_supervisione_camp.getitemnumber(fl_row, "num_reg_campionamenti")
ls_cod_prodotto = dw_supervisione_camp.getitemstring(fl_row, "cod_prodotto")

ll_ret = dw_supervisione_test.retrieve(s_cs_xx.cod_azienda, &
														ll_anno_reg_camp, &
														ll_num_reg_camp, &
														ls_cod_prodotto)
														
for ll_index = 1 to ll_ret
	ls_cod_prodotto = dw_supervisione_test.getitemstring(ll_index, "cod_prodotto")
	ls_cod_test = dw_supervisione_test.getitemstring(ll_index, "cod_test")
	ldt_data_validita = dw_supervisione_test.getitemdatetime(ll_index, "data_validita")
	ldc_valore = dw_supervisione_test.getitemdecimal(ll_index, "quantita")
	
	li_return = wf_controlla_tolleranza(ls_cod_prodotto, ls_cod_test, ldt_data_validita, ldc_valore)
	//test sul ritorno: se 0 OK, se 1 è inferiore al MIN, se 2 è superiore al MAX
	if li_return <> 0 then
		dw_supervisione_test.setitem(ll_index, "fl", "S")
	else
		dw_supervisione_test.setitem(ll_index, "fl", "N")
	end if
next								
dw_supervisione_test.groupcalc()

return ll_ret
end function

public function integer wf_controlla_tolleranza (string fs_cod_prodotto, string fs_cod_test, datetime fdt_data_validita, decimal fd_valore);//funzione per determinare se il valore numerico immesso rientra nei limiti della tolleranza
//torna 0 se tutto a posto oppure nessun limite impostato
//torna 1 se il valore è più piccolo dell'estremo inferiore
//torna 2 se  il valore è più grande dell'estremo superiore

decimal ldc_tol_minima, ldc_tol_massima

select
	 tol_minima
	,tol_massima
into 
	 :ldc_tol_minima
	,:ldc_tol_massima
from tab_test_prodotti
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_prodotto = :fs_cod_prodotto  
	and cod_test = :fs_cod_test  
	and data_validita = :fdt_data_validita
		;

if isnull(ldc_tol_minima) or ldc_tol_minima = 0 then
	//forse non è stato impostato l'estremo inferiore
	if isnull(ldc_tol_massima) or ldc_tol_massima = 0 then
		//non è stato impostato neppure l'estremo superiore: non fare nessun controllo		
	else
		//è stato impostato l'estremo superiore
		if fd_valore > ldc_tol_massima then return 2
	end if
else
	//estremo inferiore impostato
	if isnull(ldc_tol_massima) or ldc_tol_massima = 0 then
		//non è stato impostato neppure l'estremo superiore, ma quello inferiore si
		if fd_valore < ldc_tol_minima then return 1
	else
		//è stato impostato l'estremo superiore e l'estremo inferiore
		if fd_valore > ldc_tol_massima then return 2
		if fd_valore < ldc_tol_minima then return 1
	end if
end if

return 0
end function

public subroutine wf_ricerca ();datetime ldt_data_registrazione_da, ldt_data_registrazione_a, ldt_data_prelievo_da, ldt_data_prelievo_a, ldt_data_validita
long ll_num_reg_campionamento, ll_anno_reg_campionamento
string ls_num_campionamento, ls_cod_prodotto, ls_cod_fornitore
string ls_flag_conforme, ls_flag_letto, ls_filter
long ll_rows, ll_count_chiusura, ll_index
date ldt_appoggio

dw_filtro.AcceptText()

dw_supervisione_camp.setredraw(false)
setpointer(hourglass!)

ll_anno_reg_campionamento = dw_filtro.getitemnumber(1, "anno_reg_camp")
if ll_anno_reg_campionamento < 0 then setnull(ll_anno_reg_campionamento)

ll_num_reg_campionamento = dw_filtro.getitemnumber(1, "num_reg_camp")
if ll_num_reg_campionamento < 0 then setnull(ll_anno_reg_campionamento)

ls_num_campionamento = dw_filtro.getitemstring(1, "num_campionamento")

ls_cod_prodotto = dw_filtro.getitemstring(1, "cod_prodotto")
ls_cod_fornitore = dw_filtro.getitemstring(1, "cod_fornitore")

ldt_data_registrazione_da = dw_filtro.getitemdatetime(1, "data_registrazione_da")
ldt_data_registrazione_a = dw_filtro.getitemdatetime(1, "data_registrazione_a")
ldt_data_prelievo_da = dw_filtro.getitemdatetime(1, "data_prelievo_da")
ldt_data_prelievo_a = dw_filtro.getitemdatetime(1, "data_prelievo_a")

if not isnull(ldt_data_registrazione_da) then
	ldt_appoggio = date(ldt_data_registrazione_da)
	ldt_data_registrazione_da = datetime(ldt_appoggio, time("00:00:00"))
end if
if not isnull(ldt_data_registrazione_a) then
	ldt_appoggio = date(ldt_data_registrazione_a)
	ldt_data_registrazione_a = datetime(ldt_appoggio, time("23:59:59"))
end if
if not isnull(ldt_data_prelievo_da) then
	ldt_appoggio = date(ldt_data_prelievo_da)
	ldt_data_prelievo_da = datetime(ldt_appoggio, time("00:00:00"))
end if
if not isnull(ldt_data_prelievo_a) then
	ldt_appoggio = date(ldt_data_prelievo_a)
	ldt_data_prelievo_a = datetime(ldt_appoggio, time("23:59:59"))
end if

ls_flag_conforme = dw_filtro.getitemstring(1, "flag_conforme")
ls_flag_letto = "T"//dw_filtro.getitemstring(1, "flag_letto")

dw_supervisione_camp.reset()
dw_supervisione_camp.setfilter("")

ll_rows = dw_supervisione_camp.retrieve( &
													 s_cs_xx.cod_azienda 						&
													,ll_anno_reg_campionamento 			&
													,ll_num_reg_campionamento 			&
													,ls_cod_fornitore 								&
													,ls_num_campionamento 					&
													,ls_cod_prodotto 								&
													,ldt_data_prelievo_da 						&
													,ldt_data_prelievo_a 							&
													,ldt_data_registrazione_da 					&
													,ldt_data_registrazione_a 					&
													,ls_flag_conforme								&
													,ls_flag_letto										&
												)

//script per capire se c'è almeno un test associato con flag chiusura pari a 1
for ll_index = 1 to ll_rows
	ls_cod_prodotto = dw_supervisione_camp.getitemstring(ll_index, "cod_prodotto")
	ll_anno_reg_campionamento = dw_supervisione_camp.getitemnumber(ll_index, "anno_reg_campionamenti")
	ll_num_reg_campionamento = dw_supervisione_camp.getitemnumber(ll_index, "num_reg_campionamenti")
	ldt_data_validita = dw_supervisione_camp.getitemdatetime(ll_index, "data_validita")
	
	ll_count_chiusura = 0
	
	select count(tab_test_prodotti.flag_chiusura)
	into :ll_count_chiusura
	from det_campionamenti
	join tab_test_prodotti on (tab_test_prodotti.cod_prodotto = det_campionamenti.cod_prodotto
		and tab_test_prodotti.cod_test = det_campionamenti.cod_test
		and tab_test_prodotti.data_validita = det_campionamenti.data_validita)
	where det_campionamenti.cod_azienda = :s_cs_xx.cod_azienda
		and det_campionamenti.anno_reg_campionamenti = :ll_anno_reg_campionamento
		and det_campionamenti.num_reg_campionamenti = :ll_num_reg_campionamento
		and det_campionamenti.cod_prodotto = :ls_cod_prodotto
		and det_campionamenti.data_validita = :ldt_data_validita
		and tab_test_prodotti.flag_chiusura = 'S'
	;
	
	if isnull(ll_count_chiusura) then ll_count_chiusura = 0
	dw_supervisione_camp.setitem(ll_index, "cf_chiusura", ll_count_chiusura)
	
next

ls_flag_letto = dw_filtro.getitemstring(1, "flag_letto")
choose case ls_flag_letto
	case "T"
		//tutto normale
		ls_filter = ""
	case "S"
		//visualizza solo quelli con flag_letto = 'S' e con almeno un test associato con flag chiusura pari a 1
		// e assieme quelli con flag_letto = 'N' e con NESSUN test associato con flag chiusura pari a 1
		ls_filter = " (flag_letto = 'S' and cf_chiusura>0) or (flag_letto = 'N' and cf_chiusura=0 ) "
	case "N"
		//visualizza solo quelli con flag_letto = 'N' e con almeno un test associato con flag chiusura pari a 1
		ls_filter = "flag_letto = 'N' and cf_chiusura>0 "
end choose

if ll_rows <=0 then 
	dw_supervisione_test.reset()
else
	dw_supervisione_camp.setfilter(ls_filter)
	dw_supervisione_camp.filter()
end if

setpointer(arrow!)
dw_supervisione_camp.setredraw(true)
end subroutine

on w_supervisione_risultati.create
int iCurrent
call super::create
this.dw_supervisione_test=create dw_supervisione_test
this.cb_refresh=create cb_refresh
this.dw_folder=create dw_folder
this.dw_supervisione_camp=create dw_supervisione_camp
this.dw_filtro=create dw_filtro
this.cb_cerca=create cb_cerca
this.cb_reset=create cb_reset
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_supervisione_test
this.Control[iCurrent+2]=this.cb_refresh
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_supervisione_camp
this.Control[iCurrent+5]=this.dw_filtro
this.Control[iCurrent+6]=this.cb_cerca
this.Control[iCurrent+7]=this.cb_reset
end on

on w_supervisione_risultati.destroy
call super::destroy
destroy(this.dw_supervisione_test)
destroy(this.cb_refresh)
destroy(this.dw_folder)
destroy(this.dw_supervisione_camp)
destroy(this.dw_filtro)
destroy(this.cb_cerca)
destroy(this.cb_reset)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]
string ls_path, ls_expression, ls_bitmap, ls_fileimg, ls_fileimg_conforme
string ls_fileimg_non_conforme, ls_fileimg_in_attesa_esito
integer li_anno

//dw_filtro.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)
//dw_campionamenti.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)
//dw_test.set_dw_options(sqlca,pcca.null_object,c_NoNew+c_NoModify+c_NoDelete,c_NoResizeDW)

//  ------------------------  imposto folder di ricerca ---------------------------

dw_filtro.insertrow(0)
dw_supervisione_camp.settransobject(sqlca)
dw_supervisione_test.settransobject(sqlca)
dw_supervisione_camp.setrowfocusindicator(hand!)
dw_supervisione_test.setrowfocusindicator(hand!)

// Lista -------------------------------
lw_oggetti[1] = dw_supervisione_camp
lw_oggetti[2] = cb_refresh
lw_oggetti[3] = dw_supervisione_test
dw_folder.fu_assigntab(2, "Lista", lw_oggetti[])
//---------------------------------------
// Filtro -------------------------------
lw_oggetti[1] = dw_filtro
lw_oggetti[2] = cb_cerca
lw_oggetti[3] = cb_reset
dw_folder.fu_assigntab(1, "Filtro", lw_oggetti[])
//---------------------------------------

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

li_anno = year(today())
dw_filtro.setitem(1,"anno_reg_camp",li_anno)

//--------------------------------------------------------------------------------------------------------------------------
//imposta le immagini per la dw_filtro
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "conforme_silver.bmp"
dw_filtro.object.p_conforme.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "non_conforme_silver.bmp"
dw_filtro.object.p_non_conforme.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "in_attesa_esito_silver.bmp"
dw_filtro.object.p_in_attesa_esito.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "new.bmp"
dw_filtro.object.p_non_letto.FileName = ls_path

//--------------------------------------------------------------------------------------------------------------------------
//imposta le immagini per la dw_supervisione camp
ls_fileimg = s_cs_xx.volume + s_cs_xx.risorse + "new.bmp"
ls_bitmap = "bitmap('"+ls_fileimg+"')"
dw_supervisione_camp.object.cf_alert.expression = ls_bitmap

ls_fileimg_conforme = s_cs_xx.volume + s_cs_xx.risorse + "conforme_silver.bmp"
ls_fileimg_non_conforme = s_cs_xx.volume + s_cs_xx.risorse + "non_conforme_silver.bmp"
ls_fileimg_in_attesa_esito = s_cs_xx.volume + s_cs_xx.risorse + "in_attesa_esito_silver.bmp"

ls_expression = "case (flag_conforme when 'S' then '"+ls_fileimg_conforme+"' when 'N' then '"+ls_fileimg_non_conforme+"' else '"+ls_fileimg_in_attesa_esito+"') "

ls_bitmap = "bitmap("+ls_expression+")"
dw_supervisione_camp.object.cf_flag_conforme.expression = ls_bitmap

//--------------------------------------------------------------------------------------------------------------------------
//imposta le immagini per la dw_supervisione_test
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "icn_search.bmp"
dw_supervisione_test.object.b_consulta.FileName = ls_path
//dw_supervisione_test.object.p_consulta.FileName = ls_path


//NON SERVE PIU' PERCHE' SONO STATI RESI INVISIBILI
////disabilita l'uso di alcuni pulsanti nella dw_supervisione_test e proteggi alcuni campi
//dw_supervisione_test.object.b_cod_valore.enabled = false
//dw_supervisione_test.object.b_altro.enabled = false
////-----
//dw_supervisione_test.object.flag_esito.protect = 1
//dw_supervisione_test.object.flag_esito.background.color = 12632256 //silver
//
//dw_supervisione_test.object.quantita.protect = 1
////dw_supervisione_test.object.quantita.background.color = 12632256 //silver
//
//dw_supervisione_test.object.flag_altro_valore.protect = 1
//dw_supervisione_test.object.flag_altro_valore.background.color = 12632256 //silver
//
//
//##############################
setnull(idc_sec_timer)

select 	numero
into 		:idc_sec_timer
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and flag_parametro = 'N'
	and cod_parametro = 'TLB';
	
if isnull(idc_sec_timer) or idc_sec_timer <=0 then
	g_mb.messagebox("OMNIA", "Parametro TLB (intervallo di refresh) non impostato nei parametri aziendali!" &
										+" Non sarà attivato il timer!", Exclamation!)
	ib_timer = false
else
	ib_timer = true						
	timer(int(idc_sec_timer))								
end if
//##############################
end event

event timer;call super::timer;long ll_row

ll_row = dw_supervisione_camp.getrow()

wf_ricerca()

if ll_row > 0 then dw_supervisione_camp.setrow(ll_row)
end event

type dw_supervisione_test from datawindow within w_supervisione_risultati
integer x = 69
integer y = 1680
integer width = 3566
integer height = 1160
integer taborder = 70
string title = "none"
string dataobject = "d_det_campionamenti_supervisione"
boolean vscrollbar = true
boolean livescroll = true
end type

event buttonclicked;string ls_flag_altro_valore, ls_altro_valore, ls_cod_test,ls_cod_prodotto, ls_null
s_tab_test_valori_griglia ls_tab_test_valori_griglia
decimal ld_null
datetime ldt_data_validita
s_test_prodotti ls_test_prodotti

setnull(ls_null)
setnull(ld_null)

if row > 0 then
	this.setrow(row)
	AcceptText()
	choose case dwo.name
		case "b_consulta"						
			ls_cod_prodotto = getitemstring(row,"cod_prodotto")
			ls_cod_test = getitemstring(row,"cod_test")
			ldt_data_validita = getitemdatetime(row, "data_validita")
			
			ls_test_prodotti.s_cod_prodotto = ls_cod_prodotto
			ls_test_prodotti.s_cod_test = ls_cod_test
			ls_test_prodotti.dt_validita = ldt_data_validita
			openwithparm(w_consulta_test_prodotto, ls_test_prodotti)
			
//		case "b_cod_test"
//			//recuperare il test e tutte le info scritte nella corrispondente det_piani_campionamento
//			
//			
//		case "b_cod_valore"
//			ls_cod_test = getitemstring(row,"cod_test")
//			ls_tab_test_valori_griglia.s_cod_test = ls_cod_test
//			ls_tab_test_valori_griglia.s_cod_valore = ""
//			ls_tab_test_valori_griglia.s_desc_valore = ""
//			openwithparm(w_inserisci_valori_per_test, ls_tab_test_valori_griglia)
//			
//			ls_tab_test_valori_griglia = Message.PowerObjectParm
//			if ls_tab_test_valori_griglia.s_cod_valore = "cs_team_esc_pressed" then				
//			else
//				setitem(row, "cod_valore", ls_tab_test_valori_griglia.s_cod_valore)
//				setitem(row, "des_valore", ls_tab_test_valori_griglia.s_desc_valore)
//				//annulla gli altri valori
//				setitem(row,"flag_esito", "I")
//				setitem(row,"quantita",ld_null)
//				setitem(row,"flag_altro_valore","N")
//				setitem(row,"altro_valore","")
//				setitem(row, "fl", "N")
//			end if
//			
//		case "flag_altro_valore"
//			ls_flag_altro_valore = getitemstring(row,"flag_altro_valore")
//			if ls_flag_altro_valore = "N" then setitem(row,"altro_valore","")
//			
//		case "b_altro"
//			ls_flag_altro_valore = getitemstring(row,"flag_altro_valore")
//			if ls_flag_altro_valore = "N" then 
//				if g_mb.messagebox("OMNIA", "Inserire un altro valore?", Question!, YesNo!, 1) = 1 then
//					setitem(row,"flag_altro_valore","S")
//					ls_flag_altro_valore = "S"
//				else
//					ls_flag_altro_valore = "N"
//				end if
//			end if
//			
//			if ls_flag_altro_valore = "S" then 	
//				ls_altro_valore = getitemstring(row,"altro_valore")
//				if isnull(ls_altro_valore) then ls_altro_valore = ""
//				openwithparm(w_inserisci_altro_valore,ls_altro_valore)
//				
//				ls_altro_valore = Message.StringParm
//				if ls_altro_valore <> "cs_team_esc_pressed" then 
//					setitem(row,"altro_valore",ls_altro_valore)
//					//annulla tutti gli altri valori
//					setitem(row,"flag_esito", "I")
//					setitem(row,"cod_valore",ls_null)
//					setitem(row,"des_valore",ls_null)
//					setitem(row,"quantita",ld_null)
//					setitem(row, "fl", "N")
//				end if
//			end if
	end choose
end if
end event

type cb_refresh from commandbutton within w_supervisione_risultati
integer x = 69
integer y = 160
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Refresh"
end type

event clicked;wf_ricerca()
end event

type dw_folder from u_folder within w_supervisione_risultati
integer x = 46
integer width = 3634
integer height = 2860
integer taborder = 10
boolean border = false
end type

type dw_supervisione_camp from datawindow within w_supervisione_risultati
integer x = 69
integer y = 240
integer width = 3566
integer height = 1400
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_supervisione_camp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;if currentrow > 0 then
//	selectrow(0,false)
//	selectrow(currentrow,true)
	
	wf_retrieve_test(currentrow)
end if
end event

event clicked;if row > 0 then
//	selectrow(0,false)
//	selectrow(row,true)

	wf_retrieve_test(row)
end if
end event

event doubleclicked;string ls_letto, ls_cod_utente_lettura
datetime ldt_data_utente_lettura
long ll_anno_reg_camp, ll_num_reg_camp, ll_cf_chiusura

if row > 0 then
	if dwo.name = "cf_alert" or dwo.name = "t_alert" then
		ls_letto = getitemstring(row, "flag_letto")
		ll_cf_chiusura = getitemnumber(row, "cf_chiusura")
		if ls_letto = "N" and ll_cf_chiusura>0 then
			ll_anno_reg_camp = getitemnumber(row,"anno_reg_campionamenti")
			ll_num_reg_camp = getitemnumber(row,"num_reg_campionamenti")
			
			if g_mb.messagebox("OMNIA",&
									"Confermare lo stato del Campionamento " &
											+string(ll_anno_reg_camp)+"/"+string(ll_num_reg_camp)+ " a 'LETTO'?", &
									Question!,YesNo!, 1) = 1 then
					
					ls_cod_utente_lettura = s_cs_xx.cod_utente
					ldt_data_utente_lettura = datetime(today(), now())
					
					update tes_campionamenti
					set 
						 flag_letto = 'S'
						,cod_utente_lettura = :ls_cod_utente_lettura
						,data_utente_lettura = :ldt_data_utente_lettura
					where cod_azienda = :s_cs_xx.cod_azienda
						and anno_reg_campionamenti = :ll_anno_reg_camp
						and num_reg_campionamenti = :ll_num_reg_camp
					;
					if sqlca.sqlcode = 0 then
						commit;
						if sqlca.sqlcode = 0 then
							wf_ricerca()
						else
							g_mb.messagebox("OMNIA","Errore durante la conferma dell'aggiornamento",StopSign!)
							rollback;
						end if
					else
						g_mb.messagebox("OMNIA","Errore durante l'aggiornamento",StopSign!)
						rollback;
					end if
			end if
		end if
	end if	
end if

end event

type dw_filtro from u_dw_search within w_supervisione_risultati
integer x = 69
integer y = 140
integer width = 2583
integer height = 900
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_filtro_supervisione_camp"
boolean border = false
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_filtro
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_filtro,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_filtro,"cod_fornitore")
end choose
end event

type cb_cerca from commandbutton within w_supervisione_risultati
integer x = 1943
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;wf_ricerca()

dw_folder.fu_selecttab(2)
end event

type cb_reset from commandbutton within w_supervisione_risultati
integer x = 1554
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;datetime ldt_oggi
datetime ldt_oggi_a_x_gg
string ls_null
integer li_anno
long ll_null

setnull(ls_null)
setnull(ldt_oggi)
setnull(ll_null)

li_anno = year(today())

dw_filtro.setitem(1,"anno_reg_camp",li_anno)
dw_filtro.setitem(1,"num_reg_camp",ll_null)
dw_filtro.setitem(1,"num_campionamento",ls_null)
dw_filtro.setitem(1,"cod_fornitore",ls_null)
dw_filtro.setitem(1,"cod_prodotto",ls_null)

dw_filtro.setitem(1,"data_prelievo_da",ldt_oggi)
dw_filtro.setitem(1,"data_prelievo_a",ldt_oggi)
dw_filtro.setitem(1,"data_registrazione_da",ldt_oggi)
dw_filtro.setitem(1,"data_registrazione_a",ldt_oggi)

dw_filtro.setitem(1,"flag_conforme","T")
dw_filtro.setitem(1,"flag_letto","T")

end event

