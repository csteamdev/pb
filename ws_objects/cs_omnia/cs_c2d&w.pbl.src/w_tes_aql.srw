﻿$PBExportHeader$w_tes_aql.srw
$PBExportComments$Window tes_aql
forward
global type w_tes_aql from w_cs_xx_principale
end type
type dw_tes_aql_lista from uo_cs_xx_dw within w_tes_aql
end type
type dw_det_aql_det from uo_cs_xx_dw within w_tes_aql
end type
type dw_det_aql_lista from uo_cs_xx_dw within w_tes_aql
end type
type dw_tes_aql_det from uo_cs_xx_dw within w_tes_aql
end type
type dw_folder from u_folder within w_tes_aql
end type
end forward

global type w_tes_aql from w_cs_xx_principale
int Width=2643
int Height=1585
boolean TitleBar=true
string Title="Tabella AQL"
dw_tes_aql_lista dw_tes_aql_lista
dw_det_aql_det dw_det_aql_det
dw_det_aql_lista dw_det_aql_lista
dw_tes_aql_det dw_tes_aql_det
dw_folder dw_folder
end type
global w_tes_aql w_tes_aql

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_tes_piani_campionamento_dett,"cod_piano_campionamento",sqlca,&
//                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//

end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects_1[]
windowobject l_objects_2[]
windowobject l_objects_3[]

l_objects_1[1] = dw_tes_aql_det
dw_folder.fu_AssignTab(1, "&Principale", l_Objects_1[])
l_objects_2[1] = dw_det_aql_det
l_objects_2[2] = dw_det_aql_lista
dw_folder.fu_AssignTab(2, "&Dettaglio", l_Objects_2[])


dw_tes_aql_lista.set_dw_key("cod_azienda")
dw_tes_aql_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_aql_det.set_dw_options(sqlca,dw_tes_aql_lista,c_sharedata+c_scrollparent,c_default)

dw_det_aql_lista.set_dw_key("cod_azienda")
dw_det_aql_lista.set_dw_key("cod_aql")
dw_det_aql_lista.set_dw_options(sqlca,dw_tes_aql_lista,c_scrollparent,c_default)
dw_det_aql_det.set_dw_options(sqlca,dw_det_aql_lista,c_sharedata+c_scrollparent,c_default)

//iuo_dw_main = dw_tes_aql_lista

dw_folder.fu_FolderCreate(2,4)
dw_folder.fu_SelectTab(1)
end event

on w_tes_aql.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_aql_lista=create dw_tes_aql_lista
this.dw_det_aql_det=create dw_det_aql_det
this.dw_det_aql_lista=create dw_det_aql_lista
this.dw_tes_aql_det=create dw_tes_aql_det
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_aql_lista
this.Control[iCurrent+2]=dw_det_aql_det
this.Control[iCurrent+3]=dw_det_aql_lista
this.Control[iCurrent+4]=dw_tes_aql_det
this.Control[iCurrent+5]=dw_folder
end on

on w_tes_aql.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_aql_lista)
destroy(this.dw_det_aql_det)
destroy(this.dw_det_aql_lista)
destroy(this.dw_tes_aql_det)
destroy(this.dw_folder)
end on

event pc_new;call super::pc_new;//dw_tes_piani_campionamento_lista.setitem(dw_tes_piani_campionamento_lista.getrow(),"data_registrazione",datetime(today()))
//dw_tes_piani_campionamento_lista.setitem(dw_tes_piani_campionamento_lista.getrow(),"data_validita",datetime(today()))
end event

type dw_tes_aql_lista from uo_cs_xx_dw within w_tes_aql
int X=23
int Y=21
int Width=2561
int Height=341
int TabOrder=10
string DataObject="d_tes_aql_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end on

event updatestart;call super::updatestart;if i_extendmode then
	
	integer  li_i
	string   ls_cod_aql

	for li_i = 1 to this.deletedcount()
	   ls_cod_aql = this.getitemstring(li_i, "cod_aql", delete!, true)

	   delete from det_aql
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	cod_aql=:ls_cod_aql;

	next

end if

end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event getfocus;call super::getfocus;if i_extendmode then uo_dw_main = dw_tes_aql_lista
end event

type dw_det_aql_det from uo_cs_xx_dw within w_tes_aql
event pcd_delete pbm_custom42
int X=1395
int Y=501
int Width=1143
int Height=921
int TabOrder=40
string DataObject="d_det_aql_det"
end type

type dw_det_aql_lista from uo_cs_xx_dw within w_tes_aql
event updatestart pbm_dwnupdatestart
event pcd_delete pbm_custom42
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
event pcd_setkey pbm_custom68
event pcd_view pbm_custom75
int X=69
int Y=501
int Width=1143
int Height=921
int TabOrder=30
string DataObject="d_det_aql_lista"
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_aql

ls_cod_aql = dw_tes_aql_lista.getitemstring(dw_tes_aql_lista.getrow(),"cod_aql")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_aql)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_prog_riga
string ls_cod_aql

ls_cod_aql = dw_tes_aql_lista.getitemstring(dw_tes_aql_lista.getrow(),"cod_aql")

select max(prog_riga)
into   :ll_prog_riga
from   det_aql
where  cod_azienda=:s_cs_xx.cod_Azienda
and    cod_aql=:ls_cod_aql;

if isnull(ll_prog_riga) then
	ll_prog_riga= 1
else
	ll_prog_riga ++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_aql", ls_cod_aql)
		SetItem(l_Idx, "prog_riga", ll_prog_riga)
   END IF
NEXT

end event

event pcd_validaterow;call super::pcd_validaterow;double ldd_lim_inf,ldd_lim_sup,ldd_num_accettazione,ldd_num_rifiuto,ldd_num_campionaria

ldd_lim_inf = getitemnumber(getrow(),"lim_inferiore_stock")
ldd_lim_sup = getitemnumber(getrow(),"lim_superiore_stock")
ldd_num_accettazione = getitemnumber(getrow(),"num_accettazione")
ldd_num_rifiuto = getitemnumber(getrow(),"num_rifiuto")
ldd_num_campionaria = getitemnumber(getrow(),"num_campionaria")


if ldd_lim_inf > ldd_lim_sup then
	g_mb.messagebox("Omnia","Il limite inferiore è maggiore del limite superiore!",exclamation!)
	pcca.error=c_valfailed
	return 1
end if

if ldd_lim_inf < ldd_num_campionaria then
	g_mb.messagebox("Omnia","Il limite inferiore è minore della numerosità campionaria!",exclamation!)
	pcca.error=c_valfailed
	return 1
end if

if ldd_num_campionaria < ldd_num_rifiuto then
	g_mb.messagebox("Omnia","La numerosità campionaria è minore del numero rifiuto!",exclamation!)
	pcca.error=c_valfailed
	return 1
end if

if ldd_num_rifiuto < ldd_num_accettazione then
	g_mb.messagebox("Omnia","Il numero rifiuto è minore del numero accettazione!",exclamation!)
	pcca.error=c_valfailed
	return 1
end if

end event

event getfocus;call super::getfocus;if i_extendmode then uo_dw_main = dw_det_aql_lista
end event

type dw_tes_aql_det from uo_cs_xx_dw within w_tes_aql
int X=69
int Y=501
int Width=2469
int Height=921
int TabOrder=20
string DataObject="d_tes_aql_det"
end type

type dw_folder from u_folder within w_tes_aql
int X=23
int Y=381
int Width=2561
int Height=1081
int TabOrder=50
end type

event po_tabclicked;call super::po_tabclicked;choose case i_SelectedTab	
	case 1
		dw_tes_aql_lista.change_dw_current()
		uo_dw_main = dw_tes_aql_lista
	case 2
		dw_tes_aql_lista.change_dw_current()
		uo_dw_main = dw_det_aql_lista
end choose


end event

