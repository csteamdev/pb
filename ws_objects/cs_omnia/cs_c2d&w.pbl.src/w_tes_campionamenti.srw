﻿$PBExportHeader$w_tes_campionamenti.srw
$PBExportComments$Window tes_campionamenti
forward
global type w_tes_campionamenti from w_cs_xx_principale
end type
type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_tes_campionamenti
end type
type cb_stock from cb_stock_ricerca within w_tes_campionamenti
end type
type cb_procedure from cb_apri_manuale within w_tes_campionamenti
end type
type dw_tes_campionamenti_det_2 from uo_cs_xx_dw within w_tes_campionamenti
end type
type dw_folder from u_folder within w_tes_campionamenti
end type
type gb_1 from groupbox within w_tes_campionamenti
end type
type st_tipo_piano from statictext within w_tes_campionamenti
end type
type dw_tes_campionamenti_det_1 from uo_cs_xx_dw within w_tes_campionamenti
end type
end forward

global type w_tes_campionamenti from w_cs_xx_principale
integer width = 2478
integer height = 1424
string title = "Campionamenti"
dw_tes_campionamenti_lista dw_tes_campionamenti_lista
cb_stock cb_stock
cb_procedure cb_procedure
dw_tes_campionamenti_det_2 dw_tes_campionamenti_det_2
dw_folder dw_folder
gb_1 gb_1
st_tipo_piano st_tipo_piano
dw_tes_campionamenti_det_1 dw_tes_campionamenti_det_1
end type
global w_tes_campionamenti w_tes_campionamenti

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_tes_campionamenti_det_1
dw_folder.fu_AssignTab(1, "&Dettaglio", l_Objects[])

l_objects[1] = dw_tes_campionamenti_det_2
l_objects[2] = cb_stock
dw_folder.fu_AssignTab(2, "&Stock", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_tes_campionamenti_lista.set_dw_key("cod_azienda")
dw_tes_campionamenti_lista.set_dw_key("anno_reg_campionamenti")
dw_tes_campionamenti_lista.set_dw_key("num_reg_campionamenti")
dw_tes_campionamenti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_campionamenti_det_1.set_dw_options(sqlca,dw_tes_campionamenti_lista,c_sharedata & 
													  + c_scrollparent,c_default)
dw_tes_campionamenti_det_2.set_dw_options(sqlca,dw_tes_campionamenti_lista,c_sharedata & 
													  + c_scrollparent,c_default)

dw_folder.fu_SelectTab(1)
iuo_dw_main = dw_tes_campionamenti_lista

//if s_cs_xx.parametri.parametro_s_1 = "det_campionamenti" then
//	cb_dettagli.visible = false
//elseif s_cs_xx.parametri.parametro_s_1 = "acc_materiali" then
//	cb_dettagli.visible = true
//end if
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_1,"cod_piano_campionamento",sqlca,&
                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento in (select cod_piano_campionamento from tes_piani_campionamento where cod_azienda = '" + s_cs_xx.cod_azienda + "' and data_validita <= '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "')")
					  					                   
f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_tes_campionamenti.create
int iCurrent
call super::create
this.dw_tes_campionamenti_lista=create dw_tes_campionamenti_lista
this.cb_stock=create cb_stock
this.cb_procedure=create cb_procedure
this.dw_tes_campionamenti_det_2=create dw_tes_campionamenti_det_2
this.dw_folder=create dw_folder
this.gb_1=create gb_1
this.st_tipo_piano=create st_tipo_piano
this.dw_tes_campionamenti_det_1=create dw_tes_campionamenti_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_campionamenti_lista
this.Control[iCurrent+2]=this.cb_stock
this.Control[iCurrent+3]=this.cb_procedure
this.Control[iCurrent+4]=this.dw_tes_campionamenti_det_2
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.gb_1
this.Control[iCurrent+7]=this.st_tipo_piano
this.Control[iCurrent+8]=this.dw_tes_campionamenti_det_1
end on

on w_tes_campionamenti.destroy
call super::destroy
destroy(this.dw_tes_campionamenti_lista)
destroy(this.cb_stock)
destroy(this.cb_procedure)
destroy(this.dw_tes_campionamenti_det_2)
destroy(this.dw_folder)
destroy(this.gb_1)
destroy(this.st_tipo_piano)
destroy(this.dw_tes_campionamenti_det_1)
end on

event pc_new;call super::pc_new;integer li_row_orig, li_row_dest
string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
datetime ldt_data_stock
long ll_prog_stock

li_row_dest = dw_tes_campionamenti_lista.getrow()
dw_tes_campionamenti_lista.setitem(li_row_dest,"data_registrazione",datetime(today()))
if isvalid(s_cs_xx.parametri.parametro_dw_1) and s_cs_xx.parametri.parametro_s_1 = "acc_materiali" then
	li_row_orig = s_cs_xx.parametri.parametro_dw_1.getrow()

	ls_cod_prodotto = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_prodotto")
	ls_cod_deposito = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_deposito")
	ls_cod_ubicazione = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_ubicazione")
	ls_cod_lotto = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_lotto")
	ldt_data_stock = s_cs_xx.parametri.parametro_dw_1.getitemdatetime(li_row_orig, "data_stock")
	ll_prog_stock = s_cs_xx.parametri.parametro_dw_1.getitemnumber(li_row_orig, "prog_stock")
	
	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_prodotto", ls_cod_prodotto)	
	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_deposito", ls_cod_deposito)	
	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_ubicazione", ls_cod_ubicazione)	
	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_lotto", ls_cod_lotto)	
	dw_tes_campionamenti_lista.setitem(li_row_dest, "data_stock", ldt_data_stock)	
	dw_tes_campionamenti_lista.setitem(li_row_dest, "prog_stock", ll_prog_stock)	
end if
end event

event close;call super::close;//if isvalid(w_det_campionamenti.dw_tes_campionamenti_lista) then 
//	w_det_campionamenti.dw_tes_campionamenti_lista.change_dw_current()
//	w_det_campionamenti.triggerevent("pc_retrieve")
//end if
end event

type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_tes_campionamenti
integer x = 23
integer y = 20
integer width = 1760
integer height = 500
integer taborder = 50
string dataobject = "d_tes_campionamenti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
	
end on

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_tes_campionamenti_det_2.object.b_ricerca_prodotto.enabled = false
	cb_stock.enabled = false
//	cb_dettagli.enabled = true
	cb_procedure.enabled = true
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	dw_tes_campionamenti_det_2.object.b_ricerca_prodotto.enabled = false
	cb_stock.enabled = false
//	cb_dettagli.enabled = true
	cb_procedure.enabled = true
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_num_reg_campionamenti,ll_anno

ll_anno = year(today())

select max(num_reg_campionamenti) 
into:ll_num_reg_campionamenti 
from tes_campionamenti 
where cod_azienda=:s_cs_xx.cod_azienda 
and anno_reg_campionamenti=:ll_anno; 

if isnull(ll_num_reg_campionamenti) then
	ll_num_reg_campionamenti = 1
else
	ll_num_reg_campionamenti++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "anno_reg_campionamenti", year(today()))
		SetItem(l_Idx, "num_reg_campionamenti", ll_num_reg_campionamenti)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_tes_campionamenti_det_2.object.b_ricerca_prodotto.enabled = true
	cb_stock.enabled = true
//	cb_dettagli.enabled = false
	cb_procedure.enabled = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	dw_tes_campionamenti_det_2.object.b_ricerca_prodotto.enabled = true
	cb_stock.enabled = true
//	cb_dettagli.enabled = false
	cb_procedure.enabled = false
end if
end event

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
	
	integer li_i, li_anno_reg_campionamenti
	long ll_num_reg_campionamenti

	for li_i = 1 to this.deletedcount()
	   li_anno_reg_campionamenti = this.getitemnumber(li_i, "anno_reg_campionamenti", delete!, true)
   	ll_num_reg_campionamenti = this.getitemnumber(li_i, "num_reg_campionamenti", delete!, true)

	   delete from det_campionamenti
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	anno_reg_campionamenti=:li_anno_reg_campionamenti
		and 	ll_num_reg_campionamenti=:ll_num_reg_campionamenti;

		update collaudi  
	   set    data_carta = null,   
      	    cod_carta = ''  
   	where  collaudi.anno_reg_campionamenti = :li_anno_reg_campionamenti 
		and    collaudi.num_reg_campionamenti = :ll_num_reg_campionamenti;

	next

end if

end on

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_cod_piano_campionamento,ls_flag_tipo_piano
	datetime ld_data_validita
	
	ls_cod_piano_campionamento = getitemstring(getrow(),"cod_piano_campionamento")
	ld_data_validita = getitemdatetime(getrow(),"data_validita")

	select flag_tipo_piano
	into   :ls_flag_tipo_piano
	from   tes_piani_campionamento
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_piano_campionamento=:ls_cod_piano_campionamento
	and    data_validita=:ld_data_validita;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return
	end if

	choose case ls_flag_tipo_piano
		case 'A'
			st_tipo_piano.text = "Accettazione"

		case 'P'
			st_tipo_piano.text = "Produzione"

		case 'F'
			st_tipo_piano.text = "Finale"

		case else
			st_tipo_piano.text = ""

	end choose
end if
 
end event

type cb_stock from cb_stock_ricerca within w_tes_campionamenti
integer x = 2263
integer y = 760
integer width = 73
integer height = 80
integer taborder = 30
boolean enabled = false
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_10 = dw_tes_campionamenti_det_2.getitemstring(dw_tes_campionamenti_det_2.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_tes_campionamenti_det_2.getitemstring(dw_tes_campionamenti_det_2.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_tes_campionamenti_det_2.getitemstring(dw_tes_campionamenti_det_2.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_tes_campionamenti_det_2.getitemstring(dw_tes_campionamenti_det_2.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_tes_campionamenti_det_2.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end event

type cb_procedure from cb_apri_manuale within w_tes_campionamenti
event clicked pbm_bnclicked
integer x = 1806
integer y = 20
integer height = 80
integer taborder = 80
string text = "&Procedure"
end type

event clicked;call super::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLG")
end event

type dw_tes_campionamenti_det_2 from uo_cs_xx_dw within w_tes_campionamenti
event itemchanged pbm_dwnitemchange
integer x = 46
integer y = 640
integer width = 2331
integer taborder = 70
string dataobject = "d_tes_campionamenti_det_2"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tes_campionamenti_det_2,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_tes_campionamenti
integer x = 23
integer y = 540
integer width = 2400
integer height = 756
integer taborder = 20
boolean border = false
end type

type gb_1 from groupbox within w_tes_campionamenti
integer x = 1806
integer y = 140
integer width = 617
integer height = 200
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Tipo Campionamento"
end type

type st_tipo_piano from statictext within w_tes_campionamenti
integer x = 1829
integer y = 220
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Accettazione"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_tes_campionamenti_det_1 from uo_cs_xx_dw within w_tes_campionamenti
integer x = 46
integer y = 640
integer width = 2309
integer height = 540
integer taborder = 60
string dataobject = "d_tes_campionamenti_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	datetime ld_data_validita,ld_td
	string ls_cod_prodotto,ls_ch

	choose case i_colname
		case  "cod_piano_campionamento" 
			setnull(ls_cod_prodotto)
			ld_td=getitemdatetime(getrow(),"data_registrazione")

			select cod_prodotto 
			into:ls_ch
			from det_piani_campionamento 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and cod_piano_campionamento=:i_coltext
			and data_validita <= :ld_td;
			
			if ls_ch <> "" then
				if s_cs_xx.parametri.parametro_s_1 = "det_campionamenti" then
					f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2,"cod_prodotto",sqlca,&
									  "anag_prodotti","cod_prodotto","des_prodotto",&
									  "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in(select cod_prodotto from det_piani_campionamento where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento='" + i_coltext + "' and data_validita<='" + string(datetime(today()),s_cs_xx.db_funzioni.formato_data) + "')")
				end if
			else
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
				pcca.error = c_valfailed
				return
			end if
         		
			SELECT max(data_validita)
	      INTO   :ld_data_validita  
		   FROM   tes_piani_campionamento  
   		WHERE  cod_azienda = :s_cs_xx.cod_azienda 
			AND 	 tes_piani_campionamento.cod_piano_campionamento = :i_coltext
			and    data_validita <=:ld_td ;
			
			setitem(getrow(),"data_validita",ld_data_validita)
			if s_cs_xx.parametri.parametro_s_1 = "det_campionamenti" then
				setitem(getrow(),"cod_prodotto",ls_cod_prodotto)
			end if		
	end choose
end if
end event

