﻿$PBExportHeader$w_tes_campionamenti_blob.srw
$PBExportComments$Window blob tes campionamenti
forward
global type w_tes_campionamenti_blob from w_cs_xx_principale
end type
type dw_tes_campionamenti_blob from uo_cs_xx_dw within w_tes_campionamenti_blob
end type
type cb_note_esterne from commandbutton within w_tes_campionamenti_blob
end type
end forward

global type w_tes_campionamenti_blob from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1906
integer height = 1292
string title = "Documenti  Campionamento"
dw_tes_campionamenti_blob dw_tes_campionamenti_blob
cb_note_esterne cb_note_esterne
end type
global w_tes_campionamenti_blob w_tes_campionamenti_blob

on w_tes_campionamenti_blob.create
int iCurrent
call super::create
this.dw_tes_campionamenti_blob=create dw_tes_campionamenti_blob
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_campionamenti_blob
this.Control[iCurrent+2]=this.cb_note_esterne
end on

on w_tes_campionamenti_blob.destroy
call super::destroy
destroy(this.dw_tes_campionamenti_blob)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;dw_tes_campionamenti_blob.set_dw_key("cod_azienda")

dw_tes_campionamenti_blob.set_dw_key("anno_reg_campionamenti")

dw_tes_campionamenti_blob.set_dw_key("num_reg_campionamenti")

dw_tes_campionamenti_blob.set_dw_key("prog_blob")

dw_tes_campionamenti_blob.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)


iuo_dw_main = dw_tes_campionamenti_blob

cb_note_esterne.enabled = false







end event

type dw_tes_campionamenti_blob from uo_cs_xx_dw within w_tes_campionamenti_blob
integer x = 23
integer y = 20
integer width = 1829
integer height = 1060
integer taborder = 20
string dataobject = "d_tes_campionamenti_blob"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore,ll_num_registrazione
integer li_anno_registrazione



li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_campionamenti")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_campionamenti")

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_registrazione,ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_num_registrazione
integer li_anno_registrazione,ll_prog_blob

li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_campionamenti")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_campionamenti")

select max(prog_blob)
into   :ll_prog_blob
from   tes_campionamenti_blob
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_reg_campionamenti = :li_anno_registrazione and    
		 num_reg_campionamenti = :ll_num_registrazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

if ll_prog_blob = 0 or isnull(ll_prog_blob) then
	ll_prog_blob = 1
else
	ll_prog_blob++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
	   SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "anno_reg_campionamenti", li_anno_registrazione)
		SetItem(l_Idx, "num_reg_campionamenti", ll_num_registrazione)
		SetItem(l_Idx, "prog_blob", ll_prog_blob)
		ll_prog_blob++
   END IF
NEXT
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
		if not isnull(getitemnumber(getrow(), "anno_reg_campionamenti")) then
			cb_note_esterne.enabled = true
		else
			cb_note_esterne.enabled = false
		end if
	end if
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
		if not isnull(getitemnumber(getrow(), "anno_reg_campionamenti")) then
			cb_note_esterne.enabled = true
		else
			cb_note_esterne.enabled = false
		end if
	end if
end if
end event

type cb_note_esterne from commandbutton within w_tes_campionamenti_blob
integer x = 1486
integer y = 1100
integer width = 366
integer height = 80
integer taborder = 21
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;integer li_anno_registrazione, li_risposta
long ll_prog_blob,ll_num_registrazione,ll_i
blob lbl_null
string ls_db
transaction sqlcb

if dw_tes_campionamenti_blob.getrow() > 0 then
	
	setnull(lbl_null)
	
	ll_i = dw_tes_campionamenti_blob.getrow()
	
	li_anno_registrazione = dw_tes_campionamenti_blob.getitemnumber(ll_i, "anno_reg_campionamenti")
	
	ll_num_registrazione = dw_tes_campionamenti_blob.getitemnumber(ll_i, "num_reg_campionamenti")
	
	ll_prog_blob = dw_tes_campionamenti_blob.getitemnumber(ll_i, "prog_blob")
	
	ls_db = f_db()
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
		selectblob blob
		into       :s_cs_xx.parametri.parametro_bl_1
		from       tes_campionamenti_blob
		where      cod_azienda = :s_cs_xx.cod_azienda 
		and        anno_reg_campionamenti = :li_anno_registrazione
		and        num_reg_campionamenti = :ll_num_registrazione
		and        prog_blob = :ll_prog_blob
		using      sqlcb;
		
		if sqlcb.sqlcode <> 0 then
			s_cs_xx.parametri.parametro_bl_1 = lbl_null
		end if		
		
		destroy sqlcb
		
	else
		
		selectblob blob
		into       :s_cs_xx.parametri.parametro_bl_1
		from       tes_campionamenti_blob
		where      cod_azienda = :s_cs_xx.cod_azienda 
		and        anno_reg_campionamenti = :li_anno_registrazione
		and        num_reg_campionamenti = :ll_num_registrazione
		and        prog_blob = :ll_prog_blob;
		
		if sqlca.sqlcode <> 0 then
			s_cs_xx.parametri.parametro_bl_1 = lbl_null
		end if
		
	end if
	
	window_open(w_ole, 0)
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob tes_campionamenti_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda 
			and        anno_reg_campionamenti = :li_anno_registrazione
			and        num_reg_campionamenti = :ll_num_registrazione
			and        prog_blob = :ll_prog_blob
			using      sqlcb;			
			
			destroy sqlcb;
			
		else
		
			updateblob tes_campionamenti_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda 
			and        anno_reg_campionamenti = :li_anno_registrazione
			and        num_reg_campionamenti = :ll_num_registrazione
			and        prog_blob = :ll_prog_blob;
	
		end if		
		
		commit;
	end if
end if
end event

