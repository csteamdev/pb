﻿$PBExportHeader$w_tes_campionamenti_finali.srw
$PBExportComments$Window campionamenti finali
forward
global type w_tes_campionamenti_finali from w_cs_xx_principale
end type
type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_tes_campionamenti_finali
end type
type cb_1 from cb_prod_ricerca within w_tes_campionamenti_finali
end type
type cb_3 from commandbutton within w_tes_campionamenti_finali
end type
type cb_2 from cb_stock_ricerca within w_tes_campionamenti_finali
end type
type dw_tes_campionamenti_dett from uo_cs_xx_dw within w_tes_campionamenti_finali
end type
end forward

global type w_tes_campionamenti_finali from w_cs_xx_principale
integer width = 2277
integer height = 1760
string title = "Campionamenti Finali"
dw_tes_campionamenti_lista dw_tes_campionamenti_lista
cb_1 cb_1
cb_3 cb_3
cb_2 cb_2
dw_tes_campionamenti_dett dw_tes_campionamenti_dett
end type
global w_tes_campionamenti_finali w_tes_campionamenti_finali

event pc_setwindow;call super::pc_setwindow;string   ls_cod_piano_campionamento
datetime ld_oggi

	dw_tes_campionamenti_lista.set_dw_key("cod_azienda")
	dw_tes_campionamenti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
	dw_tes_campionamenti_dett.set_dw_options(sqlca,dw_tes_campionamenti_lista,c_sharedata+c_scrollparent,c_default)

	iuo_dw_main = dw_tes_campionamenti_lista

	cb_1.enabled = false
	cb_2.enabled = false
	cb_3.enabled = false

	ld_oggi = datetime(today())

//	select cod_piano_campionamento into:ls_cod_piano_campionamento
//	from	 tes_piani_campionamento
//	where  data_validita <=:ld_oggi;
//
//	if isnull(ls_cod_piano_campionamento) or ls_cod_piano_campionamento = "" then
//		messagebox("Omnia","Attenzione, non esiste nessun piano di campionamento valido. Creare un nuovo piano di campionamento.",exclamation!)
//		triggerevent("pc_close")
//
//	end if
//
//
end event

event pc_setddlb;call super::pc_setddlb;string ls_cod_prodotto, ls_cod_piano_campionamento

f_PO_LoadDDDW_DW(dw_tes_campionamenti_dett,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_dett,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_dett,"cod_piano_campionamento",sqlca,&
                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento in (select cod_piano_campionamento from tes_piani_campionamento where cod_azienda = '" + s_cs_xx.cod_azienda + "' and data_validita <= '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "')")                 

end event

on w_tes_campionamenti_finali.create
int iCurrent
call super::create
this.dw_tes_campionamenti_lista=create dw_tes_campionamenti_lista
this.cb_1=create cb_1
this.cb_3=create cb_3
this.cb_2=create cb_2
this.dw_tes_campionamenti_dett=create dw_tes_campionamenti_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_campionamenti_lista
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_3
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.dw_tes_campionamenti_dett
end on

on w_tes_campionamenti_finali.destroy
call super::destroy
destroy(this.dw_tes_campionamenti_lista)
destroy(this.cb_1)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.dw_tes_campionamenti_dett)
end on

type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_tes_campionamenti_finali
integer x = 23
integer y = 20
integer width = 2194
integer height = 340
integer taborder = 30
string dataobject = "d_tes_campionamenti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
	
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
	cb_1.enabled = false
	cb_2.enabled = false
	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_azienda"))) then
		cb_3.enabled = true
	else
		cb_3.enabled = false
	end if
end if
end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
	cb_1.enabled = false
	cb_2.enabled = false

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_azienda"))) then
		cb_3.enabled = true
	else
		cb_3.enabled = false
	end if

end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx,ll_num_reg_campionamenti,ll_anno

ll_anno = year(today())
select max(num_reg_campionamenti) 
into:ll_num_reg_campionamenti 
from tes_campionamenti 
where cod_azienda=:s_cs_xx.cod_azienda 
and anno_reg_campionamenti=:ll_anno; 

if isnull(ll_num_reg_campionamenti) then
	ll_num_reg_campionamenti = 1
else
	ll_num_reg_campionamenti++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "anno_reg_campionamenti", year(today()))
		SetItem(l_Idx, "num_reg_campionamenti", ll_num_reg_campionamenti)
   END IF
NEXT

end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
	cb_1.enabled = true
	cb_2.enabled = true
	cb_3.enabled = false
end if
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
	cb_1.enabled = true
	cb_2.enabled = true
	cb_3.enabled = false
end if
end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
	
	integer li_i, li_anno_reg_campionamenti
	long ll_num_reg_campionamenti

	for li_i = 1 to this.deletedcount()
	   li_anno_reg_campionamenti = this.getitemnumber(li_i, "anno_reg_campionamenti", delete!, true)
   	ll_num_reg_campionamenti = this.getitemnumber(li_i, "num_reg_campionamenti", delete!, true)

	   delete from det_campionamenti
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	anno_reg_campionamenti=:li_anno_reg_campionamenti
		and 	ll_num_reg_campionamenti=:ll_num_reg_campionamenti;

		update collaudi  
	   set    data_carta = null,   
      	    cod_carta = ''  
   	where  collaudi.anno_reg_campionamenti = :li_anno_reg_campionamenti 
		and    collaudi.num_reg_campionamenti = :ll_num_reg_campionamenti;

	next

end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;if i_extendmode then

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_azienda"))) then
		cb_3.enabled = true
	else
		cb_3.enabled = false
	end if

end if
end on

type cb_1 from cb_prod_ricerca within w_tes_campionamenti_finali
integer x = 2126
integer y = 740
integer width = 73
integer height = 80
integer taborder = 20
end type

on getfocus;call cb_prod_ricerca::getfocus;dw_tes_campionamenti_dett.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end on

type cb_3 from commandbutton within w_tes_campionamenti_finali
integer x = 1851
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;string   ls_cod_piano_campionamento,ls_cod_test,ls_cod_prodotto
long     ll_prog_riga_piani,ll_num_reg_campionamenti,ll_prog_riga_campionamenti,ll_conteggio,ll_conteggio_1
datetime ld_data_validita
integer  li_anno_reg_campionamenti

ld_data_validita = datetime(today())
ls_cod_piano_campionamento = dw_tes_campionamenti_dett.getitemstring(dw_tes_campionamenti_dett.getrow(),"cod_piano_campionamento")
li_anno_reg_campionamenti = dw_tes_campionamenti_dett.getitemnumber(dw_tes_campionamenti_dett.getrow(),"anno_reg_campionamenti")
ll_num_reg_campionamenti = dw_tes_campionamenti_dett.getitemnumber(dw_tes_campionamenti_dett.getrow(),"num_reg_campionamenti")
ll_conteggio = 0
ll_conteggio_1 = 0

setnull(ls_cod_test)
select  cod_test
into	  :ls_cod_test
from	  det_piani_campionamento
where	  cod_azienda = :s_cs_xx.cod_azienda
and 	  cod_piano_campionamento = :ls_cod_piano_campionamento
and 	  data_validita <= :ld_data_validita
and	  flag_coll_finale = 'S';

if isnull(ls_cod_test) then
	g_mb.messagebox("Omnia","Attenzione! Il Piano di Campionamento non prevede alcun collaudo finale.",exclamation!)
	return
end if


declare righe_det_piani_campionamento cursor for 
select  cod_test,
		  cod_prodotto
from	  det_piani_campionamento
where	  cod_azienda = :s_cs_xx.cod_azienda
and 	  cod_piano_campionamento = :ls_cod_piano_campionamento
and 	  data_validita <= :ld_data_validita
and	  flag_coll_finale = 'N';
								
open righe_det_piani_campionamento;

do while 1 = 1
 	fetch righe_det_piani_campionamento into :ls_cod_test, :ls_cod_prodotto;
	ll_conteggio_1++
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or (sqlca.sqlcode <> 0) then 
		if ll_conteggio_1 = 1 then
			ll_conteggio = 1
		end if
		exit
	end if

	setnull(ll_prog_riga_campionamenti)
		
	select prog_riga_campionamenti
	into   :ll_prog_riga_campionamenti
	from det_campionamenti
	where  cod_azienda =:s_cs_xx.cod_azienda
	and 	 anno_reg_campionamenti =:li_anno_reg_campionamenti
	and	 num_reg_campionamenti =:ll_num_reg_campionamenti
	and	 cod_test =:ls_cod_test	
	and    cod_prodotto =:ls_cod_prodotto;
	
	if isnull(ll_prog_riga_campionamenti) then ll_conteggio++

loop

close righe_det_piani_campionamento;

if ll_conteggio > 0 then
	g_mb.messagebox("Omnia","Attenzione! Non è possibile eseguire un campionamento finale poichè mancano alcuni test previsti dal piano di campionamento.",exclamation!)
	return
end if 

s_cs_xx.parametri.parametro_s_1 = "S"
window_open_parm(w_selezione_piano_campionamento,-1,dw_tes_campionamenti_dett)
end event

type cb_2 from cb_stock_ricerca within w_tes_campionamenti_finali
integer x = 2126
integer y = 820
integer width = 73
integer height = 80
integer taborder = 10
end type

event clicked;call cb_stock_ricerca::clicked;s_cs_xx.parametri.parametro_s_10 = dw_tes_campionamenti_dett.getitemstring(dw_tes_campionamenti_dett.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_tes_campionamenti_dett.getitemstring(dw_tes_campionamenti_dett.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_tes_campionamenti_dett.getitemstring(dw_tes_campionamenti_dett.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_tes_campionamenti_dett.getitemstring(dw_tes_campionamenti_dett.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_tes_campionamenti_dett.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end event

type dw_tes_campionamenti_dett from uo_cs_xx_dw within w_tes_campionamenti_finali
integer y = 380
integer width = 2217
integer height = 1160
integer taborder = 40
string dataobject = "d_tes_campionamenti_dett"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	datetime ld_data_validita,ld_td
	string ls_cod_prodotto,ls_ch

	choose case i_colname
		case  "cod_piano_campionamento" 
			setnull(ls_cod_prodotto)
			ld_td=getitemdatetime(getrow(),"data_registrazione")

			select cod_prodotto 
			into:ls_ch
			from det_piani_campionamento 
			where cod_azienda=:s_cs_xx.cod_azienda 
			and cod_piano_campionamento=:i_coltext
			and data_validita <= :ld_td;
			
			if ls_ch <> "" then
				f_PO_LoadDDDW_DW(dw_tes_campionamenti_dett,"cod_prodotto",sqlca,&
   				              "anag_prodotti","cod_prodotto","des_prodotto",&
      	   			        "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in(select cod_prodotto from det_piani_campionamento where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento='" + i_coltext + "' and data_validita<='" + string(today(),s_cs_xx.db_funzioni.formato_data) + "')")
			else
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
				pcca.error = c_valfailed
				return
			end if
         		
			SELECT max(data_validita)
	      INTO   :ld_data_validita  
		   FROM   tes_piani_campionamento  
   		WHERE  cod_azienda = :s_cs_xx.cod_azienda 
			AND 	 tes_piani_campionamento.cod_piano_campionamento = :i_coltext
			and    data_validita <=:ld_td ;
			
			setitem(getrow(),"data_validita",ld_data_validita)
			setitem(getrow(),"cod_prodotto",ls_cod_prodotto)
			
	end choose
end if
end event

on pcd_delete;call uo_cs_xx_dw::pcd_delete;if i_extendmode then
	
	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_azienda"))) then
		cb_3.enabled = true
	else
		cb_3.enabled = false
	end if

end if
end on

