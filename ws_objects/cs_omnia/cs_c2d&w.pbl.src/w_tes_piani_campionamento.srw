﻿$PBExportHeader$w_tes_piani_campionamento.srw
$PBExportComments$Window tes_piani_campionamento
forward
global type w_tes_piani_campionamento from w_cs_xx_principale
end type
type dw_tes_piani_campionamento_dett from uo_cs_xx_dw within w_tes_piani_campionamento
end type
type dw_tes_piani_campionamento_lista from uo_cs_xx_dw within w_tes_piani_campionamento
end type
type cb_1 from commandbutton within w_tes_piani_campionamento
end type
end forward

global type w_tes_piani_campionamento from w_cs_xx_principale
integer width = 2427
integer height = 1788
string title = "Piani di Campionamento"
dw_tes_piani_campionamento_dett dw_tes_piani_campionamento_dett
dw_tes_piani_campionamento_lista dw_tes_piani_campionamento_lista
cb_1 cb_1
end type
global w_tes_piani_campionamento w_tes_piani_campionamento

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_piani_campionamento_dett,"cod_piano_campionamento",sqlca,&
                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tes_piani_campionamento_lista.set_dw_key("cod_azienda")
dw_tes_piani_campionamento_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_piani_campionamento_dett.set_dw_options(sqlca,dw_tes_piani_campionamento_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_piani_campionamento_lista
cb_1.enabled = false
end on

on w_tes_piani_campionamento.create
int iCurrent
call super::create
this.dw_tes_piani_campionamento_dett=create dw_tes_piani_campionamento_dett
this.dw_tes_piani_campionamento_lista=create dw_tes_piani_campionamento_lista
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_piani_campionamento_dett
this.Control[iCurrent+2]=this.dw_tes_piani_campionamento_lista
this.Control[iCurrent+3]=this.cb_1
end on

on w_tes_piani_campionamento.destroy
call super::destroy
destroy(this.dw_tes_piani_campionamento_dett)
destroy(this.dw_tes_piani_campionamento_lista)
destroy(this.cb_1)
end on

event pc_new;call super::pc_new;dw_tes_piani_campionamento_lista.setitem(dw_tes_piani_campionamento_lista.getrow(),"data_registrazione",datetime(today()))
dw_tes_piani_campionamento_lista.setitem(dw_tes_piani_campionamento_lista.getrow(),"data_validita",datetime(today()))
end event

event getfocus;call super::getfocus;dw_tes_piani_campionamento_dett.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
s_cs_xx.parametri.parametro_tipo_ricerca = 2
end event

type dw_tes_piani_campionamento_dett from uo_cs_xx_dw within w_tes_piani_campionamento
integer x = 23
integer y = 540
integer width = 2354
integer height = 1040
integer taborder = 20
string dataobject = "d_tes_piani_campionamento_dett"
borderstyle borderstyle = styleraised!
end type

on pcd_delete;call uo_cs_xx_dw::pcd_delete;if i_extendmode then

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_piano_campionamento"))) then
		cb_1.enabled = true
	else
		cb_1.enabled = false
	end if

end if
end on

event itemchanged;call super::itemchanged;string ls_flag_tipo_scad_campionamento

if i_extendmode then
	
	choose case i_colname
			
		case "flag_tipo_scad_campionamento"
			
			choose case i_coltext
					
				case 'T'

					dw_tes_piani_campionamento_dett.Object.periodicita.TabSequence = 80
					
					dw_tes_piani_campionamento_dett.Object.frequenza.TabSequence = 90
					
					dw_tes_piani_campionamento_dett.Object.periodicita.Background.Color = RGB(255, 255, 255)
					
					dw_tes_piani_campionamento_dett.Object.frequenza.Background.Color = RGB(255, 255, 255)					
					
				case else
					
					setitem( getrow(), "periodicita", "G")
					
					setitem( getrow(), "frequenza", "0")
					
					dw_tes_piani_campionamento_dett.Object.periodicita.TabSequence = 0
					
					dw_tes_piani_campionamento_dett.Object.frequenza.TabSequence = 0
					
					dw_tes_piani_campionamento_dett.Object.periodicita.Background.Color = RGB(192, 192, 192)
					
					dw_tes_piani_campionamento_dett.Object.frequenza.Background.Color = RGB(192, 192, 192)
				
			end choose
		
	end choose				
	
end if
end event

event pcd_modify;call super::pcd_modify;string ls_flag

ls_flag = getitemstring( getrow(), "flag_tipo_scad_campionamento")

choose case ls_flag
		
	case 'T'

		dw_tes_piani_campionamento_dett.Object.periodicita.TabSequence = 80
					
		dw_tes_piani_campionamento_dett.Object.frequenza.TabSequence = 90
					
		dw_tes_piani_campionamento_dett.Object.periodicita.Background.Color = RGB(255, 255, 255)
				
		dw_tes_piani_campionamento_dett.Object.frequenza.Background.Color = RGB(255, 255, 255)					
					
	case else
					
		setitem( getrow(), "periodicita", "G")
					
		setitem( getrow(), "frequenza", "0")
					
		dw_tes_piani_campionamento_dett.Object.periodicita.TabSequence = 0
					
		dw_tes_piani_campionamento_dett.Object.frequenza.TabSequence = 0
					
		dw_tes_piani_campionamento_dett.Object.periodicita.Background.Color = RGB(192, 192, 192)
					
		dw_tes_piani_campionamento_dett.Object.frequenza.Background.Color = RGB(192, 192, 192)
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_piani_campionamento_dett,"cod_fornitore")
end choose
end event

type dw_tes_piani_campionamento_lista from uo_cs_xx_dw within w_tes_piani_campionamento
integer x = 23
integer y = 20
integer width = 2354
integer height = 500
integer taborder = 10
string dataobject = "d_tes_piani_campionamento_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_delete;call super::pcd_delete;if i_extendmode then
dw_tes_piani_campionamento_dett.object.b_ricerca_fornitore.enabled = false
	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_piano_campionamento"))) then
		cb_1.enabled = true
	else
		cb_1.enabled = false
	end if

end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
dw_tes_piani_campionamento_dett.object.b_ricerca_fornitore.enabled = false
	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_piano_campionamento"))) then
		cb_1.enabled = true
	else
		cb_1.enabled = false
	end if

end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
dw_tes_piani_campionamento_dett.object.b_ricerca_fornitore.enabled = true
	cb_1.enabled = false
end if
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end on

event updatestart;call super::updatestart;if i_extendmode then
	
	integer  li_i
	string   ls_cod_piano_campionamento
	datetime ld_data_validita

	for li_i = 1 to this.deletedcount()
	   ls_cod_piano_campionamento = this.getitemstring(li_i, "cod_piano_campionamento", delete!, true)
   	ld_data_validita = this.getitemdatetime(li_i, "data_validita", delete!, true)

	   delete from det_piani_campionamento
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	cod_piano_campionamento=:ls_cod_piano_campionamento
		and 	data_validita=:ld_data_validita;

	next

end if

end event

event pcd_setkey;call super::pcd_setkey;long  l_idx, ll_max
string ls_cod_piano_campionamento
datetime ldt_data_validita

for l_idx = 1 to rowcount()
   if isnull(getitemstring(l_idx, "cod_azienda")) then
      setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
	if isnull(getitemnumber(l_idx, "prog_piano_campionamento")) or getitemnumber(l_idx, "prog_piano_campionamento") < 1 then
		ls_cod_piano_campionamento = getitemstring(l_idx, "cod_piano_campionamento")
		ldt_data_validita = getitemdatetime(l_idx, "data_validita")
		
		select max(prog_piano_campionamento)
		into   :ll_max
		from   tes_piani_campionamento
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_piano_campionamento = :ls_cod_piano_campionamento and
				 data_validita = :ldt_data_validita;
		
		if isnull(ll_max) or ll_max < 1 then
			ll_max = 1
		else
			ll_max ++
		end if
		
      setitem(l_idx, "prog_piano_campionamento", ll_max)
		
	end if

next

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
dw_tes_piani_campionamento_dett.object.b_ricerca_fornitore.enabled = true
	cb_1.enabled = false
end if
end event

type cb_1 from commandbutton within w_tes_piani_campionamento
integer x = 2011
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;window_open_parm(w_det_piani_campionamento, -1, dw_tes_piani_campionamento_lista)
end event

