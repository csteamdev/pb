﻿$PBExportHeader$w_test_campionamenti.srw
$PBExportComments$testata dei campionamenti (creati in base ai piani)
forward
global type w_test_campionamenti from w_cs_xx_principale
end type
type cb_ricerca_campione from cb_prod_ricerca within w_test_campionamenti
end type
type cb_1 from cb_piani_campionamento_ricerca within w_test_campionamenti
end type
type cb_doc_anomalia from commandbutton within w_test_campionamenti
end type
type cb_report from commandbutton within w_test_campionamenti
end type
type cb_dettaglio from commandbutton within w_test_campionamenti
end type
type cb_nc from commandbutton within w_test_campionamenti
end type
type cb_stock from cb_stock_ricerca within w_test_campionamenti
end type
type cb_ricerca_campione_filtro from cb_prod_ricerca within w_test_campionamenti
end type
type cb_ricerca from commandbutton within w_test_campionamenti
end type
type cb_reset from commandbutton within w_test_campionamenti
end type
type dw_tes_campionamenti_det_1 from uo_cs_xx_dw within w_test_campionamenti
end type
type dw_folder from u_folder within w_test_campionamenti
end type
type dw_tes_campionamenti_det_3 from uo_cs_xx_dw within w_test_campionamenti
end type
type dw_tes_campionamenti_det_2 from uo_cs_xx_dw within w_test_campionamenti
end type
type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_test_campionamenti
end type
type dw_folder_search from u_folder within w_test_campionamenti
end type
type dw_ricerca from uo_dw_search within w_test_campionamenti
end type
end forward

global type w_test_campionamenti from w_cs_xx_principale
integer width = 2779
integer height = 2216
string title = "Campionamenti"
cb_ricerca_campione cb_ricerca_campione
cb_1 cb_1
cb_doc_anomalia cb_doc_anomalia
cb_report cb_report
cb_dettaglio cb_dettaglio
cb_nc cb_nc
cb_stock cb_stock
cb_ricerca_campione_filtro cb_ricerca_campione_filtro
cb_ricerca cb_ricerca
cb_reset cb_reset
dw_tes_campionamenti_det_1 dw_tes_campionamenti_det_1
dw_folder dw_folder
dw_tes_campionamenti_det_3 dw_tes_campionamenti_det_3
dw_tes_campionamenti_det_2 dw_tes_campionamenti_det_2
dw_tes_campionamenti_lista dw_tes_campionamenti_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
end type
global w_test_campionamenti w_test_campionamenti

forward prototypes
public function string wf_trova_prodotto (string fs_cod_piano, datetime fdt_data_validita)
end prototypes

public function string wf_trova_prodotto (string fs_cod_piano, datetime fdt_data_validita);string ls_cod_prodotto

declare cu_prodotto cursor for
	select cod_prodotto 	
	from   det_piani_campionamento 
	where  cod_azienda = :s_cs_xx.cod_azienda and    
       	 cod_piano_campionamento = :fs_cod_piano and    
			 data_validita <= :fdt_data_validita;

open cu_prodotto;

if sqlca.sqlcode < 0 then
	
	g_mb.messagebox("OMNIA", "Errore durante l'apertura di cu_prodotto: " + sqlca.sqlerrtext)
	
	close cu_prodotto;
	
	return ""

end if

setnull(ls_cod_prodotto)

do while 1 = 1
	
	fetch cu_prodotto into :ls_cod_prodotto;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		
		g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_prodotto: " + sqlca.sqlerrtext)
		
		close cu_prodotto;
		
		return ""
		
	end if
	
	exit
	
loop	

close cu_prodotto;

if sqlca.sqlcode < 0 then
	
	g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_prodotto: " + sqlca.sqlerrtext)
	
	close cu_prodotto;
	
	return ""
	
end if

return ls_cod_prodotto
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], l_Objects1[]

l_objects[1] = dw_tes_campionamenti_det_3

//l_objects[2] = cb_laboratorio

dw_folder.fu_AssignTab(3, "&Dati Analisi", l_Objects[])

l_objects[1] = dw_tes_campionamenti_det_2

l_objects[2] = cb_stock

l_objects[3] = cb_ricerca_campione

dw_folder.fu_AssignTab(2, "&Lotto / Note", l_Objects[])

l_objects[1] = dw_tes_campionamenti_det_1

l_objects[2] = cb_nc

l_objects[3] = cb_1

dw_folder.fu_AssignTab(1, "&Campionamento", l_Objects[])

dw_folder.fu_FolderCreate(3,3)

dw_tes_campionamenti_lista.set_dw_key("cod_azienda")

dw_tes_campionamenti_lista.set_dw_key("anno_reg_campionamenti")

dw_tes_campionamenti_lista.set_dw_key("num_reg_campionamenti")

dw_tes_campionamenti_lista.set_dw_options( sqlca, &
                                           pcca.null_object, &
														 c_noretrieveonopen, &
														 c_default)
														 
dw_tes_campionamenti_det_1.set_dw_options( sqlca, &
                                           dw_tes_campionamenti_lista, &
														 c_sharedata + c_scrollparent, &
														 c_default)
														 
dw_tes_campionamenti_det_2.set_dw_options( sqlca, &
                                           dw_tes_campionamenti_lista, &
														 c_sharedata + c_scrollparent, &
														 c_default)
														 
dw_tes_campionamenti_det_3.set_dw_options( sqlca, &
                                           dw_tes_campionamenti_lista, &
														 c_sharedata + c_scrollparent, &
														 c_default)														 
														 
dw_folder.fu_SelectTab(1)

iuo_dw_main = dw_tes_campionamenti_lista

// ----------------------  folder ricerca -----------------------

dw_folder_search.fu_folderoptions( dw_folder_search.c_defaultheight, &
                                   dw_folder_search.c_foldertableft)

l_Objects1[1] = dw_tes_campionamenti_lista

dw_folder_search.fu_assigntab(1, "L.", l_Objects1[])

l_Objects1[1] = dw_ricerca

l_Objects1[2] = cb_ricerca

l_Objects1[3] = cb_reset


//Donato 28-10-2008: aggiunto per ricerca per cod_campione --------
l_Objects1[4] = cb_ricerca_campione_filtro
//----------------------------------------------------------------------

dw_folder_search.fu_assigntab(2, "R.", l_Objects1[])

dw_folder_search.fu_foldercreate(2,2)

dw_folder_search.fu_selectTab(2)

dw_tes_campionamenti_det_3.modify("flag_fornitura.visible=~"0~tif(flag_commercializzazione='S',1,0)~"")
dw_tes_campionamenti_det_3.modify("flag_reso.visible=~"0~tif(flag_commercializzazione='S',1,0)~"")

dw_tes_campionamenti_det_3.modify("des_fornitura.visible=~"0~tif(flag_fornitura='S' and flag_commercializzazione = 'S',1,0)~"")
dw_tes_campionamenti_det_3.modify("des_reso.visible=~"0~tif(flag_reso='S' and flag_commercializzazione = 'S',1,0)~"")

dw_tes_campionamenti_det_3.modify("flag_archiviazione.visible=~"0~tif(flag_commercializzazione='N',1,0)~"")
dw_tes_campionamenti_det_3.modify("flag_confronto.visible=~"0~tif(flag_commercializzazione='N',1,0)~"")

dw_tes_campionamenti_det_3.modify("des_archiviazione.visible=~"0~tif(flag_archiviazione='S',1,0)~"")
dw_tes_campionamenti_det_3.modify("des_confronto.visible=~"0~tif(flag_confronto='S',1,0)~"")


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_1,"cod_piano_campionamento",sqlca,&
                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento in (select cod_piano_campionamento from tes_piani_campionamento where cod_azienda = '" + s_cs_xx.cod_azienda + "' and data_validita <= '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "')")
										                     
f_PO_LoadDDDW_DW(dw_ricerca,"cod_piano_campionamento",sqlca,&
                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

end event

on w_test_campionamenti.create
int iCurrent
call super::create
this.cb_ricerca_campione=create cb_ricerca_campione
this.cb_1=create cb_1
this.cb_doc_anomalia=create cb_doc_anomalia
this.cb_report=create cb_report
this.cb_dettaglio=create cb_dettaglio
this.cb_nc=create cb_nc
this.cb_stock=create cb_stock
this.cb_ricerca_campione_filtro=create cb_ricerca_campione_filtro
this.cb_ricerca=create cb_ricerca
this.cb_reset=create cb_reset
this.dw_tes_campionamenti_det_1=create dw_tes_campionamenti_det_1
this.dw_folder=create dw_folder
this.dw_tes_campionamenti_det_3=create dw_tes_campionamenti_det_3
this.dw_tes_campionamenti_det_2=create dw_tes_campionamenti_det_2
this.dw_tes_campionamenti_lista=create dw_tes_campionamenti_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ricerca_campione
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_doc_anomalia
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.cb_dettaglio
this.Control[iCurrent+6]=this.cb_nc
this.Control[iCurrent+7]=this.cb_stock
this.Control[iCurrent+8]=this.cb_ricerca_campione_filtro
this.Control[iCurrent+9]=this.cb_ricerca
this.Control[iCurrent+10]=this.cb_reset
this.Control[iCurrent+11]=this.dw_tes_campionamenti_det_1
this.Control[iCurrent+12]=this.dw_folder
this.Control[iCurrent+13]=this.dw_tes_campionamenti_det_3
this.Control[iCurrent+14]=this.dw_tes_campionamenti_det_2
this.Control[iCurrent+15]=this.dw_tes_campionamenti_lista
this.Control[iCurrent+16]=this.dw_folder_search
this.Control[iCurrent+17]=this.dw_ricerca
end on

on w_test_campionamenti.destroy
call super::destroy
destroy(this.cb_ricerca_campione)
destroy(this.cb_1)
destroy(this.cb_doc_anomalia)
destroy(this.cb_report)
destroy(this.cb_dettaglio)
destroy(this.cb_nc)
destroy(this.cb_stock)
destroy(this.cb_ricerca_campione_filtro)
destroy(this.cb_ricerca)
destroy(this.cb_reset)
destroy(this.dw_tes_campionamenti_det_1)
destroy(this.dw_folder)
destroy(this.dw_tes_campionamenti_det_3)
destroy(this.dw_tes_campionamenti_det_2)
destroy(this.dw_tes_campionamenti_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
end on

event pc_new;call super::pc_new;integer  li_row_orig, li_row_dest

string   ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto

datetime ldt_data_stock, ldt_data_consegna
date		ld_data

long     ll_prog_stock

li_row_dest = dw_tes_campionamenti_lista.getrow()

dw_tes_campionamenti_lista.setitem(li_row_dest,"data_registrazione",datetime(today()))

ld_data = relativedate(today(), 7)
ldt_data_consegna = datetime(ld_data,00:00:00)
dw_tes_campionamenti_lista.setitem(li_row_dest, "data_consegna",ldt_data_consegna )

end event

event close;call super::close;//if isvalid(w_det_campionamenti.dw_tes_campionamenti_lista) then 
//	w_det_campionamenti.dw_tes_campionamenti_lista.change_dw_current()
//	w_det_campionamenti.triggerevent("pc_retrieve")
//end if
end event

type cb_ricerca_campione from cb_prod_ricerca within w_test_campionamenti
integer x = 2354
integer y = 1060
integer width = 73
integer height = 72
integer taborder = 80
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_campionamenti_det_2
s_cs_xx.parametri.parametro_s_1 = "cod_campione"
end event

type cb_1 from cb_piani_campionamento_ricerca within w_test_campionamenti
integer x = 2171
integer y = 1072
integer height = 72
integer taborder = 70
fontcharset fontcharset = ansi!
end type

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_campionamenti_det_1
s_cs_xx.parametri.parametro_uo_dw_2 = dw_tes_campionamenti_det_2
end event

type cb_doc_anomalia from commandbutton within w_test_campionamenti
integer x = 23
integer y = 2020
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Do&cumenti"
end type

event clicked;long ll_anno, ll_numero

if dw_tes_campionamenti_lista.rowcount() > 0 then

//	ll_anno = dw_tes_campionamenti_lista.getitemnumber( dw_tes_campionamenti_lista.getrow(), "anno_reg_campionamenti")
//	ll_numero = dw_tes_campionamenti_lista.getitemnumber( dw_tes_campionamenti_lista.getrow(), "num_reg_campionamenti")
//	if not isnull(ll_anno) and not isnull(ll_numero) and ll_anno > 0 and ll_numero > 0 then
//		window_open_parm( w_tes_campionamenti_blob, -1, dw_tes_campionamenti_lista)
//	end if

	s_cs_xx.parametri.parametro_ul_1 = dw_tes_campionamenti_lista.getitemnumber( dw_tes_campionamenti_lista.getrow(), "anno_reg_campionamenti")
	s_cs_xx.parametri.parametro_ul_2 = dw_tes_campionamenti_lista.getitemnumber( dw_tes_campionamenti_lista.getrow(), "num_reg_campionamenti")
	
	if s_cs_xx.parametri.parametro_ul_1 = 1 or s_cs_xx.parametri.parametro_ul_2 < 1 then return
	open( w_tes_campionamenti_ole)
else
	
	g_mb.messagebox( "OMNIA", "Attenzione: selezionare una riga per vedere i documenti!")
	
end if
end event

type cb_report from commandbutton within w_test_campionamenti
integer x = 1975
integer y = 2020
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;string ls_tipo_analisi

long ll_riga

ll_riga = dw_tes_campionamenti_lista.getrow()

if ll_riga < 1 then 
	
	g_mb.messagebox( "OMNIA", "Attenzione: selezionare un campionamento!")
	
	return -1
	
end if

ls_tipo_analisi = dw_tes_campionamenti_lista.getitemstring( ll_riga, "flag_tipo_analisi")

choose case ls_tipo_analisi
		
	case "O"
		
		if not isvalid(w_report_campionamenti_org) then
			
			setnull(s_cs_xx.parametri.parametro_d_1)			
			setnull(s_cs_xx.parametri.parametro_d_2)			
				
			s_cs_xx.parametri.parametro_d_1 = dw_tes_campionamenti_lista.getitemnumber( ll_riga, "anno_reg_campionamenti")			
			s_cs_xx.parametri.parametro_d_2 = dw_tes_campionamenti_lista.getitemnumber( ll_riga, "num_reg_campionamenti")			

			window_open(w_report_campionamenti_org,-1)
		
		end if			
		
	case "M"
		
		if not isvalid(w_report_campionamenti) then
			
			s_cs_xx.parametri.parametro_d_1 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"anno_reg_campionamenti")
			
			s_cs_xx.parametri.parametro_d_2 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"num_reg_campionamenti")
			
			window_open(w_report_campionamenti,-1)
		
		end if			
		
	case else

		if not isvalid(w_report_campionamenti) then
			
			s_cs_xx.parametri.parametro_d_1 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"anno_reg_campionamenti")
			
			s_cs_xx.parametri.parametro_d_2 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"num_reg_campionamenti")
			
			window_open(w_report_campionamenti,-1)
		
		end if	
		
end choose


return 0
end event

type cb_dettaglio from commandbutton within w_test_campionamenti
integer x = 2363
integer y = 2020
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettaglio"
end type

event clicked;window_open_parm( w_dett_campionamenti, -1, dw_tes_campionamenti_lista)


end event

type cb_nc from commandbutton within w_test_campionamenti
integer x = 2583
integer y = 1812
integer width = 73
integer height = 72
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;long   ll_num_righe, ll_t, ll_anno, ll_num

double ldd_reset[]

s_cs_xx.parametri.parametro_d_1_a = ldd_reset

s_cs_xx.parametri.parametro_d_2_a = ldd_reset

ll_anno = dw_tes_campionamenti_lista.getitemnumber( dw_tes_campionamenti_lista.getrow(), "anno_non_conf")

ll_num = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "num_non_conf")

if ( ll_anno = 1 ) and ( ll_num = 1 ) then 
	
	setnull(ll_anno)
	
	setnull(ll_num)
	
end if

s_cs_xx.parametri.parametro_d_1_a[1] =	ll_anno
		
s_cs_xx.parametri.parametro_d_2_a[1] = ll_num

window_open(w_seleziona_non_conformita,0)

ll_num_righe = upperbound(s_cs_xx.parametri.parametro_d_1_a[])

if ll_num_righe < 1 then
	
	setnull(ll_anno)
	
	setnull(ll_num)
	
	dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "anno_non_conf", ll_anno)
		
	dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "num_non_conf", ll_num)	
		
else	

	for ll_t = 1 to ll_num_righe
		
		if ( ll_num_righe > 1 ) then		
	
			g_mb.messagebox("OMNIA", "Attenzione: verrà considerata solo la Prima Non Conformità selezionata!")
	
		end if
		
		ll_anno = s_cs_xx.parametri.parametro_d_1_a[1]
		
		ll_num = s_cs_xx.parametri.parametro_d_2_a[1]
		
		dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "anno_non_conf", ll_anno)
		
		dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "num_non_conf", ll_num)	
		
		exit
		
	next

end if

return 0	
end event

event getfocus;call super::getfocus;dw_tes_campionamenti_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"

end event

type cb_stock from cb_stock_ricerca within w_test_campionamenti
integer x = 2359
integer y = 896
integer width = 73
integer height = 72
integer taborder = 30
boolean bringtotop = true
boolean enabled = false
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_10 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_tes_campionamenti_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)

string   ls_cod_prodotto, ls_cod_ubicazione, ls_cod_deposito, ls_cod_lotto

long     ll_prog_stock, currentrow
	
datetime ldt_data_scadenza_stock

currentrow = dw_tes_campionamenti_det_1.getrow()

ls_cod_prodotto = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_prodotto")
	
ls_cod_ubicazione = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_ubicazione")

ls_cod_deposito = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_deposito")

ls_cod_lotto = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_lotto")

ll_prog_stock = dw_tes_campionamenti_det_1.getitemnumber( currentrow, "prog_stock")


select data_scadenza
into   :ldt_data_scadenza_stock
from   stock
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto AND 
		 cod_ubicazione = :ls_cod_ubicazione AND 
		 cod_deposito = :ls_cod_deposito AND 
		 cod_lotto = :ls_cod_lotto AND 
  	 	 prog_stock = :ll_prog_stock;
			 
dw_tes_campionamenti_det_1.setitem( currentrow, "data_scadenza_stock", ldt_data_scadenza_stock)			 
end event

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_campionamenti_det_3
s_cs_xx.parametri.parametro_s_1 = "cod_laboratorio"

end event

type cb_ricerca_campione_filtro from cb_prod_ricerca within w_test_campionamenti
integer x = 2363
integer y = 540
integer width = 73
integer height = 72
integer taborder = 80
boolean bringtotop = true
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
s_cs_xx.parametri.parametro_s_1 = "cod_campione"
end event

type cb_ricerca from commandbutton within w_test_campionamenti
integer x = 1929
integer y = 640
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_folder_search.fu_SelectTab(1)
dw_tes_campionamenti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cb_reset from commandbutton within w_test_campionamenti
integer x = 2341
integer y = 640
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string   ls_null

long     ll_null

datetime ldt_null

setnull(ls_null)

setnull(ll_null)

setnull(ldt_null)

dw_ricerca.setItem(1, "anno_registrazione", ll_null)

dw_ricerca.setItem(1, "data_registrazione_da", ldt_null)

dw_ricerca.setItem(1, "data_registrazione_a", ldt_null)

dw_ricerca.setItem(1, "data_prelievo_da", ldt_null)

dw_ricerca.setItem(1, "data_prelievo_a", ldt_null)

dw_ricerca.setItem(1, "cod_prodotto_da", ls_null)

dw_ricerca.setItem(1, "cod_fornitore", ls_null)

dw_ricerca.setItem(1, "cod_cliente", ls_null)

dw_ricerca.setItem(1, "cod_piano_campionamento", ls_null)

//Donato 28-10-2008 annulla ricerca per cod_campione
dw_ricerca.setItem(1, "cod_campione", ls_null)
//-----------------------
end event

type dw_tes_campionamenti_det_1 from uo_cs_xx_dw within w_test_campionamenti
integer x = 27
integer y = 868
integer width = 2674
integer height = 1128
integer taborder = 60
string dataobject = "d_test_campionamenti_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	datetime ld_data_validita, ld_td, ldt_data_registrazione, ldt_data_consegna
	date		ld_data
	
	string   ls_cod_prodotto, ls_ch, ls_des_cat_mer, ls_modify

	choose case i_colname
			
		case  "data_registrazione" 
			ld_data = date(left(data,10))
			if not isnull(ld_data) and ld_data > date("01/01/1900") then
				ld_data = relativedate(ld_data, 7)
				ldt_data_consegna = datetime(ld_data,00:00:00)
				setitem(row, "data_consegna",ldt_data_consegna )
			else
				setnull(ldt_data_consegna)
				setitem(row, "data_consegna",ldt_data_consegna )
			end if
			
		case  "cod_piano_campionamento" 
			
			setnull(ls_cod_prodotto)
			
			ld_td = getitemdatetime( getrow(), "data_registrazione")

			ls_cod_prodotto = wf_trova_prodotto( i_coltext, ld_td)
			
			if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then

				setitem( getrow(), "cod_prodotto", ls_cod_prodotto)
				
				SELECT max(data_validita)
				INTO   :ld_data_validita  
				FROM   tes_piani_campionamento  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND 	 
						 tes_piani_campionamento.cod_piano_campionamento = :i_coltext and    
						 data_validita <= :ld_td ;
				
				setitem( getrow(), "data_validita", ld_data_validita)		
				
				select des_cat_mer
				into   :ls_des_cat_mer
				from   tab_cat_mer
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cat_mer in (select cod_cat_mer 
											  from   anag_prodotti
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
														
				ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
				modify(ls_modify)				
					
			else
				
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
					
				pcca.error = c_valfailed
					
				return
				
			end if
         		
			
		case "anno_non_conf"
			
			long ll_null
			
			setnull(ll_null)
			
			if len(i_coltext) = 0  or i_coltext = "0" then 
				
				setitem( getrow(), "anno_non_conf", ll_null)
				
				setitem( getrow(), "num_non_conf", ll_null)
				
			end if
			
		case "cod_prodotto"
			
			select des_cat_mer
			into   :ls_des_cat_mer
			from   tab_cat_mer
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_mer in (select cod_cat_mer 
					                 from   anag_prodotti
										  where  cod_azienda = :s_cs_xx.cod_azienda and
										         cod_prodotto = :i_coltext);
													
			ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
			modify(ls_modify)
				
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tes_campionamenti_det_1,"cod_prodotto_da")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_campionamenti_det_1,"cod_fornitore")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_campionamenti_det_1,"cod_cliente")
end choose
end event

type dw_folder from u_folder within w_test_campionamenti
integer x = 23
integer y = 756
integer width = 2702
integer height = 1248
integer taborder = 80
end type

type dw_tes_campionamenti_det_3 from uo_cs_xx_dw within w_test_campionamenti
integer x = 183
integer y = 860
integer width = 2446
integer height = 1120
integer taborder = 80
string dataobject = "d_test_campionamenti_det_3"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	datetime ld_data_validita, ld_td
	
	string   ls_cod_prodotto, ls_ch, ls_des_cat_mer, ls_modify

	choose case i_colname
			
		case  "cod_piano_campionamento" 
			
			setnull(ls_cod_prodotto)
			
			ld_td = getitemdatetime( getrow(), "data_registrazione")

			ls_cod_prodotto = wf_trova_prodotto( i_coltext, ld_td)
			
			if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then

				setitem( getrow(), "cod_prodotto", ls_cod_prodotto)
				
				SELECT max(data_validita)
				INTO   :ld_data_validita  
				FROM   tes_piani_campionamento  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND 	 
						 tes_piani_campionamento.cod_piano_campionamento = :i_coltext and    
						 data_validita <= :ld_td ;
				
				setitem( getrow(), "data_validita", ld_data_validita)		
				
				select des_cat_mer
				into   :ls_des_cat_mer
				from   tab_cat_mer
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cat_mer in (select cod_cat_mer 
											  from   anag_prodotti
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
														
				ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
				modify(ls_modify)				
					
			else
				
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
					
				pcca.error = c_valfailed
					
				return
				
			end if
         		
			
		case "anno_non_conf"
			
			long ll_null
			
			setnull(ll_null)
			
			if len(i_coltext) = 0  or i_coltext = "0" then 
				
				setitem( getrow(), "anno_non_conf", ll_null)
				
				setitem( getrow(), "num_non_conf", ll_null)
				
			end if
			
		case "cod_prodotto"
			
			select des_cat_mer
			into   :ls_des_cat_mer
			from   tab_cat_mer
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_mer in (select cod_cat_mer 
					                 from   anag_prodotti
										  where  cod_azienda = :s_cs_xx.cod_azienda and
										         cod_prodotto = :i_coltext);
													
			ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
			modify(ls_modify)
				
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_laboratorio"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_campionamenti_det_3, "cod_laboratorio")
		
end choose
end event

type dw_tes_campionamenti_det_2 from uo_cs_xx_dw within w_test_campionamenti
integer x = 96
integer y = 856
integer width = 2583
integer height = 1128
integer taborder = 70
string dataobject = "d_test_campionamenti_det_2"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	datetime ld_data_validita, ld_td
	
	string   ls_cod_prodotto, ls_ch, ls_des_cat_mer, ls_modify

	choose case i_colname
			
		case  "cod_piano_campionamento" 
			
			setnull(ls_cod_prodotto)
			
			ld_td = getitemdatetime( getrow(), "data_registrazione")

			ls_cod_prodotto = wf_trova_prodotto( i_coltext, ld_td)
			
			if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then

				setitem( getrow(), "cod_prodotto", ls_cod_prodotto)
				
				SELECT max(data_validita)
				INTO   :ld_data_validita  
				FROM   tes_piani_campionamento  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND 	 
						 tes_piani_campionamento.cod_piano_campionamento = :i_coltext and    
						 data_validita <= :ld_td ;
				
				setitem( getrow(), "data_validita", ld_data_validita)		
				
				select des_cat_mer
				into   :ls_des_cat_mer
				from   tab_cat_mer
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cat_mer in (select cod_cat_mer 
											  from   anag_prodotti
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
														
				ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
				modify(ls_modify)				
					
			else
				
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
					
				pcca.error = c_valfailed
					
				return
				
			end if
         		
			
		case "anno_non_conf"
			
			long ll_null
			
			setnull(ll_null)
			
			if len(i_coltext) = 0  or i_coltext = "0" then 
				
				setitem( getrow(), "anno_non_conf", ll_null)
				
				setitem( getrow(), "num_non_conf", ll_null)
				
			end if
			
		case "cod_prodotto"
			
			select des_cat_mer
			into   :ls_des_cat_mer
			from   tab_cat_mer
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_mer in (select cod_cat_mer 
					                 from   anag_prodotti
										  where  cod_azienda = :s_cs_xx.cod_azienda and
										         cod_prodotto = :i_coltext);
													
			ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
			modify(ls_modify)
				
	end choose
end if
end event

type dw_tes_campionamenti_lista from uo_cs_xx_dw within w_test_campionamenti
integer x = 137
integer y = 40
integer width = 2542
integer height = 672
integer taborder = 50
string dataobject = "d_test_campionamenti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long     ll_anno, ll_index, ll_ret, ll_errore

datetime ldt_registrazione_da, ldt_registrazione_a, ldt_prelievo_da, ldt_prelievo_a

string   ls_cod_prodotto, ls_cod_fornitore, where_clause, ls_prova, new_select, ls_cod_cliente, &
         ls_cod_piano_campionamento
			
//Donato 28-10-2008: filtro di ricerca per cod_campione
string ls_cod_campione

dw_ricerca.accepttext()

ll_anno = dw_ricerca.GetItemNumber(1, "anno_registrazione")

ldt_registrazione_da = dw_ricerca.GetItemDatetime(1, "data_registrazione_da")

ldt_registrazione_a = dw_ricerca.GetItemDatetime(1, "data_registrazione_a")

ldt_prelievo_da = dw_ricerca.GetItemDatetime(1, "data_prelievo_da")

ldt_prelievo_a = dw_ricerca.GetItemDatetime(1, "data_prelievo_a")

ls_cod_prodotto = dw_ricerca.GetItemString(1, "cod_prodotto_da")

ls_cod_fornitore = dw_ricerca.getitemstring(1, "cod_fornitore")

ls_cod_cliente = dw_ricerca.getitemstring(1, "cod_cliente")

ls_cod_piano_campionamento = dw_ricerca.getitemstring(1, "cod_piano_campionamento")

where_clause = " where (cod_azienda = '" + s_cs_xx.cod_azienda + "') "

if ll_anno > 0 and not isnull(ll_anno) then where_clause = where_clause + " AND anno_reg_campionamenti = " + string(ll_anno)

if not isnull(ldt_registrazione_da) then where_clause = where_clause + " AND data_registrazione >= " + string(ldt_registrazione_da, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_registrazione_a) then where_clause = where_clause + " AND data_registrazione <= " + string(ldt_registrazione_a, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_prelievo_da) then where_clause = where_clause + " AND data_prelievo >= " + string(ldt_prelievo_da, s_cs_xx.db_funzioni.formato_data)

if not isnull(ldt_prelievo_a) then where_clause = where_clause + " AND data_prelievo <= " + string(ldt_prelievo_a, s_cs_xx.db_funzioni.formato_data)

if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then where_clause = where_clause + " AND cod_prodotto = '" + ls_cod_prodotto + "' "

if not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then where_clause = where_clause + " AND cod_fornitore = '" + ls_cod_fornitore + "' "

if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then where_clause = where_clause + " AND cod_cliente = '" + ls_cod_cliente + "' "

if not isnull(ls_cod_piano_campionamento) and ls_cod_piano_campionamento <> "" then where_clause = where_clause + " AND cod_piano_campionamento = '" + ls_cod_piano_campionamento + "' "

//Donato 28-10-2008: filtro ricerca per cod_campione ------------------------------
ls_cod_campione = dw_ricerca.getitemstring(1, "cod_campione")
if not isnull(ls_cod_campione) and ls_cod_campione <> "" then where_clause = where_clause + " AND cod_campione = '" + ls_cod_campione + "' "
//fine modifica -----------------------------------------------------------------------

where_clause = where_clause + " order by anno_reg_campionamenti ASC, num_reg_campionamenti ASC "

ls_prova = upper(dw_tes_campionamenti_lista.GETSQLSELECT())

ll_index = pos(ls_prova, "WHERE")
if ll_index = 0 then
	new_select = dw_tes_campionamenti_lista.GETSQLSELECT() + where_clause
else	
	new_select = left(dw_tes_campionamenti_lista.GETSQLSELECT(), ll_index - 1) + where_clause
end if

ll_ret = dw_tes_campionamenti_lista.SetSQLSelect(new_select)

if ll_ret < 0 then 
	g_mb.messagebox("OMNIA","Errore nel filtro di selezione")
	pcca.error = c_fatal
else
	dw_tes_campionamenti_lista.resetupdate()
	ll_errore = retrieve()

	if ll_errore < 0 then
		pcca.error = c_fatal
	end if
end if
	
end event

event pcd_view;call super::pcd_view;if i_extendmode then

	cb_stock.enabled = false
	
	cb_nc.enabled = false
	
	cb_report.enabled = true

	cb_dettaglio.enabled = true
	
	dw_tes_campionamenti_det_1.object.b_ricerca_fornitore.enabled = false
	dw_tes_campionamenti_det_1.object.b_ricerca_cliente.enabled = false
	dw_tes_campionamenti_det_3.object.b_ricerca_laboratorio.enabled = false
	
	cb_1.enabled = false
	
	cb_ricerca_campione.enabled = false
	
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then

	cb_stock.enabled = false

	cb_nc.enabled = false
	
	cb_report.enabled = true

	cb_dettaglio.enabled = true
	
	cb_1.enabled = false
	
	cb_ricerca_campione.enabled = false
	
	dw_tes_campionamenti_det_1.object.b_ricerca_fornitore.enabled = false
	dw_tes_campionamenti_det_1.object.b_ricerca_cliente.enabled = false
	dw_tes_campionamenti_det_3.object.b_ricerca_laboratorio.enabled = false
	
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_num_reg_campionamenti,ll_anno

ll_anno = year(today())

select max(num_reg_campionamenti) 
into   :ll_num_reg_campionamenti 
from   tes_campionamenti 
where  cod_azienda = :s_cs_xx.cod_azienda 
and    anno_reg_campionamenti = :ll_anno; 

if isnull(ll_num_reg_campionamenti) then
	
	ll_num_reg_campionamenti = 1
	
else
	
	ll_num_reg_campionamenti++
	
end if

FOR l_Idx = 1 TO RowCount()
	
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
		
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		
		SetItem(l_Idx, "anno_reg_campionamenti", year(today()))
		
		SetItem(l_Idx, "num_reg_campionamenti", ll_num_reg_campionamenti)
		
   END IF
	
NEXT

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then

	cb_stock.enabled = true

	cb_nc.enabled = true

	cb_report.enabled = false

	cb_dettaglio.enabled = false
		
	cb_1.enabled = true
	
	cb_ricerca_campione.enabled = true
	
	dw_tes_campionamenti_det_1.object.b_ricerca_fornitore.enabled = true
	dw_tes_campionamenti_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_campionamenti_det_3.object.b_ricerca_laboratorio.enabled = true
	
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then

	cb_stock.enabled = true

	cb_nc.enabled = true
	
	cb_report.enabled = false

	cb_dettaglio.enabled = false
		
	cb_1.enabled = true
	
	cb_ricerca_campione.enabled = true
	
	dw_tes_campionamenti_det_1.object.b_ricerca_fornitore.enabled = true
	dw_tes_campionamenti_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_campionamenti_det_3.object.b_ricerca_laboratorio.enabled = true
	
end if
end event

event updatestart;call super::updatestart;integer li_i, li_anno_reg_campionamenti
	
long    ll_num_reg_campionamenti, ll_anno, ll_num

if i_extendmode then
	
	ll_anno = getitemnumber( getrow(), "anno_non_conf")
				
	ll_num = getitemnumber( getrow(), "num_non_conf")
	
	if ll_anno < 1 or ll_num < 1 or ll_anno = 1 then
		
		setnull(ll_anno)
		
		setnull(ll_num)
		
		setitem( getrow(), "anno_non_conf", ll_anno)
		
		setitem( getrow(), "num_non_conf", ll_num)		

	end if
	
	for li_i = 1 to this.deletedcount()
		
	   li_anno_reg_campionamenti = this.getitemnumber(li_i, "anno_reg_campionamenti", delete!, true)
		
   	ll_num_reg_campionamenti = this.getitemnumber(li_i, "num_reg_campionamenti", delete!, true)

	   delete from det_campionamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
		       anno_reg_campionamenti = :li_anno_reg_campionamenti and 	
				 ll_num_reg_campionamenti = :ll_num_reg_campionamenti;

		update collaudi  
	   set    data_carta = null,   
      	    cod_carta = ''  
   	where  collaudi.anno_reg_campionamenti = :li_anno_reg_campionamenti and    
		       collaudi.num_reg_campionamenti = :ll_num_reg_campionamenti;

	next

end if

end event

type dw_folder_search from u_folder within w_test_campionamenti
integer x = 23
integer y = 20
integer width = 2706
integer height = 724
integer taborder = 70
end type

type dw_ricerca from uo_dw_search within w_test_campionamenti
integer x = 183
integer y = 40
integer width = 2286
integer height = 660
integer taborder = 80
string dataobject = "d_ricerca_campionamenti"
boolean border = false
end type

event itemchanged;call super::itemchanged;//choose case getcolumnname()
//		
//	case "attrezzatura_da"
//		postevent("ue_cod_attr")
//		postevent("ue_carica_tipologie")
//		
//	case "attrezzatura_a","cod_reparto","cod_categoria","cod_area"
//		postevent("ue_carica_tipologie")
//
//end choose
//
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_da")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"cod_fornitore")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")
end choose
end event

