﻿$PBExportHeader$w_test_campionamenti_aut.srw
$PBExportComments$testata dei campionamenti (creati in base ai piani)
forward
global type w_test_campionamenti_aut from w_cs_xx_principale
end type
type cb_report from commandbutton within w_test_campionamenti_aut
end type
type cb_dettaglio from commandbutton within w_test_campionamenti_aut
end type
type dw_folder from u_folder within w_test_campionamenti_aut
end type
type dw_tes_campionamenti_det_3 from uo_cs_xx_dw within w_test_campionamenti_aut
end type
type dw_tes_campionamenti_det_2 from uo_cs_xx_dw within w_test_campionamenti_aut
end type
type cb_ricerca_campione from cb_prod_ricerca within w_test_campionamenti_aut
end type
type cb_stock from cb_stock_ricerca within w_test_campionamenti_aut
end type
type dw_tes_campionamenti_det_1 from uo_cs_xx_dw within w_test_campionamenti_aut
end type
type cb_1 from cb_piani_campionamento_ricerca within w_test_campionamenti_aut
end type
type cb_nc from commandbutton within w_test_campionamenti_aut
end type
end forward

global type w_test_campionamenti_aut from w_cs_xx_principale
integer width = 2779
integer height = 1632
string title = "Campionamenti"
cb_report cb_report
cb_dettaglio cb_dettaglio
dw_folder dw_folder
dw_tes_campionamenti_det_3 dw_tes_campionamenti_det_3
dw_tes_campionamenti_det_2 dw_tes_campionamenti_det_2
cb_ricerca_campione cb_ricerca_campione
cb_stock cb_stock
dw_tes_campionamenti_det_1 dw_tes_campionamenti_det_1
cb_1 cb_1
cb_nc cb_nc
end type
global w_test_campionamenti_aut w_test_campionamenti_aut

type variables
s_campionamento_aut is_campionamento_aut
end variables

forward prototypes
public function string wf_trova_prodotto (string fs_cod_piano, datetime fdt_data_validita)
public subroutine wf_invia_mail (string fs_oggetto, string fs_testo, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist, string fs_domanda)
public function string wf_crea_elenco_test (long fl_anno_reg_camp, long fl_num_reg_camp, string fs_cod_prodotto)
public function long wf_imposta_default_cod_valore (long fl_anno_reg_camp, long fl_num_reg_camp)
end prototypes

public function string wf_trova_prodotto (string fs_cod_piano, datetime fdt_data_validita);string ls_cod_prodotto

declare cu_prodotto cursor for
	select cod_prodotto 	
	from   det_piani_campionamento 
	where  cod_azienda = :s_cs_xx.cod_azienda and    
       	 cod_piano_campionamento = :fs_cod_piano and    
			 data_validita <= :fdt_data_validita;

open cu_prodotto;

if sqlca.sqlcode < 0 then
	
	g_mb.messagebox("OMNIA", "Errore durante l'apertura di cu_prodotto: " + sqlca.sqlerrtext)
	
	close cu_prodotto;
	
	return ""

end if

setnull(ls_cod_prodotto)

do while 1 = 1
	
	fetch cu_prodotto into :ls_cod_prodotto;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		
		g_mb.messagebox("OMNIA", "Errore durante la fetch di cu_prodotto: " + sqlca.sqlerrtext)
		
		close cu_prodotto;
		
		return ""
		
	end if
	
	exit
	
loop	

close cu_prodotto;

if sqlca.sqlcode < 0 then
	
	g_mb.messagebox("OMNIA", "Errore durante la chiusura di cu_prodotto: " + sqlca.sqlerrtext)
	
	close cu_prodotto;
	
	return ""
	
end if

return ls_cod_prodotto
end function

public subroutine wf_invia_mail (string fs_oggetto, string fs_testo, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist, string fs_domanda);string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, &
       ls_cod_cat_mer

long   ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente

//ls_cod_tipo_lista_dist = "CDV"
//ls_cod_lista_dist = "RNCA"
						 		
ls_cod_cat_mer = ""
ls_cod_area_aziendale = ""
ll_num_sequenza_corrente = 1
		
//	ll_anno = 2008
//	ll_num = 12
						
s_cs_xx.parametri.parametro_s_1 = "L"
s_cs_xx.parametri.parametro_s_2 = fs_cod_tipo_lista_dist//ls_cod_tipo_lista_dist
s_cs_xx.parametri.parametro_s_3 = fs_cod_lista_dist//ls_cod_lista_dist
s_cs_xx.parametri.parametro_s_4 = ""
s_cs_xx.parametri.parametro_s_5 = fs_oggetto//"oggetto: E' stata modificata la Non conformità " + string(ll_anno) + "/" + string(ll_num)
s_cs_xx.parametri.parametro_s_6 = fs_testo//"test: S_6 ** E' stata modificata la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
s_cs_xx.parametri.parametro_s_7 = fs_domanda
s_cs_xx.parametri.parametro_s_8 = ""
s_cs_xx.parametri.parametro_s_9 = ""//ls_cod_area_aziendale
s_cs_xx.parametri.parametro_s_10 = ""//ls_cod_cat_mer
s_cs_xx.parametri.parametro_s_11 = ""
s_cs_xx.parametri.parametro_s_12 = ""
s_cs_xx.parametri.parametro_s_13 = ""//string(ll_anno)
s_cs_xx.parametri.parametro_s_14 = ""//string(ll_num)
s_cs_xx.parametri.parametro_s_15 = "N"
s_cs_xx.parametri.parametro_ul_3 = 0//ll_num_sequenza_corrente
s_cs_xx.parametri.parametro_b_1 = true
						
openwithparm( w_notifica_messaggi_lab, 0)	

return

end subroutine

public function string wf_crea_elenco_test (long fl_anno_reg_camp, long fl_num_reg_camp, string fs_cod_prodotto);datastore lds_test
string ls_sql, ls_errore, ls_sintassi, ls_return, ls_cod_test, ls_desc_test
long ll_righe, ll_index

ls_sql = "select " +&
				"det_campionamenti.cod_test" + &
				",tab_test.des_test" + &
				" " +&
        		"from det_campionamenti " + &
        		"join tab_test on tab_test.cod_test=det_campionamenti.cod_test " + &
        	"  where det_campionamenti.cod_azienda = '" + s_cs_xx.cod_azienda + "' "+ &
			  	"and det_campionamenti.anno_reg_campionamenti="+string(fl_anno_reg_camp) + " " + &
				  "and det_campionamenti.num_reg_campionamenti="+string(fl_num_reg_camp) + " " + &
				  "and det_campionamenti.cod_prodotto='" + fs_cod_prodotto+"' "		  
ls_sql += " ORDER BY det_campionamenti.prog_riga_campionamenti "

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore dei test:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
   RETURN "error"
END IF

lds_test = create datastore

lds_test.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore dei test:" + ls_errore)
	destroy lds_test;
   RETURN "error"
END IF

lds_test.settransobject( sqlca)

ll_righe = lds_test.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe dei test:" + sqlca.sqlerrtext)
	destroy lds_test;
	return "error"
end if

ls_return = ""
for ll_index = 1 to ll_righe
	ls_cod_test = lds_test.getitemstring(ll_index, "det_campionamenti_cod_test")
	ls_desc_test = lds_test.getitemstring(ll_index, "tab_test_des_test")
	if isnull(ls_desc_test) then ls_desc_test = ""
	
	ls_return += " - " + ls_cod_test + "~t~t~t" + ls_desc_test + "~r~n"
	
next

return ls_return
end function

public function long wf_imposta_default_cod_valore (long fl_anno_reg_camp, long fl_num_reg_camp);datastore lds_test
string ls_sql, ls_errore, ls_sintassi, ls_val_default, ls_cod_test
long ll_righe, ll_index

ls_sql = "select " +&
				"cod_test" + &				
				" " +&
        		"from det_campionamenti " + &        		
        	"  where cod_azienda = '" + s_cs_xx.cod_azienda + "' "+ &
			  	"and anno_reg_campionamenti="+string(fl_anno_reg_camp) + " " + &
				  "and num_reg_campionamenti="+string(fl_num_reg_camp) + " " + &
				  "and (quantita = 0 or quantita is null) and flag_esito='I' and cod_valore is null and flag_altro_valore ='N' "

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore dei test per i valori di default:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
   RETURN -1
END IF

lds_test = create datastore

lds_test.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore dei test per i valori di default:" + ls_errore)
	destroy lds_test;
   RETURN -1
END IF

lds_test.settransobject(sqlca)

ll_righe = lds_test.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe dei test per i valori di default:" + sqlca.sqlerrtext)
	destroy lds_test;
	return -1
end if

for ll_index = 1 to ll_righe
	ls_cod_test = lds_test.getitemstring(ll_index, "cod_test")
	
	setnull(ls_val_default)
	
	//recupera il valore di default (se c'è) e aggiorna la det_campionamenti
	select cod_valore
	into :ls_val_default
	from tab_test_valori
	where cod_test = :ls_cod_test and flag_default = "S";
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "OMNIA", "Errore durante la retrieve dei valori di default:" + sqlca.sqlerrtext)
		destroy lds_test;
		return -1
	end if
	
	if not isnull(ls_val_default) and ls_val_default <> "" then
		//imposta il valore
		update det_campionamenti
		set cod_valore = :ls_val_default
		where cod_azienda = :s_cs_xx.cod_azienda
				and anno_reg_campionamenti = :fl_anno_reg_camp and num_reg_campionamenti = :fl_num_reg_camp
				and cod_test = :ls_cod_test;
			
		if sqlca.sqlcode = -1 then
			g_mb.messagebox( "OMNIA", "Errore durante l'aggiornamento dei valori di default dei test:" + sqlca.sqlerrtext)
			destroy lds_test;
			return -1
		end if		
	end if	
next

return 1
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], l_Objects1[]
string ls_cod_prodotto, ls_modify, ls_des_cat_mer
datetime ld_td

is_campionamento_aut = message.powerobjectparm

l_objects[1] = dw_tes_campionamenti_det_3
dw_folder.fu_AssignTab(3, "&Dati Analisi", l_Objects[])

l_objects[1] = dw_tes_campionamenti_det_2
l_objects[2] = cb_stock
l_objects[3] = cb_ricerca_campione
dw_folder.fu_AssignTab(2, "&Lotto / Note", l_Objects[])

l_objects[1] = dw_tes_campionamenti_det_1
l_objects[2] = cb_nc
l_objects[3] = cb_1
dw_folder.fu_AssignTab(1, "&Campionamento", l_Objects[])

dw_folder.fu_FolderCreate(3,3)

dw_tes_campionamenti_det_1.set_dw_key("cod_azienda")
dw_tes_campionamenti_det_1.set_dw_key("anno_reg_campionamenti")
dw_tes_campionamenti_det_1.set_dw_key("num_reg_campionamenti")		

dw_tes_campionamenti_det_1.set_dw_options( sqlca, &
                                           c_NullDW, &
														 c_sharedata + c_scrollparent, &
														 c_default)
														 
dw_tes_campionamenti_det_2.set_dw_options( sqlca, &
                                           dw_tes_campionamenti_det_1, &
														 c_sharedata + c_scrollparent, &
														 c_default)
														 
dw_tes_campionamenti_det_3.set_dw_options( sqlca, &
                                           dw_tes_campionamenti_det_1, &
														 c_sharedata + c_scrollparent, &
														 c_default)
														 
dw_folder.fu_SelectTab(1)
iuo_dw_main = dw_tes_campionamenti_det_1

dw_tes_campionamenti_det_3.modify("flag_fornitura.visible=~"0~tif(flag_commercializzazione='S',1,0)~"")
dw_tes_campionamenti_det_3.modify("flag_reso.visible=~"0~tif(flag_commercializzazione='S',1,0)~"")

dw_tes_campionamenti_det_3.modify("des_fornitura.visible=~"0~tif(flag_fornitura='S' and flag_commercializzazione = 'S',1,0)~"")
dw_tes_campionamenti_det_3.modify("des_reso.visible=~"0~tif(flag_reso='S' and flag_commercializzazione = 'S',1,0)~"")

dw_tes_campionamenti_det_3.modify("flag_archiviazione.visible=~"0~tif(flag_commercializzazione='N',1,0)~"")
dw_tes_campionamenti_det_3.modify("flag_confronto.visible=~"0~tif(flag_commercializzazione='N',1,0)~"")

dw_tes_campionamenti_det_3.modify("des_archiviazione.visible=~"0~tif(flag_archiviazione='S',1,0)~"")
dw_tes_campionamenti_det_3.modify("des_confronto.visible=~"0~tif(flag_confronto='S',1,0)~"")

dw_tes_campionamenti_det_1.triggerevent("pcd_new")
dw_tes_campionamenti_det_1.postevent("pcd_modify")

dw_tes_campionamenti_det_1.setitem(dw_tes_campionamenti_det_1.getrow(), &
																	"cod_piano_campionamento", &
																	is_campionamento_aut.s_cod_piano_campionamento)
																	
dw_tes_campionamenti_det_1.setitem(dw_tes_campionamenti_det_1.getrow(), &
																	"data_validita", &
																	is_campionamento_aut.dt_data_validita)
																	
dw_tes_campionamenti_det_1.setitem(dw_tes_campionamenti_det_1.getrow(), &
																	"prog_piano_campionamento", &
																	is_campionamento_aut.l_prog_piano_campionamento)

dw_tes_campionamenti_det_1.setitem(dw_tes_campionamenti_det_1.getrow(),"cod_azienda", s_cs_xx.cod_azienda)
dw_tes_campionamenti_det_1.setitem(dw_tes_campionamenti_det_1.getrow(),"data_registrazione",datetime(today()))

f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2, "cod_prodotto", sqlca,&
                    "anag_prodotti", "cod_prodotto", "des_prodotto",&
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+ &
						  "cod_prodotto in ( select cod_prodotto from det_piani_campionamento "+ &
						  							"where cod_piano_campionamento = '" + is_campionamento_aut.s_cod_piano_campionamento + "' and "+ &
													  "data_validita = '" + string(is_campionamento_aut.dt_data_validita, "yyyymmdd") + "' and "+ &
													  "prog_piano_campionamento = " + string(is_campionamento_aut.l_prog_piano_campionamento) + " )")	


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_1,"cod_piano_campionamento",sqlca,&
                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento in (select cod_piano_campionamento from tes_piani_campionamento where cod_azienda = '" + s_cs_xx.cod_azienda + "' and data_validita <= '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "')")
										                     
//f_PO_LoadDDDW_DW(dw_ricerca,"cod_piano_campionamento",sqlca,&
//                 "tab_tipi_piani_campionamento","cod_piano_campionamento","des_piano_campionamento",&
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
//f_PO_LoadDDDW_DW(dw_tes_campionamenti_det_2,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
end event

on w_test_campionamenti_aut.create
int iCurrent
call super::create
this.cb_report=create cb_report
this.cb_dettaglio=create cb_dettaglio
this.dw_folder=create dw_folder
this.dw_tes_campionamenti_det_3=create dw_tes_campionamenti_det_3
this.dw_tes_campionamenti_det_2=create dw_tes_campionamenti_det_2
this.cb_ricerca_campione=create cb_ricerca_campione
this.cb_stock=create cb_stock
this.dw_tes_campionamenti_det_1=create dw_tes_campionamenti_det_1
this.cb_1=create cb_1
this.cb_nc=create cb_nc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report
this.Control[iCurrent+2]=this.cb_dettaglio
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_tes_campionamenti_det_3
this.Control[iCurrent+5]=this.dw_tes_campionamenti_det_2
this.Control[iCurrent+6]=this.cb_ricerca_campione
this.Control[iCurrent+7]=this.cb_stock
this.Control[iCurrent+8]=this.dw_tes_campionamenti_det_1
this.Control[iCurrent+9]=this.cb_1
this.Control[iCurrent+10]=this.cb_nc
end on

on w_test_campionamenti_aut.destroy
call super::destroy
destroy(this.cb_report)
destroy(this.cb_dettaglio)
destroy(this.dw_folder)
destroy(this.dw_tes_campionamenti_det_3)
destroy(this.dw_tes_campionamenti_det_2)
destroy(this.cb_ricerca_campione)
destroy(this.cb_stock)
destroy(this.dw_tes_campionamenti_det_1)
destroy(this.cb_1)
destroy(this.cb_nc)
end on

event pc_new;call super::pc_new;integer  li_row

li_row = dw_tes_campionamenti_det_1.getrow()
dw_tes_campionamenti_det_1.setitem(li_row,"data_registrazione",datetime(today()))


//if isvalid(s_cs_xx.parametri.parametro_dw_1) and s_cs_xx.parametri.parametro_s_1 = "acc_materiali" then
//	
//	li_row_orig = s_cs_xx.parametri.parametro_dw_1.getrow()
//
//	ls_cod_prodotto = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_prodotto")
//	ls_cod_deposito = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_deposito")
//	ls_cod_ubicazione = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_ubicazione")
//	ls_cod_lotto = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_row_orig, "cod_lotto")
//	ldt_data_stock = s_cs_xx.parametri.parametro_dw_1.getitemdatetime(li_row_orig, "data_stock")
//	ll_prog_stock = s_cs_xx.parametri.parametro_dw_1.getitemnumber(li_row_orig, "prog_stock")
//	
//	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_prodotto", ls_cod_prodotto)	
//	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_deposito", ls_cod_deposito)	
//	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_ubicazione", ls_cod_ubicazione)	
//	dw_tes_campionamenti_lista.setitem(li_row_dest, "cod_lotto", ls_cod_lotto)	
//	dw_tes_campionamenti_lista.setitem(li_row_dest, "data_stock", ldt_data_stock)	
//	dw_tes_campionamenti_lista.setitem(li_row_dest, "prog_stock", ll_prog_stock)	
//end if
end event

event close;call super::close;//if isvalid(w_det_campionamenti.dw_tes_campionamenti_lista) then 
//	w_det_campionamenti.dw_tes_campionamenti_lista.change_dw_current()
//	w_det_campionamenti.triggerevent("pc_retrieve")
//end if
end event

event pc_save;call super::pc_save;IF PCCA.Error = c_Success THEN
	postevent("pc_close")
END IF
end event

type cb_report from commandbutton within w_test_campionamenti_aut
boolean visible = false
integer x = 1975
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;//string ls_tipo_analisi
//
//long ll_riga
//
//ll_riga = dw_tes_campionamenti_lista.getrow()
//
//if ll_riga < 1 then 
//	
//	g_mb.messagebox( "OMNIA", "Attenzione: selezionare un campionamento!")
//	
//	return -1
//	
//end if
//
//ls_tipo_analisi = dw_tes_campionamenti_lista.getitemstring( ll_riga, "flag_tipo_analisi")
//
//choose case ls_tipo_analisi
//		
//	case "O"
//		
//		if not isvalid(w_report_campionamenti_org) then
//			
//			setnull(s_cs_xx.parametri.parametro_d_1)			
//			setnull(s_cs_xx.parametri.parametro_d_2)			
//				
//			s_cs_xx.parametri.parametro_d_1 = dw_tes_campionamenti_lista.getitemnumber( ll_riga, "anno_reg_campionamenti")			
//			s_cs_xx.parametri.parametro_d_2 = dw_tes_campionamenti_lista.getitemnumber( ll_riga, "num_reg_campionamenti")			
//
//			window_open(w_report_campionamenti_org,-1)
//		
//		end if			
//		
//	case "M"
//		
//		if not isvalid(w_report_campionamenti) then
//			
//			s_cs_xx.parametri.parametro_d_1 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"anno_reg_campionamenti")
//			
//			s_cs_xx.parametri.parametro_d_2 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"num_reg_campionamenti")
//			
//			window_open(w_report_campionamenti,-1)
//		
//		end if			
//		
//	case else
//
//		if not isvalid(w_report_campionamenti) then
//			
//			s_cs_xx.parametri.parametro_d_1 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"anno_reg_campionamenti")
//			
//			s_cs_xx.parametri.parametro_d_2 = dw_tes_campionamenti_lista.getitemnumber(ll_riga,"num_reg_campionamenti")
//			
//			window_open(w_report_campionamenti,-1)
//		
//		end if	
//		
//end choose
//
//
//return 0
end event

type cb_dettaglio from commandbutton within w_test_campionamenti_aut
boolean visible = false
integer x = 2363
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettaglio"
end type

event clicked;//window_open_parm( w_dett_campionamenti, -1, dw_tes_campionamenti_lista)


end event

type dw_folder from u_folder within w_test_campionamenti_aut
integer x = 23
integer y = 16
integer width = 2702
integer height = 1248
integer taborder = 80
end type

type dw_tes_campionamenti_det_3 from uo_cs_xx_dw within w_test_campionamenti_aut
integer x = 183
integer y = 120
integer width = 2446
integer height = 1120
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_test_campionamenti_det_3_new"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	datetime ld_data_validita, ld_td
	
	string   ls_cod_prodotto, ls_ch, ls_des_cat_mer, ls_modify

	choose case i_colname
			
		case  "cod_piano_campionamento" 
			
			setnull(ls_cod_prodotto)
			
			ld_td = getitemdatetime( getrow(), "data_registrazione")

			ls_cod_prodotto = wf_trova_prodotto( i_coltext, ld_td)
			
			if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then

				setitem( getrow(), "cod_prodotto", ls_cod_prodotto)
				
				SELECT max(data_validita)
				INTO   :ld_data_validita  
				FROM   tes_piani_campionamento  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND 	 
						 tes_piani_campionamento.cod_piano_campionamento = :i_coltext and    
						 data_validita <= :ld_td ;
				
				setitem( getrow(), "data_validita", ld_data_validita)		
				
				select des_cat_mer
				into   :ls_des_cat_mer
				from   tab_cat_mer
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cat_mer in (select cod_cat_mer 
											  from   anag_prodotti
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
														
				ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
				modify(ls_modify)				
					
			else
				
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
					
				pcca.error = c_valfailed
					
				return
				
			end if
         		
			
		case "anno_non_conf"
			
			long ll_null
			
			setnull(ll_null)
			
			if len(i_coltext) = 0  or i_coltext = "0" then 
				
				setitem( getrow(), "anno_non_conf", ll_null)
				
				setitem( getrow(), "num_non_conf", ll_null)
				
			end if
			
		case "cod_prodotto"
			
			select des_cat_mer
			into   :ls_des_cat_mer
			from   tab_cat_mer
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_mer in (select cod_cat_mer 
					                 from   anag_prodotti
										  where  cod_azienda = :s_cs_xx.cod_azienda and
										         cod_prodotto = :i_coltext);
													
			ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
			modify(ls_modify)
				
	end choose
end if
end event

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_campionamenti_det_3
s_cs_xx.parametri.parametro_s_1 = "cod_laboratorio"

end event

type dw_tes_campionamenti_det_2 from uo_cs_xx_dw within w_test_campionamenti_aut
integer x = 96
integer y = 116
integer width = 2583
integer height = 1128
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_test_campionamenti_det_2_new"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	datetime ld_data_validita, ld_td
	
	string   ls_cod_prodotto, ls_ch, ls_des_cat_mer, ls_modify

	choose case i_colname
			
		case  "cod_piano_campionamento" 
			
			setnull(ls_cod_prodotto)
			
			ld_td = getitemdatetime( getrow(), "data_registrazione")

			ls_cod_prodotto = wf_trova_prodotto( i_coltext, ld_td)
			
			if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then

				setitem( getrow(), "cod_prodotto", ls_cod_prodotto)
				
				SELECT max(data_validita)
				INTO   :ld_data_validita  
				FROM   tes_piani_campionamento  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND 	 
						 tes_piani_campionamento.cod_piano_campionamento = :i_coltext and    
						 data_validita <= :ld_td ;
				
				setitem( getrow(), "data_validita", ld_data_validita)		
				
				select des_cat_mer
				into   :ls_des_cat_mer
				from   tab_cat_mer
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cat_mer in (select cod_cat_mer 
											  from   anag_prodotti
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
														
				ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
				modify(ls_modify)				
					
			else
				
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
					
				pcca.error = c_valfailed
					
				return
				
			end if
         		
			
		case "anno_non_conf"
			
			long ll_null
			
			setnull(ll_null)
			
			if len(i_coltext) = 0  or i_coltext = "0" then 
				
				setitem( getrow(), "anno_non_conf", ll_null)
				
				setitem( getrow(), "num_non_conf", ll_null)
				
			end if
			
		case "cod_prodotto"
			
			select des_cat_mer
			into   :ls_des_cat_mer
			from   tab_cat_mer
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_mer in (select cod_cat_mer 
					                 from   anag_prodotti
										  where  cod_azienda = :s_cs_xx.cod_azienda and
										         cod_prodotto = :i_coltext);
													
			ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
			modify(ls_modify)
				
	end choose
end if
end event

type cb_ricerca_campione from cb_prod_ricerca within w_test_campionamenti_aut
integer x = 2354
integer y = 320
integer width = 73
integer height = 72
integer taborder = 80
boolean bringtotop = true
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_campionamenti_det_2
s_cs_xx.parametri.parametro_s_1 = "cod_campione"
end event

type cb_stock from cb_stock_ricerca within w_test_campionamenti_aut
integer x = 2359
integer y = 156
integer width = 73
integer height = 72
integer taborder = 30
boolean bringtotop = true
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_10 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_tes_campionamenti_det_1.getitemstring(dw_tes_campionamenti_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_tes_campionamenti_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)

string   ls_cod_prodotto, ls_cod_ubicazione, ls_cod_deposito, ls_cod_lotto

long     ll_prog_stock, currentrow
	
datetime ldt_data_scadenza_stock

currentrow = dw_tes_campionamenti_det_1.getrow()

ls_cod_prodotto = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_prodotto")
	
ls_cod_ubicazione = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_ubicazione")

ls_cod_deposito = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_deposito")

ls_cod_lotto = dw_tes_campionamenti_det_1.getitemstring( currentrow, "cod_lotto")

ll_prog_stock = dw_tes_campionamenti_det_1.getitemnumber( currentrow, "prog_stock")


select data_scadenza
into   :ldt_data_scadenza_stock
from   stock
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto AND 
		 cod_ubicazione = :ls_cod_ubicazione AND 
		 cod_deposito = :ls_cod_deposito AND 
		 cod_lotto = :ls_cod_lotto AND 
  	 	 prog_stock = :ll_prog_stock;
			 
dw_tes_campionamenti_det_1.setitem( currentrow, "data_scadenza_stock", ldt_data_scadenza_stock)			 
end event

type dw_tes_campionamenti_det_1 from uo_cs_xx_dw within w_test_campionamenti_aut
integer x = 27
integer y = 128
integer width = 2674
integer height = 1248
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_test_campionamenti_det_1_new"
boolean border = false
end type

event itemchanged;call super::itemchanged;datetime ld_data_validita, ld_td
string   ls_cod_prodotto, ls_ch, ls_des_cat_mer, ls_modify, ls_null

if i_extendMode then
	choose case i_colname
			
		case  "cod_piano_campionamento" 
			
			setnull(ls_cod_prodotto)
			
			ld_td = getitemdatetime( getrow(), "data_registrazione")

			ls_cod_prodotto = wf_trova_prodotto( i_coltext, ld_td)
			
			if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then

				setitem( getrow(), "cod_prodotto", ls_cod_prodotto)
				
				SELECT max(data_validita)
				INTO   :ld_data_validita  
				FROM   tes_piani_campionamento  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND 	 
						 tes_piani_campionamento.cod_piano_campionamento = :i_coltext and    
						 data_validita <= :ld_td ;
				
				setitem( getrow(), "data_validita", ld_data_validita)		
				
				select des_cat_mer
				into   :ls_des_cat_mer
				from   tab_cat_mer
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cat_mer in (select cod_cat_mer 
											  from   anag_prodotti
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
														
				ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
				modify(ls_modify)				
					
			else
				
				g_mb.messagebox("Omnia","Attenzione! Non esiste nessun prodotto associato al piano di campionamento selezionato con data di validità compatibile. Selezionare un altro piano di campionamento")
					
				pcca.error = c_valfailed
					
				return
				
			end if
         		
			
		case "anno_non_conf"
			
			long ll_null
			
			setnull(ll_null)
			
			if len(i_coltext) = 0  or i_coltext = "0" then 
				
				setitem( getrow(), "anno_non_conf", ll_null)
				
				setitem( getrow(), "num_non_conf", ll_null)
				
			end if
			
		case "cod_prodotto"
			
			select des_cat_mer
			into   :ls_des_cat_mer
			from   tab_cat_mer
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_mer in (select cod_cat_mer 
					                 from   anag_prodotti
										  where  cod_azienda = :s_cs_xx.cod_azienda and
										         cod_prodotto = :i_coltext);
													
			ls_modify = "categoria.text = '" + ls_des_cat_mer + "' "
			modify(ls_modify)
			
			case  "flag_letto"			
				if data = "N" then
					setnull(ld_td)
					setnull(ls_null)
					setitem(getrow(),"cod_utente_lettura",ls_null)
					setitem(getrow(),"data_utente_lettura",ld_td)
					
				elseif data = "S" then
					ld_td = datetime(today(),now())
					setitem(getrow(),"cod_utente_lettura",s_cs_xx.cod_utente)
					setitem(getrow(),"data_utente_lettura",ld_td)
				end if
				
	end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_prodotto

ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")

if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
	g_mb.messagebox( "OMNIA", "E' necessario indicare il prodotto!")
	pcca.error = c_fatal
	dw_tes_campionamenti_det_2.change_dw_current()
	dw_folder.fu_SelectTab(2)
	return
end if
end event

event updatestart;call super::updatestart;long  l_Idx,ll_num_reg_campionamenti,ll_anno
string ls_cod_azienda
long ll_anno_reg_campionamenti, ll_num_reg_campionamenti2
dwItemStatus l_status
datetime ldt_datetime
date ld_date

l_status = GetItemStatus(GetRow(), "data_prelievo", Primary!)
if l_status=DataModified! or l_status=NewModified! then
	ldt_datetime = getitemdatetime(getrow(), "data_prelievo")
	ld_date = date(ldt_datetime)
	ldt_datetime = datetime(ld_date, now())
	setitem(getrow(), "data_prelievo", ldt_datetime)
end if
l_status = GetItemStatus(GetRow(), "data_registrazione", Primary!)
if l_status=DataModified! or l_status=NewModified! then
	ldt_datetime = getitemdatetime(getrow(), "data_registrazione")
	ld_date = date(ldt_datetime)
	ldt_datetime = datetime(ld_date, now())
	setitem(getrow(), "data_registrazione", ldt_datetime)
end if

ll_anno = year(today())

select max(num_reg_campionamenti) 
into   :ll_num_reg_campionamenti 
from   tes_campionamenti 
where  cod_azienda = :s_cs_xx.cod_azienda 
and    anno_reg_campionamenti = :ll_anno; 

if isnull(ll_num_reg_campionamenti) then
	ll_num_reg_campionamenti = 1
else
	ll_num_reg_campionamenti++
end if

ls_cod_azienda = GetItemstring(getrow(), "cod_azienda")
ll_anno_reg_campionamenti = GetItemnumber(getrow(), "anno_reg_campionamenti")
ll_num_reg_campionamenti2 = GetItemnumber(getrow(), "num_reg_campionamenti")

if IsNull(ls_cod_azienda) then 
	SetItem(getrow(), "cod_azienda", s_cs_xx.cod_azienda)
end if
if IsNull(ll_anno_reg_campionamenti) then 
	SetItem(getrow(), "anno_reg_campionamenti", ll_anno)	
end if
if IsNull(ll_num_reg_campionamenti2) then 
	SetItem(getrow(), "num_reg_campionamenti", ll_num_reg_campionamenti)
end if

end event

event pcd_new;call super::pcd_new;long ll_null

setnull(ll_null)

SetItem(getrow(), "anno_reg_campionamenti",ll_null)
SetItem(getrow(), "num_reg_campionamenti",ll_null)
end event

event updateend;call super::updateend;long ll_anno_reg_campionamento, ll_num_reg_campionamento
string ls_cod_piano_campionamento, ls_null, ls_cod_prodotto
string ls_oggetto, ls_messaggio, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_domanda, ls_testo
long ll_prog_riga_piano_campionamento, ll_prog_piano_campionamento, ll_ret
datetime ldt_data_validita_piano
decimal ldc_null

if rowsinserted > 0 then
	//leggi la chiave primaria della tes_campionamenti e altri parametri
	ll_anno_reg_campionamento = getitemnumber(getrow(), "anno_reg_campionamenti")
	ll_num_reg_campionamento = getitemnumber(getrow(), "num_reg_campionamenti")
	ls_cod_piano_campionamento = getitemstring(getrow(), "cod_piano_campionamento")
	ldt_data_validita_piano = getitemdatetime(getrow(), "data_validita")
	ll_prog_piano_campionamento = getitemnumber(getrow(), "prog_piano_campionamento")
	ls_cod_prodotto = getitemstring(getrow(), "cod_prodotto")
	setnull(ls_null)
	setnull(ldc_null)
		
	//inserire automaticamente i test nella det_campionamenti
	INSERT INTO det_campionamenti
		SELECT
			 :s_cs_xx.cod_azienda
			,:ll_anno_reg_campionamento
			,:ll_num_reg_campionamento
			,prog_riga_piani
			,oggetto_test
			,cod_test
			,:ldt_data_validita_piano
			,cod_misura
			,cod_prodotto
			,flag_tipo_quantita
			,numerosita_campionaria
			,percentuale_campionaria
			,livello_significativita
			,modalita_campionamento
			,flag_direzione
			,:ldc_null		//quantita
			,0					//capacita_processo
			,0					//centratura_processo
			,""					//note
			,:ls_null			//cod_reparto
			,:ls_null			//cod_lavorazione
			,:ls_null			//cod_fornitore
			,"I"					//flag_esito
			,"N"				//flag_collaudo
			,:ls_null			//cod_aql
			,0					//prog_riga (riferiro all'aql)
			,0					//numerosita_stock
			,:ls_null			//cod_valore
			,"N"				//flag_altro_valore
			,:ls_null			//altro_valore
			,:ldc_null	//valore_rif_laboratorio
		FROM det_piani_campionamento
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
				 	cod_piano_campionamento = :ls_cod_piano_campionamento AND  
				 	data_validita = :ldt_data_validita_piano AND
	          		prog_piano_campionamento = :ll_prog_piano_campionamento AND
					cod_prodotto = :ls_cod_prodotto
		;
		
		if SQLCA.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore durante l'inserimento automatico dei test: " +char(13)+ SQLCA.SQLErrText)
			ROLLBACK;
			pcca.error = c_fatal
		else
			
			//Donato 28-10-2008: modifica per impostare il valore default su cod_valore
			ll_ret = wf_imposta_default_cod_valore(ll_anno_reg_campionamento, ll_num_reg_campionamento)
			if ll_ret = -1 then
				ROLLBACK;
				pcca.error = c_fatal
			end if
			//fine modifica
			
			ls_testo = wf_crea_elenco_test(ll_anno_reg_campionamento, ll_num_reg_campionamento, ls_cod_prodotto)
			
			if isnull(ls_testo) or ls_testo = "" then
				g_mb.messagebox("OMNIA", "Non risultano test associati! Nessuna notifica sarà inviata.")
				return
			end if
			
			//messaggio di errore già lanciato dalla wf_crea_elenco_test
			if ls_testo = "error" then return
			
			ls_oggetto = string(ll_anno_reg_campionamento)+"/"+string(ll_num_reg_campionamento)  + &
												" - Notifica identificazione nuovo campionamento"
			ls_messaggio =  ""
			ls_cod_tipo_lista_dist = ""
			ls_cod_lista_dist = ""
			ls_domanda = ""
			wf_invia_mail(ls_oggetto, ls_testo, "", "", "Inviare notifica identificazione nuovo campionamento "&
									+string(ll_anno_reg_campionamento)+"/"+string(ll_num_reg_campionamento))
		end if
		
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_campionamenti_det_1,"cod_fornitore")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_campionamenti_det_1,"cod_cliente")
end choose
end event

type cb_1 from cb_piani_campionamento_ricerca within w_test_campionamenti_aut
integer x = 2171
integer y = 332
integer height = 72
integer taborder = 70
boolean bringtotop = true
fontcharset fontcharset = ansi!
end type

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_campionamenti_det_1
s_cs_xx.parametri.parametro_uo_dw_2 = dw_tes_campionamenti_det_2
end event

type cb_nc from commandbutton within w_test_campionamenti_aut
integer x = 2583
integer y = 1072
integer width = 73
integer height = 72
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;//long   ll_num_righe, ll_t, ll_anno, ll_num
//
//double ldd_reset[]
//
//s_cs_xx.parametri.parametro_d_1_a = ldd_reset
//
//s_cs_xx.parametri.parametro_d_2_a = ldd_reset
//
//ll_anno = dw_tes_campionamenti_lista.getitemnumber( dw_tes_campionamenti_lista.getrow(), "anno_non_conf")
//
//ll_num = dw_tes_campionamenti_lista.getitemnumber(dw_tes_campionamenti_lista.getrow(), "num_non_conf")
//
//if ( ll_anno = 1 ) and ( ll_num = 1 ) then 
//	
//	setnull(ll_anno)
//	
//	setnull(ll_num)
//	
//end if
//
//s_cs_xx.parametri.parametro_d_1_a[1] =	ll_anno
//		
//s_cs_xx.parametri.parametro_d_2_a[1] = ll_num
//
//window_open(w_seleziona_non_conformita,0)
//
//ll_num_righe = upperbound(s_cs_xx.parametri.parametro_d_1_a[])
//
//if ll_num_righe < 1 then
//	
//	setnull(ll_anno)
//	
//	setnull(ll_num)
//	
//	dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "anno_non_conf", ll_anno)
//		
//	dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "num_non_conf", ll_num)	
//		
//else	
//
//	for ll_t = 1 to ll_num_righe
//		
//		if ( ll_num_righe > 1 ) then		
//	
//			g_mb.messagebox("OMNIA", "Attenzione: verrà considerata solo la Prima Non Conformità selezionata!")
//	
//		end if
//		
//		ll_anno = s_cs_xx.parametri.parametro_d_1_a[1]
//		
//		ll_num = s_cs_xx.parametri.parametro_d_2_a[1]
//		
//		dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "anno_non_conf", ll_anno)
//		
//		dw_tes_campionamenti_lista.setitem( dw_tes_campionamenti_lista.getrow(), "num_non_conf", ll_num)	
//		
//		exit
//		
//	next
//
//end if
//
//return 0	
end event

