﻿$PBExportHeader$w_distinta_base_attrezzature.srw
$PBExportComments$Finestra per la gestione Distinta Base
forward
global type w_distinta_base_attrezzature from w_cs_xx_principale
end type
type cb_modifica_attrezzaggio from commandbutton within w_distinta_base_attrezzature
end type
type cb_crea_attrezzatura from commandbutton within w_distinta_base_attrezzature
end type
type em_1 from editmask within w_distinta_base_attrezzature
end type
type st_13 from statictext within w_distinta_base_attrezzature
end type
type cb_2 from commandbutton within w_distinta_base_attrezzature
end type
type dw_ricerca from u_dw_search within w_distinta_base_attrezzature
end type
type tv_db from treeview within w_distinta_base_attrezzature
end type
type cb_comprimi from commandbutton within w_distinta_base_attrezzature
end type
type cb_espandi from commandbutton within w_distinta_base_attrezzature
end type
type r_1 from rectangle within w_distinta_base_attrezzature
end type
type dw_distinta_det from uo_cs_xx_dw within w_distinta_base_attrezzature
end type
type dw_folder from u_folder within w_distinta_base_attrezzature
end type
type s_chiave_distinta from structure within w_distinta_base_attrezzature
end type
end forward

type s_chiave_distinta from structure
	string		cod_attrezzatura_padre
	long		prog_attrezzaggio
	long		num_sequenza
	string		cod_attrezzatura_figlio
end type

global type w_distinta_base_attrezzature from w_cs_xx_principale
integer width = 4462
integer height = 2384
string title = "Distinta Base Attrezzatura"
event ue_seleziona_tab ( )
cb_modifica_attrezzaggio cb_modifica_attrezzaggio
cb_crea_attrezzatura cb_crea_attrezzatura
em_1 em_1
st_13 st_13
cb_2 cb_2
dw_ricerca dw_ricerca
tv_db tv_db
cb_comprimi cb_comprimi
cb_espandi cb_espandi
r_1 r_1
dw_distinta_det dw_distinta_det
dw_folder dw_folder
end type
global w_distinta_base_attrezzature w_distinta_base_attrezzature

type variables
string is_cod_attrezzatura, is_cod_stampo, is_des_versione
long   il_prog_attrezzaggio, il_handle, il_cicli


end variables

forward prototypes
public function integer wf_inizio ()
public function integer wf_espandi (long al_handle, long al_livello, long al_cicli)
public function integer wf_pre_inizio ()
public function integer wf_comprimi (long al_handle)
public function integer wf_imposta_tv (string fs_cod_attrezzatura, integer fi_num_livello_cor, long fl_handle, ref string fs_errore)
public function integer wf_trova (string fs_cod_attrezzatura, long al_handle)
public function integer wf_trova_attrezzatura (string fs_cod_attrezzatura_padre, string fs_cod_attrezzatura_inserito, ref string fs_messaggio, ref string fs_errore)
end prototypes

event ue_seleziona_tab();dw_folder.fu_SelectTab(1)
end event

public function integer wf_inizio ();string 		 ls_errore, ls_des_attrezzatura
long 			 ll_risposta
treeviewitem tvi_campo

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_attrezzatura_padre = is_cod_attrezzatura
l_chiave_distinta.prog_attrezzaggio = il_prog_attrezzaggio
l_chiave_distinta.cod_attrezzatura_figlio = ""

tv_db.setredraw(false)
tv_db.deleteitem(0)

select descrizione
into   :ls_des_attrezzatura
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_attrezzatura = :l_chiave_distinta.cod_attrezzatura_padre;
		 
ls_des_attrezzatura = trim(ls_des_attrezzatura)

tvi_campo.itemhandle = 1
tvi_campo.data = l_chiave_distinta
tvi_campo.label = is_cod_stampo + "," + is_des_versione//l_chiave_distinta.cod_attrezzatura_padre + "," + ls_des_attrezzatura
tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1


ll_risposta = tv_db.insertitemlast(0, tvi_campo)

wf_imposta_tv( l_chiave_distinta.cod_attrezzatura_padre, 1, 1, ls_errore)

tv_db.expandall(1)
tv_db.setredraw(true)
tv_db.triggerevent("pcd_clicked")

this.title = "Distinta Base dell'attrezzatura:   " + is_cod_attrezzatura + "   Attrezzaggio: " + string(il_prog_attrezzaggio)

return 0
end function

public function integer wf_espandi (long al_handle, long al_livello, long al_cicli);long ll_handle, ll_padre, ll_i

treeviewitem ltv_item


il_cicli ++

if il_cicli > al_livello then
	return 100
end if

tv_db.getitem( al_handle, ltv_item)
tv_db.ExpandItem ( al_handle )	

ll_handle = tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	wf_espandi(ll_handle, al_livello, il_cicli)
	il_cicli --
	
end if

ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	il_cicli --
	wf_espandi(ll_handle, al_livello, il_cicli)
//	il_cicli --
	
end if

return 100
end function

public function integer wf_pre_inizio ();uo_dw_main = dw_distinta_det

tv_db.setfocus()
tv_db.triggerevent("clicked")
postevent("ue_seleziona_tab")

return 0
end function

public function integer wf_comprimi (long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)

end if

tv_db.collapseitem( ll_handle)

ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)
	
end if

tv_db.collapseitem( ll_handle)

tv_db.collapseitem( al_handle)

return 100
end function

public function integer wf_imposta_tv (string fs_cod_attrezzatura, integer fi_num_livello_cor, long fl_handle, ref string fs_errore);long              ll_num_figli, ll_num_righe, ll_handle, li_risposta
string            ls_des_attrezzatura, ls_test_attrezzatura_f, ls_errore


s_chiave_distinta l_chiave_distinta
treeviewitem 		tvi_campo
datastore 			lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta_attrezzature"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_attrezzatura, il_prog_attrezzaggio)
	
for ll_num_figli = 1 to ll_num_righe

	l_chiave_distinta.cod_attrezzatura_figlio = lds_righe_distinta.getitemstring( ll_num_figli, "cod_attrezzatura_figlio")
	l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber( ll_num_figli, "num_sequenza")
	l_chiave_distinta.cod_attrezzatura_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_attrezzatura_padre")

   select descrizione
	into   :ls_des_attrezzatura
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_attrezzatura = :l_chiave_distinta.cod_attrezzatura_figlio;
	
	ls_des_attrezzatura = trim(ls_des_attrezzatura)

	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = l_chiave_distinta
	tvi_campo.label = l_chiave_distinta.cod_attrezzatura_figlio + "," + ls_des_attrezzatura

	select cod_attrezzatura_figlio 
	into   :ls_test_attrezzatura_f
	from   distinta_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
			 cod_attrezzatura_padre = :l_chiave_distinta.cod_attrezzatura_figlio and    
			 prog_attrezzaggio = :il_prog_attrezzaggio;

	if isnull(ls_test_attrezzatura_f) or ls_test_attrezzatura_f = "" then
		tvi_campo.pictureindex = 3
		tvi_campo.selectedpictureindex = 3
		tvi_campo.overlaypictureindex = 3
		ll_handle = tv_db.insertitemlast(fl_handle, tvi_campo)
		continue
	else
		tvi_campo.pictureindex = 2
		tvi_campo.selectedpictureindex = 2
		tvi_campo.overlaypictureindex = 2
		ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
		setnull(ls_test_attrezzatura_f)
		li_risposta = wf_imposta_tv( l_chiave_distinta.cod_attrezzatura_figlio, fi_num_livello_cor + 1, ll_handle, ls_errore)
	end if

next

destroy(lds_righe_distinta)

return 0
end function

public function integer wf_trova (string fs_cod_attrezzatura, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_attrezzatura),1) > 0 then
		tv_db.setfocus()
		tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_attrezzatura,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper( fs_cod_attrezzatura),1) > 0 then
		tv_db.setfocus()
		tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova( fs_cod_attrezzatura,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_padre = al_handle

do
	
	ll_padre = tv_db.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tv_db.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tv_db.getitem(ll_handle,ltv_item)
			
			if pos(upper(ltv_item.label),upper(fs_cod_attrezzatura),1) > 0 then
				tv_db.setfocus()
				tv_db.selectitem(ll_handle)
				il_handle = ll_handle
				return 0
			else
				if wf_trova( fs_cod_attrezzatura,ll_handle) = 0 then
					return 0
				else
					return 100
				end if
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

public function integer wf_trova_attrezzatura (string fs_cod_attrezzatura_padre, string fs_cod_attrezzatura_inserito, ref string fs_messaggio, ref string fs_errore);string    ls_cod_attrezzatura_figlio,  ls_test_attrezzatura_f, ls_errore, ls_messaggio
long      ll_num_figli,ll_num_righe
integer   li_risposta

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta_attrezzature"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_attrezzatura_padre, il_prog_attrezzaggio)
	
for ll_num_figli = 1 to ll_num_righe
	
	ls_cod_attrezzatura_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_attrezzatura_figlio")
	
	if ls_cod_attrezzatura_figlio = upper(fs_cod_attrezzatura_inserito) or ls_cod_attrezzatura_figlio = lower(fs_cod_attrezzatura_inserito) then
		fs_messaggio = "Attenzione! Prodotto già presente in questo ramo!"
		return 1
	end if
	
	select cod_attrezzatura_figlio 
	into   :ls_test_attrezzatura_f
	from   distinta_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       cod_attrezzatura_padre = :ls_cod_attrezzatura_figlio and    
			 prog_attrezzaggio = :il_prog_attrezzaggio;

	if isnull(ls_test_attrezzatura_f) or ls_test_attrezzatura_f = "" then
		continue
	else
		
		li_risposta = wf_trova_attrezzatura( ls_cod_attrezzatura_figlio, fs_cod_attrezzatura_inserito, ls_messaggio, ls_errore)
		
		if li_risposta = -1 then
			fs_errore=ls_errore
			destroy lds_righe_distinta
			return -1
		end if
		
		if li_risposta = 1 then
			fs_messaggio = ls_messaggio
			destroy lds_righe_distinta
			return 1
		end if
	end if

next

destroy lds_righe_distinta

return 0
end function

on w_distinta_base_attrezzature.create
int iCurrent
call super::create
this.cb_modifica_attrezzaggio=create cb_modifica_attrezzaggio
this.cb_crea_attrezzatura=create cb_crea_attrezzatura
this.em_1=create em_1
this.st_13=create st_13
this.cb_2=create cb_2
this.dw_ricerca=create dw_ricerca
this.tv_db=create tv_db
this.cb_comprimi=create cb_comprimi
this.cb_espandi=create cb_espandi
this.r_1=create r_1
this.dw_distinta_det=create dw_distinta_det
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_modifica_attrezzaggio
this.Control[iCurrent+2]=this.cb_crea_attrezzatura
this.Control[iCurrent+3]=this.em_1
this.Control[iCurrent+4]=this.st_13
this.Control[iCurrent+5]=this.cb_2
this.Control[iCurrent+6]=this.dw_ricerca
this.Control[iCurrent+7]=this.tv_db
this.Control[iCurrent+8]=this.cb_comprimi
this.Control[iCurrent+9]=this.cb_espandi
this.Control[iCurrent+10]=this.r_1
this.Control[iCurrent+11]=this.dw_distinta_det
this.Control[iCurrent+12]=this.dw_folder
end on

on w_distinta_base_attrezzature.destroy
call super::destroy
destroy(this.cb_modifica_attrezzaggio)
destroy(this.cb_crea_attrezzatura)
destroy(this.em_1)
destroy(this.st_13)
destroy(this.cb_2)
destroy(this.dw_ricerca)
destroy(this.tv_db)
destroy(this.cb_comprimi)
destroy(this.cb_espandi)
destroy(this.r_1)
destroy(this.dw_distinta_det)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]
string ls_flag

set_w_options(c_noresizewin)

l_objects[1] = dw_distinta_det
dw_folder.fu_AssignTab(1, "Dettaglio", l_Objects[])

//l_objects_4[1] = dw_prodotti_finiti
//dw_folder.fu_AssignTab(4, "Elenco Distinte PF", l_Objects_4[])
//l_objects_4[1] = dw_distinta_base_padre
//dw_folder.fu_AssignTab(2, "Prodotto Padre", l_Objects_4[])


dw_distinta_det.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen + c_disableCC + c_disableCCinsert, c_default + c_noresizedw)

//dw_prodotti_finiti.set_dw_options( sqlca, pcca.null_object, c_disableCC + c_nonew + c_nomodify + c_nodelete, c_default + c_noresizedw)

//dw_distinta_base_padre.set_dw_options( sqlca, pcca.null_object, c_disableCC + c_nonew + c_nomodify + c_nodelete, c_default + c_noresizedw)


//dw_distinta_det.ib_proteggi_chiavi = false
uo_dw_main = dw_distinta_det


dw_folder.fu_FolderCreate(1,1)

// *** michela: metto la lettura del prodotto e della versione fuori della wf_inizio
if not isnull(i_openparm) then

	is_cod_attrezzatura = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_attrezzatura")
	il_prog_attrezzaggio = s_cs_xx.parametri.parametro_dw_2.getitemnumber(s_cs_xx.parametri.parametro_dw_2.getrow(),"prog_attrezzaggio")
	is_cod_stampo = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_stampo")
	is_des_versione = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"des_versione")
	wf_pre_inizio()
	
else
	
	dw_folder.fu_SelectTab(4)
	
end if

select flag
into :ls_flag
from parametri
where flag_parametro = 'F' and
      cod_parametro = 'ABR';

if ls_flag='S' then
	cb_crea_attrezzatura.visible = true
	cb_modifica_attrezzaggio.visible = true
end if



// *** fine


end event

type cb_modifica_attrezzaggio from commandbutton within w_distinta_base_attrezzature
boolean visible = false
integer x = 1920
integer y = 180
integer width = 325
integer height = 80
integer taborder = 90
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "MODIFICA"
end type

event clicked;treeviewitem			l_tvitem
s_chiave_distinta		l_chiave_distinta
string					ls_test, ls_des, ls_cod_cat_attrez, ls_des_attrezzatura, ls_ret
boolean					lb_chiudi

//N.B. il pulsante è visibile solo se il parm multiazienda ABR è a S

//preleva info sul nodo selezionato -----------------
if il_handle > 0 then
else
	g_mb.error("Selezionare un ramo della distinta!")
	return
end if
tv_db.GetItem(il_handle, l_tvitem)
l_chiave_distinta = l_tvitem.data


//verifica se MP ------------------------------------
select cod_attrezzatura_figlio 
into   :ls_test
from   distinta_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and	 
		 cod_attrezzatura_padre = :l_chiave_distinta.cod_attrezzatura_figlio and    
		 prog_attrezzaggio = :il_prog_attrezzaggio;

if isnull(ls_test) or ls_test = "" then
	//MP
else
	//NO MP
	
	ls_des = l_tvitem.label
	
	g_mb.error("Il codice "+ls_des+" non è una MP in distinta!")
	return
end if

//PROCEDI
try
	s_cs_xx.parametri.parametro_s_1_a[1] = l_chiave_distinta.cod_attrezzatura_figlio
	s_cs_xx.parametri.parametro_s_1_a[2] = ls_des_attrezzatura
	s_cs_xx.parametri.parametro_s_1_a[3] = l_chiave_distinta.cod_attrezzatura_padre
	s_cs_xx.parametri.parametro_s_1_a[4] = string(l_chiave_distinta.num_sequenza)
	s_cs_xx.parametri.parametro_s_1_a[5] = string(il_prog_attrezzaggio)
	s_cs_xx.parametri.parametro_s_1_a[6] = is_cod_attrezzatura
	
	setnull(s_cs_xx.parametri.parametro_s_10)
	
	window_open(w_distinta_stampi_modifica, 0)
	
	//gestione ritorno
	ls_ret = s_cs_xx.parametri.parametro_s_10
	setnull(s_cs_xx.parametri.parametro_s_10)
	
	choose case ls_ret
		case "1"
			//inserimento con nuovo attrezzaggio
			lb_chiudi = true
			
		case "2"
			//solo inserimento senza nuovo attrezzaggio
			lb_chiudi = true
			
		case else
			//annullato tutto
			lb_chiudi = false
			
	end choose
	
catch (runtimeerror e)
end try

if lb_chiudi then
	g_mb.show("La finestra della distinta sarà ora chiusa per permettere l'aggionamento dei dati!")
	close(parent)
end if


end event

type cb_crea_attrezzatura from commandbutton within w_distinta_base_attrezzature
boolean visible = false
integer x = 1586
integer y = 180
integer width = 325
integer height = 80
integer taborder = 90
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CREA ATTR."
end type

event clicked;treeviewitem			l_tvitem
s_chiave_distinta		l_chiave_distinta
string					ls_test, ls_des, ls_cod_cat_attrez, ls_des_attrezzatura

//N.B. il pulsante è visibile solo se il parm multiazienda ABR è a S

//preleva info sul nodo selezionato -----------------
if il_handle > 0 then
else
	g_mb.error("Selezionare un ramo della distinta!")
	return
end if
tv_db.GetItem(il_handle, l_tvitem)
l_chiave_distinta = l_tvitem.data


//verifica se MP ------------------------------------
select cod_attrezzatura_figlio 
into   :ls_test
from   distinta_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and	 
		 cod_attrezzatura_padre = :l_chiave_distinta.cod_attrezzatura_figlio and    
		 prog_attrezzaggio = :il_prog_attrezzaggio;

if isnull(ls_test) or ls_test = "" then
	//MP
else
	//NO MP
	
	ls_des = l_tvitem.label
	
	g_mb.error("Il codice "+ls_des+" non è una MP in distinta!")
	return
end if


//PROCEDI

//controllo se è stata specificata la categoria dell'attrezzatura
select cod_cat_attrezzature, descrizione
into :ls_cod_cat_attrez, :ls_des_attrezzatura
from anag_attrezzature
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_attrezzatura=:l_chiave_distinta.cod_attrezzatura_figlio;
		
if sqlca.sqlcode<0 then
	g_mb.error("Errore in controllo categoria!")
	return
end if

if ls_cod_cat_attrez = "" or isnull(ls_cod_cat_attrez) then
	g_mb.messagebox("OMNIA","Per il calcolo in automatico del codice attrezzatura è necessario "+ &
					"specificare la categoria di appartenenza dell'attrezzatura stessa! "+&
					"Impossibile continuare!",Exclamation!)
	return
end if


//apri una finestra di creazione codici a partire da quello selezionato
try
	s_cs_xx.parametri.parametro_s_1_a[1] = l_chiave_distinta.cod_attrezzatura_figlio
	s_cs_xx.parametri.parametro_s_1_a[2] = ls_des_attrezzatura
	s_cs_xx.parametri.parametro_s_1_a[3] = l_chiave_distinta.cod_attrezzatura_padre
	s_cs_xx.parametri.parametro_s_1_a[4] = string(l_chiave_distinta.num_sequenza)
	s_cs_xx.parametri.parametro_s_1_a[5] = string(il_prog_attrezzaggio)
	s_cs_xx.parametri.parametro_s_1_a[6] = is_cod_attrezzatura
	
	window_open(w_distinta_stampi_nuovi, 0)
catch (runtimeerror e)
end try

//refresh dati a video
if s_cs_xx.parametri.parametro_b_2 then
	wf_inizio()
end if

s_cs_xx.parametri.parametro_b_2 = false


end event

type em_1 from editmask within w_distinta_base_attrezzature
integer x = 503
integer y = 180
integer width = 389
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
double increment = 1
string minmax = "0~~99999"
end type

type st_13 from statictext within w_distinta_base_attrezzature
integer x = 23
integer y = 180
integer width = 457
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Espandi a livello:"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_distinta_base_attrezzature
integer x = 2382
integer y = 40
integer width = 91
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;string ls_cod_attrezzatura

ls_cod_attrezzatura = dw_ricerca.getitemstring( 1, "rs_cod_attrezzatura")

if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura = "" then
	g_mb.messagebox( "OMNIA", "Attenzione:selezionare un'attrezzatura per eseguire la ricerca!")
	return -1
end if

if isnull(il_handle) or il_handle = 0 then
	il_handle = tv_db.finditem(roottreeitem!,0)
end if

tv_db.setfocus()

tv_db.selectitem(il_handle)

if il_handle > 0 then
	if wf_trova( ls_cod_attrezzatura, il_handle) = 100 then
		g_mb.messagebox( "OMNIA", "Attrezzatura non trovata!", information!)
		tv_db.setfocus()
	end if
end if
end event

type dw_ricerca from u_dw_search within w_distinta_base_attrezzature
event ue_key pbm_dwnkey
integer x = 46
integer y = 20
integer width = 2313
integer height = 120
integer taborder = 20
string dragicon = "H:\CS_XX_50\cs_sep\Cs_sep.ico"
string dataobject = "d_distinta_attrezzature_padri_cerca2"
boolean border = false
end type

event constructor;call super::constructor;this.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "cs_sep.ico"
end event

event clicked;call super::clicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca,"rs_cod_attrezzatura")
		
	case else
		drag(Begin!)
	
end choose

end event

type tv_db from treeview within w_distinta_base_attrezzature
integer x = 23
integer y = 280
integer width = 1989
integer height = 1960
integer taborder = 190
string dragicon = "Exclamation!"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
boolean disabledragdrop = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")

end event

event clicked;if handle<>0 then
	il_handle = handle
	dw_distinta_det.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
end if
end event

event key;
if key = keydelete! then
	if g_mb.messagebox( "OMNIA", "Sei sicuro di voler cancellare il ramo di distinta?",Question!,yesno!,2) = 1 then

		s_chiave_distinta l_chiave_distinta
		treeviewitem tvi_campo
		long ll_risposta
		
		tv_db.GetItem ( il_handle, tvi_campo )
		l_chiave_distinta = tvi_campo.data
		
		delete from distinta_attrezzature
		where cod_azienda = :s_cs_xx.cod_azienda and   
				cod_attrezzatura_padre = :l_chiave_distinta.cod_attrezzatura_padre and   
				num_sequenza = :l_chiave_distinta.num_sequenza 	and   
				cod_attrezzatura_figlio = :l_chiave_distinta.cod_attrezzatura_figlio and   
				prog_attrezzaggio = :il_prog_attrezzaggio;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			return
		end if
		
		commit;
		
		ll_risposta = wf_inizio()	

	end if
	
end if
end event

event dragwithin;TreeViewItem		ltvi_Over

If GetItem(handle, ltvi_Over) = -1 Then
	SetDropHighlight(0)

	Return
End If


SetDropHighlight(handle)

il_handle = handle
end event

event dragdrop;s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo
long         ll_risposta, ll_num_sequenza, ll_num_battute
string       ls_cod_attrezzatura_figlio, ls_cod_attrezzatura_padre, ls_messaggio, ls_errore
integer      li_risposta

GetItem (il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data
ls_cod_attrezzatura_figlio = upper(dw_ricerca.getitemstring(dw_ricerca.getrow(), "rs_cod_attrezzatura"))

if isnull(ls_cod_attrezzatura_figlio) then
	 g_mb.messagebox( "OMNIA", "Attenzione: non hai selezionato alcuna attrezzatura da inserire in distinta!",stopsign!)
	 return
end if
	
if l_chiave_distinta.cod_attrezzatura_figlio = ls_cod_attrezzatura_figlio   then
	 g_mb.messagebox("OMNIA","Attenzione: padre e figlio non possono coincidere!")
	 return
end if

// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
long ll_i, ll_handle
string  ls_cod_attrezzatura, ls_cod_figlio
string prova
treeviewitem  tvi_campo_2
s_chiave_distinta l_chiave_distinta_2
ll_i = 1
//numero del nodo da cui parto il_handle
ll_handle = il_handle

do while 1 = ll_i 

	GetItem (ll_handle, tvi_campo_2 )
	l_chiave_distinta_2 = tvi_campo_2.data
	
	if isnull(l_chiave_distinta_2.cod_attrezzatura_figlio) or len(l_chiave_distinta_2.cod_attrezzatura_figlio) < 1 then			
		ls_cod_attrezzatura = l_chiave_distinta_2.cod_attrezzatura_padre
	else
		ls_cod_attrezzatura = l_chiave_distinta_2.cod_attrezzatura_figlio
	end if
	
	if upper(ls_cod_attrezzatura) = upper(ls_cod_attrezzatura_figlio) then
		g_mb.messagebox("OMNIA","Impossibile inserire una attrezzatura come figlio di se stesso!~r~nOperazione annullata", stopsign!)
		return
	end if
	
	ll_handle = FindItem ( ParentTreeItem!	,  ll_handle)
	if ll_handle = -1 then exit  

loop

//*********fine*** 

select max(num_sequenza)
into   :ll_num_sequenza
from   distinta_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_attrezzatura_padre = :l_chiave_distinta.cod_attrezzatura_padre;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ll_num_sequenza) then ll_num_sequenza = 0

ll_num_sequenza = ll_num_sequenza + 10

if tvi_campo.level = 1 then
	ls_cod_attrezzatura_padre = l_chiave_distinta.cod_attrezzatura_padre
else
	ls_cod_attrezzatura_padre = l_chiave_distinta.cod_attrezzatura_figlio
end if

li_risposta = wf_trova_attrezzatura( ls_cod_attrezzatura_padre, ls_cod_attrezzatura_figlio, ls_messaggio, ls_errore)

if li_risposta = 1 then g_mb.messagebox("OMNIA",ls_messaggio,information!)

if li_risposta = -1 then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	return 
end if

//select num_battute
//into   :ll_num_battute
//from   anag_attrezzature
//where  cod_azienda = :s_cs_xx.cod_azienda and
//       cod_attrezzatura = :ls_cod_attrezzatura_figlio;

insert into distinta_attrezzature (cod_azienda,
											  cod_attrezzatura_padre,
											  prog_attrezzaggio,
											  cod_attrezzatura_figlio,
											  num_sequenza,
											  cod_attrezzatura_alt,
											  num_battute)
							 values		 (:s_cs_xx.cod_azienda,
											  :ls_cod_attrezzatura_padre,
											  :il_prog_attrezzaggio,
											  :ls_cod_attrezzatura_figlio,
											  :ll_num_sequenza,
											  null,
											  0);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

commit;

ll_risposta = wf_inizio()	
end event

type cb_comprimi from commandbutton within w_distinta_base_attrezzature
integer x = 1257
integer y = 180
integer width = 320
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Comprimi"
end type

event clicked;long ll_tvi

tv_db.setredraw( false)
ll_tvi = tv_db.FindItem(RootTreeItem! , 0)
wf_comprimi(ll_tvi)
tv_db.setredraw( true)

end event

type cb_espandi from commandbutton within w_distinta_base_attrezzature
integer x = 914
integer y = 180
integer width = 320
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Espandi"
end type

event clicked;long ll_handle, ll_livelli

ll_livelli = long(em_1.text)
il_cicli = 0

tv_db.setredraw( false)

il_handle = tv_db.finditem(roottreeitem!,0)
wf_comprimi(il_handle)

tv_db.setfocus()

tv_db.selectitem(il_handle)

if il_handle > 0 then
	wf_espandi(il_handle, ll_livelli, il_cicli)
end if

tv_db.setredraw( true)
end event

type r_1 from rectangle within w_distinta_base_attrezzature
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 23
integer width = 2469
integer height = 160
end type

type dw_distinta_det from uo_cs_xx_dw within w_distinta_base_attrezzature
integer x = 2126
integer y = 400
integer width = 2240
integer height = 1740
integer taborder = 180
string dataobject = "d_distinta_base_attrezzature_det"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  				l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem 		tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
						  l_chiave_distinta.cod_attrezzatura_padre, & 
						  il_prog_attrezzaggio, &
						  l_chiave_distinta.cod_attrezzatura_figlio, &
						  l_chiave_distinta.num_sequenza)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_new;call super::pcd_new;if i_extendmode then
	
	integer li_risposta
	long ll_sequenza
	string ls_cod_attrezzatura_padre
	
//	ib_proteggi_chiavi = false
	s_chiave_distinta l_chiave_distinta
	treeviewitem tvi_campo
	
	li_risposta = tv_db.GetItem ( il_handle, tvi_campo )

	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Selezionare almeno un elemento dalla struttura ad albero!", Exclamation!)
 	   triggerevent("pcd_delete")
	   triggerevent("pcd_save")
	   return
	end if 

	l_chiave_distinta = tvi_campo.data

	if l_chiave_distinta.cod_attrezzatura_figlio="" then
		ls_cod_attrezzatura_padre = l_chiave_distinta.cod_attrezzatura_padre
	else
		ls_cod_attrezzatura_padre = l_chiave_distinta.cod_attrezzatura_figlio
	end if
	
	setitem(getrow(), "cod_attrezzatura_padre",ls_cod_attrezzatura_padre)		
   triggerevent("itemchanged")

   select max(num_sequenza)
   into   :ll_sequenza
   from   distinta_attrezzature
   where  cod_azienda = :s_cs_xx.cod_azienda and 
          cod_attrezzatura_padre = :ls_cod_attrezzatura_padre;

   if isnull(ll_sequenza) then ll_sequenza = 0
   ll_sequenza = ll_sequenza + 10
   setitem(getrow(), "num_sequenza", ll_sequenza)
	triggerevent("itemchanged")
   setcolumn("cod_attrezzatura_figlio")	
	
end if

this.object.b_figlio.enabled = true
this.object.b_alternativo.enabled = true
end event

event pcd_modify;call super::pcd_modify;this.object.b_figlio.enabled = true
this.object.b_alternativo.enabled = true
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)	
		SetItem(l_Idx, "prog_attrezzaggio", il_prog_attrezzaggio)	
   END IF
NEXT

end event

event pcd_delete;call super::pcd_delete;triggerevent("pcd_save")
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	string ls_cod_attrezzatura_padre, ls_descrizione, ls_cod_attrezzatura
	choose case i_colname
	
	case "cod_attrezzatura_figlio"

		if len(data) < 1 then
          PCCA.Error = c_ValFailed
          return
      end if   

      ls_cod_attrezzatura_padre = upper(this.getitemstring(this.getrow(), "cod_attrezzatura_padre"))
		
		//controllo che esista il codice del prodotto
		if not isnull(data) and (len(data) > 0) then
			
			select cod_attrezzatura
			into   :ls_descrizione
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_attrezzatura = :data;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Codice attrezzatura inesistente!~r~nCorreggere il dato."+ sqlca.sqlerrtext)	
				return 1
			end if
			
		end if
		
  	    if (ls_cod_attrezzatura_padre = data) and (len(data) > 0)  then
          g_mb.messagebox("OMNIA","Attenzione: padre e figlio non possono coincidere!")
          PCCA.Error = c_ValFailed
          return
		end if
	

		// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
		long ll_handle
		string  ls_cod_prodotto, ls_cod_figlio
		treeviewitem  tvi_campo_2
		s_chiave_distinta l_chiave_distinta_2
		//numero del nodo da cui parto il_handle
		ll_handle = il_handle

		do while true
			//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
			tv_db.getItem (ll_handle, tvi_campo_2 )
			l_chiave_distinta_2 = tvi_campo_2.data
			if isnull(l_chiave_distinta_2.cod_attrezzatura_figlio) or len(l_chiave_distinta_2.cod_attrezzatura_figlio) < 1 then
				//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
				ls_cod_attrezzatura = l_chiave_distinta_2.cod_attrezzatura_padre
			else
				ls_cod_attrezzatura = l_chiave_distinta_2.cod_attrezzatura_figlio
			end if
			
			if upper(ls_cod_attrezzatura) = upper(data) then
				g_mb.messagebox("OMNIA","Impossibile inserire un'attrezzatura come figlia di se stesso!~r~nCorreggere il dato.", stopsign!)
				return 1
			end if
			//cerca il padre del nodo numero ll_handle
			ll_handle = tv_db.findItem ( ParentTreeItem!	,  ll_handle)
			if ll_handle = -1 then exit   // allora sono alla radice
		
		loop

//*********fine*** 	
		
	
      if (ls_cod_attrezzatura_padre = data) and (len(data) > 0)  then
          g_mb.messagebox("OMNIA","Attenzione: padre e figlio non possono coincidere!")
          PCCA.Error = c_ValFailed
          return
      end if
	
//		SELECT cod_misura_mag,
//		       flag_escludibile,
//				 flag_materia_prima
//      INTO   :ls_cod_misura,
//		       :ls_flag_escludibile,
//				 :ls_flag_materia_prima
//      FROM   anag_prodotti  
//      WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
//		       cod_prodotto = :data;

//      if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
//         setitem(getrow(), "cod_misura", ls_cod_misura)
//      end if
		

	end choose
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	
	integer li_risposta
	string ls_cod_attrezzatura_padre, ls_cod_attrezzatura_figlio, ls_messaggio, ls_errore

	if getitemstatus( GetRow(), 0, Primary!) = DataModified! then
	
		ls_cod_attrezzatura_padre = getitemstring(getrow(),"cod_attrezzatura_padre")
		ls_cod_attrezzatura_figlio = getitemstring(getrow(),"cod_attrezzatura_figlio")
		il_prog_attrezzaggio = getitemnumber(getrow(),"prog_attrezzaggio")
		
		li_risposta = wf_trova_attrezzatura( ls_cod_attrezzatura_padre, ls_cod_attrezzatura_figlio, ls_messaggio, ls_errore)
		
		if li_risposta = 1 then g_mb.messagebox("OMNIA",ls_messaggio,information!)
	
		if li_risposta = -1 then
			g_mb.messagebox( "OMNIA", ls_errore, stopsign!)
			return 
		end if
		
	end if

end if

this.object.b_figlio.enabled = false
this.object.b_alternativo.enabled = false
end event

event clicked;call super::clicked;//choose case dwo.name
//	case "b_figlio"
//		
//		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
//		s_cs_xx.parametri.parametro_uo_dw_1 = dw_distinta_det
//		s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura_figlio"
//		if not isvalid(w_attrezzature_ricerca) then
//		   window_open(w_attrezzature_ricerca, 0)
//		end if
//
//		w_attrezzature_ricerca.show()
//		
//	case "b_alternativo"
//		
//		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
//		s_cs_xx.parametri.parametro_uo_dw_1 = dw_distinta_det
//		s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura_alt"
//		if not isvalid(w_attrezzature_ricerca) then
//		   window_open(w_attrezzature_ricerca, 0)
//		end if
//
//		w_attrezzature_ricerca.show()		
//end choose
end event

event pcd_view;call super::pcd_view;this.object.b_figlio.enabled = false
this.object.b_alternativo.enabled = false
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_padre"
		guo_ricerca.uof_ricerca_attrezzatura(dw_distinta_det,"cod_attrezzatura_padre")
	case "b_figlio"
		guo_ricerca.uof_ricerca_attrezzatura(dw_distinta_det,"cod_attrezzatura_figlio")
end choose
end event

type dw_folder from u_folder within w_distinta_base_attrezzature
integer x = 2080
integer y = 260
integer width = 2309
integer height = 2000
integer taborder = 120
boolean border = false
end type

