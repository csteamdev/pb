﻿$PBExportHeader$w_distinta_padri_attrezz_blob.srw
$PBExportComments$Finestra di dettaglio documenti per distinta attrezzature
forward
global type w_distinta_padri_attrezz_blob from w_cs_xx_principale
end type
type dw_distinta_attrezzature_blob_padri from uo_cs_xx_dw within w_distinta_padri_attrezz_blob
end type
type cb_note_esterne from commandbutton within w_distinta_padri_attrezz_blob
end type
end forward

global type w_distinta_padri_attrezz_blob from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2875
integer height = 1224
string title = "Documenti"
dw_distinta_attrezzature_blob_padri dw_distinta_attrezzature_blob_padri
cb_note_esterne cb_note_esterne
end type
global w_distinta_padri_attrezz_blob w_distinta_padri_attrezz_blob

type variables
blob ib_blob
string is_cod_attrezzatura
long   il_prog_attrezzaggio



end variables

on w_distinta_padri_attrezz_blob.create
int iCurrent
call super::create
this.dw_distinta_attrezzature_blob_padri=create dw_distinta_attrezzature_blob_padri
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_distinta_attrezzature_blob_padri
this.Control[iCurrent+2]=this.cb_note_esterne
end on

on w_distinta_padri_attrezz_blob.destroy
call super::destroy
destroy(this.dw_distinta_attrezzature_blob_padri)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;string ls_descrizione

dw_distinta_attrezzature_blob_padri.set_dw_key("cod_azienda")
dw_distinta_attrezzature_blob_padri.set_dw_key("cod_attrezzatura")
dw_distinta_attrezzature_blob_padri.set_dw_key("prog_attrezzaggio")
dw_distinta_attrezzature_blob_padri.set_dw_options(sqlca, &
																	i_openparm, &
																	c_scrollparent, &
																	c_default)

iuo_dw_main = dw_distinta_attrezzature_blob_padri
end event

type dw_distinta_attrezzature_blob_padri from uo_cs_xx_dw within w_distinta_padri_attrezz_blob
integer x = 23
integer y = 20
integer width = 2789
integer height = 960
integer taborder = 20
string dataobject = "d_distinta_attrezzature_blob_padri_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_descrizione, ls_cod_stampo, ls_des_versione

is_cod_attrezzatura = dw_distinta_attrezzature_blob_padri.i_parentdw.getitemstring(dw_distinta_attrezzature_blob_padri.i_parentdw.i_selectedrows[1], "cod_attrezzatura")
il_prog_attrezzaggio = dw_distinta_attrezzature_blob_padri.i_parentdw.getitemnumber(dw_distinta_attrezzature_blob_padri.i_parentdw.i_selectedrows[1], "prog_attrezzaggio")
ls_cod_stampo = dw_distinta_attrezzature_blob_padri.i_parentdw.getitemstring(dw_distinta_attrezzature_blob_padri.i_parentdw.i_selectedrows[1], "cod_stampo")
ls_des_versione = dw_distinta_attrezzature_blob_padri.i_parentdw.getitemstring(dw_distinta_attrezzature_blob_padri.i_parentdw.i_selectedrows[1], "des_versione")

if not isnull(ls_cod_stampo) and ls_cod_stampo <> "" then
	parent.title = "Documenti Attrezzatura " + ls_cod_stampo + " Attrezzaggio " + string(il_prog_attrezzaggio) + " " + ls_des_versione
else
	parent.title = "Documenti Attrezzatura " + is_cod_attrezzatura + " Attrezzaggio " + string(il_prog_attrezzaggio)
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura, il_prog_attrezzaggio)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

is_cod_attrezzatura = dw_distinta_attrezzature_blob_padri.i_parentdw.getitemstring(dw_distinta_attrezzature_blob_padri.i_parentdw.i_selectedrows[1], "cod_attrezzatura")
il_prog_attrezzaggio = dw_distinta_attrezzature_blob_padri.i_parentdw.getitemnumber(dw_distinta_attrezzature_blob_padri.i_parentdw.i_selectedrows[1], "prog_attrezzaggio")

for ll_i = 1 to this.rowcount()
   this.setitem( ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem( ll_i, "cod_attrezzatura", is_cod_attrezzatura)
	this.setitem( ll_i, "prog_attrezzaggio", il_prog_attrezzaggio)
next


end event

event pcd_save;call super::pcd_save;cb_note_esterne.enabled = true	
end event

event pcd_view;call super::pcd_view;cb_note_esterne.enabled = true	
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false	
end event

event pcd_new;call super::pcd_new;long   ll_max


select max(progressivo)
into   :ll_max
from   distinta_att_padri_blob
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :is_cod_attrezzatura and
		 prog_attrezzaggio = :il_prog_attrezzaggio;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'impostazione del progressivo documento: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ll_max) then ll_max = 0
ll_max ++

setitem( getrow(), "progressivo", ll_max)
end event

type cb_note_esterne from commandbutton within w_distinta_padri_attrezz_blob
integer x = 2446
integer y = 1000
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long    ll_i, ll_progressivo, ll_prog_mimetype, ll_prog_attrezzaggio
integer li_risposta
string  ls_db, ls_cod_attrezzatura
transaction sqlcb
blob    lbl_null

setnull(lbl_null)

ll_i = dw_distinta_attrezzature_blob_padri.getrow()

if ll_i < 1 then return

ll_progressivo = dw_distinta_attrezzature_blob_padri.getitemnumber(ll_i, "progressivo")
ll_prog_mimetype = dw_distinta_attrezzature_blob_padri.getitemnumber( ll_i, "prog_mimetype")
ls_cod_attrezzatura = dw_distinta_attrezzature_blob_padri.getitemstring( ll_i, "cod_attrezzatura")
ll_prog_attrezzaggio = dw_distinta_attrezzature_blob_padri.getitemnumber( ll_i, "prog_attrezzaggio")

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       distinta_att_padri_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_attrezzatura = :ls_cod_attrezzatura and 
				  prog_attrezzaggio = :ll_prog_attrezzaggio and
				  progressivo = :ll_progressivo
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

	
	destroy sqlcb;
	
else
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       distinta_att_padri_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_attrezzatura = :ls_cod_attrezzatura and 
				  prog_attrezzaggio = :ll_prog_attrezzaggio and
				  progressivo = :ll_progressivo;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

if isnull(s_cs_xx.parametri.parametro_bl_1) or ( not isnull(s_cs_xx.parametri.parametro_bl_1) and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) then

	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
	
	window_open(w_ole_documenti, 0)
	
	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob distinta_att_padri_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_attrezzatura = :ls_cod_attrezzatura and 
						  prog_attrezzaggio = :ll_prog_attrezzaggio and
						  progressivo = :ll_progressivo
			using      sqlcb;
				
			if sqlcb.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", sqlcb.sqlerrtext)						
			end if	
	
			destroy sqlcb;
		
		else
		
			updateblob distinta_att_padri_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_attrezzatura = :ls_cod_attrezzatura and 
						  prog_attrezzaggio = :ll_prog_attrezzaggio and
						  progressivo = :ll_progressivo;
					
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Prova", sqlca.sqlerrtext)
			end if	
			
		end if
					
		commit;
		
		dw_distinta_attrezzature_blob_padri.setitem( ll_i, "prog_mimetype", ll_prog_mimetype)
		
		parent.triggerevent("pc_save")
		
		parent.triggerevent("pc_view")
		
	end if
end if

return

end event

