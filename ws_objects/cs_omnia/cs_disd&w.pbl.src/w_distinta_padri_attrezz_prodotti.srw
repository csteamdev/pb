﻿$PBExportHeader$w_distinta_padri_attrezz_prodotti.srw
$PBExportComments$Finestra di dettaglio prodotti per distinta attrezzature
forward
global type w_distinta_padri_attrezz_prodotti from w_cs_xx_principale
end type
type cb_approva from commandbutton within w_distinta_padri_attrezz_prodotti
end type
type dw_ricerca from u_dw_search within w_distinta_padri_attrezz_prodotti
end type
type dw_distinta_padri_attrezz_prodotto_lista from uo_cs_xx_dw within w_distinta_padri_attrezz_prodotti
end type
end forward

global type w_distinta_padri_attrezz_prodotti from w_cs_xx_principale
integer width = 2967
integer height = 1380
string title = "Articoli"
cb_approva cb_approva
dw_ricerca dw_ricerca
dw_distinta_padri_attrezz_prodotto_lista dw_distinta_padri_attrezz_prodotto_lista
end type
global w_distinta_padri_attrezz_prodotti w_distinta_padri_attrezz_prodotti

type variables
string is_cod_attrezzatura
long   il_prog_attrezzaggio


end variables

on w_distinta_padri_attrezz_prodotti.create
int iCurrent
call super::create
this.cb_approva=create cb_approva
this.dw_ricerca=create dw_ricerca
this.dw_distinta_padri_attrezz_prodotto_lista=create dw_distinta_padri_attrezz_prodotto_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_approva
this.Control[iCurrent+2]=this.dw_ricerca
this.Control[iCurrent+3]=this.dw_distinta_padri_attrezz_prodotto_lista
end on

on w_distinta_padri_attrezz_prodotti.destroy
call super::destroy
destroy(this.cb_approva)
destroy(this.dw_ricerca)
destroy(this.dw_distinta_padri_attrezz_prodotto_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_descrizione

dw_distinta_padri_attrezz_prodotto_lista.set_dw_key("cod_azienda")
dw_distinta_padri_attrezz_prodotto_lista.set_dw_key("cod_attrezzatura")
dw_distinta_padri_attrezz_prodotto_lista.set_dw_key("prog_attrezzaggio")
dw_distinta_padri_attrezz_prodotto_lista.set_dw_options(sqlca, &
																		  i_openparm, &
																		  c_nonew + c_scrollparent, &
																		  c_default)

iuo_dw_main = dw_distinta_padri_attrezz_prodotto_lista
end event

type cb_approva from commandbutton within w_distinta_padri_attrezz_prodotti
integer x = 2491
integer y = 1140
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Inserisci"
end type

event clicked;string ls_cod_prodotto, ls_null
dec{4} ld_quan_prodotta
long   ll_num_battute

dw_ricerca.accepttext()

ls_cod_prodotto = dw_ricerca.getitemstring( dw_ricerca.getrow(), "rs_cod_prodotto")
ld_quan_prodotta = dw_ricerca.getitemnumber( dw_ricerca.getrow(), "quan_prodotta")
ll_num_battute = dw_ricerca.getitemnumber( dw_ricerca.getrow(), "num_battute")


insert into distinta_att_padri_prod ( cod_azienda,
												  cod_attrezzatura,
												  prog_attrezzaggio,
												  cod_prodotto,
												  num_battute,
												  quan_prodotta)
									values   ( :s_cs_xx.cod_azienda,
									           :is_cod_attrezzatura,
												  :il_prog_attrezzaggio,
												  :ls_cod_prodotto,
												  1,
												  :ld_quan_prodotta);
												  
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'inserimento dell'articolo:" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

parent.triggerevent("pc_retrieve")

setnull(ls_null)
dw_ricerca.setitem( dw_ricerca.getrow(), "rs_cod_prodotto", ls_null)
dw_ricerca.setitem( dw_ricerca.getrow(), "quan_prodotta", 1)
dw_ricerca.setitem( dw_ricerca.getrow(), "num_battute", 1)
end event

type dw_ricerca from u_dw_search within w_distinta_padri_attrezz_prodotti
event ue_key pbm_dwnkey
integer x = 23
integer y = 940
integer width = 2880
integer height = 320
integer taborder = 30
string dataobject = "d_distinta_attrezzature_prod_padri_cerca"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
end choose
end event

type dw_distinta_padri_attrezz_prodotto_lista from uo_cs_xx_dw within w_distinta_padri_attrezz_prodotti
integer x = 23
integer y = 20
integer width = 2880
integer height = 900
integer taborder = 20
string dataobject = "d_distinta_attrezzature_prod_padri_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG   l_Error
string ls_descrizione, ls_cod_stampo, ls_des_versione

is_cod_attrezzatura = dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.getitemstring(dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.i_selectedrows[1], "cod_attrezzatura")
il_prog_attrezzaggio = dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.getitemnumber(dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.i_selectedrows[1], "prog_attrezzaggio")
ls_cod_stampo = dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.getitemstring(dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.i_selectedrows[1], "cod_stampo")
ls_des_versione = dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.getitemstring(dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.i_selectedrows[1], "des_versione")

if not isnull(ls_cod_stampo) and ls_cod_stampo <> "" then
	parent.title = "Articoli Attrezzatura " + ls_cod_stampo + " Attrezzaggio " + string(il_prog_attrezzaggio) + " " + ls_des_versione
else
	parent.title = "Articoli Attrezzatura " + is_cod_attrezzatura + "  Attrezzaggio " + string(il_prog_attrezzaggio)
end if

l_Error = Retrieve( s_cs_xx.cod_azienda, is_cod_attrezzatura, il_prog_attrezzaggio)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

is_cod_attrezzatura = dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.getitemstring(dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.i_selectedrows[1], "cod_attrezzatura")
il_prog_attrezzaggio = dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.getitemnumber(dw_distinta_padri_attrezz_prodotto_lista.i_parentdw.i_selectedrows[1], "prog_attrezzaggio")

for ll_i = 1 to this.rowcount()
	
   this.setitem( ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem( ll_i, "cod_attrezzatura", is_cod_attrezzatura)
	this.setitem( ll_i, "prog_attrezzaggio", il_prog_attrezzaggio)
	
next


end event

