﻿$PBExportHeader$w_distinta_padri_attrezzature.srw
$PBExportComments$Window distinta padri attrezzature
forward
global type w_distinta_padri_attrezzature from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_distinta_padri_attrezzature
end type
type cb_1 from commandbutton within w_distinta_padri_attrezzature
end type
type cb_componenti from commandbutton within w_distinta_padri_attrezzature
end type
type dw_distinta_padri_attrezzature_det from uo_cs_xx_dw within w_distinta_padri_attrezzature
end type
type cb_reset from commandbutton within w_distinta_padri_attrezzature
end type
type cb_ricerca from commandbutton within w_distinta_padri_attrezzature
end type
type dw_ricerca from u_dw_search within w_distinta_padri_attrezzature
end type
type dw_distinta_padri_attrezzature_lista from uo_cs_xx_dw within w_distinta_padri_attrezzature
end type
end forward

global type w_distinta_padri_attrezzature from w_cs_xx_principale
integer width = 2757
integer height = 2144
string title = "Distinta Padri Attrezzature"
event ue_cerca_prodotto ( )
event ue_seleziona_versione ( )
cb_2 cb_2
cb_1 cb_1
cb_componenti cb_componenti
dw_distinta_padri_attrezzature_det dw_distinta_padri_attrezzature_det
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_ricerca dw_ricerca
dw_distinta_padri_attrezzature_lista dw_distinta_padri_attrezzature_lista
end type
global w_distinta_padri_attrezzature w_distinta_padri_attrezzature

event ue_cerca_prodotto();dw_ricerca.setitem( 1, "rs_cod_prodotto", s_cs_xx.parametri.parametro_s_1)
dw_ricerca.fu_buildsearch(TRUE)
dw_distinta_padri_attrezzature_lista.change_dw_current()
triggerevent("pc_retrieve")
postevent("ue_seleziona_versione")
end event

event ue_seleziona_versione();string ls_cod_versione
long ll_i

for ll_i = 1 to dw_distinta_padri_attrezzature_lista.rowcount()
	
	ls_cod_versione = dw_distinta_padri_attrezzature_lista.getitemstring( ll_i, "cod_versione")
	
	if not isnull(ls_cod_versione) and ls_cod_versione <> "" and ls_cod_versione = s_cs_xx.parametri.parametro_s_2 and not isnull(s_cs_xx.parametri.parametro_s_2) and s_cs_xx.parametri.parametro_s_2 <> "" then
		dw_distinta_padri_attrezzature_lista.setrow( ll_i)
		dw_distinta_padri_attrezzature_lista.scrolltorow( ll_i)
		dw_distinta_padri_attrezzature_lista.selectrow( ll_i, true)
	else
		dw_distinta_padri_attrezzature_lista.selectrow( ll_i, false)
	end if
	
next

cb_componenti.visible = false
end event

on w_distinta_padri_attrezzature.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.cb_componenti=create cb_componenti
this.dw_distinta_padri_attrezzature_det=create dw_distinta_padri_attrezzature_det
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_ricerca=create dw_ricerca
this.dw_distinta_padri_attrezzature_lista=create dw_distinta_padri_attrezzature_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_componenti
this.Control[iCurrent+4]=this.dw_distinta_padri_attrezzature_det
this.Control[iCurrent+5]=this.cb_reset
this.Control[iCurrent+6]=this.cb_ricerca
this.Control[iCurrent+7]=this.dw_ricerca
this.Control[iCurrent+8]=this.dw_distinta_padri_attrezzature_lista
end on

on w_distinta_padri_attrezzature.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.cb_componenti)
destroy(this.dw_distinta_padri_attrezzature_det)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_ricerca)
destroy(this.dw_distinta_padri_attrezzature_lista)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[],ls_test


dw_distinta_padri_attrezzature_lista.set_dw_key("cod_azienda")

dw_distinta_padri_attrezzature_lista.set_dw_options(sqlca,pcca.null_object, & 
													c_noretrieveonopen,c_default)

dw_distinta_padri_attrezzature_det.set_dw_options(sqlca,dw_distinta_padri_attrezzature_lista, & 
												 c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_distinta_padri_attrezzature_lista



l_criteriacolumn[1] = "rs_cod_attrezzatura"
l_criteriacolumn[2] = "rs_cod_stampo"
l_searchtable[1] = "distinta_attrezzature_padri"
l_searchtable[2] = "distinta_attrezzature_padri"
l_searchcolumn[1] = "cod_attrezzatura"
l_searchcolumn[2] = "cod_stampo"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_distinta_padri_attrezzature_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_distinta_padri_attrezzature_lista.change_dw_current()
end event

event close;call super::close;if isvalid(w_distinta_base) then
	close(w_distinta_base)
end if
end event

type cb_2 from commandbutton within w_distinta_padri_attrezzature
integer x = 2331
integer y = 20
integer width = 366
integer height = 80
integer taborder = 71
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_distinta_padri_attrezzature_lista.accepttext()
window_open_parm( w_distinta_padri_attrezz_blob, -1, dw_distinta_padri_attrezzature_lista)


end event

type cb_1 from commandbutton within w_distinta_padri_attrezzature
integer x = 2331
integer y = 120
integer width = 366
integer height = 80
integer taborder = 71
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Articoli"
end type

event clicked;dw_distinta_padri_attrezzature_lista.accepttext()
window_open_parm( w_distinta_padri_attrezz_prodotti, -1, dw_distinta_padri_attrezzature_lista)
end event

type cb_componenti from commandbutton within w_distinta_padri_attrezzature
integer x = 2331
integer y = 1920
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Componenti"
end type

event clicked;long ll_risposta

s_cs_xx.parametri.parametro_dw_2 = dw_distinta_padri_attrezzature_det

window_open_parm( w_distinta_base_attrezzature, -1, dw_distinta_padri_attrezzature_lista)

if isvalid( w_distinta_base_attrezzature) then
	ll_risposta = w_distinta_base_attrezzature.wf_inizio()
	w_distinta_base_attrezzature.tv_db.SelectItem (1)
end if

end event

type dw_distinta_padri_attrezzature_det from uo_cs_xx_dw within w_distinta_padri_attrezzature
integer x = 23
integer y = 800
integer width = 2674
integer height = 1100
integer taborder = 140
string dataobject = "d_distinta_attrezzature_padri_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_distinta_padri_attrezzature_det,"cod_cliente")
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_distinta_padri_attrezzature_det,"cod_attrezzatura")
end choose
end event

type cb_reset from commandbutton within w_distinta_padri_attrezzature
integer x = 2331
integer y = 600
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_distinta_padri_attrezzature_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_distinta_padri_attrezzature
integer x = 2331
integer y = 480
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type dw_ricerca from u_dw_search within w_distinta_padri_attrezzature
event ue_key pbm_dwnkey
integer x = 23
integer y = 580
integer width = 2674
integer height = 200
integer taborder = 40
string dataobject = "d_distinta_attrezzature_padri_cerca"
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca,"rs_cod_attrezzatura")
end choose
end event

type dw_distinta_padri_attrezzature_lista from uo_cs_xx_dw within w_distinta_padri_attrezzature
integer x = 23
integer y = 20
integer width = 2286
integer height = 540
integer taborder = 130
string dataobject = "d_distinta_attrezzature_padri_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "creato_da", s_cs_xx.cod_utente)
		SetItem(l_Idx, "data_creazione", datetime(today()))
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;if i_extendmode then
	dw_distinta_padri_attrezzature_det.object.b_ricerca_att.enabled = true
	dw_distinta_padri_attrezzature_det.object.b_ricerca_cliente.enabled = true
	dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	dw_distinta_padri_attrezzature_det.object.b_ricerca_att.enabled = false
	dw_distinta_padri_attrezzature_det.object.b_ricerca_cliente.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
dw_distinta_padri_attrezzature_det.object.b_ricerca_att.enabled = false
dw_distinta_padri_attrezzature_det.object.b_ricerca_cliente.enabled = false
end if
end event

event pcd_validaterow;call super::pcd_validaterow;
if i_extendmode then
	
	long    ll_prog_attrezzaggio, ll_prog_attrezzaggio_esistente, ll_cont, ll_appo
	string  ls_cod_attrezzatura, ls_test, ls_test_padre, ls_cod_stampo, ls_appo, ls_appo_old
	boolean lb_flag_ok

	ls_cod_attrezzatura = getitemstring( getrow(), "cod_attrezzatura")
	ll_prog_attrezzaggio = getitemnumber( getrow(), "prog_attrezzaggio")
	ls_cod_stampo = getitemstring( getrow(), "cod_stampo")
	
	if isnull(ls_cod_stampo) or ls_cod_stampo = "" then
		g_mb.messagebox( "OMNIA", "Attenzione: il codice stampo non può essere nullo!", stopsign!)
dw_ricerca.object.b_ricerca_attrezzatura.enabled = true
dw_ricerca.object.b_ricerca_ricerca_cliente.enabled = true
		dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1
		pcca.error = c_fatal
		return
	end if
	
	// *** controllo che lo stesso codice stampo non venga usato più volte
	
	// Aggiunta Daniele 6 Maggio 2008 su richiesta di Marco

	// E' sbagliato fare il controllo se non ho modificato il codice stampo
	if getitemstatus(getrow(), "cod_stampo", Primary!) = datamodified! or &
		getitemstatus(getrow(), 0, Primary!) = newmodified! then
	// Fine Aggiunta Daniele 6 Maggio 2008 (anche end if sotto)	
		select count(*)
		into   :ll_cont
		from   distinta_attrezzature_padri
		where  cod_azienda = :s_cs_xx.cod_azienda and
 				 cod_stampo = :ls_cod_stampo and
				 prog_attrezzaggio = :ll_prog_attrezzaggio;
			 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante il controllo esistenza stampo: " + sqlca.sqlerrtext, stopsign!)
dw_ricerca.object.b_ricerca_attrezzatura.enabled = true
dw_ricerca.object.b_ricerca_ricerca_cliente.enabled = true
			dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1		
			pcca.error = c_fatal
			return
		end if
		
		if not isnull(ll_cont) and ll_cont > 0 then
			g_mb.messagebox( "OMNIA", "Attenzione: esiste già una versione con questo codice stampo e questo attrezzaggio!", stopsign!)
dw_ricerca.object.b_ricerca_attrezzatura.enabled = true
dw_ricerca.object.b_ricerca_ricerca_cliente.enabled = true
			dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1		
			pcca.error = c_fatal
			return 
		end if
	
	end if

	// *** controllo che lo stesso codice attrezzatura non venga usato più volte con lo stesso attrezzaggio
			
	// Aggiunta Daniele 6 Maggio 2008 su richiesta di Marco

	// E' sbagliato fare il controllo se non ho modificato il codice attrezzatura
	if getitemstatus(getrow(), "cod_attrezzatura", Primary!) = datamodified! or &
		getitemstatus(getrow(), 0, Primary!) = newmodified! then
	// Fine Aggiunta Daniele 6 Maggio 2008 (anche end if sotto)	
		select count(*)
		into   :ll_cont
		from   distinta_attrezzature_padri
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_attrezzatura = :ls_cod_attrezzatura and
				 prog_attrezzaggio = :ll_prog_attrezzaggio;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante il controllo esistenza attrezzatura/prog attrezzaggio: " + sqlca.sqlerrtext, stopsign!)
dw_ricerca.object.b_ricerca_attrezzatura.enabled = true
dw_ricerca.object.b_ricerca_ricerca_cliente.enabled = true
			dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1		
			pcca.error = c_fatal
			return
		end if
		
		if not isnull(ll_cont) and ll_cont > 0 then
			g_mb.messagebox( "OMNIA", "Attenzione: esiste già una versione con questo codice attrezzatura e questo attrezzaggio!", stopsign!)
dw_ricerca.object.b_ricerca_attrezzatura.enabled = true
dw_ricerca.object.b_ricerca_ricerca_cliente.enabled = true
			dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1		
			pcca.error = c_fatal
			return 
		end if	
	end if

	//		 michela 23/01/2007: eliminato controllo su richiesta di marco
	
//	declare cu_att cursor for
//	select distinct cod_attrezzatura
//	from   			 distinta_attrezzature_padri
//	where  			 cod_azienda = :s_cs_xx.cod_azienda and
//			 			 cod_stampo = :ls_cod_stampo;
//			 
//	open cu_att;
//	if sqlca.sqlcode <> 0 then
//		messagebox( "OMNIA", "Errore durante il controllo attrezzature per stampo:" + sqlca.sqlerrtext)
//		cb_attrezzatura.enabled = true
//		cb_ricerca_cliente.enabled = true
//		dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1		
//		pcca.error = c_fatal
//		return
//	end if
//
//	ll_appo = 0
//	ls_appo = ""
//	
//	do while true
//		fetch cu_att into :ls_appo;
//		if sqlca.sqlcode = 100 then exit
//		if sqlca.sqlcode < 0 then 
//			messagebox( "OMNIA", "Errore durante la fetch del cursore delle attrezzature: " + sqlca.sqlerrtext)
//			cb_attrezzatura.enabled = true
//			cb_ricerca_cliente.enabled = true
//			dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1			
//			return -1
//		end if
//		
//		if not isnull(ls_appo) and ls_appo <> "" then ll_appo ++
//		
//	loop
//	
//	close cu_att;
//	
//	if ll_appo > 1 then
//		messagebox( "OMNIA", "Attenzione: questo codice stampo è utilizzato con più attrezzature diverse. Impossibile continuare!", stopsign!)
//		cb_attrezzatura.enabled = true
//		cb_ricerca_cliente.enabled = true
//		dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 1		
//		pcca.error = c_fatal
//		return
//	elseif ll_appo = 1 and ls_appo <> ls_cod_attrezzatura then
//		messagebox( "OMNIA", "Attenzione: questo codice stampo è utilizzato con più attrezzature diverse. Impossibile continuare!", stopsign!)
//		pcca.error = c_fatal
//		return		
//	end if	
	
	// *** controllo che la stessa attrezzatura non sia associata a + stampi
	//		 michela 23/01/2007: eliminato controllo su richiesta di marco
	
//	declare cu_stampi cursor for
//	select distinct cod_stampo
//	from   			 distinta_attrezzature_padri
//	where  			 cod_azienda = :s_cs_xx.cod_azienda and
//			 			 cod_attrezzatura = :ls_cod_attrezzatura;
//			 
//	open cu_stampi;
//	if sqlca.sqlcode <> 0 then
//		messagebox( "OMNIA", "2.Errore durante il controllo attrezzature per stampo:" + sqlca.sqlerrtext)
//		pcca.error = c_fatal
//		return
//	end if
//
//	ll_appo = 0
//	ls_appo = ""
//	
//	do while true
//		fetch cu_stampi into :ls_appo;
//		if sqlca.sqlcode = 100 then exit
//		if sqlca.sqlcode < 0 then 
//			messagebox( "OMNIA", "Errore durante la fetch del cursore degli stampi: " + sqlca.sqlerrtext)
//			return -1
//		end if
//		
//		if not isnull(ls_appo) and ls_appo <> "" then ll_appo ++
//		
//	loop
//	
//	close cu_stampi;
//	
//	if ll_appo > 1 then
//		messagebox( "OMNIA", "Attenzione: questo codice attrezzatura è utilizzata con più stampi diversi. Impossibile continuare!", stopsign!)
//		pcca.error = c_fatal
//		return
//	elseif ll_appo = 1 and ls_appo <> ls_cod_stampo then
//		messagebox( "OMNIA", "Attenzione: questo codice attrezzatura è utilizzata con più stampi diversi. Impossibile continuare!", stopsign!)
//		pcca.error = c_fatal
//		return		
//	end if		
	
	// *** fine controlli stampo
	
	setnull(ls_test_padre)
	
	select cod_azienda
	into   :ls_test_padre
	from   distinta_attrezzature_padri
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 cod_attrezzatura = :ls_cod_attrezzatura;              //verifico se il prodotto è già prodotto finito
	
	if isnull(ls_test_padre) then // se il prodotto non è un PF allora verifico se è figlio in distinta
		
		declare righe_test cursor for
		select cod_attrezzatura_figlio
		from   distinta_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 cod_attrezzatura_padre = :ls_cod_attrezzatura;
		
		open righe_test;
		
		fetch righe_test
		into  :ls_test;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			close righe_test;
			pcca.error = c_fatal
			return
		end if
		
		if sqlca.sqlcode <> 100 then // se è figlio allora verifico che la versione corrente sia anche una delle 
			close righe_test;			  // 'n' versione già esistenti
			lb_flag_ok = false
		
			declare righe_db_1 cursor for
			select  prog_attrezzaggio
			from    distinta_attrezzature
			where   cod_azienda = :s_cs_xx.cod_azienda and     
					  cod_attrezzatura_padre = :ls_cod_attrezzatura;
			
			open righe_db_1;
			
			do while 1 = 1
				fetch righe_db_1
				into  :ll_prog_attrezzaggio_esistente;
				
				if (sqlca.sqlcode = 100) then exit
				if sqlca.sqlcode<>0 then
					g_mb.messagebox( "OMNIA", "Errore nel DB: " + SQLCA.SQLErrText, stopsign!)
					close righe_db_1;
					pcca.error = c_fatal
					return
				end if
				
				if ll_prog_attrezzaggio_esistente = ll_prog_attrezzaggio then lb_flag_ok = true
			
			loop
		
			close righe_db_1;
		
			if lb_flag_ok = false then	//se la versione corrente (quella appena impostata) è diversa da quelle esistenti allora costringo l'utente a impostarne una esistente
				g_mb.messagebox( "OMNIA", "Poichè l'attrezzatura scelta è un semilavorato già utilizzato all'interno di altre distinte base è necessario che la versione immessa sia compatibile con le versioni già esistenti.", information!)
				pcca.error = c_fatal
				return
			end if
		end if
		close righe_test;
	end if
end if
end event

event clicked;call super::clicked;//long ll_risposta
//
//if isvalid(w_distinta_base) then
//	
//	s_cs_xx.parametri.parametro_dw_2 = dw_distinta_padri_attrezzature_det	
//	ll_risposta = w_distinta_base.wf_inizio()
//	
//end if


end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_distinta_padri_attrezzature_det.object.b_ricerca_att.enabled = true
	dw_distinta_padri_attrezzature_det.object.b_ricerca_cliente.enabled = true
	dw_distinta_padri_attrezzature_det.object.cod_stampo.tabsequence = 0
end if
end event

event itemchanged;call super::itemchanged;string	ls_cod_attrezzatura
long		ll_cont

choose case i_colname
		
	case "cod_stampo"
		
		declare cu_att cursor for
		select cod_attrezzatura
		from   distinta_attrezzature_padri
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_stampo = :i_coltext;
				 
		open cu_att;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "OMNIA", "Errore durante il controllo attrezzature per stampo:" + sqlca.sqlerrtext)
			return -1
		end if
	
		ll_cont = 0
		do while true
			fetch cu_att into :ls_cod_attrezzatura;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox( "OMNIA", "Errore durante la fetch del cursore delle attrezzature: " + sqlca.sqlerrtext)
				return -1
			end if
			
			if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then ll_cont ++
			
		loop
		
		close cu_att;
		
		if ll_cont = 1 then
			setitem( row, "cod_attrezzatura", ls_cod_attrezzatura)
		end if
			
		
end choose
end event

