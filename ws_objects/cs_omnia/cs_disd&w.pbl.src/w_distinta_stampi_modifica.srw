﻿$PBExportHeader$w_distinta_stampi_modifica.srw
forward
global type w_distinta_stampi_modifica from w_cs_xx_risposta
end type
type dw_selezione from u_dw_search within w_distinta_stampi_modifica
end type
end forward

global type w_distinta_stampi_modifica from w_cs_xx_risposta
integer width = 2295
integer height = 900
string title = "Modifica Attrezzaggio"
dw_selezione dw_selezione
end type
global w_distinta_stampi_modifica w_distinta_stampi_modifica

type variables
string is_cod_attrezzatura, is_des_attrezzatura, is_cod_attrezzatura_padre, is_cod_stampo
long il_num_sequenza, il_prog_attrezzaggio
end variables

forward prototypes
public function integer wf_imposta_codice ()
public function integer wf_salva (ref string fs_errore)
public function integer wf_blocca_vecchio_codice (ref string fs_errore)
public function integer wf_inserisci_ricorsivo (string fs_cod_attrezzatura_padre, long fl_prog_attrezzaggio_new, long fl_prog_attrezzaggio_old, ref string fs_errore)
public function integer wf_nuovo_attrezzaggio (string fs_cod_attrezzatura_new, ref string fs_errore)
public function integer wf_modifica_codice (string fs_nuova_attrezzatura, long fl_prog_attrezzaggio, ref string fs_errore)
end prototypes

public function integer wf_imposta_codice ();string ls_cod_cat_attrez, ls_max, ls_progressivo, ls_cod_attrezzatura

long ll_len


dw_selezione.accepttext()

ls_cod_cat_attrez = dw_selezione.getitemstring(1, "cod_cat_attrezzature")

if ls_cod_cat_attrez="" or isnull(ls_cod_cat_attrez) then
	g_mb.error("OMNIA","Per calcolare automaticamente il codice è necessario selezionare la categoria!")
	return -1
end if

//prosegui con il calcolo del codice attrezzature
ll_len = len(ls_cod_cat_attrez)

select max(substring(cod_attrezzatura, :ll_len + 1, 6 - :ll_len))
into :ls_max
from anag_attrezzature
where cod_azienda = :s_cs_xx.cod_azienda
	and left(cod_attrezzatura, :ll_len) = :ls_cod_cat_attrez
	and cod_cat_attrezzature = :ls_cod_cat_attrez;

//select substring(cod_attrezzatura, :ll_len + 1, 6 - :ll_len)
//into :ls_max
//from anag_attrezzature
//where cod_azienda = :s_cs_xx.cod_azienda and
//	cod_attrezzatura=:is_cod_attrezzatura;
	
if sqlca.sqlcode = -1 then
	g_mb.error("OMNIA","Errore durante la lettura max codice attrezzatura!"+char(13)+ &
					sqlca.sqlerrtext)
	return -1
end if

// in ls_progressivo c'è il numerico di partenza
ls_progressivo = ls_max

//può servire la prima volta
if isnull(ls_progressivo) then 
	ls_progressivo = right("000000",6 - ll_len)
else
	ls_progressivo = f_crea_codice_attr_abor(ls_progressivo)
	
	if ls_progressivo = "" then
		g_mb.error("OMNIA","Errore durante il calcolo automatico del codice attrezzatura (funzione crea codice)!")
		return -1
	end if
end if

ls_cod_attrezzatura = ls_cod_cat_attrez + ls_progressivo
dw_selezione.setitem(1,"cod_attrezzatura", ls_cod_attrezzatura)

return 1

end function

public function integer wf_salva (ref string fs_errore);string ls_cod_cat_attrezzature, ls_cod_attrezzatura, ls_descrizione, ls_flag_nuovo_attrezzaggio
string ls_domanda

//verifica campi obbligatori
ls_cod_cat_attrezzature = dw_selezione.getitemstring(1,"cod_cat_attrezzature")
ls_cod_attrezzatura = dw_selezione.getitemstring(1,"cod_attrezzatura")
ls_descrizione = dw_selezione.getitemstring(1,"descrizione")

if isnull(ls_cod_cat_attrezzature) or ls_cod_cat_attrezzature="" then
	fs_errore = "La categoria del nuovo codice è obbligatoria!"
	return -1
end if
if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura="" then
	fs_errore = "Il codice della nuova attrezzatura è obbligatorio!"
	return -1
end if
if isnull(ls_descrizione) or ls_descrizione="" then
	fs_errore = "La descrizione del nuovo codice è obbligatoria!"
	return -1
end if

ls_flag_nuovo_attrezzaggio = dw_selezione.getitemstring(1,"flag_crea_attrezzaggio")

if ls_flag_nuovo_attrezzaggio="S" then
	ls_domanda="Creo un NUOVO attrezzaggio?"
else
	ls_domanda="Procedere SENZA creare un nuovo attrezzaggio?"
end if

if g_mb.confirm(ls_domanda) then
else
	fs_errore = "Operazione annullata!"
	return 0
end if


//fin qui prosegui ....

//inserisci prima in anagrafica la nuova attrezzatura-----------------------------------
insert into anag_attrezzature
			(	cod_azienda,   
				cod_attrezzatura,   
				cod_cat_attrezzature,   
				descrizione)
values (	:s_cs_xx.cod_azienda,   
			:ls_cod_attrezzatura,   
			:ls_cod_cat_attrezzature,   
			:ls_descrizione);
			
if sqlca.sqlcode<0 then
	fs_errore = "Errore in inserimento in anagrafica codice "+ls_cod_attrezzatura+" " + sqlca.sqlerrtext
	return -1
end if
//-------------------------------------------------------------------------------

setnull(s_cs_xx.parametri.parametro_s_10)

if ls_flag_nuovo_attrezzaggio="S" then
	if wf_nuovo_attrezzaggio(ls_cod_attrezzatura,fs_errore)<0 then
		//in fs_errore il messaggio
		return -1
	end if
else
	if wf_modifica_codice(ls_cod_attrezzatura, il_prog_attrezzaggio, fs_errore)<0 then
		//in fs_errore il messaggio
		return -1
	end if
end if

if wf_blocca_vecchio_codice(fs_errore)<0 then
	//in fs_errore il messaggio
	return -1
end if

//se arrivi fin qui hai fatto tutto bene
if ls_flag_nuovo_attrezzaggio="S" then
	s_cs_xx.parametri.parametro_s_10 = "1"
else
	s_cs_xx.parametri.parametro_s_10 = "2"
end if

g_mb.show("Operazione terminata con successo!")

return 1
end function

public function integer wf_blocca_vecchio_codice (ref string fs_errore);datetime ldt_oggi

ldt_oggi = datetime(today(), 00:00:00)

update anag_attrezzature
set 	flag_blocco='S',
		data_blocco=:ldt_oggi
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura=:is_cod_attrezzatura;
		
if sqlca.sqlcode<0 then
	fs_errore = "Errore in blocco vecchio codice "+is_cod_attrezzatura+" "+sqlca.sqlerrtext
	return -1
end if

return 1
end function

public function integer wf_inserisci_ricorsivo (string fs_cod_attrezzatura_padre, long fl_prog_attrezzaggio_new, long fl_prog_attrezzaggio_old, ref string fs_errore);datastore			lds_righe_distinta
long					ll_num_righe, ll_num_figli
string				ls_cod_attrezzatura_figlio, ls_cod_attrezzatura_padre,ls_test_attrezzatura_f
long					ll_num_sequenza, ll_prog_attrezzaggio_old

lds_righe_distinta = Create DataStore
lds_righe_distinta.DataObject = "d_data_store_distinta_attrezzature"
lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_attrezzatura_padre, il_prog_attrezzaggio)
	
for ll_num_figli = 1 to ll_num_righe

	ls_cod_attrezzatura_figlio = lds_righe_distinta.getitemstring( ll_num_figli, "cod_attrezzatura_figlio")
	ll_num_sequenza = lds_righe_distinta.getitemnumber( ll_num_figli, "num_sequenza")
	ls_cod_attrezzatura_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_attrezzatura_padre")


	//duplica elemento con il nuovo prog attrezzaggio
	insert into distinta_attrezzature  
		( cod_azienda,   
		  cod_attrezzatura_padre,   
		  prog_attrezzaggio,   
		  cod_attrezzatura_figlio,   
		  num_sequenza,   
		  cod_attrezzatura_alt,   
		  num_battute )  
		select 	cod_azienda,   
					cod_attrezzatura_padre,   
					:fl_prog_attrezzaggio_new,   
					cod_attrezzatura_figlio,   
					num_sequenza,   
					cod_attrezzatura_alt,   
					num_battute
		from distinta_attrezzature
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_attrezzatura_padre=:ls_cod_attrezzatura_padre and
				cod_attrezzatura_figlio=:ls_cod_attrezzatura_figlio and
				prog_attrezzaggio =:il_prog_attrezzaggio;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore duplicazione (padre="+ls_cod_attrezzatura_padre+" figlio="+ls_cod_attrezzatura_figlio+") "+sqlca.sqlerrtext
		return -1
	end if
	//--------------------------------------------------------------------------------------------------------------
	

	//test per capire se sei arrivato all'ultima parte di un ramo distinta
	setnull(ls_test_attrezzatura_f)
	
	//vedi se c'è qualcos'altro al di là
	select cod_attrezzatura_figlio 
	into   :ls_test_attrezzatura_f
	from   distinta_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
			 cod_attrezzatura_padre = :ls_cod_attrezzatura_figlio and    
			 prog_attrezzaggio = :il_prog_attrezzaggio;

	if isnull(ls_test_attrezzatura_f) or ls_test_attrezzatura_f = "" then
		//MP, quindi ultimo pezzo di un  ramo, inutile chiamare la ricorsività
		continue
	else
		
		//richiama ricorsivo
		//adesso ilpadre diventa il figlio per la nuova ricorsiva
		if wf_inserisci_ricorsivo(ls_cod_attrezzatura_figlio,fl_prog_attrezzaggio_new,fl_prog_attrezzaggio_old,fs_errore) < 0 then
			return -1
		end if
	end if

next

destroy(lds_righe_distinta)

return 1
end function

public function integer wf_nuovo_attrezzaggio (string fs_cod_attrezzatura_new, ref string fs_errore);long ll_prog_attrezzaggio_new
datetime ldt_oggi, ldt_null

ldt_oggi = datetime(today(), 00:00:00)
setnull(ldt_null)

//primo calcola il nuovo progressivo attrezzaggio
select max(prog_attrezzaggio)
into :ll_prog_attrezzaggio_new
from distinta_attrezzature_padri
where cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:is_cod_stampo;

if sqlca.sqlcode<0 then
	fs_errore="Errore in calcolo max progressivo attrezzaggio: "+sqlca.sqlerrtext
	return -1
end if

if isnull(ll_prog_attrezzaggio_new) or ll_prog_attrezzaggio_new<=0 then
	//strano errore, segnala
	fs_errore="Errore in calcolo max progressivo attrezzaggio: risultato nullo! Verificare! "
	return -1
end if

//incrementa
ll_prog_attrezzaggio_new += 1

//secondo: inserisci testata
insert into distinta_attrezzature_padri  
		( cod_azienda,   
		  cod_attrezzatura,   
		  prog_attrezzaggio,   
		  cod_cliente,   
		  creato_da,   
		  approvato_da,   
		  des_versione,   
		  data_creazione,   
		  data_approvazione,   
		  flag_blocco,   
		  data_blocco,   
		  cod_stampo )  
	select     cod_azienda,   
				  cod_attrezzatura,   
				  :ll_prog_attrezzaggio_new,   
				  cod_cliente,   
				  creato_da,   
				  approvato_da,   
				  des_versione,   
				  :ldt_oggi,   
				  data_approvazione,   
				  'N',   
				  :ldt_null,   
				  cod_stampo
	from distinta_attrezzature_padri
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:is_cod_stampo and
			prog_attrezzaggio=:il_prog_attrezzaggio;
	
if sqlca.sqlcode<0 then
	fs_errore="Errore in inserimento nuovo attrezzaggio: "+sqlca.sqlerrtext
	return -1
end if
		

//inserisci ricorsivamente i componenti
if wf_inserisci_ricorsivo(is_cod_stampo,ll_prog_attrezzaggio_new,il_prog_attrezzaggio,fs_errore)<0 then
	return -1
end if

//se arrivi fin qui modifica il nodo relativo alla sostituzionme componente
if wf_modifica_codice(fs_cod_attrezzatura_new, ll_prog_attrezzaggio_new, fs_errore)<0 then
	//in fs_errore il messaggio
	return -1
end if

return 1
end function

public function integer wf_modifica_codice (string fs_nuova_attrezzatura, long fl_prog_attrezzaggio, ref string fs_errore);
//inserisci il nuovo -----------------------------------------------
insert into distinta_attrezzature
	(	cod_azienda,   
		cod_attrezzatura_padre,   
		prog_attrezzaggio,   
		cod_attrezzatura_figlio,   
		num_sequenza,   
		cod_attrezzatura_alt,   
		num_battute)
 	select	cod_azienda,   
				cod_attrezzatura_padre,   
				prog_attrezzaggio,   
				:fs_nuova_attrezzatura,   
				num_sequenza,   
				cod_attrezzatura_alt,   
				num_battute
	from distinta_attrezzature
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura_padre=:is_cod_attrezzatura_padre and
			prog_attrezzaggio=:fl_prog_attrezzaggio and
			cod_attrezzatura_figlio=:is_cod_attrezzatura and
			num_sequenza=:il_num_sequenza;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in inserimento nuovo codice in distinta_attrezzature: "+sqlca.sqlerrtext
	return -1
end if

//cancella il vecchio -----------------------------------------------
delete from distinta_attrezzature
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_attrezzatura_padre=:is_cod_attrezzatura_padre and
		prog_attrezzaggio=:fl_prog_attrezzaggio and
		cod_attrezzatura_figlio=:is_cod_attrezzatura and
		num_sequenza=:il_num_sequenza;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in eliminazione vecchio codice dalla distinta: "+sqlca.sqlerrtext
	return -1
end if

return 1
end function

on w_distinta_stampi_modifica.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
end on

on w_distinta_stampi_modifica.destroy
call super::destroy
destroy(this.dw_selezione)
end on

event pc_setwindow;call super::pc_setwindow;
set_w_options(c_closenosave + c_autoposition + c_noresizewin)


is_cod_attrezzatura = s_cs_xx.parametri.parametro_s_1_a[1]
is_des_attrezzatura = s_cs_xx.parametri.parametro_s_1_a[2]
is_cod_attrezzatura_padre = s_cs_xx.parametri.parametro_s_1_a[3]
il_num_sequenza = long(s_cs_xx.parametri.parametro_s_1_a[4])
il_prog_attrezzaggio = long(s_cs_xx.parametri.parametro_s_1_a[5])
is_cod_stampo = s_cs_xx.parametri.parametro_s_1_a[6]

setnull(s_cs_xx.parametri.parametro_s_1_a[1])
setnull(s_cs_xx.parametri.parametro_s_1_a[2])
setnull(s_cs_xx.parametri.parametro_s_1_a[3])
setnull(s_cs_xx.parametri.parametro_s_1_a[4])
setnull(s_cs_xx.parametri.parametro_s_1_a[5])
setnull(s_cs_xx.parametri.parametro_s_1_a[6])

this.title = "Modifica codice e attrezzaggio "+is_cod_attrezzatura + " - "+is_des_attrezzatura+&
					" per Stampo "+is_cod_stampo+" (attrezzaggio="+string(il_prog_attrezzaggio)+&
									")"


end event

event pc_setddlb;call super::pc_setddlb;

f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
					  "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana='N'")
end event

type dw_selezione from u_dw_search within w_distinta_stampi_modifica
integer x = 14
integer y = 24
integer width = 2185
integer height = 736
integer taborder = 10
string dataobject = "d_distinta_stampi_modifica"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_errore

if row>0 then
	
	this.accepttext()
	
	choose case dwo.name
		case "b_conferma"
			if wf_salva(ls_errore) < 0 then
				g_mb.error(ls_errore)
				rollback;
			else
				commit;
				
				close(parent)
				
			end if
			
			return
			
	end choose
end if
end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

if row>0 then
	choose case dwo.name
		case "cod_cat_attrezzature"
			if data<>"" and not isnull(data) then
				wf_imposta_codice()
			else
				setitem(1, "cod_attrezzatura", ls_null)
			end if
			
	end choose
end if
end event

