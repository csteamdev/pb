﻿$PBExportHeader$w_distinta_stampi_nuovi.srw
forward
global type w_distinta_stampi_nuovi from w_cs_xx_risposta
end type
type cb_salva from commandbutton within w_distinta_stampi_nuovi
end type
type dw_lista from datawindow within w_distinta_stampi_nuovi
end type
type cb_imposta from commandbutton within w_distinta_stampi_nuovi
end type
type st_title from statictext within w_distinta_stampi_nuovi
end type
type em_num from editmask within w_distinta_stampi_nuovi
end type
end forward

global type w_distinta_stampi_nuovi from w_cs_xx_risposta
integer width = 2574
integer height = 1828
string title = "Imposta nuovi codici per la distinta"
cb_salva cb_salva
dw_lista dw_lista
cb_imposta cb_imposta
st_title st_title
em_num em_num
end type
global w_distinta_stampi_nuovi w_distinta_stampi_nuovi

type variables
string is_cod_attrezzatura, is_des_attrezzatura, is_cod_attrezzatura_padre, is_cod_stampo
long il_num_sequenza, il_prog_attrezzaggio
end variables

on w_distinta_stampi_nuovi.create
int iCurrent
call super::create
this.cb_salva=create cb_salva
this.dw_lista=create dw_lista
this.cb_imposta=create cb_imposta
this.st_title=create st_title
this.em_num=create em_num
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_salva
this.Control[iCurrent+2]=this.dw_lista
this.Control[iCurrent+3]=this.cb_imposta
this.Control[iCurrent+4]=this.st_title
this.Control[iCurrent+5]=this.em_num
end on

on w_distinta_stampi_nuovi.destroy
call super::destroy
destroy(this.cb_salva)
destroy(this.dw_lista)
destroy(this.cb_imposta)
destroy(this.st_title)
destroy(this.em_num)
end on

event pc_setwindow;call super::pc_setwindow;

is_cod_attrezzatura = s_cs_xx.parametri.parametro_s_1_a[1]
is_des_attrezzatura = s_cs_xx.parametri.parametro_s_1_a[2]
is_cod_attrezzatura_padre = s_cs_xx.parametri.parametro_s_1_a[3]
il_num_sequenza = long(s_cs_xx.parametri.parametro_s_1_a[4])
il_prog_attrezzaggio = long(s_cs_xx.parametri.parametro_s_1_a[5])
is_cod_stampo = s_cs_xx.parametri.parametro_s_1_a[6]

setnull(s_cs_xx.parametri.parametro_s_1_a[1])
setnull(s_cs_xx.parametri.parametro_s_1_a[2])
setnull(s_cs_xx.parametri.parametro_s_1_a[3])
setnull(s_cs_xx.parametri.parametro_s_1_a[4])
setnull(s_cs_xx.parametri.parametro_s_1_a[5])
setnull(s_cs_xx.parametri.parametro_s_1_a[6])

this.title = "Nuovi codici per "+is_cod_stampo+" (attrezzaggio="+string(il_prog_attrezzaggio)+&
									") a partire da "+is_cod_attrezzatura + " - "+is_des_attrezzatura


end event

type cb_salva from commandbutton within w_distinta_stampi_nuovi
integer x = 1033
integer y = 1604
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea"
end type

event clicked;long ll_index, ll_tot
string ls_esiste, ls_cod_attrezzatura, ls_des_attrezzatura

dw_lista.accepttext()

if g_mb.confirm("Impostare i codici nella distinta base? "+&
				"(NOTA quelli non esistenti saranno prima inseriti in anagrafica)") then
else
	return
end if

ll_tot = dw_lista.rowcount()

for ll_index=1 to ll_tot
	
	ls_esiste = dw_lista.getitemstring(ll_index, "flag_esiste")
	ls_cod_attrezzatura = dw_lista.getitemstring(ll_index, "cod_attrezzatura")
	
	if ls_esiste="N" then
		//inserisci codice prima in anagrafica
		
		ls_des_attrezzatura = dw_lista.getitemstring(ll_index, "des_attrezzatura")
		
		insert into anag_attrezzature
			(	cod_azienda,   
				cod_attrezzatura,   
				cod_prodotto,   
				cod_versione,   
				cod_cat_attrezzature,   
				descrizione,   
				fabbricante,   
				modello,   
				num_matricola,   
				data_acquisto,   
				periodicita_revisione,   
				unita_tempo,   
				utilizzo_scadenza,   
				cod_reparto,   
				certificata_fabbricante,   
				certificata_ce,   
				cod_inventario,   
				costo_orario_medio,   
				cod_area_aziendale,   
				cod_utente,   
				reperibilita,   
				grado_precisione,   
				grado_precisione_um,   
				num_certificato_ente,   
				data_certificato_ente,   
				denominazione_ente,   
				qualita_attrezzatura,   
				cod_centro_costo,   
				flag_primario,   
				flag_blocco,   
				data_blocco,   
				note,   
				data_prima_attivazione,   
				data_installazione,   
				flag_reg_man,   
				flag_manuale,   
				flag_duplicazione,   
				flag_conta_ore,   
				costo_acquisto,   
				freq_guasti,   
				accessibilita,   
				disp_ricamnbi,   
				stato_attuale,   
				id_attrezzatura,   
				ordinamento,   
				num_battute,   
				num_battute_raggiunte,   
				cod_cliente,   
				flag_stato,   
				cod_divisione,   
				latitudine,   
				longitudine )
			select 	cod_azienda,   
						:ls_cod_attrezzatura,   
						cod_prodotto,   
						cod_versione,   
						cod_cat_attrezzature,   
						:ls_des_attrezzatura,   
						fabbricante,   
						modello,   
						num_matricola,   
						data_acquisto,   
						periodicita_revisione,   
						unita_tempo,   
						utilizzo_scadenza,   
						cod_reparto,   
						certificata_fabbricante,   
						certificata_ce,   
						cod_inventario,   
						costo_orario_medio,   
						cod_area_aziendale,   
						cod_utente,   
						reperibilita,   
						grado_precisione,   
						grado_precisione_um,   
						num_certificato_ente,   
						data_certificato_ente,   
						denominazione_ente,   
						qualita_attrezzatura,   
						cod_centro_costo,   
						flag_primario,   
						flag_blocco,   
						data_blocco,   
						note,   
						data_prima_attivazione,   
						data_installazione,   
						flag_reg_man,   
						flag_manuale,   
						flag_duplicazione,   
						flag_conta_ore,   
						costo_acquisto,   
						freq_guasti,   
						accessibilita,   
						disp_ricamnbi,   
						stato_attuale,   
						id_attrezzatura,   
						ordinamento,   
						num_battute,   
						num_battute_raggiunte,   
						cod_cliente,   
						flag_stato,   
						cod_divisione,   
						latitudine,   
						longitudine 
    		from anag_attrezzature
			where cod_azienda=:s_cs_xx.cod_azienda and
					cod_attrezzatura=:is_cod_attrezzatura;
			
		if sqlca.sqlcode<0 then
			g_mb.error("Errore in inserimento in anagrafica codice "+ls_cod_attrezzatura+" " + sqlca.sqlerrtext)
			
			rollback;
			
			return
		end if

	end if
	
	//inserisci in distinta base attrezzature
	insert into distinta_attrezzature  
		( cod_azienda,   
		  cod_attrezzatura_padre,   
		  prog_attrezzaggio,   
		  cod_attrezzatura_figlio,   
		  num_sequenza,   
		  cod_attrezzatura_alt,   
		  num_battute )  
		select	cod_azienda,   
					cod_attrezzatura_padre,   
					prog_attrezzaggio,   
					:ls_cod_attrezzatura,   
					num_sequenza,   
					cod_attrezzatura_alt,   
					0
		from distinta_attrezzature
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_attrezzatura_padre=:is_cod_attrezzatura_padre and
				num_sequenza=:il_num_sequenza and
				cod_attrezzatura_figlio=:is_cod_attrezzatura and
				prog_attrezzaggio=:il_prog_attrezzaggio;
				
	if sqlca.sqlcode<0 then
		g_mb.error("Errore in inserimento in distinta del codice "+ls_cod_attrezzatura+" " + sqlca.sqlerrtext)
		
		rollback;
		
		return
	end if
	
next

//se arrivi fin qui fai il commit
commit;

//imposto variabile per fare poi la retrieve
s_cs_xx.parametri.parametro_b_2 = true

close(parent)
end event

type dw_lista from datawindow within w_distinta_stampi_nuovi
integer x = 46
integer y = 168
integer width = 2446
integer height = 1396
integer taborder = 30
string title = "none"
string dataobject = "d_distinta_stampi_nuovo"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_imposta from commandbutton within w_distinta_stampi_nuovi
integer x = 2053
integer y = 48
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta"
end type

event clicked;string ls_cod_cat_attrez, ls_max, ls_progressivo, ls_cod_attrezzatura, ls_temp, ls_des_attrezz
long ll_len, ll_index, ll_tot, ll_new


ll_tot = long(em_num.text)

if ll_tot>0 then
else
	g_mb.error("OMNIA","Specificare il numero di codici da creare!")
	em_num.setfocus( )
	return
end if

//-------------------------------------

//leggo la categoria
select cod_cat_attrezzature
into :ls_cod_cat_attrez
from anag_attrezzature
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_attrezzatura=:is_cod_attrezzatura;

//prosegui con il calcolo del codice attrezzature
ll_len = len(ls_cod_cat_attrez)

//select max(substring(cod_attrezzatura, :ll_len + 1, 6 - :ll_len))
//into :ls_max
//from anag_attrezzature
//where cod_azienda = :s_cs_xx.cod_azienda
//	and left(cod_attrezzatura, :ll_len) = :ls_cod_cat_attrez
//	and cod_cat_attrezzature = :ls_cod_cat_attrez;

select substring(cod_attrezzatura, :ll_len + 1, 6 - :ll_len)
into :ls_max
from anag_attrezzature
where cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura=:is_cod_attrezzatura;
	
if sqlca.sqlcode = -1 then
	g_mb.error("OMNIA","Errore durante la lettura info del codice attrezzatura!"+char(13)+ &
					sqlca.sqlerrtext)
	return
end if

dw_lista.reset()

// in ls_progressivo c'è il numerico di partenza
ls_progressivo = ls_max

for ll_index=1 to ll_tot
	ll_new = dw_lista.insertrow(0)
	
	//può servire la prima volta
	if isnull(ls_progressivo) then 
		ls_progressivo = right("000000",6 - ll_len)
	else
		ls_progressivo = f_crea_codice_attr_abor(ls_progressivo)
		
		if ls_progressivo = "" then
			g_mb.error("OMNIA","Errore durante il calcolo automatico del codice attrezzatura (funzione crea codice)!")
			return
		end if
	end if
	
	ls_cod_attrezzatura = ls_cod_cat_attrez + ls_progressivo
	dw_lista.setitem(ll_index,"cod_attrezzatura", ls_cod_attrezzatura)
	
	select cod_attrezzatura, descrizione
	into :ls_temp, :ls_des_attrezz
	from anag_attrezzature
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:ls_cod_attrezzatura;
			
	if sqlca.sqlcode<0 then
		g_mb.error("OMNIA","Errore in controllo esistenza codice!"+char(13)+ &
						sqlca.sqlerrtext)
			return
			
	elseif sqlca.sqlcode=100 or isnull(ls_temp) or ls_temp="" then
		dw_lista.setitem(ll_index,"flag_esiste","N")
		dw_lista.setitem(ll_index,"des_attrezzatura",is_des_attrezzatura)
	else
		dw_lista.setitem(ll_index,"flag_esiste","S")
		dw_lista.setitem(ll_index,"des_attrezzatura",ls_des_attrezz)
		
	end if
next



//-------------------------------------

end event

type st_title from statictext within w_distinta_stampi_nuovi
integer y = 52
integer width = 1527
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Quanti codici devo creare?"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num from editmask within w_distinta_stampi_nuovi
integer x = 1618
integer y = 32
integer width = 402
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####0"
boolean spin = true
string minmax = "1~~1000"
end type

