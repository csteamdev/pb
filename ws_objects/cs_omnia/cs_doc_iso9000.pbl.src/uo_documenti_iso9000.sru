﻿$PBExportHeader$uo_documenti_iso9000.sru
forward
global type uo_documenti_iso9000 from nonvisualobject
end type
end forward

global type uo_documenti_iso9000 from nonvisualobject
end type
global uo_documenti_iso9000 uo_documenti_iso9000

type variables
string is_cod_mansionario, is_autorizzazione, is_approvazione, is_validazione, is_lettura, is_modifica, is_elimina
string is_errore
long   il_num_max_revisioni
end variables

forward prototypes
public function integer uof_autorizza_documento (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo)
public function integer uof_valida_documento (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo)
public function integer uof_privilegi_documento (long fl_anno_registrazione, long fl_num_registrazione)
public function integer uof_controllo_doc_scaduti ()
public function integer uof_ripristina_revisione_prec (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo)
public function integer uof_approva_documento (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo)
public function integer uof_nuova_revisione (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo)
end prototypes

public function integer uof_autorizza_documento (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo);// -----------------------------------------------------------------------------------
//
//			FUNZIONE DI AUTORIZZAZIONE DI UN DOCUMENTO
//
//
//			PARAMETRO						PASS		TIPO			COMMENTO
//			----------------------------------------------	
//			fl_anno_registrazione		val		long			anno_del documento
//       fl_num_registrazione			val		long			numero del documento
//			fl_progressivo					val		long			progressivo del documento
//			
//			RETURN:  0 = successo			-1 = errore (vedi sqlca)
//			
//	-------------------------------------------------------------------------------------
string ls_autorizzato_da, ls_firma_autorizzato_da
datetime ldt_data
long ll_ret
	
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

if is_autorizzazione = "N" then
	//Donato 08/01/2009 prima di dare il msg controlla se c'è l'abilitazione del gruppo, in quanto tale autorizzazione
	//comanda su quella del mansionario
	
	//vecchio codice commentato
	//is_errore = "Questo mansionario non possiede il privilegio di VERIFICA: contattare l'amministratore del sistema"
	//return -1
	
	//NUOVO CODICE
	ll_ret = uof_privilegi_documento(fl_anno_registrazione, fl_num_registrazione)
	if ll_ret < 0 then
		//is_errore è già valorizzato.....verrà dato il messaggio di errore opportuno
		return -1
	end if
	//la funzione uof_privilegi_documento ha riscritto le variabili di istanza delle autorizzazioni varie in base al gruppo
	//ora verifica
	if is_autorizzazione = "N" then
		is_errore = "Questo mansionario non possiede il privilegio di VERIFICA: contattare l'amministratore del sistema"
		return -1
	end if	
	//fine modifica ------------------------------
end if

select autorizzato_da
into   :ls_autorizzato_da
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
       num_registrazione = :fl_num_registrazione and
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore codice " + string(sqlca.sqlcode) + " in ricerca del documento. Operazione annullata. " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_autorizzato_da) then
	is_errore = "Il documento selezionato è già VERIFICATO dal mansionario " + ls_autorizzato_da
	return -1
end if

ldt_data = datetime(today(), 00:00:00)
ls_firma_autorizzato_da = luo_mansionario.uof_return_firma(is_cod_mansionario)

update det_documenti
set    autorizzato_da = :is_cod_mansionario,
       autorizzato_il = :ldt_data,
		 firma_autorizzato_da = :ls_firma_autorizzato_da
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
       num_registrazione = :fl_num_registrazione and
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore codice " + string(sqlca.sqlcode) + " in UPDATE firma VERIFICA del documento. Operazione annullata. " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_valida_documento (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo);// -----------------------------------------------------------------------------------
//
//			FUNZIONE DI VALIDAZIONE DI UN DOCUMENTO
//
//
//			PARAMETRO						PASS		TIPO			COMMENTO
//			----------------------------------------------	
//			fl_anno_registrazione		val		long			anno_del documento
//       fl_num_registrazione			val		long			numero del documento
//			fl_progressivo					val		long			progressivo del documento
//			
//			RETURN:  0 = successo			-1 = errore (vedi sqlca)
//			
//	-------------------------------------------------------------------------------------
string ls_autorizzato_da, ls_approvato_da,ls_validato_da,ls_firma_validato_da
long   ll_documento_precedente, ll_valido_per
datetime ldt_data
long ll_ret

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

if is_validazione = "N" then
	//Donato 08/01/2009 prima di dare il msg controlla se c'è l'abilitazione del gruppo, in quanto tale autorizzazione
	//comanda su quella del mansionario
	
	//vecchio codice commentato
	//is_errore = "Questo mansionario non possiede il privilegio di VALIDAZIONE: contattare l'amministratore del sistema"
	//return -1
	
	//NUOVO CODICE
	ll_ret = uof_privilegi_documento(fl_anno_registrazione, fl_num_registrazione)
	if ll_ret < 0 then
		//is_errore è già valorizzato.....verrà dato il messaggio di errore opportuno
		return -1
	end if
	//la funzione uof_privilegi_documento ha riscritto le variabili di istanza delle autorizzazioni varie in base al gruppo
	//ora verifica
	if is_validazione = "N" then
		is_errore = "Questo mansionario non possiede il privilegio di VALIDAZIONE: contattare l'amministratore del sistema"
		return -1
	end if	
	//fine modifica ------------------------------	
end if

select autorizzato_da, approvato_da, validato_da, valido_per
into   :ls_autorizzato_da, :ls_approvato_da, :ls_validato_da, :ll_valido_per
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
       num_registrazione = :fl_num_registrazione and
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore codice " + string(sqlca.sqlcode) + " in ricerca del documento. Operazione annullata. " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_autorizzato_da) then
	is_errore = "Il documento selezionato non è ancora verificato e pertanto non lo si può VALIDARE"
	return -1
end if
if isnull(ls_approvato_da) then
	is_errore = "Il documento selezionato non è ancora approvato e pertanto non lo si può VALIDARE"
	return -1
end if
if not isnull(ls_validato_da) then
	is_errore = "Il documento selezionato è già stato VALIDATO dal mansionario " + ls_validato_da
	return -1
end if
if ll_valido_per < 1 then
	is_errore = "Non ha senso validate un documento in cui non erano stati impostati giorni di validità"
	return -1
end if

ldt_data = datetime(today(), 00:00:00)
ls_firma_validato_da = luo_mansionario.uof_return_firma(is_cod_mansionario)

update det_documenti
set    validato_da = :is_cod_mansionario,
       validato_il = :ldt_data,
		 firma_validato_da = :ls_firma_validato_da
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
       num_registrazione = :fl_num_registrazione and
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore codice " + string(sqlca.sqlcode) + " in UPDATE firma validazione del documento. Operazione annullata. " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_privilegi_documento (long fl_anno_registrazione, long fl_num_registrazione);// -----------------------------------------------------------------------------------
//
//			FUNZIONE DI CONTROLLO DEI PROVILEGI DEL DOCUMENTO SINGOLO PER IL MANSIONARIO CORRENTE
//
//
//			PARAMETRO						PASS		TIPO			COMMENTO
//			-------------------------------------------------------------------------
//			fl_anno_registrazione		val		long			anno_del documento
//       fl_num_registrazione			val		long			numero del documento
//			
//			RETURN:  0 = successo			-1 = errore (vedi sqlca)
//						1 = nessun privilegio trovato sul documento
//			
//	-------------------------------------------------------------------------------------
string ls_cod_gruppo_privilegio, ls_lettura="N", ls_elimina="N", ls_creazione="N", ls_approvazione="N", ls_validazione="N",&
       ls_verifica="N", ls_flag_lettura, ls_flag_elimina, ls_flag_creazione, ls_flag_approvazione, ls_flag_validazione, &
		 ls_flag_verifica, ls_flag_modifica
long ll_cont = 0

declare cu_privilegi cursor for
select cod_gruppo_privilegio
from   tes_documenti_privilegi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_mansionario = :is_cod_mansionario;

open cu_privilegi;
if sqlca.sqlcode = -1 then
	is_errore = "uof_privilegi_documento: errore in open cursore cu_privilegi. dettaglio=" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while 1=1 
	fetch cu_privilegi into :ls_cod_gruppo_privilegio;
	if sqlca.sqlcode = 100 then		// fine del curosre
		if ll_cont > 0 then
			is_lettura =ls_lettura
			is_modifica = ls_creazione
			is_elimina = ls_elimina
			is_autorizzazione = ls_verifica
			is_approvazione = ls_approvazione
			is_validazione = ls_validazione
			close cu_privilegi;
			commit;
			return 0
		else
			close cu_privilegi;
			commit;
			return 1				// nessun privilegio trovato sul documento
		end if
	end if
	if sqlca.sqlcode = -1 then
		is_errore = "uof_privilegi_documento: errore in fetch cursore cu_privilegi. dettaglio=" + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	ll_cont ++
	
	//Donato 16/01/2009 caricare assieme i privilegi di creazione e modifica:
	//basta che uno dei due sia posto a S per dare autorizzazione alla modifica/creazione
	//questo perchè sia se si vuole creare sia che si vuole modificare il documento il programma
	//vede cosa c'è nella variabile "is_modifica" e non è stata prevista la "is_creazione"
	//NUOVO CODICE
	select flag_lettura, 
			flag_elimina, 
			flag_creazione, 
			flag_approvazione,
			flag_validazione, 
			flag_verifica, 
			flag_modifica
	into :ls_flag_lettura, 
			:ls_flag_elimina, 
			:ls_flag_creazione, 
			:ls_flag_approvazione, 
			:ls_flag_validazione, 
			:ls_flag_verifica, 
			:ls_flag_modifica
	from  tab_gruppi_privilegi
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_gruppo_privilegio = :ls_cod_gruppo_privilegio;
	
	//vecchio codice
	/*
	select flag_lettura,   flag_elimina,     flag_creazione,     flag_approvazione,     flag_validazione,     flag_verifica	
	into :ls_flag_lettura, :ls_flag_elimina, :ls_flag_creazione, :ls_flag_approvazione, :ls_flag_validazione, :ls_flag_verifica
	from  tab_gruppi_privilegi
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_gruppo_privilegio = :ls_cod_gruppo_privilegio;
	*/
	//fine modifica ---------------------------------------------------------------------------------------------------
	
	if sqlca.sqlcode <> 0 then
		is_errore = "Errore imprevisto in ricerca gruppo di privilegi" + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_flag_lettura = "S" then ls_lettura = "S"
	if ls_flag_elimina = "S" then ls_elimina = "S"
	
	//Donato 
	//basta che uno dei due sia posto a S per dare autorizzazione alla modifica/creazione
	//questo perchè sia se si vuole creare sia che si vuole modificare il documento il programma
	//vede cosa c'è nella variabile "is_modifica" e non è stata prevista la "is_creazione"
	//NUOVO CODICE
	if ls_flag_creazione = "S" or ls_flag_modifica="S" then ls_creazione = "S"
	
	//vecchio codice
	//if ls_flag_creazione = "S" then ls_creazione = "S"
	
	//fine modifica -------------------------------------------------------------------------------------------
	if ls_flag_approvazione = "S" then ls_approvazione = "S"
	if ls_flag_validazione = "S" then ls_validazione = "S"
	if ls_flag_verifica = "S" then ls_verifica = "S"
loop
close cu_privilegi;
commit;
return 0

end function

public function integer uof_controllo_doc_scaduti ();// -----------------------------------------------------------------------------------
//
//			FUNZIONE DI CONTROLLO DEI DOCUMENTI LA CUI VALIDITA' E' SCADUTA
//
//
//			PARAMETRO						PASS		TIPO			COMMENTO
//			----------------------------------------------	
//			
//
//			RETURN:  0 = successo			-1 = errore (vedi sqlca)
//			
//	-------------------------------------------------------------------------------------
string ls_sql
long ll_anno_registrazione, ll_num_registrazione,ll_progressivo, ll_valido_per, ll_num_versione, ll_num_revisione
datetime ldt_data_approvazione, ldt_oggi, ldt_data_confronto

//declare cu_tes_documenti cursor for
//select anno_registrazione, num_registrazione
//from   tes_documenti
//where  cod_azienda = :s_cs_xx.cod_azienda and
//       tipo_elemento = 'D';
//		 
//declare cu_det_documenti cursor for
//select progressivo, approvato_il, valido_per, num_versione, num_revisione
//from   det_documenti
//where  cod_azienda = :s_cs_xx.cod_azienda and 
//       anno_registrazione = :ll_anno_registrazione and 
//		 num_registrazione = :ll_num_registrazione and
//       autorizzato_da is not null and approvato_da is not null and  flag_storico = 'N' and
//       validato_da is null and valido_per > 0
//group by progressivo, approvato_il, valido_per, num_versione, num_revisione
//having num_versione = max(num_versione) and num_revisione = max(num_revisione);		 

declare 	cu_tes_documenti cursor for
select 	tes_documenti.anno_registrazione, tes_documenti.num_registrazione, det_documenti.progressivo, det_documenti.approvato_il, det_documenti.valido_per, det_documenti.num_versione, det_documenti.num_revisione
from   	tes_documenti, det_documenti
where  	det_documenti.cod_azienda = tes_documenti.cod_azienda and
       	det_documenti.anno_registrazione = tes_documenti.anno_registrazione and 
		 	det_documenti.num_registrazione = tes_documenti.num_registrazione and
		 	det_documenti.cod_azienda = :s_cs_xx.cod_azienda and 
       	det_documenti.autorizzato_da is not null and det_documenti.approvato_da is not null and  det_documenti.flag_storico = 'N' and
       	det_documenti.validato_da is null and det_documenti.valido_per > 0 and
		 	tes_documenti.tipo_elemento = 'D'
group by tes_documenti.anno_registrazione, tes_documenti.num_registrazione, det_documenti.progressivo, det_documenti.approvato_il, det_documenti.valido_per, det_documenti.num_versione, det_documenti.num_revisione
having 	det_documenti.num_versione = max(det_documenti.num_versione) and det_documenti.num_revisione = max(det_documenti.num_revisione);		 


open cu_tes_documenti;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore in OPEN cursore cu_tes_documenti; dettaglio. " + sqlca.sqlerrtext
	return -1
end if

ldt_oggi = datetime(today(), 00:00:00)

do while true
	
		fetch cu_tes_documenti into :ll_anno_registrazione, :ll_num_registrazione, :ll_progressivo, :ldt_data_approvazione, :ll_valido_per, :ll_num_versione, :ll_num_revisione;

		if sqlca.sqlcode = 100 then
			close cu_tes_documenti;
			exit
		end if

		if sqlca.sqlcode = -1 then
			is_errore = "Errore in FETCH cursore cu_det_documenti; dettaglio. " + sqlca.sqlerrtext
			exit
			close cu_tes_documenti;
		end if

		ldt_data_confronto = datetime(relativedate(date(ldt_data_approvazione), ll_valido_per), 00:00:00)

		if ldt_oggi > ldt_data_confronto  then
			
			if uof_ripristina_revisione_prec(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = -1 then
				close cu_tes_documenti;
				return -1
			end if
			
			close cu_tes_documenti;
			
			exit
			
		end if

loop

close cu_tes_documenti;

return 0
end function

public function integer uof_ripristina_revisione_prec (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo);// -----------------------------------------------------------------------------------
//
//			FUNZIONE DI CREAZIONE RIPRISTINO REVISIONE PRECEDENTE CAUSA VALIDITA' SCADUTA.
//
//
//			PARAMETRO						PASS		TIPO			COMMENTO
//			----------------------------------------------	
//			fl_anno_registrazione		val		long			anno_del documento
//       fl_num_registrazione			val		long			numero del documento
//			fl_progressivo					val		long			progressivo del documento
//			
//			RETURN:  0 = successo
//                -1 = errore (vedi sqlca)
//                -2 = revisione precedente inesistente o non trovata
//			
//	-------------------------------------------------------------------------------------
string ls_nome_documento, ls_db
long ll_prog_revisione_precedente, ll_num_versione, ll_num_revisione, ll_max_progressivo
datetime ldt_oggi
blob lb_blob
integer li_risposta
transaction sqlcb

ll_prog_revisione_precedente = fl_progressivo -1
if ll_prog_revisione_precedente < 1 then 	return -2

// controllo se esiste la revisione precedente
select nome_documento
into   :ls_nome_documento
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione = :fl_anno_registrazione AND  
		 num_registrazione = :fl_num_registrazione AND  
		 progressivo = :ll_prog_revisione_precedente;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore in ricerca della revisione precedente del documento " + ls_nome_documento + ". Dettaglio errore " + sqlca.sqlerrtext
	return -1
end if

// calcolo nuovo indice di revisione
select nome_documento, num_versione, num_revisione
into   :ls_nome_documento,:ll_num_versione, :ll_num_revisione
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione = :fl_anno_registrazione AND  
		 num_registrazione = :fl_num_registrazione AND  
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore in ricerca della versione/revisione del documento " + ls_nome_documento + ". Dettaglio errore " + sqlca.sqlerrtext
	return -1
end if

if ll_num_revisione < il_num_max_revisioni then
	ll_num_revisione ++
else
	ll_num_revisione = 0
	ll_num_versione ++
end if

select max(progressivo)
into   :ll_max_progressivo
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione = :fl_anno_registrazione AND  
		 num_registrazione = :fl_num_registrazione;
if ll_max_progressivo = 0 or isnull(ll_max_progressivo) then
	is_errore = "Errore nel calcolo del progressivo di registrazione del documento"
	return -1
end if
ll_max_progressivo ++

// procedo con creazione nuova revisione
INSERT INTO det_documenti  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  progressivo,   
		  resp_conservazione_doc,   
		  num_versione,   
		  num_revisione,   
		  emesso_da,   
		  emesso_il,   
		  autorizzato_da,   
		  autorizzato_il,   
		  approvato_da,   
		  approvato_il,   
		  validato_da,   
		  valido_per,   
		  validato_il,   
		  modifiche,   
		  livello_documento,   
		  flag_uso,   
		  flag_recuperato,   
		  nome_documento,   
		  des_documento,   
		  prog_ordinamento_livello,   
		  periodo_conserv_unita_tempo,   
		  periodo_conserv,   
		  flag_doc_registraz_qualita,   
		  flag_catalogazione,   
		  data_verifica,   
		  cod_area_aziendale,   
		  flag_storico )  
SELECT cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		:ll_max_progressivo,   
		resp_conservazione_doc,   
		:ll_num_versione,   
		:ll_num_revisione,   
		emesso_da,   
		emesso_il,   
		autorizzato_da,   
		autorizzato_il,   
		approvato_da,   
		approvato_il,   
		null,   
		0,   
		null,   
		modifiche,   
		livello_documento,   
		flag_uso,   
		'S',   
		nome_documento,   
		des_documento,   
		prog_ordinamento_livello,   
		periodo_conserv_unita_tempo,   
		periodo_conserv,   
		flag_doc_registraz_qualita,   
		flag_catalogazione,   
		null,   
		cod_area_aziendale,   
		'N'  
 FROM det_documenti  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :fl_anno_registrazione AND  
		num_registrazione = :fl_num_registrazione AND  
		progressivo = :ll_prog_revisione_precedente ;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore " + string(sqlca.sqlcode) + " durante INSERT nuova revisione recuperata. Dettaglio " + sqlca.sqlerrtext
	return -1
end if

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob_documento
	into       :lb_blob
	from       det_documenti
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :ll_prog_revisione_precedente 
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlcb.sqlcode) + " durante SELECTBLOB del documento. Dettaglio " + sqlcb.sqlerrtext
		destroy sqlcb;
		return -1
	end if
			  
	updateblob det_documenti
	set        blob_documento = :lb_blob
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :ll_max_progressivo
	using		  sqlcb;
	
	if sqlcb.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlcb.sqlcode) + " durante UPDATEBLOB del documento nella nuova revisione. Dettaglio " + sqlcb.sqlerrtext
		destroy sqlcb;
		return -1
	end if	
	
	destroy sqlcb;
	
else
	
	selectblob blob_documento
	into       :lb_blob
	from       det_documenti
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :ll_prog_revisione_precedente ;
	if sqlca.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlca.sqlcode) + " durante SELECTBLOB del documento. Dettaglio " + sqlca.sqlerrtext
		return -1
	end if
			  
	updateblob det_documenti
	set        blob_documento = :lb_blob
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :ll_max_progressivo;
	
	if sqlca.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlca.sqlcode) + " durante UPDATEBLOB del documento nella nuova revisione. Dettaglio " + sqlca.sqlerrtext
		return -1
	end if

end if
	
// fine modifiche	
	
update det_documenti
set    flag_storico = 'S', valido_per = 0
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione = :fl_anno_registrazione AND  
		 num_registrazione = :fl_num_registrazione AND  
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore " + string(sqlca.sqlcode) + " durante UPDATE del flag_storico = S del documento di cui è scaduta la validità. Dettaglio " + sqlca.sqlerrtext
	return -1
end if
		 
return 0
end function

public function integer uof_approva_documento (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo);// -----------------------------------------------------------------------------------
//
//			FUNZIONE DI APPROVAZIONE DI UN DOCUMENTO
//
//
//			PARAMETRO						PASS		TIPO			COMMENTO
//			----------------------------------------------	
//			fl_anno_registrazione		val		long			anno_del documento
//       fl_num_registrazione			val		long			numero del documento
//			fl_progressivo					val		long			progressivo del documento
//			
//			RETURN:  0 = successo			-1 = errore (vedi sqlca)
//			
//	-------------------------------------------------------------------------------------
string ls_autorizzato_da, ls_approvato_da, ls_firma_approvato_da
long   ll_documento_precedente
datetime ldt_data
long ll_ret

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

if is_approvazione = "N" then
	//Donato 08/01/2009 prima di dare il msg controlla se c'è l'abilitazione del gruppo, in quanto tale autorizzazione
	//comanda su quella del mansionario
	
	//vecchio codice commentato
	//is_errore = "Questo mansionario non possiede il privilegio di APPROVAZIONE: contattare l'amministratore del sistema"
	//return -1
	
	//NUOVO CODICE
	ll_ret = uof_privilegi_documento(fl_anno_registrazione, fl_num_registrazione)
	if ll_ret < 0 then
		//is_errore è già valorizzato.....verrà dato il messaggio di errore opportuno
		return -1
	end if
	//la funzione uof_privilegi_documento ha riscritto le variabili di istanza delle autorizzazioni varie in base al gruppo
	//ora verifica
	if is_approvazione = "N" then
		is_errore = "Questo mansionario non possiede il privilegio di APPROVAZIONE: contattare l'amministratore del sistema"
		return -1
	end if	
	//fine modifica ------------------------------
end if

select autorizzato_da, approvato_da
into   :ls_autorizzato_da, :ls_approvato_da
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
       num_registrazione = :fl_num_registrazione and
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore codice " + string(sqlca.sqlcode) + " in ricerca del documento. Operazione annullata. " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_autorizzato_da) then
	is_errore = "Il documento selezionato non è ancora verificato e pertanto non lo si può APPROVARE"
	return -1
end if
if not isnull(ls_approvato_da) then
	is_errore = "Il documento selezionato è già stato APPROVATO dal mansionario " + ls_approvato_da
	return -1
end if

ldt_data = datetime(today(), 00:00:00)
ls_firma_approvato_da = luo_mansionario.uof_return_firma (is_cod_mansionario)

update det_documenti
set    approvato_da = :is_cod_mansionario,
       approvato_il = :ldt_data,
		 firma_approvato_da = :ls_firma_approvato_da
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
       num_registrazione = :fl_num_registrazione and
		 progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore codice " + string(sqlca.sqlcode) + " in UPDATE firma autorizzazione del documento. Operazione annullata. " + sqlca.sqlerrtext
	return -1
end if

ll_documento_precedente = fl_progressivo - 1
if ll_documento_precedente > 0 then			// esiste un documento precedente da mettere come storico
	update det_documenti
	set    flag_storico = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 progressivo = :ll_documento_precedente;
	if sqlca.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlca.sqlcode) + " in UPDATE flag_storico. Operazione di approvazione annullata. " + sqlca.sqlerrtext
		return -1
	end if
end if	

return 0
end function

public function integer uof_nuova_revisione (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo);// -----------------------------------------------------------------------------------
//
//			FUNZIONE DI CREAZIONE NUOVA REVISIONE DI UN DOCUMENTO
//
//
//			PARAMETRO						PASS		TIPO			COMMENTO
//			----------------------------------------------	
//			fl_anno_registrazione		val		long			anno_del documento
//       fl_num_registrazione			val		long			numero del documento
//			fl_progressivo					val		long			progressivo del documento
//			
//			RETURN:  0 = successo			-1 = errore (vedi sqlca)
//			
//	-------------------------------------------------------------------------------------
string ls_autorizzato_da, ls_approvato_da, ls_db
long ll_num_versione, ll_num_revisione, ll_prog_appo, ll_prog_mimetype
datetime ldt_oggi
blob lb_blob
transaction sqlcb
integer li_risposta
long ll_ret

if is_modifica = "N" then	
	//Donato 08/01/2009 prima di dare il msg controlla se c'è l'abilitazione del gruppo, in quanto tale autorizzazione
	//comanda su quella del mansionario
	
	//vecchio codice commentato
	//is_errore = "Questo mansionario non possiede i privilegi necessari alla creazione di nuove revisioni: contattare l'amministratore del sistema"
	//return -1
	
	//NUOVO CODICE
	ll_ret = uof_privilegi_documento(fl_anno_registrazione, fl_num_registrazione)
	if ll_ret < 0 then
		//is_errore è già valorizzato.....verrà dato il messaggio di errore opportuno
		return -1
	end if
	//la funzione uof_privilegi_documento ha riscritto le variabili di istanza delle autorizzazioni varie in base al gruppo
	//ora verifica
	if is_modifica = "N" then
		is_errore = "Questo mansionario non  possiede i privilegi necessari alla creazione di nuove revisioni: contattare l'amministratore del sistema"
		return -1
	end if	
	//fine modifica ------------------------------
end if

ldt_oggi = datetime(today(), 00:00:00)

// imposto nuovo indice di versione / revisione
select num_versione, 
       num_revisione, 
		 autorizzato_da, 
		 approvato_da, 
		 prog_mimetype
into   :ll_num_versione, 
       :ll_num_revisione, 
		 :ls_autorizzato_da, 
		 :ls_approvato_da, 
		 :ll_prog_mimetype
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione = :fl_anno_registrazione AND  
		 num_registrazione = :fl_num_registrazione AND  
		 progressivo = :fl_progressivo;
		 
if sqlca.sqlcode <> 0 then	
	is_errore = "Errore in ricerca del documento di cui si vuol creare una nuova revisione. Dettaglio errore " + sqlca.sqlerrtext	
	return -1
end if

if isnull(ls_autorizzato_da) then
	is_errore = "Il documento selezionato non è ancora verificato e pertanto non si può emettere una nuova revisione."
	return -1
end if
if isnull(ls_approvato_da) then
	is_errore = "Il documento selezionato non è ancora approvato e pertanto non si può emettere una nuova revisione."
	return -1
end if

if ll_num_revisione < il_num_max_revisioni then
	ll_num_revisione ++
else
	ll_num_revisione = 0
	ll_num_versione ++
end if

// procedo con creazione nuova revisione
INSERT INTO det_documenti  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  progressivo,   
		  resp_conservazione_doc,   
		  num_versione,   
		  num_revisione,   
		  emesso_da,   
		  emesso_il,   
		  autorizzato_da,   
		  autorizzato_il,   
		  approvato_da,   
		  approvato_il,   
		  validato_da,   
		  valido_per,   
		  validato_il,   
		  modifiche,   
		  livello_documento,   
		  flag_uso,   
		  flag_recuperato,   
		  nome_documento,   
		  des_documento,   
		  prog_ordinamento_livello,   
		  periodo_conserv_unita_tempo,   
		  periodo_conserv,   
		  flag_doc_registraz_qualita,   
		  flag_catalogazione,   
		  data_verifica,   
		  cod_area_aziendale,   
		  flag_storico,
		  prog_mimetype,
		  prog_lista_distribuzione)  
SELECT cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		progressivo + 1,   
		resp_conservazione_doc,   
		:ll_num_versione,   
		:ll_num_revisione,   
		:is_cod_mansionario,   
		:ldt_oggi,   
		null,   
		null,   
		null,   
		null,   
		null,   
		0,   
		null,   
		modifiche,   
		livello_documento,   
		flag_uso,   
		'N',   
		nome_documento,   
		des_documento,   
		prog_ordinamento_livello,   
		periodo_conserv_unita_tempo,   
		periodo_conserv,   
		flag_doc_registraz_qualita,   
		flag_catalogazione,   
		null,   
		cod_area_aziendale,   
		'N',
		:ll_prog_mimetype,
		prog_lista_distribuzione
 FROM det_documenti  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :fl_anno_registrazione AND  
		num_registrazione = :fl_num_registrazione AND  
		progressivo = :fl_progressivo ;
if sqlca.sqlcode <> 0 then
	is_errore = "Errore " + string(sqlca.sqlcode) + " durante INSERT nuova revisione. Dettaglio " + sqlca.sqlerrtext
	return -1
end if

// 11-07-2002 modifica Michela : devo controllare l'enginetype e gestire MSSQL

ls_db = f_db()

if ls_db = "MSSQL" then
	
	// aggiunta mia
	commit;	
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob_documento
	into       :lb_blob
	from       det_documenti
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :fl_progressivo
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlcb.sqlcode) + " durante SELECTBLOB del documento. Dettaglio " + sqlcb.sqlerrtext
		destroy sqlcb;
		return -1
	end if
	
	destroy sqlcb;

else
	
	selectblob blob_documento
	into       :lb_blob
	from       det_documenti
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :fl_progressivo; 
	
	if sqlca.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlca.sqlcode) + " durante SELECTBLOB del documento. Dettaglio " + sqlca.sqlerrtext
		return -1
	end if

end if	


if ls_db = "MSSQL" then

	li_risposta = f_crea_sqlcb(sqlcb)
		
	updateblob det_documenti
	set        blob_documento = :lb_blob
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :fl_progressivo + 1 
	using		  sqlcb;
	
	if sqlcb.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlcb.sqlcode) + " durante UPDATEBLOB del documento nella nuova revisione. Dettaglio " + sqlcb.sqlerrtext
		destroy sqlcb;
		return -1
	end if
	
	destroy sqlcb;
	
else
	
	updateblob det_documenti
	set        blob_documento = :lb_blob
	where      cod_azienda = :s_cs_xx.cod_azienda AND  
			     anno_registrazione = :fl_anno_registrazione AND  
			     num_registrazione = :fl_num_registrazione AND  
			     progressivo = :fl_progressivo + 1 ;
	
	if sqlca.sqlcode <> 0 then
		is_errore = "Errore " + string(sqlca.sqlcode) + " durante UPDATEBLOB del documento nella nuova revisione. Dettaglio " + sqlca.sqlerrtext
		return -1
	end if
	
end if

// fine modifica

return 0


end function

on uo_documenti_iso9000.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_documenti_iso9000.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;//
// carico i provilegi utente in variabili di istanza dell'oggetto

//Giulio: 09/11/2011 nuova gestione privilegi mansionario
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

//select cod_resp_divisione, 
//       flag_approvazione, 
//		 flag_autorizzazione, 
//		 flag_validazione, 
//		 flag_lettura, 
//		 flag_modifica, 
//		 flag_elimina
//into   :is_cod_mansionario,
//       :is_approvazione,
//		 :is_autorizzazione,
//		 :is_validazione,
//		 :is_lettura,
//		 :is_modifica,
//		 :is_elimina
//from   mansionari
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_utente=:s_cs_xx.cod_utente;

select cod_resp_divisione
into   :is_cod_mansionario
from   mansionari
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_utente=:s_cs_xx.cod_utente;

is_approvazione = 'N'
is_autorizzazione = 'N'
is_validazione = 'N'
is_lettura = 'N'
is_modifica = 'N'
is_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then is_approvazione = 'S' 
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then is_autorizzazione = 'S' 
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then is_validazione = 'S' 
if luo_mansionario.uof_get_privilege(luo_mansionario.lettura) then is_lettura = 'S' 
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then is_modifica = 'S' 
if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then is_elimina = 'S' 


if sqlca.sqlcode < 0 then
	is_errore = "Errore in ricerca mansionario dell'utente "+s_cs_xx.cod_utente+"; impossibile proseguire. Dettaglio errore: " + sqlca.sqlerrtext
	return -1
end if
if sqlca.sqlcode = 100 or isnull(is_cod_mansionario) then
	is_errore = "Nessun mansionario trovato per l'utente "+s_cs_xx.cod_utente+"; impossibile proseguire"
	return -1
end if

select numero  
into   :il_num_max_revisioni
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_parametro = 'NED' ;
if sqlca.sqlcode = 100 then
	is_errore = "Parametro NED inesistente in parametri aziendali; impossibile proseguire! Contattare il responsabile del sistema"
	return -1
end if

end event

