﻿$PBExportHeader$uo_editor_documenti.sru
forward
global type uo_editor_documenti from uo_editor
end type
end forward

global type uo_editor_documenti from uo_editor
end type
global uo_editor_documenti uo_editor_documenti

type variables
private:
	int ii_prog_processo
	uo_editor_documenti_nodo iuo_collegamenti[]
	boolean ib_click_pressed = false
	boolean ib_drag = false
	
	
public:
	string is_colore_mappa = "16758897"
	string is_colore_mappa_selezione = "16758897"
	string is_colore_documento = "8421376"
	string is_colore_documento_selezione = "8421376"
end variables

forward prototypes
public function boolean uof_carica_mappa (integer ai_prog_processo)
private function boolean uof_carica_collegamenti (integer ai_prog_processo)
protected subroutine uof_reset ()
public function boolean uof_mappa_rinomina (integer ai_prog_processo, string as_new_name)
public function boolean uof_mappa_elimina (integer ai_prog_processo)
public function boolean uof_mappa_area (integer ai_prog_processo, string as_cod_area)
public function boolean uof_mappa_immagine (integer ai_prog_processo, string as_image_path, string as_image_name, long al_width, long al_height)
public function boolean uof_mappa_aggiungi (ref integer ai_prog_processo, integer ai_prog_processo_modello, string as_des_processo, string as_img_path, string as_img_name, long al_width, long al_height)
protected function string uof_get_labels ()
public function boolean uof_elimina_collegamento ()
public function boolean uof_aggiungi_collegamento (integer ai_prog_processo, long ai_prog_collegamento, integer ai_x, integer ai_y, integer ai_width, integer ai_height, long al_prog_elemento, string as_tipo_elemento, string as_valore)
end prototypes

public function boolean uof_carica_mappa (integer ai_prog_processo);string ls_des_processo, ls_path_blob
int		li_altezza, li_larghezza
blob 	lblb_mappa

uof_reset()

// -- Blob
selectblob blob_processo
into :lblb_mappa
from processi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_processo = :ai_prog_processo;
	
if sqlca.sqlcode <> 0 or isnull(lblb_mappa) then
	msg("Nessuna immagine è associata a questo mappa.~r~n" + sqlca.sqlerrtext)
	return false
end if

// -- Valori
select des_processo, path_blob, altezza, larghezza
into :ls_des_processo, :ls_path_blob, :li_altezza, :li_larghezza
from processi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_processo = :ai_prog_processo;
	
if sqlca.sqlcode <> 0 then
	msg("Impossibile recuperare informazioni mappa.~r~n" + sqlca.sqlerrtext)
	return false
end if

// -- Dw
is_image_path = is_user_temporaly + ls_path_blob
if f_blob_to_file(is_image_path, lblb_mappa) <> 0 then
	msg("Impossibile salvare l'immagine nella cartella temporanea, probabilmente non esiste la cartella temporanea.~r~n" + is_image_path)
	return false
end if

// -- Dimensioni Immagine
il_image_width = PixelsToUnits(li_larghezza, XPixelsToUnits!)
il_image_height = PixelsToUnits(li_altezza, YPixelsToUnits!)
il_image_pixel_width = li_larghezza
il_image_pixel_height = li_altezza
this.uof_resize()
// ----

uof_carica_collegamenti(ai_prog_processo)

uof_fix_tilde(is_image_path)
uof_draw()
ii_prog_processo = ai_prog_processo

return true
end function

private function boolean uof_carica_collegamenti (integer ai_prog_processo);string ls_sql, ls_tipo, ls_valore
int		li_prog_collegamento
long	ll_rows, ll_row, x1, x2, y1, y2, ll_prog_elemento
datastore lds_store

ls_sql = "SELECT prog_collegamento, x1, y1, x2, y2, prog_elemento, tipo_elemento, valore "
ls_sql += "FROM tab_collegamenti_processi WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND  "
ls_sql += "prog_processo=" + string(ai_prog_processo)

if not f_crea_datastore(lds_store, ls_sql) then
	msg("Impossibile creare datastore per controllare i collegamenti associati alla mappa")
	return false
end if

ll_rows = lds_store.retrieve()
if ll_rows > 0 then
	for ll_row = 1 to ll_rows
		
		li_prog_collegamento = lds_store.getitemnumber(ll_row, "prog_collegamento")
		ll_prog_elemento = lds_store.getitemnumber(ll_row, "prog_elemento")
		ls_tipo = lds_store.getitemstring(ll_row, "tipo_elemento")
		ls_valore = lds_store.getitemstring(ll_row, "valore")
		x1 = PixelsToUnits(lds_store.getitemnumber(ll_row, "x1") *  il_image_pixel_width, XPixelsToUnits!)
		y1 = PixelsToUnits(lds_store.getitemnumber(ll_row, "y1") *  il_image_pixel_height, YPixelsToUnits!)
	
		x2 = PixelsToUnits(lds_store.getitemnumber(ll_row, "x2") *  il_image_pixel_width, XPixelsToUnits!)
		y2 = PixelsToUnits(lds_store.getitemnumber(ll_row, "y2") *  il_image_pixel_height, YPixelsToUnits!)
		
		uof_aggiungi_collegamento(ai_prog_processo, li_prog_collegamento, x1,y1, x2 - x1, y2 - y1, ll_prog_elemento, ls_tipo, ls_valore)
		
next
end if
end function

protected subroutine uof_reset ();ii_prog_processo = 0


// -- Reset array collegamenti
uo_editor_documenti_nodo luo_empty[]
iuo_collegamenti = luo_empty

end subroutine

public function boolean uof_mappa_rinomina (integer ai_prog_processo, string as_new_name);update
	processi
set
	des_processo = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_processo = :ai_prog_processo;
	
if sqlca.sqlcode = 0 then
	commit;
	return true
elseif sqlca.sqlcode = 100 then
	rollback;
	msg("Impossibile rinominare mappa con codice " + string(ai_prog_processo))
	return false
else
	return false
end if
end function

public function boolean uof_mappa_elimina (integer ai_prog_processo);string ls_sql
long ll_nodes, ll_node
datastore lds_store

ls_sql = "SELECT prog_processo FROM processi WHERE cod_azienda='"+ s_cs_xx.cod_azienda+"' AND "
ls_sql += "prog_processo_modello = " + string(ai_prog_processo) + " AND prog_processo <> " + string(ai_prog_processo)

if not f_crea_datastore(lds_store, ls_sql) then
	msg("Impossibile creare datastore.")
	return false
end if

ll_nodes = lds_store.retrieve()
if ll_nodes > 0 then
	for ll_node = 1 to ll_nodes
		if uof_mappa_elimina(lds_store.getitemnumber(ll_node, 1)) = false then
			return false
		end if
	next
end if

// -- Elimino collegamenti alla mappa
delete from tab_collegamenti_processi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_processo = :ai_prog_processo;
	
if sqlca.sqlcode < 0 then
	msg("Impossibile eliminare collegamenti associati alla mappa.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
end if
// ----

// -- Elimino mappa
delete from processi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_processo = :ai_prog_processo;
	
if sqlca.sqlcode = 0 then
	commit;
	return true
else
	msg("Impossibile eleminare mappa.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
end if
// ----
end function

public function boolean uof_mappa_area (integer ai_prog_processo, string as_cod_area);update processi
set
	cod_area_aziendale = :as_cod_area
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_processo = :ai_prog_processo;
	
if sqlca.sqlcode <> 0 then
	msg("Impossibile assegnare area aziendale alla mappa.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
else
	commit;
	return true
end if
end function

public function boolean uof_mappa_immagine (integer ai_prog_processo, string as_image_path, string as_image_name, long al_width, long al_height);int li_pixel_width, li_pixel_height
blob lblb_image

// -- aggiusto dimensione in pixel
li_pixel_width = UnitsToPixels(al_width, XUnitsToPixels!)
li_pixel_height = UnitsToPixels(al_height, YUnitsToPixels!)
// ----

if f_file_to_blob(as_image_path, lblb_image) = 0 then
	
	updateblob processi
	set blob_processo = :lblb_image
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		prog_processo = :ai_prog_processo;
		
	if sqlca.sqlcode = 0 then
		
		update processi
		set
			larghezza = :li_pixel_width,
			altezza = :li_pixel_height,
			path_blob = :as_image_name
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			prog_processo = :ai_prog_processo;
			
		if sqlca.sqlcode = 0 then
			is_image_path = as_image_path
			il_image_width = al_width
			il_image_height = al_height
			il_image_pixel_width = li_pixel_width
			il_image_pixel_height = li_pixel_height
			commit;
			this.uof_resize()
			return true
		else
			msg("Impossibile salvare dimensioni immagine.~r~n" + sqlca.sqlerrtext)
			rollback;
			return false
		end if
	else
		msg("Impossibile salvare l'immagine all'intero del database.~r~n" +  sqlca.sqlerrtext)
		rollback;
		return false
	end if

else
	msg("Impossibile convertire immagine per il salvataggio.")
	return false
end if
end function

public function boolean uof_mappa_aggiungi (ref integer ai_prog_processo, integer ai_prog_processo_modello, string as_des_processo, string as_img_path, string as_img_name, long al_width, long al_height);int li_prog_processo, li_pixel_width, li_pixel_height, li_n_nodi
datetime today
blob lblb_image

today = datetime(today(), now())

// -- calcolo progressivo
select max(prog_processo)
into :li_prog_processo
from processi
where	cod_azienda = :s_cs_xx.cod_azienda;
	
if sqlca.sqlcode < 0 then
	msg("Impossibile calcolare progressivo mappa.~r~n" + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(li_prog_processo) then
	li_prog_processo = 0
end if

li_prog_processo++
// ----

// -- aggiusto dimensione in pixel
li_pixel_width = UnitsToPixels(al_width, XUnitsToPixels!)
li_pixel_height = UnitsToPixels(al_height, YUnitsToPixels!)
// ----

if ai_prog_processo_modello < 1 then
	// è un padre
	ai_prog_processo_modello = li_prog_processo
end if

insert into processi (
	cod_azienda,
	prog_processo,
	prog_processo_modello,
	des_processo,
	path_blob,
	altezza,
	larghezza)
values (
	:s_cs_xx.cod_azienda,
	:li_prog_processo,
	:ai_prog_processo_modello,
	:as_des_processo,
	:as_img_name,
	:li_pixel_height,
	:li_pixel_width);
	
if sqlca.sqlcode < 0 then
	msg("Errore durante il salvataggio della mappa.~r~n" + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 0 then
	if f_file_to_blob(as_img_path, lblb_image) = 0 then
		
		updateblob processi
		set blob_processo = :lblb_image
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			prog_processo = :li_prog_processo;
			
		if sqlca.sqlcode <> 0 then
			rollback;
			msg("Errore durante il salvataggio immagine mappa.~r~n" + sqlca.sqlerrtext)
			return false
		end if
		
		commit;
		ai_prog_processo = li_prog_processo
		return true
	else
		rollback;
		msg("Errore, non è stato possibile caricare l'immagine della mappa")
		return false
	end if
end if
	
end function

protected function string uof_get_labels ();string ls_modify, ls_mode, ls_text, ls_color
int		li_i

if ib_editor then
	ls_mode = "1"
else
	ls_mode = "0"
end if

for li_i = 1 to upperbound(this.iuo_collegamenti)
	if isvalid(this.iuo_collegamenti[li_i]) then
		
		// -- Text
		if this.iuo_collegamenti[li_i].tipo_elemento = "M" then
			ls_text = "Mappa: "
			ls_color = is_colore_mappa
		else
			ls_text = "Documento: "
			ls_color = is_colore_documento
		end if
		ls_text += this.iuo_collegamenti[li_i].valore
		// ----

		this.iuo_collegamenti[li_i].text = ls_text
		//ls_text = ""
		
		ls_modify += "text(band=detail alignment=~"0~" border=~"0~" html.valueishtml=~"0~" visible=~"1~" font.face=~"Tahoma~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"2~" background.gradient.color=~"8421504~" background.gradient.transparency=~"0~" background.gradient.angle=~"0~" background.brushmode=~"0~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" background.gradient.repetition.length=~"100~" background.gradient.focus=~"0~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" tooltip.backcolor=~"134217752~" tooltip.delay.visible=~"32000~" tooltip.enabled=~"1~" tooltip.hasclosebutton=~"0~" tooltip.icon=~"0~" tooltip.isbubble=~"0~" tooltip.maxwidth=~"0~" tooltip.textcolor=~"134217751~" tooltip.transparency=~"0~" transparency=~"0~" "
		ls_modify += "text='' "
		ls_modify += "color=~"16777215~" "
		ls_modify += "x=~"" + string(this.iuo_collegamenti[li_i].x) + "~" "
		ls_modify += "y=~"" + string(this.iuo_collegamenti[li_i].y)+ "~" "
		ls_modify += "height=~"" + string(this.iuo_collegamenti[li_i].height) + "~" "
		ls_modify += "width=~"" + string(this.iuo_collegamenti[li_i].width) + "~" "
		ls_modify += "name=t_" + string(li_i) +" "
		ls_modify += "font.height=~"-10~" "
		ls_modify += "background.transparency=~"50~" "
		ls_modify += "background.color=~"" + ls_color + "~" "
		ls_modify += "resizeable=" + ls_mode + " moveable=" + ls_mode + " "
		ls_modify += "pointer=~"Size!~" "
		ls_modify += "tooltip.tip='" + ls_text + "'  tooltip.delay.initial='750' "
		ls_modify += " ) "
		
	end if
next

return ls_modify
end function

public function boolean uof_elimina_collegamento ();if il_t_active > 0 then
	if this.iuo_collegamenti[il_t_active].remove() then
		destroy(this.iuo_collegamenti[il_t_active])
	
		is_t_active = ""
		il_t_active = -1
		
		return true
	else
		return false
	end if
end if
return true
end function

public function boolean uof_aggiungi_collegamento (integer ai_prog_processo, long ai_prog_collegamento, integer ai_x, integer ai_y, integer ai_width, integer ai_height, long al_prog_elemento, string as_tipo_elemento, string as_valore);string ls_error
int li_index

li_index = upperbound(iuo_collegamenti) + 1

this.iuo_collegamenti[li_index] = create uo_editor_documenti_nodo
this.iuo_collegamenti[li_index].prog_processo = ai_prog_processo
this.iuo_collegamenti[li_index].prog_collegamento = ai_prog_collegamento
this.iuo_collegamenti[li_index].x = ai_x
this.iuo_collegamenti[li_index].y = ai_y 
this.iuo_collegamenti[li_index].width = ai_width
this.iuo_collegamenti[li_index].height = ai_height
this.iuo_collegamenti[li_index].prog_elemento = al_prog_elemento
this.iuo_collegamenti[li_index].tipo_elemento = as_tipo_elemento
this.iuo_collegamenti[li_index].valore = as_valore

if ai_prog_collegamento < 1 then
	// Salvo
	ls_error = this.iuo_collegamenti[li_index].Save(il_image_pixel_width, il_image_pixel_height)
	if ls_error <> "" then
		msg(ls_error)
		return false
	end if
end if

return true
end function

on uo_editor_documenti.create
call super::create
end on

on uo_editor_documenti.destroy
call super::destroy
end on

event constructor;call super::constructor;this.uof_set_directory("editor_documenti")
end event

event uoe_dragend;call super::uoe_dragend;if left(as_name, 2) <> "t_" then
	return	
end if

string ls_error
int li_index
boolean ib_force_redraw = false
li_index = integer(mid(as_name, 3))

// controllo dimensioni minime
if ai_x < 0 then 
	ai_x = 0
	ib_force_redraw = true
end if
if ai_y < 0 then 
	ai_y = 0
	ib_force_redraw = true
end if
if ai_width < 10 then 
	ai_width = 30
	ib_force_redraw = true
end if
if ai_height < 10 then 
	ai_height = 30
	ib_force_redraw = true
end if
if ai_x + ai_width > il_image_width then 
	ai_x = il_image_width - ai_width
	ib_force_redraw = true
end if
if ai_y + ai_height > il_image_height then 
	ai_y = il_image_height - ai_height
	ib_force_redraw = true
end if
// ----

if isvalid(this.iuo_collegamenti[li_index]) then
	this.iuo_collegamenti[li_index].x = ai_x
	this.iuo_collegamenti[li_index].y = ai_y
	this.iuo_collegamenti[li_index].width = ai_width
	this.iuo_collegamenti[li_index].height = ai_height
	
	ls_error = this.iuo_collegamenti[li_index].save(il_image_pixel_width, il_image_pixel_height)
	if ls_error <> "" then
		msg(ls_error)
	elseif ib_force_redraw then
		this.uof_draw()
	end if
end if
end event

event uoe_selection_changed;call super::uoe_selection_changed;modify("t_" + string(ai_index) + ".text='" + iuo_collegamenti[ai_index].text + "'")
end event

event uoe_selection_changing;call super::uoe_selection_changing;modify("t_" + string(ai_old_id) + ".text=''")
return true
end event

