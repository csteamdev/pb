﻿$PBExportHeader$uo_editor_documenti_nodo.sru
forward
global type uo_editor_documenti_nodo from nonvisualobject
end type
end forward

global type uo_editor_documenti_nodo from nonvisualobject
end type
global uo_editor_documenti_nodo uo_editor_documenti_nodo

type variables
public:
	int prog_processo
	int prog_collegamento
	
	int x
	int y
	int width
	int height
	
	long prog_elemento
	string tipo_elemento
	string valore	
	
	boolean modify
	
	string text
	
private:
	dec{4} x1, x2
	dec{4} y1, y2
	int ii_pixel_width
	int ii_pixel_height
end variables

forward prototypes
public subroutine dimension (integer ai_pixel_width, integer ai_pixel_height)
public function boolean remove ()
public function string save (integer ai_pixel_width, integer ai_pixel_height)
end prototypes

public subroutine dimension (integer ai_pixel_width, integer ai_pixel_height);ii_pixel_width = ai_pixel_width
ii_pixel_height = ai_pixel_height
end subroutine

public function boolean remove ();if this.prog_collegamento > 0 then
	
	delete from tab_collegamenti_processi
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		prog_processo = :this.prog_processo and
		prog_collegamento = :this.prog_collegamento;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Editor Documenti", "Impossibile eliminare collegamento.~r~n" + sqlca.sqlerrtext)
		rollback;
		return false
	elseif sqlca.sqlcode = 0 then
		commit;
		return true
	else
		return false
	end if
	
end if

return false
end function

public function string save (integer ai_pixel_width, integer ai_pixel_height);string ls_error
int		li_prog_collegamento

// -- Coordinale
this.x1 = round( UnitsToPixels(this.x, XUnitsToPixels!) / ai_pixel_width, 4)
this.y1 = round( UnitsToPixels(this.y, YUnitsToPixels!) / ai_pixel_height, 4)
this.x2 = round( UnitsToPixels(this.x + this.width, XUnitsToPixels!) / ai_pixel_width, 4)
this.y2 = round( UnitsToPixels(this.y + this.height, YUnitsToPixels!) / ai_pixel_height, 4)

// -- Salvataggio
if this.prog_collegamento = -1 then
	
	// -- max + 1
	select max(prog_collegamento)
	into	:li_prog_collegamento
	from	tab_collegamenti_processi
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		prog_processo = :this.prog_processo;
	
	if sqlca.sqlcode < 0 then
		return "Impossibile calcolare progressivo coleggamento.~r~n" + sqlca.sqlerrtext
	elseif sqlca.sqlcode = 100 or isnull(li_prog_collegamento) then
		li_prog_collegamento = 0
	end if
	
	li_prog_collegamento++
	this.prog_collegamento = li_prog_collegamento
	// ----
	
	insert into tab_collegamenti_processi (
		cod_azienda,
		prog_processo,
		prog_collegamento,
		x1, x2, y1, y2,
		prog_elemento,
		tipo_elemento,
		valore)
	values (
		:s_cs_xx.cod_azienda,
		:this.prog_processo,
		:this.prog_collegamento,
		:this.x1, :this.x2,
		:this.y1, :this.y2,
		:this.prog_elemento,
		:this.tipo_elemento,
		:this.valore);
	
	if sqlca.sqlcode <> 0 then
		rollback;
		return sqlca.sqlerrtext
	else
		commit;
		return ""
	end if
	
else
	
	//if modify then
		
		update tab_collegamenti_processi
		set 
			x1 = :this.x1, x2 = :this.x2,
			y1 = :this.y1, y2 = :this.y2,
			prog_elemento = :this.prog_elemento,
			tipo_elemento = :this.tipo_elemento,
			valore = :this.valore
		where
			cod_azienda=:s_cs_xx.cod_azienda and
			prog_processo = :this.prog_processo and
			prog_collegamento = :this.prog_collegamento;
			
		if sqlca.sqlcode <0 then
			ls_error = "Errore durante l'aggiornamento del collegamento.~r~n" + sqlca.sqlerrtext
			rollback;
			return ls_error
		elseif sqlca.sqlcode = 100 then
			rollback;
			ls_error = "Impossibile trovare collegamento con codice " + string(this.prog_collegamento)
		else
			commit;
			return ""
		end if
		
	//end if
end if

return ""
end function

on uo_editor_documenti_nodo.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_editor_documenti_nodo.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

