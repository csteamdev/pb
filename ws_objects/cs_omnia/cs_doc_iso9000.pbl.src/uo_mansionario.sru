﻿$PBExportHeader$uo_mansionario.sru
forward
global type uo_mansionario from nonvisualobject
end type
end forward

global type uo_mansionario from nonvisualobject
end type
global uo_mansionario uo_mansionario

type variables
/**/
private:
	boolean ib_cs_system = false
	uo_array iuo_privileges
	
	string is_error_message
	string is_cod_mansionario
	string is_firma

// Inserire qui la lista delle costanti che identificano il privilegio
public:
	constant string VERIFICA = '001'
	constant string LETTURA = '002'
	constant string QUALIFICAZIONE = '003'
	constant string APPROVAZIONE = '004'
	constant string MODIFICA = '005'
	constant string LIVELLO_DOC = '006'
	constant string VALIDAZIONE = '007'
	constant string ELIMINA = '008'
	constant string SUPERVISORE = '009'
	constant string APERTURA_NC = '010'
	constant string CHIUSURA_NC = '011'
	constant string AUTORIZZA_RESO = '012'
	constant string CHIUDI_RECLAMI = '013'
	constant string APPROVA_AZ_PREV = '014'
	constant string CHIUSURA_AZ_PREV = '015'
	constant string APPROVA_AZ_CORR = '016'
	constant string CHIUSURA_AZ_CORR = '017'
	constant string APPROVA_RDA = '018'
	constant string AUTORIZZA_RDA = '019'
	constant string APPROVA_ORD_ACQ = '020'
	constant string CHIUSURA_ORD_ACQ = '021'
	constant string APPROVA_OFF_VEN = '022'
	constant string SCADENZ_MEZZI = '023' //15/11/2011: aggiunto da Donato per Sintexcal
	constant string AUTORIZZA_TRAT_NC = '024' //13/11/2012: aggiunto da Stefanop per Vignoni
end variables

forward prototypes
public function boolean uof_load_mansionario_corrente ()
public function boolean uof_load_mansionario (string as_cod_mansionario)
public function boolean uof_get_privilege (string as_cod_privilege)
public function string uof_get_privilege_string (string as_cod_privilege)
public function string uof_get_error ()
public function string uof_get_cod_mansionario ()
private function integer uof_load_firma (string as_cod_mansionario)
public function string uof_get_firma ()
public function string uof_return_firma (string as_cod_mansionario)
end prototypes

public function boolean uof_load_mansionario_corrente ();/**
 * stefanop
 * 03/11/2011
 *
 * Recupero il codice del mansionario collegato
 **/
 
string ls_sql, ls_cod_resp_divisione
int li_count
datastore lds_store

setnull(is_error_message)
setnull(ls_cod_resp_divisione)

// sono cs_system?
if s_cs_xx.cod_utente <> "CS_SYSTEM" then

	ls_sql = "SELECT cod_resp_divisione FROM mansionari WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND cod_utente='" + s_cs_xx.cod_utente + "'"
	li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)
	
	if li_count < 1 then
		is_error_message = "L'utente collegato non ha nessun mansionario associato"
		return false
	else
		ls_cod_resp_divisione = lds_store.getitemstring(1,1)
	end if
	
else
	ls_cod_resp_divisione = "CS_SYSTEM"
	
end if

uof_load_firma(ls_cod_resp_divisione)


return uof_load_mansionario(ls_cod_resp_divisione)
end function

public function boolean uof_load_mansionario (string as_cod_mansionario);/**
 * stefanop
 * 03/11/2011
 *
 * Carico i privilegi del mansionario passato
 **/
 
string ls_sql, ls_errore
int li_count, li_i
datastore lds_store

is_cod_mansionario = as_cod_mansionario
ib_cs_system = (as_cod_mansionario = "CS_SYSTEM")
iuo_privileges.removeall( )

// sono CS_SYSTEM quindi abilito tutti i permessi
if ib_cs_system then return true

ls_sql = "SELECT cod_privilegio, cod_valore_lista FROM tab_privilegi_mansionari WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_sql += "AND cod_mansionario='" + as_cod_mansionario + "' "
ls_sql += "ORDER BY cod_privilegio"

li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql, is_error_message)

if li_count < 0 then return false

for li_i = 1 to li_count
	
	iuo_privileges.set(lds_store.getitemstring(li_i, 1), lds_store.getitemstring(li_i, 2))
	
next

return true
end function

public function boolean uof_get_privilege (string as_cod_privilege);/**
 * stefanop
 * 03/11/2011
 *
 * Recupero il permesso boolean legato al mansionario.
 * Se il privilegio non esiste o non è associato torna sempre FALSE
 **/
 
// se sono CS_SYSTEM ho il potere completo
if ib_cs_system then return true

return iuo_privileges.haskey(as_cod_privilege)
end function

public function string uof_get_privilege_string (string as_cod_privilege);/**
 * stefanop
 * 03/11/2011
 *
 * Recupero il permesso stringa legato al mansionario.
 * Se il privilegio non esiste o non è associato torna sempre un valore nullo
 **/
 
string ls_result

setnull(ls_result)

if iuo_privileges.haskey(as_cod_privilege) then
	ls_result = iuo_privileges.get(as_cod_privilege)
end if

return ls_result
end function

public function string uof_get_error ();/**
 * stefanop
 * 09/11/2011
 *
 * Ritorno il messaggio di errore dell'oggetto
 **/
 
return is_error_message
end function

public function string uof_get_cod_mansionario ();/** 
 * Giulio
 * 09/11/2011
 * 
 * ritorna il codice del mansionario caricato
 **/
 
return is_cod_mansionario
end function

private function integer uof_load_firma (string as_cod_mansionario);/** 
 * Giulio
 * 15/12/2011
 * 
 * recupera il nome e cognome del mansionario attraverso il suo codice
 **/

string ls_cod_utente, ls_nome_cognome


if as_cod_mansionario = 'CS_SYSTEM' then
	is_firma = 'SYSTEM'
else
	select mansionari.cod_utente, utenti.nome_cognome
	into :ls_cod_utente ,:ls_nome_cognome
	from mansionari join utenti on utenti.cod_utente = mansionari.cod_utente
	where 
		mansionari.cod_azienda = :s_cs_xx.cod_azienda and
		mansionari.cod_resp_divisione = :as_cod_mansionario ;
	
//	if sqlca.sqlcode <> 0 then
//		return -1
//	end if
	
	if isnull(ls_nome_cognome) or len(ls_nome_cognome) < 1 then
		is_firma = ls_cod_utente
	else
		is_firma = ls_nome_cognome
	end if	
		
end if

return 0
end function

public function string uof_get_firma ();/** 
 * Giulio
 * 15/12/2011
 * 
 * ritorna la firma dell'utente
 **/

return is_firma
end function

public function string uof_return_firma (string as_cod_mansionario);/**
 * Giulio
 * 19/12/2011
 *
 * Recupero il codice del mansionario
 * Se sono CS_SYSTEM la firma sarà SYSTEM
 * Altrimenti recupera il nome utente associato al codice e lo ritorna
 **/

string ls_cod_utente, ls_nome_cognome


if as_cod_mansionario = 'CS_SYSTEM' then
	is_firma = 'SYSTEM'
else
	select mansionari.cod_utente, utenti.nome_cognome
	into :ls_cod_utente ,:ls_nome_cognome
	from mansionari join utenti on utenti.cod_utente = mansionari.cod_utente
	where 
		mansionari.cod_azienda = :s_cs_xx.cod_azienda and
		mansionari.cod_resp_divisione = :as_cod_mansionario ;
	
//	if sqlca.sqlcode <> 0 then
//		return -1
//	end if
	
	if isnull(ls_nome_cognome) or len(ls_nome_cognome) < 1 then
		is_firma = ls_cod_utente
	else
		is_firma = ls_nome_cognome
	end if	
		
end if

return is_firma
end function

on uo_mansionario.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mansionario.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_privileges = create uo_array

uof_load_mansionario_corrente()
end event

event destructor;destroy iuo_privileges
end event

