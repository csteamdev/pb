﻿$PBExportHeader$uo_navigatore.sru
forward
global type uo_navigatore from datawindow
end type
end forward

global type uo_navigatore from datawindow
integer width = 882
integer height = 584
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
event ue_rect_added ( integer as_index,  integer ai_prog_collegamento,  long al_prog_elemento,  string as_tipo_elemento,  string as_nome_elemento )
event ue_rect_selected ( integer ai_index )
event ue_rect_removed ( integer ai_index )
event ue_map_rename ( integer ai_index,  string as_newtext )
end type
global uo_navigatore uo_navigatore

type variables
public string is_user_temppath		// Percorso cartella temporanea utente
public string is_user_documents	// Percorso cartella documenti utente
private string is_image_path			// Percorso immagine datawindow
private string is_image_name		// Nome immagine per salvarla nell'HD
private string is_type_last_rect		// usato per la selezione dell'ultimo nodo (clicked)
private string is_cod_area			// Limite area aziendale

private string is_des_processo
private uo_rectangle ir_rectangles[]	// Rettangoli
private long il_rectangles_to_delete[]	// Indice dei rettangoli da eliminare dal DB

private int ii_map_height_pixel	// Dimensioni mappa in pixel
private int ii_map_width_pixel	// Dimensioni mappa in pixel
private int ii_map_height			// Dimensioni mappa in PB
private int ii_map_width			// Dimensioni mappa in PB
public int   ii_max_dw_width		// Dimensioni massime della DW
public int   ii_max_dw_height	// Dimensioni massime della DW

private int ii_prog_processo		// Progressivo Processo
private int ii_prog_processo_modello

private boolean ib_modified = false
private boolean ib_image_change = false
private boolean ib_area_change = false

private int ii_rect_active_index
private int ii_last_rect_active_index

public long il_color_map
public long il_color_map_select
public long il_color_document
public long il_color_document_select
end variables

forward prototypes
public function boolean uof_load_map (integer ai_prog_processo)
public subroutine uof_reset ()
private subroutine uof_tilde (ref string as_string)
public function boolean uof_set_image (string as_path, string as_name, integer ai_width, integer ai_height)
public subroutine uof_draw ()
private function integer uof_load_node (integer ai_prog_processo)
public function boolean uof_add_rect (integer ai_x, integer ai_y, integer ai_width, integer ai_height, integer ai_prog_collegamento, long al_prog_elemento, string as_tipo_elemento, string as_nome_elemento)
private function string uof_build_node ()
public function boolean uof_save_rect_position (boolean ab_save_all)
public function boolean uof_remove_map (integer ai_prog_processo)
public function boolean uof_save ()
private function boolean uof_save_map ()
private function boolean uof_save_rects ()
public function integer uof_get_progprocesso ()
public subroutine uof_set_desprocesso (string as_des_processo)
public subroutine uof_set_progprocesso_modello (integer ai_prog_processo_modello)
public function boolean uof_set_visibility (integer ai_index, string as_visible)
public function boolean uof_set_rect_info (long al_prog_elemento, string as_tipo_elemento, string as_valore)
public function boolean uof_remove_rect ()
public subroutine uof_rename_child (integer ai_prog_processo, string as_new_text)
public function string uof_get_area ()
public subroutine uof_set_area (string as_cod_area)
end prototypes

public function boolean uof_load_map (integer ai_prog_processo);/**
 * Stefano Pulze
 * 21/09/2009
 *
 * Dato un codice processo viene caricata la mappa (sarà salvata nella cartella temporale dell'utente
 * collegato a windows) e tutti gli eventuali nodi associti.
 *
 * @param int ai_prog_processo
 * @return int
 */
 
string ls_map_name, ls_map_path, ls_cod_area
blob lb_blob_mappa

if ai_prog_processo < 1 then 
	// processo non valido
	return false
end if

// resettto la datawindow e tutte le variabili locali
this.uof_reset()

// carico il blob della mappa
selectblob 	blob_processo
into			:lb_blob_mappa
from			processi
where			cod_azienda = :s_cs_xx.cod_azienda and
				prog_processo = :ai_prog_processo;
				
if sqlca.sqlcode <> 0 or isnull(lb_blob_mappa) then
	g_mb.messagebox("Navigatore", "Impossibile recuperare file immagine.~r~n" + sqlca.sqlerrtext)
	return  false
end if

select		path_blob, altezza, larghezza, cod_area_aziendale
into		:ls_map_name, :ii_map_height_pixel, :ii_map_width_pixel, :ls_cod_area
from		processi
where		cod_azienda = :s_cs_xx.cod_azienda and
			prog_processo = :ai_prog_processo;
			
if sqlca.sqlcode <> 0 or isnull(ls_map_name) or ls_map_name = "" then
	g_mb.messagebox("Navigatore", "Impossibile recuperare dimensioni mappa~r~n" + sqlca.sqlerrtext)
	return false
end if
// ---------------

ii_prog_processo = ai_prog_processo

ii_map_width  = PixelsToUnits(ii_map_width_pixel, xpixelstounits!)
ii_map_height = PixelsToUnits(ii_map_height_pixel, ypixelstounits!)

setredraw(false)

if isnull(is_user_temppath) or len(is_user_temppath) < 1 then
	g_mb.messagebox("Navigatore", "Impossibile individuale cartella temporanea")
	return false
end if

ls_map_path = is_user_temppath + ls_map_name
if f_blob_to_file(ls_map_path, lb_blob_mappa) = 0 then
	
	this.uof_set_image(ls_map_path, ls_map_name,  ii_map_width, ii_map_height)
	this.uof_load_node(ai_prog_processo)
	
	// Fix, l'immagine non è stata cambiata ma è quella originaria, quindi resetto il flag
	ib_image_change = false
	
end if

ib_modified = false
uof_draw()

bringtotop = true
setredraw(true)

return true
end function

public subroutine uof_reset ();/**
 * Stefano Pulze
 * 21/09/2009
 *
 * Reset dell'array dei rettangoli
 */
 
// reset rettangoli
uo_rectangle lr_empty[]
ir_rectangles = lr_empty
ii_rect_active_index = -1
ii_last_rect_active_index = -1
// ----

// reset dimensioni immagine
ii_map_height_pixel = -1
ii_map_width_pixel = -1
ii_map_width = -1
ii_map_height = -1
ib_image_change = false
is_des_processo = ""
// ----

ib_modified = false
ii_prog_processo = -1
ii_prog_processo_modello = -1
is_cod_area = ""
end subroutine

private subroutine uof_tilde (ref string as_string);string	ls_tilde
integer li_lunghezza_formula,li_t,li_posizione,li

ls_tilde = "~~"

li_lunghezza_formula = len(as_string)

if li_lunghezza_formula=0 then return

li_posizione = 1

do while 1=1
	li_posizione = pos(as_string, ls_tilde, li_posizione)
	if li_posizione = 0 then exit	
	as_string = replace ( as_string, li_posizione, len(ls_tilde), ls_tilde + ls_tilde ) 
	li_posizione += len(ls_tilde + ls_tilde)
loop
end subroutine

public function boolean uof_set_image (string as_path, string as_name, integer ai_width, integer ai_height);/**
 * Stefano Pulze
 * 07/10/2009
 * 
 * Imposta un immagine di sfondo alla datawindow, aggiorna l'altezza del detail e ne controlla la
 * dimensione massima.
 */
 
string ls_error

if isnull(as_path) or as_path = "" or is_image_path = as_path then return false

// Quoto le tilde presenti nella stringa che alla DW danno fastidio.
this.uof_tilde(ref as_path)
this.is_image_path = as_path
this.is_image_name = as_name

ls_error = this.modify("DataWindow.Picture.file='" + is_image_path + "'")

if ls_error <> "" then
	g_mb.messagebox("Navigatore", "Impossibile assegnare la nuova mappa alla datawindow.~r~n" + ls_error)
	return false
else
	ib_modified = true
	ib_image_change = true
	
	// Units
	ii_map_width = ai_width
	ii_map_height = ai_height
	// --
	
	// Pixels
	ii_map_width_pixel = UnitsToPixels(ii_map_width, XUnitsToPixels!)
	ii_map_height_pixel = UnitsToPixels(ii_map_height, YUnitsToPixels!)
	// ----
	
	modify("DataWindow.detail.Height=" + string(ii_map_height))
	
	// Controllo dimensione massime
	hscrollbar = false
	vscrollbar = false
	
	if ii_max_dw_width < ii_map_width then
		ii_map_width = ii_max_dw_width
		hscrollbar = true
	end if
	
	if ii_max_dw_height < ii_map_height then
		ii_map_height = ii_max_dw_height
		vscrollbar = true
	end if
	
	resize(ii_map_width, ii_map_height)
	// ----
	
	return true
end if
end function

public subroutine uof_draw ();string ls_dw, ls_error
boolean lb_return

ls_dw = 'release 11.5; &
datawindow(units=0 timer_interval=0 color=16777215 brushmode=6 transparency=0 gradient.angle=0 gradient.color=8421504 gradient.focus=0 gradient.repetition.count=0 gradient.repetition.length=100 gradient.repetition.mode=0 gradient.scale=100 gradient.spread=100 gradient.transparency=0 picture.blur=0 picture.clip.bottom=0 picture.clip.left=0 picture.clip.right=0 picture.clip.top=0 picture.mode=0 picture.scale.x=100 picture.scale.y=100 picture.transparency=0 processing=0 HTMLDW=no print.printername="" print.documentname="" print.orientation = 0 print.margin.left = 110 print.margin.right = 110 print.margin.top = 96 print.margin.bottom = 96 print.paper.source = 0 print.paper.size = 0 print.canusedefaultprinter=yes print.prompt=no print.buttons=no print.preview.buttons=no print.cliptext=no print.overrideprintjob=no print.collate=yes print.background=no print.preview.background=no print.preview.outline=yes hidegrayline=no showbackcoloronxp=no picture.file="' + this.is_image_path + '" ) &
header(height=0 color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" ) &
summary(height=0 color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" ) &
footer(height=0 color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" ) &
detail(height=' + string(this.height) +' color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" height.autosize=yes ) &
table(column=(type=char(10) updatewhereclause=yes name=a dbname="a" ) &
 )  ' + uof_build_node() + ' &
htmltable(border="1" ) &
htmlgen(clientevents="1" clientvalidation="1" clientcomputedfields="1" clientformatting="0" clientscriptable="0" generatejavascript="1" encodeselflinkargs="1" netscapelayers="0" pagingmethod=0 generatedddwframes="1" ) &
xhtmlgen() cssgen(sessionspecific="0" ) &
xmlgen(inline="0" ) &
xsltgen() &
jsgen() &
export.xml(headgroups="1" includewhitespace="0" metadatatype=0 savemetadata=0 ) &
import.xml() &
export.pdf(method=0 distill.custompostscript="0" xslfop.print="0" ) &
export.xhtml()'

setredraw(false)
if create(ls_dw, ref ls_error) < 0 then
	messagebox("uo_dw_navigator::uof_redraw", ls_error)
else
	this.insertrow(0)
end if

bringtotop = true
setredraw(true)
end subroutine

private function integer uof_load_node (integer ai_prog_processo);string ls_sql, ls_tipo_elemento
long	ll_rows, ll_row, ll_r, x1, y1, x2, y2
datastore lds_store

ls_sql = "SELECT prog_collegamento, x1, y1, x2, y2, prog_elemento, tipo_elemento, valore FROM tab_collegamenti_processi WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND  prog_processo=" + string(ai_prog_processo)
if not f_crea_datastore(ref lds_store, ls_sql) then 
	g_mb.messagebox("Navigatore", "Errore durante la creazione del datastore per i nodi.")
	return -1
end if

ll_rows = lds_store.retrieve()

if ll_rows = 0 then return 0

for ll_row = 1 to ll_rows
	
	x1 = PixelsToUnits(lds_store.getitemnumber(ll_row, "x1") *  ii_map_width_pixel, XPixelsToUnits!)
	y1 = PixelsToUnits(lds_store.getitemnumber(ll_row, "y1") *  ii_map_height_pixel, YPixelsToUnits!)
	
	x2 = PixelsToUnits(lds_store.getitemnumber(ll_row, "x2") *  ii_map_width_pixel, XPixelsToUnits!)
	y2 = PixelsToUnits(lds_store.getitemnumber(ll_row, "y2") *  ii_map_height_pixel, YPixelsToUnits!)
	
	uof_add_rect(x1, y1, x2 - x1, y2 - y1, lds_store.getitemnumber(ll_row, "prog_collegamento"), lds_store.getitemnumber(ll_row, "prog_elemento"), lds_store.getitemstring(ll_row, "tipo_elemento"), lds_store.getitemstring(ll_row, "valore"))
	
next

destroy lds_store

return ll_rows
end function

public function boolean uof_add_rect (integer ai_x, integer ai_y, integer ai_width, integer ai_height, integer ai_prog_collegamento, long al_prog_elemento, string as_tipo_elemento, string as_nome_elemento);/**
 * Stefano Pulze
 * 11/09/2009
 *
 * Consente di aggiungere un rettangolo alla datawindow in base ai parametri passati.
 *
 * @param int ai_x posizione x nella datawindow
 * @param int ai_y posizione y nella datawindow
 * @param int ai_width larghezza del rettangolo di selezione
 * @param int ai_height altezza del rettangolo di selezione
 * @param int ai_prog_collegamento progressivo univoco del collegamento
 * @param long al_prog_elemento
 * @param string as_tipo_elemento M per mappa, D per documento
 * @param string as_nome_elemento
 */
 
integer li_index

li_index = upperbound(this.ir_rectangles) + 1

// Controllo coordinate
if ai_x >= ii_map_width then ai_x = ii_map_width - ai_width
if ai_y >= ii_map_height then ai_y = ii_map_height - ai_height
// --

this.ir_rectangles[li_index] = create uo_rectangle
this.ir_rectangles[li_index].x = ai_x
this.ir_rectangles[li_index].y = ai_y
this.ir_rectangles[li_index].width = ai_width
this.ir_rectangles[li_index].height = ai_height

this.ir_rectangles[li_index].name = "r_" + string(li_index)
this.ir_rectangles[li_index].visible = "1"

this.ir_rectangles[li_index].prog_collegamento = ai_prog_collegamento
this.ir_rectangles[li_index].prog_elemento = al_prog_elemento
this.ir_rectangles[li_index].tipo_elemento = as_tipo_elemento
this.ir_rectangles[li_index].valore = as_nome_elemento

ib_modified = true
event ue_rect_added(li_index, ai_prog_collegamento, al_prog_elemento, as_tipo_elemento, as_nome_elemento)
return true
end function

private function string uof_build_node ();/**
 * Stefano Pulze
 * 21/09/2009
 *
 * Crea il codice per la visualizzazione dinamica dei rettangoli all'interno della datawindow
 * il nome associato sarà sempre r_ più un progressivo numerico
 *
 * @return string
 */
string ls_code, ls_color
integer li_i, li_rectangles_count

ls_code = ""

li_rectangles_count = upperbound(this.ir_rectangles)

if li_rectangles_count > 0 then

	for li_i = 1 to li_rectangles_count
	
		// Controllo che il rettangolo non sia stato eliminato
		if isvalid(this.ir_rectangles[li_i]) then
			ls_code += "rectangle(band=detail resizeable=1 moveable=1 brush.hatch='8' brush.color='0' pen.style='0' pen.width='9' background.mode='2' background.color='33554432' background.gradient.color='8421504' background.gradient.transparency='0' background.gradient.angle='0' background.brushmode='0' background.gradient.repetition.mode='0' background.gradient.repetition.count='0' background.gradient.repetition.length='100' background.gradient.focus='0' background.gradient.scale='100' background.gradient.spread='100' tooltip.backcolor='134217752' tooltip.delay.initial='0' tooltip.delay.visible='32000' tooltip.enabled='0' tooltip.hasclosebutton='0' tooltip.icon='0' tooltip.isbubble='0' tooltip.maxwidth='0' tooltip.textcolor='134217751' tooltip.transparency='0'"
			ls_code += "x='" + string(this.ir_rectangles[li_i].x) + "' "
			ls_code += "y='" + string(this.ir_rectangles[li_i].y) + "' "
			ls_code += "height='" + string(this.ir_rectangles[li_i].height) + "' "
			ls_code += "width='" + string(this.ir_rectangles[li_i].width) + "' "
			ls_code += "name=r_" + string(li_i) + " "
			ls_code += "visible='" + string(this.ir_rectangles[li_i].visible) + "'"
			
			if this.ir_rectangles[li_i].tipo_elemento = "D" then
				ls_color = string(il_color_document)
			else
				ls_color = string(il_color_map)
			end if
			
			ls_code += "pen.color='" + ls_color + "' "
			ls_code += "background.transparency='50' "
			ls_code += ") "
		end if
	next

end if

return ls_code
end function

public function boolean uof_save_rect_position (boolean ab_save_all);integer li_rectangles_count, li_i

if ab_save_all then
	li_rectangles_count = upperbound(this.ir_rectangles)
else
	li_rectangles_count = upperbound(this.ir_rectangles) - 1
end if

if li_rectangles_count > 0 then
	
	for li_i = 1 to li_rectangles_count
		
		// controllo eventuali errori quali l'esistenza del rettangolo e l'accessibilità delle proprietà
		if not isvalid(ir_rectangles[li_i]) then continue
		if describe(ir_rectangles[li_i].name + ".x") = "!" then continue
		
		ir_rectangles[li_i].x = integer(describe(ir_rectangles[li_i].name + ".x"))
		ir_rectangles[li_i].y = integer(describe(ir_rectangles[li_i].name + ".y"))
		ir_rectangles[li_i].width = integer(describe(ir_rectangles[li_i].name + ".width"))
		ir_rectangles[li_i].height = integer(describe(ir_rectangles[li_i].name + ".height"))
		ir_rectangles[li_i].visible = string(describe(ir_rectangles[li_i].name + ".visible"))
			
	next
	
end if

return true
end function

public function boolean uof_remove_map (integer ai_prog_processo);if ai_prog_processo < 1 then
	ai_prog_processo = ii_prog_processo
end if

// Elimino prima i nodi
DELETE FROM tab_collegamenti_processi 
WHERE cod_azienda=:s_cs_xx.cod_azienda AND  prog_processo=:ai_prog_processo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Navigatore", "Errore durante la cancellazione dei nodi.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
end if
// ---

// Elimino i figli se ci sono
DELETE FROM processi
WHERE cod_azienda=:s_cs_xx.cod_azienda AND prog_processo_modello=:ai_prog_processo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Navigatore", "Errore durante la cancellazione delle mappa figlie.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
end if
// ---

// Elimino mappa
DELETE FROM processi
WHERE cod_azienda=:s_cs_xx.cod_azienda AND prog_processo=:ai_prog_processo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Navigatore", "Errore durante la cancellazione della mappa.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
end if
// ---

commit;

if ai_prog_processo = ii_prog_processo then
	ii_prog_processo = -1
end if

return true
end function

public function boolean uof_save ();// Prima salvo la mappa
if uof_save_map() then
	
	if uof_save_rects() then
		commit;
		
		ib_modified = false
		ib_image_change = false
		return true
	else
		rollback;
		return false;
	end if
	
else
	rollback;
	return false
	
end if
end function

private function boolean uof_save_map ();long 	ll_prog_processo
date 	ld_today
blob	lblb_map

if ii_prog_processo = -1 then
	
	// Calcolo il progressivo
	select max(prog_processo)
	into	:ll_prog_processo
	from	processi
	where	cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Navigatore", "Errore durante il calcolo del progressivo processo.~r~n"+sqlca.sqlerrtext)
		return false
	elseif sqlca.sqlcode = 100 or isnull(ll_prog_processo) then
		ll_prog_processo = 0
	end if
	
	ll_prog_processo++
	ii_prog_processo = ll_prog_processo
	
	if ii_prog_processo_modello = -1 then
		// La mappa inserita è un nuovo padre, quindi il prog_processo deve essere uguale prog_processo_modello
		ii_prog_processo_modello = ll_prog_processo
	end if
	
	ld_today = today()
	
	insert into processi (
		cod_azienda,
		prog_processo,
		des_processo,
		data_creazione,
		note,
		modello,
		prog_processo_modello,
		cod_area_aziendale)
	values (
		:s_cs_xx.cod_azienda,
		:ll_prog_processo,
		:is_des_processo,
		:ld_today,
		:is_des_processo,
		'D',
		:ii_prog_processo_modello,
		:is_cod_area);
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Navigatore", "Impossibile salvare la nuova mappa.~r~n" + sqlca.sqlerrtext)
		return false
	end if
	
end if

if ib_area_change then
	update processi
	set
		cod_area_aziendale = :is_cod_area
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		prog_processo = :ii_prog_processo;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Navigatore", "Errore durante il salvataggio del codice area aziendale.~r~n" + sqlca.sqlerrtext)
		return false
	end if
end if

if ib_image_change then
		
	update processi
	set
		altezza = :ii_map_height_pixel,
		larghezza = :ii_map_width_pixel,
		path_blob = :is_image_name
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		prog_processo = :ii_prog_processo;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Navigatore", "Errore durante il salvataggio misure mappa.~r~n" + sqlca.sqlerrtext)
		return false
	end if
	
	if f_file_to_blob(is_image_path, ref lblb_map) <> 0 then
		g_mb.messagebox("Navigatore", "Errore durante la conversione del file immagine in blob.")
		return false
	end if
	
	updateblob processi
	set blob_processo = :lblb_map
	where cod_azienda = :s_cs_xx.cod_azienda and
			 prog_processo = :ii_prog_processo;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Navigatore", "Errore durante il salvataggio file blob.~r~n" + sqlca.sqlerrtext)
		return false
	end if
	
end if

ib_image_change = false
return true
end function

private function boolean uof_save_rects ();integer li_i, li_prog_collegamento
dec{4} ld_x1, ld_y1, ld_x2, ld_y2

if ii_prog_processo < 1 then
	return false
end if

uof_save_rect_position(true)

for li_i = 1 to upperbound(ir_rectangles)
	
	if not isvalid(ir_rectangles[li_i]) then
		// Il nodo è stato elimanto
		continue
	end if
	
	// Posizioni x y in pixels
	ld_x1 = abs(UnitsToPixels(ir_rectangles[li_i].x, XUnitsToPixels!) / ii_map_width_pixel)
	ld_y1 = abs(UnitsToPixels(ir_rectangles[li_i].y, YUnitsToPixels!) / ii_map_height_pixel)
	
	ld_x2 = abs(UnitsToPixels(ir_rectangles[li_i].width + ir_rectangles[li_i].x, XUnitsToPixels!) / ii_map_width_pixel)
	ld_y2 = abs(UnitsToPixels(ir_rectangles[li_i].height + ir_rectangles[li_i].y, YUnitsToPixels!) / ii_map_height_pixel)
	// --
	
	if ir_rectangles[li_i].prog_collegamento < 1 then
		
		select max(prog_collegamento)
		into	:li_prog_collegamento
		from	tab_collegamenti_processi
		where	cod_azienda = :s_cs_xx.cod_azienda and
				prog_processo = :ii_prog_processo;
				
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Navigatore", "Impossibile calcolare progressivo elemento.~r~n" + sqlca.sqlerrtext)
			return false
		elseif sqlca.sqlcode = 100 or isnull(li_prog_collegamento) then
			li_prog_collegamento = 0
		end if
		
		li_prog_collegamento++
		ir_rectangles[li_i].prog_collegamento = li_prog_collegamento
		
		insert into tab_collegamenti_processi (
			cod_azienda,
			prog_processo,
			prog_collegamento,
			x1, y1, x2, y2,
			prog_elemento,
			tipo_elemento,
			valore)
		values (
			:s_cs_xx.cod_azienda,
			:ii_prog_processo,
			:li_prog_collegamento,
			:ld_x1, :ld_y1, :ld_x2, :ld_y2,
			:ir_rectangles[li_i].prog_elemento,
			:ir_rectangles[li_i].tipo_elemento,
			:ir_rectangles[li_i].valore);
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Navigatore", "Errore durante l'inserimento del nuovo elemento.~r~n" + sqlca.sqlerrtext)
			return false
		end if
	else
		
		update tab_collegamenti_processi
		set x1 = :ld_x1,
			y1 = :ld_y1,
			x2 = :ld_x2,
			y2 = :ld_y2,
			prog_elemento = :ir_rectangles[li_i].prog_elemento,
			tipo_elemento = :ir_rectangles[li_i].tipo_elemento,
			valore = :ir_rectangles[li_i].valore
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_processo = :ii_prog_processo and
				prog_collegamento = :ir_rectangles[li_i].prog_collegamento;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Navigatore", "Errore durante l'aggiornamento dell'elemento.~r~n" + sqlca.sqlerrtext)
			return false
		end if
		
	end if
	
next

// Controllo i nodi da eliminare
if upperbound(il_rectangles_to_delete) > 0 then
	for li_i = 1 to upperbound(il_rectangles_to_delete)
		
		delete from tab_collegamenti_processi
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_processo = :ii_prog_processo and
				prog_collegamento = :il_rectangles_to_delete[li_i];
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Navigatore", "Errore durante la cancellazione del nodo dal database.~r~n" + sqlca.sqlerrtext)
			return false
		end if
		
	next
end if

return true
end function

public function integer uof_get_progprocesso ();return ii_prog_processo
end function

public subroutine uof_set_desprocesso (string as_des_processo);is_des_processo = as_des_processo
end subroutine

public subroutine uof_set_progprocesso_modello (integer ai_prog_processo_modello);ii_prog_processo_modello = ai_prog_processo_modello
end subroutine

public function boolean uof_set_visibility (integer ai_index, string as_visible);if not isnull(ai_index) or ai_index > 0 or ai_index <= upperbound(this.ir_rectangles) then
	
	
	modify("r_" + string(ai_index) + ".visible='" + as_visible + "'")
	
else
	return false
	
end if
end function

public function boolean uof_set_rect_info (long al_prog_elemento, string as_tipo_elemento, string as_valore);if ii_rect_active_index > 0 and ii_rect_active_index <= upperbound(this.ir_rectangles) then
	
	this.ir_rectangles[ii_rect_active_index].prog_elemento = al_prog_elemento
	this.ir_rectangles[ii_rect_active_index].tipo_elemento = as_tipo_elemento
	this.ir_rectangles[ii_rect_active_index].valore = as_valore
	
else
	return false
end if

end function

public function boolean uof_remove_rect ();integer li_rectangles_count

if ii_rect_active_index > 0 and ii_rect_active_index <= upperbound(this.ir_rectangles) then
	
	uof_save_rect_position(true)
	
	if ir_rectangles[ii_rect_active_index].prog_collegamento > 0 then
		il_rectangles_to_delete[upperbound(il_rectangles_to_delete) + 1] = ir_rectangles[ii_rect_active_index].prog_collegamento
	end if
	
	destroy(this.ir_rectangles[ii_rect_active_index])
	event ue_rect_removed(ii_rect_active_index)
	
	uof_draw()
	return true
else
	return false
end if
end function

public subroutine uof_rename_child (integer ai_prog_processo, string as_new_text);int li_row, li_i

li_row = upperbound(this.ir_rectangles)

if li_row > 0 then
	for li_i = 1 to li_row
		if isvalid(this.ir_rectangles[li_i]) then
			
			if this.ir_rectangles[li_i].prog_elemento = ai_prog_processo and this.ir_rectangles[li_i].tipo_elemento = "M" then
				this.ir_rectangles[li_i].valore = as_new_text	
				this.event ue_map_rename(li_i, as_new_text)
			end if
			
		end if
	next
end if
end subroutine

public function string uof_get_area ();return is_cod_area
end function

public subroutine uof_set_area (string as_cod_area);if is_cod_area <> as_cod_area then
	ib_area_change = true
	is_cod_area = as_cod_area
elseif isnull(as_cod_area) then
	ib_area_change = true
	setnull(is_cod_area)
end if
end subroutine

on uo_navigatore.create
end on

on uo_navigatore.destroy
end on

event constructor;call super::constructor;

this.ii_max_dw_width = this.width
this.ii_max_dw_height = this.height
end event

event clicked;string ls_color

if left(dwo.name, 2) = "r_" then
	
	ii_rect_active_index = integer(mid(dwo.name, 3))
	
	if ii_last_rect_active_index <> ii_rect_active_index then
		
		// imposto il colore normale
		if is_type_last_rect = "D" then
			ls_color = string(il_color_document)
		else
			ls_color = string(il_color_map)
		end if
		modify("r_" + string(ii_last_rect_active_index) + ".pen.color='" + ls_color + "'")
		// ----

		// seleziono il nuovo
		if this.ir_rectangles[ii_rect_active_index].tipo_elemento = "D" then
			ls_color = string(il_color_document_select)
		else
			ls_color = string(il_color_map_select)
		end if
		
		modify(dwo.name + ".pen.color='" + ls_color + "'")
		// ----
		
		ii_last_rect_active_index = ii_rect_active_index
		is_type_last_rect = this.ir_rectangles[ii_rect_active_index].tipo_elemento
		
		event ue_rect_selected(ii_rect_active_index)
	end if
end if
end event

event rbuttondown;event clicked(xpos, ypos, row, dwo)
end event

