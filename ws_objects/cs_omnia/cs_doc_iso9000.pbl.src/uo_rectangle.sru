﻿$PBExportHeader$uo_rectangle.sru
forward
global type uo_rectangle from nonvisualobject
end type
end forward

global type uo_rectangle from nonvisualobject
end type
global uo_rectangle uo_rectangle

type variables
public integer X
public integer Y
public integer Width
public integer Height

public string Name
public string Visible

// Dati per il salvataggio su DB
public int Prog_Collegamento
public long Prog_Elemento
public string Tipo_Elemento
public string Valore = ""
end variables

on uo_rectangle.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_rectangle.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

