﻿$PBExportHeader$w_controllo_doc_scaduti.srw
forward
global type w_controllo_doc_scaduti from window
end type
type st_1 from statictext within w_controllo_doc_scaduti
end type
end forward

global type w_controllo_doc_scaduti from window
integer width = 2309
integer height = 372
windowtype windowtype = response!
long backcolor = 15780518
string pointer = "HourGlass!"
event ue_controllo ( )
st_1 st_1
end type
global w_controllo_doc_scaduti w_controllo_doc_scaduti

event ue_controllo();long ll_ret, ll_prima, ll_dopo
uo_documenti_iso9000 luo_documenti

ll_prima = cpu()

luo_documenti = CREATE uo_documenti_iso9000
ll_ret = luo_documenti.uof_controllo_doc_scaduti()
choose case ll_ret
	case 0
		commit;
	case -1
		rollback;
		g_mb.messagebox("Omnia",luo_documenti.is_errore)
end choose
destroy luo_documenti

do while (cpu() - ll_prima) < 2000 
loop

close(this)

end event

event open;this.x = s_cs_xx.parametri.parametro_d_1 + 100
this.y = s_cs_xx.parametri.parametro_d_2 + 200
postevent("ue_controllo")
end event

on w_controllo_doc_scaduti.create
this.st_1=create st_1
this.Control[]={this.st_1}
end on

on w_controllo_doc_scaduti.destroy
destroy(this.st_1)
end on

type st_1 from statictext within w_controllo_doc_scaduti
integer x = 9
integer y = 4
integer width = 2277
integer height = 280
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 15780518
string text = "Verifica documenti non validati in corso ..... Attendere !!!"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;close(PARENT)
end event

