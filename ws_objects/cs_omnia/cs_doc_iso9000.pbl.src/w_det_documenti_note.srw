﻿$PBExportHeader$w_det_documenti_note.srw
forward
global type w_det_documenti_note from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_det_documenti_note
end type
type dw_documenti_note_det from uo_cs_xx_dw within w_det_documenti_note
end type
type dw_documenti_note_lista from uo_cs_xx_dw within w_det_documenti_note
end type
end forward

global type w_det_documenti_note from w_cs_xx_principale
integer width = 2327
integer height = 1408
string title = "Note - Documento"
boolean maxbox = false
boolean resizable = false
cb_documento cb_documento
dw_documenti_note_det dw_documenti_note_det
dw_documenti_note_lista dw_documenti_note_lista
end type
global w_det_documenti_note w_det_documenti_note

type variables
long il_anno, il_num, il_prog
end variables

on w_det_documenti_note.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.dw_documenti_note_det=create dw_documenti_note_det
this.dw_documenti_note_lista=create dw_documenti_note_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.dw_documenti_note_det
this.Control[iCurrent+3]=this.dw_documenti_note_lista
end on

on w_det_documenti_note.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.dw_documenti_note_det)
destroy(this.dw_documenti_note_lista)
end on

event open;call super::open;iuo_dw_main = dw_documenti_note_lista

dw_documenti_note_lista.change_dw_current()

dw_documenti_note_lista.set_dw_options(sqlca, &
													i_openparm, &
													c_default, &
													c_default)

dw_documenti_note_det.set_dw_options(sqlca, &
												 dw_documenti_note_lista, &
												 c_sharedata + c_scrollparent, &
												 c_default)
end event

type cb_documento from commandbutton within w_det_documenti_note
integer x = 1920
integer y = 560
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documento"
end type

event clicked;blob   lbl_null

string ls_nota, ls_db

long	 ll_row, ll_risposta

n_tran sqlcb


setnull(lbl_null)

ll_row = dw_documenti_note_lista.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare una nota prima di continuare",exclamation!)
	return -1
end if

ls_nota = dw_documenti_note_lista.getitemstring(ll_row,"cod_nota")

if isnull(ls_nota) then
	g_mb.messagebox("OMNIA","Selezionare una nota prima di continuare",exclamation!)
	return -1
end if

ls_db = f_db()

if ls_db = "MSSQL" then
	
	ll_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_documenti_note
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :il_anno and
	           num_registrazione = :il_num and
	           progressivo = :il_prog and
				  cod_nota = :ls_nota
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_documenti_note
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :il_anno and
	           num_registrazione = :il_num and
	           progressivo = :il_prog and
				  cod_nota = :ls_nota;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		ll_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob det_documenti_note
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :il_anno and
					  num_registrazione = :il_num and
					  progressivo = :il_prog and
					  cod_nota = :ls_nota
		using      sqlcb;
				
		if sqlcb.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA","Errore in aggiornamento blob: " + sqlcb.sqlerrtext,stopsign!)
		end if	

		destroy sqlcb;
		
	else
		
	   updateblob det_documenti_note
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :il_anno and
					  num_registrazione = :il_num and
					  progressivo = :il_prog and
					  cod_nota = :ls_nota;
		
		if sqlca.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA","Errore in aggiornamento blob: " + sqlca.sqlerrtext,stopsign!)
		end if	
		
	end if
				
   update det_documenti_note
   set    path_documento = :s_cs_xx.parametri.parametro_s_1
   where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :il_anno and
	       num_registrazione = :il_num and
	       progressivo = :il_prog and
		  	 cod_nota = :ls_nota;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in aggiornamento path documento: " + sqlca.sqlerrtext,stopsign!)
	end if
	
   commit;
	
end if
end event

type dw_documenti_note_det from uo_cs_xx_dw within w_det_documenti_note
integer x = 23
integer y = 660
integer width = 2263
integer height = 640
integer taborder = 20
string dataobject = "d_documenti_note_det"
end type

type dw_documenti_note_lista from uo_cs_xx_dw within w_det_documenti_note
integer x = 23
integer y = 20
integer width = 1874
integer height = 620
integer taborder = 10
string dataobject = "d_documenti_note_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_versione, ll_revisione

il_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_registrazione")

il_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_registrazione")

il_prog = i_parentdw.getitemnumber(i_parentdw.getrow(),"progressivo")

ll_versione = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_versione")

ll_revisione = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_revisione")

parent.title = "Note - Documento " + string(il_anno) + "/" + string(il_num) + &
					" " + string(ll_versione) + "." + string(ll_revisione)
					
retrieve(s_cs_xx.cod_azienda,il_anno,il_num,il_prog)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_i,"anno_registrazione")) then
		setitem(ll_i,"anno_registrazione",il_anno)
	end if
	
	if isnull(getitemnumber(ll_i,"num_registrazione")) then
		setitem(ll_i,"num_registrazione",il_num)
	end if
	
	if isnull(getitemnumber(ll_i,"progressivo")) then
		setitem(ll_i,"progressivo",il_prog)
	end if
	
next
end event

event pcd_modify;call super::pcd_modify;cb_documento.enabled = false
end event

event pcd_new;call super::pcd_new;cb_documento.enabled = false
end event

event pcd_view;call super::pcd_view;cb_documento.enabled = true
end event

event updateend;call super::updateend;cb_documento.enabled = true
end event

