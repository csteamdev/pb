﻿$PBExportHeader$w_det_documenti_ole.srw
forward
global type w_det_documenti_ole from w_ole_v2
end type
end forward

global type w_det_documenti_ole from w_ole_v2
end type
global w_det_documenti_ole w_det_documenti_ole

type variables

end variables

forward prototypes
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name)
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public subroutine wf_load_documents ()
end prototypes

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);delete from det_documenti_note
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :astr_data.progressivo and
	cod_nota = :astr_data.cod_nota;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name);update det_documenti_note
set des_blob = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :astr_data.anno_registrazione and
	num_registrazione = :astr_data.num_registrazione and
	progressivo = :astr_data.progressivo and
	cod_nota = :astr_data.cod_nota;
	
if sqlca.sqlcode <> 0 then
	// errore durante il salvataggio
	return false
else
	// tutto ok
	return true
end if
end function

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);string ls_cod_nota
str_ole lstr_data

// Calcolo progressivo
select max(cod_nota)
into :ls_cod_nota
from det_documenti_note
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :il_progressivo;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il calcolo del progressivo per il file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(ls_cod_nota) or ls_cod_nota = "" then
	ls_cod_nota = "0"
end if

ls_cod_nota = string(integer(ls_cod_nota) + 1)
	
// Inserimento nuova riga
insert into det_documenti_note (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	progressivo,
	cod_nota,
	des_nota,
	prog_mimetype
)
values (
	:s_cs_xx.cod_azienda,
	:il_anno_registrazione,
	:il_num_registrazione,
	:il_progressivo,
	:ls_cod_nota,
	:as_file_name,
	:al_prog_mimetype
);

if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Aggiornamento campo blob
updateblob det_documenti_note
set blob = :ab_file_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :il_progressivo and
	cod_nota = :ls_cod_nota;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Creo struttura e aggiongo icona
lstr_data.anno_registrazione = il_anno_registrazione
lstr_data.num_registrazione = il_num_registrazione
lstr_data.progressivo = il_progressivo
lstr_data.cod_nota = ls_cod_nota
lstr_data.prog_mimetype = al_prog_mimetype
// aggiungere informazioni alla struttura se necessario

wf_add_document(as_file_name, lstr_data)

return true
end function

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);selectblob blob
into :ab_file_blob
from det_documenti_note
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :astr_data.progressivo and
	cod_nota = :astr_data.cod_nota;

if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la lettura del documento " + alv_item.label + ". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public subroutine wf_load_documents ();string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

ls_sql = "SELECT &
	cod_nota, &
	des_nota, &
	progressivo, &
	prog_mimetype &
FROM det_documenti_note &
WHERE &
	cod_azienda ='" + s_cs_xx.cod_azienda +"' and &
	anno_registrazione =" + string(il_anno_registrazione) + " and &
	num_registrazione =" + string(il_num_registrazione) + " and &
	progressivo=" + string(il_progressivo) + " &
ORDER BY &
	cod_azienda ASC, &
	anno_registrazione ASC, &
	num_registrazione ASC "
	
if not f_crea_datastore(ref lds_documenti, ls_sql) then
	return 
end if

li_rows = lds_documenti.retrieve()
for li_i = 1 to li_rows
	lstr_data.anno_registrazione = il_anno_registrazione
	lstr_data.num_registrazione = il_num_registrazione
	lstr_data.progressivo = lds_documenti.getitemnumber(li_i, "progressivo")
	lstr_data.cod_nota = lds_documenti.getitemstring(li_i, "cod_nota")
	lstr_data.prog_mimetype = lds_documenti.getitemnumber(li_i, "prog_mimetype")
	
	wf_add_document(lds_documenti.getitemstring(li_i, "des_nota"), lstr_data)
next
end subroutine

on w_det_documenti_ole.create
call super::create
end on

on w_det_documenti_ole.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;il_anno_registrazione = s_cs_xx.parametri.parametro_ul_1
il_num_registrazione = s_cs_xx.parametri.parametro_ul_2
il_progressivo = s_cs_xx.parametri.parametro_ul_3
end event

type st_loading from w_ole_v2`st_loading within w_det_documenti_ole
end type

type cb_cancella from w_ole_v2`cb_cancella within w_det_documenti_ole
end type

type lv_documenti from w_ole_v2`lv_documenti within w_det_documenti_ole
end type

type ddlb_style from w_ole_v2`ddlb_style within w_det_documenti_ole
end type

type cb_sfoglia from w_ole_v2`cb_sfoglia within w_det_documenti_ole
end type

