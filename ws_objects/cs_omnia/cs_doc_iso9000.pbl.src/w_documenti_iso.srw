﻿$PBExportHeader$w_documenti_iso.srw
forward
global type w_documenti_iso from w_cs_xx_principale
end type
type tab_1 from tab within w_documenti_iso
end type
type firme from userobject within tab_1
end type
type dw_lista_docs from datawindow within firme
end type
type dw_firme from uo_cs_xx_dw within firme
end type
type firme from userobject within tab_1
dw_lista_docs dw_lista_docs
dw_firme dw_firme
end type
type conservazione from userobject within tab_1
end type
type dw_conservazione from uo_cs_xx_dw within conservazione
end type
type conservazione from userobject within tab_1
dw_conservazione dw_conservazione
end type
type ole from userobject within tab_1
end type
type ole_1 from olecontrol within ole
end type
type ole from userobject within tab_1
ole_1 ole_1
end type
type tab_1 from tab within w_documenti_iso
firme firme
conservazione conservazione
ole ole
end type
type dw_filtro from uo_std_dw within w_documenti_iso
end type
type dw_folder_search from u_folder within w_documenti_iso
end type
type tv_1 from treeview within w_documenti_iso
end type
end forward

global type w_documenti_iso from w_cs_xx_principale
integer width = 4215
integer height = 2264
string title = "Documenti ISO 9000"
event pcd_elimina ( )
event pcd_rinomina_nodo ( )
event pcd_nuovo_documento ( )
event pcd_nuovo_menu ( )
event pcd_verifica_documento ( )
event pcd_approva_documento ( )
event pcd_valida_documento ( )
event pcd_revisiona_documento ( )
event pcd_note_documento ( )
event pcd_nuovo_documento_template ( )
event pcd_cambia_documento ( )
event pcd_privilegi_documento ( )
event pcd_liste_distribuzione ( )
event pcd_elimina_liste_distribuzione ( )
event pcd_notifiche_lettura ( )
event pcd_paragrafi_iso ( )
event pcd_elimina_revisione ( )
tab_1 tab_1
dw_filtro dw_filtro
dw_folder_search dw_folder_search
tv_1 tv_1
end type
global w_documenti_iso w_documenti_iso

type variables
private:
	string is_sql
	string is_cod_mansionario
	string is_livello_mansionario			// livello di documento a cui ha accesso il mansionario  (A,B,C,%)
	string is_visualizza_doc				// N=ultime revisione oppure S=storico
	string is_livello_doc					// livello di documentazione attualmente selezionato (A,B,C)
	
	string is_desktop_folder
	string is_temp_folder
	string is_window_title
	string is_AZP		//parametro per capire se sei Sintexcal
	
	//long il_livello_corrente
	
	boolean ib_clear_tree = false
	boolean ib_force_close = false		// Se tru indica che l'utente non può aprire la finestra
	boolean ib_autorizzazione  = false, ib_approvazione  = false, ib_validazione = false
	boolean ib_elimina_doc = false, ib_modifica_doc = false, ib_lettura_doc = false
	boolean ib_supervisore = false
	boolean ib_can_save_doc = false	// Se true vuol dire che hai fatto doppio clic sull'ole e i privilegi e le condizioni del documento ti consentono poi di salvarlo
	
	s_cs_xx_parametri istr_can_save_doc, istr_parametri_vuoto
	string is_tipo_log_cambio_firma = "MODFIRMA"
	
	// ICONE
	int BOOK_ICON
	int FOLDER_ICON
	int FOLDER_OPEN_ICON
	int MOD_ICON
	int REVISION_ICON
	
	str_doc_iso istr_doc_iso
	m_doc_iso_documento im_menu_document
	uo_documenti_iso9000 iuo_documenti_iso9000
	
	boolean ib_includi_cod_padre = true
	long il_current_tv_handle
end variables

forward prototypes
public subroutine wf_controlla_mansionario ()
public subroutine wf_init_tree_icons ()
public subroutine wf_imposta_ricerca ()
public function treeviewitem wf_new_treeviewitem (boolean ab_children, integer ai_icon)
public subroutine wf_cancella_tree ()
public function long wf_inserisci_figli (long al_handle, long al_padre)
public function long wf_inserisci_revisioni (long al_handle, long al_anno_registrazione, long al_num_registrazione)
public subroutine wf_set_dw_options ()
public function integer wf_nuovo_menu ()
public function boolean wf_trova_max (ref long al_max, ref long al_max_registrazione)
public function integer wf_crea_privilegi (long fl_anno_registrazione_padre, long fl_num_registrazione_padre, long fl_anno_registrazione_figlio, long fl_num_registrazione_figlio)
public function integer wf_elimina ()
public function integer wf_nuovo_documento ()
public function integer wf_rinomina_nodo ()
public function boolean wf_get_currentitem (ref treeviewitem atvi_item)
public function boolean wf_get_currentitem (long al_handle, ref treeviewitem atvi_item)
public subroutine wf_set_tree_menu ()
public function boolean wf_get_file_info (string as_file, ref string as_file_name, ref string as_file_extension, ref integer ai_mimetype)
public function integer wf_verifica_documento ()
public subroutine wf_approva_documento ()
public subroutine wf_valida_documento ()
public subroutine wf_revisione_documento ()
public subroutine wf_invia_email (long al_anno_registrazione, long al_num_registrazione)
public subroutine wf_note_documento ()
public subroutine wf_controlla_documenti_scaduti ()
public subroutine wf_nuovo_documento_template ()
public function integer wf_controlla_se_approvato (long fl_anno, long fl_num, long fl_prog)
public subroutine wf_anteprima_file ()
public function integer wf_can_save_doc (string fs_errore)
public subroutine wf_abilita_modifica_firme ()
public function integer wf_elimina_revisione ()
public subroutine wf_pulisci_nodo (ref long al_parent_handle)
public subroutine wf_refresh_parent_node (long al_children_node)
end prototypes

event pcd_elimina();wf_elimina()
end event

event pcd_rinomina_nodo();wf_rinomina_nodo()
end event

event pcd_nuovo_documento();wf_nuovo_documento()
end event

event pcd_nuovo_menu();wf_nuovo_menu()
end event

event pcd_verifica_documento();wf_verifica_documento()
end event

event pcd_approva_documento();wf_approva_documento()
end event

event pcd_valida_documento();wf_valida_documento()
end event

event pcd_revisiona_documento();
wf_revisione_documento()
end event

event pcd_note_documento();wf_note_documento()
end event

event pcd_nuovo_documento_template();/*
*/

wf_nuovo_documento_template()
end event

event pcd_cambia_documento();string ls_file, ls_path, ls_file_name, ls_file_ext
int li_prog_mimetype
long ll_ret, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, ll_blocca
blob lbb_blob_documento
integer li_risposta
transaction sqlcb

ll_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")

ll_blocca = wf_controlla_se_approvato(ll_anno_registrazione, ll_num_registrazione, ll_progressivo)
if ll_blocca = 1 then
	//blocca perchè il doc è già approvato	
	g_mb.messagebox("Omnia","Impossibile proseguire: documento già approvato!",Exclamation!)
	return
	
elseif ll_blocca = -1 then
	return
end if

ll_ret = getfileopenname("Cerca Oggetto",ls_path, ls_file, "DOC","Microsoft Word (*.DOC),*.DOC," + &
  		                                                         + "Microsoft Excel (*.XLS),*.XLS," + &
  		                                                         + "Tutti i files (*.*),*.*")
if ll_ret < 1 then return

f_file_to_blob(ls_path,lbb_blob_documento)

wf_get_file_info(ls_path, ls_file_name, ls_file_ext, li_prog_mimetype)

updateblob det_documenti
set blob_documento = :lbb_blob_documento
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione and
	num_registrazione = :ll_num_registrazione and
	progressivo = :ll_progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in salvataggio del documento: ", sqlca)
	rollback;
	return
end if

update det_documenti
set prog_mimetype = :li_prog_mimetype
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione and
	num_registrazione = :ll_num_registrazione and
	progressivo = :ll_progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore l'aggiornamento del mimetype: ", sqlca)
	rollback;
	return
end if

commit;
end event

event pcd_privilegi_documento();long ll_handle_corrente
treeviewitem tvi_campo
str_doc_iso lstr_data

if tab_1.firme.dw_firme.rowcount() > 0 then
	
	if not wf_get_currentitem(tvi_campo) then
		g_mb.error("Attenzione! E' necessario selezionare almeno un elemento.")
		return
	end if
	
	lstr_data = tvi_campo.data
	s_cs_xx.parametri.parametro_d_1 = lstr_data.anno_registrazione
	s_cs_xx.parametri.parametro_d_2 = lstr_data.num_registrazione
	window_open(w_tes_documenti_privilegi, 0)
end if

end event

event pcd_liste_distribuzione();long ll_progr_lista_distribuzione,ll_progressivo,ll_num_registrazione
integer li_anno_registrazione

li_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")

select prog_lista_distribuzione
into :ll_progr_lista_distribuzione
from det_documenti
where
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_registrazione=:li_anno_registrazione and
	num_registrazione=:ll_num_registrazione and
	progressivo=:ll_progressivo;

if isnull(ll_progr_lista_distribuzione) or ll_progr_lista_distribuzione=0 then
	
	ll_progr_lista_distribuzione = f_progressivo_lista_dist()
	
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_distribuzione	
	s_cs_xx.parametri.parametro_s_1 = ""

	window_open(w_seleziona_lista_dist,0)
	
	if s_cs_xx.parametri.parametro_d_1 <> -1 then
		
		update det_documenti
		set prog_lista_distribuzione=:ll_progr_lista_distribuzione
		where
			cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_registrazione and
			num_registrazione=:ll_num_registrazione and
			progressivo=:ll_progressivo;
			
	end if

else
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_distribuzione
		
	s_cs_xx.parametri.parametro_s_10 = string(li_anno_registrazione)
	s_cs_xx.parametri.parametro_s_11 = string(ll_num_registrazione)
	s_cs_xx.parametri.parametro_s_12 = string(ll_progressivo)
	
	window_open(w_liste_dist_det_doc,0)
	
	s_cs_xx.parametri.parametro_s_10 = ""
	s_cs_xx.parametri.parametro_s_11 = ""
	s_cs_xx.parametri.parametro_s_12 = ""

end if
end event

event pcd_elimina_liste_distribuzione();long    ll_progressivo,ll_progr_lista_distribuzione,ll_risposta,ll_num_registrazione
integer li_anno_registrazione
string  ls_path_doc_from ,ls_flag_approvazione, ls_flag_autorizzazione, & 
		  ls_flag_validazione, ls_flag_lettura,ls_flag_modifica,ls_flag_elimina 

SELECT flag_approvazione,
		 flag_autorizzazione,
		 flag_validazione,
		 flag_lettura,
		 flag_modifica,
		 flag_elimina
INTO   :ls_flag_approvazione, 
	    :ls_flag_autorizzazione,
	    :ls_flag_validazione, 
	    :ls_flag_lettura,
	    :ls_flag_modifica, 		 
	    :ls_flag_elimina &
FROM mansionari
WHERE
	cod_azienda=:s_cs_xx.cod_azienda AND
	cod_utente =:s_cs_xx.cod_utente;

if ls_flag_modifica="N" then 
	g_mb.messagebox("Omnia","Non si possiede il privilegio di modifica del documento, pertanto non è possibile eliminare la lista di distribuzione associata",exclamation!)
end if

ll_risposta = g_mb.messagebox("Omnia", "Sei sicuro di voler eliminare la lista di distribuzione associata?",  Exclamation!, YesNo!, 2)

if ll_risposta=2 then return

if isnull(tab_1.firme.dw_firme.getitemstring(1, "nome_documento")) then return

li_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")


select prog_lista_distribuzione
into   :ll_progr_lista_distribuzione
from   det_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    progressivo=:ll_progressivo;

if isnull(ll_progr_lista_distribuzione) or ll_progr_lista_distribuzione=0 then
	g_mb.messagebox("OMNIA","Non risulta associata al documento alcuna lista di distribuzione", StopSign!)
	return
end if

update det_documenti
set    prog_lista_distribuzione=0
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    progressivo=:ll_progressivo;

choose case sqlca.sqlcode
	case 100
		g_mb.messagebox("OMNIA","Impossibile disassociare il numero lista: non trovo il documento "+string(ll_progressivo), StopSign!)
	case 0
		
	case else
		g_mb.messagebox("OMNIA","Errore durante la disassociazione lista di distribuzione. Errore SQL: "+sqlca.sqlerrtext, StopSign!)
end choose

if f_elimina_liste_dist_doc(ll_progr_lista_distribuzione) = 0 then
	g_mb.messagebox("OMNIA", "Operazione eseguita con successo", Information!)
end if
end event

event pcd_notifiche_lettura();s_cs_xx_parametri lstr_param

lstr_param.parametro_ul_1 = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
lstr_param.parametro_ul_2 = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
lstr_param.parametro_ul_3 = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")

openWithParm(w_notifiche_documenti, lstr_param)
end event

event pcd_paragrafi_iso();if tab_1.firme.dw_firme.rowcount() > 0 then
	window_open_parm(w_tes_documenti_paragrafi_iso, -1, tab_1.firme.dw_firme)
end if
end event

event pcd_elimina_revisione();wf_elimina_revisione()
end event

public subroutine wf_controlla_mansionario ();string ls_flag_supervisore

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	
	select 
		cod_resp_divisione, 
		autoriz_liv_doc, 
		flag_supervisore
	into   
		:is_cod_mansionario,
		:is_livello_mansionario, 
		:ls_flag_supervisore
	from mansionari
	where  
		cod_azienda=:s_cs_xx.cod_azienda and
		cod_utente=:s_cs_xx.cod_utente;

	if sqlca.sqlcode < 0 then
		ib_force_close = true
		g_mb.error("Errore in ricerca mansionario dell'utente: accesso negato!", sqlca)
		postevent("pc_close")
	elseif sqlca.sqlcode = 100 or isnull(is_cod_mansionario) then
		ib_force_close = true
		g_mb.error("L'utente non ha alcun mansionario: accesso negato!")
		postevent("pc_close")
	end if

	ib_supervisore = ls_flag_supervisore = "S"

	if is_livello_mansionario = "" or isnull(is_livello_mansionario) then is_livello_mansionario = "A"

	dw_filtro.setitem(1, "flag_livello", is_livello_mansionario)
	
	is_visualizza_doc = "N"

	is_livello_doc = is_livello_mansionario
end if
end subroutine

public subroutine wf_init_tree_icons ();/**
 * stefanop
 * 22/09/2011
 *
 * Carico icone albero
 **/
 
tv_1.deletepictures()

BOOK_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "book.ico")
FOLDER_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview/cartella.png")
MOD_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview/documento_blu.png")
REVISION_ICON = tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview/documento_grigio.png")
end subroutine

public subroutine wf_imposta_ricerca ();/**
 * Imposta Ricerca
 **/
 
string ls_flag_tipo_documeto, ls_tes, ls_flag_livello
long ll_anno_registrazione, ll_num_registrazione

dw_filtro.accepttext()

ls_tes = "tes_documenti."
is_sql = " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' "

ls_flag_tipo_documeto = dw_filtro.getitemstring(1, "flag_tipo_documeto")
is_visualizza_doc = dw_filtro.getitemstring(1, "flag_visualizza")
ls_flag_livello = dw_filtro.getitemstring(1, "flag_livello")
ll_anno_registrazione = dw_filtro.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = dw_filtro.getitemnumber(1, "num_registrazione")

// Controllo livello mansionario
if ls_flag_livello = "T" then
	is_livello_doc = is_livello_mansionario
else 
	is_livello_doc = ls_flag_livello
end if

if is_livello_doc < is_livello_mansionario then
	is_livello_doc = is_livello_mansionario
	g_mb.show("Utente non abilitato a questo livello di documentazione!")
	return
end if
// ---

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 1900 and not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	ib_includi_cod_padre = false
	is_sql += " AND tes_documenti.anno_registrazione = " + string(ll_anno_registrazione) 
	is_sql += " AND tes_documenti.num_registrazione = " + string(ll_num_registrazione) 
end if
if ls_flag_tipo_documeto = "D" then
	is_sql += " AND (tes_documenti.tipo_elemento = 'M' OR (tes_documenti.tipo_elemento = 'D' AND tes_documenti.cod_padre <> 0)) "
elseif ls_flag_tipo_documeto = "P" then
	is_sql += " AND tes_documenti.tipo_elemento = 'D' AND tes_documenti.cod_padre = 0 "
end if

wf_cancella_tree()

//wf_aggiungi_nodo_documento()
wf_inserisci_figli(0, 0)

tv_1.setfocus()

dw_folder_search.fu_SelectTab(2)
end subroutine

public function treeviewitem wf_new_treeviewitem (boolean ab_children, integer ai_icon);treeviewitem ltvi_campo

ltvi_campo.expanded = false
ltvi_campo.selected = false
ltvi_campo.children = ab_children	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ltvi_campo.pictureindex = ai_icon		
ltvi_campo.selectedpictureindex = ai_icon		
//ltvi_campo.overlaypictureindex = 4

return ltvi_campo
end function

public subroutine wf_cancella_tree ();ib_clear_tree = true
tv_1.deleteitem(0)
ib_clear_tree = false
end subroutine

public function long wf_inserisci_figli (long al_handle, long al_padre);/**
 * Inserisco i figli del nodo
 **/

string ls_sql, ls_tipo_elemento, ls_flag_blocco, ls_des_elemento
long ll_rows, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_cod_padre, ll_cod_figlio, ll_num_documenti, &
		ll_test_figlio
datastore lds_store
treeviewitem ltvi_item

ls_sql = "SELECT &
				tes_documenti.anno_registrazione, &
				tes_documenti.num_registrazione, &
				tes_documenti.cod_padre, &
				tes_documenti.cod_figlio, &
				tes_documenti.tipo_elemento, &
				tes_documenti.flag_blocco, &
				tes_documenti.des_elemento &
			FROM tes_documenti " + is_sql
	
if ib_includi_cod_padre then
	ls_sql += " AND tes_documenti.cod_padre = " + string(al_padre)
else
	// Se arrivo qui è perchè ho inserito un numero all'interno della ricerca
	// e quindi devo escludere il padre dalla ricerca
	ib_includi_cod_padre = true
end if

ls_sql += " ORDER BY tes_documenti.des_elemento ASC"

if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_figli)")
	return -1
end if

ll_rows = lds_store.retrieve()

for ll_i = 1 to ll_rows
	
	ll_anno_registrazione = lds_store.getitemnumber(ll_i, 1)
	ll_num_registrazione = lds_store.getitemnumber(ll_i, 2)
	ll_cod_padre = lds_store.getitemnumber(ll_i, 3)
	ll_cod_figlio = lds_store.getitemnumber(ll_i, 4)
	ls_tipo_elemento = lds_store.getitemstring(ll_i, 5)
	ls_flag_blocco = lds_store.getitemstring(ll_i, 6)
	ls_des_elemento = lds_store.getitemstring(ll_i, 7)
	
	// modifica Michela : devo fare in modo che ignori gli elementi di tipo "X".
	// spiego meglio: all'inizio non c'era la gestione dei processi. ogni processo aveva delle fasi ed ogni fase poteva
	// essere costituita da delle liste di distribuzione o da documenti. non è stata creata una tabella dei documenti, bensi
	// è stata creata una testata con tipo_elemento = X per indicare le testate dei processi.
	if ls_tipo_elemento = "X" then
		continue
	elseif ls_tipo_elemento = "D" then	
		ll_num_documenti = 0
		
		// conto le revisioni
		select count(*)
		into :ll_num_documenti
		from det_documenti
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
		
		if ll_num_documenti = 0 or isnull(ll_num_documenti) then continue
				
	end if
	// fine modifica
	
	str_doc_iso lstr_data
	lstr_data.tipo_elemento = ls_tipo_elemento
	lstr_data.codice = ll_cod_figlio
	lstr_data.cod_padre = ll_cod_padre
	lstr_data.anno_registrazione = ll_anno_registrazione
	lstr_data.num_registrazione = ll_num_registrazione
	
	if isnull(ls_des_elemento) then ls_des_elemento = ""
	
	if lstr_data.tipo_elemento = 'M' then
		ltvi_item = wf_new_treeviewitem(true, FOLDER_ICON)
	else
		ltvi_item = wf_new_treeviewitem(true, MOD_ICON)
	end if
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_des_elemento
	
	tv_1.insertitemlast(al_handle, ltvi_item)
	
next

return ll_rows
end function

public function long wf_inserisci_revisioni (long al_handle, long al_anno_registrazione, long al_num_registrazione);/**
 * Inserisco i figli del nodo
 **/

string ls_sql, ls_des_elemento, ls_nome_documento
long ll_rows, ll_i, ll_progressivo, ll_num_versione, ll_num_revisione
datastore lds_store
treeviewitem ltvi_item

ls_sql = "SELECT " + &
				"det_documenti.progressivo, " + &
				"det_documenti.num_versione, " + &
				"det_documenti.num_revisione, " + &
				"det_documenti.nome_documento " + &
			"FROM det_documenti WHERE cod_azienda ='" + s_cs_xx.cod_azienda + "' "
	
ls_sql += " AND det_documenti.anno_registrazione = " + string(al_anno_registrazione)
ls_sql += " AND det_documenti.num_registrazione = " + string(al_num_registrazione)
//ls_sql += " AND  det_documenti.flag_storico = '" + is_visualizza_doc + "' "
ls_sql += " ORDER BY det_documenti.num_versione DESC, det_documenti.num_revisione DESC"

if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore (wf_inserisci_revisioni)")
	return -1
end if

ll_rows = lds_store.retrieve()

// visualizzo solo le ultime revisioni
//if ll_rows > 1 and is_visualizza_doc = "S" then ll_rows = 1
if ll_rows > 1 and is_visualizza_doc = "N" then ll_rows = 1

for ll_i = 1 to ll_rows
	
	ll_progressivo = lds_store.getitemnumber(ll_i, 1)
	ll_num_versione = lds_store.getitemnumber(ll_i, 2)
	ll_num_revisione = lds_store.getitemnumber(ll_i, 3)
	ls_nome_documento = lds_store.getitemstring(ll_i, 4)
		
	str_doc_iso lstr_data
	lstr_data.tipo_elemento = "R"
	lstr_data.anno_registrazione = al_anno_registrazione
	lstr_data.num_registrazione = al_num_registrazione
	lstr_data.progressivo = ll_progressivo
	
	ltvi_item = wf_new_treeviewitem(false, REVISION_ICON)
		
	ls_des_elemento = ""
	if not isnull(ls_nome_documento) and ls_nome_documento <> "" then ls_des_elemento =  ls_nome_documento + " - "
	
	ls_des_elemento += string(ll_num_versione) + "." + string(ll_num_revisione)
	ls_des_elemento += " - " + string(al_anno_registrazione) + "/" + string(al_num_registrazione) + "/" + string(ll_progressivo)
	
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_des_elemento
	
	tv_1.insertitemlast(al_handle, ltvi_item)
	
next

return ll_rows
end function

public subroutine wf_set_dw_options ();/**
 * Controllo i permessi del mansionario e imposto le dw
 **/
 
unsignedlong lu_privilegi
uo_documenti_iso9000 luo_documenti_iso9000

// lettura dei privilegi degli utenti
luo_documenti_iso9000 = CREATE uo_documenti_iso9000

ib_autorizzazione = luo_documenti_iso9000.is_autorizzazione = "S"
ib_approvazione = luo_documenti_iso9000.is_approvazione = "S"
ib_validazione = luo_documenti_iso9000.is_validazione = "S"

ib_modifica_doc = luo_documenti_iso9000.is_modifica = "S"
ib_lettura_doc = luo_documenti_iso9000.is_lettura = "S"
ib_elimina_doc = luo_documenti_iso9000.is_elimina = "S"

tab_1.ole.enabled = ib_lettura_doc

// proprietà DW
lu_privilegi = c_nonew

if luo_documenti_iso9000.is_modifica = "S" then 
	lu_privilegi = lu_privilegi +  c_modifyok
else
	lu_privilegi = lu_privilegi +  c_nomodify
end if

if luo_documenti_iso9000.is_elimina = "S" then 
	lu_privilegi = lu_privilegi +  c_deleteok
else
	lu_privilegi = lu_privilegi +  c_nodelete
end if

tab_1.firme.dw_firme.set_dw_options(sqlca, pcca.null_object, lu_privilegi, c_NoCursorRowFocusRect)
tab_1.conservazione.dw_conservazione.set_dw_options(sqlca, tab_1.firme.dw_firme, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = tab_1.firme.dw_firme


destroy luo_documenti_iso9000
end subroutine

public function integer wf_nuovo_menu ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_max,ll_ultimo_handle, & 
	  ll_num_registrazione,ll_max_num_registrazione,ll_anno_registrazione_padre,ll_num_registrazione_padre
integer li_anno_registrazione
string ls_tipo
str_doc_iso lstr_data
treeviewitem tvi_campo

li_anno_registrazione = f_anno_esercizio()

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente < 0 then 
	ll_handle_corrente = 0
else

	tv_1.GetItem (ll_handle_corrente, tvi_campo )
	
	lstr_data = tvi_campo.data
	
	ll_cod_padre = lstr_data.cod_padre
	ll_cod_figlio = lstr_data.codice
	ls_tipo = lstr_data.tipo_elemento
	ll_anno_registrazione_padre = lstr_data.anno_registrazione
	ll_num_registrazione_padre = lstr_data.num_registrazione
	
	if ls_tipo = 'D' then
		g_mb.messagebox("Omnia","Attenzione! I documenti non possono contenere al loro interno dei menù!",stopsign!)
		return 0
	end if

	lstr_data.cod_padre = ll_cod_figlio
end if

if not wf_trova_max(ll_max,ll_max_num_registrazione) then
	return -1
end if

lstr_data.codice = ll_max
lstr_data.tipo_elemento = 'M'
lstr_data.anno_registrazione=li_anno_registrazione
lstr_data.num_registrazione=ll_max_num_registrazione

insert into tes_documenti (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	cod_padre,
	cod_figlio,
	tipo_elemento,
	flag_blocco,
	des_elemento
) values (
	:s_cs_xx.cod_azienda,
	:lstr_data.anno_registrazione,
	:lstr_data.num_registrazione,
	:lstr_data.cod_padre,
	:lstr_data.codice,
	'M',
	'N',
	'');

if sqlca.sqlcode< 0 then
	g_mb.error("Errore durante l'inserimento del nuovo menu", sqlca)
	return -1
end if

tvi_campo.itemhandle = ll_handle_corrente 
tvi_campo.data = lstr_data
tvi_campo.label = ""
tvi_campo.pictureindex = 2
tvi_campo.selectedpictureindex = 2
tvi_campo.overlaypictureindex = 2
ll_ultimo_handle = tv_1.InsertItemLast ( ll_handle_corrente, tvi_campo )
tv_1.selectItem(ll_ultimo_handle)
tv_1.EditLabel (ll_ultimo_handle )

wf_crea_privilegi(ll_anno_registrazione_padre, ll_num_registrazione_padre, lstr_data.anno_registrazione, lstr_data.num_registrazione) 

commit;

return 0
end function

public function boolean wf_trova_max (ref long al_max, ref long al_max_registrazione);long ll_max_padre,ll_max_figlio,ll_max_num_registrazione
integer li_anno_registrazione

li_anno_registrazione = f_anno_esercizio()

select max(cod_padre)
into :ll_max_padre
from tes_documenti
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.error("Errore sul DB.", sqlca)
	return false
end if

if isnull(ll_max_padre) then ll_max_padre = 0

select max(cod_figlio)
into :ll_max_figlio
from tes_documenti
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.error("Errore sul DB", sqlca)
	return false
end if

if isnull(ll_max_figlio) then ll_max_figlio = 0

if ll_max_padre >= ll_max_figlio then
	al_max = ll_max_padre
else
	al_max = ll_max_figlio
end if

if al_max=0 then 
	al_max=1
else
	al_max++
end if

select max(num_registrazione)
into :ll_max_num_registrazione
from tes_documenti
where cod_azienda =:s_cs_xx.cod_azienda
and anno_registrazione =:li_anno_registrazione;

if sqlca.sqlcode<0 then
	g_mb.error("Errore sul DB: ", sqlca)
	return false
end if

if isnull(ll_max_num_registrazione) or ll_max_num_registrazione = 0 then 
	ll_max_num_registrazione=1
else
	ll_max_num_registrazione++
end if

al_max_registrazione = ll_max_num_registrazione

return true
end function

public function integer wf_crea_privilegi (long fl_anno_registrazione_padre, long fl_num_registrazione_padre, long fl_anno_registrazione_figlio, long fl_num_registrazione_figlio);string ls_cod_gruppo_privilegio[],ls_cod_mansionario[]
long ll_t,ll_i


ll_t = 1

declare r_tes_documenti_privilegi cursor for
select cod_gruppo_privilegio,
		 cod_mansionario
from   tes_documenti_privilegi
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fl_anno_registrazione_padre
and    num_registrazione=:fl_num_registrazione_padre;


open r_tes_documenti_privilegi;

do while 1=1
	fetch r_tes_documenti_privilegi
	into  :ls_cod_gruppo_privilegio[ll_t],
			:ls_cod_mansionario[ll_t];
			
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
	if sqlca.sqlcode =100 then exit
	
	ll_t++
loop

close r_tes_documenti_privilegi;


for ll_i = 1 to upperbound(ls_cod_mansionario[]) -1
	insert into tes_documenti_privilegi
	(cod_azienda,
	 anno_registrazione,
	 num_registrazione,
	 cod_gruppo_privilegio,
	 cod_mansionario)
	values
	(:s_cs_xx.cod_azienda,
	 :fl_anno_registrazione_figlio,
	 :fl_num_registrazione_figlio,
	 :ls_cod_gruppo_privilegio[ll_i],
	 :ls_cod_mansionario[ll_i]);
	 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
next

return 0
end function

public function integer wf_elimina ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle,ll_num_registrazione
integer li_anno_registrazione
string ls_tipo
str_doc_iso lstr_doc_iso
treeviewitem tvi_campo

if not ib_elimina_doc  then
	g_mb.messagebox("Omnia","Il mansionario " + is_cod_mansionario + " non è in possesso del privilegio di ELIMINAZIONE.")
	return 0
end if

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","E' necessario selezionare almeno un elemento.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

lstr_doc_iso = tvi_campo.data

ll_cod_padre = lstr_doc_iso.cod_padre
ll_cod_figlio = lstr_doc_iso.codice
li_anno_registrazione = lstr_doc_iso.anno_registrazione
ll_num_registrazione = lstr_doc_iso.num_registrazione
ls_tipo = lstr_doc_iso.tipo_elemento

if not g_mb.confirm("Omnia","Sei Sicuro di voler cancellare il documento " + tvi_campo.label + " ?",2)  then return 0

if ll_cod_padre=0 and ll_cod_figlio = 0 then 
	g_mb.messagebox("Omnia","Non puoi eliminare l'elemento di origine dei documenti di qualità!",stopsign!)
	return 0
end if

select cod_figlio
into   :ll_test_figlio
from   tes_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_padre=:ll_cod_figlio;

if sqlca.sqlcode <0 and (isnull(ll_test_figlio)) then
	g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

if sqlca.sqlcode = 100 then
	
	delete from det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella det_documenti. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from tes_documenti_privilegi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella tes_documenti_privilegi. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from tes_documenti_paragrafi_iso
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella tes_documenti_paragrafi_iso. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from collegamenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella tes_documenti_paragrafi_iso. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from tes_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_padre=:ll_cod_padre
	and    cod_figlio=:ll_cod_figlio;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if
	tv_1.deleteitem(ll_handle_corrente)
		
else
	g_mb.messagebox("Omnia","Non è possibile cancellare l'elemento poichè contiene altri elementi!" + &
				  "~nPADRE = " + string(ll_cod_padre) + "~nFIGLIO = " + string(ll_cod_figlio),stopsign!)
end if

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
tv_1.selectItem(ll_handle_corrente)

commit;
return 0

end function

public function integer wf_nuovo_documento ();string ls_file_path, ls_file_name_full, ls_file_name, ls_file_ext, &
		 ls_emesso, ls_autorizzato, ls_approvato, ls_validato, ls_db, ls_reverse, ls_tipof, &
		 ls_nome_documento,ls_des_documento,ls_cod_area_aziendale,ls_file,ls_livello_documento, ls_firma_emesso_da
int li_result, li_anno_registrazione, li_prog_mimetype
long ll_num_registrazione, ll_cod_figlio, ll_max_progressivo, ll_new_handle, ll_num_versione, ll_num_revisione, &
		ll_valido_per
datetime ldt_emesso, ldt_autorizzato, ldt_approvato, ldt_validato
treeviewitem ltvi_item, ltvi_new_item
str_doc_iso lstr_data
blob lblb_blob

if not ib_modifica_doc then
	g_mb.messagebox("Omnia","Il mansionario " + is_cod_mansionario + " non è in possesso del privilegio di modifica / nuovo sulla documentazione")
	return 0
end if

if not wf_get_currentitem(ltvi_item) then
	return 0
end if

lstr_data = ltvi_item.data

if lstr_data.tipo_elemento = 'D' or lstr_data.tipo_elemento = "R" then
	g_mb.show("Attenzione! I documenti non possono contenere al loro interno altri documenti!")
	return 0
end if

// Apro finestra selezione
window_open(w_nuovo_doc,0)

choose case s_cs_xx.parametri.parametro_i_1
	case -1
		return -1
	case 100
		ls_emesso = s_cs_xx.parametri.parametro_s_6
		ldt_emesso = s_cs_xx.parametri.parametro_data_1
		ls_autorizzato = s_cs_xx.parametri.parametro_s_7
		ldt_autorizzato = s_cs_xx.parametri.parametro_data_2
		ls_approvato = s_cs_xx.parametri.parametro_s_8
		ldt_approvato = s_cs_xx.parametri.parametro_data_3
		ls_validato = s_cs_xx.parametri.parametro_s_9
		ldt_validato = s_cs_xx.parametri.parametro_data_4
	case 0
		ls_emesso = is_cod_mansionario
		ldt_emesso = datetime(today(), 00:00:00)
		setnull(ls_autorizzato)
		setnull(ldt_autorizzato)
		setnull(ls_approvato)
		setnull(ldt_approvato)
		setnull(ls_validato)
		setnull(ldt_validato)
end choose

ls_nome_documento = s_cs_xx.parametri.parametro_s_1
ls_des_documento = s_cs_xx.parametri.parametro_s_2
ls_cod_area_aziendale = s_cs_xx.parametri.parametro_s_3 
ls_file = s_cs_xx.parametri.parametro_s_4 
ls_livello_documento = s_cs_xx.parametri.parametro_s_5
ll_num_versione = s_cs_xx.parametri.parametro_ul_1  // =long(em_edizione.text)
ll_num_revisione = s_cs_xx.parametri.parametro_ul_2 // =long(em_revisione.text)
ll_valido_per = s_cs_xx.parametri.parametro_ul_3
li_anno_registrazione = f_anno_esercizio()

wf_trova_max(ll_cod_figlio, ll_num_registrazione)

lstr_data.cod_padre = lstr_data.codice
lstr_data.codice = ll_cod_figlio
lstr_data.tipo_elemento = "D"
lstr_data.anno_registrazione=li_anno_registrazione
lstr_data.num_registrazione=ll_num_registrazione

insert into tes_documenti (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	cod_padre,
	cod_figlio,
	tipo_elemento,
	flag_blocco,
	des_elemento
) values (
	:s_cs_xx.cod_azienda,
	:lstr_data.anno_registrazione,
	:lstr_data.num_registrazione,
	:lstr_data.cod_padre,
	:lstr_data.codice,
	'D',
	'N',
	:ls_nome_documento);

if sqlca.sqlcode< 0 then
	g_mb.error("Errore sul DB:" , sqlca)
	rollback;
	return 0
end if

// dettaglio
select max(progressivo)
into :ll_max_progressivo
from det_documenti
where 
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_registrazione=:lstr_data.anno_registrazione and
	num_registrazione=:lstr_data.num_registrazione;

if sqlca.sqlcode< 0 then
	g_mb.error("Si è verificato un errore durante la ricerca del max di progressivo su det_documenti.", sqlca)
	rollback;
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_max_progressivo) then
	ll_max_progressivo = 0
end if

ll_max_progressivo++

wf_get_file_info(ls_file, ls_file_name, ls_file_ext, li_prog_mimetype)

//Giulio 23/02/2012: inserita firma per approvazione, verifica, validazione, emissione.

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_firma_emesso_da = luo_mansionario.uof_return_firma(ls_emesso)

insert into det_documenti (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	progressivo,
	resp_conservazione_doc,
	num_versione,
	num_revisione,
	emesso_da,
	emesso_il,
	firma_emesso_da,
	autorizzato_da,
	autorizzato_il,
	approvato_da,
	approvato_il,
	validato_da,
	validato_il,
	valido_per,
	modifiche,
	livello_documento,
	flag_uso,
	flag_recuperato,
	nome_documento,
	des_documento,
	prog_ordinamento_livello,
	periodo_conserv_unita_tempo,
	periodo_conserv,
	flag_doc_registraz_qualita,
	flag_catalogazione,
	cod_area_aziendale,
	flag_storico,
	prog_mimetype
) values (
	:s_cs_xx.cod_azienda,
	:lstr_data.anno_registrazione,
	:lstr_data.num_registrazione,
	:ll_max_progressivo,
	null,
	:ll_num_versione,
	:ll_num_revisione,
	:ls_emesso,
	:ldt_emesso,
	:ls_firma_emesso_da,
	:ls_autorizzato,
	:ldt_autorizzato,
	:ls_approvato,
	:ldt_approvato,
	:ls_validato,
	:ldt_validato,
	:ll_valido_per,
	null,
	:ls_livello_documento,
	'G',
	'N',
	:ls_nome_documento,
	:ls_des_documento,
	null,
	'G',
	0,
	'N',
	'A',
	:ls_cod_area_aziendale,
	'N',
	:li_prog_mimetype);
		 
if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore durante l'insert in det_documenti.", sqlca)
	rollback;
	return 0
end if

// carico il blob
if f_file_to_blob(ls_file, lblb_blob) < 0 then 
	g_mb.error("Errore durante la conversione del file per il salvataggio sul database.")
	rollback;
	return -1
end if

updateblob det_documenti
set blob_documento = :lblb_blob
where 
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_registrazione=:lstr_data.anno_registrazione and
	num_registrazione =:lstr_data.num_registrazione and
	progressivo=:ll_max_progressivo;

if sqlca.sqlcode <0 then
	g_mb.error("Il salvataggio dell'oggetto nel documento ha provocato il seguente errore: ", sqlca)
	rollback;
	return 0
end if

// aggiungo il nodo 
ltvi_new_item = wf_new_treeviewitem(true, MOD_ICON)

ltvi_new_item.data = lstr_data
ltvi_new_item.label = ls_nome_documento
ll_new_handle = tv_1.InsertItemLast (ltvi_item.ItemHandle, ltvi_new_item )
tv_1.selectItem(ll_new_handle)

return 0
end function

public function integer wf_rinomina_nodo ();long ll_handle_corrente

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

tv_1.editlabel(ll_handle_corrente)

return 0
end function

public function boolean wf_get_currentitem (ref treeviewitem atvi_item);/**
 * Recupero l'item corrente dell'albero
 **/
 
 
long ll_handle

return wf_get_currentitem(tv_1.finditem(CurrentTreeItem!, 0), atvi_item)
end function

public function boolean wf_get_currentitem (long al_handle, ref treeviewitem atvi_item);/**
 * Recupero l'item corrente dell'albero
 **/
 
 if al_handle > 0 then
	
	tv_1.getitem(al_handle, atvi_item)
	return true
	
else
	return false
end if
end function

public subroutine wf_set_tree_menu ();/**
 * Creo i menu di istanza e ne imposto le proprietà
 **/
 
im_menu_document = create m_doc_iso_documento


// -- stefanop 11/10/10: Mostro il pulsante notifiche solo se abilitato
string ls_test
select flag
into :ls_test
from	parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'IDL';
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la lettura del flag IDL.", sqlca)
elseif sqlca.sqlcode = 0 and ls_test = "S" then
	im_menu_document.m_notifichelettura.visible = true
	im_menu_document.m_2.visible = true //separatore
end if
// ----
end subroutine

public function boolean wf_get_file_info (string as_file, ref string as_file_name, ref string as_file_extension, ref integer ai_mimetype);/**
 * Recupera tutte le informazioni relative al file
 **/

int li_pos

// recupero ultima posizione del . per identificare l'estensione
li_pos = LastPos(as_file, ".")

if li_pos < 1 then
	g_mb.error("Impossibile identificare l'estensione del file selezionato.")
	return false
end if

as_file_name = left(as_file, li_pos - 1)
as_file_extension = mid(as_file, li_pos + 1)

if isnull(as_file_extension) or as_file_extension = "" then
	g_mb.error("L'estensione del file risulta essere vuota.")
	return false
end if

// recupero Mimetype
select prog_mimetype
into :ai_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda
and estensione = :as_file_extension;

if sqlca.sqlcode< 0 then
	g_mb.error("Si è verificato un errore durante la ricerca del mimetype.", sqlca)
	return false
elseif sqlca.sqlcode = 100 then
	g_mb.error("L'estensione (" + as_file_extension + ") usata dal file non è presente nel database.")
	return false
end if

return true
end function

public function integer wf_verifica_documento ();/**
 * Verifico documento
 **/
 
string ls_livello
long   ll_anno_registrazione, ll_num_registrazione, ll_progressivo, ll_count

ll_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")

if iuo_documenti_iso9000.uof_autorizza_documento(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	commit;
	g_mb.messagebox("OMNIA","Verifica eseguita con successo !")
else
	rollback;
	g_mb.messagebox("OMNIA", iuo_documenti_iso9000.is_errore,stopsign!)
	return -1
end if

tab_1.firme.dw_firme.change_dw_current()

tab_1.firme.dw_firme.triggerevent("pcd_retrieve")

// invio email
if s_cs_xx.num_livello_mail > 0 then
	
	select count(*)
	into   :ll_count
	from   tes_documenti_privilegi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 cod_gruppo_privilegio in (select cod_gruppo_privilegio
			 									from tab_gruppi_privilegi
												where cod_azienda = :s_cs_xx.cod_azienda and
														flag_approvazione = 'S');
														
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in controllo gruppi privilegi: " + sqlca.sqlerrtext)
		ll_count = 0
	end if
	
	if ll_count > 0 then
	
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " cod_resp_divisione in (select cod_mansionario from tes_documenti_privilegi " + &
													 "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + &
													 string(ll_anno_registrazione) + " and num_registrazione = " + &
													 string(ll_num_registrazione) + " and cod_gruppo_privilegio in " + &
													 "(select cod_gruppo_privilegio from tab_gruppi_privilegi where cod_azienda = '" + &
													 s_cs_xx.cod_azienda + "' and flag_approvazione = 'S') ) "
		s_cs_xx.parametri.parametro_s_5 = "Verifica del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata verificato il documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai gruppi privilegi?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	else
	
		ls_livello = tab_1.firme.dw_firme.getitemstring(1, "livello_documento")
		
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " flag_approvazione = 'S' and autoriz_liv_doc <= '" + ls_livello + "' "
		s_cs_xx.parametri.parametro_s_5 = "Verifica del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata verificato il documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari con privilegio di approvazione?"
		s_cs_xx.parametri.parametro_s_8 = ""
	
	end if
	
	openwithparm(w_invio_messaggi,0)
		
end if
end function

public subroutine wf_approva_documento ();long ll_anno_registrazione, ll_num_registrazione, ll_progressivo

ll_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")

// cacchio fa boh!
//wf_salvafile_nodelete()

if iuo_documenti_iso9000.uof_approva_documento(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	commit;
	g_mb.messagebox("OMNIA","Approvazione eseguita con successo !")
else
	rollback;
	g_mb.messagebox("OMNIA",iuo_documenti_iso9000.is_errore,stopsign!)
end if

tab_1.firme.dw_firme.change_dw_current()
tab_1.firme.dw_firme.triggerevent("pcd_retrieve")
end subroutine

public subroutine wf_valida_documento ();long ll_anno_registrazione, ll_num_registrazione, ll_progressivo


ll_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")

if iuo_documenti_iso9000.uof_valida_documento(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	commit;
	g_mb.messagebox("OMNIA","Validazione eseguita con successo !")
else
	rollback;
	g_mb.messagebox("OMNIA",iuo_documenti_iso9000.is_errore,stopsign!)
end if


tab_1.firme.dw_firme.change_dw_current()
tab_1.firme.dw_firme.postevent("pcd_retrieve")


end subroutine

public subroutine wf_revisione_documento ();string ls_livello
long   ll_anno_registrazione, ll_num_registrazione, ll_progressivo, ll_count, ll_handle_corrente
string ls_AZP
treeviewitem ltvi_item

ll_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(1, "num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(1, "progressivo")

if iuo_documenti_iso9000.uof_nuova_revisione(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	commit;
	g_mb.messagebox("OMNIA","Nuova Revisione eseguita con successo.")
else
	rollback;
	g_mb.messagebox("OMNIA", iuo_documenti_iso9000.is_errore,stopsign!)
	return
end if

tab_1.firme.dw_firme.triggerevent("pcd_retrieve")

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//31/05/2012 chiesto da Barbara Guidoboni  - personalizzazione per Sintexcal
//parametro per abilitare il pulsante ALTRI DESTINATARI quando invierai il messaggio di avviso in seguito a revisione documento
select stringa
into   :ls_AZP
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull( ls_AZP ) and ls_AZP<>"" then
	//sintexcal
	s_cs_xx.parametri.parametro_d_4 = 9999
else
	//no sintexcal
	setnull(s_cs_xx.parametri.parametro_d_4)
end if
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

if s_cs_xx.num_livello_mail > 0 then
else
	setnull(s_cs_xx.parametri.parametro_d_4)
end if

wf_invia_email(ll_anno_registrazione, ll_num_registrazione)

// Recupero il padre e forzo un refresh per caricare i nuovi dati
ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
ll_handle_corrente = tv_1.FindItem(ParentTreeItem!, ll_handle_corrente)
tv_1.GetItem (ll_handle_corrente, ltvi_item)
ltvi_item.Expanded = false
ltvi_item.ExpandedOnce = false
wf_pulisci_nodo(ll_handle_corrente)
tv_1.setitem(ll_handle_corrente, ltvi_item)
tv_1.ExpandItem(ll_handle_corrente)
tv_1.selectitem(ll_handle_corrente)




end subroutine

public subroutine wf_invia_email (long al_anno_registrazione, long al_num_registrazione);/**
 * Invio email alle liste distribuzione
 **/
 
string ls_livello
long ll_count 

if s_cs_xx.num_livello_mail > 0 then
	
	select count(*)
	into   :ll_count
	from   tes_documenti_privilegi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :al_anno_registrazione and
			 num_registrazione = :al_num_registrazione and
			 cod_gruppo_privilegio in (select cod_gruppo_privilegio
			 									from tab_gruppi_privilegi
												where cod_azienda = :s_cs_xx.cod_azienda and
														flag_verifica = 'S');
														
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in controllo gruppi privilegi: " + sqlca.sqlerrtext)
		ll_count = 0
	end if
	
	if ll_count > 0 then
	
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " cod_resp_divisione in (select cod_mansionario from tes_documenti_privilegi " + &
													 "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + &
													 string(al_anno_registrazione) + " and num_registrazione = " + &
													 string(al_num_registrazione) + " and cod_gruppo_privilegio in " + &
													 "(select cod_gruppo_privilegio from tab_gruppi_privilegi where cod_azienda = '" + &
													 s_cs_xx.cod_azienda + "' and flag_verifica = 'S') ) "
		s_cs_xx.parametri.parametro_s_5 = "Nuova revisione del documento " + string(al_anno_registrazione) + "/" + string(al_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata emessa una nuova revisione del documento " + string(al_anno_registrazione) + "/" + string(al_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai gruppi privilegi?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	else
	
		ls_livello = tab_1.firme.dw_firme.getitemstring(1, "livello_documento")
			
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " flag_autorizzazione = 'S' and autoriz_liv_doc <= '" + ls_livello + "' "
		s_cs_xx.parametri.parametro_s_5 = "Nuova revisione del documento " + string(al_anno_registrazione) + "/" + string(al_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata emessa una nuova revisione del documento " + string(al_anno_registrazione) + "/" + string(al_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari con privilegio di verifica?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	end if
		
	openwithparm(w_invio_messaggi,0)
		
end if
end subroutine

public subroutine wf_note_documento ();long ll_row


ll_row = tab_1.firme.dw_firme.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare una revisione di un documento prima di continuare",exclamation!)
	return 
end if

if isnull(tab_1.firme.dw_firme.getitemnumber(ll_row,"anno_registrazione")) or &
	isnull(tab_1.firme.dw_firme.getitemnumber(ll_row,"num_registrazione")) or &
	isnull(tab_1.firme.dw_firme.getitemnumber(ll_row,"progressivo")) then
	g_mb.messagebox("OMNIA","Selezionare una revisione di un documento prima di continuare",exclamation!)
	return
end if

//window_open_parm(w_det_documenti_note,-1,dw_det_documenti_lista)
s_cs_xx.parametri.parametro_ul_1 = tab_1.firme.dw_firme.getitemnumber(ll_row,"anno_registrazione")
s_cs_xx.parametri.parametro_ul_2 = tab_1.firme.dw_firme.getitemnumber(ll_row,"num_registrazione")
s_cs_xx.parametri.parametro_ul_3 = tab_1.firme.dw_firme.getitemnumber(ll_row,"progressivo")
open(w_det_documenti_ole)
end subroutine

public subroutine wf_controlla_documenti_scaduti ();/**
 * Donato 
 * 18-09-2008
 *
 * Modifica su richiesta di Marco
 * Ho insrito un parametro multiaziendale chiamato "CDS" di tipo stringa (controlla documenti scaduti)
 * Se presente in tabella e con valore "N" allora non esegue il controllo, altrimenti 
 * (se non c'è oppure vale "S") il controllo viene effettuato
 **/
 
string ls_cds
int li_return

if ib_force_close then return

select stringa
into :ls_cds
from parametri
where 
	flag_parametro = 'S' and
	cod_parametro = 'CDS';
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la ricerca del parametro multiaziendale 'CDS'", sqlca)
	return
elseif sqlca.sqlcode = 100 or ls_cds = "S" then
	// controllo documenti
	
	title = is_window_title + " - Controllo documenti scaduti..."
	li_return = iuo_documenti_iso9000.uof_controllo_doc_scaduti()
	title = is_window_title
	
	if li_return = 0 then
		commit;
	else
		rollback;
		g_mb.error(iuo_documenti_iso9000.is_errore)
	end if
end if
end subroutine

public subroutine wf_nuovo_documento_template ();/*

*/

end subroutine

public function integer wf_controlla_se_approvato (long fl_anno, long fl_num, long fl_prog);//Controlla se il documento corrente è stato approvato, in funzione anche del parametro aziendale BMD
//torna 0 se BMD è "N" o inesistente oppure  
//				BMD è "S" e il documento non è stato ancora approvato: NON BLOCCARE
//torna 1 se BMD è "S" e il documento è stato già approvato: BLOCCARE
// torna -1 se errore

string ls_flag_BMD, ls_autorizzato_da

//Leggi il parametro BMD (blocca modifica documento se approvato)
select flag
into :ls_flag_BMD
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'BMD' and flag_parametro = 'F';			
if sqlca.sqlcode = 0 then
else
	ls_flag_BMD = "N"
end if	

//leggi se il documento è approvato
select	approvato_da
into   	:ls_autorizzato_da
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno and
		 num_registrazione = :fl_num and
		 progressivo = :fl_prog;
		 
if sqlca.sqlcode < 0 then
	g_mb.error("Attenzione!","Errore in lettura parametri approvazione del documento. Operazione annullata. " + sqlca.sqlerrtext)
	return -1
end if

if ls_flag_BMD = "N" then
	//non bloccare
	return 0
else
	//BMD = "S"
	if ls_autorizzato_da <> "" and not isnull(ls_autorizzato_da) then
		//doc già approvato: blocca
		return 1
	else
		//doc non ancora approvato
		return 0
	end if
end if
end function

public subroutine wf_anteprima_file ();/**
 * stefanop
 * visualizzo anteprima file
 **/
 
string ls_file_name, ls_file_ext
long	ll_longblob
treeviewitem ltvi_item
str_doc_iso lstr_data
blob lblb_blob

if not wf_get_currentitem(ltvi_item) then return

lstr_data = ltvi_item.data

if lstr_data.tipo_elemento <> "R" then return

selectblob blob_documento
into :lblb_blob
from det_documenti
where 
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_registrazione=:lstr_data.anno_registrazione and
	num_registrazione =:lstr_data.num_registrazione and
	progressivo=:lstr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante il prelievo del documento dal database." , sqlca)
	return
end if

ll_longblob = lenA(lblb_blob)
	
select estensione
into :ls_file_ext
from tab_mimetype
join det_documenti on 
	det_documenti.cod_azienda = tab_mimetype.cod_azienda and
	det_documenti.prog_mimetype = tab_mimetype.prog_mimetype
where 
	det_documenti.cod_azienda=:s_cs_xx.cod_azienda and
	det_documenti.anno_registrazione=:lstr_data.anno_registrazione and
	det_documenti.num_registrazione =:lstr_data.num_registrazione and
	det_documenti.progressivo=:lstr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante il prelievo dell'estensione del file dal database." , sqlca)
	return
end if
	
ls_file_name = is_temp_folder + "cs_" + string(now(), "hh_mm_ss") + "." + ls_file_ext

f_blob_to_file(ls_file_name, lblb_blob)

tab_1.ole.ole_1.insertfile(ls_file_name)

filedelete(ls_file_name)

//updateblob det_documenti
//set blob_documento = :lblb_blob
//where 
//	cod_azienda=:s_cs_xx.cod_azienda and
//	anno_registrazione=:lstr_data.anno_registrazione and
//	num_registrazione =:lstr_data.num_registrazione and
//	progressivo=:ll_max_progressivo;
end subroutine

public function integer wf_can_save_doc (string fs_errore);string ls_file
integer li_anno_registrazione
long ll_num_registrazione, ll_progressivo
blob lblb_documento

ls_file = istr_can_save_doc.parametro_s_1

if g_mb.confirm( "Attenzione!", "Salvare eventuali modifiche al documento " + ls_file +"?", 2) then
	
	li_anno_registrazione = istr_can_save_doc.parametro_d_1_a[1]
	ll_num_registrazione = istr_can_save_doc.parametro_d_1_a[2]
	ll_progressivo = istr_can_save_doc.parametro_d_1_a[3]
	
	lblb_documento = tab_1.ole.ole_1.objectdata
	
	updateblob det_documenti
	set blob_documento = :lblb_documento
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_registrazione and
				num_registrazione =:ll_num_registrazione and
				progressivo=:ll_progressivo;
	
	if sqlca.sqlcode <0 then
		fs_errore = "Salvataggio del documento fallito: " + sqlca.sqlerrtext
		istr_can_save_doc = istr_parametri_vuoto
		ib_can_save_doc = false
		rollback;
		return -1
	end if
	
	commit;
	
end if

istr_can_save_doc = istr_parametri_vuoto
ib_can_save_doc = false

return 0
end function

public subroutine wf_abilita_modifica_firme ();//parametrizzata gestione modifiche firme e gestione visualizzazione dello storico delle modifiche
//per ora solo per Sintexcal: parametro aziendale AZP (se esiste ed è valorizzato)
string									ls_AZP, ls_ret, ls_visible_emesso, ls_visible_autorizzato, ls_visible_approvato, ls_firma, ls_visible_storico
long									ll_row, ll_num_registrazione, ll_progressivo
uo_documenti_iso9000			luo_documenti_iso9000
integer								li_ret, li_anno_registrazione
boolean								lb_modifica

//se non sei Sintexcal, esci subito, i pulsanti resterenno invisibili (default)...
//COMPORTAMENTO STANDARD, assenza parametro AZP
if is_AZP="" then return

//controlla se hai almeno una riga
ll_row = tab_1.firme.dw_firme.getrow()
if ll_row>0 then
else
	//no rows
	ls_visible_emesso = "0"
	ls_visible_autorizzato = "0"
	ls_visible_approvato = "0"
	ls_visible_storico="0"
	goto IMPOSTA
end if

//controlla se hai il privilegio di modifica
li_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(ll_row,"anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(ll_row,"num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(ll_row,"progressivo")

if li_anno_registrazione>0 and ll_num_registrazione>0 and ll_progressivo>0 then
else
	//no data
	ls_visible_emesso = "0"
	ls_visible_autorizzato = "0"
	ls_visible_approvato = "0"
	ls_visible_storico="0"
	goto IMPOSTA
end if

luo_documenti_iso9000 = CREATE uo_documenti_iso9000
li_ret = luo_documenti_iso9000.uof_privilegi_documento(li_anno_registrazione, ll_num_registrazione)
	
if li_ret < 0 then
	//me ne frego dell'errore, ma non do la possibilità di modificare
	//no rows
	ls_visible_emesso = "0"
	ls_visible_autorizzato = "0"
	ls_visible_approvato = "0"
	ls_visible_storico="0"
	goto IMPOSTA
else
	//trasferisco le autorizzazioni alle variabili booleane
	lb_modifica = luo_documenti_iso9000.is_modifica = "S"
end if

if not lb_modifica then
	ls_visible_emesso = "0"
	ls_visible_autorizzato = "0"
	ls_visible_approvato = "0"
	ls_visible_storico="0"
	goto IMPOSTA
end if


 //visualizza, ma solo se ci sono le rispettive firme -----------------------------------------------------------------------------
 ls_visible_emesso = "0"
 ls_firma =  tab_1.firme.dw_firme.getitemstring(ll_row, "emesso_da")
 if ls_firma<>"" and not isnull(ls_firma) then
	ls_visible_emesso = "1"
end if
 
 ls_visible_autorizzato = "0"
 ls_firma =  tab_1.firme.dw_firme.getitemstring(ll_row, "autorizzato_da")
  if ls_firma<>"" and not isnull(ls_firma) then
	ls_visible_autorizzato = "1"
end if
  
  ls_visible_approvato = "0"
  ls_firma =  tab_1.firme.dw_firme.getitemstring(ll_row, "approvato_da")
   if ls_firma<>"" and not isnull(ls_firma) then
	ls_visible_approvato = "1"
end if

//basta che anche uno dei 3 pulsanti sopra indicati sia visibile per rendere visibile anche il, puilsante dello storico
if pos(ls_visible_emesso+ls_visible_autorizzato+ls_visible_approvato, "1")>0 then
	ls_visible_storico = "1"
else
	ls_visible_storico="0"
end if

goto IMPOSTA



IMPOSTA:
ls_ret = tab_1.firme.dw_firme.modify("b_mod_emesso.visible=~"0~tif('"+ls_visible_emesso+"'='1',1,0)~"")
ls_ret = tab_1.firme.dw_firme.modify("b_mod_autorizzato.visible=~"0~tif('"+ls_visible_autorizzato+"'='1',1,0)~"")
ls_ret = tab_1.firme.dw_firme.modify("b_mod_approvato.visible=~"0~tif('"+ls_visible_approvato+"'='1',1,0)~"")
ls_ret = tab_1.firme.dw_firme.modify("b_mod_firme_storico.visible=~"0~tif('"+ls_visible_storico+"'='1',1,0)~"")




return




end subroutine

public function integer wf_elimina_revisione ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle,ll_num_registrazione, ll_progressivo, ll_count
integer li_anno_registrazione
string ls_tipo
str_doc_iso lstr_doc_iso
treeviewitem tvi_campo

if not ib_elimina_doc  then
	g_mb.messagebox("Omnia","Il mansionario " + is_cod_mansionario + " non è in possesso del privilegio di ELIMINAZIONE.")
	return 0
end if

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","E' necessario selezionare almeno un elemento.",stopsign!)
	return 0
end if

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
tv_1.GetItem (ll_handle_corrente, tvi_campo )

lstr_doc_iso = tvi_campo.data
ll_cod_padre = lstr_doc_iso.cod_padre
ll_cod_figlio = lstr_doc_iso.codice
li_anno_registrazione = lstr_doc_iso.anno_registrazione
ll_num_registrazione = lstr_doc_iso.num_registrazione
ll_progressivo = lstr_doc_iso.progressivo
ls_tipo = lstr_doc_iso.tipo_elemento

// controllo che esista almeno una revisione, altrimenti devo cancellare l'interno documento
select count(*)
into :ll_count
from det_documenti
where
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_registrazione=:li_anno_registrazione and
	num_registrazione=:ll_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo del numero di revisione per il documento selezionato", sqlca)
	return -1
elseif ll_count = 1 then
	g_mb.warning("Attenzione: il documento presenta una sola revisione; procedere con la cancellazione del documento!")
	return 0
end if

if not g_mb.confirm("Omnia", "Sei Sicuro di voler cancellare il documento " + tvi_campo.label + " ?" , 2)  then return 0

delete from det_documenti
where
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_registrazione=:li_anno_registrazione and
	num_registrazione=:ll_num_registrazione and
	progressivo =: ll_progressivo;

if sqlca.sqlcode <0 then
	g_mb.messagebox("Omnia","Errore in cancellazione su tabella det_documenti. " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

tv_1.deleteitem(ll_handle_corrente)
//ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
tv_1.selectItem(ll_handle_corrente)

commit;
return 0

end function

public subroutine wf_pulisci_nodo (ref long al_parent_handle);long ll_children_handle

ll_children_handle = tv_1.FindItem(ChildTreeItem!, al_parent_handle)

DO UNTIL ll_children_handle = -1
	//Loop through all items in the child, delete(level 2)
	tv_1.DeleteItem(ll_children_handle)
	
	ll_children_handle = tv_1.FindItem(ChildTreeItem!, al_parent_handle)
LOOP
end subroutine

public subroutine wf_refresh_parent_node (long al_children_node);/**
 * stefanop
 * 09/01/2013
 *
 * Recupera il nodo padre, cancella i figli e riesegue la retreieve
 **/
 
long ll_handle_corrente
treeviewitem ltvi_item

ll_handle_corrente = tv_1.FindItem(ParentTreeItem!, al_children_node)
tv_1.GetItem (ll_handle_corrente, ltvi_item)
ltvi_item.Expanded = false
ltvi_item.ExpandedOnce = false
wf_pulisci_nodo(ll_handle_corrente)
tv_1.ExpandItem(ll_handle_corrente)
tv_1.setitem(ll_handle_corrente, ltvi_item)
end subroutine

on w_documenti_iso.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.dw_filtro=create dw_filtro
this.dw_folder_search=create dw_folder_search
this.tv_1=create tv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.dw_filtro
this.Control[iCurrent+3]=this.dw_folder_search
this.Control[iCurrent+4]=this.tv_1
end on

on w_documenti_iso.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.dw_filtro)
destroy(this.dw_folder_search)
destroy(this.tv_1)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_empty[]


tab_1.firme.dw_lista_docs.settransobject(sqlca)

dw_filtro.insertrow(0)
dw_filtro.triggerevent("ue_proteggi_campi_insert")
wf_init_tree_icons()

lw_oggetti = lw_empty
lw_oggetti[1] = tv_1
dw_folder_search.fu_assigntab(2, "Documenti", lw_oggetti[])
lw_oggetti[1] = dw_filtro
dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

is_window_title = title
iuo_documenti_iso9000 = create uo_documenti_iso9000
is_desktop_folder = guo_functions.uof_get_user_desktop_folder()
is_temp_folder = guo_functions.uof_get_user_temp_folder()

// controllo se l'utente può aprire la finestra
wf_controlla_mansionario()

// se è stato richiesto di forzare la chiusura delle finestra non occorre fare altri controlli
if ib_force_close then return

// controllo permessi e imposto datawindow
wf_set_dw_options()

// controllo documenti scaduti
wf_controlla_documenti_scaduti()

// menu
wf_set_tree_menu()

//###############################
select stringa
into   :is_AZP
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull( is_AZP ) and is_AZP<>"" then
	//sintexcal
	is_AZP = "S"
else
	//non sintexcal
	is_AZP = ""
end if
//###############################
end event

event resize;/** TOLTO ANCESTOR **/
long ll_offset

dw_folder_search.move(20,20)
dw_folder_search.resize(dw_folder_search.width, newheight - 40)

ll_offset = dw_folder_search.x + dw_folder_search.width + 20
tab_1.move(ll_offset, 20)
tab_1.resize(newwidth - ll_offset - 20, newheight - 40)

tv_1.height = newheight - tv_1.y - 80
end event

event close;call super::close;destroy iuo_documenti_iso9000
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_1.conservazione.dw_conservazione,"resp_conservazione_doc",sqlca,&
                 "mansionari","cod_resp_divisione","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type tab_1 from tab within w_documenti_iso
event ue_resize pbm_size
integer x = 1801
integer y = 32
integer width = 2354
integer height = 1904
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
firme firme
conservazione conservazione
ole ole
end type

event ue_resize;/**
 * aggiorno dimensioni dw
 **/
 
 long			ll_offset, ll_height
 
 
//ll_offset = 30

//firme.dw_firme.resize(newwidth, newheight)
firme.dw_firme.resize(newwidth, (newheight / 4) * 3)

firme.dw_lista_docs.y = firme.dw_firme.y + firme.dw_firme.height
firme.dw_lista_docs.resize(newwidth, (newheight / 4) * 1)

conservazione.dw_conservazione.resize(newwidth, newheight)
ole.ole_1.resize(newwidth, newheight)
end event

on tab_1.create
this.firme=create firme
this.conservazione=create conservazione
this.ole=create ole
this.Control[]={this.firme,&
this.conservazione,&
this.ole}
end on

on tab_1.destroy
destroy(this.firme)
destroy(this.conservazione)
destroy(this.ole)
end on

event selectionchanged;// se mi sto spostando nell'ole carico il documento
if newindex = 3 then
	
	wf_anteprima_file()
	
end if
//
end event

type firme from userobject within tab_1
integer x = 18
integer y = 116
integer width = 2318
integer height = 1772
long backcolor = 12632256
string text = "Firme Elettroniche"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista_docs dw_lista_docs
dw_firme dw_firme
end type

on firme.create
this.dw_lista_docs=create dw_lista_docs
this.dw_firme=create dw_firme
this.Control[]={this.dw_lista_docs,&
this.dw_firme}
end on

on firme.destroy
destroy(this.dw_lista_docs)
destroy(this.dw_firme)
end on

type dw_lista_docs from datawindow within firme
integer y = 1524
integer width = 2025
integer height = 216
integer taborder = 20
string title = "none"
string dataobject = "d_det_documenti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_firme from uo_cs_xx_dw within firme
integer width = 2025
integer height = 1496
integer taborder = 30
string dataobject = "d_det_documenti_det_1_tv"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long			ll_errore
string			ls_storico


ll_errore = retrieve(s_cs_xx.cod_azienda, istr_doc_iso.anno_registrazione, istr_doc_iso.num_registrazione, istr_doc_iso.progressivo)

if ll_errore < 0 then
   pcca.error = c_fatal	
end if

ls_storico = dw_filtro.getitemstring(1, "flag_visualizza")

tab_1.firme.dw_lista_docs.retrieve(s_cs_xx.cod_azienda, istr_doc_iso.anno_registrazione, istr_doc_iso.num_registrazione, ls_storico)

//personalizzazione SINTEXCAL: abilitare la modifica delle firme dei documenti
//ma solo se si ha il privilegio di modifica del documento e se le firme ci sono già!!!!
wf_abilita_modifica_firme()
end event

event buttonclicked;call super::buttonclicked;integer				li_anno_registrazione
long					ll_num_registrazione, ll_progressivo
string					ls_azione, ls_messaggio, ls_nuova_firma, ls_colonna, ls_sql, ls_testo, ls_vecchia_firma, ls_des_nuova_firma, ls_anno_num_prog
datetime				ldt_data_firma, ldt_oggi, ldt_adesso
s_cs_xx_parametri lstr_parametri
uo_mansionario	luo_mansionario


if row>0 then
else
	return
end if

//controlla se hai il privilegio di modifica
li_anno_registrazione = tab_1.firme.dw_firme.getitemnumber(row,"anno_registrazione")
ll_num_registrazione = tab_1.firme.dw_firme.getitemnumber(row,"num_registrazione")
ll_progressivo = tab_1.firme.dw_firme.getitemnumber(row,"progressivo")


if li_anno_registrazione>0 and ll_num_registrazione>0 and ll_progressivo>0 then
else
	return
end if

ls_messaggio = "Modificare la firma "

choose case dwo.name
	case "b_mod_emesso"
		ls_azione = "E"			//modifica firma Emissione
		ls_messaggio += "di Emissione"
		ls_colonna = "emesso"
		
		select emesso_da
		into :ls_vecchia_firma
		from det_documenti
		where	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_registrazione and
					num_registrazione=:ll_num_registrazione and
					progressivo=:ll_progressivo;
		
	case "b_mod_autorizzato"
		ls_azione = "V"			//modifica firma Verifica
		ls_messaggio += "di Verifica"
		ls_colonna = "autorizzato"
		
		select autorizzato_da
		into :ls_vecchia_firma
		from det_documenti
		where	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_registrazione and
					num_registrazione=:ll_num_registrazione and
					progressivo=:ll_progressivo;
		
	case "b_mod_approvato"
		ls_azione = "A"			//modifica firma Approvazione
		ls_messaggio += "di Approvazione"
		ls_colonna = "approvato"
		
		select approvato_da
		into :ls_vecchia_firma
		from det_documenti
		where	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_registrazione and
					num_registrazione=:ll_num_registrazione and
					progressivo=:ll_progressivo;
		
	case "b_mod_firme_storico"
		ls_azione = "X"			//visualizza storico modifica firme
		ls_messaggio = "Visualizzare lo storico modifiche delle firme"
		
end choose

ls_messaggio += " del documento corrente?"

if not g_mb.confirm( "Attenzione!", ls_messaggio, 1) then
	return
end if

//apri l'unica finestra che fa fare una delle precedenti azioni
s_cs_xx.parametri.parametro_d_3_a[1] = li_anno_registrazione
s_cs_xx.parametri.parametro_d_3_a[2] = ll_num_registrazione
s_cs_xx.parametri.parametro_d_3_a[3] = ll_progressivo
s_cs_xx.parametri.parametro_s_10 = ls_azione
s_cs_xx.parametri.parametro_s_11 = is_tipo_log_cambio_firma
window_open(w_documenti_iso_cambia_firme, 0)

lstr_parametri = message.powerobjectparm

ls_nuova_firma = lstr_parametri.parametro_s_1

if ls_nuova_firma<>"" and not isnull(ls_nuova_firma) then
	
	ldt_data_firma = lstr_parametri.parametro_data_1
	ldt_data_firma = datetime(date(ldt_data_firma), 00:00:00)
	
	luo_mansionario = create uo_mansionario
	ls_des_nuova_firma = luo_mansionario.uof_return_firma(ls_nuova_firma)
	destroy luo_mansionario
	
	if isnull(ls_des_nuova_firma) or ls_des_nuova_firma="" then ls_des_nuova_firma = ls_nuova_firma
	
	ls_sql = "update det_documenti "+&
				"set " + ls_colonna + "_da='"+ls_nuova_firma+"', "+&
							ls_colonna+"_il='"+string(ldt_data_firma, s_cs_xx.db_funzioni.formato_data)+"', "+&
						"firma_"+ls_colonna+"_da='"+ls_des_nuova_firma+"' "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and  "+&
						  "anno_registrazione="+string(li_anno_registrazione)+" and "+&  
						  "num_registrazione="+string(ll_num_registrazione)+" and  "+&
						  "progressivo="+string(ll_progressivo) + " "    
	
	execute immediate :ls_sql;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in aggiornamento firma e data:" + sqlca.sqlerrtext)
		rollback;
		return
	else
		//Aggiornamento firma e data eseguito! Prosegui con la scrittura nel log sistema per lo storico
		
		ldt_oggi = datetime(today(), 00:00:00)
		ldt_adesso = datetime(date(1900, 1, 1),now())
		ls_testo = "Modificata firma '"+ls_colonna+"' da '"+ls_vecchia_firma + "' a '"+ls_nuova_firma+"'"
		ls_anno_num_prog = s_cs_xx.cod_azienda + "/" + string(li_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_progressivo)
		
		insert into log_sistema
			(	data_registrazione,
				ora_registrazione,
				utente,
				flag_tipo_log,
				db_sqlerrtext,
				messaggio)
		values (	:ldt_oggi,
					:ldt_adesso,
					:s_cs_xx.cod_utente,
					:is_tipo_log_cambio_firma,
					:ls_anno_num_prog,
					:ls_testo);
		
		if sqlca.sqlcode<0 then
			g_mb.error("Errore in aggiornamentostorico modifiche:" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		//se arrivi fin qui fai COMMIT
		commit;
		tab_1.firme.dw_firme.postevent("pcd_retrieve")
		return
		
	end if
end if

return


end event

type conservazione from userobject within tab_1
integer x = 18
integer y = 116
integer width = 2318
integer height = 1772
long backcolor = 12632256
string text = "Dati Conservazione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_conservazione dw_conservazione
end type

on conservazione.create
this.dw_conservazione=create dw_conservazione
this.Control[]={this.dw_conservazione}
end on

on conservazione.destroy
destroy(this.dw_conservazione)
end on

type dw_conservazione from uo_cs_xx_dw within conservazione
integer width = 1851
integer height = 1652
integer taborder = 11
string dataobject = "d_det_documenti_det_2"
boolean border = false
end type

type ole from userobject within tab_1
integer x = 18
integer y = 116
integer width = 2318
integer height = 1772
long backcolor = 12632256
string text = "Anteprima"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
ole_1 ole_1
end type

on ole.create
this.ole_1=create ole_1
this.Control[]={this.ole_1}
end on

on ole.destroy
destroy(this.ole_1)
end on

type ole_1 from olecontrol within ole
integer width = 1989
integer height = 1460
integer taborder = 30
boolean border = false
boolean focusrectangle = false
string binarykey = "w_documenti_iso.win"
omactivation activation = activateondoubleclick!
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
end type

event doubleclicked;integer							ll_blocca
long								ll_cur_row, ll_ret, ll_anno_registrazione, ll_num_registrazione, ll_progressivo
uo_documenti_iso9000		luo_documenti_iso9000
string								ls_nome_documento

//Donato 08/01/2009 controlla se c'è l'abilitazione del gruppo, in quanto tale autorizzazione
//comanda su quella del mansionario
ll_cur_row = tab_1.conservazione.dw_conservazione.getrow()

ole_1.activate(offsite!)
ib_can_save_doc = false
istr_can_save_doc = istr_parametri_vuoto

if ll_cur_row > 0 then
	ll_anno_registrazione = tab_1.conservazione.dw_conservazione.getitemnumber(ll_cur_row,"anno_registrazione")
	ll_num_registrazione = tab_1.conservazione.dw_conservazione.getitemnumber(ll_cur_row,"num_registrazione")
	ll_progressivo = tab_1.conservazione.dw_conservazione.getitemnumber(ll_cur_row,"progressivo")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 and ll_progressivo>0 then
	else
		ib_can_save_doc = false
		return
	end if
	
	//controlla se devo andare avanti, se approvato (e in base al parametro BMD) non devo andare avanti
	//torna 0 se devo andare avanti, 1 se devo boloccare la modifica
	ll_blocca = wf_controlla_se_approvato(ll_anno_registrazione, ll_num_registrazione, ll_progressivo)
	if ll_blocca < 0 then
		//messaggio già dato
		ib_can_save_doc = false
		return
		
	elseif ll_blocca = 1 then
		ib_can_save_doc = false
		g_mb.messagebox("Omnia","Attenzione, Documento Approvato!! Tutte le modifiche che verranno apportate al documento non saranno memorizzate.")	
		return
	end if
	
	luo_documenti_iso9000 = CREATE uo_documenti_iso9000
	ll_ret = luo_documenti_iso9000.uof_privilegi_documento(ll_anno_registrazione, ll_num_registrazione)
	
	if ll_ret < 0 then
		//is_errore è già valorizzato.....verrà dato il messaggio di errore opportuno
		ib_can_save_doc = false
		if luo_documenti_iso9000.is_errore <> "" and not isnull(luo_documenti_iso9000.is_errore) then &
									g_mb.error(luo_documenti_iso9000.is_errore)
		return -1
	else
		//trasferisco le autorizzazioni alle variabili b0oleane
		ib_autorizzazione = luo_documenti_iso9000.is_autorizzazione = "S"
		ib_approvazione = luo_documenti_iso9000.is_approvazione = "S"
		ib_validazione = luo_documenti_iso9000.is_validazione = "S"
		ib_modifica_doc = luo_documenti_iso9000.is_modifica = "S"
		ib_lettura_doc = luo_documenti_iso9000.is_lettura = "S"
		ib_elimina_doc = luo_documenti_iso9000.is_elimina = "S"
	
	end if
	//la funzione uof_privilegi_documento ha riscritto le variabili di istanza delle autorizzazioni varie in base al gruppo
end if

if (ib_lettura_doc and not ib_modifica_doc) then
	ib_can_save_doc = false
	g_mb.messagebox("Omnia","Attenzione !! Tutte le modifiche che verranno apportate al documento non saranno memorizzate.")
	return
	
else
	if ib_modifica_doc then
		ib_can_save_doc = true
		istr_can_save_doc.parametro_d_1_a[1] = ll_anno_registrazione
		istr_can_save_doc.parametro_d_1_a[2] = ll_num_registrazione
		istr_can_save_doc.parametro_d_1_a[3] = ll_progressivo
		
		select des_elemento
		into :ls_nome_documento
		from tes_documenti
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione;
		
		if ls_nome_documento="" or isnull(ls_nome_documento) then ls_nome_documento = "Senza Nome"

		istr_can_save_doc.parametro_s_1 = ls_nome_documento
	end if
end if
end event

type dw_filtro from uo_std_dw within w_documenti_iso
event ue_key pbm_dwnkey
event ue_reset ( )
integer x = 55
integer y = 128
integer width = 1669
integer height = 1764
integer taborder = 10
string title = "none"
string dataobject = "d_documenti_iso_ricerca"
boolean border = false
end type

event buttonclicked;choose case dwo.name
		
	case "b_cerca"
		wf_imposta_ricerca()
		
	case "b_ricerca_documento"
		guo_ricerca.uof_ricerca_documenti(dw_filtro, "")
		
	case "b_imp_preferito"
		//wf_memorizza_filtro()
		
	case "b_preferito"
		//wf_leggi_filtro()
		
	case "b_cerca_cliente"
		//guo_ricerca.uof_ricerca_cliente(dw_filtro)
		
	case "b_cerca_contatto"
		//guo_ricerca.uof_ricerca_contatto(dw_filtro, "cod_contatto")
		
end choose
end event

event itemchanged;
choose case dwo.name
	case "livello_1"
		setitem(getrow(),"livello_2", "N")
		setitem(getrow(),"livello_3", "N")
		settaborder("livello_2", 0)
		settaborder("livello_3", 0)
		if data <> "N" then
			settaborder("livello_2", 250)
		end if
		
	case "livello_2"
		setitem(getrow(),"livello_3", "N")
		settaborder("livello_3", 0)
		if data <> "N" then
			settaborder("livello_3", 260)
		end if
				
end choose

end event

type dw_folder_search from u_folder within w_documenti_iso
integer x = 18
integer y = 28
integer width = 1760
integer height = 1908
integer taborder = 10
end type

type tv_1 from treeview within w_documenti_iso
integer x = 55
integer y = 128
integer width = 1673
integer height = 1764
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean editlabels = true
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"Custom093!","C:\CS_70\framework\RISORSE\Foglio2_Dim_Orig.bmp","Start!","Picture!","Custom096!","Custom024!"}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event itempopulate;treeviewitem ltv_item
str_doc_iso lstr_doc_iso


if isnull(handle) or handle <= 0 or ib_clear_tree then
	return 0
end if

getitem(handle, ltv_item)

lstr_doc_iso = ltv_item.data

if lstr_doc_iso.tipo_elemento = 'D' then
	// Documento, inserisco revisioni
	if wf_inserisci_revisioni(handle, lstr_doc_iso.anno_registrazione, lstr_doc_iso.num_registrazione) = 0 then
		ltv_item.children = false
		tv_1.setitem(handle, ltv_item)
	end if
elseif lstr_doc_iso.tipo_elemento <> 'R' then
	// Cartelle ed altro, popolo figli
	if wf_inserisci_figli(handle, lstr_doc_iso.codice) = 0 then
			ltv_item.children = false
			tv_1.setitem(handle, ltv_item)
		end if
end if
end event

event rightclicked;treeviewitem ltvi_item
m_doc_iso_folder lm_menu_folder
str_doc_iso lstr_data

if handle < 1 then return

selectitem(handle)

if not wf_get_currentitem(handle, ltvi_item) then
	return
end if

lstr_data = ltvi_item.data

// Documento o Cartella?
if lstr_data.tipo_elemento = "R" then

	im_menu_document.m_verifica.enabled = isnull(tab_1.firme.dw_firme.getitemstring(1, "autorizzato_da")) and ib_autorizzazione
	im_menu_document.m_approva.enabled = isnull(tab_1.firme.dw_firme.getitemstring(1, "approvato_da")) and ib_approvazione
	im_menu_document.m_valida.enabled = isnull(tab_1.firme.dw_firme.getitemstring(1, "validato_da")) and ib_validazione
	im_menu_document.m_revisiona.enabled = ib_modifica_doc
	im_menu_document.m_cambiadocumento.enabled = ib_modifica_doc
	im_menu_document.m_paragrafiiso.enabled = ib_modifica_doc
	im_menu_document.m_privilegidocumento.enabled = ib_supervisore
	im_menu_document.m_eliminarevisione.enabled = ib_elimina_doc
	
	im_menu_document.popmenu(w_cs_xx_mdi.pointerx(), w_cs_xx_mdi.pointery())

else
	//m_doc_iso_documento
	lm_menu_folder = CREATE m_doc_iso_folder
	
	lm_menu_folder.m_modifica.enabled = ib_modifica_doc
	lm_menu_folder.m_elimina.enabled = ib_elimina_doc
	
	lm_menu_folder.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu_folder
end if

end event

event selectionchanged;treeviewitem ltv_item
str_doc_iso lstr_data
integer li_ret
string ls_errore

if isnull(newhandle) or newhandle <= 0 or ib_clear_tree then
	return 0
end if

il_current_tv_handle = newhandle

getitem(newhandle, ltv_item)
lstr_data = ltv_item.data

//verifico se sono in modalità modifica del documento (es. ho fatto doppio clic sull'ole dell'eventuale documento precedente)
//quindi provo ad estrapolare il documento dall'ole e salvarlo in tabella
if ib_can_save_doc then
	li_ret = wf_can_save_doc(ls_errore)
	
	if li_ret<0 then
		g_mb.error(ls_errore)
	end if
end if

if lstr_data.tipo_elemento = "R" then
	
	istr_doc_iso = lstr_data
	tab_1.firme.dw_firme.change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	// anteprima
	if tab_1.selectedtab = 3 then
		wf_anteprima_file()
	end if
end if
end event

event endlabeledit;long ll_handle_corrente,ll_cod_padre,ll_cod_figlio,ll_num_registrazione
string ls_tipo,ls_descrizione
integer li_anno_registrazione
str_doc_iso lstr_doc_iso
treeviewitem tvi_campo

if isnull(newtext) or newtext = "" then
	//g_mb.messagebox("Omnia","Inserire un nome per l'elemento!",stopsign!)
	//editlabel(handle)	
	return
end if

tv_1.GetItem (handle, tvi_campo )

lstr_doc_iso = tvi_campo.data

ll_cod_padre = lstr_doc_iso.cod_padre
ll_cod_figlio = lstr_doc_iso.codice
ls_tipo = lstr_doc_iso.tipo_elemento
//tvi_campo.label = newtext

update tes_documenti
set    des_elemento=:newtext
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 cod_padre=:ll_cod_padre
and    cod_figlio=:ll_cod_figlio;

if sqlca.sqlcode <0 then
	g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

commit;

return 0
end event

event key;long ll_handle

if key = KeyF2! then

	ll_handle = finditem(CurrentTreeItem!, 0)
	
	if ll_handle > 0 then
		
		editlabel(ll_handle)
		
	end if
	
end if
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Fw_documenti_iso.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Fw_documenti_iso.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
