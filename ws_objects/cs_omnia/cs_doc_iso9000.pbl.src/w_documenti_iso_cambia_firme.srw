﻿$PBExportHeader$w_documenti_iso_cambia_firme.srw
forward
global type w_documenti_iso_cambia_firme from w_cs_xx_risposta
end type
type cb_chiudi from commandbutton within w_documenti_iso_cambia_firme
end type
type cb_conferma from commandbutton within w_documenti_iso_cambia_firme
end type
type dw_storico from datawindow within w_documenti_iso_cambia_firme
end type
type dw_firme from datawindow within w_documenti_iso_cambia_firme
end type
end forward

global type w_documenti_iso_cambia_firme from w_cs_xx_risposta
integer width = 2811
integer height = 1852
string title = "Cambia Firme/ Visualizza storico"
boolean controlmenu = false
cb_chiudi cb_chiudi
cb_conferma cb_conferma
dw_storico dw_storico
dw_firme dw_firme
end type
global w_documenti_iso_cambia_firme w_documenti_iso_cambia_firme

type variables
integer ii_anno_registrazione
long il_num_registrazione, il_progressivo
string is_azione
string is_tipo_log_cambio_firma
end variables

event pc_setwindow;call super::pc_setwindow;string ls_anno_num_prog

dw_firme.settransobject(sqlca)
dw_storico.settransobject(sqlca)


ii_anno_registrazione = s_cs_xx.parametri.parametro_d_3_a[1]
il_num_registrazione = s_cs_xx.parametri.parametro_d_3_a[2]
il_progressivo = s_cs_xx.parametri.parametro_d_3_a[3]
is_azione = s_cs_xx.parametri.parametro_s_10
is_tipo_log_cambio_firma =  s_cs_xx.parametri.parametro_s_11

//reset variabili globali
setnull(s_cs_xx.parametri.parametro_d_3_a[1])
setnull(s_cs_xx.parametri.parametro_d_3_a[2])
setnull(s_cs_xx.parametri.parametro_d_3_a[3])
s_cs_xx.parametri.parametro_s_10 = ""
s_cs_xx.parametri.parametro_s_11 = ""

//A01/2012/125/2
ls_anno_num_prog = s_cs_xx.cod_azienda + "/" + string(ii_anno_registrazione) + "/" + string(il_num_registrazione) + "/" + string(il_progressivo)

if is_azione = "X" then
	this.title = "Visualizza storico"
	
	cb_conferma.visible = false
	cb_chiudi.text = "Chiudi"
	
	dw_firme.visible=false
	dw_storico.visible= true
	dw_storico.retrieve(ls_anno_num_prog, is_tipo_log_cambio_firma)
	
else
	this.title = "Cambia Firme"
	dw_firme.visible=true
	dw_storico.visible= false
	
	dw_firme.retrieve(s_cs_xx.cod_azienda, ii_anno_registrazione, il_num_registrazione, il_progressivo)
	
	dw_firme.object.nome_documento.tabsequence = 0
	dw_firme.object.des_documento.tabsequence = 0
	dw_firme.object.valido_per.tabsequence = 0
	
	//poi in base all'azione abilita i campi opportuni
	choose case is_azione
		case "E"		//cambia emissione
			dw_firme.object.emesso_da.tabsequence = 10
			dw_firme.object.emesso_il.tabsequence = 20
			
			dw_firme.object.autorizzato_da.tabsequence = 0
			dw_firme.object.autorizzato_il.tabsequence = 0
			
			dw_firme.object.approvato_da.tabsequence = 0
			dw_firme.object.approvato_il.tabsequence = 0
			
			this.title += " Emissione"
			
		case "V"		//cambia verificatore/autorizzatore
			dw_firme.object.emesso_da.tabsequence = 0
			dw_firme.object.emesso_il.tabsequence = 0
			
			dw_firme.object.autorizzato_da.tabsequence = 10
			dw_firme.object.autorizzato_il.tabsequence = 20
			
			dw_firme.object.approvato_da.tabsequence = 0
			dw_firme.object.approvato_il.tabsequence = 0
			
			this.title += " Verifica"
			
		case "A"		//cambia approvatore
			dw_firme.object.emesso_da.tabsequence = 0
			dw_firme.object.emesso_il.tabsequence = 0
			
			dw_firme.object.autorizzato_da.tabsequence = 0
			dw_firme.object.autorizzato_il.tabsequence = 0
			
			dw_firme.object.approvato_da.tabsequence = 10
			dw_firme.object.approvato_il.tabsequence = 20
			
			this.title += " Approva"
			
	end choose
	
	dw_firme.setfocus()
	
end if
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_firme,"emesso_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_firme,"autorizzato_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_firme,"approvato_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_documenti_iso_cambia_firme.create
int iCurrent
call super::create
this.cb_chiudi=create cb_chiudi
this.cb_conferma=create cb_conferma
this.dw_storico=create dw_storico
this.dw_firme=create dw_firme
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_chiudi
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.dw_storico
this.Control[iCurrent+4]=this.dw_firme
end on

on w_documenti_iso_cambia_firme.destroy
call super::destroy
destroy(this.cb_chiudi)
destroy(this.cb_conferma)
destroy(this.dw_storico)
destroy(this.dw_firme)
end on

type cb_chiudi from commandbutton within w_documenti_iso_cambia_firme
integer x = 2345
integer y = 28
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;
s_cs_xx_parametri lstr_parametri

lstr_parametri.parametro_s_1 = ""

closewithreturn(parent, lstr_parametri)
end event

type cb_conferma from commandbutton within w_documenti_iso_cambia_firme
integer x = 27
integer y = 28
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string ls_cod_firma, ls_nome_colonna
datetime ldt_data
s_cs_xx_parametri lstr_parametri

dw_firme.accepttext()

choose case is_azione
	case "E"		//cambia emissione
		ls_nome_colonna = "emesso"
		
	case "V"		//cambia verificatore/autorizzatore
		ls_nome_colonna = "autorizzato"
		
	case "A"		//cambia approvatore
		ls_nome_colonna = "approvato"
		
end choose

ls_cod_firma = dw_firme.getitemstring(1, ls_nome_colonna+"_da")
ldt_data = dw_firme.getitemdatetime(1, ls_nome_colonna+"_il")

if isnull(ls_cod_firma) or ls_cod_firma="" then
	g_mb.error("Selezionare un mansionario per la firma!")
	return
end if

if isnull(ldt_data) or year(date(ldt_data))<=1950 then
	g_mb.error("Inserire una data valida per la firma!")
	return
end if

if not g_mb.confirm("Attenzione!", "Confermi la modifica della firma al valore '"+ls_cod_firma+"'?", 1) then
	return
end if

lstr_parametri.parametro_s_1 = ls_cod_firma
lstr_parametri.parametro_data_1 = ldt_data

closewithreturn(parent, lstr_parametri)
end event

type dw_storico from datawindow within w_documenti_iso_cambia_firme
integer x = 27
integer y = 200
integer width = 2665
integer height = 1536
integer taborder = 20
string title = "none"
string dataobject = "d_det_documenti_cambio_firme"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_firme from datawindow within w_documenti_iso_cambia_firme
integer x = 23
integer y = 148
integer width = 2725
integer height = 1588
integer taborder = 10
string title = "none"
string dataobject = "d_det_documenti_det_1_tv"
boolean livescroll = true
end type

