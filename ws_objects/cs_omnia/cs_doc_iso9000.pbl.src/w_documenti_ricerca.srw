﻿$PBExportHeader$w_documenti_ricerca.srw
$PBExportComments$Finestra Documenti Ricerca
forward
global type w_documenti_ricerca from w_cs_xx_principale
end type
type st_1 from statictext within w_documenti_ricerca
end type
type cb_ok from commandbutton within w_documenti_ricerca
end type
type cb_annulla from uo_cb_close within w_documenti_ricerca
end type
type dw_stringa_ricerca_documenti from datawindow within w_documenti_ricerca
end type
type dw_documenti_lista from datawindow within w_documenti_ricerca
end type
end forward

global type w_documenti_ricerca from w_cs_xx_principale
integer width = 3031
integer height = 1696
string title = "Ricerca Documenti"
st_1 st_1
cb_ok cb_ok
cb_annulla cb_annulla
dw_stringa_ricerca_documenti dw_stringa_ricerca_documenti
dw_documenti_lista dw_documenti_lista
end type
global w_documenti_ricerca w_documenti_ricerca

type variables
long   il_row
string is_tipo_ricerca = 'D'
end variables

forward prototypes
public subroutine wf_pos_ricerca ()
public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca)
end prototypes

public subroutine wf_pos_ricerca ();// s_cs_xx.parametri.parametro_tipo_ricerca  = 1   ----> ricerca per anno
//															  2   ----> ricerca per numero documento
//															  3   ----> ricerca per descrizione documento

dw_documenti_lista.reset()

if isnull(s_cs_xx.parametri.parametro_tipo_ricerca) then
	
	is_tipo_ricerca = "A"
	
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	
	st_1.text = "Ricerca per Anno:"

	if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) then
		
		dw_stringa_ricerca_documenti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
		
	end if

elseif is_tipo_ricerca = "N" then
	
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	
	st_1.text = "Ricerca per Numero:"
		
	if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) then
		
		dw_stringa_ricerca_documenti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
		
	end if
	
elseif is_tipo_ricerca = "D" then
	
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	
	st_1.text = "Ricerca per Nome Doc.:"
		
	if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) then
		
		dw_stringa_ricerca_documenti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
		
	end if
	
end if	

setnull(s_cs_xx.parametri.parametro_pos_ricerca)

dw_documenti_lista.setfocus()
end subroutine

public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca);string  ls_sql, ls_stringa, ls_str1, ls_str2, ls_prova, new_select
long    ll_i, ll_y, ll_ret, ll_pos
integer li_risposta, ll_index


ls_sql = " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			"        tipo_elemento = 'D' "


choose case fs_tipo_ricerca 
		
	case "D" // aggiungo asterisco se ricerca per descrizione
		
		if right(trim(fs_stringa_ricerca), 1) <> "*" and pos(fs_stringa_ricerca, "&") < 1 then
			
			fs_stringa_ricerca += "*"

		end if

end choose



ll_i = len(fs_stringa_ricerca)

for ll_y = 1 to ll_i
	
	if mid(fs_stringa_ricerca, ll_y, 1) = "*" then
		
		fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_y, 1, "%")
		
	end if
	
next


//****************** correzione ricerca con apostrofo - Michele 23/04/2002 ******************

ll_pos = 1

do

   ll_pos = pos(fs_stringa_ricerca,"'",ll_pos)
	
   if ll_pos <> 0 then
      fs_stringa_ricerca = replace(fs_stringa_ricerca,ll_pos,1,"''")
      ll_pos = ll_pos + 2
   end if	

loop while ll_pos <> 0

//*******************************************************************************************

//********** visualizzo solo i documenti di competenza

ls_sql = ls_sql + " and (select count(*) " + &
                  "      from   det_documenti " + &
                  "      where  cod_azienda = tes_documenti.cod_azienda and " + &
						"             anno_registrazione = tes_documenti.anno_registrazione and " + &
                  "             num_registrazione = tes_documenti.num_registrazione and " + &
                  "             livello_documento >= '" + s_cs_xx.parametri.parametro_s_1 + "'  and " + &
					   "             flag_storico = '" + s_cs_xx.parametri.parametro_s_2 + "' ) > 0  "
												
ls_sql = ls_sql + " and (select count(*) " + &
                  "      from   det_documenti " + &
                  "      where  cod_azienda = tes_documenti.cod_azienda and " + &
						"             anno_registrazione = tes_documenti.anno_registrazione and " + &
                  "             num_registrazione = tes_documenti.num_registrazione ) > 0  "
						
//ls_sql = ls_sql + " and flag_blocco <> 'S' "

//**********

choose case fs_tipo_ricerca
		
	case "A"  				// anno
		
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
	
			ls_sql = ls_sql + " and anno_registrazione = " + fs_stringa_ricerca 
			
		end if
		
	   ls_sql = ls_sql + " order by anno_registrazione "
		
	case "N"             // numero
		
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
	
			ls_sql = ls_sql + " and num_registrazione = " + fs_stringa_ricerca 
			
		end if
		
	   ls_sql = ls_sql + " order by num_registrazione "
		
	case "D"             // descrizione documento
		
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			
			if pos(fs_stringa_ricerca, "&") > 0 then
				
				ls_str1 = left(fs_stringa_ricerca, pos(fs_stringa_ricerca, "&") - 1)
				
				ls_str2 = mid(fs_stringa_ricerca, pos(fs_stringa_ricerca, "&") + 1)
				
				ls_sql = ls_sql + " and des_elemento like '" + ls_str1 + "%' "
				
			else	
				
				if pos(fs_stringa_ricerca, "%") > 0 then 
					
					ls_sql = ls_sql + " and des_elemento like '" + fs_stringa_ricerca + "'"
					
				else
					
					ls_sql = ls_sql + " and des_elemento = '" + fs_stringa_ricerca + "'"
					
				end if		
				
			end if
			
		end if
		
	   ls_sql = ls_sql + " order by des_elemento"
		
end choose


ls_prova = upper(dw_documenti_lista.GETSQLSELECT())

ll_index = pos(ls_prova, "WHERE")

if ll_index = 0 then
	
	new_select = dw_documenti_lista.GETSQLSELECT() + ls_sql
	
else	
	
	new_select = left(dw_documenti_lista.GETSQLSELECT(), ll_index - 1) + ls_sql
	
end if

ll_ret = dw_documenti_lista.SetSQLSelect(new_select)

if ll_ret < 1 then
	
	g_mb.messagebox("APICE", "Errore nella assegnazione sintassi SQL di ricerca con setsqlselect: comunicare l'errore all'amministratore del sistema")
	
	return
	
end if

ll_y = dw_documenti_lista.retrieve()

if ll_y > 0 then

else
	
	dw_stringa_ricerca_documenti.setfocus()
	
end if

end subroutine

on deactivate;call w_cs_xx_principale::deactivate;this.hide()
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_documenti_lista.settransobject(sqlca)
dw_stringa_ricerca_documenti.insertrow(0)
dw_documenti_lista.setrowfocusindicator(hand!)
if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(s_cs_xx.parametri.parametro_pos_ricerca) > 0 then
	wf_pos_ricerca()
	dw_documenti_lista.setfocus()
else
	dw_stringa_ricerca_documenti.setfocus()	
end if

//dw_folder.fu_folderoptions(dw_folder.c_defaultheight , dw_folder.c_foldertabright)

//l_objects[1] = dw_nuovo
//l_objects[1] = cb_nuovo
//l_objects[2] = st_3
//dw_folder.fu_assigntab(2, "R.", l_Objects[])
//l_objects[1] = dw_documenti_lista
//l_objects[2] = cb_annulla
//l_objects[3] = cb_ok
//l_objects[4] = dw_stringa_ricerca_documenti
//l_objects[5] = st_1
////l_objects[6] = st_2
//dw_folder.fu_assigntab(1, "L.", l_Objects[])
//dw_folder.fu_foldercreate(2,2)
//dw_folder.fu_selectTab(1)



end event

on w_documenti_ricerca.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_stringa_ricerca_documenti=create dw_stringa_ricerca_documenti
this.dw_documenti_lista=create dw_documenti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.dw_stringa_ricerca_documenti
this.Control[iCurrent+5]=this.dw_documenti_lista
end on

on w_documenti_ricerca.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_stringa_ricerca_documenti)
destroy(this.dw_documenti_lista)
end on

event activate;call super::activate;if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(s_cs_xx.parametri.parametro_pos_ricerca) > 0 then
	wf_pos_ricerca()
else
	dw_stringa_ricerca_documenti.setfocus()
end if


end event

event closequery;save_on_close(c_socnosave)
call super ::closequery
end event

type st_1 from statictext within w_documenti_ricerca
integer x = 46
integer y = 60
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ricerca per Nome Doc:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_documenti_ricerca
integer x = 2606
integer y = 1500
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if dw_documenti_lista.getrow() > 0 then
	
	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		
		s_cs_xx.parametri.parametro_uo_dw_1.Setitem( 1, "anno_registrazione", dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"anno_registrazione"))

		s_cs_xx.parametri.parametro_uo_dw_1.Setitem( 1, "num_registrazione", dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"num_registrazione"))
		
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("des_elemento")
		
		s_cs_xx.parametri.parametro_uo_dw_1.SetText (dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"des_elemento"))		
		
		s_cs_xx.parametri.parametro_uo_dw_1.AcceptText()		
		
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("anno_registrazione")
		
		s_cs_xx.parametri.parametro_uo_dw_1.setfocus()
		
		s_cs_xx.parametri.parametro_uo_dw_1.postevent("ue_cerca_doc")		
		
	else
		
		s_cs_xx.parametri.parametro_uo_dw_search.Setitem( 1, "anno_registrazione", dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"anno_registrazione"))

		s_cs_xx.parametri.parametro_uo_dw_search.Setitem( 1, "num_registrazione", dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"num_registrazione"))
		
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn("des_elemento")
		
		s_cs_xx.parametri.parametro_uo_dw_search.SetText (dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"des_elemento"))		
		
		s_cs_xx.parametri.parametro_uo_dw_search.AcceptText()		
		
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn("anno_registrazione")
		
		s_cs_xx.parametri.parametro_uo_dw_search.setfocus()
		
		s_cs_xx.parametri.parametro_uo_dw_search.postevent("ue_cerca_doc")
		
	end if
	
	if il_row > 0 and not isnull(il_row) then
		
		dw_documenti_lista.setrow(il_row - 1)
		
		il_row = 0
		
	end if
	
	parent.hide()
	
end if
end event

type cb_annulla from uo_cb_close within w_documenti_ricerca
integer x = 2217
integer y = 1500
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type dw_stringa_ricerca_documenti from datawindow within w_documenti_ricerca
event ue_key pbm_dwnkey
integer x = 960
integer y = 40
integer width = 2011
integer height = 100
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_stringa_ricerca_prodotti"
end type

event ue_key;string ls_stringa

if key = keyenter! then
	
	ls_stringa = dw_stringa_ricerca_documenti.gettext()
	
	wf_dw_select(is_tipo_ricerca, ls_stringa)
	
	dw_documenti_lista.setfocus()
	
end if
end event

event getfocus;string ls_str

if getrow() > 0 then
	
	ls_str = getitemstring(1,"stringa_ricerca")
	
	this.setcolumn("stringa_ricerca")
	
	this.selecttext(1,len(ls_str))
	
end if
end event

type dw_documenti_lista from datawindow within w_documenti_ricerca
event ue_key pbm_dwnkey
integer x = 46
integer y = 160
integer width = 2926
integer height = 1320
integer taborder = 70
string dataobject = "d_documenti_ricerca"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_key;CHOOSE CASE key
		
	CASE KeyEnter!
		
		il_row = this.getrow()
		
		cb_ok.triggerevent("clicked")
		
	Case keydownarrow! 
		
		if il_row < this.rowcount() then
			
			il_row = this.getrow() + 1
			
		end if
		
	Case keyuparrow!
		
		if il_row > 1 then
			
			il_row = this.getrow() - 1
			
		end if
		
END CHOOSE

end event

event doubleclicked;cb_ok.postevent(clicked!)


end event

event buttonclicked;choose case dwo.name
		
	case "cb_anno" 
		
		dw_documenti_lista.reset()
		
		is_tipo_ricerca = "A"
		
		st_1.text = "Ricerca per Anno:"
		
		dw_stringa_ricerca_documenti.setfocus()
		
	case "cb_numero" 
		
		dw_documenti_lista.reset()
		
		is_tipo_ricerca = "N"
		
		st_1.text = "Ricerca per Numero Doc.:"
		
		dw_stringa_ricerca_documenti.setfocus()		
		
	case "cb_descrizione" 
		
		dw_documenti_lista.reset()
		
		is_tipo_ricerca = "D"
		
		st_1.text = "Ricerca per Nome Doc.:"
		
		dw_stringa_ricerca_documenti.setfocus()
		
end choose
	
	
	
end event

