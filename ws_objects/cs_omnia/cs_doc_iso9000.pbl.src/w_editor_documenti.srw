﻿$PBExportHeader$w_editor_documenti.srw
forward
global type w_editor_documenti from w_cs_xx_principale
end type
type p_1 from picture within w_editor_documenti
end type
type dw_1 from uo_editor_documenti within w_editor_documenti
end type
type rectangle_1 from rectangle within w_editor_documenti
end type
type split_1 from uo_splitbar within w_editor_documenti
end type
type tv_1 from treeview within w_editor_documenti
end type
end forward

global type w_editor_documenti from w_cs_xx_principale
integer width = 3273
integer height = 2072
string title = "Editor Documenti"
event we_menu_mappa_rinomina ( )
event we_menu_mappa_elimina ( )
event we_menu_mappa_immagine ( )
event we_menu_mappa_area ( )
event we_menu_mappa_aggiungi ( )
event we_menu_documento_elimina ( )
event we_menu_documento_aggiungi ( )
p_1 p_1
dw_1 dw_1
rectangle_1 rectangle_1
split_1 split_1
tv_1 tv_1
end type
global w_editor_documenti w_editor_documenti

type variables
private string is_msg_name = "Editor Documenti"


// -- User
private:
	string is_user_temporaly
	string is_user_documents
	long il_prog_processo = -1
	
// -- handle
private:
	long il_handle_rclick = -1
	long il_handle_drag = -1
	boolean ib_treeview_dbclick = false
end variables

forward prototypes
public function string wf_userinfo (string as_key)
public function integer wf_carica_mappe (long al_handle, integer ai_prog_processo)
public function boolean wf_trova_immagine (ref string as_image_path, ref string as_image_name, ref integer ai_image_width, ref integer ai_image_height)
public function boolean wf_trova_documento (ref long al_prog_elemento, ref string as_tipo_elemento, ref string as_nome_elemento)
end prototypes

event we_menu_mappa_rinomina();if il_handle_rclick > 0 then

	tv_1.editlabels = true
	tv_1.editlabel(il_handle_rclick)
	
end if
end event

event we_menu_mappa_elimina();if il_handle_rclick < 0 then
	il_handle_rclick = -1
	return
end if

if g_mb.messagebox(is_msg_name, "Eliminare la mappa e tutti i nodi assegnati?", Question!, YesNo!) = 2 then
	il_handle_rclick = -1
	return
end if

treeviewitem ltvi_item
str_editor_documenti lstr_data

tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

if dw_1.uof_mappa_elimina(lstr_data.prog_processo) then
	tv_1.deleteitem(il_handle_rclick)
	dw_1.uof_clear()
	
	if lstr_data.prog_processo = il_prog_processo then
		dw_1.visible = false
	end if
end if

il_handle_rclick = -1
end event

event we_menu_mappa_immagine();string ls_img_path, ls_img_name, ls_des_nodo
int li_cod_nodo, li_img_width, li_img_height
treeviewitem ltvi_item
str_editor_documenti lstr_data

if il_handle_rclick < 1 then
	il_handle_rclick = -1
	return
end if

// -- Seleziono l'immagine
if not wf_trova_immagine(ls_img_path, ls_img_name, li_img_width, li_img_height) then
	il_handle_rclick = -1
	return
end if
// ----

tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

if dw_1.uof_mappa_immagine(lstr_data.prog_processo, ls_img_path, ls_img_name, li_img_width, li_img_height) then
	dw_1.uof_draw()
end if

il_handle_rclick = -1
end event

event we_menu_mappa_area();treeviewitem ltvi_item
str_editor_documenti lstr_data

if il_handle_rclick > 0 then
	
	open(w_nav_doc_aree)
	
	tv_1.getitem(il_handle_rclick, ltvi_item)
	lstr_data = ltvi_item.data
	
	if dw_1.uof_mappa_area(lstr_data.prog_processo, s_cs_xx.parametri.parametro_s_1) then
		
		lstr_data.cod_area = s_cs_xx.parametri.parametro_s_1
		lstr_data.des_area = s_cs_xx.parametri.parametro_s_2

		ltvi_item.data = lstr_data
		tv_1.setitem(il_handle_rclick, ltvi_item)
		
	end if
	
	setnull(s_cs_xx.parametri.parametro_s_1)
	setnull(s_cs_xx.parametri.parametro_s_2)
end if

il_handle_rclick = -1
end event

event we_menu_mappa_aggiungi();string ls_img_path, ls_img_name, ls_des_nodo
int li_img_width, li_img_height, li_cnt, li_prog_processo_modello, li_prog_processo
treeviewitem ltvi_item, ltvi_child
str_editor_documenti lstr_data

if il_handle_rclick < 0 then
	il_handle_rclick = -1
	return
end if

// -- Seleziono l'immagine
if not wf_trova_immagine(ls_img_path, ls_img_name, li_img_width, li_img_height) then
	il_handle_rclick = -1
	return
end if
// ----

if il_handle_rclick = 0 then
	// aggiungi un padre
	li_prog_processo_modello = 0
else
	// aggiungo un figlio
	tv_1.getitem(il_handle_rclick, ltvi_item)
	lstr_data = ltvi_item.data
	li_prog_processo_modello = lstr_data.prog_processo
end if



// -- Calcolo progressivo per il nome tv (trick, solo per scopo estetico)
select count(prog_processo)
into :li_cnt
from processi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	des_processo like 'Mappa%';
	
if sqlca.sqlcode <> 0 or isnull(li_cnt) then li_cnt = 0

li_cnt++
ls_des_nodo = "Mappa " + string(li_cnt)
// ----

if dw_1.uof_mappa_aggiungi(li_prog_processo, li_prog_processo_modello, ls_des_nodo, ls_img_path, ls_img_name, li_img_width, li_img_height) then
	
	lstr_data.livello = "M"
	lstr_data.prog_processo = li_prog_processo
	lstr_data.des_processo = ls_des_nodo
	
	ltvi_child.data = lstr_data
	ltvi_child.label = ls_des_nodo
	ltvi_child.children = false
	ltvi_child.selected = false
	
	if il_handle_rclick = 0 then
		ltvi_child.pictureindex = 1
		ltvi_child.selectedpictureindex = 1
	else
		ltvi_child.pictureindex = 2
		ltvi_child.selectedpictureindex = 2
	end if
	
	tv_1.insertitemfirst(il_handle_rclick, ltvi_child)
	
	ltvi_item.children = true
	ltvi_item.expanded = true
	tv_1.setitem(il_handle_rclick, ltvi_item)
	
end if

il_handle_rclick = -1


end event

event we_menu_documento_elimina();if g_mb.messagebox(is_msg_name, "Eliminare il collegamento?", Question!, YesNo!) = 2 then
	return
end if

if dw_1.uof_elimina_collegamento() then
	dw_1.uof_draw()
end if

end event

event we_menu_documento_aggiungi();string		ls_tipo_collegamento, ls_nome_elemento
integer 	li_row, li_x, li_y	
long		ll_prog_elemento

li_x = PointerX() - dw_1.x
li_y = PointerY() - dw_1.y

if wf_trova_documento(ll_prog_elemento, ls_tipo_collegamento, ls_nome_elemento) then
		
	if dw_1.uof_aggiungi_collegamento(il_prog_processo, -1, li_x, li_y, 300, 200, ll_prog_elemento, "D", ls_nome_elemento) then
		dw_1.uof_draw()
	end if
	
end if 
end event

public function string wf_userinfo (string as_key);// Ritorna le variabili d'ambiente del sistema
string ls_Path
string ls_values[]
ContextKeyword lcxk_base

this.GetContextService("Keyword", lcxk_base)
lcxk_base.GetContextKeywords(as_key, ls_values)
IF Upperbound(ls_values) > 0 THEN
   ls_Path = ls_values[1]
ELSE
   ls_Path = "*UNDEFINED*"
END IF

return ls_Path
end function

public function integer wf_carica_mappe (long al_handle, integer ai_prog_processo);/**
 * Carico le mappe nella treeview. Se viene passato -1 al progressivo processo allora devo caricare
 * solo le mappe padri, altrimenti sto cercando i figli.
 */
 
string ls_sql
long	ll_count, ll_i
datastore lds_store
treeviewitem ltvi_item
str_editor_documenti lstr_data

if isnull(al_handle) or al_handle < 0 then
	al_handle = 0
end if

if ai_prog_processo < 1 then
	// solo padri
	ls_sql = "SELECT DISTINCT(prog_processo), des_processo, cod_area_aziendale FROM processi WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND prog_processo = prog_processo_modello"
else
	// solo figli
	ls_sql = "SELECT DISTINCT(prog_processo), des_processo, cod_area_aziendale FROM processi WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND prog_processo_modello=" + string(ai_prog_processo) + " AND prog_processo <> prog_processo_modello"
end if

// -- datastore
if not f_crea_datastore(ref lds_store, ls_sql) then
	g_mb.messagebox(this.is_msg_name, "Errore durante la creazione del datastore per le mappe.~r~nwf_carica_mappe()")
	destroy lds_store
	return 0
end if

ll_count = lds_store.retrieve()
// ----

if ll_count <= 0 then
	return 0
else
	for ll_i = 1 to ll_count
		
		lstr_data.livello = "M"
		lstr_data.prog_processo = lds_store.getitemnumber(ll_i, 1)
		lstr_data.des_processo = lds_store.getitemstring(ll_i, 2)
		
		// Area aziendale
		if lds_store.getitemstring(ll_i, 3) <> "" then
			lstr_data.cod_area = lds_store.getitemstring(ll_i, 3)
			
			select des_area
			into	:lstr_data.des_area
			from 	tab_aree_aziendali 
			where cod_azienda=:s_cs_xx.cod_azienda and
					cod_area_aziendale=:lstr_data.cod_area;
		
		else
			lstr_data.cod_area = ""
			lstr_data.des_area = ""
		end if
		// ----
		
		ltvi_item.data = lstr_data
		ltvi_item.label = lds_store.getitemstring(ll_i, 2)
		ltvi_item.selected = false
		ltvi_item.children = true
		
		if ai_prog_processo < 1 then
			ltvi_item.pictureindex = 1
			ltvi_item.selectedpictureindex = 1
			ltvi_item.overlaypictureindex = 1
		else
			ltvi_item.pictureindex = 2
			ltvi_item.selectedpictureindex = 2
			ltvi_item.overlaypictureindex = 2
		end if
		
		tv_1.insertitemlast(al_handle, ltvi_item)
	next
	
	return ll_count
end if
end function

public function boolean wf_trova_immagine (ref string as_image_path, ref string as_image_name, ref integer ai_image_width, ref integer ai_image_height);integer li_return

li_return = GetFileOpenName("Seleziona un'immagine", as_image_path, as_image_name, "jpg", "File Immagine, *.bmp;*.gif;*.jpg;*.png", is_user_documents)

if li_return = 0 then
	// User press cancel
	return false
else
	p_1.picturename = as_image_path
	
	ai_image_width = p_1.width
	ai_image_height = p_1.height
	
	p_1.picturename = ""

	return true
end if
end function

public function boolean wf_trova_documento (ref long al_prog_elemento, ref string as_tipo_elemento, ref string as_nome_elemento);string ls_anno, ls_num

open(w_nav_doc_associa)

if isnull(s_cs_xx.parametri.parametro_s_1) or s_cs_xx.parametri.parametro_s_1 = "" then 
	return false
end if

al_prog_elemento =long(string(s_cs_xx.parametri.parametro_i_2) + string(s_cs_xx.parametri.parametro_i_1))
as_tipo_elemento = s_cs_xx.parametri.parametro_s_1
as_nome_elemento = s_cs_xx.parametri.parametro_s_2
return true
end function

on w_editor_documenti.create
int iCurrent
call super::create
this.p_1=create p_1
this.dw_1=create dw_1
this.rectangle_1=create rectangle_1
this.split_1=create split_1
this.tv_1=create tv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.p_1
this.Control[iCurrent+2]=this.dw_1
this.Control[iCurrent+3]=this.rectangle_1
this.Control[iCurrent+4]=this.split_1
this.Control[iCurrent+5]=this.tv_1
end on

on w_editor_documenti.destroy
call super::destroy
destroy(this.p_1)
destroy(this.dw_1)
destroy(this.rectangle_1)
destroy(this.split_1)
destroy(this.tv_1)
end on

event resize;call super::resize;tv_1.move(20,20)
tv_1.height = newheight - 40

split_1.move(tv_1.x + tv_1.width, 20)
split_1.resize(15, tv_1.height)

rectangle_1.move(split_1.x + split_1.width, 20)
rectangle_1.resize(newwidth - (split_1.x + split_1.width + 20), split_1.height)

dw_1.move(rectangle_1.x + 5, rectangle_1.y + 5)
dw_1.uof_set_dimension(rectangle_1.width - 5, rectangle_1.height - 5)

end event

event pc_setwindow;call super::pc_setwindow;// Calcolo percorsi utente
try
	f_getuserpath(is_user_temporaly)
	is_user_temporaly += "omnia\editor_documenti\"
	
	if not DirectoryExists(is_user_temporaly) then
		CreateDirectory(is_user_temporaly)
	end if
	
	is_user_documents  = wf_userinfo("HOMEDRIVE")
	is_user_documents += wf_userinfo("HOMEPATH")
	is_user_documents += "\Documenti\"
catch (RuntimeError ex)
	setnull(is_user_temporaly)
	setnull(is_user_documents)
end try
// ---

// -- Nascondo la datawindow
dw_1.visible = false

// -- Imposto icone treeview ricerca
tv_1.deletepictures()
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_editor_doc_parent.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_editor_doc_child.png")
// ----

wf_carica_mappe(0 , 0)
end event

type p_1 from picture within w_editor_documenti
boolean visible = false
integer x = 1806
integer y = 1720
integer width = 229
integer height = 200
boolean originalsize = true
boolean focusrectangle = false
end type

type dw_1 from uo_editor_documenti within w_editor_documenti
integer x = 937
integer y = 20
integer width = 2263
integer height = 1160
integer taborder = 20
boolean hscrollbar = false
boolean vscrollbar = false
boolean livescroll = false
end type

event dragdrop;call super::dragdrop;string ls_tipo_collegamento, ls_valore
int li_x, li_y, li_prog_processo
treeviewitem ltvi_item
str_editor_documenti lstr_data

if il_handle_drag < 0 then
	return
end if

if il_handle_drag > 0 then
	
	tv_1.getitem(il_handle_drag, ltvi_item)
	lstr_data = ltvi_item.data
	
	li_prog_processo = lstr_data.prog_processo
				
	ls_valore = lstr_data.des_processo
		
	li_x = PointerX()
	li_y = PointerY()
	
	
	if dw_1.uof_aggiungi_collegamento(il_prog_processo, -1, li_x, li_y, 300, 200, li_prog_processo, "M", ls_valore) then
		dw_1.uof_draw()
	end if
	
end if

il_handle_drag = -1
end event

event rbuttondown;call super::rbuttondown;m_editor_documenti lm_menu
lm_menu = create m_editor_documenti

// -- Nascondo i pulsanti relativi alle mappe
lm_menu.m_mappa_attuale.visible= false
lm_menu.m_sep1.visible= false
lm_menu.m_mappa_aggiungi.visible= false
lm_menu.m_mappa_area.visible= false
lm_menu.m_mappa_immagine.visible= false
lm_menu.m_mappa_rinomina.visible= false
lm_menu.m_sep.visible= false
lm_menu.m_mappa_elimina.visible= false
// ----

if left(dwo.name, 2) = "t_" then
	lm_menu.m_documento_elimina.visible= true
else
	lm_menu.m_documento_aggiungi.visible= true
end if

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu
end event

type rectangle_1 from rectangle within w_editor_documenti
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 16777215
integer x = 937
integer y = 1740
integer width = 229
integer height = 200
end type

type split_1 from uo_splitbar within w_editor_documenti
integer x = 869
integer y = 20
integer height = 1928
end type

event constructor;call super::constructor;uof_register(tv_1, LEFT)
uof_register(dw_1, RIGHT)
end event

event uoe_enddrag;call super::uoe_enddrag;rectangle_1.x += ai_delta_x
rectangle_1.width -= ai_delta_x
end event

type tv_1 from treeview within w_editor_documenti
integer x = 23
integer y = 20
integer width = 846
integer height = 1920
integer taborder = 10
string dragicon = "DataWindow5!"
boolean dragauto = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean linesatroot = true
boolean disabledragdrop = false
boolean hideselection = false
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event itempopulate;str_editor_documenti lstr_data
treeviewitem ltvi_item

if handle < 0 or ib_treeview_dbclick then return

tv_1.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

choose case lstr_data.livello
	case "M" // mappa
		if wf_carica_mappe(handle, lstr_data.prog_processo) < 1 then
			ltvi_item.children = false
			tv_1.setitem(handle, ltvi_item)
		end if
		
end choose
end event

event doubleclicked;str_editor_documenti lstr_data
treeviewitem ltvi_item

if handle < 1 then return

ib_treeview_dbclick = true
tv_1.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

choose case lstr_data.livello
	case "M" // mappa
		if dw_1.uof_carica_mappa(lstr_data.prog_processo) then
			il_prog_processo = lstr_data.prog_processo
			dw_1.visible = true
		end if
		
end choose
end event

event endlabeledit;treeviewitem ltvi_item
str_editor_documenti lstr_data

// prevent empty char
if newtext = "" or isnull(newtext) or handle < 0 then
	il_handle_rclick = -1
	return -1
end if

tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

if dw_1.uof_mappa_rinomina(lstr_data.prog_processo, newtext)  then
	lstr_data.des_processo = newtext
	ltvi_item.data = lstr_data
	tv_1.setitem(il_handle_rclick, ltvi_item)
end if

il_handle_rclick = -1
end event

event rightclicked;treeviewitem ltvi_item
str_editor_documenti lstr_data
m_editor_documenti lm_menu

if handle < 0 then return

il_handle_rclick = handle
lm_menu = create m_editor_documenti

if handle = 0 then
	// cliccato sul bianco, probabilmente voglio aggiungere una nuova mappa
	lm_menu.m_mappa_attuale.visible = false
	lm_menu.m_sep1.visible = false
	lm_menu.m_mappa_aggiungi.visible = true
	lm_menu.m_mappa_area.visible = false
	lm_menu.m_mappa_immagine.visible = false
	lm_menu.m_mappa_rinomina.visible = false
	lm_menu.m_sep.visible = false
	lm_menu.m_mappa_elimina.visible = false
else
	tv_1.getitem(handle, ltvi_item)
	lstr_data = ltvi_item.data
	
	if len(lstr_data.cod_area) > 0 then
		lm_menu.m_mappa_attuale.visible = true
		lm_menu.m_mappa_attuale.text = lstr_data.des_area
		lm_menu.m_sep1.visible = true
	else
		lm_menu.m_mappa_attuale.visible = false
		lm_menu.m_sep1.visible = false
	end if
	
end if

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu
end event

event dragwithin;// Bisogna usare una variabile e settarla solo una volta altrimenti se l'ultente
// scorre sopra gli altri nodi della tv l'handle viene cambiato con l'ultimo selezionato.

if il_handle_drag = -1 then
	il_handle_drag = handle
end if
end event

event itemexpanding;// se la variabile ib_treeview_dbclick è a true allora ho fatto doppio click sul nodo
// e devo prevenire la chiusuara/apertura del nodo.

if ib_treeview_dbclick then
	ib_treeview_dbclick = false
	return 1
end if
end event

event itemcollapsing;// se la variabile ib_treeview_dbclick è a true allora ho fatto doppio click sul nodo
// e devo prevenire la chiusuara/apertura del nodo.

if ib_treeview_dbclick then
	ib_treeview_dbclick = false
	return 1
end if
end event

