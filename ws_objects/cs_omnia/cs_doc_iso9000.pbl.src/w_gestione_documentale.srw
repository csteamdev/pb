﻿$PBExportHeader$w_gestione_documentale.srw
forward
global type w_gestione_documentale from window
end type
type ole_1 from olecustomcontrol within w_gestione_documentale
end type
type ws_processi from structure within w_gestione_documentale
end type
end forward

type ws_processi from structure
	long		prog_processo
	string		des_processo
	datetime		data_creazione
	string		note
	string		modello
	long		prog_processo_modello
	string		path_blob
	long		h
	long		w
end type

global type w_gestione_documentale from window
integer width = 2871
integer height = 1864
boolean titlebar = true
string title = "Gestione Documentale"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
ole_1 ole_1
end type
global w_gestione_documentale w_gestione_documentale

type variables
oleobject ilo_processi, ilo_documenti, ilo_nodo

integer   ii_trovato 

string    is_tmp

private   ws_processi iws_processi[]

boolean   gb_wait

end variables

forward prototypes
public function oleobject wf_nuovoattributo (ref oleobject xnodo, string nomeattributo, string valoreattributo)
public subroutine wf_on_elimina_mappa (ref oleobject xnodo)
public subroutine wf_on_elimina_rect (ref oleobject xnodo)
public subroutine wf_on_rinomina_mappa (ref oleobject xnodo)
public subroutine wf_on_rinomina_rect (ref oleobject xnodo)
public subroutine wf_on_collega_documento_navigatore (ref oleobject xnodo)
public subroutine wf_on_collega_mappa_navigatore (ref oleobject xnodo)
public subroutine wf_on_selezione (ref oleobject xnodo)
public function integer wf_salva_dati ()
public subroutine wf_on_immagine_mappa (ref oleobject xnodo)
public subroutine wf_nuova_mappa ()
public function string nomefile (string fs_path)
public function string ExtFile (string fs_path)
public function string wf_allega_collegamenti (long fd_prog_processo, ref string fs_collegamenti)
public function string wf_allega_processi (long fd_prog_processo, ref string fs_xml)
public function string wf_costruisci_processi (ref string fs_xml)
public function string wf_nuovosubnodo (ref oleobject xparent, string nomesubnodo, string valoresubnodo)
public subroutine wf_on_nuova_mappa (ref oleobject xnodo)
public subroutine wf_on_nuovo_figlio_mappa (ref oleobject xnodo)
public function blob caricablob (string fs_path)
public function string wf_costruisci_doc (long fd_cod_confronto, ref string fs_xml_doc)
public subroutine wf_salva_igx (ref oleobject xnodo)
public subroutine wf_associa_area (integer fi_tipo_chiamata, ref oleobject xnodo)
end prototypes

public function oleobject wf_nuovoattributo (ref oleobject xnodo, string nomeattributo, string valoreattributo);oleobject xa
xa = create oleobject
xa.connectToNewObject("Msxml2.IXMLDOMAttribute")


xa = ilo_processi.createAttribute(nomeAttributo)
if not isnull(xa) then
	xa.nodeTypedValue = valoreAttributo
   xnodo.Attributes.setNamedItem(xa)
	if sqlca.sqlcode < 0 then
		setnull(xa)
	end if
end if
return xa

end function

public subroutine wf_on_elimina_mappa (ref oleobject xnodo);integer li_ri
long ll_processo_padre, ll_n_copie, ld_appo
oleobject lo_appo
	
ii_trovato = 1
	
li_ri = g_mb.messagebox("Elimina Mappa", "Vuoi eliminare la Mappa selezionata?",Exclamation!, OKCancel!, 2)
	
if li_ri = 1 then
	
		
		ll_processo_padre = long(xnodo.selectSingleNode("codprocesso").text)
		
		select prog_processo
		into   :ld_appo
		from   processi
		where  prog_processo_modello = :ll_processo_padre and prog_processo <> :ll_processo_padre;
		
		if sqlca.sqlcode <> 100 then
			g_mb.messagebox("OMNIA","Eliminare prima le mappe figlie!")
			rollback;
			return			
		end if
		
		delete from tab_collegamenti_processi
		where       cod_azienda = :s_cs_xx.cod_azienda
		and         prog_processo = :ll_processo_padre;
				
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Elimina Collegamenti Mappa - Err. " + string(sqlca.sqlcode) + " - " + sqlca.sqlerrtext)
			rollback;
			return
		end if				
										
		delete from processi
		where       cod_azienda = :s_cs_xx.cod_azienda
		and         prog_processo = :ll_processo_padre;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Elimina Mappa - Err. " + string(sqlca.sqlcode) + " - " + sqlca.sqlerrtext)
			rollback;
			return
		end if
					
		commit;
				
		lo_appo = create oleobject			
				
		lo_appo = xnodo.parentNode
		lo_appo.removeChild(xnodo)				
		ole_1.object.refresh(0)
					
		destroy lo_appo


end if	
end subroutine

public subroutine wf_on_elimina_rect (ref oleobject xnodo);long ll_collegamento,ll_processo
oleobject lo_appo

lo_appo = create oleobject

lo_appo.connectToNewObject("Msxml2.IXMLDOMNode")
if not isnull(xnodo) then

	ll_collegamento = long(xnodo.selectSingleNode("cod_collegamento").text)
	ll_processo = long(xnodo.selectSingleNode("cod_processo").text)

	if not isnull(ll_collegamento) and not isnull(ll_processo) then
			
		delete from tab_collegamenti_processi
		where cod_azienda = :s_cs_xx.cod_azienda
		and   prog_processo = :ll_processo
		and   prog_collegamento = :ll_collegamento;
				
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
			rollback;
			destroy lo_appo
			return
		else
			commit;	
			lo_appo = xnodo.parentNode
			lo_appo.removeChild(xnodo)				
			ole_1.object.refresh(-1)
		end if
			
	end if
end if
destroy lo_appo
end subroutine

public subroutine wf_on_rinomina_mappa (ref oleobject xnodo);string ls_np
long ll_c_p1
		
ii_trovato = 1
		
ls_np = ole_1.object.myInputBox("Nuovo nome della Mappa:","Mappa","")
if ls_np <> "" then
	xnodo.selectSingleNode("nome").nodeTypedValue = ls_np

	ll_c_p1 = long(xnodo.selectSingleNode("codprocesso").text)
			
	update processi
	set des_processo = :ls_np
	where cod_azienda = :s_cs_xx.cod_azienda
	and prog_processo = :ll_c_p1;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Err. " + string(sqlca.sqlcode) + ": " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
		ole_1.object.Refresh(0)
	end if
			
end if		
end subroutine

public subroutine wf_on_rinomina_rect (ref oleobject xnodo);string s 
long ll_cod_processo, ll_cod_collegamento	

ii_trovato = 1	

		
s = ole_1.object.myInputBox("Nuovo nome del collegamento", "Collegamento", "")
if s <> "" then
	xnodo.selectSingleNode("tag").nodeTypedValue = s
	ll_cod_processo = long(xnodo.selectSingleNode("cod_processo").text)
	ll_cod_collegamento = long(xnodo.selectSingleNode("cod_collegamento").text)
			
	update tab_collegamenti_processi
	set valore = :s
	where cod_azienda = :s_cs_xx.cod_azienda
	and prog_processo = :ll_cod_processo
	and prog_collegamento = :ll_cod_collegamento;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Err. " + string(sqlca.sqlcode) + ": " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
		ole_1.object.Refresh(-1)
	end if
	
end if
end subroutine

public subroutine wf_on_collega_documento_navigatore (ref oleobject xnodo);long      ll_ncoll, ld_max_collegamento, proc_confronto, ll_annoregis, ll_numregis
oleobject xelem1, nddx
string    ls_des_documento, ls_msg
decimal   ld_prog_elemento
		
ii_trovato = 1		
		

xelem1 = create oleobject		
nddx   = create oleobject
nddx.connectToNewObject("MSXML2.IXMLDOMNode")

        				  
if not isnull(ole_1.object.nododestra) then			
	nddx = ole_1.object.nododestra
	proc_confronto = long(nddx.selectSingleNode("codprocesso").text)
else
	g_mb.messagebox("OMNIA","Impossibile creare il collegamento!")
	return
end if			
		
		
ll_annoregis = long(xnodo.selectSingleNode("annoregis").text)
ll_numregis = long(xnodo.selectSingleNode("numregis").text)
ls_des_documento = xnodo.selectSingleNode("nome").text		

				
		
select max(prog_collegamento)
into   :ld_max_collegamento
from   tab_collegamenti_processi
where  cod_azienda = :s_cs_xx.cod_azienda
and    prog_processo = :proc_confronto;
		
		
if isnull(ld_max_collegamento) or ld_max_collegamento = 0 then
	ld_max_collegamento = 1
else
	ld_max_collegamento = ld_max_collegamento + 1
end if
				
ld_prog_elemento = (ll_numregis * 10000) + ll_annoregis 								

insert into tab_collegamenti_processi(cod_azienda,prog_processo,prog_collegamento,x1,y1,x2,y2,prog_elemento,tipo_elemento,valore)
values(:s_cs_xx.cod_azienda,:proc_confronto,:ld_max_collegamento,0,0,0.1,0.1,:ld_prog_elemento,'D',:ls_des_documento);
		
if sqlca.sqlcode <> -1 then
	commit;
   xelem1 = ilo_processi.createElement("nodo")
   ll_ncoll = nddx.selectSingleNode("collegamenti").childNodes.length		
   ls_msg = wf_nuovosubnodo(REF xelem1,"cod_processo", string(proc_confronto))
   ls_msg = wf_nuovosubnodo(REF xelem1,"cod_collegamento",string(ld_max_collegamento))
   ls_msg = wf_nuovosubnodo(REF xelem1,"valore",string(ld_prog_elemento))
   ls_msg = wf_nuovosubnodo(REF xelem1,"tag",ls_des_documento)
   ls_msg = wf_nuovosubnodo(REF xelem1,"tipo", "COLLEGAMENTO")
   ls_msg = wf_nuovosubnodo(REF xelem1,"x1","0")
   ls_msg = wf_nuovosubnodo(REF xelem1,"y1","0")
   ls_msg = wf_nuovosubnodo(REF xelem1,"x2","0,1")
   ls_msg = wf_nuovosubnodo(REF xelem1,"y2","0,1")	
   ls_msg = wf_nuovosubnodo(REF xelem1,"tipoelemento","D")		
   wf_nuovoattributo(xelem1, "id", "C" + string(proc_confronto) + string(ll_ncoll + 1))	    
   ole_1.object.AddDestra(xelem1,True)			
else
	g_mb.messagebox("OMNIA","Err. " + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
	rollback;			
end if		

destroy xelem1
destroy nddx
return
end subroutine

public subroutine wf_on_collega_mappa_navigatore (ref oleobject xnodo);long      ll_ncoll, ld_max_collegamento, proc_confronto, ld_processo
oleobject xelem1, nddx
string    ls_des_processo, ls_msg
decimal   ld_prog_elemento
		
ii_trovato = 1		
		

xelem1 = create oleobject		
nddx   = create oleobject
nddx.connectToNewObject("MSXML2.IXMLDOMNode")

        				  
if not isnull(ole_1.object.nododestra) then			
	nddx = ole_1.object.nododestra
	proc_confronto = long(nddx.selectSingleNode("codprocesso").text)
else
	g_mb.messagebox("OMNIA","Impossibile effettuare il collegamento!")
	destroy xelem1
	destroy nddx
	return
end if			
		
		
ld_processo = long(xnodo.selectSingleNode("codprocesso").text)
ls_des_processo = xnodo.selectSingleNode("nome").text		


if ld_processo = proc_confronto then
	g_mb.messagebox("OMNIA","Impossibile collegare una mappa a sè stessa!")
	destroy xelem1
	destroy nddx	
	return
end if
		
select max(prog_collegamento)
into :ld_max_collegamento
from tab_collegamenti_processi
where cod_azienda = :s_cs_xx.cod_azienda
and prog_processo = :proc_confronto;
		
		
if isnull(ld_max_collegamento) or ld_max_collegamento=0 then
	ld_max_collegamento = 1
else
	ld_max_collegamento = ld_max_collegamento + 1
end if

			 								

insert into tab_collegamenti_processi(cod_azienda,prog_processo,prog_collegamento,x1,y1,x2,y2,prog_elemento,tipo_elemento,valore)
values(:s_cs_xx.cod_azienda,:proc_confronto,:ld_max_collegamento,0,0,0.1,0.1,:ld_processo,'M',:ls_des_processo);
		
if sqlca.sqlcode <> -1 then
	commit;
   xelem1 = ilo_processi.createElement("nodo")
   ll_ncoll = nddx.selectSingleNode("collegamenti").childNodes.length		
   ls_msg = wf_nuovosubnodo(REF xelem1,"cod_processo", string(proc_confronto))
   ls_msg = wf_nuovosubnodo(REF xelem1,"cod_collegamento",string(ld_max_collegamento))
   ls_msg = wf_nuovosubnodo(REF xelem1,"valore",string(ld_processo))
   ls_msg = wf_nuovosubnodo(REF xelem1,"tag",ls_des_processo)
   ls_msg = wf_nuovosubnodo(REF xelem1,"tipo", "COLLEGAMENTO")
   ls_msg = wf_nuovosubnodo(REF xelem1,"x1","0")
   ls_msg = wf_nuovosubnodo(REF xelem1,"y1","0")
   ls_msg = wf_nuovosubnodo(REF xelem1,"x2","0,1")
   ls_msg = wf_nuovosubnodo(REF xelem1,"y2","0,1")	
   ls_msg = wf_nuovosubnodo(REF xelem1,"tipoelemento","M")

   wf_nuovoattributo(xelem1, "id", "C" + string(proc_confronto) + string(ll_ncoll + 1))	    
   ole_1.object.AddDestra(xelem1,True)			
else
	g_mb.messagebox("OMNIA","Err. " + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
	rollback;			
end if		

destroy xelem1
destroy nddx
return
end subroutine

public subroutine wf_on_selezione (ref oleobject xnodo);	//case "onSelezione"
		
oleobject lo_r, lo_collegamenti, lo_nodo
string    tipo, ls_tag, ls_tipoelemento
dec{4}    ld_x1, ld_y1, ld_x2, ld_y2, ld_prog_collegamento, ld_p_f, ld_prog_processo
int       i	
		
		
if ii_trovato = 1 then
					
				
	if not isnull(ole_1.object.nododestra) then			
		
		lo_collegamenti  = create oleobject
		lo_nodo          = create oleobject	
		lo_r             = create oleobject				
					
		lo_r = ole_1.object.nododestra
		tipo = lo_r.selectSingleNode("tipo").text
		if tipo = "MAPPA" then
					
			ld_prog_processo = dec(lo_r.selectSingleNode("codprocesso").text)
			lo_collegamenti = lo_r.selectSingleNode("collegamenti")						

				
				
			delete from tab_collegamenti_processi 
			where cod_azienda = :s_cs_xx.cod_azienda
			and prog_processo = :ld_prog_processo;
			
			if sqlca.sqlcode <> -1 then
						
				select max(prog_collegamento)
				into :ld_prog_collegamento
				from tab_collegamenti_processi
				where cod_azienda = :s_cs_xx.cod_azienda
				and prog_processo = :ld_prog_processo;
							
				if isnull(ld_prog_collegamento) or ld_prog_collegamento = 0 then
					ld_prog_collegamento = 1
				else
					ld_prog_collegamento = ld_prog_collegamento + 1
				end if
						
				if (lo_collegamenti.hasChildNodes) then
					for i = 0 to (lo_collegamenti.childNodes.length -1)
						lo_nodo = lo_collegamenti.childNodes.item(i)											
						ld_x1 = dec(lo_nodo.selectSingleNode("x1").text)
						ld_y1 = dec(lo_nodo.selectSingleNode("y1").text)
						ld_x2 = dec(lo_nodo.selectSingleNode("x2").text)
						ld_y2 = dec(lo_nodo.selectSingleNode("y2").text)					
						ls_tag = lo_nodo.selectsingleNode("tag").text
						ld_p_f = dec(lo_nodo.selectSingleNode("valore").text)
						ls_tipoelemento = lo_nodo.selectSingleNode("tipoelemento").text
						
						insert into tab_collegamenti_processi(cod_azienda,prog_processo,prog_collegamento,x1,y1,x2,y2,prog_elemento,tipo_elemento,valore)
						values(:s_cs_xx.cod_azienda, :ld_prog_processo, :ld_prog_collegamento, :ld_x1, :ld_y1, :ld_x2, :ld_y2, :ld_p_f,:ls_tipoelemento,:ls_tag);
			
									
						if sqlca.sqlcode = -1 then
							g_mb.messagebox ("OMNIA","Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
							rollback;
							return 
						end if
						commit;						
						ld_prog_collegamento = ld_prog_collegamento + 1
					next 						
				end if					
				return
			else
				g_mb.messagebox ("OMNIA","Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
				rollback;				
				return 
			end if	
		end if
	end if
else
	ii_trovato = 1
end if
return
end subroutine

public function integer wf_salva_dati ();oleobject lo_r, lo_collegamenti, lo_nodo
string    tipo, ls_tag, ls_tipoelemento
dec{4}    ld_x1, ld_y1, ld_x2, ld_y2, ld_prog_collegamento, ld_p_f, ld_prog_processo
int       i	
				
if not isnull(ole_1.object.nododestra) then			
		
	lo_collegamenti  = create oleobject
	lo_nodo          = create oleobject	
	lo_r             = create oleobject				
				
	lo_r = ole_1.object.nododestra
	tipo = lo_r.selectSingleNode("tipo").text
	if tipo = "MAPPA" then
				
		ld_prog_processo = dec(lo_r.selectSingleNode("codprocesso").text)
		lo_collegamenti = lo_r.selectSingleNode("collegamenti")						

				
				
		delete from tab_collegamenti_processi 
		where cod_azienda = :s_cs_xx.cod_azienda
		and prog_processo = :ld_prog_processo;
			
		if sqlca.sqlcode <> -1 then
					
			select max(prog_collegamento)
			into :ld_prog_collegamento
			from tab_collegamenti_processi
			where cod_azienda = :s_cs_xx.cod_azienda
			and prog_processo = :ld_prog_processo;
						
			if isnull(ld_prog_collegamento) or ld_prog_collegamento = 0 then
				ld_prog_collegamento = 1
			else
				ld_prog_collegamento = ld_prog_collegamento + 1
			end if
					
			if (lo_collegamenti.hasChildNodes) then
				for i = 0 to (lo_collegamenti.childNodes.length -1)
					lo_nodo = lo_collegamenti.childNodes.item(i)											
					ld_x1 = dec(lo_nodo.selectSingleNode("x1").text)
					ld_y1 = dec(lo_nodo.selectSingleNode("y1").text)
					ld_x2 = dec(lo_nodo.selectSingleNode("x2").text)
					ld_y2 = dec(lo_nodo.selectSingleNode("y2").text)					
					ls_tag = lo_nodo.selectsingleNode("tag").text
					ld_p_f = dec(lo_nodo.selectSingleNode("valore").text)
					ls_tipoelemento = lo_nodo.selectSingleNode("tipoelemento").text
					
					insert into tab_collegamenti_processi(cod_azienda,prog_processo,prog_collegamento,x1,y1,x2,y2,prog_elemento,tipo_elemento,valore)
					values(:s_cs_xx.cod_azienda, :ld_prog_processo, :ld_prog_collegamento, :ld_x1, :ld_y1, :ld_x2, :ld_y2, :ld_p_f,:ls_tipoelemento,:ls_tag);
		
								
					if sqlca.sqlcode = -1 then
						g_mb.messagebox ("OMNIA","Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
						rollback;
						return 1
					end if
					commit;						
					ld_prog_collegamento = ld_prog_collegamento + 1
				next 						
			end if					
			return 0		
		else
			g_mb.messagebox ("OMNIA","Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
			rollback;				
			return 1
		end if	
	else
		return 0
	end if
	
// ALESSANDRO 12/08 (disperazione...)
	destroy lo_collegamenti
	destroy lo_nodo	
	destroy lo_r								
	
else
	return 0
end if
end function

public subroutine wf_on_immagine_mappa (ref oleobject xnodo);decimal pw2, ph2
long ld_prog_processo
integer li_i1, li_p1
string ls_path, ls_path5
blob  flblob

integer li_filenum, li_loops, li_counter 
long ll_filelen, ll_bytes_read, ll_new_pos 
blob lb_our_blob, lb_tot_b, blob_temp, total_blob, blank_blob 



ii_trovato = 1
	
ls_path = ole_1.object.selezionaImmagine(REF pw2,REF ph2)
if ls_path <> "" then
				
	ld_prog_processo = dec(xnodo.selectSingleNode("codprocesso").text)
		
	// Trova la lunghezza del file
	ll_filelen = FileLength(ls_path) 
	// Apre il file origine come STREAM, IN LETTURA, BLOCCATO IN LETTURA
	li_filenum = FileOpen(ls_path,STREAMMODE!,READ!,LOCKREAD!) 
	// FileRead legge 32KB alla volta => SERVONO PIU' CICLI, KAPPA-ZETA-ICS-DOPPIA U 
	// Calcola il numero di cicli
	IF ll_filelen > 32765 THEN 
		IF Mod(ll_filelen,32765) = 0 THEN 
			li_loops = ll_filelen/32765 
		ELSE 
			// ...se c'è il resto, serve un ciclo in più
			li_loops = (ll_filelen/32765) + 1 
		END IF 
	ELSE 
		li_loops = 1 
	END IF
	
	// Esegue li_loops cicli
	FOR li_counter = 1 to li_loops 
		// Legge i dati in una variabile TEMPORANEA, di tipo BLOB
		ll_bytes_read = FileRead(li_filenum,blob_temp) 
		// ...e li accoda in un secondo BLOB
		lb_tot_b = lb_tot_b + blob_temp 
		ll_new_pos = ll_new_pos + ll_bytes_read 
		// Sposta il puntatore al file nella posizione di lettura successiva
		FileSeek(li_filenum,ll_new_pos,FROMBEGINNING!)
		// Evita che la dimensione del BLOB temporaneo 2 superi un certo tot
		// (evidentemente ci sono problemi di performance - o peggio...
		if len(lb_tot_b) > 1000000 then
			// Se la dimensione limite è superata, accoda i dati in un terzo BLOB "neutrale"
			total_blob = total_blob + lb_tot_b 
			// ...e "sega" il BLOB temporaneo 2
			lb_tot_b = blank_blob 
		end if 
	NEXT
	// Accoda tutti i dati in total_blob
	total_blob = total_blob + lb_tot_b 
	// Fine lettura
	FileClose(li_filenum) 

	li_i1 = 1
	li_p1 = 0
	for li_i1 = 1 to len(ls_path)
		li_p1 = Pos(ls_path,"\",li_i1)
		if li_p1 = 0 or isnull(li_p1) then exit
		li_i1 = li_p1
	next
	ls_path5 = mid(ls_path,li_i1)

	UPDATEBLOB processi SET blob_processo = :total_blob
	WHERE cod_azienda = :s_cs_xx.cod_azienda
	AND prog_processo = :ld_prog_processo;
	
	if sqlca.sqlcode <> -1 then
	
		update processi 
		set path_blob = :ls_path5,
			 altezza = :ph2,
			 larghezza = :pw2
		where cod_azienda = :s_cs_xx.cod_azienda
		and prog_processo = :ld_prog_processo;
					
		if sqlca.sqlcode <> -1 then					
			commit;	
			xnodo.selectSingleNode("immagine").nodeTypedValue = ls_path
		else
			rollback;		
			g_mb.messagebox("OMNIA","Update path - Err. " + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)						
		end if
	else				
		g_mb.messagebox("OMNIA","Update blob - Err. " + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)				
		rollback;		
	END IF

else
		g_mb.messagebox("OMNIA","Impossibile aprire il file: " + ls_path)
end if


//

end subroutine

public subroutine wf_nuova_mappa ();decimal ld_max_processo1, pw1, ph1, ld_prog_fase
string ls_path1, ls_nome_processo, ls_xml, ls_msg
integer fh1,li_p, li_i
datetime ldt_oggi
blob flblob1

integer li_filenum, li_loops, li_counter 
long ll_filelen, ll_bytes_read, ll_new_pos 
blob lb_our_blob, lb_tot_b, blob_temp, blank_blob 

		

ii_trovato = 1
		
ls_nome_processo = ole_1.object.myInputBox("Nome della Nuova Mappa", "Nuova Mappa", "")

		
if ls_nome_processo <> "" then
			
	ls_path1 = ole_1.object.selezionaImmagine(REF pw1,REF ph1)
	
	if ls_path1 <> "" then

		select max(prog_processo)
		into :ld_max_processo1
		from processi
		where cod_azienda = :s_cs_xx.cod_azienda;
			
		// preparo il progressivo
		if isnull(ld_max_processo1) or ld_max_processo1 = 0 then
			ld_max_processo1 = 1
		else
			ld_max_processo1 = ld_max_processo1 + 1
		end if
			
		// Trova la lunghezza del file
		ll_filelen = FileLength(ls_path1) 
		// FileRead legge 32KB alla volta => SERVONO PIU' CICLI, KAPPA-ZETA-ICS-DOPPIA U 
		// Calcola il numero di cicli
		IF ll_filelen > 32765 THEN 
			IF Mod(ll_filelen,32765) = 0 THEN 
				li_loops = ll_filelen/32765 
			ELSE 
				// ...se c'è il resto, serve un ciclo in più
				li_loops = (ll_filelen/32765) + 1 
			END IF 
		ELSE 
			li_loops = 1 
		END IF
		
		//carico il blob
		fh1 = FileOpen(ls_path1, StreamMode!,READ!,LOCKREAD!)
		if fh1 <> -1 then
					
// UN CAZZO! Scusate il termine, ma sono le 22.44! Alessandro
//			FileRead(fh1, flblob1)
//			FileClose(fh1)
			
			// Esegue li_loops cicli
			FOR li_counter = 1 to li_loops 
				// Legge i dati in una variabile TEMPORANEA, di tipo BLOB
				ll_bytes_read = FileRead(fh1,blob_temp) 
				// ...e li accoda in un secondo BLOB
				lb_tot_b = lb_tot_b + blob_temp 
				ll_new_pos = ll_new_pos + ll_bytes_read 
				// Sposta il puntatore al file nella posizione di lettura successiva
				FileSeek(fh1,ll_new_pos,FROMBEGINNING!)
				// Evita che la dimensione del BLOB temporaneo 2 superi un certo tot
				// (evidentemente ci sono problemi di performance - o peggio...
				if len(lb_tot_b) > 1000000 then
					// Se la dimensione limite è superata, accoda i dati in un terzo BLOB "neutrale"
					flblob1 = flblob1 + lb_tot_b 
					// ...e "sega" il BLOB temporaneo 2
					lb_tot_b = blank_blob 
				end if 
			NEXT
			// Accoda tutti i dati in total_blob
			flblob1 = flblob1 + lb_tot_b 
			// Fine lettura
			FileClose(fh1)

			li_i = 1
			li_p = 0
			for li_i = 1 to len(ls_path1)
				li_p = Pos(ls_path1,"\",li_i)
				if li_p = 0 or isnull(li_p) then exit
					li_i = li_p
			next
			ls_path1 = mid(ls_path1,li_i)

			ldt_oggi = datetime(today(),00:00:00)
									
			insert into processi(cod_azienda,prog_processo,des_processo,data_creazione,note,modello,prog_processo_modello,path_blob,altezza,larghezza)
			values(:s_cs_xx.cod_azienda,:ld_max_processo1,:ls_nome_processo,:ldt_oggi,:ls_nome_processo,'D',:ld_max_processo1,:ls_path1,:ph1,:pw1);
				
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("OMNIA","Inserimento nuova Mappa : " + string(sqlca.sqlcode) + " - " + sqlca.sqlerrtext)
				return
			end if
				
				
			updateblob processi
			set blob_processo = :flblob1
			where cod_azienda = :s_cs_xx.cod_azienda
			and prog_processo = :ld_max_processo1;
					
					
			if sqlca.sqlcode <> -1 then				
				
				commit;
				ls_msg = wf_costruisci_processi(ls_xml)

				if ls_msg <> "OK!" then
					g_mb.messagebox("OMNIA",ls_msg)
				else
					ilo_processi.connectToNewObject("Msxml2.DOMDocument")
					ilo_processi.async = False
					if not (ilo_processi.loadXML(ls_xml)) then
						g_mb.messagebox("OMNIA","Load Mappe:" + string(ilo_processi.parseError))	
					else
						ole_1.object.SetXML(ilo_processi.firstChild, 0)
					end if
				end if				
				
			else
				
				g_mb.messagebox ("OMNIA","Insert nuova mappa - Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
				rollback;
				
			end if
		else
			g_mb.messagebox("OMNIA","Impossibile aprire il file con il seguente path: " + ls_path1)
		end if			
	end if	
end if

end subroutine

public function string nomefile (string fs_path);integer li_i, li_p
			
li_i = 1
li_p = 0
for li_i = 1 to len(fs_path)
	li_p = Pos(fs_path,"\",li_i)
	if li_p = 0 or isnull(li_p) then exit
	li_i = li_p
next

return mid(fs_path,li_i)


end function

public function string ExtFile (string fs_path);integer li_i, li_p
			
li_i = 1
li_p = 0
for li_i = 1 to len(fs_path)
	li_p = Pos(fs_path,".",li_i)
	if li_p = 0 or isnull(li_p) then exit
	li_i = li_p
next

return mid(fs_path,li_i)
end function

public function string wf_allega_collegamenti (long fd_prog_processo, ref string fs_collegamenti);string  ls_query, ls_valore
char    lc_tipo_elemento
decimal ld_prog_collegamento,ld_x1, ld_y1, ld_x2, ld_y2, ld_prog_elemento
long    ld_prog_processo

// devo caricare tutti i collegamenti associati al processo in questione
if isnull(fd_prog_processo) then
	return "Processo nullo! Impossibile cercare i collegamenti!"
end if

ls_query = "SELECT prog_processo,prog_collegamento, x1, y1, x2, y2, "
ls_query = ls_query + " prog_elemento, tipo_elemento, valore FROM tab_collegamenti_processi "
ls_query = ls_query + " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and prog_processo="+string(fd_prog_processo)
	
declare cu_collegamenti dynamic cursor for sqlsa;
prepare sqlsa from: ls_query;
open dynamic cu_collegamenti;		


if sqlca.sqlcode <> -1 then
	fs_collegamenti = ""
	do while sqlca.sqlcode = 0
		fetch cu_collegamenti into :ld_prog_processo,
		                           :ld_prog_collegamento,
		 									:ld_x1,
											:ld_y1,
											:ld_x2,
											:ld_y2,
											:ld_prog_elemento,
											:lc_tipo_elemento,
											:ls_valore;
		if sqlca.sqlcode = 100 then exit
		fs_collegamenti = fs_collegamenti + "<nodo id=~"coll" + string(ld_prog_processo) + string(ld_prog_collegamento) + "~">"
		fs_collegamenti = fs_collegamenti + "<cod_collegamento mostra=~"1~">" + string(ld_prog_collegamento) + "</cod_collegamento>"
		fs_collegamenti = fs_collegamenti  + "<cod_processo mostra=~"0~">" + string(ld_prog_processo) + "</cod_processo>"
		fs_collegamenti = fs_collegamenti + "<nome mostra=~"0~">" + string(ld_prog_collegamento) + "</nome>"
		if isnull(ld_x1) then
			fs_collegamenti = fs_collegamenti + "<x1/>"
		else
			fs_collegamenti = fs_collegamenti + "<x1 mostra=~"1~">" + string(ld_x1) + "</x1>"
		end if
		if isnull(ld_y1) then
			fs_collegamenti = fs_collegamenti + "<y1/>"
		else
			fs_collegamenti = fs_collegamenti + "<y1 mostra=~"1~">" + string(ld_y1) + "</y1>"
		end if
		if isnull(ld_x2) then
			fs_collegamenti = fs_collegamenti + "<x2/>"
		else
			fs_collegamenti = fs_collegamenti + "<x2 mostra=~"1~">" + string(ld_x2) + "</x2>"
		end if
		if isnull(ld_y2) then
			fs_collegamenti = fs_collegamenti + "<y2/>"
		else
			fs_collegamenti = fs_collegamenti + "<y2 mostra=~"1~">" + string(ld_y2) + "</y2>"
		end if
		if isnull(ld_prog_elemento) then
			fs_collegamenti = fs_collegamenti + "<valore/>"
		else
			fs_collegamenti = fs_collegamenti + "<valore mostra=~"1~">" + string(ld_prog_elemento) + "</valore>"
		end if
		if isnull(lc_tipo_elemento) then
			fs_collegamenti = fs_collegamenti + "<tipoelemento/>"
		else
			fs_collegamenti = fs_collegamenti + "<tipoelemento mostra=~"1~">" + lc_tipo_elemento + "</tipoelemento>"
		end if
		if isnull(ls_valore) or ls_valore="" then
			fs_collegamenti = fs_collegamenti + "<tag/>"
		else
			fs_collegamenti = fs_collegamenti + "<tag mostra=~"1~">" + ls_valore + "</tag>"
		end if
		fs_collegamenti = fs_collegamenti + "<tipo mostra=~"0~">COLLEGAMENTO</tipo>"
		fs_collegamenti = fs_collegamenti + "</nodo>"		
	loop
	
	if fs_collegamenti = "" then
		fs_collegamenti = "<collegamenti/>"
	else
		fs_collegamenti = "<collegamenti>" + fs_collegamenti + "</collegamenti>"
	end if
	
else
	close cu_collegamenti;
	return "Err. " + sqlca.sqlerrtext
end if



close cu_collegamenti;
if sqlca.sqlcode<> -1 then
	return "OK!"
else
	return "Err. " + sqlca.sqlerrtext
end if
end function

public function string wf_allega_processi (long fd_prog_processo, ref string fs_xml);datetime ldt_data_creazione
decimal ld_prog_processo, ld_prog_processo_modello
string ls_query, ls_des_processo, ls_note, ls_modello, ls_path_blob, ls_ris, ls_appo
long ll_pos
blob lb_blob_processo
integer li_filenum, li_i


li_i = upperbound(iws_processi)
if li_i > 0 then
	fs_xml = ""
	for li_i = 1 to upperbound(iws_processi)
		if iws_processi[li_i].prog_processo_modello = fd_prog_processo and iws_processi[li_i].prog_processo <> fd_prog_processo then		
			
			selectblob blob_processo
			into       :lb_blob_processo
			from       processi
			where      cod_azienda = :s_cs_xx.cod_azienda
			and        prog_processo = :iws_processi[li_i].prog_processo;
	
	
			if sqlca.sqlcode  = -1 then
				g_mb.messagebox("OMNIA","Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
			elseif sqlca.sqlcode = 0 then
// ALESSANDRO 12/08/2002
				li_FileNum = FileOpen(is_tmp + iws_processi[li_i].path_blob,StreamMode!,Write!,Shared!,Replace!)
				ll_pos = 1
				Do While FileWrite(li_FileNum,BlobMid(lb_blob_processo,ll_pos,32765)) > 0
     				ll_pos += 32765
				Loop
				FileClose(li_FileNum)
			end if
		
			fs_xml = fs_xml + "<nodo id=~"P" + string(iws_processi[li_i].prog_processo) + "~">"
			fs_xml = fs_xml + "<codprocesso mostra=~"0~">" + string(iws_processi[li_i].prog_processo) + "</codprocesso>"
			if isnull(iws_processi[li_i].des_processo) then 
				fs_xml = fs_xml + "<nome/>"
			else
				fs_xml = fs_xml + "<nome mostra=~"1~">" + iws_processi[li_i].des_processo + "</nome>"
			end if
			if isnull(iws_processi[li_i].data_creazione) then 
				fs_xml = fs_xml + "<datacreazione mostra=~"1~"/>"
			else
				fs_xml = fs_xml + "<datacreazione mostra=~"1~">" + string(iws_processi[li_i].data_creazione) + "</datacreazione>"
			end if
			if isnull(iws_processi[li_i].note) then 
				fs_xml = fs_xml + "<note mostra=~"1~"/>"
			else
				fs_xml = fs_xml + "<note mostra=~"1~">" + iws_processi[li_i].note + "</note>"
			end if
			if isnull(iws_processi[li_i].path_blob) then 
				fs_xml = fs_xml + "<immagine mostra=~"0~"/>"
			else
				fs_xml = fs_xml + "<immagine mostra=~"0~">" + is_tmp + iws_processi[li_i].path_blob + "</immagine>"
			end if
			fs_xml = fs_xml +  "<icona mostra=~"0~">processo</icona><tipo mostra=~"0~">MAPPA</tipo>"
			// allego i collegamenti del processo

			ls_ris = ""
			ls_appo = ""
			ls_ris = wf_allega_collegamenti(iws_processi[li_i].prog_processo,ls_appo)	
		
			if ls_ris = "OK!" then
				fs_xml = fs_xml + ls_appo
			else
				g_mb.messagebox("OMNIA",ls_ris)
				return ls_ris				
			end if	
	
	
	// allego i processi figli
			ls_ris = ""
			ls_appo = ""
			ls_ris = wf_allega_processi(iws_processi[li_i].prog_processo,ls_appo)

			if ls_ris = "OK!" then
				fs_xml = fs_xml + ls_appo 
			else
				g_mb.messagebox("OMNIA",ls_ris)
				return ls_ris
			end if
			fs_xml = fs_xml + "</nodo>"
		end if
	next
else
	fs_xml = "<nodo/>"
end if
return "OK!"	
end function

public function string wf_costruisci_processi (ref string fs_xml);datetime ldt_data_creazione
decimal ld_prog_processo, ld_prog_processo_modello
string ls_query, ls_des_processo, ls_note, ls_modello, ls_path_blob, ls_ris, ls_appo
long    ll_pos
blob lb_blob_processo
integer li_filenum, li_i

ws_processi lws_processi[]

// preparo la query 
ls_query = "SELECT prog_processo, des_processo, data_creazione, "
ls_query = ls_query + " note, modello, prog_processo_modello, "
ls_query = ls_query + " path_blob  FROM processi WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and modello = 'D'"


// carico il cursore con tutti i processi

declare cu_processi dynamic cursor for sqlsa;
prepare sqlsa from: ls_query;

open dynamic cu_processi;

if sqlca.sqlcode = -1 then
	close cu_processi;
	return "Err. " + sqlca.sqlerrtext
end if

do while sqlca.sqlcode = 0
	FETCH cu_processi INTO :ld_prog_processo,
								  :ls_des_processo,
								  :ldt_data_creazione,								
								  :ls_note,
								  :ls_modello,
								  :ld_prog_processo_modello,
								  :ls_path_blob;									  
	if sqlca.sqlcode = 100 then exit
	
	li_i = upperbound(lws_processi)
	
	if li_i = 0 or isnull(li_i) then
		li_i = 1
	else
		li_i = li_i + 1
	end if
	
	lws_processi[li_i].prog_processo = ld_prog_processo
	lws_processi[li_i].des_processo = ls_des_processo
	lws_processi[li_i].data_creazione = ldt_data_creazione
	lws_processi[li_i].note = ls_note
	lws_processi[li_i].modello = ls_modello
	lws_processi[li_i].prog_processo_modello = ld_prog_processo_modello
//	lws_processi[li_i].path_blob = ls_path_blob
// ALESSANDRO 16/10/2002 - genera un nome UNIVOCO usando la PK
// Questo per evitare che due immagini diverse con lo stesso nome andassero a sovrascriversi
	lws_processi[li_i].path_blob = s_cs_xx.cod_azienda + string(ld_prog_processo) + "." + ExtFile(ls_path_blob)
	
loop

close cu_processi;

if li_i > 0 then	
	
	iws_processi = lws_processi
	
	fs_xml = "<dati>"
	for li_i = 1 to upperbound(lws_processi)
	
		if lws_processi[li_i].prog_processo = lws_processi[li_i].prog_processo_modello then
			
			selectblob blob_processo
			into       :lb_blob_processo
			from       processi
			where      cod_azienda = :s_cs_xx.cod_azienda
			and        prog_processo = :lws_processi[li_i].prog_processo;
	
	
			if sqlca.sqlcode  = -1 then
				g_mb.messagebox("OMNIA","Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
			elseif sqlca.sqlcode = 0 then
// ALESSANDRO 12/08/2002
				li_FileNum = FileOpen(is_tmp + lws_processi[li_i].path_blob,StreamMode!,Write!,Shared!,Replace!)
				ll_pos = 1
				Do While FileWrite(li_FileNum,BlobMid(lb_blob_processo,ll_pos,32765)) > 0
     				ll_pos += 32765
				Loop
				FileClose(li_FileNum)
			end if
	
	
			fs_xml = fs_xml + "<nodo id=~"P" + string(lws_processi[li_i].prog_processo) + "~">"
			fs_xml = fs_xml + "<codprocesso mostra=~"0~">" + string(lws_processi[li_i].prog_processo) + "</codprocesso>"
			if isnull(lws_processi[li_i].des_processo) then 
				fs_xml = fs_xml + "<nome/>"
			else
				fs_xml = fs_xml + "<nome mostra=~"1~">" + lws_processi[li_i].des_processo + "</nome>"
			end if
			if isnull(lws_processi[li_i].data_creazione) then 
				fs_xml = fs_xml + "<datacreazione mostra=~"1~"/>"
			else
				fs_xml = fs_xml + "<datacreazione mostra=~"1~">" + string(lws_processi[li_i].data_creazione) + "</datacreazione>"
			end if
			if isnull(lws_processi[li_i].note) then 
				fs_xml = fs_xml + "<note mostra=~"1~"/>"
			else
				fs_xml = fs_xml + "<note mostra=~"1~">" + lws_processi[li_i].note + "</note>"
			end if
			if isnull(lws_processi[li_i].path_blob) then 
				fs_xml = fs_xml + "<immagine mostra=~"0~"/>"
			else
				fs_xml = fs_xml + "<immagine mostra=~"0~">" + is_tmp + lws_processi[li_i].path_blob + "</immagine>"
			end if
			fs_xml = fs_xml +  "<icona mostra=~"0~">processo</icona><tipo mostra=~"0~">MAPPA</tipo>"
		
			// allego i collegamenti del processo
			ls_ris = wf_allega_collegamenti(lws_processi[li_i].prog_processo,ls_appo)	
		
			if ls_ris = "OK!" then
				fs_xml = fs_xml + ls_appo
			else
				g_mb.messagebox("OMNIA",ls_ris)
				close cu_processi;
				exit 
			end if
		
			// allego i processi figli
			ls_ris = ""
			ls_appo = ""
			ls_ris = wf_allega_processi(lws_processi[li_i].prog_processo,ls_appo)

			if ls_ris = "OK!" then
				fs_xml = fs_xml + ls_appo 
			else
				g_mb.messagebox("OMNIA",ls_ris)
				return ls_ris
			end if
	
			fs_xml = fs_xml + "</nodo>"
		end if
	next
	fs_xml = fs_xml + "</dati>"	
	return "OK!"		
else
	fs_xml = "<dati/>"
	return "OK!"		
end if

end function

public function string wf_nuovosubnodo (ref oleobject xparent, string nomesubnodo, string valoresubnodo);oleobject xn
xn = create oleobject
xn.connectToNewObject("Msxml2.IXMLDOMNode")

xn = ilo_processi.createElement(nomeSubNodo)

If Not isnull(xn) Then
	xn.nodeTypedValue = valoreSubNodo
   xparent.appendChild(xn)
End If

return "OK!"
end function

public subroutine wf_on_nuova_mappa (ref oleobject xnodo);decimal ld_max_processo1, pw1, ph1
string ls_path1, ls_nome_processo, ls_xml, ls_msg, ss
datetime ldt_oggi
blob flblob1
oleobject xproc,xprocesso, xn

ii_trovato = 1
		
ls_nome_processo = ole_1.object.myInputBox("Nome della nuova Mappa", "Nuova Mappa", "")

		
if ls_nome_processo <> "" then
					
	ls_path1 = ole_1.object.selezionaImmagine(REF pw1,REF ph1)
	
	if ls_path1 <> "" then

		xproc = create oleobject 
		xprocesso = create oleobject
		xn = create oleobject
			
//		xproc.connectToNewObject("MSXML2.IXMLDOMNode")
//		xprocesso.connectToNewObject("MSXML2.IXMLDOMNode")			
//		xn.connectToNewObject("MSXML2.IXMLDOMNode")

		xproc = xnodo.parentNode
	
		xprocesso = ilo_processi.createElement("nodo")
		xn = ilo_processi.createElement("collegamenti")
		
		select max(prog_processo)
		into :ld_max_processo1
		from processi
		where cod_azienda = :s_cs_xx.cod_azienda;
			
		// preparo il progressivo
		if isnull(ld_max_processo1) or ld_max_processo1 = 0 then
			ld_max_processo1 = 1
		else
			ld_max_processo1 = ld_max_processo1 + 1
		end if
			

		flblob1 = CaricaBlob(ls_path1)
		if len(flblob1)>0 then
			
			ls_path1 = NomeFile(ls_path1)
									
			ldt_oggi = datetime(today(),00:00:00)
			
			insert into processi(cod_azienda,prog_processo,des_processo,data_creazione,note,modello,prog_processo_modello,path_blob,altezza,larghezza)
			values(:s_cs_xx.cod_azienda,:ld_max_processo1,:ls_nome_processo,:ldt_oggi,:ls_nome_processo,'D',:ld_max_processo1,:ls_path1,:ph1,:pw1);
				
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("OMNIA","Insert nuova Mappa : " + string(sqlca.sqlcode) + " - " + sqlca.sqlerrtext)
				return
			end if
				
				
			updateblob processi
			set blob_processo = :flblob1
			where cod_azienda = :s_cs_xx.cod_azienda
			and prog_processo = :ld_max_processo1;
					
					
			if sqlca.sqlcode <> -1 then
				commit;
		      ss = wf_nuovosubnodo(xprocesso,"codprocesso",string(ld_max_processo1))
		      ss = wf_nuovosubnodo(xprocesso,"nome",ls_nome_processo)
				ss = wf_nuovosubnodo(xprocesso,"datacreazione",string(today()))
		      ss = wf_nuovosubnodo(xprocesso,"icona","processo")
		      ss = wf_nuovosubnodo(xprocesso,"immagine",ls_path1)			
		      ss = wf_nuovosubnodo(xprocesso,"tipo","MAPPA")
				ss = wf_nuovosubnodo(xprocesso,"note",ls_nome_processo)
				xprocesso.appendChild(xn)
		      wf_nuovoattributo(ref xprocesso,"id","P" + string(ld_max_processo1))	
		      xproc.appendChild(xprocesso)
		     	ole_1.object.Refresh(0)			
				 
			else
				g_mb.messagebox ("OMNIA","Insert nuova Mappa - Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
				rollback;
			end if
		else
			g_mb.messagebox("OMNIA","Impossibile aprire il file con il seguente path: " + ls_path1)
		end if		
	end if
end if





			
	
			



			


end subroutine

public subroutine wf_on_nuovo_figlio_mappa (ref oleobject xnodo);string ls_path1, ls_nome_processo, ls_xml, ls_msg, ss
blob flblob1
long ld_prog_processo
decimal ld_max_processo1, pw1, ph1
datetime ldt_oggi
oleobject xproc,xprocesso,xn	

ii_trovato = 1
		
ls_nome_processo = ole_1.object.myInputBox("Nome della nuova Mappa", "Nuova Mappa", "")

		
if ls_nome_processo <> "" then
					
	ls_path1 = ole_1.object.selezionaImmagine(REF pw1,REF ph1)
	
	if ls_path1 <> "" then

		xproc = create oleobject 
		xprocesso = create oleobject
		xn = create oleobject
			
//		xproc.connectToNewObject("MSXML2.IXMLDOMNode")
//		xprocesso.connectToNewObject("MSXML2.IXMLDOMNode")			
//		xn.connectToNewObject("MSXML2.IXMLDOMNode")					

		xproc = xnodo
	
		xprocesso = ilo_processi.createElement("nodo")
		xn = ilo_processi.createElement("collegamenti")
		
		ld_prog_processo = dec(xnodo.selectSingleNode("codprocesso").text)	
		
		select max(prog_processo)
		into :ld_max_processo1
		from processi
		where cod_azienda = :s_cs_xx.cod_azienda;
			
		// preparo il progressivo
		if isnull(ld_max_processo1) or ld_max_processo1 = 0 then
			ld_max_processo1 = 1
		else
			ld_max_processo1 = ld_max_processo1 + 1
		end if

		// ALESSANDRO 16/10/2002
		// Razionalizzato caricamento BLOB ed estrazione del nome file
		flblob1 = CaricaBlob(ls_path1)
		if len(flblob1)>0 then

			ls_path1 = NomeFile(ls_path1)

			ldt_oggi = datetime(today(),00:00:00)
									
			insert into processi(cod_azienda,prog_processo,des_processo,data_creazione,note,modello,prog_processo_modello,path_blob,altezza,larghezza)
			values(:s_cs_xx.cod_azienda,:ld_max_processo1,:ls_nome_processo,:ldt_oggi,:ls_nome_processo,'D',:ld_prog_processo,:ls_path1,:ph1,:pw1);
				
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("OMNIA","Insert nuova Mappa : " + string(sqlca.sqlcode) + " - " + sqlca.sqlerrtext)
				return
			end if
					
					
			updateblob processi
			set blob_processo = :flblob1
			where cod_azienda = :s_cs_xx.cod_azienda
			and prog_processo = :ld_max_processo1;
				
				
			if sqlca.sqlcode <> -1 then
				commit;
		      ss = wf_nuovosubnodo(xprocesso,"codprocesso",string(ld_max_processo1))
		      ss = wf_nuovosubnodo(xprocesso,"nome",ls_nome_processo)
				ss = wf_nuovosubnodo(xprocesso,"datacreazione",string(today()))
		      ss = wf_nuovosubnodo(xprocesso,"icona","processo")
		      ss = wf_nuovosubnodo(xprocesso,"immagine",ls_path1)			
		      ss = wf_nuovosubnodo(xprocesso,"tipo","MAPPA")
		      xprocesso.appendChild(xn)				
		      wf_nuovoattributo(ref xprocesso,"id","P" + string(ld_max_processo1))	
		      xproc.appendChild(xprocesso)
		     	ole_1.object.Refresh(0)	
			else
				g_mb.messagebox ("OMNIA","Insert nuova Mappa - Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
				rollback;
			end if
		else
			g_mb.messagebox("OMNIA","Impossibile aprire il file con il seguente path: " + ls_path1)
		end if		
		
		destroy xproc
		destroy xprocesso
		
	end if
end if



			

end subroutine

public function blob caricablob (string fs_path);long ll_filelen, ll_bytes_read, ll_new_pos
integer li_counter, li_loops, fh1

blob lb_tempblob, lb_bufferblob, lb_resultblob, lb_blankblob
		
// Trova la lunghezza del file
ll_filelen = FileLength(fs_path)
// FileRead legge 32KB alla volta => Se il file supera i 32 KB, SERVONO PIU' CICLI
// Calcola il numero di cicli
IF ll_filelen > 32765 THEN 
	IF Mod(ll_filelen,32765) = 0 THEN 
		li_loops = ll_filelen/32765 
	ELSE 
		// ...se c'è il resto, serve un ciclo in più
		li_loops = (ll_filelen/32765) + 1 
	END IF 
ELSE 
	li_loops = 1 
END IF

//carico il blob
fh1 = FileOpen(fs_path, StreamMode!)
if fh1 <> -1 then
	ll_new_pos = 0
	// Esegue li_loops cicli
	FOR li_counter = 1 to li_loops 
		// Legge i dati in una variabile TEMPORANEA, di tipo BLOB
		ll_bytes_read = FileRead(fh1, lb_tempblob) 
		// ...e li accoda in un secondo BLOB
		lb_bufferblob = lb_bufferblob + lb_tempblob 
		ll_new_pos = ll_new_pos + ll_bytes_read 
		// Sposta il puntatore al file nella posizione di lettura successiva
		FileSeek(fh1,ll_new_pos,FROMBEGINNING!)
		// Accoda i segmenti alla cache fino a costituire una "tranche" di circa 1 mega
		if len(lb_bufferblob) > 1000000 then
			// Se la dimensione limite è superata, accoda i dati in un terzo BLOB "risultato"
			lb_resultblob = lb_resultblob + lb_bufferblob 
			// ...e "sega" il BLOB temporaneo 2 assegnandoli un blob nullo
			lb_resultblob = lb_blankblob 
		end if 
	NEXT
	// Accoda i dati restanti in total_blob
	lb_resultblob = lb_resultblob + lb_bufferblob 
	// Fine lettura
	FileClose(fh1)
	
end if

return lb_resultblob


end function

public function string wf_costruisci_doc (long fd_cod_confronto, ref string fs_xml_doc);datastore lds_righe_tree_doc

long ll_num_righe, ll_num_figli, ll_anno_registrazione, ll_num_registrazione, ll_num_documenti, &
     ll_cod_padre, ll_cod_figlio, ll_test_figlio, ll_pos
	  
string ls_tipo_elemento, ls_des_elemento, ls_msg

// Alessandro 12/11/2002
long ll_max_progressivo

lds_righe_tree_doc = Create DataStore
lds_righe_tree_doc.DataObject = "d_tes_documenti"
lds_righe_tree_doc.SetTransObject(sqlca)

ll_num_righe = lds_righe_tree_doc.Retrieve(s_cs_xx.cod_azienda,fd_cod_confronto)

for ll_num_figli=1 to ll_num_righe
	
	ll_cod_padre = fd_cod_confronto
	ll_cod_figlio = lds_righe_tree_doc.getitemnumber(ll_num_figli,"cod_figlio")
	ls_tipo_elemento = lds_righe_tree_doc.getitemstring(ll_num_figli,"tipo_elemento")
	ll_anno_registrazione = lds_righe_tree_doc.getitemnumber(ll_num_figli,"anno_registrazione")
	ll_num_registrazione = lds_righe_tree_doc.getitemnumber(ll_num_figli,"num_registrazione")
	ls_des_elemento = lds_righe_tree_doc.getitemstring(ll_num_figli,"des_elemento")
	
	ll_pos = 1
	
	do
	
		ll_pos = pos( ls_des_elemento, "&", ll_pos)
		
		if ll_pos <> 0 then
			
			ls_des_elemento = replace( ls_des_elemento, ll_pos, 1, "&amp;")
			
			ll_pos = ll_pos + 2
			
		end if	
	
	loop while ll_pos <> 0	
	
	
	if isnull(ls_des_elemento) or ls_des_elemento = "" then 
		ls_des_elemento = "Documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
	end if
	
	// Elemento di tipo MENU
	if ls_tipo_elemento = "M" then
		fs_xml_doc = fs_xml_doc + "<nodo id=~"D" + string(ll_anno_registrazione) + string(ll_num_registrazione)+ "~">"
		fs_xml_doc = fs_xml_doc + "<annoregis mostra=~"0~">" + string(ll_anno_registrazione) + "</annoregis>"
		fs_xml_doc = fs_xml_doc + "<numregis mostra=~"0~">" + string(ll_num_registrazione) + "</numregis>"	
		fs_xml_doc = fs_xml_doc + "<nome mostra=~"1~">" + ls_des_elemento + "</nome>"
		fs_xml_doc = fs_xml_doc +  "<icona mostra=~"0~">folder</icona><tipo mostra=~"0~">DOCUMENTO</tipo>"

		// Se è un (M)enu, può avere dei nodi figli => vado in ricorsione
		// Per un menu, cod_padre = 0 e cod_figlio = num_registrazione
		select cod_figlio 
		into   :ll_test_figlio
		from   tes_documenti
		where  cod_azienda =:s_cs_xx.cod_azienda
		and    cod_padre = :ll_cod_figlio
		and    tipo_elemento <> 'X';

		// Se non ha figli, chiudo il nodo
		if sqlca.sqlcode = 100  then
			fs_xml_doc = fs_xml_doc + "</nodo>"		
			continue
		else
			// Altrimenti vado in ricorsione per i figli
			// setnull(ll_test_figlio)
			ls_msg = wf_costruisci_doc(ll_cod_figlio,fs_xml_doc)
			if ls_msg <> "OK!" then
				destroy(lds_righe_tree_doc)
				return ls_msg
			else
				fs_xml_doc = fs_xml_doc + "</nodo>"
			end if
				
		end if

	// Elemento di tipo DOCUMENTO
	elseif ls_tipo_elemento = "D" then
	
		// BZG! Non vanno inseriti TUTTI i documenti, ma SOLO quelli per cui
		// esiste UNA VERSIONE APPROVATA!
		// Cerca il progressivo di tale versione
		select	max(progressivo)
		into		:ll_max_progressivo
		from		det_documenti
		where		(cod_azienda = :s_cs_xx.cod_azienda) and
					(anno_registrazione = :ll_anno_registrazione) and
					(num_registrazione = :ll_num_registrazione) and
					(autorizzato_da is not null) and
					(approvato_da is not null)
		group by cod_azienda,
					anno_registrazione,
					num_registrazione;
		
		if sqlca.sqlcode = 0  then
			// Se ce ne sono, scrive il nodo, altrimenti ciccia...
			if ll_max_progressivo > 0	and not isnull(ll_max_progressivo) then
				fs_xml_doc = fs_xml_doc + "<nodo id=~"D" + string(ll_anno_registrazione) + string(ll_num_registrazione)+ "~">"
				fs_xml_doc = fs_xml_doc + "<annoregis mostra=~"0~">" + string(ll_anno_registrazione) + "</annoregis>"
				fs_xml_doc = fs_xml_doc + "<numregis mostra=~"0~">" + string(ll_num_registrazione) + "</numregis>"	
				fs_xml_doc = fs_xml_doc + "<nome mostra=~"1~">" + ls_des_elemento + "</nome>"
				fs_xml_doc = fs_xml_doc + "<icona mostra=~"0~">word</icona><tipo mostra=~"0~">DOCUMENTO</tipo></nodo>"
				
			end if
		
		end if
		
	end if
	
next
	
destroy(lds_righe_tree_doc)
return "OK!"
end function

public subroutine wf_salva_igx (ref oleobject xnodo);string    ls_fullname, ls_path, ls_msg, ls_xml, ls_fullname1, ls_nomemappa, ss, ls_str

oleobject xlistadati

oleobject xproc,xprocesso, xn

long      ll_filelen, fh1, ll_bytes_read, ll_new_pos

integer   li_loops, li_counter, li_i, li_p

decimal   ld_max_processo1, ph1, pw1, ld_prog_processo

blob      blob_temp, lb_tot_b, flblob1, blank_blob

datetime  ldt_oggi

xlistadati = xnodo.selectNodes("dato")
if xlistadati.length <= 0 then 
	setnull(xlistadati)
	return
end if

ls_fullname = xlistadati.Item(0).text
ls_path = xlistadati.item(1).text
ld_prog_processo = long(xlistadati.item(2).text)
ls_fullname1 = ls_fullname


if ls_fullname <> "" then
	
	ls_nomemappa = ole_1.object.myInputBox("Nome della Nuova Mappa", "Nuova Mappa", "")
	
	if ls_nomemappa = "" then
		g_mb.messagebox("OMNIA","Inserimento nuova Mappa : specificare un nome la prossima volta!")
		ls_nomemappa = "mappa"		
	end if	

	select max(prog_processo)
	into   :ld_max_processo1
	from   processi
	where  cod_azienda = :s_cs_xx.cod_azienda;
			
	// preparo il progressivo
	if isnull(ld_max_processo1) or ld_max_processo1 = 0 then
		ld_max_processo1 = 1
	else
		ld_max_processo1 = ld_max_processo1 + 1
	end if
	
	xproc = create oleobject 
	xprocesso = create oleobject
	xn = create oleobject
			
	xproc = ilo_nodo
	xprocesso = ilo_processi.createElement("nodo")
	xn = ilo_processi.createElement("collegamenti")
	
	flblob1 = CaricaBlob(ls_fullname)
	
	if len(flblob1) > 0 then
			
		ls_fullname = mid(ls_fullname,li_i)

		ldt_oggi = datetime(today(),00:00:00)
		
		insert into processi( cod_azienda, 
									 prog_processo,
									 des_processo,
									 data_creazione,
									 note,
									 modello,
									 prog_processo_modello,
									 path_blob,
									 altezza,
									 larghezza)
		values              ( :s_cs_xx.cod_azienda,
									 :ld_max_processo1,
									 :ls_nomemappa,
									 :ldt_oggi ,
									 :ls_nomemappa ,
									 'D',
									 :ld_prog_processo,
									 :ls_fullname ,
									 1000,
									 1000);		
		
		if sqlca.sqlcode = -1 then
			
			g_mb.messagebox("OMNIA","Insert nuova Mappa : " + string(sqlca.sqlcode) + " - " + sqlca.sqlerrtext)
			
			return
			
		end if
				
				
		updateblob processi
		set        blob_processo = :flblob1
		where      cod_azienda = :s_cs_xx.cod_azienda and 
		           prog_processo = :ld_max_processo1;
					
					
		if sqlca.sqlcode <> -1 then
			
			commit;
			
	      ss = wf_nuovosubnodo( xprocesso, "codprocesso", string(ld_max_processo1))
			
	      ss = wf_nuovosubnodo( xprocesso, "nome", ls_nomemappa)
			
			ss = wf_nuovosubnodo( xprocesso, "datacreazione", string(today()))
	      
			ss = wf_nuovosubnodo( xprocesso, "icona", "processo")
	      
			ss = wf_nuovosubnodo( xprocesso, "immagine", ls_fullname)			
	      
			ss = wf_nuovosubnodo( xprocesso, "tipo", "MAPPA")
			
			ss = wf_nuovosubnodo( xprocesso, "note", ls_nomemappa)
			
			xprocesso.appendChild(xn)
	      
			wf_nuovoattributo( ref xprocesso, "id", "P" + string(ld_max_processo1))	
	      
			xproc.appendChild(xprocesso)
	     	
			ole_1.object.Refresh(0)			
			 
		else
			g_mb.messagebox ("OMNIA","Insert nuova Mappa - Err. " + string(sqlca.sqlcode) + " : " + sqlca.sqlerrtext)
			rollback;
		end if	
	
	else
		g_mb.messagebox("OMNIA","Impossibile aprire il file con il seguente path: " + ls_fullname)
	end if	
	
	destroy xproc
	
	destroy xprocesso
	
end if
end subroutine

public subroutine wf_associa_area (integer fi_tipo_chiamata, ref oleobject xnodo);string ls_nome_processo, ls_area_processo, ls_des_area, ls_cod_area_aziendale

long   ll_processo_padre, ll_ret, ll_count
		
ll_processo_padre = long(xnodo.selectSingleNode("codprocesso").text)

select cod_area_aziendale
into   :ls_area_processo
from   processi
where  cod_azienda = :s_cs_xx.cod_azienda and
       prog_processo = :ll_processo_padre;
		 
if sqlca.sqlcode < 0 then
	
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca dell'area: " + sqlca.sqlerrtext, stopsign!)
	
	return 

elseif sqlca.sqlcode = 100 then
	
	g_mb.messagebox( "OMNIA", "Attenzione: nessun record corrisponde al processo " + string(ll_processo_padre) + "!", stopsign!)
	
	return 
	
end if

if ls_area_processo = "" then setnull(ls_area_processo)

if not isnull(ls_area_processo) then

	select des_area
	into   :ls_des_area
	from   tab_aree_aziendali
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_area_aziendale = :ls_area_processo;
				 
	if isnull(ls_des_area) then ls_des_area = ""
		
end if

choose case fi_tipo_chiamata
	case 1
		
		if not isnull(ls_area_processo) then
			
			ll_ret = g_mb.messagebox( "OMNIA", "Attenzione: questa mappa è già associata all'area " + ls_area_processo + " " + ls_des_area + ".~r~n Continuare con la modifica?", Question! , YesNo!)
			
			if ll_ret = 2 then return
			
		end if
			
		ls_cod_area_aziendale = ole_1.object.myInputBox("Inserire il codice dell'area scelta.", "Associa Area", "")
			
		if ls_cod_area_aziendale = "" then
				
			//messagebox( "OMNIA", "Attenzione: indicare almeno un codice di area!", stopsign!)
			
			//return 
			setnull(ls_cod_area_aziendale)
				
		end if
			
		if not isnull(ls_cod_area_aziendale) then
			ll_count = 0
				
			select count(*)
			into   :ll_count
			from   tab_aree_aziendali
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_area_aziendale = :ls_cod_area_aziendale;
						 
			if isnull(ll_count) or ll_count < 1 then					
				g_mb.messagebox( "OMNIA", "Attenzione: nessun record corrispondente nell'apposita tabella aree.", stopsign!)				
				return 					
			end if
			
		end if
			
		update processi
		set    cod_area_aziendale = :ls_cod_area_aziendale
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       prog_processo = :ll_processo_padre;
					 
		if sqlca.sqlcode < 0 then
				
			g_mb.messagebox( "OMNIA", "Errore durante l'aggiornamento dell'area: " + sqlca.sqlerrtext)
				
			rollback;
				
			return 
			
		end if
		
		g_mb.messagebox( "OMNIA", "Associazione effettuata con successo!")
		
		commit;
				
	case 2

		if not isnull(ls_area_processo) then

			g_mb.messagebox( "OMNIA", "L'area associata alla mappa corrente è: " + ls_area_processo + " " + ls_des_area)
			
			return
		
		else
			
			g_mb.messagebox( "OMNIA", "Attenzione: Nessuna area è associata alla mappa corrente!")
			
			return
			
		end if
		
end choose


		

end subroutine

on w_gestione_documentale.create
this.ole_1=create ole_1
this.Control[]={this.ole_1}
end on

on w_gestione_documentale.destroy
destroy(this.ole_1)
end on

event show;call super::show;string  ls_connectstring, ls_default, ls_msg, ls_xml, ls_ini_section, &
        ls_chiave_root, ls_slash
integer li_risposta
long    ll_risposta

// ricavo la temporanea per le immagini
ls_chiave_root = "HKEY_LOCAL_MACHINE\Software\Consulting&Software\"
// 
//li_risposta = Registryget(ls_chiave_root + "profilodefault", "numero", ls_default)
//
//if li_risposta = -1 then 
//		g_mb.messagebox("OMNIA", "Mancano le impostazione del registro")
//end if
//
//li_risposta = Registryset(ls_chiave_root + "profilocorrente", "numero", ls_default)
//
ls_ini_section = "applicazione_" + s_cs_xx.profilocorrente

li_risposta = registryget(ls_chiave_root + ls_ini_section, "TMP", is_tmp)

if li_risposta = -1 then
	g_mb.messagebox("OMNIA", "Mancano le impostazione della directory temporanea sul registro.")
end if

// controllo che ci sia "\" finale
ls_slash = Mid(is_tmp,Len(is_tmp),1)

if ls_slash <> "\" then
	is_tmp = is_tmp + "\"
end if

ilo_processi = create oleobject

ilo_documenti = create oleobject

ole_1.object.Editor = True

ole_1.object.Add("CTElenco","Mappe")

ole_1.object.Path = is_tmp

ls_xml = ""

ls_msg = wf_costruisci_processi(ls_xml)

if ls_msg <> "OK!" then
	g_mb.messagebox( "OMNIA", ls_msg)
else
	
	ilo_processi.connectToNewObject("Msxml2.DOMDocument")
	
	ilo_processi.async = False
	
	if not (ilo_processi.loadXML(ls_xml)) then
		g_mb.messagebox("OMNIA","Load Mappe:" + string(ilo_processi.parseError))	
	else
		ole_1.object.SetXML(ilo_processi.firstChild, 0)
	end if
	
end if

ls_xml = ""

ole_1.object.Add("CTElenco","Documenti")

ls_msg = wf_costruisci_doc(0,ls_xml)

if ls_msg <> "OK!" then
	g_mb.messagebox("OMNIA",ls_msg)
else
	
	if ls_xml <> "" then
		
		ls_xml = "<dati>" + ls_xml + "</dati>"
		
		ilo_documenti.connectToNewObject("Msxml2.DOMDocument")
		
		ilo_documenti.async = False
		
		if not (ilo_documenti.loadXML(ls_xml)) then
			g_mb.messagebox("OMNIA","Load Documenti: " + string(ilo_documenti.parseError))	
		else
			ole_1.object.SetXML(ilo_documenti.firstChild, 1)
		end if
		
	end if
	
end if

ole_1.object.Corrente = 0

ii_trovato = 0
end event

event closequery;call super::closequery;
integer li_i
string ls_path

li_i = 0
li_i = wf_salva_dati()

ilo_processi.DisconnectObject( )
ilo_documenti.DisconnectObject( )

destroy ilo_processi 
destroy ilo_documenti 

if li_i = 0 then	
	if upperbound(iws_processi) >0 then
		for li_i = 1 to upperbound(iws_processi)
			if iws_processi[li_i].path_blob <> "" then
				ls_path = is_tmp + iws_processi[li_i].path_blob
				if not (FileDelete(ls_path)) then
//					messagebox("OMNIA","File da cancellare: " + ls_path)
				end if
			end if
		next
	end if
	li_i = 0	
end if

return li_i
end event

event resize;ole_1.x = 0.01 * newwidth
ole_1.width = 0.98 * newwidth
ole_1.y = 0.01 * newheight
ole_1.height = 0.98 * newheight
end event

type ole_1 from olecustomcontrol within w_gestione_documentale
event onmenu ( ref oleobject xnodo,  integer indice )
event onevento ( string codevento,  ref oleobject xnodo,  integer indice )
integer x = 23
integer y = 32
integer width = 2798
integer height = 1724
integer taborder = 10
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
string binarykey = "w_gestione_documentale.win"
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event onevento(string codevento, ref oleobject xnodo, integer indice);choose case codevento
		
		
	case "onSelezione"
		
		ilo_nodo = xnodo
		
		wf_on_selezione(ref xnodo)
//***************************    onNuova      *****************************************


	case "onNuovaMappa"
		wf_on_nuova_mappa(ref xnodo)

		
	case "onNuovoFiglioMappa"
		wf_on_nuovo_figlio_mappa(ref xnodo)
		

	case "onNuovo"
		if indice = 0 then
			wf_nuova_mappa()
		end if	
		
//***************************    onRinomina   *****************************************				


	case "onRinominaMappa"
		wf_on_rinomina_mappa(ref xnodo)


	case "onRinominaRect"
		wf_on_rinomina_rect(ref xnodo)
		
		
//***************************    onCollega    *****************************************
	
	
	case "onCollegaMappa"
		wf_on_collega_mappa_navigatore(ref xnodo)

		
	case "onCollegaDocumentoNavigatore"
		wf_on_collega_documento_navigatore(REF xnodo)


//***************************    onElimina    *****************************************		

		
	case "onEliminaMappa"
		wf_on_elimina_mappa(ref xnodo)
		

	case "onEliminaRect"
		wf_on_elimina_rect(ref xnodo)
		
		
//***************************    onImmagine   *****************************************	


	case "onImmagineMappa"	
		wf_on_immagine_mappa(ref xnodo)

				
//***************************    Nuovo documento IGrafx   *****************************

	case "salvaigx"
		wf_salva_igx(ref xnodo)
	
//***************************    Michela 10/02/2005: interroga/associa area aziendale per marr

	case "onAssociaArea"
		wf_associa_area( 1, ref xnodo)			
		
	case "onInterrogaArea"
		wf_associa_area( 2, ref xnodo)			
				
end choose
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Bw_gestione_documentale.bin 
2000000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000a9bbb74001c7ac1900000003000001400000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000010000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004479d0d1d46223a04e088b585cc9427c100000000a9bbb74001c7ac19a9bbb74001c7ac19000000000000000000000000fffffffe00000002fffffffe00000004fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
20ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006400610063006e0065006d0066006c00660061006800630062006800610068006f0068007400730032002000300030005c0033003b0022003a00430050005c0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400003f41000800034757f20affffffe00065005f00740078006e00650079007400002c8c002e0031003b0035003a00430050005c006f007200720067006d00610069006d0053005c0062007900730061005c006500640041007000610000b29300000048000800034757f20b000000200065005f00740078006e00650078007400003f41000800034757f20affffffe00065005f00740078006e00650079007400002c8c0067006f00610072006d006d005c00690079005300610062006500730053005c004c0051004100200079006e006800770072006500200065005c0038006900770033006e003b0032003a00430050005c006f007200720067006d00610069006d0053005c0062007900730061005c00650068005300720061006400650053005c006200790073006100200065006500430074006e006100720020006c002e0034003b0031003a00430069005c0065006e00700074006200750077005c00770077006f00720074006f0067005c003700730030002e005c003500690062003b006e003a00430050005c004f005200520047007e0041005c0031004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000300000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Bw_gestione_documentale.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
