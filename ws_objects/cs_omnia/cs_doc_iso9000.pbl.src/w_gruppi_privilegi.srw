﻿$PBExportHeader$w_gruppi_privilegi.srw
$PBExportComments$Window Gruppi Privilegi
forward
global type w_gruppi_privilegi from w_cs_xx_principale
end type
type st_2 from statictext within w_gruppi_privilegi
end type
type st_1 from statictext within w_gruppi_privilegi
end type
type dw_mansionari_lista from uo_cs_xx_dw within w_gruppi_privilegi
end type
type dw_tab_gruppi_privilegi_mansionari from uo_cs_xx_dw within w_gruppi_privilegi
end type
type dw_tab_gruppi_privilegi_lista from uo_cs_xx_dw within w_gruppi_privilegi
end type
end forward

global type w_gruppi_privilegi from w_cs_xx_principale
integer width = 3534
integer height = 1476
string title = "Gruppi Privilegi"
st_2 st_2
st_1 st_1
dw_mansionari_lista dw_mansionari_lista
dw_tab_gruppi_privilegi_mansionari dw_tab_gruppi_privilegi_mansionari
dw_tab_gruppi_privilegi_lista dw_tab_gruppi_privilegi_lista
end type
global w_gruppi_privilegi w_gruppi_privilegi

on w_gruppi_privilegi.create
int iCurrent
call super::create
this.st_2=create st_2
this.st_1=create st_1
this.dw_mansionari_lista=create dw_mansionari_lista
this.dw_tab_gruppi_privilegi_mansionari=create dw_tab_gruppi_privilegi_mansionari
this.dw_tab_gruppi_privilegi_lista=create dw_tab_gruppi_privilegi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_mansionari_lista
this.Control[iCurrent+4]=this.dw_tab_gruppi_privilegi_mansionari
this.Control[iCurrent+5]=this.dw_tab_gruppi_privilegi_lista
end on

on w_gruppi_privilegi.destroy
call super::destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_mansionari_lista)
destroy(this.dw_tab_gruppi_privilegi_mansionari)
destroy(this.dw_tab_gruppi_privilegi_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_cod_mansionario,ls_flag_supervisore

//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//select cod_resp_divisione, flag_supervisore
//into   :ls_cod_mansionario,:ls_flag
//from   mansionari
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_utente=:s_cs_xx.cod_utente;

select cod_resp_divisione
into   :ls_cod_mansionario
from   mansionari
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_utente=:s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_supervisore = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.supervisore) then ls_flag_supervisore = 'S'

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore in ricerca mansionario dell'utente: accesso negato! Dettaglio errore: " + sqlca.sqlerrtext,stopsign!)
	postevent("pc_close")
end if

if sqlca.sqlcode = 100 or isnull(ls_cod_mansionario) then
	g_mb.messagebox("Omnia","L'utente non ha alcun mansionario: accesso negato!")
	postevent("pc_close")
end if
//--- Fine modifica Giulio

if ls_flag_supervisore="N" or isnull(ls_flag_supervisore) then
	g_mb.messagebox("Omnia","QUESTO UTENTE NON E' IL SUPERVISORE: accesso negato!")
	postevent("pc_close")
end if

dw_tab_gruppi_privilegi_lista.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)
							  
dw_tab_gruppi_privilegi_mansionari.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_nonew + c_nomodify + c_noretrieveonopen, &
                       c_default)

dw_mansionari_lista.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_nonew + c_nomodify + c_nodelete, &
                       c_default)
							  
iuo_dw_main = dw_tab_gruppi_privilegi_lista

end event

type st_2 from statictext within w_gruppi_privilegi
integer x = 1787
integer y = 700
integer width = 1691
integer height = 100
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711935
long backcolor = 12632256
string text = "Elenco Mansionari"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_1 from statictext within w_gruppi_privilegi
integer x = 23
integer y = 700
integer width = 1691
integer height = 100
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Elenco Mansionari già inseriti associati al gruppo"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_mansionari_lista from uo_cs_xx_dw within w_gruppi_privilegi
integer x = 1792
integer y = 804
integer width = 1696
integer height = 560
integer taborder = 10
string dragicon = "DataPipeline!"
string dataobject = "d_gruppi_prov_lista_mansionari"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event clicked;call super::clicked;if this.getrow() > 0 then Drag(Begin!)
end event

type dw_tab_gruppi_privilegi_mansionari from uo_cs_xx_dw within w_gruppi_privilegi
integer x = 27
integer y = 804
integer width = 1691
integer height = 560
integer taborder = 10
string dataobject = "d_tab_gruppi_privilegi_mansionari"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_gruppo_privilegio

ls_cod_gruppo_privilegio = dw_tab_gruppi_privilegi_lista.getitemstring(dw_tab_gruppi_privilegi_lista.getrow(),"cod_gruppo_privilegio")

ll_errore = retrieve(s_cs_xx.cod_azienda,ls_cod_gruppo_privilegio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event dragdrop;call super::dragdrop;string ls_cod_gruppo_privilegio,ls_cod_mansionario

dw_mansionari_lista.Drag(end!)
ls_cod_gruppo_privilegio = dw_tab_gruppi_privilegi_lista.getitemstring(dw_tab_gruppi_privilegi_lista.getrow(),"cod_gruppo_privilegio")
ls_cod_mansionario = dw_mansionari_lista.getitemstring(dw_mansionari_lista.getrow(),"cod_resp_divisione")

insert into tab_gruppi_privilegi_man
(cod_azienda,
 cod_gruppo_privilegio,
 cod_mansionario)
values
(:s_cs_xx.cod_azienda,
 :ls_cod_gruppo_privilegio,
 :ls_cod_mansionario);
 
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia","Mansionario già associato al gruppo")
	dw_mansionari_lista.triggerevent("pcd_retrieve")
	return 0
end if
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore in fase associazione del mansionario al gruppo; probabilmente esiste già! (dettaglio errore:" + sqlca.sqlerrtext + ")",stopsign!)
	dw_mansionari_lista.triggerevent("pcd_retrieve")
	return 0
end if

dw_mansionari_lista.triggerevent("pcd_retrieve")
this.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

event pcd_delete;string ls_mansionario,ls_cod_gruppo_privilegio

ls_mansionario = getitemstring(getrow(),"cod_mansionario")
ls_cod_gruppo_privilegio = dw_tab_gruppi_privilegi_lista.getitemstring(dw_tab_gruppi_privilegi_lista.getrow(),"cod_gruppo_privilegio")

if g_mb.messagebox("OMNIA","Sei sicuro di voler cancellare il mansionario " +ls_mansionario+" dal gruppo di privilegi " +ls_cod_gruppo_privilegio+"?", question!,YesNo!,1) = 2 then return

delete from tab_gruppi_privilegi_man
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_gruppo_privilegio = :ls_cod_gruppo_privilegio and
      cod_mansionario = :ls_mansionario;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore durante la cancellazione del mansionario. Dettaglio=" + sqlca.sqlerrtext)
	rollback;
	return
end if
commit;

retrieve(s_cs_xx.cod_azienda,ls_cod_gruppo_privilegio)


end event

type dw_tab_gruppi_privilegi_lista from uo_cs_xx_dw within w_gruppi_privilegi
integer x = 23
integer y = 16
integer width = 3461
integer height = 668
integer taborder = 10
string dataobject = "d_tab_gruppi_privilegi_lista"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event rowfocuschanged;call super::rowfocuschanged;if  i_extendmode then
	
	if rowcount()>0 then
		
		dw_tab_gruppi_privilegi_mansionari.triggerevent("pcd_retrieve")
		
	end if
	
end if
end event

