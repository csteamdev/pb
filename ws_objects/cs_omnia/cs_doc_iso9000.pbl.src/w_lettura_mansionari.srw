﻿$PBExportHeader$w_lettura_mansionari.srw
forward
global type w_lettura_mansionari from window
end type
type cbx_manuale from checkbox within w_lettura_mansionari
end type
type st_2 from statictext within w_lettura_mansionari
end type
type st_1 from statictext within w_lettura_mansionari
end type
type b_manuale from commandbutton within w_lettura_mansionari
end type
type sle_azienda from singlelineedit within w_lettura_mansionari
end type
type cbx_log from checkbox within w_lettura_mansionari
end type
type st_log from statictext within w_lettura_mansionari
end type
type hpb_1 from hprogressbar within w_lettura_mansionari
end type
type b_elabora from commandbutton within w_lettura_mansionari
end type
end forward

global type w_lettura_mansionari from window
integer width = 1838
integer height = 1176
boolean titlebar = true
string title = "Lettura Privilegi Mansionari"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cbx_manuale cbx_manuale
st_2 st_2
st_1 st_1
b_manuale b_manuale
sle_azienda sle_azienda
cbx_log cbx_log
st_log st_log
hpb_1 hpb_1
b_elabora b_elabora
end type
global w_lettura_mansionari w_lettura_mansionari

type variables
uo_array iuo_array
string is_azienda_corrente
end variables

forward prototypes
public subroutine wf_assoc_array ()
public subroutine wf_leggi_aziende ()
public function integer wf_leggi_mansionari (string as_cod_azienda)
public function integer wf_leggi_permesso (string as_cod_resp_divisione, string as_flag, string as_cod_azienda)
public function integer wf_leggi_permessi (string as_cod_resp_divisione, string as_cod_azienda)
end prototypes

public subroutine wf_assoc_array ();iuo_array.set('flag_autorizzazione','001')
iuo_array.set('flag_lettura','002')
iuo_array.set('flag_qualificazione','003')
iuo_array.set('flag_approvazione','004')
iuo_array.set('flag_modifica','005')
iuo_array.set('autoriz_liv_doc','006')
iuo_array.set('flag_validazione','007')
iuo_array.set('flag_elimina','008')
iuo_array.set('flag_supervisore','009')
iuo_array.set('flag_apertura_nc','010')
iuo_array.set('flag_chiusura_nc','011')
iuo_array.set('flag_autorizza_reso','012')
iuo_array.set('flag_chiudi_reclami','013')
iuo_array.set('flag_approva_az_prev','014')
iuo_array.set('flag_chiudi_az_prev','015')
iuo_array.set('flag_approva_az_corr','016')
iuo_array.set('flag_chiudi_az_corr','017')
iuo_array.set('flag_approva_rda','018')
iuo_array.set('flag_autorizza_rda','019')
iuo_array.set('flag_approva_ord_acq','020')
iuo_array.set('flag_chiudi_ord_acq','021')
iuo_array.set('approva_offerte','022')
iuo_array.set('scadenziario_mezzi','023')
end subroutine

public subroutine wf_leggi_aziende ();string ls_sql, ls_cod_azienda, ls_filename
datastore lds_store
long ll_righe, ll_i, ll_file
int li_result

if cbx_manuale.checked = true then
	ls_sql  = "SELECT cod_azienda from aziende where cod_azienda = '"+ sle_azienda.text +"' "
else
	ls_sql  = "SELECT cod_azienda from aziende"
end if
ll_righe = guo_functions.uof_crea_datastore(lds_store, ls_sql)
//hpb_1.maxposition = ll_righe

for ll_i = 1 to ll_righe
	ls_cod_azienda = lds_store.getitemstring(ll_i, "cod_azienda")
//	ls_filename = "log_leggi_aziende.txt"
//	ll_file = FileOpen( ls_filename, TextMode!, Write!, LockReadWrite!, Append!)
//	FileWriteEx( ll_file, "Azienda:" + " " )
//	FileWriteEx( ll_file,ls_cod_azienda + "~r~n" )
//	FileClose( ll_file )
	Yield()
	st_log.text = "Elaborazione azienda "+ls_cod_azienda
	
//	hpb_1.position = ll_i
	li_result = wf_leggi_mansionari(ls_cod_azienda)

	if (li_result < 0) then
		rollback;
		return 
	end if
next

commit ;
g_mb.show("Elaborazione completata!")

end subroutine

public function integer wf_leggi_mansionari (string as_cod_azienda);string ls_sql, ls_cod_resp_divisione, ls_filename
datastore lds_store
long ll_righe, ll_i, ll_file
int li_result

ls_sql  = "SELECT cod_resp_divisione from mansionari where cod_azienda = '"+ as_cod_azienda +"' "
ll_righe = guo_functions.uof_crea_datastore(lds_store, ls_sql)
hpb_1.maxposition = ll_righe

for ll_i = 1 to ll_righe
	ls_cod_resp_divisione = lds_store.getitemstring(ll_i, "cod_resp_divisione")
//	ls_filename = "log_leggi_mansionari.txt"
//	ll_file = FileOpen( ls_filename, TextMode!, Write!, LockReadWrite!, Append!)
//	FileWriteEx( ll_file, "Mansionario:" + "")
//	FileWriteEx( ll_file, ls_cod_resp_divisione + "~r~n")
//	FileClose( ll_file )
	li_result = wf_leggi_permessi(ls_cod_resp_divisione, as_cod_azienda)
	hpb_1.position = ll_i
	
	if (li_result < 0) then
		return -1
	end if
next


return 0
end function

public function integer wf_leggi_permesso (string as_cod_resp_divisione, string as_flag, string as_cod_azienda);string ls_flag_array, ls_flag, ls_sql, ls_cod_valore_lista, ls_cod_mansionario, ls_filename
datastore lds_store
long ll_righe, ll_file

setnull (ls_cod_valore_lista)
ls_flag_array = iuo_array.get(as_flag)

ls_sql = "select " + as_flag + " from mansionari where  cod_azienda = '"+ as_cod_azienda + "'" 
ls_sql += " and cod_resp_divisione ='" + as_cod_resp_divisione +"' "//+ "' and  flag_autorizzazione = 'S' "
ll_righe = guo_functions.uof_crea_datastore(lds_store, ls_sql)

if(ll_righe > 0) then

	if(as_flag = 'autoriz_liv_doc') then
		ls_cod_valore_lista = lds_store.getitemstring(1,1)
	elseif lds_store.getitemstring(1,1) <> 'S' then
		return 0
	end if
	
	//controllo se il permesso esiste
	select cod_mansionario
	into :ls_cod_mansionario
	from tab_privilegi_mansionari
	where 
		cod_azienda = :as_cod_azienda and
		cod_mansionario = :as_cod_resp_divisione and
		cod_privilegio = :ls_flag_array ;
	
	if sqlca.sqlcode = 0 then
//		if cbx_log.checked = true then
//		ls_filename = "log_leggi_permesso_non_scritto.txt"
//		ll_file = FileOpen( ls_filename, TextMode!, Write!, LockReadWrite!, Append!)
//
//		FileWriteEx( ll_file, ls_flag_array + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Privilegio già scritto!" + "~r~n")
//		FileClose( ll_file )
//		end if
		return 0
	end if
	
	insert into tab_privilegi_mansionari (
		cod_azienda,
		cod_mansionario,
		cod_privilegio, 
		cod_valore_lista
	)  values (
		:as_cod_azienda,
		:as_cod_resp_divisione,
		:ls_flag_array,
		:ls_cod_valore_lista
	) ;
//	if cbx_log.checked = true then
//	ls_filename = "log_leggi_permesso_scritto.txt"
//	ll_file = FileOpen( ls_filename, TextMode!, Write!, LockReadWrite!, Append!)
//	FileWriteEx( ll_file, ls_flag_array + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Privilegio scritto!" + "~r~n")
//	FileClose( ll_file )
//	end if
	if (sqlca.sqlcode < 0) then
		g_mb.error("Errore nella query" + as_flag + " mansionario: " + as_cod_resp_divisione + " azienda: " +as_cod_azienda , sqlca)
		return -1
	end if
else
	return 0
end if
return 0
end function

public function integer wf_leggi_permessi (string as_cod_resp_divisione, string as_cod_azienda);string ls_flag, ls_filename
long ll_file

//ls_filename = "log_leggi_permessi.txt"
//ll_file = FileOpen( ls_filename, TextMode!, Write!, LockReadWrite!, Append!)

if wf_leggi_permesso(as_cod_resp_divisione, 'flag_autorizzazione', as_cod_azienda) < 0 then
//	FileWriteEx( ll_file, "flag_autorizzazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_autorizzazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if

if wf_leggi_permesso(as_cod_resp_divisione, 'flag_lettura', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_lettura" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_lettura" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if

if wf_leggi_permesso(as_cod_resp_divisione, 'flag_qualificazione', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_qualificazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_qualificazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_approvazione', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_approvazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_approvazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_modifica', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_modifica" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_modifica" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'autoriz_liv_doc', as_cod_azienda) < 0 then
//	FileWriteEx( ll_file, "autoriz_liv_doc" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "autoriz_liv_doc" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_validazione', as_cod_azienda) < 0 then
//	FileWriteEx( ll_file, "flag_validazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_validazione" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_elimina', as_cod_azienda) < 0 then
//	FileWriteEx( ll_file, "flag_elimina" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
		return  -1 
else
//	FileWriteEx(  ll_file, "flag_elimina" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if

if wf_leggi_permesso(as_cod_resp_divisione, 'flag_supervisore', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_supervisore" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_supervisore" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_apertura_nc',as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_apertura_nc" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_apertura_nc" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_chiusura_nc', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_chiusura_nc" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_chiusura_nc" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_autorizza_reso', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_autorizza_reso" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_autorizza_reso" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_chiudi_reclami', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_chiudi_reclami" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_chiudi_reclami" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_approva_az_prev', as_cod_azienda) < 0 then
//	FileWriteEx( ll_file, "flag_approva_az_prev" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_approva_az_prev" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_chiudi_az_prev', as_cod_azienda) < 0 then
//	FileWriteEx( ll_file, "flag_chiudi_az_prev" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_chiudi_az_prev" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_approva_az_corr', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_approva_az_corr" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_approva_az_corr" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_chiudi_az_corr', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_chiudi_az_corr" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_chiudi_az_corr" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_approva_rda', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_approva_rda" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_approva_rda" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_autorizza_rda', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_autorizza_rda" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_autorizza_rda" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_approva_ord_acq', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_approva_ord_acq" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_approva_ord_acq" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'flag_chiudi_ord_acq', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "flag_chiudi_ord_acq" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "flag_chiudi_ord_acq" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'approva_offerte', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "approva_offerte" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "approva_offerte" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
if wf_leggi_permesso(as_cod_resp_divisione, 'scadenziario_mezzi', as_cod_azienda) < 0 then 
//	FileWriteEx( ll_file, "scadenziario_mezzi" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Non scritto!~n" + "~r~n")
	return  -1 
else
//	FileWriteEx(  ll_file, "scadenziario_mezzi" + "-" + as_cod_resp_divisione + "-" + as_cod_azienda + "Scritto!" + "~r~n")
end if
//FileClose( ll_file )
return 0
end function

on w_lettura_mansionari.create
this.cbx_manuale=create cbx_manuale
this.st_2=create st_2
this.st_1=create st_1
this.b_manuale=create b_manuale
this.sle_azienda=create sle_azienda
this.cbx_log=create cbx_log
this.st_log=create st_log
this.hpb_1=create hpb_1
this.b_elabora=create b_elabora
this.Control[]={this.cbx_manuale,&
this.st_2,&
this.st_1,&
this.b_manuale,&
this.sle_azienda,&
this.cbx_log,&
this.st_log,&
this.hpb_1,&
this.b_elabora}
end on

on w_lettura_mansionari.destroy
destroy(this.cbx_manuale)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.b_manuale)
destroy(this.sle_azienda)
destroy(this.cbx_log)
destroy(this.st_log)
destroy(this.hpb_1)
destroy(this.b_elabora)
end on

event open;iuo_array = create uo_array
wf_assoc_array()
end event

event close;destroy iuo_array
end event

type cbx_manuale from checkbox within w_lettura_mansionari
integer x = 137
integer y = 760
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Manuale"
end type

type st_2 from statictext within w_lettura_mansionari
integer x = 137
integer y = 580
integer width = 891
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Azienda ad inserimento manuale:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_lettura_mansionari
integer x = 137
integer y = 20
integer width = 558
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Cicla tutte le aziende"
boolean focusrectangle = false
end type

type b_manuale from commandbutton within w_lettura_mansionari
integer x = 549
integer y = 820
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Elabora"
end type

event clicked;wf_leggi_aziende()

end event

type sle_azienda from singlelineedit within w_lettura_mansionari
integer x = 1074
integer y = 560
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cbx_log from checkbox within w_lettura_mansionari
integer x = 23
integer y = 280
integer width = 229
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Log"
boolean checked = true
end type

type st_log from statictext within w_lettura_mansionari
integer x = 663
integer y = 200
integer width = 1029
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_lettura_mansionari
integer x = 640
integer y = 100
integer width = 1029
integer height = 80
unsignedinteger maxposition = 100
integer setstep = 10
end type

type b_elabora from commandbutton within w_lettura_mansionari
integer x = 69
integer y = 80
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Elabora"
end type

event clicked;wf_leggi_aziende()

end event

