﻿$PBExportHeader$w_mansionari.srw
$PBExportComments$Anagrafica Mansionari
forward
global type w_mansionari from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_3 from uo_std_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_3 dw_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_4 dw_4
end type
end forward

global type w_mansionari from w_cs_xx_treeview
integer width = 3881
integer height = 1952
string title = "Mansionari"
end type
global w_mansionari w_mansionari

type variables
private:
	int ICONA_MANSIONARIO
	int ICONA_DIVISIONE
	int ICONA_AREA_AZIENDALE
	int ICONA_REPARTO
	int ICONA_MANSIONARIO_ELIMINATO
	
	boolean ib_new
	
	datastore ids_store
	long il_livello_corrente

end variables

forward prototypes
public subroutine wf_treeview_icons ()
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_inserisci_reparti (long al_handle)
public function integer wf_inserisci_mansionari (long al_handle)
public function integer wf_inserisci_aree_aziendali (long al_handle)
public function integer wf_inserisci_divisioni (long al_handle)
public subroutine wf_imposta_ricerca ()
public subroutine wf_valori_livelli (ref string as_valori[])
public subroutine wf_valori_livelli ()
public function long wf_aggiungi_mansionario ()
end prototypes

public subroutine wf_treeview_icons ();/**
 * stefanop
 * 19/10/2011
 *
 * Carico icone albero
 **/
 

ICONA_MANSIONARIO = tab_ricerca.selezione.tv_selezione.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\operatore.png")
ICONA_DIVISIONE = tab_ricerca.selezione.tv_selezione.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\deposito.png")
ICONA_AREA_AZIENDALE =tab_ricerca.selezione.tv_selezione.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\area_aziendale.png")
ICONA_REPARTO = tab_ricerca.selezione.tv_selezione.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\reparto.png")
ICONA_MANSIONARIO_ELIMINATO = tab_ricerca.selezione.tv_selezione.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\cliente_eliminato.png")

end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
	
		case "D"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND mansionari.cod_divisione is null "
			else
				as_sql += " AND mansionari.cod_divisione = '" + lstr_data.codice + "' "
			end if
			
		case "A"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND mansionari.cod_area_aziendale is null "
			else
				as_sql += " AND mansionari.cod_area_aziendale = '" + lstr_data.codice + "' "
			end if
			
		case "R"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND mansionari.cod_reparto is null "
			else
				as_sql += " AND mansionari.cod_reparto = '" + lstr_data.codice + "' "
			end if
				
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello_corrente = ai_livello

choose case tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "livello_" + string(ai_livello))
	case "D" // divisioni
		return wf_inserisci_divisioni(al_handle) 
		
	case "A" // aree aziendali
		return wf_inserisci_aree_aziendali(al_handle)

	case "R" // reparti
		return wf_inserisci_reparti(al_handle)
				
	case else
		return wf_inserisci_mansionari(al_handle)
		
end choose
end function

public function integer wf_inserisci_reparti (long al_handle);string ls_sql, ls_error, ls_cod_reparto, ls_des_reparto
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data

ls_sql = "SELECT DISTINCT anag_reparti.cod_reparto, anag_reparti.des_reparto FROM mansionari &
	LEFT OUTER JOIN anag_reparti ON &
		anag_reparti.cod_azienda = mansionari.cod_azienda AND &
		anag_reparti.cod_reparto = mansionari.cod_reparto &
	WHERE mansionari.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro
	
wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_item(true, ICONA_REPARTO)
	
	ls_cod_reparto =  ids_store.getitemstring(ll_i, 1)
	ls_des_reparto =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "R"
	lstr_data.codice = ls_cod_reparto
	lstr_data.livello = il_livello_corrente
	
	if isnull(ls_cod_reparto) and isnull(ls_des_reparto)  then
		ltvi_item.label = "Reparto Mancante"
	elseif isnull(ls_des_reparto) then
		ltvi_item.label = ls_cod_reparto
	else
		ltvi_item.label = ls_cod_reparto + " - " + ls_des_reparto
	end if
	
	ltvi_item.data = lstr_data
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_inserisci_mansionari (long al_handle);/**
 * stefanop
 * 19/10/2011
 *
 * Carico i mansionari
 **/
 
string ls_sql, ls_error, ls_cod_resp_divisione, ls_nome, ls_incarico
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data

ls_sql = "SELECT DISTINCT cod_resp_divisione, nome, cognome &
	FROM mansionari &
	WHERE mansionari.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro
	
wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_item(false, ICONA_MANSIONARIO)
	
	ls_cod_resp_divisione =  ids_store.getitemstring(ll_i, 1)
	ls_nome =  ids_store.getitemstring(ll_i, 2)
	ls_incarico =  ids_store.getitemstring(ll_i, 3)
	
	lstr_data.tipo_livello = "M"
	lstr_data.codice = ls_cod_resp_divisione
	lstr_data.livello = il_livello_corrente
	
	ls_nome = ls_nome + " - " + ls_incarico
	if isnull(ls_nome) or ls_nome = ""  then
		ltvi_item.label = "nome mancante"
	else
		ltvi_item.label = ls_nome
	end if
	
	ltvi_item.data = lstr_data
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_inserisci_aree_aziendali (long al_handle);string ls_sql, ls_error, ls_cod_area_aziendale, ls_des_area
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data

ls_sql = "SELECT DISTINCT tab_aree_aziendali.cod_area_aziendale, tab_aree_aziendali.des_area FROM mansionari &
	LEFT OUTER JOIN tab_aree_aziendali ON &
		tab_aree_aziendali.cod_azienda = mansionari.cod_azienda AND &
		tab_aree_aziendali.cod_area_aziendale = mansionari.cod_area_aziendale &
	WHERE mansionari.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro
	
wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_item(true, ICONA_AREA_AZIENDALE)
	
	ls_cod_area_aziendale =  ids_store.getitemstring(ll_i, 1)
	ls_des_area =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "A"
	lstr_data.codice = ls_cod_area_aziendale
	lstr_data.livello = il_livello_corrente
	
	if isnull(ls_cod_area_aziendale) and isnull(ls_des_area)  then
		ltvi_item.label = "Area Aziendale Mancante"
	elseif isnull(ls_des_area) then
		ltvi_item.label = ls_cod_area_aziendale
	else
		ltvi_item.label = ls_cod_area_aziendale + " - " + ls_des_area
	end if
	
	ltvi_item.data = lstr_data
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_inserisci_divisioni (long al_handle);string ls_sql, ls_error, ls_cod_divisione, ls_des_divisione
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data

ls_sql = "SELECT DISTINCT anag_divisioni.cod_divisione, anag_divisioni.des_divisione FROM mansionari &
	LEFT OUTER JOIN anag_divisioni ON &
		anag_divisioni.cod_azienda = mansionari.cod_azienda AND &
		anag_divisioni.cod_divisione = mansionari.cod_divisione &
	WHERE mansionari.cod_azienda='" + s_cs_xx.cod_azienda + "' " + is_sql_filtro
	
wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_item(true, ICONA_DIVISIONE)
	
	ls_cod_divisione =  ids_store.getitemstring(ll_i, 1)
	ls_des_divisione =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_divisione
	lstr_data.livello = il_livello_corrente
	
	if isnull(ls_cod_divisione) and isnull(ls_des_divisione)  then
		ltvi_item.label = "Divisione Mancante"
	elseif isnull(ls_des_divisione) then
		ltvi_item.label = ls_cod_divisione
	else
		ltvi_item.label = ls_cod_divisione + " - " + ls_des_divisione
	end if
	
	ltvi_item.data = lstr_data
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public subroutine wf_imposta_ricerca ();string ls_var

is_sql_filtro = ""

// leggo filtri
ls_var = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_mansionario")
if not isnull(ls_var) and ls_var <> "" then is_sql_filtro += " AND mansionari.cod_resp_divisione='" + ls_var + "' "

ls_var = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_divisione")
if not isnull(ls_var) and ls_var <> "" then is_sql_filtro += " AND mansionari.cod_divisione='" + ls_var + "' "

ls_var = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_area_aziendale")
if not isnull(ls_var) and ls_var <> "" then is_sql_filtro += " AND mansionari.cod_area_aziendale='" + ls_var + "' "

ls_var = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_reparto")
if not isnull(ls_var) and ls_var <> "" then is_sql_filtro += " AND mansionari.cod_reparto='" + ls_var + "' "

ls_var = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_utente")
if not isnull(ls_var) and ls_var <> "" then is_sql_filtro += " AND mansionari.cod_utente='" + ls_var + "' "
// ----
end subroutine

public subroutine wf_valori_livelli (ref string as_valori[]);as_valori = { &
	"Non specificato~tN", &
	"Divisioni~tD", &
	"Area Aziendale~tA", &
	"Reparti~tR" &
}
end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Divisioni", "D")
wf_add_valore_livello("Area Aziendali", "A")
wf_add_valore_livello("Reparti", "R")
end subroutine

public function long wf_aggiungi_mansionario ();/*
*/

string ls_cod_resp_divisione, ls_nome, ls_incarico, ls_label
long ll_i
treeviewitem ltvi_item
str_treeview lstr_data

ll_i = tab_dettaglio.det_1.dw_1.getrow()
ltvi_item = wf_new_item(false, ICONA_MANSIONARIO)

ls_cod_resp_divisione =  tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_resp_divisione")
ls_nome =  tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "nome")
ls_incarico =  tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cognome")

lstr_data.tipo_livello = "M"
lstr_data.codice = ls_cod_resp_divisione
lstr_data.livello = il_livello_corrente

ls_label = ls_cod_resp_divisione
if not isnull(ls_nome) or ls_nome <> ""  then
	ls_label += " - " + ls_nome
end if

ltvi_item.label = ls_label
ltvi_item.data = lstr_data

return tab_ricerca.selezione.tv_selezione.insertitemlast(0, ltvi_item)
end function

on w_mansionari.create
int iCurrent
call super::create
end on

on w_mansionari.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, pcca.null_object, c_default, c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata+c_scrollparent,c_default)
tab_dettaglio.det_4.dw_4.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata+c_scrollparent,c_default)

iuo_dw_main = tab_dettaglio.det_1.dw_1

tab_dettaglio.det_3.dw_3.move(0,0)
tab_dettaglio.det_3.dw_3.settransobject(sqlca)
tab_dettaglio.det_3.dw_3.retrieve(s_cs_xx.cod_azienda)


tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true
//tab_dettaglio.det_3.dw_3.ib_dw_detail = true
tab_dettaglio.det_4.dw_4.ib_dw_detail = true
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_dettaglio.det_2.dw_2,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto","cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_2.dw_2,"cod_divisione",sqlca,&
                "anag_divisioni","cod_divisione","des_divisione","anag_divisioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_2.dw_2,"cod_resp_div_padre",sqlca,&
                 "mansionari","cod_resp_divisione","cognome","mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_2.dw_2,"cod_utente",sqlca,&
                 "utenti","cod_utente","nome_cognome + ' (' + flag_blocco + ')'","")

f_PO_LoadDDDW_DW(tab_dettaglio.det_4.dw_4,"cod_livello_1",sqlca,&
                 "tab_livelli_doc","cod_livello","des_livello"," cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_livello_padre is null or cod_livello_padre = '') ")
					  
f_PO_LoadDDDW_DW(tab_dettaglio.det_4.dw_4,"cod_livello_2",sqlca,&
                 "tab_livelli_doc","cod_livello","des_livello"," cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_livello_padre is not null  ")
					  
f_PO_LoadDDDW_DW(tab_dettaglio.det_4.dw_4,"cod_livello_3",sqlca,&
                 "tab_livelli_doc","cod_livello","des_livello"," cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_livello_padre is not null  ")
					  
f_PO_LoadDDDW_DW(tab_dettaglio.det_4.dw_4,"cod_livello_4",sqlca,&
                 "tab_livelli_doc","cod_livello","des_livello"," cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_livello_padre is not null  ")
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_mansionari
event create ( )
event destroy ( )
det_2 det_2
det_3 det_3
det_4 det_4
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
end on

event tab_dettaglio::clicked;call super::clicked;iuo_dw_main.change_dw_current()
end event

event tab_dettaglio::ue_resize;call super::ue_resize;det_3.dw_3.resize(det_3.width, det_3.height)
end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
string text = "Anagrafica"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_mansionari
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
string dataobject = "d_mansionari_ricerca"
boolean hscrollbar = false
boolean vscrollbar = false
boolean livescroll = false
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_mansionario"
		guo_ricerca.uof_ricerca_mansionario(dw_ricerca, "cod_mansionario")
		
	case "b_ricerca_divisione"
		guo_ricerca.uof_ricerca_divisione(dw_ricerca, "cod_divisione")
		
	case "b_ricerca_area_aziendale"
		guo_ricerca.uof_ricerca_area_aziendale(dw_ricerca, "cod_area_aziendale")
		
	case "b_ricerca_reparto"
		guo_ricerca.uof_ricerca_reparto(dw_ricerca, "cod_reparto")
		
	case "b_ricerca_utente"
		guo_ricerca.uof_ricerca_utente(dw_ricerca, "cod_utente")
				
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltv_item
str_treeview lstr_data


if isnull(handle) or handle <= 0 or wf_is_clear() then
	return 0
end if

getitem(handle,ltv_item)

lstr_data = ltv_item.data

il_livello_corrente = lstr_data.livello

if il_livello_corrente < 3 then
	
	il_livello_corrente++
	
	wf_leggi_livello(handle, il_livello_corrente)
else

	wf_inserisci_mansionari(handle)
	
end if
end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

pcca.window_current = getwindow()

//if isnull(newhandle) or newhandle <= 0 or wf_is_clear() then
//	return 0
//end if

if AncestorReturnValue < 0 then return

getitem(newhandle, ltvi_item)
istr_data = ltvi_item.data

tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

type dw_1 from uo_cs_xx_dw within det_1
integer x = 23
integer y = 28
integer width = 2299
integer height = 924
integer taborder = 30
string dataobject = "d_mansionari_1"
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_delete;call super::pcd_delete;ib_new = false
end event

event pcd_modify;call super::pcd_modify;ib_new = false
end event

event pcd_new;call super::pcd_new;ib_new = true
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.codice)

tab_dettaglio.det_3.dw_3.event ue_retrieve()
tab_dettaglio.det_3.dw_3.visible = (ll_errore > 0)

if ll_errore < 0 then
	pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  ll_i

for ll_i = 1 to RowCount()
   if isnull(GetItemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

	string ls_cod_utente
	long ll_count
	
	ls_cod_utente = getitemstring(getrow(), "cod_utente")
	
	if isnull(ls_cod_utente) then
		g_mb.messagebox("Anagrafica Mansionari", "Codice Utente Obbligatorio", stopsign!)
		pcca.error = c_fatal
	else
		select count(cod_utente)
		into :ll_count
		from mansionari
		where 
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :ls_cod_utente;
			
		if  ll_count > 0 then
			messagebox("Anagrafica Mansionari", "Questo codice utente è già presente in altri mansionari", stopsign!)
			pcca.error = c_fatal
		end if         
	end if

end if
end event

event pcd_view;call super::pcd_view;ib_new = false
end event

event updatestart;call super::updatestart;string ls_cod_resp_divisione
long ll_i
treeviewitem ltvi_item

// cancello eventuali privilegi associati ai mansionari
for ll_i = 1 to deletedcount()
	
	ls_cod_resp_divisione = getitemstring(ll_i, "cod_resp_divisione", Delete!, false)
	
	delete from tab_privilegi_mansionari
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_mansionario = :ls_cod_resp_divisione;
			
next

if DeletedCount() > 0 then
	wf_set_deleted_item_status()
end if

if ib_new then
	il_current_handle = wf_aggiungi_mansionario()
end if



end event

type det_2 from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 2409
integer height = 1676
long backcolor = 12632256
string text = "Qualità"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer x = 18
integer y = 12
integer width = 2341
integer height = 940
integer taborder = 11
string dataobject = "d_mansionari_2"
boolean border = false
end type

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2409
integer height = 1676
long backcolor = 12632256
string text = "Privilegi"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from uo_std_dw within det_3
event ue_retrieve ( )
integer y = 8
integer width = 2400
integer height = 1656
integer taborder = 40
string dataobject = "d_mansionari_privilegi_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean ib_colora = false
end type

event ue_retrieve();/**
 * stefanop
 * 04/11/2011
 *
 * Carico i privilegi associato all'utente
 **/

string ls_sql, ls_cod_privilegio, ls_cod_valore_lista
int li_count, li_i, li_row, li_rowcount
datastore lds_store

setredraw(false)
li_rowcount = rowcount()

// pulisco valori vecchi
for li_i = 1 to li_rowcount
	setitem(li_i, "flag_abilitato", "N")
next

ls_sql = "SELECT cod_privilegio, cod_valore_lista FROM tab_privilegi_mansionari WHERE cod_azienda ='" + s_cs_xx.cod_azienda  + "' "
ls_sql += " AND cod_mansionario='" + istr_data.codice + "' "
 
li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)
 
for li_i = 1 to li_count
	
	ls_cod_privilegio = lds_store.getitemstring(li_i, 1)
	ls_cod_valore_lista = lds_store.getitemstring(li_i, 2)
	
	li_row = find("cod_privilegio='" + ls_cod_privilegio + "'", 0, li_rowcount)
	
	if li_row > 0 then
		setitem(li_row, "flag_abilitato", "S")
		setitem(li_row, "cod_valore_lista", ls_cod_valore_lista)
	end if
	
next

setredraw(true)
end event

event doubleclicked;call super::doubleclicked;string ls_cod_privilegio, ls_flag_abilitato, ls_cod_valore_lista

if row < 1 then return

ls_cod_privilegio = getitemstring(row, "cod_privilegio")
ls_flag_abilitato = getitemstring(row, "flag_abilitato")
ls_cod_valore_lista = getitemstring(row, "cod_valore_lista")

if ls_flag_abilitato = "S" then
	// elimino il privilegio
	delete from tab_privilegi_mansionari
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_mansionario = :istr_data.codice and
		cod_privilegio = :ls_cod_privilegio;
	
else
	// salvo il privilegio
	insert into tab_privilegi_mansionari (
		cod_azienda,
		cod_mansionario,
		cod_privilegio,
		cod_valore_lista
	) values (
		:s_cs_xx.cod_azienda,
		:istr_data.codice,
		:ls_cod_privilegio,
		:ls_cod_valore_lista);
end if

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la modifica del privilegio.", sqlca)
	rollback;
	return
else
	commit;
	
	if ls_flag_abilitato= "S" then 
		ls_flag_abilitato = "N"
	else
		ls_flag_abilitato = "S"
	end if
	
	setitem(row, "flag_abilitato", ls_flag_abilitato)
end if
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_flag_tipo, ls_cod_privilegio

if currentrow < 1 then return 0

// carico il dettaglio nel caso sia LISTA
ls_flag_tipo = getitemstring(currentrow, "flag_tipo")
ls_cod_privilegio  = getitemstring(currentrow, "cod_privilegio")

if ls_flag_tipo <> "L" then return 0

f_PO_LoadDDDW_DW(tab_dettaglio.det_3.dw_3,&
	"cod_valore_lista", &
	sqlca,&
	"det_privilegi", &
	"cod_valore_lista", &
	"des_valore_lista", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_privilegio='"+ ls_cod_privilegio + "'")
end event

type det_4 from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 2409
integer height = 1676
long backcolor = 12632256
string text = "Livelli Prot."
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_4 dw_4
end type

on det_4.create
this.dw_4=create dw_4
this.Control[]={this.dw_4}
end on

on det_4.destroy
destroy(this.dw_4)
end on

type dw_4 from uo_cs_xx_dw within det_4
integer x = 18
integer y = 24
integer width = 2331
integer height = 388
integer taborder = 11
string dataobject = "d_mansionari_4"
boolean border = false
end type

