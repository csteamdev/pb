﻿$PBExportHeader$w_mimetype.srw
forward
global type w_mimetype from w_cs_xx_principale
end type
type dw_mimetype_lista from uo_dw_main within w_mimetype
end type
end forward

global type w_mimetype from w_cs_xx_principale
integer width = 3456
integer height = 2256
string title = "Gestione MimeType"
dw_mimetype_lista dw_mimetype_lista
end type
global w_mimetype w_mimetype

on w_mimetype.create
int iCurrent
call super::create
this.dw_mimetype_lista=create dw_mimetype_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_mimetype_lista
end on

on w_mimetype.destroy
call super::destroy
destroy(this.dw_mimetype_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_mimetype_lista.set_dw_key("cod_azienda")
dw_mimetype_lista.set_dw_options(sqlca, &
                        			pcca.null_object, &
			                        c_default, &
			                        c_default)
										
//dw_mimetype_det.set_dw_options(sqlca, &
//                               dw_mimetype_lista, &
//                               c_sharedata + c_scrollparent, &
//                               c_default)

iuo_dw_main = dw_mimetype_lista
end event

type dw_mimetype_lista from uo_dw_main within w_mimetype
integer x = 23
integer y = 20
integer width = 3374
integer height = 2092
integer taborder = 10
string title = "Lista Mimetype"
string dataobject = "d_mimetype_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_new;call super::pcd_new;long ll_prog_mimetype

select max(prog_mimetype)
into   :ll_prog_mimetype
from   tab_mimetype
where  cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ll_prog_mimetype) or ll_prog_mimetype = 0 then 
	ll_prog_mimetype = 1
else 
	ll_prog_mimetype++
end if


dw_mimetype_lista.SetItem(dw_mimetype_lista.getrow(), "prog_mimetype", ll_prog_mimetype)
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = dw_mimetype_lista.retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
long ll_prog_mimetype

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next



end event

