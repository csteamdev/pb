﻿$PBExportHeader$w_nav_doc_aree.srw
forward
global type w_nav_doc_aree from w_cs_xx_risposta
end type
type dw_1 from uo_cs_xx_dw within w_nav_doc_aree
end type
type cb_2 from commandbutton within w_nav_doc_aree
end type
type cb_1 from commandbutton within w_nav_doc_aree
end type
type st_1 from statictext within w_nav_doc_aree
end type
end forward

global type w_nav_doc_aree from w_cs_xx_risposta
integer width = 1737
integer height = 1148
string title = "Assegna Aree"
dw_1 dw_1
cb_2 cb_2
cb_1 cb_1
st_1 st_1
end type
global w_nav_doc_aree w_nav_doc_aree

on w_nav_doc_aree.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.cb_2=create cb_2
this.cb_1=create cb_1
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.st_1
end on

on w_nav_doc_aree.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.settransobject(sqlca)
dw_1.retrieve(s_cs_xx.cod_azienda)

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
end event

type dw_1 from uo_cs_xx_dw within w_nav_doc_aree
integer x = 46
integer y = 120
integer width = 1600
integer height = 740
integer taborder = 20
string dataobject = "d_nav_doc_aree"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;call super::doubleclicked;if row > 0 then
	cb_1.event clicked()
end if
end event

type cb_2 from commandbutton within w_nav_doc_aree
integer x = 46
integer y = 900
integer width = 389
integer height = 100
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Azzera"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = ""
s_cs_xx.parametri.parametro_s_2 = ""

close(parent)
end event

type cb_1 from commandbutton within w_nav_doc_aree
integer x = 1280
integer y = 900
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Assegna"
end type

event clicked;if dw_1.getrow() > 0 then
	
	s_cs_xx.parametri.parametro_s_1 = dw_1.getitemstring(dw_1.getrow(), 1)
	s_cs_xx.parametri.parametro_s_2 = dw_1.getitemstring(dw_1.getrow(), 2)
	
else
	
	s_cs_xx.parametri.parametro_s_1 = ""
	s_cs_xx.parametri.parametro_s_2 = ""
	
end if

close(parent)
end event

type st_1 from statictext within w_nav_doc_aree
integer x = 46
integer y = 40
integer width = 1605
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seleziona l~'area aziendale per limitarne l~'accesso alla mappa"
boolean focusrectangle = false
end type

