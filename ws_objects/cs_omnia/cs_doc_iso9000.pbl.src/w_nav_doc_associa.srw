﻿$PBExportHeader$w_nav_doc_associa.srw
forward
global type w_nav_doc_associa from w_cs_xx_risposta
end type
type cb_cerca from commandbutton within w_nav_doc_associa
end type
type sle_cerca from singlelineedit within w_nav_doc_associa
end type
type cb_2 from commandbutton within w_nav_doc_associa
end type
type cb_1 from commandbutton within w_nav_doc_associa
end type
type tv_1 from treeview within w_nav_doc_associa
end type
end forward

global type w_nav_doc_associa from w_cs_xx_risposta
integer width = 2153
integer height = 1224
string title = "Navigatore Documenti"
cb_cerca cb_cerca
sle_cerca sle_cerca
cb_2 cb_2
cb_1 cb_1
tv_1 tv_1
end type
global w_nav_doc_associa w_nav_doc_associa

type variables
private long il_handle
private string is_last_search
end variables

forward prototypes
public function integer wf_carica_documenti (long al_handle, long al_cod_padre)
public function integer wf_trova (string as_ricerca, long al_handle)
end prototypes

public function integer wf_carica_documenti (long al_handle, long al_cod_padre);string ls_sql
long ll_rows, ll_row
datastore lds_store
str_nav_doc_associa lstr_data
treeviewitem ltvi_item

ls_sql = "SELECT &
	anno_registrazione, &
	num_registrazione, &
	cod_padre, &
	cod_figlio, &
	tipo_elemento, &
	des_elemento &
FROM &
	tes_documenti &
WHERE &
	cod_azienda='" + s_cs_xx.cod_azienda +"' AND &
	flag_blocco <> 'S' AND &
	cod_padre=" + string(al_cod_padre)
	
if not f_crea_datastore(ref lds_store, ls_sql) then
	g_mb.messagebox("Navigatore", "Impossibile creare Datastore")
	return -1
end if

ll_rows = lds_store.retrieve()
if ll_rows = 0 then return 0

for ll_row = 1 to ll_rows
	
	lstr_data.cod_padre = lds_store.getitemnumber(ll_row, "cod_padre")
	lstr_data.cod_figlio = lds_store.getitemnumber(ll_row, "cod_figlio")
	lstr_data.anno_registrazione = lds_store.getitemnumber(ll_row, "anno_registrazione")
	lstr_data.num_registrazione = lds_store.getitemnumber(ll_row, "num_registrazione")
	lstr_data.tipo_elemento = lds_store.getitemstring(ll_row, "tipo_elemento")
	
	ltvi_item.data = lstr_data
	ltvi_item.label = lds_store.getitemstring(ll_row, "des_elemento")
	ltvi_item.selected = false
	
	if lds_store.getitemstring(ll_row, "tipo_elemento") = "M" then
		ltvi_item.pictureindex = 1
		ltvi_item.selectedpictureindex = 1
		ltvi_item.overlaypictureindex = 1
		ltvi_item.children = true
	else
		ltvi_item.pictureindex = 2
		ltvi_item.selectedpictureindex = 2
		ltvi_item.overlaypictureindex = 2
		ltvi_item.children = false
	end if
	
	tv_1.insertitemlast(al_handle, ltvi_item)
next
end function

public function integer wf_trova (string as_ricerca, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tv_1.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_1.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
		tv_1.setfocus()
		tv_1.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(as_ricerca,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_handle = tv_1.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_1.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
		tv_1.setfocus()
		tv_1.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(as_ricerca,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_padre = al_handle

do
	
	ll_padre = tv_1.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tv_1.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tv_1.getitem(ll_handle,ltv_item)
			
			if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
				tv_1.setfocus()
				tv_1.selectitem(ll_handle)
				il_handle = ll_handle
				return 0
			else
				if wf_trova(as_ricerca,ll_handle) = 0 then
					return 0
				else
					return 100
				end if
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

on w_nav_doc_associa.create
int iCurrent
call super::create
this.cb_cerca=create cb_cerca
this.sle_cerca=create sle_cerca
this.cb_2=create cb_2
this.cb_1=create cb_1
this.tv_1=create tv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cerca
this.Control[iCurrent+2]=this.sle_cerca
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.tv_1
end on

on w_nav_doc_associa.destroy
call super::destroy
destroy(this.cb_cerca)
destroy(this.sle_cerca)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.tv_1)
end on

event pc_setwindow;call super::pc_setwindow;tv_1.DeletePictures()

tv_1.AddPicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\mappa.png")
tv_1.AddPicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\documento.png")

// pulisco i parametri
setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_i_1)
setnull(s_cs_xx.parametri.parametro_i_2)
// --

wf_carica_documenti(0,0)
end event

type cb_cerca from commandbutton within w_nav_doc_associa
integer x = 1806
integer y = 40
integer width = 297
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "cerca"
end type

event clicked;sle_cerca.text = trim(sle_cerca.text)

if len(sle_cerca.text) > 0 then
	
	cb_cerca.enabled = false
	
	if is_last_search = sle_cerca.text then
		wf_trova(sle_cerca.text, il_handle)
	else
		is_last_search = sle_cerca.text
		wf_trova(sle_cerca.text, 0)
	end if
	
	cb_cerca.enabled = true
end if
end event

type sle_cerca from singlelineedit within w_nav_doc_associa
integer x = 1120
integer y = 40
integer width = 663
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_2 from commandbutton within w_nav_doc_associa
integer x = 1349
integer y = 1000
integer width = 320
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = ""
close(parent)
end event

type cb_1 from commandbutton within w_nav_doc_associa
integer x = 1691
integer y = 1000
integer width = 411
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;treeviewitem ltvi_item
str_nav_doc_associa lstr_data

if il_handle < 1 then
	g_mb.messagebox("Navigatore", "Attenzione: non è stato selezionato nessun documento.")
	return -1
else
	tv_1.getitem(il_handle, ltvi_item)
	lstr_data = ltvi_item.data
	
	if lstr_data.tipo_elemento = "M" then
		g_mb.messagebox("Navigatore", "E' possibile selezionare solo un documento e non una mappa.")
		return -1
	end if
	
	s_cs_xx.parametri.parametro_i_1 = lstr_data.anno_registrazione
	s_cs_xx.parametri.parametro_i_2 = lstr_data.num_registrazione
	s_cs_xx.parametri.parametro_s_1 = string(lstr_data.tipo_elemento)
	s_cs_xx.parametri.parametro_s_2 = ltvi_item.label

	close(parent)
end if
end event

type tv_1 from treeview within w_nav_doc_associa
integer x = 23
integer y = 140
integer width = 2080
integer height = 816
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean linesatroot = true
string picturename[] = {"C:\cs_115\framework\risorse\11.5\mappa.png","C:\cs_115\framework\risorse\11.5\documento.png"}
long picturemaskcolor = 536870912
string statepicturename[] = {"C:\cs_115\framework\risorse\11.5\mappa.png","C:\cs_115\framework\risorse\11.5\documento.png"}
long statepicturemaskcolor = 536870912
end type

event itempopulate;treeviewitem ltvi_item
str_nav_doc_associa lstr_data

if handle < 1 then return

tv_1.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

if wf_carica_documenti(handle, lstr_data.cod_figlio) = 0 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if


end event

event selectionchanged;if newhandle < 1 then return

il_handle = newhandle
end event

event doubleclicked;if handle < 1 then return

cb_1.event clicked()
end event

