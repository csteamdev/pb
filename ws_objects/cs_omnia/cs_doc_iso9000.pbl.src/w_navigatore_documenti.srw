﻿$PBExportHeader$w_navigatore_documenti.srw
forward
global type w_navigatore_documenti from w_cs_xx_principale
end type
type dw_rects from datawindow within w_navigatore_documenti
end type
type p_hidden_1 from picture within w_navigatore_documenti
end type
type cbx_1 from checkbox within w_navigatore_documenti
end type
type st_split2 from uo_splitbar within w_navigatore_documenti
end type
type dw_navigatore from uo_navigatore within w_navigatore_documenti
end type
type st_split1 from uo_splitbar within w_navigatore_documenti
end type
type tv_mappe from treeview within w_navigatore_documenti
end type
end forward

global type w_navigatore_documenti from w_cs_xx_principale
integer width = 3547
integer height = 1896
windowstate windowstate = maximized!
event ue_mappa_refresh ( )
event ue_mappa_rinomina ( )
event ue_mappa_immagine ( )
event ue_mappa_aggiungi ( )
event ue_mappa_elimina ( )
event ue_documento_aggiungi ( )
event ue_documento_cambia ( )
event ue_documento_elimina ( )
event ue_mappa_area ( )
dw_rects dw_rects
p_hidden_1 p_hidden_1
cbx_1 cbx_1
st_split2 st_split2
dw_navigatore dw_navigatore
st_split1 st_split1
tv_mappe tv_mappe
end type
global w_navigatore_documenti w_navigatore_documenti

type variables
private string is_user_temporaly
private string is_user_documents

private long	il_handle = -1
private long il_drag_handle = -1
private long il_temp_handle = -1
private long il_save_handle = -1

private boolean ib_modified = false
private boolean ib_treeview_dbclick = false	// serve per impedire che al doppio click il nodo si chiuda
private boolean ib_auto_save = false

end variables

forward prototypes
public function string wf_userinfo (string as_key)
public function integer wf_carica_mappe (long al_handle, integer ai_prog_processo)
public function boolean wf_trova_immagine (ref string as_image_path, ref string as_image_name, ref integer ai_image_width, ref integer ai_image_height)
public function boolean wf_trova_documento (ref long al_prog_elemento, ref string as_tipo_elemento, ref string as_nome_elemento)
end prototypes

event ue_mappa_refresh();/*
 * Consente di ricaricare il nodo padre
 */
 
long ll_handle, ll_children_handle
treeviewitem ltvi_item

// ritrovo handle corrente
ll_handle = tv_mappe.finditem(currenttreeitem!, 0)

if ll_handle > 0 then
	tv_mappe.collapseitem(ll_handle)
	tv_mappe.getitem(ll_handle, ltvi_item)
	
	if ltvi_item.children then
		
		ll_children_handle = tv_mappe.FindItem(ChildTreeItem!, ll_handle)
		do until ll_children_handle = -1
			 tv_mappe.DeleteItem(ll_children_handle)
			 ll_children_handle = tv_mappe.FindItem(ChildTreeItem!, ll_handle)
		loop
		
	end if
	
	ltvi_item.children = true
	ltvi_item.expandedonce = false
	
	tv_mappe.setitem(ll_handle, ltvi_item)
end if
end event

event ue_mappa_rinomina();tv_mappe.editlabels = true
tv_mappe.editlabel(il_temp_handle)
end event

event ue_mappa_immagine();string ls_path, ls_name
int		li_width, li_height

if wf_trova_immagine(ls_path, ls_name, li_width, li_height) then
	dw_navigatore.uof_set_image(ls_path, ls_name, li_width, li_height)
end if
end event

event ue_mappa_aggiungi();string ls_path, ls_name
int		li_width, li_height, li_prog_processo_modello
long	ll_new_handle
treeviewitem ltvi_item, ltvi_new
str_navigatore_documenti lstr_data

if wf_trova_immagine(ls_path, ls_name, li_width, li_height) then
	
	if il_handle > 0 and il_temp_handle > 0 then
		il_temp_handle = il_handle
		tv_mappe.getitem(il_temp_handle, ltvi_item)
		lstr_data = ltvi_item.data
		li_prog_processo_modello = lstr_data.prog_processo
		
		/*
		PER USO FUTURO
		if istr_data.tipo_livello = "D" then
			// DOCUMENTO
		end if
		*/
		
		ltvi_new.pictureindex = 2
		ltvi_new.selectedpictureindex = 2
		ltvi_new.overlaypictureindex = 2
		
	elseif il_temp_handle = 0 then
		ltvi_new.pictureindex = 1
		ltvi_new.selectedpictureindex = 1
		ltvi_new.overlaypictureindex = 1
		
	end if
	
	lstr_data.livello = "M"
	lstr_data.prog_processo = -1
	
	ltvi_new.data = lstr_data
	ltvi_new.label = "Nuova Mappa"
	ltvi_new.children = false
	ltvi_new.selected = false
	
	ll_new_handle = tv_mappe.insertitemlast(il_temp_handle, ltvi_new)

	ltvi_item.children = true
	ltvi_item.expanded = true
	ltvi_item.selected = false
	tv_mappe.setitem(il_temp_handle, ltvi_item)

	tv_mappe.selectitem(ll_new_handle)
	
	dw_navigatore.uof_reset()
	dw_navigatore.uof_set_image(ls_path, ls_name, li_width, li_height)
	dw_navigatore.uof_set_desprocesso(ltvi_new.label)
	if il_temp_handle > 0 then
		dw_navigatore.uof_set_progprocesso_modello(li_prog_processo_modello)
	else
		dw_navigatore.uof_set_progprocesso_modello(-1)
	end if
	
	// per il salvataggio
	il_save_handle = ll_new_handle
	ib_modified = true
		
	dw_rects.reset()
	
	// salvo subito
	ib_auto_save = true
	event pc_save(1,1)
	ib_auto_save = false
end if

// forzo il redraw
tv_mappe.setredraw(true)
end event

event ue_mappa_elimina();string ls_message
treeviewitem ltvi_item
str_navigatore_documenti lstr_data

if il_temp_handle < 1 then
	return
end if
	
tv_mappe.getitem(il_temp_handle, ltvi_item)
if ltvi_item.children then
	g_mb.messagebox("Navigatore", "Eliminare prima le mappe figlie e poi procedere con la cancellazione del padre.")
	return
end if

ls_message = "Procedere con la cancellazione della mappa " + ltvi_item.label + " e di tutti i collegamenti associati?"
if g_mb.messagebox("Navigatore",ls_message, Question!, YesNo!) = 2 then
	return
end if

lstr_data = ltvi_item.data

if lstr_data.prog_processo < 1 then
	// la mappa non è salvata, basta eliminarla dall'albero
	tv_mappe.deleteitem(il_temp_handle)
		
elseif dw_navigatore.uof_remove_map(lstr_data.prog_processo) then
	
	dw_navigatore.uof_reset()
	dw_navigatore.uof_draw()
	dw_rects.reset()
	
	/*ll_new_handle = tv_1.FindItem(NextVisibleTreeItem!, il_handle)
	ll_old_handle = il_handle
	if ll_new_handle > -1 then
		
		tv_1.selectitem(ll_new_handle)
		tv_1.event doubleclicked(ll_new_handle)
				
	end if*/
	
	tv_mappe.deleteitem(il_temp_handle)
end if

il_temp_handle = -1
end event

event ue_documento_aggiungi();string		ls_tipo_collegamento, ls_nome_elemento
integer 	li_row, li_x, li_y	
long		ll_prog_elemento

li_x = PointerX() - dw_navigatore.x
li_y = PointerY() - dw_navigatore.y

if wf_trova_documento(ll_prog_elemento, ls_tipo_collegamento, ls_nome_elemento) then
	
	dw_navigatore.uof_save_rect_position(true)
	
	if dw_navigatore.uof_add_rect(li_x, li_y, 300, 200, -1, ll_prog_elemento, ls_tipo_collegamento, ls_nome_elemento) then
		dw_navigatore.uof_draw()
		ib_modified = true
	end if
	
end if 
end event

event ue_documento_cambia();string		ls_tipo_collegamento, ls_nome_elemento
long		ll_prog_elemento


if wf_trova_documento(ll_prog_elemento, ls_tipo_collegamento, ls_nome_elemento) then
	
	dw_navigatore.uof_save_rect_position(true)
	dw_navigatore.uof_set_rect_info(ll_prog_elemento, ls_tipo_collegamento, ls_nome_elemento)
	
	// TODO: aggiornare la dw_rects
	if dw_rects.getrow() > 0 then
		dw_rects.setitem(dw_rects.getrow(), "name", ls_nome_elemento)
	end if
	// ----
	
	ib_modified = true
	
end if 
end event

event ue_documento_elimina();dw_navigatore.uof_remove_rect()

if dw_rects.getrow() > 0 then
	dw_rects.deleterow(dw_rects.getrow())
end if

ib_modified = true
end event

event ue_mappa_area();treeviewitem ltvi_item
str_navigatore_documenti lstr_data

if il_temp_handle > 0 then
	
	open(w_nav_doc_aree)
	
	tv_mappe.getitem(il_temp_handle, ltvi_item)
	lstr_data = ltvi_item.data
	
	if s_cs_xx.parametri.parametro_s_1 <> "" then
			
		lstr_data.cod_area = s_cs_xx.parametri.parametro_s_1
		lstr_data.des_area = s_cs_xx.parametri.parametro_s_2
		
	else
		
		setnull(lstr_data.cod_area)
		setnull(lstr_data.des_area)
		
	end if
	
	ltvi_item.data = lstr_data
	tv_mappe.setitem(il_temp_handle, ltvi_item)
	
	dw_navigatore.uof_set_area(lstr_data.cod_area)
	
	setnull(s_cs_xx.parametri.parametro_s_1)
	setnull(s_cs_xx.parametri.parametro_s_2)
	
	il_temp_handle = -1
end if
end event

public function string wf_userinfo (string as_key);// Ritorna le variabili d'ambiente del sistema
string ls_Path
string ls_values[]
ContextKeyword lcxk_base

this.GetContextService("Keyword", lcxk_base)
lcxk_base.GetContextKeywords(as_key, ls_values)
IF Upperbound(ls_values) > 0 THEN
   ls_Path = ls_values[1]
ELSE
   ls_Path = "*UNDEFINED*"
END IF

return ls_Path
end function

public function integer wf_carica_mappe (long al_handle, integer ai_prog_processo);/**
 * Carico le mappe nella treeview. Se viene passato -1 al progressivo processo allora devo caricare
 * solo le mappe padri, altrimenti sto cercando i figli.
 */
 
string ls_sql
long	ll_count, ll_i
datastore lds_store
treeviewitem ltvi_item
str_navigatore_documenti lstr_data

if isnull(al_handle) or al_handle < 0 then
	al_handle = 0
end if

if ai_prog_processo < 1 then
	// solo padri
	ls_sql = "SELECT DISTINCT(prog_processo), des_processo, cod_area_aziendale FROM processi WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND prog_processo = prog_processo_modello"
else
	// solo figli
	ls_sql = "SELECT DISTINCT(prog_processo), des_processo, cod_area_aziendale FROM processi WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND prog_processo_modello=" + string(ai_prog_processo) + " AND prog_processo <> prog_processo_modello"
end if

// -- datastore
if not f_crea_datastore(ref lds_store, ls_sql) then
	g_mb.messagebox("Navigatore", "Errore durante la creazione del datastore per le mappe.~r~nwf_carica_mappe()")
	destroy lds_store
	return 0
end if

ll_count = lds_store.retrieve()
// ----

if ll_count <= 0 then
	return 0
else
	for ll_i = 1 to ll_count
		
		lstr_data.livello = "M"
		lstr_data.prog_processo = lds_store.getitemnumber(ll_i, 1)
		
		// Area aziendale
		if lds_store.getitemstring(ll_i, 3) <> "" then
			lstr_data.cod_area = lds_store.getitemstring(ll_i, 3)
			
			select des_area
			into	:lstr_data.des_area
			from 	tab_aree_aziendali 
			where cod_azienda=:s_cs_xx.cod_azienda and
					cod_area_aziendale=:lstr_data.cod_area;
		
		else
			lstr_data.cod_area = ""
			lstr_data.des_area = ""
		end if
		// ----
		
		ltvi_item.data = lstr_data
		ltvi_item.label = lds_store.getitemstring(ll_i, 2)
		ltvi_item.selected = false
		ltvi_item.children = true
		
		if ai_prog_processo < 1 then
			ltvi_item.pictureindex = 1
			ltvi_item.selectedpictureindex = 1
			ltvi_item.overlaypictureindex = 1
		else
			ltvi_item.pictureindex = 2
			ltvi_item.selectedpictureindex = 2
			ltvi_item.overlaypictureindex = 2
		end if
		
		tv_mappe.insertitemlast(al_handle, ltvi_item)
	next
	
	return ll_count
end if
end function

public function boolean wf_trova_immagine (ref string as_image_path, ref string as_image_name, ref integer ai_image_width, ref integer ai_image_height);integer li_return

li_return = GetFileOpenName("Seleziona un'immagine", as_image_path, as_image_name, "jpg", "File Immagine, *.bmp;*.gif;*.jpg;*.png", is_user_documents)

if li_return = 0 then
	// User press cancel
	return false
else
	p_hidden_1.picturename = as_image_path
	
	ai_image_width = p_hidden_1.width
	ai_image_height = p_hidden_1.height
	
	p_hidden_1.picturename = ""

	return true
end if
end function

public function boolean wf_trova_documento (ref long al_prog_elemento, ref string as_tipo_elemento, ref string as_nome_elemento);string ls_anno, ls_num

open(w_nav_doc_associa)

if isnull(s_cs_xx.parametri.parametro_s_1) or s_cs_xx.parametri.parametro_s_1 = "" then 
	return false
end if

al_prog_elemento =long(string(s_cs_xx.parametri.parametro_i_2) + string(s_cs_xx.parametri.parametro_i_1))
as_tipo_elemento = s_cs_xx.parametri.parametro_s_1
as_nome_elemento = s_cs_xx.parametri.parametro_s_2
return true
end function

on w_navigatore_documenti.create
int iCurrent
call super::create
this.dw_rects=create dw_rects
this.p_hidden_1=create p_hidden_1
this.cbx_1=create cbx_1
this.st_split2=create st_split2
this.dw_navigatore=create dw_navigatore
this.st_split1=create st_split1
this.tv_mappe=create tv_mappe
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_rects
this.Control[iCurrent+2]=this.p_hidden_1
this.Control[iCurrent+3]=this.cbx_1
this.Control[iCurrent+4]=this.st_split2
this.Control[iCurrent+5]=this.dw_navigatore
this.Control[iCurrent+6]=this.st_split1
this.Control[iCurrent+7]=this.tv_mappe
end on

on w_navigatore_documenti.destroy
call super::destroy
destroy(this.dw_rects)
destroy(this.p_hidden_1)
destroy(this.cbx_1)
destroy(this.st_split2)
destroy(this.dw_navigatore)
destroy(this.st_split1)
destroy(this.tv_mappe)
end on

event resize;call super::resize;tv_mappe.move(20,20)
tv_mappe.height = newheight - 50

st_split1.move(tv_mappe.x + tv_mappe.width, 20)
st_split1.height = tv_mappe.height

dw_navigatore.move(st_split1.x + st_split1.width, 20)
dw_navigatore.height = st_split1.height

cbx_1.move(newwidth - cbx_1.width -20 , 20)
st_split2.move(cbx_1.x - st_split2.width, 20)
st_split2.height = tv_mappe.height
dw_rects.move(cbx_1.x, cbx_1.y + cbx_1.height +  20)
dw_rects.height = st_split2.height - (cbx_1.y + cbx_1.height + 20)

dw_navigatore.width = st_split2.x - dw_navigatore.x

dw_navigatore.ii_max_dw_width = st_split2.x - dw_navigatore.x
dw_navigatore.ii_max_dw_height = st_split2.height
end event

event pc_save;call super::pc_save;treeviewitem ltvi_item
str_navigatore_documenti lstr_data

tv_mappe.enabled = false

if dw_navigatore.uof_save() then
	ib_modified = false
	
	if il_save_handle < 1 then
		g_mb.messagebox("Navigatore", "Per salvare è necessario salvare una mappa")
		/*if il_temp_handle < 1 then
			if il_handle < 1 then
				g_mb.messagebox("Navigatore", "Per salvare è necessario salvare una mappa")
				return
			end if
			
			il_temp_handle = il_handle
		end if*/
		return -1
	end if
	
	tv_mappe.getitem(il_save_handle, ltvi_item)
	lstr_data = ltvi_item.data
	
	lstr_data.livello = "M"
	lstr_data.prog_processo = dw_navigatore.uof_get_progprocesso()
	
	ltvi_item.data = lstr_data
	tv_mappe.setitem(il_save_handle, ltvi_item)
	
	il_handle = il_save_handle
	il_save_handle = -1
	
	if ib_auto_save = false then
		g_mb.messagebox("Navigatore", "Mappa salvata correttamente")
	end if
	
else
	
	if ib_auto_save = false then
		g_mb.messagebox("Navigatore", "Errore durante il salvataggio della mappa")
	end if
	
end if

tv_mappe.enabled = true
end event

event pc_setwindow;call super::pc_setwindow;// Calcolo percorsi utente
try
	f_getuserpath(is_user_temporaly)
	is_user_temporaly += "omnia_navigatore\"
	
	if not DirectoryExists(is_user_temporaly) then
		CreateDirectory(is_user_temporaly)
	end if
	
	is_user_documents  = wf_userinfo("HOMEDRIVE")
	is_user_documents += wf_userinfo("HOMEPATH")
	is_user_documents += "\Documenti\"
catch (RuntimeError ex)
	setnull(is_user_temporaly)
	setnull(is_user_documents)
end try
// ---

// Inizializzo navigatore
dw_navigatore.insertrow(0)
dw_navigatore.is_user_temppath = is_user_temporaly
dw_navigatore.is_user_documents = is_user_documents
dw_navigatore.il_color_map = 16744448
dw_navigatore.il_color_map_select = 16777088
dw_navigatore.il_color_document = 4227072
dw_navigatore.il_color_document_select = 4259584
// ----

wf_carica_mappe(0, 0)
end event

type dw_rects from datawindow within w_navigatore_documenti
integer x = 2834
integer y = 100
integer width = 663
integer height = 1680
integer taborder = 30
string title = "none"
string dataobject = "d_nav_doc_resoconto"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;if dwo.name = "visible" then
	
	dw_navigatore.uof_set_visibility(getitemnumber(row, "id"), data)
	
end if
end event

type p_hidden_1 from picture within w_navigatore_documenti
boolean visible = false
integer x = 3086
integer y = 740
integer width = 229
integer height = 200
boolean originalsize = true
boolean focusrectangle = false
end type

type cbx_1 from checkbox within w_navigatore_documenti
integer x = 2834
integer y = 20
integer width = 663
integer height = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seleziona Tutti"
end type

event clicked;string ls_visible
long ll_rows, ll_row

ll_rows = dw_rects.rowcount()

if ll_rows > 0 then
	if checked then
		ls_visible = "1"
	else
		ls_visible = "0"
	end if
	
	dw_navigatore.setredraw(false)
	for ll_row = 1 to ll_rows
		
		if dw_rects.getitemstring(ll_row, "visible") <> ls_visible then
			dw_rects.setitem(ll_row, "visible", ls_visible)
			dw_rects.event itemchanged(ll_row, dw_rects.object.visible, ls_visible)
		end if
		
	next
	dw_navigatore.setredraw(true)
	
end if
end event

type st_split2 from uo_splitbar within w_navigatore_documenti
integer x = 2789
integer y = 20
integer width = 32
integer height = 1760
end type

event clicked;call super::clicked;uof_register(dw_navigatore, LEFT)
uof_register(cbx_1, RIGHT)
uof_register(dw_rects, RIGHT)
end event

type dw_navigatore from uo_navigatore within w_navigatore_documenti
integer x = 754
integer y = 20
integer width = 2034
integer height = 1760
integer taborder = 20
boolean hscrollbar = true
boolean vscrollbar = true
end type

event dragdrop;call super::dragdrop;int li_x, li_y
treeviewitem ltvi_item
str_navigatore_documenti lstr_data

if il_drag_handle > 0 then
	
	tv_mappe.getitem(il_drag_handle, ltvi_item)
	lstr_data = ltvi_item.data
	
	if lstr_data.prog_processo < 1 then
		g_mb.messagebox("Navigatore", "Attenzione: la mappa non risulta ancora salva quindi procedere con il salvataggio e ripetere l'azione")
		return
	end if

	li_x = PointerX()// - dw_navigatore.x
	li_y = PointerY()// - dw_navigatore.y
	
	dw_navigatore.uof_save_rect_position(true)
	dw_navigatore.uof_add_rect(li_x, li_y, 300, 200, -1, long(lstr_data.prog_processo), "M", ltvi_item.label)
	dw_navigatore.uof_draw()
	ib_modified = true
	il_drag_handle = -1
	
end if

il_drag_handle = -1
end event

event rbuttondown;call super::rbuttondown;int li_find

m_navigatore_documenti lm_menu
lm_menu = create m_navigatore_documenti

if left(string(dwo.name), 2) = "r_" then
	lm_menu.m_documento_aggiungi.visible = false
else
	lm_menu.m_documento_cambia.visible = false
	lm_menu.m_sep.visible = false
	lm_menu.m_documento_elimina.visible = false
end if

lm_menu.popmenu(parent.PointerX(), parent.PointerY() + 100)
destroy lm_menu
end event

event ue_rect_added;call super::ue_rect_added;long ll_row

ll_row = dw_rects.insertrow(0)

dw_rects.setitem(ll_row, "id", as_index) // prelevo la parte destra di r_xxx
dw_rects.setitem(ll_row, "name", as_nome_elemento)
dw_rects.setitem(ll_row, "visible", 1)
dw_rects.setitem(ll_row, "tipo_elemento", as_tipo_elemento)
end event

event ue_rect_removed;call super::ue_rect_removed;// as
end event

event ue_rect_selected;call super::ue_rect_selected;int li_find

dw_rects.selectrow(0, false)
li_find = dw_rects.find("id=" + string(ai_index), 1, dw_rects.rowcount())
if li_find > 0 then
	dw_rects.selectrow(li_find, true)
end if
end event

event ue_map_rename;call super::ue_map_rename;int li_find

dw_rects.selectrow(0, false)
li_find = dw_rects.find("id=" + string(ai_index), 1, dw_rects.rowcount())
if li_find > 0 then
	dw_rects.setitem(li_find, "name", as_newtext)
end if
end event

type st_split1 from uo_splitbar within w_navigatore_documenti
integer x = 731
integer y = 20
integer width = 32
integer height = 1760
end type

event constructor;call super::constructor;uof_register(tv_mappe, LEFT)
uof_register(dw_navigatore, RIGHT)
end event

type tv_mappe from treeview within w_navigatore_documenti
integer x = 23
integer y = 20
integer width = 709
integer height = 1760
integer taborder = 10
string dragicon = "Window!"
boolean dragauto = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean linesatroot = true
boolean disabledragdrop = false
string picturename[] = {"Application5!","ArrangeIcons!"}
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event doubleclicked;treeviewitem ltvi_item
str_navigatore_documenti lstr_data

if handle < 1 then
	return
end if

// Salvo modifiche
if ib_modified then
	if g_mb.messagebox("Navigatore", "Ci sono modifiche pendenti, salvarle?", Question!, YesNo!) = 1 then
		event pc_save(1,1)
	end if
end if
// ----

tv_mappe.enabled = false

il_handle = handle
il_save_handle = handle
ib_treeview_dbclick = true

tv_mappe.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

dw_rects.reset()
dw_navigatore.uof_load_map(lstr_data.prog_processo)

tv_mappe.enabled = true


end event

event itemexpanding;// se la variabile ib_treeview_dbclick è a true allora ho fatto doppio click sul nodo
// e devo prevenire la chiusuara/apertura del nodo.

if ib_treeview_dbclick then
	ib_treeview_dbclick = false
	return 1
end if
end event

event itempopulate;treeviewitem ltvi_item
str_navigatore_documenti lstr_data

// Se la variabile è a true allora ho fatto doppio click e quindi non devo popolare niente, altrimenti
// l'albero viene riempito due volte con gli stessi dati
if ib_treeview_dbclick then
	return
end if
// ----

getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

choose case lstr_data.livello
		
	// al momento vengono salvate solo mappe quindi è logico che ci sia solo questo
	// nodo ma in futuro potrebbe essere necessario visualizzare anche i documenti.
	case "M"
		if wf_carica_mappe(handle, lstr_data.prog_processo) = 0 then
			ltvi_item.children = false
			setitem(handle, ltvi_item)
		end if
			
end choose
end event

event dragwithin;// Bisogna usare una variabile e settarla solo una volta altrimenti se l'ultente
// scorre sopra gli altri nodi della tv l'handle viene cambiato con l'ultimo selezionato.

if il_drag_handle = -1 then
	il_drag_handle = handle
end if
end event

event itemcollapsing;// se la variabile ib_treeview_dbclick è a true allora ho fatto doppio click sul nodo
// e devo prevenire la chiusuara/apertura del nodo.

if ib_treeview_dbclick then
	ib_treeview_dbclick = false
	return 1
end if
end event

event rightclicked;treeviewitem ltvi_item
str_navigatore_documenti lstr_data
m_navigatore_mappe lm_menu

lm_menu = create m_navigatore_mappe

// TODO: controllare modifiche

il_temp_handle = handle

if handle > 0 then
	// Il nodo esiste
	getitem(handle, ltvi_item)
	lstr_data = ltvi_item.data
	
	// Pulsante "refresh" solo sul padre
	if ltvi_item.children then
		lm_menu.m_mappa_refresh.visible = true
	else
		lm_menu.m_mappa_refresh.visible = false
	end if
	// ----
	
	// Pulsante "aggiungi mappa" solo se salvata e se sono nel nodo corrente
	if lstr_data.prog_processo >= 1 and il_handle = handle then
		lm_menu.m_mappa_aggiungi.visible = true
		lm_menu.m_mappa_area.visible = true
	else
		lm_menu.m_mappa_aggiungi.visible = false
		lm_menu.m_mappa_area.visible = false
	end if
	// ----
	
	// Area aziendale associata
	if lstr_data.cod_area <> "" then
		lm_menu.m_mappa_attuale.visible = true
		lm_menu.m_mappa_attuale.text = lstr_data.des_area
		lm_menu.m_sep1.visible = true
	else
		lm_menu.m_mappa_attuale.visible = false
		lm_menu.m_sep1.visible = false
	end if
	// ----
	
	// Pulsanti "cambia immagine" solo se sono nel nodo corrente
	if handle = il_handle then
		lm_menu.m_mappa_immagine.visible = true
	else
		lm_menu.m_mappa_immagine.visible = false
	end if
	// ----
else
	// ho cliccato sul bianco, probabilmente voglio aggiungere una nuova mappa padre
	il_temp_handle = 0
	lm_menu.m_mappa_immagine.visible = false
	lm_menu.m_mappa_rinomina.visible = false
	lm_menu.m_mappa_refresh.visible = false
	lm_menu.m_sep.visible = false
	lm_menu.m_mappa_elimina.visible = false
end if

lm_menu.popmenu(PointerX(), PointerY() + 100)

destroy lm_menu
end event

event endlabeledit;treeviewitem ltvi_item
str_navigatore_documenti lstr_data

tv_mappe.editlabels = false

if handle < 1 or isnull(newtext) or newtext = "" then
	return
end if

tv_mappe.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

if lstr_data.prog_processo < 1 then
	// Mappa non ancora salvata, ancora in modalità modifica
	dw_navigatore.uof_set_desprocesso(newtext)
	ib_modified = true
else
	
	update 
		processi
	set
		des_processo = :newtext
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		prog_processo = :lstr_data.prog_processo;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Navigatore", "Errore durante l'azione di rinomina mappa.~r~n" + sqlca.sqlerrtext)
		rollback;
		il_temp_handle = -1
		return -1
	end if
	
	// aggiorno i nodi che hanno questa mappa linkata
	update
		tab_collegamenti_processi
	set
		valore = :newtext
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		prog_elemento = :lstr_data.prog_processo and
		tipo_elemento = "M";
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Navigatore", "Errore durante l'aggiornamento dei collegamenti a questa mappa.~r~n"+sqlca.sqlerrtext)
		rollback;
		il_temp_handle = -1
		return -1
	else
		commit;
	end if
	// ----
	
	// aggiorno i nodi che sono caricati nella mappa attuale
	dw_navigatore.uof_rename_child(lstr_data.prog_processo, newtext)
	// ----
	
	il_temp_handle = -1
end if
end event

