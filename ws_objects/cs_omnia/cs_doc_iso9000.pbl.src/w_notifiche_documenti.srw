﻿$PBExportHeader$w_notifiche_documenti.srw
forward
global type w_notifiche_documenti from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_notifiche_documenti
end type
type dw_1 from uo_cs_xx_dw within w_notifiche_documenti
end type
end forward

global type w_notifiche_documenti from w_cs_xx_principale
integer width = 2002
integer height = 1568
string title = "Notifche Lettura"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_1 cb_1
dw_1 dw_1
end type
global w_notifiche_documenti w_notifiche_documenti

on w_notifiche_documenti.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_1
end on

on w_notifiche_documenti.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_1)
end on

event open;call super::open;long ll_anno, ll_num, ll_rev
s_cs_xx_parametri lstr_params

lstr_params = message.powerobjectparm

setnull(message.powerobjectparm)

ll_anno = lstr_params.parametro_ul_1
ll_num = lstr_params.parametro_ul_2
ll_rev = lstr_params.parametro_ul_3

dw_1.settransobject(sqlca)
dw_1.retrieve(s_cs_xx.cod_azienda, ll_anno, ll_num, ll_rev)
end event

type cb_1 from commandbutton within w_notifiche_documenti
integer x = 1554
integer y = 1360
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Chiudi"
end type

event clicked;close(parent)
end event

type dw_1 from uo_cs_xx_dw within w_notifiche_documenti
integer x = 23
integer y = 20
integer width = 1943
integer height = 1300
integer taborder = 10
string dataobject = "d_det_documenti_letture"
boolean vscrollbar = true
end type

