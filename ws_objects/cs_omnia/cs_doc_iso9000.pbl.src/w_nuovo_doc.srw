﻿$PBExportHeader$w_nuovo_doc.srw
$PBExportComments$Windows nuovo doc
forward
global type w_nuovo_doc from w_cs_xx_risposta
end type
type em_validato from editmask within w_nuovo_doc
end type
type em_approvato from editmask within w_nuovo_doc
end type
type em_verificato from editmask within w_nuovo_doc
end type
type em_emesso from editmask within w_nuovo_doc
end type
type st_validato_il from statictext within w_nuovo_doc
end type
type st_approvato_il from statictext within w_nuovo_doc
end type
type st_verificato_il from statictext within w_nuovo_doc
end type
type st_emesso_il from statictext within w_nuovo_doc
end type
type ddlb_validato from dropdownlistbox within w_nuovo_doc
end type
type ddlb_approvato from dropdownlistbox within w_nuovo_doc
end type
type ddlb_verificato from dropdownlistbox within w_nuovo_doc
end type
type ddlb_emesso from dropdownlistbox within w_nuovo_doc
end type
type st_validato_da from statictext within w_nuovo_doc
end type
type st_approvato_da from statictext within w_nuovo_doc
end type
type st_verificato_da from statictext within w_nuovo_doc
end type
type st_emesso_da from statictext within w_nuovo_doc
end type
type cb_sfoglia from picturebutton within w_nuovo_doc
end type
type st_file from statictext within w_nuovo_doc
end type
type st_1 from statictext within w_nuovo_doc
end type
type cb_annulla from commandbutton within w_nuovo_doc
end type
type cb_ok from commandbutton within w_nuovo_doc
end type
type rb_2 from radiobutton within w_nuovo_doc
end type
type st_path_modelli from statictext within w_nuovo_doc
end type
type st_8 from statictext within w_nuovo_doc
end type
type rb_1 from radiobutton within w_nuovo_doc
end type
type lb_lista_doc from listbox within w_nuovo_doc
end type
type em_revisione from editmask within w_nuovo_doc
end type
type st_21 from statictext within w_nuovo_doc
end type
type em_edizione from editmask within w_nuovo_doc
end type
type st_16 from statictext within w_nuovo_doc
end type
type ddlb_area from dropdownlistbox within w_nuovo_doc
end type
type st_7 from statictext within w_nuovo_doc
end type
type ddlb_livello_doc from dropdownlistbox within w_nuovo_doc
end type
type st_15 from statictext within w_nuovo_doc
end type
type em_gg_validita from editmask within w_nuovo_doc
end type
type st_9 from statictext within w_nuovo_doc
end type
type em_des_documento from editmask within w_nuovo_doc
end type
type st_5 from statictext within w_nuovo_doc
end type
type em_nome_doc from editmask within w_nuovo_doc
end type
type st_2 from statictext within w_nuovo_doc
end type
type gb_3 from groupbox within w_nuovo_doc
end type
type gb_1 from groupbox within w_nuovo_doc
end type
type gb_firme from groupbox within w_nuovo_doc
end type
end forward

global type w_nuovo_doc from w_cs_xx_risposta
integer width = 3095
integer height = 1944
string title = "Nuovo Documento"
boolean controlmenu = false
em_validato em_validato
em_approvato em_approvato
em_verificato em_verificato
em_emesso em_emesso
st_validato_il st_validato_il
st_approvato_il st_approvato_il
st_verificato_il st_verificato_il
st_emesso_il st_emesso_il
ddlb_validato ddlb_validato
ddlb_approvato ddlb_approvato
ddlb_verificato ddlb_verificato
ddlb_emesso ddlb_emesso
st_validato_da st_validato_da
st_approvato_da st_approvato_da
st_verificato_da st_verificato_da
st_emesso_da st_emesso_da
cb_sfoglia cb_sfoglia
st_file st_file
st_1 st_1
cb_annulla cb_annulla
cb_ok cb_ok
rb_2 rb_2
st_path_modelli st_path_modelli
st_8 st_8
rb_1 rb_1
lb_lista_doc lb_lista_doc
em_revisione em_revisione
st_21 st_21
em_edizione em_edizione
st_16 st_16
ddlb_area ddlb_area
st_7 st_7
ddlb_livello_doc ddlb_livello_doc
st_15 st_15
em_gg_validita em_gg_validita
st_9 st_9
em_des_documento em_des_documento
st_5 st_5
em_nome_doc em_nome_doc
st_2 st_2
gb_3 gb_3
gb_1 gb_1
gb_firme gb_firme
end type
global w_nuovo_doc w_nuovo_doc

type variables
string  is_percorso_mod
boolean ib_dcp, ib_chiudi = false


end variables

forward prototypes
public function integer wf_parametri (ref string fs_path_mod)
public function integer wf_carica_utenti_firme ()
end prototypes

public function integer wf_parametri (ref string fs_path_mod);string ls_vql,ls_default,ls_percorso_mod
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vql", ls_vql)

SELECT stringa
INTO 	 :ls_percorso_mod
FROM 	 parametri_azienda
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    cod_parametro = 'PTM';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore in ricerca parametro PTM in parametri azienda: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

fs_path_mod = ls_vql + ls_percorso_mod

return 0
end function

public function integer wf_carica_utenti_firme ();f_PO_LoadDDLB(ddlb_emesso, &
              sqlca, &
				  "mansionari", &
				  "cod_resp_divisione", &
				  "cognome + ' ' + nome", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
				  "")
				  
	
				  
f_PO_LoadDDLB(ddlb_verificato, &
              sqlca, &
				  "mansionari", &
				  "cod_resp_divisione", &
				  "cognome + ' ' + nome", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
				  "")
				  
f_PO_LoadDDLB(ddlb_approvato, &
              sqlca, &
				  "mansionari", &
				  "cod_resp_divisione", &
				  "cognome + ' ' + nome", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
				  "")
				  
f_PO_LoadDDLB(ddlb_validato, &
              sqlca, &
				  "mansionari", &
				  "cod_resp_divisione", &
				  "cognome + ' ' + nome", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
				  "")

return 0
end function

on w_nuovo_doc.create
int iCurrent
call super::create
this.em_validato=create em_validato
this.em_approvato=create em_approvato
this.em_verificato=create em_verificato
this.em_emesso=create em_emesso
this.st_validato_il=create st_validato_il
this.st_approvato_il=create st_approvato_il
this.st_verificato_il=create st_verificato_il
this.st_emesso_il=create st_emesso_il
this.ddlb_validato=create ddlb_validato
this.ddlb_approvato=create ddlb_approvato
this.ddlb_verificato=create ddlb_verificato
this.ddlb_emesso=create ddlb_emesso
this.st_validato_da=create st_validato_da
this.st_approvato_da=create st_approvato_da
this.st_verificato_da=create st_verificato_da
this.st_emesso_da=create st_emesso_da
this.cb_sfoglia=create cb_sfoglia
this.st_file=create st_file
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.rb_2=create rb_2
this.st_path_modelli=create st_path_modelli
this.st_8=create st_8
this.rb_1=create rb_1
this.lb_lista_doc=create lb_lista_doc
this.em_revisione=create em_revisione
this.st_21=create st_21
this.em_edizione=create em_edizione
this.st_16=create st_16
this.ddlb_area=create ddlb_area
this.st_7=create st_7
this.ddlb_livello_doc=create ddlb_livello_doc
this.st_15=create st_15
this.em_gg_validita=create em_gg_validita
this.st_9=create st_9
this.em_des_documento=create em_des_documento
this.st_5=create st_5
this.em_nome_doc=create em_nome_doc
this.st_2=create st_2
this.gb_3=create gb_3
this.gb_1=create gb_1
this.gb_firme=create gb_firme
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_validato
this.Control[iCurrent+2]=this.em_approvato
this.Control[iCurrent+3]=this.em_verificato
this.Control[iCurrent+4]=this.em_emesso
this.Control[iCurrent+5]=this.st_validato_il
this.Control[iCurrent+6]=this.st_approvato_il
this.Control[iCurrent+7]=this.st_verificato_il
this.Control[iCurrent+8]=this.st_emesso_il
this.Control[iCurrent+9]=this.ddlb_validato
this.Control[iCurrent+10]=this.ddlb_approvato
this.Control[iCurrent+11]=this.ddlb_verificato
this.Control[iCurrent+12]=this.ddlb_emesso
this.Control[iCurrent+13]=this.st_validato_da
this.Control[iCurrent+14]=this.st_approvato_da
this.Control[iCurrent+15]=this.st_verificato_da
this.Control[iCurrent+16]=this.st_emesso_da
this.Control[iCurrent+17]=this.cb_sfoglia
this.Control[iCurrent+18]=this.st_file
this.Control[iCurrent+19]=this.st_1
this.Control[iCurrent+20]=this.cb_annulla
this.Control[iCurrent+21]=this.cb_ok
this.Control[iCurrent+22]=this.rb_2
this.Control[iCurrent+23]=this.st_path_modelli
this.Control[iCurrent+24]=this.st_8
this.Control[iCurrent+25]=this.rb_1
this.Control[iCurrent+26]=this.lb_lista_doc
this.Control[iCurrent+27]=this.em_revisione
this.Control[iCurrent+28]=this.st_21
this.Control[iCurrent+29]=this.em_edizione
this.Control[iCurrent+30]=this.st_16
this.Control[iCurrent+31]=this.ddlb_area
this.Control[iCurrent+32]=this.st_7
this.Control[iCurrent+33]=this.ddlb_livello_doc
this.Control[iCurrent+34]=this.st_15
this.Control[iCurrent+35]=this.em_gg_validita
this.Control[iCurrent+36]=this.st_9
this.Control[iCurrent+37]=this.em_des_documento
this.Control[iCurrent+38]=this.st_5
this.Control[iCurrent+39]=this.em_nome_doc
this.Control[iCurrent+40]=this.st_2
this.Control[iCurrent+41]=this.gb_3
this.Control[iCurrent+42]=this.gb_1
this.Control[iCurrent+43]=this.gb_firme
end on

on w_nuovo_doc.destroy
call super::destroy
destroy(this.em_validato)
destroy(this.em_approvato)
destroy(this.em_verificato)
destroy(this.em_emesso)
destroy(this.st_validato_il)
destroy(this.st_approvato_il)
destroy(this.st_verificato_il)
destroy(this.st_emesso_il)
destroy(this.ddlb_validato)
destroy(this.ddlb_approvato)
destroy(this.ddlb_verificato)
destroy(this.ddlb_emesso)
destroy(this.st_validato_da)
destroy(this.st_approvato_da)
destroy(this.st_verificato_da)
destroy(this.st_emesso_da)
destroy(this.cb_sfoglia)
destroy(this.st_file)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.rb_2)
destroy(this.st_path_modelli)
destroy(this.st_8)
destroy(this.rb_1)
destroy(this.lb_lista_doc)
destroy(this.em_revisione)
destroy(this.st_21)
destroy(this.em_edizione)
destroy(this.st_16)
destroy(this.ddlb_area)
destroy(this.st_7)
destroy(this.ddlb_livello_doc)
destroy(this.st_15)
destroy(this.em_gg_validita)
destroy(this.st_9)
destroy(this.em_des_documento)
destroy(this.st_5)
destroy(this.em_nome_doc)
destroy(this.st_2)
destroy(this.gb_3)
destroy(this.gb_1)
destroy(this.gb_firme)
end on

event pc_setwindow;call super::pc_setwindow;integer li_risposta
string  ls_dcp

li_risposta=wf_parametri(is_percorso_mod)

lb_lista_doc.DirList(is_percorso_mod+"\*.*", 0)

lb_lista_doc.SelectItem(1)

st_path_modelli.text = is_percorso_mod
em_edizione.text = "1"
em_revisione.text = "0"
em_gg_validita.text = "0"

ddlb_livello_doc.AddItem ("A")
ddlb_livello_doc.AddItem ("B")
ddlb_livello_doc.AddItem ("C")

rb_1.postevent("clicked")

select flag
into   :ls_dcp
from   parametri
where  flag_parametro = 'F' and
		 cod_parametro = 'DCP';
		 
if sqlca.sqlcode = 0 then	
	if ls_dcp = 'S' then
		ib_dcp = true
	else
		ib_dcp = false
	end if	
elseif sqlca.sqlcode = 100 then	
	ib_dcp = false	
elseif sqlca.sqlcode < 0 then	
	g_mb.messagebox("OMNIA","Errore in lettura parametro DCP da tabella parametri: " + sqlca.sqlerrtext + "~nParametro impostato a 'N' per default")
	ib_dcp = false	
end if

if ib_dcp then
	wf_carica_utenti_firme()
	gb_firme.enabled = true
	st_emesso_da.enabled = true
	ddlb_emesso.enabled = true
else
	gb_firme.enabled = false
	st_emesso_da.enabled = false
	st_emesso_il.enabled = false
	st_verificato_da.enabled = false
	st_verificato_il.enabled = false
	st_approvato_da.enabled = false
	st_approvato_il.enabled = false
	st_validato_da.enabled = false
	st_validato_il.enabled = false
	ddlb_emesso.enabled = false
	ddlb_verificato.enabled = false
	ddlb_approvato.enabled = false
	ddlb_validato.enabled = false
	em_emesso.enabled = false
	em_verificato.enabled = false
	em_approvato.enabled = false
	em_validato.enabled = false
end if
end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_area, &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

event closequery;call super::closequery;if ib_chiudi = false then
	return 1
end if	
end event

type em_validato from editmask within w_nuovo_doc
integer x = 2560
integer y = 1700
integer width = 402
integer height = 84
integer taborder = 200
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
end type

type em_approvato from editmask within w_nuovo_doc
integer x = 2560
integer y = 1580
integer width = 402
integer height = 84
integer taborder = 180
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
end type

event modified;st_validato_da.enabled = true
ddlb_validato.enabled = true
end event

type em_verificato from editmask within w_nuovo_doc
integer x = 2560
integer y = 1460
integer width = 402
integer height = 84
integer taborder = 160
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
end type

event modified;st_approvato_da.enabled = true
ddlb_approvato.enabled = true
end event

type em_emesso from editmask within w_nuovo_doc
integer x = 2560
integer y = 1340
integer width = 402
integer height = 84
integer taborder = 140
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
end type

event modified;st_verificato_da.enabled = true
ddlb_verificato.enabled = true
end event

type st_validato_il from statictext within w_nuovo_doc
integer x = 2194
integer y = 1700
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Validato il:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type st_approvato_il from statictext within w_nuovo_doc
integer x = 2194
integer y = 1580
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Approvato il:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type st_verificato_il from statictext within w_nuovo_doc
integer x = 2194
integer y = 1460
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Verificato il:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type st_emesso_il from statictext within w_nuovo_doc
integer x = 2194
integer y = 1340
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Emesso il:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type ddlb_validato from dropdownlistbox within w_nuovo_doc
integer x = 434
integer y = 1700
integer width = 1349
integer height = 616
integer taborder = 190
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;st_validato_il.enabled = true
em_validato.enabled = true
end event

type ddlb_approvato from dropdownlistbox within w_nuovo_doc
integer x = 434
integer y = 1580
integer width = 1349
integer height = 616
integer taborder = 170
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;st_approvato_il.enabled = true
em_approvato.enabled = true
end event

type ddlb_verificato from dropdownlistbox within w_nuovo_doc
integer x = 434
integer y = 1460
integer width = 1349
integer height = 616
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;st_verificato_il.enabled = true
em_verificato.enabled = true
end event

type ddlb_emesso from dropdownlistbox within w_nuovo_doc
integer x = 434
integer y = 1340
integer width = 1349
integer height = 600
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;st_emesso_il.enabled = true
em_emesso.enabled = true
end event

type st_validato_da from statictext within w_nuovo_doc
integer x = 69
integer y = 1700
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Validato da:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type st_approvato_da from statictext within w_nuovo_doc
integer x = 69
integer y = 1580
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Approvato da:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type st_verificato_da from statictext within w_nuovo_doc
integer x = 69
integer y = 1460
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Verificato da:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type st_emesso_da from statictext within w_nuovo_doc
integer x = 69
integer y = 1340
integer width = 361
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Emesso da:"
alignment alignment = right!
boolean focusrectangle = false
boolean disabledlook = true
end type

type cb_sfoglia from picturebutton within w_nuovo_doc
integer x = 1010
integer y = 948
integer width = 101
integer height = 88
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "C:\cs_70\framework\RISORSE\MENUVOCA.BMP"
string disabledname = "C:\cs_70\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string docname,named
integer result, li_ret
 
li_ret = GetFileOpenName("Select File", docname, named, "", "Tutti i Files (*.*),*.*,")
if li_ret < 0 then
	g_mb.messagebox("Omnia","Impossibile trovare l'oogetto selezionato",Stopsign!)
	return
end if
st_file.text = docname
end event

type st_file from statictext within w_nuovo_doc
integer x = 709
integer y = 1120
integer width = 1554
integer height = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_nuovo_doc
integer x = 23
integer y = 1120
integer width = 672
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Oggetto/file selezionato:"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_nuovo_doc
integer x = 2286
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = -1
ib_chiudi = true
close(parent)
end event

type cb_ok from commandbutton within w_nuovo_doc
integer x = 2674
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if len(trim(st_file.text)) < 1 or isnull(trim(st_file.text)) then
	g_mb.messagebox("Omnia","Selezionare un oggetto facendo doppio click sulla lista dei modelli o con la ricerca libera",StopSign!)
	return
end if

s_cs_xx.parametri.parametro_s_1 = em_nome_doc.text
s_cs_xx.parametri.parametro_s_2 = em_des_documento.text
s_cs_xx.parametri.parametro_s_3 = f_po_selectddlb(ddlb_area)
s_cs_xx.parametri.parametro_s_4 = st_file.text
s_cs_xx.parametri.parametro_s_5 = f_po_selectddlb(ddlb_livello_doc)	
s_cs_xx.parametri.parametro_ul_1=long(em_edizione.text)
s_cs_xx.parametri.parametro_ul_2=long(em_revisione.text)
s_cs_xx.parametri.parametro_ul_3=long(em_gg_validita.text)

if isnull(s_cs_xx.parametri.parametro_s_5) or len(s_cs_xx.parametri.parametro_s_5) < 1 then
	g_mb.messagebox("Omnia","La selezione del livello del documento è obbligatoria.",StopSign!)
	return
end if

if isnull(s_cs_xx.parametri.parametro_s_3) or len(s_cs_xx.parametri.parametro_s_3) < 1 then
	g_mb.messagebox("Omnia","La selezione dell'area è obbligatoria.",StopSign!)
	return
end if

// ************* Modifica Michele per specifica Sempl_doc_iso9000  14/11/2001 *****************

if ib_dcp then
	
	s_cs_xx.parametri.parametro_s_6 = f_po_selectddlb(ddlb_emesso)
	
	if s_cs_xx.parametri.parametro_s_6 = "" then
		setnull(s_cs_xx.parametri.parametro_s_6)
	end if
	
	s_cs_xx.parametri.parametro_s_7 = f_po_selectddlb(ddlb_verificato)
	
	if s_cs_xx.parametri.parametro_s_7 = "" then
		setnull(s_cs_xx.parametri.parametro_s_7)
	end if
	
	s_cs_xx.parametri.parametro_s_8 = f_po_selectddlb(ddlb_approvato)
	
	if s_cs_xx.parametri.parametro_s_8 = "" then
		setnull(s_cs_xx.parametri.parametro_s_8)
	end if
	
	s_cs_xx.parametri.parametro_s_9 = f_po_selectddlb(ddlb_validato)
	
	if s_cs_xx.parametri.parametro_s_9 = "" then
		setnull(s_cs_xx.parametri.parametro_s_9)
	end if
	
	if em_emesso.text = "" then
		setnull(s_cs_xx.parametri.parametro_data_1)
	else
		s_cs_xx.parametri.parametro_data_1 = datetime(date(em_emesso.text),00:00:00)
	end if
	
	if em_verificato.text = "" then
		setnull(s_cs_xx.parametri.parametro_data_2)
	else
		s_cs_xx.parametri.parametro_data_2 = datetime(date(em_verificato.text),00:00:00)
	end if
	
	if em_approvato.text = "" then
		setnull(s_cs_xx.parametri.parametro_data_3)
	else
		s_cs_xx.parametri.parametro_data_3 = datetime(date(em_approvato.text),00:00:00)
	end if
	
	if em_validato.text = "" then
		setnull(s_cs_xx.parametri.parametro_data_4)
	else
		s_cs_xx.parametri.parametro_data_4 = datetime(date(em_validato.text),00:00:00)
	end if
	
	s_cs_xx.parametri.parametro_i_1 = 100
	
else
	
	s_cs_xx.parametri.parametro_i_1 = 0
	
end if

// ************* Fine Modifica *****************

ib_chiudi = true

close(parent)
end event

type rb_2 from radiobutton within w_nuovo_doc
integer x = 69
integer y = 948
integer width = 891
integer height = 76
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Crea associando oggetto libero"
end type

event clicked;if checked=true then 
	cb_sfoglia.enabled=true
	lb_lista_doc.enabled=false
else
	cb_sfoglia.enabled=false
	lb_lista_doc.enabled=true
end if
end event

type st_path_modelli from statictext within w_nuovo_doc
integer x = 1989
integer y = 100
integer width = 1051
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type st_8 from statictext within w_nuovo_doc
integer x = 1989
integer y = 20
integer width = 1051
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Modelli o documenti disponibili in"
alignment alignment = center!
boolean focusrectangle = false
end type

type rb_1 from radiobutton within w_nuovo_doc
integer x = 69
integer y = 824
integer width = 809
integer height = 60
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Crea da modello/documento"
boolean checked = true
end type

event clicked;//lb_1.DirList(i_percorso_mod+"\*.dot", 0)
//lb_1.SelectItem(1)
//st_8.text = "Modelli Disponibili"
//lb_1.triggerevent("selectionchanged")

if checked=true then 
	cb_sfoglia.enabled=false
	lb_lista_doc.enabled=true
else
	cb_sfoglia.enabled=true
	lb_lista_doc.enabled=false
end if
end event

type lb_lista_doc from listbox within w_nuovo_doc
integer x = 1975
integer y = 184
integer width = 1065
integer height = 908
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
boolean hscrollbar = true
boolean vscrollbar = true
end type

event doubleclicked;st_file.text = is_percorso_mod +"\"+ lb_lista_doc.selecteditem()
end event

type em_revisione from editmask within w_nuovo_doc
integer x = 1376
integer y = 568
integer width = 293
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
alignment alignment = center!
string mask = "##0"
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "0~~999"
end type

type st_21 from statictext within w_nuovo_doc
integer x = 823
integer y = 568
integer width = 526
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Revisione Iniziale:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_edizione from editmask within w_nuovo_doc
integer x = 526
integer y = 568
integer width = 293
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
alignment alignment = center!
string mask = "##0"
boolean spin = true
string displaydata = "Ä"
double increment = 1
string minmax = "1~~999"
end type

type st_16 from statictext within w_nuovo_doc
integer x = 96
integer y = 568
integer width = 430
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Edizione Iniziale:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_area from dropdownlistbox within w_nuovo_doc
integer x = 526
integer y = 332
integer width = 1408
integer height = 960
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
end type

type st_7 from statictext within w_nuovo_doc
integer x = 46
integer y = 336
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Area: "
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_livello_doc from dropdownlistbox within w_nuovo_doc
integer x = 1371
integer y = 444
integer width = 293
integer height = 356
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
end type

type st_15 from statictext within w_nuovo_doc
integer x = 1042
integer y = 448
integer width = 311
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Livello Doc."
long bordercolor = 79741120
boolean focusrectangle = false
end type

type em_gg_validita from editmask within w_nuovo_doc
integer x = 526
integer y = 448
integer width = 293
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
alignment alignment = center!
string mask = "##0"
boolean spin = true
string displaydata = ""
string minmax = "0~~999"
end type

type st_9 from statictext within w_nuovo_doc
integer x = 46
integer y = 448
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Valido per (giorni): "
alignment alignment = right!
end type

type em_des_documento from editmask within w_nuovo_doc
integer x = 526
integer y = 220
integer width = 1413
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
maskdatatype maskdatatype = stringmask!
string displaydata = ""
end type

event modified;if len(this.text) > 100 then
	g_mb.messagebox("Omnia","La descrizione del documento non può superare i 100 caratteri")
end if
end event

type st_5 from statictext within w_nuovo_doc
integer x = 46
integer y = 220
integer width = 480
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Des. Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_nome_doc from editmask within w_nuovo_doc
integer x = 526
integer y = 104
integer width = 1417
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
maskdatatype maskdatatype = stringmask!
string displaydata = "0"
end type

event modified;if len(this.text) > 100 then
	g_mb.messagebox("Omnia","Il nome del documento non può superare i 100 caratteri")
end if
end event

type st_2 from statictext within w_nuovo_doc
integer x = 46
integer y = 104
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nome Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_3 from groupbox within w_nuovo_doc
integer x = 23
integer width = 1943
integer height = 672
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Dati del Documento"
end type

type gb_1 from groupbox within w_nuovo_doc
integer x = 23
integer y = 692
integer width = 1943
integer height = 400
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Opzioni di creazione"
end type

type gb_firme from groupbox within w_nuovo_doc
integer x = 23
integer y = 1220
integer width = 3017
integer height = 596
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Impostazione manuale firme elettroniche"
end type

