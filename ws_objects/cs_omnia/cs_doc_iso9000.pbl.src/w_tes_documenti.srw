﻿$PBExportHeader$w_tes_documenti.srw
$PBExportComments$Window tes documenti iso 9000
forward
global type w_tes_documenti from w_cs_xx_principale
end type
type rb_tutti from radiobutton within w_tes_documenti
end type
type rb_protocolli from radiobutton within w_tes_documenti
end type
type rb_iso9000 from radiobutton within w_tes_documenti
end type
type cb_1 from cb_documenti_ricerca within w_tes_documenti
end type
type dw_ricerca from uo_dw_search within w_tes_documenti
end type
type st_3 from statictext within w_tes_documenti
end type
type tv_1 from treeview within w_tes_documenti
end type
type r_1 from rectangle within w_tes_documenti
end type
type tab_1 from tab within w_tes_documenti
end type
type tabpage_1 from userobject within tab_1
end type
type ddlb_livello from dropdownlistbox within tabpage_1
end type
type st_2 from statictext within tabpage_1
end type
type st_1 from statictext within tabpage_1
end type
type cb_revisione from commandbutton within tabpage_1
end type
type cbx_blocca from checkbox within tabpage_1
end type
type cb_valida from commandbutton within tabpage_1
end type
type ddlb_visualizza from dropdownlistbox within tabpage_1
end type
type pb_apri_file from picturebutton within tabpage_1
end type
type cb_approva from commandbutton within tabpage_1
end type
type cb_autorizza from commandbutton within tabpage_1
end type
type tabpage_1 from userobject within tab_1
ddlb_livello ddlb_livello
st_2 st_2
st_1 st_1
cb_revisione cb_revisione
cbx_blocca cbx_blocca
cb_valida cb_valida
ddlb_visualizza ddlb_visualizza
pb_apri_file pb_apri_file
cb_approva cb_approva
cb_autorizza cb_autorizza
end type
type tabpage_2 from userobject within tab_1
end type
type cb_paragrafi_iso from commandbutton within tabpage_2
end type
type cb_privilegi from commandbutton within tabpage_2
end type
type tabpage_2 from userobject within tab_1
cb_paragrafi_iso cb_paragrafi_iso
cb_privilegi cb_privilegi
end type
type tabpage_3 from userobject within tab_1
end type
type cb_notifiche_letture from commandbutton within tabpage_3
end type
type cb_elimina_liste_dist from commandbutton within tabpage_3
end type
type cb_liste_dist from commandbutton within tabpage_3
end type
type tabpage_3 from userobject within tab_1
cb_notifiche_letture cb_notifiche_letture
cb_elimina_liste_dist cb_elimina_liste_dist
cb_liste_dist cb_liste_dist
end type
type tab_1 from tab within w_tes_documenti
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
type cb_note from commandbutton within w_tes_documenti
end type
type dw_det_documenti_lista from uo_cs_xx_dw within w_tes_documenti
end type
type ole_1 from olecontrol within w_tes_documenti
end type
type dw_det_documenti_det_2 from uo_cs_xx_dw within w_tes_documenti
end type
type dw_det_documenti_det_1 from uo_cs_xx_dw within w_tes_documenti
end type
type s_chiave_doc from structure within w_tes_documenti
end type
end forward

type s_chiave_doc from structure
	integer		anno_registrazione
	long		num_registrazione
	long		cod_padre
	long		cod_figlio
	string		tipo_elemento
	string		des_elemento
end type

global type w_tes_documenti from w_cs_xx_principale
integer width = 4320
integer height = 2160
string title = "Documenti ISO 9000"
event nuovo_menu ( )
event nuovo_documento ( )
event modifica ( )
event elimina ( )
event ue_controllo_doc_scaduti ( )
rb_tutti rb_tutti
rb_protocolli rb_protocolli
rb_iso9000 rb_iso9000
cb_1 cb_1
dw_ricerca dw_ricerca
st_3 st_3
tv_1 tv_1
r_1 r_1
tab_1 tab_1
cb_note cb_note
dw_det_documenti_lista dw_det_documenti_lista
ole_1 ole_1
dw_det_documenti_det_2 dw_det_documenti_det_2
dw_det_documenti_det_1 dw_det_documenti_det_1
end type
global w_tes_documenti w_tes_documenti

type prototypes
FUNCTION long ShellExecuteA(long hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, long nShowCmd) LIBRARY "SHELL32.DLL" ALIAS FOR "ShellExecuteA;ansi"
end prototypes

type variables
string is_cod_mansionario
string is_visualizza_doc         // N=ultime revisione oppure S=storico
string is_livello_mansionario		// livello di documento a cui ha accesso il mansionario  (A,B,C,%)
string is_livello_doc				// livello di documentazione attualmente selezionato (A,B,C)


// ******************* privilegi dell'utente corrente **************************
string is_lettura_doc, is_modifica_doc, is_elimina_doc, is_autorizzazione, is_approvazione, is_validazione

// aggiunto il parametro per caricareil blob nel file temporaneo
string is_tmp

// 
// is_dir: directory dentro la quale vengono salvati i doc
// is_ultimo_doc: il nome dell'ultimo documento visualizzato.
// is_file = is_dir + is_ultimo_doc
string is_dir, is_ultimo_doc, is_file, is_path_app


long   il_handle // per la ricerca del documento
end variables

forward prototypes
public function integer wf_crea_privilegi (long fl_anno_registrazione_padre, long fl_num_registrazione_padre, long fl_anno_registrazione_figlio, long fl_num_registrazione_figlio)
public function integer wf_modifica ()
public function integer wf_elimina ()
public function integer wf_imposta_tv (long fl_cod_padre, long fl_handle)
public function integer wf_trova_max (ref long fl_max, ref long fl_max_num_registrazione)
public subroutine wf_ricerca_posizione (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_controllo_privilegi (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_fine_modifica (long fl_handle, string fs_label)
public function integer wf_nuovo_menu ()
public function integer wf_inizio ()
public function blob wf_carica_blob (string fs_path)
public function integer wf_updateblob ()
public function integer wf_salvafile ()
public function integer wf_refresh_ole ()
public function integer wf_nuovo_documento ()
public function integer wf_trova (integer fl_anno, integer fl_numero, integer fl_handle)
public function integer wf_da_db_a_ole ()
public function integer wf_controlla_se_approvato (long fl_anno, long fl_num, long fl_prog)
public function integer wf_salvafile_nodelete ()
end prototypes

event nuovo_menu();wf_nuovo_menu()
end event

event nuovo_documento();wf_nuovo_documento()
end event

event modifica();wf_modifica()
end event

event elimina();wf_elimina()
end event

event ue_controllo_doc_scaduti();open(w_controllo_doc_scaduti)
end event

public function integer wf_crea_privilegi (long fl_anno_registrazione_padre, long fl_num_registrazione_padre, long fl_anno_registrazione_figlio, long fl_num_registrazione_figlio);string ls_cod_gruppo_privilegio[],ls_cod_mansionario[]
long ll_t,ll_i


ll_t = 1

declare r_tes_documenti_privilegi cursor for
select cod_gruppo_privilegio,
		 cod_mansionario
from   tes_documenti_privilegi
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fl_anno_registrazione_padre
and    num_registrazione=:fl_num_registrazione_padre;


open r_tes_documenti_privilegi;

do while 1=1
	fetch r_tes_documenti_privilegi
	into  :ls_cod_gruppo_privilegio[ll_t],
			:ls_cod_mansionario[ll_t];
			
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
	if sqlca.sqlcode =100 then exit
	
	ll_t++
loop

close r_tes_documenti_privilegi;


for ll_i = 1 to upperbound(ls_cod_mansionario[]) -1
	insert into tes_documenti_privilegi
	(cod_azienda,
	 anno_registrazione,
	 num_registrazione,
	 cod_gruppo_privilegio,
	 cod_mansionario)
	values
	(:s_cs_xx.cod_azienda,
	 :fl_anno_registrazione_figlio,
	 :fl_num_registrazione_figlio,
	 :ls_cod_gruppo_privilegio[ll_i],
	 :ls_cod_mansionario[ll_i]);
	 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
next

return 0
end function

public function integer wf_modifica ();long ll_handle_corrente

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

tv_1.editlabel(ll_handle_corrente)

return 0
end function

public function integer wf_elimina ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle,ll_num_registrazione
integer li_anno_registrazione
string ls_tipo
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo

if is_elimina_doc = "N" then
	g_mb.messagebox("Omnia","Il mansionario " + is_cod_mansionario + " non è in possesso del privilegio di ELIMINAZIONE.")
	return 0
end if

if g_mb.messagebox("Omnia","Sei Sicuro?",Question!,YesNo!,2) = 2 then return 0

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","E' necessario selezionare almeno un elemento.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.cod_padre
ll_cod_figlio = l_chiave_doc.cod_figlio
li_anno_registrazione = l_chiave_doc.anno_registrazione
ll_num_registrazione = l_chiave_doc.num_registrazione
ls_tipo = l_chiave_doc.tipo_elemento

if ll_cod_padre=0 and ll_cod_figlio = 0 then 
	g_mb.messagebox("Omnia","Non puoi eliminare l'elemento di origine dei documenti di qualità!",stopsign!)
	return 0
end if

select cod_figlio
into   :ll_test_figlio
from   tes_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_padre=:ll_cod_figlio;

if sqlca.sqlcode <0 and (isnull(ll_test_figlio)) then
	g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

if sqlca.sqlcode = 100 then
	
	delete from det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella det_documenti. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from tes_documenti_privilegi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella tes_documenti_privilegi. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from tes_documenti_paragrafi_iso
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella tes_documenti_paragrafi_iso. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from collegamenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore in cancellazione su tabella tes_documenti_paragrafi_iso. " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

	delete from tes_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_padre=:ll_cod_padre
	and    cod_figlio=:ll_cod_figlio;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if
	tv_1.deleteitem(ll_handle_corrente)
		
else
	g_mb.messagebox("Omnia","Non è possibile cancellare l'elemento poichè contiene altri elementi!" + &
				  "~nPADRE = " + string(ll_cod_padre) + "~nFIGLIO = " + string(ll_cod_figlio),stopsign!)
end if

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
tv_1.selectItem(ll_handle_corrente)

commit;
return 0

end function

public function integer wf_imposta_tv (long fl_cod_padre, long fl_handle);string  ls_tipo,ls_des_elemento
long    ll_num_figli,ll_handle,ll_num_righe,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_num_registrazione,ll_num_documenti
integer li_num_priorita,li_risposta,li_anno_registrazione
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo
datastore lds_righe_tree_doc

ll_num_figli = 1

lds_righe_tree_doc = Create DataStore

//Donato 18-09-2008 il tv è popolato in base allo stato di un radiobutton
if rb_iso9000.checked then
	lds_righe_tree_doc.DataObject = "d_tes_documenti_iso9000"
elseif rb_protocolli.checked then
	lds_righe_tree_doc.DataObject = "d_tes_documenti_protocolli"
else
	//tutti
	lds_righe_tree_doc.DataObject = "d_tes_documenti"
end if

//lds_righe_tree_doc.DataObject = "d_tes_documenti"
//fine modifica -------------------------------------------------------

lds_righe_tree_doc.SetTransObject(sqlca)

ll_num_righe = lds_righe_tree_doc.Retrieve(s_cs_xx.cod_azienda,fl_cod_padre)

for ll_num_figli=1 to ll_num_righe
		
	l_chiave_doc.cod_padre = fl_cod_padre
	
	l_chiave_doc.cod_figlio = lds_righe_tree_doc.getitemnumber(ll_num_figli,"cod_figlio")
	
	l_chiave_doc.tipo_elemento = lds_righe_tree_doc.getitemstring(ll_num_figli,"tipo_elemento")
	
	l_chiave_doc.anno_registrazione = lds_righe_tree_doc.getitemnumber(ll_num_figli,"anno_registrazione")
	
	l_chiave_doc.num_registrazione= lds_righe_tree_doc.getitemnumber(ll_num_figli,"num_registrazione")
	
	l_chiave_doc.des_elemento = lds_righe_tree_doc.getitemstring(ll_num_figli,"des_elemento")
	
	// modifica Michela : devo fare in modo che ignori gli elementi di tipo "X".
	// spiego meglio: all'inizio non c'era la gestione dei processi. ogni processo aveva delle fasi ed ogni fase poteva
	// essere costituita da delle liste di distribuzione o da documenti. non è stata creata una tabella dei documenti, bensi
	// è stata creata una testata con tipo_elemento = X per indicare le testate dei processi.
	if l_chiave_doc.tipo_elemento = "X" then
		
		continue
		
	end if
	// fine modifica
	
	if l_chiave_doc.tipo_elemento = "D" then
		
		ll_num_documenti = 0
		
		select count(*)
		into   :ll_num_documenti
		from   det_documenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :l_chiave_doc.anno_registrazione and
				 num_registrazione = :l_chiave_doc.num_registrazione;
				 
		if ll_num_documenti > 0	and not isnull(ll_num_documenti) then
			
			ll_num_documenti = 0
			
			select count(*)
			into   :ll_num_documenti
			from   det_documenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :l_chiave_doc.anno_registrazione and
					 num_registrazione = :l_chiave_doc.num_registrazione and
					 livello_documento >= :is_livello_doc and
					 flag_storico = :is_visualizza_doc;
					 
			if ll_num_documenti = 0 or isnull(ll_num_documenti) then
				continue
			end if	
			
		end if
		
	end if

	if isnull(ls_des_elemento) then ls_des_elemento = ""
	
	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = l_chiave_doc
	tvi_campo.label =l_chiave_doc.des_elemento

	select cod_figlio 
	into   :ll_test_figlio
	from   tes_documenti
	where  cod_azienda =:s_cs_xx.cod_azienda
	and    cod_padre=:l_chiave_doc.cod_figlio;

	if sqlca.sqlcode = 100  then
		if l_chiave_doc.tipo_elemento = 'M' then
			tvi_campo.pictureindex = 3
			tvi_campo.selectedpictureindex = 2
			tvi_campo.overlaypictureindex = 2
			ll_handle=tv_1.insertitemlast(fl_handle, tvi_campo)		
		else
			tvi_campo.pictureindex = 4
			tvi_campo.selectedpictureindex = 4
			tvi_campo.overlaypictureindex = 4
			ll_handle=tv_1.insertitemlast(fl_handle, tvi_campo)
		end if
		continue
	else
		tvi_campo.pictureindex = 3
		tvi_campo.selectedpictureindex = 2
		tvi_campo.overlaypictureindex = 2
		ll_handle=tv_1.insertitemlast(fl_handle, tvi_campo)
		setnull(ll_test_figlio)
		li_risposta=wf_imposta_tv(l_chiave_doc.cod_figlio,ll_handle)
	end if

next

destroy(lds_righe_tree_doc)

return 0
end function

public function integer wf_trova_max (ref long fl_max, ref long fl_max_num_registrazione);long ll_max_padre,ll_max_figlio,ll_max_num_registrazione
integer li_anno_registrazione

li_anno_registrazione = f_anno_esercizio()

select max(cod_padre)
into   :ll_max_padre
from   tes_documenti
where  cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ll_max_padre) then ll_max_padre = 0

select max(cod_figlio)
into   :ll_max_figlio
from   tes_documenti
where  cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ll_max_figlio) then ll_max_figlio = 0

if ll_max_padre >= ll_max_figlio then
	fl_max=ll_max_padre
else
	fl_max=ll_max_figlio
end if

if fl_max=0 then 
	fl_max=1
else
	fl_max++
end if

select max(num_registrazione)
into   :ll_max_num_registrazione
from   tes_documenti
where  cod_azienda =:s_cs_xx.cod_azienda
and    anno_registrazione =:li_anno_registrazione;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ll_max_num_registrazione) or ll_max_num_registrazione = 0 then 
	ll_max_num_registrazione=1
else
	ll_max_num_registrazione++
end if

fl_max_num_registrazione = ll_max_num_registrazione

commit;

return 0
end function

public subroutine wf_ricerca_posizione (long fl_anno_registrazione, long fl_num_registrazione);long ll_i, ll_handle
string ls_des_identificativa
s_chiave_doc lstr_record, lstr_record_2
treeviewitem tvl_campo

lstr_record.anno_registrazione = fl_anno_registrazione
lstr_record.num_registrazione = fl_num_registrazione

ll_i = 1
ll_handle = tv_1.getitem(ll_i, tvl_campo)
lstr_record_2 = tvl_campo.data
do while	lstr_record_2.anno_registrazione <> lstr_record.anno_registrazione or &
			lstr_record_2.num_registrazione <> lstr_record.num_registrazione 
		ll_i ++
		ll_handle = tv_1.getitem(ll_i, tvl_campo)
		if ll_handle = -1 then exit
		lstr_record_2 = tvl_campo.data			

loop

if ll_handle <> -1 then

	tv_1.SelectItem (ll_i)
	
	//il_handle = ll_i
	dw_det_documenti_lista.Change_DW_Current( )
	w_tes_documenti.triggerevent("pc_retrieve")	
	
	tv_1.setfocus()
else 
	g_mb.messagebox("Ricerca Posizione","Elemento non trovato")
end if	
end subroutine

public function integer wf_controllo_privilegi (long fl_anno_registrazione, long fl_num_registrazione);long ll_ret
uo_documenti_iso9000 luo_documenti_iso9000

luo_documenti_iso9000 = CREATE uo_documenti_iso9000
ll_ret = luo_documenti_iso9000.uof_privilegi_documento(fl_anno_registrazione, fl_num_registrazione)
choose case ll_ret
	case is < 0 
	g_mb.messagebox("OMNIA", luo_documenti_iso9000.is_errore)
	destroy luo_documenti_iso9000
	return -1
case else
	is_modifica_doc = luo_documenti_iso9000.is_modifica
	is_lettura_doc = luo_documenti_iso9000.is_lettura
	is_elimina_doc = luo_documenti_iso9000.is_elimina
	is_autorizzazione = luo_documenti_iso9000.is_autorizzazione
	is_approvazione = luo_documenti_iso9000.is_approvazione
	is_validazione = luo_documenti_iso9000.is_validazione
	destroy luo_documenti_iso9000
	
	if is_autorizzazione = "N" then 
		tab_1.tabpage_1.cb_autorizza.enabled = false
	else
		tab_1.tabpage_1.cb_autorizza.enabled = true
	end if
	if is_approvazione = "N" then 
		tab_1.tabpage_1.cb_approva.enabled = false
	else
		tab_1.tabpage_1.cb_approva.enabled = true
	end if
	
	if is_validazione = "N" then 
		tab_1.tabpage_1.cb_valida.enabled = false
	else
		tab_1.tabpage_1.cb_valida.enabled = true
	end if
	
	if is_modifica_doc = "N" then 
		tab_1.tabpage_1.cb_revisione.enabled = false
		tab_1.tabpage_1.pb_apri_file.enabled = false
		tab_1.tabpage_2.cb_paragrafi_iso.enabled = false
	else
		tab_1.tabpage_1.cb_revisione.enabled = true
		tab_1.tabpage_1.pb_apri_file.enabled = true
		tab_1.tabpage_2.cb_paragrafi_iso.enabled = true
	end if
	if is_lettura_doc = "N" then
		tab_1.tabpage_1.cbx_blocca.checked = true
		tab_1.tabpage_1.cbx_blocca.enabled = false
	end if
end choose
return 0
end function

public function integer wf_fine_modifica (long fl_handle, string fs_label);long ll_handle_corrente,ll_cod_padre,ll_cod_figlio,ll_num_registrazione
string ls_tipo,ls_descrizione
integer li_anno_registrazione
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo


tv_1.GetItem (fl_handle, tvi_campo )

l_chiave_doc = tvi_campo.data
ls_descrizione= fs_label

ll_cod_padre = l_chiave_doc.cod_padre
ll_cod_figlio = l_chiave_doc.cod_figlio
ls_tipo = l_chiave_doc.tipo_elemento
l_chiave_doc.des_elemento = ls_descrizione

update tes_documenti
set    des_elemento=:ls_descrizione
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 cod_padre=:ll_cod_padre
and    cod_figlio=:ll_cod_figlio;

if sqlca.sqlcode <0 then
	g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

commit;

return 0
end function

public function integer wf_nuovo_menu ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_max,ll_ultimo_handle, & 
	  ll_num_registrazione,ll_max_num_registrazione,ll_anno_registrazione_padre,ll_num_registrazione_padre
integer li_anno_registrazione
string ls_tipo
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo

li_anno_registrazione = f_anno_esercizio()

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","Attenzione! E' necessario selezionare almeno un elemento.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.cod_padre
ll_cod_figlio = l_chiave_doc.cod_figlio
ls_tipo = l_chiave_doc.tipo_elemento
ll_anno_registrazione_padre = l_chiave_doc.anno_registrazione
ll_num_registrazione_padre = l_chiave_doc.num_registrazione

if ls_tipo = 'D' then
	g_mb.messagebox("Omnia","Attenzione! I documenti non possono contenere al loro interno dei menù!",stopsign!)
	return 0
end if

l_chiave_doc.cod_padre=ll_cod_figlio

ll_risposta=wf_trova_max(ll_max,ll_max_num_registrazione)

l_chiave_doc.cod_figlio = ll_max
l_chiave_doc.tipo_elemento = 'M'
l_chiave_doc.anno_registrazione=li_anno_registrazione
l_chiave_doc.num_registrazione=ll_max_num_registrazione

insert into tes_documenti
	(cod_azienda,
	 anno_registrazione,
	 num_registrazione,
	 cod_padre,
	 cod_figlio,
	 tipo_elemento,
	 flag_blocco,
	 des_elemento)
values
	(:s_cs_xx.cod_azienda,
	 :l_chiave_doc.anno_registrazione,
	 :l_chiave_doc.num_registrazione,
	 :l_chiave_doc.cod_padre,
	 :l_chiave_doc.cod_figlio,
	 'M',
	 'N',
	 '');

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return 0
end if

tvi_campo.itemhandle = ll_handle_corrente 
tvi_campo.data = l_chiave_doc
tvi_campo.label = ""
tvi_campo.pictureindex = 2
tvi_campo.selectedpictureindex = 2
tvi_campo.overlaypictureindex = 2
ll_ultimo_handle=tv_1.InsertItemLast ( ll_handle_corrente, tvi_campo )
tv_1.selectItem(ll_ultimo_handle)
tv_1.EditLabel (ll_ultimo_handle )

wf_crea_privilegi(ll_anno_registrazione_padre, &
						ll_num_registrazione_padre, &
						l_chiave_doc.anno_registrazione, &
						l_chiave_doc.num_registrazione) 

commit;

return 0
end function

public function integer wf_inizio ();long         ll_handle

integer      li_risposta

treeviewitem tvi_campo

s_chiave_doc l_chiave_doc

st_3.text = "Mansionario: " + is_cod_mansionario + "   Accesso max dal livello: " + is_livello_mansionario

tv_1.setredraw(false)

tv_1.deleteitem(0)

l_chiave_doc.cod_padre=0

l_chiave_doc.cod_figlio=0

tvi_campo.itemhandle = 1

tvi_campo.label = "Documenti di Qualità"

tvi_campo.pictureindex = 1

tvi_campo.selectedpictureindex = 1

tvi_campo.overlaypictureindex = 1

tvi_campo.data = l_chiave_doc

ll_handle = tv_1.insertitemlast(0, tvi_campo)

li_risposta = wf_imposta_tv(0,ll_handle)

//tv_1.expandall(1)

tv_1.setredraw(true)

commit;

return 0
end function

public function blob wf_carica_blob (string fs_path);long ll_filelen, ll_bytes_read, ll_new_pos
integer li_counter, li_loops, fh1

blob lb_tempblob, lb_bufferblob, lb_resultblob, lb_blankblob
		
// Trova la lunghezza del file
ll_filelen = FileLength(fs_path)

// FileRead legge 32KB alla volta => Se il file supera i 32 KB, SERVONO PIU' CICLI
// Calcola il numero di cicli
IF ll_filelen > 32765 THEN 
	IF Mod(ll_filelen,32765) = 0 THEN 
		li_loops = ll_filelen/32765 
	ELSE 
		// ...se c'è il resto, serve un ciclo in più
		li_loops = (ll_filelen/32765) + 1 
	END IF 
ELSE 
	li_loops = 1 
END IF

//carico il blob
fh1 = FileOpen(fs_path, StreamMode!)
if fh1 <> -1 then
	ll_new_pos = 0
	// Esegue li_loops cicli
	FOR li_counter = 1 to li_loops 
		// Legge i dati in una variabile TEMPORANEA, di tipo BLOB
		ll_bytes_read = FileRead(fh1, lb_tempblob) 
		// ...e li accoda in un secondo BLOB
		lb_bufferblob = lb_bufferblob + lb_tempblob 
		ll_new_pos = ll_new_pos + ll_bytes_read 
		// Sposta il puntatore al file nella posizione di lettura successiva
		FileSeek(fh1,ll_new_pos,FROMBEGINNING!)
		// Accoda i segmenti alla cache fino a costituire una "tranche" di circa 1 mega
		if len(lb_bufferblob) > 1000000 then
			// Se la dimensione limite è superata, accoda i dati in un terzo BLOB "risultato"
			lb_resultblob = lb_resultblob + lb_bufferblob 
			// ...e "sega" il BLOB temporaneo 2 assegnandoli un blob nullo
			lb_resultblob = lb_blankblob 
		end if 
	NEXT
	// Accoda i dati restanti in total_blob
	lb_resultblob = lb_resultblob + lb_bufferblob 
	// Fine lettura
	FileClose(fh1)
	
end if

return lb_resultblob


end function

public function integer wf_updateblob ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle, & 
	  ll_num_registrazione,ll_progressivo
string ls_tipo, ls_db
blob lbb_blob_documento
integer li_risposta,li_anno_registrazione
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo
transaction sqlcb

if is_modifica_doc = "S" then
	ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
	
	if ll_handle_corrente=-1 then
		g_mb.messagebox("Omnia","Selezionare almeno un elemento dell'albero.",stopsign!)
		return 0
	end if
	
	tv_1.GetItem (ll_handle_corrente, tvi_campo )
	
	l_chiave_doc = tvi_campo.data
	
	ll_cod_padre = l_chiave_doc.cod_padre
	ll_cod_figlio = l_chiave_doc.cod_figlio
	li_anno_registrazione=l_chiave_doc.anno_registrazione
	ll_num_registrazione=l_chiave_doc.num_registrazione
	ls_tipo = l_chiave_doc.tipo_elemento
	ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")
	
	if ll_cod_padre=0 and ll_cod_figlio = 0 then 
		g_mb.messagebox("Omnia","Non puoi inserire documenti sull'elemento di origine dei documenti di qualità!",stopsign!)
		return 0
	end if
	
	if ls_tipo = "M" then
		g_mb.messagebox("Omnia","Non puoi associare oggetti ad un menù ma solo ai documenti!",stopsign!)
		return 0
	end if

// modifiche michela 23/05/2003	
	dec     ld_prog_mimetype, ld_num
	string  ls_p, ls_estensione, ls_nome_doc
	integer li_FileNum
	long    ll_pos

	select prog_mimetype, nome_documento
	into   :ld_prog_mimetype, :ls_nome_doc
	from   det_documenti
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    anno_registrazione = :li_anno_registrazione
	and    num_registrazione = :ll_num_registrazione
	and    progressivo = :ll_progressivo;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("","Impossibile risalire al tipo di documento!")
		return -1
	end if
	
	select estensione
	into   :ls_estensione
	from   tab_mimetype
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    prog_mimetype = :ld_prog_mimetype;
	
	if sqlca.sqlcode <> 0 or ls_estensione = "" then
		g_mb.messagebox("OMNIA", "Impossibile aggiornare il documento!")
		return -1
	end if
	
	if not isnull(ls_nome_doc) and ls_nome_doc <> "" then
		ls_p = is_tmp + ls_nome_doc + "." + ls_estensione
	else
		ls_p = is_tmp + "cs." + ls_estensione
	end if
	
	li_FileNum = FileOpen(ls_p,StreamMode!,Write!,Shared!,Replace!)
	if li_FileNum = -1 then
		g_mb.messagebox("OMNIA","Errore nell'apertura del file: " + ls_p)
		return -1
	end if	
	
	long ll_len
	ll_len = len(ole_1.objectdata)
	
	ll_pos = 1
	Do While FileWrite(li_FileNum,BlobMid(ole_1.objectdata,ll_pos,32765)) > 0
		ll_pos += 32765
	Loop

	
	FileClose(li_FileNum)

	lbb_blob_documento = wf_carica_blob(ls_p)


	FileDelete(ls_p)
	
	//lbb_blob_documento = ole_1.objectdata   da decommentare !!
	
	
// 11-07-2002 modifiche Michela: controllo l'enginetype	

	ls_db = f_db()
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
		updateblob det_documenti
		set    blob_documento=:lbb_blob_documento
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    progressivo = :ll_progressivo
		using  sqlcb;
	
		if sqlcb.sqlcode <0 then
			g_mb.messagebox("Omnia","Errore sul db: " + sqlcb.sqlerrtext,stopsign!)
			destroy sqlcb;
			rollback;
			return 0
		end if		
		
		destroy sqlcb;
		
	else
	
		updateblob det_documenti
		set    blob_documento=:lbb_blob_documento
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    progressivo = :ll_progressivo;
	
		if sqlca.sqlcode <0 then
			g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 0
		end if
		
	end if
	
// fine modifiche	
	
	ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
	li_risposta = tv_1.selectItem(ll_handle_corrente)
	tv_1.setfocus()
	
	commit;
end if
return 0
end function

public function integer wf_salvafile ();// questa funzione carica il blob con il contenuto dell'ultimo documento aperto
// solo se l'utente ha il permesso di modificarlo.

blob        lbb_blob_documento
long        ll_anno, ll_pos, ll_num, ll_pos2, ll_prog, ll_length, ll_ris
string      ls_db, ls_flag_BMD, ls_autorizzato_da
integer     li_risposta
transaction sqlcb
integer li_ret

ll_anno = long(Left(is_ultimo_doc,4))
ll_pos = pos(is_ultimo_doc,"_")
ll_num = long(mid(is_ultimo_doc,5,ll_pos -5))
ll_pos2 = pos(is_ultimo_doc,".")
ll_prog = long(mid(is_ultimo_doc,ll_pos + 1,ll_pos2 - ll_pos -1))

li_ret = wf_controlla_se_approvato(ll_anno, ll_num, ll_prog)

if is_modifica_doc = "S"  and is_file <> "" and not isnull(is_file) and li_ret = 0 then
	
	setnull(lbb_blob_documento)
	lbb_blob_documento = wf_carica_blob( is_file)

	ll_anno = long(Left(is_ultimo_doc,4))
	ll_pos = pos(is_ultimo_doc,"_")
	ll_num = long(mid(is_ultimo_doc,5,ll_pos -5))
	ll_pos2 = pos(is_ultimo_doc,".")
	ll_prog = long(mid(is_ultimo_doc,ll_pos + 1,ll_pos2 - ll_pos -1))

	ls_db = f_db()	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)		
		updateblob det_documenti
		set        blob_documento = :lbb_blob_documento
		where      cod_azienda = :s_cs_xx.cod_azienda
		and        anno_registrazione = :ll_anno
		and        num_registrazione = :ll_num
		and        progressivo = :ll_prog
		using      sqlcb;
	
		if sqlcb.sqlcode = 100 then
			g_mb.messagebox("OMNIA","Attenzione: il riferimento al documento non è valido!",stopsign!)
			destroy sqlcb;			
			rollback;
			return -1
		elseif sqlcb.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nel salvataggio del documento  " + is_file + " :  " + sqlcb.sqlerrtext,stopsign!)
			destroy sqlcb;			
			rollback;
			return -1
		end if				
		destroy sqlcb;
		
	else
	
		updateblob det_documenti
		set        blob_documento = :lbb_blob_documento
		where      cod_azienda = :s_cs_xx.cod_azienda
		and        anno_registrazione = :ll_anno
		and        num_registrazione = :ll_num
		and        progressivo = :ll_prog;
	
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("OMNIA","Attenzione: il riferimento al documento non è valido!",stopsign!)
			rollback;
			return -1
		elseif sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nel salvataggio del documento  " + is_file + " :  " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if		
			
	end if

	if filedelete(is_file) <> true then
		g_mb.messagebox("OMNIA","Impossibile cancellare il file " + is_file)	
	end if
	commit;
	return 0
else
	if li_ret = 1 then
		if filedelete(is_file) <> true then
			g_mb.messagebox("OMNIA","Impossibile cancellare il file " + is_file)	
		end if
	end if
	return 100
end if
end function

public function integer wf_refresh_ole ();long        ll_num_registrazione,ll_progressivo,ll_righe, li_FileNum, li_r, li_i, li_ris, ll_len, ll_pos
integer     li_risposta,li_anno_registrazione
blob        lbb_blob_documento, lbb_blob
string      ls_db, ls_p, ls_estensione, ls_nome_doc, ls_tipo_doc
transaction sqlcb
dec         ld_prog, ld_num

if tab_1.tabpage_1.cbx_blocca.checked = true then return 0

ll_righe = dw_det_documenti_lista.rowcount()

if ll_righe = 0 then return 0

li_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

ls_db = f_db()
if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob_documento
	into   :lbb_blob_documento
	from   det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    progressivo = :ll_progressivo
	using	 sqlcb;

	if sqlcb.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		destroy sqlcb;
		return 0
	end if	
	
	destroy sqlcb;
	
else

	selectblob blob_documento
	into   :lbb_blob_documento
	from   det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    progressivo = :ll_progressivo;

	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
end if

// trovo il tipo di elemento
select tipo_elemento
into   :ls_tipo_doc
from   tes_documenti
where  cod_azienda = :s_cs_xx.cod_azienda
and    anno_registrazione = :li_anno_registrazione
and    num_registrazione = :ll_num_registrazione;
		
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore nella ricerca del tipo di elemento!")
	return -1
elseif sqlca.sqlcode = 100 then
	return -1
elseif sqlca.sqlcode = 0 then
	if ls_tipo_doc <> "D" then
		return -1
	end if
end if

// se è un documento ma il blob è vuoto allora esco direttamente
ll_len = len(lbb_blob_documento)
if len(lbb_blob_documento) = 0 or isnull(len(lbb_blob_documento)) then
	return 0
end if
		
// trovo di che tipo è il documento, quindi l'estensione		
select prog_mimetype
into   :ld_prog
from   det_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    progressivo = :ll_progressivo;

if sqlca.sqlcode <> 0 or isnull(ll_progressivo) then
	g_mb.messagebox("OMNIA","Impossibile risalire al tipo di documento, quindi non verrà visualizzato l'anteprima!")
	return -1
end if

select estensione
into   :ls_estensione
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda
and    prog_mimetype = :ld_prog;
		
if sqlca.sqlcode <> 0 or ls_estensione = "" then
	return -1
end if
		
// il nome del documento ha il seguente formato: aaaann..n_pppp.estensione			
ls_nome_doc = string(li_anno_registrazione) 				
ls_nome_doc = ls_nome_doc + string(ll_num_registrazione) 
ls_nome_doc = ls_nome_doc + "_" + string(ll_progressivo) 
ls_nome_doc = ls_nome_doc + "." + ls_estensione

// associo la directory col file
//ls_p = is_dir + "\" + ls_nome_doc
ls_p = is_dir + ls_nome_doc

// Salvo su db il documento aperto prima
li_ris = wf_salvafile()

li_FileNum = FileOpen(ls_p,StreamMode!,Write!,Shared!,Replace!)
if li_FileNum = -1 then
	g_mb.messagebox("OMNIA","Attenzione: errore durante l'apertura del file " + ls_p)
	rollback;
	return -1
end if

ll_pos = 1
Do While FileWrite(li_FileNum,BlobMid(lbb_blob_documento,ll_pos,32765)) > 0
	ll_pos += 32765
Loop
FileClose(li_FileNum)

// ora salvo l'originale
is_file = ls_p
is_ultimo_doc = ls_nome_doc

ole_1.insertfile(ls_p)
//ole_1.objectdata = lbb_blob_documento   // da decommentare

commit;
return 0
end function

public function integer wf_nuovo_documento ();long     ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_max,ll_ultimo_handle, & 
		   ll_num_registrazione,ll_max_num_registrazione,ll_num_versione,ll_num_revisione,ll_max_progressivo, &
			ll_valido_per, ll_anno_registrazione_padre,ll_num_registrazione_padre, ll_count
			
string   ls_tipo,ls_nome_documento,ls_des_documento,ls_cod_area_aziendale,ls_file,ls_livello_documento, &
		   ls_emesso, ls_autorizzato, ls_approvato, ls_validato, ls_db, ls_reverse, ls_tipof, ls_firma_emesso_da
			
integer  li_anno_registrazione,li_risposta, li_r, li_pos

datetime ldt_oggi, ldt_emesso, ldt_autorizzato, ldt_approvato, ldt_validato

blob     lbb_blob_documento

dec {4} ld_prog_mimetype

s_chiave_doc l_chiave_doc

treeviewitem tvi_campo

transaction sqlcb


if is_modifica_doc = "N" then
	g_mb.messagebox("Omnia","Il mansionario " + is_cod_mansionario + " non è in possesso del privilegio di modifica / nuovo sulla documentazione")
	return 0
end if

ldt_oggi= datetime(today(),00:00:00)
ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","Attenzione! E' necessario selezionare almeno un elemento.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.cod_padre
ll_cod_figlio = l_chiave_doc.cod_figlio
ls_tipo = l_chiave_doc.tipo_elemento
ll_anno_registrazione_padre = l_chiave_doc.anno_registrazione
ll_num_registrazione_padre = l_chiave_doc.num_registrazione

if ls_tipo = 'D' then
	g_mb.messagebox("Omnia","Attenzione! I documenti non possono contenere al loro interno altri documenti!",stopsign!)
	return 0
end if

window_open(w_nuovo_doc,0)

// *************** Modifica Michele per specifica Sempl_doc_iso9000  14/11/2001 ****************

choose case s_cs_xx.parametri.parametro_i_1
	case -1
		return -1
	case 100
		ls_emesso = s_cs_xx.parametri.parametro_s_6
		ldt_emesso = s_cs_xx.parametri.parametro_data_1
		ls_autorizzato = s_cs_xx.parametri.parametro_s_7
		ldt_autorizzato = s_cs_xx.parametri.parametro_data_2
		ls_approvato = s_cs_xx.parametri.parametro_s_8
		ldt_approvato = s_cs_xx.parametri.parametro_data_3
		ls_validato = s_cs_xx.parametri.parametro_s_9
		ldt_validato = s_cs_xx.parametri.parametro_data_4
	case 0
		ls_emesso = is_cod_mansionario
		ldt_emesso = ldt_oggi
		setnull(ls_autorizzato)
		setnull(ldt_autorizzato)
		setnull(ls_approvato)
		setnull(ldt_approvato)
		setnull(ls_validato)
		setnull(ldt_validato)
end choose

// *************** Fine modifica ****************

ls_nome_documento = s_cs_xx.parametri.parametro_s_1
ls_des_documento = s_cs_xx.parametri.parametro_s_2
ls_cod_area_aziendale = s_cs_xx.parametri.parametro_s_3 
ls_file = s_cs_xx.parametri.parametro_s_4 
ls_livello_documento = s_cs_xx.parametri.parametro_s_5
ll_num_versione = s_cs_xx.parametri.parametro_ul_1  // =long(em_edizione.text)
ll_num_revisione = s_cs_xx.parametri.parametro_ul_2 // =long(em_revisione.text)
ll_valido_per = s_cs_xx.parametri.parametro_ul_3

li_anno_registrazione = f_anno_esercizio()

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","E' necessario selezionare almeno un elemento.",stopsign!)
	return 0
end if

l_chiave_doc.cod_padre=ll_cod_figlio
ll_risposta=wf_trova_max(ll_max,ll_max_num_registrazione)
l_chiave_doc.cod_figlio = ll_max
l_chiave_doc.tipo_elemento = "D"
l_chiave_doc.anno_registrazione=li_anno_registrazione
l_chiave_doc.num_registrazione=ll_max_num_registrazione




insert into tes_documenti
	(cod_azienda,
	 anno_registrazione,
	 num_registrazione,
	 cod_padre,
	 cod_figlio,
	 tipo_elemento,
	 flag_blocco,
	 des_elemento)
values
	(:s_cs_xx.cod_azienda,
	 :l_chiave_doc.anno_registrazione,
	 :l_chiave_doc.num_registrazione,
	 :l_chiave_doc.cod_padre,
	 :l_chiave_doc.cod_figlio,
	 'D',
	 'N',
	 :ls_nome_documento);

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

select max(progressivo)
into   :ll_max_progressivo
from   det_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:l_chiave_doc.anno_registrazione
and  	 num_registrazione=:l_chiave_doc.num_registrazione;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Omnia","Si è verificato un errore durante la ricerca del max di progressivo su det_documenti; dettaglio:" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

if ll_max_progressivo = 0 or isnull(ll_max_progressivo) then
	ll_max_progressivo = 1
else
	ll_max_progressivo++
end if

// modifiche Michela: trovo il mimetype del documento

select count(*)
into   :li_pos
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Si è verificato un errore durante la lettura della tabella dei mimetype; dettaglio:" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia","Attenzione! Nessun mimetype definito per questa azienda!",stopsign!)
	rollback;
	return 0
end if

li_pos = 0
ls_reverse = reverse(ls_file)

li_pos = pos(ls_reverse,".")
if li_pos > 0 then
	ls_tipof = right(ls_file,(li_pos - 1))
	ls_tipof = upper(ls_tipof)
else
	setnull(ls_tipof)
	g_mb.messagebox("Omnia","Attenzione, il tipo di documento non è codificato! Impossibile effettuare l'inserimento.",stopsign!)
	rollback;
	return 0
end if

if not isnull(ls_tipof) then
	
	select prog_mimetype
	into   :ld_prog_mimetype
	from   tab_mimetype
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    estensione = :ls_tipof;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Omnia","Si è verificato un errore durante la ricerca del mimetype; dettaglio:" + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if	
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("Omnia","Attenzione, il tipo di documento non è codificato! Impossibile effettuare l'inserimento.",stopsign!)
		rollback;
		return 0
	end if
else
	setnull(ld_prog_mimetype)
	g_mb.messagebox("Omnia","Attenzione, il tipo di documento non è codificato! Impossibile effettuare l'inserimento.",stopsign!)
	rollback;
	return 0
end if

// fine modifiche

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_firma_emesso_da = luo_mansionario.uof_return_firma(ls_emesso)

insert into det_documenti
		 (cod_azienda,
		  anno_registrazione,
		  num_registrazione,
		  progressivo,
		  resp_conservazione_doc,
		  num_versione,
		  num_revisione,
		  emesso_da,
		  emesso_il,
		  firma_emesso_da,
		  autorizzato_da,
		  autorizzato_il,
		  approvato_da,
		  approvato_il,
		  validato_da,
		  validato_il,
		  valido_per,
		  modifiche,
		  livello_documento,
		  flag_uso,
		  flag_recuperato,
		  nome_documento,
		  des_documento,
		  prog_ordinamento_livello,
		  periodo_conserv_unita_tempo,
		  periodo_conserv,
		  flag_doc_registraz_qualita,
		  flag_catalogazione,
		  cod_area_aziendale,
		  flag_storico,
		  prog_mimetype)
values(:s_cs_xx.cod_azienda,
		 :l_chiave_doc.anno_registrazione,
 	    :l_chiave_doc.num_registrazione,
		 :ll_max_progressivo,
		 null,
		 :ll_num_versione,
		 :ll_num_revisione,
		 :ls_emesso,
		 :ldt_emesso,
		 :ls_firma_emesso_da,
		 :ls_autorizzato,
		 :ldt_autorizzato,
		 :ls_approvato,
		 :ldt_approvato,
		 :ls_validato,
		 :ldt_validato,
		 :ll_valido_per,
		 null,
		 :ls_livello_documento,
		 'G',
		 'N',
		 :ls_nome_documento,
		 :ls_des_documento,
		 null,
		 'G',
		 0,
		 'N',
		 'A',
		 :ls_cod_area_aziendale,
		 'N',
		 :ld_prog_mimetype);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia","Si è verificato un errore durante l'insert in det_documenti; dettaglio:" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if
		  
tvi_campo.itemhandle = ll_handle_corrente 
tvi_campo.data = l_chiave_doc
tvi_campo.label = ls_nome_documento
tvi_campo.pictureindex = 4
tvi_campo.selectedpictureindex = 4
tvi_campo.overlaypictureindex = 4
ll_ultimo_handle=tv_1.InsertItemLast ( ll_handle_corrente, tvi_campo )
tv_1.selectItem(ll_ultimo_handle)
tv_1.EditLabel (ll_ultimo_handle )


// carico il file nell'olecontrol e il blob
li_risposta = ole_1.InsertFile(ls_file)    
lbb_blob_documento = wf_carica_blob(ls_file)


//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************


string ls_nome_doc, ls_p
integer li_ris, li_filenum
long    ll_pos
ls_nome_doc = string(l_chiave_doc.anno_registrazione) 				
ls_nome_doc = ls_nome_doc + string(l_chiave_doc.num_registrazione) 
ls_nome_doc = ls_nome_doc + "_" + string(ll_max_progressivo) 
ls_nome_doc = ls_nome_doc + "." + ls_tipof

ls_p = is_dir + ls_nome_doc

// Salvo su db il documento aperto prima
li_ris = wf_salvafile()

li_FileNum = FileOpen(ls_p,StreamMode!,Write!,Shared!,Replace!)
if li_FileNum = -1 then
	g_mb.messagebox("OMNIA","Attenzione: errore durante l'apertura del file " + ls_p)
	rollback;
	return -1
end if
ll_pos = 1
Do While FileWrite(li_FileNum,BlobMid(lbb_blob_documento,ll_pos,32765)) > 0
	ll_pos += 32765
Loop
FileClose(li_FileNum)

// ora salvo l'originale
is_file = ls_p
is_ultimo_doc = ls_nome_doc

//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************

ls_db = f_db()
if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	updateblob det_documenti
	set        blob_documento=:lbb_blob_documento
	where      cod_azienda=:s_cs_xx.cod_azienda
	and 		  anno_registrazione=:l_chiave_doc.anno_registrazione
	and  	     num_registrazione =:l_chiave_doc.num_registrazione
	and  	     progressivo=:ll_max_progressivo
	using      sqlcb;
	
	if sqlcb.sqlcode <0 then
		g_mb.messagebox("Omnia","Il salvataggio dell'oggetto nel documento ha provocato il seguente errore: " + sqlcb.sqlerrtext,stopsign!)
		rollback;
		destroy sqlcb;
		return 0
	end if	
	
	destroy sqlcb;
	
else
	
	updateblob det_documenti
	set        blob_documento=:lbb_blob_documento
	where      cod_azienda=:s_cs_xx.cod_azienda
	and 		  anno_registrazione=:l_chiave_doc.anno_registrazione
	and  	     num_registrazione =:l_chiave_doc.num_registrazione
	and  	     progressivo=:ll_max_progressivo;
	
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Il salvataggio dell'oggetto nel documento ha provocato il seguente errore: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

end if

// fine modifiche

wf_crea_privilegi(ll_anno_registrazione_padre, &
						ll_num_registrazione_padre, &
						l_chiave_doc.anno_registrazione, &
						l_chiave_doc.num_registrazione) 

commit;

if s_cs_xx.num_livello_mail > 0 then
	
	select count(*)
	into   :ll_count
	from   tes_documenti_privilegi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :l_chiave_doc.anno_registrazione and
			 num_registrazione = :l_chiave_doc.num_registrazione and
			 cod_gruppo_privilegio in (select cod_gruppo_privilegio
			 									from tab_gruppi_privilegi
												where cod_azienda = :s_cs_xx.cod_azienda and
														flag_verifica = 'S');
														
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in controllo gruppi privilegi: " + sqlca.sqlerrtext)
		ll_count = 0
	end if
	
	if ll_count > 0 then
	
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " cod_resp_divisione in (select cod_mansionario from tes_documenti_privilegi " + &
													 "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + &
													 string(l_chiave_doc.anno_registrazione) + " and num_registrazione = " + &
													 string(l_chiave_doc.num_registrazione) + " and cod_gruppo_privilegio in " + &
													 "(select cod_gruppo_privilegio from tab_gruppi_privilegi where cod_azienda = '" + &
													 s_cs_xx.cod_azienda + "' and flag_verifica = 'S') ) "
		s_cs_xx.parametri.parametro_s_5 = "Emissione del documento " + string(l_chiave_doc.anno_registrazione) + "/" + string(l_chiave_doc.num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata emesso un nuovo documento: " + string(l_chiave_doc.anno_registrazione) + "/" + string(l_chiave_doc.num_registrazione) + " " + ls_nome_documento
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai gruppi privilegi?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	else
		
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " flag_autorizzazione = 'S' and autoriz_liv_doc <= '" + ls_livello_documento + "' "
		s_cs_xx.parametri.parametro_s_5 = "Emissione del documento " + string(l_chiave_doc.anno_registrazione) + "/" + string(l_chiave_doc.num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata emesso un nuovo documento: " + string(l_chiave_doc.anno_registrazione) + "/" + string(l_chiave_doc.num_registrazione) + " " + ls_nome_documento
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari con privilegio di verifica?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	end if
	
	openwithparm(w_invio_messaggi,0)
		
end if

//Donato 12/01/2009 refresh abilitazione/disabilitazione pulsanti
dw_det_documenti_lista.change_dw_current()
postevent("pc_retrieve")
//fine modifica

return 0
end function

public function integer wf_trova (integer fl_anno, integer fl_numero, integer fl_handle);long ll_handle, ll_padre, ll_anno, ll_numero

treeviewitem ltv_item

s_chiave_doc l_chiave_doc


ll_handle = tv_1.finditem(childtreeitem!,fl_handle)

if ll_handle > 0 then
	
	tv_1.getitem(ll_handle,ltv_item)
	
	l_chiave_doc = ltv_item.data
	
	ll_anno = l_chiave_doc.anno_registrazione
	
	ll_numero = l_chiave_doc.num_registrazione
	
	if ll_anno = fl_anno and ll_numero = fl_numero then
		
		tv_1.setfocus()
		
		tv_1.selectitem(ll_handle)
		
		il_handle = ll_handle
		
		return 0
		
	else
		
		if wf_trova( fl_anno, fl_numero, ll_handle) = 0 then
			
			return 0
			
		else
			
			return 100
			
		end if
		
	end if
	
end if

ll_handle = tv_1.finditem(nexttreeitem!,fl_handle)

if ll_handle > 0 then
	
	tv_1.getitem( ll_handle, ltv_item)
	
	l_chiave_doc = ltv_item.data
	
	ll_anno = l_chiave_doc.anno_registrazione
	
	ll_numero = l_chiave_doc.num_registrazione

	if ll_anno = fl_anno and ll_numero = fl_numero then
		
		tv_1.setfocus()
		
		tv_1.selectitem(ll_handle)
		
		il_handle = ll_handle
		
		return 0
		
	else
		
		if wf_trova( fl_anno, fl_numero, ll_handle) = 0 then
			
			return 0
			
		else
			
			return 100
			
		end if
		
	end if
	
end if

ll_padre = fl_handle

do
	
	ll_padre = tv_1.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tv_1.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tv_1.getitem(ll_handle,ltv_item)
			
			l_chiave_doc = ltv_item.data
			
			ll_anno = l_chiave_doc.anno_registrazione
			
			ll_numero = l_chiave_doc.num_registrazione
			
			if ll_anno = fl_anno and ll_numero = fl_numero then
				
				tv_1.setfocus()
				
				tv_1.selectitem(ll_handle)
				
				il_handle = ll_handle
				
				return 0
				
			else
				
				if wf_trova( fl_anno, fl_numero, ll_handle) = 0 then
					
					return 0
					
				else
					
					return 100
					
				end if
				
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

public function integer wf_da_db_a_ole ();long        ll_num_registrazione,ll_progressivo,ll_righe, li_FileNum, li_r, li_i, li_ris, ll_len, ll_pos
integer     li_risposta,li_anno_registrazione
blob        lbb_blob_documento, lbb_blob
string      ls_db, ls_p, ls_estensione, ls_nome_doc, ls_tipo_doc
transaction sqlcb
dec         ld_prog, ld_num

if tab_1.tabpage_1.cbx_blocca.checked = true then return 0

ll_righe = dw_det_documenti_lista.rowcount()

if ll_righe = 0 then return 0

li_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

ls_db = f_db()
if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob_documento
	into   :lbb_blob_documento
	from   det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    progressivo = :ll_progressivo
	using	 sqlcb;

	if sqlcb.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		destroy sqlcb;
		return 0
	end if	
	
	destroy sqlcb;
	
else

	selectblob blob_documento
	into   :lbb_blob_documento
	from   det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    progressivo = :ll_progressivo;

	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
end if

// trovo il tipo di elemento
select tipo_elemento
into   :ls_tipo_doc
from   tes_documenti
where  cod_azienda = :s_cs_xx.cod_azienda
and    anno_registrazione = :li_anno_registrazione
and    num_registrazione = :ll_num_registrazione;
		
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore nella ricerca del tipo di elemento!")
	return -1
elseif sqlca.sqlcode = 100 then
	return -1
elseif sqlca.sqlcode = 0 then
	if ls_tipo_doc <> "D" then
		return -1
	end if
end if

// se è un documento ma il blob è vuoto allora esco direttamente
ll_len = len(lbb_blob_documento)
if len(lbb_blob_documento) = 0 or isnull(len(lbb_blob_documento)) then
	return 0
end if
		
// trovo di che tipo è il documento, quindi l'estensione		
select prog_mimetype
into   :ld_prog
from   det_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    progressivo = :ll_progressivo;

if sqlca.sqlcode <> 0 or isnull(ll_progressivo) then
	g_mb.messagebox("OMNIA","Impossibile risalire al tipo di documento, quindi non verrà visualizzato l'anteprima!")
	return -1
end if

select estensione
into   :ls_estensione
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda
and    prog_mimetype = :ld_prog;
		
if sqlca.sqlcode <> 0 or ls_estensione = "" then
	return -1
end if
		
// il nome del documento ha il seguente formato: aaaann..n_pppp.estensione			
ls_nome_doc = string(li_anno_registrazione) 				
ls_nome_doc = ls_nome_doc + string(ll_num_registrazione) 
ls_nome_doc = ls_nome_doc + "_" + string(ll_progressivo) 
ls_nome_doc = ls_nome_doc + "." + ls_estensione

// associo la directory col file
//ls_p = is_dir + "\" + ls_nome_doc
ls_p = is_dir + ls_nome_doc

//// Salvo su db il documento aperto prima
//li_ris = wf_salvafile()

li_FileNum = FileOpen(ls_p,StreamMode!,Write!,Shared!,Replace!)
if li_FileNum = -1 then
	g_mb.messagebox("OMNIA","Attenzione: errore durante l'apertura del file " + ls_p)
	rollback;
	return -1
end if

ll_pos = 1
Do While FileWrite(li_FileNum,BlobMid(lbb_blob_documento,ll_pos,32765)) > 0
	ll_pos += 32765
Loop
FileClose(li_FileNum)

// ora salvo l'originale
is_file = ls_p
is_ultimo_doc = ls_nome_doc

ole_1.insertfile(ls_p)
//ole_1.objectdata = lbb_blob_documento   // da decommentare

return 0
end function

public function integer wf_controlla_se_approvato (long fl_anno, long fl_num, long fl_prog);//Controlla se il documento corrente è stato approvato, in funzione anche del parametro aziendale BMD
//torna 0 se BMD è "N" o inesistente oppure  
//				BMD è "S" e il documento non è stato ancora approvato: NON BLOCCARE
//torna 1 se BMD è "S" e il documento è stato già approvato: BLOCCARE
// torna -1 se errore

string ls_flag_BMD, ls_autorizzato_da

//Leggi il parametro BMD (blocca modifica documento se approvato)
select flag
into :ls_flag_BMD
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'BMD' and flag_parametro = 'F';			
if sqlca.sqlcode = 0 then
else
	ls_flag_BMD = "N"
end if	

//leggi se il documento è approvato
select	approvato_da
into   	:ls_autorizzato_da
from   det_documenti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno and
		 num_registrazione = :fl_num and
		 progressivo = :fl_prog;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Attenzione!","Errore codice " + string(sqlca.sqlcode) + &
									" in lettura parametri approvazione del documento. Operazione annullata. " + sqlca.sqlerrtext, &
									StopSign!, ok!)
	return -1
end if

if ls_flag_BMD = "N" then
	//non bloccare
	return 0
else
	//BMD = "S"
	if ls_autorizzato_da <> "" and not isnull(ls_autorizzato_da) then
		//doc già approvato: blocca
		return 1
	else
		//doc non ancora approvato
		return 0
	end if
end if
end function

public function integer wf_salvafile_nodelete ();// questa funzione carica il blob con il contenuto dell'ultimo documento aperto
// solo se l'utente ha il permesso di modificarlo.

blob        lbb_blob_documento
long        ll_anno, ll_pos, ll_num, ll_pos2, ll_prog, ll_length, ll_ris
string      ls_db, ls_flag_BMD, ls_autorizzato_da
integer     li_risposta
transaction sqlcb
integer li_ret

ll_anno = long(Left(is_ultimo_doc,4))
ll_pos = pos(is_ultimo_doc,"_")
ll_num = long(mid(is_ultimo_doc,5,ll_pos -5))
ll_pos2 = pos(is_ultimo_doc,".")
ll_prog = long(mid(is_ultimo_doc,ll_pos + 1,ll_pos2 - ll_pos -1))

li_ret = wf_controlla_se_approvato(ll_anno, ll_num, ll_prog)

if is_modifica_doc = "S"  and is_file <> "" and not isnull(is_file) and li_ret = 0 then
	
	setnull(lbb_blob_documento)
	lbb_blob_documento = wf_carica_blob( is_file)

	ll_anno = long(Left(is_ultimo_doc,4))
	ll_pos = pos(is_ultimo_doc,"_")
	ll_num = long(mid(is_ultimo_doc,5,ll_pos -5))
	ll_pos2 = pos(is_ultimo_doc,".")
	ll_prog = long(mid(is_ultimo_doc,ll_pos + 1,ll_pos2 - ll_pos -1))

	ls_db = f_db()	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)		
		updateblob det_documenti
		set        blob_documento = :lbb_blob_documento
		where      cod_azienda = :s_cs_xx.cod_azienda
		and        anno_registrazione = :ll_anno
		and        num_registrazione = :ll_num
		and        progressivo = :ll_prog
		using      sqlcb;
	
		if sqlcb.sqlcode = 100 then
			g_mb.messagebox("OMNIA","Attenzione: il riferimento al documento non è valido!",stopsign!)
			destroy sqlcb;			
			rollback;
			return -1
		elseif sqlcb.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nel salvataggio del documento  " + is_file + " :  " + sqlcb.sqlerrtext,stopsign!)
			destroy sqlcb;			
			rollback;
			return -1
		end if				
		destroy sqlcb;
		
	else
	
		updateblob det_documenti
		set        blob_documento = :lbb_blob_documento
		where      cod_azienda = :s_cs_xx.cod_azienda
		and        anno_registrazione = :ll_anno
		and        num_registrazione = :ll_num
		and        progressivo = :ll_prog;
	
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("OMNIA","Attenzione: il riferimento al documento non è valido!",stopsign!)
			rollback;
			return -1
		elseif sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nel salvataggio del documento  " + is_file + " :  " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if		
			
	end if

//	if filedelete(is_file) <> true then
//		g_mb.messagebox("OMNIA","Impossibile cancellare il file " + is_file)	
//	end if
	commit;
	return 0
else
	return 100
end if
end function

on w_tes_documenti.create
int iCurrent
call super::create
this.rb_tutti=create rb_tutti
this.rb_protocolli=create rb_protocolli
this.rb_iso9000=create rb_iso9000
this.cb_1=create cb_1
this.dw_ricerca=create dw_ricerca
this.st_3=create st_3
this.tv_1=create tv_1
this.r_1=create r_1
this.tab_1=create tab_1
this.cb_note=create cb_note
this.dw_det_documenti_lista=create dw_det_documenti_lista
this.ole_1=create ole_1
this.dw_det_documenti_det_2=create dw_det_documenti_det_2
this.dw_det_documenti_det_1=create dw_det_documenti_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_tutti
this.Control[iCurrent+2]=this.rb_protocolli
this.Control[iCurrent+3]=this.rb_iso9000
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.dw_ricerca
this.Control[iCurrent+6]=this.st_3
this.Control[iCurrent+7]=this.tv_1
this.Control[iCurrent+8]=this.r_1
this.Control[iCurrent+9]=this.tab_1
this.Control[iCurrent+10]=this.cb_note
this.Control[iCurrent+11]=this.dw_det_documenti_lista
this.Control[iCurrent+12]=this.ole_1
this.Control[iCurrent+13]=this.dw_det_documenti_det_2
this.Control[iCurrent+14]=this.dw_det_documenti_det_1
end on

on w_tes_documenti.destroy
call super::destroy
destroy(this.rb_tutti)
destroy(this.rb_protocolli)
destroy(this.rb_iso9000)
destroy(this.cb_1)
destroy(this.dw_ricerca)
destroy(this.st_3)
destroy(this.tv_1)
destroy(this.r_1)
destroy(this.tab_1)
destroy(this.cb_note)
destroy(this.dw_det_documenti_lista)
destroy(this.ole_1)
destroy(this.dw_det_documenti_det_2)
destroy(this.dw_det_documenti_det_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag_supervisore
unsignedlong lu_privilegi

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//select cod_resp_divisione, 
//       autoriz_liv_doc, 
//		 flag_supervisore
//into   :is_cod_mansionario,
//       :is_livello_mansionario, 
//		 :ls_flag_supervisore
//from   mansionari
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_utente=:s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_supervisore = 'N'

is_livello_mansionario = luo_mansionario.uof_get_privilege_string(luo_mansionario.livello_doc)
is_cod_mansionario = luo_mansionario.uof_get_cod_mansionario()
if luo_mansionario.uof_get_privilege(luo_mansionario.supervisore) then ls_flag_supervisore = 'S'
//--- Fine modifica Giulio

//if sqlca.sqlcode < 0 then
//	
//	g_mb.messagebox("Omnia","Errore in ricerca mansionario dell'utente: accesso negato! Dettaglio errore: " + sqlca.sqlerrtext,stopsign!)
//	
//	postevent("pc_close")
//	
//end if

//if sqlca.sqlcode = 100 or isnull(is_cod_mansionario) then
if isnull(is_cod_mansionario) then

	g_mb.messagebox("Omnia","L'utente non ha alcun mansionario: accesso negato!")
	
	postevent("pc_close")
	
end if

if ls_flag_supervisore = "S" then tab_1.tabpage_2.cb_privilegi.enabled = true

tab_1.tabpage_1.ddlb_visualizza.selectitem("Ultime revisioni",1)

tab_1.tabpage_1.ddlb_livello.selectitem("Livello " + is_livello_mansionario,1)

if is_livello_mansionario = "" or isnull(is_livello_mansionario) then is_livello_mansionario = "A"

is_visualizza_doc = "N"

is_livello_doc = is_livello_mansionario

wf_inizio()

// ---------------  lettura dei privilegi degli utenti -----------------------------
uo_documenti_iso9000 luo_documenti_iso9000

luo_documenti_iso9000 = CREATE uo_documenti_iso9000

is_modifica_doc = luo_documenti_iso9000.is_modifica

is_lettura_doc = luo_documenti_iso9000.is_lettura

is_elimina_doc = luo_documenti_iso9000.is_elimina

is_autorizzazione = luo_documenti_iso9000.is_autorizzazione

is_approvazione = luo_documenti_iso9000.is_approvazione

is_validazione = luo_documenti_iso9000.is_validazione

destroy luo_documenti_iso9000

if is_autorizzazione = "N" then tab_1.tabpage_1.cb_autorizza.enabled = false

if is_approvazione = "N" then tab_1.tabpage_1.cb_approva.enabled = false

if is_validazione = "N" then tab_1.tabpage_1.cb_valida.enabled = false

if is_modifica_doc = "N" then 
	
	tab_1.tabpage_1.cb_revisione.enabled = false
	
	tab_1.tabpage_1.pb_apri_file.enabled = false
	
end if

if is_lettura_doc = "N" then
	
	tab_1.tabpage_1.cbx_blocca.checked = true
	
	tab_1.tabpage_1.cbx_blocca.enabled = false
	
end if

// -------------------------------  impostazione DW visualizzazione proprietà documenti --------------------------

lu_privilegi = c_nonew

if is_modifica_doc = "S" then 
	
	lu_privilegi = lu_privilegi +  c_modifyok
	
else
	
	lu_privilegi = lu_privilegi +  c_nomodify
	
end if

if is_elimina_doc = "S" then 
	
	lu_privilegi = lu_privilegi +  c_deleteok
	
else
	
	lu_privilegi = lu_privilegi +  c_nodelete
	
end if

dw_det_documenti_lista.set_dw_options(sqlca, &
				                          pcca.null_object, &
                                      lu_privilegi, &
                                      c_NoCursorRowFocusRect)
							  
dw_det_documenti_det_1.set_dw_options(sqlca, &
							  	  			     dw_det_documenti_lista, &
							 				     c_sharedata + c_scrollparent, &
										 	     c_default)

dw_det_documenti_det_2.set_dw_options(sqlca, &
							  	  			     dw_det_documenti_lista, &
							 				     c_sharedata + c_scrollparent, &
										 	     c_default)

iuo_dw_main = dw_det_documenti_lista

s_cs_xx.parametri.parametro_d_1 = this.x

s_cs_xx.parametri.parametro_d_2 = this.y


//Donato 18-09-2008
//Modifica su richiesta di Marco
//Ho insrito un parametro multiaziendale chiamato "CDS" di tipo stringa (controlla documenti scaduti)
//Se presente in tabella e con valore "N" allora non esegue il controllo, altrimenti 
//(se non c'è oppure vale "S") il controllo viene effettuato
string ls_cds

select stringa
into :ls_cds
from parametri
where flag_parametro = 'S' and cod_parametro = 'CDS'
;
choose case sqlca.sqlcode
	case 0
		if ls_cds = "S" then
			//fai il controllo
			postevent("ue_controllo_doc_scaduti")			
		else
			//non fare il controllo
		end if
	case 100
		//record non trovato: fai il controllo
		postevent("ue_controllo_doc_scaduti")
	case else
		g_mb.messagebox("OMNIA","Errore durante la ricerca del parametro multiaziendale 'CDS'" + char(13) + &
													sqlca.sqlerrtext, Exclamation!)
end choose	
//postevent("ue_controllo_doc_scaduti")
//fine modifica -----------------------------------------------------------















end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_documenti_det_2,"resp_conservazione_doc",sqlca,&
                 "mansionari","cod_resp_divisione","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event open;call super::open;
// Il nome della cartella temporanea è nel parametro aziendale TDD 

string  ls_dir, ls_path, ls_default, ls_valore, ls_str
integer li_risposta

ls_dir = s_cs_xx.volume

setnull(ls_valore)

select stringa
into   :ls_valore
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_parametro = 'TDD';

if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Attenzione: Errore nella select del parametro TDD!~r~n" + sqlca.sqlerrtext)
	return -1	
end if

// *** michela 18/03/04: se non trovo il parametro allora i documenti li vado a mettere nel folder delle risorse

if sqlca.sqlcode = 100 then
	setnull(ls_valore)
	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "ris", ls_valore)	
end if

// *** 

if isnull(ls_valore) or ls_valore = "" then
	g_mb.messagebox("OMNIA","Attenzione: impostare il parametro aziendale TDD o il parametro del registro ris. ")
	return -1		
end if

setnull(ls_str)
ls_str = left(ls_valore, 1)
if ls_str <> "\" then ls_valore = "\" + ls_valore

setnull(ls_str)
ls_str = right(ls_valore, 1)
if ls_str <> "\" then ls_valore = ls_valore + "\"

is_dir = ls_dir + ls_valore


//f_getuserpath(is_dir)

// -- stefanop 11/10/10: Mostro il pulsante notifiche solo se abilitato
string ls_test
select flag
into :ls_test
from	parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'IDL';
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox(this.title, "Errore durante la lettura del flag IDL.~r~n" + sqlca.sqlerrtext)
elseif sqlca.sqlcode = 100 then
	tab_1.tabpage_3.cb_notifiche_letture.visible = false
elseif sqlca.sqlcode = 0 and ls_test = "N" then
	tab_1.tabpage_3.cb_notifiche_letture.visible = false
end if
// ----

end event

event close;call super::close;wf_salvafile()
end event

type rb_tutti from radiobutton within w_tes_documenti
integer x = 1280
integer y = 160
integer width = 192
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tutti"
end type

event clicked;rb_iso9000.checked = false
rb_protocolli.checked=false

setpointer(HourGlass!)
wf_inizio()
setpointer(Arrow!)
end event

type rb_protocolli from radiobutton within w_tes_documenti
integer x = 731
integer y = 160
integer width = 329
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Protocolli"
end type

event clicked;rb_iso9000.checked = false
rb_tutti.checked=false

setpointer(HourGlass!)
wf_inizio()
setpointer(Arrow!)
end event

type rb_iso9000 from radiobutton within w_tes_documenti
integer x = 46
integer y = 160
integer width = 535
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Documentazione"
boolean checked = true
end type

event clicked;rb_protocolli.checked = false
rb_tutti.checked=false

setpointer(HourGlass!)
wf_inizio()
setpointer(Arrow!)
end event

type cb_1 from cb_documenti_ricerca within w_tes_documenti
integer x = 1509
integer y = 20
integer width = 279
integer height = 100
integer taborder = 30
string text = "Cerca"
end type

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
s_cs_xx.parametri.parametro_s_1 = is_livello_doc
s_cs_xx.parametri.parametro_s_2 = is_visualizza_doc



end event

type dw_ricerca from uo_dw_search within w_tes_documenti
event ue_cerca_doc ( )
integer x = 23
integer y = 2080
integer width = 1696
integer height = 100
integer taborder = 60
string dataobject = "d_documenti_ricerca_ext"
boolean border = false
end type

event ue_cerca_doc();long ll_anno, ll_numero

ll_anno = this.getitemnumber( 1, "anno_registrazione")

ll_numero = this.getitemnumber( 1, "num_registrazione")

if ll_anno > 0 and ll_numero > 0 then

	tv_1.setredraw(false)
	
	wf_inizio()
	
	tv_1.setredraw(true)
	
	il_handle = tv_1.finditem(roottreeitem!,0)
		
	tv_1.setfocus()
	
	tv_1.selectitem(il_handle)	
	
	if il_handle > 0 then
		
		if wf_trova( ll_anno, ll_numero, il_handle) = 100 then
			
			g_mb.messagebox("Menu Principale","Elemento non trovato",information!)
			
			tv_1.setfocus()
			
		end if
		
	end if
	
end if
end event

type st_3 from statictext within w_tes_documenti
integer x = 23
integer y = 20
integer width = 1486
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type tv_1 from treeview within w_tes_documenti
integer x = 23
integer y = 260
integer width = 1783
integer height = 1780
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean border = false
boolean editlabels = true
boolean disabledragdrop = false
boolean hideselection = false
string picturename[] = {"","","",""}
long picturemaskcolor = 553648127
long statepicturemaskcolor = 536870912
end type

event clicked;selectitem(handle)
end event

event endlabeledit;treeviewitem tvi

getitem(handle,tvi)

if isnull(newtext) then 
	if tvi.label="" then
		g_mb.messagebox("Omnia","Inserire un nome per l'elemento!",stopsign!)
		editlabel(handle)	
		return
	else
		return
	end if
end if
	

if newtext = "" then
	tvi.label = ""
	setitem(handle,tvi)
	g_mb.messagebox("Omnia","Inserire un nome per l'elemento!",stopsign!)
	editlabel(handle)
	return
end if

wf_fine_modifica(handle,newtext)
end event

event key;choose case key
	case keydelete! 
		wf_elimina()
		
	case keyf2!
		//wf_modifica()
		
end choose

end event

event rightclicked;m_tree_documenti popmenu

selectitem(handle)

popmenu = CREATE m_tree_documenti

popmenu.PopMenu (PCCA.MDI_Frame.PointerX()-20, PCCA.MDI_Frame.PointerY()-20)
end event

event selectionchanged;dw_det_documenti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

wf_refresh_ole()
end event

event constructor;do while true
	if deletepicture(1) = -1 then exit
loop
addpicture(s_cs_xx.volume + s_cs_xx.risorse + "book.ico")
addpicture(s_cs_xx.volume + s_cs_xx.risorse + "cartella_apri.bmp")
addpicture(s_cs_xx.volume + s_cs_xx.risorse + "cartella_chiudi.bmp")
addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mod.bmp")

end event

type r_1 from rectangle within w_tes_documenti
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 23
integer y = 140
integer width = 1760
integer height = 100
end type

type tab_1 from tab within w_tes_documenti
event create ( )
event destroy ( )
integer x = 1806
integer width = 2446
integer height = 2040
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

event selectionchanged;choose case newindex
	case 1
		dw_det_documenti_lista.visible = true
		dw_det_documenti_det_1.visible = false
		dw_det_documenti_det_2.visible = false
		ole_1.visible = true
	case 2
		dw_det_documenti_lista.visible = false
		dw_det_documenti_det_1.visible = true
		dw_det_documenti_det_2.visible = false
		ole_1.visible = false
	case 3
		dw_det_documenti_lista.visible = false
		dw_det_documenti_det_1.visible = false
		dw_det_documenti_det_2.visible = true
		ole_1.visible = false
end choose
end event

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 2409
integer height = 1916
long backcolor = 12632256
string text = "Edizioni/Revisioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
ddlb_livello ddlb_livello
st_2 st_2
st_1 st_1
cb_revisione cb_revisione
cbx_blocca cbx_blocca
cb_valida cb_valida
ddlb_visualizza ddlb_visualizza
pb_apri_file pb_apri_file
cb_approva cb_approva
cb_autorizza cb_autorizza
end type

on tabpage_1.create
this.ddlb_livello=create ddlb_livello
this.st_2=create st_2
this.st_1=create st_1
this.cb_revisione=create cb_revisione
this.cbx_blocca=create cbx_blocca
this.cb_valida=create cb_valida
this.ddlb_visualizza=create ddlb_visualizza
this.pb_apri_file=create pb_apri_file
this.cb_approva=create cb_approva
this.cb_autorizza=create cb_autorizza
this.Control[]={this.ddlb_livello,&
this.st_2,&
this.st_1,&
this.cb_revisione,&
this.cbx_blocca,&
this.cb_valida,&
this.ddlb_visualizza,&
this.pb_apri_file,&
this.cb_approva,&
this.cb_autorizza}
end on

on tabpage_1.destroy
destroy(this.ddlb_livello)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_revisione)
destroy(this.cbx_blocca)
destroy(this.cb_valida)
destroy(this.ddlb_visualizza)
destroy(this.pb_apri_file)
destroy(this.cb_approva)
destroy(this.cb_autorizza)
end on

type ddlb_livello from dropdownlistbox within tabpage_1
integer x = 1170
integer y = 560
integer width = 411
integer height = 380
integer taborder = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
string item[] = {"Livello A","Livello B","Livello C","Tutti"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;choose case index
	case 1
		is_livello_doc = "A"
	case 2
		is_livello_doc = "B"
	case 3
		is_livello_doc = "C"
	case 4
		is_livello_doc = is_livello_mansionario
end choose
if is_livello_doc < is_livello_mansionario then
	is_livello_doc = is_livello_mansionario
	g_mb.messagebox("OMNIA","Utente non abilitato a questo livello di documentazione")
	ddlb_livello.selectitem("Livello " + is_livello_mansionario,1)
end if
wf_inizio()

end event

type st_2 from statictext within tabpage_1
integer x = 942
integer y = 572
integer width = 261
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Livello:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within tabpage_1
integer x = 14
integer y = 572
integer width = 302
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza:"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_revisione from commandbutton within tabpage_1
integer x = 965
integer y = 468
integer width = 311
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Revisione"
end type

event clicked;string ls_livello
long   ll_anno_registrazione, ll_num_registrazione, ll_progressivo, ll_count
uo_documenti_iso9000 luo_documenti_iso9000

ll_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

luo_documenti_iso9000 = CREATE uo_documenti_iso9000
if luo_documenti_iso9000.uof_nuova_revisione(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	commit;
	g_mb.messagebox("OMNIA","Nuova Revisione eseguita con successo.")
else
	rollback;
	g_mb.messagebox("OMNIA",luo_documenti_iso9000.is_errore,stopsign!)
	destroy luo_documenti_iso9000
	return
end if
destroy luo_documenti_iso9000

//dw_det_documenti_lista.postevent("pcd_retrieve")
dw_det_documenti_lista.triggerevent("pcd_retrieve")
if dw_det_documenti_lista.rowcount() > 0 then
	dw_det_documenti_lista.selectrow(0, false)
	dw_det_documenti_lista.selectrow(1, true)
end if
wf_da_db_a_ole()
//-----------------------------------------------------------------

if s_cs_xx.num_livello_mail > 0 then
	
	select count(*)
	into   :ll_count
	from   tes_documenti_privilegi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 cod_gruppo_privilegio in (select cod_gruppo_privilegio
			 									from tab_gruppi_privilegi
												where cod_azienda = :s_cs_xx.cod_azienda and
														flag_verifica = 'S');
														
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in controllo gruppi privilegi: " + sqlca.sqlerrtext)
		ll_count = 0
	end if
	
	if ll_count > 0 then
	
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " cod_resp_divisione in (select cod_mansionario from tes_documenti_privilegi " + &
													 "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + &
													 string(ll_anno_registrazione) + " and num_registrazione = " + &
													 string(ll_num_registrazione) + " and cod_gruppo_privilegio in " + &
													 "(select cod_gruppo_privilegio from tab_gruppi_privilegi where cod_azienda = '" + &
													 s_cs_xx.cod_azienda + "' and flag_verifica = 'S') ) "
		s_cs_xx.parametri.parametro_s_5 = "Nuova revisione del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata emessa una nuova revisione del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai gruppi privilegi?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	else
	
		ls_livello = dw_det_documenti_lista.getitemstring(dw_det_documenti_lista.getrow(),"livello_documento")
			
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " flag_autorizzazione = 'S' and autoriz_liv_doc <= '" + ls_livello + "' "
		s_cs_xx.parametri.parametro_s_5 = "Nuova revisione del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata emessa una nuova revisione del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari con privilegio di verifica?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	end if
		
	openwithparm(w_invio_messaggi,0)
		
end if
end event

type cbx_blocca from checkbox within tabpage_1
integer x = 5
integer y = 664
integer width = 891
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Blocca Aggiornamento Oggetto"
boolean checked = true
end type

type cb_valida from commandbutton within tabpage_1
integer x = 645
integer y = 468
integer width = 311
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Valida"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione, ll_progressivo
uo_documenti_iso9000 luo_documenti_iso9000

ll_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

luo_documenti_iso9000 = CREATE uo_documenti_iso9000
if luo_documenti_iso9000.uof_valida_documento(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	commit;
	g_mb.messagebox("OMNIA","Validazione eseguita con successo !")
else
	rollback;
	g_mb.messagebox("OMNIA",luo_documenti_iso9000.is_errore,stopsign!)
end if

destroy luo_documenti_iso9000
dw_det_documenti_lista.change_dw_current()
dw_det_documenti_lista.postevent("pcd_retrieve")



end event

type ddlb_visualizza from dropdownlistbox within tabpage_1
integer x = 320
integer y = 560
integer width = 635
integer height = 260
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
string item[] = {"Ultime Revisioni","Storico Documenti"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;choose case index
	case 1
		is_visualizza_doc = "S"
	case 2
		is_visualizza_doc = "N"
end choose
wf_inizio()

end event

type pb_apri_file from picturebutton within tabpage_1
integer x = 1481
integer y = 660
integer width = 101
integer height = 84
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_70\framework\RISORSE\MENUVOCA.BMP"
string disabledname = "C:\cs_70\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string ls_file, ls_path, ls_db
long ll_ret, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, ll_blocca
blob lbb_blob_documento
integer li_risposta
transaction sqlcb

ll_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

ll_blocca = wf_controlla_se_approvato(ll_anno_registrazione, ll_num_registrazione, ll_progressivo)
if ll_blocca = 1 then
	//blocca perchè il doc è già approvato	
	g_mb.messagebox("Omnia","Impossibile proseguire: documento già approvato!",Exclamation!)
	return 0
	
elseif ll_blocca = -1 then
	//errore con msg già dato (return)
	return 0
end if

ll_ret = getfileopenname("Cerca Oggetto",ls_path, ls_file, "DOC","Microsoft Word (*.DOC),*.DOC," + &
  		                                                         + "Microsoft Excel (*.XLS),*.XLS," + &
  		                                                         + "Tutti i files (*.*),*.*")
if ll_ret < 1 then return

ll_ret = ole_1.InsertFile(ls_path)
if ll_ret < 0 then 
	g_mb.messagebox("Omnia","Errore durante il caricamento dell'oggetto OLE (ad es. il potrebbe non essere più disponibile in rete): contattare l'amministratore del sistema",StopSign!)
	return 0
end if

ll_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

// modifiche michela

lbb_blob_documento = wf_carica_blob(ls_path)
	
//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************
string ls_nome_doc, ls_p, ls_reverse, ls_tipof
integer li_ris, li_filenum, li_pos
long    ll_pos

li_pos = 0
ls_reverse = reverse(ls_path)

li_pos = pos(ls_reverse,".")
if li_pos > 0 then
	ls_tipof = right(ls_file,(li_pos - 1))
	ls_tipof = upper(ls_tipof)
else
	setnull(ls_tipof)
	g_mb.messagebox("Omnia","Attenzione, il tipo di documento non è codificato! Impossibile effettuare l'inserimento.",stopsign!)
	rollback;
	return 0
end if



ls_nome_doc = string(ll_anno_registrazione) 				
ls_nome_doc = ls_nome_doc + string(ll_num_registrazione) 
ls_nome_doc = ls_nome_doc + "_" + string(ll_progressivo) 
ls_nome_doc = ls_nome_doc + "." + ls_tipof

//ls_p = is_dir + "\" + ls_nome_doc
ls_p = is_dir + ls_nome_doc

li_ris = wf_salvafile()

li_FileNum = FileOpen(ls_p,StreamMode!,Write!,Shared!,Replace!)
if li_FileNum = -1 then
	g_mb.messagebox("OMNIA","Attenzione: errore durante l'apertura del file " + ls_p)
	rollback;
	return -1
end if
ll_pos = 1
Do While FileWrite(li_FileNum,BlobMid(lbb_blob_documento,ll_pos,32765)) > 0
	ll_pos += 32765
Loop
FileClose(li_FileNum)

// ora salvo l'originale
is_file = ls_p
is_ultimo_doc = ls_nome_doc

//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************
//**************************************************************








// 11-07-2002 modifiche Michela: controllo enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	updateblob  det_documenti
	set         blob_documento = :lbb_blob_documento
	where       cod_azienda = :s_cs_xx.cod_azienda and
	 		      anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione and
					progressivo = :ll_progressivo
	using       sqlcb;

	if sqlcb.sqlcode <> 0 then
		g_mb.messagebox("Omnia","Errore in salvataggio del documento; dettaglio: " + sqlcb.sqlerrtext,stopsign!)
		rollback;
		destroy sqlcb;
		return 0
	end if	
	
	destroy sqlcb;
	
else
	
	updateblob  det_documenti
	set         blob_documento = :lbb_blob_documento
	where       cod_azienda = :s_cs_xx.cod_azienda and
	 		      anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione and
					progressivo = :ll_progressivo;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia","Errore in salvataggio del documento; dettaglio: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return 0
	end if

end if

// fine modifiche

commit;
return 0
end event

type cb_approva from commandbutton within tabpage_1
integer x = 325
integer y = 468
integer width = 311
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Approva"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione, ll_progressivo
uo_documenti_iso9000 luo_documenti_iso9000
string ls_firma_approvato_da
 
ll_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

wf_salvafile_nodelete()

luo_documenti_iso9000 = CREATE uo_documenti_iso9000
if luo_documenti_iso9000.uof_approva_documento(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	commit;
	g_mb.messagebox("OMNIA","Approvazione eseguita con successo !")
else
	rollback;
	g_mb.messagebox("OMNIA",luo_documenti_iso9000.is_errore,stopsign!)
end if

destroy luo_documenti_iso9000
dw_det_documenti_lista.change_dw_current()

//dw_det_documenti_lista.postevent("pcd_retrieve")
dw_det_documenti_lista.triggerevent("pcd_retrieve")
if dw_det_documenti_lista.rowcount() > 0 then
	dw_det_documenti_lista.selectrow(0, false)
	dw_det_documenti_lista.selectrow(1, true)
end if
//wf_da_db_a_ole()
//-----------------------------------------------------------------

end event

type cb_autorizza from commandbutton within tabpage_1
integer x = 5
integer y = 468
integer width = 311
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Verifica"
end type

event clicked;string ls_livello
long   ll_anno_registrazione, ll_num_registrazione, ll_progressivo, ll_count
uo_documenti_iso9000 luo_documenti_iso9000

ll_anno_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

luo_documenti_iso9000 = CREATE uo_documenti_iso9000
if luo_documenti_iso9000.uof_autorizza_documento(ll_anno_registrazione, ll_num_registrazione, ll_progressivo) = 0 then
	
	commit;
	g_mb.messagebox("OMNIA","Verifica eseguita con successo !")
else
	rollback;
	g_mb.messagebox("OMNIA",luo_documenti_iso9000.is_errore,stopsign!)
	destroy luo_documenti_iso9000
	return
end if

destroy luo_documenti_iso9000
dw_det_documenti_lista.change_dw_current()

//dw_det_documenti_lista.postevent("pcd_retrieve")
dw_det_documenti_lista.triggerevent("pcd_retrieve")
if dw_det_documenti_lista.rowcount() > 0 then
	dw_det_documenti_lista.selectrow(0, false)
	dw_det_documenti_lista.selectrow(1, true)
end if
//wf_da_db_a_ole()
//-----------------------------------------------------------------

if s_cs_xx.num_livello_mail > 0 then
	
	select count(*)
	into   :ll_count
	from   tes_documenti_privilegi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 cod_gruppo_privilegio in (select cod_gruppo_privilegio
			 									from tab_gruppi_privilegi
												where cod_azienda = :s_cs_xx.cod_azienda and
														flag_approvazione = 'S');
														
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in controllo gruppi privilegi: " + sqlca.sqlerrtext)
		ll_count = 0
	end if
	
	if ll_count > 0 then
	
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " cod_resp_divisione in (select cod_mansionario from tes_documenti_privilegi " + &
													 "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + &
													 string(ll_anno_registrazione) + " and num_registrazione = " + &
													 string(ll_num_registrazione) + " and cod_gruppo_privilegio in " + &
													 "(select cod_gruppo_privilegio from tab_gruppi_privilegi where cod_azienda = '" + &
													 s_cs_xx.cod_azienda + "' and flag_approvazione = 'S') ) "
		s_cs_xx.parametri.parametro_s_5 = "Verifica del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata verificato il documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai gruppi privilegi?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
	else
	
		ls_livello = dw_det_documenti_lista.getitemstring(dw_det_documenti_lista.getrow(),"livello_documento")
		
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		s_cs_xx.parametri.parametro_s_4 = " flag_approvazione = 'S' and autoriz_liv_doc <= '" + ls_livello + "' "
		s_cs_xx.parametri.parametro_s_5 = "Verifica del documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_6 = "E' stata verificato il documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari con privilegio di approvazione?"
		s_cs_xx.parametri.parametro_s_8 = ""
	
	end if
	
	openwithparm(w_invio_messaggi,0)
		
end if
end event

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2409
integer height = 1916
long backcolor = 12632256
string text = "Firme Elettroniche"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_paragrafi_iso cb_paragrafi_iso
cb_privilegi cb_privilegi
end type

on tabpage_2.create
this.cb_paragrafi_iso=create cb_paragrafi_iso
this.cb_privilegi=create cb_privilegi
this.Control[]={this.cb_paragrafi_iso,&
this.cb_privilegi}
end on

on tabpage_2.destroy
destroy(this.cb_paragrafi_iso)
destroy(this.cb_privilegi)
end on

type cb_paragrafi_iso from commandbutton within tabpage_2
integer x = 841
integer y = 1568
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Paragrafi ISO"
end type

event clicked;if dw_det_documenti_lista.rowcount() > 0 then
	window_open_parm(w_tes_documenti_paragrafi_iso, -1, dw_det_documenti_lista)
end if
end event

type cb_privilegi from commandbutton within tabpage_2
integer x = 1234
integer y = 1568
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Privilegi Doc."
end type

event clicked;long ll_handle_corrente
treeviewitem tvi_campo
s_chiave_doc l_chiave_doc

if dw_det_documenti_lista.rowcount() > 0 then
	ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
	if ll_handle_corrente=-1 then
		g_mb.messagebox("Omnia","Attenzione! E' necessario selezionare almeno un elemento.",stopsign!)
		return 0
	end if
	tv_1.GetItem (ll_handle_corrente, tvi_campo )
	l_chiave_doc = tvi_campo.data
	s_cs_xx.parametri.parametro_d_1 = l_chiave_doc.anno_registrazione
	s_cs_xx.parametri.parametro_d_2 = l_chiave_doc.num_registrazione
	open(w_tes_documenti_privilegi)
end if

end event

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2409
integer height = 1916
long backcolor = 12632256
string text = "Dati Conservazione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_notifiche_letture cb_notifiche_letture
cb_elimina_liste_dist cb_elimina_liste_dist
cb_liste_dist cb_liste_dist
end type

on tabpage_3.create
this.cb_notifiche_letture=create cb_notifiche_letture
this.cb_elimina_liste_dist=create cb_elimina_liste_dist
this.cb_liste_dist=create cb_liste_dist
this.Control[]={this.cb_notifiche_letture,&
this.cb_elimina_liste_dist,&
this.cb_liste_dist}
end on

on tabpage_3.destroy
destroy(this.cb_notifiche_letture)
destroy(this.cb_elimina_liste_dist)
destroy(this.cb_liste_dist)
end on

type cb_notifiche_letture from commandbutton within tabpage_3
integer x = 347
integer y = 1572
integer width = 485
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Notifiche Lettura"
end type

event clicked;s_cs_xx_parametri lstr_param

lstr_param.parametro_ul_1 = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
lstr_param.parametro_ul_2 = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
lstr_param.parametro_ul_3 = dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

openWithParm(w_notifiche_documenti, lstr_param)
end event

type cb_elimina_liste_dist from commandbutton within tabpage_3
integer x = 1230
integer y = 1576
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina L.D."
end type

event clicked;long    ll_progressivo,ll_progr_lista_distribuzione,ll_risposta,ll_num_registrazione
integer li_anno_registrazione
string  ls_path_doc_from ,ls_flag_approvazione, ls_flag_autorizzazione, & 
		  ls_flag_validazione, ls_flag_lettura,ls_flag_modifica,ls_flag_elimina 
		  
//Giulio: 09/11/2011 cambio gestione privilegi mansionario
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario
//SELECT flag_approvazione,
//		 flag_autorizzazione,
//		 flag_validazione,
//		 flag_lettura,
//		 flag_modifica,
//		 flag_elimina
//INTO   :ls_flag_approvazione, 
//	    :ls_flag_autorizzazione,
//	    :ls_flag_validazione, 
//	    :ls_flag_lettura,
//	    :ls_flag_modifica, 		 
//	    :ls_flag_elimina &
//FROM   mansionari
//WHERE  cod_azienda=:s_cs_xx.cod_azienda 
//AND 	 cod_utente =:s_cs_xx.cod_utente;

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'
ls_flag_lettura = 'N'
ls_flag_modifica = 'N'
ls_flag_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.lettura) then ls_flag_lettura = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'

if ls_flag_modifica="N" then 
	g_mb.messagebox("Omnia","Non si possiede il privilegio di modifica del documento, pertanto non è possibile eliminare la lista di distribuzione associata",exclamation!)
	return 
end if

ll_risposta = g_mb.messagebox("Omnia", "Sei sicuro di voler eliminare la lista di distribuzione associata?",  Exclamation!, YesNo!, 2)

if ll_risposta=2 then return

if isnull(dw_det_documenti_lista.getitemstring(dw_det_documenti_lista.getrow(),"nome_documento")) then return

li_anno_registrazione=dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione=dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo=dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")


select prog_lista_distribuzione
into   :ll_progr_lista_distribuzione
from   det_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    progressivo=:ll_progressivo;

if isnull(ll_progr_lista_distribuzione) or ll_progr_lista_distribuzione=0 then
	g_mb.messagebox("OMNIA","Non risulta associata al documento alcuna lista di distribuzione", StopSign!)
	return
end if

update det_documenti
set    prog_lista_distribuzione=0
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    progressivo=:ll_progressivo;

choose case sqlca.sqlcode
	case 100
		g_mb.messagebox("OMNIA","Impossibile disassociare il numero lista: non trovo il documento "+string(ll_progressivo), StopSign!)
	case 0
		
	case else
		g_mb.messagebox("OMNIA","Errore durante la disassociazione lista di distribuzione. Errore SQL: "+sqlca.sqlerrtext, StopSign!)
end choose

if f_elimina_liste_dist_doc(ll_progr_lista_distribuzione) = 0 then
	g_mb.messagebox("OMNIA", "Operazione eseguita con successo", Information!)
end if
end event

type cb_liste_dist from commandbutton within tabpage_3
integer x = 841
integer y = 1576
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Liste Dist."
end type

event clicked;long ll_progr_lista_distribuzione,ll_progressivo,ll_num_registrazione
integer li_anno_registrazione

//if isnull(dw_documenti_lista.getitemstring(dw_det_documenti_lista.getrow(),"nome_documento")) then return

li_anno_registrazione=dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione=dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"num_registrazione")
ll_progressivo=dw_det_documenti_lista.getitemnumber(dw_det_documenti_lista.getrow(),"progressivo")

select prog_lista_distribuzione
into   :ll_progr_lista_distribuzione
from   det_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    progressivo=:ll_progressivo;

if isnull(ll_progr_lista_distribuzione) or ll_progr_lista_distribuzione=0 then
	
	ll_progr_lista_distribuzione=f_progressivo_lista_dist()
	
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_distribuzione	

//*********   la path non serve più poichè non esiste più il file ma il blob dentro al DB, nel momento in cui
//********* spedisco il doc. allora trasformo il blob del DB in un file temporaneo.

//	if fileexists(ls_path_doc_from) then
//		ls_path_doc_to=left(ls_path_doc_from,len(ls_path_doc_from)-4) + right(ls_path_doc_from,3) + ".doc"
//		w_POManager_STD.EXT.fu_FileCopy(ls_path_doc_from,ls_path_doc_to)
//	end if

	s_cs_xx.parametri.parametro_s_1 = "" // ls_path_doc_to

	window_open(w_seleziona_lista_dist,0)
	
	if s_cs_xx.parametri.parametro_d_1 <> -1 then
		
		update det_documenti
		set    prog_lista_distribuzione=:ll_progr_lista_distribuzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    progressivo=:ll_progressivo;
		
	end if

else
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_distribuzione
		
	s_cs_xx.parametri.parametro_s_10 = string(li_anno_registrazione)
	s_cs_xx.parametri.parametro_s_11 = string(ll_num_registrazione)
	s_cs_xx.parametri.parametro_s_12 = string(ll_progressivo)
	
	window_open(w_liste_dist_det_doc,0)
	
	s_cs_xx.parametri.parametro_s_10 = ""
	s_cs_xx.parametri.parametro_s_11 = ""
	s_cs_xx.parametri.parametro_s_12 = ""

end if
end event

type cb_note from commandbutton within w_tes_documenti
integer x = 3109
integer y = 576
integer width = 311
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Note"
end type

event clicked;long ll_row


ll_row = dw_det_documenti_lista.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare una revisione di un documento prima di continuare",exclamation!)
	return -1
end if

if isnull(dw_det_documenti_lista.getitemnumber(ll_row,"anno_registrazione")) or &
	isnull(dw_det_documenti_lista.getitemnumber(ll_row,"num_registrazione")) or &
	isnull(dw_det_documenti_lista.getitemnumber(ll_row,"progressivo")) then
	g_mb.messagebox("OMNIA","Selezionare una revisione di un documento prima di continuare",exclamation!)
	return -1
end if

//window_open_parm(w_det_documenti_note,-1,dw_det_documenti_lista)
s_cs_xx.parametri.parametro_ul_1 = dw_det_documenti_lista.getitemnumber(ll_row,"anno_registrazione")
s_cs_xx.parametri.parametro_ul_2 = dw_det_documenti_lista.getitemnumber(ll_row,"num_registrazione")
s_cs_xx.parametri.parametro_ul_3 = dw_det_documenti_lista.getitemnumber(ll_row,"progressivo")
open(w_det_documenti_ole)
end event

type dw_det_documenti_lista from uo_cs_xx_dw within w_tes_documenti
integer x = 1829
integer y = 120
integer width = 2377
integer height = 440
integer taborder = 30
string dataobject = "d_det_documenti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event rowfocuschanged;call super::rowfocuschanged;wf_refresh_ole()
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle, & 
	  ll_num_registrazione,ll_errore
string ls_tipo
blob lbb_blob_documento
integer li_risposta,li_anno_registrazione
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo


ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
li_risposta = tv_1.GetItem (ll_handle_corrente, tvi_campo )

if li_risposta = -1 then return -1

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.cod_padre
ll_cod_figlio = l_chiave_doc.cod_figlio
li_anno_registrazione=l_chiave_doc.anno_registrazione
ll_num_registrazione=l_chiave_doc.num_registrazione
ls_tipo = l_chiave_doc.tipo_elemento

ll_errore = retrieve(s_cs_xx.cod_azienda,li_anno_registrazione,ll_num_registrazione,is_visualizza_doc)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

wf_controllo_privilegi(li_anno_registrazione,ll_num_registrazione)
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	if is_modifica_doc <> "S" then
		g_mb.messagebox("OMNIA","Mansionario non abilitato alla modifica")
		pcca.error = c_fatal
		return
	end if
end if
end event

type ole_1 from olecontrol within w_tes_documenti
event salvadati ( )
integer x = 1829
integer y = 860
integer width = 2377
integer height = 1160
integer taborder = 20
string dragicon = "D:\TR\BOOK.ICO"
boolean border = false
long backcolor = 16777215
string pointer = "HyperLink!"
boolean focusrectangle = false
boolean isdragtarget = true
string binarykey = "w_tes_documenti.win"
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
omlinkupdateoptions linkupdateoptions = linkupdatemanual!
sizemode sizemode = clip!
end type

event salvadati();//long ll_t
//
//for ll_t = 1 to 200000
//next
//	
//if is_modifica_doc = "S" then
//	wf_updateblob()
//end if

end event

event doubleclicked;integer  li_ris, ll_blocca
string   ls_percorso
long hwnd, ll_cur_row, ll_ret, ll_anno_registrazione, ll_num_registrazione, ll_progressivo

ls_percorso = is_file


//Donato 08/01/2009 controlla se c'è l'abilitazione del gruppo, in quanto tale autorizzazione
//comanda su quella del mansionario

ll_cur_row = dw_det_documenti_lista.getrow()

if ll_cur_row > 0 then
	ll_anno_registrazione = dw_det_documenti_lista.getitemnumber(ll_cur_row,"anno_registrazione")
	ll_num_registrazione = dw_det_documenti_lista.getitemnumber(ll_cur_row,"num_registrazione")
	ll_progressivo = dw_det_documenti_lista.getitemnumber(ll_cur_row,"progressivo")
	
	ll_blocca = wf_controlla_se_approvato(ll_anno_registrazione, ll_num_registrazione, ll_progressivo)
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
		uo_documenti_iso9000 luo_documenti_iso9000
		
		luo_documenti_iso9000 = CREATE uo_documenti_iso9000
		
		ll_ret = luo_documenti_iso9000.uof_privilegi_documento(ll_anno_registrazione, ll_num_registrazione)
		
		if ll_ret < 0 then
			//is_errore è già valorizzato.....verrà dato il messaggio di errore opportuno
			return -1
		else
			is_modifica_doc = luo_documenti_iso9000.is_modifica
			is_lettura_doc = luo_documenti_iso9000.is_lettura
			is_elimina_doc = luo_documenti_iso9000.is_elimina
			is_autorizzazione = luo_documenti_iso9000.is_autorizzazione
			is_approvazione = luo_documenti_iso9000.is_approvazione
			is_validazione = luo_documenti_iso9000.is_validazione
		end if
		//la funzione uof_privilegi_documento ha riscritto le variabili di istanza delle autorizzazioni varie in base al gruppo
	end if
end if


if (is_lettura_doc = "S" and is_modifica_doc = "N") then
	g_mb.messagebox("Omnia","Attenzione !! Tutte le modifiche che verranno apportate al documento non saranno memorizzate.")	

	hwnd = handle(parent)	
	li_ris = shellexecuteA(hwnd,'open',is_file,'','',1)
	if li_ris <= 32 then
		FileDelete(is_file)
		g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento " + is_file, exclamation!, ok!)		
	end if
	
	// 15/07/2003
//	li_ris = run(ls_percorso,normal!)	
//	if li_ris < 0 then
//		FileDelete(is_file)
//		messagebox("Attenzione", "Impossibile visualizzare il documento " + is_file, exclamation!, ok!)
//	end if

elseif ll_blocca = 1 then 
	g_mb.messagebox("Omnia","Attenzione, Documento Approvato!! Tutte le modifiche che verranno apportate al documento non saranno memorizzate.")	

	hwnd = handle(parent)	
	li_ris = shellexecuteA(hwnd,'open',is_file,'','',1)
	if li_ris <= 32 then
		FileDelete(is_file)
		g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento " + is_file, exclamation!, ok!)		
	end if
	
else
	if is_modifica_doc = "S" then
		
		if ll_ret = 1 then
			
		end if

		hwnd = handle(parent)	
		li_ris = shellexecuteA(hwnd,'open',is_file,'','',1)
		if li_ris <= 32 then
			FileDelete(is_file)
			g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento " + is_file, exclamation!, ok!)		
		end if		
		// 15/07/2003		
//		li_ris = run(ls_percorso,normal!)	
//		if li_ris < 0 then
//			FileDelete(is_file)
//			messagebox("Attenzione", "Impossibile visualizzare il documento " + is_file, exclamation!, ok!)
//		end if	
	end if
end if
end event

event save;//postevent("salvadati")
end event

type dw_det_documenti_det_2 from uo_cs_xx_dw within w_tes_documenti
integer x = 1829
integer y = 120
integer width = 1600
integer height = 1520
integer taborder = 40
string dataobject = "d_det_documenti_det_2"
boolean border = false
end type

type dw_det_documenti_det_1 from uo_cs_xx_dw within w_tes_documenti
integer x = 1806
integer y = 140
integer width = 2377
integer height = 1480
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_det_documenti_det_1"
boolean border = false
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
00w_tes_documenti.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
10w_tes_documenti.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
