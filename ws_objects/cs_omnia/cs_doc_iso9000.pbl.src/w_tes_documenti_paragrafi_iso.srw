﻿$PBExportHeader$w_tes_documenti_paragrafi_iso.srw
$PBExportComments$Finestra Associazione Documenti a Paragrafi Iso
forward
global type w_tes_documenti_paragrafi_iso from w_cs_xx_principale
end type
type dw_paragrafi_iso_documenti from uo_cs_xx_dw within w_tes_documenti_paragrafi_iso
end type
end forward

global type w_tes_documenti_paragrafi_iso from w_cs_xx_principale
integer width = 2231
integer height = 1140
string title = "Riferimento Paragrafi Normativa"
dw_paragrafi_iso_documenti dw_paragrafi_iso_documenti
end type
global w_tes_documenti_paragrafi_iso w_tes_documenti_paragrafi_iso

type variables
boolean ib_in_new
end variables

event pc_setwindow;call super::pc_setwindow;dw_paragrafi_iso_documenti.set_dw_key("cod_azienda")
dw_paragrafi_iso_documenti.set_dw_key("anno_registrazione")
dw_paragrafi_iso_documenti.set_dw_key("num_registrazione")
dw_paragrafi_iso_documenti.set_dw_options(sqlca, i_openparm, c_default, c_default)

iuo_dw_main = dw_paragrafi_iso_documenti
dw_paragrafi_iso_documenti.ib_proteggi_chiavi = false


end event

on w_tes_documenti_paragrafi_iso.create
int iCurrent
call super::create
this.dw_paragrafi_iso_documenti=create dw_paragrafi_iso_documenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_paragrafi_iso_documenti
end on

on w_tes_documenti_paragrafi_iso.destroy
call super::destroy
destroy(this.dw_paragrafi_iso_documenti)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_paragrafi_iso_documenti,"cod_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo",&
                 "(tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_paragrafi_iso.flag_blocco <> 'S') or (tab_paragrafi_iso.flag_blocco = 'S' and tab_paragrafi_iso.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type dw_paragrafi_iso_documenti from uo_cs_xx_dw within w_tes_documenti_paragrafi_iso
integer x = 23
integer y = 20
integer width = 2149
integer height = 1000
integer taborder = 20
string dataobject = "d_tes_documenti_paragrafi_iso"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_cod_documento, ll_anno_registrazione ,ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "anno_registrazione")) or GetItemnumber(l_Idx, "anno_registrazione") = 0 THEN
      SetItem(l_Idx, "anno_registrazione", ll_anno_registrazione)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "num_registrazione")) or GetItemnumber(l_Idx, "num_registrazione") = 0 THEN
      SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
   END IF
NEXT


end event

event updatestart;call super::updatestart;long ll_cont, ll_anno_registrazione ,ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
SELECT count(flag_riferimento_primario)  
INTO   :ll_cont  
FROM   tes_documenti_paragrafi_iso
WHERE  ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		 ( anno_registrazione = :ll_anno_registrazione ) AND  
		 ( num_registrazione = :ll_num_registrazione ) AND  
		 ( flag_riferimento_primario = 'S' )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante la verifica esistenza riferimento primario: " + sqlca.sqlerrtext, Information!)
end if		
if (isnull(ll_cont) or (ll_cont = 0)) and (getitemstring(this.getrow(),"flag_riferimento_primario") <> "S") then
	g_mb.messagebox("OMNIA", "Attenzione: per ogni documento deve esistere un riferimento primario",StopSign!)
	pcca.error = c_valfailed		
end if   


if getitemstring(this.getrow(),"flag_riferimento_primario") = "S" then
	if ll_cont > 0 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
else
	if ll_cont > 1 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_anno_registrazione ,ll_num_registrazione


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione,ll_num_registrazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

