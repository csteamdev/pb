﻿$PBExportHeader$w_tes_documenti_privilegi.srw
forward
global type w_tes_documenti_privilegi from window
end type
type tab_1 from tab within w_tes_documenti_privilegi
end type
type tabpage_1 from userobject within tab_1
end type
type cb_rimuovi from commandbutton within tabpage_1
end type
type cb_aggiungi from commandbutton within tabpage_1
end type
type dw_ext_selezione_gruppi from datawindow within tabpage_1
end type
type dw_gruppi_privilegi_lista from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
cb_rimuovi cb_rimuovi
cb_aggiungi cb_aggiungi
dw_ext_selezione_gruppi dw_ext_selezione_gruppi
dw_gruppi_privilegi_lista dw_gruppi_privilegi_lista
end type
type tabpage_2 from userobject within tab_1
end type
type dw_documenti_privilegi from datawindow within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_documenti_privilegi dw_documenti_privilegi
end type
type tab_1 from tab within w_tes_documenti_privilegi
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
type cb_annulla from commandbutton within w_tes_documenti_privilegi
end type
type cb_ok from commandbutton within w_tes_documenti_privilegi
end type
end forward

global type w_tes_documenti_privilegi from window
integer width = 3511
integer height = 1384
boolean titlebar = true
string title = "Privilegi Documento"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
tab_1 tab_1
cb_annulla cb_annulla
cb_ok cb_ok
end type
global w_tes_documenti_privilegi w_tes_documenti_privilegi

type variables
long il_anno_registrazione, il_num_registrazione
end variables

event open;string ls_cod_gruppo,ls_des_gruppo_privilegio
long ll_riga

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

tab_1.tabpage_1.dw_gruppi_privilegi_lista.settransobject(sqlca)
tab_1.tabpage_1.dw_gruppi_privilegi_lista.retrieve(s_cs_xx.cod_azienda)
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.cod_gruppo_privilegio.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.des_gruppo_privilegio.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.flag_creazione.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.flag_verifica.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.flag_approvazione.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.flag_validazione.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.flag_lettura.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.flag_modifica.tabsequence = 0
tab_1.tabpage_1.dw_gruppi_privilegi_lista.object.flag_elimina.tabsequence = 0


tab_1.tabpage_2.dw_documenti_privilegi.settransobject(sqlca)
tab_1.tabpage_2.dw_documenti_privilegi.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)


declare cu_privilegi cursor for
select cod_gruppo_privilegio
from   tes_documenti_privilegi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione
group by cod_gruppo_privilegio;

open cu_privilegi;

tab_1.tabpage_1.dw_ext_selezione_gruppi.reset()
do while 1=1
	fetch cu_privilegi into :ls_cod_gruppo;
	if sqlca.sqlcode <> 0 then exit
	
	ll_riga = tab_1.tabpage_1.dw_ext_selezione_gruppi.insertrow(0)
	tab_1.tabpage_1.dw_ext_selezione_gruppi.setitem(ll_riga, "cod_gruppo", ls_cod_gruppo)
	select des_gruppo_privilegio
	into   :ls_des_gruppo_privilegio
   from   tab_gruppi_privilegi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_gruppo_privilegio = :ls_cod_gruppo;
	tab_1.tabpage_1.dw_ext_selezione_gruppi.setitem(ll_riga, "des_gruppo", ls_des_gruppo_privilegio)
loop
close cu_privilegi;
commit;
end event

on w_tes_documenti_privilegi.create
this.tab_1=create tab_1
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.Control[]={this.tab_1,&
this.cb_annulla,&
this.cb_ok}
end on

on w_tes_documenti_privilegi.destroy
destroy(this.tab_1)
destroy(this.cb_annulla)
destroy(this.cb_ok)
end on

type tab_1 from tab within w_tes_documenti_privilegi
integer x = 5
integer y = 4
integer width = 3465
integer height = 1180
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3429
integer height = 1056
long backcolor = 12632256
string text = "Aggiungi / Rimuovi Gruppi"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
cb_rimuovi cb_rimuovi
cb_aggiungi cb_aggiungi
dw_ext_selezione_gruppi dw_ext_selezione_gruppi
dw_gruppi_privilegi_lista dw_gruppi_privilegi_lista
end type

on tabpage_1.create
this.cb_rimuovi=create cb_rimuovi
this.cb_aggiungi=create cb_aggiungi
this.dw_ext_selezione_gruppi=create dw_ext_selezione_gruppi
this.dw_gruppi_privilegi_lista=create dw_gruppi_privilegi_lista
this.Control[]={this.cb_rimuovi,&
this.cb_aggiungi,&
this.dw_ext_selezione_gruppi,&
this.dw_gruppi_privilegi_lista}
end on

on tabpage_1.destroy
destroy(this.cb_rimuovi)
destroy(this.cb_aggiungi)
destroy(this.dw_ext_selezione_gruppi)
destroy(this.dw_gruppi_privilegi_lista)
end on

type cb_rimuovi from commandbutton within tabpage_1
integer x = 1687
integer y = 460
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<< Rimuovi"
end type

event clicked;if dw_ext_selezione_gruppi.getrow() > 0 then
	dw_ext_selezione_gruppi.deleterow(dw_ext_selezione_gruppi.getrow())
end if
end event

type cb_aggiungi from commandbutton within tabpage_1
integer x = 1682
integer y = 320
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi >>"
end type

event clicked;long ll_riga
string ls_codice, ls_descrizione

ls_codice = tab_1.tabpage_1.dw_gruppi_privilegi_lista.getitemstring(tab_1.tabpage_1.dw_gruppi_privilegi_lista.getrow(),"cod_gruppo_privilegio")
ls_descrizione = tab_1.tabpage_1.dw_gruppi_privilegi_lista.getitemstring(tab_1.tabpage_1.dw_gruppi_privilegi_lista.getrow(),"des_gruppo_privilegio")

if dw_ext_selezione_gruppi.rowcount() > 0 then
	if dw_ext_selezione_gruppi.find("cod_gruppo = '" + ls_codice + "'", 1 , dw_ext_selezione_gruppi.rowcount()) > 0 then
		g_mb.messagebox("OMNIA","Elemento già inserito")
		return
	end if
end if

ll_riga = dw_ext_selezione_gruppi.insertrow(0)
dw_ext_selezione_gruppi.setitem(ll_riga, "cod_gruppo", ls_codice)
dw_ext_selezione_gruppi.setitem(ll_riga, "des_gruppo", ls_descrizione)
dw_ext_selezione_gruppi.accepttext()

end event

type dw_ext_selezione_gruppi from datawindow within tabpage_1
integer x = 2071
integer y = 8
integer width = 1349
integer height = 1040
integer taborder = 30
string title = "none"
string dataobject = "d_ext_selezione_gruppi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gruppi_privilegi_lista from datawindow within tabpage_1
integer x = 5
integer y = 8
integer width = 1659
integer height = 1040
integer taborder = 20
string title = "none"
string dataobject = "d_tab_gruppi_privilegi_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3429
integer height = 1056
long backcolor = 12632256
string text = "Visualizza Privilegi"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_documenti_privilegi dw_documenti_privilegi
end type

on tabpage_2.create
this.dw_documenti_privilegi=create dw_documenti_privilegi
this.Control[]={this.dw_documenti_privilegi}
end on

on tabpage_2.destroy
destroy(this.dw_documenti_privilegi)
end on

type dw_documenti_privilegi from datawindow within tabpage_2
integer y = 4
integer width = 3429
integer height = 1048
integer taborder = 40
string title = "none"
string dataobject = "d_tes_documenti_privilegi"
boolean hscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_annulla from commandbutton within w_tes_documenti_privilegi
integer x = 3104
integer y = 1196
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_tes_documenti_privilegi
integer x = 2715
integer y = 1196
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;string ls_cod_gruppo, ls_des_gruppo
long ll_riga, ll_num_righe, ll_i


ll_num_righe = tab_1.tabpage_1.dw_ext_selezione_gruppi.rowcount()

delete from tes_documenti_privilegi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;


for ll_i = 1 to ll_num_righe
	ls_cod_gruppo = tab_1.tabpage_1.dw_ext_selezione_gruppi.getitemstring(ll_i, "cod_gruppo")
	ls_des_gruppo = tab_1.tabpage_1.dw_ext_selezione_gruppi.getitemstring(ll_i, "des_gruppo")
	
	INSERT INTO tes_documenti_privilegi  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  cod_gruppo_privilegio,   
			  cod_mansionario )  
	select cod_azienda,
	       :il_anno_registrazione,
			 :il_num_registrazione,
			 cod_gruppo_privilegio,
			 cod_mansionario
	from   tab_gruppi_privilegi_man
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_gruppo_privilegio = :ls_cod_gruppo;
next

commit;
close(parent)
end event

