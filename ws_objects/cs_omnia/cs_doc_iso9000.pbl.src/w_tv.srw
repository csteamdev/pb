﻿$PBExportHeader$w_tv.srw
forward
global type w_tv from window
end type
type cb_2 from commandbutton within w_tv
end type
type plb_1 from picturelistbox within w_tv
end type
type cb_1 from commandbutton within w_tv
end type
type ole_1 from olecontrol within w_tv
end type
type cb_modifica from commandbutton within w_tv
end type
type cb_refresh from commandbutton within w_tv
end type
type cb_nuovo_doc from commandbutton within w_tv
end type
type cb_elimina from commandbutton within w_tv
end type
type cb_inserisci from commandbutton within w_tv
end type
type tv_1 from treeview within w_tv
end type
type s_chiave_doc from structure within w_tv
end type
end forward

type s_chiave_doc from structure
	long		padre
	long		figlio
	string		tipo
	string		descrizione
end type

global type w_tv from window
integer width = 3008
integer height = 2560
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
long backcolor = 67108864
event nuovo_menu ( )
cb_2 cb_2
plb_1 plb_1
cb_1 cb_1
ole_1 ole_1
cb_modifica cb_modifica
cb_refresh cb_refresh
cb_nuovo_doc cb_nuovo_doc
cb_elimina cb_elimina
cb_inserisci cb_inserisci
tv_1 tv_1
end type
global w_tv w_tv

forward prototypes
public function integer wf_elimina ()
public function integer wf_fine_modifica (long fl_handle, string fs_label)
public function integer wf_imposta_tv (long fl_cod_padre, long fl_handle)
public function integer wf_inizio ()
public function integer wf_modifica ()
public function integer wf_nuovo_documento ()
public function integer wf_nuovo_menu ()
public function integer wf_trova_max (ref long fl_max)
public function integer wf_refresh_ole (long fl_handle)
public function integer update_blob ()
end prototypes

event nuovo_menu();wf_nuovo_menu()
end event

public function integer wf_elimina ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle
string ls_tipo
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo

if g_mb.messagebox("Omnia","Sei Sicuro?",Question!,YesNo!,2) = 2 then return 0

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","Pagliaccio! Devi selezionare almeno un elemento.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.padre
ll_cod_figlio = l_chiave_doc.figlio
ls_tipo = l_chiave_doc.tipo

if ll_cod_padre=0 and ll_cod_figlio = 0 then 
	g_mb.messagebox("Omnia","Non puoi eliminare l'elemento di origine dei documenti di qualità!(Macaco)",stopsign!)
	return 0
end if

select cod_figlio
into   :ll_test_figlio
from   tree_documenti
where  cod_padre=:ll_cod_figlio;

if sqlca.sqlcode <0 and (isnull(ll_test_figlio)) then
	g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

if sqlca.sqlcode = 100 then
	delete from tree_documenti
	where  cod_padre=:ll_cod_padre
	and    cod_figlio=:ll_cod_figlio;
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	tv_1.deleteitem(ll_handle_corrente)
else
	g_mb.messagebox("Omnia","Non è possibile cancellare l'elemento poichè contiene a altri elementi!",stopsign!)
end if

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
tv_1.selectItem(ll_handle_corrente)

commit;
return 0


end function

public function integer wf_fine_modifica (long fl_handle, string fs_label);long ll_handle_corrente,ll_cod_padre,ll_cod_figlio
string ls_tipo,ls_descrizione
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo


tv_1.GetItem (fl_handle, tvi_campo )

l_chiave_doc = tvi_campo.data
ls_descrizione= fs_label

ll_cod_padre = l_chiave_doc.padre
ll_cod_figlio = l_chiave_doc.figlio
ls_tipo = l_chiave_doc.tipo
l_chiave_doc.descrizione = ls_descrizione

update tree_documenti
set    descrizione=:ls_descrizione
where  cod_padre=:ll_cod_padre
and    cod_figlio=:ll_cod_figlio;

if sqlca.sqlcode <0 then
	g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

commit;

return 0


end function

public function integer wf_imposta_tv (long fl_cod_padre, long fl_handle);string  ls_tipo,ls_descrizione
long    ll_num_figli,ll_handle,ll_num_righe,ll_cod_padre,ll_cod_figlio,ll_test_figlio
integer li_num_priorita,li_risposta
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo
datastore lds_righe_tree_doc

ll_num_figli = 1

lds_righe_tree_doc = Create DataStore

lds_righe_tree_doc.DataObject = "d_tree_documenti"

lds_righe_tree_doc.SetTransObject(sqlca)

ll_num_righe = lds_righe_tree_doc.Retrieve(fl_cod_padre)

for ll_num_figli=1 to ll_num_righe
	l_chiave_doc.padre = fl_cod_padre
	l_chiave_doc.figlio = lds_righe_tree_doc.getitemnumber(ll_num_figli,"cod_figlio")
	l_chiave_doc.tipo = lds_righe_tree_doc.getitemstring(ll_num_figli,"tipo")
	l_chiave_doc.descrizione = lds_righe_tree_doc.getitemstring(ll_num_figli,"descrizione")

	if isnull(ls_descrizione) then ls_descrizione = ""
	
	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = l_chiave_doc
	tvi_campo.label =l_chiave_doc.descrizione

	select cod_figlio 
	into   :ll_test_figlio
	from   tree_documenti
	where  cod_padre=:l_chiave_doc.figlio;

	if sqlca.sqlcode = 100  then
		if l_chiave_doc.tipo = 'M' then
			tvi_campo.pictureindex = 3
			tvi_campo.selectedpictureindex = 2
			tvi_campo.overlaypictureindex = 2
			ll_handle=tv_1.insertitemlast(fl_handle, tvi_campo)		
		else
			tvi_campo.pictureindex = 4
			tvi_campo.selectedpictureindex = 4
			tvi_campo.overlaypictureindex = 4
			ll_handle=tv_1.insertitemlast(fl_handle, tvi_campo)
		end if
		continue
	else
		tvi_campo.pictureindex = 3
		tvi_campo.selectedpictureindex = 2
		tvi_campo.overlaypictureindex = 2
		ll_handle=tv_1.insertitemlast(fl_handle, tvi_campo)
		setnull(ll_test_figlio)
		li_risposta=wf_imposta_tv(l_chiave_doc.figlio,ll_handle)
	end if

next

destroy(lds_righe_tree_doc)

return 0
end function

public function integer wf_inizio ();long ll_handle
integer li_risposta
treeviewitem tvi_campo
s_chiave_doc l_chiave_doc

tv_1.setredraw(false)
tv_1.deleteitem(0)

l_chiave_doc.padre=0
l_chiave_doc.figlio=0

tvi_campo.itemhandle = 1
tvi_campo.label = "Documenti di Qualità"
tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1
tvi_campo.data = l_chiave_doc


ll_handle=tv_1.insertitemlast(0, tvi_campo)




li_risposta =wf_imposta_tv(0,ll_handle)

tv_1.expandall(1)
tv_1.setredraw(true)
commit;
return 0
end function

public function integer wf_modifica ();long ll_handle_corrente

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

tv_1.editlabel(ll_handle_corrente)

return 0
end function

public function integer wf_nuovo_documento ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_max,ll_ultimo_handle
string ls_tipo

s_chiave_doc l_chiave_doc
treeviewitem tvi_campo

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","Pagliaccio! Devi selezionare almeno un elemento.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.padre
ll_cod_figlio = l_chiave_doc.figlio
ls_tipo = l_chiave_doc.tipo

if ls_tipo = 'D' then
	g_mb.messagebox("Omnia","Attenzione! I documenti non possono contenere al loro interno altri documenti!",stopsign!)
	return 0
end if

l_chiave_doc.padre=ll_cod_figlio

ll_risposta=wf_trova_max(ll_max)

if ll_max = 0 then 
	ll_max=1
else
	ll_max++
end if

l_chiave_doc.figlio = ll_max
l_chiave_doc.tipo = "D"

insert into tree_documenti
	(cod_padre,
	 cod_figlio,
	 tipo,
	 descrizione)
values
	(:l_chiave_doc.padre,
	 :l_chiave_doc.figlio,
	 "D",
	 "");

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return 0
end if

tvi_campo.itemhandle = ll_handle_corrente 
tvi_campo.data = l_chiave_doc
tvi_campo.label = ""
tvi_campo.pictureindex = 4
tvi_campo.selectedpictureindex = 4
tvi_campo.overlaypictureindex = 4
ll_ultimo_handle=tv_1.InsertItemLast ( ll_handle_corrente, tvi_campo )
tv_1.selectItem(ll_ultimo_handle)
tv_1.EditLabel (ll_ultimo_handle )

commit;
return 0
end function

public function integer wf_nuovo_menu ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_max,ll_ultimo_handle
string ls_tipo
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","Pagliaccio! Devi selezionare almeno un elemento.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.padre
ll_cod_figlio = l_chiave_doc.figlio
ls_tipo = l_chiave_doc.tipo

if ls_tipo = 'D' then
	g_mb.messagebox("Omnia","Attenzione! I documenti non possono contenere al loro interno dei menù!",stopsign!)
	return 0
end if

l_chiave_doc.padre=ll_cod_figlio

ll_risposta=wf_trova_max(ll_max)

if isnull(ll_max) or ll_max = 0 then 
	ll_max=1
else
	ll_max++
end if

l_chiave_doc.figlio = ll_max
l_chiave_doc.tipo = 'M'

insert into tree_documenti
	(cod_padre,
	 cod_figlio,
	 tipo,
	 descrizione)
values
	(:l_chiave_doc.padre,
	 :l_chiave_doc.figlio,
	 "M",
	 "");

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return 0
end if

tvi_campo.itemhandle = ll_handle_corrente 
tvi_campo.data = l_chiave_doc
tvi_campo.label = ""
tvi_campo.pictureindex = 2
tvi_campo.selectedpictureindex = 2
tvi_campo.overlaypictureindex = 2
ll_ultimo_handle=tv_1.InsertItemLast ( ll_handle_corrente, tvi_campo )
tv_1.selectItem(ll_ultimo_handle)
tv_1.EditLabel (ll_ultimo_handle )

commit;
return 0
end function

public function integer wf_trova_max (ref long fl_max);long ll_max_padre,ll_max_figlio


select max(cod_padre)
into   :ll_max_padre
from   tree_documenti;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ll_max_padre) then ll_max_padre = 0

select max(cod_figlio)
into   :ll_max_figlio
from   tree_documenti;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ll_max_figlio) then ll_max_figlio = 0

if ll_max_padre >= ll_max_figlio then
	fl_max=ll_max_padre
else
	fl_max=ll_max_figlio
end if

commit;
return 0
end function

public function integer wf_refresh_ole (long fl_handle);long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle
string ls_tipo, ls_db
blob lbb_blob_documento
integer li_risposta
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo
transaction sqlcb

li_risposta = tv_1.GetItem (fl_handle, tvi_campo )

if li_risposta = -1 then return -1

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.padre
ll_cod_figlio = l_chiave_doc.figlio
ls_tipo = l_chiave_doc.tipo

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)

	selectblob blob_documento
	into   :lbb_blob_documento
	from   tree_documenti
	where  cod_padre=:ll_cod_padre
	and    cod_figlio=:ll_cod_figlio
	using  sqlcb;

	if sqlcb.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlcb.sqlerrtext,stopsign!)
		return 0
		destroy sqlcb;
	end if
	
	destroy sqlcb;
	
else

	selectblob blob_documento
	into   :lbb_blob_documento
	from   tree_documenti
	where  cod_padre=:ll_cod_padre
	and    cod_figlio=:ll_cod_figlio;

	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
end if

ole_1.objectdata = lbb_blob_documento

commit;
return 0
end function

public function integer update_blob ();long ll_risposta,ll_handle_corrente,ll_numero_figli,ll_cod_padre,ll_cod_figlio,ll_test_figlio,ll_handle
string ls_tipo, ls_db
blob lbb_blob_documento
integer li_risposta
s_chiave_doc l_chiave_doc
treeviewitem tvi_campo
transaction sqlcb

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)

if ll_handle_corrente=-1 then
	g_mb.messagebox("Omnia","Selezionare almeno un elemento dell'albero.",stopsign!)
	return 0
end if

tv_1.GetItem (ll_handle_corrente, tvi_campo )

l_chiave_doc = tvi_campo.data

ll_cod_padre = l_chiave_doc.padre
ll_cod_figlio = l_chiave_doc.figlio
ls_tipo = l_chiave_doc.tipo

if ll_cod_padre=0 and ll_cod_figlio = 0 then 
	g_mb.messagebox("Omnia","Non puoi inserire documenti sull'elemento di origine dei documenti di qualità!",stopsign!)
	return 0
end if

if ls_tipo = "M" then
	g_mb.messagebox("Omnia","Non puoi associare oggetti ad un menù ma solo ai documenti!",stopsign!)
	return 0
end if

lbb_blob_documento=ole_1.objectdata

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)

	updateblob tree_documenti
	set    blob_documento=:lbb_blob_documento
	where  cod_padre=:ll_cod_padre
	and    cod_figlio=:ll_cod_figlio
	using  sqlcb;
	
	if sqlcb.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlcb.sqlerrtext,stopsign!)
		destroy sqlcb;
		return 0
	end if
	
	destroy sqlcb;
	
else
	
	updateblob tree_documenti
	set    blob_documento=:lbb_blob_documento
	where  cod_padre=:ll_cod_padre
	and    cod_figlio=:ll_cod_figlio;
	
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if

end if

// fine modifiche

ll_handle_corrente = tv_1.FindItem(CurrentTreeItem!, 0)
li_risposta = tv_1.selectItem(ll_handle_corrente)
tv_1.setfocus()

commit;

return 0

end function

on w_tv.create
this.cb_2=create cb_2
this.plb_1=create plb_1
this.cb_1=create cb_1
this.ole_1=create ole_1
this.cb_modifica=create cb_modifica
this.cb_refresh=create cb_refresh
this.cb_nuovo_doc=create cb_nuovo_doc
this.cb_elimina=create cb_elimina
this.cb_inserisci=create cb_inserisci
this.tv_1=create tv_1
this.Control[]={this.cb_2,&
this.plb_1,&
this.cb_1,&
this.ole_1,&
this.cb_modifica,&
this.cb_refresh,&
this.cb_nuovo_doc,&
this.cb_elimina,&
this.cb_inserisci,&
this.tv_1}
end on

on w_tv.destroy
destroy(this.cb_2)
destroy(this.plb_1)
destroy(this.cb_1)
destroy(this.ole_1)
destroy(this.cb_modifica)
destroy(this.cb_refresh)
destroy(this.cb_nuovo_doc)
destroy(this.cb_elimina)
destroy(this.cb_inserisci)
destroy(this.tv_1)
end on

event open;plb_1.DirList("H:\CS_DOC\MODULI\*.DOT", 0)
wf_inizio()
end event

type cb_2 from commandbutton within w_tv
integer x = 2491
integer y = 1500
integer width = 402
integer height = 112
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "none"
end type

event clicked;string docname,named
integer result
 
 GetFileOpenName("Select File", &
		+ docname, named, "DOC", &
		+ "Text Files (*.*),*.*,")

result = ole_1.InsertFile(docname)

update_blob()
end event

type plb_1 from picturelistbox within w_tv
integer x = 2354
integer y = 860
integer width = 617
integer height = 400
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
end type

event doubleclicked;integer result
string ls_Item

ls_Item = SelectedItem()

result = ole_1.InsertFile("H:\CS_DOC\MODULI\" + ls_item)

update_blob()

end event

type cb_1 from commandbutton within w_tv
integer x = 2354
integer y = 520
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Update Blob"
end type

event clicked;update_blob()
end event

type ole_1 from olecontrol within w_tv
event salvadati ( )
integer x = 23
integer y = 1400
integer width = 2309
integer height = 1060
integer taborder = 50
string dragicon = "D:\TR\BOOK.ICO"
long backcolor = 16777215
string pointer = "HyperLink!"
boolean focusrectangle = false
boolean isdragtarget = true
string binarykey = "w_tv.win"
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
omlinkupdateoptions linkupdateoptions = linkupdatemanual!
sizemode sizemode = clip!
end type

event salvadati();long ll_t

for ll_t = 1 to 200000
next


update_blob()

end event

event doubleclicked;ole_1.activate(offsite!)
end event

event datachange;//postevent(save!)
end event

event save;postevent("salvadati")
end event

type cb_modifica from commandbutton within w_tv
integer x = 2354
integer y = 320
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Modifica"
end type

event clicked;wf_modifica()
end event

type cb_refresh from commandbutton within w_tv
integer x = 2354
integer y = 420
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Refresh"
end type

event clicked;wf_inizio()
end event

type cb_nuovo_doc from commandbutton within w_tv
integer x = 2354
integer y = 120
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nuovo &Doc."
end type

event clicked;wf_nuovo_documento()
end event

type cb_elimina from commandbutton within w_tv
integer x = 2354
integer y = 220
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;wf_elimina()
end event

type cb_inserisci from commandbutton within w_tv
integer x = 2354
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo Menù"
end type

event clicked;wf_nuovo_menu()
end event

type tv_1 from treeview within w_tv
integer x = 23
integer y = 20
integer width = 2309
integer height = 1360
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "Times New Roman"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean editlabels = true
boolean disabledragdrop = false
boolean hideselection = false
string picturename[] = {"D:\TR\BOOK.ICO","D:\TR\MENUVOCA.BMP","D:\TR\MENUVOCC.BMP","D:\TR\MOD.BMP"}
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event key;choose case key
	case keydelete! 
		wf_elimina()
		
	case keyf2!
		wf_modifica()
		
end choose

end event

event rightclicked;m_tree_documenti popmenu

selectitem(handle)

popmenu = CREATE m_tree_documenti

popmenu.PopMenu (PointerX(), PointerY())
end event

event endlabeledit;treeviewitem tvi

getitem(handle,tvi)

if isnull(newtext) then 
	if tvi.label="" then
		g_mb.messagebox("Omnia","Inserire un nome per l'elemento!",stopsign!)
		editlabel(handle)	
		return
	else
		return
	end if
end if
	

if newtext = "" then
	tvi.label = ""
	setitem(handle,tvi)
	g_mb.messagebox("Omnia","Inserire un nome per l'elemento!",stopsign!)
	editlabel(handle)
	return
end if

wf_fine_modifica(handle,newtext)
end event

event clicked;selectitem(handle)
end event

event selectionchanged;integer li_risposta
li_risposta = wf_refresh_ole(newhandle)

if li_risposta = -1 then
	selectItem(oldhandle)
end if
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
07w_tv.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
17w_tv.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
