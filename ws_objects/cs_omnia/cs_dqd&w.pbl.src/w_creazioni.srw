﻿$PBExportHeader$w_creazioni.srw
$PBExportComments$Window di creazione documenti
forward
global type w_creazioni from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_creazioni
end type
type st_1 from statictext within w_creazioni
end type
type st_2 from statictext within w_creazioni
end type
type st_3 from statictext within w_creazioni
end type
type st_4 from statictext within w_creazioni
end type
type st_6 from statictext within w_creazioni
end type
type rb_1 from radiobutton within w_creazioni
end type
type rb_2 from radiobutton within w_creazioni
end type
type st_7 from statictext within w_creazioni
end type
type st_9 from statictext within w_creazioni
end type
type lb_1 from listbox within w_creazioni
end type
type st_8 from statictext within w_creazioni
end type
type gb_1 from groupbox within w_creazioni
end type
type ddlb_1 from dropdownlistbox within w_creazioni
end type
type cb_4 from cb_apri_manuale within w_creazioni
end type
type st_13 from statictext within w_creazioni
end type
type ddlb_livello_doc from dropdownlistbox within w_creazioni
end type
type st_15 from statictext within w_creazioni
end type
type st_16 from statictext within w_creazioni
end type
type em_edizione from editmask within w_creazioni
end type
type st_21 from statictext within w_creazioni
end type
type em_revisione from editmask within w_creazioni
end type
type cb_esterni from commandbutton within w_creazioni
end type
type em_gg_validita from editmask within w_creazioni
end type
type em_nome_doc from editmask within w_creazioni
end type
type em_des_documento from editmask within w_creazioni
end type
type gb_3 from groupbox within w_creazioni
end type
type st_5 from statictext within w_creazioni
end type
type cbx_modelli_default from checkbox within w_creazioni
end type
end forward

global type w_creazioni from w_cs_xx_principale
integer width = 3141
integer height = 1220
string title = "Creazione Documenti"
cb_2 cb_2
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
st_6 st_6
rb_1 rb_1
rb_2 rb_2
st_7 st_7
st_9 st_9
lb_1 lb_1
st_8 st_8
gb_1 gb_1
ddlb_1 ddlb_1
cb_4 cb_4
st_13 st_13
ddlb_livello_doc ddlb_livello_doc
st_15 st_15
st_16 st_16
em_edizione em_edizione
st_21 st_21
em_revisione em_revisione
cb_esterni cb_esterni
em_gg_validita em_gg_validita
em_nome_doc em_nome_doc
em_des_documento em_des_documento
gb_3 gb_3
st_5 st_5
cbx_modelli_default cbx_modelli_default
end type
global w_creazioni w_creazioni

type variables
string i_password
string i_percorso_word
string i_percorso_doc
string i_percorso_mod
string i_percorso_com
integer i_num_max_edizioni
end variables

forward prototypes
public function integer wf_azzera_campi ()
public function integer wf_crea_path_documento ()
public function integer wf_inserisci ()
end prototypes

public function integer wf_azzera_campi ();em_nome_doc.text = ""
em_des_documento.text = ""
em_gg_validita.text = "0"
em_edizione.text = "1"
em_revisione.text = "0"


return 0
end function

public function integer wf_crea_path_documento ();if long(em_revisione.text) < 10 then
   st_6.text = i_percorso_doc + "\" + em_nome_doc.text + "." + em_edizione.text + "0" + em_revisione.text
else
   st_6.text = i_percorso_doc + "\" + em_nome_doc.text + "." + em_edizione.text + em_revisione.text
end if	
return 0
end function

public function integer wf_inserisci ();string ls_risposta,ls_nome,ls_cognome,ls_valore
integer li_versione_word,li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vew", ls_valore)

li_versione_word = integer (ls_valore)

if li_versione_word = 6 then
	ls_risposta=f_word("startofdocument(0)",i_percorso_word)	  
else
	ls_risposta=f_word("startofdocument(0)",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("MostraIntestazione",i_percorso_word)	  
else
	ls_risposta=f_word("viewheader",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if
			
if li_versione_word = 6 then
	ls_risposta=f_word("CellaSuccessiva",i_percorso_word)	  
else
	ls_risposta=f_word("Nextcell",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("CellaSuccessiva",i_percorso_word)	  
else
	ls_risposta=f_word("Nextcell",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("FineRiga(0)",i_percorso_word)
else
	ls_risposta=f_word("endofrow(0)",i_percorso_word)
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("CarattereDestra(4, 0)",i_percorso_word)	  
else
	ls_risposta=f_word("charright(4, 0)",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("FineLinea(1)",i_percorso_word)	  
else
	ls_risposta=f_word("endofline(1)",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("Inserisci " + char(34) + " " + em_edizione.text + char(34),i_percorso_word)	  
else
	ls_risposta=f_word("Insert " + char(34) + " " + em_edizione.text + char(34),i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("RigaGiù(3, 0)",i_percorso_word)	  
else
	ls_risposta=f_word("Linedown(3, 0)",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("CarattereSinistra(4, 1)",i_percorso_word)	  
else
	ls_risposta=f_word("charleft(4, 1)",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("Inserisci " + char(34) + " " + em_revisione.text + char(34),i_percorso_word)	  
else
	ls_risposta=f_word("Insert " + char(34) + " " + em_revisione.text + char(34),i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("RigaGiù(1, 0)",i_percorso_word)	  
else
	ls_risposta=f_word("Linedown(1, 0)",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("FineLinea(1)",i_percorso_word)	  
else
	ls_risposta=f_word("endofline(1)",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("Inserisci " + char(34) + " " + em_nome_doc.text + char(34),i_percorso_word)	  
else
	ls_risposta=f_word("Insert " + char(34) + " " + em_nome_doc.text + char(34),i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if li_versione_word = 6 then
	ls_risposta=f_word("MostraIntestazione",i_percorso_word)	  
else
	ls_risposta=f_word("viewheader",i_percorso_word)	  
end if

if ls_risposta = "no" then	
	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	rollback;
	return 0
end if

if ddlb_livello_doc.text = "A" or ddlb_livello_doc.text ="B" then
	
	if li_versione_word = 6 then
		ls_risposta=f_word("modificasegnalibro.nome=" + char(34) + "inizio_tabella" + char(34) + ",.vaia",i_percorso_word)
	else
		ls_risposta=f_word("editbookmark.name=" + char(34) + "inizio_tabella" + char(34) + ",.goto",i_percorso_word)
	end if

	if ls_risposta = "no" then	
		rollback;
		return 0
	end if
	
	if li_versione_word = 6 then
		ls_risposta=f_word("FineLinea(1)",i_percorso_word)	  
	else
		ls_risposta=f_word("endofline(1)",i_percorso_word)	  
	end if

	if ls_risposta = "no" then	
		g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		rollback;
		return 0
	end if
	
	SELECT nome,
			 cognome
	INTO	:ls_nome,
			:ls_cognome
	FROM  mansionari 
	WHERE cod_azienda=:s_cs_xx.cod_azienda
	AND   cod_utente =:s_cs_xx.cod_utente;
	
	if li_versione_word = 6 then
		ls_risposta=f_word("Inserisci " + char(34) + " " + ls_nome + "-" + ls_cognome + char(34),i_percorso_word)	  
	else
		ls_risposta=f_word("Insert " + char(34) + " " + ls_nome + "-" + ls_cognome + char(34),i_percorso_word)	  
	end if

	if ls_risposta = "no" then	
		g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		rollback;
		return 0
	end if
	
	if li_versione_word = 6 then
		ls_risposta=f_word("RigaGiù(1, 0)",i_percorso_word)	  
	else
		ls_risposta=f_word("linedown(1, 0)",i_percorso_word)	  
	end if

	if ls_risposta = "no" then	
		g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		rollback;
		return 0
	end if
	
	if li_versione_word = 6 then
		ls_risposta=f_word("FineLinea(1)",i_percorso_word)	  
	else
		ls_risposta=f_word("Endofline(1)",i_percorso_word)	  
	end if

	if ls_risposta = "no" then	
		g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		rollback;
		return 0
	end if

	if li_versione_word = 6 then	
		ls_risposta=f_word("Inserisci " + char(34) + " " + string(today()) + char(34),i_percorso_word)	  
	else
		ls_risposta=f_word("Insert " + char(34) + " " + string(today()) + char(34),i_percorso_word)	  
	end if

	if ls_risposta = "no" then	
		g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		rollback;
		return 0
	end if
	
end if

return 0
end function

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_1, &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")

end event

event pc_setwindow;call super::pc_setwindow;string ls_risposta
string ls_autoriz_liv_doc

Set_W_Options(c_NoEnablePopup)
rb_1.checked=true
ls_risposta=f_parametri(i_password,i_percorso_word,i_percorso_doc,i_percorso_mod, i_num_max_edizioni,i_percorso_com)

lb_1.DirList(i_percorso_mod+"\*.dot", 0)
lb_1.SelectItem(1)
lb_1.postevent("SelectionChanged")
em_edizione.text = "1"
em_revisione.text = "0"
em_gg_validita.text = "0"

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT autoriz_liv_doc
//INTO	 :ls_autoriz_liv_doc
//FROM   mansionari 
//WHERE  cod_azienda=:s_cs_xx.cod_azienda
//AND    cod_utente=:s_cs_xx.cod_utente;

//if sqlca.sqlcode= 100 then
//	g_mb.messagebox("Omnia","Attenzione! L'utente non ha un mansionario abilitato",exclamation!)
//else

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_autoriz_liv_doc = luo_mansionario.uof_get_privilege_string(luo_mansionario.livello_doc)

if isnull(luo_mansionario.uof_get_cod_mansionario()) then
	g_mb.messagebox("Omnia","Attenzione! L'utente non ha un mansionario abilitato",exclamation!)
else
	choose case ls_autoriz_liv_doc
		case "A"
			ddlb_livello_doc.AddItem ("A")
			ddlb_livello_doc.AddItem ("B")
			ddlb_livello_doc.AddItem ("C")
	
		case "B"
			ddlb_livello_doc.AddItem ("B")
			ddlb_livello_doc.AddItem ("C")
	
		case "C"
			ddlb_livello_doc.AddItem ("C")
			
	end choose			
end if
end event

on w_creazioni.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.st_6=create st_6
this.rb_1=create rb_1
this.rb_2=create rb_2
this.st_7=create st_7
this.st_9=create st_9
this.lb_1=create lb_1
this.st_8=create st_8
this.gb_1=create gb_1
this.ddlb_1=create ddlb_1
this.cb_4=create cb_4
this.st_13=create st_13
this.ddlb_livello_doc=create ddlb_livello_doc
this.st_15=create st_15
this.st_16=create st_16
this.em_edizione=create em_edizione
this.st_21=create st_21
this.em_revisione=create em_revisione
this.cb_esterni=create cb_esterni
this.em_gg_validita=create em_gg_validita
this.em_nome_doc=create em_nome_doc
this.em_des_documento=create em_des_documento
this.gb_3=create gb_3
this.st_5=create st_5
this.cbx_modelli_default=create cbx_modelli_default
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_3
this.Control[iCurrent+5]=this.st_4
this.Control[iCurrent+6]=this.st_6
this.Control[iCurrent+7]=this.rb_1
this.Control[iCurrent+8]=this.rb_2
this.Control[iCurrent+9]=this.st_7
this.Control[iCurrent+10]=this.st_9
this.Control[iCurrent+11]=this.lb_1
this.Control[iCurrent+12]=this.st_8
this.Control[iCurrent+13]=this.gb_1
this.Control[iCurrent+14]=this.ddlb_1
this.Control[iCurrent+15]=this.cb_4
this.Control[iCurrent+16]=this.st_13
this.Control[iCurrent+17]=this.ddlb_livello_doc
this.Control[iCurrent+18]=this.st_15
this.Control[iCurrent+19]=this.st_16
this.Control[iCurrent+20]=this.em_edizione
this.Control[iCurrent+21]=this.st_21
this.Control[iCurrent+22]=this.em_revisione
this.Control[iCurrent+23]=this.cb_esterni
this.Control[iCurrent+24]=this.em_gg_validita
this.Control[iCurrent+25]=this.em_nome_doc
this.Control[iCurrent+26]=this.em_des_documento
this.Control[iCurrent+27]=this.gb_3
this.Control[iCurrent+28]=this.st_5
this.Control[iCurrent+29]=this.cbx_modelli_default
end on

on w_creazioni.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_6)
destroy(this.rb_1)
destroy(this.rb_2)
destroy(this.st_7)
destroy(this.st_9)
destroy(this.lb_1)
destroy(this.st_8)
destroy(this.gb_1)
destroy(this.ddlb_1)
destroy(this.cb_4)
destroy(this.st_13)
destroy(this.ddlb_livello_doc)
destroy(this.st_15)
destroy(this.st_16)
destroy(this.em_edizione)
destroy(this.st_21)
destroy(this.em_revisione)
destroy(this.cb_esterni)
destroy(this.em_gg_validita)
destroy(this.em_nome_doc)
destroy(this.em_des_documento)
destroy(this.gb_3)
destroy(this.st_5)
destroy(this.cbx_modelli_default)
end on

type cb_2 from commandbutton within w_creazioni
integer x = 2327
integer y = 1020
integer width = 361
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera Doc."
end type

event clicked;datetime ld_oggi
string ls_risposta,ls_resp_divisione,ls_nome_divisione,ls_area, & 
		 ls_path_documento,ls_nome_modello,ls_flag_doc_automatico,ls_valore
decimal ldd_num_versione,ldd_num_edizione,li_valido_per,ll_progr
integer li_versione_word,li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vew", ls_valore)

li_versione_word = integer(ls_valore)

ls_flag_doc_automatico="N"

if cbx_modelli_default.checked = true then
	choose case ddlb_livello_doc.text
		case "A"
			select stringa
			into   :ls_nome_modello
			from   parametri_omnia
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_parametro ='MLA';
			
		case "B"
			select stringa
			into   :ls_nome_modello
			from   parametri_omnia
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_parametro ='MLB';
			
		case "C"
			select stringa
			into   :ls_nome_modello
			from   parametri_omnia
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_parametro ='MLC';
		case else
			g_mb.messagebox("Omnia","Attenzione!, Non è possibile creare un documento senza specificarne il livello")
			return			
	end choose

	if ls_nome_modello="" or isnull(ls_nome_modello) then
		g_mb.messagebox("OMNIA","Attenzione! Mancano i parametri MLA, MLB e MLC in tabella parametri OMNIA, verificare la tabella o disinserire l'opzione usa modelli predefiniti in creazione.",exclamation!)
		return
	else
		st_4.text = ls_nome_modello
		ls_flag_doc_automatico="S"
	end if

end if

ldd_num_versione=long(em_edizione.text)
ldd_num_edizione=long(em_revisione.text)

if isnull(i_password) or len(i_password) < 1 or &
   isnull(i_percorso_word) or len(i_percorso_word) < 1 or &
	isnull(i_percorso_doc)  or len(i_percorso_doc)  < 1 or &
	isnull(i_percorso_mod)  or len(i_percorso_mod)  < 1 or &
	isnull(i_percorso_com)  or len(i_percorso_com)  < 1 then
	g_mb.messagebox("Omnia","Attenzione! Mancano uno o più parametri indispensabili per la gestione documenti (password, percorsi, numero edizioni). Controllare in parametri azienda inserendo il/i parametro/i mancante/i",exclamation!)
	return
end if

if len(lb_1.selecteditem()) < 1 then 
	g_mb.messagebox("Omnia","Non vi sono documenti o modelli utilizzabili. Controllare i parametri PTD e PTM, assicurandosi che contegano il percorso corretto.",exclamation!)
	return
end if

if len(em_nome_doc.text) < 1 then
	g_mb.messagebox("Omnia","Assegnare un nome al documento che si desidera creare", StopSign!)
	return
end if	

if len(st_4.text) < 1  then
	g_mb.messagebox("Omnia","Selezionare il modello o documento da cui creare il documento", StopSign!)
	return
end if	

if len(st_6.text) < 1 then
	g_mb.messagebox("Omnia","Il percorso del documento da creare non è indicato", StopSign!)
	return
end if	

if fileexists(st_6.text) then
	g_mb.messagebox("Omnia","Attenzione! Un documento con questo nome risulta già presente nella directory dei documenti di OMNIA pur non risultando in Archivio. Cambiare la directory dei documenti o eliminare il file.")	
	return
end if

ls_risposta=f_ctrl_doc_2(em_nome_doc.text)
if ls_risposta <> "nessuno" then
	g_mb.messagebox("Omnia","Attenzione! Un documento con questo nome risulta già in archivio.")
	return
end if

ls_risposta=f_ctrl_doc_3(em_nome_doc.text)
if ls_risposta <> "nessuno" then
	g_mb.messagebox("Omnia","Attenzione! Un documento con questo nome risulta già in archivio.")
	return
end if

if len(em_des_documento.text) < 1 then
	g_mb.messagebox("Omnia","Assegnare una descrizione al documento che si desidera creare", StopSign!)
	return
end if	

if len(ddlb_livello_doc.text) < 1 or isnull(ddlb_livello_doc.text) then
	g_mb.messagebox("Omnia","Attenzione!, Non è possibile creare un documento senza specificarne il livello")
	return
end if

if len(em_gg_validita.text) < 1 or long(em_gg_validita.text) < 0 then
	g_mb.messagebox("Omnia","Il periodo di validità è mancante oppure non impostato correttamente!",StopSign!)
	return
end if
li_valido_per=integer(em_gg_validita.text)

if long(em_edizione.text) < 1 or isnull(long(em_edizione.text)) then
	g_mb.messagebox("Omnia","Attenzione! Impossibile procedere se numero edizione è a zero oppure non è impostato", StopSign!)
	return
end if

if long(em_revisione.text) < 0 or isnull(long(em_revisione.text))  or long(em_revisione.text) > i_num_max_edizioni then
	g_mb.messagebox("Omnia","Attenzione! Numero di revisione documento inammissibile!" , StopSign!)
	return
end if

if len(lb_1.selecteditem()) < 1 then 
	g_mb.messagebox("Omnia","Non vi sono documenti o modelli utilizzabili. Controllare i parametri PTD e PTM, assicurandosi che contegano il percorso corretto.",exclamation!)
	return
end if

ls_area=f_po_selectddlb(ddlb_1)
if len(ls_area) < 1 then
	g_mb.messagebox("Omnia","Attenzione!, Non è possibile creare un documento senza specificare un'area di appartenenza")
	return
end if

ls_path_documento=st_6.text
SELECT mansionari.cod_resp_divisione, 
		 mansionari.cod_divisione 
INTO:ls_resp_divisione,
    :ls_nome_divisione & 
FROM mansionari 
WHERE (mansionari.cod_azienda=:s_cs_xx.cod_azienda) AND 
	   (mansionari.cod_utente =:s_cs_xx.cod_utente);

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Omnia","Attenzione l'utente non ha un mansionario assegnato un mansionario.",exclamation!)
	return
end if

if st_4.text<>"" and em_nome_doc.text<>"" and st_6.text<>"" then
  
	ld_oggi=datetime(today())
	ll_progr=f_maxprogr()

	INSERT INTO documenti  
      	   ( cod_azienda,   
         	  progr,   
	           nome_documento,   
   	        num_versione,   
      	     num_edizione,   
         	  nome_modello,   
	           emesso_il,   
   	        cod_resp_divisione,   
      	     approvato_da,   
         	  autorizzato_da,   
	           autorizzato_il,   
   	        valido_per,   
      	     validato_da,   
         	  cod_area_aziendale,   
	           flag_uso,
				  flag_recuperato,
				  livello_documento,
				  progr_lista_dist,
				  des_documento,
				  path_documento_esterno,
				  prog_ordinamento_livello,
				  cod_resp_conserv_doc,
				  periodo_conserv_unita_tempo,
				  periodo_conserv,
				  flag_doc_registraz_qualita,
				  flag_catalogazione,
				  flag_doc_automatico,
				  flag_cap_manuale)  
	VALUES (   :s_cs_xx.cod_azienda,   
        		  :ll_progr,   
	           :em_nome_doc.text,   
   	        :ldd_num_versione,   
      	     :ldd_num_edizione,   
         	  :st_4.text,   
	           :ld_oggi,   
   	        :ls_resp_divisione,   
      	     null,   
	           null,   
   	        null,   
      	     :li_valido_per,   
         	  null,   
	           :ls_area,   
   	        'G',
				  'N',
				  :ddlb_livello_doc.text, 
				  null,
				  :em_des_documento.text,
				  null,
				  0,
				  null,
				  'G',
				  0,
				  'N',
				  'P',
				  :ls_flag_doc_automatico,
				  'N');

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Omnia","Errore durante insert in tabella documenti",exclamation!)
   return
end if


	  INSERT INTO doc_generici  
   	      ( cod_azienda,   
      	     progr,   
      	     path_documento )  
	  VALUES ( :s_cs_xx.cod_azienda,   
   	        :ll_progr,   
   	        :ls_path_documento )  ;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Omnia","Errore durante insert in tabella doc_generici",exclamation!)
	return
end if


	  INSERT INTO doc_divisioni  
         ( cod_azienda,   
           progr,   
           cod_divisione )  
     VALUES ( :s_cs_xx.cod_azienda,   
              :ll_progr,   
              :ls_nome_divisione )  ;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Omnia","Errore durante insert in tabella doc_divisioni",exclamation!)
	return
end if

if rb_1.checked=true then
		
	if li_versione_word  = 6 then
		ls_risposta=f_word("filenuovo",i_percorso_word)	
	else
		ls_risposta=f_word("filenew",i_percorso_word)	
	end if

	if ls_risposta = "no" then	
	 g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	 rollback;
	 return
	end if
	
	if li_versione_word = 6 then
		ls_risposta=f_word("filechiuditutto",i_percorso_word)	
	else
		ls_risposta=f_word("filecloseall",i_percorso_word)	
	end if

	if ls_risposta = "no" then	
	 g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	 rollback;
	 return
	end if
	
	if li_versione_word = 6 then
		ls_risposta=f_word("filenuovo.modello =" + char(34) + st_4.text + char(34) ,i_percorso_word)
	else 
		ls_risposta=f_word("filenew.template =" + char(34) + st_4.text + char(34) ,i_percorso_word)
	end if
		
	if ls_risposta = "no" then	
	 g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	 rollback;
	 return
	end if	
	
	if li_versione_word = 6 then	
		ls_risposta=f_word("filesalvaconnome.nome=" + char(34) + st_6.text + char(34) +",.password=" +char(34)+ i_password +char(34),i_percorso_word)	  
	else
		ls_risposta=f_word("filesaveas.name=" + char(34) + st_6.text + char(34) +",.password=" +char(34)+ i_password +char(34),i_percorso_word)	  
	end if

	if ls_risposta = "no" then	
	 g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
	 rollback;
	 return
	end if
	
	if cbx_modelli_default.checked = true then
		wf_inserisci()
	end if

end if
	if rb_2.checked=true then		

		if li_versione_word = 6 then
			ls_risposta=f_word("filenuovo",i_percorso_word)	
		else
			ls_risposta=f_word("filenew",i_percorso_word)	
		end if

		if ls_risposta = "no" then	
		 g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		 rollback;
		 return
		end if
		
		if li_versione_word = 6 then
			ls_risposta=f_word("filechiuditutto",i_percorso_word)	
		else
			ls_risposta=f_word("filecloseall",i_percorso_word)	
		end if

		if ls_risposta = "no" then	
		 	g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
			rollback;
			return
		end if
		
		if li_versione_word = 6 then
			ls_risposta=f_word("fileapri " + char(34) + st_4.text + char(34) +",.passworddocumento="+char(34)+i_password+char(34),i_percorso_word)
		else
			ls_risposta=f_word("fileopen " + char(34) + st_4.text + char(34) +",.passworddoc="+char(34)+i_password+char(34),i_percorso_word)			
		end if
		if ls_risposta = "no" then	
		 g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		 rollback;
		 return
		end if
		
		if li_versione_word = 6 then
			ls_risposta=f_word("filesalvaconnome.nome=" + char(34) + st_6.text + char(34) +",.password=" +char(34)+ i_password +char(34),i_percorso_word) 
		else
			ls_risposta=f_word("filesaveas.name=" + char(34) + st_6.text + char(34) +",.password=" +char(34)+ i_password +char(34),i_percorso_word) 			
		end if

		if ls_risposta = "no" then	
		 g_mb.messagebox("Omnia","Attenzione! Il documento non è stato creato",exclamation!)
		 rollback;
		 return
		end if
	end if	
	if ls_risposta = "ok" then
	  window_open(w_risposta_salva,0)
	end if

else
	g_mb.messagebox("Omnia","Attenzione! Non è possibile creare un documento senza specificare il modello e il nome del file.")
end if	
end event

type st_1 from statictext within w_creazioni
integer x = 41
integer y = 100
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nome Modello:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_creazioni
integer x = 41
integer y = 300
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nome Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_creazioni
integer x = 41
integer y = 196
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Percorso Doc.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_creazioni
integer x = 544
integer y = 100
integer width = 1207
integer height = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

on doubleclicked;//string d_nome_documento, d_nome
//integer d_valore
//
//if rb_1.checked=true then
//   d_valore = GetFileOpenName("Seleziona il Modello",  &
//	d_nome_documento, d_nome, "DOT",  &
//	"Dot  (*.DOT),*.DOT," +  &
//	"Doc  (*.DOC),*.DOC")
//end if
//
//if rb_2.checked=true then		
//   d_valore = GetFileOpenName("Seleziona il Documento",  &
//	d_nome_documento, d_nome, "DOC",  &
//	"Doc  (*.DOC),*.DOC," +  &
//	"Dot  (*.DOT),*.DOT")
//end if	
//
//IF d_valore = 1 THEN 
//	st_4.text=d_nome_documento
//end if

end on

type st_6 from statictext within w_creazioni
integer x = 544
integer y = 196
integer width = 1207
integer height = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

on doubleclicked;//string d_percorso_doc, d_nome_doc
//integer d_valore
//
//d_valore = GetFilesaveName("Nome e percorso documento",  &
//	d_percorso_doc, d_nome_doc, "DOC",  &
//	"Doc  (*.DOC),*.DOC," +  &
//	"Dot  (*.DOT),*.DOT")
//
//IF d_valore = 1 THEN 
//	st_5.text=left(d_nome_doc,len(d_nome_doc)-4)
//	st_6.text=left(d_percorso_doc,len(d_percorso_doc)-4)+".100"
//end if

end on

type rb_1 from radiobutton within w_creazioni
integer x = 64
integer y = 880
integer width = 384
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Da Modello"
end type

event clicked;lb_1.DirList(i_percorso_mod+"\*.dot", 0)
lb_1.SelectItem(1)
st_8.text = "Modelli Disponibili"
lb_1.triggerevent("selectionchanged")
end event

type rb_2 from radiobutton within w_creazioni
integer x = 498
integer y = 880
integer width = 480
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Da documento"
end type

event clicked;lb_1.DirList(i_percorso_doc+"\*.*", 0)
lb_1.SelectItem(1)
st_8.text = "Documenti Disponibili"
lb_1.triggerevent("selectionchanged")
end event

type st_7 from statictext within w_creazioni
integer x = 41
integer y = 596
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Area: "
alignment alignment = right!
boolean focusrectangle = false
end type

type st_9 from statictext within w_creazioni
integer x = 41
integer y = 500
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Valido per (giorni): "
alignment alignment = right!
boolean focusrectangle = false
end type

type lb_1 from listbox within w_creazioni
integer x = 2030
integer y = 100
integer width = 1047
integer height = 900
integer taborder = 10
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;if rb_1.checked=true then
	st_4.text=i_percorso_mod+"\"+lb_1.selecteditem()
end if

if rb_2.checked=true then
	st_4.text=i_percorso_doc+"\"+lb_1.selecteditem()
end if
end event

type st_8 from statictext within w_creazioni
integer x = 2030
integer y = 20
integer width = 1047
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Modelli o Documenti Disponibili"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_creazioni
integer x = 18
integer y = 800
integer width = 1984
integer height = 196
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Opzioni"
end type

type ddlb_1 from dropdownlistbox within w_creazioni
integer x = 818
integer y = 596
integer width = 1138
integer height = 960
integer taborder = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;st_13.text = f_po_selectddlb(ddlb_1)
wf_crea_path_documento()
end event

type cb_4 from cb_apri_manuale within w_creazioni
integer x = 2720
integer y = 1020
integer height = 80
integer taborder = 110
string text = "&Procedure"
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLA")
end on

type st_13 from statictext within w_creazioni
integer x = 544
integer y = 596
integer width = 270
integer height = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type ddlb_livello_doc from dropdownlistbox within w_creazioni
integer x = 1184
integer y = 500
integer width = 293
integer height = 356
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;wf_crea_path_documento()
end event

type st_15 from statictext within w_creazioni
integer x = 864
integer y = 500
integer width = 311
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Livello Doc."
long bordercolor = 79741120
boolean focusrectangle = false
end type

type st_16 from statictext within w_creazioni
integer x = 87
integer y = 700
integer width = 430
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Edizione Iniziale:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_edizione from editmask within w_creazioni
integer x = 544
integer y = 700
integer width = 293
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
boolean spin = true
string displaydata = "Ä"
double increment = 1
string minmax = "1~~999"
end type

event modified;wf_crea_path_documento()
end event

type st_21 from statictext within w_creazioni
integer x = 910
integer y = 700
integer width = 466
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Revisione Iniziale:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_revisione from editmask within w_creazioni
integer x = 1390
integer y = 700
integer width = 293
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "0~~999"
end type

event modified;wf_crea_path_documento()
end event

type cb_esterni from commandbutton within w_creazioni
event clicked pbm_bnclicked
integer x = 1938
integer y = 1020
integer width = 361
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "D. &Esterni"
end type

event clicked;datetime ld_oggi
string ls_risposta,ls_resp_divisione,ls_nome_divisione,ls_area,ls_path_documento, &
       ls_flag_doc_automatico
decimal ldd_num_versione,ldd_num_edizione,li_valido_per,ll_progr

ls_flag_doc_automatico="N"

if cbx_modelli_default.checked = true then
		g_mb.messagebox("OMNIA","Attenzione! Impossibile creare documento esterno usando i modelli predefiniti.",exclamation!)
		return
end if

ldd_num_versione=long(em_edizione.text)
ldd_num_edizione=long(em_revisione.text)

if isnull(i_password) or len(i_password) < 1 or &
   isnull(i_percorso_word) or len(i_percorso_word) < 1 or &
	isnull(i_percorso_doc)  or len(i_percorso_doc)  < 1 or &
	isnull(i_percorso_mod)  or len(i_percorso_mod)  < 1 or &
	isnull(i_percorso_com)  or len(i_percorso_com)  < 1 then
	g_mb.messagebox("Omnia","Attenzione! Mancano uno o più parametri indispensabili per la gestione documenti (password, percorsi, numero edizioni). Controllare in parametri azienda inserendo il/i parametro/i mancante/i", StopSign!)
	return
end if

if len(lb_1.selecteditem()) < 1 then 
	g_mb.messagebox("Omnia","Non vi sono documenti o modelli utilizzabili. Controllare i parametri PTD e PTM, assicurandosi che contegano il percorso corretto.",exclamation!)
	return
end if

if fileexists(st_6.text) then
	g_mb.messagebox("Omnia","Attenzione! Un documento con questo nome risulta già presente nella directory dei documenti di OMNIA pur non risultando in Archivio. Cambiare la directory dei documenti o eliminare il file.", StopSign!)	
	return
end if

if len(em_nome_doc.text) < 1 then
	g_mb.messagebox("Omnia","Assegnare un nome al documento che si desidera creare", StopSign!)
	return
end if	

ls_risposta=f_ctrl_doc_2(em_nome_doc.text)
if ls_risposta <> "nessuno" then
	g_mb.messagebox("Omnia","Attenzione! Un documento con questo nome risulta già in archivio.")
	return
end if

ls_risposta=f_ctrl_doc_3(em_nome_doc.text)
if ls_risposta <> "nessuno" then
	g_mb.messagebox("Omnia","Attenzione! Un documento con questo nome risulta già in archivio.")
	return
end if

if len(em_des_documento.text) < 1 then
	g_mb.messagebox("Omnia","Assegnare una descrizione al documento che si desidera creare", StopSign!)
	return
end if	

if len(ddlb_livello_doc.text) < 1 or isnull(ddlb_livello_doc.text) then
	g_mb.messagebox("Omnia","Attenzione!, Non è possibile creare un documento senza specificarne il livello", StopSign!)
	return
end if

if len(em_gg_validita.text) < 1 or long(em_gg_validita.text) < 0 then
	g_mb.messagebox("Omnia","Il periodo di validità è mancante oppure non impostato correttamente!",StopSign!)
	return
end if
li_valido_per=integer(em_gg_validita.text)

if long(em_edizione.text) < 1 or isnull(long(em_edizione.text)) then
	g_mb.messagebox("Omnia","Attenzione! Impossibile procedere se numero edizione è a zero oppure non è impostato", StopSign!)
	return
end if

if long(em_revisione.text) < 0 or isnull(long(em_revisione.text))  or long(em_revisione.text) > i_num_max_edizioni then
	g_mb.messagebox("Omnia","Attenzione! Numero di revisione documento inammissibile!" , StopSign!)
	return
end if

ls_area=f_po_selectddlb(ddlb_1)
if len(ls_area) < 1 then
	g_mb.messagebox("Omnia","Attenzione!, Non è possibile creare un documento senza specificare un'area di appartenenza", StopSign!)
	return
end if

ls_path_documento=st_6.text
SELECT mansionari.cod_resp_divisione, 
		 mansionari.cod_divisione 
INTO:ls_resp_divisione,
    :ls_nome_divisione & 
FROM mansionari 
WHERE (mansionari.cod_azienda=:s_cs_xx.cod_azienda) AND 
	   (mansionari.cod_utente =:s_cs_xx.cod_utente);

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Omnia","Attenzione l'utente non ha alcun mansionario assegnato.",exclamation!)
	return
end if

if len(em_nome_doc.text) > 0 then
  
	ld_oggi=datetime(today())
	ll_progr=f_maxprogr()

	INSERT INTO documenti  
      	   ( cod_azienda,   
         	  progr,   
	           nome_documento,   
   	        num_versione,   
      	     num_edizione,   
         	  nome_modello,   
	           emesso_il,   
   	        cod_resp_divisione,   
      	     approvato_da,   
         	  autorizzato_da,   
	           autorizzato_il,   
   	        valido_per,   
      	     validato_da,   
         	  cod_area_aziendale,   
	           flag_uso,
				  flag_recuperato,
				  livello_documento,
				  progr_lista_dist,
				  des_documento,
				  path_documento_esterno,  
				  prog_ordinamento_livello,
				  cod_resp_conserv_doc,
				  periodo_conserv_unita_tempo,
				  periodo_conserv,
				  flag_doc_registraz_qualita,
				  flag_catalogazione,
				  flag_doc_automatico)  
	VALUES (   :s_cs_xx.cod_azienda,   
        		  :ll_progr,   
	           :em_nome_doc.text,   
   	        :ldd_num_versione,   
      	     :ldd_num_edizione,   
         	  :st_4.text,   
	           :ld_oggi,   
   	        :ls_resp_divisione,   
      	     null,   
	           null,   
   	        null,   
      	     :li_valido_per,   
         	  null,   
	           :ls_area,   
   	        'G',
				  'N',
				  :ddlb_livello_doc.text,
				  null,
				  :em_des_documento.text,
				  null,
				  0,
				  null,
				  'G',
				  0,
				  'N',
				  'P',
				  :ls_flag_doc_automatico);
				  
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Omnia","Errore durante insert in tabella documenti",exclamation!)
		return
	end if

	INSERT INTO doc_divisioni  
			( cod_azienda,   
			progr,   
			cod_divisione )  
	VALUES ( :s_cs_xx.cod_azienda,   
			:ll_progr,   
			:ls_nome_divisione )  ;
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Omnia","Errore durante insert in tabella doc_divisioni",exclamation!)
		return
	end if
	
	g_mb.messagebox("Omnia","Creazione del documento eseguita con successo!", Information!)
	
end if
end event

type em_gg_validita from editmask within w_creazioni
integer x = 544
integer y = 500
integer width = 293
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
boolean spin = true
string displaydata = ""
string minmax = "0~~999"
end type

event modified;wf_crea_path_documento()
end event

type em_nome_doc from editmask within w_creazioni
integer x = 544
integer y = 300
integer width = 704
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "xxxxxxxxxxxxxxx"
string displaydata = "0"
end type

event modified;wf_crea_path_documento()
end event

type em_des_documento from editmask within w_creazioni
integer x = 544
integer y = 400
integer width = 1413
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
string displaydata = ""
end type

event modified;wf_crea_path_documento()
end event

type gb_3 from groupbox within w_creazioni
integer x = 18
integer width = 1984
integer height = 800
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Dati del Documento"
end type

type st_5 from statictext within w_creazioni
integer x = 64
integer y = 400
integer width = 448
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Des. Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cbx_modelli_default from checkbox within w_creazioni
integer x = 1298
integer y = 880
integer width = 681
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Usa modelli predefiniti"
boolean checked = true
end type

event clicked;if cbx_modelli_default.checked=true then
	rb_1.checked = true
	rb_2.enabled = false
else
	rb_2.enabled = true
end if
end event

