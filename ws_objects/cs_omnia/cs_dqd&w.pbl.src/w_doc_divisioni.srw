﻿$PBExportHeader$w_doc_divisioni.srw
$PBExportComments$Appartenenza Documenti divisioni
forward
global type w_doc_divisioni from w_cs_xx_principale
end type
type dw_doc_divisioni from uo_cs_xx_dw within w_doc_divisioni
end type
end forward

global type w_doc_divisioni from w_cs_xx_principale
int Width=2780
int Height=1025
boolean TitleBar=true
string Title="Documenti Divisioni"
dw_doc_divisioni dw_doc_divisioni
end type
global w_doc_divisioni w_doc_divisioni

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_doc_divisioni,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione","anag_divisioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_doc_divisioni,"progr",sqlca,&
                 "documenti","progr","nome_documento","documenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_doc_divisioni.set_dw_key("cod_azienda")
dw_doc_divisioni.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
iuo_dw_main = dw_doc_divisioni
end on

on w_doc_divisioni.create
int iCurrent
call w_cs_xx_principale::create
this.dw_doc_divisioni=create dw_doc_divisioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_doc_divisioni
end on

on w_doc_divisioni.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_doc_divisioni)
end on

type dw_doc_divisioni from uo_cs_xx_dw within w_doc_divisioni
int X=23
int Y=21
int Width=2698
int Height=881
int TabOrder=10
string DataObject="d_doc_divisioni"
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

