﻿$PBExportHeader$w_documenti_contratti.srw
$PBExportComments$Finestra Dati Specifici Documenti Generici
forward
global type w_documenti_contratti from w_cs_xx_principale
end type
type dw_documenti_contratti from uo_cs_xx_dw within w_documenti_contratti
end type
type dw_folder from u_folder within w_documenti_contratti
end type
end forward

global type w_documenti_contratti from w_cs_xx_principale
int Width=2049
int Height=649
boolean TitleBar=true
string Title="Percorso Fisico Documento"
dw_documenti_contratti dw_documenti_contratti
dw_folder dw_folder
end type
global w_documenti_contratti w_documenti_contratti

type variables
boolean ib_in_new
end variables

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_documenti_contratti
dw_folder.fu_AssignTab(1, "&Opzioni", l_Objects[])

dw_folder.fu_FolderCreate(1,4)
dw_folder.fu_SelectTab(1)

dw_documenti_contratti.set_dw_options(sqlca,i_openparm,c_nonew+c_nodelete,c_default)
iuo_dw_main = dw_documenti_contratti


end event

on w_documenti_contratti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_documenti_contratti=create dw_documenti_contratti
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_documenti_contratti
this.Control[iCurrent+2]=dw_folder
end on

on w_documenti_contratti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_documenti_contratti)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_documenti_contratti,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1", &
                 "anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_documenti_contratti from uo_cs_xx_dw within w_documenti_contratti
int X=46
int Y=121
int Width=1898
int Height=381
int TabOrder=20
string DataObject="d_documenti_contratti"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_cod_documento

ll_cod_documento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progr")
l_Error = Retrieve(s_cs_xx.cod_azienda, ll_cod_documento)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_folder from u_folder within w_documenti_contratti
int X=23
int Y=21
int Width=1966
int Height=501
end type

