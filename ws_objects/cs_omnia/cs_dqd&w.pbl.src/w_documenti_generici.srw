﻿$PBExportHeader$w_documenti_generici.srw
$PBExportComments$Finestra Dati Specifici Documenti Generici
forward
global type w_documenti_generici from w_cs_xx_principale
end type
type dw_documenti_generici from uo_cs_xx_dw within w_documenti_generici
end type
type dw_folder from u_folder within w_documenti_generici
end type
end forward

global type w_documenti_generici from w_cs_xx_principale
int Width=1893
int Height=649
boolean TitleBar=true
string Title="Percorso Fisico Documento"
dw_documenti_generici dw_documenti_generici
dw_folder dw_folder
end type
global w_documenti_generici w_documenti_generici

type variables
boolean ib_in_new
end variables

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_documenti_generici
dw_folder.fu_AssignTab(1, "&Opzioni", l_Objects[])

dw_folder.fu_FolderCreate(1,4)
dw_folder.fu_SelectTab(1)

dw_documenti_generici.set_dw_options(sqlca,i_openparm,c_nonew+c_nodelete,c_default)
iuo_dw_main = dw_documenti_generici


end event

on w_documenti_generici.create
int iCurrent
call w_cs_xx_principale::create
this.dw_documenti_generici=create dw_documenti_generici
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_documenti_generici
this.Control[iCurrent+2]=dw_folder
end on

on w_documenti_generici.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_documenti_generici)
destroy(this.dw_folder)
end on

type dw_documenti_generici from uo_cs_xx_dw within w_documenti_generici
int X=46
int Y=341
int Width=1761
int Height=161
int TabOrder=20
string DataObject="d_documenti_generici"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_cod_documento

ll_cod_documento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progr")
l_Error = Retrieve(s_cs_xx.cod_azienda, ll_cod_documento)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_folder from u_folder within w_documenti_generici
int X=23
int Y=21
int Width=1806
int Height=501
end type

