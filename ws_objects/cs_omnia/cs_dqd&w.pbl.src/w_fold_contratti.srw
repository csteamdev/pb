﻿$PBExportHeader$w_fold_contratti.srw
$PBExportComments$Finestra Gestione Documenti Contratti Fornitori
forward
global type w_fold_contratti from w_cs_xx_principale
end type
type dw_documenti_lista from uo_cs_xx_dw within w_fold_contratti
end type
type rb_1 from radiobutton within w_fold_contratti
end type
type rb_2 from radiobutton within w_fold_contratti
end type
type rb_6 from radiobutton within w_fold_contratti
end type
type rb_3 from radiobutton within w_fold_contratti
end type
type rb_4 from radiobutton within w_fold_contratti
end type
type rb_5 from radiobutton within w_fold_contratti
end type
type gb_1 from groupbox within w_fold_contratti
end type
type cb_1 from commandbutton within w_fold_contratti
end type
type cb_procedure from cb_apri_manuale within w_fold_contratti
end type
type cb_liste_dist from commandbutton within w_fold_contratti
end type
type cb_elimina_liste_dist from commandbutton within w_fold_contratti
end type
type dw_documenti_det_1 from uo_cs_xx_dw within w_fold_contratti
end type
type cb_note_esterne from commandbutton within w_fold_contratti
end type
type dw_documenti_det_2 from uo_cs_xx_dw within w_fold_contratti
end type
type dw_folder from u_folder within w_fold_contratti
end type
type cb_dettagli from commandbutton within w_fold_contratti
end type
type cb_paragrafi_iso from commandbutton within w_fold_contratti
end type
end forward

global type w_fold_contratti from w_cs_xx_principale
integer width = 2368
integer height = 1760
string title = "Documenti Contratti"
dw_documenti_lista dw_documenti_lista
rb_1 rb_1
rb_2 rb_2
rb_6 rb_6
rb_3 rb_3
rb_4 rb_4
rb_5 rb_5
gb_1 gb_1
cb_1 cb_1
cb_procedure cb_procedure
cb_liste_dist cb_liste_dist
cb_elimina_liste_dist cb_elimina_liste_dist
dw_documenti_det_1 dw_documenti_det_1
cb_note_esterne cb_note_esterne
dw_documenti_det_2 dw_documenti_det_2
dw_folder dw_folder
cb_dettagli cb_dettagli
cb_paragrafi_iso cb_paragrafi_iso
end type
global w_fold_contratti w_fold_contratti

type variables
string i_percorso_word
string i_password
integer i_check
long i_riga_corrente
string is_autoriz_liv_doc
string is_livello_doc
end variables

event pc_setwindow;call super::pc_setwindow;string ls_risposta,ls_pdoc,ls_pmod, ls_pcom
integer li_num_edizioni

windowobject l_objects[ ]

l_objects[1] = dw_documenti_det_1
dw_folder.fu_AssignTab(1, "&Dettaglio", l_Objects[])
l_objects[1] = dw_documenti_det_2
dw_folder.fu_AssignTab(2, "&Modifiche", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_documenti_lista.set_dw_key("cod_azienda")
dw_documenti_lista.set_dw_key("progr")
dw_documenti_lista.set_dw_options(sqlca, &
                            c_NullDW, &
									 c_nonew + &
									 c_nodelete, &
									 c_Default)

dw_documenti_det_1.set_dw_options(sqlca, &
											 dw_documenti_lista, &
											 c_nonew + &
											 c_nodelete + &
											 c_sharedata + &
											 c_scrollparent , &
											 c_default)

dw_documenti_det_2.set_dw_options(sqlca, &
										    dw_documenti_lista, &
											 c_nonew +&
											 c_nodelete + &
										    c_sharedata + &
											 c_scrollparent , &
										    c_default)
iuo_dw_main = dw_documenti_lista
dw_folder.fu_FolderCreate(2,4)
dw_folder.fu_SelectTab(1)


rb_1.checked=true
ls_risposta=f_parametri(i_password,i_percorso_word,ls_pdoc,ls_pmod,li_num_edizioni,ls_pcom)
i_check=1

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT autoriz_liv_doc
//INTO	 :is_autoriz_liv_doc
//FROM   mansionari 
//WHERE  cod_azienda=:s_cs_xx.cod_azienda
//AND    cod_utente=:s_cs_xx.cod_utente;
//
//if sqlca.sqlcode = 100 then
//	g_mb.messagebox("Omnia","Attenzione! L'utente non ha un mansionario abilitato",exclamation!)
//else
//	if sqlca.sqlcode <> 0 then
//		g_mb.messagebox("Omnia","Attenzione! Errore durante la ricerca codice utente nel mansionario: verificare che il codice utente con cui si è entrati in OMNIA sia associato ad un mansionario e che lo stesso codice NON sia associato a più mansionari", StopSign!)
//		this.postevent("pc_close")
//	end if
//end if
//

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

is_autoriz_liv_doc = luo_mansionario.uof_get_privilege_string(luo_mansionario.livello_doc)

if isnull (luo_mansionario.uof_get_cod_mansionario()) then
	g_mb.messagebox("Omnia","Attenzione! L'utente non ha un mansionario abilitato",exclamation!)
end if

//--- Fine modifica Giulio

end event

on w_fold_contratti.create
int iCurrent
call super::create
this.dw_documenti_lista=create dw_documenti_lista
this.rb_1=create rb_1
this.rb_2=create rb_2
this.rb_6=create rb_6
this.rb_3=create rb_3
this.rb_4=create rb_4
this.rb_5=create rb_5
this.gb_1=create gb_1
this.cb_1=create cb_1
this.cb_procedure=create cb_procedure
this.cb_liste_dist=create cb_liste_dist
this.cb_elimina_liste_dist=create cb_elimina_liste_dist
this.dw_documenti_det_1=create dw_documenti_det_1
this.cb_note_esterne=create cb_note_esterne
this.dw_documenti_det_2=create dw_documenti_det_2
this.dw_folder=create dw_folder
this.cb_dettagli=create cb_dettagli
this.cb_paragrafi_iso=create cb_paragrafi_iso
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_documenti_lista
this.Control[iCurrent+2]=this.rb_1
this.Control[iCurrent+3]=this.rb_2
this.Control[iCurrent+4]=this.rb_6
this.Control[iCurrent+5]=this.rb_3
this.Control[iCurrent+6]=this.rb_4
this.Control[iCurrent+7]=this.rb_5
this.Control[iCurrent+8]=this.gb_1
this.Control[iCurrent+9]=this.cb_1
this.Control[iCurrent+10]=this.cb_procedure
this.Control[iCurrent+11]=this.cb_liste_dist
this.Control[iCurrent+12]=this.cb_elimina_liste_dist
this.Control[iCurrent+13]=this.dw_documenti_det_1
this.Control[iCurrent+14]=this.cb_note_esterne
this.Control[iCurrent+15]=this.dw_documenti_det_2
this.Control[iCurrent+16]=this.dw_folder
this.Control[iCurrent+17]=this.cb_dettagli
this.Control[iCurrent+18]=this.cb_paragrafi_iso
end on

on w_fold_contratti.destroy
call super::destroy
destroy(this.dw_documenti_lista)
destroy(this.rb_1)
destroy(this.rb_2)
destroy(this.rb_6)
destroy(this.rb_3)
destroy(this.rb_4)
destroy(this.rb_5)
destroy(this.gb_1)
destroy(this.cb_1)
destroy(this.cb_procedure)
destroy(this.cb_liste_dist)
destroy(this.cb_elimina_liste_dist)
destroy(this.dw_documenti_det_1)
destroy(this.cb_note_esterne)
destroy(this.dw_documenti_det_2)
destroy(this.dw_folder)
destroy(this.cb_dettagli)
destroy(this.cb_paragrafi_iso)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_documenti_det_1,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_documenti_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
end event

type dw_documenti_lista from uo_cs_xx_dw within w_fold_contratti
integer x = 23
integer y = 20
integer width = 1714
integer height = 500
integer taborder = 110
string dataobject = "d_documenti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,'C',is_autoriz_liv_doc,is_livello_doc)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_dettagli.enabled = false
cb_elimina_liste_dist.enabled = false
cb_liste_dist.enabled = false
cb_note_esterne.enabled = false

end event

event pcd_new;call super::pcd_new;cb_dettagli.enabled = false
cb_elimina_liste_dist.enabled = false
cb_liste_dist.enabled = false
cb_note_esterne.enabled = false

end event

event pcd_view;call super::pcd_view;cb_dettagli.enabled = true
cb_elimina_liste_dist.enabled = true
cb_liste_dist.enabled = true
cb_note_esterne.enabled = true

end event

type rb_1 from radiobutton within w_fold_contratti
integer x = 1829
integer y = 60
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Lettura"
end type

on clicked;i_check=1
end on

type rb_2 from radiobutton within w_fold_contratti
integer x = 1829
integer y = 120
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Modifica "
end type

on clicked;i_check=2
end on

type rb_6 from radiobutton within w_fold_contratti
integer x = 1829
integer y = 180
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Elimina"
end type

on clicked;i_check=6

end on

type rb_3 from radiobutton within w_fold_contratti
integer x = 1829
integer y = 240
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Verifica"
end type

on clicked;i_check=3
end on

type rb_4 from radiobutton within w_fold_contratti
integer x = 1829
integer y = 300
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Approva"
end type

on clicked;i_check=4
end on

type rb_5 from radiobutton within w_fold_contratti
integer x = 1829
integer y = 360
integer width = 251
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Valida"
end type

on clicked;i_check=5
end on

type gb_1 from groupbox within w_fold_contratti
integer x = 1760
integer width = 549
integer height = 440
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Opzioni"
end type

type cb_1 from commandbutton within w_fold_contratti
integer x = 1760
integer y = 440
integer width = 549
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_nome_documento,ls_risposta
long ll_progr,ll_riga_dw, ll_risposta


if i_check = 6 then
	ll_risposta = g_mb.messagebox("OMNIA","Sei sicuro di voler eliminare il documento selezionato?",Question!,OKCancel! ,2)
	if ll_risposta <> 1  then return
end if

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return
setpointer(hourglass!)
ll_progr = dw_documenti_lista.GetItemNumber(dw_documenti_lista.getrow(),"progr")
SELECT doc_generici.path_documento
INTO   :ls_nome_documento  
FROM   doc_generici  
WHERE  (doc_generici.cod_azienda = :s_cs_xx.cod_azienda) AND  
		 (doc_generici.progr = :ll_progr);
choose case sqlca.sqlcode
	case 100
		g_mb.messagebox("OMNIA","Attenzione: manca la registrazione del documento nella tabella contratti", StopSign!)
	case 0
   
	case else
		g_mb.messagebox("OMNIA","Errore durante la ricerca documento nella tabella contratti. Errore SQL: "+sqlca.sqlerrtext, StopSign!)
end choose

ls_risposta = f_esepri(i_check,ls_nome_documento,ll_progr)
ll_riga_dw  = dw_documenti_lista.getrow()
dw_documenti_lista.change_dw_current()
dw_documenti_lista.triggerevent("pcd_retrieve")
dw_documenti_lista.setrow(ll_riga_dw)
setpointer(arrow!)
rb_1.triggerevent("Clicked")

end event

type cb_procedure from cb_apri_manuale within w_fold_contratti
integer x = 1943
integer y = 540
integer width = 370
integer height = 80
integer taborder = 10
string text = "&Procedure"
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLA")
end on

type cb_liste_dist from commandbutton within w_fold_contratti
event clicked pbm_bnclicked
integer x = 1943
integer y = 940
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Liste Dist."
end type

event clicked;long ll_progr_lista_dist,ll_progr
string ls_path_doc_from,ls_path_doc_to

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return
ll_progr=dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"progr")
SELECT doc_generici.path_documento  
INTO :ls_path_doc_from 
FROM doc_generici  
WHERE ( doc_generici.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( doc_generici.progr = :ll_progr )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Impossibile associare lista di distribuzione ai documenti esterni", StopSign!)
   return
end if

select progr_lista_dist
into   :ll_progr_lista_dist
from   documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr=:ll_progr;

if isnull(ll_progr_lista_dist) or ll_progr_lista_dist=0 then
	
	ll_progr_lista_dist=f_progressivo_lista_dist()
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_dist	
	if fileexists(ls_path_doc_from) then
		ls_path_doc_to=left(ls_path_doc_from,len(ls_path_doc_from)-4) + right(ls_path_doc_from,3) + ".doc"
		w_POManager_STD.EXT.fu_FileCopy(ls_path_doc_from,ls_path_doc_to)
	end if
	s_cs_xx.parametri.parametro_s_1 = ls_path_doc_to
	
	update documenti
	set    progr_lista_dist=:ll_progr_lista_dist
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr=:ll_progr;

	window_open(w_seleziona_lista_dist,0)

else
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_dist
	window_open(w_liste_dist_doc,0)

end if
end event

type cb_elimina_liste_dist from commandbutton within w_fold_contratti
event clicked pbm_bnclicked
integer x = 1943
integer y = 840
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina L.D."
end type

event clicked;long ll_progr,ll_progr_lista_dist
string ls_path_doc_from 

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return

ll_progr=dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"progr")

SELECT doc_generici.path_documento  
INTO :ls_path_doc_from 
FROM doc_generici  
WHERE ( doc_generici.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( doc_generici.progr = :ll_progr )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Impossibile associare lista di distribuzione ai documenti esterni", StopSign!)
   return
end if

select progr_lista_dist
into   :ll_progr_lista_dist
from   documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr=:ll_progr;
if isnull(ll_progr_lista_dist) or ll_progr_lista_dist=0 then
	g_mb.messagebox("OMNIA","Non risulta associata al documento alcuna lista di distribuzione", StopSign!)
	return
end if

update documenti
set    progr_lista_dist=0
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr=:ll_progr;
choose case sqlca.sqlcode
	case 100
		g_mb.messagebox("OMNIA","Impossibile disassociare il numero lista: non trovo il documento "+string(ll_progr), StopSign!)
	case 0
		
	case else
		g_mb.messagebox("OMNIA","Errore durante la disassociazione lista di distribuzione. Errore SQL: "+sqlca.sqlerrtext, StopSign!)
end choose
if f_elimina_liste_dist_doc(ll_progr_lista_dist) = 0 then
	g_mb.messagebox("OMNIA", "Operazione eseguita con successo", Information!)
end if
end event

type dw_documenti_det_1 from uo_cs_xx_dw within w_fold_contratti
integer x = 46
integer y = 660
integer width = 1851
integer height = 960
integer taborder = 60
string dataobject = "d_documenti_det_1"
boolean border = false
end type

type cb_note_esterne from commandbutton within w_fold_contratti
event clicked pbm_bnclicked
integer x = 1943
integer y = 640
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "D&ocumento"
end type

event clicked;string ls_cod_prodotto, ls_cod_blob, ls_db
long ll_i, ll_progr
blob lbl_null
integer li_risposta
transaction sqlcb

setnull(lbl_null)

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return
ll_i = dw_documenti_lista.getrow()
ll_progr = dw_documenti_lista.getitemnumber(ll_i, "progr")

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)

	selectblob documenti.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       documenti
	where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
	           documenti.progr = :ll_progr
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else

	selectblob documenti.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       documenti
	where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
	           documenti.progr = :ll_progr;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob documenti
	   set        documenti.blob = :s_cs_xx.parametri.parametro_bl_1
	   where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
	              documenti.progr = :ll_progr
		using      sqlcb;		
		
		destroy sqlcb;
		
	else
		
	   updateblob documenti
	   set        documenti.blob = :s_cs_xx.parametri.parametro_bl_1
	   where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
	              documenti.progr = :ll_progr;
	end if
		
// fine modifiche

   update     documenti
   set        documenti.path_documento_esterno = :s_cs_xx.parametri.parametro_s_1
   where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
              documenti.progr = :ll_progr;
   commit;
end if

end event

type dw_documenti_det_2 from uo_cs_xx_dw within w_fold_contratti
integer x = 46
integer y = 660
integer width = 1829
integer height = 876
integer taborder = 70
string dataobject = "d_documenti_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_fold_contratti
integer x = 23
integer y = 540
integer width = 1897
integer height = 1100
integer taborder = 90
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Dettaglio"
      SetFocus(dw_documenti_det_1)
   CASE "&Modifiche"
      SetFocus(dw_documenti_det_2)
END CHOOSE

end event

type cb_dettagli from commandbutton within w_fold_contratti
event clicked pbm_bnclicked
integer x = 1943
integer y = 740
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "De&ttagli"
end type

event clicked;long ll_selected[]

dw_documenti_lista.get_selected_rows(ll_selected)
if upperbound(ll_selected) > 0 then
	window_open_parm(w_documenti_contratti, -1, dw_documenti_lista)
else
	g_mb.messagebox("OMNIA","Selezionare una riga dalla lista Documenti", StopSign!)
end if
end event

type cb_paragrafi_iso from commandbutton within w_fold_contratti
event clicked pbm_bnclicked
integer x = 1943
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 51
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Para&grafi"
end type

event clicked;long ll_selected[]

dw_documenti_lista.get_selected_rows(ll_selected)
if upperbound(ll_selected) > 0 then
	window_open_parm(w_paragrafi_iso_documenti, -1, dw_documenti_lista)
	else
	g_mb.messagebox("OMNIA","Selezionare una riga dalla lista Documenti", StopSign!)
end if
end event

