﻿$PBExportHeader$w_fold_generici.srw
$PBExportComments$Window Documenti Generici in unica soluzione con folder
forward
global type w_fold_generici from w_cs_xx_principale
end type
type rb_1 from radiobutton within w_fold_generici
end type
type rb_2 from radiobutton within w_fold_generici
end type
type rb_6 from radiobutton within w_fold_generici
end type
type rb_3 from radiobutton within w_fold_generici
end type
type rb_4 from radiobutton within w_fold_generici
end type
type rb_5 from radiobutton within w_fold_generici
end type
type gb_2 from groupbox within w_fold_generici
end type
type gb_1 from groupbox within w_fold_generici
end type
type cb_1 from commandbutton within w_fold_generici
end type
type dw_documenti_lista from uo_cs_xx_dw within w_fold_generici
end type
type cb_procedue from cb_apri_manuale within w_fold_generici
end type
type cb_liste_dist from commandbutton within w_fold_generici
end type
type cb_elimina_liste_dist from commandbutton within w_fold_generici
end type
type cb_note_esterne from commandbutton within w_fold_generici
end type
type cb_dettagli from commandbutton within w_fold_generici
end type
type cb_paragrafi_iso from commandbutton within w_fold_generici
end type
type rb_livello_a from radiobutton within w_fold_generici
end type
type rb_livello_b from radiobutton within w_fold_generici
end type
type rb_livello_c from radiobutton within w_fold_generici
end type
type rb_livello_tutti from radiobutton within w_fold_generici
end type
type dw_documenti_det_1 from uo_cs_xx_dw within w_fold_generici
end type
type dw_documenti_det_2 from uo_cs_xx_dw within w_fold_generici
end type
type dw_folder from u_folder within w_fold_generici
end type
type dw_documenti_det_3 from uo_cs_xx_dw within w_fold_generici
end type
end forward

global type w_fold_generici from w_cs_xx_principale
integer width = 2373
integer height = 1704
string title = "Registro Delle Norme"
rb_1 rb_1
rb_2 rb_2
rb_6 rb_6
rb_3 rb_3
rb_4 rb_4
rb_5 rb_5
gb_2 gb_2
gb_1 gb_1
cb_1 cb_1
dw_documenti_lista dw_documenti_lista
cb_procedue cb_procedue
cb_liste_dist cb_liste_dist
cb_elimina_liste_dist cb_elimina_liste_dist
cb_note_esterne cb_note_esterne
cb_dettagli cb_dettagli
cb_paragrafi_iso cb_paragrafi_iso
rb_livello_a rb_livello_a
rb_livello_b rb_livello_b
rb_livello_c rb_livello_c
rb_livello_tutti rb_livello_tutti
dw_documenti_det_1 dw_documenti_det_1
dw_documenti_det_2 dw_documenti_det_2
dw_folder dw_folder
dw_documenti_det_3 dw_documenti_det_3
end type
global w_fold_generici w_fold_generici

type variables
string i_percorso_word
string i_password
integer i_check
long i_riga_corrente
string is_autoriz_liv_doc, is_livello_doc="%"
boolean ib_in_new=false
end variables

forward prototypes
public function integer wf_elimina_doc_esterno (long wl_progr)
public function integer wf_modifica_doc_esterno (long wl_progr)
end prototypes

public function integer wf_elimina_doc_esterno (long wl_progr);string ls_flag_elimina, ls_risposta

//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.flag_elimina
//INTO :ls_flag_elimina
//FROM mansionari
//WHERE (mansionari.cod_azienda=:s_cs_xx.cod_azienda) AND 
//	   (mansionari.cod_utente =:s_cs_xx.cod_utente);

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'


//if sqlca.sqlcode<>0 then
if isnull(luo_mansionario.uof_get_cod_mansionario()) then
	g_mb.messagebox("OMNIA", "Errore in ricerca nel DB: " + SQLCA.SQLErrText + "	OPERAZIONE BLOCCATA",StopSign!)
	return -1
end if
//--- Fine modifica Giulio

if ls_flag_elimina="N" then	
	g_mb.messagebox("Omnia","Non si è in possesso del privilegio di eliminazione documento")
	return 1
end if	
ls_risposta=f_elimina_doc(wl_progr)
return 1
end function

public function integer wf_modifica_doc_esterno (long wl_progr);//	Funzione di modifica documenti esterni OLE
//
// nome: f_esepri
// tipo: string
//  
//	Variabili passate: 		nome					 tipo				passaggio per
//							
//							 codice_documento			long				valore
//

string l_dati,l_risposta,l_password,l_percorso_word, l_approvato,l_autorizzato,l_validato,ls_codice_documento, &
       ls_pcom, l_flag_lettura,l_flag_modifica,l_flag_elimina,l_flag_approvazione, &
		 l_flag_autorizzazione, l_flag_validazione,ls_pdoc,ls_pmod, &
		 l_nome_doc,l_nome_modello,ls_resp_divisione,l_approvato_da, &
       l_autorizzato_da,l_validato_da,l_area,l_flag_uso,l_path_documento,l_versione_edizione,l_nuovo_nome_doc, &
       l_num_contratto,l_cod_fornitore,l_desc_progetto,l_prodotto,ls_nome_divisione, &
       ls_flag_recuperato, ls_validato_da, ls_autorizzato_da, ls_modifiche, ls_livello_documento, &
       ls_des_documento, ls_path_documento_esterno, ls_null, ls_cod_resp_conserv_doc, & 
		 ls_periodo_conserv_unita_tempo,ls_flag_doc_registraz_qualita, ls_flag_catalogazione, &
		 ls_flag_doc_automatico, ls_flag_cap_manuale, ls_db
integer li_num_edizioni, li_risposta
decimal l_num_versione,l_num_edizione,l_valido_per
long l_progr, ll_progr_lista_dist, ll_prog_ordinamento_livello, ll_periodo_conserv
datetime l_oggi, ld_null, ld_data_verifica
datetime l_emesso_il,l_autorizzato_il,l_data_contratto
blob lbl_documento_esterno
transaction sqlcb



l_oggi=datetime(today())
setnull(ld_null)
setnull(ls_null)
l_risposta=f_parametri(l_password,l_percorso_word,ls_pdoc,ls_pmod,li_num_edizioni, ls_pcom)

commit;

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.flag_approvazione,
//		 mansionari.flag_autorizzazione,
//		 mansionari.flag_validazione,
//		 mansionari.flag_lettura,
//		 mansionari.flag_modifica,
//		 mansionari.flag_elimina,
//		 mansionari.cod_resp_divisione, 
//		 mansionari.cod_divisione 
//INTO :l_flag_approvazione, 
//	  :l_flag_autorizzazione,
//	  :l_flag_validazione, 
//	  :l_flag_lettura,
//	  :l_flag_modifica, 		 
//	  :l_flag_elimina ,
//	  :ls_resp_divisione,
//     :ls_nome_divisione
//FROM mansionari
//WHERE (mansionari.cod_azienda=:s_cs_xx.cod_azienda) AND 
//	   (mansionari.cod_utente =:s_cs_xx.cod_utente);

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

SELECT mansionari.cod_resp_divisione, 
		   mansionari.cod_divisione 
INTO 
	 :ls_resp_divisione,
     :ls_nome_divisione
FROM mansionari
WHERE (mansionari.cod_azienda=:s_cs_xx.cod_azienda) AND 
	   (mansionari.cod_utente =:s_cs_xx.cod_utente);

l_flag_approvazione = 'N'
l_flag_autorizzazione = 'N'
l_flag_validazione = 'N'
l_flag_lettura = 'N'
l_flag_modifica = 'N'
l_flag_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then l_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then l_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then l_flag_validazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.lettura) then l_flag_lettura = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then l_flag_modifica = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then l_flag_elimina = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA", "Errore in ricerca nel DB: " + SQLCA.SQLErrText + "	OPERAZIONE BLOCCATA",StopSign!)
	return 0
end if

SELECT documenti.approvato_da,
		 documenti.autorizzato_da,
		 documenti.validato_da
INTO :l_approvato, 
	  :l_autorizzato, 
	  :l_validato &
FROM documenti
WHERE (documenti.cod_azienda=:s_cs_xx.cod_azienda) AND 
	   (documenti.progr =:wl_progr);

if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA", "Errore in ricerca nel DB: " + SQLCA.SQLErrText + "	OPERAZIONE BLOCCATA",StopSign!)
	return -1
end if

ls_codice_documento=string(wl_progr)
if l_flag_modifica="N" then	
	g_mb.messagebox("Omnia","Non si è in possesso del privilegio di modifica")
	return 0
end if	

if g_mb.messagebox ("Omnia", "Creo una nuova revisione?", Question!, YesNo! , 2) = 1 then
	if isnull(l_approvato) or isnull(l_autorizzato) then
				g_mb.messagebox("OMNIA","Non è possibile creare una nuova revisione poichè il documento non è verificato e approvato.")
	else
		l_risposta=f_ctrl_doc(ls_codice_documento)
		choose case l_risposta 
			 case "ok"	
				g_mb.messagebox("OMNIA","Non è possibile creare una nuova revisione poichè esistono già 2 documenti con lo stesso nome di cui uno non ancora approvato.")
			 case "no"
				l_oggi=datetime(today())
				SELECT documenti.progr,
						 documenti.approvato_da,
						 documenti.nome_documento,
						 documenti.num_versione,
						 documenti.num_edizione,
						 documenti.nome_modello,
						 documenti.emesso_il,
						 documenti.cod_resp_divisione,
						 documenti.autorizzato_il,
						 documenti.valido_per,
						 documenti.cod_area_aziendale,
						 documenti.flag_uso,
						 documenti.flag_recuperato,
						 documenti.validato_da,
						 documenti.autorizzato_da,
						 documenti.modifiche,
						 documenti.livello_documento,
						 documenti.progr_lista_dist,
						 documenti.des_documento,
						 documenti.path_documento_esterno,
						 documenti.prog_ordinamento_livello,
						 documenti.cod_resp_conserv_doc,
						 documenti.periodo_conserv_unita_tempo,
						 documenti.periodo_conserv,
						 documenti.flag_doc_registraz_qualita,
						 documenti.flag_catalogazione,
						 documenti.data_verifica,
						 documenti.flag_doc_automatico,
						 documenti.flag_cap_manuale						 
				INTO	 :l_progr, 
						 :l_approvato_da,
						 :l_nome_doc,
						 :l_num_versione,
						 :l_num_edizione,
						 :l_nome_modello,
						 :l_emesso_il,
						 :ls_resp_divisione,
						 :l_autorizzato_il,
						 :l_valido_per,
						 :l_area,
						 :l_flag_uso ,
						 :ls_flag_recuperato,
						 :ls_validato_da,
						 :ls_autorizzato_da,
						 :ls_modifiche,
						 :ls_livello_documento,
						 :ll_progr_lista_dist,
						 :ls_des_documento,
						 :ls_path_documento_esterno,
						 :ll_prog_ordinamento_livello,
						 :ls_cod_resp_conserv_doc,
						 :ls_periodo_conserv_unita_tempo,
						 :ll_periodo_conserv,
						 :ls_flag_doc_registraz_qualita,
						 :ls_flag_catalogazione,
						 :ld_data_verifica,
						 :ls_flag_doc_automatico,
						 :ls_flag_cap_manuale
				FROM 	 documenti 
				WHERE  documenti.cod_azienda=:s_cs_xx.cod_azienda AND 
						 documenti.progr =:wl_progr;
				
				if sqlca.sqlcode<>0 then
					g_mb.messagebox("OMNIA","Errore in fase di ricerca documento: " + SQLCA.SQLErrText,Information!)
					return -1
				end if
						
// 11-07-2002 modifiche Michela: controllo l'enginetype

				ls_db = f_db()
				
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					selectblob documenti.blob
	            into :lbl_documento_esterno
					from documenti
					where  documenti.cod_azienda=:s_cs_xx.cod_azienda AND 
							 documenti.progr =:wl_progr
					using  sqlcb;
							 
					if sqlcb.sqlcode<>0 then
						g_mb.messagebox("OMNIA","Errore in fase di ricerca documento: " + SQLCA.SQLErrText,Information!)
						destroy sqlcb;
						return -1
					end if
					
					destroy sqlcb;
					
				else
					
					selectblob documenti.blob
	            into :lbl_documento_esterno
					from documenti
					where  documenti.cod_azienda=:s_cs_xx.cod_azienda AND 
							 documenti.progr =:wl_progr;
							 
					if sqlca.sqlcode<>0 then
						g_mb.messagebox("OMNIA","Errore in fase di ricerca documento: " + SQLCA.SQLErrText,Information!)
						return -1
					end if
					
				end if
	
				
				if l_num_edizione=li_num_edizioni then
					l_num_edizione=0
					l_num_versione++
				else
					l_num_edizione++		  
				end if
				
				
				if len(string(l_num_edizione))<2 then
					l_versione_edizione=string(l_num_versione) + "0" + string(l_num_edizione)
				else	
					l_versione_edizione=string(l_num_versione) + string(l_num_edizione) 
				end if
				
				l_progr=f_maxprogr()
				
				INSERT INTO documenti
						 (cod_azienda,
						 progr,
						 approvato_da,
						 nome_documento, 
						 num_versione,
						 num_edizione,
						 nome_modello,
						 emesso_il,
						 cod_resp_divisione,
						 autorizzato_il,
						 valido_per,
						 cod_area_aziendale,
						 flag_uso,
						 flag_recuperato,
						 validato_da,
						 autorizzato_da,
						 modifiche,
						 livello_documento,
						 progr_lista_dist,
						 des_documento,
						 path_documento_esterno,
						 prog_ordinamento_livello,
						 cod_resp_conserv_doc,
						 periodo_conserv_unita_tempo,
						 periodo_conserv,
						 flag_doc_registraz_qualita,
						 flag_catalogazione,
						 data_verifica,
						 flag_doc_automatico,
						 flag_cap_manuale)
				VALUES(:s_cs_xx.cod_azienda,
						 :l_progr, 
						 :ls_null,
						 :l_nome_doc,
						 :l_num_versione,
						 :l_num_edizione,
						 :l_nome_modello,
						 :l_oggi,
						 :ls_resp_divisione,
						 :ld_null,
						 0,
						 :l_area,
						 :l_flag_uso,
						 'N',
						 :ls_null,
						 :ls_null,
						 :ls_null,
						 :ls_livello_documento,
						 :ll_progr_lista_dist,
						 :ls_des_documento,
						 :ls_path_documento_esterno,
						 :ll_prog_ordinamento_livello,
						 :ls_cod_resp_conserv_doc,
						 :ls_periodo_conserv_unita_tempo,
						 :ll_periodo_conserv,
						 :ls_flag_doc_registraz_qualita,
						 :ls_flag_catalogazione,
						 :ld_null,
						 :ls_flag_doc_automatico,
						 :ls_flag_cap_manuale); 
				
				if sqlca.sqlcode<>0 then
					g_mb.messagebox("Errore nel DB","Errore durante creazione nuova revisione: " + SQLCA.SQLErrText,Information!)
					rollback;
					return -1
				end if
				
				if not isnull(lbl_documento_esterno) then
					
					if ls_db = "MSSQL" then
						
						li_risposta = f_crea_sqlcb(sqlcb)
						
						
						updateblob documenti
						set        documenti.blob = :lbl_documento_esterno
						where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
									  documenti.progr = :l_progr
						using      sqlcb;
						
						if sqlcb.sqlcode<>0 then
							g_mb.messagebox("Errore nel DB","Errore durante creazione nuova revisione: " + sqlcb.SQLErrText,Information!)
							rollback;
							destroy sqlcb;
							return -1
						end if
						
						destroy sqlcb;
						
					else
						
						updateblob documenti
						set        documenti.blob = :lbl_documento_esterno
						where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
									  documenti.progr = :l_progr;
						
						if sqlca.sqlcode<>0 then
							g_mb.messagebox("Errore nel DB","Errore durante creazione nuova revisione: " + SQLCA.SQLErrText,Information!)
							rollback;
							return -1
						end if

					end if

// fine modifiche
											
				end if
				
				INSERT INTO doc_divisioni  
							  (cod_azienda,   
								progr,   
								cod_divisione )  
				VALUES     (:s_cs_xx.cod_azienda,   
							   :l_progr,   
							   :ls_nome_divisione )  ;
				
				if sqlca.sqlcode<>0 then
					g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)
					rollback;
					return -1
				end if
			 case "problema"
				g_mb.messagebox("OMNIA","Si è verificato un malfunzionamento nel database.")
		end choose
	end if	
end if	

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_risposta,ls_pdoc,ls_pmod,ls_pcom
integer li_num_edizioni
windowobject l_objects[ ]


l_objects[1] = dw_documenti_det_1
dw_folder.fu_AssignTab(1, "&Dettaglio", l_Objects[])
l_objects[1] = dw_documenti_det_2
dw_folder.fu_AssignTab(2, "&Modifiche", l_Objects[])
l_objects[1] = dw_documenti_det_3
dw_folder.fu_AssignTab(3, "&Conservazione", l_Objects[])

dw_documenti_lista.set_dw_key("cod_azienda")
dw_documenti_lista.set_dw_key("progr")
dw_documenti_lista.set_dw_options(sqlca, &
   									    pcca.null_object, &
											 c_nonew +&
											 c_nodelete ,&
									       c_default)

dw_documenti_det_1.set_dw_options(sqlca, &
											 dw_documenti_lista, &
											 c_nonew + &
											 c_nodelete + &
											 c_sharedata + &
											 c_scrollparent , &
											 c_default)

dw_documenti_det_2.set_dw_options(sqlca, &
										    dw_documenti_lista, &
											 c_nonew +&
											 c_nodelete + &
										    c_sharedata + &
											 c_scrollparent , &
										    c_default)
dw_documenti_det_3.set_dw_options(sqlca, &
										    dw_documenti_lista, &
											 c_nonew +&
											 c_nodelete + &
										    c_sharedata + &
											 c_scrollparent , &
										    c_default)
iuo_dw_main = dw_documenti_lista
dw_folder.fu_FolderCreate(3,4)
dw_folder.fu_SelectTab(1)

rb_1.checked=true
ls_risposta=f_parametri(i_password,i_percorso_word,ls_pdoc,ls_pmod,li_num_edizioni,ls_pcom)
i_check=1

SELECT autoriz_liv_doc
INTO	 :is_autoriz_liv_doc
FROM   mansionari 
WHERE  cod_azienda=:s_cs_xx.cod_azienda
AND    cod_utente=:s_cs_xx.cod_utente;


if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia","Attenzione! L'utente non ha un mansionario abilitato",exclamation!)
else
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia","Attenzione! Errore durante la ricerca codice utente nel mansionario: verificare che il codice utente con cui si è entrati in OMNIA sia associato ad un mansionario e che lo stesso codice NON sia associato a più mansionari", StopSign!)
		this.postevent("pc_close")
	end if
end if

rb_livello_tutti.triggerevent("clicked")
rb_livello_tutti.checked = true












end event

on w_fold_generici.create
int iCurrent
call super::create
this.rb_1=create rb_1
this.rb_2=create rb_2
this.rb_6=create rb_6
this.rb_3=create rb_3
this.rb_4=create rb_4
this.rb_5=create rb_5
this.gb_2=create gb_2
this.gb_1=create gb_1
this.cb_1=create cb_1
this.dw_documenti_lista=create dw_documenti_lista
this.cb_procedue=create cb_procedue
this.cb_liste_dist=create cb_liste_dist
this.cb_elimina_liste_dist=create cb_elimina_liste_dist
this.cb_note_esterne=create cb_note_esterne
this.cb_dettagli=create cb_dettagli
this.cb_paragrafi_iso=create cb_paragrafi_iso
this.rb_livello_a=create rb_livello_a
this.rb_livello_b=create rb_livello_b
this.rb_livello_c=create rb_livello_c
this.rb_livello_tutti=create rb_livello_tutti
this.dw_documenti_det_1=create dw_documenti_det_1
this.dw_documenti_det_2=create dw_documenti_det_2
this.dw_folder=create dw_folder
this.dw_documenti_det_3=create dw_documenti_det_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_1
this.Control[iCurrent+2]=this.rb_2
this.Control[iCurrent+3]=this.rb_6
this.Control[iCurrent+4]=this.rb_3
this.Control[iCurrent+5]=this.rb_4
this.Control[iCurrent+6]=this.rb_5
this.Control[iCurrent+7]=this.gb_2
this.Control[iCurrent+8]=this.gb_1
this.Control[iCurrent+9]=this.cb_1
this.Control[iCurrent+10]=this.dw_documenti_lista
this.Control[iCurrent+11]=this.cb_procedue
this.Control[iCurrent+12]=this.cb_liste_dist
this.Control[iCurrent+13]=this.cb_elimina_liste_dist
this.Control[iCurrent+14]=this.cb_note_esterne
this.Control[iCurrent+15]=this.cb_dettagli
this.Control[iCurrent+16]=this.cb_paragrafi_iso
this.Control[iCurrent+17]=this.rb_livello_a
this.Control[iCurrent+18]=this.rb_livello_b
this.Control[iCurrent+19]=this.rb_livello_c
this.Control[iCurrent+20]=this.rb_livello_tutti
this.Control[iCurrent+21]=this.dw_documenti_det_1
this.Control[iCurrent+22]=this.dw_documenti_det_2
this.Control[iCurrent+23]=this.dw_folder
this.Control[iCurrent+24]=this.dw_documenti_det_3
end on

on w_fold_generici.destroy
call super::destroy
destroy(this.rb_1)
destroy(this.rb_2)
destroy(this.rb_6)
destroy(this.rb_3)
destroy(this.rb_4)
destroy(this.rb_5)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.cb_1)
destroy(this.dw_documenti_lista)
destroy(this.cb_procedue)
destroy(this.cb_liste_dist)
destroy(this.cb_elimina_liste_dist)
destroy(this.cb_note_esterne)
destroy(this.cb_dettagli)
destroy(this.cb_paragrafi_iso)
destroy(this.rb_livello_a)
destroy(this.rb_livello_b)
destroy(this.rb_livello_c)
destroy(this.rb_livello_tutti)
destroy(this.dw_documenti_det_1)
destroy(this.dw_documenti_det_2)
destroy(this.dw_folder)
destroy(this.dw_documenti_det_3)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_documenti_det_1,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_documenti_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_documenti_det_3,"cod_resp_conserv_doc",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
end event

type rb_1 from radiobutton within w_fold_generici
integer x = 1829
integer y = 60
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Lettura"
end type

on clicked;i_check=1
end on

type rb_2 from radiobutton within w_fold_generici
integer x = 1829
integer y = 120
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Modifica "
end type

on clicked;i_check=2
end on

type rb_6 from radiobutton within w_fold_generici
integer x = 1829
integer y = 180
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Elimina"
end type

on clicked;i_check=6

end on

type rb_3 from radiobutton within w_fold_generici
integer x = 1829
integer y = 240
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Verifica"
end type

on clicked;i_check=3
end on

type rb_4 from radiobutton within w_fold_generici
integer x = 1829
integer y = 300
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Approva"
end type

on clicked;i_check=4
end on

type rb_5 from radiobutton within w_fold_generici
integer x = 1829
integer y = 360
integer width = 251
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Valida"
end type

on clicked;i_check=5
end on

type gb_2 from groupbox within w_fold_generici
integer x = 1943
integer y = 560
integer width = 357
integer height = 340
integer taborder = 51
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
string text = "Visualizza"
end type

type gb_1 from groupbox within w_fold_generici
integer x = 1760
integer width = 549
integer height = 440
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Opzioni"
end type

type cb_1 from commandbutton within w_fold_generici
integer x = 1760
integer y = 440
integer width = 549
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_nome_documento,ls_risposta
long ll_progr,ll_riga_dw, ll_risposta

if i_check = 6 then
	ll_risposta = g_mb.messagebox("OMNIA","Sei sicuro di voler eliminare il documento selezionato?",Question!,OKCancel! ,2)
	if ll_risposta <> 1  then return
end if

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return
setpointer(hourglass!)
ll_progr = dw_documenti_lista.GetItemNumber(dw_documenti_lista.getrow(),"progr")
SELECT doc_generici.path_documento
INTO   :ls_nome_documento  
FROM   doc_generici  
WHERE  (doc_generici.cod_azienda = :s_cs_xx.cod_azienda) AND  
		 (doc_generici.progr = :ll_progr);
if sqlca.sqlcode <> 0 then				//  è un documento esterno

	choose case i_check
		case 6
			wf_elimina_doc_esterno(ll_progr)
		case 2
			wf_modifica_doc_esterno(ll_progr)
		case 1
		   g_mb.messagebox("OMNIA","Questo è un documento esterno; per visionarlo fare click sul bottone Documento", StopSign!)
		case else
			ls_risposta=f_esepri(i_check,ls_nome_documento,ll_progr)
	end choose
else
   ls_risposta=f_esepri(i_check,ls_nome_documento,ll_progr)
end if
ll_riga_dw=dw_documenti_lista.getrow()
dw_documenti_lista.change_dw_current()
dw_documenti_lista.triggerevent("pcd_retrieve")
dw_documenti_lista.setrow(ll_riga_dw)
setpointer(arrow!)
rb_1.triggerevent("Clicked")
rb_1.checked = true

end event

type dw_documenti_lista from uo_cs_xx_dw within w_fold_generici
integer x = 23
integer y = 20
integer width = 1714
integer height = 500
integer taborder = 130
string dataobject = "d_documenti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,'G',is_autoriz_liv_doc, is_livello_doc)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_modify;call super::pcd_modify;cb_dettagli.enabled = true
cb_elimina_liste_dist.enabled = true
cb_liste_dist.enabled = true
cb_note_esterne.enabled = true
if this.getitemstring(this.getrow(),"flag_doc_automatico") = "S" then
	dw_documenti_det_1.object.livello_documento.tabsequence = 0
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event updatestart;call super::updatestart;if ib_in_new then

   long ll_num_registrazione

   select  max(documenti.progr)
     into  :ll_num_registrazione
     from  documenti
     where documenti.cod_azienda = :s_cs_xx.cod_azienda;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"progr", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "progr", 1)
   end if

   ib_in_new = false
end if

end event

event pcd_new;call super::pcd_new;ib_in_new = true
cb_dettagli.enabled = true
cb_elimina_liste_dist.enabled = true
cb_liste_dist.enabled = true
cb_note_esterne.enabled = true
end event

event pcd_view;call super::pcd_view;ib_in_new = false
cb_dettagli.enabled = true
cb_elimina_liste_dist.enabled = true
cb_liste_dist.enabled = true
cb_note_esterne.enabled = true
end event

type cb_procedue from cb_apri_manuale within w_fold_generici
integer x = 1943
integer y = 920
integer width = 370
integer height = 80
integer taborder = 10
string text = "&Procedure"
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLA")
end on

type cb_liste_dist from commandbutton within w_fold_generici
integer x = 1943
integer y = 1320
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Liste Dist."
end type

event clicked;long ll_progr_lista_dist,ll_progr
string ls_path_doc_from,ls_path_doc_to

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return
ll_progr=dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"progr")
SELECT doc_generici.path_documento  
INTO :ls_path_doc_from 
FROM doc_generici  
WHERE ( doc_generici.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( doc_generici.progr = :ll_progr )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Impossibile associare lista di distribuzione ai documenti esterni", StopSign!)
   return
end if

select progr_lista_dist
into   :ll_progr_lista_dist
from   documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr=:ll_progr;

if isnull(ll_progr_lista_dist) or ll_progr_lista_dist=0 then
	
	ll_progr_lista_dist=f_progressivo_lista_dist()
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_dist	
	if fileexists(ls_path_doc_from) then
		ls_path_doc_to=left(ls_path_doc_from,len(ls_path_doc_from)-4) + right(ls_path_doc_from,3) + ".doc"
		w_POManager_STD.EXT.fu_FileCopy(ls_path_doc_from,ls_path_doc_to)
	end if
	s_cs_xx.parametri.parametro_s_1 = ls_path_doc_to
	
	update documenti
	set    progr_lista_dist=:ll_progr_lista_dist
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr=:ll_progr;

	window_open(w_seleziona_lista_dist,0)

else
	s_cs_xx.parametri.parametro_ul_2 = ll_progr_lista_dist
	window_open(w_liste_dist_doc,0)

end if
end event

type cb_elimina_liste_dist from commandbutton within w_fold_generici
event clicked pbm_bnclicked
integer x = 1943
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina L.D."
end type

event clicked;long   ll_progr,ll_progr_lista_dist,ll_risposta
string ls_path_doc_from ,ls_flag_approvazione, ls_flag_autorizzazione, & 
		 ls_flag_validazione, ls_flag_lettura,ls_flag_modifica,ls_flag_elimina 
		 
//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT flag_approvazione,
//		 flag_autorizzazione,
//		 flag_validazione,
//		 flag_lettura,
//		 flag_modifica,
//		 flag_elimina
//INTO   :ls_flag_approvazione, 
//	    :ls_flag_autorizzazione,
//	    :ls_flag_validazione, 
//	    :ls_flag_lettura,
//	    :ls_flag_modifica, 		 
//	    :ls_flag_elimina &
//FROM   mansionari
//WHERE  cod_azienda=:s_cs_xx.cod_azienda 
//AND 	 cod_utente =:s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'
ls_flag_lettura = 'N'
ls_flag_modifica = 'N'
ls_flag_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.lettura) then ls_flag_lettura = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'
//--- Fine modifica Giulio

if ls_flag_modifica="N" then 
	g_mb.messagebox("Omnia","Non si possiede il privilegio di modifica del documento, pertanto non è possibile eliminare la lista di distribuzione associata",exclamation!)
end if

ll_risposta = g_mb.messagebox("Omnia", "Sei sicuro di voler eliminare la lista di distribuzione associata?",  Exclamation!, YesNo!, 2)

if ll_risposta=2 then return

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return

ll_progr=dw_documenti_lista.getitemnumber(dw_documenti_lista.getrow(),"progr")

SELECT doc_generici.path_documento  
INTO :ls_path_doc_from 
FROM doc_generici  
WHERE ( doc_generici.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( doc_generici.progr = :ll_progr )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Impossibile associare lista di distribuzione ai documenti esterni", StopSign!)
   return
end if

select progr_lista_dist
into   :ll_progr_lista_dist
from   documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr=:ll_progr;
if isnull(ll_progr_lista_dist) or ll_progr_lista_dist=0 then
	g_mb.messagebox("OMNIA","Non risulta associata al documento alcuna lista di distribuzione", StopSign!)
	return
end if

update documenti
set    progr_lista_dist=0
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr=:ll_progr;
choose case sqlca.sqlcode
	case 100
		g_mb.messagebox("OMNIA","Impossibile disassociare il numero lista: non trovo il documento "+string(ll_progr), StopSign!)
	case 0
		
	case else
		g_mb.messagebox("OMNIA","Errore durante la disassociazione lista di distribuzione. Errore SQL: "+sqlca.sqlerrtext, StopSign!)
end choose
if f_elimina_liste_dist_doc(ll_progr_lista_dist) = 0 then
	g_mb.messagebox("OMNIA", "Operazione eseguita con successo", Information!)
end if
end event

type cb_note_esterne from commandbutton within w_fold_generici
event clicked pbm_bnclicked
integer x = 1943
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_prodotto, ls_cod_blob, ls_db
long ll_i, ll_progr
blob lbl_null
integer li_risposta
transaction sqlcb

setnull(lbl_null)

if isnull(dw_documenti_lista.getitemstring(dw_documenti_lista.getrow(),"nome_documento")) then return
ll_i = dw_documenti_lista.getrow()
ll_progr = dw_documenti_lista.getitemnumber(ll_i, "progr")

// 11-07-2002 modifiche Michela: controllo dell'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob documenti.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       documenti
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           progr = :ll_progr
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob documenti.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       documenti
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           progr = :ll_progr;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob documenti
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              progr = :ll_progr
		using      sqlcb;
		
		destroy sqlcb;
       		
	else
		
	   updateblob documenti
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              progr = :ll_progr;
					  
	end if

   update     documenti
   set        path_documento_esterno = :s_cs_xx.parametri.parametro_s_1
   where      cod_azienda = :s_cs_xx.cod_azienda and
              progr = :ll_progr;
   commit;
end if

end event

type cb_dettagli from commandbutton within w_fold_generici
integer x = 1943
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pe&rcorso"
end type

event clicked;long ll_selected[]

dw_documenti_lista.get_selected_rows(ll_selected)
if upperbound(ll_selected) > 0 then
	window_open_parm(w_documenti_generici, -1, dw_documenti_lista)
	else
	g_mb.messagebox("OMNIA","Selezionare una riga dalla lista Documenti", StopSign!)
end if
end event

type cb_paragrafi_iso from commandbutton within w_fold_generici
event clicked pbm_bnclicked
integer x = 1943
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Para&grafi"
end type

event clicked;long ll_selected[]
string ls_flag_modifica

SELECT  mansionari.flag_modifica
INTO    :ls_flag_modifica
FROM    mansionari
WHERE ( mansionari.cod_azienda = :s_cs_xx.cod_azienda  ) AND 
	   ( mansionari.cod_utente = :s_cs_xx.cod_utente  );
if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA", "Errore in ricerca nel DB: " + SQLCA.SQLErrText + "	OPERAZIONE BLOCCATA",StopSign!)
   return
end if

if ls_flag_modifica <> "S" then
	g_mb.messagebox("OMNIA", "Attenzione! Solo chi possiede il privilegio di MODIFICA può accedere a questa maschera!",StopSign!)
   return
end if

dw_documenti_lista.get_selected_rows(ll_selected)
if upperbound(ll_selected) > 0 then
	window_open_parm(w_paragrafi_iso_documenti, -1, dw_documenti_lista)
	else
	g_mb.messagebox("OMNIA","Selezionare una riga dalla lista Documenti", StopSign!)
end if
end event

type rb_livello_a from radiobutton within w_fold_generici
integer x = 1966
integer y = 620
integer width = 297
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Livello A"
end type

event clicked;is_livello_doc = "A"
parent.triggerevent("pc_retrieve")

end event

type rb_livello_b from radiobutton within w_fold_generici
integer x = 1966
integer y = 680
integer width = 297
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Livello B"
end type

event clicked;is_livello_doc = "B"
parent.triggerevent("pc_retrieve")

end event

type rb_livello_c from radiobutton within w_fold_generici
integer x = 1966
integer y = 740
integer width = 297
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Livello C"
end type

event clicked;is_livello_doc = "C"
parent.triggerevent("pc_retrieve")

end event

type rb_livello_tutti from radiobutton within w_fold_generici
integer x = 1966
integer y = 800
integer width = 297
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Tutti"
end type

event clicked;is_livello_doc = "%"
parent.triggerevent("pc_retrieve")

end event

type dw_documenti_det_1 from uo_cs_xx_dw within w_fold_generici
integer x = 46
integer y = 660
integer width = 1851
integer height = 880
integer taborder = 90
string dataobject = "d_documenti_det_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;string ls_livello, ls_ord_livello, ls_codice_doc, ls_des_doc

if i_extendmode then
	if i_colname = "prog_ordinamento_livello" then
		ls_livello = this.getitemstring(this.getrow(),"livello_documento")
		select prog_ordinamento_livello, progr, des_documento
		into   :ls_ord_livello, :ls_codice_doc, :ls_des_doc
		from   documenti
		where  (documenti.cod_azienda = :s_cs_xx.cod_azienda) and
				 (documenti.livello_documento = :ls_livello) and
		       (documenti.prog_ordinamento_livello = :i_coltext);
		if (sqlca.sqlcode = 0) and (i_coltext <> "0") then
			g_mb.messagebox("OMNIA","Ordinamento già esistente utilizzato per il documento "+ls_codice_doc+" "+ls_des_doc,stopsign!)
			pcca.error = c_valfailed
		end if
	end if
end if
end event

type dw_documenti_det_2 from uo_cs_xx_dw within w_fold_generici
integer x = 46
integer y = 660
integer width = 1829
integer height = 876
integer taborder = 70
string dataobject = "d_documenti_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_fold_generici
integer x = 23
integer y = 540
integer width = 1897
integer height = 1040
integer taborder = 110
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Dettaglio"
      SetFocus(dw_documenti_det_1)
   CASE "&Modifiche"
      SetFocus(dw_documenti_det_2)
END CHOOSE

end event

type dw_documenti_det_3 from uo_cs_xx_dw within w_fold_generici
integer x = 46
integer y = 660
integer width = 1829
integer height = 860
integer taborder = 80
string dataobject = "d_documenti_det_3"
boolean border = false
end type

