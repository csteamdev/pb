﻿$PBExportHeader$w_fold_old_documenti.srw
$PBExportComments$Window Old Documenti in unica soluzione con folder
forward
global type w_fold_old_documenti from w_cs_xx_principale
end type
type cb_5 from commandbutton within w_fold_old_documenti
end type
type dw_folder from u_folder within w_fold_old_documenti
end type
type dw_contratti from uo_cs_xx_dw within w_fold_old_documenti
end type
type dw_generici from uo_cs_xx_dw within w_fold_old_documenti
end type
type dw_progetti from uo_cs_xx_dw within w_fold_old_documenti
end type
end forward

global type w_fold_old_documenti from w_cs_xx_principale
int Width=3054
int Height=1393
boolean TitleBar=true
string Title="Revisioni Precedenti Documenti"
cb_5 cb_5
dw_folder dw_folder
dw_contratti dw_contratti
dw_generici dw_generici
dw_progetti dw_progetti
end type
global w_fold_old_documenti w_fold_old_documenti

type variables
string i_percorso_word
string i_password
integer i_check
long i_riga_corrente
end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;string ls_risposta,ls_pdoc,ls_pmod, ls_pcom
integer li_num_edizioni

windowobject l_objects[ ]

l_objects[1] = dw_generici
dw_folder.fu_AssignTab(1, "D. &Generici", l_Objects[])
l_objects[1] = dw_contratti
dw_folder.fu_AssignTab(2, "D. &Contratti", l_Objects[])
l_objects[1] = dw_progetti
dw_folder.fu_AssignTab(3, "D. &Progetti", l_Objects[])


dw_folder.fu_FolderCreate(3,4)

dw_generici.set_dw_options(sqlca,c_NullDW,c_nomodify+c_nonew+c_selectonrowfocuschange,c_Default)
dw_contratti.set_dw_options(sqlca,c_NullDW,c_nomodify+c_nonew+c_selectonrowfocuschange,c_Default)
dw_progetti.set_dw_options(sqlca,c_NullDW,c_nomodify+c_nonew+c_selectonrowfocuschange,c_Default)
iuo_dw_main = dw_generici

dw_folder.fu_SelectTab(1)

ls_risposta=f_parametri(i_password,i_percorso_word,ls_pdoc,ls_pmod,li_num_edizioni,ls_pcom)
end on

on w_fold_old_documenti.create
int iCurrent
call w_cs_xx_principale::create
this.cb_5=create cb_5
this.dw_folder=create dw_folder
this.dw_contratti=create dw_contratti
this.dw_generici=create dw_generici
this.dw_progetti=create dw_progetti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_5
this.Control[iCurrent+2]=dw_folder
this.Control[iCurrent+3]=dw_contratti
this.Control[iCurrent+4]=dw_generici
this.Control[iCurrent+5]=dw_progetti
end on

on w_fold_old_documenti.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_5)
destroy(this.dw_folder)
destroy(this.dw_contratti)
destroy(this.dw_generici)
destroy(this.dw_progetti)
end on

type cb_5 from commandbutton within w_fold_old_documenti
int X=2629
int Y=1201
int Width=366
int Height=81
int TabOrder=30
string Text="&Aggiorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;string ls_risposta

setpointer(hourglass!)

ls_risposta=f_aggiorna_doc()
dw_generici.postevent("pcd_retrieve")
dw_contratti.postevent("pcd_retrieve")
dw_progetti.postevent("pcd_retrieve")

setpointer(arrow!)
end on

type dw_folder from u_folder within w_fold_old_documenti
int X=23
int Y=21
int Width=2972
int Height=1161
int TabOrder=50
end type

on itemchanged;call u_folder::itemchanged;CHOOSE CASE i_SelectedTabName
   CASE "D. &Generici"
      SetFocus(dw_generici)
   CASE "D. &Contratti"
      SetFocus(dw_contratti)
   CASE "D. &Progetti"
      SetFocus(dw_progetti)
END CHOOSE

end on

type dw_contratti from uo_cs_xx_dw within w_fold_old_documenti
int X=69
int Y=141
int Width=2881
int Height=1001
int TabOrder=20
string DataObject="d_old_documenti_contratti"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,"C")

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_generici from uo_cs_xx_dw within w_fold_old_documenti
int X=69
int Y=141
int Width=2881
int Height=1001
int TabOrder=40
string DataObject="d_old_documenti_generici"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,"g")

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_progetti from uo_cs_xx_dw within w_fold_old_documenti
int X=69
int Y=141
int Width=2881
int Height=1001
int TabOrder=10
string DataObject="d_old_documenti_progetti"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,"p")

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

