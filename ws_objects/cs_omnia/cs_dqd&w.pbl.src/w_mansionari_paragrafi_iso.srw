﻿$PBExportHeader$w_mansionari_paragrafi_iso.srw
$PBExportComments$Finestra Mansionari Paragrafi ISO
forward
global type w_mansionari_paragrafi_iso from w_cs_xx_principale
end type
type dw_mansionari_paragrafi_iso from uo_cs_xx_dw within w_mansionari_paragrafi_iso
end type
end forward

global type w_mansionari_paragrafi_iso from w_cs_xx_principale
int Width=2433
int Height=1141
boolean TitleBar=true
string Title="Funzioni dei Responsabili"
dw_mansionari_paragrafi_iso dw_mansionari_paragrafi_iso
end type
global w_mansionari_paragrafi_iso w_mansionari_paragrafi_iso

type variables
boolean ib_in_new
end variables

event pc_setwindow;call super::pc_setwindow;dw_mansionari_paragrafi_iso.set_dw_key("cod_azienda")
dw_mansionari_paragrafi_iso.set_dw_key("cod_resp_divisione")
dw_mansionari_paragrafi_iso.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_mansionari_paragrafi_iso
dw_mansionari_paragrafi_iso.ib_proteggi_chiavi = false


end event

on w_mansionari_paragrafi_iso.create
int iCurrent
call w_cs_xx_principale::create
this.dw_mansionari_paragrafi_iso=create dw_mansionari_paragrafi_iso
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_mansionari_paragrafi_iso
end on

on w_mansionari_paragrafi_iso.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_mansionari_paragrafi_iso)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_mansionari_paragrafi_iso,"cod_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo",&
                 "(tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) and " + &
					  "((tab_paragrafi_iso.flag_blocco <> 'S') or (tab_paragrafi_iso.flag_blocco = 'S' and tab_paragrafi_iso.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_mansionari_paragrafi_iso from uo_cs_xx_dw within w_mansionari_paragrafi_iso
int X=23
int Y=21
int Width=2355
int Height=1001
int TabOrder=20
string DataObject="d_mansionari_paragrafi_iso"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx
string ls_cod_resp_divisione

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

ls_cod_resp_divisione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_resp_divisione")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_resp_divisione")) THEN
      SetItem(l_Idx, "cod_resp_divisione", ls_cod_resp_divisione)
   END IF
NEXT


end event

event updatestart;call super::updatestart;//long ll_cont
//string ls_cod_resp_divisione
//
//ls_cod_resp_divisione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "cod_resp_divisione")
//SELECT count(flag_rif_primario)  
//INTO   :ll_cont  
//FROM   paragrafi_iso_documenti
//WHERE  ( paragrafi_iso_documenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//		 ( paragrafi_iso_documenti.progr = :ll_cod_documento ) AND  
//		 ( paragrafi_iso_documenti.flag_rif_primario = 'S' )   ;
//if sqlca.sqlcode <> 0 then
//	messagebox("OMNIA","Errore durante la verifica esistenza riferimento primario: " + sqlca.sqlerrtext, Information!)
//end if		
//if (isnull(ll_cont) or (ll_cont = 0)) and (getitemstring(this.getrow(),"flag_rif_primario") <> "S") then
//	messagebox("OMNIA", "Attenzione: per ogni documento deve esistere un riferimento primario",StopSign!)
//	pcca.error = c_valfailed		
//end if   
//
//
//if getitemstring(this.getrow(),"flag_rif_primario") = "S" then
//	if ll_cont > 0 then
//		messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
//		pcca.error = c_valfailed		
//	end if   
//else
//	if ll_cont > 1 then
//		messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
//		pcca.error = c_valfailed		
//	end if   
//end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_resp_divisione

ls_cod_resp_divisione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_resp_divisione")
l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_resp_divisione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

