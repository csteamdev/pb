﻿$PBExportHeader$w_paragrafi_iso_documenti.srw
$PBExportComments$Finestra Associazione Documenti a Paragrafi Iso
forward
global type w_paragrafi_iso_documenti from w_cs_xx_principale
end type
type dw_paragrafi_iso_documenti from uo_cs_xx_dw within w_paragrafi_iso_documenti
end type
end forward

global type w_paragrafi_iso_documenti from w_cs_xx_principale
int Width=1925
int Height=1141
boolean TitleBar=true
string Title="Riferimento Paragrafi Normativa"
dw_paragrafi_iso_documenti dw_paragrafi_iso_documenti
end type
global w_paragrafi_iso_documenti w_paragrafi_iso_documenti

type variables
boolean ib_in_new
end variables

event pc_setwindow;call super::pc_setwindow;dw_paragrafi_iso_documenti.set_dw_key("cod_azienda")
dw_paragrafi_iso_documenti.set_dw_key("progr")
dw_paragrafi_iso_documenti.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_paragrafi_iso_documenti
dw_paragrafi_iso_documenti.ib_proteggi_chiavi = false


end event

on w_paragrafi_iso_documenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_paragrafi_iso_documenti=create dw_paragrafi_iso_documenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_paragrafi_iso_documenti
end on

on w_paragrafi_iso_documenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_paragrafi_iso_documenti)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_paragrafi_iso_documenti,"cod_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo",&
                 "(tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_paragrafi_iso.flag_blocco <> 'S') or (tab_paragrafi_iso.flag_blocco = 'S' and tab_paragrafi_iso.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type dw_paragrafi_iso_documenti from uo_cs_xx_dw within w_paragrafi_iso_documenti
int X=23
int Y=21
int Width=1852
int Height=1001
int TabOrder=20
string DataObject="d_paragrafi_iso_documenti"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_cod_documento

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

ll_cod_documento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progr")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "progr")) or GetItemnumber(l_Idx, "progr") = 0 THEN
      SetItem(l_Idx, "progr", ll_cod_documento)
   END IF
NEXT


end event

event updatestart;call super::updatestart;long ll_cod_documento, ll_cont

ll_cod_documento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progr")
SELECT count(flag_rif_primario)  
INTO   :ll_cont  
FROM   paragrafi_iso_documenti
WHERE  ( paragrafi_iso_documenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		 ( paragrafi_iso_documenti.progr = :ll_cod_documento ) AND  
		 ( paragrafi_iso_documenti.flag_rif_primario = 'S' )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante la verifica esistenza riferimento primario: " + sqlca.sqlerrtext, Information!)
end if		
if (isnull(ll_cont) or (ll_cont = 0)) and (getitemstring(this.getrow(),"flag_rif_primario") <> "S") then
	g_mb.messagebox("OMNIA", "Attenzione: per ogni documento deve esistere un riferimento primario",StopSign!)
	pcca.error = c_valfailed		
end if   


if getitemstring(this.getrow(),"flag_rif_primario") = "S" then
	if ll_cont > 0 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
else
	if ll_cont > 1 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_cod_documento

ll_cod_documento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progr")
l_Error = Retrieve(s_cs_xx.cod_azienda, ll_cod_documento)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

