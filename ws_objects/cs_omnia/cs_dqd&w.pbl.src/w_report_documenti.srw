﻿$PBExportHeader$w_report_documenti.srw
$PBExportComments$Finestra Report Documenti
forward
global type w_report_documenti from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_documenti
end type
type cb_report from commandbutton within w_report_documenti
end type
type cb_selezione from commandbutton within w_report_documenti
end type
type dw_selezione from uo_cs_xx_dw within w_report_documenti
end type
type dw_report from uo_cs_xx_dw within w_report_documenti
end type
end forward

global type w_report_documenti from w_cs_xx_principale
integer width = 3557
integer height = 1664
string title = "Report Registro Delle Norme in Vigore"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_documenti w_report_documenti

event pc_setwindow;call super::pc_setwindow;string			ls_expression, ls_AZP

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2250
this.height = 1000  //805



select stringa
into   :ls_AZP
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull( ls_AZP ) and ls_AZP<>"" then
	//sintexcal
	ls_expression = " det_documenti_nome_documento "
else
	//no sintexcal
	ls_expression = " det_documenti_anno_registrazione + '/' +  det_documenti_num_registrazione + ' - ' +  det_documenti_nome_documento "
end if

dw_report.object.compute_1.expression = ls_expression




end event

on w_report_documenti.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_documenti.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_resp_conserv_doc",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//Donato 30/01/2009 aggiunti filtri per approvatore e verificatore -------------------------
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_resp_divisione_approva",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_resp_divisione_autorizza",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//----------------------------------------------------------------------------------------------------------						  
end event

type cb_annulla from commandbutton within w_report_documenti
integer x = 1394
integer y = 732
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_documenti
integer x = 1783
integer y = 732
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_area, ls_cod_resp_divisione, ls_livello_documento, ls_resp_conserv_doc
string ls_flag_registrazione_qualita
long ll_da_num_ordinamento, ll_a_num_ordinamento
string ls_cod_resp_divisione_approva, ls_cod_resp_divisione_autorizza


ls_cod_area = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
ls_cod_resp_divisione = dw_selezione.getitemstring(1,"rs_cod_resp_divisione")
ls_livello_documento = dw_selezione.getitemstring(1,"rs_livello_documento") 
ls_resp_conserv_doc = dw_selezione.getitemstring(1,"rs_resp_conserv_doc")
ls_flag_registrazione_qualita = dw_selezione.getitemstring(1,"rs_flag_registrazione_qualita")
ll_da_num_ordinamento = dw_selezione.getitemnumber(1,"rn_da_num_ordinamento")
ll_a_num_ordinamento = dw_selezione.getitemnumber(1,"rn_a_num_ordinamento")

//Donato 30/01/2009 aggiunti filtri per approvatore e verificatore
ls_cod_resp_divisione_approva = dw_selezione.getitemstring(1,"rs_cod_resp_divisione_approva")
ls_cod_resp_divisione_autorizza = dw_selezione.getitemstring(1,"rs_cod_resp_divisione_autorizza")
//-------------------------------------------------------------------------------

if isnull(ls_cod_area ) then ls_cod_area = "%"
//if isnull(ls_cod_resp_divisione) then  ls_cod_resp_divisione  = "%"
if isnull(ls_livello_documento) then  ls_livello_documento  = "%"
if isnull(ls_resp_conserv_doc) then  ls_resp_conserv_doc  = "%"
if isnull(ls_flag_registrazione_qualita) then  ls_flag_registrazione_qualita  = "%"
if isnull(ll_da_num_ordinamento) then  ll_da_num_ordinamento  = 0
if isnull(ll_a_num_ordinamento) or ll_a_num_ordinamento = 0 then  ll_a_num_ordinamento  = 99999

//Donato 30/01/2009 aggiunti filtri per approvatore e verificatore

if ls_cod_resp_divisione="" then  setnull(ls_cod_resp_divisione)
if ls_cod_resp_divisione_approva="" then  setnull(ls_cod_resp_divisione_approva)
if ls_cod_resp_divisione_autorizza = "" then  setnull(ls_cod_resp_divisione_autorizza)
//---------------------------------------------------------------------------------



dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()
long ll_row

//ll_row = dw_report.retrieve(s_cs_xx.cod_azienda, &
//						ls_cod_area, &
//						ls_cod_resp_divisione, &
//						ls_livello_documento, &
//						ll_da_num_ordinamento, &
//						ll_a_num_ordinamento, &
//						ls_resp_conserv_doc, &
//						ls_flag_registrazione_qualita)
						
ll_row = dw_report.retrieve(s_cs_xx.cod_azienda, &
						ls_cod_area, &
						ls_cod_resp_divisione, &
						ls_livello_documento, &
						ll_da_num_ordinamento, &
						ll_a_num_ordinamento, &
						ls_resp_conserv_doc, &
						ls_flag_registrazione_qualita,&
						ls_cod_resp_divisione_approva,&
						ls_cod_resp_divisione_autorizza)
						
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_documenti
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2250
parent.height = 1000  //805

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_documenti
integer x = 23
integer y = 20
integer width = 2171
integer height = 732
integer taborder = 20
string dataobject = "d_selezione_report_documenti_2"
boolean border = false
end type

type dw_report from uo_cs_xx_dw within w_report_documenti
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_documenti_iso9000_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

