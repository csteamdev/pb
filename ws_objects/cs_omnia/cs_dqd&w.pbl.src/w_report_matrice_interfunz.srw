﻿$PBExportHeader$w_report_matrice_interfunz.srw
$PBExportComments$Finestra Report Matrice Interfunzionale
forward
global type w_report_matrice_interfunz from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_matrice_interfunz
end type
end forward

global type w_report_matrice_interfunz from w_cs_xx_principale
integer width = 3552
integer height = 1560
string title = "Report Matrice Interfunzionale"
dw_report dw_report
end type
global w_report_matrice_interfunz w_report_matrice_interfunz

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)
dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report


end event

on w_report_matrice_interfunz.create
int iCurrent
call super::create
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
end on

on w_report_matrice_interfunz.destroy
call super::destroy
destroy(this.dw_report)
end on

type dw_report from uo_cs_xx_dw within w_report_matrice_interfunz
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_matrice_interfunz"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

