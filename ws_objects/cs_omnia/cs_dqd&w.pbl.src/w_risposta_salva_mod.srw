﻿$PBExportHeader$w_risposta_salva_mod.srw
$PBExportComments$Window di salvataggio modelli
forward
global type w_risposta_salva_mod from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_risposta_salva_mod
end type
type cb_2 from commandbutton within w_risposta_salva_mod
end type
end forward

global type w_risposta_salva_mod from w_cs_xx_risposta
integer width = 1280
integer height = 316
cb_1 cb_1
cb_2 cb_2
end type
global w_risposta_salva_mod w_risposta_salva_mod

type variables
string i_password
string i_percorso_word
end variables

event open;call super::open;string ls_pdoc,ls_pmod,ls_risposta,ls_pcom
integer li_num_edizioni

ls_risposta=f_parametri(i_password,i_percorso_word,ls_pdoc,ls_pmod,li_num_edizioni,ls_pcom)
end event

on w_risposta_salva_mod.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_2
end on

on w_risposta_salva_mod.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_2)
end on

type cb_1 from commandbutton within w_risposta_salva_mod
integer x = 69
integer y = 60
integer width = 544
integer height = 108
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Salva Modello"
end type

event clicked;string d_risposta,d_dati,ls_valore
integer li_versione_word,li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vew", ls_valore)

li_versione_word = integer(ls_valore)

if li_versione_word = 6 then
	d_risposta=f_word("strumproteggidocumento.passworddoc="+char(34)+ i_password +char(34),i_percorso_word)
	d_risposta=f_word("filesalva",i_percorso_word)
else
	d_risposta=f_word("toolsprotectdocument.passworddoc="+char(34)+ i_password +char(34),i_percorso_word)
	d_risposta=f_word("filesave",i_percorso_word)
end if

close(parent)
end event

type cb_2 from commandbutton within w_risposta_salva_mod
integer x = 640
integer y = 60
integer width = 544
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

