﻿$PBExportHeader$w_scad_validazioni.srw
$PBExportComments$Scadenziario Documenti Emessi, Approvati, Autorizzati, Non Validati per Data  - DA PROVARE BENE
forward
global type w_scad_validazioni from w_cs_xx_principale
end type
type gb_selezioni from groupbox within w_scad_validazioni
end type
type st_3 from statictext within w_scad_validazioni
end type
type em_data_inizio from editmask within w_scad_validazioni
end type
type st_4 from statictext within w_scad_validazioni
end type
type em_data_fine from editmask within w_scad_validazioni
end type
type st_da_data_scad from statictext within w_scad_validazioni
end type
type st_1 from statictext within w_scad_validazioni
end type
type em_da_scadenza from editmask within w_scad_validazioni
end type
type em_a_scadenza from editmask within w_scad_validazioni
end type
type dw_scad_validazioni from uo_cs_xx_dw within w_scad_validazioni
end type
end forward

global type w_scad_validazioni from w_cs_xx_principale
int Width=3434
int Height=1425
boolean TitleBar=true
string Title="Scadenzario Validazioni"
gb_selezioni gb_selezioni
st_3 st_3
em_data_inizio em_data_inizio
st_4 st_4
em_data_fine em_data_fine
st_da_data_scad st_da_data_scad
st_1 st_1
em_da_scadenza em_da_scadenza
em_a_scadenza em_a_scadenza
dw_scad_validazioni dw_scad_validazioni
end type
global w_scad_validazioni w_scad_validazioni

event pc_setwindow;call super::pc_setwindow;dw_scad_validazioni.set_dw_options(sqlca, &
                                   pcca.null_object, &
											  c_nonew + &
											  c_nodelete + &
											  c_nomodify + &
											  c_disableCC , &
											  c_default)

iuo_dw_main = dw_scad_validazioni

em_data_inizio.text = string(relativedate(today(),-90),"dd/mm/yyyy")
em_data_fine.text = string(today(),"dd/mm/yyyy")
em_da_scadenza.text = string(today(),"dd/mm/yyyy")
em_a_scadenza.text = string(relativedate(today(),+90),"dd/mm/yyyy")
end event

on w_scad_validazioni.create
int iCurrent
call w_cs_xx_principale::create
this.gb_selezioni=create gb_selezioni
this.st_3=create st_3
this.em_data_inizio=create em_data_inizio
this.st_4=create st_4
this.em_data_fine=create em_data_fine
this.st_da_data_scad=create st_da_data_scad
this.st_1=create st_1
this.em_da_scadenza=create em_da_scadenza
this.em_a_scadenza=create em_a_scadenza
this.dw_scad_validazioni=create dw_scad_validazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=gb_selezioni
this.Control[iCurrent+2]=st_3
this.Control[iCurrent+3]=em_data_inizio
this.Control[iCurrent+4]=st_4
this.Control[iCurrent+5]=em_data_fine
this.Control[iCurrent+6]=st_da_data_scad
this.Control[iCurrent+7]=st_1
this.Control[iCurrent+8]=em_da_scadenza
this.Control[iCurrent+9]=em_a_scadenza
this.Control[iCurrent+10]=dw_scad_validazioni
end on

on w_scad_validazioni.destroy
call w_cs_xx_principale::destroy
destroy(this.gb_selezioni)
destroy(this.st_3)
destroy(this.em_data_inizio)
destroy(this.st_4)
destroy(this.em_data_fine)
destroy(this.st_da_data_scad)
destroy(this.st_1)
destroy(this.em_da_scadenza)
destroy(this.em_a_scadenza)
destroy(this.dw_scad_validazioni)
end on

type gb_selezioni from groupbox within w_scad_validazioni
int X=23
int Y=21
int Width=3361
int Height=261
int TabOrder=10
string Text="Selezioni"
BorderStyle BorderStyle=StyleLowered!
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_scad_validazioni
int X=46
int Y=101
int Width=823
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Da Data Emissione Documento:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_data_inizio from editmask within w_scad_validazioni
int X=892
int Y=101
int Width=412
int Height=81
int TabOrder=50
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
string MinMax="01/01/1900~~31/12/2999"
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_scad_validazioni
int X=1349
int Y=101
int Width=823
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="A Data Emissione Documento:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_data_fine from editmask within w_scad_validazioni
int X=2195
int Y=101
int Width=412
int Height=81
int TabOrder=20
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="À"
string MinMax="01/01/1900~~31/12/2999"
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_da_data_scad from statictext within w_scad_validazioni
int X=46
int Y=181
int Width=823
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Da Data Scadenza:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_scad_validazioni
int X=1692
int Y=181
int Width=467
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="A Data Scadenza:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_da_scadenza from editmask within w_scad_validazioni
int X=892
int Y=181
int Width=412
int Height=81
int TabOrder=30
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
string MinMax="01/01/1900~~31/12/2999"
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_a_scadenza from editmask within w_scad_validazioni
int X=2195
int Y=181
int Width=412
int Height=81
int TabOrder=40
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
string MinMax="01/01/1900~~31/12/2999"
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_scad_validazioni from uo_cs_xx_dw within w_scad_validazioni
int X=23
int Y=301
int Width=3361
int Height=1001
int TabOrder=0
string DataObject="d_scadenzario_validazioni"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event doubleclicked;call super::doubleclicked;long ll_progr

if (i_extendmode) and (getrow() > 0) then
	ll_progr = this.getitemnumber(this.getrow(),"progr")
	
	
	select doc_generici.progr  
	into   :ll_progr  
	from   doc_generici  
	where  (doc_generici.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			 (doc_generici.progr = :ll_progr )   ;
	if sqlca.sqlcode = 0 then
		if not isvalid(w_fold_generici) then
			window_open(w_fold_generici, -1)
			return
		end if
	end if
end if
end event

event pcd_retrieve;call super::pcd_retrieve;datetime ld_data_1, ld_data_2
string ls_filter, ls_data_1, ls_data_2
LONG  l_Error


ld_data_1 = datetime(date(em_data_inizio.text))
ld_data_2 = datetime(date(em_data_fine.text))
l_error   = retrieve(s_cs_xx.cod_azienda, ld_data_1, ld_data_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

ls_data_1 = char(34) + em_da_scadenza.text + char(34)
ls_data_2 = char(34) + em_a_scadenza.text + char(34)

ls_filter = "cf_data_scadenza >= datetime(date("+ ls_data_1 + ")) and cf_data_scadenza <= datetime(date(" + ls_data_2 + "))"
dw_scad_validazioni.setfilter(ls_filter)
dw_scad_validazioni.filter()
end event

