﻿$PBExportHeader$w_vista_doc_entrata.srw
$PBExportComments$Vista dei Documenti che un Responsabile di Divisione può Ricevere
forward
global type w_vista_doc_entrata from w_cs_xx_principale
end type
type dw_vista_doc_dest from uo_cs_xx_dw within w_vista_doc_entrata
end type
end forward

global type w_vista_doc_entrata from w_cs_xx_principale
int Width=2565
int Height=1045
WindowType WindowType=response!
boolean TitleBar=true
string Title="Documenti in Arrivo"
boolean MinBox=false
boolean MaxBox=false
boolean Resizable=false
dw_vista_doc_dest dw_vista_doc_dest
end type
global w_vista_doc_entrata w_vista_doc_entrata

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_vista_doc_dest.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nomodify+ &
                        c_nodelete+c_disableCC+c_disableCCinsert ,c_default)

end on

on w_vista_doc_entrata.create
int iCurrent
call w_cs_xx_principale::create
this.dw_vista_doc_dest=create dw_vista_doc_dest
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_vista_doc_dest
end on

on w_vista_doc_entrata.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_vista_doc_dest)
end on

type dw_vista_doc_dest from uo_cs_xx_dw within w_vista_doc_entrata
int X=23
int Y=21
int Width=2515
int Height=921
string DataObject="d_vista_doc_entrata"
boolean Border=false
boolean HScrollBar=true
boolean VScrollBar=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error
string ls_resp_divisione

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.cod_utente)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

