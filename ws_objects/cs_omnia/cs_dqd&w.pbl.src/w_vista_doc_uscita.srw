﻿$PBExportHeader$w_vista_doc_uscita.srw
$PBExportComments$Vista dei Documenti che un Responsabile di Divisione può Emettere
forward
global type w_vista_doc_uscita from w_cs_xx_principale
end type
type dw_doc_spedizioniere from uo_cs_xx_dw within w_vista_doc_uscita
end type
end forward

global type w_vista_doc_uscita from w_cs_xx_principale
int Width=2574
int Height=1045
boolean TitleBar=true
string Title="Lista dei Documenti in Uscita"
boolean Resizable=false
dw_doc_spedizioniere dw_doc_spedizioniere
end type
global w_vista_doc_uscita w_vista_doc_uscita

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_doc_spedizioniere.set_dw_options(sqlca,pcca.null_object,c_disableCC+ &
                                    c_disableCCinsert + c_nonew + c_nodelete+ &
                                    c_nomodify,c_noresizedw)
end on

on w_vista_doc_uscita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_doc_spedizioniere=create dw_doc_spedizioniere
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_doc_spedizioniere
end on

on w_vista_doc_uscita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_doc_spedizioniere)
end on

type dw_doc_spedizioniere from uo_cs_xx_dw within w_vista_doc_uscita
int X=23
int Y=21
int Width=2515
int Height=921
string DataObject="d_vista_doc_uscita"
boolean Border=false
boolean HScrollBar=true
boolean VScrollBar=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error
string ls_resp_divisione

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.cod_utente)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

