﻿$PBExportHeader$w_visualizza_modelli.srw
$PBExportComments$Window di visualizzazione modelli
forward
global type w_visualizza_modelli from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_visualizza_modelli
end type
type cb_2 from commandbutton within w_visualizza_modelli
end type
end forward

global type w_visualizza_modelli from w_cs_xx_risposta
integer width = 1371
integer height = 332
cb_1 cb_1
cb_2 cb_2
end type
global w_visualizza_modelli w_visualizza_modelli

type variables
string i_password
string i_percorso_word
end variables

event pc_setwindow;call super::pc_setwindow;string ls_pdoc,ls_pmod,ls_risposta, ls_pcom
integer li_num_edizioni

ls_risposta=f_parametri(i_password,i_percorso_word,ls_pdoc,ls_pmod,li_num_edizioni,ls_pcom)



end event

on w_visualizza_modelli.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_2
end on

on w_visualizza_modelli.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_2)
end on

type cb_1 from commandbutton within w_visualizza_modelli
integer x = 69
integer y = 60
integer width = 562
integer height = 108
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Seleziona Modello"
end type

event clicked;string d_percorso_doc, d_nome_doc,d_risposta,d_dati,ls_valore
integer d_valore
integer li_versione_word,li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vew", ls_valore)

li_versione_word = integer(ls_valore)

d_valore = GetfileopenName("Seleziona il Modello",  &
	d_percorso_doc, d_nome_doc, "DOT",  &
	"Dot  (*.DOT),*.DOT") 

if d_nome_doc <>"" then
  if li_versione_word = 6 then
	  d_risposta=f_word("fileapri " + char(34) + d_percorso_doc + char(34),i_percorso_word)
  else
	  d_risposta=f_word("fileopen " + char(34) + d_percorso_doc + char(34),i_percorso_word)
  end if
else
  g_mb.messagebox("Omnia","Non è stato selezionato alcun modello")
  return
end if	


end event

type cb_2 from commandbutton within w_visualizza_modelli
integer x = 709
integer y = 60
integer width = 562
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

