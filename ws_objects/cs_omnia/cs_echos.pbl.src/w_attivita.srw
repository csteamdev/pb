﻿$PBExportHeader$w_attivita.srw
forward
global type w_attivita from w_cs_xx_principale
end type
type dw_attivita_det_1 from uo_cs_xx_dw within w_attivita
end type
type dw_attivita_lista from uo_cs_xx_dw within w_attivita
end type
end forward

global type w_attivita from w_cs_xx_principale
integer width = 2185
integer height = 1424
string title = "Attività"
boolean maxbox = false
boolean resizable = false
dw_attivita_det_1 dw_attivita_det_1
dw_attivita_lista dw_attivita_lista
end type
global w_attivita w_attivita

on w_attivita.create
int iCurrent
call super::create
this.dw_attivita_det_1=create dw_attivita_det_1
this.dw_attivita_lista=create dw_attivita_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_attivita_det_1
this.Control[iCurrent+2]=this.dw_attivita_lista
end on

on w_attivita.destroy
call super::destroy
destroy(this.dw_attivita_det_1)
destroy(this.dw_attivita_lista)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_attivita_lista

dw_attivita_lista.change_dw_current()

dw_attivita_lista.set_dw_key("cod_azienda")

dw_attivita_lista.set_dw_key("cod_attivita")

dw_attivita_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default, &
                                 c_default)

dw_attivita_det_1.set_dw_options(sqlca, &
                                dw_attivita_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
end event

type dw_attivita_det_1 from uo_cs_xx_dw within w_attivita
integer x = 23
integer y = 740
integer width = 2126
integer height = 580
integer taborder = 20
string dataobject = "d_attivita_det_1"
end type

type dw_attivita_lista from uo_cs_xx_dw within w_attivita
integer x = 23
integer y = 20
integer width = 2126
integer height = 700
integer taborder = 10
string dataobject = "d_attivita_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
next
end event

