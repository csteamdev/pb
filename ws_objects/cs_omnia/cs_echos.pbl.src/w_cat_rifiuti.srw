﻿$PBExportHeader$w_cat_rifiuti.srw
$PBExportComments$Finestra Gestione Categorie Rifiuti
forward
global type w_cat_rifiuti from w_cs_xx_principale
end type
type dw_cat_rifiuti_lista from uo_cs_xx_dw within w_cat_rifiuti
end type
type dw_cat_rifiuti_dett from uo_cs_xx_dw within w_cat_rifiuti
end type
end forward

global type w_cat_rifiuti from w_cs_xx_principale
int Width=2972
int Height=1353
boolean TitleBar=true
string Title="Gestione Categorie Rifiuti"
dw_cat_rifiuti_lista dw_cat_rifiuti_lista
dw_cat_rifiuti_dett dw_cat_rifiuti_dett
end type
global w_cat_rifiuti w_cat_rifiuti

on w_cat_rifiuti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_cat_rifiuti_lista=create dw_cat_rifiuti_lista
this.dw_cat_rifiuti_dett=create dw_cat_rifiuti_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_cat_rifiuti_lista
this.Control[iCurrent+2]=dw_cat_rifiuti_dett
end on

on w_cat_rifiuti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_cat_rifiuti_lista)
destroy(this.dw_cat_rifiuti_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_cat_rifiuti_lista.set_dw_key("cod_azienda")
dw_cat_rifiuti_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_cat_rifiuti_dett.set_dw_options(sqlca, &
                                  dw_cat_rifiuti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_cat_rifiuti_lista
end event

type dw_cat_rifiuti_lista from uo_cs_xx_dw within w_cat_rifiuti
int X=23
int Y=21
int Width=2881
int Height=581
string DataObject="d_cat_rifiuti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_new;call super::pcd_new;dw_cat_rifiuti_dett.setitem(dw_cat_rifiuti_lista.getrow(), "flag_blocco", 'N')

dw_cat_rifiuti_dett.SetFocus( )
end event

type dw_cat_rifiuti_dett from uo_cs_xx_dw within w_cat_rifiuti
int X=23
int Y=621
int Width=2881
int Height=601
int TabOrder=2
string DataObject="d_cat_rifiuti_dett"
BorderStyle BorderStyle=StyleRaised!
end type

