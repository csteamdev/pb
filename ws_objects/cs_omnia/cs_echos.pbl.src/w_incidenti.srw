﻿$PBExportHeader$w_incidenti.srw
forward
global type w_incidenti from w_cs_xx_principale
end type
type dw_incidenti_lista from uo_cs_xx_dw within w_incidenti
end type
type dw_incidenti_dett from uo_cs_xx_dw within w_incidenti
end type
end forward

global type w_incidenti from w_cs_xx_principale
int Width=2807
int Height=1645
boolean TitleBar=true
string Title="Gestione Incidenti"
dw_incidenti_lista dw_incidenti_lista
dw_incidenti_dett dw_incidenti_dett
end type
global w_incidenti w_incidenti

on w_incidenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_incidenti_lista=create dw_incidenti_lista
this.dw_incidenti_dett=create dw_incidenti_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_incidenti_lista
this.Control[iCurrent+2]=dw_incidenti_dett
end on

on w_incidenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_incidenti_lista)
destroy(this.dw_incidenti_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_incidenti_lista.set_dw_key("cod_azienda")
dw_incidenti_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_incidenti_dett.set_dw_options(sqlca, &
                                  dw_incidenti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_incidenti_lista
end event

type dw_incidenti_lista from uo_cs_xx_dw within w_incidenti
int X=23
int Y=21
int Width=2721
int TabOrder=1
string DataObject="d_incidenti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_new;call super::pcd_new;dw_incidenti_dett.setitem(dw_incidenti_lista.getrow(), "flag_blocco", 'N')
dw_incidenti_dett.setitem(dw_incidenti_lista.getrow(), "flag_tipo_incidente", 'I')

dw_incidenti_dett.SetFocus( )
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

type dw_incidenti_dett from uo_cs_xx_dw within w_incidenti
int X=23
int Y=661
int Width=2721
int Height=857
int TabOrder=2
string DataObject="d_incidenti_dett"
BorderStyle BorderStyle=StyleRaised!
end type

