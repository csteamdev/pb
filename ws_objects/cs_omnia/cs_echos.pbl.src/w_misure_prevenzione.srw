﻿$PBExportHeader$w_misure_prevenzione.srw
$PBExportComments$Finestra Gestione Misure Prevenzione
forward
global type w_misure_prevenzione from w_cs_xx_principale
end type
type dw_misure_prevenzione_lista from uo_cs_xx_dw within w_misure_prevenzione
end type
type dw_misure_prevenzione_dett from uo_cs_xx_dw within w_misure_prevenzione
end type
end forward

global type w_misure_prevenzione from w_cs_xx_principale
integer width = 2734
integer height = 1640
string title = "Gestione Misure di Prevenzione"
dw_misure_prevenzione_lista dw_misure_prevenzione_lista
dw_misure_prevenzione_dett dw_misure_prevenzione_dett
end type
global w_misure_prevenzione w_misure_prevenzione

event pc_setwindow;call super::pc_setwindow;dw_misure_prevenzione_lista.set_dw_key("cod_azienda")
dw_misure_prevenzione_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_misure_prevenzione_dett.set_dw_options(sqlca, &
                                  dw_misure_prevenzione_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_misure_prevenzione_lista
end event

on w_misure_prevenzione.create
int iCurrent
call super::create
this.dw_misure_prevenzione_lista=create dw_misure_prevenzione_lista
this.dw_misure_prevenzione_dett=create dw_misure_prevenzione_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_misure_prevenzione_lista
this.Control[iCurrent+2]=this.dw_misure_prevenzione_dett
end on

on w_misure_prevenzione.destroy
call super::destroy
destroy(this.dw_misure_prevenzione_lista)
destroy(this.dw_misure_prevenzione_dett)
end on

type dw_misure_prevenzione_lista from uo_cs_xx_dw within w_misure_prevenzione
integer x = 23
integer y = 20
integer width = 2651
string dataobject = "d_misure_prevenzione_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;call super::pcd_new;dw_misure_prevenzione_dett.setitem(dw_misure_prevenzione_lista.getrow(), "flag_blocco", 'N')

// stefanop 23/03/2010: progetto 130
dw_misure_prevenzione_dett.setitem(dw_misure_prevenzione_lista.getrow(), "flag_fermo_macchina", 'N')

dw_misure_prevenzione_dett.SetFocus( )
end event

type dw_misure_prevenzione_dett from uo_cs_xx_dw within w_misure_prevenzione
integer x = 23
integer y = 660
integer width = 2651
integer height = 856
integer taborder = 2
string dataobject = "d_misure_prevenzione_dett"
borderstyle borderstyle = styleraised!
end type

