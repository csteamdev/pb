﻿$PBExportHeader$w_performance.srw
$PBExportComments$Finestra Testata Revisione Sistema Qualità
forward
global type w_performance from window
end type
type cb_report from commandbutton within w_performance
end type
type cb_grafico from commandbutton within w_performance
end type
type dw_grafico from datawindow within w_performance
end type
type dw_report from datawindow within w_performance
end type
end forward

global type w_performance from window
integer x = 1074
integer y = 484
integer width = 3803
integer height = 2008
boolean titlebar = true
string title = "Performance"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cb_report cb_report
cb_grafico cb_grafico
dw_grafico dw_grafico
dw_report dw_report
end type
global w_performance w_performance

on w_performance.create
this.cb_report=create cb_report
this.cb_grafico=create cb_grafico
this.dw_grafico=create dw_grafico
this.dw_report=create dw_report
this.Control[]={this.cb_report,&
this.cb_grafico,&
this.dw_grafico,&
this.dw_report}
end on

on w_performance.destroy
destroy(this.cb_report)
destroy(this.cb_grafico)
destroy(this.dw_grafico)
destroy(this.dw_report)
end on

event open;string ls_cod_resp_divisione, ls_cod_area_aziendale, ls_note, ls_flag_sistema, ls_flag_sistema_esteso
double ld_calc_effettivo, ld_calc_target
long ll_i

dw_grafico.SetTransObject(SQLCA)

select cod_resp_divisione, 
		 cod_area_aziendale, 
		 note,
		 flag_sistema
  into :ls_cod_resp_divisione,
  		 :ls_cod_area_aziendale,
		 :ls_note,
		 :ls_flag_sistema
  from tes_rev_sistema
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_obiettivo = :s_cs_xx.parametri.parametro_s_1
	and cod_indicatore = :s_cs_xx.parametri.parametro_s_2;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella TES_REV_SISTEMA")
	return
end if

if sqlca.sqlcode = 0 then
//	dw_report.setitem(1, "note", ls_note)
	
	declare cu_dettaglio cursor for
	 select calc_effettivo,
	 		  calc_target
	   from det_rev_sistema
	  where cod_azienda = :s_cs_xx.cod_azienda
	    and cod_obiettivo = :s_cs_xx.parametri.parametro_s_1
		 and cod_indicatore = :s_cs_xx.parametri.parametro_s_2;
	
	open cu_dettaglio;
	ll_i = 1
	
	do while 1 = 1
		fetch cu_dettaglio into :ld_calc_effettivo, :ld_calc_target;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella DET_REV_SISTEMA")
			return
		end if
		
		if sqlca.sqlcode = 100 then exit
//			messagebox("Omnia", "Non sono atati inseriti dati in corrispondenza dell'obiettivo " + s_cs_xx.parametri.parametro_s_1 + " e dell'indicatore " + s_cs_xx.parametri.parametro_s_2)
//		end if
	
		if sqlca.sqlcode = 0 then
			dw_report.insertrow(0)
			
			dw_report.setitem(1, "cod_obiettivo", s_cs_xx.parametri.parametro_s_1)
			dw_report.setitem(1, "cod_indicatore", s_cs_xx.parametri.parametro_s_2)
			dw_report.setitem(1, "cod_resp_divisione", ls_cod_resp_divisione)
			dw_report.setitem(1, "cod_area_aziendale", ls_cod_area_aziendale)
			dw_report.setitem(ll_i, "note", ls_note)			
			if ls_flag_sistema = 'A' then ls_flag_sistema_esteso = 'Ambientale'
			if ls_flag_sistema = 'Q' then ls_flag_sistema_esteso = 'Qualita'
			if ls_flag_sistema = 'E' then ls_flag_sistema_esteso = 'Entrambi'
			dw_report.setitem(1, "sistema", ls_flag_sistema_esteso)			
			
			dw_report.setitem(ll_i, "progressivo", ll_i)
			dw_report.setitem(ll_i, "calc_effettivo", ld_calc_effettivo)
			dw_report.setitem(ll_i, "calc_target", ld_calc_target)
			ll_i ++
		end if	
	loop
	close cu_dettaglio;
end if	

dw_grafico.hide()
dw_report.show()
end event

type cb_report from commandbutton within w_performance
integer x = 3383
integer y = 20
integer width = 366
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_resp_divisione, ls_cod_area_aziendale, ls_note, ls_flag_sistema, ls_flag_sistema_esteso
double ld_calc_effettivo, ld_calc_target
long ll_i

dw_report.Reset ( ) 

select cod_resp_divisione, 
		 cod_area_aziendale, 
		 note,
		 flag_sistema
  into :ls_cod_resp_divisione,
  		 :ls_cod_area_aziendale,
		 :ls_note,
		 :ls_flag_sistema
  from tes_rev_sistema
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_obiettivo = :s_cs_xx.parametri.parametro_s_1
	and cod_indicatore = :s_cs_xx.parametri.parametro_s_2;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella TES_REV_SISTEMA")
	return
end if

if sqlca.sqlcode = 0 then
//	dw_report.setitem(1, "note", ls_note)
	
	declare cu_dettaglio cursor for
	 select calc_effettivo,
	 		  calc_target
	   from det_rev_sistema
	  where cod_azienda = :s_cs_xx.cod_azienda
	    and cod_obiettivo = :s_cs_xx.parametri.parametro_s_1
		 and cod_indicatore = :s_cs_xx.parametri.parametro_s_2;
	
	open cu_dettaglio;
	ll_i = 1
	
	do while 1 = 1
		fetch cu_dettaglio into :ld_calc_effettivo, :ld_calc_target;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella DET_REV_SISTEMA")
			return
		end if
		
		if sqlca.sqlcode = 100 then exit
//			messagebox("Omnia", "Non sono atati inseriti dati in corrispondenza dell'obiettivo " + s_cs_xx.parametri.parametro_s_1 + " e dell'indicatore " + s_cs_xx.parametri.parametro_s_2)
//		end if
	
		if sqlca.sqlcode = 0 then
			dw_report.insertrow(0)
			
			dw_report.setitem(1, "cod_obiettivo", s_cs_xx.parametri.parametro_s_1)
			dw_report.setitem(1, "cod_indicatore", s_cs_xx.parametri.parametro_s_2)
			dw_report.setitem(1, "cod_resp_divisione", ls_cod_resp_divisione)
			dw_report.setitem(1, "cod_area_aziendale", ls_cod_area_aziendale)
			dw_report.setitem(ll_i, "note", ls_note)			
			if ls_flag_sistema = 'A' then ls_flag_sistema_esteso = 'Ambientale'
			if ls_flag_sistema = 'Q' then ls_flag_sistema_esteso = 'Qualita'
			if ls_flag_sistema = 'E' then ls_flag_sistema_esteso = 'Entrambi'
			dw_report.setitem(1, "sistema", ls_flag_sistema_esteso)			
			
			dw_report.setitem(ll_i, "progressivo", ll_i)
			dw_report.setitem(ll_i, "calc_effettivo", ld_calc_effettivo)
			dw_report.setitem(ll_i, "calc_target", ld_calc_target)
			ll_i ++
		end if	
	loop
	close cu_dettaglio;
end if	

dw_grafico.hide()
dw_report.show()
end event

type cb_grafico from commandbutton within w_performance
integer x = 3383
integer y = 120
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Grafico"
end type

event clicked;long ll_progressivo
double ld_calc_effettivo, ld_calc_target
string ls_descrizione

delete from tab_grafi;

commit;

declare cu_performance cursor for
 select prog_riga_det_rev, calc_effettivo, 'Effettivo' as descrizione
   from det_rev_sistema
  where cod_azienda = :s_cs_xx.cod_azienda
    and cod_obiettivo = :s_cs_xx.parametri.parametro_s_1
    and cod_indicatore = :s_cs_xx.parametri.parametro_s_2
  union all 
 select prog_riga_det_rev, calc_target, 'Taget' as descrizione
   from det_rev_sistema
  where cod_azienda = :s_cs_xx.cod_azienda
    and cod_obiettivo = :s_cs_xx.parametri.parametro_s_1
    and cod_indicatore = :s_cs_xx.parametri.parametro_s_2;
	 
open cu_performance;

do while 1 = 1
	fetch cu_performance into :ll_progressivo, :ld_calc_effettivo, :ls_descrizione;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati in UNION " + sqlca.sqlerrtext )
		return
	end if
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		insert into tab_grafi (cod_valore, des_valore, valore)
			  values (:ll_progressivo, :ls_descrizione, :ld_calc_effettivo) ;
	end if
loop
close cu_performance;
		
commit;

dw_grafico.retrieve()

dw_report.hide()
dw_grafico.show()


end event

type dw_grafico from datawindow within w_performance
boolean visible = false
integer x = 23
integer y = 20
integer width = 3337
integer height = 1860
integer taborder = 30
string dataobject = "d_graf_performance"
boolean livescroll = true
end type

type dw_report from datawindow within w_performance
integer x = 23
integer y = 20
integer width = 3337
integer height = 1860
integer taborder = 20
string dataobject = "d_report_performance"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

