﻿$PBExportHeader$w_registro_incidenti.srw
$PBExportComments$Finestra Registro degli Infortuni
forward
global type w_registro_incidenti from w_cs_xx_principale
end type
type dw_reg_incidenti_lista from uo_cs_xx_dw within w_registro_incidenti
end type
type dw_reg_incidenti_dett from uo_cs_xx_dw within w_registro_incidenti
end type
end forward

global type w_registro_incidenti from w_cs_xx_principale
int Width=2926
int Height=1857
boolean TitleBar=true
string Title="Gestione Registro Incidenti"
dw_reg_incidenti_lista dw_reg_incidenti_lista
dw_reg_incidenti_dett dw_reg_incidenti_dett
end type
global w_registro_incidenti w_registro_incidenti

on w_registro_incidenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_reg_incidenti_lista=create dw_reg_incidenti_lista
this.dw_reg_incidenti_dett=create dw_reg_incidenti_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_reg_incidenti_lista
this.Control[iCurrent+2]=dw_reg_incidenti_dett
end on

on w_registro_incidenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_reg_incidenti_lista)
destroy(this.dw_reg_incidenti_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_reg_incidenti_lista.set_dw_key("cod_azienda")
dw_reg_incidenti_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_reg_incidenti_dett.set_dw_options(sqlca, &
                                  dw_reg_incidenti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_reg_incidenti_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_reg_incidenti_dett,"cod_procedura",sqlca,&
                 "tes_procedure_lavoro","cod_procedura","des_procedura",&
                 "tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_reg_incidenti_dett,"cod_incidente",sqlca,&
                 "tab_incidenti","cod_incidente","des_incidente",&
                 "tab_incidenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_reg_incidenti_dett,"cod_prodotto",sqlca,&
                 "prod_pericolosi","cod_prodotto","nome_commerciale",&
                 "prod_pericolosi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

type dw_reg_incidenti_lista from uo_cs_xx_dw within w_registro_incidenti
int X=23
int Y=21
int Width=2835
string DataObject="d_reg_incidenti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_new;call super::pcd_new;long ll_num_registrazione, ll_anno_registrazione

ll_anno_registrazione = year(today())

dw_reg_incidenti_dett.setitem(dw_reg_incidenti_lista.getrow(), "anno_registrazione", ll_anno_registrazione)

select max(num_registrazione) + 1
  into :ll_num_registrazione
  from registro_incidenti
 where cod_azienda = :s_cs_xx.cod_azienda 
   and anno_registrazione = :ll_anno_registrazione;
	
if isnull(ll_num_registrazione) then ll_num_registrazione = 1

dw_reg_incidenti_dett.setitem(dw_reg_incidenti_lista.getrow(), "num_registrazione", ll_num_registrazione)

dw_reg_incidenti_dett.SetFocus( )
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then

	string ls_cod_misura_mag, ls_cod_prodotto, ls_cod_prodotto_comodo
	double ld_saldo_quan_inizio_anno, ld_prog_quan_entrata, ld_prog_quan_uscita, ld_giacenza
			 
	select cod_prodotto
	  into :ls_cod_prodotto_comodo
	  from prod_pericolosi
	 where cod_azienda = :s_cs_xx.cod_azienda; 
	
	if not isnull(ls_cod_prodotto_comodo) then
		if dw_reg_incidenti_lista.RowCount( ) > 0 then
			ls_cod_prodotto = dw_reg_incidenti_lista.getitemstring(dw_reg_incidenti_lista.getrow(), "cod_prodotto")
			if isnull(ls_cod_prodotto) then
				ls_cod_prodotto = ls_cod_prodotto_comodo
			end if
		end if
		
		select saldo_quan_inizio_anno,
				 prog_quan_entrata,
				 prog_quan_uscita,
				 cod_misura_mag
		  into :ld_saldo_quan_inizio_anno,
				 :ld_prog_quan_entrata,
				 :ld_prog_quan_uscita,
				 :ls_cod_misura_mag
		  from anag_prodotti
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_prodotto = :ls_cod_prodotto;
			
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Safe", "Errore in lettura dati dalla tabella ANAG_PRODOTTI")
			g_mb.messagebox("Safe", sqlca.sqlerrtext)
			return
		end if
		
		if sqlca.sqlcode = 0 then 
			ld_giacenza = ld_saldo_quan_inizio_anno + ld_prog_quan_entrata - ld_prog_quan_uscita
			dw_reg_incidenti_dett.Object.st_um_1.Text = ls_cod_misura_mag
			dw_reg_incidenti_dett.Object.st_um_2.Text = ls_cod_misura_mag
			dw_reg_incidenti_dett.Object.st_giacenza.Text = string(ld_giacenza)
		end if	
		
	end if

end if	
end event

type dw_reg_incidenti_dett from uo_cs_xx_dw within w_registro_incidenti
int X=23
int Y=661
int Width=2835
int Height=1077
int TabOrder=2
string DataObject="d_reg_incidenti_dett"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	long ll_edizione
	string ls_distanza_zona_impatto, ls_estenzione_zona_impatto, ls_est_lineare_zona_imp

   choose case i_colname
		case "cod_procedura"
			f_PO_LoadDDLB_DW(dw_reg_incidenti_dett,"num_edizione",sqlca,&
								  "tes_procedure_lavoro","num_edizione","num_edizione",&
								  "tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_procedura = '" + i_coltext + "' ")			
			
			dw_reg_incidenti_dett.accepttext()
			ll_edizione = dw_reg_incidenti_dett.getitemnumber(dw_reg_incidenti_lista.getrow(), "num_edizione")
			
			f_PO_LoadDDLB_DW(dw_reg_incidenti_dett,"num_revisione",sqlca,&
								  "tes_procedure_lavoro","num_revisione","num_revisione",&
								  "tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_procedura = '" + i_coltext + "' and num_edizione = " + string(ll_edizione))
		case "cod_prodotto"
			select distanza_zona_impatto,
					 estensione_zona_impatto,
					 est_lineare_zona_imp
			  into :ls_distanza_zona_impatto,
				 	 :ls_estenzione_zona_impatto,
					 :ls_est_lineare_zona_imp
			  from prod_pericolosi
			 where cod_azienda = :s_cs_xx.cod_azienda 
			   and cod_prodotto = :i_coltext;	
			  
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Safe", "Errore in lettura dati dalla tabella PRODOTTI PERICOLOSI")
				pcca.error = c_fatal
				return
			end if 
			
			dw_reg_incidenti_dett.setitem(dw_reg_incidenti_lista.getrow(), "distanza_zona_impatto", ls_distanza_zona_impatto)
			dw_reg_incidenti_dett.setitem(dw_reg_incidenti_lista.getrow(), "estensione_zona_impatto", ls_estenzione_zona_impatto)
			dw_reg_incidenti_dett.setitem(dw_reg_incidenti_lista.getrow(), "est_lineare_zona_imp", ls_est_lineare_zona_imp)			
			
   end choose
end if

end event

