﻿$PBExportHeader$w_report_proc_amb.srw
$PBExportComments$Report Procedura Ambiente
forward
global type w_report_proc_amb from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_proc_amb
end type
type cb_report from commandbutton within w_report_proc_amb
end type
type cb_selezione from commandbutton within w_report_proc_amb
end type
type dw_selezione from uo_cs_xx_dw within w_report_proc_amb
end type
type dw_report from uo_cs_xx_dw within w_report_proc_amb
end type
end forward

global type w_report_proc_amb from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Procedure Ambiente"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_proc_amb w_report_proc_amb

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 1980
this.height = 620

end event

on w_report_proc_amb.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_proc_amb.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_attrezzatura",sqlca,&
					  "anag_attrezzature","cod_attrezzatura","descrizione",&
					  "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_ambiente",sqlca,&
                 "tab_ambienti_lavoro","cod_ambiente","des_ambiente",&
                 "(tab_ambienti_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_ambienti_lavoro.flag_blocco <> 'S') or (tab_ambienti_lavoro.flag_blocco = 'S' and tab_ambienti_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_selezione,"cod_postazione",sqlca,&
                 "tab_postazioni_lavoro","cod_postazione","des_postazione",&
                 "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_selezione,"cod_procedura",sqlca,&
					  "tes_procedure_lavoro","cod_procedura","des_procedura",&
					  "tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_annulla from commandbutton within w_report_proc_amb
integer x = 1166
integer y = 420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_proc_amb
integer x = 1554
integer y = 420
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_attrezzatura, ls_ambiente, ls_postazione, ls_cod_procedura

dw_selezione.accepttext()

ls_cod_procedura = dw_selezione.getitemstring(1,"cod_procedura")
ls_attrezzatura = dw_selezione.getitemstring(1,"cod_attrezzatura")
ls_ambiente  = dw_selezione.getitemstring(1,"cod_ambiente")
ls_postazione  = dw_selezione.getitemstring(1,"cod_postazione")

if isnull(ls_cod_procedura) then ls_cod_procedura = "%"
if isnull(ls_attrezzatura) then ls_attrezzatura = "%"
if isnull(ls_ambiente) then ls_ambiente = "%"
if isnull(ls_postazione) then ls_postazione = "%"

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()
dw_report.retrieve(s_cs_xx.cod_azienda, ls_attrezzatura, ls_ambiente, ls_postazione, ls_cod_procedura)
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_proc_amb
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
parent.x = 741
parent.y = 885
parent.width = 1980
parent.height = 620
end event

type dw_selezione from uo_cs_xx_dw within w_report_proc_amb
integer x = 23
integer y = 20
integer width = 1897
integer height = 380
integer taborder = 20
string dataobject = "d_selezione_report_proc_amb"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	if i_colname = "cod_ambiente" and not isnull(i_coltext) then
		f_PO_LoadDDDW_DW(dw_selezione,"cod_postazione",sqlca,&
							  "tab_postazioni_lavoro","cod_postazione","des_postazione",&
							  "(tab_postazioni_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
							  "((tab_postazioni_lavoro.flag_blocco <> 'S') or (tab_postazioni_lavoro.flag_blocco = 'S' and tab_postazioni_lavoro.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
	end if
end if
end event

type dw_report from uo_cs_xx_dw within w_report_proc_amb
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_proc_amb"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

