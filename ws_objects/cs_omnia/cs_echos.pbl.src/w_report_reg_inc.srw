﻿$PBExportHeader$w_report_reg_inc.srw
forward
global type w_report_reg_inc from w_cs_xx_principale
end type
type dw_report_registro_incidenti from uo_cs_xx_dw within w_report_reg_inc
end type
type dw_sel_report_registro_incidenti from uo_cs_xx_dw within w_report_reg_inc
end type
type cb_annulla from commandbutton within w_report_reg_inc
end type
type cb_report from commandbutton within w_report_reg_inc
end type
type cb_selezione from commandbutton within w_report_reg_inc
end type
end forward

global type w_report_reg_inc from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Registro Incidenti"
dw_report_registro_incidenti dw_report_registro_incidenti
dw_sel_report_registro_incidenti dw_sel_report_registro_incidenti
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
end type
global w_report_reg_inc w_report_reg_inc

on w_report_reg_inc.create
int iCurrent
call super::create
this.dw_report_registro_incidenti=create dw_report_registro_incidenti
this.dw_sel_report_registro_incidenti=create dw_sel_report_registro_incidenti
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_registro_incidenti
this.Control[iCurrent+2]=this.dw_sel_report_registro_incidenti
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.cb_selezione
end on

on w_report_reg_inc.destroy
call super::destroy
destroy(this.dw_report_registro_incidenti)
destroy(this.dw_sel_report_registro_incidenti)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_registro_incidenti.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_registro_incidenti.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_sel_report_registro_incidenti.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report_registro_incidenti

this.x = 741
this.y = 885
this.width = 2650
this.height = 560

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_sel_report_registro_incidenti,"cod_procedura",sqlca,&
					  "tes_procedure_lavoro","cod_procedura","des_procedura",&
					  "tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_sel_report_registro_incidenti,"cod_prodotto",sqlca,&
                 "prod_pericolosi","cod_prodotto","nome_commerciale",&
					  "prod_pericolosi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_report_registro_incidenti from uo_cs_xx_dw within w_report_reg_inc
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 30
string dataobject = "d_report_registro_incidenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_sel_report_registro_incidenti from uo_cs_xx_dw within w_report_reg_inc
integer x = 23
integer y = 20
integer width = 2560
integer height = 316
integer taborder = 40
string dataobject = "d_sel_report_registro_incidenti"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;dw_sel_report_registro_incidenti.setitem(1, "anno_registrazione", year(today()))
end event

type cb_annulla from commandbutton within w_report_reg_inc
integer x = 1829
integer y = 360
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_reg_inc
integer x = 2217
integer y = 360
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_procedura, ls_cod_prodotto
long ll_anno_registrazione

dw_sel_report_registro_incidenti.accepttext()

ll_anno_registrazione = dw_sel_report_registro_incidenti.getitemnumber(1, "anno_registrazione")
ls_cod_procedura = dw_sel_report_registro_incidenti.getitemstring(1,"cod_procedura")
ls_cod_prodotto  = dw_sel_report_registro_incidenti.getitemstring(1,"cod_prodotto")

if isnull(ls_cod_procedura) then ls_cod_procedura = "%"
if isnull(ls_cod_prodotto) then ls_cod_prodotto = "%"

dw_sel_report_registro_incidenti.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report_registro_incidenti.show()
dw_report_registro_incidenti.retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ls_cod_procedura, ls_cod_prodotto)
dw_sel_report_registro_incidenti.hide()

dw_sel_report_registro_incidenti.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_reg_inc
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 4
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_sel_report_registro_incidenti.show()

dw_report_registro_incidenti.hide()

dw_sel_report_registro_incidenti.change_dw_current()
parent.x = 741
parent.y = 885
parent.width = 2650
parent.height = 560
end event

