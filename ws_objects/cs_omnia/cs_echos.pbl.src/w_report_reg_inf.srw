﻿$PBExportHeader$w_report_reg_inf.srw
forward
global type w_report_reg_inf from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_reg_inf
end type
type cb_report from commandbutton within w_report_reg_inf
end type
type cb_selezione from commandbutton within w_report_reg_inf
end type
type dw_report_registro_infortuni from uo_cs_xx_dw within w_report_reg_inf
end type
type dw_sel_report_registro_infortuni from uo_cs_xx_dw within w_report_reg_inf
end type
end forward

global type w_report_reg_inf from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Registro Infortuni"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report_registro_infortuni dw_report_registro_infortuni
dw_sel_report_registro_infortuni dw_sel_report_registro_infortuni
end type
global w_report_reg_inf w_report_reg_inf

on w_report_reg_inf.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report_registro_infortuni=create dw_report_registro_infortuni
this.dw_sel_report_registro_infortuni=create dw_sel_report_registro_infortuni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_report_registro_infortuni
this.Control[iCurrent+5]=this.dw_sel_report_registro_infortuni
end on

on w_report_reg_inf.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report_registro_infortuni)
destroy(this.dw_sel_report_registro_infortuni)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_registro_infortuni.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_registro_infortuni.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_sel_report_registro_infortuni.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report_registro_infortuni

this.x = 741
this.y = 885
this.width = 2650
this.height = 560

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_sel_report_registro_infortuni,"cod_procedura",sqlca,&
					  "tes_procedure_lavoro","cod_procedura","des_procedura",&
					  "tes_procedure_lavoro.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_sel_report_registro_infortuni,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
					  "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_annulla from commandbutton within w_report_reg_inf
integer x = 1829
integer y = 360
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_reg_inf
integer x = 2217
integer y = 360
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_procedura, ls_cod_operaio
long ll_anno_registrazione

dw_sel_report_registro_infortuni.accepttext()

ll_anno_registrazione = dw_sel_report_registro_infortuni.getitemnumber(1, "anno_registrazione")
ls_cod_procedura = dw_sel_report_registro_infortuni.getitemstring(1,"cod_procedura")
ls_cod_operaio  = dw_sel_report_registro_infortuni.getitemstring(1,"cod_operaio")

if isnull(ls_cod_procedura) then ls_cod_procedura = "%"
if isnull(ls_cod_operaio) then ls_cod_operaio = "%"

dw_sel_report_registro_infortuni.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report_registro_infortuni.show()
dw_report_registro_infortuni.retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ls_cod_procedura, ls_cod_operaio)
dw_sel_report_registro_infortuni.hide()

dw_sel_report_registro_infortuni.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_reg_inf
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_sel_report_registro_infortuni.show()

dw_report_registro_infortuni.hide()

dw_sel_report_registro_infortuni.change_dw_current()
parent.x = 741
parent.y = 885
parent.width = 2650
parent.height = 560
end event

type dw_report_registro_infortuni from uo_cs_xx_dw within w_report_reg_inf
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 30
string dataobject = "d_report_registro_infortuni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_sel_report_registro_infortuni from uo_cs_xx_dw within w_report_reg_inf
integer x = 23
integer y = 20
integer width = 2560
integer height = 320
integer taborder = 2
string dataobject = "d_sel_report_registro_infortuni"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;long		li_anno_esc

li_anno_esc = f_anno_esercizio()

if isnull(li_anno_esc) or li_anno_esc=0 then li_anno_esc = year(today())


dw_sel_report_registro_infortuni.setitem(1, "anno_registrazione", li_anno_esc)
end event

