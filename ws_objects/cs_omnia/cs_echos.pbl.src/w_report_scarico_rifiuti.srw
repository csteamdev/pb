﻿$PBExportHeader$w_report_scarico_rifiuti.srw
forward
global type w_report_scarico_rifiuti from w_cs_xx_principale
end type
type dw_sel_scarico_rifiuti from u_dw_search within w_report_scarico_rifiuti
end type
type cb_cerca from commandbutton within w_report_scarico_rifiuti
end type
type cb_annulla from commandbutton within w_report_scarico_rifiuti
end type
type dw_report_scarico_rifiuti from uo_cs_xx_dw within w_report_scarico_rifiuti
end type
type dw_folder from u_folder within w_report_scarico_rifiuti
end type
end forward

global type w_report_scarico_rifiuti from w_cs_xx_principale
integer width = 3447
integer height = 1828
string title = "Report Scarico Rifiuti"
boolean maxbox = false
boolean resizable = false
dw_sel_scarico_rifiuti dw_sel_scarico_rifiuti
cb_cerca cb_cerca
cb_annulla cb_annulla
dw_report_scarico_rifiuti dw_report_scarico_rifiuti
dw_folder dw_folder
end type
global w_report_scarico_rifiuti w_report_scarico_rifiuti

forward prototypes
public function integer wf_imposta_anno (integer al_anno, integer al_visible, string as_text)
public function integer wf_report ()
end prototypes

public function integer wf_imposta_anno (integer al_anno, integer al_visible, string as_text);string ls_modify, ls_return


ls_modify = "anno_" + string(al_anno) + "_t.text='" + as_text + "'"
		
ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

ls_modify = "anno_" + string(al_anno) + "_t.visible=" + string(al_visible)

ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

ls_modify = "anno_" + string(al_anno) + ".visible=" + string(al_visible)

ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

ls_modify = "l_anno_" + string(al_anno) + "_h_t.visible=" + string(al_visible)

ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

ls_modify = "l_anno_" + string(al_anno) + "_h_r.visible=" + string(al_visible)

ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

ls_modify = "l_anno_" + string(al_anno) + "_d_t.visible=" + string(al_visible)

ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

ls_modify = "l_anno_" + string(al_anno) + "_d_r.visible=" + string(al_visible)

ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

ls_modify = "l_anno_" + string(al_anno) + "_s_t.visible=" + string(al_visible)

ls_return = dw_report_scarico_rifiuti.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("OMNIA",ls_return,stopsign!)
	return -1
end if

return 0
end function

public function integer wf_report ();datastore lds_date, lds_gruppi, lds_righe, lds_rifiuti

dec{4}	 ld_quan_rifiuto

long		 ll_i, ll_j, ll_count, ll_anno, ll_riga, ll_num_righe, ll_anni[]

datetime  ldt_da, ldt_a, ldt_min, ldt_max, ldt_inizio, ldt_fine

string	 ls_cod_cat_rifiuto, ls_cod_rifiuto, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_attivita, &
			 ls_des_cat_rifiuto, ls_des_rifiuto, ls_des_area_aziendale, ls_des_reparto, ls_des_attivita, &
			 ls_where, ls_sql, ls_errore


dw_report_scarico_rifiuti.reset()

for ll_i = 1 to 5
	if wf_imposta_anno(ll_i,0,"") = -1 then
		return -1
	end if
next

dw_sel_scarico_rifiuti.accepttext()

ldt_da = dw_sel_scarico_rifiuti.getitemdatetime(1,"data_reg_da")

if isnull(ldt_da) then
	ldt_da = datetime(date("01/01/1900"),00:00:00)
end if

ldt_a = dw_sel_scarico_rifiuti.getitemdatetime(1,"data_reg_a")

if isnull(ldt_a) then
	ldt_a = datetime(date("31/12/2999"),00:00:00)
end if

if ldt_da > ldt_a then
	g_mb.messagebox("OMNIA","Impostare correttamente le date di selezione",exclamation!)
	return -1
end if

ls_cod_cat_rifiuto = dw_sel_scarico_rifiuti.getitemstring(1,"cod_cat_rifiuti")

ls_cod_rifiuto = dw_sel_scarico_rifiuti.getitemstring(1,"cod_rifiuto")

ls_cod_area_aziendale = dw_sel_scarico_rifiuti.getitemstring(1,"cod_area_aziendale")

ls_cod_reparto = dw_sel_scarico_rifiuti.getitemstring(1,"cod_reparto")

ls_cod_attivita = dw_sel_scarico_rifiuti.getitemstring(1,"cod_attivita")

ls_where = &
"from	  rilevazione_rifiuti, " + &
"		  tab_rifiuti " + &
"where  tab_rifiuti.cod_azienda = rilevazione_rifiuti.cod_azienda and " + &
"		  tab_rifiuti.cod_rifiuto = rilevazione_rifiuti.cod_rifiuto and " + &
"		  rilevazione_rifiuti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  data_registrazione >= '" + string(date(ldt_da),s_cs_xx.db_funzioni.formato_data) + "' and " + &
"		  data_registrazione <= '" + string(date(ldt_a),s_cs_xx.db_funzioni.formato_data) + "' "

if not isnull(ls_cod_cat_rifiuto) then
	ls_where += " and cod_cat_rifiuto = '" + ls_cod_cat_rifiuto + "' "
end if

if not isnull(ls_cod_rifiuto) then
	ls_where += " and rilevazione_rifiuti.cod_rifiuto = '" + ls_cod_rifiuto + "' "
end if

if not isnull(ls_cod_area_aziendale) then
	ls_where += " and cod_area_aziendale = '" + ls_cod_area_aziendale + "' "
end if

if not isnull(ls_cod_reparto) then
	ls_where += " and cod_reparto = '" + ls_cod_reparto + "' "
end if

if not isnull(ls_cod_attivita) then
	ls_where += " and cod_attivita = '" + ls_cod_attivita + "' "
end if

setnull(ls_errore)

ls_sql = &
"select distinct cod_cat_rifiuto, " + &
"		  rilevazione_rifiuti.cod_rifiuto, " + &
"		  cod_area_aziendale, " + &
"		  cod_reparto, " + &
"		  cod_attivita " + &
ls_where + &
"group by cod_cat_rifiuto, " + &
"			 rilevazione_rifiuti.cod_rifiuto, " + &
"			 cod_area_aziendale, " + &
"			 cod_reparto, " + &
"			 cod_attivita " + &
"order by cod_cat_rifiuto ASC, " + &
"			 rilevazione_rifiuti.cod_rifiuto ASC, " + &
"			 cod_area_aziendale ASC, " + &
"			 cod_reparto ASC, " + &
"			 cod_attivita ASC "

ls_sql = sqlca.syntaxfromsql(ls_sql,"grid",ls_errore)

if ls_sql = "" and not isnull(ls_errore) then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	return -1
end if

lds_gruppi = create datastore

if lds_gruppi.create(ls_sql,ls_errore) = -1 then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	return -1
end if

if lds_gruppi.settransobject(sqlca) = -1 then
	g_mb.messagebox("OMNIA","Errore in impostazione transazione",stopsign!)
	return -1
end if

if lds_gruppi.retrieve() = -1 then
	g_mb.messagebox("OMNIA","Errore in retrieve dei dati",stopsign!)
	return -1
end if

if lds_gruppi.rowcount() < 1 then
	destroy lds_gruppi
	return -1
end if

setnull(ls_errore)

ls_sql = &
"select min(data_registrazione)," + &
"		  max(data_registrazione) " + &
ls_where

ls_sql = sqlca.syntaxfromsql(ls_sql,"grid",ls_errore)

if ls_sql = "" and not isnull(ls_errore) then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	return -1
end if

lds_date = create datastore

if lds_date.create(ls_sql,ls_errore) = -1 then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	return -1
end if

if lds_date.settransobject(sqlca) = -1 then
	g_mb.messagebox("OMNIA","Errore in impostazione transazione",stopsign!)
	return -1
end if

if lds_date.retrieve() = -1 then
	g_mb.messagebox("OMNIA","Errore in retrieve dei dati",stopsign!)
	return -1
end if

ldt_min = lds_date.getitemdatetime(1,1)

ldt_max = lds_date.getitemdatetime(1,2)

if isnull(ldt_min) or isnull(ldt_max) then
	g_mb.messagebox("OMNIA","Errore in lettura date minima e massima",stopsign!)
	return -1
end if

ll_count = 0
	
for ll_anno = year(date(ldt_min)) to year(date(ldt_max))
	
	ldt_inizio = datetime(date("01/01/" + string(ll_anno)),00:00:00)
		
	ldt_fine = datetime(date("31/12/" + string(ll_anno)),00:00:00)
		
	setnull(ls_errore)

	ls_sql = &
	"select count(*) " + &
	ls_where
	
	ls_sql = sqlca.syntaxfromsql(ls_sql,"grid",ls_errore)
	
	if ls_sql = "" and not isnull(ls_errore) then
		g_mb.messagebox("OMNIA",ls_errore,stopsign!)
		return -1
	end if
	
	lds_righe = create datastore
	
	if lds_righe.create(ls_sql,ls_errore) = -1 then
		g_mb.messagebox("OMNIA",ls_errore,stopsign!)
		return -1
	end if
	
	if lds_righe.settransobject(sqlca) = -1 then
		g_mb.messagebox("OMNIA","Errore in impostazione transazione",stopsign!)
		return -1
	end if
	
	if lds_righe.retrieve() = -1 then
		g_mb.messagebox("OMNIA","Errore in retrieve dei dati",stopsign!)
		return -1
	end if
	
	ll_num_righe = lds_righe.getitemnumber(1,1)
	
	if ll_num_righe < 1 then
		continue
	end if
	
	ll_count ++
	
	if ll_count > 5 then
		if g_mb.messagebox("OMNIA","I dati inclusi nella selezione interessano un numero di anni " + &
					  "superiore al limite previsto di 5.~nIl report visualizzerà solamente " + &
					  "i primi 5 anni.~nContinuare?",question!,yesno!,2) = 2 then
			return -1
		else
			ll_count --
			exit
		end if
	end if
	
	ll_anni[ll_count] = ll_anno
	
	if wf_imposta_anno(ll_count,1,string(ll_anno)) = -1 then
		return -1
	end if
	
next

for ll_i = 1 to lds_gruppi.rowcount()
	
	ls_cod_cat_rifiuto = lds_gruppi.getitemstring(ll_i,1)
	
	select des_cat_rifiuto
	into	 :ls_des_cat_rifiuto
	from	 tab_cat_rifiuti
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cat_rifiuto = :ls_cod_cat_rifiuto;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura descrizione categoria rifiuti: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		setnull(ls_des_cat_rifiuto)
	end if

	ls_cod_rifiuto = lds_gruppi.getitemstring(ll_i,2)
	
	select des_rifiuto
	into	 :ls_des_rifiuto
	from	 tab_rifiuti
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_rifiuto = :ls_cod_rifiuto;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in lettura descrizione rifiuto: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		setnull(ls_des_rifiuto)
	end if
	
	ls_cod_area_aziendale = lds_gruppi.getitemstring(ll_i,3)
	
	select des_area
	into	 :ls_des_area_aziendale
	from	 tab_aree_aziendali
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_area_aziendale = :ls_cod_area_aziendale;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in lettura descrizione area_aziendale: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		setnull(ls_des_area_aziendale)
	end if
	
	ls_cod_reparto = lds_gruppi.getitemstring(ll_i,4)
	
	select des_reparto
	into	 :ls_des_reparto
	from	 anag_reparti
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_reparto = :ls_cod_reparto;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in lettura descrizione reparto: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		setnull(ls_des_reparto)
	end if
	
	ls_cod_attivita = lds_gruppi.getitemstring(ll_i,5)
	
	select des_attivita
	into	 :ls_des_attivita
	from	 attivita
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attivita = :ls_cod_attivita;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in lettura descrizione rifiuto: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		setnull(ls_des_attivita)
	end if
	
	ll_riga = dw_report_scarico_rifiuti.insertrow(0)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"cod_categoria",ls_cod_cat_rifiuto)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"des_categoria",ls_des_cat_rifiuto)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"cod_rifiuto",ls_cod_rifiuto)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"des_rifiuto",ls_des_rifiuto)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"cod_area_aziendale",ls_cod_area_aziendale)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"des_area_aziendale",ls_des_area_aziendale)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"cod_reparto",ls_cod_reparto)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"des_reparto",ls_des_reparto)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"cod_attivita",ls_cod_attivita)
	
	dw_report_scarico_rifiuti.setitem(ll_riga,"des_attivita",ls_des_attivita)
	
	for ll_j = 1 to ll_count
		
		ldt_inizio = datetime(date("01/01/" + string(ll_anni[ll_j])),00:00:00)
			
		ldt_fine = datetime(date("31/12/" + string(ll_anni[ll_j])),00:00:00)
		
		ls_where = &
		"from	  rilevazione_rifiuti, " + &
		"		  tab_rifiuti " + &
		"where  tab_rifiuti.cod_azienda = rilevazione_rifiuti.cod_azienda and " + &
		"		  tab_rifiuti.cod_rifiuto = rilevazione_rifiuti.cod_rifiuto and " + &
		"		  rilevazione_rifiuti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"		  data_registrazione >= '" + string(date(ldt_inizio),s_cs_xx.db_funzioni.formato_data) + "' and " + &
		"		  data_registrazione <= '" + string(date(ldt_fine),s_cs_xx.db_funzioni.formato_data) + "' "
		
		if not isnull(ls_cod_cat_rifiuto) then
			ls_where += " and cod_cat_rifiuto = '" + ls_cod_cat_rifiuto + "' "
		end if
		
		if not isnull(ls_cod_rifiuto) then
			ls_where += " and rilevazione_rifiuti.cod_rifiuto = '" + ls_cod_rifiuto + "' "
		end if
		
		if not isnull(ls_cod_area_aziendale) then
			ls_where += " and cod_area_aziendale = '" + ls_cod_area_aziendale + "' "
		end if
		
		if not isnull(ls_cod_reparto) then
			ls_where += " and cod_reparto = '" + ls_cod_reparto + "' "
		end if
		
		if not isnull(ls_cod_attivita) then
			ls_where += " and cod_attivita = '" + ls_cod_attivita + "' "
		end if
		
		setnull(ls_errore)
		
		ls_sql = &
		"select sum(quan_rifiuto) as quan_rifiuto " + &
		ls_where
		
		ls_sql = sqlca.syntaxfromsql(ls_sql,"grid",ls_errore)
		
		if ls_sql = "" and not isnull(ls_errore) then
			g_mb.messagebox("OMNIA",ls_errore,stopsign!)
			return -1
		end if
		
		lds_rifiuti = create datastore
		
		if lds_rifiuti.create(ls_sql,ls_errore) = -1 then
			g_mb.messagebox("OMNIA",ls_errore,stopsign!)
			return -1
		end if
		
		if lds_rifiuti.settransobject(sqlca) = -1 then
			g_mb.messagebox("OMNIA","Errore in impostazione transazione",stopsign!)
			return -1
		end if
		
		if lds_rifiuti.retrieve() = -1 then
			g_mb.messagebox("OMNIA","Errore in retrieve dei dati",stopsign!)
			return -1
		end if
		
		ld_quan_rifiuto = lds_rifiuti.getitemnumber(1,"quan_rifiuto")
		
		if isnull(ld_quan_rifiuto) then
			ld_quan_rifiuto = 0
		end if
		
		dw_report_scarico_rifiuti.setitem(ll_riga,"anno_" + string(ll_j),ld_quan_rifiuto)
		
		destroy lds_rifiuti
		
	next
	
next

destroy lds_gruppi

dw_report_scarico_rifiuti.resetupdate()

return 0
end function

on w_report_scarico_rifiuti.create
int iCurrent
call super::create
this.dw_sel_scarico_rifiuti=create dw_sel_scarico_rifiuti
this.cb_cerca=create cb_cerca
this.cb_annulla=create cb_annulla
this.dw_report_scarico_rifiuti=create dw_report_scarico_rifiuti
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_scarico_rifiuti
this.Control[iCurrent+2]=this.cb_cerca
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.dw_report_scarico_rifiuti
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_scarico_rifiuti.destroy
call super::destroy
destroy(this.dw_sel_scarico_rifiuti)
destroy(this.cb_cerca)
destroy(this.cb_annulla)
destroy(this.dw_report_scarico_rifiuti)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_scarico_rifiuti,"cod_cat_rifiuti",sqlca,"tab_cat_rifiuti","cod_cat_rifiuto", &
					  "des_cat_rifiuto", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_scarico_rifiuti,"cod_rifiuto",sqlca,"tab_rifiuti","cod_rifiuto", &
					  "des_rifiuto", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_scarico_rifiuti,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
					  "des_area", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_scarico_rifiuti,"cod_reparto",sqlca,"anag_reparti","cod_reparto", &
					  "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_scarico_rifiuti,"cod_attivita",sqlca,"attivita","cod_attivita", &
					  "des_attivita", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_report_scarico_rifiuti.ib_dw_report = true

set_w_options(c_closenosave + c_noenablepopup)

iuo_dw_main = dw_report_scarico_rifiuti

dw_report_scarico_rifiuti.change_dw_current()

dw_report_scarico_rifiuti.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_noretrieveonopen + c_nonew + c_nodelete + c_nomodify, &
                                 c_nohighlightselected + c_nocursorrowfocusrect)

dw_report_scarico_rifiuti.object.datawindow.print.preview = 'Yes'

dw_folder.fu_folderoptions(dw_folder.c_defaultheight,dw_folder.c_foldertabtop)

l_objects[1] = dw_report_scarico_rifiuti
dw_folder.fu_assigntab(2,"Report",l_Objects[])

l_objects[1] = dw_sel_scarico_rifiuti
l_objects[2] = cb_annulla
l_objects[3] = cb_cerca
dw_folder.fu_assigntab(1,"Selezione",l_Objects[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selectTab(1)
end event

type dw_sel_scarico_rifiuti from u_dw_search within w_report_scarico_rifiuti
integer x = 46
integer y = 140
integer width = 2629
integer height = 520
integer taborder = 40
string dataobject = "d_sel_scarico_rifiuti"
end type

type cb_cerca from commandbutton within w_report_scarico_rifiuti
integer x = 2309
integer y = 680
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_report_scarico_rifiuti.change_dw_current()

setpointer(hourglass!)

dw_report_scarico_rifiuti.setredraw(false)

parent.triggerevent("pc_retrieve")

dw_report_scarico_rifiuti.setredraw(true)

setpointer(arrow!)

dw_folder.fu_selecttab(2)
end event

type cb_annulla from commandbutton within w_report_scarico_rifiuti
integer x = 1920
integer y = 680
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string	ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_sel_scarico_rifiuti.setitem(1,"data_reg_da",ldt_null)

dw_sel_scarico_rifiuti.setitem(1,"data_reg_a",ldt_null)

dw_sel_scarico_rifiuti.setitem(1,"cod_cat_rifiuti",ls_null)

dw_sel_scarico_rifiuti.setitem(1,"cod_rifiuto",ls_null)

dw_sel_scarico_rifiuti.setitem(1,"cod_area_aziendale",ls_null)

dw_sel_scarico_rifiuti.setitem(1,"cod_reparto",ls_null)

dw_sel_scarico_rifiuti.setitem(1,"cod_attivita",ls_null)
end event

type dw_report_scarico_rifiuti from uo_cs_xx_dw within w_report_scarico_rifiuti
integer x = 46
integer y = 140
integer width = 3337
integer height = 1560
integer taborder = 30
string dataobject = "d_report_scarico_rifiuti"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

type dw_folder from u_folder within w_report_scarico_rifiuti
integer x = 23
integer y = 20
integer width = 3383
integer height = 1700
integer taborder = 10
end type

