﻿$PBExportHeader$w_rifiuti.srw
$PBExportComments$Finestra Gestione Rifiuti
forward
global type w_rifiuti from w_cs_xx_principale
end type
type dw_rifiuti_lista from uo_cs_xx_dw within w_rifiuti
end type
type dw_rifiuti_dett from uo_cs_xx_dw within w_rifiuti
end type
end forward

global type w_rifiuti from w_cs_xx_principale
int Width=2926
int Height=1809
boolean TitleBar=true
string Title="Gestione Rifiuti"
dw_rifiuti_lista dw_rifiuti_lista
dw_rifiuti_dett dw_rifiuti_dett
end type
global w_rifiuti w_rifiuti

event pc_setwindow;call super::pc_setwindow;dw_rifiuti_lista.set_dw_key("cod_azienda")
dw_rifiuti_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_rifiuti_dett.set_dw_options(sqlca, &
                                  dw_rifiuti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_rifiuti_lista
end event

on w_rifiuti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_rifiuti_lista=create dw_rifiuti_lista
this.dw_rifiuti_dett=create dw_rifiuti_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_rifiuti_lista
this.Control[iCurrent+2]=dw_rifiuti_dett
end on

on w_rifiuti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_rifiuti_lista)
destroy(this.dw_rifiuti_dett)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_rifiuti_dett, &
                 "cod_tipo_impatto", &
                 sqlca, &
					  "tab_tipi_impatti", &
                 "cod_tipo_impatto", &
					  "des_tipo_impatto", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_rifiuti_dett, &
                 "cod_cat_rifiuto", &
                 sqlca, &
					  "tab_cat_rifiuti", &
                 "cod_cat_rifiuto", &
					  "des_cat_rifiuto", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

type dw_rifiuti_lista from uo_cs_xx_dw within w_rifiuti
int X=23
int Y=21
int Width=2835
int Height=621
string DataObject="d_rifiuti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;call super::pcd_new;dw_rifiuti_dett.setitem(dw_rifiuti_lista.getrow(), "flag_blocco", 'N')

dw_rifiuti_dett.SetFocus( )
end event

type dw_rifiuti_dett from uo_cs_xx_dw within w_rifiuti
int X=23
int Y=661
int Width=2835
int Height=1021
int TabOrder=2
string DataObject="d_rifiuti_dett"
BorderStyle BorderStyle=StyleRaised!
end type

