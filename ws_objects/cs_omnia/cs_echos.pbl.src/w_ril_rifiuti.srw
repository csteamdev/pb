﻿$PBExportHeader$w_ril_rifiuti.srw
forward
global type w_ril_rifiuti from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_ril_rifiuti
end type
type cb_annulla from commandbutton within w_ril_rifiuti
end type
type cb_cerca from commandbutton within w_ril_rifiuti
end type
type dw_ril_rifiuti_sel from u_dw_search within w_ril_rifiuti
end type
type dw_ril_rifiuti_det_1 from uo_cs_xx_dw within w_ril_rifiuti
end type
type dw_ril_rifiuti_lista from uo_cs_xx_dw within w_ril_rifiuti
end type
type dw_folder from u_folder within w_ril_rifiuti
end type
end forward

global type w_ril_rifiuti from w_cs_xx_principale
integer width = 2277
integer height = 1524
string title = "Rilevazione Rifiuti"
boolean maxbox = false
boolean resizable = false
cb_documento cb_documento
cb_annulla cb_annulla
cb_cerca cb_cerca
dw_ril_rifiuti_sel dw_ril_rifiuti_sel
dw_ril_rifiuti_det_1 dw_ril_rifiuti_det_1
dw_ril_rifiuti_lista dw_ril_rifiuti_lista
dw_folder dw_folder
end type
global w_ril_rifiuti w_ril_rifiuti

on w_ril_rifiuti.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.cb_annulla=create cb_annulla
this.cb_cerca=create cb_cerca
this.dw_ril_rifiuti_sel=create dw_ril_rifiuti_sel
this.dw_ril_rifiuti_det_1=create dw_ril_rifiuti_det_1
this.dw_ril_rifiuti_lista=create dw_ril_rifiuti_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_cerca
this.Control[iCurrent+4]=this.dw_ril_rifiuti_sel
this.Control[iCurrent+5]=this.dw_ril_rifiuti_det_1
this.Control[iCurrent+6]=this.dw_ril_rifiuti_lista
this.Control[iCurrent+7]=this.dw_folder
end on

on w_ril_rifiuti.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.cb_annulla)
destroy(this.cb_cerca)
destroy(this.dw_ril_rifiuti_sel)
destroy(this.dw_ril_rifiuti_det_1)
destroy(this.dw_ril_rifiuti_lista)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]


iuo_dw_main = dw_ril_rifiuti_lista

dw_ril_rifiuti_lista.change_dw_current()

dw_ril_rifiuti_lista.set_dw_key("cod_azienda")

dw_ril_rifiuti_lista.set_dw_key("anno_registrazione")

dw_ril_rifiuti_lista.set_dw_key("num_registrazione")

dw_ril_rifiuti_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_noretrieveonopen, &
                                 c_default)

dw_ril_rifiuti_det_1.set_dw_options(sqlca, &
                                dw_ril_rifiuti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
										  
dw_folder.fu_folderoptions(dw_folder.c_defaultheight,dw_folder.c_foldertableft)

l_objects[1] = dw_ril_rifiuti_lista
l_objects[2] = cb_documento
dw_folder.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_ril_rifiuti_sel
l_objects[2] = cb_annulla
l_objects[3] = cb_cerca
dw_folder.fu_assigntab(2, "R.", l_Objects[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selectTab(2)

cb_annulla.postevent("clicked")
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ril_rifiuti_det_1,"cod_rifiuto",sqlca,"tab_rifiuti","cod_rifiuto", &
					  "des_rifiuto", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ril_rifiuti_det_1,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
					  "des_area", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ril_rifiuti_det_1,"cod_reparto",sqlca,"anag_reparti","cod_reparto", &
					  "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ril_rifiuti_det_1,"cod_attivita",sqlca,"attivita","cod_attivita", &
					  "des_attivita", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_ril_rifiuti_sel,"cod_rifiuto",sqlca,"tab_rifiuti","cod_rifiuto", &
					  "des_rifiuto", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ril_rifiuti_sel,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
					  "des_area", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ril_rifiuti_sel,"cod_reparto",sqlca,"anag_reparti","cod_reparto", &
					  "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ril_rifiuti_sel,"cod_attivita",sqlca,"attivita","cod_attivita", &
					  "des_attivita", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_documento from commandbutton within w_ril_rifiuti
integer x = 1829
integer y = 60
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;long ll_row


ll_row = dw_ril_rifiuti_lista.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare una registrazione prima di continuare!",exclamation!)
	return -1
end if

if isnull(dw_ril_rifiuti_lista.getitemnumber(ll_row,"anno_registrazione")) or &
	isnull(dw_ril_rifiuti_lista.getitemnumber(ll_row,"num_registrazione")) then
	g_mb.messagebox("OMNIA","Selezionare una registrazione prima di continuare!",exclamation!)
	return -1
end if

window_open_parm(w_ril_rifiuti_doc, -1, dw_ril_rifiuti_lista)
end event

type cb_annulla from commandbutton within w_ril_rifiuti
integer x = 1440
integer y = 760
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string 	ls_null

datetime	ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_ril_rifiuti_sel.setitem(1,"anno_reg",f_anno_esercizio())

dw_ril_rifiuti_sel.setitem(1,"num_reg_da",1)

dw_ril_rifiuti_sel.setitem(1,"num_reg_a",999999)

dw_ril_rifiuti_sel.setitem(1,"data_reg_da",ldt_null)

dw_ril_rifiuti_sel.setitem(1,"data_reg_a",ldt_null)

dw_ril_rifiuti_sel.setitem(1,"cod_rifiuto",ls_null)

dw_ril_rifiuti_sel.setitem(1,"cod_area_aziendale",ls_null)

dw_ril_rifiuti_sel.setitem(1,"cod_reparto",ls_null)

dw_ril_rifiuti_sel.setitem(1,"cod_attivita",ls_null)
end event

type cb_cerca from commandbutton within w_ril_rifiuti
integer x = 1829
integer y = 760
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ril_rifiuti_lista.change_dw_current()

parent.postevent("pc_retrieve")
end event

type dw_ril_rifiuti_sel from u_dw_search within w_ril_rifiuti
integer x = 137
integer y = 40
integer width = 2080
integer height = 600
integer taborder = 20
string dataobject = "d_ril_rifiuti_sel"
boolean border = false
end type

type dw_ril_rifiuti_det_1 from uo_cs_xx_dw within w_ril_rifiuti
integer x = 23
integer y = 900
integer width = 2217
integer height = 520
integer taborder = 20
string dataobject = "d_ril_rifiuti_det_1"
end type

type dw_ril_rifiuti_lista from uo_cs_xx_dw within w_ril_rifiuti
integer x = 137
integer y = 40
integer width = 1669
integer height = 800
integer taborder = 10
string dataobject = "d_ril_rifiuti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long 		ll_anno, ll_num_da, ll_num_a

datetime	ldt_data_da, ldt_data_a

string	ls_cod_rifiuto, ls_cod_area, ls_cod_reparto, ls_cod_attivita


dw_ril_rifiuti_sel.accepttext()

ll_anno = dw_ril_rifiuti_sel.getitemnumber(1,"anno_reg")

if ll_anno = 0 then
	setnull(ll_anno)
end if

ll_num_da = dw_ril_rifiuti_sel.getitemnumber(1,"num_reg_da")

if isnull(ll_num_da) or ll_num_da = 0 then
	ll_num_da = 1
end if

ll_num_a = dw_ril_rifiuti_sel.getitemnumber(1,"num_reg_a")

if isnull(ll_num_a) or ll_num_a = 0 then
	ll_num_a = 999999
end if

ldt_data_da = dw_ril_rifiuti_sel.getitemdatetime(1,"data_reg_da")

if isnull(ldt_data_da) then
	ldt_data_da = datetime(date("01/01/1900"),00:00:00)
end if

ldt_data_a = dw_ril_rifiuti_sel.getitemdatetime(1,"data_reg_a")

if isnull(ldt_data_a) then
	ldt_data_a = datetime(date("31/12/2999"),00:00:00)
end if

ls_cod_rifiuto = dw_ril_rifiuti_sel.getitemstring(1,"cod_rifiuto")

ls_cod_area = dw_ril_rifiuti_sel.getitemstring(1,"cod_area_aziendale")

ls_cod_reparto = dw_ril_rifiuti_sel.getitemstring(1,"cod_reparto")

ls_cod_attivita = dw_ril_rifiuti_sel.getitemstring(1,"cod_attivita")

retrieve(s_cs_xx.cod_azienda,ll_anno,ll_num_da,ll_num_a,ldt_data_da,ldt_data_a,&
			ls_cod_rifiuto,ls_cod_area,ls_cod_reparto,ls_cod_attivita)

dw_folder.fu_selecttab(1)
end event

event pcd_new;call super::pcd_new;cb_documento.enabled = false

setitem(getrow(),"data_registrazione",datetime(today(),00:00:00))
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno, ll_num


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_i,"anno_registrazione")) then
		setitem(ll_i,"anno_registrazione",f_anno_esercizio())
	end if
	
	if isnull(getitemnumber(ll_i,"num_registrazione")) then
		
		ll_anno = getitemnumber(ll_i,"anno_registrazione")
		
		select max(num_registrazione)
		into	 :ll_num
		from	 rilevazione_rifiuti
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo di registrazione: " + sqlca.sqlerrtext,stopsign!)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_num) then
			ll_num = 0
		end if
		
		ll_num ++
		
		setitem(ll_i,"num_registrazione",ll_num)
		
	end if
	
next
end event

event pcd_modify;call super::pcd_modify;cb_documento.enabled = false
end event

event pcd_view;call super::pcd_view;cb_documento.enabled = true
end event

event updateend;call super::updateend;cb_documento.enabled = true
end event

type dw_folder from u_folder within w_ril_rifiuti
integer x = 23
integer y = 20
integer width = 2217
integer height = 860
integer taborder = 20
end type

