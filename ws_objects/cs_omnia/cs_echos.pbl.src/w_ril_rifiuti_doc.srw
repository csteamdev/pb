﻿$PBExportHeader$w_ril_rifiuti_doc.srw
$PBExportComments$Finestra di dettaglio documenti per attrezzature
forward
global type w_ril_rifiuti_doc from w_cs_xx_principale
end type
type dw_ril_rifiuti_doc from uo_cs_xx_dw within w_ril_rifiuti_doc
end type
type cb_chiavi from commandbutton within w_ril_rifiuti_doc
end type
type cb_note_esterne from commandbutton within w_ril_rifiuti_doc
end type
end forward

global type w_ril_rifiuti_doc from w_cs_xx_principale
integer width = 2875
integer height = 1024
string title = "Documenti"
boolean maxbox = false
dw_ril_rifiuti_doc dw_ril_rifiuti_doc
cb_chiavi cb_chiavi
cb_note_esterne cb_note_esterne
end type
global w_ril_rifiuti_doc w_ril_rifiuti_doc

type variables
blob 	ib_blob

long	il_anno, il_num
end variables

on w_ril_rifiuti_doc.create
int iCurrent
call super::create
this.dw_ril_rifiuti_doc=create dw_ril_rifiuti_doc
this.cb_chiavi=create cb_chiavi
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ril_rifiuti_doc
this.Control[iCurrent+2]=this.cb_chiavi
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_ril_rifiuti_doc.destroy
call super::destroy
destroy(this.dw_ril_rifiuti_doc)
destroy(this.cb_chiavi)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;dw_ril_rifiuti_doc.set_dw_key("cod_azienda")
dw_ril_rifiuti_doc.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_ril_rifiuti_doc

end event

type dw_ril_rifiuti_doc from uo_cs_xx_dw within w_ril_rifiuti_doc
integer x = 23
integer y = 20
integer width = 2789
integer height = 780
integer taborder = 20
string dataobject = "d_ril_rifiuti_doc"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

il_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_registrazione")

il_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_registrazione")

parent.title = "Documenti - rilevazione rifiuti " + string(il_anno) + "/" + string(il_num)

l_Error = Retrieve(s_cs_xx.cod_azienda,il_anno,il_num)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_progressivo, ll_protocollo


for ll_i = 1 to this.rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_i,"anno_registrazione")) then
		setitem(ll_i,"anno_registrazione",il_anno)
	end if
	
	if isnull(getitemnumber(ll_i,"num_registrazione")) then
		setitem(ll_i,"num_registrazione",il_num)
	end if
   
	if isnull(getitemnumber(ll_i,"prog_documento")) then
		
		select max(prog_documento)
		into 	 :ll_progressivo
		from 	 rilevazione_rifiuti_doc
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno and
				 num_registrazione = :il_num;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo documenti: " + sqlca.sqlerrtext,stopsign!)
			dw_ril_rifiuti_doc.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return
		end if
		
		if isnull(ll_progressivo) then
			ll_progressivo = 0
		end if
		
		ll_progressivo ++
		
		setitem(ll_i,"prog_documento",ll_progressivo)
	
	end if
	
	if isnull(getitemnumber(ll_i,"num_protocollo")) then
		
		select max(num_protocollo)
		into 	 :ll_protocollo
		from 	 tab_protocolli
		where  cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura massimo numero protocollo: " + sqlca.sqlerrtext,stopsign!)
			dw_ril_rifiuti_doc.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return
		end if
		
		if isnull(ll_protocollo) then
			ll_protocollo = 0
		end if
		
		ll_protocollo ++
		
		insert
		into 	 tab_protocolli
				 (cod_azienda,
				 num_protocollo)
		values (:s_cs_xx.cod_azienda,
				 :ll_protocollo);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in aggiornamento tabella protocolli: " + sqlca.sqlerrtext,stopsign!)
			dw_ril_rifiuti_doc.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return
		end if
		
		setitem(ll_i,"num_protocollo",ll_protocollo)
		
	end if
	
next
end event

event pcd_new;call super::pcd_new;if i_extendmode then

	this.setitem(this.getrow(), "flag_blocco", "N")
	
	cb_chiavi.enabled = false
	cb_note_esterne.enabled = false		
	
end if	
end event

event updateend;call super::updateend;long ll_i
double ld_num_protocollo
string ls_cod_famiglia_chiavi, ls_cod_chiave

for ll_i = 1 to this.deletedcount()
	
	ld_num_protocollo = getitemnumber(ll_i, "num_protocollo", delete!, true)

	delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
	where cod_azienda = :s_cs_xx.cod_azienda
	  and num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return
	end if	

	delete from tab_protocolli  //cancellazione tabella protocollo
	where cod_azienda = :s_cs_xx.cod_azienda
	  and num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return 1
	end if
	
	cb_chiavi.enabled = true
	cb_note_esterne.enabled = true
	
next



end event

event pcd_view;call super::pcd_view;cb_chiavi.enabled = true
cb_note_esterne.enabled = true	
end event

event pcd_modify;call super::pcd_modify;cb_chiavi.enabled = false
cb_note_esterne.enabled = false	
end event

type cb_chiavi from commandbutton within w_ril_rifiuti_doc
integer x = 2057
integer y = 820
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiavi"
end type

event clicked;if dw_ril_rifiuti_doc.getrow() < 1 or &
	isnull(dw_ril_rifiuti_doc.getrow()) then
	g_mb.messagebox("OMNIA", "Inserire almeno un dettaglio")
	return
end if	

window_open_parm(w_tab_chiavi_protocollo, -1, dw_ril_rifiuti_doc)
end event

type cb_note_esterne from commandbutton within w_ril_rifiuti_doc
integer x = 2446
integer y = 820
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long ll_i, ll_progressivo
integer li_risposta
string ls_db

transaction sqlcb
blob lbl_null

setnull(lbl_null)
ll_i = dw_ril_rifiuti_doc.getrow()

ll_progressivo = dw_ril_rifiuti_doc.getitemnumber(ll_i, "prog_documento")

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       rilevazione_rifiuti_doc
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :il_anno and
				  num_registrazione = :il_num and
				  prog_documento = :ll_progressivo
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

	
	destroy sqlcb;
	
else
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       rilevazione_rifiuti_doc
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :il_anno and
				  num_registrazione = :il_num and
				  prog_documento = :ll_progressivo;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

ib_blob = s_cs_xx.parametri.parametro_bl_1

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob rilevazione_rifiuti_doc
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :il_anno and
					  num_registrazione = :il_num and
					  prog_documento = :ll_progressivo
		using      sqlcb;
				
		if sqlcb.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA",sqlcb.sqlerrtext,stopsign!)
		end if	

		destroy sqlcb;
		
	else
		
	   updateblob rilevazione_rifiuti_doc
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :il_anno and
					  num_registrazione = :il_num and
					  prog_documento = :ll_progressivo;
				
		if sqlca.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA",sqlca.sqlerrtext,stopsign!)
		end if	
		
	end if
				
   update     rilevazione_rifiuti_doc
   set        path_documento = :s_cs_xx.parametri.parametro_s_1
   where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :il_anno and
				  num_registrazione = :il_num and
				  prog_documento = :ll_progressivo;

   commit;
end if
end event

