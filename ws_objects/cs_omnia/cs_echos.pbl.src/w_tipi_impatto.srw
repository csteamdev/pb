﻿$PBExportHeader$w_tipi_impatto.srw
$PBExportComments$Finestra Gestione Tipi Impatto
forward
global type w_tipi_impatto from w_cs_xx_principale
end type
type dw_tipi_impatto_lista from uo_cs_xx_dw within w_tipi_impatto
end type
type dw_tipi_impatto_dett from uo_cs_xx_dw within w_tipi_impatto
end type
end forward

global type w_tipi_impatto from w_cs_xx_principale
int Width=3041
int Height=1645
boolean TitleBar=true
string Title="Gestione Tipi Impatto"
dw_tipi_impatto_lista dw_tipi_impatto_lista
dw_tipi_impatto_dett dw_tipi_impatto_dett
end type
global w_tipi_impatto w_tipi_impatto

on w_tipi_impatto.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_impatto_lista=create dw_tipi_impatto_lista
this.dw_tipi_impatto_dett=create dw_tipi_impatto_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_impatto_lista
this.Control[iCurrent+2]=dw_tipi_impatto_dett
end on

on w_tipi_impatto.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_impatto_lista)
destroy(this.dw_tipi_impatto_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_impatto_lista.set_dw_key("cod_azienda")
dw_tipi_impatto_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_tipi_impatto_dett.set_dw_options(sqlca, &
                                  dw_tipi_impatto_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_tipi_impatto_lista

end event

type dw_tipi_impatto_lista from uo_cs_xx_dw within w_tipi_impatto
int X=23
int Y=21
int Width=2949
string DataObject="d_tipi_impatto_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_new;call super::pcd_new;dw_tipi_impatto_dett.setitem(dw_tipi_impatto_lista.getrow(), "flag_blocco", 'N')

dw_tipi_impatto_dett.SetFocus( )
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

type dw_tipi_impatto_dett from uo_cs_xx_dw within w_tipi_impatto
int X=23
int Y=661
int Width=2949
int Height=857
int TabOrder=2
string DataObject="d_tipi_impatto_dett"
BorderStyle BorderStyle=StyleRaised!
end type

