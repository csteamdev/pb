﻿$PBExportHeader$w_det_analisi_fmea.srw
$PBExportComments$Tabella Aree Aziendali
forward
global type w_det_analisi_fmea from w_cs_xx_principale
end type
type cb_dettaglio from commandbutton within w_det_analisi_fmea
end type
type dw_det_analisi_fmea_lista from uo_cs_xx_dw within w_det_analisi_fmea
end type
type dw_det_analisi_fmea_det from uo_cs_xx_dw within w_det_analisi_fmea
end type
end forward

global type w_det_analisi_fmea from w_cs_xx_principale
integer width = 2830
integer height = 1884
string title = "Gestione Dettagli Analisi FMEA"
event ue_seleziona_riga ( )
cb_dettaglio cb_dettaglio
dw_det_analisi_fmea_lista dw_det_analisi_fmea_lista
dw_det_analisi_fmea_det dw_det_analisi_fmea_det
end type
global w_det_analisi_fmea w_det_analisi_fmea

type variables
long il_anno_registrazione, il_num_registrazione
end variables

event ue_seleziona_riga();return
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]

dw_det_analisi_fmea_lista.set_dw_key("cod_azienda")
dw_det_analisi_fmea_lista.set_dw_options(sqlca, &
                               i_openparm, &
                               c_default, &
                               c_default)
dw_det_analisi_fmea_det.set_dw_options(sqlca, &
                             dw_det_analisi_fmea_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
iuo_dw_main = dw_det_analisi_fmea_lista

end event

on w_det_analisi_fmea.create
int iCurrent
call super::create
this.cb_dettaglio=create cb_dettaglio
this.dw_det_analisi_fmea_lista=create dw_det_analisi_fmea_lista
this.dw_det_analisi_fmea_det=create dw_det_analisi_fmea_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettaglio
this.Control[iCurrent+2]=this.dw_det_analisi_fmea_lista
this.Control[iCurrent+3]=this.dw_det_analisi_fmea_det
end on

on w_det_analisi_fmea.destroy
call super::destroy
destroy(this.cb_dettaglio)
destroy(this.dw_det_analisi_fmea_lista)
destroy(this.dw_det_analisi_fmea_det)
end on

type cb_dettaglio from commandbutton within w_det_analisi_fmea
integer x = 2309
integer y = 1680
integer width = 457
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Modi di Guasto"
end type

event clicked;window_open_parm(w_det_analisi_fmea_guasti, -1, dw_det_analisi_fmea_lista)
end event

type dw_det_analisi_fmea_lista from uo_cs_xx_dw within w_det_analisi_fmea
integer x = 23
integer y = 20
integer width = 2743
integer height = 580
integer taborder = 10
string dataobject = "d_det_analisi_fmea_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_divisione, ls_old_select, new_select, where_clause, ls_count
long ll_i, ll_errore, ll_index

il_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

parent.title = "Gestione Dettagli Analisi FMEA: " + string(il_anno_registrazione) + "/" + string(il_num_registrazione)

ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long l_idx, ll_progressivo

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "anno_registrazione")) or getitemnumber(l_idx, "anno_registrazione") = 0 then
		setitem(l_idx, "anno_registrazione", il_anno_registrazione)
	end if		
	
   if isnull(getitemnumber(l_idx, "num_registrazione")) or getitemnumber(l_idx, "num_registrazione") = 0 then
		setitem(l_idx, "num_registrazione", il_num_registrazione)			
	end if
	
   if isnull(getitemnumber(l_idx, "progressivo")) or getitemnumber(l_idx, "progressivo") = 0 then
		
		setnull(ll_progressivo)
		
		select max(progressivo)
		into   :ll_progressivo
		from   det_analisi_fmea
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		
		if isnull(ll_progressivo) then ll_progressivo = 0
		
		ll_progressivo ++
		
		setitem(l_idx, "progressivo", ll_progressivo)
		
	end if	
		
next


end event

event pcd_new;call super::pcd_new;long l_idx, ll_progressivo

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "anno_registrazione")) or getitemnumber(l_idx, "anno_registrazione") = 0 then
		setitem(l_idx, "anno_registrazione", il_anno_registrazione)
	end if		
	
   if isnull(getitemnumber(l_idx, "num_registrazione")) or getitemnumber(l_idx, "num_registrazione") = 0 then
		setitem(l_idx, "num_registrazione", il_num_registrazione)			
	end if
	
   if isnull(getitemnumber(l_idx, "progressivo")) or getitemnumber(l_idx, "progressivo") = 0 then
		
		setnull(ll_progressivo)
		
		select max(progressivo)
		into   :ll_progressivo
		from   det_analisi_fmea
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		
		if isnull(ll_progressivo) then ll_progressivo = 0
		
		ll_progressivo ++
		
		setitem(l_idx, "progressivo", ll_progressivo)
		
	end if	
		
next
//
//cb_ricerca_prodotto.enabled = true
end event

event pcd_modify;call super::pcd_modify;//cb_ricerca_prodotto.enabled = true
end event

event pcd_save;call super::pcd_save;//cb_ricerca_prodotto.enabled = false
end event

event pcd_view;call super::pcd_view;//cb_ricerca_prodotto.enabled = false
end event

type dw_det_analisi_fmea_det from uo_cs_xx_dw within w_det_analisi_fmea
integer x = 23
integer y = 600
integer width = 2743
integer height = 1060
integer taborder = 20
string dataobject = "d_det_analisi_fmea_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_analisi_fmea_det,"cod_prodotto")
end choose
end event

