﻿$PBExportHeader$w_det_analisi_fmea_guasti.srw
$PBExportComments$Tabella Aree Aziendali
forward
global type w_det_analisi_fmea_guasti from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_det_analisi_fmea_guasti
end type
type dw_det_analisi_fmea_guasti_lista from uo_cs_xx_dw within w_det_analisi_fmea_guasti
end type
type dw_det_analisi_fmea_guasti_det from uo_cs_xx_dw within w_det_analisi_fmea_guasti
end type
end forward

global type w_det_analisi_fmea_guasti from w_cs_xx_principale
integer width = 2313
integer height = 2096
string title = "Gestione Dettagli Analisi FMEA"
event ue_seleziona_riga ( )
cb_1 cb_1
dw_det_analisi_fmea_guasti_lista dw_det_analisi_fmea_guasti_lista
dw_det_analisi_fmea_guasti_det dw_det_analisi_fmea_guasti_det
end type
global w_det_analisi_fmea_guasti w_det_analisi_fmea_guasti

type variables
long il_anno_registrazione, il_num_registrazione, il_progressivo
end variables

event ue_seleziona_riga();return
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]

dw_det_analisi_fmea_guasti_lista.set_dw_key("cod_azienda")
dw_det_analisi_fmea_guasti_lista.set_dw_options(sqlca, &
                               i_openparm, &
                               c_default, &
                               c_default)
dw_det_analisi_fmea_guasti_det.set_dw_options(sqlca, &
                             dw_det_analisi_fmea_guasti_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
iuo_dw_main = dw_det_analisi_fmea_guasti_lista

end event

on w_det_analisi_fmea_guasti.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_det_analisi_fmea_guasti_lista=create dw_det_analisi_fmea_guasti_lista
this.dw_det_analisi_fmea_guasti_det=create dw_det_analisi_fmea_guasti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_det_analisi_fmea_guasti_lista
this.Control[iCurrent+3]=this.dw_det_analisi_fmea_guasti_det
end on

on w_det_analisi_fmea_guasti.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_det_analisi_fmea_guasti_lista)
destroy(this.dw_det_analisi_fmea_guasti_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "cod_errore", &
						sqlca, &
                  "tab_difformita", &
						"cod_errore", &
						"des_difformita", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						
f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "cod_effetto", &
						sqlca, &
                  "tab_effetti", &
						"cod_effetto", &
						"des_effetto", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						
f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "cod_tipo_causa", &
						sqlca, &
                  "tab_tipi_cause", &
						"cod_tipo_causa", &
						"des_tipo_causa", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "'")		
						
f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "cod_causa", &
						sqlca, &
                  "cause", &
						"cod_causa", &
						"des_causa", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
						
f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "cod_test", &
						sqlca, &
                  "tab_test", &
						"cod_test", &
						"des_test", &
                  " ")			
						
f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "valore_o", &
						sqlca, &
                  "tab_valori_fmea", &
						"valore", &
						"des_valore", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_parametro = 'O' ")
						
f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "valore_s", &
						sqlca, &
                  "tab_valori_fmea", &
						"valore", &
						"des_valore", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_parametro = 'S' ")
						
f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "valore_r", &
						sqlca, &
                  "tab_valori_fmea", &
						"valore", &
						"des_valore", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_parametro = 'R' ")						
end event

type cb_1 from commandbutton within w_det_analisi_fmea_guasti
integer x = 1029
integer y = 1432
integer width = 137
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A.P."
end type

event clicked;long ll_num_righe, ll_t, ll_anno, ll_num
double ldd_reset[]

s_cs_xx.parametri.parametro_d_1_a = ldd_reset
s_cs_xx.parametri.parametro_d_2_a = ldd_reset

ll_anno = dw_det_analisi_fmea_guasti_lista.getitemnumber( dw_det_analisi_fmea_guasti_lista.getrow(), "anno_az_prev")
ll_num = dw_det_analisi_fmea_guasti_lista.getitemnumber( dw_det_analisi_fmea_guasti_lista.getrow(), "num_az_prev")

if not isnull(ll_anno) and not isnull(ll_num) and ll_anno > 0 and ll_num > 0 then
	s_cs_xx.parametri.parametro_d_1_a[1] = ll_anno
	s_cs_xx.parametri.parametro_d_2_a[1] = ll_num
else
	s_cs_xx.parametri.parametro_d_1_a[1] = 0
	s_cs_xx.parametri.parametro_d_2_a[1] = 0
end if

window_open(w_seleziona_azione_preventiva,0)

ll_num_righe = upperbound(s_cs_xx.parametri.parametro_d_1_a[])

if ll_num_righe = 1 then
	
	ll_anno = s_cs_xx.parametri.parametro_d_1_a[1]
	ll_num = s_cs_xx.parametri.parametro_d_2_a[1]
	
	if not isnull(ll_anno) and ll_anno > 0 and not isnull(ll_num) and ll_num > 0 then
		dw_det_analisi_fmea_guasti_lista.setitem( dw_det_analisi_fmea_guasti_lista.getrow(), "anno_az_prev", ll_anno)
		dw_det_analisi_fmea_guasti_lista.setitem( dw_det_analisi_fmea_guasti_lista.getrow(), "num_az_prev", ll_num)
		parent.triggerevent("pc_save")
	end if
	
end if

return 0	
end event

type dw_det_analisi_fmea_guasti_lista from uo_cs_xx_dw within w_det_analisi_fmea_guasti
integer x = 23
integer y = 20
integer width = 2217
integer height = 580
integer taborder = 10
string dataobject = "d_det_analisi_fmea_guasti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_divisione, ls_old_select, new_select, where_clause, ls_count
long ll_i, ll_errore, ll_index

il_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
il_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

parent.title = "Gestione Dettagli Analisi FMEA: " + string(il_anno_registrazione) + "/" + string(il_num_registrazione) + "/" + string(il_progressivo)

ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione, il_progressivo)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long l_idx, ll_progressivo

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "anno_registrazione")) or getitemnumber(l_idx, "anno_registrazione") = 0 then
		setitem(l_idx, "anno_registrazione", il_anno_registrazione)
	end if		
	
   if isnull(getitemnumber(l_idx, "num_registrazione")) or getitemnumber(l_idx, "num_registrazione") = 0 then
		setitem(l_idx, "num_registrazione", il_num_registrazione)			
	end if
	
   if isnull(getitemnumber(l_idx, "progressivo")) or getitemnumber(l_idx, "progressivo") = 0 then
		setitem(l_idx, "progressivo", il_progressivo)
	end if	
	
   if isnull(getitemnumber(l_idx, "prog_guasto")) or getitemnumber(l_idx, "prog_guasto") = 0 then	

		setnull(ll_progressivo)
		
		select max(prog_guasto)
		into   :ll_progressivo
		from   det_analisi_fmea_guasti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 progressivo = :il_progressivo;
		
		if isnull(ll_progressivo) then ll_progressivo = 0
		
		ll_progressivo ++
		
		setitem(l_idx, "prog_guasto", ll_progressivo)
		
	end if
		
next


end event

event pcd_new;call super::pcd_new;long l_idx, ll_progressivo

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "anno_registrazione")) or getitemnumber(l_idx, "anno_registrazione") = 0 then
		setitem(l_idx, "anno_registrazione", il_anno_registrazione)
	end if		
	
   if isnull(getitemnumber(l_idx, "num_registrazione")) or getitemnumber(l_idx, "num_registrazione") = 0 then
		setitem(l_idx, "num_registrazione", il_num_registrazione)			
	end if
	
   if isnull(getitemnumber(l_idx, "progressivo")) or getitemnumber(l_idx, "progressivo") = 0 then
		setitem(l_idx, "progressivo", il_progressivo)
	end if	
	
   if isnull(getitemnumber(l_idx, "prog_guasto")) or getitemnumber(l_idx, "prog_guasto") = 0 then	

		setnull(ll_progressivo)
		
		select max(prog_guasto)
		into   :ll_progressivo
		from   det_analisi_fmea_guasti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 progressivo = :il_progressivo;
		
		if isnull(ll_progressivo) then ll_progressivo = 0
		
		ll_progressivo ++
		
		setitem(l_idx, "prog_guasto", ll_progressivo)
		
	end if
		
next


end event

type dw_det_analisi_fmea_guasti_det from uo_cs_xx_dw within w_det_analisi_fmea_guasti
integer x = 23
integer y = 600
integer width = 2217
integer height = 1380
integer taborder = 20
string dataobject = "d_det_analisi_fmea_guasti_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;dec {4} ld_o, ld_s, ld_r, ld_ipr

choose case i_colname
	case "cod_tipo_causa"
	
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
									"cod_causa", &
									sqlca, &
									"cause", &
									"cod_causa", &
									"des_causa", &
									" cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "' ")					
		else
			f_PO_LoadDDDW_DW( dw_det_analisi_fmea_guasti_det, &
                  "cod_causa", &
						sqlca, &
                  "cause", &
						"cod_causa", &
						"des_causa", &
                  " cod_azienda = '" + s_cs_xx.cod_azienda + "'")						
		end if
		
		
	case "valore_o"
		
		ld_s = getitemnumber( row, "valore_s")
		ld_o = dec(i_coltext)
		ld_r = getitemnumber( row, "valore_r")
		
		if isnull(ld_s) then ld_s = 0
		if isnull(ld_r) then ld_r = 0
		if isnull(ld_o) then ld_o = 0		
		
		ld_ipr = ld_s * ld_r * ld_o		
		setitem( row, "valore_ipr", ld_ipr)
		
	case "valore_s"
		
		ld_s = dec(i_coltext)
		ld_o = getitemnumber( row, "valore_o")
		ld_r = getitemnumber( row, "valore_r")
		
		if isnull(ld_s) then ld_s = 0
		if isnull(ld_r) then ld_r = 0
		if isnull(ld_o) then ld_o = 0		
		
		ld_ipr = ld_s * ld_r * ld_o		
		setitem( row, "valore_ipr", ld_ipr)
		
	case "valore_r"
		
		ld_s = getitemnumber( row, "valore_s")
		ld_o = getitemnumber( row, "valore_o")
		ld_r = dec(i_coltext)
		
		if isnull(ld_s) then ld_s = 0
		if isnull(ld_r) then ld_r = 0
		if isnull(ld_o) then ld_o = 0
		
		ld_ipr = ld_s * ld_r * ld_o		
		setitem( row, "valore_ipr", ld_ipr)		
		
end choose
end event

