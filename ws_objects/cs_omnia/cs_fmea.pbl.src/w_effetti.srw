﻿$PBExportHeader$w_effetti.srw
$PBExportComments$Tabella Errori e Difformità
forward
global type w_effetti from w_cs_xx_principale
end type
type dw_effetti_lista from uo_cs_xx_dw within w_effetti
end type
type dw_effetti_det from uo_cs_xx_dw within w_effetti
end type
end forward

global type w_effetti from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2336
integer height = 1264
string title = "Tabella Effetti"
dw_effetti_lista dw_effetti_lista
dw_effetti_det dw_effetti_det
end type
global w_effetti w_effetti

on w_effetti.create
int iCurrent
call super::create
this.dw_effetti_lista=create dw_effetti_lista
this.dw_effetti_det=create dw_effetti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_effetti_lista
this.Control[iCurrent+2]=this.dw_effetti_det
end on

on w_effetti.destroy
call super::destroy
destroy(this.dw_effetti_lista)
destroy(this.dw_effetti_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_effetti_lista.set_dw_key("cod_azienda")

dw_effetti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_effetti_det.set_dw_options(sqlca,dw_effetti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_effetti_lista
end event

type dw_effetti_lista from uo_cs_xx_dw within w_effetti
integer x = 23
integer y = 20
integer width = 2240
integer height = 500
integer taborder = 10
string dataobject = "d_tab_effetti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_effetti_det from uo_cs_xx_dw within w_effetti
integer x = 23
integer y = 540
integer width = 2240
integer height = 580
integer taborder = 10
string dataobject = "d_tab_effetti_det"
borderstyle borderstyle = styleraised!
end type

