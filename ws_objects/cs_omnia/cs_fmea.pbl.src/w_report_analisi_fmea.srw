﻿$PBExportHeader$w_report_analisi_fmea.srw
forward
global type w_report_analisi_fmea from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_analisi_fmea
end type
type cb_report from commandbutton within w_report_analisi_fmea
end type
type st_1 from statictext within w_report_analisi_fmea
end type
type dw_sel_analisi_fmea from uo_cs_xx_dw within w_report_analisi_fmea
end type
type dw_report from uo_cs_xx_dw within w_report_analisi_fmea
end type
type dw_folder from u_folder within w_report_analisi_fmea
end type
type wstr_des_periodo from structure within w_report_analisi_fmea
end type
type wstr_fmea from structure within w_report_analisi_fmea
end type
end forward

type wstr_des_periodo from structure
	string		des_periodo
end type

type wstr_fmea from structure
	decimal{0}		anno
	decimal{0}		numero
	string		funzione
	string		guasto
	string		effetto
	string		causa
	decimal{0}		val_O
	decimal{0}		val_S
	decimal{0}		val_R
	decimal{0}		val_IPR
end type

global type w_report_analisi_fmea from w_cs_xx_principale
integer width = 4146
integer height = 1996
string title = "Report Analisi FMEA"
boolean resizable = false
cb_annulla cb_annulla
cb_report cb_report
st_1 st_1
dw_sel_analisi_fmea dw_sel_analisi_fmea
dw_report dw_report
dw_folder dw_folder
end type
global w_report_analisi_fmea w_report_analisi_fmea

type variables
private wstr_des_periodo istr_des_periodo[]
private boolean ib_guerrato
private string is_des_livello_N, is_des_livello_R, is_des_livello_E, is_des_livello_C, is_des_livello_T, is_des_livello_A, is_des_livello_D

end variables

forward prototypes
public function integer wf_report (ref long fl_prog_task)
end prototypes

public function integer wf_report (ref long fl_prog_task);string ls_selezione, ls_descrizione, ls_cod_prodotto, ls_des_processo, ls_cod_cliente, ls_riferimenti, ls_flag_prodotto, ls_sql, &
		 ls_cod_prodotto_cu, ls_des_processo_cu, ls_cod_cliente_cu, ls_riferimenti_cu, ls_flag_prodotto_cu, ls_descrizione_cu , &
		 ls_descrizione_det_cu, ls_cod_prodotto_det_cu, ls_funzioni_cu, ls_cod_errore_cu, ls_effetto_cu, ls_cod_tipo_causa_cu, ls_cod_causa_cu, &
		 ls_cod_test_cu, ls_note_cu, ls_des_errore, ls_des_tipo_causa, ls_des_causa, ls_des_effetto, ls_des_test, ls_des_ap, ls_cod_mansionario, &
		 ls_responsabile, ls_cognome, ls_nome, ls_cliente_tes, ls_prodotto_tes, ls_elenco_partecipanti_cu, ls_revisione_cu
		 
dec    ld_valore_o_cu, ld_valore_s_cu, ld_valore_r_cu, ld_valore_ipr_cu		 

long   ll_anno_registrazione, ll_num_registrazione, ll_progressivo_cu, ll_prog_guasto_cu, ll_anno_ap_cu, ll_num_ap_cu, ll_riga, ll_anno_cu, ll_num_cu

datetime ldt_data_registrazione, ldt_data_cu, ldt_pc

dw_report.setredraw(false)
dw_report.reset()
dw_sel_analisi_fmea.accepttext()

ls_selezione = ""

ll_anno_registrazione = dw_sel_analisi_fmea.getitemnumber( 1, "anno_registrazione")
ll_num_registrazione = dw_sel_analisi_fmea.getitemnumber( 1, "num_registrazione")
ldt_data_registrazione = dw_sel_analisi_fmea.getitemdatetime( 1, "data_registrazione")
ls_descrizione = dw_sel_analisi_fmea.getitemstring( 1, "descrizione")
ls_cod_prodotto = dw_sel_analisi_fmea.getitemstring( 1, "cod_prodotto")
ls_des_processo = dw_sel_analisi_fmea.getitemstring( 1, "descrizione_processo")
ls_cod_cliente = dw_sel_analisi_fmea.getitemstring( 1, "cod_cliente")
ls_riferimenti = dw_sel_analisi_fmea.getitemstring( 1, "riferimenti")
ls_flag_prodotto = dw_sel_analisi_fmea.getitemstring( 1, "flag_prodotto")

declare cu_fmea dynamic cursor for sqlsa;

ls_sql = " select anno_registrazione, " + &
			"        num_registrazione " + &
		   " from   tes_analisi_fmea " + &
			" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 0 and ll_num_registrazione > 0 and not isnull(ll_num_registrazione) then
	ls_sql += " and anno_registrazione = " + string(ll_anno_registrazione)
	ls_sql += " and num_registrazione = " + string(ll_num_registrazione)
else
	
	if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 0 then
		ls_sql += " and anno_registrazione = " + string(ll_anno_registrazione)
	end if
	
	if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
		ls_sql += " and num_registrazione = " + string(ll_num_registrazione)
	end if	
	
	if not isnull(ls_descrizione) and ls_descrizione <> "" then
		ls_sql += " and descrizione like '%" + ls_descrizione + "%' "
	end if
	
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
		ls_sql += " and cod_prodotto = '" + ls_cod_prodotto + "' "
	end if
	
	if not isnull(ls_des_processo) and ls_des_processo <> "" then
		ls_sql += " and des_processo like '%" + ls_des_processo + "%' "
	end if
	
	if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
		ls_sql += " and cod_cliente = '" + ls_cod_cliente + "' "
	end if
	
	if not isnull(ls_riferimenti) and ls_riferimenti <> "" then
		ls_sql += " and riferimenti like '%" + ls_riferimenti + "%' "
	end if	
	
	if not isnull(ls_flag_prodotto) and ls_flag_prodotto <> "" then
		if ls_flag_prodotto = "S" then
			ls_sql += " and flag_prodotto = 'S' "
		elseif ls_flag_prodotto = "N" then			
			ls_sql += " and flag_prodotto = 'N' "
		end if
	end if		
	
end if

ls_sql += " order by anno_registrazione, num_registrazione "

PREPARE SQLSA FROM :ls_sql ;

open dynamic cu_fmea;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore cu_fmea. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch cu_fmea
	into  :ll_anno_cu,
	      :ll_num_cu;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
		close cu_fmea;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit		
	end if
	
	setnull(ldt_data_cu)	
	setnull(ls_cod_prodotto_cu)
	setnull(ls_des_processo_cu)
	setnull(ls_cod_cliente_cu)
	setnull(ls_riferimenti_cu)
	setnull(ls_flag_prodotto_cu)
	setnull(ls_descrizione_cu)
	setnull(ls_elenco_partecipanti_cu)
	setnull(ls_revisione_cu)
	
	SELECT data_registrazione,   
          cod_prodotto,   
          des_processo,   
          cod_cliente,   
          riferimenti,   
          flag_prodotto,   
          descrizione,
			 elenco_partecipanti,
			 revisione
   into   :ldt_data_cu,   
          :ls_cod_prodotto_cu,   
          :ls_des_processo_cu,   
          :ls_cod_cliente_cu,   
          :ls_riferimenti_cu,   
          :ls_flag_prodotto_cu,   
          :ls_descrizione_cu,
			 :ls_elenco_partecipanti_cu,
			 :ls_revisione_cu
    FROM  tes_analisi_fmea  
    WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( anno_registrazione = :ll_anno_cu ) AND  
          ( num_registrazione = :ll_num_cu )   ;
	
	setnull(ls_cliente_tes)
		
	select rag_soc_1
	into   :ls_cliente_tes
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente_cu;
			 
	setnull(ls_prodotto_tes)
	
	select des_prodotto
	into   :ls_prodotto_tes
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto_cu;
	
	
	declare cu_det cursor for
	select progressivo,
	       descrizione,
			 cod_prodotto,
			 funzioni
	from   det_analisi_fmea
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_cu and
			 num_registrazione = :ll_num_cu
	order  by progressivo;
	
	open cu_det;
	
	do while 1 = 1
		
		fetch cu_det into :ll_progressivo_cu,
		           :ls_descrizione_det_cu,
					  :ls_cod_prodotto_det_cu,
					  :ls_funzioni_cu;
					  
		if sqlca.sqlcode <> 0 then exit		
		
		declare cu_det_guasti cursor for
		select prog_guasto,   
         	 cod_errore,   
	          cod_effetto,   
	          cod_tipo_causa,   
	          cod_causa,   
	          cod_test,   
	          valore_o,   
	          valore_s,   
	          valore_r,   
	          valore_ipr,   
	          note,   
	          anno_az_prev,   
	          num_az_prev  
		from   det_analisi_fmea_guasti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_cu and
				 num_registrazione = :ll_num_cu and
				 progressivo = :ll_progressivo_cu
		order  by prog_guasto;
		
		open cu_det_guasti;
		
		do while 1 = 1
			fetch cu_det_guasti into :ll_prog_guasto_cu,
			                         :ls_cod_errore_cu,
											 :ls_effetto_cu,
											 :ls_cod_tipo_causa_cu,
											 :ls_cod_causa_cu,
											 :ls_cod_test_cu,
											 :ld_valore_o_cu,
											 :ld_valore_s_cu,
											 :ld_valore_r_cu,
											 :ld_valore_ipr_cu,
											 :ls_note_cu,
											 :ll_anno_ap_cu,
											 :ll_num_ap_cu;
											 
			if sqlca.sqlcode <> 0 then exit
			
			ll_riga = dw_report.insertrow( 0)
			
			// *** imposto i dati di testata
			
			dw_report.setitem( ll_riga, "note", ls_riferimenti_cu)
			dw_report.setitem( ll_riga, "cliente_tes", ls_cliente_tes)
			dw_report.setitem( ll_riga, "prodotto_tes", ls_prodotto_tes)
			dw_report.setitem( ll_riga, "des_processo_tes", ls_des_processo_cu)
			dw_report.setitem( ll_riga, "data_tes", ldt_data_cu)
			dw_report.setitem( ll_riga, "elenco_partecipanti", ls_elenco_partecipanti_cu)
			dw_report.setitem( ll_riga, "revisione", ls_revisione_cu)
			
			// ***
			
			dw_report.setitem( ll_riga, "anno", ll_anno_cu)
			dw_report.setitem( ll_riga, "numero", ll_num_cu)
			dw_report.setitem( ll_riga, "progressivo", ll_progressivo_cu)
			dw_report.setitem( ll_riga, "prog_guasto", ll_prog_guasto_cu)
			
			if not isnull( ls_descrizione_det_cu) and ls_descrizione_det_cu <> "" then
				dw_report.setitem( ll_riga, "funzione", ls_descrizione_det_cu)
			else
				if not isnull(ls_funzioni_cu) and ls_funzioni_cu <> "" then
					dw_report.setitem( ll_riga, "funzione", ls_funzioni_cu)
				else
					dw_report.setitem( ll_riga, "funzione", ls_cod_prodotto_det_cu)
				end if
			end if
			
			setnull(ls_des_errore)
			
			select des_difformita
			into   :ls_des_errore
			from   tab_difformita
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_errore = :ls_cod_errore_cu;					 
			
			dw_report.setitem( ll_riga, "guasto", ls_des_errore)
			
			setnull(ls_des_tipo_causa)
			
			select des_tipo_causa
			into   :ls_des_tipo_causa
			from   tab_tipi_cause
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_causa = :ls_cod_tipo_causa_cu;
			
			dw_report.setitem( ll_riga, "tipo_causa", ls_des_tipo_causa)
			
			setnull(ls_des_causa)
			
			select des_causa
			into   :ls_des_causa
			from   cause
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_causa = :ls_cod_causa_cu;
			
			dw_report.setitem( ll_riga, "causa", ls_des_causa)
			
			setnull(ls_des_effetto)
			
			select des_effetto
			into   :ls_des_effetto
			from   tab_effetti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_effetto = :ls_effetto_cu;
			
			dw_report.setitem( ll_riga, "effetto", ls_des_effetto)
			dw_report.setitem( ll_riga, "errore", ls_des_errore)
			
			setnull(ls_des_test)
			
			select des_test
			into   :ls_des_test
			from   tab_test
			where  cod_test = :ls_cod_test_cu;
			
			dw_report.setitem( ll_riga, "test", ls_des_test)
			dw_report.setitem( ll_riga, "valore_o", ld_valore_o_cu)
			dw_report.setitem( ll_riga, "valore_s", ld_valore_s_cu)
			dw_report.setitem( ll_riga, "valore_r", ld_valore_r_cu)
			dw_report.setitem( ll_riga, "valore_ipr", ld_valore_ipr_cu)
			dw_report.setitem( ll_riga, "note_riga", ls_note_cu)
			
			
			setnull(ls_des_ap)
			setnull(ldt_pc)
			setnull(ls_cod_mansionario)
			
			select descrizione,
			       data_prevista_chiusura,
					 emessa_da
			into   :ls_des_ap,
			       :ldt_pc,
					 :ls_cod_mansionario
			from   azioni_preventive
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_ap_cu and
					 num_registrazione = :ll_num_ap_cu;
					 
			if not isnull(ll_anno_ap_cu) and not isnull(ll_num_ap_cu) and ll_anno_ap_cu <> 0 and ll_num_ap_cu <> 0 then
				if isnull(ls_des_ap) then
					ls_des_ap = string(ll_anno_ap_cu) + "/" + string(ll_num_ap_cu)
				else
					ls_des_ap = string(ll_anno_ap_cu) + "/" + string(ll_num_ap_cu) + " " + ls_des_ap
				end if
			end if				
					 
			setnull(ls_responsabile)
			setnull(ls_cognome)
			setnull(ls_nome)
			
			select cognome, nome
			into   :ls_cognome, :ls_nome
			from   mansionari
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_resp_divisione = :ls_cod_mansionario;
					 
			ls_responsabile = ls_cognome + " " + ls_nome
			
			dw_report.setitem( ll_riga, "descrizione_tes", ls_descrizione_cu)			
			dw_report.setitem( ll_riga, "descrizione_ap", ls_des_ap)
			dw_report.setitem( ll_riga, "responsabile", ls_responsabile)
			dw_report.setitem( ll_riga, "data_pc", ldt_pc)
			
			
		loop
		
		close cu_det_guasti;
		
	loop

	close cu_det;

loop

close cu_fmea;

dw_report.sort()

dw_report.groupcalc()

dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview = 'Yes'

dw_folder.fu_selecttab(2)

dw_report.change_dw_current()

return 0
end function

event pc_setwindow;call super::pc_setwindow;//string ls_path_logo, ls_modify, ls_test
//
//
//set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)
//
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_1", 1, is_des_livello_N + "~tN")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_2", 1, is_des_livello_N + "~tN")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_3", 1, is_des_livello_N + "~tN")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_4", 1, is_des_livello_N + "~tN")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_5", 1, is_des_livello_N + "~tN")
//
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_1", 2, is_des_livello_D + "~tD")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_1", 3, is_des_livello_R + "~tR")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_1", 4, is_des_livello_E + "~tE")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_1", 5, is_des_livello_C + "~tC")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_1", 6, is_des_livello_T + "~tT")
//dw_sel_analisi_fmea.setvalue("flag_tipo_livello_1", 7, is_des_livello_A + "~tA")

string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_sel_analisi_fmea.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_sel_analisi_fmea
l_objects[2] = cb_report
l_objects[3] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

end event

on w_report_analisi_fmea.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.st_1=create st_1
this.dw_sel_analisi_fmea=create dw_sel_analisi_fmea
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.dw_sel_analisi_fmea
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
end on

on w_report_analisi_fmea.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.st_1)
destroy(this.dw_sel_analisi_fmea)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

type cb_annulla from commandbutton within w_report_analisi_fmea
integer x = 2126
integer y = 960
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string 	ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_sel_analisi_fmea.setitem(1,"data_da",ldt_null)

dw_sel_analisi_fmea.setitem(1,"data_a",ldt_null)

dw_sel_analisi_fmea.setitem(1,"cod_operaio",ls_null)

dw_sel_analisi_fmea.setitem(1,"cod_guasto",ls_null)

dw_sel_analisi_fmea.setitem(1,"attrezzatura_da",ls_null)

dw_sel_analisi_fmea.setitem(1,"attrezzatura_a",ls_null)

dw_sel_analisi_fmea.setitem(1,"cod_tipo_manutenzione",ls_null)

dw_sel_analisi_fmea.setitem(1,"cod_categoria",ls_null)

dw_sel_analisi_fmea.setitem(1,"cod_reparto",ls_null)

dw_sel_analisi_fmea.setitem(1,"cod_area",ls_null)

dw_sel_analisi_fmea.setitem(1,"cod_divisione",ls_null)

dw_sel_analisi_fmea.setitem(1,"flag_ordinaria","S")

dw_sel_analisi_fmea.setitem(1,"flag_ric_car","N")

dw_sel_analisi_fmea.setitem(1,"flag_ric_tel","N")

dw_sel_analisi_fmea.setitem(1,"flag_giro_isp","N")

dw_sel_analisi_fmea.setitem(1,"flag_altre","N")

f_PO_LoadDDDW_DW (dw_sel_analisi_fmea,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_report from commandbutton within w_report_analisi_fmea
integer x = 2514
integer y = 960
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;long ll_num
wf_report(ll_num)


rollback;
end event

type st_1 from statictext within w_report_analisi_fmea
integer x = 480
integer y = 960
integer width = 1600
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_sel_analisi_fmea from uo_cs_xx_dw within w_report_analisi_fmea
event ue_cod_attr ( )
event ue_carica_tipologie ( )
event ue_aree_aziendali ( )
integer x = 69
integer y = 180
integer width = 2811
integer height = 740
integer taborder = 10
string dataobject = "d_sel_report_analisi_fmea"
boolean border = false
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"attrezzatura_da")) then
	setitem(getrow(),"attrezzatura_a",getitemstring(getrow(),"attrezzatura_da"))
end if
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"attrezzatura_da")

ls_attr_a = getitemstring(getrow(),"attrezzatura_a")

ls_reparto = getitemstring(getrow(),"cod_reparto")

ls_categoria = getitemstring(getrow(),"cod_categoria")

ls_area = getitemstring(getrow(),"cod_area")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_sort (dw_sel_analisi_fmea,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ", " ordinamento ASC ")					  		
else
	f_PO_LoadDDDW_sort (dw_sel_analisi_fmea,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  	
end if

triggerevent("ue_carica_tipologie")
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "attrezzatura_da"
		postevent("ue_cod_attr")
		postevent("ue_carica_tipologie")
		
	case "attrezzatura_a","cod_reparto","cod_categoria","cod_area"
		postevent("ue_carica_tipologie")
		
	case "cod_divisione"
		postevent("ue_aree_aziendali")
		
	case "flag_tipo_livello_1"
		if i_coltext = "N" then
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_2", 0)
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_3", 0)
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_4", 0)
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_5", 0)
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_2", "N")
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_3", "N")
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_2", 220)
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_2", 2, is_des_livello_D + "~tD")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_2", 3, is_des_livello_R + "~tR")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_2", 4, is_des_livello_E + "~tE")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_2", 5, is_des_livello_C + "~tC")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_2", 6, is_des_livello_T + "~tT")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_2", 7, is_des_livello_A + "~tA")
		end if
		
	case "flag_tipo_livello_2"
		if i_coltext = "N" then
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_3", 0)
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_4", 0)
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_5", 0)
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_3", "N")
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_3", 221)
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_3", 2, is_des_livello_D + "~tD")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_3", 3, is_des_livello_R + "~tR")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_3", 4, is_des_livello_E + "~tE")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_3", 5, is_des_livello_C + "~tC")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_3", 6, is_des_livello_T + "~tT")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_3", 7, is_des_livello_A + "~tA")
		end if
		
	case "flag_tipo_livello_3"
		if i_coltext = "N" then
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_4", 0)
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_5", 0)
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_4", 222)
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_4", 2, is_des_livello_D + "~tD")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_4", 3, is_des_livello_R + "~tR")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_4", 4, is_des_livello_E + "~tE")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_4", 5, is_des_livello_C + "~tC")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_4", 6, is_des_livello_T + "~tT")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_4", 7, is_des_livello_A + "~tA")
		end if
		
	case "flag_tipo_livello_4"
		if i_coltext = "N" then
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_5", 0)
			dw_sel_analisi_fmea.setitem(dw_sel_analisi_fmea.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_analisi_fmea.settaborder("flag_tipo_livello_5", 223)
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_5", 2, is_des_livello_D + "~tD")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_5", 3, is_des_livello_R + "~tR")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_5", 4, is_des_livello_E + "~tE")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_5", 5, is_des_livello_C + "~tC")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_5", 6, is_des_livello_T + "~tT")
			dw_sel_analisi_fmea.setvalue("flag_tipo_livello_5", 7, is_des_livello_A + "~tA")
		end if		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_sel_analisi_fmea,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_sel_analisi_fmea,"cod_prodotto")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_analisi_fmea
integer x = 69
integer y = 180
integer width = 3954
integer height = 1620
integer taborder = 60
string dataobject = "d_report_analisi_fmea"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_analisi_fmea
integer x = 23
integer y = 20
integer width = 4073
integer height = 1832
integer taborder = 70
boolean border = false
end type

