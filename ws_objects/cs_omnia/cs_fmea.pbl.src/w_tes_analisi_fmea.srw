﻿$PBExportHeader$w_tes_analisi_fmea.srw
$PBExportComments$Tabella Aree Aziendali
forward
global type w_tes_analisi_fmea from w_cs_xx_principale
end type
type cb_dettaglio from commandbutton within w_tes_analisi_fmea
end type
type dw_tes_analisi_fmea_lista from uo_cs_xx_dw within w_tes_analisi_fmea
end type
type dw_tes_analisi_fmea_det from uo_cs_xx_dw within w_tes_analisi_fmea
end type
end forward

global type w_tes_analisi_fmea from w_cs_xx_principale
integer width = 2848
integer height = 2052
string title = "Gestione Analisi FMEA"
event ue_seleziona_riga ( )
cb_dettaglio cb_dettaglio
dw_tes_analisi_fmea_lista dw_tes_analisi_fmea_lista
dw_tes_analisi_fmea_det dw_tes_analisi_fmea_det
end type
global w_tes_analisi_fmea w_tes_analisi_fmea

event ue_seleziona_riga();return
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]

dw_tes_analisi_fmea_lista.set_dw_key("cod_azienda")
dw_tes_analisi_fmea_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_tes_analisi_fmea_det.set_dw_options(sqlca, &
                             dw_tes_analisi_fmea_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
iuo_dw_main = dw_tes_analisi_fmea_lista



end event

on w_tes_analisi_fmea.create
int iCurrent
call super::create
this.cb_dettaglio=create cb_dettaglio
this.dw_tes_analisi_fmea_lista=create dw_tes_analisi_fmea_lista
this.dw_tes_analisi_fmea_det=create dw_tes_analisi_fmea_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettaglio
this.Control[iCurrent+2]=this.dw_tes_analisi_fmea_lista
this.Control[iCurrent+3]=this.dw_tes_analisi_fmea_det
end on

on w_tes_analisi_fmea.destroy
call super::destroy
destroy(this.cb_dettaglio)
destroy(this.dw_tes_analisi_fmea_lista)
destroy(this.dw_tes_analisi_fmea_det)
end on

type cb_dettaglio from commandbutton within w_tes_analisi_fmea
integer x = 2395
integer y = 1848
integer width = 361
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;window_open_parm(w_det_analisi_fmea, -1, dw_tes_analisi_fmea_lista)

end event

type dw_tes_analisi_fmea_lista from uo_cs_xx_dw within w_tes_analisi_fmea
integer x = 23
integer y = 20
integer width = 2743
integer height = 580
integer taborder = 10
string dataobject = "d_tes_analisi_fmea_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_divisione, ls_old_select, new_select, where_clause, ls_count
long ll_i, ll_errore, ll_index

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, li_anno_esercizio, ll_num_registrazione

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "anno_registrazione")) or getitemnumber(l_idx, "anno_registrazione") = 0 then
		li_anno_esercizio = f_anno_esercizio()
		setitem(l_idx, "anno_registrazione", li_anno_esercizio)
	end if		
	
   if isnull(getitemnumber(l_idx, "num_registrazione")) or getitemnumber(l_idx, "num_registrazione") = 0 then
		
	   li_anno_esercizio = f_anno_esercizio()

		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_analisi_fmea
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 anno_registrazione = :li_anno_esercizio;
				 
		if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione ++
		end if
		
		setitem(l_idx, "num_registrazione", ll_num_registrazione)			
		
	end if
next


end event

event pcd_new;call super::pcd_new;LONG  l_Idx, li_anno_esercizio, ll_num_registrazione

setitem(getrow(), "data_registrazione",datetime(today(),00:00:00))

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "anno_registrazione")) or getitemnumber(l_idx, "anno_registrazione") = 0 then
		li_anno_esercizio = f_anno_esercizio()
		setitem(l_idx, "anno_registrazione", li_anno_esercizio)
	end if		
	
   if isnull(getitemnumber(l_idx, "num_registrazione")) or getitemnumber(l_idx, "num_registrazione") = 0 then
		
	   li_anno_esercizio = f_anno_esercizio()

		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_analisi_fmea
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 anno_registrazione = :li_anno_esercizio;
				 
		if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione ++
		end if
		
		setitem(l_idx, "num_registrazione", ll_num_registrazione)			
		
	end if
next

dw_tes_analisi_fmea_det.object.b_ricerca_cliente.enabled = true
dw_tes_analisi_fmea_det.object.b_ricerca_prodotto.enabled = true
end event

event pcd_modify;call super::pcd_modify;dw_tes_analisi_fmea_det.object.b_ricerca_cliente.enabled = true
dw_tes_analisi_fmea_det.object.b_ricerca_prodotto.enabled = true
end event

event pcd_save;call super::pcd_save;dw_tes_analisi_fmea_det.object.b_ricerca_cliente.enabled = false
dw_tes_analisi_fmea_det.object.b_ricerca_prodotto.enabled = false
end event

event pcd_view;call super::pcd_view;dw_tes_analisi_fmea_det.object.b_ricerca_cliente.enabled = false
dw_tes_analisi_fmea_det.object.b_ricerca_prodotto.enabled = false
end event

type dw_tes_analisi_fmea_det from uo_cs_xx_dw within w_tes_analisi_fmea
integer x = 23
integer y = 600
integer width = 2743
integer height = 1220
integer taborder = 20
string dataobject = "d_tes_analisi_fmea_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_analisi_fmea_det,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tes_analisi_fmea_det,"cod_prodotto")
end choose
end event

