﻿$PBExportHeader$w_tes_analisi_reclami.srw
$PBExportComments$Gestione Accettazione Materiali
forward
global type w_tes_analisi_reclami from w_cs_xx_principale
end type
type cb_report_ap from commandbutton within w_tes_analisi_reclami
end type
type cb_doc_errore from commandbutton within w_tes_analisi_reclami
end type
type cb_doc_analisi from commandbutton within w_tes_analisi_reclami
end type
type cb_doc_5 from cb_documenti_compilati within w_tes_analisi_reclami
end type
type cb_doc_4 from cb_documenti_compilati within w_tes_analisi_reclami
end type
type cb_doc_3 from cb_documenti_compilati within w_tes_analisi_reclami
end type
type cb_doc_2 from cb_documenti_compilati within w_tes_analisi_reclami
end type
type cb_azione_preventiva from commandbutton within w_tes_analisi_reclami
end type
type cb_azioni_correttive from cb_stock_ricerca within w_tes_analisi_reclami
end type
type cb_1 from commandbutton within w_tes_analisi_reclami
end type
type cb_ric_stock from cb_stock_ricerca within w_tes_analisi_reclami
end type
type cb_doc_1 from cb_documenti_compilati within w_tes_analisi_reclami
end type
type cb_ric_prod from cb_prod_ricerca within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_lista from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_1 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type tab_folder from tab within w_tes_analisi_reclami
end type
type tabpage_1 from userobject within tab_folder
end type
type tabpage_1 from userobject within tab_folder
end type
type tabpage_2 from userobject within tab_folder
end type
type tabpage_2 from userobject within tab_folder
end type
type tabpage_3 from userobject within tab_folder
end type
type tabpage_3 from userobject within tab_folder
end type
type tabpage_4 from userobject within tab_folder
end type
type tabpage_4 from userobject within tab_folder
end type
type tabpage_5 from userobject within tab_folder
end type
type tabpage_5 from userobject within tab_folder
end type
type tabpage_6 from userobject within tab_folder
end type
type tabpage_6 from userobject within tab_folder
end type
type tabpage_7 from userobject within tab_folder
end type
type tabpage_7 from userobject within tab_folder
end type
type tabpage_8 from userobject within tab_folder
end type
type tabpage_8 from userobject within tab_folder
end type
type tabpage_9 from userobject within tab_folder
end type
type tabpage_9 from userobject within tab_folder
end type
type tab_folder from tab within w_tes_analisi_reclami
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
tabpage_7 tabpage_7
tabpage_8 tabpage_8
tabpage_9 tabpage_9
end type
type dw_tes_analisi_reclami_det_5 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_9 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_6 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_7 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_8 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_4 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_3 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
type dw_tes_analisi_reclami_det_2 from uo_cs_xx_dw within w_tes_analisi_reclami
end type
end forward

global type w_tes_analisi_reclami from w_cs_xx_principale
integer width = 3241
integer height = 2744
string title = "Analisi 8D"
event ue_azioni_correttive ( )
cb_report_ap cb_report_ap
cb_doc_errore cb_doc_errore
cb_doc_analisi cb_doc_analisi
cb_doc_5 cb_doc_5
cb_doc_4 cb_doc_4
cb_doc_3 cb_doc_3
cb_doc_2 cb_doc_2
cb_azione_preventiva cb_azione_preventiva
cb_azioni_correttive cb_azioni_correttive
cb_1 cb_1
cb_ric_stock cb_ric_stock
cb_doc_1 cb_doc_1
cb_ric_prod cb_ric_prod
dw_tes_analisi_reclami_lista dw_tes_analisi_reclami_lista
dw_tes_analisi_reclami_det_1 dw_tes_analisi_reclami_det_1
tab_folder tab_folder
dw_tes_analisi_reclami_det_5 dw_tes_analisi_reclami_det_5
dw_tes_analisi_reclami_det_9 dw_tes_analisi_reclami_det_9
dw_tes_analisi_reclami_det_6 dw_tes_analisi_reclami_det_6
dw_tes_analisi_reclami_det_7 dw_tes_analisi_reclami_det_7
dw_tes_analisi_reclami_det_8 dw_tes_analisi_reclami_det_8
dw_tes_analisi_reclami_det_4 dw_tes_analisi_reclami_det_4
dw_tes_analisi_reclami_det_3 dw_tes_analisi_reclami_det_3
dw_tes_analisi_reclami_det_2 dw_tes_analisi_reclami_det_2
end type
global w_tes_analisi_reclami w_tes_analisi_reclami

type variables

end variables

forward prototypes
public function integer wf_crea_liste_con_comp (long fl_num, ref long al_prog_liste_con_comp, ref long al_num_edizione, ref long al_num_versione)
end prototypes

event ue_azioni_correttive();iuo_dw_main = dw_tes_analisi_reclami_det_6
dw_tes_analisi_reclami_det_6.change_dw_current()
triggerevent("pc_retrieve")

iuo_dw_main = dw_tes_analisi_reclami_det_7
dw_tes_analisi_reclami_det_7.change_dw_current()
triggerevent("pc_retrieve")

iuo_dw_main = dw_tes_analisi_reclami_lista
dw_tes_analisi_reclami_lista.change_dw_current()
end event

public function integer wf_crea_liste_con_comp (long fl_num, ref long al_prog_liste_con_comp, ref long al_num_edizione, ref long al_num_versione);string ls_des_lista, ls_cod_area_aziendale, ls_cod_resp_divisione, ls_approvato_da, &
       ls_autorizzato_da, ls_validato_da, ls_flag_uso, ls_flag_recuperato, ls_note, &
       ls_sql, ls_des_domanda, ls_flag_tipo_risposta, ls_des_check_1, ls_des_check_2, &
       ls_check_button, ls_des_option[], ls_option_button, ls_des_option_libero, &
       ls_campo_libero, ls_des_libero, ls_des_subtotale
long   ll_valido_per, ll_num_opzioni, ll_num_reg_lista, ll_prog_riga_liste_contr, ll_i, &
       ll_valore_riferimento, ll_peso_check_1, ll_peso_check_2, ll_peso_option_1, &
       ll_peso_option_2, ll_peso_option_3, ll_peso_option_4, ll_peso_option_5, &
       ll_peso_option_libero, ll_val_subtotale
datetime ldt_emesso_il, ldt_autorizzato_il

ll_num_reg_lista = fl_num

select tes_liste_controllo.num_versione,
       tes_liste_controllo.num_edizione,
       tes_liste_controllo.des_lista,
       tes_liste_controllo.cod_area_aziendale,
       tes_liste_controllo.cod_resp_divisione,
       tes_liste_controllo.approvato_da,
       tes_liste_controllo.emesso_il,
       tes_liste_controllo.autorizzato_da,
       tes_liste_controllo.autorizzato_il,
       tes_liste_controllo.valido_per,
       tes_liste_controllo.validato_da,
       tes_liste_controllo.flag_uso,
       tes_liste_controllo.flag_recuperato,
       tes_liste_controllo.note,
       tes_liste_controllo.valore_riferimento
into   :al_num_versione,
       :al_num_edizione,
       :ls_des_lista,
       :ls_cod_area_aziendale,
       :ls_cod_resp_divisione,
       :ls_approvato_da,
       :ldt_emesso_il,
       :ls_autorizzato_da,
       :ldt_autorizzato_il,
       :ll_valido_per,
       :ls_validato_da,
       :ls_flag_uso,
       :ls_flag_recuperato,
       :ls_note,
       :ll_valore_riferimento
from   tes_liste_controllo
where  tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and
       tes_liste_controllo.num_reg_lista = :ll_num_reg_lista and
       tes_liste_controllo.flag_valido = 'S';
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Liste controllo","Lista controllo specificata (" + string(ll_num_reg_lista) + ") inesistente")
	return -1
elseif sqlca.sqlcode <> 0 then
	g_mb.messagebox("Liste controllo","Errore in ricerca lista controllo specificata (" + string(ll_num_reg_lista) + ")~r~nDettaglio errore: " + sqlca.sqlerrtext)
	return -1
end if
select max(tes_liste_con_comp.prog_liste_con_comp)
into   :al_prog_liste_con_comp
from   tes_liste_con_comp
where  tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda and
       tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista;

if isnull(al_prog_liste_con_comp) or al_prog_liste_con_comp = 0 then
   al_prog_liste_con_comp = 1
else
   al_prog_liste_con_comp ++
end if

insert into tes_liste_con_comp
           (cod_azienda,
            num_reg_lista,
            prog_liste_con_comp,
            num_versione,
            num_edizione,
            des_lista,
            cod_area_aziendale,
            cod_resp_divisione,
            approvato_da,
            emesso_il,
            autorizzato_da,
            autorizzato_il,
            valido_per,
            validato_da,
            flag_uso,
            flag_recuperato,
            note,
            valore_riferimento,
            valore_ottenuto,
            azione_intrapresa)
values     (:s_cs_xx.cod_azienda,
            :ll_num_reg_lista,
            :al_prog_liste_con_comp,
            :al_num_versione,
            :al_num_edizione,
            :ls_des_lista,
            :ls_cod_area_aziendale,
            :ls_cod_resp_divisione,
            :ls_approvato_da,
            :ldt_emesso_il,
            :ls_autorizzato_da,
            :ldt_autorizzato_il,
            :ll_valido_per,
            :ls_validato_da,
            :ls_flag_uso,
            'N',
            :ls_note,
            :ll_valore_riferimento,
            0,
            null );

if sqlca.sqlcode = -1 then
   g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di inserimento Testata Liste di Controllo Compilate.~r~nDettaglio Errore: " + sqlca.sqlerrtext, &
              exclamation!, ok!)
   rollback;
   return -1
end if

declare cu_dett dynamic cursor for sqlsa;

ls_sql = "select det_liste_controllo.prog_riga_liste_contr," + &
                "det_liste_controllo.des_domanda," + &
                "det_liste_controllo.flag_tipo_risposta," + &   
                "det_liste_controllo.des_check_1," + &   
                "det_liste_controllo.des_check_2," + &   
                "det_liste_controllo.check_button," + &   
                "det_liste_controllo.des_option_1," + &   
                "det_liste_controllo.des_option_2," + &   
                "det_liste_controllo.des_option_3," + &   
                "det_liste_controllo.des_option_4," + &   
                "det_liste_controllo.des_option_5," + &   
                "det_liste_controllo.num_opzioni," + &   
                "det_liste_controllo.option_button," + &
                "det_liste_controllo.peso_check_1," + &
                "det_liste_controllo.peso_check_2," + &
                "det_liste_controllo.peso_option_1," + &
                "det_liste_controllo.peso_option_2," + &
                "det_liste_controllo.peso_option_3," + &
                "det_liste_controllo.peso_option_4," + &
                "det_liste_controllo.peso_option_5," + &
                "det_liste_controllo.peso_option_libero," + &
                "det_liste_controllo.des_option_libero," + &
                "det_liste_controllo.campo_libero," + &
                "det_liste_controllo.des_libero," + &
                "det_liste_controllo.des_subtotale," + &
                "det_liste_controllo.val_subtotale  " + &
         "from   det_liste_controllo " + & 
         "where  det_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
                "det_liste_controllo.num_reg_lista = " + string(ll_num_reg_lista) + " and " + & 
                "det_liste_controllo.num_versione = " + string(al_num_versione) + " and " + & 
                "det_liste_controllo.num_edizione = " + string(al_num_edizione)

prepare sqlsa from :ls_sql ;

open dynamic cu_dett ;

do while 0 = 0
   fetch cu_dett into :ll_prog_riga_liste_contr, :ls_des_domanda, :ls_flag_tipo_risposta, :ls_des_check_1, :ls_des_check_2, :ls_check_button, :ls_des_option[1], :ls_des_option[2], :ls_des_option[3], :ls_des_option[4], :ls_des_option[5], :ll_num_opzioni, :ls_option_button, :ll_peso_check_1, :ll_peso_check_2, :ll_peso_option_1, :ll_peso_option_2, :ll_peso_option_3, :ll_peso_option_4, :ll_peso_option_5, :ll_peso_option_libero, :ls_des_option_libero, :ls_campo_libero, :ls_des_libero, :ls_des_subtotale, :ll_val_subtotale ;
   if sqlca.sqlcode <> 0 then exit

   insert into det_liste_con_comp
         (cod_azienda,
          num_reg_lista,
          prog_liste_con_comp,
          prog_riga_liste_contr,
          des_domanda,
          flag_tipo_risposta,
          des_check_1,
          des_check_2,
          check_button,
          des_option_1,
          des_option_2,
          des_option_3,
          des_option_4,
          des_option_5,
          num_opzioni,
          option_button,
          peso_check_1,
          peso_check_2,
          peso_option_1,
          peso_option_2,
          peso_option_3,
          peso_option_4,
          peso_option_5,
          peso_option_libero,
          des_option_libero,
          campo_libero,
          des_libero,
          des_subtotale,
          val_subtotale)
  values (:s_cs_xx.cod_azienda,
          :ll_num_reg_lista,
          :al_prog_liste_con_comp,
          :ll_prog_riga_liste_contr,
          :ls_des_domanda,
          :ls_flag_tipo_risposta,
          :ls_des_check_1,
          :ls_des_check_2,
          :ls_check_button,
          :ls_des_option[1],
          :ls_des_option[2],
          :ls_des_option[3],
          :ls_des_option[4],
          :ls_des_option[5],
          :ll_num_opzioni,
          :ls_option_button,
          :ll_peso_check_1,
          :ll_peso_check_2,
          :ll_peso_option_1,
          :ll_peso_option_2,
          :ll_peso_option_3,
          :ll_peso_option_4,
          :ll_peso_option_5,
          :ll_peso_option_libero,
          :ls_des_option_libero,
          :ls_campo_libero,
          :ls_des_libero,
          :ls_des_subtotale,
          :ll_val_subtotale);

   if sqlca.sqlcode = -1 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di inserimento Dettaglio Liste di Controllo Compilate.~r~nDettaglio errore: " + sqlca.sqlerrtext, &
                 exclamation!, ok!)
      rollback;
      close cu_dett;
      return -1
   end if
loop
close cu_dett;
return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_flag_rif_ord, ls_flag_rif_bol, ls_flag_rif_fat,l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[]

dw_tes_analisi_reclami_det_6.set_dw_options(sqlca,pcca.null_object, c_noenablepopup + c_noretrieveonopen + c_nonew + c_nodelete + c_nomodify,c_default)
dw_tes_analisi_reclami_det_7.set_dw_options(sqlca,pcca.null_object, c_noenablepopup + c_noretrieveonopen + c_nonew + c_nodelete + c_nomodify,c_default)

dw_tes_analisi_reclami_lista.set_dw_key("cod_azienda")
dw_tes_analisi_reclami_lista.set_dw_key("anno_registrazione")
dw_tes_analisi_reclami_lista.set_dw_key("num_registrazione")
dw_tes_analisi_reclami_lista.set_dw_options( sqlca,pcca.null_object, c_default, c_default)
dw_tes_analisi_reclami_det_1.set_dw_options(sqlca,dw_tes_analisi_reclami_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_analisi_reclami_det_2.set_dw_options(sqlca,dw_tes_analisi_reclami_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_analisi_reclami_det_3.set_dw_options(sqlca,dw_tes_analisi_reclami_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_analisi_reclami_det_4.set_dw_options(sqlca,dw_tes_analisi_reclami_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_analisi_reclami_det_5.set_dw_options(sqlca,dw_tes_analisi_reclami_lista,c_sharedata+c_scrollparent,c_default)


dw_tes_analisi_reclami_det_8.set_dw_options(sqlca,dw_tes_analisi_reclami_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_analisi_reclami_det_9.set_dw_options(sqlca,dw_tes_analisi_reclami_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_tes_analisi_reclami_lista

cb_ric_prod.enabled = false
cb_ric_stock.enabled = false
//cb_ricerca_cliente.enabled = false

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_1, &
                  "cod_deposito", &
						sqlca, &
                  "anag_depositi", &
						"cod_deposito", &
						"des_deposito",&
                  "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_1, &
                  "cod_mansionario_verifica", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_1, &
                  "cod_mansionario_valida", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_2, &
                  "cod_mansionario_1", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
						
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_2, &
                  "cod_mansionario_2", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
						
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_2, &
                  "cod_mansionario_3", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
						
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_2, &
                  "cod_mansionario_4", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
						
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_2, &
                  "cod_mansionario_5", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
						
						
// dw_3

f_PO_LoadDDDW_DW(dw_tes_analisi_reclami_det_3,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
//							"cod_tipo_causa", &
//							sqlca, &
//							"tab_tipi_cause", &
//							"cod_tipo_causa", &
//                     "des_tipo_causa", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
//
//f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
//							"cod_causa", &
//							sqlca, &
//							"cause", &
//							"cod_causa", &
//                     "des_causa", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
							
// dw_4			

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_4, &
                  "cod_mansionario_autorizza", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
						
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_4, &
                  "cod_mansionario_efficacia", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
						
// dw_7						

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_8, &
                  "cod_mansionario_preventiva", &
						sqlca, &
                  "mansionari", &
						"cod_resp_divisione", &
						" nome ",&
                  "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")								  

// dw_5

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_tipo_causa_1", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_1", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
							
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_tipo_causa_2", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_2", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
							
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_tipo_causa_3", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_3", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
							
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_tipo_causa_4", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_4", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
							
f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_tipo_causa_5", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_5", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")							


f_PO_LoadDDDW_DW(dw_tes_analisi_reclami_det_5,"num_lista_controllo_1",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )

f_PO_LoadDDDW_DW(dw_tes_analisi_reclami_det_5,"num_lista_controllo_2",sqlca,&
						"tes_liste_controllo","num_reg_lista","des_lista",&
						  "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )
						  
f_PO_LoadDDDW_DW(dw_tes_analisi_reclami_det_5,"num_lista_controllo_3",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )
					  
f_PO_LoadDDDW_DW(dw_tes_analisi_reclami_det_5,"num_lista_controllo_4",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )
					  
f_PO_LoadDDDW_DW(dw_tes_analisi_reclami_det_5,"num_lista_controllo_5",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )					  
end event

on w_tes_analisi_reclami.create
int iCurrent
call super::create
this.cb_report_ap=create cb_report_ap
this.cb_doc_errore=create cb_doc_errore
this.cb_doc_analisi=create cb_doc_analisi
this.cb_doc_5=create cb_doc_5
this.cb_doc_4=create cb_doc_4
this.cb_doc_3=create cb_doc_3
this.cb_doc_2=create cb_doc_2
this.cb_azione_preventiva=create cb_azione_preventiva
this.cb_azioni_correttive=create cb_azioni_correttive
this.cb_1=create cb_1
this.cb_ric_stock=create cb_ric_stock
this.cb_doc_1=create cb_doc_1
this.cb_ric_prod=create cb_ric_prod
this.dw_tes_analisi_reclami_lista=create dw_tes_analisi_reclami_lista
this.dw_tes_analisi_reclami_det_1=create dw_tes_analisi_reclami_det_1
this.tab_folder=create tab_folder
this.dw_tes_analisi_reclami_det_5=create dw_tes_analisi_reclami_det_5
this.dw_tes_analisi_reclami_det_9=create dw_tes_analisi_reclami_det_9
this.dw_tes_analisi_reclami_det_6=create dw_tes_analisi_reclami_det_6
this.dw_tes_analisi_reclami_det_7=create dw_tes_analisi_reclami_det_7
this.dw_tes_analisi_reclami_det_8=create dw_tes_analisi_reclami_det_8
this.dw_tes_analisi_reclami_det_4=create dw_tes_analisi_reclami_det_4
this.dw_tes_analisi_reclami_det_3=create dw_tes_analisi_reclami_det_3
this.dw_tes_analisi_reclami_det_2=create dw_tes_analisi_reclami_det_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report_ap
this.Control[iCurrent+2]=this.cb_doc_errore
this.Control[iCurrent+3]=this.cb_doc_analisi
this.Control[iCurrent+4]=this.cb_doc_5
this.Control[iCurrent+5]=this.cb_doc_4
this.Control[iCurrent+6]=this.cb_doc_3
this.Control[iCurrent+7]=this.cb_doc_2
this.Control[iCurrent+8]=this.cb_azione_preventiva
this.Control[iCurrent+9]=this.cb_azioni_correttive
this.Control[iCurrent+10]=this.cb_1
this.Control[iCurrent+11]=this.cb_ric_stock
this.Control[iCurrent+12]=this.cb_doc_1
this.Control[iCurrent+13]=this.cb_ric_prod
this.Control[iCurrent+14]=this.dw_tes_analisi_reclami_lista
this.Control[iCurrent+15]=this.dw_tes_analisi_reclami_det_1
this.Control[iCurrent+16]=this.tab_folder
this.Control[iCurrent+17]=this.dw_tes_analisi_reclami_det_5
this.Control[iCurrent+18]=this.dw_tes_analisi_reclami_det_9
this.Control[iCurrent+19]=this.dw_tes_analisi_reclami_det_6
this.Control[iCurrent+20]=this.dw_tes_analisi_reclami_det_7
this.Control[iCurrent+21]=this.dw_tes_analisi_reclami_det_8
this.Control[iCurrent+22]=this.dw_tes_analisi_reclami_det_4
this.Control[iCurrent+23]=this.dw_tes_analisi_reclami_det_3
this.Control[iCurrent+24]=this.dw_tes_analisi_reclami_det_2
end on

on w_tes_analisi_reclami.destroy
call super::destroy
destroy(this.cb_report_ap)
destroy(this.cb_doc_errore)
destroy(this.cb_doc_analisi)
destroy(this.cb_doc_5)
destroy(this.cb_doc_4)
destroy(this.cb_doc_3)
destroy(this.cb_doc_2)
destroy(this.cb_azione_preventiva)
destroy(this.cb_azioni_correttive)
destroy(this.cb_1)
destroy(this.cb_ric_stock)
destroy(this.cb_doc_1)
destroy(this.cb_ric_prod)
destroy(this.dw_tes_analisi_reclami_lista)
destroy(this.dw_tes_analisi_reclami_det_1)
destroy(this.tab_folder)
destroy(this.dw_tes_analisi_reclami_det_5)
destroy(this.dw_tes_analisi_reclami_det_9)
destroy(this.dw_tes_analisi_reclami_det_6)
destroy(this.dw_tes_analisi_reclami_det_7)
destroy(this.dw_tes_analisi_reclami_det_8)
destroy(this.dw_tes_analisi_reclami_det_4)
destroy(this.dw_tes_analisi_reclami_det_3)
destroy(this.dw_tes_analisi_reclami_det_2)
end on

event open;call super::open;dw_tes_analisi_reclami_det_1.object.b_ricerca_cliente.enabled=false
end event

type cb_report_ap from commandbutton within w_tes_analisi_reclami
integer x = 1897
integer y = 1080
integer width = 389
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report A.P."
end type

event clicked;if not isvalid(w_report_azioni_prev) then
	
	if dw_tes_analisi_reclami_lista.getrow() < 1 then
		g_mb.messagebox("OMNIA","Selezionare un'azione preventiva dalla lista",stopsign!)
		return -1
	end if

	setnull(s_cs_xx.parametri.parametro_d_1)
	setnull(s_cs_xx.parametri.parametro_d_2)
	s_cs_xx.parametri.parametro_d_1 = dw_tes_analisi_reclami_lista.getitemnumber(dw_tes_analisi_reclami_lista.getrow(),"anno_az_preventiva")
	s_cs_xx.parametri.parametro_d_2 = dw_tes_analisi_reclami_lista.getitemnumber(dw_tes_analisi_reclami_lista.getrow(),"num_az_preventiva")
	
	if isnull(s_cs_xx.parametri.parametro_d_2) or s_cs_xx.parametri.parametro_d_2 = 0 or isnull(s_cs_xx.parametri.parametro_d_1) or s_cs_xx.parametri.parametro_d_1 = 0 then
		g_mb.messagebox("OMNIA","Selezionare un'azione Preventiva dalla lista",stopsign!)
		return -1
	end if
	
	window_open(w_report_azioni_prev,-1)

end if
end event

type cb_doc_errore from commandbutton within w_tes_analisi_reclami
integer x = 2194
integer y = 2020
integer width = 393
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long    ll_i, ll_progressivo, ll_prog_mimetype, ll_anno_registrazione, ll_num_registrazione
integer li_risposta
string  ls_db, ls_doc
transaction sqlcb
blob    lbl_null, lbl_blob

setnull(lbl_null)

ll_i = dw_tes_analisi_reclami_lista.getrow()

if ll_i < 1 then return

ll_anno_registrazione = dw_tes_analisi_reclami_lista.getitemnumber( ll_i, "anno_registrazione")
ll_num_registrazione = dw_tes_analisi_reclami_lista.getitemnumber( ll_i, "num_registrazione")

select prog_mimetype_2
into :ll_prog_mimetype
from tes_analisi_reclami
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione and
	num_registrazione = :ll_num_registrazione;


selectblob blob_errore
into :lbl_blob
from tes_analisi_reclami
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione and 
	num_registrazione = :ll_num_registrazione;
	
if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tes_analisi_reclami
		set blob_errore = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and 
			num_registrazione = :ll_num_registrazione;
	else
		updateblob tes_analisi_reclami
		set blob_errore = :lbl_blob
		where
			cocod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and 
			num_registrazione = :ll_num_registrazione;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update tes_analisi_reclami
	set prog_mimetype_2 = :ll_prog_mimetype
	where
		cod_acod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and 
		num_registrazione = :ll_num_registrazione;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if
// *** se il blob non è nullo ed esiste il prog mimetype: sono documenti intranet
// *** se il blob non è nullo ma il progressivo si: sono documenti client/server
// *** se il blob è nullo: procedo con l'inserimento intranet
//
//if isnull(s_cs_xx.parametri.parametro_bl_1) or ( not isnull(s_cs_xx.parametri.parametro_bl_1) and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) then
//
//	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
//	
//	window_open(w_ole_documenti, 0)
//	
//	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
//	
//	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
//		
//		updateblob tes_analisi_reclami
//		set        blob_errore = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :ll_anno_registrazione and 
//					  num_registrazione = :ll_num_registrazione;
//				
//		if sqlca.sqlcode <> 0 then
//			g_mb.messagebox("Prova", sqlca.sqlerrtext)
//		end if	
//			
//		update     tes_analisi_reclami
//		set        prog_mimetype_2 = :ll_prog_mimetype
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :ll_anno_registrazione and 
//					  num_registrazione = :ll_num_registrazione;
//					  
//		commit;
//		
//	end if
//else
//	
//	window_open(w_ole, 0)
//	
//	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//		
//		updateblob tes_analisi_reclami
//		set        blob_errore = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :ll_anno_registrazione and 
//					  num_registrazione = :ll_num_registrazione;
//
//		commit;
//		
//	end if
//	
//end if

return

end event

type cb_doc_analisi from commandbutton within w_tes_analisi_reclami
integer x = 183
integer y = 1000
integer width = 393
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long    ll_i, ll_progressivo, ll_prog_mimetype, ll_anno_registrazione, ll_num_registrazione
integer li_risposta
string  ls_db, ls_doc
transaction sqlcb
blob    lbl_null, lbl_blob

setnull(lbl_null)

ll_i = dw_tes_analisi_reclami_lista.getrow()

if ll_i < 1 then return

ll_anno_registrazione = dw_tes_analisi_reclami_lista.getitemnumber( ll_i, "anno_registrazione")
ll_num_registrazione = dw_tes_analisi_reclami_lista.getitemnumber( ll_i, "num_registrazione")

select prog_mimetype_1
into :ll_prog_mimetype
from tes_analisi_reclami
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione and
	num_registrazione = :ll_num_registrazione;


selectblob blob_analisi
into :lbl_blob
from tes_analisi_reclami
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_registrazione and 
	num_registrazione = :ll_num_registrazione;
	
if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tes_analisi_reclami
		set blob_analisi = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and 
			num_registrazione = :ll_num_registrazione;
	else
		updateblob tes_analisi_reclami
		set blob_analisi = :lbl_blob
		where
			cocod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and 
			num_registrazione = :ll_num_registrazione;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update tes_analisi_reclami
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_acod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and 
		num_registrazione = :ll_num_registrazione;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if
	
// *** se il blob non è nullo ed esiste il prog mimetype: sono documenti intranet
// *** se il blob non è nullo ma il progressivo si: sono documenti client/server
// *** se il blob è nullo: procedo con l'inserimento intranet
//
//if isnull(s_cs_xx.parametri.parametro_bl_1) or ( not isnull(s_cs_xx.parametri.parametro_bl_1) and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) then
//
//	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
//	
//	window_open(w_ole_documenti, 0)
//	
//	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
//	
//	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
//		
//		updateblob tes_analisi_reclami
//		set        blob_analisi = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :ll_anno_registrazione and 
//					  num_registrazione = :ll_num_registrazione;
//				
//		if sqlca.sqlcode <> 0 then
//			g_mb.messagebox("Prova", sqlca.sqlerrtext)
//		end if	
//			
//		update     tes_analisi_reclami
//		set        prog_mimetype_1 = :ll_prog_mimetype
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :ll_anno_registrazione and 
//					  num_registrazione = :ll_num_registrazione;
//					  
//		commit;
//		
//	end if
//else
//	
//	window_open(w_ole, 0)
//	
//	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//		
//		updateblob tes_analisi_reclami
//		set        blob_analisi = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :ll_anno_registrazione and 
//					  num_registrazione = :ll_num_registrazione;
//
//		commit;
//		
//	end if
//	
//end if

return

end event

type cb_doc_5 from cb_documenti_compilati within w_tes_analisi_reclami
integer x = 2263
integer y = 1820
integer width = 69
integer height = 76
integer taborder = 100
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;//string ls_ritorno
//
//s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_analisi_reclami_det_5
//s_cs_xx.parametri.parametro_s_1 = "doc_compilato_5"
//s_cs_xx.parametri.parametro_s_2 = "DQ1"
//s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
//
//if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
//   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
//
//   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
////      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
////      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
//   end if      
//else
//   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
//end if
//

long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_anno_reg_visita, ll_num_reg_visita, ll_progressivo
   
ll_i[1] = dw_tes_analisi_reclami_lista.getrow()

if isnull(dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_5")) or &
   dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_5") = 0 then
      g_mb.messagebox("Schede Intervento","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione interventi", StopSign!)
      return
end if
ll_num_reg_lista = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_lista_controllo_5") 
ll_prog_liste_con_comp = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"prog_lista_controllo_5") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
	
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
	
   wf_crea_liste_con_comp( ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"anno_registrazione")
   ll_num_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_registrazione")
   
   update tes_analisi_reclami
   set    prog_versione_5 = :ll_num_versione,
          prog_edizione_5 = :ll_num_edizione,
          num_lista_controllo_5 = :ll_num_reg_lista,
          prog_lista_controllo_5 = :ll_prog_liste_con_comp
   where  cod_azienda = :s_cs_xx.cod_azienda and
          anno_registrazione = :ll_anno_reg_visita and 
          num_registrazione = :ll_num_reg_visita ;
   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   parent.triggerevent("pc_save")

end if

s_cs_xx.parametri.parametro_s_1 = "5"

window_open_parm(w_det_liste_con_comp_fmea, 0, dw_tes_analisi_reclami_lista)

end event

type cb_doc_4 from cb_documenti_compilati within w_tes_analisi_reclami
integer x = 2263
integer y = 1540
integer width = 69
integer height = 76
integer taborder = 100
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;//string ls_ritorno
//
//s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_analisi_reclami_det_5
//s_cs_xx.parametri.parametro_s_1 = "doc_compilato_4"
//s_cs_xx.parametri.parametro_s_2 = "DQ1"
//s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
//
//if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
//   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
//
//   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
////      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
////      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
//   end if      
//else
//   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
//end if
//

long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_anno_reg_visita, ll_num_reg_visita, ll_progressivo
   
ll_i[1] = dw_tes_analisi_reclami_lista.getrow()

if isnull(dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_4")) or &
   dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_4") = 0 then
      g_mb.messagebox("Schede Intervento","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione interventi", StopSign!)
      return
end if
ll_num_reg_lista = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_lista_controllo_4") 
ll_prog_liste_con_comp = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"prog_lista_controllo_4") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
	
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
	
   wf_crea_liste_con_comp( ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"anno_registrazione")
   ll_num_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_registrazione")
   
   update tes_analisi_reclami
   set    prog_versione_4 = :ll_num_versione,
          prog_edizione_4 = :ll_num_edizione,
          num_lista_controllo_4 = :ll_num_reg_lista,
          prog_lista_controllo_4 = :ll_prog_liste_con_comp
   where  cod_azienda = :s_cs_xx.cod_azienda and
          anno_registrazione = :ll_anno_reg_visita and 
          num_registrazione = :ll_num_reg_visita ;
   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   parent.triggerevent("pc_save")

end if

s_cs_xx.parametri.parametro_s_1 = "4"

window_open_parm(w_det_liste_con_comp_fmea, 0, dw_tes_analisi_reclami_lista)

end event

type cb_doc_3 from cb_documenti_compilati within w_tes_analisi_reclami
integer x = 2263
integer y = 1260
integer width = 69
integer height = 76
integer taborder = 100
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;//string ls_ritorno
//
//s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_analisi_reclami_det_5
//s_cs_xx.parametri.parametro_s_1 = "doc_compilato_3"
//s_cs_xx.parametri.parametro_s_2 = "DQ1"
//s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
//
//if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
//   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
//
//   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
////      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
////      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
//   end if      
//else
//   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
//end if
//

long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_anno_reg_visita, ll_num_reg_visita, ll_progressivo
   
ll_i[1] = dw_tes_analisi_reclami_lista.getrow()

if isnull(dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_3")) or &
   dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_3") = 0 then
      g_mb.messagebox("Schede Intervento","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione interventi", StopSign!)
      return
end if
ll_num_reg_lista = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_lista_controllo_3") 
ll_prog_liste_con_comp = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"prog_lista_controllo_3") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
	
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
	
   wf_crea_liste_con_comp( ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"anno_registrazione")
   ll_num_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_registrazione")
   
   update tes_analisi_reclami
   set    prog_versione_3 = :ll_num_versione,
          prog_edizione_3 = :ll_num_edizione,
          num_lista_controllo_3 = :ll_num_reg_lista,
          prog_lista_controllo_3 = :ll_prog_liste_con_comp
   where  cod_azienda = :s_cs_xx.cod_azienda and
          anno_registrazione = :ll_anno_reg_visita and 
          num_registrazione = :ll_num_reg_visita ;
   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   parent.triggerevent("pc_save")

end if

s_cs_xx.parametri.parametro_s_1 = "3"

window_open_parm(w_det_liste_con_comp_fmea, 0, dw_tes_analisi_reclami_lista)

end event

type cb_doc_2 from cb_documenti_compilati within w_tes_analisi_reclami
integer x = 2263
integer y = 980
integer width = 69
integer height = 76
integer taborder = 80
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;//string ls_ritorno
//
//s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_analisi_reclami_det_5
//s_cs_xx.parametri.parametro_s_1 = "doc_compilato_2"
//s_cs_xx.parametri.parametro_s_2 = "DQ1"
//s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
//
//if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
//   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
//
//   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
////      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
////      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
//   end if      
//else
//   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
//end if
//

long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_anno_reg_visita, ll_num_reg_visita, ll_progressivo
   
ll_i[1] = dw_tes_analisi_reclami_lista.getrow()

if isnull(dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_2")) or &
   dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_2") = 0 then
      g_mb.messagebox("Schede Intervento","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione interventi", StopSign!)
      return
end if
ll_num_reg_lista = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_lista_controllo_2") 
ll_prog_liste_con_comp = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"prog_lista_controllo_2") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
	
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
	
   wf_crea_liste_con_comp( ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"anno_registrazione")
   ll_num_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_registrazione")
   
   update tes_analisi_reclami
   set    prog_versione_2 = :ll_num_versione,
          prog_edizione_2 = :ll_num_edizione,
          num_lista_controllo_2 = :ll_num_reg_lista,
          prog_lista_controllo_2 = :ll_prog_liste_con_comp
   where  cod_azienda = :s_cs_xx.cod_azienda and
          anno_registrazione = :ll_anno_reg_visita and 
          num_registrazione = :ll_num_reg_visita ;
   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   parent.triggerevent("pc_save")

end if

s_cs_xx.parametri.parametro_s_1 = "2"

window_open_parm(w_det_liste_con_comp_fmea, 0, dw_tes_analisi_reclami_lista)

end event

type cb_azione_preventiva from commandbutton within w_tes_analisi_reclami
integer x = 2354
integer y = 1080
integer width = 137
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A.P."
end type

event clicked;long ll_num_righe, ll_t, ll_anno, ll_num
double ldd_reset[]

s_cs_xx.parametri.parametro_d_1_a = ldd_reset
s_cs_xx.parametri.parametro_d_2_a = ldd_reset

ll_anno = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "anno_az_preventiva")
ll_num = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "num_az_preventiva")

if not isnull(ll_anno) and not isnull(ll_num) and ll_anno > 0 and ll_num > 0 then
	s_cs_xx.parametri.parametro_d_1_a[1] = ll_anno
	s_cs_xx.parametri.parametro_d_2_a[1] = ll_num
else
	s_cs_xx.parametri.parametro_d_1_a[1] = 0
	s_cs_xx.parametri.parametro_d_2_a[1] = 0
end if

window_open(w_seleziona_azione_preventiva,0)

ll_num_righe = upperbound(s_cs_xx.parametri.parametro_d_1_a[])

if ll_num_righe = 1 then
	
	ll_anno = s_cs_xx.parametri.parametro_d_1_a[1]
	ll_num = s_cs_xx.parametri.parametro_d_2_a[1]
	
	if not isnull(ll_anno) and ll_anno > 0 and not isnull(ll_num) and ll_num > 0 then
		dw_tes_analisi_reclami_lista.setitem( dw_tes_analisi_reclami_lista.getrow(), "anno_az_preventiva", ll_anno)
		dw_tes_analisi_reclami_lista.setitem( dw_tes_analisi_reclami_lista.getrow(), "num_az_preventiva", ll_num)
		parent.triggerevent("pc_save")
	end if
	
end if

return 0	
end event

type cb_azioni_correttive from cb_stock_ricerca within w_tes_analisi_reclami
integer x = 2217
integer y = 1820
integer width = 361
integer height = 76
integer taborder = 70
boolean bringtotop = true
string text = "Azioni Corr."
end type

event clicked;call super::clicked;long ll_num_righe, ll_t, ll_anno, ll_num
double ldd_reset[]

s_cs_xx.parametri.parametro_d_1_a = ldd_reset
s_cs_xx.parametri.parametro_d_2_a = ldd_reset

ll_anno = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "anno_az_correttiva")
ll_num = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "num_az_correttiva")

if not isnull(ll_anno) and not isnull(ll_num) and ll_anno > 0 and ll_num > 0 then
	s_cs_xx.parametri.parametro_d_1_a[1] = ll_anno
	s_cs_xx.parametri.parametro_d_2_a[1] = ll_num
else
	s_cs_xx.parametri.parametro_d_1_a[1] = 0
	s_cs_xx.parametri.parametro_d_2_a[1] = 0
end if

window_open(w_seleziona_azione_correttiva,0)

ll_num_righe = upperbound(s_cs_xx.parametri.parametro_d_1_a[])

if ll_num_righe = 1 then
	
	ll_anno = s_cs_xx.parametri.parametro_d_1_a[1]
	ll_num = s_cs_xx.parametri.parametro_d_2_a[1]
	
	if not isnull(ll_anno) and ll_anno > 0 and not isnull(ll_num) and ll_num > 0 then
		dw_tes_analisi_reclami_lista.setitem( dw_tes_analisi_reclami_lista.getrow(), "anno_az_correttiva", ll_anno)
		dw_tes_analisi_reclami_lista.setitem( dw_tes_analisi_reclami_lista.getrow(), "num_az_correttiva", ll_num)
		parent.triggerevent("pc_save")
//		iuo_dw_main = dw_tes_analisi_reclami_det_6
//		dw_tes_analisi_reclami_det_6.change_dw_current()
//		parent.triggerevent("pc_retrieve")
		parent.triggerevent("ue_azioni_correttive")
		
	end if
	
end if

return 0	
end event

type cb_1 from commandbutton within w_tes_analisi_reclami
integer x = 2766
integer y = 20
integer width = 320
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;if not isvalid(w_report_tes_analisi_reclami) then
	
	if dw_tes_analisi_reclami_lista.getrow() < 1 then
		//messagebox("OMNIA","Selezionare una non conformità dalla lista",stopsign!)
		return -1
	end if

	s_cs_xx.parametri.parametro_d_1 = dw_tes_analisi_reclami_lista.getitemnumber(dw_tes_analisi_reclami_lista.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = dw_tes_analisi_reclami_lista.getitemnumber(dw_tes_analisi_reclami_lista.getrow(),"num_registrazione")
	
	window_open(w_report_tes_analisi_reclami,-1)

end if
end event

type cb_ric_stock from cb_stock_ricerca within w_tes_analisi_reclami
integer x = 2309
integer y = 1280
integer width = 361
integer height = 76
integer taborder = 60
boolean bringtotop = true
string text = "Ric.Stock"
end type

event clicked;call super::clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

s_cs_xx.parametri.parametro_s_10 = dw_tes_analisi_reclami_det_1.getitemstring(dw_tes_analisi_reclami_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_tes_analisi_reclami_det_1.getitemstring(dw_tes_analisi_reclami_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_tes_analisi_reclami_det_1.getitemstring(dw_tes_analisi_reclami_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_tes_analisi_reclami_det_1.getitemstring(dw_tes_analisi_reclami_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_tes_analisi_reclami_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"
s_cs_xx.parametri.parametro_s_7 = ""
s_cs_xx.parametri.parametro_s_8 = ""

window_open(w_ricerca_stock, 0)
end event

type cb_doc_1 from cb_documenti_compilati within w_tes_analisi_reclami
integer x = 2272
integer y = 700
integer width = 69
integer height = 76
integer taborder = 50
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;//string ls_ritorno
//
//s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_analisi_reclami_det_5
//s_cs_xx.parametri.parametro_s_1 = "doc_compilato_1"
//s_cs_xx.parametri.parametro_s_2 = "DQ1"
//s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
//
//if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
//   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
//
//   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
////      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
////      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
//   end if      
//else
//   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
//end if
//
//

long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_anno_reg_visita, ll_num_reg_visita, ll_progressivo
   
ll_i[1] = dw_tes_analisi_reclami_lista.getrow()

if isnull(dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_1")) or &
   dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1], "num_lista_controllo_1") = 0 then
      g_mb.messagebox("Schede Intervento","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione interventi", StopSign!)
      return
end if
ll_num_reg_lista = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_lista_controllo_1") 
ll_prog_liste_con_comp = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"prog_lista_controllo_1") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
	
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
	
   wf_crea_liste_con_comp( ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"anno_registrazione")
   ll_num_reg_visita = dw_tes_analisi_reclami_lista.getitemnumber(ll_i[1],"num_registrazione")
   
   update tes_analisi_reclami
   set    prog_versione_1 = :ll_num_versione,
          prog_edizione_1 = :ll_num_edizione,
          num_lista_controllo_1 = :ll_num_reg_lista,
          prog_lista_controllo_1 = :ll_prog_liste_con_comp
   where  cod_azienda = :s_cs_xx.cod_azienda and
          anno_registrazione = :ll_anno_reg_visita and 
          num_registrazione = :ll_num_reg_visita ;
   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   parent.triggerevent("pc_save")

end if

s_cs_xx.parametri.parametro_s_1 = "1"

window_open_parm(w_det_liste_con_comp_fmea, 0, dw_tes_analisi_reclami_lista)

end event

type cb_ric_prod from cb_prod_ricerca within w_tes_analisi_reclami
integer x = 2702
integer y = 964
integer width = 73
integer height = 76
integer taborder = 90
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_tes_analisi_reclami_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type dw_tes_analisi_reclami_lista from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 23
integer y = 20
integer width = 2720
integer height = 340
integer taborder = 130
string dataobject = "d_tes_analisi_reclami_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;cb_ric_prod.enabled=true
cb_ric_stock.enabled = true
dw_tes_analisi_reclami_det_1.object.b_ricerca_cliente.enabled=true

setitem( getrow(), "data_creazione", today())
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_anno_registrazione, ll_num_registrazione

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
	if isnull(GetItemnumber(l_Idx, "anno_registrazione")) or GetItemnumber(l_Idx, "anno_registrazione") = 0 then
		SetItem(l_Idx, "anno_registrazione", f_anno_esercizio())
	end if	

	if isnull(GetItemnumber(l_Idx, "num_registrazione")) or GetItemnumber(l_Idx, "num_registrazione") = 0 then
		
		ll_anno_registrazione = f_anno_esercizio()
		ll_num_registrazione = 0
		
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_analisi_reclami
		where  ( cod_azienda = :s_cs_xx.cod_azienda) and 
		       ( anno_registrazione = :ll_anno_registrazione);
						  
		if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione = ll_num_registrazione + 1
		end if
		setitem(this.getrow(),"num_registrazione", ll_num_registrazione)
	end if	
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled=true
cb_ric_stock.enabled = true
dw_tes_analisi_reclami_det_1.object.b_ricerca_cliente.enabled=true



end event

event updateend;call super::updateend;cb_ric_prod.enabled = false
cb_ric_stock.enabled = false
//cb_ricerca_cliente.enabled = false
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
//	iuo_dw_main = dw_tes_analisi_reclami_det_6
//	dw_tes_analisi_reclami_det_6.change_dw_current()	
	parent.postevent("ue_azioni_correttive")
end if
end event

event pcd_view;call super::pcd_view;dw_tes_analisi_reclami_det_1.object.b_ricerca_cliente.enabled=false
end event

event pcd_delete;call super::pcd_delete;dw_tes_analisi_reclami_det_1.object.b_ricerca_cliente.enabled=false
end event

type dw_tes_analisi_reclami_det_1 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 274
integer y = 500
integer width = 2629
integer height = 1960
integer taborder = 10
string dataobject = "d_tes_analisi_reclami_det_1"
boolean border = false
end type

event updateend;call super::updateend;//cb_nc.enabled = true
//
//cb_cancella_camp.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_analisi_reclami_det_1,"cod_cliente")
end choose
end event

type tab_folder from tab within w_tes_analisi_reclami
integer x = 23
integer y = 380
integer width = 3109
integer height = 2120
integer taborder = 10
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
tabpage_7 tabpage_7
tabpage_8 tabpage_8
tabpage_9 tabpage_9
end type

event selectionchanged;choose case newindex
	case 1
		dw_tes_analisi_reclami_det_1.show()
		dw_tes_analisi_reclami_det_1.change_dw_current()
		cb_ric_prod.show()
		cb_ric_stock.show()
//		cb_ricerca_cliente.show()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()				
		dw_tes_analisi_reclami_det_6.hide()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.hide()
		dw_tes_analisi_reclami_det_9.hide()
		cb_azione_preventiva.hide()
		cb_doc_analisi.hide()
		cb_doc_errore.hide()
		cb_report_ap.hide()
	case 2
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.show()
		dw_tes_analisi_reclami_det_2.change_dw_current()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()				
		dw_tes_analisi_reclami_det_6.hide()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.hide()		
		dw_tes_analisi_reclami_det_9.hide()
		cb_azione_preventiva.hide()
		cb_doc_analisi.hide()
		cb_doc_errore.hide()
		cb_report_ap.hide()
	case 3
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.show()
		dw_tes_analisi_reclami_det_3.change_dw_current()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()				
		dw_tes_analisi_reclami_det_6.hide()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.hide()
		dw_tes_analisi_reclami_det_9.hide()
		cb_azione_preventiva.hide()
		cb_doc_analisi.show()
		cb_doc_errore.hide()
		cb_report_ap.hide()
	case 4
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.show()
		dw_tes_analisi_reclami_det_4.change_dw_current()
		dw_tes_analisi_reclami_det_4.setcolumn("note_problema")
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()		
		dw_tes_analisi_reclami_det_6.hide()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.hide()		
		dw_tes_analisi_reclami_det_9.hide()
		cb_azione_preventiva.hide()
		cb_doc_analisi.hide()
		cb_doc_errore.hide()
		cb_report_ap.hide()
	case 5
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.show()
		dw_tes_analisi_reclami_det_5.change_dw_current()
		cb_doc_1.show()
		cb_doc_2.show()
		cb_doc_3.show()
		cb_doc_4.show()
		cb_doc_5.show()
		dw_tes_analisi_reclami_det_6.hide()
		cb_azioni_correttive.hide()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.hide()		
		dw_tes_analisi_reclami_det_9.hide()
		cb_azione_preventiva.hide()
		cb_doc_analisi.hide()
		cb_doc_errore.show()
		cb_report_ap.hide()
	case 6
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()				
		dw_tes_analisi_reclami_det_6.show()
		dw_tes_analisi_reclami_det_6.change_dw_current()
		cb_azioni_correttive.show()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.hide()		
		dw_tes_analisi_reclami_det_9.hide()
		cb_azione_preventiva.hide()
		cb_doc_analisi.hide()
		cb_doc_errore.hide()
		cb_report_ap.hide()
	case 7
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()				
		dw_tes_analisi_reclami_det_6.hide()
		cb_azioni_correttive.hide()
		dw_tes_analisi_reclami_det_7.show()
		dw_tes_analisi_reclami_det_7.change_dw_current()
		dw_tes_analisi_reclami_det_8.hide()		
		dw_tes_analisi_reclami_det_9.hide()		
		cb_azione_preventiva.hide()
		cb_doc_analisi.hide()
		cb_doc_errore.hide()
		cb_report_ap.hide()
		
	case 8
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()				
		dw_tes_analisi_reclami_det_6.hide()
		cb_azioni_correttive.hide()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.show()
		dw_tes_analisi_reclami_det_8.change_dw_current()
		dw_tes_analisi_reclami_det_9.hide()	
		cb_azione_preventiva.show()
		cb_doc_analisi.hide()
		cb_doc_errore.hide()
		cb_report_ap.show()
	case 9
		dw_tes_analisi_reclami_det_1.hide()
		cb_ric_prod.hide()
		cb_ric_stock.hide()		
//		cb_ricerca_cliente.hide()
		dw_tes_analisi_reclami_det_2.hide()
		dw_tes_analisi_reclami_det_3.hide()
		dw_tes_analisi_reclami_det_4.hide()
		dw_tes_analisi_reclami_det_5.hide()
		cb_doc_1.hide()
		cb_doc_2.hide()
		cb_doc_3.hide()
		cb_doc_4.hide()
		cb_doc_5.hide()				
		dw_tes_analisi_reclami_det_6.hide()
		cb_azioni_correttive.hide()
		dw_tes_analisi_reclami_det_7.hide()
		dw_tes_analisi_reclami_det_8.hide()
		dw_tes_analisi_reclami_det_9.show()
		dw_tes_analisi_reclami_det_9.change_dw_current()
		cb_azione_preventiva.hide()
		cb_doc_analisi.hide()
		cb_doc_errore.hide()
		cb_report_ap.hide()
end choose	
end event

on tab_folder.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.tabpage_6=create tabpage_6
this.tabpage_7=create tabpage_7
this.tabpage_8=create tabpage_8
this.tabpage_9=create tabpage_9
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5,&
this.tabpage_6,&
this.tabpage_7,&
this.tabpage_8,&
this.tabpage_9}
end on

on tab_folder.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
destroy(this.tabpage_6)
destroy(this.tabpage_7)
destroy(this.tabpage_8)
destroy(this.tabpage_9)
end on

type tabpage_1 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "        0D        "
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

type tabpage_2 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "1D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_3 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "2D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_4 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "3D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_5 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "4D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_6 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "5D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_7 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "6D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_8 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "7D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_9 from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3072
integer height = 1996
long backcolor = 12632256
string text = "8D"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type dw_tes_analisi_reclami_det_5 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 137
integer y = 500
integer width = 2926
integer height = 1920
integer taborder = 20
string dataobject = "d_tes_analisi_reclami_det_5"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_prog_lista, ll_lista, ll_edizione, ll_versione

choose case i_colname
		
	case "cod_tipo_causa_1"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_1", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_1", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
	case "cod_tipo_causa_2"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_2", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_2", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
	case "cod_tipo_causa_3"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_3", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_3", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
	case "cod_tipo_causa_4"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_4", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_4", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
	case "cod_tipo_causa_5"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_5", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_5, &
							"cod_causa_5", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if		
		
end choose

end event

event updateend;call super::updateend;//cb_nc.enabled = true
//
//cb_cancella_camp.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

type dw_tes_analisi_reclami_det_9 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 114
integer y = 500
integer width = 2926
integer height = 1920
integer taborder = 30
string dataobject = "d_tes_analisi_reclami_det_9"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_prog_lista, ll_lista, ll_edizione, ll_versione

choose case i_colname
		
	case "num_lista_controlli"
	
		ll_prog_lista = getitemnumber(getrow(),"prog_lista_controlli")
	
		if not isnull(ll_prog_lista) then
			g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
			return 1
		end if	
		
		ll_lista = long(i_coltext)
		
		select max(num_edizione)
		into   :ll_edizione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		select max(num_versione)
		into   :ll_versione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 num_edizione = :ll_edizione and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		setnull(ll_prog_lista)
		
		setitem(getrow(),"prog_edizione",ll_edizione)
		setitem(getrow(),"prog_versione",ll_versione)
		setitem(getrow(),"prog_lista_controlli",ll_prog_lista)
		
	case "cod_tipo_causa"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
end choose

end event

event updateend;call super::updateend;//cb_nc.enabled = true
//
//cb_cancella_camp.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

type dw_tes_analisi_reclami_det_6 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 114
integer y = 520
integer width = 2926
integer height = 1900
integer taborder = 40
string dataobject = "d_tes_analisi_reclami_det_6"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno, ll_num


ll_anno = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "anno_az_correttiva")
ll_num = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "num_az_correttiva")

retrieve( s_cs_xx.cod_azienda, ll_anno, ll_num)



end event

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
////	iuo_dw_main = dw_tes_analisi_reclami_det_7
////	dw_tes_analisi_reclami_det_7.change_dw_current()
//	parent.postevent("pc_retrieve")
//	
//end if
end event

type dw_tes_analisi_reclami_det_7 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 114
integer y = 500
integer width = 2926
integer height = 1920
integer taborder = 40
string dataobject = "d_tes_analisi_reclami_det_7"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno, ll_num


ll_anno = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "anno_az_correttiva")
ll_num = dw_tes_analisi_reclami_lista.getitemnumber( dw_tes_analisi_reclami_lista.getrow(), "num_az_correttiva")

retrieve( s_cs_xx.cod_azienda, ll_anno, ll_num)

iuo_dw_main = dw_tes_analisi_reclami_lista
dw_tes_analisi_reclami_lista.change_dw_current()
end event

type dw_tes_analisi_reclami_det_8 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 114
integer y = 500
integer width = 2926
integer height = 1920
integer taborder = 30
string dataobject = "d_tes_analisi_reclami_det_8"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_prog_lista, ll_lista, ll_edizione, ll_versione

choose case i_colname
		
	case "num_lista_controlli"
	
		ll_prog_lista = getitemnumber(getrow(),"prog_lista_controlli")
	
		if not isnull(ll_prog_lista) then
			g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
			return 1
		end if	
		
		ll_lista = long(i_coltext)
		
		select max(num_edizione)
		into   :ll_edizione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		select max(num_versione)
		into   :ll_versione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 num_edizione = :ll_edizione and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		setnull(ll_prog_lista)
		
		setitem(getrow(),"prog_edizione",ll_edizione)
		setitem(getrow(),"prog_versione",ll_versione)
		setitem(getrow(),"prog_lista_controlli",ll_prog_lista)
		
	case "cod_tipo_causa"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
end choose

end event

event updateend;call super::updateend;//cb_nc.enabled = true
//
//cb_cancella_camp.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

type dw_tes_analisi_reclami_det_4 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 114
integer y = 520
integer width = 2926
integer height = 1900
integer taborder = 30
string dataobject = "d_tes_analisi_reclami_det_4"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_prog_lista, ll_lista, ll_edizione, ll_versione

choose case i_colname
		
	case "num_lista_controlli"
	
		ll_prog_lista = getitemnumber(getrow(),"prog_lista_controlli")
	
		if not isnull(ll_prog_lista) then
			g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
			return 1
		end if	
		
		ll_lista = long(i_coltext)
		
		select max(num_edizione)
		into   :ll_edizione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		select max(num_versione)
		into   :ll_versione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 num_edizione = :ll_edizione and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		setnull(ll_prog_lista)
		
		setitem(getrow(),"prog_edizione",ll_edizione)
		setitem(getrow(),"prog_versione",ll_versione)
		setitem(getrow(),"prog_lista_controlli",ll_prog_lista)
		
	case "cod_tipo_causa"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
end choose

end event

event updateend;call super::updateend;//cb_nc.enabled = true
//
//cb_cancella_camp.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

type dw_tes_analisi_reclami_det_3 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 137
integer y = 500
integer width = 2903
integer height = 1940
integer taborder = 20
string dataobject = "d_tes_analisi_reclami_det_3"
boolean border = false
end type

event updateend;call super::updateend;//cb_nc.enabled = true
//
//cb_cancella_camp.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event itemchanged;call super::itemchanged;long ll_prog_lista, ll_lista, ll_edizione, ll_versione

choose case i_colname
		
	case "num_lista_controlli"
	
		ll_prog_lista = getitemnumber(getrow(),"prog_lista_controlli")
	
		if not isnull(ll_prog_lista) then
			g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
			return 1
		end if	
		
		ll_lista = long(i_coltext)
		
		select max(num_edizione)
		into   :ll_edizione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		select max(num_versione)
		into   :ll_versione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 num_edizione = :ll_edizione and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		setnull(ll_prog_lista)
		
		setitem(getrow(),"prog_edizione",ll_edizione)
		setitem(getrow(),"prog_versione",ll_versione)
		setitem(getrow(),"prog_lista_controlli",ll_prog_lista)
		
	case "cod_tipo_causa"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_causa = '" + i_coltext + "'")
		else
			f_PO_LoadDDDW_DW( dw_tes_analisi_reclami_det_3, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
end choose

end event

type dw_tes_analisi_reclami_det_2 from uo_cs_xx_dw within w_tes_analisi_reclami
integer x = 91
integer y = 500
integer width = 2949
integer height = 1920
integer taborder = 20
string dataobject = "d_tes_analisi_reclami_det_2"
boolean border = false
end type

event updateend;call super::updateend;//cb_nc.enabled = true
//
//cb_cancella_camp.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ric_prod.enabled = true
cb_ric_stock.enabled = true
end event

