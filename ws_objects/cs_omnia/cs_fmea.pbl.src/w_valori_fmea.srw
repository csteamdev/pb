﻿$PBExportHeader$w_valori_fmea.srw
$PBExportComments$Tabella Aree Aziendali
forward
global type w_valori_fmea from w_cs_xx_principale
end type
type dw_tab_valori_fmea_lista from uo_cs_xx_dw within w_valori_fmea
end type
type dw_tab_valori_fmea_det from uo_cs_xx_dw within w_valori_fmea
end type
end forward

global type w_valori_fmea from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2322
integer height = 1484
string title = "Parametri FMEA"
event ue_seleziona_riga ( )
dw_tab_valori_fmea_lista dw_tab_valori_fmea_lista
dw_tab_valori_fmea_det dw_tab_valori_fmea_det
end type
global w_valori_fmea w_valori_fmea

event ue_seleziona_riga();return
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]

dw_tab_valori_fmea_lista.set_dw_key("cod_azienda")
dw_tab_valori_fmea_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_tab_valori_fmea_det.set_dw_options(sqlca, &
                             dw_tab_valori_fmea_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
iuo_dw_main = dw_tab_valori_fmea_lista



end event

on w_valori_fmea.create
int iCurrent
call super::create
this.dw_tab_valori_fmea_lista=create dw_tab_valori_fmea_lista
this.dw_tab_valori_fmea_det=create dw_tab_valori_fmea_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_valori_fmea_lista
this.Control[iCurrent+2]=this.dw_tab_valori_fmea_det
end on

on w_valori_fmea.destroy
call super::destroy
destroy(this.dw_tab_valori_fmea_lista)
destroy(this.dw_tab_valori_fmea_det)
end on

type dw_tab_valori_fmea_lista from uo_cs_xx_dw within w_valori_fmea
integer x = 23
integer y = 20
integer width = 2217
integer height = 580
integer taborder = 10
string dataobject = "d_tab_valori_fmea_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_divisione, ls_old_select, new_select, where_clause, ls_count
long ll_i, ll_errore, ll_index

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tab_valori_fmea_det from uo_cs_xx_dw within w_valori_fmea
integer x = 23
integer y = 608
integer width = 2226
integer height = 740
integer taborder = 20
string dataobject = "d_tab_valori_fmea_det"
borderstyle borderstyle = styleraised!
end type

