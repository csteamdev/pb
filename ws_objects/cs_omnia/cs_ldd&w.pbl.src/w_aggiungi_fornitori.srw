﻿$PBExportHeader$w_aggiungi_fornitori.srw
$PBExportComments$Window aggiungi fornitorri
forward
global type w_aggiungi_fornitori from w_cs_xx_risposta
end type
type dw_aggiungi_fornitore from uo_cs_xx_dw within w_aggiungi_fornitori
end type
type cb_1 from commandbutton within w_aggiungi_fornitori
end type
type rb_totale from radiobutton within w_aggiungi_fornitori
end type
type rb_parziale from radiobutton within w_aggiungi_fornitori
end type
end forward

global type w_aggiungi_fornitori from w_cs_xx_risposta
int Width=2254
int Height=1241
boolean TitleBar=true
string Title="Aggiungi Fornitorri"
dw_aggiungi_fornitore dw_aggiungi_fornitore
cb_1 cb_1
rb_totale rb_totale
rb_parziale rb_parziale
end type
global w_aggiungi_fornitori w_aggiungi_fornitori

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup)

dw_aggiungi_fornitore.set_dw_options(sqlca,pcca.null_object,c_multiselect + c_nonew + c_nomodify + c_nodelete + c_default + c_disablecc + c_disableccinsert,c_default)
end event

on w_aggiungi_fornitori.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_aggiungi_fornitore=create dw_aggiungi_fornitore
this.cb_1=create cb_1
this.rb_totale=create rb_totale
this.rb_parziale=create rb_parziale
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_aggiungi_fornitore
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=rb_totale
this.Control[iCurrent+4]=rb_parziale
end on

on w_aggiungi_fornitori.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_aggiungi_fornitore)
destroy(this.cb_1)
destroy(this.rb_totale)
destroy(this.rb_parziale)
end on

type dw_aggiungi_fornitore from uo_cs_xx_dw within w_aggiungi_fornitori
int X=23
int Y=21
int Width=2172
int Height=1001
string DataObject="d_aggiungi_fornitore"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_1 from commandbutton within w_aggiungi_fornitori
int X=1829
int Y=1041
int Width=366
int Height=81
int TabOrder=2
boolean BringToTop=true
string Text="&Aggiungi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_riga, ll_selected[], ll_riga_sel[]
string ls_cod_destinatario,ls_des_destinatario,ls_indirizzo

setpointer(hourglass!)

if rb_totale.checked then
  	for ll_riga = 1 to dw_aggiungi_fornitore.rowcount()
	    ls_cod_destinatario=left(dw_aggiungi_fornitore.getitemstring(ll_riga,"cod_fornitore"),10)
		 ls_des_destinatario=left(dw_aggiungi_fornitore.getitemstring(ll_riga,"rag_soc_1"),20)
 		 ls_indirizzo=left(dw_aggiungi_fornitore.getitemstring(ll_riga,"indirizzo"),40)
		   INSERT INTO tab_ind_dest  
         ( cod_azienda,   
           cod_destinatario,   
           des_destinatario,   
           indirizzo,   
           email )  
  			VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_destinatario,   
           :ls_des_destinatario,   
           :ls_indirizzo,   
           'N' )  ;

	next
else
  	dw_aggiungi_fornitore.get_selected_rows(ll_selected[])
	for ll_riga = 1 to upperbound(ll_selected[])
	    ls_cod_destinatario=left(dw_aggiungi_fornitore.getitemstring(ll_selected[ll_riga],"cod_fornitore"),10)
		 ls_des_destinatario=left(dw_aggiungi_fornitore.getitemstring(ll_selected[ll_riga],"rag_soc_1"),20)
 		 ls_indirizzo=left(dw_aggiungi_fornitore.getitemstring(ll_selected[ll_riga],"indirizzo"),40)
		   INSERT INTO tab_ind_dest  
         ( cod_azienda,   
           cod_destinatario,   
           des_destinatario,   
           indirizzo,   
           email )  
  			VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_destinatario,   
           :ls_des_destinatario,   
           :ls_indirizzo,   
           'N' )  ;
  	next
end if


end event

type rb_totale from radiobutton within w_aggiungi_fornitori
int X=732
int Y=1041
int Width=426
int Height=77
boolean BringToTop=true
string Text="Tutti i Record"
boolean Checked=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_parziale from radiobutton within w_aggiungi_fornitori
int X=1212
int Y=1041
int Width=567
int Height=77
boolean BringToTop=true
string Text="Record Selezionati"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

