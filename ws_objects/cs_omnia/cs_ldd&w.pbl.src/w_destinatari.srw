﻿$PBExportHeader$w_destinatari.srw
forward
global type w_destinatari from w_cs_xx_principale
end type
type cb_liste from commandbutton within w_destinatari
end type
type cb_importa from commandbutton within w_destinatari
end type
type dw_destinatari from uo_cs_xx_dw within w_destinatari
end type
end forward

global type w_destinatari from w_cs_xx_principale
integer width = 2761
integer height = 1876
string title = "Elenco Destinatari"
boolean maxbox = false
boolean resizable = false
cb_liste cb_liste
cb_importa cb_importa
dw_destinatari dw_destinatari
end type
global w_destinatari w_destinatari

on w_destinatari.create
int iCurrent
call super::create
this.cb_liste=create cb_liste
this.cb_importa=create cb_importa
this.dw_destinatari=create dw_destinatari
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_liste
this.Control[iCurrent+2]=this.cb_importa
this.Control[iCurrent+3]=this.dw_destinatari
end on

on w_destinatari.destroy
call super::destroy
destroy(this.cb_liste)
destroy(this.cb_importa)
destroy(this.dw_destinatari)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_destinatari

dw_destinatari.change_dw_current()

dw_destinatari.set_dw_options(sqlca, &
										pcca.null_object, &
										c_default, &
										c_default)
end event

type cb_liste from commandbutton within w_destinatari
integer x = 2363
integer y = 1700
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Liste"
end type

event clicked;long 	 ll_row

string ls_cod_dest, ls_des_dest


ll_row = dw_destinatari.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare un destinatario prima di continuare!",exclamation!)
	return -1
end if

ls_cod_dest = dw_destinatari.getitemstring(ll_row,"cod_destinatario")

if isnull(ls_cod_dest) then
	g_mb.messagebox("OMNIA","Selezionare un destinatario prima di continuare!",exclamation!)
	return -1
end if

ls_des_dest = dw_destinatari.getitemstring(ll_row,"des_destinatario")

s_cs_xx.parametri.parametro_s_1 = ls_cod_dest

s_cs_xx.parametri.parametro_s_2 = ls_des_dest

open(w_selezione_liste)
end event

type cb_importa from commandbutton within w_destinatari
integer x = 9
integer y = 1700
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;open(w_leggi_outlook)

parent.postevent("pc_retrieve")
end event

type dw_destinatari from uo_cs_xx_dw within w_destinatari
integer x = 9
integer y = 8
integer width = 2720
integer height = 1680
integer taborder = 10
string dataobject = "d_destinatari"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event pcd_new;call super::pcd_new;cb_liste.enabled = false
cb_importa.enabled = false

setitem(getrow(),"email","S")
end event

event pcd_setkey;call super::pcd_setkey;string ls_codice

long 	 ll_i, ll_codice, ll_count


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemstring(ll_i,"cod_destinatario")) then
	
		select max(cod_destinatario)
		into	 :ls_codice
		from	 tab_ind_dest
		where	 cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo destinatario: " + sqlca.sqlerrtext,stopsign!)
			return -1
		elseif sqlca.sqlcode = 100 then
			ll_codice = 0
		else
			ll_codice = long(ls_codice)
		end if
		
		if isnull(ll_codice) then
			ll_codice = 0
		end if
		
		do
		
			ll_codice ++
		
			ls_codice = string(ll_codice,"0000000000")
			
			ll_count = 0
			
			select count(*)
			into	 :ll_count
			from	 tab_ind_dest
			where	 cod_azienda = :s_cs_xx.cod_azienda and
					 cod_destinatario = :ls_codice;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in controllo esistenza progressivo destinatario: " + sqlca.sqlerrtext,stopsign!)
				return -1
			elseif sqlca.sqlcode = 100 or isnull(ll_count) then
				ll_count = 0
			end if
			
		loop while ll_count <> 0
		
		setitem(ll_i,"cod_destinatario",ls_codice)
		
	end if
	
next
end event

event pcd_modify;call super::pcd_modify;cb_liste.enabled = false
cb_importa.enabled = false
end event

event pcd_view;call super::pcd_view;cb_liste.enabled = true
cb_importa.enabled = true
end event

event updateend;call super::updateend;cb_liste.enabled = true
cb_importa.enabled = true
end event

