﻿$PBExportHeader$w_destinatari_ricerca.srw
$PBExportComments$Finestra destinatari Ricerca
forward
global type w_destinatari_ricerca from w_cs_xx_principale
end type
type sle_1 from singlelineedit within w_destinatari_ricerca
end type
type cb_tutti from commandbutton within w_destinatari_ricerca
end type
type cb_azzera from commandbutton within w_destinatari_ricerca
end type
type cb_1 from commandbutton within w_destinatari_ricerca
end type
type cb_ok from commandbutton within w_destinatari_ricerca
end type
type cb_annulla from uo_cb_close within w_destinatari_ricerca
end type
type dw_destinatari_lista from datawindow within w_destinatari_ricerca
end type
end forward

global type w_destinatari_ricerca from w_cs_xx_principale
integer width = 1838
integer height = 1672
string title = "Ricerca Destinatari"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
sle_1 sle_1
cb_tutti cb_tutti
cb_azzera cb_azzera
cb_1 cb_1
cb_ok cb_ok
cb_annulla cb_annulla
dw_destinatari_lista dw_destinatari_lista
end type
global w_destinatari_ricerca w_destinatari_ricerca

type variables
long   il_row, il_selezione
string is_tipo_ricerca = 'D'
string is_stringa = ''

end variables

forward prototypes
public subroutine wf_pos_ricerca ()
public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca)
public subroutine wf_carica_destinatari ()
end prototypes

public subroutine wf_pos_ricerca ();
end subroutine

public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca);
end subroutine

public subroutine wf_carica_destinatari ();long   ll_riga

string ls_select, ls_destinatario, ls_indirizzo, ls_cod_destinatario

ls_select = &
"select cod_destinatario, " + &
"       des_destinatario, " + &
"		  indirizzo " + &
"from	  tab_ind_dest " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"       email = 'S' and flag_blocco <> 'S' "+&
"order by des_destinatario "

declare destinatari dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open destinatari;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore destinatari: " + sqlca.sqlerrtext)
	return
end if

do while true
	
	fetch destinatari
	into  :ls_cod_destinatario,
	      :ls_destinatario,
			:ls_indirizzo;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore destinatari: " + sqlca.sqlerrtext)
		close destinatari;
		return
	elseif sqlca.sqlcode = 100	then
		close destinatari;
		exit
	end if
	
	ll_riga = dw_destinatari_lista.insertrow(0)
	dw_destinatari_lista.setitem(ll_riga,"codice",ls_cod_destinatario)	
	dw_destinatari_lista.setitem(ll_riga,"descrizione",ls_destinatario)
	dw_destinatari_lista.setitem(ll_riga,"indirizzo",ls_indirizzo)
	dw_destinatari_lista.setitem(ll_riga,"selezione","N")
	
loop

return 
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_destinatari_lista.settransobject(sqlca)

dw_destinatari_lista.setrowfocusindicator(hand!)



end event

on w_destinatari_ricerca.create
int iCurrent
call super::create
this.sle_1=create sle_1
this.cb_tutti=create cb_tutti
this.cb_azzera=create cb_azzera
this.cb_1=create cb_1
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_destinatari_lista=create dw_destinatari_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_1
this.Control[iCurrent+2]=this.cb_tutti
this.Control[iCurrent+3]=this.cb_azzera
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.cb_ok
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.dw_destinatari_lista
end on

on w_destinatari_ricerca.destroy
call super::destroy
destroy(this.sle_1)
destroy(this.cb_tutti)
destroy(this.cb_azzera)
destroy(this.cb_1)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_destinatari_lista)
end on

event open;call super::open;wf_carica_destinatari()
end event

type sle_1 from singlelineedit within w_destinatari_ricerca
integer x = 46
integer y = 20
integer width = 1408
integer height = 104
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

type cb_tutti from commandbutton within w_destinatari_ricerca
integer x = 46
integer y = 1500
integer width = 343
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Tutti"
end type

event clicked;long ll_i


for ll_i = 1 to dw_destinatari_lista.rowcount()	
	dw_destinatari_lista.setitem(ll_i,"selezione","S")	
next

enabled = false

cb_azzera.enabled = true


end event

type cb_azzera from commandbutton within w_destinatari_ricerca
integer x = 411
integer y = 1500
integer width = 343
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Azzera"
end type

event clicked;long ll_i


for ll_i = 1 to dw_destinatari_lista.rowcount()	
	dw_destinatari_lista.setitem(ll_i,"selezione","N")	
next

enabled = false

cb_tutti.enabled = true


end event

type cb_1 from commandbutton within w_destinatari_ricerca
integer x = 1463
integer y = 20
integer width = 183
integer height = 100
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;dw_destinatari_lista.postevent("ue_trova_destinatario")
end event

type cb_ok from commandbutton within w_destinatari_ricerca
integer x = 1440
integer y = 1500
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;long   ll_j, ll_i

string ls_destinatari[]

ll_j = 0
	
for ll_i = 1 to dw_destinatari_lista.rowcount()
		
	if dw_destinatari_lista.getitemstring(ll_i,"selezione") = "S" then
		ll_j ++
		ls_destinatari[ll_j] = dw_destinatari_lista.getitemstring(ll_i,"codice")
	end if
	
next

s_cs_xx.parametri.parametro_s_1_a = ls_destinatari

if UpperBound(s_cs_xx.parametri.parametro_s_1_a) > 0 then

	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		s_cs_xx.parametri.parametro_uo_dw_1.postevent("ue_inserisci_destinatari")
	else
		if not isnull(s_cs_xx.parametri.parametro_dw_1) then
			s_cs_xx.parametri.parametro_dw_1.postevent("ue_inserisci_destinatari")
		end if
	end if
	
end if

close(parent)
end event

type cb_annulla from uo_cb_close within w_destinatari_ricerca
integer x = 1051
integer y = 1500
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type dw_destinatari_lista from datawindow within w_destinatari_ricerca
event ue_key pbm_dwnkey
event ue_trova_destinatario ( )
integer x = 46
integer y = 160
integer width = 1760
integer height = 1320
integer taborder = 70
string dataobject = "d_invio_messaggi_destinatari"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_trova_destinatario();long 	  ll_i, ll_row

string  ls_descrizione, ls_window, ls_des_test, ls_win_test

boolean lb_trovato


ls_descrizione = upper(sle_1.text)

if ls_descrizione = "" then
	
	setnull(ls_descrizione)
	
end if

if isnull(ls_descrizione) then
	
	g_mb.messagebox("Lista Destinatari","Immettere i parametri di ricerca",exclamation!)
	
	return
	
end if

ll_row = getrow()

if isnull(ll_row) or ll_row <= 0 then
	
	ll_row = 1
	if is_stringa <> ls_descrizione then
		is_stringa = ls_descrizione
	end if
	
else
	
	if is_stringa <> ls_descrizione then
		is_stringa = ls_descrizione
		ll_row = 1
	else
		ll_row++
	end if
	
end if

lb_trovato = false

for ll_i = ll_row to rowcount()
	
	ls_des_test = upper(getitemstring(ll_i,"descrizione"))
	
	if pos(ls_des_test,ls_descrizione,1) > 0 then
		
		setrow(ll_i)
		
		scrolltorow(ll_i)
		
		lb_trovato = true
		
		exit
		
	end if
	
next

if not lb_trovato then
	
	g_mb.messagebox("Lista Destinatari","Testo non trovato",information!)
	
end if
end event

event itemchanged;long ll_count


ll_count = dw_destinatari_lista.rowcount()

if dwo.name = "selezione" then
	
	choose case data
			
		case "S"
			
			// Nuovo destinatario selezionato
			
			il_selezione ++
			
			cb_azzera.enabled = true
			
			if il_selezione = ll_count then
				
				cb_tutti.enabled = false
				
			end if
			
		case "N"
			
			// Un destinatario è stato deselezionato
			
			il_selezione --
			
			cb_tutti.enabled = true
			
			if il_selezione = 0 then
				
				cb_azzera.enabled = false
							
			end if				
			
	end choose
	
end if
end event

