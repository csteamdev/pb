﻿$PBExportHeader$w_det_liste_dist_campi_nc.srw
forward
global type w_det_liste_dist_campi_nc from w_cs_xx_principale
end type
type cb_giu from commandbutton within w_det_liste_dist_campi_nc
end type
type cb_su from commandbutton within w_det_liste_dist_campi_nc
end type
type cb_entrambi from commandbutton within w_det_liste_dist_campi_nc
end type
type cb_non_modificabili from commandbutton within w_det_liste_dist_campi_nc
end type
type cb_modificabili from commandbutton within w_det_liste_dist_campi_nc
end type
type cb_nessuno from commandbutton within w_det_liste_dist_campi_nc
end type
type cb_tutti from commandbutton within w_det_liste_dist_campi_nc
end type
type cb_ricarica from commandbutton within w_det_liste_dist_campi_nc
end type
type dw_folder from u_folder within w_det_liste_dist_campi_nc
end type
type dw_list from uo_cs_xx_dw within w_det_liste_dist_campi_nc
end type
type dw_list2 from uo_cs_xx_dw within w_det_liste_dist_campi_nc
end type
end forward

global type w_det_liste_dist_campi_nc from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2217
integer height = 2060
string title = ""
cb_giu cb_giu
cb_su cb_su
cb_entrambi cb_entrambi
cb_non_modificabili cb_non_modificabili
cb_modificabili cb_modificabili
cb_nessuno cb_nessuno
cb_tutti cb_tutti
cb_ricarica cb_ricarica
dw_folder dw_folder
dw_list dw_list
dw_list2 dw_list2
end type
global w_det_liste_dist_campi_nc w_det_liste_dist_campi_nc

type variables
string is_cod_tipo_lista_dist, is_cod_lista_dist, is_cod_destinatario
decimal{0} id_num_sequenza
end variables

forward prototypes
public subroutine wf_inizializza ()
end prototypes

public subroutine wf_inizializza ();string ls_sql, ls_sintassi, ls_errore, ls_column_name
datastore lds_campi, lds_dw_nc
long ll_righe, ll_ordine, ll_ordine_2
integer li_column, ll_ret, li_index, li_index2
string ls_count, ls_count2, ls_col_index, ls_column_name2, ls_flag_modificabile
string ls_etichetta
integer i
boolean lb_trovata = false

//recupero i campi già eventualmente salvati -------------------------------
setnull(ll_ordine)
	
select max(ordine)
into :ll_ordine
from det_liste_dist_campi_nc
where cod_azienda=:s_cs_xx.cod_azienda
	and cod_tipo_lista_dist=:is_cod_tipo_lista_dist
	and cod_lista_dist=:is_cod_lista_dist
	and cod_destinatario=:is_cod_destinatario
	and num_sequenza=:id_num_sequenza
;
if isnull(ll_ordine) then ll_ordine = 0

ls_sql = "select "+&
				 "cod_azienda"+&
				",cod_tipo_lista_dist"+&
				",cod_lista_dist"+&
				",cod_destinatario"+&
				",num_sequenza"+&
				",colonna"+&
				",flag_modificabile"+&
				",etichetta"+&
				",ordine"+&
				" "+&
			"from det_liste_dist_campi_nc "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
				"and cod_tipo_lista_dist='"+is_cod_tipo_lista_dist+"' "+&
				"and cod_lista_dist='"+is_cod_lista_dist+"' "+&
				"and cod_destinatario='"+is_cod_destinatario+"' "+&
				"and num_sequenza="+string(id_num_sequenza)+" "+&
			"order by ordine, etichetta"
				
ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
   RETURN
END IF

lds_campi = create datastore
lds_campi.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	destroy lds_campi
	RETURN
END IF

lds_campi.settransobject(sqlca)
ll_righe = lds_campi.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe dei campi NC:" + sqlca.sqlerrtext)
	destroy lds_campi;
	return 
end if
//campi salvati in lds_campi ----------------------------

//cancellazione dalla tabella perchè le righe devono essere reinserite
if ll_righe > 0 then
	delete from det_liste_dist_campi_nc
	where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_lista_dist=:is_cod_tipo_lista_dist
			and cod_lista_dist=:is_cod_lista_dist
			and cod_destinatario=:is_cod_destinatario
			and num_sequenza=:id_num_sequenza
	;
	if sqlca.sqlcode < 0 then	
		rollback;
		g_mb.messagebox( "OMNIA", "Errore durante la cancellazione dei campi NC:" + sqlca.sqlerrtext)
		destroy lds_campi
		return
	end if
end if

//creo ds per recuperare di nuovo le colonne dalla lista delle NC
lds_dw_nc = create datastore
lds_dw_nc.dataobject = "d_non_conformita_lista"
lds_dw_nc.settransobject(sqlca)
//n° di colonne presenti nella lista NC
ls_count = lds_dw_nc.Describe("DataWindow.Column.Count")

for li_index = 1 to integer(ls_count)
	//colonna presente nella lista NC da inserire
	ls_col_index = "#"+string(li_index)
	ls_column_name = lds_dw_nc.Describe(ls_col_index+".name")
	ls_flag_modificabile = "N"
	ls_etichetta = ls_column_name + "_t"
	
	//per ogni riga del ds (salvato precedentemente)
	for li_index2 = 1 to  ll_righe
		//leggi il valore della colonna
		ls_column_name2 = lds_campi.getitemstring(li_index2, "colonna")
		if ls_column_name2 = ls_column_name then
			//colonna trovata: setta il flag_modificabile
			ls_flag_modificabile = lds_campi.getitemstring(li_index2, "flag_modificabile")
			ll_ordine_2 = lds_campi.getitemnumber(li_index2, "ordine")
			ls_etichetta = lds_campi.getitemstring(li_index2, "etichetta")
			
			lb_trovata = true
			exit
		end if
	next
	
	//inserisci
	if lb_trovata then
		lb_trovata = false
		
		insert into det_liste_dist_campi_nc
		(	cod_azienda,
			cod_tipo_lista_dist,
			cod_lista_dist,
			cod_destinatario,
			num_sequenza,
			colonna,
			flag_modificabile,
			etichetta,
			ordine
		) values (	:s_cs_xx.cod_azienda,
						:is_cod_tipo_lista_dist,
						:is_cod_lista_dist,
						:is_cod_destinatario,
						:id_num_sequenza,
						:ls_column_name,
						:ls_flag_modificabile,
						:ls_etichetta,
						:ll_ordine_2
					);
	else
		//ll_ordine += 1
		
		insert into det_liste_dist_campi_nc
		(	cod_azienda,
			cod_tipo_lista_dist,
			cod_lista_dist,
			cod_destinatario,
			num_sequenza,
			colonna,
			flag_modificabile,
			etichetta,
			ordine
		) values (	:s_cs_xx.cod_azienda,
						:is_cod_tipo_lista_dist,
						:is_cod_lista_dist,
						:is_cod_destinatario,
						:id_num_sequenza,
						:ls_column_name,
						:ls_flag_modificabile,
						:ls_etichetta,
						0//:ll_ordine
					);
	end if
	
	if sqlca.sqlcode <> 0 then
		rollback;
		g_mb.messagebox( "OMNIA", "Errore durante l' inserimento dei campi NC:" + sqlca.sqlerrtext)
		destroy lds_campi
		destroy lds_dw_nc
		return
	end if
next

commit;
dw_list.postevent("pcd_retrieve")
dw_list2.postevent("pcd_retrieve")
end subroutine

on w_det_liste_dist_campi_nc.create
int iCurrent
call super::create
this.cb_giu=create cb_giu
this.cb_su=create cb_su
this.cb_entrambi=create cb_entrambi
this.cb_non_modificabili=create cb_non_modificabili
this.cb_modificabili=create cb_modificabili
this.cb_nessuno=create cb_nessuno
this.cb_tutti=create cb_tutti
this.cb_ricarica=create cb_ricarica
this.dw_folder=create dw_folder
this.dw_list=create dw_list
this.dw_list2=create dw_list2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_giu
this.Control[iCurrent+2]=this.cb_su
this.Control[iCurrent+3]=this.cb_entrambi
this.Control[iCurrent+4]=this.cb_non_modificabili
this.Control[iCurrent+5]=this.cb_modificabili
this.Control[iCurrent+6]=this.cb_nessuno
this.Control[iCurrent+7]=this.cb_tutti
this.Control[iCurrent+8]=this.cb_ricarica
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.dw_list
this.Control[iCurrent+11]=this.dw_list2
end on

on w_det_liste_dist_campi_nc.destroy
call super::destroy
destroy(this.cb_giu)
destroy(this.cb_su)
destroy(this.cb_entrambi)
destroy(this.cb_non_modificabili)
destroy(this.cb_modificabili)
destroy(this.cb_nessuno)
destroy(this.cb_tutti)
destroy(this.cb_ricarica)
destroy(this.dw_folder)
destroy(this.dw_list)
destroy(this.dw_list2)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_list2
l_objects[2] = cb_su
l_objects[3] = cb_giu
dw_folder.fu_AssignTab(2, "Etichette e Ordine", l_Objects[])

l_objects[1] = dw_list
l_objects[2] = cb_tutti
l_objects[3] = cb_nessuno
l_objects[4] = cb_ricarica
l_objects[5] = cb_modificabili
l_objects[6] = cb_non_modificabili
l_objects[7] = cb_entrambi
dw_folder.fu_AssignTab(1, "Campi Modificabili", l_Objects[])

dw_folder.fu_FolderCreate(2,2)
dw_folder.fu_SelectTab(1)


//iuo_dw_main = dw_list
dw_list.change_dw_current()
dw_list.set_dw_options(sqlca, &
										  i_openparm, &
										  c_scrollparent + c_nonew, &
										  c_default)
										  
dw_list2.set_dw_options(sqlca, &
										  i_openparm, &
										  c_scrollparent + c_nonew, &
										  c_default)
										  

end event

type cb_giu from commandbutton within w_det_liste_dist_campi_nc
integer x = 1577
integer y = 240
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Giu"
end type

event clicked;long ll_rows, ll_index, ll_ordine, ll_curr_row, ll_ordine_succ, ll_succ_row

//riga corrente
ll_curr_row = dw_list2.getrow()
//max riga
ll_rows = dw_list2.rowcount()

//se non c'è riga selezionata esci
if ll_curr_row <=0 then return

ll_ordine = dw_list2.getitemnumber(ll_curr_row, "ordine")

//se è l'ordine massimo esci
if ll_ordine = ll_rows or ll_curr_row = ll_rows then return

ll_succ_row = ll_curr_row +1
ll_ordine_succ = dw_list2.getitemnumber(ll_succ_row, "ordine")

dw_list2.setitem(ll_succ_row, "ordine", ll_ordine)
dw_list2.setitem(ll_curr_row, "ordine", ll_ordine_succ)

dw_list2.sort()

dw_list2.setrow(ll_succ_row)
end event

type cb_su from commandbutton within w_det_liste_dist_campi_nc
integer x = 1577
integer y = 140
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Su"
end type

event clicked;long ll_rows, ll_index, ll_ordine, ll_curr_row, ll_ordine_prec, ll_prec_row

//riga corrente
ll_curr_row = dw_list2.getrow()
if ll_curr_row <=0 then return

ll_ordine = dw_list2.getitemnumber(ll_curr_row, "ordine")

if ll_ordine = 1 or ll_curr_row = 1 then return

ll_prec_row = ll_curr_row -1
ll_ordine_prec = dw_list2.getitemnumber(ll_prec_row, "ordine")

dw_list2.setitem(ll_prec_row, "ordine", ll_ordine)
dw_list2.setitem(ll_curr_row, "ordine", ll_ordine_prec)

dw_list2.sort()

dw_list2.setrow(ll_prec_row)
end event

type cb_entrambi from commandbutton within w_det_liste_dist_campi_nc
integer x = 1006
integer y = 240
integer width = 517
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Entrambi (default)"
end type

event clicked;string ls_filter

ls_filter = ""
dw_list.setfilter(ls_filter)
dw_list.filter()
end event

type cb_non_modificabili from commandbutton within w_det_liste_dist_campi_nc
integer x = 526
integer y = 240
integer width = 457
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "non Modificabili"
end type

event clicked;string ls_filter

ls_filter = "flag_modificabile<>'S'"
dw_list.setfilter(ls_filter)
dw_list.filter()
end event

type cb_modificabili from commandbutton within w_det_liste_dist_campi_nc
integer x = 46
integer y = 240
integer width = 457
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Modificabili"
end type

event clicked;string ls_filter

ls_filter = "flag_modificabile='S'"
dw_list.setfilter(ls_filter)
dw_list.filter()

end event

type cb_nessuno from commandbutton within w_det_liste_dist_campi_nc
integer x = 526
integer y = 140
integer width = 457
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nessuno"
end type

event clicked;long ll_rows, ll_index

ll_rows = dw_list.rowcount()
for ll_index = 1 to ll_rows
	dw_list.setitem(ll_index, "flag_modificabile", "N")
next
end event

type cb_tutti from commandbutton within w_det_liste_dist_campi_nc
integer x = 46
integer y = 140
integer width = 457
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;long ll_rows, ll_index

ll_rows = dw_list.rowcount()
for ll_index = 1 to ll_rows
	dw_list.setitem(ll_index, "flag_modificabile", "S")
next
end event

type cb_ricarica from commandbutton within w_det_liste_dist_campi_nc
integer x = 1006
integer y = 140
integer width = 517
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricarica"
end type

event clicked;if g_mb.messagebox("OMNIA","Ricaricare i campi delle NC?", Exclamation!,YesNo!, 2) = 1 then
	dw_list.setfilter("")
	dw_list.filter()
	wf_inizializza()
end if
end event

type dw_folder from u_folder within w_det_liste_dist_campi_nc
integer width = 2171
integer height = 1960
integer taborder = 30
boolean border = false
end type

type dw_list from uo_cs_xx_dw within w_det_liste_dist_campi_nc
integer x = 23
integer y = 360
integer width = 2103
integer height = 1580
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_det_liste_dist_campi_nc"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;is_cod_tipo_lista_dist = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_tipo_lista_dist")
is_cod_lista_dist = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_lista_dist")
is_cod_destinatario = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_destinatario")
id_num_sequenza = i_parentdw.getitemdecimal(i_parentdw.getrow(),"num_sequenza")

parent.title = "Campi NC - " + i_parentdw.getitemstring(i_parentdw.getrow(),"des_fase")

retrieve(s_cs_xx.cod_azienda,is_cod_tipo_lista_dist,is_cod_lista_dist,is_cod_destinatario,id_num_sequenza)
end event

event pcd_new;call super::pcd_new;cb_tutti.enabled = true
cb_nessuno.enabled = true
cb_ricarica.enabled = false

cb_modificabili.enabled = false
cb_non_modificabili.enabled = false
cb_entrambi.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_tutti.enabled = true
cb_nessuno.enabled = true
cb_ricarica.enabled = false

cb_modificabili.enabled = false
cb_non_modificabili.enabled = false
cb_entrambi.enabled = false
end event

event pcd_view;call super::pcd_view;cb_tutti.enabled = false
cb_nessuno.enabled = false
cb_ricarica.enabled = true

cb_modificabili.enabled = true
cb_non_modificabili.enabled = true
cb_entrambi.enabled = true
end event

event pcd_save;call super::pcd_save;cb_tutti.enabled = false
cb_nessuno.enabled = false
cb_ricarica.enabled = true

dw_list2.postevent("pcd_retrieve")
end event

event itemchanged;call super::itemchanged;
choose case i_colname
	case "flag_modificabile"
		if data = "N" then setitem(i_rownbr, "ordine", 0)
end choose
end event

event updatestart;call super::updatestart;long ll_rows, ll_index, ll_ordine, ll_ordine_sel
string ls_flag_modificabile

setnull(ll_ordine)
	
select max(ordine)
into :ll_ordine
from det_liste_dist_campi_nc
where cod_azienda=:s_cs_xx.cod_azienda
	and cod_tipo_lista_dist=:is_cod_tipo_lista_dist
	and cod_lista_dist=:is_cod_lista_dist
	and cod_destinatario=:is_cod_destinatario
	and num_sequenza=:id_num_sequenza
;
if isnull(ll_ordine) then ll_ordine = 0

this.setfilter("")
this.filter()

ll_rows = this.rowcount()

for ll_index = 1 to ll_rows
	
	if (this.getitemstatus(ll_index,0,primary!) = newmodified!) or (getitemstatus(ll_index,0,primary!) = datamodified!) then
		ls_flag_modificabile = this.getitemstring(ll_index, "flag_modificabile")
		if ls_flag_modificabile = "S" then
			ll_ordine_sel = this.getitemnumber(ll_index, "ordine")
			if ll_ordine_sel = 0 or isnull(ll_ordine_sel) then
				//metti il max +1
				ll_ordine += 1
				this.setitem(ll_index, "ordine", ll_ordine)
			end if
		else
			this.setitem(ll_index, "ordine", 0)
		end if
	end if	
next
end event

type dw_list2 from uo_cs_xx_dw within w_det_liste_dist_campi_nc
integer x = 23
integer y = 360
integer width = 2103
integer height = 1580
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_det_liste_dist_campi_nc2"
boolean vscrollbar = true
end type

event itemchanged;call super::itemchanged;//
//choose case i_colname
//	case "flag_modificabile"
//		if data = "N" then setitem(i_rownbr, "ordine", 0)
//end choose
end event

event pcd_modify;call super::pcd_modify;cb_su.enabled = true
cb_giu.enabled = true
end event

event pcd_new;call super::pcd_new;cb_su.enabled = true
cb_giu.enabled = true
end event

event pcd_retrieve;call super::pcd_retrieve;is_cod_tipo_lista_dist = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_tipo_lista_dist")
is_cod_lista_dist = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_lista_dist")
is_cod_destinatario = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_destinatario")
id_num_sequenza = i_parentdw.getitemdecimal(i_parentdw.getrow(),"num_sequenza")

parent.title = "Campi NC - " + i_parentdw.getitemstring(i_parentdw.getrow(),"des_fase")

retrieve(s_cs_xx.cod_azienda,is_cod_tipo_lista_dist,is_cod_lista_dist,is_cod_destinatario,id_num_sequenza)
end event

event pcd_save;call super::pcd_save;cb_su.enabled = false
cb_giu.enabled = false
end event

event pcd_view;call super::pcd_view;cb_su.enabled = false
cb_giu.enabled = false
end event

