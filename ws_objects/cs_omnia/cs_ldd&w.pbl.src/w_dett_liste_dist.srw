﻿$PBExportHeader$w_dett_liste_dist.srw
$PBExportComments$Window Dettaglio Liste Distribuzione
forward
global type w_dett_liste_dist from w_cs_xx_principale
end type
type cb_aggiungi from commandbutton within w_dett_liste_dist
end type
type dw_det_liste_dist_dett from uo_cs_xx_dw within w_dett_liste_dist
end type
type dw_det_liste_dist_lista from uo_cs_xx_dw within w_dett_liste_dist
end type
type dw_folder from u_folder within w_dett_liste_dist
end type
type dw_elenco_destinatari from uo_cs_xx_dw within w_dett_liste_dist
end type
end forward

global type w_dett_liste_dist from w_cs_xx_principale
integer width = 2025
integer height = 1544
string title = "Liste Distribuzione Dettaglio"
cb_aggiungi cb_aggiungi
dw_det_liste_dist_dett dw_det_liste_dist_dett
dw_det_liste_dist_lista dw_det_liste_dist_lista
dw_folder dw_folder
dw_elenco_destinatari dw_elenco_destinatari
end type
global w_dett_liste_dist w_dett_liste_dist

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

lw_oggetti[1] = dw_det_liste_dist_lista
lw_oggetti[2] = dw_det_liste_dist_dett
dw_folder.fu_assigntab(1, "Lista Distribuzione", lw_oggetti[])
lw_oggetti[1] = dw_elenco_destinatari
lw_oggetti[2] = cb_aggiungi
dw_folder.fu_assigntab(2, "Elenco Destinatari", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_det_liste_dist_lista.set_dw_key("cod_azienda")
dw_det_liste_dist_lista.set_dw_key("cod_tipo_lista_dist")
dw_det_liste_dist_lista.set_dw_key("cod_lista_dist")

dw_det_liste_dist_lista.set_dw_options(sqlca,i_openparm,c_multiselect + c_scrollparent,c_default)
dw_det_liste_dist_dett.set_dw_options(sqlca,dw_det_liste_dist_lista,c_sharedata + c_scrollparent,c_default)
dw_elenco_destinatari.set_dw_options(sqlca,c_nulldw,c_multiselect + c_nodelete + c_nonew + c_nomodify + c_disablecc + c_disableccinsert + c_default,c_default)

iuo_dw_main = dw_det_liste_dist_lista
end event

on w_dett_liste_dist.create
int iCurrent
call super::create
this.cb_aggiungi=create cb_aggiungi
this.dw_det_liste_dist_dett=create dw_det_liste_dist_dett
this.dw_det_liste_dist_lista=create dw_det_liste_dist_lista
this.dw_folder=create dw_folder
this.dw_elenco_destinatari=create dw_elenco_destinatari
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_aggiungi
this.Control[iCurrent+2]=this.dw_det_liste_dist_dett
this.Control[iCurrent+3]=this.dw_det_liste_dist_lista
this.Control[iCurrent+4]=this.dw_folder
this.Control[iCurrent+5]=this.dw_elenco_destinatari
end on

on w_dett_liste_dist.destroy
call super::destroy
destroy(this.cb_aggiungi)
destroy(this.dw_det_liste_dist_dett)
destroy(this.dw_det_liste_dist_lista)
destroy(this.dw_folder)
destroy(this.dw_elenco_destinatari)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_liste_dist_dett,"cod_destinatario",sqlca,&
                 "tab_ind_dest","cod_destinatario","des_destinatario","")

end event

type cb_aggiungi from commandbutton within w_dett_liste_dist
integer x = 1513
integer y = 1312
integer width = 366
integer height = 80
integer taborder = 31
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;long ll_riga, ll_selected[],ll_num_sequenza
string ls_cod_tipo_lista_dist,ls_cod_lista_dist,ls_cod_destinatario

setpointer(hourglass!)
ls_cod_tipo_lista_dist = dw_det_liste_dist_lista.i_parentdw.getitemstring(dw_det_liste_dist_lista.i_parentdw.i_selectedrows[1], "cod_tipo_lista_dist")
ls_cod_lista_dist = dw_det_liste_dist_lista.i_parentdw.getitemstring(dw_det_liste_dist_lista.i_parentdw.i_selectedrows[1], "cod_lista_dist")

dw_elenco_destinatari.get_selected_rows(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[])
	ls_cod_destinatario=dw_elenco_destinatari.getitemstring(ll_selected[ll_riga],"cod_destinatario")
	INSERT INTO det_liste_dist  
         ( cod_azienda,   
           cod_tipo_lista_dist,   
           cod_lista_dist,   
           cod_destinatario,   
           num_sequenza,   
           note )  
   VALUES ( :s_cs_xx.cod_azienda,   
            :ls_cod_tipo_lista_dist,   
            :ls_cod_lista_dist,   
            :ls_cod_destinatario,   
            0,   
            null )  ;

next

dw_det_liste_dist_lista.setfocus()
dw_det_liste_dist_lista.triggerevent("pcd_retrieve")
end event

type dw_det_liste_dist_dett from uo_cs_xx_dw within w_dett_liste_dist
integer x = 37
integer y = 740
integer width = 1851
integer height = 560
integer taborder = 40
string dataobject = "d_det_liste_dist_dett"
borderstyle borderstyle = styleraised!
end type

type dw_det_liste_dist_lista from uo_cs_xx_dw within w_dett_liste_dist
event pcd_retrieve pbm_custom60
event pcd_setkey pbm_custom68
integer x = 41
integer y = 108
integer width = 1851
integer height = 620
integer taborder = 10
string dataobject = "d_det_liste_dist_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_tipo_lista_dist,ls_cod_lista_dist

ls_cod_tipo_lista_dist = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_lista_dist")
ls_cod_lista_dist = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lista_dist")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_lista_dist,ls_cod_lista_dist)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx
string ls_cod_tipo_lista_dist,ls_cod_lista_dist

ls_cod_tipo_lista_dist = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_lista_dist")
ls_cod_lista_dist = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lista_dist")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_tipo_lista_dist", ls_cod_tipo_lista_dist)
		SetItem(l_Idx, "cod_lista_dist", ls_cod_lista_dist)
   END IF
NEXT

end event

type dw_folder from u_folder within w_dett_liste_dist
integer x = 14
integer y = 12
integer width = 1966
integer height = 1420
integer taborder = 20
end type

type dw_elenco_destinatari from uo_cs_xx_dw within w_dett_liste_dist
integer x = 46
integer y = 112
integer width = 1897
integer height = 1140
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_elenco_destinatari"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

