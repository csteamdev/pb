﻿$PBExportHeader$w_importa_dest.srw
forward
global type w_importa_dest from w_cs_xx_risposta
end type
type st_elemento from statictext within w_importa_dest
end type
type cb_file from commandbutton within w_importa_dest
end type
type cb_annulla from commandbutton within w_importa_dest
end type
type dw_importa_dest from datawindow within w_importa_dest
end type
type cb_importa from commandbutton within w_importa_dest
end type
end forward

global type w_importa_dest from w_cs_xx_risposta
integer width = 2469
integer height = 832
string title = "Importazione destinatari da file TXT"
st_elemento st_elemento
cb_file cb_file
cb_annulla cb_annulla
dw_importa_dest dw_importa_dest
cb_importa cb_importa
end type
global w_importa_dest w_importa_dest

forward prototypes
public function integer wf_importa ()
end prototypes

public function integer wf_importa ();long 	 ll_file, ll_return, ll_i, ll_count, ll_riga, ll_pos, ll_start, ll_import

string ls_filename, ls_separatore, ls_tipo_lista, ls_cod_lista, ls_riga, ls_cod_dest, ls_des_dest, ls_email, ls_rif


ls_filename = dw_importa_dest.getitemstring(1,"filename")

if isnull(ls_filename) or trim(ls_filename) = "" then
	g_mb.messagebox("OMNIA","Selezionare un file origine prima di continuare",exclamation!)
	return -1
end if

ls_separatore = dw_importa_dest.getitemstring(1,"separatore")

if isnull(ls_separatore) or trim(ls_separatore) = "" then
	g_mb.messagebox("OMNIA","Selezionare il carattere separatore prima di continuare",exclamation!)
	return -1
end if

ls_tipo_lista = dw_importa_dest.getitemstring(1,"cod_tipo_lista")

ls_cod_lista = dw_importa_dest.getitemstring(1,"cod_lista")

if isnull(ls_tipo_lista) or isnull(ls_cod_lista) then
	g_mb.messagebox("OMNIA","Selezionare la lista di distribuzione su cui importare i destinatari prima di continuare",exclamation!)
	return -1
end if

if g_mb.messagebox("OMNIA","Importazione dei destinatari dal file~n" + ls_filename + &
				  "~nSulla lista di distribuzione~n" + ls_tipo_lista + " - " + ls_cod_lista + &
				  "~nSi desidera continuare?",question!,yesno!,2) = 2 then
	return -1
end if

f_scrivi_log("INIZIO IMPORTAZIONE DESTINATARI DA FILE " + ls_filename)

ll_file = fileopen(ls_filename,linemode!)

if ll_file = -1 then
	g_mb.messagebox("OMNIA","Errore in apertura del file:~n" + ls_filename,stopsign!)
	f_scrivi_log("Errore in apertura del file: operazione interrotta")
	return -1
end if

ll_riga = 0

ll_import = 0

do while true
	
	ll_riga ++
	
	ll_return = fileread(ll_file,ls_riga)
	
	if ll_return = -1 then
		g_mb.messagebox("OMNIA","Riga " + string(ll_riga) + " - Errore in lettura da file:~n" + ls_filename,stopsign!)
		f_scrivi_log("Riga " + string(ll_riga) + " - errore in lettura da file: operazione interrotta")
		return -1
	elseif ll_return = -100 then
		exit
	elseif ll_return = 0 then
		continue
	end if
	
	st_elemento.text = ls_riga
	
	ll_start = 1
	
	ll_pos = pos(ls_riga,ls_separatore,ll_start)
	
	if isnull(ll_pos) or ll_pos = 0 then
		f_scrivi_log("Riga " + string(ll_riga) + " - impossibile trovare separatore in lettura descrizione: elemento ignorato")
		continue
	end if
	
	ls_des_dest = trim(mid(ls_riga,1,(ll_pos - 1)))
	
	if ls_des_dest = "" then
		setnull(ls_des_dest)
	end if
	
	if len(ls_des_dest) > 20 then
		ls_des_dest = mid(ls_des_dest,1,20)
	end if
	
	ll_start = ll_pos + 1
	
	ll_pos = pos(ls_riga,ls_separatore,ll_start)
	
	if isnull(ll_pos) or ll_pos = 0 then
		f_scrivi_log("Riga " + string(ll_riga) + " - impossibile trovare separatore in lettura riferimento: elemento ignorato")
		continue
	end if
	
	ls_rif = trim(mid(ls_riga,ll_start,(ll_pos - 1)))
	
	if ls_rif = "" then
		setnull(ls_rif)
	end if
	
	if len(ls_rif) > 20 then
		ls_rif = mid(ls_rif,1,20)
	end if
	
	ll_start = ll_pos + 1
	
	ls_email = trim(right(ls_riga,len(ls_riga) - ll_pos))
	
	if ls_email = "" then
		setnull(ls_email)
	end if
	
	if isnull(ls_email) then
		f_scrivi_log("Riga " + string(ll_riga) + " - indirizzo email nullo: elemento ignorato")
		continue
	end if
	
	if len(ls_email) > 40 then
		f_scrivi_log("Riga " + string(ll_riga) + " - indirizzo email superiore al limite di 40 caratteri: elemento ignorato")
		continue
	end if
	
	select count(*)
	into	 :ll_count
	from	 tab_ind_dest
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 email = 'S' and
			 indirizzo = :ls_email;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Riga " + string(ll_riga) + " - Errore in verifica esistenza indirizzo " + ls_email + ": " + sqlca.sqlerrtext,stopsign!)
		f_scrivi_log("Riga " + string(ll_riga) + " - errore in verifica esistenza indirizzo email: operazione interrotta")
		return -1
	end if
	
	if ll_count > 0 then
		f_scrivi_log("Riga " + string(ll_riga) + " - indirizzo email già esistente in rubrica: elemento ignorato")
		continue
	end if
	
	for ll_i = 1 to 999999999
		
		ls_cod_dest = string(ll_i,"000000000")
		
		select count(*)
		into   :ll_count
		from   tab_ind_dest
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_destinatario = :ls_cod_dest;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in verifica esistenza codice: " + sqlca.sqlerrtext,stopsign!)
			f_scrivi_log("Riga " + string(ll_riga) + " - errore in generazione automatica codice: operazione interrotta")
			return -1
		end if
		
		if ll_count > 0 then
			continue
		end if
		
		insert
		into   tab_ind_dest
				 (cod_azienda,
				 cod_destinatario,
				 des_destinatario,
				 indirizzo,
				 email,
				 riferimento)
		values (:s_cs_xx.cod_azienda,
				 :ls_cod_dest,
				 :ls_des_dest,
				 :ls_email,
				 'S',
				 :ls_rif);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento " + ls_des_dest + ": " + sqlca.sqlerrtext,stopsign!)
			f_scrivi_log("Riga " + string(ll_riga) + " - errore in inserimento destinatario in rubrica: operazione interrotta")
			rollback;
			return -1
		end if
		
		insert
		into	 det_liste_dist
				 (cod_azienda,
				 cod_tipo_lista_dist,
				 cod_lista_dist,
				 cod_destinatario,
				 num_sequenza,
				 note)
		values (:s_cs_xx.cod_azienda,
				 :ls_tipo_lista,
				 :ls_cod_lista,
				 :ls_cod_dest,
				 1,
				 null);
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in collegamento di " + ls_des_dest + " alla lista di distribuzione" + &
						  ls_tipo_lista + " - " + ls_cod_lista + ": " + sqlca.sqlerrtext,stopsign!)
			f_scrivi_log("Riga " + string(ll_riga) + " - errore in collegamento destinatario alla lista di distribuzione: operazione interrotta")
			rollback;
			return -1
		end if
			
		commit;
		
		ll_import ++
		
		exit
		
	next
	
loop

st_elemento.text = ""

ll_return = fileclose(ll_file)

if ll_return = -1 then
	g_mb.messagebox("OMNIA","Errore in chiusura del file:~n" + ls_filename,stopsign!)
	f_scrivi_log("Errore in chiusura del file")
	return -1
end if

g_mb.messagebox("OMNIA","Operazione completata: " + string(ll_import) + " destinatari importati.~nConsultare il file LOG per maggiori informazioni",information!)

f_scrivi_log("FINE IMPORTAZIONE DESTINATARI DA FILE " + ls_filename)

return 0
end function

on w_importa_dest.create
int iCurrent
call super::create
this.st_elemento=create st_elemento
this.cb_file=create cb_file
this.cb_annulla=create cb_annulla
this.dw_importa_dest=create dw_importa_dest
this.cb_importa=create cb_importa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_elemento
this.Control[iCurrent+2]=this.cb_file
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.dw_importa_dest
this.Control[iCurrent+5]=this.cb_importa
end on

on w_importa_dest.destroy
call super::destroy
destroy(this.st_elemento)
destroy(this.cb_file)
destroy(this.cb_annulla)
destroy(this.dw_importa_dest)
destroy(this.cb_importa)
end on

event pc_setwindow;call super::pc_setwindow;dw_importa_dest.insertrow(0)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_importa_dest,"cod_tipo_lista",sqlca,"tab_tipi_liste_dist","cod_tipo_lista_dist","descrizione","")
end event

type st_elemento from statictext within w_importa_dest
integer x = 46
integer y = 620
integer width = 1577
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_file from commandbutton within w_importa_dest
integer x = 2299
integer y = 120
integer width = 69
integer height = 76
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string ls_path, ls_filename


if getfileopenname("Selezione file origine",ls_path,ls_filename,"txt","File di testo (*.txt),*.txt") <> 1 then
	dw_importa_dest.setitem(1,"filename","")
else
	dw_importa_dest.setitem(1,"filename",ls_path)
end if
end event

type cb_annulla from commandbutton within w_importa_dest
integer x = 1646
integer y = 620
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type dw_importa_dest from datawindow within w_importa_dest
integer x = 23
integer y = 20
integer width = 2400
integer height = 600
integer taborder = 10
string dataobject = "d_importa_dest"
boolean border = false
boolean livescroll = true
end type

event itemchanged;if dwo.name = "cod_tipo_lista" then
	
	string ls_null
		
	setnull(ls_null)
	
	setitem(1,"cod_lista",ls_null)
	
	if isnull(data) then
		
		f_po_loaddddw_dw(this,"cod_lista",sqlca,"tes_liste_dist","cod_lista_dist","des_lista_dist",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist is null")
		
	else
		
		f_po_loaddddw_dw(this,"cod_lista",sqlca,"tes_liste_dist","cod_lista_dist","des_lista_dist",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + data + "'")
		
	end if
	
end if
end event

type cb_importa from commandbutton within w_importa_dest
integer x = 2034
integer y = 620
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;setpointer(hourglass!)

wf_importa()

setpointer(arrow!)
end event

