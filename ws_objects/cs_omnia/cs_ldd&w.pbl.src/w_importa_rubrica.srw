﻿$PBExportHeader$w_importa_rubrica.srw
forward
global type w_importa_rubrica from w_cs_xx_principale
end type
type cb_file from commandbutton within w_importa_rubrica
end type
type cb_importa from commandbutton within w_importa_rubrica
end type
type cb_chiudi from commandbutton within w_importa_rubrica
end type
type sle_percorso from singlelineedit within w_importa_rubrica
end type
end forward

global type w_importa_rubrica from w_cs_xx_principale
integer width = 1934
integer height = 348
string title = "Importa Rubrica"
cb_file cb_file
cb_importa cb_importa
cb_chiudi cb_chiudi
sle_percorso sle_percorso
end type
global w_importa_rubrica w_importa_rubrica

on w_importa_rubrica.create
int iCurrent
call super::create
this.cb_file=create cb_file
this.cb_importa=create cb_importa
this.cb_chiudi=create cb_chiudi
this.sle_percorso=create sle_percorso
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_file
this.Control[iCurrent+2]=this.cb_importa
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.sle_percorso
end on

on w_importa_rubrica.destroy
call super::destroy
destroy(this.cb_file)
destroy(this.cb_importa)
destroy(this.cb_chiudi)
destroy(this.sle_percorso)
end on

type cb_file from commandbutton within w_importa_rubrica
integer x = 1806
integer y = 20
integer width = 69
integer height = 76
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string ls_path, ls_filename


if getfileopenname("Selezione file origine",ls_path,ls_filename,"txt","File di testo (*.txt),*.txt") <> 1 then
	
else
	sle_percorso.text = ls_path
end if
end event

type cb_importa from commandbutton within w_importa_rubrica
integer x = 1509
integer y = 140
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;long 	ll_file, ll_return, ll_count, ll_riga,ll_num_righe
string ls_filename, ls_des_destinatario, ls_indirizzo, ls_test, ls_count,ls_riga

setpointer(Hourglass!)

ls_filename = sle_percorso.text

if isnull(ls_filename) or trim(ls_filename) = "" then
	g_mb.messagebox("OMNIA","Selezionare un file origine prima di continuare",exclamation!)
	setpointer(arrow!)
	return -1
end if

ll_file = fileopen(ls_filename,linemode!)

if ll_file = -1 then
	g_mb.messagebox("OMNIA","Errore in apertura del file:~n" + ls_filename,stopsign!)
	setpointer(arrow!)
	return -1
end if

ll_riga = 0

do while true
	
	ll_riga ++
	ll_num_righe++
	
	if ll_riga =8 then ll_riga = 1
	
	ll_return = fileread(ll_file,ls_riga)
	
	if ll_return = -1 then
		g_mb.messagebox("OMNIA","Riga " + string(ll_riga) + " - Errore in lettura da file:~n" + ls_filename,stopsign!)
		setpointer(arrow!)
		return -1
	elseif ll_return = -100 then
		exit
	end if
	
	if ll_riga = 1 then ls_des_destinatario = ls_riga
	
	if ll_riga = 3 then ls_indirizzo = ls_riga
	
	ls_des_destinatario = left(ls_des_destinatario,20)
	ls_indirizzo = trim(ls_indirizzo)
	ls_indirizzo=left(ls_indirizzo,40)
		
	if ll_riga = 7 then
		select cod_azienda
		into   :ls_test
		from tab_ind_dest
		where cod_azienda = :s_cs_xx.cod_azienda
		and   des_destinatario=:ls_des_destinatario;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			setpointer(arrow!)
			exit
		end if
		
		if sqlca.sqlcode = 100 then
	
			ll_count ++			
			ls_count = string(ll_count)

			do while 1=1
				select cod_azienda
				into   :ls_test
				from   tab_ind_dest
				where  cod_azienda =:s_cs_xx.cod_azienda
				and    cod_destinatario = :ls_count;
				
				if sqlca.sqlcode = 100 then exit

				ll_count ++			
				ls_count = string(ll_count)
				
			loop
			
			insert into tab_ind_dest
			(cod_azienda,
			 cod_destinatario,
			 des_destinatario,
			 indirizzo,
			 email,
			 riferimento)
			values
			(:s_cs_xx.cod_azienda,
			 :ls_count,
			 :ls_des_destinatario,
			 :ls_indirizzo,
			 'S',
			 null);
	
			 if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				setpointer(arrow!)
				exit
			end if
			
		else
			update tab_ind_dest
			set indirizzo =:ls_indirizzo
			where	cod_azienda =:s_cs_xx.cod_azienda
			and   des_destinatario=:ls_des_destinatario;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				setpointer(arrow!)
				exit
			end if
			
		end if
		
	end if
		
loop

ll_return = fileclose(ll_file)

if ll_return = -1 then
	g_mb.messagebox("OMNIA","Errore in chiusura del file:~n" + ls_filename,stopsign!)
	setpointer(arrow!)
	return -1
end if

setpointer(arrow!)
g_mb.messagebox("OMNIA","Operazione completata: ",information!)


return 0
end event

type cb_chiudi from commandbutton within w_importa_rubrica
integer x = 1097
integer y = 140
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type sle_percorso from singlelineedit within w_importa_rubrica
integer x = 23
integer y = 20
integer width = 1760
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
end type

