﻿$PBExportHeader$w_leggi_outlook.srw
forward
global type w_leggi_outlook from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_leggi_outlook
end type
type cb_tutti from commandbutton within w_leggi_outlook
end type
type cb_azzera from commandbutton within w_leggi_outlook
end type
type cb_leggi from commandbutton within w_leggi_outlook
end type
type dw_leggi_outlook from datawindow within w_leggi_outlook
end type
type st_stato from statictext within w_leggi_outlook
end type
end forward

global type w_leggi_outlook from w_cs_xx_risposta
integer width = 2784
integer height = 1488
string title = "Importa indirizzi Outlook"
boolean resizable = false
cb_ok cb_ok
cb_tutti cb_tutti
cb_azzera cb_azzera
cb_leggi cb_leggi
dw_leggi_outlook dw_leggi_outlook
st_stato st_stato
end type
global w_leggi_outlook w_leggi_outlook

type variables
boolean ib_chiudi = false
end variables

forward prototypes
public function integer wf_leggi_outlook ()
end prototypes

public function integer wf_leggi_outlook ();string ls_address, ls_name

long 	 ll_return, ll_i, ll_j, ll_k, ll_riga, ll_count, ll_find, ll_pos

oleobject ole_outlook, ole_namespace, ole_addresslists, ole_addressentries


ole_outlook = create oleobject

st_stato.text = "Connessione ad Outlook..."

ll_return = ole_outlook.connecttonewobject("outlook.application")

if ll_return <> 0 then
	g_mb.messagebox("OMNIA","Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return),stopsign!)
	destroy ole_outlook
	return -1
end if

dw_leggi_outlook.reset()

ole_namespace = ole_outlook.getnamespace("MAPI")

ole_addresslists = ole_namespace.addresslists

for ll_i = 1 to ole_addresslists.count()
	
	ole_addressentries = ole_addresslists.item(ll_i).addressentries
	
	for ll_j = 1 to ole_addressentries.count()
		
		ls_address = ole_addressentries.item(ll_j).address
		
		ll_pos = 0
		
		ll_find = 1
		
		do
			
			ll_find = pos(ls_address,"/cn=",ll_find)
		
			if not isnull(ll_find) and ll_find > 0 then
				ll_pos = ll_find
				ll_find ++
			end if
			
		loop while not isnull(ll_find) and ll_find > 0
		
		if ll_pos > 0 then		
			ls_address = mid(ls_address,ll_pos + 4,len(ls_address) - (ll_pos + 3))
		end if
		
		ls_name = ole_addressentries.item(ll_j).name
		
		st_stato.text = "Rubrica " + string(ll_i) + " - Elemento " + string(ll_j) + " - " + ls_name
		
		ll_count = 0
		
		select count(*)
		into	 :ll_count
		from	 tab_ind_dest
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 email = 'S' and
				 indirizzo = :ls_address;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in controllo esistenza destinatario: " + sqlca.sqlerrtext,stopsign!)
			ole_outlook.disconnectobject()
			destroy ole_outlook
			return -1
		end if
		
		if ll_count > 0 then
			continue
		end if
		
		ll_count = 0
		
		for ll_k = 1 to dw_leggi_outlook.rowcount()			
			if dw_leggi_outlook.getitemstring(ll_k,"email") = ls_address then
				ll_count = 1
				exit
			end if			
		next
		
		if ll_count > 0 then
			continue
		end if
		
		ll_riga = dw_leggi_outlook.insertrow(0)
		
		dw_leggi_outlook.setitem(ll_riga,"des_destinatario",ls_name)
		
		dw_leggi_outlook.setitem(ll_riga,"email",ls_address)
		
		dw_leggi_outlook.setitem(ll_riga,"selezione","N")
		
	next
	
next

ole_outlook.disconnectobject()

destroy ole_outlook

return 0
end function

on w_leggi_outlook.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.cb_tutti=create cb_tutti
this.cb_azzera=create cb_azzera
this.cb_leggi=create cb_leggi
this.dw_leggi_outlook=create dw_leggi_outlook
this.st_stato=create st_stato
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.cb_tutti
this.Control[iCurrent+3]=this.cb_azzera
this.Control[iCurrent+4]=this.cb_leggi
this.Control[iCurrent+5]=this.dw_leggi_outlook
this.Control[iCurrent+6]=this.st_stato
end on

on w_leggi_outlook.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.cb_tutti)
destroy(this.cb_azzera)
destroy(this.cb_leggi)
destroy(this.dw_leggi_outlook)
destroy(this.st_stato)
end on

event closequery;call super::closequery;if not ib_chiudi then
	if g_mb.messagebox("OMNIA","Uscire dall'utilità di importazione?",question!,yesno!,2) = 2 then
		return 1
	end if
end if
end event

event pc_setwindow;call super::pc_setwindow;cb_leggi.postevent("clicked")
end event

type cb_ok from commandbutton within w_leggi_outlook
integer x = 2377
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "OK"
end type

event clicked;string ls_codice, ls_descrizione, ls_email, ls_riferimento

long 	 ll_i, ll_codice, ll_count


setpointer(hourglass!)

for ll_i = 1 to dw_leggi_outlook.rowcount()
	
	if dw_leggi_outlook.getitemstring(ll_i,"selezione") = "N" then
		continue
	end if
	
	ls_descrizione = dw_leggi_outlook.getitemstring(ll_i,"des_destinatario")
	
	ls_email = dw_leggi_outlook.getitemstring(ll_i,"email")
	
	ls_riferimento = dw_leggi_outlook.getitemstring(ll_i,"riferimento")
	
	select max(cod_destinatario)
	into	 :ls_codice
	from	 tab_ind_dest
	where	 cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo destinatario: " + sqlca.sqlerrtext,stopsign!)
		setpointer(arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		ll_codice = 0
	else
		ll_codice = long(ls_codice)
	end if
	
	if isnull(ll_codice) then
		ll_codice = 0
	end if
	
	do
	
		ll_codice ++
	
		ls_codice = string(ll_codice,"0000000000")
		
		ll_count = 0
		
		select count(*)
		into	 :ll_count
		from	 tab_ind_dest
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_destinatario = :ls_codice;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in controllo esistenza progressivo destinatario: " + sqlca.sqlerrtext,stopsign!)
			setpointer(arrow!)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_count) then
			ll_count = 0
		end if
		
	loop while ll_count <> 0
	
	insert
	into	 tab_ind_dest
			 (cod_azienda,
			 cod_destinatario,
			 des_destinatario,
			 indirizzo,
			 email,
			 riferimento)
	values (:s_cs_xx.cod_azienda,
			 :ls_codice,
			 :ls_descrizione,
			 :ls_email,
			 'S',
			 :ls_riferimento);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in inserimento destinatario: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		setpointer(arrow!)
		return -1
	end if
	
	commit;
	
next

setpointer(arrow!)

ib_chiudi = true

close(parent)
end event

type cb_tutti from commandbutton within w_leggi_outlook
integer x = 1989
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Tutti"
end type

event clicked;long ll_i


setpointer(hourglass!)

for ll_i = 1 to dw_leggi_outlook.rowcount()
	dw_leggi_outlook.setitem(ll_i,"selezione","S")
next

setpointer(arrow!)
end event

type cb_azzera from commandbutton within w_leggi_outlook
integer x = 1600
integer y = 20
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Azzera"
end type

event clicked;long ll_i


setpointer(hourglass!)

for ll_i = 1 to dw_leggi_outlook.rowcount()
	dw_leggi_outlook.setitem(ll_i,"selezione","N")
next

setpointer(arrow!)
end event

type cb_leggi from commandbutton within w_leggi_outlook
integer x = 23
integer y = 20
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Leggi"
end type

event clicked;setpointer(hourglass!)

st_stato.text = ""

enabled = false

cb_azzera.enabled = false

cb_tutti.enabled = false

cb_ok.enabled = false

wf_leggi_outlook()

enabled = true

cb_azzera.enabled = true

cb_tutti.enabled = true

cb_ok.enabled = true

st_stato.text = ""

setpointer(arrow!)
end event

type dw_leggi_outlook from datawindow within w_leggi_outlook
integer x = 23
integer y = 120
integer width = 2720
integer height = 1260
integer taborder = 20
string dataobject = "d_leggi_outlook"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_stato from statictext within w_leggi_outlook
integer x = 411
integer y = 20
integer width = 1166
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

