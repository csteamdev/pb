﻿$PBExportHeader$w_liste_dist_det_doc.srw
$PBExportComments$Window liste dist det doc (per la nuova gestione documenti)
forward
global type w_liste_dist_det_doc from w_cs_xx_risposta
end type
type cb_deseleziona from commandbutton within w_liste_dist_det_doc
end type
type cb_seleziona from commandbutton within w_liste_dist_det_doc
end type
type cb_aggiungi from commandbutton within w_liste_dist_det_doc
end type
type cb_togli from commandbutton within w_liste_dist_det_doc
end type
type cb_chiudi from commandbutton within w_liste_dist_det_doc
end type
type ole_1 from olecontrol within w_liste_dist_det_doc
end type
type dw_liste_dist_doc from uo_cs_xx_dw within w_liste_dist_det_doc
end type
type cb_invia from commandbutton within w_liste_dist_det_doc
end type
type sle_oggetto from singlelineedit within w_liste_dist_det_doc
end type
type st_1 from statictext within w_liste_dist_det_doc
end type
type st_2 from statictext within w_liste_dist_det_doc
end type
type mle_nota from multilineedit within w_liste_dist_det_doc
end type
end forward

global type w_liste_dist_det_doc from w_cs_xx_risposta
integer width = 2843
integer height = 1684
string title = "Lista Distribuzione Documento"
cb_deseleziona cb_deseleziona
cb_seleziona cb_seleziona
cb_aggiungi cb_aggiungi
cb_togli cb_togli
cb_chiudi cb_chiudi
ole_1 ole_1
dw_liste_dist_doc dw_liste_dist_doc
cb_invia cb_invia
sle_oggetto sle_oggetto
st_1 st_1
st_2 st_2
mle_nota mle_nota
end type
global w_liste_dist_det_doc w_liste_dist_det_doc

type prototypes
function long GetTempPathA  (Long nBufferLength, ref String lpBuffer) Library  "kernel32.dll" ALIAS FOR "GetTempPathA;ansi"


end prototypes

type variables
// -- stefanop 16/12/2009: Modifica distribuzione documenti
private:
	long il_anno, il_num, il_prog = 0
	boolean ib_idl = false

end variables

forward prototypes
public subroutine wf_imposta_url ()
public function integer wf_registra_invio (string as_cod_destinatario)
end prototypes

public subroutine wf_imposta_url ();/**
 * Stefano Pulze
 * 16/11/2009
 *
 * Controllo l'esistenza del paramentro multi-aziendale SRV e imposto il testo del messaggio
 * con l'url corretto.
 */
 
string ls_url

select
	stringa
into
	:ls_url
from
	parametri
where
	flag_parametro = 'S' and
	cod_parametro = 'SRV';
		
if sqlca.sqlcode <> 0 or isnull(ls_url) then
	g_mb.messagebox("Lista Distribuzione Documento", "Impossibile recuperare paramentro SRV.", StopSign!)
	close(this)
end if
// ----

il_anno = long(s_cs_xx.parametri.parametro_s_10)
il_num = long(s_cs_xx.parametri.parametro_s_11)
il_prog = long(s_cs_xx.parametri.parametro_s_12)

ls_url   = "http://" + ls_url + "/omniadocs/?idl=1"
ls_url += "&anno=" + s_cs_xx.parametri.parametro_s_10
ls_url += "&num=" + s_cs_xx.parametri.parametro_s_11
ls_url += "&prog=" + s_cs_xx.parametri.parametro_s_12
	
mle_nota.text = ls_url
	
end subroutine

public function integer wf_registra_invio (string as_cod_destinatario);/**
 * Registra l'invio della mail all'utente
 * 28/01/2010
 *
 * @param strn as_cod_utente
 **/

long ll_prog_reg
datetime ldt_date, ldt_time

ldt_date = datetime(today(), 00:00:00)
ldt_time = datetime(date("1900/1/1"), now())

// -- progressivo
select max(progressivo_reg)
into	:ll_prog_reg
from	det_documenti_invio
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno and
	num_registrazione = :il_num and
	progressivo_doc = :il_prog;
	
if sqlca.sqlcode = 100 or isnull(ll_prog_reg) then 
	ll_prog_reg = 0
elseif sqlca.sqlcode < 0 then
	g_mb.messagebox("Lista Distribuzione Documento", "Errore calcolo progressivo registrazione.~r~n" + sqlca.sqlerrtext)
	return -1
end if

ll_prog_reg++

insert into det_documenti_invio (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	progressivo_doc,
	progressivo_reg,
	cod_destinatario,
	data_invio,
	ora_invio)
values (
	:s_cs_xx.cod_azienda,
	:il_anno,
	:il_num,
	:il_prog,
	:ll_prog_reg,
	:as_cod_destinatario,
	:ldt_date,
	:ldt_time);
	
if sqlca.sqlcode = 0 then
	return 0;
else
	g_mb.messagebox("Lista Distribuzione Documento", "Errore registrazione invio.~r~n" + sqlca.sqlerrtext)
	return -1
end if
end function

on w_liste_dist_det_doc.create
int iCurrent
call super::create
this.cb_deseleziona=create cb_deseleziona
this.cb_seleziona=create cb_seleziona
this.cb_aggiungi=create cb_aggiungi
this.cb_togli=create cb_togli
this.cb_chiudi=create cb_chiudi
this.ole_1=create ole_1
this.dw_liste_dist_doc=create dw_liste_dist_doc
this.cb_invia=create cb_invia
this.sle_oggetto=create sle_oggetto
this.st_1=create st_1
this.st_2=create st_2
this.mle_nota=create mle_nota
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_deseleziona
this.Control[iCurrent+2]=this.cb_seleziona
this.Control[iCurrent+3]=this.cb_aggiungi
this.Control[iCurrent+4]=this.cb_togli
this.Control[iCurrent+5]=this.cb_chiudi
this.Control[iCurrent+6]=this.ole_1
this.Control[iCurrent+7]=this.dw_liste_dist_doc
this.Control[iCurrent+8]=this.cb_invia
this.Control[iCurrent+9]=this.sle_oggetto
this.Control[iCurrent+10]=this.st_1
this.Control[iCurrent+11]=this.st_2
this.Control[iCurrent+12]=this.mle_nota
end on

on w_liste_dist_det_doc.destroy
call super::destroy
destroy(this.cb_deseleziona)
destroy(this.cb_seleziona)
destroy(this.cb_aggiungi)
destroy(this.cb_togli)
destroy(this.cb_chiudi)
destroy(this.ole_1)
destroy(this.dw_liste_dist_doc)
destroy(this.cb_invia)
destroy(this.sle_oggetto)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.mle_nota)
end on

event pc_setwindow;call super::pc_setwindow;//dw_liste_dist_doc.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete + c_default,c_default)
/**
 * DA VERIFICARE CON ENRICO
 * il codice originale (sopra) non premette la selezione della colonna!
 **/
dw_liste_dist_doc.settransobject(sqlca)
dw_liste_dist_doc.event pcd_retrieve(0,0)

// -- stefanop 16/12/2009: Modifica distribuzione documenti
string ls_test

select
	flag
into
	:ls_test
from
	parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'IDL';
	
if sqlca.sqlcode <> 0 or isnull(ls_test) or ls_test = 'N' then
	ib_idl = false
else
	ib_idl = true
	wf_imposta_url()
end if
// ----
end event

type cb_deseleziona from commandbutton within w_liste_dist_det_doc
integer x = 1829
integer y = 880
integer width = 503
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Deseleziona tutto"
end type

event clicked;long ll_i

if dw_liste_dist_doc.rowcount() > 0 then
	for ll_i = 0 to dw_liste_dist_doc.rowcount()
		dw_liste_dist_doc.setitem(ll_i, "allowsend", "N")
	next
	
	dw_liste_dist_doc.resetupdate()
end if
end event

type cb_seleziona from commandbutton within w_liste_dist_det_doc
integer x = 2354
integer y = 880
integer width = 439
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona tutto"
end type

event clicked;long ll_i

if dw_liste_dist_doc.rowcount() > 0 then
	for ll_i = 0 to dw_liste_dist_doc.rowcount()
		dw_liste_dist_doc.setitem(ll_i, "allowsend", "S")
	next
	
	dw_liste_dist_doc.resetupdate()
end if
end event

type cb_aggiungi from commandbutton within w_liste_dist_det_doc
integer x = 434
integer y = 880
integer width = 425
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi Dest."
end type

event clicked;string ls_flag_blocco, ls_cod_destinatario

long   ll_i

s_cs_xx.parametri.parametro_uo_dw_1 = dw_liste_dist_doc

window_open(w_destinatari_ricerca, 0)

end event

type cb_togli from commandbutton within w_liste_dist_det_doc
integer x = 23
integer y = 880
integer width = 402
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina Dest."
end type

event clicked;string ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_destinatario, ls_cod_utente

long   ll_riga, ll_num_sequenza, ll_prog_lista_dist, ll_risposta

if dw_liste_dist_doc.rowcount() < 2 then
	
	g_mb.messagebox( "OMNIA", "Attenzione: è necessario almento un destinatario!")
	
	return -1
	
end if

ll_riga = dw_liste_dist_doc.getrow()

if ll_riga > 0 and ll_riga <= dw_liste_dist_doc.rowcount() then
	
	ls_cod_tipo_lista_dist = dw_liste_dist_doc.getitemstring( ll_riga, "cod_tipo_lista_dist")
	
	ls_cod_lista_dist = dw_liste_dist_doc.getitemstring( ll_riga, "cod_lista_dist")
	
	ls_cod_destinatario = dw_liste_dist_doc.getitemstring( ll_riga, "cod_destinatario")
	
	ll_num_sequenza = dw_liste_dist_doc.getitemnumber( ll_riga, "num_sequenza")
	
	ll_prog_lista_dist = dw_liste_dist_doc.getitemnumber( ll_riga, "progr_lista_dist")
	
	ls_cod_utente = dw_liste_dist_doc.getitemstring( ll_riga, "cod_utente")
	
	ll_risposta = g_mb.messagebox("Omnia", "Sei sicuro di voler eliminare il destinatario dalla lista ?",  Exclamation!, YesNo!, 2)

	if ll_risposta = 2 then return -1

	delete from liste_dist_doc
	where       cod_azienda = :s_cs_xx.cod_azienda and
	            cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
					cod_lista_dist = :ls_cod_lista_dist and
					cod_destinatario = :ls_cod_destinatario and
					num_sequenza = :ll_num_sequenza and
					progr_lista_dist = :ll_prog_lista_dist and
					cod_utente = :ls_cod_utente;
					
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione del destinatario: " + sqlca.sqlerrtext)
		
		rollback;
		
		return -1
		
	end if
	
	commit;
	
	parent.postevent("pc_retrieve")
	
end if
end event

type cb_chiudi from commandbutton within w_liste_dist_det_doc
integer x = 2423
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type ole_1 from olecontrol within w_liste_dist_det_doc
integer x = 2423
integer y = 980
integer width = 366
integer height = 360
integer taborder = 80
boolean enabled = false
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
string binarykey = "w_liste_dist_det_doc.win"
omactivation activation = activateondoubleclick!
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
end type

type dw_liste_dist_doc from uo_cs_xx_dw within w_liste_dist_det_doc
event ue_inserisci_destinatari ( )
event ue_controlla_invii ( )
integer x = 23
integer y = 20
integer width = 2766
integer height = 840
integer taborder = 10
string dataobject = "d_liste_dist_doc"
boolean vscrollbar = true
boolean i_active = true
end type

event ue_inserisci_destinatari();string ls_flag_blocco, ls_cod_destinatario

long   ll_i, ll_prova

if UpperBound(s_cs_xx.parametri.parametro_s_1_a) < 1 then return 

for ll_i = 1 to UpperBound(s_cs_xx.parametri.parametro_s_1_a)
	
	ls_cod_destinatario = s_cs_xx.parametri.parametro_s_1_a[ll_i]
	
	select flag_blocco
	into   :ls_flag_blocco
	from   tab_ind_dest
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_destinatario = :ls_cod_destinatario;
	
	if ls_flag_blocco = "S" then continue
	
	select count(*)
	into   :ll_prova
	from   liste_dist_doc
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_destinatario = :ls_cod_destinatario and
			 progr_lista_dist = :s_cs_xx.parametri.parametro_ul_2;
			 
	if ll_prova > 0 then continue
	
	INSERT INTO liste_dist_doc  
  			    ( cod_azienda,   
           		cod_tipo_lista_dist,   
		         cod_lista_dist,   
      	      cod_destinatario,   
          	   num_sequenza,   
	            progr_lista_dist,  
					cod_utente, 
    	         path_doc,   
 	            flag_ca,   
    	         data_comm )  
  VALUES    ( :s_cs_xx.cod_azienda,   
   	        'X',   
      	     'X',   
         	  :ls_cod_destinatario,   
	           1,   
   	        :s_cs_xx.parametri.parametro_ul_2,   
				  :s_cs_xx.cod_utente, 
      	     null,   
         	  'N',   
	           null)  ;
				  
	if sqlca.sqlcode <> 0 then
		
		g_mb.messagebox("OMNIA", "Errore l'inserimento dei destinatari del documento~r~nOperazione Annullata.~r~n"+sqlca.sqlerrtext)
		
		rollback;
		
		return
		
	end if	
	
	
next	

commit; 

parent.postevent("pc_retrieve")




end event

event ue_controlla_invii();string ls_cod_dest
long ll_i, ll_count

if dw_liste_dist_doc.rowcount() > 0 then
	for ll_i = 0 to dw_liste_dist_doc.rowcount()
		ll_count = 0
		ls_cod_dest = dw_liste_dist_doc.getitemstring(ll_i, "cod_destinatario")
		
		select count(progressivo_reg)
		into	:ll_count
		from det_documenti_invio
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno and
			num_registrazione  = :il_num and
			progressivo_doc = :il_prog and
			cod_destinatario = :ls_cod_dest;
			
		if sqlca.sqlcode = 100 or isnull(ll_count) or ll_count < 1 then
			dw_liste_dist_doc.setitem(ll_i, "allowsend", "S")
		elseif sqlca.sqlcode = 0 and ll_count < 1 then
			dw_liste_dist_doc.setitem(ll_i, "allowsend", "S")
		else
			dw_liste_dist_doc.setitem(ll_i, "allowsend", "N")
		end if
	next
	
	dw_liste_dist_doc.resetupdate()
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_ul_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
ELSE
	event post ue_controlla_invii()
END IF
end event

type cb_invia from commandbutton within w_liste_dist_det_doc
integer x = 2423
integer y = 1380
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Invia"
end type

event clicked;string  	ls_destinatari[], ls_path_documento, ls_cod_destinatario, ls_indirizzo, ls_allegati[], &
			ls_nome_documento, ls_dir_temp, ls_messaggio, ls_db, ls_estensione, ls_default, ls_str
integer 	li_risposta, li_filenum
decimal	ld_prog_mimetype
long    	ll_riga, ll_progr_lista_dist, ll_temp, ll_pos, ll_anno_reg_doc, ll_num_reg_doc, ll_prog_doc
boolean 	lb_risposta, lb_error
blob    	lbb_blob_documento

transaction sqlcb
uo_outlook luo_outlook

lb_error = false
if s_cs_xx.num_livello_mail>0 then
	
	// stefanop 16/12/2009: Controllo se IDL = true allora non devo inviare il documento
	// ma solo una mail con il link al documento.
	if ib_idl then
		//ls_allegati[]
	else
		if s_cs_xx.parametri.parametro_s_10 <> "" and not isnull(s_cs_xx.parametri.parametro_s_10) then
			ll_anno_reg_doc = long(s_cs_xx.parametri.parametro_s_10)
		end if
		if s_cs_xx.parametri.parametro_s_11 <> "" and not isnull(s_cs_xx.parametri.parametro_s_11) then
			ll_num_reg_doc = long(s_cs_xx.parametri.parametro_s_11)
		end if
		if s_cs_xx.parametri.parametro_s_12 <> "" and not isnull(s_cs_xx.parametri.parametro_s_12) then
			ll_prog_doc = long(s_cs_xx.parametri.parametro_s_12)
		end if
		
		ll_progr_lista_dist = s_cs_xx.parametri.parametro_ul_2
		
		// 11-07-2002 modifiche Michela: controllo l'enginetype
		ls_db = f_db()
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob blob_documento
			into   :lbb_blob_documento
			from   det_documenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_reg_doc and
				num_registrazione = :ll_num_reg_doc and
				progressivo=:ll_prog_doc
			using  sqlcb;
		
			if sqlcb.sqlcode <0 then
				g_mb.messagebox("Omnia","Errore sul db: " + sqlcb.sqlerrtext,stopsign!)
				destroy sqlcb;
				return 0
			end if		
			
			destroy sqlcb;
			
		else
			
			selectblob blob_documento
			into   :lbb_blob_documento
			from   det_documenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_reg_doc and
				num_registrazione = :ll_num_reg_doc and
				progressivo=:ll_prog_doc;
				
			if sqlca.sqlcode <0 then
				g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				return 0
			end if
			
		end if
			
		select nome_documento,
				 prog_mimetype			 
		into   :ls_nome_documento,
				 :ld_prog_mimetype
		from   det_documenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_reg_doc and
			num_registrazione = :ll_num_reg_doc and
			progressivo=:ll_prog_doc;
		
		if sqlca.sqlcode <0 then
			
			g_mb.messagebox("Omnia","Errore durante la select Nome documento: " + sqlca.sqlerrtext,stopsign!)
			
			return -1
			
		end if
		
		select estensione
		into   :ls_estensione
		from   tab_mimetype
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    prog_mimetype = :ld_prog_mimetype;
		
		if sqlca.sqlcode <> 0 or ls_estensione = "" then
			
			g_mb.messagebox("OMNIA", "Impossibile aggiornare il documento: Estensione non riconosciuta!")
			
			return -1
			
		end if
	
		if isnull(ls_nome_documento) then ls_nome_documento = "cs"
		
		ls_nome_documento = 	ls_nome_documento + "." + ls_estensione
		
		// *** leggo la dir temporanea dove mettere il file
		setnull(ls_dir_temp)
	
		select stringa
		into   :ls_dir_temp
		from   parametri_azienda
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_parametro = 'TDD';
	
		if sqlca.sqlcode = -1 then
			
			g_mb.messagebox("OMNIA","Attenzione: Errore nella select del parametro TDD! Dettaglio: " + sqlca.sqlerrtext)
			
			rollback;
			
			return -1	
				
		end if
	
		if sqlca.sqlcode = 100 then
			
			setnull(ls_dir_temp)
			
			li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "ris", ls_dir_temp)	
			
		end if
	
		if isnull(ls_dir_temp) or ls_dir_temp = "" then
			
			g_mb.messagebox("OMNIA","Attenzione: impostare il parametro aziendale TDD o il parametro del registro ris. ")
			
			return -1		
			
		end if
	
		setnull(ls_str)
		
		ls_str = left(ls_dir_temp, 1)
		
		if ls_str <> "\" then ls_dir_temp = "\" + ls_dir_temp
		
		setnull(ls_str)
		
		ls_str = right(ls_dir_temp, 1)
		
		if ls_str <> "\" then ls_dir_temp = ls_dir_temp + "\"
		
		ls_dir_temp = s_cs_xx.volume + ls_dir_temp	
		
		// *** fine impostazione directory temporanea (ps: come fatto nei documenti!)
		// *** li_risposta = ole_1.SaveAs( ls_dir_temp + ls_nome_documento )
		// *** salvo su file ... che è meglio!!!
		li_FileNum = FileOpen( ls_dir_temp + ls_nome_documento, StreamMode!, Write!, Shared!, Replace!)
		
		if li_FileNum = -1 then
			g_mb.messagebox("OMNIA","Errore nell'apertura del file: " + ls_dir_temp + ls_nome_documento)
			return -1
		end if	
		
		ll_pos = 1
		
		
		Do While FileWrite( li_FileNum, BlobMid( lbb_blob_documento, ll_pos, 32765) ) > 0
			ll_pos += 32765
		Loop
		
		FileClose(li_FileNum)
		ls_allegati[1] = ls_dir_temp + ls_nome_documento
	end if
	
	// Invio Mail
	for ll_riga = 1 to dw_liste_dist_doc.rowcount()
		
		// Invio solo se la colonna allowsend è true
		if "N" = dw_liste_dist_doc.getitemstring( ll_riga, "allowsend") then
			continue
		end if
		
		ls_cod_destinatario = dw_liste_dist_doc.getitemstring( ll_riga, "cod_destinatario")

		select indirizzo
		into   :ls_indirizzo
		from   tab_ind_dest
		where  cod_azienda = :s_cs_xx.cod_azienda
		and	 cod_destinatario = :ls_cod_destinatario
		and    email='S';
		
		if isnull(ls_indirizzo) then ls_indirizzo=""

		ls_destinatari[1] = ls_indirizzo
		
		luo_outlook = create uo_outlook		
		
		if luo_outlook.uof_outlook(0, "A", sle_oggetto.text, mle_nota.text, ls_destinatari, ls_allegati, false, ls_messaggio) = 0 then

			dw_liste_dist_doc.setitem( ll_riga, "flag_ca", "S")	
			dw_liste_dist_doc.setitem( ll_riga, "data_comm", today())
			if ( -1 = wf_registra_invio(ls_cod_destinatario)) then
				destroy luo_outlook
				lb_error = true
				exit
			end if
			
		else
			
			g_mb.messagebox( "OMNIA", ls_messaggio)
			dw_liste_dist_doc.setitem( ll_riga, "flag_ca", "N")
			destroy luo_outlook
			lb_error = true
			exit
		end if
		
		destroy luo_outlook

		dw_liste_dist_doc.triggerevent("pcd_save")
	
	next
	
	// stefanop 16/12/2009: Se il flag IDL è a no allora devo eliminare il file temporaneo salvato
	// per inviare l'allegato.
	if not ib_idl then
		FileDelete( ls_dir_temp + ls_nome_documento )
	end if
	
	if not lb_error then
		g_mb.messagebox("Omnia","Spedizione ai destinatari avvenuta con successo!",information!)
	end if
else
	
	g_mb.messagebox("Omnia","Per utilizzare la posta elettronica impostare correttamente il parametro mail del registro!",exclamation!)
	
end if
end event

type sle_oggetto from singlelineedit within w_liste_dist_det_doc
integer x = 23
integer y = 1040
integer width = 2331
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
end type

type st_1 from statictext within w_liste_dist_det_doc
integer x = 23
integer y = 980
integer width = 229
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Oggetto"
boolean focusrectangle = false
end type

type st_2 from statictext within w_liste_dist_det_doc
integer x = 23
integer y = 1140
integer width = 229
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Testo"
boolean focusrectangle = false
end type

type mle_nota from multilineedit within w_liste_dist_det_doc
integer x = 23
integer y = 1200
integer width = 2331
integer height = 360
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
02w_liste_dist_det_doc.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
12w_liste_dist_det_doc.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
