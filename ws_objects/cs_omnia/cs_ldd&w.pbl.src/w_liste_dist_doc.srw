﻿$PBExportHeader$w_liste_dist_doc.srw
$PBExportComments$Window liste dist doc
forward
global type w_liste_dist_doc from w_cs_xx_risposta
end type
type dw_liste_dist_doc from uo_cs_xx_dw within w_liste_dist_doc
end type
type cb_invia from commandbutton within w_liste_dist_doc
end type
type sle_oggetto from singlelineedit within w_liste_dist_doc
end type
type st_1 from statictext within w_liste_dist_doc
end type
type st_2 from statictext within w_liste_dist_doc
end type
type mle_nota from multilineedit within w_liste_dist_doc
end type
end forward

global type w_liste_dist_doc from w_cs_xx_risposta
integer width = 2642
integer height = 1616
string title = "Lista Distribuzione Documento"
dw_liste_dist_doc dw_liste_dist_doc
cb_invia cb_invia
sle_oggetto sle_oggetto
st_1 st_1
st_2 st_2
mle_nota mle_nota
end type
global w_liste_dist_doc w_liste_dist_doc

on w_liste_dist_doc.create
int iCurrent
call super::create
this.dw_liste_dist_doc=create dw_liste_dist_doc
this.cb_invia=create cb_invia
this.sle_oggetto=create sle_oggetto
this.st_1=create st_1
this.st_2=create st_2
this.mle_nota=create mle_nota
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_liste_dist_doc
this.Control[iCurrent+2]=this.cb_invia
this.Control[iCurrent+3]=this.sle_oggetto
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.mle_nota
end on

on w_liste_dist_doc.destroy
call super::destroy
destroy(this.dw_liste_dist_doc)
destroy(this.cb_invia)
destroy(this.sle_oggetto)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.mle_nota)
end on

event pc_setwindow;call super::pc_setwindow;dw_liste_dist_doc.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete + c_default,c_default)
end event

type dw_liste_dist_doc from uo_cs_xx_dw within w_liste_dist_doc
integer x = 23
integer y = 20
integer width = 2560
integer height = 1000
integer taborder = 10
string dataobject = "d_liste_dist_doc"
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_ul_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_invia from commandbutton within w_liste_dist_doc
integer x = 2217
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Invia"
end type

event clicked;string ls_destinatari[],ls_path_documento,ls_cod_destinatario,ls_indirizzo,ls_allegati[], ls_messaggio
long ll_riga
boolean lb_risposta

uo_outlook luo_outlook

if s_cs_xx.num_livello_mail>0 then
	ls_allegati[1]=dw_liste_dist_doc.getitemstring(1,"path_doc")

	for ll_riga = 1 to dw_liste_dist_doc.rowcount()
		ls_cod_destinatario=dw_liste_dist_doc.getitemstring(ll_riga,"cod_destinatario")
	
		select indirizzo
   	into   :ls_indirizzo
		from   tab_ind_dest
		where  cod_azienda=:s_cs_xx.cod_azienda
		and	 cod_destinatario=:ls_cod_destinatario
 		and    email='S';

		if isnull(ls_indirizzo) then ls_indirizzo=""

		ls_destinatari[1] = ls_indirizzo
		
		luo_outlook = create uo_outlook

		if luo_outlook.uof_outlook(0,"A",sle_oggetto.text,mle_nota.text,ls_destinatari,ls_allegati,false,ls_messaggio) = 0 then
			dw_liste_dist_doc.setitem(ll_riga,"flag_ca","S")	
			dw_liste_dist_doc.setitem(ll_riga,"data_comm",today())
		else
			g_mb.messagebox("OMNIA",ls_messaggio)
	   	dw_liste_dist_doc.setitem(ll_riga,"flag_ca","N")
		end if

		destroy luo_outlook

//		lb_risposta = s_cs_xx.mail.uf_invia(ls_destinatari[], sle_oggetto.text, mle_nota.text, ls_allegati[], true)
//
//		if lb_risposta= true then
//	   	dw_liste_dist_doc.setitem(ll_riga,"flag_ca","S")	
//			dw_liste_dist_doc.setitem(ll_riga,"data_comm",today())	  	    
//		else
//	   	dw_liste_dist_doc.setitem(ll_riga,"flag_ca","N")
//		end if

		dw_liste_dist_doc.triggerevent("pcd_save")
	
	next
else
	g_mb.messagebox("Omnia","Per utilizzare la posta elettronica impostare correttamente il parametro mail del file INI",exclamation!)
end if
end event

type sle_oggetto from singlelineedit within w_liste_dist_doc
integer x = 297
integer y = 1040
integer width = 1897
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
end type

type st_1 from statictext within w_liste_dist_doc
integer x = 23
integer y = 1040
integer width = 251
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Oggetto :"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_liste_dist_doc
integer x = 23
integer y = 1140
integer width = 251
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Note :"
alignment alignment = right!
boolean focusrectangle = false
end type

type mle_nota from multilineedit within w_liste_dist_doc
integer x = 297
integer y = 1140
integer width = 1897
integer height = 360
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
end type

