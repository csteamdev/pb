﻿$PBExportHeader$w_liste_distribuzione.srw
$PBExportComments$Finestra Liste distribuzione
forward
global type w_liste_distribuzione from w_cs_xx_principale
end type
type st_1 from statictext within w_liste_distribuzione
end type
type tv_1 from treeview within w_liste_distribuzione
end type
type dw_destinatario_ricerca from uo_cs_xx_dw within w_liste_distribuzione
end type
type dw_det_liste_dist_destinatari_det from uo_cs_xx_dw within w_liste_distribuzione
end type
type dw_tes_liste_dist from uo_cs_xx_dw within w_liste_distribuzione
end type
type dw_tab_tipi_liste_dist from uo_cs_xx_dw within w_liste_distribuzione
end type
type dw_folder from u_folder within w_liste_distribuzione
end type
type dw_det_liste_dist from uo_cs_xx_dw within w_liste_distribuzione
end type
type dw_det_liste_dist_new_fasi from uo_cs_xx_dw within w_liste_distribuzione
end type
type ws_record from structure within w_liste_distribuzione
end type
end forward

type ws_record from structure
	string		cod_tipo_lista_dist
	string		cod_lista_dist
	string		flag_gestione_fasi
	string		cod_destinatario
	long		handle_padre
	string		tipo_livello
	integer		livello
	long		num_sequenza
	string		cod_destinatario_mail
end type

global type w_liste_distribuzione from w_cs_xx_principale
integer width = 3483
integer height = 1376
string title = "Gestione Liste di Distribuzione"
event ue_close ( )
event ue_nuovo_tipo_lista ( )
event ue_nuova_lista ( )
event ue_cancella ( )
event ue_modifica ( )
event ue_ricaricamenu ( )
event ue_nuovo_destinatario ( )
event ue_nuova_fase ( )
event ue_nuovo_destinatariofase ( )
st_1 st_1
tv_1 tv_1
dw_destinatario_ricerca dw_destinatario_ricerca
dw_det_liste_dist_destinatari_det dw_det_liste_dist_destinatari_det
dw_tes_liste_dist dw_tes_liste_dist
dw_tab_tipi_liste_dist dw_tab_tipi_liste_dist
dw_folder dw_folder
dw_det_liste_dist dw_det_liste_dist
dw_det_liste_dist_new_fasi dw_det_liste_dist_new_fasi
end type
global w_liste_distribuzione w_liste_distribuzione

type variables
boolean      ib_carica_tutto=FALSE, ib_retrieve=true, ib_fam = false, ib_new = false, ib_delete

long         il_handle, il_anno_registrazione, il_num_registrazione, il_livello_corrente,il_handle_modificato,il_handle_cancellato

string       is_tipo_manut, is_tipo_ordinamento, is_flag_eseguito, is_sql_filtro, is_area

string       is_cod_tipo_lista_dist, is_cod_lista_dist, is_cod_destinatario, is_tipo_retrieve, is_tipo_livello, is_cod_destinatario_mail

long         il_num_sequenza

treeviewitem tvi_campo

string       is_codice_rubrica

string is_flag_BNC
end variables

forward prototypes
public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_tv ()
public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura)
public function integer wf_inserisci_dettagli (long fl_handle, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist, string fs_gestione_fasi)
public function integer wf_inserisci_tipi (long fl_handle)
public function integer wf_inserisci_testate (long fl_handle, string fs_cod_tipo_lista_dist)
public function integer wf_nuovo_tipo_lista ()
public function integer wf_nuova_lista ()
public function integer wf_nuovo_destinatario ()
public function integer wf_nuova_fase ()
public function integer wf_inserisci_destinatari (long fl_handle, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist, string fs_cod_destinatario, long fl_num_sequenza)
public function integer wf_nuovo_destinatario_fase ()
public function integer wf_nuovo_destinatario_fase_drop ()
public function integer wf_nuovo_destinatario_drop ()
end prototypes

event ue_close();
close(this)
end event

event ue_nuovo_tipo_lista();wf_nuovo_tipo_lista()
end event

event ue_nuova_lista();wf_nuova_lista()
end event

event ue_cancella();string       ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_destinatario, ls_cod_destinatario_mail

treeviewitem ltv_item
		
ws_record    lstr_voce

long         ll_count, ll_num_sequenza





if isnull(il_handle) or il_handle < 1 then return
		
tv_1.getitem(il_handle,ltv_item)
		
lstr_voce = ltv_item.data

choose case lstr_voce.tipo_livello
		
	case "L"
		
		ls_cod_tipo_lista_dist = lstr_voce.cod_tipo_lista_dist
		
		select count(*)
		into   :ll_count
		from   tes_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
				 
		if ll_count > 0 and not isnull(ll_count) then
			
			g_mb.messagebox( "OMNIA", "Attenzione, eliminare prima le liste di distribuzione!")
			return
			
		end if
		
		delete from tab_tipi_liste_dist
		where       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
		
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante la cancellazione del tipo di lista: " + sqlca.sqlerrtext)
			
			rollback;
			
			return
			
		end if
		
		commit;
		
		triggerevent("ue_ricaricamenu")
		
		return
		
	case "T"
		
		ls_cod_tipo_lista_dist = lstr_voce.cod_tipo_lista_dist
		
		ls_cod_lista_dist = lstr_voce.cod_lista_dist
		
		select count(*)
		into   :ll_count
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist;
				 
		if ll_count > 0 and not isnull(ll_count) then
			
			g_mb.messagebox( "OMNIA", "Attenzione, eliminare prima le fasi e/o i destinatari!")
			return
			
		end if
		
		delete from tes_liste_dist
		where       cod_azienda = :s_cs_xx.cod_azienda and
		            cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
						cod_lista_dist = :ls_cod_lista_dist;
		
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante la cancellazione della lista: " + sqlca.sqlerrtext)
			
			rollback;
			
			return
			
		end if
		
		commit;
		
		tv_1.deleteitem(il_handle)
		
		return				
		
	case "F"
		
		ls_cod_tipo_lista_dist = lstr_voce.cod_tipo_lista_dist
		
		ls_cod_lista_dist = lstr_voce.cod_lista_dist
		
		ls_cod_destinatario = lstr_voce.cod_destinatario
		
		ll_num_sequenza = lstr_voce.num_sequenza
		
		select count(*)
		into   :ll_count
		from   det_liste_dist_destinatari
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza = :ll_num_sequenza;
				 
		if ll_count > 0 and not isnull(ll_count) then
			
			g_mb.messagebox( "OMNIA", "Attenzione, eliminare prima le fasi e/o i destinatari!")
			return
			
		end if
		
		delete from det_liste_dist
		where       cod_azienda = :s_cs_xx.cod_azienda and
		            cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
						cod_lista_dist = :ls_cod_lista_dist and
						cod_destinatario = :ls_cod_destinatario and
						num_sequenza = :ll_num_sequenza;
		
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante la cancellazione della fase/destinatario: " + sqlca.sqlerrtext)
			
			rollback;
			
			return
			
		end if
		
		commit;
		
		tv_1.deleteitem(il_handle)
		
		return						
		
	case "D"
		
		ls_cod_tipo_lista_dist = lstr_voce.cod_tipo_lista_dist
		
		ls_cod_lista_dist = lstr_voce.cod_lista_dist
		
		ls_cod_destinatario = lstr_voce.cod_destinatario
		
		ls_cod_destinatario_mail = lstr_voce.cod_destinatario_mail
		
		ll_num_sequenza = lstr_voce.num_sequenza
		
		delete from det_liste_dist_destinatari
		where       cod_azienda = :s_cs_xx.cod_azienda and
		            cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
						cod_lista_dist = :ls_cod_lista_dist and
						cod_destinatario = :ls_cod_destinatario and
						cod_destinatario_mail = :ls_cod_destinatario_mail and
						num_sequenza = :ll_num_sequenza;
		
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante la cancellazione del destinatario: " + sqlca.sqlerrtext)
			
			rollback;
			
			return
			
		end if
		
		commit;
		
		tv_1.deleteitem(il_handle)
		
		return								
		
end choose
end event

event ue_modifica();if isnull(il_handle) or il_handle < 1 then return

string       ls_cod_tipo_lista_dist, ls_cod_lista_dist

treeviewitem ltv_item
		
ws_record    lstr_voce

long         ll_count
		
tv_1.getitem(il_handle,ltv_item)
		
lstr_voce = ltv_item.data

choose case lstr_voce.tipo_livello
		
	case "L"

		dw_tab_tipi_liste_dist.postevent("pcd_modify")
		
	case "T"

		dw_tes_liste_dist.postevent("pcd_modify")
		
	case "F"
		
		if lstr_voce.flag_gestione_fasi <> "S" then
		
			dw_det_liste_dist.postevent("pcd_modify")
			
		else
			
			dw_det_liste_dist_new_fasi.postevent("pcd_modify")
			
		end if
		
	case "D"
		
		dw_det_liste_dist_destinatari_det.postevent("pcd_modify")
		
end choose
end event

event ue_ricaricamenu();wf_cancella_treeview()

wf_imposta_tv()
end event

event ue_nuovo_destinatario();wf_nuovo_destinatario()
end event

event ue_nuova_fase();wf_nuova_fase()
end event

event ue_nuovo_destinatariofase();wf_nuovo_destinatario_fase()
end event

public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione);// --------------------------------  wf_carica_registrazione_manutenzione  ---------------------------
//
// Funzione specifica da richiamare dall'esterno per caricare solo una singola registrazione di manutenzione.
//
// ---------------------------------------------------------------------------------------------------

string ls_null

setnull(ls_null)
//dw_filtro.change_dw_current()
//dw_filtro.setitem(dw_filtro.getrow(),"anno_registrazione", fl_anno_registrazione)
//dw_filtro.setitem(dw_filtro.getrow(),"num_registrazione", fl_num_registrazione)
//dw_filtro.setitem(dw_filtro.getrow(),"cod_attrezzatura", ls_null)
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "N")
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_1", "N")
//dw_filtro.accepttext()
//cb_cerca.postevent("clicked")
//dw_filtro.postevent("ue_reset")

end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);

return 0
end function

public subroutine wf_cancella_treeview ();long tvi_hdl = 0

do 
	tvi_hdl = tv_1.finditem(roottreeitem!,0)
	if tvi_hdl <> -1 then
		tv_1.deleteitem(tvi_hdl)
	end if
loop while tvi_hdl <> -1

return
end subroutine

public subroutine wf_imposta_tv ();string    ls_null, ls_cod_attrezzatura, ls_des_attrezzatura

long      ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione

ws_record lstr_record

setnull(ls_null)

tvi_campo.expanded = false

tvi_campo.selected = false

tvi_campo.children = false	

ll_i = 0

setpointer(HourGlass!)

tv_1.setredraw(false)

il_livello_corrente = 0

wf_inserisci_tipi(0)

tv_1.setredraw(true)

setpointer(arrow!)

return
end subroutine

public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento registrazioni di manutenzione / taratura (STD)
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false

string  ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, &
        ls_ordine_registrazione, ls_flag_ordinarie, ls_flag_eseguito, ls_data_registrazione, ls_cod_guasto_filtro, &
		  ls_cod_tipo_manut_filtro, ls_flag_scadenze
		  
long    ll_risposta, ll_i, ll_num_righe, ll_anno_registrazione, ll_num_registrazione, ll_livello, ll_anno_reg_filtro, &
        ll_anno_reg_programma, ll_num_reg_programma
		  
datetime ldt_data_registrazione, ldt_data_inizio, ldt_data_fine

ws_record lstr_record

ll_livello = il_livello_corrente

declare cu_registrazioni dynamic cursor for sqlsa;
ls_sql = "SELECT anno_registrazione, " + &
         "       num_registrazione, " + &
			"       data_registrazione, " + &
			"       cod_tipo_manutenzione, " + &
			"       cod_attrezzatura, " + &
			"       flag_eseguito, " + &
			"       anno_reg_programma, " + &
			"       num_reg_programma " + &			
			"FROM   manutenzioni " + &
			"WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

//----------- Michele 26/04/2004 correzione caricamento registrazioni -------------
if len(is_sql_filtro) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro + ")"
end if
//----------- Fine Modifica -------------

//choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_eseguito")
//	case "S"
//		ls_sql += " and flag_eseguito = 'S' "
//	case "N"
//		ls_sql += " and flag_eseguito = 'N' "
//end choose
//
//ll_anno_reg_filtro = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")
//
//ls_flag_ordinarie = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_ordinarie")
//
//ls_ordine_registrazione = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_ordinamento_registrazioni")
//
//ldt_data_inizio = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_inizio")
//
//ldt_data_fine = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_fine")
//
//ls_cod_tipo_manut_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione")
//
//ls_cod_guasto_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_guasto")

if not isnull(ll_anno_reg_filtro) and ll_anno_reg_filtro > 0 then
	ls_sql += " and anno_registrazione = " + string(ll_anno_reg_filtro) + " "
end if	

if not isnull(ldt_data_inizio) and ldt_data_inizio >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine) and ldt_data_fine >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if

choose case ls_flag_ordinarie
	case "S"
		ls_sql += " and flag_ordinario = 'S' "
	case "N"
		ls_sql += " and flag_ordinario = 'N' "
end choose

if len(ls_cod_tipo_manut_filtro) < 1 then setnull(ls_cod_tipo_manut_filtro)

if not isnull(ls_cod_tipo_manut_filtro) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
end if

if len(ls_cod_guasto_filtro) < 1 then setnull(ls_cod_guasto_filtro)

if not isnull(ls_cod_guasto_filtro) then
	ls_sql += " and cod_guasto = '" + ls_cod_guasto_filtro + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
end if

if isnull(ls_ordine_registrazione) then ls_ordine_registrazione = "D"

choose case ls_ordine_registrazione
	case "C"
		ls_sql += " order by data_registrazione ASC "
	case "D"
		ls_sql += " order by data_registrazione DESC "
	case "A"
		ls_sql += " order by cod_attrezzatura ASC " 
	case "T"
		ls_sql += " order by cod_tipo_manutenzione ASC "
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_registrazioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch cu_registrazioni into :ll_anno_registrazione, 
	                            :ll_num_registrazione, 
										 :ldt_data_registrazione, 
										 :ls_cod_tipo_manutenzione, 
										 :ls_cod_attrezzatura, 
										 :ls_flag_eseguito,
										 :ll_anno_reg_programma,
										 :ll_num_reg_programma;

   if (sqlca.sqlcode = 100) then
		close cu_registrazioni;
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_registrazioni (w_manutenzioni:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
		close cu_registrazioni;
		return -1
	end if

	if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""
	
	lb_dati = true
	
	setnull(ls_des_tipo_manutenzione)
	
	select des_tipo_manutenzione
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;

	if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = "<DESCRIZIONE MANCANTE>"
	
	if isnull(ldt_data_registrazione) then
		ls_data_registrazione = "DATA MANCANTE"
	else
		ls_data_registrazione = string(ldt_data_registrazione,"dd/mm/yyyy")
	end if
	

	lstr_record.livello = 100
	
	lstr_record.tipo_livello = "M"
	
	choose case ls_ordine_registrazione
		case "C"
			ls_str = ls_data_registrazione + " " + ls_cod_tipo_manutenzione + " " + ls_des_tipo_manutenzione
		case "D"
			ls_str = ls_data_registrazione + " " + ls_cod_tipo_manutenzione + " " + ls_des_tipo_manutenzione
		case "A"
			ls_str = ls_cod_attrezzatura + " " + ls_data_registrazione + " " + ls_cod_tipo_manutenzione + " " + ls_des_tipo_manutenzione
		case "T"
			ls_str = ls_cod_tipo_manutenzione + " " + ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " 
	end choose
	
	tvi_campo.data = lstr_record
	
	tvi_campo.label = ls_str
		
	if ls_flag_eseguito = "S" then
		
		if ls_flag_scadenze = 'S' then
		
			tvi_campo.pictureindex = 6
		
			tvi_campo.selectedpictureindex = 6
		
			tvi_campo.overlaypictureindex = 6
			
		else
			
			tvi_campo.pictureindex = 10
		
			tvi_campo.selectedpictureindex = 10
		
			tvi_campo.overlaypictureindex = 10			
			
		end if
		
	else
		
		if ls_flag_scadenze = 'S' then
			
			tvi_campo.pictureindex = 7
		
			tvi_campo.selectedpictureindex = 7
		
			tvi_campo.overlaypictureindex = 7
			
		else
			
			tvi_campo.pictureindex = 11
		
			tvi_campo.selectedpictureindex = 11
		
			tvi_campo.overlaypictureindex = 11
						
		end if
		
	end if	
	
	tvi_campo.children = false
	tvi_campo.selected = false
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
loop

close cu_registrazioni;
return 0
end function

public function integer wf_inserisci_dettagli (long fl_handle, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist, string fs_gestione_fasi);string    ls_cod_destinatario, ls_note, ls_des_fase, ls_flag_tipo_dist_multipla, ls_des_destinatario

long      ll_risposta, ll_i, ll_num_righe, ll_livello, ll_num_sequenza, ll_num_sequenza_prec, ll_count

ws_record lstr_record

ll_livello = il_livello_corrente + 1

DECLARE  cu_det_liste_dist CURSOR FOR  
SELECT   cod_destinatario,   
         num_sequenza,   
         note,   
         des_fase,   
         flag_tipo_dist_multipla,   
         num_sequenza_prec  
FROM     det_liste_dist  
WHERE    ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( cod_tipo_lista_dist = :fs_cod_tipo_lista_dist ) AND  
         ( cod_lista_dist = :fs_cod_lista_dist )
order by num_sequenza;

open cu_det_liste_dist;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_det_liste_dist (w_manutenzioni:wf_inserisci_dettagli)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch cu_det_liste_dist into :ls_cod_destinatario,   
                                :ll_num_sequenza,   
								        :ls_note,   
                                :ls_des_fase,   
                                :ls_flag_tipo_dist_multipla,   
                                :ll_num_sequenza_prec;
										  
   if (sqlca.sqlcode = 100) then exit
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_det_liste_dist (w_manutenzioni:wf_inserisci_dettagli)~r~n" + sqlca.sqlerrtext)
		close cu_det_liste_dist;
		return -1
	end if
	
	if isnull(ls_des_fase) then ls_des_fase = ""

	lstr_record.cod_tipo_lista_dist = fs_cod_tipo_lista_dist
	
	lstr_record.cod_lista_dist = fs_cod_lista_dist	
	
	lstr_record.cod_destinatario = ls_cod_destinatario
	
	lstr_record.num_sequenza = ll_num_sequenza
	
	lstr_record.handle_padre = fl_handle
	
	lstr_record.livello = ll_livello
	
	lstr_record.tipo_livello = "F"
	
	lstr_record.flag_gestione_fasi = fs_gestione_fasi
	
	tvi_campo.data = lstr_record
	
	if fs_gestione_fasi <> "S" then
		
		select des_destinatario
		into   :ls_des_destinatario
		from   tab_ind_dest
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_destinatario = :ls_cod_destinatario;
				 
		if isnull(ls_des_destinatario) then ls_des_destinatario = ""				 
	
		tvi_campo.label = string(ll_num_sequenza) + ", " + ls_des_destinatario
		
		tvi_campo.children = false
		
	else
		
		tvi_campo.label = string(ll_num_sequenza) + ", " + ls_des_fase
		
		select count(*)
		into   :ll_count
		from   det_liste_dist_destinatari
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :fs_cod_tipo_lista_dist and
				 cod_lista_dist = :fs_cod_lista_dist and 
				 num_sequenza = :ll_num_sequenza and
				 cod_destinatario = :ls_cod_destinatario;
		
		
		if ll_count > 0 then
			tvi_campo.children = true
		else
			tvi_campo.children = false
		end if
		
	end if

	tvi_campo.pictureindex = 3
	
	tvi_campo.selectedpictureindex = 3
	
	tvi_campo.overlaypictureindex = 3
	
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
	if fs_gestione_fasi = "S" then
		
		wf_inserisci_destinatari( ll_risposta, fs_cod_tipo_lista_dist, fs_cod_lista_dist, ls_cod_destinatario, ll_num_sequenza)
		
	end if
	
loop

close cu_det_liste_dist;

return 0
end function

public function integer wf_inserisci_tipi (long fl_handle);boolean   lb_dati = false

string    ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_des_lista_dist, ls_null, ls_flag_gestione_fasi

long      ll_risposta, ll_i, ll_livello, ll_count

ws_record lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

DECLARE  cu_lista CURSOR FOR  
SELECT   cod_tipo_lista_dist,   
         descrizione,   
         flag_gestione_fasi  
FROM     tab_tipi_liste_dist 
order by descrizione;

open cu_lista;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_lista (wf_inserisci_tipi)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1 = 1
	
   fetch cu_lista into :ls_cod_tipo_lista_dist,   
                       :ls_des_lista_dist,   
                       :ls_flag_gestione_fasi ;
										  
   if (sqlca.sqlcode = 100) then exit
	
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_lista (wf_inserisci_tipi)~r~n" + sqlca.sqlerrtext)
		close cu_lista;
		return -1
	end if
	
	if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
	
	if isnull(ls_des_lista_dist) then ls_des_lista_dist = ""

	lstr_record.cod_tipo_lista_dist = ls_cod_tipo_lista_dist 
	
	lstr_record.cod_lista_dist = ls_null
	
	lstr_record.cod_destinatario = ls_null
	
	lstr_record.num_sequenza = 0
	
	lstr_record.flag_gestione_fasi = ls_flag_gestione_fasi
	
	lstr_record.tipo_livello = "L"
	
	lstr_record.livello = ll_livello
	
	lstr_record.handle_padre = fl_handle
	
	tvi_campo.data = lstr_record
	
	tvi_campo.label = ls_des_lista_dist
	
	tvi_campo.pictureindex = 1
	
	tvi_campo.selectedpictureindex = 1
	
	tvi_campo.overlaypictureindex = 1
	
	select count(*)
	into   :ll_count
	from   tes_liste_dist
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
			 
	if ll_count > 0 and not isnull(ll_count) then
	
		tvi_campo.children = true
		
	else
	
		tvi_campo.children = false
				
	end if
	
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento nel TREEVIEW")
		close cu_lista;
		return 0
	end if
	
	wf_inserisci_testate( ll_risposta, ls_cod_tipo_lista_dist)			
	
loop

close cu_lista;
return 0
end function

public function integer wf_inserisci_testate (long fl_handle, string fs_cod_tipo_lista_dist);boolean   lb_dati = false

string    ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_des_lista_dist, ls_null, ls_flag_gestione_fasi

long      ll_risposta, ll_i, ll_livello, ll_count

ws_record lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

DECLARE  cu_tes_liste_dist CURSOR FOR  
SELECT   cod_tipo_lista_dist,   
         cod_lista_dist,   
         des_lista_dist  
FROM     tes_liste_dist  
WHERE    cod_azienda = :s_cs_xx.cod_azienda and
         cod_tipo_lista_dist = :fs_cod_tipo_lista_dist
order by des_lista_dist;

open cu_tes_liste_dist;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_tes_liste_dist (w_manutenzioni:wf_inserisci_testate)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1 = 1
	
   fetch cu_tes_liste_dist into :ls_cod_tipo_lista_dist,   
                                :ls_cod_lista_dist,   
                                :ls_des_lista_dist ;
										  
   if (sqlca.sqlcode = 100) then exit
	
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_tes_liste_dist (w_manutenzioni:wf_inserisci_testate)~r~n" + sqlca.sqlerrtext)
		close cu_tes_liste_dist;
		return -1
	end if
	
	select flag_gestione_fasi
	into   :ls_flag_gestione_fasi
	from   tab_tipi_liste_dist
	where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
	
	if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
	
	if isnull(ls_des_lista_dist) then ls_des_lista_dist = ""

	lstr_record.cod_tipo_lista_dist = ls_cod_tipo_lista_dist 
	
	lstr_record.cod_lista_dist = ls_cod_lista_dist
	
	lstr_record.cod_destinatario = ls_null
	
	lstr_record.num_sequenza = 0	
	
	lstr_record.flag_gestione_fasi = ls_flag_gestione_fasi
	
	lstr_record.tipo_livello = "T"
	
	lstr_record.livello = ll_livello
	
	lstr_record.handle_padre = fl_handle
	
	tvi_campo.data = lstr_record
	
	tvi_campo.label = ls_des_lista_dist
	
	tvi_campo.pictureindex = 2
	
	tvi_campo.selectedpictureindex = 2
	
	tvi_campo.overlaypictureindex = 2
	
	select count(*)
	into   :ll_count
	from   det_liste_dist
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
			 cod_lista_dist = :ls_cod_lista_dist;
			 
	if ll_count > 0 and not isnull(ll_count) then
	
		tvi_campo.children = true
		
	else
	
		tvi_campo.children = false
				
	end if
	
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW")
		close cu_tes_liste_dist;
		return 0
	end if
	
	wf_inserisci_dettagli( ll_risposta, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi)				
	
loop

close cu_tes_liste_dist;
return 0
end function

public function integer wf_nuovo_tipo_lista ();long         ll_progressivo, ll_handle, ll_ordinamento

dw_tab_tipi_liste_dist.show()

dw_tes_liste_dist.hide()

dw_det_liste_dist.hide()

dw_det_liste_dist_new_fasi.hide()

dw_det_liste_dist_destinatari_det.hide()

iuo_dw_main = dw_tab_tipi_liste_dist

dw_tab_tipi_liste_dist.triggerevent("pcd_new")

return 0
end function

public function integer wf_nuova_lista ();long         ll_progressivo, ll_handle, ll_ordinamento

dw_tab_tipi_liste_dist.hide()

dw_tes_liste_dist.show()

dw_det_liste_dist.hide()

dw_det_liste_dist_new_fasi.hide()

dw_det_liste_dist_destinatari_det.hide()

iuo_dw_main = dw_tes_liste_dist

dw_tes_liste_dist.triggerevent("pcd_new")

return 0
end function

public function integer wf_nuovo_destinatario ();long         ll_progressivo, ll_handle, ll_ordinamento

dw_tab_tipi_liste_dist.hide()

dw_tes_liste_dist.hide()

dw_det_liste_dist.show()

dw_det_liste_dist_new_fasi.hide()

dw_det_liste_dist_destinatari_det.hide()

iuo_dw_main = dw_tab_tipi_liste_dist

dw_det_liste_dist.triggerevent("pcd_new")

return 0
end function

public function integer wf_nuova_fase ();long         ll_progressivo, ll_handle, ll_ordinamento

dw_det_liste_dist_new_fasi.show()

dw_tes_liste_dist.hide()

dw_tab_tipi_liste_dist.hide()

dw_det_liste_dist.hide()

dw_det_liste_dist_destinatari_det.hide()

iuo_dw_main = dw_det_liste_dist_new_fasi

dw_det_liste_dist_new_fasi.triggerevent("pcd_new")

return 0
end function

public function integer wf_inserisci_destinatari (long fl_handle, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist, string fs_cod_destinatario, long fl_num_sequenza);string    ls_cod_area_aziendale, ls_cod_cat_mer, ls_cod_destinatario, ls_des_destinatario

long      ll_risposta, ll_i, ll_num_righe, ll_livello, ll_num_sequenza, ll_num_sequenza_prec, ll_count

ws_record lstr_record

treeviewitem tv_campo

ll_livello = il_livello_corrente + 1

DECLARE  cu_destinatari CURSOR FOR  
SELECT   cod_destinatario_mail,   
         cod_area_aziendale,   
         cod_cat_mer
FROM     det_liste_dist_destinatari  
WHERE    ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( cod_tipo_lista_dist = :fs_cod_tipo_lista_dist ) AND  
         ( cod_lista_dist = :fs_cod_lista_dist ) AND
			( cod_destinatario = :fs_cod_destinatario ) AND
			( num_sequenza = :fl_num_sequenza ) 
order by cod_destinatario_mail;

open cu_destinatari;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_destinatari (wf_inserisci_destinatari)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch cu_destinatari into :ls_cod_destinatario,   
                             :ls_cod_area_aziendale,   
								     :ls_cod_cat_mer;
										  
   if (sqlca.sqlcode = 100) then exit
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_destinatari (wf_inserisci_destinatari)~r~n" + sqlca.sqlerrtext)
		close cu_destinatari;
		return -1
	end if
	
	select des_destinatario
	into   :ls_des_destinatario 
	from   tab_ind_dest 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_destinatario = :ls_cod_destinatario;
	
	if isnull(ls_des_destinatario) then ls_des_destinatario = ""

	lstr_record.cod_tipo_lista_dist = fs_cod_tipo_lista_dist
	
	lstr_record.cod_lista_dist = fs_cod_lista_dist	
	
	lstr_record.cod_destinatario = fs_cod_destinatario
	
	lstr_record.cod_destinatario_mail = ls_cod_destinatario	
	
	lstr_record.num_sequenza = fl_num_sequenza
	
	lstr_record.handle_padre = fl_handle
	
	lstr_record.livello = ll_livello
	
	lstr_record.tipo_livello = "D"
	
	lstr_record.flag_gestione_fasi = "S"
	
	tv_campo.data = lstr_record
	
	tv_campo.label = ls_des_destinatario
	
	tv_campo.children = false
		
	tv_campo.pictureindex = 4
	
	tv_campo.selectedpictureindex = 4
	
	tv_campo.overlaypictureindex = 4
	
	tv_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tv_campo)
	
loop

close cu_destinatari;

return 0
end function

public function integer wf_nuovo_destinatario_fase ();long         ll_progressivo, ll_handle, ll_ordinamento

dw_tab_tipi_liste_dist.hide()

dw_tes_liste_dist.hide()

dw_det_liste_dist.hide()

dw_det_liste_dist_new_fasi.hide()

dw_det_liste_dist_destinatari_det.show()

iuo_dw_main = dw_det_liste_dist_destinatari_det

dw_det_liste_dist_destinatari_det.triggerevent("pcd_new")

return 0
end function

public function integer wf_nuovo_destinatario_fase_drop ();string         ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_destinatario, ls_cod_destinatario_mail

treeviewitem   ltv_item

ws_record      lstr_voce

long           ll_handle, ll_num_sequenza

tv_1.getitem( il_handle, ltv_item)

lstr_voce = ltv_item.data

ls_cod_tipo_lista_dist = lstr_voce.cod_tipo_lista_dist

ls_cod_lista_dist = lstr_voce.cod_lista_dist

ls_cod_destinatario = lstr_voce.cod_destinatario
	
ll_num_sequenza = lstr_voce.num_sequenza
	
ls_cod_destinatario_mail = is_codice_rubrica

insert into det_liste_dist_destinatari (cod_azienda,
                                        cod_tipo_lista_dist,
													 cod_lista_dist,
													 num_sequenza,
													 cod_destinatario,
													 cod_destinatario_mail)
									values      (:s_cs_xx.cod_azienda,
									             :ls_cod_tipo_lista_dist,
													 :ls_cod_lista_dist,
													 :ll_num_sequenza,
													 :ls_cod_destinatario,
													 :ls_cod_destinatario_mail);
													 
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante inserimento destinatario: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

lstr_voce.cod_destinatario_mail = ls_cod_destinatario_mail

lstr_voce.flag_gestione_fasi = "S"

lstr_voce.tipo_livello = "D"

lstr_voce.livello = ll_handle

lstr_voce.handle_padre = il_handle

ltv_item.label = st_1.text
	
ltv_item.expanded = false
	
ltv_item.selected = false
	
ltv_item.children = false
	
ltv_item.data = lstr_voce
	
ltv_item.pictureindex = 4
	
ltv_item.selectedpictureindex = 4

ltv_item.overlaypictureindex = 4
	
ll_handle = tv_1.insertitemlast( il_handle, ltv_item)
	
tv_1.selectitem(ll_handle)

il_handle = ll_handle
	
return 0


end function

public function integer wf_nuovo_destinatario_drop ();string         ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_destinatario, ls_cod_destinatario_mail

treeviewitem   ltv_item, ltv_nuovo

ws_record      lstr_voce, lstr_nuovo

long           ll_handle, ll_num_sequenza

tv_1.getitem( il_handle, ltv_item)

lstr_voce = ltv_item.data

ls_cod_tipo_lista_dist = lstr_voce.cod_tipo_lista_dist

ls_cod_lista_dist = lstr_voce.cod_lista_dist

ls_cod_destinatario = is_codice_rubrica
	
ll_num_sequenza = 10

insert into det_liste_dist (cod_azienda,
                            cod_tipo_lista_dist,
									 cod_lista_dist,
									 num_sequenza,
									 cod_destinatario)
					values      (:s_cs_xx.cod_azienda,
					             :ls_cod_tipo_lista_dist,
									 :ls_cod_lista_dist,
									 :ll_num_sequenza,
									 :ls_cod_destinatario);
													 
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante inserimento destinatario: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

lstr_nuovo.cod_tipo_lista_dist = ls_cod_tipo_lista_dist
	
lstr_nuovo.cod_lista_dist = ls_cod_lista_dist
	
lstr_nuovo.cod_destinatario = ls_cod_destinatario	
	
lstr_nuovo.flag_gestione_fasi = "N"
	
lstr_nuovo.tipo_livello = "F"
	
lstr_nuovo.num_sequenza = 10
	
lstr_nuovo.livello = 3
	
lstr_nuovo.handle_padre = il_handle	
	
ltv_nuovo.label = st_1.text
	
ltv_nuovo.expanded = false
	
ltv_nuovo.selected = false
	
ltv_nuovo.children = false
	
ltv_nuovo.data = lstr_nuovo
	
ltv_nuovo.pictureindex = 3
	
ltv_nuovo.selectedpictureindex = 3
	
ltv_nuovo.overlaypictureindex = 3
	
ll_handle = tv_1.insertitemlast( il_handle, ltv_nuovo)
	
tv_1.selectitem(ll_handle)
	
il_handle = ll_handle

return 0


end function

on w_liste_distribuzione.create
int iCurrent
call super::create
this.st_1=create st_1
this.tv_1=create tv_1
this.dw_destinatario_ricerca=create dw_destinatario_ricerca
this.dw_det_liste_dist_destinatari_det=create dw_det_liste_dist_destinatari_det
this.dw_tes_liste_dist=create dw_tes_liste_dist
this.dw_tab_tipi_liste_dist=create dw_tab_tipi_liste_dist
this.dw_folder=create dw_folder
this.dw_det_liste_dist=create dw_det_liste_dist
this.dw_det_liste_dist_new_fasi=create dw_det_liste_dist_new_fasi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.tv_1
this.Control[iCurrent+3]=this.dw_destinatario_ricerca
this.Control[iCurrent+4]=this.dw_det_liste_dist_destinatari_det
this.Control[iCurrent+5]=this.dw_tes_liste_dist
this.Control[iCurrent+6]=this.dw_tab_tipi_liste_dist
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_det_liste_dist
this.Control[iCurrent+9]=this.dw_det_liste_dist_new_fasi
end on

on w_liste_distribuzione.destroy
call super::destroy
destroy(this.st_1)
destroy(this.tv_1)
destroy(this.dw_destinatario_ricerca)
destroy(this.dw_det_liste_dist_destinatari_det)
destroy(this.dw_tes_liste_dist)
destroy(this.dw_tab_tipi_liste_dist)
destroy(this.dw_folder)
destroy(this.dw_det_liste_dist)
destroy(this.dw_det_liste_dist_new_fasi)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

dw_tab_tipi_liste_dist.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen , c_default)

dw_tes_liste_dist.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen, c_default)

dw_det_liste_dist.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen , c_default)

dw_det_liste_dist_new_fasi.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen , c_default)

dw_det_liste_dist_destinatari_det.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen , c_default)

dw_destinatario_ricerca.set_dw_options( sqlca, pcca.null_object, c_retrieveonopen , c_default)

dw_tab_tipi_liste_dist.hide()

dw_tes_liste_dist.hide()

dw_det_liste_dist.hide()

dw_det_liste_dist_new_fasi.hide()

dw_det_liste_dist_destinatari_det.hide()

dw_tab_tipi_liste_dist.change_dw_current()

lw_oggetti[1] = dw_destinatario_ricerca

lw_oggetti[2] = st_1

dw_folder.fu_assigntab(2, "Rubrica Destinatari", lw_oggetti[])

lw_oggetti[1] = dw_tab_tipi_liste_dist

lw_oggetti[2] = dw_tes_liste_dist

lw_oggetti[3] = dw_det_liste_dist

lw_oggetti[4] = dw_det_liste_dist_new_fasi

lw_oggetti[5] = dw_det_liste_dist_destinatari_det

dw_folder.fu_assigntab(1, "Liste Di Distribuzione", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)

dw_folder.fu_selecttab(1)

tv_1.deletepictures()

tv_1.PictureHeight = 16

tv_1.PictureWidth = 16

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_categorie.bmp")

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_reparti.bmp")

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_area.bmp")

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_attrezzature.bmp")

//Donato 26-01-2009 parametro per abilitare o meno la gestione protezione dei camopi della NC
//in base a quelli specificati nelle fasi del ciclo di vita

select flag
into :is_flag_BNC
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'BNC' and flag_parametro = 'F';			
if sqlca.sqlcode = 0 then
else
	is_flag_BNC = "N"
end if				
//fine modifica -----------------------------------------------------------------------------------------------------

wf_cancella_treeview()

wf_imposta_tv()

tv_1.setfocus()

ib_retrieve = true
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_liste_dist, &
                 "cod_tipo_lista_dist", &
					  sqlca, &
                 "tab_tipi_liste_dist", &
					  "cod_tipo_lista_dist", &
					  "descrizione", &
					  "")
					  
					  
f_PO_LoadDDDW_DW(dw_det_liste_dist, &
                 "cod_destinatario", &
					  sqlca, &
                 "tab_ind_dest", &
					  "cod_destinatario", &
					  "des_destinatario", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")				
					  
f_PO_LoadDDDW_DW(dw_det_liste_dist_destinatari_det, &
                 "cod_destinatario_mail", &
					  sqlca, &
                 "tab_ind_dest", &
					  "cod_destinatario", &
					  "des_destinatario", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")						  
					  
f_PO_LoadDDDW_DW(dw_det_liste_dist_destinatari_det,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_liste_dist_destinatari_det, &
                 "cod_cat_mer", &
					  sqlca, &
                 "tab_cat_mer", &
					  "cod_cat_mer", &
					  "des_cat_mer", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
//f_PO_LoadDDDW_DW(dw_destinatario_ricerca, &
//                 "cod_destinatario", &
//					  sqlca, &
//                 "tab_ind_dest", &
//					  "cod_destinatario", &
//					  "des_destinatario", &
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
//
					  

			  
end event

event pc_close;call super::pc_close;dw_destinatario_ricerca.resetupdate()

end event

event rbuttondown;call super::rbuttondown;m_menu_liste menu
	
menu = create m_menu_liste

s_cs_xx.parametri.parametro_w_cs_xx_1 = this
	
m_menu_liste.m_nuovotipo.visible = true
m_menu_liste.m_nuovalista.visible = false			
m_menu_liste.m_cancella.visible = false
m_menu_liste.m_modifica.visible = false		
m_menu_liste.m_ricaricamenu.visible = true
		
m_menu_liste.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())	

end event

type st_1 from statictext within w_liste_distribuzione
integer x = 1234
integer y = 1140
integer width = 2011
integer height = 80
string dragicon = "UserObject5!"
boolean dragauto = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type tv_1 from treeview within w_liste_distribuzione
integer x = 18
integer y = 20
integer width = 1157
integer height = 1240
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"Custom093!","C:\CS_70\framework\RISORSE\Foglio2_Dim_Orig.bmp","Start!","Picture!","Custom096!","Custom024!"}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event selectionchanged;treeviewitem ltv_item

ws_record lws_record

if isnull(newhandle) or newhandle <= 0 then return 0

il_handle = newhandle

getitem( newhandle, ltv_item)

lws_record = ltv_item.data

dw_folder.fu_selecttab(1)

choose case lws_record.tipo_livello
		
	case "L"    //tab_tipi_liste_dist
				
		is_cod_tipo_lista_dist = lws_record.cod_tipo_lista_dist
		dw_tab_tipi_liste_dist.show()
		dw_tes_liste_dist.hide()
		dw_det_liste_dist.hide()
		dw_det_liste_dist_new_fasi.hide()
		
		dw_det_liste_dist_destinatari_det.hide()	
		
		iuo_dw_main = dw_tab_tipi_liste_dist
		dw_tab_tipi_liste_dist.Change_DW_Current( )	
		is_tipo_retrieve = "L"
		parent.triggerevent("pc_retrieve")
		
	case "T"    //tes_liste_dist
		
		is_cod_tipo_lista_dist = lws_record.cod_tipo_lista_dist
		is_cod_lista_dist = lws_record.cod_lista_dist		
		dw_tab_tipi_liste_dist.hide()
		dw_tes_liste_dist.show()
		dw_det_liste_dist.hide()
		dw_det_liste_dist_new_fasi.hide()
		
		dw_det_liste_dist_destinatari_det.hide()				
		
		iuo_dw_main = dw_tes_liste_dist
		dw_tes_liste_dist.Change_DW_Current( )
		is_tipo_retrieve = "T"
		parent.triggerevent("pc_retrieve")	
		
				
		
	case "F"    //det_liste_dist
		
		if lws_record.flag_gestione_fasi <> "S" then
			
			is_cod_tipo_lista_dist = lws_record.cod_tipo_lista_dist
			is_cod_lista_dist = lws_record.cod_lista_dist	
			is_cod_destinatario = lws_record.cod_destinatario
			il_num_sequenza = lws_record.num_sequenza
			dw_tab_tipi_liste_dist.hide()
			dw_tes_liste_dist.hide()
			dw_det_liste_dist_new_fasi.hide()
			
			dw_det_liste_dist_destinatari_det.hide()
			dw_det_liste_dist.show()	
			
			iuo_dw_main = dw_det_liste_dist
			dw_det_liste_dist.Change_DW_Current( )
			is_tipo_retrieve = "F"
			parent.triggerevent("pc_retrieve")	
			
		else
			
			is_cod_tipo_lista_dist = lws_record.cod_tipo_lista_dist
			is_cod_lista_dist = lws_record.cod_lista_dist	
			is_cod_destinatario = lws_record.cod_destinatario
			il_num_sequenza = lws_record.num_sequenza
			dw_tab_tipi_liste_dist.hide()
			dw_tes_liste_dist.hide()
			dw_det_liste_dist_new_fasi.show()
			
			dw_det_liste_dist.hide()
			dw_det_liste_dist_destinatari_det.hide()
			
			iuo_dw_main = dw_det_liste_dist_new_fasi
			dw_det_liste_dist_new_fasi.Change_DW_Current( )
			is_tipo_retrieve = "F"
			parent.triggerevent("pc_retrieve")				
			
		end if
		
	case "D"
		
		is_cod_tipo_lista_dist = lws_record.cod_tipo_lista_dist
		is_cod_lista_dist = lws_record.cod_lista_dist	
		is_cod_destinatario = lws_record.cod_destinatario
		il_num_sequenza = lws_record.num_sequenza
		is_cod_destinatario_mail = lws_record.cod_destinatario_mail		
		dw_tab_tipi_liste_dist.hide()
		dw_tes_liste_dist.hide()
		dw_det_liste_dist.hide()
		dw_det_liste_dist_new_fasi.hide()
		
		dw_det_liste_dist_destinatari_det.show()
		
		iuo_dw_main = dw_det_liste_dist_destinatari_det
		dw_det_liste_dist_destinatari_det.Change_DW_Current( )
		is_tipo_retrieve = "D"
		parent.triggerevent("pc_retrieve")			
		
end choose

end event

event dragdrop;long ll_drop, ll_drag

treeviewitem ltv_appo

ws_record  lstr_record

if handle = 0 or isnull(handle) then
	return -1
end if

tv_1.getitem( handle, ltv_appo)

lstr_record = ltv_appo.data

if lstr_record.flag_gestione_fasi = "S" and lstr_record.tipo_livello = "F" then
	il_handle = handle
	wf_nuovo_destinatario_fase_drop()
	SetDropHighlight(0)
	return 0
elseif lstr_record.flag_gestione_fasi = "N" and lstr_record.tipo_livello = "T" then	
	il_handle = handle
	wf_nuovo_destinatario_drop()
	SetDropHighlight(0)
	return 0
else	
//	messagebox( "OMNIA", "Attenzione: è possibile associare un destinatario solo ad una lista di distribuzione!")	
	SetDropHighlight(0)
	return -1
end if
	




end event

event dragwithin;TreeViewItem ltvi_Over

ws_record    lstr_voce

If GetItem(handle, ltvi_Over) = -1 Then
	
	SetDropHighlight(0)

	Return
	
End If

lstr_voce = ltvi_Over.data

if (lstr_voce.flag_gestione_fasi = "S" and lstr_voce.tipo_livello = "F") or (lstr_voce.flag_gestione_fasi = "N" and lstr_voce.tipo_livello = "T") then

	SetDropHighlight(handle)
	
	il_handle = handle
	
end if



end event

event rightclicked;selectitem(handle)

il_handle = handle

m_menu_liste menu
	
menu = create m_menu_liste

s_cs_xx.parametri.parametro_w_cs_xx_1 = parent
	
if not isnull(il_handle) and il_handle > 0 then
		
	treeviewitem ltv_item
		
	ws_record lstr_voce
		
	getitem(il_handle,ltv_item)
		
	lstr_voce = ltv_item.data
	
	choose case lstr_voce.tipo_livello
				
		case "L"
				
			m_menu_liste.m_nuovotipo.visible = true
			m_menu_liste.m_nuovalista.visible = true	
			m_menu_liste.m_cancella.visible = true
			m_menu_liste.m_modifica.visible = true
			m_menu_liste.m_ricaricamenu.visible = true
			m_menu_liste.m_nuovodestinatario.visible = false
			m_menu_liste.m_nuovafase.visible = false
			m_menu_liste.m_nuovodestinatariofase.visible = false
			
		case "T"
			
			m_menu_liste.m_nuovotipo.visible = false
			m_menu_liste.m_nuovalista.visible = false							
			m_menu_liste.m_cancella.visible = true
			m_menu_liste.m_modifica.visible = true			
			m_menu_liste.m_ricaricamenu.visible = true
			
			if lstr_voce.flag_gestione_fasi <> "S" then
				
				m_menu_liste.m_nuovodestinatario.visible = true
				m_menu_liste.m_nuovafase.visible = false
				m_menu_liste.m_nuovodestinatariofase.visible = false				
				
			else
				
				m_menu_liste.m_nuovodestinatario.visible = false
				m_menu_liste.m_nuovafase.visible = true
				m_menu_liste.m_nuovodestinatariofase.visible = false
				
			end if
			
		case "F"
			
			m_menu_liste.m_nuovotipo.visible = false
			m_menu_liste.m_nuovalista.visible = false
			m_menu_liste.m_cancella.visible = true
			m_menu_liste.m_modifica.visible = true			
			m_menu_liste.m_ricaricamenu.visible = true		
			
			if lstr_voce.flag_gestione_fasi <> "S" then
				
				m_menu_liste.m_nuovodestinatario.visible = false
				m_menu_liste.m_nuovafase.visible = false
				m_menu_liste.m_nuovodestinatariofase.visible = false				
				
			else
				
				m_menu_liste.m_nuovodestinatario.visible = false
				m_menu_liste.m_nuovafase.visible = false
				m_menu_liste.m_nuovodestinatariofase.visible = true
				
			end if	
			
		case "D"
			
			m_menu_liste.m_nuovotipo.visible = false
			m_menu_liste.m_nuovalista.visible = false
			m_menu_liste.m_cancella.visible = true
			m_menu_liste.m_modifica.visible = true			
			m_menu_liste.m_ricaricamenu.visible = true		
			m_menu_liste.m_nuovodestinatario.visible = false
			m_menu_liste.m_nuovafase.visible = false
			m_menu_liste.m_nuovodestinatariofase.visible = false				
				
		case else
			
			m_menu_liste.m_nuovotipo.visible = true
			m_menu_liste.m_nuovalista.visible = false			
			m_menu_liste.m_cancella.visible = false
			m_menu_liste.m_modifica.visible = false		
			m_menu_liste.m_ricaricamenu.visible = true
			
	end choose
	
	m_menu_liste.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())	

end if
end event

type dw_destinatario_ricerca from uo_cs_xx_dw within w_liste_distribuzione
event ue_lista ( )
integer x = 1234
integer y = 160
integer width = 2149
integer height = 940
integer taborder = 30
string dataobject = "d_ind_dest_lista"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;//this.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "cs_sep.ico"
end event

event pcd_close;call super::pcd_close;dw_destinatario_ricerca.resetupdate()

end event

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event rowfocuschanged;call super::rowfocuschanged;if this.getrow() < 1 then return 

st_1.text = "Trascina - " + this.getitemstring( this.getrow(), "des_destinatario")
is_codice_rubrica = this.getitemstring( this.getrow(), "cod_destinatario")
end event

type dw_det_liste_dist_destinatari_det from uo_cs_xx_dw within w_liste_distribuzione
event ue_lista ( )
event ue_reimposta_flag_responsabile ( )
integer x = 1257
integer y = 140
integer width = 2126
integer height = 1080
integer taborder = 40
string dataobject = "d_det_liste_dist_destinatari_det"
boolean border = false
end type

event ue_reimposta_flag_responsabile();dw_det_liste_dist_destinatari_det.setitem(dw_det_liste_dist_destinatari_det.getrow(), "flag_responsabile", "N")
end event

event updatestart;call super::updatestart;long ll_riga

if ib_new then

	if isnull(is_cod_tipo_lista_dist) or is_cod_tipo_lista_dist = "" then
		g_mb.messagebox( "OMNIA", "Attenzione: specificare il tipo di lista di distribuzione!")
		return -1
	end if
	
	ll_riga = this.getrow()
	if ll_riga < 1 then
		g_mb.messagebox( "OMNIA", "Attenzione: Impossobile accedere ai dati della riga!")
		return -1
	end if	
	
	this.setitem( ll_riga, "cod_tipo_lista_dist", is_cod_tipo_lista_dist)
	this.setitem( ll_riga, "cod_lista_dist", is_cod_lista_dist)	
	this.setitem( ll_riga, "cod_destinatario", is_cod_destinatario)
	this.setitem( ll_riga, "num_sequenza", il_num_sequenza)
	this.setitem( ll_riga, "cod_azienda", s_cs_xx.cod_azienda)
	
end if
end event

event pcd_delete;call super::pcd_delete;ib_new = false

ib_delete = true

tv_1.deleteitem(il_handle)

il_handle = 0

end event

event pcd_modify;call super::pcd_modify;ib_new = false

ib_delete = false
end event

event pcd_new;call super::pcd_new;long ll_riga

ib_new = true

ib_delete = false

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

ib_new = false

ib_delete = false

if	is_tipo_retrieve <> "D" then return 0

l_Error = Retrieve( s_cs_xx.cod_azienda, is_cod_tipo_lista_dist, is_cod_lista_dist, is_cod_destinatario, il_num_sequenza, is_cod_destinatario_mail)

if l_Error < 0 then
		PCCA.Error = c_Fatal
end if	
	

end event

event pcd_save;call super::pcd_save;string         ls_des_destinatario

treeviewitem   ltv_item

ws_record      lstr_voce

long           ll_handle

if ib_new then

	if this.getrow() < 1 then return -1
	
	lstr_voce.cod_tipo_lista_dist = this.getitemstring( this.getrow(), "cod_tipo_lista_dist")
	
	lstr_voce.cod_lista_dist = this.getitemstring( this.getrow(), "cod_lista_dist")
	
	lstr_voce.cod_destinatario = this.getitemstring( this.getrow(), "cod_destinatario")
	
	lstr_voce.cod_destinatario_mail = this.getitemstring( this.getrow(), "cod_destinatario_mail")
	
	lstr_voce.num_sequenza = this.getitemnumber( this.getrow(), "num_sequenza")
	
	lstr_voce.handle_padre = il_handle
	
	lstr_voce.tipo_livello = "D"
	
	select des_destinatario
	into   :ls_des_destinatario
	from   tab_ind_dest
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_destinatario = :lstr_voce.cod_destinatario_mail;
	
	ltv_item.label = ls_des_destinatario
	
	ltv_item.expanded = false
	
	ltv_item.selected = false
	
	ltv_item.children = true
	
	ltv_item.data = lstr_voce
	
	ltv_item.pictureindex = 4
	
	ltv_item.selectedpictureindex = 4
	
	ll_handle = tv_1.insertitemlast( il_handle, ltv_item)
	
	tv_1.selectitem(ll_handle)
	
	ib_new = false
	
end if

if ib_delete then
//	
//	tv_1.deleteitem(il_handle)
//	
	ib_delete = false
	
end if


return 0


end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "flag_responsabile"
		if is_flag_BNC = "S" then
		else
			g_mb.messagebox("OMNIA","Attenzione! Per questa azione è necessario che il parametro aziendale" + &
					" di tipo flag 'BNC' esista e sia posto a 'S'!", Exclamation!)
			dw_det_liste_dist_destinatari_det.postevent("ue_reimposta_flag_responsabile")
		end if			
			
end choose
end event

type dw_tes_liste_dist from uo_cs_xx_dw within w_liste_distribuzione
event ue_lista ( )
integer x = 1257
integer y = 140
integer width = 2080
integer height = 1096
integer taborder = 20
string dataobject = "d_tes_liste_dist_dett_new"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

ib_new = false

ib_delete = false

if	is_tipo_retrieve <> "T" then return 0

l_Error = Retrieve( s_cs_xx.cod_azienda, is_cod_tipo_lista_dist, is_cod_lista_dist)

if l_Error < 0 then
		PCCA.Error = c_Fatal
end if	
	

end event

event updatestart;call super::updatestart;long ll_riga

if ib_new then

	if isnull(is_cod_tipo_lista_dist) or is_cod_tipo_lista_dist = "" then
		g_mb.messagebox( "OMNIA", "Attenzione: specificare il tipo di lista di distribuzione!")
		return -1
	end if
	
	ll_riga = this.getrow()
	if ll_riga < 1 then
		g_mb.messagebox( "OMNIA", "Attenzione: Impossobile accedere ai dati della riga!")
		return -1
	end if	
	
	this.setitem( ll_riga, "cod_tipo_lista_dist", is_cod_tipo_lista_dist)
	this.setitem( ll_riga, "cod_azienda", s_cs_xx.cod_azienda)
	
end if
end event

event pcd_new;call super::pcd_new;long ll_riga

ib_new = true

ib_delete = false

end event

event pcd_modify;call super::pcd_modify;ib_new = false

ib_delete = false
end event

event pcd_delete;call super::pcd_delete;ib_new = false

ib_delete = true

tv_1.deleteitem(il_handle)

il_handle = 0

end event

event pcd_view;call super::pcd_view;treeviewitem   ltv_item

ws_record      lstr_voce

long           ll_handle, ll_riga

string         ls_null

if ib_new then

	ll_riga = this.getrow() 
	
	if ll_riga < 1 then return -1
	
	setnull(ls_null)
	
	lstr_voce.cod_tipo_lista_dist = this.getitemstring( ll_riga, "cod_tipo_lista_dist")
	
	lstr_voce.cod_lista_dist = this.getitemstring( ll_riga, "cod_lista_dist")
	
	lstr_voce.cod_destinatario = ls_null	
	
	lstr_voce.flag_gestione_fasi = "N"
	
	lstr_voce.tipo_livello = "T"
	
	lstr_voce.num_sequenza = 0
	
	lstr_voce.livello = 2
	
	lstr_voce.handle_padre = il_handle	
	
	ltv_item.label = this.getitemstring( ll_riga, "des_lista_dist")
	
	ltv_item.expanded = false
	
	ltv_item.selected = false
	
	ltv_item.children = false
	
	ltv_item.data = lstr_voce
	
	ltv_item.pictureindex = 2
	
	ltv_item.selectedpictureindex = 2
	
	ltv_item.overlaypictureindex = 2
	
	ll_handle = tv_1.insertitemlast( il_handle, ltv_item)
	
	tv_1.selectitem(ll_handle)
	
	ib_new = false
	
end if

if ib_delete then

	ib_delete = false
	
end if


return 0


end event

type dw_tab_tipi_liste_dist from uo_cs_xx_dw within w_liste_distribuzione
event ue_lista ( )
integer x = 1211
integer y = 140
integer width = 2149
integer height = 1096
integer taborder = 60
string dataobject = "d_tipi_liste_dist_dett_new"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

ib_new = false

ib_delete = false

if	is_tipo_retrieve <> "L" then return 0

l_Error = Retrieve(is_cod_tipo_lista_dist)

if l_Error < 0 then
		PCCA.Error = c_Fatal
end if	
	

end event

event pcd_new;call super::pcd_new;ib_new = true

ib_delete = false
end event

event pcd_modify;call super::pcd_modify;ib_new = false

ib_delete = false
end event

event pcd_view;call super::pcd_view;treeviewitem   ltv_item

ws_record      lstr_voce

long           ll_handle

if ib_new then

	if this.getrow() < 1 then return -1
	
	lstr_voce.cod_tipo_lista_dist = this.getitemstring( this.getrow(), "cod_tipo_lista_dist")
	
	lstr_voce.tipo_livello = "L"
	
	ltv_item.label = this.getitemstring( this.getrow(), "descrizione")
	
	ltv_item.expanded = false
	
	ltv_item.selected = false
	
	ltv_item.children = true
	
	ltv_item.data = lstr_voce
	
	ltv_item.pictureindex = 1
	
	ltv_item.selectedpictureindex = 1
	
	ll_handle = tv_1.insertitemlast( 0, ltv_item)
	
	tv_1.selectitem(ll_handle)
	
	ib_new = false
	
end if

if ib_delete then
	
	tv_1.deleteitem(il_handle)
	
	ib_delete = false
	
end if



return 0

end event

event pcd_delete;call super::pcd_delete;ib_delete = true
end event

type dw_folder from u_folder within w_liste_distribuzione
integer x = 1189
integer y = 20
integer width = 2240
integer height = 1240
integer taborder = 30
end type

type dw_det_liste_dist from uo_cs_xx_dw within w_liste_distribuzione
event ue_lista ( )
integer x = 1234
integer y = 140
integer width = 2149
integer height = 1100
integer taborder = 70
string dataobject = "d_det_liste_dist_dett_new"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

if	is_tipo_retrieve <> "F" then return 0

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_tipo_lista_dist, is_cod_lista_dist, is_cod_destinatario, il_num_sequenza)

if l_Error < 0 then
		PCCA.Error = c_Fatal
end if	
	
end event

event updateend;call super::updateend;//wf_cancella_treeview()
//wf_imposta_tv()
end event

event updatestart;call super::updatestart;long ll_riga

if ib_new then

	if isnull(is_cod_tipo_lista_dist) or is_cod_tipo_lista_dist = "" then
		g_mb.messagebox( "OMNIA", "Attenzione: specificare il tipo di lista di distribuzione!")
		return -1
	end if
	
	if isnull(is_cod_lista_dist) or is_cod_lista_dist = "" then
		g_mb.messagebox( "OMNIA", "Attenzione: specificare la lista di distribuzione!")
		return -1
	end if	
	
	ll_riga = this.getrow()
	if ll_riga < 1 then
		g_mb.messagebox( "OMNIA", "Attenzione: Impossobile accedere ai dati della riga!")
		return -1
	end if	
	
	this.setitem( ll_riga, "cod_tipo_lista_dist", is_cod_tipo_lista_dist)
	
	this.setitem( ll_riga, "cod_lista_dist", is_cod_lista_dist)
	
	this.setitem( ll_riga, "cod_azienda", s_cs_xx.cod_azienda)
	
end if
end event

event pcd_save;call super::pcd_save;string         ls_des_destinatario

treeviewitem   ltv_item

ws_record      lstr_voce

long           ll_handle, ll_num_sequenza

if ib_new then

	if this.getrow() < 1 then return -1
	
	lstr_voce.cod_tipo_lista_dist = this.getitemstring( this.getrow(), "cod_tipo_lista_dist")
	
	lstr_voce.cod_lista_dist = this.getitemstring( this.getrow(), "cod_lista_dist")
	
	lstr_voce.cod_destinatario = this.getitemstring( this.getrow(), "cod_destinatario")
	
	lstr_voce.num_sequenza = this.getitemnumber( this.getrow(), "num_sequenza")
	
	lstr_voce.flag_gestione_fasi = "N"
	
	lstr_voce.tipo_livello = "F"
	
	select des_destinatario
	into   :ls_des_destinatario
	from   tab_ind_dest
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_destinatario = :lstr_voce.cod_destinatario;
				 
	if isnull(ls_des_destinatario) then ls_des_destinatario = ""				 
	
	ltv_item.label = string(lstr_voce.num_sequenza) + ", " + ls_des_destinatario
	
	ltv_item.expanded = false
	
	ltv_item.selected = false
	
	ltv_item.children = false
	
	ltv_item.data = lstr_voce
	
	ltv_item.pictureindex = 5
	
	ltv_item.selectedpictureindex = 5
	
	ll_handle = tv_1.insertitemlast( il_handle, ltv_item)
	
	tv_1.selectitem(ll_handle)
	
	ib_new = false
	
end if

return 0


end event

event pcd_new;call super::pcd_new;ib_new = true

ib_delete = false

end event

event pcd_modify;call super::pcd_modify;ib_new = false

ib_delete = false

end event

type dw_det_liste_dist_new_fasi from uo_cs_xx_dw within w_liste_distribuzione
event ue_lista ( )
integer x = 1234
integer y = 120
integer width = 2149
integer height = 1100
integer taborder = 50
string dataobject = "d_det_liste_dist_dett_new_fasi"
boolean border = false
end type

event updateend;call super::updateend;//wf_cancella_treeview()
//wf_imposta_tv()
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

if	is_tipo_retrieve <> "F" then return 0

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_tipo_lista_dist, is_cod_lista_dist, is_cod_destinatario, il_num_sequenza)

if l_Error < 0 then
		PCCA.Error = c_Fatal
end if	
	
end event

event updatestart;call super::updatestart;string ls_codice

long   ll_riga

if ib_new then

	if isnull(is_cod_tipo_lista_dist) or is_cod_tipo_lista_dist = "" then
		g_mb.messagebox( "OMNIA", "Attenzione: specificare il tipo di lista di distribuzione!")
		return -1
	end if
	
	if isnull(is_cod_lista_dist) or is_cod_lista_dist = "" then
		g_mb.messagebox( "OMNIA", "Attenzione: specificare la lista di distribuzione!")
		return -1
	end if	
	
	ll_riga = this.getrow()
	if ll_riga < 1 then
		g_mb.messagebox( "OMNIA", "Attenzione: Impossobile accedere ai dati della riga!")
		return -1
	end if	
	
	this.setitem( ll_riga, "cod_tipo_lista_dist", is_cod_tipo_lista_dist)
	
	this.setitem( ll_riga, "cod_lista_dist", is_cod_lista_dist)
	
	this.setitem( ll_riga, "cod_azienda", s_cs_xx.cod_azienda)
	
	// *** parametro con codice destinatario cs
	
	select stringa
	into   :ls_codice
	from   parametri_omnia
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'PCU';
			 
	if sqlca.sqlcode <> 0 then
		
		g_mb.messagebox( "OMNIA", "Errore durante la selezione del parametro PCU: " + sqlca.sqlerrtext)
		
		return -1
		
	end if
	
	this.setitem( ll_riga, "cod_destinatario", ls_codice)
	
end if
end event

event pcd_new;call super::pcd_new;ib_new = true

ib_delete = false

this.object.b_campi.visible = true
this.object.b_campi.enabled = false
end event

event pcd_modify;call super::pcd_modify;ib_new = false

ib_delete = false

this.object.b_campi.visible = true
this.object.b_campi.enabled = false
end event

event pcd_delete;call super::pcd_delete;ib_new = false

ib_delete = false
end event

event pcd_save;call super::pcd_save;string         ls_descrizione

treeviewitem   ltv_item

ws_record      lstr_voce

long           ll_handle, ll_num_sequenza

this.object.b_campi.visible = true
this.object.b_campi.enabled = true

if ib_new then

	if this.getrow() < 1 then return -1

	ls_descrizione = this.getitemstring( this.getrow(), "des_fase")
	
	lstr_voce.cod_tipo_lista_dist = this.getitemstring( this.getrow(), "cod_tipo_lista_dist")
	
	lstr_voce.cod_lista_dist = this.getitemstring( this.getrow(), "cod_lista_dist")
	
	lstr_voce.cod_destinatario = this.getitemstring( this.getrow(), "cod_destinatario")
	
	lstr_voce.num_sequenza = this.getitemnumber( this.getrow(), "num_sequenza")
	
	lstr_voce.flag_gestione_fasi = "S"
	
	lstr_voce.tipo_livello = "F"
	
	ltv_item.label = string(lstr_voce.num_sequenza) + ", " + ls_descrizione
	
	ltv_item.expanded = false
	
	ltv_item.selected = false
	
	ltv_item.children = false
	
	ltv_item.data = lstr_voce
	
	ltv_item.pictureindex = 3
	
	ltv_item.selectedpictureindex = 3
	
	ll_handle = tv_1.insertitemlast( il_handle, ltv_item)
	
	tv_1.selectitem(ll_handle)
	
	ib_new = false
	
end if

return 0


end event

event buttonclicked;call super::buttonclicked;string ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_destinatario
decimal ld_num_sequenza
integer ll_count
s_dati_x_campi_nc ls_dati_x_campi_nc

if row > 0 then
	if dwo.name = "b_campi" then
		//Donato 29-01-2009 se il parametro BNC, blocco campi NC è posto a S vai avanti
		//altrimenti segnala e non aprire la w_det_liste_dist_campi_nc
		if is_flag_BNC = "S" then
			//controlla se la riga padre è stata salvata
			ls_cod_tipo_lista_dist = getitemstring(row, "cod_tipo_lista_dist")
			ls_cod_lista_dist = getitemstring(row, "cod_lista_dist")
			ls_cod_destinatario = getitemstring(row, "cod_destinatario")
			ld_num_sequenza = getitemdecimal(row, "num_sequenza")
			
			select count(cod_azienda)
			into :ll_count
			from det_liste_dist
			where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
				and cod_lista_dist=:ls_cod_lista_dist and cod_destinatario=:ls_cod_destinatario
				and num_sequenza=:ld_num_sequenza
			;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore durante la lettura dei dati in det_liste_dist", StopSign!)
				return
			end if
			
			if sqlca.sqlcode = 0 then
				//il dato è presente in tabella: aprire la maschera di dettaglio dei campi			
				window_open_parm(w_det_liste_dist_campi_nc, -1, dw_det_liste_dist_new_fasi)
			end if
		else
			g_mb.messagebox("OMNIA","Attenzione! Per questa funzione è necessario che il parametro aziendale" + &
					" di tipo flag 'BNC' esista e sia posto a 'S'!", Exclamation!)
		end if		
	end if
end if
end event

event pcd_view;call super::pcd_view;this.object.b_campi.visible = true
this.object.b_campi.enabled = true
end event

