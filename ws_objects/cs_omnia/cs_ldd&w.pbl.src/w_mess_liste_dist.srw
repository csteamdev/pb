﻿$PBExportHeader$w_mess_liste_dist.srw
forward
global type w_mess_liste_dist from w_cs_xx_principale
end type
type cb_deseleziona from commandbutton within w_mess_liste_dist
end type
type sle_oggetto from singlelineedit within w_mess_liste_dist
end type
type mle_testo from multilineedit within w_mess_liste_dist
end type
type cb_seleziona from commandbutton within w_mess_liste_dist
end type
type st_allegato from statictext within w_mess_liste_dist
end type
type cb_invia from commandbutton within w_mess_liste_dist
end type
type dw_liste from datawindow within w_mess_liste_dist
end type
type dw_destinatari from datawindow within w_mess_liste_dist
end type
type tab_1 from tab within w_mess_liste_dist
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tab_1 from tab within w_mess_liste_dist
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
end forward

global type w_mess_liste_dist from w_cs_xx_principale
integer x = 0
integer y = 0
integer width = 3694
integer height = 2324
string title = "Invio Messaggi a Liste di Distribuzione"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_deseleziona cb_deseleziona
sle_oggetto sle_oggetto
mle_testo mle_testo
cb_seleziona cb_seleziona
st_allegato st_allegato
cb_invia cb_invia
dw_liste dw_liste
dw_destinatari dw_destinatari
tab_1 tab_1
end type
global w_mess_liste_dist w_mess_liste_dist

type variables
boolean ib_inviato = false, ib_log = true, ib_esci

uo_outlook iuo_outlook
end variables

forward prototypes
public function integer wf_carica_liste ()
public function integer wf_elimina_destinatari (string as_tipo_lista, string as_cod_lista)
public function integer wf_carica_destinatari (string as_tipo_lista, string as_cod_lista)
end prototypes

public function integer wf_carica_liste ();long   ll_riga

string ls_select, ls_tipo_lista, ls_cod_lista, ls_des_lista


// Viene impostata la select per la lettura delle liste di distribuzione

ls_select = &
"select cod_tipo_lista_dist, " + &
"		  cod_lista_dist, " + &
"		  des_lista_dist " + &
"from	  tes_liste_dist " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

declare liste dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open liste;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore liste: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch liste
	into  :ls_tipo_lista,
			:ls_cod_lista,
			:ls_des_lista;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore liste: " + sqlca.sqlerrtext)
		close liste;
		return -1
	elseif sqlca.sqlcode = 100	then
		close liste;
		exit
	end if
	
	// La lista letta viene inserita nell'elenco
	
	ll_riga = dw_liste.insertrow(0)
	dw_liste.setitem(ll_riga,"tipo_lista",ls_tipo_lista)
	dw_liste.setitem(ll_riga,"cod_lista",ls_cod_lista)
	dw_liste.setitem(ll_riga,"descrizione",ls_des_lista)
	dw_liste.setitem(ll_riga,"selezione","N")
	
loop

return 0
end function

public function integer wf_elimina_destinatari (string as_tipo_lista, string as_cod_lista);long ll_i


// La funzione elimina dall'elenco dei destinatari tutti i destinatari appartenenti ad una certa lista di distribuzione

for ll_i = dw_destinatari.rowcount() to 1 step -1
	
	// Si controlla se la lista di appartenenza del destinatario è quella indicata alla chiamata della funzione
	
	if dw_destinatari.getitemstring(ll_i,"tipo_lista") <> as_tipo_lista or &
		dw_destinatari.getitemstring(ll_i,"cod_lista") <> as_cod_lista then
		continue
	end if
	
	// Il destinatario viene eliminato
	
	dw_destinatari.deleterow(ll_i)
	
next

return 0
end function

public function integer wf_carica_destinatari (string as_tipo_lista, string as_cod_lista);long   ll_riga

string ls_select, ls_destinatario, ls_indirizzo


// Viene impostata la select per la lettura dei destinatari dalla lista di distribuzione specificata

ls_select = &
"select des_destinatario, " + &
"		  indirizzo " + &
"from	  tab_ind_dest " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"       email = 'S' and " + &
"       cod_destinatario in ( select cod_destinatario " + &
"										from   det_liste_dist " + &
"									   where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"                                    cod_tipo_lista_dist = '" + as_tipo_lista + "' and " +&
"												 cod_lista_dist = '" + as_cod_lista + "' ) "

declare destinatari dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open destinatari;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore destinatari: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch destinatari
	into  :ls_destinatario,
			:ls_indirizzo;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore destinatari: " + sqlca.sqlerrtext)
		close destinatari;
		return -1
	elseif sqlca.sqlcode = 100	then
		close destinatari;
		exit
	end if
	
	// Ogni destinatario trovato viene inserito nell'elenco e sarà inizialmente deselezionato
	// Per identificare la lista di appartenenza di ogni destinatario vengono riportati tipo e codice della lista
	
	ll_riga = dw_destinatari.insertrow(0)
	dw_destinatari.setitem(ll_riga,"tipo_lista",as_tipo_lista)
	dw_destinatari.setitem(ll_riga,"cod_lista",as_cod_lista)
	dw_destinatari.setitem(ll_riga,"descrizione",ls_destinatario)
	dw_destinatari.setitem(ll_riga,"indirizzo",ls_indirizzo)
	dw_destinatari.setitem(ll_riga,"selezione","N")
	
loop

return 0
end function

on w_mess_liste_dist.create
int iCurrent
call super::create
this.cb_deseleziona=create cb_deseleziona
this.sle_oggetto=create sle_oggetto
this.mle_testo=create mle_testo
this.cb_seleziona=create cb_seleziona
this.st_allegato=create st_allegato
this.cb_invia=create cb_invia
this.dw_liste=create dw_liste
this.dw_destinatari=create dw_destinatari
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_deseleziona
this.Control[iCurrent+2]=this.sle_oggetto
this.Control[iCurrent+3]=this.mle_testo
this.Control[iCurrent+4]=this.cb_seleziona
this.Control[iCurrent+5]=this.st_allegato
this.Control[iCurrent+6]=this.cb_invia
this.Control[iCurrent+7]=this.dw_liste
this.Control[iCurrent+8]=this.dw_destinatari
this.Control[iCurrent+9]=this.tab_1
end on

on w_mess_liste_dist.destroy
call super::destroy
destroy(this.cb_deseleziona)
destroy(this.sle_oggetto)
destroy(this.mle_testo)
destroy(this.cb_seleziona)
destroy(this.st_allegato)
destroy(this.cb_invia)
destroy(this.dw_liste)
destroy(this.dw_destinatari)
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_test


wf_carica_liste()
		
// Controllo per l'abilitazione del file LOG

select stringa
into   :ls_test
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LOG';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Funzione LOG INVIO MESSAGGI disabilitata")
	ib_log = false
end if	
end event

event closequery;call super::closequery;if ib_inviato = false then
	if g_mb.messagebox("OMNIA","Uscire senza inviare il messaggio?",question!,yesno!,2) = 2 then
		return 1
	end if
end if
end event

type cb_deseleziona from commandbutton within w_mess_liste_dist
integer x = 434
integer y = 2120
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;if st_allegato.text <> "" then
	if g_mb.messagebox("OMNIA","Eliminare l'allegato attualmente selezionato?",question!,yesno!,2) = 2 then
		return
	end if
end if

st_allegato.text = ""
end event

type sle_oggetto from singlelineedit within w_mess_liste_dist
integer x = 46
integer y = 140
integer width = 3589
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "Oggetto"
borderstyle borderstyle = stylelowered!
end type

type mle_testo from multilineedit within w_mess_liste_dist
integer x = 46
integer y = 240
integer width = 3589
integer height = 1860
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "Testo del messaggio"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_seleziona from commandbutton within w_mess_liste_dist
integer x = 46
integer y = 2120
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allegato"
end type

event clicked;string ls_filename


if st_allegato.text <> "" then
	if g_mb.messagebox("OMNIA","Sostituire l'allegato attualmente selezionato?",question!,yesno!,2) = 2 then
		return
	end if
end if

getfileopenname("Selezione Allegato",st_allegato.text,ls_filename,"*","Tutti i file (*.*),*.*")
end event

type st_allegato from statictext within w_mess_liste_dist
integer x = 823
integer y = 2120
integer width = 2423
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_invia from commandbutton within w_mess_liste_dist
integer x = 3269
integer y = 2120
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia"
end type

event clicked;boolean lb_conferma

long    ll_i, ll_j, ll_return

string  ls_destinatari[], ls_allegati[1], ls_messaggio


// Lettura file allegato

ls_allegati[1] = st_allegato.text

// Lettura destinatari
	
ll_j = 0
	
for ll_i = 1 to dw_destinatari.rowcount()
		
	if dw_destinatari.getitemstring(ll_i,"selezione") = "S" then
		ll_j ++
		ls_destinatari[ll_j] = dw_destinatari.getitemstring(ll_i,"indirizzo")
	end if
	
next

// Invio messaggio

iuo_outlook = create uo_outlook

if iuo_outlook.uof_outlook(0,"A",sle_oggetto.text,mle_testo.text,ls_destinatari,ls_allegati,false,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy iuo_outlook
	return -1
end if

destroy iuo_outlook

// Eventuale registrazione su file LOG

if ib_log then
	for ll_i = 1 to upperbound(ls_destinatari)
		f_scrivi_log("Messaggio ~"" + sle_oggetto.text + "~" inviato a: " + ls_destinatari[ll_i])
	next
end if

ib_inviato = true

close(parent)
end event

type dw_liste from datawindow within w_mess_liste_dist
integer x = 46
integer y = 140
integer width = 1783
integer height = 2060
integer taborder = 10
string dataobject = "d_invio_messaggi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;string ls_tipo_lista, ls_cod_lista, ls_nome


if dwo.name = "selezione" then
	
	ls_tipo_lista = getitemstring(row,"tipo_lista")
	
	ls_cod_lista = getitemstring(row,"cod_lista")
	
	choose case data
			
		case "S"
			
			// Vengono caricati nell'elenco i destinatari della lista selezionata
			
			wf_carica_destinatari(ls_tipo_lista,ls_cod_lista)
			
		case "N"
			
			// Lista deselezionata, i destinatari della lista deselezionata vengono eliminati dall'elenco
			
			wf_elimina_destinatari(ls_tipo_lista,ls_cod_lista)
			
	end choose
	
end if
end event

type dw_destinatari from datawindow within w_mess_liste_dist
integer x = 1851
integer y = 140
integer width = 1783
integer height = 2060
integer taborder = 10
string dataobject = "d_invio_messaggi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_1 from tab within w_mess_liste_dist
integer x = 23
integer y = 20
integer width = 3634
integer height = 2200
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

event selectionchanged;choose case newindex
	
	case 1
		
		sle_oggetto.hide()
		
		mle_testo.hide()
		
		cb_seleziona.hide()
		
		st_allegato.hide()
		
		cb_invia.hide()
		
		dw_liste.show()
		
		dw_destinatari.show()
		
	case 2
		
		dw_liste.hide()
		
		dw_destinatari.hide()
		
		sle_oggetto.show()
		
		mle_testo.show()
		
		cb_seleziona.show()
		
		st_allegato.show()
		
		cb_invia.show()
		
end choose
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3598
integer height = 2076
long backcolor = 12632256
string text = "Selezione Destinatari"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3598
integer height = 2076
long backcolor = 12632256
string text = "Composizione Messaggio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

