﻿$PBExportHeader$w_report_documenti_letture.srw
$PBExportComments$Finestra Report Documenti Letture
forward
global type w_report_documenti_letture from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_documenti_letture
end type
type cb_selezione from commandbutton within w_report_documenti_letture
end type
type cb_report from commandbutton within w_report_documenti_letture
end type
type dw_selezione from uo_cs_xx_dw within w_report_documenti_letture
end type
type dw_report from uo_cs_xx_dw within w_report_documenti_letture
end type
end forward

global type w_report_documenti_letture from w_cs_xx_principale
integer width = 2098
integer height = 852
string title = "Report Attrezzature"
cb_annulla cb_annulla
cb_selezione cb_selezione
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_documenti_letture w_report_documenti_letture

type variables
private:
	string is_original_sql = ""
	long il_init_width = 0
	long il_init_height = 0
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

dw_report.visible = false
cb_selezione.visible = false

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

is_original_sql = dw_report.GetSQLSelect()

il_init_width = 2087
il_init_height = 880

this.x = 741
this.y = 885
this.width = il_init_width
this.height = il_init_height

// -- Parametri apertura
s_cs_xx_parametri lstr_param
lstr_param = message.powerobjectparm

if not isnull(lstr_param) and isvalid(lstr_param) then
	if not isnull(lstr_param.parametro_ul_1) and not isnull(lstr_param.parametro_ul_2) and &
		not isnull(lstr_param.parametro_ul_3) then
		
		dw_selezione.event post ue_imposta_dati(lstr_param.parametro_ul_1, lstr_param.parametro_ul_2, lstr_param.parametro_ul_3)

	end if
end if


end event

on w_report_documenti_letture.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_selezione=create cb_selezione
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_selezione
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_documenti_letture.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_selezione)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione, "cod_tipo_lista", sqlca, &
	"tab_tipi_liste_dist", "cod_tipo_lista_dist", "descrizione", "")
	
/*f_PO_LoadDDDW_DW(dw_selezione,"cod_lista",sqlca, &
	"tes_liste_dist", "cod_tipo_lista_dist", "descrizione", "")
	*/
end event

event resize;call super::resize;
dw_selezione.move(20,20)


cb_selezione.move(newwidth - cb_selezione.width  - 20, newheight - cb_selezione.height - 20)
dw_report.move(20,20)
dw_report.resize(cb_selezione.x + cb_selezione.width - 20 , cb_selezione.y - 40)
end event

type cb_annulla from commandbutton within w_report_documenti_letture
integer x = 1280
integer y = 640
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_selezione from commandbutton within w_report_documenti_letture
integer x = 1669
integer y = 640
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = il_init_width
parent.height = il_init_height

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type cb_report from commandbutton within w_report_documenti_letture
integer x = 1669
integer y = 640
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_descrizione, ls_cod_tipo_lista, ls_cod_lista, ls_where, ls_filtri, ls_rag_soc_1, ls_rag_soc_2
long ll_anno, ll_num, ll_prog

dw_report.reset()
dw_selezione.accepttext()

ll_anno = dw_selezione.getitemnumber(1, "anno_registrazione")
ll_num = dw_selezione.getitemnumber(1, "num_registrazione")
ll_prog = dw_selezione.getitemnumber(1, "progressivo_doc")
ls_descrizione = dw_selezione.getitemstring(1, "descrizione")
ls_cod_tipo_lista = dw_selezione.getitemstring(1, "cod_tipo_lista")
ls_cod_lista = dw_selezione.getitemstring(1 ,"cod_lista")

// -- Imposto filtri
ls_where = " AND liste_dist_doc.cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_filtri = ""

if not isnull(ll_anno) and ll_anno > 1900 then
	ls_where += " AND det_documenti.anno_registrazione=" + string(ll_anno)
	ls_filtri += "Anno Doc: " + string(ll_anno) + "; "
end if

if not isnull(ll_num) and ll_num > 0 then
	ls_where += " AND det_documenti.num_registrazione=" + string(ll_num)
	ls_filtri += "Num Doc: " + string(ll_num) + "; "
end if

if not isnull(ll_prog) and ll_prog > 0 then
	ls_where += " AND det_documenti.progressivo=" + string(ll_prog)
	ls_filtri += "Progressivo Doc: " + string(ll_prog) + "; "
end if

if not isnull(ls_descrizione) and len(ls_descrizione) > 0 then
	ls_where += " AND (det_documenti.des_documento LIKE '%" + ls_descrizione + "%' "
	ls_where += " OR det_documenti.nome_documento LIKE '%" + ls_descrizione + "%') "
	ls_filtri += "Descrizione Doc: " + ls_descrizione + "; "
end if

if not isnull(ls_cod_tipo_lista) and len(ls_cod_tipo_lista) > 0 then
	ls_where += " AND liste_dist_doc.cod_tipo_lista_dist='" + ls_cod_tipo_lista + "'"
	ls_filtri += "Tipo lista: " + ls_cod_tipo_lista + "; "
end if

if not isnull(ls_cod_lista) and len(ls_cod_lista) > 0 then
	ls_where += " AND liste_dist_doc.cod_lista_dist='" + ls_cod_lista + "'"
	ls_filtri += "Lista: " + ls_cod_lista + "; "
end if
// ----

if dw_report.SetSQLSelect(is_original_sql + ls_where) = -1 then
	g_mb.messagebox("Report Documenti", "Impossibile impostare il codice SQL alla datawindow")
	return -1
end if

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3566
parent.height = 1685

dw_report.show()

dw_report.setredraw(false)
dw_report.retrieve()

// -- Testi aggiuntivi DW
if len(ls_filtri) > 1 then dw_report.object.t_filtri.text = "Filtri =  " + ls_filtri

select rag_soc_1, rag_soc_2
into :ls_rag_soc_1, :ls_rag_soc_2
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode = 0 then
	if not isnull(ls_rag_soc_2) then
		ls_rag_soc_1 += " - " + ls_rag_soc_2
	end if
	
	dw_report.object.t_rag_soc_1.text = ls_rag_soc_1
end if

dw_report.setredraw(true)

cb_selezione.show()

dw_report.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_documenti_letture
event ue_imposta_dati ( long al_anno,  long al_num,  long al_progressivo )
integer x = 23
integer y = 20
integer width = 2011
integer height = 600
integer taborder = 20
string dataobject = "d_selezione_doc_letture"
borderstyle borderstyle = stylelowered!
end type

event ue_imposta_dati(long al_anno, long al_num, long al_progressivo);setitem(1, "anno_registrazione", al_anno)
setitem(1, "num_registrazione", al_num)
setitem(1, "progressivo_doc", al_progressivo)

cb_report.event post clicked()
end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case dwo.name
	case "cod_tipo_lista"
		if not isnull(data) or data <> "" then
			setitem(row, "cod_lista", ls_null)
			f_PO_LoadDDDW_DW(dw_selezione, "cod_lista", sqlca, &
				"tes_liste_dist", "cod_lista_dist", "des_lista_dist", "cod_azienda='" + s_cs_xx.cod_azienda +"' AND cod_tipo_lista_dist='"+data+"'")
		end if
end choose





end event

type dw_report from uo_cs_xx_dw within w_report_documenti_letture
integer x = 23
integer y = 20
integer width = 2011
integer height = 600
integer taborder = 10
string dataobject = "d_report_documenti_letture"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

