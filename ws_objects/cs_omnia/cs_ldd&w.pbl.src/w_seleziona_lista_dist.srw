﻿$PBExportHeader$w_seleziona_lista_dist.srw
$PBExportComments$Window seleziona_lista_dist
forward
global type w_seleziona_lista_dist from w_cs_xx_risposta
end type
type st_1 from statictext within w_seleziona_lista_dist
end type
type dw_seleziona_ld_lista from uo_cs_xx_dw within w_seleziona_lista_dist
end type
type st_2 from statictext within w_seleziona_lista_dist
end type
type dw_seleziona_ld_dettaglio from uo_cs_xx_dw within w_seleziona_lista_dist
end type
type cb_seleziona from commandbutton within w_seleziona_lista_dist
end type
end forward

global type w_seleziona_lista_dist from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 1618
integer height = 1676
string title = "Elenco Liste Distribuzione"
st_1 st_1
dw_seleziona_ld_lista dw_seleziona_ld_lista
st_2 st_2
dw_seleziona_ld_dettaglio dw_seleziona_ld_dettaglio
cb_seleziona cb_seleziona
end type
global w_seleziona_lista_dist w_seleziona_lista_dist

type variables

end variables

on w_seleziona_lista_dist.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_seleziona_ld_lista=create dw_seleziona_ld_lista
this.st_2=create st_2
this.dw_seleziona_ld_dettaglio=create dw_seleziona_ld_dettaglio
this.cb_seleziona=create cb_seleziona
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_seleziona_ld_lista
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.dw_seleziona_ld_dettaglio
this.Control[iCurrent+5]=this.cb_seleziona
end on

on w_seleziona_lista_dist.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_seleziona_ld_lista)
destroy(this.st_2)
destroy(this.dw_seleziona_ld_dettaglio)
destroy(this.cb_seleziona)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup)
dw_seleziona_ld_lista.set_dw_options(sqlca,pcca.null_object,c_nomodify + c_nonew + c_nodelete + c_default + c_disablecc +c_disableccinsert,c_default)
dw_seleziona_ld_dettaglio.set_dw_options(sqlca,pcca.null_object,c_nomodify + c_nonew + c_nodelete + c_default + c_disablecc +c_disableccinsert,c_default)

s_cs_xx.parametri.parametro_d_1 = -1
end event

type st_1 from statictext within w_seleziona_lista_dist
integer x = 9
integer y = 12
integer width = 1554
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Elenco Liste di Distribuzione Disponibili"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_seleziona_ld_lista from uo_cs_xx_dw within w_seleziona_lista_dist
event pcd_pickedrow pbm_custom55
event pcd_retrieve pbm_custom60
integer x = 18
integer y = 88
integer width = 1550
integer height = 400
integer taborder = 10
string title = "Elenco Liste di Distribuzione Disponibili"
string dataobject = "d_seleziona_ld_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_pickedrow;call super::pcd_pickedrow;dw_seleziona_ld_dettaglio.triggerevent("pcd_retrieve")
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type st_2 from statictext within w_seleziona_lista_dist
integer x = 23
integer y = 520
integer width = 1554
integer height = 72
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Destinatari della Lista selezionata sopra"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_seleziona_ld_dettaglio from uo_cs_xx_dw within w_seleziona_lista_dist
event pcd_retrieve pbm_custom60
integer x = 18
integer y = 600
integer width = 1550
integer height = 860
integer taborder = 20
string dataobject = "d_seleziona_ld_dettaglio"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_tipo_lista_dist,ls_cod_lista_dist

ls_cod_tipo_lista_dist=dw_seleziona_ld_lista.getitemstring(dw_seleziona_ld_lista.getrow(),"cod_tipo_lista_dist")
ls_cod_lista_dist=dw_seleziona_ld_lista.getitemstring(dw_seleziona_ld_lista.getrow(),"cod_lista_dist")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_tipo_lista_dist,ls_cod_lista_dist)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_seleziona from commandbutton within w_seleziona_lista_dist
integer x = 1202
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Seleziona"
end type

event clicked;string ls_cod_tipo_lista_dist,ls_cod_lista_dist,ls_cod_destinatario,ls_path_documento, ls_flag_blocco
long ll_num_sequenza,ll_progr_lista_dist

ls_cod_tipo_lista_dist = dw_seleziona_ld_lista.getitemstring(dw_seleziona_ld_lista.getrow(),"cod_tipo_lista_dist")
ls_cod_lista_dist      = dw_seleziona_ld_lista.getitemstring(dw_seleziona_ld_lista.getrow(),"cod_lista_dist")
ll_progr_lista_dist    = s_cs_xx.parametri.parametro_ul_2
ls_path_documento      = s_cs_xx.parametri.parametro_s_1

declare righe_det_liste_dist cursor for
select cod_destinatario,
		 num_sequenza
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda
and	 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist
and    cod_lista_dist = :ls_cod_lista_dist;

open righe_det_liste_dist;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA", "Errore durante l'apertura del cursore righe_det_liste_dist.~r~nDettaglio~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
  	fetch righe_det_liste_dist into :ls_cod_destinatario, :ll_num_sequenza;
	  
   if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA", "Errore durante la generazione della lista di distribuzione.~r~nDettaglio~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if

	
	select flag_blocco
	into   :ls_flag_blocco
	from   tab_ind_dest
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_destinatario = :ls_cod_destinatario;
	
	if ls_flag_blocco = "S" then continue
	
	INSERT INTO liste_dist_doc  
  			    ( cod_azienda,   
           		cod_tipo_lista_dist,   
		         cod_lista_dist,   
      	      cod_destinatario,   
          	   num_sequenza,   
	            progr_lista_dist,  
					cod_utente, 
    	         path_doc,   
 	            flag_ca,   
    	         data_comm )  
  VALUES    ( :s_cs_xx.cod_azienda,   
   	        :ls_cod_tipo_lista_dist,   
      	     :ls_cod_lista_dist,   
         	  :ls_cod_destinatario,   
	           :ll_num_sequenza,   
   	        :ll_progr_lista_dist,   
				  :s_cs_xx.cod_utente, 
      	     :ls_path_documento,   
         	  'N',   
	           null)  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore l'inserimento della lista di distribuzione del documento~r~nOperazione Annullata.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if

loop

close righe_det_liste_dist;

commit; 

s_cs_xx.parametri.parametro_d_1 = 0

close(parent)
end event

