﻿$PBExportHeader$w_selezione_liste.srw
forward
global type w_selezione_liste from w_cs_xx_risposta
end type
type dw_selezione_liste from datawindow within w_selezione_liste
end type
end forward

global type w_selezione_liste from w_cs_xx_risposta
integer width = 3054
integer height = 1548
string title = "Selezione Liste Distribuzione"
boolean resizable = false
dw_selezione_liste dw_selezione_liste
end type
global w_selezione_liste w_selezione_liste

type variables
string is_destinatario
end variables

forward prototypes
public function integer wf_carica_liste ()
end prototypes

public function integer wf_carica_liste ();long 	 ll_i, ll_riga, ll_count

string ls_sql, ls_errore, ls_cod_tipo, ls_des_tipo, ls_cod_lista, ls_des_lista

datastore lds_liste

ls_sql = &
"select " + &
"	a.cod_tipo_lista_dist, " + &
"	a.descrizione, " + &
"	b.cod_lista_dist, " + &
"	b.des_lista_dist " + &
"from " + &
"	tab_tipi_liste_dist a, " + &
"	tes_liste_dist b " + &
"where " + &
"	a.cod_tipo_lista_dist = b.cod_tipo_lista_dist " + &
"order by " + &
"	a.cod_tipo_lista_dist ASC, " + &
"	b.cod_lista_dist ASC"

setnull(ls_errore)

ls_sql = sqlca.syntaxfromsql(ls_sql,"style(type=grid)",ls_errore)

if ls_sql = "" and not isnull(ls_errore) then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	return -1
end if

lds_liste = create datastore

if lds_liste.create(ls_sql,ls_errore) = -1 then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	return -1
end if

if lds_liste.settransobject(sqlca) = -1 then
	g_mb.messagebox("OMNIA","Errore in impostazione transazione",stopsign!)
	return -1
end if

if lds_liste.retrieve() = -1 then
	g_mb.messagebox("OMNIA","Errore in retrieve dei dati",stopsign!)
	return -1
end if

dw_selezione_liste.reset()

for ll_i = 1 to lds_liste.rowcount()
	
	ls_cod_tipo = lds_liste.getitemstring(ll_i,1)
	
	ls_des_tipo = lds_liste.getitemstring(ll_i,2)
	
	ls_cod_lista = lds_liste.getitemstring(ll_i,3)
	
	ls_des_lista = lds_liste.getitemstring(ll_i,4)
	
	select
		count(*)
	into
		:ll_count
	from
		det_liste_dist
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_lista_dist = :ls_cod_tipo and
		cod_lista_dist = :ls_cod_lista and
		cod_destinatario = :is_destinatario;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in controllo presenza destinatario: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 or isnull(ll_count) then
		ll_count = 0
	end if
	
	ll_riga = dw_selezione_liste.insertrow(0)
	
	dw_selezione_liste.setitem(ll_riga,"cod_tipo",ls_cod_tipo)
	
	dw_selezione_liste.setitem(ll_riga,"des_tipo",ls_des_tipo)
	
	dw_selezione_liste.setitem(ll_riga,"cod_lista",ls_cod_lista)
	
	dw_selezione_liste.setitem(ll_riga,"des_lista",ls_des_lista)
	
	if ll_count > 0 then
		dw_selezione_liste.setitem(ll_riga,"incluso","S")
	else
		dw_selezione_liste.setitem(ll_riga,"incluso","N")
	end if
	
next

return 0
end function

on w_selezione_liste.create
int iCurrent
call super::create
this.dw_selezione_liste=create dw_selezione_liste
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione_liste
end on

on w_selezione_liste.destroy
call super::destroy
destroy(this.dw_selezione_liste)
end on

event pc_setwindow;call super::pc_setwindow;is_destinatario = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)

title = "Selezione Liste - Destinatario " + s_cs_xx.parametri.parametro_s_2

setnull(s_cs_xx.parametri.parametro_s_2)

setpointer(hourglass!)

wf_carica_liste()

setpointer(arrow!)
end event

type dw_selezione_liste from datawindow within w_selezione_liste
integer x = 23
integer y = 20
integer width = 2994
integer height = 1420
integer taborder = 10
string dataobject = "d_selezione_liste"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;long	 ll_sequenza

string ls_cod_tipo, ls_cod_lista


if dwo.name = "incluso" then
	
	ls_cod_tipo = getitemstring(row,"cod_tipo")
	
	ls_cod_lista = getitemstring(row,"cod_lista")
	
	if data = "N" then
		
		if g_mb.messagebox("OMNIA","Rimuovere il destinatario dalla lista selezionata?",question!,yesno!,2) = 2 then
			return 1
		end if
		
		delete from
			liste_dist_doc
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_lista_dist = :ls_cod_tipo and
			cod_lista_dist = :ls_cod_lista and
			cod_destinatario = :is_destinatario;
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in cancellazione documenti esterni: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 1
		end if
		
		delete from
			det_liste_dist
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_lista_dist = :ls_cod_tipo and
			cod_lista_dist = :ls_cod_lista and
			cod_destinatario = :is_destinatario;
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in cancellazione dettaglio lista: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 1
		end if
		
		commit;
		
	elseif data = "S" then
		
		insert into
			det_liste_dist
			(cod_azienda,
			cod_tipo_lista_dist,
			cod_lista_dist,
			cod_destinatario,
			num_sequenza,
			note)
		values
			(:s_cs_xx.cod_azienda,
			:ls_cod_tipo,
			:ls_cod_lista,
			:is_destinatario,
			1,
			null);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento destinatario: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 1
		end if
		
		commit;
		
	end if
		
end if
end event

