﻿$PBExportHeader$w_tab_ind_dest.srw
$PBExportComments$Window Rubrica Indirizzi Destinatari
forward
global type w_tab_ind_dest from w_cs_xx_principale
end type
type cb_importa from commandbutton within w_tab_ind_dest
end type
type dw_ind_dest_lista from uo_cs_xx_dw within w_tab_ind_dest
end type
type dw_d_ind_dest_dett from uo_cs_xx_dw within w_tab_ind_dest
end type
type cb_1 from commandbutton within w_tab_ind_dest
end type
type cb_2 from commandbutton within w_tab_ind_dest
end type
type cb_3 from commandbutton within w_tab_ind_dest
end type
end forward

global type w_tab_ind_dest from w_cs_xx_principale
integer width = 2217
integer height = 1312
string title = "Rubrica Destinatari"
cb_importa cb_importa
dw_ind_dest_lista dw_ind_dest_lista
dw_d_ind_dest_dett dw_d_ind_dest_dett
cb_1 cb_1
cb_2 cb_2
cb_3 cb_3
end type
global w_tab_ind_dest w_tab_ind_dest

event pc_setwindow;call super::pc_setwindow;dw_ind_dest_lista.set_dw_key("cod_azienda")
dw_ind_dest_lista.set_dw_options(sqlca,pcca.null_object,c_multiselect + c_default,c_default)
dw_d_ind_dest_dett.set_dw_options(sqlca,dw_ind_dest_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_ind_dest_lista
end event

on w_tab_ind_dest.create
int iCurrent
call super::create
this.cb_importa=create cb_importa
this.dw_ind_dest_lista=create dw_ind_dest_lista
this.dw_d_ind_dest_dett=create dw_d_ind_dest_dett
this.cb_1=create cb_1
this.cb_2=create cb_2
this.cb_3=create cb_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_importa
this.Control[iCurrent+2]=this.dw_ind_dest_lista
this.Control[iCurrent+3]=this.dw_d_ind_dest_dett
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.cb_2
this.Control[iCurrent+6]=this.cb_3
end on

on w_tab_ind_dest.destroy
call super::destroy
destroy(this.cb_importa)
destroy(this.dw_ind_dest_lista)
destroy(this.dw_d_ind_dest_dett)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.cb_3)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_d_ind_dest_dett, &
                 "cod_utente", &
					  sqlca, &
                 "utenti", &
					  "cod_utente", &
					  "nome_cognome", &
					  "")
end event

type cb_importa from commandbutton within w_tab_ind_dest
integer x = 23
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;window_open(w_importa_dest,0)

parent.triggerevent("pc_retrieve")
end event

type dw_ind_dest_lista from uo_cs_xx_dw within w_tab_ind_dest
integer x = 23
integer y = 20
integer width = 2126
integer height = 480
integer taborder = 10
string dataobject = "d_ind_dest_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type dw_d_ind_dest_dett from uo_cs_xx_dw within w_tab_ind_dest
integer x = 23
integer y = 520
integer width = 2126
integer height = 580
integer taborder = 20
string dataobject = "d_ind_dest_dett"
borderstyle borderstyle = styleraised!
end type

type cb_1 from commandbutton within w_tab_ind_dest
integer x = 1783
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "P.O.Box "
end type

event clicked;//if s_cs_xx.num_livello_mail>0 then
//	s_cs_xx.mail.uf_elenco_indirizzi()
//else
//	messagebox("Omnia","Per attivare la posta elettronica impostare correttamente il parametro mail del file INI",exclamation!)
//end if

window_open(w_importa_rubrica,-1)
end event

type cb_2 from commandbutton within w_tab_ind_dest
integer x = 1394
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Agg.Clien."
end type

event clicked;window_open(w_aggiungi_clienti,0)
dw_ind_dest_lista.setfocus()
dw_ind_dest_lista.triggerevent("pcd_retrieve")
end event

type cb_3 from commandbutton within w_tab_ind_dest
integer x = 1006
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 23
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Agg.Forn."
end type

event clicked;window_open(w_aggiungi_fornitori,0)
dw_ind_dest_lista.setfocus()
dw_ind_dest_lista.triggerevent("pcd_retrieve")
end event

