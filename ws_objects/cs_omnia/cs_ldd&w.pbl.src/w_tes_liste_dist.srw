﻿$PBExportHeader$w_tes_liste_dist.srw
$PBExportComments$Window Testata Liste Distribuzione
forward
global type w_tes_liste_dist from w_cs_xx_principale
end type
type dw_tes_liste_dist_lista from uo_cs_xx_dw within w_tes_liste_dist
end type
type dw_tes_liste_dist_dett from uo_cs_xx_dw within w_tes_liste_dist
end type
type cb_dettaglio from commandbutton within w_tes_liste_dist
end type
end forward

global type w_tes_liste_dist from w_cs_xx_principale
integer width = 1911
integer height = 1084
string title = "Liste Distribuzione"
dw_tes_liste_dist_lista dw_tes_liste_dist_lista
dw_tes_liste_dist_dett dw_tes_liste_dist_dett
cb_dettaglio cb_dettaglio
end type
global w_tes_liste_dist w_tes_liste_dist

event pc_setwindow;call super::pc_setwindow;dw_tes_liste_dist_lista.set_dw_key("cod_azienda")
dw_tes_liste_dist_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_liste_dist_dett.set_dw_options(sqlca,dw_tes_liste_dist_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_liste_dist_lista
end event

on w_tes_liste_dist.create
int iCurrent
call super::create
this.dw_tes_liste_dist_lista=create dw_tes_liste_dist_lista
this.dw_tes_liste_dist_dett=create dw_tes_liste_dist_dett
this.cb_dettaglio=create cb_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_liste_dist_lista
this.Control[iCurrent+2]=this.dw_tes_liste_dist_dett
this.Control[iCurrent+3]=this.cb_dettaglio
end on

on w_tes_liste_dist.destroy
call super::destroy
destroy(this.dw_tes_liste_dist_lista)
destroy(this.dw_tes_liste_dist_dett)
destroy(this.cb_dettaglio)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_liste_dist_dett,"cod_tipo_lista_dist",sqlca,&
                 "tab_tipi_liste_dist","cod_tipo_lista_dist","descrizione","")

end event

event close;call super::close;close(w_dett_liste_dist)
end event

type dw_tes_liste_dist_lista from uo_cs_xx_dw within w_tes_liste_dist
integer x = 23
integer y = 20
integer width = 1829
integer height = 480
integer taborder = 20
string dataobject = "d_tes_liste_dist_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event updatestart;call super::updatestart;if i_extendmode then
	
	long ll_i
	string ls_cod_tipo_lista_dist
	string ls_cod_lista_dist

	for ll_i = 1 to this.deletedcount()
		ls_cod_tipo_lista_dist = this.getitemstring(ll_i, "cod_tipo_lista_dist", delete!, true)
		ls_cod_lista_dist = this.getitemstring(ll_i, "cod_lista_dist", delete!, true)

		delete from det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda
		and 	 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist
		and 	 cod_lista_dist = :ls_cod_lista_dist;

	next

end if 
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then	
	cb_dettaglio.enabled=false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then	
	cb_dettaglio.enabled=false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then	
	cb_dettaglio.enabled=true
end if
end event

type dw_tes_liste_dist_dett from uo_cs_xx_dw within w_tes_liste_dist
integer x = 23
integer y = 520
integer width = 1829
integer height = 340
integer taborder = 30
string dataobject = "d_tes_liste_dist_dett"
borderstyle borderstyle = styleraised!
end type

type cb_dettaglio from commandbutton within w_tes_liste_dist
integer x = 1486
integer y = 880
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;window_open_parm(w_dett_liste_dist,-1,dw_tes_liste_dist_lista)
end event

