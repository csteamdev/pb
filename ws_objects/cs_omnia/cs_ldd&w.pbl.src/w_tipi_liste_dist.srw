﻿$PBExportHeader$w_tipi_liste_dist.srw
$PBExportComments$Window Tipi Liste Dist
forward
global type w_tipi_liste_dist from w_cs_xx_principale
end type
type dw_tipi_liste_dist_lista from uo_cs_xx_dw within w_tipi_liste_dist
end type
type dw_tipi_liste_dist_dett from uo_cs_xx_dw within w_tipi_liste_dist
end type
end forward

global type w_tipi_liste_dist from w_cs_xx_principale
integer width = 1696
integer height = 936
string title = "Tipi Liste Distribuzione"
dw_tipi_liste_dist_lista dw_tipi_liste_dist_lista
dw_tipi_liste_dist_dett dw_tipi_liste_dist_dett
end type
global w_tipi_liste_dist w_tipi_liste_dist

event pc_setwindow;call super::pc_setwindow;dw_tipi_liste_dist_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tipi_liste_dist_dett.set_dw_options(sqlca,dw_tipi_liste_dist_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tipi_liste_dist_lista
end event

on w_tipi_liste_dist.create
int iCurrent
call super::create
this.dw_tipi_liste_dist_lista=create dw_tipi_liste_dist_lista
this.dw_tipi_liste_dist_dett=create dw_tipi_liste_dist_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_liste_dist_lista
this.Control[iCurrent+2]=this.dw_tipi_liste_dist_dett
end on

on w_tipi_liste_dist.destroy
call super::destroy
destroy(this.dw_tipi_liste_dist_lista)
destroy(this.dw_tipi_liste_dist_dett)
end on

type dw_tipi_liste_dist_lista from uo_cs_xx_dw within w_tipi_liste_dist
integer x = 23
integer y = 20
integer width = 1623
integer height = 480
string dataobject = "d_tipi_liste_dist_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_tipi_liste_dist_dett from uo_cs_xx_dw within w_tipi_liste_dist
integer x = 23
integer y = 520
integer width = 1623
integer height = 300
integer taborder = 2
string dataobject = "d_tipi_liste_dist_dett"
borderstyle borderstyle = styleraised!
end type

