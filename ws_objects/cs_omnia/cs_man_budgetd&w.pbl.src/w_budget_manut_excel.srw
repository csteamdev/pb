﻿$PBExportHeader$w_budget_manut_excel.srw
forward
global type w_budget_manut_excel from w_cs_xx_principale
end type
type dw_selezione_budget_excel from uo_cs_xx_dw within w_budget_manut_excel
end type
type cb_1 from commandbutton within w_budget_manut_excel
end type
end forward

global type w_budget_manut_excel from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2057
integer height = 952
string title = "BUDGET MANUTENZIONI EXCEL"
dw_selezione_budget_excel dw_selezione_budget_excel
cb_1 cb_1
end type
global w_budget_manut_excel w_budget_manut_excel

on w_budget_manut_excel.create
int iCurrent
call super::create
this.dw_selezione_budget_excel=create dw_selezione_budget_excel
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione_budget_excel
this.Control[iCurrent+2]=this.cb_1
end on

on w_budget_manut_excel.destroy
call super::destroy
destroy(this.dw_selezione_budget_excel)
destroy(this.cb_1)
end on

event open;call super::open;//dw_selezione_budget_excel.insertrow(0)
dw_selezione_budget_excel.scrolltorow(1)
dw_selezione_budget_excel.setfocus()
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)
								 


dw_selezione_budget_excel.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC +&
									 c_TriggerRefresh, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_selezione_budget_excel



end event

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_selezione_budget_excel, &
//                 "tes_budget", &
//                 sqlca, &
//					  "tes_budget_manutenzioni", &
//                 "progressivo", &
//					  "cod_budget +' - '+ des_budget +' [rev.'+ string(num_revisione) +']'", &	
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' ")

f_po_loaddddw_dw(dw_selezione_budget_excel, &
                 "tes_budget", &
                 sqlca, &
					  "tes_budget_manutenzioni", &
                 "progressivo", &
					  "cod_budget +' - '+ des_budget +' [rev.'+ str(num_revisione) +']'", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' ")
end event

type dw_selezione_budget_excel from uo_cs_xx_dw within w_budget_manut_excel
integer x = 18
integer width = 1989
integer height = 740
integer taborder = 10
string dataobject = "d_selezione_budget_manut_excel"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;
string  ls_flag_tipo_periodo, ls_periodo
long ll_num_revisione, ll_num_periodi, ll_progressivo
datetime ldt_data_elaborazione, ldt_da_data, ldt_a_data,ldt_data_consolidato

if i_extendmode then
	
	choose case i_colname
			
		case "tes_budget"
			
			dw_selezione_budget_excel.AcceptText()
			ll_progressivo = dw_selezione_budget_excel.getitemnumber(1, "tes_budget")
			
			select data_consolidato, num_revisione, flag_tipo_periodo, num_periodi, data_elaborazione
			into :ldt_data_consolidato, :ll_num_revisione, :ls_flag_tipo_periodo, :ll_num_periodi, :ldt_data_elaborazione
			from tes_budget_manutenzioni
			where cod_azienda =:s_cs_xx.cod_azienda and
			progressivo = :ll_progressivo;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
				return
			end if
			select max(data_fine), min(data_inizio)
			into :ldt_a_data, :ldt_da_data
			from det_budget_manut_periodi
			where cod_azienda =:s_cs_xx.cod_azienda and
			progressivo = :ll_progressivo;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
				return
			end if	
		
		 dw_selezione_budget_excel.setitem(1, "da_data", ldt_da_data)
		 dw_selezione_budget_excel.setitem(1, "a_data", ldt_a_data)
		 dw_selezione_budget_excel.setitem(1, "revisione", ll_num_revisione)
		 dw_selezione_budget_excel.setitem(1, "data_elab", ldt_data_elaborazione)
		 dw_selezione_budget_excel.setitem(1, "data_cons", ldt_data_consolidato)
		
		ls_periodo = "Il periodo è stato suddiviso in "+string(ll_num_periodi)+" " 
		choose case ls_flag_tipo_periodo
			case 'T'
				ls_periodo += "Trimestri."
			case 'S'
				ls_periodo += "Settimane."
			case 'B'
				ls_periodo += "Bi-settimane."
			case 'E'
				ls_periodo += "Mesi."
			case 'I'
				ls_periodo += "Bimestri."
			case 'R'
				ls_periodo += "Semestri."
			case 'A'
				ls_periodo += "Anni."
				
		end choose
		
		dw_selezione_budget_excel.setitem(1, "periodi", ls_periodo)
	
		
		end choose
end if	




//select 
end event

type cb_1 from commandbutton within w_budget_manut_excel
integer x = 1623
integer y = 760
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;
long  ll_progressivo

uo_budget_manut_excel luo_budget
luo_budget = create uo_budget_manut_excel

ll_progressivo = dw_selezione_budget_excel.getitemnumber(1, "tes_budget")
//ll_progressivo = 10

luo_budget.uof_crea_foglio(ll_progressivo)


destroy luo_budget
end event

