﻿$PBExportHeader$w_budget_manut_periodi_excel.srw
$PBExportComments$visualizza il foglio excel in base al periodo richiesto e calcola il totale
forward
global type w_budget_manut_periodi_excel from w_cs_xx_principale
end type
type dw_selezione_budget_excel from uo_cs_xx_dw within w_budget_manut_periodi_excel
end type
type cb_1 from commandbutton within w_budget_manut_periodi_excel
end type
end forward

global type w_budget_manut_periodi_excel from w_cs_xx_principale
integer width = 2176
integer height = 1140
string title = "BUDGET MANUTENZIONI PERIODI EXCEL"
dw_selezione_budget_excel dw_selezione_budget_excel
cb_1 cb_1
end type
global w_budget_manut_periodi_excel w_budget_manut_periodi_excel

type variables
DataWindowchild id_child_periodo

end variables

on w_budget_manut_periodi_excel.create
int iCurrent
call super::create
this.dw_selezione_budget_excel=create dw_selezione_budget_excel
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione_budget_excel
this.Control[iCurrent+2]=this.cb_1
end on

on w_budget_manut_periodi_excel.destroy
call super::destroy
destroy(this.dw_selezione_budget_excel)
destroy(this.cb_1)
end on

event open;call super::open;//dw_selezione_budget_excel.insertrow(0)


//id_child_periodo = create DataWindowchild
dw_selezione_budget_excel.scrolltorow(1)
dw_selezione_budget_excel.setfocus()
dw_selezione_budget_excel.GetChild( "num_periodo",  ref id_child_periodo) //dw_selezione_budget_excel.object.num_periodo.d_dddw_number)


end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)
								 


dw_selezione_budget_excel.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC +&
									 c_TriggerRefresh, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_selezione_budget_excel



end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione_budget_excel, &
                 "tes_budget", &
                 sqlca, &
					  "tes_budget_manutenzioni", &
                 "progressivo", &
	 				  "cod_budget +' - '+des_budget +' [rev.'+ str(num_revisione) +']'", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' ")
					  
   
//		 				  "cod_budget +' - '+des_budget +' [rev.' + num_revisione +']'", &	

end event

type dw_selezione_budget_excel from uo_cs_xx_dw within w_budget_manut_periodi_excel
integer width = 2121
integer height = 932
integer taborder = 10
string dataobject = "d_selezione_budget_manut_periodi_excel"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;
string  ls_flag_tipo_periodo, ls_periodo
long ll_num_revisione, ll_num_periodi, ll_progressivo, ll_i
datetime ldt_data_elaborazione, ldt_da_data, ldt_a_data,ldt_data_consolidato

if i_extendmode then
	
	choose case i_colname
			
		case "tes_budget"
			
			dw_selezione_budget_excel.AcceptText()
			ll_progressivo = dw_selezione_budget_excel.getitemnumber(1, "tes_budget")
			
			select data_consolidato, num_revisione, flag_tipo_periodo, num_periodi, data_elaborazione
			into :ldt_data_consolidato, :ll_num_revisione, :ls_flag_tipo_periodo, :ll_num_periodi, :ldt_data_elaborazione
			from tes_budget_manutenzioni
			where cod_azienda =:s_cs_xx.cod_azienda and
			progressivo = :ll_progressivo;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
				return
			end if
			select max(data_fine), min(data_inizio)
			into :ldt_a_data, :ldt_da_data
			from det_budget_manut_periodi
			where cod_azienda =:s_cs_xx.cod_azienda and
			progressivo = :ll_progressivo;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
				return
			end if	
		
		 dw_selezione_budget_excel.setitem(1, "da_data", ldt_da_data)
		 dw_selezione_budget_excel.setitem(1, "a_data", ldt_a_data)
		 dw_selezione_budget_excel.setitem(1, "revisione", ll_num_revisione)
		 dw_selezione_budget_excel.setitem(1, "data_elab", ldt_data_elaborazione)
		 dw_selezione_budget_excel.setitem(1, "data_cons", ldt_data_consolidato)
		
		ls_periodo = "Il periodo è stato suddiviso in "+string(ll_num_periodi)+" " 
		choose case ls_flag_tipo_periodo
			case 'T'
				ls_periodo += "Trimestri."
			case 'S'
				ls_periodo += "Settimane."
			case 'B'
				ls_periodo += "Bi-settimane."
			case 'E'
				ls_periodo += "Mesi."
			case 'I'
				ls_periodo += "Bimestri."
			case 'R'
				ls_periodo += "Semestri."
			case 'A'
				ls_periodo += "Anni."
				
		end choose
		
		dw_selezione_budget_excel.setitem(1, "periodi", ls_periodo)
		id_child_periodo.reset()
		id_child_periodo.insertrow(1)
		id_child_periodo.setitem(ll_i, "data_column", ll_i - 1)
		
		for ll_i = 2 to ll_num_periodi + 1 
			//w_budget_manut_periodi_excel.object.periodo.AddItem(ll_i)
			
			id_child_periodo.insertrow(ll_i)
			id_child_periodo.setitem(ll_i, "data_column", ll_i - 1)
			id_child_periodo.setitem(ll_i, "display_column", "periodo "+string(ll_i - 1))
			
		//	dw_selezione_budget_excel.object.num_periodo.InsertItem(string(ll_i), 1)
		next
	

	
	
	end choose
end if	




//select 
end event

type cb_1 from commandbutton within w_budget_manut_periodi_excel
integer x = 1746
integer y = 944
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;
long  ll_progressivo
long ll_periodo
string ls_totale
date ld_data_elab
//periodo = 2
//totale = true
ll_periodo = 1
ls_totale = "N"
uo_budget_manut_excel luo_budget
luo_budget = create uo_budget_manut_excel

ll_progressivo = dw_selezione_budget_excel.getitemnumber(1, "tes_budget")
//ll_progressivo = 10
//ll_periodo = id_child_periodo.getitemnumber(id_child_periodo.getrow(),"data_column")
ll_periodo = dw_selezione_budget_excel.getitemnumber(1, "num_periodo")
ls_totale = dw_selezione_budget_excel.getitemstring(1, "flag_totale")
ld_data_elab = dw_selezione_budget_excel.getitemdate(1, "data_elab")

if isnull(ld_data_elab) and ls_totale = 'S' and isnull(ll_periodo) then
	g_mb.messagebox("OMNIA","Se il budget non è elaborato non è possibile creare il foglio excel!")
	return
end if

if isnull(ll_periodo) and (isnull(ls_totale) or ls_totale = 'N')   then
	g_mb.messagebox("OMNIA","Scegliere almeno un periodo oppure il totale!")
	return
end if
if  isnull(ll_periodo) then ll_periodo = 0
if  isnull(ls_totale) then ls_totale = 'N'
luo_budget.uof_creafoglio_periodo(ll_progressivo, ll_periodo, ls_totale)


destroy luo_budget


end event

