﻿$PBExportHeader$w_budget_manutenzioni.srw
$PBExportComments$Finestra Controllo e Lancio Budget
forward
global type w_budget_manutenzioni from w_cs_xx_principale
end type
type dw_selezione from datawindow within w_budget_manutenzioni
end type
type cb_esegui from commandbutton within w_budget_manutenzioni
end type
type dw_report from datawindow within w_budget_manutenzioni
end type
type cb_visualizzazione from commandbutton within w_budget_manutenzioni
end type
type st_1 from statictext within w_budget_manutenzioni
end type
type st_2 from statictext within w_budget_manutenzioni
end type
type st_3 from statictext within w_budget_manutenzioni
end type
type st_4 from statictext within w_budget_manutenzioni
end type
type st_5 from statictext within w_budget_manutenzioni
end type
type st_6 from statictext within w_budget_manutenzioni
end type
type st_7 from statictext within w_budget_manutenzioni
end type
end forward

global type w_budget_manutenzioni from w_cs_xx_principale
int Width=2643
int Height=1853
boolean TitleBar=true
string Title="BUDGET MANUTENZIONI"
dw_selezione dw_selezione
cb_esegui cb_esegui
dw_report dw_report
cb_visualizzazione cb_visualizzazione
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
st_5 st_5
st_6 st_6
st_7 st_7
end type
global w_budget_manutenzioni w_budget_manutenzioni

on w_budget_manutenzioni.create
int iCurrent
call w_cs_xx_principale::create
this.dw_selezione=create dw_selezione
this.cb_esegui=create cb_esegui
this.dw_report=create dw_report
this.cb_visualizzazione=create cb_visualizzazione
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.st_5=create st_5
this.st_6=create st_6
this.st_7=create st_7
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_selezione
this.Control[iCurrent+2]=cb_esegui
this.Control[iCurrent+3]=dw_report
this.Control[iCurrent+4]=cb_visualizzazione
this.Control[iCurrent+5]=st_1
this.Control[iCurrent+6]=st_2
this.Control[iCurrent+7]=st_3
this.Control[iCurrent+8]=st_4
this.Control[iCurrent+9]=st_5
this.Control[iCurrent+10]=st_6
this.Control[iCurrent+11]=st_7
end on

on w_budget_manutenzioni.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_selezione)
destroy(this.cb_esegui)
destroy(this.dw_report)
destroy(this.cb_visualizzazione)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.st_7)
end on

event open;call super::open;dw_selezione.insertrow(0)

dw_selezione.setfocus()
dw_selezione.setcolumn("anno_da")
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_area_aziendale", &
                 sqlca, &
					  "tab_aree_aziendali", &
                 "cod_area_aziendale", &
					  "des_area", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_divisione", &
                 sqlca, &
					  "anag_divisioni", &
                 "cod_divisione", &
					  "des_divisione", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_reparto", &
                 sqlca, &
					  "anag_reparti", &
                 "cod_reparto", &
					  "des_reparto", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")							  
end event

type dw_selezione from datawindow within w_budget_manutenzioni
int Y=1081
int Width=2081
int Height=381
int TabOrder=30
boolean BringToTop=true
string DataObject="d_selezione_budget"
boolean Border=false
boolean LiveScroll=true
end type

type cb_esegui from commandbutton within w_budget_manutenzioni
int X=2218
int Y=1081
int Width=366
int Height=81
int TabOrder=40
string Text="Elabora"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_cod_ricambio
string ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna
long ll_anno_registrazione, ll_num_registrazione
double ll_risorsa_ore_previste, ll_risorsa_minuti_previsti,ll_anno_reg_man,ll_num_reg_man, ll_operaio_ore_previste, ll_operaio_minuti_previsti
decimal ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario
decimal ld_risorsa_costi_aggiuntivi, ld_tot_costo_intervento
integer li_anno, li_count_att

datetime ldt_data_registrazione, ldt_prima_data, ld_a_dt
string ls_comodo_attrezzatura, ls_max_cod_attrezzatura, ls_tabella_fine, ls_comodo_attrezzatura2
date ld_da, ld_a
string ls_periodicita, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_divisione, ls_comodo_anno
integer li_frequenza
long ll_progressivo_num_reg
uo_budget_manutenzioni  luo_man

integer li_num_periodi, li_lunghezza_periodo, li_contatore, sw_prossimo_periodo
double ll_quan_ricambio, ll_costo_unitario_ricambio, ll_costo_orario_operaio		
long ll_progressivo_attrezzatura
string ls_cod_centro_costo, ls_flag_ordinario
double ll_risorsa_costo_orario, ll_risorsa_costi_aggiuntivi
double ll_costo_prev_ricambi, ll_costo_prev_ricambi_str, ll_costo_prev_risorse_int, ll_costo_prev_risorse_int_str
double ll_costo_prev_risorse_est, ll_costo_prev_risorse_est_str
double ld_costo_prev_ricambi, ld_costo_prev_risorse_int, ld_costo_prev_risorse_est
datetime ld_da_dt, ld_da_dt2

double ll_costo_cons_ricambi, ll_costo_cons_ricambi_str, ll_costo_cons_risorse_int, ll_costo_cons_risorse_int_str
double ll_costo_cons_risorse_est, ll_costo_cons_risorse_est_str, ll_max_progr_attrezzature

double ll_costo_tot_ricambi, ll_costo_tot_ricambi_str, ll_costo_tot_risorse_int, ll_costo_tot_risorse_int_str
double ll_costo_tot_risorse_est, ll_costo_tot_risorse_est_str
double ll_operaio_ore, ll_operaio_minuti, ll_risorsa_ore, ll_risorsa_minuti
//double ldd_percentuale_ricambi, ldd_percentuale_ris_int, ldd_percentuale_ris_est
double ldd_percentuale, ldd_trend
double ldd_somma_prev, ldd_somma_cons, ldd_differenza_c_p
integer li_contatore_percentuale, ls_sw_prima_volta, sw_ok
string ls_stringa_num_periodo, ls_comodo_attrezzatura_3

datetime ldt_comodo_d_r
string ls_comodo_c_a

string ciccio 


SetPointer(HourGlass!)

select num_periodi 
  into :li_num_periodi
  from con_budget_manutenzioni;

if sqlca.sqlcode = 100 then
	insert into con_budget_manutenzioni (cod_azienda, num_periodi)
		  values (:s_cs_xx.cod_azienda, 12);
end if		  


st_7.text = "Caricamento tabella MANUTENZIONI_SIMULATE in corso"
//////////////////////////////////////////////////////////////////////////////////////// 
 declare cu_prog_manutenzioni cursor for
  select anno_registrazione, 
  			num_registrazione,
         cod_attrezzatura,
			cod_tipo_manutenzione,
			flag_tipo_intervento,
			periodicita,
			frequenza,
			cod_ricambio,
			ricambio_non_codificato,
			quan_ricambio,
			costo_unitario_ricambio,
			cod_operaio,
			costo_orario_operaio,
			operaio_ore_previste,
			operaio_minuti_previsti,
			cod_cat_risorse_esterne,
			cod_risorsa_esterna,
			risorsa_ore_previste,
			risorsa_minuti_previsti,
			risorsa_costo_orario,
			risorsa_costi_aggiuntivi,
			tot_costo_intervento
    from programmi_manutenzione
   where cod_azienda = :s_cs_xx.cod_azienda;
	
//reperire da dw_selezione data_da e data_a
ll_progressivo_num_reg = 1
li_count_att = 0

dw_selezione.accepttext()

li_anno = dw_selezione.getitemnumber(1, "anno_da")

ld_da = date("01/01/" + string(li_anno))
ld_a = date("31/12/" + string(li_anno))

ldt_data_registrazione = datetime(date("01/01/90"), time("00:00:00"))

open cu_prog_manutenzioni;

do while 1 = 1
	fetch cu_prog_manutenzioni into :ll_anno_registrazione,
											  :ll_num_registrazione,
											  :ls_cod_attrezzatura,
											  :ls_cod_tipo_manutenzione,
											  :ls_flag_tipo_intervento,
											  :ls_periodicita,
											  :li_frequenza,
											  :ls_cod_ricambio,
											  :ls_ricambio_non_codificato,
											  :ld_quan_ricambio,
											  :ld_costo_unitario_ricambio,
											  :ls_cod_operaio,
											  :ld_costo_orario_operaio,
											  :ll_operaio_ore_previste,
											  :ll_operaio_minuti_previsti,
											  :ls_cod_cat_risorse_esterne,
											  :ls_cod_risorsa_esterna,
											  :ll_risorsa_ore_previste,
											  :ll_risorsa_minuti_previsti,
											  :ld_risorsa_costo_orario,
											  :ld_risorsa_costi_aggiuntivi,
											  :ld_tot_costo_intervento;													

	if sqlca.sqlcode = 100 or sqlca.sqlcode < 0 then exit
   if sqlca.sqlcode = 0 then 
		select anno_registrazione, 
		       num_registrazione
		into   :ll_anno_reg_man,
		       :ll_num_reg_man
		from   manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_reg_programma = :ll_anno_registrazione and
				 num_reg_programma = :ll_num_registrazione
		group by anno_registrazione, 
		       num_registrazione
		having anno_registrazione = max(anno_registrazione) and
		       num_registrazione = max(num_registrazione);
				
		if sqlca.sqlcode = 0 or sqlca.sqlcode = -1 then
			select data_registrazione 
			  into :ldt_data_registrazione
			  from manutenzioni
			 where cod_azienda = :s_cs_xx.cod_azienda and 
			       anno_registrazione = :ll_anno_reg_man and
					 num_registrazione = :ll_num_reg_man;
			if sqlca.sqlcode < 0 then exit
		end if
		if sqlca.sqlcode < 0 then exit

		if ldt_data_registrazione = datetime(date("01/01/90"), time("00:00:00")) then
			ldt_data_registrazione = datetime(ld_da, time("00:00:00"))
		end if		

/////////////////////////////////////////////////////////////////////////////////////////		
		ls_comodo_anno = string(date(ldt_data_registrazione), "ddmmyyyy")		
		ll_anno_registrazione = long(mid(ls_comodo_anno, 5,4))
		
		if ll_anno_registrazione < 1950 then
			goto ProssimoRecord
		end if
/////////////////////////////////////////////////////////////////////////////////////////		
			
		if ldt_data_registrazione < datetime(ld_da, time("00:00:00")) then
			//Passaggio dati a funzione per restituzione prima data valida
			luo_man = create uo_budget_manutenzioni
				ldt_prima_data = luo_man.uof_prossima_scadenza(ldt_data_registrazione, ld_da, ld_a, ls_periodicita, li_frequenza)
			destroy luo_man
			//restituisce ldt_prima_data
		else 
			if ldt_data_registrazione <= datetime(ld_a, time("23:59:59")) then
				ldt_prima_data = ldt_data_registrazione
			else
				goto ProssimoRecord
			end if	
	   end if
		
		ls_comodo_anno = string(date(ldt_prima_data), "ddmmyyyy")		
		ll_anno_registrazione = long(mid(ls_comodo_anno, 5,4))
		
		select count(cod_attrezzatura)
		  into :li_count_att
		  from manutenzioni_simulate
		 where cod_attrezzatura = :ls_cod_attrezzatura and
		       data_registrazione = :ldt_prima_data;
		if sqlca.sqlcode < 0 then exit		
		if li_count_att = 0 then 
			do while ldt_prima_data < datetime(ld_a, time("23:59:59"))   
				insert into  manutenzioni_simulate
					  values (:s_cs_xx.cod_azienda,
								 :ll_anno_registrazione,
								 :ll_progressivo_num_reg,
								 :ldt_prima_data,
								 :ls_cod_attrezzatura,
								 :ls_cod_tipo_manutenzione,
								 :ls_flag_tipo_intervento,
								 :ls_cod_ricambio,
								 :ls_ricambio_non_codificato,
								 :ld_quan_ricambio,
								 :ld_costo_unitario_ricambio,
								 :ls_cod_operaio,
								 :ld_costo_orario_operaio,
								 :ll_operaio_ore_previste,
								 :ll_operaio_minuti_previsti,
								 :ls_cod_cat_risorse_esterne,
								 :ls_cod_risorsa_esterna,
								 :ll_risorsa_ore_previste,
								 :ll_risorsa_minuti_previsti,
								 :ld_risorsa_costo_orario,
								 :ld_risorsa_costi_aggiuntivi,
								 :ld_tot_costo_intervento);
				
				luo_man = create uo_budget_manutenzioni
					ldt_prima_data = luo_man.uof_calcolo_p_s(ldt_prima_data, ls_periodicita, li_frequenza)
				destroy luo_man
		
				ll_progressivo_num_reg = ll_progressivo_num_reg + 1
	
				ls_comodo_anno = string(date(ldt_prima_data), "ddmmyyyy")		
				ll_anno_registrazione = long(mid(ls_comodo_anno, 5,4))	
			loop
		end if
	end if	
ProssimoRecord:
li_count_att = 0
loop
close cu_prog_manutenzioni;
commit;

/////////////////////////////////////////////////////////////////////////////////////////////
//Fine prima parte
/////////////////////////////////////////////////////////////////////////////////////////////
st_7.text = "Caricamento tabella BUDGET_MANUTENZIONI parte preventivo"

ll_costo_tot_ricambi = 0
ll_costo_tot_ricambi_str = 0
ll_costo_tot_risorse_int = 0
ll_costo_tot_risorse_int_str = 0
ll_costo_tot_risorse_est = 0
ll_costo_tot_risorse_est_str = 0	

ls_comodo_attrezzatura_3 = ""

ls_comodo_c_a = ""

ls_comodo_attrezzatura = 'AAAAAA'

ll_progressivo_attrezzatura = 1

delete from budget_manutenzioni;

select num_periodi 
  into :li_num_periodi
  from con_budget_manutenzioni;

//if sqlca.sqlcode < 0 or sqlca.sqlcode = 100 then goto FinePreventivo

if sqlca.sqlcode = 0 then
	li_lunghezza_periodo = int(365 / li_num_periodi)
end if 

li_contatore = 1

do while li_contatore <= li_num_periodi
	
	sw_ok = 0
	
	ld_a = relativedate(ld_da, li_lunghezza_periodo)	
	ld_da_dt = datetime(ld_da, time("00:00:00"))
	ld_a_dt = datetime(ld_a, time("23:59:59"))
	
	select min(cod_attrezzatura)
	  into :ls_comodo_attrezzatura
	  from manutenzioni_simulate
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and data_registrazione <= :ld_a_dt
		and data_registrazione >= :ld_da_dt;
		
	select min(data_registrazione)
	  into :ldt_data_registrazione
	  from manutenzioni_simulate
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and data_registrazione <= :ld_a_dt
		and data_registrazione >= :ld_da_dt
		and cod_attrezzatura = :ls_comodo_attrezzatura;		
			 
    select data_registrazione,
	 		  cod_attrezzatura,
 			  quan_ricambio,
			  costo_unitario_ricambio,
			  costo_orario_operaio,
			  operaio_ore_previste,
			  operaio_minuti_previsti,
			  risorsa_ore_previste,
			  risorsa_minuti_previsti, 
			  risorsa_costo_orario,
			  risorsa_costi_aggiuntivi
		into :ldt_data_registrazione,
			  :ls_cod_attrezzatura,
			  :ll_quan_ricambio,
			  :ll_costo_unitario_ricambio,
			  :ll_costo_orario_operaio,
			  :ll_operaio_ore_previste,
			  :ll_operaio_minuti_previsti,
			  :ll_risorsa_ore_previste,
			  :ll_risorsa_minuti_previsti, 
			  :ll_risorsa_costo_orario,
			  :ll_risorsa_costi_aggiuntivi					  
	   from manutenzioni_simulate
	  where cod_azienda = :s_cs_xx.cod_azienda and
	        data_registrazione = 	(select min(data_registrazione)
	  										   from manutenzioni_simulate
	 										  where cod_azienda = :s_cs_xx.cod_azienda
	   										 and data_registrazione <= :ld_a_dt
												 and data_registrazione >= :ld_da_dt
												 and cod_attrezzatura = :ls_comodo_attrezzatura) and
			  cod_attrezzatura = :ls_comodo_attrezzatura;		 
			 
ciccio =	sqlca.sqlerrtext + " " + string(sqlca.sqlcode)				  			 
			 
	select max(cod_attrezzatura)
	  into :ls_max_cod_attrezzatura
	  from manutenzioni_simulate
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and data_registrazione <= :ld_a_dt
		and data_registrazione >= :ld_da_dt;	 
			 
   declare cu_manutenzioni_simulate cursor for
    select data_registrazione,
	 		  cod_attrezzatura,
 			  quan_ricambio,
			  costo_unitario_ricambio,
			  costo_orario_operaio,
			  operaio_ore_previste,
			  operaio_minuti_previsti,
			  risorsa_ore_previste,
			  risorsa_minuti_previsti, 
			  risorsa_costo_orario,
			  risorsa_costi_aggiuntivi 
	   from manutenzioni_simulate
	  where cod_azienda = :s_cs_xx.cod_azienda and
	        data_registrazione >= :ld_da_dt2 and
			  cod_attrezzatura >= :ls_comodo_attrezzatura
  order by cod_attrezzatura,
  			  data_registrazione;

//			  cod_attrezzatura >= :ls_comodo_attrezzatura

//	ld_da_dt2 = datetime(date(ldt_data_registrazione), time(relativetime(time(ldt_data_registrazione), 1)))
	ld_da_dt2 = ld_da_dt
	
	open cu_manutenzioni_simulate;	
	
	do while ls_comodo_attrezzatura <= ls_max_cod_attrezzatura
		
		if ls_cod_attrezzatura = ls_comodo_attrezzatura and ldt_data_registrazione >= ld_da_dt and ldt_data_registrazione <= ld_a_dt then
			
			select cod_area_aziendale, 
					 cod_reparto,
					 cod_centro_costo
			  into :ls_cod_area_aziendale,
			       :ls_cod_reparto,
					 :ls_cod_centro_costo
			  from anag_attrezzature
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_attrezzatura = :ls_cod_attrezzatura;
			if sqlca.sqlcode < 0 then exit
			if sqlca.sqlcode = 0 or sqlca.sqlcode = 100 then
				select cod_divisione
				  into :ls_cod_divisione
				  from tab_aree_aziendali
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_area_aziendale = :ls_cod_area_aziendale;
				if sqlca.sqlcode < 0 then exit
				if sqlca.sqlcode = 0 or sqlca.sqlcode = 100 then
						if ldt_comodo_d_r <> ldt_data_registrazione or ls_comodo_c_a <> ls_cod_attrezzatura then
						
							ll_costo_tot_ricambi = ll_costo_tot_ricambi + (ll_quan_ricambio * ll_costo_unitario_ricambio)
							ll_costo_tot_ricambi_str = 0
							ll_costo_tot_risorse_int = ll_costo_tot_risorse_int + (((ll_operaio_minuti_previsti / 60) + ll_operaio_ore_previste) * ll_costo_orario_operaio)
							ll_costo_tot_risorse_int_str = 0
							ll_costo_tot_risorse_est = ll_costo_tot_risorse_est + ((((ll_risorsa_minuti_previsti / 60) + ll_risorsa_ore_previste) * ll_risorsa_costo_orario) + ll_risorsa_costi_aggiuntivi)
							ll_costo_tot_risorse_est_str = 0
							
							ldt_comodo_d_r = ldt_data_registrazione
							ls_comodo_c_a = ls_cod_attrezzatura
						end if	
			   end if
			end if
		else
			
			if ls_comodo_attrezzatura_3 <> "" then
				ls_comodo_attrezzatura = ls_comodo_attrezzatura_3
			end if				
			
			insert into  budget_manutenzioni (cod_azienda, anno_budget, num_periodo, progr_attrezzatura, cod_divisione, cod_area_aziendale, cod_reparto, cod_attrezzatura, cod_centro_costo, costo_prev_ricambi, costo_prev_ricambi_str, costo_prev_risorse_int, costo_prev_risorse_int_str, costo_prev_risorse_est, costo_prev_risorse_est_str)
				  values (:s_cs_xx.cod_azienda,
							 :li_anno,
							 :li_contatore,
							 :ll_progressivo_attrezzatura,
							 :ls_cod_divisione,
							 :ls_cod_area_aziendale,
							 :ls_cod_reparto,
							 :ls_cod_attrezzatura,
							 :ls_cod_centro_costo,				
							 :ll_costo_tot_ricambi,
							 :ll_costo_tot_ricambi_str,
							 :ll_costo_tot_risorse_int,
							 :ll_costo_tot_risorse_int_str,
							 :ll_costo_tot_risorse_est,
							 :ll_costo_tot_risorse_est_str);			 			
ciccio =	sqlca.sqlerrtext + " " + string(sqlca.sqlcode)				  							 
			if sqlca.sqlcode < 0 then exit 
			
				ll_progressivo_attrezzatura = ll_progressivo_attrezzatura + 1
				
				ll_costo_tot_ricambi = 0
				ll_costo_tot_ricambi_str = 0
				ll_costo_tot_risorse_int = 0
				ll_costo_tot_risorse_int_str = 0
				ll_costo_tot_risorse_est = 0
				ll_costo_tot_risorse_est_str = 0		
				
				ls_comodo_attrezzatura_3 = ""
			
			if ls_cod_attrezzatura <> ls_comodo_attrezzatura then
				ls_comodo_attrezzatura = ls_cod_attrezzatura
				goto SaltoLettura
			else
				
//				ll_costo_tot_ricambi = (ll_quan_ricambio * ll_costo_unitario_ricambio)
//				ll_costo_tot_ricambi_str = 0
//				ll_costo_tot_risorse_int = (((ll_operaio_minuti_previsti / 60) + ll_operaio_ore_previste) * ll_costo_orario_operaio)
//				ll_costo_tot_risorse_int_str = 0
//				ll_costo_tot_risorse_est = ((((ll_risorsa_minuti_previsti / 60) + ll_risorsa_ore_previste) * ll_risorsa_costo_orario) + ll_risorsa_costi_aggiuntivi)
//				ll_costo_tot_risorse_est_str = 0			
	
				close cu_manutenzioni_simulate;			

				ls_comodo_attrezzatura2 = ls_comodo_attrezzatura
				select min(cod_attrezzatura)
				  into :ls_comodo_attrezzatura
				  from manutenzioni_simulate
				 where cod_azienda = :s_cs_xx.cod_azienda
					and data_registrazione <= :ld_a_dt
					and data_registrazione >= :ld_da_dt
					and cod_attrezzatura > :ls_comodo_attrezzatura2;
	
				if isnull(ls_comodo_attrezzatura) or sqlca.sqlcode = 100 or ls_comodo_attrezzatura = "XXXXXX" then
					ls_comodo_attrezzatura = "YYYYYY"
					goto SaltoLettura
				end if	
				
				ld_da_dt2 = ld_da_dt 
				
				open cu_manutenzioni_simulate;
			
			end if
		
		end if
		
		fetch cu_manutenzioni_simulate 
		into :ldt_data_registrazione,
			  :ls_cod_attrezzatura,
			  :ll_quan_ricambio,
			  :ll_costo_unitario_ricambio,
			  :ll_costo_orario_operaio,
			  :ll_operaio_ore_previste,
			  :ll_operaio_minuti_previsti,
			  :ll_risorsa_ore_previste,
			  :ll_risorsa_minuti_previsti, 
			  :ll_risorsa_costo_orario,
			  :ll_risorsa_costi_aggiuntivi;		
ciccio =	sqlca.sqlerrtext + " " + string(sqlca.sqlcode)
		
		
		if sqlca.sqlcode = 100 and sw_ok = 1 then			
			ls_comodo_attrezzatura = "XXXXXX"
		end if
		
		if sqlca.sqlcode = 100 and sw_ok = 0 then
			ls_comodo_attrezzatura_3 = ls_comodo_attrezzatura
			ls_comodo_attrezzatura = "AAAAAA"
			sw_ok = 1
		end if
		
//		if sqlca.sqlcode = 100 then
//			ls_comodo_attrezzatura = "XXXXXX"
//		end if
		
		if sqlca.sqlcode < 0 then exit
		if isnull(ll_quan_ricambio) then ll_quan_ricambio = 0
		if isnull(ll_costo_unitario_ricambio) then ll_costo_unitario_ricambio = 0
		if isnull(ll_costo_orario_operaio) then ll_costo_orario_operaio = 0
		if isnull(ll_operaio_ore_previste) then ll_operaio_ore_previste = 0
		if isnull(ll_operaio_minuti_previsti) then ll_operaio_minuti_previsti = 0
		if isnull(ll_risorsa_ore_previste) then ll_risorsa_ore_previste = 0
		if isnull(ll_risorsa_minuti_previsti) then ll_risorsa_minuti_previsti = 0
		if isnull(ll_risorsa_costo_orario) then ll_risorsa_costo_orario = 0
		if isnull(ll_risorsa_costi_aggiuntivi) then ll_risorsa_costi_aggiuntivi = 0	
SaltoLettura:	
	loop
	
	close cu_manutenzioni_simulate;	
	ld_da = relativedate(ld_a, 1)
	li_contatore = li_contatore + 1	
loop	
/////////////////////////////////////////////////////////////////////////////////////////////
//Fine seconda parte
/////////////////////////////////////////////////////////////////////////////////////////////
st_7.text = "Caricamento tabella BUDGET_MANUTENZIONI parte consuntivo"

ll_costo_tot_ricambi = 0
ll_costo_tot_ricambi_str = 0
ll_costo_tot_risorse_int = 0
ll_costo_tot_risorse_int_str = 0
ll_costo_tot_risorse_est = 0
ll_costo_tot_risorse_est_str = 0	

ls_comodo_c_a = ""

ls_comodo_attrezzatura_3 = ""

li_contatore = 1

ld_da = date("01/01/" + string(li_anno))

do while li_contatore <= li_num_periodi
	
	sw_ok = 0
	
	ld_a = relativedate(ld_da, li_lunghezza_periodo)	
	ld_da_dt = datetime(ld_da, time("00:00:00"))
	ld_a_dt = datetime(ld_a, time("23:59:59"))
	
	select (count(progr_attrezzatura)) + 1
	  into :ll_progressivo_attrezzatura
	  from budget_manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda;
	
	select min(cod_attrezzatura)
	  into :ls_comodo_attrezzatura
	  from manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and data_registrazione <= :ld_a_dt
		and data_registrazione >= :ld_da_dt;
		
	select min(data_registrazione)
	  into :ldt_data_registrazione
	  from manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and data_registrazione <= :ld_a_dt
		and data_registrazione >= :ld_da_dt
		and cod_attrezzatura = :ls_comodo_attrezzatura;		
			 
    select data_registrazione,
	 		  cod_attrezzatura,
			  flag_ordinario,	
 			  quan_ricambio,
			  costo_unitario_ricambio,
			  costo_orario_operaio,
			  operaio_ore,
			  operaio_minuti,
			  risorsa_ore,
			  risorsa_minuti, 
			  risorsa_costo_orario,
			  risorsa_costi_aggiuntivi
		into :ldt_data_registrazione,
			  :ls_cod_attrezzatura,
			  :ls_flag_ordinario,
			  :ll_quan_ricambio,
			  :ll_costo_unitario_ricambio,
			  :ll_costo_orario_operaio,
			  :ll_operaio_ore,
			  :ll_operaio_minuti,
			  :ll_risorsa_ore,
			  :ll_risorsa_minuti, 
			  :ll_risorsa_costo_orario,
			  :ll_risorsa_costi_aggiuntivi					  
	   from manutenzioni
	  where cod_azienda = :s_cs_xx.cod_azienda and
	        data_registrazione = 	(select min(data_registrazione)
	  										   from manutenzioni
	 										  where cod_azienda = :s_cs_xx.cod_azienda
	   										 and data_registrazione <= :ld_a_dt
												 and data_registrazione >= :ld_da_dt
												 and cod_attrezzatura = :ls_comodo_attrezzatura) and
			  cod_attrezzatura = :ls_comodo_attrezzatura;		 
			 
ciccio =	sqlca.sqlerrtext + " " + string(sqlca.sqlcode)				  			 
			 
	select max(cod_attrezzatura)
	  into :ls_max_cod_attrezzatura
	  from manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and data_registrazione <= :ld_a_dt
		and data_registrazione >= :ld_da_dt;	 
			 
   declare cu_manutenzioni cursor for
    select data_registrazione,
	 		  cod_attrezzatura,
			  flag_ordinario,	
 			  quan_ricambio,
			  costo_unitario_ricambio,
			  costo_orario_operaio,
			  operaio_ore,
			  operaio_minuti,
			  risorsa_ore,
			  risorsa_minuti, 
			  risorsa_costo_orario,
			  risorsa_costi_aggiuntivi 
	   from manutenzioni
	  where cod_azienda = :s_cs_xx.cod_azienda and
	        data_registrazione >= :ld_da_dt2 and
			  cod_attrezzatura >= :ls_comodo_attrezzatura
  order by cod_attrezzatura,
  			  data_registrazione;

//	ld_da_dt2 = datetime(date(ldt_data_registrazione), time(relativetime(time(ldt_data_registrazione), 1)))
	ld_da_dt2 = ld_da_dt
	
	open cu_manutenzioni;	
	
	do while ls_comodo_attrezzatura <= ls_max_cod_attrezzatura
		
		if ls_cod_attrezzatura = ls_comodo_attrezzatura and ldt_data_registrazione >= ld_da_dt and ldt_data_registrazione <= ld_a_dt then
			
			select cod_area_aziendale, 
					 cod_reparto,
					 cod_centro_costo
			  into :ls_cod_area_aziendale,
			       :ls_cod_reparto,
					 :ls_cod_centro_costo
			  from anag_attrezzature
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_attrezzatura = :ls_cod_attrezzatura;
			if sqlca.sqlcode < 0 then exit
			if sqlca.sqlcode = 0 or sqlca.sqlcode = 100 then
				select cod_divisione
				  into :ls_cod_divisione
				  from tab_aree_aziendali
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_area_aziendale = :ls_cod_area_aziendale;
				if sqlca.sqlcode < 0 then exit
				if sqlca.sqlcode = 0 or sqlca.sqlcode = 100 then
						if ldt_comodo_d_r <> ldt_data_registrazione or ls_comodo_c_a <> ls_cod_attrezzatura then
						
							if ls_flag_ordinario = 'S' then
								ll_costo_tot_ricambi = ll_costo_tot_ricambi + (ll_quan_ricambio * ll_costo_unitario_ricambio)
								ll_costo_tot_ricambi_str = 0
								ll_costo_tot_risorse_int = ll_costo_tot_risorse_int + (((ll_operaio_minuti / 60) + ll_operaio_ore) * ll_costo_orario_operaio)
								ll_costo_tot_risorse_int_str = 0
								ll_costo_tot_risorse_est = ll_costo_tot_risorse_est + ((((ll_risorsa_minuti / 60) + ll_risorsa_ore) * ll_risorsa_costo_orario) + ll_risorsa_costi_aggiuntivi)
								ll_costo_tot_risorse_est_str = 0						
							else
								ll_costo_tot_ricambi = 0
								ll_costo_tot_ricambi_str = ll_costo_tot_ricambi_str + (ll_quan_ricambio * ll_costo_unitario_ricambio)
								ll_costo_tot_risorse_int = 0
								ll_costo_tot_risorse_int_str = ll_costo_tot_risorse_int_str + (((ll_operaio_minuti / 60) + ll_operaio_ore) * ll_costo_orario_operaio)
								ll_costo_tot_risorse_est = 0
								ll_costo_tot_risorse_est_str = ll_costo_tot_risorse_est_str + ((((ll_risorsa_minuti / 60) + ll_risorsa_ore) * ll_risorsa_costo_orario) + ll_risorsa_costi_aggiuntivi)
							end if
						
							ldt_comodo_d_r = ldt_data_registrazione
							ls_comodo_c_a = ls_cod_attrezzatura
						end if	
				end if
			end if
		else

			if ls_comodo_attrezzatura_3 <> "" then
				ls_comodo_attrezzatura = ls_comodo_attrezzatura_3
			end if				

			update budget_manutenzioni
				set costo_cons_ricambi = :ll_costo_tot_ricambi,
					 costo_cons_ricambi_str = :ll_costo_tot_ricambi_str,
					 costo_cons_risorse_int = :ll_costo_tot_risorse_int,
					 costo_cons_risorse_int_str = :ll_costo_tot_risorse_int_str,
					 costo_cons_risorse_est = :ll_costo_tot_risorse_est,
					 costo_cons_risorse_est_str = :ll_costo_tot_risorse_est_str						
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_budget = :li_anno and
					 num_periodo = :li_contatore and
					 cod_attrezzatura = :ls_comodo_attrezzatura;	

ciccio =	sqlca.sqlerrtext + " " + string(sqlca.sqlcode)					 
					 
			if sqlca.sqlcode <> 0 then exit

			if sqlca.sqlnrows = 0 then
				
//				if ldt_data_registrazione >= ld_da_dt and ldt_data_registrazione <= ld_a_dt then
				
					insert into  budget_manutenzioni (cod_azienda, anno_budget, num_periodo, progr_attrezzatura, cod_divisione, cod_area_aziendale, cod_reparto, cod_attrezzatura, cod_centro_costo, costo_cons_ricambi, costo_cons_ricambi_str, costo_cons_risorse_int, costo_cons_risorse_int_str, costo_cons_risorse_est, costo_cons_risorse_est_str)
						  values (:s_cs_xx.cod_azienda,
									 :li_anno,
									 :li_contatore,
									 :ll_progressivo_attrezzatura,
									 :ls_cod_divisione,
									 :ls_cod_area_aziendale,
									 :ls_cod_reparto,
									 :ls_comodo_attrezzatura,
									 :ls_cod_centro_costo,				
									 :ll_costo_tot_ricambi,
									 :ll_costo_tot_ricambi_str,
									 :ll_costo_tot_risorse_int,
									 :ll_costo_tot_risorse_int_str,
									 :ll_costo_tot_risorse_est,
									 :ll_costo_tot_risorse_est_str);			 			
ciccio =	sqlca.sqlerrtext + " " + string(sqlca.sqlcode)				  							 
					if sqlca.sqlcode < 0 then exit		
					ll_progressivo_attrezzatura = ll_progressivo_attrezzatura + 1
//				end if
			
			end if
			
			ll_costo_tot_ricambi = 0
			ll_costo_tot_ricambi_str = 0
			ll_costo_tot_risorse_int = 0
			ll_costo_tot_risorse_int_str = 0
			ll_costo_tot_risorse_est = 0
			ll_costo_tot_risorse_est_str = 0		
			
			ls_comodo_attrezzatura_3 = ""
			
//			if ls_flag_ordinario = 'S' then
//				ll_costo_tot_ricambi = (ll_quan_ricambio * ll_costo_unitario_ricambio)
//				ll_costo_tot_ricambi_str = 0
//				ll_costo_tot_risorse_int = (((ll_operaio_minuti / 60) + ll_operaio_ore) * ll_costo_orario_operaio)
//				ll_costo_tot_risorse_int_str = 0
//				ll_costo_tot_risorse_est = ((((ll_risorsa_minuti / 60) + ll_risorsa_ore) * ll_risorsa_costo_orario) + ll_risorsa_costi_aggiuntivi)
//				ll_costo_tot_risorse_est_str = 0						
//			else
//				ll_costo_tot_ricambi = 0
//				ll_costo_tot_ricambi_str = (ll_quan_ricambio * ll_costo_unitario_ricambio)
//				ll_costo_tot_risorse_int = 0
//				ll_costo_tot_risorse_int_str = (((ll_operaio_minuti / 60) + ll_operaio_ore) * ll_costo_orario_operaio)
//				ll_costo_tot_risorse_est = 0
//				ll_costo_tot_risorse_est_str = ((((ll_risorsa_minuti / 60) + ll_risorsa_ore) * ll_risorsa_costo_orario) + ll_risorsa_costi_aggiuntivi)
//			end if

			if ls_cod_attrezzatura <> ls_comodo_attrezzatura then
				ls_comodo_attrezzatura = ls_cod_attrezzatura
				goto SaltoLettura2
			else						
							
				close cu_manutenzioni;			
	
				ls_comodo_attrezzatura2 = ls_comodo_attrezzatura
				select min(cod_attrezzatura)
				  into :ls_comodo_attrezzatura
				  from manutenzioni
				 where cod_azienda = :s_cs_xx.cod_azienda
					and data_registrazione <= :ld_a_dt
					and data_registrazione >= :ld_da_dt
					and cod_attrezzatura > :ls_comodo_attrezzatura2;
					
				if isnull(ls_comodo_attrezzatura) or sqlca.sqlcode = 100 or ls_comodo_attrezzatura = "XXXXXX" then
					ls_comodo_attrezzatura = "YYYYYY"
					goto SaltoLettura2					
				end if	
				
				ld_da_dt2 = ld_da_dt 
		
				open cu_manutenzioni;
			end if				
		end if
		
		fetch cu_manutenzioni
		into :ldt_data_registrazione,
			  :ls_cod_attrezzatura,
			  :ls_flag_ordinario,
			  :ll_quan_ricambio,
			  :ll_costo_unitario_ricambio,
			  :ll_costo_orario_operaio,
			  :ll_operaio_ore,
			  :ll_operaio_minuti,
			  :ll_risorsa_ore,
			  :ll_risorsa_minuti, 
			  :ll_risorsa_costo_orario,
			  :ll_risorsa_costi_aggiuntivi;		

		if sqlca.sqlcode = 100 and sw_ok = 1 then			
			ls_comodo_attrezzatura = "XXXXXX"
		end if
		
		if sqlca.sqlcode = 100 and sw_ok = 0 then
			ls_comodo_attrezzatura_3 = ls_comodo_attrezzatura
			ls_comodo_attrezzatura = "AAAAAA"
			sw_ok = 1
		end if
			
//		if sqlca.sqlcode = 100 then
//			ls_comodo_attrezzatura = "XXXXXX"
//		end if

		if sqlca.sqlcode < 0 then exit
		if isnull(ll_quan_ricambio) then ll_quan_ricambio = 0
		if isnull(ll_costo_unitario_ricambio) then ll_costo_unitario_ricambio = 0
		if isnull(ll_costo_orario_operaio) then ll_costo_orario_operaio = 0
		if isnull(ll_operaio_ore) then ll_operaio_ore = 0
		if isnull(ll_operaio_minuti) then ll_operaio_minuti = 0
		if isnull(ll_risorsa_ore) then ll_risorsa_ore = 0
		if isnull(ll_risorsa_minuti) then ll_risorsa_minuti = 0
		if isnull(ll_risorsa_costo_orario) then ll_risorsa_costo_orario = 0
		if isnull(ll_risorsa_costi_aggiuntivi) then ll_risorsa_costi_aggiuntivi = 0	
SaltoLettura2:	
	loop
	
	close cu_manutenzioni;	
	ld_da = relativedate(ld_a, 1)
	li_contatore = li_contatore + 1	
loop	

commit;

/////////////////////////////////////////////////////////////////////////////////////////////
//Fine terza parte
/////////////////////////////////////////////////////////////////////////////////////////////
//
//li_contatore = 1
//
//ld_da = date("01/01/" + string(li_anno))
//
//do while li_contatore <= li_num_periodi
//	
//	ld_a = relativedate(ld_da, li_lunghezza_periodo)	
//	ld_da_dt = datetime(ld_da, time(00:00:00))
//	ld_a_dt = datetime(ld_a, time(23:59:59))
//	
//	select count(progr_attrezzatura)
//	  into :ll_progressivo_attrezzatura
//	  from budget_manutenzioni
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and num_periodo = :li_contatore;
//	
////	select (sum((((costo_cons_ricambi + costo_cons_ricambi) - (costo_prev_ricambi + costo_prev_ricambi)) * 100) / (costo_prev_ricambi + costo_prev_ricambi))) / :ll_progressivo_attrezzatura,
////			 (sum((((costo_cons_risorse_int + costo_cons_risorse_int_str) - (costo_prev_risorse_int + costo_prev_risorse_int_str)) * 100) / (costo_prev_risorse_int + costo_prev_risorse_int_str))) / :ll_progressivo_attrezzatura,			 	
////			 (sum((((costo_cons_risorse_est + costo_cons_risorse_est_str) - (costo_prev_risorse_est + costo_prev_risorse_est_str)) * 100) / (costo_prev_risorse_est + costo_prev_risorse_est_str))) / :ll_progressivo_attrezzatura	
//   select ((sum((costo_cons_ricambi + costo_cons_ricambi_str) - (costo_prev_ricambi + costo_prev_ricambi_str))) * 100 / sum(costo_prev_ricambi + costo_prev_ricambi_str)) / :ll_progressivo_attrezzatura,
//			 ((sum((costo_cons_risorse_int + costo_cons_risorse_int_str) - (costo_prev_risorse_int + costo_prev_risorse_int_str))) * 100 / sum(costo_prev_risorse_int + costo_prev_risorse_int_str)) / :ll_progressivo_attrezzatura,		  
//			 ((sum((costo_cons_risorse_est + costo_cons_risorse_est_str) - (costo_prev_risorse_est + costo_prev_risorse_est_str))) * 100 / sum(costo_prev_risorse_est + costo_prev_risorse_est_str)) / :ll_progressivo_attrezzatura	
//	  into :ldd_percentuale_ricambi,
//	       :ldd_percentuale_ris_int,
//			 :ldd_percentuale_ris_est
//	  from budget_manutenzioni
//	 where cod_azienda = :s_cs_xx.cod_azienda 
//	   and num_periodo = :li_contatore;
//ciccio =	sqlca.sqlerrtext + " " + string(sqlca.sqlcode)				 	
//	if sqlca.sqlcode < 0 then exit
//	
//	if ldd_percentuale_ricambi <> 0 or ldd_percentuale_ris_int <> 0 or ldd_percentuale_ris_est <> 0 then
//		update budget_manutenzioni
//		   set costo_trend_ricambi = ((costo_prev_ricambi + costo_prev_ricambi) + ((costo_prev_ricambi + costo_prev_ricambi) * abs(:ldd_percentuale_ricambi) / 100 )),
//			    costo_trend_risorse_int = ((costo_prev_risorse_int + costo_prev_risorse_int_str) + ((costo_prev_risorse_int + costo_prev_risorse_int_str) * :ldd_percentuale_ris_int / 100)),
//			    costo_trend_risorse_est = ((costo_prev_risorse_est + costo_prev_risorse_est_str) + ((costo_prev_risorse_est + costo_prev_risorse_est_str) * :ldd_percentuale_ris_est / 100))
//		 where cod_azienda = :s_cs_xx.cod_azienda
//		   and num_periodo = :li_contatore
//			and (costo_cons_ricambi = 0 and
//			     costo_cons_ricambi_str = 0 and
//				  costo_cons_risorse_int = 0 and
//				  costo_cons_risorse_int_str = 0 and
//				  costo_cons_risorse_est = 0 and
//				  costo_cons_risorse_est_str = 0)
//			and (costo_prev_ricambi > 0 or
//			     costo_prev_ricambi_str > 0 or
//				  costo_prev_risorse_int > 0 or
//				  costo_prev_risorse_int_str > 0 or
//				  costo_prev_risorse_est > 0 or
//				  costo_prev_risorse_est_str > 0);	 
//		if sqlca.sqlcode <> 0 then exit
//		
//	end if	
//	
//	ld_da = relativedate(ld_a, 1)
//	li_contatore = li_contatore + 1	
//loop	
//
//commit;	

/////////////////////////////////////////////////////////////////////////////////////////////
//Fine quarta parte
/////////////////////////////////////////////////////////////////////////////////////////////
li_contatore = 1
li_contatore_percentuale = 0
ldd_percentuale = 0
ls_sw_prima_volta = 0

ls_cod_area_aziendale = dw_selezione.getitemstring(1, "cod_area_aziendale")
ls_cod_divisione = dw_selezione.getitemstring(1, "cod_divisione")
ls_cod_reparto = dw_selezione.getitemstring(1, "cod_reparto")

dw_report.reset()

for li_contatore = 1 to li_num_periodi
	dw_report.insertrow(li_contatore)	
	ls_stringa_num_periodo = string(li_contatore) + "° periodo"
	dw_report.setitem(li_contatore,"num_periodo", ls_stringa_num_periodo)
	dw_report.setitem(li_contatore,"c",0.0)
	dw_report.setitem(li_contatore,"p",0.0)
	dw_report.setitem(li_contatore,"c_p",0.0)
	dw_report.setitem(li_contatore,"trend",0.0)
next

if ls_cod_area_aziendale = "" or isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = "%"
if ls_cod_divisione = "" or isnull(ls_cod_divisione) then ls_cod_divisione = "%"
if ls_cod_reparto = "" or isnull(ls_cod_reparto) then ls_cod_reparto ="%"

for li_contatore = 1 to li_num_periodi
	
	select sum((costo_prev_ricambi + costo_prev_ricambi_str) + (costo_prev_risorse_int + costo_prev_risorse_int_str) + (costo_prev_risorse_est + costo_prev_risorse_est_str)),
			 sum((costo_cons_ricambi + costo_cons_ricambi_str) + (costo_cons_risorse_int + costo_cons_risorse_int_str) + (costo_cons_risorse_est + costo_cons_risorse_est_str))
     into :ldd_somma_prev,
	  		 :ldd_somma_cons 
     from budget_manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and anno_budget = :li_anno
		and num_periodo = :li_contatore		
		and (cod_divisione like :ls_cod_divisione or (:ls_cod_divisione = '%' and cod_divisione is null))	
		and (cod_area_aziendale like :ls_cod_area_aziendale or (:ls_cod_area_aziendale = '%' and cod_area_aziendale is null)) 
		and (cod_reparto like :ls_cod_reparto or (:ls_cod_reparto = '%' and cod_reparto is null));
	
	if sqlca.sqlcode < 0 then exit
	
	if sqlca.sqlcode = 0 and sqlca.sqlcode <> 100 then
		
		dw_report.setitem(li_contatore,"c",ldd_somma_cons)
		dw_report.setitem(li_contatore,"p",ldd_somma_prev)
		
		if ldd_somma_cons > 0 then
			ldd_differenza_c_p = ldd_somma_cons - ldd_somma_prev
			dw_report.setitem(li_contatore,"c_p",ldd_differenza_c_p)		
		end if

		if ldd_somma_cons > 0 and ldd_somma_prev = 0 then
			li_contatore_percentuale = li_contatore_percentuale + 1 
			ldd_percentuale = ldd_percentuale + 100 
		end if	
		if ldd_somma_cons > 0 and ldd_somma_prev > 0 then
			li_contatore_percentuale = li_contatore_percentuale + 1 
			ldd_percentuale = (ldd_percentuale + ((ldd_somma_cons - ldd_somma_prev) * 100 / ldd_somma_prev))
			ldd_trend = 0.0
		end if 
		if ldd_somma_prev > 0 and ldd_somma_cons = 0.0 and li_contatore > 1 and ldd_percentuale <> 0 then
			if ls_sw_prima_volta = 0 then
				ldd_percentuale = ldd_percentuale / li_contatore_percentuale
				ls_sw_prima_volta = 1
			end if
			ldd_trend = ldd_somma_prev + (ldd_somma_prev * ldd_percentuale / 100)
			dw_report.setitem(li_contatore,"trend",ldd_trend)
		end if			

	end if

next		


st_7.text = "Elaborazione eseguita con successo"	

SetPointer(Arrow!)
end event

type dw_report from datawindow within w_budget_manutenzioni
int X=23
int Y=21
int Width=2561
int Height=1041
int TabOrder=10
boolean BringToTop=true
string DataObject="d_budget_anno"
boolean LiveScroll=true
end type

type cb_visualizzazione from commandbutton within w_budget_manutenzioni
int X=2218
int Y=1181
int Width=366
int Height=81
int TabOrder=20
string Text="Carica"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;double ldd_percentuale, ldd_trend
double ldd_somma_prev, ldd_somma_cons, ldd_differenza_c_p
integer li_contatore_percentuale, ls_sw_prima_volta, li_contatore, li_num_periodi, li_anno
string ls_stringa_num_periodo, ls_cod_area_aziendale, ls_cod_divisione, ls_cod_reparto

li_contatore = 1
li_contatore_percentuale = 0
ldd_percentuale = 0
ls_sw_prima_volta = 0

dw_report.reset()

select num_periodi 
  into :li_num_periodi
  from con_budget_manutenzioni;

dw_selezione.accepttext()

li_anno = dw_selezione.getitemnumber(1, "anno_da")

ls_cod_area_aziendale = dw_selezione.getitemstring(1, "cod_area_aziendale")
ls_cod_divisione = dw_selezione.getitemstring(1, "cod_divisione")
ls_cod_reparto = dw_selezione.getitemstring(1, "cod_reparto")

for li_contatore = 1 to li_num_periodi
	dw_report.insertrow(li_contatore)	
	ls_stringa_num_periodo = string(li_contatore) + "° periodo"
	dw_report.setitem(li_contatore,"num_periodo", ls_stringa_num_periodo)
	dw_report.setitem(li_contatore,"c",0.0)
	dw_report.setitem(li_contatore,"p",0.0)
	dw_report.setitem(li_contatore,"c_p",0.0)
	dw_report.setitem(li_contatore,"trend",0.0)
next

if ls_cod_area_aziendale = "" or isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = "%"
if ls_cod_divisione = "" or isnull(ls_cod_divisione) then ls_cod_divisione = "%"
if ls_cod_reparto = "" or isnull(ls_cod_reparto) then ls_cod_reparto ="%"

for li_contatore = 1 to li_num_periodi
	
	select sum(costo_prev_ricambi + costo_prev_ricambi_str + costo_prev_risorse_int + costo_prev_risorse_int_str + costo_prev_risorse_est + costo_prev_risorse_est_str),
			 sum(costo_cons_ricambi + costo_cons_ricambi_str + costo_cons_risorse_int + costo_cons_risorse_int_str + costo_cons_risorse_est + costo_cons_risorse_est_str)
     into :ldd_somma_prev,
	  		 :ldd_somma_cons 
     from budget_manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and anno_budget = :li_anno
		and num_periodo = :li_contatore
		and (cod_divisione like :ls_cod_divisione or (:ls_cod_divisione = '%' and cod_divisione is null))	
		and (cod_area_aziendale like :ls_cod_area_aziendale or (:ls_cod_area_aziendale = '%' and cod_area_aziendale is null)) 
		and (cod_reparto like :ls_cod_reparto or (:ls_cod_reparto = '%' and cod_reparto is null));
			  
	if sqlca.sqlcode < 0 then exit
	
	if sqlca.sqlcode = 0  and sqlca.sqlcode <> 100 then
		
		dw_report.setitem(li_contatore,"p",ldd_somma_prev)
		dw_report.setitem(li_contatore,"c",ldd_somma_cons)
		
		if ldd_somma_cons > 0 then
			ldd_differenza_c_p = ldd_somma_cons - ldd_somma_prev
			dw_report.setitem(li_contatore,"c_p",ldd_differenza_c_p)		
		end if

		if ldd_somma_cons > 0 and ldd_somma_prev = 0 then
			li_contatore_percentuale = li_contatore_percentuale + 1 
			ldd_percentuale = ldd_percentuale + 100 
		end if	
		if ldd_somma_cons > 0 and ldd_somma_prev > 0 then
			li_contatore_percentuale = li_contatore_percentuale + 1 
			ldd_percentuale = (ldd_percentuale + ((ldd_somma_cons - ldd_somma_prev) * 100 / ldd_somma_prev))
			ldd_trend = 0.0
		end if 
		if ldd_somma_prev > 0 and ldd_somma_cons = 0.0 and li_contatore > 1 and ldd_percentuale <> 0 then
			if ls_sw_prima_volta = 0 then
				ldd_percentuale = ldd_percentuale / li_contatore_percentuale
				ls_sw_prima_volta = 1
			end if
			ldd_trend = ldd_somma_prev + (ldd_somma_prev * ldd_percentuale / 100)
			dw_report.setitem(li_contatore,"trend",ldd_trend)
		end if			

	ldd_somma_prev = 0
	ldd_somma_cons = 0
	ldd_differenza_c_p = 0
	
	end if
	
next		

end event

type st_1 from statictext within w_budget_manutenzioni
int X=252
int Y=1481
int Width=2172
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="= carica la tabella Budget_manutenzioni reperendo i dati dalla tabella  Manutenzioni"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_budget_manutenzioni
int X=23
int Y=1481
int Width=206
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elabora"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_budget_manutenzioni
int X=252
int Y=1541
int Width=2172
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="e dalla tabella Manutenzioni_simulate. La sua esecuzione puo' richiedere parecchio"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_budget_manutenzioni
int X=252
int Y=1601
int Width=183
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="tempo"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_budget_manutenzioni
int X=23
int Y=1661
int Width=206
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Carica"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_budget_manutenzioni
int X=252
int Y=1661
int Width=435
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="= visualizza dati"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_7 from statictext within w_budget_manutenzioni
int X=869
int Y=1661
int Width=1715
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

