﻿$PBExportHeader$w_det_budget_ins_periodi.srw
forward
global type w_det_budget_ins_periodi from w_cs_xx_risposta
end type
type cb_indietro from commandbutton within w_det_budget_ins_periodi
end type
type cb_avanti from commandbutton within w_det_budget_ins_periodi
end type
type cbx_periodi_vuoti from checkbox within w_det_budget_ins_periodi
end type
type cb_conferma from commandbutton within w_det_budget_ins_periodi
end type
type dw_folder from u_folder within w_det_budget_ins_periodi
end type
type dw_risorse from uo_cs_xx_dw within w_det_budget_ins_periodi
end type
type dw_periodi_ris_est from datawindow within w_det_budget_ins_periodi
end type
type dw_periodi_ris_int from datawindow within w_det_budget_ins_periodi
end type
type st_des_tipo_manutenzione from statictext within w_det_budget_ins_periodi
end type
type dw_tipi_manutenzione from datawindow within w_det_budget_ins_periodi
end type
type dw_det_budget_ins_periodi_1 from uo_cs_xx_dw within w_det_budget_ins_periodi
end type
end forward

global type w_det_budget_ins_periodi from w_cs_xx_risposta
integer width = 2693
integer height = 2084
string title = "Inserimento Costi Budget"
cb_indietro cb_indietro
cb_avanti cb_avanti
cbx_periodi_vuoti cbx_periodi_vuoti
cb_conferma cb_conferma
dw_folder dw_folder
dw_risorse dw_risorse
dw_periodi_ris_est dw_periodi_ris_est
dw_periodi_ris_int dw_periodi_ris_int
st_des_tipo_manutenzione st_des_tipo_manutenzione
dw_tipi_manutenzione dw_tipi_manutenzione
dw_det_budget_ins_periodi_1 dw_det_budget_ins_periodi_1
end type
global w_det_budget_ins_periodi w_det_budget_ins_periodi

type variables
integer ib_passo = 1
end variables

forward prototypes
public subroutine wf_carica_periodi ()
end prototypes

public subroutine wf_carica_periodi ();long   ll_num_periodi, ll_i,ll_operaio_ore_previste,ll_operaio_minuti_previsti,ll_risorsa_ore_previste,ll_risorsa_minuti_previsti, &
       ll_prog_periodo, ll_riga,ll_progressivo, ll_y

string ls_flag_blocco,ls_cod_divisione_sel,ls_cod_area_aziendale_sel,ls_cod_reparto_sel,ls_cod_cat_attrezzature_sel, &
       ls_cod_attrezzatura_sel,ls_flag_tipo_periodo, ls_cod_attrezzatura,ls_flag_tipo_intervento, ls_ricambio_non_codificato, &
		 ls_cod_ricambio,ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_flag_combustibile, &
		 ls_cod_attrezzatura_prec, ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzatura, &
		 ls_cod_tipo_manutenzione

dec{4} ld_costo_unitario_ricambio,ld_costo_orario_operaio,ld_risorsa_costo_orario,ld_risorsa_costi_aggiuntivi,&
       ld_tot_costo_intervento,ld_costo_prev_ricambi,ld_costo_prev_risorse_int,ld_costo_prev_risorse_est, ld_tot_prog_costo, &
		 ld_quan_ricambio,ld_totale, ld_tot_ore_risorsa_interna,ld_costo_ora_risorsa_interna, ld_tot_costo_risorsa_esterna, &
		 ld_tot_prog_ore, ld_percento

date     ld_data

datetime ldt_data_inizio_elaborazione, ldt_data_consolidato,ldt_data_fine_elaborazione, ldt_data_registrazione, &
         ldt_periodo_inizio[], ldt_periodo_fine[],ldt_oggi


if dw_tipi_manutenzione.rowcount() < 1 then
	g_mb.messagebox("OMNIA", "Per questa attrezzatura non sono previste tipologie manutentive")
	cb_avanti.triggerevent("ue_indietro")
	return
end if


ll_progressivo = dw_det_budget_ins_periodi_1.i_parentdw.getitemnumber(dw_det_budget_ins_periodi_1.i_parentdw.i_selectedrows[1], "progressivo")		

uo_manutenzioni luo_manutenzioni

ldt_oggi = datetime(today(),00:00:00)

select data_inizio_elaborazione, 
       flag_blocco, 
		 data_consolidato,
		 cod_divisione,
		 cod_area_aziendale,
		 cod_reparto,
		 cod_cat_attrezzature,
		 cod_attrezzatura,
		 flag_tipo_periodo,
		 num_periodi
into   :ldt_data_inizio_elaborazione, 
       :ls_flag_blocco, 
		 :ldt_data_consolidato,
		 :ls_cod_divisione_sel,
		 :ls_cod_area_aziendale_sel,
		 :ls_cod_reparto_sel,
		 :ls_cod_cat_attrezzature_sel,
		 :ls_cod_attrezzatura_sel,
		 :ls_flag_tipo_periodo,
		 :ll_num_periodi
from   tes_budget_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca testata budget.~r~n" + sqlca.sqlerrtext)
	return
end if

if ls_flag_blocco = "S" then
	g_mb.messagebox("OMNIA","Elaborazione interrotta: budget bloccato!")
	return
end if
	
if not isnull(ldt_data_consolidato) then
	g_mb.messagebox("OMNIA","Elaborazione interrotta: budget già consolidato!")
	return
end if

if isnull(ldt_data_inizio_elaborazione) then
	g_mb.messagebox("OMNIA","Indicare una data inizio elaborazione!")
	return
end if

// creo il vettore con i range di date che mi servono.

luo_manutenzioni = create uo_manutenzioni

ldt_data_registrazione = ldt_data_inizio_elaborazione

for ll_i = 1 to ll_num_periodi

	ldt_data_fine_elaborazione = luo_manutenzioni.uof_prossima_scadenza (ldt_data_registrazione , ls_flag_tipo_periodo, 1)
	ldt_periodo_inizio[ll_i] = ldt_data_registrazione
	
	ld_data = date(ldt_data_fine_elaborazione)
	ld_data = relativedate(ld_data, -1)
	
	ldt_periodo_fine[ll_i] = datetime(ld_data, 00:00:00)
	
	ldt_data_registrazione = ldt_data_fine_elaborazione

next

destroy luo_manutenzioni

// carico la tabella dei periodi

ls_cod_attrezzatura_prec = ""

ls_flag_tipo_periodo = dw_det_budget_ins_periodi_1.i_parentdw.getitemstring(dw_det_budget_ins_periodi_1.i_parentdw.i_selectedrows[1], "flag_tipo_periodo")		

if ld_totale >= 0 then dw_periodi_ris_int.reset()
if ld_tot_costo_risorsa_esterna >= 0 then dw_periodi_ris_est.reset()


if ls_flag_combustibile = "S" and ls_flag_tipo_periodo = "E" then
	// in caso di totali suddivisi per combustibili
	
	for ll_y = 1 to dw_tipi_manutenzione.rowcount()
		
		ld_tot_ore_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_ore_risorsa_interna")
		ld_costo_ora_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"costo_ora_risorsa_interna")
		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
		ld_tot_costo_risorsa_esterna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_costo_risorsa_esterna")
		ls_flag_combustibile = dw_tipi_manutenzione.getitemstring(ll_y, "flag_combustibile")
			
		// risorse interne
		ld_tot_prog_costo = 0
		ld_tot_prog_ore   = 0
		for ll_i = 1 to ll_num_periodi
		
		
			if ld_totale >= 0 then
				ll_riga = dw_periodi_ris_int.insertrow(0)
				dw_periodi_ris_int.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
				dw_periodi_ris_int.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
				dw_periodi_ris_int.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
				dw_periodi_ris_int.setitem(ll_riga, "num_periodo", ll_i)
				
				choose case month(date(ldt_periodo_inizio[ll_i]))
					case 1
						ld_percento = 22.5
					case 2
						ld_percento = 15.2
					case 3
						ld_percento = 13.5
					case 4
						ld_percento = 5.00
					case 10
						ld_percento = 5.80
					case 11
						ld_percento = 16.20
					case 12
						ld_percento = 21.80
					case else
						ld_percento = 0.00
				end choose
				
				if ll_i <> ll_num_periodi then
					dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", round((ld_tot_ore_risorsa_interna * ld_percento) / 100  ,2))
					dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", round((ld_totale * ld_percento) / 100, 2))
					ld_tot_prog_costo += round((ld_totale * ld_percento) / 100,2)
					ld_tot_prog_ore += round((ld_tot_ore_risorsa_interna * ld_percento) / 100, 0)
				else
					if (ld_totale - ld_tot_prog_costo) >= 0 then
						dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", ld_totale - ld_tot_prog_costo)
					else
						dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", 0)
					end if
					if (ld_tot_ore_risorsa_interna - ld_tot_prog_ore) >= 0 then
						dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", ld_tot_ore_risorsa_interna - ld_tot_prog_ore)
					else
						dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", 0)
					end if
				end if		
					
			end if
			
		next
		
	next
	
	for ll_y = 1 to dw_tipi_manutenzione.rowcount()
		ld_tot_ore_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_ore_risorsa_interna")
		ld_costo_ora_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"costo_ora_risorsa_interna")
		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
		ld_tot_costo_risorsa_esterna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_costo_risorsa_esterna")
		ls_flag_combustibile = dw_tipi_manutenzione.getitemstring(ll_y, "flag_combustibile")
			
		// risorse esterne
		ld_tot_prog_costo = 0
		for ll_i = 1 to ll_num_periodi
		
			if ld_tot_costo_risorsa_esterna >= 0 then
				ll_riga = dw_periodi_ris_est.insertrow(0)
				dw_periodi_ris_est.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
				dw_periodi_ris_est.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
				dw_periodi_ris_est.setitem(ll_riga, "costo_periodo", 0)
				dw_periodi_ris_est.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
				dw_periodi_ris_est.setitem(ll_riga, "num_periodo", ll_i)
			end if
			
		next
		
	next
	
else	
	// gestione normale no combustibili
	for ll_y = 1 to dw_tipi_manutenzione.rowcount()
		ld_tot_ore_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_ore_risorsa_interna")
		ld_costo_ora_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"costo_ora_risorsa_interna")
		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
		ld_tot_costo_risorsa_esterna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_costo_risorsa_esterna")
		ls_flag_combustibile = dw_tipi_manutenzione.getitemstring(ll_y, "flag_combustibile")
	
		// risorse interne
		ld_tot_prog_costo = 0
		ld_tot_prog_ore   = 0
		for ll_i = 1 to ll_num_periodi
		
		
			if ld_totale >= 0 then
				ll_riga = dw_periodi_ris_int.insertrow(0)
				dw_periodi_ris_int.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
				dw_periodi_ris_int.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
				if ll_i <> ll_num_periodi then
					dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", round(ld_tot_ore_risorsa_interna/ ll_num_periodi,0))
					dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", round(ld_totale / ll_num_periodi,2))
					ld_tot_prog_costo += round(ld_totale / ll_num_periodi,2)
					ld_tot_prog_ore += round(ld_tot_ore_risorsa_interna / ll_num_periodi,0)
				else
					dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", ld_totale - ld_tot_prog_costo)
					dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", ld_tot_ore_risorsa_interna - ld_tot_prog_ore)
				end if		
				dw_periodi_ris_int.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
				dw_periodi_ris_int.setitem(ll_riga, "num_periodo", ll_i)
			end if
		next
	next
	
	// risorse esterne
	
	for ll_y = 1 to dw_tipi_manutenzione.rowcount()
		ld_tot_ore_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_ore_risorsa_interna")
		ld_costo_ora_risorsa_interna = dw_tipi_manutenzione.getitemnumber(ll_y,"costo_ora_risorsa_interna")
		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
		ld_tot_costo_risorsa_esterna = dw_tipi_manutenzione.getitemnumber(ll_y,"tot_costo_risorsa_esterna")
		ls_flag_combustibile = dw_tipi_manutenzione.getitemstring(ll_y, "flag_combustibile")
		
		ld_tot_prog_costo = 0
		for ll_i = 1 to ll_num_periodi
		
			if ld_tot_costo_risorsa_esterna >= 0 then
				ll_riga = dw_periodi_ris_est.insertrow(0)
				dw_periodi_ris_est.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
				dw_periodi_ris_est.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
				if ll_i <> ll_num_periodi then
					dw_periodi_ris_est.setitem(ll_riga, "costo_periodo", round(ld_tot_costo_risorsa_esterna / ll_num_periodi,2))
					ld_tot_prog_costo += round(ld_tot_costo_risorsa_esterna / ll_num_periodi,2)
				else
					dw_periodi_ris_est.setitem(ll_riga, "costo_periodo", ld_tot_costo_risorsa_esterna - ld_tot_prog_costo)
				end if		
				dw_periodi_ris_est.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
				dw_periodi_ris_est.setitem(ll_riga, "num_periodo", ll_i)
					
			end if

		next
		
	next

end if

dw_periodi_ris_int.accepttext()

dw_periodi_ris_est.accepttext()

return
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]
set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_det_budget_ins_periodi_1.set_dw_options(sqlca, &
                            i_openparm, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


dw_risorse.set_dw_options(sqlca, &
                            i_openparm, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


lw_oggetti[1] = dw_tipi_manutenzione
dw_folder.fu_assigntab(2, "Tipologie Manutenzione", lw_oggetti[])

lw_oggetti[1] = dw_det_budget_ins_periodi_1
//lw_oggetti[2] = cb_attrezzature_ricerca
dw_folder.fu_assigntab(1, "Attrezzature", lw_oggetti[])

lw_oggetti[1] = dw_risorse
lw_oggetti[2] = dw_periodi_ris_est
lw_oggetti[3] = dw_periodi_ris_int
lw_oggetti[4] = st_des_tipo_manutenzione
dw_folder.fu_assigntab(3, "Periodi/Risorse", lw_oggetti[])

dw_folder.fu_foldercreate(3, 4)
	
dw_folder.fu_selecttab(1)
dw_folder.fu_hidetab(2)
dw_folder.fu_hidetab(3)
cb_conferma.enabled = false

dw_tipi_manutenzione.setrowfocusindicator(Hand!)

dw_det_budget_ins_periodi_1.postevent("itemchanged")
end event

on w_det_budget_ins_periodi.create
int iCurrent
call super::create
this.cb_indietro=create cb_indietro
this.cb_avanti=create cb_avanti
this.cbx_periodi_vuoti=create cbx_periodi_vuoti
this.cb_conferma=create cb_conferma
this.dw_folder=create dw_folder
this.dw_risorse=create dw_risorse
this.dw_periodi_ris_est=create dw_periodi_ris_est
this.dw_periodi_ris_int=create dw_periodi_ris_int
this.st_des_tipo_manutenzione=create st_des_tipo_manutenzione
this.dw_tipi_manutenzione=create dw_tipi_manutenzione
this.dw_det_budget_ins_periodi_1=create dw_det_budget_ins_periodi_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_indietro
this.Control[iCurrent+2]=this.cb_avanti
this.Control[iCurrent+3]=this.cbx_periodi_vuoti
this.Control[iCurrent+4]=this.cb_conferma
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_risorse
this.Control[iCurrent+7]=this.dw_periodi_ris_est
this.Control[iCurrent+8]=this.dw_periodi_ris_int
this.Control[iCurrent+9]=this.st_des_tipo_manutenzione
this.Control[iCurrent+10]=this.dw_tipi_manutenzione
this.Control[iCurrent+11]=this.dw_det_budget_ins_periodi_1
end on

on w_det_budget_ins_periodi.destroy
call super::destroy
destroy(this.cb_indietro)
destroy(this.cb_avanti)
destroy(this.cbx_periodi_vuoti)
destroy(this.cb_conferma)
destroy(this.dw_folder)
destroy(this.dw_risorse)
destroy(this.dw_periodi_ris_est)
destroy(this.dw_periodi_ris_int)
destroy(this.st_des_tipo_manutenzione)
destroy(this.dw_tipi_manutenzione)
destroy(this.dw_det_budget_ins_periodi_1)
end on

type cb_indietro from commandbutton within w_det_budget_ins_periodi
integer x = 1874
integer y = 1880
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<< Indietro"
end type

event clicked;cb_avanti.triggerevent("ue_indietro")
end event

type cb_avanti from commandbutton within w_det_budget_ins_periodi
event ue_avanti ( )
event ue_indietro ( )
event ue_passi ( )
integer x = 2263
integer y = 1880
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Avanti >>"
end type

event ue_avanti();// condizioni per andare avanti
choose case ib_passo
	case 1
		if isnull(dw_det_budget_ins_periodi_1.getitemstring(dw_det_budget_ins_periodi_1.getrow(),"cod_attrezzatura")) then
			g_mb.messagebox("OMNIA","L'indicazione dell'attrezzatura è obbligatorio")
			return
		end if
		
		if isnull(dw_det_budget_ins_periodi_1.getitemnumber(dw_det_budget_ins_periodi_1.getrow(),"num_attrezzature")) then
			g_mb.messagebox("OMNIA","L'indicazione dell'attrezzatura è obbligatorio")
			return
		end if
		
	case 2
		
	case 3
		
end choose


if ib_passo < 3 then
	ib_passo = ib_passo + 1
end if

triggerevent("ue_passi")
end event

event ue_indietro();if ib_passo > 1 then
	ib_passo = ib_passo - 1
end if

triggerevent("ue_passi")

cb_avanti.enabled = true

end event

event ue_passi();choose case ib_passo
	case 1 // ingresso al passo 1
		dw_folder.fu_showtab(1)
		dw_folder.fu_selecttab(1)
		dw_folder.fu_hidetab(2)
		dw_folder.fu_hidetab(3)
		cb_conferma.enabled = false
		cb_avanti.enabled = false
		
	case 2	// ingresso al passo 2
		
		dw_folder.fu_showtab(2)
		dw_folder.fu_selecttab(2)
		dw_folder.fu_hidetab(1)
		dw_folder.fu_hidetab(3)
		cb_conferma.enabled = false
		
	case 3	// ingresso al passo 3
		dw_folder.fu_showtab(3)
		dw_folder.fu_selecttab(3)
		dw_folder.fu_hidetab(1)
		dw_folder.fu_hidetab(2)
		cb_conferma.enabled = true
		cb_avanti.enabled = false
		
		if dw_periodi_ris_int.rowcount() < 1 and dw_periodi_ris_est.rowcount() < 1 then
			wf_carica_periodi()
		end if
		
		st_des_tipo_manutenzione.text = dw_tipi_manutenzione.getitemstring(dw_tipi_manutenzione.getrow(),"cod_tipo_manutenzione") + " - " + dw_tipi_manutenzione.getitemstring(dw_tipi_manutenzione.getrow(),"des_tipo_manutenzione")
		
		dw_periodi_ris_int.triggerevent("ue_filtra")
		dw_periodi_ris_est.triggerevent("ue_filtra")
		
end choose
		
end event

event clicked;triggerevent("ue_avanti")
end event

type cbx_periodi_vuoti from checkbox within w_det_budget_ins_periodi
integer x = 402
integer y = 1872
integer width = 969
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Inserisci i periodi anche se vuoti "
boolean checked = true
end type

type cb_conferma from commandbutton within w_det_budget_ins_periodi
integer x = 23
integer y = 1880
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fine"
end type

event clicked;long   ll_i, ll_prog_periodo,  ll_progressivo, ll_num_copie_attrezzature,ll_found, ll_num_periodo
string ls_flag_blocco,ls_cod_divisione_sel,ls_cod_area_aziendale_sel,ls_cod_reparto_sel,ls_cod_cat_attrezzature_sel, &
       ls_cod_attrezzatura_sel,ls_flag_tipo_periodo, ls_cod_attrezzatura,ls_flag_tipo_intervento, ls_ricambio_non_codificato, &
		 ls_cod_ricambio,ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_cod_tipo_manutenzione, &
		 ls_cod_attrezzatura_prec, ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzatura
dec{4} ld_costo_unitario_ricambio,ld_costo_orario_operaio,ld_risorsa_costo_orario,ld_risorsa_costi_aggiuntivi,&
       ld_tot_costo_intervento,ld_costo_prev_ricambi,ld_costo_prev_risorse_int,ld_costo_prev_risorse_est, &
		 ld_quan_ricambio,ld_operaio_ore_previste
date     ld_data
datetime ldt_data_inizio_elaborazione, ldt_data_consolidato,ldt_data_fine_elaborazione, ldt_data_registrazione, &
         ldt_periodo_inizio, ldt_periodo_fine,ldt_oggi
			

if g_mb.messagebox("OMNIA","Procedo con caricamento dati?", Question!, YesNo!, 2) = 2 then return

ll_progressivo = dw_det_budget_ins_periodi_1.i_parentdw.getitemnumber(dw_det_budget_ins_periodi_1.i_parentdw.i_selectedrows[1], "progressivo")		
ls_cod_attrezzatura = dw_det_budget_ins_periodi_1.getitemstring(dw_det_budget_ins_periodi_1.getrow(),"cod_attrezzatura")
ll_num_copie_attrezzature   = dw_det_budget_ins_periodi_1.getitemnumber(dw_det_budget_ins_periodi_1.getrow(), "num_attrezzature")

dw_periodi_ris_int.setfilter("")
dw_periodi_ris_est.setfilter("")
dw_periodi_ris_int.filter()
dw_periodi_ris_est.filter()

for ll_i = 1 to dw_periodi_ris_int.rowcount()

		ll_num_periodo = dw_periodi_ris_int.getitemnumber(ll_i,"num_periodo")
		ldt_periodo_inizio = dw_periodi_ris_int.getitemdatetime(ll_i, "data_inizio")
		ldt_periodo_fine   = dw_periodi_ris_int.getitemdatetime(ll_i, "data_fine")
		ls_cod_tipo_manutenzione   = dw_periodi_ris_int.getitemstring(ll_i, "cod_tipo_manutenzione")
		
		ld_quan_ricambio = 0
		ld_costo_unitario_ricambio = 0
		ld_costo_prev_ricambi = 0
		
		// trovo il costo associato al realtivo tipo manutenzione
		ll_found = dw_tipi_manutenzione.Find("cod_tipo_manutenzione = '" + ls_cod_tipo_manutenzione + "'", 1, dw_tipi_manutenzione.RowCount())		
		ld_costo_orario_operaio = dw_tipi_manutenzione.getitemnumber(ll_found,"costo_ora_risorsa_interna")
		
		ld_operaio_ore_previste	= dw_periodi_ris_int.getitemnumber(ll_i, "ore_periodo")
		ld_costo_prev_risorse_int = dw_periodi_ris_int.getitemnumber(ll_i, "costo_periodo")
		
		if isnull(ld_costo_orario_operaio) then ld_costo_orario_operaio = 0
		ld_costo_prev_risorse_est = dw_periodi_ris_est.getitemnumber(ll_i, "costo_periodo")

		if ld_operaio_ore_previste = 0 and ld_costo_prev_risorse_est = 0 and not cbx_periodi_vuoti.checked then continue
		
		
		select prog_periodo
		into   :ll_prog_periodo
		from   det_budget_manut_periodi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :ll_progressivo and
				 num_periodo = :ll_num_periodo and
				 cod_attrezzatura = :ls_cod_attrezzatura and
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
				 flag_record_dati = 'S';
		
		if sqlca.sqlcode = 100 then
			// procedo con INSERT
			
			setnull(ls_cod_divisione)
			setnull(ls_cod_area_aziendale)
			setnull(ls_cod_reparto)
			setnull(ls_cod_cat_attrezzatura)
			
			select cod_area_aziendale,
			       cod_reparto,
					 cod_cat_attrezzature
			into   :ls_cod_area_aziendale,
			       :ls_cod_reparto,
					 :ls_cod_cat_attrezzatura
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_attrezzatura = :ls_cod_attrezzatura;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in select da anag_attrezzature~r~n" + sqlca.sqlerrtext)
				return
			end if
			
			if not isnull(ls_cod_area_aziendale) then
				select cod_divisione
				into   :ls_cod_divisione
				from   tab_aree_aziendali
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_area_aziendale = :ls_cod_area_aziendale;
			
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("OMNIA","Errore in select da anag_attrezzature~r~n" + sqlca.sqlerrtext)
					return -1
				end if
			end if		
			
			select max(prog_periodo)
			into   :ll_prog_periodo
			from   det_budget_manut_periodi
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 progressivo = :ll_progressivo and
					 num_periodo = :ll_num_periodo ;
			if isnull(ll_prog_periodo) or ll_prog_periodo = 0 then
				ll_prog_periodo = 1 
			else
				ll_prog_periodo ++
			end if
			
			INSERT INTO det_budget_manut_periodi  
					( cod_azienda,   
					  progressivo,   
					  num_periodo,   
					  prog_periodo,   
					  data_inizio,   
					  data_fine,   
					  cod_divisione,   
					  cod_area_aziendale,   
					  cod_reparto,   
					  cod_cat_attrezzature,   
					  cod_attrezzatura,   
					  costo_prev_ricambi,   
					  costo_prev_ricambi_str,   
					  ore_prev_risorse_int,
					  costo_ora_prev_risorse_int,
					  costo_prev_risorse_int,   
					  costo_prev_risorse_int_str,   
					  costo_prev_risorse_est,   
					  costo_prev_risorse_est_str,   
					  costo_cons_ricambi,   
					  costo_cons_ricambi_str,   
					  costo_cons_risorse_int,   
					  costo_cons_risorse_int_str,   
					  costo_cons_risorse_est,   
					  costo_cons_risorse_est_str,   
					  costo_trend_ricambi,   
					  costo_trend_risorse_int,   
					  costo_trend_risorse_est,   
					  flag_consolidato,   
					  flag_manuale,
					  flag_record_dati,
					  cod_tipo_manutenzione,
					  num_copie_attrezzatura)  
			VALUES ( :s_cs_xx.cod_azienda,   
					   :ll_progressivo,   
					   :ll_num_periodo,   
					   :ll_prog_periodo,   
					   :ldt_periodo_inizio,   
					   :ldt_periodo_fine,   
					   :ls_cod_divisione,   
					   :ls_cod_area_aziendale,   
					   :ls_cod_reparto,   
					   :ls_cod_cat_attrezzatura,   
					   :ls_cod_attrezzatura,   
						:ld_costo_prev_ricambi,   
						0,   
						:ld_operaio_ore_previste,
						:ld_costo_orario_operaio,
						:ld_costo_prev_risorse_int,   
						0,   
						:ld_costo_prev_risorse_est,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
					   'N',   
					   'S',
						'S',
						:ls_cod_tipo_manutenzione,
						:ll_num_copie_attrezzature)  ;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in INSERT det_budget_manut_periodi~r~n" + sqlca.sqlerrtext)
				return -1
			end if
					
		else	
			// procedo con UPDATE
			
			update det_budget_manut_periodi
			set   costo_prev_ricambi = costo_prev_ricambi + :ld_costo_prev_ricambi,  
			      costo_prev_risorse_int = costo_prev_risorse_int + :ld_costo_prev_risorse_int, 
				   costo_prev_risorse_est = costo_prev_risorse_est + :ld_costo_prev_risorse_est
			where cod_azienda = :s_cs_xx.cod_azienda and
			      progressivo = :ll_progressivo and
					num_periodo = :ll_num_periodo and
					prog_periodo = :ll_prog_periodo;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in UPDATE det_budget_manut_periodi~r~n" + sqlca.sqlerrtext)
				return -1
			end if
			
		end if
		
	
next

close(parent)
end event

type dw_folder from u_folder within w_det_budget_ins_periodi
integer x = 18
integer y = 12
integer width = 2606
integer height = 1840
integer taborder = 10
end type

type dw_risorse from uo_cs_xx_dw within w_det_budget_ins_periodi
integer x = 50
integer y = 108
integer width = 2505
integer height = 1708
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_det_budget_ins_periodi_4"
boolean border = false
end type

event itemchanged;call super::itemchanged;//long   ll_num_periodi, ll_i,ll_operaio_ore_previste,ll_operaio_minuti_previsti,ll_risorsa_ore_previste,ll_risorsa_minuti_previsti, &
//       ll_prog_periodo, ll_riga,ll_progressivo, ll_y
//
//string ls_flag_blocco,ls_cod_divisione_sel,ls_cod_area_aziendale_sel,ls_cod_reparto_sel,ls_cod_cat_attrezzature_sel, &
//       ls_cod_attrezzatura_sel,ls_flag_tipo_periodo, ls_cod_attrezzatura,ls_flag_tipo_intervento, ls_ricambio_non_codificato, &
//		 ls_cod_ricambio,ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_flag_combustibile, &
//		 ls_cod_attrezzatura_prec, ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzatura, &
//		 ls_cod_tipo_manutenzione
//
//dec{4} ld_costo_unitario_ricambio,ld_costo_orario_operaio,ld_risorsa_costo_orario,ld_risorsa_costi_aggiuntivi,&
//       ld_tot_costo_intervento,ld_costo_prev_ricambi,ld_costo_prev_risorse_int,ld_costo_prev_risorse_est, ld_tot_prog_costo, &
//		 ld_quan_ricambio,ld_totale, ld_tot_ore_risorsa_interna,ld_costo_ora_risorsa_interna, ld_tot_costo_risorsa_esterna, &
//		 ld_tot_prog_ore, ld_percento
//
//date     ld_data
//
//datetime ldt_data_inizio_elaborazione, ldt_data_consolidato,ldt_data_fine_elaborazione, ldt_data_registrazione, &
//         ldt_periodo_inizio[], ldt_periodo_fine[],ldt_oggi
//
//
//if dw_tipi_manutenzione.rowcount() < 1 then
//	messagebox("OMNIA", "Per questa attrezzatura non sono previste tipologie manutentive")
//	cb_avanti.triggerevent("ue_indietro")
//	return
//end if
//
//choose case i_colname
//	case "tot_ore_risorsa_interna"
//		ld_tot_ore_risorsa_interna = dec(i_coltext)			
//		ld_costo_ora_risorsa_interna = getitemnumber(getrow(),"costo_ora_risorsa_interna")
//		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
//		ld_tot_costo_risorsa_esterna = getitemnumber(getrow(),"tot_costo_risorsa_esterna")
//		ls_flag_combustibile = getitemstring(1, "flag_combustibile")
//		
//	case "costo_ora_risorsa_interna"
//		ld_costo_ora_risorsa_interna = dec(i_coltext)		
//		ld_tot_ore_risorsa_interna = getitemnumber(getrow(),"tot_ore_risorsa_interna")
//		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
//		ld_tot_costo_risorsa_esterna = getitemnumber(getrow(),"tot_costo_risorsa_esterna")
//		ls_flag_combustibile = getitemstring(1, "flag_combustibile")
//		
//	case "tot_costo_risorsa_esterna"
//		ld_tot_costo_risorsa_esterna = dec(i_coltext)		
//		ld_tot_ore_risorsa_interna = getitemnumber(getrow(),"tot_ore_risorsa_interna")
//		ld_costo_ora_risorsa_interna = getitemnumber(getrow(),"costo_ora_risorsa_interna")
//		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
//		ls_flag_combustibile = getitemstring(1, "flag_combustibile")
//		
//	case "flag_combustibile"
//		ls_flag_combustibile = i_coltext
//		ld_tot_costo_risorsa_esterna = getitemnumber(getrow(),"tot_costo_risorsa_esterna")
//		ld_tot_ore_risorsa_interna = getitemnumber(getrow(),"tot_ore_risorsa_interna")
//		ld_costo_ora_risorsa_interna = getitemnumber(getrow(),"costo_ora_risorsa_interna")
//		ld_totale = ld_tot_ore_risorsa_interna * ld_costo_ora_risorsa_interna
//		
//end choose
//
//ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")		
//
//uo_manutenzioni luo_manutenzioni
//
//ldt_oggi = datetime(today(),00:00:00)
//
//select data_inizio_elaborazione, 
//       flag_blocco, 
//		 data_consolidato,
//		 cod_divisione,
//		 cod_area_aziendale,
//		 cod_reparto,
//		 cod_cat_attrezzature,
//		 cod_attrezzatura,
//		 flag_tipo_periodo,
//		 num_periodi
//into   :ldt_data_inizio_elaborazione, 
//       :ls_flag_blocco, 
//		 :ldt_data_consolidato,
//		 :ls_cod_divisione_sel,
//		 :ls_cod_area_aziendale_sel,
//		 :ls_cod_reparto_sel,
//		 :ls_cod_cat_attrezzature_sel,
//		 :ls_cod_attrezzatura_sel,
//		 :ls_flag_tipo_periodo,
//		 :ll_num_periodi
//from   tes_budget_manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda and
//       progressivo = :ll_progressivo;
//		 
//if sqlca.sqlcode <> 0 then
//	messagebox("OMNIA","Errore in ricerca testata budget.~r~n" + sqlca.sqlerrtext)
//	return
//end if
//
//if ls_flag_blocco = "S" then
//	messagebox("OMNIA","Elaborazione interrotta: budget bloccato!")
//	return
//end if
//	
//if not isnull(ldt_data_consolidato) then
//	messagebox("OMNIA","Elaborazione interrotta: budget già consolidato!")
//	return
//end if
//
//if isnull(ldt_data_inizio_elaborazione) then
//	messagebox("OMNIA","Indicare una data inizio elaborazione!")
//	return
//end if
//
//// creo il vettore con i range di date che mi servono.
//
//luo_manutenzioni = create uo_manutenzioni
//
//ldt_data_registrazione = ldt_data_inizio_elaborazione
//
//for ll_i = 1 to ll_num_periodi
//
//	ldt_data_fine_elaborazione = luo_manutenzioni.uof_prossima_scadenza (ldt_data_registrazione , ls_flag_tipo_periodo, 1)
//	ldt_periodo_inizio[ll_i] = ldt_data_registrazione
//	
//	ld_data = date(ldt_data_fine_elaborazione)
//	ld_data = relativedate(ld_data, -1)
//	
//	ldt_periodo_fine[ll_i] = datetime(ld_data, 00:00:00)
//	
//	ldt_data_registrazione = ldt_data_fine_elaborazione
//
//next
//
//destroy luo_manutenzioni
//
//// carico la tabella dei periodi
//
//ls_cod_attrezzatura_prec = ""
//
//ls_flag_tipo_periodo = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "flag_tipo_periodo")		
//
//if ld_totale >= 0 then dw_periodi_ris_int.reset()
//if ld_tot_costo_risorsa_esterna >= 0 then dw_periodi_ris_est.reset()
//
//ld_tot_prog_costo = 0
//ld_tot_prog_ore   = 0
//
//if ls_flag_combustibile = "S" and ls_flag_tipo_periodo = "E" then
//	// in caso di totali suddivisi per combustibili
//	
//	// risorse interne
//	for ll_i = 1 to ll_num_periodi
//		
//		for ll_y = 1 to dw_tipi_manutenzione.rowcount()
//		
//			if ld_totale >= 0 then
//				ll_riga = dw_periodi_ris_int.insertrow(0)
//				dw_periodi_ris_int.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
//				dw_periodi_ris_int.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
//				dw_periodi_ris_int.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
//				
//				choose case month(date(ldt_periodo_inizio[ll_i]))
//					case 1
//						ld_percento = 22.5
//					case 2
//						ld_percento = 15.2
//					case 3
//						ld_percento = 13.5
//					case 4
//						ld_percento = 5.00
//					case 10
//						ld_percento = 5.80
//					case 11
//						ld_percento = 16.20
//					case 12
//						ld_percento = 21.80
//					case else
//						ld_percento = 0.00
//				end choose
//				
//				if ll_i <> ll_num_periodi then
//					dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", round((ld_tot_ore_risorsa_interna * ld_percento) / 100  ,2))
//					dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", round((ld_totale * ld_percento) / 100, 2))
//					ld_tot_prog_costo += round((ld_totale * ld_percento) / 100,2)
//					ld_tot_prog_ore += round((ld_tot_ore_risorsa_interna * ld_percento) / 100, 0)
//				else
//					if (ld_totale - ld_tot_prog_costo) >= 0 then
//						dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", ld_totale - ld_tot_prog_costo)
//					else
//						dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", 0)
//					end if
//					if (ld_tot_ore_risorsa_interna - ld_tot_prog_ore) >= 0 then
//						dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", ld_tot_ore_risorsa_interna - ld_tot_prog_ore)
//					else
//						dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", 0)
//					end if
//				end if		
//					
//			end if
//			
//		next
//		
//	next
//	
//	// risorse esterne
//	ld_tot_prog_costo = 0
//	for ll_i = 1 to ll_num_periodi
//		
//		for ll_y = 1 to dw_tipi_manutenzione.rowcount()
//		
//			if ld_tot_costo_risorsa_esterna >= 0 then
//				ll_riga = dw_periodi_ris_est.insertrow(0)
//				dw_periodi_ris_est.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
//				dw_periodi_ris_est.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
//				dw_periodi_ris_est.setitem(ll_riga, "costo_periodo", 0)
//				dw_periodi_ris_est.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
//			end if
//			
//		next
//		
//	next
//	
//else	
//	// gestione normale no combustibili
//	
//	// risorse interne
//	for ll_i = 1 to ll_num_periodi
//		
//		for ll_y = 1 to dw_tipi_manutenzione.rowcount()
//		
//			if ld_totale >= 0 then
//				ll_riga = dw_periodi_ris_int.insertrow(0)
//				dw_periodi_ris_int.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
//				dw_periodi_ris_int.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
//				if ll_i <> ll_num_periodi then
//					dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", round(ld_tot_ore_risorsa_interna/ ll_num_periodi,0))
//					dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", round(ld_totale / ll_num_periodi,2))
//					ld_tot_prog_costo += round(ld_totale / ll_num_periodi,2)
//					ld_tot_prog_ore += round(ld_tot_ore_risorsa_interna / ll_num_periodi,0)
//				else
//					dw_periodi_ris_int.setitem(ll_riga, "costo_periodo", ld_totale - ld_tot_prog_costo)
//					dw_periodi_ris_int.setitem(ll_riga, "ore_periodo", ld_tot_ore_risorsa_interna - ld_tot_prog_ore)
//				end if		
//				dw_periodi_ris_int.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
//			end if
//		next
//	next
//	
//	// risorse esterne
//	ld_tot_prog_costo = 0
//	for ll_i = 1 to ll_num_periodi
//		
//		for ll_y = 1 to dw_tipi_manutenzione.rowcount()
//		
//			if ld_tot_costo_risorsa_esterna >= 0 then
//				ll_riga = dw_periodi_ris_est.insertrow(0)
//				dw_periodi_ris_est.setitem(ll_riga, "data_inizio", ldt_periodo_inizio[ll_i])
//				dw_periodi_ris_est.setitem(ll_riga, "data_fine", ldt_periodo_fine[ll_i])
//				if ll_i <> ll_num_periodi then
//					dw_periodi_ris_est.setitem(ll_riga, "costo_periodo", round(ld_tot_costo_risorsa_esterna / ll_num_periodi,2))
//					ld_tot_prog_costo += round(ld_tot_costo_risorsa_esterna / ll_num_periodi,2)
//				else
//					dw_periodi_ris_est.setitem(ll_riga, "costo_periodo", ld_tot_costo_risorsa_esterna - ld_tot_prog_costo)
//				end if		
//				dw_periodi_ris_est.setitem(ll_riga, "cod_tipo_manutenzione", dw_tipi_manutenzione.getitemstring(ll_y,"cod_tipo_manutenzione") )
//					
//			end if
//
//		next
//		
//	next
//
//end if
//
//// applico il filtro
//ls_cod_tipo_manutenzione = dw_tipi_manutenzione.getitemstring(dw_tipi_manutenzione.getrow(),"cod_tipo_manutenzione")
//
//dw_periodi_ris_est.setfilter("cod_tipo_manutenzione = '" + ls_cod_tipo_manutenzione + "'")
//dw_periodi_ris_int.setfilter("cod_tipo_manutenzione = '" + ls_cod_tipo_manutenzione + "'")
//dw_periodi_ris_est.filter()
//dw_periodi_ris_int.filter()
//
//return
end event

type dw_periodi_ris_est from datawindow within w_det_budget_ins_periodi
event ue_totalizza ( )
event ue_filtra ( )
integer x = 133
integer y = 1156
integer width = 2217
integer height = 512
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_det_budget_ins_periodi_3"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_totalizza();long ll_i
dec{4} ld_tot_costo

ld_tot_costo = 0

for ll_i = 1 to rowcount()
	ld_tot_costo += getitemnumber(ll_i,"costo_periodo")
next

dw_tipi_manutenzione.setitem(dw_tipi_manutenzione.getrow(), "tot_costo_risorsa_esterna", ld_tot_costo)
dw_risorse.setitem(dw_risorse.getrow(), "tot_costo_risorsa_esterna", ld_tot_costo)

end event

event ue_filtra();string ls_cod_tipo_manutenzione

ls_cod_tipo_manutenzione = dw_tipi_manutenzione.getitemstring(dw_tipi_manutenzione.getrow(),"cod_tipo_manutenzione")

dw_periodi_ris_est.setfilter("cod_tipo_manutenzione = '" + ls_cod_tipo_manutenzione + "'")
dw_periodi_ris_est.filter()


end event

event itemfocuschanged;this.selecttext(1, len(this.gettext()))
end event

event getfocus;this.selecttext(1, len(this.gettext()))
end event

event itemchanged;postevent("ue_totalizza")
end event

event losefocus;dw_risorse.accepttext()
end event

type dw_periodi_ris_int from datawindow within w_det_budget_ins_periodi
event ue_totalizza ( )
event ue_filtra ( )
integer x = 128
integer y = 448
integer width = 2217
integer height = 512
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_det_budget_ins_periodi_2"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_totalizza();long ll_i
dec{4} ld_tot_costo, ld_tot_ore

ld_tot_costo = 0
ld_tot_ore   = 0

for ll_i = 1 to rowcount()
	ld_tot_costo += getitemnumber(ll_i,"costo_periodo")
	ld_tot_ore   += getitemnumber(ll_i,"ore_periodo")
next

dw_tipi_manutenzione.setitem(dw_tipi_manutenzione.getrow(), "tot_ore_risorsa_interna", ld_tot_ore)
dw_risorse.setitem(dw_risorse.getrow(), "tot_ore_risorsa_interna", ld_tot_ore)

end event

event ue_filtra();string ls_cod_tipo_manutenzione

ls_cod_tipo_manutenzione = dw_tipi_manutenzione.getitemstring(dw_tipi_manutenzione.getrow(),"cod_tipo_manutenzione")

dw_periodi_ris_int.setfilter("cod_tipo_manutenzione = '" + ls_cod_tipo_manutenzione + "'")
dw_periodi_ris_int.filter()


end event

event itemfocuschanged;this.selecttext(1, len(this.gettext()))
end event

event getfocus;this.selecttext(1, len(this.gettext()))

end event

event itemchanged;dec{4} ld_ore, ld_costo
if dwo.name = "ore_periodo" then
	//dw_det_budget_ins_periodi_1.accepttext()
	ld_ore = dec(data)
	ld_costo = dw_tipi_manutenzione.getitemnumber(dw_tipi_manutenzione.getrow(), "costo_ora_risorsa_interna")
	setitem(row,"costo_periodo", ld_costo * ld_ore )
end if

postevent("ue_totalizza")
end event

event losefocus;dw_risorse.accepttext()
end event

type st_des_tipo_manutenzione from statictext within w_det_budget_ins_periodi
integer x = 128
integer y = 136
integer width = 2217
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_tipi_manutenzione from datawindow within w_det_budget_ins_periodi
event ue_retrieve ( )
event ue_carica_periodi ( )
integer x = 46
integer y = 128
integer width = 2551
integer height = 1696
integer taborder = 50
string title = "none"
string dataobject = "d_det_budget_ins_periodi_5"
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_retrieve();long ll_i

settransobject(sqlca)

ll_i = retrieve(s_cs_xx.cod_azienda, dw_det_budget_ins_periodi_1.getitemstring(dw_det_budget_ins_periodi_1.getrow(),"cod_attrezzatura"))

if ll_i = 0 then
	g_mb.messagebox("OMNIA","Per questa attrezzatura non esiste alcuna tipologia di manutenzione dichiarata!")
	cb_avanti.triggerevent("ue_indietro")
end if

end event

event ue_carica_periodi();wf_carica_periodi()
end event

event itemchanged;if isvalid(dwo) then
	
	postevent("ue_carica_periodi")
	
end if
end event

type dw_det_budget_ins_periodi_1 from uo_cs_xx_dw within w_det_budget_ins_periodi
integer x = 46
integer y = 120
integer width = 2327
integer height = 284
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_det_budget_ins_periodi_1"
boolean border = false
end type

event losefocus;call super::losefocus;accepttext()
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_attrezzatura"
		if not isnull(i_coltext) and len(i_coltext) > 0 then
			parent.title = "Inserimento Costi Budget - Attrezzatura " + i_coltext
			dw_tipi_manutenzione.postevent("ue_retrieve")
		else
			parent.title = "Inserimento Costi Budget"	
			return
		end if
end choose

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_det_budget_ins_periodi_1,"cod_attrezzatura")
end choose
end event

