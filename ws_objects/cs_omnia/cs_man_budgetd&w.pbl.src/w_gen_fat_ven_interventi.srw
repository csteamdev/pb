﻿$PBExportHeader$w_gen_fat_ven_interventi.srw
$PBExportComments$Finestra Generazione Fatture Vendita da interventi
forward
global type w_gen_fat_ven_interventi from w_cs_xx_principale
end type
type st_1 from statictext within w_gen_fat_ven_interventi
end type
type cb_genera from uo_cb_ok within w_gen_fat_ven_interventi
end type
type dw_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven_interventi
end type
type dw_ext_sel_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven_interventi
end type
type dw_folder from u_folder within w_gen_fat_ven_interventi
end type
type dw_gen_orig_dest from uo_cs_xx_dw within w_gen_fat_ven_interventi
end type
type cb_1 from commandbutton within w_gen_fat_ven_interventi
end type
end forward

global type w_gen_fat_ven_interventi from w_cs_xx_principale
integer width = 3515
integer height = 2004
string title = "Fatturazione Interventi"
st_1 st_1
cb_genera cb_genera
dw_gen_fat_ven dw_gen_fat_ven
dw_ext_sel_gen_fat_ven dw_ext_sel_gen_fat_ven
dw_folder dw_folder
dw_gen_orig_dest dw_gen_orig_dest
cb_1 cb_1
end type
global w_gen_fat_ven_interventi w_gen_fat_ven_interventi

type variables
boolean ib_messaggio_retrieve=true
string is_cod_documento[], is_numeratore_documento[]
long il_anno_documento[], il_num_documento[]

end variables

forward prototypes
public function integer wf_crea_nota_fissa (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer wf_genera_fatture_singole_1 (long al_anno_registrazione, string fs_flag_risorse_esterne, string fs_flag_risorse_esterne_indip, string fs_flag_fattura, ref string ls_messaggio)
public subroutine wf_inserisci_testata (string fs_flag_fattura, string fs_cod_tipo_fat_ven, string fs_cod_cliente, ref long fl_anno_fattura, ref long fl_numero_fattura, ref string fs_errore)
public subroutine wf_inserisci_riga (long fl_anno_fattura, long fl_num_fattura, string fs_cod_tipo_det_ven, string fs_des_prodotto, ref string fs_errore, ref long fl_prog_fattura)
public function integer wf_aggiorna_riga (long fl_anno_fattura, long fl_numero_fattura, long fl_riga_fattura, string fs_flag_risorse_esterne, string fs_flag_risorse_esterne_indip, string fs_cod_tipo_det_ven, string fs_des_prodotto, string fs_note, long fl_tot_operaio, long fl_tot_risorsa, ref string fs_errore)
end prototypes

public function integer wf_crea_nota_fissa (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);string ls_cod_cliente, ls_nota_testata, ls_nota_piede, ls_nota_video, &
		 ls_db, ls_nota_testata_1, ls_nota_piede_1, ls_cod_tipo_fat_ven, ls_null
datetime ldt_data_registrazione
integer li_risposta

setnull(ls_null)
li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

			
select cod_cliente, 
		nota_testata,
		nota_piede,
		data_registrazione,
		cod_tipo_fat_ven
into   :ls_cod_cliente,
		:ls_nota_testata,
		:ls_nota_piede,
		:ldt_data_registrazione,
		:ls_cod_tipo_fat_ven
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca fattura per inserimento nota fissa: verificare"
	return -1
end if

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "GEN_FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
else
	ls_nota_testata_1 = ls_nota_testata
	ls_nota_piede_1 = ls_nota_piede
end if

// ----- controllo lunghezza note per ASE
if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
	ls_nota_testata_1 = left(ls_nota_testata_1, 255)
end if
if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
	ls_nota_piede_1 = left(ls_nota_piede_1, 255)
end if
// -----  fine controllo lunghezza note per ASE

update tes_fat_ven  
	set nota_testata = :ls_nota_testata_1,
		 nota_piede = :ls_nota_piede_1
 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_ven.anno_registrazione = :fl_anno_registrazione and  
		 tes_fat_ven.num_registrazione = :fl_num_registrazione;
		
if sqlca.sqlcode = -1 then
	fs_messaggio = "Si è verificato un errore in fase di aggiornamento note nella Testata Fattura Vendita."
	return -1
end if

return 0
end function

public function integer wf_genera_fatture_singole_1 (long al_anno_registrazione, string fs_flag_risorse_esterne, string fs_flag_risorse_esterne_indip, string fs_flag_fattura, ref string ls_messaggio);string     ls_db, ls_cdo_fornitore, ls_cod_tipo_fat_ven, ls_cod_tipo_det_ven_rif, ls_cod_tipo_det_ven_regis, &
           ls_cod_tipo_det_ven_ricambi, ls_flag_riferimento, ls_des_riferimento, ls_cod_cliente_appo, ls_cod_tipo_manutenzione_appo, ls_riferimento, &
			  ls_cod_cliente, ls_cod_tipo_manutenzione, ls_errore, ls_des_prodotto, ls_cod_attrezzatura, ls_des_tipo_manutenzione_appo, ls_des_attrezzatura

datetime   ldt_data_registrazione_appo, ldt_data_registrazione

integer    li_risposta

long       ll_tot_operaio, ll_tot_risorsa, ll_i, ll_anno_manut, ll_numero_manut, ll_numero_fattura, ll_prog_fattura, ll_operaio_ore_m, ll_operaio_minuti_m, &
           ll_risorsa_ore_m, ll_risorsa_minuti_m, ll_count, ll_ret

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

select cod_tipo_fat_ven,  
		 cod_tipo_det_ven_rif,
		 cod_tipo_det_ven_regis,
		 cod_tipo_det_ven_ricambi,
		 flag_riferimento,
		 des_riferimento
into   :ls_cod_tipo_fat_ven,  
		 :ls_cod_tipo_det_ven_rif,
		 :ls_cod_tipo_det_ven_regis,
		 :ls_cod_tipo_det_ven_ricambi,
		 :ls_flag_riferimento,
		 :ls_des_riferimento
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	ls_messaggio = "Errore durante la lettura della tabella Parametri Manutenzioni."
	return -1
end if

if isnull(ls_cod_tipo_fat_ven) or len(ls_cod_tipo_fat_ven) = 0 then
	ls_messaggio = "Attenzione: il tipo fattura vendita non può essere nullo nella tabella Parametri Manutenzioni!"
	return -1
end if

if isnull(ls_cod_tipo_det_ven_regis) or len(ls_cod_tipo_det_ven_regis) = 0 then
	ls_messaggio = "Attenzione: il tipo dettaglio registrazione attività non può essere nullo nella tabella Parametri Manutenzioni!"
	return -1
end if

if not isnull(ls_flag_riferimento) and ls_flag_riferimento = "S" then
	if isnull(ls_cod_tipo_det_ven_rif) or ls_cod_tipo_det_ven_rif = "" then
		ls_messaggio = "Attenzione: il tipo dettaglio riferimento  non può essere nullo nella tabella Parametri Manutenzioni!"
		return -1		
	end if
end if

ls_cod_cliente_appo = ""
ls_cod_tipo_manutenzione_appo = ""
ldt_data_registrazione_appo = datetime(date("01/01/1910"), 00:00:00)
ls_riferimento = "ATTIVITA' ESEGUITE SULLE SEGUENTI APPARECCHIATURE:"
ll_tot_operaio = 0
ll_tot_risorsa = 0
ll_count = 0

for ll_i = 1 to dw_gen_fat_ven.rowcount()
	
	ls_cod_cliente = dw_gen_fat_ven.getitemstring( ll_i, "cod_cliente")
	ldt_data_registrazione = dw_gen_fat_ven.getitemdatetime( ll_i, "data_registrazione")
	ls_cod_tipo_manutenzione = dw_gen_fat_ven.getitemstring( ll_i, "cod_tipo_manutenzione")
	ls_cod_attrezzatura = dw_gen_fat_ven.getitemstring( ll_i, "cod_attrezzatura")
	ll_anno_manut = dw_gen_fat_ven.getitemnumber( ll_i, "anno_registrazione")
	ll_numero_manut = dw_gen_fat_ven.getitemnumber( ll_i, "num_registrazione")	
	
	select des_tipo_manutenzione
	into   :ls_des_prodotto
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			 
	select descrizione
	into   :ls_des_attrezzatura
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura;
	
	ls_des_prodotto = left( ls_des_prodotto, 40)
	
	if not isnull(ls_cod_cliente) and ls_cod_cliente <> ls_cod_cliente_appo then
		
		if ll_count > 0 then
			ll_ret = wf_aggiorna_riga( al_anno_registrazione, &
												ll_numero_fattura, &
												ll_prog_fattura, &
												fs_flag_risorse_esterne, &
												fs_flag_risorse_esterne_indip, &
												ls_cod_tipo_det_ven_regis, &
												ls_des_tipo_manutenzione_appo, &
												ls_riferimento, &
												ll_tot_operaio, &
												ll_tot_risorsa, &
												ls_messaggio)
												
			if ll_ret < 0 then
				rollback;
				return -1
			end if						
		end if
		
		wf_inserisci_testata(fs_flag_fattura, ls_cod_tipo_fat_ven,ls_cod_cliente, al_anno_registrazione, ll_numero_fattura, ls_errore)
		if len(ls_errore) > 0 then
			ls_messaggio = ls_errore
			rollback;
			return -1
		end if
		if ls_flag_riferimento = "S" and not isnull(ls_flag_riferimento) and not isnull(ls_cod_tipo_det_ven_rif) then
			wf_inserisci_riga( al_anno_registrazione, ll_numero_fattura, ls_cod_tipo_det_ven_rif, ls_des_riferimento + " " + string(ldt_data_registrazione, "dd/mm/yyyy"), ls_errore, ll_prog_fattura)
			if len(ls_errore) > 0 then
				ls_messaggio = ls_errore
				rollback;
				return -1
			end if	
		end if								
		wf_inserisci_riga( al_anno_registrazione, ll_numero_fattura, ls_cod_tipo_det_ven_regis, ls_des_prodotto, ls_errore, ll_prog_fattura)
		if len(ls_errore) > 0 then
			ls_messaggio = ls_errore
			rollback;
			return -1
		end if	
		
		ls_cod_cliente_appo = ls_cod_cliente
		ls_cod_tipo_manutenzione_appo = ls_cod_tipo_manutenzione
		ldt_data_registrazione_appo = ldt_data_registrazione
		ls_riferimento = "ATTIVITA' ESEGUITE SULLE SEGUENTI APPARECCHIATURE:"
		ll_tot_operaio = 0
		ll_tot_risorsa = 0	
		ls_des_tipo_manutenzione_appo = ls_des_prodotto
		
	else
		if not isnull(ldt_data_registrazione) and ldt_data_registrazione <> ldt_data_registrazione_appo then
			
			
			if ll_count > 0 then				
				ll_ret = wf_aggiorna_riga( al_anno_registrazione, &
													ll_numero_fattura, &
													ll_prog_fattura, &
													fs_flag_risorse_esterne, &
													fs_flag_risorse_esterne_indip, &
													ls_cod_tipo_det_ven_regis, &
													ls_des_tipo_manutenzione_appo, &
													ls_riferimento, &
													ll_tot_operaio, &
													ll_tot_risorsa, &
													ls_messaggio)
													
				if ll_ret < 0 then
					rollback;
					return -1
				end if						
			end if			
			
			
			if ls_flag_riferimento = "S" and not isnull(ls_flag_riferimento) and not isnull(ls_cod_tipo_det_ven_rif) then
				wf_inserisci_riga( al_anno_registrazione, ll_numero_fattura, ls_cod_tipo_det_ven_rif, ls_des_riferimento + " " + string(ldt_data_registrazione, "dd/mm/yyyy"), ls_errore, ll_prog_fattura)
				if len(ls_errore) > 0 then
					ls_messaggio = ls_errore
					rollback;
					return -1
				end if	
			end if								

			wf_inserisci_riga( al_anno_registrazione, ll_numero_fattura, ls_cod_tipo_det_ven_regis, ls_des_prodotto, ls_errore, ll_prog_fattura)
			
			if len(ls_errore) > 0 then
				ls_messaggio = ls_errore
				rollback;
				return -1
			end if	
			
			ls_cod_cliente_appo = ls_cod_cliente
			ls_cod_tipo_manutenzione_appo = ls_cod_tipo_manutenzione
			ldt_data_registrazione_appo = ldt_data_registrazione
			ls_riferimento = "ATTIVITA' ESEGUITE SULLE SEGUENTI APPARECCHIATURE:"
			ll_tot_operaio = 0
			ll_tot_risorsa = 0	
			ls_des_tipo_manutenzione_appo = ls_des_prodotto
			
		else
			if not isnull(ls_cod_tipo_manutenzione) and ls_cod_tipo_manutenzione <> ls_cod_tipo_manutenzione_appo then
				
				
				if ll_count > 0 then
					ll_ret = wf_aggiorna_riga( al_anno_registrazione, &
														ll_numero_fattura, &
														ll_prog_fattura, &
														fs_flag_risorse_esterne, &
														fs_flag_risorse_esterne_indip, &
														ls_cod_tipo_det_ven_regis, &
														ls_des_tipo_manutenzione_appo, &
														ls_riferimento, &
														ll_tot_operaio, &
														ll_tot_risorsa, &
														ls_messaggio)
														
					if ll_ret < 0 then
						rollback;
						return -1
					end if						
				end if
				
				if ls_flag_riferimento = "S" and not isnull(ls_flag_riferimento) and not isnull(ls_cod_tipo_det_ven_rif) then
					wf_inserisci_riga( al_anno_registrazione, ll_numero_fattura, ls_cod_tipo_det_ven_rif, ls_des_riferimento + " " + string(ldt_data_registrazione, "dd/mm/yyyy"), ls_errore, ll_prog_fattura)
					if len(ls_errore) > 0 then
						ls_messaggio = ls_errore
						rollback;
						return -1
					end if	
				end if												
				
				wf_inserisci_riga( al_anno_registrazione, ll_numero_fattura, ls_cod_tipo_det_ven_regis, ls_des_prodotto, ls_errore, ll_prog_fattura)
				if len(ls_errore) > 0 then
					ls_messaggio = ls_errore
					rollback;
					return -1
				end if	
				
				ls_cod_cliente_appo = ls_cod_cliente
				ls_cod_tipo_manutenzione_appo = ls_cod_tipo_manutenzione
				ldt_data_registrazione_appo = ldt_data_registrazione
				ls_riferimento = "ATTIVITA' ESEGUITE SULLE SEGUENTI APPARECCHIATURE:"
				ll_tot_operaio = 0
				ll_tot_risorsa = 0	
				ls_des_tipo_manutenzione_appo = ls_des_prodotto
				
			end if
		end if
	end if
	
	select operaio_ore,
			 operaio_minuti,
			 risorsa_ore,
			 risorsa_minuti
	into   :ll_operaio_ore_m,
	       :ll_operaio_minuti_m,
			 :ll_risorsa_ore_m,
			 :ll_risorsa_minuti_m
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_manut and
			 num_registrazione = :ll_numero_manut;			 
	
	if isnull(ll_operaio_ore_m) then ll_operaio_ore_m = 0
	if isnull(ll_operaio_minuti_m) then ll_operaio_minuti_m = 0
	if isnull(ll_risorsa_ore_m) then ll_risorsa_ore_m = 0
	if isnull(ll_risorsa_minuti_m) then ll_risorsa_minuti_m = 0
	ll_tot_operaio += (ll_operaio_ore_m * 60) + ll_operaio_minuti_m
	ll_tot_risorsa += (ll_risorsa_ore_m * 60) + ll_risorsa_minuti_m
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""
	if isnull(ls_des_attrezzatura) then ls_des_attrezzatura = ""
	
	ls_riferimento += "~r~n" + ls_cod_attrezzatura + " " + ls_des_attrezzatura + " rif.(" + string(ll_anno_manut) + "/" + string(ll_numero_manut) + ")"
	
	update manutenzioni
	set    flag_fatturato = 'S',
			 anno_reg_fat_ven = :al_anno_registrazione,
			 num_reg_fat_ven = :ll_numero_fattura,
			 prog_riga_fat_ven = :ll_prog_fattura
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_manut and
			 num_registrazione = :ll_numero_manut;
	if sqlca.sqlcode <> 0 then 
		ls_messaggio = "Errore durante aggiornamento intervento:" + sqlca.sqlerrtext
		rollback;
		return -1
	end if		
	
	ll_count ++
	

next

if ll_count > 0 then
	ll_ret = wf_aggiorna_riga( al_anno_registrazione, &
										ll_numero_fattura, &
										ll_prog_fattura, &
										fs_flag_risorse_esterne, &
										fs_flag_risorse_esterne_indip, &
										ls_cod_tipo_det_ven_regis, &
										ls_des_tipo_manutenzione_appo, &
										ls_riferimento, &
										ll_tot_operaio, &
										ll_tot_risorsa, &
										ls_messaggio)
												
	if ll_ret < 0 then
		rollback;
		return -1
	end if							
end if

return 0
end function

public subroutine wf_inserisci_testata (string fs_flag_fattura, string fs_cod_tipo_fat_ven, string fs_cod_cliente, ref long fl_anno_fattura, ref long fl_numero_fattura, ref string fs_errore);long    ll_num_registrazione, ll_max_registrazione, ll_anno_registrazione_ft, ll_num_registrazione_ft, ll_riga

string  ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
        ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_agente_1, &
        ls_cod_agente_2, ls_cod_banca_clien_for, ls_cod_banca, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
        ls_cod_resa, ls_flag_fuori_fido, ls_flag_blocco, ls_cod_tipo_analisi, ls_causale_trasporto_tab, ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, &
		  ls_des_riferimento_fat_ven, ls_flag_rif_cod_documento, ls_flag_rif_numeratore_doc, ls_flag_rif_anno_documento, ls_flag_rif_num_documento, &
		  ls_flag_rif_data_fattura, ls_cod_causale_tab, ls_lire, ls_cod_des_fattura_com, ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, &
		  ls_localita_dest_fat, ls_frazione_dest_fat, ls_cap_dest_fat, ls_provincia_dest_fat

datetime ldt_data_blocco, ldt_data_registrazione, ldt_data_registrazione_ft

decimal  ld_sconto, ld_sconto_pagamento

boolean  lb_crea

lb_crea = false

if fs_flag_fattura = "S" then
	
	select anno_registrazione,
	       num_registrazione,
			 data_registrazione
	into   :ll_anno_registrazione_ft,
	       :ll_num_registrazione_ft,
			 :ldt_data_registrazione_ft
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :fs_cod_cliente and
			 cod_documento is null and
			 ( num_documento is null or num_documento = 0 );
			 
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante il controllo di fatture non contabilizzate: " + sqlca.sqlerrtext
		return
	elseif sqlca.sqlcode = 100 then
		lb_crea = true
	elseif sqlca.sqlcode = 0 then
		ll_riga = dw_gen_orig_dest.insertrow(0)
		dw_gen_orig_dest.setitem( ll_riga, "anno_documento", ll_anno_registrazione_ft)
		dw_gen_orig_dest.setitem( ll_riga, "num_documento", ll_num_registrazione_ft)
		dw_gen_orig_dest.setitem( ll_riga, "data_documento", ldt_data_registrazione_ft)
		fl_anno_fattura = ll_anno_registrazione_ft
		fl_numero_fattura = ll_num_registrazione_ft
		fs_errore = ""
		return		
		
	end if
	
end if

select con_fat_ven.num_registrazione
into   :ll_num_registrazione
from   con_fat_ven
where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura della tabella Controllo Fatture Vendita."
	return 
end if

ll_num_registrazione ++
	
ll_max_registrazione = 0
		
select max(num_registrazione)
into   :ll_max_registrazione
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_fattura;
			 
if not isnull(ll_max_registrazione) then
	if ll_max_registrazione >= ll_num_registrazione then
		ll_num_registrazione = ll_max_registrazione + 1
	end if
end if
	
update con_fat_ven
set    con_fat_ven.num_registrazione = :ll_num_registrazione
where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita."
	return 
end if
	
select    anag_clienti.rag_soc_1,
          anag_clienti.rag_soc_2,
          anag_clienti.indirizzo,
          anag_clienti.frazione,
          anag_clienti.cap,
          anag_clienti.localita,
          anag_clienti.provincia,
          anag_clienti.telefono,
          anag_clienti.fax,
          anag_clienti.telex,
          anag_clienti.partita_iva,
          anag_clienti.cod_fiscale,
          anag_clienti.cod_deposito,
          anag_clienti.cod_valuta,
          anag_clienti.cod_tipo_listino_prodotto,
          anag_clienti.cod_pagamento,
          anag_clienti.sconto,
          anag_clienti.cod_agente_1,
          anag_clienti.cod_agente_2,
          anag_clienti.cod_banca_clien_for,
          anag_clienti.cod_banca,
          anag_clienti.cod_imballo,
          anag_clienti.cod_vettore,
          anag_clienti.cod_inoltro,
          anag_clienti.cod_mezzo,
          anag_clienti.cod_porto,
          anag_clienti.cod_resa,
          anag_clienti.flag_fuori_fido,
			 anag_clienti.flag_blocco,
			 anag_clienti.data_blocco
into      :ls_rag_soc_1,    
			 :ls_rag_soc_2,
			 :ls_indirizzo,    
			 :ls_frazione,
			 :ls_cap,
			 :ls_localita,
			 :ls_provincia,
			 :ls_telefono,
			 :ls_fax,
			 :ls_telex,
          :ls_partita_iva,
          :ls_cod_fiscale,
          :ls_cod_deposito,
          :ls_cod_valuta,
          :ls_cod_tipo_listino_prodotto,
          :ls_cod_pagamento,
          :ld_sconto,
          :ls_cod_agente_1,
          :ls_cod_agente_2,
          :ls_cod_banca_clien_for,
          :ls_cod_banca,
          :ls_cod_imballo,
          :ls_cod_vettore,
          :ls_cod_inoltro,
          :ls_cod_mezzo,
          :ls_cod_porto,
          :ls_cod_resa,
          :ls_flag_fuori_fido,
			 :ls_flag_blocco,
			 :ldt_data_blocco
from      anag_clienti
where     anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
          anag_clienti.cod_cliente = :fs_cod_cliente;
 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca del cliente: " + sqlca.sqlerrtext
	rollback;
	return 
end if
	
ldt_data_registrazione = datetime(today())

select tab_tipi_fat_ven.cod_tipo_analisi,
		 tab_tipi_fat_ven.causale_trasporto,
		 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
		 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
		 tab_tipi_fat_ven.des_riferimento_fat_ven,
		 tab_tipi_fat_ven.flag_rif_cod_documento,
		 tab_tipi_fat_ven.flag_rif_numeratore_doc,
		 tab_tipi_fat_ven.flag_rif_anno_documento,
		 tab_tipi_fat_ven.flag_rif_num_documento,
		 tab_tipi_fat_ven.flag_rif_data_fattura,
		 tab_tipi_fat_ven.cod_causale
into	 :ls_cod_tipo_analisi,
		 :ls_causale_trasporto_tab,
		 :ls_flag_abilita_rif_bol_ven,
		 :ls_cod_tipo_det_ven_rif_bol_ven,
		 :ls_des_riferimento_fat_ven,
		 :ls_flag_rif_cod_documento,
		 :ls_flag_rif_numeratore_doc,
		 :ls_flag_rif_anno_documento,
		 :ls_flag_rif_num_documento,
		 :ls_flag_rif_data_fattura,
		 :ls_cod_causale_tab
from   tab_tipi_fat_ven  
where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_fat_ven.cod_tipo_fat_ven = :fs_cod_tipo_fat_ven;

if sqlca.sqlcode <> 0 then
	fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture." + sqlca.sqlerrtext
	return 
end if
	
select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'S' and &
		 parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
	fs_errore = "Configurare il codice valuta per le Lire Italiane."
	return 
end if

select tab_pagamenti.sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
		 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	ld_sconto_pagamento = 0
end if

select cod_des_fattura
into   :ls_cod_des_fattura_com
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_fat_ven = :fs_cod_tipo_fat_ven;
			
if sqlca.sqlcode <> 0 then	
	setnull(ls_rag_soc_1_dest_fat)
	setnull(ls_rag_soc_2_dest_fat)
	setnull(ls_indirizzo_dest_fat)   
	setnull(ls_localita_dest_fat)   
	setnull(ls_frazione_dest_fat)   
	setnull(ls_cap_dest_fat)   
	setnull(ls_provincia_dest_fat)
else 
	select rag_soc_1,
			 rag_soc_2,
			 indirizzo,
			 localita,
			 frazione,
			 cap,
			 provincia
	into   :ls_rag_soc_1_dest_fat,
	  		 :ls_rag_soc_2_dest_fat,
			 :ls_indirizzo_dest_fat,
			 :ls_localita_dest_fat,
			 :ls_frazione_dest_fat,
			 :ls_cap_dest_fat,
			 :ls_provincia_dest_fat
	from   anag_des_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	 		 cod_cliente = :fs_cod_cliente and 
			 cod_des_cliente = :ls_cod_des_fattura_com;
			
	if sqlca.sqlcode <> 0 then
		setnull(ls_rag_soc_1_dest_fat)
		setnull(ls_rag_soc_2_dest_fat)
		setnull(ls_indirizzo_dest_fat)   
		setnull(ls_localita_dest_fat)   
		setnull(ls_frazione_dest_fat)   
		setnull(ls_cap_dest_fat)   
		setnull(ls_provincia_dest_fat)
	end if	
end if		

insert into tes_fat_ven
				(cod_azienda,   
				 anno_registrazione,   
				 num_registrazione,   
				 data_registrazione,  
				 cod_tipo_fat_ven,   
				 cod_cliente,
				 rag_soc_1,   
				 rag_soc_2,   
				 indirizzo,   
				 localita,   
				 frazione,   
				 cap,   
				 provincia,
		       rag_soc_1_dest_fat,
				 rag_soc_2_dest_fat,
				 indirizzo_dest_fat,
				 localita_dest_fat,
				 frazione_dest_fat,
				 cap_dest_fat,
				 provincia_dest_fat,					 
				 cod_deposito,   
				 cod_valuta,   
				 cod_tipo_listino_prodotto,   
				 cod_pagamento,   
				 cod_agente_1,
				 cod_agente_2,
				 cod_banca,    
				 cod_imballo,   
				 cod_vettore,   
				 cod_inoltro,   
				 cod_mezzo,
				 cod_porto,   
				 cod_resa,   
				 cod_documento,
				 numeratore_documento,
				 anno_documento,
				 num_documento,
				 data_fattura,
				 flag_fuori_fido,
				 flag_blocco,   
				 flag_movimenti,  
				 flag_contabilita,
				 tot_merci,
				 tot_spese_trasporto,
				 tot_spese_imballo,
				 tot_spese_bolli,
				 tot_spese_varie,
				 tot_sconto_cassa,
				 tot_sconti_commerciali,
				 imponibile_provvigioni_1,
				 imponibile_provvigioni_2,
				 tot_provvigioni_1,
				 tot_provvigioni_2,
				 importo_iva,
				 imponibile_iva,
				 importo_iva_valuta,
				 imponibile_iva_valuta,
				 tot_fattura,
				 tot_fattura_valuta,
				 flag_cambio_zero,
				 cod_banca_clien_for,
				 flag_st_note_tes,  					 					 
				 flag_st_note_pie)  					 
values      (:s_cs_xx.cod_azienda,   
				 :fl_anno_fattura,   
				 :ll_num_registrazione,   
				 :ldt_data_registrazione,    
				 :fs_cod_tipo_fat_ven,   
				 :fs_cod_cliente,
				 :ls_rag_soc_1,   
				 :ls_rag_soc_2,   
				 :ls_indirizzo,   
				 :ls_localita,   
				 :ls_frazione,   
				 :ls_cap,   
				 :ls_provincia,   
		       :ls_rag_soc_1_dest_fat,
		  		 :ls_rag_soc_2_dest_fat,
				 :ls_indirizzo_dest_fat,
				 :ls_localita_dest_fat,
				 :ls_frazione_dest_fat,
				 :ls_cap_dest_fat,
				 :ls_provincia_dest_fat,			 
				 :ls_cod_deposito,   
				 :ls_cod_valuta,   
				 :ls_cod_tipo_listino_prodotto,   
				 :ls_cod_pagamento,   
				 :ls_cod_agente_1,
				 :ls_cod_agente_2,
				 :ls_cod_banca,   
				 :ls_cod_imballo,   
				 :ls_cod_vettore,   
				 :ls_cod_inoltro,   
				 :ls_cod_mezzo,   
				 :ls_cod_porto,   
				 :ls_cod_resa,   
				 null,
				 null,
				 0,
				 0,
				 null,
				 'N',
				 'N',   
				 'S',
				 'N',
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 'N',
				 :ls_cod_banca_clien_for,
				 'I',
				 'I');					 

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la scrittura Testata Fatture Vendita."
	return 
end if

ll_riga = dw_gen_orig_dest.insertrow(0)
dw_gen_orig_dest.setitem( ll_riga, "anno_documento", fl_anno_fattura)
dw_gen_orig_dest.setitem( ll_riga, "num_documento", ll_num_registrazione)
dw_gen_orig_dest.setitem( ll_riga, "data_documento", ldt_data_registrazione)

fl_numero_fattura = ll_num_registrazione
fs_errore = ""
return
end subroutine

public subroutine wf_inserisci_riga (long fl_anno_fattura, long fl_num_fattura, string fs_cod_tipo_det_ven, string fs_des_prodotto, ref string fs_errore, ref long fl_prog_fattura);long    ll_prog_riga_fat_ven

string  ls_des_prodotto, ls_flag_stampa_nota

decimal ld_quan_consegnata, ld_prezzo_vendita, ld_fat_conversione_ven
			
select max(prog_riga_fat_ven)
into   :ll_prog_riga_fat_ven
from   det_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_fattura and
		 num_registrazione = :fl_num_fattura;
		 
if isnull(ll_prog_riga_fat_ven) then ll_prog_riga_fat_ven = 0

select flag_st_note_ft
into   :ls_flag_stampa_nota
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_det_ven = :fs_cod_tipo_det_ven;
			
ld_quan_consegnata = 0
ld_prezzo_vendita  = 0
ld_fat_conversione_ven = 1

ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10				
				
insert into det_fat_ven
			  (cod_azienda, 
				anno_registrazione,
				num_registrazione,
				prog_riga_fat_ven,
				cod_tipo_det_ven,
				cod_deposito,
				cod_ubicazione,
				cod_lotto,
				data_stock,
				progr_stock,
				cod_misura,
				cod_prodotto,
				des_prodotto,
				quan_fatturata,
				prezzo_vendita,
				sconto_1,
				sconto_2,
				provvigione_1,
				provvigione_2,
				cod_iva,
				cod_tipo_movimento,
				num_registrazione_mov_mag,
				anno_registrazione_fat,
				num_registrazione_fat,
				nota_dettaglio,
				anno_registrazione_bol_ven,
				num_registrazione_bol_ven,
				prog_riga_bol_ven,
				anno_commessa,
				num_commessa,
				cod_centro_costo,
				sconto_3,
				sconto_4,
				sconto_5,
				sconto_6,
				sconto_7,
				sconto_8,
				sconto_9,
				sconto_10,
				anno_registrazione_mov_mag,
				fat_conversione_ven,
				anno_reg_des_mov,
				num_reg_des_mov,
				anno_reg_ord_ven,
				num_reg_ord_ven,
				prog_riga_ord_ven,	
				cod_versione,
				num_confezioni,
				num_pezzi_confezione,
				flag_st_note_det,
				quantita_um,
				prezzo_um)							
	values  (:s_cs_xx.cod_azienda,
				:fl_anno_fattura,
				:fl_num_fattura,
				:ll_prog_riga_fat_ven,
				:fs_cod_tipo_det_ven,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				:fs_des_prodotto,
				0,
				0,
				0,
				0,
				0,
				0,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				null,
				1,
				null,
				null,
				null,
				null,
				null,
				null,
				0,
				0,
				:ls_flag_stampa_nota,
				0,
				0);

if sqlca.sqlcode = -1 then
	fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Fattura Vendita."
	return
end if
fl_prog_fattura = ll_prog_riga_fat_ven

fs_errore = ""
return 
end subroutine

public function integer wf_aggiorna_riga (long fl_anno_fattura, long fl_numero_fattura, long fl_riga_fattura, string fs_flag_risorse_esterne, string fs_flag_risorse_esterne_indip, string fs_cod_tipo_det_ven, string fs_des_prodotto, string fs_note, long fl_tot_operaio, long fl_tot_risorsa, ref string fs_errore);long       ll_prog_riga_risorse

decimal{2} ld_totale_operaio, ld_totale_risorsa, ld_totale

ld_totale_operaio = fl_tot_operaio/60
ld_totale_risorsa = fl_tot_risorsa/60
ld_totale = ld_totale_operaio

if fs_flag_risorse_esterne = "S" then
	
	if fs_flag_risorse_esterne_indip = "N" then
		
		ld_totale += ld_totale_risorsa
		
	else
		if ld_totale_risorsa > 0 then
			// inserisco una riga per le risorse
			wf_inserisci_riga(fl_anno_fattura, fl_numero_fattura, fs_cod_tipo_det_ven, fs_des_prodotto, fs_errore, ll_prog_riga_risorse)
			if len(fs_errore) > 0 then
				rollback;
				return -1			
			end if
			
			update det_fat_ven 
			set    quan_fatturata = :ld_totale_risorsa,
					 nota_dettaglio = :fs_note
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_fattura and
					 num_registrazione = :fl_numero_fattura and
					 prog_riga_fat_ven = :ll_prog_riga_risorse;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante l'aggiornamento della riga di fattura: " + sqlca.sqlerrtext
				rollback;
				return -1
			end if
		end if
	end if
	
end if

update det_fat_ven 
set    quan_fatturata = :ld_totale,
		 nota_dettaglio = :fs_note
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_fattura and
		 num_registrazione = :fl_numero_fattura and
		 prog_riga_fat_ven = :fl_riga_fattura;
		 
if sqlca.sqlcode <> 0 then		 
	fs_errore = "Errore durante l'aggiornamento della riga di fattura: " + sqlca.sqlerrtext
	rollback;
	return -1
end if		 

fs_errore = ""
return 0		 
end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


set_w_options(c_noenablepopup)

lw_oggetti[1] = dw_gen_orig_dest
dw_folder.fu_assigntab(2, "Fatture Generate", lw_oggetti[])
lw_oggetti[1] = dw_gen_fat_ven
lw_oggetti[2] = dw_ext_sel_gen_fat_ven
lw_oggetti[3] = cb_genera
//lw_oggetti[4] = cb_ricerca_cliente
lw_oggetti[4] = st_1
lw_oggetti[5] = cb_1
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)


dw_gen_orig_dest.set_dw_options(sqlca, &
                             	  pcca.null_object, &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete + &
										  c_disablecc + &
										  c_disableccinsert, &
	                             c_viewmodeblack)
													 
dw_ext_sel_gen_fat_ven.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_nomodify + &
												 c_nodelete + &
												 c_newonopen + &
												 c_disableCC, &
												 c_noresizedw + &
												 c_nohighlightselected + &
												 c_cursorrowpointer)
dw_gen_fat_ven.set_dw_options(sqlca, &
									  pcca.null_object, &
									  c_nonew + &
									  c_nomodify + &
									  c_nodelete + &
									  c_disablecc + &
									  c_noretrieveonopen + &
									  c_disableccinsert, &
									  c_ViewModeBorderUnchanged + &
									  c_ViewModeColorUnchanged + &
									  c_NoCursorRowFocusRect + &
									  c_NoCursorRowPointer)

iuo_dw_main = dw_gen_fat_ven												 
save_on_close(c_socnosave)

end event

on w_gen_fat_ven_interventi.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_genera=create cb_genera
this.dw_gen_fat_ven=create dw_gen_fat_ven
this.dw_ext_sel_gen_fat_ven=create dw_ext_sel_gen_fat_ven
this.dw_folder=create dw_folder
this.dw_gen_orig_dest=create dw_gen_orig_dest
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_genera
this.Control[iCurrent+3]=this.dw_gen_fat_ven
this.Control[iCurrent+4]=this.dw_ext_sel_gen_fat_ven
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_gen_orig_dest
this.Control[iCurrent+7]=this.cb_1
end on

on w_gen_fat_ven_interventi.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_genera)
destroy(this.dw_gen_fat_ven)
destroy(this.dw_ext_sel_gen_fat_ven)
destroy(this.dw_folder)
destroy(this.dw_gen_orig_dest)
destroy(this.cb_1)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW (dw_ext_sel_gen_fat_ven,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type st_1 from statictext within w_gen_fat_ven_interventi
integer x = 46
integer y = 788
integer width = 3383
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "ELENCO INTERVENTI TROVATI DALLA RICERCA ESEGUITA: TUTTI QUESTI INTERVENTI SARANNO FATTURATI"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_genera from uo_cb_ok within w_gen_fat_ven_interventi
event clicked pbm_bnclicked
integer x = 3040
integer y = 680
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;string ls_messaggio, ls_cod_valuta, ls_cod_lire, ls_cod_valuta_corrente, ls_flag_risorse_esterne, ls_flag_risorse_esterne_indip, ls_flag_fattura

long   ll_anno_registrazione, ll_anno_fat_ven, ll_num_fat_ven, ll_rows, ll_i

setpointer(hourglass!)

dw_gen_orig_dest.reset()

select parametri_azienda.numero
into   :ll_anno_registrazione
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'N' and &
		 parametri_azienda.cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore durante la lettura del parametro ESC.", Exclamation!, ok!)
	setpointer(arrow!)
	return
end if

ls_flag_risorse_esterne = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_risorse_esterne")
ls_flag_fattura = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_fattura")
ls_flag_risorse_esterne_indip = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_risorse_esterne_indip")

if wf_genera_fatture_singole_1(ll_anno_registrazione, ls_flag_risorse_esterne, ls_flag_risorse_esterne_indip, ls_flag_fattura, ls_messaggio) = -1 then
	g_mb.messagebox( "OMNIA",  ls_messaggio, Exclamation!, ok!)
	rollback;
	dw_gen_orig_dest.reset()
	setpointer(arrow!)
	return
else
	commit;
end if


dw_folder.fu_selecttab(2)
ib_messaggio_retrieve = false
dw_gen_fat_ven.triggerevent("pcd_retrieve")
ib_messaggio_retrieve = true
setpointer(arrow!)
end event

type dw_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven_interventi
event pcd_retrieve pbm_custom60
integer x = 46
integer y = 868
integer width = 3387
integer height = 1000
integer taborder = 50
string dataobject = "d_ext_gen_fat_ven_int"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;datetime ldt_data_da, ldt_data_a

string   ls_cod_cliente, ls_cod_tipo_manutenzione, ls_flag_risorse, ls_flag_fattura, ls_sql, ls_sintassi, ls_errore, ls_cod_attrezzatura, ls_des_attrezzatura, ls_des_tipo_manutenzione, &
         ls_cod_area_aziendale, ls_cod_divisione, ls_des_cliente, ls_des_divisione, ls_flag_risorse_esterne_indip, ls_flag_tipo_intervento

long     ll_errore, ll_i, ll_riga

datastore lds_datastore

dw_gen_fat_ven.reset()


ldt_data_da = dw_ext_sel_gen_fat_ven.getitemdatetime(1,"data_registrazione_da")
ldt_data_a   = dw_ext_sel_gen_fat_ven.getitemdatetime(1,"data_registrazione_a")
ls_cod_cliente = dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_cliente")
ls_cod_tipo_manutenzione = dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_tipo_manutenzione")
ls_flag_risorse = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_risorse_esterne")
ls_flag_fattura = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_fattura")
ls_flag_risorse_esterne_indip = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_risorse_esterne_indip")
ls_flag_tipo_intervento = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_tipo_intervento")

if isnull(ls_flag_risorse) then ls_flag_risorse = "N"

//       + "        ora_inizio_intervento, " &
//       + "        data_inizio_intervento, " &
//		 + "			ora_fine_intervento, " &
//		 + "			data_fine_intervento, " &		 

ls_sql = " SELECT anno_registrazione, " &
       + "        num_registrazione, " &
       + "        data_registrazione, " &
       + "        cod_attrezzatura, " &
       + "        cod_tipo_manutenzione, " &
		 + "			operaio_ore, " &		 		 
		 + "			operaio_minuti, " &		 
		 + "			risorsa_ore, " &		 		 
		 + "			risorsa_minuti " &		 		 
		 + " FROM   manutenzioni " &		 		 
		 + " WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_fatturato = 'N' and flag_eseguito = 'S' " &
		 
if not isnull(ldt_data_da) and string(ldt_data_da) <> "01/01/1900" then
	ls_sql += " and data_registrazione >= '" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_a) and string(ldt_data_a) <> "01/01/1900" then
	ls_sql += " and data_registrazione <= '" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	ls_sql += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione IN (select cod_divisione from anag_divisioni where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' ))) "	
end if

if not isnull(ls_cod_tipo_manutenzione) and ls_cod_tipo_manutenzione <> "" then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manutenzione + "' "	
end if

if not isnull(ls_flag_tipo_intervento) and ls_flag_tipo_intervento <> "T" then
	if ls_flag_tipo_intervento = "S" then
		ls_sql += " and flag_straordinario = 'S' "
	elseif ls_flag_tipo_intervento = "O" then
		ls_sql += " and flag_ordinario = 'S' "
	end if
end if

ls_sintassi = sqlca.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la generazione della sintassi sql: " + ls_errore)
   RETURN
END IF

lds_datastore = create datastore

lds_datastore.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore: " + ls_errore)
   RETURN
END IF

lds_datastore.settransobject(sqlca)

ll_errore = lds_datastore.retrieve()
if ll_errore < 0 then
   g_mb.messagebox( "OMNIA","Errore durante la ricerca degli interventi da fatturare:~r~n" + sqlca.sqlerrtext)
	destroy lds_datastore
	return
end if

dw_gen_fat_ven.setredraw(false)

for ll_i = 1 to ll_errore

	ls_cod_attrezzatura = lds_datastore.getitemstring(ll_i, "cod_attrezzatura")
	ls_cod_tipo_manutenzione = lds_datastore.getitemstring(ll_i, "cod_tipo_manutenzione")
	
	select descrizione,
	       cod_area_aziendale
	into   :ls_des_attrezzatura,
	       :ls_cod_area_aziendale
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura;
			 
	
	select des_tipo_manutenzione
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			 
	select cod_cliente,
	       cod_divisione
	into   :ls_cod_cliente,
	       :ls_cod_divisione
	from   anag_divisioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_divisione = (select cod_divisione from tab_aree_aziendali where cod_azienda = :s_cs_xx.cod_azienda and cod_area_aziendale = :ls_cod_area_aziendale);
	
	select rag_soc_1
	into   :ls_des_cliente
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
			 
	select des_divisione
	into   :ls_des_divisione
	from   anag_divisioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_divisione = :ls_cod_divisione;
			 
	if isnull(ls_cod_cliente) or len(ls_cod_cliente) = 0 then continue
	
	ll_riga = dw_gen_fat_ven.insertrow(0)			
	dw_gen_fat_ven.setitem(ll_riga, "data_registrazione",lds_datastore.getitemdatetime(ll_i, "data_registrazione"))
	dw_gen_fat_ven.setitem(ll_riga, "anno_registrazione",lds_datastore.getitemnumber(ll_i, "anno_registrazione"))
	dw_gen_fat_ven.setitem(ll_riga, "num_registrazione",lds_datastore.getitemnumber(ll_i, "num_registrazione") )
	dw_gen_fat_ven.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
	dw_gen_fat_ven.setitem(ll_riga, "des_attrezzatura", ls_des_attrezzatura)
	dw_gen_fat_ven.setitem(ll_riga, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
	dw_gen_fat_ven.setitem(ll_riga, "des_tipo_manutenzione", ls_des_tipo_manutenzione)
	dw_gen_fat_ven.setitem(ll_riga, "cod_divisione", ls_cod_divisione)
	dw_gen_fat_ven.setitem(ll_riga, "des_divisione", ls_des_divisione)	
	dw_gen_fat_ven.setitem(ll_riga, "cod_cliente", ls_cod_cliente)
	dw_gen_fat_ven.setitem(ll_riga, "des_cliente", ls_des_cliente)		
	dw_gen_fat_ven.setitem(ll_riga, "operaio_ore", lds_datastore.getitemnumber(ll_i, "operaio_ore"))
	dw_gen_fat_ven.setitem(ll_riga, "operaio_minuti", lds_datastore.getitemnumber(ll_i, "operaio_minuti"))
	if ls_flag_risorse = "S" then
		dw_gen_fat_ven.setitem(ll_riga, "risorsa_ore", lds_datastore.getitemnumber(ll_i, "risorsa_ore"))
		dw_gen_fat_ven.setitem(ll_riga, "risorsa_minuti", lds_datastore.getitemnumber(ll_i, "risorsa_minuti"))	
	else
		dw_gen_fat_ven.setitem(ll_riga, "risorsa_ore", 0)
		dw_gen_fat_ven.setitem(ll_riga, "risorsa_minuti", 0)			
	end if
	dw_gen_fat_ven.setitem(ll_riga, "flag_risorse_esterne", ls_flag_risorse)

next

destroy lds_datastore

dw_gen_fat_ven.SetSort(" cod_cliente ASC, data_registrazione ASC, cod_tipo_manutenzione ASC")
dw_gen_fat_ven.setredraw(true)

dw_gen_fat_ven.Reset_DW_Modified(c_ResetChildren)
dw_ext_sel_gen_fat_ven.Reset_DW_Modified(c_ResetChildren)


end event

type dw_ext_sel_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven_interventi
integer x = 46
integer y = 128
integer width = 2930
integer height = 676
integer taborder = 20
string dataobject = "d_ext_sel_gen_fat_ven_int"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ext_sel_gen_fat_ven,"cod_cliente")
end choose
end event

type dw_folder from u_folder within w_gen_fat_ven_interventi
integer x = 23
integer y = 20
integer width = 3429
integer height = 1868
integer taborder = 40
end type

type dw_gen_orig_dest from uo_cs_xx_dw within w_gen_fat_ven_interventi
event pcd_retrieve pbm_custom60
integer x = 46
integer y = 140
integer width = 3383
integer height = 1640
integer taborder = 30
string dataobject = "d_gen_orig_dest_fat_int"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type cb_1 from commandbutton within w_gen_fat_ven_interventi
integer x = 1989
integer y = 600
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_ext_sel_gen_fat_ven.accepttext()
dw_ext_sel_gen_fat_ven.change_dw_focus(dw_gen_fat_ven)
parent.postevent("pc_retrieve")

end event

