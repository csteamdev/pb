﻿$PBExportHeader$w_tes_budget_manut_consolida.srw
forward
global type w_tes_budget_manut_consolida from w_cs_xx_risposta
end type
type cb_2 from commandbutton within w_tes_budget_manut_consolida
end type
type cb_1 from commandbutton within w_tes_budget_manut_consolida
end type
type dw_tes_budget_manut_consolida from uo_cs_xx_dw within w_tes_budget_manut_consolida
end type
end forward

global type w_tes_budget_manut_consolida from w_cs_xx_risposta
integer width = 2190
integer height = 956
string title = "Cosolida Budget"
cb_2 cb_2
cb_1 cb_1
dw_tes_budget_manut_consolida dw_tes_budget_manut_consolida
end type
global w_tes_budget_manut_consolida w_tes_budget_manut_consolida

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_tes_budget_manut_consolida.set_dw_options(sqlca, &
                            i_openparm, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
s_cs_xx.parametri.parametro_s_1 = "ANNULLA"
end event

on w_tes_budget_manut_consolida.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_tes_budget_manut_consolida=create dw_tes_budget_manut_consolida
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_tes_budget_manut_consolida
end on

on w_tes_budget_manut_consolida.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_tes_budget_manut_consolida)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_budget_manut_consolida,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

		

end event

type cb_2 from commandbutton within w_tes_budget_manut_consolida
integer x = 1358
integer y = 756
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "ANNULLA"
close(parent)
end event

type cb_1 from commandbutton within w_tes_budget_manut_consolida
integer x = 1746
integer y = 756
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = dw_tes_budget_manut_consolida.getitemstring(dw_tes_budget_manut_consolida.getrow(),"cod_divisione")
if isnull(s_cs_xx.parametri.parametro_s_1) then
	g_mb.messagebox("OMNIA","L'indicazione della divisione è obbligatoria!")
	return
end if

s_cs_xx.parametri.parametro_s_2 = dw_tes_budget_manut_consolida.getitemstring(dw_tes_budget_manut_consolida.getrow(),"cod_area_aziendale")

s_cs_xx.parametri.parametro_s_3 = dw_tes_budget_manut_consolida.getitemstring(dw_tes_budget_manut_consolida.getrow(),"cod_reparto")

if isnull(s_cs_xx.parametri.parametro_s_2) then
	g_mb.messagebox("OMNIA","Ai fini del corretto uso di OMNIA l'indicazione dell'area è obbligatorio; sarà preso quello della attrezzatura di origine.")
	return
end if

if isnull(s_cs_xx.parametri.parametro_s_3) then
	g_mb.messagebox("OMNIA","Ai fini del corretto uso di OMNIA l'indicazione del reparto è obbligatorio; sarà preso quello della attrezzatura di origine.")
	return
end if

close(parent)
end event

type dw_tes_budget_manut_consolida from uo_cs_xx_dw within w_tes_budget_manut_consolida
integer x = 14
integer y = 8
integer width = 2130
integer height = 732
integer taborder = 10
string dataobject = "d_tes_budget_manut_consolida"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_cod_divisione, ls_cod_reparto, ls_cod_area_aziendale

ls_cod_divisione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_divisione")		
ls_cod_area_aziendale = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_area_aziendale")		
ls_cod_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")		


f_PO_LoadDDDW_DW(dw_tes_budget_manut_consolida,"cod_area_aziendale",sqlca,&
					  "tab_aree_aziendali","cod_area_aziendale","des_area",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_cod_divisione + "'")

f_PO_LoadDDDW_DW(dw_tes_budget_manut_consolida,"cod_reparto",sqlca,&
					  "anag_reparti","cod_reparto","des_reparto",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" +ls_cod_area_aziendale + "'")

setitem(getrow(),"cod_divisione", ls_cod_divisione)
setitem(getrow(),"cod_area_aziendale", ls_cod_area_aziendale)
setitem(getrow(),"cod_reparto", ls_cod_reparto)


end event

event itemchanged;call super::itemchanged;choose case i_colname
		
	case "cod_divisione"
		
		f_PO_LoadDDDW_DW(dw_tes_budget_manut_consolida,"cod_area_aziendale",sqlca,&
							  "tab_aree_aziendali","cod_area_aziendale","des_area",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + i_coltext + "'")
		
	case "cod_area_aziendale"
		
		f_PO_LoadDDDW_DW(dw_tes_budget_manut_consolida,"cod_reparto",sqlca,&
							  "anag_reparti","cod_reparto","des_reparto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + i_coltext + "'")
							  
end choose
		

end event

