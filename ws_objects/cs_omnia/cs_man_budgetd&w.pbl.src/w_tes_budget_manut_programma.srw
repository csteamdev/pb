﻿$PBExportHeader$w_tes_budget_manut_programma.srw
forward
global type w_tes_budget_manut_programma from w_cs_xx_risposta
end type
type cb_2 from commandbutton within w_tes_budget_manut_programma
end type
type cb_1 from commandbutton within w_tes_budget_manut_programma
end type
type dw_tes_budget_manut_programma_1 from uo_cs_xx_dw within w_tes_budget_manut_programma
end type
type dw_tes_budget_manut_programma_2 from uo_cs_xx_dw within w_tes_budget_manut_programma
end type
end forward

global type w_tes_budget_manut_programma from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 2409
integer height = 948
string title = "Programma Budget"
event ue_inizio ( )
cb_2 cb_2
cb_1 cb_1
dw_tes_budget_manut_programma_1 dw_tes_budget_manut_programma_1
dw_tes_budget_manut_programma_2 dw_tes_budget_manut_programma_2
end type
global w_tes_budget_manut_programma w_tes_budget_manut_programma

type variables
string is_tipo_programmazione
end variables

event ue_inizio();dw_tes_budget_manut_programma_1.setitem( 1, "data_programmazione", s_cs_xx.parametri.parametro_data_1)
dw_tes_budget_manut_programma_1.setitem( 1, "tipo_programmazione", "S")
dw_tes_budget_manut_programma_1.setitem( 1, "flag_scadenze", "S")

dw_tes_budget_manut_programma_2.setitem( 1, "tipo_programmazione", "S")
dw_tes_budget_manut_programma_2.setitem( 1, "da_data", s_cs_xx.parametri.parametro_data_2)
dw_tes_budget_manut_programma_2.setitem( 1, "flag_scadenze_esistenti", "S")


setnull(s_cs_xx.parametri.parametro_data_1)
setnull(s_cs_xx.parametri.parametro_data_2)
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_tes_budget_manut_programma_1.set_dw_options(sqlca, &
                            i_openparm, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
dw_tes_budget_manut_programma_2.set_dw_options(sqlca, &
                            i_openparm, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)									 
									 
if s_cs_xx.parametri.parametro_s_1 = "S" then
	dw_tes_budget_manut_programma_1.visible = true
	dw_tes_budget_manut_programma_2.visible = false
	is_tipo_programmazione = "S"
else
	dw_tes_budget_manut_programma_2.visible = true
	dw_tes_budget_manut_programma_1.visible = false	
	is_tipo_programmazione = "P"
end if

postevent("ue_inizio")
end event

on w_tes_budget_manut_programma.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_tes_budget_manut_programma_1=create dw_tes_budget_manut_programma_1
this.dw_tes_budget_manut_programma_2=create dw_tes_budget_manut_programma_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_tes_budget_manut_programma_1
this.Control[iCurrent+4]=this.dw_tes_budget_manut_programma_2
end on

on w_tes_budget_manut_programma.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_tes_budget_manut_programma_1)
destroy(this.dw_tes_budget_manut_programma_2)
end on

type cb_2 from commandbutton within w_tes_budget_manut_programma
integer x = 32
integer y = 736
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_data_1)
setnull(s_cs_xx.parametri.parametro_data_2)
setnull(s_cs_xx.parametri.parametro_data_3)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_s_3)
setnull(s_cs_xx.parametri.parametro_s_4)

close(parent)
end event

type cb_1 from commandbutton within w_tes_budget_manut_programma
integer x = 1975
integer y = 736
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;long     ll_ret
datetime ldt_data_programmazione, ldt_da_data, ldt_a_data
string   ls_tipo_programmazione, ls_flag_scadenze, ls_flag_scadenze_esistenti


ll_ret = g_mb.messagebox( "OMNIA", "Attenzione: confermi i dati inseriti per la programmazione degli interventi?", Exclamation!, OKCancel!, 1)

if ll_ret <> 1 then
	return
end if

if is_tipo_programmazione = "S" then
	
	dw_tes_budget_manut_programma_1.accepttext()
	ldt_data_programmazione = dw_tes_budget_manut_programma_1.getitemdatetime( 1, "data_programmazione")
	ls_tipo_programmazione = dw_tes_budget_manut_programma_1.getitemstring( 1, "tipo_programmazione")
	ls_flag_scadenze = dw_tes_budget_manut_programma_1.getitemstring( 1, "flag_scadenze")
	
	if isnull(ldt_data_programmazione) then
		g_mb.messagebox( "OMNIA", "Attenzione: indicare la data di inizio programmazione!", stopsign!)
		return
	end if
	
	s_cs_xx.parametri.parametro_data_1 = ldt_data_programmazione
	setnull(s_cs_xx.parametri.parametro_data_2)
	setnull(s_cs_xx.parametri.parametro_data_3)
	s_cs_xx.parametri.parametro_s_2 = ls_tipo_programmazione
	s_cs_xx.parametri.parametro_s_3 =  ls_flag_scadenze
	setnull(s_cs_xx.parametri.parametro_s_4)
	
else 
	
	dw_tes_budget_manut_programma_2.accepttext()
	ldt_da_data = dw_tes_budget_manut_programma_2.getitemdatetime( 1, "da_data")
	ldt_a_data = dw_tes_budget_manut_programma_2.getitemdatetime( 1, "a_data")
	ls_flag_scadenze_esistenti = dw_tes_budget_manut_programma_2.getitemstring( 1, "flag_scadenze_esistenti")
	ls_tipo_programmazione = dw_tes_budget_manut_programma_2.getitemstring( 1, "tipo_programmazione")
	
	if isnull(ldt_da_data) or isnull(ldt_a_data) then
		g_mb.messagebox( "OMNIA", "Attenzione: indicare il periodo per la programmazione!", stopsign!)
		return
	end if
	
	setnull(s_cs_xx.parametri.parametro_data_1)
	s_cs_xx.parametri.parametro_data_2 = ldt_da_data
	s_cs_xx.parametri.parametro_data_3 = ldt_a_data
	s_cs_xx.parametri.parametro_s_2 = ls_tipo_programmazione
	setnull(s_cs_xx.parametri.parametro_s_3)
	s_cs_xx.parametri.parametro_s_4 = ls_flag_scadenze_esistenti
	
end if

close(parent)
end event

type dw_tes_budget_manut_programma_1 from uo_cs_xx_dw within w_tes_budget_manut_programma
integer y = 60
integer width = 2331
integer height = 640
integer taborder = 10
string dataobject = "d_tes_budget_manut_programma_1"
end type

type dw_tes_budget_manut_programma_2 from uo_cs_xx_dw within w_tes_budget_manut_programma
boolean visible = false
integer y = 60
integer width = 2331
integer height = 640
integer taborder = 10
string dataobject = "d_tes_budget_manut_programma_2"
end type

