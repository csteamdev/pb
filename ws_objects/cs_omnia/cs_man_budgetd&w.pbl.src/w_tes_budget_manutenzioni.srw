﻿$PBExportHeader$w_tes_budget_manutenzioni.srw
forward
global type w_tes_budget_manutenzioni from w_cs_xx_principale
end type
type em_prossimo_rinnovo from editmask within w_tes_budget_manutenzioni
end type
type st_prossimo_rinnovo from statictext within w_tes_budget_manutenzioni
end type
type st_1 from statictext within w_tes_budget_manutenzioni
end type
type hpb_1 from hprogressbar within w_tes_budget_manutenzioni
end type
type cb_respingi from commandbutton within w_tes_budget_manutenzioni
end type
type cb_invia from commandbutton within w_tes_budget_manutenzioni
end type
type cb_comunicazioni from commandbutton within w_tes_budget_manutenzioni
end type
type cb_revisione from commandbutton within w_tes_budget_manutenzioni
end type
type cb_consuntivo from commandbutton within w_tes_budget_manutenzioni
end type
type cb_consolida from commandbutton within w_tes_budget_manutenzioni
end type
type cb_sblocco from commandbutton within w_tes_budget_manutenzioni
end type
type cb_blocco from commandbutton within w_tes_budget_manutenzioni
end type
type cb_elabora from commandbutton within w_tes_budget_manutenzioni
end type
type dw_tes_budget_manutenzioni_det_1 from uo_cs_xx_dw within w_tes_budget_manutenzioni
end type
type cb_cerca from commandbutton within w_tes_budget_manutenzioni
end type
type dw_tes_budget_manutenzioni_lista from uo_cs_xx_dw within w_tes_budget_manutenzioni
end type
type dw_folder from u_folder within w_tes_budget_manutenzioni
end type
type dw_det_budget_manut_periodi from uo_cs_xx_dw within w_tes_budget_manutenzioni
end type
type dw_det_budget_manut_attrezzature_totali from uo_cs_xx_dw within w_tes_budget_manutenzioni
end type
type dw_ricerca from u_dw_search within w_tes_budget_manutenzioni
end type
end forward

global type w_tes_budget_manutenzioni from w_cs_xx_principale
integer width = 4494
integer height = 2452
string title = "Budget Manutenzioni"
em_prossimo_rinnovo em_prossimo_rinnovo
st_prossimo_rinnovo st_prossimo_rinnovo
st_1 st_1
hpb_1 hpb_1
cb_respingi cb_respingi
cb_invia cb_invia
cb_comunicazioni cb_comunicazioni
cb_revisione cb_revisione
cb_consuntivo cb_consuntivo
cb_consolida cb_consolida
cb_sblocco cb_sblocco
cb_blocco cb_blocco
cb_elabora cb_elabora
dw_tes_budget_manutenzioni_det_1 dw_tes_budget_manutenzioni_det_1
cb_cerca cb_cerca
dw_tes_budget_manutenzioni_lista dw_tes_budget_manutenzioni_lista
dw_folder dw_folder
dw_det_budget_manut_periodi dw_det_budget_manut_periodi
dw_det_budget_manut_attrezzature_totali dw_det_budget_manut_attrezzature_totali
dw_ricerca dw_ricerca
end type
global w_tes_budget_manutenzioni w_tes_budget_manutenzioni

forward prototypes
public function integer wf_carica_manut_simulate (long fl_progressivo, ref string fs_errore)
public function integer wf_carica_manut_periodi (long fl_progressivo, ref string fs_errore)
public function integer wf_carica_totali_periodi (long fl_progressivo, ref string fs_errore)
public function string wf_aggiungi_colonna (string fs_flag_tipo_colonna, string fs_string)
public function integer wf_carica_consuntivo (long fl_progressivo, ref string fs_errore)
public function string wf_crea_codice (string fs_codice)
public function integer wf_nuova_attrezzatura (string fs_cod_attrezzatura, string fs_cod_reparto, string fs_cod_area, ref string fs_errore, ref string fs_cod_attrezzatura_new)
public function integer wf_copia_tipologie (long fl_progressivo, string fs_cod_attrezzatura_modello, string fs_cod_attrezzatura, string fs_periodicita, long fl_frequenza, ref string fs_errore, ref string fs_tipi_manutenzione[])
public function integer wf_programmazione_semplice (long fl_progressivo, datetime fdt_data, string fs_tipo_data, string fs_flag_scadenze, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione[], ref string fs_errore, datastore fds_periodi)
public function integer wf_copia_periodi (long fl_progressivo, string fs_cod_divisione, string fs_cod_attrezzatura, string fs_cod_attrezzatura_new, ref string fs_errore)
public function integer wf_programmazione_periodo (datetime fdt_da_data, datetime fdt_a_data, string fs_flag_scadenze_esistenti, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione[], ref string fs_errore)
public function integer wf_copia_budget (ref string fs_errore)
public function integer wf_rinnova ()
public function integer wf_consolida_rinnovo ()
public function integer wf_tipi_manutenzione (long fl_progressivo, string fs_cod_attrezzatura_modello, ref string fs_tipi_manutenzione[], ref string fs_errore)
end prototypes

public function integer wf_carica_manut_simulate (long fl_progressivo, ref string fs_errore);/* Questa funzione provvede a caricare da data a data la tabella det_budget_manu_simulate
   partendo dai parametri presenti in tes_budget_manutenzioni.
	
	parametro		tipo		descrizione
	----------------------------------------------
	fl_progressivo	LONG		numero del budget
	fs_errore		STRING	messaggio di errore
	
	RETURN 
	0 = OK
	-1 = ERRORE
*/

long   ll_num_periodi,ll_anno_registrazione_prog,ll_num_registrazione_prog,ll_frequenza,ll_operaio_ore_previste,&
       ll_operaio_minuti_previsti,ll_risorsa_ore_previste,ll_risorsa_minuti_previsti,ll_prog_simulazione
string ls_flag_blocco,ls_cod_divisione_sel,ls_cod_area_aziendale_sel,ls_cod_reparto_sel,ls_cod_cat_attrezzature_sel, &
       ls_cod_attrezzatura_sel,ls_flag_tipo_periodo, ls_sql,ls_cod_attrezzatura,ls_cod_tipo_manutenzione, &
		 ls_flag_tipo_intervento,ls_periodicita,ls_cod_ricambio,ls_ricambio_non_codificato,ls_cod_operaio,&
		 ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna, ls_flag_scadenze
dec{4} ld_quan_ricambio,ld_costo_unitario_ricambio,ld_costo_orario_operaio,ld_risorsa_costo_orario,ld_risorsa_costi_aggiuntivi,&
       ld_tot_costo_intervento,ld_quan_utilizzo, ld_prezzo_ricambio
date   ld_data
datetime ldt_data_inizio_elaborazione, ldt_data_consolidato,ldt_data_fine_elaborazione, ldt_data_registrazione, ldt_null
uo_manutenzioni luo_manutenzioni

setnull(ldt_null)

select data_inizio_elaborazione, 
       flag_blocco, 
		 data_consolidato,
		 cod_divisione,
		 cod_area_aziendale,
		 cod_reparto,
		 cod_cat_attrezzature,
		 cod_attrezzatura,
		 flag_tipo_periodo,
		 num_periodi
into   :ldt_data_inizio_elaborazione, 
       :ls_flag_blocco, 
		 :ldt_data_consolidato,
		 :ls_cod_divisione_sel,
		 :ls_cod_area_aziendale_sel,
		 :ls_cod_reparto_sel,
		 :ls_cod_cat_attrezzature_sel,
		 :ls_cod_attrezzatura_sel,
		 :ls_flag_tipo_periodo,
		 :ll_num_periodi
from   tes_budget_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca testata budget.~r~n" + sqlca.sqlerrtext
	return -1
end if

if ls_flag_blocco = "S" then
	fs_errore = "Elaborazione interrotta: budget bloccato!"
	return -1
end if
	
if not isnull(ldt_data_consolidato) then
	fs_errore = "Elaborazione interrotta: budget già consolidato!"
	return -1
end if

if isnull(ldt_data_inizio_elaborazione) then
	fs_errore = "Indicare una data inizio elaborazione!"
	return -1
end if

ld_data = date(ldt_data_inizio_elaborazione)
ld_data = relativedate(ld_data, -1)
ldt_data_inizio_elaborazione = datetime(ld_data, 00:00:00)

luo_manutenzioni = create uo_manutenzioni
ldt_data_fine_elaborazione = luo_manutenzioni.uof_prossima_scadenza ( ldt_data_inizio_elaborazione, ls_flag_tipo_periodo, ll_num_periodi )
destroy luo_manutenzioni

// ---------------  prima di tutti cancello i dati delle manutenzioni  ----------------

delete det_budget_manut_simulate
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in cancellazione manutenzioni simulate.~r~n" + sqlca.sqlerrtext
	return -1
end if
		 
delete det_budget_manut_periodi
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo and
		 flag_manuale = 'N' ;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in cancellazione periodi di manutenzioni.~r~n" + sqlca.sqlerrtext
	return -1
end if
		 
update det_budget_manut_periodi
set    flag_consolidato = 'N'
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in rimozione consolidato.~r~n" + sqlca.sqlerrtext
	return -1
end if

update tes_budget_manutenzioni
set    data_consolidato = :ldt_null
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in rimozione consolidato.~r~n" + sqlca.sqlerrtext
	return -1
end if

// ---------------  procedo con lettura programmi di manutenzione ---------------------

declare cu_prog_manutenzioni dynamic cursor for SQLSA;

ls_sql = "select anno_registrazione, " + &
		"num_registrazione, " + &
		"cod_attrezzatura, " + &
		"cod_tipo_manutenzione, " + &
		"flag_scadenze, " + &
		"flag_tipo_intervento, " + &
		"periodicita, " + &
		"frequenza, " + &
		"cod_ricambio, " + &
		"ricambio_non_codificato, " + &
		"quan_ricambio, " + &
		"costo_unitario_ricambio, " + &
		"cod_operaio, " + &
		"costo_orario_operaio, " + &
		"operaio_ore_previste, " + &
		"operaio_minuti_previsti, " + &
		"cod_cat_risorse_esterne, " + &
		"cod_risorsa_esterna, " + &
		"risorsa_ore_previste, " + &
		"risorsa_minuti_previsti, " + &
		"risorsa_costo_orario, " + &
		"risorsa_costi_aggiuntivi, " + &
		"tot_costo_intervento " + &
" from programmi_manutenzione " + &
"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_budget = 'S' and flag_storico = 'N' and flag_blocco = 'N' " 

if not isnull(ls_cod_attrezzatura_sel) then
	ls_sql += " and cod_attrezzatura = '" + ls_cod_attrezzatura_sel + "' "
else
	if not isnull(ls_cod_reparto_sel) then
		ls_sql += " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_reparto = '" + ls_cod_reparto_sel + "')"
	elseif not isnull(ls_cod_area_aziendale_sel) then
		ls_sql += " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + ls_cod_area_aziendale_sel + "')"
	elseif not isnull(ls_cod_divisione_sel) then
		ls_sql += " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale in ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '"+ls_cod_divisione_sel+"') )"
	end if		
end if
	
		
prepare SQLSA from :ls_sql;

open cu_prog_manutenzioni;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in OPEN cu_prog_manutenzioni~r~n"+sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_prog_manutenzioni into :ll_anno_registrazione_prog,
											  :ll_num_registrazione_prog,
											  :ls_cod_attrezzatura,
											  :ls_cod_tipo_manutenzione,
											  :ls_flag_scadenze,
											  :ls_flag_tipo_intervento,
											  :ls_periodicita,
											  :ll_frequenza,
											  :ls_cod_ricambio,
											  :ls_ricambio_non_codificato,
											  :ld_quan_ricambio,
											  :ld_costo_unitario_ricambio,
											  :ls_cod_operaio,
											  :ld_costo_orario_operaio,
											  :ll_operaio_ore_previste,
											  :ll_operaio_minuti_previsti,
											  :ls_cod_cat_risorse_esterne,
											  :ls_cod_risorsa_esterna,
											  :ll_risorsa_ore_previste,
											  :ll_risorsa_minuti_previsti,
											  :ld_risorsa_costo_orario,
											  :ld_risorsa_costi_aggiuntivi,
											  :ld_tot_costo_intervento;													

	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in FETCH cu_prog_manutenzioni~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_periodicita) or ls_periodicita = "" then continue
	if isnull(ll_frequenza) then continue
	// calcolo il costo dei ricambi programmati.
	select sum(quan_utilizzo), sum(prezzo_ricambio)
	into   :ld_quan_utilizzo, :ld_prezzo_ricambio
	from   prog_manutenzioni_ricambi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione_prog and 
			 num_registrazione = :ll_num_registrazione_prog;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in totalizzazione ricambi~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// se c'è più di un ricambio programmato devo produrre un importo totale.
	if ld_quan_utilizzo > 0 then
		ld_costo_unitario_ricambio = (ld_quan_ricambio * ld_costo_unitario_ricambio ) + (ld_quan_utilizzo * ld_prezzo_ricambio)
		ld_quan_ricambio = 1
	end if

	// in base al programma di manutenzione procedo con la generazione delle scadenze.
	
	ldt_data_registrazione = ldt_data_inizio_elaborazione
	
	select max(prog_simulazione)
	into   :ll_prog_simulazione
	from   det_budget_manut_simulate
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       progressivo = :fl_progressivo;
	if isnull(ll_prog_simulazione) or ll_prog_simulazione < 1 then
		ll_prog_simulazione = 0
	end if
	
	do while ldt_data_registrazione < ldt_data_fine_elaborazione
		luo_manutenzioni = create uo_manutenzioni
		ldt_data_registrazione = luo_manutenzioni.uof_prossima_scadenza ( ldt_data_registrazione, ls_periodicita, ll_frequenza)
		destroy luo_manutenzioni
		
		ll_prog_simulazione++
		
		INSERT INTO det_budget_manut_simulate  
         ( cod_azienda,   
           progressivo,   
           prog_simulazione,   
           cod_tipo_manutenzione,   
           cod_attrezzatura,   
           flag_tipo_intervento,   
           data_registrazione,   
           ricambio_non_codificato,   
           cod_prodotto,   
           quan_ricambio,   
           costo_unitario_ricambio,   
           cod_operaio,   
           costo_orario_operaio,   
           operaio_ore_previste,   
           operaio_minuti_previsti,   
           cod_cat_risorse_esterne,   
           cod_risorsa_esterna,   
           risorsa_ore_previste,   
           risorsa_minuti_previsti,   
           risorsa_costo_orario,   
           risorsa_costi_aggiuntivi,   
           tot_costo_intervento )  
	  values (:s_cs_xx.cod_azienda,
				 :fl_progressivo,
				 :ll_prog_simulazione,
				 :ls_cod_tipo_manutenzione,
				 :ls_cod_attrezzatura,
				 :ls_flag_tipo_intervento,
				 :ldt_data_registrazione,
				 :ls_ricambio_non_codificato,
				 :ls_cod_ricambio,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_operaio,
				 :ld_costo_orario_operaio,
				 :ll_operaio_ore_previste,
				 :ll_operaio_minuti_previsti,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ll_risorsa_ore_previste,
				 :ll_risorsa_minuti_previsti,
				 :ld_risorsa_costo_orario,
				 :ld_risorsa_costi_aggiuntivi,
				 :ld_tot_costo_intervento);
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in INSERT det_budget_manut_simulate~r~n"+sqlca.sqlerrtext
			return -1
		end if

	loop	
	
loop

return 0
	
end function

public function integer wf_carica_manut_periodi (long fl_progressivo, ref string fs_errore);/* Questa funzione provvede a caricare la tabella det_budget_manu_periodi 
   partendo dalle manutenzioni simulate
	
	parametro		tipo		descrizione
	----------------------------------------------
	fl_progressivo	LONG		numero del budget
	fs_errore		STRING	messaggio di errore
	
	RETURN 
	0 = OK
	-1 = ERRORE
*/

long   ll_num_periodi, ll_i,ll_operaio_ore_previste,ll_operaio_minuti_previsti,ll_risorsa_ore_previste,ll_risorsa_minuti_previsti, &
       ll_prog_periodo, ll_cont
string ls_flag_blocco,ls_cod_divisione_sel,ls_cod_area_aziendale_sel,ls_cod_reparto_sel,ls_cod_cat_attrezzature_sel, &
       ls_cod_attrezzatura_sel,ls_flag_tipo_periodo, ls_cod_attrezzatura,ls_flag_tipo_intervento, ls_ricambio_non_codificato, &
		 ls_cod_ricambio,ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, &
		 ls_cod_attrezzatura_prec, ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzatura, ls_cod_tipo_manutenzione
dec{4} ld_costo_unitario_ricambio,ld_costo_orario_operaio,ld_risorsa_costo_orario,ld_risorsa_costi_aggiuntivi,&
       ld_tot_costo_intervento,ld_costo_prev_ricambi,ld_costo_prev_risorse_int,ld_costo_prev_risorse_est, &
		 ld_quan_ricambio, ld_ore_prev_risorse_int
date     ld_data
datetime ldt_data_inizio_elaborazione, ldt_data_consolidato,ldt_data_fine_elaborazione, ldt_data_registrazione, &
         ldt_periodo_inizio[], ldt_periodo_fine[],ldt_oggi
			
uo_manutenzioni luo_manutenzioni

DECLARE cu_simulate CURSOR FOR  
SELECT cod_attrezzatura,   
		ricambio_non_codificato,   
		cod_prodotto,   
		quan_ricambio,   
		costo_unitario_ricambio,   
		cod_operaio,   
		costo_orario_operaio,   
		operaio_ore_previste,   
		operaio_minuti_previsti,   
		cod_cat_risorse_esterne,   
		cod_risorsa_esterna,   
		risorsa_ore_previste,   
		risorsa_minuti_previsti,   
		risorsa_costo_orario,   
		risorsa_costi_aggiuntivi,
		cod_tipo_manutenzione
 FROM det_budget_manut_simulate  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		progressivo = :fl_progressivo AND  
		data_registrazione >= :ldt_periodo_inizio[ll_i] AND  
		data_registrazione <= :ldt_periodo_fine[ll_i]   
ORDER BY cod_attrezzatura ASC  ;



ldt_oggi = datetime(today(),00:00:00)


select data_inizio_elaborazione, 
       flag_blocco, 
		 data_consolidato,
		 cod_divisione,
		 cod_area_aziendale,
		 cod_reparto,
		 cod_cat_attrezzature,
		 cod_attrezzatura,
		 flag_tipo_periodo,
		 num_periodi
into   :ldt_data_inizio_elaborazione, 
       :ls_flag_blocco, 
		 :ldt_data_consolidato,
		 :ls_cod_divisione_sel,
		 :ls_cod_area_aziendale_sel,
		 :ls_cod_reparto_sel,
		 :ls_cod_cat_attrezzature_sel,
		 :ls_cod_attrezzatura_sel,
		 :ls_flag_tipo_periodo,
		 :ll_num_periodi
from   tes_budget_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca testata budget.~r~n" + sqlca.sqlerrtext
	return -1
end if

if ls_flag_blocco = "S" then
	fs_errore = "Elaborazione interrotta: budget bloccato!"
	return -1
end if
	
if not isnull(ldt_data_consolidato) then
	fs_errore = "Elaborazione interrotta: budget già consolidato!"
	return -1
end if

if isnull(ldt_data_inizio_elaborazione) then
	fs_errore = "Indicare una data inizio elaborazione!"
	return -1
end if

// creo il vettore con i range di date che mi servono.

luo_manutenzioni = create uo_manutenzioni

ldt_data_registrazione = ldt_data_inizio_elaborazione

for ll_i = 1 to ll_num_periodi

	ldt_data_fine_elaborazione = luo_manutenzioni.uof_prossima_scadenza (ldt_data_registrazione , ls_flag_tipo_periodo, 1)
	ldt_periodo_inizio[ll_i] = ldt_data_registrazione
	
	ld_data = date(ldt_data_fine_elaborazione)
	ld_data = relativedate(ld_data, -1)
	
	ldt_periodo_fine[ll_i] = datetime(ld_data, 00:00:00)
	
	ldt_data_registrazione = ldt_data_fine_elaborazione

next

destroy luo_manutenzioni

// carico la tabella dei periodi

ls_cod_attrezzatura_prec = ""

for ll_i = 1 to ll_num_periodi

	open cu_simulate;
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore in OPEN cu_simulate~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		fetch cu_simulate into  :ls_cod_attrezzatura, 
										:ls_ricambio_non_codificato, 
										:ls_cod_ricambio, 
										:ld_quan_ricambio, 
										:ld_costo_unitario_ricambio, 
										:ls_cod_operaio,   
										:ld_costo_orario_operaio, 
										:ll_operaio_ore_previste, 
										:ll_operaio_minuti_previsti,  
										:ls_cod_cat_risorse_esterne, 
										:ls_cod_risorsa_esterna, 
										:ll_risorsa_ore_previste,   
										:ll_risorsa_minuti_previsti, 
										:ld_risorsa_costo_orario, 
										:ld_risorsa_costi_aggiuntivi,
										:ls_cod_tipo_manutenzione;
										
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH cu_simulate~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_quan_ricambio) then ld_quan_ricambio = 0
		if isnull(ld_costo_unitario_ricambio) then ld_costo_unitario_ricambio = 0
		
		ld_costo_prev_ricambi = ld_quan_ricambio * ld_costo_unitario_ricambio
		
		if isnull(ld_costo_orario_operaio) then ld_costo_orario_operaio = 0
		if isnull(ll_operaio_ore_previste) then ll_operaio_ore_previste = 0
		if isnull(ll_operaio_minuti_previsti) then ll_operaio_minuti_previsti = 0
		ld_ore_prev_risorse_int = (ll_operaio_ore_previste + (ll_operaio_minuti_previsti / 60))
		ld_costo_prev_risorse_int = ld_costo_orario_operaio * ld_ore_prev_risorse_int

		if isnull(ld_risorsa_costo_orario) then ld_risorsa_costo_orario = 0
		if isnull(ll_risorsa_ore_previste) then ll_risorsa_ore_previste = 0
		if isnull(ll_risorsa_minuti_previsti) then ll_risorsa_minuti_previsti = 0
		if isnull(ld_risorsa_costi_aggiuntivi) then ld_risorsa_costi_aggiuntivi = 0
		ld_costo_prev_risorse_est = ld_risorsa_costo_orario * (ll_risorsa_ore_previste + (ll_risorsa_minuti_previsti / 60)) + ld_risorsa_costi_aggiuntivi
		
		select prog_periodo
		into   :ll_prog_periodo
		from   det_budget_manut_periodi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :fl_progressivo and
				 num_periodo = :ll_i and
				 cod_attrezzatura = :ls_cod_attrezzatura and
				 flag_record_dati = 'S';
		
		if sqlca.sqlcode = 100 then
			// procedo con INSERT
			
			setnull(ls_cod_divisione)
			setnull(ls_cod_area_aziendale)
			setnull(ls_cod_reparto)
			setnull(ls_cod_cat_attrezzatura)
			
			select cod_area_aziendale,
			       cod_reparto,
					 cod_cat_attrezzature
			into   :ls_cod_area_aziendale,
			       :ls_cod_reparto,
					 :ls_cod_cat_attrezzatura
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_attrezzatura = :ls_cod_attrezzatura;
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in select da anag_attrezzature~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			if not isnull(ls_cod_area_aziendale) then
				select cod_divisione
				into   :ls_cod_divisione
				from   tab_aree_aziendali
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_area_aziendale = :ls_cod_area_aziendale;
			
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore in select da anag_attrezzature~r~n" + sqlca.sqlerrtext
					return -1
				end if
			end if		
			
			select max(prog_periodo)
			into   :ll_prog_periodo
			from   det_budget_manut_periodi
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 progressivo = :fl_progressivo and
					 num_periodo = :ll_i ;
			if isnull(ll_prog_periodo) or ll_prog_periodo = 0 then
				ll_prog_periodo = 1 
			else
				ll_prog_periodo ++
			end if
			
			INSERT INTO det_budget_manut_periodi  
					( cod_azienda,   
					  progressivo,   
					  num_periodo,   
					  prog_periodo,   
					  data_inizio,   
					  data_fine,   
					  cod_divisione,   
					  cod_area_aziendale,   
					  cod_reparto,   
					  cod_cat_attrezzature,   
					  cod_attrezzatura,   
					  costo_prev_ricambi,   
					  costo_prev_ricambi_str,   
					  ore_prev_risorse_int,
					  costo_ora_prev_risorse_int,
					  costo_prev_risorse_int,   
					  costo_prev_risorse_int_str,   
					  costo_prev_risorse_est,   
					  costo_prev_risorse_est_str,   
					  costo_cons_ricambi,   
					  costo_cons_ricambi_str,   
					  costo_cons_risorse_int,   
					  costo_cons_risorse_int_str,   
					  costo_cons_risorse_est,   
					  costo_cons_risorse_est_str,   
					  costo_trend_ricambi,   
					  costo_trend_risorse_int,   
					  costo_trend_risorse_est,   
					  flag_consolidato,   
					  flag_manuale,
					  flag_record_dati,
					  cod_tipo_manutenzione,
					  num_copie_attrezzatura)  
			VALUES ( :s_cs_xx.cod_azienda,   
					   :fl_progressivo,   
					   :ll_i,   
					   :ll_prog_periodo,   
					   :ldt_periodo_inizio[ll_i],   
					   :ldt_periodo_fine[ll_i],   
					   :ls_cod_divisione,   
					   :ls_cod_area_aziendale,   
					   :ls_cod_reparto,   
					   :ls_cod_cat_attrezzatura,   
					   :ls_cod_attrezzatura,   
						:ld_costo_prev_ricambi,   
						0,   
						:ld_ore_prev_risorse_int,
						:ld_costo_orario_operaio,
						:ld_costo_prev_risorse_int,   
						0,   
						:ld_costo_prev_risorse_est,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
					   'N',   
					   'N',
						'S',
						:ls_cod_tipo_manutenzione,
						1)  ;
			
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in INSERT det_budget_manut_periodi~r~n" + sqlca.sqlerrtext
				return -1
			end if
					
		else	
			// procedo con UPDATE
			
			update det_budget_manut_periodi
			set   costo_prev_ricambi = costo_prev_ricambi + :ld_costo_prev_ricambi,  
			      costo_prev_risorse_int = costo_prev_risorse_int + :ld_costo_prev_risorse_int, 
				   costo_prev_risorse_est = costo_prev_risorse_est + :ld_costo_prev_risorse_est
			where cod_azienda = :s_cs_xx.cod_azienda and
			      progressivo = :fl_progressivo and
					num_periodo = :ll_i and
					prog_periodo = :ll_prog_periodo;

			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in UPDATE det_budget_manut_periodi~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
		end if
		
		
	loop
	
	close cu_simulate;
	
next

update tes_budget_manutenzioni
set    data_elaborazione = :ldt_oggi
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in UPDATE data ultima elaborazione~r~n" + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer wf_carica_totali_periodi (long fl_progressivo, ref string fs_errore);/* Questa funzione provvede a caricare i totali raggruppati in det_budget_manu_periodi 
	
	parametro		tipo		descrizione
	----------------------------------------------
	fl_progressivo	LONG		numero del budget
	fs_errore		STRING	messaggio di errore
	
	RETURN 
	0 = OK
	-1 = ERRORE
*/

long   ll_i,ll_livello_strato, ll_y, ll_livello,ll_rows, ll_z, ll_t, ll_max, ll_num_periodo, ll_num_copie_attrezzatura
string ls_flag_tipo_livello_1, ls_flag_tipo_livello_2, ls_flag_tipo_livello_3, &
       ls_flag_tipo_livello_4, ls_flag_tipo_livello_5, ls_colonne_sql, ls_group_sql, &
		 ls_sql, ls_errore, ls_syntax, ls_error, ls_colonne_livelli[5],ls_flag_tipo_livello[5], &
		 ls_sql_insert,ls_sql_values, ls_test
double ld_valore /// LASCIARE QUESTA VARIABILE COME DOUBLE
datetime ldt_data_inizio, ldt_data_fine
datastore lds_data	
boolean   lb_attrezzatura

//dec{4} 	

select flag_tipo_livello_1, 
		 flag_tipo_livello_2, 
		 flag_tipo_livello_3, 
		 flag_tipo_livello_4, 
		 flag_tipo_livello_5
into   :ls_flag_tipo_livello[1], 
		 :ls_flag_tipo_livello[2], 
		 :ls_flag_tipo_livello[3], 
		 :ls_flag_tipo_livello[4], 
		 :ls_flag_tipo_livello[5]
from   tes_budget_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :fl_progressivo;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca testata budget.~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_livello = 0
for ll_i = 1 to 5
	if ls_flag_tipo_livello[ll_i] = "N" then exit
	ls_colonne_livelli[ll_i] = wf_aggiungi_colonna(ls_flag_tipo_livello[ll_i], "")
	ll_livello = ll_i
next

// nessun livello psecificato, esco.
if ll_livello = 0 then return 0

// inizio composizione SQL

lb_attrezzatura = false

for ll_y = ll_livello to 1 step -1

	ls_colonne_sql = ""
	ls_group_sql = " GROUP BY num_periodo "
	ll_livello_strato = ll_y
	
	// creo SQL con i livelli appropriati
	for ll_i = 1 to ll_livello_strato
		
		if isnull(ls_flag_tipo_livello[ll_i]) or ls_flag_tipo_livello[ll_i] = "N" then continue

		ls_colonne_sql = wf_aggiungi_colonna(ls_flag_tipo_livello[ll_i], ls_colonne_sql)
		ls_group_sql   = wf_aggiungi_colonna(ls_flag_tipo_livello[ll_i], ls_group_sql)
		
		if ls_flag_tipo_livello[ll_i] = "A" then
			lb_attrezzatura = true
		end if
				
	
	next
	
	ls_colonne_sql = ls_colonne_sql + ", sum(costo_prev_ricambi * num_copie_attrezzatura) " + &
											  + ", sum(costo_prev_ricambi_str * num_copie_attrezzatura)  " + &
											  + ", sum(costo_prev_risorse_int * num_copie_attrezzatura) " + &
											  + ", sum(costo_prev_risorse_int_str * num_copie_attrezzatura) " + &
											  + ", sum(costo_prev_risorse_est * num_copie_attrezzatura) " + &
											  + ", sum(costo_prev_risorse_est_str * num_copie_attrezzatura) " + &
											  + ", sum(costo_cons_ricambi)" + &
											  + ", sum(costo_cons_ricambi_str)" + &
											  + ", sum(costo_cons_risorse_int)" + &
											  + ", sum(costo_cons_risorse_int_str)" + &
											  + ", sum(costo_cons_risorse_est)" + &
											  + ", sum(costo_cons_risorse_est_str)"
											  
	
	ls_sql = "select num_periodo, " + ls_colonne_sql + " from det_budget_manut_periodi "
	
	ls_sql += " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and progressivo = " + string(fl_progressivo) + " and flag_record_dati = 'S' "
	
	ls_sql += ls_group_sql
	
	
	// creo datastore
	
	ls_syntax = sqlca.syntaxfromsql(ls_sql, "", ls_errore)
	if ls_syntax = "" then
		fs_errore = ls_errore
		return -1
	end if
	
	lds_data = CREATE datastore
	lds_data.create(ls_syntax, ls_error)
	lds_data.settransobject(sqlca)
	ll_rows = lds_data.retrieve()
	

	// procedo con lettura dei totali ottenuti
	integer li_appo, ll_ret
	string  ls_nome, ls_tipo,ls_VAL
	
	for ll_z = 1 to ll_rows
		ls_sql_insert = ""
		ls_sql_values = ""
		for ll_t = 1 to ll_livello_strato
			if len(ls_sql_insert) > 0 then ls_sql_insert += ", "
			if len(ls_sql_values) > 0 then ls_sql_values += ", "
			ls_sql_insert = ls_sql_insert + ls_colonne_livelli[ll_t]
			li_appo = ll_t + 1
			

			ls_nome = lds_data.Describe("#" + string(li_appo) + ".Name")
			ls_tipo = lds_data.Describe( ls_nome + ".ColType")
			ls_VAL = lds_data.getitemstring(ll_z, ls_nome)
			if isnull(ls_val) then
				g_mb.messagebox( "OMNIA", "Attenzione la colonna " + ls_nome + " risulta avere valore nullo. Controllare la riga " + string(ll_z) + ".")
			end if
			ls_sql_values = ls_sql_values + "'" + lds_data.getitemstring(ll_z, li_appo) + "'"
		next
		
		ls_sql = "INSERT into det_budget_manut_periodi (cod_azienda, progressivo, num_periodo, prog_periodo, data_inizio, data_fine, " + ls_sql_insert + &
		         ", costo_prev_ricambi, costo_prev_ricambi_str, costo_prev_risorse_int, costo_prev_risorse_int_str "  + &
					", costo_prev_risorse_est, costo_prev_risorse_est_str, costo_cons_ricambi, costo_cons_ricambi_str "  + &
					", costo_cons_risorse_int, costo_cons_risorse_int_str, costo_cons_risorse_est, costo_cons_risorse_est_str " + &
					", flag_consolidato, flag_manuale, flag_record_dati) "
		ls_sql += " VALUES ('" + s_cs_xx.cod_azienda + "', " + string(fl_progressivo)
		          
		ll_num_periodo = lds_data.getitemnumber(ll_z, 1)
		
		select data_inizio, data_fine, max(prog_periodo)
		into   :ldt_data_inizio, :ldt_data_fine, :ll_max
		from   det_budget_manut_periodi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :fl_progressivo and
				 num_periodo = :ll_num_periodo
		group by data_inizio, data_fine;
		
		if ll_max = 0 or isnull(ll_max) then 
			// questo caso non dovrebbe MAI essere possibile dato che devono sempre esserci dei dati.
			ll_max = 1
		else
			ll_max ++
		end if
		
		ls_sql += ", " + string(ll_num_periodo) + ", " + string(ll_max) + ", " + &
		          "'" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "', " + &
		          "'" + string(ldt_data_fine,   s_cs_xx.db_funzioni.formato_data) + "', "
					 
		for ll_t = 1 to 12
			ld_valore = lds_data.getitemnumber(ll_z, ll_t + ll_livello_strato + 1)
			if isnull(ld_valore) then ld_valore = 0
			ls_sql_values +=  "," + f_double_to_string(ld_valore)
		next
		
		ls_sql_values +=  ",'N','N','N')"
		
		ls_sql += ls_sql_values
		
		execute immediate :ls_sql;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in inserimento totali budget.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// *** imposto il numero di copie attrezzatura
		
		setnull(ls_test)
		
		select cod_attrezzatura
		into   :ls_test
		from   det_budget_manut_periodi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :fl_progressivo and
				 num_periodo = :ll_num_periodo and 
				 prog_periodo = :ll_max;
				 
		if sqlca.sqlcode = 0 and not isnull(ls_test) and ls_test <> "" then
			
			select max(num_copie_attrezzatura)
			into   :ll_num_copie_attrezzatura
			from   det_budget_manut_periodi
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       progressivo = :fl_progressivo and
					 cod_attrezzatura = :ls_test and
					 flag_record_dati = 'S';
					 
			if sqlca.sqlcode = 0 then
				
				update det_budget_manut_periodi
				set    num_copie_attrezzatura = :ll_num_copie_attrezzatura
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 progressivo = :fl_progressivo and
						 num_periodo = :ll_num_periodo and 
						 prog_periodo = :ll_max;
				
			end if
			
		end if
		
	next
	
	destroy lds_data
next

return 0

end function

public function string wf_aggiungi_colonna (string fs_flag_tipo_colonna, string fs_string);choose case fs_flag_tipo_colonna
	case "D"
		if len(fs_string) > 0 then fs_string = fs_string + ", "
		fs_string = fs_string + "cod_divisione"

	case "E"
		if len(fs_string) > 0 then fs_string = fs_string + ", "
		fs_string = fs_string + "cod_area_aziendale"

	
	case "R"
		if len(fs_string) > 0 then fs_string = fs_string + ", "
		fs_string = fs_string + "cod_reparto"


	case "C"
		if len(fs_string) > 0 then fs_string = fs_string + ", "
		fs_string = fs_string + "cod_cat_attrezzature"

	case "A"
		if len(fs_string) > 0 then fs_string = fs_string + ", "
		fs_string = fs_string + "cod_attrezzatura"

end choose

return fs_string
end function

public function integer wf_carica_consuntivo (long fl_progressivo, ref string fs_errore);/* Questa funzione provvede a caricare il consuntivo del budget 
	
	parametro		tipo		descrizione
	----------------------------------------------
	fl_progressivo	LONG		numero del budget
	fs_errore		STRING	messaggio di errore
	
	RETURN 
	0 = OK
	-1 = ERRORE
*/

string ls_cod_attrezzatura,ld_cod_cat_attrezzature,ls_cod_reparto,ls_cod_area_aziendale,&
       ls_cod_divisione, ls_flag_global_service, ls_cod_tipo_manutenzione
long ll_rows, ll_i, ll_righe, ll_y, ll_operaio_ore, ll_operaio_minuti, ll_risorsa_ore, &
     ll_risorsa_minuti, ll_num_periodo, ll_prog_periodo, ll_anno_reg_manut, ll_num_reg_manut
dec{4} ld_costo_cons_ricambi,	ld_costo_cons_risorse_int,ld_costo_unitario_ricambio, &
       ld_costo_cons_risorse_est, ld_costo_cons_ricambi_str, ld_costo_cons_risorse_int_str, &
		 ld_costo_cons_risorse_est_str,ld_quan_ricambio, ld_costo_orario_operaio, &
		 ld_costo_orario_risorsa, ld_altri_costi_risorsa,ld_quan_utilizzo, ld_prezzo_ricambio, &
		 ld_tot_costo_risorsa,ld_tot_costo_operaio, ls_costi_aggiuntivi_risorse

datetime ldt_data_inizio, ldt_data_fine
datastore lds_totali, lds_manutenzioni

select cod_parametro
into   :ls_flag_global_service
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if sqlca.sqlcode <> 0 then
	ls_flag_global_service = "N"
else
	ls_flag_global_service = "S"
end if

lds_totali = CREATE datastore
lds_totali.dataobject = 'd_ds_budget_periodi'
lds_totali.settransobject(SQLCA)

lds_manutenzioni = CREATE datastore
lds_manutenzioni.dataobject = 'd_ds_budget_consuntivi'
lds_manutenzioni.settransobject(SQLCA)

ll_rows = lds_totali.retrieve(s_cs_xx.cod_azienda, fl_progressivo)

hpb_1.setrange(1, ll_rows)
hpb_1.setstep = 1

for ll_i = 1 to ll_rows
	hpb_1.stepit()
	// carico i dati dai totali per periodo
	ll_num_periodo = lds_totali.getitemnumber(ll_i, "num_periodo")
	ll_prog_periodo = lds_totali.getitemnumber(ll_i, "prog_periodo")
	
	
	ls_cod_attrezzatura = lds_totali.getitemstring(ll_i, "cod_attrezzatura")
	ls_cod_tipo_manutenzione = lds_totali.getitemstring( ll_i, "cod_tipo_manutenzione")
	ld_cod_cat_attrezzature = lds_totali.getitemstring(ll_i, "cod_cat_attrezzature")
	ls_cod_reparto = lds_totali.getitemstring(ll_i, "cod_reparto")
	ls_cod_area_aziendale = lds_totali.getitemstring(ll_i, "cod_area_aziendale")
	ls_cod_divisione = lds_totali.getitemstring(ll_i, "cod_divisione")
	ldt_data_inizio = lds_totali.getitemdatetime(ll_i, "data_inizio")
	ldt_data_fine = lds_totali.getitemdatetime(ll_i, "data_fine")
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = "%"
	if isnull(ld_cod_cat_attrezzature) then ld_cod_cat_attrezzature = "%"
	if isnull(ls_cod_reparto) then ls_cod_reparto = "%"
	if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = "%"
	if isnull(ls_cod_divisione) then ls_cod_divisione = "%"
	if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = "%"

	ll_righe = lds_manutenzioni.retrieve(s_cs_xx.cod_azienda, &
										 date(ldt_data_inizio), &
										 date(ldt_data_fine),&
										 ls_cod_divisione, &
										 ls_cod_area_aziendale, &
										 ls_cod_reparto, &
										 ld_cod_cat_attrezzature, &
										 ls_cod_attrezzatura, &
										 ls_cod_tipo_manutenzione)

	ld_costo_cons_ricambi = 0
	ld_costo_cons_risorse_int = 0
	ld_costo_cons_risorse_est = 0
	ld_costo_cons_ricambi_str = 0
	ld_costo_cons_risorse_int_str = 0
	ld_costo_cons_risorse_est_str = 0
	
	// leggo e valorizzo singola riga manutenzioni
	for ll_y = 1 to ll_righe
		
		st_1.text = string(ll_i) + "-" + string(ll_rows) + " " + string(ll_y) + "-" + string(ll_righe)
		
		ll_anno_reg_manut = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_anno_registrazione")
		ll_num_reg_manut  = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_num_registrazione")
		
		ld_quan_ricambio = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_quan_ricambio")
		if isnull(ld_quan_ricambio) then ld_quan_ricambio = 0
		
		ld_costo_unitario_ricambio = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_costo_unitario_ricambio")
		if isnull(ld_costo_unitario_ricambio) then ld_costo_unitario_ricambio = 0
		
		// controllo da dove andare a prendere i consuntivi orari e costi.
		if ls_flag_global_service = "S" then
			select sum(tot_costo_operaio)
			into   :ld_tot_costo_operaio
			from   manutenzioni_fasi_operai
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in totalizzazione costo operatori~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			if isnull(ld_tot_costo_operaio) then ld_tot_costo_operaio = 0

					 
			select sum(tot_costo_risorsa)
			into   :ld_tot_costo_risorsa
			from   manutenzioni_fasi_risorse
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in totalizzazione costo risorse est.~r~n" + sqlca.sqlerrtext
				return -1
			end if
					 
			if isnull(ld_tot_costo_risorsa) then ld_tot_costo_risorsa = 0
			
		else
			ld_costo_orario_operaio = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_costo_orario_operaio")
			ll_operaio_ore = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_operaio_ore")
			ll_operaio_minuti = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_operaio_minuti")
			if isnull(ld_costo_orario_operaio) then ld_costo_orario_operaio = 0
			if isnull(ll_operaio_ore) then ll_operaio_ore = 0
			if isnull(ll_operaio_minuti) then ll_operaio_minuti = 0
		
		
			ld_costo_orario_risorsa = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_risorsa_costo_orario")
			ll_risorsa_ore = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_risorsa_ore")
			ll_risorsa_minuti = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_risorsa_minuti")
			if isnull(ld_costo_orario_risorsa) then ld_costo_orario_risorsa = 0
			if isnull(ll_risorsa_ore) then ll_risorsa_ore = 0
			if isnull(ll_risorsa_minuti) then ll_risorsa_minuti = 0
			
			ld_tot_costo_operaio = ld_costo_orario_operaio * ( ( (ll_operaio_ore * 60) + ll_operaio_minuti ) / 60 )
			ld_tot_costo_risorsa = ld_costo_orario_risorsa * ( ( (ll_risorsa_ore * 60) + ll_risorsa_minuti ) / 60 )
			
		end if		
		
		ls_costi_aggiuntivi_risorse = lds_manutenzioni.getitemnumber(ll_y,"manutenzioni_risorsa_costi_aggiuntivi")
		if isnull(ls_costi_aggiuntivi_risorse) then ls_costi_aggiuntivi_risorse = 0
		
		// eseguo somma valore ricambi
		
		select sum(quan_utilizzo), sum(prezzo_ricambio)
		into   :ld_quan_utilizzo, :ld_prezzo_ricambio
		from   manutenzioni_ricambi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_reg_manut and 
				 num_registrazione = :ll_num_reg_manut;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in totalizzazione ricambi~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
		if isnull(ld_prezzo_ricambio) then ld_prezzo_ricambio = 0
		
		
		choose case lds_manutenzioni.getitemstring(ll_y,"manutenzioni_flag_ordinario")
			case "S"
				
				ld_costo_cons_ricambi += ld_quan_ricambio * ld_costo_unitario_ricambio
				ld_costo_cons_risorse_int += ld_tot_costo_operaio 
				ld_costo_cons_risorse_est += ld_tot_costo_risorsa + ls_costi_aggiuntivi_risorse
			case else
				
				ld_costo_cons_ricambi_str += ld_quan_ricambio * ld_costo_unitario_ricambio
				ld_costo_cons_risorse_int_str += ld_tot_costo_operaio
				ld_costo_cons_risorse_est_str += ld_tot_costo_risorsa + ls_costi_aggiuntivi_risorse
		end choose
		
	next
	
	
	// eseguo update si totali periodo
	update det_budget_manut_periodi
	set    costo_cons_ricambi = :ld_costo_cons_ricambi, 
	       costo_cons_ricambi_str = :ld_costo_cons_ricambi_str,
			 costo_cons_risorse_int = :ld_costo_cons_risorse_int,
			 costo_cons_risorse_int_str = :ld_costo_cons_risorse_int_str,
			 costo_cons_risorse_est = :ld_costo_cons_risorse_est,
			 costo_cons_risorse_est_str = :ld_costo_cons_risorse_est_str
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       progressivo = :fl_progressivo and
			 num_periodo = :ll_num_periodo and
			 prog_periodo = :ll_prog_periodo;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in aggiornamento totali budget periodo~r~n"+sqlca.sqlerrtext
		return -1
	end if

next

destroy lds_manutenzioni
destroy lds_totali

return 0
end function

public function string wf_crea_codice (string fs_codice);// funzione di creazione di un nuovo codice attrezzatura in base alla codifica ASCII
integer li_ascii, li_i, li_cont, li_sostituisci, li_j
string ls_str, ls_ascii

string u, si

long   ll_i, v

// aggiorno etichetta

st_1.text = fs_codice

//select max(cod_attrezzatura)
//into   :ls_str
//from   anag_attrezzature
//where  cod_azienda = :s_cs_xx.cod_azienda and
//       cod_attrezzatura like :fs_codice;
//
//if isnull(ls_str) then ls_str = fs_codice
//
//if len(ls_str) < 6 then
//	// se è più piccolo di 6 caratteri riempi di zeri a sinistra
//	ls_str = ls_str + fill("0", 6 - len(ls_str))
//else
//	
//	li_cont = 0
//	li_sostituisci = 4
//	
//	do 
//		// altrimenti cerco il codice ascii successivo rispetto agli ultimi 3 caratteri
//		ls_ascii = right(ls_str,4)
//		
//		for li_i = 4 to 1 step -1
//			if mid(ls_ascii, li_i,1) = "Z" then
//				continue
//			else
//				do while true
//					li_ascii = asc(mid(ls_ascii, li_i,1))
//					li_ascii ++
//					
//					if li_ascii > 57 and li_ascii < 65 then li_ascii = 65  // passo dal 9 alla A
//					if li_ascii > 90 then 
//						li_cont = 1 
//						
//						// *** azzero tutti i caratteri da quel carattere in giù
//						for li_j = 4 to 1 step -1
//							ls_ascii = replace( ls_ascii, li_j, 1,'0')
//						next
//						
//						li_sostituisci --
//						//continue // sono alla Z, salto
//						// Sostituisco il carattere con lo 0
//						
//						if (li_i - 1) > 0 then
//							li_i --
//							//ls_ascii = replace(ls_ascii,li_i,1,'1')	
//							ls_ascii = replace(ls_ascii,li_sostituisci,1,'1')
//							li_i = 5 // devo ripartire da capo
//						end if						
//
//						exit
//					end if
//					
//					ls_ascii = replace(ls_ascii,li_i,1,char(li_ascii))
//					
//					ls_str = left(ls_str,2) + ls_ascii
//					
//					select count(*)
//					into   :li_cont
//					from   anag_attrezzature
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 cod_attrezzatura = :ls_str;
//		
//					if li_cont = 0 then exit
//					
//				loop
//				
//				if li_cont = 0 then exit
//				
//			end if
//			
//		next
//		
//	loop until li_cont < 1	
//	
//end if
//
//return ls_str

	
u = ""

ll_i = len(fs_codice)

do while ll_i > 0
		
		si = mid( fs_codice, ll_i, 1)
		
		v = asc(si) + 1
			
		if ll_i = 1 then
			
			if v > 57 and v < 65 then v = 65
			
			if v < 91 then
				
				u = u + char(v)
				
				u = reverse(u)		
				
				exit
			else
				
				return "-1"
				
			end if
			
		end if
		
		if v > 57 and v < 65 then v = 65
				
		if v < 91 then 
			
			u = left( fs_codice, ll_i -1) + char(v) 
			
			exit
			
		else
			if ll_i <> 1 then
				
				u = u + "A"
				
			end if
			
		end if
		
		ll_i = ll_i - 1
		
	loop
		
	if len(u) <> len(fs_codice) then
		
		u = reverse(u)
		
		u = left( fs_codice, len(fs_codice) - len(u)) + u
		
end if
	
return u	
	


		 

end function

public function integer wf_nuova_attrezzatura (string fs_cod_attrezzatura, string fs_cod_reparto, string fs_cod_area, ref string fs_errore, ref string fs_cod_attrezzatura_new);string ls_cod_attrezzatura_new

setnull(ls_cod_attrezzatura_new)

select max(cod_attrezzatura)
into   :ls_cod_attrezzatura_new
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ls_cod_attrezzatura_new) then ls_cod_attrezzatura_new = "000000"

ls_cod_attrezzatura_new = wf_crea_codice(ls_cod_attrezzatura_new)
	
// creo nuova attrezzatura

INSERT INTO anag_attrezzature  
		( cod_azienda,   
		cod_attrezzatura,   
		cod_cat_attrezzature,   
		descrizione,   
		fabbricante,   
		modello,   
		num_matricola,   
		data_acquisto,   
		periodicita_revisione,   
		unita_tempo,   
		utilizzo_scadenza,   
		cod_reparto,   
		certificata_fabbricante,   
		certificata_ce,   
		cod_inventario,   
		costo_orario_medio,   
		cod_prodotto,   
		cod_area_aziendale,   
		cod_utente,   
		reperibilita,   
		grado_precisione,   
		grado_precisione_um,   
		num_certificato_ente,   
		data_certificato_ente,   
		denominazione_ente,   
		qualita_attrezzatura,   
		cod_centro_costo,   
		flag_primario,   
		flag_blocco,   
		data_blocco,   
		cod_versione,   
		note,   
		data_prima_attivazione,   
		data_installazione,   
		flag_reg_man,   
		flag_manuale,   
		flag_duplicazione,   
		flag_conta_ore,   
		costo_acquisto,   
		freq_guasti,   
		accessibilita,   
		disp_ricamnbi,   
		stato_attuale,   
		indice_capacita,   
		indice_affidabilita,   
		indice_manutenibilita,   
		indice_costo,   
		valore_residuo,   
		vita_utile )  
SELECT cod_azienda,   
		:ls_cod_attrezzatura_new,   
		cod_cat_attrezzature,   
		descrizione,   
		fabbricante,   
		modello,   
		null,   
		data_acquisto,   
		periodicita_revisione,   
		unita_tempo,   
		utilizzo_scadenza,   
		:fs_cod_reparto,   
		certificata_fabbricante,   
		certificata_ce,   
		cod_inventario,   
		costo_orario_medio,   
		cod_prodotto,   
		:fs_cod_area,   
		cod_utente,   
		reperibilita,   
		grado_precisione,   
		grado_precisione_um,   
		num_certificato_ente,   
		data_certificato_ente,   
		denominazione_ente,   
		qualita_attrezzatura,   
		cod_centro_costo,   
		flag_primario,   
		flag_blocco,   
		data_blocco,   
		cod_versione,   
		note,   
		data_prima_attivazione,   
		data_installazione,   
		flag_reg_man,   
		flag_manuale,   
		flag_duplicazione,   
		flag_conta_ore,   
		costo_acquisto,   
		freq_guasti,   
		accessibilita,   
		disp_ricamnbi,   
		stato_attuale,   
		indice_capacita,   
		indice_affidabilita,   
		indice_manutenibilita,   
		indice_costo,   
		valore_residuo,   
		vita_utile   
from  anag_attrezzature
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :fs_cod_attrezzatura;
	
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in creazione attrezzatura~r~n" + sqlca.sqlerrtext
	return -1
end if

fs_cod_attrezzatura_new = ls_cod_attrezzatura_new

return 0
end function

public function integer wf_copia_tipologie (long fl_progressivo, string fs_cod_attrezzatura_modello, string fs_cod_attrezzatura, string fs_periodicita, long fl_frequenza, ref string fs_errore, ref string fs_tipi_manutenzione[]);// *** michela
//               devo prendere tutte le tipologie manutenzione dell'attrezzature di quei periodi e crearle per la nuova attrezzatura

string ls_cod_tipo_manutenzione
long   ll_cont_tipi

declare cu_tipi cursor for
select  distinct(cod_tipo_manutenzione)
from    det_budget_manut_periodi  
where   cod_azienda = :s_cs_xx.cod_azienda and
        progressivo = :fl_progressivo and
		  cod_attrezzatura = :fs_cod_attrezzatura_modello;
		  
open cu_tipi;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore cu_tipi: " + sqlca.sqlerrtext
	return -1
end if

ll_cont_tipi = 0

do while 1 = 1
	
	fetch cu_tipi into :ls_cod_tipo_manutenzione;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante il caricamento del cursore cu_tipi: " + sqlca.sqlerrtext
		close cu_tipi;
		return -1		
	end if
	if isnull(ls_cod_tipo_manutenzione) then continue
	
	INSERT INTO tab_tipi_manutenzione  
		( cod_azienda,   
		  cod_attrezzatura,   
		  cod_tipo_manutenzione,   
		  des_tipo_manutenzione,   
		  tempo_previsto,   
		  flag_manutenzione,   
		  flag_ricambio_codificato,   
		  cod_ricambio,   
		  cod_ricambio_alternativo,   
		  des_ricambio_non_codificato,   
		  num_reg_lista,   
		  num_versione,   
		  num_edizione,   
		  modalita_esecuzione,   
		  cod_tipo_ord_acq,   
		  cod_tipo_off_acq,   
		  cod_tipo_det_acq,   
		  cod_tipo_movimento,   
		  periodicita,   
		  frequenza,   
		  quan_ricambio,   
		  costo_unitario_ricambio,   
		  cod_primario,   
		  cod_misura,   
		  val_min_taratura,   
		  val_max_taratura,   
		  valore_atteso,   
		  cod_versione,   
		  flag_blocco,   
		  data_ultima_modifica,   
		  des_tipo_manutenzione_storico,   
		  data_blocco,   
		  cod_tipo_lista_dist,   
		  cod_lista_dist,   
		  cod_operaio,   
		  cod_cat_risorse_esterne,   
		  cod_risorsa_esterna,   
		  flag_azzera_utilizzo )  
	  select 
			cod_azienda, 
			:fs_cod_attrezzatura,   
			cod_tipo_manutenzione,
			des_tipo_manutenzione,   
			tempo_previsto,   
			flag_manutenzione,   
			flag_ricambio_codificato,   
			cod_ricambio,   
			cod_ricambio_alternativo,   
			des_ricambio_non_codificato,   
			num_reg_lista,   
			num_versione,   
			num_edizione,   
			modalita_esecuzione,   
			cod_tipo_ord_acq,   
			cod_tipo_off_acq,   
			cod_tipo_det_acq,   
			cod_tipo_movimento,   
			periodicita,   
			frequenza,   
			quan_ricambio,   
			costo_unitario_ricambio,   
			cod_primario,   
			cod_misura,   
			val_min_taratura,   
			val_max_taratura,   
			valore_atteso,   
			cod_versione,   
			flag_blocco,   
			data_ultima_modifica,   
			des_tipo_manutenzione_storico,   
			data_blocco,   
			cod_tipo_lista_dist,   
			cod_lista_dist,   
			cod_operaio,   
			cod_cat_risorse_esterne,   
			cod_risorsa_esterna,   
			flag_azzera_utilizzo  
	 FROM tab_tipi_manutenzione  
	WHERE cod_azienda = :s_cs_xx.cod_azienda and  
			cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			cod_attrezzatura = :fs_cod_attrezzatura_modello;
				
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in creazione tipo manutenzione~r~n" + sqlca.sqlerrtext
		close cu_tipi;
		return -1
	end if
	
	ll_cont_tipi ++
	fs_tipi_manutenzione[ll_cont_tipi] = ls_cod_tipo_manutenzione

	
loop

close cu_tipi;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore cu_tipi: " + sqlca.sqlerrtext
	return -1
end if
	       
return 0				
		
end function

public function integer wf_programmazione_semplice (long fl_progressivo, datetime fdt_data, string fs_tipo_data, string fs_flag_scadenze, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione[], ref string fs_errore, datastore fds_periodi);long     ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, &
	      ll_anno_registrazione, ll_num_registrazione, ll_ore, ll_minuti, ll_prog_tipo_manutenzione, ll_num_protocollo
			
string   ls_flag_gest_manutenzione, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
	      ls_flag_ricambio_codificato, ls_cod_ricambio, ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, &
		   ls_cod_primario, ls_cod_misura, ls_flag_manutenzione, ls_messaggio, ls_note_idl, ls_descrizione, ls_des_blob, &
			ls_path_documento, ls_data, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
			ls_cod_risorsa_esterna, ls_flag_scadenze
			
dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

datetime ldt_tempo_previsto, ldt_data, ldt_data_1, ldt_data_creazione_programma

blob lbb_blob

uo_manutenzioni luo_manutenzioni

long     ll_risp, ll_giorni, ll_mesi, ll_anni

string   ls_tipo_data

dec{0}	ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

	
ldt_data = fdt_data

ls_tipo_data = fs_tipo_data

ls_flag_scadenze = fs_flag_scadenze


DECLARE cu_documenti CURSOR FOR  
SELECT  prog_tipo_manutenzione,   
		  descrizione,   
		  num_protocollo,   
		  des_blob,   
		  path_documento  
FROM    det_tipi_manutenzioni  
WHERE   cod_azienda = :s_cs_xx.cod_azienda and
		  cod_attrezzatura = :ls_cod_attrezzatura and
		  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;

luo_manutenzioni = create uo_manutenzioni
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_parametro = 'ESC';
	 
if sqlca.sqlcode <> 0 then	
	fs_errore = "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext
	return -1
end if

select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 100 then
	fs_errore = "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext
	return -1
end if	
	
for ll_i = 1 to Upperbound(fs_cod_tipo_manutenzione)
	
	ls_cod_attrezzatura = fs_cod_attrezzatura
		
	ls_cod_tipo_manutenzione = fs_cod_tipo_manutenzione[ll_i]
		
	select count(anno_registrazione) 
	into   :ll_count
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in fase di lettura dati dalla tabella programmmi_manutenzione " + sqlca.sqlerrtext
		return -1
	end if	
		
	if not isnull(ll_count) and ll_count > 0 then
		
		select count(*)
		into   :ll_count
		from	 manutenzioni,
				 programmi_manutenzione
		where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
				 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
				 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
				 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
				 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
				 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
				 manutenzioni.flag_eseguito = 'N';
					 
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext
			return -1
		end if
			
		if not isnull(ll_count) and ll_count > 0 then continue
			
	end if
		
	select flag_ricambio_codificato,
			 cod_ricambio,
			 cod_ricambio_alternativo,
			 des_ricambio_non_codificato,
			 num_reg_lista,
			 num_versione,
			 num_edizione,
			 periodicita,
			 frequenza,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_primario,
			 cod_misura,
			 val_min_taratura,
			 val_max_taratura,
			 valore_atteso,
			 flag_manutenzione,
			 modalita_esecuzione,
			 tempo_previsto,
			 cod_operaio,
			 cod_cat_risorse_esterne,
			 cod_risorsa_esterna,
			 data_inizio_val_1,
			 data_fine_val_1,
			 data_inizio_val_2,
			 data_fine_val_2,
			 cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_cod_ricambio_alternativo,
			 :ls_des_ricambio_non_codificato,
			 :ll_num_reg_lista,
			 :ll_num_versione,
			 :ll_num_edizione,
			 :ls_periodicita,
			 :ll_frequenza,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_primario,
			 :ls_cod_misura,
			 :ld_val_min_taratura,
			 :ld_val_max_taratura,
			 :ld_valore_atteso,
			 :ls_flag_manutenzione,
			 :ls_note_idl,
			 :ldt_tempo_previsto,
			 :ls_cod_operaio,
			 :ls_cod_cat_risorse_esterne,
			 :ls_cod_risorsa_esterna,
			 :ld_data_inizio_val_1,
			 :ld_data_fine_val_1,
			 :ld_data_inizio_val_2,
			 :ld_data_fine_val_2,
			 :ls_cod_tipo_lista_dist,
			 :ls_cod_lista_dist
   from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext
		return -1
	end if				
		
	ll_ore = hour(time(ldt_tempo_previsto))		
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
			 anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext
		return -1
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
			 
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)	
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
	
	// ***
		
	insert into programmi_manutenzione (cod_azienda,
													anno_registrazione,
													num_registrazione,
													data_creazione,
													cod_attrezzatura,
													cod_tipo_manutenzione,
													flag_tipo_intervento,
													flag_ricambio_codificato,
													cod_ricambio,
													cod_ricambio_alternativo,
													ricambio_non_codificato,
													num_reg_lista,
													num_versione,
													num_edizione,
													periodicita,
													frequenza,
													quan_ricambio,
													costo_unitario_ricambio,
													cod_primario,
													cod_misura,
													valore_min_taratura,
													valore_max_taratura,
													valore_atteso,
													flag_priorita,
													note_idl,
													operaio_ore_previste,
													operaio_minuti_previsti,
													cod_tipo_lista_dist,
													cod_lista_dist,
													cod_operaio,
													cod_cat_risorse_esterne,
													cod_risorsa_esterna,
													data_inizio_val_1,
													data_fine_val_1,
													data_inizio_val_2,
													data_fine_val_2)														
	values	                         (:s_cs_xx.cod_azienda,
												  :ll_anno_registrazione,
												  :ll_num_registrazione,
												  :ldt_data_creazione_programma,
												  :ls_cod_attrezzatura,
												  :ls_cod_tipo_manutenzione,
												  :ls_flag_manutenzione,
												  :ls_flag_ricambio_codificato,
												  :ls_cod_ricambio,
												  :ls_cod_ricambio_alternativo,
											 	  :ls_des_ricambio_non_codificato,
												  :ll_num_reg_lista,
												  :ll_num_versione,
												  :ll_num_edizione,
												  :ls_periodicita,
												  :ll_frequenza,
												  :ld_quan_ricambio,
												  :ld_costo_unitario_ricambio,
												  :ls_cod_primario,
												  :ls_cod_misura,
												  :ld_val_min_taratura,
												  :ld_val_max_taratura,
 												  :ld_valore_atteso,
												  'N',
												  :ls_note_idl,
												  :ll_ore,
												  :ll_minuti,
												  :ls_cod_tipo_lista_dist,
												  :ls_cod_lista_dist,
												  :ls_cod_operaio,
												  :ls_cod_cat_risorse_esterne,
												  :ls_cod_risorsa_esterna,
												  :ld_data_inizio_val_1,
												  :ld_data_fine_val_1,
												  :ld_data_inizio_val_2,
												  :ld_data_fine_val_2);
														 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext
		return -1
	end if	
	
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		    ( cod_azienda,   
		      anno_registrazione,   
		  		num_registrazione,   
		 		prog_riga_ricambio,   
		      cod_prodotto,   
		      des_prodotto,   
		      quan_utilizzo,   
		      prezzo_ricambio )  
	select   cod_azienda,   
			   :ll_anno_registrazione,   
			   :ll_num_registrazione,   
			   prog_riga_ricambio,   
			   cod_prodotto,   
			   des_prodotto,   
			   quan_utilizzo,   
			   prezzo_ricambio  
	 from    tipi_manutenzioni_ricambi
	 where   cod_azienda = :s_cs_xx.cod_azienda and
	         cod_attrezzatura = :ls_cod_attrezzatura and
			   cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore nella OPEN del cursore cu_documenti~r~n" + sqlca.sqlerrtext
		return -1
	end if

	do while 1 = 1
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore nella OPEN del cursore cu_documenti~r~n" + sqlca.sqlerrtext
			close cu_documenti;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
			
		INSERT INTO det_prog_manutenzioni  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  progressivo,   
				  descrizione,   
				  num_protocollo,   
				  flag_blocco,   
				  data_blocco,   
				  des_blob,   
				  path_documento )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ll_prog_tipo_manutenzione,   
				  :ls_descrizione,   
				  :ll_num_protocollo,   
				  'N',   
				  null,   
				  :ls_des_blob,   
				  :ls_path_documento )  ;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext
			close cu_documenti;
			return -1
		end if

		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
			        cod_attrezzatura = :ls_cod_attrezzatura and
				     cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca (select) dell'allegato~r~n"+sqlca.sqlerrtext
			close cu_documenti;
			return -1
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext
			close cu_documenti;
			return -1
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext
		return -1
	end if
					
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "O"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;

				if sqlca.sqlcode < 0 then
					fs_errore = "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext
					return -1
				end if
				
			case "M"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext
					return -1
				end if
				
			case else
				
				update programmi_manutenzione
				set    flag_scadenze = :ls_flag_scadenze
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext
					return -1
				end if
				
				if ls_flag_scadenze = "S" then

					if luo_manutenzioni.uof_crea_manutenzione_programmata(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
						fs_errore = "Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext
						return -1
					end if
					
					// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
					int mese1_i, mese1_f, mese2_i, mese2_f, giorno1_i, giorno1_f, giorno2_i, giorno2_f
										
					giorno1_i = ( ld_data_inizio_val_1 / 100)
					mese1_i = mod( ld_data_inizio_val_1, 100)
										
					giorno1_f = ( ld_data_fine_val_1 / 100)
					mese1_f = mod( ld_data_fine_val_1, 100)
					
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
					
						if month(date(ldt_data)) < mese1_i or month(date(ldt_data)) > mese1_f then
							ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data)) < giorno1_i or day(date(ldt_data)) > giorno1_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
							end if
						end if
					
					else										
						giorno2_i = ( ld_data_inizio_val_2 / 100)
						mese2_i = mod( ld_data_inizio_val_2, 100)
											
						giorno2_f = ( ld_data_fine_val_2 / 100)
						mese2_f = mod( ld_data_fine_val_2, 100)							
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data)) < mese2_i or month(date(ldt_data)) > mese2_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data)) < giorno2_i or day(date(ldt_data)) > giorno2_f then
									ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
								end if
							end if												
						end if
					end if
						
					// controllo di che tipo è la data: se è la data della prima scadenza ... allora devo cambiare la data della 
					// registrazione, altrimenti, a partire da quella data, trovo la data della prossima scadenza
		
					if ls_tipo_data <> "S" then					
						ldt_data_1 = luo_manutenzioni.uof_prossima_scadenza( ldt_data, ls_periodicita, ll_frequenza)					
					else
						ldt_data_1 = ldt_data
					end if						
					
					// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
					
						if month(date(ldt_data_1)) < mese1_i or month(date(ldt_data_1)) > mese1_f then
							ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data_1)) < giorno1_i or day(date(ldt_data_1)) > giorno1_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
							end if
						end if
					
					else										
						
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data_1)) < mese2_i or month(date(ldt_data_1)) > mese2_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data_1)) < giorno2_i or day(date(ldt_data_1)) > giorno2_f then
									ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
								end if
							end if												
						end if
					end if		
					
					
////					// *** oltre alla data aggiorno anche i tempi degli operatori e delle risorse in base ai periodi
////					
////					long    ll_rows, ll_ore_intervento, ll_minuti_intervento
////					decimal ld_tempo_previsto, ld_costo_prev_risorse_int, ld_costo_prev_risorse_est
////					
////					// -----------------------  PROCEDO CON CREAZIONE DELLE REGISTRAZIONI MANUTENZIONI -----------------
////					ll_rows = fds_periodi.retrieve(s_cs_xx.cod_azienda, fl_progressivo, fs_cod_attrezzatura, ls_cod_tipo_manutenzione)
////					
////					if ll_rows > 0 then
////						
////						ld_tempo_previsto = fds_periodi.getitemnumber( 1, "ore_prev_risorse_int")
////						ld_costo_prev_risorse_int = fds_periodi.getitemnumber( 1,"costo_prev_risorse_int")
////						ld_costo_prev_risorse_est = fds_periodi.getitemnumber( 1,"costo_prev_risorse_est")
////				
////						ll_ore_intervento = truncate(ld_tempo_previsto,0)
////						ll_minuti_intervento = round((ld_tempo_previsto - truncate(ld_tempo_previsto,0)) * 60, 0)
////						
////					else
////						
////						ll_ore_intervento = 0
////						ll_minuti_intervento = 0
////						ld_tempo_previsto = 0
////						ld_costo_prev_risorse_int = 0
////						ld_costo_prev_risorse_est = 0
////	
////					end if
					
					update manutenzioni
					set    data_registrazione = :ldt_data_1
					where  cod_azienda = :s_cs_xx.cod_azienda and    
							 anno_reg_programma = :ll_anno_registrazione	and    
							 num_reg_programma = :ll_num_registrazione;
					
					if sqlca.sqlcode <> 0 then
						fs_errore = "Errore in aggiornamento data registrazione! " + sqlca.sqlerrtext
						return -1
					end if
				end if
				
		end choose		
	end if	
next 
	 
destroy luo_manutenzioni


return 0
end function

public function integer wf_copia_periodi (long fl_progressivo, string fs_cod_divisione, string fs_cod_attrezzatura, string fs_cod_attrezzatura_new, ref string fs_errore);long      ll_prog_periodo_old, ll_i, ll_prog_periodo, ll_num_periodo, ll_prog_periodo_new
string    ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzature

datastore lds_record


select cod_area_aziendale,
		 cod_reparto,
		 cod_cat_attrezzature
into   :ls_cod_area_aziendale,
		 :ls_cod_reparto,
		 :ls_cod_cat_attrezzature
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :fs_cod_attrezzatura_new;

lds_record = create datastore
lds_record.dataobject = "d_ds_det_budget_periodi"
lds_record.settransobject( sqlca)

lds_record.retrieve( s_cs_xx.cod_azienda, fl_progressivo, fs_cod_attrezzatura)

ll_prog_periodo_old = 0

for ll_i = 1 to lds_record.rowcount()
	
	ll_num_periodo = lds_record.getitemnumber( ll_i, "num_periodo")
	ll_prog_periodo = lds_record.getitemnumber( ll_i, "prog_periodo")
	
	select max(prog_periodo)
	into   :ll_prog_periodo_new
	from   det_budget_manut_periodi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 progressivo = :fl_progressivo and
			 num_periodo = :ll_num_periodo;
			
	if isnull(ll_prog_periodo_new) then ll_prog_periodo_new = 0
	ll_prog_periodo_new ++	

	
   INSERT INTO det_budget_manut_periodi  
		( cod_azienda,   
		  progressivo,   
		  num_periodo,   
		  prog_periodo,   
		  data_inizio,   
		  data_fine,   
		  cod_divisione,   
		  cod_area_aziendale,   
		  cod_reparto,   
		  cod_cat_attrezzature,   
		  cod_attrezzatura,   
		  costo_prev_ricambi,   
		  costo_prev_ricambi_str,   
		  costo_prev_risorse_int,   
		  costo_prev_risorse_int_str,   
		  costo_prev_risorse_est,   
		  costo_prev_risorse_est_str,   
		  costo_cons_ricambi,   
		  costo_cons_ricambi_str,   
		  costo_cons_risorse_int,   
		  costo_cons_risorse_int_str,   
		  costo_cons_risorse_est,   
		  costo_cons_risorse_est_str,   
		  costo_trend_ricambi,   
		  costo_trend_risorse_int,   
		  costo_trend_risorse_est,   
		  flag_consolidato,   
		  flag_manuale,   
		  flag_record_dati,   
		  ore_prev_risorse_int,   
		  costo_ora_prev_risorse_int,   
		  cod_tipo_manutenzione,   
		  num_copie_attrezzatura )
	select :s_cs_xx.cod_azienda,
	       :fl_progressivo,
			 :ll_num_periodo,
			 :ll_prog_periodo_new,
			 data_inizio,
			 data_fine,
			 :fs_cod_divisione,
			 :ls_cod_area_aziendale,
			 :ls_cod_reparto,
			 :ls_cod_cat_attrezzature,
			 :fs_cod_attrezzatura_new,
			 costo_prev_ricambi,
			 costo_prev_ricambi_str,   
		  	 costo_prev_risorse_int,   
		    costo_prev_risorse_int_str,   
		    costo_prev_risorse_est,   
		    costo_prev_risorse_est_str,   
		    costo_cons_ricambi,   
		    costo_cons_ricambi_str,   
		    costo_cons_risorse_int,   
		    costo_cons_risorse_int_str,   
		    costo_cons_risorse_est,   
		    costo_cons_risorse_est_str,   
		    costo_trend_ricambi,   
		    costo_trend_risorse_int,   
		    costo_trend_risorse_est,   
		    'S',   
		    flag_manuale,   
		    flag_record_dati,   
		    ore_prev_risorse_int,   
		    costo_ora_prev_risorse_int,   
		    cod_tipo_manutenzione,   
		    1
	from   det_budget_manut_periodi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       progressivo = :fl_progressivo and
			 num_periodo = :ll_num_periodo and
			 prog_periodo = :ll_prog_periodo;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante l'inserimento del periodo per l'attrezzatura " + fs_cod_attrezzatura_new + ":" + sqlca.sqlerrtext
		return -1
	end if
	
next

destroy lds_record;


return 0
end function

public function integer wf_programmazione_periodo (datetime fdt_da_data, datetime fdt_a_data, string fs_flag_scadenze_esistenti, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione[], ref string fs_errore);datetime ldt_da_data, ldt_a_data, ldt_tempo_previsto, ldt_data_1, ldt_data, ldt_scadenze[], ldt_data_registrazione, &
         ldt_data_prossimo_intervento, ldt_null, ldt_max_data_registrazione, ldt_data_creazione_programma, &
			ldt_scadenze_vuoto[]

string   ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_gest_manutenzione, ls_flag_ricambio_codificato, ls_cod_ricambio, &
         ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, ls_cod_primario, ls_cod_misura, &
			ls_flag_manutenzione, ls_note_idl, ls_des_blob, ls_path_documento, ls_descrizione, ls_messaggio, ls_flag_scadenze_esistenti, &
			ls_cod_tipo_lista_dist, ls_cod_lista_dist,ls_cod_operaio,ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna
			
long     ll_anno_registrazione, ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, ll_ore, ll_minuti, &
         ll_num_registrazione, ll_prog_tipo_manutenzione, ll_num_protocollo, ll_num_programmazioni, ll_ii

dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean  lb_inserimento

dec{0}   ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

uo_manutenzioni luo_manutenzioni

blob     lbb_blob

setnull(ldt_null)

ls_flag_scadenze_esistenti = fs_flag_scadenze_esistenti
ldt_da_data = fdt_da_data
ldt_a_data = fdt_a_data

setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT prog_tipo_manutenzione,   
		 descrizione,   
		 num_protocollo,   
		 des_blob,   
		 path_documento  
FROM   det_tipi_manutenzioni  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni

lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then	
	fs_errore = "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext
	return -1
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext
	return -1	
end if
	
if sqlca.sqlcode = 100 then
	fs_errore = "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext
	return -1
end if	

ll_num_programmazioni = upperbound( fs_cod_tipo_manutenzione)
	
for ll_i = 1 to ll_num_programmazioni
	
	ls_cod_attrezzatura = fs_cod_attrezzatura	
	ls_cod_tipo_manutenzione = fs_cod_tipo_manutenzione[ll_i]
	
	select flag_ricambio_codificato,
			 cod_ricambio,
			 cod_ricambio_alternativo,
			 des_ricambio_non_codificato,
			 num_reg_lista,
			 num_versione,
			 num_edizione,
			 periodicita,
			 frequenza,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_primario,
			 cod_misura,
			 val_min_taratura,
			 val_max_taratura,
			 valore_atteso,
			 flag_manutenzione,
			 modalita_esecuzione,
			 tempo_previsto,
			 cod_operaio,
			 cod_cat_risorse_esterne,
			 cod_risorsa_esterna,
			 data_inizio_val_1,
			 data_fine_val_1,
			 data_inizio_val_2,
			 data_fine_val_2,
			 cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_cod_ricambio_alternativo,
			 :ls_des_ricambio_non_codificato,
			 :ll_num_reg_lista,
			 :ll_num_versione,
			 :ll_num_edizione,
			 :ls_periodicita,
			 :ll_frequenza,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_primario,
			 :ls_cod_misura,
			 :ld_val_min_taratura,
			 :ld_val_max_taratura,
			 :ld_valore_atteso,
			 :ls_flag_manutenzione,
			 :ls_note_idl,
			 :ldt_tempo_previsto,
			 :ls_cod_operaio,
			 :ls_cod_cat_risorse_esterne,
			 :ls_cod_risorsa_esterna,
			 :ld_data_inizio_val_1,
			 :ld_data_fine_val_1,
			 :ld_data_inizio_val_2,
			 :ld_data_fine_val_2,
			 :ls_cod_tipo_lista_dist,
			 :ls_cod_lista_dist
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			
	if sqlca.sqlcode <> 0 then
		
		fs_errore = "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext
		return -1
		
	end if				
		
	ll_ore = hour(time(ldt_tempo_previsto))		
	
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then

		fs_errore = "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext
		return -1
		
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)	
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
	
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
		
	insert into programmi_manutenzione (cod_azienda,
													anno_registrazione,
													num_registrazione,
													data_creazione,
													cod_attrezzatura,
													cod_tipo_manutenzione,
													flag_tipo_intervento,
													flag_ricambio_codificato,
													cod_ricambio,
													cod_ricambio_alternativo,
													ricambio_non_codificato,
													num_reg_lista,
													num_versione,
													num_edizione,
													periodicita,
													frequenza,
													quan_ricambio,
													costo_unitario_ricambio,
													cod_primario,
													cod_misura,
													valore_min_taratura,
													valore_max_taratura,
													valore_atteso,
													flag_priorita,
													note_idl,
													operaio_ore_previste,
													operaio_minuti_previsti,
													cod_tipo_lista_dist,
													cod_lista_dist,
													cod_operaio,
													cod_cat_risorse_esterne,
													cod_risorsa_esterna,
													data_inizio_val_1,
													data_fine_val_1,
													data_inizio_val_2,
													data_fine_val_2)
										  values	(:s_cs_xx.cod_azienda,
													 :ll_anno_registrazione,
													 :ll_num_registrazione,
													 :ldt_data_creazione_programma,
													 :ls_cod_attrezzatura,
													 :ls_cod_tipo_manutenzione,
													 :ls_flag_manutenzione,
													 :ls_flag_ricambio_codificato,
													 :ls_cod_ricambio,
													 :ls_cod_ricambio_alternativo,
													 :ls_des_ricambio_non_codificato,
													 :ll_num_reg_lista,
													 :ll_num_versione,
													 :ll_num_edizione,
													 :ls_periodicita,
													 :ll_frequenza,
													 :ld_quan_ricambio,
													 :ld_costo_unitario_ricambio,
													 :ls_cod_primario,
													 :ls_cod_misura,
													 :ld_val_min_taratura,
													 :ld_val_max_taratura,
													 :ld_valore_atteso,
													 'N',
													 :ls_note_idl,
													 :ll_ore,
													 :ll_minuti,
													 :ls_cod_tipo_lista_dist,
													 :ls_cod_lista_dist,
													 :ls_cod_operaio,
													 :ls_cod_cat_risorse_esterne,
													 :ls_cod_risorsa_esterna,
													 :ld_data_inizio_val_1,
													 :ld_data_fine_val_1,
													 :ld_data_inizio_val_2,
													 :ld_data_fine_val_2);
													 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext
		return -1
	end if	
	
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio )  
	select cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_ricambio,   
			cod_prodotto,   
			des_prodotto,   
			quan_utilizzo,   
			prezzo_ricambio  
	 from tipi_manutenzioni_ricambi
	 where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		
		fs_errore = "Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext
		return -1
		
	end if
	
	do while 1=1
		
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento;
										
		if sqlca.sqlcode = -1 then
			
			fs_errore = "Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext
			close cu_documenti;
			return -1
			
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		INSERT INTO det_prog_manutenzioni  
					 ( cod_azienda,   
					   anno_registrazione,   
					   num_registrazione,   
					   progressivo,   
					   descrizione,   
					   num_protocollo,   
					   flag_blocco,   
					   data_blocco,   
					   des_blob,   
					   path_documento )  
		VALUES   ( :s_cs_xx.cod_azienda,   
					  :ll_anno_registrazione,   
					  :ll_num_registrazione,   
					  :ll_prog_tipo_manutenzione,   
					  :ls_descrizione,   
					  :ll_num_protocollo,   
					  'N',   
					  null,   
					  :ls_des_blob,   
					  :ls_path_documento )  ;
					 
		if sqlca.sqlcode <> 0 then
			
			fs_errore = "Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext
			close cu_documenti;
			return -1
			
		end if
	
		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_attrezzatura = :ls_cod_attrezzatura and
					  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca (select) dell'allegato~r~n" + sqlca.sqlerrtext
			close cu_documenti;
			return -1
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then			
			fs_errore = "Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext
			close cu_documenti;
			return -1			
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
		
	lb_inserimento = true
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then		
		fs_errore = "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext
		return -1		
	end if
				
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "O"  // ** ore
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then					
					fs_errore = "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext
					return -1					
				end if
				
				
			case "M" // ** minuti
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then					
					fs_errore = "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext
					return -1					
				end if
				
			case else // ** G = giorni, S = settimane, E = mesi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
						 anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then					
					fs_errore = "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext
					return -1					
				end if
				
				
				// *** programmazione periodo funzione 1: trovo il vettore con le date delle scadenze
				
				// *** controllo se esistono programmi di manutenzione
				select count(anno_registrazione) 
				into   :ll_count
				from   programmi_manutenzione
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_attrezzatura = :ls_cod_attrezzatura and 
						 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
						
				if sqlca.sqlcode < 0 then			
					fs_errore = "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext
					return -1					
				end if
					
				// controllo se esistono scadenze da confermare
				if not isnull(ll_count) and ll_count > 0 then
						
					select count(*)
					into   :ll_count
					from	 manutenzioni,
							 programmi_manutenzione
					where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
							 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
							 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
							 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
							 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
							 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
							 manutenzioni.flag_eseguito = 'N';
							 
					if sqlca.sqlcode < 0 then						
						fs_errore = "Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext
						return -1						
					end if
					
					// se ci sono scadenze da confermare allora guardo il flag
					if not isnull(ll_count) and ll_count > 0 then
						
						// *** flag = 'S' allora seleziono la massima scadenza e faccio partire il periodo di programmazione
						//     dalla prossima scadenza a partire da quella data di registrazione
						if ls_flag_scadenze_esistenti = 'S' then
							
							select MAX(data_registrazione)
							into   :ldt_max_data_registrazione
							from	 manutenzioni,
									 programmi_manutenzione
							where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
									 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
									 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
									 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
									 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
									 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
									 manutenzioni.flag_eseguito = 'N';
									 
							if sqlca.sqlcode < 0 then								
								fs_errore = "Errore in controllo data manutenzioni programmate da eseguire: " + sqlca.sqlerrtext
								return -1								
							end if
						
							if not isnull(ldt_max_data_registrazione) then ldt_max_data_registrazione = luo_manutenzioni.uof_prossima_scadenza( ldt_max_data_registrazione, ls_periodicita, ll_frequenza )								
							
							if not isnull(ldt_max_data_registrazione) then ldt_da_data = ldt_max_data_registrazione
							
						else
						// *** cancello le scadenze ancora da confermare e continuo con il periodo scritto 
						//     dall'operatore
							delete from	 manutenzioni
							where  manutenzioni.cod_azienda = :s_cs_xx.cod_azienda and
									 manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura and
									 manutenzioni.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
									 manutenzioni.flag_eseguito = 'N' and
									 manutenzioni.anno_reg_programma IS NOT NULL and
									 manutenzioni.num_reg_programma IS NOT NULL;		
									 
							if sqlca.sqlcode < 0 then								
								fs_errore = "Errore in cancellazione manutenzioni programmate da eseguire: " + sqlca.sqlerrtext
								return -1								
							end if									 
														
						end if
					end if
						
				end if
	
				// ***
				
				ldt_scadenze[] = ldt_scadenze_vuoto[]
				
				luo_manutenzioni.uof_scadenze_periodo( ldt_da_data , date(ldt_da_data), date(ldt_a_data), ls_periodicita, ll_frequenza, ldt_scadenze)
				
				if UpperBound(ldt_scadenze) < 1 then continue
				
				for ll_ii = 1 to UpperBound(ldt_scadenze)
					
					ldt_data_registrazione = ldt_scadenze[ll_ii]
					
					if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(ll_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, ls_messaggio) <> 0 then
						fs_errore = "Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext
						return -1
					end if
					
				next
				
				// ***

		end choose		
	end if	
		
next 
	 
destroy luo_manutenzioni
	
return 0
end function

public function integer wf_copia_budget (ref string fs_errore);long ll_progressivo, ll_progressivo_new, ll_num_revisione
string ls_flag_blocco, ls_cod_budget
datetime ldt_oggi, ldt_null, ldt_elaborazione

uo_manutenzioni luo_manutenzioni
datetime        ldt_data_registrazione, ldt_data_fine_elaborazione, ldt_periodo_inizio[], ldt_periodo_fine[], ldt_data_inizio[]
string          ls_flag_tipo_periodo
long            ll_num_periodi, ll_i, ll_cont
date            ld_data


ldt_oggi = datetime(today(),00:00:00)
setnull(ldt_null)

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")
ls_cod_budget = dw_tes_budget_manutenzioni_lista.getitemstring(dw_tes_budget_manutenzioni_lista.getrow(),"cod_budget")

ldt_elaborazione = datetime( date( em_prossimo_rinnovo.text), 00:00:00)

select max(progressivo)
into   :ll_progressivo_new
from   tes_budget_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca progressivo~r~n"+sqlca.sqlerrtext
	return -1
end if

if ll_progressivo_new = 0 or isnull(ll_progressivo_new) then
	ll_progressivo_new = 1
else
	ll_progressivo_new ++
end if

ll_num_revisione = 0 

INSERT INTO tes_budget_manutenzioni  
		( cod_azienda,   
		  progressivo,   
		  cod_budget,   
		  des_budget,   
		  data_creazione,   
		  num_revisione,   
		  data_consolidato,   
		  cod_tipo_lista_dist,   
		  cod_lista_dist,   
		  flag_tipo_livello_1,   
		  flag_tipo_livello_2,   
		  flag_tipo_livello_3,   
		  flag_tipo_livello_4,   
		  flag_tipo_livello_5,   
		  cod_divisione,   
		  cod_area_aziendale,   
		  cod_reparto,   
		  cod_cat_attrezzature,   
		  cod_attrezzatura,   
		  flag_blocco,   
		  flag_tipo_periodo,   
		  num_periodi,   
		  data_inizio_elaborazione,   
		  data_elaborazione,
		  flag_rinnovo,
		  progressivo_rinnovo)  
		 select cod_azienda,   
				  :ll_progressivo_new,   
				  cod_budget,   
				  des_budget,   
				  :ldt_oggi,   
				  :ll_num_revisione,   
				  :ldt_null,   
				  cod_tipo_lista_dist,   
				  cod_lista_dist,   
				  flag_tipo_livello_1,   
				  flag_tipo_livello_2,   
				  flag_tipo_livello_3,   
				  flag_tipo_livello_4,   
				  flag_tipo_livello_5,   
				  cod_divisione,   
				  cod_area_aziendale,   
				  cod_reparto,   
				  cod_cat_attrezzature,   
				  cod_attrezzatura,   
				  'N',   
				  flag_tipo_periodo,   
				  num_periodi,   
				  :ldt_elaborazione,   
				  :ldt_null,
				  'S',
				  :ll_progressivo
		  from  tes_budget_manutenzioni
		  where cod_azienda = :s_cs_xx.cod_azienda and
		        progressivo = :ll_progressivo  ;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore generazione revisione~r~n"+sqlca.sqlerrtext
	return -1
end if

// *** copio le simulate

INSERT INTO det_budget_manut_simulate  
		( cod_azienda,   
		  progressivo,   
		  prog_simulazione,   
		  cod_tipo_manutenzione,   
		  cod_attrezzatura,   
		  flag_tipo_intervento,   
		  data_registrazione,   
		  ricambio_non_codificato,   
		  cod_prodotto,   
		  quan_ricambio,   
		  costo_unitario_ricambio,   
		  cod_operaio,   
		  costo_orario_operaio,   
		  operaio_ore_previste,   
		  operaio_minuti_previsti,   
		  cod_cat_risorse_esterne,   
		  cod_risorsa_esterna,   
		  risorsa_ore_previste,   
		  risorsa_minuti_previsti,   
		  risorsa_costo_orario,   
		  risorsa_costi_aggiuntivi,   
		  tot_costo_intervento )
select  cod_azienda,   
		  :ll_progressivo_new,   
		  prog_simulazione,   
		  cod_tipo_manutenzione,   
		  cod_attrezzatura,   
		  flag_tipo_intervento,   
		  data_registrazione,   
		  ricambio_non_codificato,   
		  cod_prodotto,   
		  quan_ricambio,   
		  costo_unitario_ricambio,   
		  cod_operaio,   
		  costo_orario_operaio,   
		  operaio_ore_previste,   
		  operaio_minuti_previsti,   
		  cod_cat_risorse_esterne,   
		  cod_risorsa_esterna,   
		  risorsa_ore_previste,   
		  risorsa_minuti_previsti,   
		  risorsa_costo_orario,   
		  risorsa_costi_aggiuntivi,   
		  tot_costo_intervento	
from    det_budget_manut_simulate
where   cod_azienda = :s_cs_xx.cod_azienda and
        progressivo = :ll_progressivo;
		  
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la copia delle simulate: " + sqlca.sqlerrtext
	return -1
end if

// creo il vettore con i range di date che mi servono.

luo_manutenzioni = create uo_manutenzioni

ldt_data_registrazione = ldt_elaborazione

select flag_tipo_periodo,
		 num_periodi
into   :ls_flag_tipo_periodo,
		 :ll_num_periodi
from   tes_budget_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo;

for ll_i = 1 to ll_num_periodi

	ldt_data_fine_elaborazione = luo_manutenzioni.uof_prossima_scadenza (ldt_data_registrazione , ls_flag_tipo_periodo, 1)
	ldt_periodo_inizio[ll_i] = ldt_data_registrazione
	
	ld_data = date(ldt_data_fine_elaborazione)
	ld_data = relativedate(ld_data, -1)
	
	ldt_periodo_fine[ll_i] = datetime(ld_data, 00:00:00)
	
	ldt_data_registrazione = ldt_data_fine_elaborazione

next

destroy luo_manutenzioni

// carico vettore periodi precedeti

declare cu_date_inizio cursor for
select distinct(data_inizio)
from   det_budget_manut_periodi
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo and
		 flag_record_dati = 'S'
order  by data_inizio asc;

open cu_date_inizio;

ll_cont = 1

do while 1 = 1
	fetch cu_date_inizio into :ldt_data_inizio[ll_cont];
	
	if sqlca.sqlcode <> 0 then exit
	
	ll_cont ++
	
loop

ll_cont --
		 
close cu_date_inizio;		 
		  
// *** copia dei periodi		  
for ll_i = 1 to upperbound(ldt_data_inizio)
	
	INSERT INTO det_budget_manut_periodi  
				( cod_azienda,   
				  progressivo,   
				  num_periodo,   
				  prog_periodo,   
				  data_inizio,   
				  data_fine,   
				  cod_divisione,   
				  cod_area_aziendale,   
				  cod_reparto,   
				  cod_cat_attrezzature,   
				  cod_attrezzatura,   
				  costo_prev_ricambi,   
				  costo_prev_ricambi_str,   
				  costo_prev_risorse_int,   
				  costo_prev_risorse_int_str,   
				  costo_prev_risorse_est,   
				  costo_prev_risorse_est_str,   
				  costo_cons_ricambi,   
				  costo_cons_ricambi_str,   
				  costo_cons_risorse_int,   
				  costo_cons_risorse_int_str,   
				  costo_cons_risorse_est,   
				  costo_cons_risorse_est_str,   
				  costo_trend_ricambi,   
				  costo_trend_risorse_int,   
				  costo_trend_risorse_est,   
				  flag_consolidato,   
				  flag_manuale,   
				  flag_record_dati,   
				  ore_prev_risorse_int,   
				  costo_ora_prev_risorse_int,   
				  cod_tipo_manutenzione,   
				  num_copie_attrezzatura )  
	select	  cod_azienda,   
				  :ll_progressivo_new,   
				  num_periodo,   
				  prog_periodo,   
				  :ldt_periodo_inizio[ll_i],   
				  :ldt_periodo_fine[ll_i],   
				  cod_divisione,   
				  cod_area_aziendale,   
				  cod_reparto,   
				  cod_cat_attrezzature,   
				  cod_attrezzatura,   
				  costo_prev_ricambi,   
				  costo_prev_ricambi_str,   
				  costo_prev_risorse_int,   
				  costo_prev_risorse_int_str,   
				  costo_prev_risorse_est,   
				  costo_prev_risorse_est_str,   
				  costo_cons_ricambi,   
				  costo_cons_ricambi_str,   
				  costo_cons_risorse_int,   
				  costo_cons_risorse_int_str,   
				  costo_cons_risorse_est,   
				  costo_cons_risorse_est_str,   
				  costo_trend_ricambi,   
				  costo_trend_risorse_int,   
				  costo_trend_risorse_est,   
				  flag_consolidato,   
				  flag_manuale,   
				  flag_record_dati,   
				  ore_prev_risorse_int,   
				  costo_ora_prev_risorse_int,   
				  cod_tipo_manutenzione,   
				  num_copie_attrezzatura	
	from      det_budget_manut_periodi
	where     cod_azienda = :s_cs_xx.cod_azienda and
				 progressivo = :ll_progressivo and
				 data_inizio = :ldt_data_inizio[ll_i];
				 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la copia dei periodi: " + sqlca.sqlerrtext
		return -1
	end if
	
next

return 0
end function

public function integer wf_rinnova ();string ls_errore
long   ll_risultato


ll_risultato = wf_copia_budget( ref ls_errore)
if ll_risultato <> 0 then
	g_mb.messagebox( "OMNIA", ls_errore)
	rollback;
	return -1
end if

commit;
g_mb.messagebox( "OMNIA", "Rinnovo del contratto eseguito con successo!")
dw_tes_budget_manutenzioni_lista.postevent("pcd_retrieve")
return 0
end function

public function integer wf_consolida_rinnovo ();string    ls_flag_blocco, ls_tipo_programmazione, ls_cod_attrezzatura, ls_cod_tipi_manutenzione[], ls_vuoto[], ls_cod_tipo_manutenzione_appo, ls_errore
datetime  ldt_data_consolidato, ldt_oggi, ldt_data_inizio_elaborazione
datastore lds_attrezzatura, lds_periodi
long      ll_progressivo, ll_progressivo_rinnovo, ll_i, ll_conteggio, ll_j, ll_test, ll_risultato


ldt_oggi = datetime( date(today()), 00:00:00)

ls_flag_blocco = dw_tes_budget_manutenzioni_lista.getitemstring(dw_tes_budget_manutenzioni_lista.getrow(),"flag_blocco")

if ls_flag_blocco = "S" then
	g_mb.messagebox("OMNIA","Impossibile consolidare un Budget Bloccato!")
	return -1
end if

ldt_data_consolidato = dw_tes_budget_manutenzioni_lista.getitemdatetime(dw_tes_budget_manutenzioni_lista.getrow(),"data_consolidato")

if not isnull(ldt_data_consolidato) then
	g_mb.messagebox("OMNIA","Budget già consolidato!")
	return -1
end if

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")
ls_tipo_programmazione = dw_tes_budget_manutenzioni_lista.getitemstring( dw_tes_budget_manutenzioni_lista.getrow(), "flag_tipo_programmazione")
ldt_data_inizio_elaborazione = dw_tes_budget_manutenzioni_lista.getitemdatetime( dw_tes_budget_manutenzioni_lista.getrow(), "data_inizio_elaborazione")

s_cs_xx.parametri.parametro_s_1 = ls_tipo_programmazione
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_s_3)
setnull(s_cs_xx.parametri.parametro_s_4)
s_cs_xx.parametri.parametro_data_1 = ldt_data_inizio_elaborazione
s_cs_xx.parametri.parametro_data_2 = ldt_data_inizio_elaborazione
setnull(s_cs_xx.parametri.parametro_data_3)
	
window_open_parm(w_tes_budget_manut_programma, 0, dw_tes_budget_manutenzioni_lista)

if isnull(s_cs_xx.parametri.parametro_s_2) and isnull(s_cs_xx.parametri.parametro_s_3) and isnull(s_cs_xx.parametri.parametro_s_4) and isnull(s_cs_xx.parametri.parametro_data_1) and isnull(s_cs_xx.parametri.parametro_data_2) and isnull(s_cs_xx.parametri.parametro_data_3) then
	setpointer(arrow!)
	g_mb.messagebox("OMNIA", "Impossibile continuare con il consolida del budget.~r~nCreazione scadenze interrotta dall'utente!", stopsign!)
	return -1
end if			
	
	
lds_attrezzatura = CREATE datastore
lds_attrezzatura.dataobject = 'd_ds_tes_budget_manut_consolida_1'
lds_attrezzatura.settransobject(SQLCA)
lds_attrezzatura.retrieve(s_cs_xx.cod_azienda, ll_progressivo)
	
for ll_i = 1 to lds_attrezzatura.rowcount()
	
	ls_cod_attrezzatura = lds_attrezzatura.getitemstring(ll_i,"cod_attrezzatura")	
	
	declare cu_tipi_manut cursor for
	select  distinct(cod_tipo_manutenzione)
	from    det_budget_manut_periodi
	where   cod_azienda = :s_cs_xx.cod_azienda and
	        progressivo = :ll_progressivo and
			  cod_attrezzatura = :ls_cod_attrezzatura;
			  
	open cu_tipi_manut;
	
	ll_conteggio = 0
	
	do while 1 = 1
		fetch cu_tipi_manut into :ls_cod_tipo_manutenzione_appo;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode < 0 then exit
		ll_conteggio ++
		ls_cod_tipi_manutenzione[ll_conteggio] = ls_cod_tipo_manutenzione_appo
		
	loop
	
	close cu_tipi_manut;
	
	for ll_i = 1 to ll_conteggio
		

		if ( ls_tipo_programmazione = "S" or ls_tipo_programmazione = "P" ) and not isnull(ls_tipo_programmazione) then
		
			setpointer(hourglass!)
			
			if ls_tipo_programmazione = "S" then
				
				ll_risultato = wf_programmazione_semplice( ll_progressivo,                     &
																		 s_cs_xx.parametri.parametro_data_1, &
																		 s_cs_xx.parametri.parametro_s_2, 	 &
																		 'N',    &
																		 ls_cod_attrezzatura,            &
																		 ls_cod_tipi_manutenzione,           &
																		 ref ls_errore, &
																		 lds_periodi)
																		 
				if ll_risultato <> 0 then
					setpointer(arrow!)
					g_mb.messagebox("OMNIA", ls_errore, stopsign!)
					rollback;
					destroy lds_attrezzatura
					destroy lds_periodi
					return -1
				end if
			else
				ll_risultato = wf_programmazione_periodo( s_cs_xx.parametri.parametro_data_2, &
																		s_cs_xx.parametri.parametro_data_3, &
																		'S', &
																		ls_cod_attrezzatura, &
																		ls_cod_tipi_manutenzione, &
																		ls_errore)
	
				if ll_risultato <> 0 then
					setpointer(arrow!)
					g_mb.messagebox("OMNIA", ls_errore, stopsign!)
					rollback;
					destroy lds_attrezzatura
					destroy lds_periodi
					return -1
				end if
				
			end if
		end if
		
	next
next

destroy lds_periodi;
destroy lds_attrezzatura;
// ------------------------   

select progressivo_rinnovo
into   :ll_progressivo_rinnovo
from   tes_budget_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo;
		 
if not isnull(ll_progressivo_rinnovo) and ll_progressivo_rinnovo > 0 then		 

	lds_attrezzatura = CREATE datastore
	lds_attrezzatura.dataobject = 'd_ds_tes_budget_manut_consolida_1'
	lds_attrezzatura.settransobject(SQLCA)
	lds_attrezzatura.retrieve(s_cs_xx.cod_azienda, ll_progressivo_rinnovo)	
		
	for ll_i = 1 to lds_attrezzatura.rowcount()
		
		ls_cod_attrezzatura = lds_attrezzatura.getitemstring( ll_i, "cod_attrezzatura")
		
		ls_cod_tipi_manutenzione = ls_vuoto[]
		
		declare cu_tipi_manut2 cursor for
		select  distinct(cod_tipo_manutenzione)
		from    det_budget_manut_periodi
		where   cod_azienda = :s_cs_xx.cod_azienda and
				  progressivo = :ll_progressivo_rinnovo and
				  cod_attrezzatura = :ls_cod_attrezzatura;
				  
		open cu_tipi_manut2;
		
		ll_conteggio = 0
		
		do while 1 = 1
			fetch cu_tipi_manut2 into :ls_cod_tipo_manutenzione_appo;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then exit
			ll_conteggio ++
			ls_cod_tipi_manutenzione[ll_conteggio] = ls_cod_tipo_manutenzione_appo
			
		loop
		
		close cu_tipi_manut2;		
			
		for ll_j = 1 to ll_conteggio
			
			setnull(ll_test)
			
			select count(cod_azienda)
			into   :ll_test
			from   det_budget_manut_periodi
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       progressivo = :ll_progressivo and 
					 cod_attrezzatura = :ls_cod_attrezzatura;
					 
			if isnull(ll_test) or ll_test < 1 then  //*** blocco direttamente attrezzatura e tipologie di manutenzione
			
				update tab_tipi_manutenzione
				set    flag_blocco = 'S',
				       data_blocco = :ldt_oggi
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_attrezzatura = :ls_cod_attrezzatura;
						 
				update anag_attrezzature
				set    flag_blocco = 'S',
				       data_blocco = :ldt_oggi
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_attrezzatura = :ls_cod_attrezzatura;
						 
				exit  // *** posso uscire direttamente
			else
				
				setnull(ll_test)
			
				select count(cod_azienda)
				into   :ll_test
				from   det_budget_manut_periodi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 progressivo = :ll_progressivo_rinnovo and
						 cod_attrezzatura = :ls_cod_attrezzatura and
						 cod_tipo_manutenzione = :ls_cod_tipi_manutenzione[ll_j];
				
				if isnull(ll_test) or ll_test < 1 then   //*** blocco sia tipologia/attrezzatura
				
					update anag_attrezzature
					set    flag_blocco = 'S',
							 data_blocco = :ldt_oggi
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_attrezzatura = :ls_cod_attrezzatura;
				
				end if			
			end if
		next
		
	next
	
end if

////// ------------------------  imposto i vari flag di consolidamento -------------------------
////
////update tes_budget_manutenzioni
////set    data_consolidato = :ldt_oggi
////where  cod_azienda = :s_cs_xx.cod_azienda and
////       progressivo = :ll_progressivo;
////if sqlca.sqlcode <> 0 then
////	setpointer(arrow!)
////	messagebox("OMNIA","Errore in consolidamento BUDGET.~r~n"+sqlca.sqlerrtext)
////	rollback;
////	return -1
////end if
////
////update det_budget_manut_periodi
////set    flag_consolidato = 'S'
////where  cod_azienda = :s_cs_xx.cod_azienda and
////       progressivo = :ll_progressivo;
////if sqlca.sqlcode <> 0 then
////	setpointer(arrow!)
////	messagebox("OMNIA","Errore in consolidamento BUDGET.~r~n"+sqlca.sqlerrtext)
////	rollback;
////	return -1
////end if
////
////ll_ret = wf_carica_totali_periodi(ll_progressivo, ref ls_errore)
////
////if ll_ret <> 0 then
////	setpointer(arrow!)
////	setpointer(arrow!)
////	rollback;
////	messagebox("OMNIA",ls_errore)
////	return -1
////end if
////
////setpointer(arrow!)
////commit;
////messagebox( "OMNIA", "BUDGET CONSOLIDATO CON SUCCESSO!")
////st_1.text = ""

return 0
end function

public function integer wf_tipi_manutenzione (long fl_progressivo, string fs_cod_attrezzatura_modello, ref string fs_tipi_manutenzione[], ref string fs_errore);long ll_cont_tipi
string ls_cod_tipo_manutenzione

declare cu_tipi cursor for
select  distinct(cod_tipo_manutenzione)
from    det_budget_manut_periodi  
where   cod_azienda = :s_cs_xx.cod_azienda and
        progressivo = :fl_progressivo and
		  cod_attrezzatura = :fs_cod_attrezzatura_modello;
		  
open cu_tipi;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore cu_tipi: " + sqlca.sqlerrtext
	return -1
end if

ll_cont_tipi = 0

do while 1 = 1
	
	fetch cu_tipi into :ls_cod_tipo_manutenzione;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante il caricamento del cursore cu_tipi: " + sqlca.sqlerrtext
		close cu_tipi;
		return -1		
	end if
	if isnull(ls_cod_tipo_manutenzione) then continue
	
	ll_cont_tipi ++
	fs_tipi_manutenzione[ll_cont_tipi] = ls_cod_tipo_manutenzione

	
loop

close cu_tipi;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore cu_tipi: " + sqlca.sqlerrtext
	return -1
end if
	       
return 0				
		
end function

on w_tes_budget_manutenzioni.create
int iCurrent
call super::create
this.em_prossimo_rinnovo=create em_prossimo_rinnovo
this.st_prossimo_rinnovo=create st_prossimo_rinnovo
this.st_1=create st_1
this.hpb_1=create hpb_1
this.cb_respingi=create cb_respingi
this.cb_invia=create cb_invia
this.cb_comunicazioni=create cb_comunicazioni
this.cb_revisione=create cb_revisione
this.cb_consuntivo=create cb_consuntivo
this.cb_consolida=create cb_consolida
this.cb_sblocco=create cb_sblocco
this.cb_blocco=create cb_blocco
this.cb_elabora=create cb_elabora
this.dw_tes_budget_manutenzioni_det_1=create dw_tes_budget_manutenzioni_det_1
this.cb_cerca=create cb_cerca
this.dw_tes_budget_manutenzioni_lista=create dw_tes_budget_manutenzioni_lista
this.dw_folder=create dw_folder
this.dw_det_budget_manut_periodi=create dw_det_budget_manut_periodi
this.dw_det_budget_manut_attrezzature_totali=create dw_det_budget_manut_attrezzature_totali
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_prossimo_rinnovo
this.Control[iCurrent+2]=this.st_prossimo_rinnovo
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.hpb_1
this.Control[iCurrent+5]=this.cb_respingi
this.Control[iCurrent+6]=this.cb_invia
this.Control[iCurrent+7]=this.cb_comunicazioni
this.Control[iCurrent+8]=this.cb_revisione
this.Control[iCurrent+9]=this.cb_consuntivo
this.Control[iCurrent+10]=this.cb_consolida
this.Control[iCurrent+11]=this.cb_sblocco
this.Control[iCurrent+12]=this.cb_blocco
this.Control[iCurrent+13]=this.cb_elabora
this.Control[iCurrent+14]=this.dw_tes_budget_manutenzioni_det_1
this.Control[iCurrent+15]=this.cb_cerca
this.Control[iCurrent+16]=this.dw_tes_budget_manutenzioni_lista
this.Control[iCurrent+17]=this.dw_folder
this.Control[iCurrent+18]=this.dw_det_budget_manut_periodi
this.Control[iCurrent+19]=this.dw_det_budget_manut_attrezzature_totali
this.Control[iCurrent+20]=this.dw_ricerca
end on

on w_tes_budget_manutenzioni.destroy
call super::destroy
destroy(this.em_prossimo_rinnovo)
destroy(this.st_prossimo_rinnovo)
destroy(this.st_1)
destroy(this.hpb_1)
destroy(this.cb_respingi)
destroy(this.cb_invia)
destroy(this.cb_comunicazioni)
destroy(this.cb_revisione)
destroy(this.cb_consuntivo)
destroy(this.cb_consolida)
destroy(this.cb_sblocco)
destroy(this.cb_blocco)
destroy(this.cb_elabora)
destroy(this.dw_tes_budget_manutenzioni_det_1)
destroy(this.cb_cerca)
destroy(this.dw_tes_budget_manutenzioni_lista)
destroy(this.dw_folder)
destroy(this.dw_det_budget_manut_periodi)
destroy(this.dw_det_budget_manut_attrezzature_totali)
destroy(this.dw_ricerca)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_budget_manutenzioni_det_1,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_budget_manutenzioni_det_1,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_budget_manutenzioni_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_budget_manutenzioni_det_1,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")


f_PO_LoadDDDW_DW(dw_det_budget_manut_periodi,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_budget_manut_periodi,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_budget_manut_periodi,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_budget_manut_periodi,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")
					  
f_PO_LoadDDDW_DW(dw_tes_budget_manutenzioni_det_1, &
                 "cod_tipo_lista_dist", &
					  sqlca, &
                 "tab_tipi_liste_dist", &
					  "cod_tipo_lista_dist", &
					  "descrizione", &
					  "")
					  
f_PO_LoadDDDW_DW(dw_tes_budget_manutenzioni_det_1, &
                 "cod_lista_dist", sqlca, &
						"tes_liste_dist", &
						"cod_lista_dist", &
						"des_lista_dist", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")				  
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

dw_tes_budget_manutenzioni_lista.set_dw_key("cod_azienda")
dw_tes_budget_manutenzioni_lista.set_dw_key("progressivo")
dw_tes_budget_manutenzioni_lista.set_dw_options(sqlca,pcca.null_object,c_default, c_default)
dw_tes_budget_manutenzioni_det_1.set_dw_options(sqlca, &
											dw_tes_budget_manutenzioni_lista, &
											c_sharedata + c_scrollparent, &
											c_default)
											
dw_det_budget_manut_periodi.set_dw_options(sqlca, &
											pcca.null_object, &
											c_noretrieveonopen, &
											c_ViewModeBorderUnchanged + &
											c_ViewModeColorUnchanged + &
											c_InactiveDWColorUnchanged + &
											c_NoCursorRowPointer + &
											c_NoCursorRowFocusRecT)
											
dw_det_budget_manut_attrezzature_totali.set_dw_options(sqlca, &
											pcca.null_object, &
											c_noretrieveonopen, &
											c_ViewModeBorderUnchanged + &
											c_ViewModeColorUnchanged + &
											c_InactiveDWColorUnchanged + &
											c_NoCursorRowPointer + &
											c_NoCursorRowFocusRecT)
											
dw_det_budget_manut_periodi.ib_proteggi_chiavi = FALSE

iuo_dw_main = dw_tes_budget_manutenzioni_lista

lw_oggetti[1] = dw_det_budget_manut_attrezzature_totali
dw_folder.fu_assigntab(3, "Riassunto Budget", lw_oggetti[])
lw_oggetti[1] = dw_det_budget_manut_periodi
dw_folder.fu_assigntab(4, "Periodi", lw_oggetti[])
lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = cb_cerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
lw_oggetti[1] = dw_tes_budget_manutenzioni_lista
lw_oggetti[2] = dw_tes_budget_manutenzioni_det_1
lw_oggetti[3] = cb_elabora
//lw_oggetti[4] = cb_attrezzature_ricerca
lw_oggetti[4] = cb_blocco
lw_oggetti[5] = cb_sblocco
lw_oggetti[6] = cb_consolida
lw_oggetti[7] = cb_consuntivo
lw_oggetti[8] = cb_revisione
lw_oggetti[9] = cb_invia
lw_oggetti[10] = cb_respingi
lw_oggetti[11] = cb_comunicazioni
lw_oggetti[12] = hpb_1
lw_oggetti[13] = st_1
lw_oggetti[14] = dw_ricerca
lw_oggetti[15] = st_prossimo_rinnovo
lw_oggetti[16] = em_prossimo_rinnovo
dw_folder.fu_assigntab(2, "Lista Budget", lw_oggetti[])
dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(2)
hpb_1.visible = false

// *** imposto le tre etichette per betasint
string ls_visualizza

select flag
into   :ls_visualizza
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'BET';
		 
if sqlca.sqlcode = 0 and not isnull(ls_visualizza) and ls_visualizza = "S" then
	dw_tes_budget_manutenzioni_det_1.Modify("t_tipo_commessa.Visible='1'")
	dw_tes_budget_manutenzioni_det_1.Modify("t_centro_costo.Visible='1'")
	dw_tes_budget_manutenzioni_det_1.Modify("t_agente.Visible='1'")
end if
//
end event

event pc_save;call super::pc_save;dw_ricerca.resetupdate()
end event

type em_prossimo_rinnovo from editmask within w_tes_budget_manutenzioni
boolean visible = false
integer x = 3566
integer y = 1740
integer width = 411
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
boolean spin = true
end type

type st_prossimo_rinnovo from statictext within w_tes_budget_manutenzioni
boolean visible = false
integer x = 2446
integer y = 1740
integer width = 1120
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Data Elaborazione Prossimo Rinnovo:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_tes_budget_manutenzioni
integer x = 4005
integer y = 1456
integer width = 416
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_tes_budget_manutenzioni
integer x = 4023
integer y = 1380
integer width = 370
integer height = 64
unsignedinteger maxposition = 100
unsignedinteger position = 50
integer setstep = 10
end type

type cb_respingi from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 2068
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Re&spingi"
end type

event clicked;string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, ls_cod_cat_mer

long   ll_riga, ll_progressivo, ll_num_sequenza_corrente

ll_riga = dw_tes_budget_manutenzioni_lista.getrow()

if ll_riga < 1 then return
ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber( ll_riga, "progressivo")		
ls_cod_tipo_lista_dist = dw_tes_budget_manutenzioni_lista.getitemstring( ll_riga, "cod_tipo_lista_dist")
ls_cod_lista_dist = dw_tes_budget_manutenzioni_lista.getitemstring( ll_riga, "cod_lista_dist")
			
if not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
	select flag_gestione_fasi
	into   :ls_flag_gestione_fasi
	from   tab_tipi_liste_dist
	where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
	if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
						 
	if ls_flag_gestione_fasi <> "S" then return
		
	ls_cod_area_aziendale = dw_tes_budget_manutenzioni_lista.getitemstring( ll_riga, "cod_area_aziendale")
		
	if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""
					
	ll_num_sequenza_corrente = dw_tes_budget_manutenzioni_lista.getitemnumber( ll_riga, "num_sequenza_corrente")
		
	s_cs_xx.parametri.parametro_s_1 = "L"
						
	s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
						
	s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
							
	s_cs_xx.parametri.parametro_s_4 = " "
							
	s_cs_xx.parametri.parametro_s_5 = "Gestione Budget Manutenzioni " + string(ll_progressivo)
							
	s_cs_xx.parametri.parametro_s_6 = "Gestione Budget Manutenzioni " + string(ll_progressivo)
							
	s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati ?"
	
	s_cs_xx.parametri.parametro_s_8 = ""
		
	s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
		
	s_cs_xx.parametri.parametro_s_10 = ""
							
	s_cs_xx.parametri.parametro_s_11 = ""
							
	s_cs_xx.parametri.parametro_s_12 = ""
						
	s_cs_xx.parametri.parametro_s_13 = ""
							
	s_cs_xx.parametri.parametro_s_14 = ""
							
	s_cs_xx.parametri.parametro_s_15 = "B"
	
	s_cs_xx.parametri.parametro_d_1 = ll_progressivo
							
	s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
							
	s_cs_xx.parametri.parametro_b_1 = false
							
	openwithparm( w_invio_messaggi, 0)	
		
	if not isnull(s_cs_xx.parametri.parametro_ul_1) then
		
		if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
			
		update tes_budget_manutenzioni
		set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :ll_progressivo;
					 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
			
		commit;
			
		parent.triggerevent("pc_retrieve")
			
		dw_tes_budget_manutenzioni_lista.setrow( ll_riga)					 
			
	end if
			
else		
	g_mb.messagebox( "OMNIA", "Attenzione: controllare l'associazione tipo causa / tipo lista!")
	return -1		
end if					
end event

type cb_invia from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 1968
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "I&nvia"
end type

event clicked;string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale
       

long   ll_riga, ll_progressivo, ll_num_sequenza_corrente

ll_riga = dw_tes_budget_manutenzioni_lista.getrow()

if ll_riga < 1 then return

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber( ll_riga, "progressivo")		
ls_cod_tipo_lista_dist = dw_tes_budget_manutenzioni_lista.getitemstring( ll_riga, "cod_tipo_lista_dist")
ls_cod_lista_dist = dw_tes_budget_manutenzioni_lista.getitemstring( ll_riga, "cod_lista_dist")
			
if not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
	select flag_gestione_fasi
	into   :ls_flag_gestione_fasi
	from   tab_tipi_liste_dist
	where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
	if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
		
	if ls_flag_gestione_fasi <> "S" then return
					
	ll_num_sequenza_corrente = dw_tes_budget_manutenzioni_lista.getitemnumber( ll_riga, "num_sequenza_corrente")
	
	ls_cod_area_aziendale = dw_tes_budget_manutenzioni_lista.getitemstring( ll_riga, "cod_area_aziendale")
		
	if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""		
						
	s_cs_xx.parametri.parametro_s_1 = "L"
						
	s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
						
	s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
							
	s_cs_xx.parametri.parametro_s_4 = " "
							
	s_cs_xx.parametri.parametro_s_5 = "Registrazione Budget Manutenzioni " + string(ll_progressivo) 
							
	s_cs_xx.parametri.parametro_s_6 = "E' stato registrato il Budget Manutenzioni " + string(ll_progressivo) 
							
	s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati?"
	
	s_cs_xx.parametri.parametro_s_8 = ""
		
	s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
							
	s_cs_xx.parametri.parametro_s_10 = ""
							
	s_cs_xx.parametri.parametro_s_11 = ""
							
	s_cs_xx.parametri.parametro_s_12 = ""
						
	s_cs_xx.parametri.parametro_s_13 = ""
							
	s_cs_xx.parametri.parametro_s_14 = ""
							
	s_cs_xx.parametri.parametro_s_15 = "B"
	
	s_cs_xx.parametri.parametro_d_1 = ll_progressivo
							
	s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
							
	s_cs_xx.parametri.parametro_b_1 = true
							
	openwithparm( w_invio_messaggi, 0)	
		
	if not isnull(s_cs_xx.parametri.parametro_ul_1) then
		
		if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
			
		update tes_budget_manutenzioni
		set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :ll_progressivo;
					 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
			
		commit;
			
		parent.triggerevent("pc_retrieve")
		
		dw_tes_budget_manutenzioni_lista.setrow( ll_riga)
				 
			
	end if
			
else
		
	g_mb.messagebox( "OMNIA", "Attenzione: controllare l'associazionetipo lista!")
	return -1
	
end if
end event

type cb_comunicazioni from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 2168
integer width = 370
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Co&municaz."
end type

event clicked;long   ll_progressivo, ll_row

ll_row = dw_tes_budget_manutenzioni_lista.getrow()

if ll_row < 1 then return

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(ll_row, "progressivo")

setnull(s_cs_xx.parametri.parametro_ul_3)

s_cs_xx.parametri.parametro_ul_3 = ll_progressivo

window_open(w_schede_intervento_comunicazioni,-1)


end event

type cb_revisione from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 1840
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Revisione"
end type

event clicked;long ll_progressivo, ll_progressivo_new, ll_num_revisione
string ls_flag_blocco, ls_cod_budget
datetime ldt_oggi, ldt_null, ldt_data_consolida


ldt_data_consolida = dw_tes_budget_manutenzioni_lista.getitemdatetime(dw_tes_budget_manutenzioni_lista.getrow(),"data_consolidato")
if not isnull(ldt_data_consolida) then
	if g_mb.messagebox("OMNIA","Rinnovo il budget? (Saranno generate le nuove scadenze e non sarà più possibile modificarlo!)",Question!,YesNo!,2) = 2 then return
	wf_rinnova()
	return
end if


if g_mb.messagebox("OMNIA","Creo la revisione!",Question!,YesNo!,2) = 2 then return

ldt_oggi = datetime(today(),00:00:00)
setnull(ldt_null)

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")
ls_cod_budget = dw_tes_budget_manutenzioni_lista.getitemstring(dw_tes_budget_manutenzioni_lista.getrow(),"cod_budget")

select max(progressivo)
into :ll_progressivo_new
from  tes_budget_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca progressivo~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

if ll_progressivo_new = 0 or isnull(ll_progressivo_new) then
	ll_progressivo_new = 1
else
	ll_progressivo_new ++
end if

select max(num_revisione)
into :ll_num_revisione
from  tes_budget_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_budget = :ls_cod_budget;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca progressivo~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

if ll_num_revisione = 0 or isnull(ll_num_revisione) then
	ll_num_revisione = 1
else
	ll_num_revisione ++
end if


INSERT INTO tes_budget_manutenzioni  
		( cod_azienda,   
		  progressivo,   
		  cod_budget,   
		  des_budget,   
		  data_creazione,   
		  num_revisione,   
		  data_consolidato,   
		  cod_tipo_lista_dist,   
		  cod_lista_dist,   
		  flag_tipo_livello_1,   
		  flag_tipo_livello_2,   
		  flag_tipo_livello_3,   
		  flag_tipo_livello_4,   
		  flag_tipo_livello_5,   
		  cod_divisione,   
		  cod_area_aziendale,   
		  cod_reparto,   
		  cod_cat_attrezzature,   
		  cod_attrezzatura,   
		  flag_blocco,   
		  flag_tipo_periodo,   
		  num_periodi,   
		  data_inizio_elaborazione,   
		  data_elaborazione)  
		 select cod_azienda,   
				  :ll_progressivo_new,   
				  cod_budget,   
				  des_budget,   
				  :ldt_oggi,   
				  :ll_num_revisione,   
				  :ldt_null,   
				  cod_tipo_lista_dist,   
				  cod_lista_dist,   
				  flag_tipo_livello_1,   
				  flag_tipo_livello_2,   
				  flag_tipo_livello_3,   
				  flag_tipo_livello_4,   
				  flag_tipo_livello_5,   
				  cod_divisione,   
				  cod_area_aziendale,   
				  cod_reparto,   
				  cod_cat_attrezzature,   
				  cod_attrezzatura,   
				  'N',   
				  flag_tipo_periodo,   
				  num_periodi,   
				  data_inizio_elaborazione,   
				  :ldt_null
		  from  tes_budget_manutenzioni
		  where cod_azienda = :s_cs_xx.cod_azienda and
		        progressivo = :ll_progressivo  ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore generazione revisione~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

commit;

dw_tes_budget_manutenzioni_lista.postevent("pcd_retrieve")

end event

type cb_consuntivo from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 1292
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Consuntiva"
end type

event clicked;long ll_ret
string ls_errore

if g_mb.messagebox("OMNIA","Procedo con consuntivazione?",Question!,YesNo!,2) = 2 then return

setpointer(hourglass!)
hpb_1.show()

ll_ret = wf_carica_consuntivo(dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo"), ref ls_errore)

hpb_1.hide()

if ll_ret = 0 then
	setpointer(arrow!)
	commit;
	g_mb.messagebox("OMNIA","Operazione eseguita con successo!")
else
	setpointer(arrow!)
	rollback;
	g_mb.messagebox("OMNIA",ls_errore)
end if
end event

type cb_consolida from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Consolida"
end type

event clicked;long ll_progressivo, ll_cont, ll_i, ll_ret, ll_rows, ll_y, ll_anno_registrazione, ll_frequenza,&
     ll_num_reg_programma, ll_num_reg_manut, ll_cont_tipi, ll_ore_intervento, ll_minuti_intervento, &
	  ll_num_reg_manutenzione, ll_num_copie_attrezzatura, ll_j, ll_risultato, ll_progressivo_rinnovo
string ls_flag_blocco, ls_cod_divisione_origine, ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_flag_rinnovo, &
       ls_cod_area_new, ls_cod_reparto_new, ls_cod_attrezzatura, ls_cod_attrezzatura_new, ls_errore, &
		 ls_flag_periodicita, ls_vuoto[], ls_cod_tipi_manutenzione[], ls_tipo_programmazione
dec{4} ld_tempo_previsto, ld_costo_prev_risorse_int,ld_costo_prev_risorse_est
datetime ldt_oggi, ldt_data_consolidato, ldt_data_inizio_elaborazione, ldt_data_registrazione, ldt_data_consolida
datastore lds_attrezzatura, lds_periodi
uo_manutenzioni luo_manutenzioni


if g_mb.messagebox("OMNIA","Consolido il budget? Saranno generate le scadenze e quindi non sarà più possibile modificarlo!",Question!,YesNo!,2) = 2 then return

ls_flag_rinnovo = dw_tes_budget_manutenzioni_lista.getitemstring(dw_tes_budget_manutenzioni_lista.getrow(),"flag_rinnovo")
ll_progressivo_rinnovo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo_rinnovo")

if ls_flag_rinnovo = "S" and not isnull(ls_flag_rinnovo) and not isnull(ll_progressivo_rinnovo) and ll_progressivo_rinnovo > 0 then
	
	// wf_consolida_rinnovo
	
end if

ldt_oggi = datetime(today(),00:00:00)

ls_flag_blocco = dw_tes_budget_manutenzioni_lista.getitemstring(dw_tes_budget_manutenzioni_lista.getrow(),"flag_blocco")

if ls_flag_blocco = "S" then
	g_mb.messagebox("OMNIA","Impossibile consolidare un Budget Bloccato!")
	return
end if

ldt_data_consolidato = dw_tes_budget_manutenzioni_lista.getitemdatetime(dw_tes_budget_manutenzioni_lista.getrow(),"data_consolidato")

if not isnull(ldt_data_consolidato) then
	g_mb.messagebox("OMNIA","Budget già consolidato!")
	return
end if

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")
ls_cod_divisione_origine = dw_tes_budget_manutenzioni_lista.getitemstring(dw_tes_budget_manutenzioni_lista.getrow(),"cod_divisione")
ls_flag_periodicita = dw_tes_budget_manutenzioni_lista.getitemstring(dw_tes_budget_manutenzioni_lista.getrow(),"flag_tipo_periodo")
ldt_data_inizio_elaborazione = dw_tes_budget_manutenzioni_lista.getitemdatetime(dw_tes_budget_manutenzioni_lista.getrow(),"data_inizio_elaborazione")
ll_frequenza = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"num_periodi")
ls_tipo_programmazione = dw_tes_budget_manutenzioni_lista.getitemstring( dw_tes_budget_manutenzioni_lista.getrow(), "flag_tipo_programmazione")

ll_anno_registrazione = f_anno_esercizio()

select max(num_registrazione)
into   :ll_num_reg_programma
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_reg_programma) then
	ll_num_reg_programma = 0
end if

select max(num_registrazione)
into   :ll_num_reg_manutenzione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_reg_manutenzione) then
	ll_num_reg_manutenzione = 0
end if

		 
// ------------------------  richiedo divisione/reparto/area effettivi ---------------------

s_cs_xx.parametri.parametro_s_1 = ""
s_cs_xx.parametri.parametro_s_2 = ""
s_cs_xx.parametri.parametro_s_3 = ""
window_open_parm(w_tes_budget_manut_consolida, 0, dw_tes_budget_manutenzioni_lista)
if s_cs_xx.parametri.parametro_s_1 = "ANNULLA" then return

setpointer(hourglass!)

ls_cod_divisione = s_cs_xx.parametri.parametro_s_1
ls_cod_area_aziendale = s_cs_xx.parametri.parametro_s_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_3


//eseguo conversione periodicita fra budget e manutenzioni
choose case ls_flag_periodicita
	case "S", "E"		// settimana
		//va bene
	case "B"				// bisettimanale
		ls_flag_periodicita = "G"
		ll_frequenza = ll_frequenza * 15
	case "T"				// trimestrale
		ls_flag_periodicita = "E"
		ll_frequenza = ll_frequenza * 3
	case "A"				// annuale
		ls_flag_periodicita = "E"
		ll_frequenza = ll_frequenza * 12
	case "I"				// bimensile
		ls_flag_periodicita = "E"
		ll_frequenza = ll_frequenza * 2
	case "R"				// semestrale
		ls_flag_periodicita = "E"
		ll_frequenza = ll_frequenza * 6
end choose		

// ------------------------  procedo con creazione attrezzature e tipi manutenzione --------

if ls_cod_divisione <> ls_cod_divisione_origine then
	
	
	// ------------------------  richiedo i dati che mi servono per la programmazione ---------------------
				
	s_cs_xx.parametri.parametro_s_1 = ls_tipo_programmazione
	setnull(s_cs_xx.parametri.parametro_s_2)
	setnull(s_cs_xx.parametri.parametro_s_3)
	setnull(s_cs_xx.parametri.parametro_s_4)
	s_cs_xx.parametri.parametro_data_1 = ldt_data_inizio_elaborazione
	s_cs_xx.parametri.parametro_data_2 = ldt_data_inizio_elaborazione
	setnull(s_cs_xx.parametri.parametro_data_3)
	
	window_open_parm(w_tes_budget_manut_programma, 0, dw_tes_budget_manutenzioni_lista)
	
	if isnull(s_cs_xx.parametri.parametro_s_2) and isnull(s_cs_xx.parametri.parametro_s_3) and isnull(s_cs_xx.parametri.parametro_s_4) and isnull(s_cs_xx.parametri.parametro_data_1) and isnull(s_cs_xx.parametri.parametro_data_2) and isnull(s_cs_xx.parametri.parametro_data_3) then
		setpointer(arrow!)
		g_mb.messagebox("OMNIA", "Impossibile continuare con il consolida del budget.~r~nCreazione scadenze interrotta dall'utente!", stopsign!)
		destroy lds_attrezzatura
		destroy lds_periodi
		rollback;
		return		
	end if			
	
	
	// cambiato divisione (commessa se global service) e quindi procedo con la duplicazione di tutto; 
	// altrimenti consolido semplicemente il budget corrente.
	
	
	lds_attrezzatura = CREATE datastore
	lds_attrezzatura.dataobject = 'd_ds_tes_budget_manut_consolida_1'
	lds_attrezzatura.settransobject(SQLCA)
	ll_cont = lds_attrezzatura.retrieve(s_cs_xx.cod_azienda, ll_progressivo)
	
	lds_periodi = CREATE datastore
	lds_periodi.dataobject = 'd_ds_tes_budget_manut_consolida_3'
	lds_periodi.settransobject(SQLCA)
	// procedo con creazione attrezzature e tipologie manutenzioni
	
	for ll_i = 1 to ll_cont
		
		ls_cod_attrezzatura = lds_attrezzatura.getitemstring(ll_i,"cod_attrezzatura")	
		ll_num_copie_attrezzatura = lds_attrezzatura.getitemnumber( ll_i, "num_copie_attrezzatura")
		
		select cod_area_aziendale, 
		       cod_reparto
		into   :ls_cod_area_new, 
		       :ls_cod_reparto_new
		from   anag_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_attrezzatura = :ls_cod_attrezzatura;
				 
		if sqlca.sqlcode <> 0 then
			
			setpointer(arrow!)
			g_mb.messagebox("OMNIA","Errore in ricerca attrezzatura~r~n"+sqlca.sqlerrtext)
			destroy lds_attrezzatura
			destroy lds_periodi
			rollback;
			destroy lds_attrezzatura
			return
			
		end if
		
		if not isnull(ls_cod_area_aziendale) then ls_cod_area_new = ls_cod_area_aziendale
		if not isnull(ls_cod_reparto) then ls_cod_reparto_new = ls_cod_reparto
		
		// *** devo creare n copie dell'attrezzatura
		
		for ll_j = 1 to ll_num_copie_attrezzatura
			
			setnull(ls_cod_attrezzatura_new)

			ll_risultato = wf_nuova_attrezzatura( ls_cod_attrezzatura, ls_cod_reparto_new, ls_cod_area_new, ref ls_errore, ref ls_cod_attrezzatura_new)
				
			if ll_risultato <> 0 then
				setpointer(arrow!)
				g_mb.messagebox( "OMNIA", ls_errore)
				destroy lds_attrezzatura
				destroy lds_periodi
				rollback;
				destroy lds_attrezzatura
				return
			end if
			
			// copio le tipologie manutenzione
			
			ls_cod_tipi_manutenzione = ls_vuoto
			ll_risultato = wf_copia_tipologie( ll_progressivo, ls_cod_attrezzatura, ls_cod_attrezzatura_new, ls_flag_periodicita, ll_frequenza, ref ls_errore, ref ls_cod_tipi_manutenzione[])
			if ll_risultato <> 0 then
				setpointer(arrow!)
				g_mb.messagebox( "OMNIA", ls_errore)
				destroy lds_attrezzatura
				destroy lds_periodi
				rollback;
				return
			end if
			
			// aggiorno i periodi ora che ho creato le tipologie di manutenzione per 
			// l'attrezzatura ----------------------   
			
			ll_risultato = wf_copia_periodi( ll_progressivo, ls_cod_divisione, ls_cod_attrezzatura, ls_cod_attrezzatura_new, ref ls_errore)
			if ll_risultato <> 0 then
				setpointer( arrow!)
				g_mb.messagebox( "OMNIA", ls_errore)
				destroy lds_attrezzatura;
				destroy lds_periodi;
				rollback;
				return
			end if
			
			update det_budget_manut_periodi
			set    cod_attrezzatura = :ls_cod_attrezzatura_new, 
					 cod_divisione = :ls_cod_divisione,
					 cod_area_aziendale = :ls_cod_area_new,
					 cod_reparto = :ls_cod_reparto_new
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 progressivo = :ll_progressivo and
					 cod_attrezzatura = :ls_cod_attrezzatura_new;
					 
			if sqlca.sqlcode <> 0 then
				setpointer(arrow!)
				g_mb.messagebox("OMNIA", "3.Errore in consolidamento BUDGET.~r~n"+sqlca.sqlerrtext)
				destroy lds_attrezzatura
				destroy lds_periodi
				rollback;
				return
			end if		
			
			
			if ( ls_tipo_programmazione = "S" or ls_tipo_programmazione = "P" ) and not isnull(ls_tipo_programmazione) then
			
				setpointer(hourglass!)
				
				if ls_tipo_programmazione = "S" then
					ll_risultato = wf_programmazione_semplice( ll_progressivo,                     &
                                                          s_cs_xx.parametri.parametro_data_1, &
					                                           s_cs_xx.parametri.parametro_s_2, 	 &
																			 s_cs_xx.parametri.parametro_s_3,    &
																			 ls_cod_attrezzatura_new,            &
																			 ls_cod_tipi_manutenzione,           &
																			 ref ls_errore, &
																			 lds_periodi)
																			 
					if ll_risultato <> 0 then
						setpointer(arrow!)
						g_mb.messagebox("OMNIA", ls_errore, stopsign!)
						rollback;
						destroy lds_attrezzatura
						destroy lds_periodi						
						return								
					end if
				else
					ll_risultato = wf_programmazione_periodo( s_cs_xx.parametri.parametro_data_2, &
					                                          s_cs_xx.parametri.parametro_data_3, &
																			s_cs_xx.parametri.parametro_s_4, &
																			ls_cod_attrezzatura_new, &
																			ls_cod_tipi_manutenzione, &
																			ls_errore)

					if ll_risultato <> 0 then
						setpointer(arrow!)
						g_mb.messagebox("OMNIA", ls_errore, stopsign!)
						rollback;
						destroy lds_attrezzatura
						destroy lds_periodi						
						return								
					end if
					
				end if
				
			
			end if			
			
		next
			
	next
	
	for ll_i = 1 to ll_cont
		
		ls_cod_attrezzatura = lds_attrezzatura.getitemstring(ll_i,"cod_attrezzatura")	
		
		delete from det_budget_manut_periodi
		where       cod_azienda = :s_cs_xx.cod_azienda and
		            progressivo = :ll_progressivo and
						cod_attrezzatura = :ls_cod_attrezzatura;
						
		if sqlca.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox("OMNIA", ls_errore, stopsign!)
			rollback;
			destroy lds_attrezzatura
			destroy lds_periodi						
			return								
		end if			
		
	next
		
	destroy lds_attrezzatura
	destroy lds_periodi

else
	
	ls_cod_area_new = ls_cod_area_aziendale
	ls_cod_reparto_new = ls_cod_reparto
	
	s_cs_xx.parametri.parametro_s_1 = ls_tipo_programmazione
	setnull(s_cs_xx.parametri.parametro_s_2)
	setnull(s_cs_xx.parametri.parametro_s_3)
	setnull(s_cs_xx.parametri.parametro_s_4)
	s_cs_xx.parametri.parametro_data_1 = ldt_data_inizio_elaborazione
	s_cs_xx.parametri.parametro_data_2 = ldt_data_inizio_elaborazione
	setnull(s_cs_xx.parametri.parametro_data_3)
	
	window_open_parm(w_tes_budget_manut_programma, 0, dw_tes_budget_manutenzioni_lista)
	
	if isnull(s_cs_xx.parametri.parametro_s_2) and isnull(s_cs_xx.parametri.parametro_s_3) and isnull(s_cs_xx.parametri.parametro_s_4) and isnull(s_cs_xx.parametri.parametro_data_1) and isnull(s_cs_xx.parametri.parametro_data_2) and isnull(s_cs_xx.parametri.parametro_data_3) then
		setpointer(arrow!)
		g_mb.messagebox("OMNIA", "Impossibile continuare con il consolida del budget.~r~nCreazione scadenze interrotta dall'utente!", stopsign!)
		destroy lds_attrezzatura
		destroy lds_periodi
		rollback;
		return		
	end if			
	
	
	// cambiato divisione (commessa se global service) e quindi procedo con la duplicazione di tutto; 
	// altrimenti consolido semplicemente il budget corrente.
	
	
	lds_attrezzatura = CREATE datastore
	lds_attrezzatura.dataobject = 'd_ds_tes_budget_manut_consolida_1'
	lds_attrezzatura.settransobject(SQLCA)
	ll_cont = lds_attrezzatura.retrieve(s_cs_xx.cod_azienda, ll_progressivo)
	
	lds_periodi = CREATE datastore
	lds_periodi.dataobject = 'd_ds_tes_budget_manut_consolida_3'
	lds_periodi.settransobject(SQLCA)
	// procedo con creazione attrezzature e tipologie manutenzioni
	
	for ll_i = 1 to ll_cont
		
		ls_cod_attrezzatura = lds_attrezzatura.getitemstring(ll_i,"cod_attrezzatura")	
		ll_num_copie_attrezzatura = lds_attrezzatura.getitemnumber( ll_i, "num_copie_attrezzatura")
		
		ll_ret = wf_tipi_manutenzione( ll_progressivo, ls_cod_attrezzatura, ref ls_cod_tipi_manutenzione, ref ls_errore)
		
		if ll_ret <> 0 then
			
			setpointer(arrow!)
			g_mb.messagebox("OMNIA", ls_errore)
			destroy lds_attrezzatura
			destroy lds_periodi
			rollback;
			destroy lds_attrezzatura
			return
			
		end if
		
		select cod_area_aziendale, 
		       cod_reparto
		into   :ls_cod_area_new, 
		       :ls_cod_reparto_new
		from   anag_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_attrezzatura = :ls_cod_attrezzatura;
				 
		if sqlca.sqlcode <> 0 then
			
			setpointer(arrow!)
			g_mb.messagebox("OMNIA","Errore in ricerca attrezzatura~r~n"+sqlca.sqlerrtext)
			destroy lds_attrezzatura
			destroy lds_periodi
			rollback;
			destroy lds_attrezzatura
			return
			
		end if
		

		update det_budget_manut_periodi
		set    cod_divisione = :ls_cod_divisione,
				 cod_area_aziendale = :ls_cod_area_new,
				 cod_reparto = :ls_cod_reparto_new
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 progressivo = :ll_progressivo and
				 cod_attrezzatura = :ls_cod_attrezzatura;
					 
		if sqlca.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox("OMNIA", "4.Errore in consolidamento BUDGET.~r~n"+sqlca.sqlerrtext)
			destroy lds_attrezzatura
			destroy lds_periodi
			rollback;
			return
		end if		
			
			
		if ( ls_tipo_programmazione = "S" or ls_tipo_programmazione = "P" ) and not isnull(ls_tipo_programmazione) then
		
			setpointer(hourglass!)
			
			if ls_tipo_programmazione = "S" then
				ll_risultato = wf_programmazione_semplice( ll_progressivo,                     &
																		 s_cs_xx.parametri.parametro_data_1, &
																		 s_cs_xx.parametri.parametro_s_2, 	 &
																		 s_cs_xx.parametri.parametro_s_3,    &
																		 ls_cod_attrezzatura,            &
																		 ls_cod_tipi_manutenzione,           &
																		 ref ls_errore, &
																		 lds_periodi)
																		 
				if ll_risultato <> 0 then
					setpointer(arrow!)
					g_mb.messagebox("OMNIA", ls_errore, stopsign!)
					rollback;
					destroy lds_attrezzatura
					destroy lds_periodi						
					return								
				end if
			else
				ll_risultato = wf_programmazione_periodo( s_cs_xx.parametri.parametro_data_2, &
																		s_cs_xx.parametri.parametro_data_3, &
																		s_cs_xx.parametri.parametro_s_4, &
																		ls_cod_attrezzatura, &
																		ls_cod_tipi_manutenzione, &
																		ls_errore)

				if ll_risultato <> 0 then
					setpointer(arrow!)
					g_mb.messagebox("OMNIA", ls_errore, stopsign!)
					rollback;
					destroy lds_attrezzatura
					destroy lds_periodi						
					return								
				end if
				
			end if
			
		
		end if			

	next
	
	destroy lds_attrezzatura
	destroy lds_periodi	
	


end if

// ------------------------  imposto i vari flag di consolidamento -------------------------

update tes_budget_manutenzioni
set    data_consolidato = :ldt_oggi, 
		 cod_divisione = :ls_cod_divisione,
		 cod_area_aziendale = :ls_cod_area_new,
		 cod_reparto = :ls_cod_reparto_new
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo;
if sqlca.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox("OMNIA","1.Errore in consolidamento BUDGET.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

update det_budget_manut_periodi
set    flag_consolidato = 'S'
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo;
if sqlca.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox("OMNIA","2.Errore in consolidamento BUDGET.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

ll_ret = wf_carica_totali_periodi(ll_progressivo, ref ls_errore)

if ll_ret <> 0 then
	setpointer(arrow!)
	setpointer(arrow!)
	rollback;
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

setpointer(arrow!)
commit;
g_mb.messagebox( "OMNIA", "BUDGET CONSOLIDATO CON SUCCESSO!")
st_1.text = ""
end event

type cb_sblocco from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 1640
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sblocco"
end type

event clicked;long ll_progressivo


if g_mb.messagebox("OMNIA","Sblocco il budget?",Question!,YesNo!,2) = 2 then return

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")

update tes_budget_manutenzioni
set    flag_blocco = 'N'
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in blocco BUDGET.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

commit;

end event

type cb_blocco from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Blocco"
end type

event clicked;long ll_progressivo


if g_mb.messagebox("OMNIA","Blocco il budget?",Question!,YesNo!,2) = 2 then return

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")

update tes_budget_manutenzioni
set    flag_blocco = 'S'
where  cod_azienda = :s_cs_xx.cod_azienda and
       progressivo = :ll_progressivo;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in blocco BUDGET.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

commit;

end event

type cb_elabora from commandbutton within w_tes_budget_manutenzioni
integer x = 4027
integer y = 1192
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;long ll_ret
string ls_errore

if g_mb.messagebox("OMNIA","Procedo con l'elaborazione?",Question!,YesNo!,2) = 2 then return

setpointer(hourglass!)
ll_ret = wf_carica_manut_simulate(dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo"), ref ls_errore)

if ll_ret <> 0 then
	setpointer(arrow!)
	rollback;
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

ll_ret = wf_carica_manut_periodi(dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo"), ref ls_errore)

if ll_ret <> 0 then
	setpointer(arrow!)
	rollback;
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

ll_ret = wf_carica_totali_periodi(dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo"), ref ls_errore)

if ll_ret <> 0 then
	setpointer(arrow!)
	rollback;
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

setpointer(arrow!)

g_mb.messagebox("OMNIA","Operazione eseguita con successo!")

commit;
end event

type dw_tes_budget_manutenzioni_det_1 from uo_cs_xx_dw within w_tes_budget_manutenzioni
event ue_aree_aziendali ( )
event ue_carica_reparti ( )
integer x = 18
integer y = 1184
integer width = 4037
integer height = 1108
integer taborder = 20
string dataobject = "d_tes_budget_manutenzioni_det_1"
boolean border = false
end type

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_sort (dw_tes_budget_manutenzioni_det_1,"cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_divisione = '" + ls_divisione + "' or cod_divisione is null ) ", " ordinamento ASC ")					  		
else
	f_PO_LoadDDDW_sort (dw_tes_budget_manutenzioni_det_1,"cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  	
end if
end event

event ue_carica_reparti();string ls_cod_area_aziendale

ls_cod_area_aziendale = getitemstring(getrow(),"cod_area_aziendale")

if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" then
	f_po_loaddddw_sort(dw_tes_budget_manutenzioni_det_1,&
						  "cod_reparto", &
						  sqlca, &
						  "anag_reparti",&
						  "cod_reparto",&
						  "des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_area_aziendale = '" + ls_cod_area_aziendale + "' or cod_area_aziendale IS NULL ) ", &
						  " cod_reparto ASC ")	
else
	f_po_loaddddw_sort( dw_tes_budget_manutenzioni_det_1,&
						     "cod_reparto", &
						     sqlca, &
						     "anag_reparti",&
						     "cod_reparto",&
						     "des_reparto", &
						     "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
						     " cod_reparto ASC ")		
end if


end event

event itemchanged;call super::itemchanged;string ls_null, ls_text

setnull(ls_null)
ls_text = gettext()
choose case getcolumnname()
	case "flag_tipo_livello_1"
		setitem(getrow(),"flag_tipo_livello_2", "N")
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		setitem(getrow(),"flag_tipo_livello_5", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		settaborder("flag_tipo_livello_5", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_2", 90)
		end if
		
	case "flag_tipo_livello_2"
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		setitem(getrow(),"flag_tipo_livello_5", "N")
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		settaborder("flag_tipo_livello_5", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_3", 100)
		end if
		
	case "flag_tipo_livello_3"
		setitem(getrow(),"flag_tipo_livello_4", "N")
		setitem(getrow(),"flag_tipo_livello_5", "N")
		settaborder("flag_tipo_livello_4", 0)
		settaborder("flag_tipo_livello_5", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_4", 110)
		end if
		
	case "flag_tipo_livello_4"
		setitem(getrow(),"flag_tipo_livello_5", "N")
		settaborder("flag_tipo_livello_5", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_5", 120)
		end if
		
	case "cod_tipo_lista_dist"
		setitem( getrow(), "cod_lista_dist", ls_null)
			
		f_po_loaddddw_dw( dw_tes_budget_manutenzioni_det_1, "cod_lista_dist", sqlca, &
								"tes_liste_dist", &
								"cod_lista_dist", &
								"des_lista_dist", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + ls_text + "' ")
								
								
	case "cod_divisione"
		
		postevent("ue_aree_aziendali")
		
	case "cod_area_aziendale"
		
		postevent("ue_carica_reparti")
	
		
end choose

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_tes_budget_manutenzioni_det_1,"cod_attrezzatura")
end choose
end event

event pcd_modify;call super::pcd_modify;object.b_ricerca_att.enabled = true
end event

event pcd_new;call super::pcd_new;object.b_ricerca_att.enabled = true
end event

event pcd_view;call super::pcd_view;object.b_ricerca_att.enabled = false
end event

event pcd_save;call super::pcd_save;object.b_ricerca_att.enabled = false
end event

type cb_cerca from commandbutton within w_tes_budget_manutenzioni
integer x = 2633
integer y = 1052
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_folder.fu_SelectTab(2)
dw_tes_budget_manutenzioni_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


//string   ls_flag_blocco, ls_flag_consolidato, where_clause, ls_prova, new_select
//datetime ldt_data_inizio, ldt_data_fine
//long     ll_index
//
//ls_flag_blocco = dw_ricerca.getitemstring( 1, "flag_blocco")
//ls_flag_consolidato = dw_ricerca.getitemstring( 1, "flag_consolidato")
//ldt_data_inizio = dw_ricerca.getitemdatetime( 1, "data_inizio")
//ldt_data_fine = dw_ricerca.getitemdatetime( 1, "data_fine")
//
//where_clause = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
//
//if not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
//	where_clause = where_clause + " and flag_blocco = 'S'"
//end if
//
//if not isnull(ls_flag_blocco) and ls_flag_blocco = "N" then
//	where_clause = where_clause + " and flag_blocco = 'N'"
//end if
//
//if not isnull(ls_flag_consolidato) and ls_flag_consolidato = "S" then
//	where_clause = where_clause + " and data_consolidato is not null"
//end if
//
//if not isnull(ls_flag_consolidato) and ls_flag_consolidato = "S" then
//	where_clause = where_clause + " and data_consolidato is not null"
//end if
//
//if not isnull(ldt_data_inizio) then
//	where_clause = where_clause + " and data_elaborazione >= '" + string( ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
//end if
//
//if not isnull(ldt_data_fine) then
//	where_clause = where_clause + " and data_elaborazione <= '" + string( ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
//end if
//
//where_clause = where_clause + " order by cod_budget ASC"
//
//ls_prova = upper(dw_tes_budget_manutenzioni_lista.GETSQLSELECT())
//
//ll_index = pos(ls_prova, "WHERE")
//if ll_index = 0 then
//	new_select = dw_tes_budget_manutenzioni_lista.GETSQLSELECT() + where_clause
//else	
//	new_select = left(dw_tes_budget_manutenzioni_lista.GETSQLSELECT(), ll_index - 1) + where_clause
//end if
//
//dw_tes_budget_manutenzioni_lista.SetSQLSelect(new_select)
//
//dw_tes_budget_manutenzioni_lista.resetupdate()
//
//ll_index = dw_tes_budget_manutenzioni_lista.retrieve(s_cs_xx.cod_azienda)
//
//if ll_index < 0 then
//   pcca.error = c_fatal
//end if
//
//dw_folder.fu_selecttab(2)
//
end event

type dw_tes_budget_manutenzioni_lista from uo_cs_xx_dw within w_tes_budget_manutenzioni
integer x = 32
integer y = 120
integer width = 4361
integer height = 1052
integer taborder = 20
string dataobject = "d_tes_budget_manutenzioni_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;//LONG  l_Error
//
//l_Error = Retrieve(s_cs_xx.cod_azienda)
//
//IF l_Error < 0 THEN
//   PCCA.Error = c_Fatal
//END IF
//

string   ls_flag_blocco, ls_flag_consolidato, where_clause, ls_prova, new_select
datetime ldt_data_inizio, ldt_data_fine
long     ll_index

dw_ricerca.accepttext()

ls_flag_blocco = dw_ricerca.getitemstring( 1, "flag_blocco")
ls_flag_consolidato = dw_ricerca.getitemstring( 1, "flag_consolidato")
ldt_data_inizio = dw_ricerca.getitemdatetime( 1, "data_inizio")
ldt_data_fine = dw_ricerca.getitemdatetime( 1, "data_fine")

where_clause = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
	where_clause = where_clause + " and flag_blocco = 'S'"
end if

if not isnull(ls_flag_blocco) and ls_flag_blocco = "N" then
	where_clause = where_clause + " and flag_blocco = 'N'"
end if

if not isnull(ls_flag_consolidato) and ls_flag_consolidato = "S" then
	where_clause = where_clause + " and data_consolidato is not null"
end if

if not isnull(ls_flag_consolidato) and ls_flag_consolidato = "N" then
	where_clause = where_clause + " and data_consolidato is null"
end if

if not isnull(ldt_data_inizio) then
	where_clause = where_clause + " and data_elaborazione >= '" + string( ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine) then
	where_clause = where_clause + " and data_elaborazione <= '" + string( ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if

where_clause = where_clause + " order by cod_budget ASC"

ls_prova = upper(dw_tes_budget_manutenzioni_lista.GETSQLSELECT())

ll_index = pos(ls_prova, "WHERE")
if ll_index = 0 then
	new_select = GETSQLSELECT() + where_clause
else	
	new_select = left(GETSQLSELECT(), ll_index - 1) + where_clause
end if

SetSQLSelect(new_select)

//dw_tes_budget_manutenzioni_lista.resetupdate()

ll_index = retrieve()//s_cs_xx.cod_azienda)

if ll_index < 0 then
   pcca.error = c_fatal
end if

if ll_index > 0 then
	if isnull(getitemdatetime( 1, "data_consolidato" )) then
		cb_revisione.text = "Revisione"
		st_prossimo_rinnovo.visible = false
		em_prossimo_rinnovo.visible = false		
	else
		cb_revisione.text = "Rinnovo"
		st_prossimo_rinnovo.visible = true
		em_prossimo_rinnovo.visible = true
	end if
end if

dw_folder.fu_selecttab(2)

end event

event pcd_setkey;call super::pcd_setkey;LONG   l_Idx, ll_progressivo, ll_anno
string ls_codice, ls_appo


FOR l_Idx = 1 TO RowCount()
	
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
	   SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
   IF IsNull(GetItemnumber(l_Idx, "progressivo")) or GetItemnumber(l_Idx, "progressivo") < 1 THEN
		
		select max(progressivo)
		into   :ll_progressivo
		from   tes_budget_manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda;
		
		if isnull(ll_progressivo) or ll_progressivo < 1 then
			ll_progressivo = 1
		else
			ll_progressivo ++
		end if
		
	   SetItem(l_Idx, "progressivo", ll_progressivo)
		
		// *** trasformo il progressivo in codice
		select numero
		into   :ll_anno
		from   parametri_azienda 
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 flag_parametro = 'N' and
		       cod_parametro = 'ESC';
				 
		if sqlca.sqlcode <> 0 or isnull(ll_anno) then
			ll_anno = year(today())
		end if
		
		select MAX(cod_budget)
		into   :ls_appo
		from   tes_budget_manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda;
		
		if isnull(ls_appo) or sqlca.sqlcode <> 0 then
			ll_progressivo = 1
		else
			if left(ls_appo,2) = right(string(ll_anno), 2) then
				ls_appo = right(ls_appo, len(ls_appo) - 3)
				ll_progressivo = long(ls_appo) + 1
			else
				ll_progressivo = 1
			end if
		end if
		
		choose case len(string(ll_progressivo))
			case 1
				ls_codice = right(string(ll_anno), 2) + "-000" + string(ll_progressivo)
			case 2
				ls_codice = right(string(ll_anno), 2) + "-00" + string(ll_progressivo)
			case 3
				ls_codice = right(string(ll_anno), 2) + "-0" + string(ll_progressivo)
			case else
				ls_codice = right(string(ll_anno), 2) + "-" + string(ll_progressivo)
		end choose
		
		SetItem(l_Idx, "cod_budget", ls_codice)
		//
		
   END IF
	
NEXT
end event

event updatestart;call super::updatestart;if i_extendmode then
	
	long ll_i, ll_progressivo
	
   for ll_i = 1 to this.deletedcount()
		ll_progressivo = this.getitemnumber(ll_i, "progressivo", delete!, true)
		
		delete det_budget_manut_periodi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :ll_progressivo;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in cancellazione dati periodi.~r~n"+sqlca.sqlerrtext)
			return 1
		end if

		delete det_budget_manut_simulate
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :ll_progressivo;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in cancellazione simulazione manutenzioni.~r~n"+sqlca.sqlerrtext)
			return 1
		end if
	next
end if
end event

event rowfocuschanged;call super::rowfocuschanged;long   ll_progressivo
string ls_cod_divisione, ls_cod_centro_costo, ls_cod_agente, ls_cod_tipo_commessa

if currentrow < 1 or isnull(currentrow) then return

ll_progressivo = getitemnumber( currentrow, "progressivo")
ls_cod_divisione = getitemstring( currentrow, "cod_divisione")

if not isnull(ls_cod_divisione) and ls_cod_divisione <> "" then
	
	select cod_agente,
	       cod_centro_costo,
			 cod_tipo_commessa
	into   :ls_cod_agente,
	       :ls_cod_centro_costo,
			 :ls_cod_tipo_commessa
	from   anag_divisioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_divisione = :ls_cod_divisione;
			 
	dw_tes_budget_manutenzioni_det_1.Object.t_tipo_commessa.text = ls_cod_tipo_commessa	
	dw_tes_budget_manutenzioni_det_1.Object.t_centro_costo.text = ls_cod_centro_costo
	dw_tes_budget_manutenzioni_det_1.Object.t_agente.text = ls_cod_agente
else
	dw_tes_budget_manutenzioni_det_1.Object.t_tipo_commessa.text = ""
	dw_tes_budget_manutenzioni_det_1.Object.t_centro_costo.text = ""
	dw_tes_budget_manutenzioni_det_1.Object.t_agente.text = ""	
end if
if isnull(getitemdatetime( currentrow, "data_consolidato" )) then
	cb_revisione.text = "Revisione"
	st_prossimo_rinnovo.visible = false
	em_prossimo_rinnovo.visible = false			
else
	cb_revisione.text = "Rinnovo"
	
//	select count(*)
//	into   :ll_prova
//	from   tes_budget_manutenzioni
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       progressivo_rinnovo = :ll_progressivo;
//			 
//	if not isnull(ll_prova) and ll_prova > 0 then
	
	st_prossimo_rinnovo.visible = true
	em_prossimo_rinnovo.visible = true
end if
end event

type dw_folder from u_folder within w_tes_budget_manutenzioni
integer width = 4434
integer height = 2320
integer taborder = 10
end type

event po_tabclicked;call super::po_tabclicked;choose case i_SelectedTab
	case 1
		dw_ricerca.visible = true
		cb_cerca.visible = true
		dw_det_budget_manut_periodi.reset()
//		iuo_dw_main = dw_ricerca
//		dw_ricerca.change_dw_current()		
	case 2
		dw_ricerca.visible = false
		cb_cerca.visible = false
		dw_det_budget_manut_periodi.reset()
		iuo_dw_main = dw_tes_budget_manutenzioni_lista
		dw_tes_budget_manutenzioni_lista.change_dw_current()
	case 4
		dw_ricerca.visible = false
		cb_cerca.visible = false		
		iuo_dw_main = dw_det_budget_manut_periodi
		dw_det_budget_manut_periodi.change_dw_current()
		
		parent.postevent("pc_retrieve")
		
	case 3
		dw_ricerca.visible = false
		cb_cerca.visible = false		
		iuo_dw_main = dw_det_budget_manut_attrezzature_totali
		dw_det_budget_manut_attrezzature_totali.change_dw_current()
		
		parent.postevent("pc_retrieve")
		
end choose
end event

type dw_det_budget_manut_periodi from uo_cs_xx_dw within w_tes_budget_manutenzioni
integer x = 37
integer y = 124
integer width = 4375
integer height = 2176
integer taborder = 30
string dataobject = "d_det_budget_manut_periodi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;long  l_idx, ll_prog_periodo, ll_progressivo


for l_idx = 1 to rowcount()
	ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "progressivo")) or getitemnumber(l_idx, "progressivo") < 1 then
	   setitem(l_idx, "progressivo", ll_progressivo)
	end if
	
   if isnull(getitemnumber(l_idx, "prog_periodo")) or getitemnumber(l_idx, "prog_periodo") < 1 then
		select max(prog_periodo)
		into   :ll_prog_periodo
		from   det_budget_manut_periodi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       progressivo = :ll_progressivo;
		
		if isnull(ll_prog_periodo) or ll_prog_periodo < 1 then
			ll_prog_periodo = 1
		else
			ll_prog_periodo ++
		end if
		
	   setitem(l_idx, "prog_periodo", ll_prog_periodo)
   end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_progressivo

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_progressivo)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

event itemchanged;call super::itemchanged;string ls_null,ls_cod_cat_attrezzatura,ls_cod_area_aziendale,ls_cod_reparto,ls_cod_divisione
long   ll_ore

choose case i_colname
	case "costo_ora_prev_risorse_int"
		ll_ore = getitemnumber(row, "ore_prev_risorse_int")
		if not isnull(ll_ore) and ll_ore > 0 then
			setitem(row, "costo_prev_risorse_int", ll_ore * dec(i_coltext))
		end if
		//*** 14/12/05 CLAUDIA aggiunta per aggiornare anche quando si modificano le ore
	case "ore_prev_risorse_int"
		ll_ore = getitemnumber(row, "costo_ora_prev_risorse_int")
		if not isnull(ll_ore) and ll_ore > 0 then
			setitem(row, "costo_prev_risorse_int", ll_ore * dec(i_coltext))
		end if
		//***fine modifica
	case "cod_attrezzatura"
		if isnull(i_coltext) then
			setitem(row, "cod_divisione", ls_null)
			setitem(row, "cod_area_aziendale", ls_null)
			setitem(row, "cod_reparto", ls_null)
			setitem(row, "cod_cat_attrezzature", ls_null)
		else
			select cod_cat_attrezzature,
			       cod_area_aziendale,
					 cod_reparto
			into   :ls_cod_cat_attrezzatura,
			       :ls_cod_area_aziendale,
					 :ls_cod_reparto
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_attrezzatura = :i_coltext;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca attrezzatura~r~n" + sqlca.sqlerrtext)
				return 1
			end if
			
			setitem(row, "cod_area_aziendale", ls_cod_area_aziendale)
			setitem(row, "cod_reparto", ls_cod_reparto)
			setitem(row, "cod_cat_attrezzature", ls_cod_cat_attrezzatura)
		
			select cod_divisione
			into   :ls_cod_divisione
			from   tab_aree_aziendali
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_area_aziendale = :ls_cod_area_aziendale;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca area aziendale~r~n" + sqlca.sqlerrtext)
				return 1
			end if
		
			setitem(row, "cod_divisione", ls_cod_divisione)
		end if
end choose
			
end event

event ue_key;call super::ue_key;if key = keyInsert! and keyflags = 1 then
	window_open_parm(w_det_budget_ins_periodi, 0, dw_tes_budget_manutenzioni_lista)
end if


end event

type dw_det_budget_manut_attrezzature_totali from uo_cs_xx_dw within w_tes_budget_manutenzioni
integer x = 37
integer y = 124
integer width = 4375
integer height = 2176
integer taborder = 60
string dataobject = "d_det_budget_manut_attrezzature_totali"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_progressivo

ll_progressivo = dw_tes_budget_manutenzioni_lista.getitemnumber(dw_tes_budget_manutenzioni_lista.getrow(),"progressivo")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_progressivo)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

type dw_ricerca from u_dw_search within w_tes_budget_manutenzioni
event ue_cod_attr ( )
integer x = 46
integer y = 140
integer width = 2994
integer height = 1020
integer taborder = 20
string dataobject = "d_tes_budget_manutenzioni_ricerca"
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"cod_attrezzatura")) then
	setitem(getrow(),"cod_attrezzatura_a",getitemstring(getrow(),"cod_attrezzatura"))
end if
end event

event itemchanged;call super::itemchanged;if getcolumnname() = "cod_attrezzatura" then
	postevent("ue_cod_attr")
end if
end event

