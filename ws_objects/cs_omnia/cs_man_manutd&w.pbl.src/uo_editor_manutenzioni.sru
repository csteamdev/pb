﻿$PBExportHeader$uo_editor_manutenzioni.sru
forward
global type uo_editor_manutenzioni from uo_editor
end type
end forward

global type uo_editor_manutenzioni from uo_editor
end type
global uo_editor_manutenzioni uo_editor_manutenzioni

type variables
private:
	int ii_cod_mappa
	int ii_cod_nodo
	uo_editor_manutenzioni_nodo iuo_collegamenti[]
	boolean ib_click_pressed = false
	boolean ib_drag = false
end variables

forward prototypes
private function string uof_build_text ()
public function boolean uof_mappa_aggiorna (integer ai_cod_mappa, string as_des_mappa)
public function boolean uof_mappa_aggiungi (string as_des_mappa)
public function boolean uof_nodo_aggiungi (integer ai_cod_mappa, ref integer ai_cod_nodo, string as_des_nodo, string as_img_path, string as_img_name, long al_width, long al_height)
public function boolean uof_nodo_rinomina (integer ai_cod_mappa, integer ai_cod_nodo, string as_des_nodo)
public function boolean uof_nodo_primo (integer ai_cod_mappa, integer ai_cod_nodo)
public function boolean uof_carica_nodo (integer ai_cod_mappa, integer ai_cod_nodo)
private function boolean uof_carica_collegamenti (integer ai_cod_mappa, integer ai_cod_nodo)
protected subroutine uof_reset ()
protected function string uof_get_labels ()
public function boolean uof_aggiungi_collegamento (integer ai_cod_mappa, integer ai_cod_nodo, integer ai_cod_collegamento, integer ai_x, integer ai_y, integer ai_width, integer ai_height, integer ai_cod_elemento, string as_tipo_elemento, string as_valore)
public function boolean uof_aggiungi_collegamento (integer ai_x, integer ai_y, integer ai_width, integer ai_height, integer ai_cod_elemento, string as_tipo_elemento, string as_valore)
public function boolean uof_nodo_elimina (integer ai_cod_mappa, integer ai_cod_nodo)
public function boolean uof_nodo_immagine (integer ai_cod_mappa, integer ai_cod_nodo, string as_image_path, string as_image_name, long al_width, long al_height)
public function boolean uof_mappa_elimina (integer ai_cod_mappa)
public function boolean uof_collegamento_elimina ()
public function integer uof_get_cod_mappa ()
public function boolean uof_get_nodo (integer ai_index, ref uo_editor_manutenzioni_nodo astr_nodo)
end prototypes

private function string uof_build_text ();string ls_modify

ls_modify = "text(band=detail alignment=~"0~" text=~"asdasd~" border=~"0~" color=~"33554432~" x=~"754~" y=~"544~" height=~"360~" width=~"1120~" html.valueishtml=~"0~"  name=t_1 visible=~"1~"  resizeable=1  moveable=1  font.face=~"Tahoma~" font.height=~"-"+"10"+"~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"2~" background.color=~"32889792~" background.transparency=~"0~" background.gradient.color=~"8421504~" background.gradient.transparency=~"0~" background.gradient.angle=~"0~" background.brushmode=~"0~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" background.gradient.repetition.length=~"100~" background.gradient.focus=~"0~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" tooltip.backcolor=~"134217752~" tooltip.delay.initial=~"0~" tooltip.delay.visible=~"32000~" tooltip.enabled=~"0~" tooltip.hasclosebutton=~"0~" tooltip.icon=~"0~" tooltip.isbubble=~"0~" tooltip.maxwidth=~"0~" tooltip.textcolor=~"134217751~" tooltip.transparency=~"0~" transparency=~"0~" )"
return ls_modify
end function

public function boolean uof_mappa_aggiorna (integer ai_cod_mappa, string as_des_mappa);update
	tab_mappe_manutenzioni
set
	des_mappa = :as_des_mappa
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa;
	
if sqlca.sqlcode = 0 then
	commit;
	return true
elseif sqlca.sqlcode = 100 then
	rollback;
	msg("Impossibile rinominare mappa con codice " + string(ai_cod_mappa))
	return false
else
	return false
end if
end function

public function boolean uof_mappa_aggiungi (string as_des_mappa);int li_cod_mappa

// -- calcolo progressivo
select max(cod_mappa)
into :li_cod_mappa
from tab_mappe_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	msg("Impossibile calcolare progressivo codice mappa.~r~n"+sqlca.sqlerrtext)
	return false
// stefanop 25/06/2010: ticket 2010/154
elseif sqlca.sqlcode = 100 or isnull(li_cod_mappa) then
	li_cod_mappa = 0
end if
li_cod_mappa++
// ----

insert into tab_mappe_manutenzioni (
	cod_azienda, 
	cod_mappa, 
	des_mappa,
	n_nodi,
	n_collegamenti,
	primo_nodo)
values
	(:s_cs_xx.cod_azienda,
	:li_cod_mappa,
	:as_des_mappa,
	0,
	0,
	0);
	
if sqlca.sqlcode = 0 then
	ii_cod_mappa = li_cod_mappa
	return true
else
	msg("Impossibile salvare nuova mappa!.~r~n"+sqlca.sqlerrtext)
	return false
end if
end function

public function boolean uof_nodo_aggiungi (integer ai_cod_mappa, ref integer ai_cod_nodo, string as_des_nodo, string as_img_path, string as_img_name, long al_width, long al_height);int li_cod_nodo, li_pixel_width, li_pixel_height, li_n_nodi
blob lblb_image

// -- calcolo progressivo
select
	max(cod_nodo)
into
	:li_cod_nodo
from
	tab_nodi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa;
	
if sqlca.sqlcode < 0 then
	msg("Impossibile calcolare progressivo nodo.~r~n"+sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(li_cod_nodo) then
	li_cod_nodo = 0
end if

li_cod_nodo++
ai_cod_nodo = li_cod_nodo
// ----

// -- aggiusto dimensione in pixel
li_pixel_width = UnitsToPixels(al_width, XUnitsToPixels!)
li_pixel_height = UnitsToPixels(al_height, YUnitsToPixels!)
// ----

insert into tab_nodi_manutenzioni (
	cod_azienda,
	cod_mappa,
	cod_nodo,
	des_nodo,
	percorso_immagine,
	percorso_orig,
	altezza,
	larghezza)
values (
	:s_cs_xx.cod_azienda,
	:ai_cod_mappa,
	:li_cod_nodo,
	:as_des_nodo,
	:as_img_name,
	:as_img_path,
	:li_pixel_height,
	:li_pixel_width);
	
if sqlca.sqlcode < 0 then
	msg("Errore durante il salvataggio nodo.~r~n"+sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 0 then
	if f_file_to_blob(as_img_path, lblb_image) = 0 then
		
		updateblob tab_nodi_manutenzioni
		set blob = :lblb_image
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_mappa = :ai_cod_mappa and
			cod_nodo = :li_cod_nodo;
			
		if sqlca.sqlcode <> 0 then
			rollback;
			msg("Errore durante il salvataggio immagine mappa.~r~n"+sqlca.sqlerrtext)
			return false
		end if
		
		commit;
		
		// -- Aggiorno contattori mappa
		select count(cod_nodo)
		into	 :li_n_nodi
		from	 tab_nodi_manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_mappa = :ai_cod_mappa;
		
		if sqlca.sqlcode = 0 then
			update tab_mappe_manutenzioni
			set	  n_nodi = :li_n_nodi
			where  cod_azienda=:s_cs_xx.cod_azienda and
					  cod_mappa = :ai_cod_mappa;
					  
			if sqlca.sqlcode <> 0 then
				msg("Errore in aggiornamento numero nodi mappa.~r~n"+sqlca.sqlerrtext)
			else
				commit;
			end if
		end if
		// ----
		return true
	else
		rollback;
		msg("Errore, non è stato possibile caricare l'immagine della mappa")
		return false
	end if
end if
	
end function

public function boolean uof_nodo_rinomina (integer ai_cod_mappa, integer ai_cod_nodo, string as_des_nodo);update
	tab_nodi_manutenzioni
set
	des_nodo = :as_des_nodo
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa and
	cod_nodo = :ai_cod_nodo;
	
if sqlca.sqlcode = 0 then
	commit;
	return true
elseif sqlca.sqlcode = 100 then
	rollback;
	msg("Impossibile rinominare nodo con codice " + string(ai_cod_mappa))
	return false
else
	return false
end if
end function

public function boolean uof_nodo_primo (integer ai_cod_mappa, integer ai_cod_nodo);update
	tab_mappe_manutenzioni
set
	primo_nodo = :ai_cod_nodo
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa;
	
if sqlca.sqlcode < 0 then
	msg("Impossibile assegnare primo nodo.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
elseif sqlca.sqlcode = 100 then
	rollback;
	msg("Impossibile trovare mappa per assegnare il primo nodo.~r~n" + sqlca.sqlerrtext)
	return false
else
	commit;
	return true
end if
end function

public function boolean uof_carica_nodo (integer ai_cod_mappa, integer ai_cod_nodo);string ls_des_nodo, ls_percorso_immagine, ls_percorso_orig
int		li_altezza, li_larghezza
blob 	lblb_mappa

uof_reset()

// -- Blob
selectblob blob
into :lblb_mappa
from tab_nodi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa and
	cod_nodo = :ai_cod_nodo;
	
if sqlca.sqlcode <> 0 or isnull(lblb_mappa) then
	msg("Nessuna immagine è associata a questo nodo.~r~n" + sqlca.sqlerrtext)
	return false
end if

// -- Valori
select des_nodo, percorso_immagine, altezza, larghezza, percorso_orig
into :ls_des_nodo, :ls_percorso_immagine, :li_altezza, :li_larghezza, :ls_percorso_orig
from tab_nodi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa and
	cod_nodo = :ai_cod_nodo;
	
if sqlca.sqlcode <> 0 then
	msg("Impossibile recuperare informazioni nodo.~r~n" + sqlca.sqlerrtext)
	return false
end if

// -- Dw
is_image_path = is_user_temporaly + ls_percorso_immagine
if f_blob_to_file(is_image_path, lblb_mappa) <> 0 then
	msg("Impossibile salvare l'immagine nella cartella temporanea, probabilmente non esiste la cartella temporanea.~r~n" + is_image_path)
	return false
end if

// -- Dimensioni Immagine
il_image_width = PixelsToUnits(li_larghezza, XPixelsToUnits!)
il_image_height = PixelsToUnits(li_altezza, YPixelsToUnits!)
il_image_pixel_width = li_larghezza
il_image_pixel_height = li_altezza
il_image_width_original = il_image_width
il_image_height_original = il_image_height
this.uof_resize()
// ----

uof_carica_collegamenti(ai_cod_mappa, ai_cod_nodo)

uof_fix_tilde(is_image_path)
uof_draw()

return true
end function

private function boolean uof_carica_collegamenti (integer ai_cod_mappa, integer ai_cod_nodo);string ls_sql, ls_tipo, ls_valore
int		li_cod_elemento
long	ll_rows, ll_row, x1, x2, y1, y2
datastore lds_store

ls_sql = "SELECT * FROM tab_collegamenti_manutenzioni WHERE "
ls_sql += "cod_azienda='" + s_cs_xx.cod_azienda + "' AND "
ls_sql += "cod_mappa=" + string(ai_cod_mappa) + " AND "
ls_sql += "cod_nodo=" + string(ai_cod_nodo)

if not f_crea_datastore(lds_store, ls_sql) then
	msg("Impossibile creare datastore per controllare i collegamenti associati alla mappa")
	return false
end if

ii_cod_mappa = ai_cod_mappa
ii_cod_nodo = ai_cod_nodo

ll_rows = lds_store.retrieve()
if ll_rows > 0 then
	for ll_row = 1 to ll_rows
		
		li_cod_elemento = lds_store.getitemnumber(ll_row, "cod_elemento")
		ls_tipo = lds_store.getitemstring(ll_row, "tipo_elemento")
		ls_valore = lds_store.getitemstring(ll_row, "valore")
		x1 = PixelsToUnits(lds_store.getitemnumber(ll_row, "x1") *  il_image_pixel_width, XPixelsToUnits!)
		y1 = PixelsToUnits(lds_store.getitemnumber(ll_row, "y1") *  il_image_pixel_height, YPixelsToUnits!)
	
		x2 = PixelsToUnits(lds_store.getitemnumber(ll_row, "x2") *  il_image_pixel_width, XPixelsToUnits!)
		y2 = PixelsToUnits(lds_store.getitemnumber(ll_row, "y2") *  il_image_pixel_height, YPixelsToUnits!)
		
		uof_aggiungi_collegamento(ai_cod_mappa, ai_cod_nodo, lds_store.getitemnumber(ll_row, "cod_collegamento"), x1,y1, x2 - x1, y2 - y1,li_cod_elemento, ls_tipo, ls_valore)
	next
end if
end function

protected subroutine uof_reset ();ii_cod_mappa = 0
ii_cod_nodo = 0

// -- Reset array collegamenti
uo_editor_manutenzioni_nodo luo_empty[]
iuo_collegamenti = luo_empty

end subroutine

protected function string uof_get_labels ();string ls_modify, ls_mode, ls_text, ls_opacity
int		li_i

for li_i = 1 to upperbound(this.iuo_collegamenti)
	if isvalid(this.iuo_collegamenti[li_i]) then
		
		if ib_editor then
			ls_mode = "1"
			ls_opacity = "50"
			ls_text = upper(this.iuo_collegamenti[li_i].tipo_elemento) +": " + this.iuo_collegamenti[li_i].valore
		else
			ls_mode = "0"
			
			ls_opacity = "50"
			//ls_opacity = "100"
			
			ls_text = upper(this.iuo_collegamenti[li_i].tipo_elemento) +": " + this.iuo_collegamenti[li_i].valore
			//ls_text = ""
		end if

		ls_modify += "text(band=detail alignment=~"0~" border=~"0~" html.valueishtml=~"0~" visible=~"1~" font.face=~"Tahoma~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"2~" background.color=~"32889792~" background.gradient.color=~"8421504~" background.gradient.transparency=~"0~" background.gradient.angle=~"0~" background.brushmode=~"0~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" background.gradient.repetition.length=~"100~" background.gradient.focus=~"0~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" tooltip.backcolor=~"134217752~" tooltip.delay.initial=~"0~" tooltip.delay.visible=~"32000~" tooltip.enabled=~"0~" tooltip.hasclosebutton=~"0~" tooltip.icon=~"0~" tooltip.isbubble=~"0~" tooltip.maxwidth=~"0~" tooltip.textcolor=~"134217751~" tooltip.transparency=~"0~" transparency=~"0~" "
		ls_modify += "text=~"" + ls_text +  "~" "
		ls_modify += "color=~"33554432~" "
		ls_modify += "x=~"" + string(this.iuo_collegamenti[li_i].x * id_zoom_scale) + "~" "
		ls_modify += "y=~"" + string(this.iuo_collegamenti[li_i].y * id_zoom_scale)+ "~" "
		ls_modify += "height=~"" + string(this.iuo_collegamenti[li_i].height * id_zoom_scale) + "~" "
		ls_modify += "width=~"" + string(this.iuo_collegamenti[li_i].width * id_zoom_scale) + "~" "
		ls_modify += "name=t_" + string(li_i) +" "
		ls_modify += "font.height=~"-10~" "
		ls_modify += "background.transparency=~"" + ls_opacity + "~""
		ls_modify += "resizeable=" + ls_mode + " moveable=" + ls_mode + " "
		if not ib_editor then ls_modify += "pointer=~"HyperLink!~""
		ls_modify += " ) "
		
	end if
next

return ls_modify
end function

public function boolean uof_aggiungi_collegamento (integer ai_cod_mappa, integer ai_cod_nodo, integer ai_cod_collegamento, integer ai_x, integer ai_y, integer ai_width, integer ai_height, integer ai_cod_elemento, string as_tipo_elemento, string as_valore);string ls_error
int li_index

li_index = upperbound(iuo_collegamenti) + 1

this.iuo_collegamenti[li_index] = create uo_editor_manutenzioni_nodo
this.iuo_collegamenti[li_index].cod_mappa = ai_cod_mappa
this.iuo_collegamenti[li_index].cod_nodo = ai_cod_nodo
this.iuo_collegamenti[li_index].cod_collegamento = ai_cod_collegamento
this.iuo_collegamenti[li_index].x = ai_x
this.iuo_collegamenti[li_index].y = ai_y 
this.iuo_collegamenti[li_index].width = ai_width
this.iuo_collegamenti[li_index].height = ai_height
this.iuo_collegamenti[li_index].cod_elemento = ai_cod_elemento
this.iuo_collegamenti[li_index].tipo_elemento = as_tipo_elemento
this.iuo_collegamenti[li_index].valore = as_valore

if ai_cod_collegamento = -1 then
	// Salvo
	ls_error = this.iuo_collegamenti[li_index].Save(il_image_pixel_width, il_image_pixel_height)
	if ls_error <> "" then
		msg(ls_error)
		return false
	end if
end if

return true
end function

public function boolean uof_aggiungi_collegamento (integer ai_x, integer ai_y, integer ai_width, integer ai_height, integer ai_cod_elemento, string as_tipo_elemento, string as_valore);int li_index

uof_aggiungi_collegamento(ii_cod_mappa, ii_cod_nodo, -1, ai_x, ai_y, ai_width, ai_height, ai_cod_elemento, as_tipo_elemento, as_valore)

return true
end function

public function boolean uof_nodo_elimina (integer ai_cod_mappa, integer ai_cod_nodo);// -- Elimino prima i figli
delete from tab_collegamenti_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa and
	cod_nodo = :ai_cod_nodo;
	
if sqlca.sqlcode < 0 then
	rollback;
	msg("Impossibile eliminare collegamenti associati alla mappa.~r~n" + sqlca.sqlerrtext)
	return false;
end if

// -- Elimino nodo
delete from tab_nodi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa and
	cod_nodo = :ai_cod_nodo;
	
if sqlca.sqlcode < 0 then
	rollback;
	msg("Impossibile eliminare nodo.~r~n" + sqlca.sqlerrtext)
	return false
end if

// -- Tutto è stato eliminato
return true
end function

public function boolean uof_nodo_immagine (integer ai_cod_mappa, integer ai_cod_nodo, string as_image_path, string as_image_name, long al_width, long al_height);int li_pixel_width, li_pixel_height
blob lblb_image

// -- aggiusto dimensione in pixel
li_pixel_width = UnitsToPixels(al_width, XUnitsToPixels!)
li_pixel_height = UnitsToPixels(al_height, YUnitsToPixels!)
// ----

if f_file_to_blob(as_image_path, lblb_image) = 0 then
	
	updateblob tab_nodi_manutenzioni
	set blob = :lblb_image
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_mappa = :ai_cod_mappa and
		cod_nodo = :ai_cod_nodo;
		
	if sqlca.sqlcode = 0 then
		
		update tab_nodi_manutenzioni
		set
			larghezza = :li_pixel_width,
			altezza = :li_pixel_height,
			percorso_immagine = :as_image_name,
			percorso_orig = :as_image_path
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_mappa = :ai_cod_mappa and
			cod_nodo = :ai_cod_nodo;
			
		if sqlca.sqlcode = 0 then
			is_image_path = as_image_path
			il_image_width = al_width
			il_image_height = al_height
			il_image_pixel_width = li_pixel_width
			il_image_pixel_height = li_pixel_height
			commit;
			this.uof_resize()
			return true
		else
			msg("Impossibile salvare dimensioni immagine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return false
		end if
	else
		msg("Impossibile salvare l'immagine all'intero del database.~r~n"+ sqlca.sqlerrtext)
		rollback;
		return false
	end if

else
	msg("Impossibile convertire immagine per il salvataggio.")
	return false
end if
end function

public function boolean uof_mappa_elimina (integer ai_cod_mappa);long ll_nodes, ll_node
datastore lds_store

if not f_crea_datastore(lds_store, "SELECT cod_nodo FROM tab_nodi_manutenzioni WHERE cod_azienda='"+s_cs_xx.cod_azienda+"' AND cod_mappa="+string(ai_cod_mappa)) then
	msg("Impossibile creare datastore temporaneo per la cancellazione")
	return false
end if

ll_nodes = lds_store.retrieve()
if ll_nodes > 0 then
	for ll_node = 1 to ll_nodes
		if not this.uof_nodo_elimina(ai_cod_mappa, lds_store.getitemnumber(ll_node, 1)) then
			return false
		end if
	next
end if

delete from tab_mappe_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :ai_cod_mappa;
	
if sqlca.sqlcode = 0 then
	commit;
	return true
else
	msg("Impossibile eleminare mappa.~r~n"+sqlca.sqlerrtext)
	rollback;
	return false
end if
end function

public function boolean uof_collegamento_elimina ();if il_t_active > 0 then
	
	if this.iuo_collegamenti[il_t_active].Remove() then
		destroy(this.iuo_collegamenti[il_t_active])
	
		is_t_active = ""
		il_t_active = -1
		
		return true
	end if

end if

return false
end function

public function integer uof_get_cod_mappa ();return this.ii_cod_mappa
end function

public function boolean uof_get_nodo (integer ai_index, ref uo_editor_manutenzioni_nodo astr_nodo);if ai_index <= upperbound(this.iuo_collegamenti) then
	if isvalid(this.iuo_collegamenti[ai_index]) then
		astr_nodo = this.iuo_collegamenti[ai_index]
		return true
	else
		return false
	end if
else
	return false
end if
end function

on uo_editor_manutenzioni.create
call super::create
end on

on uo_editor_manutenzioni.destroy
call super::destroy
end on

event constructor;call super::constructor;this.uof_set_directory("editor_manutenzioni")
end event

event uoe_dragend;call super::uoe_dragend;if left(as_name, 2) <> "t_" then
	return	
end if

string ls_error
int li_index
boolean ib_force_redraw = false
li_index = integer(mid(as_name, 3))

// controllo dimensioni minime
if ai_x < 0 then 
	ai_x = 0
	ib_force_redraw = true
end if
if ai_y < 0 then 
	ai_y = 0
	ib_force_redraw = true
end if
if ai_width < 10 then 
	ai_width = 30
	ib_force_redraw = true
end if
if ai_height < 10 then 
	ai_height = 30
	ib_force_redraw = true
end if
if ai_x + ai_width > il_image_width then 
	ai_x = il_image_width - ai_width
	ib_force_redraw = true
end if
if ai_y + ai_height > il_image_height then 
	ai_y = il_image_height - ai_height
	ib_force_redraw = true
end if
// ----

if isvalid(this.iuo_collegamenti[li_index]) then
	this.iuo_collegamenti[li_index].x = ai_x
	this.iuo_collegamenti[li_index].y = ai_y
	this.iuo_collegamenti[li_index].width = ai_width
	this.iuo_collegamenti[li_index].height = ai_height
	
	ls_error = this.iuo_collegamenti[li_index].save(il_image_pixel_width, il_image_pixel_height)
	if ls_error <> "" then
		msg(ls_error)
	elseif ib_force_redraw then
		this.uof_draw()
	end if
end if
end event

