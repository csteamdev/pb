﻿$PBExportHeader$uo_editor_manutenzioni_nodo.sru
forward
global type uo_editor_manutenzioni_nodo from nonvisualobject
end type
end forward

global type uo_editor_manutenzioni_nodo from nonvisualobject
end type
global uo_editor_manutenzioni_nodo uo_editor_manutenzioni_nodo

type variables
public:
	int cod_mappa
	int cod_nodo
	int cod_collegamento
	
	int x
	int y
	int width
	int height
	
	int cod_elemento
	string tipo_elemento
	string valore	
	
	boolean modify
	
private:
	dec{4} x1, x2
	dec{4} y1, y2
	int ii_pixel_width
	int ii_pixel_height
end variables

forward prototypes
public function boolean remove ()
public subroutine dimension (integer ai_pixel_width, integer ai_pixel_height)
public function string save (integer ai_pixel_width, integer ai_pixel_height)
end prototypes

public function boolean remove ();if this.cod_collegamento > 0 then
	
	delete from tab_collegamenti_manutenzioni
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_mappa = :this.cod_mappa and
		cod_nodo = :this.cod_nodo and
		cod_collegamento = :this.cod_collegamento;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Editor Manutenzioni", "Impossibile eliminare collegamento.~r~n" + sqlca.sqlerrtext)
		rollback;
		return false
	elseif sqlca.sqlcode = 0 then
		commit;
		return true
	else
		return false
	end if
	
end if

return false
end function

public subroutine dimension (integer ai_pixel_width, integer ai_pixel_height);ii_pixel_width = ai_pixel_width
ii_pixel_height = ai_pixel_height
end subroutine

public function string save (integer ai_pixel_width, integer ai_pixel_height);string ls_error
int		li_cod_collegamento

// -- Coordinale
this.x1 = round( UnitsToPixels(this.x, XUnitsToPixels!) / ai_pixel_width, 4)
this.y1 = round( UnitsToPixels(this.y, YUnitsToPixels!) / ai_pixel_height, 4)
this.x2 = round( UnitsToPixels(this.x + this.width, XUnitsToPixels!) / ai_pixel_width, 4)
this.y2 = round( UnitsToPixels(this.y + this.height, YUnitsToPixels!) / ai_pixel_height, 4)

// -- Salvataggio
if this.cod_collegamento = -1 then
	
	// -- max + 1
	select max(cod_collegamento)
	into	:li_cod_collegamento
	from	tab_collegamenti_manutenzioni
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_mappa = :this.cod_mappa and
		cod_nodo = :this.cod_nodo;
	
	if sqlca.sqlcode < 0 then
		return "Impossibile calcolare progressivo coleggamento.~r~n" + sqlca.sqlerrtext
	elseif sqlca.sqlcode = 100 or isnull(li_cod_collegamento) then
		li_cod_collegamento = 0
	end if
	
	li_cod_collegamento++
	this.cod_collegamento = li_cod_collegamento
	// ----
	
	insert into tab_collegamenti_manutenzioni (
		cod_azienda,
		cod_mappa,
		cod_nodo,
		cod_collegamento,
		x1, x2, y1, y2,
		cod_elemento,
		tipo_elemento,
		valore)
	values (
		:s_cs_xx.cod_azienda,
		:this.cod_mappa,
		:this.cod_nodo,
		:this.cod_collegamento,
		:this.x1, :this.x2,
		:this.y1, :this.y2,
		:this.cod_elemento,
		:this.tipo_elemento,
		:this.valore);
	
	if sqlca.sqlcode <> 0 then
		rollback;
		return sqlca.sqlerrtext
	else
		commit;
		return ""
	end if
	
else
	
	//if modify then
		
		update tab_collegamenti_manutenzioni
		set 
			x1 = :this.x1, x2 = :this.x2,
			y1 = :this.y1, y2 = :this.y2,
			cod_elemento = :this.cod_elemento,
			tipo_elemento = :this.tipo_elemento,
			valore = :this.valore
		where
			cod_azienda=:s_cs_xx.cod_azienda and
			cod_mappa = :this.cod_mappa and
			cod_nodo = :this.cod_nodo and
			cod_collegamento = :this.cod_collegamento;
			
		if sqlca.sqlcode <0 then
			ls_error = "Errore durante l'aggiornamento del collegamento.~r~n" + sqlca.sqlerrtext
			rollback;
			return ls_error
		elseif sqlca.sqlcode = 100 then
			rollback;
			ls_error = "Impossibile trovare collegamento con codice " + string(this.cod_collegamento)
		else
			commit;
			return ""
		end if
		
	//end if
end if

return ""
end function

on uo_editor_manutenzioni_nodo.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_editor_manutenzioni_nodo.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

