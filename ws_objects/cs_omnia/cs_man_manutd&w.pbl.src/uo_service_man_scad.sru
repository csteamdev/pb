﻿$PBExportHeader$uo_service_man_scad.sru
forward
global type uo_service_man_scad from uo_service_base
end type
end forward

global type uo_service_man_scad from uo_service_base
end type
global uo_service_man_scad uo_service_man_scad

type variables
private:
		long			il_gg_in_scadenza = 0
		string			is_from, is_sender_name, is_smtp_server, is_smtp_usd, is_smtp_pwd
		string			is_br = "<br>"
		boolean		ib_test = false, ib_notifica_senza_ld = false
		string			is_mail_test =  "donato.cassano@csteam.com" //"assistenza@csteam.com"
		string			is_soggetto_scadute = "Invio manutenzioni ordinarie che in OMNIA risultano non ancora eseguite (SCADUTE) alla data "
		string			is_soggetto_in_scadenza = "Invio manutenzioni ordinarie IN SCADENZA in OMNIA nel periodo dal "
end variables

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
public function integer uof_get_liste_dist (ref datetime adt_oggi, ref datetime adt_in_scadenza, ref datastore ads_liste_dist_scadute, ref datastore ads_liste_dist_in_scadenza, ref boolean ab_scadute, ref boolean ab_in_scadenza, ref string as_errore)
public function integer uof_manutenzioni_scadute (datetime adt_data, string as_cod_tipo_lista_dist, string as_cod_lista_dist, ref string as_messaggio, ref string as_errore)
public function integer uof_manutenzioni_in_scadenza (datetime adt_data, datetime adt_data_in_scadenza, string as_cod_tipo_lista_dist, string as_cod_lista_dist, ref string as_messaggio, ref string as_errore)
public function integer uof_mail (string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_subject, string as_message, ref string as_error)
public function integer uof_mail (string as_to[], string as_subject, string as_message, ref string as_error)
public function integer uof_check_senza_lista_dist (integer ai_tipo, string as_sql)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);


//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=man_scad,<codutente>,<gg>,<codazienda>,<verbosità>
//																			dove
//																					1.<codutente>					: utente da cui legge le impostazioni per invio mail (smtp, user, pwd, ecc...)
//																					2.<gg>								: giorni prima della scadenza da cui iniziare ad avvisare della imminente scadenza
//																					3.<codazienda>					: valore per inizializzare s_cs_xx.cod_azienda
//																					4.<notifica se senza LD S/N>	: normalmente a N (se S manda email a is_from con tipologie senza liste distribuzione)
//																					5.<test S/N>						: normalmente a N (se S le email vanno tutte ad assistenza@csteam.com)
//																					6.<verbosità> 					: numero che indica la verbosità del log (min 1, max 5, default 2)
//esempio 		cs_team.exe S=man_scad,001,5,A01,N,N,3
//esempio 		cs_team.exe S=man_scad,001,5,A01,N,S,3
//					log scritto nella cartella logs delle risorse

/*

MAIL ORDINARIE SCADUTE
da ieri a ritroso

MAIL ORDINARIE IN SCADENZA
da oggi fino ai prossimi N giorni in avanti
(N è il parametro n°2 del comando)

*/

//##########################################################################################

//ib_notifica_senza_ld


datastore				lds_liste_dist_scadute, lds_liste_dist_in_scadenza
boolean					lb_scadute, lb_in_scadenza
string						ls_errore, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_soggetto, ls_testo, ls_test
integer					li_ret, li_param
long						ll_index, ll_count
datetime					ldt_oggi, ldt_in_scadenza


iuo_log.set_date_time_format("dd/mm/yyyy hh:mm:ss")
iuo_log.warn("####### INIZIO PROCESSO CONTROLLO MANUTENZIONI ORDINARIE SCADUTE E IN SCADENZA  ##########################")


//-----------------------------------------------------------------------------------------------------------------------------------------------------
li_param = 0

//1) imposto l'utente
li_param += 1
uof_set_cod_utente(auo_params.uof_get_string(li_param,"001"))

//2) imposto i giorni prima della scadenza da cui iniziare ad avvisare della imminente scadenza
li_param += 1
il_gg_in_scadenza = auo_params.uof_get_long(li_param, 5)
if isnull(il_gg_in_scadenza) or il_gg_in_scadenza < 0 then il_gg_in_scadenza = 0

//3) imposto l'azienda
li_param += 1
uof_set_s_cs_xx(auo_params.uof_get_string(li_param,"A01"))

//4) vedo se devo notificare all'amministratore tipi manutenzione senza lista distribuzione
li_param += 1
ls_test = auo_params.uof_get_string(li_param,"N")
if ls_test="S" then ib_notifica_senza_ld=true

//5) vedo se sono in modalità test
li_param += 1
ls_test = auo_params.uof_get_string(li_param,"N")
if ls_test="S" then ib_test=true

//6) imposto il livello di logging (1..5 se non passo niente metto 2)
li_param += 1
iuo_log.set_log_level(auo_params.uof_get_int(li_param, 2))


iuo_log.info("Letti parametri input")
iuo_log.info("Utente: "+s_cs_xx.cod_utente + "   GG in scadenza: "+string(il_gg_in_scadenza) + "  Azienda: "+s_cs_xx.cod_azienda + "  Test Mode: " + ls_test)


//controllo che l'utente sia esistente e che abbia valori che servono impostati
if s_cs_xx.cod_utente = "" or isnull(s_cs_xx.cod_utente) then
	iuo_log.error("Codice Utente non impostato nei parametri del processo! PROCESSO TERMINATO")
	return -1
end if

//recupero info principali per inviare email (sender, smtp, user_smtp, pwd_smtp)
SELECT		e_mail,
				sender_name,
				smtp_server,
				smtp_usr,
				smtp_pwd
into		:is_from,
			:is_sender_name,
			:is_smtp_server,
			:is_smtp_usd,
			:is_smtp_pwd
from utenti
where cod_utente=:s_cs_xx.cod_utente;

if sqlca.sqlcode < 0 then
	//errore
	iuo_log.error("Errore in lettura valori SMTP in tabella utenti: "+sqlca.sqlerrtext + " PROCESSO TERMINATO")
	return -1
	
elseif sqlca.sqlcode = 100 then
	//utente non trovato in tabella
	iuo_log.error("Utente con codice '"+s_cs_xx.cod_utente+"' non trovato in tabella utenti!  PROCESSO TERMINATO")
	return -1
end if

if isnull(is_from) or is_from="" then
	iuo_log.error("E-mail non specificata per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return -1
end if

if isnull(is_smtp_server) or is_smtp_server="" then
	iuo_log.error("Indirizzo server SMTP non specificato per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return -1
end if

if isnull(is_sender_name) then is_sender_name = "<Sender>"
if isnull(is_smtp_usd) then is_smtp_usd = ""
if isnull(is_smtp_pwd) then is_smtp_pwd = ""

iuo_log.info("Letti valori SMTP per l'utente '"+s_cs_xx.cod_utente+"'")
iuo_log.info("E-mail mittente: "+is_from + "   Sender mittente: "+is_sender_name + "  Server SMTP: "+is_smtp_server + "  Utente SMTP: "+is_smtp_usd+ "  Password SMTP: "+is_smtp_pwd)
//------------------------------------------------------------------------------------


//recupero le liste di distribuzione per gli alert delle man. ordinarie scadute e in scadenza
lb_scadute= false
lb_in_scadenza = false
li_ret = uof_get_liste_dist(ldt_oggi, ldt_in_scadenza, lds_liste_dist_scadute, lds_liste_dist_in_scadenza, lb_scadute, lb_in_scadenza, ls_errore)

if li_ret < 0 then
	iuo_log.error(ls_errore + " PROCESSO TERMINATO")
	return -1
end if

//faccio una e-mail per ogni lista di distribuzione per le manutenzioni ordinarie scadute
//				e una e-mail per ogni lista di distribuzione per le manutenzioni ordinarie in scadenza)
if lb_scadute then
	
	ls_soggetto = is_soggetto_scadute + string(ldt_oggi, "dd-mm-yyyy")
	
	for ll_index=1 to lds_liste_dist_scadute.rowcount()
		ls_cod_tipo_lista_dist = lds_liste_dist_scadute.getitemstring(ll_index, 1)
		ls_cod_lista_dist = lds_liste_dist_scadute.getitemstring(ll_index, 2)
		
		//costruisco testo email per le manutenzioni ordinarie scadute
		ls_testo = ""
		li_ret = uof_manutenzioni_scadute(ldt_oggi, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_testo, ls_errore)
		
		if li_ret<0 then
			destroy lds_liste_dist_scadute
			destroy lds_liste_dist_in_scadenza
			iuo_log.error("MAN.ORD.SCADUTE: " + ls_errore + " PROCESSO TERMINATO")
			return -1
			
		elseif li_ret=1 then
			//no dati da processare, già segnalato nel log come warning!. Fuori dal ciclo!
			exit
		end if
		
		//procedi pure con l'invio mail
		li_ret = uof_mail(ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_soggetto, ls_testo, ls_errore)
		if li_ret<0 then
			destroy lds_liste_dist_scadute
			destroy lds_liste_dist_in_scadenza
			iuo_log.error("sendmail error MAN.ORD.SCADUTE: " + ls_errore + " PROCESSO TERMINATO")
			return -1
			
		elseif li_ret=1 then
			//no destinatari con indirizzo mail valido: salta la lista di distribuzione. Già segnalato nel log come warning!
			continue
		end if
		
		iuo_log.warn("E-mails MAN.ORD.SCADUTE inviate alla lista di distribuzione Tipo/Codice "+ls_cod_tipo_lista_dist+"/"+ls_cod_lista_dist)
	next
end if

destroy lds_liste_dist_scadute

if lb_in_scadenza then
	
	ls_soggetto = is_soggetto_in_scadenza + string(ldt_oggi, "dd-mm-yyyy") + " al " + string(ldt_in_scadenza, "dd-mm-yyyy")
	
	for ll_index=1 to lds_liste_dist_in_scadenza.rowcount()
		ls_cod_tipo_lista_dist = lds_liste_dist_in_scadenza.getitemstring(ll_index, 1)
		ls_cod_lista_dist = lds_liste_dist_in_scadenza.getitemstring(ll_index, 2)
	
		//costruisco testo email per le manutenzioni ordinarie in scadenza
		ls_testo = ""
		li_ret = uof_manutenzioni_in_scadenza(ldt_oggi, ldt_in_scadenza, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_testo, ls_errore)
		
		if li_ret<0 then
			destroy lds_liste_dist_in_scadenza
			iuo_log.error("MAN.ORD.IN SCADENZA: " + ls_errore + " PROCESSO TERMINATO")
			return -1
			
		elseif li_ret=1 then
			//no dati da processare, già segnalato nel log come warning!. Fuori dal ciclo!
			exit
		end if
		
		//procedi pure con l'invio mail
		li_ret = uof_mail(ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_soggetto, ls_testo, ls_errore)
		if li_ret<0 then
			destroy lds_liste_dist_in_scadenza
			iuo_log.error("sendmail error MAN.ORD.IN SCADENZA: " + ls_errore + " PROCESSO TERMINATO")
			return -1
			
		elseif li_ret=1 then
			//no destinatari con indirizzo mail valido: salta la lista di distribuzione. Già segnalato nel log come warning!
			continue
		end if
		
		iuo_log.warn("E-mails MAN.ORD.IN SCADENZA inviate alla lista di distribuzione Tipo/Codice "+ls_cod_tipo_lista_dist+"/"+ls_cod_lista_dist)
	
	next
end if

destroy lds_liste_dist_in_scadenza

iuo_log.warn("####### PROCESSO CONTROLLO MANUTENZIONI ORDINARIE SCADUTE E IN SCADENZA TERMINATO!  ##########################")


return 0
end function

public function integer uof_get_liste_dist (ref datetime adt_oggi, ref datetime adt_in_scadenza, ref datastore ads_liste_dist_scadute, ref datastore ads_liste_dist_in_scadenza, ref boolean ab_scadute, ref boolean ab_in_scadenza, ref string as_errore);string			ls_from, ls_sql, ls_sql_senza_lista_dist, ls_temp
long			ll_ret


adt_oggi = datetime(today(), 00:00:00)

ls_from		=	"from manutenzioni as m "+&
						"join tab_tipi_manutenzione as tm on tm.cod_azienda=m.cod_azienda and "+&
															"tm.cod_attrezzatura=m.cod_attrezzatura and "+&
															"tm.cod_tipo_manutenzione=m.cod_tipo_manutenzione "+&
						"where	m.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"m.flag_ordinario='S' and "+&
								"m.flag_eseguito='N' "


ls_sql_senza_lista_dist = "select distinct tm.cod_attrezzatura, tm.cod_tipo_manutenzione, tm.des_tipo_manutenzione "+&
								ls_from + &
								" and (tm.cod_tipo_lista_dist is null or tm.cod_lista_dist is  null) "

//-----------------------------------------------------------------------------
//logga le manutenzioni scadute (eventuali) senza lista distribuzione, per avvisare che non saranno notificate ...
ls_temp = ls_sql_senza_lista_dist + " and m.data_registrazione < '"+string(adt_oggi, s_cs_xx.db_funzioni.formato_data)+"'"
uof_check_senza_lista_dist(1, ls_temp)
//-----------------------------------------------------------------------------


//liste di distribuzione per le man. ordinarie scadute ######################################################
ls_sql = "select distinct tm.cod_tipo_lista_dist, tm.cod_lista_dist " + &
			ls_from + &
					" and tm.cod_tipo_lista_dist is not null and tm.cod_lista_dist is not null " + &
					" and m.data_registrazione < '"+string(adt_oggi, s_cs_xx.db_funzioni.formato_data)+"'"

ll_ret = guo_functions.uof_crea_datastore(ads_liste_dist_scadute, ls_sql, as_errore)
if ll_ret < 0 then
	//errore
	return -1
	
elseif ll_ret = 0 then
	//non ci sono manutenzioni scadute oppure nessuna lista distribuzione è stata assegnata, lo metto come warning
	iuo_log.warn("Non ci sono manutenzioni scadute alla data " + string(adt_oggi, "dd/mm/yyyy") + &
						" oppure nessuna lista distribuzione è stata assegnata alle rispettive manutenzioni che risultano scadute!")
	ab_scadute = false
else
	ab_scadute = true
end if
//#######################################################################################


if il_gg_in_scadenza > 0 then
	
	adt_in_scadenza = datetime(relativedate(date(adt_oggi), il_gg_in_scadenza), 00:00:00)
	
	//-----------------------------------------------------------------------------
	//logga le manutenzioni in scadenza (eventuali) senza lista distribuzione, per avvisare che non saranno notificate ...
	
	ls_temp = ls_sql_senza_lista_dist +	&
							"m.data_registrazione >= '"+string(adt_oggi, s_cs_xx.db_funzioni.formato_data)+"' and " +&
							"m.data_registrazione <= '"+string(adt_in_scadenza, s_cs_xx.db_funzioni.formato_data)+"' "
	uof_check_senza_lista_dist(2, ls_temp)
	//-----------------------------------------------------------------------------
	
	
	//liste di distribuzione per le man. ordinarie in scadenza ######################################################
	ls_sql = "select distinct tm.cod_tipo_lista_dist, tm.cod_lista_dist " + &
				ls_from + &
						"and tm.cod_tipo_lista_dist is not null and tm.cod_lista_dist is not null and " + &
						"m.data_registrazione >= '"+string(adt_oggi, s_cs_xx.db_funzioni.formato_data)+"' and " +&
						"m.data_registrazione <= '"+string(adt_in_scadenza, s_cs_xx.db_funzioni.formato_data)+"' "
	
	ll_ret = guo_functions.uof_crea_datastore(ads_liste_dist_in_scadenza, ls_sql, as_errore)
	if ll_ret < 0 then
		//errore
		return -1
	elseif ll_ret = 0 then
		//non ci sono manutenzioni in scadenza oppure nessuna lista distribuzione è stata assegnata alle rispettive manutenzioni che risultano in scadenza,  lo metto come warning
		iuo_log.warn("Non ci sono manutenzioni in scadenza nel periodo dal " + string(adt_oggi, "dd/mm/yyyy") + " al " + string(adt_in_scadenza, "dd/mm/yyyy") + " " + &
							"oppure nessuna lista distribuzione è stata assegnata alle rispettive manutenzioni che risultano in scadenza!")
		ab_in_scadenza = false
	else
		ab_in_scadenza = true
	end if
	
else
	//parametro giorni entro cui considerare in scadenza non specificato
	ab_in_scadenza = false
	//segnalo nel log come warning che tale attività è disattivata
	iuo_log.warn("Parametro numero giorni iniziare ad avvisare che sono in scadenza manutenzioni ordinarie non impostato: non saranno controllate manutenzioni in scadenza!")
	
end if
//#######################################################################################

return 0
end function

public function integer uof_manutenzioni_scadute (datetime adt_data, string as_cod_tipo_lista_dist, string as_cod_lista_dist, ref string as_messaggio, ref string as_errore);datastore			lds_data
string					ls_sql
long					ll_count, ll_index

as_messaggio = "Gentile Manutentore,"+is_br
as_messaggio += "Le forniamo di seguito l'elenco delle manutenzioni ordinarie che in OMNIA risultano non ancora eseguite (SCADUTE) alla data "+string(adt_data, "dd/mm/yyyy") + is_br + is_br

as_errore = ""
//dalla più vecchia alla più recente ----------------------------------------------
ls_sql	= 		"select 	m.anno_registrazione,"+&
							"m.num_registrazione,"+&
							"m.data_registrazione,"+&
							"m.cod_attrezzatura,"+&
							"a.descrizione as des_attrezzatura,"+&
							"m.cod_tipo_manutenzione,"+&
							"isnull(tm.des_tipo_manutenzione, '') as des_tipo_manutenzione "+&
				"from manutenzioni as m "+&
				"join tab_tipi_manutenzione as tm on tm.cod_azienda=m.cod_azienda and "+&
													"tm.cod_attrezzatura=m.cod_attrezzatura and "+&
													"tm.cod_tipo_manutenzione=m.cod_tipo_manutenzione "+&
				"join anag_attrezzature as a on a.cod_azienda=m.cod_azienda and "+&
													"a.cod_attrezzatura=m.cod_attrezzatura "+&
				"where	m.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"m.flag_ordinario='S' and "+&
							"m.flag_eseguito='N' and "+&
							"m.data_registrazione<'" + string(adt_data, s_cs_xx.db_funzioni.formato_data) + "' and "+&
							"tm.cod_tipo_lista_dist='"+as_cod_tipo_lista_dist+"' and tm.cod_lista_dist='"+as_cod_lista_dist+"' "+&
				"order by m.data_registrazione asc"
			
ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_count<0 then
	return -1
	
elseif ll_count=0 then
	destroy lds_data
	iuo_log.warn("Non sono state trovate manutenzioni scadute alla data "+string(adt_data, "dd/mm/yyyy")+" per la lista di distribuzione (tipo/codice) '"+as_cod_tipo_lista_dist+"/"+as_cod_lista_dist+"'")
	iuo_log.info("Query senza risultati: " + ls_sql)
	return 1
end if


as_messaggio += "<table>"

//INTESTAZIONE
as_messaggio +="<tr>"
as_messaggio +="<td><strong>ANNO/NUMERO</strong></td>"
	as_messaggio += "<td><strong>DATA SCADENZA</strong></td>"
	as_messaggio += "<td><strong>ATTREZZATURA</strong></td>"
	as_messaggio += "<td><strong>TIPO MANUTENZIONE</strong></td>"
as_messaggio +="</tr>"
//FINE INTESTAZIONE

for ll_index=1 to ll_count
	as_messaggio +="<tr>"
	as_messaggio +="<td>"+ string(lds_data.getitemnumber(ll_index, 1)) + "/" +  string(lds_data.getitemnumber(ll_index, 2)) + "</td>"
	as_messaggio += "<td>" + string(lds_data.getitemdatetime(ll_index, 3), "dd/mm/yyyy") + "</td>"
	as_messaggio += "<td>"+lds_data.getitemstring(ll_index, 4)+" "+lds_data.getitemstring(ll_index, 5)+"</td>"
	as_messaggio += "<td>" + lds_data.getitemstring(ll_index, 6)+" "+lds_data.getitemstring(ll_index, 7) + "</td>"
	as_messaggio +="</tr>"
next

as_messaggio += "</table>"

as_messaggio += is_br + is_br + "Distinti Saluti e Buon Lavoro!"

destroy lds_data

return 0
end function

public function integer uof_manutenzioni_in_scadenza (datetime adt_data, datetime adt_data_in_scadenza, string as_cod_tipo_lista_dist, string as_cod_lista_dist, ref string as_messaggio, ref string as_errore);datastore			lds_data
string					ls_sql
long					ll_count, ll_index


as_messaggio = "Gentile Manutentore," + is_br
as_messaggio += "Le forniamo di seguito l'elenco delle manutenzioni ordinarie che in OMNIA risultano IN SCADENZA, e da eseguire nel periodo dal "+string(adt_data, "dd/mm/yyyy") + " al " + string(adt_data_in_scadenza, "dd/mm/yyyy") + is_br + is_br
as_errore = ""

//dalla più vecchia alla più recente ----------------------------------------------
ls_sql	= 		"select 	m.anno_registrazione,"+&
							"m.num_registrazione,"+&
							"m.data_registrazione,"+&
							"m.cod_attrezzatura,"+&
							"a.descrizione as des_attrezzatura,"+&
							"m.cod_tipo_manutenzione,"+&
							"isnull(tm.des_tipo_manutenzione, '') as des_tipo_manutenzione "+&
				"from manutenzioni as m "+&
				"join tab_tipi_manutenzione as tm on tm.cod_azienda=m.cod_azienda and "+&
													"tm.cod_attrezzatura=m.cod_attrezzatura and "+&
													"tm.cod_tipo_manutenzione=m.cod_tipo_manutenzione "+&
				"join anag_attrezzature as a on a.cod_azienda=m.cod_azienda and "+&
													"a.cod_attrezzatura=m.cod_attrezzatura "+&
				"where	m.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"m.flag_ordinario='S' and "+&
							"m.flag_eseguito='N' and "+&
							"m.data_registrazione>='" + string(adt_data, s_cs_xx.db_funzioni.formato_data) + "' and "+&
							"m.data_registrazione<='" + string(adt_data_in_scadenza, s_cs_xx.db_funzioni.formato_data) + "' and "+&
							"tm.cod_tipo_lista_dist='"+as_cod_tipo_lista_dist+"' and tm.cod_lista_dist='"+as_cod_lista_dist+"' "+&
				"order by m.data_registrazione asc"
			
ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_count<0 then
	return -1
	
elseif ll_count=0 then
	destroy lds_data
	iuo_log.warn("Non sono state trovate manutenzioni in scadenza nel periodo dal "+string(adt_data, "dd/mm/yyyy")+" al "+string(adt_data_in_scadenza, "dd/mm/yyyy")+" per la lista di distribuzione (tipo/codice) '"+as_cod_tipo_lista_dist+"/"+as_cod_lista_dist+"'")
	iuo_log.info("Query senza risultati: " + ls_sql)
	return 1
end if


as_messaggio += "<table>"

//INTESTAZIONE
as_messaggio +="<tr>"
as_messaggio +="<td><strong>ANNO/NUMERO</strong></td>"
	as_messaggio += "<td><strong>DATA SCADENZA</strong></td>"
	as_messaggio += "<td><strong>ATTREZZATURA</strong></td>"
	as_messaggio += "<td><strong>TIPO MANUTENZIONE</strong></td>"
as_messaggio +="</tr>"
//FINE INTESTAZIONE

for ll_index=1 to ll_count
	as_messaggio +="<tr>"
	as_messaggio +="<td>"+ string(lds_data.getitemnumber(ll_index, 1)) + "/" +  string(lds_data.getitemnumber(ll_index, 2)) + "</td>"
	as_messaggio += "<td>" + string(lds_data.getitemdatetime(ll_index, 3), "dd/mm/yyyy") + "</td>"
	as_messaggio += "<td>"+lds_data.getitemstring(ll_index, 4)+" "+lds_data.getitemstring(ll_index, 5)+"</td>"
	as_messaggio += "<td>" + lds_data.getitemstring(ll_index, 6)+" "+lds_data.getitemstring(ll_index, 7) + "</td>"
	as_messaggio +="</tr>"
next

as_messaggio += "</table>"

as_messaggio += is_br + is_br + "Distinti Saluti e Buon Lavoro!"

destroy lds_data

return 0
end function

public function integer uof_mail (string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_subject, string as_message, ref string as_error);datastore			lds_data
string					ls_sql, ls_to[]
long					ll_count, ll_index


//in modalità test le uniche mail che partono vanno all'indirizzo di test

if ib_test then
	//test mode -------------------------------------------------------------------------------------------------------
	ls_to[1] = is_mail_test
	//-------------------------------------------------------------------------------------------------------------------
else
	//no test mode ---------------------------------------------------------------------------------------------------
	as_error = ""
	
	//recupero i destinatari della lista di distribuzione e mando l'email
	ls_sql = 		"select distinct a.indirizzo "+&
					"from tab_ind_dest as a "+&
					"join det_liste_dist as b on b.cod_azienda=a.cod_azienda and "+&
													"b.cod_destinatario=a.cod_destinatario "+&
					"where 	a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"b.cod_tipo_lista_dist='"+as_cod_tipo_lista_dist+"' and "+&
								"b.cod_lista_dist='"+as_cod_lista_dist+"' and "+&
								"a.indirizzo<>'' and a.indirizzo is not null "
	
	ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_error)
	
	if ll_count<0 then
		return -1
		
	elseif ll_count=0 then
		iuo_log.warn("Non ci sono destinatari con indirizzo email specificato per la lista di distribuzione Tipo/Codice " + as_cod_tipo_lista_dist + "/" + as_cod_lista_dist + " ! Impossibile inviare e-mail!")
		return 1
	end if
	
	for ll_index=1 to ll_count
		ls_to[ll_index] = lds_data.getitemstring(ll_index, 1)
	next
	//-------------------------------------------------------------------------------------------------------------------
end if

return uof_mail(ls_to[], as_subject, as_message, as_error)

end function

public function integer uof_mail (string as_to[], string as_subject, string as_message, ref string as_error);

boolean 		lb_result
integer		li_ret


uo_sendmail luo_sendmail
luo_sendmail = create uo_sendmail

luo_sendmail.uof_set_from(is_from)
luo_sendmail.uof_set_to(as_to[])
//luo_sendmail.uof_set_cc(as_cc[])
//luo_sendmail.uof_set_bcc(as_bcc[])
//luo_sendmail.uof_set_attachments(as_attachment[])
luo_sendmail.uof_set_subject(as_subject)
luo_sendmail.uof_set_message(as_message)
luo_sendmail.uof_set_smtp(is_smtp_server, is_smtp_usd, is_smtp_pwd)
luo_sendmail.uof_set_html( true )

lb_result = luo_sendmail.uof_send()
as_error = luo_sendmail.uof_get_error()

if lb_result then
	li_ret = 0
else
	li_ret=-1
end if

return li_ret
end function

public function integer uof_check_senza_lista_dist (integer ai_tipo, string as_sql);
//invia mail all'indirizzo principale (is_from) comunicando eventuali manutenzioni scadute o in scadenza non notificate al manutentore 
//in quanto la tipologia di manutenzione non ha lsta di distribuzione associata

//ai_tipo		1=manutenzioni scadute
//				2=manutenzioni in scadenza

datastore			lds_data
string					ls_errore, ls_oggetto, ls_messaggio, ls_to[]
long					ll_index, ll_count

if not ib_notifica_senza_ld then return 0

ll_count = guo_functions.uof_crea_datastore(lds_data, as_sql, ls_errore)

if ll_count>0 then
	
	if ai_tipo=1 then
		ls_oggetto = "Impossibile notificare le seguenti manutenzioni ordinarie scadute"
		ls_messaggio = "Si comunica che alcune manutenzioni ordinarie scadute non sono state notificate al manutentore "+&
							"in quanto le seguenti tipologie di manutenzione ad esse relative in OMNIA sono prive di lista di distribuzione!"
		
	elseif ai_tipo=2 then
		ls_oggetto = "Impossibile notificare le seguenti manutenzioni ordinarie in scadenza"
		ls_messaggio = "Si comunica che alcune manutenzioni ordinarie in scadenza non sono state notificate al manutentore "+&
							"in quanto le seguenti tipologie di manutenzione ad esse relative in OMNIA sono prive di lista di distribuzione!"
		
	else
		destroy lds_data
		return 0
	end if
	
	ls_messaggio += is_br + is_br +"<table>"

	//INTESTAZIONE
	ls_messaggio +="<tr>"
	ls_messaggio +="<td><strong>ATTREZZATURA</strong></td>"
		ls_messaggio += "<td><strong>TIPO MANUTENZIONE</strong></td>"
	ls_messaggio +="</tr>"
	//FINE INTESTAZIONE
	
	for ll_index=1 to ll_count
		ls_messaggio +="<tr>"
		ls_messaggio += "<td>"+lds_data.getitemstring(ll_index, 1) + "</td>"
		ls_messaggio += "<td>" + lds_data.getitemstring(ll_index, 2)+" "+lds_data.getitemstring(ll_index, 3) + "</td>"
		ls_messaggio +="</tr>"
	next
	
	ls_messaggio += "</table>"
	ls_messaggio += is_br + is_br + "Distinti Saluti e Buon Lavoro!"
	
	ls_to[1] = is_from
	ls_to[2] = is_mail_test
	uof_mail(ls_to[], ls_oggetto, ls_messaggio, ls_errore)
	
end if


destroy lds_data

return 0
end function

on uo_service_man_scad.create
call super::create
end on

on uo_service_man_scad.destroy
call super::destroy
end on

