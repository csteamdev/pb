﻿$PBExportHeader$w_cambia_prog_manut.srw
forward
global type w_cambia_prog_manut from w_cs_xx_principale
end type
type cb_azzera from commandbutton within w_cambia_prog_manut
end type
type cb_tutti from commandbutton within w_cambia_prog_manut
end type
type dw_tipi_manut from datawindow within w_cambia_prog_manut
end type
type cb_mod_prog_manut from commandbutton within w_cambia_prog_manut
end type
type cb_ricerca from commandbutton within w_cambia_prog_manut
end type
type dw_tipo_manut_ricerca from uo_cs_xx_dw within w_cambia_prog_manut
end type
end forward

global type w_cambia_prog_manut from w_cs_xx_principale
integer width = 2629
integer height = 1752
string title = "Utilità di Programmazione"
cb_azzera cb_azzera
cb_tutti cb_tutti
dw_tipi_manut dw_tipi_manut
cb_mod_prog_manut cb_mod_prog_manut
cb_ricerca cb_ricerca
dw_tipo_manut_ricerca dw_tipo_manut_ricerca
end type
global w_cambia_prog_manut w_cambia_prog_manut

forward prototypes
public function integer wf_prog_manut ()
public function integer wf_prog_manut_ubicazione ()
end prototypes

public function integer wf_prog_manut ();integer li_righe, li_i, li_intervallo, li_appo, li_trovato 
datetime ldt_data_rif
string   ls_flag_ubicazione, ls_sql, ls_check, ls_sql1, ls_cod_categoria, ls_cod_reparto

dw_tipo_manut_ricerca.accepttext()

ldt_data_rif = dw_tipo_manut_ricerca.getitemdatetime(dw_tipo_manut_ricerca.getrow(),"data_rif")
ls_cod_categoria = dw_tipo_manut_ricerca.getitemstring( dw_tipo_manut_ricerca.getrow(), "categoria")
ls_cod_reparto = dw_tipo_manut_ricerca.getitemstring(dw_tipo_manut_ricerca.getrow(),"reparto")


if isnull(ldt_data_rif) then
	g_mb.messagebox("OMNIA","Attenzione! La data di riferimento è nulla.")
	return -1
end if


li_righe = dw_tipi_manut.rowcount()
if li_righe > 0 then
	ls_sql1 = ""
	for li_i = 1 to li_righe
		ls_check = dw_tipi_manut.getitemstring(li_i,"selezione")
		if ls_check = 'S' then
			if ls_sql1 <> "" then
				ls_sql1 += ","
			end if
			ls_sql1 = ls_sql1 + " '" + dw_tipi_manut.getitemstring(li_i,"cod_manut") + "' "
		end if
	next
	
	if ls_sql1 = "" then
		g_mb.messagebox("OMNIA","Selezionare almeno una tipologia di manutenzione!")
		return -1
	end if
	
	ls_sql = "update manutenzioni set data_registrazione = '" + string(ldt_data_rif,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql = ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' " &
	                + " and cod_tipo_manutenzione in (" + ls_sql1 + ")" &
						 + " and flag_eseguito ='N' " 
	
	
	if ls_cod_categoria <> "" and not isnull(ls_cod_categoria) then
		ls_sql = ls_sql + " and cod_attrezzatura in ( " &
		                + " select cod_attrezzatura " &
							 + " from anag_attrezzature " &
							 + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' " &
							 + " and   cod_cat_attrezzature = '" + ls_cod_categoria + "') "
	end if

	if ls_cod_reparto <> "" and not isnull(ls_cod_reparto) then
		ls_sql = ls_sql + " and cod_attrezzatura in ( " &
		                + " select cod_attrezzatura " &
							 + " from anag_attrezzature " &
							 + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' " &
							 + " and   cod_reparto = '" + ls_cod_reparto + "') "
	end if
	
	execute immediate :ls_sql;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nell'update manutenzioni. ~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	commit;
	g_mb.messagebox("OMNIA","Modifiche avvenute con successo!")
	return 0
end if
return 0
end function

public function integer wf_prog_manut_ubicazione ();datetime ldt_data_rif, ldt_data_man

long		ll_intervallo, ll_i, ll_count

string	ls_cod_categoria, ls_cod_reparto, ls_cod_area, ls_elenco_tipologie, ls_sql


dw_tipo_manut_ricerca.accepttext()

ll_intervallo = dw_tipo_manut_ricerca.getitemnumber(dw_tipo_manut_ricerca.getrow(),"intervallo")

ldt_data_rif = dw_tipo_manut_ricerca.getitemdatetime(dw_tipo_manut_ricerca.getrow(),"data_rif")

ls_cod_categoria = dw_tipo_manut_ricerca.getitemstring( dw_tipo_manut_ricerca.getrow(), "categoria")

ls_cod_reparto = dw_tipo_manut_ricerca.getitemstring(dw_tipo_manut_ricerca.getrow(),"reparto")

if isnull(ldt_data_rif) then
	g_mb.messagebox("OMNIA","Attenzione! La data di riferimento è nulla.")
	return -1
end if

ls_elenco_tipologie = ""
	
for ll_i = 1 to dw_tipi_manut.rowcount()
		
	if dw_tipi_manut.getitemstring(ll_i,"selezione") = 'S' then
		
		if ls_elenco_tipologie <> "" then
			ls_elenco_tipologie += ","
		end if
		
		ls_elenco_tipologie += "'" + dw_tipi_manut.getitemstring(ll_i,"cod_manut") + "'"
		
	end if
	
next
	
if ls_elenco_tipologie = "" then
	g_mb.messagebox("OMNIA","Selezionare almeno una tipologia di manutenzione!")
	return -1
end if

declare ubicazioni cursor for
select  	cod_area_aziendale
from    	tab_aree_aziendali
where   	cod_azienda = :s_cs_xx.cod_azienda
order by ordinamento ASC;

open ubicazioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore ubicazioni: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ll_i = 0

do while true
	
	fetch ubicazioni
	into  :ls_cod_area;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore ubicazioni: " + sqlca.sqlerrtext,stopsign!)
		close ubicazioni;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		close ubicazioni;
		exit
	end if
	
	select count(*)
	into	 :ll_count
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura in (select cod_attrezzatura
			 							 from   anag_attrezzature
										 where  cod_azienda = :s_cs_xx.cod_azienda and
										 		  cod_reparto = :ls_cod_reparto and
												  cod_cat_attrezzature = :ls_cod_categoria and
												  cod_area_aziendale = :ls_cod_area);
												  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura tipologie per ubicazione " + ls_cod_area + ": " + sqlca.sqlerrtext,stopsign!)
		close ubicazioni;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 or ll_count = 0 then
		continue
	end if
	
	ldt_data_man = datetime(relativedate(date(ldt_data_rif),(ll_intervallo * ll_i)),00:00:00)
	
	ll_i ++
	
	ls_sql = "update manutenzioni set data_registrazione = '" + &
				string(ldt_data_man,s_cs_xx.db_funzioni.formato_data) + "' " + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
				" and cod_tipo_manutenzione in (" + ls_elenco_tipologie + ")" + &
				" and flag_eseguito ='N' " + &
				" and cod_attrezzatura in ( " + &
				" select cod_attrezzatura " + &
				" from anag_attrezzature " + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	
	if not isnull(ls_cod_categoria) then
		ls_sql += " and cod_cat_attrezzature = '" + ls_cod_categoria + "' "
	end if
	
	if not isnull(ls_cod_reparto) then
		ls_sql += " and cod_reparto = '" + ls_cod_reparto + "' "
	end if
	
	ls_sql += " and cod_area_aziendale = '" + ls_cod_area + "') "
	
	execute immediate :ls_sql;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nell'update manutenzioni. ~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
loop

commit;

return 0
end function

on w_cambia_prog_manut.create
int iCurrent
call super::create
this.cb_azzera=create cb_azzera
this.cb_tutti=create cb_tutti
this.dw_tipi_manut=create dw_tipi_manut
this.cb_mod_prog_manut=create cb_mod_prog_manut
this.cb_ricerca=create cb_ricerca
this.dw_tipo_manut_ricerca=create dw_tipo_manut_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_azzera
this.Control[iCurrent+2]=this.cb_tutti
this.Control[iCurrent+3]=this.dw_tipi_manut
this.Control[iCurrent+4]=this.cb_mod_prog_manut
this.Control[iCurrent+5]=this.cb_ricerca
this.Control[iCurrent+6]=this.dw_tipo_manut_ricerca
end on

on w_cambia_prog_manut.destroy
call super::destroy
destroy(this.cb_azzera)
destroy(this.cb_tutti)
destroy(this.dw_tipi_manut)
destroy(this.cb_mod_prog_manut)
destroy(this.cb_ricerca)
destroy(this.dw_tipo_manut_ricerca)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipo_manut_ricerca, &
                 "categoria", &
                 sqlca, &
                 "tab_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "des_cat_attrezzature", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_tipo_manut_ricerca, &
                 "reparto", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave)
dw_tipo_manut_ricerca.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 

									 
iuo_dw_main = dw_tipo_manut_ricerca
end event

type cb_azzera from commandbutton within w_cambia_prog_manut
integer x = 411
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;long ll_i


for ll_i = 1 to dw_tipi_manut.rowcount()
	dw_tipi_manut.setitem(ll_i,"selezione","N")
next
end event

type cb_tutti from commandbutton within w_cambia_prog_manut
integer x = 23
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;long ll_i


for ll_i = 1 to dw_tipi_manut.rowcount()
	dw_tipi_manut.setitem(ll_i,"selezione","S")
next
end event

type dw_tipi_manut from datawindow within w_cambia_prog_manut
integer x = 23
integer y = 420
integer width = 2537
integer height = 1100
integer taborder = 30
string title = "none"
string dataobject = "d_elenco_manut"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_mod_prog_manut from commandbutton within w_cambia_prog_manut
integer x = 2194
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;integer li_ris
string   ls_flag_ubicazione

dw_tipo_manut_ricerca.accepttext()


ls_flag_ubicazione = dw_tipo_manut_ricerca.getitemstring(dw_tipo_manut_ricerca.getrow(),"flag_ubicazione")

setpointer(hourglass!)

if ls_flag_ubicazione = "S" then
	li_ris = wf_prog_manut_ubicazione()
else
	li_ris = wf_prog_manut()
end if

setpointer(arrow!)

return 0
end event

type cb_ricerca from commandbutton within w_cambia_prog_manut
integer x = 2194
integer y = 320
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tipologie"
end type

event clicked;string   ls_cod_categoria, ls_cod_reparto, ls_flag_ubicazione, ls_sql, ls_cod_tipo_manut, &
			ls_des_tipo_manut, ls_tipo_manut1, ls_tipo_manut2, ls_tipo_manut3, ls_tipo_manut4, ls_tipo_manut5
datetime ldt_data_rif
long     ll_giorni_ubicazione
integer  li_trovato, li_riga

dw_tipo_manut_ricerca.accepttext()
dw_tipi_manut.reset()

ldt_data_rif = dw_tipo_manut_ricerca.getitemdatetime( dw_tipo_manut_ricerca.getrow(), "data_rif")


ls_cod_categoria = dw_tipo_manut_ricerca.getitemstring( dw_tipo_manut_ricerca.getrow(), "categoria")
ls_cod_reparto = dw_tipo_manut_ricerca.getitemstring(dw_tipo_manut_ricerca.getrow(),"reparto")

if (isnull(ls_cod_categoria) or ls_cod_categoria = "") and (isnull(ls_cod_reparto) or ls_cod_reparto = "") then
	g_mb.messagebox("OMNIA","Attenzione! Selezionare la categoria o l'impianto!")
	return -1
end if

ls_flag_ubicazione = dw_tipo_manut_ricerca.getitemstring(dw_tipo_manut_ricerca.getrow(),"flag_ubicazione")
li_trovato = 0

ls_sql = " select distinct(cod_tipo_manutenzione), " &
       + " des_tipo_manutenzione " &
       + " from tab_tipi_manutenzione            " &
		 + " where cod_attrezzatura in    " &
		 + " ( select cod_attrezzatura    " &
		 + "   from anag_attrezzature     " &
		 + "   where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
		 
if ls_cod_categoria <> "" and not isnull(ls_cod_categoria) then
	ls_sql = ls_sql + " and cod_cat_attrezzature = '" + ls_cod_categoria + "' "
end if

if ls_cod_reparto <> "" and not isnull(ls_cod_reparto) then
	ls_sql = ls_sql + " and cod_reparto = '" + ls_cod_reparto + "' "
end if


ls_sql = ls_sql + " ) and cod_azienda = '" + s_cs_xx.cod_azienda + "' "


select stringa
into   :ls_tipo_manut1
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM1';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Attenzione manca il parametro TM1 in parametri azienda; chiedere all'amministratore del sistema !!!")
	return -1
end if	

select stringa
into   :ls_tipo_manut2
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM2';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Attenzione manca il parametro TM2 in parametri azienda; chiedere all'amministratore del sistema !!!")
	return -1
end if	

select stringa
into   :ls_tipo_manut3
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM3';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Attenzione manca il parametro TM3 in parametri azienda; chiedere all'amministratore del sistema !!!")
	return -1
end if	

select stringa
into   :ls_tipo_manut4
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM4';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Attenzione manca il parametro TM4 in parametri azienda; chiedere all'amministratore del sistema !!!")
	return -1
end if	

select stringa
into   :ls_tipo_manut5
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM5';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Attenzione manca il parametro TM5 in parametri azienda; chiedere all'amministratore del sistema !!!")
	return -1
end if	


ls_sql = ls_sql + " and cod_tipo_manutenzione not in ('" + ls_tipo_manut1 + "','" + ls_tipo_manut2 + "','" + ls_tipo_manut3 + "','" + ls_tipo_manut4 + "','" + ls_tipo_manut5 + "')"
ls_sql = ls_sql + " and (flag_blocco is null or flag_blocco = 'N') order by cod_tipo_manutenzione "


DECLARE cu_tipi_manut DYNAMIC CURSOR FOR SQLSA;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cu_tipi_manut;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nell'apertura del cursore. ~n" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

li_riga = -1
do while 1 = 1 
	FETCH cu_tipi_manut INTO :ls_cod_tipo_manut,
	                         :ls_des_tipo_manut;
	if sqlca.sqlcode = 100 then exit
	
	li_riga = dw_tipi_manut.insertrow(0)
	dw_tipi_manut.setitem(li_riga,"cod_manut",ls_cod_tipo_manut)
	dw_tipi_manut.setitem(li_riga,"des_manut",ls_des_tipo_manut)
	
loop

dw_tipi_manut.resetupdate()
CLOSE cu_tipi_manut ;

if li_riga = -1 then
	g_mb.messagebox("OMNIA","Nessun risultato per la ricerca!")
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella chiusura del cursore." + sqlca.sqlerrtext)
	rollback;
	return -1
end if
return 0



end event

type dw_tipo_manut_ricerca from uo_cs_xx_dw within w_cambia_prog_manut
integer x = 23
integer y = 20
integer width = 2537
integer height = 280
integer taborder = 10
string dataobject = "d_riordina_manut"
end type

