﻿$PBExportHeader$w_cancella_schede.srw
forward
global type w_cancella_schede from w_cs_xx_risposta
end type
type cb_cancella from commandbutton within w_cancella_schede
end type
type dw_cancella_schede from uo_cs_xx_dw within w_cancella_schede
end type
end forward

global type w_cancella_schede from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 2862
integer height = 1316
string title = "Cancellazione Schede"
cb_cancella cb_cancella
dw_cancella_schede dw_cancella_schede
end type
global w_cancella_schede w_cancella_schede

on w_cancella_schede.create
int iCurrent
call super::create
this.cb_cancella=create cb_cancella
this.dw_cancella_schede=create dw_cancella_schede
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cancella
this.Control[iCurrent+2]=this.dw_cancella_schede
end on

on w_cancella_schede.destroy
call super::destroy
destroy(this.cb_cancella)
destroy(this.dw_cancella_schede)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)				 

dw_cancella_schede.set_dw_options(sqlca, &
                                   pcca.null_object, &
                                   c_multiselect + &
							    		     c_nomodify + &
											  c_nodelete + &
											  c_disablecc + &
											  c_default, &
                                   c_default)
end event

event open;call super::open;long ll_cod_scheda, ll_i
string ls_des_scheda, ls_valore

ll_i = 1
dw_cancella_schede.reset()

declare cu_schede cursor for 
 select cod_scheda,
 		  des_scheda,
		  valore
	from tab_schede_manutenzioni
  where cod_azienda = :s_cs_xx.cod_azienda;	
  
open cu_schede;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in fase di apertura cursore cu_schede " + sqlca.sqlerrtext)
	return
end if

do while 1 = 1
	fetch cu_schede into :ll_cod_scheda,
							   :ls_des_scheda,
								:ls_valore;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_schede_manutenzioni " + sqlca.sqlerrtext)
		return
	end if								
	if sqlca.sqlcode = 100 then exit
	
	dw_cancella_schede.insertrow(0)
	dw_cancella_schede.setitem(ll_i, "cod_scheda", ll_cod_scheda)				
	dw_cancella_schede.setitem(ll_i, "des_scheda", ls_des_scheda)
	dw_cancella_schede.setitem(ll_i, "valore", ls_valore)
	ll_i ++
loop
close cu_schede;
end event

type cb_cancella from commandbutton within w_cancella_schede
integer x = 2418
integer y = 1100
integer width = 370
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cancella"
end type

event clicked;long ll_i, ll_cod_scheda
string ls_valore,ls_des_scheda
integer li_risposta
boolean lb_result

li_risposta = g_mb.messagebox("Omnia", "Sei sicuro di voler cancellare la scheda?", question!, YesNo!)

if li_risposta = 1 then

	for ll_i = 1 to dw_cancella_schede.rowcount()
		lb_result = dw_cancella_schede.IsSelected(ll_i)
		if lb_result then
			ll_cod_scheda = dw_cancella_schede.getitemnumber(ll_i, "cod_scheda")
			ls_valore = dw_cancella_schede.getitemstring(ll_i, "valore")
			if ((not isnull(ls_valore)) and ls_valore <> '' and ls_valore <> ' ' )then
				
				delete from tab_schede_manutenzioni
				where cod_azienda = :s_cs_xx.cod_azienda and
						tab_schede_manutenzioni.valore = :ls_valore;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Omnia", "Errore in fase di cancellazione dati dalla tabella tab_schede_manutenzioni " + sqlca.sqlerrtext)
					rollback;
					return
				end if	
					
				delete from tab_collegamenti_manutenzioni
				where cod_azienda = :s_cs_xx.cod_azienda and
						tab_collegamenti_manutenzioni.valore = :ls_valore;
//***************************claudia 22/03/2005---------------------
//modificato perchè cosi cancella anche la tabella tab_collegamenti_manutenzioni
//questa è la parte di prima ora cancellata
//			delete from tab_schede_manutenzioni
//					where cod_azienda = :s_cs_xx.cod_azienda
//					  and cod_scheda = :ll_cod_scheda;
//*********************************************************				  
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Omnia", "Errore in fase di cancellazione dati dalla tabella tab_collegamenti_manutenzioni " + sqlca.sqlerrtext)
					rollback;
					return
				end if	
				commit;
				ll_i = 1
				dw_cancella_schede.reset()

				declare cu_schede cursor for 
 				select cod_scheda,
 						 des_scheda,
				 		 valore
				from tab_schede_manutenzioni
  				where cod_azienda = :s_cs_xx.cod_azienda;	
  
				open cu_schede;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Omnia", "Errore in fase di apertura cursore cu_schede " + sqlca.sqlerrtext)
					return
				end if

				do while 1 = 1
					fetch cu_schede into :ll_cod_scheda,
											   :ls_des_scheda,
												:ls_valore;
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_schede_manutenzioni " + sqlca.sqlerrtext)
						return
					end if								
					if sqlca.sqlcode = 100 then exit
	
					dw_cancella_schede.insertrow(0)
					dw_cancella_schede.setitem(ll_i, "cod_scheda", ll_cod_scheda)					
					dw_cancella_schede.setitem(ll_i, "des_scheda", ls_des_scheda)
					dw_cancella_schede.setitem(ll_i, "valore", ls_valore)
					ll_i ++
				loop
				close cu_schede;
				s_cs_xx.parametri.parametro_b_1 = true
				exit
			end if
		end if	
	next	

	if s_cs_xx.parametri.parametro_b_1 then
		close(w_cancella_schede)
	end if

end if


end event

type dw_cancella_schede from uo_cs_xx_dw within w_cancella_schede
integer y = 20
integer width = 2789
integer height = 1060
integer taborder = 10
string dataobject = "d_cancella_schede"
boolean vscrollbar = true
boolean livescroll = true
end type

