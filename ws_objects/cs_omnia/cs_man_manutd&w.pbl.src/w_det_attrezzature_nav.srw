﻿$PBExportHeader$w_det_attrezzature_nav.srw
$PBExportComments$Finestra di dettaglio documenti per attrezzature
forward
global type w_det_attrezzature_nav from w_cs_xx_principale
end type
type st_1 from statictext within w_det_attrezzature_nav
end type
type dw_det_attrezzature from uo_cs_xx_dw within w_det_attrezzature_nav
end type
type cb_note_esterne from commandbutton within w_det_attrezzature_nav
end type
end forward

global type w_det_attrezzature_nav from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2875
integer height = 1024
string title = "Documenti"
st_1 st_1
dw_det_attrezzature dw_det_attrezzature
cb_note_esterne cb_note_esterne
end type
global w_det_attrezzature_nav w_det_attrezzature_nav

type variables
blob ib_blob
string is_tipo


end variables

on w_det_attrezzature_nav.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_det_attrezzature=create dw_det_attrezzature
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_det_attrezzature
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_det_attrezzature_nav.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_det_attrezzature)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;string ls_descrizione

Set_W_Options(c_NoEnablePopup + c_CloseNoSave)

is_tipo = s_cs_xx.parametri.parametro_s_11

choose case is_tipo
	case "scheda"
		dw_det_attrezzature.dataobject = "d_det_attrezzature_nav"
		
		select descrizione
		into   :ls_descrizione
		from   anag_attrezzature 
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_attrezzatura = :s_cs_xx.parametri.parametro_s_10;
		if sqlca.sqlcode = 0 then
			this.title = "Documenti: " + s_cs_xx.parametri.parametro_s_10 + " " + ls_descrizione
		end if
	case "area"
		dw_det_attrezzature.dataobject = "d_det_aree_nav"
		
		select des_area
		into   :ls_descrizione
		from   tab_aree_aziendali
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_area_aziendale = :s_cs_xx.parametri.parametro_s_10;
		if sqlca.sqlcode = 0 then
			this.title = "Documenti: " + s_cs_xx.parametri.parametro_s_10 + " " + ls_descrizione
		end if
		
	case "reparto"
		dw_det_attrezzature.dataobject = "d_det_reparti_nav"
		
		select des_reparto
		into   :ls_descrizione
		from   anag_reparti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_reparto = :s_cs_xx.parametri.parametro_s_10;
		if sqlca.sqlcode = 0 then
			this.title = "Documenti: " + s_cs_xx.parametri.parametro_s_10 + " " + ls_descrizione
		end if
		
	case "divisione"
		dw_det_attrezzature.dataobject = "d_det_divisioni_nav"
		
		select des_divisione
		into   :ls_descrizione
		from   anag_divisioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_divisione = :s_cs_xx.parametri.parametro_s_10;
		if sqlca.sqlcode = 0 then
			this.title = "Documenti: " + s_cs_xx.parametri.parametro_s_10 + " " + ls_descrizione
		end if		
		
	case else
		dw_det_attrezzature.dataobject = "d_det_attrezzature_nav"
end choose

dw_det_attrezzature.set_dw_key("cod_azienda")
dw_det_attrezzature.set_dw_options(sqlca, &
											i_openparm, &
											c_nomodify + &
			                        c_nodelete + &
      	                        c_disableCC, &
											c_default)

iuo_dw_main = dw_det_attrezzature

end event

type st_1 from statictext within w_det_attrezzature_nav
integer x = 23
integer y = 820
integer width = 2400
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
string text = "Attenzione: da questa maschera non è possibile modificare i documenti."
boolean focusrectangle = false
end type

type dw_det_attrezzature from uo_cs_xx_dw within w_det_attrezzature_nav
integer x = 23
integer y = 20
integer width = 2789
integer height = 780
integer taborder = 0
string dataobject = "d_det_attrezzature_nav"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_10)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_note_esterne from commandbutton within w_det_attrezzature_nav
integer x = 2446
integer y = 820
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long    ll_i, ll_progressivo, ll_prog_mimetype

integer li_risposta

string  ls_db

transaction sqlcb

blob    lbl_null

setnull(lbl_null)

ll_i = dw_det_attrezzature.getrow()

if ll_i < 1 then return

if is_tipo = "scheda" then
	ll_progressivo = dw_det_attrezzature.getitemnumber(ll_i, "prog_attrezzatura")
else
	ll_progressivo = dw_det_attrezzature.getitemnumber(ll_i, "progressivo")
end if

ll_prog_mimetype = dw_det_attrezzature.getitemnumber( ll_i, "prog_mimetype")

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	choose case is_tipo
		case "scheda"
	
			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       det_attrezzature
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_attrezzatura = :s_cs_xx.parametri.parametro_s_10 and 
						  prog_attrezzatura = :ll_progressivo
			using      sqlcb;
			
		case "area"

			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       tab_aree_aziendali_blob
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_area_aziendale = :s_cs_xx.parametri.parametro_s_10 and 
						  progressivo = :ll_progressivo
			using      sqlcb;
			
		case "reparto"
			
			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       anag_reparti_blob
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_reparto = :s_cs_xx.parametri.parametro_s_10 and 
						  progressivo = :ll_progressivo
			using      sqlcb;			
			
		case "divisione"
			
			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       anag_divisioni_blob
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_divisione = :s_cs_xx.parametri.parametro_s_10 and 
						  progressivo = :ll_progressivo
			using      sqlcb;						
			
		case else
			
	end choose
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

	
	destroy sqlcb;
	
else
	
	choose case is_tipo
		case "scheda"
	
			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       det_attrezzature
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_attrezzatura = :s_cs_xx.parametri.parametro_s_10 and 
						  prog_attrezzatura = :ll_progressivo;
			
		case "area"

			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       tab_aree_aziendali_blob
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_area_aziendale = :s_cs_xx.parametri.parametro_s_10 and 
						  progressivo = :ll_progressivo;
			
		case "reparto"
			
			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       anag_reparti_blob
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_reparto = :s_cs_xx.parametri.parametro_s_10 and 
						  progressivo = :ll_progressivo;			
			
		case "divisione"
			
			selectblob blob
			into       :s_cs_xx.parametri.parametro_bl_1
			from       anag_divisioni_blob
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_divisione = :s_cs_xx.parametri.parametro_s_10 and 
						  progressivo = :ll_progressivo;						
			
		case else
			
	end choose
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype

window_open(w_ole_documenti, 0)

return

end event

