﻿$PBExportHeader$w_det_registro_tarature.srw
$PBExportComments$DW Registro Tarature - Test di Ripetibilità
forward
global type w_det_registro_tarature from w_cs_xx_risposta
end type
type dw_det_registro_tarature from uo_cs_xx_dw within w_det_registro_tarature
end type
type cb_chiudi from commandbutton within w_det_registro_tarature
end type
type cb_annulla from commandbutton within w_det_registro_tarature
end type
end forward

global type w_det_registro_tarature from w_cs_xx_risposta
int Width=1230
int Height=1241
boolean TitleBar=true
string Title="Test di Ripetibilità"
dw_det_registro_tarature dw_det_registro_tarature
cb_chiudi cb_chiudi
cb_annulla cb_annulla
end type
global w_det_registro_tarature w_det_registro_tarature

event pc_setwindow;call super::pc_setwindow;dw_det_registro_tarature.set_dw_options(sqlca, &
													 i_openparm, &
													 c_nonew + &
													 c_nodelete + &
													 c_modifyonopen, &
													 c_default)

end event

on w_det_registro_tarature.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_det_registro_tarature=create dw_det_registro_tarature
this.cb_chiudi=create cb_chiudi
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_registro_tarature
this.Control[iCurrent+2]=cb_chiudi
this.Control[iCurrent+3]=cb_annulla
end on

on w_det_registro_tarature.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_det_registro_tarature)
destroy(this.cb_chiudi)
destroy(this.cb_annulla)
end on

type dw_det_registro_tarature from uo_cs_xx_dw within w_det_registro_tarature
int X=23
int Y=21
int Width=1143
int Height=1001
int TabOrder=10
string DataObject="d_det_registro_tarature"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_num_registrazione, ll_anno_registrazione, ll_anno_reg_taratura, &
		ll_num_reg_taratura

//ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_attrezzatura")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_registrazione")
ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_registrazione")
//ll_prog_riga_manutenzione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_manutenzione")
ll_anno_reg_taratura = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_reg_taratura")
ll_num_reg_taratura = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_reg_taratura")



l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ll_anno_registrazione, &
						 ll_num_registrazione, &
						 ll_anno_reg_taratura, &
						 ll_num_reg_taratura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_chiudi from commandbutton within w_det_registro_tarature
int X=801
int Y=1041
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;save_on_close(c_socsave)
parent.triggerevent("pc_close")
end event

type cb_annulla from commandbutton within w_det_registro_tarature
event clicked pbm_bnclicked
int X=412
int Y=1041
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;save_on_close(c_socnosave)
parent.triggerevent("pc_close")
end event

