﻿$PBExportHeader$w_det_schede_manutenzioni.srw
$PBExportComments$Finestra Dettaglio Dati Registrazione Manutenzioni
forward
global type w_det_schede_manutenzioni from w_cs_xx_principale
end type
type cb_procedure from cb_apri_manuale within w_det_schede_manutenzioni
end type
type cb_lista_comp from commandbutton within w_det_schede_manutenzioni
end type
type cb_ricerca_prod from cb_prod_ricerca within w_det_schede_manutenzioni
end type
type cb_reg_tarature from commandbutton within w_det_schede_manutenzioni
end type
type cb_programma from commandbutton within w_det_schede_manutenzioni
end type
type cb_proc_tarature from cb_apri_manuale within w_det_schede_manutenzioni
end type
type cb_controllo from commandbutton within w_det_schede_manutenzioni
end type
type dw_det_schede_manutenz_lista from uo_cs_xx_dw within w_det_schede_manutenzioni
end type
type dw_det_schede_manutenz_det_2 from uo_cs_xx_dw within w_det_schede_manutenzioni
end type
type dw_det_schede_manutenz_det_1 from uo_cs_xx_dw within w_det_schede_manutenzioni
end type
type dw_folder from u_folder within w_det_schede_manutenzioni
end type
end forward

global type w_det_schede_manutenzioni from w_cs_xx_principale
integer width = 2601
integer height = 1876
string title = "Dettaglio Dati Manutenzione"
cb_procedure cb_procedure
cb_lista_comp cb_lista_comp
cb_ricerca_prod cb_ricerca_prod
cb_reg_tarature cb_reg_tarature
cb_programma cb_programma
cb_proc_tarature cb_proc_tarature
cb_controllo cb_controllo
dw_det_schede_manutenz_lista dw_det_schede_manutenz_lista
dw_det_schede_manutenz_det_2 dw_det_schede_manutenz_det_2
dw_det_schede_manutenz_det_1 dw_det_schede_manutenz_det_1
dw_folder dw_folder
end type
global w_det_schede_manutenzioni w_det_schede_manutenzioni

type variables
boolean ib_new=FALSE, ib_modify=FALSE
long il_rows_modified[], il_new_row, il_num_rows
end variables

forward prototypes
public function integer wf_prod_codificato (string ws_flag_prod_codificato)
public function integer wf_flag_tipo_manut (string ws_flag_tipo_manut)
public function long wf_new_schede_manutenzione (readonly string ws_cod_attrezzatura)
public function long wf_new_det_schede_manutenzione (readonly string ws_cod_attrezzatura, readonly long wl_num_registrazione)
public function integer wf_crea_det_manutenzione (readonly string ws_cod_attrezzatura, readonly long wl_num_registrazione, readonly long wl_prog_riga_manutenzione, readonly long wl_prog_piani_manutenzione)
public function integer wf_insert_schede_manutenzione (readonly string ws_cod_attrezzatura, readonly long wl_num_registrazione, readonly datetime wdt_prossimo_intervento, readonly string ws_des_manutenzione, readonly string ws_cod_operaio, readonly string ws_flag_ordinario)
end prototypes

public function integer wf_prod_codificato (string ws_flag_prod_codificato);choose case ws_flag_prod_codificato
case "S"
   dw_det_schede_manutenz_det_2.Modify("cod_ricambio_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cf_cod_ricambio.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cod_ricambio.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("des_ricambio_non_codificato_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("des_ricambio_non_codificato.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("flag_ricambio_codificato.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("flag_ricambio_codificato_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("quan_ricambi_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("quan_ricambi.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("costo_ricambio_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("costo_ricambio.Visible=1")

   dw_det_schede_manutenz_det_2.Modify("ret_1.Visible=1")
case "N"
   dw_det_schede_manutenz_det_2.Modify("cod_ricambio_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cf_cod_ricambio.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cod_ricambio.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("des_ricambio_non_codificato_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("des_ricambio_non_codificato.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("flag_ricambio_codificato.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("flag_ricambio_codificato_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("quan_ricambi_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("quan_ricambi.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("costo_ricambio_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("costo_ricambio.Visible=1")

   dw_det_schede_manutenz_det_2.Modify("ret_1.Visible=1")
case "T"
   dw_det_schede_manutenz_det_2.Modify("cod_ricambio_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cf_cod_ricambio.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cod_ricambio.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("des_ricambio_non_codificato_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("des_ricambio_non_codificato.Visible=0")
//   cb_ricerca_prod.visible = false

   dw_det_schede_manutenz_det_2.Modify("flag_ricambio_codificato.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("flag_ricambio_codificato_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("quan_ricambi_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("quan_ricambi.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("costo_ricambio_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("costo_ricambio.Visible=0")

   dw_det_schede_manutenz_det_2.Modify("ret_1.Visible=0")
end choose
return 0   



end function

public function integer wf_flag_tipo_manut (string ws_flag_tipo_manut);choose case ws_flag_tipo_manut
case "M"
   wf_prod_codificato(dw_det_schede_manutenz_det_2.getitemstring( &
                      dw_det_schede_manutenz_det_2.getrow(),"flag_ricambio_codificato") )
   dw_det_schede_manutenz_det_2.Modify("des_taratura_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("des_taratura.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cod_attrezzatura_taratura_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cod_attrezzatura_taratura.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cf_cod_attrezzatura_taratura.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cod_misura.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cod_misura_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("cf_cod_misura.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("valore_min_taratura.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("valore_min_taratura_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("valore_max_taratura.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("valore_max_taratura_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("valore_atteso.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("valore_atteso_t.Visible=0")
   dw_det_schede_manutenz_det_2.Modify("ret_2.Visible=0")
case "T"
   dw_det_schede_manutenz_det_2.Modify("des_taratura_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("des_taratura.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cod_attrezzatura_taratura_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cod_attrezzatura_taratura.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cf_cod_attrezzatura_taratura.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cod_misura.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cod_misura_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("cf_cod_misura.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("valore_min_taratura.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("valore_min_taratura_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("valore_max_taratura.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("valore_max_taratura_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("valore_atteso.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("valore_atteso_t.Visible=1")
   dw_det_schede_manutenz_det_2.Modify("ret_2.Visible=1")
   wf_prod_codificato("T")
end choose
return 0

end function

public function long wf_new_schede_manutenzione (readonly string ws_cod_attrezzatura);long ll_num_registrazione

select max(schede_manutenzioni.num_registrazione)
into   :ll_num_registrazione
from   schede_manutenzioni
where  (schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda) and 
       (schede_manutenzioni.cod_attrezzatura = :ws_cod_attrezzatura) ;

if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
   ll_num_registrazione = 1
else
   ll_num_registrazione = ll_num_registrazione + 1
end if
return ll_num_registrazione

end function

public function long wf_new_det_schede_manutenzione (readonly string ws_cod_attrezzatura, readonly long wl_num_registrazione);long ll_num_registrazione

select max(det_schede_manutenzioni.prog_riga_manutenzione)
into   :ll_num_registrazione
from   det_schede_manutenzioni
where  (det_schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda) and 
       (det_schede_manutenzioni.cod_attrezzatura = :ws_cod_attrezzatura) and
	    (det_schede_manutenzioni.num_registrazione = :wl_num_registrazione) ;

if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
	ll_num_registrazione = 10
else
	ll_num_registrazione = ll_num_registrazione + 10
end if
return ll_num_registrazione
end function

public function integer wf_crea_det_manutenzione (readonly string ws_cod_attrezzatura, readonly long wl_num_registrazione, readonly long wl_prog_riga_manutenzione, readonly long wl_prog_piani_manutenzione);string ls_null
long   ll_prog_riga_manutenzione, ll_num_riga, ll_reg, ll_costo_orario

select tab_piani_manutenzione.prog_riga_manutenzione
into	 :ll_num_riga
from   tab_piani_manutenzione  
where  (tab_piani_manutenzione.cod_azienda = :s_cs_xx.cod_azienda ) and
		 (tab_piani_manutenzione.cod_attrezzatura = :ws_cod_attrezzatura ) and
		 (tab_piani_manutenzione.prog_riga_manutenzione = :wl_prog_piani_manutenzione );
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","Il piano di manutenzione di origine non esiste",Information!)
	return sqlca.sqlcode
end if

select det_schede_manutenzioni.prog_riga_manutenzione
into   :ll_reg
from   det_schede_manutenzioni
where  (det_schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda ) and
		 (det_schede_manutenzioni.cod_attrezzatura = :ws_cod_attrezzatura ) and
		 (det_schede_manutenzioni.num_registrazione = :wl_num_registrazione ) and
		 (det_schede_manutenzioni.prog_riga_manutenzione = :ll_num_riga );
if sqlca.sqlcode = 0 then
	ll_reg = wf_new_det_schede_manutenzione(ws_cod_attrezzatura, wl_num_registrazione)
else
	ll_reg = 10
end if
	
SELECT tab_cat_attrezzature.costo_medio_orario  
INTO   :ll_costo_orario  
FROM   tab_cat_attrezzature  
WHERE  (tab_cat_attrezzature.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (tab_cat_attrezzature.cod_cat_attrezzature = 
             (SELECT anag_operai.cod_cat_attrezzature  
              FROM   anag_operai  
              WHERE  (anag_operai.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                     (anag_operai.cod_operaio = 
								  (select tab_piani_manutenzione.cod_operaio   
									from   tab_piani_manutenzione  
 									where  (tab_piani_manutenzione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
	 	  									 (tab_piani_manutenzione.cod_attrezzatura = :ws_cod_attrezzatura ) AND  
		  									 (tab_piani_manutenzione.prog_riga_manutenzione = :wl_prog_piani_manutenzione ) )  	)  ))   ;
if sqlca.sqlcode <> 0 then
	ll_costo_orario = 0
end if

setnull(ls_null)

insert into det_schede_manutenzioni  
		( cod_azienda,   
		  cod_attrezzatura,   
		  num_registrazione,   
		  prog_riga_manutenzione,   
		  cod_tipo_manutenzione,   
		  flag_manutenzione,   
		  modalita_esecuzione,   
		  flag_ricambio_codificato,   
		  cod_ricambio,   
		  des_ricambio_non_codificato,   
		  quan_ricambi,   
		  costo_ricambio,   
		  cod_operaio,   
		  costo_orario_operaio,   
		  tempo_impiegato,   
		  costo_manodopera_esterna,   
		  des_taratura,   
		  cod_attrezzatura_taratura,   
		  cod_misura,   
		  valore_min_taratura,   
		  valore_max_taratura,   
		  doc_compilato,   
		  num_reg_lista,   
		  prog_liste_con_comp,   
		  num_versione,   
		  num_edizione,   
		  ore_impiegate,   
		  minuti_impiegati,   
		  prog_piani_manutenzione,
		  valore_atteso,
		  flag_eseguito)  
  select tab_piani_manutenzione.cod_azienda,   
			tab_piani_manutenzione.cod_attrezzatura,   
			:wl_num_registrazione,   
			:ll_reg,   
			tab_piani_manutenzione.cod_tipo_manutenzione,   
			tab_piani_manutenzione.flag_manutenzione,   
			tab_piani_manutenzione.modalita_esecuzione,   
			tab_piani_manutenzione.flag_ricambio_codificato,   
			tab_piani_manutenzione.cod_ricambio,   
			tab_piani_manutenzione.des_ricambio_non_codificato,   
			tab_piani_manutenzione.quan_ricambi,   
			0,   
			tab_piani_manutenzione.cod_operaio,   
			:ll_costo_orario,   
			tab_piani_manutenzione.tempo_necessario,   
			0,   
			tab_piani_manutenzione.des_taratura,   
			tab_piani_manutenzione.cod_attrezzatura_taratura,   
			tab_piani_manutenzione.cod_misura,   
			tab_piani_manutenzione.valore_min_taratura,   
			tab_piani_manutenzione.valore_max_taratura,   
			:ls_null,   
			:ls_null,   
			:ls_null,   
			:ls_null,   
			:ls_null,   
			0,   
			0,   
			:wl_prog_piani_manutenzione,
			tab_piani_manutenzione.valore_atteso,
			'N'
	 from tab_piani_manutenzione  
	where ( tab_piani_manutenzione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( tab_piani_manutenzione.cod_attrezzatura = :ws_cod_attrezzatura ) AND  
			( tab_piani_manutenzione.prog_riga_manutenzione = :wl_prog_piani_manutenzione );

return sqlca.sqlcode
end function

public function integer wf_insert_schede_manutenzione (readonly string ws_cod_attrezzatura, readonly long wl_num_registrazione, readonly datetime wdt_prossimo_intervento, readonly string ws_des_manutenzione, readonly string ws_cod_operaio, readonly string ws_flag_ordinario);INSERT INTO schede_manutenzioni  
		  ( cod_azienda,   
		  cod_attrezzatura,   
		  num_registrazione,   
		  data_intervento,   
		  desc_intervento,   
		  ordinario,   
		  note,   
		  prossimo_intervento,   
		  cod_operaio,   
		  flag_eseguito,
		  doc_compilato,
		  num_reg_lista,
		  prog_liste_con_comp,
		  num_versione,
		  num_edizione,
		  cod_guasto,
		  anno_non_conf,
		  num_non_conf)  
	 VALUES ( :s_cs_xx.cod_azienda,   
		  :ws_cod_attrezzatura,   
		  :wl_num_registrazione,   
		  :wdt_prossimo_intervento,   
		  :ws_des_manutenzione,   
		  :ws_flag_ordinario,   
		  null,   
		  null,   
		  :ws_cod_operaio,   
		  'N',
		  null,
		  null,
		  null,
		  null,
		  null,
		  null,
		  null,
		  null )  ;
return sqlca.sqlcode
end function

event pc_setddlb;call super::pc_setddlb;string ls_cod_attrezzatura

ls_cod_attrezzatura = s_cs_xx.parametri.parametro_s_1
f_PO_LoadDDDW_DW(dw_det_schede_manutenz_det_1,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and (tab_tipi_manutenzione.cod_attrezzatura = '" + ls_cod_attrezzatura + "')")

f_PO_LoadDDDW_DW(dw_det_schede_manutenz_lista,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_schede_manutenz_det_2,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

f_PO_LoadDDDW_DW(dw_det_schede_manutenz_det_2,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_schede_manutenz_det_2,"cod_attrezzatura_taratura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_schede_manutenz_det_2,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_schede_manutenz_det_2,"cod_ricambio",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_det_schede_manutenz_det_1
dw_folder.fu_AssignTab(1, "&Generici", l_Objects[])
l_objects[1] = dw_det_schede_manutenz_det_2
l_objects[2] = cb_ricerca_prod
dw_folder.fu_AssignTab(2, "&Manutenzione", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_det_schede_manutenz_lista.set_dw_key("cod_azienda")
dw_det_schede_manutenz_lista.set_dw_key("cod_attrezzatura")
dw_det_schede_manutenz_lista.set_dw_key("num_registrazione")
dw_det_schede_manutenz_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_det_schede_manutenz_det_1.set_dw_options(sqlca,dw_det_schede_manutenz_lista,c_sharedata+c_scrollparent,c_default)
dw_det_schede_manutenz_det_2.set_dw_options(sqlca,dw_det_schede_manutenz_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_det_schede_manutenz_lista

dw_folder.fu_SelectTab(1)

wf_flag_tipo_manut("M")
end event

on w_det_schede_manutenzioni.create
int iCurrent
call super::create
this.cb_procedure=create cb_procedure
this.cb_lista_comp=create cb_lista_comp
this.cb_ricerca_prod=create cb_ricerca_prod
this.cb_reg_tarature=create cb_reg_tarature
this.cb_programma=create cb_programma
this.cb_proc_tarature=create cb_proc_tarature
this.cb_controllo=create cb_controllo
this.dw_det_schede_manutenz_lista=create dw_det_schede_manutenz_lista
this.dw_det_schede_manutenz_det_2=create dw_det_schede_manutenz_det_2
this.dw_det_schede_manutenz_det_1=create dw_det_schede_manutenz_det_1
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_procedure
this.Control[iCurrent+2]=this.cb_lista_comp
this.Control[iCurrent+3]=this.cb_ricerca_prod
this.Control[iCurrent+4]=this.cb_reg_tarature
this.Control[iCurrent+5]=this.cb_programma
this.Control[iCurrent+6]=this.cb_proc_tarature
this.Control[iCurrent+7]=this.cb_controllo
this.Control[iCurrent+8]=this.dw_det_schede_manutenz_lista
this.Control[iCurrent+9]=this.dw_det_schede_manutenz_det_2
this.Control[iCurrent+10]=this.dw_det_schede_manutenz_det_1
this.Control[iCurrent+11]=this.dw_folder
end on

on w_det_schede_manutenzioni.destroy
call super::destroy
destroy(this.cb_procedure)
destroy(this.cb_lista_comp)
destroy(this.cb_ricerca_prod)
destroy(this.cb_reg_tarature)
destroy(this.cb_programma)
destroy(this.cb_proc_tarature)
destroy(this.cb_controllo)
destroy(this.dw_det_schede_manutenz_lista)
destroy(this.dw_det_schede_manutenz_det_2)
destroy(this.dw_det_schede_manutenz_det_1)
destroy(this.dw_folder)
end on

type cb_procedure from cb_apri_manuale within w_det_schede_manutenzioni
integer x = 2171
integer y = 120
integer height = 80
integer taborder = 20
string text = "&Proc.Manut."
end type

event clicked;call super::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLN")
end event

type cb_lista_comp from commandbutton within w_det_schede_manutenzioni
integer x = 2674
integer y = 260
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;long   ll_i, ll_num_versione, ll_prog_liste_con_comp, ll_num_registrazione, ll_prog_riga, &
       ll_num_edizione, ll_num_reg_lista_comp, ll_i_1
string ls_cod_attrezzatura

if isnull(dw_det_schede_manutenz_lista.getitemnumber(dw_det_schede_manutenz_lista.getrow(),"num_reg_lista")) then
   g_mb.messagebox("Compilazione Lista di controllo","Non è stata specificata la lista di controllo", StopSign!)
   return
end if

ll_i = dw_det_schede_manutenz_lista.getrow()
ll_num_reg_lista_comp = dw_det_schede_manutenz_lista.getitemnumber(ll_i,"prog_liste_con_comp") 
ls_cod_attrezzatura = dw_det_schede_manutenz_lista.getitemstring(ll_i,"cod_attrezzatura")
ll_num_registrazione = dw_det_schede_manutenz_lista.getitemnumber(ll_i,"num_registrazione")
ll_prog_riga = dw_det_schede_manutenz_lista.getitemnumber(ll_i,"prog_riga_manutenzione")

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)
   UPDATE det_schede_manutenzioni  
     SET prog_liste_con_comp = :ll_prog_liste_con_comp,   
         num_versione = :ll_num_versione,   
         num_edizione = :ll_num_edizione 
   WHERE ( det_schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( det_schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura ) AND  
         ( det_schede_manutenzioni.num_registrazione = :ll_num_registrazione ) AND  
         ( det_schede_manutenzioni.prog_riga_manutenzione = :ll_prog_riga )   ;
   commit;
   dw_det_schede_manutenz_lista.triggerevent("pcd_retrieve")
end if

ll_i = dw_det_schede_manutenz_lista.getrow()
ll_i_1 = dw_det_schede_manutenz_lista.getitemnumber(ll_i,"prog_liste_con_comp")
window_open_parm(w_det_liste_con_comp, 0, dw_det_schede_manutenz_lista)

end event

type cb_ricerca_prod from cb_prod_ricerca within w_det_schede_manutenzioni
integer x = 2423
integer y = 760
integer width = 73
integer height = 80
integer taborder = 10
end type

on getfocus;call cb_prod_ricerca::getfocus;dw_det_schede_manutenz_det_2.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_ricambio"
end on

type cb_reg_tarature from commandbutton within w_det_schede_manutenzioni
integer x = 2171
integer y = 220
integer width = 370
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Reg.Tarat."
end type

event clicked;if dw_det_schede_manutenz_lista.getitemstring(dw_det_schede_manutenz_lista.getrow(),"flag_manutenzione") = "M" then
	g_mb.messagebox("OMNIA","Registro visualizzabile solo per le tarature", Information!)
 	return
end if

if dw_det_schede_manutenz_lista.getrow() > 0 then
	window_open_parm(w_tes_registro_tarature, -1, dw_det_schede_manutenz_lista)
end if
end event

type cb_programma from commandbutton within w_det_schede_manutenzioni
integer x = 2171
integer y = 320
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prog.&Interv."
end type

event clicked;string ls_unita_tempo, ls_cod_attrezzatura, ls_cod_operaio, ls_uso_scadenza, ls_ordinario
long ll_num_registrazione, ll_periodicita, ll_num_reg_scheda, &
     ll_prog_riga_manutenzione, ll_prog_piani_manutenzione, ll_max, ll_i
datetime ldt_prossimo_intervento, ldt_original_value, ldt_data, ldt_data_intervento


ls_cod_attrezzatura = dw_det_schede_manutenz_lista.GetItemstring(dw_det_schede_manutenz_lista.getrow(), "cod_attrezzatura")
ll_prog_riga_manutenzione = dw_det_schede_manutenz_lista.getitemnumber(dw_det_schede_manutenz_lista.getrow(), "prog_riga_manutenzione")
ll_num_registrazione = dw_det_schede_manutenz_lista.getitemnumber(dw_det_schede_manutenz_lista.getrow(), "num_registrazione")
ll_prog_piani_manutenzione = dw_det_schede_manutenz_lista.getitemnumber(dw_det_schede_manutenz_lista.getrow(), "prog_piani_manutenzione")


setnull(s_cs_xx.parametri.parametro_data_1)
window_open(w_input_data_prossimo_intervento, 0)
if isnull(s_cs_xx.parametri.parametro_data_1) then return

commit;

ldt_prossimo_intervento = s_cs_xx.parametri.parametro_data_1
ls_cod_operaio = dw_det_schede_manutenz_lista.getitemstring(dw_det_schede_manutenz_lista.getrow(), "cod_operaio")
if (ldt_prossimo_intervento > datetime(date("01/01/1900"))) and (not isnull(ldt_prossimo_intervento)) then
	select schede_manutenzioni.num_registrazione
	into   :ll_num_registrazione
	from   schede_manutenzioni
	where  (schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda) and 
			 (schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura) and
			 (schede_manutenzioni.data_intervento = :ldt_prossimo_intervento) ;
	if sqlca.sqlcode = 100 then 
		ll_num_registrazione = wf_new_schede_manutenzione(ls_cod_attrezzatura)
		if wf_insert_schede_manutenzione(ls_cod_attrezzatura, &
													ll_num_registrazione, &
													ldt_prossimo_intervento, &
													"MANUTENZIONE STRAORDINARIA", &
													ls_cod_operaio, &
													"N") <> 0 then
			g_mb.messagebox("Manutenzione Attrezzature","ERRORE NELLA CREAZIONE DELLA SUCCESSIVA MANUTENZIONE PROGRAMMATA  " + sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	if wf_crea_det_manutenzione(ls_cod_attrezzatura, &  
										 ll_num_registrazione, & 
										 ll_prog_riga_manutenzione, &
										 ll_prog_piani_manutenzione) <> 0 then 
		g_mb.messagebox("Manutenzione Attrezzature","ERRORE NELLA CREAZIONE DELLA SUCCESSIVA MANUTENZIONE PROGRAMMATA  " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	commit;
end if

end event

type cb_proc_tarature from cb_apri_manuale within w_det_schede_manutenzioni
event clicked pbm_bnclicked
integer x = 2171
integer y = 20
integer height = 80
integer taborder = 50
string text = "&Proc.Tarat."
end type

event clicked;call super::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLQ")
end event

type cb_controllo from commandbutton within w_det_schede_manutenzioni
event clicked pbm_bnclicked
integer x = 2171
integer y = 420
integer width = 366
integer height = 80
integer taborder = 21
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;string ls_cod_attrezzatura

long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, ll_num_registrazione, &
       ll_num_edizione, ll_num_reg_lista_comp, ll_prog_riga_manutenzione
   
ll_i[1] = dw_det_schede_manutenz_lista.getrow()
ll_num_reg_lista_comp = dw_det_schede_manutenz_lista.getitemnumber(ll_i[1],"prog_liste_con_comp") 

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

	ls_cod_attrezzatura = dw_det_schede_manutenz_lista.getitemstring(ll_i[1],"cod_attrezzatura") 
   ll_num_registrazione =dw_det_schede_manutenz_lista.getitemnumber(ll_i[1],"num_registrazione") 
	ll_prog_riga_manutenzione = dw_det_schede_manutenz_lista.getitemnumber(ll_i[1],"prog_riga_manutenzione") 

   update det_schede_manutenzioni
   set    det_schede_manutenzioni.num_versione = :ll_num_versione,
          det_schede_manutenzioni.num_edizione = :ll_num_edizione,
          det_schede_manutenzioni.num_reg_lista = :ll_num_reg_lista_comp,
          det_schede_manutenzioni.prog_liste_con_comp = :ll_prog_liste_con_comp
   where  det_schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda and
          det_schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura and 
          det_schede_manutenzioni.num_registrazione = :ll_num_registrazione and 
          det_schede_manutenzioni.prog_riga_manutenzione = :ll_prog_riga_manutenzione;

   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_det_schede_manutenz_lista.triggerevent("pcd_retrieve")
   dw_det_schede_manutenz_lista.set_selected_rows(1, &
                                                  ll_i[], &
                                                  c_ignorechanges, &
                                                  c_refreshchildren, &
                                                  c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_det_schede_manutenz_lista)

end event

type dw_det_schede_manutenz_lista from uo_cs_xx_dw within w_det_schede_manutenzioni
integer x = 23
integer y = 20
integer width = 2126
integer height = 500
integer taborder = 40
string dataobject = "d_det_schede_manutenz_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;long ll_num_reg,ll_num_registrazione
string ls_cod_attrezzatura
   
cb_lista_comp.enabled   = false
cb_ricerca_prod.enabled = true
cb_reg_tarature.enabled = false
cb_programma.enabled = false
cb_controllo.enabled = false

ll_num_reg = this.i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_attrezzatura = this.i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_attrezzatura")
SELECT max(prog_riga_manutenzione)  
INTO   :ll_num_registrazione  
FROM   det_schede_manutenzioni  
WHERE  (det_schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		 (det_schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura ) AND  
		 (det_schede_manutenzioni.num_registrazione = :ll_num_reg )   ;
if sqlca.sqlcode <> 0  or isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 10
else
	ll_num_registrazione = ll_num_registrazione + 10
end if
this.SetItem (this.GetRow(),"prog_riga_manutenzione", ll_num_registrazione)

ib_new = true
il_new_row = this.getrow()

end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx, l_Error, ll_num_registrazione
string ls_cod_attrezzatura

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

ll_num_registrazione = dw_det_schede_manutenz_lista.i_parentdw.getitemnumber(dw_det_schede_manutenz_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "num_registrazione")) THEN
      SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
   END IF
NEXT

ls_cod_attrezzatura = dw_det_schede_manutenz_lista.i_parentdw.getitemstring(dw_det_schede_manutenz_lista.i_parentdw.i_selectedrows[1], "cod_attrezzatura")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_attrezzatura")) THEN
      SetItem(l_Idx, "cod_attrezzatura", ls_cod_attrezzatura)
   END IF
NEXT

end on

event pcd_validaterow;call super::pcd_validaterow;if isnull(getitemstring(getrow(),"cod_tipo_manutenzione")) then
   g_mb.messagebox("Dettaglio Manutenzione","E' obbligatorio indicare un tipo di manutenzione",StopSign!)
   pcca.error = c_valfailed
   return
end if

if isnull(getitemstring(getrow(),"cod_operaio")) then
   g_mb.messagebox("Dettaglio Manutenzione","Il codice dell'operatore della manutenzione è obbligatorio",StopSign!)
   pcca.error = c_valfailed
   return
end if

if not isnull(getitemstring(getrow(),"cod_operaio")) and (getitemnumber(getrow(),"costo_orario_operaio") = 0)   then
   g_mb.messagebox("Dettaglio Manutenzione","Il costo orario del manutentore è obbligatorio",StopSign!)
   pcca.error = c_valfailed
   return
end if

//if not isnull(getitemnumber(getrow(),"num_reg_lista")) and isnull(getitemnumber(getrow(),"prog_liste_con_comp")) then
//   messagebox("Dettaglio Manutenzione","Impossibile proseguire senza aver compilato la lista di controllo",StopSign!)
//   pcca.error = c_valfailed
//   return
//end if

if getitemnumber(getrow(),"ore_impiegate") = 0  and getitemnumber(getrow(),"minuti_impiegati") = 0 then
   g_mb.messagebox("Dettaglio Manutenzione","E' obbligatorio indicare il tempo impiegato nella manutenzione",StopSign!)
   pcca.error = c_valfailed
   return
end if



choose case getitemstring(getrow(),"flag_manutenzione")
case "M"
   if getitemstring(getrow(),"flag_ricambio_codificato") = "S" and isnull(getitemstring(getrow(),"cod_ricambio")) then
      g_mb.messagebox("Dettaglio Manutenzione","Il ricambio è di tipo codificato, ma non hai indicato nessun codice ricambio",StopSign!)
      pcca.error = c_valfailed
      return
   end if

   if getitemnumber(getrow(),"quan_ricambi") > 0 and getitemnumber(getrow(),"costo_ricambio") = 0 then
      g_mb.messagebox("Dettaglio Manutenzione","Indicare costo unitario del ricambio utilizzato",StopSign!)
      pcca.error = c_valfailed
      return
   end if
    
case "T"
   if (isnull( getitemstring(getrow(),"des_taratura" ))) or (len(getitemstring(getrow(),"des_taratura")) < 1) then
      g_mb.messagebox("Dettaglio Manutenzione","La descrizione della taratura è obbligatoria",StopSign!)
      pcca.error = c_valfailed
      return
   end if

   if isnull(getitemstring(getrow(),"cod_misura")) then
      g_mb.messagebox("Dettaglio Manutenzione","Indicazione dell'unità di misura obbligatorio",StopSign!)
      pcca.error = c_valfailed
      return
   end if

   if getitemnumber(getrow(),"valore_max_taratura") <  getitemnumber(getrow(),"valore_min_taratura") then
      g_mb.messagebox("Dettaglio Manutenzione","Valori di taratura attrezzatura incoerenti",StopSign!)
      pcca.error = c_valfailed
      return
   end if
end choose
end event

event pcd_view;call super::pcd_view;cb_ricerca_prod.enabled = false

if il_num_rows > 0 then
	cb_lista_comp.enabled = true
	cb_reg_tarature.enabled = true
	cb_programma.enabled = true
	cb_controllo.enabled = true
else
	cb_lista_comp.enabled = false
	cb_reg_tarature.enabled = false
	cb_programma.enabled = false
	cb_controllo.enabled = false
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_num_registrazione
string ls_cod_attrezzatura

ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_attrezzatura")

l_Error = retrieve(s_cs_xx.cod_azienda, &
                   ls_cod_attrezzatura, &
						 ll_num_registrazione)

if l_Error < 0 then
   pcca.error = c_fatal
end if

il_num_rows = l_error
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_lista_comp.enabled = false
	cb_ricerca_prod.enabled = true
	cb_reg_tarature.enabled = false
	cb_programma.enabled = false
	cb_controllo.enabled = false
	ib_modify = true
end if
end event

event updateend;call super::updateend;long ll_num, ll_riga, ll_prog_riga_manutenzione, ll_return
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione

ll_num = dw_det_schede_manutenz_lista.RowCount( )
ll_riga = -1
do while (ll_riga <= ll_num) and (ll_riga <> 0)
	if ll_riga = -1 then ll_riga = 0
	ll_riga = dw_det_schede_manutenz_lista.GetNextModified(ll_riga, Primary!)
	if ll_riga > 0 THEN 
		ls_cod_attrezzatura = getitemstring(ll_riga,"cod_attrezzatura")
		ll_prog_riga_manutenzione = getitemnumber(ll_riga,"prog_riga_manutenzione")
		ls_cod_tipo_manutenzione = getitemstring(ll_riga,"cod_tipo_manutenzione")
		ll_return = g_mb.messagebox("OMNIA","Azzero le ore di utilizzo dell'attrezzatura " + string(ls_cod_attrezzatura) + "nei piani di manutenzione?", Question!, YesNo!, 1)
		if ll_return = 1 then
			update tab_piani_manutenzione
			set    num_ore_utilizzo = 0
			where  tab_piani_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
					 tab_piani_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
					 tab_piani_manutenzione.prog_riga_manutenzione = :ll_prog_riga_manutenzione and
					 tab_piani_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			choose case sqlca.sqlcode
				case 100
					g_mb.messagebox("OMNIA","Non è stato possibile azzera le ore di utilizzo attrezzatura perchè " + &
											 "non esiste in piani di manutenzione un riga che abbia la stessa attrezzatura " + &
											 "progressivo riga e tipo manutenzione: verificare!", Information!)
				case 0
				case else
					g_mb.messagebox("OMNIA","Errore durante l'aggiornamento dei piani manutenzione per azzera " + &
											 "le ore di utilizzo attrezzatura", Information!)	
			end choose
		end if
	end if
loop

end event

event updatestart;call super::updatestart;string ls_unita_tempo, ls_cod_attrezzatura, ls_cod_operaio, ls_uso_scadenza, ls_ordinario
long ll_num_registrazione, ll_periodicita, ll_reg_scheda, ll_num_reg_scheda, &
     ll_prog_riga_manutenzione, ll_prog_piani_manutenzione, ll_max, ll_i, ll_ritorno
datetime ldt_prossimo_intervento, ldt_original_value, ldt_data, ldt_data_intervento


if ib_modify then
// Se sono in modifica significa che ho inserito dei dati riguardanti la manutenzione;
// pertanto chiedo all'utente se questa manutenzione è da ritendersi conclusa e se si vado    
// a generare subito la nuova manutenzione programmata in base alla periodicità revisione
// Controllo anche se esiste una data di prossimo intervento ed eventualmente vado a creare 
// il record prossima manutenzione
	ll_ritorno = g_mb.messagebox("OMNIA","Considero chiuse le manutenzioni modificate e creo le nuove scadenze?",Question!,YesNo!,1)
	if ll_ritorno = 1 then
		ll_max = upperbound(il_rows_modified)
		for ll_i = 1 to ll_max
			if getitemstring(il_rows_modified[ll_i],"flag_eseguito") = "N" then
				ls_cod_operaio = getitemstring(il_rows_modified[ll_i],"cod_operaio")
				ls_cod_attrezzatura = getitemstring(il_rows_modified[ll_i], "cod_attrezzatura")
				ll_prog_riga_manutenzione = getitemnumber(il_rows_modified[ll_i], "prog_riga_manutenzione")
				ll_num_registrazione = getitemnumber(il_rows_modified[ll_i], "num_registrazione")
				ll_prog_piani_manutenzione = getitemnumber(il_rows_modified[ll_i],"prog_piani_manutenzione")

			
				if isnull(ll_prog_piani_manutenzione) or ll_prog_piani_manutenzione = 0 then
					g_mb.messagebox("OMNIA","Riferimento al piano di manutenzione mancante: verificare", Information!)
					return -1
				end if
			
				update det_schede_manutenzioni
				set    flag_eseguito = 'S'
				where  (det_schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda ) and
						 (det_schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura ) and
						 (det_schede_manutenzioni.num_registrazione = :ll_num_registrazione ) and
						 (det_schede_manutenzioni.prog_riga_manutenzione = :ll_prog_riga_manutenzione );  

				select schede_manutenzioni.ordinario, schede_manutenzioni.data_intervento
				into   :ls_ordinario, :ldt_data_intervento
				from   schede_manutenzioni
				where  (schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda ) and
						 (schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura ) and
						 (schede_manutenzioni.num_registrazione = :ll_num_registrazione );
			
				SELECT tab_piani_manutenzione.unita_tempo, 
						 tab_piani_manutenzione.genero_scadenze,
						 tab_piani_manutenzione.periodicita_revisione
				INTO   :ls_unita_tempo,
						 :ls_uso_scadenza, 
						 :ll_periodicita
				FROM   tab_piani_manutenzione
				WHERE  (tab_piani_manutenzione.cod_azienda = :s_cs_xx.cod_azienda ) and
						 (tab_piani_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura ) and
						 (tab_piani_manutenzione.prog_riga_manutenzione = :ll_prog_piani_manutenzione);
			
				if (ls_unita_tempo <> "M") and (ls_unita_tempo <> "O") and (ls_uso_scadenza = "S") and (ls_ordinario = "S")then 
					
					setnull(ldt_prossimo_intervento)
					choose case ls_unita_tempo
					case "E"
						ldt_prossimo_intervento = datetime(relativedate(date(ldt_data_intervento), ll_periodicita * 30))
					case "S" 
						ldt_prossimo_intervento = datetime(relativedate(date(ldt_data_intervento), ll_periodicita * 7))
					case "G"
						ldt_prossimo_intervento = datetime(relativedate(date(ldt_data_intervento), ll_periodicita))
					end choose
			
					if not isnull(ldt_prossimo_intervento) then
						select schede_manutenzioni.num_registrazione
						into   :ll_num_registrazione
						from   schede_manutenzioni
						where  (schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda) and 
								 (schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura) and
								 (schede_manutenzioni.data_intervento = :ldt_prossimo_intervento) ;
						if sqlca.sqlcode = 100 then 
							ll_num_registrazione = wf_new_schede_manutenzione(ls_cod_attrezzatura)
							if wf_insert_schede_manutenzione(ls_cod_attrezzatura, &
																		ll_num_registrazione, &
																		ldt_prossimo_intervento, &
																		"MANUTENZIONE ORDINARIA", &
																		ls_cod_operaio, &
																		"S") <> 0 then
								g_mb.messagebox("Manutenzione Attrezzature","ERRORE NELLA CREAZIONE DELLA SUCCESSIVA MANUTENZIONE PROGRAMMATA  " + sqlca.sqlerrtext)
								return
							end if
						end if
						wf_crea_det_manutenzione(ls_cod_attrezzatura, &
														 ll_num_registrazione, &
														 ll_prog_riga_manutenzione, &
														 ll_prog_piani_manutenzione)
					end if
				end if
			end if
		next
		ib_modify = false
	end if
end if

end event

event pcd_pickedrow;call super::pcd_pickedrow;wf_flag_tipo_manut(getitemstring(getrow(),"flag_manutenzione"))

end event

type dw_det_schede_manutenz_det_2 from uo_cs_xx_dw within w_det_schede_manutenzioni
integer x = 46
integer y = 660
integer width = 2469
integer height = 1080
integer taborder = 100
string dataobject = "d_det_schede_manutenz_det_2"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;long ll_costo_orario

choose case i_colname
case "flag_ricambio_codificato"
   wf_prod_codificato(i_coltext)

case "cod_operaio"
   SELECT tab_cat_attrezzature.costo_medio_orario  
   INTO   :ll_costo_orario  
   FROM   tab_cat_attrezzature  
   WHERE  (tab_cat_attrezzature.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          (tab_cat_attrezzature.cod_cat_attrezzature = 
                (SELECT anag_operai.cod_cat_attrezzature  
                 FROM   anag_operai  
                 WHERE  (anag_operai.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                        (anag_operai.cod_operaio = :i_coltext )  ))   ;
   if sqlca.sqlcode = 100 then
      g_mb.messagebox("Dettaglio Manutenzioni","Non è stata indicato il codice mansione nella scheda dipendente: impossibile caricare il costo", information!)
      return
   else
      if sqlca.sqlcode <> 0 then
         g_mb.messagebox("Dettaglio Manutenzioni","RICERCA COSTO DIPENDENTE:" + sqlca.sqlerrtext, stopsign!)
         return
      end if
   end if
   setitem(getrow(),"costo_orario_operaio",ll_costo_orario)
   
end choose

end event

event getfocus;call super::getfocus;if i_extendmode then

	if ib_modify or ib_new then
		if dw_det_schede_manutenz_lista.getitemstring(dw_det_schede_manutenz_lista.getrow(),"flag_manutenzione") = "M" then
			cb_ricerca_prod.visible = true
		else
			cb_ricerca_prod.visible = false
		end if
	end if

end if
end event

event itemchanged;call super::itemchanged;if ib_modify then
	long ll_i, ll_y
	ll_i = upperbound(il_rows_modified)
	if isnull(ll_i) or ll_i < 1 then
		il_rows_modified[1] = getrow() 
		return
	end if
	for ll_y = 1 to ll_i
		if il_rows_modified[ll_y] = getrow() then return
	next
	il_rows_modified[ll_i + 1] = getrow()
end if

end event

type dw_det_schede_manutenz_det_1 from uo_cs_xx_dw within w_det_schede_manutenzioni
integer x = 91
integer y = 660
integer width = 2377
integer height = 1060
integer taborder = 90
string dataobject = "d_det_schede_manutenz_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_tipo_manutenzione, ls_flag_manutenzione, ls_flag_ricambio_codificato, ls_note
string ls_cod_ricambio, ls_des_ricambio, ls_cod_attrezzatura, ls_testo, ls_cod_ricambio_alternativo
datetime ldt_tempo_previsto
long ll_num_reg_lista, ll_riga, ll_i, ll_y, ll_ore, ll_minuti

choose case i_colname
case "cod_tipo_manutenzione"
   ls_cod_attrezzatura = dw_det_schede_manutenz_lista.i_parentdw.getitemstring(dw_det_schede_manutenz_lista.i_parentdw.i_selectedrows[1],"cod_attrezzatura")
   SELECT tab_tipi_manutenzione.tempo_previsto,   
          tab_tipi_manutenzione.flag_manutenzione,   
          tab_tipi_manutenzione.flag_ricambio_codificato,   
          tab_tipi_manutenzione.cod_ricambio,   
          tab_tipi_manutenzione.cod_ricambio_alternativo,   
          tab_tipi_manutenzione.des_ricambio_non_codificato,   
          tab_tipi_manutenzione.num_reg_lista,
          tab_tipi_manutenzione.modalita_esecuzione
     INTO :ldt_tempo_previsto,   
          :ls_flag_manutenzione,   
          :ls_flag_ricambio_codificato,   
          :ls_cod_ricambio,
          :ls_cod_ricambio_alternativo,   
          :ls_des_ricambio,   
          :ll_num_reg_lista,
			 :ls_note
     FROM tab_tipi_manutenzione  
    WHERE ( tab_tipi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( tab_tipi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura ) AND  
          ( tab_tipi_manutenzione.cod_tipo_manutenzione = :i_coltext )   ;
   if sqlca.sqlcode = 100 then
      g_mb.messagebox("Dettaglio Manutenzione","Impossibile trovare tipo manutenzione:"+i_coltext + "per l'attrezzatura "+ls_cod_attrezzatura, StopSign!)
      return
   end if
   
   setitem(getrow(), "modalita_esecuzione", ls_note)
	ll_ore = hour(time(ldt_tempo_previsto))
	ll_minuti=minute(time(ldt_tempo_previsto))
   setitem(getrow(), "ore_impiegate", ll_ore)
   setitem(getrow(), "minuti_impiegati", ll_minuti)
   setitem(getrow(), "flag_manutenzione", ls_flag_manutenzione)
   setitem(getrow(), "flag_ricambio_codificato", ls_flag_ricambio_codificato)
   if ls_flag_ricambio_codificato = "S" then
      setitem(getrow(), "cod_ricambio", ls_cod_ricambio)
   else
      setitem(getrow(), "des_ricambio_non_codificato", ls_des_ricambio)
   end if
   setitem(getrow(), "num_reg_lista", ll_num_reg_lista)
   wf_flag_tipo_manut(ls_flag_manutenzione)
   f_PO_LoadDDDW_DW(dw_det_schede_manutenz_det_2,"cod_ricambio",sqlca,&
                    "anag_prodotti","cod_prodotto","des_prodotto", &
                    "(anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                    "(anag_prodotti.cod_prodotto = '" + ls_cod_ricambio + "') and " + &
                    "(anag_prodotti.cod_prodotto = '" + ls_cod_ricambio_alternativo + "')" )
case "flag_manutenzione"
   wf_flag_tipo_manut(i_coltext)
end choose
   
if ib_modify then
	ll_i = upperbound(il_rows_modified)
	if isnull(ll_i) or ll_i < 1 then
		il_rows_modified[1] = getrow() 
		return
	end if
	for ll_y = 1 to ll_i
		if il_rows_modified[ll_y] = getrow() then return
	next
	il_rows_modified[ll_i + 1] = getrow()
end if


end event

event getfocus;call super::getfocus;if i_extendmode then
	cb_ricerca_prod.visible = false
end if

end event

type dw_folder from u_folder within w_det_schede_manutenzioni
integer x = 23
integer y = 540
integer width = 2514
integer height = 1220
integer taborder = 80
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Generici"
      SetFocus(dw_det_schede_manutenz_det_1)
   CASE "&Manutenzione"
      SetFocus(dw_det_schede_manutenz_det_2)
END CHOOSE

end event

