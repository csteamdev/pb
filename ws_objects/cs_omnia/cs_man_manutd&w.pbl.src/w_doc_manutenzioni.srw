﻿$PBExportHeader$w_doc_manutenzioni.srw
$PBExportComments$Finestra di dettaglio documenti per manutenzioni
forward
global type w_doc_manutenzioni from w_cs_xx_principale
end type
type dw_doc_manutenzioni from uo_cs_xx_dw within w_doc_manutenzioni
end type
type cb_chiavi from commandbutton within w_doc_manutenzioni
end type
type cb_note_esterne from commandbutton within w_doc_manutenzioni
end type
end forward

global type w_doc_manutenzioni from w_cs_xx_principale
integer width = 2985
integer height = 1100
string title = "Documenti Manutenzioni"
dw_doc_manutenzioni dw_doc_manutenzioni
cb_chiavi cb_chiavi
cb_note_esterne cb_note_esterne
end type
global w_doc_manutenzioni w_doc_manutenzioni

event pc_setwindow;call super::pc_setwindow;dw_doc_manutenzioni.set_dw_key("cod_azienda")
dw_doc_manutenzioni.set_dw_options(sqlca, &
											pcca.null_object, &
											c_scrollself, &
											c_default)

iuo_dw_main = dw_doc_manutenzioni

end event

on w_doc_manutenzioni.create
int iCurrent
call super::create
this.dw_doc_manutenzioni=create dw_doc_manutenzioni
this.cb_chiavi=create cb_chiavi
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_doc_manutenzioni
this.Control[iCurrent+2]=this.cb_chiavi
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_doc_manutenzioni.destroy
call super::destroy
destroy(this.dw_doc_manutenzioni)
destroy(this.cb_chiavi)
destroy(this.cb_note_esterne)
end on

type dw_doc_manutenzioni from uo_cs_xx_dw within w_doc_manutenzioni
integer x = 23
integer y = 20
integer width = 2903
integer height = 860
integer taborder = 10
string dataobject = "d_doc_manutenzioni"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
next


end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_new;call super::pcd_new;if i_extendmode then

	long   ll_progressivo
	
	dec{6} ld_num_protocollo
	
	
	select max(prog_documento) + 1
	into   :ll_progressivo
	from   doc_manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella DOC_MANUTENZIONI")
		pcca.error = c_Fatal
		return
	end if
	
	if ll_progressivo = 0 or isnull(ll_progressivo) then
		ll_progressivo = 1
	end if

	// Assegnare un nuovo numero di protocollo
	
	setnull(ld_num_protocollo)
	
	select max(num_protocollo)
	into   :ld_num_protocollo
	from   tab_protocolli
	where  cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext )
		dw_doc_manutenzioni.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then
		ld_num_protocollo = 0
	end if
	
	ld_num_protocollo = ld_num_protocollo + 1	
	
	insert into tab_protocolli
	(cod_azienda, num_protocollo)
	values (:s_cs_xx.cod_azienda, :ld_num_protocollo);

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
		dw_doc_manutenzioni.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	setitem(getrow(), "num_protocollo", ld_num_protocollo)
	setitem(getrow(), "prog_documento", ll_progressivo)
	setitem(getrow(), "flag_blocco", "N")
	
	cb_chiavi.enabled = false
	cb_note_esterne.enabled = false		
	
end if	
end event

event updateend;call super::updateend;long   ll_i

dec{6} ld_num_protocollo

string ls_cod_famiglia_chiavi, ls_cod_chiave


for ll_i = 1 to this.deletedcount()
	
	ld_num_protocollo = getitemnumber(ll_i, "num_protocollo", delete!, true)

	//cancellazione tabella chiavi protocollo
	delete
	from   tab_chiavi_protocollo
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return
	end if		  

	//cancellazione tabella protocollo
	delete
	from   tab_protocolli
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return 1
	end if
	
	cb_chiavi.enabled = true
	cb_note_esterne.enabled = true
	
next
end event

event pcd_modify;call super::pcd_modify;cb_chiavi.enabled = false
cb_note_esterne.enabled = false	
end event

event pcd_save;call super::pcd_save;cb_chiavi.enabled = true
cb_note_esterne.enabled = true
end event

event pcd_view;call super::pcd_view;cb_chiavi.enabled = true
cb_note_esterne.enabled = true
end event

type cb_chiavi from commandbutton within w_doc_manutenzioni
integer x = 2171
integer y = 900
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiavi"
end type

event clicked;if dw_doc_manutenzioni.getrow() < 1 or isnull(dw_doc_manutenzioni.getrow()) then
	g_mb.messagebox("Omnia", "Inserire almeno un dettaglio")
	return
end if

window_open_parm(w_tab_chiavi_protocollo, -1, dw_doc_manutenzioni)
end event

type cb_note_esterne from commandbutton within w_doc_manutenzioni
integer x = 2560
integer y = 900
integer width = 366
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_db
long ll_progressivo
integer li_risposta
blob lbl_null
transaction sqlcb

setnull(lbl_null)

if dw_doc_manutenzioni.getrow() < 1 or isnull(dw_doc_manutenzioni.getrow()) then
	g_mb.messagebox("Omnia", "Inserire almeno un dettaglio")
	return
end if

ll_progressivo = dw_doc_manutenzioni.getitemnumber(dw_doc_manutenzioni.getrow(), "prog_documento")

// 11-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
		
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       doc_manutenzioni
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           prog_documento = :ll_progressivo
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if	
	
	destroy sqlcb;
	
else
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       doc_manutenzioni
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           prog_documento = :ll_progressivo;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob doc_manutenzioni
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  prog_documento = :ll_progressivo
		using      sqlcb;
		
		destroy sqlcb;
					  
	else
		
	   updateblob doc_manutenzioni
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  prog_documento = :ll_progressivo;

	end if
	
// fine modifiche

   commit;
	
end if
end event

