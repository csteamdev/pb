﻿$PBExportHeader$w_doc_manutenzioni_ole.srw
$PBExportComments$Finestra di dettaglio documenti per manutenzioni
forward
global type w_doc_manutenzioni_ole from w_ole_v2
end type
end forward

global type w_doc_manutenzioni_ole from w_ole_v2
end type
global w_doc_manutenzioni_ole w_doc_manutenzioni_ole

type variables

end variables

forward prototypes
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name)
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
public subroutine wf_load_documents ()
end prototypes

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);// cancello riga
delete from doc_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_documento = :astr_data.progressivo;

	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
else
	return true
end if

//cancellazione tabella chiavi protocollo
delete from tab_chiavi_protocollo
where
	cod_azienda = :s_cs_xx.cod_azienda and
	num_protocollo = :astr_data.num_registrazione;
  
if sqlca.sqlcode < 0 then
	g_mb.error("Errore cancellazione chiavi procollo:" + string(astr_data.num_registrazione), "Errore: " + sqlca.sqlerrtext)
	return false
end if		  

//cancellazione tabella protocollo
delete from tab_protocolli
where
	cod_azienda = :s_cs_xx.cod_azienda and
	num_protocollo = :astr_data.num_registrazione;
  
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore cancellazione procollo:" + string(astr_data.num_registrazione), "Errore: " + sqlca.sqlerrtext)
	return false
end if
// ----
end function

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);selectblob blob
into :ab_file_blob
from doc_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_documento = :astr_data.progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la lettura del documento " + alv_item.label + ". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name);update doc_manutenzioni
set 
	des_blob = :as_new_name,
	descrizione = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_documento = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	// errore durante il salvataggio
	return false
else
	// tutto ok
	return true
end if
end function

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);long ll_progressivo, ll_num_protocollo
str_ole lstr_data

// Assegnare un nuovo numero di protocollo
select max(num_protocollo)
into :ll_num_protocollo
from tab_protocolli
where
	cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext )
	return false
end if

if sqlca.sqlcode <> 0 or isnull(ll_num_protocollo) then ll_num_protocollo = 0

ll_num_protocollo ++

insert into tab_protocolli(cod_azienda, num_protocollo)
values (:s_cs_xx.cod_azienda, :ll_num_protocollo);

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
	return false
end if
// ----
	
// Calcolo progressivo
select max(prog_documento)
into :ll_progressivo
from doc_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il calcolo del progressivo per il file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) or ll_progressivo < 1 then
	ll_progressivo = 0
end if

ll_progressivo ++
	
// Inserimento nuova riga
insert into doc_manutenzioni (
	cod_azienda,
	prog_documento,
	descrizione,
	des_blob,
	flag_blocco,
	prog_mimetype,
	num_protocollo)
values (
	:s_cs_xx.cod_azienda,
	:ll_progressivo,
	:as_file_name,
	:as_file_name,
	'N',
	:al_prog_mimetype,
	:ll_num_protocollo);
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Aggiornamento campo blob
updateblob doc_manutenzioni
set blob = :ab_file_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_documento = :ll_progressivo;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Creo struttura e aggiongo icona
lstr_data.num_registrazione = ll_num_protocollo
lstr_data.progressivo = ll_progressivo
lstr_data.prog_mimetype = al_prog_mimetype
// aggiungere informazioni alla struttura se necessario

wf_add_document(as_file_name, lstr_data)

return true
end function

public subroutine wf_load_documents ();string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

ls_sql = "SELECT &
	prog_documento, &
	descrizione, &
	prog_mimetype, &
	num_protocollo &
FROM doc_manutenzioni &
WHERE &
	cod_azienda ='" + s_cs_xx.cod_azienda +"' "
	
if not f_crea_datastore(ref lds_documenti, ls_sql) then
	return 
end if

li_rows = lds_documenti.retrieve()
for li_i = 1 to li_rows
	lstr_data.num_registrazione = lds_documenti.getitemnumber(li_i, "num_protocollo") // per non creare una variabile in più!
	lstr_data.progressivo = lds_documenti.getitemnumber(li_i, "prog_documento")
	lstr_data.prog_mimetype = lds_documenti.getitemnumber(li_i, "prog_mimetype")
	
	wf_add_document(lds_documenti.getitemstring(li_i, "descrizione"), lstr_data)
next
end subroutine

on w_doc_manutenzioni_ole.create
call super::create
end on

on w_doc_manutenzioni_ole.destroy
call super::destroy
end on

type st_loading from w_ole_v2`st_loading within w_doc_manutenzioni_ole
end type

type cb_cancella from w_ole_v2`cb_cancella within w_doc_manutenzioni_ole
end type

type lv_documenti from w_ole_v2`lv_documenti within w_doc_manutenzioni_ole
end type

type ddlb_style from w_ole_v2`ddlb_style within w_doc_manutenzioni_ole
end type

type cb_sfoglia from w_ole_v2`cb_sfoglia within w_doc_manutenzioni_ole
end type

