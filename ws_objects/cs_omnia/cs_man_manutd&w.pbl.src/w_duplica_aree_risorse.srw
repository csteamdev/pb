﻿$PBExportHeader$w_duplica_aree_risorse.srw
$PBExportComments$duplicazione risorse collegate alle aree aziendali
forward
global type w_duplica_aree_risorse from w_cs_xx_principale
end type
type cb_3 from commandbutton within w_duplica_aree_risorse
end type
type st_2 from statictext within w_duplica_aree_risorse
end type
type em_origine from editmask within w_duplica_aree_risorse
end type
type cb_2 from commandbutton within w_duplica_aree_risorse
end type
type cb_1 from commandbutton within w_duplica_aree_risorse
end type
type em_anno from editmask within w_duplica_aree_risorse
end type
type st_1 from statictext within w_duplica_aree_risorse
end type
type cb_cerca from commandbutton within w_duplica_aree_risorse
end type
type dw_ricerca from uo_dw_search within w_duplica_aree_risorse
end type
end forward

global type w_duplica_aree_risorse from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2912
integer height = 1560
string title = "Duplicazione Risorse"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
event ue_carica_risorse ( )
cb_3 cb_3
st_2 st_2
em_origine em_origine
cb_2 cb_2
cb_1 cb_1
em_anno em_anno
st_1 st_1
cb_cerca cb_cerca
dw_ricerca dw_ricerca
end type
global w_duplica_aree_risorse w_duplica_aree_risorse

type variables
long	il_anno_riferimento
string	is_cod_area
end variables

event ue_carica_risorse();long			ll_riga
string			ls_cod_area_aziendale, ls_nome, ls_cognome, ls_cod_operaio, ls_codice, ls_descrizione, ls_des_operaio
dec{4}		ld_costo_orario, ld_n_giorni_chiusura, ld_n_persone, ld_n_ore_giorno

dw_ricerca.setredraw( false)

il_anno_riferimento = long(em_origine.text)
if il_anno_riferimento = 0 or isnull(il_anno_riferimento) then
	g_mb.messagebox( "OMNIA", "Attenzione: impostare l'anno di origine!")
end if

ls_cod_area_aziendale = is_cod_area
if isnull(ls_cod_area_aziendale) or ls_cod_area_aziendale = "" then 
	ls_cod_area_aziendale = "%"
end if

declare cu_risorse cursor for
select		cod_area_aziendale,
			cod_operaio,
			costo_orario,
			n_giorni_chiusura,
			n_persone,
			n_ore_giorno
from		tab_aree_risorse
where		cod_azienda = :s_cs_xx.cod_azienda and
			(cod_area_aziendale = :ls_cod_area_aziendale or :ls_cod_area_aziendale = '%' ) and
			anno_riferimento = :il_anno_riferimento;
			
open cu_risorse;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore delle risorse:" + sqlca.sqlerrtext, stopsign!)
	return
end if

dw_ricerca.reset()
		
do while true
	fetch cu_risorse into	:ls_cod_area_aziendale,
								:ls_cod_operaio,
								:ld_costo_orario,
								:ld_n_giorni_chiusura,
								:ld_n_persone,
								:ld_n_ore_giorno;
								
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la fetch del cursore:" + sqlca.sqlerrtext, stopsign!)
		close cu_risorse;
		rollback;
		return
	end if
	
	if isnull(ls_cod_operaio) or ls_cod_operaio = "" then continue
	
	select nome, 
			 cognome
	into	:ls_nome, 
			:ls_cognome
	from	anag_operai
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_operaio = :ls_cod_operaio;
			
	if isnull(ls_nome) then ls_nome = ""
	if isnull(ls_cognome) then ls_cognome = ""
			
	ls_des_operaio = ls_nome + ls_cognome
	
	ll_riga = dw_ricerca.insertrow(0)
	dw_ricerca.setitem( ll_riga, "cod_area", ls_cod_area_aziendale)
	dw_ricerca.setitem( ll_riga, "cod_risorsa", ls_cod_operaio)
	dw_ricerca.setitem( ll_riga, "des_risorsa", ls_des_operaio)
	dw_ricerca.setitem( ll_riga, "costo", ld_costo_orario)
	dw_ricerca.setitem( ll_riga, "flag_seleziona", "N")
	dw_ricerca.setitem( ll_riga, "flag_tipo_risorsa", "O")
	dw_ricerca.setitem( ll_riga, "n_giorni_chiusura", ld_n_giorni_chiusura)
	dw_ricerca.setitem( ll_riga, "n_persone", ld_n_persone)
	dw_ricerca.setitem( ll_riga, "n_ore_giorno", ld_n_ore_giorno)
	
loop

close cu_risorse;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore delle risorse:" + sqlca.sqlerrtext, stopsign!)
	return
end if		

rollback;

dw_ricerca.sort()

dw_ricerca.setredraw( true)
end event

event pc_setwindow;call super::pc_setwindow;datawindow ldw_appo

ldw_appo = i_openparm
il_anno_riferimento =  ldw_appo.getitemnumber(ldw_appo.getrow(), "anno_riferimento")
is_cod_area = ldw_appo.getitemstring(ldw_appo.getrow(), "cod_area")
em_origine.text = string( il_anno_riferimento)

postevent("ue_carica_risorse")


end event

on w_duplica_aree_risorse.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.st_2=create st_2
this.em_origine=create em_origine
this.cb_2=create cb_2
this.cb_1=create cb_1
this.em_anno=create em_anno
this.st_1=create st_1
this.cb_cerca=create cb_cerca
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.em_origine
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.cb_1
this.Control[iCurrent+6]=this.em_anno
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.cb_cerca
this.Control[iCurrent+9]=this.dw_ricerca
end on

on w_duplica_aree_risorse.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.st_2)
destroy(this.em_origine)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.em_anno)
destroy(this.st_1)
destroy(this.cb_cerca)
destroy(this.dw_ricerca)
end on

type cb_3 from commandbutton within w_duplica_aree_risorse
integer x = 434
integer y = 1380
integer width = 389
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "DeSeleziona"
end type

event clicked;long	ll_i
for ll_i = 1 to dw_ricerca.rowcount()
	dw_ricerca.setitem( ll_i, "flag_seleziona", "N")
next
end event

type st_2 from statictext within w_duplica_aree_risorse
integer x = 23
integer y = 20
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Origine:"
boolean focusrectangle = false
end type

type em_origine from editmask within w_duplica_aree_risorse
integer x = 411
integer y = 20
integer width = 402
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
end type

type cb_2 from commandbutton within w_duplica_aree_risorse
integer x = 23
integer y = 1380
integer width = 389
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona"
end type

event clicked;long	ll_i
for ll_i = 1 to dw_ricerca.rowcount()
	dw_ricerca.setitem( ll_i, "flag_seleziona", "S")
next
end event

type cb_1 from commandbutton within w_duplica_aree_risorse
integer x = 2491
integer y = 1380
integer width = 389
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
end type

event clicked;long	ll_ret, ll_progressivo, ll_anno_destinazione, ll_prova
string	ls_cod_operaio, ls_cod_area
dec{4}	ld_costo, ld_n_giorni_chiusura, ld_n_persone, ld_n_ore_giorno

ll_ret = g_mb.messagebox( "OMNIA", "Continuare con la duplicazione delle risorse?", question!, YesNo!)
if ll_ret <> 1 then
	g_mb.messagebox( "OMNIA", "Operazione Annullata!", Information!)
	return
end if

ll_anno_destinazione = long(em_anno.text)

if isnull(ll_anno_destinazione) or ll_anno_destinazione = 0 then
	g_mb.messagebox( "OMNIA", "Attenzione:impostare l'anno di destinazione!", stopsign!)
	return
end if

dw_ricerca.accepttext()

for ll_ret = 1 to dw_ricerca.rowcount()
	
	if dw_ricerca.getitemstring( ll_ret, "flag_seleziona") <> 'S' then continue
	
	ls_cod_operaio = dw_ricerca.getitemstring( ll_ret, "cod_risorsa")
	ls_cod_area = dw_ricerca.getitemstring( ll_ret, "cod_area")
	ld_costo = dw_ricerca.getitemnumber( ll_ret, "costo")
	ld_n_giorni_chiusura = dw_ricerca.getitemnumber( ll_ret, "n_giorni_chiusura")
	ld_n_persone = dw_ricerca.getitemnumber( ll_ret, "n_persone")
	ld_n_ore_giorno = dw_ricerca.getitemnumber( ll_ret, "n_ore_giorno")
	
	select count(*)
	into	:ll_prova
	from	tab_aree_risorse
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_area_aziendale = :ls_cod_area and
			cod_operaio = :ls_cod_operaio and
			anno_riferimento = :ll_anno_destinazione;
			
	if sqlca.sqlcode = 0 and not isnull(ll_prova) and ll_prova > 0 then
		g_mb.messagebox( "OMNIA", "Attenzione: la risorsa " + ls_cod_operaio + " risulta già inserita per l'anno " + string(ll_anno_destinazione), stopsign!)
		continue
	end if
	
	select max(progressivo)
	into	:ll_progressivo
	from	tab_aree_risorse
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_area_aziendale = :ls_cod_area;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la ricerca del progressivo:" + sqlca.sqlerrtext, stopsign!)
		g_mb.messagebox( "OMNIA", "Operazione annullata!", Information!)
		rollback;
		return
	end if
	
	if isnull(ll_progressivo) then ll_progressivo = 0
	ll_progressivo ++
	
	insert into tab_aree_risorse( cod_azienda,
										 cod_area_aziendale,
										 progressivo,
										 cod_fornitore,
										 cod_cat_risorse_esterne,
										 cod_risorsa_esterna,
										 cod_operaio,
										 costo_orario,
										 n_giorni_chiusura,
										 n_ore_giorno,
										 n_persone,
										 anno_riferimento)
	values				(			:s_cs_xx.cod_azienda,
										:ls_cod_area,
										:ll_progressivo,
										null,
										null,
										null,
										:ls_cod_operaio,
										:ld_costo,
										:ld_n_giorni_chiusura,
										:ld_n_ore_giorno,
										:ld_n_persone,
										:ll_anno_destinazione );

	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento della risorsa:" + sqlca.sqlerrtext, stopsign!)
		g_mb.messagebox( "OMNIA", "Operazione Interrotta!", Information!)
		rollback;
		return
	end if
	
next

commit;
g_mb.messagebox( "OMNIA", "Operazione terminata con successo!", Information!)
close(parent)
end event

type em_anno from editmask within w_duplica_aree_risorse
integer x = 411
integer y = 120
integer width = 402
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
end type

type st_1 from statictext within w_duplica_aree_risorse
integer x = 23
integer y = 120
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Destinazione:"
boolean focusrectangle = false
end type

type cb_cerca from commandbutton within w_duplica_aree_risorse
integer x = 846
integer y = 20
integer width = 297
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;parent.postevent("ue_carica_risorse")
end event

type dw_ricerca from uo_dw_search within w_duplica_aree_risorse
integer x = 23
integer y = 220
integer width = 2857
integer height = 1120
integer taborder = 50
string dataobject = "d_duplica_aree_risorse"
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;choose case getcolumnname()
		
	case "cod_cat_risorsa_esterna"
		
			f_po_loaddddw_dw(dw_ricerca,&
								  "cod_risorsa_esterna", &
								  sqlca, &
								  "anag_risorse_esterne",&
								  "cod_risorsa_esterna",&
								  "descrizione", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + data + "' ")	
			
		
end choose

end event

