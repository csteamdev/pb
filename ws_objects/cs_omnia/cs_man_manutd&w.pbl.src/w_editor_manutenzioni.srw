﻿$PBExportHeader$w_editor_manutenzioni.srw
forward
global type w_editor_manutenzioni from w_cs_xx_principale
end type
type tab_1 from tab within w_editor_manutenzioni
end type
type page_1 from userobject within tab_1
end type
type tv_1 from treeview within page_1
end type
type page_1 from userobject within tab_1
tv_1 tv_1
end type
type page_2 from userobject within tab_1
end type
type cb_cerca from commandbutton within page_2
end type
type dw_search from u_dw_search within page_2
end type
type page_2 from userobject within tab_1
cb_cerca cb_cerca
dw_search dw_search
end type
type page_3 from userobject within tab_1
end type
type tv_2 from treeview within page_3
end type
type page_3 from userobject within tab_1
tv_2 tv_2
end type
type tab_1 from tab within w_editor_manutenzioni
page_1 page_1
page_2 page_2
page_3 page_3
end type
type dw_1 from uo_editor_manutenzioni within w_editor_manutenzioni
end type
type p_1 from picture within w_editor_manutenzioni
end type
type st_1 from uo_splitbar within w_editor_manutenzioni
end type
type r_1 from rectangle within w_editor_manutenzioni
end type
end forward

global type w_editor_manutenzioni from w_cs_xx_principale
integer width = 3378
integer height = 2340
string title = "Editor Manutenzioni"
windowstate windowstate = maximized!
event we_menu_mappa_aggiungi ( )
event we_menu_mappa_rinomina ( )
event we_menu_nodo_aggiungi ( )
event we_menu_nodo_rinomina ( )
event we_menu_nodo_elimina ( )
event we_menu_nodo_primo ( )
event we_menu_nodo_immagine ( )
event we_menu_mappa_elimina ( )
event we_menu_collegamento_elimina ( )
event we_controlla_tabella_obsoleta ( )
tab_1 tab_1
dw_1 dw_1
p_1 p_1
st_1 st_1
r_1 r_1
end type
global w_editor_manutenzioni w_editor_manutenzioni

type variables
private string is_msg_name = "Editor Manutenzione"


// -- User
private:
	string is_user_temporaly
	string is_user_documents
	
// -- handle
private:
	long il_handle_rclick = -1
	long il_handle_drag = -1
	long il_current_cod_mappa = -1
	long il_tv_dragging = -1
	
private:
	long il_cod_mappa = -1
	long il_cod_nodo = -1
	string is_dwo_name = ""
	
// -- Treeview ricerca
private:
	string is_sql_filtro
	long il_livello_corrente
	treeviewitem tvi_campo
end variables

forward prototypes
private function integer wf_carica_mappe ()
public function integer wf_carica_nodi (long al_handle, integer ai_cod_mappa, integer ai_primo_nodo)
private function string wf_userinfo (string as_key)
public function boolean wf_trova_immagine (ref string as_image_path, ref string as_image_name, ref integer ai_image_width, ref integer ai_image_height)
public subroutine wf_imposta_tv ()
public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria)
public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione)
public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura)
public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto)
public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura)
public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura_da, string fs_cod_attrezzatura_a)
public subroutine wf_inserisci_singola_registrazione (long fl_handle, long fl_anno_registrazione, long fl_num_registrazione)
public function string wf_leggi_parent (long fl_handle)
public subroutine wf_controlla_tabella_obsoleta ()
end prototypes

event we_menu_mappa_aggiungi();/**
 * Stefano Pulze
 * 29-11-2009
 *
 * Aggiungo una nuova mappa al nodo e salvo subito sul database
 */
 
int li_count
treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

select count(cod_mappa)
into	 :li_count
from	 tab_mappe_manutenzioni
where	 cod_azienda=:s_cs_xx.cod_azienda and
		 des_mappa like 'Nuova Mappa%';
		 
// stefanop 25/06/2010: ticket 2010/154
if sqlca.sqlcode <> 0 or isnull(li_count) then
	li_count = 0
end if

li_count++

ltvi_item.label = "Nuova Mappa " + string(li_count)
ltvi_item.children = true
ltvi_item.selected = false
ltvi_item.pictureindex = 1
ltvi_item.selectedpictureindex = 1

if dw_1.uof_mappa_aggiungi(ltvi_item.label) then
	
	lstr_data.cod_mappa = dw_1.uof_get_cod_mappa()
	lstr_data.cod_nodo = 0
	lstr_data.tipo_nodo = "M"
	lstr_data.primo_nodo = 0
	ltvi_item.data = lstr_data
	
	tab_1.page_1.tv_1.insertitemlast(0, ltvi_item)
	
end if


end event

event we_menu_mappa_rinomina();if il_handle_rclick > 0 then
	
	tab_1.page_1.tv_1.editlabels = true
	tab_1.page_1.tv_1.editlabel(il_handle_rclick)
	
end if
end event

event we_menu_nodo_aggiungi();string ls_img_path, ls_img_name, ls_des_nodo
int li_cod_nodo, li_img_width, li_img_height
treeviewitem ltvi_item, ltvi_child
str_editor_manutenzioni lstr_data

if il_handle_rclick < 1 then
	il_handle_rclick = -1
	return
end if

// -- Seleziono l'immagine
if not wf_trova_immagine(ls_img_path, ls_img_name, li_img_width, li_img_height) then
	il_handle_rclick = -1
	return
end if
// ----

tab_1.page_1.tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

// -- Calcolo progressivo per il nome nodo (trick, solo per scopo estetico)
select
	count(cod_nodo)
into
	:li_cod_nodo
from
	tab_nodi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_mappa = :lstr_data.cod_mappa and
	des_nodo like 'Nodo%';
	
if sqlca.sqlcode <> 0 then
	li_cod_nodo = 0
end if

li_cod_nodo++
ls_des_nodo = "Nodo " + string(li_cod_nodo)
// ----

if dw_1.uof_nodo_aggiungi(lstr_data.cod_mappa, li_cod_nodo, ls_des_nodo, ls_img_path, ls_img_name, li_img_width, li_img_height) then
	
	lstr_data.tipo_nodo = "N"
	lstr_data.cod_nodo = li_cod_nodo
	lstr_data.primo_nodo = -1
	ltvi_child.data = lstr_data
	ltvi_child.label = ls_des_nodo
	ltvi_child.children = false
	ltvi_child.selected = false
	ltvi_child.pictureindex = 2
	ltvi_child.selectedpictureindex = 2
	tab_1.page_1.tv_1.insertitemfirst(il_handle_rclick, ltvi_child)
	
	ltvi_item.children = true
	ltvi_item.expanded = true
	tab_1.page_1.tv_1.setitem(il_handle_rclick, ltvi_item)
	
end if

il_handle_rclick = -1


end event

event we_menu_nodo_rinomina();if il_handle_rclick > 0 then
	
	tab_1.page_1.tv_1.editlabels = true
	tab_1.page_1.tv_1.editlabel(il_handle_rclick)
	
end if
end event

event we_menu_nodo_elimina();if il_handle_rclick < 0 or g_mb.messagebox(is_msg_name, "Eliminare il nodo e tutti i suoi collegamenti?", Question!, YesNo!) = 2 then
	return
end if

treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

tab_1.page_1.tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

if dw_1.uof_nodo_elimina(lstr_data.cod_mappa, lstr_data.cod_nodo) then
	
	tab_1.page_1.tv_1.deleteitem(il_handle_rclick)
	
	if il_cod_mappa = lstr_data.cod_mappa and il_cod_nodo = lstr_data.cod_nodo then
		dw_1.visible = false
	end if
	
end if

il_handle_rclick = -1


end event

event we_menu_nodo_primo();if il_handle_rclick < 1 then
	il_handle_rclick = -1
	return
end if

long li_parent_handle, li_child_handle
treeviewitem ltvi_item, ltvi_child
str_editor_manutenzioni lstr_data

tab_1.page_1.tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

if dw_1.uof_nodo_primo(lstr_data.cod_mappa, lstr_data.cod_nodo) then
	
	// Cerco il vecchio primo nodo  e cambio icona
	li_parent_handle = tab_1.page_1.tv_1.finditem(ParentTreeItem!, il_handle_rclick)	
	if li_parent_handle > 0 then
		li_child_handle = tab_1.page_1.tv_1.finditem(ChildTreeItem!, li_parent_handle)
		do while  li_child_handle > 0
			
			tab_1.page_1.tv_1.getitem(li_child_handle, ltvi_child)
			if ltvi_child.pictureindex = 3 then
				ltvi_child.pictureindex = 2
				ltvi_child.selectedpictureindex = 2
				
				tab_1.page_1.tv_1.setitem(li_child_handle, ltvi_child)
				exit
			end if
			
			li_child_handle = tab_1.page_1.tv_1.finditem(NextTreeItem!, li_child_handle)
		loop
	end if
	// ----
	
	// Icona primo nodo
	ltvi_item.pictureindex = 3
	ltvi_item.selectedpictureindex = 3
	tab_1.page_1.tv_1.setitem(il_handle_rclick, ltvi_item)
	
	il_handle_rclick = -1
end if

end event

event we_menu_nodo_immagine();string ls_img_path, ls_img_name, ls_des_nodo
int li_cod_nodo, li_img_width, li_img_height
treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

if il_handle_rclick < 1 then
	il_handle_rclick = -1
	return
end if

// -- Seleziono l'immagine
if not wf_trova_immagine(ls_img_path, ls_img_name, li_img_width, li_img_height) then
	il_handle_rclick = -1
	return
end if
// ----

tab_1.page_1.tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

if dw_1.uof_nodo_immagine(lstr_data.cod_mappa, lstr_data.cod_nodo, ls_img_path, ls_img_name, li_img_width, li_img_height) then
	dw_1.uof_draw()
end if

il_handle_rclick = -1
end event

event we_menu_mappa_elimina();if il_handle_rclick < 0 then
	il_handle_rclick = -1
	return
end if

if g_mb.messagebox(is_msg_name, "Eliminare la mappa e tutti i nodi/collegamenti assegnati?", Question!, YesNo!) = 2 then
	il_handle_rclick = -1
	return
end if

treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

tab_1.page_1.tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

if dw_1.uof_mappa_elimina(lstr_data.cod_mappa) then
	tab_1.page_1.tv_1.deleteitem(il_handle_rclick)
	dw_1.uof_clear()
	
	if lstr_data.cod_mappa = il_cod_mappa then
		dw_1.visible = false
	end if
end if

il_handle_rclick = -1
end event

event we_menu_collegamento_elimina();if is_dwo_name = "" then
	return
end if

if g_mb.messagebox(is_msg_name, "Eliminare il collegamento?", Question!, YesNo!) = 2 then
	return
end if
	
if dw_1.uof_collegamento_elimina() then
	dw_1.uof_draw()
end if

is_dwo_name = ""
end event

event we_controlla_tabella_obsoleta();/**
 * Stefanop
 * 08/02/2010
 *
 * Controlla se all'interno della tabella tab_schede_manutezioni ci sono ancora delle righe.
 * Se si allora l'importazione deve ancora essere fatta.
 **/
 
string ls_message
long ll_count

ls_message  = "Attenzione: sono presenti record della vecchia gestione delle manutenzioni "
ls_message += "procedere l'aggiornamento alla nuova?"

select count(cod_azienda)
into	:ll_count
from	tab_schede_manutenzioni
where	cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode = 100 or (sqlca.sqlcode = 0 and ll_count > 0) then
	if g_mb.messagebox(this.title, ls_message, Question!, YesNo!) = 1 then
		open(w_sistema_schede_manutenzioni)
		close(this)
	end if
end if
end event

private function integer wf_carica_mappe ();string ls_sql
int		li_rows, li_row
datastore lds_store
str_editor_manutenzioni lstr_data
treeviewitem ltvi_item

ls_sql = "SELECT * FROM tab_mappe_manutenzioni WHERE cod_azienda='"+s_cs_xx.cod_azienda+"'"
if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.messagebox(is_msg_name, "Impossibile creare il datastore per controllare le mappe.~r~n"+ls_sql)
	return -1
end if

li_rows = lds_store.retrieve()
if li_rows > 0 then
	for li_row = 1 to li_rows
		
		ltvi_item.label = lds_store.getitemstring(li_row, "des_mappa")
		
		lstr_data.tipo_nodo = "M" // Mappa
		lstr_data.cod_mappa = lds_store.getitemnumber(li_row, "cod_mappa")
		lstr_data.primo_nodo = lds_store.getitemnumber(li_row, "primo_nodo")
		ltvi_item.data = lstr_data
		ltvi_item.children = true
		ltvi_item.selected = false
		ltvi_item.pictureindex = 1
		ltvi_item.selectedpictureindex = 1

		
		tab_1.page_1.tv_1.insertitemlast(0, ltvi_item)
		
	next
end if
end function

public function integer wf_carica_nodi (long al_handle, integer ai_cod_mappa, integer ai_primo_nodo);string ls_sql
int		li_rows, li_row
datastore lds_store
treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

ls_sql = "SELECT cod_nodo, des_nodo FROM tab_nodi_manutenzioni WHERE cod_azienda='"+s_cs_xx.cod_azienda+"' AND cod_mappa="+string(ai_cod_mappa)
if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.messagebox(is_msg_name, "Impossibile creare datastore per controllare i nodi.~r~n"+ls_sql)
	return -1
end if

li_rows = lds_store.retrieve()
if li_rows > 0 then
	for li_row = 1 to li_rows
		
		lstr_data.tipo_nodo = "N"
		lstr_data.cod_mappa = ai_cod_mappa
		lstr_data.cod_nodo = lds_store.getitemnumber(li_row, "cod_nodo")
		if ai_primo_nodo = lstr_data.cod_nodo then
			lstr_data.primo_nodo = lstr_data.cod_nodo
		else
			lstr_data.primo_nodo = -1
		end if
		ltvi_item.data = lstr_data
		ltvi_item.label = lds_store.getitemstring(li_row, "des_nodo")
		
		if ai_primo_nodo = lstr_data.cod_nodo then
			ltvi_item.pictureindex = 3
			ltvi_item.selectedpictureindex = 3
		else
			ltvi_item.pictureindex = 2
			ltvi_item.selectedpictureindex = 2
		end if
		
		tab_1.page_1.tv_1.insertitemlast(al_handle, ltvi_item)
		
	next
end if

return li_rows
end function

private function string wf_userinfo (string as_key);// Ritorna le variabili d'ambiente del sistema
string ls_Path
string ls_values[]
ContextKeyword lcxk_base

this.GetContextService("Keyword", lcxk_base)
lcxk_base.GetContextKeywords(as_key, ls_values)
IF Upperbound(ls_values) > 0 THEN
   ls_Path = ls_values[1]
ELSE
   ls_Path = "*UNDEFINED*"
END IF

return ls_Path
end function

public function boolean wf_trova_immagine (ref string as_image_path, ref string as_image_name, ref integer ai_image_width, ref integer ai_image_height);integer li_return

li_return = GetFileOpenName("Seleziona un'immagine", as_image_path, as_image_name, "jpg", "File Immagine, *.bmp;*.gif;*.jpg;*.png", is_user_documents)

if li_return = 0 then
	// User press cancel
	return false
else
	p_1.picturename = as_image_path
	
	ai_image_width = p_1.width
	ai_image_height = p_1.height
	
	p_1.picturename = ""

	return true
end if
end function

public subroutine wf_imposta_tv ();string ls_cod_da, ls_cod_a
setpointer(HourGlass!)
tab_1.page_3.tv_2.setredraw(false)


// se non è stato impostato alcun livello ed è stata specificata una attrezzatura
// procedo con il caricamento delle registrazioni della sola attrezzatura richiesta 
if tab_1.page_2.dw_search.getitemstring(1, "flag_tipo_livello_1") = "N" then
	// -- stefanop 09/10/2009: Elimino il trim
	ls_cod_da = tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_da")
	ls_cod_a  = tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_a")
	// ----
	
	wf_inserisci_attrezzature(0, ls_cod_da, ls_cod_a)
	tab_1.page_3.tv_2.setredraw(true)
	setpointer(arrow!)
	return

end if
// --------------------------------

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case tab_1.page_2.dw_search.getitemstring(1, "flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "D"
		 wf_inserisci_divisioni(0, tab_1.page_2.dw_search.getitemstring(1, "cod_divisione"))
		// caricamento divisioni
	case "R"
		wf_inserisci_reparti(0, tab_1.page_2.dw_search.getitemstring(1, "cod_reparto"))
		// caricamento reparti
	case "C"
		wf_inserisci_categorie(0, tab_1.page_2.dw_search.getitemstring(1, "cod_cat_attrezzatura"))
		// caricamento categorie
	case "E" 
		wf_inserisci_aree(0, tab_1.page_2.dw_search.getitemstring(1, "cod_area_aziendale"))
		// caricamento aree
	case "A"
		wf_inserisci_attrezzature(0, tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_da"), tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_a"))
		// caricamento attrezzature
	case "N" 
		//wf_inserisci_registrazioni(0, tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura"))
		wf_inserisci_attrezzature(0, "")
end choose
// --------------------------------

tab_1.page_3.tv_2.setredraw(true)
setpointer(arrow!)
end subroutine

public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_area_aziendale, ls_des_area, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i, ll_livello
str_attrezzature_tree lstr_record
setnull(ls_null)
declare cu_aree dynamic cursor for sqlsa;

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_area_aziendale, des_area FROM tab_aree_aziendali WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_area) and fs_cod_area <> "" then
	ls_sql += " and cod_area_aziendale = '" + fs_cod_area + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_area_aziendale in (select cod_area_aziendale from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + is_sql_filtro + ") "
end if

choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
	case "C"
		ls_sql += " order by cod_area_aziendale"
	case "D"
		ls_sql += " order by des_area"
end choose
//ls_sql = ls_sql + "ORDER BY des_area"

prepare sqlsa from :ls_sql;
open dynamic cu_aree;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore apertura cursore cu_aree (w_manutenzioni:wf_inserisci_aree)~r~n" + sqlca.sqlerrtext)
end if

do while 1=1
   fetch cu_aree into :ls_cod_area_aziendale, :ls_des_area;
   if (sqlca.sqlcode = 100) then
		close cu_aree;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_aree (w_manutenzioni:wf_inserisci_aree)~r~n" + sqlca.sqlerrtext)
		close cu_aree;
		return -1
	end if
	
	lb_dati = true
	lstr_record.codice = ls_cod_area_aziendale
	lstr_record.descrizione = ls_des_area
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "E"
	tvi_campo.data = lstr_record
	
	choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
		case "C"
			tvi_campo.label = ls_des_area + ", " + ls_cod_area_aziendale
		case else
			tvi_campo.label = ls_cod_area_aziendale + ", " + ls_des_area
	end choose
	//tvi_campo.label = ls_cod_area_aziendale + ", " + ls_des_area

	tvi_campo.pictureindex = 3
	tvi_campo.selectedpictureindex = 3
	tvi_campo.overlaypictureindex = 3
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tab_1.page_3.tv_2.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello AREA")
		close cu_aree;
		return 0
	end if

loop
close cu_aree;
return 0
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);str_attrezzature_tree lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = al_handle

if ll_item = 0 then
	return 0
end if

do
	
	tab_1.page_3.tv_2.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello
		case "D"
			as_sql += " and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + lstr_item.codice + "' ) "			
		case "R"
			as_sql += " and cod_reparto = '" + lstr_item.codice + "' "
		case "C"
			as_sql += " and cod_cat_attrezzature = '" + lstr_item.codice + "' "
		case "E"
			as_sql += " and cod_area_aziendale = '" + lstr_item.codice + "' "
		case "A"
			as_sql += " and cod_attrezzatura = '" + lstr_item.codice + "' "
		case "N"
	end choose
	
	ll_item = tab_1.page_3.tv_2.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_cat_attrezzatura, ls_des_cat_attrezzatura, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i,ll_livello
str_attrezzature_tree lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

declare cu_categorie dynamic cursor for sqlsa;
ls_sql = "SELECT cod_cat_attrezzature, des_cat_attrezzature FROM tab_cat_attrezzature WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_categoria) and fs_cod_categoria <> "" then
	ls_sql += " and cod_cat_attrezzature = '" + fs_cod_categoria + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_cat_attrezzature in (select cod_cat_attrezzature from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + is_sql_filtro + ") "
end if

choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
	case "C"
		ls_sql += " order by cod_cat_attrezzature"
	case "D"
		ls_sql += " order by des_cat_attrezzature"
end choose
//ls_sql = ls_sql + "ORDER BY des_cat_attrezzature"

prepare sqlsa from :ls_sql;
open dynamic cu_categorie;
do while 1=1
   fetch cu_categorie into :ls_cod_cat_attrezzatura, :ls_des_cat_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_categorie;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_categorie (w_manutenzioni:wf_inserisci_categorie)~r~n" + sqlca.sqlerrtext)
		close cu_categorie;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_cat_attrezzatura) then ls_des_cat_attrezzatura = ""
	
	tvi_campo.itemhandle = fl_handle
	lstr_record.codice = ls_cod_cat_attrezzatura
	lstr_record.descrizione = ls_des_cat_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.tipo_livello = "C"
	lstr_record.livello = ll_livello
	tvi_campo.data = lstr_record
	
	choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
		case "D"
			tvi_campo.label = ls_des_cat_attrezzatura + ", " + ls_cod_cat_attrezzatura
		case else
			tvi_campo.label = ls_cod_cat_attrezzatura + ", " + ls_des_cat_attrezzatura
	end choose

	tvi_campo.pictureindex = 4
	tvi_campo.selectedpictureindex = 4
	tvi_campo.overlaypictureindex = 4
	tvi_campo.children = true
	tvi_campo.selected = false
	
	ll_risposta = tab_1.page_3.tv_2.insertitemlast(fl_handle, tvi_campo)
	
loop
close cu_categorie;
return 0
end function

public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false

string    ls_sql, ls_cod_divisione, ls_des_divisione, ls_null, ls_appoggio, ls_sql_livello, ls_padre

long      ll_risposta, ll_i, ll_livello

str_attrezzature_tree lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_divisione, des_divisione FROM anag_divisioni WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_divisione)  and len(fs_cod_divisione) > 0 then
	ls_sql += " and cod_divisione = '" + fs_cod_divisione + "' "
end if


// *** michela 19/07/2005: nel caso delle divisioni non mi interessa mettere alcuna where in più rispetto al padre ( ma solo nel caso delle aree)
ls_sql_livello = ""
ls_padre = ""

if ll_livello > 1 then
	ls_padre = wf_leggi_parent(fl_handle)
end if

if ls_padre = "E" then
	wf_leggi_parent(fl_handle,ls_sql_livello)
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_divisione IN ( select cod_divisione from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "'  " + ls_sql_livello + " ) "
end if

choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
	case "C"
		ls_sql += " order by cod_divisione"
	case "D"
		ls_sql += " order by des_divisione"
end choose

declare cu_divisioni dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_divisioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_divisioni: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_divisioni;
	return 0
end if

do while 1=1
   fetch cu_divisioni into :ls_cod_divisione, 
								   :ls_des_divisione;
									
   if (sqlca.sqlcode = 100) then
		close cu_divisioni;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_divisioni (w_manutenzioni:wf_inserisci_divisioni)~r~n" + sqlca.sqlerrtext)
		close cu_divisioni;
		return -1
	end if
	
	lb_dati = true
	lstr_record.codice = ls_cod_divisione
	lstr_record.descrizione = ls_des_divisione
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "D"
	tvi_campo.data = lstr_record
	
	choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
		case "D"
			tvi_campo.label = ls_des_divisione + ", " + ls_cod_divisione
		case else
			tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione
	end choose

	tvi_campo.pictureindex = 5
	tvi_campo.selectedpictureindex = 5
	tvi_campo.overlaypictureindex = 5
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tab_1.page_3.tv_2.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello DIVISIONE")
		close cu_divisioni;
		return 0
	end if

loop
close cu_divisioni;
return 0
end function

public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento registrazioni di manutenzione / taratura (STD)
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false

string  ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, &
        ls_ordine_registrazione, ls_flag_ordinarie, ls_flag_eseguito, ls_data_registrazione, ls_cod_guasto_filtro, &
		  ls_cod_tipo_manut_filtro, ls_flag_scadenze
		  
long    ll_risposta, ll_i, ll_num_righe, ll_anno_registrazione, ll_num_registrazione, ll_livello, ll_anno_reg_filtro, &
        ll_anno_reg_programma, ll_num_reg_programma
		  
datetime ldt_data_registrazione, ldt_data_inizio, ldt_data_fine

str_attrezzature_tree lstr_record

ll_livello = il_livello_corrente

declare cu_registrazioni dynamic cursor for sqlsa;
ls_sql = "SELECT anno_registrazione, " + &
         "       num_registrazione, " + &
			"       data_registrazione, " + &
			"       cod_tipo_manutenzione, " + &
			"       cod_attrezzatura, " + &
			"       flag_eseguito, " + &
			"       anno_reg_programma, " + &
			"       num_reg_programma " + &			
			"FROM   manutenzioni " + &
			"WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

//----------- Michele 26/04/2004 correzione caricamento registrazioni -------------
if len(is_sql_filtro) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro + ")"
end if
//----------- Fine Modifica -------------

choose case tab_1.page_2.dw_search.getitemstring(tab_1.page_2.dw_search.getrow(),"flag_eseguito")
	case "S"
		ls_sql += " and flag_eseguito = 'S' "
	case "N"
		ls_sql += " and flag_eseguito = 'N' "
end choose

ll_anno_reg_filtro = tab_1.page_2.dw_search.getitemnumber(tab_1.page_2.dw_search.getrow(),"anno_registrazione")
ls_flag_ordinarie = tab_1.page_2.dw_search.getitemstring(tab_1.page_2.dw_search.getrow(),"flag_ordinarie")
ls_ordine_registrazione = tab_1.page_2.dw_search.getitemstring(tab_1.page_2.dw_search.getrow(),"flag_ordinamento_registrazioni")
ldt_data_inizio = tab_1.page_2.dw_search.getitemdatetime(tab_1.page_2.dw_search.getrow(),"data_inizio")
ldt_data_fine = tab_1.page_2.dw_search.getitemdatetime(tab_1.page_2.dw_search.getrow(),"data_fine")
ls_cod_tipo_manut_filtro = tab_1.page_2.dw_search.getitemstring(tab_1.page_2.dw_search.getrow(),"cod_tipo_manutenzione")
ls_cod_guasto_filtro = tab_1.page_2.dw_search.getitemstring(tab_1.page_2.dw_search.getrow(),"cod_guasto")

if not isnull(ll_anno_reg_filtro) and ll_anno_reg_filtro > 0 then
	ls_sql += " and anno_registrazione = " + string(ll_anno_reg_filtro) + " "
end if	

if not isnull(ldt_data_inizio) and ldt_data_inizio >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine) and ldt_data_fine >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if

choose case ls_flag_ordinarie
	case "S"
		ls_sql += " and flag_ordinario = 'S' "
	case "N"
		ls_sql += " and flag_ordinario = 'N' "
end choose

if len(ls_cod_tipo_manut_filtro) < 1 then setnull(ls_cod_tipo_manut_filtro)

if not isnull(ls_cod_tipo_manut_filtro) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
end if

if len(ls_cod_guasto_filtro) < 1 then setnull(ls_cod_guasto_filtro)

if not isnull(ls_cod_guasto_filtro) then
	ls_sql += " and cod_guasto = '" + ls_cod_guasto_filtro + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
end if

if isnull(ls_ordine_registrazione) then ls_ordine_registrazione = "D"

// -- stefanop 04/06/2009: commento perchè il flag non è più usato.
//choose case ls_ordine_registrazione
//	case "C"
//		ls_sql += " order by data_registrazione ASC "
//	case "D"
//		ls_sql += " order by data_registrazione DESC "
//	case "A"
//		ls_sql += " order by cod_attrezzatura ASC " 
//	case "T"
//		ls_sql += " order by cod_tipo_manutenzione ASC "
//end choose

choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
	case "C"
		ls_sql += " order by cod_attrezzatura"
	case "D"
		ls_sql += " order by cod_tipo_manutenzione"
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_registrazioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch cu_registrazioni into :ll_anno_registrazione, 
	                            :ll_num_registrazione, 
										 :ldt_data_registrazione, 
										 :ls_cod_tipo_manutenzione, 
										 :ls_cod_attrezzatura, 
										 :ls_flag_eseguito,
										 :ll_anno_reg_programma,
										 :ll_num_reg_programma;

   if (sqlca.sqlcode = 100) then
		close cu_registrazioni;
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_registrazioni (w_manutenzioni:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
		close cu_registrazioni;
		return -1
	end if

	if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""
	
	lb_dati = true
	
	setnull(ls_des_tipo_manutenzione)
	
	select des_tipo_manutenzione
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;

	if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = "<DESCRIZIONE MANCANTE>"
	
	if isnull(ldt_data_registrazione) then
		ls_data_registrazione = "DATA MANCANTE"
	else
		ls_data_registrazione = string(ldt_data_registrazione,"dd/mm/yyyy")
	end if
	
	
	//lstr_record.des_manutenzione = ls_des_tipo_manutenzione
	
	lstr_record.codice = ""		
	lstr_record.descrizione = ""
	lstr_record.livello = 100
	lstr_record.tipo_livello = "M"
	

	choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
		case "C"
			ls_str = ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " + ls_cod_tipo_manutenzione
		case else
			ls_str = ls_cod_tipo_manutenzione + " " + ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " 
	end choose
	//ls_str = ls_cod_tipo_manutenzione + " " + ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " 
	
	tvi_campo.data = lstr_record
	tvi_campo.label = ls_str
	
	/*
	ls_flag_scadenze = wf_controlla_scadenza_periodo( ll_anno_reg_programma, ll_num_reg_programma)
	if ls_flag_eseguito = "S" then	
		if ls_flag_scadenze = 'S' then
			tvi_campo.pictureindex = 6
			tvi_campo.selectedpictureindex = 6
			tvi_campo.overlaypictureindex = 6	
		else	
			tvi_campo.pictureindex = 10
			tvi_campo.selectedpictureindex = 10
			tvi_campo.overlaypictureindex = 10				
		end if
	else
		if ls_flag_scadenze = 'S' then	
			tvi_campo.pictureindex = 7	
			tvi_campo.selectedpictureindex = 7
			tvi_campo.overlaypictureindex = 7
		else
			tvi_campo.pictureindex = 11
			tvi_campo.selectedpictureindex = 11
			tvi_campo.overlaypictureindex = 11
		end if
	end if	
	*/
	
	tvi_campo.children = false
	tvi_campo.selected = false
	ll_risposta = tab_1.page_3.tv_2.insertitemlast(fl_handle, tvi_campo)
loop

close cu_registrazioni;
return 0
end function

public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_reparto, ls_des_reparto, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i,ll_livello
str_attrezzature_tree lstr_record
setnull(ls_null)

ll_livello = il_livello_corrente + 1

declare cu_reparti dynamic cursor for sqlsa;
ls_sql = "SELECT cod_reparto, des_reparto FROM anag_reparti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_reparto) and fs_cod_reparto <> "" then
	ls_sql += " and cod_reparto = '" + fs_cod_reparto + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_reparto in (select cod_reparto from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + is_sql_filtro + ") "
end if

choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
	case "C"
		ls_sql += " order by cod_reparto"
	case "D"
		ls_sql += " order by des_reparto"	
end choose
//ls_sql = ls_sql + "ORDER BY des_reparto"

prepare sqlsa from :ls_sql;

open dynamic cu_reparti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1=1
   fetch cu_reparti into :ls_cod_reparto, :ls_des_reparto;
   if (sqlca.sqlcode = 100) then
		close cu_reparti;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
		close cu_reparti;
		return -1
	end if
	
	if isnull(ls_des_reparto) then ls_des_reparto = ""
	lb_dati = true
	lstr_record.codice = ls_cod_reparto
	lstr_record.descrizione = ls_des_reparto
	lstr_record.tipo_livello = "R"
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle

	tvi_campo.data = lstr_record
	
	choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
		case "D"
			tvi_campo.label = ls_des_reparto + ", " + ls_cod_reparto
		case else
			tvi_campo.label = ls_cod_reparto + ", " + ls_des_reparto
	end choose
	//tvi_campo.label = ls_cod_reparto + ", " + ls_des_reparto

	tvi_campo.pictureindex = 6
	tvi_campo.selectedpictureindex = 6
	tvi_campo.overlaypictureindex = 6
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tab_1.page_3.tv_2.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW")
		close cu_reparti;
		return 0
	end if
	
loop
close cu_reparti;
return 0
end function

public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura);return wf_inserisci_attrezzature(fl_handle, fs_cod_attrezzatura, "")
end function

public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura_da, string fs_cod_attrezzatura_a);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean 			lb_dati = false, lb_rosso = false
string 			ls_sql, ls_cod_attrezzatura, ls_des_attrezzatura, ls_des_identificativa, ls_flag_eseguito, &
					ls_appoggio, ls_flag_visione_strumenti, ls_qualita_attrezzatura
long 				ll_risposta, ll_i, ll_num_righe, ll_livello
datetime			ldt_data_intervento
str_attrezzature_tree lstr_record


ll_livello = il_livello_corrente + 1

declare cu_attrezzature dynamic cursor for sqlsa;

ls_sql = "select cod_attrezzatura, descrizione, qualita_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

// -- stefanop 28/05/2009: Sistemo il becanotto che ho fatto, fortuna che c'è marco :D
if not isnull(fs_cod_attrezzatura_da) and fs_cod_attrezzatura_da <> "" then
	if not isnull(fs_cod_attrezzatura_a) and fs_cod_attrezzatura_a <> "" then
		ls_sql += " and (cod_attrezzatura >= '" + fs_cod_attrezzatura_da + "' and cod_attrezzatura <= '" + fs_cod_attrezzatura_a + "') "
	else
		ls_sql += " and cod_attrezzatura >= '" + fs_cod_attrezzatura_da + "' "
	end if
elseif isnull(fs_cod_attrezzatura_da) and not isnull(fs_cod_attrezzatura_a) then
	ls_sql += " and cod_attrezzatura <= '" + fs_cod_attrezzatura_a + "' "
end if
// --------------

ls_flag_visione_strumenti = tab_1.page_2.dw_search.getitemstring(tab_1.page_2.dw_search.getrow(),"flag_visione_strumenti")
if not isnull(ls_flag_visione_strumenti) then
	choose case ls_flag_visione_strumenti
		case "A"
			ls_sql += " and qualita_attrezzatura = 'A' "
		case "S"
			ls_sql += " and qualita_attrezzatura in ('P','S') "
	end choose
end if

wf_leggi_parent(fl_handle,ls_sql)

choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
	case "C"
		ls_sql += " order by cod_attrezzatura"
	case "D"
		ls_sql += " order by descrizione"
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_attrezzature;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_attrezzature into :ls_cod_attrezzatura, :ls_des_attrezzatura, :ls_qualita_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_attrezzature;
		if lb_dati then return 0
		return 1
	end if
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
		close cu_attrezzature;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_attrezzatura) then ls_des_attrezzatura = ""

	lstr_record.codice = ls_cod_attrezzatura
	
	if isnull(ls_des_attrezzatura) or ls_des_attrezzatura = "" then
		lstr_record.descrizione = "<descrizione mancante>"
	elseif len(ls_des_attrezzatura) > 30 then
		lstr_record.descrizione = left(ls_des_attrezzatura, 30) + "..."
	else
		lstr_record.descrizione = ls_des_attrezzatura
	end if
	
	lstr_record.handle_padre = fl_handle
	lstr_record.tipo_livello = "A"
	lstr_record.livello = ll_livello
	tvi_campo.data = lstr_record
	
	choose case tab_1.page_2.dw_search.getitemstring(1, "flag_ordine")
		case "D"
			tvi_campo.label = lstr_record.descrizione + ", " + ls_cod_attrezzatura
		case else
			tvi_campo.label = ls_cod_attrezzatura + ", " + lstr_record.descrizione
	end choose
	
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 2
	tvi_campo.overlaypictureindex = 2
	
	tvi_campo.children = false
	tvi_campo.selected = false
	
	ll_risposta = tab_1.page_3.tv_2.insertitemlast(fl_handle, tvi_campo)
loop
close cu_attrezzature;

return 0
end function

public subroutine wf_inserisci_singola_registrazione (long fl_handle, long fl_anno_registrazione, long fl_num_registrazione);//// return
//long ll_handle
//treeviewitem tvi_item
//str_attrezzature_tree lstr_record
//
//// -- Controllo il nodo selezionato
//ll_handle = tab_1.page_3.tv_2.finditem(currenttreeitem!, 0)
//if ll_handle = -1 then return
//
//if tab_1.page_3.tv_2.getitem(ll_handle, tvi_item) = -1 then return
//
//lstr_record = tvi_item.data
//if lstr_record.tipo_livello = "A" then
//	ll_handle = tab_1.page_3.tv_2.finditem(parenttreeitem!, ll_handle)
//end if
//// ---------------------
//
//lstr_record.codice = dw_attrezzature_1.getitemstring(row, "cod_attrezzatura")
//
//lstr_record.descrizione = dw_attrezzature_1.getitemstring(row, "descrizione")
//if isnull(lstr_record.descrizione) or lstr_record.descrizione = "" then
//	lstr_record.descrizione = "<descrizione mancante>"
//elseif len(lstr_record.descrizione) > 30 then
//	lstr_record.descrizione = left(lstr_record.descrizione, 30) + "..."
//end if
//
//lstr_record.handle_padre = ll_handle
//lstr_record.tipo_livello = "A"
//lstr_record.livello = il_livello_corrente + 1
//tvi_campo.data = lstr_record
//tvi_campo.label = lstr_record.codice + ", " + lstr_record.descrizione
//
//tvi_campo.pictureindex = 1
//tvi_campo.selectedpictureindex = 1
//tvi_campo.overlaypictureindex = 1
//
//tvi_campo.children = false
//tvi_campo.selected = false
//
//tab_1.page_3.tv_2.insertitemlast(ll_handle, tvi_campo)
end subroutine

public function string wf_leggi_parent (long fl_handle);str_attrezzature_tree lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = fl_handle
if ll_item = 0 then
	return ""
end if

do
	tab_1.page_3.tv_2.getitem(ll_item,ltv_item)
	lstr_item = ltv_item.data
	return lstr_item.tipo_livello	
	ll_item = tab_1.page_3.tv_2.finditem(parenttreeitem!,ll_item)
loop while ll_item <> -1

return ""
end function

public subroutine wf_controlla_tabella_obsoleta ();/**
 * Stefanop
 * 08/02/2010
 *
 * Controlla se all'interno della tabella tab_schede_manutezioni ci sono ancora delle righe.
 * Se si allora l'importazione deve ancora essere fatta.
 **/
 
string ls_message
long ll_count

ls_message  = "Attenzione: sono presenti record della vecchia gestione delle manutenzioni "
ls_message += "procedere l'aggiornamento alla nuova?"

select count(cod_azienda)
into	:ll_count
from	tab_schede_manutenzioni
where	cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode = 100 or (sqlca.sqlcode = 0 and ll_count > 0) then
	if g_mb.messagebox(this.title, ls_message, Question!, YesNo!) = 1 then
		open(w_sistema_schede_manutenzioni)
		event close()
	end if
end if
end subroutine

on w_editor_manutenzioni.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.dw_1=create dw_1
this.p_1=create p_1
this.st_1=create st_1
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.dw_1
this.Control[iCurrent+3]=this.p_1
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.r_1
end on

on w_editor_manutenzioni.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.dw_1)
destroy(this.p_1)
destroy(this.st_1)
destroy(this.r_1)
end on

event resize;call super::resize;tab_1.move(20,20)
tab_1.height = newheight - 50

tab_1.page_1.tv_1.move(0,0)
tab_1.page_2.dw_search.move(0,0)
tab_1.page_3.tv_2.move(0,0)

st_1.x = tab_1.x + tab_1.width
st_1.height = tab_1.height

r_1.move(st_1.x + st_1.width, 20)
dw_1.move(r_1.x + 4, r_1.y + 2)

r_1.resize(newwidth - (st_1.x + st_1.y) - 60, tab_1.height)
dw_1.uof_set_dimension(r_1.width - 6, r_1.height - 6)
end event

event pc_setwindow;call super::pc_setwindow;// Calcolo percorsi utente
try
	f_getuserpath(is_user_temporaly)
	is_user_temporaly += "omnia\editor_documenti\"
	
	if not DirectoryExists(is_user_temporaly) then
		CreateDirectory(is_user_temporaly)
	end if
	
	is_user_documents  = wf_userinfo("HOMEDRIVE")
	is_user_documents += wf_userinfo("HOMEPATH")
	is_user_documents += "\Documenti\"
catch (RuntimeError ex)
	setnull(is_user_temporaly)
	setnull(is_user_documents)
end try
// ---

// -- Nascondo la datawindow
dw_1.visible = false

// -- Imposto icone treeview ricerca
tab_1.page_3.tv_2.deletepictures()
tab_1.page_3.tv_2.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_off.png")
tab_1.page_3.tv_2.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_on.png")
tab_1.page_3.tv_2.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_area.png")
tab_1.page_3.tv_2.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_categorie.png")
tab_1.page_3.tv_2.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_divisioni.png")
tab_1.page_3.tv_2.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_reparti.png")
// ----

tab_1.page_2.dw_search.reset()
tab_1.page_2.dw_search.insertrow(0)
wf_carica_mappe()

event post we_controlla_tabella_obsoleta()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_1.page_2.dw_search,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_1.page_2.dw_search,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_1.page_2.dw_search,"cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(tab_1.page_2.dw_search,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
					  

end event

type tab_1 from tab within w_editor_manutenzioni
event create ( )
event destroy ( )
event resize pbm_size
integer x = 23
integer y = 20
integer width = 1120
integer height = 2200
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
page_1 page_1
page_2 page_2
page_3 page_3
end type

on tab_1.create
this.page_1=create page_1
this.page_2=create page_2
this.page_3=create page_3
this.Control[]={this.page_1,&
this.page_2,&
this.page_3}
end on

on tab_1.destroy
destroy(this.page_1)
destroy(this.page_2)
destroy(this.page_3)
end on

event resize;this.page_1.tv_1.resize(newwidth - 40, newheight - 120)
this.page_2.dw_search.resize(newwidth - 40, newheight - 120)
this.page_3.tv_2.resize(newwidth - 40, newheight - 120)
end event

type page_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 100
integer width = 1083
integer height = 2084
long backcolor = 12632256
string text = "Mappe"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
tv_1 tv_1
end type

on page_1.create
this.tv_1=create tv_1
this.Control[]={this.tv_1}
end on

on page_1.destroy
destroy(this.tv_1)
end on

type tv_1 from treeview within page_1
integer width = 1074
integer height = 2080
integer taborder = 40
string dragicon = "DataWindow5!"
boolean dragauto = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean linesatroot = true
boolean disabledragdrop = false
boolean tooltips = false
string picturename[] = {"Library5!","Tab!","Tab1!"}
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event doubleclicked;if handle < 0 then return -1

treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

this.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

if lstr_data.tipo_nodo = "N" then
	
	il_cod_mappa = lstr_data.cod_mappa
	il_cod_nodo   = lstr_data.cod_nodo
	if dw_1.uof_carica_nodo(lstr_data.cod_mappa, lstr_data.cod_nodo) then
		dw_1.visible = true
	end if
	
end if

end event

event dragwithin;// Bisogna usare una variabile e settarla solo una volta altrimenti se l'ultente
// scorre sopra gli altri nodi della tv l'handle viene cambiato con l'ultimo selezionato.

if il_handle_drag = -1 then
	il_handle_drag = handle
	il_tv_dragging = 1
end if
end event

event endlabeledit;treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

// prevent empty char
if newtext = "" or isnull(newtext) then
	il_handle_rclick = -1
	return -1
end if

tv_1.getitem(il_handle_rclick, ltvi_item)
lstr_data = ltvi_item.data

choose case lstr_data.tipo_nodo
	case "M"
		dw_1.uof_mappa_aggiorna(il_handle_rclick, newtext) 
		
	case "N"
		dw_1.uof_nodo_rinomina(lstr_data.cod_mappa, lstr_data.cod_nodo, newtext) 
end choose

il_handle_rclick = -1
end event

event itempopulate;str_editor_manutenzioni lstr_data
treeviewitem ltvi_item

// prevent invalid handle
if handle < 0 then return -1

tv_1.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

choose case lstr_data.tipo_nodo
	case "M" // mappa
		if wf_carica_nodi(handle, lstr_data.cod_mappa, lstr_data.primo_nodo) < 1 then
			ltvi_item.children = false
			tv_1.setitem(handle, ltvi_item)
		/*else
			dw_1.uof_carica_nodo(lstr_data.cod_mappa, lstr_data.primo_nodo)*/
		end if
		
end choose


end event

event rightclicked;treeviewitem ltvi_item
str_editor_manutenzioni lstr_data
m_editor_manutenzioni lm_menu

// prevent invalid handle
if handle < 0 then return

lm_menu = create m_editor_manutenzioni

if handle = 0 then
	// cliccato sul bianco, probabilmente voglio aggiungere una nuova mappa
	lm_menu.m_mappa_rinomina.visible = false
	lm_menu.m_mappa_elimina.visible = false
	lm_menu.m_sep_0.visible = false
	lm_menu.m_nodo_aggiungi.visible = false
	lm_menu.m_nodo_primo.visible = false
	lm_menu.m_nodo_immagine.visible = false
	lm_menu.m_nodo_rinomina.visible = false
	lm_menu.m_nodo_elimina.visible = false
else
	getitem(handle, ltvi_item)
	lstr_data = ltvi_item.data
	il_handle_rclick = handle
	
	choose case lstr_data.tipo_nodo
		case "M" // Mappa
			lm_menu.m_mappa_aggiungi.visible = false
			lm_menu.m_nodo_primo.visible = false
			lm_menu.m_nodo_immagine.visible = false
			lm_menu.m_nodo_rinomina.visible = false
			lm_menu.m_nodo_elimina.visible = false
			
		case "N" // Nodo
			lm_menu.m_mappa_aggiungi.visible = false
			lm_menu.m_mappa_rinomina.visible = false
			lm_menu.m_mappa_elimina.visible = false
			lm_menu.m_sep_0.visible = false
			lm_menu.m_nodo_aggiungi.visible = false //Per sottonodi eliminare questa riga
			if lstr_data.primo_nodo > 0 then
				lm_menu.m_nodo_primo.visible = false // E' gia un primo nodo
			end if
			
	end choose
end if


lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu
end event

type page_2 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 1083
integer height = 2084
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_cerca cb_cerca
dw_search dw_search
end type

on page_2.create
this.cb_cerca=create cb_cerca
this.dw_search=create dw_search
this.Control[]={this.cb_cerca,&
this.dw_search}
end on

on page_2.destroy
destroy(this.cb_cerca)
destroy(this.dw_search)
end on

type cb_cerca from commandbutton within page_2
integer x = 576
integer y = 1700
integer width = 480
integer height = 100
integer taborder = 170
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;tab_1.page_2.dw_search.accepttext()

// -- Filtro base sql
is_sql_filtro = ""
if not isnull(parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_area_aziendale")) then
	is_sql_filtro += " and cod_area_aziendale = '" + parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_area_aziendale") + "' "
end if

if not isnull(parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_divisione")) and len(parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_divisione")) > 0 then
	is_sql_filtro += " and cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  cod_divisione = '" + parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_divisione") + "' ) "
end if

if not isnull(parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_reparto")) then
	is_sql_filtro += " and cod_reparto = '" + parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_reparto") + "' "
end if

if not isnull(parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_cat_attrezzatura")) then
	is_sql_filtro += " and cod_cat_attrezzature = '" + parent.dw_search.getitemstring(parent.dw_search.getrow(),"cod_cat_attrezzatura") + "' "
end if
// ----

//ib_retrieve = false
//choose case parent.dw_search.getitemstring(1,"flag_eseguito")
//	case "S"
//		is_tipo_manut = "Storico"
//	case "N"
//		is_tipo_manut = "Non Eseguite"
//	case "T"
//		is_tipo_manut = "Tutte"
//end choose
//
//is_tipo_ordinamento = parent.dw_search.getitemstring(1,"flag_ordinamento_dati")
//

// -- Pulisco i vecchi risultati
tab_1.page_3.tv_2.deleteitem(0)

wf_imposta_tv()

tab_1.SelectTab(3)
end event

type dw_search from u_dw_search within page_2
event ue_cerca_attrezzatura ( string as_column )
integer width = 1074
integer height = 1660
integer taborder = 40
string title = "none"
string dataobject = "d_attrezzature_filtro"
boolean border = false
boolean livescroll = true
end type

event ue_cerca_attrezzatura(string as_column);guo_ricerca.uof_ricerca_attrezzatura(dw_search,as_column)
end event

event itemchanged;string ls_null
setnull(ls_null)

choose case dwo.name
	case "cod_attrezzatura_da"
		if data <> "" or not isnull(data) then								  
			setitem(row, "cod_attrezzatura_a", data)
		end if
		
	case "flag_tipo_livello_1"
		setitem(row, "flag_tipo_livello_2", "N")
		setitem(row, "flag_tipo_livello_3", "N")
		setitem(row, "flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_2", 150)
		end if
		
	case "flag_tipo_livello_2"
		setitem(row, "flag_tipo_livello_3", "N")
		setitem(row, "flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_3", 160)
		end if
		
	case "flag_tipo_livello_3"
		setitem(row, "flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_4", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_4", 170)
		end if
		
end choose
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_attrezzatura_da"
//		event ue_cerca_attrezzatura("cod_attrezzatura_da")
		guo_ricerca.uof_ricerca_attrezzatura(dw_search,"cod_attrezzatura_da")

	case "b_attrezzatura_a"
//		event ue_cerca_attrezzatura("cod_attrezzatura_a")
		guo_ricerca.uof_ricerca_attrezzatura(dw_search,"cod_attrezzatura_a")
		
end choose
end event

type page_3 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 1083
integer height = 2084
long backcolor = 12632256
string text = "Risultati"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
tv_2 tv_2
end type

on page_3.create
this.tv_2=create tv_2
this.Control[]={this.tv_2}
end on

on page_3.destroy
destroy(this.tv_2)
end on

type tv_2 from treeview within page_3
integer width = 1074
integer height = 2080
integer taborder = 40
string dragicon = "Form!"
boolean dragauto = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean linesatroot = true
boolean disabledragdrop = false
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event itempopulate;string ls_null
long ll_livello = 0
treeviewitem ltv_item
str_attrezzature_tree lws_record

setnull(ls_null)

if isnull(handle) or handle <= 0 then
	return 0
end if

getitem(handle,ltv_item)

lws_record = ltv_item.data
ll_livello = lws_record.livello

il_livello_corrente = ll_livello

if ll_livello < 4 then
	// controlla cosa c'è al livello successivo ???
	choose case tab_1.page_2.dw_search.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1))
		case "D"
			// caricamento divisioni
			if wf_inserisci_divisioni(handle, tab_1.page_2.dw_search.getitemstring(1, "cod_divisione")) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if
			
		case "R"
			// caricamento reparti
			if wf_inserisci_reparti(handle, tab_1.page_2.dw_search.getitemstring(1, "cod_reparto")) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if
			
		case "C"
			// caricamento categorie
			if wf_inserisci_categorie(handle, tab_1.page_2.dw_search.getitemstring(1, "cod_cat_attrezzatura")) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if
			
		case "E" 
			// caricamento aree
			if wf_inserisci_aree(handle, tab_1.page_2.dw_search.getitemstring(1, "cod_area_aziendale")) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if
			
		case "A", "N"
			// caricamento attrezzature
			if wf_inserisci_attrezzature(handle, tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_da"), tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_a")) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if
			
			
		/*case "A"
			// caricamento attrezzature
			if wf_inserisci_attrezzature(handle, tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura")) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if
			
		case "N"
			// caricamento attrezzature
			if wf_inserisci_attrezzature(handle, ls_null) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if
		*/	
		/*case "N" 
			if wf_inserisci_registrazioni(handle, ls_null) = 1 then
				ltv_item.children = false
				this.setitem(handle, ltv_item)
			end if*/
			
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
else
	// inserisco le registrazioni
	if wf_inserisci_attrezzature(handle, tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_da"), tab_1.page_2.dw_search.getitemstring(1, "cod_attrezzatura_a")) = 1 then
		ltv_item.children = false
		this.setitem(handle, ltv_item)
	end if
	
end if

commit;
end event

event dragwithin;// Bisogna usare una variabile e settarla solo una volta altrimenti se l'ultente
// scorre sopra gli altri nodi della tv l'handle viene cambiato con l'ultimo selezionato.

if il_handle_drag = -1 then
	il_handle_drag = handle
	il_tv_dragging = 2
end if
end event

type dw_1 from uo_editor_manutenzioni within w_editor_manutenzioni
integer x = 1211
integer y = 20
integer width = 2034
integer height = 1640
integer taborder = 20
end type

event dragdrop;call super::dragdrop;string ls_tipo_collegamento, ls_valore
int li_x, li_y, li_cod_nodo
treeviewitem ltvi_item
str_editor_manutenzioni lstr_data
str_attrezzature_tree lstr_data2

if il_handle_drag < 0 then
	return
end if

if il_handle_drag > 0 then
	
	if il_tv_dragging = 1 then
		tab_1.page_1.tv_1.getitem(il_handle_drag, ltvi_item)
		lstr_data = ltvi_item.data
		li_cod_nodo = lstr_data.cod_nodo
		ls_tipo_collegamento = "collegamento"
		//ls_valore = string(li_cod_nodo)
		
	elseif il_tv_dragging = 2 then
		tab_1.page_3.tv_2.getitem(il_handle_drag, ltvi_item)
		lstr_data2 = ltvi_item.data
		
		ls_valore = lstr_data2.codice
		li_cod_nodo = lstr_data2.livello
		
		choose case lstr_data2.tipo_livello
			case "A"
				ls_tipo_collegamento = "scheda"
				
			case "D"
				ls_tipo_collegamento = "divisione"
				
			case "E"
				ls_tipo_collegamento = "area"
				
			case "R"
				ls_tipo_collegamento = "reparto"
		end choose
	else
		return
	end if
	
	
	li_x = PointerX()// - dw_navigatore.x
	li_y = PointerY()// - dw_navigatore.y
	
	if dw_1.uof_aggiungi_collegamento(li_x, li_y, 300, 200, li_cod_nodo, ls_tipo_collegamento, ls_valore) then
		dw_1.uof_draw()
	end if
	
end if

il_handle_drag = -1
end event

event rbuttondown;call super::rbuttondown;m_editor_manutenzioni lm_menu

if left(dwo.name, 2) = "t_" then
	lm_menu = create m_editor_manutenzioni
	
	lm_menu.m_mappa_aggiungi.visible = false
	lm_menu.m_mappa_rinomina.visible = false
	lm_menu.m_mappa_elimina.visible = false
	lm_menu.m_sep_0.visible = false
	lm_menu.m_nodo_aggiungi.visible = false
	lm_menu.m_nodo_primo.visible = false
	lm_menu.m_nodo_immagine.visible = false
	lm_menu.m_nodo_rinomina.visible = false
	lm_menu.m_nodo_elimina.visible = false
	lm_menu.m_elimina_collegamento.visible = true
	
	is_dwo_name = string(dwo.name)
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
end if

destroy lm_menu
end event

type p_1 from picture within w_editor_manutenzioni
boolean visible = false
integer x = 846
integer y = 1540
integer width = 229
integer height = 200
boolean originalsize = true
boolean focusrectangle = false
end type

type st_1 from uo_splitbar within w_editor_manutenzioni
integer x = 1143
integer width = 32
integer height = 2220
end type

event constructor;call super::constructor;uof_register(tab_1, LEFT)
uof_register(dw_1, RIGHT)
end event

event uoe_enddrag;call super::uoe_enddrag;r_1.x += ai_delta_x
r_1.width -= ai_delta_x
end event

type r_1 from rectangle within w_editor_manutenzioni
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 16777215
integer x = 1211
integer y = 1680
integer width = 229
integer height = 200
end type

