﻿$PBExportHeader$w_elenco_attrez.srw
$PBExportComments$Finestra caricamento programmazione manutenzione
forward
global type w_elenco_attrez from w_cs_xx_risposta
end type
type st_conferma from statictext within w_elenco_attrez
end type
type st_elemento from statictext within w_elenco_attrez
end type
type cb_cerca from commandbutton within w_elenco_attrez
end type
type cb_annulla_filtri from commandbutton within w_elenco_attrez
end type
type st_4 from statictext within w_elenco_attrez
end type
type st_3 from statictext within w_elenco_attrez
end type
type st_2 from statictext within w_elenco_attrez
end type
type cbx_tutto from checkbox within w_elenco_attrez
end type
type st_1 from statictext within w_elenco_attrez
end type
type cb_programma from commandbutton within w_elenco_attrez
end type
type cb_annulla from commandbutton within w_elenco_attrez
end type
type cb_scarica from commandbutton within w_elenco_attrez
end type
type dw_elenco_attrez_a from uo_cs_xx_dw within w_elenco_attrez
end type
type dw_sel_elenco_attrez from uo_cs_xx_dw within w_elenco_attrez
end type
type dw_elenco_attrez_da from datawindow within w_elenco_attrez
end type
type dw_ext_prog_manut from uo_cs_xx_dw within w_elenco_attrez
end type
end forward

global type w_elenco_attrez from w_cs_xx_risposta
integer width = 3182
integer height = 2140
string title = "Elenco Apparecchiature"
boolean resizable = false
st_conferma st_conferma
st_elemento st_elemento
cb_cerca cb_cerca
cb_annulla_filtri cb_annulla_filtri
st_4 st_4
st_3 st_3
st_2 st_2
cbx_tutto cbx_tutto
st_1 st_1
cb_programma cb_programma
cb_annulla cb_annulla
cb_scarica cb_scarica
dw_elenco_attrez_a dw_elenco_attrez_a
dw_sel_elenco_attrez dw_sel_elenco_attrez
dw_elenco_attrez_da dw_elenco_attrez_da
dw_ext_prog_manut dw_ext_prog_manut
end type
global w_elenco_attrez w_elenco_attrez

type variables
datastore ids_tipi_manutenzioni
string    is_flag_gestione_manut
end variables

forward prototypes
public function integer wf_programmazione_semplice ()
public function integer wf_programmazione_periodo ()
public function integer wf_carica ()
end prototypes

public function integer wf_programmazione_semplice ();long     ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, &
	      ll_anno_registrazione, ll_num_registrazione, ll_ore, ll_minuti, ll_prog_tipo_manutenzione, ll_num_protocollo
			
string   ls_flag_gest_manutenzione, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
	      ls_flag_ricambio_codificato, ls_cod_ricambio, ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, &
		   ls_cod_primario, ls_cod_misura, ls_flag_manutenzione, ls_messaggio, ls_note_idl, ls_descrizione, ls_des_blob, &
			ls_path_documento, ls_data, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
			ls_cod_risorsa_esterna, ls_flag_scadenze, ls_pubblica_intranet, ls_flag_abr
			
dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean  lb_inserimento

datetime ldt_tempo_previsto, ldt_data, ldt_data_1, ldt_data_creazione_programma

blob lbb_blob

uo_manutenzioni luo_manutenzioni

long     ll_risp, ll_giorni, ll_mesi, ll_anni

string   ls_tipo_data

dec{0}	ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

	
ls_data = string(dw_ext_prog_manut.getitemdatetime(1, "data_programmazione"), "dd/mm/yyyy")

if isnull(ls_data) or ls_data = "" then
	g_mb.messagebox("Manutenzioni","Attenzione! Inserire una data valida!")
	return 0
end if

ldt_data = datetime(date(ls_data),00:00:00)

ls_tipo_data = dw_ext_prog_manut.getitemstring(1, "tipo_programmazione")

if ls_tipo_data = "" or isnull(ls_tipo_data) then	
	g_mb.messagebox("Manutenzioni","Attenzione! Scegliere il tipo di uso della data!")
	return 0	
end if

ls_flag_scadenze = dw_ext_prog_manut.getitemstring(1, "flag_scadenze")

	
setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT  prog_tipo_manutenzione,   
		  descrizione,   
		  num_protocollo,   
		  des_blob,   
		  path_documento  
FROM    det_tipi_manutenzioni  
WHERE   cod_azienda = :s_cs_xx.cod_azienda and
		  cod_attrezzatura = :ls_cod_attrezzatura and
		  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni
lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';
		 
if sqlca.sqlcode <> 0 then	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if	
	
for ll_i = 1 to ids_tipi_manutenzioni.rowcount()
	
	ls_cod_attrezzatura = ids_tipi_manutenzioni.getitemstring(ll_i, "cod_attrezzatura")
		
	ls_cod_tipo_manutenzione = ids_tipi_manutenzioni.getitemstring(ll_i, "cod_tipo_manutenzione")
		
	select count(anno_registrazione) 
	into   :ll_count
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
		
	if not isnull(ll_count) and ll_count > 0 then
		
		select count(*)
		into   :ll_count
		from	 manutenzioni,
				 programmi_manutenzione
		where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
				 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
				 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
				 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
				 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
				 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
				 manutenzioni.flag_eseguito = 'N';
					 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
			setpointer(arrow!)
			return -1
		end if
			
		if not isnull(ll_count) and ll_count > 0 then continue
			
	end if
	//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
	//******modifica fatta per abor specifica stampi
	select flag
	into :ls_flag_abr
	from parametri 
	where cod_parametro = 'ABR' and
			flag_parametro = 'F';
	
	//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
	//che tra l'altro è una personalizzazione del cliente ABOR
	if sqlca.sqlcode <> 0 then
		ls_flag_abr = "N"
	end if
	/*
	if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
			setpointer(arrow!)
			return -1
	end if
	*/
	//fine modifica ---------------------------------------------------------------------
	
	if ls_flag_abr = 'S' then
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2,
				 flag_pubblica_intranet
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2,
				 :ls_pubblica_intranet
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda	and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if				
	else
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda	and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if			
	end if
	ll_ore = hour(time(ldt_tempo_previsto))		
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
			 anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
	
	// *** 26/10/2004 seleziono lista di distribuzione
	
	select cod_tipo_lista_dist,
	       cod_lista_dist 
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			 cod_attrezzatura = :ls_cod_attrezzatura;
			 
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
	
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
	
	// ***
	if ls_flag_abr = 'S' then	
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2,
														flag_pubblica_intranet)														
		values	                         (:s_cs_xx.cod_azienda,
													  :ll_anno_registrazione,
													  :ll_num_registrazione,
													  :ldt_data_creazione_programma,
													  :ls_cod_attrezzatura,
													  :ls_cod_tipo_manutenzione,
													  :ls_flag_manutenzione,
													  :ls_flag_ricambio_codificato,
													  :ls_cod_ricambio,
													  :ls_cod_ricambio_alternativo,
													  :ls_des_ricambio_non_codificato,
													  :ll_num_reg_lista,
													  :ll_num_versione,
													  :ll_num_edizione,
													  :ls_periodicita,
													  :ll_frequenza,
													  :ld_quan_ricambio,
													  :ld_costo_unitario_ricambio,
													  :ls_cod_primario,
														:ls_cod_misura,
														:ld_val_min_taratura,
														:ld_val_max_taratura,
														:ld_valore_atteso,
														'N',
														:ls_note_idl,
														:ll_ore,
														:ll_minuti,
														:ls_cod_tipo_lista_dist,
														:ls_cod_lista_dist,
														:ls_cod_operaio,
														:ls_cod_cat_risorse_esterne,
														:ls_cod_risorsa_esterna,
														:ld_data_inizio_val_1,
														:ld_data_fine_val_1,
														:ld_data_inizio_val_2,
														:ld_data_fine_val_2,
														:ls_pubblica_intranet);
															 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	else
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2)														
		values	                         (:s_cs_xx.cod_azienda,
													  :ll_anno_registrazione,
													  :ll_num_registrazione,
													  :ldt_data_creazione_programma,
													  :ls_cod_attrezzatura,
													  :ls_cod_tipo_manutenzione,
													  :ls_flag_manutenzione,
													  :ls_flag_ricambio_codificato,
													  :ls_cod_ricambio,
													  :ls_cod_ricambio_alternativo,
													  :ls_des_ricambio_non_codificato,
													  :ll_num_reg_lista,
													  :ll_num_versione,
													  :ll_num_edizione,
													  :ls_periodicita,
													  :ll_frequenza,
													  :ld_quan_ricambio,
													  :ld_costo_unitario_ricambio,
													  :ls_cod_primario,
														:ls_cod_misura,
														:ld_val_min_taratura,
														:ld_val_max_taratura,
														:ld_valore_atteso,
														'N',
														:ls_note_idl,
														:ll_ore,
														:ll_minuti,
														:ls_cod_tipo_lista_dist,
														:ls_cod_lista_dist,
														:ls_cod_operaio,
														:ls_cod_cat_risorse_esterne,
														:ls_cod_risorsa_esterna,
														:ld_data_inizio_val_1,
														:ld_data_fine_val_1,
														:ld_data_inizio_val_2,
														:ld_data_fine_val_2);
															 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	end if
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio )  
	select cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_ricambio,   
			cod_prodotto,   
			des_prodotto,   
			quan_utilizzo,   
			prezzo_ricambio  
	 from tipi_manutenzioni_ricambi
	 where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	do while 1=1
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
		if sqlca.sqlcode = 100 then exit
			
		INSERT INTO det_prog_manutenzioni  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  progressivo,   
				  descrizione,   
				  num_protocollo,   
				  flag_blocco,   
				  data_blocco,   
				  des_blob,   
				  path_documento )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ll_prog_tipo_manutenzione,   
				  :ls_descrizione,   
				  :ll_num_protocollo,   
				  'N',   
				  null,   
				  :ls_des_blob,   
				  :ls_path_documento )  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if

		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
			        cod_attrezzatura = :ls_cod_attrezzatura and
				     cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in ricerca (select) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
		
	lb_inserimento = true
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if
					
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "B"     // battute stampi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if				
				
			case "O"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
			case "M"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
			case else
				
				update programmi_manutenzione
				set    flag_scadenze = :ls_flag_scadenze
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
				if ls_flag_scadenze = "S" then

					if luo_manutenzioni.uof_crea_manutenzione_programmata(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
						g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
						rollback;
						setpointer(arrow!)
						return -1
					end if
					
					// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
					int mese1_i, mese1_f, mese2_i, mese2_f, giorno1_i, giorno1_f, giorno2_i, giorno2_f
										
					giorno1_i = ( ld_data_inizio_val_1 / 100)
					mese1_i = mod( ld_data_inizio_val_1, 100)
										
					giorno1_f = ( ld_data_fine_val_1 / 100)
					mese1_f = mod( ld_data_fine_val_1, 100)
					
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
					
						if month(date(ldt_data)) < mese1_i or month(date(ldt_data)) > mese1_f then
							ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data)) < giorno1_i or day(date(ldt_data)) > giorno1_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
							end if
						end if
					
					else										
						giorno2_i = ( ld_data_inizio_val_2 / 100)
						mese2_i = mod( ld_data_inizio_val_2, 100)
											
						giorno2_f = ( ld_data_fine_val_2 / 100)
						mese2_f = mod( ld_data_fine_val_2, 100)							
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data)) < mese2_i or month(date(ldt_data)) > mese2_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data)) < giorno2_i or day(date(ldt_data)) > giorno2_f then
									ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
								end if
							end if												
						end if
					end if
						
					// controllo di che tipo è la data: se è la data della prima scadenza ... allora devo cambiare la data della 
					// registrazione, altrimenti, a partire da quella data, trovo la data della prossima scadenza
		
					if ls_tipo_data <> "Data prima scadenza" then					
						ldt_data_1 = luo_manutenzioni.uof_prossima_scadenza( ldt_data, ls_periodicita, ll_frequenza)					
					else
						ldt_data_1 = ldt_data
					end if						
					
					// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
					
						if month(date(ldt_data_1)) < mese1_i or month(date(ldt_data_1)) > mese1_f then
							ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data_1)) < giorno1_i or day(date(ldt_data_1)) > giorno1_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
							end if
						end if
					
					else										
						
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data_1)) < mese2_i or month(date(ldt_data_1)) > mese2_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data_1)) < giorno2_i or day(date(ldt_data_1)) > giorno2_f then
									ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
								end if
							end if												
						end if
					end if					
					
					update manutenzioni
					set    data_registrazione = :ldt_data_1
					where  cod_azienda = :s_cs_xx.cod_azienda and    
							 anno_reg_programma = :ll_anno_registrazione	and    
							 num_reg_programma = :ll_num_registrazione;
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Manutenzioni","Errore in aggiornamento data registrazione! " + sqlca.sqlerrtext)
						rollback;
						setpointer(arrow!)
						return -1
					end if
				end if
				
		end choose		
	end if	
next 
	 
destroy luo_manutenzioni
	
commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	g_mb.messagebox("Omnia", "Programmazione avvenuta con successo!~n~rAttenzione la finestra Programmi Manutenzione verrà chiusa per permettere l'aggiornamento della stessa")
	close(w_elenco_attrez)
end if

return 0
end function

public function integer wf_programmazione_periodo ();datetime ldt_da_data, ldt_a_data, ldt_tempo_previsto, ldt_data_1, ldt_data, ldt_scadenze[], ldt_data_registrazione, &
         ldt_data_prossimo_intervento, ldt_null, ldt_max_data_registrazione, ldt_data_creazione_programma, &
			ldt_scadenze_vuoto[]

string   ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_gest_manutenzione, ls_flag_ricambio_codificato, ls_cod_ricambio, &
         ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, ls_cod_primario, ls_cod_misura, &
			ls_flag_manutenzione, ls_note_idl, ls_des_blob, ls_path_documento, ls_descrizione, ls_messaggio, ls_flag_scadenze_esistenti, &
			ls_cod_tipo_lista_dist, ls_cod_lista_dist,ls_cod_operaio,ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna, ls_pubblica_intranet, ls_flag_abr
			
long     ll_anno_registrazione, ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, ll_ore, ll_minuti, &
         ll_num_registrazione, ll_prog_tipo_manutenzione, ll_num_protocollo, ll_num_programmazioni, ll_ii

dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean  lb_inserimento

dec{0}   ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

uo_manutenzioni luo_manutenzioni

blob     lbb_blob

setnull(ldt_null)

ls_flag_scadenze_esistenti = dw_ext_prog_manut.getitemstring( 1, "flag_scadenze_esistenti")

ldt_da_data = dw_ext_prog_manut.getitemdatetime( 1, "da_data")

ldt_a_data = dw_ext_prog_manut.getitemdatetime( 1, "a_data")

if isnull(ldt_da_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data inizio programmazione!")
	return -1
end if

if isnull(ldt_a_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data fine programmazione!")
	return -1	
end if

if ldt_da_data > ldt_a_data then
	g_mb.messagebox("OMNIA", "Attenzione: Controllare le date del periodo di programmazione!")
	return -1	
end if

setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT prog_tipo_manutenzione,   
		 descrizione,   
		 num_protocollo,   
		 des_blob,   
		 path_documento  
FROM   det_tipi_manutenzioni  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni

lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then	
	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 

	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
if sqlca.sqlcode = 100 then
	
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if	

//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------


ll_num_programmazioni = ids_tipi_manutenzioni.rowcount()
	
for ll_i = 1 to ll_num_programmazioni
	
	ls_cod_attrezzatura = ids_tipi_manutenzioni.getitemstring(ll_i, "cod_attrezzatura")
	
	ls_cod_tipo_manutenzione = ids_tipi_manutenzioni.getitemstring(ll_i, "cod_tipo_manutenzione")
	
	st_conferma.text = ls_cod_attrezzatura + " - " + ls_cod_tipo_manutenzione

	if ls_flag_abr = 'S' then
	
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2,
				 flag_pubblica_intranet
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2,
				 :ls_pubblica_intranet
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
			
		end if				
	else
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
			
	end if	
end if

	ll_ore = hour(time(ldt_tempo_previsto))		
	
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then

		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)	
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
	
	// *** 26/10/2004 seleziono lista di distribuzione
	
	select cod_tipo_lista_dist,
	       cod_lista_dist 
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			 cod_attrezzatura = :ls_cod_attrezzatura;
			 
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
	
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
	
	// ***		
	if ls_flag_abr = 'S' then
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2,
														flag_pubblica_intranet)
											  values	(:s_cs_xx.cod_azienda,
														 :ll_anno_registrazione,
														 :ll_num_registrazione,
														 :ldt_data_creazione_programma,
														 :ls_cod_attrezzatura,
														 :ls_cod_tipo_manutenzione,
														 :ls_flag_manutenzione,
														 :ls_flag_ricambio_codificato,
														 :ls_cod_ricambio,
														 :ls_cod_ricambio_alternativo,
														 :ls_des_ricambio_non_codificato,
														 :ll_num_reg_lista,
														 :ll_num_versione,
														 :ll_num_edizione,
														 :ls_periodicita,
														 :ll_frequenza,
														 :ld_quan_ricambio,
														 :ld_costo_unitario_ricambio,
														 :ls_cod_primario,
														 :ls_cod_misura,
														 :ld_val_min_taratura,
														 :ld_val_max_taratura,
														 :ld_valore_atteso,
														 'N',
														 :ls_note_idl,
														 :ll_ore,
														 :ll_minuti,
														 :ls_cod_tipo_lista_dist,
														 :ls_cod_lista_dist,
														 :ls_cod_operaio,
														 :ls_cod_cat_risorse_esterne,
														 :ls_cod_risorsa_esterna,
														 :ld_data_inizio_val_1,
														 :ld_data_fine_val_1,
														 :ld_data_inizio_val_2,
														 :ld_data_fine_val_2,
														 :ls_pubblica_intranet);
														 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	else
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2)
											  values	(:s_cs_xx.cod_azienda,
														 :ll_anno_registrazione,
														 :ll_num_registrazione,
														 :ldt_data_creazione_programma,
														 :ls_cod_attrezzatura,
														 :ls_cod_tipo_manutenzione,
														 :ls_flag_manutenzione,
														 :ls_flag_ricambio_codificato,
														 :ls_cod_ricambio,
														 :ls_cod_ricambio_alternativo,
														 :ls_des_ricambio_non_codificato,
														 :ll_num_reg_lista,
														 :ll_num_versione,
														 :ll_num_edizione,
														 :ls_periodicita,
														 :ll_frequenza,
														 :ld_quan_ricambio,
														 :ld_costo_unitario_ricambio,
														 :ls_cod_primario,
														 :ls_cod_misura,
														 :ld_val_min_taratura,
														 :ld_val_max_taratura,
														 :ld_valore_atteso,
														 'N',
														 :ls_note_idl,
														 :ll_ore,
														 :ll_minuti,
														 :ls_cod_tipo_lista_dist,
														 :ls_cod_lista_dist,
														 :ls_cod_operaio,
														 :ls_cod_cat_risorse_esterne,
														 :ls_cod_risorsa_esterna,
														 :ld_data_inizio_val_1,
														 :ld_data_fine_val_1,
														 :ld_data_inizio_val_2,
														 :ld_data_fine_val_2);
														 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	end if
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio )  
	select cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_ricambio,   
			cod_prodotto,   
			des_prodotto,   
			quan_utilizzo,   
			prezzo_ricambio  
	 from tipi_manutenzioni_ricambi
	 where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		
		g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if
	
	do while 1=1
		
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento;
										
		if sqlca.sqlcode = -1 then
			
			g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		INSERT INTO det_prog_manutenzioni  
					 ( cod_azienda,   
					   anno_registrazione,   
					   num_registrazione,   
					   progressivo,   
					   descrizione,   
					   num_protocollo,   
					   flag_blocco,   
					   data_blocco,   
					   des_blob,   
					   path_documento )  
		VALUES   ( :s_cs_xx.cod_azienda,   
					  :ll_anno_registrazione,   
					  :ll_num_registrazione,   
					  :ll_prog_tipo_manutenzione,   
					  :ls_descrizione,   
					  :ll_num_protocollo,   
					  'N',   
					  null,   
					  :ls_des_blob,   
					  :ls_path_documento )  ;
					 
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
	
		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_attrezzatura = :ls_cod_attrezzatura and
					  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in ricerca (select) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
		
	lb_inserimento = true
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if
				
	// ** GESTIONE SEMPLICE DELLE MANUTENZIONI
	
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "B"     // battute stampi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
			case "O"  // ** ore
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
			case "M" // ** minuti
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
			case else // ** G = giorni, S = settimane, E = mesi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
						 anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
				// *** programmazione periodo funzione 1: trovo il vettore con le date delle scadenze
				
				// *** controllo se esistono programmi di manutenzione
				select count(anno_registrazione) 
				into   :ll_count
				from   programmi_manutenzione
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_attrezzatura = :ls_cod_attrezzatura and 
						 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
						
				if sqlca.sqlcode < 0 then			
					g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1					
				end if
					
				// controllo se esistono scadenze da confermare
				if not isnull(ll_count) and ll_count > 0 then
						
					select count(*)
					into   :ll_count
					from	 manutenzioni,
							 programmi_manutenzione
					where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
							 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
							 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
							 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
							 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
							 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
							 manutenzioni.flag_eseguito = 'N';
							 
					if sqlca.sqlcode < 0 then
						
						g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
						setpointer(arrow!)
						return -1
						
					end if
					
					// se ci sono scadenze da confermare allora guardo il flag
					if not isnull(ll_count) and ll_count > 0 then
						
						// *** flag = 'S' allora seleziono la massima scadenza e faccio partire il periodo di programmazione
						//     dalla prossima scadenza a partire da quella data di registrazione
						if ls_flag_scadenze_esistenti = 'S' then
							
							select MAX(data_registrazione)
							into   :ldt_max_data_registrazione
							from	 manutenzioni,
									 programmi_manutenzione
							where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
									 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
									 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
									 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
									 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
									 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
									 manutenzioni.flag_eseguito = 'N';
									 
							if sqlca.sqlcode < 0 then
								
								g_mb.messagebox("Omnia","Errore in controllo data manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
								setpointer(arrow!)
								return -1
								
							end if
						
							if not isnull(ldt_max_data_registrazione) then ldt_max_data_registrazione = luo_manutenzioni.uof_prossima_scadenza( ldt_max_data_registrazione, ls_periodicita, ll_frequenza )								
							
							if not isnull(ldt_max_data_registrazione) then ldt_da_data = ldt_max_data_registrazione
							//--------------------CLAUDIA 21/01/08
							// Se ci sono manutenzioni non eseguite e magari create in modalità semplice, modifico il programma di manutenzione impostando il flag_scadenze a no così diventa programmata e
							// quando la eseguo non chiede la manitenzione successiva
							update programmi_manutenzione
							set flag_scadenze = 'N'
							where programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
									 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
									 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione ;
							if sqlca.sqlcode < 0 then
								
								g_mb.messagebox("Omnia","Errore in modifica tipologia manutenzione codice: "+ls_cod_tipo_manutenzione +" " + sqlca.sqlerrtext,stopsign!)
								setpointer(arrow!)
								return -1
								
							end if
							
							
							//--------FINE CLAUDIA 21/01/08
						else
						// *** cancello le scadenze ancora da confermare e continuo con il periodo scritto 
						//     dall'operatore
							delete from	 manutenzioni
							where  manutenzioni.cod_azienda = :s_cs_xx.cod_azienda and
									 manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura and
									 manutenzioni.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
									 manutenzioni.flag_eseguito = 'N' and
									 manutenzioni.anno_reg_programma IS NOT NULL and
									 manutenzioni.num_reg_programma IS NOT NULL;		
									 
							if sqlca.sqlcode < 0 then
								
								g_mb.messagebox("Omnia","Errore in cancellazione manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
								rollback;
								setpointer(arrow!)
								return -1
								
							end if									 
														
						end if
					end if
						
				end if
	
				// ***
				
				ldt_scadenze[] = ldt_scadenze_vuoto[]
				
				luo_manutenzioni.uof_scadenze_periodo( ldt_da_data , date(ldt_da_data), date(ldt_a_data), ls_periodicita, ll_frequenza, ldt_scadenze)
				
				if UpperBound(ldt_scadenze) < 1 then continue
				
				for ll_ii = 1 to UpperBound(ldt_scadenze)
					
					ldt_data_registrazione = ldt_scadenze[ll_ii]
					
					if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(ll_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, ls_messaggio) <> 0 then
							g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
							rollback;
							setpointer(arrow!)
							return -1
					end if
					
				next
				
				// ***

		end choose		
	end if	
		
next 
	 
destroy luo_manutenzioni
	
commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	
	g_mb.messagebox("Omnia", "Programmazione avvenuta con successo!~n~rAttenzione la finestra Programmi Manutenzione verrà chiusa per permettere l'aggiornamento della stessa")
	close(w_elenco_attrez)
	
end if
	
return 0
end function

public function integer wf_carica ();long 	 ll_riga, ll_cont_1, ll_cont_2

string ls_prog, ls_non_prog, ls_parziali, ls_cod_reparto, ls_cod_categoria, ls_cod_area, ls_attr_da, &
		 ls_attr_a, ls_cod_attrezzatura, ls_des_attrezzatura, ls_tipo_man_1, ls_tipo_man_2, ls_tipo_man_3, &
		 ls_sql, ls_flag_programmata, ls_tipo_man_4, ls_tipo_man_5


dw_sel_elenco_attrez.accepttext()
cbx_tutto.checked = false
cb_scarica.enabled = true
dw_elenco_attrez_da.reset()

dw_elenco_attrez_a.reset()

dw_elenco_attrez_a.insertrow(0)

dw_elenco_attrez_da.setredraw(false)

ls_prog = dw_sel_elenco_attrez.getitemstring(1,"flag_prog")

ls_non_prog = dw_sel_elenco_attrez.getitemstring(1,"flag_non_prog")

ls_parziali = dw_sel_elenco_attrez.getitemstring(1,"flag_parziali")

ls_cod_reparto = dw_sel_elenco_attrez.getitemstring(1,"cod_reparto")

ls_cod_categoria = dw_sel_elenco_attrez.getitemstring(1,"cod_categoria")

ls_cod_area = dw_sel_elenco_attrez.getitemstring(1,"cod_area")

ls_attr_da = dw_sel_elenco_attrez.getitemstring(1,"cod_attr_da")

ls_attr_a = dw_sel_elenco_attrez.getitemstring(1,"cod_attr_a")

select stringa
into   :ls_tipo_man_1
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM1';
		 
if sqlca.sqlcode <> 0 then
	ls_tipo_man_1 = "XXXXXXXXXX"
end if	
		 
select stringa
into   :ls_tipo_man_2
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM2';
		 
if sqlca.sqlcode <> 0 then
	ls_tipo_man_2 = "XXXXXXXXXX"
end if	
		 
select stringa
into   :ls_tipo_man_3
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM3';
		 
if sqlca.sqlcode <> 0 then
	ls_tipo_man_3 = "XXXXXXXXXX"
end if	

select stringa
into   :ls_tipo_man_4
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM4';
		 
if sqlca.sqlcode <> 0 then
	ls_tipo_man_4 = "XXXXXXXXXX"
end if

select stringa
into   :ls_tipo_man_5
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM5';
		 
if sqlca.sqlcode <> 0 then
	ls_tipo_man_5 = "XXXXXXXXXX"
end if

ls_sql = &
"select cod_attrezzatura, " + &
"  	  descrizione " + &
"from   anag_attrezzature " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  (flag_blocco = 'N' or data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "') "

if not isnull(ls_cod_reparto) then
	ls_sql = ls_sql + " and cod_reparto = '" + ls_cod_reparto + "' "
end if

if not isnull(ls_cod_categoria) then
	ls_sql = ls_sql + " and cod_cat_attrezzature = '" + ls_cod_categoria + "' "
end if

if not isnull(ls_cod_area) then
	ls_sql = ls_sql + " and cod_area_aziendale = '" + ls_cod_area + "' "
end if

if not isnull(ls_attr_da) then
	ls_sql = ls_sql + " and cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_sql = ls_sql + " and cod_attrezzatura <= '" + ls_attr_a + "' "
end if

ls_sql = ls_sql + " order by cod_attrezzatura "

declare cu_attrezzature dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_attrezzature;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in apertura cursore cu_attrezzature " + sqlca.sqlerrtext)
	dw_elenco_attrez_da.setredraw(true)
	return -1
end if

do while true
	
	fetch cu_attrezzature
	into  :ls_cod_attrezzatura,
			:ls_des_attrezzatura;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella anag_attrezzature " + sqlca.sqlerrtext)
		close cu_attrezzature;
		dw_elenco_attrez_da.setredraw(true)
		return -1
	elseif sqlca.sqlcode = 100 then
		close cu_attrezzature;
		exit
	end if
	
	st_elemento.text = ls_cod_attrezzatura + " - " + ls_des_attrezzatura
	
	ll_cont_1 = 0
	
	ll_cont_2 = 0
	

	select count(*) 
	into   :ll_cont_1
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda  and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione NOT IN ( :ls_tipo_man_1,
			                                :ls_tipo_man_2,
			                                :ls_tipo_man_3,
													  :ls_tipo_man_4,
													  :ls_tipo_man_5 ) and
			 (flag_blocco is null or flag_blocco = 'N');
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in conteggio tipi manutenzioni: " + sqlca.sqlerrtext)
		close cu_attrezzature;
		rollback;
		dw_elenco_attrez_da.setredraw(true)
		return -1
	end if
	
	if ll_cont_1 > 0 then
	
		// *** michela 26/08/04: se non esiste alcun programma associato all'attrezzatura 
		//                       la segno direttamente di rosso in quanto nessuna tipologia è in programmazione.
		
		select count(*)
		into   :ll_cont_2
		from   programmi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda  and
				 cod_attrezzatura = :ls_cod_attrezzatura;
				 
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox("OMNIA","Errore in conteggio programmi: " + sqlca.sqlerrtext)
			
			close cu_attrezzature;
			
			rollback;
			
			dw_elenco_attrez_da.setredraw(true)
			
			return -1
			
		end if		
	
		if isnull(ll_cont_2) then ll_cont_2 = 0
	
		if ll_cont_2 > 0 then
	
			select count(*)
			into   :ll_cont_2
			from   tab_tipi_manutenzione
			where  cod_azienda = :s_cs_xx.cod_azienda  and
					 cod_attrezzatura = :ls_cod_attrezzatura and
					 cod_tipo_manutenzione in (select cod_tipo_manutenzione
														from   programmi_manutenzione
														where  cod_azienda = :s_cs_xx.cod_azienda  and
																 cod_attrezzatura = :ls_cod_attrezzatura) and
					 cod_tipo_manutenzione not in (:ls_tipo_man_1, 
															 :ls_tipo_man_2,
															 :ls_tipo_man_3,
															 :ls_tipo_man_4,
															 :ls_tipo_man_5) and
					(flag_blocco is null or flag_blocco = 'N');
			
			if sqlca.sqlcode < 0 then
				
				g_mb.messagebox("OMNIA","Errore in conteggio tipologie non programmate: " + sqlca.sqlerrtext)
				
				close cu_attrezzature;
				
				rollback;
				
				dw_elenco_attrez_da.setredraw(true)
				
				return -1
				
			end if
		
		end if
		
	end if
	
	if ll_cont_1 = 0 then
		ls_flag_programmata = "N"
		if ls_non_prog = "N" then continue
	else
		if ll_cont_2 = 0 then
			ls_flag_programmata = "N"
			if ls_non_prog = "N" then continue
		else
			if ll_cont_1 = ll_cont_2 then
				ls_flag_programmata = "S"
				if ls_prog = "N" then continue
			else
				ls_flag_programmata = "X"
				if ls_parziali = "N" then continue
			end if
		end if
	end if


	ll_riga = dw_elenco_attrez_da.insertrow(0)
	
	dw_elenco_attrez_da.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
	
	dw_elenco_attrez_da.setitem(ll_riga, "des_attrezzatura", ls_des_attrezzatura)
	
	dw_elenco_attrez_da.setitem(ll_riga, "flag_programmata", ls_flag_programmata)
	
loop

ids_tipi_manutenzioni = create datastore

ids_tipi_manutenzioni.dataobject = 'd_elenco_attrez_ds'

ids_tipi_manutenzioni.settransobject(sqlca)

ids_tipi_manutenzioni.reset()

dw_elenco_attrez_da.setredraw(true)

return 0
end function

on w_elenco_attrez.create
int iCurrent
call super::create
this.st_conferma=create st_conferma
this.st_elemento=create st_elemento
this.cb_cerca=create cb_cerca
this.cb_annulla_filtri=create cb_annulla_filtri
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.cbx_tutto=create cbx_tutto
this.st_1=create st_1
this.cb_programma=create cb_programma
this.cb_annulla=create cb_annulla
this.cb_scarica=create cb_scarica
this.dw_elenco_attrez_a=create dw_elenco_attrez_a
this.dw_sel_elenco_attrez=create dw_sel_elenco_attrez
this.dw_elenco_attrez_da=create dw_elenco_attrez_da
this.dw_ext_prog_manut=create dw_ext_prog_manut
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_conferma
this.Control[iCurrent+2]=this.st_elemento
this.Control[iCurrent+3]=this.cb_cerca
this.Control[iCurrent+4]=this.cb_annulla_filtri
this.Control[iCurrent+5]=this.st_4
this.Control[iCurrent+6]=this.st_3
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.cbx_tutto
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.cb_programma
this.Control[iCurrent+11]=this.cb_annulla
this.Control[iCurrent+12]=this.cb_scarica
this.Control[iCurrent+13]=this.dw_elenco_attrez_a
this.Control[iCurrent+14]=this.dw_sel_elenco_attrez
this.Control[iCurrent+15]=this.dw_elenco_attrez_da
this.Control[iCurrent+16]=this.dw_ext_prog_manut
end on

on w_elenco_attrez.destroy
call super::destroy
destroy(this.st_conferma)
destroy(this.st_elemento)
destroy(this.cb_cerca)
destroy(this.cb_annulla_filtri)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cbx_tutto)
destroy(this.st_1)
destroy(this.cb_programma)
destroy(this.cb_annulla)
destroy(this.cb_scarica)
destroy(this.dw_elenco_attrez_a)
destroy(this.dw_sel_elenco_attrez)
destroy(this.dw_elenco_attrez_da)
destroy(this.dw_ext_prog_manut)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_elenco_attrez_a.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
									 c_noretrieveonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
dw_sel_elenco_attrez.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
									 c_newonopen + &
									 c_noretrieveonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
cb_annulla_filtri.postevent("clicked")




dw_ext_prog_manut.set_dw_options(sqlca, &
                   pcca.null_object, &
                   c_nomodify + &
                   c_nodelete + &
	   				 c_newonopen + &
						 c_noretrieveonopen + &
                   c_disableCC, &
                   c_noresizedw + &
                   c_nohighlightselected + &
                   c_cursorrowpointer)
end event

event pc_close;destroy ids_tipi_manutenzioni
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_elenco_attrez,"cod_reparto",sqlca,"anag_reparti","cod_reparto", &
					  "des_reparto","cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_elenco_attrez,"cod_categoria",sqlca,"tab_cat_attrezzature","cod_cat_attrezzature", &
					  "des_cat_attrezzature","cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")

//f_po_loaddddw_dw(dw_sel_elenco_attrez,"cod_area",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
//					  "des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_sort(dw_sel_elenco_attrez,"cod_area",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")					  
end event

event open;call super::open;// S = gestione semplice
// A = gestione avanzata
// P = gestione periodica

//string ls_flag_tipo_gestione_manutenzioni
//
//select flag_gest_manutenzione
//into   :ls_flag_tipo_gestione_manutenzioni
//from   parametri_manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda;
//
//if isnull(ls_flag_tipo_gestione_manutenzioni) or ls_flag_tipo_gestione_manutenzioni = "" then ls_flag_tipo_gestione_manutenzioni = "S"
//
//is_flag_gestione_manut = ls_flag_tipo_gestione_manutenzioni
//if dw_ext_prog_manut.rowcount() > 0 then
//	dw_ext_prog_manut.setitem( 1, "flag_tipo_gestione", is_flag_gestione_manut)
//end if
end event

type st_conferma from statictext within w_elenco_attrez
integer x = 23
integer y = 1980
integer width = 2057
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_elemento from statictext within w_elenco_attrez
integer x = 91
integer y = 460
integer width = 2263
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_cerca from commandbutton within w_elenco_attrez
integer x = 2766
integer y = 460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;setpointer(hourglass!)

wf_carica()

st_elemento.text = ""
	
setpointer(arrow!)
end event

type cb_annulla_filtri from commandbutton within w_elenco_attrez
integer x = 2377
integer y = 460
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null


setnull(ls_null)

dw_sel_elenco_attrez.setitem(1,"flag_prog","N")

dw_sel_elenco_attrez.setitem(1,"flag_parziali","S")

dw_sel_elenco_attrez.setitem(1,"flag_non_prog","S")

dw_sel_elenco_attrez.setitem(1,"cod_reparto",ls_null)

dw_sel_elenco_attrez.setitem(1,"cod_categoria",ls_null)

dw_sel_elenco_attrez.setitem(1,"cod_area",ls_null)

dw_sel_elenco_attrez.setitem(1,"cod_attr_da",ls_null)

dw_sel_elenco_attrez.setitem(1,"cod_attr_a",ls_null)


end event

type st_4 from statictext within w_elenco_attrez
integer x = 2171
integer y = 1720
integer width = 983
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 65280
string text = "COMPLETAMENTE PROGRAMMATA"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_elenco_attrez
integer x = 1074
integer y = 1720
integer width = 1029
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 65535
string text = "PARZIALMENTE PROGRAMMATA"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_elenco_attrez
integer x = 23
integer y = 1720
integer width = 983
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 255
string text = "NON  PROGRAMMATA"
alignment alignment = center!
boolean focusrectangle = false
end type

type cbx_tutto from checkbox within w_elenco_attrez
integer x = 603
integer y = 1796
integer width = 73
integer height = 76
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean lefttext = true
end type

event clicked;string ls_cod_attrezzatura, ls_des_attrezzatura, ls_cod_tipo_manutenzione, ls_tipo_man_1, ls_tipo_man_2, &
       ls_tipo_man_3, ls_tipo_man_4, ls_tipo_man_5
long ll_i, ll_y, ll_count, ll_riga


// quando leggo le tipologie di manutenzione non devo mettere TM1--> TM5

select stringa
into   :ls_tipo_man_1
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM1';
		 
select stringa
into   :ls_tipo_man_2
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM2';
		 
select stringa
into   :ls_tipo_man_3
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM3';
		 
select stringa
into   :ls_tipo_man_4
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM4';


select stringa
into   :ls_tipo_man_5
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM5';


declare cu_tipi_manutenzioni cursor for
 select cod_tipo_manutenzione
	from tab_tipi_manutenzione
  where cod_azienda = :s_cs_xx.cod_azienda
	 and cod_attrezzatura = :ls_cod_attrezzatura
	 and not cod_tipo_manutenzione = :ls_tipo_man_1
	 and not cod_tipo_manutenzione = :ls_tipo_man_2
	 and not cod_tipo_manutenzione = :ls_tipo_man_3
	 and not cod_tipo_manutenzione = :ls_tipo_man_4
	 and not cod_tipo_manutenzione = :ls_tipo_man_5;
			 
if cbx_tutto.checked = true then
	dw_elenco_attrez_a.reset()	
	dw_elenco_attrez_a.setredraw(false)
	ids_tipi_manutenzioni.reset()
	
	for ll_i = 1 to dw_elenco_attrez_da.rowcount()	
		ls_cod_attrezzatura = dw_elenco_attrez_da.getitemstring(ll_i, "cod_attrezzatura")
		ls_des_attrezzatura = dw_elenco_attrez_da.getitemstring(ll_i, "des_attrezzatura")	
		
		select count(cod_attrezzatura)
		  into :ll_count
		  from tab_tipi_manutenzione
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_attrezzatura = :ls_cod_attrezzatura;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_tipi_manutenzione " + sqlca.sqlerrtext)
			return
		end if		
		
		if sqlca.sqlcode = 0 and ll_count > 0 then
			ll_riga = dw_elenco_attrez_a.insertrow(0)
			dw_elenco_attrez_a.setitem(ll_riga, "cod_attrezzatura_a", ls_cod_attrezzatura)
			dw_elenco_attrez_a.setitem(ll_riga, "des_attrezzatura_a", ls_des_attrezzatura)
		end if	
		
		open cu_tipi_manutenzioni;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in apertura cursore cu_tipi_manutenzioni " + sqlca.sqlerrtext)
			return
		end if
		
		do while 1 = 1
			fetch cu_tipi_manutenzioni into :ls_cod_tipo_manutenzione;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_tipi_manutenzione " + sqlca.sqlerrtext)
				return
			end if		  
			
			if sqlca.sqlcode = 100 then exit
			
			ll_y = ids_tipi_manutenzioni.insertrow(0)
			ids_tipi_manutenzioni.setitem(ll_y, "cod_attrezzatura", ls_cod_attrezzatura)
			ids_tipi_manutenzioni.setitem(ll_y, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)		
		loop
		close cu_tipi_manutenzioni;
	next	
	
//	dw_elenco_attrez_a.enabled = false
	cb_scarica.enabled = false	
else 
	dw_elenco_attrez_a.enabled = true
	cb_scarica.enabled = true
end if

dw_elenco_attrez_a.setredraw(true)


end event

type st_1 from statictext within w_elenco_attrez
integer x = 274
integer y = 1804
integer width = 329
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Selez. Tutto:"
boolean focusrectangle = false
end type

type cb_programma from commandbutton within w_elenco_attrez
integer x = 2811
integer y = 1976
integer width = 347
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Program."
end type

event clicked;long   ll_risp
string ls_flag_programma_periodo

dw_ext_prog_manut.accepttext()

ll_risp = g_mb.messagebox("Manutenzioni","Continuare con la programmazione delle Apparecchiature?", Exclamation!, OKCancel!, 2)

if ll_risp = 2 then
	g_mb.messagebox("Manutenzioni","Operazione annullata!")
	return 0
end if

ls_flag_programma_periodo = dw_ext_prog_manut.getitemstring( 1, "flag_programma_periodo")

if ls_flag_programma_periodo <> 'S' then
	
	// *** programmazione semplice
	wf_programmazione_semplice()
	
else
	
	// *** programmazione periodo
	wf_programmazione_periodo()
	
end if
end event

type cb_annulla from commandbutton within w_elenco_attrez
integer x = 2455
integer y = 1976
integer width = 347
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;long ll_risp


ll_risp = g_mb.messagebox("Manutenzioni","Annullare l'operazione eseguita?", Exclamation!, OKCancel!, 2)
if ll_risp = 1 then
	close(w_elenco_attrez)
end if
end event

type cb_scarica from commandbutton within w_elenco_attrez
integer x = 2098
integer y = 1976
integer width = 347
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Scarica El."
end type

event clicked;string ls_cod_attrezzatura, ls_des_attrezzatura
long ll_i, ll_risp

ll_risp = g_mb.messagebox("Manutenzioni","Scaricare l'elenco delle apparecchiature?", Exclamation!, OKCancel!, 2)

IF ll_risp = 1 THEN
	dw_elenco_attrez_a.reset()
	dw_elenco_attrez_a.insertrow(0)
	
	ids_tipi_manutenzioni.reset()
END IF



end event

type dw_elenco_attrez_a from uo_cs_xx_dw within w_elenco_attrez
event ue_ultimo ( )
integer x = 1669
integer y = 580
integer width = 1486
integer height = 1120
integer taborder = 60
string dataobject = "d_elenco_attrez_a"
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_ultimo;long ll_row

dw_elenco_attrez_a.accepttext()

ll_row = dw_elenco_attrez_a.GetClickedRow()

dw_elenco_attrez_a.selectrow(ll_row, true)

end event

event clicked;call super::clicked;long ll_row

ll_row = dw_elenco_attrez_a.GetSelectedRow(0)

dw_elenco_attrez_a.selectrow(ll_row, false)

dw_elenco_attrez_a.postevent("ue_ultimo")
end event

event doubleclicked;call super::doubleclicked;long ll_row, ll_find, ll_i
string ls_cod_attrezzatura, ls_des_attrezzatura

if not cbx_tutto.checked then
	ll_row = dw_elenco_attrez_a.GetClickedRow()
	
	ls_cod_attrezzatura = dw_elenco_attrez_a.getitemstring(ll_row, "cod_attrezzatura_a")
	ls_des_attrezzatura = dw_elenco_attrez_a.getitemstring(ll_row, "des_attrezzatura_a")
			
	dw_elenco_attrez_a.DeleteRow(ll_row)
	
	if isnull(dw_elenco_attrez_a.rowcount()) or dw_elenco_attrez_a.rowcount() < 1 then
			dw_elenco_attrez_a.insertrow(0)
	end if	
	
	for ll_i = 1 to ids_tipi_manutenzioni.rowcount()
		ll_find = ids_tipi_manutenzioni.find("cod_attrezzatura = '" + ls_cod_attrezzatura + "'", 1, ids_tipi_manutenzioni.rowcount())
		
		if ll_find > 0 then
			ids_tipi_manutenzioni.deleterow(ll_find)	
		end if	
	next
	
	if ids_tipi_manutenzioni.rowcount() = 1 then
		ll_find = ids_tipi_manutenzioni.find("cod_attrezzatura = '" + ls_cod_attrezzatura + "'", 1, ids_tipi_manutenzioni.rowcount())
		
		if ll_find > 0 then
			ids_tipi_manutenzioni.deleterow(ll_find)	
		end if	
	end if	
end if
end event

type dw_sel_elenco_attrez from uo_cs_xx_dw within w_elenco_attrez
integer x = 23
integer y = 20
integer width = 3131
integer height = 540
integer taborder = 10
string dataobject = "d_sel_elenco_attrez"
end type

event itemchanged;call super::itemchanged;if i_extendmode and i_colname = "cod_attr_da" and not isnull(i_coltext) then
	setitem(row,"cod_attr_a",i_coltext)
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_elenco_attrez,"cod_attr_da")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_elenco_attrez,"cod_attr_a")
end choose
end event

type dw_elenco_attrez_da from datawindow within w_elenco_attrez
event ue_ultimo ( )
integer x = 23
integer y = 580
integer width = 1623
integer height = 1120
integer taborder = 120
string dataobject = "d_elenco_attrez_da"
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_ultimo();long ll_row

ll_row = dw_elenco_attrez_da.getrow()

dw_elenco_attrez_da.selectrow(ll_row, true)
end event

event clicked;long ll_row

ll_row = dw_elenco_attrez_da.getrow()

dw_elenco_attrez_da.selectrow(ll_row, false)

dw_elenco_attrez_da.postevent("ue_ultimo")
end event

event doubleclicked;long   ll_row, ll_find, ll_i, ll_count, ll_new

string ls_cod_attrezzatura, ls_des_attrezzatura, ls_null[]

s_cs_xx.parametri.parametro_s_1_a[] = ls_null[]

ll_row = dw_elenco_attrez_da.getrow()

ls_cod_attrezzatura = dw_elenco_attrez_da.getitemstring(ll_row, "cod_attrezzatura")

ls_des_attrezzatura = dw_elenco_attrez_da.getitemstring(ll_row, "des_attrezzatura")

ll_find = dw_elenco_attrez_a.find("cod_attrezzatura_a = '" + ls_cod_attrezzatura + "'", 1, dw_elenco_attrez_a.rowcount())

if dw_elenco_attrez_a.rowcount() > 0 then
	if isnull(dw_elenco_attrez_a.getitemstring(dw_elenco_attrez_a.getrow(), "cod_attrezzatura_a")) then dw_elenco_attrez_a.reset()
else
	dw_elenco_attrez_a.reset()
end if

if ll_find = 0 then
	
	select count(cod_attrezzatura)
	into   :ll_count
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_tipi_manutenzione " + sqlca.sqlerrtext)
		return
	end if		

	if sqlca.sqlcode = 0 and ll_count > 0 then
		
		setnull(s_cs_xx.parametri.parametro_s_10)
		
		s_cs_xx.parametri.parametro_s_10 = ls_cod_attrezzatura
		
		window_open(w_elenco_tipi_man, 0)
		
		setnull(s_cs_xx.parametri.parametro_s_10)
		
		if s_cs_xx.parametri.parametro_s_1_a[] <> ls_null[] then
		
			dw_elenco_attrez_a.insertrow(0)
			
			dw_elenco_attrez_a.setitem(dw_elenco_attrez_a.rowcount(), "cod_attrezzatura_a", ls_cod_attrezzatura)
			
			dw_elenco_attrez_a.setitem(dw_elenco_attrez_a.rowcount(), "des_attrezzatura_a", ls_des_attrezzatura)
		
			for ll_i = 1 to UpperBound(s_cs_xx.parametri.parametro_s_1_a)
				
				ll_new = ids_tipi_manutenzioni.insertrow(0)
				
				ids_tipi_manutenzioni.setitem( ll_new, "cod_attrezzatura", ls_cod_attrezzatura)
				
				ids_tipi_manutenzioni.setitem( ll_new, "cod_tipo_manutenzione", s_cs_xx.parametri.parametro_s_1_a[ll_i])
				
			next
		else 
			
			dw_elenco_attrez_a.insertrow(0)
			
		end if	
	else 
		g_mb.messagebox("Omnia", "Non esiste nessuna tipologia di manutenzione associata all'attrezzatura selezionata.")
	end if	
	
end if	


end event

type dw_ext_prog_manut from uo_cs_xx_dw within w_elenco_attrez
integer x = 23
integer y = 1796
integer width = 3131
integer height = 160
integer taborder = 80
string dataobject = "d_ext_prog_manut"
boolean border = false
end type

event itemchanged;call super::itemchanged;datetime ldt_null

setnull(ldt_null)

if not i_extendmode then return 0
if i_colname = "flag_programma_periodo" then //and not isnull(i_coltext) then
	if i_coltext = 'S' then
		setitem( row, "da_data", ldt_null)
		setitem( row, "a_data", ldt_null)
	end if
end if
end event

