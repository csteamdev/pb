﻿$PBExportHeader$w_elenco_attrez_semplice.srw
$PBExportComments$Finestra caricamento programmazione manutenzione
forward
global type w_elenco_attrez_semplice from w_cs_xx_risposta
end type
type st_4 from statictext within w_elenco_attrez_semplice
end type
type st_3 from statictext within w_elenco_attrez_semplice
end type
type st_piano from statictext within w_elenco_attrez_semplice
end type
type st_manutenzione from statictext within w_elenco_attrez_semplice
end type
type st_attrezzatura from statictext within w_elenco_attrez_semplice
end type
type st_conferma from statictext within w_elenco_attrez_semplice
end type
type st_2 from statictext within w_elenco_attrez_semplice
end type
type cb_programma from commandbutton within w_elenco_attrez_semplice
end type
type cb_annulla from commandbutton within w_elenco_attrez_semplice
end type
type dw_ext_prog_manut from uo_cs_xx_dw within w_elenco_attrez_semplice
end type
end forward

global type w_elenco_attrez_semplice from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 3227
integer height = 804
string title = "Elenco Apparecchiature"
boolean resizable = false
st_4 st_4
st_3 st_3
st_piano st_piano
st_manutenzione st_manutenzione
st_attrezzatura st_attrezzatura
st_conferma st_conferma
st_2 st_2
cb_programma cb_programma
cb_annulla cb_annulla
dw_ext_prog_manut dw_ext_prog_manut
end type
global w_elenco_attrez_semplice w_elenco_attrez_semplice

type variables
string is_cod_attrezzatura, is_cod_tipo_manutenzione
long   il_anno, il_numero
end variables

forward prototypes
public function integer wf_programmazione_semplice ()
public function integer wf_programmazione_periodo ()
end prototypes

public function integer wf_programmazione_semplice ();long     ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, &
	      ll_anno_registrazione, ll_num_registrazione, ll_ore, ll_minuti, ll_prog_tipo_manutenzione, ll_num_protocollo
			
string   ls_flag_gest_manutenzione, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
	      ls_flag_ricambio_codificato, ls_cod_ricambio, ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, &
		   ls_cod_primario, ls_cod_misura, ls_flag_manutenzione, ls_messaggio, ls_note_idl, ls_descrizione, ls_des_blob, &
			ls_path_documento, ls_data, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
			ls_cod_risorsa_esterna, ls_flag_scadenze, ls_pubblica_intranet, ls_flag_abr
			
dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean  lb_inserimento

datetime ldt_tempo_previsto, ldt_data, ldt_data_1, ldt_data_creazione_programma

blob lbb_blob

uo_manutenzioni luo_manutenzioni

long     ll_risp, ll_giorni, ll_mesi, ll_anni

string   ls_tipo_data

dec{0}	ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

	
ls_data = string(dw_ext_prog_manut.getitemdatetime(1, "data_programmazione"), "dd/mm/yyyy")

if isnull(ls_data) or ls_data = "" then
	g_mb.messagebox("Manutenzioni","Attenzione! Inserire una data valida!")
	return 0
end if

ldt_data = datetime(date(ls_data),00:00:00)

ls_tipo_data = dw_ext_prog_manut.getitemstring(1, "tipo_programmazione")

if ls_tipo_data = "" or isnull(ls_tipo_data) then	
	g_mb.messagebox("Manutenzioni","Attenzione! Scegliere il tipo di uso della data!")
	return 0	
end if

ls_flag_scadenze = dw_ext_prog_manut.getitemstring(1, "flag_scadenze")

	
setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT  prog_tipo_manutenzione,   
		  descrizione,   
		  num_protocollo,   
		  des_blob,   
		  path_documento  
FROM    det_tipi_manutenzioni  
WHERE   cod_azienda = :s_cs_xx.cod_azienda and
		  cod_attrezzatura = :ls_cod_attrezzatura and
		  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni
lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';
		 
if sqlca.sqlcode <> 0 then	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if	
	

	
ls_cod_attrezzatura = is_cod_attrezzatura
	
ls_cod_tipo_manutenzione = is_cod_tipo_manutenzione
	
select count(anno_registrazione) 
into   :ll_count
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_attrezzatura = :ls_cod_attrezzatura and 
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmmi_manutenzione " + sqlca.sqlerrtext)
	rollback;
	setpointer(arrow!)
	return -1
end if	
		
if not isnull(ll_count) and ll_count > 0 then
	
	select count(*)
	into   :ll_count
	from	 manutenzioni,
			 programmi_manutenzione
	where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
			 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
			 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
			 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
			 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
			 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			 manutenzioni.flag_eseguito = 'N';
				 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
		setpointer(arrow!)
		return -1
	end if
		
	if not isnull(ll_count) and ll_count > 0 then
		g_mb.messagebox("Omnia","Attenzione: esistono già delle manutenzioni aperte per questo piano di manutenzione!",stopsign!)
		setpointer(arrow!)
		return -1
	end if		
		
end if

//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------

if ls_flag_abr = 'S' then
	select flag_ricambio_codificato,
			 cod_ricambio,
			 cod_ricambio_alternativo,
			 des_ricambio_non_codificato,
			 num_reg_lista,
			 num_versione,
			 num_edizione,
			 periodicita,
			 frequenza,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_primario,
			 cod_misura,
			 val_min_taratura,
			 val_max_taratura,
			 valore_atteso,
			 flag_manutenzione,
			 modalita_esecuzione,
			 tempo_previsto,
			 cod_operaio,
			 cod_cat_risorse_esterne,
			 cod_risorsa_esterna,
			 data_inizio_val_1,
			 data_fine_val_1,
			 data_inizio_val_2,
			 data_fine_val_2,
			 flag_pubblica_intranet
	into   :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_cod_ricambio_alternativo,
			 :ls_des_ricambio_non_codificato,
			 :ll_num_reg_lista,
			 :ll_num_versione,
			 :ll_num_edizione,
			 :ls_periodicita,
			 :ll_frequenza,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_primario,
			 :ls_cod_misura,
			 :ld_val_min_taratura,
			 :ld_val_max_taratura,
			 :ld_valore_atteso,
			 :ls_flag_manutenzione,
			 :ls_note_idl,
			 :ldt_tempo_previsto,
			 :ls_cod_operaio,
			 :ls_cod_cat_risorse_esterne,
			 :ls_cod_risorsa_esterna,
			 :ld_data_inizio_val_1,
			 :ld_data_fine_val_1,
			 :ld_data_inizio_val_2,
			 :ld_data_fine_val_2,
			 :ls_pubblica_intranet
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
			 cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if				
else
	select flag_ricambio_codificato,
			 cod_ricambio,
			 cod_ricambio_alternativo,
			 des_ricambio_non_codificato,
			 num_reg_lista,
			 num_versione,
			 num_edizione,
			 periodicita,
			 frequenza,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_primario,
			 cod_misura,
			 val_min_taratura,
			 val_max_taratura,
			 valore_atteso,
			 flag_manutenzione,
			 modalita_esecuzione,
			 tempo_previsto,
			 cod_operaio,
			 cod_cat_risorse_esterne,
			 cod_risorsa_esterna,
			 data_inizio_val_1,
			 data_fine_val_1,
			 data_inizio_val_2,
			 data_fine_val_2
	into   :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_cod_ricambio_alternativo,
			 :ls_des_ricambio_non_codificato,
			 :ll_num_reg_lista,
			 :ll_num_versione,
			 :ll_num_edizione,
			 :ls_periodicita,
			 :ll_frequenza,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_primario,
			 :ls_cod_misura,
			 :ld_val_min_taratura,
			 :ld_val_max_taratura,
			 :ld_valore_atteso,
			 :ls_flag_manutenzione,
			 :ls_note_idl,
			 :ldt_tempo_previsto,
			 :ls_cod_operaio,
			 :ls_cod_cat_risorse_esterne,
			 :ls_cod_risorsa_esterna,
			 :ld_data_inizio_val_1,
			 :ld_data_fine_val_1,
			 :ld_data_inizio_val_2,
			 :ld_data_fine_val_2
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
			 cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if			
end if

ll_ore = hour(time(ldt_tempo_previsto))		
ll_minuti = minute(time(ldt_tempo_previsto))
	
ldt_data_creazione_programma = datetime(date(today()), 00:00:00)
		
ll_anno_registrazione = il_anno
ll_num_registrazione = il_numero
	
select periodicita
into   :ls_periodicita
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 anno_registrazione = :ll_anno_registrazione and 
		 num_registrazione = :ll_num_registrazione;
		
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
	rollback;
	setpointer(arrow!)
	return -1
end if
					
if ls_flag_gest_manutenzione = "N" then
	
	choose case ls_periodicita
			
		case "B"     // battute stampi
			
			update programmi_manutenzione
			set    flag_scadenze = 'N'
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
			end if				
			
		case "O"
			
			update programmi_manutenzione
			set    flag_scadenze = 'N'
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
			end if
			
		case "M"
			
			update programmi_manutenzione
			set    flag_scadenze = 'N'
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
			end if
			
		case else
			
			update programmi_manutenzione
			set    flag_scadenze = :ls_flag_scadenze
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
			end if
			
			if ls_flag_scadenze = "S" then

				if luo_manutenzioni.uof_crea_manutenzione_programmata(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
					g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
				// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
				
				int mese1_i, mese1_f, mese2_i, mese2_f, giorno1_i, giorno1_f, giorno2_i, giorno2_f
									
				giorno1_i = ( ld_data_inizio_val_1 / 100)
				mese1_i = mod( ld_data_inizio_val_1, 100)
									
				giorno1_f = ( ld_data_fine_val_1 / 100)
				mese1_f = mod( ld_data_fine_val_1, 100)
				
				if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
				
					if month(date(ldt_data)) < mese1_i or month(date(ldt_data)) > mese1_f then
						ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
					else
						if day(date(ldt_data)) < giorno1_i or day(date(ldt_data)) > giorno1_f then
							ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
						end if
					end if
				
				else										
					giorno2_i = ( ld_data_inizio_val_2 / 100)
					mese2_i = mod( ld_data_inizio_val_2, 100)
										
					giorno2_f = ( ld_data_fine_val_2 / 100)
					mese2_f = mod( ld_data_fine_val_2, 100)							
					if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
						if month(date(ldt_data)) < mese2_i or month(date(ldt_data)) > mese2_f then
							ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
						else
							if day(date(ldt_data)) < giorno2_i or day(date(ldt_data)) > giorno2_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
							end if
						end if												
					end if
				end if
					
				// controllo di che tipo è la data: se è la data della prima scadenza ... allora devo cambiare la data della 
				// registrazione, altrimenti, a partire da quella data, trovo la data della prossima scadenza
	
				if ls_tipo_data <> "Data prima scadenza" then					
					ldt_data_1 = luo_manutenzioni.uof_prossima_scadenza( ldt_data, ls_periodicita, ll_frequenza)					
				else
					ldt_data_1 = ldt_data
				end if						
				
				// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
				
				if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
				
					if month(date(ldt_data_1)) < mese1_i or month(date(ldt_data_1)) > mese1_f then
						ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
					else
						if day(date(ldt_data_1)) < giorno1_i or day(date(ldt_data_1)) > giorno1_f then
							ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
						end if
					end if
				
				else										
					
					if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
						if month(date(ldt_data_1)) < mese2_i or month(date(ldt_data_1)) > mese2_f then
							ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
						else
							if day(date(ldt_data_1)) < giorno2_i or day(date(ldt_data_1)) > giorno2_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
							end if
						end if												
					end if
				end if					
				
				update manutenzioni
				set    data_registrazione = :ldt_data_1
				where  cod_azienda = :s_cs_xx.cod_azienda and    
						 anno_reg_programma = :ll_anno_registrazione	and    
						 num_reg_programma = :ll_num_registrazione;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Manutenzioni","Errore in aggiornamento data registrazione! " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
			end if
			
	end choose		
end if	

	 
destroy luo_manutenzioni
	
commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	g_mb.messagebox("Omnia", "Programmazione avvenuta con successo!~n~rAttenzione la finestra Programmi Manutenzione verrà chiusa per permettere l'aggiornamento della stessa")
	close(w_elenco_attrez)
end if

return 0
end function

public function integer wf_programmazione_periodo ();datetime ldt_da_data, ldt_a_data, ldt_tempo_previsto, ldt_data_1, ldt_data, ldt_scadenze[], ldt_data_registrazione, &
         ldt_data_prossimo_intervento, ldt_null, ldt_max_data_registrazione, ldt_data_creazione_programma, &
			ldt_scadenze_vuoto[]

string   ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_gest_manutenzione, ls_flag_ricambio_codificato, ls_cod_ricambio, &
         ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, ls_cod_primario, ls_cod_misura, &
			ls_flag_manutenzione, ls_note_idl, ls_des_blob, ls_path_documento, ls_descrizione, ls_messaggio, ls_flag_scadenze_esistenti, &
			ls_cod_tipo_lista_dist, ls_cod_lista_dist,ls_cod_operaio,ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna, ls_pubblica_intranet, ls_flag_abr
			
long     ll_anno_registrazione, ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, ll_ore, ll_minuti, &
         ll_num_registrazione, ll_prog_tipo_manutenzione, ll_num_protocollo, ll_num_programmazioni, ll_ii

dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean  lb_inserimento

dec{0}   ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

uo_manutenzioni luo_manutenzioni

blob     lbb_blob

setnull(ldt_null)

ls_flag_scadenze_esistenti = dw_ext_prog_manut.getitemstring( 1, "flag_scadenze_esistenti")

ldt_da_data = dw_ext_prog_manut.getitemdatetime( 1, "da_data")

ldt_a_data = dw_ext_prog_manut.getitemdatetime( 1, "a_data")

if isnull(ldt_da_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data inizio programmazione!")
	return -1
end if

if isnull(ldt_a_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data fine programmazione!")
	return -1	
end if

if ldt_da_data > ldt_a_data then
	g_mb.messagebox("OMNIA", "Attenzione: Controllare le date del periodo di programmazione!")
	return -1	
end if

setpointer(hourglass!)
	
luo_manutenzioni = create uo_manutenzioni

lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then	
	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 

	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
if sqlca.sqlcode = 100 then
	
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if	

//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------

ls_cod_attrezzatura = is_cod_attrezzatura
ls_cod_tipo_manutenzione = is_cod_tipo_manutenzione
st_conferma.text = ls_cod_attrezzatura + " - " + ls_cod_tipo_manutenzione

ll_ore = hour(time(ldt_tempo_previsto))		
ll_minuti = minute(time(ldt_tempo_previsto))
ldt_data_creazione_programma = datetime(date(today()), 00:00:00)	
	
ll_anno_registrazione = il_anno
ll_num_registrazione = il_numero
	
lb_inserimento = true
	
select periodicita
into   :ls_periodicita
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 anno_registrazione = :ll_anno_registrazione and 
		 num_registrazione = :ll_num_registrazione;
		
if sqlca.sqlcode < 0 then
	
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
	rollback;
	setpointer(arrow!)
	return -1
	
end if
				
// ** GESTIONE SEMPLICE DELLE MANUTENZIONI

if ls_flag_gest_manutenzione = "N" then
	
	choose case ls_periodicita
			
		case "B"     // battute stampi
			
			update programmi_manutenzione
			set    flag_scadenze = 'N'
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode < 0 then
				
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
				
			end if
			
			
		case "O"  // ** ore
			
			update programmi_manutenzione
			set    flag_scadenze = 'N'
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode < 0 then
				
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
				
			end if
			
			
		case "M" // ** minuti
			
			update programmi_manutenzione
			set    flag_scadenze = 'N'
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode < 0 then
				
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
				
			end if
			
		case else // ** G = giorni, S = settimane, E = mesi
			
			update programmi_manutenzione
			set    flag_scadenze = 'N'
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and  
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode < 0 then
				
				g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1
				
			end if
			
			
			// *** programmazione periodo funzione 1: trovo il vettore con le date delle scadenze
			
			// *** controllo se esistono programmi di manutenzione
			select count(anno_registrazione) 
			into   :ll_count
			from   programmi_manutenzione
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_attrezzatura = :ls_cod_attrezzatura and 
					 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
					
			if sqlca.sqlcode < 0 then			
				g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
				rollback;
				setpointer(arrow!)
				return -1					
			end if
				
			// controllo se esistono scadenze da confermare
			if not isnull(ll_count) and ll_count > 0 then
					
				select count(*)
				into   :ll_count
				from	 manutenzioni,
						 programmi_manutenzione
				where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
						 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
						 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
						 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
						 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
						 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
						 manutenzioni.flag_eseguito = 'N';
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
					setpointer(arrow!)
					return -1
					
				end if
				
				// se ci sono scadenze da confermare allora guardo il flag
				if not isnull(ll_count) and ll_count > 0 then
					
					// *** flag = 'S' allora seleziono la massima scadenza e faccio partire il periodo di programmazione
					//     dalla prossima scadenza a partire da quella data di registrazione
					if ls_flag_scadenze_esistenti = 'S' then
						
						select MAX(data_registrazione)
						into   :ldt_max_data_registrazione
						from	 manutenzioni,
								 programmi_manutenzione
						where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
								 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
								 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
								 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
								 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
								 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
								 manutenzioni.flag_eseguito = 'N';
								 
						if sqlca.sqlcode < 0 then
							
							g_mb.messagebox("Omnia","Errore in controllo data manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
							setpointer(arrow!)
							return -1
							
						end if
					
						if not isnull(ldt_max_data_registrazione) then ldt_max_data_registrazione = luo_manutenzioni.uof_prossima_scadenza( ldt_max_data_registrazione, ls_periodicita, ll_frequenza )								
						
						if not isnull(ldt_max_data_registrazione) then ldt_da_data = ldt_max_data_registrazione
						
					else
					// *** cancello le scadenze ancora da confermare e continuo con il periodo scritto 
					//     dall'operatore
						delete from	 manutenzioni
						where  manutenzioni.cod_azienda = :s_cs_xx.cod_azienda and
								 manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura and
								 manutenzioni.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
								 manutenzioni.flag_eseguito = 'N' and
								 manutenzioni.anno_reg_programma IS NOT NULL and
								 manutenzioni.num_reg_programma IS NOT NULL;		
								 
						if sqlca.sqlcode < 0 then
							
							g_mb.messagebox("Omnia","Errore in cancellazione manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
							rollback;
							setpointer(arrow!)
							return -1
							
						end if									 
													
					end if
				end if
					
			end if

			// ***
			
			ldt_scadenze[] = ldt_scadenze_vuoto[]
			
			luo_manutenzioni.uof_scadenze_periodo( ldt_da_data , date(ldt_da_data), date(ldt_a_data), ls_periodicita, ll_frequenza, ldt_scadenze)
			
			if UpperBound(ldt_scadenze) < 1 then
				
			else
			
				for ll_ii = 1 to UpperBound(ldt_scadenze)
					
					ldt_data_registrazione = ldt_scadenze[ll_ii]
					
					if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(ll_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, ls_messaggio) <> 0 then
							g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
							rollback;
							setpointer(arrow!)
							return -1
					end if
					
				next
				
			end if
			
			// ***

	end choose		
end if	
	 
destroy luo_manutenzioni
	
commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	
	g_mb.messagebox("Omnia", "Programmazione avvenuta con successo!~n~rAttenzione la finestra Programmi Manutenzione verrà chiusa per permettere l'aggiornamento della stessa")
	close(w_elenco_attrez)
	
end if
	
return 0
end function

on w_elenco_attrez_semplice.create
int iCurrent
call super::create
this.st_4=create st_4
this.st_3=create st_3
this.st_piano=create st_piano
this.st_manutenzione=create st_manutenzione
this.st_attrezzatura=create st_attrezzatura
this.st_conferma=create st_conferma
this.st_2=create st_2
this.cb_programma=create cb_programma
this.cb_annulla=create cb_annulla
this.dw_ext_prog_manut=create dw_ext_prog_manut
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_4
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.st_piano
this.Control[iCurrent+4]=this.st_manutenzione
this.Control[iCurrent+5]=this.st_attrezzatura
this.Control[iCurrent+6]=this.st_conferma
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.cb_programma
this.Control[iCurrent+9]=this.cb_annulla
this.Control[iCurrent+10]=this.dw_ext_prog_manut
end on

on w_elenco_attrez_semplice.destroy
call super::destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_piano)
destroy(this.st_manutenzione)
destroy(this.st_attrezzatura)
destroy(this.st_conferma)
destroy(this.st_2)
destroy(this.cb_programma)
destroy(this.cb_annulla)
destroy(this.dw_ext_prog_manut)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

									 
dw_ext_prog_manut.set_dw_options(sqlca, &
                   pcca.null_object, &
                   c_nomodify + &
                   c_nodelete + &
	   				 c_newonopen + &
						 c_noretrieveonopen + &
                   c_disableCC, &
                   c_noresizedw + &
                   c_nohighlightselected + &
                   c_cursorrowpointer)
						 
						 
st_piano.text = s_cs_xx.parametri.parametro_s_1
st_attrezzatura.text = s_cs_xx.parametri.parametro_s_2
st_manutenzione.text = s_cs_xx.parametri.parametro_s_3

il_anno = s_cs_xx.parametri.parametro_ul_1
il_numero = s_cs_xx.parametri.parametro_ul_2
is_cod_attrezzatura = s_cs_xx.parametri.parametro_s_2
is_cod_tipo_manutenzione = s_cs_xx.parametri.parametro_s_3
end event

type st_4 from statictext within w_elenco_attrez_semplice
integer x = 46
integer y = 240
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tipologia di manutenzione:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_elenco_attrez_semplice
integer x = 46
integer y = 160
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Attrezzatura:"
boolean focusrectangle = false
end type

type st_piano from statictext within w_elenco_attrez_semplice
integer x = 2423
integer y = 160
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type st_manutenzione from statictext within w_elenco_attrez_semplice
integer x = 777
integer y = 240
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tipologia di manutenzione:"
boolean focusrectangle = false
end type

type st_attrezzatura from statictext within w_elenco_attrez_semplice
integer x = 800
integer y = 160
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Attrezzatura:"
boolean focusrectangle = false
end type

type st_conferma from statictext within w_elenco_attrez_semplice
integer x = 23
integer y = 540
integer width = 2057
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_2 from statictext within w_elenco_attrez_semplice
integer x = 23
integer y = 20
integer width = 3109
integer height = 120
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 255
string text = "CREAZIONE SCADENZE"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_programma from commandbutton within w_elenco_attrez_semplice
integer x = 2811
integer y = 536
integer width = 347
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Program."
end type

event clicked;long   ll_risp
string ls_flag_programma_periodo

dw_ext_prog_manut.accepttext()

ll_risp = g_mb.messagebox("Manutenzioni","Continuare con la programmazione delle Apparecchiature?", Exclamation!, OKCancel!, 2)

if ll_risp = 2 then
	g_mb.messagebox("Manutenzioni","Operazione annullata!")
	return 0
end if

ls_flag_programma_periodo = dw_ext_prog_manut.getitemstring( 1, "flag_programma_periodo")

if ls_flag_programma_periodo <> 'S' then
	
	// *** programmazione semplice
	wf_programmazione_semplice()
	
else
	
	// *** programmazione periodo
	wf_programmazione_periodo()
	
end if
end event

type cb_annulla from commandbutton within w_elenco_attrez_semplice
integer x = 2455
integer y = 536
integer width = 347
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;long ll_risp

ll_risp = g_mb.messagebox("Manutenzioni","Annullare l'operazione eseguita?", Exclamation!, OKCancel!, 2)
rollback;
if ll_risp = 1 then
	close(parent)
end if
end event

type dw_ext_prog_manut from uo_cs_xx_dw within w_elenco_attrez_semplice
integer x = 23
integer y = 356
integer width = 3131
integer height = 160
integer taborder = 80
string dataobject = "d_ext_prog_manut"
boolean border = false
end type

event itemchanged;call super::itemchanged;datetime ldt_null

setnull(ldt_null)

if not i_extendmode then return 0
if i_colname = "flag_programma_periodo" then //and not isnull(i_coltext) then
	if i_coltext = 'S' then
		setitem( row, "da_data", ldt_null)
		setitem( row, "a_data", ldt_null)
	end if
end if
end event

