﻿$PBExportHeader$w_elenco_elimina_prog.srw
$PBExportComments$Finestra per eliminazione programmi manutenzione
forward
global type w_elenco_elimina_prog from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_elenco_elimina_prog
end type
type cb_elimina from commandbutton within w_elenco_elimina_prog
end type
type dw_elenco_elimina_prog from uo_cs_xx_dw within w_elenco_elimina_prog
end type
type ws_record from structure within w_elenco_elimina_prog
end type
end forward

type ws_record from structure
	long		anno_registrazione
	long		num_registrazione
	string		des_manutenzione
end type

global type w_elenco_elimina_prog from w_cs_xx_risposta
integer width = 3657
integer height = 1544
string title = "Elimina Programmi Manutenzione"
cb_annulla cb_annulla
cb_elimina cb_elimina
dw_elenco_elimina_prog dw_elenco_elimina_prog
end type
global w_elenco_elimina_prog w_elenco_elimina_prog

on w_elenco_elimina_prog.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_elimina=create cb_elimina
this.dw_elenco_elimina_prog=create dw_elenco_elimina_prog
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_elimina
this.Control[iCurrent+3]=this.dw_elenco_elimina_prog
end on

on w_elenco_elimina_prog.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_elimina)
destroy(this.dw_elenco_elimina_prog)
end on

event open;call super::open;long ll_count, ll_anno_registrazione, ll_num_registrazione, ll_i, ll_count_2
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione

dw_elenco_elimina_prog.reset()
ll_i = 1

declare cu_programmi cursor for 
 select anno_registrazione,
        num_registrazione,
		  cod_attrezzatura,
		  cod_tipo_manutenzione
	from programmi_manutenzione
  where cod_azienda = :s_cs_xx.cod_azienda
  order by cod_attrezzatura, cod_tipo_manutenzione;
  
open cu_programmi;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in fase di apertura cursore cu_programmi " + sqlca.sqlerrtext)
	return
end if

do while 1 = 1
	fetch cu_programmi into :ll_anno_registrazione,
									:ll_num_registrazione,
									:ls_cod_attrezzatura,
									:ls_cod_tipo_manutenzione;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		return
	end if

	if sqlca.sqlcode = 100 then exit
	
	select count(anno_registrazione)
	  into :ll_count
	  from manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_reg_programma = :ll_anno_registrazione
		and num_reg_programma = :ll_num_registrazione
		and flag_eseguito = 'S';	
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella manutenzione " + sqlca.sqlerrtext)
		return
	end if		
	
	if isnull(ll_count) or ll_count < 1 then
		
		select count(anno_registrazione)
		  into :ll_count_2
		  from det_cal_progr_manut
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and anno_registrazione = :ll_anno_registrazione
			and num_registrazione = :ll_num_registrazione;
			
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella det_cal_progr_manut " + sqlca.sqlerrtext)
			return
		end if					
		
		if isnull(ll_count_2) or ll_count_2 < 1 then		
			dw_elenco_elimina_prog.insertrow(0)
			dw_elenco_elimina_prog.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
			dw_elenco_elimina_prog.setitem(ll_i, "num_registrazione", ll_num_registrazione)
			dw_elenco_elimina_prog.setitem(ll_i, "cod_attrezzatura", ls_cod_attrezzatura)
			dw_elenco_elimina_prog.setitem(ll_i, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)		
			ll_i ++ 
		end if	
	end if	
	
loop
close cu_programmi;
		






	
	
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)				 

dw_elenco_elimina_prog.set_dw_options(sqlca, &
                                   pcca.null_object, &
                                   c_multiselect + &
							    		     c_nomodify + &
											  c_nodelete + &
											  c_disablecc + &
											  c_default, &
                                   c_default)
end event

type cb_annulla from commandbutton within w_elenco_elimina_prog
integer x = 2839
integer y = 1340
integer width = 357
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(w_elenco_elimina_prog)
end event

type cb_elimina from commandbutton within w_elenco_elimina_prog
integer x = 3232
integer y = 1340
integer width = 357
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_anno_reg_man, ll_num_reg_man, ld_num_protocollo, ld_num_protocollo_p, &
	  ll_null[], ll_anno_reg[], ll_num_reg[], ll_y
boolean lb_result
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_manutenzione

lb_result = false
ll_anno_reg[] = ll_null[]
ll_num_reg[] = ll_null[]
ll_y = 1

for ll_i = 1 to dw_elenco_elimina_prog.rowcount()
	lb_result = dw_elenco_elimina_prog.IsSelected(ll_i)
	if lb_result then
		ll_anno_registrazione = dw_elenco_elimina_prog.getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione = dw_elenco_elimina_prog.getitemnumber(ll_i, "num_registrazione")
		ls_cod_attrezzatura = dw_elenco_elimina_prog.getitemstring(ll_i, "cod_attrezzatura")
		ls_cod_tipo_manutenzione = dw_elenco_elimina_prog.getitemstring(ll_i, "cod_tipo_manutenzione")		
		
		select anno_registrazione,
				 num_registrazione
		  into :ll_anno_reg_man,
		  		 :ll_num_reg_man
		  from manutenzioni
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and anno_reg_programma = :ll_anno_registrazione
		   and num_reg_programma = :ll_num_registrazione;		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella manutenzioni " + sqlca.sqlerrtext)
			return
		end if	
		
		if sqlca.sqlcode = 0 then
		
			delete from det_registro_tarature                //cancellazione det_registro_tarature
					where cod_azienda = :s_cs_xx.cod_azienda
					  and anno_registrazione = :ll_anno_reg_man
					  and num_registrazione = :ll_num_reg_man;		
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in cancellazione dati dalla tabella det_registro_tarature " + sqlca.sqlerrtext)
				return
			end if			
			
			delete from tes_registro_tarature                //cancellazione tes_registro_tarature
					where cod_azienda = :s_cs_xx.cod_azienda
					  and anno_registrazione = :ll_anno_reg_man
					  and num_registrazione = :ll_num_reg_man;		
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in cancellazione dati dalla tabella tes_registro_tarature " + sqlca.sqlerrtext)
				return
			end if			
			
			declare cu_det_manutenzioni cursor for
				select num_protocollo
				  from det_manutenzioni
				 where cod_azienda = :s_cs_xx.cod_azienda
					and anno_registrazione = :ll_anno_reg_man
					and num_registrazione = :ll_num_reg_man;
			
			open cu_det_manutenzioni;
			
			do while 1 = 1
				fetch cu_det_manutenzioni into :ld_num_protocollo;
				if sqlca.sqlcode = 100 then exit
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Manutenzioni")
					return
				end if	
				
				if sqlca.sqlcode = 0 then
					delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
					where cod_azienda = :s_cs_xx.cod_azienda
					  and num_protocollo = :ld_num_protocollo;
					  
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
						return
					end if		  
			
					delete from det_manutenzioni  //cancellazione dettaglio manutenzioni
						where cod_azienda = :s_cs_xx.cod_azienda
						  and anno_registrazione = :ll_anno_reg_man
						  and num_registrazione = :ll_num_reg_man;
					
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Errore cancellazione dalla tabella DET_MANUTENZIONI:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
						return
					end if	  		  
					  
					delete from tab_protocolli  //cancellazione protocollo
					where cod_azienda = :s_cs_xx.cod_azienda
					  and num_protocollo = :ld_num_protocollo;
					  
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
						return
					end if
				end if
			loop	
			close cu_det_manutenzioni;
			
			delete from manutenzioni                         //cancellazione manutenzioni
					where cod_azienda = :s_cs_xx.cod_azienda
					  and anno_reg_programma = :ll_anno_registrazione
					  and num_reg_programma = :ll_num_registrazione;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in cancellazione dati dalla tabella manutenzioni " + sqlca.sqlerrtext)
				return
			end if	
		end if
		
		declare cu_det_prog_manutenzioni cursor for
			select num_protocollo
			  from det_prog_manutenzioni
			 where cod_azienda = :s_cs_xx.cod_azienda
				and anno_registrazione = :ll_anno_registrazione
				and num_registrazione = :ll_num_registrazione;
		
		open cu_det_prog_manutenzioni;
		
		do while 1 = 1
			fetch cu_det_prog_manutenzioni into :ld_num_protocollo_p;
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Programmi Manutenzioni " + sqlca.sqlerrtext)
				return
			end if	
			
			if sqlca.sqlcode = 0 then
				delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
				where cod_azienda = :s_cs_xx.cod_azienda
				  and num_protocollo = :ld_num_protocollo_p;
				  
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if		  
		
				delete from det_prog_manutenzioni  //cancellazione dettaglio programmi manutenzioni
					where cod_azienda = :s_cs_xx.cod_azienda
					  and anno_registrazione = :ll_anno_registrazione
					  and num_registrazione = :ll_num_registrazione;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione dalla tabella DET_PROG_MANUTENZIONI:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if	  		  
				  
				delete from tab_protocolli  //cancellazione protocollo
				where cod_azienda = :s_cs_xx.cod_azienda
				  and num_protocollo = :ld_num_protocollo_p;
				  
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if
			end if
		loop	
		close cu_det_prog_manutenzioni;
		
//		delete from elenco_risorse
//		      where cod_azienda = :s_cs_xx.cod_azienda
//				  and cod_attrezzatura = :ls_cod_attrezzatura
//				  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia", "Errore cancellazione dati dalla tabella elenco_risorse " + sqlca.sqlerrtext)
//			return
//		end if				  						  
		
		delete from prog_man_ordinati
		      where cod_azienda = :s_cs_xx.cod_azienda
				  and anno_registrazione = :ll_anno_registrazione
				  and num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore cancellazione dati dalla tabella prog_man_ordinati " + sqlca.sqlerrtext)
			return
		end if				  				
		
		delete from programmi_manutenzione
		      where cod_azienda = :s_cs_xx.cod_azienda
				  and anno_registrazione = :ll_anno_registrazione
				  and num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore cancellazione dati dalla tabella programmi_manutenzioni " + sqlca.sqlerrtext)
			return
		end if				  		
	
	   ll_anno_reg[ll_y] = ll_anno_registrazione
		ll_num_reg[ll_y] = ll_num_registrazione
		ll_y ++

	end if
next	

s_cs_xx.parametri.parametro_d_1_a[] = ll_anno_reg[]
s_cs_xx.parametri.parametro_d_2_a[] = ll_num_reg[]

g_mb.messagebox("Omnia", "Cancellazione Effettuata!")

close(w_elenco_elimina_prog)
	
		
	
end event

type dw_elenco_elimina_prog from uo_cs_xx_dw within w_elenco_elimina_prog
integer x = 27
integer y = 20
integer width = 3561
integer height = 1296
integer taborder = 10
string dataobject = "d_elenco_elimina_prog"
boolean vscrollbar = true
boolean livescroll = true
end type

