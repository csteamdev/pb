﻿$PBExportHeader$w_elenco_oper_manut.srw
$PBExportComments$Finestra Elenco Risorse
forward
global type w_elenco_oper_manut from w_cs_xx_principale
end type
type dw_elenco_oper_manut from uo_cs_xx_dw within w_elenco_oper_manut
end type
end forward

global type w_elenco_oper_manut from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2725
integer height = 800
string title = "Operatori"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_elenco_oper_manut dw_elenco_oper_manut
end type
global w_elenco_oper_manut w_elenco_oper_manut

type variables
long il_anno, il_numero
end variables

forward prototypes
public function integer wf_carica_costo_operaio (string fs_cod_operaio)
public subroutine wf_calcola_totale_operai (long fl_row)
end prototypes

public function integer wf_carica_costo_operaio (string fs_cod_operaio);string ls_cod_cat_attrezzatura
dec{4} ld_costo_orario

if isnull(fs_cod_operaio) then return 0

select cod_cat_attrezzature
into   :ls_cod_cat_attrezzatura
from   anag_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operaio = :fs_cod_operaio;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in select anag_operai~r~n"+sqlca.sqlerrtext)
	return 0
end if

if isnull(ls_cod_cat_attrezzatura) then
	g_mb.messagebox("OMNIA", "L'operatore non è associato ad alcuna categoria di costo.")
	return 0
end if

select costo_medio_orario
into   :ld_costo_orario
from   tab_cat_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cat_attrezzature = :ls_cod_cat_attrezzatura;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in select tab_cat_attrezzature~r~n"+sqlca.sqlerrtext)
	return 0
end if

return ld_costo_orario
end function

public subroutine wf_calcola_totale_operai (long fl_row);dec{4} ld_costo_ora, ld_tempo, ld_totale

ld_costo_ora = dw_elenco_oper_manut.getitemnumber(fl_row, "costo_operatore")
ld_tempo = dw_elenco_oper_manut.getitemnumber(fl_row, "ore") + ( round(dw_elenco_oper_manut.getitemnumber(fl_row, "minuti") / 60,2) )

if isnull(ld_costo_ora) then ld_costo_ora = 0
if isnull(ld_tempo) then ld_tempo = 0

ld_totale = (ld_costo_ora * ld_tempo)

if ld_totale = 0 then 
	dw_elenco_oper_manut.setitem(fl_row, "tot_costo_operaio", 0)
else
	dw_elenco_oper_manut.setitem(fl_row, "tot_costo_operaio", round(ld_totale, 2))
end if
dw_elenco_oper_manut.accepttext()
return
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_elenco_oper_manut.set_dw_key("cod_azienda")
//dw_elenco_risorse.set_dw_options(sqlca, &
//											i_openparm, &
//											c_scrollparent, &
//											c_default)

dw_elenco_oper_manut.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_default, &
                                    c_default)

iuo_dw_main = dw_elenco_oper_manut

end event

on w_elenco_oper_manut.create
int iCurrent
call super::create
this.dw_elenco_oper_manut=create dw_elenco_oper_manut
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_elenco_oper_manut
end on

on w_elenco_oper_manut.destroy
call super::destroy
destroy(this.dw_elenco_oper_manut)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_sort(dw_elenco_oper_manut,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'","cognome ASC, nome ASC")
end event

event close;call super::close;decimal ld_righe, ld_totale, ld_i, ll_tempo, ld_tempo_ore, ld_tempo_minuti,ld_costo, ld_costo_riga

ld_righe = dw_elenco_oper_manut.rowcount()

if ld_righe > 0 then
	for ld_i = 1 to ld_righe
		ld_costo = dw_elenco_oper_manut.getitemnumber(ld_i,"costo_operatore")
		ld_tempo_minuti = dw_elenco_oper_manut.getitemnumber(ld_i, "minuti")/60
		ld_tempo_ore = dw_elenco_oper_manut.getitemnumber(ld_i, "ore") + ld_tempo_minuti
		ld_costo_riga = ld_tempo_ore * ld_costo
		ld_totale = ld_totale + ld_costo_riga 
	next
end if

s_cs_xx.parametri.parametro_d_8 = ld_totale


end event

type dw_elenco_oper_manut from uo_cs_xx_dw within w_elenco_oper_manut
event ue_totale_riga ( )
integer x = 9
integer y = 8
integer width = 2697
integer height = 700
integer taborder = 10
string dataobject = "d_elenco_oper_manut"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_totale_riga();wf_calcola_totale_operai(getrow())
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

il_anno =  long(s_cs_xx.parametri.parametro_s_10)
il_numero = long(s_cs_xx.parametri.parametro_s_11)

l_Error = Retrieve(s_cs_xx.cod_azienda, il_anno, il_numero)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "anno_registrazione", dec(s_cs_xx.parametri.parametro_s_10))
   this.setitem(ll_i, "num_registrazione", dec(s_cs_xx.parametri.parametro_s_11))
next


end event

event pcd_new;call super::pcd_new;long	 ll_prog

this.setitem(this.getrow(),"ore",0)
this.setitem(this.getrow(),"minuti",0)
this.setitem(this.getrow(),"costo_operatore",0)
this.setitem(this.getrow(),"tot_costo_operaio",0)

select max(progressivo)
into   :ll_prog
from   manutenzioni_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno and
		 num_registrazione = :il_numero;
		 
if sqlca.sqlcode = 0 then
	if isnull(ll_prog) then ll_prog = 0
	ll_prog ++
	setitem( getrow(), "progressivo", ll_prog)
end if
end event

event updatestart;call super::updatestart;long ll_i
decimal ld_ore_operaio,ld_minuti_operaio

if rowcount() > 0 then
	for ll_i = 1 to rowcount()
		ld_ore_operaio = getitemnumber(ll_i, "ore")
		ld_minuti_operaio = getitemnumber(ll_i,"minuti")
		if (ld_ore_operaio = 0) and (ld_minuti_operaio = 0) then
			g_mb.messagebox("OMNIA"," E' obbligatoria l'indicazione della durata in ore e minuti dell'intervento!",information!)
			return 1
		end if
	next
end if
end event

event itemchanged;call super::itemchanged;double ld_costo

if i_extendmode then
	choose case i_colname
		case "cod_operaio"
			ld_costo = wf_carica_costo_operaio(i_coltext)
			setitem(row, "costo_operatore", ld_costo)
			
		case else
			postevent("ue_totale_riga")
	end choose
end if

end event

