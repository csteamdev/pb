﻿$PBExportHeader$w_elenco_tipi_man.srw
$PBExportComments$Finestra tipologie di manutenzione
forward
global type w_elenco_tipi_man from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_elenco_tipi_man
end type
type cb_conferma from commandbutton within w_elenco_tipi_man
end type
type dw_elenco_attrez_conferma from uo_cs_xx_dw within w_elenco_tipi_man
end type
end forward

global type w_elenco_tipi_man from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 2875
integer height = 1632
string title = "Elenco Tipi Manutenzioni"
cb_annulla cb_annulla
cb_conferma cb_conferma
dw_elenco_attrez_conferma dw_elenco_attrez_conferma
end type
global w_elenco_tipi_man w_elenco_tipi_man

on w_elenco_tipi_man.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.dw_elenco_attrez_conferma=create dw_elenco_attrez_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.dw_elenco_attrez_conferma
end on

on w_elenco_tipi_man.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.dw_elenco_attrez_conferma)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_elenco_attrez_conferma.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_modifyok + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
									 
									 
end event

event open;call super::open;string ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_tipo_man_1, &
		 ls_tipo_man_2, ls_tipo_man_3, ls_tipo_man_4, ls_tipo_man_5, ls_sql

long   ll_tot_manut, ll_tot_prog, ll_anno_prog, ll_num_prog, ll_i


dw_elenco_attrez_conferma.reset()

ll_i = 1

ls_cod_attrezzatura = s_cs_xx.parametri.parametro_s_10

ls_sql = " select cod_tipo_manutenzione, " + &
			"        des_tipo_manutenzione " + &
         " from   tab_tipi_manutenzione " + &
         " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			"        cod_attrezzatura = '" + ls_cod_attrezzatura + "' and " + &
			"        (flag_blocco is null or flag_blocco = 'N') "
	 
// PARAMETRI CREATI PER GUERRATO: per evitare che i tipi manutenzioni creati per chiamate telefoniche e cartacee
// vadano in programmazione.

select stringa
into   :ls_tipo_man_1
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM1';
		 
if sqlca.sqlcode = 0 and not isnull(ls_tipo_man_1) then
	ls_sql = ls_sql + " and cod_tipo_manutenzione <> '" + ls_tipo_man_1 + "' "
end if

select stringa
into   :ls_tipo_man_2
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM2';

if sqlca.sqlcode = 0 and not isnull(ls_tipo_man_2) then
	ls_sql = ls_sql + " and cod_tipo_manutenzione <> '" + ls_tipo_man_2 + "' "
end if
		 
select stringa
into   :ls_tipo_man_3
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM3';

if sqlca.sqlcode = 0 and not isnull(ls_tipo_man_3) then
	ls_sql = ls_sql + " and cod_tipo_manutenzione <> '" + ls_tipo_man_3 + "' "
end if

select stringa
into   :ls_tipo_man_4
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM4';

if sqlca.sqlcode = 0 and not isnull(ls_tipo_man_4) then
	ls_sql = ls_sql + " and cod_tipo_manutenzione <> '" + ls_tipo_man_4 + "' "
end if

select stringa
into   :ls_tipo_man_5
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM5';

if sqlca.sqlcode = 0 and not isnull(ls_tipo_man_5) then
	ls_sql = ls_sql + " and cod_tipo_manutenzione <> '" + ls_tipo_man_5 + "' "
end if


// 14/11/2003 --> non devono essere visualizzate le tipologie già programmate.
// 30/06/2004: per la funzione 2 della specifica "Programmazione_periodo" occorre far vedere anche le tipologie per cui
//             esistono  delle registrazioni non eseguite.

ls_sql = ls_sql + " order by cod_tipo_manutenzione "

declare cu_tipi_manutenzione dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_tipi_manutenzione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in fase di apertura del cursore dinamico cu_tipi_manutenzione " + sqlca.sqlerrtext)
	return
end if

do while true
	
	fetch cu_tipi_manutenzione into :ls_cod_tipo_manutenzione, 
	                                :ls_des_tipo_manutenzione;
											  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_tipi_manutenzione " + sqlca.sqlerrtext)
		return
	end if	
	
	if sqlca.sqlcode = 100 then exit
	
	ll_anno_prog = 0 
	
	ll_num_prog = 0
	
	select count(*)
	into   :ll_num_prog
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
	and    cod_attrezzatura = :ls_cod_attrezzatura;

	if ll_num_prog > 0 then
		
		dw_elenco_attrez_conferma.setitem(ll_i,"flag_morta","S")
		
		select count(*) 
		into   :ll_tot_manut
		from   manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
		and    cod_attrezzatura = :ls_cod_attrezzatura
		and    anno_reg_programma is not null
		and    num_reg_programma is not null
		and    flag_ordinario = 'S'
		and    flag_eseguito = 'N';
		
		if ll_tot_manut > 0 then 
			
			// ** TIPOLOGIA IN PROGRAMMAZIONE: infatti esistono programmi, e delle scadenze non ancora confermate
			
			dw_elenco_attrez_conferma.insertrow(0)
			
			dw_elenco_attrez_conferma.setitem(ll_i,"cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
			
			dw_elenco_attrez_conferma.setitem(ll_i,"des_tipo_manutenzione", ls_des_tipo_manutenzione)	
			
			dw_elenco_attrez_conferma.setitem(ll_i,"flag_conferma", "N")				
			
			dw_elenco_attrez_conferma.setitem(ll_i,"flag_morta","%")
			
		else
			
			// ** PROGRAMMAZIONE INTERROTTA: ho detto di no quando mi hanno chiesto se volevo generare la 
			//                               scadenza successiva
			
			dw_elenco_attrez_conferma.insertrow(0)
			
			dw_elenco_attrez_conferma.setitem(ll_i,"cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
			
			dw_elenco_attrez_conferma.setitem(ll_i,"des_tipo_manutenzione", ls_des_tipo_manutenzione)	
			
			dw_elenco_attrez_conferma.setitem(ll_i,"flag_conferma", "S")				
			
			dw_elenco_attrez_conferma.setitem(ll_i,"flag_morta","S")
			
		end if
		
	else
		
		// ** TIPOLOGIA DA PROGRAMMARE: infatti non esistono programmi
		
		dw_elenco_attrez_conferma.insertrow(0)
		
		dw_elenco_attrez_conferma.setitem(ll_i,"cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
		
		dw_elenco_attrez_conferma.setitem(ll_i,"des_tipo_manutenzione", ls_des_tipo_manutenzione)	
		
		dw_elenco_attrez_conferma.setitem(ll_i,"flag_conferma", "S")	
		
		dw_elenco_attrez_conferma.setitem(ll_i,"flag_morta","N")
		
	end if
	
	ll_i ++
loop
close cu_tipi_manutenzione;
commit;

dw_elenco_attrez_conferma.postevent("pcd_modify")
end event

type cb_annulla from commandbutton within w_elenco_tipi_man
integer x = 2075
integer y = 1440
integer width = 357
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null[]

s_cs_xx.parametri.parametro_s_1_a[] = ls_null[]

close(w_elenco_tipi_man)
end event

type cb_conferma from commandbutton within w_elenco_tipi_man
integer x = 2464
integer y = 1440
integer width = 357
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;long ll_i, ll_y
string ls_null[]

dw_elenco_attrez_conferma.accepttext()

ll_y = 1
s_cs_xx.parametri.parametro_s_1_a[] = ls_null[]

for ll_i = 1 to dw_elenco_attrez_conferma.rowcount()
	
	if dw_elenco_attrez_conferma.getitemstring(ll_i, "flag_conferma") = "S" then		
		s_cs_xx.parametri.parametro_s_1_a[ll_y] = dw_elenco_attrez_conferma.getitemstring(ll_i, "cod_tipo_manutenzione")
		ll_y ++
	end if	
	
next	

close(w_elenco_tipi_man)
end event

type dw_elenco_attrez_conferma from uo_cs_xx_dw within w_elenco_tipi_man
integer x = 23
integer y = 20
integer width = 2798
integer height = 1400
integer taborder = 10
string dataobject = "d_elenco_attrez_conferma"
boolean vscrollbar = true
end type

