﻿$PBExportHeader$w_esporta_scadenze_excel.srw
$PBExportComments$Finestra di Generazione file in excel con le scadenze delle manutenzioni.
forward
global type w_esporta_scadenze_excel from w_cs_xx_principale
end type
type pb_sfoglia from picturebutton within w_esporta_scadenze_excel
end type
type cb_esporta from commandbutton within w_esporta_scadenze_excel
end type
type dw_selezione from uo_cs_xx_dw within w_esporta_scadenze_excel
end type
type gb_1 from groupbox within w_esporta_scadenze_excel
end type
end forward

global type w_esporta_scadenze_excel from w_cs_xx_principale
integer width = 2469
integer height = 884
string title = "Esportazione Scadenze su Excel"
pb_sfoglia pb_sfoglia
cb_esporta cb_esporta
dw_selezione dw_selezione
gb_1 gb_1
end type
global w_esporta_scadenze_excel w_esporta_scadenze_excel

forward prototypes
public subroutine wf_crea_testata (ref oleobject myoleobject, long riga)
public subroutine wf_genera_date (ref oleobject myoleobject, long riga_testata, long colonna_testata, date data_inizio, date data_fine)
end prototypes

public subroutine wf_crea_testata (ref oleobject myoleobject, long riga);string ls_range

ls_range = "A" + string(riga) + ":C" + string(riga)

myoleobject.sheets("foglio1").rows(riga).rowheight = "75"
myoleobject.sheets("foglio1").range(ls_range).orientation = "90"
//myoleobject.sheets("foglio1").rows(riga).Interior.ColorIndex = "15"
//myoleobject.sheets("foglio1").rows(riga).font.size = "10"
myoleobject.sheets("foglio1").rows(riga).font.bold = true
myoleobject.sheets("foglio1").cells(riga,1).BorderAround("1","2")
myoleobject.sheets("foglio1").cells(riga,1).HorizontalAlignment = "1"
myoleobject.sheets("foglio1").cells(riga,1).value = "Codice Tipo Manutenzione"
myoleobject.sheets("foglio1").cells(riga,2).BorderAround("1","2")
myoleobject.sheets("foglio1").cells(riga,2).value = "Frequenza"
myoleobject.sheets("foglio1").cells(riga,3).BorderAround("1","2")
myoleobject.sheets("foglio1").cells(riga,3).value = "Reparto"
myoleobject.sheets("foglio1").cells(riga,4).BorderAround("1","2")
myoleobject.sheets("foglio1").cells(riga,4).HorizontalAlignment = "3"
myoleobject.sheets("foglio1").cells(riga,4).VerticalAlignment = "2"
myoleobject.sheets("foglio1").cells(riga,4).value = "DESCRIZIONE APPARECCHIATURA O IMPIANTO"
myoleobject.sheets("foglio1").range(ls_range).wraptext = true
ls_range = "A" + string(riga) + ":D" + string(riga)
myoleobject.sheets("foglio1").range(ls_range).HorizontalAlignment = "3"
myoleobject.sheets("foglio1").range(ls_range).VerticalAlignment = "2"
myoleobject.sheets("foglio1").range(ls_range).Interior.ColorIndex = "15"
//myoleobject.sheets("foglio1").rows(riga).BorderAround("1","4")
return


end subroutine

public subroutine wf_genera_date (ref oleobject myoleobject, long riga_testata, long colonna_testata, date data_inizio, date data_fine);string ls_data

do while data_inizio <= data_fine
	ls_data = string(data_inizio,"dd/mmm/yy")
	data_inizio = relativedate(data_inizio,1)
	myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).value = ls_data
	myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).font.size = "9"
	myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).orientation = "90"
	myoleobject.sheets("foglio1").columns(colonna_testata).ColumnWidth = "2"
	myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).HorizontalAlignment = "3"
	myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).VerticalAlignment = "2"
	myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).BorderAround("1","2")
	myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).Interior.ColorIndex = "15"
	colonna_testata++
loop
// inserisco la colonna di note
myoleobject.sheets("foglio1").columns(colonna_testata).ColumnWidth = "30"
myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).value = "NOTE"
myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).font.size = "12"
myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).orientation = "0"
myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).BorderAround("1","2")
myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).HorizontalAlignment = "3"
myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).VerticalAlignment = "2"
myoleobject.sheets("foglio1").cells(riga_testata,colonna_testata).Interior.ColorIndex = "15"
return


end subroutine

on w_esporta_scadenze_excel.create
int iCurrent
call super::create
this.pb_sfoglia=create pb_sfoglia
this.cb_esporta=create cb_esporta
this.dw_selezione=create dw_selezione
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.pb_sfoglia
this.Control[iCurrent+2]=this.cb_esporta
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.gb_1
end on

on w_esporta_scadenze_excel.destroy
call super::destroy
destroy(this.pb_sfoglia)
destroy(this.cb_esporta)
destroy(this.dw_selezione)
destroy(this.gb_1)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

pb_sfoglia.picturename = s_cs_xx.volume + s_cs_xx.risorse + "Listwiev.bmp"

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW (dw_selezione,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
f_PO_LoadDDDW_DW (dw_selezione,"cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  					  
end event

type pb_sfoglia from picturebutton within w_esporta_scadenze_excel
integer x = 2286
integer y = 540
integer width = 101
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
alignment htextalign = left!
end type

event clicked;string docname, named
integer value

docname = dw_selezione.getitemstring(1,"path")
if isnull(docname) then docname = "C:\DEFAULT.XLS"
	
value = GetFileSaveName("Selezione File", docname, named, "XLS", "Files Excel (*.XLS),*.XLS")

if value = 1 then
	dw_selezione.setitem(1,"path",docname)
end if
end event

type cb_esporta from commandbutton within w_esporta_scadenze_excel
integer x = 1989
integer y = 680
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esporta"
end type

event clicked;string ls_attrezzatura_inizio, ls_attrezzatura_fine, ls_cod_reparto, ls_cod_area, ls_path, ls_cod_tipo_manutenzione_old,&
       ls_cod_tipo_manutenzione, ls_cod_attrezzatura,ls_cod_attrezzatura_old, ls_periodicita, ls_des_reparto, ls_des_attrezzatura, &
		 ls_des_tipo_manutenzione, ls_range, ls_nome_azienda
long ll_righe, ll_i, ll_errore, ll_frequenza, ll_riga, ll_riga_testata, ll_colonna, ll_giorni
datetime ldt_data_inizio, ldt_data_fine, ldt_data_scadenza
datastore lds_scadenze
OLEObject myoleobject


ldt_data_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_data_fine = dw_selezione.getitemdatetime(1,"data_fine")
ls_attrezzatura_inizio = dw_selezione.getitemstring(1,"cod_attrezzatura_inizio")
ls_attrezzatura_fine = dw_selezione.getitemstring(1,"cod_attrezzatura_fine")
ls_cod_reparto = dw_selezione.getitemstring(1,"cod_reparto")
//if isnull(ls_cod_reparto) then ls_cod_reparto = "%"
ls_cod_area    = dw_selezione.getitemstring(1,"cod_area")
//if isnull(ls_cod_area) then ls_cod_area = "%"
ls_path = dw_selezione.getitemstring(1,"path")
if ldt_data_fine < ldt_data_inizio then
	g_mb.messagebox("OMNIA","Date incongruenti; verificare !!")
	return
end if

SELECT aziende.rag_soc_1  
 INTO :ls_nome_azienda  
 FROM aziende  
WHERE aziende.cod_azienda = :s_cs_xx.cod_azienda;

// ---------------------------------  INIZIO DELLA ROUTINE DI CARICAMENTO DATI ----------------------------------- //
lds_scadenze = CREATE datastore
lds_scadenze.dataobject= 'd_ds_esportazione_scadenze_excel'
lds_scadenze.settransobject(sqlca)
ll_righe = lds_scadenze.retrieve(s_cs_xx.cod_azienda, ls_attrezzatura_inizio, ls_attrezzatura_fine, ldt_data_inizio, ldt_data_fine,ls_cod_reparto,ls_cod_area)
if ll_righe = 0 then
	g_mb.messagebox("OMNIA","Nessun dato trovato in base alle selezion impostate.")
	return
end if
if ll_righe < 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore imprevisto durante l'estrazione dei dati: contattare il servizio di assistenza.")
	return
end if



// ---------------------------------  CREAZIONE DELLA TESTATA NEL FOGLIO EXCEL ----------------------------------- //
myoleobject = CREATE OLEObject
ll_errore = myoleobject.ConnectToNewObject("excel.application.9")
if ll_errore < 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return
end if
// caratteristiche del foglio in generale
myoleobject.Visible = True
myoleobject.Workbooks.Add
myoleobject.sheets("foglio1").PageSetup.PrintGridlines = True
myoleobject.sheets("foglio1").PageSetup.orientation = 2
myoleobject.sheets("foglio1").PageSetup.leftmargin = 40
myoleobject.sheets("foglio1").PageSetup.rightmargin = 40
myoleobject.sheets("foglio1").PageSetup.topmargin = 40
myoleobject.sheets("foglio1").PageSetup.bottommargin = 40
// imposto testata
myoleobject.sheets("foglio1").range("A1:D1").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A1:D1").merge(true)
myoleobject.sheets("foglio1").range("A1:D1").font.size = "14"
myoleobject.sheets("foglio1").range("A1:D1").Font.Bold = True
myoleobject.sheets("foglio1").range("A1:D1").Font.Italic = True
myoleobject.sheets("foglio1").range("A1:D1").font.ColorIndex = "1"
myoleobject.sheets("foglio1").range("A1:D1").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").cells(1,1).value = ls_nome_azienda
myoleobject.sheets("foglio1").range("A1:D1").Interior.ColorIndex = "3"
myoleobject.sheets("foglio1").range("A2:D2").Interior.ColorIndex = "3"

myoleobject.sheets("foglio1").range("A3:D3").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A3:D3").merge(true)
myoleobject.sheets("foglio1").range("A3:D3").font.size = "20"
myoleobject.sheets("foglio1").range("A3:D3").Font.Bold = True
myoleobject.sheets("foglio1").range("A3:D3").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").range("A3:D3").value = "- Dati generali Commessa -"
myoleobject.sheets("foglio1").range("A3:D3").Interior.ColorIndex = "36"
myoleobject.sheets("foglio1").range("A4:D4").Interior.ColorIndex = "36"

// wf_crea_testata
myoleobject.sheets("foglio1").columns(1).ColumnWidth = "9"
myoleobject.sheets("foglio1").columns(2).ColumnWidth = "9"
myoleobject.sheets("foglio1").columns(3).ColumnWidth = "9"
myoleobject.sheets("foglio1").columns(4).ColumnWidth = "50"

ls_cod_tipo_manutenzione_old = ""
ls_cod_attrezzatura_old = ""
ll_riga = 5
for ll_i = 1 to ll_righe
	ls_cod_attrezzatura = lds_scadenze.getitemstring(ll_i, "cod_attrezzatura")
	ls_des_attrezzatura    = lds_scadenze.getitemstring(ll_i, "descrizione_attrezzatura")
	ll_frequenza           = lds_scadenze.getitemnumber(ll_i, "frequenza_manutenzione")
	ls_periodicita         = lds_scadenze.getitemstring(ll_i, "periodicita")
	ls_cod_reparto         = lds_scadenze.getitemstring(ll_i, "cod_reparto")
	ls_des_reparto         = lds_scadenze.getitemstring(ll_i, "des_reparto")
	ls_des_attrezzatura    = lds_scadenze.getitemstring(ll_i, "descrizione_attrezzatura")
	ls_cod_tipo_manutenzione = lds_scadenze.getitemstring(ll_i, "tipo_manutenzione")
	ldt_data_scadenza 	  	 = lds_scadenze.getitemdatetime(ll_i,"data_giorno")
	
	if ls_cod_attrezzatura <> ls_cod_attrezzatura_old then   // **********  creazione della testata al cambio attrezzatura con le varie date
		ll_riga_testata = ll_riga
		wf_crea_testata(myoleobject, ll_riga_testata)
		ls_cod_attrezzatura_old = ls_cod_attrezzatura
		ll_riga ++
		myoleobject.sheets("foglio1").cells(ll_riga,4).value = ls_des_attrezzatura + " (Attrezzatura)"
		myoleobject.sheets("foglio1").cells(ll_riga,4).font.size = "10"
		myoleobject.sheets("foglio1").cells(ll_riga,4).font.Bold = True
		ll_colonna = 5
		wf_genera_date(myoleobject, ll_riga_testata,ll_colonna, date(ldt_data_inizio), date(ldt_data_fine))
	end if
	
	if ls_cod_tipo_manutenzione <> ls_cod_tipo_manutenzione_old then
		ls_cod_tipo_manutenzione_old = ls_cod_tipo_manutenzione
		
		SELECT des_tipo_manutenzione  
		INTO   :ls_des_tipo_manutenzione  
		FROM   tab_tipi_manutenzione  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		       cod_attrezzatura = :ls_cod_attrezzatura and
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		
		ll_riga ++
		
		ls_range = "A" + string(ll_riga) + ":D" + string(ll_riga)
		
		myoleobject.sheets("foglio1").range(ls_range).font.name = "Arial"
		myoleobject.sheets("foglio1").range(ls_range).font.size = "8"
		myoleobject.sheets("foglio1").range(ls_range).HorizontalAlignment = "3"
		myoleobject.sheets("foglio1").range(ls_range).VerticalAlignment = "2"
		myoleobject.sheets("foglio1").cells(ll_riga,1).value = ls_cod_tipo_manutenzione
		choose case ls_periodicita
			case "M"
				ls_periodicita = "Min"
			case "O"
				ls_periodicita = "Ore"
			case "G"
				ls_periodicita = "GG"
			case "S"
				ls_periodicita = "Settim"
			case "E"
				ls_periodicita = "Mesi"
		end choose
		
		myoleobject.sheets("foglio1").cells(ll_riga,2).value = string(ll_frequenza) + " " + ls_periodicita
		myoleobject.sheets("foglio1").cells(ll_riga,3).value = ls_cod_reparto
		myoleobject.sheets("foglio1").cells(ll_riga,4).HorizontalAlignment = "1"
		myoleobject.sheets("foglio1").cells(ll_riga,4).value = trim(ls_des_tipo_manutenzione)
		ll_giorni = daysafter(date(ldt_data_inizio), date(ldt_data_scadenza))
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).HorizontalAlignment = "3"
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).VerticalAlignment = "2"
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).value = "X"
	else
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).font.name = "Arial"
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).font.size = "9"
		ll_giorni = daysafter(date(ldt_data_inizio), date(ldt_data_scadenza))
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).HorizontalAlignment = "3"
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).VerticalAlignment = "2"
		myoleobject.sheets("foglio1").cells(ll_riga,ll_colonna+ll_giorni).value = "X"
		// scrivo solo la data successiva
	end if
next

if not isnull(ls_path) or len(ls_path) > 0 then myoleobject.sheets("foglio1").Saveas(ls_path)

destroy myoleobject


end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura_inizio"
end event

type dw_selezione from uo_cs_xx_dw within w_esporta_scadenze_excel
integer x = 46
integer y = 40
integer width = 2354
integer height = 580
integer taborder = 10
string dataobject = "d_selezione_esportazione_scadenze_excel"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_des_attrezzatura

if i_extendmode then
	choose case i_colname
		case "cod_attrezzatura_inizio"
			select descrizione
			into :ls_des_attrezzatura
			from anag_attrezzature
			where cod_azienda = :s_cs_xx.cod_azienda and
			      cod_attrezzatura = :i_coltext;
			if sqlca.sqlcode <> 0 then return 1
		case "cod_attrezzatura_fine"
			select descrizione
			into :ls_des_attrezzatura
			from anag_attrezzature
			where cod_azienda = :s_cs_xx.cod_azienda and
			      cod_attrezzatura = :i_coltext;
			if sqlca.sqlcode <> 0 then return 1
	end choose
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	setitem(getrow(),"data_inizio",datetime(date("01/01/1900"),00:00:00))
	setitem(getrow(),"data_fine",  datetime(date("31/12/2099"),00:00:00))
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura_inizio")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura_fine")
end choose
end event

type gb_1 from groupbox within w_esporta_scadenze_excel
integer x = 23
integer width = 2377
integer height = 660
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
end type

