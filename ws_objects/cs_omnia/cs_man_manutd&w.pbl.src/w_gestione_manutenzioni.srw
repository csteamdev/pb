﻿$PBExportHeader$w_gestione_manutenzioni.srw
$PBExportComments$Finestra Gestione integrata manutenzioni e tarature
forward
global type w_gestione_manutenzioni from w_cs_xx_principale
end type
type dw_attrezzature_lista from uo_cs_xx_dw within w_gestione_manutenzioni
end type
type em_data_inizio from editmask within w_gestione_manutenzioni
end type
type st_2 from statictext within w_gestione_manutenzioni
end type
type st_3 from statictext within w_gestione_manutenzioni
end type
type em_data_fine from editmask within w_gestione_manutenzioni
end type
type ddlb_dati from dropdownlistbox within w_gestione_manutenzioni
end type
type st_4 from statictext within w_gestione_manutenzioni
end type
type dw_piani_manutenzione_dettaglio from uo_cs_xx_dw within w_gestione_manutenzioni
end type
type dw_folder from u_folder within w_gestione_manutenzioni
end type
type dw_lista_piani_manut_attrezzatura from uo_cs_xx_dw within w_gestione_manutenzioni
end type
type dw_scadenze_manutenzioni from uo_cs_xx_dw within w_gestione_manutenzioni
end type
type dw_attrezzature_dettaglio from uo_cs_xx_dw within w_gestione_manutenzioni
end type
end forward

global type w_gestione_manutenzioni from w_cs_xx_principale
integer width = 3529
integer height = 1924
string title = "Manutenzioni"
dw_attrezzature_lista dw_attrezzature_lista
em_data_inizio em_data_inizio
st_2 st_2
st_3 st_3
em_data_fine em_data_fine
ddlb_dati ddlb_dati
st_4 st_4
dw_piani_manutenzione_dettaglio dw_piani_manutenzione_dettaglio
dw_folder dw_folder
dw_lista_piani_manut_attrezzatura dw_lista_piani_manut_attrezzatura
dw_scadenze_manutenzioni dw_scadenze_manutenzioni
dw_attrezzature_dettaglio dw_attrezzature_dettaglio
end type
global w_gestione_manutenzioni w_gestione_manutenzioni

type variables
boolean ib_ricerca
string is_flag_manutenzione
end variables

forward prototypes
public function integer wf_prod_codificato (string ws_flag_prod_codificato)
public function integer wf_flag_tipo_manutenzione (string ws_flag_tipo_manut)
end prototypes

public function integer wf_prod_codificato (string ws_flag_prod_codificato);choose case ws_flag_prod_codificato
case "S"
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_ricambio.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("des_ricambio_non_codificato_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("des_ricambio_non_codificato.Visible=0")

   dw_piani_manutenzione_dettaglio.Modify("flag_ricambio_codificato.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("flag_ricambio_codificato_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("quan_ricambi_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("quan_ricambi.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("costo_ricambio_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("costo_ricambio.Visible=1")

   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_alternativo_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_ricambio_alternativo.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_alternativo.Visible=1")

   dw_piani_manutenzione_dettaglio.Modify("ret_1.Visible=1")
case "N"
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_ricambio.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("des_ricambio_non_codificato_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("des_ricambio_non_codificato.Visible=1")


   dw_piani_manutenzione_dettaglio.Modify("flag_ricambio_codificato.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("flag_ricambio_codificato_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("quan_ricambi_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("quan_ricambi.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("costo_ricambio_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("costo_ricambio.Visible=1")

   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_alternativo_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_ricambio_alternativo.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_alternativo.Visible=0")

   dw_piani_manutenzione_dettaglio.Modify("ret_1.Visible=1")
case "T"
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_ricambio.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("des_ricambio_non_codificato_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("des_ricambio_non_codificato.Visible=0")

   dw_piani_manutenzione_dettaglio.Modify("flag_ricambio_codificato.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("flag_ricambio_codificato_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("quan_ricambi_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("quan_ricambi.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("costo_ricambio_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("costo_ricambio.Visible=0")

   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_alternativo_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_ricambio_alternativo.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_ricambio_alternativo.Visible=0")

   dw_piani_manutenzione_dettaglio.Modify("ret_1.Visible=0")
end choose
return 0   



end function

public function integer wf_flag_tipo_manutenzione (string ws_flag_tipo_manut);choose case ws_flag_tipo_manut
case "M"
   if dw_piani_manutenzione_dettaglio.getrow() > 0 then
      wf_prod_codificato(dw_piani_manutenzione_dettaglio.getitemstring( &
                      dw_piani_manutenzione_dettaglio.getrow(),"flag_ricambio_codificato") )
   end if
   dw_piani_manutenzione_dettaglio.Modify("des_taratura_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("des_taratura.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_attrezzatura_taratura_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_attrezzatura_taratura.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_attrezzatura_taratura.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_misura.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cod_misura_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_misura.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("valore_min_taratura.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("valore_min_taratura_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("valore_max_taratura.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("valore_max_taratura_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("valore_atteso.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("valore_atteso_t.Visible=0")
   dw_piani_manutenzione_dettaglio.Modify("ret_2.Visible=0")
case "T"
   dw_piani_manutenzione_dettaglio.Modify("des_taratura_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("des_taratura.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cod_attrezzatura_taratura_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cod_attrezzatura_taratura.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_attrezzatura_taratura.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cod_misura.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cod_misura_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("cf_cod_misura.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("valore_min_taratura.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("valore_min_taratura_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("valore_max_taratura.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("valore_max_taratura_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("valore_atteso.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("valore_atteso_t.Visible=1")
   dw_piani_manutenzione_dettaglio.Modify("ret_2.Visible=1")
   wf_prod_codificato("T")
end choose
return 0

end function

on w_gestione_manutenzioni.create
int iCurrent
call super::create
this.dw_attrezzature_lista=create dw_attrezzature_lista
this.em_data_inizio=create em_data_inizio
this.st_2=create st_2
this.st_3=create st_3
this.em_data_fine=create em_data_fine
this.ddlb_dati=create ddlb_dati
this.st_4=create st_4
this.dw_piani_manutenzione_dettaglio=create dw_piani_manutenzione_dettaglio
this.dw_folder=create dw_folder
this.dw_lista_piani_manut_attrezzatura=create dw_lista_piani_manut_attrezzatura
this.dw_scadenze_manutenzioni=create dw_scadenze_manutenzioni
this.dw_attrezzature_dettaglio=create dw_attrezzature_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_attrezzature_lista
this.Control[iCurrent+2]=this.em_data_inizio
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_3
this.Control[iCurrent+5]=this.em_data_fine
this.Control[iCurrent+6]=this.ddlb_dati
this.Control[iCurrent+7]=this.st_4
this.Control[iCurrent+8]=this.dw_piani_manutenzione_dettaglio
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.dw_lista_piani_manut_attrezzatura
this.Control[iCurrent+11]=this.dw_scadenze_manutenzioni
this.Control[iCurrent+12]=this.dw_attrezzature_dettaglio
end on

on w_gestione_manutenzioni.destroy
call super::destroy
destroy(this.dw_attrezzature_lista)
destroy(this.em_data_inizio)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.em_data_fine)
destroy(this.ddlb_dati)
destroy(this.st_4)
destroy(this.dw_piani_manutenzione_dettaglio)
destroy(this.dw_folder)
destroy(this.dw_lista_piani_manut_attrezzatura)
destroy(this.dw_scadenze_manutenzioni)
destroy(this.dw_attrezzature_dettaglio)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

em_data_fine.text = string(relativedate(today(),60))
em_data_inizio.text = string(relativedate(today(),-15))
ddlb_dati.selectitem("Tutti", 1)
ib_ricerca = false
dw_lista_piani_manut_attrezzatura.set_dw_options(sqlca, &
                                         pcca.null_object, &
													  c_nonew + c_nomodify + c_nodelete + c_scrollparent + c_noretrieveonopen, &
													  c_default)
													  
dw_piani_manutenzione_dettaglio.set_dw_options(sqlca, &
                                         dw_lista_piani_manut_attrezzatura, &
													  c_nonew + c_nomodify + c_nodelete+ c_sharedata + c_scrollparent, &
													  c_default)

dw_attrezzature_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
												 c_nonew + c_nomodify + c_nodelete, &
												 c_default)

dw_attrezzature_dettaglio.set_dw_options(sqlca, &
                                         dw_attrezzature_lista, &
													  c_nonew + c_nomodify + c_nodelete + c_sharedata + c_scrollparent, &
													  c_default)

dw_scadenze_manutenzioni.set_dw_options(sqlca, &
                                  pcca.null_object, &
											 c_nonew + &
											 c_nomodify + &
											 c_nodelete + &
											 c_disableCC, &
											 c_noresizedw + &
                                  c_nohighlightselected + &
                                  c_nocursorrowpointer + &
                                  c_nocursorrowfocusrect )

ib_ricerca = true



iuo_dw_main = dw_attrezzature_lista


lw_oggetti[1] = dw_attrezzature_dettaglio
dw_folder.fu_assigntab(1, "Attrezzatura", lw_oggetti[])
lw_oggetti[1] = dw_piani_manutenzione_dettaglio
lw_oggetti[2] = dw_lista_piani_manut_attrezzatura
dw_folder.fu_assigntab(2, "Pianificazione", lw_oggetti[])
lw_oggetti[1] = dw_scadenze_manutenzioni
lw_oggetti[2] = ddlb_dati
lw_oggetti[3] = em_data_inizio
lw_oggetti[4] = em_data_fine
lw_oggetti[5] = st_2
lw_oggetti[6] = st_3
lw_oggetti[7] = st_4
dw_folder.fu_assigntab(3, "Scadenzario", lw_oggetti[])
dw_folder.fu_foldercreate(3, 3)
dw_folder.fu_selecttab(1)


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_attrezzature_dettaglio,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
					  "anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((anag_reparti.flag_blocco <> 'S') or (anag_reparti.flag_blocco = 'S' and anag_reparti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_attrezzature_dettaglio,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW(dw_attrezzature_dettaglio,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
					  "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana='N'")

f_PO_LoadDDDW_DW(dw_attrezzature_dettaglio,"grado_precisione_um",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((tab_misure.flag_blocco <> 'S') or (tab_misure.flag_blocco = 'S' and tab_misure.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDLB_DW(dw_attrezzature_dettaglio,"cod_utente",sqlca,&
                 "utenti","cod_utente","cod_utente", &
					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_attrezzature_dettaglio,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 " (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "')")  


f_PO_LoadDDDW_DW(dw_piani_manutenzione_dettaglio,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_piani_manutenzione_dettaglio,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_piani_manutenzione_dettaglio,"cod_attrezzatura_taratura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_piani_manutenzione_dettaglio,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_piani_manutenzione_dettaglio,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_piani_manutenzione_dettaglio,"cod_ricambio",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_piani_manutenzione_dettaglio,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

end event

type dw_attrezzature_lista from uo_cs_xx_dw within w_gestione_manutenzioni
integer x = 23
integer y = 20
integer width = 3451
integer height = 400
integer taborder = 70
string dataobject = "d_gestione_attrezzature_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_attrezzatura
if i_extendmode and ib_ricerca and getrow() > 0 then
	dw_lista_piani_manut_attrezzatura.change_dw_current()
	iuo_dw_main = dw_lista_piani_manut_attrezzatura
	parent.triggerevent("pc_retrieve")
	dw_scadenze_manutenzioni.change_dw_current()
	iuo_dw_main = dw_scadenze_manutenzioni
	parent.triggerevent("pc_retrieve")
	dw_attrezzature_lista.change_dw_current()
	iuo_dw_main = dw_attrezzature_lista
	
end if
end event

type em_data_inizio from editmask within w_gestione_manutenzioni
integer x = 594
integer y = 580
integer width = 389
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "4"
string minmax = "01/01/1900~~31/12/2999"
end type

event modified;choose case ddlb_dati.text
	case "Manutenzioni"
		is_flag_manutenzione = "M"
	case "Tarature"
		is_flag_manutenzione = "T"
	case "Tutti"
		is_flag_manutenzione = "%"
end choose
dw_scadenze_manutenzioni.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type st_2 from statictext within w_gestione_manutenzioni
integer x = 91
integer y = 580
integer width = 498
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Da Data Scadenza:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_gestione_manutenzioni
integer x = 1029
integer y = 580
integer width = 498
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "A Data Scadenza:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_fine from editmask within w_gestione_manutenzioni
integer x = 1531
integer y = 580
integer width = 389
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "d"
string minmax = "01/01/1900~~31/12/2999"
end type

event modified;choose case ddlb_dati.text
	case "Manutenzioni"
		is_flag_manutenzione = "M"
	case "Tarature"
		is_flag_manutenzione = "T"
	case "Tutti"
		is_flag_manutenzione = "%"
end choose
dw_scadenze_manutenzioni.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type ddlb_dati from dropdownlistbox within w_gestione_manutenzioni
integer x = 2103
integer y = 580
integer width = 503
integer height = 320
integer taborder = 43
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
string item[] = {"Manutenzioni","Tarature","Tutti"}
end type

event selectionchanged;choose case ddlb_dati.text
	case "Manutenzioni"
		is_flag_manutenzione = "M"
	case "Tarature"
		is_flag_manutenzione = "T"
	case "Tutti"
		is_flag_manutenzione = "%"
end choose
dw_scadenze_manutenzioni.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type st_4 from statictext within w_gestione_manutenzioni
integer x = 1943
integer y = 580
integer width = 123
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Dati:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_piani_manutenzione_dettaglio from uo_cs_xx_dw within w_gestione_manutenzioni
integer x = 709
integer y = 560
integer width = 2560
integer height = 1200
integer taborder = 40
string dataobject = "d_piani_manutenzione_dettaglio"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_folder from u_folder within w_gestione_manutenzioni
integer x = 23
integer y = 440
integer width = 3451
integer height = 1360
integer taborder = 20
end type

type dw_lista_piani_manut_attrezzatura from uo_cs_xx_dw within w_gestione_manutenzioni
integer x = 46
integer y = 560
integer width = 640
integer height = 1220
integer taborder = 30
string dataobject = "d_lista_piani_manut_attrezzatura"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_attrezzatura
long   l_error

ls_cod_attrezzatura = dw_attrezzature_lista.getitemstring(dw_attrezzature_lista.getrow(),"cod_attrezzatura")
l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   wf_flag_tipo_manutenzione(this.getitemstring(this.getrow(), "flag_manutenzione"))
	if this.getitemstring(this.getrow(), "flag_manutenzione") = "M" then
		wf_prod_codificato(this.getitemstring(this.getrow(), "flag_ricambio_codificato"))
	end if
end if
end event

type dw_scadenze_manutenzioni from uo_cs_xx_dw within w_gestione_manutenzioni
event ue_post_retrieve ( )
integer x = 46
integer y = 680
integer width = 3406
integer height = 1096
integer taborder = 10
string dataobject = "d_scad_manutenzioni_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_post_retrieve;call super::ue_post_retrieve;dw_scadenze_manutenzioni.Modify("schede_manutenzioni_data_intervento.Color='0~tif( schede_manutenzioni_data_intervento > datetime(today())  , 0, 255 )'")

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   date(em_data_inizio.text), &
						 date(em_data_fine.text), &
						 dw_attrezzature_lista.getitemstring(dw_attrezzature_lista.getrow(),"cod_attrezzatura"), &
						 is_flag_manutenzione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

postevent("ue_post_retrieve")
end event

type dw_attrezzature_dettaglio from uo_cs_xx_dw within w_gestione_manutenzioni
integer x = 46
integer y = 560
integer width = 2674
integer height = 1220
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_attrezzature_dettaglio"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

