﻿$PBExportHeader$w_graf_pareto.srw
$PBExportComments$Finestra Grafici Pareto (da ereditare)
forward
global type w_graf_pareto from w_cs_xx_principale
end type
type uo_grafico from uo_std_chart within w_graf_pareto
end type
type dw_pareto_guasti from uo_cs_xx_dw within w_graf_pareto
end type
type uo_graf_cumulata from uo_std_chart within w_graf_pareto
end type
type dw_guasti_cumulata from uo_cs_xx_dw within w_graf_pareto
end type
end forward

global type w_graf_pareto from w_cs_xx_principale
integer x = 110
integer y = 248
integer width = 3397
integer height = 1588
string title = "Grafici Guasti Attrezzature"
boolean maxbox = false
boolean resizable = false
uo_grafico uo_grafico
dw_pareto_guasti dw_pareto_guasti
uo_graf_cumulata uo_graf_cumulata
dw_guasti_cumulata dw_guasti_cumulata
end type
global w_graf_pareto w_graf_pareto

type variables
string is_graf1_titolo, is_graf1_des_asse_x, is_graf1_des_asse_y
string is_graf2_titolo, is_graf2_des_asse_x, is_graf2_des_asse_y

end variables

forward prototypes
public function integer wf_pareto_init (string ps_sql)
end prototypes

public function integer wf_pareto_init (string ps_sql);//
// Function : WF_PARETO_INIT (ps_sql)
// funzione che inizializza la rappresentazione grafica di Pareto a partire dal DB
// Parametri : sintassi SQL che deve passarmi il contatore dei guasti e la relativa descrizione
// Restituisce : il numero di guasti estratti dal DataBase (per verifica correttezza SQL)
//

string ls_cod_guasto, ls_cont_cumulata, ls_graf2_des_asse_x
integer li_cont = 0, li_tot_guasti = 0, li_count_cod_guasto, li_cont_20X100, li_divisore

//
// Dichiarazione delle caratteristiche grafiche del grafico
//

declare cur_Graph dynamic cursor for sqlsa;

prepare sqlsa from :ps_sql;

open dynamic cur_Graph;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella composizione SQL per l'estrazione dei dati~r~n" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

FETCH cur_Graph INTO :li_count_cod_guasto, :ls_cod_guasto ;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore nella lettura dei dati dei dati~r~n" + sqlca.sqlerrtext)
	close cur_Graph;
	rollback;
	return -1
end if

li_cont = 0
DO WHILE SQLCA.sqlcode = 0
	li_cont ++
	li_tot_guasti = li_tot_guasti + li_count_cod_guasto
	FETCH cur_Graph INTO :li_count_cod_guasto, :ls_cod_guasto ;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella lettura dei dati dei dati~r~n" + sqlca.sqlerrtext)
		close cur_Graph;
		rollback;
		return -1
	end if
loop	
close cur_Graph;
rollback;

if li_cont > 0 then
	li_cont_20X100 = round( (li_cont/5), 0 )       //calcolo il 20% delle tipologie di guasto	
	do while li_cont_20X100 > 26                   //in formato lettere
		li_divisore = int( li_cont_20X100/26 )
		li_cont_20X100 = Mod( li_cont_20X100, 26 )
		ls_cont_cumulata = ls_cont_cumulata + char( li_divisore + 64 )
	loop
	if li_cont_20X100 > 0 then
		ls_cont_cumulata = ls_cont_cumulata + char( li_cont_20X100 + 64 ) + "2"
	else
		ls_cont_cumulata = "A1"
	end if


	open dynamic cur_Graph;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella composizione SQL per l'estrazione dei dati~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	FETCH cur_Graph INTO :li_count_cod_guasto, :ls_cod_guasto ;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella lettura dei dati dei dati~r~n" + sqlca.sqlerrtext)
		close cur_Graph;
		rollback;
		return -1
	end if

	// ----   Adesso carico dati ed etichette negli oggetti DataGrid
	
	uo_grafico.of_init(9, 0, is_graf1_titolo, is_graf1_des_asse_x, is_graf1_des_asse_y, &
						 "", "A1:B" + string(li_Cont) , "", True, False, False)

	uo_graf_cumulata.of_init(9, 0, is_graf2_titolo, ls_graf2_des_asse_x, is_graf2_des_asse_y, &
						 "", "A1:" + ls_cont_cumulata , "", False, True, False)

	uo_graf_cumulata.ole_graph.object.Stacking = True


	
	ls_graf2_des_asse_x = is_graf2_des_asse_x + "~r~n"
	li_cont = 0
	DO WHILE SQLCA.sqlcode = 0
		li_cont ++
		uo_grafico.ole_Graph.object.datagrid.RowLabel(li_Cont, 1, left(ls_cod_guasto, 9))
		uo_grafico.ole_graph.object.datagrid.setdata(li_cont, 1,li_count_cod_guasto,False)	
		if li_cont <= li_cont_20X100 then
			ls_graf2_des_asse_x += " " + ls_cod_guasto
			uo_graf_cumulata.ole_graph.object.datagrid.setdata( 1, li_cont, round( (li_count_cod_guasto/li_tot_guasti)*100, 0 ) ,False)	
		end if
		FETCH cur_Graph INTO :li_count_cod_guasto, :ls_cod_guasto ;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nella lettura dei dati dei dati~r~n" + sqlca.sqlerrtext)
			close cur_Graph;
			rollback;
			return -1
		end if
	loop	
	// richiamo per la seconda volta per mettere il titolo che voglio io.
	uo_graf_cumulata.of_init(9, 0, is_graf2_titolo, ls_graf2_des_asse_x, is_graf2_des_asse_y, &
						 "", "A1:" + ls_cont_cumulata , "", False, True, False)


	close cur_Graph;

else
	uo_grafico.of_init(9, 0, is_graf1_titolo, is_graf1_des_asse_x, is_graf1_des_asse_y, &
						 "", "" , "", False, False, False)
	uo_graf_cumulata.of_init(9, 0, is_graf2_titolo, is_graf2_des_asse_x, is_graf2_des_asse_y, &
						 "", "", "", False, False, False)

	g_mb.messagebox("Omnia"," Non ci sono dati nell' intervallo di date selezionato ! ",Exclamation!)
end if	
close cur_Graph;
rollback;
return li_cont
end function

on w_graf_pareto.create
int iCurrent
call super::create
this.uo_grafico=create uo_grafico
this.dw_pareto_guasti=create dw_pareto_guasti
this.uo_graf_cumulata=create uo_graf_cumulata
this.dw_guasti_cumulata=create dw_guasti_cumulata
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_grafico
this.Control[iCurrent+2]=this.dw_pareto_guasti
this.Control[iCurrent+3]=this.uo_graf_cumulata
this.Control[iCurrent+4]=this.dw_guasti_cumulata
end on

on w_graf_pareto.destroy
call super::destroy
destroy(this.uo_grafico)
destroy(this.dw_pareto_guasti)
destroy(this.uo_graf_cumulata)
destroy(this.dw_guasti_cumulata)
end on

type uo_grafico from uo_std_chart within w_graf_pareto
integer width = 1691
integer height = 1500
integer taborder = 10
boolean bringtotop = true
boolean enabled = false
boolean border = true
borderstyle borderstyle = stylelowered!
end type

on uo_grafico.destroy
call uo_std_chart::destroy
end on

type dw_pareto_guasti from uo_cs_xx_dw within w_graf_pareto
boolean visible = false
integer x = 46
integer y = 20
integer width = 1669
integer height = 1040
integer taborder = 20
string dataobject = "d_pareto_guasti"
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type uo_graf_cumulata from uo_std_chart within w_graf_pareto
integer x = 1691
integer width = 1691
integer height = 1500
integer taborder = 21
boolean enabled = false
boolean border = true
borderstyle borderstyle = stylelowered!
end type

on uo_graf_cumulata.destroy
call uo_std_chart::destroy
end on

type dw_guasti_cumulata from uo_cs_xx_dw within w_graf_pareto
boolean visible = false
integer x = 1737
integer y = 20
integer width = 1646
integer height = 1040
integer taborder = 30
string dataobject = "d_guasti_cumulata"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

