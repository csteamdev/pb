﻿$PBExportHeader$w_grafici_guasti.srw
$PBExportComments$Finestra Grafico di Pareto Guasti
forward
global type w_grafici_guasti from w_graf_pareto
end type
end forward

global type w_grafici_guasti from w_graf_pareto
end type
global w_grafici_guasti w_grafici_guasti

event open;call super::open;string ls_sql

ls_sql = "SELECT COUNT(COD_GUASTO),   " + &
         "schede_manutenzioni.cod_guasto  " + &
         "FROM schede_manutenzioni  " + &
         "WHERE ( schede_manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
              "( schede_manutenzioni.cod_guasto is not null )   " + &
         "GROUP BY schede_manutenzioni.cod_guasto  " + &
         "ORDER BY 1 DESC   "

wf_pareto_init (ls_sql)
end event

on w_grafici_guasti.create
call w_graf_pareto::create
end on

on w_grafici_guasti.destroy
call w_graf_pareto::destroy
end on

type uo_grafico from w_graf_pareto`uo_grafico within w_grafici_guasti
boolean BringToTop=true
end type

