﻿$PBExportHeader$w_input_data_prossimo_intervento.srw
$PBExportComments$Finestra Response Data Prossimo Intervento
forward
global type w_input_data_prossimo_intervento from w_cs_xx_risposta
end type
type st_1 from statictext within w_input_data_prossimo_intervento
end type
type st_2 from statictext within w_input_data_prossimo_intervento
end type
type em_prossimo_intervento from editmask within w_input_data_prossimo_intervento
end type
type cb_chiudi from commandbutton within w_input_data_prossimo_intervento
end type
type cb_annulla from commandbutton within w_input_data_prossimo_intervento
end type
type gb_1 from groupbox within w_input_data_prossimo_intervento
end type
end forward

global type w_input_data_prossimo_intervento from w_cs_xx_risposta
int Width=1527
int Height=529
boolean TitleBar=true
string Title="Programmazione Manutenzione"
st_1 st_1
st_2 st_2
em_prossimo_intervento em_prossimo_intervento
cb_chiudi cb_chiudi
cb_annulla cb_annulla
gb_1 gb_1
end type
global w_input_data_prossimo_intervento w_input_data_prossimo_intervento

on w_input_data_prossimo_intervento.create
int iCurrent
call w_cs_xx_risposta::create
this.st_1=create st_1
this.st_2=create st_2
this.em_prossimo_intervento=create em_prossimo_intervento
this.cb_chiudi=create cb_chiudi
this.cb_annulla=create cb_annulla
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=st_2
this.Control[iCurrent+3]=em_prossimo_intervento
this.Control[iCurrent+4]=cb_chiudi
this.Control[iCurrent+5]=cb_annulla
this.Control[iCurrent+6]=gb_1
end on

on w_input_data_prossimo_intervento.destroy
call w_cs_xx_risposta::destroy
destroy(this.st_1)
destroy(this.st_2)
destroy(this.em_prossimo_intervento)
destroy(this.cb_chiudi)
destroy(this.cb_annulla)
destroy(this.gb_1)
end on

type st_1 from statictext within w_input_data_prossimo_intervento
int X=23
int Y=21
int Width=1441
int Height=101
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Programmazione Prossimo Intervento"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-12
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_input_data_prossimo_intervento
int X=161
int Y=161
int Width=686
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Data Prossimo Intervento:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_prossimo_intervento from editmask within w_input_data_prossimo_intervento
int X=869
int Y=161
int Width=412
int Height=81
int TabOrder=20
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_chiudi from commandbutton within w_input_data_prossimo_intervento
int X=732
int Y=261
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_data_1 = datetime(date(em_prossimo_intervento.text))
parent.triggerevent("pc_close")
end event

type cb_annulla from commandbutton within w_input_data_prossimo_intervento
int X=343
int Y=261
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;parent.triggerevent("pc_close")
end event

type gb_1 from groupbox within w_input_data_prossimo_intervento
int X=23
int Y=101
int Width=1441
int Height=301
int TabOrder=31
long TextColor=8388608
long BackColor=79741120
int TextSize=-12
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

