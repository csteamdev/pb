﻿$PBExportHeader$w_manutenzioni.srw
$PBExportComments$Finestra Manutenzioni
forward
global type w_manutenzioni from w_cs_xx_principale
end type
type cb_note_1 from uo_ext_note_mss_1 within w_manutenzioni
end type
type cb_richiama_predefinito from commandbutton within w_manutenzioni
end type
type cb_predefinito from commandbutton within w_manutenzioni
end type
type cb_ricambi from commandbutton within w_manutenzioni
end type
type cb_operatori_manut from commandbutton within w_manutenzioni
end type
type cb_scadenzario from commandbutton within w_manutenzioni
end type
type cb_salva_nav from commandbutton within w_manutenzioni
end type
type cb_ricerca_ricambio from cb_prod_ricerca within w_manutenzioni
end type
type cb_conferma from commandbutton within w_manutenzioni
end type
type cb_tarature from commandbutton within w_manutenzioni
end type
type cb_cerca from commandbutton within w_manutenzioni
end type
type dw_manutenzioni_2 from uo_cs_xx_dw within w_manutenzioni
end type
type cb_controllo from commandbutton within w_manutenzioni
end type
type dw_folder from u_folder within w_manutenzioni
end type
type dw_filtro from datawindow within w_manutenzioni
end type
type dw_folder_search from u_folder within w_manutenzioni
end type
type tv_1 from treeview within w_manutenzioni
end type
type dw_manutenzioni_1 from uo_cs_xx_dw within w_manutenzioni
end type
type ws_record from structure within w_manutenzioni
end type
end forward

type ws_record from structure
	long		anno_registrazione
	long		num_registrazione
	string		des_manutenzione
	string		codice
	string		descrizione
	long		handle_padre
	string		tipo_livello
	integer		livello
end type

global type w_manutenzioni from w_cs_xx_principale
integer width = 3712
integer height = 2288
string title = "Manutenzioni & Tarature"
event ue_close ( )
event ue_controlla_navigatore ( )
event ue_carica_singola_registrazione ( )
event type integer pc_menu_valorizza ( )
event pc_menu_documenti ( )
event pc_menu_registra_sch ( )
event pc_menu_report_sch ( )
event pc_menu_report_ol ( )
event pc_menu_destinatari ( )
event pc_menu_pianificazioni ( )
cb_note_1 cb_note_1
cb_richiama_predefinito cb_richiama_predefinito
cb_predefinito cb_predefinito
cb_ricambi cb_ricambi
cb_operatori_manut cb_operatori_manut
cb_scadenzario cb_scadenzario
cb_salva_nav cb_salva_nav
cb_ricerca_ricambio cb_ricerca_ricambio
cb_conferma cb_conferma
cb_tarature cb_tarature
cb_cerca cb_cerca
dw_manutenzioni_2 dw_manutenzioni_2
cb_controllo cb_controllo
dw_folder dw_folder
dw_filtro dw_filtro
dw_folder_search dw_folder_search
tv_1 tv_1
dw_manutenzioni_1 dw_manutenzioni_1
end type
global w_manutenzioni w_manutenzioni

type variables
boolean ib_carica_tutto=FALSE, ib_retrieve=true, ib_fam = false
long il_handle, il_anno_registrazione, il_num_registrazione, il_livello_corrente,il_handle_modificato,il_handle_cancellato, &
     il_anno_reg_ricerca, il_num_reg_ricerca
string is_tipo_manut, is_tipo_ordinamento, is_flag_eseguito, is_sql_filtro, is_area
treeviewitem tvi_campo

boolean ib_manutenzioni_schedulate = true

// stefanop 21/12/2009: ottimizzazione eliminazione nodi
boolean ib_tree_deleting = false

private:
	string is_navigatore_tipo
	string is_navigatore_valore
	
	
// stefanop 12/04/2011: pianificazione email
private:
	boolean ib_pianificazione_email = false
end variables

forward prototypes
public function integer wf_prodotto_codificato (string ws_flag_codificato)
public subroutine wf_carica_attrezzatura (string fs_cod_attrezzatura)
public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria)
public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area)
public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto)
public function integer wf_inserisci_singola_registrazione (long fl_handle, long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_tv ()
public function integer wf_flag_tipo_intervento (string ws_flag_tipo_intervento, string ws_flag_ricambio_codificato)
public function string wf_controlla_scadenza_periodo (long fl_anno_reg_programma, long fl_num_reg_programma)
public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione)
public function string wf_leggi_parent (long al_handle)
public function integer wf_open_by_navigatore (string as_tipo, string as_valore)
protected function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura)
public subroutine wf_pianifica_email (integer ai_row)
public function boolean wf_cancella_pianificazioni (long al_anno, long al_numero)
end prototypes

event ue_close();close(this)
end event

event ue_controlla_navigatore();choose case is_navigatore_tipo
	case "area"
		dw_filtro.reset()
		dw_filtro.insertrow(0)
		dw_filtro.setitem(dw_filtro.getrow(), "cod_area_aziendale", is_navigatore_valore)		
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_1", "E")	
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_4", "N")
		cb_cerca.TriggerEvent(Clicked!)	
	
	case "reparto"
		dw_filtro.reset()
		dw_filtro.insertrow(0)
		dw_filtro.setitem(dw_filtro.getrow(), "cod_reparto", is_navigatore_valore)
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_1", "R")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_4", "N")	
		cb_cerca.TriggerEvent(Clicked!)
		
	case "divisione"
		dw_filtro.reset()
		dw_filtro.insertrow(0)
		dw_filtro.setitem(dw_filtro.getrow(), "cod_divisione", is_navigatore_valore)
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_1", "D")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_4", "N")	
		cb_cerca.TriggerEvent(Clicked!)	
	
	case "scheda"
		wf_carica_attrezzatura(is_navigatore_valore)
	
end choose
end event

event ue_carica_singola_registrazione();// --------------------------------  wf_carica_registrazione_manutenzione  ---------------------------
//
// Funzione specifica da richiamare dall'esterno per caricare solo una singola registrazione di manutenzione.
//
// ---------------------------------------------------------------------------------------------------

string ls_null

setnull(ls_null)
//dw_filtro.change_dw_current()
dw_filtro.setitem(dw_filtro.getrow(),"anno_registrazione", il_anno_reg_ricerca)
dw_filtro.setitem(dw_filtro.getrow(),"num_registrazione", il_num_reg_ricerca)
dw_filtro.setitem(dw_filtro.getrow(),"cod_attrezzatura", ls_null)
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_1", "N")
dw_filtro.accepttext()

cb_cerca.postevent("clicked")
dw_filtro.postevent("ue_reset")

LONG ll_anno_registrazione,ll_NUM_registrazione
ll_anno_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")
ll_num_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"num_registrazione")

il_anno_reg_ricerca = 0
il_num_reg_ricerca = 0


end event

event type integer pc_menu_valorizza();string ls_errore
long ll_anno,ll_num, ll_ret
uo_manutenzioni luo_manutenzioni
luo_manutenzioni=create uo_manutenzioni

ll_anno = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(),"anno_registrazione")

ll_num = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(),"num_registrazione")

if isnull(ll_anno) or isnull(ll_num) or ll_anno = 0 or ll_num = 0 then
	g_mb.messagebox("OMNIA","Nessuna manutenzione selezionata!",exclamation!)
	return -1
end if

ll_ret = luo_manutenzioni.uof_calcola_manutenzione(ll_anno,ll_num, ref ls_errore)
if ll_ret < 0 then
	g_mb.messagebox("OMNIA",ls_errore,stopsign!)
	rollback;
	return -1
else
	commit;
end if

dw_manutenzioni_1.change_dw_current()
dw_manutenzioni_1.postevent("pcd_retrieve")

end event

event pc_menu_documenti();dw_manutenzioni_1.accepttext()

if (isnull(dw_manutenzioni_1.getrow()) or dw_manutenzioni_1.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna manutenzione")
	return
end if	

if isnull(dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione")) or &
	dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione") < 2 then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna manutenzione")
	return
end if	

s_cs_xx.parametri.parametro_d_10 = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_d_11 = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione")

window_open(w_det_manutenzioni, 0)
end event

event pc_menu_registra_sch();long   ll_riga

string ls_cod_attrezzatura

dw_manutenzioni_1.accepttext()

ll_riga = dw_manutenzioni_1.getrow()

if ll_riga < 1 then return

ls_cod_attrezzatura = dw_manutenzioni_1.getitemstring( ll_riga, "cod_attrezzatura")

if ls_cod_attrezzatura = "" then setnull(ls_cod_attrezzatura)

if isnull(ls_cod_attrezzatura) then return 

setnull(s_cs_xx.parametri.parametro_s_10)

setnull(s_cs_xx.parametri.parametro_s_11)	

setnull(s_cs_xx.parametri.parametro_d_10)

setnull(s_cs_xx.parametri.parametro_d_11)	

s_cs_xx.parametri.parametro_s_10 = ls_cod_attrezzatura

window_open(w_registra_man, 0)

end event

event pc_menu_report_sch();long   ll_riga

string ls_cod_attrezzatura, ls_test

dw_manutenzioni_1.accepttext()

ll_riga = dw_manutenzioni_1.getrow()

if ll_riga < 1 then
	g_mb.messagebox( "OMNIA", "Prima di eseguire il report è necessario selezionare un intervento!")
	return
end if

ls_cod_attrezzatura = dw_manutenzioni_1.getitemstring( ll_riga, "cod_attrezzatura")

select descrizione
into   :ls_test
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Impossibile identificare l'attrezzatura.~nErrore nella select di anag_attrezzature: " + sqlca.sqlerrtext)
	return
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","Nessuna attrezzatura in anagrafica corrisponde all'elemento selezionato.~nPrima di eseguire il report è necessario selezionare un'attrezzatura!")
	return
end if

s_cs_xx.parametri.parametro_s_1 = ls_cod_attrezzatura

window_open( w_scad_man_attrezzatura, -1)
end event

event pc_menu_report_ol();long ll_row, ll_anno, ll_num


ll_row = dw_manutenzioni_1.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("OMNIA","Selezionare una manutenzione prima di continuare!")
	return
end if

ll_anno = dw_manutenzioni_1.getitemnumber(ll_row,"anno_registrazione")

ll_num = dw_manutenzioni_1.getitemnumber(ll_row,"num_registrazione")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 then
	g_mb.messagebox("OMNIA","Selezionare un programma manutenzione prima di continuare!")
	return
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno

s_cs_xx.parametri.parametro_d_2 = ll_num

window_open(w_report_scheda_lavoro,-1)
end event

event pc_menu_destinatari();decimal ld_ore, ld_minuti
time    lt_tempo

dw_manutenzioni_1.accepttext()

if (isnull(dw_manutenzioni_1.getrow()) or dw_manutenzioni_1.getrow() < 1) then
	
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata alcuna manutenzione")
	
	return
	
end if	

if isnull(dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione")) or dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione") = 0  then
	
	g_mb.messagebox("Omnia", "Attenzione non è stata scelta alcuna manutenzione")
	
	return
	
end if	

if isnull(dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione")) or dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione") = 0 then

	g_mb.messagebox("Omnia", "Attenzione non è stata scelta alcuna manutenzione")
	
	return

end if	

s_cs_xx.parametri.parametro_d_1 = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione")

s_cs_xx.parametri.parametro_d_2 = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione")

window_open(w_manutenzioni_destinatari, 0)

end event

event pc_menu_pianificazioni();/**
 * stefanop
 * 12/04/2011
 * 
 * visualizzo finestra delle pianificazioni
 **/
 
long ll_row
s_cs_xx_parametri lstr_parametri

ll_row = dw_manutenzioni_1.getrow()

lstr_parametri.parametro_ul_1 = dw_manutenzioni_1.getitemnumber(ll_row, "anno_registrazione")
lstr_parametri.parametro_ul_2 = dw_manutenzioni_1.getitemnumber(ll_row, "num_registrazione")

message.powerobjectparm = lstr_parametri
window_open(w_manutenzioni_avvisi, -1)
end event

public function integer wf_prodotto_codificato (string ws_flag_codificato);choose case ws_flag_codificato
case "S"
   dw_manutenzioni_1.object.cod_ricambio_t.Visible=1
   dw_manutenzioni_1.object.cod_ricambio.Visible=1
   dw_manutenzioni_1.object.cf_des_ricambio.Visible=1
   dw_manutenzioni_1.object.ricambio_non_codificato_t.Visible=0
   dw_manutenzioni_1.object.ricambio_non_codificato.Visible=0
   dw_manutenzioni_1.object.flag_ricambio_codificato.Visible=1
   dw_manutenzioni_1.object.flag_ricambio_codificato_t.Visible=1
   dw_manutenzioni_1.object.quan_ricambio_t.Visible=1
   dw_manutenzioni_1.object.quan_ricambio.Visible=1
   dw_manutenzioni_1.object.costo_unitario_ricambio_t.Visible=1
   dw_manutenzioni_1.object.costo_unitario_ricambio.Visible=1
	dw_manutenzioni_1.object.versione_t.Visible=1
	dw_manutenzioni_1.object.cf_versione.Visible=1
	dw_manutenzioni_1.object.cod_versione.Visible=1
case "N"
   dw_manutenzioni_1.object.cod_ricambio_t.Visible=0
   dw_manutenzioni_1.object.cod_ricambio.Visible=0
   dw_manutenzioni_1.object.cf_des_ricambio.Visible=0
   dw_manutenzioni_1.object.ricambio_non_codificato_t.Visible=1
   dw_manutenzioni_1.object.ricambio_non_codificato.Visible=1
   dw_manutenzioni_1.object.flag_ricambio_codificato.Visible=1
   dw_manutenzioni_1.object.flag_ricambio_codificato_t.Visible=1
   dw_manutenzioni_1.object.quan_ricambio_t.Visible=1
   dw_manutenzioni_1.object.quan_ricambio.Visible=1
   dw_manutenzioni_1.object.costo_unitario_ricambio_t.Visible=1
   dw_manutenzioni_1.object.costo_unitario_ricambio.Visible=1
	dw_manutenzioni_1.object.versione_t.Visible=0
	dw_manutenzioni_1.object.cf_versione.Visible=0
	dw_manutenzioni_1.object.cod_versione.Visible=0
case "T"
   dw_manutenzioni_1.object.cod_ricambio_t.Visible=0
   dw_manutenzioni_1.object.cod_ricambio.Visible=0
   dw_manutenzioni_1.object.cf_des_ricambio.Visible=0
   dw_manutenzioni_1.object.ricambio_non_codificato_t.Visible=0
   dw_manutenzioni_1.object.ricambio_non_codificato.Visible=0
   dw_manutenzioni_1.object.flag_ricambio_codificato.Visible=0
   dw_manutenzioni_1.object.flag_ricambio_codificato_t.Visible=0
   dw_manutenzioni_1.object.quan_ricambio_t.Visible=0
   dw_manutenzioni_1.object.quan_ricambio.Visible=0
   dw_manutenzioni_1.object.costo_unitario_ricambio_t.Visible=0
   dw_manutenzioni_1.object.costo_unitario_ricambio.Visible=0
	dw_manutenzioni_1.object.versione_t.Visible=0
	dw_manutenzioni_1.object.cf_versione.Visible=0
	dw_manutenzioni_1.object.cod_versione.Visible=0
end choose
return 0   
end function

public subroutine wf_carica_attrezzatura (string fs_cod_attrezzatura);// --------------------------------  wf_carica_registrazione_manutenzione  ---------------------------
//
// Funzione specifica per caricare solo una attrezzatura passata come parametro; per ora viene usata
// nello standard solo per il navigatore, ma non si esclude che possa essere usata da altre gestioni
//
// ---------------------------------------------------------------------------------------------------

//dw_filtro.change_dw_current()
dw_filtro.setitem(dw_filtro.getrow(),"cod_attrezzatura", fs_cod_attrezzatura)
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_1", "N")
cb_cerca.TriggerEvent("clicked")
//dw_filtro.postevent("ue_reset")

end subroutine

public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione);//// --------------------------------  wf_carica_registrazione_manutenzione  ---------------------------
////
//// Funzione specifica da richiamare dall'esterno per caricare solo una singola registrazione di manutenzione.
////
//// ---------------------------------------------------------------------------------------------------
//
//string ls_null
//
//setnull(ls_null)
//dw_filtro.change_dw_current()
//dw_filtro.setitem(dw_filtro.getrow(),"anno_registrazione", fl_anno_registrazione)
//dw_filtro.setitem(dw_filtro.getrow(),"num_registrazione", fl_num_registrazione)
//dw_filtro.setitem(dw_filtro.getrow(),"cod_attrezzatura", ls_null)
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "N")
//dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_1", "N")
//dw_filtro.accepttext()
//
//cb_cerca.postevent("clicked")
//dw_filtro.postevent("ue_reset")
//
//LONG ll_anno_registrazione,ll_NUM_registrazione
//ll_anno_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")
//ll_num_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"num_registrazione")
//
//
//
il_anno_reg_ricerca = fl_anno_registrazione
il_num_reg_ricerca = fl_num_registrazione

postevent("ue_carica_singola_registrazione")
end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);ws_record lstr_item

long	ll_item

treeviewitem ltv_item


ll_item = al_handle

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello
		case "D"
			as_sql += " and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + lstr_item.codice + "' ) "			
		case "R"
			as_sql += " and cod_reparto = '" + lstr_item.codice + "' "
		case "C"
			as_sql += " and cod_cat_attrezzature = '" + lstr_item.codice + "' "
		case "E"
			as_sql += " and cod_area_aziendale = '" + lstr_item.codice + "' "
		case "A"
			as_sql += " and cod_attrezzatura = '" + lstr_item.codice + "' "
		case "N"
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_cat_attrezzatura, ls_des_cat_attrezzatura, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i,ll_livello
ws_record lstr_record
treeviewitem ltvi_campo

setnull(ls_null)

ll_livello = il_livello_corrente + 1

declare cu_categorie dynamic cursor for sqlsa;
ls_sql = "SELECT cod_cat_attrezzature, des_cat_attrezzature FROM tab_cat_attrezzature WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_categoria) then
	ls_sql += " and cod_cat_attrezzature = '" + fs_cod_categoria + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_cat_attrezzature in (select cod_cat_attrezzature from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + is_sql_filtro + ") "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_cat_attrezzature"
else
	ls_sql = ls_sql + "ORDER BY des_cat_attrezzature"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_categorie;
do while 1=1
   fetch cu_categorie into :ls_cod_cat_attrezzatura, :ls_des_cat_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_categorie;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_categorie (w_manutenzioni:wf_inserisci_categorie)~r~n" + sqlca.sqlerrtext)
		close cu_categorie;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_cat_attrezzatura) then ls_des_cat_attrezzatura = ""
	
	ltvi_campo.itemhandle = fl_handle
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_cat_attrezzatura
	lstr_record.descrizione = ls_des_cat_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.des_manutenzione = ""
	lstr_record.tipo_livello = "C"
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_manutenzione)
	ltvi_campo.data = lstr_record
	if is_tipo_ordinamento = "D" then
		ltvi_campo.label = ls_des_cat_attrezzatura + ", " + ls_cod_cat_attrezzatura
	else
		ltvi_campo.label = ls_cod_cat_attrezzatura + ", " + ls_des_cat_attrezzatura
	end if
	ltvi_campo.pictureindex = 1
	ltvi_campo.selectedpictureindex = 1
	ltvi_campo.overlaypictureindex = 1
	ltvi_campo.children = true
	ltvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	
	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_registrazioni(ll_risposta, ls_null) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----
	
loop
close cu_categorie;
return 0
end function

public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_area_aziendale, ls_des_area, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i, ll_livello
treeviewitem ltvi_campo
ws_record lstr_record
setnull(ls_null)
declare cu_aree dynamic cursor for sqlsa;

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_area_aziendale, des_area FROM tab_aree_aziendali WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_area) then
	ls_sql += " and cod_area_aziendale = '" + fs_cod_area + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_area_aziendale in (select cod_area_aziendale from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + is_sql_filtro + ") "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_area_aziendale"
else
	ls_sql = ls_sql + "ORDER BY des_area"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_aree;
do while 1=1
   fetch cu_aree into :ls_cod_area_aziendale, :ls_des_area;
   if (sqlca.sqlcode = 100) then
		close cu_aree;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_aree (w_manutenzioni:wf_inserisci_aree)~r~n" + sqlca.sqlerrtext)
		close cu_aree;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_area_aziendale
	lstr_record.descrizione = ls_des_area
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_manutenzione)
	lstr_record.tipo_livello = "E"
	ltvi_campo.data = lstr_record
	
	if is_tipo_ordinamento = "D" then
		ltvi_campo.label = ls_des_area + ", " +  ls_cod_area_aziendale
	else
		ltvi_campo.label = ls_cod_area_aziendale + ", " + ls_des_area
	end if

	ltvi_campo.pictureindex = 3
	ltvi_campo.selectedpictureindex = 3
	ltvi_campo.overlaypictureindex = 3
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello AREA")
		close cu_aree;
		return 0
	end if

	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_registrazioni(ll_risposta, ls_null) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----
loop
close cu_aree;
return 0
end function

public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_reparto, ls_des_reparto, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i,ll_livello
treeviewitem ltvi_campo
ws_record lstr_record
setnull(ls_null)

ll_livello = il_livello_corrente + 1

declare cu_reparti dynamic cursor for sqlsa;
ls_sql = "SELECT cod_reparto, des_reparto FROM anag_reparti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_reparto) then
	ls_sql += " and cod_reparto = '" + fs_cod_reparto + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_reparto in (select cod_reparto from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + is_sql_filtro + ") "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_reparto"
else
	ls_sql = ls_sql + "ORDER BY des_reparto"
end if
prepare sqlsa from :ls_sql;

open dynamic cu_reparti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1=1
   fetch cu_reparti into :ls_cod_reparto, :ls_des_reparto;
   if (sqlca.sqlcode = 100) then
		close cu_reparti;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
		close cu_reparti;
		return -1
	end if
	
	if isnull(ls_des_reparto) then ls_des_reparto = ""
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_reparto
	lstr_record.descrizione = ls_des_reparto
	lstr_record.tipo_livello = "R"
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle
	setnull(lstr_record.des_manutenzione)

	ltvi_campo.data = lstr_record
	if is_tipo_ordinamento = "D" then
		ltvi_campo.label = ls_des_reparto + ", " + ls_cod_reparto
	else
		ltvi_campo.label = ls_cod_reparto + ", " + ls_des_reparto
	end if
	ltvi_campo.pictureindex = 2
	ltvi_campo.selectedpictureindex = 2
	ltvi_campo.overlaypictureindex = 2
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW")
		close cu_reparti;
		return 0
	end if

	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_registrazioni(ll_risposta, ls_null) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----
loop
close cu_reparti;
return 0
end function

public function integer wf_inserisci_singola_registrazione (long fl_handle, long fl_anno_registrazione, long fl_num_registrazione);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento singola registrazione
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

string    ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, &
          ls_ordine_registrazione, ls_flag_ordinarie, ls_flag_eseguito, ls_flag_scadenze

long      ll_risposta, ll_anno_reg_programma, ll_num_reg_programma

datetime  ldt_data_registrazione, ldt_data_inizio, ldt_data_fine

ws_record lstr_record

select data_registrazione, 
		 cod_tipo_manutenzione, 
		 cod_attrezzatura,
		 flag_eseguito,
		 anno_reg_programma,
		 num_reg_programma
into   :ldt_data_registrazione,
       :ls_cod_tipo_manutenzione,
		 :ls_cod_attrezzatura,
		 :ls_flag_eseguito,
		 :ll_anno_reg_programma,
		 :ll_num_reg_programma
from   manutenzioni 
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","La registrazione richiesta è inesistente")
	return 0
end if

if sqlca.sqlcode = -1 then
	//donato 01/07/2009: Marco ha detto basta con questo messaggio che scassa la minkia!
	//g_mb.messagebox("OMNIA","Errore in ricerca registrazione (w_manutenzioni:wf_inserisci_singola_registrazione)~r~n" + sqlca.sqlerrtext)
	return -1
end if

if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""

if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""

select des_tipo_manutenzione
into   :ls_des_tipo_manutenzione
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca descrizione tipo manutenzione "+ls_cod_tipo_manutenzione+" dell'attrezzatura + "+ls_cod_attrezzatura+" (w_manutenzioni:wf_inserisci_manutenzioni)~r~nProseguo lo stesso, la descrizione non verrà inserita.")
	return -1
end if
		
if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = ""
	
lstr_record.anno_registrazione = fl_anno_registrazione

lstr_record.num_registrazione = fl_num_registrazione

lstr_record.des_manutenzione = ls_des_tipo_manutenzione

lstr_record.codice = ""		

lstr_record.descrizione = ""

lstr_record.livello = 100

lstr_record.tipo_livello = "M"
	
ls_str = string(fl_anno_registrazione) + "/" + string(fl_num_registrazione) + " " + string(ldt_data_registrazione,"dd/mm/yyyy") + " " + ls_des_tipo_manutenzione

tvi_campo.data = lstr_record

tvi_campo.label = ls_str

ls_flag_scadenze = wf_controlla_scadenza_periodo( ll_anno_reg_programma, ll_num_reg_programma)

if ls_flag_eseguito = "S" then
	
	if ls_flag_scadenze = 'S' then
		tvi_campo.pictureindex = 6
		tvi_campo.selectedpictureindex = 6
		tvi_campo.overlaypictureindex = 6
	else
		tvi_campo.pictureindex = 10
		tvi_campo.selectedpictureindex = 10
		tvi_campo.overlaypictureindex = 10	
	end if
	
else
	
	if ls_flag_scadenze = 'S' then
		tvi_campo.pictureindex = 7
		tvi_campo.selectedpictureindex = 7
		tvi_campo.overlaypictureindex = 7
	else
		tvi_campo.pictureindex = 11
		tvi_campo.selectedpictureindex = 11
		tvi_campo.overlaypictureindex = 11
	end if
	
end if

tvi_campo.children = false

tvi_campo.selected = false

ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)

return 0
end function

public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_attrezzatura, ls_des_attrezzatura, ls_des_identificativa, ls_flag_eseguito, ls_appoggio, ls_flag_visione_strumenti, &
       ls_qualita_attrezzatura
long ll_risposta, ll_i, ll_num_righe, ll_livello
ws_record lstr_record

ll_livello = il_livello_corrente + 1

declare cu_attrezzature dynamic cursor for sqlsa;

ls_sql = "select cod_attrezzatura, descrizione, qualita_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro
if not isnull(fs_cod_attrezzatura) then
	ls_sql += " and cod_attrezzatura = '" + fs_cod_attrezzatura + "' "
end if

ls_flag_visione_strumenti = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_visione_strumenti")
if not isnull(ls_flag_visione_strumenti) then
	choose case ls_flag_visione_strumenti
		case "A"
			ls_sql += " and qualita_attrezzatura = 'A' "
		case "S"
			ls_sql += " and qualita_attrezzatura in ('P','S') "
	end choose
end if

wf_leggi_parent(fl_handle,ls_sql)

if is_tipo_ordinamento = "C" then
	ls_sql += " order by cod_attrezzatura"
else
	ls_sql += " order by descrizione"
end if

prepare sqlsa from :ls_sql;

open dynamic cu_attrezzature;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_attrezzature into :ls_cod_attrezzatura, :ls_des_attrezzatura, :ls_qualita_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_attrezzature;
		if lb_dati then return 0
		return 1
	end if
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
		close cu_attrezzature;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_attrezzatura) then ls_des_attrezzatura = ""

	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_attrezzatura
	lstr_record.descrizione = ls_des_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.des_manutenzione = ""
	lstr_record.tipo_livello = "A"
	lstr_record.livello = ll_livello
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "D" then
		tvi_campo.label = ls_des_attrezzatura + ", " + ls_cod_attrezzatura
	else
		tvi_campo.label = ls_cod_attrezzatura + ", " + ls_des_attrezzatura
	end if
	if ls_qualita_attrezzatura = "P" then
		tvi_campo.pictureindex = 5
		tvi_campo.selectedpictureindex = 5
		tvi_campo.overlaypictureindex = 5
	else
		tvi_campo.pictureindex = 4
		tvi_campo.selectedpictureindex = 4
		tvi_campo.overlaypictureindex = 4
	end if
	tvi_campo.children = true
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
loop
close cu_attrezzature;

return 0
end function

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_filtro.getitemdatetime(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_filtro.getitemstring(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_filtro.getitemnumber(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'MANUT';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'MANUT';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'MANUT');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'MANUT';
if sqlca.sqlcode = 100 then
	//g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public subroutine wf_cancella_treeview ();//long tvi_hdl = 0
//
//do 
//	tvi_hdl = tv_1.finditem(roottreeitem!,0)
//	if tvi_hdl <> -1 then
//		tv_1.deleteitem(tvi_hdl)
//	end if
//loop while tvi_hdl <> -1
//
//return
ib_tree_deleting = true
tv_1.deleteitem(0)
ib_tree_deleting = false
end subroutine

public subroutine wf_imposta_tv ();string ls_null, ls_cod_attrezzatura, ls_des_attrezzatura
long ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione
ws_record lstr_record

setnull(ls_null)

tvi_campo.expanded = false
tvi_campo.selected = false
tvi_campo.children = false	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ll_i = 0
setpointer(HourGlass!)

tv_1.setredraw(false)

ll_anno_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")
ll_num_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"num_registrazione")

// ----------------- se è stata richiesta una specifica registrazione propongo solo quella e niente altro------------------
//
if ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) and ll_num_registrazione > 0 and not isnull(ll_num_registrazione) then
	ib_retrieve=true
	wf_inserisci_singola_registrazione(0, ll_anno_registrazione, ll_num_registrazione )
	tv_1.setredraw(true)
	return
end if											  

// ----------------- se non è stato impostato alcun livello ed è stata specificata una attrezzatura, -----------------------
//							procedo con il caricamento delle registrazioni della sola attrezzatura richiesta 
//
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_1") = "N" and not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura")) then
	wf_inserisci_attrezzature(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura") )
	tv_1.setredraw(true)
	return
end if

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "D"
		wf_inserisci_divisioni(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione"))
		// caricamento divisioni
	case "R"
		wf_inserisci_reparti(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto"))
		// caricamento reparti
	case "C"
		wf_inserisci_categorie(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura"))
		// caricamento categorie
	case "E" 
		wf_inserisci_aree(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale"))
		// caricamento aree
	case "A"
		wf_inserisci_attrezzature(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura"))
//		 caricamento attrezzature
	case "N" 
		wf_inserisci_registrazioni(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura"))
end choose

tv_1.setredraw(true)

setpointer(arrow!)
return
end subroutine

public function integer wf_flag_tipo_intervento (string ws_flag_tipo_intervento, string ws_flag_ricambio_codificato);string ls_modify

dw_manutenzioni_1.setredraw(false)

choose case ws_flag_tipo_intervento
case "M"
   wf_prodotto_codificato(ws_flag_ricambio_codificato)
	dw_manutenzioni_2.object.cod_primario_t.Visible=0
	dw_manutenzioni_2.object.cod_primario.Visible=0
	dw_manutenzioni_2.object.cf_cod_primario.Visible=0
	dw_manutenzioni_2.object.cod_misura.Visible=0
	dw_manutenzioni_2.object.cod_misura_t.Visible=0
	dw_manutenzioni_2.object.cf_cod_misura.Visible=0
	dw_manutenzioni_2.object.valore_min_taratura.Visible=0
	dw_manutenzioni_2.object.valore_min_taratura_t.Visible=0
	dw_manutenzioni_2.object.valore_max_taratura.Visible=0
	dw_manutenzioni_2.object.valore_max_taratura_t.Visible=0
	dw_manutenzioni_2.object.valore_atteso.Visible=0
	dw_manutenzioni_2.object.valore_atteso_t.Visible=0
	dw_manutenzioni_2.object.taratura_t.Visible=0
					
case "T"
	dw_manutenzioni_2.object.cod_primario_t.Visible=1
	dw_manutenzioni_2.object.cod_primario.Visible=1
	dw_manutenzioni_2.object.cf_cod_primario.Visible=1
	dw_manutenzioni_2.object.cod_misura.Visible=1
	dw_manutenzioni_2.object.cod_misura_t.Visible=1
	dw_manutenzioni_2.object.cf_cod_misura.Visible=1
	dw_manutenzioni_2.object.valore_min_taratura.Visible=1
	dw_manutenzioni_2.object.valore_min_taratura_t.Visible=1
	dw_manutenzioni_2.object.valore_max_taratura.Visible=1
	dw_manutenzioni_2.object.valore_max_taratura_t.Visible=1
	dw_manutenzioni_2.object.valore_atteso.Visible=1
	dw_manutenzioni_2.object.valore_atteso_t.Visible=1
	dw_manutenzioni_2.object.taratura_t.Visible=1
   wf_prodotto_codificato("T")
end choose

if len(dw_manutenzioni_1.getitemstring(dw_manutenzioni_1.getrow(),"cf_des_tipo_manutenzione_storico")) > 0 then
	dw_manutenzioni_1.object.cod_tipo_manutenzione.Visible=0
	dw_manutenzioni_1.object.cf_des_tipo_manutenzione_storico.Visible=1
else
	dw_manutenzioni_1.object.cod_tipo_manutenzione.Visible=1
	dw_manutenzioni_1.object.cf_des_tipo_manutenzione_storico.Visible=0
end if	

dw_manutenzioni_1.setredraw(true)
return 0

end function

public function string wf_controlla_scadenza_periodo (long fl_anno_reg_programma, long fl_num_reg_programma);string ls_flag_scadenze

ls_flag_scadenze = 'S'

if not isnull(fl_anno_reg_programma) and not isnull(fl_num_reg_programma) and fl_anno_reg_programma > 0 and fl_num_reg_programma > 0 then

	select flag_scadenze
	into   :ls_flag_scadenze
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_reg_programma and
			 num_registrazione = :fl_num_reg_programma;
			 
	if ls_flag_scadenze <> 'N' then ls_flag_scadenze = 'S'
	
end if	

return ls_flag_scadenze
end function

public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false

string    ls_sql, ls_cod_divisione, ls_des_divisione, ls_null, ls_appoggio, ls_sql_livello, ls_padre

long      ll_risposta, ll_i, ll_livello

ws_record lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_divisione, des_divisione FROM anag_divisioni WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_divisione)  and len(fs_cod_divisione) > 0 then
	ls_sql += " and cod_divisione = '" + fs_cod_divisione + "' "
end if


// *** michela 19/07/2005: nel caso delle divisioni non mi interessa mettere alcuna where in più rispetto al padre ( ma solo nel caso delle aree)
ls_sql_livello = ""
ls_padre = ""

if ll_livello > 1 then
	ls_padre = wf_leggi_parent(fl_handle)
end if

if ls_padre = "E" then
	wf_leggi_parent(fl_handle,ls_sql_livello)
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_divisione IN ( select cod_divisione from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "'  " + ls_sql_livello + " ) "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_divisione"
else
	ls_sql = ls_sql + "ORDER BY des_divisione"
end if

declare cu_divisioni dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_divisioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_divisioni: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_divisioni;
	return 0
end if

do while 1=1
   fetch cu_divisioni into :ls_cod_divisione, 
								   :ls_des_divisione;
									
   if (sqlca.sqlcode = 100) then
		close cu_divisioni;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_divisioni (w_manutenzioni:wf_inserisci_divisioni)~r~n" + sqlca.sqlerrtext)
		close cu_divisioni;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_divisione
	lstr_record.descrizione = ls_des_divisione
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_manutenzione)
	lstr_record.tipo_livello = "D"
	tvi_campo.data = lstr_record
	
	if is_tipo_ordinamento = "D" then
		tvi_campo.label = ls_des_divisione + ", " +  ls_cod_divisione
	else
		tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione
	end if

	tvi_campo.pictureindex = 3
	tvi_campo.selectedpictureindex = 3
	tvi_campo.overlaypictureindex = 3
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello DIVISIONE")
		close cu_divisioni;
		return 0
	end if

loop
close cu_divisioni;
return 0
end function

public function string wf_leggi_parent (long al_handle);ws_record lstr_item

long	ll_item

treeviewitem ltv_item


ll_item = al_handle

if ll_item = 0 then
	return ""
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	return lstr_item.tipo_livello	
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return ""
end function

public function integer wf_open_by_navigatore (string as_tipo, string as_valore);choose case as_tipo
	case "area"
		dw_filtro.setitem( dw_filtro.getrow(), "cod_area_aziendale", as_valore)		
		dw_filtro.setitem( dw_filtro.getrow(), "flag_tipo_livello_1", "E")	
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
		cb_cerca.TriggerEvent(Clicked!)	
	
	case "reparto"
		dw_filtro.setitem( dw_filtro.getrow(), "cod_reparto", as_valore)
		dw_filtro.setitem( dw_filtro.getrow(), "flag_tipo_livello_1", "R")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")	
		cb_cerca.TriggerEvent(Clicked!)
		
	case "divisione"
		dw_filtro.setitem( dw_filtro.getrow(), "cod_divisione", as_valore)
		dw_filtro.setitem( dw_filtro.getrow(), "flag_tipo_livello_1", "D")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")	
		cb_cerca.TriggerEvent(Clicked!)	
	
	case "scheda"
		wf_carica_attrezzatura(as_valore)
	
end choose
return 1
end function

protected function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento registrazioni di manutenzione / taratura (STD)
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false

string  ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, &
        ls_ordine_registrazione, ls_flag_ordinarie, ls_flag_eseguito, ls_data_registrazione, ls_cod_guasto_filtro, &
		  ls_cod_tipo_manut_filtro, ls_flag_scadenze
		  
long    ll_risposta, ll_i, ll_num_righe, ll_anno_registrazione, ll_num_registrazione, ll_livello, ll_anno_reg_filtro, &
        ll_anno_reg_programma, ll_num_reg_programma
		  
datetime ldt_data_registrazione, ldt_data_inizio, ldt_data_fine

ws_record lstr_record

ll_livello = il_livello_corrente

declare cu_registrazioni dynamic cursor for sqlsa;
ls_sql = "SELECT anno_registrazione, " + &
         "       num_registrazione, " + &
			"       data_registrazione, " + &
			"       cod_tipo_manutenzione, " + &
			"       cod_attrezzatura, " + &
			"       flag_eseguito, " + &
			"       anno_reg_programma, " + &
			"       num_reg_programma " + &			
			"FROM   manutenzioni " + &
			"WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

//----------- Michele 26/04/2004 correzione caricamento registrazioni -------------
if len(is_sql_filtro) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro + ")"
end if
//----------- Fine Modifica -------------

choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_eseguito")
	case "S"
		ls_sql += " and flag_eseguito = 'S' "
	case "N"
		ls_sql += " and flag_eseguito = 'N' "
end choose

ll_anno_reg_filtro = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")

ls_flag_ordinarie = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_ordinarie")

ls_ordine_registrazione = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_ordinamento_registrazioni")

ldt_data_inizio = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_inizio")

ldt_data_fine = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_fine")

ls_cod_tipo_manut_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione")

ls_cod_guasto_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_guasto")

if not isnull(ll_anno_reg_filtro) and ll_anno_reg_filtro > 0 then
	ls_sql += " and anno_registrazione = " + string(ll_anno_reg_filtro) + " "
end if	

if not isnull(ldt_data_inizio) and ldt_data_inizio >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine) and ldt_data_fine >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if

choose case ls_flag_ordinarie
	case "S"
		ls_sql += " and flag_ordinario = 'S' "
	case "N"
		ls_sql += " and flag_ordinario = 'N' "
end choose

if len(ls_cod_tipo_manut_filtro) < 1 then setnull(ls_cod_tipo_manut_filtro)

if not isnull(ls_cod_tipo_manut_filtro) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
end if

if len(ls_cod_guasto_filtro) < 1 then setnull(ls_cod_guasto_filtro)

if not isnull(ls_cod_guasto_filtro) then
	ls_sql += " and cod_guasto = '" + ls_cod_guasto_filtro + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
end if

if isnull(ls_ordine_registrazione) then ls_ordine_registrazione = "D"

choose case ls_ordine_registrazione
	case "C"
		ls_sql += " order by data_registrazione ASC "
	case "D"
		ls_sql += " order by data_registrazione DESC "
	case "A"
		ls_sql += " order by cod_attrezzatura ASC " 
	case "T"
		ls_sql += " order by cod_tipo_manutenzione ASC "
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_registrazioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch cu_registrazioni into :ll_anno_registrazione, 
	                            :ll_num_registrazione, 
										 :ldt_data_registrazione, 
										 :ls_cod_tipo_manutenzione, 
										 :ls_cod_attrezzatura, 
										 :ls_flag_eseguito,
										 :ll_anno_reg_programma,
										 :ll_num_reg_programma;

   if (sqlca.sqlcode = 100) then
		close cu_registrazioni;
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_registrazioni (w_manutenzioni:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
		close cu_registrazioni;
		return -1
	end if

	if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""
	
	lb_dati = true
	
	setnull(ls_des_tipo_manutenzione)
	
	select des_tipo_manutenzione
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;

	if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = "<DESCRIZIONE MANCANTE>"
	
	if isnull(ldt_data_registrazione) then
		ls_data_registrazione = "DATA MANCANTE"
	else
		ls_data_registrazione = string(ldt_data_registrazione,"dd/mm/yyyy")
	end if
	
	lstr_record.anno_registrazione = ll_anno_registrazione
	
	lstr_record.num_registrazione = ll_num_registrazione
	
	lstr_record.des_manutenzione = ls_des_tipo_manutenzione
	
	lstr_record.codice = ""		
	
	lstr_record.descrizione = ""
	
	lstr_record.livello = 100
	
	lstr_record.tipo_livello = "M"
	
	choose case ls_ordine_registrazione
		case "C"
			ls_str = ls_data_registrazione + " " + ls_cod_tipo_manutenzione + " " + ls_des_tipo_manutenzione
		case "D"
			ls_str = ls_data_registrazione + " " + ls_cod_tipo_manutenzione + " " + ls_des_tipo_manutenzione
		case "A"
			ls_str = ls_cod_attrezzatura + " " + ls_data_registrazione + " " + ls_cod_tipo_manutenzione + " " + ls_des_tipo_manutenzione
		case "T"
			ls_str = ls_cod_tipo_manutenzione + " " + ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " 
	end choose
	
	tvi_campo.data = lstr_record
	
	tvi_campo.label = ls_str
		
	ls_flag_scadenze = wf_controlla_scadenza_periodo( ll_anno_reg_programma, ll_num_reg_programma)
	
	if ls_flag_eseguito = "S" then
		
		if ls_flag_scadenze = 'S' then
		
			tvi_campo.pictureindex = 6
		
			tvi_campo.selectedpictureindex = 6
		
			tvi_campo.overlaypictureindex = 6
			
		else
			
			tvi_campo.pictureindex = 10
		
			tvi_campo.selectedpictureindex = 10
		
			tvi_campo.overlaypictureindex = 10			
			
		end if
		
	else
		
		if ls_flag_scadenze = 'S' then
			
			tvi_campo.pictureindex = 7
		
			tvi_campo.selectedpictureindex = 7
		
			tvi_campo.overlaypictureindex = 7
			
		else
			
			tvi_campo.pictureindex = 11
		
			tvi_campo.selectedpictureindex = 11
		
			tvi_campo.overlaypictureindex = 11
						
		end if
		
	end if	
	
	tvi_campo.children = false
	tvi_campo.selected = false
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
loop

close cu_registrazioni;
return 0
end function

public subroutine wf_pianifica_email (integer ai_row);/**
 * stefanop
 * 12/04/2011
 *
 * Pianifica l'invio dell'email partendo dalla riga passata per valore
 **/
 
 
long ll_anno, ll_num, ll_pianificate
datetime ldt_today

// prevent error
if ai_row < 1 then return

ll_anno = dw_manutenzioni_1.getitemnumber(ai_row, "anno_registrazione")
ll_num = dw_manutenzioni_1.getitemnumber(ai_row, "num_registrazione")
ldt_today = datetime(today(), 00:00:00)

// controllo se è già presente una pianificazione
select max(progressivo)
into :ll_pianificate
from manutenzioni_avvisi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno and
	num_registrazione = :ll_num and 
	data_registrazione = :ldt_today;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo delle pianificazioni pendenti")
	return
elseif isnull(ll_pianificate) or ll_pianificate < 1 then
	select max(progressivo)
	into  :ll_pianificate
	from manutenzioni_avvisi
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno and
		num_registrazione = :ll_num;
	
	if isnull(ll_pianificate) or ll_pianificate < 1 then ll_pianificate = 0
	ll_pianificate++
	
	// Pianifico email
	insert into manutenzioni_avvisi (
		cod_azienda,
		anno_registrazione,
		num_registrazione,
		progressivo,
		data_registrazione,
		flag_inviata,
		flag_tipo_riga,
		descrizione
	) values (
		:s_cs_xx.cod_azienda,
		:ll_anno,
		:ll_num,
		:ll_pianificate,
		:ldt_today,
		"N",
		"P", // P = Pianificata, D = Descrittiva
		"PIANIFICAZIONE"
	);
	
	if sqlca.sqlcode = 0 then
		commit;
	else
		g_mb.error("Errore durante la pianificazione dell'invio mail.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

g_mb.show("Email di avviso pianicata correttamente")
end subroutine

public function boolean wf_cancella_pianificazioni (long al_anno, long al_numero);/**
 * stefanop
 * 12/04/2011
 *
 * Pulisce le pianificazioni della manutenzione
 **/
 
 
delete from manutenzioni_avvisi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno and
	num_registrazione = :al_numero;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la cancellazione delle pianificazioni.~r~n" + sqlca.sqlerrtext)
	rollback;
	return false
end if

return true
end function

on w_manutenzioni.create
int iCurrent
call super::create
this.cb_note_1=create cb_note_1
this.cb_richiama_predefinito=create cb_richiama_predefinito
this.cb_predefinito=create cb_predefinito
this.cb_ricambi=create cb_ricambi
this.cb_operatori_manut=create cb_operatori_manut
this.cb_scadenzario=create cb_scadenzario
this.cb_salva_nav=create cb_salva_nav
this.cb_ricerca_ricambio=create cb_ricerca_ricambio
this.cb_conferma=create cb_conferma
this.cb_tarature=create cb_tarature
this.cb_cerca=create cb_cerca
this.dw_manutenzioni_2=create dw_manutenzioni_2
this.cb_controllo=create cb_controllo
this.dw_folder=create dw_folder
this.dw_filtro=create dw_filtro
this.dw_folder_search=create dw_folder_search
this.tv_1=create tv_1
this.dw_manutenzioni_1=create dw_manutenzioni_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_note_1
this.Control[iCurrent+2]=this.cb_richiama_predefinito
this.Control[iCurrent+3]=this.cb_predefinito
this.Control[iCurrent+4]=this.cb_ricambi
this.Control[iCurrent+5]=this.cb_operatori_manut
this.Control[iCurrent+6]=this.cb_scadenzario
this.Control[iCurrent+7]=this.cb_salva_nav
this.Control[iCurrent+8]=this.cb_ricerca_ricambio
this.Control[iCurrent+9]=this.cb_conferma
this.Control[iCurrent+10]=this.cb_tarature
this.Control[iCurrent+11]=this.cb_cerca
this.Control[iCurrent+12]=this.dw_manutenzioni_2
this.Control[iCurrent+13]=this.cb_controllo
this.Control[iCurrent+14]=this.dw_folder
this.Control[iCurrent+15]=this.dw_filtro
this.Control[iCurrent+16]=this.dw_folder_search
this.Control[iCurrent+17]=this.tv_1
this.Control[iCurrent+18]=this.dw_manutenzioni_1
end on

on w_manutenzioni.destroy
call super::destroy
destroy(this.cb_note_1)
destroy(this.cb_richiama_predefinito)
destroy(this.cb_predefinito)
destroy(this.cb_ricambi)
destroy(this.cb_operatori_manut)
destroy(this.cb_scadenzario)
destroy(this.cb_salva_nav)
destroy(this.cb_ricerca_ricambio)
destroy(this.cb_conferma)
destroy(this.cb_tarature)
destroy(this.cb_cerca)
destroy(this.dw_manutenzioni_2)
destroy(this.cb_controllo)
destroy(this.dw_folder)
destroy(this.dw_filtro)
destroy(this.dw_folder_search)
destroy(this.tv_1)
destroy(this.dw_manutenzioni_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag_manutenzioni_schedulate
windowobject lw_oggetti[], lw_oggetti1[], lw_oggetti2[]

dw_manutenzioni_1.set_dw_key("cod_azienda")
dw_manutenzioni_1.set_dw_key("anno_registrazione")
dw_manutenzioni_1.set_dw_key("num_registrazione")

ls_flag_manutenzioni_schedulate = "N"
select flag_gest_manutenzione
into  :ls_flag_manutenzioni_schedulate
from  parametri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or ls_flag_manutenzioni_schedulate = "N" then
	ib_manutenzioni_schedulate = false
end if

dw_manutenzioni_1.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_manutenzioni_2.set_dw_options(sqlca, dw_manutenzioni_1, c_sharedata + c_scrollparent, c_default)


iuo_dw_main = dw_manutenzioni_1

lw_oggetti[1] = dw_manutenzioni_1
lw_oggetti[2] = cb_ricerca_ricambio
dw_folder.fu_assigntab(1, "Generale", lw_oggetti[])
lw_oggetti1[1] = dw_manutenzioni_2
lw_oggetti1[2] = cb_controllo
lw_oggetti1[3] = cb_note_1
dw_folder.fu_assigntab(2, "Riferimenti", lw_oggetti1[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

//  ------------------------  imposto folder di ricerca ---------------------------

dw_filtro.insertrow(0)

lw_oggetti2[1] = tv_1
dw_folder_search.fu_assigntab(2, "Registrazioni", lw_oggetti2[])
lw_oggetti2[1] = dw_filtro
lw_oggetti2[2] = cb_cerca
//lw_oggetti2[3] = cb_scegli_attrezzatura
lw_oggetti2[3] = cb_predefinito
lw_oggetti2[4] = cb_richiama_predefinito
dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti2[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

// -----------------------------  impostazioni relative al treeview --------------------------
tv_1.deletepictures()
tv_1.PictureHeight = 16
tv_1.PictureWidth = 16

//---- categoria
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_categorie.png")

//---- reparto
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_reparti.png")

//---- area
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_area.png")

//---- attrezzatura
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_off.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_on.png")

//---- registrazione
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eseguita.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png")

//---- registrazione eliminata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eliminata.png")

//---- registrazione modificata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png")


//---- registrazione periodica
// gestione icone programmazione periodo
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eseguita.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png")

	//---- registrazione eliminata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eliminata.png")

	//---- registrazione modificata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png")


// ------------------------  richiamo le impostazioni predefinite del filtro --------------------------
cb_richiama_predefinito.postevent("clicked")


//------------ Michele 22/04/2004 Gestione dipartimentale Sintexcal ----------------------
string ls_fam, ls_supervisore

select flag
into :ls_fam
from parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'FAM';
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura parametro aziendale FAM: " + sqlca.sqlerrtext,stopsign!)
	postevent("ue_close")
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_fam) or ls_fam <> "S" then
	ib_fam = false
else
	
		//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//	select
//		flag_supervisore,
//		cod_area_aziendale
//	into
//		:ls_supervisore,
//		:is_area
//	from
//		mansionari
//	where
//		cod_azienda = :s_cs_xx.cod_azienda and
//		cod_utente = :s_cs_xx.cod_utente;
	select
		cod_area_aziendale
	into
		:is_area
	from
		mansionari
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente;
		
	uo_mansionario luo_mansionario
	luo_mansionario = create uo_mansionario
	
	ls_supervisore = 'N'	
	
	if luo_mansionario.uof_get_privilege(luo_mansionario.supervisore) then ls_supervisore = 'S'
	//--- Fine modifica Giulio
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: " + sqlca.sqlerrtext,stopsign!)
		postevent("ue_close")
		return -1
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: nessun mansionario associato",stopsign!)
		postevent("ue_close")
		return -1
	elseif isnull(ls_supervisore) or ls_supervisore <> "S" then
		
		ib_fam = true
		
		if isnull(is_area) then
			g_mb.messagebox("OMNIA","Il mansionario dell'utente corrente non è associato ad alcuna area aziendale",stopsign!)
			postevent("ue_close")
			return -1
		else
			dw_filtro.setitem(1,"cod_area_aziendale",is_area)
			dw_filtro.object.cod_area_aziendale.protect = 1
			dw_filtro.object.cod_area_aziendale.background.color = 12632256
		end if
		
	else
		ib_fam = false
	end if
	
end if
//----------------------------- 22/04/2004 Fine -----------------------------------

// stefanop 12/04/2011: controllo parametro PEM per la pianificazione dell'invio email
string ls_pem
select flag
into :ls_pem
from parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'PEM';
	
if sqlca.sqlcode = 0 and ls_pem = 'S' then ib_pianificazione_email = true
// ----
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_manutenzioni_1,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_sort(dw_manutenzioni_1, &
                   "cod_guasto", &
						 sqlca, &
						 "tab_guasti", &
						 "cod_guasto", &
						 "des_guasto + ' ' + cod_attrezzatura", &
						 "cod_azienda = '" + s_cs_xx.cod_azienda + &
						 "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in ( select cod_guasto from tab_guasti where cod_azienda = '" + s_cs_xx.cod_azienda + "' )" , &
						  "1")
						  

f_PO_LoadDDDW_DW(dw_manutenzioni_1,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_manutenzioni_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_manutenzioni_2,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

f_PO_LoadDDDW_DW(dw_manutenzioni_2,"cod_operaio_autorizzante",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_manutenzioni_2,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'" +" and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_manutenzioni_2,"cod_primario",sqlca,&
					  "anag_attrezzature","cod_attrezzatura","descrizione",&
					  "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'"+" and qualita_attrezzatura = 'P' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_PO_LoadDDDW_DW(dw_filtro,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_filtro,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_filtro,"cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_filtro,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  

end event

event open;call super::open;long					ll_anno, ll_numero
s_cs_xx_parametri lstr_parametri

lstr_parametri = Message.PowerObjectParm

if not isnull( lstr_parametri) then
	
	if isvalid( lstr_parametri) then
		
		if not isnull(lstr_parametri.parametro_ul_1) and lstr_parametri.parametro_ul_1 > 0 then
			ll_anno = lstr_parametri.parametro_ul_1
			ll_numero = lstr_parametri.parametro_ul_2
			wf_carica_singola_registrazione( ll_anno, ll_numero)
		elseif not isnull(lstr_parametri.parametro_s_1) and not isnull(lstr_parametri.parametro_s_2) then
			is_navigatore_tipo = lstr_parametri.parametro_s_1
			is_navigatore_valore = lstr_parametri.parametro_s_2
			event post ue_controlla_navigatore()
		end if
	end if
end if
end event

type cb_note_1 from uo_ext_note_mss_1 within w_manutenzioni
integer x = 1362
integer y = 1480
integer taborder = 90
boolean bringtotop = true
end type

event clicked;call super::clicked;long   ll_row, ll_anno_registrazione, ll_num_registrazione

if not isnull(s_cs_xx.parametri.parametro_s_1) then

	ll_anno_registrazione = dw_manutenzioni_1.getitemnumber( dw_manutenzioni_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_manutenzioni_1.getitemnumber( dw_manutenzioni_1.getrow(), "num_registrazione")

	update manutenzioni
	set    note_idl = :s_cs_xx.parametri.parametro_s_1
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       	anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in inserimento della nota.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	else
		commit;
	end if
	
	ll_row = dw_manutenzioni_1.getrow()
	dw_manutenzioni_1.change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	dw_manutenzioni_1.scrolltorow(ll_row)

end if

end event

event ue_carica_valori;call super::ue_carica_valori;long   ll_row, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_manutenzioni_1.getitemnumber( dw_manutenzioni_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_manutenzioni_1.getitemnumber( dw_manutenzioni_1.getrow(), "num_registrazione")

select 	note_idl
into   		:s_cs_xx.parametri.parametro_s_1
from   	manutenzioni
where  	cod_azienda = :s_cs_xx.cod_azienda and
		   	anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;

s_cs_xx.parametri.parametro_i_1 = 5000
end event

type cb_richiama_predefinito from commandbutton within w_manutenzioni
integer x = 425
integer y = 2052
integer width = 361
integer height = 80
integer taborder = 180
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Predefinito"
end type

event clicked;wf_leggi_filtro()

//------------ Michele 22/04/2004 Gestione dipartimentale Sintexcal ----------------------
if ib_fam then
	dw_filtro.setitem(1,"cod_area_aziendale",is_area)
end if
//----------------------------- 22/04/2004 Fine -----------------------------------

dw_filtro.postevent("ue_reset")
end event

type cb_predefinito from commandbutton within w_manutenzioni
integer x = 37
integer y = 2052
integer width = 379
integer height = 80
integer taborder = 170
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Predef."
end type

event clicked;dw_filtro.accepttext()
wf_memorizza_filtro()
end event

type cb_ricambi from commandbutton within w_manutenzioni
integer x = 1966
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricambi"
end type

event clicked;long ll_row, ll_anno, ll_num


ll_row = dw_manutenzioni_1.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("OMNIA","Selezionare una manutenzione prima di continuare!")
	return -1
end if

ll_anno = dw_manutenzioni_1.getitemnumber(ll_row,"anno_registrazione")

ll_num = dw_manutenzioni_1.getitemnumber(ll_row,"num_registrazione")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 then
	g_mb.messagebox("OMNIA","Selezionare un programma manutenzione prima di continuare!")
	return -1
end if

s_cs_xx.parametri.parametro_s_1 = dw_manutenzioni_1.getitemstring(ll_row,"flag_eseguito")

window_open_parm(w_manutenzioni_ricambi,0, dw_manutenzioni_1)

//cb_calcola.triggerevent("clicked")
triggerevent("pc_menu_valorizza")
end event

type cb_operatori_manut from commandbutton within w_manutenzioni
integer x = 2743
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Altri Oper."
end type

event clicked;decimal ld_ore, ld_minuti
time lt_tempo

dw_manutenzioni_1.accepttext()

if (isnull(dw_manutenzioni_1.getrow()) or dw_manutenzioni_1.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata alcuna manutenzione")
	return
end if	
if isnull(dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione")) or dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione") = 0  then
	g_mb.messagebox("Omnia", "Attenzione non è stata scelta alcuna manutenzione")
	return
end if	
if isnull(dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione")) or dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione") = 0 then
	g_mb.messagebox("Omnia", "Attenzione non è stata scelta alcuna manutenzione")
	return
end if	


s_cs_xx.parametri.parametro_s_10 = string(dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione"))
s_cs_xx.parametri.parametro_s_11 = string(dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione"))

window_open(w_elenco_oper_manut, 0)

setnull(s_cs_xx.parametri.parametro_d_8)

//cb_calcola.triggerevent("clicked")
triggerevent("pc_menu_valorizza")
end event

type cb_scadenzario from commandbutton within w_manutenzioni
integer x = 1189
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Doc.Gen."
end type

event clicked;//window_open(w_doc_manutenzioni, -1)
open(w_doc_manutenzioni_ole)
end event

type cb_salva_nav from commandbutton within w_manutenzioni
integer x = 2354
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Navigatore"
end type

event clicked;long   ll_riga, ll_cod_elemento

string ls_cod_attrezzatura, ls_des_elemento, ls_valore

dw_manutenzioni_1.accepttext()

ll_riga = dw_manutenzioni_1.getrow()

if ll_riga < 1 then return

ls_cod_attrezzatura = dw_manutenzioni_1.getitemstring( ll_riga, "cod_attrezzatura")

select max(cod_scheda)
into   :ll_cod_elemento
from   tab_schede_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore in lettura dati da tab_mappe_manutenzioni: " + sqlca.sqlerrtext, stopsign!)
	return
end if
	
if isnull(ll_cod_elemento) or ll_cod_elemento < 1 then
	ll_cod_elemento = 1
else 
	ll_cod_elemento ++
end if
	
setnull(s_cs_xx.parametri.parametro_s_10)
	
window_open( w_sel_manutenzioni_des, 0)

if isnull(s_cs_xx.parametri.parametro_s_10) or s_cs_xx.parametri.parametro_s_10 = "" or s_cs_xx.parametri.parametro_s_10 = "ATTREZZATURANULLA" then
	g_mb.messagebox( "OMNIA", "Attenzione: impossibile inserire la scheda!", stopsign!)
	return	
end if		
	
ls_des_elemento = s_cs_xx.parametri.parametro_s_10

setnull(s_cs_xx.parametri.parametro_s_10)
	
ls_valore = ls_cod_attrezzatura
	
delete from tab_schede_manutenzioni
where       cod_azienda = :s_cs_xx.cod_azienda and 
            valore = :ls_valore;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore in cancellazione dati da tab_schede_manutenzioni: " + sqlca.sqlerrtext, stopsign!)
	return
end if
	
insert into tab_schede_manutenzioni 
			   ( cod_azienda,
			     cod_scheda,
			     des_scheda, 
			     valore)
values      ( :s_cs_xx.cod_azienda,
	  		     :ll_cod_elemento, 
			     :ls_des_elemento, 
			     :ls_valore) ;
		  
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore in inserimento dati in tab_schede_manutenzioni: " + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
else 
	commit;
	g_mb.messagebox( "OMNIA", "Salvataggio Effettuato!", information!)
	return
end if	
	  
end event

type cb_ricerca_ricambio from cb_prod_ricerca within w_manutenzioni
integer x = 3451
integer y = 1064
integer width = 73
integer height = 72
integer taborder = 20
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_manutenzioni_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_ricambio"
end event

type cb_conferma from commandbutton within w_manutenzioni
integer x = 3291
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;string ls_flag_eseguito
long 	 ll_row, ll_anno_registrazione, ll_num_registrazione

ll_row = dw_manutenzioni_1.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Nessuna manutenzione selezionata!",exclamation!)
	return -1
end if

ll_anno_registrazione = dw_manutenzioni_1.getitemnumber(ll_row,"anno_registrazione")
ll_num_registrazione = dw_manutenzioni_1.getitemnumber(ll_row,"num_registrazione")

if isnull(ll_anno_registrazione) or isnull(ll_num_registrazione) or ll_anno_registrazione = 0 or ll_num_registrazione = 0 then
	g_mb.messagebox("OMNIA","Nessuna manutenzione selezionata!",exclamation!)
	return -1
end if

select flag_eseguito
into :ls_flag_eseguito
from manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then return

if ls_flag_eseguito = "S" then
	g_mb.messagebox("OMNIA", "Manutenzione già confermata!")
	return
end if

dw_manutenzioni_1.postevent("ue_genera_scadenza")

// stefanop 12/04/2011: pianificazione email
if ib_pianificazione_email then
	wf_pianifica_email(ll_row)
end if
// ---

end event

type cb_tarature from commandbutton within w_manutenzioni
integer x = 1577
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Reg. Tarat."
end type

event clicked;call super::clicked;dw_manutenzioni_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura"
end event

type cb_cerca from commandbutton within w_manutenzioni
integer x = 795
integer y = 2052
integer width = 361
integer height = 80
integer taborder = 160
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_filtro.accepttext()

// -------------  compongo SQL con filtri ---------------------

is_sql_filtro = ""
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale")) then
	is_sql_filtro += " and cod_area_aziendale = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale") + "' "
end if

if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) and len(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) > 0 then
	is_sql_filtro += " and cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  cod_divisione = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione") + "' ) "
end if

if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")) then
	is_sql_filtro += " and cod_reparto = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto") + "' "
end if

if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) then
	is_sql_filtro += " and cod_cat_attrezzature = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura") + "' "
end if

// ------------------------------------------------------------


ib_retrieve = false
choose case dw_filtro.getitemstring(1,"flag_eseguito")
	case "S"
		is_tipo_manut = "Storico"
	case "N"
		is_tipo_manut = "Non Eseguite"
	case "T"
		is_tipo_manut = "Tutte"
end choose

is_tipo_ordinamento = dw_filtro.getitemstring(1,"flag_ordinamento_dati")

wf_cancella_treeview()

wf_imposta_tv()

dw_folder_search.fu_selecttab(2)

tv_1.setfocus()

ib_retrieve = true
end event

type dw_manutenzioni_2 from uo_cs_xx_dw within w_manutenzioni
event ue_lista ( )
integer x = 1211
integer y = 140
integer width = 2423
integer height = 1796
integer taborder = 50
string dataobject = "d_manutenzioni_2"
boolean border = false
end type

event ue_lista();long   ll_num_versione, ll_prog_liste_con_comp, ll_num_registrazione, ll_num_edizione, ll_num_reg_lista_comp, &
       ll_prog_riga_manutenzione, ll_anno_registrazione, ll_rows[]
   
if dw_manutenzioni_2.rowcount() = 1 then
	ll_num_reg_lista_comp = this.getitemnumber(1,"num_reg_lista") 
	ll_prog_liste_con_comp = this.getitemnumber(1,"prog_liste_con_comp") 
	if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then 
		g_mb.messagebox("Liste Controllo","Indicare una lista di controllo valida",StopSign!)
		return
	end if
	
	if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
		this.change_dw_current()
		this.change_dw_focus(dw_manutenzioni_2)
		f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)
		ll_anno_registrazione = this.getitemnumber(1, "anno_registrazione") 
		ll_num_registrazione  = this.getitemnumber(1, "num_registrazione") 
	
		update manutenzioni
		set    num_versione        = :ll_num_versione,
				 num_edizione        = :ll_num_edizione,
				 num_reg_lista       = :ll_num_reg_lista_comp,
				 prog_liste_con_comp = :ll_prog_liste_con_comp
		where  cod_azienda         = :s_cs_xx.cod_azienda and
				 anno_registrazione  = :ll_anno_registrazione and 
				 num_registrazione   = :ll_num_registrazione ;
		if sqlca.sqlcode = 0 then
			commit;
		else
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
						  exclamation!, ok!)
			return
		end if
	
		this.triggerevent("pcd_retrieve")
		ll_rows[1] = 1
		dw_manutenzioni_2.set_selected_rows(1, &
																  ll_rows[], &
																  c_ignorechanges, &
																  c_refreshchildren, &
																  c_refreshsame)
	end if
	
	s_cs_xx.parametri.parametro_d_6 = 1
	window_open_parm(w_det_liste_con_comp, 0, dw_manutenzioni_2)
	
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
ws_record lstr_record

tv_1.getitem(il_handle, tvi_campo)
lstr_record = tvi_campo.data

if lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0 then
	l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
	if l_Error < 0 then
		PCCA.Error = c_Fatal
	end if
end if	
	

end event

event itemchanged;call super::itemchanged;long ll_edizione, ll_versione, ll_lista, ll_prog_lista


if i_colname <> "num_reg_lista" then
	return
end if

ll_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")

if not isnull(ll_prog_lista) then
	g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
	return 1
end if	

ll_lista = long(i_coltext)

select max(num_edizione)
into   :ll_edizione
from   tes_liste_controllo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 num_reg_lista = :ll_lista and
		 approvato_da is not null;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
	return -1
end if

select max(num_versione)
into   :ll_versione
from   tes_liste_controllo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 num_reg_lista = :ll_lista and
		 num_edizione = :ll_edizione and
		 approvato_da is not null;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
	return -1
end if

setnull(ll_prog_lista)

setitem(getrow(),"num_edizione",ll_edizione)
setitem(getrow(),"num_versione",ll_versione)
setitem(getrow(),"prog_liste_con_comp",ll_prog_lista)
end event

event pcd_new;call super::pcd_new;object.note_idl.protect = 1
end event

event pcd_modify;call super::pcd_modify;object.note_idl.protect = 1
end event

event pcd_delete;call super::pcd_delete;object.note_idl.protect = 0
end event

event pcd_save;call super::pcd_save;object.note_idl.protect = 0
end event

event pcd_view;call super::pcd_view;object.note_idl.protect = 0
object.note_idl.tabsequence = 210
end event

type cb_controllo from commandbutton within w_manutenzioni
integer x = 2103
integer y = 1084
integer width = 69
integer height = 72
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;if isnull(dw_manutenzioni_2.getitemnumber(dw_manutenzioni_2.getrow(),"prog_liste_con_comp")) then
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata alla manutenzione. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
end if

dw_manutenzioni_2.triggerevent("ue_lista")
end event

type dw_folder from u_folder within w_manutenzioni
integer x = 1189
integer y = 20
integer width = 2478
integer height = 1948
integer taborder = 30
end type

type dw_filtro from datawindow within w_manutenzioni
event ue_key pbm_dwnkey
event ue_reset ( )
integer x = 41
integer y = 104
integer width = 1106
integer height = 1944
integer taborder = 160
string title = "none"
string dataobject = "d_manutenzioni_filtro"
boolean border = false
boolean livescroll = true
end type

event ue_key;if key = keyenter! then
	accepttext()
	cb_cerca.triggerevent("clicked")
	postevent("ue_reset")
end if

end event

event ue_reset();dw_filtro.resetupdate()

end event

event itemchanged;string ls_null, ls_text

setnull(ls_null)
ls_text = gettext()
choose case getcolumnname()
	case "cod_attrezzatura"
		if ls_text = "" or isnull(ls_text) then
			dw_filtro.object.cod_guasto_t.visible = false
			dw_filtro.object.compute_5.visible = false
			dw_filtro.object.cod_guasto.visible = false
			dw_filtro.object.cod_tipo_manutenzione_t.visible = false
			dw_filtro.object.compute_6.visible = false
			dw_filtro.object.cod_tipo_manutenzione.visible = false
			setitem(getrow(),"cod_guasto",ls_null)
			setitem(getrow(),"cod_tipo_manutenzione",ls_null)
		else
			dw_filtro.object.cod_guasto_t.visible = true
			dw_filtro.object.compute_5.visible = true
			dw_filtro.object.cod_guasto.visible = true
			dw_filtro.object.cod_tipo_manutenzione_t.visible = true
			dw_filtro.object.compute_6.visible = true
			dw_filtro.object.cod_tipo_manutenzione.visible = true
			setitem(getrow(),"cod_guasto",ls_null)
			setitem(getrow(),"cod_tipo_manutenzione",ls_null)
			f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
								  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_text + "'")

			f_PO_LoadDDDW_DW(dw_filtro,"cod_guasto",sqlca,&
								  "tab_guasti","cod_guasto","des_guasto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_text + "'")
			
		end if
	case "flag_tipo_livello_1"
		setitem(getrow(),"flag_tipo_livello_2", "N")
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_2", 150)
		end if
		
	case "flag_tipo_livello_2"
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_3", 160)
		end if
		
	case "flag_tipo_livello_3"
		setitem(getrow(),"flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_4", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_4", 170)
		end if
		
end choose

postevent("ue_reset")

end event

event buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_filtro,"cod_attrezzatura")
end choose
end event

type dw_folder_search from u_folder within w_manutenzioni
integer x = 18
integer y = 24
integer width = 1157
integer height = 2136
integer taborder = 40
end type

type tv_1 from treeview within w_manutenzioni
integer x = 46
integer y = 140
integer width = 1097
integer height = 2000
integer taborder = 120
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"Custom093!","C:\CS_70\framework\RISORSE\Foglio2_Dim_Orig.bmp","Start!","Picture!","Custom096!","Custom024!"}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event itempopulate;string ls_null
long ll_livello = 0
treeviewitem ltv_item
ws_record lws_record

setnull(ls_null)

if isnull(handle) or handle <= 0 or ib_tree_deleting then
	return 0
end if

getitem(handle,ltv_item)

lws_record = ltv_item.data
ll_livello = lws_record.livello

il_livello_corrente = ll_livello

if ll_livello < 4 then
	// controlla cosa c'è al livello successivo ???
	choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(ll_livello + 1))
		case "D"
			// caricamento divisioni
			if wf_inserisci_divisioni(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "R"
			// caricamento reparti
			if wf_inserisci_reparti(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "C"
			// caricamento categorie
			if wf_inserisci_categorie(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "E" 
			// caricamento aree
			if wf_inserisci_aree(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "A"
			// caricamento attrezzature
			if wf_inserisci_attrezzature(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "N" 
			if wf_inserisci_registrazioni(handle, ls_null) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
else
	// inserisco le registrazioni
	if wf_inserisci_registrazioni(handle, ls_null) = 1 then
		ltv_item.children = false
		tv_1.setitem(handle, ltv_item)
	end if
	
end if

commit;
end event

event selectionchanged;treeviewitem ltv_item
ws_record lws_record

if isnull(newhandle) or newhandle <= 0 or ib_tree_deleting then
	return 0
end if
il_handle = newhandle
getitem(newhandle,ltv_item)
lws_record = ltv_item.data

dw_manutenzioni_1.change_dw_current()

parent.postevent("pc_retrieve")

end event

event rightclicked;treeviewitem ltv_item
ws_record lws_record

getitem(handle,ltv_item)
lws_record = ltv_item.data

if lws_record.tipo_livello = "M" then
	
	// non sono im modifica o nuovo
	if not dw_manutenzioni_1.ib_stato_nuovo and not dw_manutenzioni_1.ib_stato_modifica then 
		
		m_manutenzioni_menu lm_menu
		lm_menu = create m_manutenzioni_menu
		
		// stefanop 12/04/2011: pianificazioni
		if ib_pianificazione_email and s_cs_xx.admin then
			lm_menu.m_avvisi.visible = true
		end if
		
		
		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		destroy lm_menu
	
	end if

end if
end event

type dw_manutenzioni_1 from uo_cs_xx_dw within w_manutenzioni
event ue_genera_scadenza ( )
event type long ue_salva_totale ( )
integer x = 1211
integer y = 140
integer width = 2423
integer height = 1824
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_manutenzioni_1"
boolean border = false
end type

event ue_genera_scadenza();string   ls_flag_eseguito_old, ls_flag_eseguito, ls_messaggio, ls_flag_ordinario, ls_flag_scadenze, ls_attrezzatura, ls_blocco

long     ll_anno_reg_programma, ll_num_reg_programma, ll_riga, ll_anno_registrazione, ll_num_registrazione,ll_handle

datetime ldt_prossimo_old, ldt_prossimo, ldt_data_blocco, ld_data_prossimo_intervento

uo_manutenzioni luo_manutenzioni

treeviewitem ltv_item

s_cs_xx.parametri.parametro_data_1 = GetItemDateTime(1, "data_registrazione") 

dw_manutenzioni_1.accepttext()

ll_anno_registrazione = getitemnumber(1,"anno_registrazione")

ll_num_registrazione = getitemnumber(1,"num_registrazione")

ls_attrezzatura = getitemstring(1,"cod_attrezzatura")

select flag_blocco,
		 data_blocco
into   :ls_blocco,
		 :ldt_data_blocco
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_attrezzatura;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in controllo stato attrezzatura: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_blocco = "S" and ldt_data_blocco <= s_cs_xx.parametri.parametro_data_1 then
	g_mb.messagebox("OMNIA","L'attrezzatura " + ls_attrezzatura + " è in stato di blocco dal " + &
				  string(date(s_cs_xx.parametri.parametro_data_1)) + ".~nImpossibile confermare questa manutenzione",exclamation!)
	return
end if

if not isnull(dw_manutenzioni_1.getitemstring(1, "flag_ordinario")) then
	ls_flag_ordinario = dw_manutenzioni_1.getitemstring(1, "flag_ordinario")
end if

if not isnull(dw_manutenzioni_1.getitemdatetime(1, "data_prossimo_intervento")) then
	ld_data_prossimo_intervento = dw_manutenzioni_1.getitemdatetime(1, "data_prossimo_intervento")
end if

ll_anno_reg_programma = getitemnumber(1,"anno_reg_programma")

ll_num_reg_programma = getitemnumber(1,"num_reg_programma")


if string(ld_data_prossimo_intervento, "dd/mm/yyyy") = "01/01/1900" then setnull(ld_data_prossimo_intervento)

if ll_anno_reg_programma = 0 then setnull(ll_anno_reg_programma)

if ll_num_reg_programma = 0 then setnull(ll_num_reg_programma)

luo_manutenzioni = CREATE uo_manutenzioni

if isnull(ll_anno_reg_programma) and isnull(ll_num_reg_programma) and ls_flag_ordinario = 'N' and isnull(ld_data_prossimo_intervento) then
	// SI TRATTA DI UNA MANUTENZIONE STRAORDINARIA			

	// ESEGUO SCARICO DI MAGAZZINO
	if luo_manutenzioni.uof_scarica_magazzino(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
		g_mb.messagebox("OMNIA", ls_messaggio)
		destroy luo_manutenzioni
		ROLLBACK;
		return
	end if
	
	update manutenzioni
	set    flag_eseguito = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			 
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore in aggiornamento flag_eseguito in tabella manutenzioni nella manutenzione" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		g_mb.messagebox("OMNIA", ls_messaggio)						
		destroy luo_manutenzioni
		ROLLBACK;
		return
	end if
	
	commit;
	
	ll_handle = tv_1.finditem(currenttreeitem!, 0)
	
	// cambio icona e metto icone con eseguito
	tv_1.getitem(ll_handle,ltv_item)
	
	if ls_flag_scadenze <> 'N' then
		
		ltv_item.pictureindex = 6
		
		ltv_item.selectedpictureindex = 6
		
		ltv_item.overlaypictureindex = 6
		
	else
		
		ltv_item.pictureindex = 10
		
		ltv_item.selectedpictureindex = 10
		
		ltv_item.overlaypictureindex = 10
		
	end if
	
	tv_1.setitem(ll_handle, ltv_item)
	
	// importo item corrente e aggiorno la DW
	tv_1.setfocus()
	tv_1.selectitem(ll_handle)
	tv_1.event selectionchanged(0, ll_handle)	
	destroy luo_manutenzioni
	return 
end if	

if (ll_anno_reg_programma > 0 and not isnull(ll_anno_reg_programma) and ll_num_reg_programma > 0 and not isnull(ll_num_reg_programma)) or &
   ((isnull(ll_anno_reg_programma) and isnull(ll_num_reg_programma) and ls_flag_ordinario = 'N' and not isnull(ld_data_prossimo_intervento))) then

	ls_flag_eseguito = getitemstring(1,"flag_eseguito")
	
	if ls_flag_eseguito = "N" then
		
//---------------------------------------Ricerca se manutenzione è generata da raggiunto utilizzo ------------------
		select flag_scadenze
		into   :ls_flag_scadenze
		from   programmi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and 
		       anno_registrazione = :ll_anno_reg_programma	and 
				 num_registrazione = :ll_num_reg_programma;
			
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura flag_scadenze in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			destroy luo_manutenzioni
			return
		end if
		
		if sqlca.sqlcode = 100 then
			// Modifica Michele per problema su manutenzioni non ordinarie
			if ls_flag_ordinario <> 'N' then
				ls_flag_scadenze = 'S'
			else
				ls_flag_scadenze = 'N'
			end if
			// Fine modifica: 29/05/2001
		end if
			
		// --------------------------------- scarico del magazzino ricambi ---------------------------------------------- //
		
		if luo_manutenzioni.uof_scarica_magazzino(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
			g_mb.messagebox("OMNIA", ls_messaggio)
			ROLLBACK;
			destroy luo_manutenzioni
			return
		else
			update manutenzioni
			set    flag_eseguito = 'S'
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore in aggiornamento flag_eseguito in tabella manutenzioni nella manutenzione" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
				g_mb.messagebox("OMNIA", ls_messaggio)						
				ROLLBACK;
				destroy luo_manutenzioni
				return
			end if
			
		end if				
			
		//------------------------------------------------------------------------------------------------------------------				
		if ls_flag_scadenze <> 'N' then
			
			if luo_manutenzioni.uof_crea_nuova_scadenza(getitemnumber(1,"anno_registrazione"), getitemnumber(1,"num_registrazione"), ls_messaggio) <> 0 then
				g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + ls_messaggio)
				destroy uo_manutenzioni
				ROLLBACK;
				return
			else
				// ----------------- Modifica Michele ----------------------------------
					// Tolto il codice in questo punto che ripeteva lo scarico magazzino
				// -----------------	fine modifica -------------------------------------
			end if
			
		end if	
	
		ldt_prossimo = getitemdatetime(1,"data_prossimo_intervento")
		
		if isnull(ldt_prossimo_old) then ldt_prossimo_old = datetime("01/01/1900")
		
		if not isnull(ldt_prossimo)  then
			if luo_manutenzioni.uof_crea_prossimo_intervento(getitemnumber(1,"anno_registrazione"), getitemnumber(1,"num_registrazione"), ls_messaggio) <> 0 then
				g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione del prossimo intervento richiesto.~r~nDettaglio Errore: " + ls_messaggio)
				destroy uo_manutenzioni
				ROLLBACK;
				return
			end if
		end if
		
	end if
	
	//aggiornamento tabella det_cal_progr_manut
	update det_cal_progr_manut
	set    flag_stato_manutenzione = 'E'
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_reg_manu = :ll_anno_registrazione and 
			 num_reg_manu = :ll_num_registrazione;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in aggiornamento dati dalla tabella det_cal_progr_manut " + sqlca.sqlerrtext)
		destroy uo_manutenzioni
		ROLLBACK;
		return
	end if
	
end if

destroy uo_manutenzioni

commit;

ll_handle = tv_1.finditem(currenttreeitem!, 0)

// cambio icona e metto icone con eseguito
tv_1.getitem(ll_handle,ltv_item)

if ls_flag_scadenze <> 'N' then
	
	ltv_item.pictureindex = 6
	
	ltv_item.selectedpictureindex = 6
	
	ltv_item.overlaypictureindex = 6
	
else
	
	ltv_item.pictureindex = 10
	
	ltv_item.selectedpictureindex = 10
	
	ltv_item.overlaypictureindex = 10
	
end if

tv_1.setitem(ll_handle, ltv_item)

// importo item corrente e aggiorno la DW
tv_1.setfocus()
tv_1.selectitem(ll_handle)
tv_1.event selectionchanged(0, ll_handle)


end event

event type long ue_salva_totale();long   ll_anno_registrazione, ll_num_registrazione, ll_operaio_ore, ll_operaio_minuti, ll_risorsa_ore, ll_risorsa_minuti

dec{4} ld_tot_intervento, ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio,&
		 ld_risorsa_costo_orario, ld_risorsa_costi_aggiuntivi


ll_anno_registrazione = getitemnumber(getrow(),"anno_registrazione")

ll_num_registrazione = getitemnumber(getrow(),"num_registrazione")

ld_quan_ricambio = getitemnumber(getrow(),"quan_ricambio")

ld_costo_unitario_ricambio = getitemnumber(getrow(),"costo_unitario_ricambio")

ld_costo_orario_operaio = getitemnumber(getrow(),"costo_orario_operaio")

ll_operaio_ore = getitemnumber(getrow(),"operaio_ore")

ll_operaio_minuti = getitemnumber(getrow(),"operaio_minuti")

ld_risorsa_costo_orario = getitemnumber(getrow(),"risorsa_costo_orario")

ll_risorsa_ore = getitemnumber(getrow(),"risorsa_ore")

ll_risorsa_minuti = getitemnumber(getrow(),"risorsa_minuti")

ld_risorsa_costi_aggiuntivi = getitemnumber(getrow(),"risorsa_costi_aggiuntivi")

ld_tot_intervento = ld_quan_ricambio * ld_costo_unitario_ricambio + &
						  ld_costo_orario_operaio * (ll_operaio_ore + ll_operaio_minuti / 60) + &
						  ld_risorsa_costo_orario * (ll_risorsa_ore + ll_risorsa_minuti / 60) + &
						  ld_risorsa_costi_aggiuntivi

update manutenzioni
set    tot_costo_intervento = :ld_tot_intervento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia","Errore nella update di manutenzioni: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

return 0

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_anno_registrazione, ll_max_registrazione

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
   IF IsNull(GetItemnumber(l_Idx, "anno_registrazione")) or GetItemnumber(l_Idx, "anno_registrazione") < 1 THEN
		ll_anno_registrazione = f_anno_esercizio()
		setitem(getrow(), "anno_registrazione", ll_anno_registrazione)
	end if
	
   IF IsNull(GetItemnumber(l_Idx, "num_registrazione")) or GetItemnumber(l_Idx, "num_registrazione") < 1 THEN
		ll_anno_registrazione = f_anno_esercizio()
		ll_max_registrazione = 0
		
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione;
		if isnull(ll_max_registrazione) or ll_max_registrazione < 1 then
			ll_max_registrazione = 1
		else
			ll_max_registrazione = ll_max_registrazione + 1
		end if
		
		setitem(getrow(), "num_registrazione", ll_max_registrazione)
	end if
NEXT
end event

event pcd_retrieve;call super::pcd_retrieve;long  l_Error
ws_record lstr_record

tv_1.getitem(il_handle, tvi_campo)
lstr_record = tvi_campo.data

if not(lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0) then
	setnull(lstr_record.anno_registrazione)
	setnull(lstr_record.num_registrazione)
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
if l_Error < 0 then
	PCCA.Error = c_Fatal
end if

//modifica Nicola

string ls_cod_risorsa_esterna, ls_appo

select b.cod_risorsa_esterna, b.cod_cat_risorse_esterne
  into :ls_cod_risorsa_esterna, :ls_appo
  from manutenzioni a, anag_risorse_esterne b
 where b.cod_azienda = :s_cs_xx.cod_azienda
   and b.cod_cat_risorse_esterne = a.cod_cat_risorse_esterne
	and b.cod_risorsa_esterna = a.cod_risorsa_esterna
	and a.anno_registrazione = :lstr_record.anno_registrazione
	and a.num_registrazione = :lstr_record.num_registrazione;


if sqlca.sqlcode = 0 then
	f_PO_LoadDDDW_DW(dw_manutenzioni_1, &
							"cod_risorsa_esterna", &
							sqlca, &
							"anag_risorse_esterne", &
							"cod_risorsa_esterna", &
							"descrizione", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + ls_appo + "'"+" and not (flag_blocco='S' and data_blocco <= "+s_cs_xx.db_funzioni.oggi +")")
	this.setitem(getrow(),"cod_risorsa_esterna",ls_cod_risorsa_esterna)
	this.resetupdate()

end if							 
//fine modifica Nicola

il_anno_registrazione = lstr_record.anno_registrazione
il_num_registrazione = lstr_record.num_registrazione

end event

event itemchanged;call super::itemchanged;string 	ls_cod_tipo_manutenzione, ls_flag_manutenzione, ls_flag_ricambio_codificato, ls_note, &
       	ls_cod_cat_risorse_esterne, ls_cod_cat_attrezzature, ls_cod_ricambio, ls_des_ricambio, &
		 	ls_cod_attrezzatura, ls_testo, ls_cod_ricambio_alternativo, ls_null, ls_versione
			 
datetime ldt_tempo_previsto, ldt_fine_intervento, ldt_inizio_intervento
time lt_inizio_intervento, lt_fine_intervento
string	ls_area

long ll_n_ore, ll_n_ore_2, ll_minuti_fin, ll_ore_fin

long 		ll_num_reg_lista, ll_riga, ll_i, ll_y, ll_ore, ll_minuti

dec{4} 	ld_tariffa_std, ld_diritto_chiamata, ld_totale


setnull(ls_null)

choose case i_colname

	case "cod_attrezzatura"
	
		f_PO_LoadDDDW_DW(dw_manutenzioni_1,"cod_tipo_manutenzione",sqlca,&
							  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + i_coltext + "'")
		
		f_PO_LoadDDDW_sort(dw_manutenzioni_1,"cod_guasto",sqlca,&
						  "tab_guasti","cod_guasto","des_guasto + ' ' + cod_attrezzatura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + &
						  "' and cod_attrezzatura = '" + i_coltext + &
						  "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in (select cod_guasto from tab_guasti where " + &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + &
						  i_coltext + "')","1")
		
		setitem(getrow(),"cod_tipo_manutenzione",ls_null)
	
		setitem(getrow(),"cod_guasto",ls_null)
		
		//------------ Michele 22/04/2004 Gestione dipartimentale Sintexcal ----------------------
		if ib_fam and not isnull(i_coltext) then
			
			select
				cod_area_aziendale
			into
				:ls_area
			from
				anag_attrezzature
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_attrezzatura = :i_coltext;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in verifica area aziendale attrezzatura: " + sqlca.sqlerrtext,stopsign!)
				return 1
			elseif sqlca.sqlcode = 100 then
				g_mb.messagebox("OMNIA","Errore in verifica area aziendale attrezzatura: attrezzatura non trovata",stopsign!)
				return 1
			elseif isnull(ls_area) or ls_area <> is_area then
				g_mb.messagebox("OMNIA","L'attrezzatura non appartiene all'area aziendale dell'utente corrente",stopsign!)
				return 1
			end if
			
		end if
		//------------------------------------ 22/04/2004 Fine ------------------------------------
		
	case "cod_tipo_manutenzione"
  	
	  	ls_cod_attrezzatura = getitemstring( getrow(), "cod_attrezzatura" )
   
		select tab_tipi_manutenzione.tempo_previsto,   
     	       tab_tipi_manutenzione.flag_manutenzione,   
       		 tab_tipi_manutenzione.flag_ricambio_codificato,   
          	 tab_tipi_manutenzione.cod_ricambio,   
          	 tab_tipi_manutenzione.cod_ricambio_alternativo,   
          	 tab_tipi_manutenzione.des_ricambio_non_codificato,   
          	 tab_tipi_manutenzione.num_reg_lista,
          	 tab_tipi_manutenzione.modalita_esecuzione,
				 cod_versione
    	into   :ldt_tempo_previsto,   
          	 :ls_flag_manutenzione,   
          	 :ls_flag_ricambio_codificato,   
          	 :ls_cod_ricambio,
          	 :ls_cod_ricambio_alternativo,   
          	 :ls_des_ricambio,   
          	 :ll_num_reg_lista,
			 	 :ls_note,
				 :ls_versione
    	from   tab_tipi_manutenzione  
    	where  tab_tipi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and  
          	 tab_tipi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
          	 tab_tipi_manutenzione.cod_tipo_manutenzione = :i_coltext ;
   
		if sqlca.sqlcode = 100 then
      	g_mb.messagebox("Dettaglio Manutenzione","Impossibile trovare tipo manutenzione: " + i_coltext + "per l'attrezzatura " + ls_cod_attrezzatura, StopSign!)
      	return
   	end if
   
   	setitem(getrow(), "note_idl", ls_note)
		
		if not isnull(ldt_tempo_previsto) then
		
			ll_ore = hour(time(ldt_tempo_previsto))
		
			ll_minuti = minute(time(ldt_tempo_previsto))
		
		else
			
			ll_ore = 0
		
			ll_minuti = 0
			
		end if	
   
		setitem(getrow(), "operaio_ore", ll_ore)
   
		setitem(getrow(), "operaio_minuti", ll_minuti)
   
		setitem(getrow(), "flag_tipo_intervento", ls_flag_manutenzione)
   
		setitem(getrow(), "flag_ricambio_codificato", ls_flag_ricambio_codificato)
   
		if ls_flag_ricambio_codificato = "S" then
      	
			setitem(getrow(), "cod_ricambio", ls_cod_ricambio)
			
			setitem(getrow(), "cod_versione", ls_versione)
			
			//setitem(getrow(), "cod_ricambio_alternativo", ls_cod_ricambio_alternativo)
			
			if not isnull(ls_cod_ricambio) then setitem(getrow(), "quan_ricambio", 1)
   	
		else
      	
			setitem(getrow(), "ricambio_non_codificato", ls_des_ricambio)
   	
		end if
   	
		setitem(getrow(), "num_reg_lista", ll_num_reg_lista)
		
		setitem(getrow(),"costo_unitario_ricambio", 0 )
	
		select prezzo_acquisto
		into   :ld_tariffa_std
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
	 	       cod_prodotto = :ls_cod_ricambio;
	
		if sqlca.sqlcode = 0 then
			setitem(getrow(),"costo_unitario_ricambio", ld_tariffa_std)
		end if
		
   	wf_flag_tipo_intervento(ls_flag_manutenzione,ls_flag_ricambio_codificato)
		
	case "flag_tipo_intervento"
  		
		wf_flag_tipo_intervento(i_coltext, getitemstring(getrow(),"flag_ricambio_codificato"))
	
		f_PO_LoadDDDW_DW(dw_manutenzioni_2,"cod_primario",sqlca,&
							  "anag_attrezzature","cod_attrezzatura","descrizione",&
						  	  "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'"+" and qualita_attrezzatura = 'P' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")
								 
	case "flag_ricambio_codificato"
  
  		wf_prodotto_codificato(i_coltext)

	case "cod_risorsa_esterna"
	
		setitem(getrow(),"risorsa_costo_orario", 0)
	
		setitem(getrow(),"risorsa_costi_aggiuntivi", 0)
	
		ls_cod_cat_risorse_esterne = getitemstring(getrow(),"cod_cat_risorse_esterne")
	
		select tariffa_std, diritto_chiamata
		into   :ld_tariffa_std, :ld_diritto_chiamata
		from   anag_risorse_esterne
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
		       cod_risorsa_esterna = :i_coltext ;
	
		if sqlca.sqlcode = 0 then	
			setitem(getrow(),"risorsa_costo_orario", ld_tariffa_std)
			setitem(getrow(),"risorsa_costi_aggiuntivi", ld_diritto_chiamata)
		end if
	
	case "cod_operaio"
	
		setitem(getrow(),"costo_orario_operaio", 0)
   
		select cod_cat_attrezzature
   	into   :ls_cod_cat_attrezzature  
   	from   anag_operai
   	where  cod_azienda = :s_cs_xx.cod_azienda and
    	       cod_operaio = :i_coltext ;
	
		if sqlca.sqlcode = 0 then
			
			select costo_medio_orario
			into   :ld_tariffa_std
			from   tab_cat_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_attrezzature = :ls_cod_cat_attrezzature ;
			
			if sqlca.sqlcode = 0 then
				setitem(getrow(),"costo_orario_operaio", ld_tariffa_std)
			end if
			
		end if
	
	case "cod_ricambio"
		
		setitem(getrow(),"costo_unitario_ricambio", 0 )
	  
	  	SELECT costo_ultimo      
	 	INTO :ld_tariffa_std    
	 	FROM anag_prodotti   
		WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
         	( cod_prodotto = :i_coltext );
				
		//select prezzo_acquisto
		//into   :ld_tariffa_std
		//from   anag_prodotti
		//where  cod_azienda = :s_cs_xx.cod_azienda and
		//	      cod_prodotto = :i_coltext;
		
		if sqlca.sqlcode = 0 then
			setitem(getrow(),"costo_unitario_ricambio", ld_tariffa_std)
		end if
		
		setnull(ls_null)
		
		setitem(row,"cod_versione",ls_null)
		
	case "cod_cat_risorse_esterne"
	
		f_PO_LoadDDDW_DW(dw_manutenzioni_1,"cod_risorsa_esterna",sqlca,&
							  "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "'"+" and not (flag_blocco='S' and data_blocco <= "+s_cs_xx.db_funzioni.oggi +")")
							  
		setnull(ls_null)
		
		setitem(getrow(),"cod_risorsa_esterna",ls_null)

	//modifiche Michela 23/04/2002
	case "data_inizio_intervento"
		dw_manutenzioni_1.accepttext()
		
		ldt_inizio_intervento = datetime(this.GetItemDateTime(getrow(),"data_inizio_intervento"))		
		ldt_fine_intervento = datetime(this.GetItemDateTime(getrow(),"data_fine_intervento"))
		
		lt_inizio_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_inizio_intervento"))
		lt_fine_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_fine_intervento"))
		if not(isnull(ldt_fine_intervento)) then
			ll_n_ore = DaysAfter(date(ldt_inizio_intervento),date(ldt_fine_intervento)) * 24			
			if not(isnull(lt_inizio_intervento)) and not(isnull(lt_fine_intervento)) then
				ll_n_ore_2 = SecondsAfter(lt_inizio_intervento,lt_fine_intervento)/3600
			else
				if not(isnull(lt_inizio_intervento)) then
					ll_n_ore_2 = SecondsAfter(time(string(ldt_inizio_intervento)),lt_inizio_intervento)*(-1)/3600
				else
					ll_n_ore_2 = SecondsAfter(time(string(ldt_fine_intervento)),lt_fine_intervento)/3600
				end if
			end if
			ll_minuti_fin = Mod(((ll_n_ore + ll_n_ore_2)*60),60)
			ll_ore_fin = (((ll_n_ore + ll_n_ore_2)*60)-ll_minuti_fin)/60
			if (ll_minuti_fin >= 0 ) and (ll_ore_fin >= 0 ) then
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",ll_ore_fin)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",ll_minuti_fin)			
			else
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",0)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",0)							
			end if
		end if
		
	case "data_fine_intervento"
		dw_manutenzioni_1.accepttext()
		
		ldt_fine_intervento = datetime(dw_manutenzioni_1.getitemdatetime(getrow(),"data_fine_intervento"))
		ldt_inizio_intervento = datetime(dw_manutenzioni_1.getitemdatetime(getrow(),"data_inizio_intervento"))
		
		lt_inizio_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_inizio_intervento"))
		lt_fine_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_fine_intervento"))
		
		if not(isnull(ldt_inizio_intervento))  then
			ll_n_ore = DaysAfter(date(ldt_inizio_intervento),date(ldt_fine_intervento)) * 24			
			if not(isnull(lt_inizio_intervento)) and not(isnull(lt_fine_intervento)) then
				ll_n_ore_2 = SecondsAfter(lt_inizio_intervento,lt_fine_intervento)/3600
			else
				if not(isnull(lt_inizio_intervento)) then
					ll_n_ore_2 = SecondsAfter(time(string(ldt_inizio_intervento)),lt_inizio_intervento)*(-1)/3600
				else
					ll_n_ore_2 = SecondsAfter(time(string(ldt_fine_intervento)),lt_fine_intervento)/3600
				end if
			end if
			ll_minuti_fin = Mod(((ll_n_ore + ll_n_ore_2)*60),60)
			ll_ore_fin = (((ll_n_ore + ll_n_ore_2)*60)-ll_minuti_fin)/60
			if (ll_minuti_fin >= 0 ) and (ll_ore_fin >= 0 ) then
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",ll_ore_fin)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",ll_minuti_fin)			
			else
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",0)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",0)							
			end if
		end if
		

	case "ora_inizio_intervento"
		dw_manutenzioni_1.accepttext()
		
		ldt_fine_intervento = datetime(dw_manutenzioni_1.getitemdatetime(getrow(),"data_fine_intervento"))
		ldt_inizio_intervento = datetime(dw_manutenzioni_1.getitemdatetime(getrow(),"data_inizio_intervento"))
		
		lt_inizio_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_inizio_intervento"))
		lt_fine_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_fine_intervento"))
		
		if not(isnull(ldt_inizio_intervento)) and not(isnull(ldt_fine_intervento)) then
			ll_n_ore = DaysAfter(date(ldt_inizio_intervento),date(ldt_fine_intervento)) * 24			
			if not(isnull(lt_inizio_intervento)) and not(isnull(lt_fine_intervento)) then
				ll_n_ore_2 = SecondsAfter(lt_inizio_intervento,lt_fine_intervento)/3600
			else
				if not(isnull(lt_inizio_intervento)) then
					ll_n_ore_2 = SecondsAfter(time(string(ldt_inizio_intervento)),lt_inizio_intervento)*(-1)/3600
				else
					ll_n_ore_2 = SecondsAfter(time(string(ldt_fine_intervento)),lt_fine_intervento)/3600
				end if
			end if
			ll_minuti_fin = Mod(SecondsAfter(lt_inizio_intervento,lt_fine_intervento)/60,60)
			ll_ore_fin = (ll_n_ore + ll_n_ore_2)
			if (ll_minuti_fin >= 0 ) and (ll_ore_fin >= 0 ) then
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",ll_ore_fin)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",ll_minuti_fin)			
			else
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",0)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",0)							
			end if
		end if		
		
	case "ora_fine_intervento"
		dw_manutenzioni_1.accepttext()
		
		ldt_fine_intervento = datetime(dw_manutenzioni_1.getitemdatetime(getrow(),"data_fine_intervento"))
		ldt_inizio_intervento = datetime(dw_manutenzioni_1.getitemdatetime(getrow(),"data_inizio_intervento"))
		
		lt_inizio_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_inizio_intervento"))
		lt_fine_intervento = time(dw_manutenzioni_1.getitemdatetime(getrow(),"ora_fine_intervento"))
	
		if not(isnull(ldt_inizio_intervento)) and not(isnull(ldt_fine_intervento)) then
			ll_n_ore = DaysAfter(date(ldt_inizio_intervento),date(ldt_fine_intervento)) * 24			
			if not(isnull(lt_inizio_intervento)) and not(isnull(lt_fine_intervento)) then
				ll_n_ore_2 = SecondsAfter(lt_inizio_intervento,lt_fine_intervento)/3600				
			else
				if not(isnull(lt_inizio_intervento)) then
					ll_n_ore_2 = SecondsAfter(time(string(ldt_inizio_intervento)),lt_inizio_intervento)*(-1)/3600
				else
					ll_n_ore_2 = SecondsAfter(time(string(ldt_fine_intervento)),lt_fine_intervento)/3600
				end if
			end if

			ll_minuti_fin = Mod(SecondsAfter(lt_inizio_intervento,lt_fine_intervento)/60,60)
			ll_ore_fin = (ll_n_ore + ll_n_ore_2)
			if (ll_minuti_fin >= 0 ) and (ll_ore_fin >= 0 ) then
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",ll_ore_fin)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",ll_minuti_fin)			
			else
				dw_manutenzioni_1.setitem(getrow(),"operaio_ore",0)		
				dw_manutenzioni_1.setitem(getrow(),"operaio_minuti",0)							
			end if
		end if		
	//fine modifiche
	
end choose
end event

event updateend;call super::updateend;string ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_ricambio_non_codificato, ls_messaggio, ls_flag_ricambio_codificato, &
       ls_flag_scadenze, ls_flag_eseguito

long   ll_anno_registrazione, ll_num_registrazione, ll_row,ll_i, ll_anno_reg_programma, ll_num_reg_programma, ll_risposta

ws_record    lstr_record

uo_ricambi   luo_ricambi

treeviewitem ltv_item

dw_manutenzioni_1.accepttext()                                                        
																		  
ll_row = dw_manutenzioni_1.getrow()																		  
ls_flag_ricambio_codificato = dw_manutenzioni_1.getitemstring(dw_manutenzioni_1.getrow(), "flag_ricambio_codificato")
					
if ls_flag_ricambio_codificato = "N" then
	dw_manutenzioni_1.Object.ricambio_non_codificato.TabSequence = 0
	dw_manutenzioni_1.Object.ricambio_non_codificato.Background.Color = RGB(192, 192, 192)
	
	ls_ricambio_non_codificato = dw_manutenzioni_1.getitemstring(dw_manutenzioni_1.getrow(), "ricambio_non_codificato")
	
	if not isnull(ls_ricambio_non_codificato) then
	
		ll_anno_registrazione = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione")
		
		select cod_attrezzatura, 
				 cod_tipo_manutenzione
		  into :ls_cod_attrezzatura,
				 :ls_cod_tipo_manutenzione
		  from manutenzioni
		 where cod_azienda = :s_cs_xx.cod_azienda and 
		       anno_registrazione = :ll_anno_registrazione and 
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode = 0 then
			update tab_tipi_manutenzione
				set des_ricambio_non_codificato = :ls_ricambio_non_codificato
			 where cod_azienda = :s_cs_xx.cod_azienda and 
			       cod_attrezzatura = :ls_cod_attrezzatura and 
					 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		end if		
	end if	
end if	


// gestione dei ricambi 
for ll_i = 1 to rowcount()
	
	if getitemstatus(ll_i,"cod_ricambio",primary!) = datamodified! or &
		getitemstatus(ll_i,"cod_versione",primary!) = datamodified! or &
		getitemstatus(ll_i,"cod_attrezzatura",primary!) = datamodified! or &
		getitemstatus(ll_i,"cod_tipo_manutenzione",primary!) = datamodified! then
		
		luo_ricambi = create uo_ricambi
		
		if luo_ricambi.uof_ricambi_manutenzione(getitemnumber(ll_i,"anno_registrazione"), getitemnumber(ll_i,"num_registrazione"), ls_messaggio) <> 0 then
			g_mb.messagebox("OMNIA",ls_messaggio)
			destroy luo_ricambi
			return -1
		end if
		
		destroy luo_ricambi
		
	end if
next

if il_handle_modificato > 0 then
	
	ll_anno_reg_programma = dw_manutenzioni_1.getitemnumber( ll_row, "anno_reg_programma")
		
	ll_num_reg_programma = dw_manutenzioni_1.getitemnumber( ll_row, "num_reg_programma")
			
	ls_flag_scadenze = wf_controlla_scadenza_periodo( ll_anno_reg_programma, ll_num_reg_programma)	
	
	choose case dw_manutenzioni_1.getitemstatus( ll_row, 0, Primary!)
			
		case DataModified!			
			
			tv_1.getitem(il_handle_modificato,ltv_item)

			if ls_flag_scadenze = 'S' then
				
				ltv_item.pictureindex = 9
			
				ltv_item.selectedpictureindex = 9
			
				ltv_item.overlaypictureindex = 9
				
			else
				
				ltv_item.pictureindex = 13
			
				ltv_item.selectedpictureindex = 13
			
				ltv_item.overlaypictureindex = 13
								
			end if
			
			tv_1.setitem(il_handle_modificato, ltv_item)
			
		case NewModified!	
						
			ls_flag_eseguito = dw_manutenzioni_1.getitemstring( ll_row, "flag_eseguito")
			
			lstr_record.anno_registrazione = dw_manutenzioni_1.getitemnumber( ll_row, "anno_registrazione")
			
			lstr_record.num_registrazione = dw_manutenzioni_1.getitemnumber( ll_row, "num_registrazione")
			
			lstr_record.codice = dw_manutenzioni_1.getitemstring( ll_row, "cod_attrezzatura")
			
			lstr_record.descrizione = ""
			
			lstr_record.livello = 100
			
			lstr_record.tipo_livello = "M"
			
			tvi_campo.data = lstr_record
			
			tvi_campo.label = "Nuova registrazione: " + string(lstr_record.anno_registrazione) + "/" + string(lstr_record.num_registrazione) + " " + lstr_record.codice
			
			if ls_flag_eseguito = "S" then
				
				if ls_flag_scadenze = 'S' then
					
					tvi_campo.pictureindex = 6
				
					tvi_campo.selectedpictureindex = 6
				
					tvi_campo.overlaypictureindex = 6
				
				else
					
					tvi_campo.pictureindex = 10
				
					tvi_campo.selectedpictureindex = 10
				
					tvi_campo.overlaypictureindex = 10
					
				end if
					
			else
				
				if ls_flag_scadenze = 'S' then
					
					tvi_campo.pictureindex = 7
					
					tvi_campo.selectedpictureindex = 7
					
					tvi_campo.overlaypictureindex = 7
				
				else
					
					tvi_campo.pictureindex = 11
				
					tvi_campo.selectedpictureindex = 11
				
					tvi_campo.overlaypictureindex = 11
					
				end if				
				
			end if
			
			tvi_campo.children = false
			
			tvi_campo.selected = false
			
			ll_risposta = tv_1.insertitemFirst(tv_1.Finditem(RootTreeItem! ,0), tvi_campo)
			
			tv_1.SelectItem(ll_risposta)
			
		case New!
			
	end choose
	
end if	


if il_handle_cancellato > 0 then
	
	tv_1.getitem(il_handle_cancellato,ltv_item)
	
	if ls_flag_scadenze = 'S' then
	
		ltv_item.pictureindex = 8
		
		ltv_item.selectedpictureindex = 8
		
		ltv_item.overlaypictureindex = 8
		
	else
		
		ltv_item.pictureindex = 12
		
		ltv_item.selectedpictureindex = 12
		
		ltv_item.overlaypictureindex = 12
		
	end if
	
	tv_1.setitem(il_handle_cancellato, ltv_item)
end if


// stefanop 12/04/2011: pianificazione email
if ib_pianificazione_email and rowsupdated > 0 then
	wf_pianifica_email(getrow())
end if
// ---
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()

	case "cod_ricambio"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_manutenzioni_1,"cod_ricambio")
		end if
end choose
end event

event pcd_new;call super::pcd_new;string ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_ricambio_non_codificato
long ll_anno_registrazione, ll_num_registrazione, ll_null, ll_max_registrazione

cb_ricambi.enabled = false
cb_controllo.enabled = false
cb_conferma.enabled = false
cb_operatori_manut.enabled = false
cb_salva_nav.enabled = false
cb_tarature.enabled = false
cb_scadenzario.enabled = false
cb_note_1.enabled = false

cb_ricerca_ricambio.enabled = true
dw_manutenzioni_1.object.b_ricerca_att.enabled = true

setitem(getrow(),"flag_stampato","N")


setnull(ll_null)
this.setitem(this.getrow(),"prog_liste_con_comp", ll_null)


dw_manutenzioni_1.Object.ricambio_non_codificato.TabSequence = 100
dw_manutenzioni_1.Object.ricambio_non_codificato.Background.Color = RGB(255, 255, 255)

ls_ricambio_non_codificato = dw_manutenzioni_1.getitemstring(dw_manutenzioni_1.getrow(), "ricambio_non_codificato")

if not isnull(ls_ricambio_non_codificato) then

	ll_anno_registrazione = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_manutenzioni_1.getitemnumber(dw_manutenzioni_1.getrow(), "num_registrazione")
	
	select cod_attrezzatura, 
			 cod_tipo_manutenzione
	  into :ls_cod_attrezzatura,
			 :ls_cod_tipo_manutenzione
	  from manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :ll_anno_registrazione
		and num_registrazione = :ll_num_registrazione;
		
	if sqlca.sqlcode = 0 then
		update tab_tipi_manutenzione
			set des_ricambio_non_codificato = :ls_ricambio_non_codificato
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_attrezzatura = :ls_cod_attrezzatura
			and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	end if		
end if
end event

event pcd_modify;call super::pcd_modify;cb_ricambi.enabled = false
cb_controllo.enabled = false
cb_conferma.enabled = false
cb_operatori_manut.enabled = false
cb_salva_nav.enabled = false
cb_tarature.enabled = false
cb_scadenzario.enabled = false
cb_note_1.enabled = false
cb_ricerca_ricambio.enabled = true
dw_manutenzioni_1.object.b_ricerca_att.enabled = true

dw_manutenzioni_1.Object.ricambio_non_codificato.TabSequence = 100
dw_manutenzioni_1.Object.ricambio_non_codificato.Background.Color = RGB(255, 255, 255)
end event

event pcd_view;call super::pcd_view;cb_ricambi.enabled = true
cb_controllo.enabled = true
cb_conferma.enabled = true
cb_operatori_manut.enabled = true
cb_salva_nav.enabled = true
cb_tarature.enabled = true
cb_scadenzario.enabled = true
cb_note_1.enabled = true

cb_ricerca_ricambio.enabled = false
dw_manutenzioni_1.object.b_ricerca_att.enabled = false

dw_manutenzioni_1.Object.ricambio_non_codificato.TabSequence = 0
dw_manutenzioni_1.Object.ricambio_non_codificato.Background.Color = RGB(192, 192, 192)

end event

event rowfocuschanged;call super::rowfocuschanged;integer li_riga
string ls_cod_attrezzatura

setnull(ls_cod_attrezzatura)

if not isnull(dw_manutenzioni_1.getrow()) and dw_manutenzioni_1.getrow() > 0 then
	li_riga = dw_manutenzioni_1.getrow()
	if not isnull(dw_manutenzioni_1.getitemstring(li_riga, "cod_attrezzatura")) then
		ls_cod_attrezzatura = dw_manutenzioni_1.getitemstring(li_riga, "cod_attrezzatura")
	end if	
end if

if not isnull(ls_cod_attrezzatura) then
	f_PO_LoadDDDW_DW(dw_manutenzioni_1,"cod_tipo_manutenzione",sqlca,&
						  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
						  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_cod_attrezzatura + "'")
end if						  

if this.getrow() > 0 then
	wf_flag_tipo_intervento(dw_manutenzioni_1.getitemstring(1,"flag_tipo_intervento"), dw_manutenzioni_1.getitemstring(1,"flag_ricambio_codificato"))
	f_PO_LoadDDDW_sort(dw_manutenzioni_1,"cod_guasto",sqlca,&
						  "tab_guasti","cod_guasto","des_guasto + ' ' + cod_attrezzatura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + &
						  "' and cod_attrezzatura = '" + getitemstring(getrow(),"cod_attrezzatura") + &
						  "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in (select cod_guasto from tab_guasti where " + &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + &
						  getitemstring(getrow(),"cod_attrezzatura") + "')","1")
end if
end event

event pcd_validaterow;call super::pcd_validaterow;// 26/10/2004 VEDI ANOMALIA DI SINTEXCAL: ELIMINARE LA RICHIESTA DELLA TIPOLOGIA DI MANUTENZIONE

//if this.getitemstatus(this.getrow(), 0, primary!) = datamodified! then
//	if isnull(this.getitemstring(this.getrow(), "cod_tipo_manutenzione") ) then
//		messagebox("Attenzione", "Tipo manutenzione obbligatorio", exclamation!, ok!)
//		pcca.error = c_fatal
//		return 1
//	end if
//end if
end event

event pcd_save;call super::pcd_save;cb_ricambi.enabled = true
cb_controllo.enabled = true
cb_conferma.enabled = true
cb_operatori_manut.enabled = true
cb_salva_nav.enabled = true
cb_tarature.enabled = true
cb_scadenzario.enabled = true
cb_note_1.enabled = true
cb_ricerca_ricambio.enabled = false
dw_manutenzioni_1.object.b_ricerca_att.enabled = false

postevent("ue_salva_totale")


end event

event pcd_delete;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
	
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------

IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

//--------------------------------- Modifica Nicola --------------------------------------------
long ll_count, ld_num_protocollo, ll_anno_reg_programma, ll_num_reg_programma, ll_null
datetime ldt_data_giorno, ldt_data_giorno_primo

select count(cod_azienda)
  into :ll_count
  from tes_registro_tarature
 where cod_azienda = :s_cs_xx.cod_azienda
	and anno_registrazione = :il_anno_registrazione
	and num_registrazione = :il_num_registrazione;
	
if ll_count > 0 then
	if g_mb.messagebox("Omnia", "Attenzione nel registro tarature esistono dei dati:~r~n si vuole proseguire con la cancellazione?", Exclamation!, YesNo!, 2) = 2 then
		return				
	else 
		delete from tes_registro_tarature  //cancellazione registro tarature
				where cod_azienda = :s_cs_xx.cod_azienda
				  and anno_registrazione = :il_anno_registrazione
				  and num_registrazione = :il_num_registrazione;
				  
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in fase di cancellazione dati dalla tabella Registro Tarature")
			return
		end if	
	end if	
end if	 


declare cu_det_manutenzioni cursor for
	select num_protocollo
	  from det_manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :il_anno_registrazione
		and num_registrazione = :il_num_registrazione;

open cu_det_manutenzioni;

do while 1 = 1
	fetch cu_det_manutenzioni into :ld_num_protocollo;
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Manutenzioni")
		return
	end if	
	
	if sqlca.sqlcode = 0 then
		delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
		where cod_azienda = :s_cs_xx.cod_azienda
		  and num_protocollo = :ld_num_protocollo;
		  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if		  

		delete from det_manutenzioni  //cancellazione dettaglio manutenzioni
			where cod_azienda = :s_cs_xx.cod_azienda
			  and anno_registrazione = :il_anno_registrazione
			  and num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione dalla tabella DET_MANUTENZIONI:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if	  		  
		  
		delete from tab_protocolli  //cancellazione protocollo
		where cod_azienda = :s_cs_xx.cod_azienda
		  and num_protocollo = :ld_num_protocollo;
		  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if
	end if
loop	
close cu_det_manutenzioni;

//aggiornamento tabella det_cal_progr_manu
ll_anno_reg_programma = getitemnumber(1,"anno_reg_programma")
ll_num_reg_programma = getitemnumber(1,"num_reg_programma")
setnull(ll_null)

select data_giorno 
  into :ldt_data_giorno
  from det_cal_progr_manut
 where cod_azienda = :s_cs_xx.cod_azienda
	and anno_reg_manu = :il_anno_registrazione
	and num_reg_manu = :il_num_registrazione
group by data_giorno;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella det_cal_progr_manut " + sqlca.sqlerrtext)
	return
end if		
if sqlca.sqlcode = 0 then	
	select max(data_giorno)
	  into :ldt_data_giorno_primo	
	  from det_cal_progr_manut
	 where cod_azienda = :s_cs_xx.cod_azienda
		and data_giorno < :ldt_data_giorno
		and flag_stato_manutenzione <> 'O'
		and anno_registrazione = :ll_anno_reg_programma
		and num_registrazione = :ll_num_reg_programma;	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella det_cal_progr_manut " + sqlca.sqlerrtext)
		return
	end if		
	
	if isnull(ldt_data_giorno_primo) then ldt_data_giorno_primo = datetime(Date("1900-01-01"))
	
	update det_cal_progr_manut
		set flag_stato_manutenzione = 'A', 
			 anno_reg_manu = :ll_null,
			 num_reg_manu = :ll_null
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :ll_anno_reg_programma
		and num_registrazione = :ll_num_reg_programma
		and flag_stato_manutenzione = 'O'
		and data_giorno > :ldt_data_giorno_primo
		and data_giorno < :ldt_data_giorno;		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in aggiornamento dati nella tabella det_cal_progr_manut " + sqlca.sqlerrtext)
		return
	end if			
	
	update det_cal_progr_manut
		set flag_stato_manutenzione = 'A', 
			 anno_reg_manu = :ll_null,
			 num_reg_manu = :ll_null
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_reg_manu = :il_anno_registrazione
		and num_reg_manu = :il_num_registrazione
		and flag_stato_manutenzione = 'D';
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in aggiornamento dati nella tabella det_cal_progr_manut " + sqlca.sqlerrtext)
		return
	end if				
end if	

//---------------------------------- Fine Modifica ---------------------------------------------	

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], &
               c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse

parent.postevent("pc_save")

end event

event itemfocuschanged;call super::itemfocuschanged;if row > 0 and dwo.name = "cod_versione" then
	
	f_po_loaddddw_dw(this,"cod_versione",sqlca,"distinta_padri","cod_versione","des_versione",&
									 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + &
									 getitemstring(row,"cod_ricambio") + "' and flag_esplodi_in_doc = 'S'")
	
end if
end event

event updatestart;call super::updatestart;long 	 ll_i, ll_anno_registrazione, ll_num_registrazione
string ls_cod_attrezzatura, ls_cod_guasto, ls_test

for ll_i = 1 to rowcount()
	
	ls_cod_attrezzatura = getitemstring(ll_i,"cod_attrezzatura")
	
	if isnull(ls_cod_attrezzatura) then
		continue
	end if
	
	ls_cod_guasto = getitemstring(ll_i,"cod_guasto")
	
	if isnull(ls_cod_guasto) then
		continue
	end if
	
	select cod_azienda
	into	 :ls_test
	from	 tab_guasti
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_guasto = :ls_cod_guasto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in controllo esistenza guasto specifico: " + sqlca.sqlerrtext,stopsign!)
		return 1
	elseif sqlca.sqlcode = 100 or isnull(ls_test) then
		
		insert
		into 	 tab_guasti
			  	 (cod_azienda,
			  	 cod_attrezzatura,
			  	 cod_guasto,
			  	 des_guasto)
		select cod_azienda,
				 :ls_cod_attrezzatura,
				 cod_guasto,
				 des_guasto
		from	 tab_guasti_generici
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_guasto = :ls_cod_guasto;
				 
		if sqlca.sqlcode <> 0 then
			rollback;
			g_mb.messagebox("OMNIA","Errore in inserimento guasto specifico da guasto generico: " + sqlca.sqlerrtext,stopsign!)
			return 1
		else
			commit;
			f_PO_LoadDDDW_sort(dw_manutenzioni_1,"cod_guasto",sqlca,&
						  "tab_guasti","cod_guasto","des_guasto + ' ' + cod_attrezzatura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + &
						  "' and cod_attrezzatura = '" + ls_cod_attrezzatura + &
						  "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in (select cod_guasto from tab_guasti where " + &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + &
						  ls_cod_attrezzatura + "')","1")
		end if
		
	end if
	
next

il_handle_modificato = 0
il_handle_cancellato = 0
if modifiedcount() > 0 then
	il_handle_modificato = tv_1.finditem(CurrentTreeItem!, 0)
end if
if deletedcount() > 0 then
	il_handle_cancellato = tv_1.finditem(CurrentTreeItem!, 0)
end if


for ll_i = 1 to deletedcount()
	ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione", delete!, true)
	ll_num_registrazione = getitemnumber(ll_i, "num_registrazione", delete!, true)
	
	// stefanop 12/04/2011: cancello pianificazioni
	if wf_cancella_pianificazioni(ll_anno_registrazione, ll_num_registrazione) = false then
		return 1
	end if
	// ----
next
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_manutenzioni_1,"cod_attrezzatura")
end choose
end event

