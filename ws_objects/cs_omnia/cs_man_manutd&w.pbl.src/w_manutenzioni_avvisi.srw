﻿$PBExportHeader$w_manutenzioni_avvisi.srw
forward
global type w_manutenzioni_avvisi from w_cs_xx_principale
end type
type dw_1 from uo_cs_xx_dw within w_manutenzioni_avvisi
end type
end forward

global type w_manutenzioni_avvisi from w_cs_xx_principale
integer width = 2327
integer height = 1044
boolean center = true
dw_1 dw_1
end type
global w_manutenzioni_avvisi w_manutenzioni_avvisi

type variables
s_cs_xx_parametri istr_parametri
end variables

on w_manutenzioni_avvisi.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_manutenzioni_avvisi.destroy
call super::destroy
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;istr_parametri = message.powerobjectparm

setnull(message.powerobjectparm)



dw_1.set_dw_key("cod_azienda")
dw_1.set_dw_key("anno_registrazione")
dw_1.set_dw_key("num_registrazione")
dw_1.set_dw_key("progressivo")

iuo_dw_main = dw_1
dw_1.set_dw_options(sqlca, pcca.null_object, c_default, c_nonew + c_nomodify)



title = "Pianificazioni manutenzione " + string(istr_parametri.parametro_ul_1) + "/" + string(istr_parametri.parametro_ul_2)

//dw_1.postevent("pcd_retrieve")
end event

type dw_1 from uo_cs_xx_dw within w_manutenzioni_avvisi
integer x = 23
integer y = 20
integer width = 2240
integer height = 900
integer taborder = 10
string dataobject = "d_manutenzioni_avvisi"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_rows

ll_rows = dw_1.retrieve(s_cs_xx.cod_azienda, istr_parametri.parametro_ul_1, istr_parametri.parametro_ul_2, today())

if ll_rows < 0 then
	PCCA.Error = c_Fatal
end if
end event

event pcd_new;/**
 * ATTENZIONE
 * tolto ancestor script
 **/
 
 
 
return 1
end event

event pcd_modify;/**
 * ATTENZIONE
 * tolto ancestor script
 **/
 
 
return 1
end event

