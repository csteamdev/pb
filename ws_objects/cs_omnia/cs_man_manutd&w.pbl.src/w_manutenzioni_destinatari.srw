﻿$PBExportHeader$w_manutenzioni_destinatari.srw
$PBExportComments$window dei destinatari dell'ordine di lavoro via mail
forward
global type w_manutenzioni_destinatari from w_cs_xx_risposta
end type
type ole_1 from olecontrol within w_manutenzioni_destinatari
end type
type cb_note_esterne from commandbutton within w_manutenzioni_destinatari
end type
type dw_manutenzioni_destinatari from uo_cs_xx_dw within w_manutenzioni_destinatari
end type
end forward

global type w_manutenzioni_destinatari from w_cs_xx_risposta
integer width = 2985
integer height = 1064
string title = "Destinatari Ordine di Lavoro"
ole_1 ole_1
cb_note_esterne cb_note_esterne
dw_manutenzioni_destinatari dw_manutenzioni_destinatari
end type
global w_manutenzioni_destinatari w_manutenzioni_destinatari

type prototypes
function long GetTempPathA  (Long nBufferLength, ref String lpBuffer) Library  "kernel32.dll" ALIAS FOR "GetTempPathA;ansi"


end prototypes

forward prototypes
public function blob wf_carica_blob (string fs_path)
end prototypes

public function blob wf_carica_blob (string fs_path);long ll_filelen, ll_bytes_read, ll_new_pos
integer li_counter, li_loops, fh1

blob lb_tempblob, lb_bufferblob, lb_resultblob, lb_blankblob
		
// Trova la lunghezza del file
ll_filelen = FileLength(fs_path)

// FileRead legge 32KB alla volta => Se il file supera i 32 KB, SERVONO PIU' CICLI
// Calcola il numero di cicli
IF ll_filelen > 32765 THEN 
	IF Mod(ll_filelen,32765) = 0 THEN 
		li_loops = ll_filelen/32765 
	ELSE 
		// ...se c'è il resto, serve un ciclo in più
		li_loops = (ll_filelen/32765) + 1 
	END IF 
ELSE 
	li_loops = 1 
END IF

//carico il blob
fh1 = FileOpen(fs_path, StreamMode!)
if fh1 <> -1 then
	ll_new_pos = 0
	// Esegue li_loops cicli
	FOR li_counter = 1 to li_loops 
		// Legge i dati in una variabile TEMPORANEA, di tipo BLOB
		ll_bytes_read = FileRead(fh1, lb_tempblob) 
		// ...e li accoda in un secondo BLOB
		lb_bufferblob = lb_bufferblob + lb_tempblob 
		ll_new_pos = ll_new_pos + ll_bytes_read 
		// Sposta il puntatore al file nella posizione di lettura successiva
		FileSeek(fh1,ll_new_pos,FROMBEGINNING!)
		// Accoda i segmenti alla cache fino a costituire una "tranche" di circa 1 mega
		if len(lb_bufferblob) > 1000000 then
			// Se la dimensione limite è superata, accoda i dati in un terzo BLOB "risultato"
			lb_resultblob = lb_resultblob + lb_bufferblob 
			// ...e "sega" il BLOB temporaneo 2 assegnandoli un blob nullo
			lb_resultblob = lb_blankblob 
		end if 
	NEXT
	// Accoda i dati restanti in total_blob
	lb_resultblob = lb_resultblob + lb_bufferblob 
	// Fine lettura
	FileClose(fh1)
	
end if

return lb_resultblob


end function

on w_manutenzioni_destinatari.create
int iCurrent
call super::create
this.ole_1=create ole_1
this.cb_note_esterne=create cb_note_esterne
this.dw_manutenzioni_destinatari=create dw_manutenzioni_destinatari
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_1
this.Control[iCurrent+2]=this.cb_note_esterne
this.Control[iCurrent+3]=this.dw_manutenzioni_destinatari
end on

on w_manutenzioni_destinatari.destroy
call super::destroy
destroy(this.ole_1)
destroy(this.cb_note_esterne)
destroy(this.dw_manutenzioni_destinatari)
end on

event pc_setwindow;call super::pc_setwindow;dw_manutenzioni_destinatari.set_dw_options( sqlca, pcca.null_object, c_nonew + c_nodelete + c_default, c_default)
end event

type ole_1 from olecontrol within w_manutenzioni_destinatari
boolean visible = false
integer x = 2446
integer y = 1020
integer width = 411
integer height = 432
integer taborder = 20
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
string binarykey = "w_manutenzioni_destinatari.win"
omactivation activation = activateondoubleclick!
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
end type

type cb_note_esterne from commandbutton within w_manutenzioni_destinatari
integer x = 2565
integer y = 868
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;integer li_risposta, li_filenum
long ll_anno, ll_numero, ll_i, ll_num_sequenza, ll_pos
string ls_db, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_destinatario, ls_valore, ls_str, ls_nome_doc
//
//blob lbl_null
//
//
//
//OLEControl o1
//
//transaction sqlcb
//
//if dw_manutenzioni_destinatari.getrow() > 0 then
//	setnull(lbl_null)
//	
//	ll_i = dw_manutenzioni_destinatari.getrow()
//	
//	ll_anno = dw_manutenzioni_destinatari.getitemnumber(ll_i, "anno_registrazione")
//	
//	ll_numero = dw_manutenzioni_destinatari.getitemnumber(ll_i, "num_registrazione")
//	
//	ls_cod_tipo_lista_dist = dw_manutenzioni_destinatari.getitemstring(ll_i, "cod_tipo_lista_dist")
//	
//	ls_cod_lista_dist = dw_manutenzioni_destinatari.getitemstring(ll_i, "cod_lista_dist")	
//	
//	ls_cod_destinatario = dw_manutenzioni_destinatari.getitemstring(ll_i, "cod_destinatario")
//	
//	ll_num_sequenza = dw_manutenzioni_destinatari.getitemnumber(ll_i, "num_sequenza")
//
//	ls_db = f_db()
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//
//		selectblob blob
//		into       :s_cs_xx.parametri.parametro_bl_1
//		from       manutenzioni_destinatari
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//				     anno_registrazione = :ll_anno and
//				     num_registrazione = :ll_numero and
//				     cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
//				     cod_lista_dist = :ls_cod_lista_dist and
//				     cod_destinatario = :ls_cod_destinatario and
//				     num_sequenza = :ll_num_sequenza	
//		using      sqlcb;	
//		
//		if sqlcb.sqlcode <> 0 then
//			s_cs_xx.parametri.parametro_bl_1 = lbl_null
//		end if
//		
//		destroy sqlcb;
//		
//	else
//		
//		selectblob blob
//		into       :s_cs_xx.parametri.parametro_bl_1
//		from       manutenzioni_destinatari
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//				     anno_registrazione = :ll_anno and
//				     num_registrazione = :ll_numero and
//				     cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
//				     cod_lista_dist = :ls_cod_lista_dist and
//				     cod_destinatario = :ls_cod_destinatario and
//				     num_sequenza = :ll_num_sequenza;
//	
//		if sqlca.sqlcode <> 0 then
//			s_cs_xx.parametri.parametro_bl_1 = lbl_null
//		end if
//	
//	end if
//	
//	// *** lo scrivo su file: trovo directory temporanea
//	setnull(ls_valore)
//
//	select stringa
//	into   :ls_valore
//	from   parametri_azienda
//	where  cod_azienda = :s_cs_xx.cod_azienda
//	and    cod_parametro = 'TDD';
//
//	if sqlca.sqlcode = -1 then
//		
//		g_mb.messagebox("OMNIA","Attenzione: Errore nella select del parametro TDD! Dettaglio: " + sqlca.sqlerrtext)
//		
//		return -1	
//		
//	end if
//
//	if sqlca.sqlcode = 100 then
//		
//		setnull(ls_valore)
//		
//		li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "ris", ls_valore)	
//		
//	end if
//
//	if isnull(ls_valore) or ls_valore = "" then
//		
//		g_mb.messagebox("OMNIA","Attenzione: impostare il parametro aziendale TDD o il parametro del registro ris. ")
//		
//		return -1		
//		
//	end if
//
//	setnull(ls_str)
//	
//	ls_str = left(ls_valore, 1)
//	
//	if ls_str <> "\" then ls_valore = "\" + ls_valore
//
//	setnull(ls_str)
//	
//	ls_str = right(ls_valore, 1)
//	
//	if ls_str <> "\" then ls_valore = ls_valore + "\"
//
//	ls_valore = s_cs_xx.volume + ls_valore		
//	
//	ls_nome_doc = ls_valore + string(ll_anno) + string(ll_numero) + ".PDF"
//
//	li_FileNum = FileOpen( ls_nome_doc, StreamMode!, Write!, Shared!, Replace!)
//	
//	if li_FileNum = -1 then
//		
//		g_mb.messagebox("OMNIA","Errore nell'apertura del file: " + ls_nome_doc)
//		
//		return -1
//		
//	end if	
//	
//	long ll_len
//	
//	ll_len = len(s_cs_xx.parametri.parametro_bl_1)
//	
//	ll_pos = 1
//	
//	Do While FileWrite(li_FileNum,BlobMid( s_cs_xx.parametri.parametro_bl_1, ll_pos, 32765)) > 0
//		
//		ll_pos += 32765
//		
//	Loop
//
//	FileClose(li_FileNum)
//	
//   //o1 = CREATE olecontrol  
//	
//	ole_1.insertfile( ls_nome_doc)
//	
//	s_cs_xx.parametri.parametro_bl_1 = ole_1.objectdata
//	
//	//destroy o1
//
//	FileDelete(ls_nome_doc)	
//	
//	window_open(w_ole, 0)
//	
//end if

ll_i = dw_manutenzioni_destinatari.getrow()

if ll_i < 1 then
	return
end if

s_cs_xx.parametri.parametro_ul_1 = dw_manutenzioni_destinatari.getitemnumber(ll_i, "anno_registrazione")
s_cs_xx.parametri.parametro_ul_2 = dw_manutenzioni_destinatari.getitemnumber(ll_i, "num_registrazione")
s_cs_xx.parametri.parametro_s_1 = dw_manutenzioni_destinatari.getitemstring(ll_i, "cod_tipo_lista_dist")
s_cs_xx.parametri.parametro_s_2 = dw_manutenzioni_destinatari.getitemstring(ll_i, "cod_lista_dist")	
s_cs_xx.parametri.parametro_s_3 = dw_manutenzioni_destinatari.getitemstring(ll_i, "cod_destinatario")
s_cs_xx.parametri.parametro_ul_3 = dw_manutenzioni_destinatari.getitemnumber(ll_i, "num_sequenza")

open(w_manutenzioni_destinatari_ole)
end event

type dw_manutenzioni_destinatari from uo_cs_xx_dw within w_manutenzioni_destinatari
event ue_inserisci_destinatari ( )
integer x = 23
integer y = 20
integer width = 2903
integer height = 840
integer taborder = 0
string dataobject = "d_manutenzioni_destinatari"
end type

event ue_inserisci_destinatari();//string ls_flag_blocco, ls_cod_destinatario
//
//long   ll_i, ll_prova
//
//if UpperBound(s_cs_xx.parametri.parametro_s_1_a) < 1 then return 
//
//for ll_i = 1 to UpperBound(s_cs_xx.parametri.parametro_s_1_a)
//	
//	ls_cod_destinatario = s_cs_xx.parametri.parametro_s_1_a[ll_i]
//	
//	select flag_blocco
//	into   :ls_flag_blocco
//	from   tab_ind_dest
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_destinatario = :ls_cod_destinatario;
//	
//	if ls_flag_blocco = "S" then continue
//	
//	select count(*)
//	into   :ll_prova
//	from   liste_dist_doc
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_destinatario = :ls_cod_destinatario and
//			 progr_lista_dist = :s_cs_xx.parametri.parametro_ul_2;
//			 
//	if ll_prova > 0 then continue
//	
//	INSERT INTO liste_dist_doc  
//  			    ( cod_azienda,   
//           		cod_tipo_lista_dist,   
//		         cod_lista_dist,   
//      	      cod_destinatario,   
//          	   num_sequenza,   
//	            progr_lista_dist,  
//					cod_utente, 
//    	         path_doc,   
// 	            flag_ca,   
//    	         data_comm )  
//  VALUES    ( :s_cs_xx.cod_azienda,   
//   	        'X',   
//      	     'X',   
//         	  :ls_cod_destinatario,   
//	           1,   
//   	        :s_cs_xx.parametri.parametro_ul_2,   
//				  :s_cs_xx.cod_utente, 
//      	     null,   
//         	  'N',   
//	           null)  ;
//				  
//	if sqlca.sqlcode <> 0 then
//		
//		messagebox("OMNIA", "Errore l'inserimento dei destinatari del documento~r~nOperazione Annullata.~r~n"+sqlca.sqlerrtext)
//		
//		rollback;
//		
//		return
//		
//	end if	
//	
//	
//next	
//
//commit; 
//
//parent.postevent("pc_retrieve")
//
//
//
//
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve( s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
05w_manutenzioni_destinatari.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
15w_manutenzioni_destinatari.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
