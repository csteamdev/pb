﻿$PBExportHeader$w_manutenzioni_ricambi.srw
forward
global type w_manutenzioni_ricambi from w_cs_xx_principale
end type
type dw_manutenzioni_ricambi_det from uo_cs_xx_dw within w_manutenzioni_ricambi
end type
type dw_manutenzioni_ricambi from uo_cs_xx_dw within w_manutenzioni_ricambi
end type
end forward

global type w_manutenzioni_ricambi from w_cs_xx_principale
integer width = 3813
integer height = 1912
string title = "Ricambi Manutenzione"
dw_manutenzioni_ricambi_det dw_manutenzioni_ricambi_det
dw_manutenzioni_ricambi dw_manutenzioni_ricambi
end type
global w_manutenzioni_ricambi w_manutenzioni_ricambi

forward prototypes
public function integer wf_carica_prezzo (string fs_cod_prodotto, decimal fd_quantita)
public subroutine wf_calcola_totale (long fl_row, ref datawindow fdw_data)
end prototypes

public function integer wf_carica_prezzo (string fs_cod_prodotto, decimal fd_quantita);string ls_cod_prodotto, ls_cod_cliente,ls_cod_attrezzatura,ls_cod_tipo_listino_prodotto,ls_cod_valuta
datetime ldt_data_registrazione
dec{4} ld_quantita, ld_prezzo
uo_condizioni_cliente luo_condizioni_cliente

ldt_data_registrazione = datetime(today(),00:00:00)

ls_cod_prodotto = fs_cod_prodotto

ld_quantita = fd_quantita

ls_cod_attrezzatura = dw_manutenzioni_ricambi.i_parentdw.getitemstring(dw_manutenzioni_ricambi.i_parentdw.getrow(), "cod_attrezzatura")

select cod_cliente
into   :ls_cod_cliente
from   anag_divisioni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_divisione in (select cod_divisione 
								 from   tab_aree_aziendali
								 where  cod_azienda = :s_cs_xx.cod_azienda and
										  cod_area_aziendale in ( select cod_area_aziendale
																		  from   anag_attrezzature
																		  where  cod_azienda = :s_cs_xx.cod_azienda and
																					cod_attrezzatura = :ls_cod_attrezzatura));

if not isnull(ls_cod_cliente) then

	select cod_tipo_listino_prodotto,
			 cod_valuta
	into   :ls_cod_tipo_listino_prodotto,
			 :ls_cod_valuta
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
else
	setnull(ls_cod_tipo_listino_prodotto)
	setnull(ls_cod_valuta)
end if

luo_condizioni_cliente = create uo_condizioni_cliente
luo_condizioni_cliente.str_parametri.ldw_oggetto = dw_manutenzioni_ricambi
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
luo_condizioni_cliente.str_parametri.dim_1 = 0
luo_condizioni_cliente.str_parametri.dim_2 = 0
luo_condizioni_cliente.str_parametri.quantita = ld_quantita
luo_condizioni_cliente.str_parametri.valore = 0
// luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
// luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
luo_condizioni_cliente.str_parametri.fat_conversione_ven = 1
luo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ricambio"
luo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_ricambio"//"prezzo_vendita"
luo_condizioni_cliente.ib_setitem = true//false
luo_condizioni_cliente.ib_provvigioni = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
luo_condizioni_cliente.wf_condizioni_cliente()

//Donato 12-06-2009 se la struttura variazioni è vuota allora metti il prezzo a zero
if upperbound(luo_condizioni_cliente.str_output.variazioni) > 0 then
	ld_prezzo = luo_condizioni_cliente.str_output.variazioni[1]
else
	ld_prezzo = 0
end if
//ld_prezzo = luo_condizioni_cliente.str_output.variazioni[1]

destroy luo_condizioni_cliente
	
return ld_prezzo
end function

public subroutine wf_calcola_totale (long fl_row, ref datawindow fdw_data);dec{4} ld_prezzo, ld_quantita, ld_sconto_1, ld_sconto_2, ld_totale

if fl_row>0 then
else
	return
end if

fdw_data.accepttext()

ld_prezzo = fdw_data.getitemnumber(fl_row, "prezzo_ricambio")
ld_quantita = fdw_data.getitemnumber(fl_row, "quan_utilizzo")
ld_sconto_1 = fdw_data.getitemnumber(fl_row, "sconto_1")
ld_sconto_2 = fdw_data.getitemnumber(fl_row, "sconto_2")

if isnull(ld_prezzo) then ld_prezzo = 0
if isnull(ld_quantita) then ld_quantita = 0
if isnull(ld_sconto_1) then ld_sconto_1 = 0
if isnull(ld_sconto_2) then ld_sconto_2 = 0

ld_totale = ld_quantita * ld_prezzo

if ld_totale = 0 then 
	fdw_data.setitem(fl_row, "imponibile_iva", 0)
	return
else
	if ld_sconto_1 > 0 and not isnull(ld_sconto_1) then
		ld_totale = ld_totale - (ld_totale * ld_sconto_1 / 100)
	end if
	
	if ld_sconto_2 > 0 and not isnull(ld_sconto_2) then
		ld_totale = ld_totale - (ld_totale * ld_sconto_2 / 100)
	end if
	
	fdw_data.setitem(fl_row, "imponibile_iva", round(ld_totale, 2))
end if



return
end subroutine

on w_manutenzioni_ricambi.create
int iCurrent
call super::create
this.dw_manutenzioni_ricambi_det=create dw_manutenzioni_ricambi_det
this.dw_manutenzioni_ricambi=create dw_manutenzioni_ricambi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_manutenzioni_ricambi_det
this.Control[iCurrent+2]=this.dw_manutenzioni_ricambi
end on

on w_manutenzioni_ricambi.destroy
call super::destroy
destroy(this.dw_manutenzioni_ricambi_det)
destroy(this.dw_manutenzioni_ricambi)
end on

event pc_setwindow;call super::pc_setwindow;string ls_cod_kit, ls_ver_kit, ls_eseguito


ls_eseguito = s_cs_xx.parametri.parametro_s_1
setnull(s_cs_xx.parametri.parametro_s_1)


dw_manutenzioni_ricambi.set_dw_key("cod_azienda")
dw_manutenzioni_ricambi.set_dw_key("anno_registrazione")
dw_manutenzioni_ricambi.set_dw_key("num_registrazione")



iuo_dw_main = dw_manutenzioni_ricambi

dw_manutenzioni_ricambi.set_dw_options(sqlca, &
                                       	i_openparm , &
                                       	c_scrollparent , &
                                       	c_default)

dw_manutenzioni_ricambi_det.set_dw_options(sqlca, &
													 dw_manutenzioni_ricambi, &
													 c_sharedata + c_scrollparent, &
													 c_default)


//if ls_eseguito = "S" then
//	dw_manutenzioni_ricambi.set_dw_options(	sqlca,	&
//																	i_openparm, &
//																	c_nonew + c_nodelete + c_nomodify + c_scrollparent, &
//																	c_nohighlightselected)
//																	
//	dw_manutenzioni_ricambi_det.set_dw_options(		sqlca, &
//																			dw_manutenzioni_ricambi, &
//																			c_sharedata + c_scrollparent + c_nonew + c_nodelete + c_nomodify, &
//																			c_default)
//else
//	dw_manutenzioni_ricambi.set_dw_options(	sqlca, &
//																	i_openparm, &
//																	c_scrollparent, &
//																	c_default)
//																	
//	dw_manutenzioni_ricambi_det.set_dw_options(		sqlca, &
//																			dw_manutenzioni_ricambi, &
//																			c_sharedata + c_scrollparent, &
//																			c_default)
//end if

dw_manutenzioni_ricambi.change_dw_current()

end event

type dw_manutenzioni_ricambi_det from uo_cs_xx_dw within w_manutenzioni_ricambi
event ue_totale_riga ( )
integer x = 27
integer y = 1092
integer width = 3721
integer height = 696
integer taborder = 20
string dataobject = "d_manutenzioni_ricambi_det"
borderstyle borderstyle = styleraised!
end type

event ue_totale_riga();wf_calcola_totale(getrow(), dw_manutenzioni_ricambi_det)
end event

event buttonclicked;call super::buttonclicked;

if row > 0 then
	choose case dwo.name
		case "b_ricerca_prodotto"
			dw_manutenzioni_ricambi_det.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_manutenzioni_ricambi_det, "cod_prodotto")	
	end choose
end if
end event

event itemchanged;call super::itemchanged;boolean				ib_global_service=false

dec{4}				ld_prezzo_acquisto, ld_null, ld_quan_utilizzo, ld_prezzo_acquisto_old

string				ls_des_prodotto, ls_null, ls_flag, ls_cod_prodotto



select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if sqlca.sqlcode = 0 and ls_flag = "S" then
	ib_global_service = true	
end if

choose case i_colname
	
	case "cod_prodotto"
		
		if ib_global_service then
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode = 100 then
				// digitato prodotto inesistente
				setnull(ls_null)
				setnull(ld_null)
				setitem(row,"cod_prodotto",ls_null)
				setitem(row,"des_prodotto",ls_null)
				setitem(row,"quan_utilizzo",0)
				setitem(row,"prezzo_ricambio",0)
				setitem(row,"sconto_1",0)
				setitem(row,"sconto_2",0)
				setitem(row,"imponibile_iva",0)
				s_cs_xx.parametri.parametro_pos_ricerca = i_coltext + "*"
				s_cs_xx.parametri.parametro_tipo_ricerca = 1
				dw_manutenzioni_ricambi.postevent("ue_cerca_prod")
				
				return
			end if
			
			ld_prezzo_acquisto_old = getitemnumber(row,"prezzo_ricambio")
			
			ld_prezzo_acquisto = wf_carica_prezzo(i_coltext, 1)				 
			
			if ld_prezzo_acquisto>0 then
				//le condizioni impongono un prezzo > 0
				setitem(row,"prezzo_ricambio", ld_prezzo_acquisto)
			else
				//nessun prezzo trovato o imposto prezzo zero
				if ld_prezzo_acquisto_old > 0 then
					//se c'era già un prezzo specificato lascio quello
					setitem(row,"prezzo_ricambio", ld_prezzo_acquisto_old)
				else
					//altrimenti metto zero
					setitem(row,"prezzo_ricambio", 0)
				end if
			end if
			//setitem(row,"prezzo_ricambio", ld_prezzo_acquisto)
			
			setitem(row,"des_prodotto",ls_des_prodotto)
			setitem(getrow(),"quan_utilizzo",1)
			
			
			dw_manutenzioni_ricambi_det.postevent("ue_totale_riga")
			
		else		
			select des_prodotto,
					 prezzo_acquisto
			into   :ls_des_prodotto,
					 :ld_prezzo_acquisto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode < 0 then
				g_mb.error("OMNIA","Errore in lettura descrizione prodotto da anag_prodotti: " + sqlca.sqlerrtext)
				return 1
			elseif sqlca.sqlcode = 100 then
				setnull(ls_null)
				setnull(ld_null)
				setitem(row,"cod_prodotto",ls_null)
				setitem(row,"des_prodotto",ls_null)
				setitem(row,"quan_utilizzo",ld_null)
				setitem(row,"prezzo_ricambio",ld_null)
				s_cs_xx.parametri.parametro_pos_ricerca = i_coltext + "*"
				s_cs_xx.parametri.parametro_tipo_ricerca = 1
				dw_manutenzioni_ricambi.triggerevent("ue_cerca_prod")
				
				dw_manutenzioni_ricambi_det.postevent("ue_totale_riga")
				
			else
				setitem(row,"des_prodotto",ls_des_prodotto)
				setitem(getrow(),"quan_utilizzo",1)
				setitem(row,"prezzo_ricambio",ld_prezzo_acquisto)
				
				dw_manutenzioni_ricambi_det.postevent("ue_totale_riga")
			end if
			
		end if		
		
	case "quan_utilizzo"
		
		ld_quan_utilizzo = dec(i_coltext)
		ls_cod_prodotto = getitemstring(row, "cod_prodotto")
		ld_prezzo_acquisto_old = getitemnumber(row,"prezzo_ricambio")
		
		ld_prezzo_acquisto = wf_carica_prezzo(ls_cod_prodotto, ld_quan_utilizzo)				 
		
		if ld_prezzo_acquisto>0 then
			//le condizioni impongono un prezzo > 0
			setitem(row,"prezzo_ricambio", ld_prezzo_acquisto)
		else
			//nessun prezzo trovato o imposto prezzo zero
			if ld_prezzo_acquisto_old > 0 then
				//se c'era già un prezzo specificato lascio quello
				setitem(row,"prezzo_ricambio", ld_prezzo_acquisto_old)
			else
				//altrimenti metto zero
				setitem(row,"prezzo_ricambio", 0)
			end if
		end if
		
		
		
		dw_manutenzioni_ricambi_det.postevent("ue_totale_riga")
			
	case "sconto_1", "sconto_2", "prezzo_ricambio"

		dw_manutenzioni_ricambi_det.postevent("ue_totale_riga")
			
end choose
end event

event pcd_new;call super::pcd_new;long ll_progressivo, ll_null, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")

setnull(ll_null)

setitem(getrow(),"flag_utilizzo","S")

setitem(getrow(),"anno_reg_mov_mag",ll_null)
setitem(getrow(),"num_reg_mov_mag",ll_null)

end event

type dw_manutenzioni_ricambi from uo_cs_xx_dw within w_manutenzioni_ricambi
event ue_cerca_prod ( )
event ue_totale_riga ( )
integer x = 27
integer y = 24
integer width = 3721
integer height = 1044
integer taborder = 10
string dataobject = "d_manutenzioni_ricambi"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event ue_cerca_prod();change_dw_current()
guo_ricerca.uof_ricerca_prodotto(dw_manutenzioni_ricambi,"cod_prodotto")
end event

event ue_totale_riga();wf_calcola_totale(getrow(), dw_manutenzioni_ricambi)
end event

event updatestart;call super::updatestart;//long ll_i
//
//
//for ll_i = 1 to rowcount()
//	
//	if isnull(getitemstring(ll_i,"cod_prodotto")) then
//		g_mb.warning("OMNIA","Impostare il codice prodotto del ricambio!")
//		return 1
//	end if
//	
//	if getitemnumber(ll_i,"quan_utilizzo") <= 0 then
//		g_mb.warning("OMNIA","Impostare la quantita di utilizzo del ricambio!")
//		return 1
//	end if
//	
//next
end event

event ue_key;call super::ue_key;if getcolumnname() = "cod_prodotto" then
	
	if key = keyF1!  and keyflags = 1 then
		postevent("ue_cerca_prod")
	end if
		
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione,ll_progressivo

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or getitemnumber(ll_i, "anno_registrazione") = 0 then
		setitem(ll_i,"anno_registrazione",ll_anno_registrazione)
	end if
	
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or getitemnumber(ll_i, "num_registrazione") = 0 then
		setitem(ll_i,"num_registrazione",ll_num_registrazione)
	end if
	
   if isnull(this.getitemnumber(ll_i, "prog_riga_ricambio")) or getitemnumber(ll_i, "prog_riga_ricambio") = 0 then
		select max(prog_riga_ricambio)
		into   :ll_progressivo
		from   manutenzioni_ricambi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo da manutenzioni_ricambi: " + sqlca.sqlerrtext)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
			ll_progressivo = 0
		end if

		ll_progressivo ++

		setitem(getrow(),"prog_riga_ricambio",ll_progressivo)
	end if
	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")

retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if



parent.title = "Ricambi - Manutenzione: " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)

end event

event pcd_new;call super::pcd_new;long ll_progressivo, ll_null, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")

setnull(ll_null)

setitem(getrow(),"flag_utilizzo","S")

setitem(getrow(),"anno_reg_mov_mag",ll_null)
setitem(getrow(),"num_reg_mov_mag",ll_null)

end event

event itemchanged;call super::itemchanged;boolean ib_global_service=false

dec{4} ld_prezzo_acquisto, ld_null, ld_quan_utilizzo, ld_prezzo_acquisto_old

string ls_des_prodotto, ls_null, ls_flag, ls_cod_prodotto

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if sqlca.sqlcode = 0 and ls_flag = "S" then
	ib_global_service = true	
end if

choose case i_colname
	
	case "cod_prodotto"
		
		if ib_global_service then
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode = 100 then
				// digitato prodotto inesistente
				setnull(ls_null)
				setnull(ld_null)
				setitem(row,"cod_prodotto",ls_null)
				setitem(row,"des_prodotto",ls_null)
				setitem(row,"quan_utilizzo",0)
				setitem(row,"prezzo_ricambio",0)
				setitem(row,"sconto_1",0)
				setitem(row,"sconto_2",0)
				setitem(row,"imponibile_iva",0)
				s_cs_xx.parametri.parametro_pos_ricerca = i_coltext + "*"
				s_cs_xx.parametri.parametro_tipo_ricerca = 1
				postevent("ue_cerca_prod")
				return
			end if
			
			ld_prezzo_acquisto_old = getitemnumber(row,"prezzo_ricambio")
			
			ld_prezzo_acquisto = wf_carica_prezzo(i_coltext, 1)
			
			if ld_prezzo_acquisto>0 then
				//le condizioni impongono un prezzo > 0
				setitem(row,"prezzo_ricambio", ld_prezzo_acquisto)
			else
				//nessun prezzo trovato o imposto prezzo zero
				if ld_prezzo_acquisto_old > 0 then
					//se c'era già un prezzo specificato lascio quello
					setitem(row,"prezzo_ricambio", ld_prezzo_acquisto_old)
				else
					//altrimenti metto zero
					setitem(row,"prezzo_ricambio", 0)
				end if
			end if
			//setitem(row,"prezzo_ricambio",ld_prezzo_acquisto)
			
			setitem(row,"des_prodotto", ls_des_prodotto)
			setitem(getrow(),"quan_utilizzo",1)
			
			
			postevent("ue_totale_riga")
			
		else		
			select des_prodotto,
					 prezzo_acquisto
			into   :ls_des_prodotto,
					 :ld_prezzo_acquisto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :i_coltext;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in lettura descrizione prodotto da anag_prodotti: " + sqlca.sqlerrtext)
				return 1
			elseif sqlca.sqlcode = 100 then
				setnull(ls_null)
				setnull(ld_null)
				setitem(row,"cod_prodotto",ls_null)
				setitem(row,"des_prodotto",ls_null)
				setitem(row,"quan_utilizzo",ld_null)
				setitem(row,"prezzo_ricambio",ld_null)
				s_cs_xx.parametri.parametro_pos_ricerca = i_coltext + "*"
				s_cs_xx.parametri.parametro_tipo_ricerca = 1
				triggerevent("ue_cerca_prod")
				postevent("ue_totale_riga")
			else
				setitem(row,"des_prodotto",ls_des_prodotto)
				setitem(getrow(),"quan_utilizzo",1)
				setitem(row,"prezzo_ricambio",ld_prezzo_acquisto)
				postevent("ue_totale_riga")
			end if
			
		end if		
		
	case "quan_utilizzo"
		
		ld_quan_utilizzo = dec(i_coltext)
		ls_cod_prodotto = getitemstring(row, "cod_prodotto")
		
		ld_prezzo_acquisto_old = getitemnumber(row,"prezzo_ricambio")
		
		ld_prezzo_acquisto = wf_carica_prezzo(ls_cod_prodotto, ld_quan_utilizzo)				 
		
		if ld_prezzo_acquisto>0 then
			//le condizioni impongono un prezzo > 0
			setitem(row,"prezzo_ricambio", ld_prezzo_acquisto)
		else
			//nessun prezzo trovato o imposto prezzo zero
			if ld_prezzo_acquisto_old > 0 then
				//se c'era già un prezzo specificato lascio quello
				setitem(row,"prezzo_ricambio", ld_prezzo_acquisto_old)
			else
				//altrimenti metto zero
				setitem(row,"prezzo_ricambio", 0)
			end if
		end if
		//setitem(row,"prezzo_ricambio",ld_prezzo_acquisto)
		
		
		postevent("ue_totale_riga")
			
	case "sconto_1", "sconto_2", "prezzo_ricambio"

		postevent("ue_totale_riga")
			
end choose
end event

event pcd_validaterow;call super::pcd_validaterow;
long ll_i


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_prodotto")) then
		g_mb.warning("OMNIA","Impostare il codice prodotto del ricambio!")
		PCCA.Error = c_Fatal
		return
	end if
	
	if getitemnumber(ll_i,"quan_utilizzo") <= 0 then
		g_mb.warning("OMNIA","Impostare la quantita di utilizzo del ricambio!")
		PCCA.Error = c_Fatal
		return
	end if
	
next
end event

