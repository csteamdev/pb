﻿$PBExportHeader$w_navigatore_manu.srw
$PBExportComments$Finestra Manutenzioni
forward
global type w_navigatore_manu from w_cs_xx_principale
end type
type ole_1 from olecustomcontrol within w_navigatore_manu
end type
type st_point from structure within w_navigatore_manu
end type
type st_minmaxinfo from structure within w_navigatore_manu
end type
end forward

type st_point from structure
	long		x
	long		y
end type

type st_minmaxinfo from structure
	st_point		reserved
	st_point		maxsize
	st_point		maxposition
	st_point		mintracksize
	st_point		maxtracksize
end type

global type w_navigatore_manu from w_cs_xx_principale
integer width = 3410
integer height = 1920
string title = "Navigatore Schede Manutenzioni"
event ue_resize pbm_getminmaxinfo
event ue_registrazioni ( )
event ue_attrezzatura ( )
event ue_manuale ( )
ole_1 ole_1
end type
global w_navigatore_manu w_navigatore_manu

type prototypes
subroutine GetMinMaxInfo (ref st_minmaxinfo d, long s, long l) library "kernel32.dll" alias for "RtlMoveMemory;ansi"

subroutine SetMinMaxInfo (long d, ref st_minmaxinfo s, long l) library "kernel32.dll" alias for "RtlMoveMemory;ansi"
end prototypes

type variables
string is_valore
end variables

event ue_resize;st_MinMaxInfo lst_MinMaxInfo
long ll_MinX = 600
long ll_MinY = 600


GetMinMaxInfo(lst_MinMaxInfo,MinMaxInfo,40)

if isvalid(lst_MinMaxInfo) then
	lst_MinMaxInfo.MinTrackSize.X = UnitsToPixels(ll_MinX,XUnitsToPixels!)
	lst_MinMaxInfo.MinTrackSize.Y = UnitsToPixels(ll_MinY,YUnitsToPixels!)
	SetMinMaxInfo(MinMaxInfo,lst_MinMaxInfo,40)
end if
end event

event ue_registrazioni();integer li_pos

string  ls_tipo, ls_valore

li_pos = pos(is_valore, "~~")

if li_pos > 0 then
	
	ls_tipo = mid(is_valore, li_pos + 1)
	ls_valore = mid(is_valore, 1, li_pos - 1)
	
	if ls_tipo = "area" then
		s_cs_xx.navigatore.fs_tipo_ricerca = "cod_area_aziendale"    
		s_cs_xx.navigatore.fs_valore = ls_valore		//w_manutenzioni.dw_filtro.setitem( 1, "flag_tipo_livello_1", "E")	
	elseif ls_tipo = "reparto" then
		s_cs_xx.navigatore.fs_tipo_ricerca = "cod_reparto" 
		s_cs_xx.navigatore.fs_valore = ls_valore		//w_manutenzioni.dw_filtro.setitem( 1, "flag_tipo_livello_1", "R")
	elseif ls_tipo = "divisione" then
		s_cs_xx.navigatore.fs_tipo_ricerca = "cod_divisione"
		s_cs_xx.navigatore.fs_valore = ls_valore
	else
		s_cs_xx.navigatore.fs_tipo_ricerca = "cod_attrezzatura" //w_manutenzioni.wf_carica_attrezzatura(ls_valore)
		s_cs_xx.navigatore.fs_valore = ls_valore
	end if
else
	s_cs_xx.navigatore.fs_tipo_ricerca = "cod_attrezzatura" //w_manutenzioni.wf_carica_attrezzatura(is_valore)
	s_cs_xx.navigatore.fs_valore = is_valore
end if


if not isvalid(w_manutenzioni) then
	window_open(w_manutenzioni,-1)
end if

end event

event ue_attrezzatura();integer	li_pos

string	ls_tipo


li_pos = pos(is_valore, "~~")

if li_pos > 0 then
	
	ls_tipo = mid(is_valore, li_pos + 1)
	
	is_valore = mid(is_valore, 1, li_pos - 1)
	
else
	
	ls_tipo = "scheda"
	
end if

choose case ls_tipo
		
	case "scheda"
		
		if not isvalid(w_attrezzature) then
			window_open(w_attrezzature,0)
		end if	
		// stefanop 01/04/2009
		// Codice della finestra attrezzatura originale, ora i pulsanti sono stati eliminati come la finestra di ricerca
		// w_attrezzature.cb_reset.triggerevent("clicked")		
		// w_attrezzature.dw_ricerca.setitem(1,"cod_attrezzatura",is_valore)
		// w_attrezzature.dw_ricerca.setitem(1,"cod_attrezzatura_a",is_valore)		
		// w_attrezzature.cb_ricerca.triggerevent("clicked")	
		// Codice per il nuovo albero.
		w_attrezzature.dw_filtro.triggerevent("ue_clear")
		w_attrezzature.dw_filtro.setitem(1,"cod_attrezzatura_da", is_valore)
		w_attrezzature.dw_filtro.setitem(1,"cod_attrezzatura_a", is_valore)
		w_attrezzature.cb_cerca.triggerevent("cliked")
		
	case "area"
		
		if not isvalid(w_aree_aziendali) then
			window_open(w_aree_aziendali,0)
		end if		
		w_aree_aziendali.cb_ricerca.triggerevent("clicked")				
		s_cs_xx.parametri.parametro_s_12 = is_valore
		w_aree_aziendali.postevent("ue_seleziona_riga")		
		
	case "reparto"
		
		if not isvalid(w_reparti) then
			window_open(w_reparti,0)
		end if
		
		s_cs_xx.parametri.parametro_s_12 = is_valore
		w_reparti.postevent("ue_seleziona_riga")		
		
	case "divisione"
		
		if not isvalid(w_divisioni) then
			window_open(w_divisioni,0)
		end if
		s_cs_xx.parametri.parametro_s_12 = is_valore
		w_divisioni.postevent("ue_seleziona_riga")
		
	case else
		g_mb.messagebox("OMNIA","Il collegamento selezionato non è corretto!")
		return		
end choose


end event

event ue_manuale();integer	li_pos

string	ls_tipo


li_pos = pos(is_valore, "~~")

if li_pos > 0 then
	
	ls_tipo = mid(is_valore, li_pos + 1)
	
	is_valore = mid(is_valore, 1, li_pos - 1)
	
else
	
	ls_tipo = "scheda"
	
end if

choose case ls_tipo
	case "scheda"
		s_cs_xx.parametri.parametro_s_10 = is_valore
		s_cs_xx.parametri.parametro_s_11 = "scheda"
		
	case "area"
		s_cs_xx.parametri.parametro_s_10 = is_valore
		s_cs_xx.parametri.parametro_s_11 = "area"
		
	case "reparto"
		s_cs_xx.parametri.parametro_s_10 = is_valore
		s_cs_xx.parametri.parametro_s_11 = "reparto"
		
	case "divisione"
		s_cs_xx.parametri.parametro_s_10 = is_valore
		s_cs_xx.parametri.parametro_s_11 = "divisione"		
		
	case else
		g_mb.messagebox("OMNIA","Il collegamento selezionato non corrisponde ad un'attrezzatura")
		return		
end choose

if not isvalid(w_det_attrezzature_nav) then
	window_open(w_det_attrezzature_nav,0)
end if		


end event

event show;call super::show;string ls_connectstring

long   ll_risposta

ll_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "navmanut", ls_connectstring)

if ll_risposta = -1 then
	g_mb.messagebox("Editor Navigatore","Errore in lettura stringa di connessione dal registro di sistema")
	return -1
end if

ole_1.object.avviaEditor(ls_connectstring,s_cs_xx.cod_azienda,true)
end event

on w_navigatore_manu.create
int iCurrent
call super::create
this.ole_1=create ole_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_1
end on

on w_navigatore_manu.destroy
call super::destroy
destroy(this.ole_1)
end on

event resize;// il flag "EXTEND ANCESTOR SCRIPT" è stato tolto di proposito

ole_1.resize(newwidth - 40,newheight - 40)

ole_1.move(20,20)
end event

type ole_1 from olecustomcontrol within w_navigatore_manu
event onscheda ( string codscheda )
event ue_carica_scheda ( )
event ue_manuale ( )
integer x = 27
integer y = 20
integer width = 1317
integer height = 768
integer taborder = 10
boolean border = false
boolean focusrectangle = false
string binarykey = "w_navigatore_manu.win"
integer textsize = -9
integer weight = 700
fontcharset fontcharset = easteuropecharset!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial CE"
long textcolor = 33554432
end type

event onscheda(string codscheda);integer li_pos

string  ls_tipo, ls_valore

is_valore = codscheda

if isnull(is_valore) or len(trim(is_valore)) < 1 then	
	g_mb.messagebox("Navigatore Manutenzioni","Nessuna scheda manutenzione selezionata")	
	return
end if

m_navigatore menu

menu = create m_navigatore

menu.iw_window = parent

menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())




end event

event ue_carica_scheda();//w_manutenzioni.wf_carica_attrezzatura(is_valore)

integer li_pos
string  ls_tipo, ls_valore


li_pos = pos(is_valore, "~~")
if li_pos > 0 then
	ls_tipo = mid(is_valore, li_pos + 1)
	ls_valore = mid(is_valore, 1, li_pos - 1)
	
	if ls_tipo = "area" then
		w_manutenzioni.dw_filtro.setitem( 1, "cod_area_aziendale", ls_valore)		
		w_manutenzioni.dw_filtro.setitem( 1, "flag_tipo_livello_1", "E")	
		w_manutenzioni.cb_cerca.PostEvent(Clicked!)	
	elseif ls_tipo = "reparto" then
		w_manutenzioni.dw_filtro.setitem( 1, "cod_reparto", ls_valore)
		w_manutenzioni.dw_filtro.setitem( 1, "flag_tipo_livello_1", "R")
		w_manutenzioni.cb_cerca.PostEvent(Clicked!)
	elseif ls_tipo = "divisione" then
		w_manutenzioni.dw_filtro.setitem( 1, "cod_divisione", ls_valore)
		w_manutenzioni.dw_filtro.setitem( 1, "flag_tipo_livello_1", "D")
		w_manutenzioni.cb_cerca.PostEvent(Clicked!)		
	else
		w_manutenzioni.wf_carica_attrezzatura(ls_valore)
	end if
else
	w_manutenzioni.wf_carica_attrezzatura(is_valore)
end if

end event

event ue_manuale();integer	li_pos

string	ls_tipo, ls_nomefile, ls_trash


li_pos = pos(is_valore, "~~")

if li_pos > 0 then
	
	ls_tipo = mid(is_valore, li_pos + 1)
	
	is_valore = mid(is_valore, 1, li_pos - 1)
	
else
	
	ls_tipo = "scheda"
	
end if

if ls_tipo <> "scheda" then
	g_mb.messagebox("OMNIA","Il collegamento selezionato non corrisponde ad un'attrezzatura")
	return
end if

//select file_manuale
//into 	 :ls_nomefile
//from	 anag_attrezzature
//where	 cod_azienda = :s_cs_xx.cod_azienda and
//	    cod_attrezzatura = :is_valore;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in lettura file del manuale: " + sqlca.sqlerrtext,stopsign!)
	return
end if

//if isnull(ls_nomefile) or len(trim(ls_nomefile)) < 1 then
	
	g_mb.messagebox("OMNIA","File del manuale non impostato!~nSelezionare manualmente.",exclamation!)
	
	ls_nomefile = ""
	
	ls_trash = ""
	
	getfileopenname("Selezione file del manuale",ls_nomefile,ls_trash,"*","Tutti i file (*.*),*.*")
	
	if isnull(ls_nomefile) or len(trim(ls_nomefile)) < 1 then
		g_mb.messagebox("OMNIA","Operazione interrotta!",stopsign!)
		return
	end if
	
	update
		anag_attrezzature
	set
		file_manuale = :ls_nomefile
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :is_valore;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in aggiornamento file del manuale: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
	
	commit;
	
//end if

inet l_inet
		
l_inet = create inet

l_inet.hyperlinktourl(ls_nomefile)

destroy l_inet
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Dw_navigatore_manu.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Dw_navigatore_manu.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
