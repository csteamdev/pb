﻿$PBExportHeader$w_navigatore_manutenzioni.srw
forward
global type w_navigatore_manutenzioni from w_cs_xx_principale
end type
type livello_zoom_t from statictext within w_navigatore_manutenzioni
end type
type slider_1 from dropdownlistbox within w_navigatore_manutenzioni
end type
type split_1 from uo_splitbar within w_navigatore_manutenzioni
end type
type tv_1 from treeview within w_navigatore_manutenzioni
end type
type dw_1 from uo_editor_manutenzioni within w_navigatore_manutenzioni
end type
type r_1 from rectangle within w_navigatore_manutenzioni
end type
end forward

global type w_navigatore_manutenzioni from w_cs_xx_principale
integer width = 3383
integer height = 2032
string title = "Navigatore Manutenzioni"
event we_menu_documenti ( )
event we_menu_registrazioni ( )
event we_menu_scheda ( )
event we_menu_apri ( )
livello_zoom_t livello_zoom_t
slider_1 slider_1
split_1 split_1
tv_1 tv_1
dw_1 dw_1
r_1 r_1
end type
global w_navigatore_manutenzioni w_navigatore_manutenzioni

type variables
private:
	string is_msg_name = "Navigatore Manutenzioni"
	uo_editor_manutenzioni_nodo istr_nodo
	boolean ib_load_map = false
	int ii_zoom_scale = 100
end variables

forward prototypes
public function integer wf_carica_mappe ()
public function integer wf_carica_nodi (long al_handle, integer ai_cod_mappa, integer ai_primo_nodo)
public function boolean we_carica_nodo (integer ai_cod_mappa, integer ai_cod_nodo)
end prototypes

event we_menu_documenti();s_cs_xx_parametri lstr_parametri

if isnull(istr_nodo) then
	return
end if

lstr_parametri.parametro_s_1 = istr_nodo.tipo_elemento
lstr_parametri.parametro_s_2 = istr_nodo.valore
lstr_parametri.parametro_s_3 = "doc"
setnull(istr_nodo)

openWithParm(w_attrezzature, lstr_parametri)
end event

event we_menu_registrazioni();s_cs_xx_parametri lstr_parametri

if isnull(istr_nodo) then
	return
end if

lstr_parametri.parametro_s_1 = istr_nodo.tipo_elemento
lstr_parametri.parametro_s_2 = istr_nodo.valore
setnull(istr_nodo)

openWithParm(w_manutenzioni, lstr_parametri)
end event

event we_menu_scheda();s_cs_xx_parametri lstr_parametri

if isnull(istr_nodo) then
	return
end if

lstr_parametri.parametro_s_1 = istr_nodo.tipo_elemento
lstr_parametri.parametro_s_2 = istr_nodo.valore
setnull(istr_nodo)

openWithParm(w_attrezzature, lstr_parametri)
end event

event we_menu_apri();if isnull(istr_nodo) then
	return
end if

we_carica_nodo(istr_nodo.cod_mappa, istr_nodo.cod_elemento)

setnull(istr_nodo)


end event

public function integer wf_carica_mappe ();string ls_sql
int		li_rows, li_row
datastore lds_store
str_editor_manutenzioni lstr_data
treeviewitem ltvi_item

ls_sql = "SELECT * FROM tab_mappe_manutenzioni WHERE cod_azienda='"+s_cs_xx.cod_azienda+"'"
if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.messagebox(is_msg_name, "Impossibile creare il datastore per controllare le mappe.~r~n"+ls_sql)
	return -1
end if

li_rows = lds_store.retrieve()
if li_rows > 0 then
	for li_row = 1 to li_rows
		
		ltvi_item.label = lds_store.getitemstring(li_row, "des_mappa")
		
		lstr_data.tipo_nodo = "M" // Mappa
		lstr_data.cod_mappa = lds_store.getitemnumber(li_row, "cod_mappa")
		lstr_data.primo_nodo = lds_store.getitemnumber(li_row, "primo_nodo")
		ltvi_item.data = lstr_data
		ltvi_item.children = true
		ltvi_item.selected = false
		ltvi_item.pictureindex = 1
		ltvi_item.selectedpictureindex = 1

		
		tv_1.insertitemlast(0, ltvi_item)
		
	next
end if
end function

public function integer wf_carica_nodi (long al_handle, integer ai_cod_mappa, integer ai_primo_nodo);string ls_sql
int		li_rows, li_row
datastore lds_store
treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

ls_sql = "SELECT cod_nodo, des_nodo FROM tab_nodi_manutenzioni WHERE cod_azienda='"+s_cs_xx.cod_azienda+"' AND cod_mappa="+string(ai_cod_mappa)
if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.messagebox(is_msg_name, "Impossibile creare datastore per controllare i nodi.~r~n"+ls_sql)
	return -1
end if

li_rows = lds_store.retrieve()
if li_rows > 0 then
	for li_row = 1 to li_rows
		
		lstr_data.tipo_nodo = "N"
		lstr_data.cod_mappa = ai_cod_mappa
		lstr_data.cod_nodo = lds_store.getitemnumber(li_row, "cod_nodo")
		if ai_primo_nodo = lstr_data.cod_nodo then
			lstr_data.primo_nodo = lstr_data.cod_nodo
		else
			lstr_data.primo_nodo = -1
		end if
		ltvi_item.data = lstr_data
		ltvi_item.label = lds_store.getitemstring(li_row, "des_nodo")
		
		if ai_primo_nodo = lstr_data.cod_nodo then
			ltvi_item.pictureindex = 3
			ltvi_item.selectedpictureindex = 3
		else
			ltvi_item.pictureindex = 2
			ltvi_item.selectedpictureindex = 2
		end if
		
		tv_1.insertitemlast(al_handle, ltvi_item)
		
	next
end if

return li_rows
end function

public function boolean we_carica_nodo (integer ai_cod_mappa, integer ai_cod_nodo);if dw_1.uof_carica_nodo(ai_cod_mappa, ai_cod_nodo) then
	dw_1.visible = true
	return true
else
	dw_1.visible = false
	return false
end if


end function

on w_navigatore_manutenzioni.create
int iCurrent
call super::create
this.livello_zoom_t=create livello_zoom_t
this.slider_1=create slider_1
this.split_1=create split_1
this.tv_1=create tv_1
this.dw_1=create dw_1
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.livello_zoom_t
this.Control[iCurrent+2]=this.slider_1
this.Control[iCurrent+3]=this.split_1
this.Control[iCurrent+4]=this.tv_1
this.Control[iCurrent+5]=this.dw_1
this.Control[iCurrent+6]=this.r_1
end on

on w_navigatore_manutenzioni.destroy
call super::destroy
destroy(this.livello_zoom_t)
destroy(this.slider_1)
destroy(this.split_1)
destroy(this.tv_1)
destroy(this.dw_1)
destroy(this.r_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.uof_set_editormode(false)
dw_1.visible = false
wf_carica_mappe()

ii_zoom_scale = 100
slider_1.text = string(ii_zoom_scale)
end event

event resize;//slider_1.move(20, 20)

livello_zoom_t.move(23, 40)

tv_1.move(20, slider_1.y + 110)
tv_1.height = newheight - (slider_1.y + 130)

//split_1.move(tv_1.x + tv_1.width, 20)
split_1.height = newheight - 40

r_1.move(split_1.x + split_1.width, 20)
dw_1.move(r_1.x + 4, r_1.y + 2)

r_1.resize(newwidth - (split_1.x + split_1.width + 20), split_1.height)

dw_1.uof_set_dimension(r_1.width - 6, r_1.height -6)
end event

type livello_zoom_t from statictext within w_navigatore_manutenzioni
integer x = 23
integer y = 40
integer width = 366
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Livello Zoom:"
boolean focusrectangle = false
end type

type slider_1 from dropdownlistbox within w_navigatore_manutenzioni
integer x = 434
integer y = 20
integer width = 320
integer height = 660
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean sorted = false
string item[] = {"25","50","100","125","150","175"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;ii_zoom_scale = integer(text)

tv_1.enabled = false
dw_1.uof_set_zoom( ii_zoom_scale )
tv_1.enabled = true
end event

type split_1 from uo_splitbar within w_navigatore_manutenzioni
integer x = 754
integer y = 20
integer width = 32
integer height = 1880
end type

event constructor;call super::constructor;split_1.uof_register(tv_1, LEFT)
split_1.uof_register(dw_1, RIGHT)
end event

event uoe_enddrag;call super::uoe_enddrag;r_1.x += ai_delta_x
r_1.width -= ai_delta_x
//dw_1.x = r_1.x + 4
end event

type tv_1 from treeview within w_navigatore_manutenzioni
integer x = 23
integer y = 140
integer width = 731
integer height = 1780
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean linesatroot = true
string picturename[] = {"Library5!","Tab!","Tab1!"}
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event itempopulate;str_editor_manutenzioni lstr_data
treeviewitem ltvi_item

// prevent invalid handle
if handle < 0 then return -1

tv_1.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

choose case lstr_data.tipo_nodo
	case "M" // mappa
		if wf_carica_nodi(handle, lstr_data.cod_mappa, lstr_data.primo_nodo) < 1 then
			ltvi_item.children = false
			tv_1.setitem(handle, ltvi_item)
		/*else
			dw_1.uof_carica_nodo(lstr_data.cod_mappa, lstr_data.primo_nodo)*/
		end if
		
end choose


end event

event doubleclicked;if handle < 1 then return -1

treeviewitem ltvi_item
str_editor_manutenzioni lstr_data

this.getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

if lstr_data.tipo_nodo = "N" then

	tv_1.enabled = false
	we_carica_nodo(lstr_data.cod_mappa, lstr_data.cod_nodo)
	if ii_zoom_scale <> 100 then
		dw_1.uof_set_zoom(ii_zoom_scale)
	end if
	tv_1.enabled = true
	
end if

end event

type dw_1 from uo_editor_manutenzioni within w_navigatore_manutenzioni
integer x = 800
integer y = 20
integer width = 2057
integer height = 1440
integer taborder = 10
end type

event rbuttondown;call super::rbuttondown;m_navigatore_manutenzioni lm_menu

if left(dwo.name, 2) = "t_" then
	if this.uof_get_nodo(integer(mid(dwo.name, 3)), istr_nodo) then
		
		lm_menu = create m_navigatore_manutenzioni
		
		if istr_nodo.tipo_elemento = "collegamento" then
			lm_menu.m_registrazioni.visible = false
			lm_menu.m_schedaattrezzatura.visible = false
			lm_menu.m_documenti.visible = false
			lm_menu.m_aprimappa.visible = true
		else
			if istr_nodo.tipo_elemento <> "scheda" then
				lm_menu.m_documenti.visible = false
			end if
		end if
		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		
	else
		setnull(istr_nodo)
		g_mb.messagebox(is_msg_name, "Impossibile trovare nodo")
	end if

end if
end event

event clicked;call super::clicked;m_navigatore_manutenzioni lm_menu

if ib_load_map then return

if left(dwo.name, 2) = "t_" then
	if this.uof_get_nodo(integer(mid(dwo.name, 3)), istr_nodo) then
		
		// Se è un collegamento apro direttamente la mappa associata	
		if istr_nodo.tipo_elemento = "collegamento" then
			ib_load_map = true
			we_carica_nodo(istr_nodo.cod_mappa, istr_nodo.cod_elemento)
			if ii_zoom_scale <> 100 then
				dw_1.uof_set_zoom(ii_zoom_scale)
			end if
			setnull(istr_nodo)
			ib_load_map = false
			return 
		end if
		
		// altrimenti apro il menu contestuale
		lm_menu = create m_navigatore_manutenzioni
		
		if not isnull(istr_nodo) then
			if isvalid(istr_nodo) and istr_nodo.tipo_elemento <> "scheda" then
				lm_menu.m_documenti.visible = false
			end if
		end if
		
		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	end if
end if
end event

type r_1 from rectangle within w_navigatore_manutenzioni
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 16777215
integer x = 800
integer y = 1620
integer width = 229
integer height = 200
end type

