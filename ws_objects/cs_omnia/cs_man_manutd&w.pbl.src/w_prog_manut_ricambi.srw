﻿$PBExportHeader$w_prog_manut_ricambi.srw
forward
global type w_prog_manut_ricambi from w_cs_xx_risposta
end type
type dw_prog_manut_ricambi from uo_cs_xx_dw within w_prog_manut_ricambi
end type
end forward

global type w_prog_manut_ricambi from w_cs_xx_risposta
integer width = 2990
integer height = 1028
string title = ""
dw_prog_manut_ricambi dw_prog_manut_ricambi
end type
global w_prog_manut_ricambi w_prog_manut_ricambi

type variables
long il_anno, il_num
end variables

event pc_setwindow;string ls_cod_kit, ls_ver_kit


il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_num = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

title = "Ricambi - Programma di Manutenzione: " + string(il_anno) + "/" + string(il_num)

dw_prog_manut_ricambi.set_dw_key("cod_azienda")

dw_prog_manut_ricambi.set_dw_key("anno_registrazione")

dw_prog_manut_ricambi.set_dw_key("num_registrazione")

dw_prog_manut_ricambi.set_dw_key("prog_riga_ricambi")

dw_prog_manut_ricambi.set_dw_options(sqlca,pcca.null_object,c_default,c_nohighlightselected)

dw_prog_manut_ricambi.change_dw_current()
end event

on w_prog_manut_ricambi.create
int iCurrent
call super::create
this.dw_prog_manut_ricambi=create dw_prog_manut_ricambi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prog_manut_ricambi
end on

on w_prog_manut_ricambi.destroy
call super::destroy
destroy(this.dw_prog_manut_ricambi)
end on

type dw_prog_manut_ricambi from uo_cs_xx_dw within w_prog_manut_ricambi
event ue_cerca_prod ( )
integer x = 23
integer y = 20
integer width = 2903
integer height = 880
integer taborder = 10
string dataobject = "d_prog_manut_ricambi"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event ue_cerca_prod();change_dw_current()
guo_ricerca.uof_ricerca_prodotto(dw_prog_manut_ricambi,"cod_prodotto")
end event

event itemchanged;call super::itemchanged;dec{4} ld_prezzo_acquisto, ld_null

string ls_des_prodotto, ls_null


choose case i_colname
	
	case "cod_prodotto"
		
		select des_prodotto,
				 prezzo_acquisto
		into   :ls_des_prodotto,
				 :ld_prezzo_acquisto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :i_coltext;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura descrizione prodotto da anag_prodotti: " + sqlca.sqlerrtext)
			return 1
		elseif sqlca.sqlcode = 100 then
			setnull(ls_null)
			setnull(ld_null)
			setitem(row,"cod_prodotto",ls_null)
			setitem(row,"des_prodotto",ls_null)
			setitem(row,"quan_utilizzo",ld_null)
			setitem(row,"prezzo_ricambio",ld_null)
			s_cs_xx.parametri.parametro_pos_ricerca = i_coltext + "*"
			s_cs_xx.parametri.parametro_tipo_ricerca = 1
			postevent("ue_cerca_prod")
		else
			setitem(row,"des_prodotto",ls_des_prodotto)
			setitem(getrow(),"quan_utilizzo",1)
			setitem(row,"prezzo_ricambio",ld_prezzo_acquisto)
		end if
		
end choose
end event

event updatestart;call super::updatestart;long ll_i


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_prodotto")) then
		g_mb.messagebox("OMNIA","Impostare il codice prodotto del ricambio!")
		return 1
	end if
	
	if getitemnumber(ll_i,"quan_utilizzo") <= 0 then
		g_mb.messagebox("OMNIA","Impostare la quantita di utilizzo del ricambio!")
		return 1
	end if
	
	if getitemnumber(ll_i,"prezzo_ricambio") <= 0 then
		g_mb.messagebox("OMNIA","Impostare il prezzo del ricambio!")
		return 1
	end if
	
next
end event

event pcd_new;call super::pcd_new;long ll_progressivo


select max(prog_riga_ricambio)
into   :ll_progressivo
from   prog_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno and
		 num_registrazione = :il_num;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo da prog_manutenzioni_ricambi: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
	ll_progressivo = 0
end if

ll_progressivo ++

setitem(getrow(),"prog_riga_ricambio",ll_progressivo)
end event

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,il_anno,il_num)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	setitem(ll_i,"anno_registrazione",il_anno)
	setitem(ll_i,"num_registrazione",il_num)
next
end event

event ue_key;call super::ue_key;if getcolumnname() = "cod_prodotto" then
	
	if key = keyF1!  and keyflags = 1 then
		postevent("ue_cerca_prod")
	end if
		
end if
end event

