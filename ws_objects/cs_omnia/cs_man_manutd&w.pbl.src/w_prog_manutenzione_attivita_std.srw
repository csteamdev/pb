﻿$PBExportHeader$w_prog_manutenzione_attivita_std.srw
$PBExportComments$Finestra Manutenzioni personalizzata Guerrato
forward
global type w_prog_manutenzione_attivita_std from w_cs_xx_risposta
end type
type cb_2 from commandbutton within w_prog_manutenzione_attivita_std
end type
type cb_salva from commandbutton within w_prog_manutenzione_attivita_std
end type
type dw_attivita from uo_cs_xx_dw within w_prog_manutenzione_attivita_std
end type
end forward

global type w_prog_manutenzione_attivita_std from w_cs_xx_risposta
integer width = 2619
integer height = 1104
string title = "Attività Manutentiva"
cb_2 cb_2
cb_salva cb_salva
dw_attivita dw_attivita
end type
global w_prog_manutenzione_attivita_std w_prog_manutenzione_attivita_std

type variables
boolean ib_nuovo
long il_rbuttonrow
datawindow idw_parent
end variables

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
else
	ib_nuovo = false
end if

save_on_close(c_socnosave)

dw_attivita.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1
end event

on w_prog_manutenzione_attivita_std.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_salva=create cb_salva
this.dw_attivita=create dw_attivita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_attivita
end on

on w_prog_manutenzione_attivita_std.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_salva)
destroy(this.dw_attivita)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_attivita, &
						"cod_tipo_manutenzione", &
						sqlca, &
						"tab_tipi_manut_scollegate", &
						"cod_tipo_manut_scollegata", &
						"des_tipo_manut_scollegata", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
											

end event

type cb_2 from commandbutton within w_prog_manutenzione_attivita_std
integer x = 1783
integer y = 880
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;

close(parent)
end event

type cb_salva from commandbutton within w_prog_manutenzione_attivita_std
integer x = 2171
integer y = 880
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_row,ll_anno_registrazione,ll_num_registrazione, ll_frequenza, ll_cont, ll_priorita
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione,ls_flag_periodicita,ls_note, &
       ls_des_tipo_manutenzione, ls_des_intervento, ls_flag_budget, ls_cod_tipo_manut_collegata, ls_des_tipo_manut_scollegata, &
		 ls_cod_stato, ls_cod_tipo_programma

dw_attivita.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
	if ib_nuovo then
		ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")		
	end if
end if

ls_cod_attrezzatura = dw_attivita.getitemstring(dw_attivita.getrow(), "cod_attrezzatura")
ls_cod_tipo_manutenzione  = dw_attivita.getitemstring(dw_attivita.getrow(), "cod_tipo_manutenzione")
ls_flag_periodicita  = dw_attivita.getitemstring(dw_attivita.getrow(), "flag_periodicita")
ll_frequenza = dw_attivita.getitemnumber(dw_attivita.getrow(), "frequenza")
ls_note = dw_attivita.getitemstring(dw_attivita.getrow(), "note")

if ll_frequenza < 1 or isnull(ll_frequenza) then
	g_mb.messagebox("OMNIA", "Impostare un valore nel campo PERIODICITA' / FREQUENZA")
	return
end if

if len(ls_flag_periodicita) < 1 or isnull(ls_flag_periodicita) then
	g_mb.messagebox("OMNIA", "Impostare un valore nel campo PERIODICITA' / FREQUENZA")
	return
end if

select cod_tipo_manut_collegata,
       des_tipo_manut_scollegata
into   :ls_cod_tipo_manut_collegata,
       :ls_des_tipo_manut_scollegata
from   tab_tipi_manut_scollegate
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_manut_scollegata = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la lettura del codice tipo manutenzione collegata: " + sqlca.sqlerrtext)
	return -1
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox( "OMNIA", "Attenzione: non esiste alcuna tipologia di manutenzione scollegata con il codice " + ls_cod_tipo_manutenzione + "!")
	return -1
end if

if isnull( ls_cod_tipo_manut_collegata) or ls_cod_tipo_manut_collegata = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: prima di salvare occorre configurare correttamente la tipologia scollegata " + ls_cod_tipo_manutenzione + "!")
	return -1
end if

select count(*)
into   :ll_cont
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manut_collegata;

if ll_cont < 1 then
	
	insert into tab_tipi_manutenzione
			 (cod_azienda,
			  cod_attrezzatura,
			  cod_tipo_manutenzione,
			  des_tipo_manutenzione,
			  flag_manutenzione,
			  periodicita,
			  frequenza)
	 values (:s_cs_xx.cod_azienda,
			  :ls_cod_attrezzatura,
			  :ls_cod_tipo_manut_collegata,
			  :ls_des_tipo_manut_scollegata,
			  'M',
			  :ls_flag_periodicita,
			  :ll_frequenza);
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione tipo manutenzione~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if


if ib_nuovo then 

	ll_anno_registrazione = f_anno_esercizio()
	
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
			 
	if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
		ll_num_registrazione = 1 
	else
		ll_num_registrazione ++
	end if
	
	INSERT INTO programmi_manutenzione  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  periodicita,   
			  frequenza,   
			  flag_scadenze,   
			  note_idl,   
			  flag_budget,   
			  tot_costo_intervento)  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ls_des_tipo_manut_scollegata,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manut_collegata,   
			  'M',   
			  :ls_flag_periodicita,   
			  :ll_frequenza,   
			  'S',   
			  :ls_note,
			  'S',
			  0)  ;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione attività~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

else
	
	update programmi_manutenzione  
	set    des_intervento = :ls_des_tipo_manut_scollegata,
	       cod_attrezzatura = :ls_cod_attrezzatura,
			 cod_tipo_manutenzione = :ls_cod_tipo_manut_collegata,
			 periodicita = :ls_flag_periodicita,
			 frequenza = :ll_frequenza,
			 note_idl = :ls_note
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione attività~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if
commit;

close(parent)
		  

end event

type dw_attivita from uo_cs_xx_dw within w_prog_manutenzione_attivita_std
integer x = 23
integer y = 20
integer width = 2514
integer height = 840
integer taborder = 10
string dataobject = "d_prog_manutenzione_grid_attivita_std"
end type

event pcd_new;call super::pcd_new;string ls_null, ls_cod_attrezzatura, ls_flag_periodicita,ls_flag_budget, ls_appo, ls_stringa, ls_cod_stato, ls_cod_tipo_programma
long   ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_frequenza, ll_priorita, ll_prova


setnull(ls_null)

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if ib_nuovo then
			
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ls_cod_attrezzatura = idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura")
			else
				// cerco la riga principale
				ls_cod_attrezzatura = idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura")
			end if
			
			setitem(getrow(),"cod_attrezzatura", idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura"))
			
			f_PO_LoadDDDW_DW( dw_attivita, &
									"cod_tipo_manutenzione", &
									sqlca, &
									"tab_tipi_manut_scollegate", &
									"cod_tipo_manut_scollegata", &
									"des_tipo_manut_scollegata", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")			
			
		else		
			
			// l'operazione di modifica può avvenire solo sulla riga principale.
			
//			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") <> "P" then
//				messagebox("OMNIA","L'operazione di modifica può avvenire solo sulla riga Principale")
//				rollback;
//				close(parent)
//			end if
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			
			select periodicita, 
					 frequenza
			into   :ls_flag_periodicita, 
					 :ll_frequenza
			from   programmi_manutenzione
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca piano manutenzione~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			ls_cod_attrezzatura = idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura")
			
			setitem(getrow(),"cod_attrezzatura", idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura"))
			setitem(getrow(),"flag_periodicita", ls_flag_periodicita)
			setitem(getrow(),"frequenza", ll_frequenza)
			setitem(getrow(),"note", idw_parent.getitemstring(il_rbuttonrow, "note_idl"))
	
			f_PO_LoadDDDW_DW( dw_attivita, &
									"cod_tipo_manutenzione", &
									sqlca, &
									"tab_tipi_manut_scollegate", &
									"cod_tipo_manut_collegata", &
									"des_tipo_manut_scollegata", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")			
									
			setitem(getrow(),"cod_tipo_manutenzione", idw_parent.getitemstring(il_rbuttonrow, "cod_tipo_manutenzione"))	
			
		end if
		
		accepttext()

	end if
end if
		
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_attivita,"cod_attrezzatura")
end choose
end event

