﻿$PBExportHeader$w_prog_manutenzione_operai_std.srw
$PBExportComments$Finestra per la programmazione attività standard
forward
global type w_prog_manutenzione_operai_std from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_prog_manutenzione_operai_std
end type
type cb_salva from commandbutton within w_prog_manutenzione_operai_std
end type
type dw_operai from uo_cs_xx_dw within w_prog_manutenzione_operai_std
end type
end forward

global type w_prog_manutenzione_operai_std from w_cs_xx_risposta
integer width = 2523
integer height = 704
string title = "Aggiunta Risorsa Interna"
cb_annulla cb_annulla
cb_salva cb_salva
dw_operai dw_operai
end type
global w_prog_manutenzione_operai_std w_prog_manutenzione_operai_std

type variables
boolean ib_nuovo=false
long il_rbuttonrow
datawindow idw_parent
string     is_cod_area_aziendale
end variables

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
else
	ib_nuovo = false
end if


save_on_close(c_socnosave)

dw_operai.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1

long   ll_anno_registrazione, ll_num_registrazione, ll_row
string ls_cod_attrezzatura

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
end if


select cod_attrezzatura
into   :ls_cod_attrezzatura
from   programmi_manutenzione  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione ;
		 
select cod_area_aziendale
into   :is_cod_area_aziendale
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura;
				 
end event

on w_prog_manutenzione_operai_std.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_salva=create cb_salva
this.dw_operai=create dw_operai
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_operai
end on

on w_prog_manutenzione_operai_std.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_salva)
destroy(this.dw_operai)
end on

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_operai,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome "+guo_functions.uof_concat_op()+" ', ' "+guo_functions.uof_concat_op()+" nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  

end event

type cb_annulla from commandbutton within w_prog_manutenzione_operai_std
integer x = 1701
integer y = 492
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;

close(parent)
end event

type cb_salva from commandbutton within w_prog_manutenzione_operai_std
integer x = 2094
integer y = 492
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_row,ll_anno_registrazione,ll_num_registrazione ,ll_progressivo,ll_ore,ll_minuti, ll_priorita
string ls_cod_operaio,ls_flag_prezzo_certo,ls_flag_capitalizzato, ls_cod_tipo_programma
dec{4} ld_operaio_costo_orario

dw_operai.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
//	// cerco la riga principale
//	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
//	if ib_nuovo then
//		ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
//		ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
//	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
//	end if
end if

ll_ore  = dw_operai.getitemnumber(dw_operai.getrow(), "ore")
ll_minuti  = dw_operai.getitemnumber(dw_operai.getrow(), "minuti")
ls_cod_operaio  = dw_operai.getitemstring(dw_operai.getrow(), "cod_operaio")
ld_operaio_costo_orario = dw_operai.getitemnumber(dw_operai.getrow(), "operaio_costo_orario")

if ib_nuovo then
	
	select max(progressivo)
	into   :ll_progressivo
	from   prog_manutenzioni_operai
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			 
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	INSERT INTO prog_manutenzioni_operai
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  progressivo,
			  cod_operaio,
			  ore,
			  minuti,
			  operaio_costo_orario)
	VALUES (:s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,
			  :ll_progressivo,
			  :ls_cod_operaio,
			  :ll_ore,
			  :ll_minuti,
			  :ld_operaio_costo_orario);
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione risorsa interna~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

else
	
	ll_progressivo  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")

	
	update prog_manutenzioni_operai
	set 	 cod_operaio = :ls_cod_operaio,
			 ore = :ll_ore,
			 minuti = :ll_minuti,
			 operaio_costo_orario = :ld_operaio_costo_orario
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 progressivo = :ll_progressivo;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in aggiornamento risorsa interna~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if

commit;

close(parent)
		  

end event

type dw_operai from uo_cs_xx_dw within w_prog_manutenzione_operai_std
integer x = 23
integer y = 20
integer width = 2446
integer height = 440
integer taborder = 10
string dataobject = "d_prog_manutenzione_grid_operai_std"
end type

event pcd_new;call super::pcd_new;string ls_null, ls_cod_operaio, ls_stringa, ls_flag_capitalizzato, ls_flag_prezzo_certo, ls_cod_tipo_programma

long ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, &
     ll_ore, ll_minuti, ll_priorita
dec{4} ld_costo_costo_orario

setnull(ls_null)

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if ib_nuovo then
			
			select stringa
			into   :ls_stringa
			from   parametri_azienda
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_parametro = 'COP';
			
			if sqlca.sqlcode = 0 then				
				setitem(getrow(),"cod_operaio", ls_stringa)				
			end if	
			
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			else
				// cerco la riga principale
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			end if
			
		else		
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			ll_progressivo    = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			

			SELECT cod_operaio,   
					 ore,   
					 minuti,   
					 operaio_costo_orario
		   INTO   :ls_cod_operaio,   
					 :ll_ore,   
					 :ll_minuti,   
					 :ld_costo_costo_orario
			from   prog_manutenzioni_operai
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 progressivo = :ll_progressivo;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca risorsa interna~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			setitem(getrow(),"cod_operaio", ls_cod_operaio)
			setitem(getrow(),"ore", ll_ore)
			setitem(getrow(),"minuti", ll_minuti)
			setitem(getrow(),"operaio_costo_orario", ld_costo_costo_orario)
	
		end if
		
		accepttext()

	end if
end if
		





end event

