﻿$PBExportHeader$w_prog_manutenzione_ricambio_std.srw
$PBExportComments$ricambio programmi manutenzione standard
forward
global type w_prog_manutenzione_ricambio_std from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_prog_manutenzione_ricambio_std
end type
type cb_salva from commandbutton within w_prog_manutenzione_ricambio_std
end type
type dw_ricambio from uo_cs_xx_dw within w_prog_manutenzione_ricambio_std
end type
end forward

global type w_prog_manutenzione_ricambio_std from w_cs_xx_risposta
integer width = 2414
integer height = 1308
string title = "Aggiunta Ricambio"
cb_annulla cb_annulla
cb_salva cb_salva
dw_ricambio dw_ricambio
end type
global w_prog_manutenzione_ricambio_std w_prog_manutenzione_ricambio_std

type variables
boolean ib_nuovo
long il_rbuttonrow
datawindow idw_parent
end variables

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
else
	ib_nuovo = false
end if

save_on_close(c_socnosave)

dw_ricambio.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1
end event

on w_prog_manutenzione_ricambio_std.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_salva=create cb_salva
this.dw_ricambio=create dw_ricambio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_ricambio
end on

on w_prog_manutenzione_ricambio_std.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_salva)
destroy(this.dw_ricambio)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricambio,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
		  
end event

type cb_annulla from commandbutton within w_prog_manutenzione_ricambio_std
integer x = 1595
integer y = 1092
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;

close(parent)
end event

type cb_salva from commandbutton within w_prog_manutenzione_ricambio_std
integer x = 1989
integer y = 1092
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_row,ll_anno_registrazione,ll_num_registrazione ,ll_progressivo, ll_priorita
string ls_cod_prodotto, ls_des_prodotto, ls_cod_fornitore, ls_cod_misura, ls_cod_porto, &
		 ls_flag_ricambio_codificato, ls_flag_prezzo_certo, ls_flag_capitalizzato, ls_flag_sconto_valore, ls_tkboll, ls_tkordi, ls_cod_tipo_programma

dec{4} ld_quan_utilizzo, ld_prezzo_ricambio, ld_sconto_1,ld_sconto_2,ld_sconto_3,ld_prezzo_netto,ld_sconto_1_eff,ld_sconto_2_eff,ld_sconto_3_eff,ld_prezzo_netto_eff, &
       ld_prezzo_ricambio_eff
datetime ldt_data_consegna

dw_ricambio.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	if ib_nuovo then
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")	
	end if
end if

ls_cod_prodotto = dw_ricambio.getitemstring(dw_ricambio.getrow(), "cod_prodotto")
ls_des_prodotto = dw_ricambio.getitemstring(dw_ricambio.getrow(), "des_prodotto")
ld_quan_utilizzo  = dw_ricambio.getitemnumber(dw_ricambio.getrow(), "quan_utilizzo")
ld_prezzo_ricambio = dw_ricambio.getitemnumber(dw_ricambio.getrow(), "costo_unitario")
ls_cod_fornitore = dw_ricambio.getitemstring(dw_ricambio.getrow(), "cod_fornitore")
ldt_data_consegna = dw_ricambio.getitemdatetime(dw_ricambio.getrow(), "data_consegna")
ls_cod_misura = dw_ricambio.getitemstring(dw_ricambio.getrow(), "cod_misura")
ld_sconto_1 = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_1_budget")
ld_sconto_2 = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_2_budget")
ld_sconto_3 = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_3_budget")
ld_prezzo_netto = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "prezzo_netto_budget")


ls_flag_ricambio_codificato = "S"
if isnull(ls_cod_prodotto) then ls_flag_ricambio_codificato = "N"

if ib_nuovo then
	select max(prog_riga_ricambio)
	into   :ll_progressivo
	from   prog_manutenzioni_ricambi
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	INSERT INTO prog_manutenzioni_ricambi
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ricambio,
			  cod_prodotto,
			  des_prodotto,
			  quan_utilizzo,
			  prezzo_ricambio,
			  cod_fornitore,
			  data_consegna,
			  cod_misura,
			  sconto_1_budget,
			  sconto_2_budget,
			  sconto_3_budget,
			  prezzo_netto_budget,
			  flag_prodotto_codificato)
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,
			  :ll_progressivo,
			  :ls_cod_prodotto,
			  :ls_des_prodotto,
			  :ld_quan_utilizzo,
			  :ld_prezzo_ricambio,
			  :ls_cod_fornitore,
			  :ldt_data_consegna,
			  :ls_cod_misura,
			  :ld_sconto_1,
			  :ld_sconto_2,
			  :ld_sconto_3,
			  :ld_prezzo_netto,
			  :ls_flag_ricambio_codificato);
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione ricambio~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

else
	
	ll_progressivo  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
	
	long ll_cont
	
	select count(cod_azienda)
	into   :ll_cont
	from   prog_manutenzioni_ricambi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ricambio = :ll_progressivo;
			
	if isnull(ll_cont) or ll_cont = 0 then
		g_mb.messagebox("OMNIA", "Riga non trovata:~r~n" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_progressivo))
		rollback;
		return		
	end if
	
	update prog_manutenzioni_ricambi
	set    cod_prodotto = :ls_cod_prodotto,
			 des_prodotto = :ls_des_prodotto,
			 quan_utilizzo = :ld_quan_utilizzo,
			 cod_fornitore = :ls_cod_fornitore,
			 data_consegna = :ldt_data_consegna,
			 cod_misura = :ls_cod_misura,
			 prezzo_ricambio = :ld_prezzo_ricambio,
			 sconto_1_budget = :ld_sconto_1,
			 sconto_2_budget = :ld_sconto_2,
			 sconto_3_budget = :ld_sconto_3,
			 prezzo_netto_budget = :ld_prezzo_netto,
			 flag_prodotto_codificato = :ls_flag_ricambio_codificato
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ricambio = :ll_progressivo;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in aggiornamento ricambio~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if		
	
end if

commit;

close(parent)
		  

end event

type dw_ricambio from uo_cs_xx_dw within w_prog_manutenzione_ricambio_std
integer x = 23
integer y = 20
integer width = 2331
integer height = 1040
integer taborder = 10
string dataobject = "d_prog_manutenzione_grid_ricambio_std"
end type

event itemchanged;call super::itemchanged;string ls_des_prodotto, ls_cod_misura, ls_null, ls_flag_blocco, ls_flag_sconto_valore
dec{4} ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_prezzo_unitario

setnull(ls_null)

if i_extendmode then
	choose case i_colname
		case "costo_unitario"
			
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			
			ld_prezzo_unitario = dec(i_coltext)
			if ld_sconto_1 > 0 then
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
			end if
			if ld_sconto_2 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
			end if
			if ld_sconto_3 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)			
			
		case "cod_prodotto"
			
			if isnull(i_coltext) then
				setitem(getrow(),"cod_misura", ls_null)
				setitem(getrow(),"des_prodotto", ls_null)
			else
				select des_prodotto, 
						 cod_misura_acq
				into   :ls_des_prodotto,
						 :ls_cod_misura
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :i_coltext;
				if sqlca.sqlcode = 0 then
					setitem(getrow(),"cod_misura", ls_cod_misura)
					setitem(getrow(),"des_prodotto", ls_des_prodotto)
				else
					return 2
				end if
			end if
			
		case "cod_fornitore"
			
			select flag_blocco 
			into   :ls_flag_blocco
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_fornitore = :i_coltext;
			
			if sqlca.sqlcode = 0 then
				if not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
					g_mb.messagebox( "OMNIA", "Attenzione: il fornitore scelto risulta bloccato!")
				end if
			end if
			
		case "sconto_1_budget"
		
			ld_sconto_1 = dec(data)
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			if ld_sconto_1 > 0 then
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
			end if
			if ld_sconto_2 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
			end if
			if ld_sconto_3 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)
		
		case "sconto_2_budget" 
			
			ld_sconto_2 = dec(data)
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			if ld_sconto_1 > 0 then
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
			end if
			if ld_sconto_2 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
			end if
			if ld_sconto_3 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)
			
		case "sconto_3_budget"
			
			ld_sconto_3 = dec(data)
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			if ld_sconto_1 > 0 then
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
			end if
			if ld_sconto_2 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
			end if
			if ld_sconto_3 > 0 then				
				ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)	
									
		end choose
end if
end event

event pcd_new;call super::pcd_new;string ls_null, ls_cod_porto, ls_flag_capitalizzato, ls_flag_prezzo_certo, ls_cod_prodotto, ls_cod_tipo_programma, &
       ls_des_prodotto, flag_prodotto_codificato, ls_cod_fornitore, ls_cod_misura, ls_flag_sconto_valore
long ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, &
     ll_ore, ll_minuti, ll_priorita
dec{4} ld_quan_utilizzo, ld_prezzo_ricambio, ld_sconto_1_budget, ld_sconto_2_budget, ld_sconto_3_budget,ld_sconto_1_eff, ld_sconto_2_eff, ld_sconto_3_eff, &
		 ld_prezzo_netto_budget, ld_prezzo_netto_eff, ld_prezzo_ricambio_eff
datetime ldt_data_consegna

setnull(ls_null)

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if ib_nuovo then
			
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			else
				// cerco la riga principale
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			end if
			
		else		
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			ll_progressivo    = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			

			SELECT cod_prodotto,   
					 des_prodotto,   
					 quan_utilizzo,   
					 prezzo_ricambio,   
					 flag_prodotto_codificato,   
					 cod_fornitore,   
					 cod_misura,   
					 data_consegna,   
					 sconto_1_budget,
					 sconto_2_budget,
					 sconto_3_budget,
					 prezzo_netto_budget
			INTO   :ls_cod_prodotto,   
					 :ls_des_prodotto,   
					 :ld_quan_utilizzo,   
					 :ld_prezzo_ricambio,   
					 :flag_prodotto_codificato,   
					 :ls_cod_fornitore,   
					 :ls_cod_misura,   
					 :ldt_data_consegna,   
					 :ld_sconto_1_budget,
					 :ld_sconto_2_budget,
					 :ld_sconto_3_budget,
					 :ld_prezzo_netto_budget
			FROM   prog_manutenzioni_ricambi
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ricambio = :ll_progressivo;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca risorsa interna~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			if isnull(ld_sconto_1_budget) then ld_sconto_1_budget = 0
			if isnull(ld_sconto_2_budget) then ld_sconto_2_budget = 0
			if isnull(ld_sconto_3_budget) then ld_sconto_3_budget = 0

			setitem(getrow(),"cod_prodotto", ls_cod_prodotto)
			setitem(getrow(),"des_prodotto", ls_des_prodotto)
			setitem(getrow(),"quan_utilizzo", ld_quan_utilizzo)
			setitem(getrow(),"costo_unitario", ld_prezzo_ricambio)
			setitem(getrow(),"cod_fornitore", ls_cod_fornitore)
			setitem(getrow(),"cod_misura", ls_cod_misura)
			setitem(getrow(),"data_consegna", ldt_data_consegna)
			setitem(getrow(),"sconto_1_budget", ld_sconto_1_budget)
			setitem(getrow(),"sconto_2_budget", ld_sconto_2_budget)
			setitem(getrow(),"sconto_3_budget", ld_sconto_3_budget)
			setitem(getrow(),"prezzo_netto_budget", ld_prezzo_netto_budget)
			
		end if
		
		accepttext()

	end if
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricambio,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricambio,"cod_prodotto")
end choose
end event

