﻿$PBExportHeader$w_prog_manutenzione_risorse_std.srw
$PBExportComments$Finestra programmazione attività standard
forward
global type w_prog_manutenzione_risorse_std from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_prog_manutenzione_risorse_std
end type
type cb_salva from commandbutton within w_prog_manutenzione_risorse_std
end type
type dw_risorse from uo_cs_xx_dw within w_prog_manutenzione_risorse_std
end type
end forward

global type w_prog_manutenzione_risorse_std from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 1920
integer height = 668
string title = "Aggiunta Fornitore Risorsa Esterna"
cb_annulla cb_annulla
cb_salva cb_salva
dw_risorse dw_risorse
end type
global w_prog_manutenzione_risorse_std w_prog_manutenzione_risorse_std

type variables
boolean ib_nuovo=false
long il_rbuttonrow
datawindow idw_parent
string is_cod_area_aziendale
end variables

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
else
	ib_nuovo = false
end if

save_on_close(c_socnosave)

dw_risorse.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1


long   ll_anno_registrazione, ll_num_registrazione, ll_row
string ls_cod_attrezzatura

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
end if


select cod_attrezzatura
into   :ls_cod_attrezzatura
from   programmi_manutenzione  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione ;
		 
select cod_area_aziendale
into   :is_cod_area_aziendale
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura;
				 
end event

on w_prog_manutenzione_risorse_std.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_salva=create cb_salva
this.dw_risorse=create dw_risorse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_risorse
end on

on w_prog_manutenzione_risorse_std.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_salva)
destroy(this.dw_risorse)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_risorse,"cod_cat_risorse_esterne",sqlca,&
					  "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	

f_PO_LoadDDDW_DW(dw_risorse,"cod_risorsa_esterna",sqlca,&
					  "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
end event

type cb_annulla from commandbutton within w_prog_manutenzione_risorse_std
integer x = 1093
integer y = 452
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;

close(parent)
end event

type cb_salva from commandbutton within w_prog_manutenzione_risorse_std
integer x = 1486
integer y = 452
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_row,ll_anno_registrazione,ll_num_registrazione ,ll_progressivo,ll_ore,ll_minuti, ll_priorita, ll_ore_eff, ll_minuti_eff
string ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna,ls_flag_prezzo_certo, ls_flag_eseguita, &
       ls_flag_capitalizzato, ls_cod_fornitore, ls_cod_tipo_programma
		 
dec{4} ld_risorsa_costo_orario,ld_risorsa_costi_aggiuntivi
datetime ldt_data_consegna
dw_risorse.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
//	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
//	if ib_nuovo then
//		ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
//		ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
//	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
//	end if
end if

ls_cod_cat_risorse_esterne = dw_risorse.getitemstring(dw_risorse.getrow(), "cod_cat_risorse_esterne")
ls_cod_risorsa_esterna = dw_risorse.getitemstring(dw_risorse.getrow(), "cod_risorsa_esterna")
ll_ore  = dw_risorse.getitemnumber(dw_risorse.getrow(), "ore")
ll_minuti  = dw_risorse.getitemnumber(dw_risorse.getrow(), "minuti")
ld_risorsa_costo_orario = dw_risorse.getitemnumber(dw_risorse.getrow(), "risorsa_costo_orario")
ld_risorsa_costi_aggiuntivi = dw_risorse.getitemnumber(dw_risorse.getrow(), "risorsa_costi_aggiuntivi")

if isnull(ld_risorsa_costi_aggiuntivi) then ld_risorsa_costi_aggiuntivi = 0
if isnull(ld_risorsa_costo_orario) then ld_risorsa_costo_orario = 0
if isnull(ll_minuti) then ll_minuti = 0
if isnull(ll_ore) then ll_ore = 0

if ib_nuovo then

	select max(progressivo)
	into   :ll_progressivo
	from   prog_manutenzioni_risorse_est
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			 
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	INSERT INTO prog_manutenzioni_risorse_est
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  progressivo,
			  cod_cat_risorse_esterne,
			  cod_risorsa_esterna,
			  ore,
			  minuti,
			  risorsa_costo_orario,
			  risorsa_costi_aggiuntivi)
	VALUES (:s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,
			  :ll_progressivo,
			  :ls_cod_cat_risorse_esterne,
			  :ls_cod_risorsa_esterna,
			  :ll_ore,
			  :ll_minuti,
			  :ld_risorsa_costo_orario,
			  :ld_risorsa_costi_aggiuntivi);
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione ricambio~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
else
	
	ll_progressivo  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")

	update prog_manutenzioni_risorse_est
	set    cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne,
		    cod_risorsa_esterna = :ls_cod_risorsa_esterna,
		    ore = :ll_ore,
		    minuti = :ll_minuti,
		    risorsa_costo_orario = :ld_risorsa_costo_orario,
		    risorsa_costi_aggiuntivi = :ld_risorsa_costi_aggiuntivi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 progressivo = :ll_progressivo;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in modifica ricambio~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if
commit;

close(parent)
		  

end event

type dw_risorse from uo_cs_xx_dw within w_prog_manutenzione_risorse_std
integer x = 23
integer y = 20
integer width = 1829
integer height = 400
integer taborder = 10
string dataobject = "d_prog_manutenzione_grid_risorse_std"
end type

event itemchanged;call super::itemchanged;string ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_null,ls_cod_fornitore, ls_flag_blocco
dec{4} ld_costo_orario, ld_costo_agg
setnull(ls_null)

if i_extendmode then
	
	choose case i_colname
			
		case "cod_cat_risorse_esterne"
			
			f_PO_LoadDDDW_DW(dw_risorse,"cod_risorsa_esterna",sqlca,&
								  "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + data + "' ")			
								  
		case "cod_risorsa_esterna"
			
			ls_cod_cat_risorse_esterne = getitemstring(getrow(),"cod_cat_risorse_esterne")
			
			SELECT tariffa_std,   
					 diritto_chiamata  
			INTO   :ld_costo_orario,   
					 :ld_costo_agg  
			FROM   anag_risorse_esterne  
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
					 cod_risorsa_esterna = :data;
					 
			setitem( getrow(), "risorsa_costo_orario", ld_costo_orario)
			setitem( getrow(), "risorsa_costi_aggiuntivi", ld_costo_agg)
			
end choose
end if
end event

event pcd_new;call super::pcd_new;string ls_null, ls_cod_risorsa_esterna, ls_stringa, ls_flag_capitalizzato, ls_flag_prezzo_certo, &
       ls_cod_fornitore,ls_cod_cat_risorse_esterne, ls_flag_eseguita, ls_cod_tipo_programma
long ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, &
     ll_ore, ll_minuti, ll_priorita, ll_ore_eff, ll_minuti_eff
dec{4} ld_risorsa_costo_orario, ld_risorsa_costi_aggiuntivi

setnull(ls_null)

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if not ib_nuovo then						
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			ll_progressivo    = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			

			SELECT cod_cat_risorse_esterne,   
					 cod_risorsa_esterna,   
					 ore,   
					 minuti,
					 risorsa_costo_orario,   
					 risorsa_costi_aggiuntivi
			INTO   :ls_cod_cat_risorse_esterne,   
					 :ls_cod_risorsa_esterna,   
					 :ll_ore,   
					 :ll_minuti,
					 :ld_risorsa_costo_orario,   
					 :ld_risorsa_costi_aggiuntivi
			from   prog_manutenzioni_risorse_est
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 progressivo = :ll_progressivo;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca risorsa interna~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			setitem(getrow(),"cod_cat_risorse_esterne", ls_cod_cat_risorse_esterne)
			setitem(getrow(),"cod_risorsa_esterna", ls_cod_risorsa_esterna)
			setitem(getrow(),"ore", ll_ore)
			setitem(getrow(),"minuti", ll_minuti)
			setitem(getrow(),"risorsa_costo_orario", ld_risorsa_costo_orario)
			setitem(getrow(),"risorsa_costi_aggiuntivi", ld_risorsa_costi_aggiuntivi)
		
		else
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			else
				// cerco la riga principale
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			end if
	
		end if
		
		accepttext()

	end if
end if
		





end event

