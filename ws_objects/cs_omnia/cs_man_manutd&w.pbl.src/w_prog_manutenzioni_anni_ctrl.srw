﻿$PBExportHeader$w_prog_manutenzioni_anni_ctrl.srw
forward
global type w_prog_manutenzioni_anni_ctrl from window
end type
type cb_annulla from commandbutton within w_prog_manutenzioni_anni_ctrl
end type
type cb_ok from commandbutton within w_prog_manutenzioni_anni_ctrl
end type
type ddlb_anni from dropdownlistbox within w_prog_manutenzioni_anni_ctrl
end type
type st_1 from statictext within w_prog_manutenzioni_anni_ctrl
end type
type dw_dati from datawindow within w_prog_manutenzioni_anni_ctrl
end type
end forward

global type w_prog_manutenzioni_anni_ctrl from window
integer width = 2304
integer height = 624
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
cb_ok cb_ok
ddlb_anni ddlb_anni
st_1 st_1
dw_dati dw_dati
end type
global w_prog_manutenzioni_anni_ctrl w_prog_manutenzioni_anni_ctrl

type variables
long	il_indice
end variables

forward prototypes
public function string wf_controllo_anno (string fs_flag_budget, ref boolean fb_blocco)
end prototypes

public function string wf_controllo_anno (string fs_flag_budget, ref boolean fb_blocco);string	ls_flag_chiuso
long	ll_anno

ll_anno = long(ddlb_anni.text(il_indice))

if isnull(fs_flag_budget) then fs_flag_budget = "N"

fb_blocco = false

select flag_chiuso
into	:ls_flag_chiuso
from	prog_manutenzioni_anni
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_riferimento = :ll_anno;
		
if sqlca.sqlcode = 0 and not isnull(ls_flag_chiuso) and ls_flag_chiuso = 'S' then
	fb_blocco = true
	return 'N'
else
	return fs_flag_budget
end if
end function

event open;long	ll_i, ll_anno, ll_chiusi
string	ls_flag_chiuso, ls_sql, ls_flag_budget
boolean lb_blocco

/*
 s_cs_xx.parametri.parametro_s_1 = flag budget
 s_cs_xx.parametri.parametro_d_1_a = array con anni di riferimento
 
*/

dw_dati.settransobject(sqlca)
dw_dati.insertrow(0)

ll_chiusi = 0

for ll_i = 1 to upperbound(s_cs_xx.parametri.parametro_d_1_a)

	if ll_i = 1 then
		ll_anno = s_cs_xx.parametri.parametro_d_1_a[ll_i]
	end if
	ddlb_anni.insertitem( string(s_cs_xx.parametri.parametro_d_1_a[ll_i]), ll_i)
	
	select flag_chiuso
	into	:ls_flag_chiuso
	from	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_riferimento = :s_cs_xx.parametri.parametro_d_1_a[ll_i];
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_chiuso) and ls_flag_chiuso = 'S' then
		ll_chiusi ++
	end if
	
next

il_indice = 1

ddlb_anni.selectitem(1)

dw_dati.setitem( dw_dati.getrow(), "flag_budget", s_cs_xx.parametri.parametro_s_1)

dw_dati.accepttext()

dw_dati.postevent( "ue_controllo_dati")
end event

on w_prog_manutenzioni_anni_ctrl.create
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.ddlb_anni=create ddlb_anni
this.st_1=create st_1
this.dw_dati=create dw_dati
this.Control[]={this.cb_annulla,&
this.cb_ok,&
this.ddlb_anni,&
this.st_1,&
this.dw_dati}
end on

on w_prog_manutenzioni_anni_ctrl.destroy
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.ddlb_anni)
destroy(this.st_1)
destroy(this.dw_dati)
end on

type cb_annulla from commandbutton within w_prog_manutenzioni_anni_ctrl
integer x = 46
integer y = 440
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_ul_2 = 0
close(parent)
end event

type cb_ok from commandbutton within w_prog_manutenzioni_anni_ctrl
integer x = 1920
integer y = 440
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;long	 ll_anno

dw_dati.accepttext()

s_cs_xx.parametri.parametro_s_2 = ""

ll_anno = long(ddlb_anni.text(il_indice))

if isnull(ll_anno) or ll_anno = 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: selezionare un anno valido!", stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_ul_2 = ll_anno
s_cs_xx.parametri.parametro_s_1 = dw_dati.getitemstring( dw_dati.getrow(), "flag_budget")
s_cs_xx.parametri.parametro_s_2 = dw_dati.getitemstring( dw_dati.getrow(), "cod_tipo_manutenzione")
s_cs_xx.parametri.parametro_data_1 = dw_dati.getitemdatetime( dw_dati.getrow(), "data_consegna")

if isnull(s_cs_xx.parametri.parametro_s_2) or s_cs_xx.parametri.parametro_s_2 = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: selezionare una tipologia di manutenzione valida!", stopsign!)
	return -1
end if
close(parent)
end event

type ddlb_anni from dropdownlistbox within w_prog_manutenzioni_anni_ctrl
integer x = 745
integer y = 40
integer width = 503
integer height = 340
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;il_indice = index

dw_dati.accepttext()
dw_dati.postevent("ue_controllo_dati")
end event

type st_1 from statictext within w_prog_manutenzioni_anni_ctrl
integer x = 160
integer y = 60
integer width = 571
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Anno di riferimento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_dati from datawindow within w_prog_manutenzioni_anni_ctrl
event ue_controllo_dati ( )
integer x = 46
integer y = 20
integer width = 2217
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "d_prog_manutenzioni_anni_ctrl"
boolean livescroll = true
end type

event ue_controllo_dati();string ls_flag_corrente, ls_sql
boolean lb_blocco
long		ll_anno

ls_flag_corrente = dw_dati.getitemstring( dw_dati.getrow(), "flag_budget")

ll_anno = long(ddlb_anni.text(il_indice))

if isnull(ls_flag_corrente) then ls_flag_corrente = "N"

ls_flag_corrente = wf_controllo_anno(ls_flag_corrente, lb_blocco)

dw_dati.setitem( dw_dati.getrow(), "flag_budget", ls_flag_corrente)

if lb_blocco then
	dw_dati.object.flag_budget.protect = 1
else
	dw_dati.object.flag_budget.protect = 0
end if

if ls_flag_corrente = "S" then
	ls_sql = " and flag_stampa = 'N' "
else
	ls_sql = " and flag_stampa = 'S' "
end if

f_PO_LoadDDDW_DW(dw_dati,"cod_tipo_manutenzione",sqlca,&
					  "tab_tipi_manut_scollegate","cod_tipo_manut_scollegata","des_tipo_manut_scollegata",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manut_scollegata IN ( select cod_tipo_manut_scollegata from prog_manutenzioni_anni_tm where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_riferimento = " + string(ll_anno) + "  and flag_blocco = 'N') " + ls_sql)
end event

event itemchanged;choose case dwo.name
		
	case "flag_budget"
		
		postevent("ue_controllo_dati")
		
end choose
end event

