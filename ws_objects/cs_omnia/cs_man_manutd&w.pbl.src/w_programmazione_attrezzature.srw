﻿$PBExportHeader$w_programmazione_attrezzature.srw
$PBExportComments$Finestra caricamento programmazione manutenzione
forward
global type w_programmazione_attrezzature from w_cs_xx_principale
end type
type dw_sel_elenco_attrez from uo_cs_xx_dw within w_programmazione_attrezzature
end type
type dw_risultati from datawindow within w_programmazione_attrezzature
end type
type dw_tipi_man_attr_selezionate from datawindow within w_programmazione_attrezzature
end type
type dw_piani_esistenti from datawindow within w_programmazione_attrezzature
end type
type dw_tipi_man_attrezzature from datawindow within w_programmazione_attrezzature
end type
type dw_folder from u_folder within w_programmazione_attrezzature
end type
type dw_ext_prog_manut from uo_cs_xx_dw within w_programmazione_attrezzature
end type
end forward

global type w_programmazione_attrezzature from w_cs_xx_principale
integer width = 3886
integer height = 2104
string title = "Creazione Manutenzioni Programmate"
event ue_resize ( )
event ue_imposta_filtro_attrezzature ( string as_cod_attrezzatura )
dw_sel_elenco_attrez dw_sel_elenco_attrez
dw_risultati dw_risultati
dw_tipi_man_attr_selezionate dw_tipi_man_attr_selezionate
dw_piani_esistenti dw_piani_esistenti
dw_tipi_man_attrezzature dw_tipi_man_attrezzature
dw_folder dw_folder
dw_ext_prog_manut dw_ext_prog_manut
end type
global w_programmazione_attrezzature w_programmazione_attrezzature

type variables
string    is_flag_gestione_manut, is_sql_origine=""
boolean ib_global_service = false

long il_prog_programmazione
datetime idt_data_programmazione, idt_ora_programmazione
string is_flag_abr = "N"
end variables

forward prototypes
public function integer wf_programmazione_semplice ()
public function integer wf_programmazione_periodo ()
public function integer wf_carica ()
public subroutine wf_impostazioni ()
public function integer wf_escludi_tipologie (ref string fs_tipologie[])
public function integer wf_get_stato_tipologie (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, ref string fs_errore)
public subroutine wf_conferma ()
public function integer wf_get_stato_attrezzatura (string fs_cod_attrezzatura, string fs_where, ref string fs_errore)
public function integer wf_retrieve_piani_esistenti (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, ref string fs_errore)
public subroutine wf_visualizza_manutenzione (long fl_anno_man, long fl_num_man)
public function integer wf_calcola_scadenze (integer fi_giorno_inizio, integer fi_mese_inizio, integer fi_giorno_fine, integer fi_mese_fine, datetime fdt_da_data, datetime fdt_a_data, string fs_periodicita, long fl_frequenza, long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_costo_orario_e_operaio (string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, ref string fs_cod_operaio, ref decimal fd_costo_orario_operaio)
public function integer wf_risorsa_esterna (string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, ref string fs_cod_cat_risorse_esterne, ref string fs_cod_risorsa_esterna)
public function integer wf_programmazione_semplice_gb ()
public function integer wf_programmazione_periodo_gb ()
public subroutine wf_attrezzature_ricerca_da ()
public subroutine wf_attrezzature_ricerca_a ()
public subroutine wf_annulla_filtro ()
public subroutine wf_ricerca_filtro ()
public subroutine wf_prosegui ()
public subroutine wf_riseleziona ()
public function integer wf_inserisci_manut_temporanea ()
public function integer wf_inserisci_temporanea (long fl_prog_padre, long fl_anno_programma, long fl_num_programma, string fs_flag_tipo, long fl_anno_manut, long fl_num_manut, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione)
public function integer wf_risultati_programmazione (long fl_prog_programmazione, ref string fs_errore)
public function integer wf_cancella_manutenzioni (string ls_cod_attrezzatura, string ls_cod_tipo_manutenzione, ref string fs_errore)
public function integer wf_annulla_programmazione (ref string fs_errore)
public function integer wf_controlla_attrezzatura (datastore ads_data, string as_cod_attrezzatura, ref string as_errore)
public function string wf_where_attrezzature ()
end prototypes

event ue_resize();long ll_offset

ll_offset = 350

//sistemo il folder
dw_folder.width = this.width - 15
dw_folder.height = this.height - 15

//folder 2 (risultato post ricerca)
dw_tipi_man_attrezzature.height = this.height / 2 - 200   //(this.height / 100) * 60 - ll_offset
dw_piani_esistenti.y = dw_tipi_man_attrezzature.y + dw_tipi_man_attrezzature.height + 50
dw_piani_esistenti.height = dw_tipi_man_attrezzature.height   //(this.height / 100) * 40 - ll_offset

//folder 3 (seleziona tipo programmazione)
dw_tipi_man_attr_selezionate.y = dw_ext_prog_manut.y + dw_ext_prog_manut.height + 50
dw_tipi_man_attr_selezionate.height = this.height - (dw_ext_prog_manut.height + ll_offset)

//folder 4 (risultati post programmazione)
dw_risultati.x=dw_sel_elenco_attrez.x
dw_risultati.y = dw_sel_elenco_attrez.y
dw_risultati.width = dw_sel_elenco_attrez.width
dw_risultati.height = dw_tipi_man_attrezzature.width + dw_piani_esistenti.width

end event

event ue_imposta_filtro_attrezzature(string as_cod_attrezzatura);

if as_cod_attrezzatura<>"" and not isnull(as_cod_attrezzatura) then
	
	wf_annulla_filtro()
	
	dw_sel_elenco_attrez.setitem(1,"cod_attr_da", as_cod_attrezzatura)
	dw_sel_elenco_attrez.setitem(1,"cod_attr_a", as_cod_attrezzatura)
	
	dw_sel_elenco_attrez.setitem(1,"flag_prog", "S")
	
	wf_ricerca_filtro()
end if

return
end event

public function integer wf_programmazione_semplice ();long     ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, &
	      ll_anno_registrazione, ll_num_registrazione, ll_ore, ll_minuti, ll_prog_tipo_manutenzione, ll_num_protocollo
			
string   ls_flag_gest_manutenzione, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
	      ls_flag_ricambio_codificato, ls_cod_ricambio, ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, &
		   ls_cod_primario, ls_cod_misura, ls_flag_manutenzione, ls_messaggio, ls_note_idl, ls_descrizione, ls_des_blob, &
			ls_path_documento, ls_data, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
			ls_cod_risorsa_esterna, ls_flag_scadenze, ls_pubblica_intranet, ls_selezionato
			
dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean  lb_inserimento

datetime ldt_tempo_previsto, ldt_data, ldt_data_1, ldt_data_creazione_programma
blob lbb_blob
uo_manutenzioni luo_manutenzioni
long     ll_risp, ll_giorni, ll_mesi, ll_anni, ll_null
string   ls_tipo_data
dec{0}	ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

setnull(ll_null)

ls_data = string(dw_ext_prog_manut.getitemdatetime(1, "data_programmazione"), "dd/mm/yyyy")

if isnull(ls_data) or ls_data = "" then
	g_mb.messagebox("Manutenzioni","Attenzione! Inserire una data valida!")
	return -1
end if

ldt_data = datetime(date(ls_data),00:00:00)

ls_tipo_data = dw_ext_prog_manut.getitemstring(1, "tipo_programmazione")

if ls_tipo_data = "" or isnull(ls_tipo_data) then	
	g_mb.messagebox("Manutenzioni","Attenzione! Scegliere il tipo di uso della data!")
	return -1
end if

ls_flag_scadenze = dw_ext_prog_manut.getitemstring(1, "flag_scadenze")

	
setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT  prog_tipo_manutenzione,   
		  descrizione,   
		  num_protocollo,   
		  des_blob,   
		  path_documento  
FROM    det_tipi_manutenzioni  
WHERE   cod_azienda = :s_cs_xx.cod_azienda and
		  cod_attrezzatura = :ls_cod_attrezzatura and
		  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni
lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';
		 
if sqlca.sqlcode <> 0 then	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if	
	
for ll_i = 1 to dw_tipi_man_attr_selezionate.rowcount()
	
	ls_cod_attrezzatura = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_attrezzatura")
	ls_cod_tipo_manutenzione = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_tipo_manutenzione")
	ls_selezionato = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "selezionato")
	
	dw_ext_prog_manut.object.messaggio_t.text = ls_cod_attrezzatura + " - " + ls_cod_tipo_manutenzione
	
	if ls_selezionato="S" then
	else
		continue
	end if
		
	select count(anno_registrazione) 
	into   :ll_count
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
		
	if not isnull(ll_count) and ll_count > 0 then
		
		select count(*)
		into   :ll_count
		from	 manutenzioni,
				 programmi_manutenzione
		where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
				 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
				 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
				 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
				 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
				 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
				 manutenzioni.flag_eseguito = 'N';
					 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
			setpointer(arrow!)
			return -1
		end if
			
		if not isnull(ll_count) and ll_count > 0 then continue
			
	end if
	
	//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
	if is_flag_abr = 'S' then
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2,
				 flag_pubblica_intranet
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2,
				 :ls_pubblica_intranet
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda	and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if				
	else
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda	and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if			
	end if
	ll_ore = hour(time(ldt_tempo_previsto))		
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
			 anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
	
	// *** 26/10/2004 seleziono lista di distribuzione
	
	select cod_tipo_lista_dist,
	       cod_lista_dist 
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			 cod_attrezzatura = :ls_cod_attrezzatura;
			 
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
	
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
	
	// ***
	if is_flag_abr = 'S' then	
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2,
														flag_pubblica_intranet)														
		values	                         (:s_cs_xx.cod_azienda,
													  :ll_anno_registrazione,
													  :ll_num_registrazione,
													  :ldt_data_creazione_programma,
													  :ls_cod_attrezzatura,
													  :ls_cod_tipo_manutenzione,
													  :ls_flag_manutenzione,
													  :ls_flag_ricambio_codificato,
													  :ls_cod_ricambio,
													  :ls_cod_ricambio_alternativo,
													  :ls_des_ricambio_non_codificato,
													  :ll_num_reg_lista,
													  :ll_num_versione,
													  :ll_num_edizione,
													  :ls_periodicita,
													  :ll_frequenza,
													  :ld_quan_ricambio,
													  :ld_costo_unitario_ricambio,
													  :ls_cod_primario,
														:ls_cod_misura,
														:ld_val_min_taratura,
														:ld_val_max_taratura,
														:ld_valore_atteso,
														'N',
														:ls_note_idl,
														:ll_ore,
														:ll_minuti,
														:ls_cod_tipo_lista_dist,
														:ls_cod_lista_dist,
														:ls_cod_operaio,
														:ls_cod_cat_risorse_esterne,
														:ls_cod_risorsa_esterna,
														:ld_data_inizio_val_1,
														:ld_data_fine_val_1,
														:ld_data_inizio_val_2,
														:ld_data_fine_val_2,
														:ls_pubblica_intranet);
															 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	else
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2)														
		values	                         (:s_cs_xx.cod_azienda,
													  :ll_anno_registrazione,
													  :ll_num_registrazione,
													  :ldt_data_creazione_programma,
													  :ls_cod_attrezzatura,
													  :ls_cod_tipo_manutenzione,
													  :ls_flag_manutenzione,
													  :ls_flag_ricambio_codificato,
													  :ls_cod_ricambio,
													  :ls_cod_ricambio_alternativo,
													  :ls_des_ricambio_non_codificato,
													  :ll_num_reg_lista,
													  :ll_num_versione,
													  :ll_num_edizione,
													  :ls_periodicita,
													  :ll_frequenza,
													  :ld_quan_ricambio,
													  :ld_costo_unitario_ricambio,
													  :ls_cod_primario,
														:ls_cod_misura,
														:ld_val_min_taratura,
														:ld_val_max_taratura,
														:ld_valore_atteso,
														'N',
														:ls_note_idl,
														:ll_ore,
														:ll_minuti,
														:ls_cod_tipo_lista_dist,
														:ls_cod_lista_dist,
														:ls_cod_operaio,
														:ls_cod_cat_risorse_esterne,
														:ls_cod_risorsa_esterna,
														:ld_data_inizio_val_1,
														:ld_data_fine_val_1,
														:ld_data_inizio_val_2,
														:ld_data_fine_val_2);
															 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	end if
	
	//#################################################################
	//memorizza piano manutenzione inserito
	if wf_inserisci_temporanea(ll_null, ll_anno_registrazione, ll_num_registrazione, "P", ll_null, ll_null, ls_cod_attrezzatura, ls_cod_tipo_manutenzione) < 0 then
		//messaggio e rollback già dati
		setpointer(arrow!)
		return -1
	end if
	//#################################################################
	
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio )  
	select cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_ricambio,   
			cod_prodotto,   
			des_prodotto,   
			quan_utilizzo,   
			prezzo_ricambio  
	 from tipi_manutenzioni_ricambi
	 where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	do while 1=1
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
		if sqlca.sqlcode = 100 then exit
			
		INSERT INTO det_prog_manutenzioni  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  progressivo,   
				  descrizione,   
				  num_protocollo,   
				  flag_blocco,   
				  data_blocco,   
				  des_blob,   
				  path_documento )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ll_prog_tipo_manutenzione,   
				  :ls_descrizione,   
				  :ll_num_protocollo,   
				  'N',   
				  null,   
				  :ls_des_blob,   
				  :ls_path_documento )  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if

		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
			        cod_attrezzatura = :ls_cod_attrezzatura and
				     cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in ricerca (select) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
		
	lb_inserimento = true
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if
					
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "B"     // battute stampi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if				
				
			case "O"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
			case "M"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
			case else
				
				update programmi_manutenzione
				set    flag_scadenze = :ls_flag_scadenze
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
				if ls_flag_scadenze = "S" then

					if luo_manutenzioni.uof_crea_manutenzione_programmata(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
						g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
						rollback;
						setpointer(arrow!)
						return -1
					end if
					
					// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
					int mese1_i, mese1_f, mese2_i, mese2_f, giorno1_i, giorno1_f, giorno2_i, giorno2_f
										
					giorno1_i = ( ld_data_inizio_val_1 / 100)
					mese1_i = mod( ld_data_inizio_val_1, 100)
										
					giorno1_f = ( ld_data_fine_val_1 / 100)
					mese1_f = mod( ld_data_fine_val_1, 100)
					
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
					
						if month(date(ldt_data)) < mese1_i or month(date(ldt_data)) > mese1_f then
							ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data)) < giorno1_i or day(date(ldt_data)) > giorno1_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
							end if
						end if
					
					else										
						giorno2_i = ( ld_data_inizio_val_2 / 100)
						mese2_i = mod( ld_data_inizio_val_2, 100)
											
						giorno2_f = ( ld_data_fine_val_2 / 100)
						mese2_f = mod( ld_data_fine_val_2, 100)							
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data)) < mese2_i or month(date(ldt_data)) > mese2_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data)) < giorno2_i or day(date(ldt_data)) > giorno2_f then
									ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
								end if
							end if												
						end if
					end if
						
					// controllo di che tipo è la data: se è la data della prima scadenza ... allora devo cambiare la data della 
					// registrazione, altrimenti, a partire da quella data, trovo la data della prossima scadenza
		
					if ls_tipo_data <> "Data prima scadenza" then					
						ldt_data_1 = luo_manutenzioni.uof_prossima_scadenza( ldt_data, ls_periodicita, ll_frequenza)					
					else
						ldt_data_1 = ldt_data
					end if						
					
					// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
					
						if month(date(ldt_data_1)) < mese1_i or month(date(ldt_data_1)) > mese1_f then
							ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data_1)) < giorno1_i or day(date(ldt_data_1)) > giorno1_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
							end if
						end if
					
					else										
						
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data_1)) < mese2_i or month(date(ldt_data_1)) > mese2_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data_1)) < giorno2_i or day(date(ldt_data_1)) > giorno2_f then
									ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
								end if
							end if												
						end if
					end if					
					
					update manutenzioni
					set    data_registrazione = :ldt_data_1
					where  cod_azienda = :s_cs_xx.cod_azienda and    
							 anno_reg_programma = :ll_anno_registrazione	and    
							 num_reg_programma = :ll_num_registrazione;
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Manutenzioni","Errore in aggiornamento data registrazione! " + sqlca.sqlerrtext)
						rollback;
						setpointer(arrow!)
						return -1
					end if
				end if
				
		end choose		
	end if	
next 
	 
destroy luo_manutenzioni

//spostato fuori dalla funzione
//commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	dw_ext_prog_manut.object.messaggio_t.text = "Programmazione avvenuta con successo!"
end if

return 1
end function

public function integer wf_programmazione_periodo ();datetime					ldt_da_data, ldt_a_data, ldt_tempo_previsto, ldt_data_1, ldt_data, ldt_scadenze[], ldt_data_registrazione, &
							ldt_data_prossimo_intervento, ldt_null, ldt_max_data_registrazione, ldt_data_creazione_programma, &
							ldt_scadenze_vuoto[]

string						ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_gest_manutenzione, ls_flag_ricambio_codificato, ls_cod_ricambio, &
							ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, ls_cod_primario, ls_cod_misura, ls_selezionato, &
							ls_flag_manutenzione, ls_note_idl, ls_des_blob, ls_path_documento, ls_descrizione, ls_messaggio, ls_flag_scadenze_esistenti, &
							ls_cod_tipo_lista_dist, ls_cod_lista_dist,ls_cod_operaio,ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna, ls_pubblica_intranet,&
							ls_errore
			
long						ll_anno_registrazione, ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, ll_ore, ll_minuti, &
							ll_num_registrazione, ll_prog_tipo_manutenzione, ll_num_protocollo, ll_num_programmazioni, ll_ii, ll_null

dec{4}					ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean					lb_inserimento

dec{0}					ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

uo_manutenzioni		luo_manutenzioni

blob						lbb_blob


setnull(ldt_null)
setnull(ll_null)

ls_flag_scadenze_esistenti = dw_ext_prog_manut.getitemstring( 1, "flag_scadenze_esistenti")
ldt_da_data = dw_ext_prog_manut.getitemdatetime( 1, "da_data")
ldt_a_data = dw_ext_prog_manut.getitemdatetime( 1, "a_data")

if isnull(ldt_da_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data inizio programmazione!")
	return -1
end if

if isnull(ldt_a_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data fine programmazione!")
	return -1	
end if

if ldt_da_data > ldt_a_data then
	g_mb.messagebox("OMNIA", "Attenzione: Controllare le date del periodo di programmazione!")
	return -1	
end if

setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT prog_tipo_manutenzione,   
		 descrizione,   
		 num_protocollo,   
		 des_blob,   
		 path_documento  
FROM   det_tipi_manutenzioni  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni

lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then	
	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 

	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
if sqlca.sqlcode = 100 then
	
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if	

ll_num_programmazioni = dw_tipi_man_attr_selezionate.rowcount()
	
for ll_i = 1 to ll_num_programmazioni
	
	ls_cod_attrezzatura = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_attrezzatura")
	ls_cod_tipo_manutenzione = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_tipo_manutenzione")
	ls_selezionato = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "selezionato")
	
	dw_ext_prog_manut.object.messaggio_t.text = ls_cod_attrezzatura + " - " + ls_cod_tipo_manutenzione
	
	if ls_selezionato="S" then
	else
		continue
	end if
	
	if is_flag_abr = 'S' then
	
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2,
				 flag_pubblica_intranet
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2,
				 :ls_pubblica_intranet
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
			
		end if				
	else
		select flag_ricambio_codificato,
				 cod_ricambio,
				 cod_ricambio_alternativo,
				 des_ricambio_non_codificato,
				 num_reg_lista,
				 num_versione,
				 num_edizione,
				 periodicita,
				 frequenza,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_primario,
				 cod_misura,
				 val_min_taratura,
				 val_max_taratura,
				 valore_atteso,
				 flag_manutenzione,
				 modalita_esecuzione,
				 tempo_previsto,
				 cod_operaio,
				 cod_cat_risorse_esterne,
				 cod_risorsa_esterna,
				 data_inizio_val_1,
				 data_fine_val_1,
				 data_inizio_val_2,
				 data_fine_val_2
		into   :ls_flag_ricambio_codificato,
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,
				 :ls_des_ricambio_non_codificato,
				 :ll_num_reg_lista,
				 :ll_num_versione,
				 :ll_num_edizione,
				 :ls_periodicita,
				 :ll_frequenza,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_primario,
				 :ls_cod_misura,
				 :ld_val_min_taratura,
				 :ld_val_max_taratura,
				 :ld_valore_atteso,
				 :ls_flag_manutenzione,
				 :ls_note_idl,
				 :ldt_tempo_previsto,
				 :ls_cod_operaio,
				 :ls_cod_cat_risorse_esterne,
				 :ls_cod_risorsa_esterna,
				 :ld_data_inizio_val_1,
				 :ld_data_fine_val_1,
				 :ld_data_inizio_val_2,
				 :ld_data_fine_val_2
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
			
	end if	
end if

	ll_ore = hour(time(ldt_tempo_previsto))		
	
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then

		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)	
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
	
	// *** 26/10/2004 seleziono lista di distribuzione
	
	select cod_tipo_lista_dist,
	       cod_lista_dist 
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			 cod_attrezzatura = :ls_cod_attrezzatura;
			 
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
	
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
	
	// ***		
	if is_flag_abr = 'S' then
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2,
														flag_pubblica_intranet)
											  values	(:s_cs_xx.cod_azienda,
														 :ll_anno_registrazione,
														 :ll_num_registrazione,
														 :ldt_data_creazione_programma,
														 :ls_cod_attrezzatura,
														 :ls_cod_tipo_manutenzione,
														 :ls_flag_manutenzione,
														 :ls_flag_ricambio_codificato,
														 :ls_cod_ricambio,
														 :ls_cod_ricambio_alternativo,
														 :ls_des_ricambio_non_codificato,
														 :ll_num_reg_lista,
														 :ll_num_versione,
														 :ll_num_edizione,
														 :ls_periodicita,
														 :ll_frequenza,
														 :ld_quan_ricambio,
														 :ld_costo_unitario_ricambio,
														 :ls_cod_primario,
														 :ls_cod_misura,
														 :ld_val_min_taratura,
														 :ld_val_max_taratura,
														 :ld_valore_atteso,
														 'N',
														 :ls_note_idl,
														 :ll_ore,
														 :ll_minuti,
														 :ls_cod_tipo_lista_dist,
														 :ls_cod_lista_dist,
														 :ls_cod_operaio,
														 :ls_cod_cat_risorse_esterne,
														 :ls_cod_risorsa_esterna,
														 :ld_data_inizio_val_1,
														 :ld_data_fine_val_1,
														 :ld_data_inizio_val_2,
														 :ld_data_fine_val_2,
														 :ls_pubblica_intranet);
														 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	else
		insert into programmi_manutenzione (cod_azienda,
														anno_registrazione,
														num_registrazione,
														data_creazione,
														cod_attrezzatura,
														cod_tipo_manutenzione,
														flag_tipo_intervento,
														flag_ricambio_codificato,
														cod_ricambio,
														cod_ricambio_alternativo,
														ricambio_non_codificato,
														num_reg_lista,
														num_versione,
														num_edizione,
														periodicita,
														frequenza,
														quan_ricambio,
														costo_unitario_ricambio,
														cod_primario,
														cod_misura,
														valore_min_taratura,
														valore_max_taratura,
														valore_atteso,
														flag_priorita,
														note_idl,
														operaio_ore_previste,
														operaio_minuti_previsti,
														cod_tipo_lista_dist,
														cod_lista_dist,
														cod_operaio,
														cod_cat_risorse_esterne,
														cod_risorsa_esterna,
														data_inizio_val_1,
														data_fine_val_1,
														data_inizio_val_2,
														data_fine_val_2)
											  values	(:s_cs_xx.cod_azienda,
														 :ll_anno_registrazione,
														 :ll_num_registrazione,
														 :ldt_data_creazione_programma,
														 :ls_cod_attrezzatura,
														 :ls_cod_tipo_manutenzione,
														 :ls_flag_manutenzione,
														 :ls_flag_ricambio_codificato,
														 :ls_cod_ricambio,
														 :ls_cod_ricambio_alternativo,
														 :ls_des_ricambio_non_codificato,
														 :ll_num_reg_lista,
														 :ll_num_versione,
														 :ll_num_edizione,
														 :ls_periodicita,
														 :ll_frequenza,
														 :ld_quan_ricambio,
														 :ld_costo_unitario_ricambio,
														 :ls_cod_primario,
														 :ls_cod_misura,
														 :ld_val_min_taratura,
														 :ld_val_max_taratura,
														 :ld_valore_atteso,
														 'N',
														 :ls_note_idl,
														 :ll_ore,
														 :ll_minuti,
														 :ls_cod_tipo_lista_dist,
														 :ls_cod_lista_dist,
														 :ls_cod_operaio,
														 :ls_cod_cat_risorse_esterne,
														 :ls_cod_risorsa_esterna,
														 :ld_data_inizio_val_1,
														 :ld_data_fine_val_1,
														 :ld_data_inizio_val_2,
														 :ld_data_fine_val_2);
														 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
			rollback;
			setpointer(arrow!)
			return -1
		end if	
	end if
	
	//#################################################################
	//memorizza piano manutenzione inserito
	if wf_inserisci_temporanea(ll_null, ll_anno_registrazione, ll_num_registrazione, "P", ll_null, ll_null,ls_cod_attrezzatura, ls_cod_tipo_manutenzione) < 0 then
		//messaggio e rollback già dati
		setpointer(arrow!)
		return -1
	end if
	//#################################################################
	
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio )  
	select cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_ricambio,   
			cod_prodotto,   
			des_prodotto,   
			quan_utilizzo,   
			prezzo_ricambio  
	 from tipi_manutenzioni_ricambi
	 where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		
		g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if
	
	do while 1=1
		
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento;
										
		if sqlca.sqlcode = -1 then
			
			g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		INSERT INTO det_prog_manutenzioni  
					 ( cod_azienda,   
					   anno_registrazione,   
					   num_registrazione,   
					   progressivo,   
					   descrizione,   
					   num_protocollo,   
					   flag_blocco,   
					   data_blocco,   
					   des_blob,   
					   path_documento )  
		VALUES   ( :s_cs_xx.cod_azienda,   
					  :ll_anno_registrazione,   
					  :ll_num_registrazione,   
					  :ll_prog_tipo_manutenzione,   
					  :ls_descrizione,   
					  :ll_num_protocollo,   
					  'N',   
					  null,   
					  :ls_des_blob,   
					  :ls_path_documento )  ;
					 
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
	
		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_attrezzatura = :ls_cod_attrezzatura and
					  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in ricerca (select) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
		
	lb_inserimento = true
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if
				
	// ** GESTIONE SEMPLICE DELLE MANUTENZIONI
	
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "B"     // battute stampi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
			case "O"  // ** ore
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
			case "M" // ** minuti
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
			case else // ** G = giorni, S = settimane, E = mesi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
						 anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
				// *** programmazione periodo funzione 1: trovo il vettore con le date delle scadenze
				
				// *** controllo se esistono programmi di manutenzione
				select count(anno_registrazione) 
				into   :ll_count
				from   programmi_manutenzione
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_attrezzatura = :ls_cod_attrezzatura and 
						 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
						
				if sqlca.sqlcode < 0 then			
					g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1					
				end if
					
				// controllo se esistono scadenze da confermare
				if not isnull(ll_count) and ll_count > 0 then
						
					select count(*)
					into   :ll_count
					from	 manutenzioni,
							 programmi_manutenzione
					where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
							 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
							 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
							 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
							 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
							 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
							 manutenzioni.flag_eseguito = 'N';
							 
					if sqlca.sqlcode < 0 then
						
						g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
						setpointer(arrow!)
						return -1
						
					end if
					
					// se ci sono scadenze da confermare allora guardo il flag
					if not isnull(ll_count) and ll_count > 0 then
						
						// *** flag = 'S' allora seleziono la massima scadenza e faccio partire il periodo di programmazione
						//     dalla prossima scadenza a partire da quella data di registrazione
						if ls_flag_scadenze_esistenti = 'S' then
							
							select MAX(data_registrazione)
							into   :ldt_max_data_registrazione
							from	 manutenzioni,
									 programmi_manutenzione
							where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
									 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
									 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
									 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
									 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
									 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
									 manutenzioni.flag_eseguito = 'N';
									 
							if sqlca.sqlcode < 0 then
								
								g_mb.messagebox("Omnia","Errore in controllo data manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
								setpointer(arrow!)
								return -1
								
							end if
						
							if not isnull(ldt_max_data_registrazione) then ldt_max_data_registrazione = luo_manutenzioni.uof_prossima_scadenza( ldt_max_data_registrazione, ls_periodicita, ll_frequenza )								
							
							if not isnull(ldt_max_data_registrazione) then ldt_da_data = ldt_max_data_registrazione
							//--------------------CLAUDIA 21/01/08
							// Se ci sono manutenzioni non eseguite e magari create in modalità semplice, modifico il programma di manutenzione impostando il flag_scadenze a no così diventa programmata e
							// quando la eseguo non chiede la manitenzione successiva
							update programmi_manutenzione
							set flag_scadenze = 'N'
							where programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
									 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
									 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione ;
							if sqlca.sqlcode < 0 then
								
								g_mb.messagebox("Omnia","Errore in modifica tipologia manutenzione codice: "+ls_cod_tipo_manutenzione +" " + sqlca.sqlerrtext,stopsign!)
								setpointer(arrow!)
								return -1
								
							end if
							
							
							//--------FINE CLAUDIA 21/01/08
						else
						// *** cancello le scadenze ancora da confermare e continuo con il periodo scritto 
						//     dall'operatore
						
							if wf_cancella_manutenzioni(ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_errore)<0 then
								g_mb.error("Omnia",ls_errore)
								rollback;
								setpointer(arrow!)
								return -1
							end if

						end if
					end if
						
				end if
	
				// ***
				
				ldt_scadenze[] = ldt_scadenze_vuoto[]
				
				luo_manutenzioni.uof_scadenze_periodo( ldt_da_data , date(ldt_da_data), date(ldt_a_data), ls_periodicita, ll_frequenza, ldt_scadenze)
				
				if UpperBound(ldt_scadenze) < 1 then continue
				
				for ll_ii = 1 to UpperBound(ldt_scadenze)
					
					ldt_data_registrazione = ldt_scadenze[ll_ii]
					
					if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(ll_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, ls_messaggio) <> 0 then
							g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
							rollback;
							setpointer(arrow!)
							return -1
					end if
					
				next
				
				// ***

		end choose		
	end if	
		
next 
	 
destroy luo_manutenzioni

//spostato fuori dalla funzione
//commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	dw_ext_prog_manut.object.messaggio_t.text = "Programmazione avvenuta con successo!"
end if
	
return 1
end function

public function integer wf_carica ();long 	 		ll_riga, ll_tot_man, ll_index

string 		ls_prog, ls_non_prog, ls_parziali, ls_cod_reparto, ls_cod_categoria, ls_cod_area, ls_attr_da, ls_attr_a, ls_sql, ls_where, ls_flag_interrotte, &
				ls_flag_programmata, ls_tipo_man_escludi[], ls_tipo_man_where, ls_stato, ls_errore, ls_appo, ls_stato_attrezzatura, ls_cod_divisione, ls_cod_tipo_man, &
				ls_cod_attrezzatura_cu, ls_cod_tipo_manutenzione_cu, ls_des_tipo_manutenzione_cu, ls_periodicita_cu, ls_des_attrezzatura_cu
				
decimal		ld_frequenza_cu

integer		li_stato_attrezz, li_stato_tipo_man

datastore	lds_tipi_man

setpointer(Hourglass!)

dw_sel_elenco_attrez.accepttext()
dw_tipi_man_attrezzature.reset()
dw_piani_esistenti.reset()

dw_tipi_man_attrezzature.setredraw(false)

ls_prog = dw_sel_elenco_attrez.getitemstring(1,"flag_prog")
ls_non_prog = dw_sel_elenco_attrez.getitemstring(1,"flag_non_prog")
ls_parziali = dw_sel_elenco_attrez.getitemstring(1,"flag_parziali")
ls_flag_interrotte = dw_sel_elenco_attrez.getitemstring(1,"flag_interrotte")

ls_cod_reparto = dw_sel_elenco_attrez.getitemstring(1,"cod_reparto")				//impianto
ls_cod_categoria = dw_sel_elenco_attrez.getitemstring(1,"cod_categoria")			//categoria
ls_cod_area = dw_sel_elenco_attrez.getitemstring(1,"cod_area")						//area aziendale
ls_cod_divisione = dw_sel_elenco_attrez.getitemstring(1,"cod_divisione")			//commessa (sarà utilizzato solo se ib_global_service=TRUE)

ls_attr_da = dw_sel_elenco_attrez.getitemstring(1,"cod_attr_da")
ls_attr_a = dw_sel_elenco_attrez.getitemstring(1,"cod_attr_a")

ls_cod_tipo_man = dw_sel_elenco_attrez.getitemstring(1,"cod_tipo_manutenzione")

ls_sql = is_sql_origine

ls_where = "where  tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "(anag_attrezzature.flag_blocco = 'N' or anag_attrezzature.data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "') and "+&
					  "(tab_tipi_manutenzione.flag_blocco = 'N' or tab_tipi_manutenzione.data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "') "

//aggiungo il resto dei filtri
if g_str.isnotempty(ls_cod_reparto) then
	ls_where += "and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
end if
if g_str.isnotempty(ls_cod_categoria) then
	ls_where += "and anag_attrezzature.cod_cat_attrezzature = '" + ls_cod_categoria + "' "
end if
if g_str.isnotempty(ls_cod_area) then
	ls_where += "and anag_attrezzature.cod_area_aziendale = '" + ls_cod_area + "' "
end if

//(sarà utilizzato solo se ib_global_service=TRUE)
if g_str.isnotempty(ls_cod_divisione) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_cod_divisione + "' ) "
end if
//-----------------------------------------------------

if g_str.isnotempty(ls_attr_da) then
	ls_where += "and anag_attrezzature.cod_attrezzatura >= '" + ls_attr_da + "' "
end if
if g_str.isnotempty(ls_attr_a) then
	ls_where += "and anag_attrezzature.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if g_str.isnotempty(ls_cod_tipo_man) then
	ls_where += "and tab_tipi_manutenzione.cod_tipo_manutenzione = '" + ls_cod_tipo_man + "' "
end if

//non far vedere quelle cartacee, telefoniche, giro ispettivo, immissione e dismissione (per GUERRATO)
wf_escludi_tipologie(ls_tipo_man_escludi[])

ls_tipo_man_where = ""
for ll_riga=1 to upperbound(ls_tipo_man_escludi[])
	if ll_riga=1 then
		//metto la colonna e apro parentesi
		ls_tipo_man_where = "and tab_tipi_manutenzione.cod_tipo_manutenzione not in ("
	end if
	
	if ll_riga>1 then
		//faccio precedere da una virgola separatrice
		ls_tipo_man_where += ","
	end if

	ls_tipo_man_where += "'"+ls_tipo_man_escludi[ll_riga]+"'"
	
	if ll_riga=upperbound(ls_tipo_man_escludi[]) then
		//chiudi parentesi
		ls_tipo_man_where += ")"
	end if
next

ls_where += ls_tipo_man_where
//----------------------------------------------------------------------------

ls_sql += ls_where

//order by
ls_sql += " order by anag_attrezzature.cod_attrezzatura,tab_tipi_manutenzione.cod_tipo_manutenzione "

if not f_crea_datastore(lds_tipi_man, ls_sql) then
	//messaggio di errore già dato
	dw_tipi_man_attrezzature.setredraw(true)
	setpointer(Arrow!)
	return -1
end if

ll_tot_man = lds_tipi_man.retrieve()

for ll_index= 1 to ll_tot_man
	ls_cod_attrezzatura_cu = lds_tipi_man.getitemstring(ll_index, 1)
	ls_cod_tipo_manutenzione_cu = lds_tipi_man.getitemstring(ll_index, 2)
	ls_des_tipo_manutenzione_cu = lds_tipi_man.getitemstring(ll_index, 3)
	ls_periodicita_cu = lds_tipi_man.getitemstring(ll_index, 4)
	ld_frequenza_cu = lds_tipi_man.getitemnumber(ll_index, 5)
	ls_des_attrezzatura_cu = lds_tipi_man.getitemstring(ll_index, 6)			
	
	dw_sel_elenco_attrez.object.messaggio_t.text = ls_cod_attrezzatura_cu + " - " + ls_des_attrezzatura_cu
	
	li_stato_attrezz = wf_get_stato_attrezzatura(ls_cod_attrezzatura_cu, ls_where, ls_errore)
	choose case li_stato_attrezz
		case 0
			//NON PROGRAMMATA
			if ls_non_prog<>"S" then continue
			ls_stato_attrezzatura = "N"
			
		case 1
			//PARZIALMENTE PROGRAMMATA
			if ls_parziali<> "S" then continue
			ls_stato_attrezzatura = "P"
			
		case 2
			//COMPLETAMENTE PROGRAMMATA
			if ls_prog<>"S" then continue
			ls_stato_attrezzatura = "S"
			
		case 3
			//INTERROTTA
			if ls_flag_interrotte<>"S" then continue
			ls_stato_attrezzatura = "I"
			
		case else
			//ERRORE
			g_mb.error(ls_errore)
			destroy lds_tipi_man;
			dw_tipi_man_attrezzature.setredraw(true)
			setpointer(Arrow!)
			return -1
			
	end choose

	li_stato_tipo_man = wf_get_stato_tipologie(ls_cod_attrezzatura_cu, ls_cod_tipo_manutenzione_cu, ls_errore)
	choose case li_stato_tipo_man
		case 0
			//da PROGRAMMARE
			ls_stato = "X"
			
		case 1
			//INTERROTTA
			ls_stato = "R"
			
		case 2
			//PROGRAMMATA
			ls_stato = "V"
			
		case else
			//ERRORE
			g_mb.error(ls_errore)
			destroy lds_tipi_man;
			dw_tipi_man_attrezzature.setredraw(true)
			setpointer(Arrow!)
			return -1
			
	end choose

	ll_riga = dw_tipi_man_attrezzature.insertrow(0)
	dw_tipi_man_attrezzature.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura_cu)
	dw_tipi_man_attrezzature.setitem(ll_riga, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione_cu)
	dw_tipi_man_attrezzature.setitem(ll_riga, "des_tipo_manutenzione", ls_des_tipo_manutenzione_cu)
	dw_tipi_man_attrezzature.setitem(ll_riga, "periodicita", ls_periodicita_cu)
	dw_tipi_man_attrezzature.setitem(ll_riga, "frequenza", ld_frequenza_cu)
	dw_tipi_man_attrezzature.setitem(ll_riga, "des_attrezzatura", ls_des_attrezzatura_cu)
	dw_tipi_man_attrezzature.setitem(ll_riga, "stato", ls_stato)
	dw_tipi_man_attrezzature.setitem(ll_riga, "stato_attrezzatura", ls_stato_attrezzatura)
next

destroy lds_tipi_man;

dw_tipi_man_attrezzature.sort()
dw_tipi_man_attrezzature.groupcalc()

dw_tipi_man_attrezzature.setredraw(true)

dw_folder.fu_selecttab(2)
dw_folder.fu_disabletab(3)
dw_folder.fu_disabletab(4)

setpointer(Arrow!)

return 0
end function

public subroutine wf_impostazioni ();string				ls_expression, ls_tipoman_programmata, ls_tipoman_interrotta, ls_tipoman_non_programmata, ls_bitmap, ls_riga_selezionata, &
					ls_attr_non_programmate, ls_attr_parz_programmate, ls_attr_compl_programmata, ls_attr_interrotta, &
					ls_p_semplice, ls_p_periodo_1, ls_p_periodo_2, ls_man_eseguita, ls_man_non_eseguita

//per la legenda attrezzature
ls_attr_interrotta					= s_cs_xx.volume + s_cs_xx.risorse + "treeview\cartella_rossa.png"			//rossa: attrezzature interrotte
ls_attr_parz_programmate		= s_cs_xx.volume + s_cs_xx.risorse + "treeview\cartella.png"					//gialla: attrezzature parzialmente programmate
ls_attr_compl_programmata	= s_cs_xx.volume + s_cs_xx.risorse + "treeview\cartella_verde.png"			//verde: attrezzature completamente programmate
ls_attr_non_programmate		= s_cs_xx.volume + s_cs_xx.risorse + "treeview\cartella_grigia.png"			//grigia: attrezzature non programmate

//per la legenda tipi manutenzione
ls_tipoman_programmata = s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_verde.png"			//verde: tipologia manutenzione programmata
ls_tipoman_non_programmata = s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_grigio.png"		//grigia: tipologia manutenzione NON programmata
ls_tipoman_interrotta = s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_rosso.png"					//rossa: tipologia manutenzione interrotta

ls_riga_selezionata = s_cs_xx.volume + s_cs_xx.risorse + "ULT.BMP"

ls_p_semplice = s_cs_xx.volume + s_cs_xx.risorse + "p_semplice_s.png"
ls_p_periodo_1 = s_cs_xx.volume + s_cs_xx.risorse + "p_periodo_1_s.png"
ls_p_periodo_2 = s_cs_xx.volume + s_cs_xx.risorse + "p_periodo_2_s.png"

ls_man_eseguita = s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eseguita.png"
ls_man_non_eseguita = s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png"

//Icona Attrezzatura ---------------------------------------------------------------------------------------------------------------------
//Rossa:		INTERROTTA
//Gialla:		PARZIALMENTE PROGRAMMATA
//Verde:		COMPLETAMENTE PROGRAMMATA
//Grigia:		NON PROGRAMMATA
ls_expression = "case (stato_attrezzatura when 'I' then '"+ls_attr_interrotta+"' when 'N' then '"+ls_attr_non_programmate+"' when 'P' then '"+ls_attr_parz_programmate+"' else '"+ls_attr_compl_programmata+"') "
ls_bitmap = "bitmap("+ls_expression+")"
dw_tipi_man_attrezzature.object.cf_img_stato_attrezzatura.expression = ls_bitmap
dw_tipi_man_attr_selezionate.object.cf_img_stato_attrezzatura.expression = ls_bitmap


//Icona Tipo Manutenzione ---------------------------------------------------------------------------------------------------------------------
//Verde:		PROGRAMMATA
//Rossa:		INTERROTTA
//Grigia:		DA PROGRAMMARE
ls_expression = "case (stato when 'V' then '"+ls_tipoman_programmata+"' when 'R' then '"+ls_tipoman_interrotta+"' else '"+ls_tipoman_non_programmata+"') "
ls_bitmap = "bitmap("+ls_expression+")"
dw_tipi_man_attrezzature.object.cf_img_stato.expression = ls_bitmap
dw_tipi_man_attr_selezionate.object.cf_img_stato.expression = ls_bitmap


ls_expression = "if(pos( scadenza_1 , '*', 1) >  0 , '"+ ls_man_non_eseguita + "' , '"+ ls_man_eseguita+ "' )"
ls_bitmap = "bitmap("+ls_expression+")"
dw_piani_esistenti.object.cf_1.expression = ls_bitmap
dw_risultati.object.cf_1.expression = ls_bitmap
//-----
ls_expression = "if(pos( scadenza_2 , '*', 1) >  0 , '"+ ls_man_non_eseguita + "' , '"+ ls_man_eseguita+ "' )"
ls_bitmap = "bitmap("+ls_expression+")"
dw_piani_esistenti.object.cf_2.expression = ls_bitmap
dw_risultati.object.cf_2.expression = ls_bitmap
//-----
ls_expression = "if(pos( scadenza_3 , '*', 1) >  0 , '"+ ls_man_non_eseguita + "' , '"+ ls_man_eseguita+ "' )"
ls_bitmap = "bitmap("+ls_expression+")"
dw_piani_esistenti.object.cf_3.expression = ls_bitmap
dw_risultati.object.cf_3.expression = ls_bitmap
//-----
ls_expression = "if(pos( scadenza_4 , '*', 1) >  0 , '"+ ls_man_non_eseguita + "' , '"+ ls_man_eseguita+ "' )"
ls_bitmap = "bitmap("+ls_expression+")"
dw_piani_esistenti.object.cf_4.expression = ls_bitmap
dw_risultati.object.cf_4.expression = ls_bitmap
//-----
ls_expression = "if(pos( scadenza_5 , '*', 1) >  0 , '"+ ls_man_non_eseguita + "' , '"+ ls_man_eseguita+ "' )"
ls_bitmap = "bitmap("+ls_expression+")"
dw_piani_esistenti.object.cf_5.expression = ls_bitmap
dw_risultati.object.cf_5.expression = ls_bitmap
//-----


//immagine su riga selezionata
dw_tipi_man_attrezzature.object.p_riga_selezionata.FileName = ls_riga_selezionata
dw_tipi_man_attr_selezionate.object.p_riga_selezionata.FileName = ls_riga_selezionata


//LEGENDA
//legenda su attrezzature in testata ------------------
dw_tipi_man_attrezzature.object.p_non_programmate.FileName = ls_attr_non_programmate
dw_tipi_man_attrezzature.object.p_parzialmente_programmate.FileName = ls_attr_parz_programmate
dw_tipi_man_attrezzature.object.p_completamente_programmate.FileName = ls_attr_compl_programmata
dw_tipi_man_attrezzature.object.p_interrotte.FileName = ls_attr_interrotta

dw_tipi_man_attr_selezionate.object.p_non_programmate.FileName = ls_attr_non_programmate
dw_tipi_man_attr_selezionate.object.p_parzialmente_programmate.FileName = ls_attr_parz_programmate
dw_tipi_man_attr_selezionate.object.p_completamente_programmate.FileName = ls_attr_compl_programmata
dw_tipi_man_attr_selezionate.object.p_interrotte.FileName = ls_attr_interrotta


dw_piani_esistenti.object.p_eseguita.FileName = ls_man_eseguita
dw_piani_esistenti.object.p_non_eseguita.FileName = ls_man_non_eseguita

dw_risultati.object.p_eseguita.FileName = ls_man_eseguita
dw_risultati.object.p_non_eseguita.FileName = ls_man_non_eseguita



//mmagini esplicative dei diversi tipi di programmazione -------------------------------------------------------------------------------------
//Semplice, per periodo (considera scadenze esistenti e no)
dw_ext_prog_manut.object.p_semplice.FileName = ls_p_semplice
dw_ext_prog_manut.object.p_periodo_1.FileName = ls_p_periodo_1
dw_ext_prog_manut.object.p_periodo_2.FileName = ls_p_periodo_2


//legenda su tipologie manutenzione in testata -----
dw_tipi_man_attrezzature.object.p_tipoman_da_programmare.FileName = ls_tipoman_non_programmata			//grigia
dw_tipi_man_attrezzature.object.p_tipoman_interrotta.FileName = ls_tipoman_interrotta								//rossa
dw_tipi_man_attrezzature.object.p_tipoman_programmata.FileName = ls_tipoman_programmata					//verde

dw_tipi_man_attr_selezionate.object.p_tipoman_da_programmare.FileName = ls_tipoman_non_programmata	//grigia
dw_tipi_man_attr_selezionate.object.p_tipoman_interrotta.FileName = ls_tipoman_interrotta							//rossa
dw_tipi_man_attr_selezionate.object.p_tipoman_programmata.FileName = ls_tipoman_programmata				//verde


//nascondi i pulsanti e le funzioni della dw che non servono
dw_tipi_man_attr_selezionate.object.b_seleziona.visible = false
dw_tipi_man_attr_selezionate.object.b_prosegui.visible = false
dw_tipi_man_attr_selezionate.object.selezionato.visible = false


//memorizza sql di origine
is_sql_origine = dw_tipi_man_attrezzature.getsqlselect()
end subroutine

public function integer wf_escludi_tipologie (ref string fs_tipologie[]);//per guerrato

string ls_tipo_man
long ll_index

ll_index = 0

//------------------------------------------------------------------------------------------------------
select stringa
into   :ls_tipo_man
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM1';
		 
if sqlca.sqlcode = 0 and ls_tipo_man<>"" and not isnull(ls_tipo_man) then
	ll_index += 1
	fs_tipologie[ll_index] = ls_tipo_man
end if	

//------------------------------------------------------------------------------------------------------
select stringa
into   :ls_tipo_man
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM2';
		 
if sqlca.sqlcode = 0 and ls_tipo_man<>"" and not isnull(ls_tipo_man) then
	ll_index += 1
	fs_tipologie[ll_index] = ls_tipo_man
end if	

//------------------------------------------------------------------------------------------------------
select stringa
into   :ls_tipo_man
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM3';
		 
if sqlca.sqlcode = 0 and ls_tipo_man<>"" and not isnull(ls_tipo_man) then
	ll_index += 1
	fs_tipologie[ll_index] = ls_tipo_man
end if	

//------------------------------------------------------------------------------------------------------
select stringa
into   :ls_tipo_man
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM4';
		 
if sqlca.sqlcode = 0 and ls_tipo_man<>"" and not isnull(ls_tipo_man) then
	ll_index += 1
	fs_tipologie[ll_index] = ls_tipo_man
end if

//------------------------------------------------------------------------------------------------------
select stringa
into   :ls_tipo_man
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TM5';
		 
if sqlca.sqlcode = 0 and ls_tipo_man<>"" and not isnull(ls_tipo_man) then
	ll_index += 1
	fs_tipologie[ll_index] = ls_tipo_man
end if

return 1
end function

public function integer wf_get_stato_tipologie (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, ref string fs_errore);long ll_num_prog, ll_tot_manut

select count(*)
into :ll_num_prog
from programmi_manutenzione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:fs_cod_attrezzatura and
			cod_tipo_manutenzione=:fs_cod_tipo_manutenzione;
			
if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo programmi manutenzione per tipologia "+fs_cod_tipo_manutenzione+" e attrezzatura "+fs_cod_attrezzatura+": "+sqlca.sqlerrtext
	return -1
end if

if isnull(ll_num_prog) then ll_num_prog=0

if ll_num_prog>0 then
	//esistono piani
	//controlla le manutenzioni non ancora eseguite, ordinarie con piano di manutenzione
	select count(*) 
	into   :ll_tot_manut
	from   manutenzioni
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_manutenzione = :fs_cod_tipo_manutenzione and
				cod_attrezzatura = :fs_cod_attrezzatura and
				anno_reg_programma is not null and
				num_reg_programma is not null and
				flag_ordinario = 'S' and
				flag_eseguito = 'N';
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in controllo manutenzioni per tipologia "+fs_cod_tipo_manutenzione+" e attrezzatura "+fs_cod_attrezzatura+": "+sqlca.sqlerrtext
		return -1
	end if
	
	if ll_tot_manut>0 then
		//TIPOLOGIA IN PROGRAMMAZIONE (o PROGRAMMATA): infatti esistono programmi, e delle scadenze non ancora confermate
		return 2
		
	else
		//TIPOLOGIA con PROGRAMMAZIONE INTERROTTA
		return 1
		
	end if
else
	//non esistono programmi
	return 0
	
end if


end function

public subroutine wf_conferma ();long   ll_risp
string ls_flag_programma_periodo, ls_errore
integer li_ret

dw_ext_prog_manut.accepttext()

if dw_tipi_man_attr_selezionate.rowcount() > 0 then
else
	g_mb.error("OMNIA", "Nessuna tipologia di manutenzione da programmare è stata selezionata!")
	return
end if

if not g_mb.confirm("OMNIA", "Continuare con la programmazione delle manutenzioni per le tipologie selezionate?", 1) then
	return
end if

ls_flag_programma_periodo = dw_ext_prog_manut.getitemstring( 1, "flag_programma_periodo")

//genera qui i valori della programmazione dell'utente ###################################
select max(prog_programmazione)
into :il_prog_programmazione
from temp_programmazione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura MAX prog. in temporanea prima della programmazione!", sqlca)
	return
end if

if isnull(il_prog_programmazione) then il_prog_programmazione = 0
il_prog_programmazione += 1

idt_data_programmazione = datetime(today(), 00:00:00)
idt_ora_programmazione = datetime(date(1900, 1, 1), now())
//#######################################################################


if ls_flag_programma_periodo <> 'S' then
	//PROGRAMMAZIONE SEMPLICE --------------------------------
	if ib_global_service then
		//caso global service e gestione fasi
		li_ret = wf_programmazione_semplice_gb()
	else
		li_ret = wf_programmazione_semplice()
	end if
	
else
	//PROGRAMMAZIONE PER PERIODO ------------------------------
	if ib_global_service then
		//caso global service e gestione fasi
		li_ret = wf_programmazione_periodo_gb()
	else
		li_ret = wf_programmazione_periodo()
	end if
end if

//chiama qui la funzione che popola recupera le manutenzioni appena create e le mette nella temporanea
if li_ret > 0 then
	if wf_inserisci_manut_temporanea() < 0 then
		//eventuali messaggi di errore già dati
		dw_ext_prog_manut.object.messaggio_t.text = "Errore in fase di programmazione manutenzioni (T)!"
		rollback;
		return
	end if
else
	//eventuali messaggi di errore già dati
	dw_ext_prog_manut.object.messaggio_t.text = "Errore in fase di programmazione manutenzioni!"
	rollback;
	return
end if

//se arrivi fin qui fai Commit e prosegui
commit;

//tutto a posto, visualizza le programmazioni fatte e dai la possibilità di annullare nel folder 4
dw_folder.fu_enabletab(4)
dw_folder.fu_selecttab(4)

dw_folder.fu_disabletab(3)
dw_folder.fu_disabletab(2)
dw_folder.fu_disabletab(1)

if wf_risultati_programmazione(il_prog_programmazione, ls_errore) < 0 then
	g_mb.error(ls_errore)
end if

return
end subroutine

public function integer wf_get_stato_attrezzatura (string fs_cod_attrezzatura, string fs_where, ref string fs_errore);long					ll_num_prog, ll_count_tipi_man, ll_count_piani, ll_index, ll_count_aperte, ll_count_chiuse, ll_ret
string					ls_sql, ls_cod_tipo_man
datastore			lds_data


ls_sql = 	"select tab_tipi_manutenzione.cod_tipo_manutenzione "+&
			"from tab_tipi_manutenzione "+&
			 "join anag_attrezzature on tab_tipi_manutenzione.cod_azienda = anag_attrezzature.cod_azienda and "+&
         							    "tab_tipi_manutenzione.cod_attrezzatura = anag_attrezzature.cod_attrezzatura "

ls_sql += fs_where
ls_sql += " and tab_tipi_manutenzione.cod_attrezzatura='"+fs_cod_attrezzatura+"' "

if not f_crea_datastore(lds_data, ls_sql) then
	//messaggio già dato
	fs_errore = "Errore in lettura tipologie manutenzione per attrezzatura "+fs_cod_attrezzatura+": "+sqlca.sqlerrtext
	return -1
end if

ll_count_piani = 0

//di sicuro ce ne sarà almeno uno
ll_count_tipi_man = lds_data.retrieve()

for ll_index=1 to ll_count_tipi_man
	ls_cod_tipo_man = lds_data.getitemstring(ll_index, 1)
	
	select count(*)
	into :ll_num_prog
	from programmi_manutenzione
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_attrezzatura=:fs_cod_attrezzatura and
				cod_tipo_manutenzione=:ls_cod_tipo_man;
				
	if sqlca.sqlcode<0 then
		fs_errore = sqlca.sqlerrtext
		destroy lds_data;
		return -1
	end if
	
	if isnull(ll_num_prog) then ll_num_prog=0
	
	if ll_num_prog>0 then
		//ci sono programmi
		ll_count_piani += 1
	else
		//per questa tipologia non ci sono programmi
	end if
next


//alla fine confronta ll_count_piani con ll_count_tipi_man
// se ll_count_piani=ll_count_tipi_man 	allora l'apparecchiatura è COMPLETAMENTE PROGRAMMATA
//se ll_count_piani = 0 						allora l'apparecchiatura è NON PROGRAMMATA
//in tutti gli altri casi							l'apparecchiatura è PARZIALMENTE PROGRAMMATA

//NOTA: ll_count_tipi_man non può essere zero in quanto vengono elaborate solo attrezzature con almeno un tipo manutenzione ...

if ll_count_piani=ll_count_tipi_man then
	//se entri qui dentro vuol dire che ogni tipologia manutenzione ha almeno un piano
	
	//prima di stabilire che sia completamente programmata, verifica caso mai sia da considerarsi interrotta o parzialmente programmata
	//infatti può essere che pur avendo ogni tipologia di manutenzione il rispettivo piano:
	//		1) nessuna tipologia dell'attrezzatura abbia manutenzione aperte -> CASO INTERROTTA
	//		2) esiste almeno una tipologia senza manutenzioni aperte ed almeno una con manutenzioni aperte -> CASO PARZIALMENTE PROGRAMMATA
	//		3) tutte le tipologia hanno manutenzioni aperte -> CASO COMPLETAMENTE PROGRAMMATA
	ll_ret = wf_controlla_attrezzatura(lds_data, fs_cod_attrezzatura, fs_errore)
	destroy lds_data;
	return ll_ret
	
//	//COMPLETAMENTE PROGRAMMATA
//	return 2
	
elseif ll_count_piani=0 then
	//nessuna tipologia di manutenzione ha il piano
	//NON PROGRAMMATA
	destroy lds_data;
	return 0
	
else
	//almeno una tipologia di manutenzione non ha il piano
	//PARZIALMENTE PROGRAMMATA
	destroy lds_data;
	return 1
end if











end function

public function integer wf_retrieve_piani_esistenti (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, ref string fs_errore);string			ls_sql, ls_cod_attrezz_cu, ls_cod_tipo_man_cu, ls_periodicita_cu, ls_flag_eseguito_cu, ls_appo, ls_hold, ls_piano
datastore	lds_piani
long			ll_tot, ll_index, ll_num_scadenze_per_riga, ll_new, ll_prossimo_indice_a_capo, ll_indice_colonna, &
				ll_anno_prog_cu, ll_num_prog_cu, ll_anno_man_cu, ll_num_man_cu
decimal		ld_frequenza_cu
datetime		ldt_data_programma_cu, ldt_data_man_cu
boolean		lb_nuova_riga

dw_piani_esistenti.reset()

ls_sql = "SELECT "+&
				"programmi_manutenzione.anno_registrazione,"+&
				"programmi_manutenzione.num_registrazione,"+&
				"programmi_manutenzione.cod_attrezzatura,"+&
				"programmi_manutenzione.cod_tipo_manutenzione,"+&
				"programmi_manutenzione.periodicita,"+&
				"programmi_manutenzione.frequenza,"+&
				"programmi_manutenzione.data_creazione,"+&
				"manutenzioni.anno_registrazione,"+&
				"manutenzioni.num_registrazione,"+&
				"manutenzioni.data_registrazione,"+&
				"manutenzioni.flag_eseguito "+&
		 "FROM manutenzioni "+&
		 "JOIN programmi_manutenzione ON 	programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and "+&
										"programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and "+&
										"programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma "+&
		"WHERE 	programmi_manutenzione.cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
						"programmi_manutenzione.cod_attrezzatura = '"+fs_cod_attrezzatura+"' and "+&
						"programmi_manutenzione.cod_tipo_manutenzione = '"+fs_cod_tipo_manutenzione+"' "+&
		"ORDER BY programmi_manutenzione.anno_registrazione DESC,"+&
					  "programmi_manutenzione.num_registrazione DESC,"+&
					  "manutenzioni.data_registrazione ASC "

if not f_crea_datastore(lds_piani, ls_sql) then
	fs_errore = "Errore creazione datastore!"
	return -1
end if

ll_tot = lds_piani.retrieve()

//sistema le scadenze nella stessa riga ogni 5 date
ll_num_scadenze_per_riga = 5
ll_prossimo_indice_a_capo = 1		//al primo dato fai insert row comunque

ll_indice_colonna = 0

ls_hold = ""

for ll_index=1 to ll_tot
	
	//leggi da datastore ----------------------------------------------
	ll_anno_prog_cu = lds_piani.getitemnumber(ll_index, 1)
	ll_num_prog_cu = lds_piani.getitemnumber(ll_index, 2)
	ls_cod_attrezz_cu = lds_piani.getitemstring(ll_index, 3)
	ls_cod_tipo_man_cu = lds_piani.getitemstring(ll_index, 4)
	ls_periodicita_cu = lds_piani.getitemstring(ll_index, 5)
	ld_frequenza_cu = lds_piani.getitemdecimal(ll_index, 6)
	ldt_data_programma_cu = lds_piani.getitemdatetime(ll_index, 7)
	ll_anno_man_cu = lds_piani.getitemnumber(ll_index, 8)
	ll_num_man_cu = lds_piani.getitemnumber(ll_index, 9)
	ldt_data_man_cu = lds_piani.getitemdatetime(ll_index, 10)
	ls_flag_eseguito_cu = lds_piani.getitemstring(ll_index, 11)
	
	ls_piano = "Programma n° "+string(ll_anno_prog_cu)+"/"+string(ll_num_prog_cu)+&
																					" del "+string(ldt_data_programma_cu, "dd/mm/yyyy")
	
	lb_nuova_riga = false
	ll_indice_colonna += 1
	
	if ll_indice_colonna > 5 or ll_index=1 or ls_hold <> ls_piano then
		//vai a capo
		ll_new = dw_piani_esistenti.insertrow(0)
		lb_nuova_riga = true
		if ll_index <> 1 then ll_indice_colonna = 1
		
		ls_hold = ls_piano
		
	end if

	
	//fai setitem
	if lb_nuova_riga then
	
		dw_piani_esistenti.setitem(ll_new, "piano_manutenzione", ls_piano)
																					
		dw_piani_esistenti.setitem(ll_new, "anno_reg_programma", ll_anno_prog_cu)
		dw_piani_esistenti.setitem(ll_new, "num_reg_programma", ll_num_prog_cu)
																					
		choose case ls_periodicita_cu
			case "M"
				ls_appo = "Minuti"
			case "O"
				ls_appo = "Ore"
			case "G"
				ls_appo = "Giorni"
			case "S"
				ls_appo = "Settimane"
			case "E"
				ls_appo = "Mesi"
			case "B"
				ls_Appo = "Nr.Battute"
			case else
				ls_appo = "???"
		end choose
		dw_piani_esistenti.setitem(ll_new, "periodicita", ls_appo + ": "+string(ld_frequenza_cu, "#####0"))
		
		dw_piani_esistenti.setitem(ll_new, "cod_attr_tipo_man", "(" + ls_cod_attrezz_cu + " - " + ls_cod_tipo_man_cu + ")")
	end if

	if ls_flag_eseguito_cu="S" then
		ls_appo = " "//"Eseguita!"
	else
		ls_appo = "*"//"Aperta"
	end if

	dw_piani_esistenti.setitem(ll_new, "scadenza_" + string(ll_indice_colonna), ls_appo+string(ldt_data_man_cu, "dd/mm/yy"))
	dw_piani_esistenti.setitem(ll_new, "anno_man_" + string(ll_indice_colonna), ll_anno_man_cu)
	dw_piani_esistenti.setitem(ll_new, "num_man_" + string(ll_indice_colonna), ll_num_man_cu)
	
next

destroy lds_piani;

dw_piani_esistenti.sort()
dw_piani_esistenti.groupcalc()

dw_piani_esistenti.object.datawindow.print.preview = "yes"

return 1
end function

public subroutine wf_visualizza_manutenzione (long fl_anno_man, long fl_num_man);s_cs_xx_parametri lstr_parametri

if fl_anno_man>0 and fl_num_man>0 then
else
	return
end if

if not isvalid(w_manutenzioni) then
			
	lstr_parametri.parametro_ul_1 = fl_anno_man
	lstr_parametri.parametro_ul_2 = fl_num_man

	window_open_parm(w_manutenzioni, -1, lstr_parametri)			
	
else
	w_manutenzioni.show()
	w_manutenzioni.wf_carica_singola_registrazione(fl_anno_man, fl_num_man)
end if		
end subroutine

public function integer wf_calcola_scadenze (integer fi_giorno_inizio, integer fi_mese_inizio, integer fi_giorno_fine, integer fi_mese_fine, datetime fdt_da_data, datetime fdt_a_data, string fs_periodicita, long fl_frequenza, long fl_anno_registrazione, long fl_num_registrazione);//usata solo in modalità global service --------

long						ll_anno_1, ll_anno_2, ll_anni[], ll_giorni_da_anticipo
date						ldt_date_inizio[], ldt_date_fine[]
datetime					ldt_scadenze[], ldt_scadenze_vuoto[]
integer					li_i, li_ii, li_appo
string 					ls_messaggio

uo_manutenzioni 		luo_manutenzioni

ll_anno_1 = year(date(fdt_da_data))
ll_anno_2 = year(date(fdt_a_data))

li_i = 0
do while ll_anno_1 <= ll_anno_2
	li_i ++
	ldt_date_inizio[li_i] = date( ll_anno_1, fi_mese_inizio, fi_giorno_inizio)
	ldt_date_fine[li_i] = date( ll_anno_1, fi_mese_fine, fi_giorno_fine)
	ll_anno_1 ++	
loop

if fdt_da_data > datetime( ldt_date_inizio[1], 00:00:00) and fdt_da_data < datetime( ldt_date_fine[1], 00:00:00) then
	ldt_date_inizio[1] = date(fdt_da_data)
elseif fdt_da_data >= datetime( ldt_date_fine[1], 00:00:00) then
	setnull(ldt_date_inizio[1])
	setnull(ldt_date_fine[1])	
end if

if fdt_a_data > datetime( ldt_date_inizio[li_i], 00:00:00) and fdt_a_data <= datetime( ldt_date_fine[li_i], 00:00:00) then
	ldt_date_inizio[li_i] = date(fdt_a_data)
elseif fdt_a_data < datetime( ldt_date_inizio[li_i], 00:00:00) then	
	setnull(ldt_date_inizio[li_i])
	setnull(ldt_date_fine[li_i])
end if

luo_manutenzioni = create uo_manutenzioni

for li_i = 1 to Upperbound(ldt_date_inizio)  
	
	if not isnull(ldt_date_inizio[li_i]) and not isnull(ldt_date_fine[li_i]) then
		
		ldt_scadenze[] = ldt_scadenze_vuoto[]
	
		luo_manutenzioni.uof_scadenze_periodo( datetime(ldt_date_inizio[li_i], 00:00:00) , ldt_date_inizio[li_i], ldt_date_fine[li_i], fs_periodicita, fl_frequenza, ldt_scadenze)
					
		if UpperBound(ldt_scadenze) < 1 then continue	
		
		for li_ii = 1 to UpperBound(ldt_scadenze)
						
			if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(fl_anno_registrazione, fl_num_registrazione, ldt_scadenze[li_ii], ls_messaggio) <> 0 then
				destroy luo_manutenzioni
				g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext + ";" + ls_messaggio)
				return -1
			end if
				
		next								
		
		if li_i < Upperbound(ldt_date_inizio) then
			ll_giorni_da_anticipo = daysafter(date(ldt_scadenze[UpperBound(ldt_scadenze)]),ldt_date_fine[li_i])
			if not isnull(ll_giorni_da_anticipo) and ll_giorni_da_anticipo > 0 then
				ll_giorni_da_anticipo = ll_giorni_da_anticipo * (-1)
				li_appo = li_i + 1
				ldt_scadenze[] = ldt_scadenze_vuoto[]
				luo_manutenzioni.uof_scadenze_periodo( datetime(relativedate(ldt_date_inizio[li_appo], ll_giorni_da_anticipo), 00:00:00) , relativedate(ldt_date_inizio[li_appo], ll_giorni_da_anticipo), ldt_date_fine[li_appo], fs_periodicita, fl_frequenza, ldt_scadenze)
				if UpperBound(ldt_scadenze) < 1 then continue
				ldt_date_inizio[li_appo] = date(ldt_scadenze[2])
			end if
		end if
		
	end if
next
destroy luo_manutenzioni
return 0
end function

public function integer wf_costo_orario_e_operaio (string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, ref string fs_cod_operaio, ref decimal fd_costo_orario_operaio);//usata solo in modalità global service ---------------------------------------------------------

if isnull(fs_cod_tipo_manutenzione) or fs_cod_tipo_manutenzione = "" then
	setnull(fs_cod_operaio)
	fd_costo_orario_operaio = 0.0
	return -1
end if
if isnull(fs_cod_attrezzatura) or fs_cod_attrezzatura = "" then
	setnull(fs_cod_operaio)
	fd_costo_orario_operaio = 0.0
	return -1
end if

select
	tab_tipi_manutenzione.cod_operaio
into
	:fs_cod_operaio
from tab_tipi_manutenzione
where tab_tipi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda
	and tab_tipi_manutenzione.cod_attrezzatura = :fs_cod_attrezzatura
	and tab_tipi_manutenzione.cod_tipo_manutenzione = :fs_cod_tipo_manutenzione
;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore durante la selezione dell'operatore!",Exclamation!)
	setnull(fs_cod_operaio)
	fd_costo_orario_operaio = 0.0
	return -1
end if

select 
	 tab_cat_attrezzature.costo_medio_orario
	//,anag_operai.cod_operaio
into
	 :fd_costo_orario_operaio
	//,:fs_cod_operaio
from tab_cat_attrezzature
join anag_operai on anag_operai.cod_azienda=tab_cat_attrezzature.cod_azienda
							and anag_operai.cod_cat_attrezzature=tab_cat_attrezzature.cod_cat_attrezzature
join tab_tipi_manutenzione on tab_tipi_manutenzione.cod_azienda=anag_operai.cod_azienda
											and tab_tipi_manutenzione.cod_operaio=anag_operai.cod_operaio
where tab_cat_attrezzature.cod_azienda = :s_cs_xx.cod_azienda
		and tab_tipi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda
		and tab_tipi_manutenzione.cod_tipo_manutenzione = :fs_cod_tipo_manutenzione
		and tab_tipi_manutenzione.cod_attrezzatura = :fs_cod_attrezzatura
;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore durante la selezione del costo medio orario!",Exclamation!)
	setnull(fs_cod_operaio)
	fd_costo_orario_operaio = 0.0
	return -1
end if


return 1
end function

public function integer wf_risorsa_esterna (string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, ref string fs_cod_cat_risorse_esterne, ref string fs_cod_risorsa_esterna);if isnull(fs_cod_tipo_manutenzione) or fs_cod_tipo_manutenzione = "" then
	setnull(fs_cod_cat_risorse_esterne)
	setnull(fs_cod_risorsa_esterna)
	//fd_costo_orario_operaio = 0.0
	return -1
end if
if isnull(fs_cod_attrezzatura) or fs_cod_attrezzatura = "" then
	setnull(fs_cod_cat_risorse_esterne)
	setnull(fs_cod_risorsa_esterna)
	//fd_costo_orario_operaio = 0.0
	return -1
end if

select 	cod_cat_risorse_esterne,
			cod_risorsa_esterna
into 		:fs_cod_cat_risorse_esterne,
			:fs_cod_risorsa_esterna
from tab_tipi_manutenzione
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :fs_cod_attrezzatura and cod_tipo_manutenzione = :fs_cod_tipo_manutenzione;
	
if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore durante la selezione della risorsa esterna!",Exclamation!)
	setnull(fs_cod_cat_risorse_esterne)
	setnull(fs_cod_risorsa_esterna)
	//fd_costo_orario_operaio = 0.0
	return -1
end if

return 1
end function

public function integer wf_programmazione_semplice_gb ();long     ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, &
	      ll_anno_registrazione, ll_num_registrazione, ll_ore, ll_minuti, ll_prog_tipo_manutenzione, ll_num_protocollo, ll_prog_mimetype
			
string   ls_flag_gest_manutenzione, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
	      ls_flag_ricambio_codificato, ls_cod_ricambio, ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, &
		   ls_cod_primario, ls_cod_misura, ls_flag_manutenzione, ls_messaggio, ls_note_idl, ls_descrizione, ls_des_blob, &
			ls_path_documento, ls_data, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_selezionato
			
dec{4}   ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean  lb_inserimento

datetime ldt_tempo_previsto, ldt_data, ldt_data_1, ldt_data_creazione_programma

blob lbb_blob

uo_manutenzioni luo_manutenzioni

long     ll_risp, ll_giorni, ll_mesi, ll_anni, ll_null

string   ls_tipo_data

dec{0}   ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

//donato 15/07/2008 + 20/04/2010 (recupero risorsa esterna)
//variabili usate per l'inserimento nella tabella programmi_manutenzione dell'operaio e del suo costo orario
decimal ld_costo_orario
string ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna
//--------------------------------

setnull(ll_null)
ls_data = string(dw_ext_prog_manut.getitemdatetime(1, "data_programmazione"), "dd/mm/yyyy")

if isnull(ls_data) or ls_data = "" then
	g_mb.messagebox("Manutenzioni","Attenzione! Inserire una data valida!")
	return -1
end if

ldt_data = datetime(date(ls_data),00:00:00)

ls_tipo_data = dw_ext_prog_manut.getitemstring(1, "tipo_programmazione")

if ls_tipo_data = "" or isnull(ls_tipo_data) then	
	g_mb.messagebox("Manutenzioni","Attenzione! Scegliere il tipo di uso della data!")
	return -1
end if
	
setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT  prog_tipo_manutenzione,   
		  descrizione,   
		  num_protocollo,   
		  des_blob,   
		  path_documento,
		  prog_mimetype
FROM    det_tipi_manutenzioni  
WHERE   cod_azienda = :s_cs_xx.cod_azienda and
		  cod_attrezzatura = :ls_cod_attrezzatura and
		  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni
lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';
		 
if sqlca.sqlcode <> 0 then	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if
	
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if	
	
for ll_i = 1 to dw_tipi_man_attr_selezionate.rowcount()
	
	ls_cod_attrezzatura = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_attrezzatura")
	ls_cod_tipo_manutenzione = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_tipo_manutenzione")
	ls_selezionato = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "selezionato")
	
	dw_ext_prog_manut.object.messaggio_t.text = ls_cod_attrezzatura + " - " + ls_cod_tipo_manutenzione
	
	if ls_selezionato="S" then
	else
		continue
	end if
	
	select count(anno_registrazione) 
	into   :ll_count
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
		
	if not isnull(ll_count) and ll_count > 0 then
		
		select count(*)
		into   :ll_count
		from	 manutenzioni,
				 programmi_manutenzione
		where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
				 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
				 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
				 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
				 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
				 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
				 manutenzioni.flag_eseguito = 'N';
					 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
			setpointer(arrow!)
			return -1
		end if
			
		if not isnull(ll_count) and ll_count > 0 then continue
			
	end if
		
	select flag_ricambio_codificato,
			 cod_ricambio,
			 cod_ricambio_alternativo,
			 des_ricambio_non_codificato,
			 num_reg_lista,
			 num_versione,
			 num_edizione,
			 periodicita,
			 frequenza,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_primario,
			 cod_misura,
			 val_min_taratura,
			 val_max_taratura,
			 valore_atteso,
			 flag_manutenzione,
			 modalita_esecuzione,
			 tempo_previsto,
			 data_inizio_val_1,
			 data_fine_val_1,
			 data_inizio_val_2,
			 data_fine_val_2
	into   :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_cod_ricambio_alternativo,
			 :ls_des_ricambio_non_codificato,
			 :ll_num_reg_lista,
			 :ll_num_versione,
			 :ll_num_edizione,
			 :ls_periodicita,
			 :ll_frequenza,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_primario,
			 :ls_cod_misura,
			 :ld_val_min_taratura,
			 :ld_val_max_taratura,
			 :ld_valore_atteso,
			 :ls_flag_manutenzione,
			 :ls_note_idl,
			 :ldt_tempo_previsto,
			 :ld_data_inizio_val_1,
			 :ld_data_fine_val_1,
			 :ld_data_inizio_val_2,
			 :ld_data_fine_val_2
   from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if				
		
	ll_ore = hour(time(ldt_tempo_previsto))		
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
			 anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
	
	// *** 26/10/2004 seleziono lista di distribuzione
	
	select cod_tipo_lista_dist,
	       cod_lista_dist 
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			 cod_attrezzatura = :ls_cod_attrezzatura;
			 
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
	
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
	
	//Donato 15/07/2008
	//recupero le info su operaio e suo costo orario
	wf_costo_orario_e_operaio(ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_cod_operaio, ld_costo_orario)
	if ls_cod_operaio = "" then setnull(ls_cod_operaio)
	// ***
	
	//Donato 20/04/2010
	//recupero le eventuali info sulle risorse estrene
	wf_risorsa_esterna(ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna)
	if ls_cod_cat_risorse_esterne="" then setnull(ls_cod_cat_risorse_esterne)
	if ls_cod_risorsa_esterna="" then setnull(ls_cod_risorsa_esterna)
	//---------
	
	insert into programmi_manutenzione (cod_azienda,
													anno_registrazione,
													num_registrazione,
													data_creazione,
													cod_attrezzatura,
													cod_tipo_manutenzione,
													flag_tipo_intervento,
													flag_ricambio_codificato,
													cod_ricambio,
													cod_ricambio_alternativo,
													ricambio_non_codificato,
													num_reg_lista,
													num_versione,
													num_edizione,
													periodicita,
													frequenza,
													quan_ricambio,
													costo_unitario_ricambio,
													cod_primario,
													cod_misura,
													valore_min_taratura,
													valore_max_taratura,
													valore_atteso,
													flag_priorita,
													note_idl,
													//Donato 15/07/2008:inserimento operaio e del suo costo orario
													cod_operaio,
													costo_orario_operaio,
													//-------------------------
													operaio_ore_previste,
													operaio_minuti_previsti,
													//Donato 20/04/2010: inserimento risorsa esterna
													cod_cat_risorse_esterne,
													cod_risorsa_esterna,
													//------------------------
													cod_tipo_lista_dist,
													cod_lista_dist,
													data_inizio_val_1,
													data_fine_val_1,
													data_inizio_val_2,
													data_fine_val_2)														
	values	                         (			:s_cs_xx.cod_azienda,
												:ll_anno_registrazione,
												:ll_num_registrazione,
												:ldt_data_creazione_programma,
												:ls_cod_attrezzatura,
												:ls_cod_tipo_manutenzione,
												:ls_flag_manutenzione,
												:ls_flag_ricambio_codificato,
												:ls_cod_ricambio,
												:ls_cod_ricambio_alternativo,
												:ls_des_ricambio_non_codificato,
												:ll_num_reg_lista,
												:ll_num_versione,
												:ll_num_edizione,
												:ls_periodicita,
												:ll_frequenza,
												:ld_quan_ricambio,
												:ld_costo_unitario_ricambio,
												:ls_cod_primario,
												:ls_cod_misura,
												:ld_val_min_taratura,
												:ld_val_max_taratura,
												:ld_valore_atteso,
												'N',
												:ls_note_idl,
												//Donato 15/07/2008:inserimento operaio e del suo costo orario
												 :ls_cod_operaio,
												 :ld_costo_orario,
												//-------------------------
												:ll_ore,
												:ll_minuti,
												//Donato 20/04/2010: inserimento risorsa esterna
												:ls_cod_cat_risorse_esterne,
												:ls_cod_risorsa_esterna,
												//-----------------------
												:ls_cod_tipo_lista_dist,
												:ls_cod_lista_dist,
												:ld_data_inizio_val_1,
												:ld_data_fine_val_1,
												:ld_data_inizio_val_2,
												:ld_data_fine_val_2);
														 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
	
	//#################################################################
	//memorizza piano manutenzione inserito
	if wf_inserisci_temporanea(ll_null, ll_anno_registrazione, ll_num_registrazione, "P", ll_null, ll_null, ls_cod_attrezzatura, ls_cod_tipo_manutenzione) < 0 then
		//messaggio e rollback già dati
		setpointer(arrow!)
		return -1
	end if
	//#################################################################
	
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio )  
	select cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_ricambio,   
			cod_prodotto,   
			des_prodotto,   
			quan_utilizzo,   
			prezzo_ricambio  
	 from tipi_manutenzioni_ricambi
	 where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	do while 1=1
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento,
										:ll_prog_mimetype;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
		if sqlca.sqlcode = 100 then exit
			
		INSERT INTO det_prog_manutenzioni  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  progressivo,   
				  descrizione,   
				  num_protocollo,   
				  flag_blocco,   
				  data_blocco,   
				  des_blob,   
				  path_documento,
				  prog_mimetype)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ll_prog_tipo_manutenzione,   
				  :ls_descrizione,   
				  :ll_num_protocollo,   
				  'N',   
				  null,   
				  :ls_des_blob,   
				  :ls_path_documento,
				  :ll_prog_mimetype)  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if

		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
			        cod_attrezzatura = :ls_cod_attrezzatura and
				     cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in ricerca (select) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
		
	lb_inserimento = true
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if
					
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "O"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
			case "M"
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
			case else
				
				update programmi_manutenzione
				set    flag_scadenze = 'S'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
				end if
				
				// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
				int mese1_i, mese1_f, mese2_i, mese2_f, giorno1_i, giorno1_f, giorno2_i, giorno2_f
										
				giorno1_i = ( ld_data_inizio_val_1 / 100)
				mese1_i = mod( ld_data_inizio_val_1, 100)										
				giorno1_f = ( ld_data_fine_val_1 / 100)
				mese1_f = mod( ld_data_fine_val_1, 100)
				
				//Donato 12/11/2008
				giorno2_i = ( ld_data_inizio_val_2 / 100)
				mese2_i = mod( ld_data_inizio_val_2, 100)										
				giorno2_f = ( ld_data_fine_val_2 / 100)
				mese2_f = mod( ld_data_fine_val_2, 100)
				//--------------------------------------
				
				if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 &
						and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 &
						and not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 &
						and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
					//sono specificati due intervalli
					if ls_tipo_data <> "Data prima scadenza" then
						ldt_data_1 = luo_manutenzioni.uof_prossima_scadenza( ldt_data, ls_periodicita, ll_frequenza)					
					else
						//vorrebbe forzare la data
						ldt_data_1 = ldt_data
					end if
					//controlla che cada negli intervalli o nella tolleranza o altrove
					ldt_data_1 = luo_manutenzioni.uof_scadenza_semplice_con_intervalli(ldt_data_1, ld_data_inizio_val_1,ld_data_fine_val_1, &
																	ld_data_inizio_val_2, ld_data_fine_val_2)
					if not isnull(ldt_data_1) then
						//prosegui con la registrazione della scadenza						
						if luo_manutenzioni.uof_crea_manut_progr_semplice2(ll_anno_registrazione, ll_num_registrazione, ldt_data_1, ls_messaggio) <> 0 then
							g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
							rollback;
							setpointer(arrow!)
							return -1
						end if
					else
						g_mb.messagebox("Manutenzioni","Si è verificato un errore durante il calcolo della scadenza della manutenzione programmata.")
						rollback;
						setpointer(arrow!)
						return -1
					end if
					
				else
					//tutto come prima del 12/11/2008
					if luo_manutenzioni.uof_crea_manutenzione_programmata(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
						g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
						rollback;
						setpointer(arrow!)
						return -1
					end if
					
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
					
						if month(date(ldt_data)) < mese1_i or month(date(ldt_data)) > mese1_f then
							ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data)) < giorno1_i or day(date(ldt_data)) > giorno1_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese1_i, giorno1_i))
							end if
						end if
						
					else										
						giorno2_i = ( ld_data_inizio_val_2 / 100)
						mese2_i = mod( ld_data_inizio_val_2, 100)
											
						giorno2_f = ( ld_data_fine_val_2 / 100)
						mese2_f = mod( ld_data_fine_val_2, 100)							
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data)) < mese2_i or month(date(ldt_data)) > mese2_f then
								ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data)) < giorno2_i or day(date(ldt_data)) > giorno2_f then
									ldt_data = datetime(date(year(date(ldt_data)), mese2_i, giorno2_i))
									end if
							end if												
						end if
					end if				
						
					// controllo di che tipo è la data: se è la data della prima scadenza ... allora devo cambiare la data della 
					// registrazione, altrimenti, a partire da quella data, trovo la data della prossima scadenza
		
					if ls_tipo_data <> "Data prima scadenza" then					
						ldt_data_1 = luo_manutenzioni.uof_prossima_scadenza( ldt_data, ls_periodicita, ll_frequenza)					
					else
						ldt_data_1 = ldt_data
					end if
					
					// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
						
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
						
						if month(date(ldt_data_1)) < mese1_i or month(date(ldt_data_1)) > mese1_f then
							ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
						else
							if day(date(ldt_data_1)) < giorno1_i or day(date(ldt_data_1)) > giorno1_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese1_i, giorno1_i))
							end if
						end if
						
					else										
							
						if not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
							if month(date(ldt_data_1)) < mese2_i or month(date(ldt_data_1)) > mese2_f then
								ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
							else
								if day(date(ldt_data_1)) < giorno2_i or day(date(ldt_data_1)) > giorno2_f then
									ldt_data_1 = datetime(date(year(date(ldt_data_1)), mese2_i, giorno2_i))
								end if
							end if												
						end if
					end if					
					
					update manutenzioni
					set    data_registrazione = :ldt_data_1
					where  cod_azienda = :s_cs_xx.cod_azienda and    
							 anno_reg_programma = :ll_anno_registrazione	and    
							 num_reg_programma = :ll_num_registrazione;
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Manutenzioni","Errore in aggiornamento data registrazione! " + sqlca.sqlerrtext)
						rollback;
						setpointer(arrow!)
						return -1
					end if
				end if
				
		end choose		
	end if	
next 
	 
destroy luo_manutenzioni

//spostato fuori dalla finestra
//commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	dw_ext_prog_manut.object.messaggio_t.text = "Programmazione avvenuta con successo!"
end if

return 1
end function

public function integer wf_programmazione_periodo_gb ();datetime					ldt_da_data, ldt_a_data, ldt_tempo_previsto, ldt_data_1, ldt_data, ldt_scadenze[], ldt_data_registrazione, &
							ldt_data_prossimo_intervento, ldt_null, ldt_max_data_registrazione, ldt_data_creazione_programma,ldt_scadenze_vuoto[]

string						ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_gest_manutenzione, ls_flag_ricambio_codificato, ls_cod_ricambio, &
							ls_cod_ricambio_alternativo, ls_des_ricambio_non_codificato, ls_periodicita, ls_cod_primario, ls_cod_misura, ls_selezionato, &
							ls_flag_manutenzione, ls_note_idl, ls_des_blob, ls_path_documento, ls_descrizione, ls_messaggio, ls_flag_scadenze_esistenti, &
							ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_errore
			
long						ll_anno_registrazione, ll_i, ll_count, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_frequenza, ll_ore, ll_minuti, &
							ll_num_registrazione, ll_prog_tipo_manutenzione, ll_num_protocollo, ll_num_programmazioni, ll_ii, ll_prog_mimetype, ll_null

dec{4}					ld_quan_ricambio, ld_costo_unitario_ricambio, ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso

boolean					lb_inserimento

uo_manutenzioni		luo_manutenzioni

blob						lbb_blob

dec{0}					ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2

//donato 15/07/2008 + 20/04/2010 (recupero info su risorsa esterna)
//variabili usate per l'inserimento nella tabella programmi_manutenzione dell'operaio e del suo costo orario
decimal					ld_costo_orario
string						ls_cod_operaio,ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna
//--------------------------------


setnull(ldt_null)
setnull(ll_null)

ls_flag_scadenze_esistenti = dw_ext_prog_manut.getitemstring( 1, "flag_scadenze_esistenti")

ldt_da_data = dw_ext_prog_manut.getitemdatetime( 1, "da_data")

ldt_a_data = dw_ext_prog_manut.getitemdatetime( 1, "a_data")

if isnull(ldt_da_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data inizio programmazione!")
	return -1
end if

if isnull(ldt_a_data) then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data fine programmazione!")
	return -1	
end if

if ldt_da_data > ldt_a_data then
	g_mb.messagebox("OMNIA", "Attenzione: Controllare le date del periodo di programmazione!")
	return -1	
end if

setpointer(hourglass!)
	
DECLARE cu_documenti CURSOR FOR  
SELECT prog_tipo_manutenzione,   
		 descrizione,   
		 num_protocollo,   
		 des_blob,   
		 path_documento,
		 prog_mimetype
FROM   det_tipi_manutenzioni  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
luo_manutenzioni = create uo_manutenzioni

lb_inserimento = false
	
select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then	
	
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_azienda " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
select flag_gest_manutenzione
into   :ls_flag_gest_manutenzione
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;
	 
if sqlca.sqlcode < 0 then 

	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if
	
if sqlca.sqlcode = 100 then
	
	g_mb.messagebox("Omnia", "Attenzione la tabella parametri_manutenzioni è vuota, non è possibile proseguire." + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
	
end if	

ll_num_programmazioni = dw_tipi_man_attr_selezionate.rowcount()
	
for ll_i = 1 to ll_num_programmazioni
	
	ls_cod_attrezzatura = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_attrezzatura")
	ls_cod_tipo_manutenzione = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "cod_tipo_manutenzione")
	
	ls_selezionato = dw_tipi_man_attr_selezionate.getitemstring(ll_i, "selezionato")
	
	dw_ext_prog_manut.object.messaggio_t.text = ls_cod_attrezzatura + " - " + ls_cod_tipo_manutenzione
	
	if ls_selezionato="S" then
	else
		continue
	end if

	select flag_ricambio_codificato,
			 cod_ricambio,
			 cod_ricambio_alternativo,
			 des_ricambio_non_codificato,
			 num_reg_lista,
			 num_versione,
			 num_edizione,
			 periodicita,
			 frequenza,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_primario,
			 cod_misura,
			 val_min_taratura,
			 val_max_taratura,
			 valore_atteso,
			 flag_manutenzione,
			 modalita_esecuzione,
			 tempo_previsto,
			 data_inizio_val_1,
			 data_fine_val_1,
			 data_inizio_val_2,
			 data_fine_val_2
	into   :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_cod_ricambio_alternativo,
			 :ls_des_ricambio_non_codificato,
			 :ll_num_reg_lista,
			 :ll_num_versione,
			 :ll_num_edizione,
			 :ls_periodicita,
			 :ll_frequenza,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_primario,
			 :ls_cod_misura,
			 :ld_val_min_taratura,
			 :ld_val_max_taratura,
			 :ld_valore_atteso,
			 :ls_flag_manutenzione,
			 :ls_note_idl,
			 :ldt_tempo_previsto,
			 :ld_data_inizio_val_1,
			 :ld_data_fine_val_1,
			 :ld_data_inizio_val_2,
			 :ld_data_fine_val_2
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			
	if sqlca.sqlcode <> 0 then
		
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if				
		
	ll_ore = hour(time(ldt_tempo_previsto))		
	
	ll_minuti = minute(time(ldt_tempo_previsto))
	
	select max(num_registrazione) + 1
   into   :ll_num_registrazione
	from   programmi_manutenzione
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione;
			
	if sqlca.sqlcode < 0 then

		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if							
		
	ldt_data_creazione_programma = datetime(date(today()), 00:00:00)	
		
	if isnull(ll_num_registrazione) then ll_num_registrazione = 1
	
	// *** 26/10/2004 seleziono lista di distribuzione
	
	select cod_tipo_lista_dist,
	       cod_lista_dist 
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
			 cod_attrezzatura = :ls_cod_attrezzatura;
			 
	if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
	
	if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
	
	
	//Donato 15/07/2008
	//recupero le info su operaio e suo costo orario
	wf_costo_orario_e_operaio(ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_cod_operaio, ld_costo_orario)
	if ls_cod_operaio = "" then setnull(ls_cod_operaio)
	// ***			
	
	//Donato 20/04/2010
	//recupero le eventuali info sulle risorse estrene
	wf_risorsa_esterna(ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna)
	if ls_cod_cat_risorse_esterne="" then setnull(ls_cod_cat_risorse_esterne)
	if ls_cod_risorsa_esterna="" then setnull(ls_cod_risorsa_esterna)
	//---------
	
	insert into programmi_manutenzione (cod_azienda,
													anno_registrazione,
													num_registrazione,
													data_creazione,
													cod_attrezzatura,
													cod_tipo_manutenzione,
													flag_tipo_intervento,
													flag_ricambio_codificato,
													cod_ricambio,
													cod_ricambio_alternativo,
													ricambio_non_codificato,
													num_reg_lista,
													num_versione,
													num_edizione,
													periodicita,
													frequenza,
													quan_ricambio,
													costo_unitario_ricambio,
													cod_primario,
													cod_misura,
													valore_min_taratura,
													valore_max_taratura,
													valore_atteso,
													flag_priorita,
													note_idl,
													//Donato 15/07/2008:inserimento operaio e del suo costo orario
													cod_operaio,
													costo_orario_operaio,
													//-------------------------
													operaio_ore_previste,
													operaio_minuti_previsti,
													//Donato 20/04/2010:inserimento risorsa esterna
													cod_cat_risorse_esterne,
													cod_risorsa_esterna,
													//-----------------------
													cod_tipo_lista_dist,
													cod_lista_dist,
													data_inizio_val_1,
													data_fine_val_1,
													data_inizio_val_2,
													data_fine_val_2)														
										  values	(:s_cs_xx.cod_azienda,
													 :ll_anno_registrazione,
													 :ll_num_registrazione,
													 :ldt_data_creazione_programma,
													 :ls_cod_attrezzatura,
													 :ls_cod_tipo_manutenzione,
													 :ls_flag_manutenzione,
													 :ls_flag_ricambio_codificato,
													 :ls_cod_ricambio,
													 :ls_cod_ricambio_alternativo,
													 :ls_des_ricambio_non_codificato,
													 :ll_num_reg_lista,
													 :ll_num_versione,
													 :ll_num_edizione,
													 :ls_periodicita,
													 :ll_frequenza,
													 :ld_quan_ricambio,
													 :ld_costo_unitario_ricambio,
													 :ls_cod_primario,
													 :ls_cod_misura,
													 :ld_val_min_taratura,
													 :ld_val_max_taratura,
													 :ld_valore_atteso,
													 'N',
													 :ls_note_idl,
													 //Donato 15/07/2008:inserimento operaio e del suo costo orario
													 :ls_cod_operaio,
													 :ld_costo_orario,
													//-------------------------
													 :ll_ore,
													 :ll_minuti,
													 //Donato 20/04/2010:inserimento risorsa esterna
													 :ls_cod_cat_risorse_esterne,
													 :ls_cod_risorsa_esterna,
													//-----------------------
													 :ls_cod_tipo_lista_dist,
													 :ls_cod_lista_dist,
													 :ld_data_inizio_val_1,
													 :ld_data_fine_val_1,
													 :ld_data_inizio_val_2,
													 :ld_data_fine_val_2);
													 
	if sqlca.sqlcode <> 0 then
		
		g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if
	
	//#################################################################
	//memorizza piano manutenzione inserito
	if wf_inserisci_temporanea(ll_null, ll_anno_registrazione, ll_num_registrazione, "P", ll_null, ll_null, ls_cod_attrezzatura, ls_cod_tipo_manutenzione) < 0 then
		//messaggio e rollback già dati
		setpointer(arrow!)
		return -1
	end if
	//#################################################################
	
	// inserisco anche i ricambi da utilizzare prendendoli dal tipo manutenzione
	
	insert into prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio )  
	select cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_ricambio,   
			cod_prodotto,   
			des_prodotto,   
			quan_utilizzo,   
			prezzo_ricambio  
	 from tipi_manutenzioni_ricambi
	 where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento ricambi in programmi manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
	end if	
	
	// ----------------------------  passo anche i documenti allegati al tipo manutenzione -----------------------
	open cu_documenti;
	if sqlca.sqlcode <> 0 then
		
		g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if
	
	do while 1=1
		
		fetch cu_documenti into :ll_prog_tipo_manutenzione, 
		                        :ls_descrizione, 
										:ll_num_protocollo,  
										:ls_des_blob, 
										:ls_path_documento,
										:ll_prog_mimetype;
										
		if sqlca.sqlcode = -1 then
			
			g_mb.messagebox("OMNIA","Errore nella OPEN del cursore cu_documenti~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		INSERT INTO det_prog_manutenzioni  
					 ( cod_azienda,   
					   anno_registrazione,   
					   num_registrazione,   
					   progressivo,   
					   descrizione,   
					   num_protocollo,   
					   flag_blocco,   
					   data_blocco,   
					   des_blob,   
					   path_documento,
						prog_mimetype)  
		VALUES   ( :s_cs_xx.cod_azienda,   
					  :ll_anno_registrazione,   
					  :ll_num_registrazione,   
					  :ll_prog_tipo_manutenzione,   
					  :ls_descrizione,   
					  :ll_num_protocollo,   
					  'N',   
					  null,   
					  :ls_des_blob,   
					  :ls_path_documento,
					  :ll_prog_mimetype)  ;
					 
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in inserimento dei documenti allegati~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
	
		selectblob blob
		into       :lbb_blob
		from       det_tipi_manutenzioni  
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_attrezzatura = :ls_cod_attrezzatura and
					  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
					  prog_tipo_manutenzione = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in ricerca (select) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
			
		updateblob det_prog_manutenzioni  
		set        blob = :lbb_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
				     anno_registrazione = :ll_anno_registrazione and
				     num_registrazione = :ll_num_registrazione and
				     progressivo = :ll_prog_tipo_manutenzione;
					  
		if sqlca.sqlcode <> 0 then
			
			g_mb.messagebox("OMNIA","Errore in memorizzazione (updateblob) dell'allegato~r~n"+sqlca.sqlerrtext)
			close cu_documenti;
			rollback;
			setpointer(arrow!)
			return -1
			
		end if
	
	loop
	close cu_documenti;
	// -----------------------------------------------------------------------------------------------------------
		
	lb_inserimento = true
	
	select periodicita
   into   :ls_periodicita
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		rollback;
		setpointer(arrow!)
		return -1
		
	end if
				
	// ** GESTIONE SEMPLICE DELLE MANUTENZIONI
	
	if ls_flag_gest_manutenzione = "N" then
		
		choose case ls_periodicita
				
			case "O"  // ** ore
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
			case "M" // ** minuti
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
				       anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
			case else // ** G = giorni, S = settimane, E = mesi
				
				update programmi_manutenzione
				set    flag_scadenze = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda and  
						 anno_registrazione = :ll_anno_registrazione and  
						 num_registrazione = :ll_num_registrazione;
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox("Omnia", "Errore in fase di update del flag scadenze in programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1
					
				end if
				
				
				// *** programmazione periodo funzione 1: trovo il vettore con le date delle scadenze
				
				// *** controllo se esistono programmi di manutenzione
				select count(anno_registrazione) 
				into   :ll_count
				from   programmi_manutenzione
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_attrezzatura = :ls_cod_attrezzatura and 
						 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
						
				if sqlca.sqlcode < 0 then			
					g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
					rollback;
					setpointer(arrow!)
					return -1					
				end if
					
				// controllo se esistono scadenze da confermare
				if not isnull(ll_count) and ll_count > 0 then
						
					select count(*)
					into   :ll_count
					from	 manutenzioni,
							 programmi_manutenzione
					where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
							 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
							 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
							 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
							 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
							 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
							 manutenzioni.flag_eseguito = 'N';
							 
					if sqlca.sqlcode < 0 then
						
						g_mb.messagebox("Omnia","Errore in controllo manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
						setpointer(arrow!)
						return -1
						
					end if
					
					// se ci sono scadenze da confermare allora guardo il flag
					if not isnull(ll_count) and ll_count > 0 then
						
						// *** flag = 'S' allora seleziono la massima scadenza e faccio partire il periodo di programmazione
						//     dalla prossima scadenza a partire da quella data di registrazione
						if ls_flag_scadenze_esistenti = 'S' then
							
							select MAX(data_registrazione)
							into   :ldt_max_data_registrazione
							from	 manutenzioni,
									 programmi_manutenzione
							where  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and
									 programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and
									 programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma and
									 programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
									 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
									 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione and
									 manutenzioni.flag_eseguito = 'N';
									 
							if sqlca.sqlcode < 0 then
								
								g_mb.messagebox("Omnia","Errore in controllo data manutenzioni programmate da eseguire: " + sqlca.sqlerrtext,stopsign!)
								setpointer(arrow!)
								return -1
								
							end if
						
							if not isnull(ldt_max_data_registrazione) then ldt_max_data_registrazione = luo_manutenzioni.uof_prossima_scadenza( ldt_max_data_registrazione, ls_periodicita, ll_frequenza )								
							
							if not isnull(ldt_max_data_registrazione) then ldt_da_data = ldt_max_data_registrazione
							//--------------------CLAUDIA 21/01/08
							// Se ci sono manutenzioni non eseguite e magari create in modalità semplice, modifico il programma di manutenzione impostando il flag_scadenze a no così diventa programmata e
							// quando la eseguo non chiede la manitenzione successiva
							update programmi_manutenzione
							set flag_scadenze = 'N'
							where programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and
									 programmi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
									 programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo_manutenzione ;
							if sqlca.sqlcode < 0 then
								
								g_mb.messagebox("Omnia","Errore in modifica tipologia manutenzione codice: "+ls_cod_tipo_manutenzione +" " + sqlca.sqlerrtext,stopsign!)
								setpointer(arrow!)
								return -1
								
							end if
							
							
							//--------FINE CLAUDIA 21/01/08
						else
						// *** cancello le scadenze ancora da confermare e continuo con il periodo scritto 
						//     dall'operatore
							if wf_cancella_manutenzioni(ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_errore)<0 then
								g_mb.error("Omnia",ls_errore)
								rollback;
								setpointer(arrow!)
								return -1
							end if							

						end if
					end if
						
				end if
	
				// ***
				
				// michela 14/09/2005: controllo che i due intervalli di tempo della tipologia non siano nulli
					
				int mese1_i, mese1_f, mese2_i, mese2_f, giorno1_i, giorno1_f, giorno2_i, giorno2_f, giorno_inizio, mese_inizio, giorno_fine, mese_fine, &
				    mese_effettivo, anno_effettivo, giorno_effettivo
					 
				date ldt_data_partenza, ldt_data_arrivo, ldt_appo
										
				giorno1_i = ( ld_data_inizio_val_1 / 100)
				mese1_i = mod( ld_data_inizio_val_1, 100)						
				giorno1_f = ( ld_data_fine_val_1 / 100)
				mese1_f = mod( ld_data_fine_val_1, 100)
				
				//Donato 11/11/2008
				giorno2_i = ( ld_data_inizio_val_2 / 100)
				mese2_i = mod( ld_data_inizio_val_2, 100)										
				giorno2_f = ( ld_data_fine_val_2 / 100)
				mese2_f = mod( ld_data_fine_val_2, 100)
				//--------------------------------------
				
				if ( not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 )   or   (not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0 and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0) then
					//Donato 11/11/2008
					if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 &
						and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 &
						and not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0  &
						and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 then
						
						//sono stati specificati tutti e due gli intervalli
						ldt_scadenze[] = ldt_scadenze_vuoto[]

						luo_manutenzioni.uof_scadenze_periodo(ldt_da_data , date(ldt_da_data), date(ldt_a_data), &
																					ld_data_inizio_val_1, ld_data_fine_val_1, &
																					ld_data_inizio_val_2, ld_data_fine_val_2, &
																					ls_periodicita, ll_frequenza, ldt_scadenze)
						
						if UpperBound(ldt_scadenze) < 1 then continue
						
						for ll_ii = 1 to UpperBound(ldt_scadenze)
							ldt_data_registrazione = ldt_scadenze[ll_ii]
							if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(ll_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, ls_messaggio) <> 0 then
									g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
									rollback;
									setpointer(arrow!)
									return -1
							end if
						next
						//--------------------------------------
					else
						//fai quello che facevi prima
						if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 then
							giorno_inizio = giorno1_i
							mese_inizio = mese1_i
							giorno_fine = giorno1_f
							mese_fine = mese1_f
						else
							giorno_inizio = giorno2_i
							mese_inizio = mese2_i
							giorno_fine = giorno2_f
							mese_fine = mese2_f						
						end if
						
						if year(date(ldt_da_data)) = year(date(ldt_a_data)) then // sono nello stesso anno
						
							if month(date(ldt_da_data)) <= mese_inizio then
								
								mese_effettivo = mese_inizio
								anno_effettivo = year(date(ldt_da_data))
								
								if day(date(ldt_da_data)) <= giorno_inizio then
									giorno_effettivo = giorno_inizio
								elseif day(date(ldt_da_data)) > giorno_inizio then
									giorno_effettivo = day(date(ldt_da_data))
								end if
								
								ldt_data_partenza = date(string(anno_effettivo) + "-" + string(mese_effettivo) + "-" + string(giorno_effettivo))
								
								if month(date(ldt_a_data)) <= mese_fine then
									mese_effettivo = month(date(ldt_a_data))
								else
									mese_effettivo = mese_fine
								end if
								
								if day(date(ldt_a_data)) <= giorno_fine then
									giorno_effettivo = day(date(ldt_a_data))
								else
									giorno_effettivo = giorno_fine
								end if								
	
								ldt_data_arrivo = date(string(anno_effettivo) + "-" + string(mese_effettivo) + "-" + string(giorno_effettivo))
								
								ldt_scadenze[] = ldt_scadenze_vuoto[]
				
								luo_manutenzioni.uof_scadenze_periodo( datetime(ldt_data_partenza, 00:00:00) , ldt_data_partenza, ldt_data_arrivo, ls_periodicita, ll_frequenza, ldt_scadenze)
								
								if UpperBound(ldt_scadenze) < 1 then continue
								
								for ll_ii = 1 to UpperBound(ldt_scadenze)
									
									ldt_data_registrazione = ldt_scadenze[ll_ii]
									
									if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(ll_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, ls_messaggio) <> 0 then
											g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
											rollback;
											setpointer(arrow!)
											return -1
									end if
									
								next							
								
							elseif month(date(ldt_da_data)) > mese_inizio then        
								
								// non posso fare nulla perchè sono nello stesso anno con un intervallo maggiore a quello consentito
								continue
								
							end if					
						else
							
							if wf_calcola_scadenze(giorno_inizio, mese_inizio, giorno_fine, mese_fine, ldt_da_data, ldt_a_data, ls_periodicita, ll_frequenza, ll_anno_registrazione, ll_num_registrazione) < 0 then 
								rollback;
								setpointer(arrow!)
								return -1							
							end if
							
						end if
					end if
					
				else						//********************************************* CASO SENZA INTERVALLI
					
					ldt_scadenze[] = ldt_scadenze_vuoto[]
	
					luo_manutenzioni.uof_scadenze_periodo( ldt_da_data , date(ldt_da_data), date(ldt_a_data), ls_periodicita, ll_frequenza, ldt_scadenze)
					
					if UpperBound(ldt_scadenze) < 1 then continue
					
					for ll_ii = 1 to UpperBound(ldt_scadenze)
						
						ldt_data_registrazione = ldt_scadenze[ll_ii]
						
						if luo_manutenzioni.uof_crea_manutenzione_programmata_periodo(ll_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, ls_messaggio) <> 0 then
								g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
								rollback;
								setpointer(arrow!)
								return -1
						end if
						
					next
					
				end if
				
				

		end choose		
	end if	
		
next 
	 
destroy luo_manutenzioni

//spostato fuori dalla funzione
//commit;
	
setpointer(arrow!)
	
if lb_inserimento = true then
	dw_ext_prog_manut.object.messaggio_t.text = "Programmazione avvenuta con successo!"
end if
	
return 1
end function

public subroutine wf_attrezzature_ricerca_da ();

guo_ricerca.uof_ricerca_attrezzatura(pcca.window_currentdw, "cod_attr_da")
end subroutine

public subroutine wf_attrezzature_ricerca_a ();
guo_ricerca.uof_ricerca_attrezzatura(pcca.window_currentdw, "cod_attr_a")
end subroutine

public subroutine wf_annulla_filtro ();string ls_null

setnull(ls_null)
dw_sel_elenco_attrez.setitem(1,"flag_prog","N")
dw_sel_elenco_attrez.setitem(1,"flag_parziali","S")
dw_sel_elenco_attrez.setitem(1,"flag_non_prog","S")
dw_sel_elenco_attrez.setitem(1,"flag_interrotte","S")
dw_sel_elenco_attrez.setitem(1,"cod_reparto",ls_null)
dw_sel_elenco_attrez.setitem(1,"cod_categoria",ls_null)
dw_sel_elenco_attrez.setitem(1,"cod_area",ls_null)
dw_sel_elenco_attrez.setitem(1,"cod_attr_da",ls_null)
dw_sel_elenco_attrez.setitem(1,"cod_attr_a",ls_null)
dw_sel_elenco_attrez.setitem(1,"cod_divisione",ls_null)		//lo faccio sempre, tanto al più è solo invisibile se ib_global_Service è TRUE
end subroutine

public subroutine wf_ricerca_filtro ();setpointer(hourglass!)
wf_carica()
dw_sel_elenco_attrez.object.messaggio_t.text = "Pronto!"
setpointer(arrow!)
end subroutine

public subroutine wf_prosegui ();long				ll_index
string				ls_selezionato
boolean			lb_prosegui = false

//controlla di aver selezionato almeno una tipologia da programmare
for ll_index=1 to dw_tipi_man_attrezzature.rowcount()
	ls_selezionato = dw_tipi_man_attrezzature.getitemstring(ll_index, "selezionato")
	
	if ls_selezionato="S" then
		//almeno una è selezionata, prosegui
		lb_prosegui = true
		exit
	end if
next

if not lb_prosegui then
	//nessuna selezionata
	g_mb.error("OMNIA", "E' necessario selezionare almeno una tipologia di manutenzione da programmare!")
	return
end if

if not g_mb.confirm("OMNIA", "Proseguire con la programmazione (verranno richieste ulteriori conferme)?", 1) then
	return
end if


//carica la dw con i dati selezionati
dw_tipi_man_attr_selezionate.reset()
dw_tipi_man_attr_selezionate.setredraw(false)

dw_tipi_man_attrezzature.RowsCopy(1, dw_tipi_man_attrezzature.rowcount(), Primary!, dw_tipi_man_attr_selezionate, 1, Primary!)

dw_tipi_man_attr_selezionate.setfilter("selezionato = 'S'")
dw_tipi_man_attr_selezionate.filter()

dw_tipi_man_attr_selezionate.setredraw(true)

dw_folder.fu_enabletab(3)
dw_folder.fu_selecttab(3)

dw_folder.fu_disabletab(4)
dw_folder.fu_disabletab(2)
dw_folder.fu_disabletab(1)

return
end subroutine

public subroutine wf_riseleziona ();

dw_folder.fu_enabletab(2)
dw_folder.fu_enabletab(1)
dw_folder.fu_selecttab(2)

dw_folder.fu_disabletab(4)
dw_folder.fu_disabletab(3)


end subroutine

public function integer wf_inserisci_manut_temporanea ();string					ls_sql, ls_cod_attrezzatura_cu, ls_cod_tipo_manutenzione_cu
datastore			lds_programmi, lds_manutenzioni
long					ll_prog_cu, ll_anno_reg_programma_cu, ll_num_reg_programma_cu, ll_index, ll_tot, &
						ll_index_2, ll_tot_2, ll_anno_reg_man_cu, ll_num_reg_man_cu
datetime				ldt_data_prog_cu, ldt_ora_prog_cu

ls_sql = "select  progressivo,"+&
					"anno_programma,"+&
		 			"num_programma,"+&
					"cod_attrezzatura,"+&
					"cod_tipo_manutenzione,"+&
					"data_programmazione,"+&
					"ora_programmazione "+&
			"from temp_programmazione "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_utente='"+s_cs_xx.cod_utente+"' and "+&
						"prog_programmazione="+string(il_prog_programmazione)+" and "+&
						"flag_tipo='P' "

if not f_crea_datastore(lds_programmi,ls_sql) then
	//messaggio già dato
	return -1
end if

ll_tot = lds_programmi.retrieve()
for ll_index=1 to ll_tot
	ll_prog_cu							= lds_programmi.getitemnumber(ll_index, 1)
	ll_anno_reg_programma_cu		= lds_programmi.getitemnumber(ll_index, 2)
	ll_num_reg_programma_cu			= lds_programmi.getitemnumber(ll_index, 3)
	ls_cod_attrezzatura_cu			= lds_programmi.getitemstring(ll_index, 4)
	ls_cod_tipo_manutenzione_cu	= lds_programmi.getitemstring(ll_index, 5)
	ldt_data_prog_cu					= lds_programmi.getitemdatetime(ll_index, 6)
	ldt_ora_prog_cu					= lds_programmi.getitemdatetime(ll_index, 7)
	
	ls_sql = "select anno_registrazione,"+&
						"num_registrazione "+&
				"from manutenzioni "+&
				"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"anno_reg_programma="+string(ll_anno_reg_programma_cu) + " and "+&
							"num_reg_programma="+string(ll_num_reg_programma_cu) + "  "
	
	if not f_crea_datastore(lds_manutenzioni,ls_sql) then
		//messaggio già dato
		destroy lds_programmi;
		return -1
	end if
	
	ll_tot_2 = lds_manutenzioni.retrieve()
	for ll_index_2=1 to ll_tot_2
		ll_anno_reg_man_cu = lds_manutenzioni.getitemnumber(ll_index_2, 1)
		ll_num_reg_man_cu = lds_manutenzioni.getitemnumber(ll_index_2, 2)
		
		if wf_inserisci_temporanea(ll_prog_cu, ll_anno_reg_programma_cu, ll_num_reg_programma_cu, "M", &
										ll_anno_reg_man_cu, ll_num_reg_man_cu, ls_cod_attrezzatura_cu, ls_cod_tipo_manutenzione_cu) < 0 then
			//messaggi già dati
			destroy lds_manutenzioni;
			destroy lds_programmi;
			return -1
		end if
		
	next
	destroy lds_manutenzioni;
	
next

destroy lds_programmi;

return 1
end function

public function integer wf_inserisci_temporanea (long fl_prog_padre, long fl_anno_programma, long fl_num_programma, string fs_flag_tipo, long fl_anno_manut, long fl_num_manut, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione);long ll_progressivo

if isnull(il_prog_programmazione) or il_prog_programmazione<=0 or &
	isnull(idt_data_programmazione) or isnull(idt_ora_programmazione) then
	
	g_mb.error("Le variabili temporanee (progressivo, data e ora programmazione) non sono state valorizzate!")
	rollback;
	return -1
end if

select max(progressivo)
into :ll_progressivo
from temp_programmazione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente and
			prog_programmazione = :il_prog_programmazione;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura MAX prog. temporanea!", sqlca)
	rollback;
	return -1
end if

if isnull(ll_progressivo) then ll_progressivo = 0
ll_progressivo += 1


insert into temp_programmazione
(		  cod_azienda,
		  cod_utente,
		  prog_programmazione,
		  progressivo,
		  progressivo_padre,
		  anno_programma,
		  num_programma,
		  anno_manut,
		  num_manut,
		  flag_tipo,
		  cod_attrezzatura,
		  cod_tipo_manutenzione,
		  data_programmazione,
		  ora_programmazione)
values (		:s_cs_xx.cod_azienda,
				:s_cs_xx.cod_utente,
				:il_prog_programmazione,
				:ll_progressivo,
				:fl_prog_padre,
				:fl_anno_programma,
				:fl_num_programma,
				:fl_anno_manut,
				:fl_num_manut,
				:fs_flag_tipo,
				:fs_cod_attrezzatura,
				:fs_cod_tipo_manutenzione,
				:idt_data_programmazione,
				:idt_ora_programmazione);
				
if sqlca.sqlcode<0 then
	g_mb.error("Errore inserimento in temporanea!", sqlca)
	rollback;
	return -1
end if

return 1
				
end function

public function integer wf_risultati_programmazione (long fl_prog_programmazione, ref string fs_errore);string			ls_sql, ls_cod_attrezz_cu, ls_cod_tipo_man_cu, ls_periodicita_cu, ls_flag_eseguito_cu, ls_appo, &
				ls_sql_livello_1, ls_scadenza, ls_piano, ls_hold
datastore	lds_piani, lds_livello_1
long			ll_tot, ll_index, ll_num_scadenze_per_riga, ll_new, ll_prossimo_indice_a_capo, ll_indice_colonna, &
				ll_anno_prog_cu, ll_num_prog_cu, ll_anno_man_cu, ll_num_man_cu, ll_index_livello_1, ll_tot_livello_1
decimal		ld_frequenza_cu
datetime		ldt_data_programma_cu, ldt_data_man_cu
boolean		lb_nuova_riga

dw_risultati.reset()

dw_risultati.setredraw(false)

//leggo quante cod_attrezzatura-cod_tipo_manutenzione sono interessate dall'ultima programmazione
ls_sql_livello_1 = "select distinct "+&
							"cod_attrezzatura,"+&
							"cod_tipo_manutenzione "+&
						"from temp_programmazione "+&
						"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_utente='"+s_cs_xx.cod_utente+"' and "+&
								"prog_programmazione="+string(fl_prog_programmazione)+" "+&
						"order by cod_attrezzatura,cod_tipo_manutenzione "

if not f_crea_datastore(lds_livello_1, ls_sql_livello_1) then
	fs_errore = "Errore creazione datastore attrezzature/tipologie programmate! Operazione annullata!"
	dw_risultati.setredraw(true)
	return -1
end if

ll_tot_livello_1 = lds_livello_1.retrieve()

for ll_index_livello_1=1 to ll_tot_livello_1
	
	//leggo la coppia attrezzatura/tipo manutenzione
	ls_cod_attrezz_cu = lds_livello_1.getitemstring(ll_index_livello_1, 1)
	ls_cod_tipo_man_cu = lds_livello_1.getitemstring(ll_index_livello_1, 2)
	
	ls_sql = "SELECT "+&
					"programmi_manutenzione.anno_registrazione,"+&
					"programmi_manutenzione.num_registrazione,"+&
					"programmi_manutenzione.periodicita,"+&
					"programmi_manutenzione.frequenza,"+&
					"programmi_manutenzione.data_creazione,"+&
					"manutenzioni.anno_registrazione,"+&
					"manutenzioni.num_registrazione,"+&
					"manutenzioni.data_registrazione,"+&
					"manutenzioni.flag_eseguito "+&
			 "FROM manutenzioni "+&
			 "JOIN programmi_manutenzione ON 	programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and "+&
											"programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and "+&
											"programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma "+&
			"WHERE 	programmi_manutenzione.cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
							"programmi_manutenzione.cod_attrezzatura = '"+ls_cod_attrezz_cu+"' and "+&
							"programmi_manutenzione.cod_tipo_manutenzione = '"+ls_cod_tipo_man_cu+"' "+&
			"ORDER BY programmi_manutenzione.anno_registrazione DESC,"+&
						  "programmi_manutenzione.num_registrazione DESC,"+&
						  "manutenzioni.data_registrazione ASC "
	
	if not f_crea_datastore(lds_piani, ls_sql) then
		fs_errore = "Errore creazione datastore!"
		dw_risultati.setredraw(true)
		destroy lds_livello_1;
		return -1
	end if
	
	ll_tot = lds_piani.retrieve()
	
	//sistema le scadenze nella stessa riga ogni 5 date
	ll_num_scadenze_per_riga = 5
	ll_prossimo_indice_a_capo = 1		//al primo dato fai insert row comunque
	
	ll_indice_colonna = 0
	
	for ll_index=1 to ll_tot
		
		//leggi da datastore ----------------------------------------------
		ll_anno_prog_cu = lds_piani.getitemnumber(ll_index, 1)
		ll_num_prog_cu = lds_piani.getitemnumber(ll_index, 2)
		ls_periodicita_cu = lds_piani.getitemstring(ll_index, 3)
		ld_frequenza_cu = lds_piani.getitemdecimal(ll_index, 4)
		ldt_data_programma_cu = lds_piani.getitemdatetime(ll_index, 5)
		ll_anno_man_cu = lds_piani.getitemnumber(ll_index, 6)
		ll_num_man_cu = lds_piani.getitemnumber(ll_index, 7)
		ldt_data_man_cu = lds_piani.getitemdatetime(ll_index, 8)
		ls_flag_eseguito_cu = lds_piani.getitemstring(ll_index, 9)
		
		ls_piano = "Programma n° "+string(ll_anno_prog_cu)+"/"+string(ll_num_prog_cu)+&
																						" del "+string(ldt_data_programma_cu, "dd/mm/yy")
		
		
		lb_nuova_riga = false
		ll_indice_colonna += 1
		
		if ll_indice_colonna > 5 or ll_index=1 or ls_hold <> ls_piano then
			//vai a capo
			ll_new = dw_risultati.insertrow(0)
			lb_nuova_riga = true
			if ll_index <> 1 then ll_indice_colonna = 1
			
			ls_hold = ls_piano
			
		end if
		
		
		//fai setitem
		if lb_nuova_riga then
			
			dw_risultati.setitem(ll_new, "cod_attrezzatura", ls_cod_attrezz_cu)
			dw_risultati.setitem(ll_new, "cod_tipo_manutenzione", ls_cod_tipo_man_cu)
			dw_risultati.setitem(ll_new, "piano_manutenzione", ls_piano)
			choose case ls_periodicita_cu
				case "M"
					ls_appo = "Minuti"
				case "O"
					ls_appo = "Ore"
				case "G"
					ls_appo = "Giorni"
				case "S"
					ls_appo = "Settimane"
				case "E"
					ls_appo = "Mesi"
				case "B"
					ls_appo = "Nr.Battute"
				case else
					ls_appo = "???"
			end choose
			dw_risultati.setitem(ll_new, "periodicita", ls_appo + ": "+string(ld_frequenza_cu, "#####0"))
		end if
	
		if ls_flag_eseguito_cu="S" then
			//ls_scadenza = " "+ string(ldt_data_man_cu, "dd/mm/yyyy") + "    OK    "//"Eseguita!"
			ls_appo = " " 	//"Eseguita!"
		else
			//ls_scadenza = "*"+ string(ldt_data_man_cu, "dd/mm/yyyy") + " da eseguire"//"Aperta"
			ls_appo = "*"	//"Aperta"
		end if
	
		dw_risultati.setitem(ll_new, "scadenza_" + string(ll_indice_colonna), ls_appo+string(ldt_data_man_cu, "dd/mm/yyyy"))
		//dw_risultati.setitem(ll_new, "scadenza_" + string(ll_indice_colonna), ls_scadenza)
		
		dw_risultati.setitem(ll_new, "anno_man_" + string(ll_indice_colonna), ll_anno_man_cu)
		dw_risultati.setitem(ll_new, "num_man_" + string(ll_indice_colonna), ll_num_man_cu)
		
	next
	destroy lds_piani;
	
next

destroy lds_livello_1;

dw_risultati.sort()
dw_risultati.groupcalc()

dw_risultati.object.datawindow.print.preview = "yes"
dw_risultati.setredraw(true)

return 1
end function

public function integer wf_cancella_manutenzioni (string ls_cod_attrezzatura, string ls_cod_tipo_manutenzione, ref string fs_errore);//cancella le tabelle di manutenzione relative ad una attrezzatura e ad un tpo manutenzione
//usata in caso di programmazione per periodo con flag considera scadenze esistenti posto a SI

long					ll_anno_reg_man_cu, ll_num_reg_man_cu, ll_index, ll_tot
string					ls_sql_manut, ls_manut
datastore			lds_man_da_cancellare

//select manutenzioni aperte
ls_sql_manut = "select 	manutenzioni.anno_registrazione,"+&
								"manutenzioni.num_registrazione "+&
					"from manutenzioni "+&
					"join programmi_manutenzione on  programmi_manutenzione.cod_azienda = manutenzioni.cod_azienda and "+&
																"programmi_manutenzione.anno_registrazione = manutenzioni.anno_reg_programma and "+&
																"programmi_manutenzione.num_registrazione = manutenzioni.num_reg_programma "+&
					"where manutenzioni.cod_azienda = '"+s_cs_xx.cod_azienda +"' and "+&
							 "programmi_manutenzione.cod_attrezzatura = '"+ls_cod_attrezzatura+"' and "+&
							 "programmi_manutenzione.cod_tipo_manutenzione = '"+ls_cod_tipo_manutenzione+"' and "+&
							 "manutenzioni.flag_eseguito = 'N' "

if not f_crea_datastore(lds_man_da_cancellare, ls_sql_manut) then
	//messaggio già dato
	fs_errore = "Errore datastore!"
	return -1
end if

ll_tot = lds_man_da_cancellare.retrieve()

for ll_index=1 to ll_tot
	ll_anno_reg_man_cu = lds_man_da_cancellare.getitemnumber(ll_index, 1)
	ll_num_reg_man_cu = lds_man_da_cancellare.getitemnumber(ll_index, 2)
	
	ls_manut = " "+string(ll_anno_reg_man_cu)+ "/" +string(ll_num_reg_man_cu)+" "
	
	if ib_global_service then
	
		//elimina fasi_risorse eventuali
		delete from manutenzioni_fasi_risorse
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg_man_cu and
				num_registrazione=:ll_num_reg_man_cu;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore eliminazione fasi risorse esterne manutenzione "+ls_manut+" : "+sqlca.sqlerrtext
			destroy lds_man_da_cancellare;
			
			return -1
		end if
		
		//elimina fasi_operai eventuali
		delete from manutenzioni_fasi_operai
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg_man_cu and
				num_registrazione=:ll_num_reg_man_cu;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore eliminazione fasi operai manutenzione "+ls_manut+" : "+sqlca.sqlerrtext
			destroy lds_man_da_cancellare;
			
			return -1
		end if
		
		//elimina fasi eventuali
		delete from manutenzioni_fasi
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg_man_cu and
				num_registrazione=:ll_num_reg_man_cu;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore eliminazione fasi manutenzione "+ls_manut+" : "+sqlca.sqlerrtext
			destroy lds_man_da_cancellare;
			
			return -1
		end if
		
	end if
	//fine global service
	
	
	//elimina manutenzioni_ricambi eventuali
	delete from manutenzioni_ricambi
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg_man_cu and
			num_registrazione=:ll_num_reg_man_cu;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore eliminazione manutenzioni ricambi manutenzione "+ls_manut+" : "+sqlca.sqlerrtext
		destroy lds_man_da_cancellare;
		
		return -1
	end if
	
	//caso non global service  ##########

	//elimina manutenzioni_operai eventuali
	delete from manutenzioni_operai
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg_man_cu and
			num_registrazione=:ll_num_reg_man_cu;

	//elimina manutenzioni_risorse_esterne eventuali
	delete from manutenzioni_risorse_esterne
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg_man_cu and
			num_registrazione=:ll_num_reg_man_cu;

	//elimina manutenzioni_avvisi eventuali
	delete from manutenzioni_avvisi
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg_man_cu and
			num_registrazione=:ll_num_reg_man_cu;

	//elimina manutenzioni_avvisi eventuali
	delete from manutenzioni_destinatari
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg_man_cu and
			num_registrazione=:ll_num_reg_man_cu;
	
	//#########################
	
	//elimina manutenzione
	delete from manutenzioni
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg_man_cu and
			num_registrazione=:ll_num_reg_man_cu;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore eliminazione manutenzione "+ls_manut+" : "+sqlca.sqlerrtext
		destroy lds_man_da_cancellare;
		
		return -1
	end if

next

destroy lds_man_da_cancellare;

return 1
end function

public function integer wf_annulla_programmazione (ref string fs_errore);/*

il problema è dovuto al fatto che in alcuni casi, su programmazione per periodo
con flag considera scadenze esistenti posto a NO, la programmazione
cancella le manutenzioni aperte
Come fare in tali casi a ripristinarle in caso di annullamento?????

In tal caso l'utente è stato già avvisato, prima di confermare la programmazione
che tali interventi aperti non saranno ripristinati, in caso di successivo annullamento programmazione

*/

string					ls_sql
datastore			lds_data
long					ll_tot, ll_index, ll_anno_cu, ll_num_cu

//se sei qui procedi all'annullamento

//leggo le manutenzioni dell'ultima programmazione
ls_sql = 				"select "+&
							"anno_manut,"+&
							"num_manut "+&
						"from temp_programmazione "+&
						"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_utente='"+s_cs_xx.cod_utente+"' and "+&
								"prog_programmazione="+string(il_prog_programmazione)+" and "+&
								"flag_tipo='M' "+&
						"order by anno_manut,num_manut "

if not f_crea_datastore(lds_data, ls_sql) then
	fs_errore = "Errore creazione datastore manutenzioni programmate! Operazione annullata!"
	
	return -1
end if

ll_tot = lds_data.retrieve()

for ll_index=1 to ll_tot
	ll_anno_cu = lds_data.getitemnumber(ll_index, 1)
	ll_num_cu = lds_data.getitemnumber(ll_index, 2)
	
	if ib_global_service then
		
		//elimina le fasi operaio
		delete from manutenzioni_fasi_operai
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_cu and
				num_registrazione=:ll_num_cu;
				
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in cancellazione fasi operai: "+sqlca.sqlerrtext
			destroy lds_data;
			
			return -1
		end if
		
		//elimina le fasi risorse esterne
		delete from manutenzioni_fasi_risorse
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_cu and
				num_registrazione=:ll_num_cu;
				
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in cancellazione fasi risorse esterne: "+sqlca.sqlerrtext
			destroy lds_data;
			
			return -1
		end if
		
		//elimina le fasi
		delete from manutenzioni_fasi
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_cu and
				num_registrazione=:ll_num_cu;
				
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in cancellazione fasi: "+sqlca.sqlerrtext
			destroy lds_data;
			
			return -1
		end if
		
	end if
	//fine global service
	
	//elimina ricambi
	delete from manutenzioni_ricambi
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione ricambi: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
	//elimina manutenzione
	delete from manutenzioni
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione manutenzione: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
next

destroy lds_data;

//leggo i piani dell'ultima programmazione ---------------
ls_sql = 				"select "+&
							"anno_programma,"+&
							"num_programma "+&
						"from temp_programmazione "+&
						"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_utente='"+s_cs_xx.cod_utente+"' and "+&
								"prog_programmazione="+string(il_prog_programmazione)+" and "+&
								"flag_tipo='P' "+&
						"order by anno_programma,num_programma "

if not f_crea_datastore(lds_data, ls_sql) then
	fs_errore = "Errore creazione datastore piani programmazione! Operazione annullata!"
	
	return -1
end if

ll_tot = lds_data.retrieve()

for ll_index=1 to ll_tot
	ll_anno_cu = lds_data.getitemnumber(ll_index, 1)
	ll_num_cu = lds_data.getitemnumber(ll_index, 2)
	
	//elimina prog_manutenzioni_ricambi
	delete from prog_manutenzioni_ricambi
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione prog manut. ricambi: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
	//elimina det_prog_manutenzioni
	delete from det_prog_manutenzioni
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione det. prog manutenzioni: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
	//elimina piano manutenzione
	delete from programmi_manutenzione
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione piano manutenzione: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
next

return 1


end function

public function integer wf_controlla_attrezzatura (datastore ads_data, string as_cod_attrezzatura, ref string as_errore);long				ll_count, ll_index, ll_conteggio, ll_count_aperte, ll_tot_tipi_man
string				ls_where_tipi_man, ls_sql, ls_sql_base, ls_cod_tipo_man
datastore		lds_manutenzioni

//se entri in questa funzione vuol dire che ogni tipologia manutenzione ha almeno un piano
	
	//prima di stabilire che sia completamente programmata, verifica caso mai sia da considerarsi interrotta o parzialmente programmata
	//infatti può essere che pur avendo ogni tipologia di manutenzione il rispettivo piano:
	//		1) nessuna tipologia dell'attrezzatura abbia manutenzione aperte -> CASO INTERROTTA (ret 3)
	//		2) tutte le tipologie hanno manutenzioni aperte -> CASO COMPLETAMENTE PROGRAMMATA (ret 2)
	//		3) altrimenti esiste almeno una tipologia senza manutenzioni aperte ed almeno una con manutenzioni aperte -> CASO PARZIALMENTE PROGRAMMATA (ret 1)
	//		

//NOTA: ll_tot_tipi_man non può essere zero in quanto vengono elaborate solo attrezzature con almeno un tipo manutenzione ...
ll_tot_tipi_man = ads_data.rowcount()


ls_where_tipi_man = " where cod_azienda='"+s_cs_xx.cod_azienda+"' and cod_attrezzatura='"+as_cod_attrezzatura+"' and cod_tipo_manutenzione in ("

for ll_index=1 to ll_tot_tipi_man
	ls_cod_tipo_man = ads_data.getitemstring(ll_index, 1)
	
	if ll_index>1 then ls_where_tipi_man+= ","
	ls_where_tipi_man += "'"+ls_cod_tipo_man+"'"

next

ls_where_tipi_man += ")"

ls_sql_base = "select count(*) from manutenzioni " + ls_where_tipi_man

ls_sql = ls_sql_base + " and flag_eseguito='N' "
ll_count = guo_functions.uof_crea_datastore(lds_manutenzioni, ls_sql, as_errore)

//ll_count non può essere zero
if ll_count>0 then
	//count manutenzioni aperte dell'attrezzatura e per tutte le tipologie ad essa associate
	ll_count = lds_manutenzioni.getitemnumber(1, 1)
	
	destroy lds_manutenzioni
	
	if ll_count=0 or isnull(ll_count) then
		//nessuna tipologia dell'attrezzatura ha manutenzione aperte
		//CONSIDERA L'ATTREZZATURA COME INTERROTTA
		return 3
	end if
	
	ll_count_aperte = 0
	//ll_tot_tipi_man non può essere zero
	for ll_index=1 to  ll_tot_tipi_man
		ls_cod_tipo_man = ads_data.getitemstring(ll_index, 1)
		
		select count(*)
		into :ll_count
		from manutenzioni
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_attrezzatura=:as_cod_attrezzatura and
					cod_tipo_manutenzione=:ls_cod_tipo_man and
					flag_eseguito='N';
					
		if sqlca.sqlcode<0 then
			as_errore = sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ll_count) then ll_count=0
		
		if ll_count>0 then
			//ci manutenzioni aperte per questa tipologia
			ll_count_aperte += 1
		else
			//per questa tipologia non ci sono manutenzioni aperte
		end if
	next
	
	if ll_tot_tipi_man = ll_count_aperte then
		//ogni tipologia ha manutenzioni aperte
		//CONSIDERA L'ATTREZZATURA COME COMPLETAMENTE PROGRAMMATA
		return 2
	else
		//CONSIDERA L'ATTREZZATURA COME COMPLETAMENTE PARZIALMENTE PROGRAMMATA
		return 1
	end if
	
else
	//c'è un errore
	return -1
end if




	
	
end function

public function string wf_where_attrezzature ();string			ls_da, ls_a, ls_ret

ls_ret = ""

ls_da = dw_sel_elenco_attrez.getitemstring(1, "cod_attr_da")
ls_a = dw_sel_elenco_attrez.getitemstring(1, "cod_attr_a")

if ls_da <> "" and not isnull(ls_da) then
	ls_ret += " cod_attrezzatura>= '"+ls_da+"'"
end if

if ls_a <> "" and not isnull(ls_a) then
	if ls_ret<>"" then ls_ret += " and"
	
	ls_ret += " cod_attrezzatura<= '"+ls_a+"'"
end if

return ls_ret
end function

on w_programmazione_attrezzature.create
int iCurrent
call super::create
this.dw_sel_elenco_attrez=create dw_sel_elenco_attrez
this.dw_risultati=create dw_risultati
this.dw_tipi_man_attr_selezionate=create dw_tipi_man_attr_selezionate
this.dw_piani_esistenti=create dw_piani_esistenti
this.dw_tipi_man_attrezzature=create dw_tipi_man_attrezzature
this.dw_folder=create dw_folder
this.dw_ext_prog_manut=create dw_ext_prog_manut
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_elenco_attrez
this.Control[iCurrent+2]=this.dw_risultati
this.Control[iCurrent+3]=this.dw_tipi_man_attr_selezionate
this.Control[iCurrent+4]=this.dw_piani_esistenti
this.Control[iCurrent+5]=this.dw_tipi_man_attrezzature
this.Control[iCurrent+6]=this.dw_folder
this.Control[iCurrent+7]=this.dw_ext_prog_manut
end on

on w_programmazione_attrezzature.destroy
call super::destroy
destroy(this.dw_sel_elenco_attrez)
destroy(this.dw_risultati)
destroy(this.dw_tipi_man_attr_selezionate)
destroy(this.dw_piani_esistenti)
destroy(this.dw_tipi_man_attrezzature)
destroy(this.dw_folder)
destroy(this.dw_ext_prog_manut)
end on

event pc_setwindow;call super::pc_setwindow;windowobject					lw_oggetti[], lw_vuoto[]
string								ls_flag, ls_cod_attrezzatura


try
	ls_cod_attrezzatura = message.stringparm
catch (runtimeerror err)
end try


set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_sel_elenco_attrez.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
						c_newonopen + &
						c_noretrieveonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_ext_prog_manut.set_dw_options(sqlca, &
                   pcca.null_object, &
                   c_nomodify + &
                   c_nodelete + &
	   			 c_newonopen + &
				c_noretrieveonopen + &
                   c_disableCC, &
                   c_noresizedw + &
                   c_nohighlightselected + &
                   c_cursorrowpointer)

dw_piani_esistenti.settransobject(sqlca)

lw_oggetti[1] = dw_sel_elenco_attrez
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_tipi_man_attrezzature
lw_oggetti[2] = dw_piani_esistenti
dw_folder.fu_assigntab(2, "Selezione", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_ext_prog_manut
lw_oggetti[2] = dw_tipi_man_attr_selezionate
dw_folder.fu_assigntab(3, "Programmazione", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_risultati
dw_folder.fu_assigntab(4, "Risultati", lw_oggetti[])

dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

dw_folder.fu_disabletab(3)
dw_folder.fu_disabletab(4)

move(600,0)

this.height = w_cs_xx_mdi.mdi_1.height - 50

wf_impostazioni()
wf_annulla_filtro()

//---------------------------------------------------
setnull(il_prog_programmazione)
setnull(idt_data_programmazione)
setnull(idt_ora_programmazione)
//---------------------------------------------------

postevent("ue_resize")

//controllo se sono in global service --------------------------
select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if sqlca.sqlcode = 0 and ls_flag ="S" then ib_global_service = true

if ib_global_service then
else
	dw_sel_elenco_attrez.object.cod_divisione.visible = false
	dw_sel_elenco_attrez.object.cf_cod_divisione.visible = false
	dw_sel_elenco_attrez.object.cod_divisione_t.visible = false
end if
//----------------------------------------------------------------

//controllo se ABOR ------------------------------------------
select flag
into :is_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

if sqlca.sqlcode <> 0 or len(is_flag_abr)<=0 then
	is_flag_abr = "N"
end if
//-----------------------------------------------------------------

if ls_cod_attrezzatura<>"" and not isnull(ls_cod_attrezzatura) then
	event post ue_imposta_filtro_attrezzature(ls_cod_attrezzatura)
end if


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_elenco_attrez,"cod_reparto",sqlca,"anag_reparti","cod_reparto", &
					  "des_reparto","cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_elenco_attrez,"cod_categoria",sqlca,"tab_cat_attrezzature","cod_cat_attrezzature", &
					  "des_cat_attrezzature","cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")
					  
f_PO_LoadDDDW_sort(dw_sel_elenco_attrez,"cod_area",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")

//if ib_global_service then
	f_po_loaddddw_dw(dw_sel_elenco_attrez,"cod_divisione",sqlca,"anag_divisioni","cod_divisione", &
				  "des_divisione","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//end if
end event

type dw_sel_elenco_attrez from uo_cs_xx_dw within w_programmazione_attrezzature
integer x = 37
integer y = 124
integer width = 3785
integer height = 992
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_sel_elenco_attrez_new"
end type

event itemchanged;call super::itemchanged;if i_extendmode and i_colname = "cod_attr_da" and not isnull(i_coltext) then
	setitem(row,"cod_attr_a",i_coltext)
end if
end event

event buttonclicked;call super::buttonclicked;string				ls_null

if row>0 then
	
	setnull(ls_null)
	
	choose case dwo.name
		case "b_sel_attrezz_da"
			wf_attrezzature_ricerca_da()
			
	
		case "b_sel_attrezz_a"
			wf_attrezzature_ricerca_a()
		
		
		case "b_annulla"
			wf_annulla_filtro()
			
			
		case "b_cerca"
			wf_ricerca_filtro()
			
			
		case "b_sel_tipoman"
			guo_ricerca.uof_set_where(wf_where_attrezzature())
			guo_ricerca.uof_ricerca_tipo_manutenzione(dw_sel_elenco_attrez, "cod_tipo_manutenzione")
			
		case "b_cancella_tipoman"
			setitem(row, "cod_tipo_manutenzione", ls_null)
		
	end choose
end if
end event

type dw_risultati from datawindow within w_programmazione_attrezzature
integer x = 37
integer y = 1264
integer width = 3643
integer height = 400
integer taborder = 80
boolean bringtotop = true
string title = "none"
string dataobject = "d_risultati_programmazione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;string ls_indice
long ll_anno_man, ll_num_man

if row>0 then
	choose case dwo.name
		case "scadenza_1", "scadenza_2", "scadenza_3", "scadenza_4", "scadenza_5", &
				"cf_1", "cf_2", "cf_3", "cf_4","cf_5"
				
			ls_indice = right(dwo.name, 1)
			
			ll_anno_man = getitemnumber(row, "anno_man_"+ls_indice)
			ll_num_man = getitemnumber(row, "num_man_"+ls_indice)
			
			wf_visualizza_manutenzione(ll_anno_man, ll_num_man)
			
	end choose
end if
end event

event clicked;string ls_errore, ls_cod_tipo_manutenzione, ls_cod_attrezzatura
long ll_row

choose case dwo.name
	case "t_nuovo"
		
		//esegui una'altra programmazione
		if g_mb.confirm("OMNIA", "Si desidera effettuare una nuova programmazione?", 1) then
			
			setnull(il_prog_programmazione)
			setnull(idt_data_programmazione)
			setnull(idt_ora_programmazione)
			
			//torna allo step 1
			dw_tipi_man_attrezzature.reset()
			dw_piani_esistenti.reset()
			dw_tipi_man_attr_selezionate.reset()
			
			dw_folder.fu_enabletab(2)
			dw_folder.fu_enabletab(1)
			dw_folder.fu_selecttab(1)		//torna al tab 1 per una nuova ricerca
			
			dw_folder.fu_disabletab(4)
			dw_folder.fu_disabletab(3)
		end if
		
		
	case "t_chiudi"
		
		//chiudi la finestra
		if g_mb.confirm("OMNIA", "Si desidera uscire dalla programmazione e chiudere questa finestra?", 1) then
			close(parent)
		end if
		
	case "t_annulla"
		if g_mb.confirm("OMNIA", "Si desidera annullare l'ultima programmazione eseguita?", 1) then
			//elimina gli inserimenti fatti
			if wf_annulla_programmazione(ls_errore)<0 then
				g_mb.error("OMNIA",ls_errore)
				
				rollback;
				return
			end if
			
			//commit dell'annulla
			commit;
			
			setnull(il_prog_programmazione)
			setnull(idt_data_programmazione)
			setnull(idt_ora_programmazione)
		
			//torna allo step 2 per una nuova selezione
			dw_tipi_man_attr_selezionate.reset()
			
			dw_folder.fu_enabletab(2)
			dw_folder.fu_enabletab(1)
			dw_folder.fu_selecttab(2)		//torna al tab 2 della precedente selezione
			
			dw_folder.fu_disabletab(4)
			dw_folder.fu_disabletab(3)
			
			//refresh sulla prima riga ------------------
			ll_row = dw_tipi_man_attrezzature.rowcount()
			if ll_row > 0 then

				ls_cod_tipo_manutenzione = dw_tipi_man_attrezzature.getitemstring(1, "cod_tipo_manutenzione")
				ls_cod_attrezzatura = dw_tipi_man_attrezzature.getitemstring(1, "cod_attrezzatura")
				
				dw_tipi_man_attrezzature.setrow(1)
				dw_tipi_man_attrezzature.scrolltorow(1)
				
				if len(ls_cod_tipo_manutenzione) > 0 and len(ls_cod_attrezzatura) > 0 then
					if wf_retrieve_piani_esistenti(ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_errore) < 0 then
						g_mb.error(ls_errore)
						return
					end if
				end if
			end if
			//----------------------------------------------
			
		end if
		
end choose

end event

type dw_tipi_man_attr_selezionate from datawindow within w_programmazione_attrezzature
integer x = 37
integer y = 856
integer width = 3785
integer height = 908
integer taborder = 70
boolean bringtotop = true
string title = "none"
string dataobject = "d_sel_tipi_man_attrez_prog"
boolean livescroll = true
end type

type dw_piani_esistenti from datawindow within w_programmazione_attrezzature
integer x = 37
integer y = 1320
integer width = 3785
integer height = 556
integer taborder = 50
boolean bringtotop = true
string title = "none"
string dataobject = "d_sel_piani_man_attrez_prog"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;string ls_indice
long ll_anno_man, ll_num_man

if row>0 then
	choose case dwo.name
		case "scadenza_1", "scadenza_2", "scadenza_3", "scadenza_4", "scadenza_5", &
				"cf_1", "cf_2", "cf_3", "cf_4","cf_5"
				
			ls_indice = right(dwo.name, 1)
			
			ll_anno_man = getitemnumber(row, "anno_man_"+ls_indice)
			ll_num_man = getitemnumber(row, "num_man_"+ls_indice)
			
			wf_visualizza_manutenzione(ll_anno_man, ll_num_man)
			
	end choose
end if
end event

type dw_tipi_man_attrezzature from datawindow within w_programmazione_attrezzature
integer x = 37
integer y = 124
integer width = 3785
integer height = 1164
integer taborder = 60
boolean bringtotop = true
string title = "none"
string dataobject = "d_sel_tipi_man_attrez_prog"
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;string ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_errore

if currentrow > 0 then

	ls_cod_tipo_manutenzione = getitemstring(currentrow, "cod_tipo_manutenzione")
	ls_cod_attrezzatura = getitemstring(currentrow, "cod_attrezzatura")
	
	scrolltorow(currentrow)
	
	if len(ls_cod_tipo_manutenzione) > 0 and len(ls_cod_attrezzatura) > 0 then
		if wf_retrieve_piani_esistenti(ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_errore) < 0 then
			g_mb.error(ls_errore)
			return
		end if
		
	end if
	
end if
end event

event buttonclicked;string ls_seleziona, ls_nuovo_testo, ls_messaggio, ls_appo
long ll_index

choose case dwo.name
	case "b_seleziona"
		
		if rowcount()>0 then
		else
			g_mb.error("OMNIA", "Nessuna tipologia manutenzione disponibile in lista!")
			return
		end if
		
		if upper(dwo.text) = upper("Sel.Tutte") then
			//si vogliono selezionare tutte le tipologie di tutte le attrezzature visualizzate
			ls_seleziona = "S"
			ls_nuovo_testo = "Desel.Tutte"
			ls_appo = "Selezionare "
		else
			//si vogliono de-selezionare tutte le tipologie di tutte le attrezzature visualizzate
			ls_seleziona = "N"
			ls_nuovo_testo = "Sel.Tutte"
			ls_appo = "Deselezionare "
		end if
		
		ls_messaggio = ls_appo + "tutte le tipologie di manutenzione per tutte le attrezzature mostrate?"
		if not g_mb.confirm("OMNIA", ls_messaggio, 1) then
			//operazione annullata
			return
		end if
		
		for ll_index=1 to rowcount()
			setitem(ll_index, "selezionato", ls_seleziona)
		next
		
		object.b_seleziona.text = ls_nuovo_testo
		
	
	case "b_prosegui"
		wf_prosegui()
		
end choose
end event

event clicked;string ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_errore

if row>0 then
	choose case dwo.name
		case"cf_tipo_manutenzione"
			
			ls_cod_tipo_manutenzione = getitemstring(row, "cod_tipo_manutenzione")
			ls_cod_attrezzatura = getitemstring(row, "cod_attrezzatura")
			
			scrolltorow(row)
			
			if len(ls_cod_tipo_manutenzione) > 0 and len(ls_cod_attrezzatura) > 0 then
				if wf_retrieve_piani_esistenti(ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_errore) < 0 then
					g_mb.error(ls_errore)
					return
				end if
			end if
			
	end choose
	
end if
end event

type dw_folder from u_folder within w_programmazione_attrezzature
integer x = 5
integer y = 4
integer width = 3831
integer height = 1984
integer taborder = 60
boolean bringtotop = true
end type

type dw_ext_prog_manut from uo_cs_xx_dw within w_programmazione_attrezzature
integer x = 37
integer y = 124
integer width = 3785
integer height = 1416
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_ext_prog_manut_new"
end type

event itemchanged;call super::itemchanged;datetime ldt_null

setnull(ldt_null)

if not i_extendmode then return 0
if i_colname = "flag_programma_periodo" then //and not isnull(i_coltext) then
	if i_coltext = 'S' then
		setitem( row, "da_data", ldt_null)
		setitem( row, "a_data", ldt_null)
	end if
end if
end event

event buttonclicked;call super::buttonclicked;if row>0 then
	choose case dwo.name
		case "b_indietro"
			wf_riseleziona()
			
		case "b_conferma"
			wf_conferma()
			
	end choose
end if
end event

