﻿$PBExportHeader$w_programmi_manutenzione.srw
$PBExportComments$Finestra Manutenzioni personalizzata Guerrato
forward
global type w_programmi_manutenzione from w_cs_xx_principale
end type
type cb_programma_griglia from commandbutton within w_programmi_manutenzione
end type
type cb_ordini from commandbutton within w_programmi_manutenzione
end type
type cb_risorse from commandbutton within w_programmi_manutenzione
end type
type cb_programmazione from commandbutton within w_programmi_manutenzione
end type
type cb_predefinito from commandbutton within w_programmi_manutenzione
end type
type cb_richiama_predefinito from commandbutton within w_programmi_manutenzione
end type
type cb_cerca from commandbutton within w_programmi_manutenzione
end type
type cb_ricambi from commandbutton within w_programmi_manutenzione
end type
type cb_documento from commandbutton within w_programmi_manutenzione
end type
type htb_1 from htrackbar within w_programmi_manutenzione
end type
type cb_controllo from commandbutton within w_programmi_manutenzione
end type
type cb_note_1 from uo_ext_note_mss_1 within w_programmi_manutenzione
end type
type dw_filtro from uo_std_dw within w_programmi_manutenzione
end type
type st_5 from statictext within w_programmi_manutenzione
end type
type st_6 from statictext within w_programmi_manutenzione
end type
type st_8 from statictext within w_programmi_manutenzione
end type
type st_4 from statictext within w_programmi_manutenzione
end type
type st_3 from statictext within w_programmi_manutenzione
end type
type st_2 from statictext within w_programmi_manutenzione
end type
type st_1 from statictext within w_programmi_manutenzione
end type
type tv_1 from treeview within w_programmi_manutenzione
end type
type dw_folder_search from u_folder within w_programmi_manutenzione
end type
type st_7 from statictext within w_programmi_manutenzione
end type
type dw_programmi_manutenzione_3 from uo_cs_xx_dw within w_programmi_manutenzione
end type
type dw_grid from datawindow within w_programmi_manutenzione
end type
type dw_programmi_manutenzione from uo_cs_xx_dw within w_programmi_manutenzione
end type
type dw_programmi_manutenzione_2 from uo_cs_xx_dw within w_programmi_manutenzione
end type
type dw_folder from u_folder within w_programmi_manutenzione
end type
type ws_record from structure within w_programmi_manutenzione
end type
end forward

type ws_record from structure
	long		anno_registrazione
	long		num_registrazione
	string		des_manutenzione
	string		codice
	string		descrizione
	long		handle_padre
	string		tipo_livello
	integer		livello
end type

global type w_programmi_manutenzione from w_cs_xx_principale
integer width = 4123
integer height = 2660
string title = "Piano di Manutenzione"
event ue_legenda ( )
cb_programma_griglia cb_programma_griglia
cb_ordini cb_ordini
cb_risorse cb_risorse
cb_programmazione cb_programmazione
cb_predefinito cb_predefinito
cb_richiama_predefinito cb_richiama_predefinito
cb_cerca cb_cerca
cb_ricambi cb_ricambi
cb_documento cb_documento
htb_1 htb_1
cb_controllo cb_controllo
cb_note_1 cb_note_1
dw_filtro dw_filtro
st_5 st_5
st_6 st_6
st_8 st_8
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
tv_1 tv_1
dw_folder_search dw_folder_search
st_7 st_7
dw_programmi_manutenzione_3 dw_programmi_manutenzione_3
dw_grid dw_grid
dw_programmi_manutenzione dw_programmi_manutenzione
dw_programmi_manutenzione_2 dw_programmi_manutenzione_2
dw_folder dw_folder
end type
global w_programmi_manutenzione w_programmi_manutenzione

type variables
boolean ib_carica_tutto = FALSE, ib_retrieve = TRUE, ib_grid = false, ib_sintexcal = false

long 	  il_handle, il_anno_registrazione, il_num_registrazione, il_livello_corrente,il_handle_modificato, &
     	  il_handle_cancellato, il_rbuttonrow, il_riga_copia

string  is_tipo_manut, is_tipo_ordinamento, is_flag_eseguito, is_sql_filtro, is_nome

datastore ids_cat_attrezzature, ids_rep_attrezzature, ids_manutenzioni, ids_lista_attrezzature

treeviewitem tvi_campo

uo_manutenzioni iuo_manutenzioni

uo_mansionario iuo_mansionario


n_tran tran_import
string is_chiave_root = "HKEY_LOCAL_MACHINE\Software\Consulting&Software\db_esterni\sintexcal\"
end variables

forward prototypes
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area)
public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura)
public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria)
public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto)
public function integer wf_inserisci_singola_registrazione (long fl_handle, long fl_anno_registrazione, long fl_num_registrazione)
public subroutine wf_imposta_tv ()
public subroutine wf_cancella_treeview ()
public subroutine wf_carica_attrezzatura (string fs_cod_attrezzatura)
public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_flag_tipo_intervento (string ws_flag_tipo_intervento, string ws_flag_ricambio_codificato)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public function integer wf_prodotto_codificato (string ws_flag_codificato)
public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura)
public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione)
public function string wf_leggi_parent (long al_handle)
public function integer wf_carica_lista_programmi (long fl_handle)
public function integer wf_incolla_piano_manutenzione (long fl_riga_copia, long fl_riga_incolla, string fs_operazione)
public function integer wf_posiziona_livello (long fl_handle, string fs_livello)
public function integer wf_filtra_righe ()
public function integer wf_incolla_ricambio (long fl_riga_copia, long fl_riga_incolla, string fs_operazione)
public function integer wf_carica_lista_programmi_std (long fl_handle)
public function integer wf_incolla_ricambio_std (long fl_riga_copia, long fl_riga_incolla, string fs_operazione)
public function integer wf_incolla_piano_manutenzione_std (long fl_riga_copia, long fl_riga_incolla, string fs_operazione)
public function integer wf_incolla_ricambio_semplice (long fl_riga_copia, long fl_riga_incolla, string fs_operazione)
public function integer wf_preordine (string fs_tkordi, ref string fs_errore, ref boolean fb_preordine)
public function integer wf_fatturato (string fs_tkboll, string fs_tkposi, ref string fs_errore, ref boolean fb_fatturato, ref string fs_descrizione)
public function boolean wf_operatore_rda ()
public function integer wf_esegui ()
public function integer wf_registro (boolean fb_connetti)
public function integer wf_controllo_anno (long fl_anno, long fl_numero, ref long fl_anno_riferimento, ref string fs_flag_budget)
public function long wf_anno_riferimento (string fs_cod_tipo_manutenzione, ref long fl_anni[])
public function integer wf_incolla_ricambio_piano (long fl_anno_origine, long fl_num_origine, long fl_anno_dest, long fl_num_dest, string fs_operazione, long fl_anno_riferimento)
end prototypes

event ue_legenda();st_1.BackColor = rgb(204,153,255)
st_2.BackColor = rgb(187,47,0)
st_3.BackColor = rgb(230,115,0)
st_4.BackColor = rgb(255,204,153)
st_5.BackColor = rgb(255,255,190)
st_6.BackColor = rgb(190,255,255)	
st_7.BackColor =  rgb(255,204,204)	//rgb(253,253,0)	
st_8.BackColor = rgb(215,215,0)
end event

public function integer wf_leggi_parent (long al_handle, ref string as_sql);ws_record lstr_item

long	ll_item

treeviewitem ltv_item


ll_item = al_handle

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello
			
		case "D"
			as_sql += " and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + lstr_item.codice + "' ) "

		case "R"
			as_sql += " and cod_reparto = '" + lstr_item.codice + "' "
		
		case "C"
			as_sql += " and cod_cat_attrezzature = '" + lstr_item.codice + "' "
		
		case "E"
			as_sql += " and cod_area_aziendale = '" + lstr_item.codice + "' "
		
		case "A"
			as_sql += " and cod_attrezzatura = '" + lstr_item.codice + "' "
		
		case "N"
			
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_area_aziendale, ls_des_area, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i, ll_livello
treeviewitem ltvi_campo
ws_record lstr_record
setnull(ls_null)
declare cu_aree dynamic cursor for sqlsa;

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_area_aziendale, des_area FROM tab_aree_aziendali WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_area) then
	ls_sql += " and cod_area_aziendale = '" + fs_cod_area + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_area_aziendale in (select cod_area_aziendale from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + is_sql_filtro + ") "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_area_aziendale"
else
	ls_sql = ls_sql + "ORDER BY des_area"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_aree;
do while 1=1
   fetch cu_aree into :ls_cod_area_aziendale, :ls_des_area;
   if (sqlca.sqlcode = 100) then
		close cu_aree;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_aree (w_manutenzioni:wf_inserisci_aree)~r~n" + sqlca.sqlerrtext)
		close cu_aree;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_area_aziendale
	lstr_record.descrizione = ls_des_area
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_manutenzione)
	lstr_record.tipo_livello = "E"
	ltvi_campo.data = lstr_record
	
	if is_tipo_ordinamento = "D" then
		ltvi_campo.label = ls_des_area + ", " +  ls_cod_area_aziendale
	else
		ltvi_campo.label = ls_cod_area_aziendale + ", " + ls_des_area
	end if

	ltvi_campo.pictureindex = 3
	ltvi_campo.selectedpictureindex = 3
	ltvi_campo.overlaypictureindex = 3
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello AREA")
		close cu_aree;
		return 0
	end if
	
	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_registrazioni(ll_risposta, ls_null) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----

loop
close cu_aree;
return 0
end function

public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_attrezzatura, ls_des_attrezzatura, ls_des_identificativa, ls_flag_eseguito, ls_appoggio, ls_flag_visione_strumenti, &
       ls_qualita_attrezzatura
long ll_risposta, ll_i, ll_num_righe, ll_livello
ws_record lstr_record

ll_livello = il_livello_corrente + 1

declare cu_attrezzature dynamic cursor for sqlsa;

ls_sql = "select cod_attrezzatura, descrizione, qualita_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro
if not isnull(fs_cod_attrezzatura) then
	ls_sql += " and cod_attrezzatura = '" + fs_cod_attrezzatura + "' "
end if

ls_flag_visione_strumenti = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_visione_strumenti")
if not isnull(ls_flag_visione_strumenti) then
	choose case ls_flag_visione_strumenti
		case "A"
			ls_sql += " and qualita_attrezzatura = 'A' "
		case "S"
			ls_sql += " and qualita_attrezzatura in ('P','S') "
	end choose
end if

wf_leggi_parent(fl_handle,ls_sql)

if is_tipo_ordinamento = "C" then
	ls_sql += " order by cod_attrezzatura"
else
	ls_sql += " order by descrizione"
end if

prepare sqlsa from :ls_sql;

open dynamic cu_attrezzature;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_attrezzature into :ls_cod_attrezzatura, :ls_des_attrezzatura, :ls_qualita_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_attrezzature;
		if lb_dati then return 0
		return 1
	end if
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
		close cu_attrezzature;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_attrezzatura) then ls_des_attrezzatura = ""

	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_attrezzatura
	lstr_record.descrizione = ls_des_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.des_manutenzione = ""
	lstr_record.tipo_livello = "A"
	lstr_record.livello = ll_livello
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "D" then
		tvi_campo.label = ls_des_attrezzatura + ", " + ls_cod_attrezzatura
	else
		tvi_campo.label = ls_cod_attrezzatura + ", " + ls_des_attrezzatura
	end if
	if ls_qualita_attrezzatura = "P" then
		tvi_campo.pictureindex = 5
		tvi_campo.selectedpictureindex = 5
		tvi_campo.overlaypictureindex = 5
	else
		tvi_campo.pictureindex = 4
		tvi_campo.selectedpictureindex = 4
		tvi_campo.overlaypictureindex = 4
	end if
	tvi_campo.children = true
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
loop
close cu_attrezzature;

return 0
end function

public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_cat_attrezzatura, ls_des_cat_attrezzatura, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i,ll_livello
ws_record lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

declare cu_categorie dynamic cursor for sqlsa;
ls_sql = "SELECT cod_cat_attrezzature, des_cat_attrezzature FROM tab_cat_attrezzature WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_categoria) then
	ls_sql += " and cod_cat_attrezzature = '" + fs_cod_categoria + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_cat_attrezzature in (select cod_cat_attrezzature from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + is_sql_filtro + ") "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_cat_attrezzature"
else
	ls_sql = ls_sql + "ORDER BY des_cat_attrezzature"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_categorie;
do while 1=1
   fetch cu_categorie into :ls_cod_cat_attrezzatura, :ls_des_cat_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_categorie;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_categorie (w_manutenzioni:wf_inserisci_categorie)~r~n" + sqlca.sqlerrtext)
		close cu_categorie;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_cat_attrezzatura) then ls_des_cat_attrezzatura = ""
	
	tvi_campo.itemhandle = fl_handle
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_cat_attrezzatura
	lstr_record.descrizione = ls_des_cat_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.des_manutenzione = ""
	lstr_record.tipo_livello = "C"
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_manutenzione)
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "D" then
		tvi_campo.label = ls_des_cat_attrezzatura + ", " + ls_cod_cat_attrezzatura
	else
		tvi_campo.label = ls_cod_cat_attrezzatura + ", " + ls_des_cat_attrezzatura
	end if
	tvi_campo.pictureindex = 1
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1
	tvi_campo.children = true
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
loop
close cu_categorie;
return 0

end function

public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_reparto, ls_des_reparto, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i,ll_livello
treeviewitem ltvi_campo
ws_record lstr_record
setnull(ls_null)

ll_livello = il_livello_corrente + 1

declare cu_reparti dynamic cursor for sqlsa;
ls_sql = "SELECT cod_reparto, des_reparto FROM anag_reparti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_reparto) then
	ls_sql += " and cod_reparto = '" + fs_cod_reparto + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_reparto in (select cod_reparto from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + is_sql_filtro + ") "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_reparto"
else
	ls_sql = ls_sql + "ORDER BY des_reparto"
end if
prepare sqlsa from :ls_sql;

open dynamic cu_reparti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1=1
   fetch cu_reparti into :ls_cod_reparto, :ls_des_reparto;
   if (sqlca.sqlcode = 100) then
		close cu_reparti;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
		close cu_reparti;
		return -1
	end if
	
	if isnull(ls_des_reparto) then ls_des_reparto = ""
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_reparto
	lstr_record.descrizione = ls_des_reparto
	lstr_record.tipo_livello = "R"
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle
	setnull(lstr_record.des_manutenzione)

	ltvi_campo.data = lstr_record
	if is_tipo_ordinamento = "D" then
		ltvi_campo.label = ls_des_reparto + ", " + ls_cod_reparto
	else
		ltvi_campo.label = ls_cod_reparto + ", " + ls_des_reparto
	end if
	ltvi_campo.pictureindex = 2
	ltvi_campo.selectedpictureindex = 2
	ltvi_campo.overlaypictureindex = 2
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW")
		close cu_reparti;
		return 0
	end if
	
	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_registrazioni(ll_risposta, ls_null) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----
loop
close cu_reparti;
return 0

end function

public function integer wf_inserisci_singola_registrazione (long fl_handle, long fl_anno_registrazione, long fl_num_registrazione);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento singola registrazione
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

string    ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, &
          ls_ordine_registrazione, ls_flag_eseguito, ls_flag_storico, ls_des_tipo_manut_storico
			 
long      ll_risposta

datetime  ldt_data_registrazione, ldt_data_inizio, ldt_data_fine

ws_record lstr_record

select data_creazione, 
		 cod_tipo_manutenzione, 
		 cod_attrezzatura,
		 flag_storico,
		 des_tipo_manutenzione_storico
into   :ldt_data_registrazione,
       :ls_cod_tipo_manutenzione,
		 :ls_cod_attrezzatura,
		 :ls_flag_storico,
		 :ls_des_tipo_manut_storico
from   programmi_manutenzione 
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
		 
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","Il programma richiesto è inesistente")
	return 0
end if

if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore in ricerca programma (wf_inserisci_singola_registrazione)~r~n" + sqlca.sqlerrtext)
	return -1
end if

if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""

if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""

if isnull(ls_des_tipo_manut_storico) or len(ls_des_tipo_manut_storico) < 1 then
		
	select des_tipo_manutenzione
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
	if sqlca.sqlcode <> 0 then
		if (not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "") or (not isnull(ls_cod_tipo_manutenzione) and ls_cod_tipo_manutenzione <> "") then
			g_mb.messagebox("OMNIA","Errore in ricerca descrizione tipo manutenzione "+ls_cod_tipo_manutenzione+" dell'apparecchiatura + "+ls_cod_attrezzatura+" (w_manutenzioni:wf_inserisci_manutenzioni)~r~nProseguo lo stesso, la descrizione non verrà inserita." + sqlca.sqlerrtext)
		end if
	end if
		
else
		
	ls_des_tipo_manutenzione = ls_des_tipo_manut_storico
		
end if

if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = ""
	
lstr_record.anno_registrazione = fl_anno_registrazione

lstr_record.num_registrazione = fl_num_registrazione

lstr_record.des_manutenzione = ls_des_tipo_manutenzione

lstr_record.codice = ""		

lstr_record.descrizione = ""

lstr_record.livello = 100

lstr_record.tipo_livello = "M"
	
ls_str = string(fl_anno_registrazione) + "/" + string(fl_num_registrazione) + " " + string(ldt_data_registrazione,"dd/mm/yyyy") + " " + ls_des_tipo_manutenzione

tvi_campo.data = lstr_record

tvi_campo.label = ls_str

if ls_flag_storico = "S" then
		
	tvi_campo.pictureindex = 6
		
	tvi_campo.selectedpictureindex = 6
		
	tvi_campo.overlaypictureindex = 6
		
else
		
	tvi_campo.pictureindex = 7
	
	tvi_campo.selectedpictureindex = 7
		
	tvi_campo.overlaypictureindex = 7
		
end if

tvi_campo.children = false

tvi_campo.selected = false

ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)

tv_1.SelectItem(ll_risposta)

return 0
end function

public subroutine wf_imposta_tv ();string ls_null, ls_cod_attrezzatura, ls_des_attrezzatura
long ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione
ws_record lstr_record

setnull(ls_null)

tvi_campo.expanded = false

tvi_campo.selected = false

tvi_campo.children = false	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ll_i = 0

setpointer(HourGlass!)

tv_1.setredraw(false)

ll_anno_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")

ll_num_registrazione = dw_filtro.getitemnumber(dw_filtro.getrow(),"num_registrazione")

// ----------------- se è stata richiesta una specifica registrazione propongo solo quella e niente altro------------------
//
if ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) and ll_num_registrazione > 0 and not isnull(ll_num_registrazione) then

	ib_retrieve = true
	
	wf_inserisci_singola_registrazione(0, ll_anno_registrazione, ll_num_registrazione )
	
	tv_1.setredraw(true)
	
	return
	
end if											  

// ----------------- se non è stato impostato alcun livello ed è stata specificata una attrezzatura, -----------------------
//							procedo con il caricamento delle registrazioni della sola attrezzatura richiesta 
//
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_1") = "N" and not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura")) then

	wf_inserisci_attrezzature(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura") )
	
	tv_1.setredraw(true)
	
	return
	
end if

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "D"
		
		wf_inserisci_divisioni(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione"))
		// caricamento divisioni
	case "R"
		
		wf_inserisci_reparti(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto"))
		// caricamento reparti
	case "C"
		
		wf_inserisci_categorie(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura"))
		// caricamento categorie
	case "E" 
		
		wf_inserisci_aree(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale"))
		// caricamento aree
	case "A"
		
		wf_inserisci_attrezzature(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura"))
//		 caricamento attrezzature
	case "N" 
		
		wf_inserisci_registrazioni(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura"))
end choose

tv_1.triggerevent("ue_espandi_rami")

tv_1.setredraw(true)

setpointer(arrow!)

return
end subroutine

public subroutine wf_cancella_treeview ();long tvi_hdl = 0

do 
	tvi_hdl = tv_1.finditem(roottreeitem!,0)
	if tvi_hdl <> -1 then
		tv_1.deleteitem(tvi_hdl)
	end if
loop while tvi_hdl <> -1

return
end subroutine

public subroutine wf_carica_attrezzatura (string fs_cod_attrezzatura);// --------------------------------  wf_carica_attrezzatura  -----------------------------------------
//
// Funzione specifica per caricare solo una attrezzatura passata come parametro; per ora viene usata
// nello standard solo per il navigatore, ma non si esclude che possa essere usata da altre gestioni
//
// ---------------------------------------------------------------------------------------------------

dw_filtro.setitem(dw_filtro.getrow(),"cod_attrezzatura", fs_cod_attrezzatura)
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_1", "N")
cb_cerca.triggerevent("clicked")
end subroutine

public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione);// --------------------------------  wf_carica_registrazione_manutenzione  ---------------------------
//
// Funzione specifica da richiamare dall'esterno per caricare solo una singola registrazione di manutenzione.
//
// ---------------------------------------------------------------------------------------------------
string ls_null

setnull(ls_null)
dw_filtro.setitem(dw_filtro.getrow(),"anno_registrazione", fl_anno_registrazione)
dw_filtro.setitem(dw_filtro.getrow(),"num_registrazione", fl_num_registrazione)
dw_filtro.setitem(dw_filtro.getrow(),"cod_attrezzatura", ls_null)
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_1", "N")
dw_filtro.accepttext()
cb_cerca.triggerevent("clicked")
end subroutine

public function integer wf_flag_tipo_intervento (string ws_flag_tipo_intervento, string ws_flag_ricambio_codificato);string ls_modify
long   ll_riga

dw_programmi_manutenzione.setredraw(false)

choose case ws_flag_tipo_intervento
case "M"
   wf_prodotto_codificato(ws_flag_ricambio_codificato)
	dw_programmi_manutenzione_2.object.cod_primario_t.Visible=0
	dw_programmi_manutenzione_2.object.cod_primario.Visible=0
	dw_programmi_manutenzione_2.object.cf_cod_primario.Visible=0
	dw_programmi_manutenzione_2.object.cod_misura.Visible=0
	dw_programmi_manutenzione_2.object.cod_misura_t.Visible=0
	dw_programmi_manutenzione_2.object.valore_min_taratura.Visible=0
	dw_programmi_manutenzione_2.object.valore_min_taratura_t.Visible=0
	dw_programmi_manutenzione_2.object.valore_max_taratura.Visible=0
	dw_programmi_manutenzione_2.object.valore_max_taratura_t.Visible=0
	dw_programmi_manutenzione_2.object.valore_atteso.Visible=0
	dw_programmi_manutenzione_2.object.valore_atteso_t.Visible=0
	dw_programmi_manutenzione_2.object.taratura_t.Visible=0
					
case "T"
	dw_programmi_manutenzione_2.object.cod_primario_t.Visible=1
	dw_programmi_manutenzione_2.object.cod_primario.Visible=1
	dw_programmi_manutenzione_2.object.cf_cod_primario.Visible=1
	dw_programmi_manutenzione_2.object.cod_misura.Visible=1
	dw_programmi_manutenzione_2.object.cod_misura_t.Visible=1
	dw_programmi_manutenzione_2.object.valore_min_taratura.Visible=1
	dw_programmi_manutenzione_2.object.valore_min_taratura_t.Visible=1
	dw_programmi_manutenzione_2.object.valore_max_taratura.Visible=1
	dw_programmi_manutenzione_2.object.valore_max_taratura_t.Visible=1
	dw_programmi_manutenzione_2.object.valore_atteso.Visible=1
	dw_programmi_manutenzione_2.object.valore_atteso_t.Visible=1
	dw_programmi_manutenzione_2.object.taratura_t.Visible=1
   wf_prodotto_codificato("T")
end choose

ll_riga = dw_programmi_manutenzione.getrow( )

if ll_riga > 0 and not isnull(ll_riga) then
	
	if dw_programmi_manutenzione.getitemstring( ll_riga, "flag_storico") = 'S' then
		
		dw_programmi_manutenzione.object.cod_tipo_manutenzione.Visible=0
		
		dw_programmi_manutenzione.object.des_tipo_manutenzione_storico.Visible=1
		
	else
		
		dw_programmi_manutenzione.object.cod_tipo_manutenzione.Visible=1
		
		dw_programmi_manutenzione.object.des_tipo_manutenzione_storico.Visible=0
		
	end if
	
else
	
	dw_programmi_manutenzione.object.cod_tipo_manutenzione.Visible=1
	
	dw_programmi_manutenzione.object.des_tipo_manutenzione_storico.Visible=0	
	
end if

dw_programmi_manutenzione.setredraw(true)

return 0

end function

public subroutine wf_memorizza_filtro ();datetime ldt_data

long 		ll_i, ll_colonne, ll_numero,ll_cont

string 	ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente


if g_mb.messagebox("Omnia","Memorizzare l'attuale impostazione dei filtri come predefinita?",Question!,YesNo!,2) = 2 then
	return
end if

ls_colcount = dw_filtro.Object.DataWindow.Column.Count

ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

for ll_i = 1 to ll_colonne
	
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_filtro.getitemdatetime(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_filtro.getitemstring(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_filtro.getitemnumber(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'PROGR';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if		 
		
if ll_cont > 0 then
	
	update filtri_manutenzioni
	set 	 filtri = :ls_memo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente =  :ls_cod_utente and
		    tipo_filtro = 'PROGR';
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
else
	
	insert
	into filtri_manutenzioni
		  (cod_azienda,
		  cod_utente,
		  filtri,
		  tipo_filtro)
	values (:s_cs_xx.cod_azienda,
		  	 :ls_cod_utente,
		  	 :ls_memo,
			 'PROGR');
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if

commit;
end subroutine

public subroutine wf_leggi_filtro ();datetime ldt_data

long 		ll_i, ll_colonne, ll_numero, ll_anno_riferimento

string 	ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente


ls_colcount = dw_filtro.Object.DataWindow.Column.Count

ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'PROGR';

if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	rollback;
	return
end if

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ll_numero)
	end if

next

if ib_sintexcal then
	dw_filtro.setitem( dw_filtro.getrow(), "anno_ordine", year(today()))
	
	select anno_riferimento
	into	:ll_anno_riferimento
	from	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			flag_anno_riferimento = 'S' ;
			
	if sqlca.sqlcode = 0 and not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
		dw_filtro.setitem( dw_filtro.getrow(), "anno_riferimento", ll_anno_riferimento)
	else
		dw_filtro.setitem( dw_filtro.getrow(), "anno_riferimento", year(today()))
	end if
			
elseif ib_grid then
	dw_filtro.setitem( dw_filtro.getrow(), "flag_non_prog", "S")
	dw_filtro.setitem( dw_filtro.getrow(), "flag_prog", "N")
	dw_filtro.setitem( dw_filtro.getrow(), "flag_parziali", "S")
end if

end subroutine

public function integer wf_prodotto_codificato (string ws_flag_codificato);choose case ws_flag_codificato
case "S"
   dw_programmi_manutenzione_3.object.cod_ricambio_t.Visible=1
   dw_programmi_manutenzione_3.object.cod_ricambio.Visible=1
   dw_programmi_manutenzione_3.object.cf_des_ricambio.Visible=1
   dw_programmi_manutenzione_3.object.cod_ricambio_alternativo_t.Visible=1
   dw_programmi_manutenzione_3.object.cod_ricambio_alternativo.Visible=1
   dw_programmi_manutenzione_3.object.cf_des_ricambio_alternativo.Visible=1
   dw_programmi_manutenzione_3.object.ricambio_non_codificato_t.Visible=0
   dw_programmi_manutenzione_3.object.ricambio_non_codificato.Visible=0
   dw_programmi_manutenzione_3.object.flag_ricambio_codificato.Visible=1
   dw_programmi_manutenzione_3.object.flag_ricambio_codificato_t.Visible=1
   dw_programmi_manutenzione_3.object.quan_ricambio_t.Visible=1
   dw_programmi_manutenzione_3.object.quan_ricambio.Visible=1
   dw_programmi_manutenzione_3.object.costo_unitario_ricambio_t.Visible=1
   dw_programmi_manutenzione_3.object.costo_unitario_ricambio.Visible=1
   dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=true
   dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=true
case "N"
   dw_programmi_manutenzione_3.object.cod_ricambio_t.Visible=0
   dw_programmi_manutenzione_3.object.cod_ricambio.Visible=0
   dw_programmi_manutenzione_3.object.cf_des_ricambio.Visible=0
   dw_programmi_manutenzione_3.object.cod_ricambio_alternativo_t.Visible=0
   dw_programmi_manutenzione_3.object.cod_ricambio_alternativo.Visible=0
   dw_programmi_manutenzione_3.object.cf_des_ricambio_alternativo.Visible=0
   dw_programmi_manutenzione_3.object.ricambio_non_codificato_t.Visible=1
   dw_programmi_manutenzione_3.object.ricambio_non_codificato.Visible=1
   dw_programmi_manutenzione_3.object.flag_ricambio_codificato.Visible=1
   dw_programmi_manutenzione_3.object.flag_ricambio_codificato_t.Visible=1
   dw_programmi_manutenzione_3.object.quan_ricambio_t.Visible=1
   dw_programmi_manutenzione_3.object.quan_ricambio.Visible=1
   dw_programmi_manutenzione_3.object.costo_unitario_ricambio_t.Visible=1
   dw_programmi_manutenzione_3.object.costo_unitario_ricambio.Visible=1
   dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=false
   dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=false
case "T"
   dw_programmi_manutenzione_3.object.cod_ricambio_t.Visible=0
   dw_programmi_manutenzione_3.object.cod_ricambio.Visible=0
   dw_programmi_manutenzione_3.object.cf_des_ricambio.Visible=0
   dw_programmi_manutenzione_3.object.cod_ricambio_alternativo_t.Visible=0
   dw_programmi_manutenzione_3.object.cod_ricambio_alternativo.Visible=0
   dw_programmi_manutenzione_3.object.cf_des_ricambio_alternativo.Visible=0
   dw_programmi_manutenzione_3.object.ricambio_non_codificato_t.Visible=0
   dw_programmi_manutenzione_3.object.ricambio_non_codificato.Visible=0
   dw_programmi_manutenzione_3.object.flag_ricambio_codificato.Visible=0
   dw_programmi_manutenzione_3.object.flag_ricambio_codificato_t.Visible=0
   dw_programmi_manutenzione_3.object.quan_ricambio_t.Visible=0
   dw_programmi_manutenzione_3.object.quan_ricambio.Visible=0
   dw_programmi_manutenzione_3.object.costo_unitario_ricambio_t.Visible=0
   dw_programmi_manutenzione_3.object.costo_unitario_ricambio.Visible=0
   dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=true
   dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=true
end choose
return 0   



end function

public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_attrezzatura);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento registrazioni di manutenzione / taratura
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  lb_dati = false

string   ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, &
         ls_ordine_registrazione, ls_cod_tipo_manut_filtro, ls_data_registrazione, ls_flag_storico,ls_des_tipo_manutenzione_storico

long     ll_risposta, ll_i, ll_num_righe, ll_anno_registrazione, ll_num_registrazione, ll_livello, ll_anno_reg_filtro

datetime ldt_data_ultima_modifica, ldt_data_inizio, ldt_data_fine

ws_record lstr_record

ll_livello = il_livello_corrente

declare cu_registrazioni dynamic cursor for sqlsa;

ls_sql = "select anno_registrazione, " + &
         "       num_registrazione, " + &
			"       data_creazione, " + &
			"       cod_tipo_manutenzione, " + &
			"       cod_attrezzatura, " + &
			"       flag_storico, " + &
			"       des_tipo_manutenzione_storico " + &
			"FROM   programmi_manutenzione " + &
			"WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_eseguito")
		
	case "S"
		ls_sql += " and flag_storico = 'S' "
		
	case "N"
		ls_sql += " and flag_storico = 'N' "
		
end choose

choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_blocco")
		
	case "S"
		ls_sql += " and flag_blocco = 'S' "
		
	case "N"		
		ls_sql += " and flag_blocco = 'N' "
		
end choose

ll_anno_reg_filtro = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")

ls_ordine_registrazione = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_ordinamento_registrazioni")

ldt_data_inizio = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_inizio")

ldt_data_fine = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_fine")

ls_cod_tipo_manut_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione")

if not isnull(ll_anno_reg_filtro) and ll_anno_reg_filtro > 0 then
	ls_sql += " and anno_registrazione = " + string(ll_anno_reg_filtro) + " "
end if	

if not isnull(ldt_data_inizio) and ldt_data_inizio >= datetime(date("01/01/1900"), 00:00:00) then
	//Donato 21-01-2009 il filtro per data deve puntare alla colonna data_ultima_modifica oppure a data_creazione
	ls_sql += " and (data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql += " or data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//vecchio codice
	//ls_sql += " and data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	//fine modifica -------------------------------------------------------------------------------------------------------------------------------
end if

if not isnull(ldt_data_fine) and ldt_data_fine >= datetime(date("01/01/1900"), 00:00:00) then
	//Donato 21-01-2009 il filtro per data deve puntare alla colonna data_ultima_modifica oppure a data_creazione
	ls_sql += " and (data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql += " or data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//vecchio codice
	//ls_sql += " and data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	//fine modifica -------------------------------------------------------------------------------------------------------------------------------	
end if

if len(ls_cod_tipo_manut_filtro) < 1 then setnull(ls_cod_tipo_manut_filtro)

if not isnull(ls_cod_tipo_manut_filtro) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
end if

if isnull(ls_ordine_registrazione) then ls_ordine_registrazione = "D"

choose case ls_ordine_registrazione

	case "U"
		ls_sql += " order by data_ultima_modifica ASC "
		
	case "C"
		ls_sql += " order by data_creazione ASC "
		
	case "A"
		ls_sql += " order by cod_attrezzatura ASC " 
		
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_registrazioni;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_registrazioni into :ll_anno_registrazione, 
	                            :ll_num_registrazione, 
										 :ldt_data_ultima_modifica, 
										 :ls_cod_tipo_manutenzione, 
										 :ls_cod_attrezzatura, 
										 :ls_flag_storico, 
										 :ls_des_tipo_manutenzione_storico;
										 
   if (sqlca.sqlcode = 100) then
		close cu_registrazioni;
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_registrazioni (w_programmi_manutenzione:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
		close cu_registrazioni;
		return -1
	end if

	if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""
	
	lb_dati = true
	
	if isnull(ls_des_tipo_manutenzione_storico) or len(ls_des_tipo_manutenzione_storico) < 1 then
		
		select des_tipo_manutenzione
		into   :ls_des_tipo_manutenzione
		from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_attrezzatura = :ls_cod_attrezzatura and
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		/*
		if sqlca.sqlcode <> 0 then
			if (not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "") or (not isnull(ls_cod_tipo_manutenzione) and ls_cod_tipo_manutenzione <> "") then
				g_mb.messagebox("OMNIA","Errore in ricerca descrizione tipo manutenzione "+ls_cod_tipo_manutenzione+" dell'apparecchiatura + "+ls_cod_attrezzatura+" (w_manutenzioni:wf_inserisci_manutenzioni)~r~nProseguo lo stesso, la descrizione non verrà inserita." + sqlca.sqlerrtext)
			end if
		end if
		*/
		
	else
		
		ls_des_tipo_manutenzione = ls_des_tipo_manutenzione_storico
		
	end if
		
	if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = "<DESCRIZIONE MANCANTE>"
	
	if isnull(ldt_data_ultima_modifica) then
		ls_data_registrazione = "??/??/????"
	else
		ls_data_registrazione = string(ldt_data_ultima_modifica,"dd/mm/yyyy")
	end if
	
	lstr_record.anno_registrazione = ll_anno_registrazione
	
	lstr_record.num_registrazione = ll_num_registrazione
	
	lstr_record.des_manutenzione = ls_des_tipo_manutenzione
	
	lstr_record.codice = ""		
	
	lstr_record.descrizione = ""
	
	lstr_record.livello = 100
	
	lstr_record.tipo_livello = "M"
	
	choose case ls_ordine_registrazione
		case "C"
			ls_str = ls_data_registrazione + " " + ls_des_tipo_manutenzione
			
		case "D"
			ls_str = ls_data_registrazione + " " + ls_des_tipo_manutenzione
			
		case "A"
			ls_str = ls_cod_attrezzatura + " " + ls_data_registrazione + " " + ls_des_tipo_manutenzione
			
	end choose
	
	tvi_campo.data = lstr_record
	
	tvi_campo.label = ls_str
	
	if ls_flag_storico = "S" then
		
		tvi_campo.pictureindex = 6
		
		tvi_campo.selectedpictureindex = 6
		
		tvi_campo.overlaypictureindex = 6
		
	else
		
		tvi_campo.pictureindex = 7
		
		tvi_campo.selectedpictureindex = 7
		
		tvi_campo.overlaypictureindex = 7
		
	end if
	
	tvi_campo.children = false
	
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
loop

close cu_registrazioni;

return 0

end function

public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false

string    ls_sql, ls_cod_divisione, ls_des_divisione, ls_null, ls_appoggio, ls_sql_livello, ls_padre

long      ll_risposta, ll_i, ll_livello

ws_record lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_divisione, des_divisione FROM anag_divisioni WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_divisione) then
	ls_sql += " and cod_divisione = '" + fs_cod_divisione + "' "
end if


// *** michela 19/07/2005: nel caso delle divisioni non mi interessa mettere alcuna where in più rispetto al padre ( ma solo nel caso delle aree)
ls_sql_livello = ""
ls_padre = ""

if ll_livello > 1 then
	ls_padre = wf_leggi_parent(fl_handle)
end if

if ls_padre = "E" then
	wf_leggi_parent(fl_handle,ls_sql_livello)
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_divisione IN ( select cod_divisione from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "'  " + ls_sql_livello + " ) "
end if

if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + "ORDER BY cod_divisione"
else
	ls_sql = ls_sql + "ORDER BY des_divisione"
end if

declare cu_divisioni dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_divisioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_divisioni: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_divisioni;
	return 0
end if

do while 1=1
   fetch cu_divisioni into :ls_cod_divisione, 
								   :ls_des_divisione;
									
   if (sqlca.sqlcode = 100) then
		close cu_divisioni;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_divisioni (w_manutenzioni:wf_inserisci_divisioni)~r~n" + sqlca.sqlerrtext)
		close cu_divisioni;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_divisione
	lstr_record.descrizione = ls_des_divisione
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_manutenzione)
	lstr_record.tipo_livello = "D"
	tvi_campo.data = lstr_record
	
	if is_tipo_ordinamento = "D" then
		tvi_campo.label = ls_des_divisione + ", " +  ls_cod_divisione
	else
		tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione
	end if

	tvi_campo.pictureindex = 3
	tvi_campo.selectedpictureindex = 3
	tvi_campo.overlaypictureindex = 3
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello DIVISIONE")
		close cu_divisioni;
		return 0
	end if

loop
close cu_divisioni;
return 0
end function

public function string wf_leggi_parent (long al_handle);ws_record lstr_item

long	ll_item

treeviewitem ltv_item


ll_item = al_handle

if ll_item = 0 then
	return ""
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	return lstr_item.tipo_livello	
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return ""
end function

public function integer wf_carica_lista_programmi (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento lista semplificata dei programmi
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//
// ------------------------------------------------------------------------------------- //

string   ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, ls_sql1, ls_sql3,  &
         ls_cod_tipo_manut_filtro, ls_data_registrazione, ls_note_idl, ls_cod_operaio, ls_cod_risorsa_esterna,&
			ls_cod_categoria, ls_cod_reparto,ls_des_attrezzatura, ls_cod_ricambio, ls_cod_fornitore, ls_cod_prodotto_filtro, &
			ls_rag_soc_1, ls_null, ls_nome, ls_cognome, ls_des_ricambio, ls_flag_prodotto_codificato, ls_cod_misura, &
			ls_des_misura, ls_cod_cat_risorse_esterne, ls_des_intervento, ls_cod_attrezzatura_old, ls_flag_budget, ls_ordine_progen, &
			ls_cod_porto, ls_flag_prezzo_certo, ls_flag_capitalizzato,ls_flag_prezzo_certo_filtro, ls_flag_sottoattrezzature, ls_cod_attrezzatura_filtro, &
			ls_flag_capitalizzato_filtro, ls_cod_fornitore_filtro, ls_des_categoria, ls_des_reparto, ls_flag_primario, ls_flag_solo_attivita, ls_flag_risorse_interne, ls_flag_risorse_esterne, ls_flag_ricambi, &
			ls_cod_stato, ls_des_stato, ls_num_documento, ls_ordine_ricerca, ls_sql_ricambi, ls_sql_ricambi_2, ls_sql4, ls_controllo, &
			ls_ddi, ls_tkordi_rda, ls_tkboll_ddi, ls_errore, ls_tkordi_ricambio, ls_flag_rda, ls_flag_rda_approvata, ls_ordine_progen_rda, ls_descrizione, &
			ls_tkposi_ddi, ls_cod_attrezzatura_padre

long     ll_row, ll_risposta, ll_i, ll_num_righe, ll_anno_registrazione, ll_num_registrazione, ll_livello, &
         ll_anno_reg_filtro, ll_operaio_ore_previste,ll_operaio_minuti_previsti,ll_risorsa_ore_previste, &
			ll_risorsa_minuti_previsti, ll_row_old, ll_progressivo, ll_priorita, ll_rec, ll_ret, ll_ret1, ll_ret2, ll_ret3, ll_anno_ordine, ll_num_ordine, &
			ll_controllo, ll_prova, ll_anno_riferimento, ll_ore_effettive, ll_minuti_effettivi, ll_commenti

dec{4}   ld_quan_ricambio, ld_costo_ricambio, ld_risorsa_costo_orario, ld_operaio_costo_orario, &
			ld_risorsa_costi_aggiuntivi, ld_tot_tempo, ld_tot_costo, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_prezzo_netto, ld_sconto_1_eff,ld_sconto_2_eff,ld_sconto_3_eff, &
			ld_prezzo_netto_eff, ld_prezzo_ricambio_eff, ld_quan_eff
			
datetime ldt_data_ultima_modifica, ldt_data_inizio, ldt_data_fine, ldt_data_consegna, ldt_data_documento
string ls_appo

boolean  lb_fatturato, lb_controllo, lb_programmata

if ib_grid = false then return 0

//ls_sql_ricambi = " SELECT prog_riga_ricambio,	" + &
//						"		  cod_prodotto,   	" + &
//						"		  des_prodotto,   	" + &
//						"		  quan_utilizzo,   	" + &
//						"		  prezzo_ricambio,   	" + &
//						"		  flag_prodotto_codificato,  	" + & 
//						"		  cod_fornitore,   	" + &
//						"		  cod_misura,	" + &
//						"		  data_consegna,	" + &
//						"		  cod_porto,	" + &
//						"		  flag_prezzo_certo,	" + &
//						"		  flag_capitalizzato,	" + &
//						"		  priorita,	" + &
//						"		  ordine_progen,	" + &
//						"		  num_documento,	" + &
//						"		  data_documento,	" + &
//						"		  ordine_progen	 " + &
//						"		  FROM prog_manutenzioni_ricambi  	" + &
//						"		  WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( flag_ricambio_prot = 'N' or flag_ricambio_prot IS NULL ) " 

ls_sql_ricambi = " SELECT prog_riga_ricambio,	" + &
						"		  cod_prodotto,   	" + &
						"		  des_prodotto,   	" + &
						"		  quan_utilizzo,   	" + &
						"		  prezzo_ricambio,   	" + &
						"		  flag_prodotto_codificato,  	" + & 
						"		  cod_fornitore,   	" + &
						"		  cod_misura,	" + &
						"		  data_consegna,	" + &
						"		  cod_porto,	" + &
						"		  flag_prezzo_certo,	" + &
						"		  flag_capitalizzato,	" + &
						"		  priorita,	" + &
						"		  ordine_progen,	" + &
						"		  num_documento,	" + &
						"		  data_documento" + &
						"		  FROM prog_manutenzioni_ricambi  	" + &
						"		  WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( flag_ricambio_prot = 'N' or flag_ricambio_prot IS NULL ) " 


declare cu_ricambi_det cursor for
select  num_documento,   
        data_documento,
		  tkboll,
		  tkposi_b
from    prog_manutenzioni_ric_ddi
where  cod_azienda = :s_cs_xx.cod_azienda and
           anno_registrazione = :ll_anno_registrazione and
		  num_registrazione = :ll_num_registrazione and
		  prog_riga_ricambio = :ll_progressivo;

DECLARE cu_operai CURSOR FOR  
SELECT progressivo ,
      minuti,   
		ore,   
		cod_operaio,
		operaio_costo_orario,
		flag_prezzo_certo,
		flag_capitalizzato,
		priorita,
		cod_stato,
		ore_effettive,
		minuti_effettivi
 FROM prog_manutenzioni_operai  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :ll_anno_registrazione AND  
		num_registrazione = :ll_num_registrazione and
		flag_prezzo_certo  like :ls_flag_prezzo_certo_filtro and
		flag_capitalizzato like :ls_flag_capitalizzato_filtro
ORDER BY progressivo ASC  ;

DECLARE cu_risorse CURSOR FOR  
SELECT progressivo, 
      minuti,   
		ore,   
		cod_risorsa_esterna,   
		cod_cat_risorse_esterne,
		risorsa_costo_orario,
		risorsa_costi_aggiuntivi,
		flag_prezzo_certo,
		flag_capitalizzato,
		cod_fornitore,
		priorita,
		cod_stato
 FROM prog_manutenzioni_risorse_est  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :ll_anno_registrazione AND  
		num_registrazione = :ll_num_registrazione and
		cod_fornitore like :ls_cod_fornitore_filtro and
		flag_prezzo_certo  like :ls_flag_prezzo_certo_filtro and
		flag_capitalizzato like :ls_flag_capitalizzato_filtro
ORDER BY progressivo ASC  ;


setnull(ls_null)
dw_grid.reset()
dw_grid.setredraw(false)
setpointer(hourglass!)

ls_flag_sottoattrezzature = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_sottoattrezzature")
if isnull(ls_flag_sottoattrezzature) then ls_flag_sottoattrezzature = "N"

ls_cod_attrezzatura_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura")
if ls_cod_attrezzatura_filtro = "" then setnull(ls_cod_attrezzatura_filtro)

ls_flag_prezzo_certo_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_prezzo_certo")
if isnull(ls_flag_prezzo_certo_filtro) or upper(ls_flag_prezzo_certo_filtro) = "X" then
	ls_flag_prezzo_certo_filtro = "%"
end if

ls_flag_capitalizzato_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_capitalizzato")
if isnull(ls_flag_capitalizzato_filtro) or upper(ls_flag_capitalizzato_filtro) = "X" then
	ls_flag_capitalizzato_filtro = "%"
end if

ls_cod_fornitore_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_fornitore")
if isnull(ls_cod_fornitore_filtro) then
	ls_cod_fornitore_filtro = "%"
end if

ls_cod_prodotto_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_prodotto")
if isnull(ls_cod_prodotto_filtro) or ls_cod_prodotto_filtro = "" then
	ls_cod_prodotto_filtro = "%"
end if

ll_anno_riferimento = dw_filtro.getitemnumber( dw_filtro.getrow(), "anno_riferimento")
if isnull(ll_anno_riferimento) then ll_anno_riferimento = 0

ls_flag_solo_attivita = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_solo_attivita")
ls_flag_risorse_interne = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_risorse_interne")
ls_flag_risorse_esterne = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_risorse_esterne")
ls_flag_ricambi = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_ricambi")

ll_anno_ordine = dw_filtro.getitemnumber( dw_filtro.getrow(), "anno_ordine")
ll_num_ordine = dw_filtro.getitemnumber( dw_filtro.getrow(), "num_ordine")

if not isnull(ll_num_ordine) and ll_num_ordine > 0 then
	
	if isnull(ll_anno_ordine) or ll_anno_ordine = 0 then ll_anno_ordine = year(today())
	ls_ordine_ricerca = string(ll_anno_ordine) + "/" + string(ll_num_ordine)
	
else
	ls_ordine_ricerca = "%"
end if

if isnull(ls_flag_solo_attivita) then ls_flag_solo_attivita = 'N'
if isnull(ls_flag_risorse_interne) then ls_flag_risorse_interne = 'N'
if isnull(ls_flag_risorse_esterne) then ls_flag_risorse_esterne = 'N'
if isnull(ls_flag_ricambi) then ls_flag_ricambi = 'N'

declare cu_lista_programmi dynamic cursor for sqlsa;

ls_sql1 = ""
//ls_sql2 = ""
ls_sql3 = ""
ls_sql4 = ""
if not isnull(ls_cod_fornitore_filtro) and ls_cod_fornitore_filtro <> "%" then
	
	ls_sql1 = " select distinct programmi_manutenzione.anno_registrazione, " + &
						 " programmi_manutenzione.num_registrazione, " + &
						 " programmi_manutenzione.cod_tipo_manutenzione, " + &
						 " programmi_manutenzione.cod_attrezzatura, " + &
						 " programmi_manutenzione.des_intervento, " + &
						 " programmi_manutenzione.note_idl, " + &
						 " programmi_manutenzione.flag_budget " + &
				" FROM prog_manutenzioni_ricambi LEFT OUTER JOIN programmi_manutenzione ON prog_manutenzioni_ricambi.cod_azienda = programmi_manutenzione.cod_azienda AND prog_manutenzioni_ricambi.anno_registrazione = programmi_manutenzione.anno_registrazione AND prog_manutenzioni_ricambi.num_registrazione = programmi_manutenzione.num_registrazione" + &
				" where  programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' ) and flag_storico = 'N' and ( flag_blocco = 'N' or (flag_blocco = 'S' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S')))   " + &
				"    and ( flag_ricambio_prot = 'N' or flag_ricambio_prot IS NULL )    and prog_manutenzioni_ricambi.cod_fornitore = '" + ls_cod_fornitore_filtro + "'  and prog_manutenzioni_ricambi.flag_capitalizzato like '" + ls_flag_capitalizzato_filtro + "' "
				
//	ls_sql2 = " select programmi_manutenzione.anno_registrazione, " + &
//						 " programmi_manutenzione.num_registrazione, " + &
//						 " programmi_manutenzione.cod_tipo_manutenzione, " + &
//						 " programmi_manutenzione.cod_attrezzatura, " + &
//						 " programmi_manutenzione.des_intervento, " + &
//						 " programmi_manutenzione.note_idl, " + &
//						 " programmi_manutenzione.flag_budget " + &
//				" FROM prog_manutenzioni_risorse_est LEFT OUTER JOIN programmi_manutenzione ON prog_manutenzioni_risorse_est.cod_azienda = programmi_manutenzione.cod_azienda AND prog_manutenzioni_risorse_est.anno_registrazione = programmi_manutenzione.anno_registrazione AND prog_manutenzioni_risorse_est.num_registrazione = programmi_manutenzione.num_registrazione" + &
//				" where  programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' ) and flag_storico = 'N'  and ( flag_blocco = 'N' or (flag_blocco = 'S' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S'))) " + &
//				"        and prog_manutenzioni_risorse_est.cod_fornitore = '" + ls_cod_fornitore_filtro + "'  and prog_manutenzioni_risorse_est.flag_capitalizzato like '" + ls_flag_capitalizzato_filtro + "' "				
	
	if ll_anno_riferimento > 0 then
		ls_sql1 += " and ( prog_manutenzioni_ricambi.anno_riferimento = " + string( ll_anno_riferimento) + " or ( prog_manutenzioni_ricambi.anno_riferimento = 0 and programmi_manutenzione.cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S' ) ) )"
	end if
	
end if

if not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "%" then
	
	ls_sql3 = " select distinct programmi_manutenzione.anno_registrazione, " + &
						 " programmi_manutenzione.num_registrazione, " + &
						 " programmi_manutenzione.cod_tipo_manutenzione, " + &
						 " programmi_manutenzione.cod_attrezzatura, " + &
						 " programmi_manutenzione.des_intervento, " + &
						 " programmi_manutenzione.note_idl, " + &
						 " programmi_manutenzione.flag_budget " + &
				" FROM prog_manutenzioni_ricambi LEFT OUTER JOIN programmi_manutenzione ON prog_manutenzioni_ricambi.cod_azienda = programmi_manutenzione.cod_azienda AND prog_manutenzioni_ricambi.anno_registrazione = programmi_manutenzione.anno_registrazione AND prog_manutenzioni_ricambi.num_registrazione = programmi_manutenzione.num_registrazione" + &
				" where  programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' ) and flag_storico = 'N' and ( flag_blocco = 'N' or (flag_blocco = 'S' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S'))) " + &
				"   and ( flag_ricambio_prot = 'N' or flag_ricambio_prot IS NULL )     and prog_manutenzioni_ricambi.cod_prodotto = '" + ls_cod_prodotto_filtro + "'  and prog_manutenzioni_ricambi.flag_capitalizzato like '" + ls_flag_capitalizzato_filtro + "' "

	if ll_anno_riferimento > 0 then
		ls_sql3 += " and ( prog_manutenzioni_ricambi.anno_riferimento = " + string( ll_anno_riferimento) + " or ( prog_manutenzioni_ricambi.anno_riferimento = 0 and programmi_manutenzione.cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S' ) ) )"
	end if
	
end if

if not isnull(ls_ordine_ricerca) and ls_ordine_ricerca <> "%" then
	
	ls_sql4 = " select distinct programmi_manutenzione.anno_registrazione, " + &
						 " programmi_manutenzione.num_registrazione, " + &
						 " programmi_manutenzione.cod_tipo_manutenzione, " + &
						 " programmi_manutenzione.cod_attrezzatura, " + &
						 " programmi_manutenzione.des_intervento, " + &
						 " programmi_manutenzione.note_idl, " + &
						 " programmi_manutenzione.flag_budget " + &
				" FROM prog_manutenzioni_ricambi LEFT OUTER JOIN programmi_manutenzione ON prog_manutenzioni_ricambi.cod_azienda = programmi_manutenzione.cod_azienda AND prog_manutenzioni_ricambi.anno_registrazione = programmi_manutenzione.anno_registrazione AND prog_manutenzioni_ricambi.num_registrazione = programmi_manutenzione.num_registrazione" + &
				" where  programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' ) and flag_storico = 'N' and ( flag_blocco = 'N' or (flag_blocco = 'S' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S'))) " + &
				"   and ( flag_ricambio_prot = 'N' or flag_ricambio_prot IS NULL )     and ( prog_manutenzioni_ricambi.ordine_progen = '" + ls_ordine_ricerca + "' OR prog_manutenzioni_ricambi.ordine_progen_rda = '" + ls_ordine_ricerca + "' ) "
				
	if not isnull(ls_flag_capitalizzato_filtro) and ls_flag_capitalizzato_filtro <> "%" and (ls_flag_capitalizzato_filtro = 'S' or ls_flag_capitalizzato_filtro = 'N' ) then
		ls_sql4 += " and prog_manutenzioni_ricambi.flag_capitalizzato = '" + ls_flag_capitalizzato_filtro + "' "
	end if
	
	if ll_anno_riferimento > 0 then
		ls_sql4 += " and ( prog_manutenzioni_ricambi.anno_riferimento = " + string( ll_anno_riferimento) + " or ( prog_manutenzioni_ricambi.anno_riferimento = 0 and programmi_manutenzione.cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S' ) ) )"
	end if	

end if
	
ls_sql = " select distinct programmi_manutenzione.anno_registrazione, " + &
					 " programmi_manutenzione.num_registrazione, " + &
					 " programmi_manutenzione.cod_tipo_manutenzione, " + &
					 " programmi_manutenzione.cod_attrezzatura, " + &
					 " programmi_manutenzione.des_intervento, " + &
					 " programmi_manutenzione.note_idl, " + &
					 " programmi_manutenzione.flag_budget "
ls_sql += " from   programmi_manutenzione "
ls_sql += " where  programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' ) and flag_storico = 'N' and ( flag_blocco = 'N' or (flag_blocco = 'S' and cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S'))) "

		
ldt_data_inizio = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_inizio")
ldt_data_fine = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_fine")
ls_cod_tipo_manut_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione")

if not isnull(ldt_data_inizio) and ldt_data_inizio >= datetime(date("01/01/1900"), 00:00:00) then
	//Donato 21-01-2009 il filtro per data deve puntare alla colonna data_ultima_modifica oppure a data_creazione	
	ls_sql += " and (data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql += " or data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql1 += " and (data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql1 += " or data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//questa era già commentata e la lascio commentata
	//ls_sql2 += " and data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	
	ls_sql3 += " and (data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql3 += " or data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql4 += " and (data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql4 += " or data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//vecchio codice
	/*
	ls_sql += " and data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql1 += " and data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	//ls_sql2 += " and data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql3 += " and data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql4 += " and data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	*/
	//fine modifica -------------------------------------------------------------------------------------------------------------------------------
end if

if not isnull(ldt_data_fine) and ldt_data_fine >= datetime(date("01/01/1900"), 00:00:00) then
	//Donato 21-01-2009 il filtro per data deve puntare alla colonna data_ultima_modifica oppure a data_creazione
	ls_sql += " and (data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql += " or data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql1 += " and (data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql1 += " or data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//questa era già commentata e la lascio commentata
	//ls_sql2 += " and data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	
	ls_sql3 += " and (data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql3 += " or data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql4 += " and (data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql4 += " or data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//vecchio codice
	/*
	ls_sql += " and data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql1 += " and data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	//ls_sql2 += " and data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql3 += " and data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql4 += " and data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	*/
	//fine modifica -------------------------------------------------------------------------------------------------------------------------------
end if

if len(ls_cod_tipo_manut_filtro) < 1 then setnull(ls_cod_tipo_manut_filtro)

if not isnull(ls_cod_tipo_manut_filtro) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
	ls_sql1 += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
//	ls_sql2 += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
	ls_sql3 += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
	ls_sql4 += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
end if

if ll_anno_riferimento > 0 then
	ls_sql += " and ( programmi_manutenzione.anno_riferimento = " + string( ll_anno_riferimento) + " or ( programmi_manutenzione.anno_riferimento = 0 and programmi_manutenzione.cod_tipo_manutenzione IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program = 'S' ) ) )"
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

ls_sql_livello += is_sql_filtro

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
	ls_sql1 += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
//	ls_sql2 += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
	ls_sql3 += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "	
	ls_sql4 += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "	
end if

if not isnull(ls_cod_attrezzatura_filtro) and ls_cod_attrezzatura_filtro <> "" then
	
	if not isnull(ls_flag_sottoattrezzature) and ls_flag_sottoattrezzature = "S" then
//		ls_sql += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
//		ls_sql1 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
//		ls_sql2 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
//		ls_sql3 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
//		ls_sql4 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
		ls_sql += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
		ls_sql1 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
//		ls_sql2 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
		ls_sql3 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
		ls_sql4 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
	else
//		ls_sql += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "' "
//		ls_sql1 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
//		ls_sql2 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
//		ls_sql3 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
//		ls_sql4 += " and cod_attrezzatura like '" + left( ls_cod_attrezzatura_filtro, 3) + "%'  "
		ls_sql += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "' "
		ls_sql1 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
//		ls_sql2 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
		ls_sql3 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
		ls_sql4 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
	end if
end if

if (not isnull(ls_cod_fornitore_filtro) and ls_cod_fornitore_filtro <> "%") and (isnull(ls_cod_prodotto_filtro) or ls_cod_prodotto_filtro = "%") then
	
	ls_flag_risorse_interne = 'N'
	ls_sql = ls_sql1 + " union all "  //+ ls_sql2 + " union "
	
	if ls_ordine_ricerca <> "%" then
		ls_sql += ls_sql4 + " union all "
	end if	
	
	ls_sql += " select null, " + &
						 " null, " + &
						 " '|', " + &
						 " cod_attrezzatura, " + &
						 " null, " + &
						 " null, " + &
						 " null " + &
				" from   anag_attrezzature " + &
				" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_primario = 'S' " + ls_sql_livello	

elseif (not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "%") and (isnull(ls_cod_fornitore_filtro) or ls_cod_fornitore_filtro = "%") then
	
	ls_flag_risorse_esterne = "N"
	ls_flag_risorse_interne = "N"
	ls_sql = ls_sql3 + " union all "
	
	if ls_ordine_ricerca <> "%" then
		ls_sql += ls_sql4 + " union all "
	end if	
	
	ls_sql += " select null, " + &
						 " null, " + &
						 " '|', " + &
						 " cod_attrezzatura, " + &
						 " null, " + &
						 " null, " + &
						 " null " + &
				" from   anag_attrezzature " + &
				" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_primario = 'S' " + ls_sql_livello		
	
elseif not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "%" and not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "%" then

	ls_flag_risorse_interne = 'N'
	//ls_sql = ls_sql1 + " union " + ls_sql2 + " union " + ls_sql3 + " union "
	ls_sql = ls_sql1 + " union all " + ls_sql3 + " union all "
	
	if ls_ordine_ricerca <> "%" then
		ls_sql += ls_sql4 + " union all "
	end if	
	
	ls_sql += " select null, " + &
						 " null, " + &
						 " '|', " + &
						 " cod_attrezzatura, " + &
						 " null, " + &
						 " null, " + &
						 " null " + &
				" from   anag_attrezzature " + &
				" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_primario = 'S' " + ls_sql_livello	

else
	
	if ls_flag_solo_attivita = 'S' then
		
		if ls_ordine_ricerca <> "%" then
			
			ls_sql4 = ls_sql4 + " union all "
			
			ls_sql4 += " select null, " + &
								 " null, " + &
								 " '|', " + &
								 " cod_attrezzatura, " + &
								 " null, " + &
								 " null, " + &
								 " null " + &
						" from   anag_attrezzature " + &
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_primario = 'S' " + ls_sql_livello	
						
			if not isnull(ls_cod_attrezzatura_filtro) and ls_cod_attrezzatura_filtro <> "" then
				
				if not isnull(ls_flag_sottoattrezzature) and ls_flag_sottoattrezzature = "S" then
					ls_sql4 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
				else
					ls_sql4 += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "' "
				end if
			end if	
			
			ls_sql = ls_sql4
			
		else
			ls_sql += " and ( cod_attrezzatura in (select distinct cod_attrezzatura from programmi_manutenzione where cod_azienda = '" + s_cs_xx.cod_azienda + "' ) or cod_attrezzatura IN (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_primario = 'S')) "
		
			// aggiungo union per visualizzare tutte le attrezzature nella lista
			
			ls_sql += " union all "			

			ls_sql += " select null, " + &
								 " null, " + &
								 " '|', " + &
								 " cod_attrezzatura, " + &
								 " null, " + &
								 " null, " + &
								 " null " + &
						" from   anag_attrezzature " + &
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_primario = 'S' " + ls_sql_livello	
						
			if not isnull(ls_cod_attrezzatura_filtro) and ls_cod_attrezzatura_filtro <> "" then
				
				if not isnull(ls_flag_sottoattrezzature) and ls_flag_sottoattrezzature = "S" then
					ls_sql += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
				else
					ls_sql += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "' "
				end if
			end if				
		
		end if	
	
	else
	
		// aggiungo union per visualizzare tutte le attrezzature nella lista
		

		
		if ls_ordine_ricerca <> "%" then
			
			ls_sql4 = ls_sql4 + " union all "
			
			ls_sql4 += " select null, " + &
								 " null, " + &
								 " '|', " + &
								 " cod_attrezzatura, " + &
								 " null, " + &
								 " null, " + &
								 " null " + &
						" from   anag_attrezzature " + &
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello	
						
			if not isnull(ls_cod_attrezzatura_filtro) and ls_cod_attrezzatura_filtro <> "" then
				
				if not isnull(ls_flag_sottoattrezzature) and ls_flag_sottoattrezzature = "S" then
					ls_sql4 += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
				else
					ls_sql4 += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "' "
				end if
			end if		
			
			ls_sql = ls_sql4
			
		else
			ls_sql += " union all "			
			
			ls_sql += " select null, " + &
								 " null, " + &
								 " '|', " + &
								 " cod_attrezzatura, " + &
								 " null, " + &
								 " null, " + &
								 " null " + &
						" from   anag_attrezzature " + &
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello	
						
			if not isnull(ls_cod_attrezzatura_filtro) and ls_cod_attrezzatura_filtro <> "" then
				
				if not isnull(ls_flag_sottoattrezzature) and ls_flag_sottoattrezzature = "S" then
					ls_sql += " and cod_attrezzatura IN ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura_padre = '" + ls_cod_attrezzatura_filtro + "' ) "
				else
					ls_sql += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "' "
				end if
			end if								
		
		end if	
		
	end if
end if

// L'ordinamento ovviamente va alla fine.
ls_sql += " order by cod_attrezzatura ASC, cod_tipo_manutenzione DESC "

// ***		CONNESSIONE A PROGEN

ll_i = wf_registro(true)

if ll_i < 0 then
	setpointer(arrow!)
	g_mb.messagebox( "OMNIA", "Errore durante la connessione a Progen. Operazione Terminata!")
	dw_grid.setredraw( true)
	return -1
end if	

// ***

ls_appo = dw_filtro.getitemstring(dw_filtro.getrow(), "cod_area_aziendale")
		
prepare sqlsa from :ls_sql;

open dynamic cu_lista_programmi;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_lista_programmi (w_manutenzioni:wf_carica_lista_programmi)~r~n" + sqlca.sqlerrtext)
	dw_grid.setredraw(true)
	disconnect using tran_import;
	destroy tran_import;
	setpointer(arrow!)
	return -1
end if

do while true
	fetch cu_lista_programmi into :ll_anno_registrazione, 
	                            :ll_num_registrazione, 
										 :ls_cod_tipo_manutenzione, 
										 :ls_cod_attrezzatura, 
										 :ls_des_intervento,
										 :ls_note_idl,
										 :ls_flag_budget;
										 
   if (sqlca.sqlcode = 100) then exit
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_lista_programmi (w_programmi_manutenzione:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
		close cu_lista_programmi;
		disconnect using tran_import;
		destroy tran_import;		
		dw_grid.setredraw(true)
		setpointer(arrow!)
		return -1
	end if
	
	if ls_cod_attrezzatura = ls_cod_attrezzatura_old and ls_cod_tipo_manutenzione = "|" then continue
	
	// *** leggo attrezzatura padre
	
	if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
		
		select cod_attrezzatura_padre
		into	:ls_cod_attrezzatura_padre
		from	anag_attrezzature
		where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_attrezzatura = :ls_cod_attrezzatura;
				
	else
		setnull(ls_cod_attrezzatura_padre)
	end if
	
	if not isnull(ls_cod_fornitore_filtro) and ls_cod_fornitore_filtro <> "%" and ls_cod_tipo_manutenzione = "|" and (isnull(ls_cod_prodotto_filtro) or ls_cod_prodotto_filtro = "%") then
		
		if ls_ordine_ricerca = "%" then
		
			select cod_area_aziendale
			into   :ls_appo
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_attrezzatura = :ls_cod_attrezzatura;
			
			datastore lds_prova
			lds_prova = create datastore
			lds_prova.dataobject = "d_ds_conteggio_ricambi_for"
			lds_prova.settransobject( sqlca)
			ll_ret = 0
			if ls_flag_capitalizzato_filtro = "" then ls_flag_capitalizzato_filtro = "%"
			ll_ret = lds_prova.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura_padre, ls_cod_fornitore_filtro, ls_flag_capitalizzato_filtro, ls_appo, ls_cod_attrezzatura)
			if ll_ret > 0 then
				ll_ret1 = lds_prova.getitemnumber( 1, 1)
			end if		
			destroy lds_prova
			
		else
			
			ll_ret1 = 0
			
		end if
		
		datastore lds_prova4
		lds_prova4 = create datastore
		lds_prova4.dataobject = "d_ds_conteggio_ricambi_for2"
		lds_prova4.settransobject( sqlca)
		ll_ret = 0
		ll_ret = lds_prova4.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura_padre, ls_cod_fornitore_filtro, ls_flag_capitalizzato_filtro, ls_appo, ls_cod_attrezzatura, ls_ordine_ricerca)
		if ll_ret > 0 then
			ll_ret2 = lds_prova4.getitemnumber( 1, 1)
		end if
		if (ll_ret1 + ll_ret2) < 1 then 
			destroy lds_prova4
			continue		
		end if
	
		destroy lds_prova4				
		
	elseif not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "%" and ls_cod_tipo_manutenzione = "|"  and (isnull(ls_cod_fornitore_filtro) or ls_cod_fornitore_filtro = "%") then
	
		datastore lds_prova1
		lds_prova1 = create datastore
		lds_prova1.dataobject = "d_ds_conteggio_ricambi"
		lds_prova1.settransobject( sqlca)
		ll_ret = 0
		ll_ret = lds_prova1.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura_padre, ls_cod_prodotto_filtro, ls_flag_capitalizzato_filtro, ls_appo, ls_cod_attrezzatura, ls_ordine_ricerca)
		if ll_ret > 0 then
			ll_ret = lds_prova1.getitemnumber( 1, 1)
		end if
		if ll_ret < 1 then 
			destroy lds_prova1
			continue		
		end if
	
		destroy lds_prova1
	elseif not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "%" and ls_cod_tipo_manutenzione = "|"  and not isnull(ls_cod_fornitore_filtro) and ls_cod_fornitore_filtro <> "%" then
	
		if ls_ordine_ricerca = "%" then
	
			datastore lds_prova2
			lds_prova2 = create datastore
			lds_prova2.dataobject = "d_ds_conteggio_ricambi_for"
			lds_prova2.settransobject( sqlca)
			ll_ret = 0
			if ls_flag_capitalizzato_filtro = "" then ls_flag_capitalizzato_filtro = "%"
			ll_ret = lds_prova2.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura_padre, ls_cod_fornitore_filtro, ls_flag_capitalizzato_filtro, ls_appo, ls_cod_attrezzatura)
			if ll_ret > 0 then
				ll_ret1 = lds_prova2.getitemnumber( 1, 1)
			end if
			destroy lds_prova2	
			
		else
			
			ll_ret1 = 0
			
		end if
		
		datastore lds_prova5
		lds_prova5 = create datastore
		lds_prova5.dataobject = "d_ds_conteggio_ricambi_for2"
		lds_prova5.settransobject( sqlca)
		ll_ret = 0
		ll_ret = lds_prova5.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura_padre, ls_cod_fornitore_filtro, ls_flag_capitalizzato_filtro, ls_appo, ls_cod_attrezzatura, ls_ordine_ricerca)
		if ll_ret > 0 then
			ll_ret3 = lds_prova5.getitemnumber( 1, 1)
		end if
		destroy lds_prova5			
		
		datastore lds_prova3
		lds_prova3 = create datastore
		lds_prova3.dataobject = "d_ds_conteggio_ricambi"
		lds_prova3.settransobject( sqlca)
		ll_ret = 0
		ll_ret = lds_prova3.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura_padre, ls_cod_prodotto_filtro, ls_flag_capitalizzato_filtro, ls_appo, ls_cod_attrezzatura, ls_ordine_ricerca)
		if ll_ret > 0 then
			ll_ret2 = lds_prova3.getitemnumber( 1, 1)
		end if
		if (ll_ret1 + ll_ret2 + ll_ret3) < 1 then 
			destroy lds_prova3
			continue		
		end if
	
		destroy lds_prova3	
		
	else
		
		if ls_ordine_ricerca <> "%" then
			
			if ls_flag_solo_attivita = 'S' then
				
				if ll_row > 1 then
				
					select flag_primario
					into   :ls_flag_primario
					from   anag_attrezzature
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_attrezzatura = :ls_cod_attrezzatura_old;			
						
					if ls_flag_primario = 'S' and not isnull(ls_flag_primario) then
						
						if left(ls_cod_attrezzatura,3) <> left(ls_cod_attrezzatura_old,3) and ll_row_old = ll_row then
							
							dw_grid.deleterow( ll_row)
						
						end if
						
					end if
						
				end if
				
			end if						
			
			datastore lds_prova9
			lds_prova9 = create datastore
			lds_prova9.dataobject = "d_ds_conteggio_ricambi_ordine"
			lds_prova9.settransobject( sqlca)
			ll_ret = 0
			if ls_flag_capitalizzato_filtro = "" then ls_flag_capitalizzato_filtro = "%"
			ll_ret = lds_prova9.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura_padre, ls_flag_capitalizzato_filtro, ls_appo, ls_cod_attrezzatura, ls_ordine_ricerca)
			if ll_ret > 0 then
				ll_ret2 = lds_prova9.getitemnumber( 1, 1)
			end if
			if ll_ret2 < 1 or isnull(ll_ret2) or ll_ret2 = 0 then 
				destroy lds_prova9
				continue		
			end if
			
			// *** 
			
			setnull(ls_controllo)
			ll_controllo = 0
			
			select flag_primario
			into   :ls_controllo
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_attrezzatura = :ls_cod_attrezzatura;
					 
			if not isnull(ls_controllo) and sqlca.sqlcode = 0 and ls_controllo = 'N' then
				
				select count(*)
				into   :ll_controllo
				FROM prog_manutenzioni_ricambi LEFT OUTER JOIN programmi_manutenzione ON prog_manutenzioni_ricambi.cod_azienda = programmi_manutenzione.cod_azienda AND prog_manutenzioni_ricambi.anno_registrazione = programmi_manutenzione.anno_registrazione AND prog_manutenzioni_ricambi.num_registrazione = programmi_manutenzione.num_registrazione
				WHERE ( programmi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
						( programmi_manutenzione.cod_attrezzatura like :ls_cod_attrezzatura ) AND  
						( prog_manutenzioni_ricambi.flag_capitalizzato like :ls_flag_capitalizzato_filtro ) AND  
						( prog_manutenzioni_ricambi.ordine_progen = :ls_ordine_ricerca  or prog_manutenzioni_ricambi.ordine_progen_rda = :ls_ordine_ricerca );
						
				if sqlca.sqlcode = 0 and (isnull(ll_controllo) or ll_controllo = 0 ) then
					destroy lds_prova9
					continue
				end if
				
			end if
		
			destroy lds_prova9				
			
		else
		
			if ls_flag_solo_attivita = 'S' then
				
				if ll_row > 1 then
				
					select flag_primario
					into   :ls_flag_primario
					from   anag_attrezzature
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_attrezzatura = :ls_cod_attrezzatura_old;			
						
					if ls_flag_primario = 'S' and not isnull(ls_flag_primario) then
						
						if left(ls_cod_attrezzatura,3) <> left(ls_cod_attrezzatura_old,3) and ll_row_old = ll_row then
							
							dw_grid.deleterow( ll_row)
						
						end if
						
					end if
						
				end if
				
			end if			
			
		end if
	
	end if		
	
	ls_cod_attrezzatura_old = ls_cod_attrezzatura

	if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""
	
	ll_prova = 0
	lb_programmata = false
	
	select count(*)
	into	:ll_prova
	from	tab_tipi_manut_scollegate
	where	cod_azienda  = :s_cs_xx.cod_azienda and
			cod_tipo_manut_collegata = :ls_cod_tipo_manutenzione and
			flag_tipo_man_program = 'S';
		
	if sqlca.sqlcode = 0 and not isnull(ll_prova) and ll_prova > 0 then
		lb_programmata = true
	else
		lb_programmata = false
	end if
	
	ls_des_categoria = ""
	ls_des_reparto = ""
	ls_des_attrezzatura = ""

	select descrizione, 
			 cod_cat_attrezzature, 
			 cod_reparto,
			 flag_primario
	into   :ls_des_attrezzatura, 
			 :ls_cod_categoria, 
			 :ls_cod_reparto,
			 :ls_flag_primario
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura;
			 
	select des_cat_attrezzature
	into   :ls_des_categoria
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cat_attrezzature = :ls_cod_categoria;
	
	select des_reparto
	into   :ls_des_reparto
	from   anag_reparti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_reparto = :ls_cod_reparto;
	
	// inserisco la riga Principale		
	
	if isnull(ls_cod_categoria) then ls_cod_categoria = "_NULL"
	if isnull(ls_cod_reparto) then ls_cod_reparto = "_NULL"
	
	// leggo la priorita
	setnull(ll_priorita)
	
	select priorita,
			 flag_chiuso
	into   :ll_priorita,
	       :ls_cod_stato
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			 
	choose case ls_cod_stato
		case "S"
			ls_des_stato = "Attività Chiusa"
		case else
			ls_des_stato = "Attività Aperta"
	end choose
			 
	if isnull(ll_priorita) then ll_priorita = 1
	
	// *** controllo se l'attrezzatura ha commenti
	
	ll_commenti = 0
	
	select count(*)
	into	:ll_commenti
	from	anag_attrezzature_commenti
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode <> 0 or isnull(ll_commenti) then
		ll_commenti = 0
	end if
			 
	ll_row = dw_grid.insertrow(0)
	ll_row_old = ll_row
	// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
	dw_grid.setitem(ll_row, "flag_tipo_riga", "P")
	dw_grid.setitem(ll_row, "num_riga_appartenenza", 0 )
	dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
	dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )

	dw_grid.setitem(ll_row, "progressivo", 0 )
	
	dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
	dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
	dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
	dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
	dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
	dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
	dw_grid.setitem(ll_row, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
	dw_grid.setitem(ll_row, "des_manutenzione", ls_note_idl)
	dw_grid.setitem(ll_row, "des_intervento", ls_des_intervento)
	dw_grid.setitem(ll_row, "flag_budget", ls_des_intervento)
	dw_grid.setitem(ll_row, "flag_primario", ls_flag_primario)
	dw_grid.setitem(ll_row, "stato_attivita", ls_des_stato)
	dw_grid.setitem(ll_row, "rif_rda", "")
	
	if ll_commenti > 0 then
		dw_grid.setitem(ll_row, "flag_commenti", "S")
	else
		dw_grid.setitem(ll_row, "flag_commenti", "N")
	end if
	
	if lb_programmata then
		dw_grid.setitem(ll_row, "flag_programmata", "S")
	else
		dw_grid.setitem(ll_row, "flag_programmata", "N")
	end if
	
	dw_grid.setitem(ll_row, "prog_visual", 1)
	
	// solo attrezzatura...continuo
	if isnull(ll_anno_registrazione) then 
		setnull(ll_priorita)
		dw_grid.setitem(ll_row, "priorita", ll_priorita)
		continue
	else
		dw_grid.setitem(ll_row, "priorita", ll_priorita)
	end if
	
	

	if ls_flag_ricambi = 'S'	then
	
		IF ls_ordine_ricerca <> "%" then
			
			long ll_ord
			
			ll_ord = 0
			
			select count(*)
			into   :ll_ord
			from   prog_manutenzioni_ricambi
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 ( ordine_progen = :ls_ordine_ricerca or ordine_progen_rda = :ls_ordine_ricerca) ;
					 
			if sqlca.sqlcode = 0 and ( isnull(ll_ord) or ll_ord = 0 ) then continue					 
			
		end if
	
		// verifico se ci sono ricambi associati.
		
		ls_sql_ricambi_2 = ls_sql_ricambi + " AND "
		ls_sql_ricambi_2 += " anno_registrazione = " + string(ll_anno_registrazione) + "  AND 	" + &
								  " num_registrazione  = " + string(ll_num_registrazione) + "  AND 	" + & 
								  " flag_prezzo_certo  like '" + ls_flag_prezzo_certo_filtro + "' and	" + &
								  " flag_capitalizzato like '" + ls_flag_capitalizzato_filtro  + "' "								  
								  
		if not isnull(ls_cod_fornitore_filtro) and ls_cod_fornitore_filtro <> "%" then
	   	ls_sql_ricambi_2 += " and cod_fornitore = '" + ls_cod_fornitore_filtro + "' "
		end if
		if not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "%" then
	   	ls_sql_ricambi_2 += " and cod_prodotto = '" + ls_cod_prodotto_filtro + "' "
		end if
		if not isnull(ls_ordine_ricerca) and ls_ordine_ricerca <> "%" then
	   	ls_sql_ricambi_2 += " and ( ordine_progen = '" + ls_ordine_ricerca + "' or  ordine_progen_rda = '" + ls_ordine_ricerca + "' ) "
		end if
	
		ls_sql_ricambi_2 += " ORDER BY prog_riga_ricambio "
		
		DECLARE cu_ricambi DYNAMIC CURSOR FOR SQLSA ;
		prepare sqlsa from :ls_sql_ricambi_2;
		
		open dynamic cu_ricambi;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in caricamento ricambi (w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
			CLOSE cu_lista_programmi;
			disconnect using tran_import;
			destroy tran_import;			
			dw_grid.setredraw(true)
			setpointer(arrow!)
			return -1
		end if		
	
		do while true
			fetch cu_ricambi into :ll_progressivo, 
										 :ls_cod_ricambio, 
										 :ls_des_ricambio, 
										 :ld_quan_ricambio, 
										 :ld_costo_ricambio, 
										 :ls_flag_prodotto_codificato, 
										 :ls_cod_fornitore, 
										 :ls_cod_misura, 
										 :ldt_data_consegna, 
										 :ls_cod_porto, 
										 :ls_flag_prezzo_certo, 
										 :ls_flag_capitalizzato, 
										 :ll_priorita, 
										 :ls_ordine_progen, 
										 :ls_num_documento, 
										 :ldt_data_documento;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in fetch ricambi (w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
				dw_grid.setredraw(true)
				setpointer(arrow!)
				close cu_lista_programmi;
				close cu_ricambi;
				disconnect using tran_import;
				destroy tran_import;				
				return -1
			end if
			
			select sconto_1_budget,
			       sconto_2_budget,
					 sconto_3_budget,
					 prezzo_netto_budget,
					 cod_stato,
					 tkordi_rda,
					 tkordi,
					 flag_rda,
					 flag_rda_approvata,
					 ordine_progen_rda,
					 sconto_1_eff,   
					sconto_2_eff,   
					sconto_3_eff,   
					prezzo_netto_eff,   
					prezzo_ricambio_eff,   
					quan_eff  
			into   :ld_sconto_1,
			       :ld_sconto_2,
					 :ld_sconto_3,
					 :ld_prezzo_netto,
					 :ls_cod_stato,
					 :ls_tkordi_rda,
					 :ls_tkordi_ricambio,
					 :ls_flag_rda,
					 :ls_flag_rda_approvata,
					 :ls_ordine_progen_rda,
					 :ld_sconto_1_eff,   
					 :ld_sconto_2_eff,   
					 :ld_sconto_3_eff,   
					 :ld_prezzo_netto_eff,   
					 :ld_prezzo_ricambio_eff,   
					 :ld_quan_eff  
			from   prog_manutenzioni_ricambi
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
			       num_registrazione = :ll_num_registrazione and
					 prog_riga_ricambio = :ll_progressivo;
					 
			if isnull(ls_num_documento) then ls_num_documento = ""
			ls_num_documento = ls_num_documento + "(" + string(ldt_data_documento, "dd/mm/yyyy") + ")"
			if isnull(ls_flag_rda_approvata) then ls_flag_rda_approvata = 'N'
			
			if not isnull(ls_cod_stato) and ls_cod_stato <> "" then
				select des_stato
				into   :ls_des_stato
				from   tab_stato_programmi
				where  cod_stato = :ls_cod_stato;
			else
				setnull(ls_des_stato)
			end if		
			
			// *** riferimenti ai buoni d'ingresso
			
			ls_ddi = ""
			
			lb_fatturato = false
			lb_controllo = false
			ls_descrizione = ""
			
			open cu_ricambi_det;
			
			do while true
				fetch cu_ricambi_det into :ls_num_documento,  
												  :ldt_data_documento,
												  :ls_tkboll_ddi,
												  :ls_tkposi_ddi;
				if sqlca.sqlcode <> 0 then exit
				if isnull(ls_num_documento) then ls_num_documento = ""
				ls_ddi += ls_num_documento + "(" + string(ldt_data_documento,"dd/mm/yyyy") + ")"
				ls_descrizione = ""
							
				wf_fatturato( ls_tkboll_ddi, ls_tkposi_ddi, ref ls_errore, ref lb_fatturato, ref ls_descrizione)
				if lb_fatturato then
					if isnull(ls_descrizione) then ls_descrizione = ""
					ls_ddi += " Rif. Fattura: " + ls_descrizione
					lb_controllo = true
				end if				
				
				ls_ddi += "~r~n"
			loop
			
			close cu_ricambi_det;
			
			ll_row = dw_grid.insertrow(0)
			dw_grid.setitem(ll_row, "num_riga_appartenenza", ll_row_old )
			dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
			dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
			dw_grid.setitem(ll_row, "progressivo", ll_progressivo )
			dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
			dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
			dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
			dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
			dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
			dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
			dw_grid.setitem(ll_row, "flag_primario", ls_flag_primario )
			dw_grid.setitem(ll_row, "stato_attivita", ls_des_stato )
			dw_grid.setitem(ll_row, "sconto_1", ld_sconto_1 )
			dw_grid.setitem(ll_row, "sconto_2", ld_sconto_2 )
			dw_grid.setitem(ll_row, "sconto_3", ld_sconto_3 )
			dw_grid.setitem(ll_row, "prezzo_netto", ld_prezzo_netto )
			
			dw_grid.setitem(ll_row, "sconto_1_e", ld_sconto_1_eff )
			dw_grid.setitem(ll_row, "sconto_2_e", ld_sconto_2_eff )
			dw_grid.setitem(ll_row, "sconto_3_e", ld_sconto_3_eff )
			dw_grid.setitem(ll_row, "prezzo_netto_e", ld_prezzo_netto_eff )	
			dw_grid.setitem(ll_row, "quantita_eff", ld_quan_eff)
			if isnull(ld_quan_eff) then ld_quan_eff = 0
			if isnull(ld_prezzo_netto_eff) then ld_prezzo_netto_eff = 0
			ld_tot_costo = ld_quan_eff * ld_prezzo_netto_eff
			dw_grid.setitem(ll_row, "imponibile_e", ld_tot_costo)			
			
			dw_grid.setitem(ll_row, "prog_visual", 2)
			
			// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
			dw_grid.setitem(ll_row, "flag_tipo_riga", "C")
	
			/* flag_tipo risorsa (solo nella riga collegata)
						I = risorsa interna = operaio
						R = ricambio
						E = risorsa esterna
			*/
			dw_grid.setitem(ll_row, "flag_tipo_risorsa", "R")
			
			if not isnull(ls_cod_fornitore) then
			
				select rag_soc_1
				into   :ls_rag_soc_1
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_fornitore = :ls_cod_fornitore;
			else
				ls_cod_fornitore = ""
			end if
						
					 
			if isnull(ls_cod_ricambio) then ls_cod_ricambio = ""
			if isnull(ls_ordine_progen) then ls_ordine_progen = ""


			dw_grid.setitem(ll_row, "rif_ordine", ls_ordine_progen)			
			dw_grid.setitem(ll_row, "cod_ricambio", ls_cod_ricambio)
			dw_grid.setitem(ll_row, "des_ricambio", ls_des_ricambio )
			dw_grid.setitem(ll_row, "quantita", ld_quan_ricambio)
			dw_grid.setitem(ll_row, "prezzo", ld_costo_ricambio)
			ld_tot_costo = ld_quan_ricambio * ld_prezzo_netto
			dw_grid.setitem(ll_row, "imponibile", ld_tot_costo)
			dw_grid.setitem(ll_row, "cod_fornitore", ls_cod_fornitore)
			dw_grid.setitem(ll_row, "rag_soc_1", ls_rag_soc_1)
			dw_grid.setitem(ll_row, "data_consegna", ldt_data_consegna)
			dw_grid.setitem(ll_row, "cod_porto", ls_cod_porto)
			dw_grid.setitem(ll_row, "flag_prezzo_certo", ls_flag_prezzo_certo)
			dw_grid.setitem(ll_row, "flag_capitalizzato", ls_flag_capitalizzato)
			dw_grid.setitem(ll_row, "cod_misura", ls_cod_misura)
			dw_grid.setitem(ll_row, "cod_operaio", ls_null)
			dw_grid.setitem(ll_row, "des_operaio", ls_null)
			dw_grid.setitem(ll_row, "priorita", ll_priorita)
			dw_grid.setitem(ll_row, "rif_ddi", ls_ddi)
			
			if not isnull(ls_tkordi_rda) and ls_tkordi_rda <> "" then
				
				if not isnull(ls_ordine_progen_rda) and ls_ordine_progen_rda <> "" then
					dw_grid.setitem(ll_row, "rif_rda", ls_ordine_progen_rda)
				else
					dw_grid.setitem(ll_row, "rif_rda", ls_tkordi_rda)
				end if
			else
				dw_grid.setitem(ll_row, "rif_rda", "")
			end if
			
			if lb_controllo then
				dw_grid.setitem( ll_row, "fatturato", "S")
			else
				dw_grid.setitem( ll_row, "fatturato", "")
			end if
			
			// *** controllo preordine
			lb_fatturato = false
			wf_preordine( ls_tkordi_ricambio, ref ls_errore, ref lb_fatturato)
			if lb_fatturato then
				dw_grid.setitem( ll_row, "preordine", "S")
			else
				dw_grid.setitem( ll_row, "preordine", "")
			end if	
			
			dw_grid.setitem( ll_row, "flag_rda_approvata", ls_flag_rda_approvata)
			
			if ll_commenti > 0 then
				dw_grid.setitem(ll_row, "flag_commenti", "S")
			else
				dw_grid.setitem(ll_row, "flag_commenti", "N")
			end if			

		loop
		
		close cu_ricambi;
	end if

	if ls_flag_risorse_esterne = 'S' then
		
		// *** Michela 04/12/2006: quando faccio la ricerca per ordine non visualizzo le risorse esterne
		//									e le risorse interne
		
		if ls_ordine_ricerca = '%'then
		
			// verifico se ci sono risorse esterne.	
			open cu_risorse;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in caricamento risorse (w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
				close cu_lista_programmi;
				dw_grid.setredraw(true)
				setpointer(arrow!)
				return -1
			end if		
			
			do while true
				fetch cu_risorse into :ll_progressivo, 
											 :ll_risorsa_minuti_previsti, 
											 :ll_risorsa_ore_previste, 
											 :ls_cod_risorsa_esterna, 
											 :ls_cod_cat_risorse_esterne, 
											 :ld_risorsa_costo_orario, 
											 :ld_risorsa_costi_aggiuntivi, 
											 :ls_flag_prezzo_certo, 
											 :ls_flag_capitalizzato, 
											 :ls_cod_fornitore, 
											 :ll_priorita,
											 :ls_cod_stato;
											 
				if sqlca.sqlcode = 100 then exit
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("OMNIA","Errore in fetch risorse (w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
					dw_grid.setredraw(true)
					setpointer(arrow!)
					close cu_lista_programmi;
					close cu_risorse;
					return -1
				end if
				
				if not isnull(ls_cod_fornitore) then
					
					select rag_soc_1
					into   :ls_rag_soc_1
					from   anag_fornitori
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_fornitore = :ls_cod_fornitore;
							 
				elseif not isnull(ls_cod_risorsa_esterna) then
					
					select cod_fornitore
					into   :ls_cod_fornitore
					from   anag_risorse_esterne
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_risorsa_esterna = :ls_cod_risorsa_esterna and
							 cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne;
							 
					select rag_soc_1
					into   :ls_rag_soc_1
					from   anag_fornitori
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_fornitore = :ls_cod_fornitore;
		
				else
					continue
					
				end if
				
				if not isnull(ls_cod_stato) and ls_cod_stato <> "" then
					select des_stato
					into   :ls_des_stato
					from   tab_stato_programmi
					where  cod_stato = :ls_cod_stato;
				else
					setnull(ls_des_stato)
				end if				
				
				ll_row = dw_grid.insertrow(0)
				dw_grid.setitem(ll_row, "num_riga_appartenenza", ll_row_old )
				dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
				dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
				dw_grid.setitem(ll_row, "progressivo", ll_progressivo )
				dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
				dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
				dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
				dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
				dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
				dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
				dw_grid.setitem(ll_row, "flag_primario", ls_flag_primario )
				dw_grid.setitem(ll_row, "rif_ordine", "/")
				dw_grid.setitem(ll_row, "stato_attivita", ls_des_stato)
				dw_grid.setitem(ll_row, "prog_visual", 3)
				
				// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
				dw_grid.setitem(ll_row, "flag_tipo_riga", "C")
		
				/* flag_tipo risorsa (solo nella riga collegata)
							I = risorsa interna = operaio
							R = ricambio
							E = risorsa esterna
				*/
				dw_grid.setitem(ll_row, "flag_tipo_risorsa", "E")
				
				dw_grid.setitem(ll_row, "cod_fornitore", ls_cod_fornitore )
				dw_grid.setitem(ll_row, "rag_soc_1", ls_rag_soc_1 )
				ld_tot_tempo = ll_risorsa_minuti_previsti / 60
				ld_tot_tempo = ll_risorsa_ore_previste + round(ld_tot_tempo,2)
				dw_grid.setitem(ll_row, "quantita", ll_risorsa_ore_previste + round(ll_risorsa_minuti_previsti / 60,2) )
				dw_grid.setitem(ll_row, "prezzo", ld_risorsa_costo_orario )
				
				ld_tot_costo = ld_tot_tempo * ld_risorsa_costo_orario
				ld_tot_costo += ld_risorsa_costi_aggiuntivi
				dw_grid.setitem(ll_row, "imponibile", ld_tot_costo)
				dw_grid.setitem(ll_row, "cod_porto", ls_cod_porto)
				dw_grid.setitem(ll_row, "flag_prezzo_certo", ls_flag_prezzo_certo)
				dw_grid.setitem(ll_row, "flag_capitalizzato", ls_flag_capitalizzato)
				dw_grid.setitem(ll_row, "priorita", ll_priorita)
				dw_grid.setitem(ll_row, "rif_rda", "")
				dw_grid.setitem(ll_row, "flag_rda_approvata", "")
				
				if ll_commenti > 0 then
					dw_grid.setitem(ll_row, "flag_commenti", "S")
				else
					dw_grid.setitem(ll_row, "flag_commenti", "N")
				end if				
				
			loop
			
			close cu_risorse;
			
		end if
		
	end if
	
	if ls_flag_risorse_interne = 'S' then
		
		// *** Michela 04/12/2006: quando faccio la ricerca per ordine non visualizzo le risorse esterne
		//									e le risorse interne
		
		if ls_ordine_ricerca = '%'then		

			// verifico se ci sono risorse INTERNE.
			
			open cu_operai;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in caricamento operatori (w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
				dw_grid.setredraw(true)
				setpointer(arrow!)
				close cu_lista_programmi;
				return -1
			end if		
			
			do while true
				fetch cu_operai into :ll_progressivo, 
											:ll_operaio_minuti_previsti, 
											:ll_operaio_ore_previste, 
											:ls_cod_operaio, 
											:ld_operaio_costo_orario, 
											:ls_flag_prezzo_certo, 
											:ls_flag_capitalizzato, 
											:ll_priorita,
											:ls_cod_stato,
											:ll_ore_effettive,
											:ll_minuti_effettivi;
											
				if sqlca.sqlcode = 100 then exit
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("OMNIA","Errore in fetch operatori(w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
					dw_grid.setredraw(true)
					setpointer(arrow!)
					close cu_lista_programmi;
					close cu_operai;
					return -1
				end if
		
			
				// carico riga risorsa interna
				if not isnull(ls_cod_operaio) then
					select nome, cognome
					into   :ls_nome, :ls_cognome
					from   anag_operai
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_operaio = :ls_cod_operaio;
							 
					if not isnull(ls_cod_stato) and ls_cod_stato <> "" then
						select des_stato
						into   :ls_des_stato
						from   tab_stato_programmi
						where  cod_stato = :ls_cod_stato;
					else
						setnull(ls_des_stato)
					end if							 
							 
					ll_row = dw_grid.insertrow(0)
					dw_grid.setitem(ll_row, "num_riga_appartenenza", ll_row_old )
					dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
					dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
					dw_grid.setitem(ll_row, "progressivo", ll_progressivo )
					dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
					dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
					dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
					dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
					dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
					dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
					dw_grid.setitem(ll_row, "flag_primario", ls_flag_primario )
					dw_grid.setitem(ll_row, "rif_ordine", "/")
					dw_grid.setitem(ll_row, "stato_attivita", ls_des_stato)
					dw_grid.setitem(ll_row, "prog_visual", 4)
					
					// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
					dw_grid.setitem(ll_row, "flag_tipo_riga", "C")
			
					/* flag_tipo risorsa (solo nella riga collegata)
								I = risorsa interna = operaio
								R = ricambio
								E = risorsa esterna
					*/
					dw_grid.setitem(ll_row, "flag_tipo_risorsa", "I")
					
					dw_grid.setitem(ll_row, "cod_operaio", ls_cod_operaio)
					dw_grid.setitem(ll_row, "des_operaio", ls_cognome + " " + ls_nome )
					
					if isnull(ll_operaio_minuti_previsti) then ll_operaio_minuti_previsti = 0
					if isnull(ll_operaio_ore_previste) then ll_operaio_ore_previste = 0
					ld_tot_tempo = ll_operaio_minuti_previsti / 60
					ld_tot_tempo = ll_operaio_ore_previste + round(ld_tot_tempo,2)
					
					dw_grid.setitem(ll_row, "quantita", ll_operaio_ore_previste + round(ll_operaio_minuti_previsti / 60,2) )
					dw_grid.setitem(ll_row, "prezzo", ld_operaio_costo_orario )
					
					ld_tot_costo = ld_tot_tempo * ld_operaio_costo_orario
					dw_grid.setitem(ll_row, "imponibile", ld_tot_costo)
					
					if isnull(ll_ore_effettive) then ll_ore_effettive = 0
					if isnull(ll_minuti_effettivi) then ll_minuti_effettivi = 0
					ld_tot_tempo = ll_ore_effettive / 60
					ld_tot_tempo = ll_ore_effettive + round(ld_tot_tempo,2)
					
					dw_grid.setitem(ll_row, "quantita_eff", ll_ore_effettive + round(ll_minuti_effettivi / 60,2) )
					if isnull(ld_tot_tempo) then ld_tot_tempo = 0
					if isnull(ld_tot_costo) then ld_tot_costo = 0
					ld_tot_costo = ld_tot_tempo * ld_operaio_costo_orario
					dw_grid.setitem(ll_row, "imponibile_e", ld_tot_costo)					
					
					dw_grid.setitem(ll_row, "cod_porto", ls_cod_porto)
					dw_grid.setitem(ll_row, "flag_prezzo_certo", ls_flag_prezzo_certo)
					dw_grid.setitem(ll_row, "flag_capitalizzato", ls_flag_capitalizzato)
					dw_grid.setitem(ll_row, "priorita", ll_priorita)
					dw_grid.setitem(ll_row, "rif_rda", "")
					dw_grid.setitem(ll_row, "flag_rda_approvata", "")
					
					if ll_commenti > 0 then
						dw_grid.setitem(ll_row, "flag_commenti", "S")
					else
						dw_grid.setitem(ll_row, "flag_commenti", "N")
					end if					
					
				end if	
			
			loop
			
			close cu_operai;
			
		end if
		
	end if
	
loop

close cu_lista_programmi;

dw_grid.setsort("cod_reparto A, cod_categoria A, cod_attrezzatura A, anno_registrazione A, num_registrazione ASC, prog_visual ASC, progressivo ASC")// , flag_tipo_riga D")//, des_manutenzione A")
dw_grid.sort()

dw_grid.groupcalc()
dw_grid.setredraw(true)
setpointer(arrow!)

rollback using tran_import;
disconnect using tran_import;
destroy tran_import;

return 0

end function

public function integer wf_incolla_piano_manutenzione (long fl_riga_copia, long fl_riga_incolla, string fs_operazione);long ll_anno_registrazione, ll_num_registrazione, ll_anno_reg_origine, ll_num_reg_origine, ll_anni[], ll_vuoto[], &
     ll_cont, ll_offset, ll_ret, ll_prova, ll_anno_riferimento, ll_progressivo, ll_ore, ll_minuti, ll_priorita, ll_ore_effettive, ll_minuti_effettivi
string ls_cod_attrezz_copia, ls_cod_attrezz_incolla, ls_cod_tipo_manutenzione, ls_d, ls_cod_stato, ls_flag_budget, ls_des_tipo_manutenzione, ls_cod_operaio, ls_flag_prezzo_certo, ls_flag_capitalizzato, ls_cod_tipo_programma, &
		 ls_flag_confermato
boolean lb_nuovo
dec{4}  ld_costo_orario
datetime	ldt_data_esecuzione

lb_nuovo = false

if fs_operazione = "D" then
	ls_d = "D"
	fs_operazione = "T"
else
	ls_d = ""
end if

ll_anno_reg_origine = dw_grid.getitemnumber(fl_riga_copia, "anno_registrazione")
ll_num_reg_origine  = dw_grid.getitemnumber(fl_riga_copia, "num_registrazione")
ls_cod_tipo_manutenzione = dw_grid.getitemstring(fl_riga_copia, "cod_tipo_manutenzione")
ls_cod_attrezz_copia   = dw_grid.getitemstring(fl_riga_copia,   "cod_attrezzatura")
ls_cod_attrezz_incolla = dw_grid.getitemstring(fl_riga_incolla, "cod_attrezzatura")

ll_anno_registrazione = f_anno_esercizio()

if upper(fs_operazione) = "T" then
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
			 
	if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
		ll_num_registrazione = 0
	else
		ll_num_registrazione ++
	end if
else
	ll_anno_registrazione = ll_anno_reg_origine
	ll_num_registrazione = ll_num_reg_origine
end if

select flag_budget
into	:ls_flag_budget
from	programmi_manutenzione
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_reg_origine and
		num_registrazione = :ll_num_reg_origine;
		
if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"

// *** Michela: sostituisco la funzione f_anno riferimento xkè non mi bastano i controlli che ho messo
//ll_anno_riferimento = f_anno_riferimento()

ll_anno_riferimento = wf_anno_riferimento( ls_cod_tipo_manutenzione, ll_anni)

if ll_anno_riferimento < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il controllo dell'anno di riferimento: l'anno risulta nullo o uguale a zero!", stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_s_1 = ls_flag_budget

if ll_anno_riferimento = 0 then 			// in ll_anni ho gli anni a disposizione

	s_cs_xx.parametri.parametro_d_1_a = ll_anni
	window_open(w_prog_manutenzioni_anni_ctrl, 0)
	
	if s_cs_xx.parametri.parametro_ul_2 > 0 then
		ll_anno_riferimento = s_cs_xx.parametri.parametro_ul_2
		ls_flag_budget = s_cs_xx.parametri.parametro_s_1
		ls_cod_tipo_manutenzione = s_cs_xx.parametri.parametro_s_2
	else
		g_mb.messagebox( "OMNIA", "Operazione interrotta!", stopsign!)
		return -1
	end if
else
	
	s_cs_xx.parametri.parametro_d_1_a = ll_vuoto
	s_cs_xx.parametri.parametro_d_1_a[1] = ll_anno_riferimento
	window_open(w_prog_manutenzioni_anni_ctrl, 0)
	
	if s_cs_xx.parametri.parametro_ul_2 > 0 then
		ll_anno_riferimento = s_cs_xx.parametri.parametro_ul_2
		ls_flag_budget = s_cs_xx.parametri.parametro_s_1
		ls_cod_tipo_manutenzione = s_cs_xx.parametri.parametro_s_2
	else
		g_mb.messagebox( "OMNIA", "Operazione interrotta!", stopsign!)
		return -1
	end if	
	
end if

select stringa
into	:ls_cod_stato
from	parametri
where	cod_parametro = 'MSN';

if upper(fs_operazione) = "T" then

	// verifico presenza del tipo manutenzione ed eventualmente duplico anche quello
	select count(*)
	into   :ll_cont
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezz_incolla AND
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			 
	if ll_cont = 0 then
		
		select des_tipo_manut_scollegata
		into	:ls_des_tipo_manutenzione
		from	tab_tipi_manut_scollegate
		where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_manut_collegata = :ls_cod_tipo_manutenzione;
		
	  INSERT INTO tab_tipi_manutenzione  
				( cod_azienda,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  des_tipo_manutenzione,   
				  tempo_previsto,   
				  flag_manutenzione,   
				  flag_ricambio_codificato,   
				  cod_ricambio,   
				  cod_ricambio_alternativo,   
				  des_ricambio_non_codificato,   
				  num_reg_lista,   
				  num_versione,   
				  num_edizione,   
				  modalita_esecuzione,   
				  cod_tipo_ord_acq,   
				  cod_tipo_off_acq,   
				  cod_tipo_det_acq,   
				  cod_tipo_movimento,   
				  periodicita,   
				  frequenza,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_primario,   
				  cod_misura,   
				  val_min_taratura,   
				  val_max_taratura,   
				  valore_atteso,   
				  cod_versione,   
				  flag_blocco,   
				  data_ultima_modifica,   
				  des_tipo_manutenzione_storico,   
				  data_blocco,   
				  cod_tipo_lista_dist,   
				  cod_lista_dist,   
				  cod_operaio,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna)  
		values (  :s_cs_xx.cod_azienda,   
					:ls_cod_attrezz_incolla,   
					:ls_cod_tipo_manutenzione,   
					:ls_des_tipo_manutenzione,   
					null,   
					'M',   
					'S',   
					null,   
					null,   
					null,   
					null,   
					null,   
					null,   
					'',   
					null,   
					null,   
					null,   
					null,   
					null,   
					1,   
					0,   
					0,   
					null,   
					null,   
					0,   
					0,   
					0,   
					null,   
					'N',   
					null,   
					'',   
					null,   
					null,   
					null,   
					null,   
					null,
					null);
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore durante l'operazione di copia tipo manutenzione~r~n" + sqlca.sqlerrtext)
			return -1
		end if	
		
	end if
	
	
	// procedo con insert programma
	INSERT INTO programmi_manutenzione  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  des_intervento,   
		  cod_attrezzatura,   
		  cod_tipo_manutenzione,   
		  flag_tipo_intervento,   
		  periodicita,   
		  frequenza,   
		  flag_scadenze,   
		  note_idl,   
		  flag_ricambio_codificato,   
		  cod_ricambio,   
		  cod_ricambio_alternativo,   
		  ricambio_non_codificato,   
		  quan_ricambio,   
		  costo_unitario_ricambio,   
		  cod_operaio,   
		  costo_orario_operaio,   
		  operaio_ore_previste,   
		  operaio_minuti_previsti,   
		  cod_cat_risorse_esterne,   
		  cod_risorsa_esterna,   
		  risorsa_ore_previste,   
		  risorsa_minuti_previsti,   
		  risorsa_costo_orario,   
		  risorsa_costi_aggiuntivi,   
		  anno_contratto_assistenza,   
		  num_contratto_assistenza,   
		  anno_ord_acq_risorsa,   
		  num_ord_acq_risorsa,   
		  prog_riga_ord_acq_risorsa,   
		  rif_contratto_assistenza,   
		  rif_ordine_acq_risorsa,   
		  flag_budget,   
		  cod_primario,   
		  cod_misura,   
		  valore_min_taratura,   
		  valore_max_taratura,   
		  valore_atteso,   
		  tot_costo_intervento,   
		  num_reg_lista,   
		  prog_liste_con_comp,   
		  num_versione,   
		  num_edizione,   
		  flag_blocco,   
		  data_blocco,   
		  flag_priorita,   
		  data_forzata,   
		  cod_versione,   
		  anno_reg_progr_padre,   
		  num_reg_progr_padre,   
		  data_creazione,   
		  data_ultima_modifica,   
		  flag_storico,   
		  num_revisione,   
		  commento_modifiche,   
		  des_tipo_manutenzione_storico,   
		  redatto_da,   
		  autorizzato_da,   
		  approvato_da,   
		  cod_tipo_lista_dist,   
		  cod_lista_dist,   
		  data_inizio_val_1,   
		  data_fine_val_1,   
		  data_inizio_val_2,   
		  data_fine_val_2,
		  priorita,
		  cod_stato,   
		  cod_tipo_programma,   
		  flag_chiuso,   
		  flag_attivita_budget,   
		  anno_riferimento,   
		  flag_confermato				  )  
	SELECT cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			des_intervento,   
			:ls_cod_attrezz_incolla,   
			:ls_cod_tipo_manutenzione,   
			flag_tipo_intervento,   
			periodicita,   
			frequenza,   
			flag_scadenze,   
			note_idl,   
			flag_ricambio_codificato,   
			cod_ricambio,   
			cod_ricambio_alternativo,   
			ricambio_non_codificato,   
			quan_ricambio,   
			costo_unitario_ricambio,   
			cod_operaio,   
			costo_orario_operaio,   
			operaio_ore_previste,   
			operaio_minuti_previsti,   
			cod_cat_risorse_esterne,   
			cod_risorsa_esterna,   
			risorsa_ore_previste,   
			risorsa_minuti_previsti,   
			risorsa_costo_orario,   
			risorsa_costi_aggiuntivi,   
			anno_contratto_assistenza,   
			num_contratto_assistenza,   
			anno_ord_acq_risorsa,   
			num_ord_acq_risorsa,   
			prog_riga_ord_acq_risorsa,   
			rif_contratto_assistenza,   
			rif_ordine_acq_risorsa,   
			:ls_flag_budget,   
			cod_primario,   
			cod_misura,   
			valore_min_taratura,   
			valore_max_taratura,   
			valore_atteso,   
			tot_costo_intervento,   
			num_reg_lista,   
			prog_liste_con_comp,   
			num_versione,   
			num_edizione,   
			flag_blocco,   
			data_blocco,   
			flag_priorita,   
			data_forzata,   
			cod_versione,   
			anno_reg_progr_padre,   
			num_reg_progr_padre,   
			data_creazione,   
			data_ultima_modifica,   
			flag_storico,   
			num_revisione,   
			commento_modifiche,   
			des_tipo_manutenzione_storico,   
			redatto_da,   
			autorizzato_da,   
			approvato_da,   
			cod_tipo_lista_dist,   
			cod_lista_dist,   
			data_inizio_val_1,   
			data_fine_val_1,   
			data_inizio_val_2,   
			data_fine_val_2,
			priorita,
			:ls_cod_stato,   
			cod_tipo_programma,   
			'N',   
			:ls_flag_budget,   
			:ll_anno_riferimento,   
			'N'
	 FROM programmi_manutenzione  
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( anno_registrazione = :ll_anno_reg_origine ) AND  
			( num_registrazione = :ll_num_reg_origine )   ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante l'operazione di copia piano manutentivo~r~n" + sqlca.sqlerrtext)
		return -1
	end if
	
	lb_nuovo = true
	
end if

ll_prova = 0

select count(*) 
into   :ll_prova
from   prog_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda AND 
		 anno_registrazione = :ll_anno_reg_origine AND
		 num_registrazione = :ll_num_reg_origine    ;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante il conteggio delle risorse esterne:~r~n" + sqlca.sqlerrtext)
	return -1
end if	

if not isnull(ll_prova) and ll_prova > 0 then
	
	ll_ret = g_mb.messagebox( "OMNIA", "Copiare tutti i ricambi del piano selezionato?", Exclamation!, YESNO!, 2)
	
	if ll_ret = 1 then
		
		ll_ret = wf_incolla_ricambio_piano( ll_anno_reg_origine, ll_num_reg_origine, ll_anno_registrazione, ll_num_registrazione, ls_d, ll_anno_riferimento)					  
		if ll_ret < 0 then
			return -1
		end if	
	
	end if
	
end if

ll_prova = 0

select count(*) 
into   :ll_prova
from   prog_manutenzioni_operai
where  cod_azienda = :s_cs_xx.cod_azienda AND 
		 anno_registrazione = :ll_anno_reg_origine AND
		 num_registrazione = :ll_num_reg_origine    ;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante il conteggio delle risorse interne:~r~n" + sqlca.sqlerrtext)
	return -1
end if	

if not isnull(ll_prova) and ll_prova > 0 then

	ll_ret = g_mb.messagebox( "OMNIA", "Copiare tutte le risorse interne del piano selezionato?", Exclamation!, YESNO!, 2)
	
	if ll_ret = 1 then
		
		declare cu_operai cursor for
		select     progressivo,   
					  cod_operaio,   
					  ore,   
					  minuti,
					  operaio_costo_orario,
					  flag_prezzo_certo,
					  flag_capitalizzato,
					  priorita,
					  cod_tipo_programma,   
					  ore_effettive,   
					  minuti_effettivi,   
					  flag_confermato,   
					  data_esecuzione	
		 from      prog_manutenzioni_operai
		 where   cod_azienda = :s_cs_xx.cod_azienda AND  
				      anno_registrazione = :ll_anno_reg_origine AND  
				      num_registrazione = :ll_num_reg_origine    
		order by  progressivo;		
						
		open cu_operai;
		
		do while true
			fetch cu_operai into :ll_progressivo,
										   :ls_cod_operaio,
											:ll_ore,
											:ll_minuti,
											:ld_costo_orario,
											:ls_flag_prezzo_certo,
											:ls_flag_capitalizzato,
											:ll_priorita,
										   :ls_cod_tipo_programma,   
										  :ll_ore_effettive,   
										  :ll_minuti_effettivi,   
										  :ls_flag_confermato,   
										  :ldt_data_esecuzione	;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore durante l'apertura del cursore cu_operai - l'operazione di copia operatore ~r~n" + sqlca.sqlerrtext)
				return -1				
			end if
			
			if isnull(ll_ore) then ll_ore = 0
			if isnull(ll_minuti) then ll_minuti = 0
			if isnull(ld_costo_orario) then ld_costo_orario = 0
			if isnull(ls_flag_prezzo_certo) then ls_flag_prezzo_certo = "N"
			if isnull(ls_flag_capitalizzato) then ls_flag_capitalizzato = "N"
			if isnull(ll_priorita) then ll_priorita = 0
			if isnull(ll_ore_effettive) then ll_ore_effettive = 0
			if isnull(ll_minuti_effettivi) then ll_minuti_effettivi = 0
			if isnull(ls_flag_confermato) then ls_flag_confermato = "N"
			
			INSERT INTO prog_manutenzioni_operai  
					( cod_azienda,   
					  anno_registrazione,   
					  num_registrazione,   
					  progressivo,   
					  cod_operaio,   
					  ore,   
					  minuti,
					  operaio_costo_orario,
					  flag_prezzo_certo,
					  flag_capitalizzato,
					  priorita,
					  cod_tipo_programma,   
					  cod_stato,   
					  ore_effettive,   
					  minuti_effettivi,   
					  flag_rda,   
					  anno_riferimento,   
					  flag_confermato,   
					  flag_budget,   
					  data_esecuzione	  )
			values ( :s_cs_xx.cod_azienda,   
					  :ll_anno_registrazione,   
					  :ll_num_registrazione,   
					  :ll_progressivo,   
					  :ls_cod_operaio,   
					  :ll_ore,   
					  :ll_minuti,
					  :ld_costo_orario,
					  :ls_flag_prezzo_certo,
					  :ls_flag_capitalizzato,
					  :ll_priorita,
					  :ls_cod_tipo_programma,   
					  :ls_cod_stato,   
					  :ll_ore_effettive,   
					  :ll_minuti_effettivi,   
					  'N',   
					  :ll_anno_riferimento,   
					  :ls_flag_confermato,   
					  :ls_flag_budget,   
					  :ldt_data_esecuzione);
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore durante l'operazione di copia operatore ~r~n" + sqlca.sqlerrtext)
				return -1
			end if			
			
		loop
		
		close cu_operai;
		
	end if
	
end if

return 0
end function

public function integer wf_posiziona_livello (long fl_handle, string fs_livello);long ll_handle, ll_padre, ll_i
string ls_tipo_livello
treeviewitem ltv_item
ws_record lws_record

tv_1.getitem(fl_handle,ltv_item)

lws_record = ltv_item.data
ls_tipo_livello = lws_record.tipo_livello

if ls_tipo_livello = fs_livello then
	// fuori: trivato livello cercato
	return 100
end if

tv_1.ExpandItem(fl_handle)

ll_handle = tv_1.finditem(childtreeitem!,fl_handle)

if ll_handle > 0 then
	wf_posiziona_livello(ll_handle, fs_livello)
end if

ll_handle = tv_1.finditem(nexttreeitem!,fl_handle)

if ll_handle > 0 then
	wf_posiziona_livello(ll_handle, fs_livello)
end if

return 100

end function

public function integer wf_filtra_righe ();string ls_cod_fornitore_filtro, ls_cod_prodotto_filtro,ls_flag_solo_attivita, ls_flag_risorse_interne, ls_flag_risorse_esterne, ls_flag_ricambi, &
       ls_cod_attrezzatura
long   ll_i, ll_anno_registrazione, ll_num_registrazione

dw_filtro.accepttext()

ls_cod_fornitore_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_fornitore")
ls_cod_prodotto_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_prodotto")

ls_flag_solo_attivita = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_solo_attivita")
ls_flag_risorse_interne = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_risorse_interne")
ls_flag_risorse_esterne = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_risorse_esterne")
ls_flag_ricambi = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_ricambi")

if isnull(ls_flag_solo_attivita) then ls_flag_solo_attivita = 'N'
if isnull(ls_flag_risorse_interne) then ls_flag_risorse_interne = 'N'
if isnull(ls_flag_risorse_esterne) then ls_flag_risorse_esterne = 'N'
if isnull(ls_flag_ricambi) then ls_flag_ricambi = 'N'

//for ll_i = 1 to dw_grid.rowcount()
//	
//	ll_anno_registrazione = dw_grid.getitemnumber( ll_i, "anno_registrazione")
//	ll_num_registrazione = dw_grid.getitemnumber( ll_i, "num_registrazione")
//	ls_cod_attrezzatura = dw_grid.getitemstring( ll_i, "cod_attrezzatura")
//	
//	select flag_primario
//	into   :ls_flag_primario
//	from   anag_attrezzature
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_attrezzatura = :ls_cod_attrezzatura;
//			 
//	if not isnull(ls_flag_primario) and ls_flag_primario = "S" then
//		
//		ls_appo = left(ls_cod_attrezzatura, 3)
//		
////		select count(cod_azienda)
//		into   :ll_rec
//		from   prog_manutenzioni_risorse_est
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       anno_registrazione = :ll_anno_registrazione and
//				 num_registrazione = :ll_num_registrazione and
//				 cod_fornitore = :ls_cod_fornitore_filtro and
//				 cod_fornitore is not null;
//				 
//		if isnull(ll_rec) then ll_rec = 0
//		if ll_rec < 1 then continue
//	
//	// *** visualizzo solo le attività combinate del fornitore scelto
//	if not isnull(ls_flag_solo_attivita) and ls_flag_solo_attivita = "S" and not isnull(ls_flag_risorse_esterne) and ls_flag_risorse_esterne = "S" and not isnull(ls_cod_fornitore_filtro) and ls_cod_fornitore_filtro <> "" then
//		
//		ll_rec = 0
//		
//		select count(cod_azienda)
//		into   :ll_rec
//		from   prog_manutenzioni_risorse_est
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       anno_registrazione = :ll_anno_registrazione and
//				 num_registrazione = :ll_num_registrazione and
//				 cod_fornitore = :ls_cod_fornitore_filtro and
//				 cod_fornitore is not null;
//				 
//		if isnull(ll_rec) then ll_rec = 0
//		if ll_rec < 1 then continue
//	
//	end if
//	//	
//	
//	// *** visualizzo solo le attività combinate del prodotto scelto
//	if not isnull(ls_flag_solo_attivita) and ls_flag_solo_attivita = "S" and not isnull(ls_flag_ricambi) and ls_flag_ricambi = "S" and not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "" then
//		
//		ll_rec = 0
//		
//		select count(cod_azienda)
//		into   :ll_rec
//		from   prog_manutenzioni_ricambi
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       anno_registrazione = :ll_anno_registrazione and
//				 num_registrazione = :ll_num_registrazione and
//				 cod_prodotto = :ls_cod_prodotto_filtro and
//				 cod_prodotto is not null;
//				 
//		if isnull(ll_rec) then ll_rec = 0
//		if ll_rec < 1 then continue
//	
//	end if
//	//			
	
	
//next


return 0
end function

public function integer wf_incolla_ricambio (long fl_riga_copia, long fl_riga_incolla, string fs_operazione);long ll_anno_reg_destinazione, ll_num_reg_destinazione, ll_anno_reg_origine, ll_num_reg_origine, ll_anno_riferimento, &
     ll_cont, ll_offset, ll_ret, ll_prog_origine
string ls_cod_attrezz_copia, ls_cod_attrezz_incolla, ls_cod_tipo_manutenzione, ls_des_prodotto
dec{4} ld_quantita, ld_quan_utilizzo

//	***	questo metodo sposta proprio parte del ricambio -- decrementa la quantita nel ricambio da cui proviene

ll_anno_reg_origine = dw_grid.getitemnumber(fl_riga_copia, "anno_registrazione")
ll_num_reg_origine  = dw_grid.getitemnumber(fl_riga_copia, "num_registrazione")
ll_prog_origine = dw_grid.getitemnumber(fl_riga_copia, "progressivo")

ll_anno_reg_destinazione = dw_grid.getitemnumber(fl_riga_incolla, "anno_registrazione")
ll_num_reg_destinazione  = dw_grid.getitemnumber(fl_riga_incolla, "num_registrazione")

// *** Michela 17/10/2007: prendo l'anno di riferimento dall'attività padre quando incollo i ricambi

select anno_riferimento
into	:ll_anno_riferimento
from	programmi_manutenzione
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_reg_destinazione and
		num_registrazione = :ll_num_reg_destinazione;
		
s_cs_xx.parametri.parametro_dec4_1 = 0
s_cs_xx.parametri.parametro_b_1 = false

select des_prodotto,
       quan_utilizzo
into   :ls_des_prodotto,
       :ld_quan_utilizzo
from   prog_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg_origine and
		 num_registrazione = :ll_num_reg_origine and
		 prog_riga_ricambio = :ll_prog_origine;

s_cs_xx.parametri.parametro_s_1 = ls_des_prodotto
s_cs_xx.parametri.parametro_s_2 = string(ld_quan_utilizzo)

window_open(w_rileva_quantita_ricambio, 0)

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)

if not s_cs_xx.parametri.parametro_b_1 then return -1

if not isnull(s_cs_xx.parametri.parametro_dec4_1) and s_cs_xx.parametri.parametro_dec4_1 > 0 then
	ld_quantita = s_cs_xx.parametri.parametro_dec4_1
else
	ld_quantita = 0
end if
		
ll_offset = 0

select max(prog_riga_ricambio)
into   :ll_offset
from   prog_manutenzioni_ricambi  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg_destinazione and
		 num_registrazione = :ll_num_reg_destinazione;
		 
if isnull(ll_offset) or ll_offset = 0 then
	ll_offset = 1
else
	ll_offset ++
end if

INSERT INTO prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio,   
		  flag_prodotto_codificato,   
		  cod_fornitore,   
		  cod_misura,   
		  data_consegna,
		  flag_prezzo_certo,
		  flag_capitalizzato,
		  priorita,
		  cod_porto,
		  tkposi,
		  tkordi,
		  ordine_progen,
		  nrposi,
		  sconto_1_budget,
		  sconto_2_budget,
		  sconto_3_budget,
		  prezzo_netto_budget,
		  sconto_1_eff,
		  sconto_2_eff,
		  sconto_3_eff,
		  prezzo_netto_eff,
		  prezzo_ricambio_eff,
		  tkboll,
		  tkposi_b,
		  nrposi_b,
		  num_documento,
		  data_documento,
		  flag_sconto_valore,
		  cod_stato,   
		  cod_tipo_programma,   
		  quan_consegnata,   
		  flag_evaso,   
		  quan_eff,   
		  flag_rda,   
		  ordine_progen_rda,   
		  tkordi_rda,   
		  tkposi_rda,   
		  nrposi_rda,   
		  anno_riferimento,   
		  flag_confermato,   
		  cod_stato_ordine,   
		  flag_budget,   
		  flag_rda_approvata,   
		  flag_ricambio_prot  		 )  
 SELECT cod_azienda,   
		  :ll_anno_reg_destinazione,   
		  :ll_num_reg_destinazione,   
		  :ll_offset,   
		  cod_prodotto,   
		  des_prodotto,   
		  :ld_quantita,   
		  prezzo_ricambio,   
		  flag_prodotto_codificato,   
		  cod_fornitore,   
		  cod_misura,   
		  data_consegna,
		  flag_prezzo_certo,
		  flag_capitalizzato,
		  priorita,
		  cod_porto,
		  tkposi,
		  tkordi,
		  ordine_progen,
		  nrposi,
		  sconto_1_budget,
		  sconto_2_budget,
		  sconto_3_budget,
		  prezzo_netto_budget,
		  sconto_1_eff,
		  sconto_2_eff,
		  sconto_3_eff,
		  prezzo_netto_eff,
		  prezzo_ricambio_eff,
		  tkboll,
		  tkposi_b,
		  nrposi_b,
		  num_documento,
		  data_documento,
		  flag_sconto_valore,
		  cod_stato,   
		  cod_tipo_programma,   
		  quan_consegnata,   
		  flag_evaso,   
		  quan_eff,   
		  'N',   
		  ordine_progen_rda,   
		  tkordi_rda,   
		  tkposi_rda,   
		  nrposi_rda,   
		  :ll_anno_riferimento,   
		  flag_confermato,   
		  cod_stato_ordine,   
		  flag_budget,   
		  flag_rda_approvata,   
		  flag_ricambio_prot  		  
FROM    prog_manutenzioni_ricambi  
WHERE   cod_azienda = :s_cs_xx.cod_azienda AND  
		  anno_registrazione = :ll_anno_reg_origine AND  
		  num_registrazione = :ll_num_reg_origine    and
		  prog_riga_ricambio = :ll_prog_origine;
		  
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante l'operazione di copia ricambio ~r~n" + sqlca.sqlerrtext)
	return -1
end if

update prog_manutenzioni_ricambi
set    quan_utilizzo = quan_utilizzo - :ld_quantita
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_reg_origine and
		 num_registrazione = :ll_num_reg_origine and
		 prog_riga_ricambio = :ll_prog_origine;
		 
if sqlca.sqlcode <> 0 then 
	g_mb.messagebox( "OMNIA", "Errore durante aggiornamento quantità riga origine: " + sqlca.sqlerrtext)
	return -1
end if

// *** se la quantità = 0 allora elimino la riga principale..

select quan_utilizzo
into   :ld_quantita
from   prog_manutenzioni_ricambi
where		cod_azienda = :s_cs_xx.cod_azienda and
       		anno_registrazione = :ll_anno_reg_origine and
			 num_registrazione = :ll_num_reg_origine and
			 prog_riga_ricambio = :ll_prog_origine;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la select della quantità di utilizzo:" + sqlca.sqlerrtext)
	return -1
end if

if isnull(ld_quantita) or ld_quantita = 0 or ld_quantita < 0 then
	
	delete from prog_manutenzioni_ricambi
	where			cod_azienda = :s_cs_xx.cod_azienda and
	            		anno_registrazione = :ll_anno_reg_origine and
					num_registrazione = :ll_num_reg_origine and
					prog_riga_ricambio = :ll_prog_origine;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la select della quantità di utilizzo:" + sqlca.sqlerrtext)
		return -1
	end if

end if

return 0
end function

public function integer wf_carica_lista_programmi_std (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento lista semplificata dei programmi
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//
// ------------------------------------------------------------------------------------- //

string		ls_sql_ricambi, ls_sql_risorse, ls_sql_operai, ls_null, ls_cod_attrezzatura_filtro, ls_cod_fornitore_filtro, ls_cod_prodotto_filtro, &
				ls_flag_solo_attivita, ls_flag_risorse_interne, ls_flag_risorse_esterne, ls_flag_ricambi, ls_cod_tipo_manut_filtro, ls_sql_livello, &
				ls_sql_attrezzature, ls_sql, ls_sql_attivita, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_intervento, ls_note_idl, ls_sintassi, ls_errore, &
				ls_des_categoria, ls_des_reparto, ls_des_attrezzatura, ls_cod_categoria, ls_cod_reparto, ls_flag_primario, ls_cod_ricambio, ls_des_ricambio, &
				ls_flag_prodotto_codificato, ls_cod_fornitore, ls_cod_misura, ls_rag_soc_1, ls_cod_risorsa_esterna, ls_cod_cat_risorse_esterne, ls_cod_operaio, &
				ls_nome, ls_cognome, ls_des_risorsa, ls_des_tipo_manut, ls_non_prog, ls_prog, ls_parziali

long			ll_anno_registrazione, ll_num_registrazione, ll_progressivo, ll_tipo_riga, ll_i, ll_righe, ll_row, ll_row_old, ll_cont_1, ll_cont_2

dec{4}		ld_quan_ricambio, ld_costo_ricambio, ld_prezzo_netto, ld_risorsa_minuti_previsti, ld_risorsa_ore_previste, ld_risorsa_costo_orario, &
				ld_risorsa_costi_aggiuntivi, ld_operaio_minuti_previsti, ld_operaio_ore_previste, ld_tot_tempo, ld_tot_costo, ld_operaio_costo_orario

datetime 	ldt_data_ultima_modifica, ldt_data_inizio, ldt_data_fine, ldt_data_consegna, ldt_data_documento

datastore   lds_righe

if ib_grid = false then return 0

ls_sql_attivita = "  SELECT programmi_manutenzione.cod_attrezzatura, " + &
						"			 programmi_manutenzione.cod_tipo_manutenzione,    " + &
						"			 programmi_manutenzione.anno_registrazione, " + &
					   "			 programmi_manutenzione.num_registrazione,  " + &  
					   "			 0,    " + &					   
					   "			 programmi_manutenzione.des_intervento,    " + &
					   "			 programmi_manutenzione.note_idl,    " + &
					   "			 2 as tipo_riga   " + &
					   "	FROM   programmi_manutenzione   " + &
					   "	WHERE ( programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'  ) " 

ls_sql_ricambi  = "  SELECT programmi_manutenzione.cod_attrezzatura,    " + &
					   "			 programmi_manutenzione.cod_tipo_manutenzione,    " + &
						"			 programmi_manutenzione.anno_registrazione, " + &
					   "			 programmi_manutenzione.num_registrazione,  " + &  
					   "			 prog_manutenzioni_ricambi.prog_riga_ricambio,    " + &
					   "			 programmi_manutenzione.des_intervento,    " + &
					   "			 programmi_manutenzione.note_idl,    " + &
					   "			 3 as tipo_riga   " + &
					   "	FROM {oj programmi_manutenzione LEFT OUTER JOIN prog_manutenzioni_ricambi ON programmi_manutenzione.cod_azienda = prog_manutenzioni_ricambi.cod_azienda AND programmi_manutenzione.anno_registrazione = prog_manutenzioni_ricambi.anno_registrazione AND programmi_manutenzione.num_registrazione = prog_manutenzioni_ricambi.num_registrazione}   " + &
					   "	WHERE ( programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'  ) "
						
ls_sql_risorse  = "   SELECT programmi_manutenzione.cod_attrezzatura,    " + &
					  "			 programmi_manutenzione.cod_tipo_manutenzione,    " + &
					  "			  programmi_manutenzione.anno_registrazione, " + &
					  "			 programmi_manutenzione.num_registrazione,  " + &  
					  "			 prog_manutenzioni_risorse_est.progressivo,    " + &
					  "			 programmi_manutenzione.des_intervento,    " + &
					  "			 programmi_manutenzione.note_idl,    " + &
					  "			 4 as tipo_riga   " + &
					  "	FROM {oj programmi_manutenzione LEFT OUTER JOIN prog_manutenzioni_risorse_est ON programmi_manutenzione.cod_azienda = prog_manutenzioni_risorse_est.cod_azienda AND programmi_manutenzione.anno_registrazione = prog_manutenzioni_risorse_est.anno_registrazione AND programmi_manutenzione.num_registrazione = prog_manutenzioni_risorse_est.num_registrazione}   " + &
					  "	WHERE ( programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "

ls_sql_operai  = "   SELECT programmi_manutenzione.cod_attrezzatura,    " + &
					  "			 programmi_manutenzione.cod_tipo_manutenzione,    " + &
					  "			 programmi_manutenzione.anno_registrazione, " + &
					  "			 programmi_manutenzione.num_registrazione,  " + &  
					  "			 prog_manutenzioni_operai.progressivo,    " + &
					  "			 programmi_manutenzione.des_intervento,    " + &
					  "			 programmi_manutenzione.note_idl,    " + &
					  "			 5 as tipo_riga   " + &
					  "	FROM {oj programmi_manutenzione LEFT OUTER JOIN prog_manutenzioni_operai ON programmi_manutenzione.cod_azienda = prog_manutenzioni_operai.cod_azienda AND programmi_manutenzione.anno_registrazione = prog_manutenzioni_operai.anno_registrazione AND programmi_manutenzione.num_registrazione = prog_manutenzioni_operai.num_registrazione}   " + &
					  "	WHERE ( programmi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "

ls_sql_attrezzature = "  SELECT cod_attrezzatura,    " + &
					  "			 	  '|' as cod_tipo_manutenzione,    " + &
					  "				  0 as anno_registrazione, " + &
					  "				  0 as num_registrazione, " + &  
					  "			 	  0 as progressivo,    " + &
					  "			 	  descrizione,    " + &
					  "			 	  '|',    " + &
					  "			 	  1 as tipo_riga   " + &
					  " from anag_attrezzature  " + &
					  " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

setnull(ls_null)

dw_grid.reset()
dw_grid.setredraw(false)
setpointer(hourglass!)

ls_cod_attrezzatura_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura")
ls_cod_fornitore_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_fornitore")
ls_cod_prodotto_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_prodotto")
ls_flag_solo_attivita = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_solo_attivita")
ls_flag_risorse_interne = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_risorse_interne")
ls_flag_risorse_esterne = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_risorse_esterne")
ls_flag_ricambi = dw_filtro.getitemstring( dw_filtro.getrow(), "flag_ricambi")
ldt_data_inizio = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_inizio")
ldt_data_fine = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_fine")
ls_cod_tipo_manut_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione")

ls_non_prog = dw_filtro.getitemstring(dw_filtro.getrow(), "flag_non_prog")
ls_prog = dw_filtro.getitemstring(dw_filtro.getrow(), "flag_prog")
ls_parziali = dw_filtro.getitemstring(dw_filtro.getrow(), "flag_parziali")

if isnull(ls_flag_solo_attivita) then ls_flag_solo_attivita = 'N'
if isnull(ls_flag_risorse_interne) then ls_flag_risorse_interne = 'N'
if isnull(ls_flag_risorse_esterne) then ls_flag_risorse_esterne = 'N'
if isnull(ls_flag_ricambi) then ls_flag_ricambi = 'N'

if not isnull(ls_cod_fornitore_filtro) and ls_cod_fornitore_filtro <> "" then
	ls_sql_ricambi += " and prog_manutenzioni_ricambi.cod_fornitore = '" + ls_cod_fornitore_filtro + "' "
end if

if not isnull(ls_cod_prodotto_filtro) and ls_cod_prodotto_filtro <> "" then
	ls_sql_ricambi += " and prog_manutenzioni_ricambi.cod_prodotto = '" + ls_cod_prodotto_filtro + "' "
end if

if not isnull(ls_cod_tipo_manut_filtro) then 
	ls_sql_ricambi += " and programmi_manutenzione.cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
	ls_sql_risorse += " and programmi_manutenzione.cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
	ls_sql_operai += " and programmi_manutenzione.cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
	ls_sql_attivita += " and programmi_manutenzione.cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
end if

if not isnull(ldt_data_inizio) and ldt_data_inizio >= datetime(date("01/01/1900"), 00:00:00) then
	//Donato 21-01-2009 il filtro per data deve puntare alla colonna data_ultima_modifica oppure a data_creazione	
	ls_sql_ricambi += " and (programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_ricambi += " or programmi_manutenzione.data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql_risorse += " and (programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " or programmi_manutenzione.data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql_operai += " and (programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operai += " or programmi_manutenzione.data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql_attivita += " and (programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_attivita += " or programmi_manutenzione.data_creazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//vecchio codice
	/*
	ls_sql_ricambi += " and programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " and programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operai += " and programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_attivita += " and programmi_manutenzione.data_ultima_modifica >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	*/
	//fine modifica ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end if

if not isnull(ldt_data_fine) and ldt_data_fine >= datetime(date("01/01/1900"), 00:00:00) then
	//Donato 21-01-2009 il filtro per data deve puntare alla colonna data_ultima_modifica oppure a data_creazione	
	ls_sql_ricambi += " and (programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_ricambi += " or programmi_manutenzione.data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql_risorse += " and (programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " or programmi_manutenzione.data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql_operai += " and (programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operai += " or programmi_manutenzione.data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	ls_sql_attivita += " and (programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_attivita += " or programmi_manutenzione.data_creazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	
	//vecchio codice
	/*
	ls_sql_ricambi += " and programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " and programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operai += " and programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_attivita += " and programmi_manutenzione.data_ultima_modifica <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	*/
	//fine modifica ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

ls_sql_livello += is_sql_filtro

if len(trim(ls_sql_livello)) > 0 then
	ls_sql_ricambi += " and programmi_manutenzione.cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
	ls_sql_risorse += " and programmi_manutenzione.cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
	ls_sql_operai += " and programmi_manutenzione.cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
	ls_sql_attivita += " and programmi_manutenzione.cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
	ls_sql_attrezzature += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
end if

if not isnull(ls_cod_attrezzatura_filtro) and ls_cod_attrezzatura_filtro <> "" then
	ls_sql_ricambi += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "' "
	ls_sql_risorse += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "'  "
	ls_sql_operai += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "'  "
	ls_sql_attrezzature += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "'  "
	ls_sql_attivita += " and cod_attrezzatura = '" + ls_cod_attrezzatura_filtro + "'  "
end if

ls_sql = ls_sql_attrezzature + " union all " + ls_sql_operai + " union all " + ls_sql_risorse + " union all " + ls_sql_ricambi
ls_sql += " order by 1 ASC, 2 ASC, 3 ASC, 4 ASC, 5 ASC, 8 ASC "

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)
if len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi:" + ls_errore)
	return -1
end if

lds_righe = create datastore

lds_righe.Create( ls_sintassi, ls_errore)
if len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	return -1	
end if

lds_righe.settransobject( sqlca)
ll_righe = lds_righe.retrieve()

if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve del datastore:" + sqlca.sqlerrtext)
	return -1
end if

for ll_i = 1 to lds_righe.rowcount()
	
	ls_cod_attrezzatura = lds_righe.getitemstring( ll_i, 1)
   ls_cod_tipo_manutenzione = lds_righe.getitemstring( ll_i, 2)
	ll_anno_registrazione = lds_righe.getitemnumber( ll_i, 3)
   ll_num_registrazione = lds_righe.getitemnumber( ll_i, 4)
	ll_progressivo = lds_righe.getitemnumber( ll_i, 5)
	ls_des_intervento = lds_righe.getitemstring( ll_i, 6)
	ls_note_idl = lds_righe.getitemstring( ll_i, 7)
	ll_tipo_riga = lds_righe.getitemnumber( ll_i, 8)
	
	if isnull(ls_cod_tipo_manutenzione) or ls_cod_tipo_manutenzione = "" then continue
	
	setnull(ls_des_tipo_manut)
	
	select des_tipo_manutenzione
	into   :ls_des_tipo_manut
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			 
	if isnull(ls_des_tipo_manut) then ls_des_tipo_manut = ""
	ls_des_tipo_manut += " (PROGR. " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + ")"
		
	if ls_flag_ricambi = "N" then
		if ll_tipo_riga = 3 then continue
	end if
	
	if ls_flag_risorse_esterne = "N" then
		if ll_tipo_riga = 4 then continue
	end if
	
	if ls_flag_risorse_interne = "N" then
		if ll_tipo_riga = 5 then continue
	end if
		
	ls_des_categoria = ""
	ls_des_reparto = ""
	ls_des_attrezzatura = ""

	select descrizione, 
			 cod_cat_attrezzature, 
			 cod_reparto,
			 flag_primario
	into   :ls_des_attrezzatura, 
			 :ls_cod_categoria, 
			 :ls_cod_reparto,
			 :ls_flag_primario
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura;
			 
	select des_cat_attrezzature
	into   :ls_des_categoria
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cat_attrezzature = :ls_cod_categoria;
	
	select des_reparto
	into   :ls_des_reparto
	from   anag_reparti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_reparto = :ls_cod_reparto;
			 
	// ***
	
	ll_cont_1 = 0	
	ll_cont_2 = 0
	

	select count(*) 
	into   :ll_cont_1
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda  and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 (flag_blocco is null or flag_blocco = 'N');
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in conteggio tipi manutenzioni: " + sqlca.sqlerrtext)
		destroy lds_righe;
		return -1
	end if
	
	if not isnull(ll_cont_1) and ll_cont_1 > 0 then	
		select count(*)
		into   :ll_cont_2
		from   programmi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda  and
				 cod_attrezzatura = :ls_cod_attrezzatura;
				 
		if sqlca.sqlcode < 0 then			
			g_mb.messagebox("OMNIA","Errore in conteggio programmi: " + sqlca.sqlerrtext)			
			destroy lds_righe;
			return -1
		end if		
	
		if isnull(ll_cont_2) then ll_cont_2 = 0
	
		if ll_cont_2 > 0 then
	
			select count(*)
			into   :ll_cont_2
			from   tab_tipi_manutenzione
			where  cod_azienda = :s_cs_xx.cod_azienda  and
					 cod_attrezzatura = :ls_cod_attrezzatura and
					 cod_tipo_manutenzione in (select cod_tipo_manutenzione
														from   programmi_manutenzione
														where  cod_azienda = :s_cs_xx.cod_azienda  and
																 cod_attrezzatura = :ls_cod_attrezzatura) and
					(flag_blocco is null or flag_blocco = 'N');
			
			if sqlca.sqlcode < 0 then				
				g_mb.messagebox("OMNIA","Errore in conteggio tipologie non programmate: " + sqlca.sqlerrtext)				
				destroy lds_righe;				
				return -1				
			end if		
		end if		
	end if
	
	if ll_cont_1 = 0 then
		if ls_non_prog = "N" then continue
	else
		if ll_cont_2 = 0 then
			if ls_non_prog = "N" then continue
		else
			if ll_cont_1 = ll_cont_2 then
				if ls_prog = "N" then continue
			else
				if ls_parziali = "N" then continue
			end if
		end if
	end if

	// inserisco la riga Principale		
	
	if isnull(ls_cod_categoria) then ls_cod_categoria = "_NULL"
	if isnull(ls_cod_reparto) then ls_cod_reparto = "_NULL"
	
	if ll_tipo_riga = 1 or ( not isnull(ll_anno_registrazione) and ll_anno_registrazione > 0 and not isnull(ll_num_registrazione) and ll_num_registrazione > 0 and ( isnull(ll_progressivo) or ll_progressivo = 0 ) ) then		 
		
		if ll_tipo_riga = 1 then
			ll_row = dw_grid.insertrow(0)
			ll_row_old = ll_row
			
			// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
			
			dw_grid.setitem(ll_row, "flag_tipo_riga", "P")
			dw_grid.setitem(ll_row, "num_riga_appartenenza", 0 )
			dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
			dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
			dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
			dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
			dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
			dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
			dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
			dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
			dw_grid.setitem(ll_row, "cod_tipo_manutenzione", "")
			dw_grid.setitem(ll_row, "des_manutenzione", "")
			dw_grid.setitem(ll_row, "des_intervento", ls_des_intervento)
			dw_grid.setitem(ll_row, "prog_visual", 1)
		else
			
			if ll_i < ll_righe then
				
				long ll_appo_1, ll_appo_2
				
				ll_appo_1 = ll_progressivo
				ll_appo_2 = lds_righe.getitemnumber( ll_i + 1, 5)
				
				if isnull(ll_appo_1) then ll_appo_1 = 0
				if isnull(ll_appo_2) then ll_appo_2 = 0				
				
				if ll_anno_registrazione = lds_righe.getitemnumber( ll_i + 1, 3) and ll_num_registrazione = lds_righe.getitemnumber( ll_i + 1, 4) and ll_appo_1 = ll_appo_2  then 
					continue
				elseif ll_anno_registrazione = lds_righe.getitemnumber( ll_i + 1, 3) and ll_num_registrazione = lds_righe.getitemnumber( ll_i + 1, 4) then
					continue
				else
					
					ll_row = dw_grid.insertrow(0)
					ll_row_old = ll_row
					
					// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
					
					dw_grid.setitem(ll_row, "flag_tipo_riga", "P")
					dw_grid.setitem(ll_row, "num_riga_appartenenza", 0 )
					dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
					dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
					dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
					dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
					dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
					dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
					dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
					dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
					dw_grid.setitem(ll_row, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
					dw_grid.setitem(ll_row, "des_manutenzione", ls_des_tipo_manut) //ls_note_idl)
					dw_grid.setitem(ll_row, "des_intervento", ls_des_intervento)
					dw_grid.setitem(ll_row, "note_idl", ls_note_idl)
					dw_grid.setitem(ll_row, "prog_visual", 1)					
				end if				
			end if			
			
		end if
	
	elseif ll_tipo_riga = 3	and not isnull(ll_anno_registrazione) and ll_anno_registrazione > 0 and not isnull(ll_num_registrazione) and ll_num_registrazione > 0 and not isnull(ll_progressivo) and ll_progressivo > 0 then
	
		SELECT cod_prodotto,   
				 des_prodotto,   
				 quan_utilizzo,   
				 prezzo_ricambio,   
				 flag_prodotto_codificato,   
				 cod_fornitore,   
				 cod_misura,   
				 data_consegna,   
				 prezzo_netto_budget  
		into   :ls_cod_ricambio,   
				 :ls_des_ricambio,   
				 :ld_quan_ricambio,   
				 :ld_costo_ricambio,   
				 :ls_flag_prodotto_codificato,   
				 :ls_cod_fornitore,   
				 :ls_cod_misura,   
				 :ldt_data_consegna,
				 :ld_prezzo_netto
		from   prog_manutenzioni_ricambi  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
			 	 anno_registrazione = :ll_anno_registrazione AND  
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ricambio = :ll_progressivo;	 	
	
		if sqlca.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox("OMNIA","Errore in caricamento ricambi (w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
			destroy lds_righe;
			dw_grid.setredraw(true)			
			return -1
		end if		

		ll_row = dw_grid.insertrow(0)
		dw_grid.setitem(ll_row, "num_riga_appartenenza", ll_row_old )
		dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
		dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
		dw_grid.setitem(ll_row, "progressivo", ll_progressivo )
		dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
		dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
		dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
		dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
		dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
		dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
		dw_grid.setitem(ll_row, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
		dw_grid.setitem(ll_row, "des_manutenzione", ls_des_tipo_manut) //ls_note_idl)
		dw_grid.setitem(ll_row, "des_intervento", ls_des_intervento)
		dw_grid.setitem(ll_row, "note_idl", ls_note_idl)
		dw_grid.setitem(ll_row, "prezzo_netto", ld_prezzo_netto )
		dw_grid.setitem(ll_row, "flag_tipo_riga", "C")
		dw_grid.setitem(ll_row, "flag_tipo_risorsa", "R")
		
		if not isnull(ls_cod_fornitore) then
		
			select rag_soc_1
			into   :ls_rag_soc_1
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_fornitore = :ls_cod_fornitore;
		else
			ls_cod_fornitore = ""
		end if
					
				 
		if isnull(ls_cod_ricambio) then ls_cod_ricambio = ""

		dw_grid.setitem(ll_row, "cod_ricambio", ls_cod_ricambio)
		dw_grid.setitem(ll_row, "des_ricambio", ls_des_ricambio )
		dw_grid.setitem(ll_row, "quantita", ld_quan_ricambio)
		dw_grid.setitem(ll_row, "prezzo", ld_costo_ricambio)
		dw_grid.setitem(ll_row, "imponibile", ld_quan_ricambio * ld_prezzo_netto)
		dw_grid.setitem(ll_row, "cod_fornitore", ls_cod_fornitore)
		dw_grid.setitem(ll_row, "rag_soc_1", ls_rag_soc_1)
		dw_grid.setitem(ll_row, "data_consegna", ldt_data_consegna)
		dw_grid.setitem(ll_row, "cod_misura", ls_cod_misura)
		dw_grid.setitem(ll_row, "cod_operaio", ls_null)
		dw_grid.setitem(ll_row, "des_operaio", ls_null)
		dw_grid.setitem(ll_row, "prog_visual", 2)

	elseif ll_tipo_riga = 4	and not isnull(ll_anno_registrazione) and ll_anno_registrazione > 0 and not isnull(ll_num_registrazione) and ll_num_registrazione > 0 and not isnull(ll_progressivo) and ll_progressivo > 0 then
	
		SELECT minuti,   
				 ore,   
				 cod_risorsa_esterna,   
				 cod_cat_risorse_esterne,
				 risorsa_costo_orario,
				 risorsa_costi_aggiuntivi
		into   :ld_risorsa_minuti_previsti,
				 :ld_risorsa_ore_previste,
				 :ls_cod_risorsa_esterna,
				 :ls_cod_cat_risorse_esterne,
				 :ld_risorsa_costo_orario,
				 :ld_risorsa_costi_aggiuntivi
		FROM   prog_manutenzioni_risorse_est  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
				 anno_registrazione = :ll_anno_registrazione AND  
				 num_registrazione = :ll_num_registrazione and
				 progressivo = :ll_progressivo;
				 
		if sqlca.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox("OMNIA","Errore in fetch risorse (w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
			dw_grid.setredraw(true)			
			destroy lds_righe;
			return -1
		end if
		
		select descrizione
		into   :ls_des_risorsa
		from   anag_risorse_esterne
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
				 cod_risorsa_esterna = :ls_cod_risorsa_esterna;
				
		ll_row = dw_grid.insertrow(0)
		dw_grid.setitem(ll_row, "num_riga_appartenenza", ll_row_old )
		dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
		dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
		dw_grid.setitem(ll_row, "progressivo", ll_progressivo )
		dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
		dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
		dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
		dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
		dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
		dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
		dw_grid.setitem(ll_row, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
		dw_grid.setitem(ll_row, "des_manutenzione", ls_des_tipo_manut) //ls_note_idl)
		dw_grid.setitem(ll_row, "des_intervento", ls_des_intervento)	
		dw_grid.setitem(ll_row, "note_idl", ls_note_idl)
		dw_grid.setitem(ll_row, "flag_tipo_riga", "C")
		dw_grid.setitem(ll_row, "flag_tipo_risorsa", "E")		
		dw_grid.setitem(ll_row, "cod_fornitore", ls_cod_risorsa_esterna)
		dw_grid.setitem(ll_row, "rag_soc_1", ls_des_risorsa)
		
		if isnull(ld_risorsa_costo_orario) then ld_risorsa_costo_orario = 0
		if isnull(ld_risorsa_costi_aggiuntivi) then ld_risorsa_costi_aggiuntivi = 0
		
		ld_tot_tempo = ld_risorsa_minuti_previsti / 60
		ld_tot_tempo = ld_risorsa_ore_previste + round(ld_tot_tempo,2)
		
		dw_grid.setitem(ll_row, "quantita", ld_risorsa_ore_previste + round(ld_risorsa_minuti_previsti / 60,2) )
		dw_grid.setitem(ll_row, "prezzo", ld_risorsa_costo_orario )
		
		ld_tot_costo = ld_tot_tempo * ld_risorsa_costo_orario
		dw_grid.setitem(ll_row, "imponibile", ld_tot_costo)		
		dw_grid.setitem(ll_row, "prog_visual", 3)
		
	elseif ll_tipo_riga = 5	and not isnull(ll_anno_registrazione) and ll_anno_registrazione > 0 and not isnull(ll_num_registrazione) and ll_num_registrazione > 0 and not isnull(ll_progressivo) and ll_progressivo > 0 then

		SELECT minuti,   
				 ore,   
				 cod_operaio,
				 operaio_costo_orario
		into   :ld_operaio_minuti_previsti,
		       :ld_operaio_ore_previste,
				 :ls_cod_operaio,
				 :ld_operaio_costo_orario
		FROM   prog_manutenzioni_operai  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
				 anno_registrazione = :ll_anno_registrazione AND  
				 num_registrazione = :ll_num_registrazione and
				 progressivo = :ll_progressivo;

		if sqlca.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox("OMNIA","Errore in fetch operatori(w_programmi_manutenzione:wf_lista_programmi)~r~n" + sqlca.sqlerrtext)
			dw_grid.setredraw(true)			
			destroy lds_righe;
			return -1
		end if

		select nome, cognome
		into   :ls_nome, :ls_cognome
		from   anag_operai
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_operaio = :ls_cod_operaio;
					 
		ll_row = dw_grid.insertrow(0)
		dw_grid.setitem(ll_row, "num_riga_appartenenza", ll_row_old )
		dw_grid.setitem(ll_row, "anno_registrazione", ll_anno_registrazione )
		dw_grid.setitem(ll_row, "num_registrazione", ll_num_registrazione )
		dw_grid.setitem(ll_row, "progressivo", ll_progressivo )
		dw_grid.setitem(ll_row, "cod_reparto", ls_cod_reparto )
		dw_grid.setitem(ll_row, "cod_categoria", ls_cod_categoria )
		dw_grid.setitem(ll_row, "des_reparto", ls_des_reparto )
		dw_grid.setitem(ll_row, "des_categoria", ls_des_categoria )
		dw_grid.setitem(ll_row, "cod_attrezzatura", ls_cod_attrezzatura )
		dw_grid.setitem(ll_row, "des_attrezzatura", ls_des_attrezzatura )
		dw_grid.setitem(ll_row, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
		dw_grid.setitem(ll_row, "des_manutenzione", ls_des_tipo_manut) //ls_note_idl)
		dw_grid.setitem(ll_row, "des_intervento", ls_des_intervento)	
		dw_grid.setitem(ll_row, "note_idl", ls_note_idl)
		dw_grid.setitem(ll_row, "flag_tipo_riga", "C")
		dw_grid.setitem(ll_row, "flag_tipo_risorsa", "I")
			
		dw_grid.setitem(ll_row, "cod_operaio", ls_cod_operaio)
		dw_grid.setitem(ll_row, "des_operaio", ls_cognome + " " + ls_nome )
		if isnull(ld_operaio_minuti_previsti) then ld_operaio_minuti_previsti = 0
		if isnull(ld_operaio_ore_previste) then ld_operaio_ore_previste = 0
		ld_tot_tempo = ld_operaio_minuti_previsti / 60
		ld_tot_tempo = ld_operaio_ore_previste + round(ld_tot_tempo,2)
		
		dw_grid.setitem(ll_row, "quantita", ld_operaio_ore_previste + round(ld_operaio_minuti_previsti / 60,2) )
		dw_grid.setitem(ll_row, "prezzo", ld_operaio_costo_orario )
		
		ld_tot_costo = ld_tot_tempo * ld_operaio_costo_orario
		dw_grid.setitem(ll_row, "imponibile", ld_tot_costo)
		dw_grid.setitem(ll_row, "prog_visual", 4)

	end if
	
next

dw_grid.setsort("cod_reparto A, cod_categoria A, cod_attrezzatura A, cod_tipo_manutenzione A, anno_registrazione A, num_registrazione A, prog_visual A, progressivo A ")// , flag_tipo_riga D")//, des_manutenzione A")
dw_grid.sort()

dw_grid.groupcalc()
dw_grid.setredraw(true)
setpointer(arrow!)


return 0

end function

public function integer wf_incolla_ricambio_std (long fl_riga_copia, long fl_riga_incolla, string fs_operazione);long ll_anno_reg_destinazione, ll_num_reg_destinazione, ll_anno_reg_origine, ll_num_reg_origine,&
     ll_cont, ll_offset, ll_ret, ll_prog_origine
string ls_cod_attrezz_copia, ls_cod_attrezz_incolla, ls_cod_tipo_manutenzione
dec{4} ld_quantita

ll_anno_reg_origine = dw_grid.getitemnumber(fl_riga_copia, "anno_registrazione")
ll_num_reg_origine  = dw_grid.getitemnumber(fl_riga_copia, "num_registrazione")
ll_prog_origine = dw_grid.getitemnumber(fl_riga_copia, "progressivo")

ll_anno_reg_destinazione = dw_grid.getitemnumber(fl_riga_incolla, "anno_registrazione")
ll_num_reg_destinazione  = dw_grid.getitemnumber(fl_riga_incolla, "num_registrazione")

ll_offset = 0

select max(prog_riga_ricambio)
into   :ll_offset
from   prog_manutenzioni_ricambi  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg_destinazione and
		 num_registrazione = :ll_num_reg_destinazione;
		 
if isnull(ll_offset) or ll_offset = 0 then
	ll_offset = 1
else
	ll_offset ++
end if

INSERT INTO prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio,   
		  flag_prodotto_codificato,   
		  cod_fornitore,   
		  cod_misura,   
		  data_consegna,
		  sconto_1_budget,
		  sconto_2_budget,
		  sconto_3_budget,
		  prezzo_netto_budget)  
 SELECT cod_azienda,   
		  :ll_anno_reg_destinazione,   
		  :ll_num_reg_destinazione,   
		  :ll_offset,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio,   
		  flag_prodotto_codificato,   
		  cod_fornitore,   
		  cod_misura,   
		  data_consegna,
		  sconto_1_budget,
		  sconto_2_budget,
		  sconto_3_budget,
		  prezzo_netto_budget
FROM    prog_manutenzioni_ricambi  
WHERE   cod_azienda = :s_cs_xx.cod_azienda AND  
		  anno_registrazione = :ll_anno_reg_origine AND  
		  num_registrazione = :ll_num_reg_origine    and
		  prog_riga_ricambio = :ll_prog_origine;
		  
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante l'operazione di copia ricambio ~r~n" + sqlca.sqlerrtext)
	return -1
end if

return 0
end function

public function integer wf_incolla_piano_manutenzione_std (long fl_riga_copia, long fl_riga_incolla, string fs_operazione);long ll_anno_registrazione, ll_num_registrazione, ll_anno_reg_origine, ll_num_reg_origine,&
     ll_cont, ll_offset, ll_ret
string ls_cod_attrezz_copia, ls_cod_attrezz_incolla, ls_cod_tipo_manutenzione

ll_ret = g_mb.messagebox( "OMNIA", "Copiare anche le risorse esterne del piano selezionato?", Exclamation!, YESNO!, 2)

ll_anno_reg_origine = dw_grid.getitemnumber(fl_riga_copia, "anno_registrazione")
ll_num_reg_origine  = dw_grid.getitemnumber(fl_riga_copia, "num_registrazione")
ls_cod_tipo_manutenzione = dw_grid.getitemstring(fl_riga_copia, "cod_tipo_manutenzione")
ls_cod_attrezz_copia   = dw_grid.getitemstring(fl_riga_copia,   "cod_attrezzatura")
ls_cod_attrezz_incolla = dw_grid.getitemstring(fl_riga_incolla, "cod_attrezzatura")

ll_anno_registrazione = f_anno_esercizio()

if upper(fs_operazione) = "T" then
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
			 
	if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
		ll_num_registrazione = 0
	else
		ll_num_registrazione ++
	end if
else
	ll_anno_registrazione = ll_anno_reg_origine
	ll_num_registrazione = ll_num_reg_origine
end if

if upper(fs_operazione) = "T" then

	// verifico presenza del tipo manutenzione ed eventualmente duplico anche quello
	select count(*)
	into   :ll_cont
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezz_incolla AND
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			 
	if ll_cont = 0 then
		
	  INSERT INTO tab_tipi_manutenzione  
				( cod_azienda,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  des_tipo_manutenzione,   
				  tempo_previsto,   
				  flag_manutenzione,   
				  flag_ricambio_codificato,   
				  cod_ricambio,   
				  cod_ricambio_alternativo,   
				  des_ricambio_non_codificato,   
				  num_reg_lista,   
				  num_versione,   
				  num_edizione,   
				  modalita_esecuzione,   
				  cod_tipo_ord_acq,   
				  cod_tipo_off_acq,   
				  cod_tipo_det_acq,   
				  cod_tipo_movimento,   
				  periodicita,   
				  frequenza,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_primario,   
				  cod_misura,   
				  val_min_taratura,   
				  val_max_taratura,   
				  valore_atteso,   
				  cod_versione,   
				  flag_blocco,   
				  data_ultima_modifica,   
				  des_tipo_manutenzione_storico,   
				  data_blocco,   
				  cod_tipo_lista_dist,   
				  cod_lista_dist,   
				  cod_operaio,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  data_inizio_val_1,   
				  data_fine_val_1,   
				  data_inizio_val_2,   
				  data_fine_val_2 )  
		  SELECT cod_azienda,   
					:ls_cod_attrezz_incolla,   
					cod_tipo_manutenzione,   
					des_tipo_manutenzione,   
					tempo_previsto,   
					flag_manutenzione,   
					flag_ricambio_codificato,   
					cod_ricambio,   
					cod_ricambio_alternativo,   
					des_ricambio_non_codificato,   
					num_reg_lista,   
					num_versione,   
					num_edizione,   
					modalita_esecuzione,   
					cod_tipo_ord_acq,   
					cod_tipo_off_acq,   
					cod_tipo_det_acq,   
					cod_tipo_movimento,   
					periodicita,   
					frequenza,   
					quan_ricambio,   
					costo_unitario_ricambio,   
					cod_primario,   
					cod_misura,   
					val_min_taratura,   
					val_max_taratura,   
					valore_atteso,   
					cod_versione,   
					flag_blocco,   
					data_ultima_modifica,   
					des_tipo_manutenzione_storico,   
					data_blocco,   
					cod_tipo_lista_dist,   
					cod_lista_dist,   
					cod_operaio,   
					cod_cat_risorse_esterne,   
					cod_risorsa_esterna,   
					data_inizio_val_1,   
					data_fine_val_1,   
					data_inizio_val_2,   
					data_fine_val_2  
			 FROM tab_tipi_manutenzione  
			WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
					cod_attrezzatura = :ls_cod_attrezz_copia AND  
					cod_tipo_manutenzione = :ls_cod_tipo_manutenzione ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore durante l'operazione di copia tipo manutenzione~r~n" + sqlca.sqlerrtext)
			return -1
		end if	
	end if
	
	
	// procedo con insert programma
	INSERT INTO programmi_manutenzione  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  des_intervento,   
		  cod_attrezzatura,   
		  cod_tipo_manutenzione,   
		  flag_tipo_intervento,   
		  periodicita,   
		  frequenza,   
		  flag_scadenze,   
		  note_idl,   
		  flag_ricambio_codificato,   
		  cod_ricambio,   
		  cod_ricambio_alternativo,   
		  ricambio_non_codificato,   
		  quan_ricambio,   
		  costo_unitario_ricambio,   
		  cod_operaio,   
		  costo_orario_operaio,   
		  operaio_ore_previste,   
		  operaio_minuti_previsti,   
		  cod_cat_risorse_esterne,   
		  cod_risorsa_esterna,   
		  risorsa_ore_previste,   
		  risorsa_minuti_previsti,   
		  risorsa_costo_orario,   
		  risorsa_costi_aggiuntivi,   
		  anno_contratto_assistenza,   
		  num_contratto_assistenza,   
		  anno_ord_acq_risorsa,   
		  num_ord_acq_risorsa,   
		  prog_riga_ord_acq_risorsa,   
		  rif_contratto_assistenza,   
		  rif_ordine_acq_risorsa,   
		  flag_budget,   
		  cod_primario,   
		  cod_misura,   
		  valore_min_taratura,   
		  valore_max_taratura,   
		  valore_atteso,   
		  tot_costo_intervento,   
		  num_reg_lista,   
		  prog_liste_con_comp,   
		  num_versione,   
		  num_edizione,   
		  flag_blocco,   
		  data_blocco,   
		  flag_priorita,   
		  data_forzata,   
		  cod_versione,   
		  anno_reg_progr_padre,   
		  num_reg_progr_padre,   
		  data_creazione,   
		  data_ultima_modifica,   
		  flag_storico,   
		  num_revisione,   
		  commento_modifiche,   
		  des_tipo_manutenzione_storico,   
		  redatto_da,   
		  autorizzato_da,   
		  approvato_da,   
		  cod_tipo_lista_dist,   
		  cod_lista_dist,   
		  data_inizio_val_1,   
		  data_fine_val_1,   
		  data_inizio_val_2,   
		  data_fine_val_2)  
	SELECT cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			des_intervento,   
			:ls_cod_attrezz_incolla,   
			:ls_cod_tipo_manutenzione,   
			flag_tipo_intervento,   
			periodicita,   
			frequenza,   
			flag_scadenze,   
			note_idl,   
			flag_ricambio_codificato,   
			cod_ricambio,   
			cod_ricambio_alternativo,   
			ricambio_non_codificato,   
			quan_ricambio,   
			costo_unitario_ricambio,   
			cod_operaio,   
			costo_orario_operaio,   
			operaio_ore_previste,   
			operaio_minuti_previsti,   
			cod_cat_risorse_esterne,   
			cod_risorsa_esterna,   
			risorsa_ore_previste,   
			risorsa_minuti_previsti,   
			risorsa_costo_orario,   
			risorsa_costi_aggiuntivi,   
			anno_contratto_assistenza,   
			num_contratto_assistenza,   
			anno_ord_acq_risorsa,   
			num_ord_acq_risorsa,   
			prog_riga_ord_acq_risorsa,   
			rif_contratto_assistenza,   
			rif_ordine_acq_risorsa,   
			flag_budget,   
			cod_primario,   
			cod_misura,   
			valore_min_taratura,   
			valore_max_taratura,   
			valore_atteso,   
			tot_costo_intervento,   
			num_reg_lista,   
			prog_liste_con_comp,   
			num_versione,   
			num_edizione,   
			flag_blocco,   
			data_blocco,   
			flag_priorita,   
			data_forzata,   
			cod_versione,   
			anno_reg_progr_padre,   
			num_reg_progr_padre,   
			data_creazione,   
			data_ultima_modifica,   
			flag_storico,   
			num_revisione,   
			commento_modifiche,   
			des_tipo_manutenzione_storico,   
			redatto_da,   
			autorizzato_da,   
			approvato_da,   
			cod_tipo_lista_dist,   
			cod_lista_dist,   
			data_inizio_val_1,   
			data_fine_val_1,   
			data_inizio_val_2,   
			data_fine_val_2
	 FROM programmi_manutenzione  
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( anno_registrazione = :ll_anno_reg_origine ) AND  
			( num_registrazione = :ll_num_reg_origine )   ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante l'operazione di copia piano manutentivo~r~n" + sqlca.sqlerrtext)
		return -1
	end if
end if


if upper(fs_operazione) = "T" or upper(fs_operazione) = "R" then
	
	ll_offset = 0
	
	select max(prog_riga_ricambio)
	into   :ll_offset
	from   prog_manutenzioni_ricambi  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			 
	if isnull(ll_offset) or ll_offset = 0 then
		ll_offset = 1
	else
		ll_offset ++
	end if


	INSERT INTO prog_manutenzioni_ricambi  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ricambio,   
			  cod_prodotto,   
			  des_prodotto,   
			  quan_utilizzo,   
			  prezzo_ricambio,   
			  flag_prodotto_codificato,   
			  cod_fornitore,   
			  cod_misura,   
			  data_consegna)  
	 SELECT cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ll_offset,   
			  cod_prodotto,   
			  des_prodotto,   
			  quan_utilizzo,   
			  prezzo_ricambio,   
			  flag_prodotto_codificato,   
			  cod_fornitore,   
			  cod_misura,   
			  data_consegna
	FROM    prog_manutenzioni_ricambi  
	WHERE   cod_azienda = :s_cs_xx.cod_azienda AND  
			  anno_registrazione = :ll_anno_reg_origine AND  
			  num_registrazione = :ll_num_reg_origine    ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante l'operazione di copia ricambio ~r~n" + sqlca.sqlerrtext)
		return -1
	end if
end if

if ll_ret = 1 then

	INSERT INTO prog_manutenzioni_risorse_est  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  progressivo,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  ore,   
			  minuti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi)  
	SELECT  cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  progressivo,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  ore,   
			  minuti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi
	 FROM   prog_manutenzioni_risorse_est 	
	 WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
			  anno_registrazione = :ll_anno_reg_origine AND  
			  num_registrazione = :ll_num_reg_origine    ;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante l'operazione di copia risorsa est. ~r~n" + sqlca.sqlerrtext)
		return -1
	end if
	
end if

INSERT INTO prog_manutenzioni_operai  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  progressivo,   
		  cod_operaio,   
		  ore,   
		  minuti,
		  operaio_costo_orario)  
SELECT  cod_azienda,   
		  :ll_anno_registrazione,   
		  :ll_num_registrazione,   
		  progressivo,   
		  cod_operaio,   
		  ore,   
		  minuti,
		  operaio_costo_orario
 FROM   prog_manutenzioni_operai
 WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		  anno_registrazione = :ll_anno_reg_origine AND  
		  num_registrazione = :ll_num_reg_origine    ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante l'operazione di copia operatore ~r~n" + sqlca.sqlerrtext)
	return -1
end if


return 0
end function

public function integer wf_incolla_ricambio_semplice (long fl_riga_copia, long fl_riga_incolla, string fs_operazione);long ll_anno_reg_destinazione, ll_num_reg_destinazione, ll_anno_reg_origine, ll_num_reg_origine,&
     ll_cont, ll_offset, ll_ret, ll_prog_origine, ll_anno_riferimento
string ls_cod_attrezz_copia, ls_cod_attrezz_incolla, ls_cod_tipo_manutenzione, ls_des_prodotto, ls_flag_budget, ls_cod_fornitore, ls_flag_blocco
dec{4} ld_quantita, ld_quan_utilizzo

ll_anno_reg_origine = dw_grid.getitemnumber(fl_riga_copia, "anno_registrazione")
ll_num_reg_origine  = dw_grid.getitemnumber(fl_riga_copia, "num_registrazione")
ll_prog_origine = dw_grid.getitemnumber(fl_riga_copia, "progressivo")

ll_anno_reg_destinazione = dw_grid.getitemnumber(fl_riga_incolla, "anno_registrazione")
ll_num_reg_destinazione  = dw_grid.getitemnumber(fl_riga_incolla, "num_registrazione")

if wf_controllo_anno( ll_anno_reg_destinazione, ll_num_reg_destinazione, ll_anno_riferimento, ls_flag_budget) < 0 then
	return -1
end if

ll_offset = 0

select max(prog_riga_ricambio)
into   :ll_offset
from   prog_manutenzioni_ricambi  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg_destinazione and
		 num_registrazione = :ll_num_reg_destinazione;
		 
if isnull(ll_offset) or ll_offset = 0 then
	ll_offset = 1
else
	ll_offset ++
end if

// *** controllo fornitore

select cod_fornitore
into	:ls_cod_fornitore
from	prog_manutenzioni_ricambi
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_reg_origine AND  
		num_registrazione = :ll_num_reg_origine    and
		prog_riga_ricambio = :ll_prog_origine;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "1.Errore durante la ricerca del fornitore del ricambio:" + sqlca.sqlerrtext, stopsign!)
	return -1
end if

select flag_blocco
into	:ls_flag_blocco
from	anag_fornitori
where	cod_azienda = :s_cs_xx.cod_azienda and
		cod_fornitore = :ls_cod_fornitore;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "2.Errore durante la ricerca del fornitore del ricambio:" + sqlca.sqlerrtext, stopsign!)
	return -1
end if

if not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
	g_mb.messagebox( "OMNIA", "Attenzione: il fornitore " + ls_cod_fornitore + " risulta bloccato. Impossibile continuare.", stopsign!)
	return -1	
end if

INSERT INTO prog_manutenzioni_ricambi  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ricambio,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio,   
		  flag_prodotto_codificato,   
		  cod_fornitore,   
		  cod_misura,   
		  data_consegna,
		  flag_prezzo_certo,
		  flag_capitalizzato,
		  priorita,
		  cod_porto,
		  tkposi,
		  tkordi,
		  ordine_progen,
		  nrposi,
		  sconto_1_budget,
		  sconto_2_budget,
		  sconto_3_budget,
		  prezzo_netto_budget,
		  sconto_1_eff,
		  sconto_2_eff,
		  sconto_3_eff,
		  prezzo_netto_eff,
		  prezzo_ricambio_eff,
		  tkboll,
		  tkposi_b,
		  nrposi_b,
		  num_documento,
		  data_documento,
		  flag_sconto_valore,
		  cod_stato,
		  anno_riferimento,
		  cod_tipo_programma,
		  flag_budget,
		  flag_rda)  
 SELECT cod_azienda,   
		  :ll_anno_reg_destinazione,   
		  :ll_num_reg_destinazione,   
		  :ll_offset,   
		  cod_prodotto,   
		  des_prodotto,   
		  quan_utilizzo,   
		  prezzo_ricambio,   
		  flag_prodotto_codificato,   
		  cod_fornitore,   
		  cod_misura,   
		  data_consegna,
		  flag_prezzo_certo,
		  flag_capitalizzato,
		  priorita,
		  cod_porto,
		  null,
		  null,
		  null,
		  null,
		  sconto_1_budget,
		  sconto_2_budget,
		  sconto_3_budget,
		  prezzo_netto_budget,
		  sconto_1_eff,
		  sconto_2_eff,
		  sconto_3_eff,
		  prezzo_netto_eff,
		  prezzo_ricambio_eff,
		  null,
		  null,
		  null,
		  null,
		  null,
		  flag_sconto_valore,
		  cod_stato,
		  :ll_anno_riferimento,
		  cod_tipo_programma,
		  :ls_flag_budget,
		  'N'
FROM    prog_manutenzioni_ricambi  
WHERE   cod_azienda = :s_cs_xx.cod_azienda AND  
		  anno_registrazione = :ll_anno_reg_origine AND  
		  num_registrazione = :ll_num_reg_origine    and
		  prog_riga_ricambio = :ll_prog_origine;
		  
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante l'operazione di copia ricambio ~r~n" + sqlca.sqlerrtext)
	return -1
end if

return 0
end function

public function integer wf_preordine (string fs_tkordi, ref string fs_errore, ref boolean fb_preordine);long	   ll_tkordi
datetime ldt_data_fattura
STRING   ls_cod_azienda, ls_tipo_ordine, ls_codice_preordine

ll_tkordi = long(fs_tkordi)

select stringa
into   :ls_cod_azienda
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'AZP';
		 
select stringa
into   :ls_codice_preordine
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TOP';


select cdtorac
into   :ls_tipo_ordine
from   pgmr.ordacq_test
where  tkordi = :ll_tkordi
using  tran_import;

if tran_import.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca dell'ordine:" + tran_import.sqlerrtext
	return -1		
end if

if not isnull(ls_tipo_ordine) and ls_tipo_ordine = ls_codice_preordine then
	fb_preordine = true
	return 0
end if

fb_preordine = false
return 0
end function

public function integer wf_fatturato (string fs_tkboll, string fs_tkposi, ref string fs_errore, ref boolean fb_fatturato, ref string fs_descrizione);long	   ll_tkboll, ll_num_fattura, ll_tkposi, ll_tkfatt, ll_anno
string	ls_cod_azienda, ls_tkforn
datetime ldt_data_fattura, ldt_data_contabile
dec 		ld_protiva, ld_num_protocollo

select  stringa
into    :ls_cod_azienda
from    parametri_azienda
where   cod_azienda = :s_cs_xx.cod_azienda and
        cod_parametro = 'AZP';

ll_tkboll = long(fs_tkboll)
ll_tkposi = long(fs_tkposi)

select nrifft,
		 dtrift,
		 tkforn
into   :ll_num_fattura,
		 :ldt_data_fattura,
		 :ls_tkforn
from   pgmr.bollacq_test
where  tkboll = :ll_tkboll
using  tran_import;


if tran_import.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca del buono d'ingresso:" + tran_import.sqlerrtext
	return -1		
end if

if not isnull(ll_num_fattura) and ll_num_fattura > 0 then
	
	fb_fatturato = true
	
	// prendo tkfatt
	select tkfatt 
	into   :ll_tkfatt
	from   pgmr.bollacq_posi
	where  tkboll = :ll_tkboll and
	       tkposi = :ll_tkposi
	using  tran_import;
	
	if tran_import.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca della fattura del buono d'ingresso:" + tran_import.sqlerrtext
		return -1		
	end if	
	
	select nrprot_cont,
	       dtcontab
	into   :ld_num_protocollo,
	       :ldt_data_contabile
	from   pgmr.bolla_test
	where  tkfatt = :ll_tkfatt
	using  tran_import;
	
	if tran_import.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca protocollo contabile del buono d'ingresso:" + tran_import.sqlerrtext
		return -1		
	end if		
	
	ll_anno = year(date(ldt_data_fattura))
	
	select protiva
	into   :ld_protiva
	from   pgmr.mac_arctest
	where  protcon = :ld_num_protocollo and
	       cdazie = :ls_cod_azienda and
          DatePart(yy,dtdocext) = :ll_anno and
			 tkforn = :ls_tkforn
	using  tran_import;
	
	if tran_import.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca protocollo iva del buono d'ingresso:" + tran_import.sqlerrtext
		return -1		
	end if
	
	fs_descrizione = string(ld_protiva) + " " + string(ldt_data_contabile, "dd/mm/yyyy")
	
	return 0
end if

fb_fatturato = false
return 0
end function

public function boolean wf_operatore_rda ();string ls_amministratore
long	ll_cont

// *** se è un mansionario allora ordini
//     altrimenti RDA

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	
	select flag_collegato
	into   :ls_amministratore
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode = 0 and ls_amministratore = "S" and not isnull(ls_amministratore) then
		return false
	else
	
		//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//		select count(*)
//		into   :ll_cont 
//		from   mansionari
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_utente = :s_cs_xx.cod_utente and
//				 flag_autorizza_rda = 'S' and
//				 flag_approva_rda = 'S';




	if not iuo_mansionario.uof_get_privilege(iuo_mansionario.approva_rda) or not iuo_mansionario.uof_get_privilege(iuo_mansionario.autorizza_rda)  then 
				 
//		if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont > 0 then
			return false
		else
//			select count(*)
//			into   :ll_cont 
//			from   mansionari
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_utente = :s_cs_xx.cod_utente and
//					 flag_autorizza_rda = 'N' and
//					 flag_approva_rda = 'S';
						 
//			if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont > 0 then	
			if iuo_mansionario.uof_get_privilege(iuo_mansionario.approva_rda) and not iuo_mansionario.uof_get_privilege(iuo_mansionario.autorizza_rda)  then 
				return true
			else
				return false
			end if
			//--- Fine modifica Giulio
			
		end if
		
	end if
end if

return false
end function

public function integer wf_esegui ();long 		ll_riga, ll_anno, ll_numero, ll_prog_riga_ricambio, ll_prog_riga_new, ll_i, ll_prog_bolla_progen, ll_chiave_testata, ll_anno_documento, ll_chiave_pos, ll_chiave, ll_chiave_2, ll_max

string		ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore, ls_flag_rda, ls_cod_prodotto, ls_cod_fornitore, ls_cod_misura, ls_cod_porto, ls_des_prodotto, ls_des_estesa, &
			ls_flag_sconto_valore, ls_cod_stato, ls_cod_fornitore_old, ls_cod_tipo_programma, ls_controllo_reparto, ls_controllo_attrezzatura, ls_cod_azienda, ls_cod_centro_costo_mezzi, ls_prefisso_mg, &
			ls_cod_area_aziendale, ls_cod_centro_costo, ls_cdceco, ls_cdrmpo_ord, ls_adspds_1_ord, ls_adspds_2_ord, ls_cdarti, ls_cdpian, ls_cod_misura_2, ls_tkforn, ls_pgcodi, ls_dpcodi, ls_cddpag, &
			ls_ffinme, ls_cdbanc, ls_cdente, ls_cdunil, ls_cdenul, ls_cdusul, ls_cdunil_m, ls_dsunil, ls_cddipa, ls_num_documento, ls_cdtrme, ls_cdtdim_m, ls_cdstab_appo, ls_cdtdim, ls_cdstab_m, ls_cdstab, &
			ls_indiri, ls_cdtdoc, ls_cdcatm, ls_cdtpmg, ls_1, ls_2, ls_errore
			
boolean	lb_rda

datetime	ldt_data_consegna, ldt_data_documento, ldt_oggi, ldt_data_1, ldt_data_2

dec{4}	ld_sconto_1_eff, ld_sconto_2_eff, ld_sconto_3_eff, ld_prezzo_netto_eff, ld_prezzo_ricambio_eff, ld_quan_eff, 	ld_adspim_1_ord, ld_adspim_2_ord, ld_scocas_ord, ld_scrap1_ord, ld_scrap2_ord, &
			ld_cstrkg_ord, ld_oneacc_ord
			
window  lw_window			
			
uo_progen_documenti luo_ddi			

ll_riga = il_rbuttonrow
ll_anno = dw_grid.getitemnumber(ll_riga, "anno_registrazione")
ll_numero = dw_grid.getitemnumber(ll_riga, "num_registrazione")

//	***	leggo l'area aziendale

select cod_area_aziendale
into    :ls_cod_area_aziendale
from   anag_attrezzature
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura IN ( select cod_attrezzatura from programmi_manutenzione where cod_azienda = :s_cs_xx.cod_azienda and anno_registrazione = :ll_anno and num_registrazione = :ll_numero);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca dell'area aziendale:" + sqlca.sqlerrtext, stopsign!)
	return -1
end if	 

s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
s_cs_xx.parametri.parametro_dec4_1 = ll_anno
s_cs_xx.parametri.parametro_dec4_2 = ll_numero

window_type_open( lw_window, "w_prog_manutenzioni_program_ddi", 0)

if s_cs_xx.parametri.parametro_b_1 then
	cb_cerca.triggerevent("clicked")
end if
end function

public function integer wf_registro (boolean fb_connetti);string ls_default,ls_dbparm
integer li_risposta

if fb_connetti then
	// -----------------  leggo le informazioni della transazione dal registro ----------------------
	
	tran_import = CREATE n_tran
	li_risposta = registryget(is_chiave_root+ "database", "servername", tran_import.ServerName)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database", "logid", tran_import.LogId)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database", "logpass", tran_import.LogPass)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "dbms", tran_import.DBMS)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "database", tran_import.Database)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "userid", tran_import.UserId)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database", "dbpass", tran_import.DBPass)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "dbparm", ls_dbparm)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	tran_import.DBParm = ls_dbparm
	
	li_risposta = registryget(is_chiave_root+ "database" , "lock", tran_import.Lock)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	// ----------------------------------------------------------------------------------------------
	if f_po_connect(tran_import, true) <> 0 then
		return -1
	end if

else
	
	rollback using tran_import;
	disconnect using tran_import;
	destroy tran_import;
	
end if
return 0
end function

public function integer wf_controllo_anno (long fl_anno, long fl_numero, ref long fl_anno_riferimento, ref string fs_flag_budget);string	ls_cod_tipo_manutenzione, ls_flag_budget_chiuso, ls_flag_budget, ls_flag_blocco, ls_null, ls_flag_amministratore
long	ll_anno_riferimento

select anno_riferimento,
		flag_budget,
		cod_tipo_manutenzione
into	:ll_anno_riferimento,
		:ls_flag_budget,
		:ls_cod_tipo_manutenzione
from	programmi_manutenzione
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno and
		num_registrazione = :fl_numero;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: attività non trovata (" + string(fl_anno) + "/" + string(fl_numero) + ")! ", stopsign!)
	return -1
end if

if isnull(ls_flag_budget) then ls_flag_budget = "N"

if not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
	
	select flag_chiuso
	into	:ls_flag_budget_chiuso
	from 	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_riferimento = :ll_anno_riferimento;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il controllo budget consolidato per l'anno " + string(ll_anno_riferimento) + ":" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	if not isnull(ls_flag_budget_chiuso) and ls_flag_budget_chiuso = "S" and not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta consolidato! Impossibile aggiungere attività a budget!", stopsign!)
		
//		select flag_collegato
//		into	:ls_flag_amministratore
//		from	utenti
//		where	cod_utente = :s_cs_xx.cod_utente;
//		
//		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
//		else
//			rollback;
//			close(parent)
//		end if
		
		return -1
	end if

end if

fl_anno_riferimento = ll_anno_riferimento
fs_flag_budget = ls_flag_budget

return 0

end function

public function long wf_anno_riferimento (string fs_cod_tipo_manutenzione, ref long fl_anni[]);long	ll_anno_riferimento, ll_cont, ll_anni[], ll_conteggio, ll_i
date ldt_oggi

declare cu_righe cursor for
select anno_riferimento
from	prog_manutenzioni_anni
where	cod_azienda = :s_cs_xx.cod_azienda and
		flag_anno_riferimento = 'S'
order by anno_riferimento;
		
open cu_righe;
if sqlca.sqlcode <> 0 then 
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_anni_riferimento:" + sqlca.sqlerrtext, stopsign!)
	return -1
end if

ll_cont = 0

do while true
	fetch cu_righe into :ll_anno_riferimento;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la fetch del cursore cu_anni_riferimento:" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	ll_cont ++
	
	ll_anni[ll_cont] = ll_anno_riferimento
loop

close cu_righe;
if sqlca.sqlcode <> 0 then 
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore cu_anni_riferimento:" + sqlca.sqlerrtext, stopsign!)
	return -1
end if

if ll_cont > 1 then
	
	// *** ho più di un esercizio aperto, quindi controllo quello valido per la tipologia di manutenzione
	
	ll_conteggio = 0
	
	for ll_i = 1 to upperbound(ll_anni)
		
//		select count(*)
//		into	:ll_cont
//		from	prog_manutenzioni_anni_tm
//		where	cod_azienda = :s_cs_xx.cod_azienda and
//				anno_riferimento = :ll_anni[ll_i] and
//				cod_tipo_manut_scollegata = :fs_cod_tipo_manutenzione and
//				flag_blocco = 'N';
//				
//		if sqlca.sqlcode <> 0 then
//			continue
//		end if
//		
//		if isnull(ll_cont) or ll_cont = 0 then
//			continue
//		end if
		
		ll_anno_riferimento = ll_anni[ll_i]
		ll_conteggio ++
		fl_anni[ll_conteggio] = ll_anno_riferimento
		
	next
	
	if ll_conteggio > 1 then		
		// *** ci sono + anni per cui la tipologia di manutenzione è valida. quindi gliela chiedo
		return 0		
	elseif ll_conteggio = 1 then
		return ll_anno_riferimento
	elseif ll_conteggio < 1 then
		g_mb.messagebox( "OMNIA", "Attenzione: la tipologia manutenzione con codice " + fs_cod_tipo_manutenzione + " risulta bloccata o non configurata per gli anni di riferimento impostati. Verificare nell'apposita tabella!", stopsign!)
		return -1		
	end if
	
elseif ll_cont < 1 then
	
	g_mb.messagebox( "OMNIA", "Attenzione: nessun anno risulta impostato come anno di riferimento nell'apposita tabella!", stopsign!)
	return -1
	
elseif ll_cont = 1 then
	
//	// *** ho un solo anno di riferimento, ma controllo che la tipologia di manutenzione sia configurata e non bloccata per quell'anno
//	
//	select count(*)
//	into	:ll_cont
//	from	prog_manutenzioni_anni_tm
//	where	cod_azienda = :s_cs_xx.cod_azienda and
//			anno_riferimento = :ll_anno_riferimento and
//			cod_tipo_manut_scollegata = :fs_cod_tipo_manutenzione and
//			flag_blocco = 'N';
//			
//	if sqlca.sqlcode <> 0 then
//		g_mb.messagebox( "OMNIA", "Errore durante la verifica se la tipologia di manutenzione è bloccata per l'anno di riferimento " + string(ll_anno_riferimento) + ":" + sqlca.sqlerrtext, stopsign!)		
//		return -1
//	end if
//	
//	if isnull(ll_cont) or ll_cont = 0 then
//		g_mb.messagebox( "OMNIA", "Attenzione: la tipologia manutenzione con codice " + fs_cod_tipo_manutenzione + " risulta bloccata o non configurata per l'anno di riferimento " + string(ll_anno_riferimento) + ". Verificare nell'apposita tabella!", stopsign!)
//		return -1
//	end if
	
	return ll_anno_riferimento
	
end if
end function

public function integer wf_incolla_ricambio_piano (long fl_anno_origine, long fl_num_origine, long fl_anno_dest, long fl_num_dest, string fs_operazione, long fl_anno_riferimento);string	ls_cod_prodotto, ls_Flag_prodotto_codificato, ls_cod_fornitore, ls_cod_misura, ls_cod_porto, ls_flag_prezzo_certo, ls_flag_capitalizzato, &
         ls_tkposi, ls_tkordi, ls_ordine_progen, ls_tkboll, ls_tkposi_b, ls_num_documento, ls_des_prodotto, ls_flag_sconto_valore, ls_cod_stato, &
			ls_cod_tipo_programma, ls_flag_evaso, ls_flag_rda, ls_ordine_progen_rda, ls_tkordi_rda, ls_tkposi_rda, ls_flag_confermato, ls_cod_stato_ordine, ls_flag_fatturato, ls_flag_budget, &
			ls_flag_rda_approvata, ls_flag_ricambio_prot, ls_flag_blocco, ls_cod_stato_non_eseguito
long		ll_prog_riga_ricambio, ll_priorita, ll_nrposi, ll_nrposi_b, ll_nrposi_rda, ll_anno_riferimento
dec{4}   ld_quan_utilizzo, ld_prezzo_ricambio, ld_sconto_1_budget, ld_sconto_2_budget, ld_sconto_3_budget, ld_prezzo_netto_budget, ld_sconto_1_eff, ld_sconto_2_eff, &
         ld_sconto_3_eff, ld_prezzo_netto_eff, ld_prezzo_ricambio_eff, ld_quantita, ld_quan_consegnata, ld_quan_eff
datetime ldt_data_consegna, ldt_data_documento

select stringa
into	:ls_cod_stato_non_eseguito
from	parametri
where	cod_parametro = 'MSN';

declare cu_ricambi cursor for
select  prog_riga_ricambio,   
        cod_prodotto,   
        quan_utilizzo,   
        prezzo_ricambio,   
        flag_prodotto_codificato,   
        cod_fornitore,   
        cod_misura,   
        data_consegna,   
        cod_porto,   
        flag_prezzo_certo,   
        flag_capitalizzato,   
        priorita,   
        tkposi,   
        tkordi,   
        nrposi,   
        ordine_progen,   
        sconto_1_budget,   
        sconto_2_budget,   
        sconto_3_budget,   
        prezzo_netto_budget,   
        sconto_1_eff,   
        sconto_2_eff,   
        sconto_3_eff,   
        prezzo_netto_eff,   
        prezzo_ricambio_eff,   
        tkboll,   
        tkposi_b,   
        nrposi_b,   
        num_documento,   
        data_documento,   
        des_prodotto,   
        flag_sconto_valore,
	    cod_stato,
	    cod_tipo_programma,
         quan_consegnata,   
         flag_evaso,   
         quan_eff,   
         flag_rda,   
         ordine_progen_rda,   
         tkordi_rda,   
         tkposi_rda,   
         nrposi_rda,   
         anno_riferimento,   
         flag_confermato,   
         cod_stato_ordine,   
         flag_fatturato,   
         flag_budget,   
         flag_rda_approvata,   
         flag_ricambio_prot  			  
from    prog_manutenzioni_ricambi
where   cod_azienda = :s_cs_xx.cod_azienda and
        	  anno_registrazione = :fl_anno_origine and
		  num_registrazione = :fl_num_origine;
		  
open cu_ricambi;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore dei ricambi: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_ricambi into :ll_prog_riga_ricambio,
	                      :ls_cod_prodotto,
								 :ld_quan_utilizzo,
								 :ld_prezzo_ricambio,   
								 :ls_flag_prodotto_codificato,   
								 :ls_cod_fornitore,   
								 :ls_cod_misura,   
								 :ldt_data_consegna,   
								 :ls_cod_porto,   
								 :ls_flag_prezzo_certo,   
								 :ls_flag_capitalizzato,   
								 :ll_priorita,   
								 :ls_tkposi,   
								 :ls_tkordi,   
								 :ll_nrposi,   
								 :ls_ordine_progen,   
								 :ld_sconto_1_budget,   
								 :ld_sconto_2_budget,   
								 :ld_sconto_3_budget,   
								 :ld_prezzo_netto_budget,   
								 :ld_sconto_1_eff,   
								 :ld_sconto_2_eff,   
								 :ld_sconto_3_eff,   
								 :ld_prezzo_netto_eff,   
								 :ld_prezzo_ricambio_eff,   
								 :ls_tkboll,   
								 :ls_tkposi_b,   
								 :ll_nrposi_b,   
								 :ls_num_documento,   
								 :ldt_data_documento,   
								 :ls_des_prodotto,   
								 :ls_flag_sconto_valore,
								 :ls_cod_stato,
								 :ls_cod_tipo_programma,
								 :ld_quan_consegnata,   
								 :ls_flag_evaso,   
								 :ld_quan_eff,   
								 :ls_flag_rda,   
								 :ls_ordine_progen_rda,   
								 :ls_tkordi_rda,   
								 :ls_tkposi_rda,   
								 :ll_nrposi_rda,   
								 :ll_anno_riferimento,   
								 :ls_flag_confermato,   
								 :ls_cod_stato_ordine,   
								 :ls_flag_fatturato,   
								 :ls_flag_budget,   
								 :ls_flag_rda_approvata,   
								 :ls_flag_ricambio_prot;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox( "OMNIA", "Errore durante la fetch del cursore dei ricambi: " + sqlca.sqlerrtext)
		close cu_ricambi;
		return -1
	end if
	
	select flag_blocco
	into	:ls_flag_blocco
	from	anag_fornitori
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il controllo del fornitore:" + sqlca.sqlerrtext, stopsign!)
		close cu_ricambi;
		rollback;
		return -1
	end if
	
	if not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		g_mb.messagebox( "OMNIA", "Attenzione: il fornitore " + ls_cod_fornitore + " risulta bloccato. Impossibile continuare.", stopsign!)
		close cu_ricambi;
		rollback;
		return -1		
	end if
	
	if not isnull(ls_tkordi) and ls_tkordi <> "" and fs_operazione = "D" then				// *** aggiorno riga origine
	
		s_cs_xx.parametri.parametro_dec4_1 = 0
		s_cs_xx.parametri.parametro_b_1 = false
		s_cs_xx.parametri.parametro_s_1 = ls_des_prodotto
		s_cs_xx.parametri.parametro_s_2 = string(ld_quan_utilizzo)
		
		window_open(w_rileva_quantita_ricambio, 0)
		
		setnull(s_cs_xx.parametri.parametro_s_1)
		setnull(s_cs_xx.parametri.parametro_s_2)
		
		if not s_cs_xx.parametri.parametro_b_1 then continue
		
		if not isnull(s_cs_xx.parametri.parametro_dec4_1) and s_cs_xx.parametri.parametro_dec4_1 > 0 then
			ld_quantita = s_cs_xx.parametri.parametro_dec4_1
		else
			ld_quantita = ld_quan_utilizzo
		end if
		
		if isnull(ls_flag_rda) then ls_flag_rda = "N"
		
		INSERT INTO prog_manutenzioni_ricambi  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_ricambio,   
				  cod_prodotto,   
				  des_prodotto,   
				  quan_utilizzo,   
				  prezzo_ricambio,   
				  flag_prodotto_codificato,   
				  cod_fornitore,   
				  cod_misura,   
				  data_consegna,
				  flag_prezzo_certo,
				  flag_capitalizzato,
				  priorita,
				  cod_porto,
				  tkposi,
				  tkordi,
				  ordine_progen,
				  nrposi,
				  sconto_1_budget,
				  sconto_2_budget,
				  sconto_3_budget,
				  prezzo_netto_budget,
				  sconto_1_eff,
				  sconto_2_eff,
				  sconto_3_eff,
				  prezzo_netto_eff,
				  prezzo_ricambio_eff,
				  tkboll,
				  tkposi_b,
				  nrposi_b,
				  num_documento,
				  data_documento,
				  flag_sconto_valore,
				  cod_stato,
        cod_tipo_programma,   
         quan_consegnata,   
         flag_evaso,   
         quan_eff,   
         flag_rda,   
         ordine_progen_rda,   
         tkordi_rda,   
         tkposi_rda,   
         nrposi_rda,   
         anno_riferimento,   
         flag_confermato,   
         cod_stato_ordine,   
         flag_fatturato,   
         flag_budget,   
         flag_rda_approvata,   
         flag_ricambio_prot  				  ) 
		values (:s_cs_xx.cod_azienda,   
				  :fl_anno_dest,   
				  :fl_num_dest,   
				  :ll_prog_riga_ricambio,   
				  :ls_cod_prodotto,   
				  :ls_des_prodotto,   
				  :ld_quantita,   
				  :ld_prezzo_ricambio,   
				  :ls_flag_prodotto_codificato,   
				  :ls_cod_fornitore,   
				  :ls_cod_misura,   
				  :ldt_data_consegna,
				  :ls_flag_prezzo_certo,
				  :ls_flag_capitalizzato,
				  :ll_priorita,
				  :ls_cod_porto,
				  :ls_tkposi,
				  :ls_tkordi,
				  :ls_ordine_progen,
				  :ll_nrposi,
				  :ld_sconto_1_budget,
				  :ld_sconto_2_budget,
				  :ld_sconto_3_budget,
				  :ld_prezzo_netto_budget,
				  :ld_sconto_1_eff,
				  :ld_sconto_2_eff,
				  :ld_sconto_3_eff,
				  :ld_prezzo_netto_eff,
				  :ld_prezzo_ricambio_eff,
				  :ls_tkboll,
				  :ls_tkposi_b,
				  :ll_nrposi_b,
				  :ls_num_documento,
				  :ldt_data_documento,
				  :ls_flag_sconto_valore,
				  :ls_cod_stato,
				  :ls_cod_tipo_programma,
				  :ld_quan_consegnata,   
				 :ls_flag_evaso,   
				 :ld_quan_eff,   
				 :ls_flag_rda,   
				 :ls_ordine_progen_rda,   
				 :ls_tkordi_rda,   
				 :ls_tkposi_rda,   
				 :ll_nrposi_rda,   
				 :fl_anno_riferimento,   
				 :ls_flag_confermato,   
				 :ls_cod_stato_ordine,   
				 :ls_flag_fatturato,   
				 :ls_flag_budget,   
				 :ls_flag_rda_approvata,   
				 :ls_flag_ricambio_prot);
				  
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore durante l'operazione di copia ricambio ~r~n" + sqlca.sqlerrtext)
			return -1
		end if
		
		if not isnull(s_cs_xx.parametri.parametro_dec4_1) and s_cs_xx.parametri.parametro_dec4_1 > 0 then
			
			update prog_manutenzioni_ricambi
			set    quan_utilizzo = quan_utilizzo - :ld_quantita
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_origine and
					 num_registrazione = :fl_num_origine and
					 prog_riga_ricambio = :ll_prog_riga_ricambio;
					 
			if sqlca.sqlcode <> 0 then 
				g_mb.messagebox( "OMNIA", "Errore durante aggiornamento quantità riga origine: " + sqlca.sqlerrtext)
				return -1
			end if	
			
			// *** se la quantità = 0 allora elimino la riga principale..
			setnull(ld_quantita)
			
			select quan_utilizzo
			into   :ld_quantita
			from   prog_manutenzioni_ricambi
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_origine and
					 num_registrazione = :fl_num_origine and
					 prog_riga_ricambio = :ll_prog_riga_ricambio;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "OMNIA", "Errore durante la select della quantità di utilizzo:" + sqlca.sqlerrtext)
				return -1
			end if
			
			if isnull(ld_quantita) or ld_quantita = 0 or ld_quantita < 0 then
				
				delete from prog_manutenzioni_ricambi
				where       cod_azienda = :s_cs_xx.cod_azienda and
								anno_registrazione = :fl_anno_origine and
								num_registrazione = :fl_num_origine and
								prog_riga_ricambio = :ll_prog_riga_ricambio;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "OMNIA", "Errore durante la select della quantità di utilizzo:" + sqlca.sqlerrtext)
					return -1
				end if
			
			end if			
						
		end if		
	
	else																			// *** copio pari pari il ricambio
		
		select flag_budget
		into	:ls_flag_budget
		from	programmi_manutenzione
		where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :fl_anno_dest and
				num_registrazione = :fl_num_dest;
				
		if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
		
		if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
		if isnull(ld_prezzo_ricambio) then ld_prezzo_ricambio = 0
		if isnull(ls_flag_prodotto_codificato) then ls_flag_prodotto_codificato = "S"
		if isnull(ls_flag_prezzo_certo) then ls_flag_prezzo_certo = "N"
		if isnull(ls_flag_capitalizzato) then ls_flag_capitalizzato = "N"
		if isnull(ll_priorita) then ll_priorita = 0
		if isnull(ld_sconto_1_budget) then ld_sconto_1_budget = 0
		if isnull(ld_sconto_2_budget) then ld_sconto_2_budget = 0
		if isnull(ld_sconto_3_budget) then ld_sconto_3_budget = 0
		if isnull(ld_prezzo_netto_budget) then ld_prezzo_netto_budget = 0
		if isnull(ld_sconto_1_eff) then ld_sconto_1_eff = 0
		if isnull(ld_sconto_2_eff) then ld_sconto_2_eff = 0
		if isnull(ld_sconto_3_eff) then ld_sconto_3_eff = 0
		if isnull(ld_prezzo_netto_eff) then ld_prezzo_netto_eff = 0
		if isnull(ld_prezzo_ricambio_eff) then ld_prezzo_ricambio_eff = 0
	    if isnull(ls_flag_sconto_valore) then ls_flag_sconto_valore = "N"
		 if isnull(ld_quan_eff) then ld_quan_eff = 0
		
		INSERT INTO prog_manutenzioni_ricambi  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_ricambio,   
				  cod_prodotto,   
				  des_prodotto,   
				  quan_utilizzo,   
				  prezzo_ricambio,   
				  flag_prodotto_codificato,   
				  cod_fornitore,   
				  cod_misura,   
				  data_consegna,
				  flag_prezzo_certo,
				  flag_capitalizzato,
				  priorita,
				  cod_porto,
				  tkposi,
				  tkordi,
				  ordine_progen,
				  nrposi,
				  sconto_1_budget,
				  sconto_2_budget,
				  sconto_3_budget,
				  prezzo_netto_budget,
				  sconto_1_eff,
				  sconto_2_eff,
				  sconto_3_eff,
				  prezzo_netto_eff,
				  prezzo_ricambio_eff,
				  tkboll,
				  tkposi_b,
				  nrposi_b,
				  num_documento,
				  data_documento,
				  flag_sconto_valore,
				  cod_stato,
				  cod_tipo_programma,
				  quan_consegnata,
				  flag_evaso,
				  anno_riferimento,
				  flag_budget,
				  flag_rda,
				  quan_eff) 
		values (:s_cs_xx.cod_azienda,   
				  :fl_anno_dest,   
				  :fl_num_dest,   
				  :ll_prog_riga_ricambio,   
				  :ls_cod_prodotto,   
				  :ls_des_prodotto,   
				  :ld_quan_utilizzo,   
				  :ld_prezzo_ricambio,   
				  :ls_flag_prodotto_codificato,   
				  :ls_cod_fornitore,   
				  :ls_cod_misura,   
				  :ldt_data_consegna,
				  :ls_flag_prezzo_certo,
				  :ls_flag_capitalizzato,
				  :ll_priorita,
				  :ls_cod_porto,
				  null,
				  null,
				  null,
				  null,
				  :ld_sconto_1_budget,
				  :ld_sconto_2_budget,
				  :ld_sconto_3_budget,
				  :ld_prezzo_netto_budget,
				  :ld_sconto_1_eff,
				  :ld_sconto_2_eff,
				  :ld_sconto_3_eff,
				  :ld_prezzo_netto_eff,
				  :ld_prezzo_ricambio_eff,
				  null,
				  null,
				  null,
				  null,
				  null,
				  :ls_flag_sconto_valore,
				  :ls_cod_stato_non_eseguito,
				  :ls_cod_tipo_programma,
				  0,
				  'N',
				  :fl_anno_riferimento,
				  :ls_flag_budget,
				  'N',
				  :ld_quan_eff);
				  
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore durante l'operazione di copia ricambio ~r~n" + sqlca.sqlerrtext)
			return -1
		end if		
		
	end if
								 
loop

return 0
end function

on w_programmi_manutenzione.create
int iCurrent
call super::create
this.cb_programma_griglia=create cb_programma_griglia
this.cb_ordini=create cb_ordini
this.cb_risorse=create cb_risorse
this.cb_programmazione=create cb_programmazione
this.cb_predefinito=create cb_predefinito
this.cb_richiama_predefinito=create cb_richiama_predefinito
this.cb_cerca=create cb_cerca
this.cb_ricambi=create cb_ricambi
this.cb_documento=create cb_documento
this.htb_1=create htb_1
this.cb_controllo=create cb_controllo
this.cb_note_1=create cb_note_1
this.dw_filtro=create dw_filtro
this.st_5=create st_5
this.st_6=create st_6
this.st_8=create st_8
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.tv_1=create tv_1
this.dw_folder_search=create dw_folder_search
this.st_7=create st_7
this.dw_programmi_manutenzione_3=create dw_programmi_manutenzione_3
this.dw_grid=create dw_grid
this.dw_programmi_manutenzione=create dw_programmi_manutenzione
this.dw_programmi_manutenzione_2=create dw_programmi_manutenzione_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_programma_griglia
this.Control[iCurrent+2]=this.cb_ordini
this.Control[iCurrent+3]=this.cb_risorse
this.Control[iCurrent+4]=this.cb_programmazione
this.Control[iCurrent+5]=this.cb_predefinito
this.Control[iCurrent+6]=this.cb_richiama_predefinito
this.Control[iCurrent+7]=this.cb_cerca
this.Control[iCurrent+8]=this.cb_ricambi
this.Control[iCurrent+9]=this.cb_documento
this.Control[iCurrent+10]=this.htb_1
this.Control[iCurrent+11]=this.cb_controllo
this.Control[iCurrent+12]=this.cb_note_1
this.Control[iCurrent+13]=this.dw_filtro
this.Control[iCurrent+14]=this.st_5
this.Control[iCurrent+15]=this.st_6
this.Control[iCurrent+16]=this.st_8
this.Control[iCurrent+17]=this.st_4
this.Control[iCurrent+18]=this.st_3
this.Control[iCurrent+19]=this.st_2
this.Control[iCurrent+20]=this.st_1
this.Control[iCurrent+21]=this.tv_1
this.Control[iCurrent+22]=this.dw_folder_search
this.Control[iCurrent+23]=this.st_7
this.Control[iCurrent+24]=this.dw_programmi_manutenzione_3
this.Control[iCurrent+25]=this.dw_grid
this.Control[iCurrent+26]=this.dw_programmi_manutenzione
this.Control[iCurrent+27]=this.dw_programmi_manutenzione_2
this.Control[iCurrent+28]=this.dw_folder
end on

on w_programmi_manutenzione.destroy
call super::destroy
destroy(this.cb_programma_griglia)
destroy(this.cb_ordini)
destroy(this.cb_risorse)
destroy(this.cb_programmazione)
destroy(this.cb_predefinito)
destroy(this.cb_richiama_predefinito)
destroy(this.cb_cerca)
destroy(this.cb_ricambi)
destroy(this.cb_documento)
destroy(this.htb_1)
destroy(this.cb_controllo)
destroy(this.cb_note_1)
destroy(this.dw_filtro)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.st_8)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.tv_1)
destroy(this.dw_folder_search)
destroy(this.st_7)
destroy(this.dw_programmi_manutenzione_3)
destroy(this.dw_grid)
destroy(this.dw_programmi_manutenzione)
destroy(this.dw_programmi_manutenzione_2)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;string       ls_grid, ls_prova, ls_amministratore
windowobject lw_oggetti[],lw_oggetti3[]
long			 ll_cont

iuo_mansionario = create uo_mansionario

il_riga_copia = 0

select flag_grid_semplice_programmi
into   :ls_grid
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode =0 and ls_grid = "S" then
	
	ib_grid = true
	
	select stringa
	into   :ls_prova
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'AZP';
			 
	if sqlca.sqlcode = 0 and not isnull(ls_prova) then
		ib_sintexcal = true
	else
		ib_sintexcal = false
	end if
	
	if ib_sintexcal then
		dw_filtro.dataobject = 'd_programmi_manutenzioni_filtro_grid'
	else
		dw_filtro.dataobject = 'd_prog_manutenzioni_filtro_grid_std'
		dw_grid.dataobject = 'd_prog_manutenzione_grid_std'
	end if
	
end if

if ib_grid then set_w_options(c_NoResizeWin)

dw_programmi_manutenzione.set_dw_key("cod_azienda")
dw_programmi_manutenzione.set_dw_key("anno_registrazione")
dw_programmi_manutenzione.set_dw_key("num_registrazione")



dw_programmi_manutenzione.set_dw_options(sqlca, &
													  pcca.null_object,&
													  c_noretrieveonopen, &
													  c_NORESIZEDW)
													  
dw_programmi_manutenzione_2.set_dw_options(sqlca, &
														 dw_programmi_manutenzione, &
														 c_sharedata + c_scrollparent, &
														 c_NORESIZEDW)
														 
dw_programmi_manutenzione_3.set_dw_options(sqlca, &
														 dw_programmi_manutenzione, &
														 c_sharedata + c_scrollparent, &
														 c_NORESIZEDW)														 

iuo_dw_main = dw_programmi_manutenzione

if ib_grid then
	
	lw_oggetti[1] = dw_programmi_manutenzione
	dw_folder.fu_assigntab(2, "Generale", lw_oggetti[])
	
	lw_oggetti[1] = dw_programmi_manutenzione_3
	dw_folder.fu_assigntab(3, "Risorse", lw_oggetti[])
	
	lw_oggetti[1] = dw_programmi_manutenzione_2
	lw_oggetti[2] = cb_note_1
	lw_oggetti[3] = cb_controllo
	dw_folder.fu_assigntab(4, "Riferimenti", lw_oggetti[])
	
	lw_oggetti[1] = dw_grid
	lw_oggetti[2] = cb_ordini
	lw_oggetti[3] = cb_programma_griglia
	
	dw_folder.fu_assigntab(1, "Grid", lw_oggetti[])	
	
	dw_folder.fu_foldercreate(4, 4)
	
	dw_folder.fu_HideTab(2)
	dw_folder.fu_HideTab(3)
	dw_folder.fu_HideTab(4)
	
	dw_grid.setrowfocusindicator(FocusRect!)
	
else
	cb_programma_griglia.visible = false
	dw_grid.visible = false
		
	lw_oggetti[1] = dw_programmi_manutenzione
	dw_folder.fu_assigntab(1, "Generale", lw_oggetti[])
	
	lw_oggetti[1] = dw_programmi_manutenzione_3
	dw_folder.fu_assigntab(2, "Risorse", lw_oggetti[])
	
	lw_oggetti[1] = dw_programmi_manutenzione_2
	lw_oggetti[2] = cb_controllo
	lw_oggetti[3] = cb_note_1
	dw_folder.fu_assigntab(3, "Riferimenti", lw_oggetti[])
	
	dw_folder.fu_foldercreate(3, 4)
	
end if

dw_folder.fu_selecttab(1)

if ib_grid then 
	WindowState = Maximized!
	cb_ricambi.visible = false
	cb_programmazione.visible = false
	cb_risorse.visible = false
	cb_documento.visible = false
	if ib_sintexcal then
		
		// *** se è un mansionario allora ordini
		//     altrimenti RDA
		
		if s_cs_xx.cod_utente <> "CS_SYSTEM" then
			
			select flag_collegato
			into   :ls_amministratore
			from   utenti
			where  cod_utente = :s_cs_xx.cod_utente;
			
			if sqlca.sqlcode = 0 and ls_amministratore = "S" and not isnull(ls_amministratore) then
				cb_ordini.text = "Pre-Ordini"
			else
				
				//Giulio: 10/11/2011 cambio gestione privilegi mansionario			
//				select count(*)
//				into   :ll_cont 
//				from   mansionari
//				where  cod_azienda = :s_cs_xx.cod_azienda and
//						 cod_utente = :s_cs_xx.cod_utente and
//						 flag_autorizza_rda = 'S' and
//						 flag_approva_rda = 'S';

				if  iuo_mansionario.uof_get_privilege(iuo_mansionario.approva_rda) and iuo_mansionario.uof_get_privilege(iuo_mansionario.autorizza_rda)  then 
						 
//				if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont > 0 then
					cb_ordini.text = "Pre-Ordini"
				else
//					select count(*)
//					into   :ll_cont 
//					from   mansionari
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 cod_utente = :s_cs_xx.cod_utente and
//							 flag_autorizza_rda = 'N' and
//							 flag_approva_rda = 'S';
					
					if  iuo_mansionario.uof_get_privilege(iuo_mansionario.approva_rda) and not iuo_mansionario.uof_get_privilege(iuo_mansionario.autorizza_rda)  then 
						
//					if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont > 0 then	
						cb_ordini.text = "Crea RDA"
					else
						cb_ordini.text = "Crea RDA"
					end if
				end if
				
			end if
		end if
		
		cb_ordini.visible = true
	elseif ib_grid then
		cb_ordini.visible = false
		cb_programma_griglia.visible = true
	else
		cb_ordini.visible = false
		cb_programma_griglia.visible = false
	end if
else
	cb_ordini.visible = false
end if

// impostazione icone treeview

tv_1.deletepictures()
tv_1.PictureHeight = 16
tv_1.PictureWidth = 16

//---- categoria
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_categorie.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_categorie.png")

//---- reparto
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_reparti.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_reparti.png")

//---- area
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_area.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_area.png")

//---- attrezzatura
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_attrezzature.bmp")
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_strumenti.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_off.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_on.png")

//---- registrazione
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eseguita.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png")

//---- registrazione eliminata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eliminata.png")

//---- registrazione modificata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png")

// impostazione folder ricerca

dw_filtro.insertrow(0)
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_1", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_2", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_3", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_tipo_livello_4", "N")
dw_filtro.setitem(dw_filtro.getrow(),"flag_visione_strumenti", "T")

if ib_sintexcal then
	
	lw_oggetti3[1] = tv_1
	lw_oggetti3[2] = htb_1
	dw_folder_search.fu_assigntab(2, "Registrazioni", lw_oggetti3[])
	
	lw_oggetti3[1] = dw_filtro
	lw_oggetti3[2] = cb_cerca
	lw_oggetti3[3] = cb_predefinito
	lw_oggetti3[4] = cb_richiama_predefinito
	dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti3[])
	
	//spiegazione colori della griglia (solo ib_sintexcal = true)
	postevent("ue_legenda")
	
	lw_oggetti3[1] = st_1
	lw_oggetti3[2] = st_2
	lw_oggetti3[3] = st_3
	lw_oggetti3[4] = st_4
	lw_oggetti3[5] = st_5
	lw_oggetti3[6] = st_6
	lw_oggetti3[7] = st_7	
	lw_oggetti3[8] = st_8
	dw_folder_search.fu_assigntab(3, "Legenda", lw_oggetti3[])		
	
	dw_folder_search.fu_foldercreate(3, 3)
	dw_folder_search.fu_selecttab(1)	
	
else
	
	lw_oggetti3[1] = tv_1
	lw_oggetti3[2] = htb_1
	dw_folder_search.fu_assigntab(2, "Registrazioni", lw_oggetti3[])
	lw_oggetti3[1] = dw_filtro
	lw_oggetti3[2] = cb_cerca
	lw_oggetti3[3] = cb_predefinito
	lw_oggetti3[4] = cb_richiama_predefinito
	if ib_grid then 
	
	else
	end if
	
	dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti3[])
	dw_folder_search.fu_foldercreate(2, 2)
	dw_folder_search.fu_selecttab(1)	
	
end if

if ib_grid then
	
	f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
						  "tab_tipi_manut_scollegate","cod_tipo_manut_scollegata","des_tipo_manut_scollegata",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						  
else

	f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
						  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end if

cb_richiama_predefinito.postevent("clicked")

dw_filtro.postevent("ue_posiziona_cursore")

if ib_sintexcal then

else
	
	st_1.visible = false
	st_2.visible = false
	st_3.visible = false
	st_4.visible = false
	st_5.visible = false
	st_6.visible = false
	st_7.visible = false
	
end if

dw_filtro.postevent("ue_proteggi_campi_insert")
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_filtro,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
if ib_grid then
	f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
					  "tab_tipi_manut_scollegate","cod_tipo_manut_scollegata","des_tipo_manut_scollegata",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	if ib_sintexcal then
		f_PO_LoadDDDW_sort(dw_filtro,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cdstab is not null ", " cod_area_aziendale ASC ")						  							
	else
		f_PO_LoadDDDW_sort(dw_filtro,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " cod_area_aziendale ASC ")						  
	end if
else
	f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
							  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
							  
	f_PO_LoadDDDW_sort(dw_filtro,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")						  
end if
					
f_PO_LoadDDDW_DW(dw_filtro,"cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N'")	
					  
f_PO_LoadDDDW_DW(dw_filtro,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_programmi_manutenzione,"cod_tipo_manutenzione",sqlca,&
					  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
					  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_programmi_manutenzione,"cod_tipo_lista_dist",sqlca,&
                 "tab_tipi_liste_dist","cod_tipo_lista_dist","descrizione","")
					  
f_po_loaddddw_dw( dw_programmi_manutenzione, "cod_lista_dist", sqlca, &
						"tes_liste_dist", &
						"cod_lista_dist", &
						"des_lista_dist", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_programmi_manutenzione_2,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

f_PO_LoadDDDW_DW(dw_programmi_manutenzione_2,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' and not (flag_blocco ='S' and data_blocco<='"+string(today(),s_cs_xx.db_funzioni.formato_data)+"')")

f_PO_LoadDDDW_DW(dw_programmi_manutenzione_2,"cod_primario",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione", &
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and not (flag_blocco ='S' and data_blocco<='"+string(today(),s_cs_xx.db_funzioni.formato_data)+"') and qualita_attrezzatura = 'P'")

f_PO_LoadDDDW_sort(dw_programmi_manutenzione_3,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'", " cognome ASC, nome ASC ")

f_PO_LoadDDDW_DW(dw_programmi_manutenzione_3,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event open;call super::open;iuo_manutenzioni = create uo_manutenzioni
dw_programmi_manutenzione.object.b_ricerca_att.enabled=false
end event

event close;call super::close;destroy iuo_manutenzioni
destroy iuo_mansionario
end event

event resize;call super::resize;dw_folder.width = 2784 + (newwidth - 4100) //4014)
dw_grid.width = 2720 + (newwidth - 4100) //4014)

dw_folder.height = 2172 + (newheight - 2300)
dw_grid.height = 2008 + (newheight - 2300)

end event

type cb_programma_griglia from commandbutton within w_programmi_manutenzione
integer x = 23
integer y = 2560
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Progrm."
end type

event clicked;window_open(w_elenco_attrez, 0)

close(parent)
end event

type cb_ordini from commandbutton within w_programmi_manutenzione
integer x = 800
integer y = 2560
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pre-Ordini"
end type

event clicked;long     ll_row, ll_anno, ll_num, ll_cont
string	ls_amministratore
boolean lb_approva_rda, lb_autorizza_rda

// *** se è un mansionario allora ordini
//     altrimenti RDA

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	
	select flag_collegato
	into   :ls_amministratore
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode = 0 and ls_amministratore = "S" and not isnull(ls_amministratore) then
		window_open(w_programmi_manutenzione_ordini, 0)
	else
		
		 lb_approva_rda = iuo_mansionario.uof_get_privilege(iuo_mansionario.approva_rda)
		 lb_autorizza_rda = iuo_mansionario.uof_get_privilege(iuo_mansionario.autorizza_rda)
		
		if  lb_approva_rda and lb_autorizza_rda  then 		
			//window_open(w_programmi_manutenzione_ordini,0)
			window_open(w_programmi_manutenzione_rda,0)
		else

			if lb_approva_rda and not lb_autorizza_rda then
				WINDOW	l_Window
				PCCA.Error = Window_Type_Open(l_Window, "w_programmi_manutenzione_rda", -1)

				//window_open(w_programmi_manutenzione_rda_aut, 0)
			else
				
				window_open(w_programmi_manutenzione_rda, 0)
			end if
		end if
		
	end if
end if


end event

type cb_risorse from commandbutton within w_programmi_manutenzione
integer x = 3186
integer y = 2444
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cat.Ris.Prg"
end type

event clicked;decimal ld_ore, ld_minuti
time lt_tempo

dw_programmi_manutenzione.accepttext()

if (isnull(dw_programmi_manutenzione.getrow()) or dw_programmi_manutenzione.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato nessun programma manutenzione")
	return
end if	

if isnull(dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_tipo_manutenzione")) then
	g_mb.messagebox("Omnia", "Attenzione non è inserito nessuna tipologia di manutenzione")
	return
end if	

s_cs_xx.parametri.parametro_s_10 = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_attrezzatura")
s_cs_xx.parametri.parametro_s_11 = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_tipo_manutenzione")

//window_open_parm(w_elenco_risorse, -1, dw_tipi_manutenzione_lista)

window_open(w_elenco_risorse, 0)

setnull(s_cs_xx.parametri.parametro_d_8)
end event

type cb_programmazione from commandbutton within w_programmi_manutenzione
integer x = 2798
integer y = 2444
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Progrm."
end type

event clicked;window_open(w_elenco_attrez, 0)

close(parent)
end event

type cb_predefinito from commandbutton within w_programmi_manutenzione
integer x = 37
integer y = 2452
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Pred."
end type

event clicked;dw_filtro.accepttext()
wf_memorizza_filtro()
end event

type cb_richiama_predefinito from commandbutton within w_programmi_manutenzione
integer x = 402
integer y = 2452
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Predefinito"
end type

event clicked;wf_leggi_filtro()
end event

type cb_cerca from commandbutton within w_programmi_manutenzione
integer x = 768
integer y = 2452
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_filtro.accepttext()

// -------------  compongo SQL con filtri ---------------------

is_sql_filtro = ""
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale")) then
	is_sql_filtro += " and cod_area_aziendale = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale") + "' "
end if

if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")) then
	is_sql_filtro += " and cod_reparto = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto") + "' "
end if

if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) then
	is_sql_filtro += " and cod_cat_attrezzature = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura") + "' "
end if

if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) and dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione") <> "" then
	is_sql_filtro += " and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione") + "' ) "
end if

// ------------------------------------------------------------
ib_retrieve = false
choose case dw_filtro.getitemstring(1,"flag_eseguito")
	case "S"
		is_tipo_manut = "Storico"
	case "N"
		is_tipo_manut = "Non Eseguite"
	case "T"
		is_tipo_manut = "Tutte"
end choose

is_tipo_ordinamento = dw_filtro.getitemstring(1,"flag_ordinamento_dati")

wf_cancella_treeview()

wf_imposta_tv()

dw_folder_search.fu_selecttab(2)

tv_1.setfocus()
ib_retrieve = true

if ib_grid then
	// carico sempre il primo
	long ll_handleroot
	
	ll_handleroot = tv_1.FindItem(RootTreeItem!, 0)
	if ib_sintexcal then
		wf_carica_lista_programmi(ll_handleroot)
	elseif ib_grid then
		wf_carica_lista_programmi_std( ll_handleroot)
	end if
	il_handle = ll_handleroot
end if
	

end event

type cb_ricambi from commandbutton within w_programmi_manutenzione
integer x = 2409
integer y = 2444
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricambi"
end type

event clicked;long ll_row, ll_anno, ll_num


ll_row = dw_programmi_manutenzione.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("OMNIA","Selezionare un programma di manutenzione prima di continuare!")
	return -1
end if

ll_anno = dw_programmi_manutenzione.getitemnumber(ll_row,"anno_registrazione")

ll_num = dw_programmi_manutenzione.getitemnumber(ll_row,"num_registrazione")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 then
	g_mb.messagebox("OMNIA","Selezionare un programma di manutenzione prima di continuare!")
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno

s_cs_xx.parametri.parametro_d_2 = ll_num

window_open(w_prog_manut_ricambi,0)
end event

type cb_documento from commandbutton within w_programmi_manutenzione
integer x = 3575
integer y = 2444
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_programmi_manutenzione.accepttext()

if (isnull(dw_programmi_manutenzione.getrow()) or dw_programmi_manutenzione.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato nessun programma di manutenzione")
	return
end if	

if isnull(dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "anno_registrazione")) or &
	dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "anno_registrazione") < 2 then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato nessun programma di manutenzione")
	return
end if	

s_cs_xx.parametri.parametro_d_10 = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_d_11 = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "num_registrazione")

window_open_parm(w_det_prog_manut, -1, dw_programmi_manutenzione)
end event

type htb_1 from htrackbar within w_programmi_manutenzione
integer x = 23
integer y = 2260
integer width = 1120
integer height = 180
string pointer = "UpArrow!"
integer maxposition = 200
integer tickfrequency = 5
htickmarks tickmarks = hticksontop!
end type

event moved;parent.setredraw(false)

tv_1.width = 1097 + (scrollpos * 10)
dw_folder.x = tv_1.x + tv_1.width + 65
dw_programmi_manutenzione.x = dw_folder.x + 10
dw_programmi_manutenzione_2.x = dw_folder.x + 10
dw_programmi_manutenzione_3.x = dw_folder.x + 10
dw_grid.x = dw_folder.x + 10
cb_controllo.x = dw_folder.x + 946
//cb_ricerca_ricambio.x = dw_folder.x + 2391
//cb_ricerca_ricambio_alt.x = dw_folder.x + 2391
//cb_attrezzature_ricerca.x = dw_folder.x + 2267
parent.setredraw(true)

tv_1.setfocus()
end event

type cb_controllo from commandbutton within w_programmi_manutenzione
integer x = 2222
integer y = 848
integer width = 69
integer height = 72
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;if isnull(dw_programmi_manutenzione_2.getitemnumber(dw_programmi_manutenzione_2.getrow(),"prog_liste_con_comp")) then
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata alla manutenzione. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
end if

dw_programmi_manutenzione_2.triggerevent("ue_lista")
end event

type cb_note_1 from uo_ext_note_mss_1 within w_programmi_manutenzione
integer x = 1861
integer y = 920
integer taborder = 30
boolean bringtotop = true
end type

event clicked;call super::clicked;long   ll_row, ll_anno_registrazione, ll_num_registrazione

if not isnull(s_cs_xx.parametri.parametro_s_1) then

	ll_anno_registrazione = dw_programmi_manutenzione.getitemnumber( dw_programmi_manutenzione_2.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_programmi_manutenzione.getitemnumber( dw_programmi_manutenzione_2.getrow(), "num_registrazione")

	update programmi_manutenzione
	set    note_idl = :s_cs_xx.parametri.parametro_s_1
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       	anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in inserimento della nota.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	else
		commit;
	end if
	
	ll_row = dw_programmi_manutenzione.getrow()
	dw_programmi_manutenzione.change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	dw_programmi_manutenzione.scrolltorow(ll_row)

end if

end event

event ue_carica_valori;call super::ue_carica_valori;long   ll_row, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_programmi_manutenzione.getitemnumber( dw_programmi_manutenzione_2.getrow(), "anno_registrazione")
ll_num_registrazione = dw_programmi_manutenzione.getitemnumber( dw_programmi_manutenzione_2.getrow(), "num_registrazione")

select 	note_idl
into   		:s_cs_xx.parametri.parametro_s_1
from   	programmi_manutenzione
where  	cod_azienda = :s_cs_xx.cod_azienda and
		   	anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;

s_cs_xx.parametri.parametro_i_1 = 5000
end event

type dw_filtro from uo_std_dw within w_programmi_manutenzione
event ue_key pbm_dwnkey
event ue_posiziona_cursore ( )
integer x = 27
integer y = 136
integer width = 1184
integer height = 2380
integer taborder = 20
string dataobject = "d_programmi_manutenzioni_filtro"
boolean vscrollbar = true
boolean border = false
end type

event ue_key;string ls_barcode
long ll_anno_registrazione, ll_num_registrazione

if key = keyenter! then
	
	if this.getcolumnname() = "barcode" then
		
		ls_barcode = gettext()
		
		if isnull(ls_barcode) or len(ls_barcode) < 1 then return
		
		ll_anno_registrazione = long(mid(ls_barcode,2,4))
		
		if ll_anno_registrazione = 0 or isnull(ll_anno_registrazione) then
			g_mb.messagebox("OMNIA","Anno registrazione non corente")
			return
		end if
		
		ll_num_registrazione = long(mid(ls_barcode,6,6))
		
		if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			g_mb.messagebox("OMNIA","Numero registrazione non corente")
			return
		end if
		
		setitem(getrow(),"anno_registrazione", ll_anno_registrazione)
		setitem(getrow(),"num_registrazione", ll_num_registrazione)
		s_cs_xx.parametri.parametro_b_1 = true
	
	end if
	cb_cerca.triggerevent("clicked")
end if
end event

event ue_posiziona_cursore();if ib_grid then
	
	dw_filtro.setitem( 1, "flag_solo_attivita", "N")
	dw_filtro.setitem( 1, "flag_risorse_interne", "S")
	dw_filtro.setitem( 1, "flag_risorse_esterne", "S")
	dw_filtro.setitem( 1, "flag_ricambi", "S")	
	
end if

setfocus()
setcolumn("barcode")

end event

event itemchanged;string ls_null, ls_text


setnull(ls_null)

ls_text = gettext()

choose case getcolumnname()
		
	case "cod_attrezzatura"
		if ls_text = "" or isnull(ls_text) then
			setitem(getrow(),"cod_tipo_manutenzione",ls_null)
			f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
								  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

		else
			setitem(getrow(),"cod_tipo_manutenzione",ls_null)
			f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
								  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_text + "'")

		end if
		
	case "flag_tipo_livello_1"
		setitem(getrow(),"flag_tipo_livello_2", "N")
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		setitem(getrow(),"flag_tipo_livello_5", "N")
		
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		settaborder("flag_tipo_livello_5", 0)
		
		setitem(getrow(),"flag_apri_liv_2", "N")
		setitem(getrow(),"flag_apri_liv_3", "N")
		setitem(getrow(),"flag_apri_liv_4", "N")
		setitem(getrow(),"flag_apri_liv_5", "N")
		
		settaborder("flag_apri_liv_2", 0)
		settaborder("flag_apri_liv_3", 0)
		settaborder("flag_apri_liv_4", 0)
		settaborder("flag_apri_liv_5", 0)
		
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_2", 150)
			settaborder("flag_apri_liv_2", 151)
		end if
		
	case "flag_tipo_livello_2"
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		setitem(getrow(),"flag_tipo_livello_5", "N")
		
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		settaborder("flag_tipo_livello_5", 0)
		
		setitem(getrow(),"flag_apri_liv_3", "N")
		setitem(getrow(),"flag_apri_liv_4", "N")
		setitem(getrow(),"flag_apri_liv_5", "N")
		
		settaborder("flag_apri_liv_3", 0)
		settaborder("flag_apri_liv_4", 0)
		settaborder("flag_apri_liv_5", 0)
		
		
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_3", 160)
			settaborder("flag_apri_liv_3", 161)
		end if
		
	case "flag_tipo_livello_3"
		setitem(getrow(),"flag_tipo_livello_4", "N")
		setitem(getrow(),"flag_tipo_livello_5", "N")
		
		settaborder("flag_tipo_livello_4", 0)
		settaborder("flag_tipo_livello_5", 0)
		
		setitem(getrow(),"flag_apri_liv_4", "N")
		setitem(getrow(),"flag_apri_liv_5", "N")
		
		settaborder("flag_apri_liv_4", 0)
		settaborder("flag_apri_liv_5", 0)
		
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_4", 170)
			settaborder("flag_apri_liv_4", 171)
		end if
		
	case "flag_tipo_livello_4"
		setitem(getrow(),"flag_tipo_livello_5", "N")
		
		settaborder("flag_tipo_livello_5", 0)
		
		setitem(getrow(),"flag_apri_liv_5", "N")
		
		settaborder("flag_apri_liv_5", 0)
		
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_5", 180)
			settaborder("flag_apri_liv_5", 181)
		end if
		
	case	"flag_apri_liv_1"
		if ls_text = "S" then
			setitem(getrow(),"flag_apri_liv_2", "N")
			setitem(getrow(),"flag_apri_liv_3", "N")
			setitem(getrow(),"flag_apri_liv_4", "N")
			setitem(getrow(),"flag_apri_liv_5", "N")
		end if
		
	case	"flag_apri_liv_2"
		if ls_text = "S" then
			setitem(getrow(),"flag_apri_liv_1", "N")
			setitem(getrow(),"flag_apri_liv_3", "N")
			setitem(getrow(),"flag_apri_liv_4", "N")
			setitem(getrow(),"flag_apri_liv_5", "N")
		end if
		
	case	"flag_apri_liv_3"
		if ls_text = "S" then
			setitem(getrow(),"flag_apri_liv_1", "N")
			setitem(getrow(),"flag_apri_liv_2", "N")
			setitem(getrow(),"flag_apri_liv_4", "N")
			setitem(getrow(),"flag_apri_liv_5", "N")
		end if
		
	case	"flag_apri_liv_4"
		if ls_text = "S" then
			setitem(getrow(),"flag_apri_liv_1", "N")
			setitem(getrow(),"flag_apri_liv_2", "N")
			setitem(getrow(),"flag_apri_liv_3", "N")
			setitem(getrow(),"flag_apri_liv_5", "N")
		end if
		
	case	"flag_apri_liv_5"
		if ls_text = "S" then
			setitem(getrow(),"flag_apri_liv_1", "N")
			setitem(getrow(),"flag_apri_liv_2", "N")
			setitem(getrow(),"flag_apri_liv_3", "N")
			setitem(getrow(),"flag_apri_liv_4", "N")
		end if		
		
end choose
end event

event clicked;choose case dwo.name
	
	case "b_attrezzatura"
		
		guo_ricerca.uof_ricerca_attrezzatura(dw_filtro,"cod_attrezzatura")
	
	case "b_fornitore"
		
//		s_cs_xx.parametri.parametro_dw_1 = dw_filtro
//		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		guo_ricerca.uof_ricerca_fornitore(dw_filtro,"cod_fornitore")
	
	case "b_prodotto"
	
//		setnull(s_cs_xx.parametri.parametro_s_10)
//		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		guo_ricerca.uof_ricerca_prodotto(dw_filtro,"cod_prodotto")	
		
//		if not isnull(s_cs_xx.parametri.parametro_s_10) and s_cs_xx.parametri.parametro_s_10 <> "" then
//			dw_filtro.setitem( 1, "cod_prodotto",s_cs_xx.parametri.parametro_s_10)
//		else
//			dw_filtro.setitem( 1, "cod_prodotto", "")
//		end if
	
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_filtro,"cod_attrezzatura")
end choose
end event

type st_5 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 716
integer width = 960
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Richiesta di Approvigionamento"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_6 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 796
integer width = 965
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Riga a Budget"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_8 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 636
integer width = 960
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "RDA Non Approvata"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_4 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 476
integer width = 960
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Pre-Ordine di Acquisto"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 396
integer width = 960
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ordine"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 316
integer width = 960
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Buono D~'Ingresso"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 236
integer width = 960
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documento Fatturato"
alignment alignment = center!
boolean focusrectangle = false
end type

type tv_1 from treeview within w_programmi_manutenzione
event ue_espandi_rami ( )
integer x = 46
integer y = 140
integer width = 1097
integer height = 2160
integer taborder = 120
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean linesatroot = true
boolean hideselection = false
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event ue_espandi_rami();string ls_livello
long ll_handle

ls_livello = ""

if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_apri_liv_1") = "S" then ls_livello = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_1")
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_apri_liv_2") = "S" then ls_livello = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_2")
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_apri_liv_3") = "S" then ls_livello = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_3")
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_apri_liv_4") = "S" then ls_livello = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_4")
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_apri_liv_5") = "S" then ls_livello = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_5")

if len(ls_livello) < 1 or ls_livello = "N" then
	return
else
	ll_handle = tv_1.finditem(roottreeitem!,0)
	tv_1.selectitem(ll_handle)
	if ll_handle > 0 then
		wf_posiziona_livello(ll_handle, ls_livello)
	end if
end if

end event

event selectionchanged;if ib_retrieve then

	treeviewitem ltv_item
	ws_record lws_record
	
	if isnull(newhandle) or newhandle <= 0 then
		return 0
	end if
	
	il_handle = newhandle
	
	getitem(newhandle,ltv_item)
	
	lws_record = ltv_item.data
	
	dw_programmi_manutenzione.change_dw_current()
	parent.postevent("pc_retrieve")
	
	if ib_sintexcal then
		wf_carica_lista_programmi(il_handle)
	elseif ib_grid then
		wf_carica_lista_programmi_std(il_handle)
	end if

end if
end event

event itempopulate;string ls_null
long ll_livello = 0
treeviewitem ltv_item
ws_record lws_record

setnull(ls_null)

if isnull(handle) or handle <= 0 then
	return 0
end if

getitem(handle,ltv_item)

lws_record = ltv_item.data
ll_livello = lws_record.livello

il_livello_corrente = ll_livello

if ll_livello < 5 then
	// controlla cosa c'è al livello successivo ???
	choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(ll_livello + 1))
		case "D"
			// caricamento divisioni
			if wf_inserisci_divisioni(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
		
		case "R"
			// caricamento reparti
			if wf_inserisci_reparti(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "C"
			// caricamento categorie
			if wf_inserisci_categorie(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "E" 
			// caricamento aree
			if wf_inserisci_aree(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "A"
			// caricamento attrezzature
			if wf_inserisci_attrezzature(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "N" 
			if wf_inserisci_registrazioni(handle, ls_null) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
else
	// inserisco le registrazioni
	if wf_inserisci_registrazioni(handle, ls_null) = 1 then
		ltv_item.children = false
		tv_1.setitem(handle, ltv_item)
	end if
	
end if

commit;
end event

event losefocus;tv_1.width = 1097
end event

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_dw_1 = dw_filtro
s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura"
end event

type dw_folder_search from u_folder within w_programmi_manutenzione
integer y = 20
integer width = 1253
integer height = 2520
integer taborder = 40
end type

type st_7 from statictext within w_programmi_manutenzione
integer x = 82
integer y = 556
integer width = 960
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "RDA Approvata"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_programmi_manutenzione_3 from uo_cs_xx_dw within w_programmi_manutenzione
integer x = 1294
integer y = 140
integer width = 2743
integer height = 1700
integer taborder = 160
string dataobject = "d_programmi_manutenzione_3"
boolean border = false
end type

event itemchanged;call super::itemchanged;string   ls_cod_tipo_manutenzione, ls_flag_manutenzione, ls_flag_ricambio_codificato, ls_note, &
         ls_cod_cat_risorse_esterne, ls_cod_cat_attrezzature, &
			ls_cod_ricambio, ls_des_ricambio, ls_cod_attrezzatura, ls_testo, ls_cod_ricambio_alternativo, ls_null, &
			ls_cod_tipo_lista_dist, ls_cod_lista_dist

datetime ldt_tempo_previsto

long     ll_num_reg_lista, ll_riga, ll_i, ll_y, ll_ore, ll_minuti

double   ld_tariffa_std, ld_diritto_chiamata

setnull(ls_null)

choose case i_colname
		
	case "flag_ricambio_codificato"
		
		wf_prodotto_codificato(i_coltext)
		
	case "cod_risorsa_esterna"
		
		setitem(getrow(),"risorsa_costo_orario", 0)
		
		setitem(getrow(),"risorsa_costi_aggiuntivi", 0)
		
		ls_cod_cat_risorse_esterne = getitemstring(getrow(),"cod_cat_risorse_esterne")
		
		select tariffa_std, 
		       diritto_chiamata
		into   :ld_tariffa_std, 
		       :ld_diritto_chiamata
		from   anag_risorse_esterne
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
				 cod_risorsa_esterna = :i_coltext ;
				 
		if sqlca.sqlcode = 0 then
			
			setitem(getrow(),"risorsa_costo_orario", ld_tariffa_std)
			
			setitem(getrow(),"risorsa_costi_aggiuntivi", ld_diritto_chiamata)
			
		end if
		
	case "cod_operaio"
		
		setitem(getrow(),"costo_orario_operaio", 0)
		
		select cod_cat_attrezzature
		into   :ls_cod_cat_attrezzature  
		from   anag_operai
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_operaio = :i_coltext ;
				 
		if sqlca.sqlcode = 0 then
			
			select costo_medio_orario
			into   :ld_tariffa_std
			from   tab_cat_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_cat_attrezzature = :ls_cod_cat_attrezzature ;
					 
			if sqlca.sqlcode = 0 then
				
				setitem(getrow(),"costo_orario_operaio", ld_tariffa_std)
				
			end if
			
		end if
		
	case "cod_ricambio"
		
		setitem(getrow(),"costo_unitario_ricambio", 0 )
		
		SELECT costo_ultimo  
		INTO   :ld_tariffa_std  
		FROM   anag_prodotti  
		WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
				( cod_prodotto = :i_coltext )   ;

		if sqlca.sqlcode = 0 then
			
			setitem(getrow(),"costo_unitario_ricambio", ld_tariffa_std)
			
		end if
		
	case "cod_ricambio_alternativo"
		
		setitem(getrow(),"costo_unitario_ricambio", 0 )
		
	   SELECT costo_ultimo  
		INTO   :ld_tariffa_std  
		FROM   anag_prodotti  
		WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
				( cod_prodotto = :i_coltext )   ;
	
		if sqlca.sqlcode = 0 then
			
			setitem(getrow(),"costo_unitario_ricambio", ld_tariffa_std)
			
		end if
		
	case "cod_cat_risorse_esterne"
		
		f_PO_LoadDDDW_DW( dw_programmi_manutenzione_3, &
		                  "cod_risorsa_esterna", &
								sqlca, &		
							   "anag_risorse_esterne", &
								"cod_risorsa_esterna", &
								"descrizione", &
							   "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "' and not (flag_blocco ='S' and data_blocco<='"+string(today(),s_cs_xx.db_funzioni.formato_data)+"')")

		setitem(getrow(),"cod_risorsa_esterna",ls_null)
		
end choose
   

end event

event pcd_retrieve;call super::pcd_retrieve;//LONG  l_Error, ll_ret
//ws_record lstr_record
//
//ll_ret = tv_1.getitem(il_handle, tvi_campo)
//
//if ll_ret < 0 then return
//
//lstr_record = tvi_campo.data
//
//if not(lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0) then
//	setnull(lstr_record.anno_registrazione)
//	setnull(lstr_record.num_registrazione)
//end if
//l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
//if l_Error < 0 then
//	PCCA.Error = c_Fatal
//else
//	if l_error = 1 then wf_flag_tipo_intervento(dw_programmi_manutenzione.getitemstring(1,"flag_tipo_intervento"), dw_programmi_manutenzione.getitemstring(1,"flag_ricambio_codificato"))
//end if
//	
//il_anno_registrazione = lstr_record.anno_registrazione
//il_num_registrazione = lstr_record.num_registrazione
//	
//
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()

	case "cod_ricambio"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_programmi_manutenzione_3,"cod_ricambio")	
		end if
	case "cod_ricambio_alternativo"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_programmi_manutenzione_3,"cod_ricambio")		
		end if
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_ricambio"
		guo_ricerca.uof_ricerca_prodotto(dw_programmi_manutenzione_3, "cod_ricambio")
	case "b_ricerca_ricambio_alt"
		guo_ricerca.uof_ricerca_prodotto(dw_programmi_manutenzione_3, "cod_ricambio_alternativo")
end choose
end event

type dw_grid from datawindow within w_programmi_manutenzione
event ue_elimina ( )
event ue_attivita ( )
event ue_ricambio ( )
event ue_risorsa ( )
event ue_copia ( )
event ue_incolla ( )
event ue_operaio ( )
event ue_modifica ( )
event ue_incolla_attrezzatura ( )
event ue_programma ( )
event ue_operaio_rda ( )
event ue_ricambio_rda ( )
event ue_sposta ( )
event ue_esegui ( )
event ue_commenti ( )
integer x = 1303
integer y = 160
integer width = 2720
integer height = 2248
integer taborder = 170
string title = "none"
string dataobject = "d_programmi_manutenzione_grid"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_elimina();long ll_anno_registrazione,ll_num_registrazione, ll_progressivo, ll_riga, ll_anno_riferimento_rif, &
     ll_anno_reg_manut, ll_num_reg_manut, ll_cont
string ls_flag_tipo_risorsa, ls_tkordi, ls_flag_budget_rif

if getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	if g_mb.messagebox("OMNIA", "Cancello la riga selezionata e le sue righe collegate?", question!, yesNo!, 2) = 2 then return
else
	if g_mb.messagebox("OMNIA", "Cancello la risorsa/ricambio selezionata?", question!, yesNo!, 2) = 2 then return
end if

ll_riga = il_rbuttonrow

choose case getitemstring(il_rbuttonrow, "flag_tipo_riga")
		
	case "P"
		
		// *** cancellazione di una riga principale = cancellazione attività e risorse collegate
		
		ll_anno_registrazione = getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = getitemnumber(il_rbuttonrow, "num_registrazione")
		
		if isnull(ll_anno_registrazione) or ll_anno_registrazione = 0 then return
		
		select flag_budget,
				anno_riferimento
		into	:ls_flag_budget_rif,
				:ll_anno_riferimento_rif
		from	programmi_manutenzione
		where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "OMNIA", "Errore durante il controllo budget/anno riferimento dell'attività:" + sqlca.sqlerrtext, stopsign!)
			rollback;
			return 
		end if
		
		if isnull(ls_flag_budget_rif) or ls_flag_budget_rif = "" then ls_flag_budget_rif = "N"
		if ls_flag_budget_rif = "S" then
			
			ls_flag_budget_rif = "N"
			
			select flag_chiuso
			into	:ls_flag_budget_rif
			from	prog_manutenzioni_anni
			where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_riferimento = :ll_anno_riferimento_rif;
					
			if sqlca.sqlcode = 0 and not isnull(ls_flag_budget_rif) and ls_flag_budget_rif = "S" then
				g_mb.messagebox( "OMNIA", "Attenzione: il budget per l'anno " + string(ll_anno_riferimento_rif) + " risulta consolidato, quindi non è possibile cancellare righe a budget per quest'anno!", stopsign!)
				rollback;
				return 
			end if
			
		end if
		
		delete prog_manutenzioni_risorse_est
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in cancellazione Risorse Esterne.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
			
		delete prog_manutenzioni_operai
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in cancellazione Risorse Interne.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		ll_cont = 0
		
		select count(*)
		into   :ll_cont
		from   prog_manutenzioni_ric_ddi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
				 
		if not isnull(ll_cont) and ll_cont > 0 then
			g_mb.messagebox("OMNIA", "Impossibile cancellare la riga. Risultano DDI collegati.", stopsign!)
			rollback;
			return					
		end if
		
		ll_cont = 0
		
		select count(*)
		into   :ll_cont
		from   prog_manutenzioni_ricambi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 tkordi is not null and tkordi <> '';
				 
		if not isnull(ll_cont) and ll_cont > 0 then
			g_mb.messagebox("OMNIA", "Impossibile cancellare la riga. Risultano ordini collegati.", stopsign!)
			rollback;
			return					
		end if						 		
		
		ll_cont = 0
		
		select count(*)
		into   :ll_cont
		from   prog_manutenzioni_ricambi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 tkordi_rda is not null and tkordi_rda <> '';
				 
		if not isnull(ll_cont) and ll_cont > 0 then
			g_mb.messagebox("OMNIA", "Impossibile cancellare la riga. Risultano RDA collegate.", stopsign!)
			rollback;
			return					
		end if					
		
		delete prog_manutenzioni_ricambi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in cancellazione Ricambi.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		// procedo con eventuale cancellazione attività di manutenzione già
		// create in tabella manutenzioni
		
		declare cu_manutenzioni cursor for
		select anno_registrazione, num_registrazione
		from   manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_reg_programma = :ll_anno_registrazione and
				 num_reg_programma = :ll_num_registrazione;
		
		open cu_manutenzioni;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in cancellazione Registrazioni di manutenzione.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		do while true
			fetch cu_manutenzioni into :ll_anno_reg_manut, :ll_num_reg_manut;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore in cancellazione Registrazioni di manutenzione.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			delete manutenzioni_ricambi
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore in cancellazione Ricambi in Manutenzioni.~r~nForse sono già state pianificate attività.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			delete manutenzioni_operai
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore in cancellazione Operatori Manutenzioni.~r~nForse sono già state pianificate attività.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			delete manutenzioni_risorse_esterne
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore in cancellazione Risorse Est. Manutenzioni.~r~nForse sono già state pianificate attività.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			delete det_manutenzioni
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore in cancellazione Documenti Manutenzioni.~r~nForse sono già state pianificate attività.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			delete manutenzioni_destinatari
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore in cancellazione Documenti Manutenzioni.~r~nForse sono già state pianificate attività.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			delete manutenzioni
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_reg_manut and
					 num_registrazione = :ll_num_reg_manut;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore in cancellazione Registrazione Manutenzioni.~r~nForse sono già state pianificate attività.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
		loop	
		
		close cu_manutenzioni;
				 
		// cancello programma manutenzione
		delete programmi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in cancellazione Piano di Manutenziona.~r~nForse sono già state pianificate attività.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	case "C"
		
		// *** cancellazione di risorse collegate ad un'attività
		
		ll_anno_registrazione = getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = getitemnumber(il_rbuttonrow, "num_registrazione")
		ll_progressivo  = getitemnumber(il_rbuttonrow, "progressivo")
		ls_flag_tipo_risorsa = getitemstring(il_rbuttonrow, "flag_tipo_risorsa")
		
		/* flag_tipo risorsa (solo nella riga collegata)
			I = risorsa interna = operaio
			R = ricambio
			E = risorsa esterna		*/
		choose case ls_flag_tipo_risorsa
				
			case "R"
				
				select flag_budget,
						anno_riferimento
				into	:ls_flag_budget_rif,
						:ll_anno_riferimento_rif
				from	prog_manutenzioni_ricambi
				where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						prog_riga_ricambio = :ll_progressivo;
						
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "OMNIA", "Errore durante il controllo budget/anno riferimento del ricambio:" + sqlca.sqlerrtext, stopsign!)
					rollback;
					return 
				end if
				
				if isnull(ls_flag_budget_rif) or ls_flag_budget_rif = "" then ls_flag_budget_rif = "N"
				if ls_flag_budget_rif = "S" then
					
					ls_flag_budget_rif = "N"
					
					select flag_chiuso
					into	:ls_flag_budget_rif
					from	prog_manutenzioni_anni
					where	cod_azienda = :s_cs_xx.cod_azienda and
							anno_riferimento = :ll_anno_riferimento_rif;
							
					if sqlca.sqlcode = 0 and not isnull(ls_flag_budget_rif) and ls_flag_budget_rif = "S" then
						g_mb.messagebox( "OMNIA", "Attenzione: il budget per l'anno " + string(ll_anno_riferimento_rif) + " risulta consolidato, quindi non è possibile cancellare righe a budget per quest'anno!", stopsign!)
						rollback;
						return 
					end if
					
				end if			
				
				select count(*)
				into   :ll_cont
				from   prog_manutenzioni_ric_ddi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ricambio = :ll_progressivo;
						 
				if not isnull(ll_cont) and ll_cont > 0 then
					g_mb.messagebox("OMNIA", "Impossibile cancellare la riga. Risultano DDI collegati.", stopsign!)
					rollback;
					return					
				end if
				
				select tkordi
				into   :ls_tkordi
				from   prog_manutenzioni_ricambi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ricambio = :ll_progressivo;
						 
				if not isnull(ls_tkordi) and ls_tkordi <> "" then
					g_mb.messagebox("OMNIA", "Impossibile cancellare la riga. Risultano ordini collegati.", stopsign!)
					rollback;
					return					
				end if	
				
				setnull(ls_tkordi)
				
				select tkordi_rda
				into   :ls_tkordi
				from   prog_manutenzioni_ricambi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ricambio = :ll_progressivo;
						 
				if not isnull(ls_tkordi) and ls_tkordi <> "" then
					g_mb.messagebox("OMNIA", "Impossibile cancellare la riga. Risultano RDA collegate.", stopsign!)
					rollback;
					return					
				end if							
						 
				
				delete prog_manutenzioni_ricambi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ricambio = :ll_progressivo;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("OMNIA", "Errore in cancellazione Ricambi.~r~n" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				 
				
			case "I"
				
				select flag_budget,
						anno_riferimento
				into	:ls_flag_budget_rif,
						:ll_anno_riferimento_rif
				from	prog_manutenzioni_operai
				where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						progressivo = :ll_progressivo;
						
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "OMNIA", "Errore durante il controllo budget/anno riferimento della riga di risorsa interna:" + sqlca.sqlerrtext, stopsign!)
					rollback;
					return 
				end if
				
				if isnull(ls_flag_budget_rif) or ls_flag_budget_rif = "" then ls_flag_budget_rif = "N"
				if ls_flag_budget_rif = "S" then
					
					ls_flag_budget_rif = "N"
					
					select flag_chiuso
					into	:ls_flag_budget_rif
					from	prog_manutenzioni_anni
					where	cod_azienda = :s_cs_xx.cod_azienda and
							anno_riferimento = :ll_anno_riferimento_rif;
							
					if sqlca.sqlcode = 0 and not isnull(ls_flag_budget_rif) and ls_flag_budget_rif = "S" then
						g_mb.messagebox( "OMNIA", "Attenzione: il budget per l'anno " + string(ll_anno_riferimento_rif) + " risulta consolidato, quindi non è possibile cancellare righe a budget per quest'anno!", stopsign!)
						rollback;
						return 
					end if
					
				end if							
				
				delete prog_manutenzioni_operai
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 progressivo = :ll_progressivo;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("OMNIA", "Errore in cancellazione Risorse Interne.~r~n" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				 
				
			case "E"
				
				delete prog_manutenzioni_risorse_est
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 progressivo = :ll_progressivo;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("OMNIA", "Errore in cancellazione Risorse Esterne.~r~n" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				
		end choose
		
end choose

commit;
if ib_sintexcal then
	wf_carica_lista_programmi(il_handle)
else
	wf_carica_lista_programmi_std(il_handle)
end if

dw_grid.scrolltorow(ll_riga)
end event

event ue_attivita();long ll_riga
string	ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore

// significa Nuova attività
s_cs_xx.parametri.parametro_s_1 = "N"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid

ll_riga = il_rbuttonrow

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if


if ib_sintexcal then
	window_open_parm(w_programmi_manutenzione_attivita, 0, dw_grid)
elseif ib_grid then
	window_open_parm(w_prog_manutenzione_attivita_std, 0, dw_grid)
end if

setnull(s_cs_xx.parametri.parametro_s_1)

if ib_sintexcal then
	if s_cs_xx.parametri.parametro_b_1 then
		wf_carica_lista_programmi(il_handle)
	end if
else
	wf_carica_lista_programmi_std(il_handle)
end if

dw_grid.scrolltorow(ll_riga)
end event

event ue_ricambio();long ll_riga
string	ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore

s_cs_xx.parametri.parametro_s_1 = "N"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid
ll_riga = il_rbuttonrow

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if

//	***	se l'attività per l'anno di riferimento risulta bloccata non posso inserire ricambi nuovi

long	ll_anno, ll_numero, ll_anno_riferimento, ll_prova
string	ls_cod_tipo_manutenzione

ll_anno = getitemnumber( ll_riga, "anno_registrazione")
ll_numero = getitemnumber( ll_riga, "num_registrazione")

select cod_tipo_manutenzione,
		anno_riferimento
into	:ls_cod_tipo_manutenzione,
		:ll_anno_riferimento
from	programmi_manutenzione
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno and
		num_registrazione = :ll_numero;
		
if sqlca.sqlcode = 0 then
	
	if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
		ll_anno_riferimento = f_anno_riferimento()
	end if
	
	if ll_anno_riferimento > 0 then
		
		select count(*)
		into	:ll_prova
		from	prog_manutenzioni_anni_tm
		where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_riferimento = :ll_anno_riferimento and
				cod_tipo_manut_scollegata IN (select cod_tipo_manut_scollegata
														from	tab_tipi_manut_scollegate
														where	cod_azienda = :s_cs_xx.cod_azienda and
																cod_tipo_manut_collegata = :ls_cod_tipo_manutenzione) and
				flag_blocco = 'S';
																
		if sqlca.sqlcode = 0 and not isnull(ll_prova) and ll_prova > 0 then
			g_mb.messagebox( "OMNIA", "Attenzione: la tipologia di manutenzione " + ls_cod_tipo_manutenzione + " risulta bloccata per l'anno di riferimento " + string(ll_anno_riferimento) + "! E' quindi impossibile aggiungere nuovi ricambi all'attività!", stopsign!)
			return 
		elseif sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante il controllo del blocco della tipologia di manutenzione:" + sqlca.sqlerrtext, stopsign!)
			return
		end if
		
	end if
	
else
	
	g_mb.messagebox( "OMNIA", "Errore durante il controllo della tipologia di manutenzione:" + sqlca.sqlerrtext, stopsign!)
	return
	
end if

if ib_sintexcal then
	window_open_parm(w_programmi_manutenzione_ricambio, 0, dw_grid)
	if s_cs_xx.parametri.parametro_b_1 then
		wf_carica_lista_programmi(il_handle)
	end if
else
	window_open_parm(w_prog_manutenzione_ricambio_std, 0, dw_grid)
	wf_carica_lista_programmi_std(il_handle)
end if

dw_grid.scrolltorow(ll_riga)
end event

event ue_risorsa();long ll_riga
string	ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore

ll_riga = il_rbuttonrow

s_cs_xx.parametri.parametro_s_1 = "N"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if

if ib_sintexcal then
	window_open_parm(w_programmi_manutenzione_risorse, 0, dw_grid)
	if (s_cs_xx.parametri.parametro_b_1) then
		wf_carica_lista_programmi(il_handle)
	end if
else
	window_open_parm(w_prog_manutenzione_risorse_std, 0, dw_grid)
	wf_carica_lista_programmi_std(il_handle)	
end if

dw_grid.scrolltorow(ll_riga)
end event

event ue_copia();il_riga_copia = il_rbuttonrow
end event

event ue_incolla();string ls_flag_tipo_riga, ls_flag_tipo_risorsa
long ll_row, ll_ret

if il_riga_copia > 0 then
	
	// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
	ls_flag_tipo_riga = dw_grid.getitemstring(il_riga_copia, "flag_tipo_riga")
	if ls_flag_tipo_riga = "P" then
		
		if ib_sintexcal then
			if wf_incolla_piano_manutenzione(il_riga_copia, il_rbuttonrow,"T") = -1 then
				rollback;
				il_riga_copia = 0	
			else
				commit;
				
				if ib_sintexcal then
					wf_carica_lista_programmi(il_handle)
				else
					wf_carica_lista_programmi_std(il_handle)
				end if
				
				dw_grid.scrolltorow(il_riga_copia)
				
				il_riga_copia = 0				
				
			end if
		elseif ib_grid then
			if wf_incolla_piano_manutenzione(il_riga_copia, il_rbuttonrow,"T") = -1 then
				rollback;
				il_riga_copia = 0	
			else
				commit;
				
				if ib_sintexcal then
					wf_carica_lista_programmi(il_handle)
				else
					wf_carica_lista_programmi_std(il_handle)
				end if
				
				dw_grid.scrolltorow(il_riga_copia)
				
				il_riga_copia = 0							
				
			end if
		end if
		
	else
		/* flag_tipo risorsa (solo nella riga collegata)
					I = risorsa interna = operaio
					R = ricambio
					E = risorsa esterna
		*/
		ls_flag_tipo_risorsa = dw_grid.getitemstring(il_riga_copia, "flag_tipo_risorsa")
		if ls_flag_tipo_risorsa = "R" then
			
			ll_ret = g_mb.messagebox( "OMNIA", "Continuare con la semplice copia della riga di ricambio (senza eventuali riferimenti all'ordine)?", Exclamation!, YesNo!, 2)
			if ll_ret <> 1 then 
				il_riga_copia = 0
				return 			
			end if			
			
			if wf_incolla_ricambio_semplice( il_riga_copia, il_rbuttonrow,"T") = -1 then
				rollback;
				il_riga_copia = 0	
			else
				commit;
				
				if ib_sintexcal then
					wf_carica_lista_programmi(il_handle)
				else
					wf_carica_lista_programmi_std(il_handle)
				end if
				
				dw_grid.scrolltorow(il_riga_copia)
				
				il_riga_copia = 0							
				
			end if			
			
		end if
	end if	
end if


end event

event ue_operaio();long ll_riga
string	ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore

ll_riga = il_rbuttonrow

s_cs_xx.parametri.parametro_s_1 = "N"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if

//	***	se l'attività per l'anno di riferimento risulta bloccata non posso inserire risorse interne nuove

long	ll_anno, ll_numero, ll_anno_riferimento, ll_prova
string	ls_cod_tipo_manutenzione

ll_anno = getitemnumber( ll_riga, "anno_registrazione")
ll_numero = getitemnumber( ll_riga, "num_registrazione")

select cod_tipo_manutenzione,
		anno_riferimento
into	:ls_cod_tipo_manutenzione,
		:ll_anno_riferimento
from	programmi_manutenzione
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno and
		num_registrazione = :ll_numero;
		
if sqlca.sqlcode = 0 then
	
	if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
		ll_anno_riferimento = f_anno_riferimento()
	end if
	
	if ll_anno_riferimento > 0 then
		
		select count(*)
		into	:ll_prova
		from	prog_manutenzioni_anni_tm
		where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_riferimento = :ll_anno_riferimento and
				cod_tipo_manut_scollegata IN (select cod_tipo_manut_scollegata
														from	tab_tipi_manut_scollegate
														where	cod_azienda = :s_cs_xx.cod_azienda and
																cod_tipo_manut_collegata = :ls_cod_tipo_manutenzione) and
				flag_blocco = 'S';
																
		if sqlca.sqlcode = 0 and not isnull(ll_prova) and ll_prova > 0 then
			g_mb.messagebox( "OMNIA", "Attenzione: la tipologia di manutenzione " + ls_cod_tipo_manutenzione + " risulta bloccata per l'anno di riferimento " + string(ll_anno_riferimento) + "! E' quindi impossibile aggiungere nuove risorse interne!", stopsign!)
			return 
		elseif sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante il controllo del blocco della tipologia di manutenzione:" + sqlca.sqlerrtext, stopsign!)
			return
		end if
		
	end if
	
else
	
	g_mb.messagebox( "OMNIA", "Errore durante il controllo della tipologia di manutenzione:" + sqlca.sqlerrtext, stopsign!)
	return
	
end if

if ib_sintexcal then
	window_open_parm(w_programmi_manutenzione_operai, 0, dw_grid)
	if s_cs_xx.parametri.parametro_b_1 then
		wf_carica_lista_programmi(il_handle)
	end if
else
	window_open_parm(w_prog_manutenzione_operai_std, 0, dw_grid)
	wf_carica_lista_programmi_std(il_handle)
end if



dw_grid.scrolltorow(ll_riga)
end event

event ue_modifica();long 		ll_riga, ll_anno, ll_numero, ll_prog
string		ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore, ls_flag_rda
boolean	lb_rda

// significa Modifica attività
s_cs_xx.parametri.parametro_s_1 = "M"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid
s_cs_xx.parametri.parametro_b_1 = false

ll_riga = il_rbuttonrow

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if

// *** controllo di che tipo è l'operatore

lb_rda = wf_operatore_rda()

if getitemstring(ll_riga, "flag_tipo_riga") = "P" then
	
	if ib_sintexcal then
		window_open_parm(w_programmi_manutenzione_attivita, 0, dw_grid)
	elseif ib_grid then
		window_open_parm(w_prog_manutenzione_attivita_std, 0, dw_grid)
	end if	
	setnull(s_cs_xx.parametri.parametro_s_1)
	
	if s_cs_xx.parametri.parametro_b_1 then
		if ib_sintexcal then
			wf_carica_lista_programmi(il_handle)
		else
			wf_carica_lista_programmi_std(il_handle)
		end if
		dw_grid.scrolltorow(ll_riga)
	end if
	
else
	
	if is_nome = "cod_tipo_manutenzione" or is_nome = "des_manutenzione" then
		
		if ib_sintexcal then
			window_open_parm(w_programmi_manutenzione_attivita, 0, dw_grid)
		elseif ib_grid then
			window_open_parm(w_prog_manutenzione_attivita_std, 0, dw_grid)
		end if	
		setnull(s_cs_xx.parametri.parametro_s_1)
		if s_cs_xx.parametri.parametro_b_1 then
			if ib_sintexcal then
				wf_carica_lista_programmi(il_handle)
			else
				wf_carica_lista_programmi_std(il_handle)
			end if
			dw_grid.scrolltorow(ll_riga)		
		end if
			
	else
	
		choose case getitemstring(ll_riga, "flag_tipo_risorsa")
			case "I"
				
				if ib_sintexcal then
					
					if lb_rda then
						
//						ll_anno = getitemnumber(ll_riga, "anno_registrazione")
//						ll_numero = getitemnumber(ll_riga, "num_registrazione")
//						ll_prog = getitemnumber(ll_riga, "progressivo")
//						
//						select flag_rda
//						into    :ls_flag_rda
//						from	prog_manutenzioni_operai
//						where	cod_azienda = :s_cs_xx.cod_azienda and
//								anno_registrazione = :ll_anno and
//								num_registrazione = :ll_numero and
//								progressivo = :ll_prog;
//								
//						if sqlca.sqlcode = 0 and not isnull(ls_flag_rda) and ls_flag_rda = "S" then
//						else
//							g_mb.messagebox( "OMNIA", "Attenzione: la risorsa che si sta cercando di modificare non proviene da rda!", stopsign!)
//							return 
//						end if
						
					end if
					
					window_open_parm(w_programmi_manutenzione_operai, 0, dw_grid)
					setnull(s_cs_xx.parametri.parametro_s_1)
					if s_cs_xx.parametri.parametro_b_1 then
						wf_carica_lista_programmi(il_handle)	
						dw_grid.scrolltorow(ll_riga)
					end if
				else
					window_open_parm(w_prog_manutenzione_operai_std, 0, dw_grid)
					setnull(s_cs_xx.parametri.parametro_s_1)
					wf_carica_lista_programmi_std(il_handle)								
					dw_grid.scrolltorow(ll_riga)
				end if
				
				
				
			case "R"
				if ib_sintexcal then
					
					if lb_rda then
						
//						ll_anno = getitemnumber(ll_riga, "anno_registrazione")
//						ll_numero = getitemnumber(ll_riga, "num_registrazione")
//						ll_prog = getitemnumber(ll_riga, "progressivo")
//						
//						select flag_rda
//						into    :ls_flag_rda
//						from	prog_manutenzioni_ricambi
//						where	cod_azienda = :s_cs_xx.cod_azienda and
//								anno_registrazione = :ll_anno and
//								num_registrazione = :ll_numero and
//								prog_riga_ricambio = :ll_prog;
//								
//						if sqlca.sqlcode = 0 and not isnull(ls_flag_rda) and ls_flag_rda = "S" then
//						else
//							g_mb.messagebox( "OMNIA", "Attenzione: il ricambio che si sta cercando di modificare non proviene da rda!", stopsign!)
//							return 
//						end if
						
					end if					
					
					window_open_parm(w_programmi_manutenzione_ricambio, 0, dw_grid)
					setnull(s_cs_xx.parametri.parametro_s_1)
					if s_cs_xx.parametri.parametro_b_1 then
						wf_carica_lista_programmi(il_handle)		
						dw_grid.scrolltorow(ll_riga)
					end if
				else
					window_open_parm(w_prog_manutenzione_ricambio_std, 0, dw_grid)				
					setnull(s_cs_xx.parametri.parametro_s_1)
					wf_carica_lista_programmi_std(il_handle)								
					dw_grid.scrolltorow(ll_riga)
				end if
	
	
			case "E"
				if ib_sintexcal then
					window_open_parm(w_programmi_manutenzione_risorse, 0, dw_grid)
					setnull(s_cs_xx.parametri.parametro_s_1)
					if s_cs_xx.parametri.parametro_b_1 then
						wf_carica_lista_programmi(il_handle)		
						dw_grid.scrolltorow(ll_riga)
					end if	
				else
					window_open_parm(w_prog_manutenzione_risorse_std, 0, dw_grid)
					setnull(s_cs_xx.parametri.parametro_s_1)
					wf_carica_lista_programmi_std(il_handle)	
					dw_grid.scrolltorow(ll_riga)
				end if
				
		end choose
	end if
end if
end event

event ue_incolla_attrezzatura();string ls_flag_tipo_riga, ls_flag_tipo_risorsa
long ll_row, ll_ret

if il_riga_copia > 0 then
	
	// flag_tipo riga P=Primaria, C=Collegata ossia una riga di risorse, ricambi o altro
	
	ls_flag_tipo_riga = dw_grid.getitemstring(il_riga_copia, "flag_tipo_riga")
	
	if ls_flag_tipo_riga = "P" then
		
		if wf_incolla_piano_manutenzione(il_riga_copia, il_rbuttonrow,"D") = -1 then
			rollback;
			il_riga_copia = 0	
		else
			commit;
			
			if ib_sintexcal then
				wf_carica_lista_programmi(il_handle)
			else
				wf_carica_lista_programmi_std(il_handle)
			end if
			
			dw_grid.scrolltorow(il_riga_copia)
			
			il_riga_copia = 0				
			
		end if		
		
	else
		/* flag_tipo risorsa (solo nella riga collegata)
					I = risorsa interna = operaio
					R = ricambio
					E = risorsa esterna
		*/
		
		ls_flag_tipo_risorsa = dw_grid.getitemstring(il_riga_copia, "flag_tipo_risorsa")
		
		if ls_flag_tipo_risorsa = "R" then
			
			if ib_sintexcal then
				
				ll_ret = g_mb.messagebox( "OMNIA", "Continuare con la copia della riga di ricambio con suddivisione della riga d'ordine?", Exclamation!, YesNo!, 2)
				if ll_ret <> 1 then 
					il_riga_copia = 0
					return 			
				end if
				
				if wf_incolla_ricambio(il_riga_copia, il_rbuttonrow,"T") = -1 then
					rollback;
					il_riga_copia = 0
				else
					commit;
					
					wf_carica_lista_programmi(il_handle)
					dw_grid.scrolltorow(il_riga_copia)					
					il_riga_copia = 0
					
				end if
			elseif ib_grid then
				if wf_incolla_ricambio(il_riga_copia, il_rbuttonrow,"T") = -1 then
					rollback;
					il_riga_copia = 0
				else
					commit;
					
					wf_carica_lista_programmi(il_handle)
					dw_grid.scrolltorow(il_riga_copia)					
					il_riga_copia = 0					
					
				end if
			end if
		end if
	end if	
end if
end event

event ue_programma();string ls_attrezzatura, ls_cod_tipo_manut, ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore
long ll_anno, ll_numero, ll_riga

ll_riga = il_rbuttonrow

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if

ls_cod_tipo_manut = dw_grid.getitemstring( ll_riga, "cod_tipo_manutenzione")
ls_attrezzatura = dw_grid.getitemstring( ll_riga, "cod_attrezzatura")
ll_anno = dw_grid.getitemnumber( ll_riga, "anno_registrazione")
ll_numero = dw_grid.getitemnumber( ll_riga, "num_registrazione")

if not isnull(ls_cod_tipo_manut) and ls_cod_tipo_manut <> "" and not isnull(ls_attrezzatura) and ls_attrezzatura <> "" and not isnull(ll_anno) and ll_anno > 0 and not isnull(ll_numero) and ll_numero > 0 then
	
	s_cs_xx.parametri.parametro_s_1 = string(ll_anno) + "/" + string(ll_numero)
	s_cs_xx.parametri.parametro_ul_1 = ll_anno
	s_cs_xx.parametri.parametro_ul_2 = ll_numero
	s_cs_xx.parametri.parametro_s_2 = ls_attrezzatura
	s_cs_xx.parametri.parametro_s_3 = ls_cod_tipo_manut

	window_open_parm(w_elenco_attrez_semplice, 0, dw_grid)
	
end if
end event

event ue_operaio_rda();long ll_riga
string ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore

ll_riga = il_rbuttonrow

s_cs_xx.parametri.parametro_s_1 = "NRDA"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if

if ib_sintexcal then
	window_open_parm(w_programmi_manutenzione_operai, 0, dw_grid)
	if s_cs_xx.parametri.parametro_b_1 then
		wf_carica_lista_programmi(il_handle)
	end if
else
	window_open_parm(w_prog_manutenzione_operai_std, 0, dw_grid)
	wf_carica_lista_programmi_std(il_handle)
end if



dw_grid.scrolltorow(ll_riga)
end event

event ue_ricambio_rda();long ll_riga
string	ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore

s_cs_xx.parametri.parametro_s_1 = "NRDA"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid
ll_riga = il_rbuttonrow

//	***	se l'attrezzatura è bloccata devo impedire all'utente solo se non amministratore

ls_cod_attrezzatura = getitemstring( ll_riga, "cod_attrezzatura")

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	select flag_blocco
	into    :ls_flag_blocco
	from	anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
			
	if sqlca.sqlcode = 0 and not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
		
		// *** è bloccata ----> avverto l'utente
		g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
		
		select flag_collegato
		into    :ls_flag_amministratore
		from   utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			g_mb.messagebox( "OMNIA", "Operazione Bloccata!", stopsign!)
		end if
		
	end if
	
end if

if ib_sintexcal then
	window_open_parm(w_programmi_manutenzione_ricambio, 0, dw_grid)
	if s_cs_xx.parametri.parametro_b_1 then
		wf_carica_lista_programmi(il_handle)
	end if
else
	window_open_parm(w_prog_manutenzione_ricambio_std, 0, dw_grid)
	wf_carica_lista_programmi_std(il_handle)
end if

dw_grid.scrolltorow(ll_riga)
end event

event ue_sposta();long ll_riga

s_cs_xx.parametri.parametro_s_1 = "N"
s_cs_xx.parametri.parametro_d_1 = il_rbuttonrow
s_cs_xx.parametri.parametro_dw_1 = dw_grid

ll_riga = il_rbuttonrow

window_open_parm(w_programmi_manutenzione_sposta_att, 0, dw_grid)

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_d_1)

if ( s_cs_xx.parametri.parametro_b_1 ) then

	wf_carica_lista_programmi(il_handle)
	dw_grid.scrolltorow(ll_riga)
	
end if
end event

event ue_esegui();wf_esegui()
	

end event

event ue_commenti();WINDOW	l_Window

s_cs_xx.parametri.parametro_s_1 = getitemstring( il_rbuttonrow, "cod_attrezzatura")

PCCA.Error = Window_Type_Open_parm(l_Window, "w_attrezzature_commenti", -1, dw_grid)
end event

event rbuttondown;long	ll_cont, ll_anno_reg, ll_num_reg
string ls_amministratore, ls_flag_programmata, ls_flag_tipo_riga
boolean lb_utente = false, lb_programmata = false

if isvalid(dwo) then
	
	is_nome = dwo.name
	
	if ib_sintexcal then
		
		//	*** Michela 07/06/2007: se è una tipologia programmata allora posso solo fare esegui
		
		lb_programmata = false
		
		if row > 0 then					
			this.setrow(row)
			ll_anno_reg = getitemnumber(row, "anno_registrazione")
			ll_num_reg = getitemnumber(row, "num_registrazione")
			//ls_flag_tipo_riga = getitemstring(row, "flag_tipo_riga")
			
			if not isnull(ll_anno_reg) and ll_anno_reg > 0 and not isnull(ll_num_reg) and ll_num_reg > 0 then
				
				select flag_tipo_man_program
				into	:ls_flag_programmata
				from	tab_tipi_manut_scollegate
				where	cod_azienda = :s_cs_xx.cod_azienda and
						cod_tipo_manut_collegata IN ( select cod_tipo_manutenzione
																from 	programmi_manutenzione	
																where	cod_azienda = :s_cs_xx.cod_azienda and
																		anno_registrazione = 	:ll_anno_reg and
																		num_registrazione = :ll_num_reg );
																		
				if sqlca.sqlcode = 0 and not isnull(ls_flag_programmata) and ls_flag_programmata = "S" then
					lb_programmata = true
				end if
				
			end if
			
		end if					
					// si tratta di una riga con la sola attrezzatura, quindi consento solo 
					// di creare una nuova attività oppure di incollare da un'altra.
		
		m_programmi_manutenzione l_menu
		m_prog_manutenzione_pgr	  l_menu_pgr
		
		l_menu = create m_programmi_manutenzione
		
		if s_cs_xx.cod_utente <> "CS_SYSTEM" then
			
			if lb_programmata then
				l_menu_pgr = create m_prog_manutenzione_pgr																//	***	menu di esegui programmate
			else
			
				select flag_collegato
				into   :ls_amministratore
				from   utenti
				where  cod_utente = :s_cs_xx.cod_utente;
				
				if sqlca.sqlcode = 0 and ls_amministratore = "S" and not isnull(ls_amministratore) then
					l_menu  = create m_programmi_manutenzione																//	*** menu sintexcal programmazione per amministratore
				else
				
					select count(*)
					into   :ll_cont 
					from   mansionari
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_utente = :s_cs_xx.cod_utente and
							 flag_autorizza_rda = 'S';
	
					if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont > 0 then
							
						l_menu  = create m_programmi_manutenzione															//	*** menu sintexcal programmazione
					else
						m_prog_manutenzione_rda l_menu_rda	
						l_menu_rda  = create m_prog_manutenzione_rda														//	*** menu sintexcal rda
						lb_utente = true
					end if
					
				end if
				
			end if
			
		else
			l_menu  = create m_programmi_manutenzione																		//	*** menu sintexcal programmazione per cs_system	
		end if		
				
	else
		m_prog_manutenzione_std l_menu2
		l_menu2 = create m_prog_manutenzione_std																			//	*** menu standard programmazione per amministratore
	end if
	
	if ib_sintexcal then
	
		if lb_utente = false then
			
			if row > 0 then	
				
				this.setrow(row)
				
				if isnull(getitemnumber(row, "anno_registrazione")) or getitemnumber(row, "anno_registrazione") = 0 then
					
					// si tratta di una riga con la sola attrezzatura, quindi consento solo 
					// di creare una nuova attività oppure di incollare da un'altra.				
					l_menu.m_commentiattrezzatura.visible = false
					l_menu.m_aggiungi_attivita.enabled = true
					l_menu.m_agg_ricambio.enabled = false
					l_menu.m_agg_operaio.enabled = false
					l_menu.m_elimina.visible = false
					l_menu.m_separatore_1.visible = false
					l_menu.m_modifica.visible = false
					l_menu.m_separatore_2.visible = true
					l_menu.m_copia.enabled = false
					l_menu.m_incolla.enabled = false				
					l_menu.m_incollaattdestinazione.visible = false
					l_menu.m_commentiattrezzatura.visible = true
					l_menu.m_commentiattrezzatura.enabled = true
					
					if il_riga_copia > 0 then 
						l_menu.m_incolla.enabled = true
						l_menu.m_incollaattdestinazione.visible = true
					end if
				else
					
					if getitemstring(row, "flag_tipo_riga") = "P" then															//	*** riga principale di sintexcal con attività
						
						l_menu.m_commentiattrezzatura.visible = true
						l_menu.m_commentiattrezzatura.enabled = true						
						//	*** in questo caso mi trovo in una riga con una attività, quindi se il tipo di manutenzione è programmato devo far vedere il tipo programmato
						
						if lb_programmata then
							
							l_menu.m_commentiattrezzatura.visible = true
							l_menu.m_elimina.visible = false
							l_menu.m_separatore_1.visible = false
							l_menu.m_modifica.visible = false
							l_menu.m_separatore_2.visible = false
							l_menu.m_copia.visible = false
							l_menu.m_incolla.visible = false
							l_menu.m_incollaattdestinazione.visible = false
							l_menu.m_incolla.visible = false
							l_menu.m_incollaattdestinazione.visible = false
							l_menu.m_sposta.visible = false
							l_menu.m_sposta.visible = false
							l_menu.m_aggiungi_attivita.visible = false
							l_menu.m_agg_ricambio.visible = false
							l_menu.m_agg_operaio.visible = false
							
							l_menu.idw_datawindow = this
							il_rbuttonrow = row
								
							l_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())				
							
							return 0
							
						else
							
							//l_menu.m_commentiattrezzatura.visible = false
							l_menu.m_elimina.visible = true
							l_menu.m_separatore_1.visible = true
							l_menu.m_modifica.visible = true
							l_menu.m_separatore_2.visible = false
							l_menu.m_copia.enabled = true
							l_menu.m_incolla.enabled = false
							l_menu.m_incollaattdestinazione.visible = false
							
							if il_riga_copia > 0 then 
								l_menu.m_incolla.enabled = true	
								l_menu.m_incollaattdestinazione.visible = true
							end if
							
							l_menu.m_sposta.visible = true
							l_menu.m_sposta.enabled = true
							
						end if
						
					else																													// *** riga di risorsa collegata all'attività (ricambio o risorsa interna)
						
						if lb_programmata then							
							l_menu_pgr.m_esegui.visible = true
							l_menu_pgr.m_esegui.enabled = false
							l_menu_pgr.m_modifica.visible = true
							l_menu_pgr.m_modifica.enabled = true
							l_menu_pgr.idw_datawindow = this
							il_rbuttonrow = row								
							//l_menu_pgr.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())											
							return 0							
						end if
						l_menu.m_commentiattrezzatura.visible = false
						l_menu.m_elimina.visible = true
						l_menu.m_separatore_1.visible = true
						l_menu.m_modifica.visible = true
						l_menu.m_separatore_2.visible = true
						l_menu.m_copia.enabled = false
						l_menu.m_incolla.enabled = false
						l_menu.m_incollaattdestinazione.enabled = false		
						l_menu.m_sposta.visible = false
						l_menu.m_sposta.enabled = false						
						
						if il_riga_copia > 0 then 
							l_menu.m_incolla.enabled = true	
							l_menu.m_incollaattdestinazione.enabled = true
						end if
						
						if getitemstring( row, "flag_tipo_riga") = "C" and getitemstring( row, "flag_tipo_risorsa") = "R" then			// *** ricambio
							
							long	 ll_anno, ll_numero, ll_prog
							string ls_tkordi, ls_tkboll, ls_flag_amministratore
							
							ll_anno = getitemnumber(row, "anno_registrazione") 
							ll_numero = getitemnumber(row, "num_registrazione")
							ll_prog = getitemnumber( row, "progressivo")
							
							// *** conto le righe di dettaglio
							select count(*)
							into   :ll_cont
							from   prog_manutenzioni_ric_ddi
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 anno_registrazione = :ll_anno and
									 num_registrazione = :ll_numero and
									 prog_riga_ricambio = :ll_prog;
									 
							if sqlca.sqlcode <> 0 then
								g_mb.messagebox( "OMNIA", "Errore durante ricerca dettagli ddi ricambio:" + sqlca.sqlerrtext)
								return -1
							end if
							
							if isnull(ll_cont) then ll_cont = 0
							
							select tkordi
							into   :ls_tkordi
							from   prog_manutenzioni_ricambi
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 anno_registrazione = :ll_anno and
									 num_registrazione = :ll_numero and
									 prog_riga_ricambio = :ll_prog;
									 
							if sqlca.sqlcode = 0 and ( ( not isnull(ls_tkordi) and ls_tkordi <> "" ) or ( not isnull(ll_cont) and ll_cont > 0 ) ) then
								
								select flag_collegato
								into   :ls_flag_amministratore
								from   utenti
								where  cod_utente = :s_cs_xx.cod_utente;
								
								if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then								
									l_menu.m_elimina.enabled = true
									l_menu.m_modifica.enabled = true								
								else							
									l_menu.m_elimina.enabled = false
									l_menu.m_modifica.enabled = true
								end if
								
								if il_riga_copia > 0 then								
									l_menu.m_incollaattdestinazione.visible = true
									l_menu.m_incollaattdestinazione.enabled = true								
								end if							
								
							end if
								
							l_menu.m_copia.enabled = true
						
						end if			
						
					end if
				end if
				
			else
				l_menu.m_commentiattrezzatura.visible = false
				l_menu.m_aggiungi_attivita.enabled = false
				//l_menu.m_agg_fornitore.enabled = false
				l_menu.m_agg_ricambio.enabled = false
				l_menu.m_agg_operaio.enabled = false
				l_menu.m_elimina.visible = false
				l_menu.m_separatore_1.visible = false
				l_menu.m_modifica.visible = false
				l_menu.m_separatore_2.visible = true
				l_menu.m_copia.enabled = true
				l_menu.m_incolla.enabled = false		
				if ib_sintexcal then
					l_menu.m_incollaattdestinazione.visible = true
					l_menu.m_incollaattdestinazione.enabled = false		
				else
					l_menu.m_incollaattdestinazione.visible = false
					l_menu.m_incollaattdestinazione.enabled = false					
				end if
				
			end if
				
			l_menu.idw_datawindow = this
			il_rbuttonrow = row
				
			l_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
			
		else
			if row > 0 then	
				
				this.setrow(row)
				
				if isnull(getitemnumber(row, "anno_registrazione")) or getitemnumber(row, "anno_registrazione") = 0 then
					
					// si tratta di una riga con la sola attrezzatura, quindi consento solo 
					// di creare una nuova attività oppure di incollare da un'altra.
					
					l_menu_rda.m_aggiungi_attivita.enabled = true
					l_menu_rda.m_agg_ricambio.enabled = false
					l_menu_rda.m_agg_operaio.enabled = false
					l_menu_rda.m_modifica.enabled = false
					
				else
					
					if getitemstring(row, "flag_tipo_riga") = "P" then															//	*** riga principale di sintexcal con attività
						
						//	*** in questo caso mi trovo in una riga con una attività, quindi se il tipo di manutenzione è programmato devo far vedere il tipo programmato
						
						if lb_programmata then
							
							l_menu_pgr.m_esegui.visible = true
							l_menu_pgr.m_esegui.enabled = true
							l_menu_pgr.m_modifica.visible = true
							l_menu_pgr.m_modifica.enabled = false
							l_menu_pgr.idw_datawindow = this
							il_rbuttonrow = row
								
							//l_menu_pgr.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())				
							
							return 0
							
						end if
					
					end if
					
				end if
				
			else
				
				l_menu_rda.m_aggiungi_attivita.enabled = false
				l_menu_rda.m_agg_ricambio.enabled = false
				l_menu_rda.m_agg_operaio.enabled = false
				l_menu_rda.m_modifica.enabled = false
				
			end if
				
			l_menu_rda.idw_datawindow = this
			il_rbuttonrow = row
				
			l_menu_rda.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())			
		end if
		
	else
		
		// **********************			PARTE STANDARD					*********************************
		// **********************												*********************************
		
		if row > 0 then	
			this.setrow(row)
			
			if isnull(getitemnumber(row, "anno_registrazione")) or getitemnumber(row, "anno_registrazione") = 0 then
				
				// si tratta di una riga con la sola attrezzatura, quindi consento solo 
				// di creare una nuova attività oppure di incollare da un'altra.
				
				l_menu2.m_aggiungi_attivita.enabled = true
				l_menu2.m_agg_fornitore.enabled = false
				l_menu2.m_agg_ricambio.enabled = false
				l_menu2.m_agg_operaio.enabled = false
				l_menu2.m_elimina.visible = false
				l_menu2.m_separatore_1.visible = false
				l_menu2.m_modifica.visible = false
				l_menu2.m_separatore_2.visible = true
				l_menu2.m_copia.enabled = false
				l_menu2.m_incolla.enabled = false
				l_menu2.m_programma.visible = false
				l_menu2.m_programma.enabled = false
				
				if il_riga_copia > 0 then 
					l_menu2.m_incolla.enabled = true
				end if
				
			else
				if getitemstring(row, "flag_tipo_riga") = "P" then
					l_menu2.m_elimina.visible = true
					l_menu2.m_separatore_1.visible = true
					l_menu2.m_modifica.visible = true
					l_menu2.m_separatore_2.visible = false
					l_menu2.m_copia.enabled = true
					l_menu2.m_incolla.enabled = false
					
					if il_riga_copia > 0 then 
						l_menu2.m_incolla.enabled = true						
					end if
					
					l_menu2.m_programma.visible = TRUE
					l_menu2.m_programma.enabled = TRUE					
					
									
				else
					
					l_menu2.m_elimina.visible = true
					l_menu2.m_separatore_1.visible = true
					l_menu2.m_modifica.visible = true
					l_menu2.m_separatore_2.visible = true
					l_menu2.m_copia.enabled = false
					l_menu2.m_incolla.enabled = false
					
					if il_riga_copia > 0 then 
						l_menu2.m_incolla.enabled = true				
					end if
					
					if getitemstring( row, "flag_tipo_riga") = "C" and getitemstring( row, "flag_tipo_risorsa") = "R" then
						
						l_menu2.m_copia.enabled = true
						
					end if		
					
					l_menu2.m_programma.visible = TRUE
					l_menu2.m_programma.enabled = TRUE
					
				end if
			end if
			
		else
			
			l_menu2.m_aggiungi_attivita.enabled = false
			l_menu2.m_agg_fornitore.enabled = false
			l_menu2.m_agg_ricambio.enabled = false
			l_menu2.m_agg_operaio.enabled = false
			l_menu2.m_elimina.visible = false
			l_menu2.m_separatore_1.visible = false
			l_menu2.m_modifica.visible = false
			l_menu2.m_separatore_2.visible = true
			l_menu2.m_copia.enabled = true
			l_menu2.m_incolla.enabled = false	
			
			l_menu2.m_programma.visible = false
			l_menu2.m_programma.enabled = false						
			
		end if
			
		l_menu2.idw_datawindow = this
		il_rbuttonrow = row
			
		l_menu2.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())		
		
	end if
		
end if
end event

type dw_programmi_manutenzione from uo_cs_xx_dw within w_programmi_manutenzione
integer x = 1280
integer y = 140
integer width = 2747
integer height = 1988
integer taborder = 20
string dataobject = "d_programmi_manutenzione_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;string   ls_cod_tipo_manutenzione, ls_flag_manutenzione, ls_flag_ricambio_codificato, ls_note, &
         ls_cod_cat_risorse_esterne, ls_cod_cat_attrezzature, &
			ls_cod_ricambio, ls_des_ricambio, ls_cod_attrezzatura, ls_testo, ls_cod_ricambio_alternativo, ls_null, &
			ls_cod_tipo_lista_dist, ls_cod_lista_dist

datetime ldt_tempo_previsto

long     ll_num_reg_lista, ll_riga, ll_i, ll_y, ll_ore, ll_minuti

double   ld_tariffa_std, ld_diritto_chiamata

setnull(ls_null)

choose case i_colname
	case "cod_attrezzatura"
		
		f_PO_LoadDDDW_DW( dw_programmi_manutenzione, &
		                  "cod_tipo_manutenzione", &
								sqlca, &
							   "tab_tipi_manutenzione", &
								"cod_tipo_manutenzione", &
								"des_tipo_manutenzione", &
							   "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + i_coltext + "'")
								
		setitem(getrow(),"cod_tipo_manutenzione",ls_null)	
		
	case "cod_tipo_manutenzione"
		
		ls_cod_attrezzatura = getitemstring( getrow(), "cod_attrezzatura" )
		
		select tab_tipi_manutenzione.tempo_previsto,   
				 tab_tipi_manutenzione.flag_manutenzione,   
				 tab_tipi_manutenzione.flag_ricambio_codificato,   
				 tab_tipi_manutenzione.cod_ricambio,   
				 tab_tipi_manutenzione.cod_ricambio_alternativo,   
				 tab_tipi_manutenzione.des_ricambio_non_codificato,   
				 tab_tipi_manutenzione.num_reg_lista,
				 tab_tipi_manutenzione.modalita_esecuzione,
				 tab_tipi_manutenzione.cod_tipo_lista_dist,
				 tab_tipi_manutenzione.cod_lista_dist
		 into  :ldt_tempo_previsto,   
				 :ls_flag_manutenzione,   
				 :ls_flag_ricambio_codificato,   
				 :ls_cod_ricambio,
				 :ls_cod_ricambio_alternativo,   
				 :ls_des_ricambio,   
				 :ll_num_reg_lista,
				 :ls_note,
				 :ls_cod_tipo_lista_dist,
				 :ls_cod_lista_dist
		 from  tab_tipi_manutenzione  
		 where tab_tipi_manutenzione.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_manutenzione.cod_attrezzatura = :ls_cod_attrezzatura and
				 tab_tipi_manutenzione.cod_tipo_manutenzione = :i_coltext ;
				 
		if sqlca.sqlcode = 100 then
			
			g_mb.messagebox("Dettaglio Manutenzione","Impossibile trovare tipo manutenzione: " + i_coltext + "per l'attrezzatura " + ls_cod_attrezzatura, StopSign!)
			
			return
			
		end if
		
		setitem(getrow(), "note_idl", ls_note)
		
		ll_ore = hour(time(ldt_tempo_previsto))
		
		ll_minuti = minute(time(ldt_tempo_previsto))
		
		setitem(getrow(), "operaio_ore_previste", ll_ore)
		
		setitem(getrow(), "operaio_minuti_previsti", ll_minuti)
		
		setitem(getrow(), "flag_tipo_intervento", ls_flag_manutenzione)
		
		setitem(getrow(), "flag_ricambio_codificato", ls_flag_ricambio_codificato)
		
		setitem( getrow(), "cod_tipo_lista_dist", ls_cod_tipo_lista_dist)
		
		setitem( getrow(), "cod_lista_dist", ls_cod_lista_dist)
		
		if ls_flag_ricambio_codificato = "S" then
			
			setitem(getrow(), "cod_ricambio", ls_cod_ricambio)
			
			setitem(getrow(), "cod_ricambio_alternativo", ls_cod_ricambio_alternativo)
			
			if not isnull(ls_cod_ricambio) then setitem(getrow(), "quan_ricambio", 1)
			
		else
			
			setitem(getrow(), "ricambio_non_codificato", ls_des_ricambio)
			
		end if
		
		setitem(getrow(), "num_reg_lista", ll_num_reg_lista)
		
		setitem(getrow(),"costo_unitario_ricambio", 0 )
		
		select prezzo_acquisto		
		into   :ld_tariffa_std
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_ricambio;
				 
		if sqlca.sqlcode = 0 then
			
			setitem(getrow(),"costo_unitario_ricambio", ld_tariffa_std)
			
		end if
		
		wf_flag_tipo_intervento( ls_flag_manutenzione, ls_flag_ricambio_codificato)
		
	case "flag_tipo_intervento"
		
		wf_flag_tipo_intervento(i_coltext, getitemstring(getrow(),"flag_ricambio_codificato"))
				
	case "cod_tipo_lista_dist"
		
		setitem( getrow(), "cod_lista_dist", ls_null)
		
		f_po_loaddddw_dw( dw_programmi_manutenzione, "cod_lista_dist", sqlca, &
								"tes_liste_dist", &
								"cod_lista_dist", &
								"des_lista_dist", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + i_coltext + "' ")
		
end choose
   

end event

event rowfocuschanged;call super::rowfocuschanged;long   ll_riga
string ls_cod_attrezzatura, ls_cod_tipo_lista_dist, ls_cod_cat_risorse_esterne, ls_null

setnull(ls_cod_attrezzatura)

ll_riga = this.getrow()

if not isnull(ll_riga) and ll_riga > 0 then
	
	if not isnull(dw_programmi_manutenzione.getitemstring(ll_riga, "cod_attrezzatura")) then
		
		ls_cod_attrezzatura = dw_programmi_manutenzione.getitemstring(ll_riga, "cod_attrezzatura")
		
	end if	
	
end if

if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura <> "" then
	
	f_PO_LoadDDDW_DW(dw_programmi_manutenzione,"cod_tipo_manutenzione",sqlca,&
						  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
						  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_cod_attrezzatura + "'")
						  
end if					

if ll_riga > 0 then
	
	if not isnull(dw_programmi_manutenzione.getitemstring(ll_riga, "cod_tipo_lista_dist")) then
		
		ls_cod_tipo_lista_dist = dw_programmi_manutenzione.getitemstring( ll_riga, "cod_tipo_lista_dist")
	
		if not isnull(ls_cod_tipo_lista_dist) and ls_cod_tipo_lista_dist <> "" then
			
			f_po_loaddddw_dw( dw_programmi_manutenzione, "cod_lista_dist", sqlca, &
									"tes_liste_dist", &
									"cod_lista_dist", &
									"des_lista_dist", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + ls_cod_tipo_lista_dist + "' ")
		end if
		
	end if
	
	if not isnull(dw_programmi_manutenzione.getitemstring(ll_riga, "cod_cat_risorse_esterne")) then
		
		ls_cod_cat_risorse_esterne = dw_programmi_manutenzione.getitemstring( ll_riga, "cod_cat_risorse_esterne")
		
		if not isnull(ls_cod_cat_risorse_esterne) and ls_cod_cat_risorse_esterne <> "" then
			
			f_PO_LoadDDDW_DW( dw_programmi_manutenzione_3, &
									"cod_risorsa_esterna", &
									sqlca, &		
									"anag_risorse_esterne", &
									"cod_risorsa_esterna", &
									"descrizione", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne + "' and not (flag_blocco ='S' and data_blocco<='"+string(today(),s_cs_xx.db_funzioni.formato_data)+"')")
		end if		
				
	end if

	wf_flag_tipo_intervento(dw_programmi_manutenzione.getitemstring(1,"flag_tipo_intervento"), dw_programmi_manutenzione.getitemstring(1,"flag_ricambio_codificato"))

end if
end event

event updateend;call super::updateend;string ls_flag_scadenze_old, ls_flag_scadenze, ls_messaggio
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i_report, ll_frequenza, ll_prog_uso_attrezzatura
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cod_operaio, ls_des_attrezzatura, ls_des_tipo_manutenzione, ls_des_operaio, &
		 ls_flag_tipo_tempo, ls_periodicita, ls_flag_tipo_intervento
datetime ldt_data_registrazione, ldt_data_restituzione, ldt_data_max, ldt_data_comodo
decimal ld_contatore, ld_somma_contatore, ll_tot_ore

if ib_stato_nuovo then
	ls_flag_scadenze = getitemstring(getrow(),"flag_scadenze")
	if ls_flag_scadenze = "S" then
		if iuo_manutenzioni.uof_crea_manutenzione_programmata(getitemnumber(getrow(),"anno_registrazione"), getitemnumber(getrow(),"num_registrazione"), ls_messaggio) <> 0 then
			g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
		else
			wf_imposta_tv()
		end if	
//-------------------------------------------- Modifica Nicola -----------------------------------------------------
	else
		ll_tot_ore = 0		
		ll_anno_registrazione = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "num_registrazione")
		ls_cod_attrezzatura = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_attrezzatura")
		ls_cod_tipo_manutenzione = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_tipo_manutenzione")
		ls_cod_operaio = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_operaio")
		ls_periodicita = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "periodicita")
		ll_frequenza = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "frequenza")
		ls_flag_tipo_intervento = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "flag_tipo_intervento")
		
		if (ls_periodicita = 'M' or ls_periodicita = 'O') then
			
			if ls_periodicita = 'O' then
				ll_tot_ore = ll_frequenza
			elseif ls_periodicita = 'M' then 
				ll_tot_ore = ll_frequenza / 60
			end if		
			
			select max(data_registrazione)
			  into :ldt_data_registrazione
			  from manutenzioni
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_attrezzatura = :ls_cod_attrezzatura
				and flag_eseguito = 'S'
				and flag_ordinario = 'S';
			if sqlca.sqlcode < 0 then 	
				g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella MANUTENZIONI")
				return
			end if
			if isnull(ldt_data_registrazione) then ldt_data_registrazione = datetime(Date("1900/01/01"))
			ld_somma_contatore = 0
			ldt_data_max = datetime(Date("2999/12/31"))
			do while 1 = 1	
		
				 select max(data_restituzione), 
						  max(prog_uso_attrezzatura)
					into :ldt_data_comodo,
						  :ll_prog_uso_attrezzatura
					from registro_uso_attrezzature	  
				  where cod_azienda = :s_cs_xx.cod_azienda
					 and cod_attrezzatura = :ls_cod_attrezzatura
					 and data_restituzione > :ldt_data_registrazione 
					 and data_restituzione < :ldt_data_max;
					 
				if sqlca.sqlcode = 100 then exit 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in lettura data_registrazione max dalla tabella REGISTRO_USO_MANUTENZIONI")
					return
				end if
		
				 select flag_tipo_tempo, 
						  contatore			 
					into :ls_flag_tipo_tempo,
						  :ld_contatore		
					from registro_uso_attrezzature
				  where cod_azienda = :s_cs_xx.cod_azienda
					 and cod_attrezzatura = :ls_cod_attrezzatura
					 and prog_uso_attrezzatura = :ll_prog_uso_attrezzatura;
		
				if sqlca.sqlcode = 100 then exit 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella REGISTRO_USO_MANUTENZIONI")
					return
				end if
				
				if sqlca.sqlcode = 0 then
					ldt_data_max = ldt_data_comodo
					if ls_flag_tipo_tempo = "S" then
						ld_somma_contatore = ld_somma_contatore + ld_contatore
					elseif ls_flag_tipo_tempo = "N" then
						ld_somma_contatore = ld_somma_contatore + ld_contatore
						exit
					end if
				end if
			loop
		
			if ll_tot_ore < ld_somma_contatore then
				if iuo_manutenzioni.uof_crea_manutenzione_programmata(getitemnumber(getrow(),"anno_registrazione"), getitemnumber(getrow(),"num_registrazione"), ls_messaggio) <> 0 then
					g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
				else
					wf_imposta_tv()
				end if	
			end if
		end if	
//--------------------------------------------- Fine Modifica ------------------------------------------------------
	end if
end if

if ib_stato_modifica then
	ls_flag_scadenze_old = getitemstring(getrow(),"flag_scadenze",Primary!,true)
	ls_flag_scadenze = getitemstring(getrow(),"flag_scadenze")
	if ls_flag_scadenze_old <> ls_flag_scadenze and ls_flag_scadenze = "S" then
		if iuo_manutenzioni.uof_crea_manutenzione_programmata(getitemnumber(getrow(),"anno_registrazione"), getitemnumber(getrow(),"num_registrazione"), ls_messaggio) <> 0 then
			g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
		else
			wf_imposta_tv()
		end if
//-------------------------------------------- Modifica Nicola -----------------------------------------------------
	else
		ll_tot_ore = 0
		ll_anno_registrazione = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "num_registrazione")
		ls_cod_attrezzatura = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_attrezzatura")
		ls_cod_tipo_manutenzione = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_tipo_manutenzione")
		ls_cod_operaio = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "cod_operaio")
		ls_periodicita = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "periodicita")
		ll_frequenza = dw_programmi_manutenzione.getitemnumber(dw_programmi_manutenzione.getrow(), "frequenza")
		ls_flag_tipo_intervento = dw_programmi_manutenzione.getitemstring(dw_programmi_manutenzione.getrow(), "flag_tipo_intervento")
		
		if (ls_periodicita = 'M' or ls_periodicita = 'O') then
			
			if ls_periodicita = 'O' then
				ll_tot_ore = ll_frequenza
			elseif ls_periodicita = 'M' then 
				ll_tot_ore = ll_frequenza / 60
			end if		
			
			select max(data_registrazione)
			  into :ldt_data_registrazione
			  from manutenzioni
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_attrezzatura = :ls_cod_attrezzatura
				and flag_eseguito = 'S'
				and flag_ordinario = 'S';
			if sqlca.sqlcode < 0 then 	
				g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella MANUTENZIONI")
				return
			end if
			if isnull(ldt_data_registrazione) then ldt_data_registrazione = datetime(Date("1900/01/01"))
			ld_somma_contatore = 0
			ldt_data_max = datetime(Date("2999/12/31"))
			do while 1 = 1	
		
				 select max(data_restituzione), 
						  max(prog_uso_attrezzatura)
					into :ldt_data_comodo,
						  :ll_prog_uso_attrezzatura
					from registro_uso_attrezzature	  
				  where cod_azienda = :s_cs_xx.cod_azienda
					 and cod_attrezzatura = :ls_cod_attrezzatura
					 and data_restituzione > :ldt_data_registrazione 
					 and data_restituzione < :ldt_data_max;
					 
				if sqlca.sqlcode = 100 then exit 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in lettura data_registrazione max dalla tabella REGISTRO_USO_MANUTENZIONI")
					return
				end if
		
				 select flag_tipo_tempo, 
						  contatore			 
					into :ls_flag_tipo_tempo,
						  :ld_contatore		
					from registro_uso_attrezzature
				  where cod_azienda = :s_cs_xx.cod_azienda
					 and cod_attrezzatura = :ls_cod_attrezzatura
					 and prog_uso_attrezzatura = :ll_prog_uso_attrezzatura;
		
				if sqlca.sqlcode = 100 then exit 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella REGISTRO_USO_MANUTENZIONI")
					return
				end if
				
				if sqlca.sqlcode = 0 then
					ldt_data_max = ldt_data_comodo
					if ls_flag_tipo_tempo = "S" then
						ld_somma_contatore = ld_somma_contatore + ld_contatore
					elseif ls_flag_tipo_tempo = "N" then
						ld_somma_contatore = ld_somma_contatore + ld_contatore
						exit
					end if
				end if
			loop
		
			if ll_tot_ore < ld_somma_contatore then
				if iuo_manutenzioni.uof_crea_manutenzione_programmata(getitemnumber(getrow(),"anno_registrazione"), getitemnumber(getrow(),"num_registrazione"), ls_messaggio) <> 0 then
					g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
				else
					wf_imposta_tv()
				end if	
			end if
		end if	
//--------------------------------------------- Fine Modifica ------------------------------------------------------		
	end if	
end if


end event

event pcd_delete;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------

IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

//--------------------------------- Modifica Nicola --------------------------------------------
long ll_count, ld_num_protocollo

if i_extendmode then
	
	cb_controllo.enabled = false
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=false
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=false
	dw_programmi_manutenzione.object.b_ricerca_att.enabled = false
	
	cb_ricambi.enabled = false
	cb_programmazione.enabled = false
	cb_risorse.enabled = false
	cb_documento.enabled = false
end if

declare cu_det_manutenzioni cursor for
	select num_protocollo
	  from det_prog_manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :il_anno_registrazione
		and num_registrazione = :il_num_registrazione;

open cu_det_manutenzioni;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Programmi Manutenzioni (cursore cu_det_manutenzioni)" + sqlca.sqlerrtext)
	return
end if	

do while 1 = 1
	fetch cu_det_manutenzioni into :ld_num_protocollo;
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Programmi Manutenzioni " + sqlca.sqlerrtext)
		return
	end if	
	
	if sqlca.sqlcode = 0 then
		delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
		where cod_azienda = :s_cs_xx.cod_azienda
		  and num_protocollo = :ld_num_protocollo;
		  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if		  

		delete from det_prog_manutenzioni  //cancellazione dettaglio programmi manutenzioni
			where cod_azienda = :s_cs_xx.cod_azienda
			  and anno_registrazione = :il_anno_registrazione
			  and num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione dalla tabella DET_PROG_MANUTENZIONI:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if	  		  
		  
		delete from tab_protocolli  //cancellazione protocollo
		where cod_azienda = :s_cs_xx.cod_azienda
		  and num_protocollo = :ld_num_protocollo;
		  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if
	end if
loop	
close cu_det_manutenzioni;

//---------------------------------- Fine Modifica ---------------------------------------------	

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], &
               c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse

parent.postevent("pc_save")
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	cb_controllo.enabled = false
	dw_programmi_manutenzione.object.b_ricerca_att.enabled = true
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=true
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=true
	cb_note_1.enabled = false
	cb_ricambi.enabled = false
	cb_programmazione.enabled = false
	cb_risorse.enabled = false
	cb_documento.enabled = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	
	cb_controllo.enabled = false
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=true
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=true
	dw_programmi_manutenzione.object.b_ricerca_att.enabled = true
	cb_note_1.enabled = false
	
	cb_ricambi.enabled = false
	cb_programmazione.enabled = false
	cb_risorse.enabled = false
	cb_documento.enabled = false
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_ret
ws_record lstr_record

ll_ret = tv_1.getitem(il_handle, tvi_campo)

if ll_ret < 0 then return

lstr_record = tvi_campo.data

if not(lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0) then
	setnull(lstr_record.anno_registrazione)
	setnull(lstr_record.num_registrazione)
end if
l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
if l_Error < 0 then
	PCCA.Error = c_Fatal
else
	if l_error = 1 then wf_flag_tipo_intervento(dw_programmi_manutenzione.getitemstring(1,"flag_tipo_intervento"), dw_programmi_manutenzione_3.getitemstring(1,"flag_ricambio_codificato"))
end if
	
il_anno_registrazione = lstr_record.anno_registrazione
il_num_registrazione = lstr_record.num_registrazione
	

end event

event pcd_save;call super::pcd_save;dw_programmi_manutenzione.object.b_ricerca_att.enabled = true
dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=false
dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=false
cb_note_1.enabled = true

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   if this.getitemnumber(l_Idx, "anno_registrazione") = 0 or &
      isnull(this.getitemnumber(l_Idx, "anno_registrazione")) then

		ll_anno_registrazione = f_anno_esercizio()
      if ll_anno_registrazione > 0 then
         this.setitem(l_Idx, "anno_registrazione", int(ll_anno_registrazione))
      else
			g_mb.messagebox("Programmi Manutenzione","Impostare l'anno di esercizio in parametri aziendali")
		end if
		
		ll_max_registrazione = 0
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   programmi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if isnull(ll_max_registrazione) then
			ll_max_registrazione=1
		else
			ll_max_registrazione = ll_max_registrazione + 1
		end if
		
      this.setitem(l_Idx, "num_registrazione", ll_max_registrazione)
    end if	
NEXT

end event

event pcd_view;call super::pcd_view;if i_extendmode then
	
	cb_controllo.enabled = true
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio.enabled=false
	dw_programmi_manutenzione_3.object.b_ricerca_ricambio_alt.enabled=false
	dw_programmi_manutenzione.object.b_ricerca_att.enabled = false
	
	cb_note_1.enabled = true
	cb_ricambi.enabled = true
	cb_programmazione.enabled = true
	cb_risorse.enabled = true
	cb_documento.enabled = true
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_programmi_manutenzione,"cod_attrezzatura")
end choose
end event

type dw_programmi_manutenzione_2 from uo_cs_xx_dw within w_programmi_manutenzione
event ue_lista ( )
integer x = 1298
integer y = 144
integer width = 2743
integer height = 1980
integer taborder = 10
string dataobject = "d_programmi_manutenzione_2"
boolean border = false
end type

event ue_lista();long   ll_num_versione, ll_prog_liste_con_comp, ll_num_registrazione, ll_num_edizione, ll_num_reg_lista_comp, &
       ll_prog_riga_manutenzione, ll_anno_registrazione, ll_rows[]
   
if dw_programmi_manutenzione.rowcount() = 1 then
	ll_num_reg_lista_comp = this.getitemnumber(1,"num_reg_lista") 
	ll_prog_liste_con_comp = this.getitemnumber(1,"prog_liste_con_comp") 
	if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then 
		g_mb.messagebox("Liste Controllo","Indicare una lista di controllo valida",StopSign!)
		return
	end if
	
	if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
		this.change_dw_current()
		this.change_dw_focus(dw_programmi_manutenzione_2)

		f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)
	
		ll_anno_registrazione = this.getitemnumber(1, "anno_registrazione") 
		ll_num_registrazione  = this.getitemnumber(1, "num_registrazione") 
	
		update programmi_manutenzione
		set    num_versione        = :ll_num_versione,
				 num_edizione        = :ll_num_edizione,
				 num_reg_lista       = :ll_num_reg_lista_comp,
				 prog_liste_con_comp = :ll_prog_liste_con_comp
		where  cod_azienda         = :s_cs_xx.cod_azienda and
				 anno_registrazione  = :ll_anno_registrazione and 
				 num_registrazione   = :ll_num_registrazione ;
		if sqlca.sqlcode = 0 then
			commit;
		else
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
						  exclamation!, ok!)
			return
		end if
	
		this.triggerevent("pcd_retrieve")
		ll_rows[1] = 1
		dw_programmi_manutenzione.set_selected_rows(1, &
																  ll_rows[], &
																  c_ignorechanges, &
																  c_refreshchildren, &
																  c_refreshsame)
	end if
	
	window_open_parm(w_det_liste_con_comp, 0, dw_programmi_manutenzione)
end if
end event

event itemchanged;call super::itemchanged;long ll_edizione, ll_versione, ll_lista, ll_prog_lista


if i_colname <> "num_reg_lista" then
	return
end if

ll_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")

if not isnull(ll_prog_lista) then
	g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
	return 1
end if	

ll_lista = long(i_coltext)

select max(num_edizione)
into   :ll_edizione
from   tes_liste_controllo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 num_reg_lista = :ll_lista and
		 approvato_da is not null;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
	return -1
end if

select max(num_versione)
into   :ll_versione
from   tes_liste_controllo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 num_reg_lista = :ll_lista and
		 num_edizione = :ll_edizione and
		 approvato_da is not null;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
	return -1
end if

setnull(ll_prog_lista)

setitem(getrow(),"num_edizione",ll_edizione)
setitem(getrow(),"num_versione",ll_versione)
setitem(getrow(),"prog_liste_con_comp",ll_prog_lista)
end event

event pcd_retrieve;call super::pcd_retrieve;//LONG  l_Error
//ws_record lstr_record
//
//tv_1.getitem(il_handle, tvi_campo)
//lstr_record = tvi_campo.data
//
//if lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0 then
//	l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
//	if l_Error < 0 then
//		PCCA.Error = c_Fatal
//	end if
//end if	
	
//if not(lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0) then
//	setnull(lstr_record.anno_registrazione)
//	setnull(lstr_record.num_registrazione)
//end if
//l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
//if l_Error < 0 then
//	PCCA.Error = c_Fatal
//else
//	if l_error = 1 then wf_flag_tipo_intervento(dw_programmi_manutenzione.getitemstring(1,"flag_tipo_intervento"), dw_programmi_manutenzione.getitemstring(1,"flag_ricambio_codificato"))
//end if
end event

type dw_folder from u_folder within w_programmi_manutenzione
integer x = 1275
integer y = 20
integer width = 2793
integer height = 2412
integer taborder = 10
end type

