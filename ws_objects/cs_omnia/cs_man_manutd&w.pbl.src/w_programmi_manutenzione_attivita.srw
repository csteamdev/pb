﻿$PBExportHeader$w_programmi_manutenzione_attivita.srw
$PBExportComments$Finestra Manutenzioni personalizzata Guerrato
forward
global type w_programmi_manutenzione_attivita from w_cs_xx_risposta
end type
type cb_2 from commandbutton within w_programmi_manutenzione_attivita
end type
type cb_salva from commandbutton within w_programmi_manutenzione_attivita
end type
type dw_attivita from uo_cs_xx_dw within w_programmi_manutenzione_attivita
end type
end forward

global type w_programmi_manutenzione_attivita from w_cs_xx_risposta
integer width = 2990
integer height = 1504
string title = "Attività Manutentiva"
cb_2 cb_2
cb_salva cb_salva
dw_attivita dw_attivita
end type
global w_programmi_manutenzione_attivita w_programmi_manutenzione_attivita

type variables
boolean ib_nuovo
long il_rbuttonrow
datawindow idw_parent
end variables

forward prototypes
public subroutine wf_controlla_risorse ()
public function boolean wf_operatore_rda ()
public function string wf_controllo_anno (string fs_flag_budget, ref boolean fb_blocco)
public function long wf_carica_tipologia_manutenzione (string fs_flag_budget, long fl_anno_riferimento)
end prototypes

public subroutine wf_controlla_risorse ();long	   ll_anno, ll_numero, ll_cont
string	ls_aperti, ls_cod_stato

if ib_nuovo then return

ls_aperti = ""

declare cu_stati cursor for
select  cod_stato
from 	  tab_stato_programmi
where   flag_tipo_stato = 'N';

open cu_stati;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore degli stati:" + sqlca.sqlerrtext)
	return
end if

do while true
	fetch cu_stati into :ls_cod_stato;
							  
	if sqlca.sqlcode = 100 then exit;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante le fetch del cursore degli stati:" + sqlca.sqlerrtext)
		close cu_stati;
		rollback;
		return 
	end if
	
	ls_aperti += ls_cod_stato + ","
	
loop

close cu_stati;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore degli stati:" + sqlca.sqlerrtext)
	rollback;
	return
end if

if right( ls_aperti, 1) = "," then ls_aperti = mid( ls_aperti, 1, len(ls_aperti) - 1)

ll_anno = idw_parent.getitemnumber( il_rbuttonrow, "anno_registrazione")
ll_numero = idw_parent.getitemnumber( il_rbuttonrow, "num_registrazione")
			
ll_cont = 0

select count(*)
into   :ll_cont
from   prog_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_numero and
		 ( cod_stato IN ( :ls_aperti ) or cod_stato IS NULL);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il conteggio delle righe dei ricambi:" +  sqlca.sqlerrtext)
	return
end if

if not isnull(ll_cont) and ll_cont > 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: esistono delle righe di ricambio prive di stato o con stato ancora aperto!")
	return
end if

ll_cont = 0

select count(*)
into   :ll_cont
from   prog_manutenzioni_risorse_est
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno and
		 num_registrazione = :ll_numero and
		 ( cod_stato IN ( :ls_aperti ) or cod_stato IS NULL);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il conteggio delle righe delle risorse esterne:" +  sqlca.sqlerrtext)
	return
end if

if not isnull(ll_cont) and ll_cont > 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: esistono delle righe di risorsa esterna prive di stato o con stato ancora aperto!")
	return
end if

ll_cont = 0		 

select count(*)
into   :ll_cont
from   prog_manutenzioni_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno and
		 num_registrazione = :ll_numero and
		 ( cod_stato IN ( :ls_aperti ) or cod_stato IS NULL);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il conteggio delle righe delle risorse interne:" +  sqlca.sqlerrtext)
	return
end if

if not isnull(ll_cont) and ll_cont > 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: esistono delle righe di risorsa interna prive di stato o con stato ancora aperto!")
	return
end if

end subroutine

public function boolean wf_operatore_rda ();string ls_amministratore
long	ll_cont

// *** se è un mansionario allora ordini
//     altrimenti RDA

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	
	select flag_collegato
	into   :ls_amministratore
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode = 0 and ls_amministratore = "S" and not isnull(ls_amministratore) then
		return false
	else
	
				//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//		select count(*)
//		into   :ll_cont 
//		from   mansionari
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_utente = :s_cs_xx.cod_utente and
//				 flag_autorizza_rda = 'S' and
//				 flag_approva_rda = 'S';
		uo_mansionario luo_mansionario
		luo_mansionario = create uo_mansionario
		
		if  luo_mansionario.uof_get_privilege(luo_mansionario.approva_rda) and luo_mansionario.uof_get_privilege(luo_mansionario.autorizza_rda)  then 		
			
//		if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont > 0 then
			return false
		else
//			select count(*)
//			into   :ll_cont 
//			from   mansionari
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_utente = :s_cs_xx.cod_utente and
//					 flag_autorizza_rda = 'N' and
//					 flag_approva_rda = 'S';
				if  luo_mansionario.uof_get_privilege(luo_mansionario.approva_rda) and not luo_mansionario.uof_get_privilege(luo_mansionario.autorizza_rda)  then 
//			if sqlca.sqlcode = 0 and not isnull(ll_cont) and ll_cont > 0 then	
					return false
			else
				return true
			end if
		end if
		
	end if
end if

return false
end function

public function string wf_controllo_anno (string fs_flag_budget, ref boolean fb_blocco);string	ls_flag_chiuso
long	ll_anno

ll_anno = dw_attivita.getitemnumber( dw_attivita.getrow(), "anno_riferimento")

if isnull(fs_flag_budget) then fs_flag_budget = "N"

fb_blocco = false

select flag_chiuso
into	:ls_flag_chiuso
from	prog_manutenzioni_anni
where	cod_azienda = :s_cs_xx.cod_azienda and
		anno_riferimento = :ll_anno;
		
if sqlca.sqlcode = 0 and not isnull(ls_flag_chiuso) and ls_flag_chiuso = 'S' then
	fb_blocco = true
	return 'N'
else
	return fs_flag_budget
end if
end function

public function long wf_carica_tipologia_manutenzione (string fs_flag_budget, long fl_anno_riferimento);string	ls_cod_tipo_manutenzione, ls_flag_budget_chiuso,  ls_flag_blocco, ls_null, ls_where
boolean lb_utente_rda

setnull(ls_null)

lb_utente_rda = f_utente_rda()

ls_where = ""
if not isnull(fs_flag_budget) and fs_flag_budget = "S" then
	ls_where += " and ( flag_stampa = 'N' or flag_stampa is null ) "
else
	ls_where += " and flag_stampa = 'S' "
end if

if not isnull(fl_anno_riferimento) and fl_anno_riferimento > 0 then	
	ls_where += " and cod_tipo_manut_scollegata IN ( select cod_tipo_manut_scollegata from prog_manutenzioni_anni_tm where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_riferimento = " + string(fl_anno_riferimento) + " and flag_blocco = 'N' ) "
end if

if lb_utente_rda then
	ls_where += " and flag_ges_supervisore = 'N' "
end if

f_PO_LoadDDDW_DW( dw_attivita, &
						"cod_tipo_manutenzione", &
						sqlca, &
						"tab_tipi_manut_scollegate", &
						"cod_tipo_manut_scollegata", &
						"des_tipo_manut_scollegata", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_where)
						
dw_attivita.setitem( dw_attivita.getrow(), "cod_tipo_manutenzione", ls_null)

return 0

end function

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
else
	ib_nuovo = false
end if

save_on_close(c_socnosave)

dw_attivita.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1

s_cs_xx.parametri.parametro_b_1 = false
end event

on w_programmi_manutenzione_attivita.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_salva=create cb_salva
this.dw_attivita=create dw_attivita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_attivita
end on

on w_programmi_manutenzione_attivita.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_salva)
destroy(this.dw_attivita)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_attivita, &
						"cod_tipo_manutenzione", &
						sqlca, &
						"tab_tipi_manut_scollegate", &
						"cod_tipo_manut_scollegata", &
						"des_tipo_manut_scollegata", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_sort (dw_attivita,"cod_tipo_programma",sqlca,&
                 "tab_tipi_programmi","cod_tipo_programma","des_tipo_programma",&
                 "", " des_tipo_programma ASC ")					  						
						


end event

type cb_2 from commandbutton within w_programmi_manutenzione_attivita
integer x = 2176
integer y = 1280
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;
s_cs_xx.parametri.parametro_b_1 = false
close(parent)
end event

type cb_salva from commandbutton within w_programmi_manutenzione_attivita
integer x = 2569
integer y = 1280
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_row,ll_anno_registrazione,ll_num_registrazione, ll_frequenza, ll_cont, ll_priorita, ll_anno_riferimento
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione,ls_flag_periodicita,ls_note, &
       ls_des_tipo_manutenzione, ls_des_intervento, ls_flag_budget, ls_cod_tipo_manut_collegata, ls_des_tipo_manut_scollegata, &
		 ls_cod_stato, ls_cod_tipo_programma, ls_flag_capitalizzato

dw_attivita.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
	if ib_nuovo then
		ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")		
	end if
end if

ls_cod_attrezzatura = dw_attivita.getitemstring(dw_attivita.getrow(), "cod_attrezzatura")
ls_cod_tipo_manutenzione  = dw_attivita.getitemstring(dw_attivita.getrow(), "cod_tipo_manutenzione")
ls_flag_periodicita  = dw_attivita.getitemstring(dw_attivita.getrow(), "flag_periodicita")
ll_frequenza = dw_attivita.getitemnumber(dw_attivita.getrow(), "frequenza")
ls_note = dw_attivita.getitemstring(dw_attivita.getrow(), "note")
ls_des_intervento  = dw_attivita.getitemstring(dw_attivita.getrow(), "des_intervento")
ls_flag_budget  = dw_attivita.getitemstring(dw_attivita.getrow(), "flag_budget")
ll_priorita = dw_attivita.getitemnumber( dw_attivita.getrow(), "priorita")
ls_cod_stato = dw_attivita.getitemstring( dw_attivita.getrow(), "cod_stato")
ls_cod_tipo_programma = dw_attivita.getitemstring( dw_attivita.getrow(), "cod_tipo_programma")
ll_anno_riferimento = dw_attivita.getitemnumber( dw_attivita.getrow(), "anno_riferimento")
ls_flag_capitalizzato = dw_attivita.getitemstring( dw_attivita.getrow(), "flag_capitalizzato")

if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: valorizzare l'anno di riferimento dell'attività!", stopsign!)
end if

if isnull(ls_cod_tipo_programma) or ls_cod_tipo_programma = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: valorizzare il tipo attività!", stopsign!)
	return 
end if

if isnull(ls_note) or ls_note = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: valorizzare il campo con le istruzioni operative!", stopsign!)
	return -1
end if

select cod_tipo_manut_collegata,
       des_tipo_manut_scollegata
into   :ls_cod_tipo_manut_collegata,
       :ls_des_tipo_manut_scollegata
from   tab_tipi_manut_scollegate
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_manut_scollegata = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la lettura del codice tipo manutenzione collegata: " + sqlca.sqlerrtext)
	return -1
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox( "OMNIA", "Attenzione: non esiste alcuna tipologia di manutenzione scollegata con il codice " + ls_cod_tipo_manutenzione + "!")
	return -1
end if

if isnull( ls_cod_tipo_manut_collegata) or ls_cod_tipo_manut_collegata = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: prima di salvare occorre configurare correttamente la tipologia scollegata " + ls_cod_tipo_manutenzione + "!")
	return -1
end if

if ib_nuovo then
//	if wf_controllo_tipologia() <> 0 then
//		return -1
//	end if
end if

select count(*)
into   :ll_cont
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manut_collegata;

if ll_cont < 1 then
	
	insert into tab_tipi_manutenzione
			 (cod_azienda,
			  cod_attrezzatura,
			  cod_tipo_manutenzione,
			  des_tipo_manutenzione,
			  flag_manutenzione,
			  periodicita,
			  frequenza)
	 values (:s_cs_xx.cod_azienda,
			  :ls_cod_attrezzatura,
			  :ls_cod_tipo_manut_collegata,
			  :ls_des_tipo_manut_scollegata,
			  'M',
			  :ls_flag_periodicita,
			  :ll_frequenza);
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione tipo manutenzione~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if


if ib_nuovo then 

	ll_anno_registrazione = f_anno_esercizio()
	
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
			 
	if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
		ll_num_registrazione = 1 
	else
		ll_num_registrazione ++
	end if
	
	INSERT INTO programmi_manutenzione  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  periodicita,   
			  frequenza,   
			  flag_scadenze,   
			  note_idl,   
			  flag_budget,   
			  tot_costo_intervento,
			  priorita,
			  cod_stato,
			  cod_tipo_programma,
			  anno_riferimento,
			  flag_capitalizzato)  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manut_collegata,   
			  'M',   
			  :ls_flag_periodicita,   
			  :ll_frequenza,   
			  'S',   
			  :ls_note,
			  :ls_flag_budget,
			  0,
			  :ll_priorita,
			  :ls_cod_stato,
			  :ls_cod_tipo_programma,
			  :ll_anno_riferimento,
			  :ls_flag_capitalizzato)  ;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione attività~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

else
	update programmi_manutenzione  
	set    des_intervento = :ls_des_intervento,
	       	cod_attrezzatura = :ls_cod_attrezzatura,
			cod_tipo_manutenzione = :ls_cod_tipo_manut_collegata,
			periodicita = :ls_flag_periodicita,
			frequenza = :ll_frequenza,
			note_idl = :ls_note,
			priorita = :ll_priorita,
			cod_stato = :ls_cod_stato,
			cod_tipo_programma = :ls_cod_tipo_programma,
			flag_budget = :ls_flag_budget,
			anno_riferimento = :ll_anno_riferimento,
			flag_capitalizzato = :ls_flag_capitalizzato
	where cod_azienda = :s_cs_xx.cod_azienda and
		     anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione attività~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if
commit;
s_cs_xx.parametri.parametro_b_1 = true
close(parent)
		  

end event

type dw_attivita from uo_cs_xx_dw within w_programmi_manutenzione_attivita
event ue_controllo_tipologia ( )
event ue_controllo_dati ( )
integer x = 18
integer y = 16
integer width = 2917
integer height = 1224
integer taborder = 10
string dataobject = "d_programmi_manutenzione_grid_attivita"
end type

event ue_controllo_tipologia();//wf_controllo_tipologia()

end event

event ue_controllo_dati();string ls_flag_corrente, ls_sql
boolean lb_blocco
long		ll_anno, ll_null

setnull(ll_null)

ls_flag_corrente = dw_attivita.getitemstring( dw_attivita.getrow(), "flag_budget")

ll_anno = dw_attivita.getitemnumber( dw_attivita.getrow(), "anno_riferimento")

if isnull(ls_flag_corrente) then ls_flag_corrente = "N"

ls_flag_corrente = wf_controllo_anno(ls_flag_corrente, lb_blocco)

dw_attivita.setitem( dw_attivita.getrow(), "flag_budget", ls_flag_corrente)

if lb_blocco then
	dw_attivita.object.flag_budget.protect = 1
else
	dw_attivita.object.flag_budget.protect = 0
end if

if ls_flag_corrente = "S" then
	dw_attivita.setitem( dw_attivita.getrow(), "priorita", 1)
	dw_attivita.Object.priorita.Protect=0
	dw_attivita.Object.priorita.background.color=string(rgb(255,255,255))			
else
	dw_attivita.setitem( dw_attivita.getrow(), "priorita", ll_null)
	dw_attivita.Object.priorita.Protect=1
	dw_attivita.Object.priorita.background.color=string(rgb(192,192,192))
end if

wf_carica_tipologia_manutenzione( ls_flag_corrente, ll_anno)

end event

event itemchanged;call super::itemchanged;string ls_null, ls_cod_attrezzatura,ls_des_tipo_intervento,ls_modalita,ls_periodicita, ls_blocco
long ll_frequenza, ll_null

setnull(ls_null)
setnull(ll_null)

choose case i_colname
		
	case "cod_attrezzatura"
		
		if not isnull(i_coltext) and i_coltext <> "" then
		
			select flag_blocco
			into   :ls_blocco
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_attrezzatura = :i_coltext;
					 
			if sqlca.sqlcode = 0 and not isnull(ls_blocco) and ls_blocco = "S" then
				g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
			end if
					 
		end if		
		
		postevent("ue_controllo_tipologia")

		
	case "tipo_manutenzione"
		postevent("ue_controllo_tipologia")
		
	case "flag_budget"
		
		if i_coltext = "S" then
			setitem( row, "priorita", 1)
			this.Object.priorita.Protect=0
			this.Object.priorita.background.color=string(rgb(255,255,255))			
		else
			setitem( row, "priorita", ll_null)
			this.Object.priorita.Protect=1
			this.Object.priorita.background.color=string(rgb(192,192,192))
		end if
		
		postevent("ue_controllo_dati")		
		
	case "anno_riferimento"
		postevent("ue_controllo_dati")
		
	case "flag_chiuso"
		choose case i_coltext
			case "S"
				wf_controlla_risorse()
		end choose
		
end choose


end event

event pcd_new;call super::pcd_new;string ls_null, ls_cod_attrezzatura, ls_flag_periodicita,ls_flag_budget, ls_appo, ls_stringa, ls_cod_stato, ls_cod_tipo_programma, &
		 ls_flag_chiuso, ls_flag_capitalizzato, ls_stampa
long   ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_frequenza, ll_priorita, ll_prova, ll_anno_riferimento, ll_null

boolean lb_blocco = false

setnull(ls_null)
setnull(ll_null)

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if ib_nuovo then
			
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ls_cod_attrezzatura = idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura")
				setitem(getrow(),"des_intervento", idw_parent.getitemstring(il_rbuttonrow, "des_intervento"))
			else
				// cerco la riga principale
				ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
				ls_cod_attrezzatura = idw_parent.getitemstring(ll_row, "cod_attrezzatura")
				setitem(getrow(),"des_intervento", idw_parent.getitemstring(ll_row, "des_intervento"))
			end if
			
			setitem(getrow(),"cod_attrezzatura", idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura"))
			setitem(getrow(),"flag_chiuso", 'N')
			setitem(getrow(), "flag_capitalizzato", "N")
			
			if f_anno_riferimento() > 0 then
				
				setitem( getrow(), "anno_riferimento", f_anno_riferimento())
				
				// *** se l'hanno di riferimento ha il budget chiuso non posso mettere attività a budget
				
				ll_anno_riferimento = f_anno_riferimento()
				
				select flag_chiuso
				into	:ls_flag_chiuso
				from	prog_manutenzioni_anni
				where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_riferimento = :ll_anno_riferimento;
						
				if sqlca.sqlcode = 0 and not isnull(ls_flag_chiuso) and ls_flag_chiuso = "S" then lb_blocco = true
				
				if wf_operatore_rda() then
					f_PO_LoadDDDW_DW( dw_attivita, &
											"cod_tipo_manutenzione", &
											sqlca, &
											"tab_tipi_manut_scollegate", &
											"cod_tipo_manut_scollegata", &
											"des_tipo_manut_scollegata", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manut_scollegata IN (select cod_tipo_manut_scollegata from prog_manutenzioni_anni_tm where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_riferimento = " +  string(f_anno_riferimento())  + " ) and ( flag_ges_supervisore = 'N' or flag_ges_supervisore is NULL ) ")				
											
				else
					
					f_PO_LoadDDDW_DW( dw_attivita, &
											"cod_tipo_manutenzione", &
											sqlca, &
											"tab_tipi_manut_scollegate", &
											"cod_tipo_manut_scollegata", &
											"des_tipo_manut_scollegata", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_manut_scollegata IN (select cod_tipo_manut_scollegata from prog_manutenzioni_anni_tm where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_riferimento = " +  string(f_anno_riferimento())  + " ) ")												
					
				end if				
				
			else						//	*** non conosco l'anno di riferimento quindi lo imposto all' anno corrente
				
				setitem( getrow(), "anno_riferimento", year(today()))
				
				ll_anno_riferimento = year(today())
				
				select flag_chiuso
				into	:ls_flag_chiuso
				from	prog_manutenzioni_anni
				where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_riferimento = :ll_anno_riferimento;
						
				if sqlca.sqlcode = 0 and not isnull(ls_flag_chiuso) and ls_flag_chiuso = "S" then lb_blocco = true				
				
				if wf_operatore_rda() then
					f_PO_LoadDDDW_DW( dw_attivita, &
											"cod_tipo_manutenzione", &
											sqlca, &
											"tab_tipi_manut_scollegate", &
											"cod_tipo_manut_scollegata", &
											"des_tipo_manut_scollegata", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "'  and ( flag_ges_supervisore = 'N' or flag_ges_supervisore is NULL ) ")				
											
				else			
					f_PO_LoadDDDW_DW( dw_attivita, &
											"cod_tipo_manutenzione", &
											sqlca, &
											"tab_tipi_manut_scollegate", &
											"cod_tipo_manut_scollegata", &
											"des_tipo_manut_scollegata", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")		
					end if
										
			end if
	
			select stringa
			into   :ls_stringa
			from   parametri_azienda
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_parametro = 'CTM';

			if sqlca.sqlcode = 0 and not isnull(ls_stringa) and ls_stringa <> "" then
				setitem(getrow(),"cod_tipo_manutenzione", ls_stringa)	
			else
				setitem(getrow(),"cod_tipo_manutenzione", ls_null)
			end if
			
			this.Object.frequenza.Protect=1
			this.Object.frequenza.background.color=string(rgb(192,192,192))
			this.Object.flag_periodicita.Protect=1
			this.Object.flag_periodicita.background.color=string(rgb(192,192,192))
			
			if f_utente_amministratore() then
				this.object.flag_chiuso.protect = 0
			else
				this.object.flag_chiuso.protect = 1
			end if
			
//			wf_controllo_tipologia()
			setitem( getrow(), "priorita", ll_null)
			this.Object.priorita.Protect=1
			this.Object.priorita.background.color = string(rgb(192,192,192))
			
		else		
			
			// *******************************************************************************************************
			// *****************************			MODIFICA     *************************************************
			// *******************************************************************************************************
			
			// l'operazione di modifica può avvenire solo sulla riga principale.
			
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") <> "P" then
				g_mb.messagebox("OMNIA","L'operazione di modifica può avvenire solo riga Principale")
				rollback;
				close(parent)
			end if
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			
			select periodicita, 
					 frequenza, 
					 priorita,
					 cod_tipo_programma,
					 flag_chiuso,
					 flag_budget,
					 anno_riferimento,
					 flag_capitalizzato
			into   :ls_flag_periodicita, 
					 :ll_frequenza, 
					 :ll_priorita,
					 :ls_cod_tipo_programma,
					 :ls_flag_chiuso,
					 :ls_flag_budget,
					 :ll_anno_riferimento,
					 :ls_flag_capitalizzato
			from   programmi_manutenzione
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca piano manutenzione~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			if isnull(ls_flag_chiuso) or ls_flag_chiuso = "" then ls_flag_chiuso = "N"
			if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
			if isnull(ls_flag_capitalizzato) or ls_flag_capitalizzato = "" then ls_flag_capitalizzato = "N"
			if isnull(ll_anno_riferimento) then ll_anno_riferimento = f_anno_riferimento()
			
			ls_cod_attrezzatura = idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura")
			
			setitem(getrow(),"cod_attrezzatura", idw_parent.getitemstring(il_rbuttonrow, "cod_attrezzatura"))
			setitem(getrow(),"des_intervento", idw_parent.getitemstring(il_rbuttonrow, "des_intervento"))
			setitem(getrow(),"flag_periodicita", ls_flag_periodicita)
			setitem(getrow(),"frequenza", ll_frequenza)
			setitem(getrow(),"note", idw_parent.getitemstring(il_rbuttonrow, "des_manutenzione"))
			setitem(getrow(),"priorita", ll_priorita)
			setitem(getrow(), "cod_tipo_programma", ls_cod_tipo_programma)
			setitem(getrow(), "flag_chiuso", ls_flag_chiuso)
			setitem(getrow(), "flag_capitalizzato", ls_flag_capitalizzato)
			
			wf_carica_tipologia_manutenzione( ls_flag_budget, ll_anno_riferimento)									
			
			setitem(getrow(),"cod_tipo_manutenzione", idw_parent.getitemstring(il_rbuttonrow, "cod_tipo_manutenzione"))	
			setitem(getrow(),"flag_budget", ls_flag_budget)			
			setitem( getrow(), "anno_riferimento", ll_anno_riferimento)
			if ls_flag_budget = "S" then
				this.Object.priorita.Protect=0
				this.Object.priorita.background.color = string(rgb(255,255,255))								
			else
				this.Object.priorita.Protect=1
				this.Object.priorita.background.color = string(rgb(192,192,192))				
			end if
			
		end if
		
		accepttext()

	end if
end if
		
boolean ib_operatore_rda

ib_operatore_rda = wf_operatore_rda()

if ib_operatore_rda then
	this.object.flag_budget.protect = 1
	this.object.flag_capitalizzato.protect = 1
end if

if ib_nuovo then
	postevent("ue_controllo_dati")
else
	
	string ls_flag_corrente
	long	ll_anno
	
	ls_flag_corrente = dw_attivita.getitemstring( dw_attivita.getrow(), "flag_budget")
	
	ll_anno = dw_attivita.getitemnumber( dw_attivita.getrow(), "anno_riferimento")
	
	if isnull(ls_flag_corrente) then ls_flag_corrente = "N"
	
	ls_flag_corrente = wf_controllo_anno(ls_flag_corrente, lb_blocco)

	if lb_blocco then
		dw_attivita.object.flag_budget.protect = 1
		dw_attivita.object.anno_riferimento.protect = 1
		dw_attivita.object.cod_tipo_manutenzione.protect = 1
		dw_attivita.object.priorita.protect = 1
		dw_attivita.object.cod_attrezzatura.protect = 1
		if s_cs_xx.admin then
			dw_attivita.object.anno_riferimento.protect = 0
			dw_attivita.object.cod_tipo_manutenzione.protect = 0
			dw_attivita.object.priorita.protect = 0
			dw_attivita.object.cod_attrezzatura.protect = 0
		end if
	end if	
	
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_attivita,"cod_attrezzatura")
end choose
end event

