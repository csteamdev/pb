﻿$PBExportHeader$w_programmi_manutenzione_operai.srw
$PBExportComments$Finestra Manutenzioni personalizzata Guerrato
forward
global type w_programmi_manutenzione_operai from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_programmi_manutenzione_operai
end type
type cb_salva from commandbutton within w_programmi_manutenzione_operai
end type
type dw_operai from uo_cs_xx_dw within w_programmi_manutenzione_operai
end type
end forward

global type w_programmi_manutenzione_operai from w_cs_xx_risposta
integer width = 2341
integer height = 1300
string title = "Aggiunta Risorsa Interna"
event ue_carica ( )
cb_annulla cb_annulla
cb_salva cb_salva
dw_operai dw_operai
end type
global w_programmi_manutenzione_operai w_programmi_manutenzione_operai

type variables
boolean ib_nuovo=false, ib_nuovo_rda=false
long il_rbuttonrow
datawindow idw_parent
string     is_cod_area_aziendale
end variables

forward prototypes
public function integer wf_controllo_anno ()
end prototypes

event ue_carica();string ls_null

f_PO_LoadDDDW_DW( dw_operai,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_operaio IN (select cod_operaio from tab_aree_risorse where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + is_cod_area_aziendale + "' and cod_operaio is not null)")
					  
dw_operai.setitem( dw_operai.getrow(), "cod_operaio", ls_null)					  
end event

public function integer wf_controllo_anno ();string	ls_cod_tipo_manutenzione, ls_flag_budget_chiuso, ls_flag_budget, ls_flag_blocco, ls_null
long	ll_anno_riferimento

dw_operai.accepttext()

ll_anno_riferimento = dw_operai.getitemnumber( dw_operai.getrow(), "anno_riferimento")
ls_flag_budget = dw_operai.getitemstring( dw_operai.getrow(), "flag_budget")
if isnull(ls_flag_budget) then ls_flag_budget = "N"

if not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
	
	select flag_chiuso
	into	:ls_flag_budget_chiuso
	from 	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_riferimento = :ll_anno_riferimento;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il controllo budget consolidato per l'anno " + string(ll_anno_riferimento) + ":" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	if not isnull(ls_flag_budget_chiuso) and ls_flag_budget_chiuso = "S" and not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta consolidato! Impossibile aggiungere attività a budget!", stopsign!)
		return -1
	end if

end if

return 0

end function

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
	ib_nuovo_rda = false
elseif upper(s_cs_xx.parametri.parametro_s_1) = "NRDA" then
	ib_nuovo = true
	ib_nuovo_rda = true	
else
	ib_nuovo = false
end if


save_on_close(c_socnosave)

dw_operai.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1

long   ll_anno_registrazione, ll_num_registrazione, ll_row
string ls_cod_attrezzatura

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
	ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
end if


select cod_attrezzatura
into   :ls_cod_attrezzatura
from   programmi_manutenzione  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione ;
		 
select cod_area_aziendale
into   :is_cod_area_aziendale
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura;
				 
s_cs_xx.parametri.parametro_b_1 = false
//postevent("ue_carica")
end event

on w_programmi_manutenzione_operai.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_salva=create cb_salva
this.dw_operai=create dw_operai
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_operai
end on

on w_programmi_manutenzione_operai.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_salva)
destroy(this.dw_operai)
end on

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_operai,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome "+guo_functions.uof_concat_op()+" ', ' "+guo_functions.uof_concat_op()+" nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
f_PO_LoadDDDW_sort (dw_operai,"cod_tipo_programma",sqlca,&
                 "tab_tipi_programmi","cod_tipo_programma","des_tipo_programma",&
                 "", " des_tipo_programma ASC ")					  
					  
f_PO_LoadDDDW_DW( dw_operai, &
						"cod_stato", &
						sqlca, &
						"tab_stato_programmi", &
						"cod_stato", &
						"des_stato", &
						" ")									  
					  

end event

type cb_annulla from commandbutton within w_programmi_manutenzione_operai
integer x = 1509
integer y = 1080
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;
s_cs_xx.parametri.parametro_b_1 = false
close(parent)
end event

type cb_salva from commandbutton within w_programmi_manutenzione_operai
integer x = 1897
integer y = 1080
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long   ll_row,ll_anno_registrazione,ll_num_registrazione ,ll_progressivo,ll_ore,ll_minuti, ll_priorita, ll_ore_eff, &
         ll_minuti_eff, ll_anno_riferimento
string      ls_cod_operaio,ls_flag_prezzo_certo,ls_flag_capitalizzato, ls_cod_tipo_programma, ls_cod_stato, ls_rda, ls_flag_budget
dec{4}     ld_operaio_costo_orario
datetime ldt_data_esecuzione

dw_operai.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
	if ib_nuovo then
		ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
	end if
end if

ll_ore  = dw_operai.getitemnumber(dw_operai.getrow(), "ore")
ll_minuti  = dw_operai.getitemnumber(dw_operai.getrow(), "minuti")
ls_cod_operaio  = dw_operai.getitemstring(dw_operai.getrow(), "cod_operaio")
ld_operaio_costo_orario = dw_operai.getitemnumber(dw_operai.getrow(), "operaio_costo_orario")
ls_flag_prezzo_certo = dw_operai.getitemstring(dw_operai.getrow(), "flag_prezzo_certo")
ls_flag_capitalizzato = dw_operai.getitemstring(dw_operai.getrow(), "flag_capitalizzato")
ll_priorita = dw_operai.getitemnumber( dw_operai.getrow(), "priorita")
ls_cod_tipo_programma = dw_operai.getitemstring( dw_operai.getrow(), "cod_tipo_programma")
ls_cod_stato = dw_operai.getitemstring( dw_operai.getrow(), "cod_stato")
ll_ore_eff  = dw_operai.getitemnumber(dw_operai.getrow(), "ore_effettive")
ll_minuti_eff  = dw_operai.getitemnumber(dw_operai.getrow(), "minuti_effettivi")
ls_flag_budget = dw_operai.getitemstring( dw_operai.getrow(), "flag_budget")
ll_anno_riferimento = dw_operai.getitemnumber(dw_operai.getrow(), "anno_riferimento")
ldt_data_esecuzione = dw_operai.getitemdatetime( dw_operai.getrow(), "data_esecuzione")

if isnull(ls_flag_budget) then ls_flag_budget = "N"


if isnull(ll_ore) then ll_ore = 0
if isnull(ll_minuti) then ll_minuti = 0
if isnull(ll_ore_eff) then ll_ore_eff = 0
if isnull(ll_minuti_eff) then ll_minuti_eff = 0
if isnull(ls_cod_stato) then ls_cod_stato = "N"

choose case ls_cod_stato
	case "N"
		
		if ll_ore_eff > 0  or ll_minuti_eff > 0 then
		
			g_mb.messagebox( "OMNIA", "Attenzione: l'attività della risorsa non risulta da eseguire!~r~nImpossibile impostare questo stato!", stopsign!)
			dw_operai.triggerevent("ue_stato_nullo")
			return -1
			
		end if
		
	case "S", "I"
		
		if ll_ore_eff = 0  and ll_minuti_eff = 0 then
		
			g_mb.messagebox( "OMNIA", "Attenzione: l'attività della risorsa risulta da eseguire!~r~nImpossibile impostare questo stato!", stopsign!)
			dw_operai.triggerevent("ue_stato_nullo")
			return -1
			
		end if						
		
end choose


if isnull(ls_cod_tipo_programma) or ls_cod_tipo_programma = "" then
	g_mb.messagebox("OMNIA", "Attenzione: specificare un tipo attività!")
	rollback;
	return	
end if

if ib_nuovo then
	
	select max(progressivo)
	into   :ll_progressivo
	from   prog_manutenzioni_operai
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
			 
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	if ib_nuovo_rda then
		ls_rda = "S"
	else
		ls_rda = "N"
	end if
	
	INSERT INTO prog_manutenzioni_operai
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  progressivo,
			  cod_operaio,
			  ore,
			  minuti,
			  operaio_costo_orario,
			  flag_prezzo_certo,
			  flag_capitalizzato,
			  priorita,
			  cod_tipo_programma,
			  cod_stato,
			  flag_rda,
			  ore_effettive,
			  minuti_effettivi,
			  flag_budget,
			  anno_riferimento,
			  data_esecuzione,
			  flag_confermato)
	VALUES (:s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,
			  :ll_progressivo,
			  :ls_cod_operaio,
			  :ll_ore,
			  :ll_minuti,
			  :ld_operaio_costo_orario,
			  :ls_flag_prezzo_certo,
			  :ls_flag_capitalizzato,
			  :ll_priorita,
			  :ls_cod_tipo_programma,
			  :ls_cod_stato,
			  :ls_rda,
			  :ll_ore_eff,
			  :ll_minuti_eff,
			  :ls_flag_budget,
			  :ll_anno_riferimento,
			  :ldt_data_esecuzione,
			  'N')  ;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione risorsa interna~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

else
	
	ll_progressivo  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")

	
	update prog_manutenzioni_operai
	set 	cod_operaio = :ls_cod_operaio,
			ore = :ll_ore,
			minuti = :ll_minuti,
			operaio_costo_orario = :ld_operaio_costo_orario,
			flag_prezzo_certo = :ls_flag_prezzo_certo,
			flag_capitalizzato = :ls_flag_capitalizzato,
			priorita = :ll_priorita,
			cod_tipo_programma = :ls_cod_tipo_programma,
			cod_stato = :ls_cod_stato,
			ore_effettive = :ll_ore_eff,
			minuti_effettivi = :ll_minuti_eff,
			flag_budget = :ls_flag_budget,
			flag_confermato = 'N',
			anno_riferimento = :ll_anno_riferimento,
			data_esecuzione = :ldt_data_esecuzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			progressivo = :ll_progressivo;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in aggiornamento risorsa interna~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if

commit;
s_cs_xx.parametri.parametro_b_1 = true
close(parent)
		  

end event

type dw_operai from uo_cs_xx_dw within w_programmi_manutenzione_operai
event ue_stato_nullo ( )
event ue_blocca_budget ( )
event ue_controllo_anno_inizio ( )
integer x = 23
integer y = 20
integer width = 2240
integer height = 1020
integer taborder = 10
string dataobject = "d_programmi_manutenzione_grid_operai"
end type

event ue_stato_nullo();string ls_null

setnull(ls_null)

setitem(getrow(), "cod_stato", ls_null)
end event

event ue_blocca_budget();string	ls_flag_budget

g_mb.messagebox( "OMNIA", "Attenzione: impossibile modificare lo stato Budget della riga, in quanto il budget stesso per l'anno indicato risulta chiuso!", stopsign!)

accepttext()
			
ls_flag_budget = getitemstring( getrow(), "flag_budget")
			
if ls_flag_budget = "N" then									//	***		se non è a budget allora devo bloccare i campi
	setitem( getrow(), "flag_budget", "S")
else
	setitem( getrow(), "flag_budget", "N")
end if

end event

event ue_controllo_anno_inizio();string	ls_cod_tipo_manutenzione, ls_flag_budget_chiuso, ls_flag_budget, ls_flag_blocco, ls_null, ls_flag_amministratore
long	ll_anno_riferimento

accepttext()

ll_anno_riferimento = getitemnumber( getrow(), "anno_riferimento")
ls_flag_budget = getitemstring( getrow(), "flag_budget")
if isnull(ls_flag_budget) then ls_flag_budget = "N"

if not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
	
	select flag_chiuso
	into	:ls_flag_budget_chiuso
	from 	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_riferimento = :ll_anno_riferimento;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il controllo budget consolidato per l'anno " + string(ll_anno_riferimento) + ":" + sqlca.sqlerrtext, stopsign!)
		return
	end if
	
	if not isnull(ls_flag_budget_chiuso) and ls_flag_budget_chiuso = "S" and not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta consolidato! Impossibile aggiungere attività a budget!", stopsign!)
		
		select flag_collegato
		into	:ls_flag_amministratore
		from	utenti
		where	cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			rollback;
			close(parent)
		end if
		
		return
	end if

end if

return 

end event

event pcd_new;call super::pcd_new;string ls_null, ls_cod_operaio, ls_stringa, ls_flag_capitalizzato, ls_flag_prezzo_certo, ls_cod_tipo_programma, ls_cod_stato, ls_flag_budget, ls_flag_amministratore, ls_flag_rda, &
		ls_flag_budget_consolidato

long ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, &
     ll_ore, ll_minuti, ll_priorita, ll_ore_effettive, ll_minuti_effettivi, ll_anno_riferimento, ll_null
dec{4} ld_costo_costo_orario

datetime ldt_data_esecuzione

setnull(ls_null)
setnull(ll_null)

parent.triggerevent("ue_carica")

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if ib_nuovo then
			
			//	*** per le nuove righe di ricambio, se l'anno di riferimento risulta consolidato, propongo il budget di default a N, altrimenti
			//		propongo quello dell'attività
			
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			else
				// cerco la riga principale
				ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
				ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
			end if
			
			select priorita,
					 cod_tipo_programma,
					 cod_stato,
					 flag_budget,
					 flag_capitalizzato,
					 anno_riferimento
			into   :ll_priorita,
			       :ls_cod_tipo_programma,
					 :ls_cod_stato,
					 :ls_flag_budget,
					 :ls_flag_capitalizzato,
					 :ll_anno_riferimento
			from   programmi_manutenzione  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
				    anno_registrazione = :ll_anno_registrazione and
				    num_registrazione = :ll_num_registrazione ;
				 
				 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Dettaglio Manutenzione","Impossibile trovare la priorita della manutenzione: " + string(ll_anno_registrazione)+"/"+ string(ll_num_registrazione)+" "+ sqlca.sqlerrtext, StopSign!)
				return
			end if			
			
					 
			if isnull(ll_priorita) then ll_priorita = 1
			if ls_cod_tipo_programma = "" then setnull(ls_cod_tipo_programma)
			if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
			if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
				ll_anno_riferimento = f_anno_riferimento()
				if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
					ll_anno_riferimento = year(today())
				end if
			end if			
			
			select stringa
			into   :ls_stringa
			from   parametri_azienda
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_parametro = 'COP';
			
			if sqlca.sqlcode = 0 then
				setitem(getrow(),"cod_operaio", ls_stringa)
				
				select costo_orario
				into   :ld_costo_costo_orario
				from   tab_aree_risorse
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_area_aziendale = :is_cod_area_aziendale and
						 cod_operaio = :ls_stringa and
						 anno_riferimento = :ll_anno_riferimento;
						 
				setitem( getrow(), "operaio_costo_orario", ld_costo_costo_orario)								
				
			end if	

			setitem(1, "cod_tipo_programma", ls_cod_tipo_programma)
			
			// *** leggo lo stato 
			
			select stringa
			into   :ls_cod_stato
			from   parametri
			where  cod_parametro = 'MSN';
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "OMNIA", "Attenzione: impostare il parametro MSN con codice stato attività parzialmente eseguita:" + sqlca.sqlerrtext, stopsign!)
				return -1
			end if
			
			if ls_cod_stato = "" then setnull(ls_cod_stato)	
			if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
			
			if ls_flag_budget = "S" then
				setitem(1, "priorita", ll_priorita)
				this.Object.priorita.Protect=0
				this.Object.flag_prezzo_certo.Protect=0
				this.Object.priorita.background.color=string(rgb(255,255,255))				
				this.Object.flag_prezzo_certo.background.color=string(rgb(255,255,255))
			else
				setitem(1, "priorita", ll_null)
				this.Object.priorita.Protect=1
				this.Object.flag_prezzo_certo.Protect=1
				this.Object.priorita.background.color=string(rgb(192,192,192))	
				this.Object.flag_prezzo_certo.background.color=string(rgb(192,192,192))
			end if	
						
			select flag_chiuso
			into	:ls_flag_budget_consolidato
			from	prog_manutenzioni_anni
			where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_riferimento = :ll_anno_riferimento;
					
			if sqlca.sqlcode = 0 and not isnull(ls_flag_budget_consolidato) and ls_flag_budget_consolidato = "S" then
				setitem( 1, "flag_budget", "N")
			else
				setitem( 1, "flag_budget", ls_flag_budget)
			end if			
						
			setitem(1, "anno_riferimento", ll_anno_riferimento)
			setitem(1, "flag_capitalizzato", ls_flag_capitalizzato)
			setitem(1, "cod_stato", ls_cod_stato)	
			
			postevent("ue_controllo_anno_inizio")
						
		else		
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			ll_progressivo    = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			

			SELECT cod_operaio,   
					ore,   
					minuti,   
					operaio_costo_orario,   
					flag_prezzo_certo,   
					flag_capitalizzato,
					priorita,
					cod_tipo_programma,
					cod_stato,
					ore_effettive,
					minuti_effettivi,
					flag_budget,
					anno_riferimento,
					data_esecuzione,
					flag_rda
			 INTO :ls_cod_operaio,   
					:ll_ore,   
					:ll_minuti,   
					:ld_costo_costo_orario,   
					:ls_flag_prezzo_certo,   
					:ls_flag_capitalizzato,
					:ll_priorita,
					:ls_cod_tipo_programma,
					:ls_cod_stato,
					:ll_ore_effettive,
					:ll_minuti_effettivi,
					:ls_flag_budget,
					:ll_anno_riferimento,
					:ldt_data_esecuzione,
					:ls_flag_rda
			from  prog_manutenzioni_operai
			where cod_azienda = :s_cs_xx.cod_azienda and
			      anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione and
					progressivo = :ll_progressivo;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca risorsa interna~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			if isnull(ls_flag_rda) then ls_flag_rda = "N"
			
			setitem(getrow(),"cod_operaio", ls_cod_operaio)
			setitem(getrow(),"ore", ll_ore)
			setitem(getrow(),"minuti", ll_minuti)
			setitem(getrow(),"flag_prezzo_certo", ls_flag_prezzo_certo)
			setitem(getrow(),"flag_capitalizzato", ls_flag_capitalizzato)
			setitem(getrow(),"operaio_costo_orario", ld_costo_costo_orario)
			setitem(getrow(),"priorita", ll_priorita)
			setitem(getrow(),"cod_tipo_programma", ls_cod_tipo_programma)
			setitem(getrow(),"cod_stato", ls_cod_stato)
			setitem(getrow(),"ore_effettive", ll_ore_effettive)
			setitem(getrow(),"minuti_effettivi", ll_minuti_effettivi)
			setitem(getrow(),"flag_budget", ls_flag_budget)
			setitem(getrow(),"anno_riferimento", ll_anno_riferimento)
			setitem(getrow(),"data_esecuzione", ldt_data_esecuzione)
			
			if ls_flag_budget = "S" then
				setitem(1, "priorita", ll_priorita)
				this.Object.priorita.Protect=0
				this.Object.flag_prezzo_certo.Protect=0
				this.Object.priorita.background.color=string(rgb(255,255,255))				
				this.Object.flag_prezzo_certo.background.color=string(rgb(255,255,255))
			else
				setitem(1, "priorita", ll_null)
				this.Object.priorita.Protect=1
				this.Object.flag_prezzo_certo.Protect=1
				this.Object.priorita.background.color=string(rgb(192,192,192))	
				this.Object.flag_prezzo_certo.background.color=string(rgb(192,192,192))
			end if			
			
		end if

	end if
end if

//	*** Michela 23/05/2007: controllo il flag budget

accepttext()

ls_flag_budget = getitemstring( getrow(), "flag_budget")
if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"

if ls_flag_budget = "N" then									//	***		se non è a budget allora devo bloccare i campi	
	
	this.Object.ore.Protect=1
	this.Object.minuti.Protect=1
	this.Object.flag_prezzo_certo.Protect=1
	this.Object.ore.background.color=string(rgb(192,192,192))
	this.Object.minuti.background.color=string(rgb(192,192,192))	
	this.Object.ore_effettive.Protect=0
	this.Object.minuti_effettivi.Protect=0
	this.Object.data_esecuzione.Protect=0
	this.Object.ore_effettive.background.color=string(rgb(255,255,255))
	this.Object.minuti_effettivi.background.color=string(rgb(255,255,255))	
	this.Object.data_esecuzione.background.color=string(rgb(255,255,255))
	
else															//	***		altrimenti li sblocco	
	this.Object.ore.Protect=0
	this.Object.minuti.Protect=0
	this.Object.flag_prezzo_certo.Protect=0
	this.Object.ore.background.color=string(rgb(255,255,255))
	this.Object.minuti.background.color=string(rgb(255,255,255))	
	this.Object.ore_effettive.Protect=1
	this.Object.minuti_effettivi.Protect=1
	this.Object.data_esecuzione.Protect=1
	this.Object.ore_effettive.background.color=string(rgb(192,192,192))
	this.Object.minuti_effettivi.background.color=string(rgb(192,192,192))	
	this.Object.data_esecuzione.background.color=string(rgb(192,192,192))	
end if
		
select flag_collegato
into	:ls_flag_amministratore
from	utenti
where	cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
else	
	this.Object.flag_budget.Protect=1
	this.Object.anno_riferimento.Protect=1
	this.Object.flag_budget.background.color=string(rgb(192,192,192))
	this.Object.anno_riferimento.background.color=string(rgb(192,192,192))	
end if

if ib_nuovo then
else

	if f_utente_rda()  and ls_flag_rda = "N" then
		this.Object.flag_capitalizzato.Protect=1
		this.Object.flag_capitalizzato.background.color=string(rgb(192,192,192))	
		this.Object.priorita.Protect=1
		this.Object.flag_prezzo_certo.Protect=1
		this.Object.flag_capitalizzato.Protect=1				
		this.Object.priorita.background.color=string(rgb(192,192,192))			
		this.Object.ore.Protect=1
		this.Object.minuti.Protect=1
		this.Object.ore.background.color=string(rgb(192,192,192))
		this.Object.minuti.background.color=string(rgb(192,192,192))	
		this.Object.ore_effettive.Protect=1
		this.Object.minuti_effettivi.Protect=1
		this.Object.ore_effettive.background.color=string(rgb(192,192,192))
		this.Object.minuti_effettivi.background.color=string(rgb(192,192,192))				
		this.Object.data_esecuzione.Protect=1
		this.Object.cod_operaio.Protect=1
		this.Object.operaio_costo_orario.Protect=1
		this.Object.data_esecuzione.background.color=string(rgb(192,192,192))
		this.Object.cod_operaio.background.color=string(rgb(192,192,192))
		this.Object.operaio_costo_orario.background.color=string(rgb(192,192,192))
	end if
	
end if
end event

event itemchanged;call super::itemchanged;dec{4} ld_costo_orario
long	 ll_ore, ll_minuti, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ricambio, ll_anno_riferimento
string ls_chiuso, ls_flag_budget, ls_flag_chiuso, ls_flag_amministratore, ls_cod_operaio

choose case i_colname
		
	case "cod_operaio"
		
		ll_anno_riferimento = getitemnumber( row, "anno_riferimento")
		
		select costo_orario
		into   :ld_costo_orario
		from   tab_aree_risorse
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_area_aziendale = :is_cod_area_aziendale and
				 cod_operaio = :i_coltext and
				 anno_riferimento = :ll_anno_riferimento;
				 
		setitem( row, "operaio_costo_orario", ld_costo_orario)
		
	case "anno_riferimento"
		
		ls_cod_operaio = getitemstring( row, "cod_operaio")
		ll_anno_riferimento = long(i_coltext)
		
		select costo_orario
		into   :ld_costo_orario
		from   tab_aree_risorse
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_area_aziendale = :is_cod_area_aziendale and
				 cod_operaio = :ls_cod_operaio and
				 anno_riferimento = :ll_anno_riferimento;
				 
		setitem( row, "operaio_costo_orario", ld_costo_orario)		
		
	case "cod_stato"
		
		ll_ore = getitemnumber( row, "ore_effettive")
		ll_minuti = getitemnumber( row, "minuti_effettivi")
		if isnull(ll_ore) then ll_ore = 0
		if isnull(ll_minuti) then ll_minuti = 0
		
		if not isnull(data) and data <> "" then
			
			select flag_tipo_stato
			into   :ls_chiuso
			from   tab_stato_programmi
			where  cod_stato = :data;
			
			if sqlca.sqlcode = 0 then
				
				if isnull(ls_chiuso) then ls_chiuso = "N"
				
				choose case ls_chiuso
					case "N"
						
						if ll_ore > 0  or ll_minuti > 0 then
						
							g_mb.messagebox( "OMNIA", "Attenzione: l'attività della risorsa non risulta da eseguire!~r~nImpossibile impostare questo stato!", stopsign!)
							setnull(data)
							postevent("ue_stato_nullo")
							return 
							
						end if
						
					case "S", "I"
						
						if ll_ore = 0  and ll_minuti = 0 then
						
							g_mb.messagebox( "OMNIA", "Attenzione: l'attività della risorsa risulta da eseguire!~r~nImpossibile impostare questo stato!", stopsign!)
							setnull(data)
							postevent("ue_stato_nullo")
							return 
							
						end if						
				end choose
				
			end if
			
		end if		
		
		case "flag_budget"
			
			//	*** Michela 23/05/2007: controllo il flag budget
			
			//	*** controllo di che anno fa parte la riga, e se il budget risulta chiuso. se si allora a meno che non sia amministratore non può fare nulla
			
			ls_flag_budget = data
			
			if not ib_nuovo then
			
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
				ll_prog_riga_ricambio  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			
				select anno_riferimento
				into    :ll_anno_riferimento
				from 	 prog_manutenzioni_operai
				where  cod_azienda = :s_cs_xx.cod_azienda and
				          anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 progressivo = :ll_prog_riga_ricambio;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "OMNIA", "Errore durante il controllo anno riferimento della riga di risorsa interna:" + sqlca.sqlerrtext, stopsign!)
					return -1
				end if
				
				ll_anno_riferimento = getitemnumber( getrow(), "anno_riferimento")
				
				select flag_chiuso
				into    :ls_flag_chiuso
				from   prog_manutenzioni_anni
				where cod_azienda = :s_cs_xx.cod_azienda and
							 anno_riferimento = :ll_anno_riferimento;
							 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "OMNIA", "Errore durante il controllo chiusura budget anno " + string(ll_anno_riferimento) + ": " + sqlca.sqlerrtext, stopsign!)
					return -1
				end if
				
				if not isnull(ls_flag_chiuso) and ls_flag_chiuso = "S" then
					
					select flag_collegato
					into    :ls_flag_amministratore
					from   utenti
					where cod_utente = :s_cs_xx.cod_utente;
					
					if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
						g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta già consolidato!", information!)
					else				
						postevent("ue_blocca_budget")
						return -1
					end if
				end if
				
			end if
									
			if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
			
			if ls_flag_budget = "N" then									//	***		se non è a budget allora devo bloccare i campi
				

				this.Object.ore.Protect=1
				this.Object.minuti.Protect=1
				this.Object.priorita.Protect=1
				this.Object.flag_prezzo_certo.Protect=1
				this.Object.ore.background.color=string(rgb(192,192,192))
				this.Object.minuti.background.color=string(rgb(192,192,192))
				this.Object.priorita.background.color=string(rgb(192,192,192))
				this.Object.flag_prezzo_certo.background.color=string(rgb(192,192,192))		
				
				this.Object.ore_effettive.Protect=0
				this.Object.minuti_effettivi.Protect=0
				this.Object.data_esecuzione.Protect=0
				this.Object.ore_effettive.background.color=string(rgb(255,255,255))
				this.Object.minuti_effettivi.background.color=string(rgb(255,255,255))				
				this.Object.data_esecuzione.background.color=string(rgb(255,255,255))
				
			else															//	***		altrimenti li sblocco
				
				this.Object.ore.Protect=0
				this.Object.minuti.Protect=0
				this.Object.priorita.Protect=0
				this.Object.flag_prezzo_certo.Protect=0
				this.Object.ore.background.color=string(rgb(255,255,255))
				this.Object.minuti.background.color=string(rgb(255,255,255))
				this.Object.priorita.background.color=string(rgb(255,255,255))
				this.Object.flag_prezzo_certo.background.color=string(rgb(255,255,255))		
				
				this.Object.ore_effettive.Protect=1
				this.Object.minuti_effettivi.Protect=1
				this.Object.data_esecuzione.Protect=1
				this.Object.ore_effettive.background.color=string(rgb(192,192,192))
				this.Object.minuti_effettivi.background.color=string(rgb(192,192,192))				
				this.Object.data_esecuzione.background.color=string(rgb(192,192,192))
				
			end if					
		
		
		
end choose
end event

