﻿$PBExportHeader$w_programmi_manutenzione_ordini.srw
$PBExportComments$Finestra genera ordini progen
forward
global type w_programmi_manutenzione_ordini from w_cs_xx_principale
end type
type cb_amministratore from commandbutton within w_programmi_manutenzione_ordini
end type
type dw_ricambi from datawindow within w_programmi_manutenzione_ordini
end type
type cb_2 from commandbutton within w_programmi_manutenzione_ordini
end type
type cb_1 from commandbutton within w_programmi_manutenzione_ordini
end type
type cb_seleziona from commandbutton within w_programmi_manutenzione_ordini
end type
type cb_ricambi from commandbutton within w_programmi_manutenzione_ordini
end type
type cb_report from commandbutton within w_programmi_manutenzione_ordini
end type
type dw_selezione from uo_cs_xx_dw within w_programmi_manutenzione_ordini
end type
end forward

global type w_programmi_manutenzione_ordini from w_cs_xx_principale
integer width = 4773
integer height = 2672
string title = "Creazione Pre-Ordini in PROGEN"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_amministratore cb_amministratore
dw_ricambi dw_ricambi
cb_2 cb_2
cb_1 cb_1
cb_seleziona cb_seleziona
cb_ricambi cb_ricambi
cb_report cb_report
dw_selezione dw_selezione
end type
global w_programmi_manutenzione_ordini w_programmi_manutenzione_ordini

type variables
string is_chiave_root = "HKEY_LOCAL_MACHINE\Software\Consulting&Software\db_esterni\sintexcal\"
string is_path_file_log
n_tran tran_import

boolean ib_modificato = false
end variables

forward prototypes
public function integer wf_registro ()
public function long wf_chiave_ordine (string fs_keyname, string fs_dipartimento, string fs_azienda, ref string fs_errore)
public function integer wf_controlla_porto ()
public function integer wf_invia_mail (string fs_messaggio, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist)
public function integer wf_riga ()
end prototypes

public function integer wf_registro ();string ls_default,ls_dbparm
integer li_risposta


//li_risposta = Registryget(is_chiave_root , "attivazione", ls_default)
//if li_risposta = -1 then 
//	li_risposta = RegistrySet(is_chiave_root , "attivazione", "TRUE")
//	li_risposta = RegistrySet(is_chiave_root + "database", "servername", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "servername", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "logid", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "logpass", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "dbms", "ODBC")
//	li_risposta = RegistrySet(is_chiave_root + "database", "database", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "userid", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "dbpass", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "dbparm", "Connectstring='DSN=cs_import'")
//	li_risposta = RegistrySet(is_chiave_root + "database", "lock", "")
//	li_risposta = RegistrySet(is_chiave_root + "database", "enginetype", "SYBASE_ASA")
//else
//	if upper(ls_default) <> "TRUE" then return 0
//end if

// -----------------  leggo le informazioni della transazione dal registro ----------------------

tran_import = CREATE n_tran
li_risposta = registryget(is_chiave_root+ "database", "servername", tran_import.ServerName)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(is_chiave_root+ "database", "logid", tran_import.LogId)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(is_chiave_root+ "database", "logpass", tran_import.LogPass)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(is_chiave_root+ "database" , "dbms", tran_import.DBMS)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(is_chiave_root+ "database" , "database", tran_import.Database)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(is_chiave_root+ "database" , "userid", tran_import.UserId)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(is_chiave_root+ "database", "dbpass", tran_import.DBPass)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(is_chiave_root+ "database" , "dbparm", ls_dbparm)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

tran_import.DBParm = ls_dbparm

li_risposta = registryget(is_chiave_root+ "database" , "lock", tran_import.Lock)
if li_risposta = -1 then
	g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if
// ----------------------------------------------------------------------------------------------
if f_po_connect(tran_import, true) <> 0 then
   return -1
end if

return 0
end function

public function long wf_chiave_ordine (string fs_keyname, string fs_dipartimento, string fs_azienda, ref string fs_errore);Long			rc
DateTime		ldt_oggi
Decimal		ll_key1, ll_key2, ll_inizio1, ll_fine1, ll_inizio2, ll_fine2, ll_count, ll_key
string      as_dipartimento, as_keyname, as_azienda

ldt_oggi = DateTime(Today(),Now()) 

as_dipartimento = fs_dipartimento
as_azienda = fs_azienda
as_keyname = fs_keyname

SELECT COUNT(*)
INTO   :ll_count
FROM   pgmr.keypools
WHERE  keyname = :as_keyname AND
 		 cddipa  = :as_dipartimento and
		 cdazie = :as_azienda
using  tran_import;

if tran_import.sqlcode < 0 then
	fs_errore = "Errore durante la ricerca della chiave: " + tran_import.sqlerrtext
	return -1
end if
		  
// Se non esiste il Record per la chiave richiesta ritorno un codice errore
if ll_count = 0 then
	if as_keyname = "tkgene" then
		fs_errore = "Attenzione: NON TROVATO IL TOKEN TKGENE!"
		return -1
	end if
	fs_errore = "Attenzione: NON TROVATO IL TOKEN " + as_keyname + "!"
	return -1
end if


UPDATE PGMR.KEYPOOLS
SET    init1   = init1
WHERE  keyname = :as_keyname AND
 		 cddipa  = :as_dipartimento and
		 cdazie = :as_azienda
using  tran_import;

if tran_import.SqlCode <> 0 then
	fs_errore = "Errore durante update init1 = init1 : " + tran_import.sqlerrtext
	return -6
end if

SELECT init1,
		 fine1,
		 init2,
		 fine2
INTO   :ll_inizio1,
  		 :ll_fine1,
		 :ll_inizio2,
		 :ll_fine2
FROM   pgmr.keypools
WHERE  keyname = :as_keyname AND
 		 cddipa  = :as_dipartimento and
		 cdazie = :as_azienda
using  tran_import;

// Se non sono valorizzati gli estremi per la chiave ritorno un errore
if IsNull(ll_inizio1) or IsNull(ll_fine1) or IsNull(ll_inizio2) or IsNull(ll_fine2) then 
	fs_errore = "Non sono valorizzati gli estremi per la chiave!"	
	return -2
end if

ll_key1 = ll_inizio1 + 1
ll_key2 = ll_inizio2 + 1

if ll_key1 > ll_fine1 then ll_key1 = 0
if ll_key2 > ll_fine2 then ll_key2 = 0

// Per nessuna chiave disponibile ritorno un errore
if ll_key1 = 0 AND ll_key2 = 0 then
	fs_errore = "Nessuna chiave disponibile!"
	return -3
end if

// Per tutte e due i valori validi cerco il valore, minore, disponibile per la chiave 
if ll_key1 > 0 AND ll_key2 > 0 then
	if ll_key1 < ll_key2 then
		ll_key = ll_key1
	else
		ll_key = ll_key2
	end if
end if

// Ritorno la chiave valida se l'altra è esaurita
if ll_key1 > 0 AND ll_key2 = 0 then
	ll_key = ll_key1
end if
if ll_key2 > 0 AND ll_key1 = 0 then
	ll_key = ll_key2
end if


// aggiorno la key-pool
CHOOSE CASE ll_key
	CASE ll_key1
		
		UPDATE PGMR.KEYPOOLS
		SET    init1   = :ll_key1,
			    dtulag  = :ldt_oggi
		WHERE  keyname = :as_keyname AND
		 		 cddipa  = :as_dipartimento and
				 cdazie = :as_azienda
		using  tran_import;
		
	CASE ll_key2
		
		UPDATE PGMR.KEYPOOLS
		SET    init2   = :ll_key2,
				 dtulag  = :ldt_oggi
		WHERE  keyname = :as_keyname AND
		 		 cddipa  = :as_dipartimento and
				 cdazie = :as_azienda
		using  tran_import;
		
END CHOOSE

if tran_import.SqlCode = 0 then
//	commit using tran_import;
	RETURN ll_key
else
	fs_errore = "Errore durante l'aggiornamento della key pool: " + tran_import.sqlerrtext
	rollback using tran_import;
	return -4
end if

return	ll_key
end function

public function integer wf_controlla_porto ();long   ll_i
string ls_cod_fornitore, ls_cod_fornitore_old, ls_cod_porto, ls_cod_porto_old, ls_fornitore, ls_genera

ls_cod_fornitore = ""
ls_cod_fornitore_old = ""
ls_cod_porto = ""
ls_cod_porto_old = ""

for ll_i = 1 to dw_ricambi.rowcount()

	ls_cod_fornitore = dw_ricambi.getitemstring( ll_i, "cod_fornitore")
	ls_fornitore = dw_ricambi.getitemstring( ll_i, "fornitore")
	ls_cod_porto = dw_ricambi.getitemstring( ll_i, "porto")
	ls_genera = dw_ricambi.getitemstring( ll_i, "flag_genera")
	if not isnull(ls_genera) and ls_genera = 'S' then
	
		if ls_cod_fornitore <> ls_cod_fornitore_old and ls_cod_fornitore_old <> "" then	
			
			ls_cod_fornitore_old = ls_cod_fornitore
			ls_cod_porto_old = ls_cod_porto
			
		elseif ls_cod_fornitore <> ls_cod_fornitore_old and ls_cod_fornitore_old = "" then
			
			ls_cod_fornitore_old = ls_cod_fornitore
			ls_cod_porto_old = ls_cod_porto
			
		elseif ls_cod_fornitore = ls_cod_fornitore_old then
			
			if ls_cod_porto <> ls_cod_porto_old and ls_cod_porto_old <> "" then
				
				g_mb.messagebox( "OMNIA", "Attenzione: per il fornitore " + ls_fornitore + " esiste una incongruenza fra i porti selezionati!")
				g_mb.messagebox( "OMNIA", "Operazione interrotta!")
				return -1
				
			elseif ls_cod_porto <> ls_cod_porto_old and ls_cod_porto_old = "" then
				
				ls_cod_porto_old = ls_cod_porto
				
			end if
			
		end if
	
	end if
next

return 0
end function

public function integer wf_invia_mail (string fs_messaggio, string fs_cod_tipo_lista_dist, string fs_cod_lista_dist);s_cs_xx.parametri.parametro_s_1 = "L"
s_cs_xx.parametri.parametro_s_2 = fs_cod_tipo_lista_dist
s_cs_xx.parametri.parametro_s_3 = fs_cod_lista_dist
s_cs_xx.parametri.parametro_s_4 = ""
s_cs_xx.parametri.parametro_s_5 = "Creazione preordini in Progen"
s_cs_xx.parametri.parametro_s_6 = fs_messaggio
s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione e-mail della creazione dei preordini?"
s_cs_xx.parametri.parametro_s_8 = ""
s_cs_xx.parametri.parametro_s_9 = ""
s_cs_xx.parametri.parametro_s_10 = ""
s_cs_xx.parametri.parametro_s_11 = ""
s_cs_xx.parametri.parametro_s_12 = ""
s_cs_xx.parametri.parametro_s_13 = ""
s_cs_xx.parametri.parametro_s_14 = ""
s_cs_xx.parametri.parametro_s_15 = "N"
s_cs_xx.parametri.parametro_ul_3 = 0
s_cs_xx.parametri.parametro_b_1 = true
					
openwithparm( w_invio_messaggi, 0)	
		
return 0
end function

public function integer wf_riga ();string	ls_return
long	ll_pos

ls_return = dw_ricambi.getbandatpointer()
ll_pos = pos(ls_return,"~t",1)
if ll_pos < 1 then
	return -1
end if

ls_return = trim(right(ls_return,len(ls_return) - ll_pos))

return long(ls_return)


end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify, ls_flag_collegato
windowobject l_objects[ ]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
								 
iuo_dw_main = dw_selezione


dw_ricambi.dataobject = "d_programmi_manutenzione_grid_ord"
dw_ricambi.SetTransObject( sqlca)


//select flag_collegato
//into   :ls_flag_collegato
//from   utenti
//where  cod_utente = :s_cs_xx.cod_utente;
//
//if sqlca.sqlcode = 0 and not isnull(ls_flag_collegato) and ls_flag_collegato = "S" then
//	cb_amministratore.visible = true
//else
//	cb_amministratore.visible = false
//end if
end event

on w_programmi_manutenzione_ordini.create
int iCurrent
call super::create
this.cb_amministratore=create cb_amministratore
this.dw_ricambi=create dw_ricambi
this.cb_2=create cb_2
this.cb_1=create cb_1
this.cb_seleziona=create cb_seleziona
this.cb_ricambi=create cb_ricambi
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_amministratore
this.Control[iCurrent+2]=this.dw_ricambi
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.cb_seleziona
this.Control[iCurrent+6]=this.cb_ricambi
this.Control[iCurrent+7]=this.cb_report
this.Control[iCurrent+8]=this.dw_selezione
end on

on w_programmi_manutenzione_ordini.destroy
call super::destroy
destroy(this.cb_amministratore)
destroy(this.dw_ricambi)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.cb_seleziona)
destroy(this.cb_ricambi)
destroy(this.cb_report)
destroy(this.dw_selezione)
end on

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_selezione,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale"," des_area ", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cdstab is not null")
					  
f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manut_scollegate","cod_tipo_manut_scollegata"," des_tipo_manut_scollegata ", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_man_program <> 'S' ")		
					  
					  
f_PO_LoadDDDW_DW(dw_ricambi,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","cod_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_ricambi,"porto",sqlca,&
                 "tab_porti","cod_porto","des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")								  
end event

type cb_amministratore from commandbutton within w_programmi_manutenzione_ordini
boolean visible = false
integer x = 4114
integer y = 20
integer width = 594
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva Prezzi Budget"
end type

event clicked;long     ll_i, ll_anno, ll_numero, ll_riga
dec{4}   ld_quantita, ld_prezzo, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_prezzo_netto
datetime ldt_data_consegna
string   ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_porto

dw_ricambi.accepttext()

for ll_i = 1 to dw_ricambi.rowcount()

	ll_anno = dw_ricambi.getitemnumber( ll_i, "anno_registrazione")
	ll_numero = dw_ricambi.getitemnumber( ll_i, "num_registrazione")
	ll_riga = dw_ricambi.getitemnumber( ll_i, "prog_riga")
	ls_cod_prodotto = dw_ricambi.getitemstring( ll_i, "cod_prodotto")
	ls_des_prodotto = dw_ricambi.getitemstring( ll_i, "des_prodotto")
	ls_cod_misura = dw_ricambi.getitemstring( ll_i, "cod_misura")
	ls_porto = dw_ricambi.getitemstring( ll_i, "porto")
	ld_quantita = dw_ricambi.getitemnumber( ll_i, "quantita")
	ld_prezzo = dw_ricambi.getitemnumber( ll_i, "prezzo_unitario")
	ld_sconto_1 = dw_ricambi.getitemnumber( ll_i, "sconto_1")
	ld_sconto_2 = dw_ricambi.getitemnumber( ll_i, "sconto_2")
	ld_sconto_3 = dw_ricambi.getitemnumber( ll_i, "sconto_3")
	ld_prezzo_netto = dw_ricambi.getitemnumber( ll_i, "prezzo_netto")
	
	ldt_data_consegna = dw_ricambi.getitemdatetime( ll_i, "data_consegna")
	
	
	update prog_manutenzioni_ricambi
	set    quan_utilizzo = :ld_quantita,
	       prezzo_ricambio_eff = :ld_prezzo,
			 sconto_1_eff = :ld_sconto_1,
			 sconto_2_eff = :ld_sconto_2,
			 sconto_3_eff = :ld_sconto_3,
			 prezzo_netto_eff = :ld_prezzo_netto,
	       prezzo_ricambio = :ld_prezzo,
			 sconto_1_budget = :ld_sconto_1,
			 sconto_2_budget = :ld_sconto_2,
			 sconto_3_budget = :ld_sconto_3,
			 prezzo_netto_budget = :ld_prezzo_netto,			 
			 data_consegna = :ldt_data_consegna,
			 cod_prodotto = :ls_cod_prodotto,
			 des_prodotto = :ls_des_prodotto,
			 cod_misura = :ls_cod_misura,
			 cod_porto = :ls_porto
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero and
			 prog_riga_ricambio = :ll_riga;
			 
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox( "OMNIA", "Errore durante la modifica della riga " + string(ll_anno) + "/" + string(ll_numero) + "/" + string(ll_riga) + " : " + sqlca.sqlerrtext)
		rollback;
		continue
	end if
	
	commit;
	
next

g_mb.messagebox( "OMNIA", "Procedura Terminata!")
end event

type dw_ricambi from datawindow within w_programmi_manutenzione_ordini
event ue_ordina ( )
integer x = 23
integer y = 480
integer width = 4686
integer height = 1960
integer taborder = 20
string title = "none"
string dataobject = "d_programmi_manutenzione_grid_ord"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_ordina();dw_ricambi.sort()
dw_ricambi.groupcalc()
end event

event itemchanged;string ls_cod_divisione, ls_cod_area_aziendale
dec{4} ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_prezzo_unitario, ld_quantita

ib_modificato = true

choose case dwo.name
	case "prezzo_unitario_budget"
		ib_modificato = true
		
		ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
		ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
		ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
		
		ld_prezzo_unitario = dec(data)
		if ld_sconto_1 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
		end if
		if ld_sconto_2 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
		end if
		if ld_sconto_3 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
		end if
		
		setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)		
		
	case "sconto_1_budget"
		
		ib_modificato = true
		ld_sconto_1 = dec(data)
		ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
		ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
		
		ld_prezzo_unitario = getitemnumber( getrow(), "prezzo_unitario_budget")
		if ld_sconto_1 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
		end if
		if ld_sconto_2 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
		end if
		if ld_sconto_3 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
		end if
		
		setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)
	
	case "sconto_2_budget" 
		
		ib_modificato = true
		ld_sconto_2 = dec(data)
		ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget_budget")
		ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget_budget")
		
		ld_prezzo_unitario = getitemnumber( getrow(), "prezzo_unitario_budget")
		if ld_sconto_1 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
		end if
		if ld_sconto_2 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
		end if
		if ld_sconto_3 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
		end if
		
		setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)
		
	case "sconto_3_budget"
		
		ib_modificato = true
		ld_sconto_3 = dec(data)
		ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
		ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
		
		ld_prezzo_unitario = getitemnumber( getrow(), "prezzo_unitario_budget")
		if ld_sconto_1 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
		end if
		if ld_sconto_2 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
		end if
		if ld_sconto_3 > 0 then
			ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
		end if
		
		setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)		
		
end choose

end event

event doubleclicked;string ls_des_prodotto

if row <= 0 then return 

dw_ricambi.setfocus()
setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_uo_dw_search)
guo_ricerca.uof_ricerca_prodotto(dw_ricambi,"cod_prodotto")	

if not isnull(s_cs_xx.parametri.parametro_s_10)  and s_cs_xx.parametri.parametro_s_10 <> "" then

	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :s_cs_xx.parametri.parametro_s_10;
			 
	setitem( row, "cod_prodotto", s_cs_xx.parametri.parametro_s_10)		 
	setitem( row, "des_prodotto", ls_des_prodotto)		
	
end if
end event

event clicked;long	ll_ret, ll_riga
string	ls_cod_fornitore_old, ls_cod_fornitore_new, ls_rag_soc_fornitore

choose case dwo.name
		
	case "b_fornitore"
		
		ll_riga = wf_riga()
		if ll_riga > 0 then
			
			ls_cod_fornitore_old = dw_ricambi.getitemstring( ll_riga, "cod_fornitore")
			
			dw_ricambi.setrow( ll_riga)
			dw_ricambi.setcolumn( "cod_fornitore")
		
			setnull(s_cs_xx.parametri.parametro_uo_dw_search)
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			setnull(s_cs_xx.parametri.parametro_dw_1)
			//s_cs_xx.parametri.parametro_dw_1 = dw_ricambi
			s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_fornitore(dw_ricambi,"cod_fornitore")
			
			//dw_ricambi.accepttext()
			
			ls_cod_fornitore_new = s_cs_xx.parametri.parametro_s_1//dw_ricambi.getitemstring( ll_riga, "cod_fornitore")
			
			if ls_cod_fornitore_old <> ls_cod_fornitore_new and not isnull(ls_cod_fornitore_new) and ls_cod_fornitore_new <> "" and ls_cod_fornitore_new <> "cod_fornitore" then
				
//				dw_ricambi.setitem( ll_riga, "cod_fornitore", ls_cod_fornitore_new)
//				dw_ricambi.setitem( ll_riga, "fornitore", ls_rag_soc_fornitore)
				
				select rag_soc_1
				into   :ls_rag_soc_fornitore
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_fornitore = :ls_cod_fornitore_new;
	
				for ll_ret = 1 to dw_ricambi.rowcount()
					if dw_ricambi.getitemstring( ll_ret, "cod_fornitore") = ls_cod_fornitore_old then
						dw_ricambi.setitem( ll_ret, "cod_fornitore", ls_cod_fornitore_new)
						dw_ricambi.setitem( ll_ret, "fornitore", ls_rag_soc_fornitore)
					end if
				next
				
				dw_ricambi.SetRedraw(false)
				dw_ricambi.setSort(" fornitore A, attrezzatura A, anno_registrazione A, num_registrazione A, prog_riga A ")
				dw_ricambi.Sort()
				dw_ricambi.GroupCalc()
				dw_ricambi.SetRedraw(true)
			end if
			
		end if		
		
end choose
end event

type cb_2 from commandbutton within w_programmi_manutenzione_ordini
boolean visible = false
integer x = 4320
integer y = 380
integer width = 389
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long     ll_i, ll_anno, ll_numero, ll_riga
dec{4}   ld_quantita, ld_prezzo, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_prezzo_netto
datetime ldt_data_consegna
string   ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_porto, ls_des_estesa

dw_ricambi.accepttext()

for ll_i = 1 to dw_ricambi.rowcount()

	ll_anno = dw_ricambi.getitemnumber( ll_i, "anno_registrazione")
	ll_numero = dw_ricambi.getitemnumber( ll_i, "num_registrazione")
	ll_riga = dw_ricambi.getitemnumber( ll_i, "prog_riga")
	ls_cod_prodotto = dw_ricambi.getitemstring( ll_i, "cod_prodotto")
	ls_des_prodotto = dw_ricambi.getitemstring( ll_i, "des_prodotto")
	ls_cod_misura = dw_ricambi.getitemstring( ll_i, "cod_misura")
	ls_porto = dw_ricambi.getitemstring( ll_i, "porto")
	ld_quantita = dw_ricambi.getitemnumber( ll_i, "quantita")
	ld_prezzo = dw_ricambi.getitemnumber( ll_i, "prezzo_unitario_budget")
	ld_sconto_1 = dw_ricambi.getitemnumber( ll_i, "sconto_1_budget")
	ld_sconto_2 = dw_ricambi.getitemnumber( ll_i, "sconto_2_budget")
	ld_sconto_3 = dw_ricambi.getitemnumber( ll_i, "sconto_3_budget")
	ld_prezzo_netto = dw_ricambi.getitemnumber( ll_i, "prezzo_netto_budget")
	
	ldt_data_consegna = dw_ricambi.getitemdatetime( ll_i, "data_consegna")
	ls_des_estesa = dw_ricambi.getitemstring( ll_i, "des_estesa")
	
	update prog_manutenzioni_ricambi
	set    quan_utilizzo = :ld_quantita,
	       prezzo_ricambio_eff = :ld_prezzo,
			 sconto_1_eff = :ld_sconto_1,
			 sconto_2_eff = :ld_sconto_2,
			 sconto_3_eff = :ld_sconto_3,
			 prezzo_netto_eff = :ld_prezzo_netto,
			 data_consegna = :ldt_data_consegna,
			 cod_prodotto = :ls_cod_prodotto,
			 des_prodotto = :ls_des_prodotto,
			 cod_misura = :ls_cod_misura,
			 cod_porto = :ls_porto,
			 des_estesa = :ls_des_estesa
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero and
			 prog_riga_ricambio = :ll_riga;
			 
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox( "OMNIA", "Errore durante la modifica della riga " + string(ll_anno) + "/" + string(ll_numero) + "/" + string(ll_riga) + " : " + sqlca.sqlerrtext)
		rollback;
		continue
	end if
	
	commit;
	
next

g_mb.messagebox( "OMNIA", "Procedura Terminata!")
end event

type cb_1 from commandbutton within w_programmi_manutenzione_ordini
integer x = 434
integer y = 2460
integer width = 389
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Deseleziona"
end type

event clicked;long ll_i

for ll_i = 1 to dw_ricambi.rowcount()
	dw_ricambi.setitem( ll_i, "flag_genera", "N")
next
end event

type cb_seleziona from commandbutton within w_programmi_manutenzione_ordini
integer x = 23
integer y = 2460
integer width = 389
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Seleziona"
end type

event clicked;long ll_i

for ll_i = 1 to dw_ricambi.rowcount()
	dw_ricambi.setitem( ll_i, "flag_genera", "S")
next
end event

type cb_ricambi from commandbutton within w_programmi_manutenzione_ordini
integer x = 3200
integer y = 360
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_cod_fornitore, ls_cod_prodotto, ls_des_prodotto, ls_cod_attrezzatura, ls_des_attrezzatura, ls_cod_area_aziendale, ls_cod_misura, ls_cod_fornitore_cu, &
       ls_cod_reparto_att, ls_cod_cat_attrezzature, ls_des_cat, ls_cod_tipo_manutenzione, ls_flag_capitalizzazione, ls_cod_tipo_manut_attivita, ls_note_idl, &
		 ls_cod_porto, ls_prefisso, ls_cod_attrezzatura_p, ls_descrizione_p, ls_des_porto, ls_rag_soc_fornitore, ls_cod_attrezzatura_sel, &
		 ls_cod_tipo_manutenzione_sel, ls_cod_attrezzatura_scelta, ls_des_estesa, ls_flag_budget
		 
long   ll_anno, ll_numero, ll_prog, ll_riga, ll_count
dec{4} ld_quantita, ld_prezzo_ricambio, ld_sconto_1_budget, ld_sconto_2_budget,ld_sconto_3_budget,ld_prezzo_netto_budget,ld_sconto_1_eff,ld_sconto_2_eff,ld_sconto_3_eff,ld_prezzo_ricambio_eff, &
		 ld_prezzo_netto_eff, ld_quan_effettiva
datetime ldt_consegna, ldt_data_consegna_sel, ldt_data_nulla

dw_ricambi.setredraw( false)

dw_selezione.accepttext()

ib_modificato = false
ls_cod_fornitore = dw_selezione.getitemstring( 1, "cod_fornitore")
ls_cod_area_aziendale = dw_selezione.getitemstring( 1, "cod_area_aziendale")
ls_cod_tipo_manutenzione = dw_selezione.getitemstring( 1, "cod_tipo_manutenzione")
ls_flag_capitalizzazione = dw_selezione.getitemstring( 1, "flag_capitalizzazioni")
ls_cod_attrezzatura_sel = dw_selezione.getitemstring( 1, "cod_attrezzatura")
ldt_data_consegna_sel = dw_selezione.getitemdatetime( 1, "data_consegna")

if isnull(ldt_data_consegna_sel) or string(ldt_data_consegna_sel,"dd/mm/yyyy") = "01/01/1900" then
	ldt_data_nulla = datetime( date("01/01/1901"), 00:00:00)
	ldt_data_consegna_sel = datetime( date("01/01/1901"), 00:00:00)
end if

if isnull(ls_cod_area_aziendale) or ls_cod_area_aziendale = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: selezionare un'area aziendale!")
	return -1
end if

if isnull(ls_cod_fornitore) or ls_cod_fornitore = "" then
	ls_cod_fornitore = "%"
end if

if isnull(ls_flag_capitalizzazione) or ls_flag_capitalizzazione = "" then
	ls_flag_capitalizzazione = "%"
end if		

if isnull(ls_cod_attrezzatura_sel) or ls_cod_attrezzatura_sel = "" then
	ls_cod_attrezzatura_sel = "%"
else
	ls_cod_attrezzatura_sel = left( ls_cod_attrezzatura_sel, 3) + "%"
end if


dw_ricambi.reset()
setpointer(hourglass!)

declare cu_ricambi cursor for
select prog_manutenzioni_ricambi.anno_registrazione,   
       prog_manutenzioni_ricambi.num_registrazione,   
       prog_manutenzioni_ricambi.prog_riga_ricambio,   
       prog_manutenzioni_ricambi.cod_prodotto,   
       prog_manutenzioni_ricambi.des_prodotto, 
		 prog_manutenzioni_ricambi.cod_fornitore,
       prog_manutenzioni_ricambi.quan_utilizzo,   
       prog_manutenzioni_ricambi.prezzo_ricambio ,
		 prog_manutenzioni_ricambi.cod_misura,
		 prog_manutenzioni_ricambi.data_consegna,
		 prog_manutenzioni_ricambi.cod_porto,		 
		 prog_manutenzioni_ricambi.sconto_1_budget,
		 prog_manutenzioni_ricambi.sconto_2_budget,
		 prog_manutenzioni_ricambi.sconto_3_budget,
		 prog_manutenzioni_ricambi.prezzo_netto_budget,
		 prog_manutenzioni_ricambi.quan_eff,
		 prog_manutenzioni_ricambi.sconto_1_eff,
		 prog_manutenzioni_ricambi.sconto_2_eff,
		 prog_manutenzioni_ricambi.sconto_3_eff,
		 prog_manutenzioni_ricambi.prezzo_ricambio_eff,
		 prog_manutenzioni_ricambi.prezzo_netto_eff,
		 programmi_manutenzione.cod_attrezzatura,
		 prog_manutenzioni_ricambi.des_estesa,
		 prog_manutenzioni_ricambi.flag_budget
FROM   prog_manutenzioni_ricambi LEFT OUTER JOIN programmi_manutenzione ON prog_manutenzioni_ricambi.cod_azienda = programmi_manutenzione.cod_azienda AND prog_manutenzioni_ricambi.anno_registrazione = programmi_manutenzione.anno_registrazione AND prog_manutenzioni_ricambi.num_registrazione = programmi_manutenzione.num_registrazione  
WHERE ( prog_manutenzioni_ricambi.cod_azienda = :s_cs_xx.cod_azienda ) AND  
      ( programmi_manutenzione.cod_attrezzatura like :ls_cod_attrezzatura_sel ) AND  
      ( prog_manutenzioni_ricambi.tkordi is NULL ) AND  
      ( prog_manutenzioni_ricambi.tkposi is NULL ) AND  
      ( prog_manutenzioni_ricambi.nrposi is NULL ) AND
		( prog_manutenzioni_ricambi.cod_fornitore like :ls_cod_fornitore ) and
		( prog_manutenzioni_ricambi.flag_capitalizzato like :ls_flag_capitalizzazione ) and
		( prog_manutenzioni_ricambi.data_consegna = :ldt_data_consegna_sel or :ldt_data_consegna_sel = :ldt_data_nulla) and
		( prog_manutenzioni_ricambi.flag_rda = 'N' or prog_manutenzioni_ricambi.flag_rda is null ) and
	( programmi_manutenzione.cod_tipo_manutenzione NOT IN ( select cod_tipo_manut_collegata from tab_tipi_manut_scollegate where cod_azienda = :s_cs_xx.cod_azienda and flag_tipo_man_program = 'S' ) )
order  by prog_manutenzioni_ricambi.prog_riga_ricambio asc;
		 
open cu_ricambi;
if sqlca.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore: " + sqlca.sqlerrtext)
	return -1	
end if

do while true 
	fetch cu_ricambi into :ll_anno,
	                      :ll_numero,
								 :ll_prog,
								 :ls_cod_prodotto,
								 :ls_des_prodotto,
								 :ls_cod_fornitore_cu,
								 :ld_quantita,
								 :ld_prezzo_ricambio,
								 :ls_cod_misura,
								 :ldt_consegna,
								 :ls_cod_porto,
		 						 :ld_sconto_1_budget,
		                   		  :ld_sconto_2_budget,
		 						 :ld_sconto_3_budget,
								 :ld_prezzo_netto_budget,
								 :ld_quan_effettiva,
								 :ld_sconto_1_eff,
								 :ld_sconto_2_eff,
								 :ld_sconto_3_eff,
								 :ld_prezzo_ricambio_eff,
								 :ld_prezzo_netto_eff,
								 :ls_cod_attrezzatura,
								 :ls_des_estesa,
								 :ls_flag_budget;
	if sqlca.sqlcode <> 0 then exit;
	
	//	***	se esiste un buono d'ingresso continuo
	
	ll_count = 0
	
	select count(*)
	into 	:ll_count
	from	prog_manutenzioni_ric_ddi
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno and
			num_registrazione = :ll_numero and
			prog_riga_ricambio = :ll_prog;
			
	if not isnull(ll_count) and ll_count > 0 then continue
	
	ll_count = 0
	
	if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" and ls_cod_attrezzatura_sel = "%" then
		
		ll_count = 0
		
		select count(cod_attrezzatura)
		into   :ll_count
		from   programmi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero and
				 cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = :s_cs_xx.cod_azienda and cod_area_aziendale = :ls_cod_area_aziendale);
				 
		if isnull(ll_count) or ll_count = 0 then continue			 
		
	end if
	
	if not isnull(ls_cod_attrezzatura_sel) and ls_cod_attrezzatura_sel <> "" and len(ls_cod_attrezzatura_sel) > 3 then
		
		ll_count = 0
		
		select count(cod_attrezzatura)
		into   :ll_count
		from   anag_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_attrezzatura = :ls_cod_attrezzatura and
				 cod_reparto = 'M';
				 
		if isnull(ll_count) or ll_count = 0 then continue			 
		
	end if	
	
	if isnull(ls_cod_fornitore_cu) or ls_cod_fornitore_cu = "" then continue
	
	select rag_soc_1
	into   :ls_rag_soc_fornitore
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_fornitore = :ls_cod_fornitore_cu;
	
	if isnull(ls_rag_soc_fornitore) then ls_rag_soc_fornitore = ""

	setnull(ls_cod_tipo_manut_attivita)
	setnull(ls_note_idl)

	select cod_tipo_manutenzione,
	       note_idl
	into   :ls_cod_tipo_manut_attivita,
	       :ls_note_idl
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero;
			 
	if not isnull(ls_cod_tipo_manutenzione) and ls_cod_tipo_manutenzione <> "" then
		if ls_cod_tipo_manut_attivita <> ls_cod_tipo_manutenzione then continue
	end if
	
	select cod_attrezzatura
	into   :ls_cod_attrezzatura
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero;
			 
	select descrizione
	into   :ls_des_attrezzatura
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura;
			 
	ls_prefisso = left( ls_cod_attrezzatura, 3) + "%"
	
	select cod_attrezzatura,
	       descrizione
	into   :ls_cod_attrezzatura_p,
	       :ls_descrizione_p
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_aziendale = :ls_cod_area_aziendale and
			 flag_primario = 'S' and
			 cod_attrezzatura like :ls_prefisso;
			 
	if sqlca.sqlcode = 100 then
		
		select cod_attrezzatura,
				 descrizione
		into   :ls_cod_attrezzatura_p,
				 :ls_descrizione_p
		from   anag_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 flag_primario = 'S' and
				 cod_attrezzatura like :ls_prefisso;		
				 
	end if	
	
	if isnull(ls_flag_budget) then ls_flag_budget = "N"
			 
	ll_riga = dw_ricambi.insertrow(0)
	dw_ricambi.setitem( ll_riga, "cod_fornitore", ls_cod_fornitore_cu)
	dw_ricambi.setitem( ll_riga, "fornitore", ls_rag_soc_fornitore)
	dw_ricambi.setitem( ll_riga, "cod_prodotto", ls_cod_prodotto)
	dw_ricambi.setitem( ll_riga, "des_prodotto", ls_des_prodotto)
	dw_ricambi.setitem( ll_riga, "anno_registrazione", ll_anno)
	dw_ricambi.setitem( ll_riga, "num_registrazione", ll_numero)
	dw_ricambi.setitem( ll_riga, "prog_riga", ll_prog)	
	dw_ricambi.setitem( ll_riga, "cod_misura", ls_cod_misura)
	dw_ricambi.setitem( ll_riga, "data_consegna", ldt_consegna)
	dw_ricambi.setitem( ll_riga, "cod_tipo_manutenzione", ls_cod_tipo_manut_attivita)
	dw_ricambi.setitem( ll_riga, "des_tipo_manutenzione", ls_note_idl)
	dw_ricambi.setitem( ll_riga, "flag_genera", "S")
	dw_ricambi.setitem( ll_riga, "attrezzatura", ls_cod_attrezzatura + " " + ls_des_attrezzatura)
	dw_ricambi.setitem( ll_riga, "porto", ls_cod_porto)
	dw_ricambi.setitem( ll_riga, "cod_attrezzatura_prim", ls_cod_attrezzatura_p + " - " + ls_descrizione_p)	
	dw_ricambi.setitem( ll_riga, "flag_budget", ls_flag_budget)

	if ls_flag_budget = "N" then

	//	dw_ricambi.setitem( ll_riga, "sconto_1", ld_sconto_1_eff)
	//	dw_ricambi.setitem( ll_riga, "sconto_2", ld_sconto_2_eff)
	//	dw_ricambi.setitem( ll_riga, "sconto_3", ld_sconto_3_eff)
	//	dw_ricambi.setitem( ll_riga, "prezzo_unitario", ld_prezzo_ricambio_eff)
	//	dw_ricambi.setitem( ll_riga, "prezzo_netto", ld_prezzo_netto_eff)	
	
		dw_ricambi.setitem( ll_riga, "sconto_1_budget", ld_sconto_1_eff)
		dw_ricambi.setitem( ll_riga, "sconto_2_budget", ld_sconto_2_eff)
		dw_ricambi.setitem( ll_riga, "sconto_3_budget", ld_sconto_3_eff)
		dw_ricambi.setitem( ll_riga, "prezzo_unitario_budget", ld_prezzo_ricambio_eff)
		dw_ricambi.setitem( ll_riga, "prezzo_netto_budget", ld_prezzo_netto_eff)
		dw_ricambi.setitem( ll_riga, "quantita", ld_quan_effettiva)
	
	else
		
		dw_ricambi.setitem( ll_riga, "sconto_1_budget", ld_sconto_1_budget)
		dw_ricambi.setitem( ll_riga, "sconto_2_budget", ld_sconto_2_budget)
		dw_ricambi.setitem( ll_riga, "sconto_3_budget", ld_sconto_3_budget)
		dw_ricambi.setitem( ll_riga, "prezzo_unitario_budget", ld_prezzo_ricambio)
		dw_ricambi.setitem( ll_riga, "prezzo_netto_budget", ld_prezzo_netto_budget)
		dw_ricambi.setitem( ll_riga, "quantita", ld_quantita)
		
	end if
	
	dw_ricambi.setitem( ll_riga, "des_estesa", ls_des_estesa)
loop								 

close cu_ricambi;
if sqlca.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore: " + sqlca.sqlerrtext)
	return -1	
end if

dw_ricambi.setSort(" fornitore A, attrezzatura A, anno_registrazione A, num_registrazione A, prog_riga A ")
dw_ricambi.Sort()
dw_ricambi.GroupCalc()
dw_ricambi.SetRedraw(true)

setpointer(arrow!)
cb_report.enabled = true
cb_2.enabled = true
return 0
end event

type cb_report from commandbutton within w_programmi_manutenzione_ordini
integer x = 3909
integer y = 2460
integer width = 800
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Pre-Ordine"
end type

event clicked;string ls_cod_prodotto, ls_des_prodotto, ls_errore, ls_cdtorac, ls_chiave, ls_tkforn, ls_cod_fornitore, ls_cod_azienda, ls_pgcodi, ls_dpcodi, ls_cddpag,ls_ffinme, &
       ls_cdbanc, ls_cdrmpo, ls_cdtrme, ls_cdente, ls_cdunil, ls_cdenul, ls_cdusul, ls_cdunil_m, ls_dsunil, ls_indiri, ls_cddipa, ls_cod_area_aziendale, &
		 ls_cdstab, ls_cdarti, ls_cod_misura, ls_cdtolc, ls_1, ls_porto, ls_2, ls_cod_misura_2, ls_cod_fornitore_old, ls_genera, ls_ordine_progen, ls_cdstab_m, &
		 ls_lista_ordini, ls_testo, ls_des_area, ls_des_azienda, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_des_estesa, ls_rag_soc_fornitore, ls_cod_iva_progen
		 
long   ll_ret, ll_chiave, ll_anno, ll_anno_ordine, ll_num_ordine, ll_chiave_testata, ll_i, ll_numero, ll_prog_riga, ll_chiave_2, ll_chiave_pos, ll_prog_ordine_progen, &
		 ll_ret_modifiche
datetime ldt_data, ldt_oggi, ldt_consegna, ldt_data_consegna
dec{4} ld_prezzo, ld_quantita, ld_sconto_3, ld_sconto_2, ld_sconto_1, ld_prezzo_netto


//############################################################
//12/06/2012 Donato leggo il cod iva da passare a progen dalla tabella parametri (CIP)
uo_progen_documenti luo_progen
luo_progen = create uo_progen_documenti
ls_cod_iva_progen = luo_progen.uof_cdiva_progen()
destroy luo_progen
//############################################################

if ib_modificato then
	
	ll_ret_modifiche = g_mb.messagebox( "OMNIA", "Salvare le modifiche?", Exclamation!, OKCancel!, 2)
	if ll_ret_modifiche <> 1 then
		g_mb.messagebox( "OMNIA", "Operazione annullata!")
		return -1		
	end if
	
end if

ll_ret = g_mb.messagebox( "OMNIA", "Continuare con la creazione degli ordini?", Exclamation!, OKCancel!, 2)

if ll_ret = 2 then
	g_mb.messagebox( "OMNIA", "Operazione annullata!")
	return -1
end if

dw_selezione.accepttext()
dw_ricambi.accepttext()
for ll_i = 1 to dw_ricambi.rowcount()

	ls_genera = dw_ricambi.getitemstring( ll_i, "flag_genera")
	if not isnull(ls_genera) and ls_genera = "S" then
		
		ldt_data_consegna = dw_ricambi.getitemdatetime( ll_i, "data_consegna")
		if isnull(ldt_data_consegna) then
			
			g_mb.messagebox("OMNIA", "Attenzione:controllare le date di consegna delle righe selezionate!")
			return -1
			
		end if
		
	end if
	
next

ll_ret = wf_controlla_porto()
if ll_ret < 0 then return -1

setpointer(hourglass!)

ldt_oggi = datetime( date(today()), time(now()))

ls_cod_area_aziendale = dw_selezione.getitemstring( 1, "cod_area_aziendale")
ldt_data = dw_selezione.getitemdatetime( 1, "data_importazione")	

select stringa
into   :ls_cdtorac
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'TOP';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: impostare il parametro TOP come tipo ordine acquisto per progen nella tabella parametri_azienda!")
	return -1
end if

select stringa
into   :ls_cdtrme
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CDM';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: impostare il parametro CDM come tipo ordine acquisto per progen nella tabella parametri_azienda!")
	return -1
end if

select stringa
into   :ls_cdtolc
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CTC';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: impostare il parametro CTC come tipo ordine acquisto per progen nella tabella parametri_azienda!")
	return -1
end if

select stringa
into   :ls_cod_azienda
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'AZP';
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore in lettura parametro aziendale AZP (azienda PROGEN)")
	return -1
end if

select cdstab,
       des_area
into   :ls_cdstab_m,
       :ls_des_area
from   tab_aree_aziendali 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_area_aziendale = :ls_cod_area_aziendale;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca di cdstab in anagrafica aree_aziendali per l'area " + ls_cod_area_aziendale + " : " + sqlca.sqlerrtext)
	return -1
end if

select rag_soc_1
into   :ls_des_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

// ******************		CONNESSIONE A SINTEX

ll_ret = wf_registro()

SELECT  pgmr.stabilimento.cdstab,
        pgmr.stabilimento.via
into    :ls_cdstab,
        :ls_indiri
FROM    pgmr.stabilimento
WHERE   pgmr.stabilimento.cdazie = :ls_cod_azienda and
		  pgmr.stabilimento.cdstab_m = :ls_cdstab_m
using   tran_import;

if tran_import.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox( "OMNIA", "1.Errore durante la ricerca dati stabilimento: " + tran_import.sqlerrtext)
	disconnect using tran_import;
	destroy tran_import;
	return -1
end if

// ******************		INIZIO GIA' A SCORRERE

ls_cod_fornitore_old = ""
ll_prog_ordine_progen = 0
ls_lista_ordini = ""
ls_ordine_progen = ""
ls_testo = ""

for ll_i = 1 to dw_ricambi.rowcount()
	
	ls_cod_prodotto = dw_ricambi.getitemstring( ll_i, "cod_prodotto")
	ls_des_prodotto = left(dw_ricambi.getitemstring( ll_i, "des_prodotto"),40)
	ls_cod_misura = dw_ricambi.getitemstring( ll_i, "cod_misura")
	ld_prezzo = dw_ricambi.getitemnumber( ll_i, "prezzo_unitario_budget")
	ld_prezzo_netto = dw_ricambi.getitemnumber( ll_i, "prezzo_netto_budget")
	ld_quantita = dw_ricambi.getitemnumber( ll_i, "quantita")
	ll_anno = dw_ricambi.getitemnumber( ll_i, "anno_registrazione")
	ll_numero = dw_ricambi.getitemnumber( ll_i, "num_registrazione")
	ll_prog_riga = dw_ricambi.getitemnumber( ll_i, "prog_riga")
	ldt_consegna = dw_ricambi.getitemdatetime( ll_i, "data_consegna")
	ls_cod_fornitore = dw_ricambi.getitemstring( ll_i, "cod_fornitore")
	ls_genera = dw_ricambi.getitemstring( ll_i, "flag_genera")
	ls_porto = dw_ricambi.getitemstring( ll_i, "porto")
	ld_sconto_1 = dw_ricambi.getitemnumber( ll_i, "sconto_1_budget")
	ld_sconto_2 = dw_ricambi.getitemnumber( ll_i, "sconto_2_budget")
	ld_sconto_3 = dw_ricambi.getitemnumber( ll_i, "sconto_3_budget")
	ls_des_estesa = dw_ricambi.getitemstring( ll_i, "des_estesa")	
			 
	if isnull(ls_genera) or ls_genera = "N" then continue
	
	setnull(ls_cdrmpo)
	
	select cod_porto_progen
	into   :ls_cdrmpo
	from   tab_porti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_porto = :ls_porto;	
	
	if sqlca.sqlcode <> 0 or isnull(ls_cdrmpo) or ls_cdrmpo = "" then
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback using sqlca;
		g_mb.messagebox( "OMNIA", "Attenzione: impostare il codice progen per il porto:" + ls_porto)
		return -1	
	end if	
	
	// CONTROLLO PRODOTTO
	select pgmr.mrp_arch_articoli.cdarti
	into   :ls_cdarti
	from   pgmr.mrp_arch_articoli
	where  pgmr.mrp_arch_articoli.cdartm = :ls_cod_prodotto and
			 pgmr.mrp_arch_articoli.cdazie = :ls_cod_azienda
	using  tran_import;
	
	if tran_import.sqlcode <> 0 or isnull(ls_cdarti) or ls_cdarti = "" then
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback using sqlca;
		g_mb.messagebox( "OMNIA", "Attenzione: impostare il codice prodotto progen per il prodotto:" + ls_cod_prodotto)
		return -1	
	end if		
	

	if ls_cod_fornitore_old <> ls_cod_fornitore then 		// *** LEGGO I DATI DEL FORNITORE
	
		select tkforn,
				 rag_soc_1
		into   :ls_tkforn,
		       :ls_rag_soc_fornitore
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
				 
		if sqlca.sqlcode <> 0 or isnull(ls_tkforn) or ls_tkforn = "" then
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			g_mb.messagebox( "OMNIA", "Attenzione: impostare il codice tkforn per il fornitore selezionato!")
			return -1	
		end if	
		
		// **** 	dati del fornitore
		
		SELECT  pgmr.archforn.pgcodi ,
				  pgmr.archforn.dpcodi ,
				  pgmr.archforn.cddpag ,
				  pgmr.archforn.ffinme ,
				  pgmr.archforn.cdbanc
		into    :ls_pgcodi,
				  :ls_dpcodi,
				  :ls_cddpag,
				  :ls_ffinme,
				  :ls_cdbanc
		FROM    pgmr.archforn 
		WHERE   pgmr.archforn.cdazie = :ls_cod_azienda and
				  pgmr.archforn.tkforn = :ls_tkforn
		using   tran_import;
		
		if tran_import.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox( "OMNIA", "Errore durante la ricerca dati fornitore: " + tran_import.sqlerrtext)
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			return -1
		end if
		
		select pgmr.archforn.cdente
		into   :ls_cdente
		from   pgmr.archforn
		where  pgmr.archforn.cdazie = :ls_cod_azienda and
		       pgmr.archforn.tkforn = :ls_tkforn
		using  tran_import;
		
		if tran_import.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox( "OMNIA", "2.Errore durante la ricerca ente fornitore: " + tran_import.sqlerrtext)
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			return -1
		end if	
		
		SELECT pgmr.enteuniloc.cdunil ,
				 pgmr.enteuniloc.cdusul
		into   :ls_cdunil,
				 :ls_cdusul		 
		FROM   pgmr.enteuniloc
		WHERE (  pgmr.enteuniloc.cdente = :ls_cdente ) and          
				( pgmr.enteuniloc.cdusul = '0000000015' )
		using  tran_import;	
				
		if tran_import.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox( "OMNIA", "3.Errore durante la ricerca dati fornitore: " + tran_import.sqlerrtext)
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			return -1
		end if					
				
		SELECT pgmr.unitalocali.cdunil ,
				 pgmr.unitalocali.cdunil_m ,
				 pgmr.unitalocali.dsunil ,
				 pgmr.unitalocali.cddipa
		into   :ls_cdunil ,
				 :ls_cdunil_m ,
				 :ls_dsunil ,
				 :ls_cddipa				 
		FROM   pgmr.unitalocali
		WHERE ( pgmr.unitalocali.cdunil = :ls_cdunil )
		using  tran_import;
		
		if tran_import.sqlcode <> 0 then
			setpointer(arrow!)
			g_mb.messagebox( "OMNIA", "2.Errore durante la ricerca dati fornitore: " + tran_import.sqlerrtext)
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			return -1
		end if	
		
		//	***	TOKEN DI POSIZIONE TESTATA PER IL NUOVO FORNITORE
		
		ll_chiave_testata = wf_chiave_ordine("TKGENE", "01", "01", ls_errore)
		if ll_chiave < 0 then
			setpointer(arrow!)
			g_mb.messagebox( "OMNIA", ls_errore)
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			g_mb.messagebox( "OMNIA", "Operazione interrotta!")
			return -1
		end if

		ls_chiave = "NUMORD_A_" + string(year(today()))
		ll_anno_ordine = year(today())
		
		//--------------------------------------------------------------------------------------------------------------------------------------------
		//Donato 18/03/2013 anomalia segnala da marvelli
		//deve cercare rispetto alla transcodifica CDSTAB su progen e non rispetto al cod_area_aziendale di OMNIA
		ll_num_ordine = wf_chiave_ordine( ls_chiave, ls_cdstab_m, ls_cod_azienda, ls_errore)
		//ll_num_ordine = wf_chiave_ordine( ls_chiave, ls_cod_area_aziendale, ls_cod_azienda, ls_errore)
		//FINE MODIFICA ------------------------------------------------------------------------------------------------------------------------
		
		if ll_num_ordine < 0 then
			setpointer(arrow!)
			g_mb.messagebox( "OMNIA", ls_errore)
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			g_mb.messagebox( "OMNIA", "Operazione interrotta!")
			return -1
		end if
		 

		// ***			INSERIMENTO TESTATA
		INSERT INTO pgmr.ordacq_test(tkordi,
											  cdtorac,
											  anno,
											  numord,
											  dtordi,
											  cdriff,
											  dtrifo,
											  cdnsri,
											  dtnsri,
											  tkforn,
											  cdentf,
											  cdulio,
											  vacodi,
											  cdfisc,
											  cdiva,
											  pgcodi,
											  dpcodi,
											  cddpag,
											  ffinme,
											  scocas,
											  scrap1,
											  scrap2,
											  tkbanr,
											  cdrmpo,
											  cdulrm,
											  dsulrm,
											  cdtrme,
											  cdvett,
											  cdentv,
											  cdulve,
											  cstrkg,
											  faaics,
											  fstord,
											  fstcor,
											  adspds_1,
											  adspim_1,
											  adspds_2,
											  adspim_2,
											  oneacc,
											  cdazie,
											  cddipa,
											  profil,
											  dtinse,
											  dtulag,
											  cambio,
											  tkordi_link,
											  cdordi_link,
											  cdstab,
											  cdautome,
											  fevaso)
							VALUES       (:ll_chiave_testata,
											  :ls_cdtorac,
											  :ll_anno_ordine,
											  :ll_num_ordine,
											  :ldt_data,
											  NULL,
											  NULL,
											  NULL,
											  NULL,
											  :ls_tkforn,
											  :ls_cdente,
											  :ls_cdunil,
											  '0000000003',
											  '0000000002',
											  :ls_cod_iva_progen,
											  :ls_pgcodi,
											  :ls_dpcodi,
											  :ls_cddpag,
											  :ls_ffinme,
											  0,
											  0,
											  0,
											  :ls_cdbanc,
											  :ls_cdrmpo,
											  null,
											  :ls_indiri,
											  :ls_cdtrme,
											  null,
											  null,
											  null,
											  0,
											  'N',
											  'S',
											  'N',
											  null,
											  0,
											  null,
											  0,
											  0,
											  :ls_cod_azienda,
											  :ls_cdstab_m,						//:ls_cod_area_aziendale,
											  :s_cs_xx.cod_utente,
											  :ldt_oggi,
											  :ldt_oggi,
											  1,
											  null,
											  null,
											  :ls_cdstab,
											  null,
											  'N')
						using   tran_import; 
						
		if tran_import.sqlcode < 0 then
			setpointer(arrow!)
			g_mb.messagebox( "OMNIA", "Errore durante l'inserimento della testata:" + tran_import.sqlerrtext)
			rollback using tran_import;
			disconnect using tran_import;
			destroy tran_import;
			rollback using sqlca;
			g_mb.messagebox( "OMNIA", "Operazione interrotta!")
			return -1
		end if	

		ll_prog_ordine_progen = 0
		ls_cod_fornitore_old = ls_cod_fornitore
		
		if isnull(ls_rag_soc_fornitore) then ls_rag_soc_fornitore = ""
		
		ls_lista_ordini += "Il preordine " + string(ll_anno_ordine) + "/" + string(ll_num_ordine) + " è stato creato correttamente!~r~n"
		ls_testo += "E' stato creato il preordine " + string(ll_anno_ordine) + "/" + string(ll_num_ordine) + " dell'area " + ls_des_area + " dell'azienda " + ls_des_azienda + " per il fornitore " + ls_cod_fornitore + " " + ls_rag_soc_fornitore
		
	end if

	// *** POSIZIONI
	
	ll_prog_ordine_progen ++
	
	// ***		CHIAVE POSIZIONE PER IL NUOVO ORDINE DEL NUOVO FORNITORE
	
	ll_chiave_pos = wf_chiave_ordine("TKGENE", "01", "01", ls_errore)
	if ll_chiave_pos < 0 then
		setpointer(arrow!)
		g_mb.messagebox( "OMNIA", ls_errore)
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback using sqlca;
		g_mb.messagebox( "OMNIA", "Operazione interrotta!")
		return -1
	end if	

	// ***		CHIAVE 1 CONSEGNA
	
	ll_chiave = wf_chiave_ordine("TKGENE", "01", "01", ls_errore)
	if ll_chiave < 0 then
		setpointer(arrow!)
		g_mb.messagebox( "OMNIA", ls_errore)
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback using sqlca;
		g_mb.messagebox( "OMNIA", "Operazione interrotta!")
		return -1
	end if	

	// ***		CHIAVE 2 CONSEGNA
	
	ll_chiave_2 = wf_chiave_ordine("TKGENE", "01", "01", ls_errore)
	if ll_chiave_2 < 0 then
		setpointer(arrow!)
		g_mb.messagebox( "OMNIA", ls_errore)
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback using sqlca;
		g_mb.messagebox( "OMNIA", "Operazione interrotta!")
		return -1
	end if			
	
	select cdunim
	into   :ls_cod_misura_2
	from   pgmr.unimisura
	where  cdunim_m = :ls_cod_misura
	using  tran_import;
	
	if tran_import.sqlcode <> 0 then
		setpointer(arrow!)
		g_mb.messagebox( "OMNIA", "Errore durante la ricerca del codice unità di misura " + ls_cod_misura + " :" + tran_import.sqlerrtext)
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback using sqlca;
		g_mb.messagebox( "OMNIA", "Operazione interrotta!")
		return -1
	end if	
	
	INSERT INTO pgmr.ordacq_posi(tkposi,
										  tkordi,
										  nrposi,
										  cdfisc,
										  cdiva,
										  cdarti,
										  dssart,
										  qtatot,
										  cdunim,
										  impuni,
										  sconto1,
										  sconto2,
										  sconto3,
										  fraggl,
										  oneacq,
										  cdmacq,
										  nvmacq,             
										  cdazie,
										  cddipa,
										  profil,
										  dtinse,
										  dtulag,
										  fscoval,
										  qtacons,
										  qtacons_s,
										  fgsaldo,
										  dsestesa)
					values          (:ll_chiave_pos,
					                 :ll_chiave_testata,
										  :ll_prog_ordine_progen,
										  '0000000002',
										  :ls_cod_iva_progen,
										  :ls_cdarti,
										  :ls_des_prodotto,
										  :ld_quantita,
										  :ls_cod_misura_2,
										  :ld_prezzo,
										  :ld_sconto_1,
										  :ld_sconto_2,
										  :ld_sconto_3,
										  null,
										  0,
										  null,
										  null,
										  :ls_cod_azienda,
										  :ls_cdstab_m,						//:ls_cod_area_aziendale,
										  :s_cs_xx.cod_utente,
										  :ldt_oggi,
										  :ldt_oggi,
										  'S',
										  0,
										  0,
										  'N',
										  :ls_des_prodotto)				
				using   tran_import; 
	if tran_import.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento della posizione:" + tran_import.sqlerrtext)
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback;
		g_mb.messagebox( "OMNIA", "Operazione interrotta!")
		return -1
	end if	
		
	// *** 	CONSEGNE
	
	INSERT INTO pgmr.ordacq_cons(tkcons,
										  tkposi,
										  tkordi,
										  nrcons,
										  cdtolc,
										  dtconf,
										  dtcona, 
									     qtaord,
										  nroper,
										  cdazie,
										  cddipa,
										  profil,
										  dtinse,
										  dtulag,
										  fsaldo,
										  qtacons,
	 									  qtacons_s)	
						VALUES		 (:ll_chiave,
										  :ll_chiave_pos,
										  :ll_chiave_testata,
										  1,
										  :ls_cdtolc,
										  :ldt_consegna,
										  :ldt_consegna,
										  :ld_quantita,
										  :ll_chiave_2,
										  :ls_cod_azienda,
										  :ls_cdstab_m,						//:ls_cod_area_aziendale,
										  :s_cs_xx.cod_utente,
										  :ldt_oggi,
										  :ldt_oggi,
										  'N',
										  0,
										  0)
				using   tran_import; 
	if tran_import.sqlcode < 0 then
		setpointer(arrow!)
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento della consegna:" + tran_import.sqlerrtext)
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback;
		g_mb.messagebox( "OMNIA", "Operazione interrotta!")
		return -1
	end if	
	
	ls_1 = string(ll_chiave_pos)
	ls_2 = string(ll_chiave_testata)
	ls_ordine_progen = string(ll_anno_ordine) + "/" + string(ll_num_ordine)
	
//	if ll_ret_modifiche = 1 then
		
//		update prog_manutenzioni_ricambi
//		set    tkposi = :ls_1,
//				 tkordi = :ls_2,
//				 nrposi = :ll_prog_ordine_progen,
//				 ordine_progen = :ls_ordine_progen,			 
//				 quan_utilizzo = :ld_quantita,			 
//				 prezzo_ricambio = :ld_prezzo,
//				 sconto_1_budget = :ld_sconto_1,
//				 sconto_2_budget = :ld_sconto_2,
//				 sconto_3_budget = :ld_sconto_3,
//				 prezzo_netto_budget = :ld_prezzo_netto,				 			 
//				 prezzo_ricambio_eff = :ld_prezzo,
//				 sconto_1_eff = :ld_sconto_1,
//				 sconto_2_eff = :ld_sconto_2,
//				 sconto_3_eff = :ld_sconto_3,
//				 prezzo_netto_eff = :ld_prezzo_netto,
//				 data_consegna = :ldt_consegna,
//				 cod_prodotto = :ls_cod_prodotto,
//				 des_prodotto = :ls_des_prodotto,
//				 cod_misura = :ls_cod_misura,
//				 cod_porto = :ls_porto,
//				 quan_eff = :ld_quantita,
//				 des_estesa = :ls_des_estesa
//		WHERE  cod_azienda = :s_cs_xx.cod_azienda and
//				 anno_registrazione = :ll_anno and
//				 num_registrazione = :ll_numero and
//				 prog_riga_ricambio = :ll_prog_riga;
//				 
//	else
		
		update prog_manutenzioni_ricambi
		set    tkposi = :ls_1,
				 tkordi = :ls_2,
				 nrposi = :ll_prog_ordine_progen,
				 ordine_progen = :ls_ordine_progen,			 
				 prezzo_ricambio_eff = :ld_prezzo,
				 sconto_1_eff = :ld_sconto_1,
				 sconto_2_eff = :ld_sconto_2,
				 sconto_3_eff = :ld_sconto_3,
				 prezzo_netto_eff = :ld_prezzo_netto,
				 quan_eff = :ld_quantita,
				 des_estesa = :ls_des_estesa,
				 cod_fornitore = :ls_cod_fornitore
		WHERE  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero and
				 prog_riga_ricambio = :ll_prog_riga;		
				
//	end if
			 
	if sqlca.sqlcode < 0 then
		setpointer(arrow!)
		g_mb.messagebox( "OMNIA", "Errore durante l'aggiornamento della riga del ricambio:" + sqlca.sqlerrtext)
		rollback using tran_import;
		disconnect using tran_import;
		destroy tran_import;
		rollback using sqlca;
		g_mb.messagebox( "OMNIA", "Operazione interrotta!")
		return -1
	end if				 


next

commit using tran_import;
disconnect using tran_import;
destroy tran_import;

commit using sqlca;
setpointer(arrow!)
g_mb.messagebox( "OMNIA", "Operazione terminata!~r~n" + ls_lista_ordini)

select cod_tipo_lista_dist,
       cod_lista_dist
into   :ls_cod_tipo_lista_dist,
       :ls_cod_lista_dist
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Attenzione, impossibile inviare l'email: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist = "" or isnull(ls_cod_lista_dist) or ls_cod_lista_dist = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: indicare la lista di distribuzione che si vuole considerare nei parametri manutenzione!")
	return -1
end if

wf_invia_mail(ls_testo, ls_cod_tipo_lista_dist, ls_cod_lista_dist)

cb_report.enabled = false
cb_2.enabled = false

return 0

end event

type dw_selezione from uo_cs_xx_dw within w_programmi_manutenzione_ordini
integer x = 23
integer y = 20
integer width = 3611
integer height = 440
integer taborder = 20
string dataobject = "d_programmi_manutenzione_grid_ordini"
end type

event pcd_new;call super::pcd_new;setitem( 1, "data_importazione", datetime(date(today()), 00:00:00))
end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_att"
		//guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura")
		
		setcolumn("cod_attrezzatura")
		guo_ricerca.uof_set_response( parent )
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione, "cod_attrezzatura")
		
	case "b_ricerca_fornitore"
		//guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
		
		setcolumn("cod_fornitore")
		guo_ricerca.uof_set_response( parent )
		guo_ricerca.uof_ricerca_fornitore(dw_selezione, "cod_fornitore")
		
end choose
end event

