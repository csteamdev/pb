﻿$PBExportHeader$w_programmi_manutenzione_ricambio.srw
$PBExportComments$Finestra Manutenzioni personalizzata Guerrato
forward
global type w_programmi_manutenzione_ricambio from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_programmi_manutenzione_ricambio
end type
type cb_salva from commandbutton within w_programmi_manutenzione_ricambio
end type
type dw_ricambio from uo_cs_xx_dw within w_programmi_manutenzione_ricambio
end type
end forward

global type w_programmi_manutenzione_ricambio from w_cs_xx_risposta
integer width = 3634
integer height = 2088
string title = "Aggiunta Ricambio"
boolean resizable = false
cb_annulla cb_annulla
cb_salva cb_salva
dw_ricambio dw_ricambio
end type
global w_programmi_manutenzione_ricambio w_programmi_manutenzione_ricambio

type variables
boolean ib_nuovo, ib_nuovo_rda
long il_rbuttonrow
datawindow idw_parent
n_tran	tran_import
end variables

forward prototypes
public function integer wf_controllo_anno ()
public function integer wf_controllo_anno_inizio ()
public function integer wf_aggiorna ()
end prototypes

public function integer wf_controllo_anno ();string	ls_cod_tipo_manutenzione, ls_flag_budget_chiuso, ls_flag_budget, ls_flag_blocco, ls_null, ls_flag_amministratore
long	ll_anno_riferimento

dw_ricambio.accepttext()

ll_anno_riferimento = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "anno_riferimento")
ls_flag_budget = dw_ricambio.getitemstring( dw_ricambio.getrow(), "flag_budget")
if isnull(ls_flag_budget) then ls_flag_budget = "N"

if not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
	
	select flag_chiuso
	into	:ls_flag_budget_chiuso
	from 	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_riferimento = :ll_anno_riferimento;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il controllo budget consolidato per l'anno " + string(ll_anno_riferimento) + ":" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	if not isnull(ls_flag_budget_chiuso) and ls_flag_budget_chiuso = "S" and not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta consolidato! Impossibile aggiungere attività a budget!", stopsign!)
		return -1
	end if

end if

return 0

end function

public function integer wf_controllo_anno_inizio ();string	ls_cod_tipo_manutenzione, ls_flag_budget_chiuso, ls_flag_budget, ls_flag_blocco, ls_null, ls_flag_amministratore
long	ll_anno_riferimento

dw_ricambio.accepttext()

ll_anno_riferimento = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "anno_riferimento")
ls_flag_budget = dw_ricambio.getitemstring( dw_ricambio.getrow(), "flag_budget")
if isnull(ls_flag_budget) then ls_flag_budget = "N"

if not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
	
	select flag_chiuso
	into	:ls_flag_budget_chiuso
	from 	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_riferimento = :ll_anno_riferimento;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il controllo budget consolidato per l'anno " + string(ll_anno_riferimento) + ":" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	if not isnull(ls_flag_budget_chiuso) and ls_flag_budget_chiuso = "S" and not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta consolidato! Impossibile aggiungere attività a budget!", stopsign!)
		
		select flag_collegato
		into	:ls_flag_amministratore
		from	utenti
		where	cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			rollback;
			close(this)
		end if
		
		return -1
	end if

end if

return 0

end function

public function integer wf_aggiorna ();long	row
string	ls_flag_budget, ls_cod_misura

row = dw_ricambio.getrow()

ls_flag_budget = dw_ricambio.getitemstring( row, "flag_budget")
			
if not isnull(s_cs_xx.parametri.parametro_s_10) then dw_ricambio.setitem( row, "cod_prodotto", s_cs_xx.parametri.parametro_s_10)
if not isnull(s_cs_xx.parametri.parametro_s_11) then dw_ricambio.setitem( row, "des_prodotto", s_cs_xx.parametri.parametro_s_11)
if not isnull(s_cs_xx.parametri.parametro_d_1) then 
	
	if not isnull(s_cs_xx.parametri.parametro_dec4_1) and s_cs_xx.parametri.parametro_dec4_1 > 0 then
		s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_1 )/ 100)
	end if
	
	if not isnull(s_cs_xx.parametri.parametro_dec4_2) and s_cs_xx.parametri.parametro_dec4_2 > 0 then
		s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_2 )/ 100)
	end if
	
	if not isnull(s_cs_xx.parametri.parametro_dec4_3) and s_cs_xx.parametri.parametro_dec4_3 > 0 then
		s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_3 )/ 100)
	end if
	
	if not isnull(s_cs_xx.parametri.parametro_dec4_4) and s_cs_xx.parametri.parametro_dec4_4 > 0 then
		s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_4 )/ 100)
	end if
	
	if not isnull(s_cs_xx.parametri.parametro_dec4_5) and s_cs_xx.parametri.parametro_dec4_5 > 0 then
		s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_5 )/ 100)
	end if			
	
	if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		dw_ricambio.setitem( row, "costo_unitario", s_cs_xx.parametri.parametro_d_1)
	else
		dw_ricambio.setitem( row, "costo_unitario_eff", s_cs_xx.parametri.parametro_d_1)
	end if
	
end if

if not isnull(s_cs_xx.parametri.parametro_d_1) then 
	if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		dw_ricambio.setitem( row, "sconto_1_budget", 0)
	else
		dw_ricambio.setitem( row, "sconto_1_eff", 0)
	end if
end if

if not isnull(s_cs_xx.parametri.parametro_d_1) then 
	if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		dw_ricambio.setitem( row, "sconto_2_budget", 0)
	else
		dw_ricambio.setitem( row, "sconto_2_eff", 0)
	end if
end if

if not isnull(s_cs_xx.parametri.parametro_d_1) then 
	if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		dw_ricambio.setitem( row, "sconto_3_budget", 0)
	else
		dw_ricambio.setitem( row, "sconto_3_eff", 0)
	end if
end if

if not isnull(s_cs_xx.parametri.parametro_d_1) then 
	if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		dw_ricambio.setitem( row, "prezzo_netto_budget", s_cs_xx.parametri.parametro_d_1)
	else
		dw_ricambio.setitem( row, "prezzo_netto_eff", s_cs_xx.parametri.parametro_d_1)
	end if
end if

if not isnull(s_cs_xx.parametri.parametro_s_12) then 
	
	select cod_misura
	into	:ls_cod_misura
	from	tab_misure
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cdunim = :s_cs_xx.parametri.parametro_s_12;
			
	if sqlca.sqlcode = 0 and not isnull(ls_cod_misura) and ls_cod_misura <> "" then			
		dw_ricambio.setitem( row, "cod_misura", ls_cod_misura)
	end if
	
end if

dw_ricambio.accepttext()

return 0
end function

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
	ib_nuovo_rda = false
elseif upper(s_cs_xx.parametri.parametro_s_1) = "NRDA" then
	ib_nuovo = true
	ib_nuovo_rda = true
else
	ib_nuovo = false
end if

save_on_close(c_socnosave)

dw_ricambio.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1

s_cs_xx.parametri.parametro_b_1 = false
end event

on w_programmi_manutenzione_ricambio.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_salva=create cb_salva
this.dw_ricambio=create dw_ricambio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_ricambio
end on

on w_programmi_manutenzione_ricambio.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_salva)
destroy(this.dw_ricambio)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricambio,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cdunim is not null and cdunim <> '' ")
					  
f_PO_LoadDDDW_DW(dw_ricambio,"cod_porto",sqlca,&
                 "tab_porti","cod_porto","des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_sort (dw_ricambio,"cod_tipo_programma",sqlca,&
                 "tab_tipi_programmi","cod_tipo_programma","des_tipo_programma",&
                 "", " des_tipo_programma ASC ")
					  
					  
f_PO_LoadDDDW_DW( dw_ricambio, &
						"cod_stato", &
						sqlca, &
						"tab_stato_programmi", &
						"cod_stato", &
						"des_stato", &
						" ")					  
end event

type cb_annulla from commandbutton within w_programmi_manutenzione_ricambio
integer x = 2830
integer y = 1912
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;

close(parent)
end event

type cb_salva from commandbutton within w_programmi_manutenzione_ricambio
integer x = 3223
integer y = 1912
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_row,ll_anno_registrazione,ll_num_registrazione ,ll_progressivo, ll_priorita, ll_anno_riferimento
string ls_cod_prodotto, ls_des_prodotto, ls_cod_fornitore, ls_cod_misura, ls_cod_porto, ls_cod_stato, &
		 ls_flag_ricambio_codificato, ls_flag_prezzo_certo, ls_flag_capitalizzato, ls_flag_sconto_valore, ls_tkboll, ls_tkordi, ls_cod_tipo_programma, &
		 ls_flag_evaso, ls_des_estesa, ls_rda, ls_flag_budget

dec{4} ld_quan_utilizzo, ld_prezzo_ricambio, ld_sconto_1,ld_sconto_2,ld_sconto_3,ld_prezzo_netto,ld_sconto_1_eff,ld_sconto_2_eff,ld_sconto_3_eff,ld_prezzo_netto_eff, &
       ld_prezzo_ricambio_eff, ld_quan_consegnata, ld_quan_ordinata
		 
datetime ldt_data_consegna

dw_ricambio.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
	if ib_nuovo then
		ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")	
	end if
end if

ls_cod_prodotto = dw_ricambio.getitemstring(dw_ricambio.getrow(), "cod_prodotto")
ls_des_prodotto = dw_ricambio.getitemstring(dw_ricambio.getrow(), "des_prodotto")
ld_quan_utilizzo  = dw_ricambio.getitemnumber(dw_ricambio.getrow(), "quan_utilizzo")
ld_prezzo_ricambio = dw_ricambio.getitemnumber(dw_ricambio.getrow(), "costo_unitario")
ls_cod_fornitore = dw_ricambio.getitemstring(dw_ricambio.getrow(), "cod_fornitore")
ldt_data_consegna = dw_ricambio.getitemdatetime(dw_ricambio.getrow(), "data_consegna")
ls_cod_misura = dw_ricambio.getitemstring(dw_ricambio.getrow(), "cod_misura")
ls_cod_porto = dw_ricambio.getitemstring(dw_ricambio.getrow(), "cod_porto")
ls_flag_prezzo_certo = dw_ricambio.getitemstring(dw_ricambio.getrow(), "flag_prezzo_certo")
ls_flag_capitalizzato = dw_ricambio.getitemstring(dw_ricambio.getrow(), "flag_capitalizzato")
ls_flag_sconto_valore = dw_ricambio.getitemstring( dw_ricambio.getrow(), "flag_sconto_valore")
ll_priorita = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "priorita")
ld_sconto_1 = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_1_budget")
ld_sconto_2 = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_2_budget")
ld_sconto_3 = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_3_budget")
ld_prezzo_netto = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "prezzo_netto_budget")

ld_sconto_1_eff = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_1_eff")
ld_sconto_2_eff = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_2_eff")
ld_sconto_3_eff = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "sconto_3_eff")
ld_prezzo_netto_eff = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "prezzo_netto_eff")
ld_prezzo_ricambio_eff = dw_ricambio.getitemnumber(dw_ricambio.getrow(), "costo_unitario_eff")
ls_cod_tipo_programma = dw_ricambio.getitemstring( dw_ricambio.getrow(), "cod_tipo_programma")
ls_cod_stato = dw_ricambio.getitemstring( dw_ricambio.getrow(), "cod_stato")
ld_quan_consegnata = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "quan_consegnata")
ls_flag_evaso = dw_ricambio.getitemstring( dw_ricambio.getrow(), "flag_evaso")
ld_quan_ordinata = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "quan_eff")
ls_des_estesa = dw_ricambio.getitemstring( dw_ricambio.getrow(), "des_estesa")
ls_flag_budget = dw_ricambio.getitemstring( dw_ricambio.getrow(), "flag_budget")
//ls_flag_confermato = dw_ricambio.getitemstring( dw_ricambio.getrow(), "flag_confermato")
ll_anno_riferimento = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "anno_riferimento")

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: selezionare un articolo prima di salvare!", stopsign!)
	rollback;
	return
end if

if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: impostare l'anno di riferimento della riga di ricambio!", stopsign!)
	rollback;
	return
end if

if isnull(ls_cod_tipo_programma) or ls_cod_tipo_programma = "" then
	g_mb.messagebox("OMNIA", "Attenzione: specificare tipo di attività!")
	rollback;
	return		
end if

if isnull(ls_cod_porto) or ls_cod_porto = "" then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il porto per il ricambio!", stopsign!)
	rollback;
	return
end if

if isnull(ldt_data_consegna) or STRING(ldt_data_consegna, "dd/mm/yyyy") = "01/01/1900" then
	g_mb.messagebox("OMNIA", "Attenzione: specificare la data di consegna prevista per il ricambio!")
	rollback;
	return		
end if

if isnull(ls_flag_prezzo_certo) or ls_flag_prezzo_certo = "" then ls_flag_prezzo_certo = "N"
if isnull(ls_flag_capitalizzato) or ls_flag_capitalizzato = "" then ls_flag_capitalizzato = "N"
if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"
if isnull(ls_flag_evaso) or ls_flag_evaso = "" then ls_flag_evaso = "N"
if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
//if isnull(ls_flag_confermato) or ls_flag_confermato = "" then ls_flag_confermato = "N"

if isnull(ld_quan_consegnata) then ld_quan_consegnata = 0

ls_flag_ricambio_codificato = "S"
if isnull(ls_cod_prodotto) then ls_flag_ricambio_codificato = "N"

if isnull(ls_cod_fornitore) or ls_cod_fornitore = "" then
	g_mb.messagebox("OMNIA", "Attenzione: specificare un fornitore per il ricambio!")
	rollback;
	return	
end if

if ib_nuovo then
	
	select max(prog_riga_ricambio)
	into   :ll_progressivo
	from   prog_manutenzioni_ricambi
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	if ib_nuovo_rda then
		ls_rda = "S"
	else
		ls_rda = "N"
	end if
	
	if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0		
	if isnull(ld_prezzo_ricambio) then ld_prezzo_ricambio = 0
	if isnull(ld_sconto_1) then ld_sconto_1 = 0
	if isnull(ld_sconto_2) then ld_sconto_2 = 0
	if isnull(ld_sconto_3) then ld_sconto_3 = 0
	if isnull(ld_prezzo_netto) then ld_prezzo_netto = 0
	if isnull(ld_prezzo_ricambio_eff) then ld_prezzo_ricambio_eff = 0
	if isnull(ld_sconto_1_eff) then ld_sconto_1_eff = 0
	if isnull(ld_sconto_2_eff) then ld_sconto_2_eff = 0
	if isnull(ld_sconto_3_eff) then ld_sconto_3_eff = 0
	if isnull(ld_prezzo_netto_eff) then ld_prezzo_netto_eff = 0
	if isnull(ld_quan_consegnata) then ld_quan_consegnata = 0
	if isnull(ld_quan_ordinata) then ld_quan_ordinata = 0	
	if isnull(ll_priorita) then ll_priorita = 1
	
	INSERT INTO prog_manutenzioni_ricambi
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ricambio,
			  cod_prodotto,
			  des_prodotto,
			  quan_utilizzo,
			  prezzo_ricambio,
			  cod_fornitore,
			  data_consegna,
			  cod_misura,
			  cod_porto,
			  flag_prezzo_certo,
			  flag_capitalizzato,
			  priorita,
			  sconto_1_budget,
				sconto_2_budget,
				sconto_3_budget,
				prezzo_netto_budget,
				prezzo_ricambio_eff,
				sconto_1_eff,
				sconto_2_eff,
				sconto_3_eff,
				prezzo_netto_eff,
				flag_sconto_valore,
				cod_tipo_programma,
				cod_stato,
				quan_consegnata,
				flag_evaso,
				quan_eff,
				des_estesa,
				flag_rda,
				flag_budget,
				flag_confermato,
				anno_riferimento)
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,
			  :ll_progressivo,
			  :ls_cod_prodotto,
			  :ls_des_prodotto,
			  :ld_quan_utilizzo,
			  :ld_prezzo_ricambio,
			  :ls_cod_fornitore,
			  :ldt_data_consegna,
			  :ls_cod_misura,
			  :ls_cod_porto,
			  :ls_flag_prezzo_certo,
			  :ls_flag_capitalizzato,
			  :ll_priorita,
			  :ld_sconto_1,
			  :ld_sconto_2,
				:ld_sconto_3,
				:ld_prezzo_netto,
				:ld_prezzo_ricambio_eff,
				:ld_sconto_1_eff,
				:ld_sconto_2_eff,
				:ld_sconto_3_eff,
				:ld_prezzo_netto_eff,
				:ls_flag_sconto_valore,
				:ls_cod_tipo_programma,
				:ls_cod_stato,
				:ld_quan_consegnata,
				:ls_flag_evaso,
				:ld_quan_ordinata,
				:ls_des_estesa,
				:ls_rda,
				:ls_flag_budget,
				'N',
				:ll_anno_riferimento)  ;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione ricambio~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

else
	
	ll_progressivo  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
	
	long ll_cont
	
	select count(cod_azienda)
	into   :ll_cont
	from   prog_manutenzioni_ricambi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ricambio = :ll_progressivo;
			
	if isnull(ll_cont) or ll_cont = 0 then
		g_mb.messagebox("OMNIA", "Riga non trovata:~r~n" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_progressivo))
		rollback;
		return		
	end if
	
		if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0		
		if isnull(ld_prezzo_ricambio) then ld_prezzo_ricambio = 0
		if isnull(ld_sconto_1) then ld_sconto_1 = 0
		if isnull(ld_sconto_2) then ld_sconto_2 = 0
		if isnull(ld_sconto_3) then ld_sconto_3 = 0
		if isnull(ld_prezzo_netto) then ld_prezzo_netto = 0
		if isnull(ld_prezzo_ricambio_eff) then ld_prezzo_ricambio_eff = 0
		if isnull(ld_sconto_1_eff) then ld_sconto_1_eff = 0
		if isnull(ld_sconto_2_eff) then ld_sconto_2_eff = 0
		if isnull(ld_sconto_3_eff) then ld_sconto_3_eff = 0
		if isnull(ld_prezzo_netto_eff) then ld_prezzo_netto_eff = 0
		if isnull(ld_quan_consegnata) then ld_quan_consegnata = 0
		if isnull(ld_quan_ordinata) then ld_quan_ordinata = 0
		if isnull(ll_priorita) then ll_priorita = 1

		update prog_manutenzioni_ricambi
		set   cod_prodotto = :ls_cod_prodotto,
				des_prodotto = :ls_des_prodotto,
				quan_utilizzo = :ld_quan_utilizzo,
				prezzo_ricambio = :ld_prezzo_ricambio,
				cod_fornitore = :ls_cod_fornitore,
				data_consegna = :ldt_data_consegna,
				cod_misura = :ls_cod_misura,
				cod_porto = :ls_cod_porto,
				flag_prezzo_certo = :ls_flag_prezzo_certo,
				flag_capitalizzato = :ls_flag_capitalizzato,
				priorita = :ll_priorita,
				sconto_1_budget = :ld_sconto_1,
				sconto_2_budget = :ld_sconto_2,
				sconto_3_budget = :ld_sconto_3,
				prezzo_netto_budget = :ld_prezzo_netto,
				prezzo_ricambio_eff = :ld_prezzo_ricambio_eff,
				sconto_1_eff = :ld_sconto_1_eff,
				sconto_2_eff = :ld_sconto_2_eff,
				sconto_3_eff = :ld_sconto_3_eff,
				prezzo_netto_eff = :ld_prezzo_netto_eff,
				flag_sconto_valore = :ls_flag_sconto_valore,
				cod_tipo_programma = :ls_cod_tipo_programma,
				cod_stato = :ls_cod_stato,
				quan_consegnata = :ld_quan_consegnata,
				flag_evaso = :ls_flag_evaso,
				quan_eff = :ld_quan_ordinata,
				des_estesa = :ls_des_estesa,
				flag_budget = :ls_flag_budget,
				flag_confermato = 'N',
				anno_riferimento = :ll_anno_riferimento
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione and
				prog_riga_ricambio = :ll_progressivo;
				
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA", "Errore in aggiornamento ricambio~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
//		
//	end if
	
end if

commit;
s_cs_xx.parametri.parametro_b_1 = true
close(parent)
		  

end event

type dw_ricambio from uo_cs_xx_dw within w_programmi_manutenzione_ricambio
event ue_blocca_budget ( )
event ue_controllo_anno_inizio ( )
event ue_listini ( )
integer x = 23
integer y = 20
integer width = 3566
integer height = 1860
integer taborder = 10
string dataobject = "d_programmi_manutenzione_grid_ricambio"
end type

event ue_blocca_budget();string	ls_flag_budget

//		avverto l'utente che non può cambiare lo stato della riga perchè il budget è già stato chiuso
g_mb.messagebox( "OMNIA", "Attenzione: impossibile modificare lo stato Budget della riga, in quanto il budget stesso per l'anno indicato risulta chiuso!", stopsign!)

accepttext()
			
ls_flag_budget = getitemstring( getrow(), "flag_budget")
			
if ls_flag_budget = "N" then									//	***		se non è a budget allora devo bloccare i campi
	setitem( getrow(), "flag_budget", "S")
else
	setitem( getrow(), "flag_budget", "N")
end if
end event

event ue_controllo_anno_inizio();string	ls_cod_tipo_manutenzione, ls_flag_budget_chiuso, ls_flag_budget, ls_flag_blocco, ls_null, ls_flag_amministratore
long	ll_anno_riferimento

dw_ricambio.accepttext()

ll_anno_riferimento = dw_ricambio.getitemnumber( dw_ricambio.getrow(), "anno_riferimento")
ls_flag_budget = dw_ricambio.getitemstring( dw_ricambio.getrow(), "flag_budget")
if isnull(ls_flag_budget) then ls_flag_budget = "N"

if not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
	
	select flag_chiuso
	into	:ls_flag_budget_chiuso
	from 	prog_manutenzioni_anni
	where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_riferimento = :ll_anno_riferimento;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il controllo budget consolidato per l'anno " + string(ll_anno_riferimento) + ":" + sqlca.sqlerrtext, stopsign!)
		return
	end if
	
	if not isnull(ls_flag_budget_chiuso) and ls_flag_budget_chiuso = "S" and not isnull(ls_flag_budget) and ls_flag_budget = "S" then
		g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta consolidato! Impossibile aggiungere attività a budget!", stopsign!)
		
		select flag_collegato
		into	:ls_flag_amministratore
		from	utenti
		where	cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
		else
			rollback;
			close(parent)
		end if
		
		return
	end if

end if

return 

end event

event ue_listini();string		ls_tkforn, ls_cod_fornitore, ls_cod_prodotto, ls_null, ls_codice, ls_descrizione, ls_cod_misura
dec{4}	ld_prezzo, ld_sc_1, ld_sc_2, ld_sc_3
long		ll_n_listini
datetime	ldt_data_documento

uo_progen_documenti	luo_obj

setpointer( hourglass!)

setnull(ls_null)

this.object.b_listini.visible = 0

ls_cod_fornitore = getitemstring( getrow(), "cod_fornitore")
ldt_data_documento = getitemdatetime( getrow(), "data_consegna")
ls_cod_prodotto = getitemstring( getrow(), "cod_prodotto")

if not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" and not isnull(ldt_data_documento) then
	
	select tkforn
	into	:ls_tkforn
	from	anag_fornitori
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore;
			
	if sqlca.sqlcode <> 0 or isnull(ls_tkforn) or ls_tkforn = "" then
		setpointer( arrow!)
		return
	end if
	
	luo_obj = create uo_progen_documenti
	
	if luo_obj.f_registro( true, tran_import) <> 0 then
		destroy luo_obj;
		setpointer( arrow!)
		return
	end if
	
	select count(PGMR.FORNARTICOLO.CDARTI)
	into	:ll_n_listini
	from	PGMR.FORNARTICOLO ,           PGMR.ARCHFORN ,           PGMR.MRP_ARCH_ARTICOLI ,           PGMR.ARCHENTI   
	where	( PGMR.FORNARTICOLO.TKFORN = PGMR.ARCHFORN.TKFORN ) AND       
			( PGMR.FORNARTICOLO.CDARTI = PGMR.MRP_ARCH_ARTICOLI.CDARTI ) AND      
			( PGMR.ARCHFORN.CDENTE = PGMR.ARCHENTI.CDENTE ) and
			( PGMR.FORNARTICOLO.TKFORN = :ls_tkforn ) and
			( PGMR.FORNARTICOLO.DTIVAL <= :ldt_data_documento OR PGMR.FORNARTICOLO.DTIVAL IS NULL ) and
			( PGMR.FORNARTICOLO.DTFVAL  >= :ldt_data_documento OR PGMR.FORNARTICOLO.DTFVAL IS NULL )
	using	tran_import;
	
	if tran_import.sqlcode <> 0 then
		luo_obj.f_registro( false, tran_import)
		destroy luo_obj;
		setpointer( arrow!)
		g_mb.messagebox( "OMNIA", "Errore durante la verifica della presenza di listini per il fornitore " + ls_cod_fornitore + " ( tkforn=" + ls_tkforn + " ): " + tran_import.sqlerrtext + " !", stopsign!)
		return 
	end if
	
	if not isnull(ll_n_listini) and ll_n_listini > 0 then
		
		if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
			
			select pgmr.mrp_arch_articoli.cdartm,
					pgmr.mrp_arch_articoli.dsarti,
					pgmr.fornarticolo.prezzo,
					pgmr.fornarticolo.psc1,
					pgmr.fornarticolo.psc2,
					pgmr.fornarticolo.psc3,
					pgmr.mrp_arch_articoli.cdunim_1					
			into	:ls_codice,
					:ls_descrizione,
					:ld_prezzo,
					:ld_sc_1,
					:ld_sc_2,
					:ld_sc_3,
					:ls_cod_misura
			from	PGMR.FORNARTICOLO ,           PGMR.ARCHFORN ,           PGMR.MRP_ARCH_ARTICOLI ,           PGMR.ARCHENTI   
			where	( PGMR.FORNARTICOLO.TKFORN = PGMR.ARCHFORN.TKFORN ) AND       
					( PGMR.FORNARTICOLO.CDARTI = PGMR.MRP_ARCH_ARTICOLI.CDARTI ) AND      
						( PGMR.ARCHFORN.CDENTE = PGMR.ARCHENTI.CDENTE ) and
					( PGMR.FORNARTICOLO.TKFORN = :ls_tkforn ) and
					( PGMR.FORNARTICOLO.DTIVAL <= :ldt_data_documento OR PGMR.FORNARTICOLO.DTIVAL IS NULL ) and
					( PGMR.FORNARTICOLO.DTFVAL  >= :ldt_data_documento OR PGMR.FORNARTICOLO.DTFVAL IS NULL ) and
					( PGMR.MRP_ARCH_ARTICOLI.CDARTM = :ls_cod_prodotto )
			using	tran_import;
			
			if tran_import.sqlcode < 0 then
				luo_obj.f_registro( false, tran_import)
				destroy luo_obj;
				setpointer( arrow!)
				g_mb.messagebox( "OMNIA", "Errore durante la verifica della presenza di listini per il fornitore " + ls_cod_fornitore + " ( tkforn=" + ls_tkforn + " ): " + tran_import.sqlerrtext + " !", stopsign!)
				return 
			end if
			
			if tran_import.sqlcode =  0 then
				
				this.object.b_listini.visible = 1
				
				s_cs_xx.parametri.parametro_s_10 = ls_codice
				s_cs_xx.parametri.parametro_s_11 = ls_descrizione
				s_cs_xx.parametri.parametro_d_1 = ld_prezzo
				s_cs_xx.parametri.parametro_dec4_1 = ld_sc_1
				s_cs_xx.parametri.parametro_dec4_2 = ld_sc_2
				s_cs_xx.parametri.parametro_dec4_3 = ld_sc_3
				s_cs_xx.parametri.parametro_dec4_4 = 0
				s_cs_xx.parametri.parametro_dec4_5 = 0
				s_cs_xx.parametri.parametro_s_12 = ls_cod_misura
				
				wf_aggiorna()

			else
				this.object.b_listini.visible = 1
				g_mb.messagebox( "OMNIA", "Attenzione: l'articolo selezionato non è in contratto!~rE' comunque possibile continuare con l'operazione!", Information!)

			end if		
			
		else
			
			this.object.b_listini.visible = 1		
			g_mb.messagebox( "OMNIA", "Attenzione: il fornitore selezionato ha dei contratti validi nel periodo scelto!", Information!)
			
		end if

	else
		this.object.b_listini.visible = 0

	end if	
	
	luo_obj.f_registro( false, tran_import)	
	destroy luo_obj
	
end if

setpointer( arrow!)
end event

event itemchanged;call super::itemchanged;string ls_des_prodotto, ls_cod_misura, ls_null, ls_flag_blocco, ls_flag_sconto_valore, ls_cod_attrezzatura, ls_note_idl, ls_cod_cat_attrezzatura, ls_des_attrezzatura, &
		 ls_des_cat_attrezzatura, ls_cod_reparto, ls_flag_primario, ls_cod_area_aziendale, ls_prefisso, ls_descrizione_p, ls_flag_budget, ls_flag_chiuso, ls_flag_amministratore, ls_cod_attrezzatura_padre
		 
dec{4} ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_prezzo_unitario

long	 ll_num, ll_anno, ll_row, ll_inizio, ll_anno_riferimento, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ricambio

setnull(ls_null)

if i_extendmode then
	choose case i_colname
			
			// ****				PREZZI E QTA' BUDGET
		
		case "quan_utilizzo"
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"
			if isnull(ld_sconto_1) then ld_sconto_1 = 0
			if isnull(ld_sconto_2) then ld_sconto_2 = 0
			if isnull(ld_sconto_3) then ld_sconto_3 = 0
			if isnull( ld_prezzo_unitario) then ld_prezzo_unitario = 0
			
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if				
			end if
			if ld_sconto_3 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if				
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)						
		
		case "costo_unitario"
			
			ld_prezzo_unitario = dec(i_coltext)
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"
			if isnull(ld_sconto_1) then ld_sconto_1 = 0
			if isnull(ld_sconto_2) then ld_sconto_2 = 0
			if isnull(ld_sconto_3) then ld_sconto_3 = 0
			if isnull(ld_prezzo_unitario) then ld_prezzo_unitario = 0
			
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if				
			end if
			if ld_sconto_3 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if				
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)			
			
		case "sconto_1_budget"
		
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			ld_sconto_1 = dec(data)
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"
			
			if isnull(ld_sconto_1) then ld_sconto_1 = 0
			if isnull(ld_sconto_2) then ld_sconto_2 = 0
			if isnull(ld_sconto_3) then ld_sconto_3 = 0	
			if isnull(ld_prezzo_unitario) then ld_prezzo_unitario = 0
			
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if				
			end if
			if ld_sconto_3 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if				
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)
		
		case "sconto_2_budget" 
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			ld_sconto_2 = dec(data)
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"			
			
			if isnull(ld_sconto_1) then ld_sconto_1 = 0
			if isnull(ld_sconto_2) then ld_sconto_2 = 0
			if isnull(ld_sconto_3) then ld_sconto_3 = 0				
			if isnull(ld_prezzo_unitario) then ld_prezzo_unitario = 0
			
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if				
			end if
			if ld_sconto_3 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if				
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)
			
		case "sconto_3_budget"
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			ld_sconto_3 = dec(data)
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"			
			
			if isnull(ld_sconto_1) then ld_sconto_1 = 0
			if isnull(ld_sconto_2) then ld_sconto_2 = 0
			if isnull(ld_sconto_3) then ld_sconto_3 = 0				
			if isnull(ld_prezzo_unitario) then ld_prezzo_unitario = 0			
			
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if				
			end if
			if ld_sconto_3 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if				
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)				
			
		case "cod_prodotto"
			
			if isnull(i_coltext) or i_coltext="" then
				
				setitem(getrow(),"cod_misura", ls_null)
				setitem(getrow(),"des_prodotto", ls_null)
				
			else
				
				select des_prodotto, 
						 cod_misura_acq
				into   :ls_des_prodotto,
						 :ls_cod_misura
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :i_coltext;
						 
				if sqlca.sqlcode = 0 then
					
					if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
						ll_anno = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
						ll_num  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
					else
						// cerco la riga principale
						ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
						ll_anno = idw_parent.getitemnumber(ll_row, "anno_registrazione")
						ll_num  = idw_parent.getitemnumber(ll_row, "num_registrazione")
					end if
					
					
					select cod_attrezzatura,
							 note_idl
					into   :ls_cod_attrezzatura,
							 :ls_note_idl
					from   programmi_manutenzione  
					where  cod_azienda = :s_cs_xx.cod_azienda and  
							 anno_registrazione = :ll_anno and
							 num_registrazione = :ll_num ;	
							 
					if sqlca.sqlcode = 0 then
						
						select cod_cat_attrezzature,
						       descrizione,
								 cod_reparto,
								 flag_primario,
								 cod_area_aziendale,
								 cod_attrezzatura_padre
						into   :ls_cod_cat_attrezzatura,
								 :ls_des_attrezzatura,
								 :ls_cod_reparto,
								 :ls_flag_primario,
								 :ls_cod_area_aziendale,
								 :ls_cod_attrezzatura_padre
						from   anag_attrezzature
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_attrezzatura = :ls_cod_attrezzatura;
								 
						if not isnull(ls_cod_reparto) and ls_cod_reparto = "M" then
							
							select descrizione
							into   :ls_descrizione_p
							from   anag_attrezzature
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_attrezzatura = :ls_cod_attrezzatura_padre;
							
						else
								 
							select des_cat_attrezzature
							into   :ls_des_cat_attrezzatura
							from   tab_cat_attrezzature
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_cat_attrezzature = :ls_cod_cat_attrezzatura;
									 
						end if
						
					end if
					
					if not isnull(ls_cod_reparto) and ls_cod_reparto <> "" and ls_cod_reparto = "M" then
						
						ls_prefisso = left(ls_cod_attrezzatura,3)
						
						if left(ls_prefisso, 1) = "0" then
							
							ll_inizio = Pos( ls_prefisso, "0", 1)
							DO WHILE ll_inizio > 0
								ls_prefisso = Replace( ls_prefisso, ll_inizio, 1, "")
								ll_inizio = Pos( ls_prefisso, "0", ll_inizio)						
							LOOP
							
						end if
						
						ls_des_prodotto = "Nr. Aziendale " + ls_prefisso + " - " + ls_descrizione_p + " / " + ls_note_idl //+ " / " + ls_des_prodotto
					else					
						ls_des_prodotto = ls_des_cat_attrezzatura + " / " + ls_des_attrezzatura + " / " + ls_note_idl + " / " + ls_des_prodotto
					end if
					
					setitem(getrow(),"cod_misura", ls_cod_misura)
					setitem(getrow(),"des_prodotto", ls_des_prodotto)
					
				else
					return 2
				end if
			end if
			
			postevent("ue_listini")
			
		case "cod_fornitore"
			
			select flag_blocco 
			into   :ls_flag_blocco
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_fornitore = :i_coltext;
			
			if sqlca.sqlcode = 0 then
				if not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
					g_mb.messagebox( "OMNIA", "Attenzione: il fornitore scelto risulta bloccato!")
				end if
			end if
			
			postevent("ue_listini")
			
		case "data_consegna"
			
			postevent("ue_listini")
			
		case "costo_unitario_eff"

			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_eff")
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_eff")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_eff")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"			
			
			ld_prezzo_unitario = dec(i_coltext)
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if
			end if
			if ld_sconto_3 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else					
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if
			end if
			
			setitem( getrow(), "prezzo_netto_eff", ld_prezzo_unitario)						
			
		case "sconto_1_eff"
		
			ld_sconto_1 = dec(data)
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_eff")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_eff")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"			
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario_eff")
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if
			end if
			if ld_sconto_3 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else					
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if
			end if
			
			setitem( getrow(), "prezzo_netto_eff", ld_prezzo_unitario)
		
		case "sconto_2_eff" 
			
			ld_sconto_2 = dec(data)
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_eff")
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_eff")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"			
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario_eff")
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if
			end if
			if ld_sconto_3 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else					
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if
			end if
			
			setitem( getrow(), "prezzo_netto_eff", ld_prezzo_unitario)
			
		case "sconto_3_eff"
			
			ld_sconto_3 = dec(data)
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_eff")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_eff")
			ls_flag_sconto_valore = getitemstring( getrow(), "flag_sconto_valore")
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"			
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario_eff")
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if
			end if
			if ld_sconto_3 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else					
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if
			end if
			
			setitem( getrow(), "prezzo_netto_eff", ld_prezzo_unitario)	
			
		case "flag_sconto_valore"
			
			ld_sconto_3 = getitemnumber( getrow(), "sconto_3_budget")
			ld_sconto_1 = getitemnumber( getrow(), "sconto_1_budget")
			ld_sconto_2 = getitemnumber( getrow(), "sconto_2_budget")
			ls_flag_sconto_valore = data
			if isnull(ls_flag_sconto_valore) or ls_flag_sconto_valore = "" then ls_flag_sconto_valore = "N"			
			
			ld_prezzo_unitario = getitemnumber( getrow(), "costo_unitario")
			if ld_sconto_1 > 0 then
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_1
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_1 / 100)
				end if
			end if
			if ld_sconto_2 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_2
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_2 / 100)
				end if				
			end if
			if ld_sconto_3 > 0 then				
				if ls_flag_sconto_valore = "S" then
					ld_prezzo_unitario = ld_prezzo_unitario - ld_sconto_3
				else
					ld_prezzo_unitario = ld_prezzo_unitario - (ld_prezzo_unitario * ld_sconto_3 / 100)
				end if				
			end if
			
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_unitario)		
			
		case "flag_budget"
			
			//	*** Michela 23/05/2007: controllo il flag budget
			
			//	*** controllo di che anno fa parte la riga, e se il budget risulta chiuso. se si allora a meno che non sia amministratore non può fare nulla
			
			ls_flag_budget = data
			
			if not ib_nuovo then
			
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
				ll_prog_riga_ricambio  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			
				select anno_riferimento
				into    :ll_anno_riferimento
				from 	 prog_manutenzioni_ricambi
				where  cod_azienda = :s_cs_xx.cod_azienda and
				          anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ricambio = :ll_prog_riga_ricambio;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "OMNIA", "Errore durante il controllo anno riferimento della riga di ricambio:" + sqlca.sqlerrtext, stopsign!)
					return -1
				end if
				
				ll_anno_riferimento = getitemnumber( getrow(), "anno_riferimento")
				
				select flag_chiuso
				into    :ls_flag_chiuso
				from   prog_manutenzioni_anni
				where cod_azienda = :s_cs_xx.cod_azienda and
							 anno_riferimento = :ll_anno_riferimento;
							 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "OMNIA", "Errore durante il controllo chiusura budget anno " + string(ll_anno_riferimento) + ": " + sqlca.sqlerrtext, stopsign!)
					return -1
				end if
				
				if not isnull(ls_flag_chiuso) and ls_flag_chiuso = "S" then
					
					select flag_collegato
					into    :ls_flag_amministratore
					from   utenti
					where cod_utente = :s_cs_xx.cod_utente;
					
					if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
						g_mb.messagebox( "OMNIA", "Attenzione: il budget dell'anno " + string(ll_anno_riferimento) + " risulta già consolidato!", information!)
					else				
						postevent("ue_blocca_budget")
						return -1
					end if
				end if
				
			end if
									
			if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
			
			if ls_flag_budget = "N" then									//	***		se non è a budget allora devo bloccare i campi
				
//				setitem( getrow(), "quan_utilizzo", 0)
//				setitem( getrow(), "costo_unitario", 0)
//				setitem( getrow(), "flag_sconto_valore", "N")
//				setitem( getrow(), "sconto_1_budget", 0)
//				setitem( getrow(), "sconto_2_budget", 0)
//				setitem( getrow(), "sconto_3_budget", 0)				
				this.Object.quan_utilizzo.Protect=1
				this.Object.costo_unitario.Protect=1
				this.Object.flag_sconto_valore.Protect=1
				this.Object.sconto_1_budget.Protect=1
				this.Object.sconto_2_budget.Protect=1
				this.Object.sconto_3_budget.Protect=1
				this.Object.quan_utilizzo.background.color=string(rgb(192,192,192))
				this.Object.costo_unitario.background.color=string(rgb(192,192,192))
				this.Object.flag_sconto_valore.background.color=string(rgb(192,192,192))
				this.Object.sconto_1_budget.background.color=string(rgb(192,192,192))
				this.Object.sconto_2_budget.background.color=string(rgb(192,192,192))
				this.Object.sconto_3_budget.background.color=string(rgb(192,192,192))
				
				// e sbloccare gli effettivi
				
				this.Object.quan_eff.Protect=0
				this.Object.costo_unitario_eff.Protect=0
				this.Object.quan_consegnata.Protect=0
				this.Object.sconto_1_eff.Protect=0
				this.Object.sconto_2_eff.Protect=0
				this.Object.sconto_3_eff.Protect=0
				this.Object.quan_eff.background.color=string(rgb(255,255,255))
				this.Object.costo_unitario_eff.background.color=string(rgb(255,255,255))
				this.Object.quan_consegnata.background.color=string(rgb(255,255,255))
				this.Object.sconto_1_eff.background.color=string(rgb(255,255,255))
				this.Object.sconto_2_eff.background.color=string(rgb(255,255,255))
				this.Object.sconto_3_eff.background.color=string(rgb(255,255,255))
				
			else															//	***		altrimenti li sblocco
				
				this.Object.quan_utilizzo.Protect=0
				this.Object.costo_unitario.Protect=0
				this.Object.flag_sconto_valore.Protect=0
				this.Object.sconto_1_budget.Protect=0
				this.Object.sconto_2_budget.Protect=0
				this.Object.sconto_3_budget.Protect=0				
				this.Object.quan_utilizzo.background.color=string(rgb(255,255,255))
				this.Object.costo_unitario.background.color=string(rgb(255,255,255))
				this.Object.flag_sconto_valore.background.color=string(rgb(255,255,255))
				this.Object.sconto_1_budget.background.color=string(rgb(255,255,255))
				this.Object.sconto_2_budget.background.color=string(rgb(255,255,255))
				this.Object.sconto_3_budget.background.color=string(rgb(255,255,255))		
				
				this.Object.quan_eff.Protect=1
				this.Object.costo_unitario_eff.Protect=1
				this.Object.quan_consegnata.Protect=1
				this.Object.sconto_1_eff.Protect=1
				this.Object.sconto_2_eff.Protect=1
				this.Object.sconto_3_eff.Protect=1
				this.Object.quan_eff.background.color=string(rgb(192,192,192))
				this.Object.costo_unitario_eff.background.color=string(rgb(192,192,192))
				this.Object.quan_consegnata.background.color=string(rgb(192,192,192))
				this.Object.sconto_1_eff.background.color=string(rgb(192,192,192))
				this.Object.sconto_2_eff.background.color=string(rgb(192,192,192))
				this.Object.sconto_3_eff.background.color=string(rgb(192,192,192))				
				
			end if			
						
		end choose
end if


end event

event pcd_new;call super::pcd_new;string ls_null, ls_cod_porto, ls_flag_capitalizzato, ls_flag_prezzo_certo, ls_cod_prodotto, ls_cod_tipo_programma, &
       ls_des_prodotto, flag_prodotto_codificato, ls_cod_fornitore, ls_cod_misura, ls_flag_sconto_valore, ls_cod_stato, ls_flag_evaso, ls_des_estesa, &
		 ls_flag_budget, ls_tkordi, ls_flag_amministratore, ls_flag_blocco,ls_flag_budget_chiuso, ls_tkordi_rda, ls_flag_rda, ls_flag_budget_consolidato
		 
long ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, &
     ll_ore, ll_minuti, ll_priorita, ll_anno_riferimento, ll_prova
	  
dec{4} ld_quan_utilizzo, ld_prezzo_ricambio, ld_sconto_1_budget, ld_sconto_2_budget, ld_sconto_3_budget,ld_sconto_1_eff, ld_sconto_2_eff, ld_sconto_3_eff, &
		 ld_prezzo_netto_budget, ld_prezzo_netto_eff, ld_prezzo_ricambio_eff, ld_quan_consegnata, ld_quan_ordinata
		 
datetime ldt_data_consegna

setnull(ls_null)

ls_flag_blocco = "N"

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if ib_nuovo then
			
			// carico il codice PORTO di default
			select stringa
			into   :ls_cod_porto
			from   parametri_azienda
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_parametro = 'CPD';
			
			if sqlca.sqlcode = 0 then
				setitem(getrow(),"cod_porto", ls_cod_porto)
			end if	
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			else
				// cerco la riga principale
				//	Michela 20/06/2007: non è possibile guardare la riga di appartenenza perchè con il sort delle righe, i riferimenti vengono persi...siccome ho fatto tasto destro su qualcosa che esiste, allora
				//							   mi prendo l'anno e il numero di registrazione della riga di risorsa che è corretto...e in teoria sempre presente
				//ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			end if
			
			
			select priorita,
			       	 cod_tipo_programma,
					 cod_stato,
					 flag_budget,
					 anno_riferimento,
					 flag_capitalizzato
			into   :ll_priorita,
			          :ls_cod_tipo_programma,
					 :ls_cod_stato,
					 :ls_flag_budget,
					 :ll_anno_riferimento,
					 :ls_flag_capitalizzato
			from   programmi_manutenzione  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
				    anno_registrazione = :ll_anno_registrazione and
				    num_registrazione = :ll_num_registrazione ;
					 
			if isnull(ll_priorita) then ll_priorita = 1
			if ls_cod_tipo_programma = "" then setnull(ls_cod_tipo_programma)
			if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"
			if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
				ll_anno_riferimento = f_anno_riferimento()
				if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
					ll_anno_riferimento = year(today())
				end if
			end if
				 
				 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Dettaglio Manutenzione","Impossibile trovare la priorita della manutenzione: " + string(ll_anno_registrazione)+"/"+ string(ll_num_registrazione)+" "+ sqlca.sqlerrtext, StopSign!)
				return
			end if
			setitem(1, "priorita", ll_priorita)
			setitem(1, "cod_tipo_programma", ls_cod_tipo_programma)
			
			// *** leggo lo stato 
			
			select stringa
			into   :ls_cod_stato
			from   parametri
			where  cod_parametro = 'MSN';
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "OMNIA", "Attenzione: impostare il parametro MSN con codice stato attività parzialmente eseguita:" + sqlca.sqlerrtext, stopsign!)
				return -1
			end if
			
			if ls_cod_stato = "" then setnull(ls_cod_stato)	
			
			setitem(1,"cod_stato", ls_cod_stato)
			setitem( 1, "quan_consegnata", 0)
			setitem( 1, "flag_evaso", "N")
			
			//	*** per le nuove righe di ricambio, se l'anno di riferimento risulta consolidato, propongo il budget di default a N, altrimenti
			//		propongo quello dell'attività
			
			select flag_chiuso
			into	:ls_flag_budget_consolidato
			from	prog_manutenzioni_anni
			where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_riferimento = :ll_anno_riferimento;
					
			if sqlca.sqlcode = 0 and not isnull(ls_flag_budget_consolidato) and ls_flag_budget_consolidato = "S" then
				setitem( 1, "flag_budget", "N")
			else
				setitem( 1, "flag_budget", ls_flag_budget)
			end if			
						
			setitem( 1, "anno_riferimento", ll_anno_riferimento)
			setitem( 1, "flag_blocco", "N")
			setitem( 1, "flag_capitalizzato", ls_flag_capitalizzato)
			postevent("ue_controllo_anno_inizio")
			
		else		
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			ll_progressivo    = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			

			SELECT cod_prodotto,   
					des_prodotto,   
					quan_utilizzo,   
					prezzo_ricambio,   
					flag_prodotto_codificato,   
					cod_fornitore,   
					cod_misura,   
					data_consegna,   
					cod_porto,   
					flag_prezzo_certo,   
					flag_capitalizzato,
					priorita,
					sconto_1_budget,
					sconto_2_budget,
					sconto_3_budget,
					prezzo_netto_budget,
					sconto_1_eff,
					sconto_2_eff,
					sconto_3_eff,
					prezzo_netto_eff,					
					prezzo_ricambio_eff,
					flag_sconto_valore,
					cod_tipo_programma,
					cod_stato,
					quan_consegnata,
					flag_evaso,
					quan_eff,
					des_estesa,
					flag_budget,
					anno_riferimento,
					tkordi,
					tkordi_rda,
					flag_rda
			 INTO :ls_cod_prodotto,   
					:ls_des_prodotto,   
					:ld_quan_utilizzo,   
					:ld_prezzo_ricambio,   
					:flag_prodotto_codificato,   
					:ls_cod_fornitore,   
					:ls_cod_misura,   
					:ldt_data_consegna,   
					:ls_cod_porto,   
					:ls_flag_prezzo_certo,   
					:ls_flag_capitalizzato,
					:ll_priorita,
					:ld_sconto_1_budget,
					:ld_sconto_2_budget,
					:ld_sconto_3_budget,
					:ld_prezzo_netto_budget,
					:ld_sconto_1_eff,
					:ld_sconto_2_eff,
					:ld_sconto_3_eff,
					:ld_prezzo_netto_eff,					
					:ld_prezzo_ricambio_eff,
					:ls_flag_sconto_valore,
					:ls_cod_tipo_programma,
					:ls_cod_stato,
					:ld_quan_consegnata,
					:ls_flag_evaso,
					:ld_quan_ordinata,
					:ls_des_estesa,
					:ls_flag_budget,
					:ll_anno_riferimento,
					:ls_tkordi,
					:ls_tkordi_rda,
					:ls_flag_rda
			 FROM  prog_manutenzioni_ricambi
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ricambio = :ll_progressivo;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca risorsa interna~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			if isnull(ld_sconto_1_budget) then ld_sconto_1_budget = 0
			if isnull(ld_sconto_2_budget) then ld_sconto_2_budget = 0
			if isnull(ld_sconto_3_budget) then ld_sconto_3_budget = 0
			if isnull(ld_sconto_1_eff) then ld_sconto_1_eff = 0
			if isnull(ld_sconto_2_eff) then ld_sconto_2_eff = 0
			if isnull(ld_sconto_3_eff) then ld_sconto_3_eff = 0
			if isnull(ld_quan_consegnata) then ld_quan_consegnata = 0
			if isnull(ls_flag_evaso) or ls_flag_evaso = "" then ls_flag_evaso = "N"
			if isnull(ls_flag_rda) or ls_flag_rda = "" then ls_flag_rda = "N"

			setitem( getrow(),"cod_prodotto", ls_cod_prodotto)
			setitem( getrow(),"des_prodotto", ls_des_prodotto)
			setitem( getrow(),"quan_utilizzo", ld_quan_utilizzo)
			setitem( getrow(),"costo_unitario", ld_prezzo_ricambio)
			setitem( getrow(),"cod_fornitore", ls_cod_fornitore)
			setitem( getrow(),"cod_misura", ls_cod_misura)
			setitem( getrow(),"data_consegna", ldt_data_consegna)
			setitem( getrow(), "cod_porto", ls_cod_porto)
			setitem( getrow(), "flag_prezzo_certo", ls_flag_prezzo_certo)
			setitem( getrow(), "flag_capitalizzato", ls_flag_capitalizzato)
			setitem( getrow(), "priorita", ll_priorita)
			setitem( getrow(), "sconto_1_budget", ld_sconto_1_budget)
			setitem( getrow(), "sconto_2_budget", ld_sconto_2_budget)
			setitem( getrow(), "sconto_3_budget", ld_sconto_3_budget)
			setitem( getrow(), "prezzo_netto_budget", ld_prezzo_netto_budget)
			setitem( getrow(), "sconto_1_eff", ld_sconto_1_eff)
			setitem( getrow(), "sconto_2_eff", ld_sconto_2_eff)
			setitem( getrow(), "sconto_3_eff", ld_sconto_3_eff)
			setitem( getrow(), "costo_unitario_eff", ld_prezzo_ricambio_eff)
			setitem( getrow(), "prezzo_netto_eff", ld_prezzo_netto_eff)
			setitem( getrow(), "flag_sconto_valore", ls_flag_sconto_valore)
			setitem( getrow(), "cod_tipo_programma", ls_cod_tipo_programma)
			setitem( getrow(), "cod_stato", ls_cod_stato)
			setitem( getrow(), "quan_consegnata", ld_quan_consegnata)
			setitem( getrow(), "flag_evaso", ls_flag_evaso)
			setitem( getrow(), "quan_eff", ld_quan_ordinata)
			setitem( getrow(), "des_estesa", ls_des_estesa)
			if isnull(ls_flag_budget) then ls_flag_budget = "N"
			setitem( getrow(), "flag_budget", ls_flag_budget)
			setitem( getrow(), "anno_riferimento", ll_anno_riferimento)	
			
			setitem( getrow(), "flag_blocco", "N")			
			
			if s_cs_xx.cod_utente <> "CS_SYSTEM" then
				
				select flag_collegato
				into	:ls_flag_amministratore
				from	utenti
				where	cod_utente = :s_cs_xx.cod_utente;
				
				if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
					
				else
				
					if not isnull(ls_tkordi) and ls_tkordi <> "" then
						setitem( getrow(), "flag_blocco", "S")
						ls_flag_blocco = "S"
//						cb_1.enabled = false
						this.object.b_prodotto.enabled = false
						this.object.b_listini.enabled = false
					elseif not isnull(ls_tkordi_rda) and ls_tkordi_rda <> "" then
						
						
					else
						
						select count(*)
						into	:ll_prova
						from	prog_manutenzioni_ric_ddi
						where	cod_azienda = :s_cs_xx.cod_azienda and
								anno_registrazione = :ll_anno_registrazione and
								num_registrazione = :ll_num_registrazione and
								prog_riga_ricambio = :ll_progressivo;
								 
						if sqlca.sqlcode = 0 and not isnull(ll_prova) and ll_prova > 0 then
							
							
							setitem( getrow(), "flag_blocco", "S")
							ls_flag_blocco = "S"
//							cb_1.enabled = false
							this.object.b_prodotto.enabled = false
							this.object.b_listini.enabled = false
						else
							setitem( getrow(), "flag_blocco", "N")
						end if
						
					end if				
					
				end if
				
			end if
			
		end if
	end if
end if

//	*** Michela 23/05/2007: controllo il flag budget

accepttext()

ls_flag_budget = getitemstring( getrow(), "flag_budget")
if isnull(ls_flag_budget) or ls_flag_budget = "" then ls_flag_budget = "N"

if ls_flag_budget = "N" then									//	***		se non è a budget allora devo bloccare i campi
	
	if ib_nuovo or ls_flag_blocco = "N" then
		
		this.Object.quan_utilizzo.Protect=1
		this.Object.costo_unitario.Protect=1
		this.Object.sconto_1_budget.Protect=1
		this.Object.sconto_2_budget.Protect=1
		this.Object.sconto_3_budget.Protect=1
		this.Object.priorita.Protect=1
		this.Object.flag_prezzo_certo.Protect=1
		this.Object.quan_utilizzo.background.color=string(rgb(192,192,192))
		this.Object.costo_unitario.background.color=string(rgb(192,192,192))
		this.Object.sconto_1_budget.background.color=string(rgb(192,192,192))
		this.Object.sconto_2_budget.background.color=string(rgb(192,192,192))
		this.Object.sconto_3_budget.background.color=string(rgb(192,192,192))
		this.Object.priorita.background.color=string(rgb(192,192,192))
		this.Object.flag_prezzo_certo.background.color=string(rgb(192,192,192))
		
		this.Object.quan_eff.Protect=0
		this.Object.costo_unitario_eff.Protect=0
		this.Object.quan_consegnata.Protect=0
		this.Object.sconto_1_eff.Protect=0
		this.Object.sconto_2_eff.Protect=0
		this.Object.sconto_3_eff.Protect=0
		this.Object.quan_eff.background.color=string(rgb(255,255,255))
		this.Object.costo_unitario_eff.background.color=string(rgb(255,255,255))
		this.Object.quan_consegnata.background.color=string(rgb(255,255,255))
		this.Object.sconto_1_eff.background.color=string(rgb(255,255,255))
		this.Object.sconto_2_eff.background.color=string(rgb(255,255,255))
		this.Object.sconto_3_eff.background.color=string(rgb(255,255,255))				
		
//	else
//		
//		this.Object.quan_utilizzo.Protect=0
//		this.Object.costo_unitario.Protect=0
//		this.Object.sconto_1_budget.Protect=0
//		this.Object.sconto_2_budget.Protect=0
//		this.Object.sconto_3_budget.Protect=0
//		this.Object.priorita.Protect=0
//		this.Object.flag_prezzo_certo.Protect=0
//		this.Object.quan_utilizzo.background.color=string(rgb(255,255,255))
//		this.Object.costo_unitario.background.color=string(rgb(255,255,255))
//		this.Object.sconto_1_budget.background.color=string(rgb(255,255,255))
//		this.Object.sconto_2_budget.background.color=string(rgb(255,255,255))
//		this.Object.sconto_3_budget.background.color=string(rgb(255,255,255))
//		this.Object.priorita.background.color=string(rgb(255,255,255))
//		this.Object.flag_prezzo_certo.background.color=string(rgb(255,255,255))		
//		
//		this.Object.quan_eff.Protect=1
//		this.Object.costo_unitario_eff.Protect=1
//		this.Object.sconto_1_eff.Protect=1
//		this.Object.sconto_2_eff.Protect=1
//		this.Object.sconto_3_eff.Protect=1
//		this.Object.quan_eff.background.color=string(rgb(192,192,192))
//		this.Object.costo_unitario_eff.background.color=string(rgb(192,192,192))
//		this.Object.sconto_1_eff.background.color=string(rgb(192,192,192))
//		this.Object.sconto_2_eff.background.color=string(rgb(192,192,192))
//		this.Object.sconto_3_eff.background.color=string(rgb(192,192,192))						
	end if
	
else															//	***		altrimenti li sblocco
	if ib_nuovo then
		this.Object.quan_utilizzo.Protect=0
		this.Object.costo_unitario.Protect=0
		this.Object.sconto_1_budget.Protect=0
		this.Object.sconto_2_budget.Protect=0
		this.Object.sconto_3_budget.Protect=0			
		this.Object.priorita.Protect=0
		this.Object.flag_prezzo_certo.Protect=0	
		this.Object.quan_utilizzo.background.color=string(rgb(255,255,255))
		this.Object.costo_unitario.background.color=string(rgb(255,255,255))
		this.Object.sconto_1_budget.background.color=string(rgb(255,255,255))
		this.Object.sconto_2_budget.background.color=string(rgb(255,255,255))
		this.Object.sconto_3_budget.background.color=string(rgb(255,255,255))	
		this.Object.priorita.background.color=string(rgb(255,255,255))
		this.Object.flag_prezzo_certo.background.color=string(rgb(255,255,255))	
		
		this.Object.quan_eff.Protect=1
		this.Object.costo_unitario_eff.Protect=1
		this.Object.quan_consegnata.Protect=1
		this.Object.sconto_1_eff.Protect=1
		this.Object.sconto_2_eff.Protect=1
		this.Object.sconto_3_eff.Protect=1
		this.Object.quan_eff.background.color=string(rgb(192,192,192))
		this.Object.costo_unitario_eff.background.color=string(rgb(192,192,192))
		this.Object.quan_consegnata.background.color=string(rgb(192,192,192))
		this.Object.sconto_1_eff.background.color=string(rgb(192,192,192))
		this.Object.sconto_2_eff.background.color=string(rgb(192,192,192))
		this.Object.sconto_3_eff.background.color=string(rgb(192,192,192))								
		
	elseif ls_flag_blocco = "N" then
		
		select flag_chiuso
		into	:ls_flag_budget_chiuso
		from 	prog_manutenzioni_anni
		where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_riferimento = :ll_anno_riferimento;

		if not isnull(ls_flag_budget_chiuso) and ls_flag_budget_chiuso = "S" then
			
			this.Object.quan_utilizzo.Protect=1
			this.Object.costo_unitario.Protect=1
			this.Object.flag_sconto_valore.Protect=1
			this.Object.sconto_1_budget.Protect=1
			this.Object.sconto_2_budget.Protect=1
			this.Object.sconto_3_budget.Protect=1
			this.Object.priorita.Protect=1
			this.Object.flag_prezzo_certo.Protect=1
			this.Object.quan_utilizzo.background.color=string(rgb(192,192,192))
			this.Object.costo_unitario.background.color=string(rgb(192,192,192))
			this.Object.flag_sconto_valore.background.color=string(rgb(192,192,192))
			this.Object.sconto_1_budget.background.color=string(rgb(192,192,192))
			this.Object.sconto_2_budget.background.color=string(rgb(192,192,192))
			this.Object.sconto_3_budget.background.color=string(rgb(192,192,192))
			this.Object.priorita.background.color=string(rgb(192,192,192))
			this.Object.flag_prezzo_certo.background.color=string(rgb(192,192,192))		
			this.Object.quan_eff.Protect=1
			this.Object.costo_unitario_eff.Protect=1
			this.Object.quan_consegnata.Protect=1
			this.Object.sconto_1_eff.Protect=1
			this.Object.sconto_2_eff.Protect=1
			this.Object.sconto_3_eff.Protect=1
			this.Object.quan_eff.background.color=string(rgb(192,192,192))
			this.Object.costo_unitario_eff.background.color=string(rgb(192,192,192))
			this.Object.quan_consegnata.background.color=string(rgb(192,192,192))
			this.Object.sconto_1_eff.background.color=string(rgb(192,192,192))
			this.Object.sconto_2_eff.background.color=string(rgb(192,192,192))
			this.Object.sconto_3_eff.background.color=string(rgb(192,192,192))
			
		else
			
			this.Object.quan_utilizzo.Protect=0
			this.Object.costo_unitario.Protect=0
			this.Object.flag_sconto_valore.Protect=0
			this.Object.sconto_1_budget.Protect=0
			this.Object.sconto_2_budget.Protect=0
			this.Object.sconto_3_budget.Protect=0			
			this.Object.priorita.Protect=0
			this.Object.flag_prezzo_certo.Protect=0	
			this.Object.quan_utilizzo.background.color=string(rgb(255,255,255))
			this.Object.costo_unitario.background.color=string(rgb(255,255,255))
			this.Object.flag_sconto_valore.background.color=string(rgb(255,255,255))
			this.Object.sconto_1_budget.background.color=string(rgb(255,255,255))
			this.Object.sconto_2_budget.background.color=string(rgb(255,255,255))
			this.Object.sconto_3_budget.background.color=string(rgb(255,255,255))	
			this.Object.priorita.background.color=string(rgb(255,255,255))
			this.Object.flag_prezzo_certo.background.color=string(rgb(255,255,255))	
			
			this.Object.quan_eff.Protect=1
			this.Object.costo_unitario_eff.Protect=1
			this.Object.quan_consegnata.Protect=1
			this.Object.sconto_1_eff.Protect=1
			this.Object.sconto_2_eff.Protect=1
			this.Object.sconto_3_eff.Protect=1
			this.Object.quan_eff.background.color=string(rgb(192,192,192))
			this.Object.costo_unitario_eff.background.color=string(rgb(192,192,192))
			this.Object.quan_consegnata.background.color=string(rgb(192,192,192))
			this.Object.sconto_1_eff.background.color=string(rgb(192,192,192))
			this.Object.sconto_2_eff.background.color=string(rgb(192,192,192))
			this.Object.sconto_3_eff.background.color=string(rgb(192,192,192))									
			
		end if
			
	end if
end if

select flag_collegato
into	:ls_flag_amministratore
from	utenti
where	cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode = 0 and not isnull(ls_flag_amministratore) and ls_flag_amministratore = "S" then
else	
	this.Object.quan_consegnata.Protect=1
	this.Object.flag_budget.Protect=1
	this.Object.anno_riferimento.Protect=1
	this.Object.flag_evaso.Protect=1
	this.Object.quan_consegnata.background.color=string(rgb(192,192,192))
	this.Object.flag_budget.background.color=string(rgb(192,192,192))
	this.Object.anno_riferimento.background.color=string(rgb(192,192,192))	
	this.Object.flag_evaso.background.color=string(rgb(192,192,192))
end if

if ib_nuovo then
else
	
	if f_utente_rda() and ls_flag_rda = "N" then
//		cb_1.enabled = false
		this.object.cod_prodotto.protect = 1
		this.object.b_prodotto.enabled = false
		this.object.b_listini.enabled = false
		this.Object.quan_utilizzo.Protect=1
		this.Object.costo_unitario.Protect=1
		this.Object.flag_sconto_valore.Protect=1
		this.Object.sconto_1_budget.Protect=1
		this.Object.sconto_2_budget.Protect=1
		this.Object.sconto_3_budget.Protect=1
		this.Object.priorita.Protect=1
		this.Object.flag_prezzo_certo.Protect=1
		this.Object.quan_utilizzo.background.color=string(rgb(192,192,192))
		this.Object.costo_unitario.background.color=string(rgb(192,192,192))
		this.Object.flag_sconto_valore.background.color=string(rgb(192,192,192))
		this.Object.sconto_1_budget.background.color=string(rgb(192,192,192))
		this.Object.sconto_2_budget.background.color=string(rgb(192,192,192))
		this.Object.sconto_3_budget.background.color=string(rgb(192,192,192))
		this.Object.priorita.background.color=string(rgb(192,192,192))
		this.Object.flag_prezzo_certo.background.color=string(rgb(192,192,192))		
		this.Object.quan_eff.Protect=1
		this.Object.costo_unitario_eff.Protect=1
		this.Object.sconto_1_eff.Protect=1
		this.Object.sconto_2_eff.Protect=1
		this.Object.sconto_3_eff.Protect=1
		this.Object.quan_eff.background.color=string(rgb(192,192,192))
		this.Object.costo_unitario_eff.background.color=string(rgb(192,192,192))
		this.Object.sconto_1_eff.background.color=string(rgb(192,192,192))
		this.Object.sconto_2_eff.background.color=string(rgb(192,192,192))
		this.Object.sconto_3_eff.background.color=string(rgb(192,192,192))	
		this.object.cod_fornitore.Protect=1
		this.object.cod_fornitore.background.color=string(rgb(192,192,192))
		this.object.cod_prodotto.Protect=1
		this.object.cod_prodotto.background.color=string(rgb(192,192,192))	
		this.object.des_prodotto.Protect=1
		this.object.des_prodotto.background.color=string(rgb(192,192,192))	
		this.object.cod_misura.Protect=1
		this.object.cod_misura.background.color=string(rgb(192,192,192))		
		this.object.cod_porto.Protect=1
		this.object.cod_porto.background.color=string(rgb(192,192,192))
		this.object.data_consegna.Protect=1
		this.object.data_consegna.background.color=string(rgb(192,192,192))		
	end if
	
end if

postevent("ue_listini")
end event

event buttonclicked;call super::buttonclicked;
string								ls_cod_misura, ls_flag_budget
uo_progen_documenti 		luo_obj
window							lw_window

choose case dwo.name
	case "b_prodotto"
		setcolumn("cod_prodotto")
		guo_ricerca.uof_set_response( parent )
		guo_ricerca.uof_ricerca_prodotto(dw_ricambio, "cod_prodotto")
		
		
	case "b_ricerca_fornitore"
		setcolumn("cod_fornitore")
		guo_ricerca.uof_set_response( parent )
		guo_ricerca.uof_ricerca_fornitore(dw_ricambio, "cod_fornitore")
		
		
	case "b_listini"
		
		if getitemstring( getrow(), "flag_blocco") = "N" then
			s_cs_xx.parametri.parametro_s_10 = getitemstring( row, "cod_fornitore")
			s_cs_xx.parametri.parametro_data_1	= getitemdatetime( row, "data_consegna")
			s_cs_xx.parametri.parametro_s_13 = "S"
			
			ls_flag_budget = getitemstring( row, "flag_budget")
			luo_obj = create uo_progen_documenti
			if luo_obj.f_registro( true, tran_import) <> 0 then 
				destroy luo_obj;
				return		
			end if
			
			window_type_open_parm( lw_window, "w_prog_manutenzioni_ddt_listini", 0, tran_import)
			luo_obj.f_registro( false, tran_import)
			destroy luo_obj;
	
			if not isnull(s_cs_xx.parametri.parametro_s_10) then setitem( row, "cod_prodotto", s_cs_xx.parametri.parametro_s_10)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then setitem( row, "des_prodotto", s_cs_xx.parametri.parametro_s_11)
			if not isnull(s_cs_xx.parametri.parametro_d_1) then 
				
				if not isnull(s_cs_xx.parametri.parametro_dec4_1) and s_cs_xx.parametri.parametro_dec4_1 > 0 then
					s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_1 )/ 100)
				end if
				
				if not isnull(s_cs_xx.parametri.parametro_dec4_2) and s_cs_xx.parametri.parametro_dec4_2 > 0 then
					s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_2 )/ 100)
				end if
				
				if not isnull(s_cs_xx.parametri.parametro_dec4_3) and s_cs_xx.parametri.parametro_dec4_3 > 0 then
					s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_3 )/ 100)
				end if
				
				if not isnull(s_cs_xx.parametri.parametro_dec4_4) and s_cs_xx.parametri.parametro_dec4_4 > 0 then
					s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_4 )/ 100)
				end if
				
				if not isnull(s_cs_xx.parametri.parametro_dec4_5) and s_cs_xx.parametri.parametro_dec4_5 > 0 then
					s_cs_xx.parametri.parametro_d_1 = s_cs_xx.parametri.parametro_d_1 - (( s_cs_xx.parametri.parametro_d_1 * s_cs_xx.parametri.parametro_dec4_5 )/ 100)
				end if			
				
				if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
					setitem( row, "costo_unitario", s_cs_xx.parametri.parametro_d_1)
				else
					setitem( row, "costo_unitario_eff", s_cs_xx.parametri.parametro_d_1)
				end if
				
			end if
			
			if not isnull(s_cs_xx.parametri.parametro_d_1) then 
				if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
					setitem( row, "sconto_1_budget", 0)
				else
					setitem( row, "sconto_1_eff", 0)
				end if
			end if
			
			if not isnull(s_cs_xx.parametri.parametro_d_1) then 
				if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
					setitem( row, "sconto_2_budget", 0)
				else
					setitem( row, "sconto_2_eff", 0)
				end if
			end if
			
			if not isnull(s_cs_xx.parametri.parametro_d_1) then 
				if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
					setitem( row, "sconto_3_budget", 0)
				else
					setitem( row, "sconto_3_eff", 0)
				end if
			end if
			
			if not isnull(s_cs_xx.parametri.parametro_d_1) then 
				if not isnull(ls_flag_budget) and ls_flag_budget = "S" then
					setitem( row, "prezzo_netto_budget", s_cs_xx.parametri.parametro_d_1)
				else
					setitem( row, "prezzo_netto_eff", s_cs_xx.parametri.parametro_d_1)
				end if
			end if			
			
			if not isnull(s_cs_xx.parametri.parametro_s_12) then 
				
				select cod_misura
				into	:ls_cod_misura
				from	tab_misure
				where	cod_azienda = :s_cs_xx.cod_azienda and
						cdunim = :s_cs_xx.parametri.parametro_s_12;
						
				if sqlca.sqlcode = 0 and not isnull(ls_cod_misura) and ls_cod_misura <> "" then			
					setitem( row, "cod_misura", ls_cod_misura)
				end if
				
			end if
			
			accepttext()
		end if
		
end choose
end event

