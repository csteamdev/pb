﻿$PBExportHeader$w_programmi_manutenzione_risorse.srw
$PBExportComments$Finestra Manutenzioni personalizzata Guerrato
forward
global type w_programmi_manutenzione_risorse from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_programmi_manutenzione_risorse
end type
type cb_salva from commandbutton within w_programmi_manutenzione_risorse
end type
type dw_risorse from uo_cs_xx_dw within w_programmi_manutenzione_risorse
end type
end forward

global type w_programmi_manutenzione_risorse from w_cs_xx_risposta
integer width = 2629
integer height = 956
string title = "Aggiunta Fornitore Risorsa Esterna"
event ue_carica ( )
cb_annulla cb_annulla
cb_salva cb_salva
dw_risorse dw_risorse
end type
global w_programmi_manutenzione_risorse w_programmi_manutenzione_risorse

type variables
boolean ib_nuovo=false
long il_rbuttonrow
datawindow idw_parent
string is_cod_area_aziendale
end variables

event ue_carica();string ls_null

f_PO_LoadDDDW_DW(dw_risorse,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore IN (select cod_fornitore from tab_aree_risorse where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + is_cod_area_aziendale + "' and cod_fornitore is not null) ")
					  
dw_risorse.setitem( dw_risorse.getrow(), "cod_fornitore", ls_null)					  
end event

event pc_setwindow;call super::pc_setwindow;if upper(s_cs_xx.parametri.parametro_s_1) = "N" then
	ib_nuovo = true
else
	ib_nuovo = false
end if

save_on_close(c_socnosave)

dw_risorse.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1


long   ll_anno_registrazione, ll_num_registrazione, ll_row
string ls_cod_attrezzatura

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
	ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
end if


select cod_attrezzatura
into   :ls_cod_attrezzatura
from   programmi_manutenzione  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione ;
		 
select cod_area_aziendale
into   :is_cod_area_aziendale
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura;
			
s_cs_xx.parametri.parametro_b_1 = false			
end event

on w_programmi_manutenzione_risorse.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_salva=create cb_salva
this.dw_risorse=create dw_risorse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_salva
this.Control[iCurrent+3]=this.dw_risorse
end on

on w_programmi_manutenzione_risorse.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_salva)
destroy(this.dw_risorse)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_risorse,"cod_tipo_programma",sqlca,&
                 "tab_tipi_programmi","cod_tipo_programma","des_tipo_programma", &
                 "")			
					  
f_PO_LoadDDDW_DW( dw_risorse, &
						"cod_stato", &
						sqlca, &
						"tab_stato_programmi", &
						"cod_stato", &
						"des_stato", &
						" ")											  
end event

type cb_annulla from commandbutton within w_programmi_manutenzione_risorse
integer x = 1778
integer y = 712
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;
s_cs_xx.parametri.parametro_b_1 = false
close(parent)
end event

type cb_salva from commandbutton within w_programmi_manutenzione_risorse
integer x = 2171
integer y = 712
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long ll_row,ll_anno_registrazione,ll_num_registrazione ,ll_progressivo,ll_ore,ll_minuti, ll_priorita, ll_ore_eff, ll_minuti_eff
string ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna,ls_flag_prezzo_certo, ls_flag_eseguita, &
       ls_flag_capitalizzato, ls_cod_fornitore, ls_cod_tipo_programma, ls_cod_stato
		 
dec{4} ld_risorsa_costo_orario,ld_risorsa_costi_aggiuntivi
datetime ldt_data_consegna
dw_risorse.accepttext()

if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
	ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
	ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
else
	// cerco la riga principale
	ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
	if ib_nuovo then
		ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
	else
		ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
	end if
end if

if isnull(ls_cod_fornitore) then
	g_mb.messagebox("OMNIA", "Indicazione FORNITORE obbligatoria.")
	rollback;
	return
end if
	

ls_cod_fornitore = dw_risorse.getitemstring(dw_risorse.getrow(), "cod_fornitore")
ls_cod_cat_risorse_esterne = dw_risorse.getitemstring(dw_risorse.getrow(), "cod_cat_risorse_esterne")
ls_cod_risorsa_esterna = dw_risorse.getitemstring(dw_risorse.getrow(), "cod_risorsa_esterna")
ll_ore  = dw_risorse.getitemnumber(dw_risorse.getrow(), "ore")
ll_minuti  = dw_risorse.getitemnumber(dw_risorse.getrow(), "minuti")
ld_risorsa_costo_orario = dw_risorse.getitemnumber(dw_risorse.getrow(), "risorsa_costo_orario")
ld_risorsa_costi_aggiuntivi = dw_risorse.getitemnumber(dw_risorse.getrow(), "risorsa_costi_aggiuntivi")
ls_flag_prezzo_certo = dw_risorse.getitemstring(dw_risorse.getrow(), "flag_prezzo_certo")
ls_flag_capitalizzato = dw_risorse.getitemstring(dw_risorse.getrow(), "flag_capitalizzato")
ll_priorita = dw_risorse.getitemnumber( dw_risorse.getrow(), "priorita")
ls_flag_eseguita = dw_risorse.getitemstring( dw_risorse.getrow(), "flag_eseguita")
ll_ore_eff = dw_risorse.getitemnumber( dw_risorse.getrow(), "ore_eff")
ll_minuti_eff = dw_risorse.getitemnumber( dw_risorse.getrow(), "minuti_eff")
ls_cod_tipo_programma = dw_risorse.getitemstring( dw_risorse.getrow(), "cod_tipo_programma")
ls_cod_stato = dw_risorse.getitemstring( dw_risorse.getrow(), "cod_stato")

if isnull(ld_risorsa_costi_aggiuntivi) then ld_risorsa_costi_aggiuntivi = 0
if isnull(ld_risorsa_costo_orario) then ld_risorsa_costo_orario = 0
if isnull(ll_minuti) then ll_minuti = 0
if isnull(ll_ore) then ll_ore = 0
if isnull(ll_minuti_eff) then ll_minuti_eff = 0
if isnull(ll_ore_eff) then ll_ore_eff = 0

if ib_nuovo then

	select max(progressivo)
	into   :ll_progressivo
	from   prog_manutenzioni_risorse_est
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :ll_anno_registrazione and 
			 num_registrazione = :ll_num_registrazione;
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	INSERT INTO prog_manutenzioni_risorse_est
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  progressivo,
			  cod_cat_risorse_esterne,
			  cod_risorsa_esterna,
			  ore,
			  minuti,
			  risorsa_costo_orario,
			  risorsa_costi_aggiuntivi,
			  flag_prezzo_certo,
			  flag_capitalizzato,
			  cod_fornitore,
			  priorita,
			  flag_eseguita,
			  ore_eff,
			  minuti_eff,
			  cod_tipo_programma,
			  cod_stato)
	VALUES (:s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,
			  :ll_progressivo,
			  :ls_cod_cat_risorse_esterne,
			  :ls_cod_risorsa_esterna,
			  :ll_ore,
			  :ll_minuti,
			  :ld_risorsa_costo_orario,
			  :ld_risorsa_costi_aggiuntivi,
			  :ls_flag_prezzo_certo,
			  :ls_flag_capitalizzato,
			  :ls_cod_fornitore,
			  :ll_priorita,
			  :ls_flag_eseguita,
			  :ll_ore_eff,
			  :ll_minuti_eff,
			  :ls_cod_tipo_programma,
			  :ls_cod_stato)  ;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in creazione ricambio~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
else
	
	ll_progressivo  = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")

	update prog_manutenzioni_risorse_est
	set    cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne,
		    cod_risorsa_esterna = :ls_cod_risorsa_esterna,
		    ore = :ll_ore,
		    minuti = :ll_minuti,
		    risorsa_costo_orario = :ld_risorsa_costo_orario,
		    risorsa_costi_aggiuntivi = :ld_risorsa_costi_aggiuntivi,
		  	 flag_prezzo_certo = :ls_flag_prezzo_certo,
		    flag_capitalizzato = :ls_flag_capitalizzato,
		    cod_fornitore = :ls_cod_fornitore,
		    priorita = :ll_priorita,
		    flag_eseguita = :ls_flag_eseguita,
		    ore_eff = :ll_ore_eff,
		    minuti_eff = :ll_minuti_eff,
		    cod_tipo_programma = :ls_cod_tipo_programma,
		    cod_stato = :ls_cod_stato
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 progressivo = :ll_progressivo;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in modifica ricambio~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if
commit;
s_cs_xx.parametri.parametro_b_1 = true
close(parent)
		  

end event

type dw_risorse from uo_cs_xx_dw within w_programmi_manutenzione_risorse
integer x = 23
integer width = 2514
integer height = 660
integer taborder = 10
string dataobject = "d_programmi_manutenzione_grid_risorse"
end type

event itemchanged;call super::itemchanged;string ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_null,ls_cod_fornitore, ls_flag_blocco
dec{4} ld_costo_orario
setnull(ls_null)

if i_extendmode then
	choose case i_colname
			
		case "cod_fornitore"
			
			select flag_blocco 
			into   :ls_flag_blocco
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_fornitore = :i_coltext;
			
			if sqlca.sqlcode = 0 then
				if not isnull(ls_flag_blocco) and ls_flag_blocco = "S" then
					g_mb.messagebox( "OMNIA", "Attenzione: il fornitore scelto risulta bloccato!")
				end if
			end if
			
		case "cod_cat_risorse_esterne"
			f_PO_LoadDDDW_DW(dw_risorse,"cod_risorsa_esterna",sqlca,&
								  "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
								  
		case "cod_risorsa_esterna"
			
			ls_cod_cat_risorse_esterne = getitemstring(getrow(),"cod_cat_risorse_esterne")
			
			select cod_fornitore
			into   :ls_cod_fornitore
			from   anag_risorse_esterne
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
					 cod_risorsa_esterna = :i_coltext;
					 
			setitem(getrow(),"cod_fornitore", ls_cod_fornitore)
			
end choose
end if
end event

event pcd_new;call super::pcd_new;string ls_null, ls_cod_risorsa_esterna, ls_stringa, ls_flag_capitalizzato, ls_flag_prezzo_certo, &
       ls_cod_fornitore,ls_cod_cat_risorse_esterne, ls_flag_eseguita, ls_cod_tipo_programma, ls_cod_stato
long ll_row, ll_cont, ll_anno_registrazione, ll_num_registrazione, ll_progressivo, &
     ll_ore, ll_minuti, ll_priorita, ll_ore_eff, ll_minuti_eff
dec{4} ld_risorsa_costo_orario, ld_risorsa_costi_aggiuntivi

setnull(ls_null)

//parent.triggerevent("ue_carica")

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		if not ib_nuovo then						
			
			ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
			ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			ll_progressivo    = idw_parent.getitemnumber(il_rbuttonrow, "progressivo")
			

			SELECT cod_cat_risorse_esterne,   
					cod_risorsa_esterna,   
					ore,   
					minuti,
					risorsa_costo_orario,   
					risorsa_costi_aggiuntivi,
					flag_prezzo_certo,   
					flag_capitalizzato,
					cod_fornitore,
					priorita,
					flag_eseguita,
					ore_eff,
					minuti_eff,
					cod_tipo_programma,
					cod_stato
			 INTO :ls_cod_cat_risorse_esterne,   
					:ls_cod_risorsa_esterna,   
					:ll_ore,   
					:ll_minuti,
					:ld_risorsa_costo_orario,   
					:ld_risorsa_costi_aggiuntivi,
					:ls_flag_prezzo_certo,   
					:ls_flag_capitalizzato,
					:ls_cod_fornitore,
					:ll_priorita,
					:ls_flag_eseguita,
					:ll_ore_eff,
					:ll_minuti_eff,
					:ls_cod_tipo_programma,
					:ls_cod_stato
			from  prog_manutenzioni_risorse_est
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 progressivo = :ll_progressivo;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca risorsa interna~r~n"+sqlca.sqlerrtext)
				return
			end if
			
			setitem(getrow(),"cod_cat_risorse_esterne", ls_cod_cat_risorse_esterne)
			setitem(getrow(),"cod_risorsa_esterna", ls_cod_risorsa_esterna)
			setitem(getrow(),"ore", ll_ore)
			setitem(getrow(),"minuti", ll_minuti)
			setitem(getrow(),"flag_prezzo_certo", ls_flag_prezzo_certo)
			setitem(getrow(),"flag_capitalizzato", ls_flag_capitalizzato)
			setitem(getrow(),"risorsa_costo_orario", ld_risorsa_costo_orario)
			setitem(getrow(),"risorsa_costi_aggiuntivi", ld_risorsa_costi_aggiuntivi)
			setitem(getrow(),"flag_prezzo_certo", ls_flag_prezzo_certo)
			setitem(getrow(),"flag_capitalizzato", ls_flag_capitalizzato)
			setitem(getrow(),"cod_fornitore", ls_cod_fornitore)
			setitem(getrow(),"flag_eseguita", ls_flag_eseguita)
			setitem(getrow(),"ore_eff", ll_ore_eff)
			setitem(getrow(),"minuti_eff", ll_minuti_eff)
			setitem(getrow(),"priorita", ll_priorita)
			setitem(getrow(),"cod_tipo_programma", ls_cod_tipo_programma)
			setitem(getrow(),"cod_stato", ls_cod_stato)
		
		else
			if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") = "P" then
				ll_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")
			else
				// cerco la riga principale
				ll_row = idw_parent.getitemnumber(il_rbuttonrow, "num_riga_appartenenza")
				ll_anno_registrazione = idw_parent.getitemnumber(ll_row, "anno_registrazione")
				ll_num_registrazione  = idw_parent.getitemnumber(ll_row, "num_registrazione")
			end if
			
			
			select priorita,
					 cod_tipo_programma,
					 cod_stato
			into   :ll_priorita,
					 :ls_cod_tipo_programma,
					 :ls_cod_stato
			from   programmi_manutenzione  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
				    anno_registrazione = :ll_anno_registrazione and
				    num_registrazione = :ll_num_registrazione ;
				 
				 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Dettaglio Manutenzione","Impossibile trovare la priorita della manutenzione: " + string(ll_anno_registrazione)+"/"+ string(ll_num_registrazione)+" "+ sqlca.sqlerrtext, StopSign!)
				return
			end if
			setitem( 1, "priorita", ll_priorita)
			setitem( 1, "cod_tipo_programma", ls_cod_tipo_programma)
			
			// *** leggo lo stato 
			
			select stringa
			into   :ls_cod_stato
			from   parametri
			where  cod_parametro = 'MSN';
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "OMNIA", "Attenzione: impostare il parametro MSN con codice stato attività parzialmente eseguita:" + sqlca.sqlerrtext, stopsign!)
				return -1
			end if
			
			if ls_cod_stato = "" then setnull(ls_cod_stato)	
			
			setitem(getrow(),"cod_stato", ls_cod_stato)			
			setitem( 1, "flag_eseguita", "N")
	
		end if
		
		accepttext()

	end if
end if
		





end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_risorse,"cod_fornitore")
end choose
end event

