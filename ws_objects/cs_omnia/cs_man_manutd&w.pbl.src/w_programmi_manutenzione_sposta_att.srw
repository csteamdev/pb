﻿$PBExportHeader$w_programmi_manutenzione_sposta_att.srw
forward
global type w_programmi_manutenzione_sposta_att from w_cs_xx_risposta
end type
type st_1 from statictext within w_programmi_manutenzione_sposta_att
end type
type st_attivita from statictext within w_programmi_manutenzione_sposta_att
end type
type cb_2 from commandbutton within w_programmi_manutenzione_sposta_att
end type
type cb_salva from commandbutton within w_programmi_manutenzione_sposta_att
end type
type dw_attivita from uo_cs_xx_dw within w_programmi_manutenzione_sposta_att
end type
end forward

global type w_programmi_manutenzione_sposta_att from w_cs_xx_risposta
integer width = 2738
integer height = 892
string title = "Sposta Attività"
st_1 st_1
st_attivita st_attivita
cb_2 cb_2
cb_salva cb_salva
dw_attivita dw_attivita
end type
global w_programmi_manutenzione_sposta_att w_programmi_manutenzione_sposta_att

type variables
boolean ib_nuovo
long il_rbuttonrow,	il_anno_registrazione, il_num_registrazione
datawindow idw_parent
end variables

forward prototypes
public subroutine wf_controlla_risorse ()
end prototypes

public subroutine wf_controlla_risorse ();long	   ll_anno, ll_numero, ll_cont
string	ls_aperti, ls_cod_stato

if ib_nuovo then return

ls_aperti = ""

declare cu_stati cursor for
select  cod_stato
from 	  tab_stato_programmi
where   flag_tipo_stato = 'N';

open cu_stati;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore degli stati:" + sqlca.sqlerrtext)
	return
end if

do while true
	fetch cu_stati into :ls_cod_stato;
							  
	if sqlca.sqlcode = 100 then exit;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante le fetch del cursore degli stati:" + sqlca.sqlerrtext)
		close cu_stati;
		rollback;
		return 
	end if
	
	ls_aperti += ls_cod_stato + ","
	
loop

close cu_stati;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore degli stati:" + sqlca.sqlerrtext)
	rollback;
	return
end if

if right( ls_aperti, 1) = "," then ls_aperti = mid( ls_aperti, 1, len(ls_aperti) - 1)

ll_anno = idw_parent.getitemnumber( il_rbuttonrow, "anno_registrazione")
ll_numero = idw_parent.getitemnumber( il_rbuttonrow, "num_registrazione")
			
ll_cont = 0

select count(*)
into   :ll_cont
from   prog_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_numero and
		 ( cod_stato IN ( :ls_aperti ) or cod_stato IS NULL);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il conteggio delle righe dei ricambi:" +  sqlca.sqlerrtext)
	return
end if

if not isnull(ll_cont) and ll_cont > 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: esistono delle righe di ricambio prive di stato o con stato ancora aperto!")
	return
end if

ll_cont = 0

select count(*)
into   :ll_cont
from   prog_manutenzioni_risorse_est
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno and
		 num_registrazione = :ll_numero and
		 ( cod_stato IN ( :ls_aperti ) or cod_stato IS NULL);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il conteggio delle righe delle risorse esterne:" +  sqlca.sqlerrtext)
	return
end if

if not isnull(ll_cont) and ll_cont > 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: esistono delle righe di risorsa esterna prive di stato o con stato ancora aperto!")
	return
end if

ll_cont = 0		 

select count(*)
into   :ll_cont
from   prog_manutenzioni_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno and
		 num_registrazione = :ll_numero and
		 ( cod_stato IN ( :ls_aperti ) or cod_stato IS NULL);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il conteggio delle righe delle risorse interne:" +  sqlca.sqlerrtext)
	return
end if

if not isnull(ll_cont) and ll_cont > 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: esistono delle righe di risorsa interna prive di stato o con stato ancora aperto!")
	return
end if

end subroutine

event pc_setwindow;call super::pc_setwindow;save_on_close(c_socnosave)

dw_attivita.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen + c_noretrieveonopen, &
								  c_NORESIZEDW)
								  
il_rbuttonrow = s_cs_xx.parametri.parametro_d_1
idw_parent = s_cs_xx.parametri.parametro_dw_1

s_cs_xx.parametri.parametro_b_1 = false
end event

on w_programmi_manutenzione_sposta_att.create
int iCurrent
call super::create
this.st_1=create st_1
this.st_attivita=create st_attivita
this.cb_2=create cb_2
this.cb_salva=create cb_salva
this.dw_attivita=create dw_attivita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.st_attivita
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_salva
this.Control[iCurrent+5]=this.dw_attivita
end on

on w_programmi_manutenzione_sposta_att.destroy
call super::destroy
destroy(this.st_1)
destroy(this.st_attivita)
destroy(this.cb_2)
destroy(this.cb_salva)
destroy(this.dw_attivita)
end on

type st_1 from statictext within w_programmi_manutenzione_sposta_att
integer x = 23
integer y = 380
integer width = 2651
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
string text = "Selezionare l~'attrezzatura sulla quale si intende spostare l~'attività."
boolean focusrectangle = false
end type

type st_attivita from statictext within w_programmi_manutenzione_sposta_att
integer x = 23
integer y = 20
integer width = 2651
integer height = 340
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_programmi_manutenzione_sposta_att
integer x = 1879
integer y = 680
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;

close(parent)
end event

type cb_salva from commandbutton within w_programmi_manutenzione_sposta_att
integer x = 2272
integer y = 680
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sposta"
end type

event clicked;string		ls_cod_attrezzatura_new, ls_cod_attrezzatura, ls_cod_tipo_manutenzione
long		ll_prova

dw_attivita.accepttext()

if isnull(il_anno_registrazione) or il_anno_registrazione = 0 or isnull(il_num_registrazione) or il_num_registrazione = 0 then
	g_mb.messagebox("OMNIA", "Attenzione: selezionare un'attività per effettuare lo spostamento da un'attrezzatura all'altra!", stopsign!)
	return 
end if

ls_cod_attrezzatura_new = dw_attivita.getitemstring( dw_attivita.getrow(), "cod_attrezzatura")

select cod_attrezzatura,
          cod_tipo_manutenzione
into    :ls_cod_attrezzatura,
         :ls_cod_tipo_manutenzione
from   programmi_manutenzione
where cod_azienda = :s_cs_xx.cod_azienda and
         anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca dei dati dell'attività:" + sqlca.sqlerrtext, stopsign!)
	return 
end if

select count(*)
into    :ll_prova
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
           cod_attrezzatura = :ls_cod_attrezzatura_new and
		  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		  
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante il conteggio del tipo manutenzione " + ls_cod_tipo_manutenzione + " per l'attrezzatura " + ls_cod_attrezzatura_new + ": " + sqlca.sqlerrtext, stopsign!)
	return 
end if	

if isnull(ll_prova) then ll_prova = 0 

if ll_prova = 0 then
	
	  INSERT INTO tab_tipi_manutenzione  
				( cod_azienda,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  des_tipo_manutenzione,   
				  tempo_previsto,   
				  flag_manutenzione,   
				  flag_ricambio_codificato,   
				  cod_ricambio,   
				  cod_ricambio_alternativo,   
				  des_ricambio_non_codificato,   
				  num_reg_lista,   
				  num_versione,   
				  num_edizione,   
				  modalita_esecuzione,   
				  cod_tipo_ord_acq,   
				  cod_tipo_off_acq,   
				  cod_tipo_det_acq,   
				  cod_tipo_movimento,   
				  periodicita,   
				  frequenza,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_primario,   
				  cod_misura,   
				  val_min_taratura,   
				  val_max_taratura,   
				  valore_atteso,   
				  cod_versione,   
				  flag_blocco,   
				  data_ultima_modifica,   
				  des_tipo_manutenzione_storico,   
				  data_blocco,   
				  cod_tipo_lista_dist,   
				  cod_lista_dist,   
				  cod_operaio,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  data_inizio_val_1,   
				  data_fine_val_1,   
				  data_inizio_val_2,   
				  data_fine_val_2 )  	
	select       cod_azienda,   
				  :ls_cod_attrezzatura_new,   
				  cod_tipo_manutenzione,   
				  des_tipo_manutenzione,   
				  tempo_previsto,   
				  flag_manutenzione,   
				  flag_ricambio_codificato,   
				  cod_ricambio,   
				  cod_ricambio_alternativo,   
				  des_ricambio_non_codificato,   
				  num_reg_lista,   
				  num_versione,   
				  num_edizione,   
				  modalita_esecuzione,   
				  cod_tipo_ord_acq,   
				  cod_tipo_off_acq,   
				  cod_tipo_det_acq,   
				  cod_tipo_movimento,   
				  periodicita,   
				  frequenza,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_primario,   
				  cod_misura,   
				  val_min_taratura,   
				  val_max_taratura,   
				  valore_atteso,   
				  cod_versione,   
				  flag_blocco,   
				  data_ultima_modifica,   
				  des_tipo_manutenzione_storico,   
				  data_blocco,   
				  cod_tipo_lista_dist,   
				  cod_lista_dist,   
				  cod_operaio,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  data_inizio_val_1,   
				  data_fine_val_1,   
				  data_inizio_val_2,   
				  data_fine_val_2	
	from         tab_tipi_manutenzione
	where       cod_azienda = :s_cs_xx.cod_azienda and
	               cod_attrezzatura = :ls_cod_attrezzatura and
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento della tipologia di manutenzione:" + sqlca.sqlerrtext, stopsign!)
		rollback;
		g_mb.messagebox( "OMNIA", "Operazione Annullata!", stopsign!)
		return 
	end if
	
end if

update programmi_manutenzione
set      cod_attrezzatura = :ls_cod_attrezzatura_new
where  cod_azienda = :s_cs_xx.cod_azienda and
          anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'aggiornamento dell'attrezzatura nell'attività: " + sqlca.sqlerrtext, stopsign!)
	rollback;
	g_mb.messagebox( "OMNIA", "Operazione Annullata!", stopsign!)
	return
end if

commit;
g_mb.messagebox( "OMNIA", "Operazione Terminata!")
s_cs_xx.parametri.parametro_b_1 = true
close(parent)
		  

end event

type dw_attivita from uo_cs_xx_dw within w_programmi_manutenzione_sposta_att
integer x = 23
integer y = 480
integer width = 2651
integer height = 120
integer taborder = 10
string dataobject = "d_attrezzatura_singola"
end type

event itemchanged;call super::itemchanged;string ls_null, ls_cod_attrezzatura,ls_des_tipo_intervento,ls_modalita,ls_periodicita, ls_blocco
long ll_frequenza

setnull(ls_null)

choose case i_colname
		
	case "cod_attrezzatura"
		
		if not isnull(i_coltext) and i_coltext <> "" then
		
			select flag_blocco
			into   :ls_blocco
			from   anag_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_attrezzatura = :i_coltext;
					 
			if sqlca.sqlcode = 0 and not isnull(ls_blocco) and ls_blocco = "S" then
				g_mb.messagebox( "OMNIA", "Attenzione: l'attrezzatura selezionata risulta bloccata!", stopsign!)
			end if
					 
		end if		
		
end choose
end event

event pcd_new;call super::pcd_new;string	ls_null, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_note_idl, ls_des_attrezzatura, ls_des_tipo_manutenzione

setnull(ls_null)

if i_extendmode then
	
	if il_rbuttonrow > 0  then
		
		// posso spostare l'attività solo se mi trovo in una riga principale
			
		if idw_parent.getitemstring(il_rbuttonrow, "flag_tipo_riga") <> "P" then
			g_mb.messagebox("OMNIA","L'operazione di spostamento può avvenire solo sulla riga Principale (dell'attività)!")
			rollback;
			close(parent)
		end if
			
		il_anno_registrazione = idw_parent.getitemnumber(il_rbuttonrow, "anno_registrazione")
		il_num_registrazione  = idw_parent.getitemnumber(il_rbuttonrow, "num_registrazione")

		select cod_attrezzatura,
		          cod_tipo_manutenzione,
		          note_idl
		into    :ls_cod_attrezzatura,
		         :ls_cod_tipo_manutenzione,
			     :ls_note_idl
		from   programmi_manutenzione
		where   cod_azienda = :s_cs_xx.cod_azienda and
		           anno_registrazione = :il_anno_registrazione and
				  num_registrazione = :il_num_registrazione;
				  
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in ricerca piano manutenzione~r~n"+sqlca.sqlerrtext)
			return
		end if
		
		select descrizione
		into    :ls_des_attrezzatura
		from    anag_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and
		          cod_attrezzatura = :ls_cod_attrezzatura;
					 
		select des_tipo_manutenzione
		into    :ls_des_tipo_manutenzione
		from   tab_tipi_manutenzione
		where cod_azienda = :s_cs_xx.cod_azienda and
		         cod_attrezzatura = :ls_cod_attrezzatura and
			     cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;

		st_attivita.text = "Attrezzatura Originale:~r~n      " + ls_cod_attrezzatura + " " + ls_des_attrezzatura + "~r~n" + "Descrizione Attività:~r~n      " + ls_cod_tipo_manutenzione + " " + ls_note_idl

	end if
end if
		
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_attivita,"cod_attrezzatura")
end choose
end event

