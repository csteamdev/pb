﻿$PBExportHeader$w_registra_man.srw
forward
global type w_registra_man from w_cs_xx_risposta
end type
type dw_registra_man from uo_cs_xx_dw within w_registra_man
end type
type cb_cerca from commandbutton within w_registra_man
end type
type dw_sel_registra_man from uo_cs_xx_dw within w_registra_man
end type
end forward

global type w_registra_man from w_cs_xx_risposta
integer width = 2469
integer height = 1348
string title = "Registrazione Manutenzioni"
dw_registra_man dw_registra_man
cb_cerca cb_cerca
dw_sel_registra_man dw_sel_registra_man
end type
global w_registra_man w_registra_man

event pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)				 
											 
dw_sel_registra_man.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)		
									 
dw_registra_man.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_cursorrowpointer)										  
									 
dw_registra_man.enabled = false	
s_cs_xx.parametri.parametro_b_1 = false
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_sel_registra_man,"cod_tipo_manutenzione",sqlca,&
					  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
					  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + s_cs_xx.parametri.parametro_s_10 + "'")
end event

on w_registra_man.create
int iCurrent
call super::create
this.dw_registra_man=create dw_registra_man
this.cb_cerca=create cb_cerca
this.dw_sel_registra_man=create dw_sel_registra_man
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_registra_man
this.Control[iCurrent+2]=this.cb_cerca
this.Control[iCurrent+3]=this.dw_sel_registra_man
end on

on w_registra_man.destroy
call super::destroy
destroy(this.dw_registra_man)
destroy(this.cb_cerca)
destroy(this.dw_sel_registra_man)
end on

type dw_registra_man from uo_cs_xx_dw within w_registra_man
integer x = 41
integer y = 228
integer width = 1934
integer height = 988
integer taborder = 30
string dataobject = "d_registra_man"
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;string ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_des_intervento, ls_flag_tipo_intervento, ls_periodicita, &
		 ls_flag_scadenze, ls_note_idl, ls_flag_ricambio_codificato, ls_cod_ricambio, ls_cod_ricambio_alternativo, ls_ricambio_non_codificato, &
		 ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, &
		 ls_flag_budget, ls_cod_primario, ls_cod_misura, ls_flag_blocco, ls_flag_priorita, ls_flag_stato, ls_stato_manutenzione, ls_null
datetime ldt_data_da, ldt_data_a, ldt_data_blocco, ldt_data_forzata, ldt_data_giorno, ldt_data_registrazione, ldt_null
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_anno_reg_manutenzione, ll_num_reg_manutenzione, ll_null, ll_count
double ld_frequenza, ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_operaio_ore_previste, &
		 ld_operaio_minuti_previsti, ld_risorsa_ore_previste, ld_risorsa_minuti_previsti, ld_risorsa_costo_orario, &
		 ld_risorsa_costi_aggiuntivi, ld_anno_contratto_assistenza, ld_num_contratto_assistenza, ld_anno_ord_acq_risorsa, &
		 ld_num_ord_acq_risorsa, ld_prog_riga_ord_acq_risorsa, ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, &
		 ld_tot_costo_intervento, ld_num_reg_lista, ld_prog_liste_con_comp, ld_num_versione, ld_num_edizione
integer li_risposta
boolean lb_chiudi_maschera
uo_registra_manutenzione luo_registra_manutenzione

setnull(ls_null)
setnull(ldt_null)
setnull(ll_null)

ls_stato_manutenzione = dw_registra_man.getitemstring(dw_registra_man.getrow(), "stato")
ldt_data_giorno = dw_registra_man.getitemdatetime(dw_registra_man.getrow(), "data_scadenza")
ls_cod_attrezzatura = s_cs_xx.parametri.parametro_s_10
ls_cod_tipo_manutenzione = dw_sel_registra_man.getitemstring(dw_sel_registra_man.getrow(), "cod_tipo_manutenzione")

luo_registra_manutenzione = create uo_registra_manutenzione
luo_registra_manutenzione.wf_registra_man(ls_stato_manutenzione, ldt_data_giorno, ls_cod_attrezzatura, ls_cod_tipo_manutenzione)
destroy luo_registra_manutenzione



end event

type cb_cerca from commandbutton within w_registra_man
integer x = 2021
integer y = 228
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;string ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_flag_stato, ls_stato_manutenzione
datetime ldt_data_da, ldt_data_a, ldt_data_giorno, ldt_data_giorno_comodo
long ll_anno_registrazione, ll_num_registrazione, ll_i

dw_sel_registra_man.accepttext()
ls_cod_attrezzatura = s_cs_xx.parametri.parametro_s_10
ls_cod_tipo_manutenzione = dw_sel_registra_man.getitemstring(dw_sel_registra_man.getrow(), "cod_tipo_manutenzione")
ldt_data_da = dw_sel_registra_man.getitemdatetime(dw_sel_registra_man.getrow(), "data_da")
ldt_data_a = dw_sel_registra_man.getitemdatetime(dw_sel_registra_man.getrow(), "data_a")

if isnull(ls_cod_tipo_manutenzione) or isnull(ldt_data_da) or isnull(ldt_data_a) then
	g_mb.messagebox("Omnia", "Impostare tutti i filtri richiesti!")
	return
end if	

select anno_registrazione,
		 num_registrazione
  into :ll_anno_registrazione,
		 :ll_num_registrazione
  from programmi_manutenzione
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_attrezzatura = :ls_cod_attrezzatura
	and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
	return
end if	

if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia", "Attenzione non esiste nessuna programmazione in con codice attrezzatura = " + ls_cod_attrezzatura + " e tipo manutenzione = " + ls_cod_tipo_manutenzione)
	return
end if	

declare cu_det_calendario cursor for
 select data_giorno,
 		  flag_stato_manutenzione
	from det_cal_progr_manut
  where cod_azienda = :s_cs_xx.cod_azienda
    and anno_registrazione = :ll_anno_registrazione
	 and num_registrazione = :ll_num_registrazione
	 and data_giorno >= :ldt_data_da
	 and data_giorno <= :ldt_data_a
order by	data_giorno;
	 
open cu_det_calendario;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in apertura cursore cu_det_calendario " + sqlca.sqlerrtext)
	return
end if	
setnull(ldt_data_giorno_comodo)
dw_registra_man.reset()
ll_i = 1
do while 1 = 1
	fetch cu_det_calendario into :ldt_data_giorno, :ls_flag_stato;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella det_cal_progr_manut " + sqlca.sqlerrtext)
		return
	end if	
	if sqlca.sqlcode = 100 then exit
	if (ldt_data_giorno_comodo <> ldt_data_giorno) or isnull(ldt_data_giorno_comodo) then
		dw_registra_man.insertrow(0)
		dw_registra_man.setitem(ll_i, "data_scadenza", ldt_data_giorno)
		choose case ls_flag_stato
			case "A"
				ls_stato_manutenzione = "Aperta"				
			case "E"
				ls_stato_manutenzione = "Effettuata"				
			case "D"
				ls_stato_manutenzione = "Da Confermare"				
			case "S"
				ls_stato_manutenzione = "Scaduta"				
			case "O"
				ls_stato_manutenzione = "Obsoleta"				
		end choose		
		dw_registra_man.setitem(ll_i, "stato", ls_stato_manutenzione)
		ll_i ++
		ldt_data_giorno_comodo = ldt_data_giorno 
	end if	
	
loop
close cu_det_calendario;

dw_registra_man.setfocus()
dw_registra_man.enabled = true
end event

type dw_sel_registra_man from uo_cs_xx_dw within w_registra_man
integer x = 23
integer y = 24
integer width = 2395
integer height = 208
integer taborder = 10
string dataobject = "d_sel_registra_man"
boolean border = false
end type

