﻿$PBExportHeader$w_registro_uso_attrezzature.srw
$PBExportComments$Finestra Registrazioni Utilizzo Attrezzature
forward
global type w_registro_uso_attrezzature from w_cs_xx_principale
end type
type dw_registro_uso_attrezz_lista from uo_cs_xx_dw within w_registro_uso_attrezzature
end type
type dw_registro_uso_attrezz_det from uo_cs_xx_dw within w_registro_uso_attrezzature
end type
end forward

global type w_registro_uso_attrezzature from w_cs_xx_principale
integer width = 2418
integer height = 1604
string title = "Registro Utilizzo Attrezzature"
dw_registro_uso_attrezz_lista dw_registro_uso_attrezz_lista
dw_registro_uso_attrezz_det dw_registro_uso_attrezz_det
end type
global w_registro_uso_attrezzature w_registro_uso_attrezzature

type variables
boolean ib_new=FALSE
end variables

event pc_setwindow;call super::pc_setwindow;dw_registro_uso_attrezz_lista.set_dw_key("cod_azienda")
dw_registro_uso_attrezz_lista.set_dw_key("cod_attrezzatura")
dw_registro_uso_attrezz_lista.set_dw_key("prog_riga_manutenzione")
dw_registro_uso_attrezz_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_registro_uso_attrezz_det.set_dw_options(sqlca, dw_registro_uso_attrezz_lista, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_registro_uso_attrezz_lista
end event

on w_registro_uso_attrezzature.create
int iCurrent
call super::create
this.dw_registro_uso_attrezz_lista=create dw_registro_uso_attrezz_lista
this.dw_registro_uso_attrezz_det=create dw_registro_uso_attrezz_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_registro_uso_attrezz_lista
this.Control[iCurrent+2]=this.dw_registro_uso_attrezz_det
end on

on w_registro_uso_attrezzature.destroy
call super::destroy
destroy(this.dw_registro_uso_attrezz_lista)
destroy(this.dw_registro_uso_attrezz_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_registro_uso_attrezz_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type dw_registro_uso_attrezz_lista from uo_cs_xx_dw within w_registro_uso_attrezzature
integer x = 23
integer y = 20
integer width = 2331
integer height = 500
integer taborder = 10
string dataobject = "d_registro_uso_attrezz_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_attrezzatura"), &
						 i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_manutenzione"))

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_attrezzatura
long  l_Idx, ll_prog_riga_manutenzione


ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_attrezzatura")
ll_prog_riga_manutenzione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_manutenzione")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) then
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_attrezzatura")) then
      SetItem(l_Idx, "cod_attrezzatura", ls_cod_attrezzatura)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF isnull(GetItemnumber(l_Idx, "prog_riga_manutenzione")) then
      SetItem(l_Idx, "prog_riga_manutenzione", ll_prog_riga_manutenzione)
   END IF
NEXT

end event

event updatestart;call super::updatestart;if ib_new then

	string ls_cod_attrezzatura
	long ll_prog_riga_manutenzione, ll_num_registrazione, ll_anno_esercizio
  
	
	ls_cod_attrezzatura = this.GetItemstring(this.GetRow ( ), "cod_attrezzatura")
	ll_prog_riga_manutenzione = this.GetItemnumber(this.GetRow ( ), "prog_riga_manutenzione")
	ll_anno_esercizio = f_anno_esercizio()
   this.SetItem (this.GetRow(),"anno_registrazione", ll_anno_esercizio)
	
   select max(registro_uso_attrezzature.num_registrazione)
     into :ll_num_registrazione
     from registro_uso_attrezzature
     where (registro_uso_attrezzature.cod_azienda = :s_cs_xx.cod_azienda) and
	  		  (registro_uso_attrezzature.cod_attrezzatura = :ls_cod_attrezzatura) and
			  (registro_uso_attrezzature.prog_riga_manutenzione = :ll_prog_riga_manutenzione) and
			  (registro_uso_attrezzature.anno_registrazione = :ll_anno_esercizio);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow(),"num_registrazione", ll_num_registrazione)
   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_registrazione", 1)
   end if
   ib_new = false
end if

end event

event pcd_new;call super::pcd_new;ib_new = true

end event

type dw_registro_uso_attrezz_det from uo_cs_xx_dw within w_registro_uso_attrezzature
integer x = 23
integer y = 540
integer width = 2331
integer height = 940
integer taborder = 20
string dataobject = "d_registro_uso_attrezz_det"
borderstyle borderstyle = styleraised!
end type

