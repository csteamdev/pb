﻿$PBExportHeader$w_report_attivita_manut_det.srw
forward
global type w_report_attivita_manut_det from w_cs_xx_principale
end type
type cb_selezione from commandbutton within w_report_attivita_manut_det
end type
type cb_report from commandbutton within w_report_attivita_manut_det
end type
type cb_annulla from commandbutton within w_report_attivita_manut_det
end type
type dw_selezione from uo_cs_xx_dw within w_report_attivita_manut_det
end type
type dw_report_lista from uo_cs_xx_dw within w_report_attivita_manut_det
end type
type dw_report_dettaglio from uo_cs_xx_dw within w_report_attivita_manut_det
end type
end forward

global type w_report_attivita_manut_det from w_cs_xx_principale
integer width = 2034
integer height = 808
string title = "Report Attività di Manutenzione"
boolean resizable = false
event ue_report_dettaglio ( )
event type long ue_report_lista ( )
cb_selezione cb_selezione
cb_report cb_report
cb_annulla cb_annulla
dw_selezione dw_selezione
dw_report_lista dw_report_lista
dw_report_dettaglio dw_report_dettaglio
end type
global w_report_attivita_manut_det w_report_attivita_manut_det

event ue_report_dettaglio();long ll_riga, ll_num_registrazione, ll_anno_registrazione, ll_scelta
datetime ldt_data_inizio, ldt_data_fine, ldt_data_corrente
string ls_operatore, ls_cod_operatore, ls_cod_reparto_filtro, ls_cod_area_aziendale_filtro, ls_des_attrezzatura,&
       ls_des_tipo_manutenzione, ls_cod_azienda, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cognome,&
		 ls_nome, ls_cod_operatore_filtro, ls_des_reparto, ls_cod_ricambio, ls_cod_risorsa_esterna, ls_des_ricambio,&
		 ls_des_risorsa_esterna, ls_cod_cat_risorse_esterne, ls_nota, ls_des_area_aziendale, ls_cod_reparto, ls_cod_divisione_filtro, &
		 ls_cod_area_aziendale		 

dw_report_dettaglio.reset()

ls_cod_azienda=s_cs_xx.cod_azienda
ldt_data_inizio = dw_selezione.getitemdatetime(1,"rd_da_data")
ldt_data_fine = dw_selezione.getitemdatetime(1,"rd_a_data")
ls_cod_operatore_filtro = dw_selezione.getitemstring(1,"rs_cod_operatore")
if isnull(ls_cod_operatore_filtro) then
	ls_cod_operatore_filtro = "%"
end if	
ls_cod_reparto_filtro = dw_selezione.getitemstring(1,"rs_cod_reparto")
if isnull(ls_cod_reparto_filtro) then
	ls_cod_reparto_filtro = "%"
end if	
ls_cod_area_aziendale_filtro = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
if isnull(ls_cod_area_aziendale_filtro) then
	ls_cod_area_aziendale_filtro = "%"
end if	

ls_cod_divisione_filtro = dw_selezione.getitemstring(1,"rs_cod_divisione")
if isnull(ls_cod_divisione_filtro) then
	ls_cod_divisione_filtro = "%"
end if	

declare ord_det cursor for
select data_giorno, 
		 anno_registrazione, 
		 num_registrazione
from   det_cal_progr_manut
where  data_giorno between :ldt_data_inizio and :ldt_data_fine;

open ord_det;

do while true
	
	fetch ord_det into :ldt_data_corrente, 
							 :ll_anno_registrazione, 
							 :ll_num_registrazione;
		
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore ord_det. Dettaglio =" + sqlca.sqlerrtext)
		close ord_det;
		rollback;
		exit
	elseif sqlca.sqlcode = 100 then
		exit		
	end if	
	
	select cod_attrezzatura, 
			 cod_tipo_manutenzione, 
			 cod_operaio, 
			 cod_ricambio, 
			 cod_risorsa_esterna,
	       cod_cat_risorse_esterne, 
			 note_idl
	into   :ls_cod_attrezzatura, 
	       :ls_cod_tipo_manutenzione, 
			 :ls_cod_operatore, 
			 :ls_cod_ricambio,
	       :ls_cod_risorsa_esterna, 
			 :ls_cod_cat_risorse_esterne, 
			 :ls_nota
	from   programmi_manutenzione
	where  cod_azienda = :ls_cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
			 cod_operaio like :ls_cod_operatore_filtro;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di programmi_manutenzione. Dettaglio =" + sqlca.sqlerrtext)
		close ord_det;
		rollback;
		exit
	elseif sqlca.sqlcode = 100 then
		sqlca.sqlcode = 0
		continue	
	end if	  
	
	select cognome, nome
	into   :ls_cognome, :ls_nome
	from   anag_operai
	where  cod_operaio = :ls_cod_operatore and
	       cod_azienda = :ls_cod_azienda;
	
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di anag_operai. Dettaglio =" + sqlca.sqlerrtext)
		close ord_det;
		rollback;
		exit
	end if
	
	select descrizione, 
			 cod_reparto, 
			 cod_area_aziendale
	into   :ls_des_attrezzatura, 
			 :ls_cod_reparto, 
			 :ls_cod_area_aziendale
	from   anag_attrezzature
	where  cod_attrezzatura = :ls_cod_attrezzatura and
	       cod_azienda = :ls_cod_azienda and
			 cod_reparto like :ls_cod_reparto_filtro and
			 cod_area_aziendale like :ls_cod_area_aziendale_filtro and
			 cod_area_aziendale IN ( select cod_area_aziendale
			                         from   tab_aree_aziendali 
											 where  cod_azienda = :s_cs_xx.cod_azienda and
											        cod_divisione like :ls_cod_divisione_filtro);
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di anag_attrezzature. Dettaglio =" + sqlca.sqlerrtext)
		close ord_det;
		rollback;
		exit
	elseif sqlca.sqlcode = 100 then
		sqlca.sqlcode = 0
		continue
	end if
	
	
	select des_tipo_manutenzione 
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :ls_cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		  
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di tipi_manutenzione. Dettaglio =" + sqlca.sqlerrtext)
		close ord_det;
		rollback;
		exit
	end if	  
	
	select des_reparto 
	into   :ls_des_reparto
	from   anag_reparti
	where  cod_azienda = :ls_cod_azienda and
			 cod_reparto = :ls_cod_reparto;
		  
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di anag_reparti. Dettaglio =" + sqlca.sqlerrtext)
		close ord_det;
		rollback;
		exit
	end if

	if not isnull(ls_cod_ricambio) then
		
		select des_prodotto 
		into   :ls_des_ricambio
		from   anag_prodotti
		where  cod_azienda = :ls_cod_azienda and
				 cod_prodotto = :ls_cod_ricambio;
		  
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nella select di anag_prodotti. Dettaglio =" + sqlca.sqlerrtext)
			close ord_det;
			rollback;
			exit
		end if
	end if
	
	if not isnull(ls_cod_risorsa_esterna) then
		
		select descrizione 
		into   :ls_des_risorsa_esterna
		from   anag_risorse_esterne
		where  cod_azienda = :ls_cod_azienda and
	   	    cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
				 cod_risorsa_esterna = :ls_cod_risorsa_esterna;
		  
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore nella select di anag_risorse_esterne. Dettaglio =" + sqlca.sqlerrtext)
			close ord_det;
			rollback;
			exit
		end if
	end if
	
	select des_area 
	into   :ls_des_area_aziendale
	from   tab_aree_aziendali
	where  cod_azienda = :ls_cod_azienda and
	       cod_area_aziendale = :ls_cod_area_aziendale;
		  
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di aree_aziendali. Dettaglio =" + sqlca.sqlerrtext)
		close ord_det;
		rollback;
		exit
	end if

	
	ls_operatore=ls_cognome+" "+ls_nome
	ll_riga=dw_report_dettaglio.insertrow(0)
	dw_report_dettaglio.setitem(ll_riga,"data_intervento",ldt_data_corrente)
	dw_report_dettaglio.setitem(ll_riga,"des_attrezzatura",ls_des_attrezzatura)
	dw_report_dettaglio.setitem(ll_riga,"cod_attrezzatura",ls_cod_attrezzatura)
	dw_report_dettaglio.setitem(ll_riga,"des_tipo_manutenzione",ls_des_tipo_manutenzione)
	dw_report_dettaglio.setitem(ll_riga,"nome_responsabile",ls_operatore)
	dw_report_dettaglio.setitem(ll_riga,"des_reparto",ls_des_reparto)
	dw_report_dettaglio.setitem(ll_riga,"des_ricambio",ls_des_ricambio)
	dw_report_dettaglio.setitem(ll_riga,"des_risorsa_esterna",ls_des_risorsa_esterna)
	dw_report_dettaglio.setitem(ll_riga,"des_area_aziendale",ls_des_area_aziendale)
	dw_report_dettaglio.setitem(ll_riga,"note_idl",ls_nota)

loop

close ord_det;

dw_report_dettaglio.resetupdate()

dw_selezione.hide()
this.x = 72
this.y = 64
this.width = 3447
this.height = 1664
cb_selezione.x = 3040
cb_selezione.y = 1460


dw_report_dettaglio.show()
cb_selezione.show()

end event

event type long ue_report_lista();long ll_riga, ll_num_registrazione, ll_anno_registrazione
datetime ldt_data_inizio, ldt_data_fine, ldt_data_corrente
string ls_operatore, ls_cod_operatore, ls_cod_reparto, ls_cod_area_aziendale, ls_des_attrezzatura,&
       ls_des_tipo_manutenzione, ls_cod_azienda, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cod_divisione_filtro, ls_cognome,&
		 ls_nome, ls_cod_operatore_filtro

dw_report_lista.setfocus()
dw_report_lista.reset()

ls_cod_azienda = s_cs_xx.cod_azienda
ldt_data_inizio = dw_selezione.getitemdatetime(1,"rd_da_data")
ldt_data_fine = dw_selezione.getitemdatetime(1,"rd_a_data")
ls_cod_operatore_filtro = dw_selezione.getitemstring(1,"rs_cod_operatore")
if isnull(ls_cod_operatore_filtro) then
	ls_cod_operatore_filtro = "%"
end if	
ls_cod_reparto = dw_selezione.getitemstring(1,"rs_cod_reparto")
if isnull(ls_cod_reparto) then
	ls_cod_reparto = "%"
end if	
ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
if isnull(ls_cod_area_aziendale) then
	ls_cod_area_aziendale = "%"
end if	

ls_cod_divisione_filtro = dw_selezione.getitemstring(1,"rs_cod_divisione")
if isnull(ls_cod_divisione_filtro) then
	ls_cod_divisione_filtro = "%"
end if	

declare ord_list cursor for
select data_giorno,anno_registrazione,num_registrazione
from det_cal_progr_manut
where data_giorno between :ldt_data_inizio and :ldt_data_fine;

open ord_list;

do while true
	
	fetch ord_list into :ldt_data_corrente,:ll_anno_registrazione,:ll_num_registrazione;
		
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore ord_list. Dettaglio =" + sqlca.sqlerrtext)
		close ord_list;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit		
	end if
	
	select cod_attrezzatura, cod_tipo_manutenzione, cod_operaio
	into :ls_cod_attrezzatura, :ls_cod_tipo_manutenzione, :ls_cod_operatore
	from programmi_manutenzione
	where cod_azienda = :ls_cod_azienda and
	      anno_registrazione = :ll_anno_registrazione and
	      num_registrazione = :ll_num_registrazione and
			cod_operaio like :ls_cod_operatore_filtro;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di programmi_manutenzione. Dettaglio =" + sqlca.sqlerrtext)
		close ord_list;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		sqlca.sqlcode = 0
		continue	
	end if	  
	
	select cognome, nome
	into :ls_cognome, :ls_nome
	from anag_operai
	where cod_operaio = :ls_cod_operatore and
	      cod_azienda = :ls_cod_azienda;
	
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di anag_operai. Dettaglio =" + sqlca.sqlerrtext)
		close ord_list;
		rollback;
		return -1
	end if
	
	select descrizione into :ls_des_attrezzatura
	from anag_attrezzature
	where cod_attrezzatura = :ls_cod_attrezzatura and
	      cod_azienda = :ls_cod_azienda and
			cod_reparto like :ls_cod_reparto and
			cod_area_aziendale like :ls_cod_area_aziendale and
			cod_area_aziendale IN ( select cod_area_aziendale
			                        from   tab_aree_aziendali
											where  cod_azienda = :s_cs_xx.cod_azienda and
											       cod_divisione like :ls_cod_divisione_filtro);
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di anag_attrezzature. Dettaglio =" + sqlca.sqlerrtext)
		close ord_list;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		sqlca.sqlcode = 0
		continue
	end if
	
	
	select des_tipo_manutenzione into :ls_des_tipo_manutenzione
	from tab_tipi_manutenzione
	where cod_azienda = :ls_cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura and
			cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		  
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select di tipi_manutenzione. Dettaglio =" + sqlca.sqlerrtext)
		close ord_list;
		rollback;
		return -1
	end if	  
	
	ls_operatore = ls_cognome+" "+ls_nome
	ll_riga=dw_report_lista.insertrow(0)
	dw_report_lista.setitem(ll_riga,"data_giorno",ldt_data_corrente)
	dw_report_lista.setitem(ll_riga,"des_attrezzatura",ls_des_attrezzatura)
	dw_report_lista.setitem(ll_riga,"des_tipo_manutenzione",ls_des_tipo_manutenzione)
	dw_report_lista.setitem(ll_riga,"des_operaio",ls_operatore)
   dw_report_lista.setitem(ll_riga,"cod_attrezzatura",ls_cod_attrezzatura)
	dw_report_lista.setitem(ll_riga,"cod_tipo_manutenzione",ls_cod_tipo_manutenzione)
	dw_report_lista.setitem(ll_riga,"cod_operaio",ls_cod_operatore)

loop

close ord_list;

dw_report_lista.resetupdate()
cb_selezione.x = 4183
cb_selezione.y = 1460
cb_selezione.show()

dw_selezione.hide()
this.x = 72
this.y = 64
this.width = 4581
this.height = 1664
dw_report_lista.show()
return 0
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_operatore",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")										  
end event

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_report_lista.ib_dw_report = true
dw_report_dettaglio.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_report_dettaglio.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

dw_report_lista.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_dettaglio.modify(ls_modify)
dw_report_lista.modify(ls_modify)

this.width=2007
this.height=816

return 0
end event

on w_report_attivita_manut_det.create
int iCurrent
call super::create
this.cb_selezione=create cb_selezione
this.cb_report=create cb_report
this.cb_annulla=create cb_annulla
this.dw_selezione=create dw_selezione
this.dw_report_lista=create dw_report_lista
this.dw_report_dettaglio=create dw_report_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_selezione
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report_lista
this.Control[iCurrent+6]=this.dw_report_dettaglio
end on

on w_report_attivita_manut_det.destroy
call super::destroy
destroy(this.cb_selezione)
destroy(this.cb_report)
destroy(this.cb_annulla)
destroy(this.dw_selezione)
destroy(this.dw_report_lista)
destroy(this.dw_report_dettaglio)
end on

type cb_selezione from commandbutton within w_report_attivita_manut_det
boolean visible = false
integer x = 3040
integer y = 1460
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;parent.x = 672
parent.y = 264
parent.width = 1998	
parent.height = 804

dw_report_dettaglio.hide()
dw_report_lista.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
dw_selezione.show()
cb_report.show()
cb_annulla.show()
end event

type cb_report from commandbutton within w_report_attivita_manut_det
integer x = 1531
integer y = 540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_flag_tipo_report
ls_flag_tipo_report=dw_selezione.getitemstring(1,"rs_flag_tipo_report")
choose case ls_flag_tipo_report
	case "D"
		iuo_dw_main=dw_report_dettaglio
		dw_report_dettaglio.change_dw_current()
		parent.postevent("ue_report_dettaglio")
	case "L"
		dw_report_lista.change_dw_current()
		iuo_dw_main=dw_report_lista
		parent.postevent("ue_report_lista")
end choose		
end event

type cb_annulla from commandbutton within w_report_attivita_manut_det
integer x = 1143
integer y = 540
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type dw_selezione from uo_cs_xx_dw within w_report_attivita_manut_det
integer x = 23
integer y = 20
integer width = 1966
integer height = 640
integer taborder = 0
string dataobject = "d_sel_report_attivita_manut_det"
boolean border = false
end type

type dw_report_lista from uo_cs_xx_dw within w_report_attivita_manut_det
boolean visible = false
integer x = 23
integer y = 20
integer width = 4526
integer height = 1420
integer taborder = 10
string dataobject = "d_report_attivita_manut_lista"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_report_dettaglio from uo_cs_xx_dw within w_report_attivita_manut_det
boolean visible = false
integer x = 23
integer y = 20
integer width = 3406
integer height = 1400
integer taborder = 20
string dataobject = "d_report_attivita_manut_det"
boolean hscrollbar = true
boolean vscrollbar = true
end type

