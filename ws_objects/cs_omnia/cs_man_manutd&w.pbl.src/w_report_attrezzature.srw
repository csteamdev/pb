﻿$PBExportHeader$w_report_attrezzature.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_attrezzature from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_attrezzature
end type
type cb_report from commandbutton within w_report_attrezzature
end type
type cb_selezione from commandbutton within w_report_attrezzature
end type
type dw_selezione from uo_cs_xx_dw within w_report_attrezzature
end type
type dw_report from uo_cs_xx_dw within w_report_attrezzature
end type
end forward

global type w_report_attrezzature from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Attrezzature"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_attrezzature w_report_attrezzature

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2172
this.height = 850

end event

on w_report_attrezzature.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_attrezzature.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"rs_da_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_PO_LoadDDDW_DW(dw_selezione,"rs_a_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  					  

end event

type cb_annulla from commandbutton within w_report_attrezzature
integer x = 1349
integer y = 580
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_attrezzature
integer x = 1737
integer y = 580
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_da_attrezzatura, ls_a_attrezzatura, ls_cat_attrezzature, ls_cod_divisione, ls_cod_area_aziendale_s, ls_cod_reparto_s, &
		 ls_cod_azienda, ls_cod_attrezzatura, ls_descrizione, ls_fabbricante, ls_modello, ls_num_matricola, ls_unita_tempo, &
		 ls_utilizzo_scadenza, ls_cod_inventario, ls_cod_utente, ls_reperibilita, ls_cod_area_aziendale, &
		 ls_cod_reparto, ls_cod_area_aziendale_r[], ls_des_reparto, ls_des_area, ls_rag_soc_1, ls_rag_soc_2, ls_null
datetime ldt_data_acquisto
decimal ld_periodicita_revisione
long ll_i, ll_y

setnull(ls_null)
dw_report.reset()

ls_da_attrezzatura = dw_selezione.getitemstring(1,"rs_da_cod_attrezzatura")
ls_a_attrezzatura = dw_selezione.getitemstring(1,"rs_a_cod_attrezzatura")
ls_cat_attrezzature = dw_selezione.getitemstring(1,"rs_cod_cat_attrezzatura")
ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")
ls_cod_area_aziendale_s = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
ls_cod_reparto_s = dw_selezione.getitemstring(1,"rs_cod_reparto")

if isnull(ls_da_attrezzatura) then ls_da_attrezzatura = "!"
if isnull(ls_a_attrezzatura) then  ls_a_attrezzatura  = "ZZZZZZ"
if isnull(ls_cat_attrezzature) then  ls_cat_attrezzature  = "%"
if isnull(ls_cod_divisione) then  ls_cod_divisione  = "%"
if isnull(ls_cod_area_aziendale_s) then  ls_cod_area_aziendale_s  = "%"
if isnull(ls_cod_reparto_s) then  ls_cod_reparto_s  = "%"

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()

ll_i = 1
ll_y = 1
ls_cod_area_aziendale_r[1] = ls_null

if ls_cod_area_aziendale_s = "%" and ls_cod_divisione <> "%" then
	
	declare cu_aree cursor for
	 select cod_area_aziendale
		from tab_aree_aziendali
	  where cod_azienda = :s_cs_xx.cod_azienda
		 and (cod_divisione like :ls_cod_divisione or cod_divisione is null);
		 
	open cu_aree;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in apertura cursore cu_aree " + sqlca.sqlerrtext)
		return
	end if
	
	do while 1 = 1
		fetch cu_aree into :ls_cod_area_aziendale_r[ll_i];
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati da tabella tab_aree_aziendali " + sqlca.sqlerrtext)
			return
		end if		
		if sqlca.sqlcode = 100 then exit
		
		ll_i ++
	loop
	close cu_aree;
end if	

if ls_cod_area_aziendale_s = "%" and ls_cod_divisione = "%" then ls_cod_area_aziendale_r[1] = "%"
if ls_cod_area_aziendale_s <> "%" and ls_cod_divisione <> "%" then ls_cod_area_aziendale_r[1] = ls_cod_area_aziendale_s
if ls_cod_area_aziendale_s <> "%" and ls_cod_divisione = "%" then ls_cod_area_aziendale_r[1] = ls_cod_area_aziendale_s

for ll_i = 1 to UpperBound(ls_cod_area_aziendale_r)

	declare cu_cursore cursor for
	select cod_azienda,
			 cod_attrezzatura,
			 descrizione,
			 fabbricante,
			 modello,
			 num_matricola,
			 data_acquisto,
			 periodicita_revisione,
			 unita_tempo,
			 utilizzo_scadenza,
			 cod_reparto,
			 cod_inventario,
			 cod_utente,
			 reperibilita,
			 cod_area_aziendale,
			 cod_reparto
	  from anag_attrezzature
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_attrezzatura >= :ls_da_attrezzatura
		and cod_attrezzatura <= :ls_a_attrezzatura
		and (cod_cat_attrezzature like :ls_cat_attrezzature or cod_cat_attrezzature is null)
		and (cod_area_aziendale like :ls_cod_area_aziendale_r[ll_i] or cod_area_aziendale is null)
		and (cod_reparto like :ls_cod_reparto_s or cod_reparto is null);	 
	
	open cu_cursore;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in apertura cursore cu_cursore " + sqlca.sqlerrtext)
		return
	end if	
	
	do while 1 = 1
		fetch cu_cursore into :ls_cod_azienda,
									 :ls_cod_attrezzatura,
									 :ls_descrizione,
									 :ls_fabbricante,
									 :ls_modello,
									 :ls_num_matricola,
									 :ldt_data_acquisto,
									 :ld_periodicita_revisione,
									 :ls_unita_tempo,
									 :ls_utilizzo_scadenza,
									 :ls_cod_reparto,
									 :ls_cod_inventario,
									 :ls_cod_utente,
									 :ls_reperibilita,
									 :ls_cod_area_aziendale,
									 :ls_cod_reparto;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella anag_attrezzature " + sqlca.sqlerrtext)
			return
		end if									 

		if sqlca.sqlcode = 100 then exit		
		
		dw_report.insertrow(0)
		dw_report.setitem(ll_y, "cod_attrezzatura",ls_cod_attrezzatura)
		dw_report.setitem(ll_y, "descrizione", ls_descrizione)
		dw_report.setitem(ll_y, "fabbricante", ls_fabbricante)		
		dw_report.setitem(ll_y, "modello", ls_modello)
		dw_report.setitem(ll_y, "num_matricola", ls_num_matricola)		
		dw_report.setitem(ll_y, "data_acquisto", ldt_data_acquisto)
		dw_report.setitem(ll_y, "periodicita_revisione", ld_periodicita_revisione)				
		dw_report.setitem(ll_y, "unita_tempo", ls_unita_tempo)
		dw_report.setitem(ll_y, "utilizzo_scadenza", ls_utilizzo_scadenza)		
		dw_report.setitem(ll_y, "cod_reparto", ls_cod_reparto)
		dw_report.setitem(ll_y, "cod_inventario", ls_cod_inventario)						
		dw_report.setitem(ll_y, "cod_utente", ls_cod_utente)
		dw_report.setitem(ll_y, "reperibilita", ls_reperibilita)				
		dw_report.setitem(ll_y, "cod_area_aziendale", ls_cod_area_aziendale)
		
		select des_reparto
		  into :ls_des_reparto
		  from anag_reparti
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_reparto = :ls_cod_reparto;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella anag_reparti " + sqlca.sqlerrtext)
			return
		end if									 		
		
		dw_report.setitem(ll_y, "anag_reparti_des_reparto", ls_des_reparto)	

		select des_area
		  into :ls_des_area
		  from tab_aree_aziendali
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_area_aziendale = :ls_cod_area_aziendale;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella tab_aree_aziendali " + sqlca.sqlerrtext)
			return
		end if									 				
		
		dw_report.setitem(ll_y, "tab_aree_aziendali_des_area", ls_des_area)
		
		select rag_soc_1,
				 rag_soc_2
		  into :ls_rag_soc_1,
				 :ls_rag_soc_2
		  from aziende
		 where cod_azienda = :s_cs_xx.cod_azienda;
		 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella aziende " + sqlca.sqlerrtext)
			return
		end if									 						 
		
		dw_report.setitem(ll_y, "aziende_rag_soc_1", ls_rag_soc_1)		
		dw_report.setitem(ll_y, "aziende_rag_soc_2", ls_rag_soc_2)				

		ll_y ++
	loop
	close cu_cursore;
next	

cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_attrezzature
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2172
parent.height = 850

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_attrezzature
integer x = 23
integer y = 20
integer width = 2080
integer height = 540
integer taborder = 20
string dataobject = "d_selezione_report_attrezzature"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;string ls_cod_divisione, ls_cod_area_aziendale

dw_selezione.accepttext()
choose case i_colname
	case "rs_cod_divisione"
		ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")		
		if isnull(ls_cod_divisione) then
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
		else			  
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + i_coltext + "'")					  					  
		end if				  
		
	case "rs_cod_area_aziendale"
		ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")		
		if isnull(ls_cod_area_aziendale) then
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
						  "anag_reparti","cod_reparto","des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
		else						  
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
						  "anag_reparti","cod_reparto","des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + i_coltext + "'")						
		end if	
end choose





end event

type dw_report from uo_cs_xx_dw within w_report_attrezzature
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_attrezzature"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

