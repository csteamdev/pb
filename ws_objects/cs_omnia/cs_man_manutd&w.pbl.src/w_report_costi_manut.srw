﻿$PBExportHeader$w_report_costi_manut.srw
$PBExportComments$Report Costi Manutenzione per Attrezzatura
forward
global type w_report_costi_manut from w_cs_xx_principale
end type
type dw_report_costi_manutenzioni from uo_cs_xx_dw within w_report_costi_manut
end type
end forward

global type w_report_costi_manut from w_cs_xx_principale
integer width = 3872
integer height = 4772
string title = "Stampa Offerte Clienti"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_costi_manutenzioni dw_report_costi_manutenzioni
end type
global w_report_costi_manut w_report_costi_manut

type variables
boolean ib_modifica=false, ib_nuovo=false
end variables

event pc_setwindow;call super::pc_setwindow;dw_report_costi_manutenzioni.ib_dw_report = true

set_w_options(c_noresizewin)

dw_report_costi_manutenzioni.set_dw_options(sqlca, &
                                            pcca.null_object, &
                                            c_nonew + &
                                            c_nomodify + &
                                            c_nodelete + &
                                            c_noenablenewonopen + &
                                            c_noenablemodifyonopen + &
                                            c_scrollparent + &
                                            c_disableCC + &
                                            c_disableCCinsert, &
                                            c_nohighlightselected + &
                                            c_nocursorrowfocusrect + &
                                            c_nocursorrowpointer)

end event

on w_report_costi_manut.create
int iCurrent
call super::create
this.dw_report_costi_manutenzioni=create dw_report_costi_manutenzioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_costi_manutenzioni
end on

on w_report_costi_manut.destroy
call super::destroy
destroy(this.dw_report_costi_manutenzioni)
end on

type dw_report_costi_manutenzioni from uo_cs_xx_dw within w_report_costi_manut
integer x = 23
integer y = 20
integer width = 3657
integer height = 4520
integer taborder = 20
string dataobject = "d_report_costi_manutenzioni"
boolean livescroll = true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


//ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
//ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, &
                     s_cs_xx.parametri.parametro_s_1, &
                     s_cs_xx.parametri.parametro_data_1, &
                     s_cs_xx.parametri.parametro_data_2  )
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

