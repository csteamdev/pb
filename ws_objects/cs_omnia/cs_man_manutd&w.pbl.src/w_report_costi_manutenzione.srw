﻿$PBExportHeader$w_report_costi_manutenzione.srw
$PBExportComments$Finestra Report Costi Manutenzioni
forward
global type w_report_costi_manutenzione from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_costi_manutenzione
end type
type cb_report from commandbutton within w_report_costi_manutenzione
end type
type cb_selezione from commandbutton within w_report_costi_manutenzione
end type
type dw_selezione from uo_cs_xx_dw within w_report_costi_manutenzione
end type
type dw_report from uo_cs_xx_dw within w_report_costi_manutenzione
end type
end forward

global type w_report_costi_manutenzione from w_cs_xx_principale
integer width = 3552
integer height = 1668
string title = "Report Costi Manutenzione"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_costi_manutenzione w_report_costi_manutenzione

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 673
this.y = 265
this.width = 2100
this.height = 910

end event

on w_report_costi_manutenzione.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_costi_manutenzione.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_operaio",sqlca,&
                 "anag_operai","cod_operaio"," nome + ' ' + cognome ",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione"," des_divisione ",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

end event

type cb_annulla from commandbutton within w_report_costi_manutenzione
integer x = 1280
integer y = 720
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_costi_manutenzione
integer x = 1669
integer y = 720
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string   ls_cod_attrezzatura, ls_cod_operatore, ls_cod_cat_risorsa_esterna, ls_cod_risorsa_esterna, ls_prova, new_select, &
         ls_where, ls_divisione, ls_cod_cat_attrezzatura, ls_cod_attrezzatura_a

datetime ldt_da_data, ldt_a_data

long     ll_righe, ll_index

dw_selezione.setcolumn(1)

ls_cod_attrezzatura = dw_selezione.getitemstring(1,"rs_cod_attrezzatura")
ldt_da_data  = dw_selezione.getitemdatetime(1,"rd_da_data")
ldt_a_data   = dw_selezione.getitemdatetime(1,"rd_a_data")
ls_cod_operatore = dw_selezione.getitemstring( 1, "rs_cod_operaio")
ls_divisione = dw_selezione.getitemstring( 1, "rs_cod_divisione")
ls_cod_cat_risorsa_esterna = dw_selezione.getitemstring( 1, "rs_cod_cat_risorse_esterne")
ls_cod_risorsa_esterna = dw_selezione.getitemstring( 1, "rs_cod_risorsa_esterna")
ls_cod_cat_attrezzatura = dw_selezione.getitemstring( 1, "rs_cod_cat_attrezzatura")
ls_cod_attrezzatura_a = dw_selezione.getitemstring(1, "rs_cod_attrezzatura_a")

if ls_cod_cat_attrezzatura = "" then setnull(ls_cod_cat_attrezzatura)
if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura = "" then ls_cod_attrezzatura = "%"
if isnull(ls_cod_attrezzatura_a) or ls_cod_attrezzatura_a = "" then ls_cod_attrezzatura_a = "%"
if isnull(ls_cod_operatore) or ls_cod_operatore = "" then ls_cod_operatore = "%"
if isnull(ls_cod_cat_risorsa_esterna) or ls_cod_cat_risorsa_esterna = "" then ls_cod_cat_risorsa_esterna = "%"
if isnull(ls_cod_risorsa_esterna) or ls_cod_risorsa_esterna = "" then ls_cod_risorsa_esterna = "%"
if isnull(ldt_da_data) then ldt_da_data = datetime( date("1900-01-01"), 00:00:00)
if isnull(ldt_a_data) then ldt_a_data = datetime( date("2999-01-01"), 00:00:00)

if ls_cod_attrezzatura = "%" and ls_cod_attrezzatura_a = "%" then
else
	if ls_cod_attrezzatura = "%" then
		ls_cod_attrezzatura = ls_cod_attrezzatura_a 
	else
		ls_cod_attrezzatura_a = ls_cod_attrezzatura
	end if

end if
// ***

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1660

dw_report.show()
ll_righe = dw_report.retrieve(s_cs_xx.cod_azienda, &
                   				ls_cod_attrezzatura, &
                   				ls_cod_attrezzatura_a, &										 
									   ldt_da_data, &
						            ldt_a_data, &
						            ls_cod_operatore, &
						            ls_cod_cat_risorsa_esterna, &
						            ls_cod_risorsa_esterna, &
										ls_divisione, &
										ls_cod_cat_attrezzatura)
						 
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_costi_manutenzione
event ue_postevent ( )
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 673
parent.y = 265
parent.width = 2100
parent.height = 910

dw_report.hide()
cb_selezione.hide()
//cb_attrezzature_ricerca.show()
//cb_1.show()
dw_selezione.change_dw_current()

end event

type dw_selezione from uo_cs_xx_dw within w_report_costi_manutenzione
integer x = 23
integer y = 20
integer width = 2057
integer height = 680
integer taborder = 20
string dataobject = "d_sel_schede_manutenzione"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
		
	case "rs_cod_cat_risorse_esterne"
		
		if isnull(i_coltext) or i_coltext = "" then return 0
		
		this.setitem( row, "rs_cod_risorsa_esterna", ls_null)
		
		f_PO_LoadDDDW_DW ( dw_selezione, &
								 "rs_cod_risorsa_esterna", &
								 sqlca, &
							  	 "anag_risorse_esterne", &
								 "cod_risorsa_esterna", &
								 "descrizione", &
							    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "' ")			
						  
		
	case "rs_cod_attrezzatura"
		if isnull( getitemstring( row, "rs_cod_attrezzatura_a")) or getitemstring( row, "rs_cod_attrezzatura_a") = "" then
			setitem( row, "rs_cod_attrezzatura_a", i_coltext)
		end if
		
end choose




end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"rs_cod_attrezzatura")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura_a")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_costi_manutenzione
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_costi_manutenzioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

