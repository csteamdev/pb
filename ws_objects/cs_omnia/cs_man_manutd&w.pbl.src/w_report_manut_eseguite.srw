﻿$PBExportHeader$w_report_manut_eseguite.srw
forward
global type w_report_manut_eseguite from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_report_manut_eseguite
end type
type dw_report_manut_eseguite from uo_cs_xx_dw within w_report_manut_eseguite
end type
type dw_folder from u_folder within w_report_manut_eseguite
end type
type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_manut_eseguite
end type
type cb_aggiorna from commandbutton within w_report_manut_eseguite
end type
type cb_cancella_filtri from commandbutton within w_report_manut_eseguite
end type
end forward

global type w_report_manut_eseguite from w_cs_xx_principale
integer width = 3753
integer height = 2200
string title = "Report manutenzioni eseguite"
event ue_setdwcolor ( )
event ue_openparm ( )
cb_1 cb_1
dw_report_manut_eseguite dw_report_manut_eseguite
dw_folder dw_folder
dw_sel_manut_eseguite dw_sel_manut_eseguite
cb_aggiorna cb_aggiorna
cb_cancella_filtri cb_cancella_filtri
end type
global w_report_manut_eseguite w_report_manut_eseguite

forward prototypes
public function integer wf_report ()
end prototypes

event ue_setdwcolor();dw_report_manut_eseguite.Object.data_giorno.Color = "0~tIf(ritardo='S',rgb(255,0,0),rgb(0,0,0))"
dw_report_manut_eseguite.Object.cod_attrezzatura.Color = "0~tIf(ritardo='S',rgb(255,0,0),rgb(0,0,0))"
dw_report_manut_eseguite.Object.des_attrezzatura.Color = "0~tIf(ritardo='S',rgb(255,0,0),rgb(0,0,0))"
dw_report_manut_eseguite.Object.cod_tipo_manutenzione.Color = "0~tIf(ritardo='S',rgb(255,0,0),rgb(0,0,0))"
dw_report_manut_eseguite.Object.des_tipo_manutenzione.Color = "0~tIf(ritardo='S',rgb(255,0,0),rgb(0,0,0))"
//dw_report_manut_eseguite.Object.cod_operaio.Color = "0~tIf(ritardo='S',rgb(255,0,0),rgb(0,0,0))"
dw_report_manut_eseguite.Object.des_operaio.Color = "0~tIf(ritardo='S',rgb(255,0,0),rgb(0,0,0))"
end event

event ue_openparm();s_cs_xx_parametri lstr_parametri

lstr_parametri = message.powerobjectparm

setnull(message.powerobjectparm)

if isvalid(lstr_parametri) then
	
	if g_str.isnotempty(lstr_parametri.parametro_s_1) then
		dw_sel_manut_eseguite.setitem(1, "cod_attrezzatura", lstr_parametri.parametro_s_1)
		dw_sel_manut_eseguite.setitem(1, "flag_note_idl", 'S')
		cb_aggiorna.postevent("clicked")
	end if
	
end if
end event

public function integer wf_report ();long ll_riga, ll_ore_fermo, ll_minuti_fermo

datetime ldt_data_inizio, ldt_data_fine, ldt_data_registrazione

string ls_operatore, ls_cod_operatore, ls_cod_reparto_filtro, ls_des_attrezzatura, ls_des_tipo_manutenzione,&
       ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cognome, ls_nome, &
		 ls_cod_attrezzatura_filtro, ls_flag_scaduta, ls_cod_tipo_manutenzione_filtro, ls_ordinario, &
		 ls_cod_reparto, ls_cod_area_filtro, ls_cod_area_aziendale, ls_sql, ls_cod_cat_risorse_esterne, ls_des_risorsa, &
		 ls_cod_risorsa_esterna, ls_cod_cat_risorse_esterne_filtro, ls_cod_operaio_filtro, ls_cod_risorsa_esterna_filtro, &
		 ls_flag_note_idl_filtro, ls_note_idl, ls_des_intervento


dw_sel_manut_eseguite.accepttext()

ldt_data_inizio = dw_sel_manut_eseguite.getitemdatetime(1,"data_da")
//************claudia 13/03/06 commentato perchè viene impostato poi con un controllo del formato data
//if isnull(ldt_data_inizio) then
//	ldt_data_inizio = datetime(date("01/01/1900"), 00:00:00)
//end if
//
ldt_data_fine = dw_sel_manut_eseguite.getitemdatetime(1,"data_a")

//if isnull(ldt_data_fine) then
//	ldt_data_fine = datetime(date("31/12/2999"), 00:00:00)
//end if
//
ls_cod_attrezzatura_filtro = dw_sel_manut_eseguite.getitemstring(1,"cod_attrezzatura")

if isnull(ls_cod_attrezzatura_filtro) then
	ls_cod_attrezzatura_filtro = "%"
end if

ls_cod_reparto_filtro = dw_sel_manut_eseguite.getitemstring(1,"cod_reparto")

ls_cod_area_filtro = dw_sel_manut_eseguite.getitemstring(1,"cod_area_aziendale")

ls_cod_tipo_manutenzione_filtro = dw_sel_manut_eseguite.getitemstring(1,"cod_tipo_manutenzione")

ls_cod_operaio_filtro = dw_sel_manut_eseguite.getitemstring( 1, "cod_operaio")

ls_cod_cat_risorse_esterne_filtro = dw_sel_manut_eseguite.getitemstring( 1, "cod_cat_risorse_esterne")

ls_cod_risorsa_esterna_filtro = dw_sel_manut_eseguite.getitemstring( 1, "cod_risorsa_esterna")

if ls_cod_operaio_filtro = "" then setnull(ls_cod_operaio_filtro)

if ls_cod_cat_risorse_esterne_filtro = "" then setnull(ls_cod_cat_risorse_esterne_filtro)

if ls_cod_risorsa_esterna_filtro = "" then setnull(ls_cod_risorsa_esterna_filtro)

if isnull(ls_cod_tipo_manutenzione_filtro) then
	ls_cod_tipo_manutenzione_filtro = "%"
end if

ls_ordinario = dw_sel_manut_eseguite.getitemstring(1,"flag_ordinario")

ls_flag_note_idl_filtro = dw_sel_manut_eseguite.getitemstring(1,"flag_note_idl")

dw_report_manut_eseguite.reset()

// **** 26/10/2004: mi serve un cursore dinamico.
DECLARE manut_eseg DYNAMIC CURSOR FOR SQLSA ;
//*************27/09/07 claudia aggiunto il campo des_intervento che viene visualizzato quando idlnote è vuoto
ls_sql = " " + &
" select data_registrazione, " + &
" 		   cod_operaio, " + &
" 		   cod_cat_risorse_esterne, " + &
" 		   cod_risorsa_esterna, " + &
" 		   flag_scaduta, " + &
" 		   cod_attrezzatura, " + &
" 		   cod_tipo_manutenzione, " + &
" 		   note_idl, " + &
" 		   ore_fermo_macchina, " + &
" 		   minuti_fermo_macchina ," + &
" 		   des_intervento" + &
" from   manutenzioni " + &
" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
" 		flag_eseguito = 'S' and  " + &
" 		cod_attrezzatura like '" + ls_cod_attrezzatura_filtro + "' and "

if ls_cod_tipo_manutenzione_filtro = "%" then

	ls_sql = ls_sql + " (cod_tipo_manutenzione like '" + ls_cod_tipo_manutenzione_filtro + "' OR cod_tipo_manutenzione IS NULL ) and " 		

else

	ls_sql = ls_sql + " cod_tipo_manutenzione like '" + ls_cod_tipo_manutenzione_filtro + "' and " 
	
end if
//**********claudia 13/03/06 controllo formato data in base al db
if not isnull(ldt_data_inizio) then
	ls_sql = ls_sql + " 		data_registrazione >= '" + string( ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' and " 
end if

if not isnull(ldt_data_fine) then
	ls_sql = ls_sql +" 		data_registrazione <= '" + string( ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' and "
end if
//*******fine modifica
ls_sql = ls_sql +" 		flag_ordinario like '" + ls_ordinario + "' "


//ls_sql = ls_sql + " 		data_registrazione >= '" + string( ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' and " + &
//" 		data_registrazione <= '" + string( ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' and " + &
//" 		flag_ordinario like '" + ls_ordinario + "' "
//
if not isnull(ls_cod_operaio_filtro) then
	ls_sql = ls_sql + " and cod_operaio = '" + ls_cod_operaio_filtro + "' "
end if

if not isnull(ls_cod_cat_risorse_esterne_filtro) then
	ls_sql = ls_sql + " and cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne_filtro + "' "
end if

if not isnull(ls_cod_risorsa_esterna_filtro) then
	ls_sql = ls_sql + " and cod_risorsa_esterna = '" + ls_cod_risorsa_esterna_filtro + "' "
end if

ls_sql += " ORDER BY data_registrazione ASC, cod_attrezzatura ASC, cod_tipo_manutenzione ASC "

PREPARE SQLSA FROM :ls_sql ;

OPEN DYNAMIC manut_eseg ;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch manut_eseg
	into  :ldt_data_registrazione,
			:ls_cod_operatore,
			:ls_cod_cat_risorse_esterne,
			:ls_cod_risorsa_esterna,
	      :ls_flag_scaduta,
			:ls_cod_attrezzatura,
			:ls_cod_tipo_manutenzione,
			:ls_note_idl,
			:ll_ore_fermo,
			:ll_minuti_fermo,
			:ls_des_intervento;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit		
	end if	

	ls_operatore = ""
	ls_des_risorsa = ""

	select cognome,
	       nome
	into   :ls_cognome, 
	       :ls_nome
	from   anag_operai
	where  cod_operaio = :ls_cod_operatore and 
	       cod_azienda = :s_cs_xx.cod_azienda;
	
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select dell'operatore. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		rollback;
		return -1
	end if
	
	if ls_cod_operatore = "" then setnull(ls_cod_operatore)
	
	if isnull(ls_cod_operatore) then 
		ls_cognome = ""
		ls_nome = ""
	end if
	
	
	select descrizione,
			 cod_reparto,
			 cod_area_aziendale
	into 	 :ls_des_attrezzatura,
			 :ls_cod_reparto,
			 :ls_cod_area_aziendale
	from 	 anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and &
			 cod_attrezzatura = :ls_cod_attrezzatura;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select dell'attrezzatura. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		rollback;
		return -1
	elseif sqlca.sqlcode=100 then
		sqlca.sqlcode=0
		continue
	end if
	
	if not isnull(ls_cod_reparto_filtro) then
		if isnull(ls_cod_reparto) or ls_cod_reparto <> ls_cod_reparto_filtro then
			continue
		end if
	end if
	
	if not isnull(ls_cod_area_filtro) then
		if isnull(ls_cod_area_aziendale) or ls_cod_area_aziendale <> ls_cod_area_filtro then
			continue
		end if
	end if
	
	select des_tipo_manutenzione 
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_attrezzatura = :ls_cod_attrezzatura and 
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		  
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select del tipo manutenzione. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		rollback;
		return -1
	end if	  
	
	if ls_cod_tipo_manutenzione = "" then setnull(ls_cod_tipo_manutenzione)
	
	if isnull(ls_cod_tipo_manutenzione) then ls_des_tipo_manutenzione = ""
	//***claudia 27/09/07 aggiunto ls_des_intervento
	if ls_flag_note_idl_filtro = "S" then
		if len(ls_des_tipo_manutenzione) > 0 then
			ls_des_tipo_manutenzione += "~r~n"
		end if
		if len(ls_note_idl) >0 then
			ls_des_tipo_manutenzione += ls_note_idl
		else
			if len(ls_des_intervento)>0 then
			ls_des_tipo_manutenzione +=  ls_des_intervento
		end if
		end if
	end if
	
	select descrizione
	into   :ls_des_risorsa
	from   anag_risorse_esterne
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
			 cod_risorsa_esterna = :ls_cod_risorsa_esterna;
	
	if isnull(ls_cod_operatore) then ls_cod_operatore = ""
	if isnull(ls_cognome) then ls_cognome = ""
	if isnull(ls_nome) then ls_nome = ""
	if isnull(ls_cod_risorsa_esterna) then ls_cod_risorsa_esterna = ""
	if isnull(ls_des_risorsa) then ls_des_risorsa = ""

	ls_operatore = ls_cod_operatore + " " + ls_cognome + " " + ls_nome
	
	if len(ls_operatore) > 2 then ls_operatore = ls_operatore + "~r~n"
	
	ls_operatore = ls_operatore  + ls_cod_risorsa_esterna + " " + ls_des_risorsa
	
	ll_riga = dw_report_manut_eseguite.insertrow(0)
	
	dw_report_manut_eseguite.setitem(ll_riga,"ritardo",ls_flag_scaduta)
	
	dw_report_manut_eseguite.setitem(ll_riga,"data_giorno",ldt_data_registrazione)
	
	dw_report_manut_eseguite.setitem(ll_riga,"cod_attrezzatura",ls_cod_attrezzatura)
	
	dw_report_manut_eseguite.setitem(ll_riga,"des_attrezzatura",ls_des_attrezzatura)
	
	dw_report_manut_eseguite.setitem(ll_riga,"cod_tipo_manutenzione",ls_cod_tipo_manutenzione)
	
	dw_report_manut_eseguite.setitem(ll_riga,"des_tipo_manutenzione",ls_des_tipo_manutenzione)
	
	dw_report_manut_eseguite.setitem(ll_riga,"cod_operaio",ls_cod_operatore)
	
	dw_report_manut_eseguite.setitem(ll_riga,"des_operaio",ls_operatore)
	
	dw_report_manut_eseguite.setitem(ll_riga,"ore_fermo",ll_ore_fermo)
	
	dw_report_manut_eseguite.setitem(ll_riga,"minuti_fermo",ll_minuti_fermo)
	
loop

close manut_eseg;

dw_report_manut_eseguite.resetupdate()
this.postevent("ue_setdwcolor")

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

windowobject lw_oggetti[]

dw_report_manut_eseguite.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_sel_manut_eseguite.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

dw_report_manut_eseguite.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nonew + &
													c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )
													
// *** folder													

lw_oggetti[1] = dw_report_manut_eseguite
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

lw_oggetti[1] = dw_sel_manut_eseguite
//lw_oggetti[2] = cb_attrezzature_ricerca
lw_oggetti[2] = cb_cancella_filtri
lw_oggetti[3] = cb_aggiorna
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)
													
// ***

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_manut_eseguite.modify(ls_modify)

dw_report_manut_eseguite.object.datawindow.print.preview = 'Yes'

// Open parm
postevent("ue_openparm")
end event

on w_report_manut_eseguite.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_report_manut_eseguite=create dw_report_manut_eseguite
this.dw_folder=create dw_folder
this.dw_sel_manut_eseguite=create dw_sel_manut_eseguite
this.cb_aggiorna=create cb_aggiorna
this.cb_cancella_filtri=create cb_cancella_filtri
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_report_manut_eseguite
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_sel_manut_eseguite
this.Control[iCurrent+5]=this.cb_aggiorna
this.Control[iCurrent+6]=this.cb_cancella_filtri
end on

on w_report_manut_eseguite.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_report_manut_eseguite)
destroy(this.dw_folder)
destroy(this.dw_sel_manut_eseguite)
destroy(this.cb_aggiorna)
destroy(this.cb_cancella_filtri)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_manut_eseguite,"cod_reparto",sqlca,"anag_reparti","cod_reparto","des_reparto",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_manut_eseguite,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
					  "des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio"," nome + ' ' + cognome ",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")							  					  
					  
end event

type cb_1 from commandbutton within w_report_manut_eseguite
integer x = 2537
integer y = 416
integer width = 402
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "none"
end type

type dw_report_manut_eseguite from uo_cs_xx_dw within w_report_manut_eseguite
event ue_post_retrieve ( )
integer x = 46
integer y = 140
integer width = 3611
integer height = 1920
integer taborder = 0
string dataobject = "d_report_manut_eseguite"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;long ll_result
ll_result=wf_report()
if ll_result=-1 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella creazione del report")
end if	
end event

type dw_folder from u_folder within w_report_manut_eseguite
integer x = 23
integer width = 3680
integer height = 2080
integer taborder = 10
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Report"
      SetFocus(dw_report_manut_eseguite)
		
   CASE "Selezione"
      SetFocus(dw_sel_manut_eseguite)

END CHOOSE

end event

type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_manut_eseguite
integer x = 46
integer y = 140
integer width = 3611
integer height = 780
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_sel_manut_eseguite"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case i_colname
		
	case "cod_attrezzatura"

	string ls_null
	
	setnull(ls_null)
	
	setitem(row,"cod_tipo_manutenzione",ls_null)
	
	f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione","cod_tipo_manutenzione",&
					  	  "des_tipo_manutenzione","cod_azienda = '" + s_cs_xx.cod_azienda + &
						  "' and cod_attrezzatura = '" + i_coltext + "'")
						  
	case "cod_cat_risorse_esterne"
		
		if isnull(i_coltext) or i_coltext = "" then return 0
		
		this.setitem( row, "cod_risorsa_esterna", ls_null)
		
		f_PO_LoadDDDW_DW ( dw_sel_manut_eseguite, &
								 "cod_risorsa_esterna", &
								 sqlca, &
							  	 "anag_risorse_esterne", &
								 "cod_risorsa_esterna", &
								 "descrizione", &
							    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "' ")			
						  
	
end choose
						  
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_manut_eseguite,"cod_attrezzatura")
end choose
end event

type cb_aggiorna from commandbutton within w_report_manut_eseguite
integer x = 2181
integer y = 820
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;dw_report_manut_eseguite.change_dw_current()
setpointer(hourglass!)
dw_report_manut_eseguite.setredraw(false)
parent.triggerevent("pc_retrieve")
dw_report_manut_eseguite.setredraw(true)
dw_folder.fu_selecttab(2)
setpointer(arrow!)
end event

type cb_cancella_filtri from commandbutton within w_report_manut_eseguite
integer x = 1783
integer y = 820
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cancella filtri"
end type

event clicked;string   ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_da",ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_a",ldt_null)

dw_sel_manut_eseguite.setitem(1,"cod_reparto",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_attrezzatura",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_tipo_manutenzione",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_area_aziendale",ls_null)

f_po_loaddddw_dw(dw_sel_manut_eseguite,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione","cod_tipo_manutenzione",&
					  "des_tipo_manutenzione","cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura is null")
end event

