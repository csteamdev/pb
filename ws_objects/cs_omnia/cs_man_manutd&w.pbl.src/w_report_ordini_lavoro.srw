﻿$PBExportHeader$w_report_ordini_lavoro.srw
forward
global type w_report_ordini_lavoro from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_ordini_lavoro
end type
type st_2 from statictext within w_report_ordini_lavoro
end type
type st_1 from statictext within w_report_ordini_lavoro
end type
type cb_annulla from commandbutton within w_report_ordini_lavoro
end type
type cb_report from commandbutton within w_report_ordini_lavoro
end type
type dw_selezione from uo_cs_xx_dw within w_report_ordini_lavoro
end type
type dw_report from uo_cs_xx_dw within w_report_ordini_lavoro
end type
type dw_folder from u_folder within w_report_ordini_lavoro
end type
end forward

global type w_report_ordini_lavoro from w_cs_xx_principale
integer width = 3616
integer height = 2232
string title = "Report Ordine di Lavoro"
boolean resizable = false
event ue_seleziona_manut ( )
cb_stampa cb_stampa
st_2 st_2
st_1 st_1
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_ordini_lavoro w_report_ordini_lavoro

type variables
datetime idt_data_registrazione

string is_stampa, is_cod_attrezzatura, is_cod_tipo_manutenzione, is_mezzo_stampa

long 	 il_anno[], il_numero[]
end variables

forward prototypes
public function integer wf_report ()
public function integer wf_stampa_su_mail ()
public function integer wf_carica_singola_registrazione (long fl_anno, long fl_numero, ref string fs_errore, ref blob fb_blob, ref string fs_allegato)
public function blob wf_carica_blob (string fs_path)
public subroutine wf_aggiorna_text ()
public subroutine wf_aggiorna_text_ds (datastore fds_dati)
end prototypes

event ue_seleziona_manut();dw_selezione.setitem(1,"rd_da_data",idt_data_registrazione)

dw_selezione.setitem(1,"rd_a_data",idt_data_registrazione)

dw_selezione.setitem(1,"attrezzatura_da",is_cod_attrezzatura)

dw_selezione.setitem(1,"attrezzatura_a",is_cod_attrezzatura)

dw_selezione.setitem(1,"cod_tipo_manutenzione",is_cod_tipo_manutenzione)

dw_selezione.setitem(1,"flag_ordinaria","S")

dw_selezione.setitem(1,"flag_ric_car","S")

dw_selezione.setitem(1,"flag_ric_tel","S")

dw_selezione.setitem(1,"flag_giro_isp","S")

dw_selezione.setitem(1,"flag_altre","S")

dw_selezione.setitem(1,"flag_stampati","T")

dw_selezione.setitem(1,"flag_eseguito","T")

cb_report.postevent("clicked")
end event

public function integer wf_report ();integer  li_riga, li_num_ricambi

long     ll_count, ll_index

datetime ldt_da_data, ldt_a_data, ldt_data_registrazione, ldt_today

string   ls_path_logo, ls_modify, ls_cod_operatore, ls_attr_da, ls_attr_a, ls_tipo_manut, ls_categoria, ls_cod_reparto, &
         ls_area, ls_stampati, ls_tipo_stampa, ls_cod_cat_risorse_esterne_sel, ls_cod_risorsa_esterna_sel, ls_sql, ls_prova, &
			new_select, ls_divisione

dw_selezione.accepttext()

dw_report.reset()

dw_report.dataobject = "d_report_ordine_lavoro_sql"

dw_report.settransobject(sqlca)

ldt_da_data = dw_selezione.getitemdatetime(1,"rd_da_data")

ldt_a_data = dw_selezione.getitemdatetime(1,"rd_a_data")

ls_cod_operatore = dw_selezione.getitemstring(1,"rs_cod_operatore")

ls_attr_da = dw_selezione.getitemstring(1,"attrezzatura_da")

ls_attr_a = dw_selezione.getitemstring(1,"attrezzatura_a")

ls_tipo_manut = dw_selezione.getitemstring(1,"cod_tipo_manutenzione")

ls_categoria = dw_selezione.getitemstring(1,"cod_categoria")

ls_cod_reparto = dw_selezione.getitemstring(1,"rs_cod_reparto")

ls_area = dw_selezione.getitemstring(1,"cod_area")

ls_divisione = dw_selezione.getitemstring(1,"cod_divisione")

ls_stampati = dw_selezione.getitemstring(1,"flag_stampati")

ls_tipo_stampa = dw_selezione.getitemstring(1,"flag_tipo_stampa")

ls_cod_cat_risorse_esterne_sel = dw_selezione.getitemstring( 1, "cod_cat_risorse_esterne")

ls_cod_risorsa_esterna_sel = dw_selezione.getitemstring( 1, "cod_risorsa_esterna")

if ls_cod_cat_risorse_esterne_sel = "" then setnull(ls_cod_cat_risorse_esterne_sel)

if ls_cod_risorsa_esterna_sel = "" then setnull(ls_cod_risorsa_esterna_sel)

ls_sql= "  manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		  "       manutenzioni.flag_eseguito = 'N' "

if not isnull(ldt_da_data) then ls_sql += " and manutenzioni.data_registrazione >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "

if not isnull(ldt_a_data) then ls_sql += " and manutenzioni.data_registrazione <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "

if not isnull(ls_cod_operatore) then ls_sql = ls_sql + " and manutenzioni.cod_operaio = '" + ls_cod_operatore + "' "

if not isnull(ls_attr_da) then ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura >= '" + ls_attr_da + "' "

if not isnull(ls_attr_a) then ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura <= '" + ls_attr_a + "' "

if not isnull(ls_tipo_manut) then ls_sql = ls_sql + " and manutenzioni.cod_tipo_manutenzione = '" + ls_tipo_manut + "' "

if not isnull(ls_categoria) then	ls_sql = ls_sql + " and anag_attrezzature.cod_cat_attrezzature = '" + ls_categoria + "' "

if not isnull(ls_cod_reparto) then	ls_sql = ls_sql + " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "

if not isnull(ls_area) then ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale = '" + ls_area + "' "

if not isnull(ls_divisione) then ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale in ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "

if not isnull(ls_cod_cat_risorse_esterne_sel) then ls_sql = ls_sql + " and manutenzioni.cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne_sel + "' "

if not isnull(ls_cod_risorsa_esterna_sel) then ls_sql = ls_sql + " and manutenzioni.cod_risorsa_esterna = '" + ls_cod_risorsa_esterna_sel + "' "

choose case ls_stampati
		
	case "S"
		
		ls_sql += " and manutenzioni.flag_stampato = 'S' "
		
	case "N"
		
		ls_sql += " and manutenzioni.flag_stampato = 'N' "
		
end choose

//ls_sql = ls_sql + " group by manutenzioni.anno_registrazione , " + &
//                  "		     manutenzioni.num_registrazione " 
ls_sql = ls_sql + " order by manutenzioni.anno_registrazione ASC, " + &
                  "		     manutenzioni.num_registrazione ASC   "


ls_prova = upper(dw_report.GETSQLSELECT())

ll_index = pos(ls_prova, "WHERE")

if ll_index = 0 then
	
	new_select = dw_report.GETSQLSELECT() + " WHERE " + ls_sql
	
else	
	
	new_select = dw_report.getsqlselect() + " and " + ls_sql //left(dw_report.GETSQLSELECT(), ll_index - 1) + ls_sql
	
end if

ll_index = 0

ll_index = dw_report.SetSQLSelect(new_select)

if ll_index < 0 then 
	
	g_mb.messagebox( "OMNIA", "Errore durante l'impostazione della select: " + ls_sql)
	
	return -1
	
else
	
	dw_report.resetupdate()
	
	ll_index = dw_report.retrieve()

	if ll_index < 0 then
		
		g_mb.messagebox( "OMNIA", "Errore durante la retrieve della select: " + ls_sql + " ; " + sqlca.sqlerrtext)	
		
		return -1
		
	end if
	
end if

ll_index = 0

ll_count = 0

for ll_index = 1 to dw_report.rowcount()

	ll_count ++
	
	il_anno[ll_count] = dw_report.getitemnumber( ll_index, "anno_registrazione")
	
	il_numero[ll_count] = dw_report.getitemnumber( ll_index, "num_registrazione")	

next

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)

if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","Manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

dw_report.modify(ls_modify)

// *** michela 03/02/2005: attenzione, se si modificano i nomi delle label dei nested che compongono codesto report (w_report)
//                         è OBBLIGATORIO consultare la funzione che segue e aggiornarne lo script. infatti poichè non si può
//                         usare il .modify() per i nested, bisogna usare la dot notation e QUINDI FARE UN CASE STATICO!!

wf_aggiorna_text()

// ***

dw_report.sort()

dw_report.groupcalc()

dw_report.SetRedraw(true)

dw_report.change_dw_current()

dw_folder.fu_selecttab(2)

return 0
end function

public function integer wf_stampa_su_mail ();datetime ldt_today, ldt_oggi, ldt_ora

integer  li_response

long     li_ret, ll_anno_reg_programma, ll_num_reg_programma, ll_count, ll_tot_destinatari, ll_i, ll_numero, ll_anno, &
         ll_destinatari, ll_num_sequenza

string   ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_des_destinatario, ls_email, ls_cod_destinatario, &
         ls_allegati[], ls_destinatari[], ls_msg, ls_messaggio, ls_flag_inviato
			
string ls_db
				
integer li_risposta
				
transaction sqlcb

uo_outlook luo_outlook

is_stampa = dw_selezione.getitemstring( 1, "flag_tipo_stampa")

is_mezzo_stampa = dw_selezione.getitemstring( 1, "flag_mezzo_stampa")
		
if is_stampa <> "D" then return -1
			
for ll_i = 1 to dw_report.rowcount()

	ll_anno = dw_report.getitemnumber( ll_i, "anno_registrazione")
			
	ll_numero = dw_report.getitemnumber( ll_i, "num_registrazione")

	// *** trovo la lista di distribuzione
			
	select anno_reg_programma,
			 num_reg_programma
	into   :ll_anno_reg_programma,
			 :ll_num_reg_programma
	from   manutenzioni 
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero;
				 
	select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_cod_tipo_lista_dist,
			 :ls_cod_lista_dist
	from   programmi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_reg_programma and
			 num_registrazione = :ll_num_reg_programma;
				 
	if isnull(ls_cod_tipo_lista_dist) or isnull(ls_cod_lista_dist) or ls_cod_tipo_lista_dist = "" or ls_cod_lista_dist = "" then 
		
		g_mb.messagebox( "OMNIA", "Attenzione: l'ordine di lavoro " + string(ll_anno) + "/" + string(ll_numero) + " è sprovvisto di lista di distribuzione!")
		
		continue					 
	
	end if
				 
	declare cu_destinatari cursor for		
	select cod_destinatario,
			 num_sequenza
	from   det_liste_dist
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
			 cod_lista_dist = :ls_cod_lista_dist ;
			
	open cu_destinatari;
			
	do while 1 = 1		
			
		fetch cu_destinatari into :ls_cod_destinatario,
										  :ll_num_sequenza;
		if sqlca.sqlcode = 100 then exit
			
		if sqlca.sqlcode < 0 then 
					
			g_mb.messagebox( "OMNIA", "Errore durante la fetch dei destinatari: " + sqlca.sqlerrtext)
					
			close cu_destinatari;
					
			return -1
		
		end if
				
		//**** controllo se esiste già quel destinatario
				
		select count(*)				
		into   :ll_destinatari
		from   manutenzioni_destinatari
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero and 
				 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza = :ll_num_sequenza;				
				
		if sqlca.sqlcode < 0 then
					
			g_mb.messagebox( "OMNIA", "Errore in ricerca destinatari manutenzione: " + sqlca.sqlerrtext)
					
			continue
				
		end if
				
		if ll_destinatari = 0 or isnull(ll_destinatari) then		//*** non ho trovato il record, quindi non è ancora inserito 				 
									 								
			insert into manutenzioni_destinatari( cod_azienda,
															  anno_registrazione,
															  num_registrazione,
															  cod_tipo_lista_dist,
															  cod_lista_dist,
															  cod_destinatario,
															  num_sequenza,
															  flag_inviato)
													values( :s_cs_xx.cod_azienda,
															  :ll_anno,
															  :ll_numero,
															  :ls_cod_tipo_lista_dist,
															  :ls_cod_lista_dist,
															  :ls_cod_destinatario,
															  :ll_num_sequenza,
															  'N');
																	  
			ls_flag_inviato = "N"																	  
					
		end if
				
		select flag_inviato				
		into   :ls_flag_inviato
		from   manutenzioni_destinatari
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero and 
				 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza = :ll_num_sequenza;				
							
		if ls_flag_inviato = 'S' then continue		
			
		select des_destinatario,
			    indirizzo
		into   :ls_des_destinatario,
		       :ls_email
		from   tab_ind_dest
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_destinatario = :ls_cod_destinatario; 
						
		if isnull(ls_email) then continue
		
		// *** creo l'allegato
		
		blob lb_blob
		
		integer li_ris
		
		string ls_errore
		
		li_ris = wf_carica_singola_registrazione( ll_anno, ll_numero, ls_errore, lb_blob, ls_allegati[1])
		
		if li_ris < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante la creazione dell'allegato: " + ls_errore)
			
			ls_allegati[1] = ""
			
		end if
		
		// ***		
		
		ls_destinatari[1] = ls_email
				
		luo_outlook = create uo_outlook						
		
		li_response = - 5
					
		li_response = luo_outlook.uof_outlook( 0, &
		                                       "A", &
															"Ordine di lavoro " + string(ll_anno) + "/" + string(ll_numero), &
															"Si invia in allegato la stampa dell'ordine di lavoro. ", &
															ls_destinatari, &
															ls_allegati, &
															false, &
															ls_messaggio)
															
					
		if li_response = 0 then
		
				ldt_oggi = datetime(date(today()), 00:00:00)
				ldt_ora = datetime(date("01/01/1900"), time(today()))
						
				update manutenzioni_destinatari
				set    flag_inviato = 'S',
						 data_invio = :ldt_oggi,
						 ora_invio = :ldt_ora
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno and
						 num_registrazione = :ll_numero and
						 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
						 cod_lista_dist = :ls_cod_lista_dist and
						 cod_destinatario = :ls_cod_destinatario and
						 num_sequenza = :ll_num_sequenza;	
						 
				if sqlca.sqlcode < 0 then
					
					g_mb.messagebox( "OMNIA", "Errore durante l'aggiornamento invio e-mail: " + sqlca.sqlerrtext)					
					
				end if
				
				ls_db = f_db()
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob manutenzioni_destinatari
					set        blob = :lb_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
							     anno_registrazione = :ll_anno and
							     num_registrazione = :ll_numero and
							     cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
							     cod_lista_dist = :ls_cod_lista_dist and
							     cod_destinatario = :ls_cod_destinatario and
							     num_sequenza = :ll_num_sequenza	
					using      sqlcb;
					
					if sqlcb.sqlcode < 0 then
						
						g_mb.messagebox("Omnia","Il salvataggio dell'oggetto nel documento ha provocato il seguente errore: " + sqlcb.sqlerrtext,stopsign!)
						
						destroy sqlcb;
						
					end if	
					
					destroy sqlcb;
					
				else
				
					updateblob manutenzioni_destinatari
					set        blob = :lb_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
							     anno_registrazione = :ll_anno and
							     num_registrazione = :ll_numero and
							     cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
							     cod_lista_dist = :ls_cod_lista_dist and
							     cod_destinatario = :ls_cod_destinatario and
							     num_sequenza = :ll_num_sequenza;
					
					if sqlca.sqlcode < 0 then
						
						g_mb.messagebox("Omnia","Il salvataggio dell'oggetto nel documento ha provocato il seguente errore: " + sqlca.sqlerrtext,stopsign!)

					end if
				
				end if				
				
		else
						
			g_mb.messagebox( "OMNIA", "Errore durante l'invio al seguente destinatario: " + ls_des_destinatario)
			
			g_mb.messagebox( "OMNIA", "Dettaglio: " + ls_messaggio)
						
		end if
		
		FileDelete(ls_allegati[1])
					
		destroy luo_outlook															  
				
	loop				
				
	close cu_destinatari;
			
	if sqlca.sqlcode < 0 then
				
		g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore: " + sqlca.sqlerrtext)
	
	end if
										
	commit;			
			
next
	
g_mb.messagebox( "OMNIA", "Stampa completata.")

return 0
end function

public function integer wf_carica_singola_registrazione (long fl_anno, long fl_numero, ref string fs_errore, ref blob fb_blob, ref string fs_allegato);long ll_anno_registrazione, ll_num_registrazione, ll_pos, ll_index

string ls_sql, ls_prova, new_select, ls_dir_temp, ls_str
		 
integer li_riga, li_ret, li_FileNum, li_risposta

datetime ldt_data_registrazione

datastore lds_manutenzione

lds_manutenzione = create datastore

lds_manutenzione.dataobject = "d_report_ordine_lavoro_sql"

lds_manutenzione.settransobject(sqlca)

setnull(fb_blob)

ls_sql= " anno_registrazione = " + string(fl_anno) + " and " + &
"         num_registrazione = " + string(fl_numero) + " " 

ls_prova = upper(lds_manutenzione.GETSQLSELECT())

ll_index = pos(ls_prova, "WHERE")

if ll_index = 0 then
	
	new_select = lds_manutenzione.GETSQLSELECT() + " WHERE " + ls_sql
	
else	
	
	new_select = lds_manutenzione.GETSQLSELECT() + " AND " + ls_sql
	
end if

ll_index = 0

ll_index = lds_manutenzione.SetSQLSelect(new_select)

if ll_index < 0 then 
	
	g_mb.messagebox( "OMNIA", "Errore durante l'impostazione della select: " + ls_sql)
	
	return -1
	
else
	
	lds_manutenzione.resetupdate()
	
	ll_index = lds_manutenzione.retrieve()

	if ll_index < 0 then
		
		g_mb.messagebox( "OMNIA", "Errore durante la retrieve della select: " + ls_sql + " ; " + sqlca.sqlerrtext)	
		
		return -1
		
	end if
	
end if
		
string ls_path_logo, ls_modify

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode = 0 then 

	ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

end if

lds_manutenzione.sort()

lds_manutenzione.groupcalc()

lds_manutenzione.resetupdate()

lds_manutenzione.modify(ls_modify)

// *** michela 03/02/2005: attenzione, se si modificano i nomi delle label dei nested che compongono codesto report (w_report)
//                         è OBBLIGATORIO consultare la funzione che segue e aggiornarne lo script. infatti poichè non si può
//                         usare il .modify() per i nested, bisogna usare la dot notation e QUINDI FARE UN CASE STATICO!!

wf_aggiorna_text_ds(lds_manutenzione)

// ***
// *** leggo la dir temporanea dove mettere il file
	
setnull(ls_dir_temp)

select stringa
into   :ls_dir_temp
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_parametro = 'TDD';

if sqlca.sqlcode = -1 then
		
	fs_errore = "Attenzione: Errore nella select del parametro TDD! Dettaglio: " + sqlca.sqlerrtext
	
	rollback;
		
	return -1	
			
end if

if sqlca.sqlcode = 100 then
		
	setnull(ls_dir_temp)
		
	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "ris", ls_dir_temp)	
		
end if

if isnull(ls_dir_temp) or ls_dir_temp = "" then
		
	fs_errore = "Attenzione: impostare il parametro aziendale TDD o il parametro del registro ris. "
		
	return -1		
		
end if

setnull(ls_str)
	
ls_str = left(ls_dir_temp, 1)
	
if ls_str <> "\" then ls_dir_temp = "\" + ls_dir_temp
	
setnull(ls_str)
	
ls_str = right(ls_dir_temp, 1)
	
if ls_str <> "\" then ls_dir_temp = ls_dir_temp + "\"
	
ls_dir_temp = s_cs_xx.volume + ls_dir_temp	

// *** creo il pdf

li_ret = lds_manutenzione.SaveAs( ls_dir_temp + string(fl_anno) + string(fl_numero) + ".PDF", PDF!, true)

if li_ret <> 1 then
	
	fs_errore = "Errore durante la creazione del file pdf!"
	
	return -1
	
end if

destroy lds_manutenzione;

fb_blob = wf_carica_blob(ls_dir_temp + string(fl_anno) + string(fl_numero) + ".PDF")

fs_allegato = ls_dir_temp + string(fl_anno) + string(fl_numero) + ".PDF"

return 0
end function

public function blob wf_carica_blob (string fs_path);long ll_filelen, ll_bytes_read, ll_new_pos
integer li_counter, li_loops, fh1

blob lb_tempblob, lb_bufferblob, lb_resultblob, lb_blankblob
		
// Trova la lunghezza del file
ll_filelen = FileLength(fs_path)

// FileRead legge 32KB alla volta => Se il file supera i 32 KB, SERVONO PIU' CICLI
// Calcola il numero di cicli
IF ll_filelen > 32765 THEN 
	IF Mod(ll_filelen,32765) = 0 THEN 
		li_loops = ll_filelen/32765 
	ELSE 
		// ...se c'è il resto, serve un ciclo in più
		li_loops = (ll_filelen/32765) + 1 
	END IF 
ELSE 
	li_loops = 1 
END IF

//carico il blob
fh1 = FileOpen(fs_path, StreamMode!)
if fh1 <> -1 then
	ll_new_pos = 0
	// Esegue li_loops cicli
	FOR li_counter = 1 to li_loops 
		// Legge i dati in una variabile TEMPORANEA, di tipo BLOB
		ll_bytes_read = FileRead(fh1, lb_tempblob) 
		// ...e li accoda in un secondo BLOB
		lb_bufferblob = lb_bufferblob + lb_tempblob 
		ll_new_pos = ll_new_pos + ll_bytes_read 
		// Sposta il puntatore al file nella posizione di lettura successiva
		FileSeek(fh1,ll_new_pos,FROMBEGINNING!)
		// Accoda i segmenti alla cache fino a costituire una "tranche" di circa 1 mega
		if len(lb_bufferblob) > 1000000 then
			// Se la dimensione limite è superata, accoda i dati in un terzo BLOB "risultato"
			lb_resultblob = lb_resultblob + lb_bufferblob 
			// ...e "sega" il BLOB temporaneo 2 assegnandoli un blob nullo
			lb_resultblob = lb_blankblob 
		end if 
	NEXT
	// Accoda i dati restanti in total_blob
	lb_resultblob = lb_resultblob + lb_bufferblob 
	// Fine lettura
	FileClose(fh1)
	
end if

return lb_resultblob


end function

public subroutine wf_aggiorna_text ();string ls_dw_1, ls_dw_2, ls_dw_3, ls_dw_4, ls_modify, ls_nome_oggetto, ls_testo

long    ll_i

ls_dw_2 = dw_report.Object.dw_2.DataObject

ls_dw_3 = dw_report.Object.dw_3.DataObject

ls_dw_4 = dw_report.Object.dw_4.DataObject

if dw_report.rowcount() < 1 then return 

declare cu_2 cursor for
select nome_oggetto, 
       testo
from   tab_dw_text_repository
where  cod_azienda = :s_cs_xx.cod_azienda and
		 nome_dw = :ls_dw_2 and
       testo is not null and
		 cod_lingua is null;
		 
open cu_2;

do while true
	fetch cu_2 into :ls_nome_oggetto, 
	                   :ls_testo;
							 
	if sqlca.sqlcode <> 0 then exit
	
	choose case ls_nome_oggetto
		case "t_1"
			dw_report.Object.dw_2.Object.t_1.text = ls_testo				
	end choose
	
loop

close cu_2;

declare cu_3 cursor for
select nome_oggetto, 
       testo
from   tab_dw_text_repository
where  cod_azienda = :s_cs_xx.cod_azienda and
		 nome_dw = :ls_dw_3 and
       testo is not null and
		 cod_lingua is null;
		 
open cu_3;

do while true
	fetch cu_3 into :ls_nome_oggetto, 
	                :ls_testo;
							 
	if sqlca.sqlcode <> 0 then exit

	choose case ls_nome_oggetto
		case "t_2"
			dw_report.Object.dw_3.Object.t_2.text = ls_testo				
		case "t_4"
			dw_report.Object.dw_3.Object.t_4.text = ls_testo			
		case "t_5"
			dw_report.Object.dw_3.Object.t_5.text = ls_testo
		case "t_9"
			dw_report.Object.dw_3.Object.t_9.text = ls_testo
		case "t_10"
			dw_report.Object.dw_3.Object.t_10.text = ls_testo
		case "t_11"
			dw_report.Object.dw_3.Object.t_11.text = ls_testo
		case "t_12"
			dw_report.Object.dw_3.Object.t_12.text = ls_testo
		case "t_13"
			dw_report.Object.dw_3.Object.t_13.text = ls_testo
		case "t_14"
			dw_report.Object.dw_3.Object.t_14.text = ls_testo				
	end choose

loop

close cu_3;

declare cu_4 cursor for
select nome_oggetto, 
       testo
from   tab_dw_text_repository
where  cod_azienda = :s_cs_xx.cod_azienda and
		 nome_dw = :ls_dw_4 and
       testo is not null and
		 cod_lingua is null;
		 
open cu_4;

do while true
	fetch cu_4 into :ls_nome_oggetto, 
	                   :ls_testo;
							 
	if sqlca.sqlcode <> 0 then exit
	
	choose case ls_nome_oggetto
		case "t_2"
			dw_report.Object.dw_4.Object.t_2.text = ls_testo				
		case "t_4"
			dw_report.Object.dw_4.Object.t_4.text = ls_testo			
	end choose
	
loop

close cu_4;

return
end subroutine

public subroutine wf_aggiorna_text_ds (datastore fds_dati);string ls_dw_1, ls_dw_2, ls_dw_3, ls_dw_4, ls_modify, ls_nome_oggetto, ls_testo

long    ll_i

ls_dw_2 = dw_report.Object.dw_2.DataObject

ls_dw_3 = dw_report.Object.dw_3.DataObject

ls_dw_4 = dw_report.Object.dw_4.DataObject

if fds_dati.rowcount() < 1 then return 

declare cu_2 cursor for
select nome_oggetto, 
       testo
from   tab_dw_text_repository
where  cod_azienda = :s_cs_xx.cod_azienda and
		 nome_dw = :ls_dw_2 and
       testo is not null and
		 cod_lingua is null;
		 
open cu_2;

do while true
	fetch cu_2 into :ls_nome_oggetto, 
	                   :ls_testo;
							 
	if sqlca.sqlcode <> 0 then exit
	
	choose case ls_nome_oggetto
		case "t_1"
			fds_dati.Object.dw_2.Object.t_1.text = ls_testo				
	end choose
	
loop

close cu_2;

declare cu_3 cursor for
select nome_oggetto, 
       testo
from   tab_dw_text_repository
where  cod_azienda = :s_cs_xx.cod_azienda and
		 nome_dw = :ls_dw_3 and
       testo is not null and
		 cod_lingua is null;
		 
open cu_3;

do while true
	fetch cu_3 into :ls_nome_oggetto, 
	                :ls_testo;
							 
	if sqlca.sqlcode <> 0 then exit

	choose case ls_nome_oggetto
		case "t_2"
			fds_dati.Object.dw_3.Object.t_2.text = ls_testo				
		case "t_4"
			fds_dati.Object.dw_3.Object.t_4.text = ls_testo			
		case "t_5"
			fds_dati.Object.dw_3.Object.t_5.text = ls_testo
		case "t_9"
			fds_dati.Object.dw_3.Object.t_9.text = ls_testo
		case "t_10"
			fds_dati.Object.dw_3.Object.t_10.text = ls_testo
		case "t_11"
			fds_dati.Object.dw_3.Object.t_11.text = ls_testo
		case "t_12"
			fds_dati.Object.dw_3.Object.t_12.text = ls_testo
		case "t_13"
			fds_dati.Object.dw_3.Object.t_13.text = ls_testo
		case "t_14"
			fds_dati.Object.dw_3.Object.t_14.text = ls_testo				
	end choose

loop

close cu_3;

declare cu_4 cursor for
select nome_oggetto, 
       testo
from   tab_dw_text_repository
where  cod_azienda = :s_cs_xx.cod_azienda and
		 nome_dw = :ls_dw_4 and
       testo is not null and
		 cod_lingua is null;
		 
open cu_4;

do while true
	fetch cu_4 into :ls_nome_oggetto, 
	                   :ls_testo;
							 
	if sqlca.sqlcode <> 0 then exit
	
	choose case ls_nome_oggetto
		case "t_2"
			fds_dati.Object.dw_4.Object.t_2.text = ls_testo				
		case "t_4"
			fds_dati.Object.dw_4.Object.t_4.text = ls_testo			
	end choose
	
loop

close cu_4;

return
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], l_vuoto[]

dw_report.ib_dw_report = true

l_objects[1] = dw_report
l_objects[2] = cb_stampa
dw_folder.fu_assigntab(2,"&Report",l_objects[])

l_objects[1] = dw_selezione
l_objects[2] = cb_annulla
l_objects[3] = cb_report
l_objects[4] = st_1
//l_objects[5] = cb_attrezzature_ricerca
//l_objects[6] = cb_attrezzature_ricerca_2
dw_folder.fu_assigntab(1,"&Selezione",l_objects[])

dw_folder.fu_folderoptions(dw_folder.c_defaultheight,dw_folder.c_foldertabtop)

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
								 c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_report

dw_report.object.datawindow.print.preview = "Yes"
end event

on w_report_ordini_lavoro.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.st_2=create st_2
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_report
this.Control[iCurrent+6]=this.dw_selezione
this.Control[iCurrent+7]=this.dw_report
this.Control[iCurrent+8]=this.dw_folder
end on

on w_report_ordini_lavoro.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_operatore",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW (dw_selezione,"cod_categoria",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW (dw_selezione,"rs_cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_selezione,"cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_selezione,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_selezione,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//Donato 23/06/2009 carica comunque le tipologie di manutenzione
//Specifica Metlac "Report_manutenzioni_26/02/2009"
dw_selezione.postevent("ue_carica_tipologie")
end event

type cb_stampa from commandbutton within w_report_ordini_lavoro
integer x = 3177
integer y = 40
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;datetime ldt_today
long     ll_ret

is_stampa = dw_selezione.getitemstring( 1, "flag_tipo_stampa")

is_mezzo_stampa = dw_selezione.getitemstring( 1, "flag_mezzo_stampa")

choose case is_mezzo_stampa
		
	case 'C'		
		
		dw_report.print()
		
	case 'M'   // *** stampa via mail
			
		wf_stampa_su_mail()	
		
		dw_report.postevent("controlla_flag_stampato")

	case 'E'
		
		wf_stampa_su_mail()	
		
		dw_report.print()
		
		dw_report.postevent("controlla_flag_stampato")
		
end choose
end event

type st_2 from statictext within w_report_ordini_lavoro
integer x = 46
integer y = 1340
integer width = 2377
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_ordini_lavoro
integer x = 46
integer y = 1240
integer width = 1600
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_report_ordini_lavoro
integer x = 1669
integer y = 1240
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string 	ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_selezione.setitem(1,"rd_da_data",ldt_null)

dw_selezione.setitem(1,"rd_a_data",ldt_null)

dw_selezione.setitem(1,"rs_cod_operatore",ls_null)

dw_selezione.setitem(1,"attrezzatura_da",ls_null)

dw_selezione.setitem(1,"attrezzatura_a",ls_null)

dw_selezione.setitem(1,"cod_tipo_manutenzione",ls_null)

dw_selezione.setitem(1,"cod_categoria",ls_null)

dw_selezione.setitem(1,"rs_cod_reparto",ls_null)

dw_selezione.setitem(1,"cod_area",ls_null)

dw_selezione.setitem(1,"cod_divisione",ls_null)

dw_selezione.setitem(1,"flag_stampati","N")

dw_selezione.setitem(1,"flag_tipo_stampa","P")

f_po_loaddddw_dw(dw_selezione,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione","cod_tipo_manutenzione", &
					  "des_tipo_manutenzione","cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura is null")
end event

type cb_report from commandbutton within w_report_ordini_lavoro
integer x = 2057
integer y = 1240
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_report.change_dw_current()

parent.postevent("pc_retrieve")
end event

type dw_selezione from uo_cs_xx_dw within w_report_ordini_lavoro
event ue_cod_attr ( )
event ue_tipo_stampa ( )
event ue_stato_stampa ( )
event ue_carica_tipologie ( )
event ue_mezzo_stampa ( )
event ue_aree_aziendali ( )
integer x = 46
integer y = 140
integer width = 2377
integer height = 1080
integer taborder = 20
string dataobject = "d_sel_ordini_lavoro"
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"attrezzatura_da")) then
	setitem(getrow(),"attrezzatura_a",getitemstring(getrow(),"attrezzatura_da"))
end if
end event

event ue_tipo_stampa();if getitemstring(getrow(),"flag_stampati") <> "N" then
	setitem(getrow(),"flag_tipo_stampa","P")
end if
end event

event ue_stato_stampa();if getitemstring(getrow(),"flag_tipo_stampa") = "D" then
	setitem(getrow(),"flag_stampati","N")
end if
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"attrezzatura_da")

ls_attr_a = getitemstring(getrow(),"attrezzatura_a")

ls_reparto = getitemstring(getrow(),"rs_cod_reparto")

ls_categoria = getitemstring(getrow(),"cod_categoria")

ls_area = getitemstring(getrow(),"cod_area")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_mezzo_stampa();string ls_flag_mezzo_stampa

ls_flag_mezzo_stampa = getitemstring( getrow(), "flag_mezzo_stampa")

choose case ls_flag_mezzo_stampa
		
	case 'C'
		
		setitem( getrow(), "flag_mezzo_stampa", "C")
		
	case 'M'
		
		setitem( getrow(), "flag_mezzo_stampa", "M")
		
	case 'E'
		
		setitem( getrow(), "flag_mezzo_stampa", "E")
		
	case else
		
		setitem( getrow(), "flag_mezzo_stampa", "C")
		
end choose


end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_DW (dw_selezione,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "'")
else
	f_PO_LoadDDDW_DW (dw_selezione,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  	
end if

triggerevent("ue_carica_tipologie")
end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
		
	case "attrezzatura_da"
		
		postevent("ue_cod_attr")
		
		postevent("ue_carica_tipologie")
		
	case "attrezzatura_a","rs_cod_reparto","cod_categoria","cod_area"
		
		postevent("ue_carica_tipologie")
		
	case "flag_tipo_stampa"
		
		postevent("ue_stato_stampa")
		
	case "flag_stampati"
		
		postevent("ue_tipo_stampa")
		
	case "flag_mezzo_stampa"
		
		postevent("ue_mezzo_stampa")
		
	case "cod_divisione"
		
		postevent("ue_aree_aziendali")
		
	case "cod_cat_risorse_esterne"
		
		if isnull(i_coltext) or i_coltext = "" then return 0
		
		this.setitem( row, "cod_risorsa_esterna", ls_null)
		
		f_PO_LoadDDDW_DW ( dw_selezione, &
								 "cod_risorsa_esterna", &
								 sqlca, &
							  	 "anag_risorse_esterne", &
								 "cod_risorsa_esterna", &
								 "descrizione", &
							    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "' ")			
						  
		
		
end choose




end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"attrezzatura_da")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"attrezzatura_a")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_ordini_lavoro
event controlla_flag_stampato ( )
boolean visible = false
integer x = 46
integer y = 140
integer width = 3520
integer height = 1960
integer taborder = 10
string dataobject = "d_report_ordine_lavoro_sql"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event controlla_flag_stampato();long ll_righe, ll_i, ll_anno, ll_numero, ll_ok

datetime ldt_today

ll_righe = dw_report.rowcount()

if ll_righe <= 0 or isnull(ll_righe) then return

for ll_i = 1 to ll_righe
	
	ll_anno = dw_report.getitemnumber( ll_i, "anno_registrazione")
	
	ll_numero = dw_report.getitemnumber( ll_i, "num_registrazione")
	
	ll_ok = 0
	
	select count(*)
	into   :ll_ok
	from   manutenzioni_destinatari
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero and 
			 flag_inviato = 'N';
	
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox( "OMNIA", "Errore durante controllo numero invio e-mail: " + sqlca.sqlerrtext)
		
		return
		
	end if
	
	if ll_ok = 0 then
		
		ldt_today = datetime(today(),00:00:00)
		
		update manutenzioni
		set    flag_stampato = 'S',
				 data_stampa = :ldt_today
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero;
	
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante aggiornamento flag stampato: " + sqlca.sqlerrtext)
		
			rollback;
		
			return
		
		end if	
		
		commit;	
	
	elseif ll_ok > 0 then
		
		update manutenzioni
		set    flag_stampato = 'N'
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero;
	
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante aggiornamento flag stampato: " + sqlca.sqlerrtext)
		
			rollback;
		
			return
		
		end if	
		
		commit;
			
	end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;setpointer(hourglass!)

dw_report.setredraw(false)

wf_report()

dw_report.setredraw(true)

setpointer(arrow!)
end event

event printpage;call super::printpage;datetime ldt_today						
			
uo_outlook  luo_outlook			


choose case is_mezzo_stampa
		
	case 'C'
		
		if is_stampa = "D" then
			
			ldt_today = datetime(today(),00:00:00)
			
			update manutenzioni
			set    flag_stampato = 'S',
					 data_stampa = :ldt_today
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno[pagenumber] and
					 num_registrazione = :il_numero[pagenumber];
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in update flag_stampato " + string(il_anno[pagenumber]) + &
							  "/" + string(il_numero[pagenumber]) + ": " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return -1
			end if
			
			commit;
			
		end if
	
	case 'M'
		

	
	case 'E'
		
		
	case else
		
end choose
end event

event printstart;call super::printstart;is_stampa = dw_selezione.getitemstring( 1, "flag_tipo_stampa")

is_mezzo_stampa = dw_selezione.getitemstring( 1, "flag_mezzo_stampa")
end event

event ue_anteprima_pdf;long		  ll_i, ll_anno, ll_numero
string	  ls_allegati[], ls_errore
blob       lb_blob
integer    li_ris
long ll_return, ll_elemento
string  ls_messaggio, ls_path

oleobject iole_outlook, iole_item, iole_attach
			

// ***	  Michela 26/02/2007: lasciare senza flag l'ancestor script!! altrimenti non va non va non va


ll_i = dw_report.getrow()

ll_anno = dw_report.getitemnumber( ll_i, "anno_registrazione")
ll_numero = dw_report.getitemnumber( ll_i, "num_registrazione")
		
li_ris = wf_carica_singola_registrazione( ll_anno, ll_numero, ls_errore, lb_blob, ls_allegati[1])

if li_ris < 0 then	
	g_mb.messagebox( "OMNIA", "Errore durante la creazione dell'allegato: " + ls_errore)
	return -1	
end if

ll_elemento = 0

setpointer(hourglass!)

s_cs_xx.parametri.parametro_dw_1 = this
s_cs_xx.parametri.parametro_s_1 = pcca.window_current.title

uo_archivia_pdf luo_pdf
luo_pdf = create uo_archivia_pdf

ls_path = ls_allegati[1]

iole_outlook = create oleobject

ll_return = iole_outlook.connecttonewobject("outlook.application")

if ll_return <> 0 THEN
	ls_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
	g_mb.messagebox ("Errore",ls_messaggio)
	
	destroy iole_outlook
	return -1
end if

// Viene creato un nuovo elemento del tipo specificato

iole_item = iole_outlook.createitem(ll_elemento)
//visualizza la mail da inviare
iole_item.display

iole_attach = iole_item.attachments
	
// Il file specificato viene allegato solo se esiste al percorso indicato
		
if fileexists(ls_path) then
	iole_attach.add(ls_path)
else
	ls_messaggio = "allegato inesistente" + string(ll_return)
	g_mb.messagebox ("Errore",ls_messaggio)
	destroy iole_attach
	destroy iole_outlook
	return -1
end if
		
destroy iole_item		
destroy iole_attach

FileDelete(ls_allegati[1])
			
return 0
end event

type dw_folder from u_folder within w_report_ordini_lavoro
integer x = 23
integer y = 20
integer width = 3566
integer height = 2100
integer taborder = 10
end type

