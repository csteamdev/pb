﻿$PBExportHeader$w_report_scad_manutenzioni.srw
$PBExportComments$Finestra Report Scadenzario Manutenzioni
forward
global type w_report_scad_manutenzioni from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_scad_manutenzioni
end type
type cb_report from commandbutton within w_report_scad_manutenzioni
end type
type cb_selezione from commandbutton within w_report_scad_manutenzioni
end type
type dw_selezione from uo_cs_xx_dw within w_report_scad_manutenzioni
end type
type dw_report from uo_cs_xx_dw within w_report_scad_manutenzioni
end type
end forward

global type w_report_scad_manutenzioni from w_cs_xx_principale
integer width = 3557
integer height = 1668
string title = "Report Attrezzature per Tarature"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_scad_manutenzioni w_report_scad_manutenzioni

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 618
this.y = 289
this.width = 2437
this.height = 793


end event

on w_report_scad_manutenzioni.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_scad_manutenzioni.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")



end event

type cb_annulla from commandbutton within w_report_scad_manutenzioni
integer x = 1623
integer y = 580
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_scad_manutenzioni
integer x = 2011
integer y = 580
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_attrezzatura, ls_cod_cat_attrezzature, ls_cod_operaio, ls_flag_manutenzione
datetime ldt_da_data, ldt_a_data

ls_cod_attrezzatura = dw_selezione.getitemstring(1,"rs_cod_attrezzatura")
ls_cod_cat_attrezzature = dw_selezione.getitemstring(1,"rs_cod_cat_attrezzatura")
ls_cod_operaio = dw_selezione.getitemstring(1,"rs_cod_operaio")
ls_flag_manutenzione = dw_selezione.getitemstring(1,"rs_flag_manutenzione")
ldt_da_data = dw_selezione.getitemdatetime(1,"rd_da_data")
ldt_a_data = dw_selezione.getitemdatetime(1,"rd_a_data")

if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = "%"
if isnull(ls_cod_cat_attrezzature) then ls_cod_cat_attrezzature = "%"
if isnull(ls_cod_operaio) then ls_cod_operaio = "%"
if isnull(ls_flag_manutenzione) then ls_flag_manutenzione = "%"
ldt_da_data = datetime(date("01/01/1900"))
ldt_a_data = datetime(date("31/12/2999"))

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()
dw_report.retrieve(s_cs_xx.cod_azienda, &
						 ls_cod_operaio, &
						 ls_cod_attrezzatura, &
						 ls_cod_cat_attrezzature, &
						 ls_flag_manutenzione, &
						 ldt_da_data,&
						 ldt_a_data)
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_scad_manutenzioni
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 618
parent.y = 289
parent.width = 2437
parent.height = 793

dw_report.hide()
cb_selezione.hide()


dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_scad_manutenzioni
integer x = 23
integer y = 20
integer width = 2354
integer height = 540
integer taborder = 20
string dataobject = "d_sel_report_scad_manutenzioni"
borderstyle borderstyle = stylelowered!
end type

type dw_report from uo_cs_xx_dw within w_report_scad_manutenzioni
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_scad_manutenzioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

