﻿$PBExportHeader$w_report_scheda_lavoro.srw
forward
global type w_report_scheda_lavoro from w_cs_xx_principale
end type
type dw_report_scheda_lavoro from uo_cs_xx_dw within w_report_scheda_lavoro
end type
end forward

global type w_report_scheda_lavoro from w_cs_xx_principale
integer width = 3794
integer height = 2064
string title = "Report Scheda di Lavoro"
boolean maxbox = false
boolean resizable = false
dw_report_scheda_lavoro dw_report_scheda_lavoro
end type
global w_report_scheda_lavoro w_report_scheda_lavoro

type variables
long il_anno, il_num
end variables

on w_report_scheda_lavoro.create
int iCurrent
call super::create
this.dw_report_scheda_lavoro=create dw_report_scheda_lavoro
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_scheda_lavoro
end on

on w_report_scheda_lavoro.destroy
call super::destroy
destroy(this.dw_report_scheda_lavoro)
end on

event pc_setwindow;call super::pc_setwindow;string ls_logo

dw_report_scheda_lavoro.ib_dw_report = true

il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_num = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

set_w_options(c_closenosave + c_noenablepopup)

iuo_dw_main = dw_report_scheda_lavoro

dw_report_scheda_lavoro.change_dw_current()

dw_report_scheda_lavoro.set_dw_options(sqlca, &
													pcca.null_object, &
													c_nonew + c_nomodify + c_nodelete, &
													c_nohighlightselected)
													
dw_report_scheda_lavoro.object.datawindow.print.preview = "Yes"

dw_report_scheda_lavoro.object.datawindow.print.preview.rulers = "Yes"

select stringa
into   :ls_logo
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'LO1';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura parametro LO1 da parametri_azienda: " + sqlca.sqlerrtext,stopsign!)
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","manca il parametro LO1 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
else
	dw_report_scheda_lavoro.modify("logo.filename='" + s_cs_xx.volume + ls_logo + "'")
end if
end event

type dw_report_scheda_lavoro from uo_cs_xx_dw within w_report_scheda_lavoro
integer x = 23
integer y = 20
integer width = 3735
integer height = 1940
integer taborder = 10
string dataobject = "d_report_scheda_lavoro"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,il_anno,il_num)
end event

