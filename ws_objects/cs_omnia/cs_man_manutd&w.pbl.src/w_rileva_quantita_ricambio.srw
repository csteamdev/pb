﻿$PBExportHeader$w_rileva_quantita_ricambio.srw
forward
global type w_rileva_quantita_ricambio from window
end type
type em_1 from editmask within w_rileva_quantita_ricambio
end type
type st_2 from statictext within w_rileva_quantita_ricambio
end type
type mle_ricambio from multilineedit within w_rileva_quantita_ricambio
end type
type cb_1 from commandbutton within w_rileva_quantita_ricambio
end type
type cb_ricambi from commandbutton within w_rileva_quantita_ricambio
end type
type em_quantita from editmask within w_rileva_quantita_ricambio
end type
type st_1 from statictext within w_rileva_quantita_ricambio
end type
end forward

global type w_rileva_quantita_ricambio from window
integer width = 2121
integer height = 872
boolean titlebar = true
string title = "Quantità Ricambio"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
em_1 em_1
st_2 st_2
mle_ricambio mle_ricambio
cb_1 cb_1
cb_ricambi cb_ricambi
em_quantita em_quantita
st_1 st_1
end type
global w_rileva_quantita_ricambio w_rileva_quantita_ricambio

on w_rileva_quantita_ricambio.create
this.em_1=create em_1
this.st_2=create st_2
this.mle_ricambio=create mle_ricambio
this.cb_1=create cb_1
this.cb_ricambi=create cb_ricambi
this.em_quantita=create em_quantita
this.st_1=create st_1
this.Control[]={this.em_1,&
this.st_2,&
this.mle_ricambio,&
this.cb_1,&
this.cb_ricambi,&
this.em_quantita,&
this.st_1}
end on

on w_rileva_quantita_ricambio.destroy
destroy(this.em_1)
destroy(this.st_2)
destroy(this.mle_ricambio)
destroy(this.cb_1)
destroy(this.cb_ricambi)
destroy(this.em_quantita)
destroy(this.st_1)
end on

event open;mle_ricambio.text = s_cs_xx.parametri.parametro_s_1
em_1.text = s_cs_xx.parametri.parametro_s_2
end event

type em_1 from editmask within w_rileva_quantita_ricambio
integer x = 571
integer y = 560
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
alignment alignment = right!
boolean displayonly = true
borderstyle borderstyle = styleraised!
string mask = "###,###.00"
end type

type st_2 from statictext within w_rileva_quantita_ricambio
integer x = 23
integer y = 580
integer width = 521
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Quantità Principale:"
boolean focusrectangle = false
end type

type mle_ricambio from multilineedit within w_rileva_quantita_ricambio
integer x = 23
integer y = 20
integer width = 2057
integer height = 520
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_rileva_quantita_ricambio
integer x = 1714
integer y = 680
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;s_cs_xx.parametri.parametro_b_1 = true
s_cs_xx.parametri.parametro_dec4_1 = dec(em_quantita.text)
close(parent)
end event

type cb_ricambi from commandbutton within w_rileva_quantita_ricambio
integer x = 23
integer y = 680
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_b_1 = false
close(parent)
end event

type em_quantita from editmask within w_rileva_quantita_ricambio
integer x = 1669
integer y = 560
integer width = 402
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###.00"
end type

type st_1 from statictext within w_rileva_quantita_ricambio
integer x = 1120
integer y = 580
integer width = 521
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Quantità Ricambio:"
boolean focusrectangle = false
end type

