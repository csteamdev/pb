﻿$PBExportHeader$w_scad_man_attrezzatura.srw
forward
global type w_scad_man_attrezzatura from w_cs_xx_principale
end type
type dw_scad_man_attrezzatura from uo_cs_xx_dw within w_scad_man_attrezzatura
end type
end forward

global type w_scad_man_attrezzatura from w_cs_xx_principale
integer width = 3899
integer height = 2244
string title = "Manutenzioni Schedulate Attrezzatura"
boolean maxbox = false
boolean resizable = false
dw_scad_man_attrezzatura dw_scad_man_attrezzatura
end type
global w_scad_man_attrezzatura w_scad_man_attrezzatura

type variables
string is_cod_attrezzatura
end variables

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();long     ll_riga

string   ls_des_attrezzatura, ls_cod_categoria, ls_des_categoria, ls_fabbricante, ls_modello, ls_matricola,&
         ls_cod_reparto, ls_des_reparto, ls_cod_area, ls_des_area, ls_cod_tipo, ls_des_tipo
		 
datetime ldt_acquisto, ldt_installazione, ldt_attivazione, ldt_scadenza, ldt_riferimento


ldt_riferimento = datetime(today(),00:00:00)

select descrizione,
       cod_cat_attrezzature,
		 fabbricante,
		 modello,
		 num_matricola,
		 cod_reparto,
		 cod_area_aziendale,
		 data_acquisto,
		 data_installazione,
		 data_prima_attivazione
into   :ls_des_attrezzatura,
       :ls_cod_categoria,
		 :ls_fabbricante,
		 :ls_modello,
		 :ls_matricola,
		 :ls_cod_reparto,
		 :ls_cod_area,
		 :ldt_acquisto,
		 :ldt_installazione,
		 :ldt_attivazione
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :is_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in lettura dati attrezzatura da anag_attrezzature: " + sqlca.sqlerrtext)
	return -1
end if

select des_cat_attrezzature
into   :ls_des_categoria
from   tab_cat_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cat_attrezzature = :ls_cod_categoria;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura categoria attrezzatura da tab_cat_attrezzature: " + sqlca.sqlerrtext)
	return -1
end if

select des_reparto
into   :ls_des_reparto
from   anag_reparti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_reparto = :ls_cod_reparto;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura reparto da anag_reparti: " + sqlca.sqlerrtext)
	return -1
end if

select des_area
into   :ls_des_area
from   tab_aree_aziendali
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_area_aziendale = :ls_cod_area;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura area aziendale da tab_aree_aziendali: " + sqlca.sqlerrtext)
	return -1
end if

dw_scad_man_attrezzatura.object.t_attrezzatura.text = is_cod_attrezzatura + ", " + ls_des_attrezzatura
dw_scad_man_attrezzatura.object.t_categoria.text = ls_cod_categoria + ", " + ls_des_categoria
dw_scad_man_attrezzatura.object.t_fabbricante.text = ls_fabbricante
dw_scad_man_attrezzatura.object.t_modello.text = ls_modello
dw_scad_man_attrezzatura.object.t_matricola.text = ls_matricola
dw_scad_man_attrezzatura.object.t_reparto.text = ls_cod_reparto + ", " + ls_des_reparto
dw_scad_man_attrezzatura.object.t_area.text = ls_cod_area + ", " + ls_des_area
dw_scad_man_attrezzatura.object.t_acquisto.text = string(date(ldt_acquisto))
dw_scad_man_attrezzatura.object.t_installazione.text = string(date(ldt_installazione))
dw_scad_man_attrezzatura.object.t_attivazione.text = string(date(ldt_attivazione))

ll_riga = dw_scad_man_attrezzatura.insertrow(0)
dw_scad_man_attrezzatura.setitem(ll_riga,"tipo_riga","E")

declare tipi_manut cursor for
select  cod_tipo_manutenzione,
		  des_tipo_manutenzione
from    tab_tipi_manutenzione
where   cod_azienda = :s_cs_xx.cod_azienda and
		  cod_attrezzatura = :is_cod_attrezzatura;
		  
open tipi_manut;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore tipi_manut: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch tipi_manut
	into  :ls_cod_tipo,
			:ls_des_tipo;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore tipi_manut: " + sqlca.sqlerrtext)
		close tipi_manut;
		return -1
	elseif sqlca.sqlcode = 100 then
		close tipi_manut;
		exit
	end if
	
	setnull(ldt_scadenza)
	
	select   min(det_cal_progr_manut.data_giorno)
	into     :ldt_scadenza
	from     det_cal_progr_manut,
			   programmi_manutenzione
	where    programmi_manutenzione.cod_azienda = det_cal_progr_manut.cod_azienda and
	         programmi_manutenzione.anno_registrazione = det_cal_progr_manut.anno_registrazione and
			   programmi_manutenzione.num_registrazione = det_cal_progr_manut.num_registrazione and
			   det_cal_progr_manut.cod_azienda = :s_cs_xx.cod_azienda and
			   programmi_manutenzione.cod_attrezzatura = :is_cod_attrezzatura and
			   programmi_manutenzione.cod_tipo_manutenzione = :ls_cod_tipo and
			   det_cal_progr_manut.data_giorno >= :ldt_riferimento and
				det_cal_progr_manut.flag_stato_manutenzione = 'A';
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select di det_cal_progr_manut: " + sqlca.sqlerrtext)
		close tipi_manut;
		return -1
	elseif sqlca.sqlcode = 100 or isnull(ldt_scadenza) then
		continue
	end if
	
	ll_riga = dw_scad_man_attrezzatura.insertrow(0)
	dw_scad_man_attrezzatura.setitem(ll_riga,"tipo_riga","D")
	dw_scad_man_attrezzatura.setitem(ll_riga,"des_tipo_manutenzione",ls_des_tipo)
	dw_scad_man_attrezzatura.setitem(ll_riga,"scadenza",ldt_scadenza)
	
loop

dw_scad_man_attrezzatura.resetupdate()
dw_scad_man_attrezzatura.change_dw_current()

return 0
end function

on w_scad_man_attrezzatura.create
int iCurrent
call super::create
this.dw_scad_man_attrezzatura=create dw_scad_man_attrezzatura
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scad_man_attrezzatura
end on

on w_scad_man_attrezzatura.destroy
call super::destroy
destroy(this.dw_scad_man_attrezzatura)
end on

event pc_setwindow;call super::pc_setwindow;is_cod_attrezzatura = s_cs_xx.parametri.parametro_s_1

dw_scad_man_attrezzatura.set_dw_options(sqlca, &
                        		         pcca.null_object, &
                              		   c_nonew + &
		                                 c_nomodify + &
   		                              c_nodelete + &
         		                        c_noenablenewonopen + &
               		                  c_noenablemodifyonopen + &
                     		            c_scrollparent + &
													c_disablecc, &
													c_noresizedw + &
                     		            c_nohighlightselected + &
                     		            c_nocursorrowfocusrect + &
                     		            c_nocursorrowpointer)
													
dw_scad_man_attrezzatura.object.datawindow.print.preview = 'Yes'
dw_scad_man_attrezzatura.object.datawindow.print.preview.rulers = 'Yes'

wf_report()
end event

type dw_scad_man_attrezzatura from uo_cs_xx_dw within w_scad_man_attrezzatura
integer x = 23
integer y = 20
integer width = 3817
integer height = 2100
integer taborder = 10
string dataobject = "d_scad_man_attrezzatura"
boolean vscrollbar = true
end type

