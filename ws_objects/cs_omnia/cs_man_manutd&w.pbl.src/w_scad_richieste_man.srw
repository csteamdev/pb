﻿$PBExportHeader$w_scad_richieste_man.srw
$PBExportComments$Finestra Visualizzazione Scadenziario Richieste Manutenzioni
forward
global type w_scad_richieste_man from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_scad_richieste_man
end type
type cb_cerca from commandbutton within w_scad_richieste_man
end type
type dw_scadenze_richieste from uo_cs_xx_dw within w_scad_richieste_man
end type
type st_1 from statictext within w_scad_richieste_man
end type
type em_1 from editmask within w_scad_richieste_man
end type
type st_2 from statictext within w_scad_richieste_man
end type
type em_2 from editmask within w_scad_richieste_man
end type
type cb_1 from commandbutton within w_scad_richieste_man
end type
end forward

global type w_scad_richieste_man from w_cs_xx_principale
integer width = 2871
integer height = 1224
string title = "Scadenzario Richieste"
boolean maxbox = false
boolean resizable = false
cb_annulla cb_annulla
cb_cerca cb_cerca
dw_scadenze_richieste dw_scadenze_richieste
st_1 st_1
em_1 em_1
st_2 st_2
em_2 em_2
cb_1 cb_1
end type
global w_scad_richieste_man w_scad_richieste_man

event pc_setwindow;call super::pc_setwindow;em_1.text = string(today(), "dd/mm/yyyy")

em_2.text   = string(relativedate(today(), 30), "dd/mm/yyyy")

iuo_dw_main = dw_scadenze_richieste

dw_scadenze_richieste.change_dw_current()

dw_scadenze_richieste.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew+c_nodelete+c_nomodify,c_default)
end event

on w_scad_richieste_man.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_cerca=create cb_cerca
this.dw_scadenze_richieste=create dw_scadenze_richieste
this.st_1=create st_1
this.em_1=create em_1
this.st_2=create st_2
this.em_2=create em_2
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_cerca
this.Control[iCurrent+3]=this.dw_scadenze_richieste
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.em_1
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.em_2
this.Control[iCurrent+8]=this.cb_1
end on

on w_scad_richieste_man.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_cerca)
destroy(this.dw_scadenze_richieste)
destroy(this.st_1)
destroy(this.em_1)
destroy(this.st_2)
destroy(this.em_2)
destroy(this.cb_1)
end on

type cb_annulla from commandbutton within w_scad_richieste_man
integer x = 1646
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;em_1.text = ""

em_2.text = ""
end event

type cb_cerca from commandbutton within w_scad_richieste_man
integer x = 2057
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_scadenze_richieste.change_dw_current()

parent.postevent("pc_retrieve")
end event

type dw_scadenze_richieste from uo_cs_xx_dw within w_scad_richieste_man
integer x = 23
integer y = 20
integer width = 2811
integer height = 1000
integer taborder = 10
string dataobject = "d_scadenze_richieste"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ldt_data_inizio, ldt_data_fine

ldt_data_inizio = datetime(date(em_1.text))
ldt_data_fine = datetime(date(em_2.text))
l_Error = Retrieve(s_cs_xx.cod_azienda, "M", ldt_data_inizio, ldt_data_fine)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type st_1 from statictext within w_scad_richieste_man
integer x = 23
integer y = 1052
integer width = 293
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Inizio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_1 from editmask within w_scad_richieste_man
integer x = 320
integer y = 1040
integer width = 434
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "01/01/1900~~31/12/2999"
end type

type st_2 from statictext within w_scad_richieste_man
integer x = 800
integer y = 1052
integer width = 251
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Fine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_2 from editmask within w_scad_richieste_man
integer x = 1074
integer y = 1040
integer width = 434
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "01/01/1900~~31/12/2999"
end type

type cb_1 from commandbutton within w_scad_richieste_man
integer x = 2469
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Manut."
end type

event clicked;long ll_row


ll_row = dw_scadenze_richieste.getrow()

if isnull(ll_row) or ll_row = 0 then
	return -1
end if

s_cs_xx.parametri.parametro_s_1 = dw_scadenze_richieste.getitemstring(ll_row,"cod_tipo_manutenzione")
s_cs_xx.parametri.parametro_s_2 = dw_scadenze_richieste.getitemstring(ll_row,"cod_attrezzatura")
s_cs_xx.parametri.parametro_s_3 = dw_scadenze_richieste.getitemstring(ll_row,"cod_operaio")
s_cs_xx.parametri.parametro_d_1 = dw_scadenze_richieste.getitemnumber(ll_row,"anno_reg_richiesta")
s_cs_xx.parametri.parametro_d_2 = dw_scadenze_richieste.getitemnumber(ll_row,"num_reg_richiesta")
setnull(s_cs_xx.parametri.parametro_d_3)
setnull(s_cs_xx.parametri.parametro_d_4)

window_open(w_conferma_manutenzione, 0)
end event

