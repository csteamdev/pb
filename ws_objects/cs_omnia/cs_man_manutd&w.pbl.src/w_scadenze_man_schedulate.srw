﻿$PBExportHeader$w_scadenze_man_schedulate.srw
$PBExportComments$Finestra Scadenzario Manutenzioni & Tarature
forward
global type w_scadenze_man_schedulate from w_cs_xx_principale
end type
type cb_aggiorna from commandbutton within w_scadenze_man_schedulate
end type
type ddlb_dati from dropdownlistbox within w_scadenze_man_schedulate
end type
type st_4 from statictext within w_scadenze_man_schedulate
end type
type em_data_fine from editmask within w_scadenze_man_schedulate
end type
type st_3 from statictext within w_scadenze_man_schedulate
end type
type st_2 from statictext within w_scadenze_man_schedulate
end type
type em_data_inizio from editmask within w_scadenze_man_schedulate
end type
type dw_scadenze_manutenzioni from uo_cs_xx_dw within w_scadenze_man_schedulate
end type
end forward

global type w_scadenze_man_schedulate from w_cs_xx_principale
integer width = 4050
integer height = 2140
string title = "Scadenzario Manutenzioni Schedulate"
cb_aggiorna cb_aggiorna
ddlb_dati ddlb_dati
st_4 st_4
em_data_fine em_data_fine
st_3 st_3
st_2 st_2
em_data_inizio em_data_inizio
dw_scadenze_manutenzioni dw_scadenze_manutenzioni
end type
global w_scadenze_man_schedulate w_scadenze_man_schedulate

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();datetime ldt_da_data, ldt_a_data,ldt_data_giorno,ldt_giorno_corrente
string   ls_cod_attrezzatura,ls_cod_tipo_manutenzione,ls_cod_operaio,ls_flag_tipo_intervento,ls_des_tipo_manutenzione, &
			ls_des_attrezzatura,ls_des_operaio,ls_cognome,ls_nome
long     ll_anno_registrazione,ll_num_registrazione,ll_riga

dw_scadenze_manutenzioni.reset()

choose case ddlb_dati.text
	case "Manutenzioni"
		ls_flag_tipo_intervento = "M"
	case "Tarature"
		ls_flag_tipo_intervento = "T"
	case "Tutti"
		ls_flag_tipo_intervento = "%"
end choose

ldt_a_data = datetime (date(em_data_fine.text),00:00:00)
ldt_da_data = datetime (date(em_data_inizio.text),00:00:00)

ldt_giorno_corrente = ldt_da_data

do while ldt_giorno_corrente <= ldt_a_data 
	
	declare  r_det_cal_progr_manut cursor for
	select   anno_registrazione,
				num_registrazione
	from     det_cal_progr_manut
	where    cod_azienda=:s_cs_xx.cod_azienda
	and      data_giorno = :ldt_giorno_corrente
	and      (flag_stato_manutenzione = 'A' or flag_stato_manutenzione = 'S')
	group by anno_registrazione,num_registrazione
	order by anno_registrazione,num_registrazione;
	
	open r_det_cal_progr_manut;
	
	do while 1=1
		fetch r_det_cal_progr_manut
		into  :ll_anno_registrazione,
				:ll_num_registrazione;
				
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_cal_progr_manut;
			return -1
		end if
			
		if sqlca.sqlcode = 100 then exit
		
		select cod_attrezzatura,
				 cod_tipo_manutenzione,
				 cod_operaio
		into   :ls_cod_attrezzatura,
				 :ls_cod_tipo_manutenzione,
				 :ls_cod_operaio
		from   programmi_manutenzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione =:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    flag_tipo_intervento like :ls_flag_tipo_intervento;
		
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_cal_progr_manut;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then continue
		
		select des_tipo_manutenzione
		into   :ls_des_tipo_manutenzione
		from   tab_tipi_manutenzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_attrezzatura=:ls_cod_attrezzatura
		and    cod_tipo_manutenzione=:ls_cod_tipo_manutenzione;
		
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_cal_progr_manut;
			return -1
		end if
		
		select descrizione
		into   :ls_des_attrezzatura
		from   anag_attrezzature
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_attrezzatura=:ls_cod_attrezzatura;
		
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_cal_progr_manut;
			return -1
		end if
		
		select cognome,
				 nome
		into   :ls_cognome,
				 :ls_nome
		from   anag_operai
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_operaio=:ls_cod_operaio;
		
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_cal_progr_manut;
			return -1
		end if
	
		ls_des_operaio = ls_cognome + " " + ls_nome
		
		ll_riga = dw_scadenze_manutenzioni.insertrow(0)
		dw_scadenze_manutenzioni.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
		dw_scadenze_manutenzioni.setitem(ll_riga, "des_attrezzatura", ls_des_attrezzatura)
		dw_scadenze_manutenzioni.setitem(ll_riga, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
		dw_scadenze_manutenzioni.setitem(ll_riga, "des_tipo_manutenzione", ls_des_tipo_manutenzione)	
		dw_scadenze_manutenzioni.setitem(ll_riga, "cod_operaio", ls_cod_operaio)
		dw_scadenze_manutenzioni.setitem(ll_riga, "des_operaio", ls_des_operaio)
		dw_scadenze_manutenzioni.setitem(ll_riga, "data_giorno", ldt_giorno_corrente)
		dw_scadenze_manutenzioni.setitem(ll_riga, "anno_registrazione", ll_anno_registrazione)
		dw_scadenze_manutenzioni.setitem(ll_riga, "num_registrazione", ll_num_registrazione)
		

		ls_cognome="" 
		ls_nome=""
	loop
	
	close r_det_cal_progr_manut;
	
	ldt_giorno_corrente = datetime(relativedate(date(ldt_giorno_corrente),1),00:00:00)
	
loop

dw_scadenze_manutenzioni.resetupdate()

return 0
end function

on w_scadenze_man_schedulate.create
int iCurrent
call super::create
this.cb_aggiorna=create cb_aggiorna
this.ddlb_dati=create ddlb_dati
this.st_4=create st_4
this.em_data_fine=create em_data_fine
this.st_3=create st_3
this.st_2=create st_2
this.em_data_inizio=create em_data_inizio
this.dw_scadenze_manutenzioni=create dw_scadenze_manutenzioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_aggiorna
this.Control[iCurrent+2]=this.ddlb_dati
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.em_data_fine
this.Control[iCurrent+5]=this.st_3
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.em_data_inizio
this.Control[iCurrent+8]=this.dw_scadenze_manutenzioni
end on

on w_scadenze_man_schedulate.destroy
call super::destroy
destroy(this.cb_aggiorna)
destroy(this.ddlb_dati)
destroy(this.st_4)
destroy(this.em_data_fine)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.em_data_inizio)
destroy(this.dw_scadenze_manutenzioni)
end on

event type long pc_setwindow(unsignedlong wparam, long lparam);call super::pc_setwindow;string ls_modify,ls_path_logo

dw_scadenze_manutenzioni.set_dw_options(sqlca, &
                                  pcca.null_object, &
											 c_nonew + &
											 c_nomodify + &
											 c_noretrieveonopen + &
											 c_nodelete + &
											 c_disableCC, &
											 c_noresizedw + &
                                  c_nocursorrowpointer + &
                                  c_nocursorrowfocusrect )
											 
									 
											 										 

em_data_fine.text = string(relativedate(today(),5))
em_data_inizio.text = string(relativedate(today(),0))
ddlb_dati.selectitem("Tutti", 1)

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';


if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)


ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_scadenze_manutenzioni.modify(ls_modify)

return 0
end event

type cb_aggiorna from commandbutton within w_scadenze_man_schedulate
integer x = 2491
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;dw_scadenze_manutenzioni.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type ddlb_dati from dropdownlistbox within w_scadenze_man_schedulate
integer x = 1920
integer y = 40
integer width = 503
integer height = 320
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
string item[] = {"Manutenzioni","Tarature","Tutti"}
end type

type st_4 from statictext within w_scadenze_man_schedulate
integer x = 1783
integer y = 40
integer width = 123
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Dati:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_fine from editmask within w_scadenze_man_schedulate
integer x = 1349
integer y = 40
integer width = 389
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "d"
string minmax = "01/01/1900~~31/12/2999"
end type

type st_3 from statictext within w_scadenze_man_schedulate
integer x = 914
integer y = 40
integer width = 411
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "A Data giorno:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_scadenze_man_schedulate
integer x = 23
integer y = 40
integer width = 434
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Da Data giorno:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_inizio from editmask within w_scadenze_man_schedulate
integer x = 480
integer y = 40
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "4"
string minmax = "01/01/1900~~31/12/2999"
end type

type dw_scadenze_manutenzioni from uo_cs_xx_dw within w_scadenze_man_schedulate
event ue_post_retrieve ( )
integer x = 23
integer y = 140
integer width = 3977
integer height = 1880
integer taborder = 10
string dataobject = "d_scadenze_man_schedulate"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event doubleclicked;call super::doubleclicked;

s_cs_xx.parametri.parametro_i_1 = getitemnumber(row,"anno_registrazione")
s_cs_xx.parametri.parametro_ul_1 = getitemnumber(row,"num_registrazione")

window_open(w_scelta_operatore,0)

dw_scadenze_manutenzioni.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

