﻿$PBExportHeader$w_scadenze_manutenzioni.srw
$PBExportComments$Finestra Scadenzario Manutenzioni & Tarature
forward
global type w_scadenze_manutenzioni from w_cs_xx_principale
end type
type cb_stampa_2 from commandbutton within w_scadenze_manutenzioni
end type
type cb_aggiorna_1 from commandbutton within w_scadenze_manutenzioni
end type
type st_elemento_1 from statictext within w_scadenze_manutenzioni
end type
type cb_aggiorna_2 from commandbutton within w_scadenze_manutenzioni
end type
type st_elemento from statictext within w_scadenze_manutenzioni
end type
type dw_sel_scad_manut_tempo from datawindow within w_scadenze_manutenzioni
end type
type cb_esporta_2 from commandbutton within w_scadenze_manutenzioni
end type
type cb_esporta_1 from commandbutton within w_scadenze_manutenzioni
end type
type cb_stampa_1 from commandbutton within w_scadenze_manutenzioni
end type
type dw_sel_scad_man from uo_cs_xx_dw within w_scadenze_manutenzioni
end type
type dw_folder from u_folder within w_scadenze_manutenzioni
end type
type dw_scadenze_manutenzioni_2 from datawindow within w_scadenze_manutenzioni
end type
type dw_scadenze_manutenzioni from datawindow within w_scadenze_manutenzioni
end type
end forward

global type w_scadenze_manutenzioni from w_cs_xx_principale
integer width = 3598
integer height = 2056
string title = "Scadenzario Manutenzioni & Tarature"
cb_stampa_2 cb_stampa_2
cb_aggiorna_1 cb_aggiorna_1
st_elemento_1 st_elemento_1
cb_aggiorna_2 cb_aggiorna_2
st_elemento st_elemento
dw_sel_scad_manut_tempo dw_sel_scad_manut_tempo
cb_esporta_2 cb_esporta_2
cb_esporta_1 cb_esporta_1
cb_stampa_1 cb_stampa_1
dw_sel_scad_man dw_sel_scad_man
dw_folder dw_folder
dw_scadenze_manutenzioni_2 dw_scadenze_manutenzioni_2
dw_scadenze_manutenzioni dw_scadenze_manutenzioni
end type
global w_scadenze_manutenzioni w_scadenze_manutenzioni

type variables
string is_flag_manutenzione="%",  is_flag_manutenzione_2="%", is_reparto
datastore ids_manutenzioni
long il_datastore_carico = 0
end variables

forward prototypes
public subroutine wf_colore ()
public function integer wf_report ()
public subroutine wf_report_tempo ()
end prototypes

public subroutine wf_colore ();dw_scadenze_manutenzioni.Modify("data_registrazione.color='0~tif(data_registrazione > datetime(today(),00:00:00),0,255)'")
end subroutine

public function integer wf_report ();long     ll_riga, ll_anno_registrazione, ll_num_registrazione, ll_count

datetime	ldt_scadenza, ldt_da, ldt_a

string   ls_reparto, ls_tipo, ls_cod_att, ls_des_att, ls_cod_tipo, ls_des_tipo, ls_cod_operaio, ls_des_risorsa, &
			ls_cognome, ls_nome, ls_sql, ls_attr_da, ls_attr_a, ls_categoria, ls_area, ls_tipo_manut, ls_note, ls_cod_operaio_sel, &
			ls_cod_cat_risorse_esterne_sel, ls_cod_risorsa_esterna_sel, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, &
			ls_null, ls_flag_tipo_report, ls_elenco_ricambi, ls_cod_ricambio, ls_des_ricambio, ls_cod_fornitore, ls_rag_soc_1, &
			ls_path_logo, ls_modify, ls_divisione

// dichiarazione cursore per lettura ricambi
declare cu_ricambi cursor for
select  cod_prodotto, des_prodotto
from    manutenzioni_ricambi
where   cod_azienda = :s_cs_xx.cod_azienda and
		  anno_registrazione = :ll_anno_registrazione and
		  num_registrazione = :ll_num_registrazione;


setnull(ls_null)

dw_sel_scad_man.accepttext()

ls_flag_tipo_report = dw_sel_scad_man.getitemstring(1,"flag_tipo_report")

if ls_flag_tipo_report = "S" then
	dw_scadenze_manutenzioni.dataobject = 'd_scadenze_manutenzioni'
else
	dw_scadenze_manutenzioni.dataobject = 'd_scadenze_manutenzioni_tipo_2'
end if

dw_scadenze_manutenzioni.reset()

wf_colore()

ls_reparto = dw_sel_scad_man.getitemstring(1,"cod_reparto")

ls_tipo = dw_sel_scad_man.getitemstring(1,"tipo_dati")

if ls_tipo = "%" then
	setnull(ls_tipo)
end if

ldt_da = dw_sel_scad_man.getitemdatetime(1,"data_da")

ldt_a = dw_sel_scad_man.getitemdatetime(1,"data_a")

ls_attr_da = dw_sel_scad_man.getitemstring(1,"cod_attrezzatura_inizio")

ls_attr_a = dw_sel_scad_man.getitemstring(1,"cod_attrezzatura_fine")

ls_tipo_manut = dw_sel_scad_man.getitemstring(1,"cod_tipo_manutenzione")

ls_categoria = dw_sel_scad_man.getitemstring(1,"cod_cat_attrezzatura")

ls_area = dw_sel_scad_man.getitemstring(1,"cod_area_aziendale")

ls_divisione = dw_sel_scad_man.getitemstring(1,"cod_divisione")

ls_cod_operaio_sel = dw_sel_scad_man.getitemstring( 1, "cod_operaio")

if ls_cod_operaio_sel = "" then setnull(ls_cod_operaio_sel)

ls_cod_cat_risorse_esterne_sel = dw_sel_scad_man.getitemstring( 1, "cod_cat_risorse_esterne")

if ls_cod_cat_risorse_esterne_sel = "" then setnull(ls_cod_cat_risorse_esterne_sel)

ls_cod_risorsa_esterna_sel = dw_sel_scad_man.getitemstring( 1, "cod_risorsa_esterna")

if ls_cod_risorsa_esterna_sel = "" then setnull(ls_cod_risorsa_esterna_sel)


// modifiche Michela 29/03/2002 :aggiungo alla select anche l'anno e il num di registrazione

ls_sql = &
"select  data_registrazione, " + &
"		   cod_attrezzatura, " + &
"		   cod_tipo_manutenzione, " + &
"		   cod_operaio, " + &
"		   cod_cat_risorse_esterne, " + &
"		   cod_risorsa_esterna, " + &
"			anno_registrazione, " + &
"			num_registrazione, " + &
"			note_idl " + &
"from   	manutenzioni " + &
"where  	manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"			flag_eseguito = 'N' "

//fine modifiche

if not isnull(ldt_da) then
	ls_sql += " and data_registrazione >= '" + string(ldt_da,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_a) then
	ls_sql += " and data_registrazione <= '" + string(ldt_a,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ls_tipo) then
	ls_sql += " and flag_tipo_intervento = '" + ls_tipo + "' "
end if

if not isnull(ls_reparto) then
	ls_sql += " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_reparto = '" + ls_reparto + "' ) "
end if

if not isnull(ls_attr_da) then
	ls_sql += " and cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_sql += " and cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_tipo_manut) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_tipo_manut + "' "
end if

if not isnull(ls_categoria) then
	ls_sql += " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_attrezzature = '" + ls_categoria + "' ) "	
end if

if not isnull(ls_area) then
	ls_sql += " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + ls_area + "' ) "	
end if

if not isnull(ls_divisione) then
	ls_sql += " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "') ) "	
end if

if not isnull(ls_cod_operaio_sel) then
	ls_sql = ls_sql + " and cod_operaio = '" + ls_cod_operaio_sel + "' "
end if

if not isnull(ls_cod_cat_risorse_esterne_sel) then
	ls_sql = ls_sql + " and cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne_sel + "' "
end if

if not isnull(ls_cod_risorsa_esterna_sel) then
	ls_sql = ls_sql + " and cod_risorsa_esterna = '" + ls_cod_risorsa_esterna_sel + "' "
end if

ls_sql += " order by data_registrazione ASC, cod_attrezzatura ASC, cod_tipo_manutenzione ASC, cod_operaio ASC "

declare scad_man dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic scad_man;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore scad_man: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch scad_man
	into  :ldt_scadenza,
			:ls_cod_att,			
			:ls_cod_tipo,			
			:ls_cod_operaio,
			:ls_cod_cat_risorse_esterne,
			:ls_cod_risorsa_esterna,
			:ll_anno_registrazione,
			:ll_num_registrazione,
			:ls_note;
			
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore scad_man: " + sqlca.sqlerrtext)
		
		close scad_man;
		
		return -1
		
	elseif sqlca.sqlcode = 100 then
		
		close scad_man;
		
		exit
		
	end if
	
	st_elemento_1.text = string(ll_anno_registrazione) + " / " + string(ll_num_registrazione)
	
	select descrizione
	into   :ls_des_att
	from   anag_attrezzature 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_att;
	
	
	select des_tipo_manutenzione
	into   :ls_des_tipo
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_att and
			 cod_tipo_manutenzione = :ls_cod_tipo;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select di tab_tipi_manutenzione: " + sqlca.sqlerrtext)
		close scad_man;
		return -1
	end if
	
	select cognome,
			 nome
	into   :ls_cognome,
			 :ls_nome
	from   anag_operai
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_operaio = :ls_cod_operaio;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select di anag_operai: " + sqlca.sqlerrtext)
		close scad_man;
		return -1
	end if
	
	select descrizione
	into   :ls_des_risorsa
	from   anag_risorse_esterne
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
			 cod_risorsa_esterna = :ls_cod_risorsa_esterna;	
	
	if ls_cod_operaio = "" then setnull(ls_cod_operaio)
	
	if ls_cod_cat_risorse_esterne = "" then setnull(ls_cod_cat_risorse_esterne)
	
	if ls_cod_risorsa_esterna = "" then setnull(ls_cod_risorsa_esterna)
	
	// report di tipo analitico quindi aggiungo istruzioni di lavoro e ricambi
	if ls_flag_tipo_report = "A" then
		open cu_ricambi;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_ricambi~r~n"+sqlca.sqlerrtext)
			close scad_man;
			return -1
		end if
		
		ls_elenco_ricambi = ""
		
		do while true
			fetch cu_ricambi into :ls_cod_ricambio, :ls_des_ricambio;
			
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_ricambi~r~n"+sqlca.sqlerrtext)
				close scad_man;
				close cu_ricambi;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			if len(ls_elenco_ricambi) > 0 then
				ls_elenco_ricambi += "~r~n"
			end if
			
			ls_elenco_ricambi += ls_cod_ricambio + " " + ls_des_ricambio
			
			select cod_fornitore
			into   :ls_cod_fornitore
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :ls_cod_ricambio;
					 
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("OMNIA","Errore in ricerca fornitore preferenziale in anagrafica prodotti~r~n"+sqlca.sqlerrtext)
				close scad_man;
				close cu_ricambi;
				return -1
			end if
			
			if not isnull(ls_cod_fornitore) then
				select rag_soc_1
				into   :ls_rag_soc_1
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_fornitore = :ls_cod_fornitore;
						 
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("OMNIA","Errore in ricerca ragione sociale fornitore preferenziale preferenziale in anagrafica fornitori~r~n"+sqlca.sqlerrtext)
					close scad_man;
					close cu_ricambi;
					return -1
				end if
				
				ls_elenco_ricambi += "(" + ls_cod_fornitore + " " + ls_rag_soc_1 + ")"
				
			end if
				
		loop
		
		close cu_ricambi;

	end if
	
	ll_riga = dw_scadenze_manutenzioni.insertrow(0)

	dw_scadenze_manutenzioni.setitem( ll_riga, "data_registrazione", ldt_scadenza)
	
	dw_scadenze_manutenzioni.setitem( ll_riga, "cod_attrezzatura", ls_cod_att)
	
	dw_scadenze_manutenzioni.setitem( ll_riga, "descrizione", ls_des_att)
	
	dw_scadenze_manutenzioni.setitem( ll_riga, "cod_tipo_manutenzione", ls_cod_tipo)
	
	dw_scadenze_manutenzioni.setitem( ll_riga, "des_tipo_manutenzione", ls_des_tipo)

	dw_scadenze_manutenzioni.setitem( ll_riga, "anno_registrazione", ll_anno_registrazione)
	
	dw_scadenze_manutenzioni.setitem( ll_riga, "num_registrazione", ll_num_registrazione)
	
	dw_scadenze_manutenzioni.setitem( ll_riga, "note_idl", ls_note)
	
	if ls_flag_tipo_report = "A" then dw_scadenze_manutenzioni.setitem( ll_riga, "elenco_ricambi", ls_elenco_ricambi)					

	
	dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "3")			
	
	ll_count = 0
	
	if not isnull(ls_cod_operaio)  then
		
		dw_scadenze_manutenzioni.setitem( ll_riga, "cod_operaio", ls_cod_operaio)
			
		dw_scadenze_manutenzioni.setitem( ll_riga, "des_operaio", ls_cognome + " " + ls_nome)
			
		dw_scadenze_manutenzioni.setitem( ll_riga, "cod_cat_risorse_esterne", ls_null)
				
		dw_scadenze_manutenzioni.setitem( ll_riga, "cod_risorsa_esterna", ls_null)	
			
		dw_scadenze_manutenzioni.setitem( ll_riga, "des_risorsa", ls_null)	
			
		dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "1")		
		
		ll_count = ll_count + 1
		
	end if

	if not isnull(ls_cod_cat_risorse_esterne) and not isnull(ls_cod_risorsa_esterna) then
		
		if ll_count > 0 then
			
			if ls_flag_tipo_report = "A" then
				dw_scadenze_manutenzioni.setitem( ll_riga, "flag_linea", "N")
			end if
			
			ll_riga = dw_scadenze_manutenzioni.insertrow(0)
		
			dw_scadenze_manutenzioni.setitem( ll_riga, "data_registrazione", ldt_scadenza)
			
			// report sintetico
			if ls_flag_tipo_report = "S" then
				dw_scadenze_manutenzioni.setitem( ll_riga, "cod_attrezzatura", ls_cod_att)
			
				dw_scadenze_manutenzioni.setitem( ll_riga, "descrizione", ls_des_att)
			
				dw_scadenze_manutenzioni.setitem( ll_riga, "cod_tipo_manutenzione", ls_cod_tipo)
			
				dw_scadenze_manutenzioni.setitem( ll_riga, "des_tipo_manutenzione", ls_des_tipo)
		
				dw_scadenze_manutenzioni.setitem( ll_riga, "note_idl", ls_note)
				
			else
				
				dw_scadenze_manutenzioni.setitem( ll_riga, "cod_attrezzatura", ls_null)
			
				dw_scadenze_manutenzioni.setitem( ll_riga, "descrizione", ls_null)
			
				dw_scadenze_manutenzioni.setitem( ll_riga, "cod_tipo_manutenzione", ls_null)
			
				dw_scadenze_manutenzioni.setitem( ll_riga, "des_tipo_manutenzione", ls_null)
		
				dw_scadenze_manutenzioni.setitem( ll_riga, "note_idl", ls_null)
				
			end if
			
			dw_scadenze_manutenzioni.setitem( ll_riga, "anno_registrazione", ll_anno_registrazione)
			
			dw_scadenze_manutenzioni.setitem( ll_riga, "num_registrazione", ll_num_registrazione)	
			
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_operaio", ls_null)
				
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_operaio", ls_null)				
		
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_cat_risorse_esterne", ls_cod_cat_risorse_esterne)
					
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_risorsa_esterna", ls_cod_risorsa_esterna)	
					
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_risorsa", ls_des_risorsa)
					
			dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "4")	
			
		else
			
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_operaio", ls_null)
				
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_operaio", ls_null)				
		
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_cat_risorse_esterne", ls_cod_cat_risorse_esterne)
					
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_risorsa_esterna", ls_cod_risorsa_esterna)	
					
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_risorsa", ls_des_risorsa)
					
			dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "2")			
			
		end if		
			
	end if
		
	if isnull(ls_cod_operaio) and isnull(ls_cod_cat_risorse_esterne) and isnull(ls_cod_risorsa_esterna) then
		
		dw_scadenze_manutenzioni.setitem(ll_riga,"cod_operaio", ls_null)
			
		dw_scadenze_manutenzioni.setitem(ll_riga,"des_operaio", ls_null)	
		
		dw_scadenze_manutenzioni.setitem( ll_riga, "cod_cat_risorse_esterne", ls_null)
			
		dw_scadenze_manutenzioni.setitem( ll_riga, "cod_risorsa_esterna", ls_null)	
		
		dw_scadenze_manutenzioni.setitem( ll_riga, "des_risorsa", ls_null)
			
		dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "3")					
		
	end if
	
loop


select stringa
into   :ls_path_logo
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)

if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

dw_scadenze_manutenzioni.modify(ls_modify)

st_elemento_1.text = ""

dw_scadenze_manutenzioni.resetupdate()

dw_folder.fu_selecttab(2)

return 0
end function

public subroutine wf_report_tempo ();long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i_report, ll_prog_uso_attrezzatura, ll_frequenza
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cod_operaio, ls_des_attrezzatura, ls_des_tipo_manutenzione, ls_des_operaio, &
		 ls_flag_tipo_tempo, ls_periodicita,ls_path_logo, ls_modify
datetime ldt_data_registrazione, ldt_data_restituzione, ldt_data_max, ldt_data_comodo
decimal ld_contatore, ld_somma_contatore, ll_tot_ore


il_datastore_carico = 0

dw_sel_scad_manut_tempo.accepttext()

is_flag_manutenzione_2 = dw_sel_scad_manut_tempo.getitemstring(1,"tipo_dati")

dw_scadenze_manutenzioni_2.Reset()

declare  cu_manutenzioni cursor for
select   a.anno_registrazione, 
		   a.num_registrazione, 
		   a.data_registrazione,
		   a.cod_attrezzatura, 
		   a.cod_tipo_manutenzione,
		   a.cod_operaio,
		   b.periodicita,
		   b.frequenza
from     manutenzioni a, programmi_manutenzione b
where    a.cod_azienda = b.cod_azienda and 
         a.anno_reg_programma = b.anno_registrazione and 
		   a.num_reg_programma = b.num_registrazione and 
		   a.cod_azienda = :s_cs_xx.cod_azienda and 
		   a.flag_ordinario = 'S' and 
		   a.flag_eseguito = 'N'	and 
		   (b.periodicita = 'M' or b.periodicita = 'O') and 
		   a.flag_tipo_intervento like :is_flag_manutenzione_2
order by a.cod_attrezzatura, a.data_registrazione;

ll_i = 1

open cu_manutenzioni;

ids_manutenzioni = create datastore
ids_manutenzioni.DataObject = 'd_scad_man_appoggio'
ids_manutenzioni.SetTransObject (sqlca)
dw_scadenze_manutenzioni.setredraw(false)

do while 1 = 1
	
	fetch cu_manutenzioni into :ll_anno_registrazione, 
										:ll_num_registrazione, 
										:ldt_data_registrazione, 
										:ls_cod_attrezzatura, 
										:ls_cod_tipo_manutenzione, 
										:ls_cod_operaio, 
										:ls_periodicita, 
										:ll_frequenza;
										
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella manutenzioni")
		return
	end if
	
	if sqlca.sqlcode = 0 then
		
		st_elemento.text = string(ll_anno_registrazione) + " / " + string(ll_num_registrazione)
		
		ids_manutenzioni.insertrow(0)
		ids_manutenzioni.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
		ids_manutenzioni.setitem(ll_i, "num_registrazione", ll_num_registrazione)		
		ids_manutenzioni.setitem(ll_i, "cod_attrezzatura", ls_cod_attrezzatura)		
		
		select descrizione
		into   :ls_des_attrezzatura
		from   anag_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and 
		       cod_attrezzatura = :ls_cod_attrezzatura;
				 
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella ANAG_ATTREZZATURE")
			return			
		end if
		
		if sqlca.sqlcode = 100 then ls_des_attrezzatura = ""
		ids_manutenzioni.setitem(ll_i, "des_attrezzatura", ls_des_attrezzatura)		
		ids_manutenzioni.setitem(ll_i, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)		
		
		select des_tipo_manutenzione
		into   :ls_des_tipo_manutenzione
	   from   tab_tipi_manutenzione
		where  cod_azienda = :s_cs_xx.cod_azienda and 
		       cod_attrezzatura = :ls_cod_attrezzatura and 
				 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				 
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella TAB_TIPI_MANUTENZIONE")
			return			
		end if
		
		if sqlca.sqlcode = 100 then ls_des_tipo_manutenzione = ""		
		ids_manutenzioni.setitem(ll_i, "des_tipo_manutenzione", ls_des_tipo_manutenzione)		
		ids_manutenzioni.setitem(ll_i, "cod_operaio", ls_cod_operaio)
		
		select cognome
		into   :ls_des_operaio
		from   anag_operai
		where  cod_azienda = :s_cs_xx.cod_azienda and 
		       cod_operaio = :ls_cod_operaio;
				
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella ANAG_OPERAI")
			return			
		end if
		
		if sqlca.sqlcode = 100 then ls_des_operaio = ""		
		ids_manutenzioni.setitem(ll_i, "des_operaio", ls_des_operaio)
		if ls_periodicita = 'O' then
			ll_tot_ore = ll_frequenza
		elseif ls_periodicita = 'M' then 
			ll_tot_ore = ll_frequenza / 60
		end if	
		ids_manutenzioni.setitem(ll_i, "quan_utilizzo", ll_tot_ore)	
		il_datastore_carico = 1
		ll_i ++
	end if 
loop

close cu_manutenzioni;

ll_i = 1
ll_i_report = 1

if il_datastore_carico = 0 then return

for ll_i = 1 to ids_manutenzioni.RowCount()
	ll_anno_registrazione = ids_manutenzioni.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = ids_manutenzioni.getitemnumber(ll_i, "num_registrazione")
	ls_cod_attrezzatura = ids_manutenzioni.getitemstring(ll_i, "cod_attrezzatura")
	ls_des_attrezzatura = ids_manutenzioni.getitemstring(ll_i, "des_attrezzatura")
	ls_cod_tipo_manutenzione = ids_manutenzioni.getitemstring(ll_i, "cod_tipo_manutenzione")
	ls_des_tipo_manutenzione = ids_manutenzioni.getitemstring(ll_i, "des_tipo_manutenzione")
	ls_cod_operaio = ids_manutenzioni.getitemstring(ll_i, "cod_operaio")
	ls_des_operaio = ids_manutenzioni.getitemstring(ll_i, "des_operaio")	
	ll_tot_ore = ids_manutenzioni.getitemdecimal(ll_i, "quan_utilizzo")
	
	select max(data_registrazione)
	into   :ldt_data_registrazione
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_attrezzatura = :ls_cod_attrezzatura and 
			 flag_eseguito = 'S'	and 
			 flag_ordinario = 'S';
			 
	if sqlca.sqlcode < 0 then 	
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella MANUTENZIONI")
		return
	end if
	
	if isnull(ldt_data_registrazione) then ldt_data_registrazione = datetime(Date("1900/01/01"))
	ld_somma_contatore = 0
	ldt_data_max = datetime(Date("2999/12/31"))
	do while 1 = 1	

		 select max(data_restituzione), 
				  max(prog_uso_attrezzatura)
		 into   :ldt_data_comodo,
				  :ll_prog_uso_attrezzatura
		 from   registro_uso_attrezzature	  
		 where  cod_azienda = :s_cs_xx.cod_azienda and 
		        cod_attrezzatura = :ls_cod_attrezzatura and 
				  data_restituzione > :ldt_data_registrazione and 
				  data_restituzione < :ldt_data_max;
			 
		if sqlca.sqlcode = 100 then exit 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura data_registrazione max dalla tabella REGISTRO_USO_MANUTENZIONI")
			return
		end if

		 select flag_tipo_tempo, 
				  contatore			 
		 into   :ls_flag_tipo_tempo,
				  :ld_contatore		
		 from   registro_uso_attrezzature
		 where  cod_azienda = :s_cs_xx.cod_azienda and 
		        cod_attrezzatura = :ls_cod_attrezzatura and 
				  prog_uso_attrezzatura = :ll_prog_uso_attrezzatura;

		if sqlca.sqlcode = 100 then exit 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella REGISTRO_USO_MANUTENZIONI")
			return
		end if
		
		if sqlca.sqlcode = 0 then
			ldt_data_max = ldt_data_comodo
			if ls_flag_tipo_tempo = "S" then
				ld_somma_contatore = ld_somma_contatore + ld_contatore
			elseif ls_flag_tipo_tempo = "N" then
				ld_somma_contatore = ld_somma_contatore + ld_contatore
				exit
			end if
		end if
	loop

	if ll_tot_ore < ld_somma_contatore then
		dw_scadenze_manutenzioni_2.insertrow(0)
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "cod_attrezzatura", ls_cod_attrezzatura)
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "des_attrezzatura", ls_des_attrezzatura)
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "des_tipo_manutenzione", ls_des_tipo_manutenzione)	
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "cod_operaio", ls_cod_operaio)
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "des_operaio", ls_des_operaio)
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "quan_utilizzo", ld_somma_contatore)	
		dw_scadenze_manutenzioni_2.setitem(ll_i_report, "quan_limite", ll_tot_ore)		
		ll_i_report ++	
	end if	
next

st_elemento.text = ""

destroy ids_manutenzioni

select stringa
into   :ls_path_logo
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)

if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

dw_scadenze_manutenzioni_2.modify(ls_modify)
dw_scadenze_manutenzioni_2.object.datawindow.print.preview = 'Yes'


end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

set_w_options(c_closenosave + c_autoposition)

dw_sel_scad_man.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_sel_scad_man

lw_oggetti[1] = dw_scadenze_manutenzioni
lw_oggetti[2] = cb_stampa_1
lw_oggetti[3] = cb_esporta_1
dw_folder.fu_assigntab(2, "Data", lw_oggetti[])

lw_oggetti[1] = dw_sel_scad_man
//lw_oggetti[2] = cb_attrezzature_ricerca_1
//lw_oggetti[3] = cb_attrezzature_ricerca_2
lw_oggetti[2] = cb_aggiorna_1
lw_oggetti[3] = st_elemento_1
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

lw_oggetti[1] = dw_scadenze_manutenzioni_2
lw_oggetti[2] = dw_sel_scad_manut_tempo
lw_oggetti[3] = cb_aggiorna_2
lw_oggetti[4] = cb_stampa_2
lw_oggetti[5] = st_elemento
lw_oggetti[6] = cb_esporta_2
dw_folder.fu_assigntab(3, "Utilizzo", lw_oggetti[])

dw_folder.fu_foldercreate(3,3)

dw_folder.fu_selecttab(1)

dw_sel_scad_manut_tempo.insertrow(0)

dw_sel_scad_man.setitem(1,"data_a",datetime(relativedate(today(),60),00:00:00))

dw_sel_scad_man.setitem(1,"data_da",datetime(relativedate(today(),-15),00:00:00))

dw_scadenze_manutenzioni.object.datawindow.print.preview = 'Yes'

end event

on w_scadenze_manutenzioni.create
int iCurrent
call super::create
this.cb_stampa_2=create cb_stampa_2
this.cb_aggiorna_1=create cb_aggiorna_1
this.st_elemento_1=create st_elemento_1
this.cb_aggiorna_2=create cb_aggiorna_2
this.st_elemento=create st_elemento
this.dw_sel_scad_manut_tempo=create dw_sel_scad_manut_tempo
this.cb_esporta_2=create cb_esporta_2
this.cb_esporta_1=create cb_esporta_1
this.cb_stampa_1=create cb_stampa_1
this.dw_sel_scad_man=create dw_sel_scad_man
this.dw_folder=create dw_folder
this.dw_scadenze_manutenzioni_2=create dw_scadenze_manutenzioni_2
this.dw_scadenze_manutenzioni=create dw_scadenze_manutenzioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa_2
this.Control[iCurrent+2]=this.cb_aggiorna_1
this.Control[iCurrent+3]=this.st_elemento_1
this.Control[iCurrent+4]=this.cb_aggiorna_2
this.Control[iCurrent+5]=this.st_elemento
this.Control[iCurrent+6]=this.dw_sel_scad_manut_tempo
this.Control[iCurrent+7]=this.cb_esporta_2
this.Control[iCurrent+8]=this.cb_esporta_1
this.Control[iCurrent+9]=this.cb_stampa_1
this.Control[iCurrent+10]=this.dw_sel_scad_man
this.Control[iCurrent+11]=this.dw_folder
this.Control[iCurrent+12]=this.dw_scadenze_manutenzioni_2
this.Control[iCurrent+13]=this.dw_scadenze_manutenzioni
end on

on w_scadenze_manutenzioni.destroy
call super::destroy
destroy(this.cb_stampa_2)
destroy(this.cb_aggiorna_1)
destroy(this.st_elemento_1)
destroy(this.cb_aggiorna_2)
destroy(this.st_elemento)
destroy(this.dw_sel_scad_manut_tempo)
destroy(this.cb_esporta_2)
destroy(this.cb_esporta_1)
destroy(this.cb_stampa_1)
destroy(this.dw_sel_scad_man)
destroy(this.dw_folder)
destroy(this.dw_scadenze_manutenzioni_2)
destroy(this.dw_scadenze_manutenzioni)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_scad_man,&
                 "cod_reparto", &
                 sqlca, &
					  "anag_reparti",&
					  "cod_reparto",&
					  "des_reparto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_scad_man,&
                 "cod_area_aziendale", &
                 sqlca, &
					  "tab_aree_aziendali",&
					  "cod_area_aziendale",&
					  "des_area", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_sel_scad_man,&
                 "cod_cat_attrezzatura", &
                 sqlca, &
					  "tab_cat_attrezzature",&
					  "cod_cat_attrezzature",&
					  "des_cat_attrezzature", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_sel_scad_man,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio"," nome + ' ' + cognome ",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_PO_LoadDDDW_DW (dw_sel_scad_man,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
					  
f_PO_LoadDDDW_DW (dw_sel_scad_man,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")										  

//Donato 23/06/2009 carica comunque le tipologie di manutenzione
//Specifica Metlac "Report_manutenzioni_26/02/2009"
dw_sel_scad_man.postevent("ue_carica_tipologie")
end event

type cb_stampa_2 from commandbutton within w_scadenze_manutenzioni
integer x = 59
integer y = 152
integer width = 389
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA"
end type

event clicked;//long job

dw_scadenze_manutenzioni_2.print(false)
//job = PrintOpen( )
//PrintDataWindow(job, dw_scadenze_manutenzioni_2)
//PrintClose(job)
end event

type cb_aggiorna_1 from commandbutton within w_scadenze_manutenzioni
integer x = 2240
integer y = 1288
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;dw_scadenze_manutenzioni.setredraw(false)
setpointer(hourglass!)
wf_report()
setpointer(arrow!)
dw_scadenze_manutenzioni.setredraw(true)

end event

type st_elemento_1 from statictext within w_scadenze_manutenzioni
integer x = 1371
integer y = 1268
integer width = 823
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_aggiorna_2 from commandbutton within w_scadenze_manutenzioni
integer x = 3127
integer y = 152
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;setredraw(false)

setpointer(hourglass!)

wf_report_tempo()

//parent.triggerevent("pc_retrieve")

dw_scadenze_manutenzioni.setredraw(true)

setpointer(arrow!)

setredraw(true)
		 
end event

type st_elemento from statictext within w_scadenze_manutenzioni
integer x = 1531
integer y = 160
integer width = 1554
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_sel_scad_manut_tempo from datawindow within w_scadenze_manutenzioni
integer x = 869
integer y = 136
integer width = 1422
integer height = 104
integer taborder = 50
string title = "none"
string dataobject = "d_sel_scad_manut"
boolean border = false
boolean livescroll = true
end type

type cb_esporta_2 from commandbutton within w_scadenze_manutenzioni
integer x = 471
integer y = 152
integer width = 389
integer height = 80
integer taborder = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ESPORTA"
end type

event clicked;dw_scadenze_manutenzioni_2.saveas()
end event

type cb_esporta_1 from commandbutton within w_scadenze_manutenzioni
integer x = 471
integer y = 152
integer width = 389
integer height = 80
integer taborder = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ESPORTA"
end type

event clicked;string 	ls_stringa

long 	 	ll_i, ll_pos

boolean  lb_trovato


for ll_i = 1 to dw_scadenze_manutenzioni.rowcount()
	
	ls_stringa = dw_scadenze_manutenzioni.getitemstring(ll_i,"note_idl")
	
	if isnull(ls_stringa) or len(trim(ls_stringa)) < 1 then
		continue
	end if

	ll_pos = 1
	
	lb_trovato = false
	
	do
		
		ll_pos = pos(ls_stringa,char(13),ll_pos)
		
		if ll_pos <> 0 then
			ls_stringa = replace(ls_stringa,ll_pos,1,"")
			lb_trovato = true
		end if
		
	loop while ll_pos <> 0
	
	if lb_trovato then
		dw_scadenze_manutenzioni.setitem(ll_i,"note_idl",ls_stringa)
	end if
	
next

dw_scadenze_manutenzioni.saveas()
end event

type cb_stampa_1 from commandbutton within w_scadenze_manutenzioni
integer x = 59
integer y = 152
integer width = 389
integer height = 80
integer taborder = 50
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA"
end type

event clicked;//long job

dw_scadenze_manutenzioni.print(false)
//job = PrintOpen( )
//PrintDataWindow(job, dw_scadenze_manutenzioni)
//PrintClose(job)
end event

type dw_sel_scad_man from uo_cs_xx_dw within w_scadenze_manutenzioni
event ue_cod_attr ( )
event ue_carica_tipologie ( )
event ue_aree_aziendali ( )
integer x = 55
integer y = 148
integer width = 3456
integer height = 1292
integer taborder = 70
string dataobject = "d_sel_scadenze_manutenzioni"
boolean border = false
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"cod_attrezzatura_inizio")) then
	setitem(getrow(),"cod_attrezzatura_fine",getitemstring(getrow(),"cod_attrezzatura_inizio"))
end if
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"cod_attrezzatura_inizio")

ls_attr_a = getitemstring(getrow(),"cod_attrezzatura_fine")

ls_reparto = getitemstring(getrow(),"cod_reparto")

ls_categoria = getitemstring(getrow(),"cod_cat_attrezzatura")

ls_area = getitemstring(getrow(),"cod_area_aziendale")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_DW (dw_sel_scad_man,"cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "'")						
else
	f_PO_LoadDDDW_DW (dw_sel_scad_man,"cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					
					  	
end if

triggerevent("ue_carica_tipologie")


end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
		
	case "cod_attrezzatura_inizio"
		
		postevent("ue_cod_attr")
		
		postevent("ue_carica_tipologie")
		
	case "cod_attrezzatura_fine","cod_reparto","cod_cat_attrezzatura","cod_area_aziendale"
		
		postevent("ue_carica_tipologie")
		
	case "cod_divisione"
		
		postevent("ue_aree_aziendali")
		
	case "cod_cat_risorse_esterne"
		
		if isnull(i_coltext) or i_coltext = "" then return 0
		
		this.setitem( row, "cod_risorsa_esterna", ls_null)
		
		f_PO_LoadDDDW_DW ( dw_sel_scad_man, &
								 "cod_risorsa_esterna", &
								 sqlca, &
							  	 "anag_risorse_esterne", &
								 "cod_risorsa_esterna", &
								 "descrizione", &
							    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "' ")			
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_scad_man,"cod_attrezzatura_inizio")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_scad_man,"cod_attrezzatura_fine")
end choose
end event

type dw_folder from u_folder within w_scadenze_manutenzioni
integer x = 23
integer y = 20
integer width = 3520
integer height = 1920
integer taborder = 0
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Data"
      SetFocus(dw_scadenze_manutenzioni)
		wf_colore()
		
   CASE "Utilizzo"
      SetFocus(dw_scadenze_manutenzioni_2)
		
END CHOOSE

end event

type dw_scadenze_manutenzioni_2 from datawindow within w_scadenze_manutenzioni
integer x = 46
integer y = 240
integer width = 3474
integer height = 1680
integer taborder = 50
string title = "none"
string dataobject = "d_scadenze_manutenzioni_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_scadenze_manutenzioni from datawindow within w_scadenze_manutenzioni
event ue_cambia_operatore ( )
event ue_cambia_risorsa ( )
event ue_inserisci_operatore ( )
event ue_inserisci_risorsa ( )
integer x = 46
integer y = 240
integer width = 3474
integer height = 1680
integer taborder = 10
string title = "none"
string dataobject = "d_scadenze_manutenzioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_cambia_operatore();long   ll_riga, ll_anno, ll_numero

string ls_cod_operaio, ls_descrizione


ll_riga = s_cs_xx.parametri.parametro_d_1

if ll_riga <= 0 then return

ll_anno = this.getitemnumber( ll_riga, "anno_registrazione")

ll_numero = this.getitemnumber( ll_riga, "num_registrazione")

if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" then
	
	ls_cod_operaio = this.getitemstring( ll_riga, "cod_operaio")
	
	if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" then
		
		update manutenzioni
		set    cod_operaio = :s_cs_xx.parametri.parametro_s_1
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero;
				 
		if sqlca.sqlcode < 0 then
			
			g_mb.messagebox( "OMNIA", "Errore durante la modifica dell'operatore: " + sqlca.sqlerrtext)
			
			g_mb.messagebox( "OMNIA", "Operatore precedente: " + ls_cod_operaio + "~r~nOperatore scelto: " + s_cs_xx.parametri.parametro_s_1)
			
			rollback;
			
			return 
			
		end if

		select cognome + ' ' + nome
		into   :ls_descrizione
		from   anag_operai
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_operaio = :s_cs_xx.parametri.parametro_s_1;
		
		this.setitem( ll_riga, "cod_operaio", s_cs_xx.parametri.parametro_s_1)
		
		this.setitem( ll_riga, "des_operaio", ls_descrizione)	
		
		commit;
		
	end if
	
end if
end event

event ue_cambia_risorsa();long   ll_riga, ll_anno, ll_numero

string ls_cod_operaio, ls_descrizione, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna


ll_riga = s_cs_xx.parametri.parametro_d_1

if ll_riga <= 0 then return

ll_anno = this.getitemnumber( ll_riga, "anno_registrazione")

ll_numero = this.getitemnumber( ll_riga, "num_registrazione")

if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" and not isnull(s_cs_xx.parametri.parametro_s_2) and s_cs_xx.parametri.parametro_s_2 <> ""  then
	
	ls_cod_cat_risorse_esterne = this.getitemstring( ll_riga, "cod_cat_risorse_esterne")

	ls_cod_risorsa_esterna = this.getitemstring( ll_riga, "cod_risorsa_esterna")	
	
	update manutenzioni
	set    cod_cat_risorse_esterne = :s_cs_xx.parametri.parametro_s_1,
	       cod_risorsa_esterna = :s_cs_xx.parametri.parametro_s_2
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero;
				 
	if sqlca.sqlcode < 0 then
			
		g_mb.messagebox( "OMNIA", "Errore durante la modifica della risorsa: " + sqlca.sqlerrtext)
			
		rollback;
			
		return 
			
	end if

	select descrizione
	into   :ls_descrizione
	from   anag_risorse_esterne
	where  cod_azienda = :s_cs_xx.cod_azienda and
          cod_cat_risorse_esterne = :s_cs_xx.parametri.parametro_s_1 and
		    cod_risorsa_esterna = :s_cs_xx.parametri.parametro_s_2;
	
	this.setitem( ll_riga, "cod_cat_risorse_esterne", s_cs_xx.parametri.parametro_s_1)
	
	this.setitem( ll_riga, "cod_risorsa_esterna", s_cs_xx.parametri.parametro_s_2)		
	
	this.setitem( ll_riga, "des_risorsa", ls_descrizione)	
	
	commit;
	
end if
	

end event

event ue_inserisci_operatore();long   ll_riga, ll_anno, ll_numero

string ls_cod_operaio, ls_descrizione


ll_riga = s_cs_xx.parametri.parametro_d_1

if ll_riga <= 0 then return

ll_anno = this.getitemnumber( ll_riga, "anno_registrazione")

ll_numero = this.getitemnumber( ll_riga, "num_registrazione")

if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" then

	update manutenzioni
	set    cod_operaio = :s_cs_xx.parametri.parametro_s_1
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero;

	if sqlca.sqlcode < 0 then
			
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento dell'operatore: " + sqlca.sqlerrtext)
			
		g_mb.messagebox( "OMNIA", "Operatore scelto: " + s_cs_xx.parametri.parametro_s_1)
			
		rollback;
			
		return 
			
	end if

	select cognome + ' ' + nome
	into   :ls_descrizione
	from   anag_operai
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_operaio = :s_cs_xx.parametri.parametro_s_1;
		
	this.setitem( ll_riga, "cod_operaio", s_cs_xx.parametri.parametro_s_1)
		
	this.setitem( ll_riga, "des_operaio", ls_descrizione)	
		
	commit;
		
end if

end event

event ue_inserisci_risorsa();long   ll_riga, ll_anno, ll_numero

string ls_cod_operaio, ls_descrizione


ll_riga = s_cs_xx.parametri.parametro_d_1

if ll_riga <= 0 then return

ll_anno = this.getitemnumber( ll_riga, "anno_registrazione")

ll_numero = this.getitemnumber( ll_riga, "num_registrazione")

if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" and not isnull(s_cs_xx.parametri.parametro_s_2) and s_cs_xx.parametri.parametro_s_2 <> "" then
	
	update manutenzioni
	set    cod_cat_risorse_esterne = :s_cs_xx.parametri.parametro_s_1,
	       cod_risorsa_esterna = :s_cs_xx.parametri.parametro_s_2
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno and
			 num_registrazione = :ll_numero;

	
	if sqlca.sqlcode < 0 then
			
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento della risorsa: " + sqlca.sqlerrtext)
			
		rollback;
			
		return 
			
	end if

	select descrizione
	into   :ls_descrizione
	from   anag_risorse_esterne
	where  cod_azienda = :s_cs_xx.cod_azienda and
          cod_cat_risorse_esterne = :s_cs_xx.parametri.parametro_s_1 and
		    cod_risorsa_esterna = :s_cs_xx.parametri.parametro_s_2;
		
	this.setitem( ll_riga, "cod_cat_risorse_esterne", s_cs_xx.parametri.parametro_s_1)
	
	this.setitem( ll_riga, "cod_risorsa_esterna", s_cs_xx.parametri.parametro_s_2)		
	
	this.setitem( ll_riga, "des_risorsa", ls_descrizione)	
		
	commit;
		
end if

end event

event itemchanged;//decimal ld_null
//string ls_nome, ls_cognome, ls_msg
//long ll_anno_registrazione, ll_num_registrazione
//setnull(ld_null)
//
////if i_extendmode then
//	
//	choose case i_colname
//			
//		case "cod_operaio"
//			
//			setnull(ls_nome)
//			setnull(ls_cognome)
//			
//			select nome,cognome
//			into :ls_nome, :ls_cognome
//			from  anag_operai
//			where cod_azienda = :s_cs_xx.cod_azienda and
//			      cod_operaio = :i_coltext;
//			
//			if sqlca.sqlcode < 0 then
//				messagebox("OMNIA", "Errore nella select dalla tabella operai" + sqlca.sqlerrtext,stopsign!)
//			end if
//				
//			ll_anno_registrazione = getitemdecimal(this.getrow(),"manutenzioni_anno_registrazione")
//			ll_num_registrazione = getitemdecimal(this.getrow(),"manutenzioni_num_registrazione")
//			
//			update manutenzioni
//			set cod_operaio = :i_coltext
//			where manutenzioni.cod_azienda=:s_cs_xx.cod_azienda and
//			      manutenzioni.anno_registrazione=:ll_anno_registrazione and 
//					manutenzioni.num_registrazione=:ll_num_registrazione;
//					
//			if sqlca.sqlcode < 0 then
//				ls_msg = sqlca.sqlerrtext
//				rollback;
//				messagebox("OMNIA", "Errore nella update dalla tabella manutenzioni" + ls_msg,stopsign!)				
//			end if
//			
//			commit;
//
//			this.setitem(this.getrow(),"anag_operai_nome", ls_nome)
//			this.setitem(this.getrow(),"anag_operai_cognome", ls_cognome)					  
//			
//	end choose
//end if
end event

event doubleclicked;string ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna

long   ll_ret

ls_cod_operaio = this.getitemstring( row, "cod_operaio")

ls_cod_cat_risorse_esterne = this.getitemstring( row, "cod_cat_risorse_esterne")

ls_cod_risorsa_esterna = this.getitemstring( row, "cod_risorsa_esterna")

if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" then
	
	s_cs_xx.parametri.parametro_dw_1 = this
	s_cs_xx.parametri.parametro_s_1 = "cod_operaio"
	
	openwithparm(w_operai_ricerca_report,0)
	
	if ( s_cs_xx.parametri.parametro_s_2 = "S" ) then
		
		dw_scadenze_manutenzioni.setredraw(false)
		
		setpointer(hourglass!)
		
		wf_report()
		
		setpointer(arrow!)
		
		dw_scadenze_manutenzioni.setredraw(true)	
		
	else
		wf_colore()
	end if
	
	return 0
	
end if

if not isnull(ls_cod_cat_risorse_esterne) and ls_cod_cat_risorse_esterne <> "" and not isnull(ls_cod_risorsa_esterna) and ls_cod_risorsa_esterna <> "" then

	setnull(s_cs_xx.parametri.parametro_s_1)
	
	setnull(s_cs_xx.parametri.parametro_s_2)	
	
	openwithparm( w_risorse_esterne_ricerca, 0)
	
	s_cs_xx.parametri.parametro_d_1 = row
	
	triggerevent("ue_cambia_risorsa")
	
	return 0	
	
end if

if isnull(ls_cod_operaio) and isnull(ls_cod_cat_risorse_esterne) and isnull(ls_cod_risorsa_esterna) then
	
	ll_ret = g_mb.messagebox( "OMNIA", "Vuoi inserire un nuovo Operatore?", Exclamation!, OKCancel!, 1)

	IF ll_ret = 1 THEN
		
		s_cs_xx.parametri.parametro_dw_1 = this
		s_cs_xx.parametri.parametro_s_1 = "cod_operaio"
		
		openwithparm(w_operai_ricerca_report,0)
		
		if ( s_cs_xx.parametri.parametro_s_2 = "S" ) then
			
			dw_scadenze_manutenzioni.setredraw(false)
			
			setpointer(hourglass!)
			
			wf_report()
			
			setpointer(arrow!)
			
			dw_scadenze_manutenzioni.setredraw(true)	
			
		else
			wf_colore()
		end if
		
		return 0

	ELSE
	
	 	ll_ret = g_mb.messagebox( "OMNIA", "Vuoi inserire una risorsa esterna?", Exclamation!, OKCancel!, 1)

		IF ll_ret = 1 THEN
			
			setnull(s_cs_xx.parametri.parametro_s_1)
			
			setnull(s_cs_xx.parametri.parametro_s_2)			
			
			openwithparm(w_risorse_esterne_ricerca,0)
			
			s_cs_xx.parametri.parametro_d_1 = row
			
			triggerevent("ue_cambia_risorsa")

			dw_scadenze_manutenzioni.setredraw(false)
			
			setpointer(hourglass!)
			
			wf_report()
			
			setpointer(arrow!)
			
			dw_scadenze_manutenzioni.setredraw(true)	
			
			return 0
	
		END IF
	
	END IF
	
end if
end event

