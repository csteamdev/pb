﻿$PBExportHeader$w_scelta_operatore.srw
$PBExportComments$Window scelta operatore
forward
global type w_scelta_operatore from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_scelta_operatore
end type
type cb_conferma from commandbutton within w_scelta_operatore
end type
type dw_scelta_operatore from uo_cs_xx_dw within w_scelta_operatore
end type
end forward

global type w_scelta_operatore from w_cs_xx_risposta
integer width = 1659
integer height = 1756
string title = "Scelta Operatore"
cb_annulla cb_annulla
cb_conferma cb_conferma
dw_scelta_operatore dw_scelta_operatore
end type
global w_scelta_operatore w_scelta_operatore

on w_scelta_operatore.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.dw_scelta_operatore=create dw_scelta_operatore
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.dw_scelta_operatore
end on

on w_scelta_operatore.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.dw_scelta_operatore)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options( c_autoposition + c_noresizewin )

dw_scelta_operatore.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete + c_nomodify,c_default)
end event

type cb_annulla from commandbutton within w_scelta_operatore
integer x = 846
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(parent)
end event

type cb_conferma from commandbutton within w_scelta_operatore
integer x = 1234
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;integer li_anno_registrazione
long ll_num_registrazione
string ls_cod_operaio

li_anno_registrazione = s_cs_xx.parametri.parametro_i_1
ll_num_registrazione = s_cs_xx.parametri.parametro_ul_1

ls_cod_operaio = dw_scelta_operatore.getitemstring(dw_scelta_operatore.getrow(),"cod_operaio")

update programmi_manutenzione
set cod_operaio=:ls_cod_operaio
where cod_azienda=:s_cs_xx.cod_azienda
and   anno_registrazione=:li_anno_registrazione
and   num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omni","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

close(parent)
end event

type dw_scelta_operatore from uo_cs_xx_dw within w_scelta_operatore
integer x = 23
integer y = 20
integer width = 1577
integer height = 1520
integer taborder = 10
string dataobject = "d_scelta_operatore"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error


l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event doubleclicked;call super::doubleclicked;cb_conferma.triggerevent("clicked")
end event

