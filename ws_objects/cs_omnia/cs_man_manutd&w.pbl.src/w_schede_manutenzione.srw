﻿$PBExportHeader$w_schede_manutenzione.srw
$PBExportComments$Anagrafica Schede Manutenzione
forward
global type w_schede_manutenzione from w_cs_xx_principale
end type
type dw_schede_attrezzature_lista from uo_cs_xx_dw within w_schede_manutenzione
end type
type cb_procedure from cb_apri_manuale within w_schede_manutenzione
end type
type cb_dettaglio from cb_apri_manuale within w_schede_manutenzione
end type
type dw_schede_attrezzature from uo_cs_xx_dw within w_schede_manutenzione
end type
type cb_controllo from commandbutton within w_schede_manutenzione
end type
type cb_pareto from commandbutton within w_schede_manutenzione
end type
end forward

global type w_schede_manutenzione from w_cs_xx_principale
integer width = 2843
integer height = 1788
string title = "Scheda Manutenzioni Attrezzature"
dw_schede_attrezzature_lista dw_schede_attrezzature_lista
cb_procedure cb_procedure
cb_dettaglio cb_dettaglio
dw_schede_attrezzature dw_schede_attrezzature
cb_controllo cb_controllo
cb_pareto cb_pareto
end type
global w_schede_manutenzione w_schede_manutenzione

type variables
boolean ib_new=FALSE, ib_modify=FALSE, ib_in_new=FALSE
boolean ib_aggiorna_richiesta
end variables

forward prototypes
public function integer wf_crea_det_manutenzioni (string ws_cod_attrezzatura, long wn_num_registrazione)
end prototypes

public function integer wf_crea_det_manutenzioni (string ws_cod_attrezzatura, long wn_num_registrazione);string ls_lavorazione, ls_sql,	ls_cod_attrezzatura,	ls_cod_tipo_manutenzione, &
    	 ls_flag_manutenzione, ls_flag_ricambio_codificato, ls_cod_ricambio,	ls_des_ricambio_non_codificato, &
		 ls_cod_operaio, ls_des_taratura , ls_cod_attrezzatura_taratura , ls_cod_misura
long ll_quantita, ll_prog_riga_manutenzione, ll_quan_ricambi, ll_valore_min_taratura, &
     ll_valore_max_taratura, ll_num_reg_lista, ll_valore_atteso, ll_costo_orario  
datetime ldt_null

setnull(ldt_null)

declare cu_piani dynamic cursor for sqlsa;

ls_sql = "SELECT tab_piani_manutenzione.cod_attrezzatura, " +  &
                "tab_piani_manutenzione.prog_riga_manutenzione, " + &
					 "tab_piani_manutenzione.cod_tipo_manutenzione, " + &
					 "tab_piani_manutenzione.flag_manutenzione, " + &
					 "tab_piani_manutenzione.flag_ricambio_codificato, " + &
					 "tab_piani_manutenzione.cod_ricambio, " + &
					 "tab_piani_manutenzione.des_ricambio_non_codificato, " + &
					 "tab_piani_manutenzione.quan_ricambi, " + &
					 "tab_piani_manutenzione.cod_operaio, " + &
					 "tab_piani_manutenzione.des_taratura, " + &
					 "tab_piani_manutenzione.cod_attrezzatura_taratura, " + &
					 "tab_piani_manutenzione.cod_misura, " + &
					 "tab_piani_manutenzione.valore_min_taratura, " + &
					 "tab_piani_manutenzione.valore_max_taratura, " + &
					 "tab_piani_manutenzione.num_reg_lista, " + &
					 "tab_piani_manutenzione.valore_atteso " + &
                "FROM tab_piani_manutenzione  " + &
					 "WHERE (tab_piani_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "') " + &
					 "and (tab_piani_manutenzione.cod_attrezzatura = '"+ws_cod_attrezzatura+"')"

prepare sqlsa from :ls_sql;

open dynamic cu_piani;

do while 1=1
   fetch cu_piani into :ls_cod_attrezzatura,
   :ll_prog_riga_manutenzione,
	:ls_cod_tipo_manutenzione,
	:ls_flag_manutenzione,
	:ls_flag_ricambio_codificato,
	:ls_cod_ricambio,
	:ls_des_ricambio_non_codificato,
	:ll_quan_ricambi,
	:ls_cod_operaio,
	:ls_des_taratura ,
	:ls_cod_attrezzatura_taratura ,
	:ls_cod_misura,
	:ll_valore_min_taratura,
	:ll_valore_max_taratura,
	:ll_num_reg_lista,
	:ll_valore_atteso;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit


	SELECT tab_cat_attrezzature.costo_medio_orario  
	INTO   :ll_costo_orario  
	FROM   tab_cat_attrezzature  
	WHERE  (tab_cat_attrezzature.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			 (tab_cat_attrezzature.cod_cat_attrezzature = 
					 (SELECT anag_operai.cod_cat_attrezzature  
					  FROM   anag_operai  
					  WHERE  (anag_operai.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								(anag_operai.cod_operaio = :ls_cod_operaio  	)  ))   ;
	if sqlca.sqlcode <> 0 then
		ll_costo_orario = 0
	end if

   INSERT INTO det_schede_manutenzioni  
         ( cod_azienda,   
           cod_attrezzatura,   
           num_registrazione,   
           prog_riga_manutenzione,   
           cod_tipo_manutenzione,   
           flag_manutenzione,   
           modalita_esecuzione,   
           flag_ricambio_codificato,   
           cod_ricambio,   
           des_ricambio_non_codificato,   
           quan_ricambi,   
           costo_ricambio,   
           cod_operaio,   
           costo_orario_operaio,   
           tempo_impiegato,   
           costo_manodopera_esterna,   
           des_taratura,   
           cod_attrezzatura_taratura,   
           cod_misura,   
           valore_min_taratura,   
           valore_max_taratura,   
           doc_compilato,   
           num_reg_lista,   
           prog_liste_con_comp,   
           num_versione,   
           num_edizione,   
           ore_impiegate,   
           minuti_impiegati,
			  prog_piani_manutenzione,
			  valore_atteso,
			  flag_eseguito)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ws_cod_attrezzatura,   
           :wn_num_registrazione,   
           :ll_prog_riga_manutenzione,   
           :ls_cod_tipo_manutenzione,   
           :ls_flag_manutenzione,   
           null,   
           :ls_flag_ricambio_codificato,   
           :ls_cod_ricambio,   
           :ls_des_ricambio_non_codificato,   
           :ll_quan_ricambi,   
           0,   
           :ls_cod_operaio,   
           :ll_costo_orario,   
           :ldt_null,   
           0,   
           :ls_des_taratura,   
           :ls_cod_attrezzatura_taratura,   
           :ls_cod_misura,   
           :ll_valore_min_taratura,   
           :ll_valore_max_taratura,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,
			  :ll_prog_riga_manutenzione,
			  :ll_valore_atteso,
			  'N')  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante creazione dettaglio manutenzione: " + &
		                   "il dettaglio potrebbe non corrispondere a quanto impostato " + &
								 "nei piani di manutenzione",Information!)
	end if
loop
close cu_piani;
return 0
end function

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_schede_attrezzature_lista,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione", &
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_attrezzature,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione", &
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_attrezzature,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_attrezzature,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

f_PO_LoadDDDW_DW(dw_schede_attrezzature,"cod_guasto",sqlca,&
                 "tab_guasti","cod_guasto","des_guasto",&
                 "(tab_guasti.cod_azienda = '" + s_cs_xx.cod_azienda + "')"  )

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_schede_attrezzature_lista.set_dw_key("cod_azienda")
dw_schede_attrezzature_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_schede_attrezzature.set_dw_options(sqlca,dw_schede_attrezzature_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_schede_attrezzature_lista
end on

on w_schede_manutenzione.create
int iCurrent
call super::create
this.dw_schede_attrezzature_lista=create dw_schede_attrezzature_lista
this.cb_procedure=create cb_procedure
this.cb_dettaglio=create cb_dettaglio
this.dw_schede_attrezzature=create dw_schede_attrezzature
this.cb_controllo=create cb_controllo
this.cb_pareto=create cb_pareto
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_schede_attrezzature_lista
this.Control[iCurrent+2]=this.cb_procedure
this.Control[iCurrent+3]=this.cb_dettaglio
this.Control[iCurrent+4]=this.dw_schede_attrezzature
this.Control[iCurrent+5]=this.cb_controllo
this.Control[iCurrent+6]=this.cb_pareto
end on

on w_schede_manutenzione.destroy
call super::destroy
destroy(this.dw_schede_attrezzature_lista)
destroy(this.cb_procedure)
destroy(this.cb_dettaglio)
destroy(this.dw_schede_attrezzature)
destroy(this.cb_controllo)
destroy(this.cb_pareto)
end on

event pc_close;call super::pc_close;setnull(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_i_2 = 0
s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_2 = 0


end event

type dw_schede_attrezzature_lista from uo_cs_xx_dw within w_schede_manutenzione
integer x = 23
integer y = 20
integer width = 2377
integer height = 500
integer taborder = 50
string dataobject = "d_schede_manutenzione_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_modify;call super::pcd_modify;string ls_str

ib_modify = true
cb_controllo.enabled = false
cb_pareto.enabled = false
cb_dettaglio.enabled = false
ls_str = getitemstring(getrow(),"cod_attrezzatura")
if not isnull(ls_str) then
   f_PO_LoadDDDW_DW(dw_schede_attrezzature,"cod_guasto",sqlca,&
                 "tab_guasti","cod_guasto","des_guasto",&
                 "(tab_guasti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tab_guasti.cod_attrezzatura = '" + ls_str + "')"  )
end if


end event

event updatestart;call super::updatestart;string ls_unita_tempo, ls_cod_attrezzatura, ls_cod_operaio, ls_uso_scadenza
long ll_num_registrazione, ll_periodicita
datetime ldt_prossimo_intervento, ldt_original_value


if ib_new then
   ls_cod_attrezzatura = this.GetItemstring(this.GetRow ( ), "cod_attrezzatura")
   select max(schede_manutenzioni.num_registrazione)
     into :ll_num_registrazione
     from schede_manutenzioni
     where (schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda) and (:ls_cod_attrezzatura = schede_manutenzioni.cod_attrezzatura);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_registrazione", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_registrazione", 1)
   end if

   ldt_prossimo_intervento = getitemdatetime(getrow(), "prossimo_intervento")
   ls_cod_operaio         = getitemstring(getrow(), "cod_operaio")
   if (ldt_prossimo_intervento > datetime(date("01/01/1900"))) and (not isnull(ldt_prossimo_intervento)) then
      INSERT INTO schede_manutenzioni  
           ( cod_azienda,   
           cod_attrezzatura,   
           num_registrazione,   
           data_intervento,   
           desc_intervento,   
           ordinario,   
           note,   
           prossimo_intervento,   
           cod_operaio,   
           flag_eseguito )  
       VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_attrezzatura,   
           :ll_num_registrazione+1,   
           :ldt_prossimo_intervento,   
           'MANUTENZIONE STRAORDINARIA',   
           'N',   
           null,   
           null,   
           :ls_cod_operaio,   
           'N' )  ;
       if sqlca.sqlcode <> 0 then
          g_mb.messagebox("Manutenzione Attrezzature","ERRORE NELLA CREAZIONE DELLA SUCCESSIVA MANUTENZIONE PROGRAMMATA  " + sqlca.sqlerrtext)
       end if
		 wf_crea_det_manutenzioni(ls_cod_attrezzatura, ll_num_registrazione + 1)
   end if
   ib_aggiorna_richiesta = true
   ib_new = false
end if


if ib_modify then
   ls_cod_operaio         = getitemstring(getrow(), "cod_operaio")
   ls_cod_attrezzatura = this.GetItemstring(this.GetRow ( ), "cod_attrezzatura")

   SELECT anag_attrezzature.unita_tempo, utilizzo_scadenza , periodicita_revisione
   INTO   :ls_unita_tempo , :ls_uso_scadenza, :ll_periodicita
   FROM   anag_attrezzature  
   WHERE  ( anag_attrezzature.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( anag_attrezzature.cod_attrezzatura = :ls_cod_attrezzatura )   ;

//   if (getitemstring(getrow(),"flag_eseguito", primary! , true) = "N") and (getitemstring(getrow(),"flag_eseguito") = "S") and (ls_unita_tempo <> "M") and (ls_unita_tempo <> "O") and (ls_uso_scadenza = "S") and (getitemstring(getrow(), "ordinario") = "S")then 
//      select max(schede_manutenzioni.num_registrazione)
//      into :ll_num_registrazione
//      from schede_manutenzioni
//      where (schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda) and 
//      (schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura);
//
//      if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
//         ll_num_registrazione = 1
//      else
//         ll_num_registrazione = ll_num_registrazione + 1
//      end if
//
//      choose case ls_unita_tempo
//      case "E"
//         ld_prossimo_intervento = relativedate(getitemdate(getrow(), "data_intervento"), ll_periodicita * 30)
//      case "S" 
//         ld_prossimo_intervento = relativedate(getitemdate(getrow(), "data_intervento"), ll_periodicita * 7)
//      case "G"
//         ld_prossimo_intervento = relativedate(getitemdate(getrow(), "data_intervento"), ll_periodicita)
//      end choose
//
//      INSERT INTO schede_manutenzioni
//                ( cod_azienda, 
//					   cod_attrezzatura, 
//						num_registrazione, 
//						data_intervento, 
//						desc_intervento,   
//                  ordinario, 
//						note, 
//						prossimo_intervento, 
//						cod_operaio, 
//						flag_eseguito )  
//      VALUES    ( :s_cs_xx.cod_azienda, 
//		            :ls_cod_attrezzatura, 
//						:ll_num_registrazione,   
//                  :ld_prossimo_intervento, 
//						'MANUTENZIONE PROGRAMMATA', 
//						'S', 
//						null, 
//						null,
//                  :ls_cod_operaio, 
//						'N' )  ;
//       if sqlca.sqlcode <> 0 then
//          messagebox("Manutenzione Attrezzature","ERRORE NELLA CREAZIONE DELLA SUCCESSIVA MANUTENZIONE PROGRAMMATA  "+sqlca.sqlerrtext)
//       end if
//		 wf_crea_det_manutenzioni(ls_cod_attrezzatura, ll_num_registrazione)
//   end if
//
   ldt_prossimo_intervento = getitemdatetime(getrow(), "prossimo_intervento")
   ldt_original_value = getitemdatetime(getrow(),"prossimo_intervento", primary!, true)
   if isnull(ldt_original_value) then ldt_original_value = datetime(date("01/01/1900"))
   if (ldt_prossimo_intervento > datetime(date("01/01/1900"))) and (not isnull(ldt_prossimo_intervento)) and ( ldt_original_value <> ldt_prossimo_intervento) then
      select max(schede_manutenzioni.num_registrazione)
      into :ll_num_registrazione
      from schede_manutenzioni
      where (schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda) and (:ls_cod_attrezzatura = schede_manutenzioni.cod_attrezzatura);

      if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
         ll_num_registrazione = 1
      else
         ll_num_registrazione = ll_num_registrazione + 1
      end if

      ls_cod_operaio         = getitemstring(getrow(), "cod_operaio")
      INSERT INTO schede_manutenzioni  
		           ( cod_azienda,   
		           cod_attrezzatura,   
		           num_registrazione,   
		           data_intervento,   
		           desc_intervento,   
		           ordinario,   
		           note,   
		           prossimo_intervento,   
		           cod_operaio,   
		           flag_eseguito )  
      VALUES 	  ( :s_cs_xx.cod_azienda,   
		           :ls_cod_attrezzatura,   
		           :ll_num_registrazione+1,   
		           :ldt_prossimo_intervento,   
		           'MANUTENZIONE STRAORDINARIA',   
      		     'N',   
		           null,   
		           null,   
		           :ls_cod_operaio,   
      		     'N' )  ;
       if sqlca.sqlcode <> 0 then
          g_mb.messagebox("Manutenzione Attrezzature","ERRORE NELLA CREAZIONE DELLA SUCCESSIVA MANUTENZIONE STRAORDINARIA  "+sqlca.sqlerrtext)
       end if
		 wf_crea_det_manutenzioni(ls_cod_attrezzatura, ll_num_registrazione + 1)
	end if
   ib_modify = false
end if

end event

event pcd_validaterow;call super::pcd_validaterow;if isnull(getitemdatetime(getrow(), "data_intervento")) then
   g_mb.messagebox("Schede Manutenzione","Data intervento obbligatoria", StopSign!)
   pcca.error = c_fatal
end if

if isnull(getitemstring(getrow(), "cod_operaio")) then
   g_mb.messagebox("Schede Manutenzione","Indicazione del responsabile intervento obbligatoria", StopSign!)
   pcca.error = c_fatal
end if

if isnull(getitemstring(getrow(), "cod_attrezzatura")) then
   g_mb.messagebox("Schede Manutenzione","Indicazione dell'attrezzatura obbligatoria", StopSign!)
   pcca.error = c_fatal
end if


end event

event pcd_view;call super::pcd_view;ib_new = false
ib_in_new = false
ib_modify = false
cb_controllo.enabled = true
cb_pareto.enabled = true
cb_dettaglio.enabled = true
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_new;call super::pcd_new;datetime ldd_oggi

ib_new = true
ib_in_new = true
cb_controllo.enabled = false
cb_pareto.enabled = false
cb_dettaglio.enabled = false

ldd_oggi = datetime(today())
this.setitem(this.getrow(),"data_intervento",ldd_oggi)
end event

event updateend;call super::updateend;long ll_registrazione
string ls_cod_attrezzatura

ls_cod_attrezzatura = this.getitemstring(this.getrow(),"cod_attrezzatura")
ll_registrazione = this.getitemnumber(this.getrow(),"num_registrazione")

if s_cs_xx.parametri.parametro_s_1 = "MANUTENZIONE" and ib_aggiorna_richiesta then
   UPDATE richieste  
   SET    cod_attrezzatura = :ls_cod_attrezzatura,   
          num_registrazione =  :ll_registrazione
   WHERE (richieste.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         (richieste.anno_reg_richiesta = :s_cs_xx.parametri.parametro_i_1 ) AND  
         (richieste.num_reg_richiesta  = :s_cs_xx.parametri.parametro_d_1 )   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Schede Manutenzioni","Impossibile aggiornare il numero di Non Conformità in richieste manutenzioni", Information!)
   end if
   ib_aggiorna_richiesta = false
end if

if ib_in_new then
	wf_crea_det_manutenzioni(this.getitemstring(this.getrow(),"cod_attrezzatura"), &
	                         this.getitemnumber(this.getrow(),"num_registrazione") )
end if									 
end event

type cb_procedure from cb_apri_manuale within w_schede_manutenzione
integer x = 2423
integer y = 20
integer width = 370
integer height = 80
integer taborder = 10
string text = "&Procedure"
end type

event clicked;call super::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLM")
end event

type cb_dettaglio from cb_apri_manuale within w_schede_manutenzione
integer x = 2423
integer y = 220
integer height = 80
integer taborder = 20
string text = "&Dettagli"
end type

on clicked;call cb_apri_manuale::clicked;if not isvalid(w_det_schede_manutenzioni) then
   s_cs_xx.parametri.parametro_s_1 = dw_schede_attrezzature_lista.getitemstring(dw_schede_attrezzature_lista.getrow(),"cod_attrezzatura")
   window_open_parm(w_det_schede_manutenzioni, -1, dw_schede_attrezzature_lista)
end if
end on

type dw_schede_attrezzature from uo_cs_xx_dw within w_schede_manutenzione
integer x = 23
integer y = 540
integer width = 2766
integer height = 1120
integer taborder = 60
string dataobject = "d_schede_manutenzione"
borderstyle borderstyle = styleraised!
end type

event pcd_validatecol;call super::pcd_validatecol;choose case i_colname
case "cod_attrezzatura"
   f_PO_LoadDDDW_DW(dw_schede_attrezzature,"cod_guasto",sqlca,&
                    "tab_guasti","cod_guasto","des_guasto",&
                    "(tab_guasti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tab_guasti.cod_attrezzatura = '" + i_coltext + "')"  )
end choose
end event

type cb_controllo from commandbutton within w_schede_manutenzione
event clicked pbm_bnclicked
integer x = 2423
integer y = 120
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&ontrollo"
end type

event clicked;long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_num_registrazione
string ls_cod_attrezzatura
   
ll_i[1] = dw_schede_attrezzature_lista.getrow()
if isnull(dw_schede_attrezzature.getitemnumber(ll_i[1], "num_reg_lista")) or &
   dw_schede_attrezzature.getitemnumber(ll_i[1], "num_reg_lista") = 0 then
      g_mb.messagebox("Manutenzione Attrezzature","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione manutenzioni", StopSign!)
      return
end if

ll_num_reg_lista = dw_schede_attrezzature.getitemnumber(ll_i[1],"num_reg_lista") 
ll_prog_liste_con_comp = dw_schede_attrezzature.getitemnumber(ll_i[1],"prog_liste_con_comp") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ls_cod_attrezzatura = dw_schede_attrezzature_lista.getitemstring(ll_i[1],"cod_attrezzatura")
   ll_num_registrazione = dw_schede_attrezzature_lista.getitemnumber(ll_i[1],"num_registrazione")
   
   update schede_manutenzioni
   set    schede_manutenzioni.num_versione = :ll_num_versione,
          schede_manutenzioni.num_edizione = :ll_num_edizione,
          schede_manutenzioni.num_reg_lista = :ll_num_reg_lista,
          schede_manutenzioni.prog_liste_con_comp = :ll_prog_liste_con_comp
   where  schede_manutenzioni.cod_azienda = :s_cs_xx.cod_azienda and
          schede_manutenzioni.cod_attrezzatura = :ls_cod_attrezzatura and 
          schede_manutenzioni.num_registrazione = :ll_num_registrazione;
   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_schede_attrezzature_lista.triggerevent("pcd_retrieve")
   dw_schede_attrezzature_lista.set_selected_rows(1, &
                                                ll_i[], &
                                                c_ignorechanges, &
                                                c_refreshchildren, &
                                                c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_schede_attrezzature_lista)

end event

type cb_pareto from commandbutton within w_schede_manutenzione
integer x = 2423
integer y = 320
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Pareto"
end type

event clicked;window_open(w_grafici_guasti, -1)
end event

