﻿$PBExportHeader$w_schedula_manutenzioni.srw
forward
global type w_schedula_manutenzioni from w_cs_xx_principale
end type
type cbx_precedenti from checkbox within w_schedula_manutenzioni
end type
type cbx_remoto from checkbox within w_schedula_manutenzioni
end type
type cb_aggiorna from commandbutton within w_schedula_manutenzioni
end type
type cbx_tolleranza_tempo from checkbox within w_schedula_manutenzioni
end type
type cb_elimina from commandbutton within w_schedula_manutenzioni
end type
type cb_schedula from commandbutton within w_schedula_manutenzioni
end type
type em_a_data from editmask within w_schedula_manutenzioni
end type
type cbx_mantieni from checkbox within w_schedula_manutenzioni
end type
type em_primo_giorno_nuove from editmask within w_schedula_manutenzioni
end type
type st_3 from statictext within w_schedula_manutenzioni
end type
type st_4 from statictext within w_schedula_manutenzioni
end type
type em_primo_giorno_scadute from editmask within w_schedula_manutenzioni
end type
type st_5 from statictext within w_schedula_manutenzioni
end type
type dw_1 from datawindow within w_schedula_manutenzioni
end type
type ole_1 from olecustomcontrol within w_schedula_manutenzioni
end type
end forward

global type w_schedula_manutenzioni from w_cs_xx_principale
integer width = 3758
integer height = 1780
string title = "Schedulazione Manutenzioni"
cbx_precedenti cbx_precedenti
cbx_remoto cbx_remoto
cb_aggiorna cb_aggiorna
cbx_tolleranza_tempo cbx_tolleranza_tempo
cb_elimina cb_elimina
cb_schedula cb_schedula
em_a_data em_a_data
cbx_mantieni cbx_mantieni
em_primo_giorno_nuove em_primo_giorno_nuove
st_3 st_3
st_4 st_4
em_primo_giorno_scadute em_primo_giorno_scadute
st_5 st_5
dw_1 dw_1
ole_1 ole_1
end type
global w_schedula_manutenzioni w_schedula_manutenzioni

type variables
connection ict_connessione 
end variables

event pc_setwindow;call super::pc_setwindow;

em_primo_giorno_scadute.text = string(relativedate(today(),1))
em_primo_giorno_nuove.text = string(today())
//em_da_data.text= string(today())
em_a_data.text= string(relativedate(today(),30))

dw_1.settransobject(sqlca)

ole_1.object.day = day(today())
ole_1.object.month = month(today())
ole_1.object.year = year(today())

end event

on w_schedula_manutenzioni.create
int iCurrent
call super::create
this.cbx_precedenti=create cbx_precedenti
this.cbx_remoto=create cbx_remoto
this.cb_aggiorna=create cb_aggiorna
this.cbx_tolleranza_tempo=create cbx_tolleranza_tempo
this.cb_elimina=create cb_elimina
this.cb_schedula=create cb_schedula
this.em_a_data=create em_a_data
this.cbx_mantieni=create cbx_mantieni
this.em_primo_giorno_nuove=create em_primo_giorno_nuove
this.st_3=create st_3
this.st_4=create st_4
this.em_primo_giorno_scadute=create em_primo_giorno_scadute
this.st_5=create st_5
this.dw_1=create dw_1
this.ole_1=create ole_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_precedenti
this.Control[iCurrent+2]=this.cbx_remoto
this.Control[iCurrent+3]=this.cb_aggiorna
this.Control[iCurrent+4]=this.cbx_tolleranza_tempo
this.Control[iCurrent+5]=this.cb_elimina
this.Control[iCurrent+6]=this.cb_schedula
this.Control[iCurrent+7]=this.em_a_data
this.Control[iCurrent+8]=this.cbx_mantieni
this.Control[iCurrent+9]=this.em_primo_giorno_nuove
this.Control[iCurrent+10]=this.st_3
this.Control[iCurrent+11]=this.st_4
this.Control[iCurrent+12]=this.em_primo_giorno_scadute
this.Control[iCurrent+13]=this.st_5
this.Control[iCurrent+14]=this.dw_1
this.Control[iCurrent+15]=this.ole_1
end on

on w_schedula_manutenzioni.destroy
call super::destroy
destroy(this.cbx_precedenti)
destroy(this.cbx_remoto)
destroy(this.cb_aggiorna)
destroy(this.cbx_tolleranza_tempo)
destroy(this.cb_elimina)
destroy(this.cb_schedula)
destroy(this.em_a_data)
destroy(this.cbx_mantieni)
destroy(this.em_primo_giorno_nuove)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.em_primo_giorno_scadute)
destroy(this.st_5)
destroy(this.dw_1)
destroy(this.ole_1)
end on

event open;call super::open;long ll_rc

//ict_connessione = create connection
//ict_connessione.driver = "winsock"
//ict_connessione.application = "7500"
//ict_connessione.location = "cs_team_1"
//
//ll_rc = ict_connessione.ConnectToServer()

IF ll_rc <> 0 THEN
		g_mb.messagebox("Connessione a server remoto fallita.", string(ll_rc))
END IF


end event

type cbx_precedenti from checkbox within w_schedula_manutenzioni
integer x = 2766
integer y = 1420
integer width = 933
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Considera registrazioni precedenti"
boolean lefttext = true
end type

event clicked;cbx_mantieni.checked = false
end event

type cbx_remoto from checkbox within w_schedula_manutenzioni
integer x = 23
integer y = 940
integer width = 686
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Istanzia Server Remoto"
end type

type cb_aggiorna from commandbutton within w_schedula_manutenzioni
integer x = 3337
integer y = 1580
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna S."
end type

event clicked;integer li_risposta
string ls_errore
boolean lb_mantieni_precedente
datetime ldt_da_data,ldt_a_data,ldt_primo_giorno_nuove,ldt_primo_giorno_scadute
long ll_rc

uo_schedula_manutenzioni luo_schedula_manutenzioni

destroy uo_schedula_manutenzioni

if (g_mb.messagebox("Sep","Sei sicuro di aggiornare lo stato delle manutenzioni?",Exclamation!,yesno!, 2))=2 then return

setpointer(hourglass!)

ldt_a_data = datetime(date(em_a_data.text), 00:00:00)
ldt_primo_giorno_nuove = datetime(date(em_primo_giorno_nuove.text), 00:00:00)
ldt_primo_giorno_scadute = datetime(date(em_primo_giorno_scadute.text), 00:00:00)
lb_mantieni_precedente = cbx_mantieni.checked


if cbx_remoto.checked = true then
	ll_rc = ict_connessione.CreateInstance(luo_schedula_manutenzioni)
	
	if ll_rc <> 0 then 
			g_mb.messagebox("Createinstance failed", string(ll_rc))
			return
	end if
	
else
	luo_schedula_manutenzioni = create uo_schedula_manutenzioni
	
end if

luo_schedula_manutenzioni.is_cod_azienda = s_cs_xx.cod_azienda
luo_schedula_manutenzioni.is_formato_data = s_cs_xx.db_funzioni.formato_data

li_risposta = luo_schedula_manutenzioni.uof_aggiorna_scadute (ldt_primo_giorno_nuove, &
																				  ldt_primo_giorno_scadute,&
																				  ls_errore )

if li_risposta < 0 then
	g_mb.messagebox("Omnia",ls_errore,stopsign!)
	rollback;
	destroy uo_schedula_manutenzioni
	return
end if

commit;

destroy uo_schedula_manutenzioni

setpointer(arrow!)

g_mb.messagebox("Omnia","Aggiornamento manutenzioni eseguito con successo!",Information!)
end event

type cbx_tolleranza_tempo from checkbox within w_schedula_manutenzioni
integer x = 2697
integer y = 1500
integer width = 1006
integer height = 60
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Usa tolleranza tempo"
boolean lefttext = true
boolean righttoleft = true
end type

type cb_elimina from commandbutton within w_schedula_manutenzioni
integer x = 2560
integer y = 1580
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;long ll_rc
integer li_risposta
string ls_errore
uo_schedula_manutenzioni luo_schedula_manutenzioni

if cbx_remoto.checked = true then
	ll_rc = ict_connessione.CreateInstance(luo_schedula_manutenzioni)
	
	if ll_rc <> 0 then 
		g_mb.messagebox("Createinstance failed", string(ll_rc))
		return
	end if
	
else
	luo_schedula_manutenzioni = create uo_schedula_manutenzioni
	
end if

luo_schedula_manutenzioni.is_cod_azienda = s_cs_xx.cod_azienda
luo_schedula_manutenzioni.is_formato_data = s_cs_xx.db_funzioni.formato_data

li_risposta = luo_schedula_manutenzioni.uof_elimina ( ls_errore )

if li_risposta < 0 then
	g_mb.messagebox("Omnia",ls_errore,stopsign!)
	rollback;
	destroy uo_schedula_manutenzioni
	return
end if


commit;

destroy uo_schedula_manutenzioni

g_mb.messagebox("Omnia","Eliminazione della Schedulazione eseguita con successo!",Information!)
end event

type cb_schedula from commandbutton within w_schedula_manutenzioni
integer x = 2949
integer y = 1580
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Schedula"
end type

event clicked;integer li_risposta
string ls_errore
boolean lb_mantieni_precedente
datetime ldt_da_data,ldt_a_data,ldt_primo_giorno_nuove,ldt_primo_giorno_scadute
long ll_rc

uo_schedula_manutenzioni luo_schedula_manutenzioni

if (g_mb.messagebox("Sep","Sei sicuro di generare la schedulazione?",Exclamation!,yesno!, 2))=2 then return

setpointer(hourglass!)
//ldt_da_data = datetime(date(em_da_data.text),00:00:00)
ldt_a_data = datetime(date(em_a_data.text), 00:00:00)
ldt_primo_giorno_nuove = datetime(date(em_primo_giorno_nuove.text), 00:00:00)
ldt_primo_giorno_scadute = datetime(date(em_primo_giorno_scadute.text), 00:00:00)
lb_mantieni_precedente = cbx_mantieni.checked


if cbx_remoto.checked = true then
	ll_rc = ict_connessione.CreateInstance(luo_schedula_manutenzioni)
	
	if ll_rc <> 0 then 
			g_mb.messagebox("Createinstance failed", string(ll_rc))
			return
	end if
	
else
	luo_schedula_manutenzioni = create uo_schedula_manutenzioni
	
end if

luo_schedula_manutenzioni.is_cod_azienda = s_cs_xx.cod_azienda
luo_schedula_manutenzioni.is_formato_data = s_cs_xx.db_funzioni.formato_data

li_risposta = luo_schedula_manutenzioni.uof_schedula ( ldt_da_data, &
																	  ldt_a_data, &
																	  ldt_primo_giorno_nuove, &
																	  ldt_primo_giorno_scadute,&
																	  lb_mantieni_precedente, &
																	  cbx_tolleranza_tempo.checked,&
																	  cbx_precedenti.checked,&
																	  ls_errore )

if li_risposta < 0 then
	g_mb.messagebox("Omnia",ls_errore,stopsign!)
	rollback;
	destroy uo_schedula_manutenzioni
	return
end if


commit;

destroy uo_schedula_manutenzioni

setpointer(arrow!)

commit;

g_mb.messagebox("Omnia","Schedulazione eseguita con successo!",Information!)

commit;
end event

type em_a_data from editmask within w_schedula_manutenzioni
integer x = 1280
integer y = 1580
integer width = 411
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type cbx_mantieni from checkbox within w_schedula_manutenzioni
integer x = 1714
integer y = 1420
integer width = 1006
integer height = 76
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Mantieni schedulazione precedente"
boolean lefttext = true
boolean righttoleft = true
end type

event clicked;cbx_precedenti.checked = false
end event

type em_primo_giorno_nuove from editmask within w_schedula_manutenzioni
integer x = 1280
integer y = 1420
integer width = 411
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_3 from statictext within w_schedula_manutenzioni
integer x = 23
integer y = 1440
integer width = 1253
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Primo giorno sched. manut. nuove / data corrente:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_schedula_manutenzioni
integer x = 343
integer y = 1520
integer width = 937
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Primo giorno sched. manut. scadute:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_primo_giorno_scadute from editmask within w_schedula_manutenzioni
integer x = 1280
integer y = 1500
integer width = 411
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_5 from statictext within w_schedula_manutenzioni
integer x = 823
integer y = 1600
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Schedula sino a:"
alignment alignment = right!
end type

type dw_1 from datawindow within w_schedula_manutenzioni
integer x = 1051
integer y = 20
integer width = 2651
integer height = 1380
integer taborder = 20
string title = "none"
string dataobject = "d_det_cal_progr_man"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type ole_1 from olecustomcontrol within w_schedula_manutenzioni
event click ( )
event dblclick ( )
event keydown ( integer keycode,  integer shift )
event keypress ( integer keyascii )
event keyup ( integer keycode,  integer shift )
event beforeupdate ( integer cancel )
event afterupdate ( )
event newmonth ( )
event newyear ( )
integer y = 20
integer width = 1029
integer height = 900
integer taborder = 10
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
string binarykey = "w_schedula_manutenzioni.win"
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event clicked;datetime ldt_data_giorno
integer li_day,li_month,li_year

li_day = ole_1.object.day
li_month = ole_1.object.month
li_year = ole_1.object.year

ldt_data_giorno = datetime(date(string(li_day)+"/"+string(li_month)+"/"+string(li_year)),00:00:00)

dw_1.retrieve(s_cs_xx.cod_azienda,ldt_data_giorno)



end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
06w_schedula_manutenzioni.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
16w_schedula_manutenzioni.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
