﻿$PBExportHeader$w_sel_des_mappa.srw
$PBExportComments$Finestra inserimento descrizione mappa
forward
global type w_sel_des_mappa from w_cs_xx_risposta
end type
type cb_conferma from commandbutton within w_sel_des_mappa
end type
type dw_sel_des_mappa from uo_cs_xx_dw within w_sel_des_mappa
end type
end forward

global type w_sel_des_mappa from w_cs_xx_risposta
integer width = 1989
integer height = 412
string title = "Inserimento Descrizione Mappa"
cb_conferma cb_conferma
dw_sel_des_mappa dw_sel_des_mappa
end type
global w_sel_des_mappa w_sel_des_mappa

on w_sel_des_mappa.create
int iCurrent
call super::create
this.cb_conferma=create cb_conferma
this.dw_sel_des_mappa=create dw_sel_des_mappa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_conferma
this.Control[iCurrent+2]=this.dw_sel_des_mappa
end on

on w_sel_des_mappa.destroy
call super::destroy
destroy(this.cb_conferma)
destroy(this.dw_sel_des_mappa)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_sel_des_mappa.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
end event

type cb_conferma from commandbutton within w_sel_des_mappa
integer x = 1559
integer y = 212
integer width = 357
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;dw_sel_des_mappa.accepttext()

s_cs_xx.parametri.parametro_s_10 = dw_sel_des_mappa.getitemstring(1, "descrizione_mappa")

close(w_sel_des_mappa)


end event

type dw_sel_des_mappa from uo_cs_xx_dw within w_sel_des_mappa
integer x = 14
integer y = 8
integer width = 1938
integer height = 204
integer taborder = 10
string dataobject = "d_sel_des_mappa"
boolean border = false
end type

