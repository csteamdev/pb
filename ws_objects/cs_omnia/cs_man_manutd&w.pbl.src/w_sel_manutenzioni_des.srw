﻿$PBExportHeader$w_sel_manutenzioni_des.srw
$PBExportComments$Finestra selezione inventario per esportare dati in Excel
forward
global type w_sel_manutenzioni_des from w_cs_xx_risposta
end type
type dw_selezione from uo_cs_xx_dw within w_sel_manutenzioni_des
end type
type cb_conferma from commandbutton within w_sel_manutenzioni_des
end type
end forward

global type w_sel_manutenzioni_des from w_cs_xx_risposta
integer width = 2286
integer height = 452
string title = "Inserimento Dati"
dw_selezione dw_selezione
cb_conferma cb_conferma
end type
global w_sel_manutenzioni_des w_sel_manutenzioni_des

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 							 
end event

on w_sel_manutenzioni_des.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.cb_conferma=create cb_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.cb_conferma
end on

on w_sel_manutenzioni_des.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.cb_conferma)
end on

event open;call super::open;s_cs_xx.parametri.parametro_s_10 = "ATTREZZATURANULLA"
end event

type dw_selezione from uo_cs_xx_dw within w_sel_manutenzioni_des
integer y = 16
integer width = 2217
integer height = 220
integer taborder = 10
string dataobject = "d_sel_manutenzioni_des"
boolean border = false
end type

type cb_conferma from commandbutton within w_sel_manutenzioni_des
integer x = 1851
integer y = 252
integer width = 357
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;dw_selezione.accepttext()

s_cs_xx.parametri.parametro_s_10 = dw_selezione.getitemstring(1, "descrizione")
if isnull(s_cs_xx.parametri.parametro_s_10) or s_cs_xx.parametri.parametro_s_10 = "" then
	g_mb.messagebox("OMNIA", "Attenzione: inserire una descrizione per la scheda!")
	return 0
end if
close(w_sel_manutenzioni_des)


end event

