﻿$PBExportHeader$w_sel_schede_manutenzione.srw
$PBExportComments$Finestra Selezione Filtro Report Costi Manutenzione
forward
global type w_sel_schede_manutenzione from w_cs_xx_principale
end type
type dw_sel_schede_manutenzione from uo_cs_xx_dw within w_sel_schede_manutenzione
end type
type cb_report from commandbutton within w_sel_schede_manutenzione
end type
end forward

global type w_sel_schede_manutenzione from w_cs_xx_principale
int Width=1884
int Height=561
boolean TitleBar=true
string Title="Selezioni Report Costi Manutenzione"
dw_sel_schede_manutenzione dw_sel_schede_manutenzione
cb_report cb_report
end type
global w_sel_schede_manutenzione w_sel_schede_manutenzione

event pc_setwindow;call super::pc_setwindow;datetime ldt_null

dw_sel_schede_manutenzione.set_dw_options(sqlca,pcca.null_object,c_nodelete+c_nomodify+c_newonopen+c_disableCC,c_default)
iuo_dw_main = dw_sel_schede_manutenzione
setnull(ldt_null)
dw_sel_schede_manutenzione.setitem(dw_sel_schede_manutenzione.getrow(),"rd_da_data", ldt_null)
dw_sel_schede_manutenzione.setitem(dw_sel_schede_manutenzione.getrow(),"rd_a_data" , ldt_null)
save_on_close(c_socnosave)

end event

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_sel_schede_manutenzione,"rs_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on w_sel_schede_manutenzione.create
int iCurrent
call w_cs_xx_principale::create
this.dw_sel_schede_manutenzione=create dw_sel_schede_manutenzione
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_sel_schede_manutenzione
this.Control[iCurrent+2]=cb_report
end on

on w_sel_schede_manutenzione.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_sel_schede_manutenzione)
destroy(this.cb_report)
end on

type dw_sel_schede_manutenzione from uo_cs_xx_dw within w_sel_schede_manutenzione
int X=23
int Y=21
int Width=1806
int Height=317
int TabOrder=10
string DataObject="d_sel_schede_manutenzione"
BorderStyle BorderStyle=StyleLowered!
end type

type cb_report from commandbutton within w_sel_schede_manutenzione
int X=1463
int Y=361
int Width=366
int Height=81
int TabOrder=20
string Text="&Report"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if isnull(dw_sel_schede_manutenzione.getitemstring(dw_sel_schede_manutenzione.getrow(),"rs_cod_attrezzatura")) then
   s_cs_xx.parametri.parametro_s_1 = "%"
else
   s_cs_xx.parametri.parametro_s_1 = dw_sel_schede_manutenzione.getitemstring(dw_sel_schede_manutenzione.getrow(),"rs_cod_attrezzatura")
end if   

if isnull(dw_sel_schede_manutenzione.getitemdatetime(dw_sel_schede_manutenzione.getrow(),"rd_da_data")) then
   s_cs_xx.parametri.parametro_data_1 = datetime(date("01/01/1900"))
else
   s_cs_xx.parametri.parametro_data_1 = dw_sel_schede_manutenzione.getitemdatetime(dw_sel_schede_manutenzione.getrow(),"rd_da_data")
end if

if isnull(dw_sel_schede_manutenzione.getitemdatetime(dw_sel_schede_manutenzione.getrow(),"rd_a_data")) then
   s_cs_xx.parametri.parametro_data_2 = datetime(date("31/12/2999"))
else
   s_cs_xx.parametri.parametro_data_2 = dw_sel_schede_manutenzione.getitemdatetime(dw_sel_schede_manutenzione.getrow(),"rd_a_data")
end if

if s_cs_xx.parametri.parametro_data_1 > s_cs_xx.parametri.parametro_data_2 then
   g_mb.messagebox("Selezioni Report Costi Attrezzatura","La data inizio non può essere maggiore della data fine", StopSign!)
end if

window_open_parm(w_report_costi_manut, -1, dw_sel_schede_manutenzione)
end event

