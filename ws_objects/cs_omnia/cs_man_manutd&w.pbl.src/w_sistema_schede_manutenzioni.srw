﻿$PBExportHeader$w_sistema_schede_manutenzioni.srw
forward
global type w_sistema_schede_manutenzioni from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_sistema_schede_manutenzioni
end type
type dw_1 from datawindow within w_sistema_schede_manutenzioni
end type
end forward

global type w_sistema_schede_manutenzioni from w_cs_xx_principale
integer width = 2597
integer height = 1224
string title = "Sistema Schede Manutenzioni"
cb_1 cb_1
dw_1 dw_1
end type
global w_sistema_schede_manutenzioni w_sistema_schede_manutenzioni

type variables
private:
	string is_w_name = "Schede Manutenzioni"
	
	long CHECK = 12320767
	long SUCCESS = 15400917
	long WARN = 11184895
end variables

forward prototypes
public subroutine wf_carica_schede ()
public function boolean wf_aggiorna_scheda (integer ai_cod_scheda, string as_valore)
public subroutine wf_aggiorna_schede ()
public subroutine wf_log (long al_row, string as_message, long al_color)
end prototypes

public subroutine wf_carica_schede ();long ll_i, ll_rows, ll_row
datastore lds_store

if not f_crea_datastore(lds_store, "SELECT * FROM tab_schede_manutenzioni WHERE cod_azienda='" + s_cs_xx.cod_azienda + "'") then
	g_mb.messagebox(this.title, "Impossibile creare datastore")
	return
end if

ll_rows = lds_store.retrieve()

if ll_rows > 0 then
	for ll_i = 1 to ll_rows
		ll_row = dw_1.insertrow(0)
		
		dw_1.setitem(ll_row, "cod_scheda", lds_store.getitemnumber(ll_i, "cod_scheda"))
		dw_1.setitem(ll_row, "des_scheda", lds_store.getitemstring(ll_i, "des_scheda"))
		dw_1.setitem(ll_row, "valore", lds_store.getitemstring(ll_i, "valore"))
		dw_1.setitem(ll_row, "stato", "")
	next
end if
end subroutine

public function boolean wf_aggiorna_scheda (integer ai_cod_scheda, string as_valore);update tab_collegamenti_manutenzioni
set
	valore = :as_valore
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_elemento = :ai_cod_scheda and 
	tipo_elemento = 'scheda';
	
if sqlca.sqlcode < 0 then
	return false
end if

return true
end function

public subroutine wf_aggiorna_schede ();string ls_valore
long ll_i, ll_cod_scheda
boolean ib_error = false

if dw_1.rowcount() > 0 then
	for ll_i = 1 to dw_1.rowcount()
		
		wf_log(ll_i, "ricerca in corso...", CHECK)
		
		ll_cod_scheda = dw_1.getitemnumber(ll_i, "cod_scheda")
		ls_valore = dw_1.getitemstring(ll_i, "valore")
		
		if wf_aggiorna_scheda(ll_cod_scheda, ls_valore) = false then
			g_mb.messagebox(this.title, "Errore durante l'aggiornamento scheda " + string(ll_cod_scheda) + " con valore " + ls_valore +".~r~n")
			wf_log(ll_i, "errore", WARN)
			rollback;
			ib_error = true
			exit
		else
			wf_log(ll_i, "success", SUCCESS)
		end if
		
		dw_1.scrolltorow(ll_i)
		yield()
	next
	
	if ib_error = false then
		commit;
	end if
end if

end subroutine

public subroutine wf_log (long al_row, string as_message, long al_color);
dw_1.setitem(al_row, "stato", as_message)
dw_1.setitem(al_row, "colore", al_color)
end subroutine

on w_sistema_schede_manutenzioni.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_1
end on

on w_sistema_schede_manutenzioni.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;wf_carica_schede()
end event

type cb_1 from commandbutton within w_sistema_schede_manutenzioni
integer x = 2149
integer y = 1000
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Sistema"
end type

event clicked;wf_aggiorna_schede()
end event

type dw_1 from datawindow within w_sistema_schede_manutenzioni
integer x = 23
integer y = 20
integer width = 2514
integer height = 940
integer taborder = 10
string title = "none"
string dataobject = "d_sistema_schede_manutenzioni"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

