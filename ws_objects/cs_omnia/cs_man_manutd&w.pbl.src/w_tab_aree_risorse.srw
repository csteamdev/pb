﻿$PBExportHeader$w_tab_aree_risorse.srw
$PBExportComments$risorse collegate alle aree aziendali
forward
global type w_tab_aree_risorse from w_cs_xx_principale
end type
type cb_duplica from commandbutton within w_tab_aree_risorse
end type
type cb_ricerca_operaio from cb_operai_ricerca within w_tab_aree_risorse
end type
type cb_reset from commandbutton within w_tab_aree_risorse
end type
type cb_ricerca from commandbutton within w_tab_aree_risorse
end type
type dw_folder_search from u_folder within w_tab_aree_risorse
end type
type dw_ricerca from uo_dw_search within w_tab_aree_risorse
end type
type cb_1 from cb_operai_ricerca within w_tab_aree_risorse
end type
type dw_aree_risorse_lista from uo_cs_xx_dw within w_tab_aree_risorse
end type
type dw_aree_risorse_det from uo_cs_xx_dw within w_tab_aree_risorse
end type
end forward

global type w_tab_aree_risorse from w_cs_xx_principale
integer width = 2459
integer height = 1872
string title = "Tabella Aree Risorse"
cb_duplica cb_duplica
cb_ricerca_operaio cb_ricerca_operaio
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
cb_1 cb_1
dw_aree_risorse_lista dw_aree_risorse_lista
dw_aree_risorse_det dw_aree_risorse_det
end type
global w_tab_aree_risorse w_tab_aree_risorse

type variables
long il_x[],il_y[]
boolean is_colore = false
end variables

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], l_objects1[ ]

dw_aree_risorse_lista.set_dw_key("cod_azienda")
dw_aree_risorse_lista.set_dw_key("progressivo")
dw_aree_risorse_lista.set_dw_options( sqlca, pcca.null_object, c_default, c_default)
dw_aree_risorse_det.set_dw_options( sqlca, dw_aree_risorse_lista, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_aree_risorse_lista

// ----------------------  folfer ricerca -----------------------

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)

l_objects1[1] = dw_aree_risorse_lista
l_objects1[2] = cb_duplica
dw_folder_search.fu_assigntab(1, "L.", l_Objects1[])
l_objects1[1] = dw_ricerca
l_objects1[2] = cb_ricerca
l_objects1[3] = cb_reset
l_objects1[4] = cb_1
//l_objects1[5] = cb_2
dw_folder_search.fu_assigntab(2, "R.", l_Objects1[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)

dw_ricerca.setitem( dw_ricerca.getrow(), "anno_riferimento", f_anno_riferimento())
// ---------------------------------------------------------------
end event

on w_tab_aree_risorse.create
int iCurrent
call super::create
this.cb_duplica=create cb_duplica
this.cb_ricerca_operaio=create cb_ricerca_operaio
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
this.cb_1=create cb_1
this.dw_aree_risorse_lista=create dw_aree_risorse_lista
this.dw_aree_risorse_det=create dw_aree_risorse_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_duplica
this.Control[iCurrent+2]=this.cb_ricerca_operaio
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.cb_ricerca
this.Control[iCurrent+5]=this.dw_folder_search
this.Control[iCurrent+6]=this.dw_ricerca
this.Control[iCurrent+7]=this.cb_1
this.Control[iCurrent+8]=this.dw_aree_risorse_lista
this.Control[iCurrent+9]=this.dw_aree_risorse_det
end on

on w_tab_aree_risorse.destroy
call super::destroy
destroy(this.cb_duplica)
destroy(this.cb_ricerca_operaio)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
destroy(this.cb_1)
destroy(this.dw_aree_risorse_lista)
destroy(this.dw_aree_risorse_det)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca,&
                 "cod_area", &
                 sqlca, &
					  "tab_aree_aziendali",&
					  "cod_area_aziendale",&
					  "des_area", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_ricerca,&
                 "cod_cat_risorsa_esterna", &
                 sqlca, &
					  "tab_cat_risorse_esterne",&
					  "cod_cat_risorse_esterne",&
					  "des_cat_risorse_esterne", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
					  
f_po_loaddddw_dw(dw_aree_risorse_det,&
                 "cod_cat_risorse_esterne", &
                 sqlca, &
					  "tab_cat_risorse_esterne",&
					  "cod_cat_risorse_esterne",&
					  "des_cat_risorse_esterne", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")		
					  
f_po_loaddddw_dw(dw_aree_risorse_det,&
                 "cod_risorsa_esterna", &
                 sqlca, &
					  "anag_risorse_esterne",&
					  "cod_risorsa_esterna",&
					  "descrizione", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")		
					  
					  
f_po_loaddddw_dw(dw_aree_risorse_det,&
                 "cod_area_aziendale", &
                 sqlca, &
					  "tab_aree_aziendali",&
					  "cod_area_aziendale",&
					  "des_area", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
end event

type cb_duplica from commandbutton within w_tab_aree_risorse
integer x = 2034
integer y = 780
integer width = 343
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
end type

event clicked;long	ll_anno_riferimento

dw_ricerca.accepttext()
ll_anno_riferimento = dw_ricerca.getitemnumber( dw_ricerca.getrow(), "anno_riferimento")

if isnull(ll_anno_riferimento) or ll_anno_riferimento = 0 then
	g_mb.messagebox( "OMNIA", "Attenzione: indicare l'anno di riferimento!", stopsign!)
	return -1
end if

window_open_parm(w_duplica_aree_risorse, 0, dw_ricerca)
end event

event getfocus;call super::getfocus;dw_aree_risorse_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"

end event

type cb_ricerca_operaio from cb_operai_ricerca within w_tab_aree_risorse
integer x = 2181
integer y = 1052
integer width = 73
integer height = 72
integer taborder = 80
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_aree_risorse_det
s_cs_xx.parametri.parametro_s_1 = "cod_operaio"
end event

type cb_reset from commandbutton within w_tab_aree_risorse
integer x = 1943
integer y = 560
integer width = 389
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;//dw_ricerca.fu_reset()
//dw_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio() )
//
//
string ls_null

setnull(ls_null)

dw_ricerca.setItem(1, "attrezzatura_da", ls_null)
dw_ricerca.setItem(1, "attrezzatura_a", ls_null)
dw_ricerca.setItem(1, "cod_tipo_manutenzione", ls_null)
dw_ricerca.setItem(1, "cod_categoria", ls_null)
dw_ricerca.setItem(1, "cod_reparto", ls_null)
dw_ricerca.setItem(1, "cod_area", ls_null)
dw_ricerca.setItem(1, "cod_divisione", ls_null)
end event

type cb_ricerca from commandbutton within w_tab_aree_risorse
integer x = 1531
integer y = 560
integer width = 389
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_folder_search.fu_SelectTab(1)
dw_aree_risorse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type dw_folder_search from u_folder within w_tab_aree_risorse
integer x = 23
integer y = 20
integer width = 2377
integer height = 864
integer taborder = 50
end type

type dw_ricerca from uo_dw_search within w_tab_aree_risorse
integer x = 137
integer y = 40
integer width = 2222
integer height = 460
integer taborder = 50
string dataobject = "d_aree_risorse_ricerca"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case getcolumnname()
		
	case "cod_cat_risorsa_esterna"
		
			f_po_loaddddw_dw(dw_ricerca,&
								  "cod_risorsa_esterna", &
								  sqlca, &
								  "anag_risorse_esterne",&
								  "cod_risorsa_esterna",&
								  "descrizione", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + data + "' ")	
			
		
end choose

end event

type cb_1 from cb_operai_ricerca within w_tab_aree_risorse
integer x = 2258
integer y = 220
integer width = 73
integer height = 72
integer taborder = 80
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
s_cs_xx.parametri.parametro_s_1 = "cod_operaio"
end event

type dw_aree_risorse_lista from uo_cs_xx_dw within w_tab_aree_risorse
event ue_seleziona_prima ( )
integer x = 137
integer y = 60
integer width = 1874
integer height = 800
integer taborder = 40
string dataobject = "d_aree_risorse_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_area, ls_cod_operaio, ls_cod_cat_risorsa_esterna, ls_cod_risorsa_esterna, where_clause, ls_prova, ls_new_select, ls_cod_fornitore
long   ll_index, ll_ret, ll_errore, ll_anno_riferimento

dw_ricerca.accepttext()

ls_area = dw_ricerca.GetItemString(1, "cod_area")
ls_cod_operaio = dw_ricerca.getitemstring( 1, "cod_operaio")
ls_cod_cat_risorsa_esterna = dw_ricerca.getitemstring( 1, "cod_cat_risorsa_esterna")
ls_cod_risorsa_esterna = dw_ricerca.getitemstring( 1, "cod_risorsa_esterna")
ls_cod_fornitore = dw_ricerca.getitemstring( 1, "cod_fornitore")
ll_anno_riferimento = dw_ricerca.getitemnumber( 1, "anno_riferimento")

where_clause = " where (cod_azienda = '" + s_cs_xx.cod_azienda + "') "

if not isnull(ls_area) and ls_area <> "" then
	where_clause = where_clause + " and cod_area_aziendale = '" + ls_area + "' "
end if

if not isnull( ls_cod_operaio) and ls_cod_operaio <> "" then
	where_clause += " and cod_operaio = '" + ls_cod_operaio + "'"
end if

if not isnull( ls_cod_cat_risorsa_esterna) and ls_cod_cat_risorsa_esterna <> "" then
	where_clause += " and cod_cat_risorse_esterne = '" + ls_cod_cat_risorsa_esterna + "' "
end if

if not isnull( ls_cod_risorsa_esterna) and ls_cod_risorsa_esterna <> "" then
	where_clause += " and cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "' "
end if

if not isnull( ls_cod_fornitore) and ls_cod_fornitore <> "" then
	where_clause += " and cod_fornitore = '" + ls_cod_fornitore + "' "
end if

if not isnull(ll_anno_riferimento) and ll_anno_riferimento > 0 then
	where_clause += " and anno_riferimento = " + string(ll_anno_riferimento) + " "
end if

where_clause += " order by cod_area_aziendale, progressivo"

ls_prova = upper(dw_aree_risorse_lista.GETSQLSELECT())

ll_index = pos(ls_prova, "WHERE")
if ll_index = 0 then
	ls_new_select = dw_aree_risorse_lista.GETSQLSELECT() + where_clause
else	
	ls_new_select = left(dw_aree_risorse_lista.GETSQLSELECT(), ll_index - 1) + where_clause
end if

ll_ret = dw_aree_risorse_lista.SetSQLSelect(ls_new_select)

if ll_ret < 0 then 
	g_mb.messagebox("OMNIA","Errore nel filtro di selezione: " + ls_new_select)
	pcca.error = c_fatal
else
	dw_aree_risorse_lista.resetupdate()
	ll_errore = retrieve()

	if ll_errore < 0 then
		pcca.error = c_fatal

	end if
	
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_max
string ls_cod_area

FOR l_Idx = 1 TO RowCount()
	
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
		
	if isnull( getitemnumber( l_idx, "progressivo")) or getitemnumber( l_idx, "progressivo") = 0 then
		
		ls_cod_area = getitemstring( l_Idx, "cod_area_aziendale")
		
		select max(progressivo)
		into   :ll_max
		from   tab_aree_risorse
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_area_aziendale = :ls_cod_area;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante la ricerca del progressivo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		if isnull(ll_max) then ll_max = 0
		ll_max ++
		
	end if
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_ricerca_operaio.enabled = true
dw_aree_risorse_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ricerca_operaio.enabled = true
dw_aree_risorse_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;cb_ricerca_operaio.enabled = false
dw_aree_risorse_det.object.b_ricerca_fornitore.enabled = false
end event

type dw_aree_risorse_det from uo_cs_xx_dw within w_tab_aree_risorse
integer x = 23
integer y = 900
integer width = 2377
integer height = 840
integer taborder = 70
string dataobject = "d_aree_risorse_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;long ll_max

choose case i_colname 
	case "cod_area_aziendale"
		
		select max(progressivo)
		into   :ll_max
		from   tab_aree_risorse
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_area_aziendale = :i_coltext;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante la ricerca del progressivo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		if isnull(ll_max) then ll_max = 0
		ll_max ++
		
		setitem( row, "progressivo", ll_max)

	case "cod_cat_risorse_esterne"
		
		f_po_loaddddw_dw(dw_aree_risorse_det,&
							  "cod_risorsa_esterna", &
							  sqlca, &
							  "anag_risorse_esterne",&
							  "cod_risorsa_esterna",&
							  "descrizione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "'")			
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_aree_risorse_det,"cod_fornitore")
end choose
end event

