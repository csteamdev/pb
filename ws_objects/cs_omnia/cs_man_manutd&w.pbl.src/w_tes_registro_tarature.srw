﻿$PBExportHeader$w_tes_registro_tarature.srw
$PBExportComments$Finestra Registro Tarature
forward
global type w_tes_registro_tarature from w_cs_xx_principale
end type
type cb_test_rip from commandbutton within w_tes_registro_tarature
end type
type cb_elimina_test from commandbutton within w_tes_registro_tarature
end type
type cb_procedure from cb_apri_manuale within w_tes_registro_tarature
end type
type dw_tes_registro_tarature_lista from uo_cs_xx_dw within w_tes_registro_tarature
end type
type dw_tes_registro_tarature_det from uo_cs_xx_dw within w_tes_registro_tarature
end type
end forward

global type w_tes_registro_tarature from w_cs_xx_principale
integer width = 2688
integer height = 1964
string title = "Registro Tarature"
cb_test_rip cb_test_rip
cb_elimina_test cb_elimina_test
cb_procedure cb_procedure
dw_tes_registro_tarature_lista dw_tes_registro_tarature_lista
dw_tes_registro_tarature_det dw_tes_registro_tarature_det
end type
global w_tes_registro_tarature w_tes_registro_tarature

type variables
boolean ib_new=FALSE

end variables

event pc_setwindow;call super::pc_setwindow;double ld_valore_atteso


dw_tes_registro_tarature_lista.set_dw_key("cod_azienda")
//dw_tes_registro_tarature_lista.set_dw_key("cod_attrezzatura")
dw_tes_registro_tarature_lista.set_dw_key("anno_registrazione")
dw_tes_registro_tarature_lista.set_dw_key("num_registrazione")
//dw_tes_registro_tarature_lista.set_dw_key("prog_riga_manutenzione")
dw_tes_registro_tarature_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_tes_registro_tarature_det.set_dw_options(sqlca, dw_tes_registro_tarature_lista, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_tes_registro_tarature_lista

ld_valore_atteso = s_cs_xx.parametri.parametro_d_1
//ld_valore_atteso = dw_tes_registro_tarature_lista.i_parentdw.getitemnumber(dw_tes_registro_tarature_lista.i_parentdw.i_selectedrows[1],"valore_atteso")
if ld_valore_atteso = 0 or isnull(ld_valore_atteso) then
	g_mb.messagebox("OMNIA","Il valore atteso non risulta impostato",Information!)
end if
end event

on w_tes_registro_tarature.create
int iCurrent
call super::create
this.cb_test_rip=create cb_test_rip
this.cb_elimina_test=create cb_elimina_test
this.cb_procedure=create cb_procedure
this.dw_tes_registro_tarature_lista=create dw_tes_registro_tarature_lista
this.dw_tes_registro_tarature_det=create dw_tes_registro_tarature_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_test_rip
this.Control[iCurrent+2]=this.cb_elimina_test
this.Control[iCurrent+3]=this.cb_procedure
this.Control[iCurrent+4]=this.dw_tes_registro_tarature_lista
this.Control[iCurrent+5]=this.dw_tes_registro_tarature_det
end on

on w_tes_registro_tarature.destroy
call super::destroy
destroy(this.cb_test_rip)
destroy(this.cb_elimina_test)
destroy(this.cb_procedure)
destroy(this.dw_tes_registro_tarature_lista)
destroy(this.dw_tes_registro_tarature_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_registro_tarature_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_registro_tarature_det,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1",&
                 "(anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                 "((anag_fornitori.flag_blocco <> 'S') or (anag_fornitori.flag_blocco = 'S' and anag_fornitori.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_test_rip from commandbutton within w_tes_registro_tarature
integer x = 2263
integer y = 120
integer width = 370
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Test Ripetib."
end type

event clicked;string ls_cod_attrezzatura
long ll_i, ll_num_ripetizioni, ll_num_registrazione, ll_prog_riga_manutenzione, &
     ll_anno_reg_taratura, ll_num_reg_taratura, ll_num_righe, ll_NewSelections[], &
	  ll_righe, ll_anno_registrazione
double ld_media_test, ld_errore_assoluto, ld_errore_relativo, ld_somma_valori, ld_valore_atteso,&
	  ld_sommatoria, ld_num
datastore lds_valori_test


if not(dw_tes_registro_tarature_lista.getrow() > 0) then return

ll_num_ripetizioni = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"num_ripetizioni")
if ll_num_ripetizioni < 1 then
	g_mb.messagebox("OMNIA","Impostare il numero di ripetizioni del Test",StopSign!)
	return
end if	

ld_valore_atteso = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"valore_rilevato")
if ld_valore_atteso = 0 then
	g_mb.messagebox("OMNIA","Impostare il valore atteso del test di ripetibilità",StopSign!)
	return
end if	

//ls_cod_attrezzatura =  dw_tes_registro_tarature_lista.getitemstring(dw_tes_registro_tarature_lista.getrow(),"cod_attrezzatura")
ll_anno_registrazione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"num_registrazione")
//ll_prog_riga_manutenzione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"prog_riga_manutenzione")
ll_anno_reg_taratura = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"anno_reg_taratura")
ll_num_reg_taratura =dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"num_reg_taratura")

select count(valore_rilevato)
into   :ll_num_righe
from   det_registro_tarature
where  det_registro_tarature.cod_azienda            = :s_cs_xx.cod_azienda and
		 det_registro_tarature.anno_registrazione     = :ll_anno_registrazione and
		 det_registro_tarature.num_registrazione      = :ll_num_registrazione and
		 det_registro_tarature.anno_reg_taratura      = :ll_anno_reg_taratura and
		 det_registro_tarature.num_reg_taratura       = :ll_num_reg_taratura;

if ll_num_righe = 0 or isnull(ll_num_righe) then
	commit;

	for ll_i = 1 to ll_num_ripetizioni
		INSERT INTO det_registro_tarature
					 ( cod_azienda,   
						anno_registrazione,   
						num_registrazione,   
						anno_reg_taratura,   
						num_reg_taratura,   
						prog_riga_taratura,   
						valore_rilevato )  
		values 	 ( :s_cs_xx.cod_azienda,   
						:ll_anno_registrazione,   
						:ll_num_registrazione,   
						:ll_anno_reg_taratura,   
						:ll_num_reg_taratura,   
						:ll_i,   
						null )  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore durante la generazione records test di ripetibilità",StopSign!)
			rollback;
			return
		end if
	next
	commit;
end if

window_open_parm(w_det_registro_tarature, 0, dw_tes_registro_tarature_lista)

select count(valore_rilevato),
		 sum(valore_rilevato)
into   :ll_num_righe, :ld_somma_valori
from   det_registro_tarature
where  det_registro_tarature.cod_azienda            = :s_cs_xx.cod_azienda and
		 det_registro_tarature.anno_registrazione     = :ll_anno_registrazione and
		 det_registro_tarature.num_registrazione      = :ll_num_registrazione and
		 det_registro_tarature.anno_reg_taratura      = :ll_anno_reg_taratura and
		 det_registro_tarature.num_reg_taratura       = :ll_num_reg_taratura and
		 det_registro_tarature.valore_rilevato is not null;


if (ll_num_righe <> 0) and (ld_valore_atteso <> 0) then

	ld_media_test = round(ld_somma_valori / ll_num_righe, 6)
	ld_media_test = round(ld_media_test, 4)

//	ls_cod_attrezzatura = dw_tes_registro_tarature_lista.getitemstring(dw_tes_registro_tarature_lista.getrow(),"cod_attrezzatura")
	ll_num_registrazione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"num_registrazione")
	ll_anno_registrazione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"anno_registrazione")	
//	ll_prog_riga_manutenzione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"prog_riga_manutenzione")
	ll_anno_reg_taratura = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"anno_reg_taratura")
	ll_num_reg_taratura = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"num_reg_taratura")

	lds_valori_test = Create DataStore
	lds_valori_test.DataObject = "d_ds_det_registro_tarature"
	lds_valori_test.SetTransObject(sqlca)
	
	ll_righe =  lds_valori_test.Retrieve(s_cs_xx.cod_azienda, &
													 ll_anno_registrazione, &
													 ll_num_registrazione, &
													 ll_anno_reg_taratura, &
													 ll_num_reg_taratura)
	ld_sommatoria = 0
	for ll_i = 1 to ll_righe
		ld_num = lds_valori_test.object.valore_rilevato[ll_i]
		ld_num = round(ld_num, 4)
		ld_sommatoria = ld_sommatoria + ( abs( ld_num - ld_media_test) * abs( ld_num - ld_media_test) )
	next
	ld_sommatoria = ld_sommatoria / ll_num_righe            //  ottengo la viarianza rispetto alla media
	ld_sommatoria = round(ld_sommatoria, 4)

	update tes_registro_tarature
	set    media_test_rip = :ld_media_test,
			 varianza_media = :ld_sommatoria
	where  tes_registro_tarature.cod_azienda            = :s_cs_xx.cod_azienda and
			 tes_registro_tarature.anno_registrazione     = :ll_anno_registrazione and
			 tes_registro_tarature.num_registrazione      = :ll_num_registrazione and
			 tes_registro_tarature.anno_reg_taratura      = :ll_anno_reg_taratura and
			 tes_registro_tarature.num_reg_taratura       = :ll_num_reg_taratura ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante aggiornamento medie del test di ripetibilità: verificare",Information!)
	end if
else
	g_mb.messagebox("OMNIA","Non vi sono valori nel test di ripetibilità oppure il valore atteso è a zero: media valori, errore relativo e assoluto non impostati", Information!)
end if

ll_newselections[1] = dw_tes_registro_tarature_lista.getrow()

dw_tes_registro_tarature_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_tes_registro_tarature_lista.Set_Selected_Rows(1, ll_NewSelections[], &
                                   c_CheckForChanges, &
                                   c_RefreshChildren, &
                                   c_RefreshView)


end event

event getfocus;call super::getfocus;dw_tes_registro_tarature_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type cb_elimina_test from commandbutton within w_tes_registro_tarature
event clicked pbm_bnclicked
integer x = 2263
integer y = 220
integer width = 370
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Test"
end type

event clicked;long ll_i, ll_num_ripetizioni, ll_num_registrazione, ll_prog_riga_manutenzione, &
     ll_anno_reg_taratura, ll_num_reg_taratura, ll_ritorno, ll_newselections[], ll_anno_registrazione
	  

ll_ritorno = g_mb.messagebox("OMNIA","Sei sicuro di voler annullare il test precedentemente eseguito?",Question!, YesNo!,2)
if ll_ritorno = 2 then return

if dw_tes_registro_tarature_lista.getrow()  > 0 then

//	ls_cod_attrezzatura =  dw_tes_registro_tarature_lista.getitemstring(dw_tes_registro_tarature_lista.getrow(),"cod_attrezzatura")
	ll_num_registrazione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"num_registrazione")
	ll_anno_registrazione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"anno_registrazione")	
//	ll_prog_riga_manutenzione = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"prog_riga_manutenzione")
   ll_anno_reg_taratura = dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"anno_reg_taratura")
	ll_num_reg_taratura =dw_tes_registro_tarature_lista.getitemnumber(dw_tes_registro_tarature_lista.getrow(),"num_reg_taratura")
	
	commit;

	delete from det_registro_tarature
	where det_registro_tarature.cod_azienda = :s_cs_xx.cod_azienda and
	      det_registro_tarature.anno_registrazione = :ll_anno_registrazione and
	      det_registro_tarature.num_registrazione = :ll_num_registrazione and
	      det_registro_tarature.anno_reg_taratura = :ll_anno_reg_taratura and
	      det_registro_tarature.num_reg_taratura = :ll_num_reg_taratura;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante la cancellazione records test di ripetibilità",StopSign!)
		rollback;
		return
	end if

	update tes_registro_tarature
	set    media_test_rip = 0,
			 varianza_media = 0
	where  tes_registro_tarature.cod_azienda            = :s_cs_xx.cod_azienda and
			 tes_registro_tarature.anno_registrazione     = :ll_anno_registrazione and
			 tes_registro_tarature.num_registrazione      = :ll_num_registrazione and
			 tes_registro_tarature.anno_reg_taratura      = :ll_anno_reg_taratura and
			 tes_registro_tarature.num_reg_taratura       = :ll_num_reg_taratura ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore durante aggiornamento medie del test di ripetibilità: verificare",Information!)
	end if

	ll_newselections[1] = dw_tes_registro_tarature_lista.getrow()
	
	dw_tes_registro_tarature_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	dw_tes_registro_tarature_lista.Set_Selected_Rows(1, ll_NewSelections[], &
												  c_CheckForChanges, &
												  c_RefreshChildren, &
												  c_RefreshView)

	commit;
end if

end event

type cb_procedure from cb_apri_manuale within w_tes_registro_tarature
event clicked pbm_bnclicked
integer x = 2263
integer y = 20
integer height = 80
integer taborder = 21
string text = "&Procedure"
end type

event clicked;call super::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLR")
end event

type dw_tes_registro_tarature_lista from uo_cs_xx_dw within w_tes_registro_tarature
integer x = 23
integer y = 20
integer width = 2217
integer height = 500
integer taborder = 40
string dataobject = "d_tes_registro_tarature_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

//l_Error = Retrieve(s_cs_xx.cod_azienda, &
//						 i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"anno_attrezzatura"), &
//						 i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_registrazione"))

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 s_cs_xx.parametri.parametro_d_2, &
						 s_cs_xx.parametri.parametro_d_3)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long  l_Idx, ll_num_registrazione, ll_anno_registrazione


//ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_attrezzatura")
//ll_prog_riga_manutenzione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_riga_manutenzione")
//ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_registrazione")
//ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_registrazione")
ll_anno_registrazione = s_cs_xx.parametri.parametro_d_2
ll_num_registrazione = s_cs_xx.parametri.parametro_d_3

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) then
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

//FOR l_Idx = 1 TO RowCount()
//   IF IsNull(GetItemstring(l_Idx, "cod_attrezzatura")) then
//      SetItem(l_Idx, "cod_attrezzatura", ls_cod_attrezzatura)
//   END IF
//NEXT

//FOR l_Idx = 1 TO RowCount()
//   IF isnull(GetItemnumber(l_Idx, "prog_riga_manutenzione")) then
//      SetItem(l_Idx, "prog_riga_manutenzione", ll_prog_riga_manutenzione)
//   END IF
//NEXT

FOR l_Idx = 1 TO RowCount()
   IF isnull(GetItemnumber(l_Idx, "num_registrazione")) then
      SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF isnull(GetItemnumber(l_Idx, "anno_registrazione")) then
      SetItem(l_Idx, "anno_registrazione", ll_anno_registrazione)
   END IF
NEXT
end event

event updatestart;call super::updatestart;string ls_cod_attrezzatura
long ll_errore_assoluto, ll_errore_relativo, ll_anno_reg, ll_num_reg ,ll_num_registrazione, ll_anno_esercizio


if ib_new then
	
//	ls_cod_attrezzatura = this.GetItemstring(this.GetRow ( ), "cod_attrezzatura")
	ll_anno_reg = this.GetItemnumber(this.GetRow ( ), "anno_registrazione")
	ll_num_reg = this.GetItemnumber(this.GetRow ( ), "num_registrazione")
	ll_anno_esercizio = f_anno_esercizio()
   this.SetItem (this.GetRow(),"anno_reg_taratura", ll_anno_esercizio)

   select  max(tes_registro_tarature.num_reg_taratura)
   into    :ll_num_registrazione
   from    tes_registro_tarature
   where   (tes_registro_tarature.cod_azienda = :s_cs_xx.cod_azienda) and
	  		  (tes_registro_tarature.anno_registrazione = :ll_anno_reg) and
	  		  (tes_registro_tarature.num_registrazione = :ll_num_reg) and
			  (tes_registro_tarature.anno_reg_taratura = :ll_anno_esercizio);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow(),"num_reg_taratura", ll_num_registrazione)
   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_taratura", 1)
   end if
   ib_new = false
end if




end event

event pcd_new;call super::pcd_new;double ld_valore_atteso

ib_new = true
cb_elimina_test.enabled = false
dw_tes_registro_tarature_det.object.b_ricerca_fornitore.enabled = true
cb_test_rip.enabled = false
//ld_valore_atteso = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"valore_atteso")
ld_valore_atteso = s_cs_xx.parametri.parametro_d_1
this.setitem(this.getrow(),"valore_atteso", ld_valore_atteso)
this.setitem(this.getrow(),"anno_registrazione", s_cs_xx.parametri.parametro_d_2)
this.setitem(this.getrow(),"num_registrazione", s_cs_xx.parametri.parametro_d_3)
end event

event pcd_modify;call super::pcd_modify;cb_elimina_test.enabled = false
dw_tes_registro_tarature_det.object.b_ricerca_fornitore.enabled = true
cb_test_rip.enabled = false
end event

event pcd_view;call super::pcd_view;cb_elimina_test.enabled = true
dw_tes_registro_tarature_det.object.b_ricerca_fornitore.enabled = false
cb_test_rip.enabled = true
end event

type dw_tes_registro_tarature_det from uo_cs_xx_dw within w_tes_registro_tarature
integer x = 23
integer y = 540
integer width = 2606
integer height = 1300
integer taborder = 50
string dataobject = "d_tes_registro_tarature_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;double ld_errore_assoluto, ld_errore_relativo, ld_valore_rilevato, ld_valore_atteso


ld_valore_atteso = s_cs_xx.parametri.parametro_d_1
//ld_valore_atteso = dw_tes_registro_tarature_lista.i_parentdw.getitemnumber(dw_tes_registro_tarature_lista.i_parentdw.i_selectedrows[1],"valore_atteso")
choose case i_colname
	case "valore_rilevato"
		if ld_valore_atteso <> 0 then
			ld_valore_rilevato = f_string_double(i_coltext)
			ld_errore_assoluto = ld_valore_rilevato - ld_valore_atteso
			ld_errore_relativo = (ld_valore_rilevato - ld_valore_atteso) / ld_valore_atteso
			ld_errore_assoluto = round(ld_errore_assoluto, 4)
			ld_errore_relativo = round(ld_errore_relativo, 4)
			setitem(getrow(),"errore_assoluto", ld_errore_assoluto)
			setitem(getrow(),"errore_relativo", ld_errore_relativo)
		end if	
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_registro_tarature_det,"cod_fornitore")
end choose
end event

