﻿$PBExportHeader$uo_service_report_man.sru
$PBExportComments$Servizio di Produzione automatico report
forward
global type uo_service_report_man from uo_service_base
end type
end forward

global type uo_service_report_man from uo_service_base
end type
global uo_service_report_man uo_service_report_man

type variables
PRIVATE:
	boolean ib_test=false
	string		is_from, is_sender_name, is_smtp_server, is_smtp_usd, is_smtp_pwd
	
	string		is_path_file

end variables

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
public function integer uof_report_manut_excel (string as_cod_cliente, string as_cod_operaio, string as_cod_divisione, datetime adt_data_inizio, datetime adt_data_fine, ref string as_errore)
public function integer uof_send_mail (string as_cod_utente, string as_email_utente)
public function integer uof_report_scadenze_man (string as_cod_cliente, string as_cod_operaio, string as_cod_divisione, datetime adt_data_inizio, datetime adt_data_fine, ref string as_errore)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);/*##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=report_man,<codazienda>,<TEST>,<verbosità>
																			dove
																					1.<codazienda>					: valore per inizializzare s_cs_xx.cod_azienda
																					7.<test S/N>						: normalmente a N (se S le email vanno tutte ad assistenza@csteam.com)
																					8.<verbosità> 					: numero che indica la verbosità del log (min 1, max 5, default 2)
esempio 		cs_team.exe S=report_man,A01,N,3
esempio 		cs_team.exe S=report_man,A01,N,3
				log scritto nella cartella logs delle risorse

Esegue il report richiesto nella tabella BUFFER_REPORT
*/

boolean					lb_scadute, lb_in_scadenza
string						ls_test, ls_sql, ls_errore, ls_null, ls_parametri[], ls_cod_cliente, ls_cod_operaio, ls_cod_divisione, ls_null_array[], &
							ls_cod_utente
integer					li_param
long						ll_ret, ll_i, ll_id
datetime					ldt_data_inizio, ldt_data_fine, ldt_data_invio, ldt_ora_invio
datastore				lds_data

iuo_log.set_date_time_format("dd/mm/yyyy hh:mm:ss")
iuo_log.warn("####### INIZIO PROCESSO INVIO REPORT RICHIESTI  ##########################")

//-----------------------------------------------------------------------------------------------------------------------------------------------------
li_param = 0

//1) imposto l'azienda
li_param += 1
uof_set_s_cs_xx(auo_params.uof_get_string(li_param,"A01"))

//2) vedo se devo notificare all'amministratore tipi manutenzione senza lista distribuzione
li_param += 1
ls_test = auo_params.uof_get_string(li_param,"N")
if ls_test="S" then ib_test=true

ls_sql = " select id, nome_report, cod_utente_richiedente, email_richiedente, parametri_report from buffer_report where flag_evaso='N' order by 1"
ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)

if ll_ret < 0 then
	iuo_log.error( g_str.format("Errore in preparazione Datastore:$1",ls_errore))
	return -1
end if
	

for ll_i = 1 to ll_ret
	ls_parametri[] = ls_null_array[]
	
	ll_id = lds_data.getitemnumber(ll_i,1)
	
	iuo_log.warn(g_str.format("ID=$1  Parametri:$2",lds_data.getitemnumber(ll_i,1), lds_data.getitemstring(ll_i,5) ))
	iuo_log.warn(g_str.format("Utente Richiedente:$1 - Email Utente:$2", lds_data.getitemstring(ll_i,3),lds_data.getitemstring(ll_i,4) ))
	
	uof_set_cod_utente(lds_data.getitemstring(ll_i,3) )
	
	if isnull(lds_data.getitemstring(ll_i,5)) then 
		iuo_log.warn(g_str.format("La richiesta di report con ID=$1 non presenta alcun parametro e verrà saltata",lds_data.getitemnumber(ll_i,1)))
		continue
	end if
	
	guo_functions.uof_explode(lds_data.getitemstring(ll_i,5), ",", ls_parametri[])
	
	if upperbound(ls_parametri[]) < 1 then
		iuo_log.warn(g_str.format("La richiesta di report con ID=$1 non presenta alcun parametro e verrà saltata",lds_data.getitemnumber(ll_i,1)))
		continue
	end if
	
	setnull(ls_null)
	ls_cod_cliente = ls_parametri[2]
	if len(trim(ls_cod_cliente)) <= 0 then setnull(ls_cod_cliente)
	iuo_log.warn(g_str.format("La richiesta di report con ID=$1 filtro cliente=$2",lds_data.getitemnumber(ll_i,1), ls_cod_cliente))
		
	ls_cod_operaio =  ls_parametri[3]
	if len(trim(ls_cod_operaio)) <= 0 then setnull(ls_cod_operaio)
	iuo_log.warn(g_str.format("La richiesta di report con ID=$1 filtro operaio=$2",lds_data.getitemnumber(ll_i,1),ls_cod_operaio))

	ls_cod_divisione = ls_parametri[4]
	if len(trim(ls_cod_divisione)) <= 0 then setnull(ls_cod_divisione)
	iuo_log.warn(g_str.format("La richiesta di report con ID=$1 filtro divisione/commessa=$2",lds_data.getitemnumber(ll_i,1),ls_cod_divisione))

	ldt_data_inizio = datetime(date(ls_parametri[5]),00:00:00)
	iuo_log.warn(g_str.format("La richiesta di report con ID=$1 filtro data_inizio>=$2",lds_data.getitemnumber(ll_i,1),ldt_data_inizio))
	
	ldt_data_fine = datetime(date(ls_parametri[6]),00:00:00) 
	iuo_log.warn(g_str.format("La richiesta di report con ID=$1 filtro data_fine<=$2",lds_data.getitemnumber(ll_i,1),ldt_data_fine))

	is_path_file = ""

	choose case lds_data.getitemstring(ll_i,2)
		case "w_report_manut_excel"
			uof_report_manut_excel(ls_cod_cliente, ls_cod_operaio, ls_cod_divisione, ldt_data_inizio, ldt_data_fine, ls_errore)
			ls_cod_utente = lds_data.getitemstring(ll_i,3)
	
			// invio il file tramite mail
			iuo_log.warn(g_str.format("Invio Mail all'utente $1 ",ls_cod_utente))
			is_path_file = is_path_file + ".xlsx"
			iuo_log.warn(g_str.format("File Excel con percorso in $1 ",is_path_file))
			uof_send_mail(ls_cod_utente, lds_data.getitemstring(ll_i,4))
			
		case "w_report_manut_scad"
			uof_report_scadenze_man(ls_cod_cliente, ls_cod_operaio, ls_cod_divisione, ldt_data_inizio, ldt_data_fine, ls_errore)
			ls_cod_utente = lds_data.getitemstring(ll_i,3)
			
			// invio il file tramite mail
			iuo_log.warn(g_str.format("Invio Mail all'utente $1 ",ls_cod_utente))
			iuo_log.warn(g_str.format("File PDF con percorso in $1 ",is_path_file))
			uof_send_mail(ls_cod_utente, lds_data.getitemstring(ll_i,4))
			
		case else
			iuo_log.error(g_str.format("Report $1 richiesto dall'utente, non previsto dal sistema!",lds_data.getitemstring(ll_i,2), ls_cod_utente))
			
	end choose
	
	ldt_data_invio = datetime(today(),00:00:00)
	ldt_ora_invio = datetime(date("01/01/1900"),now())
	
	update buffer_report
	set 		data_invio = :ldt_data_invio,
				ora_invio = :ldt_ora_invio,
				flag_evaso = 'S'
	where id = :ll_id;
	
	if sqlca.sqlcode = 0 then
		iuo_log.warn(g_str.format("Richiesta Report ID= $1 correttamente evasa",ll_id))
		commit;
	else
		iuo_log.error(g_str.format("Richiesta Report ID= $1 Errore in fase di UPDATE flag_evasione su tabella buffer_report. $2",ll_id, sqlca.sqlerrtext))
		rollback;
	end if		
	
	iuo_log.warn("####### TERMINE PROCESSO INVIO REPORT RICHIESTI ##########################")

next

return 0
end function

public function integer uof_report_manut_excel (string as_cod_cliente, string as_cod_operaio, string as_cod_divisione, datetime adt_data_inizio, datetime adt_data_fine, ref string as_errore);string 		ls_sql, ls_des_divisione,  ls_error, ls_operaio_old, ls_sheetname, ls_test, ls_data_intervento
long			ll_i, ll_ret, ll_riga, ll_colonna, ll_giorno, ll_ora_intera, ll_riga_fine, ll_riga_inizio, ll_minuti_fine, ll_minuti_interi
datetime 	ldt_data_old, ldt_ora_inizio, ldt_ora_fine_intervento
datastore 	lds_data
uo_excel		luo_excel


ls_sql = "select B.data_inizio data_inizio, B.ora_inizio ora_inizio , B.cod_operaio cod_operaio, O.nome nome , O.cognome cognome , A.cod_attrezzatura cod_attrezzatura, C.cod_reparto cod_reparto, U.cod_divisione cod_divisione, D.cod_cliente cod_cliente, D.des_divisione des_divisione, A.anno_registrazione anno_registrazione, A.num_registrazione num_registrazione, B.data_fine data_fine, B.ora_fine ora_fine   &
from manutenzioni A &
left join manutenzioni_fasi_operai B on A.cod_azienda=B.cod_azienda and A.anno_registrazione=B.anno_registrazione  and A.num_registrazione=B.num_registrazione &
left join anag_attrezzature C on A.cod_azienda=C.cod_azienda and A.cod_attrezzatura = C.cod_attrezzatura &
left join tab_aree_aziendali U on C.cod_azienda = U.cod_azienda and C.cod_area_aziendale=U.cod_area_aziendale &
left join anag_divisioni D on U.cod_azienda = D.cod_azienda and U.cod_divisione=D.cod_divisione &
left join anag_operai O on B.cod_azienda=O.cod_azienda and B.cod_operaio=O.cod_operaio &
where A.flag_eseguito = 'S' "


if not isnull(as_cod_cliente) then
	ls_sql += " and cod_cliente = '" + as_cod_cliente + "' "
end if

if not isnull(as_cod_operaio) then
	ls_sql += " and B.cod_operaio = '" + as_cod_operaio + "' "
end if

if not isnull(as_cod_divisione) then
	ls_sql += " and U.cod_divisione = '" + as_cod_divisione + "' "
end if

if not isnull(adt_data_inizio) then
	ls_sql += " and data_inizio >= '" + string(adt_data_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(adt_data_fine) then
	ls_sql += " and data_inizio <= '" + string(adt_data_fine,s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += " order by data_inizio, cod_operaio, cod_divisione "

iuo_log.warn( g_str.format("SQL REPORT:$1 ",ls_sql) )

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_error)

if ll_ret = 0 then 	iuo_log.warn( "Nessun risultato dalla query del report" )

if ll_ret < 0 then
	as_errore = "Errore rin creazione datastore~r~n " + ls_error
	iuo_log.warn( g_str.format("Errore rin creazione datastore:$1 ",ls_error) )
	return -1
end if

// preparo il foglio Excel
luo_excel = CREATE uo_excel

is_path_file =  guo_functions.uof_get_user_documents_folder( ) + guo_functions.uof_get_random_filename( )

luo_excel.uof_create(is_path_file, false)
ls_sheetname = "Report Attività"
luo_excel.uof_add_sheet(ls_sheetname, true)
luo_excel.uof_set_sheet(ls_sheetname)

luo_excel.uof_disable_warning_messages()

luo_excel.uof_set(1, 1, "DATA")
luo_excel.uof_set_bold( 1,1)
luo_excel.uof_set(2, 1, "GIORNO")
luo_excel.uof_set_bold( 2,1)
luo_excel.uof_set(3, 1, "OPERAIO")
luo_excel.uof_set_bold( 3,1)

ll_riga = 4
for ll_i = 0 to 23
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:00",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:15",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:30",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:45",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
next
	
ll_riga = 1
ll_colonna = 1

ldt_data_old = datetime(date("01/01/1900"),00:00:00)
ls_operaio_old = ""

for ll_i = 1 to ll_ret
	if isnull(lds_data.getitemdatetime(ll_i, 1)) then continue
	
	if ldt_data_old <> lds_data.getitemdatetime(ll_i, 1) or ls_operaio_old <> lds_data.getitemstring(ll_i, 3) then 
		ll_colonna ++
		if mod(ll_colonna,26)=0 then ll_colonna ++
		ldt_data_old = lds_data.getitemdatetime(ll_i, 1)
		ls_operaio_old = lds_data.getitemstring(ll_i, 3)
		ls_data_intervento =  string(lds_data.getitemdatetime(ll_i, 1),"dd/mm/yy")
		luo_excel.uof_set(1, ll_colonna, ls_data_intervento)
		luo_excel.uof_set_autofit( 1, ll_colonna)
		ll_giorno = guo_functions.uof_get_day_number_ita( date(lds_data.getitemdatetime(ll_i, 1)) )
		luo_excel.uof_set(2, ll_colonna, guo_functions.uof_get_day_name_ita( ll_giorno ) )
		luo_excel.uof_set(3, ll_colonna, g_str.format("$1 $2", lds_data.getitemstring(ll_i, 5),lds_data.getitemstring(ll_i, 4) ) )
	end if		
	
	ldt_ora_inizio = lds_data.getitemdatetime(ll_i, 2)
	if isnull(ldt_ora_inizio) then continue
	
	ls_test = string( time( ldt_ora_inizio))
	ll_ora_intera = long( left(ls_test,2))
	ll_minuti_interi = long( mid(ls_test,4,2))
	ll_riga_inizio = 4 + (ll_ora_intera * 4) + 1
	choose case ll_minuti_interi
		case  15 to 29
			ll_riga_inizio ++
		case  30 to 44
			ll_riga_inizio ++
		case  45 to 59
			ll_riga_inizio ++
	end choose		
	
	// leggo data e ora fine
	ldt_ora_fine_intervento = lds_data.getitemdatetime(ll_i, 14)
	if isnull(ldt_ora_fine_intervento) then ldt_ora_fine_intervento = ldt_ora_inizio

	ls_test = string( time( ldt_ora_fine_intervento     )  )
	ll_ora_intera = long( left(ls_test,2) )   // prendo le ore
	ll_minuti_fine = long( mid(ls_test,4,2)) // prendo i minuti
	ll_riga_fine = 4 + (ll_ora_intera * 4) + 1
	choose case ll_minuti_fine
		case 15 to 29
			ll_riga_fine ++
		case 30 to 44
			ll_riga_fine ++
		case 45 to 59
			ll_riga_fine ++
		case else
			// se i minuti sono zero allora detraggo un'ora in modo che, ad esempio, se l'attività termina alle 12.00 non vado ad impegnare la cella che va dalle 12 alle 13
			ll_riga_fine --
	end choose		
	if ll_riga_fine < ll_riga_inizio then ll_riga_fine = ll_riga_inizio
	
	ls_des_divisione = g_str.format(lds_data.getitemstring(ll_i, 10) + " ($1/$2)", lds_data.getitemnumber(ll_i, 11), lds_data.getitemnumber(ll_i, 12) )
	iuo_log.warn(g_str.format("Record nr $1: $2",ll_i,ls_des_divisione ))
	if isnull(ls_des_divisione) or len(ls_des_divisione) < 1 then ls_des_divisione = g_str.format("Commessa non indicata ($1/$2)",lds_data.getitemnumber(ll_i, 11),lds_data.getitemnumber(ll_i, 12) )

	luo_excel.uof_set(ll_riga_inizio, ll_colonna, ls_des_divisione )
	luo_excel.uof_set_wrap_text(ll_riga_inizio, ll_colonna)
/*
	for ll_riga = ll_riga_inizio to ll_riga_fine
		luo_excel.uof_set(ll_riga, ll_colonna, ls_des_divisione )
		luo_excel.uof_set_wrap_text(ll_riga, ll_colonna)
		luo_excel.uof_set_horizontal_align( ll_riga, ll_colonna, luo_excel.TEXT_ALIGN_MIDDLE)
	next	
	*/
	luo_excel.uof_merge( ll_riga_inizio,ll_colonna, ll_riga_fine, ll_colonna)
	luo_excel.uof_set_horizontal_align( ll_riga_inizio, ll_colonna, luo_excel.TEXT_ALIGN_MIDDLE)
	luo_excel.uof_set_border( ll_riga_inizio,ll_colonna, ll_riga_fine, ll_colonna,2,2, 2,2)
	iuo_log.warn( g_str.format("Applicazione del bordo riga/colonna inizio $1/$2 - riga colonna fine $3,$4 ",ll_riga_inizio,ll_colonna, ll_riga_fine, ll_colonna) )
	
next

luo_excel.uof_set_rows_height(5,100,15)


destroy luo_excel

return 0
end function

public function integer uof_send_mail (string as_cod_utente, string as_email_utente);boolean 		lb_result
string			ls_destinatari[], ls_error, ls_attachment[]
integer		li_ret

//recupero info principali per inviare email (sender, smtp, user_smtp, pwd_smtp)
SELECT		e_mail,
				sender_name,
				smtp_server,
				smtp_usr,
				smtp_pwd
into		:is_from,
			:is_sender_name,
			:is_smtp_server,
			:is_smtp_usd,
			:is_smtp_pwd
from utenti
where cod_utente=:as_cod_utente;

if sqlca.sqlcode < 0 then
	//errore
	iuo_log.error("Errore in lettura valori SMTP in tabella utenti: "+sqlca.sqlerrtext + " PROCESSO TERMINATO")
	return -1
	
elseif sqlca.sqlcode = 100 then
	//utente non trovato in tabella
	iuo_log.error("Utente con codice '"+s_cs_xx.cod_utente+"' non trovato in tabella utenti!  PROCESSO TERMINATO")
	return -1
end if

if isnull(is_from) or is_from="" then
	iuo_log.error("E-mail non specificata per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return -1
end if

if isnull(is_smtp_server) or is_smtp_server="" then
	iuo_log.error("Indirizzo server SMTP non specificato per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return -1
end if

if isnull(is_sender_name) then is_sender_name = "<Sender>"
if isnull(is_smtp_usd) then is_smtp_usd = ""
if isnull(is_smtp_pwd) then is_smtp_pwd = ""

iuo_log.info("Letti valori SMTP per l'utente '"+s_cs_xx.cod_utente+"'")
iuo_log.info("E-mail mittente: "+is_from + "   Sender mittente: "+is_sender_name + "  Server SMTP: "+is_smtp_server + "  Utente SMTP: "+is_smtp_usd+ "  Password SMTP: "+is_smtp_pwd)
//------------------------------------------------------------------------------------

//controllo che l'utente sia esistente e che abbia valori che servono impostati
if s_cs_xx.cod_utente = "" or isnull(s_cs_xx.cod_utente) then
	iuo_log.error("Codice Utente non impostato nei parametri del processo! PROCESSO TERMINATO")
	return -1
end if


uo_sendmail luo_sendmail
luo_sendmail = create uo_sendmail

luo_sendmail.uof_set_from(is_from)
if ib_test then
	ls_destinatari[1] = "assistenza@csteam.com"
else
	ls_destinatari[1] = as_email_utente
end if
luo_sendmail.uof_set_to(ls_destinatari[])
//luo_sendmail.uof_set_cc(as_cc[])
//luo_sendmail.uof_set_bcc(as_bcc[])
ls_attachment[1]=is_path_file
luo_sendmail.uof_set_attachments(ls_attachment[])
luo_sendmail.uof_set_subject("Invio Report")
luo_sendmail.uof_set_message("Invio automatizzato Report richiesto da App Manutenzioni")
luo_sendmail.uof_set_smtp(is_smtp_server, is_smtp_usd, is_smtp_pwd)
luo_sendmail.uof_set_html( true )

lb_result = luo_sendmail.uof_send()
ls_error = luo_sendmail.uof_get_error()

if lb_result then
	li_ret = 0
	iuo_log.warn(g_str.format("Mail inviata correttamente all'indirizzo $1, con file $2", ls_destinatari[1],ls_attachment[1]))
else
	li_ret=-1
	iuo_log.error(g_str.format("Errore invio Mail all'indirizzo $1. Dettaglio errore: $2", ls_destinatari[1],ls_error))
end if
return li_ret
end function

public function integer uof_report_scadenze_man (string as_cod_cliente, string as_cod_operaio, string as_cod_divisione, datetime adt_data_inizio, datetime adt_data_fine, ref string as_errore);long     	ll_riga, ll_anno_registrazione, ll_num_registrazione, ll_count, ll_prog_fase_operaio, &
        		ll_prog_fase_risorsa, ll_n_operai, ll_index, ll_i, ll_ret

datetime	ldt_scadenza, ldt_da, ldt_a

string   	ls_reparto, ls_tipo, ls_cod_att, ls_des_att, ls_cod_tipo, ls_des_tipo, ls_operaio, &
			ls_cognome, ls_nome, ls_selezione, ls_sql, ls_flag, ls_cod_operaio, &
			ls_des_operaio, ls_null, ls_cod_operaio_sel, ls_divisione, ls_file, ls_errore
string	ls_tipo_ordinamento
datastore dw_scadenze_manutenzioni
datastore lds_data

dw_scadenze_manutenzioni = create datastore
dw_scadenze_manutenzioni.dataobject='d_scadenze_manutenzioni'

setnull(ls_null)

ls_selezione = ""

ldt_da = adt_data_inizio

if not isnull(ldt_da)  then
	ls_selezione += "Dal " + string(ldt_da,"dd/mm/yyyy")
end if

ldt_a = adt_data_fine

if not isnull(ldt_a)  then
	ls_selezione += " Al " + string(ldt_a,"dd/mm/yyyy")
end if

//dw_scadenze_manutenzioni.object.st_selezione.text = ls_selezione

ls_divisione = as_cod_divisione
if ls_divisione = "" then setnull(ls_divisione)

ls_cod_operaio_sel = as_cod_operaio

if ls_cod_operaio_sel = "" then setnull(ls_cod_operaio_sel)

// *** fine modifica

ls_sql = " select  manutenzioni.data_registrazione,  manutenzioni.cod_attrezzatura, manutenzioni.cod_tipo_manutenzione, manutenzioni.anno_registrazione, manutenzioni.num_registrazione from manutenzioni "

// aggiunto per nuova gestione richieste su tabella unica enme 17/6/2006
ll_index = 0

ls_sql += "where manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_eseguito = 'N' "

if not isnull(ldt_da) and string( ldt_da, "dd/mm/yyyy") <> "01/01/1900" then
	ls_sql += " and manutenzioni.data_registrazione >= '" + string(ldt_da,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_a) then
	ls_sql += " and manutenzioni.data_registrazione <= '" + string(ldt_a,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if g_str.isnotempty(ls_divisione) then
	ls_sql += " and manutenzioni.cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "' and tab_aree_aziendali.cod_divisione = '" + ls_divisione + "')) "
end if

/*Donato 19-12-2008 modifica per scegliere il tipo di ordinamento (Data Scadenza -"D" o Cod.Attrezz. - "A")
if ls_tipo_ordinamento = "A" then
	ls_sql += " order by manutenzioni.cod_attrezzatura ASC, manutenzioni.data_registrazione ASC, cod_tipo_manutenzione ASC "
else
	ls_sql += " order by manutenzioni.data_registrazione ASC, manutenzioni.cod_attrezzatura ASC, cod_tipo_manutenzione ASC "
end if
ls_sql += " order by manutenzioni.data_registrazione ASC, manutenzioni.cod_attrezzatura ASC, cod_tipo_manutenzione ASC "
fine modifica ----------------------------------------------------------------------------------------------------------------
*/

iuo_log.warn(g_str.format("SQL REPORT: $1", ls_sql))
ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_ret < 0 then
	iuo_log.error(g_str.format("Errore nella open del cursore scad_man: $1",sqlca.sqlerrtext))
	return -1
end if
if ll_ret = 0 then iuo_log.warn("La Query non ha ritornato alcun risultato")

for ll_i = 1 to ll_ret
	ldt_scadenza = lds_data.getitemdatetime(ll_i,1)
	ls_cod_att = lds_data.getitemstring(ll_i,2)
	ls_cod_tipo = lds_data.getitemstring(ll_i,3)
	ll_anno_registrazione = lds_data.getitemnumber(ll_i,4)
	ll_num_registrazione = lds_data.getitemnumber(ll_i,5)

	if not isnull(ls_cod_operaio_sel) then
		ll_n_operai = 0
	
		select 	count(*) 
		into   		:ll_n_operai
		from   	manutenzioni_fasi_operai
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		       		anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and 
				 	cod_operaio = :ls_cod_operaio_sel;
				 
		if isnull(ll_n_operai) then ll_n_operai = 0
		if ll_n_operai < 1 then continue
	end if
	
	ls_des_att = ""
	ls_des_tipo = ""
	
	select descrizione
	into   :ls_des_att
	from   anag_attrezzature 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	      	 cod_attrezzatura = :ls_cod_att;
	
	select des_tipo_manutenzione
	into   :ls_des_tipo
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_att and
			 cod_tipo_manutenzione = :ls_cod_tipo;
			 
	if sqlca.sqlcode < 0 then
		iuo_log.error(g_str.format("Errore nella select di tab_tipi_manutenzione:: $1",sqlca.sqlerrtext))
		return -1
	end if
	
	ll_riga = dw_scadenze_manutenzioni.insertrow(0)
	
	dw_scadenze_manutenzioni.setitem( ll_riga, "data_registrazione", ldt_scadenza)
	dw_scadenze_manutenzioni.setitem( ll_riga, "cod_attrezzatura", ls_cod_att)
	dw_scadenze_manutenzioni.setitem( ll_riga, "descrizione", ls_des_att)
	dw_scadenze_manutenzioni.setitem( ll_riga, "cod_tipo_manutenzione", ls_cod_tipo)
	dw_scadenze_manutenzioni.setitem( ll_riga, "des_tipo_manutenzione", ls_des_tipo)
	dw_scadenze_manutenzioni.setitem( ll_riga, "anno_registrazione", ll_anno_registrazione)
	dw_scadenze_manutenzioni.setitem( ll_riga, "num_registrazione", ll_num_registrazione)	
	dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "3")			
	ll_count = 0
	
	// **** operatori
	// **** se ha selezionato un operatore ne devo tener conto ...

	iuo_log.error("Avvio cursor per lettura operatori e fasi")
	
	declare cu_operatori cursor for
	select cod_operaio,
	       prog_fase
	from   manutenzioni_fasi_operai
	where  manutenzioni_fasi_operai.cod_azienda = :s_cs_xx.cod_azienda and
	       manutenzioni_fasi_operai.anno_registrazione = :ll_anno_registrazione and
			 manutenzioni_fasi_operai.num_registrazione = :ll_num_registrazione;
	
	open cu_operatori;
	if sqlca.sqlcode < 0 then
		iuo_log.error(g_str.format("Errore nell'apertura cursore operatori: $1",sqlca.sqlerrtext))
		close cu_operatori;
		return -1
	end if

	do while true
		fetch cu_operatori into :ls_cod_operaio, :ll_prog_fase_operaio;
												  
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode < 0 then
			iuo_log.error(g_str.format("Errore nello scorrimento fetch del cursore operatori: $1",sqlca.sqlerrtext))
			close cu_operatori;
			return -1			
		end if
		
		select 	cognome + ' ' + nome
		into   		:ls_des_operaio
		from   	anag_operai 
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		       		cod_operaio = :ls_cod_operaio;
				 
		if ll_count = 0 then
			dw_scadenze_manutenzioni.setitem( ll_riga, "prog_fase", ll_prog_fase_operaio)
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_operaio", ls_cod_operaio)
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_operaio", ls_des_operaio)	
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_cat_risorse_esterne", ls_null)
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_risorsa_esterna", ls_null)	
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_risorsa", ls_null)
			dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "1")
			ll_count = ll_count + 1
		else
			ll_riga = dw_scadenze_manutenzioni.insertrow(0)
			dw_scadenze_manutenzioni.setitem(ll_riga,"data_registrazione",ldt_scadenza)
			dw_scadenze_manutenzioni.setitem(ll_riga,"cod_attrezzatura",ls_cod_att)
			dw_scadenze_manutenzioni.setitem(ll_riga,"descrizione",ls_des_att)
			dw_scadenze_manutenzioni.setitem(ll_riga,"cod_tipo_manutenzione",ls_cod_tipo)
			dw_scadenze_manutenzioni.setitem(ll_riga,"des_tipo_manutenzione",ls_des_tipo)
			dw_scadenze_manutenzioni.setitem(ll_riga,"anno_registrazione",ll_anno_registrazione)
			dw_scadenze_manutenzioni.setitem(ll_riga,"num_registrazione",ll_num_registrazione)	
			dw_scadenze_manutenzioni.setitem( ll_riga, "prog_fase", ll_prog_fase_operaio)			
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_operaio", ls_cod_operaio)
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_operaio", ls_des_operaio)
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_cat_risorse_esterne", ls_null)
			dw_scadenze_manutenzioni.setitem( ll_riga, "cod_risorsa_esterna", ls_null)	
			dw_scadenze_manutenzioni.setitem( ll_riga, "des_risorsa", ls_null)	
			dw_scadenze_manutenzioni.setitem( ll_riga, "flag_tipo_riga", "2")			
			ll_count = ll_count + 1			
		end if
	loop	
	close cu_operatori;
next

is_path_file =  guo_functions.uof_get_user_documents_folder( ) + guo_functions.uof_get_random_filename( ) + ".pdf"
ll_ret = dw_scadenze_manutenzioni.saveas( is_path_file, pdf!, false)

return 0
end function

on uo_service_report_man.create
call super::create
end on

on uo_service_report_man.destroy
call super::destroy
end on

