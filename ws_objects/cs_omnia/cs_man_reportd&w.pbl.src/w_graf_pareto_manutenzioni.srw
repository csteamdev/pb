﻿$PBExportHeader$w_graf_pareto_manutenzioni.srw
forward
global type w_graf_pareto_manutenzioni from w_graf_pareto
end type
type dw_selezione from uo_cs_xx_dw within w_graf_pareto_manutenzioni
end type
type cb_1 from commandbutton within w_graf_pareto_manutenzioni
end type
type cb_2 from commandbutton within w_graf_pareto_manutenzioni
end type
end forward

global type w_graf_pareto_manutenzioni from w_graf_pareto
integer width = 3410
integer height = 2324
event ue_imposta_default ( )
dw_selezione dw_selezione
cb_1 cb_1
cb_2 cb_2
end type
global w_graf_pareto_manutenzioni w_graf_pareto_manutenzioni

forward prototypes
public subroutine wf_aggiorna ()
end prototypes

event ue_imposta_default();datetime ldt_data
string ls_data

ls_data = string(year(today()),"0000") + "/01/01"
ldt_data = datetime(date(ls_data), 00:00:00)
dw_selezione.setitem(dw_selezione.getrow(),"data_inizio", ldt_data)

ls_data = string(year(today()),"0000") + "/12/31"
ldt_data = datetime(date(ls_data), 00:00:00)
dw_selezione.setitem(dw_selezione.getrow(),"data_fine", ldt_data)

end event

public subroutine wf_aggiorna ();string ls_sql_child, ls_sql, ls_where, ls_cod_attrezzatura_inizio, ls_cod_attrezzatura_fine, ls_cod_cat_attrezzatura, ls_cod_reparto, &
       ls_cod_area_aziendale, ls_cod_divisione, ls_order
datetime ldt_data
datastore lds_pareto

lds_pareto = CREATE datastore
lds_pareto.dataobject = 'd_ds_graf_pareto_manutenzioni'
lds_pareto.settransobject(sqlca)

ls_sql = lds_pareto.getsqlselect()

if pos(ls_sql,"GROUP") > 0 then
	ls_sql_child = left(ls_sql, pos(ls_sql,"GROUP") -1 )
	ls_sql = mid(ls_sql, pos(ls_sql,"GROUP"))
elseif pos(ls_sql,"group") > 0 then
	ls_sql_child = left(ls_sql, pos(ls_sql,"group") -1 )
	ls_sql = mid(ls_sql, pos(ls_sql,"group"))
end if

if pos(ls_sql_child, "where") > 0 or pos(ls_sql_child, "WHERE") > 0 then
	ls_where = " and "
else
	ls_where = " WHERE "
end if

ls_where += " manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'"
ls_where += " and manutenzioni.cod_guasto is not null and flag_eseguito = 'S' "


// -----------  compongo la where in base alle selezioni effettuate ----------------------
ldt_data = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio")
ls_where += " and manutenzioni.data_registrazione >= " + string(ldt_data,s_cs_xx.db_funzioni.formato_data)

ldt_data = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_fine")
ls_where += " and manutenzioni.data_registrazione <= " + string(ldt_data,s_cs_xx.db_funzioni.formato_data)

ls_cod_attrezzatura_inizio = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_attrezzatura_inizio")
ls_cod_attrezzatura_fine = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_attrezzatura_fine")
if not isnull(ls_cod_attrezzatura_inizio) then
	if not isnull(ls_cod_attrezzatura_fine) then
		ls_where += " and manutenzioni.cod_attrezzatura >= '" + ls_cod_attrezzatura_inizio + "' "
		ls_where += " and manutenzioni.cod_attrezzatura <= '" + ls_cod_attrezzatura_fine + "' "
	else
		ls_where += " and manutenzioni.cod_attrezzatura = '" + ls_cod_attrezzatura_inizio + "' "
	end if
end if

ls_cod_cat_attrezzatura = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_cat_attrezzatura")
if not isnull(ls_cod_cat_attrezzatura) then
	ls_where += " and anag_attrezzature.cod_cat_attrezzature = '" + ls_cod_cat_attrezzatura + "' "
end if

ls_cod_reparto = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_reparto")
if not isnull(ls_cod_reparto) then
	ls_where += " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
end if

ls_cod_area_aziendale = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_area_aziendale")
ls_cod_divisione = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_divisione")
if not isnull(ls_cod_area_aziendale) then
	ls_where += " and anag_attrezzature.cod_area_aziendale = '" + ls_cod_area_aziendale + "' "
else
	if not isnull(ls_cod_divisione) then
	ls_where += " and anag_attrezzature.cod_area_aziendale in ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '"+s_cs_xx.cod_azienda+"' and cod_divisione = '"+ls_cod_divisione+"') "
	end if
end if

ls_sql_child = ls_sql_child + ls_where + ls_sql + " order by cont_guasti desc"

// --------------- passo l'ambaradan al cursore ------------------------------------------
destroy lds_pareto

wf_pareto_init (ls_sql_child )

end subroutine

on w_graf_pareto_manutenzioni.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_2
end on

on w_graf_pareto_manutenzioni.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.cb_1)
destroy(this.cb_2)
end on

event pc_setwindow;call super::pc_setwindow;datetime ldt_oggi

is_graf1_titolo = "Pareto Guasti su Attrezzature"
is_graf1_des_asse_x = "Tipi Guasti"
is_graf1_des_asse_y = "Numero Casi"
is_graf2_titolo = "Istrogramma Cumulato"
is_graf2_des_asse_x = "Cumulata (20% dei Guasti)"
is_graf2_des_asse_y = "% Incidenza"

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 

save_on_close(c_socnosave)

postevent("ue_imposta_default")

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")

f_PO_LoadDDDW_DW(dw_selezione,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type uo_grafico from w_graf_pareto`uo_grafico within w_graf_pareto_manutenzioni
end type

type dw_pareto_guasti from w_graf_pareto`dw_pareto_guasti within w_graf_pareto_manutenzioni
end type

type uo_graf_cumulata from w_graf_pareto`uo_graf_cumulata within w_graf_pareto_manutenzioni
end type

type dw_guasti_cumulata from w_graf_pareto`dw_guasti_cumulata within w_graf_pareto_manutenzioni
end type

event clicked;call super::clicked;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura_inizio"
end event

type dw_selezione from uo_cs_xx_dw within w_graf_pareto_manutenzioni
integer x = 14
integer y = 1512
integer width = 2432
integer height = 676
integer taborder = 11
string dataobject = "d_selezione_graf_pareto_manutenzioni"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)
choose case i_colname
	case "cod_divisione"
		if isnull(i_coltext) or i_coltext = "" then 
			f_PO_LoadDDDW_DW(dw_selezione,"cod_area_aziendale",sqlca,&
								  "tab_aree_aziendali","cod_area_aziendale","des_area",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		else
			setitem(getrow(),"cod_area_aziendale", ls_null)
			f_PO_LoadDDDW_DW(dw_selezione,"cod_area_aziendale",sqlca,&
								  "tab_aree_aziendali","cod_area_aziendale","des_area",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + i_coltext + "'")
		end if			
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura_inizio")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura_fine")
end choose
end event

type cb_1 from commandbutton within w_graf_pareto_manutenzioni
integer x = 3013
integer y = 1532
integer width = 361
integer height = 84
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Grafici"
end type

event clicked;wf_aggiorna()

end event

type cb_2 from commandbutton within w_graf_pareto_manutenzioni
integer x = 3013
integer y = 1648
integer width = 361
integer height = 84
integer taborder = 31
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long Job
string ls_str, ls_codice, ls_descrizione
datetime ldt_data

Job = PrintOpen( )

PrintDefineFont(Job, 1, "Arial", -20, 700, Default!, Decorative!, FALSE, FALSE)
PrintDefineFont(Job, 2, "Arial", -10, 400, Default!, Decorative!, FALSE, FALSE)
printsetfont(job,2)

select rag_soc_1
into   :ls_descrizione
from   aziende
where cod_azienda = :s_cs_xx.cod_azienda;

Print(Job, 10, ls_descrizione, 6000)
Print(Job, 10, "Data Stampa " + string(today(),"dd/mm/yyyy"))

printsetfont(job,1)
Print(Job, 1500, "REPORT GRAFICI MANUTENZIONI")

printsetfont(job,2)

// stampo limiti date
ldt_data = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio")
if not isnull(ldt_data) and ldt_data >= datetime(date("01/01/1900"),00:00:00) then
	ls_str += "Data inizio = " + string(ldt_data,"dd/mm/yyyy")
else
	ls_str += "Dalla prima data "
end if

ldt_data = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_fine")
if not isnull(ldt_data) and ldt_data >= datetime(date("01/01/1900"),00:00:00) then
	ls_str += "  Data fine = " + string(ldt_data,"dd/mm/yyyy")
else
	ls_str += "   All'ultima data"
end if
// cerco di stampare al centro del foglio
Print(Job, 1500 ,ls_str)

// divisione
ls_str = "DIVISIONE: "
ls_codice = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_divisione")
if not isnull(ls_codice) then
	select des_divisione
	into   :ls_descrizione
	from   anag_divisioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_divisione = :ls_codice;
	ls_str += ls_codice + "  " + ls_descrizione
else
	ls_str += "Tutte le divisioni"
end if
Print(Job, 1500 ,ls_str)

// area
ls_str = "AREA: "
ls_codice = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_area_aziendale")
if not isnull(ls_codice) then
	select des_area
	into   :ls_descrizione
	from   tab_aree_aziendali
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_aziendale = :ls_codice;
	ls_str += ls_codice + "  " + ls_descrizione
else
	ls_str += "Tutte le aree aziendali"
end if
Print(Job, 1500 ,ls_str)

// reparto
ls_str = "REPARTO: "
ls_codice = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_reparto")
if not isnull(ls_codice) then
	select des_reparto
	into   :ls_descrizione
	from   anag_reparti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_reparto = :ls_codice;
	ls_str += ls_codice + "  " + ls_descrizione
else
	ls_str += "Tutte i reparti"
end if
Print(Job, 1500 ,ls_str)

// categoria attrezzatura
ls_str = "CATEGORIA ATTREZZATURA: "
ls_codice = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_cat_attrezzatura")
if not isnull(ls_codice) then
	select des_cat_attrezzature
	into   :ls_descrizione
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cat_attrezzature = :ls_codice;
	ls_str += ls_codice + "  " + ls_descrizione
else
	ls_str += "Tutte le categorie"
end if
Print(Job, 1500 ,ls_str)

// da attrezzatura
ls_codice = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_attrezzatura_inizio")
if isnull(ls_codice) then
	ls_str = "Dal primo codice attrezzatura "
else
	ls_str = "DA ATTREZZATURA: " + ls_codice  + "  "
	setnull(ls_descrizione)
	select des_cat_attrezzature
	into   :ls_descrizione
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cat_attrezzature = :ls_codice;
	if sqlca.sqlcode = 0 and not isnull(ls_descrizione) then ls_str += ls_descrizione
end if

// da attrezzatura
ls_codice = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_attrezzatura_fine")
if isnull(ls_codice) then
	ls_str += " All'ultimo codice attrezzatura"
else
	ls_str += "DA ATTREZZATURA: " + ls_codice  + "  "
	setnull(ls_descrizione)
	select des_cat_attrezzature
	into   :ls_descrizione
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cat_attrezzature = :ls_codice;
	if sqlca.sqlcode = 0 and not isnull(ls_descrizione) then ls_str += ls_descrizione
end if
Print(Job, 1500 ,ls_str)



uo_graf_cumulata.Print(Job, 2000, 2500)

uo_grafico.Print(Job, 2000, 6500)

PrintClose(Job)
end event

