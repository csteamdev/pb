﻿$PBExportHeader$w_reg_uso_attrez_ins.srw
$PBExportComments$Finestra inserimento dati registro uso attrezzature
forward
global type w_reg_uso_attrez_ins from w_cs_xx_principale
end type
type cb_conferma from commandbutton within w_reg_uso_attrez_ins
end type
type cb_annulla from commandbutton within w_reg_uso_attrez_ins
end type
type dw_inserimento from uo_cs_xx_dw within w_reg_uso_attrez_ins
end type
end forward

global type w_reg_uso_attrez_ins from w_cs_xx_principale
integer width = 2546
integer height = 1408
string title = "Inserimento Registro Uso Attrezzature"
cb_conferma cb_conferma
cb_annulla cb_annulla
dw_inserimento dw_inserimento
end type
global w_reg_uso_attrez_ins w_reg_uso_attrez_ins

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_inserimento.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)



end event

on w_reg_uso_attrez_ins.create
int iCurrent
call super::create
this.cb_conferma=create cb_conferma
this.cb_annulla=create cb_annulla
this.dw_inserimento=create dw_inserimento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_conferma
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_inserimento
end on

on w_reg_uso_attrez_ins.destroy
call super::destroy
destroy(this.cb_conferma)
destroy(this.cb_annulla)
destroy(this.dw_inserimento)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_inserimento,"cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
					  
f_PO_LoadDDDW_DW(dw_inserimento,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			

end event

type cb_conferma from commandbutton within w_reg_uso_attrez_ins
integer x = 2103
integer y = 1200
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;string ls_null, ls_cod_attrezzatura, ls_cod_operaio, ls_note, ls_flag_tipo_tempo
long ll_null, ll_prog_attrezzatura, ll_anno_commessa, ll_num_commessa
date ld_null, ld_data_prelievo, ld_data_restituzione, ld_data_restituzione_comodo
time lt_null, lt_ora_prelievo, lt_ora_restituzione
decimal ldm_contatore

setnull(ls_null)
setnull(ll_null)
setnull(ld_null)
setnull(lt_null)

dw_inserimento.accepttext()

ls_cod_attrezzatura = dw_inserimento.getitemstring(dw_inserimento.getrow(), "cod_attrezzatura")
ll_prog_attrezzatura = dw_inserimento.getitemnumber(dw_inserimento.getrow(), "prog_attrezzatura")
ld_data_prelievo = dw_inserimento.getitemdate(dw_inserimento.getrow(), "data_prelievo")
lt_ora_prelievo = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_prelievo")
ld_data_restituzione = dw_inserimento.getitemdate(dw_inserimento.getrow(), "data_restituzione")
lt_ora_restituzione = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_restituzione")
ls_cod_operaio = dw_inserimento.getitemstring(dw_inserimento.getrow(), "cod_operaio")
ll_anno_commessa = dw_inserimento.getitemnumber(dw_inserimento.getrow(), "anno_commessa")
ll_num_commessa = dw_inserimento.getitemnumber(dw_inserimento.getrow(), "num_commessa")
ls_note = dw_inserimento.getitemstring(dw_inserimento.getrow(), "note")
ls_flag_tipo_tempo = dw_inserimento.getitemstring(dw_inserimento.getrow(), "flag_tipo_tempo")
ldm_contatore = dw_inserimento.getitemdecimal(dw_inserimento.getrow(), "contatore")


if isnull(ls_cod_attrezzatura) then
	g_mb.messagebox("Omnia", "Il campo CODICE ATTREZZATURA non può essere vuoto")
	return
end if	

if isnull(ll_prog_attrezzatura) then 
	g_mb.messagebox("Omnia", "Il campo PROGRESSIVO ATTREZZATURA non può essere vuoto")
	return
end if		

ld_data_restituzione_comodo = dw_inserimento.getitemdate(dw_inserimento.getrow(), "data_restituzione")
if isnull(ld_data_restituzione_comodo) then
	g_mb.messagebox("Omnia", "Data restituzione obbligatoria")
	return
end if	

insert into registro_uso_attrezzature 
			  (cod_azienda, 
				cod_attrezzatura, 
				prog_uso_attrezzatura, 
				data_prelievo, 
				ora_prelievo, 
				data_restituzione, 
				ora_restituzione, 
				cod_operaio, 
				anno_commessa, 
				num_commessa, 
				note, 
				flag_tipo_tempo, contatore)
	 values (:s_cs_xx.cod_azienda, 
	 		   :ls_cod_attrezzatura, 
				:ll_prog_attrezzatura, 
				:ld_data_prelievo, 
				:lt_ora_prelievo,
				:ld_data_restituzione,
				:lt_ora_restituzione,
				:ls_cod_operaio,
				:ll_anno_commessa, 
				:ll_num_commessa,
				:ls_note,
				:ls_flag_tipo_tempo,
				:ldm_contatore) ;
				
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in inserimento dati in tabella REGISTRO USO ATTREZZATURE")
	g_mb.messagebox("Omnia", sqlca.sqlerrtext + " " + string(sqlca.sqlcode))	
	return
end if	

if sqlca.sqlcode = 0 then
	commit;
	dw_inserimento.postevent("ue_gen_prog_man")
	g_mb.messagebox("Omnia", "Inserimento avvenuto con successo")
end if	

dw_inserimento.setitem(dw_inserimento.getrow(), "cod_attrezzatura", ls_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "prog_attrezzatura", ll_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "data_prelievo", ld_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "ora_prelievo", lt_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "data_restituzione", ld_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "ora_restituzione", lt_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "cod_operaio", ls_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "anno_commessa", ll_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "num_commessa", ll_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "note", ls_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "flag_tipo_tempo", "N")
dw_inserimento.setitem(dw_inserimento.getrow(), "contatore", ll_null)
end event

type cb_annulla from commandbutton within w_reg_uso_attrez_ins
integer x = 1691
integer y = 1200
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null
long ll_null
date ld_null
time lt_null

setnull(ls_null)
setnull(ll_null)
setnull(ld_null)
setnull(lt_null)

dw_inserimento.setitem(dw_inserimento.getrow(), "cod_attrezzatura", ls_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "prog_attrezzatura", ll_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "data_prelievo", ld_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "ora_prelievo", lt_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "data_restituzione", ld_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "ora_restituzione", lt_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "cod_operaio", ls_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "anno_commessa", ll_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "num_commessa", ll_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "note", ls_null)
dw_inserimento.setitem(dw_inserimento.getrow(), "flag_tipo_tempo", "N")
dw_inserimento.setitem(dw_inserimento.getrow(), "contatore", ll_null)
end event

type dw_inserimento from uo_cs_xx_dw within w_reg_uso_attrez_ins
event ue_gen_prog_man ( )
integer x = 23
integer y = 20
integer width = 2469
integer height = 1180
integer taborder = 20
string dataobject = "d_reg_uso_attrez_veloce"
boolean border = false
end type

event ue_gen_prog_man;call super::ue_gen_prog_man;string ls_cod_attrezzatura, ls_periodicita, ls_flag_tipo_tempo, ls_messaggio, ls_cod_tipo_manutenzione, ls_cod_azienda
double ll_tot_ore, ll_frequenza, ld_somma_contatore, ld_contatore
long ll_max_num_registrazione, ll_prog_uso_attrezzatura, ll_anno_registrazione, ll_num_registrazione
datetime ldt_data_registrazione, ldt_data_max, ldt_data_comodo
uo_manutenzioni luo_manutenzioni

ls_cod_attrezzatura = dw_inserimento.getitemstring(dw_inserimento.getrow(), "cod_attrezzatura")

declare cu_tipi_attrezzature cursor for
select distinct(cod_tipo_manutenzione) 
			 from programmi_manutenzione
			where cod_azienda = :s_cs_xx.cod_azienda
			  and cod_attrezzatura = :ls_cod_attrezzatura;
			
open cu_tipi_attrezzature;

do while 1 = 1
	fetch cu_tipi_attrezzature into :ls_cod_tipo_manutenzione;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura cod_tipo_manutenzione da tabella Programmi Manutenzione " + sqlca.sqlerrtext)
		return
	end if		

	select periodicita,
			 frequenza,
			 anno_registrazione,
			 num_registrazione
	  into :ls_periodicita,
			 :ll_frequenza,
			 :ll_anno_registrazione,
			 :ll_num_registrazione
	  from programmi_manutenzione
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_attrezzatura = :ls_cod_attrezzatura
		and flag_scadenze = 'N'
		and anno_registrazione = (select max(anno_registrazione)
											 from programmi_manutenzione
											where cod_azienda = :s_cs_xx.cod_azienda
											  and cod_attrezzatura = :ls_cod_attrezzatura
											  and flag_scadenze = 'N')
		and num_registrazione =  (select max(num_registrazione)
											 from programmi_manutenzione
											where cod_azienda = :s_cs_xx.cod_azienda
											  and cod_attrezzatura = :ls_cod_attrezzatura
											  and flag_scadenze = 'N')	;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella Programmi Manutenzione " + sqlca.sqlerrtext)
		return
	end if
	
	if sqlca.sqlcode = 0 then
		
	//-------------------------------------------- Modifica Nicola -----------------------------------------------------
	
			ll_tot_ore = 0
			
			if (ls_periodicita = 'M' or ls_periodicita = 'O') then
				
				if ls_periodicita = 'O' then
					ll_tot_ore = ll_frequenza
				elseif ls_periodicita = 'M' then 
					ll_tot_ore = ll_frequenza / 60
				end if		
				
				select max(data_registrazione)
				  into :ldt_data_registrazione
				  from manutenzioni
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_attrezzatura = :ls_cod_attrezzatura
					and flag_eseguito = 'S'
					and flag_ordinario = 'S';
				if sqlca.sqlcode < 0 then 	
					g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella MANUTENZIONI")
					return
				end if
				if isnull(ldt_data_registrazione) then ldt_data_registrazione = datetime(Date("1900/01/01"))
				ld_somma_contatore = 0
				ldt_data_max = datetime(Date("2999/12/31"))
				do while 1 = 1	
			
					 select max(data_restituzione), 
							  max(prog_uso_attrezzatura)
						into :ldt_data_comodo,
							  :ll_prog_uso_attrezzatura
						from registro_uso_attrezzature	  
					  where cod_azienda = :s_cs_xx.cod_azienda
						 and cod_attrezzatura = :ls_cod_attrezzatura
						 and data_restituzione > :ldt_data_registrazione 
						 and data_restituzione < :ldt_data_max;
						 
					if sqlca.sqlcode = 100 then exit 
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Omnia", "Errore in lettura data_registrazione max dalla tabella REGISTRO_USO_MANUTENZIONI")
						return
					end if
			
					 select flag_tipo_tempo, 
							  contatore			 
						into :ls_flag_tipo_tempo,
							  :ld_contatore		
						from registro_uso_attrezzature
					  where cod_azienda = :s_cs_xx.cod_azienda
						 and cod_attrezzatura = :ls_cod_attrezzatura
						 and prog_uso_attrezzatura = :ll_prog_uso_attrezzatura;
			
					if sqlca.sqlcode = 100 then exit 
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella REGISTRO_USO_MANUTENZIONI")
						return
					end if
					
					if sqlca.sqlcode = 0 then
						ldt_data_max = ldt_data_comodo
						if ls_flag_tipo_tempo = "S" then
							ld_somma_contatore = ld_somma_contatore + ld_contatore
						elseif ls_flag_tipo_tempo = "N" then
							ld_somma_contatore = ld_somma_contatore + ld_contatore
							exit
						end if
					end if
				loop
			
				if ll_tot_ore < ld_somma_contatore then
					
					select cod_azienda
					  into :ls_cod_azienda
					  from manutenzioni
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_attrezzatura = :ls_cod_attrezzatura
						and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
						and flag_eseguito = 'N'
						and flag_ordinario = 'S'
						and anno_registrazione = (select max(anno_registrazione)
															 from manutenzioni
															where cod_azienda = :s_cs_xx.cod_azienda
															  and cod_attrezzatura = :ls_cod_attrezzatura
															  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
															  and flag_eseguito = 'N'
															  and flag_ordinario = 'S')
						and num_registrazione =  (select max(num_registrazione)
															 from manutenzioni
															where cod_azienda = :s_cs_xx.cod_azienda
															  and cod_attrezzatura = :ls_cod_attrezzatura
															  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
															  and flag_eseguito = 'N'
															  and flag_ordinario = 'S')	;				
					if sqlca.sqlcode < 0 then 	
						g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella MANUTENZIONI")
						return
					end if
					
					if sqlca.sqlcode <> 0 then 						
					
						luo_manutenzioni = CREATE uo_manutenzioni
						if luo_manutenzioni.uof_crea_manutenzione_programmata(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
							g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
						end if	
					end if	
				end if
			end if	
	//--------------------------------------------- Fine Modifica ------------------------------------------------------	
	
	end if
	
	destroy luo_manutenzioni
	
loop

close cu_tipi_attrezzature;	
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_attrezzatura, ls_flag_tipo_tempo 
	long ll_max_prog
	date ld_data_prelievo, ld_data_restituzione
	time lt_ora_prelievo, lt_ora_restituzione
	decimal ldm_differenza
	uo_calcolo_tempi iuo_calcolo_tempi
	
   choose case i_colname
      case "cod_attrezzatura"
		
			if not isnull(data) then
				select max(prog_uso_attrezzatura) + 1
				  into :ll_max_prog
				  from registro_uso_attrezzature
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_attrezzatura = :data;
				
				if ll_max_prog = 0 or isnull(ll_max_prog) then ll_max_prog = 1
				if sqlca.sqlcode <> 0 then 
					g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella REGISTRO USO ATTREZZATURE")
					return
				end if	
			else 
				ll_max_prog = 1
			end if	 
				 
			dw_inserimento.setitem(dw_inserimento.getrow(), "prog_attrezzatura", ll_max_prog)	

      case "anno_commessa"
			f_PO_LoadDDLB_DW(dw_inserimento,"num_commessa",sqlca,&
								  "anag_commesse","num_commessa","num_commessa",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_commessa = " + data)									  
								  
		case "data_prelievo"
			dw_inserimento.accepttext()
			ls_flag_tipo_tempo = dw_inserimento.getitemstring(dw_inserimento.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_prelievo")
				lt_ora_prelievo = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_prelievo")
				ld_data_restituzione = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_restituzione")
				lt_ora_restituzione = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_restituzione")
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_inserimento.setitem(dw_inserimento.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if	
		case "ora_prelievo"
			dw_inserimento.accepttext()
			ls_flag_tipo_tempo = dw_inserimento.getitemstring(dw_inserimento.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_prelievo")
				lt_ora_prelievo = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_prelievo")
				ld_data_restituzione = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_restituzione")
				lt_ora_restituzione = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_restituzione")		
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_inserimento.setitem(dw_inserimento.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if				
		case "data_restituzione"
			dw_inserimento.accepttext()
			ls_flag_tipo_tempo = dw_inserimento.getitemstring(dw_inserimento.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_prelievo")
				lt_ora_prelievo = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_prelievo")
				ld_data_restituzione = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_restituzione")
				lt_ora_restituzione = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_restituzione")
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_inserimento.setitem(dw_inserimento.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if							
		case "ora_restituzione"
			dw_inserimento.accepttext()
			ls_flag_tipo_tempo = dw_inserimento.getitemstring(dw_inserimento.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_prelievo")
				lt_ora_prelievo = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_prelievo")
				ld_data_restituzione = dw_inserimento.GetItemDate(dw_inserimento.getrow(), "data_restituzione")
				lt_ora_restituzione = dw_inserimento.getitemtime(dw_inserimento.getrow(), "ora_restituzione")
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_inserimento.setitem(dw_inserimento.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if																		  								  
	end choose
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_new;call super::pcd_new;dw_inserimento.setitem(dw_inserimento.getrow(), "flag_tipo_tempo", "N")
end event

event pcd_validaterow;call super::pcd_validaterow;date ld_data_restituzione

ld_data_restituzione = dw_inserimento.getitemdate(dw_inserimento.getrow(), "data_restituzione")
if isnull(ld_data_restituzione) then
	g_mb.messagebox("Omnia", "Data restituzione obbligatoria")
	return
end if	
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_inserimento,"cod_attrezzatura")
end choose
end event

