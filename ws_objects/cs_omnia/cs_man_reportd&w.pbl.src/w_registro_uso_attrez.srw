﻿$PBExportHeader$w_registro_uso_attrez.srw
$PBExportComments$Finestra gestione registro uso attrezzature
forward
global type w_registro_uso_attrez from w_cs_xx_principale
end type
type dw_registro_uso_attrezz_dett from uo_cs_xx_dw within w_registro_uso_attrez
end type
type cb_ricerca from commandbutton within w_registro_uso_attrez
end type
type cb_reset from commandbutton within w_registro_uso_attrez
end type
type dw_selezione from uo_cs_xx_dw within w_registro_uso_attrez
end type
type dw_registro_uso_attrazz_lista from uo_cs_xx_dw within w_registro_uso_attrez
end type
type dw_folder_search from u_folder within w_registro_uso_attrez
end type
end forward

global type w_registro_uso_attrez from w_cs_xx_principale
integer width = 3013
integer height = 2044
string title = "Registro Uso Attrezzature"
dw_registro_uso_attrezz_dett dw_registro_uso_attrezz_dett
cb_ricerca cb_ricerca
cb_reset cb_reset
dw_selezione dw_selezione
dw_registro_uso_attrazz_lista dw_registro_uso_attrazz_lista
dw_folder_search dw_folder_search
end type
global w_registro_uso_attrez w_registro_uso_attrez

type variables
boolean ib_new = false, ib_modify = false, ib_delete = false
end variables

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_selezione,"cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//f_PO_LoadDDDW_DW(dw_registro_uso_attrezz_dett,"cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
					  
f_PO_LoadDDDW_DW(dw_registro_uso_attrezz_dett,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			

end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]

dw_selezione.set_dw_options(sqlca, &
								 pcca.null_object, &
								 c_noretrieveonopen + &
								 c_newonopen + &
								 c_nodelete + &
								 c_nomodify + &
								 c_disableCC, &
								 c_default)
								 
dw_registro_uso_attrazz_lista.set_dw_options(sqlca, &
                                             pcca.null_object, &
                                             c_noretrieveonopen, &
                                             c_default)
															
dw_registro_uso_attrezz_dett.set_dw_options(sqlca, &
                                            dw_registro_uso_attrazz_lista, &
                                            c_sharedata + c_scrollparent, &
                                            c_default)
iuo_dw_main=dw_registro_uso_attrazz_lista

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)




l_objects[1] = dw_registro_uso_attrazz_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_attrezzature_ricerca
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_registro_uso_attrazz_lista.change_dw_current()
dw_registro_uso_attrazz_lista.resetupdate()

//dw_selezione.insertrow(0)
//dw_selezione.resetupdate()

end event

on w_registro_uso_attrez.create
int iCurrent
call super::create
this.dw_registro_uso_attrezz_dett=create dw_registro_uso_attrezz_dett
this.cb_ricerca=create cb_ricerca
this.cb_reset=create cb_reset
this.dw_selezione=create dw_selezione
this.dw_registro_uso_attrazz_lista=create dw_registro_uso_attrazz_lista
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_registro_uso_attrezz_dett
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_registro_uso_attrazz_lista
this.Control[iCurrent+6]=this.dw_folder_search
end on

on w_registro_uso_attrez.destroy
call super::destroy
destroy(this.dw_registro_uso_attrezz_dett)
destroy(this.cb_ricerca)
destroy(this.cb_reset)
destroy(this.dw_selezione)
destroy(this.dw_registro_uso_attrazz_lista)
destroy(this.dw_folder_search)
end on

event open;call super::open;dw_selezione.postevent("getfocus")
end event

type dw_registro_uso_attrezz_dett from uo_cs_xx_dw within w_registro_uso_attrez
integer x = 23
integer y = 720
integer width = 2926
integer height = 1196
integer taborder = 50
string dataobject = "d_registro_uso_attrez_dett"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;dw_registro_uso_attrazz_lista.triggerevent("pcd_new")
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_attrezzatura, ls_flag_tipo_tempo
	long ll_max_prog
	date ld_data_prelievo, ld_data_restituzione
	time lt_ora_prelievo, lt_ora_restituzione
	decimal ldm_differenza
	uo_calcolo_tempi iuo_calcolo_tempi
		
   choose case i_colname
      case "cod_attrezzatura"
		
			if not isnull(i_coltext) then
				select max(prog_uso_attrezzatura) + 1
				  into :ll_max_prog
				  from registro_uso_attrezzature
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_attrezzatura = :i_coltext;
				
				if ll_max_prog = 0 or isnull(ll_max_prog) then ll_max_prog = 1
				if sqlca.sqlcode <> 0 then 
					g_mb.messagebox("Omnia", "Errore in lettura da ti dalla tabella REGISTRO USO ATTREZZATURE")
					return
				end if	
			else 
				ll_max_prog = 1
			end if	 
				 
			dw_registro_uso_attrezz_dett.setitem(dw_registro_uso_attrazz_lista.getrow(), "prog_uso_attrezzatura", ll_max_prog)	

      case "anno_commessa"
			f_PO_LoadDDLB_DW(dw_registro_uso_attrezz_dett,"num_commessa",sqlca,&
								  "anag_commesse","num_commessa","num_commessa",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_commessa = " + data)	
								  
		case "data_prelievo"
			dw_registro_uso_attrezz_dett.accepttext()
			ls_flag_tipo_tempo = dw_registro_uso_attrezz_dett.getitemstring(dw_registro_uso_attrezz_dett.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_prelievo"))
				lt_ora_prelievo = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_prelievo"))
				ld_data_restituzione = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_restituzione"))
				lt_ora_restituzione = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_restituzione"))			
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_registro_uso_attrezz_dett.setitem(dw_registro_uso_attrazz_lista.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if	
		case "ora_prelievo"
			dw_registro_uso_attrezz_dett.accepttext()
			ls_flag_tipo_tempo = dw_registro_uso_attrezz_dett.getitemstring(dw_registro_uso_attrezz_dett.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_prelievo"))
				lt_ora_prelievo = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_prelievo"))
				ld_data_restituzione = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_restituzione"))
				lt_ora_restituzione = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_restituzione"))			
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_registro_uso_attrezz_dett.setitem(dw_registro_uso_attrazz_lista.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if				
		case "data_restituzione"
			dw_registro_uso_attrezz_dett.accepttext()
			ls_flag_tipo_tempo = dw_registro_uso_attrezz_dett.getitemstring(dw_registro_uso_attrezz_dett.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_prelievo"))
				lt_ora_prelievo = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_prelievo"))
				ld_data_restituzione = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_restituzione"))
				lt_ora_restituzione = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_restituzione"))			
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_registro_uso_attrezz_dett.setitem(dw_registro_uso_attrazz_lista.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if							
		case "ora_restituzione"
			dw_registro_uso_attrezz_dett.accepttext()
			ls_flag_tipo_tempo = dw_registro_uso_attrezz_dett.getitemstring(dw_registro_uso_attrezz_dett.getrow(), "flag_tipo_tempo")
			if ls_flag_tipo_tempo = "S" then
				ld_data_prelievo = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_prelievo"))
				lt_ora_prelievo = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_prelievo"))
				ld_data_restituzione = date(dw_registro_uso_attrezz_dett.GetItemDatetime(dw_registro_uso_attrazz_lista.getrow(), "data_restituzione"))
				lt_ora_restituzione = time(dw_registro_uso_attrezz_dett.getitemDatetime(dw_registro_uso_attrazz_lista.getrow(), "ora_restituzione"))			
				if not isnull(ld_data_prelievo) and not isnull(lt_ora_prelievo) and not isnull(ld_data_restituzione) and not isnull(lt_ora_restituzione) then
					iuo_calcolo_tempi = create uo_calcolo_tempi
					ldm_differenza = iuo_calcolo_tempi.wf_differenza_date(ld_data_prelievo, lt_ora_prelievo, ld_data_restituzione, lt_ora_restituzione)
					dw_registro_uso_attrezz_dett.setitem(dw_registro_uso_attrazz_lista.getrow(), "contatore", ldm_differenza)
					destroy iuo_calcolo_tempi
				end if	
			end if										
	end choose
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_registro_uso_attrezz_dett,"cod_attrezzatura")
end choose
end event

type cb_ricerca from commandbutton within w_registro_uso_attrez
integer x = 2528
integer y = 60
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;// cb_search clicked event
//dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_registro_uso_attrazz_lista.change_dw_current()

parent.triggerevent("pc_retrieve")




end event

type cb_reset from commandbutton within w_registro_uso_attrez
integer x = 2528
integer y = 160
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Ric."
end type

event clicked;long ll_null
string ls_null

setnull(ls_null)
setnull(ll_null)

dw_selezione.setItem(1, "cod_attrezzatura", ls_null)
dw_selezione.setItem(1, "prog_attrezzatura", ll_null)

end event

type dw_selezione from uo_cs_xx_dw within w_registro_uso_attrez
integer x = 160
integer y = 60
integer width = 2400
integer height = 260
integer taborder = 30
string dataobject = "d_registro_uso_attrez_sel"
boolean border = false
boolean livescroll = true
end type

event getfocus;call super::getfocus;dw_selezione.postevent("pcd_inactive")
end event

event pcd_inactive;call super::pcd_inactive;dw_selezione.Set_DW_Query(c_IgnoreChanges)
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_atta"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura")
end choose
end event

type dw_registro_uso_attrazz_lista from uo_cs_xx_dw within w_registro_uso_attrez
event ue_gen_prog_man ( )
integer x = 160
integer y = 60
integer width = 2354
integer height = 620
integer taborder = 80
string dataobject = "d_registro_uso_attrez_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_gen_prog_man;call super::ue_gen_prog_man;string ls_cod_attrezzatura, ls_periodicita, ls_flag_tipo_tempo, ls_messaggio, ls_cod_tipo_manutenzione, ls_cod_azienda
double ll_tot_ore, ll_frequenza, ld_somma_contatore, ld_contatore
long ll_max_num_registrazione, ll_prog_uso_attrezzatura, ll_anno_registrazione, ll_num_registrazione
datetime ldt_data_registrazione, ldt_data_max, ldt_data_comodo
uo_manutenzioni luo_manutenzioni

ls_cod_attrezzatura = dw_registro_uso_attrazz_lista.getitemstring(dw_registro_uso_attrazz_lista.getrow(), "cod_attrezzatura")

declare cu_tipi_attrezzature cursor for
select distinct(cod_tipo_manutenzione) 
			 from programmi_manutenzione
			where cod_azienda = :s_cs_xx.cod_azienda
			  and cod_attrezzatura = :ls_cod_attrezzatura;
			
open cu_tipi_attrezzature;

do while 1 = 1
	fetch cu_tipi_attrezzature into :ls_cod_tipo_manutenzione;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura cod_tipo_manutenzione da tabella Programmi Manutenzione " + sqlca.sqlerrtext)
		return
	end if		

	select periodicita,
			 frequenza,
			 anno_registrazione,
			 num_registrazione
	  into :ls_periodicita,
			 :ll_frequenza,
			 :ll_anno_registrazione,
			 :ll_num_registrazione
	  from programmi_manutenzione
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_attrezzatura = :ls_cod_attrezzatura
		and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
		and flag_scadenze = 'N'
		and anno_registrazione = (select max(anno_registrazione)
											 from programmi_manutenzione
											where cod_azienda = :s_cs_xx.cod_azienda
											  and cod_attrezzatura = :ls_cod_attrezzatura
											  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
											  and flag_scadenze = 'N')
		and num_registrazione =  (select max(num_registrazione)
											 from programmi_manutenzione
											where cod_azienda = :s_cs_xx.cod_azienda
											  and cod_attrezzatura = :ls_cod_attrezzatura
											  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
											  and flag_scadenze = 'N')	;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella Programmi Manutenzione " + sqlca.sqlerrtext)
		return
	end if
	
	if sqlca.sqlcode = 0 then
		
	//-------------------------------------------- Modifica Nicola -----------------------------------------------------
	
			ll_tot_ore = 0
			
			if (ls_periodicita = 'M' or ls_periodicita = 'O') then
				
				if ls_periodicita = 'O' then
					ll_tot_ore = ll_frequenza
				elseif ls_periodicita = 'M' then 
					ll_tot_ore = ll_frequenza / 60
				end if		
				
				select max(data_registrazione)
				  into :ldt_data_registrazione
				  from manutenzioni
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_attrezzatura = :ls_cod_attrezzatura
					and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
					and flag_eseguito = 'S'
					and flag_ordinario = 'S';
				if sqlca.sqlcode < 0 then 	
					g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella MANUTENZIONI")
					return
				end if
				if isnull(ldt_data_registrazione) then ldt_data_registrazione = datetime(Date("1900/01/01"))
				ld_somma_contatore = 0
				ldt_data_max = datetime(Date("2999/12/31"))
				do while 1 = 1	
			
					 select max(data_restituzione), 
							  max(prog_uso_attrezzatura)
						into :ldt_data_comodo,
							  :ll_prog_uso_attrezzatura
						from registro_uso_attrezzature	  
					  where cod_azienda = :s_cs_xx.cod_azienda
						 and cod_attrezzatura = :ls_cod_attrezzatura
						 and data_restituzione > :ldt_data_registrazione 
						 and data_restituzione < :ldt_data_max;
						 
					if sqlca.sqlcode = 100 then exit 
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Omnia", "Errore in lettura data_registrazione max dalla tabella REGISTRO_USO_MANUTENZIONI")
						return
					end if
			
					 select flag_tipo_tempo, 
							  contatore			 
						into :ls_flag_tipo_tempo,
							  :ld_contatore		
						from registro_uso_attrezzature
					  where cod_azienda = :s_cs_xx.cod_azienda
						 and cod_attrezzatura = :ls_cod_attrezzatura
						 and prog_uso_attrezzatura = :ll_prog_uso_attrezzatura;
			
					if sqlca.sqlcode = 100 then exit 
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella REGISTRO_USO_MANUTENZIONI")
						return
					end if
					
					if sqlca.sqlcode = 0 then
						ldt_data_max = ldt_data_comodo
						if ls_flag_tipo_tempo = "S" then
							ld_somma_contatore = ld_somma_contatore + ld_contatore
						elseif ls_flag_tipo_tempo = "N" then
							ld_somma_contatore = ld_somma_contatore + ld_contatore
							exit
						end if
					end if
				loop
			
				if ll_tot_ore < ld_somma_contatore then
					
					select cod_azienda
					  into :ls_cod_azienda
					  from manutenzioni
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_attrezzatura = :ls_cod_attrezzatura
						and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
						and flag_eseguito = 'N'
						and flag_ordinario = 'S'
						and anno_registrazione = (select max(anno_registrazione)
															 from manutenzioni
															where cod_azienda = :s_cs_xx.cod_azienda
															  and cod_attrezzatura = :ls_cod_attrezzatura
															  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
															  and flag_eseguito = 'N'
															  and flag_ordinario = 'S')
						and num_registrazione =  (select max(num_registrazione)
															 from manutenzioni
															where cod_azienda = :s_cs_xx.cod_azienda
															  and cod_attrezzatura = :ls_cod_attrezzatura
															  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
															  and flag_eseguito = 'N'
															  and flag_ordinario = 'S')	;						
						
						
					if sqlca.sqlcode < 0 then 	
						g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella MANUTENZIONI" + SQLCA.SQLERRTEXT)
						return
					end if
					
					if sqlca.sqlcode <> 0 then 	
						luo_manutenzioni = CREATE uo_manutenzioni
						if luo_manutenzioni.uof_crea_manutenzione_programmata(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) <> 0 then
							g_mb.messagebox("Manutenzioni","Si è verificato un errore durante la creazione della manutenzione programmata.~r~nDettaglio Errore: " + sqlca.sqlerrtext)
						end if	
					end if	
				end if
			end if	
	//--------------------------------------------- Fine Modifica ------------------------------------------------------	
	
	end if
	
	destroy luo_manutenzioni
loop

close cu_tipi_attrezzature;
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_attrezzatura, new_select, where_clause, ls_count
long ll_prog_attrezzatura, ll_i, ll_errore, ll_index

dw_selezione.accepttext()

ls_cod_attrezzatura = dw_selezione.GetItemString(1, "cod_attrezzatura")
ll_prog_attrezzatura = dw_selezione.GetItemnumber(1, "prog_attrezzatura")

where_clause = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_attrezzatura) then
	where_clause = where_clause + " and cod_attrezzatura = '" + ls_cod_attrezzatura + "'"
end if

if not isnull(ll_prog_attrezzatura) then
	where_clause = where_clause + " and prog_uso_attrezzatura = " + string(ll_prog_attrezzatura)
end if	

where_clause = where_clause + " order by cod_attrezzatura, prog_uso_attrezzatura"

ll_index = pos(dw_registro_uso_attrazz_lista.GETSQLSELECT(), "where")
if ll_index = 0 then
	new_select = dw_registro_uso_attrazz_lista.GETSQLSELECT() + where_clause
else	
	new_select = left(dw_registro_uso_attrazz_lista.GETSQLSELECT(), ll_index - 1) + where_clause
end if

dw_registro_uso_attrazz_lista.SetSQLSelect(new_select)

dw_registro_uso_attrazz_lista.resetupdate()

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_new;call super::pcd_new;dw_registro_uso_attrezz_dett.setitem(dw_registro_uso_attrazz_lista.getrow(), "flag_tipo_tempo", "N")

dw_registro_uso_attrezz_dett.object.b_ricerca_att.enabled = true
  
end event

event pcd_close;call super::pcd_close;//dw_selezione.resetupdate()
end event

event pcd_view;call super::pcd_view;dw_registro_uso_attrezz_dett.object.b_ricerca_att.enabled = false
end event

event pcd_save;call super::pcd_save;dw_registro_uso_attrezz_dett.object.b_ricerca_att.enabled = false
end event

event updateend;call super::updateend;dw_registro_uso_attrazz_lista.postevent("ue_gen_prog_man")
end event

event pcd_modify;call super::pcd_modify;dw_registro_uso_attrezz_dett.object.b_ricerca_att.enabled = true
end event

type dw_folder_search from u_folder within w_registro_uso_attrez
integer x = 23
integer y = 20
integer width = 2926
integer height = 676
integer taborder = 40
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "L."
      SetFocus(dw_registro_uso_attrazz_lista)
   CASE "R."
      SetFocus(dw_folder_search)
END CHOOSE
end event

