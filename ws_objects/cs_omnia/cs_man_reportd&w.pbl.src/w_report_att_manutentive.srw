﻿$PBExportHeader$w_report_att_manutentive.srw
forward
global type w_report_att_manutentive from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_att_manutentive
end type
type dw_report from uo_cs_xx_dw within w_report_att_manutentive
end type
type dw_folder from u_folder within w_report_att_manutentive
end type
type dw_ricerca from uo_cs_xx_dw within w_report_att_manutentive
end type
type cb_cancella_filtri from commandbutton within w_report_att_manutentive
end type
type cb_aggiorna from commandbutton within w_report_att_manutentive
end type
type st_status from statictext within w_report_att_manutentive
end type
end forward

global type w_report_att_manutentive from w_cs_xx_principale
integer width = 3771
integer height = 2328
string title = "Report Attività Manutentive"
cb_stampa cb_stampa
dw_report dw_report
dw_folder dw_folder
dw_ricerca dw_ricerca
cb_cancella_filtri cb_cancella_filtri
cb_aggiorna cb_aggiorna
st_status st_status
end type
global w_report_att_manutentive w_report_att_manutentive

type variables

end variables

forward prototypes
public function integer wf_report ()
public function integer wf_get_stato_manutenzione (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_ultima_eseguita, ref string fs_prossima_scadenza)
end prototypes

public function integer wf_report ();datetime 	ldt_da_data_scadenza, ldt_a_data_scadenza, ldt_data_registrazione
string 		ls_da_cod_attrezzatura_filtro, ls_a_cod_attrezzatura_filtro, ls_cod_tipo_manutenzione_filtro
string 		ls_cod_cat_attrez_filtro, ls_cod_divisione_filtro, ls_cod_area_filtro, ls_cod_reparto_filtro
long			ll_index, ll_rows, ll_riga
string			ls_sql,ls_sintassi, ls_errore
datastore	lds_dati
long			ll_anno_registrazione, ll_num_registrazione
string			ls_cod_attrezzatura, ls_des_attrezzatura, ls_cod_tipo_manutenzione, ls_filtro
string			ls_des_tipo_manutenzione, ls_periodicita, ls_frequenza
string			ls_ultima_eseguita, ls_prossima_scadenza
long			ll_frequenza

st_status.text = ""
dw_ricerca.accepttext()
//------------------
ldt_da_data_scadenza					= dw_ricerca.getitemdatetime(1,"rdt_da_scadenza")
ldt_a_data_scadenza 						= dw_ricerca.getitemdatetime(1,"rdt_a_scadenza")
//------------------
ls_da_cod_attrezzatura_filtro 			= dw_ricerca.getitemstring(1,"rs_da_cod_attrezzatura")
ls_a_cod_attrezzatura_filtro 			= dw_ricerca.getitemstring(1,"rs_a_cod_attrezzatura")
ls_cod_tipo_manutenzione_filtro 		= dw_ricerca.getitemstring(1,"rs_cod_tipo_manutenzione")
//------------------
ls_cod_cat_attrez_filtro 					= dw_ricerca.getitemstring( 1, "rs_cod_cat_attrezzatura")
ls_cod_divisione_filtro 					= dw_ricerca.getitemstring( 1, "rs_cod_divisione")
ls_cod_area_filtro 							= dw_ricerca.getitemstring(1,"rs_cod_area_aziendale")
ls_cod_reparto_filtro 						= dw_ricerca.getitemstring(1,"rs_cod_reparto")
//------------------

setpointer(HourGlass!)

dw_report.reset()

st_status.text = "preparazione query in corso..."

ls_sql = "select "+&
				"manutenzioni.anno_registrazione,"+&
				"manutenzioni.num_registrazione,"+&
				"manutenzioni.data_registrazione,"+&
				"manutenzioni.cod_attrezzatura,"+&
				"anag_attrezzature.descrizione,"+&
				"manutenzioni.cod_tipo_manutenzione,"+&
				"tab_tipi_manutenzione.des_tipo_manutenzione,"+&
				"tab_tipi_manutenzione.frequenza,"+&
				"tab_tipi_manutenzione.periodicita "+&
			"from manutenzioni "+&
			"join anag_attrezzature on anag_attrezzature.cod_azienda = manutenzioni.cod_azienda and "+&
				"anag_attrezzature.cod_attrezzatura = manutenzioni.cod_attrezzatura "+&
			"join tab_tipi_manutenzione on tab_tipi_manutenzione.cod_azienda = anag_attrezzature.cod_azienda and "+&
				"tab_tipi_manutenzione.cod_attrezzatura = anag_attrezzature.cod_attrezzatura and "+&
				"tab_tipi_manutenzione.cod_azienda = manutenzioni.cod_azienda and "+&
				"tab_tipi_manutenzione.cod_attrezzatura = manutenzioni.cod_attrezzatura and "+&
				"tab_tipi_manutenzione.cod_tipo_manutenzione = manutenzioni.cod_tipo_manutenzione "+&
			"left outer join tab_aree_aziendali on tab_aree_aziendali.cod_azienda = anag_attrezzature.cod_azienda and "+&
    				"tab_aree_aziendali.cod_area_aziendale = anag_attrezzature.cod_area_aziendale "+&
			"where manutenzioni.cod_azienda = '"+s_cs_xx.cod_azienda+"' and manutenzioni.flag_eseguito = 'N' "

//applicazione eventuali altri filtri -------------------------------------
ls_filtro = ""

//DATA SCADENZA -----------------------------------------------------------------------------
//da data scadenza
if not isnull(ldt_da_data_scadenza) then
	ls_sql += "and manutenzioni.data_registrazione >= '"+string(ldt_da_data_scadenza,s_cs_xx.db_funzioni.formato_data)+"' "
end if

//a data scadenza
if not isnull(ldt_a_data_scadenza) then
	ls_sql += "and manutenzioni.data_registrazione <= '"+string(ldt_a_data_scadenza,s_cs_xx.db_funzioni.formato_data)+"' "
end if

if not isnull(ldt_da_data_scadenza) then
	if not isnull(ldt_a_data_scadenza) then
		ls_filtro = "Scadenza da " + string(ldt_da_data_scadenza,"dd/mm/yy") + " a " + string(ldt_a_data_scadenza,"dd/mm/yy")
	else
		ls_filtro = "Scadenza a partire da "+ string(ldt_da_data_scadenza,"dd/mm/yy")
	end if
else
	if not isnull(ldt_a_data_scadenza) then
		ls_filtro = "Scadenza fino a "+ string(ldt_a_data_scadenza,"dd/mm/yy")
	else
		ls_filtro = ""
	end if
end if
//------------------------------------------------------------------------------------------------

//ATTREZZATURA -------------------------------------------------------------------------
//da attrezzatura
if not isnull(ls_da_cod_attrezzatura_filtro) and ls_da_cod_attrezzatura_filtro <> "" then
	ls_sql += "and manutenzioni.cod_attrezzatura >= '"+ls_da_cod_attrezzatura_filtro+"' "
end if

//ad attrezzatura
if not isnull(ls_a_cod_attrezzatura_filtro) and ls_a_cod_attrezzatura_filtro <> "" then
	ls_sql += "and manutenzioni.cod_attrezzatura <= '"+ls_a_cod_attrezzatura_filtro+"' "
end if

if not isnull(ls_da_cod_attrezzatura_filtro) and ls_da_cod_attrezzatura_filtro <> "" then
	//hai specificato attrezzatura da
	if not isnull(ls_a_cod_attrezzatura_filtro) and ls_a_cod_attrezzatura_filtro <> "" then
		//hai specificato pure attrezzatura a
		if ls_filtro <> "" then ls_filtro += " - "
		ls_filtro += " Da Attrezzatura '"+ls_da_cod_attrezzatura_filtro+"' ad Attrezzatura '" + ls_a_cod_attrezzatura_filtro + "'"
	else
		//hai specificato solo attrezzatura da
		if ls_filtro <> "" then ls_filtro += " - "
		ls_filtro += " Da Attrezzatura '"+ls_da_cod_attrezzatura_filtro+"'"
	end if
else
	if not isnull(ls_a_cod_attrezzatura_filtro) and ls_a_cod_attrezzatura_filtro <> "" then
		//hai specificato attrezzatura a ma non attrezzatura da
		if ls_filtro <> "" then ls_filtro += " - "
		ls_filtro += " Fino ad Attrezzatura '"+ls_a_cod_attrezzatura_filtro+"'"
	else
		//non hai specificato niente per le attrezzature		
	end if
end if
//------------------------------------------------------------------------------------------------

//tipo manutenzione
if not isnull(ls_cod_tipo_manutenzione_filtro) and ls_cod_tipo_manutenzione_filtro <> "" then
	ls_sql += "and manutenzioni.cod_tipo_manutenzione = '"+ls_cod_tipo_manutenzione_filtro+"' "
	
	if ls_filtro <> "" then ls_filtro += " - "
	ls_filtro += " Tipo Manutenzione '"+ls_cod_tipo_manutenzione_filtro+"'"
end if

//categoria attrezzatura
if not isnull(ls_cod_cat_attrez_filtro) and ls_cod_cat_attrez_filtro <> "" then
	ls_sql += "and anag_attrezzature.cod_cat_attrezzature = '"+ls_cod_cat_attrez_filtro+"' "
	
	if ls_filtro <> "" then ls_filtro += " - "
	ls_filtro += " Categ.Attrezzatura '"+ls_cod_cat_attrez_filtro+"'"
end if

//divisione
if not isnull(ls_cod_divisione_filtro) and ls_cod_divisione_filtro <> "" then
	ls_sql += "and tab_aree_aziendali.cod_divisione = '"+ls_cod_divisione_filtro+"' "
	
	if ls_filtro <> "" then ls_filtro += " - "
	ls_filtro += " Divisione '"+ls_cod_divisione_filtro+"'"
end if

//area aziendale
if not isnull(ls_cod_area_filtro) and ls_cod_area_filtro <> "" then
	ls_sql += "and anag_attrezzature.cod_area_aziendale = '"+ls_cod_area_filtro+"' "
	
	if ls_filtro <> "" then ls_filtro += " - "
	ls_filtro += " Area Aziendale '"+ls_cod_area_filtro+"'"
end if

//reparto
if not isnull(ls_cod_reparto_filtro) and ls_cod_reparto_filtro <> "" then
	ls_sql += "and anag_attrezzature.cod_reparto = '"+ls_cod_reparto_filtro+"' "
	
	if ls_filtro <> "" then ls_filtro += " - "
	ls_filtro += " Reparto '"+ls_cod_reparto_filtro+"'"
end if

if ls_filtro = "" then ls_filtro = "Nessun filtro applicato"

//CLAUSOLA ORDER BY
ls_sql += "order by manutenzioni.cod_attrezzatura, manutenzioni.cod_tipo_manutenzione "
//---------------------------------------------------------------------------

ls_sintassi = sqlca.syntaxfromsql( ls_sql, "style(type=grid)", ls_errore)
if Len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
	setpointer(Arrow!)
	st_status.text = ""
	return -1
end if

lds_dati = create datastore

lds_dati.Create( ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	destroy lds_dati;
	setpointer(Arrow!)
	st_status.text = ""
	return -1
end if

lds_dati.settransobject(sqlca)

ll_rows = lds_dati.retrieve()
if ll_rows < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe del datastore:" + sqlca.sqlerrtext)
	destroy lds_dati;
	setpointer(Arrow!)
	st_status.text = ""
	return -1
end if

for ll_index = 1 to ll_rows
	ll_anno_registrazione				= lds_dati.getitemnumber(ll_index, 1)
	ll_num_registrazione				= lds_dati.getitemnumber(ll_index, 2)
	ldt_data_registrazione			= lds_dati.getitemdatetime(ll_index, 3)
	ls_cod_attrezzatura				= lds_dati.getitemstring(ll_index, 4)
	ls_des_attrezzatura				= lds_dati.getitemstring(ll_index, 5)
	ls_cod_tipo_manutenzione		= lds_dati.getitemstring(ll_index, 6)
	ls_des_tipo_manutenzione		= lds_dati.getitemstring(ll_index, 7)
	ll_frequenza							= lds_dati.getitemnumber(ll_index, 8)
	ls_periodicita						= lds_dati.getitemstring(ll_index, 9)
	
	st_status.text = string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " - " + &
				"Attrezzatura: '" + ls_cod_attrezzatura + "'"
	
	ll_riga = dw_report.insertrow(0)
	dw_report.setitem(ll_riga,"anno_reg",ll_anno_registrazione)
	dw_report.setitem(ll_riga,"num_reg",ll_num_registrazione)
	dw_report.setitem(ll_riga,"cod_attrezzatura",ls_cod_attrezzatura)
	dw_report.setitem(ll_riga,"des_attrezzatura",ls_des_attrezzatura)
	dw_report.setitem(ll_riga,"cod_tipo_manutenzione",ls_cod_tipo_manutenzione)
	dw_report.setitem(ll_riga,"des_tipo_manutenzione",ls_des_tipo_manutenzione)
	
	ls_frequenza = ""
	if not isnull(ll_frequenza) and ll_frequenza > 0 and &
		not isnull(ls_periodicita) and ls_periodicita <> "" then
		
		ls_frequenza = string(ll_frequenza) + "/"
		
		choose case ls_periodicita
			case "M"	//minuti
				ls_frequenza += "minuti"
				
			case "O"	//ore
				ls_frequenza += "ore"
				
			case "G"	//giorni
				ls_frequenza += "giorni"
				
			case "S"	//settimane
				ls_frequenza += "settimane"
				
			case "E"	//mesi
				ls_frequenza += "mesi"
				
			case else
				ls_frequenza += "non prev."
		end choose
	end if	
	dw_report.setitem(ll_riga,"frequenza",ls_frequenza)
	
	ls_ultima_eseguita = ""
	ls_prossima_scadenza = ""
	
	wf_get_stato_manutenzione(ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ll_anno_registrazione, ll_num_registrazione, &
											ls_ultima_eseguita, ls_prossima_scadenza)
	
	dw_report.setitem(ll_riga,"ultima_eseguita",ls_ultima_eseguita)
	
	if not isnull(ldt_data_registrazione) then
		ls_prossima_scadenza = string(ldt_data_registrazione, "dd/mm/yyyy")
	else
		ls_prossima_scadenza = ""
	end if
	dw_report.setitem(ll_riga,"prossima_scadenza",ls_prossima_scadenza)
next

dw_report.object.t_filtro.text = ls_filtro
dw_report.object.datawindow.print.preview = "yes"

dw_report.resetupdate()
setpointer(Arrow!)
st_status.text = ""

return 0
end function

public function integer wf_get_stato_manutenzione (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_ultima_eseguita, ref string fs_prossima_scadenza);string				ls_return_on_errore = "Errore in elaboraz.!"
string				ls_sql, ls_sintassi, ls_errore
datastore		lds_dati
long				ll_righe
datetime			ldt_data

setpointer(HourGlass!)

//calcolo data ultima eseguita --------------------------------------------------------
ls_sql = "select " +&
				"max(data_registrazione) "+&
				"from manutenzioni "+&
				"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
					"cod_attrezzatura = '"+fs_cod_attrezzatura +"' and "+&
					"cod_tipo_manutenzione = '"+fs_cod_tipo_manutenzione+"' and "+&
					"flag_eseguito = 'S'"

ls_sintassi = sqlca.syntaxfromsql( ls_sql, "style(type=grid)", ls_errore)
if len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore (Ultima Eseguita):" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
	setpointer(Arrow!)
	return -1
end if

lds_dati = create datastore

lds_dati.Create( ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore (Ultima Eseguita):" + ls_errore)
	destroy lds_dati;
	setpointer(Arrow!)
	return -1
end if

lds_dati.settransobject(sqlca)

ll_righe = lds_dati.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe del datastore (Ultima Eseguita):" + sqlca.sqlerrtext)
	destroy lds_dati;
	setpointer(Arrow!)
	return -1
elseif ll_righe > 0 then
	//vai solo sulla prima riga
	ldt_data = lds_dati.getitemdatetime(1, 1)
	if not isnull(ldt_data) then
		fs_ultima_eseguita = string(ldt_data, "dd/mm/yyyy")
	else
		fs_ultima_eseguita = ""
	end if
else	
	fs_ultima_eseguita = ""
end if
destroy lds_dati;
//------------------------------------------------------------------------

////calcolo data prossima scadenza-----------------------------------
//ls_sql = "select " +&
//				"data_prossimo_intervento " + &
//				"from manutenzioni " + &
//				"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
//					"anno_registrazione = " + string(fl_anno_registrazione) +" and "+&
//					"num_registrazione = " + string(fl_num_registrazione)
//
//ls_sintassi = sqlca.syntaxfromsql( ls_sql, "style(type=grid)", ls_errore)
//if len(ls_errore) > 0 then
//	g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore (Pross.Scadenza):" + ls_errore)
//	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
//	setpointer(Arrow!)
//	return -1
//end if
//
//lds_dati = create datastore
//
//lds_dati.Create( ls_sintassi, ls_errore)
//if Len(ls_errore) > 0 then
//	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore (Pross.Scadenza):" + ls_errore)
//	destroy lds_dati;
//	setpointer(Arrow!)
//	return -1
//end if
//
//lds_dati.settransobject(sqlca)
//
//ll_righe = lds_dati.retrieve()
//if ll_righe < 0 then
//	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe del datastore (Pross.Scadenza):" + sqlca.sqlerrtext)
//	destroy lds_dati;
//	setpointer(Arrow!)
//	return -1
//elseif ll_righe > 0 then
//	//se entri qui c'è una sola riga
//	ldt_data = lds_dati.getitemdatetime(1, 1)
//	if not isnull(ldt_data) then
//		fs_prossima_scadenza = string(ldt_data, "dd/mm/yyyy")
//	else
//		fs_prossima_scadenza = ""
//	end if
//else	
//	fs_prossima_scadenza = ""
//end if
//destroy lds_dati;
////-----------------------------------------------------------------------

setpointer(Arrow!)
return 1
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

windowobject lw_oggetti[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_ricerca.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

//dw_report.set_dw_options(sqlca, &
//									pcca.null_object, &
//									c_nonew + &
//									c_nomodify + &
//									c_noretrieveonopen + &
//									c_nodelete + &
//                                       c_disableCC, &
//                                       c_noresizedw + &
//                                       c_nohighlightselected + &
//                                       c_nocursorrowpointer +&
//                                       c_nocursorrowfocusrect )
													
// *** folder													

lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = cb_cancella_filtri
lw_oggetti[3] = cb_aggiorna
lw_oggetti[4] = st_status
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)

select stringa
into   :ls_path_logo
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

dw_report.modify(ls_modify)

//iuo_dw_main = dw_report
dw_report.object.datawindow.print.preview = 'Yes'
end event

on w_report_att_manutentive.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
this.cb_cancella_filtri=create cb_cancella_filtri
this.cb_aggiorna=create cb_aggiorna
this.st_status=create st_status
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_ricerca
this.Control[iCurrent+5]=this.cb_cancella_filtri
this.Control[iCurrent+6]=this.cb_aggiorna
this.Control[iCurrent+7]=this.st_status
end on

on w_report_att_manutentive.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
destroy(this.cb_cancella_filtri)
destroy(this.cb_aggiorna)
destroy(this.st_status)
end on

event pc_setddlb;call super::pc_setddlb;//cod_tipo_manutenzione
f_PO_LoadDDDW_DW (dw_ricerca,"rs_cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//categoria attrezzatura
f_PO_LoadDDDW_DW (dw_ricerca,"rs_cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'") // and flag_risorsa_umana = 'N' ")

//cod_divisione
f_PO_LoadDDDW_DW (dw_ricerca,"rs_cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")		

//cod_area_aziendale
f_po_loaddddw_dw(dw_ricerca,"rs_cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
					  "des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//cod_reparto
f_po_loaddddw_dw(dw_ricerca,"rs_cod_reparto",sqlca,"anag_reparti","cod_reparto","des_reparto",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


					 
end event

type cb_stampa from commandbutton within w_report_att_manutentive
integer x = 69
integer y = 152
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_report.print(false, true)







end event

type dw_report from uo_cs_xx_dw within w_report_att_manutentive
integer x = 46
integer y = 244
integer width = 3639
integer height = 1936
integer taborder = 80
string title = "none"
string dataobject = "d_report_att_manutentiva"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_folder from u_folder within w_report_att_manutentive
integer x = 23
integer width = 3680
integer height = 2208
integer taborder = 10
end type

type dw_ricerca from uo_cs_xx_dw within w_report_att_manutentive
event ue_carica_tipologie ( )
event ue_cod_attr ( )
event ue_aree_aziendali ( )
integer x = 46
integer y = 136
integer width = 2784
integer height = 868
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_sel_report_att_manutentiva"
boolean border = false
end type

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"rs_da_cod_attrezzatura")

ls_attr_a = getitemstring(getrow(),"rs_a_cod_attrezzatura")

ls_reparto = getitemstring(getrow(),"rs_cod_reparto")

ls_categoria = getitemstring(getrow(),"rs_cod_cat_attrezzatura")

ls_area = getitemstring(getrow(),"rs_cod_area_aziendale")

setnull(ls_null)

setitem(getrow(),"rs_cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"rs_cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_cod_attr();if not isnull(getitemstring(getrow(),"rs_da_cod_attrezzatura")) then
	setitem(getrow(),"rs_a_cod_attrezzatura",getitemstring(getrow(),"rs_da_cod_attrezzatura"))
end if
end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"rs_cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_DW (this,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "'")
else
	f_PO_LoadDDDW_DW (this,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  	
end if

triggerevent("ue_carica_tipologie")
end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
		
	case "rs_da_cod_attrezzatura"
		postevent("ue_cod_attr")
		postevent("ue_carica_tipologie")
		
	case "rs_a_cod_attrezzatura","rs_cod_reparto","rs_cod_cat_attrezzatura","rs_cod_area_aziendale"
		postevent("ue_carica_tipologie")
		
	case "rs_cod_divisione"
		postevent("ue_aree_aziendali")
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca,"rs_da_cod_attrezzatura")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca,"rs_a_cod_attrezzatura")
end choose
end event

type cb_cancella_filtri from commandbutton within w_report_att_manutentive
integer x = 1783
integer y = 996
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string   ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_ricerca.setitem(1,"rdt_da_scadenza",ldt_null)
dw_ricerca.setitem(1,"rdt_a_scadenza",ldt_null)

dw_ricerca.setitem(1,"rs_da_cod_attrezzatura",ls_null)
dw_ricerca.setitem(1,"rs_a_cod_attrezzatura",ls_null)
dw_ricerca.setitem(1,"rs_cod_tipo_manutenzione",ls_null)

dw_ricerca.setitem(1,"rs_cod_cat_attrezzatura",ls_null)
dw_ricerca.setitem(1,"rs_cod_divisione",ls_null)
dw_ricerca.setitem(1,"rs_cod_area_aziendale",ls_null)
dw_ricerca.setitem(1,"rs_cod_reparto",ls_null)








end event

type cb_aggiorna from commandbutton within w_report_att_manutentive
integer x = 2181
integer y = 996
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;call super::clicked;dw_ricerca.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "rs_da_cod_attrezzatura"
end event

type st_status from statictext within w_report_att_manutentive
integer x = 64
integer y = 912
integer width = 2537
integer height = 72
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

