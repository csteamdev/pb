﻿$PBExportHeader$w_report_attrezzature_lista.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_attrezzature_lista from w_cs_xx_principale
end type
type cb_esporta from commandbutton within w_report_attrezzature_lista
end type
type cb_stampa from commandbutton within w_report_attrezzature_lista
end type
type cb_annulla from commandbutton within w_report_attrezzature_lista
end type
type cb_report from commandbutton within w_report_attrezzature_lista
end type
type dw_selezione from uo_cs_xx_dw within w_report_attrezzature_lista
end type
type dw_report from uo_cs_xx_dw within w_report_attrezzature_lista
end type
type dw_folder from u_folder within w_report_attrezzature_lista
end type
end forward

global type w_report_attrezzature_lista from w_cs_xx_principale
integer width = 3762
integer height = 2228
string title = "Report Attrezzature"
cb_esporta cb_esporta
cb_stampa cb_stampa
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_attrezzature_lista w_report_attrezzature_lista

type variables

end variables

event pc_setwindow;call super::pc_setwindow;windowobject					lw_oggetti[]





dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

// *** folder													

lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
lw_oggetti[3] = cb_esporta
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_report
lw_oggetti[3] = cb_annulla
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)
													
// ***
dw_selezione.object.flag_includi_bloccate.visible = false






end event

on w_report_attrezzature_lista.create
int iCurrent
call super::create
this.cb_esporta=create cb_esporta
this.cb_stampa=create cb_stampa
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_esporta
this.Control[iCurrent+2]=this.cb_stampa
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.dw_selezione
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.dw_folder
end on

on w_report_attrezzature_lista.destroy
call super::destroy
destroy(this.cb_esporta)
destroy(this.cb_stampa)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"rs_da_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_PO_LoadDDDW_DW(dw_selezione,"rs_a_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cdstab IS NOT NULL ")					  
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  					  

end event

type cb_esporta from commandbutton within w_report_attrezzature_lista
integer x = 2857
integer y = 1960
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esporta"
end type

event clicked;dw_report.SaveAs("", Excel!, true)
end event

type cb_stampa from commandbutton within w_report_attrezzature_lista
integer x = 3246
integer y = 1960
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;printsetup()

if g_mb.MessageBox( "OMNIA", "Continuare con la stampa?", Exclamation!, yesno!, 2) = 1 then

	dw_report.print()
	
end if
end event

type cb_annulla from commandbutton within w_report_attrezzature_lista
integer x = 1463
integer y = 880
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_attrezzature_lista
integer x = 1851
integer y = 880
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string				ls_null, ls_da_attrezzatura, ls_a_attrezzatura, ls_cat_attrezzature, ls_cod_area_aziendale, &
						ls_cod_reparto, ls_where, ls_sql, ls_flag_mezzi, ls_des_area
						
long					ll_errore


setnull(ls_null)

dw_report.reset()

dw_selezione.accepttext()

dw_report.setredraw(false)

ls_da_attrezzatura = dw_selezione.getitemstring(1,"rs_da_cod_attrezzatura")
ls_a_attrezzatura = dw_selezione.getitemstring(1,"rs_a_cod_attrezzatura")
ls_cat_attrezzature = dw_selezione.getitemstring(1,"rs_cod_cat_attrezzatura")
ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
ls_cod_reparto = dw_selezione.getitemstring(1,"rs_cod_reparto")
ls_flag_mezzi = dw_selezione.getitemstring( 1, "flag_includi_mezzi")

if isnull(ls_flag_mezzi) then ls_flag_mezzi = "N"

ls_where = " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" then
	ls_where += " AND cod_area_aziendale = '" + ls_cod_area_aziendale + "' "
end if

if not isnull(ls_cod_reparto) and ls_cod_reparto <> "" then
	ls_where += " AND cod_reparto = '" + ls_cod_reparto + "' "
end if

if not isnull(ls_cat_attrezzature) and ls_cat_attrezzature <> "" then
	ls_where += " AND cod_cat_attrezzature = '" + ls_cat_attrezzature + "' "
end if

if not isnull(ls_da_attrezzatura) and ls_da_attrezzatura <> "" then
	ls_where += " AND cod_attrezzatura >= '" + ls_da_attrezzatura + "' "
end if

if not isnull(ls_a_attrezzatura) and ls_a_attrezzatura <> "" then
	ls_where += " AND cod_attrezzatura = '" + ls_a_attrezzatura + "' "
end if

if not isnull(ls_flag_mezzi) and ls_flag_mezzi = "N" then
	ls_where += " AND cod_reparto <> 'M' "
end if

ls_where += " ORDER BY cod_reparto ASC, cod_cat_attrezzature ASC, cod_attrezzatura ASC "

ls_sql = "SELECT anag_attrezzature.cod_reparto,   " + &
			"			anag_attrezzature.cod_cat_attrezzature,  " + &   
			"			anag_attrezzature.cod_attrezzatura,     " + &
			"			anag_attrezzature.descrizione,     " + &
			"			anag_attrezzature.flag_primario,    " + &
			"   			'' as attivita,      " + &
	         "   			'' as fornitore,      " + &
         	"   			'' as esecutore     " + &
			"FROM  anag_attrezzature   " +  ls_where

dw_report.SetSQLSelect( ls_sql)

dw_report.resetupdate()

ll_errore = dw_report.retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
else
	
	dw_report.setredraw(true)
	
	if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" then
		
		select des_area
		into	:ls_des_area
		from	tab_aree_aziendali
		where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_area_aziendale = :ls_cod_area_aziendale;
				
		dw_report.object.t_impianto.text = ls_des_area
	else
		dw_report.object.t_impianto.text = ''
	end if
	dw_report.Object.DataWindow.Print.Preview = 'Yes'
	dw_folder.fu_selecttab(2)
	dw_report.change_dw_current()	
	
end if
end event

type dw_selezione from uo_cs_xx_dw within w_report_attrezzature_lista
integer x = 137
integer y = 200
integer width = 2080
integer height = 652
integer taborder = 20
string dataobject = "d_selezione_report_attrezzature_lista"
end type

event itemchanged;call super::itemchanged;string ls_cod_divisione, ls_cod_area_aziendale

dw_selezione.accepttext()
choose case i_colname
	case "rs_cod_divisione"
		ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")		
		if isnull(ls_cod_divisione) then
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
		else			  
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + i_coltext + "'")					  					  
		end if				  
		
	case "rs_cod_area_aziendale"
		ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")		
		if isnull(ls_cod_area_aziendale) then
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
						  "anag_reparti","cod_reparto","des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
		else						  
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
						  "anag_reparti","cod_reparto","des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + i_coltext + "'")						
		end if	
end choose





end event

type dw_report from uo_cs_xx_dw within w_report_attrezzature_lista
boolean visible = false
integer x = 137
integer y = 200
integer width = 3474
integer height = 1740
integer taborder = 10
string dataobject = "d_report_attrezzature_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_attrezzature_lista
integer x = 23
integer y = 20
integer width = 3680
integer height = 2080
integer taborder = 60
end type

event po_tabclicked;call super::po_tabclicked;//CHOOSE CASE i_SelectedTabName
//   CASE "Report"
//      SetFocus(dw_report_manut_eseguite)
//		
//   CASE "Selezione"
//      SetFocus(dw_sel_manut_eseguite)
//
//END CHOOSE
//
end event

