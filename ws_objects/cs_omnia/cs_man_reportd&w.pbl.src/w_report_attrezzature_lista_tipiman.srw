﻿$PBExportHeader$w_report_attrezzature_lista_tipiman.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_attrezzature_lista_tipiman from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_attrezzature_lista_tipiman
end type
type cb_annulla from commandbutton within w_report_attrezzature_lista_tipiman
end type
type cb_report from commandbutton within w_report_attrezzature_lista_tipiman
end type
type dw_selezione from uo_cs_xx_dw within w_report_attrezzature_lista_tipiman
end type
type dw_report from uo_cs_xx_dw within w_report_attrezzature_lista_tipiman
end type
type dw_folder from u_folder within w_report_attrezzature_lista_tipiman
end type
end forward

global type w_report_attrezzature_lista_tipiman from w_cs_xx_principale
integer width = 4357
integer height = 2536
string title = "Report Tipologie Manutenzioni"
cb_stampa cb_stampa
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_attrezzature_lista_tipiman w_report_attrezzature_lista_tipiman

type variables


string			is_AZP= ""
string			is_where_sintexcal
end variables

forward prototypes
public subroutine wf_stato_tipo_manutenzione (string as_cod_attrezzatura, string as_cod_tipo_manutenzione, ref string as_stato_programmazione, ref datetime adt_ultima_eseguita, ref datetime adt_prossima_da_eseguire)
end prototypes

public subroutine wf_stato_tipo_manutenzione (string as_cod_attrezzatura, string as_cod_tipo_manutenzione, ref string as_stato_programmazione, ref datetime adt_ultima_eseguita, ref datetime adt_prossima_da_eseguire);
long		ll_count_man_aperte, ll_count_piani


//stato programmazione ----------------------------------------------------------------------------------------

//manutenzioni aperte
select count(*)
into :ll_count_man_aperte
from manutenzioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			flag_eseguito='N' and
			cod_attrezzatura=:as_cod_attrezzatura and
			cod_tipo_manutenzione=:as_cod_tipo_manutenzione;
			
if not isnull(ll_count_man_aperte) and ll_count_man_aperte>0 then
	as_stato_programmazione = "Programmata"
	
else
	//controlla i piani di manutenzione
	select count(*)
	into :ll_count_piani
	from programmi_manutenzione
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				flag_blocco<>'S' and
				cod_attrezzatura=:as_cod_attrezzatura and
				cod_tipo_manutenzione=:as_cod_tipo_manutenzione;
	
	if not isnull(ll_count_piani) and ll_count_piani>0 then
		as_stato_programmazione = "Interrotta"
	else
		as_stato_programmazione = "Non Programmata"
	end if
end if


//ultima eseguita ------------------------------------------------------------------------------------------------
select max(data_registrazione)
into :adt_ultima_eseguita
from manutenzioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			flag_eseguito='S' and
			cod_attrezzatura=:as_cod_attrezzatura and
			cod_tipo_manutenzione=:as_cod_tipo_manutenzione;

if year(date(adt_ultima_eseguita))<1950 then setnull(adt_ultima_eseguita)



//prossima da eseguire ----------------------------------------------------------------------------------------
//     solo se c'è almeno una manutenzione aperta
if ll_count_man_aperte=0 or isnull(ll_count_man_aperte) then
	setnull(adt_prossima_da_eseguire)
	
else
	select min(data_registrazione)
	into :adt_prossima_da_eseguire
	from manutenzioni
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				flag_eseguito='N' and
				cod_attrezzatura=:as_cod_attrezzatura and
				cod_tipo_manutenzione=:as_cod_tipo_manutenzione;
	
	if year(date(adt_prossima_da_eseguire))<1950 then setnull(adt_prossima_da_eseguire)
	
end if

return
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject					lw_oggetti[]





dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

// *** folder													

lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_report
lw_oggetti[3] = cb_annulla
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)
													
// ***

guo_functions.uof_get_parametro_azienda("AZP", is_AZP)
if isnull(is_AZP) then is_AZP=""

if is_AZP="" then
	//no sintexcal
	dw_selezione.object.rs_cod_area_aziendale.visible = false
	dw_selezione.object.compute_2.visible = false
	dw_selezione.object.rs_cod_area_aziendale_t.visible = false
	
	dw_selezione.object.flag_includi_mezzi.visible = false
else
	//sintexcal
	dw_selezione.object.rs_cod_area_aziendale.visible = true
	dw_selezione.object.compute_2.visible = true
	dw_selezione.object.rs_cod_area_aziendale_t.visible = true
	
	dw_selezione.object.flag_includi_mezzi.visible = true
	
end if


is_where_sintexcal = "and tm.cod_tipo_manutenzione in "+&
																	"(	select cod_tipo_manutenzione "+&
																	   "from tab_tipi_manutenzione "+&
																	   "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
																					"flag_scad_mezzi<>'S' and "+&
																					"cod_tipo_manutenzione<>'MENSRG') "






end event

on w_report_attrezzature_lista_tipiman.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
end on

on w_report_attrezzature_lista_tipiman.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"rs_da_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_PO_LoadDDDW_DW(dw_selezione,"rs_a_cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
if is_AZP<>"" then
	f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cdstab IS NOT NULL ")
else
	f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end if
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  					  

end event

event resize;
dw_folder.resize(newwidth - 40, newheight - 40)

dw_report.resize(newwidth - 100, newheight - 250 - cb_stampa.height)

cb_stampa.move(dw_report.x + dw_report.width - cb_stampa.width - 40, dw_report.y + dw_report.height + 40)
end event

type cb_stampa from commandbutton within w_report_attrezzature_lista_tipiman
integer x = 3927
integer y = 2276
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
if g_mb.confirm("Stampare il report?") then
	dw_report.print(true, true)
end if
end event

type cb_annulla from commandbutton within w_report_attrezzature_lista_tipiman
integer x = 1367
integer y = 820
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_attrezzature_lista_tipiman
integer x = 1755
integer y = 820
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string				ls_null, ls_da_attrezzatura, ls_a_attrezzatura, ls_cat_attrezzature, ls_cod_area_aziendale, &
						ls_cod_reparto, ls_sql, ls_flag_mezzi, ls_des_area, ls_flag_includi_bloccate, ls_errore, ls_azienda

long					ll_index, ll_tot, ll_new, ll_count

datastore			lds_data

string				ls_cod_attrezzatura_ds, ls_des_attrezzatura_ds, ls_cod_tipo_man_ds, ls_des_tipo_man_ds, ls_periodicita_ds, &
						ls_modalita_esecuzione_ds, ls_stato_programmazione, ls_temp
long					ll_frequenza_ds
datetime			ldt_data_ultima_eseguita, ldt_data_prossima_da_eseguire


setnull(ls_null)

dw_report.reset()

dw_selezione.accepttext()

dw_report.setredraw(false)

ls_da_attrezzatura = dw_selezione.getitemstring(1,"rs_da_cod_attrezzatura")
ls_a_attrezzatura = dw_selezione.getitemstring(1,"rs_a_cod_attrezzatura")
ls_cat_attrezzature = dw_selezione.getitemstring(1,"rs_cod_cat_attrezzatura")
ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
ls_cod_reparto = dw_selezione.getitemstring(1,"rs_cod_reparto")
ls_flag_mezzi = dw_selezione.getitemstring( 1, "flag_includi_mezzi")
ls_flag_includi_bloccate = dw_selezione.getitemstring( 1, "flag_includi_bloccate")

if isnull(ls_flag_mezzi) or ls_flag_mezzi="" then ls_flag_mezzi = "N"
if isnull(ls_flag_includi_bloccate) or ls_flag_includi_bloccate="" then ls_flag_mezzi = "N"

ls_sql = "select tm.cod_attrezzatura," + &
						"a.descrizione,"+&
						"tm.cod_tipo_manutenzione,"+&
						"tm.des_tipo_manutenzione,"+&
						"tm.periodicita,tm.frequenza,"+&
						"tm.modalita_esecuzione "+&
			"from  tab_tipi_manutenzione as tm "+&
			"join anag_attrezzature as a on a.cod_azienda=tm.cod_azienda and "+&
														"a.cod_attrezzatura=tm.cod_attrezzatura "
ls_sql += "where tm.cod_azienda='" + s_cs_xx.cod_azienda + "' "


if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" then
	ls_sql += "and a.cod_area_aziendale='" + ls_cod_area_aziendale + "' "
end if

if not isnull(ls_cod_reparto) and ls_cod_reparto <> "" then
	ls_sql += "and a.cod_reparto='" + ls_cod_reparto + "' "
end if

if not isnull(ls_cat_attrezzature) and ls_cat_attrezzature <> "" then
	ls_sql += "and a.cod_cat_attrezzature='" + ls_cat_attrezzature + "' "
end if

if not isnull(ls_da_attrezzatura) and ls_da_attrezzatura <> "" then
	ls_sql += "and tm.cod_attrezzatura>='" + ls_da_attrezzatura + "' "
end if

if not isnull(ls_a_attrezzatura) and ls_a_attrezzatura <> "" then
	ls_sql += "and tm.cod_attrezzatura<='" + ls_a_attrezzatura + "' "
end if

if is_AZP<>"" then
	if not isnull(ls_flag_mezzi) and ls_flag_mezzi = "N" then
		ls_sql += "and a.cod_reparto<>'M' "
	end if
end if

if ls_flag_includi_bloccate="N" then
	ls_sql += "and tm.flag_blocco='N' "
end if

//where aggiuntiva e personalizzata per sintexcal per filtrare alcune tipologie
if is_AZP<>"" then ls_sql += is_where_sintexcal

ls_sql += "order by tm.cod_attrezzatura asc, tm.cod_tipo_manutenzione asc"

pcca.mdi_frame.setmicrohelp("Preparazione dati in corso ...")

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot<0 then
	pcca.mdi_frame.setmicrohelp("Procedura terminata con errori!")
	g_mb.error("Errore: "+ls_errore)
	destroy lds_data
	return
end if

select rag_soc_1
into :ls_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

for ll_index=1 to ll_tot
	
	if mod(ll_index, 10) = 0 then Yield()
	
	//attrezzatura ----------------------------------------------
	ls_cod_attrezzatura_ds			= lds_data.getitemstring(ll_index, 1)
	ls_des_attrezzatura_ds			= lds_data.getitemstring(ll_index, 2)
	
	if isnull(ls_des_attrezzatura_ds) then ls_des_attrezzatura_ds = ""
	if ls_des_attrezzatura_ds <>"" then
		ls_des_attrezzatura_ds = ls_cod_attrezzatura_ds + " " + ls_des_attrezzatura_ds
	else
		ls_des_attrezzatura_ds = ls_cod_attrezzatura_ds
	end if
	
	//tipo manutenzione ---------------------------------------
	ls_cod_tipo_man_ds				= lds_data.getitemstring(ll_index, 3)
	ls_des_tipo_man_ds				= lds_data.getitemstring(ll_index, 4)
	
	pcca.mdi_frame.setmicrohelp("Elaborazione dato "+string(ll_index)+" di "+string(ll_tot)+" : "+ls_cod_attrezzatura_ds+"-"+ls_cod_tipo_man_ds+" in corso ...")
	
	if isnull(ls_des_tipo_man_ds) then ls_des_tipo_man_ds = ""
	if ls_des_tipo_man_ds <>"" then
		ls_des_tipo_man_ds = ls_cod_tipo_man_ds + " " + ls_des_tipo_man_ds
	else
		ls_des_tipo_man_ds = ls_cod_tipo_man_ds
	end if
	
	//periodicita e frequenza -------------------------------------
	ls_periodicita_ds						= lds_data.getitemstring(ll_index, 5)
	
	choose case ls_periodicita_ds
		case "M"
			ls_periodicita_ds= "Minuti"
		case "O"
			ls_periodicita_ds= "Ore"
		case "G"
			ls_periodicita_ds= "Giorni"
		case "S"
			ls_periodicita_ds= "Settimane"
		case "E"
			ls_periodicita_ds= "Mesi"
		case "X"
			ls_periodicita_ds= "Anno Esatto"  //solo sintexcal ce l'ha
		case else
			ls_periodicita_ds= "-"
	end choose
	
	ll_frequenza_ds						= lds_data.getitemnumber(ll_index, 6)
	if isnull(ll_frequenza_ds) then ll_frequenza_ds = 0
	
	ls_periodicita_ds += " - "+string(ll_frequenza_ds)
	
	//istruzione operative --------------------------------------------
	ls_modalita_esecuzione_ds		= lds_data.getitemstring(ll_index, 7)
	
	
	//inserimento riga #########################
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new, "azienda", ls_azienda)
	dw_report.setitem(ll_new, "attrezzatura", ls_des_attrezzatura_ds)
	dw_report.setitem(ll_new, "tipo_manutenzione", ls_des_tipo_man_ds)
	dw_report.setitem(ll_new, "frequenza", ls_periodicita_ds)
	dw_report.setitem(ll_new, "modalita_esecuzione", ls_modalita_esecuzione_ds)
	
	//elaborare se programmata, ultima eseguita e prossima da eseguire
	wf_stato_tipo_manutenzione(ls_cod_attrezzatura_ds, ls_cod_tipo_man_ds, ls_stato_programmazione, ldt_data_ultima_eseguita, ldt_data_prossima_da_eseguire)
	
	dw_report.setitem(ll_new, "stato_programmazione", ls_stato_programmazione)
	
	if not isnull(ldt_data_ultima_eseguita) then
		ls_temp = string(ldt_data_ultima_eseguita, "dd/mm/yyyy")
	else
		ls_temp = "Nessuna"
	end if
	dw_report.setitem(ll_new, "ultima_eseguita", ls_temp)
	
	if not isnull(ldt_data_prossima_da_eseguire) then
		ls_temp = string(ldt_data_prossima_da_eseguire, "dd/mm/yyyy")
	else
		ls_temp = "Nessuna"
	end if
	dw_report.setitem(ll_new, "prossima_da_eseguire", ls_temp)
	
next
pcca.mdi_frame.setmicrohelp("Elaborazione terminata!")

dw_report.sort()
dw_report.groupcalc()

dw_report.Object.DataWindow.Print.Preview = 'Yes'

dw_report.setredraw(true)

dw_folder.fu_selecttab(2)
dw_report.change_dw_current()	

end event

type dw_selezione from uo_cs_xx_dw within w_report_attrezzature_lista_tipiman
integer x = 41
integer y = 140
integer width = 2130
integer height = 840
integer taborder = 20
string dataobject = "d_selezione_report_attrezzature_lista"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_divisione, ls_cod_area_aziendale

dw_selezione.accepttext()
choose case i_colname
	case "rs_cod_divisione"
		ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")
		
		if isnull(ls_cod_divisione) then
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
		else			  
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + i_coltext + "'")
		end if				  
		
	case "rs_cod_area_aziendale"
		ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")		
		if isnull(ls_cod_area_aziendale) then
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
						  "anag_reparti","cod_reparto","des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		else						  
			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
						  "anag_reparti","cod_reparto","des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + i_coltext + "'")
		end if	
end choose





end event

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if

choose case dwo.name
	case "b_attrezzatura_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione, "rs_da_cod_attrezzatura")
		
	case "b_attrezzatura_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione, "rs_a_cod_attrezzatura")
		
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_attrezzature_lista_tipiman
boolean visible = false
integer x = 41
integer y = 140
integer width = 4238
integer height = 2116
integer taborder = 10
string dataobject = "d_report_attrezzature_lista_tipiman"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_attrezzature_lista_tipiman
integer x = 23
integer y = 20
integer width = 4288
integer height = 2376
integer taborder = 60
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;//CHOOSE CASE i_SelectedTabName
//   CASE "Report"
//      SetFocus(dw_report_manut_eseguite)
//		
//   CASE "Selezione"
//      SetFocus(dw_sel_manut_eseguite)
//
//END CHOOSE
//
end event

