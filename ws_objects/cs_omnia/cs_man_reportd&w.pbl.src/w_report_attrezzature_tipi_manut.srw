﻿$PBExportHeader$w_report_attrezzature_tipi_manut.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_attrezzature_tipi_manut from w_cs_xx_principale
end type
type st_1 from statictext within w_report_attrezzature_tipi_manut
end type
type cb_report from commandbutton within w_report_attrezzature_tipi_manut
end type
type dw_selezione from uo_cs_xx_dw within w_report_attrezzature_tipi_manut
end type
type dw_report from uo_cs_xx_dw within w_report_attrezzature_tipi_manut
end type
type dw_folder from u_folder within w_report_attrezzature_tipi_manut
end type
end forward

global type w_report_attrezzature_tipi_manut from w_cs_xx_principale
integer width = 3744
integer height = 2048
string title = "Report Tipologie Manutenzioni"
boolean resizable = false
st_1 st_1
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_attrezzature_tipi_manut w_report_attrezzature_tipi_manut

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report.modify(ls_modify)

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
//l_objects[3] = cb_attrezzature_ricerca_1
//l_objects[4] = cb_attrezzature_ricerca_2
l_objects[3] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

end event

on w_report_attrezzature_tipi_manut.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_attrezzature_tipi_manut.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_selezione,"rs_da_cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'" )
//
//f_PO_LoadDDDW_DW(dw_selezione,"rs_a_cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  					  

end event

type st_1 from statictext within w_report_attrezzature_tipi_manut
integer x = 41
integer y = 668
integer width = 1865
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_report_attrezzature_tipi_manut
integer x = 1938
integer y = 668
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_da_attrezzatura, ls_a_attrezzatura, ls_cat_attrezzature, ls_cod_divisione, ls_cod_area_aziendale_s, ls_cod_reparto_s, &
		 ls_cod_azienda, ls_cod_attrezzatura, ls_descrizione, ls_fabbricante, ls_modello, ls_num_matricola, ls_unita_tempo, &
		 ls_utilizzo_scadenza, ls_cod_inventario, ls_cod_utente, ls_reperibilita, ls_cod_area_aziendale, &
		 ls_cod_reparto, ls_cod_area_aziendale_r[], ls_des_reparto, ls_des_area, ls_rag_soc_1, ls_rag_soc_2, ls_null, &
		 ls_cod_cat_attrezzatura, ls_des_cat_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_periodicita, &
		 ls_modalita_esecuzione
datetime ldt_data_acquisto
decimal ld_periodicita_revisione
long ll_i, ll_riga, ll_cont, ll_frequenza

dw_report.setredraw(false)
dw_selezione.accepttext()

declare cu_tipi_manutenzioni cursor for
	select cod_tipo_manutenzione,
	       des_tipo_manutenzione,
	       periodicita,
			 frequenza,
			 modalita_esecuzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 flag_blocco = 'N';


declare cu_aree cursor for
 select cod_area_aziendale
	from tab_aree_aziendali
  where cod_azienda = :s_cs_xx.cod_azienda
	 and (cod_divisione like :ls_cod_divisione or cod_divisione is null);

declare cu_cursore cursor for
select cod_azienda,
		 cod_attrezzatura,
		 descrizione,
		 fabbricante,
		 modello,
		 num_matricola,
		 data_acquisto,
		 periodicita_revisione,
		 unita_tempo,
		 utilizzo_scadenza,
		 cod_reparto,
		 cod_inventario,
		 cod_utente,
		 reperibilita,
		 cod_area_aziendale,
		 cod_reparto,
		 cod_cat_attrezzature
  from anag_attrezzature
 where cod_azienda = :s_cs_xx.cod_azienda
	and cod_attrezzatura >= :ls_da_attrezzatura
	and cod_attrezzatura <= :ls_a_attrezzatura
	and (cod_cat_attrezzature like :ls_cat_attrezzature or cod_cat_attrezzature is null)
	and (cod_area_aziendale like :ls_cod_area_aziendale_r[ll_i] or cod_area_aziendale is null)
	and (cod_reparto like :ls_cod_reparto_s or cod_reparto is null);	 
	
setnull(ls_null)
dw_report.reset()

ls_da_attrezzatura = dw_selezione.getitemstring(1,"rs_da_cod_attrezzatura")
ls_a_attrezzatura = dw_selezione.getitemstring(1,"rs_a_cod_attrezzatura")
ls_cat_attrezzature = dw_selezione.getitemstring(1,"rs_cod_cat_attrezzatura")
ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")
ls_cod_area_aziendale_s = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
ls_cod_reparto_s = dw_selezione.getitemstring(1,"rs_cod_reparto")

if isnull(ls_da_attrezzatura) then ls_da_attrezzatura = "!"
if isnull(ls_a_attrezzatura) then  ls_a_attrezzatura  = "ZZZZZZ"
if isnull(ls_cat_attrezzature) then  ls_cat_attrezzature  = "%"
if isnull(ls_cod_divisione) then  ls_cod_divisione  = "%"
if isnull(ls_cod_area_aziendale_s) then  ls_cod_area_aziendale_s  = "%"
if isnull(ls_cod_reparto_s) then  ls_cod_reparto_s  = "%"

ll_i = 1
ls_cod_area_aziendale_r[1] = ls_null
st_1.text = "Preparazione dei dati in corso ..."
if ls_cod_area_aziendale_s = "%" and ls_cod_divisione <> "%" then
	
	open cu_aree;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in apertura cursore cu_aree " + sqlca.sqlerrtext)
		return
	end if
	
	do while 1 = 1
		fetch cu_aree into :ls_cod_area_aziendale_r[ll_i];
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati da tabella tab_aree_aziendali " + sqlca.sqlerrtext)
			return
		end if		
		if sqlca.sqlcode = 100 then exit
		
		ll_i ++
	loop
	close cu_aree;
end if	

if ls_cod_area_aziendale_s = "%" and ls_cod_divisione = "%" then ls_cod_area_aziendale_r[1] = "%"
if ls_cod_area_aziendale_s <> "%" and ls_cod_divisione <> "%" then ls_cod_area_aziendale_r[1] = ls_cod_area_aziendale_s
if ls_cod_area_aziendale_s <> "%" and ls_cod_divisione = "%" then ls_cod_area_aziendale_r[1] = ls_cod_area_aziendale_s

for ll_i = 1 to UpperBound(ls_cod_area_aziendale_r)

	open cu_cursore;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in apertura cursore cu_cursore " + sqlca.sqlerrtext)
		return
	end if	
	
	do while 1 = 1
		fetch cu_cursore into :ls_cod_azienda,
									 :ls_cod_attrezzatura,
									 :ls_descrizione,
									 :ls_fabbricante,
									 :ls_modello,
									 :ls_num_matricola,
									 :ldt_data_acquisto,
									 :ld_periodicita_revisione,
									 :ls_unita_tempo,
									 :ls_utilizzo_scadenza,
									 :ls_cod_reparto,
									 :ls_cod_inventario,
									 :ls_cod_utente,
									 :ls_reperibilita,
									 :ls_cod_area_aziendale,
									 :ls_cod_reparto,
									 :ls_cod_cat_attrezzatura;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella anag_attrezzature " + sqlca.sqlerrtext)
			return
		end if									 

		if sqlca.sqlcode = 100 then exit		
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
		dw_report.setitem(ll_riga, "descrizione", ls_descrizione)
		dw_report.setitem(ll_riga, "fabbricante", ls_fabbricante)		
		dw_report.setitem(ll_riga, "modello", ls_modello)
		dw_report.setitem(ll_riga, "num_matricola", ls_num_matricola)		
		dw_report.setitem(ll_riga, "data_acquisto", ldt_data_acquisto)
		dw_report.setitem(ll_riga, "periodicita_revisione", ld_periodicita_revisione)				
		dw_report.setitem(ll_riga, "unita_tempo", ls_unita_tempo)
		dw_report.setitem(ll_riga, "utilizzo_scadenza", ls_utilizzo_scadenza)		
		dw_report.setitem(ll_riga, "cod_reparto", ls_cod_reparto)
		dw_report.setitem(ll_riga, "cod_inventario", ls_cod_inventario)						
		dw_report.setitem(ll_riga, "cod_utente", ls_cod_utente)
		dw_report.setitem(ll_riga, "reperibilita", ls_reperibilita)				
		dw_report.setitem(ll_riga, "cod_area_aziendale", ls_cod_area_aziendale)
		dw_report.setitem(ll_riga, "cod_cat_attrezzature", ls_cod_cat_attrezzatura)
		
		select des_reparto
		  into :ls_des_reparto
		  from anag_reparti
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_reparto = :ls_cod_reparto;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella anag_reparti " + sqlca.sqlerrtext)
			return
		end if									 		
		
		dw_report.setitem(ll_riga, "anag_reparti_des_reparto", ls_des_reparto)	

		select des_area
		  into :ls_des_area
		  from tab_aree_aziendali
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_area_aziendale = :ls_cod_area_aziendale;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella tab_aree_aziendali " + sqlca.sqlerrtext)
			return
		end if									 				
		
		dw_report.setitem(ll_riga, "tab_aree_aziendali_des_area", ls_des_area)
		
		select des_cat_attrezzature
		  into :ls_des_cat_attrezzatura
		  from tab_cat_attrezzature
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_cat_attrezzature = :ls_cod_cat_attrezzatura;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella tab_cat_attrezzature " + sqlca.sqlerrtext)
			return
		end if									 				
		
		dw_report.setitem(ll_riga, "des_cat_attrezzature", ls_des_cat_attrezzatura)
		
		select rag_soc_1,
				 rag_soc_2
		  into :ls_rag_soc_1,
				 :ls_rag_soc_2
		  from aziende
		 where cod_azienda = :s_cs_xx.cod_azienda;
		 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura tabella aziende " + sqlca.sqlerrtext)
			return
		end if									 						 
		
		dw_report.setitem(ll_riga, "aziende_rag_soc_1", ls_rag_soc_1)		
		dw_report.setitem(ll_riga, "aziende_rag_soc_2", ls_rag_soc_2)				

		open cu_tipi_manutenzioni;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in apertura del cursore cu_tipi_manutenzioni " + sqlca.sqlerrtext)
			rollback;
			return
		end if	
		ll_cont = 1
		do while true	
			fetch cu_tipi_manutenzioni into :ls_cod_tipo_manutenzione,:ls_des_tipo_manutenzione, :ls_periodicita, :ll_frequenza, :ls_modalita_esecuzione;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in fecth cursore cu_tipi_manutenzioni " + sqlca.sqlerrtext)
				rollback;
				return
			end if	
			st_1.text = ls_cod_attrezzatura + "  " + ls_descrizione + "/ " + ls_cod_tipo_manutenzione
			if ll_cont > 1 then
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
				dw_report.setitem(ll_riga, "descrizione", ls_descrizione)
				dw_report.setitem(ll_riga, "fabbricante", ls_fabbricante)		
				dw_report.setitem(ll_riga, "modello", ls_modello)
				dw_report.setitem(ll_riga, "num_matricola", ls_num_matricola)		
				dw_report.setitem(ll_riga, "data_acquisto", ldt_data_acquisto)
				dw_report.setitem(ll_riga, "periodicita_revisione", ld_periodicita_revisione)				
				dw_report.setitem(ll_riga, "unita_tempo", ls_unita_tempo)
				dw_report.setitem(ll_riga, "utilizzo_scadenza", ls_utilizzo_scadenza)		
				dw_report.setitem(ll_riga, "cod_reparto", ls_cod_reparto)
				dw_report.setitem(ll_riga, "cod_inventario", ls_cod_inventario)						
				dw_report.setitem(ll_riga, "cod_utente", ls_cod_utente)
				dw_report.setitem(ll_riga, "reperibilita", ls_reperibilita)				
				dw_report.setitem(ll_riga, "cod_area_aziendale", ls_cod_area_aziendale)
				dw_report.setitem(ll_riga, "cod_cat_attrezzature", ls_cod_cat_attrezzatura)
				dw_report.setitem(ll_riga, "anag_reparti_des_reparto", ls_des_reparto)	
				dw_report.setitem(ll_riga, "tab_aree_aziendali_des_area", ls_des_area)
				dw_report.setitem(ll_riga, "des_cat_attrezzature", ls_des_cat_attrezzatura)
				dw_report.setitem(ll_riga, "aziende_rag_soc_1", ls_rag_soc_1)		
				dw_report.setitem(ll_riga, "aziende_rag_soc_2", ls_rag_soc_2)				
			end if
			dw_report.setitem(ll_riga, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
			dw_report.setitem(ll_riga, "des_tipo_manutenzione", ls_des_tipo_manutenzione)
			dw_report.setitem(ll_riga, "periodicita", ls_periodicita)
			dw_report.setitem(ll_riga, "frequenza", ll_frequenza)
			dw_report.setitem(ll_riga, "modalita_esecuzione", ls_modalita_esecuzione + "~r~n-")
				
			ll_cont ++
		loop
		close cu_tipi_manutenzioni;
	loop
	close cu_cursore;
next	

dw_report.setredraw(true)

dw_report.groupcalc()
dw_report.Object.DataWindow.Print.Preview = 'Yes'
dw_folder.fu_selecttab(2)
st_1.text = ""

dw_report.change_dw_current()

rollback;
end event

type dw_selezione from uo_cs_xx_dw within w_report_attrezzature_tipi_manut
integer x = 41
integer y = 132
integer width = 2843
integer height = 540
integer taborder = 20
string dataobject = "d_sel_report_attrezzature_tipi_manut"
boolean border = false
end type

event itemchanged;call super::itemchanged;//string ls_cod_divisione, ls_cod_area_aziendale
//
//dw_selezione.accepttext()
//choose case i_colname
//	case "rs_cod_divisione"
//		ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")		
//		if isnull(ls_cod_divisione) then
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
//						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
//		else			  
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
//						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + i_coltext + "'")					  					  
//		end if				  
//		
//	case "rs_cod_area_aziendale"
//		ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")		
//		if isnull(ls_cod_area_aziendale) then
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
//						  "anag_reparti","cod_reparto","des_reparto", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
//		else						  
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
//						  "anag_reparti","cod_reparto","des_reparto", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + i_coltext + "'")						
//		end if	
//end choose

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"rs_da_cod_attrezzatura")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"rs_a_cod_attrezzatura")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_attrezzature_tipi_manut
integer x = 23
integer y = 128
integer width = 3680
integer height = 1808
integer taborder = 10
string dataobject = "d_report_attrezzature_tipi_manut"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_attrezzature_tipi_manut
integer x = 9
integer y = 12
integer width = 3707
integer height = 1932
integer taborder = 40
boolean border = false
end type

