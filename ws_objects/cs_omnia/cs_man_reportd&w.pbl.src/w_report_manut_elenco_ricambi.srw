﻿$PBExportHeader$w_report_manut_elenco_ricambi.srw
forward
global type w_report_manut_elenco_ricambi from w_cs_xx_principale
end type
type cb_aggiorna from commandbutton within w_report_manut_elenco_ricambi
end type
type cb_cancella_filtri from commandbutton within w_report_manut_elenco_ricambi
end type
type dw_selezione from uo_cs_xx_dw within w_report_manut_elenco_ricambi
end type
type dw_report from uo_cs_xx_dw within w_report_manut_elenco_ricambi
end type
type dw_folder from u_folder within w_report_manut_elenco_ricambi
end type
end forward

global type w_report_manut_elenco_ricambi from w_cs_xx_principale
integer width = 3749
integer height = 2180
string title = "Report elenco Ricambi per Manutenzioni"
cb_aggiorna cb_aggiorna
cb_cancella_filtri cb_cancella_filtri
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_manut_elenco_ricambi w_report_manut_elenco_ricambi

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string ls_operatore, ls_cod_operatore, ls_cod_reparto_filtro, ls_des_attrezzatura, ls_des_tipo_manutenzione,&
       ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cognome, ls_nome, &
		 ls_cod_attrezzatura_inizio, ls_flag_scaduta, ls_cod_tipo_manutenzione_filtro, ls_ordinario, &
		 ls_cod_reparto, ls_cod_area_filtro, ls_cod_area_aziendale, ls_sql, ls_cod_cat_risorse_esterne, ls_des_risorsa, &
		 ls_cod_risorsa_esterna, ls_cod_cat_risorse_esterne_filtro, ls_cod_operaio_filtro, ls_cod_risorsa_esterna_filtro, &
		 ls_cod_attrezzatura_fine, ls_cod_prodotto_filtro, ls_cod_fornitore_filtro, ls_cod_prodotto, &
		 ls_des_prodotto, ls_cod_fornitore, ls_rag_soc_1, ls_des_utente, ls_des_area_aziendale

long ll_riga, ll_anno_registrazione, ll_num_registrazione

dec{4} ld_quan_utilizzo

datetime ldt_data_inizio, ldt_data_fine, ldt_data_registrazione

declare cu_ricambi cursor for
select  cod_prodotto, des_prodotto, quan_utilizzo
from    manutenzioni_ricambi
where   cod_azienda = :s_cs_xx.cod_azienda and
		  anno_registrazione = :ll_anno_registrazione and
		  num_registrazione = :ll_num_registrazione
order by prog_riga_ricambio;
	

dw_selezione.accepttext()

DECLARE manut_eseg DYNAMIC CURSOR FOR SQLSA ;

ls_sql = " " + &
" select anno_registrazione, num_registrazione, cod_attrezzatura " + &
" from   manutenzioni " + &
" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_eseguito = 'N' "

ldt_data_inizio = dw_selezione.getitemdatetime(1,"data_da")

if not isnull(ldt_data_inizio) then
	ls_sql += " and data_registrazione >= '" +string(ldt_data_inizio,s_cs_xx.db_funzioni.formato_data) + "'"
end if

ldt_data_fine = dw_selezione.getitemdatetime(1,"data_a")

if not isnull(ldt_data_fine) then
	ls_sql += " and data_registrazione <= '" +string(ldt_data_fine,s_cs_xx.db_funzioni.formato_data) + "'"
end if

ls_cod_attrezzatura_inizio = dw_selezione.getitemstring(1,"cod_attrezzatura_inizio")

if not isnull(ls_cod_attrezzatura_inizio) then
	ls_sql += " and cod_attrezzatura >= '" + ls_cod_attrezzatura_inizio + "' "
end if

ls_cod_attrezzatura_fine = dw_selezione.getitemstring(1,"cod_attrezzatura_fine")

if not isnull(ls_cod_attrezzatura_fine) then
	ls_sql += " and cod_attrezzatura <= '" + ls_cod_attrezzatura_fine + "' "
end if

ls_ordinario = dw_selezione.getitemstring(1,"flag_ordinario")

if ls_ordinario = "S" or ls_ordinario = "N" then
	ls_sql += " and flag_ordinario = '" + ls_ordinario + "' "
end if

ls_cod_tipo_manutenzione_filtro = dw_selezione.getitemstring(1,"cod_tipo_manutenzione")
if ls_cod_tipo_manutenzione_filtro = "" then setnull(ls_cod_tipo_manutenzione_filtro)
if not isnull(ls_cod_tipo_manutenzione_filtro) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manutenzione_filtro + "' "
end if

ls_cod_operaio_filtro = dw_selezione.getitemstring( 1, "cod_operaio")
if ls_cod_operaio_filtro = "" then setnull(ls_cod_operaio_filtro)
if not isnull(ls_cod_operaio_filtro) then
	ls_sql += " and cod_operaio = '" + ls_cod_operaio_filtro + "' "
end if

ls_cod_cat_risorse_esterne_filtro = dw_selezione.getitemstring( 1, "cod_cat_risorse_esterne")
if ls_cod_cat_risorse_esterne_filtro ="" then setnull(ls_cod_cat_risorse_esterne_filtro)
if not isnull(ls_cod_cat_risorse_esterne_filtro) then
	ls_sql += " and cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne_filtro + "' "
end if

ls_cod_risorsa_esterna_filtro = dw_selezione.getitemstring( 1, "cod_risorsa_esterna")
if ls_cod_risorsa_esterna_filtro = "" then setnull(ls_cod_risorsa_esterna_filtro)
if not isnull(ls_cod_risorsa_esterna_filtro) then
	ls_sql += " and cod_risorsa_esterna = '" + ls_cod_risorsa_esterna_filtro + "' "
end if

ls_cod_reparto_filtro = dw_selezione.getitemstring(1,"cod_reparto")
if ls_cod_reparto_filtro = "" then setnull(ls_cod_reparto_filtro)

ls_cod_area_filtro = dw_selezione.getitemstring(1,"cod_area_aziendale")
if ls_cod_area_filtro = "" then setnull(ls_cod_area_filtro)

ls_cod_prodotto_filtro = dw_selezione.getitemstring(1,"cod_prodotto")
if ls_cod_prodotto_filtro = "" then setnull(ls_cod_prodotto_filtro)

ls_cod_fornitore_filtro = dw_selezione.getitemstring(1,"cod_fornitore")
if ls_cod_fornitore_filtro = "" then setnull(ls_cod_fornitore_filtro)

PREPARE SQLSA FROM :ls_sql ;
OPEN DYNAMIC manut_eseg ;
if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore manut_eseg.~r~n" + sqlca.sqlerrtext)
	return -1
end if

select nome_cognome
into   :ls_des_utente
from   utenti
where  cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode <> 0 then ls_des_utente = ""

if ls_des_utente = "" then
	// avviso e proseguo
	g_mb.messagebox("OMNIA","Attenzione! L'utente corrente non ha nome e cognome impostati.", information!)
end if

dw_report.reset()

do while true
	
	fetch manut_eseg into  :ll_anno_registrazione, :ll_num_registrazione, :ls_cod_attrezzatura;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		rollback;
		return -1
	end if	
	
	if sqlca.sqlcode = 100 then  exit		
	
	select descrizione,
			 cod_reparto,
			 cod_area_aziendale
	into 	 :ls_des_attrezzatura,
			 :ls_cod_reparto,
			 :ls_cod_area_aziendale
	from 	 anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezzatura;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nella select dell'attrezzatura. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		rollback;
		return -1
	elseif sqlca.sqlcode=100 then
		sqlca.sqlcode=0
		continue
	end if
	
	if not isnull(ls_cod_reparto_filtro) then
		if isnull(ls_cod_reparto) or ls_cod_reparto <> ls_cod_reparto_filtro then
			continue
		end if
	end if
	
	if not isnull(ls_cod_area_filtro) then
		if isnull(ls_cod_area_aziendale) or ls_cod_area_aziendale <> ls_cod_area_filtro then
			continue
		end if
	end if
	
	select des_area
	into   :ls_des_area_aziendale
	from   tab_aree_aziendali
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_aziendale = :ls_cod_area_aziendale;
			 
	if sqlca.sqlcode <> 0 then
		ls_des_area_aziendale = ""
	end if
	
	// caricamento dei dati relativi ai ricambi
	
	open cu_ricambi;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_ricambi~r~n"+ sqlca.sqlerrtext)
		close manut_eseg;
		return -1
	end if
	
	do while true
		fetch cu_ricambi into :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_utilizzo;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_ricambi~r~n"+ sqlca.sqlerrtext)
			close manut_eseg;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		if not isnull(ls_cod_prodotto_filtro) then
			if isnull(ls_cod_prodotto) or ls_cod_prodotto <> ls_cod_prodotto_filtro then
				continue
			end if
		end if
		
		select cod_fornitore
		into   :ls_cod_fornitore
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in ricerca prodotto "+ls_cod_prodotto+" in anagrafica ~r~n"+ sqlca.sqlerrtext)
			close manut_eseg;
			return -1
		end if
	
		if not isnull(ls_cod_fornitore_filtro) then
			if isnull(ls_cod_fornitore) or ls_cod_fornitore <> ls_cod_fornitore_filtro then
				continue
			end if
		end if
		
		if not isnull(ls_cod_fornitore) then
			select rag_soc_1
			into   :ls_rag_soc_1
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_fornitore = :ls_cod_fornitore;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in ricerca fornitore "+ls_cod_fornitore+" in anagrafica ~r~n"+ sqlca.sqlerrtext)
				close manut_eseg;
				return -1
			end if
		else
			ls_cod_fornitore = ""
			ls_rag_soc_1     = ""
		end if
		
		ll_riga = dw_report.insertrow(0)
		
		dw_report.setitem(ll_riga, "cod_area_aziendale", ls_cod_area_aziendale)
		
		dw_report.setitem(ll_riga, "des_area_aziendale", ls_des_area_aziendale)
		
		dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
		
		dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
		
		dw_report.setitem(ll_riga, "cod_fornitore", ls_cod_fornitore)
		
		dw_report.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		
		dw_report.setitem(ll_riga, "quan_utilizzo", ld_quan_utilizzo)
	
		dw_report.setitem(ll_riga, "des_utente", ls_des_utente)
	
	loop
	
	close cu_ricambi;
	
loop

close manut_eseg;

dw_report.setsort("cod_fornitore, cod_prodotto")

dw_report.sort()

dw_report.groupcalc()

dw_report.resetupdate()
return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

windowobject lw_oggetti[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

dw_report.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nonew + &
													c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )
													
lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_cancella_filtri
lw_oggetti[3] = cb_aggiorna
//lw_oggetti[6] = cb_fornitori_ricerca
//lw_oggetti[7] = cb_ricerca_prodotto
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)
										
select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report.modify(ls_modify)

dw_report.object.datawindow.print.preview = 'Yes'
end event

on w_report_manut_elenco_ricambi.create
int iCurrent
call super::create
this.cb_aggiorna=create cb_aggiorna
this.cb_cancella_filtri=create cb_cancella_filtri
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_aggiorna
this.Control[iCurrent+2]=this.cb_cancella_filtri
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_manut_elenco_ricambi.destroy
call super::destroy
destroy(this.cb_aggiorna)
destroy(this.cb_cancella_filtri)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione,"cod_reparto",sqlca,"anag_reparti","cod_reparto","des_reparto",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_selezione,"cod_area_aziendale",sqlca,"tab_aree_aziendali","cod_area_aziendale", &
					  "des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_selezione,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio"," nome + ' ' + cognome ",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_PO_LoadDDDW_DW (dw_selezione,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")							  					  
					  
end event

event clicked;call super::clicked;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura_inizio"
end event

type cb_aggiorna from commandbutton within w_report_manut_elenco_ricambi
integer x = 2181
integer y = 1052
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;if isnull(dw_selezione.getitemstring(dw_selezione.getrow(),"cod_area_aziendale")) or len(dw_selezione.getitemstring(dw_selezione.getrow(),"cod_area_aziendale")) < 1 then
	g_mb.messagebox("OMNIA","Attenzione! Per questo report è obbligatorio l'indicazione di un'area.",stopsign!)
	return
end if

dw_report.change_dw_current()
setpointer(hourglass!)
dw_report.setredraw(false)
parent.triggerevent("pc_retrieve")
dw_report.setredraw(true)
dw_folder.fu_selecttab(2)
setpointer(arrow!)
dw_report.change_dw_current()
end event

type cb_cancella_filtri from commandbutton within w_report_manut_elenco_ricambi
integer x = 1783
integer y = 1052
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cancella filtri"
end type

event clicked;string   ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_selezione.setitem(1,"data_da",ldt_null)

dw_selezione.setitem(1,"data_a",ldt_null)

dw_selezione.setitem(1,"cod_reparto",ls_null)

dw_selezione.setitem(1,"cod_attrezzatura_inizio",ls_null)

dw_selezione.setitem(1,"cod_attrezzatura_fine",ls_null)

dw_selezione.setitem(1,"cod_prodotto",ls_null)

dw_selezione.setitem(1,"cod_fornitore",ls_null)

dw_selezione.setitem(1,"cod_area_aziendale",ls_null)

f_po_loaddddw_dw(dw_selezione,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione","cod_tipo_manutenzione",&
					  "des_tipo_manutenzione","cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura is null")
end event

type dw_selezione from uo_cs_xx_dw within w_report_manut_elenco_ricambi
event ue_cod_attr ( )
event ue_carica_tipologie ( )
integer x = 46
integer y = 140
integer width = 3611
integer height = 1148
integer taborder = 10
string dataobject = "d_sel_report_manut_elenco_ricambi"
boolean border = false
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"cod_attrezzatura_inizio")) then
	setitem(getrow(),"cod_attrezzatura_fine",getitemstring(getrow(),"cod_attrezzatura_inizio"))
end if
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"cod_attrezzatura_inizio")

ls_attr_a = getitemstring(getrow(),"cod_attrezzatura_fine")

ls_reparto = getitemstring(getrow(),"cod_reparto")

ls_area = getitemstring(getrow(),"cod_area_aziendale")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
		
	case "cod_attrezzatura_inizio"
		
		postevent("ue_cod_attr")
		
		postevent("ue_carica_tipologie")
		
	case "cod_attrezzatura_fine","cod_reparto","cod_cat_attrezzatura","cod_area_aziendale"
		
		postevent("ue_carica_tipologie")
		
						  
	case "cod_cat_risorse_esterne"
		
		if isnull(i_coltext) or i_coltext = "" then return 0
		
		this.setitem( row, "cod_risorsa_esterna", ls_null)
		
		f_PO_LoadDDDW_DW ( this, &
								 "cod_risorsa_esterna", &
								 sqlca, &
							  	 "anag_risorse_esterne", &
								 "cod_risorsa_esterna", &
								 "descrizione", &
							    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "' ")			
						  
	
end choose
						  
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura_inizio")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura_fine")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_manut_elenco_ricambi
event ue_post_retrieve ( )
integer x = 46
integer y = 140
integer width = 3611
integer height = 1920
integer taborder = 0
string dataobject = "d_report_manut_elenco_ricambi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;long ll_result
ll_result=wf_report()
if ll_result=-1 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella creazione del report")
end if	
end event

type dw_folder from u_folder within w_report_manut_elenco_ricambi
integer x = 23
integer width = 3680
integer height = 2080
integer taborder = 10
end type

