﻿$PBExportHeader$w_report_manut_excel.srw
forward
global type w_report_manut_excel from w_cs_xx_principale
end type
type dw_report_manut_excel_sel from uo_cs_xx_dw within w_report_manut_excel
end type
type wstr_des_periodo from structure within w_report_manut_excel
end type
end forward

type wstr_des_periodo from structure
	string		des_periodo
end type

global type w_report_manut_excel from w_cs_xx_principale
integer width = 1984
integer height = 1252
string title = "Report Manutenzioni Excel"
dw_report_manut_excel_sel dw_report_manut_excel_sel
end type
global w_report_manut_excel w_report_manut_excel

type variables
private wstr_des_periodo istr_des_periodo[]
private boolean ib_guerrato, ib_global_service = false
private string is_des_livello_N, is_des_livello_R, is_des_livello_E, is_des_livello_C, is_des_livello_T, is_des_livello_A, is_des_livello_D

//Donato 06/08/2008 introdotto raggruppamento per Operatore
private string is_des_livello_O
// fine modifica-----------------------------------------------------------------

uo_excel		luo_excel
end variables

forward prototypes
public function integer wf_report_excel (string as_errore)
end prototypes

public function integer wf_report_excel (string as_errore);string 		ls_sql, ls_des_divisione, ls_cod_divisione, ls_cod_cliente, ls_cod_operaio, ls_error, ls_operaio_old, ls_sheetname, ls_test, ls_data_intervento
long			ll_i, ll_ret, ll_riga, ll_colonna, ll_giorno, ll_ora_intera, ll_riga_fine, ll_riga_inizio, ll_minuti_fine, ll_minuti_interi
datetime 	ldt_data_inizio, ldt_data_fine, ldt_data_old, ldt_ora_inizio, ldt_ora_fine_intervento
datastore 	lds_data



ls_sql = "select B.data_inizio data_inizio, B.ora_inizio ora_inizio , B.cod_operaio cod_operaio, O.nome nome , O.cognome cognome , A.cod_attrezzatura cod_attrezzatura, C.cod_reparto cod_reparto, U.cod_divisione cod_divisione, D.cod_cliente cod_cliente, D.des_divisione des_divisione, A.anno_registrazione anno_registrazione, A.num_registrazione num_registrazione, B.data_fine data_fine, B.ora_fine ora_fine   &
from manutenzioni A &
left join manutenzioni_fasi_operai B on A.cod_azienda=B.cod_azienda and A.anno_registrazione=B.anno_registrazione  and A.num_registrazione=B.num_registrazione &
left join anag_attrezzature C on A.cod_azienda=C.cod_azienda and A.cod_attrezzatura = C.cod_attrezzatura &
left join tab_aree_aziendali U on C.cod_azienda = U.cod_azienda and C.cod_area_aziendale=U.cod_area_aziendale &
left join anag_divisioni D on U.cod_azienda = D.cod_azienda and U.cod_divisione=D.cod_divisione &
left join anag_operai O on B.cod_azienda=O.cod_azienda and B.cod_operaio=O.cod_operaio &
where A.flag_eseguito = 'S' "

ls_cod_cliente = dw_report_manut_excel_sel.getitemstring(1,"cod_cliente")
ls_cod_operaio = dw_report_manut_excel_sel.getitemstring(1,"cod_operaio")
ls_cod_divisione = dw_report_manut_excel_sel.getitemstring(1,"cod_commessa")
ldt_data_inizio = dw_report_manut_excel_sel.getitemdatetime(1, "data_inizio")
ldt_data_fine = dw_report_manut_excel_sel.getitemdatetime(1, "data_fine")

uo_service_report_man luo_report_man
luo_report_man = create uo_service_report_man
ll_ret = luo_report_man.uof_report_manut_excel(ls_cod_cliente,ls_cod_operaio,ls_cod_divisione,ldt_data_inizio,ldt_data_fine,as_errore )
destroy luo_report_man
return ll_ret

/*
if not isnull(ls_cod_cliente) then
	ls_sql += " and cod_cliente = '" + ls_cod_cliente + "' "
end if

if not isnull(ls_cod_operaio) then
	ls_sql += " and B.cod_operaio = '" + ls_cod_operaio + "' "
end if

if not isnull(ls_cod_divisione) then
	ls_sql += " and U.cod_divisione = '" + ls_cod_divisione + "' "
end if

if not isnull(ldt_data_inizio) then
	ls_sql += " and data_inizio >= '" + string(ldt_data_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine) then
	ls_sql += " and data_inizio <= '" + string(ldt_data_fine,s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += " order by data_inizio, cod_operaio, cod_divisione "

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_error)
if ll_ret < 0 then
	as_errore = "Errore rin creazione datastore~r~n " + ls_error
	return -1
end if

// preparo il foglio Excel
luo_excel = CREATE uo_excel

luo_excel.uof_create( guo_functions.uof_get_user_documents_folder( ) + guo_functions.uof_get_random_filename( ), true)
ls_sheetname = "Report Attività"
luo_excel.uof_add_sheet(ls_sheetname, true)
luo_excel.uof_set_sheet(ls_sheetname)

luo_excel.uof_set(1, 1, "DATA")
luo_excel.uof_set_bold( 1,1)
luo_excel.uof_set(2, 1, "GIORNO")
luo_excel.uof_set_bold( 2,1)
luo_excel.uof_set(3, 1, "OPERAIO")
luo_excel.uof_set_bold( 3,1)

ll_riga = 4
for ll_i = 0 to 23
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:00",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:15",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:30",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
	ll_riga ++
	luo_excel.uof_set(ll_riga, 1, g_str.format("Ore $1:45",ll_i))
	luo_excel.uof_set_vertical_align(ll_riga, 1, luo_excel.TEXT_ALIGN_MIDDLE  )
next
	
ll_riga = 1
ll_colonna = 1

ldt_data_old = datetime(date("01/01/1900"),00:00:00)
ls_operaio_old = ""

for ll_i = 1 to ll_ret
	if isnull(lds_data.getitemdatetime(ll_i, 1)) then continue
	
	if ldt_data_old <> lds_data.getitemdatetime(ll_i, 1) or ls_operaio_old <> lds_data.getitemstring(ll_i, 3) then 
		ll_colonna ++
		if mod(ll_colonna,26)=0 then ll_colonna ++
		ldt_data_old = lds_data.getitemdatetime(ll_i, 1)
		ls_operaio_old = lds_data.getitemstring(ll_i, 3)
		ls_data_intervento =  string(lds_data.getitemdatetime(ll_i, 1),"dd/mm/yy")
		luo_excel.uof_set(1, ll_colonna, ls_data_intervento)
		luo_excel.uof_set_autofit( 1, ll_colonna)
		ll_giorno = guo_functions.uof_get_day_number_ita( date(lds_data.getitemdatetime(ll_i, 1)) )
		luo_excel.uof_set(2, ll_colonna, guo_functions.uof_get_day_name_ita( ll_giorno ) )
		luo_excel.uof_set(3, ll_colonna, g_str.format("$1 $2", lds_data.getitemstring(ll_i, 5),lds_data.getitemstring(ll_i, 4) ) )
	end if		
	
	ldt_ora_inizio = lds_data.getitemdatetime(ll_i, 2)
	if isnull(ldt_ora_inizio) then continue
	
	ls_test = string( time( ldt_ora_inizio))
	ll_ora_intera = long( left(ls_test,2))
	ll_minuti_interi = long( mid(ls_test,4,2))
	ll_riga_inizio = 4 + (ll_ora_intera * 4) + 1
	choose case ll_minuti_interi
		case  15 to 29
			ll_riga_inizio ++
		case  30 to 44
			ll_riga_inizio ++
		case  45 to 59
			ll_riga_inizio ++
	end choose		
	
	// leggo data e ora fine
	ldt_ora_fine_intervento = lds_data.getitemdatetime(ll_i, 14)
	if isnull(ldt_ora_fine_intervento) then ldt_ora_fine_intervento = ldt_ora_inizio

	ls_test = string( time( ldt_ora_fine_intervento     )  )
	ll_ora_intera = long( left(ls_test,2) )   // prendo le ore
	ll_minuti_fine = long( mid(ls_test,4,2)) // prendo i minuti
	ll_riga_fine = 4 + (ll_ora_intera * 4) + 1
	choose case ll_minuti_fine
		case 15 to 29
			ll_riga_fine ++
		case 30 to 44
			ll_riga_fine ++
		case 45 to 59
			ll_riga_fine ++
		case else
			// se i minuti sono zero allora detraggo un'ora in modo che, ad esempio, se l'attività termina alle 12.00 non vado ad impegnare la cella che va dalle 12 alle 13
			ll_riga_fine --
	end choose		
	if ll_riga_fine < ll_riga_inizio then ll_riga_fine = ll_riga_inizio
	
	ls_des_divisione = g_str.format(lds_data.getitemstring(ll_i, 10) + " ($1/$2)", lds_data.getitemnumber(ll_i, 11), lds_data.getitemnumber(ll_i, 12) )
	if isnull(ls_des_divisione) or len(ls_des_divisione) < 1 then ls_des_divisione = g_str.format("Commessa non indicata ($1/$2)",lds_data.getitemnumber(ll_i, 11),lds_data.getitemnumber(ll_i, 12) )
	
	for ll_riga = ll_riga_inizio to ll_riga_fine
		luo_excel.uof_set(ll_riga, ll_colonna, ls_des_divisione )
		luo_excel.uof_set_wrap_text(ll_riga, ll_colonna)
		luo_excel.uof_set_horizontal_align( ll_riga, ll_colonna, luo_excel.TEXT_ALIGN_MIDDLE)
	next	
next


destroy luo_excel

return 0
*/
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify, ls_test

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_report_manut_excel_sel.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
								c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

dw_report_manut_excel_sel.object.t_path.text = guo_functions.uof_get_user_documents_folder( )

end event

on w_report_manut_excel.create
int iCurrent
call super::create
this.dw_report_manut_excel_sel=create dw_report_manut_excel_sel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_manut_excel_sel
end on

on w_report_manut_excel.destroy
call super::destroy
destroy(this.dw_report_manut_excel_sel)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_report_manut_excel_sel,"cod_commessa",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  


end event

type dw_report_manut_excel_sel from uo_cs_xx_dw within w_report_manut_excel
event ue_cod_attr ( )
event ue_carica_tipologie ( )
event ue_aree_aziendali ( )
integer width = 1943
integer height = 1140
integer taborder = 10
string dataobject = "d_report_manut_excel_sel"
boolean border = false
end type

event itemchanged;call super::itemchanged;////choose case i_colname
//	case "attrezzatura_da"
//		postevent("ue_cod_attr")
//		postevent("ue_carica_tipologie")
//		
//	case "attrezzatura_a","cod_reparto","cod_categoria","cod_area"
//		postevent("ue_carica_tipologie")
//		
//	case "cod_divisione"
//		postevent("ue_aree_aziendali")
//		
//	case "flag_tipo_livello_1"
//		if i_coltext = "N" then
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_2", 0)
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 0)
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_2", "N")
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_3", "N")
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
//		else			
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_2", 220)
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 2, is_des_livello_D + "~tD")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 3, is_des_livello_R + "~tR")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 4, is_des_livello_E + "~tE")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 5, is_des_livello_C + "~tC")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 6, is_des_livello_T + "~tT")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 7, is_des_livello_A + "~tA")
//			//Donato 06/08/2008 introdotto raggruppamento per operatore
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 8, is_des_livello_O + "~tO")
//			// fine modifica ----------------------------------------------------------------
//		end if
//		
//	case "flag_tipo_livello_2"
//		if i_coltext = "N" then
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 0)
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_3", "N")
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
//		else			
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 221)
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 2, is_des_livello_D + "~tD")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 3, is_des_livello_R + "~tR")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 4, is_des_livello_E + "~tE")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 5, is_des_livello_C + "~tC")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 6, is_des_livello_T + "~tT")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 7, is_des_livello_A + "~tA")
//			//Donato 06/08/2008 introdotto raggruppamento per operatore
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 8, is_des_livello_O + "~tO")
//			// fine modifica ----------------------------------------------------------------
//		end if
//		
//	case "flag_tipo_livello_3"
//		if i_coltext = "N" then
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
//		else			
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 222)
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 2, is_des_livello_D + "~tD")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 3, is_des_livello_R + "~tR")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 4, is_des_livello_E + "~tE")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 5, is_des_livello_C + "~tC")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 6, is_des_livello_T + "~tT")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 7, is_des_livello_A + "~tA")
//			//Donato 06/08/2008 introdotto raggruppamento per operatore
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 8, is_des_livello_O + "~tO")
//			// fine modifica ----------------------------------------------------------------
//		end if
//		
//	case "flag_tipo_livello_4"
//		if i_coltext = "N" then
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
//			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
//		else			
//			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 223)
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 2, is_des_livello_D + "~tD")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 3, is_des_livello_R + "~tR")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 4, is_des_livello_E + "~tE")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 5, is_des_livello_C + "~tC")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 6, is_des_livello_T + "~tT")
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 7, is_des_livello_A + "~tA")
//			//Donato 06/08/2008 introdotto raggruppamento per operatore
//			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 8, is_des_livello_O + "~tO")
//			// fine modifica ----------------------------------------------------------------
//		end if		
//end choose
end event

event buttonclicked;call super::buttonclicked;string		ls_errore
choose case dwo.name
	case "b_ric_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_report_manut_excel_sel,"cod_cliente")
	case "b_ric_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_report_manut_excel_sel,"cod_operaio")
	case "b_report"
		dw_report_manut_excel_sel.accepttext()

		wf_report_excel(ls_errore)
		if len(ls_errore) > 0 then
			g_mb.error(ls_errore)
			rollback;
			return
		end if
	case "b_open"
		run(guo_functions.uof_get_user_documents_folder( ))
end choose
end event

