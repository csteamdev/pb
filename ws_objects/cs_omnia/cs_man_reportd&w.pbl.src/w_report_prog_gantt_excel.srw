﻿$PBExportHeader$w_report_prog_gantt_excel.srw
forward
global type w_report_prog_gantt_excel from w_cs_xx_principale
end type
type dw_sel_tipi_richieste from datawindow within w_report_prog_gantt_excel
end type
type cb_annulla from commandbutton within w_report_prog_gantt_excel
end type
type cb_report from commandbutton within w_report_prog_gantt_excel
end type
type st_1 from statictext within w_report_prog_gantt_excel
end type
type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_prog_gantt_excel
end type
type wstr_des_periodo from structure within w_report_prog_gantt_excel
end type
end forward

type wstr_des_periodo from structure
	string		des_periodo
end type

global type w_report_prog_gantt_excel from w_cs_xx_principale
integer width = 2469
integer height = 1856
string title = "Report Programmazione Periodo Excel"
dw_sel_tipi_richieste dw_sel_tipi_richieste
cb_annulla cb_annulla
cb_report cb_report
st_1 st_1
dw_sel_manut_eseguite dw_sel_manut_eseguite
end type
global w_report_prog_gantt_excel w_report_prog_gantt_excel

type variables
private wstr_des_periodo istr_des_periodo[]
private boolean ib_guerrato, ib_global_service = false
private string is_des_livello_N, is_des_livello_R, is_des_livello_E, is_des_livello_C, is_des_livello_T, is_des_livello_A, is_des_livello_D

//Donato 06/08/2008 introdotto raggruppamento per Operatore
private string is_des_livello_O
// fine modifica-----------------------------------------------------------------

end variables

forward prototypes
public subroutine wf_pulisci_stringa (ref string fs_stringa)
public subroutine wf_calcola_periodo (datetime fdt_data_registrazione, ref long fl_num_periodo, ref string fs_label_periodo)
public function integer wf_num_settimana (ref datetime fdt_data)
public function integer wf_carica_tabella_appoggio (ref long fl_prog_task)
public subroutine wf_generazione_foglio_excel (long fl_prog_task)
public subroutine wf_calcola_durata (datetime fdt_data_inizio, datetime fdt_data_fine, string fs_tipo_periodo, ref long fl_num_periodo, ref string fs_label_periodo)
public subroutine wf_calcola_periodo_2 (datetime fdt_data_registrazione, datetime fdt_data_inizio, ref long fl_num_periodo, ref string fs_label_periodo)
public function integer wf_festivo (string fs_data, boolean fb_includi_sabato, string fs_flag_tipo_periodo)
end prototypes

public subroutine wf_pulisci_stringa (ref string fs_stringa);long ll_pos

if len(fs_stringa) < 1 then return
if isnull(fs_stringa) then return

do while true
	ll_pos = pos(fs_stringa, "~r")
	if ll_pos > 0 then
		fs_stringa = replace(fs_stringa, ll_pos, 1, "")
	else
		exit
	end if
loop

do while true
	ll_pos = pos(fs_stringa, "~n")
	if ll_pos > 0 then
		fs_stringa = replace(fs_stringa, ll_pos, 1, "")
	else
		exit
	end if
loop

return
end subroutine

public subroutine wf_calcola_periodo (datetime fdt_data_registrazione, ref long fl_num_periodo, ref string fs_label_periodo);string ls_tipo_periodo, ls_data
long ll_giorno, ll_mese, ll_anno, ll_test 
date ld_data_rif
datetime ldt_data_inizio, ldt_data_fine


ldt_data_inizio = dw_sel_manut_eseguite.getitemdatetime(1,"data_da")
ldt_data_fine   = dw_sel_manut_eseguite.getitemdatetime(1,"data_a")
ls_tipo_periodo   = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_periodo")

choose case ls_tipo_periodo
		
	case "G"
		
		fl_num_periodo = DaysAfter( date(ldt_data_inizio), date(fdt_data_registrazione) ) + 1
		
		fs_label_periodo = string(fdt_data_registrazione, "dd/mm/yyyy")
		
	case "S" 		// la settimana inizia la domenica e finisce al sabato.
		
		ll_giorno = daynumber( date(ldt_data_inizio) )
		ld_data_rif = relativedate(date(ldt_data_inizio), 7 - ll_giorno) // trovo la data del primo sabato
		
		fl_num_periodo = 1
		
		do while ld_data_rif < date(fdt_data_registrazione)
			
			fl_num_periodo ++
			
			// trovo la data del prossimo fine settimana
			ld_data_rif = relativedate(ld_data_rif, 7 )		
			
		loop
		
		fs_label_periodo = "'Sett. " + string(wf_num_settimana(fdt_data_registrazione)) + "/" + string(year(date(fdt_data_registrazione)),"0000")
		
	case "M"
		ll_test = 0
		ll_mese = month( date(ldt_data_inizio) ) - 1 
		ll_anno = year( date(ldt_data_inizio) )
		
		fl_num_periodo = 0
		
		do 
			fl_num_periodo ++
			ll_test ++
			// calcolo il prossimo mese
			
			ll_mese ++
			if ll_mese > 12 then
				ll_mese = 1
				ll_anno ++
			end if
			
			ls_data = "01/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
			
			ld_data_rif = date(ls_data)
			
		//loop until ( ld_data_rif >= date(fdt_data_registrazione) and ll_test = 1 ) or ( ld_data_rif > date(fdt_data_registrazione) and ll_test > 1 ) or ( ld_data_rif = date(fdt_data_registrazione))
		loop until ( ld_data_rif > date(fdt_data_registrazione)) or (ld_data_rif > date( ldt_data_fine))
		
//		if ( ld_data_rif > date( ldt_data_fine)) then
			fl_num_periodo --
			ll_mese --
//		end if
		
		fs_label_periodo = "Mese " + string(ll_mese) + "/" + string(ll_anno,"0000")
		
		
end choose
end subroutine

public function integer wf_num_settimana (ref datetime fdt_data);date ld_data,ld_prima_domenica,ld_primo_giorno,ld_prossima_domenica
long ll_settimana

ld_data = date(fdt_data)
ld_primo_giorno = date("01/01/" + string( year(ld_data) ) )

// cerco la data della prima domenica dell'anno
choose case daynumber(ld_primo_giorno)
	case 1
		ld_prima_domenica = ld_primo_giorno
	case 2
		ld_prima_domenica = relativedate(ld_primo_giorno,6)
	case 3
		ld_prima_domenica = relativedate(ld_primo_giorno,5)
	case 4
		ld_prima_domenica = relativedate(ld_primo_giorno,4)
	case 5
		ld_prima_domenica = relativedate(ld_primo_giorno,3)
	case 6
		ld_prima_domenica = relativedate(ld_primo_giorno,2)
	case 7
		ld_prima_domenica = relativedate(ld_primo_giorno,1)
end choose

if ld_prima_domenica > ld_data then
	ll_settimana = 52
	fdt_data = datetime(relativedate(date(fdt_data), -365),00:00:00)
else	
	// cerco la data della prossima domenica rispetto alla data odierna
	choose case daynumber(ld_data)
		case 1
			ld_prossima_domenica = relativedate(ld_data ,7)
		case 2
			ld_prossima_domenica = relativedate(ld_data ,6)
		case 3
			ld_prossima_domenica = relativedate(ld_data ,5)
		case 4
			ld_prossima_domenica = relativedate(ld_data ,4)
		case 5
			ld_prossima_domenica = relativedate(ld_data ,3)
		case 6
			ld_prossima_domenica = relativedate(ld_data ,2)
		case 7
			ld_prossima_domenica = relativedate(ld_data ,1)
	end choose
	
	ll_settimana = daysafter(ld_prima_domenica, ld_prossima_domenica) / 7
	
end if

return ll_settimana
end function

public function integer wf_carica_tabella_appoggio (ref long fl_prog_task);long 		ll_riga, ll_tempo_totale, ll_anno_registrazione, ll_num_registrazione, ll_num_periodo, &
         ll_prog_record,ll_num_eseguiti, ll_num_iniziati, ll_num_aperti, ll_nrows, ll_range, ll_y, ll_operaio_ore, ll_operaio_minuti, ll_risorsa_ore, &
			ll_risorsa_minuti, ll_durata_fase

datetime ldt_da_data, ldt_a_data, ldt_data_registrazione, ldt_data_fine_att_inizio, ldt_data_fine_att_fine, &
         ldt_data_fine_attivita, ldt_data_inizio_intervento, ldt_data_progressiva, ldt_data_inizio_fase, ldt_data_fine_fase

string 	ls_selezione, ls_operatori, ls_cod_guasto, ls_attr_da, ls_attr_a, ls_tipo_manut, ls_categoria, &
		 	ls_cod_reparto, ls_area, ls_ordinaria, ls_ric_car, ls_ric_tel, ls_giro_isp, ls_altre, ls_sql, &
		 	ls_flag, ls_flag_eseguito, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cognome, ls_nome, &
		 	ls_des_attrezzatura, ls_cod_reparto_attrezzatura, ls_des_tipo_manutenzione, ls_cat_attrezzature, &
			ls_cod_operatore, ls_cod_risorsa_esterna,ls_cod_area_aziendale, ls_flag_eseguito_sel, ls_flag_ordinario,&
			ls_flag_straordinario, ls_label_periodo,ls_cod_cat_attrezzatura, ls_test, ls_divisione, ls_cod_divisione, ls_cod_richieste, ls_cod_tipo_richiesta, &
			ls_cod_tipo_manutenzione_appo, ls_flag_ore
			
//Donato 06/08/2008 introdotto per il raggruppamento per Operatore
string ls_cod_operaio_raggupp, ls_cod_operaio_raggupp_tmp
string ls_sql_operatori, ls_sql_risorse, ls_sql_generale, ls_appo
string ls_where_operatori, ls_where_risorse, ls_where_generale
// fine modifica ------------------------------------------------------------------------
			
wstr_des_periodo lstr_des_periodo[]


dw_sel_manut_eseguite.accepttext()

ls_selezione = ""

istr_des_periodo[] = lstr_des_periodo[]

// selezione da data a data registrazione
ldt_da_data = dw_sel_manut_eseguite.getitemdatetime(1,"data_da")
ldt_a_data = dw_sel_manut_eseguite.getitemdatetime(1,"data_a")

if not isnull(ldt_da_data) or not isnull(ldt_a_data) then
	ls_selezione += "Data registrazione:"
end if

if not isnull(ldt_da_data) then
	ls_selezione += " dal " + string(ldt_da_data,"dd/mm/yyyy") + " "
end if


if not isnull(ldt_a_data) then
	ls_selezione += " al " + string(ldt_a_data,"dd/mm/yyyy")
end if

if ldt_da_data > ldt_a_data then
	g_mb.messagebox("OMNIA","Date di selezione registrazione non coerenti!",exclamation!)
	return -1
end if

// selezione da data a data fine attività
ldt_data_fine_att_inizio = dw_sel_manut_eseguite.getitemdatetime(1,"data_fine_att_inizio")
ldt_data_fine_att_fine = dw_sel_manut_eseguite.getitemdatetime(1,"data_fine_att_fine")

if not isnull(ldt_data_fine_att_inizio) or not isnull(ldt_data_fine_att_fine) then
	if len(ls_selezione) > 0 then ls_selezione += "~r~n"
	ls_selezione += "Data fine attività:"
end if

if not isnull(ldt_data_fine_att_inizio) then
	ls_selezione += " dal " + string(ldt_data_fine_att_inizio,"dd/mm/yyyy") + " "
end if


if not isnull(ldt_data_fine_att_fine) then
	ls_selezione += " al " + string(ldt_data_fine_att_fine,"dd/mm/yyyy")
end if

if ldt_data_fine_att_inizio > ldt_data_fine_att_fine then
	g_mb.messagebox("OMNIA","Date di selezione fine attività non coerenti!",exclamation!)
	return -1
end if

ls_cod_operatore = dw_sel_manut_eseguite.getitemstring(1,"cod_operaio")

ls_cod_risorsa_esterna = dw_sel_manut_eseguite.getitemstring(1,"cod_risorsa_esterna")

ls_cod_guasto = dw_sel_manut_eseguite.getitemstring(1,"cod_guasto")

ls_attr_da = dw_sel_manut_eseguite.getitemstring(1,"attrezzatura_da")

ls_attr_a = dw_sel_manut_eseguite.getitemstring(1,"attrezzatura_a")

ls_tipo_manut = dw_sel_manut_eseguite.getitemstring(1,"cod_tipo_manutenzione")

ls_categoria = dw_sel_manut_eseguite.getitemstring(1,"cod_categoria")

ls_cod_reparto = dw_sel_manut_eseguite.getitemstring(1,"cod_reparto")

ls_area = dw_sel_manut_eseguite.getitemstring(1,"cod_area")

ls_divisione = dw_sel_manut_eseguite.getitemstring(1,"cod_divisione")

ls_ordinaria = dw_sel_manut_eseguite.getitemstring(1,"flag_ordinaria")

if ib_guerrato then
	
	ls_flag_ore = dw_sel_manut_eseguite.getitemstring( 1, "flag_ore")
	
	dw_sel_tipi_richieste.accepttext()
	ls_cod_richieste = ""
	for ll_y = 1 to dw_sel_tipi_richieste.rowcount()
		
		if dw_sel_tipi_richieste.getitemstring( ll_y, "flag_selezione") = "S" then
			
			ls_cod_tipo_richiesta = dw_sel_tipi_richieste.getitemstring( ll_y, "cod_tipo_richiesta")
			
			select cod_tipo_manutenzione
			into   :ls_cod_tipo_manutenzione_appo
			from   tab_tipi_richieste
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_richiesta = :ls_cod_tipo_richiesta;
					 
			if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_manutenzione_appo) and ls_cod_tipo_manutenzione_appo <> "" then			
				if ls_cod_richieste = "" then
					ls_cod_richieste = "'" + ls_cod_tipo_manutenzione_appo + "'"
				else
					ls_cod_richieste = ls_cod_richieste + ", '" + ls_cod_tipo_manutenzione_appo + "'"
				end if
			end if
		end if
		
	next
	
end if

ls_giro_isp = dw_sel_manut_eseguite.getitemstring(1,"flag_giro_isp")
ls_altre = dw_sel_manut_eseguite.getitemstring(1,"flag_altre")
ls_flag_eseguito_sel = dw_sel_manut_eseguite.getitemstring(1,"flag_eseguito")
ls_flag_ordinario = dw_sel_manut_eseguite.getitemstring(1,"flag_ordinario")
ls_flag_straordinario = dw_sel_manut_eseguite.getitemstring(1,"flag_straordinario")

declare manut_eseg dynamic cursor for sqlsa;

ls_sql_operatori = &
"select manutenzioni.anno_registrazione, " + &
		" manutenzioni.num_registrazione, " + &
		" manutenzioni.data_registrazione, " + &
		" manutenzioni.flag_eseguito, " + &
		" manutenzioni.data_inizio_intervento, " + &
		" manutenzioni.cod_attrezzatura, " + &
		" manutenzioni.cod_tipo_manutenzione, " + &
		" anag_attrezzature.descrizione, " + &
		" anag_attrezzature.cod_reparto, " + &
		" anag_attrezzature.cod_area_aziendale, " + &
		" anag_attrezzature.cod_cat_attrezzature, " + &
		" tab_tipi_manutenzione.des_tipo_manutenzione, " + &
		" manutenzioni.operaio_ore, " + &
		" manutenzioni.operaio_minuti, " + &
		" manutenzioni.risorsa_ore, " + &
		" manutenzioni.risorsa_minuti," + &	
		" '*O*' + manutenzioni_fasi_operai.cod_operaio cod_operaio," + &
		" manutenzioni_fasi_operai.data_inizio as data_inizio," + &
		" manutenzioni_fasi_operai.data_fine as data_fine " + &
"from   manutenzioni " + &
"join anag_attrezzature on anag_attrezzature.cod_azienda = manutenzioni.cod_azienda and " + &
					" anag_attrezzature.cod_attrezzatura = manutenzioni.cod_attrezzatura  " + &
"join tab_tipi_manutenzione on tab_tipi_manutenzione.cod_azienda = manutenzioni.cod_azienda and " + &
					" tab_tipi_manutenzione.cod_attrezzatura = manutenzioni.cod_attrezzatura and " + &
					" tab_tipi_manutenzione.cod_tipo_manutenzione = manutenzioni.cod_tipo_manutenzione " + &
"join manutenzioni_fasi_operai on manutenzioni_fasi_operai.cod_azienda = manutenzioni.cod_azienda and " + &
				" manutenzioni_fasi_operai.anno_registrazione = manutenzioni.anno_registrazione and " + &
				" manutenzioni_fasi_operai.num_registrazione = manutenzioni.num_registrazione "
				
ls_sql_risorse = &
"select manutenzioni.anno_registrazione, " + &
		" manutenzioni.num_registrazione, " + &
		" manutenzioni.data_registrazione, " + &
		" manutenzioni.flag_eseguito, " + &
		" manutenzioni.data_inizio_intervento, " + &
		" manutenzioni.cod_attrezzatura, " + &
		" manutenzioni.cod_tipo_manutenzione, " + &
		" anag_attrezzature.descrizione, " + &
		" anag_attrezzature.cod_reparto, " + &
		" anag_attrezzature.cod_area_aziendale, " + &
		" anag_attrezzature.cod_cat_attrezzature, " + &
		" tab_tipi_manutenzione.des_tipo_manutenzione, " + &
		" manutenzioni.operaio_ore, " + &
		" manutenzioni.operaio_minuti, " + &
		" manutenzioni.risorsa_ore, " + &
		" manutenzioni.risorsa_minuti," + &	
		" '*R*' + manutenzioni_fasi_risorse.cod_cat_risorse_esterne + '*R*' + manutenzioni_fasi_risorse.cod_risorsa_esterna as cod_operaio," + &
		" manutenzioni_fasi_risorse.data_inizio as data_inizio," + &
		" manutenzioni_fasi_risorse.data_fine as data_fine " + &
"from   manutenzioni " + &
"join anag_attrezzature on anag_attrezzature.cod_azienda = manutenzioni.cod_azienda and " + &
					" anag_attrezzature.cod_attrezzatura = manutenzioni.cod_attrezzatura  " + &
"join tab_tipi_manutenzione on tab_tipi_manutenzione.cod_azienda = manutenzioni.cod_azienda and " + &
					" tab_tipi_manutenzione.cod_attrezzatura = manutenzioni.cod_attrezzatura and " + &
					" tab_tipi_manutenzione.cod_tipo_manutenzione = manutenzioni.cod_tipo_manutenzione " + &
"join manutenzioni_fasi_risorse on manutenzioni_fasi_risorse.cod_azienda = manutenzioni.cod_azienda and " + &
				" manutenzioni_fasi_risorse.anno_registrazione = manutenzioni.anno_registrazione and " + &
				" manutenzioni_fasi_risorse.num_registrazione = manutenzioni.num_registrazione "

ls_sql_generale = &
"select " + &
		"manutenzioni.anno_registrazione, " + &
		"manutenzioni.num_registrazione, " + &
		"manutenzioni.data_registrazione, " + &
		"manutenzioni.flag_eseguito, " + &
		"manutenzioni.data_inizio_intervento, " + &
		"manutenzioni.cod_attrezzatura, " + &
		"manutenzioni.cod_tipo_manutenzione, " + &
		"anag_attrezzature.descrizione, " + &
		"anag_attrezzature.cod_reparto, " + &
		"anag_attrezzature.cod_area_aziendale, " + &
		"anag_attrezzature.cod_cat_attrezzature, " + &
		"tab_tipi_manutenzione.des_tipo_manutenzione, " + &
		"manutenzioni.operaio_ore," + &
		"manutenzioni.operaio_minuti," + &
		"manutenzioni.risorsa_ore," + &
		"manutenzioni.risorsa_minuti," + &
		"manutenzioni_fasi_operai.cod_operaio as cod_operaio," + &
		"manutenzioni.data_registrazione as data_inizio," + &
		"manutenzioni.data_registrazione as data_fine " + &
"from   manutenzioni " + &
"join anag_attrezzature on anag_attrezzature.cod_azienda = manutenzioni.cod_azienda " + &
				"and  anag_attrezzature.cod_attrezzatura = manutenzioni.cod_attrezzatura  " + &
"join tab_tipi_manutenzione on tab_tipi_manutenzione.cod_azienda = manutenzioni.cod_azienda " + &
				"and  tab_tipi_manutenzione.cod_attrezzatura = manutenzioni.cod_attrezzatura " + &
"and  tab_tipi_manutenzione.cod_tipo_manutenzione = manutenzioni.cod_tipo_manutenzione " + &
"left outer join manutenzioni_fasi on manutenzioni_fasi.cod_azienda = manutenzioni.cod_azienda " + &
				"and  manutenzioni_fasi.anno_registrazione = manutenzioni.anno_registrazione " + &
				"and  manutenzioni_fasi.num_registrazione = manutenzioni.num_registrazione " + &
"left outer join manutenzioni_fasi_operai on manutenzioni_fasi_operai.cod_azienda = manutenzioni.cod_azienda " + &
				"and  manutenzioni_fasi_operai.anno_registrazione = manutenzioni.anno_registrazione " + &
				"and  manutenzioni_fasi_operai.num_registrazione = manutenzioni.num_registrazione " + &
"left outer join manutenzioni_fasi_risorse on manutenzioni_fasi_risorse.cod_azienda = manutenzioni.cod_azienda " + &
				"and  manutenzioni_fasi_risorse.anno_registrazione = manutenzioni.anno_registrazione " + &
				"and  manutenzioni_fasi_risorse.num_registrazione = manutenzioni.num_registrazione "

// fine modifica -------------------------------------------------------------------

	
//ls_sql += "where manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
ls_sql_operatori += "where manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
ls_sql_risorse += "where manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

ls_sql_generale += "where manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
ls_sql_generale += "and manutenzioni_fasi_operai.cod_operaio is null " + &
							"and manutenzioni_fasi_risorse.cod_risorsa_esterna is null " + &
							"and (manutenzioni_fasi.anno_registrazione is null or manutenzioni_fasi.anno_registrazione = 0) "

if not isnull(ls_flag_eseguito_sel) and ls_flag_eseguito_sel <> "T" then
	//ls_sql = ls_sql + " and manutenzioni.flag_eseguito = '" + ls_flag_eseguito_sel + "' "
	ls_sql_operatori += " and manutenzioni.flag_eseguito = '" + ls_flag_eseguito_sel + "' "
	ls_sql_risorse += " and manutenzioni.flag_eseguito = '" + ls_flag_eseguito_sel + "' "
	ls_sql_generale += " and manutenzioni.flag_eseguito = '" + ls_flag_eseguito_sel + "' "
end if

if not isnull(ls_flag_ordinario) and ls_flag_ordinario <> "T" then
	//ls_sql = ls_sql + " and manutenzioni.flag_ordinario = '" + ls_flag_ordinario + "' "
	ls_sql_operatori += " and manutenzioni.flag_ordinario = '" + ls_flag_ordinario + "' "
	ls_sql_risorse += " and manutenzioni.flag_ordinario = '" + ls_flag_ordinario + "' "
	ls_sql_generale += " and manutenzioni.flag_ordinario = '" + ls_flag_ordinario + "' "
end if

if not isnull(ls_flag_straordinario) and ls_flag_straordinario <> "T" then
	//ls_sql = ls_sql + " and manutenzioni.flag_straordinario = '" + ls_flag_straordinario + "' "
	ls_sql_operatori += " and manutenzioni.flag_straordinario = '" + ls_flag_straordinario + "' "
	ls_sql_risorse += " and manutenzioni.flag_straordinario = '" + ls_flag_straordinario + "' "
	ls_sql_generale += " and manutenzioni.flag_straordinario = '" + ls_flag_straordinario + "' "
end if

if not isnull(ls_cod_guasto)	then
	//ls_sql = ls_sql + " and manutenzioni.cod_guasto = '" + ls_cod_guasto + "' "
	ls_sql_operatori += " and manutenzioni.cod_guasto = '" + ls_cod_guasto + "' "
	ls_sql_risorse += " and manutenzioni.cod_guasto = '" + ls_cod_guasto + "' "
	ls_sql_generale += " and manutenzioni.cod_guasto = '" + ls_cod_guasto + "' "
end if

if not isnull(ldt_da_data) then
	//ls_sql += " and manutenzioni.data_registrazione >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operatori += " and manutenzioni.data_registrazione >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " and manutenzioni.data_registrazione >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_generale += " and manutenzioni.data_registrazione >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
	
	//ls_sql_operatori += " and manutenzioni_fasi_operai.data_inizio >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
	//ls_sql_risorse += " and manutenzioni_fasi_risorse.data_inizio >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_a_data) then
	//ls_sql += " and manutenzioni.data_registrazione <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operatori += " and manutenzioni.data_registrazione <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " and manutenzioni.data_registrazione <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_generale += " and manutenzioni.data_registrazione <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
	
	//ls_sql_operatori += " and manutenzioni_fasi_operai.data_inizio <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
	//ls_sql_risorse += " and manutenzioni_fasi_risorse.data_inizio <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine_att_inizio) then
	//ls_sql += " and manutenzioni.data_fine_intervento >= '" + string(ldt_data_fine_att_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operatori += " and manutenzioni.data_fine_intervento >= '" + string(ldt_data_fine_att_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " and manutenzioni.data_fine_intervento >= '" + string(ldt_data_fine_att_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_generale += " and manutenzioni.data_fine_intervento >= '" + string(ldt_data_fine_att_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine_att_fine) then
	//ls_sql += " and manutenzioni.data_fine_intervento <= '" + string(ldt_data_fine_att_fine,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_operatori += " and manutenzioni.data_fine_intervento <= '" + string(ldt_data_fine_att_fine,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_risorse += " and manutenzioni.data_fine_intervento <= '" + string(ldt_data_fine_att_fine,s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql_generale += " and manutenzioni.data_fine_intervento <= '" + string(ldt_data_fine_att_fine,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ls_cod_operatore) then
	//ls_sql = ls_sql + " and manutenzioni_fasi_operai.cod_operaio = '" + ls_cod_operatore + "' "
	ls_sql_operatori += " and manutenzioni_fasi_operai.cod_operaio = '" + ls_cod_operatore + "' "
end if

if not isnull(ls_cod_risorsa_esterna) then
	//ls_sql = ls_sql + " and manutenzioni_fasi_risorse.cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "' "
	ls_sql_risorse += " and manutenzioni_fasi_risorse.cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "' "
end if

if not isnull(ls_attr_da) then
	//ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura >= '" + ls_attr_da + "' "
	ls_sql_operatori += " and manutenzioni.cod_attrezzatura >= '" + ls_attr_da + "' "
	ls_sql_risorse += " and manutenzioni.cod_attrezzatura >= '" + ls_attr_da + "' "
	ls_sql_generale += " and manutenzioni.cod_attrezzatura >= '" + ls_attr_da + "' "
	
end if

if not isnull(ls_attr_a) then
	//ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura <= '" + ls_attr_a + "' "
	ls_sql_operatori += " and manutenzioni.cod_attrezzatura <= '" + ls_attr_a + "' "
	ls_sql_risorse += " and manutenzioni.cod_attrezzatura <= '" + ls_attr_a + "' "
	ls_sql_generale += " and manutenzioni.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_tipo_manut) then
	//ls_sql = ls_sql + " and manutenzioni.cod_tipo_manutenzione = '" + ls_tipo_manut + "' "
	ls_sql_operatori += " and manutenzioni.cod_tipo_manutenzione = '" + ls_tipo_manut + "' "
	ls_sql_risorse += " and manutenzioni.cod_tipo_manutenzione = '" + ls_tipo_manut + "' "
	ls_sql_generale += " and manutenzioni.cod_tipo_manutenzione = '" + ls_tipo_manut + "' "
end if

if not isnull(ls_cod_richieste) and ls_cod_richieste <> "" then
	//ls_sql = ls_sql + " and manutenzioni.cod_tipo_manutenzione in ( " + ls_cod_richieste + " ) "
	ls_sql_operatori += " and manutenzioni.cod_tipo_manutenzione in ( " + ls_cod_richieste + " ) "
	ls_sql_risorse += " and manutenzioni.cod_tipo_manutenzione in ( " + ls_cod_richieste + " ) "
	ls_sql_generale += " and manutenzioni.cod_tipo_manutenzione in ( " + ls_cod_richieste + " ) "
end if

if not isnull(ls_categoria) then
	//ls_sql = ls_sql + " and anag_attrezzature.cod_cat_attrezzature = '" + ls_categoria + "' "
	ls_sql_operatori += " and anag_attrezzature.cod_cat_attrezzature = '" + ls_categoria + "' "
	ls_sql_risorse += " and anag_attrezzature.cod_cat_attrezzature = '" + ls_categoria + "' "
	ls_sql_generale += " and anag_attrezzature.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_cod_reparto) then
	//ls_sql = ls_sql + " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
	ls_sql_operatori += " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
	ls_sql_risorse += " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
	ls_sql_generale += " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
end if

if not isnull(ls_area) then
	//ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale = '" + ls_area + "' "
	ls_sql_operatori += " and anag_attrezzature.cod_area_aziendale = '" + ls_area + "' "
	ls_sql_risorse += " and anag_attrezzature.cod_area_aziendale = '" + ls_area + "' "
	ls_sql_generale += " and anag_attrezzature.cod_area_aziendale = '" + ls_area + "' "
end if

if not isnull(ls_divisione) then
	//ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "
	ls_sql_operatori += " and anag_attrezzature.cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "
	ls_sql_risorse += " and anag_attrezzature.cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "
	ls_sql_generale += " and anag_attrezzature.cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "
end if

ls_flag = ""

if ls_ordinaria = "S" then
	if ls_flag <> "" then
		ls_flag += " or "
	end if
	//ls_flag += " flag_ordinario = 'S' and flag_richieste_cartacee = 'N' and flag_richieste_telefoniche = 'N' and flag_risc_giro_isp = 'N' "
	ls_flag += " flag_ordinario = 'S' "
end if


//if ls_ric_car = "S" then
//	if ls_flag <> "" then
//		ls_flag += " or "
//	end if
//ls_flag += " flag_richieste_cartacee = 'S' "
//end if
//
//if ls_ric_tel = "S" then
//	if ls_flag <> "" then
//		ls_flag += " or "
//	end if
//	ls_flag += " flag_richieste_telefoniche = 'S' "
//end if
	
if ls_giro_isp = "S" then
	if ls_flag <> "" then
		ls_flag += " or "
	end if
	ls_flag += " flag_risc_giro_isp = 'S' "
end if

if ls_altre = "S" then
	if ls_flag <> "" then
		ls_flag += " or "
	end if
	//ls_flag += " flag_ordinario = 'N' and flag_richieste_cartacee = 'N' and flag_richieste_telefoniche = 'N' and flag_risc_giro_isp = 'N' "
	ls_flag += " (flag_richieste_cartacee = 'S' or flag_richieste_telefoniche = 'S') "
end if

if ib_guerrato then

	if ls_flag = "" then
		if isnull(ls_cod_richieste) or ls_cod_richieste = "" then
			//ls_flag = " flag_ordinario = 'XXX'"
		end if
	end if
	
	if ls_flag <> "" and not isnull(ls_flag) then
		//ls_sql += " and ( " + ls_flag + " ) "
		ls_sql_operatori += " and ( " + ls_flag + " ) "
		ls_sql_risorse += " and ( " + ls_flag + " ) "
		ls_sql_generale += " and ( " + ls_flag + " ) "
	end if

end if

//-------------------
ls_sql = ""
if not isnull(ls_cod_operatore) and (isnull(ls_cod_risorsa_esterna) or ls_cod_risorsa_esterna="" ) then
	ls_sql += ls_sql_operatori
	ls_sql += " order by manutenzioni.data_registrazione, manutenzioni.cod_attrezzatura "
elseif not isnull(ls_cod_risorsa_esterna) and (isnull(ls_cod_operatore) or ls_cod_operatore="" ) then
	ls_sql += ls_sql_risorse
	ls_sql += " order by manutenzioni.data_registrazione, manutenzioni.cod_attrezzatura "
else
	ls_sql += ls_sql_generale + " union all " + ls_sql_operatori + " union all " + ls_sql_risorse
	ls_sql += " order by data_registrazione, cod_attrezzatura "
end if

//ls_sql += " order by data_registrazione, manutenzioni.cod_attrezzatura "

st_1.text = "Preparazione della Query in corso attendere ...."

select max(prog_task)
into   :fl_prog_task
from   tab_manut_report_excel
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(fl_prog_task) or fl_prog_task = 0 then
	fl_prog_task = 1
else
	fl_prog_task ++
end if

ll_prog_record = 0
setnull(ldt_data_progressiva)

PREPARE SQLSA FROM :ls_sql ;

open dynamic manut_eseg;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch manut_eseg
	into  :ll_anno_registrazione,
	      :ll_num_registrazione,
	      :ldt_data_registrazione,
			:ls_flag_eseguito,
			:ldt_data_inizio_intervento,
			:ls_cod_attrezzatura,
			:ls_cod_tipo_manutenzione,
			:ls_des_attrezzatura,
			:ls_cod_reparto_attrezzatura,
			:ls_cod_area_aziendale,
			:ls_cod_cat_attrezzatura,
			:ls_des_tipo_manutenzione,
			:ll_operaio_ore,
			:ll_operaio_minuti,
			:ll_risorsa_ore,
			:ll_risorsa_minuti,
			:ls_cod_operaio_raggupp,
			:ldt_data_inizio_fase,
			:ldt_data_fine_fase
			;
	// fine modifica -------------------------------------------------------------------------------

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit		
	end if
	
	st_1.text = string(ldt_data_registrazione,"dd/mm/yyyy") + " - " + ls_cod_attrezzatura
	
	if isnull(ll_operaio_ore) then ll_operaio_ore = 0
	if isnull(ll_operaio_minuti) then ll_operaio_minuti = 0
	if isnull(ll_risorsa_ore) then ll_risorsa_ore = 0
	if isnull(ll_risorsa_minuti) then ll_risorsa_minuti = 0
	
	ll_operaio_ore = ((ll_operaio_ore * 60) + ll_operaio_minuti) + ((ll_risorsa_ore * 60) + ll_risorsa_minuti)
	
	ll_tempo_totale = 0
	ls_operatori = ""
	ls_cat_attrezzature = ""
	
	//calcolo della durata della fase
	//es inizio=15/09/2008
	//fine=18/09/2008
	//daysafter(inizio,fine)=3
	//durata fase= daysafter + 1 = 4 giorni
	//se inizio=fine allora si ha durata 1 giorno
	
	if isnull(ldt_data_inizio_fase) or isnull(ldt_data_fine_fase) then continue
	ll_durata_fase = abs(daysafter(date(ldt_data_inizio_fase),date(ldt_data_fine_fase))) + 1
	//----------
	
	if isnull(ldt_data_progressiva) then ldt_data_progressiva = ldt_da_data		
	
	do while ldt_data_progressiva < ldt_data_inizio_fase //ldt_data_registrazione
	
		wf_calcola_periodo(ldt_data_progressiva, ref ll_num_periodo, ref ls_label_periodo)
		
		istr_des_periodo[ll_num_periodo].des_periodo = ls_label_periodo
		
		ldt_data_progressiva = datetime(relativedate(date(ldt_data_progressiva), 1),00:00:00)
		
	loop
	
//	wf_calcola_periodo(ldt_data_registrazione, ref ll_num_periodo, ref ls_label_periodo)
	wf_calcola_periodo(ldt_data_inizio_fase, ref ll_num_periodo, ref ls_label_periodo)
	
	istr_des_periodo[ll_num_periodo].des_periodo = ls_label_periodo
	
	if ll_num_periodo > 245 then			// non posso gestire più di 255 colonne con excel
		g_mb.messagebox("OMNIA","A causa dei limiti di MsExcel non è possibile gestire più di 245 periodi~nL'elaborazione verrà interrotta.~nRestringere le date di elaborazione o selezionare tipo di periodi diversi.",StopSign!)
		return -1
	end if
	
	// *** trovo divisione
	
	select cod_divisione 
	into   :ls_cod_divisione
	from   tab_aree_aziendali
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_aziendale = ( select cod_area_aziendale
			                        from   anag_attrezzature
											where  cod_azienda = :s_cs_xx.cod_azienda and
											       cod_attrezzatura = :ls_cod_attrezzatura);
	// ***
	
	ll_num_eseguiti = 0
	ll_num_iniziati = 0
	ll_num_aperti   = 0
	
	if ls_flag_eseguito = "S" then
		ll_num_eseguiti = 1
	else
		if not isnull(ldt_data_inizio_intervento) and ldt_data_inizio_intervento > datetime(date(01/01/1900),00:00:00) then
			ll_num_iniziati = 1
		else
			ll_num_aperti   = 1
		end if
	end if
	
	if ib_guerrato then
		//Donato 28/07/2008
		//se l'operatore non è stato specificato in manutenzioni provare a cercare nella programmi_manutenzione
		if isnull(ls_cod_operaio_raggupp) or ls_cod_operaio_raggupp = "" then
			/*
			SELECT
				 programmi_manutenzione.cod_operaio
			INTO
				:ls_cod_operaio_raggupp
			FROM manutenzioni
			JOIN programmi_manutenzione ON manutenzioni.cod_azienda = programmi_manutenzione.cod_azienda
				AND manutenzioni.anno_reg_programma = programmi_manutenzione.anno_registrazione
				AND manutenzioni.num_reg_programma = programmi_manutenzione.num_registrazione
			WHERE manutenzioni.cod_azienda = :s_cs_xx.cod_azienda
				AND manutenzioni.anno_registrazione=:ll_anno_registrazione 
				AND manutenzioni.num_registrazione = :ll_num_registrazione;
			*/
			SELECT
				 isnull('*O*'+programmi_manutenzione.cod_operaio,'*R*'+programmi_manutenzione.cod_cat_risorse_esterne+'*R*'+programmi_manutenzione.cod_risorsa_esterna)
			INTO
				:ls_cod_operaio_raggupp
			FROM manutenzioni
			JOIN programmi_manutenzione ON manutenzioni.cod_azienda = programmi_manutenzione.cod_azienda
				AND manutenzioni.anno_reg_programma = programmi_manutenzione.anno_registrazione
				AND manutenzioni.num_reg_programma = programmi_manutenzione.num_registrazione
			WHERE manutenzioni.cod_azienda = :s_cs_xx.cod_azienda
				AND manutenzioni.anno_registrazione=:ll_anno_registrazione 
				AND manutenzioni.num_registrazione = :ll_num_registrazione;
				
			if ls_cod_operaio_raggupp = '*O*' or ls_cod_operaio_raggupp='*R*' or &
						 ls_cod_operaio_raggupp='*R**R*' then setnull(ls_cod_operaio_raggupp)
		end if
		
		if not isnull(ls_flag_ore) and ls_flag_ore = "S" then
			ll_num_eseguiti = 0
			ll_num_iniziati = 0
			ll_num_aperti   = 0			
			if ls_flag_eseguito = "S" then
				ll_num_eseguiti = ll_operaio_ore
			else
				if not isnull(ldt_data_inizio_intervento) and ldt_data_inizio_intervento > datetime(date(01/01/1900),00:00:00) then
					ll_num_iniziati = ll_operaio_ore
				else
					//Donato 28/07/2008
					//se non è stato codificato l'operaio in manutenzioni
					//recuperare l'operaio, le ore e i minuti dalla programmi_manutenzione
					long ll_anno_registrazione_prog, ll_num_registrazione_prog
					long ll_operaio_ore_prog, ll_operaio_minuti_prog, ll_risorsa_ore_prog, ll_risorsa_minuti_prog
					
					//ll_num_aperti   = ll_operaio_ore
					/*
					SELECT
						 operaio_ore_previste
						,operaio_minuti_previsti
						,risorsa_ore_previste
						,risorsa_minuti_previsti
						,programmi_manutenzione.cod_operaio
					INTO
						 :ll_operaio_ore_prog
						,:ll_operaio_minuti_prog
						,:ll_risorsa_ore_prog
						,:ll_risorsa_minuti_prog
						,:ls_cod_operaio_raggupp
					FROM manutenzioni
					JOIN programmi_manutenzione ON manutenzioni.cod_azienda = programmi_manutenzione.cod_azienda
						AND manutenzioni.anno_reg_programma = programmi_manutenzione.anno_registrazione
						AND manutenzioni.num_reg_programma = programmi_manutenzione.num_registrazione
					WHERE manutenzioni.cod_azienda = :s_cs_xx.cod_azienda
						AND manutenzioni.anno_registrazione=:ll_anno_registrazione 
						AND manutenzioni.num_registrazione = :ll_num_registrazione;
					*/
					SELECT
						 isnull('*O*'+programmi_manutenzione.cod_operaio,'*R*'+programmi_manutenzione.cod_cat_risorse_esterne+'*R*'+programmi_manutenzione.cod_risorsa_esterna)
					INTO
						:ls_cod_operaio_raggupp
					FROM manutenzioni
					JOIN programmi_manutenzione ON manutenzioni.cod_azienda = programmi_manutenzione.cod_azienda
						AND manutenzioni.anno_reg_programma = programmi_manutenzione.anno_registrazione
						AND manutenzioni.num_reg_programma = programmi_manutenzione.num_registrazione
					WHERE manutenzioni.cod_azienda = :s_cs_xx.cod_azienda
						AND manutenzioni.anno_registrazione=:ll_anno_registrazione 
						AND manutenzioni.num_registrazione = :ll_num_registrazione;
					
					
					if isnull(ll_operaio_ore_prog) then ll_operaio_ore_prog=0
					if isnull(ll_operaio_minuti_prog) then ll_operaio_minuti_prog=0
					if isnull(ll_risorsa_ore_prog) then ll_risorsa_ore_prog=0
					if isnull(ll_risorsa_minuti_prog) then ll_risorsa_minuti_prog=0
					if ls_cod_operaio_raggupp='' then setnull(ls_cod_operaio_raggupp)
					if ls_cod_operaio_raggupp = '*O*' or ls_cod_operaio_raggupp='*R*' or &
						 ls_cod_operaio_raggupp='*R**R*' then setnull(ls_cod_operaio_raggupp)
					
					ll_operaio_ore_prog = ((ll_operaio_ore_prog * 60) + ll_operaio_minuti_prog) + ((ll_risorsa_ore_prog * 60) + ll_risorsa_minuti_prog)
					ll_num_aperti   = ll_operaio_ore_prog
					//fine modifica
				end if
			end if			
			
		end if
	end if
	
	
	ll_prog_record ++
	
	
	if isnull(ls_cod_operaio_raggupp) then ls_cod_operaio_raggupp = "1234567"	//codice sicuramente inesistente in anag_operai
	
	//codifico in "ll_num_eseguiti" se l'intervento è eseguito oppure no
	if ls_flag_eseguito = "S" then
		ll_num_eseguiti = 1
	else
		ll_num_eseguiti = 0
	end if	
	
	INSERT INTO tab_manut_report_excel  
			( cod_azienda,   
			  prog_task,   
			  prog_record,   
			  gruppo_1,   
			  gruppo_2,   
			  gruppo_3,   
			  gruppo_4,   
			  gruppo_5,   
			  gruppo_6,   
			  gruppo_7,
			  num_periodo,   
			  num_eseguiti,   
			  num_iniziati,   
			  num_aperti,
			  inizio_fase,
			  fine_fase
			  )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :fl_prog_task,   
			  :ll_prog_record,   
			  :ls_cod_area_aziendale,   
			  :ls_cod_reparto_attrezzatura,   
			  :ls_cod_cat_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_cod_attrezzatura,
			  :ls_cod_divisione,
			  :ls_cod_operaio_raggupp,   
			  :ll_num_periodo,   
			  :ll_num_eseguiti,   
			  :ll_num_iniziati,   
			  :ll_num_aperti,
			  :ldt_data_inizio_fase,
			  :ldt_data_fine_fase
			  )  ;
	// fine modifica
	
	if sqlca.sqlcode = 0 then
	else
		g_mb.messagebox("OMNIA","Errore durante l'elaborazione nella tabella tab_manut_report_excel.~n"+sqlca.sqlerrtext,StopSign!)
		return -1
	end if
	
loop


lstr_des_periodo = istr_des_periodo

close manut_eseg;

return 0
end function

public subroutine wf_generazione_foglio_excel (long fl_prog_task);string ls_gruppo_old[7], ls_gruppo[7], ls_flag_tipo_livello[5], ls_sql, ls_sql_select, ls_sql_orderby, ls_sql_groupby, &
       ls_stringvar, ls_descrizione, ls_des_date, ls_rag_soc_azienda, ls_tipo_periodo, ls_flag_ore
integer li_i, li_y
long ll_num_periodo, ll_num_eseguiti, ll_num_iniziati, ll_num_aperti,ll_errore, ll_riga_excel, ll_colonna_excel,&
	  ll_periodo_riferimento, ll_cont_eseguiti, ll_cont_iniziati, ll_cont_aperti, ll_num_livelli, ll_nrows, ll_range, &
	  ll_cont, ll_riga_periodo
	  
long ll_durata_fase, ll_durata_fase_temp, ll_max_num_periodo, ll_extended_periodo, li_i2, ll_appo
string ls_verde, ls_rosso, ls_arancio, ls_blu, ls_giallo, ls_sabato, ls_domenica
	  
decimal ld_decimalvar
datetime ldt_data_riferimento, ldt_data_inizio, ldt_data_fine
datetime ldt_inizio_fase, ldt_fine_fase, ldt_datetime, ldt_inizio_fase_tmp, ldt_fine_fase_tmp
wstr_des_periodo lstr_periodo_extd[]
string ls_appo, ls_appo_old

OLEObject myoleobject
declare cu_manut_excel DYNAMIC CURSOR FOR SQLSA ; 
	
ldt_data_inizio = dw_sel_manut_eseguite.getitemdatetime(dw_sel_manut_eseguite.getrow(),"data_da")
ldt_data_fine   = dw_sel_manut_eseguite.getitemdatetime(dw_sel_manut_eseguite.getrow(),"data_a")
ls_tipo_periodo   = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_periodo")

if ib_guerrato then	
	ls_flag_ore = dw_sel_manut_eseguite.getitemstring( 1, "flag_ore")
end if

select rag_soc_1
into   :ls_rag_soc_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ldt_data_inizio) then
	ls_des_date = "Dalla prima data  "
else
	ls_des_date = "Dal " + string(ldt_data_inizio,"dd/mm/yyyy")
end if	

if isnull(ldt_data_fine) then
	ls_des_date += "  All'ultima data  "
else
	ls_des_date += "  al " + string(ldt_data_fine,"dd/mm/yyyy")
end if	

ldt_data_riferimento = datetime(today(), 00:00:00)
wf_calcola_periodo(ldt_data_riferimento, ref ll_periodo_riferimento, ref ls_descrizione)

ls_sql_select = ""
ls_sql_orderby = ""
ls_sql_groupby = ""
ll_num_livelli = 0

for li_i = 1 to 5
	
	ls_flag_tipo_livello[li_i] = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_livello_"+string(li_i))
	
	choose case ls_flag_tipo_livello[li_i]
			
		case "N"
			continue
			
		case "R"		// Reparto - Ubicazione
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_1 "
			ls_sql_orderby += " gruppo_1 ASC "
			ls_sql_groupby += " gruppo_1 "
			ll_num_livelli ++
			
		case "E"		// Area Aziendale - Impianto
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_2 "
			ls_sql_orderby += " gruppo_2 ASC "
			ls_sql_groupby += " gruppo_2 "
			ll_num_livelli ++
		
		case "C"		// Categ Attrezzatura
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_3 "
			ls_sql_orderby += " gruppo_3 ASC "
			ls_sql_groupby += " gruppo_3 "
			ll_num_livelli ++
		
		case "T"		// Tipo Manutenzione
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_4 "
			ls_sql_orderby += " gruppo_4 ASC "
			ls_sql_groupby += " gruppo_4 "
			ll_num_livelli ++
		
		case "A"		// Attrezzatura
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_5 "
			ls_sql_orderby += " gruppo_5 ASC "
			ls_sql_groupby += " gruppo_5 "
			ll_num_livelli ++
			
		case "D"		// Divisione - Commessa
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_6 "
			ls_sql_orderby += " gruppo_6 ASC "
			ls_sql_groupby += " gruppo_6 "
			ll_num_livelli ++			
			
		//Donato 06/08/2008 inserito per il raggruppamento per Operatore
		case "O"		// Operatore
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_7 "
			ls_sql_orderby += " gruppo_7 ASC "
			ls_sql_groupby += " gruppo_7 "
			ll_num_livelli ++
		// fine modifica -------------------------------------------------------------------------
		
	end choose
next

if len(ls_sql_select) > 0 then 
	ls_sql_select += ", "
	ls_sql_orderby += ", "
	ls_sql_groupby += ", "
end if

//Donato 06/08/2008 commentato e inserito nuovo codice sotto

/*
ls_sql = " select " + ls_sql_select + " gruppo_7, num_periodo, sum(num_eseguiti), sum(num_iniziati), sum(num_aperti) " + &
         " from tab_manut_report_excel  " + &
			" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  prog_task = " + string(fl_prog_task) + &
			" group by " + ls_sql_groupby + " gruppo_7,     num_periodo " + &
			" order by " + ls_sql_orderby + " gruppo_7 ASC, num_periodo ASC "
*/

//NUOVO CODICE
ls_sql = " select " + ls_sql_select + " num_periodo, sum(num_eseguiti), sum(num_iniziati), sum(num_aperti) " + &
			",inizio_fase,fine_fase " + &
         " from tab_manut_report_excel  " + &
			" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  prog_task = " + string(fl_prog_task) + &
			" group by " + ls_sql_groupby + " num_periodo,inizio_fase,fine_fase " + &
			" order by " + ls_sql_orderby + " num_periodo ASC "
// fine modifica


PREPARE SQLSA FROM :ls_sql;

DESCRIBE SQLSA into SQLDA;
			
open DYNAMIC cu_manut_excel using descriptor SQLDA;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in apertura cursore cu_manut_excel.~r~n"+sqlca.sqlerrtext)
	return
end if

// creazione oggetto EXCEL 
myoleobject = CREATE OLEObject

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return
end if

ls_verde='4'
ls_rosso='3'
ls_arancio='45'
ls_blu='17'
ls_giallo='6'
ls_sabato = '15'
ls_domenica = '16'

// caratteristiche del foglio in generale
myoleobject.Visible = True
myoleobject.Workbooks.Add
myoleobject.sheets("foglio1").PageSetup.PrintGridlines = True
myoleobject.sheets("foglio1").PageSetup.orientation = 2
myoleobject.sheets("foglio1").PageSetup.leftmargin = 40
myoleobject.sheets("foglio1").PageSetup.rightmargin = 40
myoleobject.sheets("foglio1").PageSetup.topmargin = 40
myoleobject.sheets("foglio1").PageSetup.bottommargin = 40
// imposto testata

myoleobject.sheets("foglio1").range("A1:M1").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A1:M1").merge(true)
myoleobject.sheets("foglio1").range("A1:M1").font.size = "14"
myoleobject.sheets("foglio1").range("A1:M1").Font.Bold = True
myoleobject.sheets("foglio1").range("A1:M1").Font.Italic = True
myoleobject.sheets("foglio1").range("A1:M1").font.ColorIndex = "1"
myoleobject.sheets("foglio1").range("A1:M1").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").cells(1,1).value = ls_rag_soc_azienda
myoleobject.sheets("foglio1").range("A1:M1").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A1:M1").Interior.ColorIndex = "3"
myoleobject.sheets("foglio1").range("A2:M2").merge(true)
myoleobject.sheets("foglio1").range("A2:M2").Interior.ColorIndex = "3"
myoleobject.sheets("foglio1").range("A2:M2").borders.linestyle = "1"

myoleobject.sheets("foglio1").range("A3:M3").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A3:M3").merge(true)
myoleobject.sheets("foglio1").range("A3:M3").font.size = "20"
myoleobject.sheets("foglio1").range("A3:M3").Font.Bold = True
myoleobject.sheets("foglio1").range("A3:M3").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").range("A3:M3").value = "- PROGRAMMAZIONE PERIODO -"
myoleobject.sheets("foglio1").range("A3:M3").Interior.ColorIndex = "36"
myoleobject.sheets("foglio1").range("A3:M3").borders.linestyle = "1"

myoleobject.sheets("foglio1").range("A4:M4").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A4:M4").merge(true)
myoleobject.sheets("foglio1").range("A4:M4").font.size = "12"
myoleobject.sheets("foglio1").range("A4:M4").Font.Bold = True
myoleobject.sheets("foglio1").range("A4:M4").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").range("A4:M4").value = ls_des_date
myoleobject.sheets("foglio1").range("A4:M4").Interior.ColorIndex = "36"
myoleobject.sheets("foglio1").range("A4:M4").borders.linestyle = "1"

// TITOLO Legenda
myoleobject.sheets("foglio1").range("A5:M5").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A5:M5").font.size = "12"
myoleobject.sheets("foglio1").range("A5:M5").Font.Bold = True
myoleobject.sheets("foglio1").range("A5:M5").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").rows(5).rowheight = "15"
myoleobject.sheets("foglio1").cells(5,1).value = "LEGENDA DEI COLORI DELLE CELLE:"

// imposto la legenda del colore BLU
myoleobject.sheets("foglio1").range("A6:M6").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A6:M6").font.size = "8"
myoleobject.sheets("foglio1").range("A6:M6").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A6:A6").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A6:A6").Interior.ColorIndex = ls_blu
myoleobject.sheets("foglio1").cells(6,1).value = ""
myoleobject.sheets("foglio1").cells(6,2).value = "Attività non iniziata."

// imposto la legenda del colore VERDE
myoleobject.sheets("foglio1").range("A7:M7").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A7:M7").font.size = "8"
myoleobject.sheets("foglio1").range("A7:M7").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A7:A7").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A7:A7").Interior.ColorIndex = ls_verde
myoleobject.sheets("foglio1").cells(7,1).value = ""
myoleobject.sheets("foglio1").cells(7,2).value = "Attività regolamente iniziata e terminata."

// imposto la legenda del colore GIALLO
myoleobject.sheets("foglio1").range("A8:M8").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A8:M8").font.size = "8"
myoleobject.sheets("foglio1").range("A8:M8").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A8:A8").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A8:A8").Interior.ColorIndex = ls_giallo
myoleobject.sheets("foglio1").cells(8,1).value = ""
myoleobject.sheets("foglio1").cells(8,2).value = "Attività in scadenza."

// imposto la legenda del colore ROSSO
myoleobject.sheets("foglio1").range("A9:M9").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A9:M9").font.size = "8"
myoleobject.sheets("foglio1").range("A9:M9").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A9:A9").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A9:A9").Interior.ColorIndex = ls_rosso
myoleobject.sheets("foglio1").cells(9,1).value = ""
myoleobject.sheets("foglio1").cells(9,2).value = "Attività scaduta."

if ls_tipo_periodo = "G" then
	// imposto la legenda del colore per il SABATO
	myoleobject.sheets("foglio1").cells(8,6).borders.linestyle = "1"
	myoleobject.sheets("foglio1").cells(8,6).Interior.ColorIndex = ls_sabato
	myoleobject.sheets("foglio1").cells(8,6).value = ""
	myoleobject.sheets("foglio1").cells(8,7).value = "Sabato"
	
	// imposto la legenda del colore per la DOMENICA
	myoleobject.sheets("foglio1").cells(9,6).borders.linestyle = "1"
	myoleobject.sheets("foglio1").cells(9,6).Interior.ColorIndex = ls_domenica
	myoleobject.sheets("foglio1").cells(9,6).value = ""
	myoleobject.sheets("foglio1").cells(9,7).value = "Domenica"
end if

long ll_x, ll_y

OPEN(w_report_prog_periodo_excel_child,THIS)

ll_x = ( this.width - w_report_prog_periodo_excel_child.width ) / 2
ll_y = ( this.height - w_report_prog_periodo_excel_child.height ) / 2

w_report_prog_periodo_excel_child.x = ll_x
w_report_prog_periodo_excel_child.y = ll_y

ls_gruppo_old[1] = ""
ls_gruppo_old[2] = ""
ls_gruppo_old[3] = ""
ls_gruppo_old[4] = ""
ls_gruppo_old[5] = ""
ls_gruppo_old[6] = ""
ls_gruppo_old[7] = ""

li_i = 0

ll_riga_excel = 11
ll_riga_periodo = ll_riga_excel
ll_colonna_excel = 10
ll_cont_eseguiti = 0
ll_cont_iniziati = 0
ll_cont_aperti	  = 0
ll_cont = 0


//qui occorre scrivere un codice che eventualmente prolunga l'intestazione ------------------------------------------------------------------
select max(num_periodo)
into :ll_max_num_periodo
from tab_manut_report_excel
where cod_azienda = :s_cs_xx.cod_azienda and
      prog_task = :fl_prog_task
;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante la select max(num_periodo).~r~n"+sqlca.sqlerrtext)
	return
end if

select max(fine_fase) 
into :ldt_fine_fase_tmp
from tab_manut_report_excel
where cod_azienda = :s_cs_xx.cod_azienda and
      prog_task = :fl_prog_task and num_periodo= :ll_max_num_periodo
;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante la select max(fine_fase).~r~n"+sqlca.sqlerrtext)
	return
end if

select max(inizio_fase) 
into :ldt_inizio_fase_tmp
from tab_manut_report_excel
where cod_azienda = :s_cs_xx.cod_azienda and
      prog_task = :fl_prog_task and num_periodo= :ll_max_num_periodo and fine_fase= :ldt_fine_fase_tmp
;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante la select max(inizio_fase).~r~n"+sqlca.sqlerrtext)
	return
end if

long ll_min_col, ll_max_col, ll_min_row, ll_max_row

ll_min_col = ll_colonna_excel + 1
ll_min_row = ll_riga_excel + 1
for li_i = 1 to upperbound(istr_des_periodo)
	
	//if li_i = 1 then ldt_inizio_fase_tmp = datetime(istr_des_periodo[li_i].des_periodo)
	
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).font.size = "8"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).orientation = "90"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).value = istr_des_periodo[li_i].des_periodo
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).HorizontalAlignment = "3"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).borders.linestyle = "1"
	myoleobject.sheets("foglio1").columns(ll_colonna_excel + li_i).columnwidth = "2" //"4"
	
	ll_max_col = ll_colonna_excel + li_i
next
//N.B. all'uscita di questo for li_i vale upperbound(istr_des_periodo) + 1

ls_appo = ""
ls_appo_old = ""
li_i2 = 0

do while ldt_fine_fase_tmp > ldt_inizio_fase_tmp
	//spostati di 1 giorno
	ldt_inizio_fase_tmp = datetime(relativedate(date(ldt_inizio_fase_tmp), 1), now())
	
	//valuta la label
	wf_calcola_durata(ldt_inizio_fase_tmp, ldt_fine_fase_tmp, ls_tipo_periodo, ll_extended_periodo,ls_appo)
	if ls_appo <> ls_appo_old and ls_appo <> istr_des_periodo[li_i -1].des_periodo then
		//metti l'intestazione				
		myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i + li_i2).font.size = "8"
		myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i + li_i2).orientation = "90"
		myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i + li_i2).value = ls_appo
		myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i + li_i2).HorizontalAlignment = "3"
		myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i + li_i2).borders.linestyle = "1"
		myoleobject.sheets("foglio1").columns(ll_colonna_excel + li_i + li_i2).columnwidth = "2"//"4"
		
		ll_max_col = ll_colonna_excel + li_i + li_i2
		
		//salva l'ultima label
		ls_appo_old = ls_appo
		li_i2 += 1
	end if		
loop

//range colonne da  a
//-----------------------------------------------------------------------------------------------------------------------------------------------

do while true
	fetch cu_manut_excel USING DESCRIPTOR SQLDA ;

	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in apertura cursore cu_manut_excel.~r~n"+sqlca.sqlerrtext)
		return
	end if
	
	ll_cont ++

	st_1.text = string(ll_cont)
//	ll_riga_excel ++
	
	for li_i = 1 to upperbound(SQLDA.OutParmType[])
		CHOOSE CASE SQLDA.OutParmType[li_i]
				
			CASE TypeString!
				ls_stringvar = GetDynamicString(SQLDA, li_i)
				
			CASE TypeInteger!,TypeLong!,TypeDecimal!
					ld_decimalvar = GetDynamicNumber(SQLDA, li_i)
					
			CASE TypeDateTime!
					ldt_datetime = GetDynamicDateTime(SQLDA, li_i)
					
		END CHOOSE
		

		if li_i <= ll_num_livelli then
			ls_gruppo[li_i] = ls_stringvar
		else
			choose case li_i
				case ll_num_livelli + 1		// num_periodo
					ll_num_periodo = ld_decimalvar
				case ll_num_livelli + 2		// num_eseguiti,
					ll_num_eseguiti = ld_decimalvar
				case ll_num_livelli + 3		// num_iniziati
					ll_num_iniziati = ld_decimalvar
				case ll_num_livelli + 4		// num_aperti
					ll_num_aperti = ld_decimalvar
				case ll_num_livelli + 5		// inizio_fase
					ldt_inizio_fase = ldt_datetime
				case ll_num_livelli + 6		// fine_fase
					ldt_fine_fase = ldt_datetime
					
			end choose
		end if
			
		if li_i <= 7 then
			
			if ls_gruppo_old[li_i] <> ls_gruppo[li_i] then
				
				// nuova intestazione di gruppo
				ls_gruppo_old[li_i] = ls_gruppo[li_i]
				
				for li_y = li_i + 1 to 7		// elimino i gruppi sottostanti per evitare omonimie di codici.
					ls_gruppo_old[li_y] = ""
				next
				
				ls_descrizione = ""
				choose case ls_flag_tipo_livello[li_i]
					
					case "N"
						
					case "D"		// Divisioni - Commesse
						select des_divisione
						into :ls_descrizione
						from  anag_divisioni
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_divisione = :ls_gruppo[li_i];						
						
					case "R"		// Reparto - Impianti
						select des_area
						into :ls_descrizione
						from  tab_aree_aziendali
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_area_aziendale = :ls_gruppo[li_i];
					
					case "E"		// Area Aziendale - Ubicazioni
						select des_reparto
						into :ls_descrizione
						from anag_reparti
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_reparto = :ls_gruppo[li_i];
					
					case "C"		// Categ Attrezzatura
						select des_cat_attrezzature
						into :ls_descrizione
						from tab_cat_attrezzature
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_cat_attrezzature = :ls_gruppo[li_i];
								
					case "T"		// Tipo Manutenzione
						select des_tipo_manutenzione
						into :ls_descrizione
						from tab_tipi_manutenzione
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_tipo_manutenzione = :ls_gruppo[li_i];
						wf_pulisci_stringa(ls_descrizione)
						
					case "A"		// Attrezzatura
						select descrizione
						into :ls_descrizione
						from anag_attrezzature
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_attrezzatura = :ls_gruppo[li_i];
								
					//Donato 06/08/2008 introdotto raggruppamento per Operatore
					case "O"		// Operatore
						string ls_nome, ls_temp1, ls_temp2
						integer li_pos1
						
						ls_temp1 = ls_gruppo[li_i]
						if ls_temp1 = "1234567" then
							ls_gruppo[li_i] = ""
							ls_descrizione = "operatore/risorsa non specificato/a"
							
						elseif pos(ls_temp1, "*O*") > 0 then
							ls_temp1=right(ls_temp1, len(ls_temp1) - 3)
						
							select cognome,nome
							into :ls_descrizione,:ls_nome
							from anag_operai
							where cod_azienda = :s_cs_xx.cod_azienda and
									cod_operaio = :ls_temp1; //:ls_gruppo[li_i];
									
							if isnull(ls_descrizione) then ls_descrizione = ""
							if isnull(ls_nome) then ls_nome = ""
							ls_descrizione += " " + ls_nome
							ls_gruppo[li_i] = "" //ls_temp1
									
						elseif pos(ls_temp1, "*R*") > 0 then
							li_pos1 = pos(ls_temp1, "*R*")
							ls_temp2 = right(ls_temp1, len(ls_temp1) - 3)
							li_pos1 = pos(ls_temp2, "*R*")
							
							ls_temp1 = left(ls_temp2, li_pos1 -1)							
							ls_temp2 = right(ls_temp2, len(ls_temp2) - (li_pos1 - 1) - 3)
							
							select descrizione
							into :ls_descrizione
							from anag_risorse_esterne
							where cod_azienda = :s_cs_xx.cod_azienda and
									cod_cat_risorse_esterne = :ls_temp1 and
									cod_risorsa_esterna = :ls_temp2;
									
							if isnull(ls_descrizione) then ls_descrizione = ""
							//ls_descrizione += " " + ls_nome
							ls_gruppo[li_i] = ""
							
						else
							ls_gruppo[li_i] = ""
							ls_descrizione = "operatore/risorsa non specificato/a"
						end if
						
				end choose
				
				ll_riga_excel ++
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).font.size = "8"
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).Font.Bold = True
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).VerticalAlignment = "3"
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).HorizontalAlignment = "2"
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).value = ls_gruppo[li_i] + " " + ls_descrizione
				myoleobject.sheets("foglio1").rows(ll_riga_excel).rowheight = "15"
				
				ll_max_row = ll_riga_excel
			end if
					
		end if	
		
	next		
	
	// imposto la cella e la colonna del periodo prescelto
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).font.size = "8"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).orientation = "90"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).value = istr_des_periodo[ll_num_periodo].des_periodo
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).HorizontalAlignment = "3"
	myoleobject.sheets("foglio1").columns(ll_colonna_excel + ll_num_periodo).columnwidth = "2"//"4"

	// imposto le proprietà della riga in cui scrivero' i dati
	myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).font.size = "8"
	myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).VerticalAlignment   = "3"
	myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).HorizontalAlignment = "3"

	ll_durata_fase_temp = 0
	ll_durata_fase = 0
	
	
	//#################################
	datetime ldt_adesso
	ldt_adesso = datetime(today(),time("00:00:00"))		
	
	if ll_num_eseguiti > 0 then //ldt_inizio_fase > ldt_adesso or ll_num_eseguiti > 0 then
		//intervento eseguito
		//VERDE °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
		wf_calcola_durata(ldt_inizio_fase,ldt_fine_fase,ls_tipo_periodo,ll_durata_fase,ls_appo)
		
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = ls_verde
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).borders.linestyle = "1"
		
		if ll_durata_fase > 1 then
			//estendere l'ampiezza -----------------------------------------------------
			ll_durata_fase_temp = 2
			do while ll_durata_fase_temp <= ll_durata_fase
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).interior.ColorIndex = ls_verde
				
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).borders.linestyle = "1"
				ll_durata_fase_temp += 1
			loop
		end if	
		//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	elseif ldt_adesso >= ldt_inizio_fase and ldt_adesso <= ldt_fine_fase then
		//GIALLO °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
		wf_calcola_durata(ldt_inizio_fase,ldt_fine_fase,ls_tipo_periodo,ll_durata_fase,ls_appo)
		
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = ls_giallo
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).borders.linestyle = "1"
			
		if ll_durata_fase > 1 then
			//estendere l'ampiezza -----------------------------------------------------
			ll_durata_fase_temp = 2
			DO WHILE ll_durata_fase_temp <= ll_durata_fase
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).interior.ColorIndex = ls_giallo
				
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).borders.linestyle = "1"
				ll_durata_fase_temp += 1
			LOOP
		end if
		//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	elseif ldt_adesso > ldt_fine_fase then
		//ROSSO °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
		wf_calcola_durata(ldt_inizio_fase,ldt_fine_fase,ls_tipo_periodo,ll_durata_fase,ls_appo)
		
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = ls_rosso
		
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).borders.linestyle = "1"
			
		if ll_durata_fase > 1 then
			//estendere l'ampiezza -----------------------------------------------------
			ll_durata_fase_temp = 2
			DO WHILE ll_durata_fase_temp <= ll_durata_fase
				
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).interior.ColorIndex = ls_rosso
				
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).borders.linestyle = "1"
				ll_durata_fase_temp += 1
			LOOP
		end if
		//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	else
		//NON INIZIATA: blu
		wf_calcola_durata(ldt_inizio_fase,ldt_fine_fase,ls_tipo_periodo,ll_durata_fase,ls_appo)
		
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = ls_blu
		
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).borders.linestyle = "1"
		
		if ll_durata_fase > 1 then
			//estendere l'ampiezza -----------------------------------------------------
			ll_durata_fase_temp = 2
			DO WHILE ll_durata_fase_temp <= ll_durata_fase
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).interior.ColorIndex = ls_blu
				
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo + ll_durata_fase_temp - 1).borders.linestyle = "1"
				ll_durata_fase_temp += 1
			LOOP
		end if	
	end if
loop

long ll_i, ll_j, ll_ret

for ll_i = ll_min_row to ll_max_row
	for ll_j = ll_min_col to ll_max_col - 1
		ll_ret = 0
		ll_ret = wf_festivo(istr_des_periodo[ll_j - 10].des_periodo, true, ls_tipo_periodo)
		choose case ll_ret
			case 2
				//domenica
				myoleobject.sheets("foglio1").cells(ll_i, ll_j).interior.ColorIndex = ls_domenica
				myoleobject.sheets("foglio1").cells(ll_i, ll_j).borders.linestyle = "1"
			case 1
				//sabato
				myoleobject.sheets("foglio1").cells(ll_i, ll_j).interior.ColorIndex = ls_sabato
				myoleobject.sheets("foglio1").cells(ll_i, ll_j).borders.linestyle = "1"
			case else
				
		end choose		
	next
next

destroy myoleobject

//pulizia tabella temporanea di appoggio
delete from tab_manut_report_excel
where cod_azienda = :s_cs_xx.cod_azienda and
      prog_task = :fl_prog_task;
		
commit;

CLOSE(w_report_prog_periodo_excel_child)

this.setfocus()

g_mb.messagebox("OMNIA", "Generazione Foglio Excel~nEseguita !")

end subroutine

public subroutine wf_calcola_durata (datetime fdt_data_inizio, datetime fdt_data_fine, string fs_tipo_periodo, ref long fl_num_periodo, ref string fs_label_periodo);//Donato 30/09/2008
/*questa funzione ritorna il numero di "periodi" (fl_num_periodo) tra due date (fdt_data_inizio, fdt_data_fine) tenendo presente la tipologia del periodo (fs_tipo_periodo)
esempi
---------------------------------
- fdt_data_inizio	= 12/05/2008
- fdt_data_fine		= 21/05/2008			--->  fl_num_periodo = 10 (cioè il periodo copre l'arco di 10 giorni: 12,13,14,15,16,17,18,19,20,21)
- fs_tipo_periodo	= 'G' (giorni)
---------------------------------
---------------------------------
- fdt_data_inizio	= 12/05/2008
- fdt_data_fine		= 21/05/2008			--->  fl_num_periodo = 2 (cioè il periodo copre l'arco di 2 settimane)
- fs_tipo_periodo	= 'S' (settimane)
---------------------------------
---------------------------------
- fdt_data_inizio	= 12/05/2008
- fdt_data_fine		= 21/06/2008			--->  fl_num_periodo = 2 (cioè il periodo copre l'arco di 2 mesi: maggio, giugno)
- fs_tipo_periodo	= 'M' (mesi)
---------------------------------
*/

string ls_data
long ll_giorno, ll_mese, ll_anno, ll_test 
date ld_data_rif

choose case fs_tipo_periodo
	case "G"
		fl_num_periodo = DaysAfter( date(fdt_data_inizio), date(fdt_data_fine) ) + 1
		fs_label_periodo = string(fdt_data_inizio, "dd/mm/yyyy")
		
	//Donato 02/10/2008
	//modifica per considerare le settimane dal lunedi alla domenica
	case "S" 		// la settimana inizia la domenica e finisce al sabato.		
		ll_giorno = daynumber( date(fdt_data_inizio) )
		ld_data_rif = relativedate(date(fdt_data_inizio), 8 - ll_giorno) // trovo la data della prima domenica successiva
		fl_num_periodo = 1
		
		do while ld_data_rif < date(fdt_data_fine)
			fl_num_periodo ++
			
			// trovo la data del prossimo fine settimana
			ld_data_rif = relativedate(ld_data_rif, 7 )		
		loop
		fs_label_periodo = "'Sett. " + string(wf_num_settimana(fdt_data_inizio)) + "/" + string(year(date(fdt_data_inizio)),"0000")		
	
	//Vecchio Codice
	/*
	case "S" 		// la settimana inizia la domenica e finisce al sabato.
		ll_giorno = daynumber( date(fdt_data_inizio) )
		ld_data_rif = relativedate(date(fdt_data_inizio), 7 - ll_giorno) // trovo la data del primo sabato
		fl_num_periodo = 1
		
		do while ld_data_rif < date(fdt_data_fine)
			fl_num_periodo ++
			
			// trovo la data del prossimo fine settimana
			ld_data_rif = relativedate(ld_data_rif, 7 )		
		loop
		fs_label_periodo = "'Sett. " + string(wf_num_settimana(fdt_data_inizio)) + "/" + string(year(date(fdt_data_inizio)),"0000")		
		*/
		//fine modifica ---------------------------------------------------------
		
	case "M"
		ll_test = 0
		ll_mese = month( date(fdt_data_inizio) ) - 1 
		ll_anno = year( date(fdt_data_inizio) )
		
		fl_num_periodo = 0
		
		do 
			fl_num_periodo ++
			ll_test ++
			
			// calcolo il prossimo mese
			ll_mese ++
			if ll_mese > 12 then
				ll_mese = 1
				ll_anno ++
			end if
			
			ls_data = "01/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
			ld_data_rif = date(ls_data)
			
		loop until ld_data_rif > date( fdt_data_fine)
		
		fl_num_periodo --
		ll_mese --
		fs_label_periodo = "Mese " + string(ll_mese) + "/" + string(ll_anno,"0000")
		
end choose
end subroutine

public subroutine wf_calcola_periodo_2 (datetime fdt_data_registrazione, datetime fdt_data_inizio, ref long fl_num_periodo, ref string fs_label_periodo);string ls_tipo_periodo, ls_data
long ll_giorno, ll_mese, ll_anno, ll_test 
date ld_data_rif
datetime ldt_data_inizio
datetime ldt_data_fine


ldt_data_inizio = fdt_data_inizio//dw_sel_manut_eseguite.getitemdatetime(1,"data_da")
ldt_data_fine   = dw_sel_manut_eseguite.getitemdatetime(1,"data_a")
ls_tipo_periodo   = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_periodo")

choose case ls_tipo_periodo
		
	case "G"
		
		fl_num_periodo = DaysAfter( date(ldt_data_inizio), date(fdt_data_registrazione) ) + 1
		
		//fs_label_periodo = string(fdt_data_registrazione, "dd/mm/yyyy")
		fs_label_periodo = string(ldt_data_inizio, "dd/mm/yyyy")
		
	case "S" 		// la settimana inizia la domenica e finisce al sabato.
		
		ll_giorno = daynumber( date(ldt_data_inizio) )
		ld_data_rif = relativedate(date(ldt_data_inizio), 7 - ll_giorno) // trovo la data del primo sabato
		
		fl_num_periodo = 1
		
		do while ld_data_rif < date(fdt_data_registrazione)
			
			fl_num_periodo ++
			
			// trovo la data del prossimo fine settimana
			ld_data_rif = relativedate(ld_data_rif, 7 )		
			
		loop
		
		fs_label_periodo = "'Sett. " + string(wf_num_settimana(fdt_data_registrazione)) + "/" + string(year(date(fdt_data_registrazione)),"0000")
		
	case "M"
		ll_test = 0
		ll_mese = month( date(ldt_data_inizio) ) - 1 
		ll_anno = year( date(ldt_data_inizio) )
		
		fl_num_periodo = 0
		
		do 
			fl_num_periodo ++
			ll_test ++
			// calcolo il prossimo mese
			
			ll_mese ++
			if ll_mese > 12 then
				ll_mese = 1
				ll_anno ++
			end if
			
			ls_data = "01/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
			
			ld_data_rif = date(ls_data)
			
		//loop until ( ld_data_rif >= date(fdt_data_registrazione) and ll_test = 1 ) or ( ld_data_rif > date(fdt_data_registrazione) and ll_test > 1 ) or ( ld_data_rif = date(fdt_data_registrazione))
		loop until ( ld_data_rif > date(fdt_data_registrazione)) or (ld_data_rif > date( ldt_data_fine))
		
//		if ( ld_data_rif > date( ldt_data_fine)) then
			fl_num_periodo --
			ll_mese --
//		end if
		
		fs_label_periodo = "Mese " + string(ll_mese) + "/" + string(ll_anno,"0000")
		
		
end choose
end subroutine

public function integer wf_festivo (string fs_data, boolean fb_includi_sabato, string fs_flag_tipo_periodo);//torna 0 se il giorno della data in "fs_data" è tra lunedì e venerdì
//torna 0 se è sabato (a meno che il parametro "fb_includi_sabato" è posto a true, in tal caso torna 1)
//torna 2 se si tratta di una domenica

//se il parametro "fs_flag_tipo_periodo" non è "G" (cioè giorni), tale funzione torna sempre 0

integer li_day_number

if fs_flag_tipo_periodo <> "G" then return 0

if isnull(fs_data) or fs_data = "" then return 0

li_day_number = DayNumber ( date(fs_data) )

if li_day_number = 1 then return 2

if li_day_number = 7 and fb_includi_sabato then
	return 1
else
	return 0
end if

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify, ls_test

// non modificate questo script anche se segna errore perchè serve per testare se siamo sul DB di guerrato.

select flag_straordinario 
into :ls_test
from manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = 2004 and num_registrazione = 1;
if sqlca.sqlcode >= 0 then 			// errore vuol dire che è l'applicazione di Guerrato
	ib_guerrato = true
else
	ib_guerrato = false
end if

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_sel_manut_eseguite.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

/*
if ib_guerrato then		// guerrato
	is_des_livello_N = "(Non specificato)"
	is_des_livello_D = "Commessa"
	is_des_livello_R = "Ubicazione"
	is_des_livello_E = "Impianto"
	is_des_livello_C = "Categorie App"
	is_des_livello_T = "Tipologia Man"
	is_des_livello_A = "Apparecchiatura"
	//Donato 06/08/2008 introdotto raggruppamento per Operatore
	is_des_livello_O = "Operatore"
	// fine modifica -----------------------------------------------------------------
else 			// standard
	is_des_livello_N = "(Non specificato)"
	is_des_livello_D = "Divisione"
	is_des_livello_R = "Reparto"
	is_des_livello_E = "Area Aziendale"
	is_des_livello_C = "Categorie App"
	is_des_livello_T = "Tipologia Man"
	is_des_livello_A = "Attrezzatura"
	//Donato 06/08/2008 introdotto raggruppamento per Operatore
	is_des_livello_O = "Operatore"
	// fine modifica -----------------------------------------------------------------
end if
*/
is_des_livello_A = "Apparecchiatura"
is_des_livello_T = "Tipologia Man"
is_des_livello_O = "Operatore"
is_des_livello_N = "(Non specificato)"

dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 1, is_des_livello_A + "~tA")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 1, is_des_livello_T + "~tT")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 1, is_des_livello_N + "~tN")
//dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 1, is_des_livello_N + "~tN")
//dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 1, is_des_livello_N + "~tN")

/*
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 2, is_des_livello_D + "~tD")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 3, is_des_livello_R + "~tR")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 4, is_des_livello_E + "~tE")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 5, is_des_livello_C + "~tC")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 6, is_des_livello_T + "~tT")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 7, is_des_livello_A + "~tA")
//Donato 06/08/2008 introdotto raggruppamento per Operatore
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 8, is_des_livello_O + "~tO")
// fine modifica -----------------------------------------------------------------
*/
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 2, is_des_livello_O + "~tO")

if ib_guerrato then
	dw_sel_tipi_richieste.settransobject(sqlca)
	dw_sel_tipi_richieste.retrieve(s_cs_xx.cod_azienda)
else
	dw_sel_tipi_richieste.visible = false
end if

end event

on w_report_prog_gantt_excel.create
int iCurrent
call super::create
this.dw_sel_tipi_richieste=create dw_sel_tipi_richieste
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.st_1=create st_1
this.dw_sel_manut_eseguite=create dw_sel_manut_eseguite
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_tipi_richieste
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_sel_manut_eseguite
end on

on w_report_prog_gantt_excel.destroy
call super::destroy
destroy(this.dw_sel_tipi_richieste)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.st_1)
destroy(this.dw_sel_manut_eseguite)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_manut_eseguite,"cod_guasto",sqlca,"tab_guasti","cod_guasto","des_guasto",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " cognome ASC, nome ASC ")

f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_categoria",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")

f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_sel_manut_eseguite,"cod_risorsa_esterna",sqlca,&
                 "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_sel_manut_eseguite,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  


end event

type dw_sel_tipi_richieste from datawindow within w_report_prog_gantt_excel
integer x = 23
integer y = 1200
integer width = 1280
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "d_selezione_richieste_report"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;resetupdate()
end event

type cb_annulla from commandbutton within w_report_prog_gantt_excel
integer x = 1655
integer y = 1652
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string 	ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_da",ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_a",ldt_null)

dw_sel_manut_eseguite.setitem(1,"cod_operaio",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_guasto",ls_null)

dw_sel_manut_eseguite.setitem(1,"attrezzatura_da",ls_null)

dw_sel_manut_eseguite.setitem(1,"attrezzatura_a",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_tipo_manutenzione",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_categoria",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_reparto",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_area",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_divisione",ls_null)

dw_sel_manut_eseguite.setitem(1,"flag_ordinaria","S")

dw_sel_manut_eseguite.setitem(1,"flag_ric_car","N")

dw_sel_manut_eseguite.setitem(1,"flag_ric_tel","N")

dw_sel_manut_eseguite.setitem(1,"flag_giro_isp","N")

dw_sel_manut_eseguite.setitem(1,"flag_altre","N")

f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_report from commandbutton within w_report_prog_gantt_excel
integer x = 2043
integer y = 1652
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;long ll_task

if wf_carica_tabella_appoggio(ll_task) = -1 then
	rollback;
	return 
else
	commit;
end if

wf_generazione_foglio_excel(ll_task)

end event

type st_1 from statictext within w_report_prog_gantt_excel
integer x = 9
integer y = 1648
integer width = 1600
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_prog_gantt_excel
event ue_cod_attr ( )
event ue_carica_tipologie ( )
event ue_aree_aziendali ( )
integer x = 14
integer y = 8
integer width = 2377
integer height = 1632
integer taborder = 10
string dataobject = "d_sel_report_prog_gantt_excel"
boolean border = false
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"attrezzatura_da")) then
	setitem(getrow(),"attrezzatura_a",getitemstring(getrow(),"attrezzatura_da"))
end if
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"attrezzatura_da")

ls_attr_a = getitemstring(getrow(),"attrezzatura_a")

ls_reparto = getitemstring(getrow(),"cod_reparto")

ls_categoria = getitemstring(getrow(),"cod_categoria")

ls_area = getitemstring(getrow(),"cod_area")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ", " ordinamento ASC ")					  		
else
	f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  	
end if

triggerevent("ue_carica_tipologie")
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "attrezzatura_da"
		postevent("ue_cod_attr")
		postevent("ue_carica_tipologie")
		
	case "attrezzatura_a","cod_reparto","cod_categoria","cod_area"
		postevent("ue_carica_tipologie")
		
	case "cod_divisione"
		postevent("ue_aree_aziendali")
	/*
	case "flag_tipo_livello_1"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_2", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_2", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_3", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_2", 220)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if
		
	case "flag_tipo_livello_2"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_3", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 221)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if
		
	case "flag_tipo_livello_3"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 222)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if
		
	case "flag_tipo_livello_4"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 223)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if	
	*/
end choose
end event

event pcd_new;call super::pcd_new;//if ib_guerrato then
//	setitem( getrow(), "cod_azienda", s_cs_xx.cod_azienda)
//end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_manut_eseguite,"attrezzatura_da")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_manut_eseguite,"attrezzatura_a")
end choose
end event

