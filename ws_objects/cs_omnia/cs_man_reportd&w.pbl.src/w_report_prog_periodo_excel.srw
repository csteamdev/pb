﻿$PBExportHeader$w_report_prog_periodo_excel.srw
forward
global type w_report_prog_periodo_excel from w_cs_xx_principale
end type
type dw_sel_tipi_richieste from datawindow within w_report_prog_periodo_excel
end type
type cb_annulla from commandbutton within w_report_prog_periodo_excel
end type
type cb_report from commandbutton within w_report_prog_periodo_excel
end type
type st_1 from statictext within w_report_prog_periodo_excel
end type
type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_prog_periodo_excel
end type
type wstr_des_periodo from structure within w_report_prog_periodo_excel
end type
end forward

type wstr_des_periodo from structure
	string		des_periodo
end type

global type w_report_prog_periodo_excel from w_cs_xx_principale
integer width = 2597
integer height = 1856
string title = "Report Programmazione Periodo Excel"
dw_sel_tipi_richieste dw_sel_tipi_richieste
cb_annulla cb_annulla
cb_report cb_report
st_1 st_1
dw_sel_manut_eseguite dw_sel_manut_eseguite
end type
global w_report_prog_periodo_excel w_report_prog_periodo_excel

type variables
private wstr_des_periodo istr_des_periodo[]
private boolean ib_guerrato, ib_global_service = false
private string is_des_livello_N, is_des_livello_R, is_des_livello_E, is_des_livello_C, is_des_livello_T, is_des_livello_A, is_des_livello_D

//Donato 06/08/2008 introdotto raggruppamento per Operatore
private string is_des_livello_O
// fine modifica-----------------------------------------------------------------

end variables

forward prototypes
public subroutine wf_pulisci_stringa (ref string fs_stringa)
public subroutine wf_calcola_periodo (datetime fdt_data_registrazione, ref long fl_num_periodo, ref string fs_label_periodo)
public function integer wf_num_settimana (ref datetime fdt_data)
public function integer wf_carica_tabella_appoggio (ref long fl_prog_task)
public subroutine wf_generazione_foglio_excel (long fl_prog_task)
end prototypes

public subroutine wf_pulisci_stringa (ref string fs_stringa);long ll_pos

if len(fs_stringa) < 1 then return
if isnull(fs_stringa) then return

do while true
	ll_pos = pos(fs_stringa, "~r")
	if ll_pos > 0 then
		fs_stringa = replace(fs_stringa, ll_pos, 1, "")
	else
		exit
	end if
loop

do while true
	ll_pos = pos(fs_stringa, "~n")
	if ll_pos > 0 then
		fs_stringa = replace(fs_stringa, ll_pos, 1, "")
	else
		exit
	end if
loop

return
end subroutine

public subroutine wf_calcola_periodo (datetime fdt_data_registrazione, ref long fl_num_periodo, ref string fs_label_periodo);string ls_tipo_periodo, ls_data
long ll_giorno, ll_mese, ll_anno, ll_test 
date ld_data_rif
datetime ldt_data_inizio, ldt_data_fine


ldt_data_inizio = dw_sel_manut_eseguite.getitemdatetime(1,"data_da")
ldt_data_fine   = dw_sel_manut_eseguite.getitemdatetime(1,"data_a")
ls_tipo_periodo   = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_periodo")

choose case ls_tipo_periodo
		
	case "G"
		
		fl_num_periodo = DaysAfter( date(ldt_data_inizio), date(fdt_data_registrazione) ) + 1
		
		fs_label_periodo = string(fdt_data_registrazione, "dd/mm/yyyy")
		
	case "S" 		// la settimana inizia la domenica e finisce al sabato.
		
		ll_giorno = daynumber( date(ldt_data_inizio) )
		ld_data_rif = relativedate(date(ldt_data_inizio), 7 - ll_giorno) // trovo la data del primo sabato
		
		fl_num_periodo = 1
		
		do while ld_data_rif < date(fdt_data_registrazione)
			
			fl_num_periodo ++
			
			// trovo la data del prossimo fine settimana
			ld_data_rif = relativedate(ld_data_rif, 7 )		
			
		loop
		
		fs_label_periodo = "'Sett. " + string(wf_num_settimana(fdt_data_registrazione)) + "/" + string(year(date(fdt_data_registrazione)),"0000")
		
	case "M"
		ll_test = 0
		ll_mese = month( date(ldt_data_inizio) ) - 1 
		ll_anno = year( date(ldt_data_inizio) )
		
		fl_num_periodo = 0
		
		do 
			fl_num_periodo ++
			ll_test ++
			// calcolo il prossimo mese
			
			ll_mese ++
			if ll_mese > 12 then
				ll_mese = 1
				ll_anno ++
			end if
			
			ls_data = "01/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
			
			ld_data_rif = date(ls_data)
			
		//loop until ( ld_data_rif >= date(fdt_data_registrazione) and ll_test = 1 ) or ( ld_data_rif > date(fdt_data_registrazione) and ll_test > 1 ) or ( ld_data_rif = date(fdt_data_registrazione))
		loop until ( ld_data_rif > date(fdt_data_registrazione)) or (ld_data_rif > date( ldt_data_fine))
		
//		if ( ld_data_rif > date( ldt_data_fine)) then
			fl_num_periodo --
			ll_mese --
//		end if
		
		//non so perchè ma se il mese è 12 mette 0 nella variabile, quindi faccio questa cosa qui
		if ll_mese = 0 then
			fs_label_periodo = "Mese 12/" + string(ll_anno,"0000")
		else
			fs_label_periodo = "Mese " + string(ll_mese) + "/" + string(ll_anno,"0000")
		end if
		//fs_label_periodo = "Mese " + string(ll_mese) + "/" + string(ll_anno,"0000")
		
		
end choose
end subroutine

public function integer wf_num_settimana (ref datetime fdt_data);date ld_data,ld_prima_domenica,ld_primo_giorno,ld_prossima_domenica
long ll_settimana

ld_data = date(fdt_data)
ld_primo_giorno = date("01/01/" + string( year(ld_data) ) )

// cerco la data della prima domenica dell'anno
choose case daynumber(ld_primo_giorno)
	case 1
		ld_prima_domenica = ld_primo_giorno
	case 2
		ld_prima_domenica = relativedate(ld_primo_giorno,6)
	case 3
		ld_prima_domenica = relativedate(ld_primo_giorno,5)
	case 4
		ld_prima_domenica = relativedate(ld_primo_giorno,4)
	case 5
		ld_prima_domenica = relativedate(ld_primo_giorno,3)
	case 6
		ld_prima_domenica = relativedate(ld_primo_giorno,2)
	case 7
		ld_prima_domenica = relativedate(ld_primo_giorno,1)
end choose

if ld_prima_domenica > ld_data then
	ll_settimana = 52
	fdt_data = datetime(relativedate(date(fdt_data), -365),00:00:00)
else	
	// cerco la data della prossima domenica rispetto alla data odierna
	choose case daynumber(ld_data)
		case 1
			ld_prossima_domenica = relativedate(ld_data ,7)
		case 2
			ld_prossima_domenica = relativedate(ld_data ,6)
		case 3
			ld_prossima_domenica = relativedate(ld_data ,5)
		case 4
			ld_prossima_domenica = relativedate(ld_data ,4)
		case 5
			ld_prossima_domenica = relativedate(ld_data ,3)
		case 6
			ld_prossima_domenica = relativedate(ld_data ,2)
		case 7
			ld_prossima_domenica = relativedate(ld_data ,1)
	end choose
	
	ll_settimana = daysafter(ld_prima_domenica, ld_prossima_domenica) / 7
	
end if

return ll_settimana
end function

public function integer wf_carica_tabella_appoggio (ref long fl_prog_task);long 		ll_riga, ll_tempo_totale, ll_anno_registrazione, ll_num_registrazione, ll_num_periodo, &
         ll_prog_record,ll_num_eseguiti, ll_num_iniziati, ll_num_aperti, ll_nrows, ll_range, ll_y, ll_operaio_ore, ll_operaio_minuti, ll_risorsa_ore, &
			ll_risorsa_minuti

datetime ldt_da_data, ldt_a_data, ldt_data_registrazione, ldt_data_fine_att_inizio, ldt_data_fine_att_fine, &
         ldt_data_fine_attivita, ldt_data_inizio_intervento, ldt_data_progressiva

string 	ls_selezione, ls_operatori, ls_cod_guasto, ls_attr_da, ls_attr_a, ls_tipo_manut, ls_categoria, &
		 	ls_cod_reparto, ls_area, ls_ordinaria, ls_ric_car, ls_ric_tel, ls_giro_isp, ls_altre, ls_sql, &
		 	ls_flag, ls_flag_eseguito, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cognome, ls_nome, &
		 	ls_des_attrezzatura, ls_cod_reparto_attrezzatura, ls_des_tipo_manutenzione, ls_cat_attrezzature, &
			ls_cod_operatore, ls_cod_risorsa_esterna,ls_cod_area_aziendale, ls_flag_eseguito_sel, ls_flag_ordinario,&
			ls_flag_straordinario, ls_label_periodo,ls_cod_cat_attrezzatura, ls_test, ls_divisione, ls_cod_divisione, ls_cod_richieste, ls_cod_tipo_richiesta, &
			ls_cod_tipo_manutenzione_appo, ls_flag_ore, ls_flag_tipo_livello_1
			
//Donato 06/08/2008 introdotto per il raggruppamento per Operatore
string ls_cod_operaio_raggupp
// fine modifica ------------------------------------------------------------------------
			
wstr_des_periodo lstr_des_periodo[]


dw_sel_manut_eseguite.accepttext()

ls_selezione = ""

istr_des_periodo[] = lstr_des_periodo[]

// selezione da data a data registrazione
ldt_da_data = dw_sel_manut_eseguite.getitemdatetime(1,"data_da")
ldt_a_data = dw_sel_manut_eseguite.getitemdatetime(1,"data_a")

if not isnull(ldt_da_data) or not isnull(ldt_a_data) then
	ls_selezione += "Data registrazione:"
end if

if not isnull(ldt_da_data) then
	ls_selezione += " dal " + string(ldt_da_data,"dd/mm/yyyy") + " "
end if


if not isnull(ldt_a_data) then
	ls_selezione += " al " + string(ldt_a_data,"dd/mm/yyyy")
end if

if ldt_da_data > ldt_a_data then
	g_mb.error("OMNIA","Date di selezione registrazione non coerenti!")
	return -1
end if

ls_flag_tipo_livello_1 = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_livello_1")
if isnull(ls_flag_tipo_livello_1) or ls_flag_tipo_livello_1="" or ls_flag_tipo_livello_1="N" then
	g_mb.error("OMNIA","Specificare almeno un livello di raggruppamento!")
	return -1
end if

// selezione da data a data fine attività
ldt_data_fine_att_inizio = dw_sel_manut_eseguite.getitemdatetime(1,"data_fine_att_inizio")
ldt_data_fine_att_fine = dw_sel_manut_eseguite.getitemdatetime(1,"data_fine_att_fine")

if not isnull(ldt_data_fine_att_inizio) or not isnull(ldt_data_fine_att_fine) then
	if len(ls_selezione) > 0 then ls_selezione += "~r~n"
	ls_selezione += "Data fine attività:"
end if

if not isnull(ldt_data_fine_att_inizio) then
	ls_selezione += " dal " + string(ldt_data_fine_att_inizio,"dd/mm/yyyy") + " "
end if


if not isnull(ldt_data_fine_att_fine) then
	ls_selezione += " al " + string(ldt_data_fine_att_fine,"dd/mm/yyyy")
end if

if ldt_data_fine_att_inizio > ldt_data_fine_att_fine then
	g_mb.messagebox("OMNIA","Date di selezione fine attività non coerenti!",exclamation!)
	return -1
end if

ls_cod_operatore = dw_sel_manut_eseguite.getitemstring(1,"cod_operaio")

ls_cod_risorsa_esterna = dw_sel_manut_eseguite.getitemstring(1,"cod_risorsa_esterna")

ls_cod_guasto = dw_sel_manut_eseguite.getitemstring(1,"cod_guasto")

ls_attr_da = dw_sel_manut_eseguite.getitemstring(1,"attrezzatura_da")

ls_attr_a = dw_sel_manut_eseguite.getitemstring(1,"attrezzatura_a")

ls_tipo_manut = dw_sel_manut_eseguite.getitemstring(1,"cod_tipo_manutenzione")

ls_categoria = dw_sel_manut_eseguite.getitemstring(1,"cod_categoria")

ls_cod_reparto = dw_sel_manut_eseguite.getitemstring(1,"cod_reparto")

ls_area = dw_sel_manut_eseguite.getitemstring(1,"cod_area")

ls_divisione = dw_sel_manut_eseguite.getitemstring(1,"cod_divisione")

ls_ordinaria = dw_sel_manut_eseguite.getitemstring(1,"flag_ordinaria")

if ib_guerrato then
	
	ls_flag_ore = dw_sel_manut_eseguite.getitemstring( 1, "flag_ore")
	
	dw_sel_tipi_richieste.accepttext()
	ls_cod_richieste = ""
	for ll_y = 1 to dw_sel_tipi_richieste.rowcount()
		
		if dw_sel_tipi_richieste.getitemstring( ll_y, "flag_selezione") = "S" then
			
			ls_cod_tipo_richiesta = dw_sel_tipi_richieste.getitemstring( ll_y, "cod_tipo_richiesta")
			
			select cod_tipo_manutenzione
			into   :ls_cod_tipo_manutenzione_appo
			from   tab_tipi_richieste
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_richiesta = :ls_cod_tipo_richiesta;
					 
			if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_manutenzione_appo) and ls_cod_tipo_manutenzione_appo <> "" then			
				if ls_cod_richieste = "" then
					ls_cod_richieste = "'" + ls_cod_tipo_manutenzione_appo + "'"
				else
					ls_cod_richieste = ls_cod_richieste + ", '" + ls_cod_tipo_manutenzione_appo + "'"
				end if
			end if
		end if
		
	next
	
end if

ls_giro_isp = dw_sel_manut_eseguite.getitemstring(1,"flag_giro_isp")
ls_altre = dw_sel_manut_eseguite.getitemstring(1,"flag_altre")
ls_flag_eseguito_sel = dw_sel_manut_eseguite.getitemstring(1,"flag_eseguito")
ls_flag_ordinario = dw_sel_manut_eseguite.getitemstring(1,"flag_ordinario")
ls_flag_straordinario = dw_sel_manut_eseguite.getitemstring(1,"flag_straordinario")

declare manut_eseg dynamic cursor for sqlsa;

//Donato 06/08/2008 commentato per introdurre il raggruppamento per operatore
//vedi sotto la nuova sintassi
/*
ls_sql = &
"select manutenzioni.anno_registrazione, " + &
		" manutenzioni.num_registrazione, " + &
		" manutenzioni.data_registrazione, " + &
		" manutenzioni.flag_eseguito, " + &
		" manutenzioni.data_inizio_intervento, " + &
		" manutenzioni.cod_attrezzatura, " + &
		" manutenzioni.cod_tipo_manutenzione, " + &
		" anag_attrezzature.descrizione, " + &
		" anag_attrezzature.cod_reparto, " + &
		" anag_attrezzature.cod_area_aziendale, " + &
		" anag_attrezzature.cod_cat_attrezzature, " + &
		" tab_tipi_manutenzione.des_tipo_manutenzione, " + &
		" manutenzioni.operaio_ore, " + &
		" manutenzioni.operaio_minuti, " + &
		" manutenzioni.risorsa_ore, " + &
		" manutenzioni.risorsa_minuti " + &				
"from   manutenzioni, " + &
		" anag_attrezzature, " + &
		" tab_tipi_manutenzione "
*/
//NUOVA SINTASSI ---------------------------------------------------------
ls_sql = &
"select manutenzioni.anno_registrazione, " + &
		" manutenzioni.num_registrazione, " + &
		" manutenzioni.data_registrazione, " + &
		" manutenzioni.flag_eseguito, " + &
		" manutenzioni.data_inizio_intervento, " + &
		" manutenzioni.cod_attrezzatura, " + &
		" manutenzioni.cod_tipo_manutenzione, " + &
		" anag_attrezzature.descrizione, " + &
		" anag_attrezzature.cod_reparto, " + &
		" anag_attrezzature.cod_area_aziendale, " + &
		" anag_attrezzature.cod_cat_attrezzature, " + &
		" tab_tipi_manutenzione.des_tipo_manutenzione, " + &
		" manutenzioni.operaio_ore, " + &
		" manutenzioni.operaio_minuti, " + &
		" manutenzioni.risorsa_ore, " + &
		" manutenzioni.risorsa_minuti, "
		
		
if ib_guerrato then
	ls_sql += "manutenzioni_fasi_operai.cod_operaio "
else
	ls_sql += "manutenzioni.cod_operaio "
end if
		
		
ls_sql += "from   manutenzioni " + &
"join anag_attrezzature on anag_attrezzature.cod_azienda = manutenzioni.cod_azienda and " + &
					" anag_attrezzature.cod_attrezzatura = manutenzioni.cod_attrezzatura  " + &
"join tab_tipi_manutenzione on tab_tipi_manutenzione.cod_azienda = manutenzioni.cod_azienda and " + &
					" tab_tipi_manutenzione.cod_attrezzatura = manutenzioni.cod_attrezzatura and " + &
					" tab_tipi_manutenzione.cod_tipo_manutenzione = manutenzioni.cod_tipo_manutenzione "

if ib_guerrato then
	ls_sql += "left join manutenzioni_fasi_operai on manutenzioni_fasi_operai.cod_azienda = manutenzioni.cod_azienda and " + &
					" manutenzioni_fasi_operai.anno_registrazione = manutenzioni.anno_registrazione and " + &
					" manutenzioni_fasi_operai.num_registrazione = manutenzioni.num_registrazione " + &
				"left join manutenzioni_fasi_risorse on manutenzioni_fasi_risorse.cod_azienda = manutenzioni.cod_azienda and " + &
					" manutenzioni_fasi_risorse.anno_registrazione = manutenzioni.anno_registrazione and " + &
					" manutenzioni_fasi_risorse.num_registrazione = manutenzioni.num_registrazione "
end if
// fine modifica --------------------------------------------------------------------------------------------------------
	
ls_sql += "where manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_flag_eseguito_sel) and ls_flag_eseguito_sel <> "T" then
	ls_sql = ls_sql + " and manutenzioni.flag_eseguito = '" + ls_flag_eseguito_sel + "' "
end if

if not isnull(ls_flag_ordinario) and ls_flag_ordinario <> "T" then
	ls_sql = ls_sql + " and manutenzioni.flag_ordinario = '" + ls_flag_ordinario + "' "
end if

if not isnull(ls_flag_straordinario) and ls_flag_straordinario <> "T" then
	ls_sql = ls_sql + " and manutenzioni.flag_straordinario = '" + ls_flag_straordinario + "' "
end if

if not isnull(ls_cod_guasto)	then
	ls_sql = ls_sql + " and manutenzioni.cod_guasto = '" + ls_cod_guasto + "' "
end if

if not isnull(ldt_da_data) then
	ls_sql += " and manutenzioni.data_registrazione >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_a_data) then
	ls_sql += " and manutenzioni.data_registrazione <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine_att_inizio) then
	ls_sql += " and manutenzioni.data_fine_intervento >= '" + string(ldt_data_fine_att_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine_att_fine) then
	ls_sql += " and manutenzioni.data_fine_intervento <= '" + string(ldt_data_fine_att_fine,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if ib_guerrato then
	if not isnull(ls_cod_operatore) then
		ls_sql = ls_sql + " and manutenzioni_fasi_operai.cod_operaio = '" + ls_cod_operatore + "' "
	end if
	
	if not isnull(ls_cod_risorsa_esterna) then
		ls_sql = ls_sql + " and manutenzioni_fasi_risorse.cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "' "
	end if
else
	if not isnull(ls_cod_operatore) then
		ls_sql = ls_sql + " and manutenzioni.cod_operaio = '" + ls_cod_operatore + "' "
	end if
	
	if not isnull(ls_cod_risorsa_esterna) then
		ls_sql = ls_sql + " and manutenzioni.cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "' "
	end if
end if

if not isnull(ls_attr_da) then
	ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_tipo_manut) then
	ls_sql = ls_sql + " and manutenzioni.cod_tipo_manutenzione = '" + ls_tipo_manut + "' "
end if

if not isnull(ls_cod_richieste) and ls_cod_richieste <> "" then
	ls_sql = ls_sql + " and manutenzioni.cod_tipo_manutenzione in ( " + ls_cod_richieste + " ) "
end if

if not isnull(ls_categoria) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_cod_reparto) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
end if

if not isnull(ls_area) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale = '" + ls_area + "' "
end if

if not isnull(ls_divisione) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "
end if

ls_flag = ""

if ls_ordinaria = "S" then
	if ls_flag <> "" then
		ls_flag += " or "
	end if
	ls_flag += " flag_ordinario = 'S' and flag_richieste_cartacee = 'N' and flag_richieste_telefoniche = 'N' and flag_risc_giro_isp = 'N' "
end if


//if ls_ric_car = "S" then
//	if ls_flag <> "" then
//		ls_flag += " or "
//	end if
//ls_flag += " flag_richieste_cartacee = 'S' "
//end if
//
//if ls_ric_tel = "S" then
//	if ls_flag <> "" then
//		ls_flag += " or "
//	end if
//	ls_flag += " flag_richieste_telefoniche = 'S' "
//end if
	
if ls_giro_isp = "S" then
	if ls_flag <> "" then
		ls_flag += " or "
	end if
	ls_flag += " flag_risc_giro_isp = 'S' "
end if

if ls_altre = "S" then
	if ls_flag <> "" then
		ls_flag += " or "
	end if
	ls_flag += " flag_ordinario = 'N' and flag_richieste_cartacee = 'N' and flag_richieste_telefoniche = 'N' and flag_risc_giro_isp = 'N' "
end if

if ib_guerrato then

	if ls_flag = "" then
		if isnull(ls_cod_richieste) or ls_cod_richieste = "" then
			ls_flag = " flag_ordinario = 'XXX'"
		end if
	end if
	
	if ls_flag <> "" and not isnull(ls_flag) then
		ls_sql += " and ( " + ls_flag + " ) "
	end if

end if

ls_sql = ls_sql + " order by data_registrazione, manutenzioni.cod_attrezzatura "

st_1.text = "Preparazione della Query in corso attendere ...."

select max(prog_task)
into   :fl_prog_task
from   tab_manut_report_excel
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(fl_prog_task) or fl_prog_task = 0 then
	fl_prog_task = 1
else
	fl_prog_task ++
end if

ll_prog_record = 0
setnull(ldt_data_progressiva)

PREPARE SQLSA FROM :ls_sql ;

open dynamic manut_eseg;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

do while true
	//Donato 06/08/2008 commentato e inserito il nuovo codice (vedi sotto)
	
	/*
	fetch manut_eseg
	into  :ll_anno_registrazione,
	      :ll_num_registrazione,
	      :ldt_data_registrazione,
			:ls_flag_eseguito,
			:ldt_data_inizio_intervento,
			:ls_cod_attrezzatura,
			:ls_cod_tipo_manutenzione,
			:ls_des_attrezzatura,
			:ls_cod_reparto_attrezzatura,
			:ls_cod_area_aziendale,
			:ls_cod_cat_attrezzatura,
			:ls_des_tipo_manutenzione,
			:ll_operaio_ore,
			:ll_operaio_minuti,
			:ll_risorsa_ore,
			:ll_risorsa_minuti;
	*/
	//NUOVO CODICE per la gestione del raggruppamento per operatore
	fetch manut_eseg
	into  :ll_anno_registrazione,
	      :ll_num_registrazione,
	      :ldt_data_registrazione,
			:ls_flag_eseguito,
			:ldt_data_inizio_intervento,
			:ls_cod_attrezzatura,
			:ls_cod_tipo_manutenzione,
			:ls_des_attrezzatura,
			:ls_cod_reparto_attrezzatura,
			:ls_cod_area_aziendale,
			:ls_cod_cat_attrezzatura,
			:ls_des_tipo_manutenzione,
			:ll_operaio_ore,
			:ll_operaio_minuti,
			:ll_risorsa_ore,
			:ll_risorsa_minuti,
			:ls_cod_operaio_raggupp
			;
	// fine modifica -------------------------------------------------------------------------------

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit		
	end if
	
	st_1.text = string(ldt_data_registrazione,"dd/mm/yyyy") + " - " + ls_cod_attrezzatura
	
	if isnull(ll_operaio_ore) then ll_operaio_ore = 0
	if isnull(ll_operaio_minuti) then ll_operaio_minuti = 0
	if isnull(ll_risorsa_ore) then ll_risorsa_ore = 0
	if isnull(ll_risorsa_minuti) then ll_risorsa_minuti = 0
	
	ll_operaio_ore = ((ll_operaio_ore * 60) + ll_operaio_minuti) + ((ll_risorsa_ore * 60) + ll_risorsa_minuti)
	
	ll_tempo_totale = 0
	ls_operatori = ""
	ls_cat_attrezzature = ""
	
	if isnull(ldt_data_progressiva) then ldt_data_progressiva = ldt_da_data
	
	do while ldt_data_progressiva < ldt_data_registrazione
	
		wf_calcola_periodo(ldt_data_progressiva, ref ll_num_periodo, ref ls_label_periodo)
		
		istr_des_periodo[ll_num_periodo].des_periodo = ls_label_periodo
		
		ldt_data_progressiva = datetime(relativedate(date(ldt_data_progressiva), 1),00:00:00)
		
	loop
	
	wf_calcola_periodo(ldt_data_registrazione, ref ll_num_periodo, ref ls_label_periodo)
	istr_des_periodo[ll_num_periodo].des_periodo = ls_label_periodo
	
	if ll_num_periodo > 245 then			// non posso gestire più di 255 colonne con excel
		g_mb.messagebox("OMNIA","A causa dei limiti di MsExcel non è possibile gestire più di 245 periodi~nL'elaborazione verrà interrotta.~nRestringere le date di elaborazione o selezionare tipo di periodi diversi.",StopSign!)
		return -1
	end if
	
	// *** trovo divisione
	
	select cod_divisione 
	into   :ls_cod_divisione
	from   tab_aree_aziendali
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_aziendale = ( select cod_area_aziendale
			                        from   anag_attrezzature
											where  cod_azienda = :s_cs_xx.cod_azienda and
											       cod_attrezzatura = :ls_cod_attrezzatura);
	
	// ***
	
	ll_num_eseguiti = 0
	ll_num_iniziati = 0
	ll_num_aperti   = 0
	
	if ls_flag_eseguito = "S" then
		ll_num_eseguiti = 1
	else
		if not isnull(ldt_data_inizio_intervento) and ldt_data_inizio_intervento > datetime(date(01/01/1900),00:00:00) then
			ll_num_iniziati = 1
		else
			ll_num_aperti   = 1
		end if
	end if
	
	if ib_guerrato then
		//Donato 28/07/2008
		//se l'operatore non è stato specificato in manutenzioni provare a cercare nella programmi_manutenzione
		if isnull(ls_cod_operaio_raggupp) or ls_cod_operaio_raggupp = "" then
			SELECT
				 programmi_manutenzione.cod_operaio
			INTO
				:ls_cod_operaio_raggupp
			FROM manutenzioni
			JOIN programmi_manutenzione ON manutenzioni.cod_azienda = programmi_manutenzione.cod_azienda
				AND manutenzioni.anno_reg_programma = programmi_manutenzione.anno_registrazione
				AND manutenzioni.num_reg_programma = programmi_manutenzione.num_registrazione
			WHERE manutenzioni.cod_azienda = :s_cs_xx.cod_azienda
				AND manutenzioni.anno_registrazione=:ll_anno_registrazione 
				AND manutenzioni.num_registrazione = :ll_num_registrazione
			;
		end if
		
		if not isnull(ls_flag_ore) and ls_flag_ore = "S" then
			ll_num_eseguiti = 0
			ll_num_iniziati = 0
			ll_num_aperti   = 0			
			if ls_flag_eseguito = "S" then
				ll_num_eseguiti = ll_operaio_ore
			else
				if not isnull(ldt_data_inizio_intervento) and ldt_data_inizio_intervento > datetime(date(01/01/1900),00:00:00) then
					ll_num_iniziati = ll_operaio_ore
				else
					//Donato 28/07/2008
					//se non è stato codificato l'operaio in manutenzioni
					//recuperare l'operaio, le ore e i minuti dalla programmi_manutenzione
					long ll_anno_registrazione_prog, ll_num_registrazione_prog
					long ll_operaio_ore_prog, ll_operaio_minuti_prog, ll_risorsa_ore_prog, ll_risorsa_minuti_prog
					
					//ll_num_aperti   = ll_operaio_ore
					SELECT
						 operaio_ore_previste
						,operaio_minuti_previsti
						,risorsa_ore_previste
						,risorsa_minuti_previsti
						,programmi_manutenzione.cod_operaio
					INTO
						 :ll_operaio_ore_prog
						,:ll_operaio_minuti_prog
						,:ll_risorsa_ore_prog
						,:ll_risorsa_minuti_prog
						,:ls_cod_operaio_raggupp
					FROM manutenzioni
					JOIN programmi_manutenzione ON manutenzioni.cod_azienda = programmi_manutenzione.cod_azienda
						AND manutenzioni.anno_reg_programma = programmi_manutenzione.anno_registrazione
						AND manutenzioni.num_reg_programma = programmi_manutenzione.num_registrazione
					WHERE manutenzioni.cod_azienda = :s_cs_xx.cod_azienda
						AND manutenzioni.anno_registrazione=:ll_anno_registrazione 
						AND manutenzioni.num_registrazione = :ll_num_registrazione
					;
					if isnull(ll_operaio_ore_prog) then ll_operaio_ore_prog=0
					if isnull(ll_operaio_minuti_prog) then ll_operaio_minuti_prog=0
					if isnull(ll_risorsa_ore_prog) then ll_risorsa_ore_prog=0
					if isnull(ll_risorsa_minuti_prog) then ll_risorsa_minuti_prog=0
					if ls_cod_operaio_raggupp='' then setnull(ls_cod_operaio_raggupp)
					
					ll_operaio_ore_prog = ((ll_operaio_ore_prog * 60) + ll_operaio_minuti_prog) + ((ll_risorsa_ore_prog * 60) + ll_risorsa_minuti_prog)
					ll_num_aperti   = ll_operaio_ore_prog
					//fine modifica
				end if
			end if			
			
		end if
	end if
	
	
	ll_prog_record ++
	
	//Donato 06/08/2008 commentato e inserito il nuovo codice (vedi sotto)
	
	/*
	INSERT INTO tab_manut_report_excel  
			( cod_azienda,   
			  prog_task,   
			  prog_record,   
			  gruppo_1,   
			  gruppo_2,   
			  gruppo_3,   
			  gruppo_4,   
			  gruppo_5,   
			  gruppo_6,   
			  gruppo_7,
			  num_periodo,   
			  num_eseguiti,   
			  num_iniziati,   
			  num_aperti )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :fl_prog_task,   
			  :ll_prog_record,   
			  :ls_cod_area_aziendale,   
			  :ls_cod_reparto_attrezzatura,   
			  :ls_cod_cat_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_cod_attrezzatura,
			  :ls_cod_divisione,
			  null,   
			  :ll_num_periodo,   
			  :ll_num_eseguiti,   
			  :ll_num_iniziati,   
			  :ll_num_aperti )  ;
	*/
	//nuovo codice per la gestione del raggruppamento per operaio
	if isnull(ls_cod_operaio_raggupp) then ls_cod_operaio_raggupp = "1234567"	//codice sicuramente inesistente in anag_operai
	
	INSERT INTO tab_manut_report_excel  
			( cod_azienda,   
			  prog_task,   
			  prog_record,   
			  gruppo_1,   
			  gruppo_2,   
			  gruppo_3,   
			  gruppo_4,   
			  gruppo_5,   
			  gruppo_6,   
			  gruppo_7,
			  num_periodo,   
			  num_eseguiti,   
			  num_iniziati,   
			  num_aperti )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :fl_prog_task,   
			  :ll_prog_record,   
			  :ls_cod_area_aziendale,   
			  :ls_cod_reparto_attrezzatura,   
			  :ls_cod_cat_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_cod_attrezzatura,
			  :ls_cod_divisione,
			  :ls_cod_operaio_raggupp,   
			  :ll_num_periodo,   
			  :ll_num_eseguiti,   
			  :ll_num_iniziati,   
			  :ll_num_aperti )  ;
	// fine modifica
	
	if sqlca.sqlcode = 0 then
	else
		g_mb.messagebox("OMNIA","Errore durante l'elaborazione nella tabella tab_manut_report_excel.~n"+sqlca.sqlerrtext,StopSign!)
		return -1
	end if
	

//	f_des_operatori_tempi_manut(ll_anno_registrazione, &
//	                            ll_num_registrazione, &
//										 ref ll_tempo_totale, &
//										 ref ls_operatori, &
//										 ref ls_cat_attrezzature)

	
loop

close manut_eseg;

return 0
end function

public subroutine wf_generazione_foglio_excel (long fl_prog_task);string ls_gruppo_old[7], ls_gruppo[7], ls_flag_tipo_livello[5], ls_sql, ls_sql_select, ls_sql_orderby, ls_sql_groupby, &
       ls_stringvar, ls_descrizione, ls_des_date, ls_rag_soc_azienda, ls_tipo_periodo, ls_flag_ore, ls_cod_attrezzatura
integer li_i, li_y
long ll_num_periodo, ll_num_eseguiti, ll_num_iniziati, ll_num_aperti,ll_errore, ll_riga_excel, ll_colonna_excel,&
	  ll_periodo_riferimento, ll_cont_eseguiti, ll_cont_iniziati, ll_cont_aperti, ll_num_livelli, ll_nrows, ll_range, &
	  ll_cont, ll_riga_periodo
decimal ld_decimalvar
datetime ldt_data_riferimento, ldt_data_inizio, ldt_data_fine
OLEObject myoleobject
declare cu_manut_excel DYNAMIC CURSOR FOR SQLSA ; 
	
ldt_data_inizio = dw_sel_manut_eseguite.getitemdatetime(dw_sel_manut_eseguite.getrow(),"data_da")
ldt_data_fine   = dw_sel_manut_eseguite.getitemdatetime(dw_sel_manut_eseguite.getrow(),"data_a")
ls_tipo_periodo   = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_periodo")

if ib_guerrato then
	ls_flag_ore = dw_sel_manut_eseguite.getitemstring( 1, "flag_ore")
end if

select rag_soc_1
into   :ls_rag_soc_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ldt_data_inizio) then
	ls_des_date = "Dalla prima data  "
else
	ls_des_date = "Dal " + string(ldt_data_inizio,"dd/mm/yyyy")
end if	

if isnull(ldt_data_fine) then
	ls_des_date += "  All'ultima data  "
else
	ls_des_date += "  al " + string(ldt_data_fine,"dd/mm/yyyy")
end if	

ldt_data_riferimento = datetime(today(), 00:00:00)
wf_calcola_periodo(ldt_data_riferimento, ref ll_periodo_riferimento, ref ls_descrizione)

ls_sql_select = ""
ls_sql_orderby = ""
ls_sql_groupby = ""
ll_num_livelli = 0

for li_i = 1 to 5
	
	ls_flag_tipo_livello[li_i] = dw_sel_manut_eseguite.getitemstring(1,"flag_tipo_livello_"+string(li_i))
	
	choose case ls_flag_tipo_livello[li_i]
			
		case "N"
			continue
			
		case "R"		// Reparto - Ubicazione
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_1 "
			ls_sql_orderby += " gruppo_1 ASC "
			ls_sql_groupby += " gruppo_1 "
			ll_num_livelli ++
			
		case "E"		// Area Aziendale - Impianto
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_2 "
			ls_sql_orderby += " gruppo_2 ASC "
			ls_sql_groupby += " gruppo_2 "
			ll_num_livelli ++
		
		case "C"		// Categ Attrezzatura
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_3 "
			ls_sql_orderby += " gruppo_3 ASC "
			ls_sql_groupby += " gruppo_3 "
			ll_num_livelli ++
		
		case "T"		// Tipo Manutenzione
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_4 "
			ls_sql_orderby += " gruppo_4 ASC "
			ls_sql_groupby += " gruppo_4 "
			ll_num_livelli ++
		
		case "A"		// Attrezzatura
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_5 "
			ls_sql_orderby += " gruppo_5 ASC "
			ls_sql_groupby += " gruppo_5 "
			ll_num_livelli ++
			
		case "D"		// Divisione - Commessa
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_6 "
			ls_sql_orderby += " gruppo_6 ASC "
			ls_sql_groupby += " gruppo_6 "
			ll_num_livelli ++			
			
		//Donato 06/08/2008 inserito per il raggruppamento per Operatore
		case "O"		// Operatore
			if len(ls_sql_select) > 0 then
				ls_sql_select += ", "
				ls_sql_orderby += ", "
				ls_sql_groupby += ", "
			end if
				
			ls_sql_select += " gruppo_7 "
			ls_sql_orderby += " gruppo_7 ASC "
			ls_sql_groupby += " gruppo_7 "
			ll_num_livelli ++
		// fine modifica -------------------------------------------------------------------------
		
	end choose
next

if len(ls_sql_select) > 0 then 
	ls_sql_select += ", "
	ls_sql_orderby += ", "
	ls_sql_groupby += ", "
end if

ls_sql = " select " + ls_sql_select + " num_periodo, sum(num_eseguiti), sum(num_iniziati), sum(num_aperti) " + &
         " from tab_manut_report_excel  " + &
			" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  prog_task = " + string(fl_prog_task) + &
			" group by " + ls_sql_groupby + " num_periodo " + &
			" order by " + ls_sql_orderby + " num_periodo ASC "


PREPARE SQLSA FROM :ls_sql;
DESCRIBE SQLSA into SQLDA;
			
open DYNAMIC cu_manut_excel using descriptor SQLDA;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in apertura cursore cu_manut_excel.~r~n"+sqlca.sqlerrtext)
	return
end if

// creazione oggetto EXCEL 
myoleobject = CREATE OLEObject

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	close cu_manut_excel;
	g_mb.messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return
end if

// caratteristiche del foglio in generale
myoleobject.Visible = True
myoleobject.Workbooks.Add
myoleobject.sheets("foglio1").PageSetup.PrintGridlines = True
myoleobject.sheets("foglio1").PageSetup.orientation = 2
myoleobject.sheets("foglio1").PageSetup.leftmargin = 40
myoleobject.sheets("foglio1").PageSetup.rightmargin = 40
myoleobject.sheets("foglio1").PageSetup.topmargin = 40
myoleobject.sheets("foglio1").PageSetup.bottommargin = 40
// imposto testata

myoleobject.sheets("foglio1").range("A1:M1").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A1:M1").merge(true)
myoleobject.sheets("foglio1").range("A1:M1").font.size = "14"
myoleobject.sheets("foglio1").range("A1:M1").Font.Bold = True
myoleobject.sheets("foglio1").range("A1:M1").Font.Italic = True
myoleobject.sheets("foglio1").range("A1:M1").font.ColorIndex = "1"
myoleobject.sheets("foglio1").range("A1:M1").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").cells(1,1).value = ls_rag_soc_azienda
myoleobject.sheets("foglio1").range("A1:M1").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A1:M1").Interior.ColorIndex = "3"
myoleobject.sheets("foglio1").range("A2:M2").merge(true)
myoleobject.sheets("foglio1").range("A2:M2").Interior.ColorIndex = "3"
myoleobject.sheets("foglio1").range("A2:M2").borders.linestyle = "1"

myoleobject.sheets("foglio1").range("A3:M3").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A3:M3").merge(true)
myoleobject.sheets("foglio1").range("A3:M3").font.size = "20"
myoleobject.sheets("foglio1").range("A3:M3").Font.Bold = True
myoleobject.sheets("foglio1").range("A3:M3").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").range("A3:M3").value = "- PROGRAMMAZIONE PERIODO -"
myoleobject.sheets("foglio1").range("A3:M3").Interior.ColorIndex = "36"
myoleobject.sheets("foglio1").range("A3:M3").borders.linestyle = "1"

myoleobject.sheets("foglio1").range("A4:M4").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A4:M4").merge(true)
myoleobject.sheets("foglio1").range("A4:M4").font.size = "12"
myoleobject.sheets("foglio1").range("A4:M4").Font.Bold = True
myoleobject.sheets("foglio1").range("A4:M4").HorizontalAlignment = "3"
myoleobject.sheets("foglio1").range("A4:M4").value = ls_des_date
myoleobject.sheets("foglio1").range("A4:M4").Interior.ColorIndex = "36"
myoleobject.sheets("foglio1").range("A4:M4").borders.linestyle = "1"

// TOTOLO Legenda
myoleobject.sheets("foglio1").range("A5:M5").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A5:M5").font.size = "12"
myoleobject.sheets("foglio1").range("A5:M5").Font.Bold = True
myoleobject.sheets("foglio1").range("A5:M5").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").rows(5).rowheight = "15"
myoleobject.sheets("foglio1").cells(5,1).value = "LEGENDA DEI COLORI DELLE CELLE:"

// imposto la legenda del colore VERDE
myoleobject.sheets("foglio1").range("A6:M6").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A6:M6").font.size = "8"
myoleobject.sheets("foglio1").range("A6:M6").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A6:A6").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A6:A6").Interior.ColorIndex = "4"		// verde
myoleobject.sheets("foglio1").cells(6,1).value = "n1"
myoleobject.sheets("foglio1").cells(6,2).value = "Tutte le attività regolamente iniziate e terminate. Il valore dentro la cella (n1) indica il numero delle attività svolte."

// imposto la legenda del colore ROSSO
myoleobject.sheets("foglio1").range("A7:M7").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A7:M7").font.size = "8"
myoleobject.sheets("foglio1").range("A7:M7").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A7:A7").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A7:A7").Interior.ColorIndex = "3"		// rosso
myoleobject.sheets("foglio1").cells(7,1).value = "n1/n2"
myoleobject.sheets("foglio1").cells(7,2).value = "Scadute e alcune o tutte non eseguite. Il testo dentro la cella indica quante attività sono state svolte (n1) in rapporto a quelle previste (n2)."

// imposto la legenda del colore ARANCIO
myoleobject.sheets("foglio1").range("A8:M8").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A8:M8").font.size = "8"
myoleobject.sheets("foglio1").range("A8:M8").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A8:A8").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A8:A8").Interior.ColorIndex = "45"		// arancio
myoleobject.sheets("foglio1").cells(8,1).value = "n1/n2"
myoleobject.sheets("foglio1").cells(8,2).value = "Scadute e almeno una attività iniziata . Il testo dentro la cella indica quante attività sono state svolte (n1) in rapporto a quelle previste (n2)."

// imposto la legenda del colore BLU
myoleobject.sheets("foglio1").range("A9:M9").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A9:M9").font.size = "8"
myoleobject.sheets("foglio1").range("A9:M9").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A9:A9").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A9:A9").Interior.ColorIndex = "17"		// BLU
myoleobject.sheets("foglio1").cells(9,1).value = "n1"
myoleobject.sheets("foglio1").cells(9,2).value = "Non scadute, non eseguite e neanche una attività iniziata.Il valore dentro la cella (n1) indica il numero delle attività non ancora iniziate."

// imposto la legenda del colore GIALLO
myoleobject.sheets("foglio1").range("A10:M10").font.name = "ARIAL"
myoleobject.sheets("foglio1").range("A10:M10").font.size = "8"
myoleobject.sheets("foglio1").range("A10:M10").HorizontalAlignment = "1"
myoleobject.sheets("foglio1").range("A10:A10").borders.linestyle = "1"
myoleobject.sheets("foglio1").range("A10:A10").Interior.ColorIndex = "6"		// GIALLO
myoleobject.sheets("foglio1").cells(10,1).value = "n1/n2"
myoleobject.sheets("foglio1").cells(10,2).value = "Alcune non eseguite, ma non scadute insieme ad alcune attività già iniziate o eseguite. Il testo dentro la cella indica quante attività sono state svolte (n1) in rapporto a quelle previste (n2)."

long ll_x, ll_y

OPEN(w_report_prog_periodo_excel_child,THIS)

ll_x = ( this.width - w_report_prog_periodo_excel_child.width ) / 2
ll_y = ( this.height - w_report_prog_periodo_excel_child.height ) / 2

w_report_prog_periodo_excel_child.x = ll_x
w_report_prog_periodo_excel_child.y = ll_y


ls_gruppo_old[1] = ""
ls_gruppo_old[2] = ""
ls_gruppo_old[3] = ""
ls_gruppo_old[4] = ""
ls_gruppo_old[5] = ""
ls_gruppo_old[6] = ""
ls_gruppo_old[7] = ""

li_i = 0

ll_riga_excel = 11
ll_riga_periodo = ll_riga_excel
ll_colonna_excel = 10
ll_cont_eseguiti = 0
ll_cont_iniziati = 0
ll_cont_aperti	  = 0
ll_cont = 0

for li_i = 1 to upperbound(istr_des_periodo)
	// modifica Claudia 01/12/06 per sistemazione anomalia 
//////	if ls_tipo_periodo='M' and li_i = upperbound(istr_des_periodo) - 1 then exit
			
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).font.size = "8"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).orientation = "90"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).value = istr_des_periodo[li_i].des_periodo
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).HorizontalAlignment = "3"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + li_i).borders.linestyle = "1"
	myoleobject.sheets("foglio1").columns(ll_colonna_excel + li_i).columnwidth = "4"
next


do while true
	fetch cu_manut_excel USING DESCRIPTOR SQLDA ;

	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		close cu_manut_excel;
		g_mb.messagebox("OMNIA","Errore in apertura cursore cu_manut_excel.~r~n"+sqlca.sqlerrtext)
		return
	end if
	
	ll_cont ++

	st_1.text = string(ll_cont)
//	ll_riga_excel ++
	
	for li_i = 1 to upperbound(SQLDA.OutParmType[])
//		tipo = SQLDA.OutParmType[li_i]
		CHOOSE CASE SQLDA.OutParmType[li_i]
				
			CASE TypeString!
				ls_stringvar = GetDynamicString(SQLDA, li_i)
				
			CASE TypeInteger!,TypeLong!,TypeDecimal!
					ld_decimalvar = GetDynamicNumber(SQLDA, li_i)

		END CHOOSE
		

		if li_i <= ll_num_livelli then
			ls_gruppo[li_i] = ls_stringvar
		else
			choose case li_i
				case ll_num_livelli + 1		// num_periodo
					ll_num_periodo = ld_decimalvar
				case ll_num_livelli + 2		// num_eseguiti,
					ll_num_eseguiti = ld_decimalvar
				case ll_num_livelli + 3		// num_iniziati
					ll_num_iniziati = ld_decimalvar
				case ll_num_livelli + 4		// num_aperti
					ll_num_aperti = ld_decimalvar
			end choose
			// fine modifica -----------------------------------------------------------------------
		end if
			

		if li_i <= 7 then
			
			if ls_gruppo_old[li_i] <> ls_gruppo[li_i] then
				
				// nuova intestazione di gruppo
				ls_gruppo_old[li_i] = ls_gruppo[li_i]
				
				for li_y = li_i + 1 to 7		// elimino i gruppi sottostanti per evitare omonimie di codici.
					ls_gruppo_old[li_y] = ""
				next
				
				ls_descrizione = ""
				choose case ls_flag_tipo_livello[li_i]
					
					case "N"
						
					case "D"		// Divisioni - Commesse
						select des_divisione
						into :ls_descrizione
						from  anag_divisioni
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_divisione = :ls_gruppo[li_i];						
						
					case "R"		// Reparto - Impianti
						select des_area
						into :ls_descrizione
						from  tab_aree_aziendali
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_area_aziendale = :ls_gruppo[li_i];
					
					case "E"		// Area Aziendale - Ubicazioni
						select des_reparto
						into :ls_descrizione
						from anag_reparti
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_reparto = :ls_gruppo[li_i];
					
					case "C"		// Categ Attrezzatura
						select des_cat_attrezzature
						into :ls_descrizione
						from tab_cat_attrezzature
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_cat_attrezzature = :ls_gruppo[li_i];
								
					case "T"		// Tipo Manutenzione
						
						if li_i > 1 then
							if ls_flag_tipo_livello[li_i - 1] = "A" and ls_cod_attrezzatura<>"" then
								select des_tipo_manutenzione
								into :ls_descrizione
								from tab_tipi_manutenzione
								where cod_azienda = :s_cs_xx.cod_azienda and
										cod_tipo_manutenzione = :ls_gruppo[li_i] and
										cod_attrezzatura=:ls_cod_attrezzatura;
							else
								select des_tipo_manutenzione
								into :ls_descrizione
								from tab_tipi_manutenzione
								where cod_azienda = :s_cs_xx.cod_azienda and
										cod_tipo_manutenzione = :ls_gruppo[li_i];
							end if
						else
							select des_tipo_manutenzione
							into :ls_descrizione
							from tab_tipi_manutenzione
							where cod_azienda = :s_cs_xx.cod_azienda and
									cod_tipo_manutenzione = :ls_gruppo[li_i];
						end if
						
//						select des_tipo_manutenzione
//						into :ls_descrizione
//						from tab_tipi_manutenzione
//						where cod_azienda = :s_cs_xx.cod_azienda and
//						      cod_tipo_manutenzione = :ls_gruppo[li_i];
						wf_pulisci_stringa(ls_descrizione)
						
					case "A"		// Attrezzatura
						select descrizione
						into :ls_descrizione
						from anag_attrezzature
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_attrezzatura = :ls_gruppo[li_i];
																
						//memorizza il codice attreezzatura utilizzato
						ls_cod_attrezzatura = ls_gruppo[li_i]
						
								
					//Donato 06/08/2008 introdotto raggruppamento per Operatore
					case "O"		// Operatore
						string ls_nome
						
						select 
								cognome, 
								nome
						into 
								:ls_descrizione, 
								:ls_nome
						from anag_operai
						where cod_azienda = :s_cs_xx.cod_azienda and
						      cod_operaio = :ls_gruppo[li_i];
								
						
						if SQLCA.sqlcode = 100 then
							ls_gruppo[li_i] = ""
							ls_descrizione = "operatore non specificato"
						else
							if isnull(ls_descrizione) then ls_descrizione = ""
							if isnull(ls_nome) then ls_nome = ""
							ls_descrizione += " " + ls_nome
						end if
						
					// fine modifica
						
				end choose
				
				ll_riga_excel ++
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).font.size = "8"
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).Font.Bold = True
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).VerticalAlignment = "3"
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).HorizontalAlignment = "2"
				myoleobject.sheets("foglio1").cells(ll_riga_excel, li_i).value = ls_gruppo[li_i] + " " + ls_descrizione
				myoleobject.sheets("foglio1").rows(ll_riga_excel).rowheight = "15"
		
			end if
					
		end if	
		
	next
	
	// imposto la cella e la colonna del periodo prescelto
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).font.size = "8"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).orientation = "90"
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).value = istr_des_periodo[ll_num_periodo].des_periodo
	myoleobject.sheets("foglio1").cells(ll_riga_periodo, ll_colonna_excel + ll_num_periodo).HorizontalAlignment = "3"
	myoleobject.sheets("foglio1").columns(ll_colonna_excel + ll_num_periodo).columnwidth = "4"

	// imposto le proprietà della riga in cui scrivero' i dati
	myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).font.size = "8"
	myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).VerticalAlignment   = "3"
	myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).HorizontalAlignment = "3"

	if (ll_num_aperti + ll_num_iniziati) = 0 then
		if not isnull(ls_flag_ore) and ls_flag_ore = "S" then
			myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = string(round(ll_num_eseguiti/60,2))
		else
			myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = string(ll_num_eseguiti)
		end if
		myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = "4"		// verde
		
	else
		// aperte o iniziate e scadute
		
		if ll_num_periodo < ll_periodo_riferimento then			// la data è già passata
			if ll_num_iniziati  = 0 then		// gli interventi non sono iniziati e quindi sono ancora aperti
				if not isnull(ls_flag_ore) and ls_flag_ore = "S" then
					myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = "'" + string(round(ll_num_eseguiti/60,2)) + "/" + string(round((ll_num_eseguiti+ll_num_iniziati+ll_num_aperti)/60,2))
				else
					myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = "'" + string(ll_num_eseguiti) + "/" + string(ll_num_eseguiti+ll_num_iniziati+ll_num_aperti)
				end if			
				
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = "3"			// rosso
			else
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = "'" + string(ll_num_eseguiti) + "/" + string(ll_num_eseguiti+ll_num_iniziati+ll_num_aperti)
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = "45"//string(rgb(0,128,128))			// arancio
			end if
		else
		// aperte o iniziate Ma non scadute
			if (ll_num_iniziati + ll_num_eseguiti) = 0 then		// gli interventi non sono iniziati e quindi sono ancora aperti
				if not isnull(ls_flag_ore) and ls_flag_ore = "S" then
					myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = "'" + string(round(ll_num_aperti/60,2))
				else
					myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = "'" + string(ll_num_aperti)
				end if
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = "17" //string(rgb(0,0,255))			// blu
			else
				if not isnull(ls_flag_ore) and ls_flag_ore = "S" then
					myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = "'" + string(round(ll_num_eseguiti/60,2)) + "/" + string(round((ll_num_eseguiti+ll_num_iniziati+ll_num_aperti)/60,2))
				else
					myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).value = "'" + string(ll_num_eseguiti) + "/" + string(ll_num_eseguiti+ll_num_iniziati+ll_num_aperti)
				end if
				myoleobject.sheets("foglio1").cells(ll_riga_excel, ll_colonna_excel + ll_num_periodo).interior.ColorIndex = "6" //string(rgb(255,255,0))			// giallo

			end if
		
		end if
	end if
	
	
loop

destroy myoleobject
close cu_manut_excel;

delete from tab_manut_report_excel
where cod_azienda = :s_cs_xx.cod_azienda and
      prog_task = :fl_prog_task;
		

commit;

CLOSE(w_report_prog_periodo_excel_child)

this.setfocus()

g_mb.messagebox("OMNIA", "Generazione Foglio Excel~nEseguita !")

end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify, ls_test

// non modificate questo script anche se segna errore perchè serve per testare se siamo sul DB di guerrato.
select flag_straordinario 
into :ls_test
from manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = 2004 and num_registrazione = 1;
if sqlca.sqlcode >= 0 then 			// errore vuol dire che è l'applicazione di Guerrato
	ib_guerrato = true
else
	ib_guerrato = false
end if

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_sel_manut_eseguite.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

if ib_guerrato then		// guerrato
	is_des_livello_N = "(Non specificato)"
	is_des_livello_D = "Commessa"
	is_des_livello_R = "Ubicazione"
	is_des_livello_E = "Impianto"
	is_des_livello_C = "Categorie App"
	is_des_livello_T = "Tipologia Man"
	is_des_livello_A = "Apparecchiatura"
	//Donato 06/08/2008 introdotto raggruppamento per Operatore
	is_des_livello_O = "Operatore"
	// fine modifica -----------------------------------------------------------------
else 			// standard
	is_des_livello_N = "(Non specificato)"
	is_des_livello_D = "Divisione"
	is_des_livello_R = "Reparto"
	is_des_livello_E = "Area Aziendale"
	is_des_livello_C = "Categorie App"
	is_des_livello_T = "Tipologia Man"
	is_des_livello_A = "Attrezzatura"
	//Donato 06/08/2008 introdotto raggruppamento per Operatore
	is_des_livello_O = "Operatore"
	// fine modifica -----------------------------------------------------------------
end if

dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 1, is_des_livello_N + "~tN")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 1, is_des_livello_N + "~tN")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 1, is_des_livello_N + "~tN")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 1, is_des_livello_N + "~tN")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 1, is_des_livello_N + "~tN")

dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 2, is_des_livello_D + "~tD")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 3, is_des_livello_R + "~tR")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 4, is_des_livello_E + "~tE")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 5, is_des_livello_C + "~tC")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 6, is_des_livello_T + "~tT")
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 7, is_des_livello_A + "~tA")
//Donato 06/08/2008 introdotto raggruppamento per Operatore
dw_sel_manut_eseguite.setvalue("flag_tipo_livello_1", 8, is_des_livello_O + "~tO")
// fine modifica -----------------------------------------------------------------

if ib_guerrato then
	dw_sel_tipi_richieste.settransobject(sqlca)
	dw_sel_tipi_richieste.retrieve(s_cs_xx.cod_azienda)
else
	dw_sel_tipi_richieste.visible = false
end if

end event

on w_report_prog_periodo_excel.create
int iCurrent
call super::create
this.dw_sel_tipi_richieste=create dw_sel_tipi_richieste
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.st_1=create st_1
this.dw_sel_manut_eseguite=create dw_sel_manut_eseguite
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_tipi_richieste
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_sel_manut_eseguite
end on

on w_report_prog_periodo_excel.destroy
call super::destroy
destroy(this.dw_sel_tipi_richieste)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.st_1)
destroy(this.dw_sel_manut_eseguite)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_manut_eseguite,"cod_guasto",sqlca,"tab_guasti","cod_guasto","des_guasto",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " cognome ASC, nome ASC ")

f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_categoria",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")

f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_sel_manut_eseguite,"cod_risorsa_esterna",sqlca,&
                 "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_sel_manut_eseguite,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  


end event

type dw_sel_tipi_richieste from datawindow within w_report_prog_periodo_excel
integer x = 37
integer y = 1252
integer width = 1280
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "d_selezione_richieste_report"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;resetupdate()
end event

type cb_annulla from commandbutton within w_report_prog_periodo_excel
integer x = 1655
integer y = 1652
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string 	ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_da",ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_a",ldt_null)

dw_sel_manut_eseguite.setitem(1,"cod_operaio",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_guasto",ls_null)

dw_sel_manut_eseguite.setitem(1,"attrezzatura_da",ls_null)

dw_sel_manut_eseguite.setitem(1,"attrezzatura_a",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_tipo_manutenzione",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_categoria",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_reparto",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_area",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_divisione",ls_null)

dw_sel_manut_eseguite.setitem(1,"flag_ordinaria","S")

dw_sel_manut_eseguite.setitem(1,"flag_ric_car","N")

dw_sel_manut_eseguite.setitem(1,"flag_ric_tel","N")

dw_sel_manut_eseguite.setitem(1,"flag_giro_isp","N")

dw_sel_manut_eseguite.setitem(1,"flag_altre","N")

f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_report from commandbutton within w_report_prog_periodo_excel
integer x = 2043
integer y = 1652
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;long ll_task

if wf_carica_tabella_appoggio(ll_task) = -1 then
	rollback;
	return 
else
	commit;
end if

wf_generazione_foglio_excel(ll_task)

end event

type st_1 from statictext within w_report_prog_periodo_excel
integer x = 9
integer y = 1648
integer width = 1600
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_prog_periodo_excel
event ue_cod_attr ( )
event ue_carica_tipologie ( )
event ue_aree_aziendali ( )
integer x = 14
integer y = 8
integer width = 2377
integer height = 1632
integer taborder = 10
string dataobject = "d_sel_report_prog_periodo_excel"
boolean border = false
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"attrezzatura_da")) then
	setitem(getrow(),"attrezzatura_a",getitemstring(getrow(),"attrezzatura_da"))
end if
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a


ls_attr_da = getitemstring(getrow(),"attrezzatura_da")

ls_attr_a = getitemstring(getrow(),"attrezzatura_a")

ls_reparto = getitemstring(getrow(),"cod_reparto")

ls_categoria = getitemstring(getrow(),"cod_categoria")

ls_area = getitemstring(getrow(),"cod_area")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ", " ordinamento ASC ")					  		
else
	f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  	
end if

triggerevent("ue_carica_tipologie")
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "attrezzatura_da"
		postevent("ue_cod_attr")
		postevent("ue_carica_tipologie")
		
	case "attrezzatura_a","cod_reparto","cod_categoria","cod_area"
		postevent("ue_carica_tipologie")
		
	case "cod_divisione"
		postevent("ue_aree_aziendali")
		
	case "flag_tipo_livello_1"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_2", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_2", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_3", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_2", 220)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_2", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if
		
	case "flag_tipo_livello_2"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_3", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_3", 221)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_3", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if
		
	case "flag_tipo_livello_3"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 0)
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_4", "N")
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_4", 222)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_4", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if
		
	case "flag_tipo_livello_4"
		if i_coltext = "N" then
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 0)
			dw_sel_manut_eseguite.setitem(dw_sel_manut_eseguite.getrow(), "flag_tipo_livello_5", "N")
		else			
			dw_sel_manut_eseguite.settaborder("flag_tipo_livello_5", 223)
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 2, is_des_livello_D + "~tD")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 3, is_des_livello_R + "~tR")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 4, is_des_livello_E + "~tE")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 5, is_des_livello_C + "~tC")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 6, is_des_livello_T + "~tT")
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 7, is_des_livello_A + "~tA")
			//Donato 06/08/2008 introdotto raggruppamento per operatore
			dw_sel_manut_eseguite.setvalue("flag_tipo_livello_5", 8, is_des_livello_O + "~tO")
			// fine modifica ----------------------------------------------------------------
		end if		
end choose
end event

event pcd_new;call super::pcd_new;//if ib_guerrato then
//	setitem( getrow(), "cod_azienda", s_cs_xx.cod_azienda)
//end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_manut_eseguite,"attrezzatura_da")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_manut_eseguite,"attrezzatura_a")
end choose
end event

