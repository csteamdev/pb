﻿$PBExportHeader$w_report_registro_uso_attrez.srw
forward
global type w_report_registro_uso_attrez from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_report_registro_uso_attrez
end type
type dw_report from uo_cs_xx_dw within w_report_registro_uso_attrez
end type
type dw_folder from u_folder within w_report_registro_uso_attrez
end type
type dw_selezione from uo_cs_xx_dw within w_report_registro_uso_attrez
end type
type cb_report from commandbutton within w_report_registro_uso_attrez
end type
type st_1 from statictext within w_report_registro_uso_attrez
end type
end forward

global type w_report_registro_uso_attrez from w_cs_xx_principale
integer width = 5239
integer height = 2492
string title = "Report Km/Ore - Consumi Mezzi"
cb_reset cb_reset
dw_report dw_report
dw_folder dw_folder
dw_selezione dw_selezione
cb_report cb_report
st_1 st_1
end type
global w_report_registro_uso_attrez w_report_registro_uso_attrez

type variables
string					is_path_logo, is_cod_tipo_manutenzione, is_parametro_reg, is_flag_scad_mezzi
datastore			ids_mezzi_destinatari
end variables

forward prototypes
public function integer wf_report_data_a_data ()
public function integer wf_report_mensile ()
public function integer wf_mese (integer fi_mese, ref string fs_mese)
public function integer wf_check_manutenzione (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, long fl_mese, long fl_anno, ref datetime fdt_data_registrazione)
public function integer wf_carica_sql (ref string fs_filtro, ref string fs_sql, ref string fs_sql_distinct, boolean fb_mese_anno)
public function integer wf_dati_mensili (string fs_sql, string fs_sql_periodo, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, datetime fdt_inizio, datetime fdt_fine)
public function integer wf_carica_sql_mensile (ref string fs_filtro, ref string fs_sql, ref string fs_sql_distinct)
public function integer wf_report_mensile_2 ()
public function integer wf_dati_mensili_2 (string fs_sql, string fs_sql_periodo, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, datetime fdt_inizio, datetime fdt_fine)
public function integer wf_check_manutenzione_2 (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione)
public subroutine wf_add_filtro_per_tipo_man (ref string fs_where)
end prototypes

public function integer wf_report_data_a_data ();datetime ldt_inizio, ldt_fine, ldt_data_blocco, ldt_data_prelievo, ldt_data_restituzione
string ls_sql, ls_temp, ls_cod_attrezzatura, ls_cod_operaio, ls_cod_reparto, ls_flag_tipo_tempo
string ls_cod_tipo_attrez, ls_cod_cat_attrezzatura, ls_cod_area_aziendale, ls_filtro, ls_sql_distinct
datastore lds_data
long ll_tot, ll_index, ll_prog_uso_attrezz, ll_anno_commessa, ll_num_commessa, ll_riga
decimal ld_contatore, ld_consumo

setpointer(Hourglass!)

dw_selezione.accepttext()

dw_report.reset()
dw_report.dataobject="d_report_registro_uso_attrezz"
dw_report.reset()

dw_report.modify("intestazione.filename='" + is_path_logo + "'")

st_1.text = "Elaborazione Report in corso ...."

ls_filtro = ""
if wf_carica_sql(ls_filtro, ls_sql, ls_sql_distinct, false) < 0 then
	st_1.text = "Elaborazione terminata con errori!"
	setpointer(Arrow!)
	rollback;
	return -1
end if

//ci attacco il periodo interressato
ldt_inizio = dw_selezione.getitemdatetime(1, "data_da")
ldt_fine = dw_selezione.getitemdatetime(1, "data_a")

if not isnull(ldt_inizio) and year(date(ldt_inizio))>1950 then
	ldt_inizio = datetime(date(ldt_inizio), 00:00:00)
	ls_sql += " and a.data_prelievo>='"+string(ldt_inizio, s_cs_xx.db_funzioni.formato_data)+"' "
	
	ls_filtro = " - Dal "+string(ldt_inizio, "dd-mm-yyyy") + ls_filtro
end if
if not isnull(ldt_fine) and year(date(ldt_fine))>1950 then
	ldt_fine = datetime(date(ldt_fine), 00:00:00)
	ls_sql += " and a.data_prelievo<='"+string(ldt_fine, s_cs_xx.db_funzioni.formato_data)+"' "
	
	ls_filtro = " - Fino la "+string(ldt_fine, "dd-mm-yyyy") + ls_filtro
end if

//crea il datastore
if not f_crea_datastore(lds_data, ls_sql) then
	//dw_report.setredraw(true)
	st_1.text = "Elaborazione terminata con errori!"
	setpointer(Arrow!)
	rollback;
	return -1
end if

ll_tot = lds_data.retrieve()

if ls_filtro<>"" then dw_report.object.t_filtro.text = ls_filtro

for ll_index=1 to ll_tot
	ls_cod_attrezzatura = lds_data.getitemstring(ll_index, 1)
	
	st_1.text = "Elaborazione in corso: "+ls_cod_attrezzatura + "  ---- " + string(ll_index) + " di " + string(ll_tot)
	
	ll_prog_uso_attrezz		= lds_data.getitemnumber(ll_index, 2)
	ldt_data_prelievo			= lds_data.getitemdatetime(ll_index, 3)
	ldt_data_restituzione		= lds_data.getitemdatetime(ll_index, 4)
	ls_cod_operaio				= lds_data.getitemstring(ll_index, 5)
	ll_anno_commessa		= lds_data.getitemnumber(ll_index, 6)
	ll_num_commessa		= lds_data.getitemnumber(ll_index, 7)
	ls_flag_tipo_tempo		= lds_data.getitemstring(ll_index, 8)
	ld_contatore					= lds_data.getitemdecimal(ll_index, 9)
	ld_consumo					= lds_data.getitemdecimal(ll_index, 10)
	ls_cod_reparto				= lds_data.getitemstring(ll_index, 11)
	ls_cod_tipo_attrez			= lds_data.getitemstring(ll_index, 12)
	ls_cod_cat_attrezzatura	= lds_data.getitemstring(ll_index, 13)
	ls_cod_area_aziendale	= lds_data.getitemstring(ll_index, 14)

	ll_riga = dw_report.insertrow(0)
	
	dw_report.setitem(ll_riga, "cod_attrezzatura", ls_cod_attrezzatura)
	dw_report.setitem(ll_riga, "prog_uso_attrezzatura", ll_prog_uso_attrezz)
	dw_report.setitem(ll_riga, "data_prelievo", ldt_data_prelievo)
	dw_report.setitem(ll_riga, "data_restituzione", ldt_data_restituzione)
	dw_report.setitem(ll_riga, "cod_operaio", ls_cod_operaio)
	dw_report.setitem(ll_riga, "anno_commessa", ll_anno_commessa)
	dw_report.setitem(ll_riga, "num_commessa", ll_num_commessa)
	dw_report.setitem(ll_riga, "flag_tipo_tempo", ls_flag_tipo_tempo)
	dw_report.setitem(ll_riga, "contatore", ld_contatore)
	dw_report.setitem(ll_riga, "consumo", ld_consumo)
	dw_report.setitem(ll_riga, "cod_reparto", ls_cod_reparto)
	dw_report.setitem(ll_riga, "cod_tipo_attrez", ls_cod_tipo_attrez)
	dw_report.setitem(ll_riga, "cod_cat_attrezzature", ls_cod_cat_attrezzatura)
	dw_report.setitem(ll_riga, "cod_area_aziendale", ls_cod_area_aziendale)
next

dw_report.sort()
dw_report.groupcalc()

dw_report.object.datawindow.print.preview = "Yes"

st_1.text = "Elaborazione Completata!"
destroy lds_data;

setpointer(Arrow!)

dw_folder.fu_selecttab(2)

return 1
end function

public function integer wf_report_mensile ();datetime ldt_inizio, ldt_fine, ldt_mese_anno, ldt_data_blocco, ldt_data_prelievo, ldt_data_restituzione
string ls_sql, ls_temp, ls_cod_attrezzatura, ls_cod_operaio, ls_cod_reparto, ls_flag_tipo_tempo
string ls_cod_tipo_attrez, ls_cod_cat_attrezzatura, ls_cod_area_aziendale, ls_filtro, ls_mese1, ls_mese2
datastore lds_data_attrezz
long ll_tot, ll_index, ll_prog_uso_attrezz, ll_anno_commessa, ll_num_commessa, ll_riga, ll_tot2, ll_index2
decimal ld_contatore, ld_consumo
string ls_sql_distinct, ls_flag_scad_mezzi, ls_sql_periodo
long ll_mese, ll_anno

setpointer(Hourglass!)

dw_selezione.accepttext()
dw_report.reset()
dw_report.dataobject="d_report_registro_uso_attrezz_mens"
dw_report.reset()
ids_mezzi_destinatari.reset()

dw_report.modify("intestazione.filename='" + is_path_logo + "'")

st_1.text = "Elaborazione Report in corso ...."

//ci attacco il periodo interessato
ldt_mese_anno = dw_selezione.getitemdatetime(1, "mese_anno")
if isnull(ldt_mese_anno) or year(date(ldt_mese_anno))<1950 then
	setpointer(Arrow!)
	g_mb.messagebox("OMNIA","Impostare il mese/anno di arrivo",Exclamation!)	
	rollback;
	return -1
end if

//-------------------------------------------------------
//valuta quale cod_tipo_manutenzione ricercare (parametro CMR, che sarebbe _MENS o MENSRG)

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

if luo_mansionario.uof_get_privilege(luo_mansionario.scadenz_mezzi) then
	ls_flag_scad_mezzi = "S"
else
	ls_flag_scad_mezzi = "N"
end if

destroy luo_mansionario;
//select flag_nuove_scadenze
//into :ls_flag_scad_mezzi
//from mansionari
//where cod_azienda=:s_cs_xx.cod_azienda and
//	cod_utente=:s_cs_xx.cod_utente;
//
//if sqlca.sqlcode < 0 then
//	setpointer(Arrow!)
//	st_1.text = "Elaborazione terminata con errori!"
//	g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: " + sqlca.sqlerrtext,stopsign!)
//	return -1
//		
//elseif sqlca.sqlcode = 100 then
//	setpointer(Arrow!)
//	st_1.text = "Elaborazione terminata con errori!"
//	g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: nessun mansionario associato",stopsign!)
//	return -1
//end if
if isnull(ls_flag_scad_mezzi) then ls_flag_scad_mezzi = "N"

if ls_flag_scad_mezzi="S" then
	is_cod_tipo_manutenzione = "MENSRG"
else
	select stringa
	into	:is_cod_tipo_manutenzione
	from	parametri
	where	cod_parametro = 'CMR';
	
	if isnull(is_cod_tipo_manutenzione) or is_cod_tipo_manutenzione="" then
		g_mb.messagebox("OMNIA","Impostare il parametro multi-aziendale CMR",Exclamation!)
		st_1.text = "Elaborazione terminata con errori!"
		setpointer(Arrow!)
		rollback;
		return -1
	end if
end if
//-------------------------------------------------------


ls_filtro = ""
if wf_carica_sql_mensile(ls_filtro, ls_sql, ls_sql_distinct) < 0 then
	st_1.text = "Elaborazione terminata con errori!"
	setpointer(Arrow!)
	rollback;
	return -1
end if

choose case month(date(ldt_mese_anno))
	case 11,4,6,9  //30
		ldt_fine = datetime(date(year(date(ldt_mese_anno)), month(date(ldt_mese_anno)), 30) ,00:00:00)
		
	case 2 //28
		ldt_fine = datetime(date(year(date(ldt_mese_anno)), month(date(ldt_mese_anno)), 28) ,00:00:00)
		
	case else //31
		ldt_fine = datetime(date(year(date(ldt_mese_anno)), month(date(ldt_mese_anno)), 31) ,00:00:00)
		
end choose

choose case month(date(ldt_mese_anno))
	case 12
		//il periodo annuale andrà a gennaio dello stesso anno (con giorno 1)
		ldt_inizio = datetime(date(year(date(ldt_mese_anno)), 1, 1) ,00:00:00)
		
	case else
		//il periodo annuale andrà al mese + 1 dell'anno precedente (con giorno 1)
		ldt_inizio = datetime(date(year(date(ldt_mese_anno)) - 1, month(date(ldt_mese_anno)) + 1, 1) ,00:00:00)
		
end choose

//valorizzo nell'sql il periodo

ls_sql_periodo = " and a.data_prelievo>='"+string(ldt_inizio, s_cs_xx.db_funzioni.formato_data)+"' "
ls_sql_periodo += " and a.data_prelievo<='"+string(ldt_fine, s_cs_xx.db_funzioni.formato_data)+"' "
//ls_sql += " and a.data_prelievo>='"+string(ldt_inizio, s_cs_xx.db_funzioni.formato_data)+"' "
//ls_sql += " and a.data_prelievo<='"+string(ldt_fine, s_cs_xx.db_funzioni.formato_data)+"' "


//scrivo nella stringa filtro
wf_mese(month(date(ldt_inizio)), ls_mese1)
ls_mese1 = "Periodo da  "+ls_mese1 + " " + string(year(date(ldt_inizio)))

wf_mese(month(date(ldt_fine)), ls_mese2)
ls_mese2 = " A  "+ls_mese2 + " " + string(year(date(ldt_fine)))

ls_filtro = ls_mese1 + ls_mese2 + ls_filtro

//crea il datastore attrezzature distinct
if not f_crea_datastore(lds_data_attrezz, ls_sql_distinct) then
	//dw_report.setredraw(true)
	st_1.text = "Elaborazione terminata con errori! (2)"
	setpointer(Arrow!)
	rollback;
	return -1
end if

//preparo le etichette di intestazione
ll_mese = month(date(ldt_inizio))
ll_anno = year(date(ldt_inizio))
for ll_index2 = 1 to 12	
	wf_mese(ll_mese, ls_mese1)
	
	//prendo solo i primi 3 caratteri
	ls_mese1 = left(ls_mese1, 3)
	
	//accodo l'anno
	ls_mese1 += " " + string(ll_anno)
	
	dw_report.modify("mese"+string(ll_index2)+"_t.text='"+ls_mese1+"'")
	
	//se sfori vai ad inizio anno successivo
	ll_mese +=1
	if ll_mese = 13 then
		ll_mese = 1
		ll_anno += 1
	end if
next
//---------------------------------------------

ll_tot2 = lds_data_attrezz.retrieve()

if ls_filtro<>"" then dw_report.object.t_filtro.text = ls_filtro

lds_data_attrezz.setsort("#1 A") //per cod attrezzatura

for ll_index2=1 to ll_tot2
	ls_cod_attrezzatura = lds_data_attrezz.getitemstring(ll_index2, 1)
	
	st_1.text = "Elaborazione attrezzatura "+ls_cod_attrezzatura + " -------- "+string(ll_index2) + " di " +string(ll_tot2)
	
	if wf_dati_mensili(ls_sql, ls_sql_periodo, ls_cod_attrezzatura, is_cod_tipo_manutenzione, ldt_inizio, ldt_fine) < 0 then
		st_1.text = "Elaborazione terminata con errori! (m)"
		setpointer(Arrow!)
		rollback;
		return -1
	end if
	
next

dw_report.sort()
dw_report.groupcalc()

dw_report.object.datawindow.print.preview = "Yes"

st_1.text = "Elaborazione Completata!"

setpointer(Arrow!)

dw_folder.fu_selecttab(2)

destroy lds_data_attrezz;

return 1
end function

public function integer wf_mese (integer fi_mese, ref string fs_mese);
choose case fi_mese
	case 1
		fs_mese = "Gennaio"
	case 2
		fs_mese = "Febbraio"
	case 3
		fs_mese = "Marzo"
	case 4
		fs_mese = "Aprile"
	case 5
		fs_mese = "Maggio"
	case 6
		fs_mese = "Giugno"
	case 7
		fs_mese = "Luglio"
	case 8
		fs_mese = "Agosto"
	case 9
		fs_mese = "Settembre"
	case 10
		fs_mese = "Ottobre"
	case 11
		fs_mese = "Novembre"
	case 12
		fs_mese = "Dicembre"
end choose

return 1
end function

public function integer wf_check_manutenzione (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, long fl_mese, long fl_anno, ref datetime fdt_data_registrazione);long ll_gg, ll_daysafter, ll_count

//torna 1 	se esistono manutenzioni aperte con data inferiore alla data di sistema
//				e più vecchi di X giorni
//torna 2		se esistono manutenzioni aperte con data inferiore alla data di sistema
//				entro la tolleranza di X giorni
//torna 3		se non esistono manutenzioni (aperte o chiuse) in qualsivoglia periodo
//torna 0		tutti gli altri casi

ll_gg = 5

select count(*)
into :ll_count
from manutenzioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:fs_cod_attrezzatura and
			cod_tipo_manutenzione=:fs_cod_tipo_manutenzione;
			
if sqlca.sqlcode<0 then
	g_mb.messagebox("OMNIA","Errore in lettura dati manutenzioni : attrezzature "+fs_cod_attrezzatura+char(13)+&
									sqlca.sqlerrtext,Exclamation!)
	return -1
end if

if isnull(ll_count) or ll_count=0 or sqlca.sqlcode=100 then
	return 3
end if

select data_registrazione
into :fdt_data_registrazione
from manutenzioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:fs_cod_attrezzatura and
			cod_tipo_manutenzione=:fs_cod_tipo_manutenzione and
			flag_eseguito='N' and 
			month(data_registrazione)=:fl_mese and
			year(data_registrazione)=:fl_anno;
			
if sqlca.sqlcode<0 then
	g_mb.messagebox("OMNIA","Errore in lettura dati manutenzioni aperte: attrezzature "+fs_cod_attrezzatura+char(13)+&
									sqlca.sqlerrtext,Exclamation!)
	return -1
end if

if isnull(fdt_data_registrazione) or year(date(fdt_data_registrazione))<1950 or sqlca.sqlcode=100 then
	//non esistono manutenzioni aperte OK
	return 0
else
	//esistono manutenzioni aperte			
	ll_daysafter = daysafter(date(fdt_data_registrazione), today())
	
	choose case ll_daysafter 
		case is < 0
			//scadenza non ancora superata
			//bianco
			return 0
			
		case else
			//verifica la tolleranza
			if ll_daysafter>ll_gg then
				//rosso
				return 1
			else
				//giallo
				return 2
			end if		
	end choose
end if
end function

public function integer wf_carica_sql (ref string fs_filtro, ref string fs_sql, ref string fs_sql_distinct, boolean fb_mese_anno);datetime ldt_data_blocco
string ls_temp, ls_where

fs_sql = "select distinct "+&
				"a.cod_attrezzatura,"+&
				"a.prog_uso_attrezzatura,"+&
				"a.data_prelievo,"+&
				"a.data_restituzione,"+&
				"a.cod_operaio,"+&
				"a.anno_commessa,"+&
				"a.num_commessa,"+&
				"a.flag_tipo_tempo,"+&
				"a.contatore,"+&
				"a.consumo,"+&
				"b.cod_reparto,"+&
				"b.cod_tipo_attrez,"+&
				"b.cod_cat_attrezzature,"+&
				"b.cod_area_aziendale,"+&
				"a.costo_consumo "+&
			"from registro_uso_attrezzature  a "+&
			"join anag_attrezzature  b on 	b.cod_azienda=a.cod_azienda and "+&
														   "b.cod_attrezzatura=a.cod_attrezzatura "+&
			"join tab_tipi_manutenzione  tm on 	tm.cod_azienda=b.cod_azienda and "+&
														   "tm.cod_attrezzatura=b.cod_attrezzatura "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' "

if fb_mese_anno then
	fs_sql_distinct = 	"select distinct b.cod_attrezzatura "+&
							"from anag_attrezzature  b "+&
							"join tab_tipi_manutenzione  tm on 	tm.cod_azienda=b.cod_azienda and "+&
														   "tm.cod_attrezzatura=b.cod_attrezzatura "+&
							"where b.cod_azienda='"+s_cs_xx.cod_azienda+"' "
							
	//carica solo le attrezzature primarie
	fs_sql_distinct += " and b.flag_primario='S' "
else
	fs_sql_distinct = 	"select distinct a.cod_attrezzatura "+&
							"from registro_uso_attrezzature  a "+&
							"join anag_attrezzature  b on 	b.cod_azienda=a.cod_azienda and "+&
														   "b.cod_attrezzatura=a.cod_attrezzatura "+&
							"join tab_tipi_manutenzione  tm on 	tm.cod_azienda=b.cod_azienda and "+&
														   "tm.cod_attrezzatura=b.cod_attrezzatura "+&
							"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' "
end if

fs_filtro = ""

ls_temp = dw_selezione.getitemstring(1, "cod_utente")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_utente='"+ls_temp+"' "
	
	fs_filtro += " - Resp.Mezzo "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_attrezzatura")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_attrezzatura>='"+ls_temp+"' "
	
	fs_filtro += " - Da Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_attrezzatura_a")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_attrezzatura<='"+ls_temp+"' "	
	
	fs_filtro += " - Ad Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_tipo_attrez")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_tipo_attrez='"+ls_temp+"' "	
	
	fs_filtro += " - Tipologia Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_reparto")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_reparto='"+ls_temp+"' "
	
	fs_filtro += " - Reparto Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_cat_attrezzature")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_cat_attrezzature='"+ls_temp+"' "
	
	fs_filtro += " - Categoria Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_area_aziendale")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_area_aziendale='"+ls_temp+"' "
	
	fs_filtro += " - Area Aziendale "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "flag_blocco")
if ls_temp="N" then
	//visualizza solo quelle non bloccate
	ls_where += " and b.flag_blocco='N' "
	
	fs_filtro += " - Solo Attrezzatura NON bloccate"
else
	//visualizza anche se bloccate
	ldt_data_blocco = dw_selezione.getitemdatetime(1, "data_blocco")
	
	if not isnull(ldt_data_blocco) and year(date(ldt_data_blocco))>1950 then
		//visualizza anche quelle bloccate, purchè non lo siano in una data inferiore a quella specificata
		ls_where += " and (b.flag_blocco='N' or (b.flag_blocco='S' and data_blocco>'"+string(ldt_data_blocco, s_cs_xx.db_funzioni.formato_data)+"')) "
		
		fs_filtro += " - Attrezzatura NON bloccate ed in aggiunta quelle BLOCCATE dopo il "+string(ldt_data_blocco, "dd-MM-yyyy")
	else
		//non è stata specificata alcuna data di blocco
		//quindi includi anche quelle bloccate
		//quindi non mettere niente nella where che riguardi il flag blocco
		fs_filtro += " - Attrezzatura BLOCCATE e NON bloccate"
	end if
end if

wf_add_filtro_per_tipo_man(ls_where)

fs_sql += ls_where
fs_sql_distinct += ls_where

return 1
end function

public function integer wf_dati_mensili (string fs_sql, string fs_sql_periodo, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, datetime fdt_inizio, datetime fdt_fine);datastore lds_data
string ls_sql, ls_cod_responsabile, ls_cod_controllore, ls_sql_hold, ls_sql1
long ll_index, ll_tot, ll_i, ll_mese, ll_anno, ll_riga_ds, ll_mese_prec, ll_anno_prec
long ll_index2, ll_tot2, ll_row
decimal ld_consumo, ld_km_ore, ld_consumo_cu, ld_km_ore_cu, ld_km_ore_prec,ld_costo_cu
datetime ldt_oggi, ldt_data_scadenza
integer li_ret
boolean lb_esiste_mensile = false

//inserisci la riga relativa all'attrezzatura
ll_row = dw_report.insertrow(0)
dw_report.setitem(ll_row, "cod_attrezzatura", fs_cod_attrezzatura)

//--------------------------------------------------------------------------------------------
//recupero il controllore
select cod_utente
into :ls_cod_controllore
from anag_attrezzature
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:fs_cod_attrezzatura;

if sqlca.sqlcode<0 then
	rollback;
	st_1.text = "Elaborazione terminata con errori!"
	destroy lds_data;
	setpointer(Arrow!)
	g_mb.messagebox("OMNIA","Errore in lettura Controllore Mezzo "+ &
									fs_cod_attrezzatura + " - "+sqlca.sqlerrtext,StopSign!)		
	return -1
end if
dw_report.setitem(ll_row, "controllore", ls_cod_controllore)


//recupero il responsabile alla data di sistema
ldt_oggi = datetime(today(), now())

select cod_operaio
into :ls_cod_responsabile
from attrezzature_utilizzatori
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:fs_cod_attrezzatura and
			data_dal<=:ldt_oggi and (data_al is null or data_al>=:ldt_oggi);

if sqlca.sqlcode<0 then
	g_mb.messagebox("OMNIA","Errore in lettura Responsabile attuale Mezzo (tabella utilizzatori) "+ &
									fs_cod_attrezzatura + " - "+sqlca.sqlerrtext,StopSign!)		
	return -1
end if

if ls_cod_responsabile="" then setnull(ls_cod_responsabile)
dw_report.setitem(ll_row, "responsabile", ls_cod_responsabile)
//----------------------------------------------------------------------------------------------

ls_sql_hold = fs_sql
ls_sql_hold += " and a.cod_attrezzatura='"+fs_cod_attrezzatura+"' "

//ciclo mensile: sum nella registro_uso_attrezzature
ll_i = 0
ll_mese = month(date(fdt_inizio))
ll_anno = year(date(fdt_inizio))
lb_esiste_mensile = false

//recupero lettura mese precedente, se esiste ############################################
ll_mese_prec = ll_mese - 1
ll_anno_prec = ll_anno
if ll_mese_prec = 0 then
	ll_mese_prec = 12
	ll_anno_prec = ll_anno - 1
end if


//ls_sql1 = ls_sql_hold + " and month(data_prelievo)=" + string(ll_mese_prec) + " and year(data_prelievo)="+string(ll_anno_prec)+" "

string ls_data_inizio,ls_data_fine
datetime ldt_data_inizio, ldt_data_fine

ls_data_inizio = string(ll_anno_prec) + string(ll_mese_prec) +"01"
choose case ll_mese_prec
	case 1,3,5,7,8,10,31
		ls_data_fine = string(ll_anno_prec) + string(ll_mese_prec) + "31"
	case 6,4,9,11
		ls_data_fine = string(ll_anno_prec) + string(ll_mese_prec) + "30"
	case 2
		if mod(ll_anno_prec,4) = 0 then
			ls_data_fine = string(ll_anno_prec) + string(ll_mese_prec) + "29"
		else
			ls_data_fine = string(ll_anno_prec) + string(ll_mese_prec) + "28"
		end if
end choose
ldt_data_inizio = datetime(date(ls_data_inizio), 00:00:00)
ldt_data_fine = datetime(date(ls_data_fine), 00:00:00)

ls_sql1 = ls_sql_hold + g_str.format(" and data_prelievo >= '$1' and data_prelievo <= '$2' ", string(ldt_data_inizio,s_cs_xx.db_funzioni.formato_data), string(ldt_data_fine,s_cs_xx.db_funzioni.formato_data))

//crea datastore
if not f_crea_datastore(lds_data, ls_sql1) then
	//dw_report.setredraw(true)
	st_1.text = "Elaborazione terminata con errori!"
	destroy lds_data;
	setpointer(Arrow!)
	rollback;
	return -1
end if

ll_tot2 = lds_data.retrieve()
ld_km_ore = 0	//colonna contatore in tabella
ld_km_ore_prec = 0
ld_consumo = 0
ld_km_ore_cu = 0
ld_consumo_cu = 0

for ll_index2=1 to ll_tot2
	
	lb_esiste_mensile = true
	
	ld_km_ore_cu = lds_data.getitemdecimal(ll_index2, 3)
	
	if isnull(ld_km_ore_cu) then ld_km_ore_cu = 0
	
	ld_km_ore_prec += ld_km_ore_cu
next

destroy lds_data;

//fai i setitem
if isnull(ld_km_ore_prec) then ld_km_ore_prec = 0
dw_report.setitem(ll_row, "km_0", ld_km_ore_prec)

//##############################################

for ll_index = 1 to 12
	ll_i += 1
	
	ls_sql = ls_sql_hold + " and month(data_prelievo)=" + string(ll_mese) + " and year(data_prelievo)="+string(ll_anno)+" "
	
	//crea datastore
	if not f_crea_datastore(lds_data, ls_sql) then
		//dw_report.setredraw(true)
		st_1.text = "Elaborazione terminata con errori!"
		destroy lds_data;
		setpointer(Arrow!)
		rollback;
		return -1
	end if
	
	ll_tot2 = lds_data.retrieve()
	ld_km_ore = 0	//colonna contatore in tabella
	ld_consumo = 0
	ld_km_ore_cu = 0
	ld_consumo_cu = 0
	ld_costo_cu = 0

	for ll_index2=1 to ll_tot2
		
		lb_esiste_mensile = true
		
		ld_km_ore_cu = lds_data.getitemdecimal(ll_index2, 3)
		ld_consumo_cu = lds_data.getitemdecimal(ll_index2, 4)
		ld_costo_cu = lds_data.getitemdecimal(ll_index2, 5)
		
		if isnull(ld_km_ore_cu) then ld_km_ore_cu = 0
		if isnull(ld_consumo_cu) then ld_consumo_cu = 0
		if isnull(ld_costo_cu) then ld_costo_cu = 0
		
		ld_km_ore += ld_km_ore_cu
		ld_consumo += ld_consumo_cu	
	next
	
	destroy lds_data;
	
	//fai i setitem per differenza sul precedente per i soli km
	
	//salvo la lettura del mese
	ld_km_ore_cu = ld_km_ore
	
	if ld_km_ore > 0 then
		//se la lettura è maggiore di zero faccio la differenza con la precedente per avere la lettura netta mensile
		ld_km_ore = ld_km_ore - ld_km_ore_prec
	end if
	
	dw_report.setitem(ll_row, "km_"+string(ll_i), ld_km_ore)
	
	//poi salva la lettura del mese perchè diventi quella precedente per un eventuale mese successivo
	ld_km_ore_prec = ld_km_ore_cu
	//--------------------------------------------------------------------------------------------------------------
	
	dw_report.setitem(ll_row, "cont_"+string(ll_i), ld_consumo)
	dw_report.setitem(ll_row, "costo_"+string(ll_i), ld_costo_cu)
	
	if ld_km_ore=0 or ld_consumo=0 then
		//verifica se esiste una manutenzione con tipologia mensile (_MENS) aperta con scadenza nel mese corrente
		//se esiste e la sua scadenza è più vecchia di X giorni dalla data di sistema, allora colora di rosso
		//e memorizza il possibile invio mail dal controllore al responsabile del mezzo
		
		li_ret = wf_check_manutenzione(fs_cod_attrezzatura, fs_cod_tipo_manutenzione, ll_mese, ll_anno, ldt_data_scadenza)
		
		if li_ret<0 then
			st_1.text = "Elaborazione terminata con errori!"
			destroy lds_data;
			setpointer(Arrow!)
			rollback;
			return -1
		end if
				
		choose case li_ret
			case 1
				//colora rosso e memorizza da qualche parte la possibilità di inviare mail
				dw_report.setitem(ll_row, "scad_"+string(ll_i), "R")			
				
				//datastore con i dati per invio email
				ll_riga_ds = ids_mezzi_destinatari.insertrow(0)
				ids_mezzi_destinatari.setitem(ll_riga_ds, 1, fs_cod_attrezzatura)
				ids_mezzi_destinatari.setitem(ll_riga_ds, 2, ls_cod_responsabile)
				ids_mezzi_destinatari.setitem(ll_riga_ds, 3, ls_cod_controllore)
				ids_mezzi_destinatari.setitem(ll_riga_ds, 4, fs_cod_tipo_manutenzione)			
				ids_mezzi_destinatari.setitem(ll_riga_ds, 5, ldt_data_scadenza)
				
			case 2
				//colora giallo
				dw_report.setitem(ll_row, "scad_"+string(ll_i), "G")
				
			case 3
				//non esistono registrazione (aperte chiuse) di tipo _MENS
				dw_report.setitem(ll_row, "scad_"+string(ll_i), "N")
				
		end choose
		
	end if
	
	//mese successivo
	ll_mese += 1
	if ll_mese=13 then
		ll_mese=1
		ll_anno += 1
	end if
next

//adesso valuta la lettura attuale, se esiste (ultima effettuata) ######################################
ls_sql = ls_sql_hold + " and data_prelievo<='" + string(ldt_oggi, s_cs_xx.db_funzioni.formato_data) + "' "+&
									"and contatore>0 "
ls_sql += "order by data_prelievo desc"

//crea datastore
if not f_crea_datastore(lds_data, ls_sql) then
	//dw_report.setredraw(true)
	st_1.text = "Elaborazione terminata con errori!"
	destroy lds_data;
	setpointer(Arrow!)
	rollback;
	return -1
end if

ll_tot2 = lds_data.retrieve()
ld_km_ore = 0	//colonna contatore in tabella
ld_consumo = 0
ld_km_ore_cu = 0
ld_consumo_cu = 0

if ll_tot2>0 then
	//leggi dalla prima
	ld_km_ore = lds_data.getitemdecimal(1, 3)
end if

destroy lds_data;

//fai i setitem
if isnull(ld_km_ore) then ld_km_ore = 0
dw_report.setitem(ll_row, "km_f", ld_km_ore)

//##############################################

return 1
end function

public function integer wf_carica_sql_mensile (ref string fs_filtro, ref string fs_sql, ref string fs_sql_distinct);datetime ldt_data_blocco
string ls_temp, ls_where

fs_sql = "select distinct "+&
				"a.cod_attrezzatura,"+&
				"a.data_prelievo,"+&
				"a.contatore,"+&
				"a.consumo,"+&
				"a.costo_consumo "+&
			"from registro_uso_attrezzature a "+&
			"join anag_attrezzature b on 	b.cod_azienda=a.cod_azienda and "+&
														   "b.cod_attrezzatura=a.cod_attrezzatura "+&
			"join tab_tipi_manutenzione tm on 	tm.cod_azienda=b.cod_azienda and "+&
														   "tm.cod_attrezzatura=b.cod_attrezzatura "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' "

fs_sql_distinct = 	"select distinct b.cod_attrezzatura "+&
							"from anag_attrezzature b "+&
							"join tab_tipi_manutenzione tm on 	tm.cod_azienda=b.cod_azienda and "+&
														   "tm.cod_attrezzatura=b.cod_attrezzatura "+&
							"where b.cod_azienda='"+s_cs_xx.cod_azienda+"' "
							
//carica solo le attrezzature primarie
fs_sql_distinct += " and b.flag_primario='S' "

fs_filtro = ""

ls_temp = dw_selezione.getitemstring(1, "cod_utente")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_utente='"+ls_temp+"' "
	
	fs_filtro += " - Resp.Mezzo "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_attrezzatura")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_attrezzatura>='"+ls_temp+"' "
	
	fs_filtro += " - Da Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_attrezzatura_a")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_attrezzatura<='"+ls_temp+"' "	
	
	fs_filtro += " - Ad Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_tipo_attrez")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_tipo_attrez='"+ls_temp+"' "	
	
	fs_filtro += " - Tipologia Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_reparto")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_reparto='"+ls_temp+"' "
	
	fs_filtro += " - Reparto Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_cat_attrezzature")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_cat_attrezzature='"+ls_temp+"' "
	
	fs_filtro += " - Categoria Attrezzatura "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "cod_area_aziendale")
if not isnull(ls_temp) and ls_temp<>"" then
	ls_where += " and b.cod_area_aziendale='"+ls_temp+"' "
	
	fs_filtro += " - Area Aziendale "+ls_temp
end if

ls_temp = dw_selezione.getitemstring(1, "flag_blocco")
if ls_temp="N" then
	//visualizza solo quelle non bloccate
	ls_where += " and b.flag_blocco='N' "
	
	fs_filtro += " - Solo Attrezzatura NON bloccate"
else
	//visualizza anche se bloccate
	ldt_data_blocco = dw_selezione.getitemdatetime(1, "data_blocco")
	
	if not isnull(ldt_data_blocco) and year(date(ldt_data_blocco))>1950 then
		//visualizza anche quelle bloccate, purchè non lo siano in una data inferiore a quella specificata
		ls_where += " and (b.flag_blocco='N' or (b.flag_blocco='S' and data_blocco>'"+string(ldt_data_blocco, s_cs_xx.db_funzioni.formato_data)+"')) "
		
		fs_filtro += " - Attrezzatura NON bloccate ed in aggiunta quelle BLOCCATE dopo il "+string(ldt_data_blocco, "dd-MM-yyyy")
	else
		//non è stata specificata alcuna data di blocco
		//quindi includi anche quelle bloccate
		//quindi non mettere niente nella where che riguardi il flag blocco
		fs_filtro += " - Attrezzatura BLOCCATE e NON bloccate"
	end if
end if

wf_add_filtro_per_tipo_man(ls_where)

fs_sql += ls_where
fs_sql_distinct += ls_where

return 1
end function

public function integer wf_report_mensile_2 ();datetime ldt_inizio, ldt_fine, ldt_mese_anno, ldt_data_blocco, ldt_data_prelievo, ldt_data_restituzione
string ls_sql, ls_temp, ls_cod_attrezzatura, ls_cod_operaio, ls_cod_reparto, ls_flag_tipo_tempo
string ls_cod_tipo_attrez, ls_cod_cat_attrezzatura, ls_cod_area_aziendale, ls_filtro, ls_mese1, ls_mese2
datastore lds_data_attrezz
long ll_tot, ll_index, ll_prog_uso_attrezz, ll_anno_commessa, ll_num_commessa, ll_riga, ll_tot2, ll_index2
decimal ld_contatore, ld_consumo
string ls_sql_distinct, ls_flag_scad_mezzi, ls_sql_periodo
long ll_mese, ll_anno

setpointer(Hourglass!)

dw_selezione.accepttext()
dw_report.reset()
dw_report.dataobject="d_report_registro_uso_attrezz_mens"
dw_report.reset()
ids_mezzi_destinatari.reset()

dw_report.modify("intestazione.filename='" + is_path_logo + "'")

st_1.text = "Elaborazione Report in corso ...."

//ci attacco il periodo interessato
ldt_mese_anno = dw_selezione.getitemdatetime(1, "mese_anno")
if isnull(ldt_mese_anno) or year(date(ldt_mese_anno))<1950 then
	setpointer(Arrow!)
	g_mb.messagebox("OMNIA","Impostare il mese/anno di arrivo",Exclamation!)	
	rollback;
	return -1
end if

//-------------------------------------------------------
//valuta quale cod_tipo_manutenzione ricercare (parametro CMR, che sarebbe _MENS o MENSRG)
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

if luo_mansionario.uof_get_privilege(luo_mansionario.scadenz_mezzi) then
	ls_flag_scad_mezzi = "S"
else
	ls_flag_scad_mezzi = "N"
end if

destroy luo_mansionario;
//select flag_nuove_scadenze
//into :ls_flag_scad_mezzi
//from mansionari
//where cod_azienda=:s_cs_xx.cod_azienda and
//	cod_utente=:s_cs_xx.cod_utente;
//
//if sqlca.sqlcode < 0 then
//	setpointer(Arrow!)
//	st_1.text = "Elaborazione terminata con errori!"
//	g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: " + sqlca.sqlerrtext,stopsign!)
//	return -1
//		
//elseif sqlca.sqlcode = 100 then
//	setpointer(Arrow!)
//	st_1.text = "Elaborazione terminata con errori!"
//	g_mb.messagebox("OMNIA","Errore in verifica mansionario utente: nessun mansionario associato",stopsign!)
//	return -1
//end if
if isnull(ls_flag_scad_mezzi) then ls_flag_scad_mezzi = "N"

if ls_flag_scad_mezzi="S" then
	is_cod_tipo_manutenzione = "MENSRG"
else
	select stringa
	into	:is_cod_tipo_manutenzione
	from	parametri
	where	cod_parametro = 'CMR';
	
	if isnull(is_cod_tipo_manutenzione) or is_cod_tipo_manutenzione="" then
		g_mb.messagebox("OMNIA","Impostare il parametro multi-aziendale CMR",Exclamation!)
		st_1.text = "Elaborazione terminata con errori!"
		setpointer(Arrow!)
		rollback;
		return -1
	end if
end if
//-------------------------------------------------------


ls_filtro = ""
if wf_carica_sql_mensile(ls_filtro, ls_sql, ls_sql_distinct) < 0 then
	st_1.text = "Elaborazione terminata con errori!"
	setpointer(Arrow!)
	rollback;
	return -1
end if

choose case month(date(ldt_mese_anno))
	case 11,4,6,9  //30
		ldt_fine = datetime(date(year(date(ldt_mese_anno)), month(date(ldt_mese_anno)), 30) ,00:00:00)
		
	case 2 //28
		ldt_fine = datetime(date(year(date(ldt_mese_anno)), month(date(ldt_mese_anno)), 28) ,00:00:00)
		
	case else //31
		ldt_fine = datetime(date(year(date(ldt_mese_anno)), month(date(ldt_mese_anno)), 31) ,00:00:00)
		
end choose

choose case month(date(ldt_mese_anno))
	case 12
		//il periodo annuale andrà a gennaio dello stesso anno (con giorno 1)
		ldt_inizio = datetime(date(year(date(ldt_mese_anno)), 1, 1) ,00:00:00)
		
	case else
		//il periodo annuale andrà al mese + 1 dell'anno precedente (con giorno 1)
		ldt_inizio = datetime(date(year(date(ldt_mese_anno)) - 1, month(date(ldt_mese_anno)) + 1, 1) ,00:00:00)
		
end choose

//valorizzo nell'sql il periodo

ls_sql_periodo = " and a.data_prelievo>='"+string(ldt_inizio, s_cs_xx.db_funzioni.formato_data)+"' "
ls_sql_periodo += " and a.data_prelievo<='"+string(ldt_fine, s_cs_xx.db_funzioni.formato_data)+"' "
//ls_sql += " and a.data_prelievo>='"+string(ldt_inizio, s_cs_xx.db_funzioni.formato_data)+"' "
//ls_sql += " and a.data_prelievo<='"+string(ldt_fine, s_cs_xx.db_funzioni.formato_data)+"' "


//scrivo nella stringa filtro
wf_mese(month(date(ldt_inizio)), ls_mese1)
ls_mese1 = "Periodo da  "+ls_mese1 + " " + string(year(date(ldt_inizio)))

wf_mese(month(date(ldt_fine)), ls_mese2)
ls_mese2 = " A  "+ls_mese2 + " " + string(year(date(ldt_fine)))

ls_filtro = ls_mese1 + ls_mese2 + ls_filtro

//crea il datastore attrezzature distinct
if not f_crea_datastore(lds_data_attrezz, ls_sql_distinct) then
	//dw_report.setredraw(true)
	st_1.text = "Elaborazione terminata con errori! (2)"
	setpointer(Arrow!)
	rollback;
	return -1
end if

//preparo le etichette di intestazione
ll_mese = month(date(ldt_inizio))
ll_anno = year(date(ldt_inizio))
for ll_index2 = 1 to 12	
	wf_mese(ll_mese, ls_mese1)
	
	//prendo solo i primi 3 caratteri
	ls_mese1 = left(ls_mese1, 3)
	
	//accodo l'anno
	ls_mese1 += " " + string(ll_anno)
	
	dw_report.modify("mese"+string(ll_index2)+"_t.text='"+ls_mese1+"'")
	
	//se sfori vai ad inizio anno successivo
	ll_mese +=1
	if ll_mese = 13 then
		ll_mese = 1
		ll_anno += 1
	end if
next
//---------------------------------------------

ll_tot2 = lds_data_attrezz.retrieve()

if ls_filtro<>"" then dw_report.object.t_filtro.text = ls_filtro

lds_data_attrezz.setsort("#1 A") //per cod attrezzatura

for ll_index2=1 to ll_tot2
	ls_cod_attrezzatura = lds_data_attrezz.getitemstring(ll_index2, 1)
	
	st_1.text = "Elaborazione attrezzatura "+ls_cod_attrezzatura + " -------- "+string(ll_index2) + " di " +string(ll_tot2)
	
	if wf_dati_mensili_2(ls_sql, ls_sql_periodo, ls_cod_attrezzatura, is_cod_tipo_manutenzione, ldt_inizio, ldt_fine) < 0 then
		st_1.text = "Elaborazione terminata con errori! (m)"
		setpointer(Arrow!)
		rollback;
		return -1
	end if
	
next

dw_report.sort()
dw_report.groupcalc()

dw_report.object.datawindow.print.preview = "Yes"

st_1.text = "Elaborazione Completata!"

setpointer(Arrow!)

dw_folder.fu_selecttab(2)

destroy lds_data_attrezz;

return 1
end function

public function integer wf_dati_mensili_2 (string fs_sql, string fs_sql_periodo, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, datetime fdt_inizio, datetime fdt_fine);datastore lds_data
string ls_sql, ls_cod_responsabile, ls_cod_controllore, ls_sql_hold, ls_sql1, ls_data_inizio, ls_data_fine
long ll_index, ll_tot, ll_i, ll_mese, ll_anno, ll_riga_ds, ll_mese_prec, ll_anno_prec
long ll_index2, ll_tot2, ll_row
decimal ld_consumo, ld_km_ore, ld_consumo_cu, ld_km_ore_cu, ld_km_ore_prec,ld_costo_cu, ld_km_ore_hold_cu
datetime ldt_oggi, ldt_data_scadenza, ldt_data_inizio, ldt_data_fine
integer li_ret
boolean lb_esiste_mensile = false, lb_check_manutenzione=false

//inserisci la riga relativa all'attrezzatura
ll_row = dw_report.insertrow(0)
dw_report.setitem(ll_row, "cod_attrezzatura", fs_cod_attrezzatura)

//--------------------------------------------------------------------------------------------
//recupero il controllore
select cod_utente
into :ls_cod_controllore
from anag_attrezzature
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:fs_cod_attrezzatura;

if sqlca.sqlcode<0 then
	rollback;
	st_1.text = "Elaborazione terminata con errori!"
	destroy lds_data;
	setpointer(Arrow!)
	g_mb.messagebox("OMNIA","Errore in lettura Controllore Mezzo "+ &
									fs_cod_attrezzatura + " - "+sqlca.sqlerrtext,StopSign!)		
	return -1
end if
dw_report.setitem(ll_row, "controllore", ls_cod_controllore)


//recupero il responsabile alla data di sistema
ldt_oggi = datetime(today(), now())

select cod_operaio
into :ls_cod_responsabile
from attrezzature_utilizzatori
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:fs_cod_attrezzatura and
			data_dal<=:ldt_oggi and (data_al is null or data_al>=:ldt_oggi);

if sqlca.sqlcode<0 then
	g_mb.messagebox("OMNIA","Errore in lettura Responsabile attuale Mezzo (tabella utilizzatori) "+ &
									fs_cod_attrezzatura + " - "+sqlca.sqlerrtext,StopSign!)		
	return -1
end if

if ls_cod_responsabile="" then setnull(ls_cod_responsabile)
dw_report.setitem(ll_row, "responsabile", ls_cod_responsabile)

ll_mese = month(date(fdt_inizio))
ll_anno = year(date(fdt_inizio))

//in realtà comincia dal mese precedente
ll_mese -= 1
if ll_mese = 0 then
	ll_mese = 12
	ll_anno = ll_anno - 1
end if

ld_km_ore_prec = 0
ld_km_ore_hold_cu = 0

for ll_i = 0 to 12
	//azzero le variabili
	ld_km_ore_cu = 0
	ld_consumo_cu = 0
	ld_costo_cu = 0
	
	ls_data_inizio = "01/" +  string(ll_mese,"00") + "/" + string(ll_anno,"0000")
	choose case ll_mese
		case 1,3,5,7,8,10,31
			ls_data_fine = "31/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
		case 6,4,9,11
			ls_data_fine = "30/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
		case 2
			if mod(ll_anno_prec,4) = 0 then
				ls_data_fine = "29/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
			else
				ls_data_fine = "28/" + string(ll_mese,"00") + "/" + string(ll_anno,"0000")
			end if
	end choose
	ldt_data_inizio = datetime(date(ls_data_inizio), 00:00:00)
	ldt_data_fine = datetime(date(ls_data_fine), 00:00:00)

	select contatore,consumo,costo_consumo
	into :ld_km_ore_cu, :ld_consumo_cu, :ld_costo_cu
	from registro_uso_attrezzature
	where cod_azienda=:s_cs_xx.cod_azienda and 
	cod_attrezzatura=:fs_cod_attrezzatura and
	data_prelievo >= :ldt_data_inizio and 
	data_prelievo <= :ldt_data_fine;
//	month(data_prelievo) = :ll_mese and 
//	year(data_prelievo)=:ll_anno;
	
	if sqlca.sqlcode<0 then
		g_mb.messagebox("OMNIA","Errore in lettura rilevazioni; mese: "+string(ll_mese)+ " "+char(13)+&
									"anno: "+string(ll_anno)+ " attrezzatura: "+fs_cod_attrezzatura+char(13)+&
									sqlca.sqlerrtext, StopSign!)
		return -1
		
	elseif sqlca.sqlcode=100 then
		lb_check_manutenzione = true
		
	else
		lb_check_manutenzione=false
		
	end if
	
	if isnull(ld_km_ore_cu) then ld_km_ore_cu = 0
	if isnull(ld_consumo_cu) then ld_consumo_cu = 0
	if isnull(ld_costo_cu) then ld_costo_cu = 0		
			
	
	if ld_km_ore_cu > 0 then
		//se la lettura è maggiore di zero faccio la differenza con la precedente per avere la lettura netta mensile
		
		//mantieni la lettira prima che subisca la differenza
		ld_km_ore_hold_cu = ld_km_ore_cu
		
		//fai la differenza e con tale variabile fai poi il setitem
		ld_km_ore_cu = ld_km_ore_cu - ld_km_ore_prec		
		
		//ripristina la lettura perchè diventi quella precedente per un eventuale mese successivo
		ld_km_ore_prec = ld_km_ore_hold_cu
	end if
	dw_report.setitem(ll_row, "km_"+string(ll_i), ld_km_ore_cu)
	
//	//salva la lettura del mese perchè diventi quella precedente per un eventuale mese successivo
//	ld_km_ore_prec = ld_km_ore_cu
//	//--------------------------------------------------------------------------------------------------------------
	
	if ll_i>0 then
		dw_report.setitem(ll_row, "cont_"+string(ll_i), ld_consumo_cu)
		dw_report.setitem(ll_row, "costo_"+string(ll_i), ld_costo_cu)
	end if
		
	
	
	//mese successivo
	ll_mese += 1
	if ll_mese=13 then
		ll_mese=1
		ll_anno += 1
	end if
	
next


li_ret = wf_check_manutenzione_2(fs_cod_attrezzatura, fs_cod_tipo_manutenzione)
choose case li_ret
	case 0	//bianco
		//non fare niente
		
	case -1
		return -1
				
	case 2	//giallo
		dw_report.setitem(ll_row, "scad_1", "G")
		
	case 1	//rosso
		dw_report.setitem(ll_row, "scad_1", "R")
		
		//inserisci nel datastore
		//datastore con i dati per invio email
		ll_riga_ds = ids_mezzi_destinatari.insertrow(0)
		ids_mezzi_destinatari.setitem(ll_riga_ds, 1, fs_cod_attrezzatura)
		ids_mezzi_destinatari.setitem(ll_riga_ds, 2, ls_cod_responsabile)
		ids_mezzi_destinatari.setitem(ll_riga_ds, 3, ls_cod_controllore)
		ids_mezzi_destinatari.setitem(ll_riga_ds, 4, fs_cod_tipo_manutenzione)			
		ids_mezzi_destinatari.setitem(ll_riga_ds, 5, ldt_data_scadenza)
end choose


return 1
end function

public function integer wf_check_manutenzione_2 (string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione);long ll_gg, ll_daysafter, ll_count
datetime ldt_data_rif, ldt_oggi

//torna 1 	se esistono manutenzioni aperte con data inferiore alla data di sistema
//				e più vecchi di X giorni
//torna 2		se esistono manutenzioni aperte con data inferiore alla data di sistema
//				entro la tolleranza di X giorni
//torna 3		se non esistono manutenzioni (aperte o chiuse) in qualsivoglia periodo
//torna 0		tutti gli altri casi

ll_gg = 5
ll_gg = ll_gg * (-1)

//calcolo data comprensiva di tolleranza
ldt_oggi = datetime(today(), 00:00:00)
ldt_data_rif = datetime(relativedate(date(ldt_oggi), ll_gg), 00:00:00)

//scadenze nel periodo di tolleranza: GIALLO
select count(*)
into :ll_count
from manutenzioni
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :fs_cod_attrezzatura and
			cod_tipo_manutenzione = :fs_cod_tipo_manutenzione and
			flag_eseguito='N' and 
			data_registrazione >= :ldt_data_rif and data_registrazione <=:ldt_oggi;
			
if sqlca.sqlcode<0 then
	g_mb.messagebox("OMNIA","Errore in lettura dati manutenzioni aperte (1): attrezzature "+fs_cod_attrezzatura+char(13)+&
									sqlca.sqlerrtext,Exclamation!)
	return -1
end if

if isnull(ll_count) or ll_count=0 then
	//scadenze oltre il periodo di tolleranza: ROSSO
	
	select count(*)
	into :ll_count
	from manutenzioni
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_attrezzatura = :fs_cod_attrezzatura and
				cod_tipo_manutenzione = :fs_cod_tipo_manutenzione and
				flag_eseguito='N' and 
				data_registrazione < :ldt_data_rif;

				
	if sqlca.sqlcode<0 then
		g_mb.messagebox("OMNIA","Errore in lettura dati manutenzioni aperte (2): attrezzature "+fs_cod_attrezzatura+char(13)+&
										sqlca.sqlerrtext,Exclamation!)
		return -1
	end if
	
	if isnull(ll_count) or ll_count=0 then
		//bianco
		return 0
	else
		//giallo
		return 1
	end if
	
else
	//giallo
	return 2
end if

//bianco
return 0
end function

public subroutine wf_add_filtro_per_tipo_man (ref string fs_where);string			ls_flag_pianificata


if is_flag_scad_mezzi = "S" then
	//-------------------------------------------------------------------------------------------------
	//Il mansionario dell'utente HA il privilegio "Scadenzario Mezzi"
	//è possibile scegliere se visualizzare le Mensili o le Pianificate
	
	ls_flag_pianificata = dw_selezione.getitemstring(1, "flag_pianificata")
	
	if ls_flag_pianificata="P" then
		//caso Pianificate:
		//visualizza le tipologie manutenzioni con flag_scad_mezzi='S', cioè ad esempio ASS, TAXPRO, REV, ANAS 1, ecc...
		fs_where += " and tm.cod_tipo_manutenzione in (	select cod_tipo_manutenzione "+&
																								"from tab_tipi_manutenzione where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
																																			"flag_scad_mezzi ='S' ) "	
	else
		//caso Mensili
		//in tal caso visualizza le tipologie di manutenzione con codice 'MENSRG', cioè le mensili che devono essere gestite dalla Segreteria ...
		fs_where += " and tm.cod_tipo_manutenzione in (	select cod_tipo_manutenzione "+&
																								"from tab_tipi_manutenzione "+&
																								"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
																											"flag_scad_mezzi <>'S' and "+&
																											"cod_tipo_manutenzione='"+is_parametro_reg+"') "	
	end if
else
	//-------------------------------------------------------------------------------------------------
	//Il mansionario dell'utente NON ha il privilegio "Scadenzario Mezzi"
	//visualizza le tipologie con flag_scad_mezzi <>'S' e codice DIVERSO da 'MENSRG' (per fare in modo che siano mutualmente escludenti ...)
	fs_where += " and tm.cod_tipo_manutenzione in (		select cod_tipo_manutenzione "+&
																								"from tab_tipi_manutenzione "+&
																								"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
																											"flag_scad_mezzi<>'S' and "+&
																											"cod_tipo_manutenzione<>'MENSRG') "
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_modify, ls_sql

dw_report.ib_dw_report = true

windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_reset
l_objects[4] = st_1
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,4)
dw_folder.fu_selecttab(1)

select parametri_azienda.stringa
into   :is_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

is_path_logo = s_cs_xx.volume + is_path_logo

ls_modify = "intestazione.filename='" + is_path_logo + "'"
dw_report.modify(ls_modify)

//mi serve un datastore per 
//1		cod_attrezzatura, 
//2		cod_utilizzatore
//3		cod_controllore
//4		cod_tipo_manutenzione
//5		data_scadenza
ls_sql = "select '                                        ','                                        ',"+&
						"'                                        ','                                        ', data_blocco "+&
			"from aziende where cod_azienda='CODICEINESISTENTE'"
f_crea_datastore(ids_mezzi_destinatari, ls_sql)
ids_mezzi_destinatari.retrieve()


//#############################################################
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

if luo_mansionario.uof_get_privilege(luo_mansionario.scadenz_mezzi) then
	is_flag_scad_mezzi = "S"
else
	is_flag_scad_mezzi = "N"
end if

destroy luo_mansionario;

if is_flag_scad_mezzi="S" then
	dw_selezione.object.flag_pianificata.visible=true
	is_parametro_reg = "MENSRG"
else
	dw_selezione.object.flag_pianificata.visible=false
	
	select stringa
	into	:is_parametro_reg
	from	parametri
	where	cod_parametro = 'CMR';
end if
//#############################################################


end event

on w_report_registro_uso_attrez.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_selezione=create dw_selezione
this.cb_report=create cb_report
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.cb_report
this.Control[iCurrent+6]=this.st_1
end on

on w_report_registro_uso_attrez.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_selezione)
destroy(this.cb_report)
destroy(this.st_1)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
					   "anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  //"anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  //"((anag_reparti.flag_blocco <> 'S') or (anag_reparti.flag_blocco = 'S' and anag_reparti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_selezione,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW(dw_selezione,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
					   "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  //"tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana='N'")
					  
f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_attrez",sqlca,&
                 "tab_tipologie_attrez","cod_tipo_attrez","des_tipo_attrez", &
					  "tab_tipologie_attrez.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					 // "tab_tipologie_attrez.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  //"((tab_tipologie_attrez.flag_blocco <> 'S') or (tab_tipologie_attrez.flag_blocco = 'S' and tab_tipologie_attrez.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_selezione,"cod_utente",sqlca,&
                 "utenti","cod_utente","nome_cognome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

event closequery;call super::closequery;destroy ids_mezzi_destinatari
end event

type cb_reset from commandbutton within w_report_registro_uso_attrez
integer x = 837
integer y = 1404
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;string ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)

dw_selezione.setitem(1,"data_da", ldt_null)
dw_selezione.setitem(1,"data_a", ldt_null)
dw_selezione.setitem(1,"cod_attrezzatura", ls_null)
dw_selezione.setitem(1,"cod_attrezzatura_a", ls_null)
dw_selezione.setitem(1,"cod_tipo_attrez", ls_null)
dw_selezione.setitem(1,"cod_reparto", ls_null)
dw_selezione.setitem(1,"cod_cat_attrezzature", ls_null)
dw_selezione.setitem(1,"cod_area_aziendale", ls_null)
dw_selezione.setitem(1,"flag_blocco", "N")
dw_selezione.setitem(1,"data_blocco", ldt_null)
end event

type dw_report from uo_cs_xx_dw within w_report_registro_uso_attrez
integer x = 18
integer y = 132
integer width = 5115
integer height = 2176
integer taborder = 10
string dataobject = "d_report_registro_uso_attrezz"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event clicked;call super::clicked;long ll_rows

choose case dwo.name
	case "t_mail"
		
		//non ci sono scadenze in rosso
		ll_rows = ids_mezzi_destinatari.rowcount()		
		if ll_rows<=0 then
			g_mb.messagebox("OMNIA", "Nessuna e-mail da elaborare",  Exclamation!)
			return
		end if
		
		s_cs_xx.parametri.parametro_ds_1 = ids_mezzi_destinatari
		window_open(w_report_registro_uso_attrez_mail, 0)
		
end choose
end event

type dw_folder from u_folder within w_report_registro_uso_attrez
integer x = 9
integer y = 20
integer width = 5166
integer height = 2316
integer taborder = 20
boolean border = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_registro_uso_attrez
event ue_cerca_attrezzatura ( string as_column_name )
integer x = 23
integer y = 156
integer width = 3589
integer height = 1124
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_registro_uso_attrezz_sel"
boolean border = false
end type

event ue_cerca_attrezzatura(string as_column_name);guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,as_column_name)
end event

event itemchanged;call super::itemchanged;datetime ldt_null

setnull(ldt_null)

if row>0 then
	choose case dwo.name
		case "flag_blocco"
			if data<>"S" then
				//resetta data blocco (il protect è gestito da formula su dw)
				setitem(row, "data_blocco", ldt_null)
			end if
			
	end choose
end if
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_attrezzatura_da"
		event ue_cerca_attrezzatura("cod_attrezzatura")

	case "b_attrezzatura_a"
		event ue_cerca_attrezzatura("cod_attrezzatura_a")
		
end choose
end event

type cb_report from commandbutton within w_report_registro_uso_attrez
integer x = 1257
integer y = 1404
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_tipo_report

dw_selezione.accepttext()
ls_tipo_report = dw_selezione.getitemstring(1, "flag_tipo")

if ls_tipo_report = "A" then
	//report mensile
	//wf_report_mensile()
	wf_report_mensile_2()
else
	//report da data a data
	wf_report_data_a_data()
end if
end event

type st_1 from statictext within w_report_registro_uso_attrez
integer x = 87
integer y = 1300
integer width = 2071
integer height = 84
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8421504
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

