﻿$PBExportHeader$w_report_registro_uso_attrez_mail.srw
forward
global type w_report_registro_uso_attrez_mail from w_cs_xx_risposta
end type
type cb_mail from commandbutton within w_report_registro_uso_attrez_mail
end type
type cb_nessuno from commandbutton within w_report_registro_uso_attrez_mail
end type
type cb_tutti from commandbutton within w_report_registro_uso_attrez_mail
end type
type dw_lista from datawindow within w_report_registro_uso_attrez_mail
end type
end forward

global type w_report_registro_uso_attrez_mail from w_cs_xx_risposta
integer width = 3808
integer height = 1700
string title = "Preparazione e-mail"
cb_mail cb_mail
cb_nessuno cb_nessuno
cb_tutti cb_tutti
dw_lista dw_lista
end type
global w_report_registro_uso_attrez_mail w_report_registro_uso_attrez_mail

type variables

end variables

forward prototypes
public subroutine wf_carica_riga (ref string fs_elenco, string fs_cod_attrezzatura, string fs_des_attrezzatura, string fs_cod_tipo_man, string ls_des_tipo_man, datetime fdt_scadenza)
public subroutine wf_carica_table (ref string fs_elenco)
public subroutine wf_carica_dest_distinct (ref datastore fds_data, string fs_cod_operaio)
public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive)
public function integer wf_mail_destinatario (string fs_elenco, string fs_smtp, string fs_controllore, string fs_mail_controllore, string fs_mail_responsabile)
public subroutine wf_leggi_riga (long fl_riga, datastore fds_lista_copy, ref string fs_responsabile, ref string fs_mail_responsabile, ref string fs_cod_controllore, ref string fs_mail_controllore, ref string fs_smtp, ref string fs_cod_attrezzatura, ref string fs_des_attrezzatura, ref string fs_cod_tipo_man, ref string fs_des_tipo_man, ref datetime fdt_scadenza)
end prototypes

public subroutine wf_carica_riga (ref string fs_elenco, string fs_cod_attrezzatura, string fs_des_attrezzatura, string fs_cod_tipo_man, string ls_des_tipo_man, datetime fdt_scadenza);string ls_riga
//wf_carica_riga(ls_elenco, ls_cod_attrezzatura, ls_des_attrezzatura, ls_cod_tipo_man, ls_des_tipo_man, ldt_scadenza)

ls_riga+="<tr>"
ls_riga+="<td>"+string(fdt_scadenza, "dd/mm/yyyy")+"</td>"
ls_riga+="<td>"+fs_cod_attrezzatura+"</td>"
ls_riga+="<td>"+fs_des_attrezzatura+"</td>"
ls_riga+="<td>"+fs_cod_tipo_man+"</td>"
ls_riga+="<td>"+ls_des_tipo_man+"</td>"
ls_riga+="</tr>"

fs_elenco += ls_riga
end subroutine

public subroutine wf_carica_table (ref string fs_elenco);string ls_table

ls_table ="<table><thead>"

ls_table+="<tr>"
ls_table+="<td><b>"+"Scadenza"+"</td>"
ls_table+="<td><b>"+"Cod. Mezzo"+"</td>"
ls_table+="<td><b>"+"Descrizione Mezzo"+"</td>"
ls_table+="<td><b>"+"Cod. Tipo Manut."+"</td>"
ls_table+="<td><b>"+"Des. Tipo Manutenzione"+"</td>"
ls_table+="</tr></thead>"

fs_elenco = ls_table + fs_elenco

fs_elenco += "</table>"
end subroutine

public subroutine wf_carica_dest_distinct (ref datastore fds_data, string fs_cod_operaio);long ll_index, ll_row
string ls_cod_operaio, ls_mail, ls_responsabile

ll_row = fds_data.rowcount()
for ll_index=1 to ll_row
	ls_cod_operaio = fds_data.getitemstring(ll_index, "cod_operaio")
	if fs_cod_operaio=ls_cod_operaio then
		//operatore già presente
		return
	end if
next

//se arrivi qui non hai trovato l'operatore, quindi aggiungilo
ll_index = fds_data.insertrow(0)
fds_data.setitem(ll_index, "cod_operaio", fs_cod_operaio)

return
end subroutine

public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive);/*
as_source                                                          Reference to the string to process
as_find                                                               The text we wish to change
as_replace                                                        The new text to be put in place of <as_find>

Return Values
Value                                                                   Comments
 1                                                                                           Everything OK
-1                                                                                          Some error occured
*/
long		ll_pos

 if isnull(as_source) or as_source = "" then
	return -1
end if

if isnull(as_find) or as_find = "" then
	return -1
end if

 ll_pos = 1

do
	 if ab_case_sensitive then
		ll_pos = pos(as_source,as_find,ll_pos)
	 else
		ll_pos = pos(lower(as_source),lower(as_find),ll_pos)
	 end if

	 if ll_pos = 0 then
		continue
	 end if
	 
	 as_source = replace(as_source,ll_pos,len(as_find),as_replace)	 
	 ll_pos += len(as_replace)

loop while ll_pos > 0

return 1


end function

public function integer wf_mail_destinatario (string fs_elenco, string fs_smtp, string fs_controllore, string fs_mail_controllore, string fs_mail_responsabile);string ls_oggetto, ls_destinatario, ls_mittente, ls_server, ls_user, ls_pwd
string ls_pathsendmail, ls_logfile, ls_path_file_msg, ls_msg
boolean lb_conferma
long ll_pos_elenco
integer li_FileNum
date ldt_oggi

ldt_oggi = today()

ls_server = fs_smtp
ls_mittente = fs_mail_controllore
ls_oggetto = "Scadenze non registrate"
ls_destinatario = fs_mail_responsabile
ls_logfile = s_cs_xx.volume + s_cs_xx.risorse + "SENDEMAIL\LOG\mail_"+string(ldt_oggi, "yyyymmdd")+".log"
ls_path_file_msg = s_cs_xx.volume + s_cs_xx.risorse + "SENDEMAIL\messaggio.txt"


//leggi dal file il messaggio
li_FileNum = FileOpen(ls_path_file_msg, TextMode!)
FileReadEx(li_FileNum, ls_msg)
FileClose(li_FileNum)

ll_pos_elenco = pos(ls_msg, "[ELENCO]")

if ll_pos_elenco>0 then
else
	g_mb.messagebox("APICE","Marcatore [ELENCO] non trovato nel file di configurazione del testo del messaggio e-mail",Exclamation!)	
	return -1
end if

if wf_replace_marker(ls_msg, "[ELENCO]", fs_elenco, false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore [ELENCO] nel file di configurazione del testo del messaggio e-mail '"+ls_path_file_msg,Exclamation!)
	return -1
end if

//metto i tag html
ls_msg = "<html><head></head><body>"+ls_msg+"</body></html>"

//filedelete(ls_logfile)

ls_user = f_des_tabella("utenti", "cod_utente = '" + fs_controllore + "'",  "smtp_usr")
ls_pwd = f_des_tabella("utenti", "cod_utente = '" + fs_controllore + "'",  "smtp_pwd")

ls_pathsendmail = s_cs_xx.volume + s_cs_xx.risorse+"SENDEMAIL\sendemail.exe"
ls_pathsendmail += " -f "+ls_mittente
ls_pathsendmail += " -t "+ls_destinatario
ls_pathsendmail += " -u "+ls_oggetto
ls_pathsendmail += " -m "+ls_msg
ls_pathsendmail += " -q "
ls_pathsendmail += " -s "+ls_server

if ls_user<>"" and not isnull(ls_user) then
	ls_pathsendmail += " -xu "+ls_user
	ls_pathsendmail += " -xp "+ls_pwd
end if

ls_pathsendmail += " -l "+ls_logfile
ls_pathsendmail += " -o message-content-type=html"
//ls_pathsendmail += " -o message-file="+ls_msg_standard


run(ls_pathsendmail)


return 1
end function

public subroutine wf_leggi_riga (long fl_riga, datastore fds_lista_copy, ref string fs_responsabile, ref string fs_mail_responsabile, ref string fs_cod_controllore, ref string fs_mail_controllore, ref string fs_smtp, ref string fs_cod_attrezzatura, ref string fs_des_attrezzatura, ref string fs_cod_tipo_man, ref string fs_des_tipo_man, ref datetime fdt_scadenza);

fs_responsabile = fds_lista_copy.getitemstring(fl_riga, "cod_utilizzatore")
fs_mail_responsabile = f_des_tabella("anag_operai", "cod_operaio = '" + fs_responsabile + "'",  "casella_mail")

fs_cod_controllore = fds_lista_copy.getitemstring(fl_riga, "cod_controllore")
fs_mail_controllore = f_des_tabella("utenti", "cod_utente = '" + fs_cod_controllore + "'",  "e_mail")
fs_smtp = f_des_tabella("utenti", "cod_utente = '" + fs_cod_controllore + "'",  "smtp_server")
fs_cod_attrezzatura = fds_lista_copy.getitemstring(fl_riga, "cod_attrezzatura")
fs_des_attrezzatura = f_des_tabella("anag_attrezzature", "cod_attrezzatura = '" + fs_cod_attrezzatura + "'",  "descrizione")
fs_cod_tipo_man = fds_lista_copy.getitemstring(fl_riga, "cod_tipo_manutenzione")
fs_des_tipo_man = f_des_tabella("tab_tipi_manutenzione", "cod_attrezzatura='" +  fs_cod_attrezzatura  + "' and "+&
																"cod_tipo_manutenzione='"+fs_cod_tipo_man+"'",  "des_tipo_manutenzione")
fdt_scadenza = fds_lista_copy.getitemdatetime(fl_riga, "data_scadenza")

end subroutine

on w_report_registro_uso_attrez_mail.create
int iCurrent
call super::create
this.cb_mail=create cb_mail
this.cb_nessuno=create cb_nessuno
this.cb_tutti=create cb_tutti
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_mail
this.Control[iCurrent+2]=this.cb_nessuno
this.Control[iCurrent+3]=this.cb_tutti
this.Control[iCurrent+4]=this.dw_lista
end on

on w_report_registro_uso_attrez_mail.destroy
call super::destroy
destroy(this.cb_mail)
destroy(this.cb_nessuno)
destroy(this.cb_tutti)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;datastore lds_data, lds_temp
long ll_rows, ll_index, ll_new, ll_index2, ll_gg
string ls_sql, ls_cod_attrezzatura, ls_cod_tipo_manutenzione
datetime ldt_data_rif

setpointer(Hourglass!)
lds_data = s_cs_xx.parametri.parametro_ds_1
setnull(s_cs_xx.parametri.parametro_ds_1)

ll_gg = 5
ldt_data_rif = datetime(relativedate(today(), ll_gg), 00:00:00)

ll_rows = lds_data.rowcount()
for ll_index = 1 to ll_rows
	
	ls_cod_attrezzatura = lds_data.getitemstring(ll_index,1)
	ls_cod_tipo_manutenzione = lds_data.getitemstring(ll_index, 4)	
	
	//recupera le scadenze
	ls_sql = 	"select data_registrazione "+&
					"from manutenzioni "+&
					"where 	cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
								"cod_attrezzatura = '"+ls_cod_attrezzatura+"' and "+&
								"cod_tipo_manutenzione = '"+ls_cod_tipo_manutenzione+"' and "+&
								"flag_eseguito='N' and "+&
								"data_registrazione < '"+string(ldt_data_rif, s_cs_xx.db_funzioni.formato_data) +"' "
								
	f_crea_datastore(lds_temp, ls_sql)
	
	lds_temp.retrieve()
	for ll_index2=1 to lds_temp.rowcount()
		ll_new = dw_lista.insertrow(0)
		
		dw_lista.setitem(ll_new, "cod_attrezzatura", ls_cod_attrezzatura)
		dw_lista.setitem(ll_new,"cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
		dw_lista.setitem(ll_new, "cod_utilizzatore", lds_data.getitemstring(ll_index,2))
		dw_lista.setitem(ll_new, "cod_controllore", lds_data.getitemstring(ll_index, 3))
		
		dw_lista.setitem(ll_new, "data_scadenza", lds_temp.getitemdatetime(ll_index2, 1))
	next
	
	destroy lds_temp
next

setpointer(Arrow!)
end event

type cb_mail from commandbutton within w_report_registro_uso_attrez_mail
integer x = 32
integer y = 28
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "e-mail"
end type

event clicked;long ll_index, ll_rows, ll_i_dest, ll_tot_dest
boolean lb_almeno_uno = false
string ls_resp, ls_resp_hold, ls_elenco, ls_responsabile, ls_controllore, ls_mail_responsabile, ls_mail_controllore
string ls_mittente, ls_cod_attrezzatura, ls_des_attrezzatura, ls_cod_tipo_man, ls_des_tipo_man, ls_smtp
datetime ldt_scadenza
datastore lds_destinatari, lds_lista_copy

if g_mb.messagebox("OMNIA", "Inviare e-mail ai responsabili dei mezzi selezionati?", Question!, YesNo!, 1) = 2 then
	return
end if

dw_lista.accepttext()

//controlla che almeno uno sia selezionato
ll_rows = dw_lista.rowcount()
for ll_index=1 to ll_rows
	if dw_lista.getitemstring(ll_index, "sel")="S" then
		lb_almeno_uno = true
	end if
next

if not lb_almeno_uno then
	g_mb.messagebox("OMNIA", "Selezionare almeno una riga!", Exclamation!)
	return
end if
//-------------------------

//copia tutto in un datastore con stessa struttura
lds_lista_copy = create datastore
lds_lista_copy.dataobject = dw_lista.dataobject
dw_lista.rowscopy(1, ll_rows, Primary!, lds_lista_copy, 1, Primary!)
//-----------------------------------------------------

//carica i destinatari distinct
f_crea_datastore(lds_destinatari, "select cod_operaio from anag_operai where cod_azienda='CODICEINESISTENTE'")
lds_destinatari.retrieve()

for ll_index = 1 to ll_rows
	if dw_lista.getitemstring(ll_index, "sel")<>"S" then continue
	
	wf_leggi_riga(ll_index, lds_lista_copy, ls_responsabile, ls_mail_responsabile, ls_controllore, ls_mail_controllore, ls_smtp, ls_cod_attrezzatura, &
											ls_des_attrezzatura, ls_cod_tipo_man, ls_des_tipo_man, ldt_scadenza)
	wf_carica_dest_distinct(lds_destinatari, ls_responsabile)
next
//---------------------------------

//inizia il ciclo per destinatario
ll_tot_dest = lds_destinatari.rowcount()
for ll_i_dest = 1 to ll_tot_dest
	ls_responsabile = lds_destinatari.getitemstring(ll_i_dest, 1)
	
	//filtra il ds copia per destinatario e riga selezionata
	lds_lista_copy.setfilter("sel='S' and cod_utilizzatore='"+ls_responsabile+"'")
	lds_lista_copy.filter()
	
	//cicla la roba da mandare al destinatario (almeno una ce n'è)
	ll_rows = lds_lista_copy.rowcount()
	ls_elenco = ""
	for ll_index = 1 to ll_rows
		
		//leggi info su riga
		wf_leggi_riga(ll_index, lds_lista_copy, ls_responsabile, ls_mail_responsabile, ls_controllore, ls_mail_controllore, ls_smtp, ls_cod_attrezzatura, &
											ls_des_attrezzatura, ls_cod_tipo_man, ls_des_tipo_man, ldt_scadenza)
											
		//carica row della table html in ls_elenco
		wf_carica_riga(ls_elenco, ls_cod_attrezzatura, ls_des_attrezzatura, ls_cod_tipo_man, ls_des_tipo_man, ldt_scadenza)
	next
	
	//attacco il resto della table html
	wf_carica_table(ls_elenco)
	
	if wf_mail_destinatario(ls_elenco, ls_smtp, ls_controllore, ls_mail_controllore, ls_mail_responsabile) <0 then
		return
	end if
	
next
//--------------------------------


destroy lds_destinatari
destroy lds_lista_copy


end event

type cb_nessuno from commandbutton within w_report_registro_uso_attrez_mail
boolean visible = false
integer x = 2048
integer y = 24
integer width = 402
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Desel.tutti"
end type

event clicked;long ll_index, ll_rows
string ls_sel

ll_rows = dw_lista.rowcount()
ls_sel = "N"

for ll_index = 1 to ll_rows
	dw_lista.setitem(ll_index, "sel", ls_sel)
next
end event

type cb_tutti from commandbutton within w_report_registro_uso_attrez_mail
boolean visible = false
integer x = 1614
integer y = 24
integer width = 402
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sel.tutti"
end type

event clicked;long ll_index, ll_rows
string ls_sel

ll_rows = dw_lista.rowcount()
ls_sel = "S"

for ll_index = 1 to ll_rows
	if dw_lista.object.sel[ll_index].visible = true then dw_lista.setitem(ll_index, "sel", ls_sel)
next
end event

type dw_lista from datawindow within w_report_registro_uso_attrez_mail
integer x = 27
integer y = 128
integer width = 3721
integer height = 1444
integer taborder = 10
string title = "none"
string dataobject = "d_report_registro_uso_attrezz_mail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

