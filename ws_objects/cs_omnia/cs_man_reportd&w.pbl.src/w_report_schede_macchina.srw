﻿$PBExportHeader$w_report_schede_macchina.srw
$PBExportComments$Report Schede Macchina
forward
global type w_report_schede_macchina from w_cs_xx_principale
end type
type dw_report_shede_macchina from uo_cs_xx_dw within w_report_schede_macchina
end type
type cb_annulla from commandbutton within w_report_schede_macchina
end type
type cb_report from commandbutton within w_report_schede_macchina
end type
type cb_selezione from commandbutton within w_report_schede_macchina
end type
type dw_report_shede_macchina_sel from uo_cs_xx_dw within w_report_schede_macchina
end type
end forward

global type w_report_schede_macchina from w_cs_xx_principale
integer width = 3447
integer height = 1840
string title = "Report Schede Macchina"
dw_report_shede_macchina dw_report_shede_macchina
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report_shede_macchina_sel dw_report_shede_macchina_sel
end type
global w_report_schede_macchina w_report_schede_macchina

on w_report_schede_macchina.create
int iCurrent
call super::create
this.dw_report_shede_macchina=create dw_report_shede_macchina
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report_shede_macchina_sel=create dw_report_shede_macchina_sel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_shede_macchina
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_selezione
this.Control[iCurrent+5]=this.dw_report_shede_macchina_sel
end on

on w_report_schede_macchina.destroy
call super::destroy
destroy(this.dw_report_shede_macchina)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report_shede_macchina_sel)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_report_shede_macchina.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_shede_macchina.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_report_shede_macchina_sel.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report_shede_macchina

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_shede_macchina.modify(ls_modify)

this.x = 741
this.y = 885
this.width = 2400
this.height = 550
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_report_shede_macchina_sel, &
                 "area_aziendale", &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_report_shede_macchina_sel, &
                 "cod_attrezzatura", &
                 sqlca, &
                 "anag_attrezzature", &
                 "cod_attrezzatura", &
                 "descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

type dw_report_shede_macchina from uo_cs_xx_dw within w_report_schede_macchina
boolean visible = false
integer x = 23
integer y = 20
integer width = 3360
integer height = 1600
integer taborder = 40
string dataobject = "d_report_shede_macchina"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_annulla from commandbutton within w_report_schede_macchina
integer x = 1417
integer y = 320
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;//close(parent)
dw_report_shede_macchina_sel.reset()
dw_report_shede_macchina_sel.insertrow(0)
dw_report_shede_macchina_sel.setitem(1, "flag_manutenzioni", "U")
end event

type cb_report from commandbutton within w_report_schede_macchina
integer x = 1806
integer y = 320
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_attrezzatura, ls_cod_area_aziendale, ls_flag_manutenzione, ls_cod_attrezzatura_sel, &
		 ls_cod_tipo_manutenzione, ls_cod_operaio, ls_des_attrezzatura, ls_cod_area_aziemdale, ls_des_tipo_manutenzione, &
		 ls_des_area, ls_cognome, ls_periodicita
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_anno_reg_programma, ll_num_reg_programma	 
datetime ldt_data_registrazione
double ld_frequenza

dw_report_shede_macchina_sel.accepttext()
dw_report_shede_macchina.reset()

ls_cod_attrezzatura = dw_report_shede_macchina_sel.getitemstring(1,"cod_attrezzatura")
ls_cod_area_aziendale = dw_report_shede_macchina_sel.getitemstring(1,"area_aziendale")
ls_flag_manutenzione = dw_report_shede_macchina_sel.getitemstring(1,"flag_manutenzioni")

if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = "%"
if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = "%"
if ls_flag_manutenzione = "U" then ls_flag_manutenzione = "%"

ll_i = 1

if ls_cod_attrezzatura = "%" then
	declare cu_manutenzioni cursor for
	 select distinct(a.cod_attrezzatura)
					from manutenzioni a, anag_attrezzature b
				  where a.cod_azienda = :s_cs_xx.cod_azienda
					 and a.cod_attrezzatura like :ls_cod_attrezzatura
					 and a.flag_tipo_intervento like :ls_flag_manutenzione
					 and a.cod_azienda = b.cod_azienda
					 and a.cod_attrezzatura = b.cod_attrezzatura
					 and (b.cod_area_aziendale like :ls_cod_area_aziendale or b.cod_area_aziendale is null);
	open cu_manutenzioni;					 
else					 
	declare cu_manutenzioni_2 cursor for
	 select distinct(a.cod_attrezzatura)
					from manutenzioni a, anag_attrezzature b
				  where a.cod_azienda = :s_cs_xx.cod_azienda
					 and a.cod_attrezzatura = :ls_cod_attrezzatura
					 and a.flag_tipo_intervento like :ls_flag_manutenzione
					 and a.cod_azienda = b.cod_azienda
					 and a.cod_attrezzatura = b.cod_attrezzatura
					 and (b.cod_area_aziendale like :ls_cod_area_aziendale or b.cod_area_aziendale is null);
	open cu_manutenzioni_2;					 
end if					 

do while 1 = 1
	if ls_cod_attrezzatura = "%" then
		fetch cu_manutenzioni into :ls_cod_attrezzatura_sel;
	else
		fetch cu_manutenzioni_2 into :ls_cod_attrezzatura_sel;
	end if	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella manutenzioni " + sqlca.sqlerrtext)
		return
	end if
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		
		declare cu_manutenzioni_tipi cursor for
		select distinct(a.cod_tipo_manutenzione)
					 from manutenzioni a, anag_attrezzature b
				   where a.cod_azienda = :s_cs_xx.cod_azienda
					  and a.cod_attrezzatura like :ls_cod_attrezzatura_sel					  
					  and a.flag_tipo_intervento like :ls_flag_manutenzione
					  and a.cod_azienda = b.cod_azienda
					  and a.cod_attrezzatura = b.cod_attrezzatura
					  and (b.cod_area_aziendale like :ls_cod_area_aziendale or b.cod_area_aziendale is null);
					  
		open cu_manutenzioni_tipi;
		
		do while 1 = 1
			fetch cu_manutenzioni_tipi into :ls_cod_tipo_manutenzione;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in lettura dati da tabella manutenzioni " + sqlca.sqlerrtext)
				return
			end if
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = 0 then	
								
				select max(a.anno_registrazione), 
						 max(a.num_registrazione)
				  into :ll_anno_registrazione,
				  	    :ll_num_registrazione
				  from manutenzioni a, anag_attrezzature b
				 where a.cod_azienda = :s_cs_xx.cod_azienda
					and a.cod_attrezzatura like :ls_cod_attrezzatura_sel					  
					and a.flag_tipo_intervento like :ls_flag_manutenzione
					and a.cod_azienda = b.cod_azienda
					and a.cod_attrezzatura = b.cod_attrezzatura
					and ((b.cod_area_aziendale like :ls_cod_area_aziendale) or (b.cod_area_aziendale is null))
					and a.flag_eseguito like 'S'
					and a.cod_tipo_manutenzione like :ls_cod_tipo_manutenzione;					

				if sqlca.sqlcode < 0 then
						g_mb.messagebox("Omnia", "Errore in lettura dati da tabella manutenzioni " + sqlca.sqlerrtext)
						return
				end if
				if sqlca.sqlcode = 0 then 
					select data_registrazione,
							 cod_operaio,
							 anno_reg_programma,
							 num_reg_programma
					  into :ldt_data_registrazione,
							 :ls_cod_operaio,
							 :ll_anno_reg_programma,
							 :ll_num_reg_programma
					  from manutenzioni
					 where cod_azienda = :s_cs_xx.cod_azienda
						and anno_registrazione = :ll_anno_registrazione
						and num_registrazione = :ll_num_registrazione;
					if sqlca.sqlcode < 0 then
							g_mb.messagebox("Omnia", "Errore in lettura dati da tabella manutenzioni " + sqlca.sqlerrtext)
							return
					end if					
					
					if not isnull(ll_num_reg_programma) and ll_num_reg_programma > 0 then
						select periodicita,
								 frequenza
						  into :ls_periodicita,
						  		 :ld_frequenza
						  from programmi_manutenzione
						 where cod_azienda = :s_cs_xx.cod_azienda
						   and anno_registrazione = :ll_anno_reg_programma
							and num_registrazione = :ll_num_reg_programma;
						if sqlca.sqlcode < 0 then
								g_mb.messagebox("Omnia", "Errore in lettura dati da tabella manutenzioni " + sqlca.sqlerrtext)
								return
						end if												
					end if	  
					
					select descrizione,
							 cod_area_aziendale
					  into :ls_des_attrezzatura,
					  		 :ls_cod_area_aziemdale
					  from anag_attrezzature 
					 where cod_azienda = :s_cs_xx.cod_azienda
					   and cod_attrezzatura = :ls_cod_attrezzatura_sel;
					if sqlca.sqlcode < 0 then
							g_mb.messagebox("Omnia", "Errore in lettura dati da tabella anag_attrezzature " + sqlca.sqlerrtext)
							return
					end if											
						
//					if not isnull(ls_cod_area_aziemdale) then
//						select des_area
//						  into :ls_des_area
//						  from tab_aree_aziendali
//						 where cod_azienda = :s_cs_xx.cod_azienda
//						   and cod_area_aziendale = :ls_cod_area_aziemdale;
//						if sqlca.sqlcode < 0 then
//								messagebox("Omnia", "Errore in lettura dati da tabella tab_aree_aziendali " + sqlca.sqlerrtext)
//								return
//						end if																		
//					end if							
					
					select des_tipo_manutenzione
					  into :ls_des_tipo_manutenzione
					  from tab_tipi_manutenzione
					 where cod_azienda = :s_cs_xx.cod_azienda 
					   and cod_attrezzatura = :ls_cod_attrezzatura_sel
						and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
					if sqlca.sqlcode < 0 then
							g_mb.messagebox("Omnia", "Errore in lettura dati da tabella tab_tipi_manutenzione " + sqlca.sqlerrtext)
							return
					end if
					
					if not isnull(ls_cod_operaio) then
						select cognome
						  into :ls_cognome
						  from anag_operai
						 where cod_azienda = :s_cs_xx.cod_azienda
						   and cod_operaio = :ls_cod_operaio;
						if sqlca.sqlcode < 0 then
								g_mb.messagebox("Omnia", "Errore in lettura dati da tabella anag_operai " + sqlca.sqlerrtext)
								return
						end if																					
					end if	
					
					
					dw_report_shede_macchina.insertrow(0)
					dw_report_shede_macchina.setitem(ll_i, "cod_attrezzatura", ls_cod_attrezzatura_sel)
					dw_report_shede_macchina.setitem(ll_i, "des_attrezzatura", ls_des_attrezzatura)
					dw_report_shede_macchina.setitem(ll_i, "area_aziendale", ls_cod_area_aziemdale)
					dw_report_shede_macchina.setitem(ll_i, "data_tipologia", ldt_data_registrazione)

					if len(ls_des_tipo_manutenzione) > 25 then					
						dw_report_shede_macchina.Object.tipologia_manutenzione.Height.AutoSize = "Yes"
					else 
						dw_report_shede_macchina.Object.tipologia_manutenzione.Height.AutoSize = "No"						
					end if
					dw_report_shede_macchina.setitem(ll_i, "tipologia_manutenzione", ls_des_tipo_manutenzione)
					
					if len(ls_cognome) > 25 then
						dw_report_shede_macchina.Object.operatore.Height.AutoSize = "Yes"
					else 
						dw_report_shede_macchina.Object.operatore.Height.AutoSize = "No"						
					end if					
					dw_report_shede_macchina.setitem(ll_i, "operatore", ls_cognome)
					
					if ld_frequenza < 2 then
						if ls_periodicita = "M" then ls_periodicita = "Minuto"
						if ls_periodicita = "O" then ls_periodicita = "Ora"
						if ls_periodicita = "G" then ls_periodicita = "Giorno"
						if ls_periodicita = "S" then ls_periodicita = "Settimana"
						if ls_periodicita = "E" then ls_periodicita = "Mese"						
					else
						if ls_periodicita = "M" then ls_periodicita = "Minuti"
						if ls_periodicita = "O" then ls_periodicita = "Ore"
						if ls_periodicita = "G" then ls_periodicita = "Giorni"
						if ls_periodicita = "S" then ls_periodicita = "Settimane"
						if ls_periodicita = "E" then ls_periodicita = "Mesi"
					end if	
					
					dw_report_shede_macchina.setitem(ll_i, "periodicita", ls_periodicita)
					dw_report_shede_macchina.setitem(ll_i, "frequenza", ld_frequenza)
					
					ll_i ++ 
					
				end if			
			end if
		loop
		close cu_manutenzioni_tipi;
	end if
loop	
if ls_cod_attrezzatura = "%" then
	close cu_manutenzioni;		
else	
	close cu_manutenzioni_2;		
end if	
parent.x = 100
parent.y = 50
parent.width = 3447
parent.height = 1841

dw_report_shede_macchina.setsort("cod_attrezzatura A, data_tipologia A, tipologia_manutenzione A")
dw_report_shede_macchina.sort()
dw_report_shede_macchina.groupcalc()

dw_report_shede_macchina.change_dw_current()

dw_report_shede_macchina.show()

end event

type cb_selezione from commandbutton within w_report_schede_macchina
integer x = 3017
integer y = 1640
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_report_shede_macchina_sel.hide()
dw_report_shede_macchina.hide()

dw_report_shede_macchina_sel.change_dw_current()
dw_report_shede_macchina_sel.show()

parent.x = 741
parent.y = 885
parent.width = 2400
parent.height = 550

end event

type dw_report_shede_macchina_sel from uo_cs_xx_dw within w_report_schede_macchina
integer x = 23
integer y = 20
integer width = 2194
integer height = 300
integer taborder = 50
string dataobject = "d_report_shede_macchina_sel"
boolean border = false
end type

event pcd_new;call super::pcd_new;dw_report_shede_macchina_sel.setitem(1, "flag_manutenzioni", "U")
end event

