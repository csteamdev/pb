﻿$PBExportHeader$w_report_simulazioni.srw
$PBExportComments$Finestra report simulazioni
forward
global type w_report_simulazioni from w_cs_xx_principale
end type
type cb_elabora from commandbutton within w_report_simulazioni
end type
type cb_report from commandbutton within w_report_simulazioni
end type
type cb_selezione from commandbutton within w_report_simulazioni
end type
type dw_selezione from uo_cs_xx_dw within w_report_simulazioni
end type
type dw_report_sim_elenco from uo_cs_xx_dw within w_report_simulazioni
end type
end forward

global type w_report_simulazioni from w_cs_xx_principale
integer width = 4357
integer height = 2284
string title = "Report Simulazioni"
cb_elabora cb_elabora
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report_sim_elenco dw_report_sim_elenco
end type
global w_report_simulazioni w_report_simulazioni

type variables
integer ii_e_a_d
end variables

forward prototypes
public subroutine wf_repot_apparecchiature ()
public subroutine wf_report_elenco ()
public subroutine wf_report_riepilogo ()
end prototypes

public subroutine wf_repot_apparecchiature ();string ls_ente, ls_appalto, ls_ditta_offerente, ls_cod_attrezzatura, ls_descrizione, ls_fabbricante, &
		 ls_modello, ls_num_matricola, ls_note, ls_cod_area_aziendale, ls_cod_divisione, ls_des_area, ls_des_divisione, ls_compiti
long ll_count_programmi, ll_i

dw_selezione.accepttext()

ls_ente = dw_selezione.getitemstring(dw_selezione.getrow(), "ente")
ls_appalto = dw_selezione.getitemstring(dw_selezione.getrow(), "appalto")
ls_ditta_offerente = dw_selezione.getitemstring(dw_selezione.getrow(), "ditta_offerente")

dw_report_sim_elenco.DataObject = 'd_report_sim_attrezzature'

dw_report_sim_elenco.reset()

declare cu_attrezzature cursor for 
 select cod_attrezzatura,
 		  descrizione, 
		  fabbricante,
		  modello,
		  num_matricola,
		  note,
		  cod_area_aziendale
	from anag_attrezzature
  where cod_azienda = :s_cs_xx.cod_azienda;
  
open cu_attrezzature;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in apertura cursore cu_attrezzature " + sqlca.sqlerrtext)
	return
end if

ll_i = 1

do while 1 = 1
	fetch cu_attrezzature into :ls_cod_attrezzatura,
										:ls_descrizione, 
										:ls_fabbricante,
										:ls_modello,
										:ls_num_matricola,
										:ls_note,
										:ls_cod_area_aziendale;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore lettura dalla tabella anag_attrezzature " + sqlca.sqlerrtext)
		return
	end if										
	if sqlca.sqlcode = 100 then exit 
	
	select count(anno_registrazione)
	  into :ll_count_programmi
	  from programmi_manutenzione
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_attrezzatura = :ls_cod_attrezzatura;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore lettura dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		return
	end if											
	
	if not isnull(ll_count_programmi) and ll_count_programmi > 0 then

		select des_area,
				 cod_divisione
		  into :ls_des_area,
		  		 :ls_cod_divisione
		  from tab_aree_aziendali
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_area_aziendale = :ls_cod_area_aziendale;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_aree_aziendali " + sqlca.sqlerrtext)
			return
		end if	
		
		select des_divisione,
				 compiti
		  into :ls_des_divisione,
		  		 :ls_compiti
		  from anag_divisioni
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_divisione = :ls_cod_divisione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_divisioni " + sqlca.sqlerrtext)
			return
		end if					
		
		dw_report_sim_elenco.insertrow(0)
		dw_report_sim_elenco.setitem(ll_i, "ente", ls_ente)
		dw_report_sim_elenco.setitem(ll_i, "appalto", ls_appalto)
		dw_report_sim_elenco.setitem(ll_i, "cod_divisione", ls_cod_divisione)		
		dw_report_sim_elenco.setitem(ll_i, "des_divisione", ls_des_divisione)
		dw_report_sim_elenco.setitem(ll_i, "indirizzo", ls_compiti)
		dw_report_sim_elenco.setitem(ll_i, "cod_area", ls_cod_area_aziendale)
		dw_report_sim_elenco.setitem(ll_i, "des_area", ls_des_area)		
		dw_report_sim_elenco.setitem(ll_i, "des_attrezzatura", ls_descrizione)		
		dw_report_sim_elenco.setitem(ll_i, "fabbricante", ls_fabbricante)		
		dw_report_sim_elenco.setitem(ll_i, "modello", ls_modello)
		dw_report_sim_elenco.setitem(ll_i, "matricola", ls_num_matricola)
		dw_report_sim_elenco.setitem(ll_i, "note", ls_note)
		dw_report_sim_elenco.setitem(ll_i, "ditta_offerente", ls_ditta_offerente)	
		
		ll_i ++		
	end if	
loop
close cu_attrezzature;
		
dw_report_sim_elenco.setsort("cod_divisione A, des_attrezzatura A")
dw_report_sim_elenco.sort()
dw_report_sim_elenco.groupcalc()										
end subroutine

public subroutine wf_report_elenco ();string ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
		 ls_cod_cat_attrezzature, ls_ente, ls_appalto, ls_ditta_offerente, ls_compiti, ls_des_divisione, &
		 ls_des_area, ls_des_reparto, ls_des_risorsa, ls_des_attrezzatura, ls_des_cat_attrezzatura, ls_des_tipo_manutenzione, &
		 ls_flag_stampa, ls_cod_divisione_comodo
long ll_num_attrezzatura, ll_i
double ld_tempo_utilizzo, ll_tot_ore, ll_tot_ore_complessivo, ld_tempo_utilizzo_ore

dw_report_sim_elenco.DataObject = 'd_report_sim_elenco'

dw_report_sim_elenco.reset()
ll_i = 1
ll_tot_ore_complessivo = 0

declare cu_simulazione cursor for
select cod_divisione,
		 cod_area_aziendale, 
		 cod_reparto,
		 cod_attrezzatura,
		 cod_tipo_manutenzione,
		 cod_cat_attrezzature,
		 num_attrezzature,
		 tempo_utilizzo,
		 des_ente,
		 des_appalto,
		 des_ditta_offerente,
		 indirizzo_divisione
  from simulazione_manutenzioni		 
 where cod_azienda = :s_cs_xx.cod_azienda
order by cod_divisione, cod_area_aziendale, cod_reparto;
		 
open cu_simulazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in apertura cu_simulazione " + sqlca.sqlerrtext)
	return
end if

do while 1 = 1
  fetch cu_simulazione into :ls_cod_divisione,
									 :ls_cod_area_aziendale,
									 :ls_cod_reparto,
									 :ls_cod_attrezzatura,
									 :ls_cod_tipo_manutenzione,
									 :ls_cod_cat_attrezzature,
									 :ll_num_attrezzatura,
									 :ld_tempo_utilizzo,
									 :ls_ente,
									 :ls_appalto,
									 :ls_ditta_offerente,
									 :ls_compiti;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella simulazione_manutenzioni " + sqlca.sqlerrtext)
		return
	end if									 
	if sqlca.sqlcode = 100 then exit
	
	if ls_cod_divisione_comodo <> ls_cod_divisione then
		ll_tot_ore = 0
		ls_cod_divisione_comodo = ls_cod_divisione
	end if
			
	dw_report_sim_elenco.insertrow(0)
	dw_report_sim_elenco.setitem(ll_i, "ente", ls_ente)
	dw_report_sim_elenco.setitem(ll_i, "appalto", ls_appalto)
	dw_report_sim_elenco.setitem(ll_i, "cod_divisione", ls_cod_divisione)
	
	select des_divisione
	  into :ls_des_divisione
	  from anag_divisioni
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_divisione = :ls_cod_divisione;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_divisioni " + sqlca.sqlerrtext)
		return
	end if									 		
	
	dw_report_sim_elenco.setitem(ll_i, "des_divisione", ls_des_divisione)
	dw_report_sim_elenco.setitem(ll_i, "indirizzo", ls_compiti)
	dw_report_sim_elenco.setitem(ll_i, "cod_area", ls_cod_area_aziendale)

	select des_area
	  into :ls_des_area
	  from tab_aree_aziendali
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_area_aziendale = :ls_cod_area_aziendale;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_aree_aziendali " + sqlca.sqlerrtext)
		return
	end if									 		

	dw_report_sim_elenco.setitem(ll_i, "des_area", ls_des_area)
	dw_report_sim_elenco.setitem(ll_i, "cod_reparto", ls_cod_reparto)

	select des_reparto
	  into :ls_des_reparto
	  from anag_reparti
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_reparto = :ls_cod_reparto;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_reparti " + sqlca.sqlerrtext)
		return
	end if									 				
		
	dw_report_sim_elenco.setitem(ll_i, "des_reparto", ls_des_reparto)
	dw_report_sim_elenco.setitem(ll_i, "cod_attrezzatura", ls_cod_attrezzatura)
	
	select descrizione
	  into :ls_des_attrezzatura
	  from anag_attrezzature
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_attrezzatura = :ls_cod_attrezzatura;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_attrezzature " + sqlca.sqlerrtext)
		return
	end if			
	
	dw_report_sim_elenco.setitem(ll_i, "des_attrezzatura", ls_des_attrezzatura)
	dw_report_sim_elenco.setitem(ll_i, "cod_tipo_manutenzione", ls_cod_tipo_manutenzione)
	
	select des_tipo_manutenzione
	  into :ls_des_tipo_manutenzione
	  from tab_tipi_manutenzione
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_attrezzatura = :ls_cod_attrezzatura
		and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_tipi_manutenzione " + sqlca.sqlerrtext)
		return
	end if				
	
	dw_report_sim_elenco.setitem(ll_i, "des_tipo_manutenzione", ls_des_tipo_manutenzione)	
	dw_report_sim_elenco.setitem(ll_i, "cod_risorsa", ls_cod_cat_attrezzature)
	
	select des_cat_attrezzature
	  into :ls_des_cat_attrezzatura
	  from tab_cat_attrezzature
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_cat_attrezzature = :ls_cod_cat_attrezzature;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_cat_attrezzature " + sqlca.sqlerrtext)
		return
	end if					
	
	dw_report_sim_elenco.setitem(ll_i, "des_risorsa", ls_des_cat_attrezzatura)
	dw_report_sim_elenco.setitem(ll_i, "quantita", ll_num_attrezzatura)
	ld_tempo_utilizzo_ore = ld_tempo_utilizzo / 60
	dw_report_sim_elenco.setitem(ll_i, "tempo_unitario", ld_tempo_utilizzo_ore)	
	dw_report_sim_elenco.setitem(ll_i, "ditta_offerente", ls_ditta_offerente)	
				
	ll_tot_ore = ll_tot_ore + (ld_tempo_utilizzo / 60)
	ll_tot_ore_complessivo = ll_tot_ore_complessivo + (ld_tempo_utilizzo / 60)
	dw_report_sim_elenco.setitem(dw_report_sim_elenco.getrow(), "totale_ore", ll_tot_ore)						
	ll_i ++
	
loop
close cu_simulazione;

dw_report_sim_elenco.setitem(dw_report_sim_elenco.getrow(), "totale_ore_complessivo", ll_tot_ore_complessivo)							

dw_report_sim_elenco.setsort("cod_divisione A, cod_area_aziendale A, cod_reparto A, cod_attrezzatura A, cod_tipo_manutenzione A, cod_cat_attrezzatura A")
dw_report_sim_elenco.sort()
dw_report_sim_elenco.groupcalc()
end subroutine

public subroutine wf_report_riepilogo ();string ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
		 ls_cod_cat_attrezzature, ls_ente, ls_appalto, ls_ditta_offerente, ls_compiti, ls_des_divisione, &
		 ls_des_area, ls_des_reparto, ls_des_risorsa, ls_des_attrezzatura, ls_des_cat_attrezzatura, ls_des_tipo_manutenzione, &
		 ls_flag_stampa, ls_cod_divisione_comodo, ls_cod_risorsa, ls_cod_divisione_c, ls_cod_cat_attrezzature_c, &
		 ls_ente_c, ls_appalto_c, ls_ditta_offerente_c, ls_compiti_c	 
long ll_num_attrezzatura, ll_i, ll_tot_risorsa, ll_sqlcode
double ld_tempo_utilizzo, ld_tempo_utilizzo_ore, ll_tot_ore, ll_tot_ore_complessivo
boolean lb_prima_volta

dw_report_sim_elenco.DataObject = 'd_report_sim_riepilogo'

dw_report_sim_elenco.reset()
ll_tot_ore_complessivo = 0
lb_prima_volta = true

declare cu_simulazione cursor for
select cod_divisione,
		 cod_area_aziendale, 
		 cod_reparto,
		 cod_attrezzatura,
		 cod_tipo_manutenzione,
		 cod_cat_attrezzature,
		 num_attrezzature,
		 tempo_utilizzo,
		 des_ente,
		 des_appalto,
		 des_ditta_offerente,
		 indirizzo_divisione
  from simulazione_manutenzioni		 
 where cod_azienda = :s_cs_xx.cod_azienda
order by cod_divisione, cod_cat_attrezzature;
		 
open cu_simulazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in apertura cu_simulazione " + sqlca.sqlerrtext)
	return
end if

do while 1 = 1
  fetch cu_simulazione into :ls_cod_divisione,
									 :ls_cod_area_aziendale,
									 :ls_cod_reparto,
									 :ls_cod_attrezzatura,
									 :ls_cod_tipo_manutenzione,
									 :ls_cod_cat_attrezzature,
									 :ll_num_attrezzatura,
									 :ld_tempo_utilizzo,
									 :ls_ente,
									 :ls_appalto,
									 :ls_ditta_offerente,
									 :ls_compiti;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella simulazione_manutenzioni " + sqlca.sqlerrtext)
		return
	end if				

	ll_sqlcode = sqlca.sqlcode
	
	if lb_prima_volta and sqlca.sqlcode = 100 then exit
	
	if lb_prima_volta then
		ls_cod_risorsa = ls_cod_cat_attrezzature
		lb_prima_volta = false
	end if
	
	if ls_cod_risorsa <> ls_cod_cat_attrezzature or ll_sqlcode = 100 then
//////////////////////////////////////////////////////////////////////////////
		if ls_cod_divisione_comodo <> ls_cod_divisione then
			ll_tot_ore = 0
			ls_cod_divisione_comodo = ls_cod_divisione
		end if
				
		ll_i = dw_report_sim_elenco.insertrow(0)
		dw_report_sim_elenco.setitem(ll_i, "ente", ls_ente_c)
		dw_report_sim_elenco.setitem(ll_i, "appalto", ls_appalto_c)
		dw_report_sim_elenco.setitem(ll_i, "cod_divisione", ls_cod_divisione_c)
		
		select des_divisione
		  into :ls_des_divisione
		  from anag_divisioni
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_divisione = :ls_cod_divisione_c;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_divisioni " + sqlca.sqlerrtext)
			return
		end if									 		
		
		dw_report_sim_elenco.setitem(ll_i, "des_divisione", ls_des_divisione)
		dw_report_sim_elenco.setitem(ll_i, "indirizzo", ls_compiti_c)
		dw_report_sim_elenco.setitem(ll_i, "cod_risorsa", ls_cod_cat_attrezzature_c)
		
		select des_cat_attrezzature
		  into :ls_des_cat_attrezzatura
		  from tab_cat_attrezzature
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_cat_attrezzature = :ls_cod_cat_attrezzature_c;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_cat_attrezzature " + sqlca.sqlerrtext)
			return
		end if					
		
		dw_report_sim_elenco.setitem(ll_i, "des_risorsa", ls_des_cat_attrezzatura)
		ld_tempo_utilizzo_ore = ll_tot_risorsa / 60
		dw_report_sim_elenco.setitem(ll_i, "tempo_unitario", ld_tempo_utilizzo_ore)	
		dw_report_sim_elenco.setitem(ll_i, "ditta_offerente", ls_ditta_offerente_c)	
					
		ll_tot_ore = ll_tot_ore + (ll_tot_risorsa / 60)
		ll_tot_ore_complessivo = ll_tot_ore_complessivo + (ll_tot_risorsa / 60)
		dw_report_sim_elenco.setitem(ll_i, "totale_ore", ll_tot_ore)						
		
//////////////////////////////////////////////////////////////////////////////		
		ll_tot_risorsa = 0
		ls_cod_risorsa = ls_cod_cat_attrezzature
	end if	

	if ll_sqlcode = 100 then exit		
	
	ls_cod_divisione_c = ls_cod_divisione
	ls_cod_cat_attrezzature_c = ls_cod_cat_attrezzature
	ll_tot_risorsa = ll_tot_risorsa + ld_tempo_utilizzo
	ls_ente_c = ls_ente
	ls_appalto_c = ls_appalto
	ls_ditta_offerente_c = ls_ditta_offerente
	ls_compiti_c = ls_compiti

loop
close cu_simulazione;

dw_report_sim_elenco.setitem(ll_i, "totale_ore_complessivo", ll_tot_ore_complessivo)							

dw_report_sim_elenco.setsort("cod_divisione A, cod_cat_attrezzatura A")
dw_report_sim_elenco.sort()
dw_report_sim_elenco.groupcalc()
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report_sim_elenco.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_sim_elenco.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
//iuo_dw_main = dw_report

this.x = 677
this.y = 589
this.width = 2510
this.height = 1300
cb_selezione.hide()
end event

on w_report_simulazioni.create
int iCurrent
call super::create
this.cb_elabora=create cb_elabora
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report_sim_elenco=create dw_report_sim_elenco
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_elabora
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report_sim_elenco
end on

on w_report_simulazioni.destroy
call super::destroy
destroy(this.cb_elabora)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report_sim_elenco)
end on

type cb_elabora from commandbutton within w_report_simulazioni
integer x = 2085
integer y = 1092
integer width = 357
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elabora"
end type

event clicked;string ls_cod_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, &
		 ls_cod_cat_attrezzature, ls_des_ente, ls_des_appalto, ls_des_ditta_offerente, ls_indirizzo_divisione, ls_periodicita, &
		 ls_flag_priorita, ls_ente, ls_appalto, ls_ditta_offerente, ls_flag_stampa, ls_des_risorsa, ls_compiti
long ll_num_attrezzature, ll_frequenza, ll_i, ll_minuti, ll_ore, ll_giorni, ll_secondi, ll_differenza, ll_tempo_esecuzione, ll_y
double ld_tempo_utilizzo
datetime ldt_data_forzata, ldt_data_da, ldt_data_a, ldt_data_ciclo, ldt_date_sim[], ldt_null[]
date ldd_data
time lt_time

dw_selezione.accepttext()

ldt_data_da = dw_selezione.getitemdatetime(dw_selezione.getrow(), "data_da")
ldt_data_a = dw_selezione.getitemdatetime(dw_selezione.getrow(), "data_a")
ls_ente = dw_selezione.getitemstring(dw_selezione.getrow(), "ente")
ls_appalto = dw_selezione.getitemstring(dw_selezione.getrow(), "appalto")
ls_ditta_offerente = dw_selezione.getitemstring(dw_selezione.getrow(), "ditta_offerente")
ls_flag_stampa = dw_selezione.getitemstring(dw_selezione.getrow(), "flag_stampa")

delete from simulazione_manutenzioni;

declare cu_programmi cursor for
 select cod_attrezzatura,
		  cod_tipo_manutenzione,
		  periodicita,
		  frequenza,
		  flag_priorita, 
		  data_forzata
	from programmi_manutenzione
  where cod_azienda = :s_cs_xx.cod_azienda;	
  
open cu_programmi;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in apertura cursore cu_programmi " + sqlca.sqlerrtext)
	return
end if

do while 1 = 1
	fetch cu_programmi into :ls_cod_attrezzatura,
									:ls_cod_tipo_manutenzione,
									:ls_periodicita,
									:ll_frequenza,
									:ls_flag_priorita,
									:ldt_data_forzata;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
		return
	end if	
	if sqlca.sqlcode = 100 then exit
	
	select cod_reparto
	  into :ls_cod_reparto
	  from anag_attrezzature
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_attrezzatura = :ls_cod_attrezzatura;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_attrezzature " + sqlca.sqlerrtext)
		return
	end if	
	if isnull(ls_cod_reparto) then		
		g_mb.messagebox("Omnia", "Attenzione il cod_reparto non può essere vuoto  " + sqlca.sqlerrtext)
		return		
	end if
	
	select cod_area_aziendale
	  into :ls_cod_area_aziendale
	  from anag_reparti
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_reparto = :ls_cod_reparto;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_reparti " + sqlca.sqlerrtext)
		return
	end if	
	if isnull(ls_cod_area_aziendale) then		
		g_mb.messagebox("Omnia", "Attenzione il cod_area_aziendale non può essere vuoto  " + sqlca.sqlerrtext)
		return		
	end if	

	select cod_divisione
	  into :ls_cod_divisione
	  from tab_aree_aziendali
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_area_aziendale = :ls_cod_area_aziendale;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella tab_aree_aziendali " + sqlca.sqlerrtext)
		return
	end if	
	if isnull(ls_cod_divisione) then		
		g_mb.messagebox("Omnia", "Attenzione il cod_divisione non può essere vuoto  " + sqlca.sqlerrtext)
		return		
	end if		
	
	select compiti
	  into :ls_compiti
	  from anag_divisioni
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_divisione = :ls_cod_divisione;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati dalla tabella anag_divisioni " + sqlca.sqlerrtext)
		return
	end if			
		
	
	if ls_flag_priorita = "S" then
		if ldt_data_forzata >= ldt_data_da and &
			ldt_data_forzata <= ldt_data_a then
			ldt_data_da = ldt_data_forzata
		end if	
	end if	

	ldt_data_ciclo = ldt_data_da

	ll_i = 1
	
	do while ldt_data_ciclo >= ldt_data_da and &
		ldt_data_ciclo <= ldt_data_a 
		
		ldt_date_sim[ll_i] = ldt_data_ciclo
		
		choose case ls_periodicita
			case "M" 
				ll_giorni = int(ll_frequenza / (24 * 60))
				ll_minuti = ll_frequenza - (ll_giorni * (24 * 60))
				ll_secondi = 60 * ll_minuti
				
				ll_differenza = SecondsAfter(time(ldt_data_ciclo), 23:59:59)
				if ll_differenza > ll_secondi then
					ldd_data = RelativeDate (date(ldt_data_ciclo), ll_giorni)
					lt_time = RelativeTime (time(ldt_data_ciclo), ll_secondi)
					ldt_data_ciclo = datetime(ldd_data, lt_time)
				else 
					ldd_data = RelativeDate (date(ldt_data_ciclo), (ll_giorni + 1))
					lt_time = RelativeTime (time(ldt_data_ciclo), (ll_secondi - ll_differenza))
					ldt_data_ciclo = datetime(ldd_data, lt_time)				
				end if
			case "O"
				ll_giorni = int(ll_frequenza / 24)
				ll_minuti = (ll_frequenza - (ll_giorni * 24)) * 60
				ll_secondi = 60 * ll_minuti
				
				ll_differenza = SecondsAfter(time(ldt_data_ciclo), 23:59:59)
				if ll_differenza > ll_secondi then
					ldd_data = RelativeDate (date(ldt_data_ciclo), ll_giorni)
					lt_time = RelativeTime (time(ldt_data_ciclo), ll_secondi)
					ldt_data_ciclo = datetime(ldd_data, lt_time)
				else 
					ldd_data = RelativeDate (date(ldt_data_ciclo), (ll_giorni + 1))
					lt_time = RelativeTime (time(ldt_data_ciclo), (ll_secondi - ll_differenza))
					ldt_data_ciclo = datetime(ldd_data, lt_time)				
				end if				
			case "G"
				ldd_data = RelativeDate (date(ldt_data_ciclo), ll_frequenza)				
				ldt_data_ciclo = datetime(ldd_data)
			case "S"			
				ldd_data = RelativeDate (date(ldt_data_ciclo), (ll_frequenza * 7))				
				ldt_data_ciclo = datetime(ldd_data)				
			case "E"			
				ldd_data = RelativeDate (date(ldt_data_ciclo), (ll_frequenza * 30))				
				ldt_data_ciclo = datetime(ldd_data)								
		end choose			
		ll_i ++ 
	loop
	
	declare cu_elenco_risorse cursor for
	 select cod_cat_attrezzature,
	        des_risorsa,
			  tempo_esecuzione
		from elenco_risorse
	  where cod_azienda = :s_cs_xx.cod_azienda
	    and cod_attrezzatura = :ls_cod_attrezzatura
		 and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
	open cu_elenco_risorse;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in apertura cursore cu_elenco_risorse " + sqlca.sqlerrtext)
		return
	end if
	
	do while 1 = 1
		fetch cu_elenco_risorse	into :ls_cod_cat_attrezzature, 
											  :ls_des_risorsa, 
											  :ll_tempo_esecuzione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore lettura dati dalla tabella elenco_risorse " + sqlca.sqlerrtext)
			return
		end if											  
		if sqlca.sqlcode = 100 then exit
		
		ld_tempo_utilizzo = 0 
		for ll_y = 1 to upperbound(ldt_date_sim)
			ld_tempo_utilizzo = ld_tempo_utilizzo + ll_tempo_esecuzione
		next									  

		insert into simulazione_manutenzioni (cod_azienda,
														  cod_divisione,
														  cod_area_aziendale,
														  cod_reparto,
														  cod_attrezzatura,
														  cod_tipo_manutenzione,
														  cod_cat_attrezzature,
														  num_attrezzature,
														  tempo_utilizzo,
														  des_ente,
														  des_appalto,
														  des_ditta_offerente,
														  indirizzo_divisione)		
												 values (:s_cs_xx.cod_azienda,
												 			:ls_cod_divisione,
															:ls_cod_area_aziendale,
															:ls_cod_reparto,
															:ls_cod_attrezzatura,
															:ls_cod_tipo_manutenzione,
															:ls_cod_cat_attrezzature,
															1,
															:ld_tempo_utilizzo,
															:ls_ente,
															:ls_appalto,
															:ls_ditta_offerente,
															:ls_compiti);				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in inserimento dati nella tabella simulazioni_manutenzioni " + sqlca.sqlerrtext)
			return 
		end if	
		
	loop
	close cu_elenco_risorse;
	
	ldt_date_sim[] = ldt_null[]	
loop	
close cu_programmi;

g_mb.messagebox("Omnia", "Elaborazione Effettuata!")
end event

type cb_report from commandbutton within w_report_simulazioni
integer x = 1655
integer y = 1092
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_flag_stampa

dw_selezione.accepttext()
ls_flag_stampa = dw_selezione.getitemstring(dw_selezione.getrow(), "flag_stampa")

if ls_flag_stampa = "E" then
	wf_report_elenco()
elseif ls_flag_stampa = "R" then 	
	wf_report_riepilogo()	
elseif ls_flag_stampa = "A" then	
	wf_repot_apparecchiature()
end if

cb_elabora.hide()
cb_report.hide()
dw_selezione.hide()

parent.x = 20
parent.y = 20
parent.width = 4361
parent.height = 2284

dw_report_sim_elenco.change_dw_current()
iuo_dw_main = dw_report_sim_elenco

dw_report_sim_elenco.show()
cb_selezione.show()
end event

type cb_selezione from commandbutton within w_report_simulazioni
integer x = 3881
integer y = 2080
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_report_sim_elenco.hide()
cb_selezione.hide()

parent.x = 677
parent.y = 589
parent.width = 2510
parent.height = 1300

dw_selezione.show()
cb_report.show()
cb_elabora.show()


end event

type dw_selezione from uo_cs_xx_dw within w_report_simulazioni
event ue_ultimo ( )
integer x = 9
integer y = 20
integer width = 2464
integer height = 1080
integer taborder = 10
string dataobject = "d_report_simulazione"
boolean border = false
end type

event ue_ultimo;string ls_stringa
uo_dividi_stringa luo_dividi_stringa

luo_dividi_stringa = create uo_dividi_stringa

if ii_e_a_d = 1 then
	ls_stringa = dw_selezione.getitemstring(dw_selezione.getrow(), "ente")
	luo_dividi_stringa.wf_dividi_stringa(ref ls_stringa)		
	dw_selezione.setitem(dw_selezione.getrow(), "ente", ls_stringa)
elseif ii_e_a_d = 2 then
	ls_stringa = dw_selezione.getitemstring(dw_selezione.getrow(), "appalto")
	luo_dividi_stringa.wf_dividi_stringa(ref ls_stringa)		
	dw_selezione.setitem(dw_selezione.getrow(), "appalto", ls_stringa)
elseif ii_e_a_d = 3 then
	ls_stringa = dw_selezione.getitemstring(dw_selezione.getrow(), "ditta_offerente")
	luo_dividi_stringa.wf_dividi_stringa(ref ls_stringa)		
	dw_selezione.setitem(dw_selezione.getrow(), "ditta_offerente", ls_stringa)
end if	
destroy luo_dividi_stringa
end event

event pcd_new;call super::pcd_new;dw_selezione.setitem(1, "flag_stampa", "R")
end event

event itemchanged;call super::itemchanged;//if i_extendmode then
//	choose case i_colname
//		case "ente"
//			ii_e_a_d = 1
//			dw_selezione.postevent("ue_ultimo")
//		case "appalto"
//			ii_e_a_d = 2
//			dw_selezione.postevent("ue_ultimo")			
//		case "ditta_offerente"
//			ii_e_a_d = 3
//			dw_selezione.postevent("ue_ultimo")			
//	end choose
//end if	
end event

type dw_report_sim_elenco from uo_cs_xx_dw within w_report_simulazioni
boolean visible = false
integer x = 23
integer y = 20
integer width = 4265
integer height = 2032
integer taborder = 10
string dataobject = "d_report_sim_elenco"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

